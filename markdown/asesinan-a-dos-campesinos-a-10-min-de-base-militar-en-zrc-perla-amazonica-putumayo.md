Title: Asesinan a dos campesinos cerca a base militar en ZRC "Perla amazónica" Putumayo
Date: 2015-03-19 19:00
Author: CtgAdm
Category: DDHH, Nacional
Tags: Asesina a dos campesinos en la ZRC del Putumayo
Slug: asesinan-a-dos-campesinos-a-10-min-de-base-militar-en-zrc-perla-amazonica-putumayo
Status: published

###### Foto:Putumayo.gov.co 

###### [**Entrevista con [Wilson Medina Guerrero]:**] 

<iframe src="http://www.ivoox.com/player_ek_4237667_2_1.html?data=lZegmZuae46ZmKiakpiJd6KmlZKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRhdTZ1M7bw9OPpYzY0NiYxcbRtMbnytPc1ZDIqYzgwpDHtKiPqMbgjLXi1trRpdrjjoqkpZKns8%2FowszW0ZC2pcXd0JCah5yncZU%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

En la noche del martes 17 de Marzo, en la Comunidad de La Cabaña, Corregimiento La Carmelita, Municipio de Puerto Asís, Putumayo fueron asesinados **LUIS DE JESÚS RODRÍGUEZ PARADA** y su compañera sentimental **ADENIS JIMENEZ GUITIERREZ** quienes se encontraban en su vivienda.

Al lugar de residencia de los campesinos ingresaron sujetos desconocidos portando machetes y un arma de fuego, atacaron primero con machete a Luis, le cortaron el brazo izquierdo y le causaron otras heridas, luego le propinaron varios disparos que le quitaron la vida.

Mientras esto ocurría su compañera sentimental alcanzó a correr, gritó pidiendo auxilio, en ese momento **recibió varios machetazos en la cabeza que destruyeron su cráneo**, después le propinaron varios disparos con una pistola 9mm.

Integrantes de la comunidad de La Cabaña observaron una motocicleta salir del lugar y huir con rumbo desconocido, instantes después la comunidad llegó hasta el sitio de los acontecimientos encontrando los dos cuerpos sin vida.

Un empleado de la funeraria, quien dijo tener autorización de la Sijin para el levantamiento, lo realizó sin tomar las medidas necesarias para la **salvaguarda de evidencias importantes en el esclarecimiento del hecho**.

Según los pobladores, todos estos detalles se pueden inferir con el acta de levantamiento, y además los vecinos del sector escucharon los gritos y los disparos. Desde el momento de ocurridos los hechos, miembros de la comunidad informaron a las autoridades competentes sin que estas se hicieran presentes.

En la zona, hay una fuerte **presencia de tropas adscritas a la Brigada 27** de Selva custodiando instalaciones e infraestructura petrolera del Consorcio Colombia Energy, de hecho la **vivienda de la pareja** asesinada queda a menos de **10 minutos de la Base militar del Porvenir**.

Esta comunidad hace parte del **Corredor Puerto Vega – Teteyé**, cuyas comunidades históricamente se han movilizado en férrea oposición a la expansión de la industria petrolera debido al deterioro ambiental y conflictos sociales que ésta genera.

**Luis de Jesús Rodríguez** estaba afiliado a la comunidad de La Española perteneciente a la **Zona de Reserva Campesina Perla Amazónica**, desde hace aproximadamente un año se encontraba viviendo en la Comunidad de la Cabaña donde trabajaba en conjunto con su compañera en una ladrillera de la cual era socio.

Según **Wilson Medina Guerrero**, presidente de la JAC de la comunidad **"La española"**, Luis Rodríguez era miembro de la comunidad durante más de 20 años, tenía el cargo de fiscal de la vereda y era una persona de **"...máxima confianza para la comunidad por sus labores de paz.."**.
