Title: Fantasías caníbales en el Teatro R101
Date: 2017-11-16 11:54
Category: Cultura
Tags: Bogotá, teatro
Slug: fantasias-canibales-teatro
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/11/FANTASÍAS-CANIBALES-Púrpura-Creactivo-Foto-Rubén-Darío-Marín-Cortés-5.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Rubén Marín 

###### 16 Nov 2017

La compañía teatral Púrpura creativo presenta **del 16 al 25 de noviembre en el Teatro R101** de Bogotá su más reciente montaje: **Fantasías Caníbales**. Una pieza construida bajo el sello característico de la agrupación: un universo lleno de elementos venidos del gótico, el cómic, el neobarroco y las corrientes pop.

William Guevara Quiroz, escribe y dirige una historia de canibalismo contemporáneo, mismo que la agrupación caracteriza como parte de "**ese cotidiano en el que estamos dispuestos a comernos a nuestro semejante, ya sea por amor, por odio o por venganza**".

La obra viene de presentarse en el Teatro Matacandelas de Medellín y en el Teatro Pablo Tobón Uribe de la misma ciudad como agrupación invitada del **Festival Colombiano de Teatro Ciudad de Medellín**; en el XIII Festival de Teatro de Bogotá, y regresa al Teatro R101 para entregarse "en carne y alma" como ellos aseguran, en 6 funciones, al público capitalino.

<iframe src="https://www.youtube.com/embed/kIIvqUyN9Bk" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

**Sinopsis:**

Al son de música salvaje, Alex, un artista plástico vanguardista, se encuentra a Chicasuave;  
al saberse admirado por ella la invita a su gruta posiblemente con el deseo de comérsela. Alex habita con tres personajes extraños con los que tiene una relación tensa, pero que le permite sentirse en peligro; dos hermanos caníbales traídos de alguna selva tropical y un gato esfinge, de aquellos sin pelo, refinado, agresivo, justiciero y amante del brócoli y la zanahoria.

Esa noche Alex encuentra en Chicasuave, de día llamada Rodolfo, la imagen de la mujer que le destrozó el corazón dejándoselo como un patacón aplastado y quebradizo; al saber esto, la chica Rodolfo entre conversaciones y café sin azúcar, se ofrece como cuerpo de venganza de ese dolor que la convertirá en la protagonista de una gran instalación con un único corazón latiente.  ¿Qué tan caníbal es usted?

**Elenco:**  Virley Mendoza, Libardo Mejía, Juan Pablo Sánchez, Johan Poveda, Ferney Niño y William Guevara Quiroz.

Producción Púrpura Creativo

**Duración:** 60 minutos  
Público Joven y Adulto  
Teatro R101, Calle 70A \# 11 - 29  
Hora: 8:00 p.m.  
Boletería General \$20.000 Estudiantes \$15.000
