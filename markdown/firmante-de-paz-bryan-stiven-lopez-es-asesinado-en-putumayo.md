Title: Firmante de paz, Bryan Stiven López es asesinado en Putumayo
Date: 2020-11-21 10:23
Author: AdminContagio
Category: Actualidad, Nacional
Tags: asesinato de excombatientes, Firmante de paz, Putumayo
Slug: firmante-de-paz-bryan-stiven-lopez-es-asesinado-en-putumayo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/08/Excombatientes-e1567187945860.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: Contagio Radio

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Este viernes 20 de noviembre, el Partido FARC denunció el asesinato del firmante de paz Bryan Stiven López en zona rural de Puerto Caicedo, departamento de Putumayo.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/PartidoFARC/status/1329757780694228994","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/PartidoFARC/status/1329757780694228994

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

De acuerdo con el [Instituto de Estudios para el Desarrollo y la Paz - Indepaz-](https://twitter.com/Indepaz/status/1329763047112646656/photo/1), el excombatiente fue asesinado el jueves 19 de noviembre en el sector veredal El Picudo, ubicado a 3 horas del casco urbano de Puerto Caicedo. **En este lugar fue hallado su cuerpo con varios impactos de arma de fuego.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Bryan Stiven López se encontraba en proceso de reincorporación en el ETCR Heiler Mosquera - La Prada en el municipio de Puerto Asís.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Con Bryan López **ya son 242 los excombatientes asesinados desde que se firmó el acuerdo de paz entre **el extinto grupo armado de las FARC-EP** y el Gobierno Santos.** Además, es el cuarto asesinato de un firmante de paz en casi una semana, tras los crímenes perpetrados en los municipios de Chocó, Caquetá y Putumayo, en que perdieron la vida Heiner Cuesta Mena, Enod Lopez Verjano y Jorge Riaños. (Le puede interesar: [Tres firmantes de paz asesinados en Chocó, Caquetá y Putumayo](https://archivo.contagioradio.com/tres-firmantes-de-paz-asesinados-en-choco-caqueta-y-putumayo/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Denunciando estos hechos, el 21 de octubre se llevó a cabo **La Peregrinación por la Vida y por la Paz** en que excombatientes de FARC de diversos municipios del país se movilizaron para exigir el respeto y garantía a la vida de los excombatientes y el cumplimiento de los Acuerdos de Paz.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Igualmente, ante la falta de respuestas y acciones por parte del Gobierno para garantízar la protección de la vida de los excombatientes y sus familias, este 19 de noviembre, el Partido Farc se unió al Paro Nacional en la lucha ***«por la vida, la democracia y la negociación del pliego de emergencia**»*. (Le puede interesar: [ELN muestra voluntad para un Acuerdo Humanitario en el Catatumbo](https://archivo.contagioradio.com/eln-muestra-voluntad-para-un-acuerdo-humanitario-en-el-catatumbo/))

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
