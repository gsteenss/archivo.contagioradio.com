Title: La Chispa. Lanzamiento de disco
Date: 2020-10-19 14:48
Author: AdminContagio
Category: eventos
Tags: La Chispa, Martinica Canto Vivo
Slug: la-chispa-lanzamiento-de-disco
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/10/La-Chispa-lanzamiento.png" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/10/La-Chispa.mp3" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/10/El-rio-lo-sabia-mix1Agosto25.2020limiter.01_01.wav" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: Fundación Casa Cultural La Chispa

<!-- /wp:paragraph -->

<!-- wp:audio {"id":91693} -->

<figure class="wp-block-audio">
<audio controls src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/10/La-Chispa.mp3">
</audio>
</figure>
<!-- /wp:audio -->

<!-- wp:paragraph -->

Abrazadas por montañas y al lado de un río que a veces se pierde en medio del valle; caminando de calle en calle bajo la sombra de Cascos de Vaca y Guayacanes; despertándonos al son del Bichofué y vistos desde el cielo por halcones y loros; bailando tangos y salsa en rincones de la ciudad; parando entre palmas creciendo al lado de los semáforos y contemplando cómo se expanden cada vez más las luces de las casas colina arriba hasta casi tocar el cielo, nos unimos artistas, activistas y educadores.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Aunque no nacimos en el Pacífico sur colombiano, ni nos criamos al lado de sus músicas tradicionales, sentimos un llamado a honrar la memoria históricamente recogida en la música de marimba y en diversas manifestaciones de los territorios. Canto Vivo es un proyecto que recoge poesía escrita colectivamente y creación musical, donde se entrelazan los sonidos tradicionales con la musicalidad que nos habita, con la intención de elevar las voces resistentes y cantarle a la Colombia de hoy. (Le puede interesar: ["Las cuarentenas abiertas de América Latina"](https://archivo.contagioradio.com/las-cuarentenas-abiertas-de-america-latina/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Cantamos hoy por aquellos que todavía sueñan, escriben, cantan, comparten y creen en otras realidades posibles; le cantamos al río, al monte, a los caminos y a la tierra que tantos y tantas han liberado, y a la que está por liberar; le cantamos al arte, al amor, a la creatividad, a la colectividad, y cantamos para que el miedo se vaya; cantamos hoy por la justicia, la dignidad, por la fuerza de los pueblos, por la sangre derramada, por quienes no volverán, por la esperanza, por la memoria viva y el olvido desterrado; cantamos por los niños y las niñas y el futuro que habitarán.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":5} -->

##### Unimos hoy nuestras voces y les invitamos a que unan las suyas para que quienes tengan de sobra repartan, para que movamos las aguas, para que crucemos corazones, para que construyamos el mundo que queremos y nos re encantemos en él.

<!-- /wp:heading -->

<!-- wp:paragraph -->

Cantamos en agradecimiento a todos y todas quienes han y siguen liderando o participando de luchas sociales, ambientales y políticas en Colombia y alrededor del mundo. Cantamos a los maestros y las maestras que han mantenido vivas las tradiciones del Pacífico Sur y del resto del país. Gratitud a cada persona que ha luchado desde la Fundación Casa Cultural La Chispa, habitando este espacio y construyendo en conjunto, convencidas de que estamos para reinventar y transformar.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Llegó el lanzamiento de [Martinica, Canto Vivo](https://www.facebook.com/events/346197569991604/); un homenaje a las luchas políticas, sociales y ambientales.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

**Link del evento:** https://www.facebook.com/events/346197569991604/

<!-- /wp:paragraph -->

<!-- wp:audio {"id":91695} -->

<figure class="wp-block-audio">
<audio controls src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/10/El-rio-lo-sabia-mix1Agosto25.2020limiter.01_01.wav">
</audio>
</figure>
<!-- /wp:audio -->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->
