Title: Bombardeos en López de Micay desplazan a 240 personas
Date: 2015-06-05 17:38
Category: Nacional, Paz
Tags: afrodescendientes, bombardeos, campesinas, campesinos, Cauca, cese unilateral de las FARC, dialogos de paz, ejercito, Frente Amplio por la PAz, López de Micay, paz
Slug: bombardeos-en-lopez-de-micay-desplazan-a-240-personas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/06/desplazamiento-contagioradio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: semana.com 

##### <iframe src="http://www.ivoox.com/player_ek_4602160_2_1.html?data=lZudlJaadI6ZmKiakp6Jd6Kpk5KSmaiRdo6ZmKiakpKJe6ShkZKSmaiRdpWkjMnS1dXQpdvVxdTgjcrSb8bgjKjO18jFb9Hj05DP0dLGpdPYxtTgj4qbh4630NPhw8zNs4zGwsnW0ZCRaZi3jpk%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe> 

##### [Quilian Cuero Ruíz, párroco del municipio de López de Micay] 

###### [05 Jun 2015]

Desde el pasado 29 de mayo, el **municipio de López de Micay ha sido epicentro de bombardeos** y enfrentamientos **entre las FFMM y guerrilla**. **240 personas **se han venido desplazando, algunos de ellos al caso urbano del municipio, y otros hacia otras veredas

El día 4 de Junio, entre **campesinos, campesinas y afrodescendientes** del departamento del Cauca, quienes **se han encontrado en medio del combate**, optaron por desplazarse de sus hogares a diversas veredas lejanas y algunos hacia el casco urbano, ante los constantes bombardeos, exactamente en **Cacahual y Valente\***. Se han ubicado en la cabecera principal de la Zona de López de Micay con la intención de salvar sus vidas.

El municipio está habitado por cerca de 19.000 personas que en su mayoría habitan en la zona rural del municipio, los cuales han estado con la **constante presencia del Ejército a las afueras de la cabecera muncipal**, declara el párroco Quilian Cuero Ruíz.

Según el párroco, la **situación de las familias es delicada**, pues **no tienen espacios donde dormir**, hay ausencia de colchonetas y no han recibido la atención  necesaria; la presencia de menores de edad es alta y no han recibido la atención adecuada. Al mismo tiempo, asegura que **no hay recursos en la alcaldía para brindar ayudas**. El secretario de gobierno, en ausencia del alcalde, intenta enfrentar la situación, pero **hay una ausencia de logística** que impide avanzar para hallar soluciones rápidas. Por el momento no ha habido algún pronunciamiento de parte del gobierno departamental o nacional.

Por último, el párroco anuncia que anteriormente, al existir un cese unilateral del fuego, sentían tranquilidad y paz, sin embargo, ante las nuevas decisiones en los diálogos de paz y el rompimiento del mismo, se han presentado problema psicosociales en la comunidad, “hay una tensión terrible, todo mundo está pensando en salir”.

En caso de repetirse los enfrentamientos, hay la **posibilidad de que los y las habitantes se desplacen a las veredas más lejanas**, convirtiéndose en aproximadamente **500 personas las desplazadas.**

**\***Nombre no se descifra.
