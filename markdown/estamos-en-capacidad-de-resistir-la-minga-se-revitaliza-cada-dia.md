Title: Estamos en capacidad de resistir, la Minga se revitaliza cada día
Date: 2019-04-01 13:18
Category: Comunidad, Movilización
Tags: Minga en el Suroccidente, Minga Nacional
Slug: estamos-en-capacidad-de-resistir-la-minga-se-revitaliza-cada-dia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/D21Oc3lX4AANpNx.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: ONIC] 

###### [1 Abr 2019] 

Tras 22 días de Minga en el suroccidente del país, el pueblo indígena del Cauca ha planteado que la negociación con el Gobierno debe desplazarse de **Mandivá a  Mondomo**,  sector que hace parte del espacio territorial de la movilización, a su vez, sus integrantes han manifestado que tres semanas después de movilización, la voluntad de diálogo y su capacidad de resistencia continúa firme.

**Giovanni Yule, dinamizador político del CRIC**, se refirió a su vez a la réplica del senador Feliciano Valencia indicando que el Gobierno no se ha manifestado hasta ahora, solo lo que se ha conocido a través de los medios, pero esperan que al restablecer la mesa, se dé "una conversación seria, clara y coherente frente al país que nos permita prepararnos para la llegada del señor presidente de la República". [(Lea también: Minga responde a Duque: lo esperamos, venga, conversemos y busquemos soluciones)](https://archivo.contagioradio.com/minga-responde-duque-esperamos-venga-y-conversemos/)

[**Autonomía de los mingueros garantiza sostenibilidad de la Minga**]

Yule afirma que han establecido una coordinación entre los mingueros alternando su estadía en el lugar, "unos van y descansan y otros llegan, hay una capacidad organizativa que nos permite a nosotros estar ahí" explica el dinamizador quien agrega que el tema de la alimentación también está cubierto por cada comunidad, garantizando que haya comida suficiente, **"preveíamos que iba a ser un pulso duro con el Gobierno, pero lo cierto es que la Minga se revitaliza a medida que pasan los días"** concluye.

Añade Giovanni Yule qué, como parte del proceso organizativo, los cerca de 20.000 mingueros se someten a evaluación para conocer su proyección, reafirmando que están en capacidad de seguir resistiendo, una determinación que se ve alimentada por los diferentes colectivos y comunidades que se han unido a la movilización con el pasar de los días, adelantó que en las siguientes horas se sumarán a la Minga, las comunidades afro del Cauca. [(Le puede interesar: ¡Con fuerza! La Minga Pijao Nasa también avanza en Tolima)](https://archivo.contagioradio.com/con-fuerza-la-minga-pijao-nasa-tambien-avanza-en-tolima/)

<iframe id="audio_33946394" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_33946394_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
