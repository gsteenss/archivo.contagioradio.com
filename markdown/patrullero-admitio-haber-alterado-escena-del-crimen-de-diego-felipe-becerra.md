Title: Patrullero admitió haber alterado escena del crimen de Diego Felipe Becerra
Date: 2016-07-18 21:36
Category: Judicial, Nacional
Tags: brutalidad policial, grafitero, policia
Slug: patrullero-admitio-haber-alterado-escena-del-crimen-de-diego-felipe-becerra
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/07/Homenaje-a-Diego-Felipe-Becerra.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Ola política 

###### [18 Jul 2016] 

Este lunes el patrullero Giovanny Tovar, admitió que él fue quien llevó **el arma de fuego que apareció** en la escena del** asesinato del** **grafitero Diego Felipe Becerra, para hacer creer que esta pertenecía** al joven.** Una declaración que para la abogada de la familia, significa que si** **“hubo una clara alteración de la escena del crimen”.**

La audiencia se adelantó luego de haber sido aplazada tres veces debido a las persistentes amenazas en contra de Tovar y su abogado, **Jairo Acuña**. De hecho, en diciembre del 2015, personas desconocidas ingresaron al apartamento del abogado,** robaron documentos del proceso y mataron a su perro. **

**De acuerdo con** **Gustavo Trejos, padre de Diego Felipe en la alteración de la escena del crimen, hay trece personas implicadas**, de las cuales tres son civiles y el resto policías. 9 de ellos han decidido contar la verdad incluyendo el testimonio de subintendente Giovanny Tovar que reconoció que llevó el arma con la que pretendieron justificar el asesinato.

Es así como el  patrullero aseguró que **él consiguió el arma, la disparó en el humedal Córdoba**, antes de ponerla en la escena del crimen. Tovar** sería condenado por porte ilegal de armas** y seguiría contribuyendo con la justicia.

A pocos días de cumplirse cinco años en el asesinato de Diego Felipe Becerra, este avance es uno de los pocos pasos importantes que ha dado la justicia para encontrar la verdad frente al asesinato de "Trípido”, señala la familia.

<iframe src="http://co.ivoox.com/es/player_ej_12271252_2_1.html?data=kpefmZaWeZOhhpywj5WcaZS1k56ah5yncZOhhpywj5WRaZi3jpWah5yncajp1NnO2NSPmNPZy9TgjZKPlMLY08qYps7Jq9Cfp8rZy9XJb6PZxMrf1MaRaZi3jqjc0NnFq8rjjLfOxs7Tb46ZmKialg..&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
