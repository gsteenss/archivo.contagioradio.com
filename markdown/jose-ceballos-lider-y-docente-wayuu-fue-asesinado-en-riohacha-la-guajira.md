Title: José Ceballos, líder y docente Wayúu fue asesinado en Riohacha, La Guajira
Date: 2019-02-19 18:08
Author: AdminContagio
Category: Comunidad, Líderes sociales
Tags: La Guajira, Líderes indígenas asesinados, Riohacha
Slug: jose-ceballos-lider-y-docente-wayuu-fue-asesinado-en-riohacha-la-guajira
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/02/52347529_2093538797368578_1855107254018113536_n-770x400.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Nación Wayúu] 

###### [20 Feb 2019] 

Este 19 de febrero en horas de la mañana fue asesinado el docente y **líder índígena wayúu José Víctor Ceballos Epinayu**  al ser abordado por dos hombres que se desplazaban en motocicleta mientras salía de su casa, preparándose para  cumplir con sus labores como educador en Riohacha, La Guajira.

Víctor Ceballos era sobrino de **Edwin Ceballos Sijona**, autoridad tradicional de la comunidad Aljote y un activo integrante del movimiento Nacion Wayúu, ONG que exigido al Gobierno y a la comunidad internacional que actúe de forma eficaz frente a los asesinatos y agresiones que se vienen cometiendo contra los líderes sociales y la comunidad campesina. [(Lea también: 35 indígenas han sido asesinados durante gobierno Duque: ONIC)](https://archivo.contagioradio.com/asesinados-gobierno-duque/)

De hecho, tanto Víctor como Edwin habían aparecido en panfletos amenazantes que se conocieron en 2018, razón por la que Edwin había recibido medidas de protección por parte de la Unidad Nacional de Protección (UNP); sin embargo, Víctor no había recibido protección, porque su función social se centraba en la enseñanza para su comunidad en La Plazoleta, lugar cercano al Municipio de Riohacha.

Las autoridades Wayúu no saben quienes pudieron dar la orden para que los sicarios atentaran contra la vida del Maestro, razón por la que han manifestado la necesidad de que la Fiscalía avance prontamente sobre este caso, no obstante, Policía ni Fiscalía han emitido pronunciamiento alguno sobre los hechos. (Le puede interesar: ["Incineran cada de reuniones W. en Katsaliamana, Guajira"](https://archivo.contagioradio.com/incineran-casa-reuniones-wayuu-katsaliamana-guajira/))

Debido a su labor en defensa de los derechos humanos, los integrantes de la ONG Nación Wayúu han sido objeto de numerosas amenazas y atentados, según la **Organización Nacional Indígena de Colombia (ONIC)** para finales de 2018, la cifra de indígenas asesinados en Colombia ascendía a 35. Por esta razón, durante este miércoles, una delegación de derechos humanos de la Unión Europea se reunió con los indígenas de la Guajira para encontrar posibles soluciones a estos temas.

<iframe id="audio_32716963" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_32716963_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en] [[su correo][[Contagio Radio]
