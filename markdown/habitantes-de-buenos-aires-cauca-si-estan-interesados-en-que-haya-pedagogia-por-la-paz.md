Title: Habitantes de Buenos Aires, Cauca, están interesados en que haya pedagogía por la paz
Date: 2016-03-04 11:35
Category: Nacional, Paz
Tags: Conversaciones de paz de la habana, FARC, Juan Manuel Santos, pedagogia por la paz
Slug: habitantes-de-buenos-aires-cauca-si-estan-interesados-en-que-haya-pedagogia-por-la-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/03/buenos_aires_cauca_contagioradio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: buenosaires-cauca.gov] 

<iframe src="http://co.ivoox.com/es/player_ek_10674683_2_1.html?data=kpWjmZmafJShhpywj5WdaZS1kZiah5yncZOhhpywj5WRaZi3jpWah5yncanVw87hw9PYqdSfxcqYpNrJstDnjKbW1MrXaZO3jKjO18jFaZO3hqifh6aUt8qfxtjhh6iXaaKlz5DW0NnJtsbnwsmah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Ari Aragón] 

###### [04 Mar 2016] 

Pobladores del Municipio de [Buenos Aires en el departamento del Cauca](https://archivo.contagioradio.com/?s=buenos+aires+cauca), negaron las afirmaciones que se han venido haciendo en torno a supuestas presiones por parte de las FARC, que se encuentran en esa población y realizan una actividad de pedagogía con integrantes de esa guerrilla, además **negaron que haya contacto alguno con la población civil.**

Según el relato de Ari Aragón, uno de los habitantes, se conoce que la delegación de paz está haciendo presencia en zona rural del municipio pero **se desconoce el sitio en el que están reunidos**, además ningún civil ha hecho parte de esta actividad, contrario a lo que se ha venido afirmando en algunos medios de información.

Aragón, afirma además que la población si está interesada en conocer las dos versiones en torno a los acuerdos que se han alcanzado entre las delegaciones de paz, **pero desde las dos versiones y no solamente la versión oficial que llega a través de los medios de información** privados y de las FFMM, que tienen cobertura en todo el departamento “las FARC también tienen su versión” y se desconoce.

Sin embargo, una actividad como esta también representa algunos riesgos. Aragón resalta que luego de un despliegue tan grande como el que se está haciendo, [se corre el riesgo de las retaliaciones por parte de paramilitares o de las mismas fuerzas militares](https://archivo.contagioradio.com/se-agrava-crisis-de-derechos-humanos-en-el-cauca/) y agrega que otro de los riesgos es que **combatientes de las FARC pasen a ser parte de las filas del ELN,** en ese sentido afirma que ya se están escuchando algunos rumores.
