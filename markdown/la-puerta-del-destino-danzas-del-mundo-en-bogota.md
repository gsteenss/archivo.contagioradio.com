Title: La puerta del destino, danzas del mundo en Bogotá
Date: 2019-05-30 09:32
Author: CtgAdm
Category: Cultura
Tags: áfrica, Bogotá, China, danza
Slug: la-puerta-del-destino-danzas-del-mundo-en-bogota
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/image001-1.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/UdqnZlyr.jpeg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: mcortés-fotografía 

Como espacio para las artes y las expresiones que se viven a través del movimiento, La Factoría L’explose  durante este 2019 mantiene sus puertas abiertas a diferentes géneros y ritmos en su franja de eventos,  como fueron sus exitosas temporadas de tango y arrabal, y ahora d**anzas del Mundo, una muestra de  baile y sonidos africanos, persas, indios, chinos,** que magistralmente amalgamados deleitaran a sus espectadores.

Gracias al trabajo conjunto entre la compañía Elixir Danza y Murid Emsable presentaran en este teatro capitalino **La Puerta del Destino**, una puesta en escena  que tiene como objetivo **expresar la búsqueda de lo extraordinario y maravilloso en aquello que es ordinario y común utilizando la danza**.

Bajo la dirección general de **Johanna Vargas** y la musical de **Jaime Quijano**, cuatro músicos y cuatro bailarinas tomaran como base **algunas tradiciones del Norte de África -Magreb-, la zona de influencia Persa, el subcontinente indio y la tradición China** para mostrarnos bellas danzas y el pensamiento místico común en las expresiones espirituales de estos pueblos, elemento integrador e importante para este montaje, que en escena refleja la vivencia interior de estas tradiciones como parte del camino personal de trascendencia y auto conocimiento.

![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/image001-1-300x174.jpg){.aligncenter .wp-image-68046 width="501" height="290"}

**La Puerta del Destino hace alusión al mandato del cielo: el destino como un designio del espíritu de forma tal que sea éste quien guíe la materia y no al revés,** que todas las acciones se orienten al bienestar colectivo y al servicio, más que a la ganancia personal.

Un llamado a encontrar el talento para cumplir nuestra misión de vida y hallar el propósito de nuestra existencia con los otros. La respuesta a ese llamado es diferente en cada circunstancia, pero en esencia, es la misma. **Un reconocimiento del origen sagrado de la** **humanidad y una búsqueda para realizar el ser.**

Esta obra, que fue ganadora de la beca de creación ensamble música – danza del Portafolio Distrital de Estímulos 2018, **se presentara los días jueves 30, viernes 31 de mayo y sábado 1 de junio a las 8:00 p.m**. con un bono de apoyo \$35.000 para público general y \$28.000 para estudiantes y tercera edad. Si desea conocer más información puede consultar la página oficial de La Factoría L’explose.
