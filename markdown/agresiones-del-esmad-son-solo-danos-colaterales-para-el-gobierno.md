Title: Agresiones del ESMAD son solo "daños colaterales" para el Gobierno
Date: 2019-12-12 17:29
Author: CtgAdm
Category: Movilización, Paro Nacional, Política
Tags: Agresiones del ESMAD, Cámara de Representantes, Desmonte del ESMAD
Slug: agresiones-del-esmad-son-solo-danos-colaterales-para-el-gobierno
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/Contagio-Radio.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

Cerca de siete horas duró el debate convocado por la oposición para examinar el accionar del Escuadrón Móvil Antidisturbios (ESMAD) en la Cámara de Representantes, jornada que no solo buscó indagar sobre su uso desmedido de la fuerza , sino las directrices que obedecen y cómo han desembocado en la violación de DD.HH de manifestantes que se oponen a las medidas  de un Gobierno, que evidenció durante la audiencia su negativa al desmonte de esta unidad de la Policía.

El representante a la Cámara, David Racero resalta su preocupación tras el resultado del debate pues fue evidente que por parte del Gobierno no hay muestras de reflexión sincera ni de auto crítica ante las fotografías y videos que demuestran cómo el ESMAD sobrepasa el uso de la fuerza contra la protesta social, "para ellos son algunos fallos colaterales, algunas eventualidades". [(Lea también: Indignación en las calles por asesinato de Dilan Cruz)](https://archivo.contagioradio.com/indignacion-en-las-calles-por-asesinato-de-dilan-cruz/)

### Sin auto crítica por parte del Gobierno será más de lo mismo 

Aunque desde la bancada de oposición se han planteado soluciones inmediatas como el no uso de las denominadas "armas menos letales" que han causado daños directos en la población y asesinado a 35 personas a lo largo de 20 años, Racero considera que el Gobierno "no comprende la dinámica del país  y no interpreta el sentir popular".  [(Le recomendamos leer: En veinte años el ESMAD ha asesinado a 34 personas)](https://archivo.contagioradio.com/en-veinte-anos-el-esmad-ha-asesinado-a-34-personas/)

Las agresiones del ESMAD han ido en aumento con el pasar del tiempo, a medida que, como señalan defensores de DD.HH. se han convertido en **"un agente de ataque a la manifestación social",** llegando a un total de 540 victimizaciones para 2017 y que en el marco del paro nacional han dejado más de 300 personas heridas según la Campaña Defender la Libertad, sin mencionar el asesinato del joven Dilan Cruz y la captura de forma irregular de estudiantes. [(Lea también:“Están atacando es para matarnos”: integrante de la Primera Línea sobre el ESMAD)](https://archivo.contagioradio.com/estan-atacando-es-para-matarnos-integrante-de-la-primera-linea-sobre-el-esmad/)

Al respecto, la Procuraduría ya anunció que asumirá una investigación sobre los sucesos de la noche del 10 de diciembre, cuando la Policía obligó a una joven a abordar un vehículo que no tenía los distintivos de la institución, situación que fue detectada por un ciudadano que siguió al vehículo mientras grababa, hasta que este se detuvo y le permitieron salir.  [(Lea también: Las razones para pedir el desmonte del ESMAD a 20 años de su creación)](https://archivo.contagioradio.com/las-razones-para-pedir-el-desmonte-del-esmad-a-20-anos-de-su-creacion/)

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/video.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2Fvideos%2F574669780017795%2F&amp;show_text=0&amp;width=333" width="333" height="476" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

### El propósito del ESMAD no obedece a las necesidades de la actualidad

A su vez, congresitas como David Racero o María José Pizarro también indicaron cómo esta división de la Policía le **ha costado a los colombianos 357.000 millones de pesos entre 1999-2006.** y cómo, según cifras del Ministerio de Hacienda, en términos de aumento proporcional este ha sido de 1456% en los últimos 10 años, frente el presupuesto destinado a la educación y el agro en el país, ha sido superior el otorgado al ESMAD.

Mientras, congresistas del Centro Democrático e integrantes del Gobierno, representado en el debate por  el ministro de Defensa, Carlos Holmes Trujillo y de Interior, Nancy Patricia Gutierrez afirman que el accionar del ESMAD responde conforme a tratados internacionales y que el armamento que utiliza cumple con los procedimientos debidos, justificando su intervención; Racero es enfático en que el tema principal del debate debía girar entorno a cómo esas formas deben ser replanteadas, pues su creación en 1999 obedece a una realidad diferente a la que se vive en la actualidad.

Frente a los argumentos que ha utilizado el Gobierno para justificar el uso de la violencia, el representante señala que el ESMAD debe ser el último paso que el Estado debe dar y no hacerlo ante el derecho a la protesta, **"creen que fortalecer las instituciones de la Fuerza Pública tiene que ver con tapar lo que está pasando y con la política de la impunidad y del silencio, para fortalecer al contrario hay que protegerla y limpiarla"** agregando que los colombianos han sido testigos que en las manifestaciones en las que el ESMAD ha estado ausente, estas se han desarrollado de forma pacífica.

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

<iframe id="audio_45516458" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_45516458_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
