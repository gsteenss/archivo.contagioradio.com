Title: Las incoherencias de Colombia después del Acuerdo de París frente al fracking
Date: 2016-09-20 12:32
Category: Ambiente y Sociedad, Opinion
Tags: Acuerdos de Paris, Fracking colombia, petroleo
Slug: las-incoherencias-de-colombia-despues-del-acuerdo-de-paris-frente-al-fracking
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/09/ft-colombia-petroleo-rp.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Ecoosfera 

#### [**Por: **Marisa Amorim Fonseca. Pasante Universidade Paris Descartes** - Ambiente y Sociedad **](https://archivo.contagioradio.com/margarita-florez/) 

###### 20 Sep 2016 

El fracking es un método de producción de energía que consiste en bombear una mezcla de sustancias químicas y de agua en el suelo para romper rocas que contienen petróleo y gas. Este proceso de energía tiene un marco legal que está establecido en la resolución 87042 de 2012 del Ministerio de Minas y Energía de Colombia –modificado por las resoluciones de 2012 y 2013– y que explica el funcionamiento de la exploración y explotación de yacimientos no convencionales.

La Agencia Nacional de Hidrocarburos (ANH) asegura que el marco regulatorio es ”estable y fuerte”. No obstante, resulta insuficiente ya que no existe en Colombia una institución con la capacidad política para controlar esta actividad. La página web de la ANH solo divulga informaciones que estén en favor del fracking lo que da una visión muy distorsionada acerca de ese modo de producción.

Existen todavía varias amenazas alrededor del fracking: en primer lugar, hay un riesgo sobre el medio ambiente porque la utilización de productos químicos tiene impactos en el suelo y la salud de las personas. Existe también un impacto sobre los recursos hídricos porque extracción requiere de mucha agua. Asimismo, este tipo de exploración también puede incidir en el aumento de actividades sísmicas. De hecho son varios los estudios muestran el vínculo entre esa actividad y el aumento de los terremotos.

A pesar de estos riesgos, Colombia sigue apostando en energías fósiles y en el fracking, ambos incompatibles con el Acuerdo de París. Si bien este Acuerdo no dice claramente que el fracking es prohibido, el artículo 2 supone que este procedimiento no sea compatible con el espíritu del Acuerdo: “Mantener el aumento de la temperatura media mundial muy por debajo de 2°C con respecto a los niveles preindustriales, y proseguir los esfuerzos para limitar ese aumento de la temperatura a 1,5°C con respecto a los niveles preindustriales, reconociendo que ello reduciría considerablemente los riesgos y los efectos del cambio climático”. Es importante subrayar que aunque el fracking emite poco CO2, sí emite metano, otro gas de efecto invernadero.

El Estado colombiano fomenta el fracking en lugar de invertir en energías limpias, retrasando el cambio de energía. Además, el marco jurídico de Colombia no es lo suficientemente sólido para regular tal actividad, y muchas preguntas aún no tienen respuesta. Por ejemplo, existe la posibilidad de pedir una autorización para usar este método en varios pozos, sin considerar que cada pozo tiene sus características propias.

También hay un silencio sobre el “experto” designado en el artículo 19 de la resolución de 2012 y sobre las normas de inspección que se tiene que seguir. De modo que no sabemos todavía cuáles son los mecanismos de control que existen. Por otro lado, la resolución de 2013 impone una profundidad de no menos de 150 pies debajo del acuífero aprovechable para el consumo humano. Sin embargo, podríamos preguntarnos si este rango es suficiente para evitar la contaminación.

También hay que decir que las investigaciones realizadas sobre las zonas en que se está pensando hacer el fracking son enunciadas de forma muy general y poco precisa. Por ejemplo, no hay un inventario completo de los acuíferos subterráneos de Colombia, de modo que permitir la explotación en zonas donde la información es muy escasa es arriesgarse a contaminar acuíferos limpios.

Además, el problema de la consulta popular sigue pendiente puesto que una parte de la población está contra este procedimiento. De hecho, tales opiniones están silenciadas en muchos medios de comunicación.

El 29 de julio de 2016, la Asamblea Departamental de Santander rechazó al fracking, pero la Asamblea no tiene el poder legal para suspender esas actividades y se sigue explorando y explotando sin tener en cuenta la opinión de los habitantes, en una acción contraria a la democracia.

Por tales motivos Colombia no se encuentra preparada para el fracking porque no tiene el conocimiento técnico y ambiental de los yacimientos no convencionales. En ese aspecto está cometiendo los mismos errores en que incurrieron los Estados Unidos, que hoy están abandonando tal actividad debido a impactos ambientales graves. ¿Cómo podemos invertir en una energía que es abandonada por la mayoría de los países?

Colombia firmó el acuerdo de París y, por tanto, debe cumplir sus obligaciones de reducción de 20% de los gases de efecto invernadero para 2030, pero también debe incluir la adaptación y la mitigación. Estas metas solo se podrán cumplir si Colombia renuncia al fracking para invertir en energías limpias como las solares, eólicas o hidráulicas, así como diversificarlas para tener energía suficiente para todo el país, pero además se deben establecer nuevas políticas públicas que tengan el financiamiento necesario para su efectividad.
