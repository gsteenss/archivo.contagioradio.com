Title: Con homicidio de Servio Cuasaluzán, son 134 excombatientes asesinados
Date: 2019-06-25 10:28
Author: CtgAdm
Category: DDHH, Nacional
Tags: asesinato de excombatientes, etcr, nariño, Partido FARC
Slug: con-homicidio-de-servio-cuasaluzan-son-134-excombatientes-asesinados
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/02/Asesinato-lider-indigena.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Foto: Archivo] 

Este fin de semana, se conoció del asesinato del reincorporado Servio Delio Cuasaluzán Guanga, en la vereda Chambú, del municipio de Ricaurte en Nariño. Cuasaluzán estaba acreditado como excombatiente en el ETCR de La Variante, Tumaco.

Con este homicidio, van 17 asesinados y dos desaparecidos de excombatientes solamente en el sector de Tumaco. Mientras tanto, el último comunicado del Partido FARC informó que se han registrado[ 133 muertes violentas de reincorporados y 11 desapariciones forzadas] desde la firma del Acuerdo de Paz. (Le puede interesar: "Á[nderson Pérez, excombatiente y comunicador fue asesinado en Cauca](https://archivo.contagioradio.com/anderson-perez-excombatiente-y-comunicador-fue-asesinado-en-cauca/)")

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
