Title: Delimitación de Páramos en Colombia: Nuevos páramos
Date: 2016-04-12 12:57
Category: Ambiente y Sociedad, Opinion
Tags: Ambiente, delimitación de páramos, páramos
Slug: delimitacion-de-paramos-en-colombia-nuevos-paramos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/04/delimitacion-de-paramos.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Alejandro Cabrera 

#### [Por[ Natalia Pérez Amaya \* - Ambiente y Sociedad](https://archivo.contagioradio.com/margarita-florez/)] 

[El pasado 8 de febrero la Corte Constitucional, mediante su sentencia][[C-035/16,]](http://www.ambienteysociedad.org.co/wp-content/uploads/2016/02/C-035-16-Exp-D-10864-Paramos.pdf)[ ordenó la protección real y efectiva de los páramos en Colombia, prohibiendo de manera expresa la realización de actividades mineras en los mismos y ordenando su delimitación.]

[La mencionada sentencia resaltó la vulnerabilidad de los ecosistemas de páramo, así como la importante relación de estos tanto con los seres humanos, como con otros ecosistemas como el bosque alto andino, con el cual se encuentra interconectado. Por lo anterior, la delimitación de páramos para su protección debería tener en cuenta estudios técnicos, económicos, sociales y ambientales, viéndose concretada mediante actos administrativos proferidos por el Ministerio de Ambiente y Desarrollo Sostenible, teniendo en especial consideración que “]*[Una delimitación inadecuada, o que no consulte sólidos criterios científicos puede llegar a afectar los ecosistemas de páramo (…) con ello se podría causar un riesgo para la disponibilidad y la continuidad de servicios ambientales de los cuales depende el derecho fundamental al agua. Más aun, una delimitación inadecuada, podría llegar a permitir la utilización del suelo de los páramos para realizar actividades de minería y de hidrocarburos en estos ecosistemas.”]*[.]

[Como ejemplo de lo anterior, la Corte Constitucional mencionó la delimitación del páramo de Santurbán, siendo este el único páramo efectivamente delimitado al momento de proferir la sentencia, pero cuya delimitación no tuvo en cuenta los criterios científicos dados por el Instituto Alexander Von Humboldt, específicamente en lo referente al componente de integridad ecológica, el cual pretende mantener o mejorar las condiciones de funcionalidad y resiliencia del ecosistema, pues al reconocer la unidad ecológica entre el páramo y el bosque alto andino, tendría que asegurar su conectividad espacial y ecológica, pues la pérdida de la misma o la fragmentación pueden conducir al aislamiento de poblaciones, la extinción de especies y la pérdida de servicios ambientales.]

**¿Qué ha pasado desde la sentencia?**

[El pasado 22 de marzo del presente año, el Ministerio de Ambiente y Desarrollo Sostenible anunció la delimitación de ocho nuevos páramos, resaltando en su comunicado que “]*[En las condiciones actuales de riesgo de racionamiento y ahorro de energía, es muy importante destacar el papel de los páramos en proporcionar servicios ecosistémicos fundamentales para el bienestar de la población y para el desarrollo económico del país. Los páramos proveen el agua al 66% de la población colombiana y ahorran millones en tratamiento de agua y en infraestructura para transportarla]*[”.]

**Los páramos delimitados son:**

[![paramos delimitados](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/04/paramos-delimitados.png){.aligncenter .size-full .wp-image-22527 width="575" height="731"}](https://archivo.contagioradio.com/delimitacion-de-paramos-en-colombia-nuevos-paramos/paramos-delimitados/)Tal y como se evidencia, los páramos de Farallones de Cali, Paramillo y Tatamá, han sido delimitados en su totalidad al interior de diferentes Parques Nacionales Naturales, por lo que su manejo y competencia se encuentra en cabeza de estas áreas protegidas, manteniendo los usos y prohibiciones dispuestas en el ordenamiento jurídico para los Parques Nacionales Naturales.

[Por lo anterior, y pese a que es necesario exhortar al Gobierno Nacional a continuar con la delimitación de los ecosistemas de páramo en Colombia, se debe resaltar la importancia de realizar esta tarea en las zonas que se encuentran desprotegidas al no contar con un acto administrativo que concrete la protección, tal y como lo señaló la Corte Constitucional, razón por la cual, se debería priorizar la delimitación de los páramos que están siendo explotados por no ser áreas protegidas.]

[Los parques nacionales naturales, cuentan con una protección especial dentro del ordenamiento jurídico, siendo áreas de especial importancia ecológica debido a su diversidad, por lo que es necesaria su conservación. Teniendo en cuenta que los Parques Nacionales Naturales ya tienen prohibiciones frente a las actividades que se pueden realizar dentro de los mismos, es necesario que el gobierno nacional busqué en primera medida la delimitación y protección de las zonas de páramo que se encuentran desprotegidas por el ordenamiento, ya que son precisamente estas las que tienen mayor riesgo en su conservación, al no contar con una herramienta jurídica vinculante que asegure que no se desarrollen actividades de explotación dentro de las mismas.]

[En conclusión, si bien el gobierno, en cabeza del Ministerio de Ambiente y Desarrollo Sostenible, ha dado inicio a la delimitación de páramos en Colombia, de acuerdo a lo ordenado por la Corte Constitucional, es necesario que esta logre la efectiva protección de los ecosistemas, pues como sociedad no podemos permitir que se vean afectados nuestros derechos por falta de diligencia y planeación.   ]
