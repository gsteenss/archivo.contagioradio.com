Title: Corte Constitucional ordena adjudicación de tierras a comunidades de Puerto Gaitán
Date: 2016-11-05 09:24
Category: Nacional, Política
Tags: despojo de tierras, paramilitares colombia, Victor Carranza usurpa tierras en Puerto Gaitán
Slug: corte-constitucional-ordena-adjudicacion-tierras-comunidades-puerto-gaitan
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/auc.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Eldiariofenix.com] 

###### [4 Nov de 2016 ] 

La Corte Constitucional ordenó que se le reconozca el derecho a la propiedad a las familias habitantes de la vereda "El Porvenir" en jurisdicción de Puerto Gaitán, departamento del Meta. Sin emabrgo **persisten las preocupaciones por el accionar de grupos paramilitares, militares y civiles que han favorecido la ocupación ilegal por parte de presuntos herederos de la familia Carranza.**

En meses pasados, el caso de 18 familias que habitaban los predios El Porvenir, en Puerto Gaitán Meta, que **fueron desplazadas por grupos armados identificados como Autodefensas del Meta y Vichada, presuntamente enviadas por la familia Carranza,** fue conocido por la Corte Constitucional, quien reconoció en los últimos días, que hubo irregularidades del INCORA en la adjudicación de estas tierras.

A pesar que en el 2014 se hizo efectiva una solicitud de revocatoria de la titulación de esos predios a los Carranza, y la Agencia Nacional de Tierras ordena la recuperación material de los predios, **aún en 2016 la entidad no ha hecho presencia ni ha efectuado la restitución de estas tierras a las comunidades indígenas Cubeo Sikuani y campesinos,** que las han habitado hace más de 40 años. Le puede interesar: [18 familias fueron desplazadas en](https://archivo.contagioradio.com/18-familias-fueron-desplazadas-en-puerto-gaitan-por-autodefensas-del-meta/)Puerto Gaitán[por Autodefensas del Meta.](https://archivo.contagioradio.com/18-familias-fueron-desplazadas-en-puerto-gaitan-por-autodefensas-del-meta/)

Esta situación de limbo, generó que personas foráneas empezaran a construir en la hacienda ‘La Cristalina’, sin embargo, atendiendo la orden de la Corte, estos ocupantes son muy cercanos a los Carranza, según Francisco Henao, abogado de la Corporación Jurídica Yira Castro, sin embargo **“deben salir de los terrenos, porque han sido usurpados a través de hechos violentos, a quienes tradicionalmente han habitado estos territorios”.**

Henao resalta que son 28 mil las hectáreas que están en disputa, a pesar de haber **frenado la Zidres de Orocué, y el logro de la recuperación legítima que ha hecho la Guardia Indígena del Vichada de unas 15km,** “aún queda mucho por hacer en términos legales de restitución y protección de los derechos humanos de las comunidades”. Le puede interesar: [Agreden a reclamantes de tierras y defensores de DDHH en](https://archivo.contagioradio.com/agreden-a-reclamantes-de-tierras-y-defensores-de-ddhh-en-puerto-gaitan/)Puerto Gaitán.

Por ultimo, enunció que el próximo miércoles la Agencia Nacional de Tierras, citó a varias instituciones gubernamentales, para que **“hagan presencia, socialicen los diagnósticos realizados y garanticen derechos a unos y otros, quienes salen de los predios y las comunidades que reingresan”.**

<iframe id="audio_13619586" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_13619586_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en su correo [[bit.ly/1nvAO4u]
