Title: Boletín informativo Mayo 20
Date: 2015-05-20 17:54
Author: CtgAdm
Category: datos
Tags: Actualidad, Actualidad Contagio Radio, Boletín Informativo, Noticias
Slug: boletin-informativo-mayo-20
Status: published

[*Noticias del Día:*]

<iframe src="http://www.ivoox.com/player_ek_4524374_2_1.html?data=lZqflpibeI6ZmKiakpyJd6KmmZKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRhtDgxtmSpZiJhaXijK7byNTWscLoytvcjbLFvdCfk5Wah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

-Aunque algunos medios de información han señalado el cambio del gabinete de Defensa como un viro de la política del presidente Juan Manuel Santos para preparar sus ministerios para un eventual post conflicto, analistas políticos como **Ramiro Bejarano** no son tan optimistas.

-Exigencia de la **superintendencia de Servicios Públicos** a los a**cueductos comunitarios** administrados por la **Asociación de Usuarios del municipio de Sucre, Cauca**, para que asuman un esquema de **empresarización** podría afectar el derecho de las comunidades de acceder al **recurso hidrico a un costo razonable**. Habla Gener Hoyos de la Asociacion Campesina "Bien andantes".

-Continuan en **Perú** las **protestas en contra de proyecto minero Tía María**. Para **Rocío Silva, Coordinadora Nacional de DDHH**, la tregua decretada por el gobierno y la multinacional pretende tranquilizar la situación pero los afectados por el proyecto han decidido **continuar con el paro** al que se han sumado otras regiones del sur del país.

-**Paralizadas 516 áreas mineras** por no consultar a las comunidades. **La decisión del Consejo de Estado** paraliza las extracciones al menos **durante un año** por no haber consultado previamente a las comunidades afectadas en las las zonas que ocupan el 20% del territorio del país, con un total de 20 millones de hectáreas. Habla **Jimena González de la Organización Tierra Digna.**

-En la actualidad **54% del territorio caucano está destinado a la minería** con alrededor de 267 contratos de concesión otorgados a empresas nacionales, multinacionales y particulares de los cuales **unicamente 178 cumplen con el licenciamiento ambiental**, provocando descomposición social y poniendo en riesgo la tranquilidad de comunidades. Habla **Olinto Masabuel**, **encargado del tema minero en el Consejo Regional Indígena del Cauca.**

-Nuevo caso de **reclutamiento forzado del Ejército en Monsserrate Huila**, se trata del **joven campesino Eider Cusuaya Rivera** quien fue reclutado forzadamente por el ejército Colombiano el viernes 20 de Febrero de 2015. mientra acudia a la Registraduría Nacional a recoger su cedula de ciudadanía como cuenta su madre **Maria Bertilda Rivera.**

-**La comunidad de Galindez Cauca**, exige consulta previa con las familias ante los deslizamientos provocados por la **extracción de material del río** en la **construcción del Corredor Vial 2**, situación que afecta a las parcelas del sector. Habla **Floer Meneses**, **presidente del Consejo comunitario de Galindez.**

-**Angélica Lozano representante a la Cámara por Bogotá y Claudia López senadora**, continúan a la **espera de la decisión del presidente del consejo de Estado**, Luis Rafael Vergara respecto a su situación política **luego de la audiencia realizada el martes en que se les acusa de haber violado la Constitución por ser pareja y haberse lanzado como candidatas por el partido político Alianza Verde.**
