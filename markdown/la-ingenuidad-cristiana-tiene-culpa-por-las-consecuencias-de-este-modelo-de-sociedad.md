Title: La ingenuidad cristiana ¿Tiene culpa por las consecuencias de este modelo de sociedad?
Date: 2020-05-08 15:32
Author: A quien corresponde
Category: A quien corresponda, Opinion
Tags: cristianismo, Cristianos, ingenuidad, politica, politica latinoamericana, religion
Slug: la-ingenuidad-cristiana-tiene-culpa-por-las-consecuencias-de-este-modelo-de-sociedad
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/carretera.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:heading {"level":5} -->

##### **Con la medida que midan a los otros, los medirán a ustedes** (Mateo 7,2)

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"right"} -->

*Hay cristianos que no reconocemos los pecados personales, sociales y ambientales, mientras exigimos a los demás que no pequen o los criticamos por pecadores. *

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Estimado

<!-- /wp:paragraph -->

<!-- wp:heading {"level":5} -->

##### **Hermano en la fe**, Cristianos, cristianas, personas interesadas

<!-- /wp:heading -->

<!-- wp:paragraph -->

Cordial saludo,

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Te había escrito sobre cómo se configura nuestra mentalidad, manera de pensar, de ver y afrontar la vida, sobre la configuración de valores, conocimientos y creencias religiosas, políticas, sociales, culturales que recibimos del “ambiente” familiar, social y religioso, sin pensarlo y sin darnos cuenta; también sobre las formas como la sociedad maneja y dirige nuestros sentimientos, creencias y emociones para que veamos la realidad de acuerdo a los intereses, creencias y conveniencias de quienes la dirigen y se benefician política y económicamente de ella. Retomo estas ideas porque ayudan a  entender la **ingenuidad cristiana.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Con el intercambio “epistolar” y la distancia de la cuarentena, la intensidad de las discusiones ha bajado y la calma ayuda a ver mejor el fondo de la realidad socio-económica y ambiental y que varios temas de discusión, las diferencias son pequeñas, el problema ha sido abordarlos desde una ideología, no reconocida ni asumida, que descalifica por malos y pecadores a quienes tienen una mirada diferente de lo político y religioso, sin analizar si tiene o no la razón.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":5,"customTextColor":"#0d6090"} -->

##### [Le puede interesar: El modelo socio-económico causante de la crisis, se dice creyente. ¿Será verdad?](https://archivo.contagioradio.com/el-modelo-socio-economico-causante-de-la-crisis-se-dice-creyente-sera-verdad/) 

<!-- /wp:heading -->

<!-- wp:heading {"level":5} -->

##### Esto  hace gente religiosa y buena pero con una **ideología ingenua.**

<!-- /wp:heading -->

<!-- wp:paragraph -->

La cuarentena está mostrando las dimensiones y la profundidad de la injusticia social y la degradación ambiental, con sus graves consecuencias: muerte, hambre, desnutrición, precariedad del sistema de salud, violencia política y social, corrupción, contaminación ambiental, desaparición de miles de espacies, el cambio climático...

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

La pandemia, ha hecho inocultable la perversión e insensatez del modelo socio-económico que rige el mundo y lo contradictorio de haber sido construido con la participación de gente buena y bien intencionada que se ha convertido en su víctima, apoyado por millones de cristianos, buenos pero **ingenuos.**

<!-- /wp:paragraph -->

<!-- wp:heading {"level":5} -->

##### Lo más contradictorio es que millones de víctimas (empobrecidas y marginadas) por el sistema, lo defienden mientras padecen sus consecuencias.

<!-- /wp:heading -->

<!-- wp:paragraph -->

Esta contradicción se entiende por el engaño y la manipulación de la sociedad por los medios del poder que le “muestran la realidad”, señalan problemas secundarios como centrales y ubican las cusas de la injusticia social y de los daños ambientales en otros lados, incluso que es voluntad de Dios.  “Las mayorías” les creen, gracias a una **ingenuidad** construida, que va acompañada de miedo, odio y desconfianza.  Por esto la pertinencia de preguntarnos: *¿Hasta dónde esta ingenuidad cristiana es culpable por apoyar la construcción de un modelo social que destruye la vida humana y del planeta para la acumulación de unos y con medios perversos?*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Hablo de **ingenuidad cristiana,** porque hablando con cristianos “comunes y corrientes” y con militantes de la “derecha político-religiosa”, he encontrado personas bien intencionadas, preocupadas por el hambre y el sufrimiento de la gente, que buscan sinceramente hacer la voluntad de Dios, pero que, orientada por líderes políticos y religiosos, piensan, hablan, oran, deciden y opinan a favor de los intereses político-económicos e ideológicos de este poder, y en contra el bien común, del mensaje cristiano y de ellos mismas. 

<!-- /wp:paragraph -->

<!-- wp:heading {"level":5} -->

##### En otras palabras, hay un cristianismo que ayudó a construir y respalda este modelo de “desarrollo” y “progreso” que genera la muerte de seres humanos y de la naturaleza.

<!-- /wp:heading -->

<!-- wp:paragraph -->

Millones de creyentes y ciudadanos han sido víctimas de su economía (desempleo, subempleo, quiebras económicas…), de su racismo, discriminación social, violencia física (asesinatos políticos, conflictos armados, despojo de tierras); de su violencia simbólica (imaginarios y exigencias sociales que excluyen, marginan y culpabilizan) y de su corrupción (que ha permeado todos los estamentos sociales y afecta a las mayorías) que acabó con la economía campesina, la pequeña y mediana industria, la salud, la educación y que privatizó las empresas más rentables del Estado.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Este cristianismo, conformado mayoritariamente por gente buena y bien intencionada, apoya política, económica, simbólica y socialmente a los responsables-victimarios (socio-político-económicos) de la grave situación de la mayoría de miembros de sus iglesias. Estos responsables-victimarios, conservan su poder, sus negocios lucrativos y abusivos, su buena imagen social, sus víctimas los respaldan porque asumieron la propaganda que son excelentes personas, preocupadas por toda la sociedad y que todo lo hacen por el bien de todos. **Y lo hacen ingenua y gratuitamente,** con el apoyo, con frecuencia inconsciente de este cristianismo.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":5} -->

##### *Unos hechos que muestran este apoyo*:

<!-- /wp:heading -->

<!-- wp:list -->

-   **Primero:** en elecciones pasadas, una víctima de desplazamiento forzado y despojo de tierras, que habita en un barrio marginal de Bogotá, optó por el candidato de la derecha porque, según su afirmación, el candidato opositor: “es comunista y nos va a expropiar”. Esta víctima carecía de casa propia, pertenencias valiosas, trabajo estable, nada que le pudieran expropiar. Ya lo habían expropiado de su tierra; por el miedo a la expropiación votó por candidato de la derecha, apoyado económicamente por los responsables del despojo de su tierra.

<!-- /wp:list -->

<!-- wp:heading {"level":5} -->

##### Ya sabemos que la expropiación fue un cuento de la derecha para que la gente votara por miedo.

<!-- /wp:heading -->

<!-- wp:list -->

-   **Segundo:** miles de colombianos hemos sido víctimas, directa o indirectamente, del sistema de salud. Muchos de los afectados respaldan partidos políticos y personas responsables y beneficiarios de la quiebra y el robo a la salud; entre quienes los respaldan hay creyentes, que dicen que estos candidatos son mejores cristianos y que los “otros”, que son enviados del demonio.
-   **Tercero:** una mujer joven creyente, afirmó en  redes sociales que su pastor les dijo que los verdaderos cristianos debían votar por Iván Duque porque su opositor era ateo y enemigo de la religión. La joven analizó las relaciones y la vida de los aliados Iván Duque: Andrés Pastrana, Germán Vargas Lleras y Cesar Gaviria y del jefe Álvaro Uribe y las contrastó con la información que su pastor daba de ellos y del opositor. Concluyó que por razones éticas y religiosas no podía votar por quien decía su pastor.  Mucha “gente buena” de las iglesias atacó violentamente a la joven.

<!-- /wp:list -->

<!-- wp:paragraph -->

Con estas letras no pretendo afirmar una verdad, solo invitarte a flexionar y a buscar las raíces de los problemas sin quedarnos en la ramas, a superar “el odio visceral” a quienes critican nuestra visión religiosa y política, a dejar de “defender ciegamente” grupos político o religiosos solo porque coinciden con nuestra manera de ver y creer, **a superar la ingenuidad cómplice de la crisis humana y ambiental que vivimos.**

<!-- /wp:paragraph -->

<!-- wp:heading {"level":5} -->

##### Para avanzar, estas pistas pueden ayudarnos: 

<!-- /wp:heading -->

<!-- wp:list {"ordered":true,"type":"1"} -->

1.  Reconocer que pensamos, decidimos y opinamos con la información que nos llega y que procesamos con las emociones, ideas, “valores” y conocimientos que llevamos dentro, con frecuencia inconscientemente, que poco sabemos que la mayoría de los medios de información son propiedad del poder económico y político, y que “es normal” que informen lo que les conviene y de forma que genere sentimientos, pensamientos e ideas que mantengan el sistema socio-económico creado por ellos.
2.  **Reconocer** que, ordinariamente, vemos como bueno lo nuestro y malo lo demás, justificamos los nuestros (amigos, familias, partidos, iglesias, equipos…) y condenamos virulentamente las fallas de los demás (familias, partidos, iglesias, equipos, enemigos…); que la manera de “valorar lo nuestro” ha sido trasmitida (sin darnos cuenta) por la cultura, la educación, la religión y los medios de información del mismo poder que construyó esta sociedad.
3.  **Recordar** que Jesús de Nazaret fue víctima del poder religioso, político, económico y social de su tiempo; que los profetas bíblicos cuestionaron a sus dirigentes por la manera injusta de dirigir el país y fueron perseguidos y asesinados por ellos y que frecuentemente la religión ha estado al lado del poder abusador y explotador, unas veces consciente mente y por conveniencia  y otras sin darse cuenta, manipulada por ese mismo poder. 
4.  **Recordar** que Jesús de Nazaret no fue ingenuo frente a los poderes de su tiempo, por eso dijo a sus seguidores: *“Miren, yo los envío como ovejas en medio de lobos: sean astutos como serpientes y sencillos como palomas*” (Mateo 10,16); *“Los hijos de este mundo son más sagaces con los de su clase que los hijos de la luz”* (Lucas 16,8); también las palabras del profeta: “*¡Que fluya, sí, el derecho como agua y la justicia como arroyo perenne!”* (Amos 5,24)

<!-- /wp:list -->

<!-- wp:paragraph -->

Fraternalmente, su hermano en la fe,

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

P. Alberto Franco, CSsR, JyP <francoalberto9@gmail.com>

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Ver mas:[Nuestros columnistas](https://archivo.contagioradio.com/opinion/columnistas/)

<!-- /wp:paragraph -->
