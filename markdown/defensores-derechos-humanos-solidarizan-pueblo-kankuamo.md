Title: Pueblo Kankuamo recibe solidaridad de defensores de DDHH alrededor del mundo
Date: 2018-10-31 16:37
Author: AdminContagio
Category: DDHH, Nacional
Tags: DDHH, pueblos indígenas, Sierra Nevada
Slug: defensores-derechos-humanos-solidarizan-pueblo-kankuamo
Status: published

###### Foto: Onic 

###### 31 Oct 2018 

A través de una carta, 14 reconocidos activistas de diferentes países entre los que se encuentran **Colombia, México, Reino Unido, Camerún, Sudán y Uganda** expresaron su apoyo al **pueblo Kankuamo, de la Sierra Nevada de Santa Marta** y pidieron al Gobierno nacional por su protección tras **[los constantes ataques que ha sufrido la comunidad.](https://archivo.contagioradio.com/quema-casa-kankuamo/)**

Acerca de la más reciente agresión hacia los kankuanos, en la que un centro ceremonial fue incinerado en el corregimiento de **Atánquez**, los activistas expresaron que dichas afrentas hacen parte de una larga cadena de violaciones a los derechos humanos y son “una ofensa directa a la espiritualidad del pueblo y a la Madre Tierra. Con la quema de las casas sagradas se está poniendo en riesgo el equilibrio del mundo y la protección que hace el pueblo Kankuamo de la Sierra Nevada de Santa Marta”.

Los firmantes también destacan el rol del pueblo Kankuamo en la **resistencia y fortalecimiento de la identidad del su territorio**, “hemos aprendido de la experiencia de este pueblo cómo la alianza del conocimiento ancestral y del autogobierno, hacen de la Sierra Nevada un lugar de conservación y preservación de la Madre Tierra. Esta labor, sin duda, la han logrado por medio del **fortalecimiento de la espiritualidad del pueblo y el cuidado de los sitios sagrados**”.

Es por esta misma razón que exhortan al **Gobierno nacional** y a las instituciones correspondientes para que **acompañen al gobierno del pueblo Kankuamo** y logren establecer los correctivos necesarios para evitar que hechos como estos se repitan y se pueda sancionar a los responsables materiales e intelectuales de los ataques.

En el comunicado aparecen las firmas de Grover Cleveland Gauntt III, Mario Murillo, Noemí Santana, Ruth Anna Buffalo, Francisco Lugoviña  y Rami Avraham Efal desde los Estados Unidos, Anette Schorr y Adriana Vásquez de Colombia, Laura Salas de México, Karimu Unusa, de Camerún, Fredrick Ssenyonga, de Uganda, Liaquiat Ali, de Reino Unido; Evariste Ndikumana, de Burundi y  Benson Khemis Soro Lako, de Sudán del Sur.

###### Reciba toda la información de Contagio Radio en [[su correo]
