Title: Boletín informativo junio 9
Date: 2015-06-09 17:30
Category: datos
Tags: ampaña Global para Desmantelar el Poder de las Trasnacionales., Comisión Ética y Politica de México., Cumbre Unión Europea - CELAC, elecciones generales México, Empresa Vetra, Explotación petrolera, Nacimientos de agua Putumayo secos, Noticias del día, Segundo Foro por la Paz de Colombia
Slug: boletin-informativo-junio-9
Status: published

[*Noticias del Día: *]

<iframe src="http://www.ivoox.com/player_ek_4617861_2_1.html?data=lZuemZ2adY6ZmKiakpqJd6KlkpKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRhtDgxtmSpZiJhaXijK7byNTWscLoytvcjcnJb6vpz87cjZ6RaZi3jqjc0NnFq8rjjLfOxs7Tb46ZmKialg%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

-**Más de 20 nacimientos de agua se han secado en Putumayo** por la explotación petrolera, las comunidades campesinas, indígenas y afrodescendientes, denuncian que **la crisis ambiental que existe actualmente en el departamento se debe a las actividades de la empresa VETRA**, hasta el momento no se han tomado medidas frente a las faltas ambientales de la compañía. Habla **Paola Artunduaga** de la **Mesa Regional de Putumayo.**

-El pasado fin de semana se llevaron a cabo **elecciones generales en México**, para definir la conformación del **parlamento y las gobernaciones.** Sin embargo, **la jornada, que debió registrarse como una fiesta de la democracia, no pudo evitar ser un reflejo de la tensa calma que vive el país centroamericano**. Habla **Gilberto Lopez**, de la **Comisión Ética y Politica de México**.

-**Balance positivo al cierre del Segundo Foro por la paz de Colombia**, realizado en Montevideo Uruguay entre el 5 y el 7 de Junio, **un espacio centrado en la discusión sobre como solucionar el conflicto político-social-armado en Colombi**a, **fundamental para alcanzar la paz, la tranquilidad y la lucha antimilitarista en la región**. Habla **Laura Pinzón**, participante de las mesas de debate.

-En el marco de la **Cumbre Unión Europea - CELAC** que se realiza en Bruselas del 8 al 10 de junio, **más de 50 organizaciones y plataformas sociales de Europa y América Latina** ha convocado a una serie de actividades de discusión y de movilización **para denunciar la arquitectura económica trasnacional que se desarrolla en este tipo de encuentros,** y que viola la autodeterminación de los territorios. Habla **Diana Aguilar**, de la **Campaña Global para Desmantelar el Poder de las Trasnacionales**.
