Title: Students join the National Strike on April 25th
Date: 2019-04-24 15:51
Author: CtgAdm
Category: English
Tags: Education, National Development Plan, Strike, Students
Slug: students-join-the-national-strike-on-april-25th
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/Marcha-de-estudiantes-2.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

[The student movement announced that will join the National Strike on Thursday, April 25^th^ as **a way to strengthen the union with the the social movements and demand compliance with the agreements reached with the Government in December last year**. In this sense, the National Union of Higher Education Students (UNEES) called for all Higher Education Public Institutions (HEIs) to join the strike, and cease their activities.]{lang="EN"}

[In his statement UNEES indicated two key points that will be considered on April 25^th^: the **National Development Plan (PND)**, which has been widely criticized by social sectors and the advance of a higher **education model that works against the interests of the majority of Colombians**. As explained by the national spokesman of the Student Platform, James Pareja, UNEES participated in the meeting of social organizations that was held in February, in which the date for the strike was agreed.]{lang="EN"}

[In that meeting, has been decided that the social organizations would be deployed **against the PND, in favor of the implementation of the peace agreement and the accomplishment of the agreements reached between the National Government and the Social Movement**. In addition on these points, students claim that the development plan does not contemplate the comprehensive **reform of ICETEX** (the Colombian Institute of Educational Credit and Technical Studies Abroad]{lang="EN"}), [nor the discussion on **articles 86 and 87 of regarding the Law 30** on education.]{lang="EN"}

**[How to achieve these three points?]{lang="EN"}**

[To implement a solid roadmap which could allow to a programmatic construction on the agenda topics, **Popular Revolutionary Committees** (CORPUS) will be formed and integrated. With this committees Pareja hopes in a fruitful campaign.]{lang="EN"}

[The announcement of the students give strength to **farmers, workers, indigenous peoples, teachers of primary and post-primary school, afro communities and transporters** who have affirmed their decision to be part of this strike.]{lang="EN"}
