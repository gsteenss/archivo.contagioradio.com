Title: En Colombia no hay avances para detener la violencia contra las mujeres
Date: 2018-11-21 16:25
Author: AdminContagio
Category: DDHH, Mujer
Tags: colombia, Día de la eliminación de la violencia contra las mujeres, mujeres
Slug: en-colombia-no-hay-avances-para-detener-la-violencia-contra-las-mujeres
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/01/Mujeres-13.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo Particular] 

###### [21 Nov 2018] 

Este 25 de noviembre se conmemora internacionalmente el día de la eliminación de la violencia contra las mujeres, y en Colombia puntualmente se cumplen 10 años de la ley 1257 con la que se busca prevenir y sancionar este tipo de hechos. Sin embargo, **la realidad del país es que desde el 2013 hasta la fecha, se han presentado 6.013 feminicidios**.

Según Laura Torres, integrante de la organización Católicas por el derecho a decidir, "hay una gran ausencia en el discurso público para eliminar las distintas violencias contra las mujeres", sumado al hecho de que persisten las barreras para que las mismas accedan a la justicia.

Entre los obstáculos más comunes, que para Torres afrontan las mujeres, esta el desconocimiento por parte de los operadores de justicia de la existencia de la ley, que ha provocado que muchas mujeres sean **revictimizadas, en situaciones como indicarles que deben conciliar con el agresor o desistir de la denuncia.**

Otra de las barreras tiene que ver con las medidas de protección que se otorgan debido a que "**muchas de las mujeres que han sido víctimas de feminicidios ya han tenido medidas de prevención y protección**", hecho que para Torres cuestiona el compromiso por parte de la Policía a la hora de cuidarlas. (Le puede interesar: ["Acoso sexual en las universidades, una violencia silenciosa contra las mujeres"](https://archivo.contagioradio.com/acoso-sexual-en-la-universidades/))

Torres, también afirmó que en Colombia cada 30 horas una mujer es asesinada, mientras 1 de cada 3 ha sido víctima de violencia sexual, sin mencionar que no se tienen registros de todos los casos de violencia sicológica, económica y simbólica que continúan afectando de manera sistemática a las mujeres.

### **25 de noviembre movilización en contra de las violencias de género** 

Desde las 9:00am en Bogotá, las mujeres se darán cita en el Parque de los Hippies para realizar diversas actividades en conmemoración del día internacional de la eliminación de la violencia contra las mujeres, **posteriormente realizarán una movilización hasta el parque Nacional en donde tendrá una jornada cultural y de defensa sobre sus derechos**.

"Sin duda necesitamos que no solo en normativa esto este escrito, sino que la sociedad realmente se comprometa a eliminar todo tipo de violencia en contra de las mujeres y las niñas" afirmó Torres.

###### Reciba toda la información de Contagio Radio en [[su correo]
