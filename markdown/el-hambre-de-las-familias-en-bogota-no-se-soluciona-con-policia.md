Title: El hambre de las familias en Bogotá no se soluciona con Policía
Date: 2020-04-20 15:07
Author: AdminContagio
Category: Actualidad, Comunidad
Tags: Bogotá, ciudad bolivar, protestas
Slug: el-hambre-de-las-familias-en-bogota-no-se-soluciona-con-policia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/04/Las-protestas-del-hambre-en-Bogotá-e1587259695792.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:heading {"align":"right","level":6,"textColor":"cyan-bluish-gray"} -->

###### Foto: Contagio Radio {#foto-contagio-radio .has-cyan-bluish-gray-color .has-text-color .has-text-align-right}

<!-- /wp:heading -->

<!-- wp:paragraph -->

Durante las noches del 14 y 15 de abril se vivieron en Bogotá las primeras jornadas de protestas que fueron reprimidas con denuncias de exceso de fuerza por parte de la Policía, las personas en localidades del sur piden apoyo para poder pasar la cuarentena sin hambre. Ante la situación, y la primera respuesta oficial a cargo de la Fuerza Pública, líderes y organizaciones sociales han señalado que se debe tener un enfoque garantista de derechos, y de asistencia social.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### **No es la primera protesta exigiendo condiciones de vida digna en medio del aislamiento**

<!-- /wp:heading -->

<!-- wp:paragraph -->

Heidy Sánchez, concejala de Bogotá por Colombia Humana/UP, recuerda que desde hace tiempo se ha visto a las personas obligadas a salir a las calles exigiendo ayudas, como ocurrió el 24 de marzo con vendedores informales y ciudadanos venezolanos en la Plaza de Bolívar, y otras plazas del país. También se vió, hace semanas, con la aparición de trapos rojos en las casas que anunciaban la necesidad de comida en los sectores más vulnerables de las ciudades. 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Ante la ausencia de instituciones que respondan a este clamor, Sánchez sostiene que las personas se vieron obligadas a manifestarse públicamente y eso fue lo que pasó en en barrios como Diana Turbay o Arborizadora Baja, entre otros. La concejala afirma que las ayudas que piden no se trata únicamente de mercados, “porque el mercado dura unos días, pero tienen otras obligaciones como el arriendo”.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### **El problema de Bogotá Solidaria, y la propuesta de la Colombia Humana**

<!-- /wp:heading -->

<!-- wp:paragraph -->

La alcaldesa Claudia López creó para la ciudad el programa Bogotá solidaria en casa, que mediante el censo realizado recientemente por el DANE identificaba a hogares en situación de pobreza monetaria y vulnerables, que requieren apoyos, a través de transacciones bancarias. Parte del gasto necesario para cubrir la meta de 500 mil hogares beneficiados, provendría de donaciones.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Sin embargo, Sánchez asegura que Bogotá solidaria tiene un problema es que se basa en un censo de hogares con Sisben 1 y 2, pero que no está actualizado y no toma en cuenta que la situación de muchas personas cambió en razón del aislamiento obligatorio. “Hay personas mayores que están en el programa, pero por ejemplo, las amas de casa o quienes trabajan en el servicio doméstico por días no”, agregó. 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Para enmendar este error, la concejala solicitó a la Alcaldía que se vaya más allá de la focalización mediante el censo, y se haga por territorialización: es decir, que en las zonas donde se ha identificado pobreza multidimensional se entreguen ayudas, independientemente de los puntajes del Sisben. A ello se sumó la exigencia por una[renta básica](https://twitter.com/heidy_up/status/1249115644014080000). 

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### **La respuesta no puede ser mediante la acción de la Fuerza Pública**

<!-- /wp:heading -->

<!-- wp:paragraph -->

Tras las protestas ocurridas en las noches del miércoles y jueves se registró una persona herida, por golpe en la cabeza tras la acción de la Policía. Pero luego de nuevas protestas en otras zonas de la ciudad, el edil de Ciudad Bolívar Christian Robayo denunció que la Fuerza Pública electrocutó a una mujer en estado de embarazo y disparó contra un jóven en dos ocasiones, causándole heridas que lo tienen en estado crítico en el Hospital de Meissen. 

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/ElParcheCritico/status/1251312492229337088","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/ElParcheCritico/status/1251312492229337088

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

Ante tales situaciones, ciudadanos han pedido que la respuesta a las peticiones de las personas sea con asistencia en términos de garantías de vida digna para enfrentar la pandemia del Covid 19, y no mediante el uso excesivo de la fuerza, y saltándose el protocolo para manifestaciones que la Alcaldía impulsó al inicio del manda. 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

También lo invitamos a consultar: [La crisis económica no se resuelve con limosnas: Alirio Uribe Muñóz](https://archivo.contagioradio.com/la-crisis-economica-no-se-resuelve-con-limosnas-alirio-uribe-munoz/).

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78980} /-->
