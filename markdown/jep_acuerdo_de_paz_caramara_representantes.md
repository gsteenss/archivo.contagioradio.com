Title: Se ha dado "un golpe de muerte a la JEP": Diego Martínez
Date: 2017-11-28 17:09
Category: Nacional, Paz
Tags: acuerdo de paz, Cámara de Representantes, JEP
Slug: jep_acuerdo_de_paz_caramara_representantes
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/11/DOnqVVdWAAAlr5G-e1510770947262.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Ojo a la Paz] 

###### [28 Nov 2017] 

[Terminó el debate de la Justicia Especial para la Paz en la Cámara de Representantes, y se espera que este miércoles se inicie la conciliación entre la Cámara y el Senado del texto final de la Ley estatutaria. Sin embargo, según denuncia Voces de Paz  y organizaciones de víctimas, la JEP aprobada va en contravía de lo acordado en el acuerdo con la FARC, y no se trataría de una justicia para todas las víctimas de la guerra, sino únicamente para juzgar a los integrantes de la Fuerza Alternativa Revolucionaria del Común, FARC.]

<iframe src="https://co.ivoox.com/es/player_ek_22336465_2_1.html?data=k5eglZuYepahhpywj5WaaZS1lpuah5yncZOhhpywj5WRaZi3jpWah5yncaXdxszcjbLFttWZpJiSo6nSqdufwtjS1dTWb8vp04qwlYqliMXdxNSYxsrQb9Hm0MjS1dSPqMaf0cbnj4qbh4630NPhw8zNs4zGwsnW0ZCRaZi3jpk.&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

Para el asesor jurídico de los diálogos de paz en La Habana, Diego Martínez, con lo aprobado en el Congreso **"se ha dado casi un golpe de muerte a la JEP ya que se modifican aspectos esenciales",** que al final termina vulnerando los derechos de las víctimas en términos de garantías de verdad, justicia, reparación y garantías de no repetición.

### **Inhabilidades para magistrados de la JEP** 

En el senado se quería definir que algunas de las personas que ganaron por concurso ser magistrados de la JEP quedaran por fuera de ella por el hecho de ser defensores de derechos humanos, una situación que **además de estigmatizar ese papel, es inconstitucional** como se ha asegurado en repetidas ocasiones.

Según argumenta el abogado Martínez, esas inhabilidades son violatorias al debido proceso y a los principios internacionales, ya que en todo el mundo existen varios instrumentos con los que se busca avanzar en la defensa de los derechos humanos humanos, y por ende, son los Estados los primeros que deben trabajar para proteger y promover la labor de los defensores de derechos humanos.

<iframe src="https://co.ivoox.com/es/player_ek_22336578_2_1.html?data=k5eglZuZe5mhhpywj5WdaZS1lpyah5yncZOhhpywj5WRaZi3jpWah5yncaLiyMrZw5C2s8PgxsncjdjTptPZjNHOjdvTuMLXyoqwlYqmd8-fxcqYzsaPjqbEjMrbjaiJh5SZopbaw9fFcYarpJKw0dPYpcjd0JC_w8nNs4yhhpywj5k.&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

Por su parte, de acuerdo con la representante Ángela María Robledo, lo que se votó en Cámara fue un régimen de impedimentos y no de inhabilidades como venía desde el Senado. Es decir que **la escogencia de dichos magistrados va en línea con las mismas condiciones que se han puesto a quienes integran las altas cortes** en el país, es decir la Corte Constitucional, el Consejo de Estado o la Corte Suprema de Justicia.

### **Delitos de violencia sexual contra menores** 

Los asesores jurídicos del proceso de paz, Enrique Santiago y Diego Martínez, coinciden en que excluir de los beneficios de la JEP a quienes han cometido delitos sexuales es  "un asunto de físico populismo",  que busca únicamente "legislar en contra del otro". Además, en una entrevista a Caracol Radio el abogado Santiago afirma que los delitos de violencia sexual "nada tienen que ver con el accionar de las FARC" y enseguida agrega que se trata de **un tema que ya estaba excluido en la JEP y por eso ese artículo introducido era innecesario.**

A la representante Robledo le preocupa que se continúe juzgando esta clase de delito desde la justicia ordinaria, pues esta no ha demostrado resultados claros de acceso a la justicia para las víctimas, y en cambio se mantiene un nivel del 98% de impunidad. Razón por la cual, cuenta que en el marco del debate de la JEP **mujeres víctimas de violencia sexual asistieron al Congreso para pedir que en la JEP se incluyera el abuso sexual.**

Asimismo, Robledo dice que el ideal hubiese sido que la JEP fuera compatible con la Ley 1719 de 2014 promovida por ella y el senador Iván Cepeda con la que se buscaba generar un mecanismo para dar garantías de acceso a la justicia para las víctimas de violencia sexual, no obstante se trata de "una ley que está sin estrenar", como lo expresa la congresista.

### **Terceros en la JEP** 

La representante asegura que lo que se hizo en la JEP fue "sacar de tajo a los terceros involucrados en el conflicto armado". Según ha quedado establecido por la Cámara, el Senado e incluso la Corte Constitucional, ante la JEP solo se presentarán obligatoriamente los combatientes tanto de la guerrilla como de las fuerzas militares, pero no estarán los responsables de haber sido determinantes de la guerra, tales como empresarios, ganaderos, comerciantes y funcionarios del Estado.

De tal forma que lo que quedó, para la representante **es "un nicho de impunidad", ya que actualmente, hay cerca de 15 mil compulsas de copias que "duermen en la fiscalía"**, en las que se abría paso a investigar a terceros que fueron denunciados en el marco de la Ley 975 de 2005 Justicia y Paz.

Por su parte, para el jurista Diego Martínez, esta situación con la JEP "es una clara señal de que no hay garantías de no repetición, ya que no hay ningún tipo de condicionamiento o sanción para los actores estatales que también fueron determinantes del conflicto", y adicionalmente manifiesta que "se está intentado reproducir el modelo de impunidad para aquellos que cometieron delitos de graves en el marco del conflicto".

### **¿Qué se puede hacer?** 

Ante tal situación distintas organizaciones defensoras de derechos humanos, encabezadas por el Colectivo de Abogados José Alvear Restrepo, han construido un documento denunciando y argumentando el por qué esta JEP no es garantía de los derechos de las víctima en materia de no repetición, hablando específicamente sobre la posibilidad de que terceros no comparezcan ante los tribunales. Se espera que dicho texto sea presentado ante la Fiscal de la Corte Penal Internacional.

###### Reciba toda la información de Contagio Radio en [[su correo]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio]{.s1}](http://bit.ly/1ICYhVU)[.]{.s1}
