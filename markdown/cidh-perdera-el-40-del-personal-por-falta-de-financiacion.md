Title: CIDH perderá en junio el 40% del personal por falta de financiación
Date: 2016-05-26 16:17
Category: DDHH, Entrevistas
Tags: CIDH, Corte Interamericana de Derechos Humano, Emilio Álvarez, Sistema Interamericano de Derechos Humanos
Slug: cidh-perdera-el-40-del-personal-por-falta-de-financiacion
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/05/emilio-alvarez-CIDH-contagioradio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: dominicanosporderecho] 

###### [26 May 2016] 

La suspensión del periodo de audiencias previsto en la CIDH para Octubre de este ano fue una de las primeras decisiones a las que se vio obligada la Comisión Interamericana de DDHH por la grave crisis financiera que afronta. La falta de financiación pone en riego el Foro más importante de DDHH en América que también **perderá el 40% de su personal** para mediados del mes de Junio si no se aportan los recursos necesarios.

En entrevista especial para Contagio Radio, Álvarez Icasa, Secretario Ejecutivo de la CIDH, afirmó que “de no recibir recursos (…) hacia mediados de junio” la CIDH dejaría de prestar servicios al 40% de las víctimas y se restaría su capacidad de atender los casos que las organizaciones de DDHH presentan ante ese organismo. Según Álvarez Icasa son [tres los factores que tienen en crisis financiera](https://archivo.contagioradio.com/cidh-suspende-audiencias-y-visitas-por-grave-crisis-financiera/) al organismo unilateral.

Por una parte que la **cooperación europea ha centrado sus esfuerzos en la crisis en Siria o en otros asuntos** que consideran de mayor relevancia, otro aspecto tiene que ver con contribuciones que ya no se destinan directamente a la CIDH como en el caso de la comisión de expertos para el caso de Ayotzinapa. Un tercer aspecto tiene que ver con los aportes voluntarios que hacen los países de América y el Caribe, en algunos casos se mantiene, en otros se ha ido reduciendo y en otros sencillamente ha desaparecido.

### **Financiación de la CIDH: Un asunto de voluntad política** 

Emilio Álvarez resalta que hay algunos casos de Estados de **América Latina y el Caribe están entregando importantes recursos a la Corte Penal Internacional** que no tiene ningún caso del continente, mientras que los recursos al SIDH son limitados a pesar de llevar cerca de 7000 casos.

Por otra parte algunos **[Estados no entienden el SIDH como un mecanismo de protección a las víctimas sino como un escenario incómodo](https://archivo.contagioradio.com/la-cidh-esta-siendo-ahogada-por-estados-miembros-de-la-oea/)** para develar situaciones que confrontan sus posiciones respecto de los DDHH.

Además Icasa señala que los análisis que se hacen en torno a que la [CIDH](https://archivo.contagioradio.com/ante-la-cidh-se-expondran-violaciones-a-la-libertad-de-expresion-en-argentina/) estaría siendo condenada a una muerte lenta podrían ser ciertos si no se recibe financiación de manera inmediata. Ante ello afirmó que se realizó un encuentro con la OEA en la que se planteó la situación pero también se están buscando formas de presión y de manifestación pública en las que las víctimas y las organizaciones pueden aportar.

<iframe src="http://co.ivoox.com/es/player_ej_11679408_2_1.html?data=kpajmZ6YdJmhhpywj5aUaZS1lp6ah5yncZOhhpywj5WRaZi3jpWah5yncabhytHW0ZClsNfV08rnjZKPl8bX08rhw9fNs4y3qqm1j4qbh4630NPhw8zNs4zGwsnW0ZCRaZi3jpk%3D&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>
