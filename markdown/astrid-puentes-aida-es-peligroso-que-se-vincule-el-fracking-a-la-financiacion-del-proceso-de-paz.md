Title: “Es peligroso que  se vincule el fracking a la financiación del proceso de paz” AIDA
Date: 2015-01-27 06:20
Author: CtgAdm
Category: Ambiente, Paz
Tags: Ambiente, fracking, proceso de paz
Slug: astrid-puentes-aida-es-peligroso-que-se-vincule-el-fracking-a-la-financiacion-del-proceso-de-paz
Status: published

#### [Foto: www.cuartopoder.es]

Mientras el Ministro de Minas y Energía, Tomás González, considera que el fracking es una actividad necesaria para financiar parte del proceso de paz, frente a estas afirmaciones, Astrid Puentes, Co-Directora Ejecutiva de la Asociación Interamericana para la Defensa del Ambiente, AIDA, afirma que “es bastante peligroso que se vincule el fracking o cualquier otra actividad extractiva con el proceso de paz, aun cuando los ambientalistas apoyan una salida pacífica del conflicto”.

Aunque esta actividad se suele relacionar con el progreso económico, la realidad, es que genera impactos irreversibles para el medio ambiente y la salud.

“El problema del fracking es que al final pueden ser muchos más los costos para el país que los beneficios (…) se pide al gobierno que haga una moratoria y que se asegure que no va a haber impactos, esto no es un escándalo de ambientalistas, son decisiones que se han tomado en diferentes países de manera seria y documentada”.

En Colombia se sigue promocionando en fracking como un sinónimo de desarrollo, sin embargo, en varios países se ha prohibido debido a  los daños que causa, es por ello que desde la AIDA se pide al gobierno revisar la relación costo beneficio de esta actividad.
