Title: VIII Semana por la Memoria en Colombia
Date: 2015-10-06 11:21
Category: eventos, Sin Olvido
Tags: Bogotá, Centro Cultural Gabriel García Márquez, Centro Memoria Histórica, conflicto armado en Colombia, Cucuta, Dibulla, El Castillo, La Guajira, Magdalena, Medellin, Meta, Montería, Pereira, proceso de paz, San Onofre, Sucre y Fundación, tibu, VIII Semana por la Memoria
Slug: viii-semana-por-la-memoria-en-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/10/3e233f3bad8ed1bfa88a23b6d5355c38_XL.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<div class="itemIntroText">

###### Foto: Centro Memoria Histórica 

Del **5 al 12 de octubre, se celebra en Colombia **la **VIII Semana por la Memoria,** en la que se desarrollan diversas actividades en las que las expresiones artísticas hacen memoria por los hechos y las víctimas de casi 60 años de conflicto armado.

En **Bogotá, Medellín, Pereira, Cúcuta, Tibú (Norte de Santander), El Castillo (Meta), Medellín, Montería, Pereira, Dibulla (La Guajira), San Onofre (Sucre) y Fundación (Magdalena),** se realizarán foros, laboratorios artísticos, lanzamientos de proyectos musicales y proyecciones de películas donde se ilustrará los procesos de lucha y resistencia de las víctimas de la guerra en Colombia.

Las y los invitados hablarán de sus motivaciones e intenciones, así como del lugar del artista y gestor cultural frente a la realidad del país, con el fin de intentar responder a las inquietudes de colombianos y colombianas que están a la espera de la firma de un acuerdo de paz con la guerrilla de las FARC en menos de 6 meses.

La instalación de la VIII Semana por la Memoria se realizó este lunes en el Centro Cultural Gabriel García Márquez, de Bogotá y cerrará el 15 de octubre en Medellín, con la conmemoración de los trece años de la Operación Orión, que ocurrió entre el 16 y el 17 de octubre de 2002.

</div>

<div class="itemFullText">

</div>

[Agenda Semana Por La Memoria 2015](https://es.scribd.com/doc/283842712/Agenda-Semana-Por-La-Memoria-2015 "View Agenda Semana Por La Memoria 2015 on Scribd")

<iframe id="doc_5462" class="scribd_iframe_embed" src="https://www.scribd.com/embeds/283842712/content?start_page=1&amp;view_mode=scroll&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="undefined"></iframe>
