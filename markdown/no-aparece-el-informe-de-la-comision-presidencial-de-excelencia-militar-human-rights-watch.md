Title: No aparece el informe de la Comisión Presidencial de Excelencia Militar: HRW
Date: 2020-07-02 21:43
Author: PracticasCR
Category: Actualidad, DDHH
Tags: Derecho Internacional Humanitario, Derechos Humanos, Fuerzas militares, Human Rights Watch, Jose Miguel Vivanco
Slug: no-aparece-el-informe-de-la-comision-presidencial-de-excelencia-militar-human-rights-watch
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/07/AP.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

Un fuerte cuestionamiento al Gobierno Nacional realizó el director para América Latina de **Human Rights Watch, José Miguel Vivanco**, frente a la comisión de expertos creada hace más de un año por el presidente Iván Duque cuya principal tarea era hacer un **seguimiento y monitoreo al actuar de las Fuerzas Militares para identificar si sus prácticas eran acordes con los Derechos Humanos y el Derecho Internacional Humanitario.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

La crítica del director del organismo mundial, va en el sentido de que **en noviembre del año pasado, dicha comisión debía entregar un informe final** donde documentara el seguimiento realizado y se refiriera a las normas internas del Ejército, el cual, **después de más de 8 meses no se ha publicado.**

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/JMVivancoHRW/status/1277940450792439810","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/JMVivancoHRW/status/1277940450792439810

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:heading {"level":3} -->

### La «Comisión Presidencial de Excelencia Militar» creada por Duque

<!-- /wp:heading -->

<!-- wp:paragraph -->

Cabe recordar que en mayo del año pasado el presidente Iván Duque anunció la creación mediante [decreto](https://id.presidencia.gov.co/Documents/190524-decreto-898.pdf) de la «Comisión Presidencial de Excelencia Militar» integrada por el ex fiscal general de la nación,** Alfonso Gómez Méndez** y los exmagistrados de altas Cortes, **Mauricio González y Hernando Yepes.**

<!-- /wp:paragraph -->

<!-- wp:quote -->

> He decidido conformar  una comisión presidencial independiente que haga un análisis riguroso de todas las órdenes, manuales y documentos operacionales y que pueda evaluar con profundidad cómo esas normas procedimientos y protocolos se ajustan a las normas internacionales y nacionales en materia de Derechos Humanos y Derecho Internacional Humanitario
>
> <cite>Presidente Iván Duque. 24 de mayo de 2019. Presentación de Comisión de Expertos</cite>

<!-- /wp:quote -->

<!-- wp:paragraph -->

Esta comisión se creó tras las denuncias de una directiva institucional de las Fuerzas Militares retomadas por el gobierno Duque, cuya estrategia operacional se centra en duplicar los resultados de bajas en combate, lo que propició en su momento el episodio de ejecuciones extrajudiciales, en el que alrededor de 5.000 civiles fueron asesinados por militares que los presentaron como delincuentes. (Le puede interesar: [Hay políticas que propician la criminalidad en las Fuerzas Militares: CCEEU](https://archivo.contagioradio.com/hay-politicas-que-propician-la-criminalidad-en-las-fuerzas-militares-cceeu/))

<!-- /wp:paragraph -->

<!-- wp:core-embed/youtube {"url":"https://www.youtube.com/watch?v=DMqmeVWzNA8","type":"video","providerNameSlug":"youtube","className":"wp-embed-aspect-16-9 wp-has-aspect-ratio"} -->

<figure class="wp-block-embed-youtube wp-block-embed is-type-video is-provider-youtube wp-embed-aspect-16-9 wp-has-aspect-ratio">
<div class="wp-block-embed__wrapper">

https://www.youtube.com/watch?v=DMqmeVWzNA8

</div>

<figcaption>
Video: Presidencia de la República

</figcaption>
</figure>
<!-- /wp:core-embed/youtube -->

<!-- wp:heading {"level":3} -->

### Fuerzas militares cuestionadas por Organizaciones Internacionales

<!-- /wp:heading -->

<!-- wp:paragraph -->

Al cuestionamiento sobre la comisión de expertos, se sumó el reproche que realizó Vivanco a las Fuerzas Militares por los casos de abuso sexual por parte de integrantes del Ejército contra las niñas indígenas de las comunidades Embera Chamí en Puerto Rico, Risaralda y Nukak Maku en San José del Guaviare y las críticas en contra de la Procuraduría y la Fiscalía (Le puede interesar: **[Así es el proceso de sanación de las indígenas tras las múltiples violencias](https://archivo.contagioradio.com/asi-es-el-proceso-de-sanacion-de-las-indigenas-tras-las-multiples-violencias/)**)

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/JMVivancoHRW/status/1277928243908280320","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/JMVivancoHRW/status/1277928243908280320

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

Vivanco también se refirió a otras denuncias recibidas por parte de comunidades indígenas  aparte de las ya conocidas donde la misma comunidad Nukak Makú informó que **a mediados de 2018 dos soldados abusaron sexualmente de dos niñas y otra comunidad donde se afirma «que algunos soldados del Ejército les ofrecen dinero, dulces y gaseosas a menores de edad a cambio de sexo**» y se informaba sobre 4 casos de abuso sexual contra niñas presuntamente cometidos por soldados.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/JMVivancoHRW/status/1278009888216690694","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/JMVivancoHRW/status/1278009888216690694

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

[Human Rights Watch](https://www.hrw.org/es/americas/colombia) no ha sido la única organización internacional en condenar el accionar de las Fuerzas Militares y reprobar la actuación de los entes de investigación; la Oficina en Colombia de la Alta Comisionada de las Naciones Unidas para los Derechos Humanos, documentó en el año 2019, 13 casos de abuso sexual contra menores, en 10 de estos casos, los presuntos perpetradores fueron miembros del ELN y en los 3 restantes miembros del Ejército Nacional,

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En dichos casos la Fiscalía inició investigaciones, pero no hay ni un solo resultado de las mismas. Con base en esto, **la ONU señaló que la respuesta de las autoridades a la violencia y abuso sexual es insuficiente.** (Le puede interesar: [ONU y CIDH estarían preparando pronunciamientos sobre situación de DDHH en Colombia: CEJIL](https://archivo.contagioradio.com/onu-cidh-cejil-colombia/))

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
