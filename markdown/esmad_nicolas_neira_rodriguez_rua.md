Title: Medida de aseguramiento contra agente del ESMAD Rodríguez Rúa por asesinato de Nicolás Neira
Date: 2017-10-22 17:24
Category: Judicial, Nacional
Tags: ESMAD, nicolas neira
Slug: esmad_nicolas_neira_rodriguez_rua
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/08/nicolc3a1s-neira-matera.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Colombia en contexto] 

###### [21 Oct 2017] 

En el marco del proceso que se adelanta contra los agentes del ESMAD implicados en el asesinato del joven Nicolás Neira en el año 2005, el juez 71 de garantías de Bogotá ordenó medida de aseguramiento bajo detención domiciliaria en contra del agente de la Policía, Néstor Julio Rodríguez Rúa.

Según testigos, Rúa fue quien disparó el “lanzagases” que golpeó fuertemente la cabeza de Nicolás, y por lo que tras dos días de estar hospitalizado falleció. Sin embargo, durante la audiencia  este agente no aceptó los cargos imputados. Por su parte, el capitán Julio César Torrijos Devia, quien era comandante de la Primera Sección del ESMAD de la Policía Metropolitana de Bogotá, tampoco aceptó cargos luego de la imputación y la detención domiciliaria.

**La Fiscalía ha solicitado medida de aseguramiento en establecimiento carcelario así como también **la Procuraduría. Mientras que la defensa intentó dilatar la audiencia, alegando nuevamente, conflicto de competencia. Sin embargo, tanto el fiscal como la procuradora pidieron que continuara la audiencia.

Por su parte, ante la a orden del Consejo Superior de la Judicatura de llevar el caso a la justicia ordinaria, y compulsar copias para ordenar investigación por la tardanza y negligencia en el caso de Nicolás Neira, la defensa de los policías interpuso una tutela contra la decisión del Consejo. (Le puede interesar: [Fiscalía será investigada por negligencia en proceso por asesinato de Nicolás Neira)](https://archivo.contagioradio.com/fiscalia-sera-investigada-por-negligencia-en-el-caso-de-nicolas-neira/)

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
