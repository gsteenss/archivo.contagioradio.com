Title: Comunidad indígena Sikuani en Vichada, denuncia permanente olvido estatal
Date: 2017-08-01 19:36
Category: DDHH, Nacional
Tags: audiencia pública, indígenas
Slug: comunidad_indigena_sikuani_gobierno_audiencia_publica
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/08/sikuani-e1501614562344.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: jenzera.org 

###### [2 Ago 2017] 

Las comunidad indígena sikuani ubicada en el departamento de Vichada, denuncia el constante olvido estatal que tiene a ese pueblo en grave riesgo de extinción. Frente a ello quieren llamar la atención mediante una **Audiencia Pública de rendición de cuentas convocada para el próximo 9 de agosto.**

Ante la presencia de funcionarios del gobierno de alto nivel, las y los líderes indígenas manifestarán la situación en la que se encuentra su pueblo. Cabe recordar que a finales del año pasado, las autoridades indígenas señalaron que varios niños y niñas indígenas Sikuani fallecieron por enfermedades asociadas a la desnutrición, frente a lo cual los médicos pidieron al gobierno mayor apoyo pues no existen condiciones dignas de vida para la comunidad.

“El exterminio físico y cultural no solo ha sido originado por el desplazamiento forzado a causa del conflicto armado. Esto también obedece a otras causas como, las políticas del Estado excluyentes y a la ausencia total del mismo en la promoción y defensa de los derechos territoriales de nuestros pueblos, que **han permitido la afectación a nuestros territorios por la agroindustria, la explotación de hidrocarburos y la minería**”, señalan en un comunicado.

Pese a que hay una **asignación especial del Sistema General de Participación para resguardos indígenas – AESGPRI**, que tiene como fin poner en funcionamiento en los territorios indígenas la administración de los sistemas propios de estos pueblos y establece funciones, con el fin de proteger, reconocer, respetar y garantizar el ejercicio y goce de los derechos fundamentales de los Pueblos Indígenas a la fecha lo único palpable es la ausencia estructural del Estado.

Frente a esta situación este 9 de agosto en el Centro Educativo el Trompillo – Municipio de la Primavera, Vichada, se exigirá mejores condiciones para la comunidad en aspectos como **tierras y territorios, Victimas, Sistema General de Participación, educación, salud, saneamiento y agua potable, además de vías de comunicación.**

###### Reciba toda la información de Contagio Radio en [[su correo]
