Title: Paramilitares atentaron contra integrantes de la Comunidad de Paz de San José de Apartadó
Date: 2017-12-29 15:06
Category: DDHH, Nacional
Tags: Comunidad de Paz de San José de Apartadó, Padre Javier Giraldo, paramilitares
Slug: atentado-comunidad-san-jose-apartado
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/08/paramilitares-e1472682962275.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: zonacero 

###### 29 Dic 2017 

En la mañana de hoy 29 de diciembre paramilitares atacaron a integrantes de la comunidad de San José de Apartadó, en el asentamiento central de San Josesito, en el lugar se encontraba el líder *Germán Graciano Posso y el padre Javier Giraldo. *

Según el comunicado público emitido por la comunidad de paz, paramilitares ingresaron a la comunidad, intentaron encerrar a varios de ellos en un cuarto. Los paramilitares los amenazaron con sus armas y se generó un forcejeo en el que lograron inmovilizar y quitarles las armas a dos de los paramilitares. En el esfuerzo por desarmar a los hombres fue herido Graciano Posso, representante legal y Robiro reconocidos líderes de la comunidad.

La denuncia de la comunidad enfatiza que la intensión de los paramilitares al mando de alias "Felipe" era asesinar a Graciano Posso, y por ello hubo varios movimientos previos encaminados, según la denuncia, a identificar y ubicar al líder comunitario.

\[caption id="attachment\_50341" align="alignnone" width="800"\]![Tweet: Comunidad de Paz @cdpsanjose](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/12/comunidad-san-jose.jpg){.size-full .wp-image-50341 width="800" height="530"} Tweet: Comunidad de Paz / @[cdpsanjose](https://twitter.com/cdpsanjose)\[/caption\]

<p style="text-align: justify;">
<script src="https://platform.twitter.com/widgets.js" async charset="utf-8"></script>
La comunidad retuvo a dos de los paramilitares y a hacia la 1 de la tarde esperaban la llegada de una misión de la Defensoría del Pueblo para entregar a los armados que  atentaron contra los líderes e integrantes de la comunidad de paz.    

<script src="https://platform.twitter.com/widgets.js" async charset="utf-8"></script>
</p>
La Defensoría del Pueblo a través de su cuenta de Twitter manifestó: "*Condenamos y rechazamos hechos violentos contra Germán Graciano Posso de la Comunidad de Paz de San José de Apartadó, Antioquia. Exigimos investigación rápida y garantías de seguridad para la comunidad" *

En su comunicado la comunidad informó de un dialogo que tuvieron con el  Vicepresidente de la República, General Óscar Naranjo, a quién pidieron mayor garantía para la comunidad y una investigación que examine si existe complicidad entre las fuerzas militares y los grupos armados que tienen presencia en San José de Apartado. (Ver: [Paramilitares afianzan presencia en San José de Apartadó ](https://archivo.contagioradio.com/paramilitares-afianzan-presencia-en-san-jose-de-apartado/))

Durante el mes de diciembre se han presentado más hechos contra la comunidad de paz los cuales han sido denunciados por la misma, entre ellos reuniones a las que han sido invitados los pobladores de la comunidad, presencia de hombres con armas largas, en desarrollo de operaciones encubiertas de tipo paramilitar, e intimidando a los líderes y acompañantes internacionales.

###### Reciba toda la información de Contagio Radio [en su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio ](http://bit.ly/1ICYhVU) 
