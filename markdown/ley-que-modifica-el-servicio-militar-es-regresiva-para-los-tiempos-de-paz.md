Title: Objeción de Conciencia quedó incluida en proyecto de ley que modifica el servicio militar
Date: 2017-06-20 12:39
Category: DDHH, Nacional
Tags: Justapaz, ministerio de defensa, objeción de conciencia, servicio militar
Slug: ley-que-modifica-el-servicio-militar-es-regresiva-para-los-tiempos-de-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/06/Servicio-Militar.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Colectivo Brecha] 

###### [20 Jun. 2017] 

Luego de la aprobación por parte del Congreso del proyecto de ley que modifica el servicio militar obligatorio se han despertado críticas en torno a que ese servicio no debería ser obligatorio en tiempos de paz. La nueva norma fue aprobada por 58 votos a favor y **entre otras medidas unifica la prestación de servicio militar para soldados, campesinos y regulares a 18 meses.**

En general dos han sido las posiciones suscitadas, una ha sido la de las personas beneficiadas con las decisiones tomadas y por otro lado está la de diversos sectores políticos y sociales que, aunque saludan la inclusión de medidas como la objeción de conciencia, no ven con buenos ojos la continuidad de esa política de Estado. Le puede interesar: [1.294 jóvenes murieron prestando el servicio militar en los últimos 23 años](https://archivo.contagioradio.com/1-294-jovenes-murieron-presentando-el-servicio-militar-en-los-ultimos-23-anos/)

Según **Mireya Rojas, integrante de Justapaz**, aunque pudieron realizar algunas modificaciones y adiciones al texto presentado por el Ministerio de Defensa, esa propuesta era muy regresiva y represiva en tiempos de paz y “de ninguna manera cumplía las promesas de campaña de Juan Manuel Santos en tanto la eliminación del servicio militar”.

**Dentro de las inclusiones se logró agregar el texto de la objeción de conciencia como causal de exoneración del servicio militar obligatorio** “nosotros como organizaciones presentamos nuevas proposiciones para ampliar el tema de la objeción de conciencia sobre todo en el procedimiento porque allí es donde hay mayores vacíos” recalcó Rojas. Le puede interesar: [Servicio militar obligatorio es rechazado por los jóvenes en Colombia](https://archivo.contagioradio.com/servicio-militar-obligatorio-es-rechazado-por-los-jovenes-en-colombia/)

Cabe recordar que la Corte Constitucional en el 2016 planteó que la Comisión Interdisciplinaria debía garantizar la aprobación de este derecho, sin embargo, denuncia Justapaz, que en términos procedimentales esto no ha sido tan claro.

### **La Objeción de Conciencia una opción para no ir al servicio militar** 

Asegura Rojas que la inclusión de la objeción de conciencia en este proyecto de ley **permitirá que los jóvenes en Colombia puedan declararse objetores a la guerra, a la violencia**. Además de obligar a los comandos o distritos de reclutamiento a garantizar ese derecho.

“Vemos con buenos ojos el avance, pero aún tenemos bastantes dudas y sabemos que el trabajo no termina allí” dijo la integrante de Justapaz. Le puede interesar: [Por el derecho a decir: 'NO al servicio militar obligatorio'](https://archivo.contagioradio.com/por-el-derecho-a-decir-no-al-servicio-militar-obligatorio/)

### **Algunos puntos a tener en cuenta de la nueva ley** 

Lo que hace este proyecto de ley es reformar la actual ley 48 de 1993 que era la que regulaba todo lo relacionado con el reclutamiento y movilización en el país, es decir que con esta nueva directriz se replantean algunos asuntos de la anterior, pero **se reafirma la prestación del servicio militar como obligatorio** para los varones colombianos mayores de 18 años.

1.  Los soldados campesinos y los regulares (quienes no han terminado su bachillerato o no tienen más oportunidades) ahora pasarán de **24 meses de servicio militar a 18 meses.**
2.  Los jóvenes que son bachilleres seguirán prestando servicio militar **durante 12 meses.**
3.  De la ley se retiró el perfil policivo del Ejército, es decir no podrán hacer retenes, ni controles en las calles o en establecimientos públicos por ejemplo.
4.  Se establece con mayor detenimiento los **requisitos para declararse objetor de conciencia.**
5.  Se incluye **una modalidad de servicio social militar** que podrá hacerse en escuelas, hospitales o colegios entre otros. Posibilidad que deberá ser regulada por las fuerzas militares a la luz del Derecho Internacional Humanitario DIH.
6.  Se incluye **la amnistía en el país para quienes se encuentren remisos**. Los cuales suman alrededor de 920 mil jóvenes que no han logrado definir su situación militar.

### **Mujeres serán incentivadas para enlistarse en el Ejército** 

Según Rojas, el hecho que se pretenda incentivar a las mujeres para ingresar a las Fuerzas Militares son tan solo una más de las preocupaciones que tienen como organización social, dado que **las modificaciones en la ley buscan que más jóvenes se enlisten para lograr un sueldo.**

“Vemos con preocupación que las mujeres en algún momento por falta de oportunidades en este país pues se enlistan en el Ejército. La ley se mantiene en no obligar a las mujeres a prestar servicio militar pero lo que hemos visto es que **se ha convertido en un espacio atractivo en términos laborales y de estabilidad para muchas mujeres en el país”** asevera Rojas.

Y en ese orden de ideas recalca la integrante de Justapaz, el llamado es a decir que si se garantiza el derecho de estudio y trabajo estable para las mujeres y hombres que hagan parte de las fuerzas militares, **debe hacerse lo mismo con las demás personas del país, por ejemplo el sector del magisterio. **Le puede interesar: [Servicio militar está siendo sometido a “publicidad engañosa” por el gobierno](https://archivo.contagioradio.com/servicio-militar-esta-siendo-sometido-a-publicidad-enganosa-por-parte-del-gobierno/)

Actualmente, los soldados comenzarían a recibir \$320 mil pesos mensuales, antes recibían un poco menos de \$90 mil pesos. **Un aumento que representaría un 280% para el cual también tendría que aplicarse la regla fiscal** que se aplica  a la educación, entre otros sectores, sin embargo, el aumento sigue siendo precario y representa solo la mitad del salario mínimo.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)

<div class="osd-sms-wrapper">

</div>
