Title: Humala y su esposa en prisión preventiva por caso Odebrecht
Date: 2017-07-14 13:09
Category: El mundo
Tags: Humala, Odebrecht, Perú
Slug: humala-peru-odebrecht
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/07/humala_heredia_el_comercio.jpeg_1718483347.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: El Comercio 

###### 14 Jul 2017 

El expresidente de Perú **Ollanta Humala** (2011-2016) y su esposa **Nadine Heredia** se entregaron a la justicia luego que el juez dictó **detención preventiva** contra la pareja por presunto **delito de lavado de activos y asociación ilícita par delinquir**, relacionados con el caso de la multinacional brasilera Odebrecht.

La investigación del caso, en cabeza del fiscal del lavado de activos Germán Juárez Atoche, señala la presunta responsabilidad de Humala en la presunta recepción de **tres millones de dólares** de la constructora Odebrecht para su campaña presidencial en 2011, un favor que según lo recogido en el proceso se habría pagado mediante **la entrega de un contrato para construir un oleoducto**.

En la decisión del juez Richard Concepción de ordenar la **detención de la pareja por 18 meses**, consideró que existen elementos suficientes para cambiar la condición de Humala y su esposa, **pasando de comparecencia restringida a prisión preventiva**, para que la Fiscalía continué su investigación sin el riesgo que los vinculados emprendan una fuga hacia el exterior.

De consolidarse el caso, Humala sería el  **primer expresidente procesado por el caso Odebrecht** por el que también esta siendo investigado desde marzo pasado el ex mandatario **Alan García**, sindicado por **presuntamente cometer delitos contra la administración publica**,  durante la durante la ejecución del proyecto de construcción de la línea 1 del metro de Lima durante su segundo gobierno (2006-2011).

Los esposos, quienes ingresaron la noche del jueves al calabozo del Palacio de Justicia, se encuentra a la espera de la asignación de la cárcel a la que serán recluidos. (Le puede interesar: Odebrecht: [la punta del iceberg de la corrupción en Colombia](https://archivo.contagioradio.com/odebrecht-la-punta-del-iceberg-de-la-corrupcion-el-colombia/))

##### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
