Title: Paramilitares amenazan candidatos a la Alcaldía en Ovejas, Sucre
Date: 2019-08-08 18:17
Author: CtgAdm
Category: Comunidad, Política
Tags: Autodefensas Gaitanistas de Colombia, Elecciones 2019, Ovejas, San Onofre, Sucre
Slug: paramilitares-amenazan-candidatos-a-la-alcaldia-en-sucre
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/08/Sucre-Panfleto.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Alcaldía Municipal de San Onofre, Sucre] 

En días pasados en el municipio de Ovejas, Sucre, se conoció un nuevo panfleto atribuido a las autodenonominadas Autodefensas Gaitanistas el cual, previo a las elecciones locales de Alcaldía y Gobernación amenaza a los candidatos e integrantes de partidos alternativos como FARC, UP, Alianza Verde y Colombia Humana buscando que declinen a las candidaturas y no sean apoyados.

En el territorio ya han realizado advertencias sobre el control territorial que vienen ejerciendo las AGC y las presiones que en épocas electorales se han dado contra la comunidad en municipios como San Onofre o en Ovejas, donde su alcalde, Mauricio García, fue amenazado por trabajar junto al partido FARC en proyectos de reincorporación.

El investigador del Movimiento de Víctimas de Crímenes de Estado (MOVICE), Rodrigo Ramírez Salazar, afirma que pese a las advertencias y denuncias que se han hecho, existe "una negativa por tarde del Estado a afrontar la problemática". [(Le puede interesar: Prácticas paramilitares del pasado resurgen en Sucre)](https://archivo.contagioradio.com/practicas-paramilitares-del-pasado-resurgen-en-sucre/)

En el panfleto, el grupo armado menciona que no "participan en política", algo que para el investigador resulta una contradicción pues el hecho de **amenazar a partidos que representan un sector político y estigmatizar a sus candidatos es una forma de justificar la violencia** o prácticas de rechazo con ánimo de actuar en una contienda electoral.

"Estamos en un momento coyuntural en Sucre y a nivel nacional donde las fuerzas la alternativas que quieren un cambio están viéndose respaldadas por una población que se cansó de la corrupción y los vínculos de la política con el paramilitarismo", afirma Ramírez.

### En Sucre hay una esperanza de cambio

Ramírez resalta lo delgado que es el tejido social en Montes de María y lo frágil que es ante estas amenazas, "un panfleto afecta de manera significativa a un candidato. A la gente le da temor reunirse con él o respaldarlo y al candidato le da temor ir a algunas zonas donde no hay seguridad" debilitándole y dejándolo en condiciones de vulnerabilidad y desigualdad.

El investigador concluye que si bien es cierto que hay una sed esperanza y de cambio en los territorios, sin garantías para los candidatos y la población es muy difícil que pueda materializarse esta transformación en un nuevo gobierno local.

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe><iframe id="audio_39649161" frameborder="0" allowfullscreen scrolling="no" height="200" style="border:1px solid #EEE; box-sizing:border-box; width:100%;" src="https://co.ivoox.com/es/player_ej_39649161_4_1.html?c1=ff6600"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
