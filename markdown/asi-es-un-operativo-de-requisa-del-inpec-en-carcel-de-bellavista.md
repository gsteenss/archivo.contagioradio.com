Title: Así es un operativo de requisa del INPEC en Cárcel de Bellavista
Date: 2016-08-19 12:50
Category: DDHH, Nacional
Tags: crisis carcelaria, INPEC, presos politicos
Slug: asi-es-un-operativo-de-requisa-del-inpec-en-carcel-de-bellavista
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/08/carcel1_0.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Radio Nacional 

###### [19 Ago 2016] 

Presos del Patio 16 de la Cárcel Bellavista de Medellín, denuncian que en medio de un operativo de requisa, el pasado miércoles, **guardias del INPEC del centro penitenciario dañaron e incluso robaron, artículos de aseo, comida,** medicamentos entre otros elementos, además de haber roto fotografías de los familiares de los internos.

De acuerdo con la denuncia, dañaron artículos de primera necesidad; les quitaron comida como galletas, mantequilla, gaseosas, azúcar, atún, leche; además aseguran que destruyeron colchonetas, retretes, lava manos, y les quitaron artículos como televisores y radios a algunos presos políticos, pese a que contaban con los permisos. **Una de las denuncias más graves, es la destrucción de medicamentos esenciales para la salud de uno de los presos.**

Aunque los internos exigieron que se tomaran fotografías para evidenciar ante los organismos competentes, no ha habido algún tipo de respuesta por parte del INPEC. Se trata de una "persecución de la guardia especial mente en contra de los presos políticos, exigimos que cesen los atropellos de la guardia, destruyen todo sin justificación, e inluso dañan fotos de nuestros familiares".

Este tipo de denuncias no son nuevas. Cabe recordar que el pasado mes de mayo, desde la Fundación Comité de Solidaridad con Presos Políticos, se denunció que al interno **José Ángel Parra **recluido en el centro penitenciario ERON - Picota, la guardia del INPEC **le destruyó sus pertenencias incluido su medicamento para tratar la leucemia en fase terminal que sufre.**

<iframe src="http://co.ivoox.com/es/player_ej_12599608_2_1.html?data=kpeim56adJmhhpywj5WaaZS1kpaah5yncZOhhpywj5WRaZi3jpWah5yncaXZz9rbxc7Fb9Hmxtjc1ZCmqc3gwtvW1dnFcYarpJKw0dPYpcjd0JC_w8nNs4yhhpywj5k.&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
