Title: Nace el Movimiento 27 de Marzo por la dignidad de los artistas en Colombia
Date: 2017-03-22 12:33
Category: eventos
Tags: colombia, Cultura, movimiento 27 de marzo, teatro
Slug: teatrocolombiamovilizacion
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/03/27m.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: 27 de marzo 

###### 22 Mar 2017 

Artistas que trabajan por el teatro vienen organizando una movilización con la que buscan **sentar una protesta frente a ciertas decisiones gubernamentales** que en su sentir "desmejoran las oportunidades para el desarrollo" de las artes escénicas en Colombia, iniciativa que da como fruto lo que han denominado **Movimiento 27 de marzo**.

El nombre del movimiento surge en conmemoración a la fecha en que se celebra el día internacional del teatro. Para ese día se ha programado que artistas del teatro y otras disciplinas en todo el país, salgan a espacios representativos de sus ciudades con sus colores y creaciones, con el ánimo de protestar y "**poner en conocimiento de todos los conciudadanos la crisis por la que atraviesa el arte y la cultura en Colombia**".

**En Bogotá, la movilización tendrá lugar en la Plaza de Bolívar**, con un acto que se realizará de manera simultánea en el salón Boyacá del Congreso de la República, al cual están invitados los Ministros de Cultura, Hacienda y Educación. El evento será transmitido en directo por el Canal del Congreso.

El próximo jueves 23 de marzo, los **voceros del Movimiento realizarán una rueda de prensa a partir de las 8:30 de la mañana** en el tradicional Teatro La Candelaria, en la que darán a conocer más detalles de la propuesta. Le puede interesar: "[El vuelo del Alcaraván":Memorias de lucha en Arauca ](https://archivo.contagioradio.com/vuelo-alcaravan-documental/)
