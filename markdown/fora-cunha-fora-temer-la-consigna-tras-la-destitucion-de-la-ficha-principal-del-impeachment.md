Title: "Fora Cunha, Fora Temer" la consigna tras la destitución de la ficha principal del impeachment
Date: 2016-09-13 16:28
Category: El mundo, Otra Mirada
Tags: Dilma Rouseff, Eduardo Cunha, Golpe de estado Brasil, Impeachment
Slug: fora-cunha-fora-temer-la-consigna-tras-la-destitucion-de-la-ficha-principal-del-impeachment
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/09/2016-05-05T152048Z_01_SAO04_RTRIDSP_3_BRAZIL-POLITICS_20160505172334-kuyF-992x558@LaVanguardia-Web.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### Foto: Vanguardia.com 

##### 13 Sep 2016

Al son de los canticos "Fuera Cunha" y luego de una sesión de casi cinco horas, la Cámara de Diputados en Brasil decidió por votación este lunes destituir de su cargo al diputado **Eduardo Cunha**, uno de los principales impulsores del impeachment por el cual [Dilma Rousseff fúe retirada de su cargo](https://archivo.contagioradio.com/las-reacciones-en-america-latina-tras-la-destitucion-de-dilma-rousseff/) como presidenta del pais más grande del continente.

**45o votos a favor**, diez en contra y 9 abstenciones, determinaron el destino del representante del Partido del Movimiento Democrático Brasilero, quien habia sido [apartado de la presidencia de la Cámara](https://archivo.contagioradio.com/destitucion-de-cunha-se-da-despues-de-realizado-el-trabajo-sucio/) el pasado 5 de mayo, una semana despúes de que se votara a favor del procedimiento de juicio político contra la entonces mandataria.

Por la determinación parlamentaria, solicitada por la Procuraduría General de la República y concedida por Teori Zavascki, ministro del Supremo Tribunal Federal, **Cunha queda inhabilitado para ejercer cargos públicos por 7 años y continua su proceso por la Operación Lava Jato que investiga casos de corrupción en la Petrobras.**

Por este caso el ahora ex diputado, es acusado de** haber recibido \$5 millones de dólares** referente a un contrato del astillero Samsung Heavy Industries con la Petrobras y por la supuesta **recepción y movimientos de sobornos** en cuentas secretas en Suiza, cuyo origen sería la compra por parte de la Petrobras, de un campo de petróleo en Benin, África.

Adicionalmente Cunha es investigado por sus vinculaciones en los **desviós presupuestales en la construcción del "Porto Maravilla"**, en Rio de Janeiro y por acciones utilizadas para “**intimidar legisladores, acusados, colaboradores, abogados y antes públicos con el objetivo de obstruir o demorar las investigaciones**”.

Ante la  soledad con la que se presentó el ex presidente de la Cámara, varios diputados mencionaron que hace pocos meses, varios de los compañeros que hoy le dan la espalda tomaban vodka con el en Rusia y ahora son ministros e incluso el mismo presidente Michel Temer, quien fué el primero que lo abandonó a su suerte.

En ese sentido, la diputada Jandira Feghali, aseguro que el "**Fuera Temer**" va a crecer por todo el país por que la población entiende que "**Temer es Cunha y Cunha es Temer**", y que necesariamente la situación del segundo tendrá insidencia en el apoyo al gobierno desde la Cámara.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 

 
