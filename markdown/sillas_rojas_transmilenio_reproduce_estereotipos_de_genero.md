Title: Sillas rojas para mujeres en Transmilenio reproducen estereotipos de género
Date: 2017-05-04 19:44
Category: Mujer, Nacional
Tags: Concejo de Bogotá, Transmilenio, Violencia de género, violencia machista
Slug: sillas_rojas_transmilenio_reproduce_estereotipos_de_genero
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/05/TransMilenio-Calle-100-RCN-9-copia-e1493944742569.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: RCN La Radio 

###### [4 May 2017] 

La propuesta de las sillas rojas reproduce los estereotipos de género, es una medida de segregación hacia las mujeres que por su misma naturaleza es violenta, y además se trata de una medida que **el propio sistema de Transmilenio es incapaz de atender.**

Esa es una de las visiones de las organizaciones de mujeres sobre la propuesta del concejal de Bogotá, Marco Fidel Ramírez. Para Laura Torres, integrante de Católicas por el Derecho a Decidir, es una idea **que confunde a la sociedad sobre lo que realmente es el feminismo, y en cambio alimenta los imaginarios errados sobre las luchas por los derechos de las mujeres.**

“No nos sorprende que sea una propuesta del concejal Ramírez que ha estado en contra de los derechos de las mujeres y la diversidad sexual”, expresa Torres. Y es justamente su autor, y el mismo contenido de la iniciativa lo que reproduce estereotipos de género. **“Muestra que supuestamente las mujeres somos más débiles, incapaces, que necesitamos ayuda”**.

Para las organizaciones de mujeres se trata entonces de **“una iniciativa descabellada”, y un desgaste legislativo**. Lo que se debe plantear para prevenir cualquier tipo violencias hacia las mujeres, son medidas estructurales que promuevan la implementación efectiva de cada Ley en contra del maltrato por cuestiones de género, que además debe ir acompañado de una fuerte campaña de educación y sensibilización enmarcada en el respeto hacia la otra y el otro.

Si de lo que se trata es de realizar una acción concreta sobre el Transmilenio, debe haber una acción de vigilancia más afectiva y una campaña que genere pactos de respeto y no violencia hasta las mujeres, explica Torres, quien concluye, “**Nosotras también podemos ceder la silla, y además también nos violentan así estemos sentadas.** La separación y segregación definitivamente no es una solución”.

###### Reciba toda la información de Contagio Radio en [[su correo]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio]{.s1}](http://bit.ly/1ICYhVU)[.]{.s1}
