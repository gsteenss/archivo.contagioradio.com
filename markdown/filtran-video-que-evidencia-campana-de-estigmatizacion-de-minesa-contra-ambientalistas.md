Title: Filtran video que evidencia campaña de estigmatización de Minesa contra ambientalistas
Date: 2019-04-23 18:01
Author: CtgAdm
Category: Ambiente, Comunidad
Tags: Autoridad Nacional de Licencias Ambientales, Minesa, Páramo de Santurbán
Slug: filtran-video-que-evidencia-campana-de-estigmatizacion-de-minesa-contra-ambientalistas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/Captura-de-pantalla-67.png" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/Captura-de-pantalla-72.png" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Video filtrado] 

El Comité[ por la Defensa del Páramo de Santurbán compartió ayer un video filtrado que muestra a Santiago Urdinola, presidente de la Sociedad Minera de Santander (Minesa), detallando una estrategia mediática para impulsar la solicitud de licencia ambiental que presentaron para la construcción de Soto Norte, un proyecto minero de extracción de oro que, según organizaciones ambientalistas,  afectaría nocivamente al Páramo de Santurbán.]

[En este video de una reunión privada de Minesa, se ve a Urdinola hablando de una estrategia de comunicaciones que tiene como prioridad tener "tranquilos" a las autoridades competentes en Bogotá, durante el proceso de la solicitud de la licencia ambiental, esto a pesar de las manifestaciones públicas de las comunidades en contra de este megaproyecto. "**Si**]**yo tengo el mundo incendiado, pero en Bogotá sienten que estamos bien, estamos bien**", sostuvo.

El presidente continúa, diciendo que la manera indicada de minimizar las voces contradictorias a la mina es desarrollar una narrativa que presenta a los manifestantes como militantes de la oposición, vinculados a **los movimientos políticos del Senador Gustavo Petro, al Partido Verde, al Polo Democrático y a algunos élites de Bucaramanga,** que estarían buscando a través de estas manifestaciones tumbar el Gobierno del Presidente Iván Duque. (Le puede interesar: "[Páramo de Santurbán nuevamente amenazado por solicitud de Minesa](https://archivo.contagioradio.com/solicitud-explotacion-oro-presenta-nuevas-amenaza-al-paramo-santurban/)")

Mayerly López, vocera del Comité[ por la Defensa del Páramo de Santurbán, señala que este video filtrado confirma las denuncias que los ambientalistas organizados en contra de este proyecto habían publicado por años y que es la empresa la que emprende en una campaña de estigmatización para deslegitimar a los manifestantes señalándolos como politiqueros de izquierda, por el solo hecho de que algunos contradictores son integrantes de partidos políticos. ]

El Comité también compartió otro video de la misma reunión en que el presidente afirma que el proyecto minero Soto Norte, al ser aprobado, **estaría desplazando a 50 familias**, "de sus territorios donde han vivido por 100 años porque están en la huella del proyecto".

Frente estos hechos, el Comité planea **mandar una denuncia a la ANLA para que este video se tenga en consideración al tomar una decisión** frente la solicitud de licencia ambiental que presenta la empresa y esperan que las autoridades competentes investiguen los procederes de la empresa. Adicionalmente, el Comité realizará una marcha el **próximo 10 de mayo** para presionar a las autoridades a rechazar la solicitud de Minesa.

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/video.php?href=https%3A%2F%2Fwww.facebook.com%2Fcomitesanturban%2Fvideos%2F838925443108659%2F&amp;show_text=0&amp;width=560" width="560" height="308" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/video.php?href=https%3A%2F%2Fwww.facebook.com%2Fcomitesanturban%2Fvideos%2F2110111199104844%2F&amp;show_text=0&amp;width=560" width="560" height="315" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
