Title: After death of protestor, Colombians call for dismantling of riot control unit
Date: 2019-11-27 18:50
Author: CtgAdm
Category: English
Tags: ESMAD, National Strike, Police brutality
Slug: after-death-of-protestor-colombians-call-for-dismantling-of-riot-control-unit
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/ESMAD.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Photo: Contagio Radio] 

 

**Follow us on Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

[Human rights organizations are once again calling for the National Police’s riot control unit to be dismantled after 18-year-old Dilan Cruz died yesterday from wounds sustained by a police officer of the Mobile Anti-Disturbances Squadron (ESMAD) with an allegedly unauthorized weapon. Cruz’s death is the latest in police abuses that have sparked outrage nation-wide and intensified protests.]

[Critics of the ESMAD say that the unit has been employed to attack peaceful protests with controversial methods, both in cities and rural areas, that constitute grave human rights violations. The human rights lawyer Germán Romero said that the government has opted to send the ESMAD unit to control unrest instead of addressing the demands of citizens. ]

[This riot control unit started in 1999 with 19 units and expanded to include 3,580 police officers throughout the country in 2018. The riot control unit has a history of abuses and brutality that have gone unrecognized by the police. According to information from the 070 Self-protection Manual against the ESMAD, 20 people have been killed by the unit, most of them peasants, students and indigenous protestors. The Inspector General’s Office registered 150 complaints from 2007 to 2017.]

[Meanwhile, the Centre for Investigation and Popular Education counted 540 acts of repression and violence in 2017 — most of them committed by the ESMAD. Most recently, the Ombudsman's Office reported an increase in citizen complaints of the ESMAD during this year’s national strike in contrast to the agrarian strike of 2013. While the ESMAD was ordered by Colombia’s top administrative court to teach human rights to its officers, this measure has yet to be implemented. ]

[Romero said that the training of ESMAD forces “is designed to take an attitude and form to repress demonstrations.” There is evidence, he said, that members of the Armed Forces infiltrate protests, provoke students, harm themselves and detonate grenades to justify attacks on protestors. ]

[There is also the issue of impunity. Romero said that after the university student Jhony Silva was killed by an ESMAD agent in 2005, the captain of the unit Gabriel Bonilla was promoted to commander of the ESMAD nationally and later became commander of the Metropolitan Police in Ibagué. “Those who commit human rights violations are rewarded,” the lawyer said. There are 1,200 ESMAD-related cases that are under investigation.]

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
