Title: Cháchara y calienta puestos
Date: 2020-05-12 12:30
Author: JOHAN MENDOZA TORRES
Category: Johan Mendoza, Opinion
Tags: Clases Virtuales, Covid-19, cuarentena, cuarentena en colombia, educacion, Educación Virtual
Slug: chachara-y-calienta-puestos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/05/diseno-sin-2020_03_28_001603@2x.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/05/AUDIO-JOHAN-online-audio-converter.com_.mp3" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:heading {"align":"right","level":6,"textColor":"cyan-bluish-gray"} -->

###### Imagen de: Orientación Universia {#imagen-de-orientación-universia .has-cyan-bluish-gray-color .has-text-color .has-text-align-right}

<!-- /wp:heading -->

<!-- wp:heading {"level":5} -->

##### Gran sorpresa se llevó el mundo de la academia al tener que transformar en menos de una semana todos sus programas presenciales en virtuales. 

<!-- /wp:heading -->

<!-- wp:paragraph -->

Al principio comenzó un típico debate colombiano, un debate leguleyo sobre *“cómo denominar la nueva situación”* que no se podía decir virtual, sino que era mejor decir no-presencial, que no era a distancia, sino que era presencialidad mediada, en fin, debates típicos colombianos no solo por su trasfondo legalista, sino por su desenfoque coyuntural. Es que, si pudiéramos hacer una analogía metafórica, Colombia siempre parece un francotirador con miopía y astigmatismo sin gafas. 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Seguidamente, la sorpresa llegó con sus problemas de conectividad, así como con la propia lógica comunicativa de cada aplicación utilizada, fuera Meet, Zoom, Blackboard, Skype, Moodle, y hasta el mismo WhatsApp.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":5} -->

##### Micrófonos abiertos, silencios prolongados, interferencias con dos o más micrófonos abiertos, imágenes congeladas, cansancio de espalda, del nervio ocular, desazón profunda ante el peso de la muerte de la normalidad y así, de a pocos, pero todos, viviendo ese caos del que jamás habló Hesíodo.   

<!-- /wp:heading -->

<!-- wp:paragraph -->

No llevo muchos años en la actividad de la docencia, son casi 7, con experiencia en universidad pública y privada, lo cual no me deja mucho margen para opinar con el criterio global que ofrece una experiencia de 30 años por ejemplo, pero sé, que el mundo se ha movido mucho más rápido en estos 20 años del siglo XXI que lo que pudo moverse en tres décadas del siglo XX, por lo que me presto, desde una humilde pero arraigada visión, a hacer una defensa del acto educativo, ese que busca el conocimiento sea por los medios que sean. 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Es casi una obviedad afirmar que la presencialidad es irremplazable. Es necesario apelar a la sensatez y afirmar que no son “sustitutas”, sino que son dos formas diferentes de acercarse a lo que moviliza por antonomasia toda acción educativa: **el conocimiento. **

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Los debates sobre qué tipo de conocimiento es el que queremos adquirir, por supuesto son necesarios, pero, si nos damos cuenta, para dar un debate (serio) esto implica conocimiento. Es decir, no podemos tan siquiera debatir seriamente sin conocimientos académicos. Y con todo respeto: no necesito aprobación de youtubers para constatarlo. 

<!-- /wp:paragraph -->

<!-- wp:heading {"level":5} -->

##### En ese orden, la situación de una educación desde el confinamiento se fue asumiendo.

<!-- /wp:heading -->

<!-- wp:paragraph -->

El “encierro” se constituyó en una nueva forma de vida, mientras por la ventana y detrás de la ventana, el cadáver de la normalidad se descomponía aceleradamente ante nuestros ojos. 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Surgieron entonces en el campo educativo dos figuras emblemáticas, que sabíamos que existían, pero no estábamos dispuestos a revelar el daño de sus mascaradas.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Con la virtualidad avanzando no como opción, sino como “la única forma”, se develaron gran cantidad de personajes que cumplían dos roles en el campo educativo:

<!-- /wp:paragraph -->

<!-- wp:list -->

-   El primero, profesores que se dedicaban a no planear clases, a llegar y contar sus experiencias personales, creyendo que su sola presencia era símbolo de aprendizaje… en otras palabras, los que se dedicaban a echar cháchara.
-   El segundo, estudiantes que suponían que, con su presencia en clase, ya se suponían como asistentes a una clase; que no leían mucho salvo aquello que los alejara de Tánatos del proceso, lento muy lento de adquirir conocimiento, y que venían pasando semestres “abre fácil” con esfuerzos dosificados por la ventaja de la mascarada, es decir, entrando a clase, sí, pero a calentar puesto.  

<!-- /wp:list -->

<!-- wp:heading {"level":5} -->

##### Ambos roles jugaban a ser un Sísifo que engañaba esta vez no a la muerte sino a la normalidad.

<!-- /wp:heading -->

<!-- wp:heading {"level":6} -->

###### [Le puede interesar: Eduación virtual por Covid-19, una decisión para la que no estamos preparados](https://archivo.contagioradio.com/eduacion-virtual-por-covid-19-una-decision-para-la-que-no-estamos-preparados/)

<!-- /wp:heading -->

<!-- wp:paragraph -->

Pero ahora, con la normalidad muerta y el grado de responsabilidad que exige la educación virtual, sin duda, los ha mantenido cargando pesadas rocas, para volver a cargarlas una y otra vez, esperando por lo menos el fin del semestre. 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Asimismo, muchos estudiantes sufrieron decepciones con buenos profesores presenciales que no lo siguieron siendo en la virtualidad. Por su parte muchos docentes, recibieron buenas sorpresas con estudiantes que se afianzaron a niveles de responsabilidad que no portaban en la presencialidad. También emergió la paradoja del silencio, un amiguito tan común en medio de las clases virtuales universitarias, pero tan esquivo en medio de las clases virtuales de primaria. 

<!-- /wp:paragraph -->

<!-- wp:heading {"level":4} -->

#### Pero ¿todo este pequeño panorama qué nos deja? 

<!-- /wp:heading -->

<!-- wp:paragraph -->

La educación es un acto que nos permite acercarnos al conocimiento. Es más, si no nos gusta esta definición hay como 100 más que podríamos agotar, es solo que en todas y cada una de ellas, acercarse al conocimiento también implica un grado de voluntad muy alto. 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

La virtualidad, por principio, tiene una característica intrínseca que emergió como “problemática” para una cultura colombiana: **la puntualidad. **

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El reloj en la virtualidad avanza al mismo paso que en la presencialidad, es solo que, en la virtualidad, el hecho de no haber interacción verbal (presencial) o simbólica (presencial), hace que los actos entre las personas aguarden vacíos por tratarse de un accionalismo que se manifiesta frente a un computador o un celular (ambos objetos sin vida). Esos vacíos no pueden ser ocupados por nada, por lo que el tiempo parece mayor, y desesperadamente buscamos que no haya silencio sino comunicación, o que los niños y niñas se callen para poder ofrecer las pautas de la clase. 

<!-- /wp:paragraph -->

<!-- wp:heading {"level":5} -->

##### Pero el problema que lo asimilo como más grave, tiene que ver con el descubrimiento del poco amor que tenían los que hablaban cháchara y los calienta puestos por el conocimiento.

<!-- /wp:heading -->

<!-- wp:paragraph -->

Tanta desidia por el acto educativo virtual me hizo pensar en todo lo que tienen que pasar por ejemplo tantas personas (jóvenes o viejas) que hacen esfuerzos titánicos para educarse. Pensé en los que les toca escuchar radio para educarse, en los que se han cultivado a punta de Google o Wikipedia o quienes ven conferencias en Internet para saber un poco más, para comprender un poco más, para sentirse atravesados por todo lo que el universo tiene por mostrarnos.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Pensé también en los muchos estudiantes que han pasado por mis aulas y que trabajan a la vez que estudian, recordé sus historias de esfuerzos personales no solo por lograr un diploma, sino por cultivarse, por adquirir conocimientos que les permitieran ser alguien diferente en este mundo que padece de una pobreza intelectual tan alta; estudiantes que se toman el trabajo de cumplir la tarea de ser cualquiera en este mundo, porque ser cualquiera en este mundo, como dijo Sartre, es también una tarea. 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Esa falta de amor por el conocimiento no es un reproche que pueda caer tranquilamente sobre “un individuo”. No. Es un problema estructural derivado de esa ideología neoliberal que se presentó como “anti-ideologías” y que hizo estragos en el propósito real de todas las cosas.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":5} -->

##### Es un problema estructural derivado del consenso de Washington, de la declaración de Bolonia cuando se definió y se preparó ese cambio paradigmático tan terrible que constituyó pasar de entender la educación como un derecho, a entenderla como un servicio. 

<!-- /wp:heading -->

<!-- wp:paragraph -->

Friedman y todos sus misioneros pueden irse para el carajo, pero sus ideas han quedado bien sembradas, y si queremos que la gente comprenda que la educación es ante todo un derecho que permite a las personas desarrollar el amor por el conocimiento, nos importará muy poco si la educación es virtual, presencial, a distancia o como sea, pues el ánimo de la educación como un compromiso inherente del sujeto con la sociedad en la que vive, brillará más alto que todas las estrellas. Ya lo decía José Ortega y Gasset “yo soy yo y mi circunstancia, y si no la salvo a ella no me salvo yo”. 

<!-- /wp:paragraph -->

<!-- wp:heading {"level":5} -->

##### La circunstancia de Ortega y Gasset somos todos, somos nosotros. La circunstancia en la educación no es si me gusta o no me gusta la cosa, sino que, si no salvamos el propósito de la educación, entonces ésta perderá sentido presencial, virtual o el que sea.  

<!-- /wp:heading -->

<!-- wp:paragraph -->

Por ahora, resistir debe comprenderse como algo más allá de tener esperanza…no olvidemos que la esperanza fue el único de los males que quedó preso en la caja de Pandora, y que el problema de solo dejarnos caer en manos de la esperanza, es que ella te permite actuar creyendo que puedes controlar el futuro… y eso… eso es vivir engañado. Podemos hacer más **¡sí que podemos! **

<!-- /wp:paragraph -->

<!-- wp:audio {"id":84175} -->

<figure class="wp-block-audio">
<audio controls src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/05/AUDIO-JOHAN-online-audio-converter.com_.mp3">
</audio>
  

<figcaption>
Escuche aquí la columna completa por Johan Mendoza

</figcaption>
</figure>
<!-- /wp:audio -->
