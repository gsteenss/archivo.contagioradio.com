Title: Se destapan nuevos nexos del uribismo con el paramilitarismo
Date: 2015-02-05 19:31
Author: CtgAdm
Category: Judicial, Nacional
Tags: Alvaro Uribe, Masacre del Aro, Masacre del Nilo, Paramilitarismo
Slug: se-destapan-nuevos-nexos-del-uribismo-con-el-paramilitarismo
Status: published

###### Foto: Pulzo 

Esta semana se hicieron efectivas varias ordenes de investigación y captura contra miembros del uribismo y funcionarios que **durante el gobierno anterior colaboraron con estructuras paramilitares.**

A la orden del Tribunal de Justicia Transicional de Medellín de compulsar copias en contra del **ex-presidente y ahora senador Alvaro Uribe** por su acción u omisión en la **masacre de El Aro**, en 1997 mientras era gobernador de Antioquia, se suma ahora la medida de aseguramiento en contra del **ex-congresista Julio Manzur por ser colaborador de las Autodefensas Unidas de Colombia**, quien fuera salpicado, al igual que el expresidente Uribe, por el ex-comandante paramilitar Salvatore Mancuso Gómez.

Esta semana se conoció también que el General en retiro, **Fabio Castañeda**, condenado por su responsabilidad y participación en la **masacre de El Nilo**, municipio de Caloto, Cauca, se entregó a la justicia. A pesar de que la masacre se cometió en 1991 responsabilizando a los paramilitares por su ejecución, sólo hasta agosto del 2014 **Sala Penal de la Corte Suprema de Justicia** reabrió el proceso vinculando a miembros de la fuerza pública que realizaban acciones en ese departamento.

Para completar el expediente de la semana, el miércoles 4 de febrero se conoció la medida de aseguramiento contra el **Coronel en retiro Alejandro Robayo Rodriguez**, quien actualmente se encuentra prófugo de la justicia, por ordenar la realización ejecuciones extrajudiciales en el año 2006 mientras pertenecía en el Gaula de Valledupar.

La colaboración de las fuerzas militares con estructuras paramilitares en Colombia, así como su promoción desde escenarios político-administrativos durante la gobernación y posterior presidencia Alvaro Uribe Vélez toman cada día más fuerza en las investigaciones que se adelantan en el país.
