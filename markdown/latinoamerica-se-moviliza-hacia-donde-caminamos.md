Title: Latinoamérica, se moviliza ¿hacia dónde caminamos?
Date: 2020-12-24 18:27
Author: CtgAdm
Category: Actualidad, El mundo
Tags: Argentina, Bolivia, Chile, Movilización social
Slug: latinoamerica-se-moviliza-hacia-donde-caminamos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/12/800px-Protestas_en_Chile_20191022_06.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: movilizacion social en Latinoamérica

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Para concluir el 2020 expertos de Bolivia, Chile y Ecuador analizaron los sucesos que ocurrieron en su país en medio de la pandemia y los contextos que han llevado a la ciudadanía de Latinoamérica a movilizarse contra un Gobierno que ha usado la pandemia como una forma de impedir la protesta social y sus reivindicaciones.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Ecuador, negligencia estatal en medio de la pandemia

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

El profesor, periodista y exvicecanciller, [Kintto Lucas](https://twitter.com/KinttoLucas) señala que a lo largo del 2020, en Ecuador se extendió una crisis económica, política y social que venía de tiempo atrás y que tuvo que ver con una austeridad extrema en medio de un ajuste fiscal que afectó a la clase media pero no cobró impuesto por más de 5.000 millones que debían determinados sectores empresariales y banqueros; una crisis que se acentuó con la llegada de la Covid-19.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Tal es la crisis que el Fondo Monetario Internacional (FMI) destinó un desembolso de 2.000 millones de dólares para programas económicos y sociales ante la crisis, convirtiendose en el segundo desembolso de un crédito de 6.500 millones de dólares otorgado en septiembre al Ecuador.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Agrega que la crisis sanitaria **es producto de "la negligencia en el tratamiento de la propia pandemia" y** cuyas falencias datan incluso de meses ante cuando se empezaron a desmantelar hospitales y equipos médicos, despidiendo a los profesionales de la salud que fueron necesarios a la llegada de la Covid-19 y que estaba ausentes pues o fueron despedidos o los centros médicos estaban en vía de privatización.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

"Ya vimos que el exceso de muertos pasa los 45.000 personas, algo increíble para un país de 17 millones de habitantes (...) vimos a los muertos en las calles en una negligencia total por parte del gobierno", indica.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

En 2019, la Organización Sindical Única Nacional de Trabajadores del Ministerio de Salud Pública indicó que los despidos en el sector salud afectarían a cerca de 3.000 funcionarios a nivel nacional, entre médicos, enfermeras y administrativos.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Un año después, para marzo de 2020, mientras el Ministerio de Salud ecuatoriano anunciaba la contratación de 504 médicos, a escala nacional unos 300 profesionales que atendieron a pacientes Covid-19 fueron despedidos. De igual forma a finales de mayo, se anunció el despido de 2.279 trabajadores.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Al respecto, la institución afirma que se trató de empleos del área administrativa y no de la parte médica. Kintto Lucas advierte que estos mismos despidos se vieron en otras organizaciones del Estado lo que llevó no solo a que incrementara el desempleo sino también la informalidad en particular en los sectores populares.

<!-- /wp:paragraph -->

<!-- wp:quote -->

> “Tal vez si no se hubiera dado la pandemia, las movilizaciones hubieran seguido, la realidad es que la gente está con mucha decepción de lo que es el Gobierno y con mucha voluntad de movilizarse, pero hay un temor con tantas muertes”
>
> <cite>Kintto Lucas, periodista y escritor </cite>

<!-- /wp:quote -->

<!-- wp:paragraph {"align":"justify"} -->

Agrega que en medio de la pandemia ha comenzado una época preelectoral que considera, da esperanza a la personas que buscan un cambio en el binomio haciendo referencia al candidato Andrés Arauz, aspirante a la presidencia, y su compañero de fórmula, Carlos Rabascall, apoyados por el expresidente Rafael Correa, pese a ello advierte que el verdadero problema en la actualidad es que se busca retrasar las elecciones. [(Lea también: Ecuador de nuevo en las calles contra imposiciones de Lenin Moreno)](https://archivo.contagioradio.com/ecuador-de-nuevo-en-las-calles-contra-imposiciones-de-lenin-moreno/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

"El binomio progresista puede ganar en primera vuelta y como vieron que el candidato de la derecha no tienen chance decidieron tratar de aplazar las elecciones en contra vía de la constitución del Ecuador y si no se cumple eso, la elección podría ser desconocida después", explica el exvicecanciller que afirma que la población se está preparando para nuevas movilizaciones.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Concluye que en medio de la pandemia, el neoliberalismo en Latinoamérica y en general el mundo, comprendió que para aplicar su modelo, la única forma es "a sangre y fuego porque sino la gente se levanta y si no hubiera pandemia habría movilización" sentenciando que al igual que en muchos otros países **se ha utilizado la pandemia "no para hacer aislamiento o por bioseguridad sino para tratar de controlar  a la población y hacer un control policial".**

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Chile, el desafío de crear una nueva constitución en Latinoamérica

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

A propósito **Juan Carlos Pérez, docente e integrante de Defendamos la Paz Internacional** señala que la brutalidad policial es un problema transversal en Latinoamérica, "lamentablemente en Chile, el Gobierno optó por proteger a los policías en lugar de hacer procesos de investigación", lo que llevó a que muchos de los funcionarios de los carabineros continuaran en sus funciones, mientras al menos 352 personas a lo largo de la movilización de 2019, sufrieron heridas oculares, de ellas, 331 fueron por lesión o trauma y 21 por estallido o pérdida.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

El caso más conocido de brutalidad policial en Chile este 2020, ocurrió en octubre cuando se conoció que un  **carabinero habría arrojado a un joven de 16 años al río Mapocho, durante las manifestaciones que se desarrollaban en Santiago,** revelando según el docente, cómo se "siguen cometiendo excesos" que han llevado a abrir el debate de hacer una reforma profunda a las fuerzas policiales chilenas. [(Le puede interesar: Carabinero a prisión por arrojar a joven al río en Chile)](https://archivo.contagioradio.com/carabinero-a-prision-por-arrojar-a-joven-al-rio-en-chile/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

En medio de estos sucesos, Chile alcanzó un referendo constitucional tras un mes de protestas, un hecho histórico que para Juan Carlos Pérez, se logró gracias al estallido social marcando "un quiebre profundo en todos los ámbitos de la sociedad chilena".

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Por su parte, Jairo Cuarán-Collazos sociólogo e investigador, añadió que dichos cambios requirieron de convergencias en el orden de la sociedad civil y de la movilización social que situó a Chile "en el contexto de comprender las discusiones sociales y en un ejemplo en virtud de que es la única parte del mundo donde hay un neoliberalismo que se pudo implantar en su totalidad, a diferencia de otros lugares de Latinoamérica y del contiente que como acotaba el profesor Kintto Lucas, se opusieron a esa transformación en su totalidad.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Para lograr dichos cambios, Pérez señala que en Latinoamérica y en el caso de Colombia **es clave fortalecer los medios alternativos, permitir el flujo de información y participar lejos de la burbuja mediática**. A su vez, Cuarán- Collazos considera necesario, rodear la Constitución de 1991, fortalecer el ejercicio de la Corte Constitucional y el sistema integral de justicia.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

**"Algo por lo que debería seguir luchando la sociedad civil colombiana es seguir defendiendo la posibilidad de acceso a la justicia social"** afirma el sociólogo en contraste con Chile donde aún persisten grandes problemáticas en la defensa ecológica y en el reconocimiento de los pueblos originarios, afrodescendientes y la comunidad inmigrantes. [(Lea también: Chilenos dicen SÍ a una nueva constitución)](https://archivo.contagioradio.com/chilenos-dicen-si-a-una-nueva-constitucion/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Mientras expertos afirman que Chile será uno de los países que mejor saldrán parados de la pandemia, esto convierte al país en uno de los destinos más atractivos para los migrantes que buscan mejorar su calidad de vida. Cabe resaltar que **para 2019, según estimaciones, la cantidad de personas extranjeras residentes en el país austral bordeaba los 1,5 millones de personas.**

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

A propósito, el profesor Juan Carlos Pérez quien viene apoyando la Coordinadora Migrante, señala que el gobierno de Sebastián Piñera emuló políticas como la de sus homólogos Donald Trump y Mauricio Macri estigmatizando a la población migrante.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Algo preocupante si se tiene en cuenta que se está creando una nueva ley de migración y extranjería que si bien contempla algunos avances con relación a su predecesora, original de 1975 y creada en medio de la dictadura - **aumenta la burocracia en la solicitud de documentos, la injerencia de la Policía e incluye expulsiones desmedidas y transgresión de tratados internacionales**.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Con relación a los pueblos originarios, los académicos señalan que la desigualdad y desconocimiento de sus luchas y derechos persiste, lo que lleva a cuestionarse cómo se entiende la participación de los pueblos ancestrales en Chile. [(Lea también: La dictadura persiste contra el pueblo mapuche en Chile)](https://archivo.contagioradio.com/la-dictadura-persiste-contra-el-pueblo-mapuche-en-chile/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Para hacerse una idea, en la actualidad, aunque el proyecto de ley original planteaba otorgar de 25 a 25 escaños de la Asamblea Constituyente para comunidades indígenas, estos solo serán en total 17 de 155 pese a que representan un 9% de la población nacional. siendo el pueblo Mapuche el 84% de esta totalidad.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Bolivia, el desafío de retomar el Gobierno

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Mientras ese es el escenario con el que cierran el año países de Latinoamérica como Ecuador y Chile, Bolivia viene de celebrar las primeras elecciones presidenciales tras la salida del gobierno de Evo Morales el 10 de noviembre de 2019 denunciando que fue víctima de un golpe de Estado previa al gobierno transitorio de Jeanine Áñez calificado como un régimen de facto.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Las elecciones, otorgaron la victoria con más de 55% a Luis Arce quien fuera ministro de Economía de Evo Morales desde 2006 hasta el 2019. En su primer mes frente al Gobierno, Arce ha cumplido con entregar una ayuda estatal de 140 dólares a más de un tercio de la población en medio de la pandemia, instaurar un impuesto a las grandes fortunas y comenzar la investigación y sanción de la represión que se vivió durante el gobierno de Jeanine Áñez.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

**Adriana Guzmán, mujer del pueblo Aymara**, señala que "resistir al golpe ha sido muy difícil, fue un golpe de Estado que no esperábamos, pensábamos que era un proceso consolidado desde los pueblos originarios", expresa, agregando que se pensaba que el racismo nunca más iba a ser la forma de relacionamiento, sin embargo advierte que hoy, "el racismo es impune en las calles y durante el golpe se ha profundizado".

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

**"Llegamos a elecciones gracias a las protestas, no tiene que ver solo con Luis Arce, ni con el movimiento, es una decisión del pueblo de construir un país distinto, sin embargo recuperar el Gobierno no quiere decir que los terratenientes que han sostenido el golpe no sigan en el país, ellos siguen organizando formas de desestabilización, siguen financiando los grupos armados, paramilitares y lógicas racistas",** afirma la indígena quien señala que la polarización, que llegó a niveles muy altos en los días posteriores a la dimisión de Evo Morales, se mantiene presente.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

"Hoy queremos recuperar un proceso político en el que hemos planteado descolonización lo cual es imposible de hacer si los intereses o la mayor parte de riquezas en el país está en manos de las familias que han dado este golpe" afirma Adriana Guzmán.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Con relación a la pandemia, la mujer indígena denuncia que la cantidad de feminicidios y los índices de violencias en los hogares han aumentado con el aislamiento en toda Latinoamérica. **Según la Fiscalía General del Estado, han ocurrido 24.000 episodios de violencia física, psicológica, sexual, económica, y simbólica a lo largo del aislamiento incluyendo 125 feminicidios hasta septiembre de 2020.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

"Una de las cosas que hay que reconocer dentro de estos índices es la ausencia de los Estados, la cuarentena ha verificado la incapacidad de los Estados y la falta de voluntad que hace años denunciamos las mujeres en todos los territorios", concluye Adriana. [(Lea también: Mujeres e indígenas, tienen la clave para afrontar crisis del Capitalismo: R Zibechi)](https://archivo.contagioradio.com/es-momento-de-aprender-de-las-mujeres-y-los-pueblos-indigenas-raul-zibechi/)

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
