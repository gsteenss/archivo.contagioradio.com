Title: Una vela por la paz con justicia social en Colombia
Date: 2015-12-04 17:19
Category: Abilio, Opinion
Tags: enciende una vela por, luces de navidad, luces por la paz, paz en colombia
Slug: una-vela-por-la-paz-con-justicia-social-en-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/12/enciende-una-vela-por.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

#### **Ablio Peña - @abiliopena** 

###### [4 Dic 2015] 

Hoy  se  lanzó el lema **\#EnciendoUnaVelaPor**  “ el 7 de diciembre  desde  las 7:00 p.m” en el que se nos preguntan por qué encendemos ese día una vela.  Queremos decir las razones por las que la encendemos por una paz con justicia social.

La fecha es significativa por que conmemora la tradición cristiana  la primera navidad, entendida como  nacimiento. Más allá de las creencias religiosas,  tenemos la esperanza  de que los diálogos con las FARC-EP y ojalá la formalización de los diálogos con el ELN  generen el escenario  de parto, para el nacimiento de un país diferente,  donde la  injusticia social empiece a ser espantada.

Vale la pena retomar esta tradición cristiana  en la  aspiración de la paz con justicia, porque  en sus raíces más profundas  da cuenta de que la esperanza no es barata y que  no se equipara con el éxito promovido en   los discursos de la prosperidad y en este caso en los  de una paz pactada en un eventual acuerdo final, paso necesario, pero que no la agota.

Jesús mismo  es recordado en  los relatos de la infancia del Nuevo Testamento como pobre, forastero,  perseguido por el  poder de la época al punto de ser obligado a un desplazamiento forzado en Egipto. En el canto  de  María de la “Anunciación e inmaculada concepción” que conmemoramos el 8 de diciembre y desde el 7 con las velitas, Jesús  es hijo de una  historia  en la que Dios “dispersó  a los que son soberbios en su propio corazón, derribó a los potentados de sus tronos y exaltó a los humildes. A los hambrientos colmó de bienes y despidió a los ricos sin nada  ( Lucas 2, 51-53).  Y por los relatos de la muerte es recordado en clara contradicción con la imposición  del dios   emperador a la que opone el Reinado del Dios de la Justicia, al punto  de terminar ejecutado en una cruz por decisión de  ese poder romano  y de los gobernantes locales a su servicio.  Nada exitoso  final de Jesús.

En esa dirección entendemos  la visión de Erick Fomm, citada por el P. Javier Giraldo en su  escrito “Reconfigurar la Esperanza”  (<http://www.javiergiraldo.org/spip.php?article93> ):  “Tener esperanza significa estar presto  en todo momento para lo que todavía no nace, pero sin llegar a desesperarse si el nacimiento no  ocurre en el lapso de  nuestra vida. Carece así, de sentido, esperar lo que ya existe o lo que no puede ser. Aquellos  cuya esperanza es débil pugnan por la comodidad o por la violencia, mientras aquellos cuya esperanza es fuerte ven y fomentan todos los signos de la nueva vida y están preparados en todo momento para ayudar al advenimiento de lo que se halla en condición de nacer”.

La esperanza cristiana nos hace ver  este   momento de los diálogos para la paz como un signo diciente  del nacimiento progresivo de una sociedad diferente, que como dice From, quizás no lleguemos a ver  en varias generaciones, o quizás no se pueda concretar en toda la historia humana.      Mas  sí como la soñada apuesta  por la  distribución  equitativa de la tierra, por la soberanía alimentaria, la regulación efectiva de la inversión extranjera de tierras, el freno a la expoliación transnacional del suelo y del subsuelo,  de real participación política sin la amenaza paramilitar y militar que la imposibilita, con el desmonte de  la maquinaria electoral y del gobierno de los ricos, que hace de esta democracia solo una quimera. También  con capacidad productiva que haga más rentable a los campesinos producir alimentos que  hoja de coca,  con una justicia sin fueros para los criminales del poder y con un sistema financiero que no base sus ingresos en la usura.

Alcanzar la utopía evangélica en la  que los potentados dejen sus tronos y su modo de gobernar basado en la exclusión y se posicionen  los humildes como referentes de justicia, hace que en contexto de diálogos de paz pensemos en caminos audaces para alcanzar soluciones reales desde las víctimas que ni siquiera el precario derecho a logrado garantizar, tras la ausencia de voluntad política de los gobernantes. Se trata como lo hemos escrito en otros momentos del salto a la rebelión no violenta que nos han enseñado comunidades como las del Curvaradó, Jiguamiandó, Cacarica, San José de Apartadó, la Balsita de Dabeiba, La Perla Amazónica en Putumayo,  El Espacio Humanitario de Puente Nayero en Buenaventura, los indígneas Sikuane y algunas familias campesinas de Mapiripán, las comunidades del Alto Ariari, entre muchas otras. Ellas también en comunión con luchas no violentas globales como las del Movimiento por el Cierre de la Escuela de las Américas,   las de Monseñor Romero, las de Martin Luther King y la de Gandi, que muestran las posibilidades reales de conquistas democráticas y ambientales para los pueblos.

Como dice From, la apuesta por concretar esos valores requiere en este momento de diálogos de paz estar preparados para  su advenimiento,  sabiendo que requiere esfuerzo, implica riesgos y quizás hasta la incómoda persecución de los que se conforman con una paz entendida solo como el silenciamiento de fusiles.

Por eso encendemos la vela por la paz con justicia social este 7 de diciembre a las 7 p.m. Imagen de campaña [\#EnciendoUnaVelaPor ](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/12/peque.png)
