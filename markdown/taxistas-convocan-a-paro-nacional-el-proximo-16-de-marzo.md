Title: Taxistas convocan a paro nacional el próximo 16 de Marzo
Date: 2015-03-12 21:35
Author: CtgAdm
Category: Movilización, Nacional
Tags: derecho al trabajo, formalización laboral, Paro nacional de taxistas, Taxistas
Slug: taxistas-convocan-a-paro-nacional-el-proximo-16-de-marzo
Status: published

###### Foto: vanguardia.com 

<iframe src="http://www.ivoox.com/player_ek_4207190_2_1.html?data=lZedmZaddI6ZmKialJWJd6Kok5KSmaiRdo6ZmKiakpKJe6ShkZKSmaiRmMLsytjhw9iPtsbVzc7nw9eJh5SZopbbjdXFttCfz8bQy9TSpc2fxtGY0teJh5SZo5jly9LTb83pz8rgjZKJe6ShpNTb1sbLrdCfs8bRy9SPcYarpJKh&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Alonso Romero] 

Precios de los combustibles, rutas piratas y auge de aplicaciones móviles para servicios de transporte individual de pasajeros son algunos de los temas por los cuales el gremio de los taxistas en Colombia realizará la jornada de paro nacional el próximo 16 de Marzo. No obstante durante los próximos días esperan realizar acercamientos con el gobierno nacional para plantear sus exigencias.

Alonso Romero, líder de la Asociación Colombiana de Taxistas, afirma que aunque hay muchos problemas que se presentan actualmente con la prestación del servicio a los pasajeros que lo solicitan, las empresas y los propios taxistas están trabajando para solucionarlos. Sin embargo, lo problemas y las denuncias deberán ser atendidas por el gobierno.

[![convocatoria-paro-taxistas-contagio-radio](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/03/convocatoria-paro-taxistas-contagio-radio-300x225.jpg){.wp-image-5912 .alignleft width="339" height="254"}](https://archivo.contagioradio.com/actualidad/taxistas-convocan-a-paro-nacional-el-proximo-16-de-marzo/attachment/convocatoria-paro-taxistas-contagio-radio/)

Según el comunicado de citación otra de las grandes reivindicaciones de los taxistas es la reglamentación del decreto 1047, que tiene que ver con la formalización laboral, capacitación y la elaboración de un registro único de conductores.

Romero afirma que de no llegarse a un acuerdo en los acercamientos con el gobierno el Paro Nacional iniciará el 16 de Marzo a las 5 am. En la convocatoria se encuentras organizaciones tanto distritales en Bogotá, como agremiaciones a nivel nacional puesto que las problemáticas son las mismas en todo el país.
