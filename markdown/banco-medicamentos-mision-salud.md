Title: Banco de Medicamentos de Misión Salud: Una obra social admirable
Date: 2017-03-28 09:09
Category: Mision Salud, Opinion
Tags: Banco de medicamentos, colombia, Misión Salud, sistema de salud
Slug: banco-medicamentos-mision-salud
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/03/EO6OTITAABADTGN33BUSGNZA7U.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: La Nación 

##### Por: [María del Rosario Gómez @MisiónSaludCo](https://archivo.contagioradio.com/opinion/mision-salud/) 

###### 28 Mar 2017 

La falta de acceso a medicamentos existentes es el mayor problema de salud pública que enfrenta la humanidad, toda vez que es responsable de la muerte de 10 millones de personas anuales a nivel global. Infortunadamente Colombia no es excepción. La insuficiente cobertura del sistema de salud, sumada a los precios escandalosos de los medicamentos monopólicos y a los altos índices de pobreza, genera una creciente demanda insatisfecha de estos bienes esenciales, con los efectos consiguientes en términos de enfermedad, sufrimiento y pérdida de vidas humanas.

Para contribuir a la solución de este drama humano en lo que respecta a la población en situación de extrema pobreza y vulnerabilidad, hace 14 años la Fundación Misión Salud, una ONG privada y sin fines de lucro, fundada por organizaciones de la sociedad civil y de la Iglesia Católica, creó el Banco de Medicamentos.

Su finalidad es recibir de los laboratorios medicamentos e insumos necesarios para la salud y la nutrición, siguiendo las directrices sobre donaciones de la Organización Mundial de la Salud (OMS), y ponerlos a disposición de instituciones dedicadas al cuidado de enfermos muy pobres y vulnerables, especialmente niños, adultos mayores y discapacitados.

En sus momentos de mayor penetración el Banco de Medicamentos ha llegado a beneficiar a 45.000 personas de estas características, garantizando en todo momento la vigencia y calidad de los productos entregados, la trazabilidad plena de su destino y su uso legítimo y racional. De ellas, casi 20.000 han sido beneficiadas de manera permanente a través de más de 100 instituciones que las albergan, como hogares geriátricos, centros de discapacidad y hogares de protección, entre otras, y el resto en forma esporádica a través de brigadas de salud que cubren lugares apartados y de difícil acceso del territorio nacional, en cooperación con entidades especializadas del sector salud.

Además del suministro de medicamentos y otros bienes de la salud, el Banco brinda a las instituciones beneficiarias asesoría en diferentes campos, como el adecuado almacenamiento y uso racional de los medicamentos, la evaluación del ciclo de suministro de los mismos y el mejor aprovechamiento del Sistema de Salud.

El Banco no atiende a personas naturales sino a instituciones y únicamente recibe medicamentos previamente solicitados por éstas. De esta forma, y a través de diversos controles, se evita su vencimiento y se asegura su uso racional.

17 laboratorios han hecho posible esta admirable obra social a través de los años, siendo de destacar los 5 laboratorios fundadores que han tenido presencia permanente: Bussié, La Santé, Procaps, Tecnoquímicas y Vitalis. Ellos han hecho del Banco un programa modelo de Responsabilidad Social Empresarial (RSE). Un programa, además, único en el mundo, ya que cubre todas las enfermedades. Los demás que existen en algunos países están dedicados a una enfermedad de alto costo, como VIH-Sida y cáncer.

La salud es un derecho fundamental de todo ser humano. El Banco de Medicamentos es una demostración palpable de que entre todos podemos convertir este postulado en realidad para la población más débil y necesitada del país.
