Title: ELN plantea la posibilidad de extender cese al fuego bilateral en 2018
Date: 2017-12-04 18:16
Category: Entrevistas, Paz
Tags: ELN, Silvana Guerrero
Slug: eln-plantea-la-posibilidad-de-extender-cese-al-fuego-bilateral-en-2018
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/12/resumen-latinoamericano-eln.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:Resumen Latinoamericano] 

###### [04 Dic 2017] 

En entrevista con Contagio Radio, Silvana Guerrero, integrante de la guerrilla del ELN y de la Mesa de Diálogos de Quito manifestó que estos primeros 4 ciclos de conversaciones han tenido un balance muy positivo en términos de participación social, **además afirmó que se está dialogando sobre la posibilidad de que se extienda el cese bilateral al fuego**, luego de la evaluación que se realizará posteriormente al 9 de enero.

**Contagio:** **¿Qué han dejado las audiencias preparatorias y se ha cumplido el objetivo?**

**Silvana Guerrero:** Para nosotros hasta ahora ha sido un balance muy positivo como delegación y organización, teniendo en cuenta que el ELN dentro de sus objetivos y prioridades, esta reconocer los aportes, insumos y propuestas de todo el pueblo colombiano para este primer punto de la agenda que es la participación. (Le puede interesar: ["Finalizó el cuarto ciclo de conversaciones entre el ELN y Gobierno"](https://archivo.contagioradio.com/finalizo-cuarto-ciclo-de-conversaciones-con-balance-positivo-entre-eln-y-gobierno/))

El ejercicio de audiencias preparatorias nos ha dejado, hasta el momento insumos que tendremos en cuenta dentro del documento que haremos como organización y que será el complemento del diseño de la participación, participaron alrededor de 230 organizaciones que, a su vez, representan un sin número de colectivos y trabajo de base en sus territorios.

**C: ¿Cuándo estaría listo el mecanismo de participación de la sociedad civil en la mesa de Quito, ¿Ecuador y considera que las campañas electorales, que se están desarrollando, podrían incidir en este mecanismo?**

S.G: Es difícil prever que en el ciclo que viene, el quinto, podamos avanzar positivamente en tener más que el diseño de la participación, un arranque en la práctica del mecanismo en todo el territorio colombiano.

Sería muy importante avanzar allí para darle fuerza al proceso independientemente de quien llegue al gobierno. Así se asumiría un compromiso con el proceso por la paz de Colombia, sin embargo, los métodos y las realidades en la mesa no son favorables en algunos momentos, cuando por ejemplo el gobierno habla de desacelerar el proceso, y es **evidente su ánimo por dilatar frente a temas en los que nosotros queremos avanzar,** entonces todo dependerá de cómo avanza el quinto ciclo, de cuáles son nuestras prioridades, pero también las del gobierno.

**C: ¿Cuáles fueron las líneas que marcaron el debate en esas audiencias preparatorias?**

S.G: Las líneas fuertes que tiene visionadas el ELN, fueron ratificadas en las audiencias cuando desde las organizaciones que hacen parte de la institucionalidad, organismos internacionales y organizaciones sociales, dicen que la participación debe ser desde lo territorio, que la participación debe ir de lo local a lo regional y de lo regional a lo nacional.

También llegamos a consensos sobre que la participación debe ser amplia y diversa, debe estar presente el enfoque de género y diferencial y ahora toman mayor fuerza porque es el pueblo el que está pensando de la misma manera como lo hemos venido haciendo nosotros. (Le puede interesar: ["ELN debe generar credibilidad y reforzar su unidad interna: Carlos Velandia"](https://archivo.contagioradio.com/eln-asesinato-indigena/))

**C: Han pasado 4 ciclos desde el inicio del proceso de paz ¿cómo ven la continuidad del cese bilateral?**

S.G: El pueblo colombiano y la comunidad internacional conocen los acuerdos del cese bilateral que va hasta el 9 de enero, en seguida vendrá una evaluación de cómo trascurrió este ejercicio, independientemente venimos haciendo un informativo a la mesa, con presencia de garantes, en donde **vamos contemplando las posibilidades de hacer la prologa, pero esto no está en el acuerdo inicial del cese**.

Hemos recibido la propuesta de que este cese al fuego se prolongue porque serían una manera de darle mayores garantías de seguridad al ejercicio de la participación, sobretodo en el inicio delquinto ciclo de conversaciones.

**C: ¿Qué opinan del Movimiento Social y las divisiones que se podrían evidenciar, actualmente, con las movilizaciones que se ven en todo el país?**

S.G: Para nosotros lo importante dentro de la diversidad y realidad misma es que, este ejercicio del proceso de dialogo por la paz de Colombia, independientemente que sea con el ELN o con los Acuerdos de paz con las FARC, involucre a todo el pueblo colombiano que debe comprometerse y sumarse en un ejercicio de unidad para que esto se materialice en bien de todos los colombianos y no de las organizaciones.

El proceso no busca beneficios u objetivos individuales para las organizaciones sino para todos, dentro de esa diversidad que conocemos y **lo que ahora requiere la realidad es la unidad y la confluencia de todos para empujar la paz**.

**C: ¿Con quienes se podría contar en el gobierno para defender el proceso actual de diálogos, teniendo en cuenta la coyuntura que se está dando en la implementación de los Acuerdos de paz?**

S.G: No hay duda de que es con el pueblo organizado, unido, frente a una misma bandera que hoy puede ser posible la paz de Colombia, no hay otra herramienta o mecanismo.

**C: En esa medida, ¿tienen ustedes la confianza en el respaldo social y popular frente al proceso de paz en Quito?**

S.G: La unidad de todo el pueblo colombiano y de las organizaciones serán las que marquen una pauta muy importante y va a ser la que logre sentar al gobierno y comprometerlo con el cumplimiento de los acuerdos que salgan del mismo.

Las organizaciones han llevado muchas luchas, sin embargo, se quedan en el papel, este proceso no puede quedarse en el papel y eso solo se podrá resolver con la participación activa de quienes queremos la paz. (Le puede interesar: ["ELN y FARC le están apostando a la paz: Víctor de Currea"](https://archivo.contagioradio.com/eln-y-farc-quito/))

**C: Con estos avances, ¿cuál es entonces la invitación que el ELN le hace la sociedad civil en términos de participación y construcción de paz?**

S.G:  La invitación son muchas, pero en este momento es importante, aparte de agradecer todo el ejercicio, es el llamado a continuar con el trabajo de pedagogía y de comunicación, muy preciso y real, sobre cómo va el proceso, **porque esto implica mayores compromisos de aquellas personas que no están interesadas por esta realidad del país**.

<iframe id="audio_22358512" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_22358512_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
