Title: 400 federaciones y comunidades se movilizan en defensa de la Amazonía
Date: 2017-09-21 18:17
Category: Voces de la Tierra
Tags: amazonas, ambientalistas asesinados, Movilización
Slug: movilizacion_amazonia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/amazonas-e1478294687660.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Archivo particular 

###### [21 Sep 2017] 

Ante el peligro inminente en el que se encuentra la Amazonía, porque 12% de su territorio ha sido transformado, cerca de 400 federaciones nacionales y miles de comunidades que integran la Coordinadora de Organizaciones Indígenas de la Cuenca Amazónica (COICA), han decidido llevar a cabo la Primera Movilización Amazónica este 22 de septiembre.

Se trata de una jornada que reivindica la defensa de la selva amazónica en la que se ha destruido el equivalente a **5 millones de canchas de fútbol, hablando solo del territorio colombiano, como lo concluye un informe publicado por **el Instituto Sinchi y Parques Nacionales.

"La amenaza de exterminio como pueblos y de destrucción material de la mayor reserva forestal y acuífera del planeta", es la situación que atraviesan dichas comunidades que han convocado la movilización que tiene como lema "Amazonía, humanidad segura".

### **Las reivindicaciones de COICA** 

De acuerdo con COICA, de continuar con el ritmo actual de depredación, "en 2030 se llegará al punto de no retorno que hace irrecuperable esta reserva y su función ecológica planetaria".

La marcha busca denunciar y homenajear las vidas de las personas que han sido asesinadas por defender la Amazonía. De hecho en las últimas semanas **al menos 20 indígenas amazónicos de Brasil, cercanos la frontera peruana, han sido asesinados por mineros que buscan oro.** Frente a estas situaciones, con la marcha buscan que se gestione una Misión internacional de emergencia de la ONU a la Amazonía.

Esta es la primera de un total de siete movilizaciones a las que se suman demandas específicas nacionales en los diferentes países que hacen parte de la región Amazónica.

### Las movilizaciones 

CIDOB/ Bolivia:  TIPNIS vigilia, marcha santa cruz, bicicleteada  
OPIAC /Colombia: «Fast Track», marchas en Bogotá, Letivia, Mocoa  
FOAG /Guyana Francesa: Mina Oro, Macron, Marcha Cayenne  
CONFENIAE/ Ecuador: Foro y Feria en Quito  
ORPIA/ Venezuela: Foros en Puerto Ayacucho y Lago Maracaibo  
COIAB, APA, OIS (Guyanas y Surinam) por definir  
PERÚ, 6pm. frente a las oficinas de PetroPerú (Esquina Av. Paseo de la república con Av. Canaval y Moreira) en San Isidro  
<iframe id="audio_21037755" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_21037755_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
