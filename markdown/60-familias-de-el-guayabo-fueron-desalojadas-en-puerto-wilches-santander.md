Title: Sin techo pasarán la navidad 60 familias desalojadas del Guayabo en Santander
Date: 2017-12-18 16:11
Category: DDHH, Nacional
Tags: comunidad el guayabo, Desalojos, despojo de tierras, el guayabo, paramilitares
Slug: 60-familias-de-el-guayabo-fueron-desalojadas-en-puerto-wilches-santander
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/12/Guayabo-banner-1-e1513621370608.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: ECAP Colombia] 

###### [18 Dic 2017] 

Cerca de 120 familias viven en la comunidad del Guayabo en Puerto Wilches, Santander. De estas, **60 han denunciado que han sido desalojadas de manera arbitraria** de los predios San Felipe y Altamira donde por más de 30 años llevan realizando actividades de agricultura para su sostenimiento. Debido a una solicitud de desalojo, el pasado 13 de diciembre, el ESMAD sacó a la fuerza a estas personas y destruyeron sus viviendas.

Desde 1986, campesinos de esta comunidad han ocupado **400 hectáreas sobre los que ejercen el derecho de posesión** aun cuando no cuentan con los títulos de propiedad. Por esto, han adelantado un proceso de defensa de los derechos sobre el territorio y las condiciones dignas de vida que se han visto amenazados por los intereses de terratenientes y proyectos de desarrollo.

Tras haber realizado varias solicitudes, en 2011, la comunidad del Guayabo adelantó un proceso jurídico de prescripción adquisitiva del dominio para que **se les reconociera el derecho de posesión** y titulación. En la primera instancia del proceso, les fue reconocido pero en 2012 se presentó el señor Rodrigo López Henao como supuesto propietario de las tierras. (Le puede interesar: ["En el día internacional de los pueblos indígenas ordenan desalojo de un resguardo"](https://archivo.contagioradio.com/en-el-dia-internacional-de-los-pueblos-indigenas-ordenan-desalojo-de-un-resguardo/))

López Henao, había señalado que los miembros de la comunidad del Guayabo le habían causado un desplazamiento a él y a su familia y los calificó como **integrantes de grupos armados al margen de la ley**. La comunidad en varias oportunidades manifestó que nunca habían visto al señor López Henao en el territorio y este último, ha adelantado diferentes acciones jurídicas para despojar a la comunidad de sus tierras.

### **López Henao habría ingresado a la zona con un grupo paramilitar** 

De acuerdo con Dimax Ospino, representante legal de ASOMIGUD, “la comunidad del Guayabo somos víctima del conflicto armado y al señor que viene a reclamar la tierra no lo conocemos, lo vimos cuando en 2012 **entró con las autodefensas a la comunidad**”. Cuando esto ocurrió, a la comunidad “nos tocó pagar vacuna para usar las tierras”.

Además, denunció que los paramilitares **“montaron alcaldes y concejales”** por lo que las instituciones no han garantizado los derechos de los campesinos y ya han sido desalojados de manera irregular del territorio en 2014. Sin embargo, en un video, las comunidades denunciaron hace pocos días las arbitrariedades en los procesos de desalojo. (Le puede interesar: ["Desalojo del Bronx solo favoreció a empresas privadas"](https://archivo.contagioradio.com/a-un-ano-de-la-retoma-del-bronx-se-evidencia-la-improvisacion-del-desalojo/))

Finalmente, hay que recordar que tanto el Delegado para Asuntos Agrarios y Restitución de Tierras y el Defensor Delegado para Asuntos Agrarios y de Tierras, habían solicitado en junio del presente año que se **verificaran los procesos de desalojo** de la comunidad del Guayabo debido a que se hizo un desalojo sin tener en cuenta el debido proceso y se vulneraron los derechos de los campesinos.

La comunidad del Guayabo se encuentra desplazandose hacia Puerto Wilches para realizar un plantón frente a la alcaldía. Allí, le pedirán al alcalde del municipio que les de las garantías necesarias para su seguridad y su retorno.

<iframe id="audio_22719075" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_22719075_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio]{.s1}](http://bit.ly/1ICYhVU)[.]{.s1}
