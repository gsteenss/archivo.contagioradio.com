Title: Javier Darío Restrepo: El periodista y los medios de información en tiempos de paz
Date: 2016-09-29 12:39
Category: Entrevistas, Paz
Tags: periodismo por la paz, Periodistas
Slug: javier-dario-restrepo-el-periodista-y-los-medios-en-tiempos-de-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/09/javier-dario-restrepo.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Contagio Radio 

###### 29 Sep 2016 

Uno de los referentes éticos del periodismo en Colombia es el Maestro **Javier Darío Restrepo,** a quien invitamos a conversar sobre el papel que periodistas y medios de información deben asumir en momentos en que se busca alcanzar la paz y los retos que implica la construcción de nuevos escenarios comunicativos.

Los y las invitamos a ver esta nueva emisión de nuestra serie de conversaciones de cara a la construcción de la paz.
