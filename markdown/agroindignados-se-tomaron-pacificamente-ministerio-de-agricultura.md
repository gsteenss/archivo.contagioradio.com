Title: Agroindignados se tomaron pacíficamente Ministerio de Agricultura
Date: 2015-09-01 16:14
Category: Movilización, Nacional
Tags: 2013, Aurelio Iragorri, Cumbre Agraria, Ministerio de Agricultura, Paro Agrario, toma del ministerio de Agricultura
Slug: agroindignados-se-tomaron-pacificamente-ministerio-de-agricultura
Status: published

###### Foto: Contagio Radio 

Desde tempranas horas de la mañana, algunos de los integrantes que hacen parte de la Cumbre Agraria, decidieron tomarse pacíficamente el Ministerio de Agricultura, con el fin de dialogar con el Ministro Aurelio Iragorri sobre los incumplimientos del gobierno nacional con el campo colombiano.

Pese a que la toma ha sido completamente pacífica, el plantón de la Cumbre frente al ministerio ha sido rodeada por agentes del ESMAD y tres carro tanques que amenazan con desalojar a los campesinos, negros e indígenas. Sin embargo, el ministro se comprometió a no permitir que los policías agredan a las personas que continuarán con esa acción de resistencia en el trascurso de la tarde.

A esta hora voceros de la Cumbre Agraria mantienen una reunión con el ministro de agricultura, esperando respuestas contundentes del gobierno, ya que se ha afirmado que la jornada de movilización en Bogotá continuará hasta que el gobierno cumpla lo ya pactado durante el paro agrario del 2013.

Por su parte, los agroindiganados han rechazado los señalamientos en diferentes medios de comunicación empresariales donde se ha asegurado que son “encapuchados” o "vándalos", estigmatizando esta acción pacífica.

Noticia en desarrollo…  
 
