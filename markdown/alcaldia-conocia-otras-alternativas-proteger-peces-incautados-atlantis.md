Title: Alcaldía conocía otras alternativas para proteger peces incautados en Atlantis
Date: 2017-01-11 18:59
Category: Animales, Voces de la Tierra
Tags: Alcadía de Bogotá, atlantis plaza, Enrique Peñalosa, Peces
Slug: alcaldia-conocia-otras-alternativas-proteger-peces-incautados-atlantis
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/01/peces-atlantis.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Pulzo 

###### [11 Ene 2017] 

El pasado 2 de enero, la Alcaldía de Bogotá anunció con bombos y platillos que procedería la incautación de 40 peces, entre ellos un tiburón nocturno que estaban siendo expuestos de manera inapropiada y además de forma ilegal en el centro comercial Atlantis Plaza. Sin embargo, **tras su decomiso, en menos de 24 horas la Secretaría de Ambiente tomó la decisión de sacrificarlos,** sin si quiera analizar las alternativas que daban voceros de organizaciones expertas en el manejo de este tipo de fauna como la Fundación Leucas, o el Movimiento Ambientalista Colombiano.

**La alcaldía solo mostraba dos alternativas: el sacrificio o liberarlos en aguas colombianas.** Lo que no se había dejado claro es que, de acuerdo con esas organizaciones protectoras de fauna silvestre, claramente existían otro tipo de opciones que incluso ya se habían tramitado.

**“La misma legislación colombiana dice que los peces pueden ser trasladados** hacia un lugar de investigaciones donde puedan estar bien cuidados, sin necesidad de que sean sacrificados o sean regresados al mar", explica Andre Matiz, de la Fundación Leucas.

Matiz, cuenta que ya se había hablado con un acuario en Santa Marta donde serían recibidos los peces, y además se había organizado con la propia Policía Nacional el traslado de los animales en condiciones óptimas. Una posibilidad que en menos de 24 horas tras la incautación, fue presentada al distrito. Sin embargo, este procedió a sacrificarlos, **atendiendo un concepto de la entidad Conservación Internacional,** donde trabaja Renata Peñalosa, hija del alcalde Enrique Peñalosa.

Tanto el vocero de la Fundación Leucas como Camilo Prieto, director del Movimiento Ambientalista Colombiano, aseguran que esa recomendación la única entidad que la dio fue Conservación Internacional. Lo más grave es que **el concepto se da explícitamente por los elevados costos que implicaba trasladarlos hasta Asia,** lugar de origen de los peces.

Entre las especies sacrificadas había Cirujanos, Payasos, Cardenal, Mandarín o Dragón (procedente de Indonesia), así como Camarones limpiadores, corales, anémonas, cangrejos y una estrella de mar, entre otros.“No sabemos cuál era el afán de incautarlos, si los iban a matar”, expresa Matiz.

### **Entidades ambientales no están preparadas** 

Andre Matiz, explica que este tipo de respuestas por parte de los organismos encargados de proteger la vida de la fauna son más comunes de lo que parecen, generando temor en la sociedad al momento de denunciar, pues ahora muchos preferirán no hacerlo pensado en que se opte por el sacrificio de los animales que se pretenden salvar.

Esta situación no sucede por primera vez. De acuerdo con el biólogo, **en el año 2012 un cocodrilo que llegó a la playa Sound Bay de San Andrés fue asesinado** “a balazos” por la entidad de la región encargada de manejar la situación.

Asimismo, **el hipopótamo Pepe fue sacrificado en el año 2009**, por decisión de la corporación ambiental de Antioquia, que prefirió que este fuera cazado por unos empresarios. De manera tal que la opción muchas veces ha sido la misma: pasar por encima de la vida del animal, aludiendo la decisión a factores económicos.

<iframe id="audio_16180524" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_16180524_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
