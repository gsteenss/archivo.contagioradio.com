Title: Asesinada otra lideresa ambiental integrante del COPINH
Date: 2016-07-07 14:41
Category: El mundo, Nacional
Tags: Asesinatos ambientalistas honduras, Copinh, hidroeléctricas en Honduras, Lesbia Yaneth Urquía
Slug: asesinada-otra-lideresa-ambiental-integrante-del-copinh
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/07/COPINH.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: COPINH ] 

###### [7 jul 2016 ] 

Tras cuatro meses y cinco días del asesinato de la lideresa Berta Cáceres, es asesinada su compañera Lesbia Yaneth Urquía, quien a sus 49 años **se destacaba en la defensa de los derechos medioambientales y de las comunidades indígenas hondureñas**. La defensora fue encontrada por sus familiares en la tarde de este miércoles en el municipio de Marcala, luego de varias horas de estar desaparecida.

El asesinato ocurrió en el marco de las reuniones que se llevan a cabo en el municipio para la **aprobación del proyecto de ley que reglamentaría la Consulta Previa, Libre e Informada** a la que tienen derecho los pueblos originarios, amparados en el convenio 169 de la Organización Internacional del Trabajo OIT.

De acuerdo con las primeras informaciones, la líder presentó un cuadro de trauma encéfalo craneal abierto, producido por objeto cortante. También **hay versiones que aseveran que Lesbia murió por varios impactos de bala**; sin embargo, el cadáver ya reposa en Medicina Legal para la autopsia que esclarecería el motivo de su muerte.

La mujer estaba vinculada al Consejo Cívico de Organizaciones Populares e Indígenas de Honduras COPINH desde 2009, cuando las comunidades protestaban contra el golpe de Estado. También había sido **muy activa en las movilizaciones en rechazo a la construcción de la represa hidroeléctrica Aurora I**, en el municipio de San José.

De acuerdo con el COPINH este asesinato es "un **feminicidio político que busca callar las voces de las mujeres que con coraje y valentía defienden sus derechos** en contra del sistema patriarcal, racista y capitalista, que cada vez más se acerca a la destrucción de nuestro planeta".

###### [Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)]] 

 
