Title: Ordenan suspender operaciones de Poligrow Colombia por daños ambientales
Date: 2016-07-12 16:22
Category: Ambiente, Nacional
Tags: mapiripan, Palma, Poligrow empresa de palma
Slug: suspenden-operaciones-de-poligrow-colombia-por-infracciones-ambiantales
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/07/Poligrow.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: [peligrosbiorinoquia.blogspot] 

###### [12 Jul 2016]

La Corporación para el desarrollo del Área de manejo Especial de la Macarena, CORMACARENA, **ordenó la suspensión de operaciones de la empresa Poligrow Colombia Ltda**.  por infracciones y daños ambientales al ecosistema en Mapiripán, Meta, e inició un proceso sancionatorio en contra de la misma, por seis meses, según expresa un [comunicado de la Agencia de Investigación Ambiental, EIA](http://eia-global.org/images/uploads/Poligrow_FINAL_7_12_16_final.pdf).

Al parecer, la empresa estaría [vertiendo aguas residuales en bosques y morichales](https://archivo.contagioradio.com/audiencia-en-congreso-de-la-republica-por-accion-de-poligrow-en-mapiripan/), construyó un dique de cemento sin autorización impidiendo el flujo de las aguas y estaría usando el agua del Caño Macondo para las actividades de la empresa, **generando daños medioambientales en los recursos naturales** como lo son los bosques, morichales, los suelos y las aguas subterráneas, ecosistemas extremadamente frágiles y difíciles de recuperar.

Estos hechos se evidencian después de que [CORMACARENA](http://eia-global.org/images/uploads/acto_administrativo_CORMACARENA.pdf) (resolución) realizara una evaluación de las actividades de la empresa y concluyera que la misma no esta cumpliendo con las normas de protección de aguas, bosques y suelos y que sus actividades distan de un ejemplo de responsabilidad social ambiental. Motivo por el cual deberá suspender sus actividades durante seis meses o hasta que las infracciones sean solucionadas.

La Agencia de Investigación Ambiental, con sede en Estados Unidos, expresó que "“Las comunidades y las tierras afectadas por Poligrow solo podrán ser protegidas si los gobiernos y los mercados consumidores **demandan que las compañías de palma aceitera cumplan con las leyes**”, acogiendo las medidas de protección al ambiente que exige CORMACARENA en estas zonas de protección.

######  Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
