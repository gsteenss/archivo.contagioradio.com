Title: Organizaciones de Trabajadoras Sexuales rechazan proyecto de Clara Rojas
Date: 2017-08-09 16:43
Category: Mujer, Nacional
Tags: Clara Rojas, prostitución, Sintrasexco, trabajadoras sexuales, trabajo sexual
Slug: organizaciones-de-trabajadoras-sexuales-rechazan-proyecto-de-clara-rojas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/08/trabajo-sexual1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Universidad Villa María - Argentna] 

###### [09 Ago. 2017] 

Luego de la propuesta de la congresista del partido liberal, Clara Rojas, en la que plantea que quienes paguen por servicios sexuales a trabajadoras sexuales sean sancionados, **Fidelia Suárez, representante del primer sindicato de trabajadoras sexuales en Colombia, Sintrasexco,** que agrupa a más de 600 mujeres, manifestó que este proyecto vulnera sus derechos, las pone en riesgo y las excluye como gestoras de sus propuestas.

**Las multas que serían impuestas podrán escalar hasta los 32 salarios mínimos legales vigentes,** es decir cerca de 23 millones de pesos “el proyecto como ya se lo hemos dicho a ella, es un proyecto anticonstitucional, que en vez de favorecer a la población lo que hace es discriminar, victimizar y poner más en riesgo nuestro trabajo, obligándonos a estar más en la clandestinidad”. Le puede interesar: [Crónica del primer sindicato de trabajadoras sexuales en Colombia](https://archivo.contagioradio.com/cronica-del-primer-sindicato-de-trabajadoras-sexuales-en-colombia/)

### **Más que prohibicionista sería una propuesta abolicionista** 

Más allá de una postura prohibicionista, **dice Suárez esta propuesta tiene un carácter abolicionista**, por ello se hace necesario reconocer el carácter histórico del trabajo sexual y que “ofrece economía a nuestro país. Yo creo que primero tendrían que analizar los que están detrás de este proyecto como Secretaría de la Mujer, Integración Social y algunas ONG y además ver que nosotras venimos con un proceso legal y reconocimiento como trabajadoras”.

### **¿Cuáles son algunas realidades que están siendo desconocidas en este proyecto de ley?** 

Condiciones dignas **dónde prestar los servicios sexuales, salud integral, acceso a servicios de atención mental** y disminución del hostigamiento y persecución a las trabajadoras sexuales por parte de la Fuerza Pública, son algunas de las propuestas que tienen desde Sintrasexco para que de este modo se disminuyan las afectaciones en contra de las trabajadoras sexuales.

“Hay una exclusión 100% cuando decidimos ejercer trabajo sexual y eso no lo están viendo. El proyecto es un proyecto de exclusión no de inclusión”.

### **¿Cuál es el objetivo de este proyecto según Sintrasexco?** 

Para Suárez, estas iniciativas comienzan a surgir debido a que comienzan las campañas políticas y deben mostrar avances en la gestión de su mandato “yo invito a que investiguen los presupuestos, en qué se han invertido o en qué no en la población vulnerable, donde nos metieron a nosotras, cuando no lo somos. Investiguen, porque la población no se ha beneficiado”.

Dice además que **no entiende cómo esta propuesta va a solucionar la vida de cerca de 4 millones de hombres y mujeres que ejercen trabajo sexual** en el país “yo me pregunto cuántas empresas tiene la doctora Clara Rojas para que hombres y mujeres que ejercemos trabajo sexual en el país trabajemos, aquí hay interés político, eso lo queremos dejar claro”.

### **Con este proyecto se erradicaría la “trata de personas”: Clara Rojas ** 

Dice la representante a la Cámara, Clara Rojas que al imponer estas multas se está desincentivando el turismo sexual y con ello **se reducirán la tasa de asesinatos de mujeres trabajadoras sexuales y la trata de personas,** que es uno de los flagelos que se viven en Colombia y en el mundo. Le puede interesar: [El 66% de los casos de tráfico de personas ocurren en Europa y Asia](https://archivo.contagioradio.com/el-66-de-los-casos-de-trafico-de-personas-ocurren-en-europa-y-asia/)

Sin embargo, para Suárez el trabajo sexual no es igual a la trata de personas y dice que es lamentable que una mujer como Rojas no diferencie entre las dos realidades “cuando hablamos de trabajo en nuestro país en Colombia es mayores de 18 años, lo que es menores es explotación y hacemos muy bien la claridad”.

### **“Hay que construir con las bases”** 

Suárez exhorta a las instituciones a construir proyectos e iniciativas de la mano de las bases que están directamente implicadas al trabajo sexual **“deberían hablar con las personas que ejercemos este trabajo en el país, construir con las bases, no con terceros** creyendo supuestamente desde su imaginario, de sus vivencias, más no desde la realidad que estamos viviendo la población que ejerce trabajo sexual”.

Además, dice que es necesario capacitar y sensibilizar en materia de derechos humanos y laborales a las trabajadoras sexuales **“que nos dejen ser libres, nosotras no decidimos por otras personas,** porque a nadie le hemos dicho qué profesión desean o que profesión pueden ejercer. Ante todo, exigimos respeto, somos madres, tías, abuelas y ante todo parte del pueblo colombiano”.

Durante el 2016, según cifras de **Medicina Legal 51 mujeres trabajadoras sexuales fueron asesinadas**. Según la Secretaría de la Mujer durante el 2016 la violencia física fue la agresión que ocupó el primer lugar en denuncias, luego el abuso policial y por último el abuso sexual.

<iframe id="audio_20256996" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_20256996_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)

<div class="osd-sms-wrapper">

</div>
