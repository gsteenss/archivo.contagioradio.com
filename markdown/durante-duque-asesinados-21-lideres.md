Title: 21 líderes sociales han sido asesinados en lo corrido del Gobierno Duque
Date: 2018-09-04 08:01
Author: AdminContagio
Category: DDHH, Nacional
Tags: Derechos Humanos, Fiscal General, Iván Duque, lideres sociales
Slug: durante-duque-asesinados-21-lideres
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/08/WhatsApp-Image-2018-08-06-at-3.26.09-PM.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [3 Sept 2018] 

Durante la semana que finalizó 6 líderes sociales en diferentes zonas del país perdieron la vida: **Oscar Canas, Victor Ilamo, James Escobar, Oliver Herrera, Alirio Arenas y Amparo Rodríguez.** Con ellos, la cifra de líderes asesinados aumenta a 21, en los 37 días que lleva Iván Duque como primer mandatario de los colombianos.

### ****Martes 28 de agosto: **Son asesinados dos líderes sociales en Cauca** 

La red de derechos humanos del sur-occidente colombiano "Francisco Isaías Cifuentes", denunció el 28 de agosto el asesinato de los defensores de derechos humanos **Oscar Campos Canas y Victor Alfonso Ilamo,** así como del campesino Jhon Edward Cayapu Conda en Corinto, Cauca. Ambos líderes hacían parte de Fensuagro, el Proceso de Unidad Popular del Suroccidente Colombiano (PUPSOC) y estaban incluidos en el **Programa Nacional Integral de Sustitución de Cultivos de Uso Ilícito (PNIS).**

### ****Miércoles 29 de agosto: Asesinan a James Escobar en Tumaco, Nariño**** 

**James Escobar** fue asesinado por hombres armados, mientras se desplazaba en lancha por el río Mira, cerca a Tumáco. Era líder de las Comunidades del Alto Mira y Frontera, sobre las que la Comisión Interamericana de Derechos Humanos había emitido este año medidas cautelares para su protección. Por amenazas recibidas contra sus miembros, la comunidad viene pidiendo constantemente se tomen las medidas de protección colectiva, sin embargo, esta solicitud aún no ha sido respondida.

### ****Viernes 30 de Agosto: Asesinan a Oliver Herrera, líder social del Meta**** 

**Oliver Herrera** fue asesinado por hombres armados, que llegaron hasta su casa y  le dispararon en repetidas ocasiones hasta provocarle la muerte. Herrera, era presidente de la Junta de Acción Comunal (JAC) de la vereda Brisas del Guayabero, en La Macarena, Meta; sin embargo, no había recibido amenazas contra su vida y por el contrario, era una persona querida en su comunidad.

### ****Domingo 2 de Septiembre: Son asesinados dos líderes en Putumayo y Norte de Santander**** 

En horas de la mañana fueron asesinados la docente **Amparo Fabiola Rodríguez Muchachaisoy y su esposo, Alonso Taicus Guanga** en el resguardo indígena Playa Larga de Villagarzón, Putumayo. En los hechos, 3 sujetos asesinaron a la mujer, quien desarrolla sus labores en el establecimiento educativo Atun Ñambi, al igual que a su compañero, quien es indígena del pueblo Awa.

Por otra parte, el mismo domingo fue asesinado **Alirio Antonio Arenas Cárdenas,** presidente del Consejo de Convención, Norte de Santander, presidente de la JAC de San Isidro, en el mismo departamento y directivo del Movimiento por la Constituyente Popular. Aunque Arenas había denunciado amenazas en su contra, hasta el momento no había recibido medidas adecuadas para la protección de su vida.

### **21 líderes han sido asesinados en los 37 días del gobierno Duque ** 

Antes de llegar a la presidencia, **Iván Duque** afirmó que la protección de los líderes sociales sería un tema importante para su Gobierno, sin embargo, solo hasta hoy se nombró a Pablo Elías Gonzalez, como director de la Unidad Nacional de Protección tras el fallido intento de delegar en el cargo a **Claudia Ortíz**. Peor aún, en los 37 días que Duque ha venido ejerciendo la presidencia, han asesinado a 21 líderes y lideresas sociales.

El mismo día que Duque llegó a la presidencia (7 de agosto), fue asesinado [Uriel Rodríguez,](https://archivo.contagioradio.com/primer-lider-asesinado-gobierno-duque/) líder de sustitución de tierras en Cauca; como él, otros 3 líderes que hacían parte del **PNIS** fueron ejecutados. La tercera parte de los líderes hacían parte o eran presidentes de las JAC de sus municipios, 4 de ellos eran indígenas y los demás hacían parte de procesos de liderazgos campesinos, étnicos, juveniles o de víctimas.

Aunque el pasado 23 de agosto se firmó el "Pacto por la Vida" por parte de los funcionarios del Gobierno encargados de proteger la vida de los líderes sociales, lo cierto es que en 37 días, **18 líderes y 3 líderesas sociales han sido asesinados.** Algunos a pesar de contar con medidas de protección, como en el caso de [Alberto Niscue.](https://archivo.contagioradio.com/asesinan-lideres-pacifico/) (Le puede interesar: ["Pacto por la vida se firmó excluyendo a líderes que la tienen en riesgo"](https://archivo.contagioradio.com/pacto-vida-excluyendo-lideres/))

Incluso, el Fiscal General de la Nación, **Néstor Humberto Martínez,** afirmó recientemente que no hay impunidad en casos de asesinatos de líderes; sin embargo, organizaciones como [Somos Defensores](https://archivo.contagioradio.com/en-casos-de-lideres-sociales-no-se-alcanza-justicia/) han advertido que hay un indice muy bajo de sentencias y, en su mayoría, éstas no condenan a los autores intelectuales de los asesinatos. Es decir, solo se captura a los sicarios, que son los responsables materiales de los asesinatos de 16 de los 21 líderes. (Le puede interesar: ["Fiscal General niega impunidad en asesinatos a líderes sociales"](https://archivo.contagioradio.com/fiscal-general-niega-impunidad-en-asesinatos-a-lideres-sociales/))

###### [Reciba toda la información de Contagio Radio en] [su correo](http://bit.ly/1nvAO4u) [o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por ][Contagio Radio](http://bit.ly/1ICYhVU). 
