Title: Estudiantes de Universidad Libre retornan a cese de actividades por incumplimientos
Date: 2018-04-09 13:38
Category: Educación, Mujer
Tags: Cese de actividades, Crdr, estudiantes, Universidad Libre
Slug: comunidad-estudiantil-de-universidad-libre-retornan-a-cese-de-actividades-por-incumplimientos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/04/unilibre.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:Apoyo Paro UniLibre] 

###### [09 Abr 2018] 

La comunidad estudiantil de la Universidad Libre anunció que **regresará al cese de actividades, el próximo 24 de abril**, debido al incumplimiento por parte de las directivas de atender el pliego de exigencias de los estudiantes y garantizar un espacio de concertación en búsqueda, de acuerdo con el estudiantado, de mejorar la calidad de la educación.

Esta decisión es producto, de acuerdo con un comunicado de la comunidad estudiantil, de que las directivas de carácter nacional de la institución, después de hacerlos esperar por dos horas, se negaran a tener una reunión, previamente acordada. Posteriormente el presidente nacional del Centro Educativo, Jorge Alarcon, les manifestó que **"no escucharía el pliego de peticiones porque, porque para ello tenían representantes".**

Frente a lo que los estudiantes respondieron que actualmente hay 2 representantes en conciliatura para más de 30 mil estudiantes a nivel nacional, un representante en consejo directivo para más de 9 mil estudiantes en Bogotá y un representante en comité de unidad académica por facultad, **que para los estudiantes es insuficientes a la hora de hacer un ejercicio de representación en los espacios mencionados. **(Le puede interesar:["Estudiantes de Universidad Libre en paro por altos costos en matrículas"](https://archivo.contagioradio.com/estudiantes-de-universidad-libre-en-paro-por-altos-costos-en-matriculas/))

El cese de actividades que irá desde el 24 hasta el 25 de abril, servirá como un escenario de debate entre los estudiantes y la comunidad académica, además se realizarán actividades culturales y se espera que las directivas **accedan a cumplir la promesa del pasado mes de febrero** cuando se comprometieron a escuchar a los estudiantes y sus inconformidades en un espacio que garantice la construcción de una mejor eduación.

###### Reciba toda la información de Contagio Radio en [[su correo]
