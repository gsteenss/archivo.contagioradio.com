Title: Informe revela los impactos del fracking en 6 países de Latinoamérica
Date: 2016-11-04 12:14
Category: Ambiente, Nacional
Tags: Censat agua Viva, Fracking colombia, No más Fracking
Slug: impactos-del-fracking-en-6-paises-de-latinoamerica
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/impactos-del-fracking-colombia1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: aimdigital] 

###### [4 Nov de 2016] 

Organizaciones ambientales hicieron público el informe "**Última Frontera: Políticas públicas, impactos y resistencias al fracking en América Latina**’, un documento que revela los impactos que conlleva la implementación de esta técnica extractiva en países como Argentina, Bolivia, Brasil, Chile, Colombia y México.

El informe aborda el **contexto energético de cada país, las políticas públicas para promover y regular el *fracking*, los impactos sociales, ambientales y económicos** ocasionados sobre la población, en sus derechos humanos, en los territorios y las estrategias de incidencia, movilización y resistencia desplegadas en cada país.

**Tatiana Roa, directora de CENSAT Agua Viva**, señaló que el objetivo de este trabajo colectivo es contribuir a instalar la discusión sobre un **“urgente cambio en el modelo energético de nuestra región, hacia uno más sostenible y socialmente más justo”.**

Pese a las dificultades de acceso a la información, la ambientalista manifiesta que el documento **“sienta bases investigativas para fortalecer las acciones legales que puedan adelantar las comunidades en defensa de sus territorios”**. Por ejemplo en Colombia, con la sentencia T445 que otorga autonomía a los municipios para prohibir este tipo de actividades. Le puede interesar: [Municipios decidirán su propio modelo de desarrollo.](http://municipios%20decidirán%20su%20propio%20modelo%20de%20desarrollo)

Los resultados de la investigación, se materializa en casos como el de San Martín en el Cesar, comunidad que a través de la **protesta y la movilización social han logrado sentar las bases para frenar la incursión de esta dañina técnica a los territorios de todo el país. **Le puede interesar: [Conoco Phillips no cumple requisitos de licencia ambiental para](https://archivo.contagioradio.com/conoco-phillips-no-cumple-requisitos-de-licencia-ambiental-para-fracking-en-san-martin/) fracking [en San Martín.](https://archivo.contagioradio.com/conoco-phillips-no-cumple-requisitos-de-licencia-ambiental-para-fracking-en-san-martin/)

Roa revela que en el informe se presentan casos particulares en Norte América y América Latina, que reflejan los graves daños ambientales y sociales que genera esta técnica no convencional de extracción. Esto con el fin de hacer **un llamado de atención a los Gobiernos, que a lo largo y ancho del continente, han promovido y permitido la entrada del Fracking** a zonas de ecosistemas fundamentales para el desarrollo de la vida.

Las Organizaciones ambientales que trabajaron en la elaboración del informe hacen parte de la Alianza Latinoamericana Frente al Fracking, entre ellas se encuentra, Ambiente y Sociedad, Red de Justicia Ambiental, CENSAT Agua Viva, entre otras, hicieron el lanzamiento del informe ‘

[Última Frontera: Políticas públicas, impactos y resistencias al fracking en América Latina](https://www.scribd.com/document/329989171/Ultima-Frontera-Politicas-publicas-impactos-y-resistencias-al-fracking-en-America-Latina#from_embed "View Última Frontera: Políticas públicas, impactos y resistencias al fracking en América Latina on Scribd") by [Contagioradio](https://www.scribd.com/user/276652802/Contagioradio#from_embed "View Contagioradio's profile on Scribd") on Scribd

<iframe id="doc_157" class="scribd_iframe_embed" src="https://www.scribd.com/embeds/329989171/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-qYCOR4Pk95qLdJ7ie2Sf&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="0.7080062794348508"></iframe><iframe id="audio_13616672" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_13616672_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en su correo [[bit.ly/1nvAO4u]
