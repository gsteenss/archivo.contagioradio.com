Title: Puntos críticos de la negociación con los transportadores
Date: 2016-07-05 13:59
Category: Movilización, Nacional
Tags: Asociación Colombiana de Camioneros, esmad agrede a camioneros, paro camionero 2016, paro transportes 2016
Slug: los-4-puntos-criticos-de-la-negociacion-con-los-transportadores
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/07/Conversaciones-Paro-Camionero.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Colombia Informa ] 

###### [5 Julio 2016] 

29 días completa el paro de transportadores y 26 son las reuniones que han sostenido con el Gobierno; sin embargo no se han llegado a acuerdos porque según Pedro Aguilar, de la 'Asociación Colombiana de Camioneros', los documentos que quieren hacerles firmar no reflejan las propuestas que han discutido con los representantes del ejecutivo, así mismo, **se insiste en no cambiar el sistema de fletes, ni presentar el proyecto de decreto para la dignificación de los camioneros** y obligar a que en cese de actividades salgan vehículos de circulación.

Aguilar afirma que en 102 puntos de concentración están 250 mil camioneros, el 90% del gremio, que reclama una tasa de fletes más justa, pues con la actual por** un viaje de Bogotá a Barranquilla les pagan \$3 millones, y en peajes, combustibles y demás necesidades, gastan \$2.800.000**, es decir, les quedan sólo \$200 mil como ganancia. Durante los últimos cinco años en más de 500 mesas de negociación, los camioneros han insistido en que este sistema de fletes los tiene en la quiebra; sin embargo, el Gobierno no ha atendido sus demandas.

El vocero denuncia que efectivos del ESMAD ingresaron este lunes, en horas de la noche, a uno de los hoteles de San Alberto, Cesar, en los que se hospedan los camioneros a quienes sacaron a empujones de las habitaciones e hirieron con las canicas y bombas de aturdimiento que lanzaron dentro del hotel. En el hecho resultaron heridas 5 personas. De otro lado, Aguilar insiste en que **el principal responsable del desabastecimiento es el Gobierno** pues desde 2011 ha incumplido con los acuerdos a los que se han llegado en los 5 paros que han promovido.

<iframe src="http://co.ivoox.com/es/player_ej_12128579_2_1.html?data=kpeelJ2Ze5qhhpywj5aUaZS1lpqah5yncZOhhpywj5WRaZi3jpWah5yncbHZxdfcjabLucrgwteSlKiPhdTjxJOYpcbRrdDixtfc1ZKJe6ShpNTb1sbLrdCfs8bRy9SPcYarpJKh&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)]]

 
