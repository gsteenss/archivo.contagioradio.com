Title: Comunidades responsabilizan a Santos por compromisos incumplidos en Chocó
Date: 2017-05-09 14:11
Category: Movilización, Nacional
Tags: Chocó, Movilización, Paro cívico
Slug: el-presidente-santos-es-el-responsable-de-los-compromisos-asumidos-e-incumplidos-en-el-choco-emilio-pertuz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/05/paro_choco_0.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Zonacero] 

###### [09 Mayo 2017] 

Tras los incumplimientos de los acuerdos logrados el año pasado entre los chocoanos y el presidente Santos, **este 10 de mayo, los ciudadanos de este departamento saldrán** para exigirle al primer mandatario inversión en el sector de la salud y la construcción de vías de comunicación del Chocó con el resto del país.

Los acuerdos que incluían la creación de comisiones de evaluación y seguimientos de los compromisos, la inversión en saneamiento del hospital de segundo nivel San Francisco de Asís y la asignación de presupuesto a las vías que comunican al Chocó con la República de Colombia, llevan un año sin cumplirse. Le puede interesar: ["1500 indígenas se suman a paro de desplazados en RioSucio, Chocó" ](https://archivo.contagioradio.com/1500-indigenas-mas-se-sumaran-a-paro-en-riosucio-choco/)

Para Emilio Pertuz, representante legal del Consejo Comunitario de Capurganá y miembro de Procesos de Comunidades Negras PCN, **los chocoanos han perdido la confianza en el gobierno** y afirma que lo "mínimo que tienen que hacer para que levantemos el paro cívico, es entregar los certificados de disponibilidad presupuestal de las obras que Chocó necesita". Sin la certeza de que se van a suplir las necesidades de los chocoanos, Pertuz asegura que estarían ante un nuevo engaño del gobierno nacional.

Entre el pliego de peticiones de los chocoanos se encuentra **que los organismos de control de la República y los organismos internacionales sirvan de garantes** para que haya un seguimiento a las actividades del gobierno. Según Perutz, "si los funcionarios se comprometen y no cumplen y los organismo de control no vigilan, no se va a hacer nada serio".

Para la realización de la movilización los ciudadanos están pidiendo garantías y aseguran que la guardia indígena les ayudará con el tema de la seguridad para evitar que haya infiltrados que alteren el orden público. A su vez, **piden apoyo del Ejército y la Policía** para evitar que los paramilitares, que tienen el control de los corredores de la región para el tráfico ilícito de mercancías, se unan al paro con intenciones ajenas a la de los pobladores. Le puede interesar: ["En Chocó, indígenas y afrodescendientes marchan contra el paramilitarismo"](https://archivo.contagioradio.com/en-choco-indigenas-y-afrodescendientes-marchan-contra-el-paramilitarismo/)

De igual forma, **hacen un llamado para que la Comisión de Derechos Humanos de la Cumbre Agraria ponga a funcionar el sistema de monitoreo** en Chocó y así se pueda evitar cualquier hecho que perjudique a los participantes del paro cívico.

Las convocatoria a la movilización la hacen los chochoanos llamando también a los pueblos del pacífico colombiano para que se hagan las inversiones que ya han prometido en los 12 municipios del departamento del Chocó.

<iframe id="audio_18588196" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_18588196_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
