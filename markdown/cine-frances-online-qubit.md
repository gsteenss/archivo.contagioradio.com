Title: Lo mejor del cine jóven francés puede verse online
Date: 2017-01-16 11:54
Author: AdminContagio
Category: 24 Cuadros
Tags: Cine, Festival, francés, películas online
Slug: cine-frances-online-qubit
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/01/myfrenchfilmfestival-has-launched.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### Foto: My French Film Festival 

##### 16 Ene 2017 

Hasta el próximo 13 de febrero, las 29 producciones que hacen parte de la séptima edición de **My French Film Festival**, competencia única enfocada en destacar el cine joven francés, **podrán verse on line para toda latinoamérica y el mundo** a través de la plataforma Qubit.tv.

**10 largometrajes y 10 cortometrajes franceses** entre comedias, thrillers, películas de acción, romances y dramas, estarán disponibles en la plataforma de video on demand con la posibilidad de disfrutarlos **con traducción a diez idiomas diferentes**. Adicionalmente el evento incluye 3 largometrajes y 5 cortometrajes fuera de competencia.

El jurado de My French Film Festival, está presidido por el reconocido **director argentino Pablo Trapero**, responsable de películas como “El Clan” (2015), “Leonera” (2008), “Elefante Blanco” (2012), “Carancho”(2010) y otras joyas como Mundo Grúa (1999), Masterplan (2012).

Además de disfrutar de las [películas inéditas](https://drive.google.com/file/d/0B-FBT3EIk84iRTFpdFRtV1ZNTmQ0VmszT3gzVHBTX0FNeHNv/view), **los espectadores tendrán la posibilidad de votar y escoger las que a su gusto y valoración deben llevarse el premio del Público**. Le puede interesar: [El cine necesita de nuevas plataformas para ser visible](https://archivo.contagioradio.com/cine-plataformas-vod-qubit/).

**En 2016 el Festival alcanzó más de 6,5 millones de proyecciones en 206 lugares del mundo.** La selección oficial del evento del año anterior se encuentra disponible en la plataforma de Qubit.tv que se ha especializado en la difusión de contenidos  que se encuentran por fuera del circuito comercial.
