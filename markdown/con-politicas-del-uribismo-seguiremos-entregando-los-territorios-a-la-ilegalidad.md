Title: "Con políticas del uribismo seguiremos entregando los territorios a la ilegalidad"
Date: 2020-02-27 18:48
Author: CtgAdm
Category: DDHH, Entrevistas
Tags: coca, Sustitución Voluntaria, uribismo
Slug: con-politicas-del-uribismo-seguiremos-entregando-los-territorios-a-la-ilegalidad
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/02/Eduardo-Díaz-sobre-Uribismo.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:heading {"align":"right","level":6,"textColor":"cyan-bluish-gray"} -->

###### Foto: @Policia924FM {#foto-policia924fm .has-cyan-bluish-gray-color .has-text-color .has-text-align-right}

<!-- /wp:heading -->

<!-- wp:html -->  
<iframe id="audio_48338922" frameborder="0" allowfullscreen scrolling="no" height="200" style="border:1px solid #EEE; box-sizing:border-box; width:100%;" src="https://co.ivoox.com/es/player_ej_48338922_4_1.html?c1=ff6600"></iframe>  
<!-- /wp:html -->

<!-- wp:paragraph -->

Recientemente se conoció que el gobierno Duque no renovará el convenio que tenía con la ONU para hacer el monitoreo y verificación del Plan Nacional Integral de Sustitución (PNIS), a ello se sumó el reciente [informe](https://archivo.contagioradio.com/onu-mas-medidas-integrales-menos-accionar-policial-militar/)entregado por el Alto Comisionado de las Naciones Unidas para los DD.HH. y la respuesta del Gobierno a dicho informe. Situaciones que para el exdirector del PNIS, Eduardo Díaz Uribe, forman parte de las políticas erráticas del uribismo para conducir el país hacía la paz.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### **Sin la verificación de ONU, se pierde información veraz sobre los cultivos de uso ilícito**

<!-- /wp:heading -->

<!-- wp:paragraph -->

Díaz recordó que la Oficina de las Naciones Unidas para la Droga y el Delito (UNDOC) es una entidad que hacía acompañamiento del programa de sustitución desde el gobierno Santos e incluso, acompañó al gobierno Uribe en el programa de familias guardabosques. Ese programa duró 9 años, y logró erradicar cerca de 12 mil hectáreas.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En contraste, el exfuncionario señaló que **el PNIS en sus primeros 16 meses logró la eliminación de cerca de 34 mil hectáreas**. Es decir que en algo más de 1 año, la sustitución voluntaria logró cerca del triple de hectáreas de Coca eliminadas respecto a lo que logró el gobierno Uribe en 9 años.

<!-- /wp:paragraph -->

<!-- wp:quote -->

> "La sustitución voluntaria logró cerca del triple de hectáreas de Coca eliminadas respecto a lo que logró el gobierno Uribe en 9 años. "

<!-- /wp:quote -->

<!-- wp:paragraph -->

Los datos entregados por Díaz son tomados de informes presentados por la UNDOC, que para él, es una agencia que le ofrece garantías al país para tomar las mejores desiciones respecto a temas fundamentales como los cultivos de uso ilícito. Por lo tanto, eliminar la cooperación con este organismo significa quedar en manos de las interpretaciones de los funcionarios del gobierno, en términos más claros, "el riesgo es que desaparece información veráz".

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### **Lo que no le conviene al Uribismo que se sepa**

<!-- /wp:heading -->

<!-- wp:paragraph -->

La UNDOC certificó que, tras un año de que los campesinos arrancaran los cultivos de coca de forma voluntaria, el porcentaje de resiembra fue del 0,4%. Asimismo, avaló que a la fecha se han retirado cerca de 41 mil hectáreas de Coca en todo el territorio nacional. (Le puede interesar: ["Pactos de sustitución deben cumplirse, glifosato no debe ser prevalente"](https://archivo.contagioradio.com/pactos-de-sustitucion-deben-cumplirse-glifosato-no-debe-ser-prevalente/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Mientras tanto, el mismo Gobierno ha aceptado que acudiendo a la fumigación y la erradicación forzada, la resiembra alcanza topes de hasta el 60%. Para Díaz, las cifras avalan la idea de que el camino es la sustitución voluntaria y no la erradicación forzada, y manifestó que quizá ese tipo de datos estén detrás de la salida de la UNDOC.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### **La paz cuesta, pero cuesta más la guerra**

<!-- /wp:heading -->

<!-- wp:paragraph -->

En cuanto al funcionamiento del programa, el Exdirector del PNIS aseguró que no se le están dando los recursos financieros suficientes, así como no se ha avanzado en otros temas claves para desarrollar el potencial del Acuerdo de Paz de forma universal como la Reforma Rural Integral (RRI). (Le puede interesar: ["Desconocimiento e incumplimiento del Gobierno con la sustitución de cultivos de uso ilícito"](https://archivo.contagioradio.com/coccam-exige-garantias-para-las-familias-que-trabajan-por-la-sustitucion-de-cultivos/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Díaz recordó que la idea del Acuerdo es transformar la vida de los colombianos en la tercera parte del territorio nacional, que por la ausencia de una reforma agraria, tuvieron que colonizar selvas y allí sembrar Coca. En ese sentido, hacer la paz significa pagar la deuda de lo que el Estado dejó de hacer por sus ciudadanos en los últimos 70 años.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Esa deuda se estimó en cerca de 126 billones de pesos que tendrían que ser invertidos a lo largo de 15 años, y cubrirán gastos como el generado por el PNIS o los Planes de Desarrollo con Enfoque Territorial (PDET). "Es un esfuerzo inmenso que es costoso, pero es mucho menos costoso que seguir haciendo la guerra", concluyó Díaz.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### **Con políticas del Uribismo seguiremos entregando los territorios a la ilegalidad**

<!-- /wp:heading -->

<!-- wp:paragraph -->

Para el experto, una muestra del talante del gobierno Duque está en las palabras del consejero para la estabilización Emilio Archila, que catalogó como 'chambón' el informe entregado por el Alto Comisionado de las Naciones Unidas para los DD.HH sobre Colombia. Otras manifestaciones similares han tenido funcionarios como la [canciller Claudia Blum](https://twitter.com/CancilleriaCol/status/1233136658561871878).

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

"Este gobierno y sus líderes (del uribismo) se oponen al gobierno de paz y creen que los problemas se solucionan por la fuerza", continuó Díaz, lo que ha significado dejar el terreno libre a actores ilegales para que se acerquen al campesinado y le ofrezcan la compra de los cultivos de Coca. En cambio, el Estado llega con la erradicación forzada y la fumigación.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Con la continuidad de las política del uribismo, "seguiremos entregando los territorios a la ilegalidad", concluyó Díaz; aunque también declaró que la historia del país no termina con el gobierno Duque, y en cambio, las comunidades sí han dado muestras de dignidad y determinación para tomar el rumbo del país mediante la movilización social desde el año pasado.  

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78980} /-->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->
