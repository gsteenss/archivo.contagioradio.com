Title: La esperanza de unidad se materializa en la Cumbre Social y Política por la paz
Date: 2016-11-10 15:46
Category: Movilización, Nacional
Tags: La Paz se Moviliza, Mesa Social para la paz., Paz a la Calle
Slug: la-esperanza-de-unidad-se-materealiza-en-la-cumbre-social-y-politica-por-la-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/10/marcha-por-la-paz64.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [10 de Nov 2016] 

Diferentes sectores y organizaciones del movimiento social se reunieron para coordinar una agenda de movilizaciones y un escenario denominado **“La Paz se Moviliza”, con el fin de generar muchas más acciones en conjunto y unificar las iniciativas y posturas a nivel nacional sobre paz que culminen en la creación de una agenda social y en la Cumbre Social y Política por la paz.**

Y es que pese a las múltiples expresiones de la ciudadanía que se han sentido en el país, aún no se aplican los acuerdos de paz entre el gobierno y las FARC-EP, motivo por el cual, esta nueva plataforma pretende incidir con mucha más fuerza en la construcción de paz y en que se dé el paso hacía la refrendación de los acuerdos con las FARC además de presionar para que se avance en la instalación de la mesa de conversaciones con la guerrilla del ELN y así obtener una paz íntegra y completa.

 “**La búsqueda urgente de la paz, no puede ser un anhelo o una aspiración sino un derecho fundamental** de las y los colombianos, por este motivo diferentes organizaciones, plataformas, secotres del movimiento social nos dimos a la tarea de convocar  un escenario que impulse más iniciativas por la paz” afirmo Carlos García, miembro de Clamor social por la paz. Le puede interesar:["Listos mecanismos de participación de Mesa Social para la Paz"](https://archivo.contagioradio.com/listos-mecanismos-de-participacion-de-mesa-social-para-la-paz/)

La gran **Cumbre Social y Política por la paz**, es uno de los eventos que “La Paz se moviliza” realizará los próximos días 29 y 30 de noviembre, el encuentro tiene como finalidad recoger las opiniones de las y los ciudadanos frente a la paz y estimular la participación de las personas en estos escenarios. De igual forma pretende que una serie de pliegos y peticiones construidos por el movimiento social lleguen al escenario para poder generar una ruta más integral entre los participantes.

La Cumbre tendrá tres objetivos de trabajo el primero es **generar una unidad** que respete las diferentes posturas políticas y la diversidad al interior de la sociedad, el **segundo es la participación ciudadana** y real que logre incidir en las trasformaciones de la realidad y le último es la **construcción de una agenda social y política**.

El 17 de noviembre el Comando Nacional Unitario está convocando a una gran jornada de movilización nacional contra la Reforma Tributaria,  el escenario de “La Paz se moviliza” señalo que también participara de esta marcha, además ya existe un cronograma de actividades para el mes de noviembre.

Próximas actividades

**16 y 17 de noviembre**: Encuentro Regional de paz en Santa Marta

**17 de noviembre**: Movilización Nacional del Comando Nacional Unitario contra la Reforma Tributaria

**25 de noviembre:** Movilización de mujeres por la paz

**25 y 26 de noviembre**: Encuentro Nacional de Paz a la Calle

**28 y 29 de noviembre**: Encuentro Nacional d Sindicalismo por la Paz

**29 y 30 de noviembre**: Cumbre Social y Política por la paz

**1 diciembre:** Gran Encuentro Nacional por la paz

######  Reciba toda la información de Contagio Radio en su correo [[bit.ly/1nvAO4u]
