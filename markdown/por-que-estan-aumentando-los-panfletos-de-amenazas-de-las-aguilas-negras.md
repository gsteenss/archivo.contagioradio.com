Title: Panfletos de "Águilas Negras" reaparecen en diferentes regiones del país
Date: 2019-01-28 16:29
Author: AdminContagio
Category: DDHH, Nacional
Tags: Aguilas Negras, Fuerza Pública, Iván Duque, paramilitares
Slug: por-que-estan-aumentando-los-panfletos-de-amenazas-de-las-aguilas-negras
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/01/aguilas-negras-770x400.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo Particular] 

###### [28 Ene 2019] 

Desde que inició 2019, diferentes comunidades a lo largo del país vienen denunciado la aparición de panfletos firmados por un grupo identificado como las Águilas Negras, organización que amenaza de muerte a líderes sociales, indígenas, defensores de derechos humanos y políticos, hecho que para el senador Iván Cepeda, denota una falta de voluntad **por parte del gobierno actual para develar quiénes son las personas detrás de estas estructuras y en dónde se encuentran.**

Según Cepeda, estos hechos se atañen a una nueva generación del paramilitarismo en Colombia, razón por la cual en múltiples ocasiones le ha manifestado al gobierno la necesidad de investigar de quien se trata "nos sorprende que mientras otros grupos son fácilmente identificables tanto por su presencia territorial, sus líderes con sus alias respectivos, los hechos en los que la Fuerza Pública logra detener a esos miembros, **no se tiene nada sobre los comandantes de las Águilas Negras, quiénes son sus comandantes o cuál es su distribución territorial".**

Asimismo aseguró que "Águilas Negras es una denominación que se utiliza para realizar cierto tipo de operaciones" y recordó que hacia la década de los noventa, **miembros de Fuerza Pública utilizaban como estrategia para sus acciones en cubierto las denominaciones de grupos ilegales.** (Le puede interesar: ["Águilas Negras amenazan de muerte a Héctor Carabalí, líder social de Buenos Aires, Cauca")](https://archivo.contagioradio.com/amenazan-de-muerte-a-lider-social-en-buenos-aires-cauca/)

### **Los panfletos de las Águilas Negras** 

El más reciente panfleto se conoció este fin de semana, en el que según la Organización Regional Indígena del Valle, el grupo identificado como Águilas Negras amenazó de muerte al gobernador del cabildo de Alcalá, **Ruben Darío Vélez, debido a las denuncias que ha hecho entorno al accionar paramilitar en el territorio. **

De igual forma, durante este fin de semana otro panfleto también firmado por las Águilas Negras, amenazó a 30 organizaciones defensoras de derechos humanos, mesas locales de víctimas, y congresistas; afirmando que contra ellos y sus familias se haría **un exterminio para evitar que la izquierda se tome el país.** (Le puede interesar: ["Águilas Negras amenazan a mesas locales de Víctimas y defensores de derechos humanos"](https://archivo.contagioradio.com/aguilas-negras-amenazan/))

Además de estos hostigamientos, líderes sociales en el Cauca como Héctor Carabalí, ** **Andrés Posu, Edis Caloto y Deyanira Peña, [recibieron] panfletos del mismo grupo el pasado 8 de enero; mientras que organizaciones sociales del Putumayo, denunciaron que las Águilas Negras amenazaron de muerte a defensores de derechos humanos el día 20.

### **Hay que proteger al movimiento social** 

Cepeda manifestó que **en los próximos meses van a tener una reunión nacional, para discutir el tema del paramilitarismo a fondo**, "de lo que se trata es de asumir una agenda concreta de protección, a través de distintas iniciativas que se han venido formulando y que tienen distintos niveles de formulación".

Algunas de ellas tienen que ver con las actividades de autoprotección colectivas como la Guardia Cimarrona o Indígena, **otras con las políticas públicas que ya se han generado desde la Comisión Nacional de Garantías y el Sistema de Alertas Tempranas,** entre otras.

### **Cuatro tipos de amenazas de las "Águilas Negras"** 

La **Fundación Ideas para la Paz (Fip)**, identificó a través de una investigación en el año 2017, cuatro tipos de amenazas que son firmadas con este nombre.

-   Las que intentan impedir las acciones de líderes y organizaciones. Son amenazas que responden a las agendas que ellos impulsan y que buscan que se vayan o detengan su trabajo.
-   De tipo contrainsurgente o donde se amenaza a supuestos auxiliadores, milicianos y “guerrilleros encubiertos".
-   Con fines extorsivos. Se trata de delincuentes que usan el nombre “Águilas Negras” para intimidar y presionar un pago.
-   De limpieza social o dirigidos contra la población LGTBI, habitantes de calle, consumidores de drogas, prostitutas y ladrones, entre otros. Buscan amedrentar a estas personas para evitar que sigan cometiendo delitos o para hacer que se vayan de determinada zona.

<iframe id="audio_31885878" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_31885878_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
