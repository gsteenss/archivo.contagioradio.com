Title: Llegó la hora que las mujeres cuestionen a los candidatos a presidencia
Date: 2018-05-04 12:36
Category: Movilización, Política
Tags: candidatos presidenciales, mujeres
Slug: llego-la-hora-de-que-las-mujeres-cuestionen-a-los-candidatos-a-presidencia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/02/mujeres-paz.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [04 May 2018] 

Este próximo 8 de mayo se llevará a cabo en Casa Ensabamble, en Bogotá, la tercera versión del debate “Las mujeres preguntan”, al que asistirán los candidatos a la presidencia de Colombia y en dónde se espera **que se escuchen no solo las propuestas de los aspirantes, sino también las inquietudes de las mujeres frente a las políticas que**, para ellas, hacen falta en el país, para que verdaderamente existan garantías en sus derechos.

De acuerdo con Juliana Martínez, coordinadora de la Mesa por la vida y la salud de las mujeres, la importancia de este espacio radica en que las mujeres de todo el país puedan escuchar las propuestas de los candidatos y cómo incorporaran **la agenda que tienen las mujeres a sus planes de gobierno, sobre todo teniendo en cuenta**, que las mujeres son más de la mitad de la población habilitada para votar.

“Según la Misión de Observación Electoral, las mujeres somos 18.6 millones de personas habilitadas para votar. **Entonces tenemos es que asumir ese poder y esa fuerza electoral y cuestionar a los candidatos sobre qué propuestas tienen para nosotras**” afirmó Juliana. (Le puede interesar: ["Jóvenes le apuestan a la participación política y el voto consciente"](https://archivo.contagioradio.com/jovenes-le-apuestan-a-la-participacion-politica-y-al-voto-consciente/))

En el foro participarán mujeres colombianas que viven en el extranjero. Asimismo, las preguntas que se realizarán a los aspirantes se escogieron a través de una votación realizada virtualmente. **Los temas que se tocarán en el evento serán: democracia paritaria, violencia contra las mujeres, economía del cuidado y derechos reproductivos**.

En total son 200 organizaciones de todo el país las que están participando en la elaboración de este espacio. Las personas que deseen ingresar al auditorio deberán inscribirse previamente en las páginas web de las organizaciones.

<iframe id="audio_25793744" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_25793744_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
