Title: "No es posible tener paz sino hay una cambio en la política carcelaria  del país”: Miguel Ángel Beltrán
Date: 2016-09-01 22:31
Category: Entrevistas
Tags: FARC, miguel angel beltran, proceso de paz
Slug: no-es-posible-tener-paz-sino-hay-una-cambio-en-la-politica-carcelaria-del-pais-miguel-angel-beltran
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/09/miguel-angel-beltran.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### 2 Sep 2016 

“Es una alegría colectiva, fue el resultado de las sumas  de las luchas y las expresiones de comunicados, protestas de familiares, compañeros, colegas y amigos”, expresó Miguel Ángel Beltrán, minutos después de haber quedado en libertad luego de 14 meses de prisión en la el centro penitenciario el ERON Picota.

Aunque se esperaba que la audiencia se realizara el próximo 8 de septiembre, fue una sorpresa que se decidiera dejar libre al profesor, tras resolverse un recurso de casación por el cual la Sala Penal de la Corte Suprema de Justicia **tumbó la condena de ocho años y cuatro meses de prisión emitida por el Tribunal Superior de Bogotá** por el delito de rebelión, pues se le acusaba de tener vínculos con la guerrilla de las FARC.

Su primera captura se produjo en mayo de 2009, en un operativo adelantado por autoridades colombianas con apoyo de la Policía de México, señalado según los documentos hallados en los computadores de ‘Raúl Reyes,’ de ser el encargo de dirigir eventos académicos para promover y obtener recursos económicos para las Farc.

Sin embargo, la Defensa del docente de la Universidad Nacional había asegurado que su condena y recaptura se basaron en pruebas ilegales, citando sentencias de la propia Corte Suprema de Justicia, así mismo se evidenció en el recurso extraordinario de casación, donde se precisa que no existe una prueba directa que **haga referencia a que el profesor universitario es ‘Jaime Cienfuegos’ como sostuvo la Fiscalía General durante el proceso.**

David Albarracín, abogado de Miguel Ángel afirmaba que el sociólogo, **“fue condenado con pruebas ilegales y aparte de eso, su sentencia,** condena el pensamiento diferente, la forma de expresar, la divergencia política e ideológica y en un país que quiere ir rumbo a la paz condenar esto, es exclusión política y un mensaje negativo para cualquier proceso de paz”.

Es así, como durante estos 14 meses, el profesor universitario se caracterizó no solo por mantener su dignidad pese a las múltiples acusaciones por parte de medios de comunicación y un sector de la sociedad que lo señalaban como Jaime Cien Fuegos, sino que también se convirtió en vocero de los presos políticos y sociales, víctimas del sistema carcelario y las constantes violaciones a los derechos humanos de los internos sufren por parte del INPEC.

“No es posible tener  paz sino hay una cambio en la política carcelaria  del país”, dice el docente, quien fue víctima también de la crisis carcelaria, pues durante su tiempo en la prisión duró 6 meses sin recibir luz natural, no lo dejaban ingresar prensa o libros, las visitas conyugales las debía atender en el piso, y además, no le suministran el agua suficiente.

Beltrán dice tener una mirada diferente a cuando ingreso a la cárcel. **Durante su tiempo en prisión, pudo analizar de cerca los costos y orígenes de la guerra, pero también la parte humana de todos aquellos actores que participaron de ella.** “Uno se encuentra con personas que estuvieron en los grupos paramilitares y cuando uno habla con ellos descubre una dimensión humana, el paramilitarismo también fue una opción económica para personas pobres, dejando miles de familias destrozadas”.

Hoy asegura que en medio de esos oscuros días de una historia de persecución y estigmatización ha ganado esta batalla  y  **“ahora salen nuevos retos. Se abre un nuevo horizonte con los acuerdos de paz** y para que otras fuerzas de la sociedad participen en este proceso”, dice el profesor, quien añade que “es hora también de que salgan libres David Ravelo, Hugo Ballesteros y todos los campesinos judicializados por pensar distinto”.

Miguel Ángel siempre se opuso a la idea de que lo cubriera el indulto, porque asegura que no cometió ningún delito, “**En las cárceles hay mucha gente producto de falsos positivos y víctimas de la intolerancia, a ellos toca restituirles sus derechos** y colocarlos en el lugar donde siempre han estado. Mi invitación es a que desmovilicen esa intolerancia, abran sus pensamientos a horizontes más amplios, para que el país se pueda transformarse”.

<iframe src="http://co.ivoox.com/es/player_ej_12758084_2_1.html?data=kpekl52UfJWhhpywj5aYaZS1lp6ah5yncZOhhpywj5WRaZi3jpWah5ynca7dyNrSzpClssjZzZCvx9HYtoa3lIquk9OPpYy30NPhw8zNs4zGwsnW0ZKJe6ShpNTb1sbLrdCfs8bRy9SPcYarpJKh&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>  
<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/video.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2Fvideos%2F10153724421560812%2F&amp;show_text=0&amp;width=400" width="400" height="400" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)

 
