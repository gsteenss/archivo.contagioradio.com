Title: Peasant Association of Catatumbo has no guarantees to participate to the National Strike
Date: 2019-04-24 17:38
Author: CtgAdm
Category: English
Tags: Catatumbo, National Development Plan, Peace Agreement, Strike
Slug: peasant-association-of-catatumbo-has-no-guarantees-to-participate-to-the-national-strike
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/9441855746_1a16b28fcd_b.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

Peasant Association of Catatumbo (ASCAMCAT) announced through a statement that they wished to attend the strike on Thursday, April 25^th^, but denounced that they couldn’t be present because of the insecure conditions of the region which prevent the members from exercising the right to protest. In relation to this, as Juan Carlos Quintero (who integrates the direction of the organization) claimed, the last threat received occurred during the strike call.

 According to the Management Board, in Catatumbo there was no closure of the conflict: after the signature of the Peace Agreement there was only a "manifest calm”. After the FARC left the territories, the National Liberation Army (ELN) and the People's Liberation Army (EPL) began a dispute for the area. The situation  has devolved in clashes since March 2018, which according to the UN, ended up affecting 150 thousand people.

At the end of last year, the EPL withdrew, and the people suffered again clashes between the ELN and the Army, which assigned about 15,000 men belonging to the Vulcano Task Force and the Rapid Deployment Force ( FUDRA) . However, the military presence has not improved the living conditions of the inhabitants of Catatumbo. This situation is compromised also with the National Comprehensive Substitution Program (PNIS) that has not met the expectations of Coca growers.

> [\#DENUNCIA](https://twitter.com/hashtag/DENUNCIA?src=hash&ref_src=twsrc%5Etfw) ataque el 20 de abril Comisión de líderes ascamcat [\#Cisca](https://twitter.com/hashtag/Cisca?src=hash&ref_src=twsrc%5Etfw) y lider comunal en [\#SanCalixto](https://twitter.com/hashtag/SanCalixto?src=hash&ref_src=twsrc%5Etfw) x [@COL\_EJERCITO](https://twitter.com/COL_EJERCITO?ref_src=twsrc%5Etfw) a eso se suma grave denuncia d comunidad de Campo Alegre del asesinato e intento de desaparición de Dimar Torres excombatiente de [\#FARC](https://twitter.com/hashtag/FARC?src=hash&ref_src=twsrc%5Etfw) al parecer por [@COL\_EJERCITO](https://twitter.com/COL_EJERCITO?ref_src=twsrc%5Etfw) [pic.twitter.com/Lz0UaarcAP](https://t.co/Lz0UaarcAP)
>
> — AscamcatOficial (@AscamcatOficia) [April 23, 2019](https://twitter.com/AscamcatOficia/status/1120718465978052608?ref_src=twsrc%5Etfw)

<p>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
</p>
In a region with such conflict, the Association decided to support the strike and its claims, among them, the accomplishment of the agreements reached between Social Movements and the National Government, the implementation of the Peace Agreement and the enforcement of the National Development Plan based on the real needs of the majority of Colombians.

As Quintero explained, "the majority of the leadership of ASCAMCAT has been displaced since October, we have suffered the assassination of five of our members, the attack of two more and the repeated threats". For these reasons, the Association concluded that they didn’t have the guarantees or the physical means to participate in the strike.

 

\[caption id="attachment\_65360" align="alignnone" width="347"\]![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/WhatsApp-Image-2019-04-22-at-2.13.59-PM-214x300.jpeg){.wp-image-65360 width="347" height="486"} ASCAMCAT señala que no tiene garantías para participar en el paro nacional del 25 de abril\[/caption\]
