Title: Se levanta paro en la Universidad Distrital luego de acuerdos alcanzados
Date: 2016-06-30 12:43
Category: Educación, Nacional
Tags: crisis universidades públicas, Movimiento estudiantil Colombia, paro universidad distrital
Slug: tras-60-dias-de-paro-estudiantes-de-la-udistrital-vuelven-a-clases
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/06/UDistrital-.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Canal Capital ] 

###### [30 Junio 2016 ]

[De acuerdo con el más reciente comunicado de la Asamblea General, se levanta el paro que había sido decretado desde el pasado 28 de abril en la Universidad Distrital de Bogotá. Según afirma el estudiante Jhon Noguera, la decisión se tomó teniendo en cuenta los **acuerdos a los que llegaron con el Consejo Superior Universitario** en cinco puntos importantes, entre ellos, derechos humanos, participación y democracia, elección del rector y reforma interna de la institución. ]

[Tras diez días de instalada la mesa de negociación, se ha acordado la consolidación de un Observatorio de Derechos Humanos y la designación de presupuesto de la Universidad para su funcionamiento, así como un **cronograma para la aprobación del estatuto general de reforma de la institución** y la atención al problema de infraestructura en las distintas sedes del claustro. ]

Las representaciones estudiantiles, de profesores, del Consejo Superior y de la Asamblea Constituyente son las encargadas de verificar la negociación y serán las **encargadas de constatar el cumplimiento de los acuerdos**, como el llamado que desde el Consejo Superior se hace a la Fiscalía para que acelere la investigación en torno a los hechos que rodearon la [[muerte del estudiante Miguel Ángel Barbosa](https://archivo.contagioradio.com/fallecio-estudiante-udistrital-agredido-por-el-esmad/) ]y el que se concerte un calendario adecuado para la terminación del semestre.

<iframe src="http://co.ivoox.com/es/player_ej_12080005_2_1.html?data=kpedmpWUdJahhpywj5WdaZS1kpeah5yncZOhhpywj5WRaZi3jpWah5yncavjydOYsNTLucbmwpCajarXuNbYysbb1sqPmYy4ytjh1M7Ypc2hhpywj6jTstXVyM7cjbfFqMrjjJKSmaiReA..&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)]] 
