Title: Alerta por incremento de amenazas contra periodistas en Colombia
Date: 2018-07-13 10:01
Category: DDHH, Nacional
Tags: FLIP, Libertad de Prensa
Slug: incrementan-amenazas-periodistas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/07/violencia-prensa-e1501178542597.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Zona Cero 

###### 13 Jul 2018 

Crece la preocupación por la situación de los periodistas en Colombia, en un comunicado reciente **la Fundación para la Libertad de Prensa FLIP, da cuenta del incremento en el número de amenazas a comunicadores durante la primera mitad de 2018**, y la falta de acciones efectivas para su protección.

De acuerdo con lo reportado por la FLIP, el incremento en relación con **el mes de julio del año anterior, se tenían documentados 65 casos** de amenaza **en el presente año ya va en 89**, registros que marcan una tendencia de manera paralela con el número de asesinatos de líderes sociales en Colombia, algunos de estos contra la libertad de expresión.

Igualmente, la organización asegura haber reportado los casos de amenaza ante la Unidad Nacional de Protección UNP, ante las cuales aseguran "**en muchas ocasiones la entidad no ha sido lo suficientemente diligente para atender el riesgo** al que están expuestos los reporteros", así como **el desmonte de esquemas de protección** a periodistas amenazados.

La FLIP, afirma que los trámites en la UNP no tienen la celeridad necesaria ante tales circunstancias, y se refiere puntualmente a casos  como el del periodista **Ricardo Ruidíaz**, en la región del Magdalena Medio, quien ha denunciado la trata de menores y las amenazas a líderes sociales, a quien le fue disminuido su esquema de protección.

En la alerta se menciona que en el caso de la reportera **Catalina Vásquez**, periodista y defensora de derechos humanos que trabaja en la Comuna 13 de Medellín, por el asesinato del que han sido víctimas algunas de sus fuentes cercanas, la entidad de protección no ha atendido el llamado de urgencia, lo que pone en riesgo su vida por el ejercicio de su labor.

También en Antioquia se menciona a la periodista **Jhanuaria Gómez**, quien tuvo que desplazarse del municipio de Segovia, por las amenazas que estaba recibiendo debido a sus investigaciones sobre acciones  irregulares alrededor de la actividad minera y su  labor con las comunidades a quienes enseña sobre las consecuencias que tiene para el ambiente; y en Putumayo el caso de la periodista **Laura Montoya**, quien el 6 de julio fue víctima de amenazas telefónicas mientras promovía una manifestación en Mocoa en rechazo al asesinato de líderes sociales.

En el caso de las tres periodistas, la FLIP  asegura que "**hubo agravantes por razones de género**", factores que considera la UNP debe tener en cuenta para garantizar la seguridad de las comunicadoras y sus familias. Finalmente, la organización solicita al gobierno se redoblen los esfuerzos para salvar la vida e integridad de los periodistas (Le puede interesar: [Procuraduría investiga participación de Fuerza Pública en asesinato de líderes sociales](https://archivo.contagioradio.com/54704/)).

###### Reciba toda la información de Contagio Radio en [[su correo]

######  
