Title: ¿PDET – Enfoque territorial de quién?
Date: 2017-12-21 11:11
Category: CENSAT, Opinion
Tags: acuerdos de paz, ambientalistas, Ambiente, PDET
Slug: pdet-enfoque-territorial-de-quien
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/12/imagen-edit_PDET.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: CENSAT 

#### **[Por: CENSAT Agua Viva - Amigos de la Tierra – Colombia](https://archivo.contagioradio.com/censat-agua-viva/)**

###### 22 Dic 2017 

*[“Es que nos están metiendo el caballo de troja y no nos damos cuenta”]*[dijo que día un líder campesino de una de las 16 regiones donde se está implementando los Programas de Desarrollo con Enfoque Territorial (PDET) y la verdad, es difícil refutar esta lectura]

[Los PDET son una propuesta de ordenamiento territorial, surgida del acuerdo de paz entre el gobierno nacional y las FARC, que se constituyen en una pieza central en la implementación de punto 1 del mismo. A partir de 8 pilares temáticos:]*[ordenamiento social de la propiedad rural y uso del suelo; reactivación económica y producción agropecuaria; educación rural; vivienda, agua potable y saneamiento; derecho a la alimentación; reconciliación, convivencia y paz; infraestructura y adecuación de tierras; salud rural]*[se pretende sentar las bases para la transformación estructural del campo. La entidad encargada de la implementación de esta propuesta es la Agencia de Renovación del Territorio (ART) conjuntamente con las organizaciones y comunidades que hacen parte de las 16 zonas priorizadas. Su tarea consiste en la formulación participativa de 16 Planes de Acción para la Transformación Regional (PATR) construidos y dialogados desde lo local, llevados a lo municipal para ser finalmente aprobados al nivel regional.]

[No fueron pocos los riesgos mencionados por activistas, quienes estudiaron y han participado en la propuesta de los PDET y en efecto la mayoría se convirtió en realidad, como lo evidenciaron recientemente líderes y lideresas en un encuentro sobre los PDET en la Universidad Nacional de Colombia, Sede Bogotá. Principalmente se observó falencias y dificultades en tres ámbitos: 1) lo metodológico con lo que responde a la realización de consultas previas en territorios étnicos, la participación incluyente, la construcción de herramientas y mecanismos de trabajo y los tiempos previstos; 2) lo administrativo, en cuanto a la creación de equipos impulsores de trabajo y la contratación de funcionarios; 3) lo temático cuando se desconoce propuestas locales como las de los Territorios Campesinos Agroalimentarios, las Zonas de Reserva Campesina y los territorios étnicos con sus propias formas de concebir y vivir el territorio. Una situación, que manifiesta de inmediato en la práctica lo que es el conflicto político-teórico de fondo: no hay consenso en la noción de territorio. ¿Es el territorio un espacio que requiere ser ordenado y desarrollado a través del ejercicio de las instituciones estatales o es el territorio un espacio vivo que contempla las múltiples concepciones de espacialidad y temporalidad que se transforman a diario en él o es el territorio una combinación de todo lo anterior y que pasa cuando se sobreponen las diversas] [cosmovisiones territoriales en un mismo espacio?]

[Ante esta discusión, de tener varias concepciones de territorio en disputa, vemos con creciente preocupación cómo la propuesta de los PDET se reduce a un ordenamiento territorial a partir de unidades administrativas, como son veredas y municipios, cuando sabemos que los territorios y las relaciones socio-ambientales que los conforman son dinámicos y están por encima de límites territoriales impuestos por el estado. ¿Qué pasaría si dejáramos atrás las divisiones administrativas y partiéramos de identidades y cosmovisiones territoriales que proponen otros relacionamientos con ríos, montañas y la naturaleza en general? Es evidente que las diversas concepciones de territorio son la base para todo tipo de intervención territorial y sobreponer cosmovisión sobre otra radica en un ejercicio de poder que llevaría a un aumento de conflictos territoriales en vez de la anhelada transformación estructural del campo. La cuestión en juego no es un simple ejercicio de ordenamiento administrativo y de infraestructura con determinado número de vías, escuelas y puestos de salud, no!, se trata de la identidad y la vida misma de quienes somos estos territorios. Es inconcebible que el estado después de la firma del acuerdo de paz busque por medio de la figura de los PDET desplegar con su visión desarrollista a toda velocidad un proyecto territorial a aquellos territorios que vienen construyendo sus propias agendas desde hace décadas.]

[Por esta razón nos unimos al llamado lanzado desde los territorios, de hacer una moratoria en la implementación de los PDET para revisar cada una de las críticas señaladas. Poner las diferentes cosmovisiones territoriales a dialogar y a construir consensos territoriales debería ser el primer paso antes de seguir pensando en la implementación y es el mínimo respeto que se debe ofrecer a las comunidades que han estado resistiéndose al conflicto armado construyendo propuestas de vida. Quizá pudo el estado evitar la discusión sobre el modelo económico durante las conversaciones de paz, pero cuando se llega del discurso al territorio es inevitable asumirla.]

#### [**Leer más columnas de CENSAT Agua Viva - Amigos de la Tierra – Colombia**](https://archivo.contagioradio.com/censat-agua-viva/)

------------------------------------------------------------------------

###### Las opiniones expresadas en este artículo son de exclusiva responsabilidad del autor y no necesariamente representan la opinión de Contagio Radio.
