Title: Incursión de tipo paramilitar deja tres muertos y nueve heridos en Argelia, Cauca
Date: 2018-01-22 14:37
Category: DDHH, Nacional
Tags: Argelia, Cauca
Slug: incursion_armada_paramilitar_argelia_cauca
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/01/paramilitares.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:  Juan Torres/ La razón] 

###### [22 Ene 2018] 

[Una incursión armada en El Plateado, en el municipio de Argelia, Cauca dejó tres personas muertas este domingo a  las 6:30 p.m. De acuerdo con la Comisión de Justicia y Paz un número aún indeterminado de hombres armados con capuchas y armas largas incursionaron en en el caserío del corregimiento y dispararon contra la población que se encontraba participando de las fiestas de carnaval en la plaza principal.]

**Lenin Gómez Samboni, Belén Suárez Tribillo y Carlos Muñoz** son las víctimas mortales del hecho, además de otros nueve heridos, entre los que se encuentran Robinson Yesid Castillo, Javier Cardoso, Ángel Martínez Muñoz y Segundo Javier Macias. También se habla de un niño de 6 años que habría sido víctima de las balas de los encapuchados.

### **Los hechos** 

[De acuerdo con Neida Muñoz, presidenta de la Junta de Acción Comunal de El Plateado, **la masacre sucedió mientras más de 3500 personas celebraban los carnavale**s y, “en una calle cerca donde estaba la población celebrando apareció un grupo de sicarios que acribillaron a tres personas. Una murió mientras era trasladada en ambulancia”.]

[Asimismo, afirman que algunos líderes de la comunidad intentaron pedirle a los hombres que se fueran del lugar, pero “luego de que los pobladores evacuaran los cuerpos sin vida y los heridos, **los armados que se autodenominaron de las Autodefensas Unidas de Colombia, impidieron el paso de personas hacia el casco urbano de Argelia”**, dice un informe de la Comisión de Justicia y Paz.]

### **En riesgo de desplazamiento forzado** 

Otra de las preocupaciones es que aún se no se sabe del paradero de muchas de las personas que salieron huyendo del lugar. Habitantes de la zona han expresado su temor por lo ocurrido, pues señalan que por el comportamiento de los hombres, parecía una incursión de tipo paramilitar, **ya que llegaron en camionetas pasando por varias veredas mientras disparaban y gritaban groserías a los pobladores, como lo cuentan los testigos.**

[Ante la incertidumbre y miedo constante en el que permanecen los habitantes, la comunidad informa que cerca de dos mil personas estarían pensado en desplazarse, ya que la presencia de hombres armados es permanente, y continúan pasando por las veredas amedrentando a la población no solo en El Plateado, sino también en El Mango y Los Picos.]

### **Las acciones de las autoridades** 

Ante el hecho, **las autoridades locales y la Fuerza Pública realizaron un consejo extraordinario de seguridad para analizar la situación**, y adoptar las medidas correspondientes, sin embargo, habitantes de El Mango, señalan que no ha llegado la personería ni la Defensoría del Pueblo.

[Por su parte, el alcalde de Argelia, Diego Aguilar, lamentó el hecho e informó que se desconoce la identidad del grupo armado. El mandatario también ha expresado que “en estos días había tranquilidad, no habíamos tenido conocimiento de alteración de orden público, excepto esto que acaba de suceder” sin embargo la versión de la población es otra.]

[El sábado anterior a la masacre, la comunidad denunció la presencia de aproximadamente 20 hombres encapuchados armados con fusiles, algunos con prendas militares; en la vereda Los Picos**. Según los pobladores decían que eran de la AUC y distribuyeron volantes anunciando acciones de  limpieza social.**]

[![065413c7-9c8e-4a3d-833d-1212922bc074](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/01/065413c7-9c8e-4a3d-833d-1212922bc074-450x600.jpg){.wp-image-50716 .size-large .aligncenter width="450" height="600"}](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/01/065413c7-9c8e-4a3d-833d-1212922bc074.jpg)

### **Difícil situación en el Cauca** 

[Además de los recientes hechos ocurridos en el fin de semana, el pasado jueves, en el casco urbano del municipio de Caloto, **sujetos armados asesinaron al joven Germán Andrés Ruíz Mera, de veinte años.** Las investigaciones intentan esclarecer si se trató un hecho relacionado con lo que algunos habitantes del municipio han señalado como "limpieza social".]

"Parece que se quiere amedrentar a los líderes. Hace tres meses fue asesinado el hermano de la consejera y hoy, el joven que falleció es sobrino de la mayor.", señalaba Edwin Güetio, consejero Mayor del Cauca.

En esta zona del departamento, también hay anuncios de llevar a cabo un plan de "limpieza social" por parte de las Autodefensas Gaitanistas de Colombia, que además vienen amenazando a líderes sociales. **"E**[**s una acción sistemática de asesinatos y señalamientos contra las comunidades (...) mientras que no hay señales de justicia ni esclarecimiento de los hechos**" indicaba Edwin Güetio.]

<iframe id="audio_23291225" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_23291225_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
