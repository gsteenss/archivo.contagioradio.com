Title: Solo quedan tres rinocerontes blancos en el mundo
Date: 2015-11-23 14:13
Category: Animales, Voces de la Tierra
Tags: 1989, animales en peligro de extinción, California, EE.UU, Nola, República Checa, rinoceronte blanco, zoológico de Dvur Kralove, zoológico de San Diego
Slug: solo-quedan-tres-rinocerontes-blancos-en-el-mundo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/11/Rinoceronte-blanco.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: [globovision.com]

###### [23 Nov 2015]

En la mañana de este lunes, el **zoológico de San Diego, California en **EE.UU, anunció la muerte de **Nola, una rinoceronte blanca de 41 años** de edad, cuya pérdida disminuye a tres la población animales de es**ta especie**.

Tras la muerte de Nola, en el mundo ahora quedan únicamente dos hembras y un macho que son protegidos y cuidados en una reserva en Kenya en África. "Nola deja un **legado que sus cuidadores y personal de cuidado animal (del zoológico) esperan continúe ayudando** a conservar la especie en los próximos años", expresa el comunicado  **zoológico.**

Una infección bacterial y otros problemas médicos por la avanzada edad, tenían a Nola hace más de una semana en **cuidados veterinarios. Según el comunicado del Zoológico, durante las **últimas 24 horas el estado de salud de la rinoceronte e**mpeoró, por lo que se le debió practicar eutanasia. "Estamos profundamente devastados por esta pérdida"**, expresó la entidad a donde llegó en 1989, desde República Checa la rinoceronte, que desde entonces se volvió el eje central de sus cuidadores que luchan por conservar esa especie.

Cabe recodar, que el pasado mes de julio el zoológico de Dvur Kralove, en la región central de República Checa, informó la muerte de otra rinoceronte blanca debido a compliaciones de salud producto de un quiste.

La amenaza más grande para los rinocerontes son los depredadores naturales y los cazadores furtivos quienes les cortan los cuernos para venderlos en el mercado negro.
