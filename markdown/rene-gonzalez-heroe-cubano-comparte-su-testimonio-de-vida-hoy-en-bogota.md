Title: Héroe cubano comparte su testimonio en el Congreso de la República
Date: 2015-11-03 15:22
Category: DDHH, Política
Tags: Cuba, René Gonzáles, tratado de extradición
Slug: rene-gonzalez-heroe-cubano-comparte-su-testimonio-de-vida-hoy-en-bogota
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/11/rene-gonzales.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: realcuba.wordpress.com 

###### [3 nov 2015]

[René González, uno de los cinco Antiterroristas cubanos que estuvo detenido en las cárceles de Estados Unidos durante más de 15 años, estará hoy en el Auditorio Luis Guillermo Vélez del Congreso de la República, en un acto en el marco de la campaña de solidaridad con Cuba, desarrollada por diversas organizaciones sociales colombianas.]

[González, ciudadano de Estados Unidos de nacimiento, fue detenido en Septiembre de 1998 y fue condenado a 15 años de prisión por supuestas actividades terroristas. Salió de prisión en 2011 pero fue obligado a permanecer en ese país bajo el condicionamiento de "libertad supervisada" y en 2013 pudo regresar a Cuba con la condición de renunciar a la ciudadanía estadounidense.]

[Desde ese entonces luchó por la libertad de sus 4 compañeros, que antes del restablecimiento de las relaciones diplomáticas entre la Cuba y Estados Unidos, recobraron su libertad, algunos de ellos luego de cumplir la totalidad de la pena que les fue impuesta de manera arbitraria y sin las condiciones del debido proceso.]

[El testimonio de René resulta imprescindible en el marco de la construcción de paz en Colombia, no solo por el apoyo que ha representado el pueblo y el gobierno de ese país, sino porque se convierte en un ejemplo de la reivindicación de los derechos de todo un pueblo y un ejemplo de dignidad en cuanto a las arbitrariedades de la justicia de EEUU, que mantiene en este momento a cientos de nacionales colombianos, según señala Isaías González, presidente de Sintradistritales.]

[Por otra parte, González visita Colombia en el marco de la campaña de solidaridad con Cuba, que sigue exigiendo y denunciando una serie de políticas como el bloqueo económico y las leyes de los "pies descalzos" que a pesar del restablecimiento de relaciones diplomáticas sigue violando los derechos del pueblo cubano.]

[El acto de solidaridad se realizará a  partir de las 6 de la tarde y contará con la presencia de la embajada cubana y el apoyo del representante a la cámara Alirio Uribe y el Senador Iván Cepeda, entre otros.]

 
