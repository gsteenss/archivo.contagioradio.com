Title: Deforestación aumentó un 23% en Colombia: IDEAM
Date: 2018-06-20 16:20
Category: Ambiente, Movilización, Nacional
Tags: Ambiente, conflicto armado, deforestación, IDEAM
Slug: la-deforestacion-a-nivel-nacional-aumento-al-23-ideam
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/06/EXTRACION-DE-MADERA.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [20 Jun 2018] 

El reporte presentado por el IDEAM sobre los resultados del monitoreo de la deforestación, evidenció que en 2017 **hubo ¨una pérdida total de bosque natural de 219.973 hectáreas"** porción de tierra que es incluso superior al área de Bogotá, que es calculada en unas 177.500 hectáreas.

Para Angélica Beltrán, ingeniera forestal e integrante de la Asociación Ambiente y Sociedad, los mas afectados "son los bosques de la Amazonía los que permiten hacer la captación de agua, la regulación hídrica y mantener de ahí en adelante toda la cuenca", de tal forma que l**a pérdida de bosque se traduce inmediatamente en una alteración del ciclo natural del agua.**

Además, Beltrán señaló que estas cifras van más allá de un número, dado que **“estamos poniendo en riesgo la supervivencia de nosotros mismos y de las especies que aún están en Colombia”.**

De otra parte, la ingeniera forestal destacó que en el área Andina, la deforestación se produce en lugares en los que nacen los ríos; hecho que pone en riesgo el propio abastecimiento de agua para regiones como la sabana de Bogotá.

### **Hay áreas específicas que deben ser protegidas** 

De acuerdo con los resultados del estudio, más del 80% de la deforestación tuvo lugar en 6 departamentos: Caquetá, Meta, Antioquia, Guaviare, Santander, Chocó y Putumayo; siendo las principales causas de la tala indiscriminada: "la praderización, la ganadería extensa, los cultivos de uso ilícito, el desarrollo de infraestructura vial, la extracción ilícita de minerales y la extracción de madera".

En virtud de ello, **Beltrán considera que existe una relación entre el aumento de la deforestación y el fin del conflicto armado** pues, siguiendo las conclusiones del informe la Amazonía (con 66,6% del total de deforestación), los Andes (con 17,8%), y el Pacífico colombiano (con 9,1%), son las regiones con mayor cambio de cobertura de bosque y en las que había presencia de conflicto armado.

### **Es necesario saltar a acciones concretas** 

El reporte marca una tendencia que va en aumento, y más grave aún, es la prueba de que hay niveles altos de deforestación en parques nacionales como por ejemplo, el Parque Nacional Natural Tinigua.

No obstante, como lo señala el informe del IDEAM, 1 de cada 10 hectáreas deforestadas se encuentra en áreas de Resguardos Indígenas, “identificando en términos generales la efectividad de este tipo de áreas para la conservación del bosque natural”.

Para finalizar, Angélica Beltrán pidió que una vez conocido el informe se tomen acciones concretas, por parte de los ministerios de Minas y Energía,  Ambiente y Agricultura puesto que, con estas cifras, Colombia aumenta su vulnerabilidad frente al cambio climático. (Le puede interesar: ["Se prenden las alarmas ante permisos para realizar Fracking en Colombia"](https://archivo.contagioradio.com/se-prenden-las-alarmas-ante-permisos-para-realizar-fracking-en-colombia/))

[Informe deforestación en colombia del IDEAM](https://www.scribd.com/document/382217707/Informe-deforestacion-en-colombia-del-IDEAM#from_embed "View Informe deforestación en colombia del IDEAM on Scribd") by [Contagioradio](https://www.scribd.com/user/276652802/Contagioradio#from_embed "View Contagioradio's profile on Scribd") on Scribd

<iframe id="doc_60274" class="scribd_iframe_embed" title="Informe deforestación en colombia del IDEAM" src="https://www.scribd.com/embeds/382217707/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-KeLab7DlqttLRDfSvVUU&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="1.7790927021696252"></iframe>  
<iframe id="audio_26641401" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_26641401_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
