Title: Palestina ya es miembro oficial de la Corte Penal Internacional
Date: 2015-04-02 14:01
Author: CtgAdm
Category: El mundo, Política
Tags: Israel suspende impuestos de Cisjordania, Palestina entra en la CPI, Palestina miembro de la Corte Penal Internacional
Slug: palestina-ya-es-miembro-oficial-de-la-corte-penal-internacional
Status: published

###### Foto:Lanacion.com 

Oficialmente, el **Estado Palestino** ya es **miembro de la Corte Penal Internacional**. Después de su adhesión al Estatuto de Roma, y de aceptar la jurisprudencia de la CPI el 13 de Junio de 2014,  por lo que Palestina podrá interponer **denuncias por crímenes de guerra** cometidos por **Israel en la invasión de Agosto de 2014**.

A pesar de las presiones de Israel y EEUU, para que no entrase en la CPI, el **ministro de exteriores palestino** ha declarado que "…desde hoy el mundo está cerca de poner fin a una larga era de la impunidad y la injusticia…", y que **"…este acuerdo nos acerca a nuestros objetivos compartidos de justicia y paz…"**.

**Israel** como forma de presión para que Palestina no entrara en la CPI ha **bloqueado todos los impuestos que recauda para la Autoridad Nacional Palestina**, dejando al actual gobierno de **Cisjordania en la bancarrota** y sumiendo a los palestinos en una crisis económica que en un futuro se va a traducir en una grave crisis humanitaria.

Para las organizaciones defensoras de Derechos Humanos, no es coincidencia que **Israel y EEUU** presionen para que Palestina no entre en la CPI, cuando ellos **no han firmado el Estatuto de Roma**.

**Amnistía Internacional** ha añadido que al ser miembro oficial de la CPI también **podrán ser juzgados milicianos de Hamas** por los ataques contra población civil israelí.

La primera denuncia que interpondrá el estado palestino será contra Israel por crímenes de guerra y de lesa humanidad en la intervención militar en Gaza en Agosto de 2014.

Hay que recordar que en la **última ocupación ilegal de Gaza** por parte de Israel**, murieron 2.000 civiles y hubo más de 10.000 heridos.** Se cometieron **crímenes de lesa humanidad**, que no fueron condenados debido al veto de EEUU y de los aliados de Israel ante Naciones Unidas.

**Tampoco Israel permitió entrar a los observadores de Derechos Humanos de la ONU** para poder realizar el informe sobre los sucedido en el última ocupación.
