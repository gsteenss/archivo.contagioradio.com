Title: 32 tanques inteligentes y protestas socio-ambientales
Date: 2015-01-28 12:44
Author: CtgAdm
Category: Opinion
Tags: 32 tanques, Ambiente, conflictos socio-ambientales, megaproyectos en colombia, ministerio de defensa
Slug: 32-tanques-inteligentes-y-protestas-socio-ambientales
Status: published

###### Foto: defensaterritorios 

[[Por Isabel Zuleta ]](https://archivo.contagioradio.com/isabel-cristina/)

Los crecientes conflictos socio-ambientales en Colombia han generado un mayor descontento social. Diferentes sectores, en algunas ocasiones de manera dispersa en otras de manera coordinada, han alzado su voz de protesta por los irreparables daños que sobre los bienes de la naturaleza, agua, bosques, páramos, fauna, flora y sobre las formas de vida de cientos de comunidades esta ocasionando el modelo capitalista extractivista por despojo en el que se empeña el Estado colombiano.

La abundante creatividad de los procesos ambientales y comunitarios en defensa del territorio para mostrar su rechazo a la política minero energética del país y sus megaproyectos de destrucción con marchas, carnavales, expediciones, festivales, abrazos a la montaña, recorridos, plantones, performance, cantos, trovas, poesías, bailes, duelos, actos simbólicos, recuperación de tierras, refugios humanitarios, debates en la plaza pública, campamentos de protesta en parques públicos, en terrenos de las multinacionales, en universidades, foros, congresos, seminarios y bloqueos a las empresas;  han tenido como respuesta constante del Estado y sus instituciones al servicio de las multinacionales primero la criminalización ahora el ataque directo. Toda esta creatividad y esfuerzo por mostrar su sentir en relación a las aguas, el territorio y la vida han sido fuertemente señalados, perseguidos, judicializados, amenazados y amedrentados principalmente por parte del Estado colombiano y el aparato privado de seguridad de las empresas. Hoy con la compra de los 32 tanques y en el escenario de un posible acuerdo con las guerrillas en Colombia es claro que van dirigidos a todo aquel que se oponga al despojo por megaproyectos de infraestructura como represas u otros considerados como de utilidad pública.

Según los datos de la pagina del Ministerio de Defensa quedan claras las características de estos 32 vehículos, no son de juguete, son de combate, no previenen, amenazan o matan. ¿En que radica la inteligencia de esta compra? “Proteger a la ciudadania de todo delito, además la protección del medio ambiente” afirma MiniDefensa. ¿Qué pueden entender los procesos de ciudadanos opositores a la construcción de vías, túneles, represas, explotaciones mineras? Los 8 X 8 llegaron con su inteligencia y la claridad que da el Estado sobre su uso para implementar la estrategia en contra de comunidades que se oponen a la destrucción de los territorios el miedo como arma.

El miedo como arma, su potencia radica en la efectividad. Ha sido usada para apagar, no solucionar la mayoría de protestas sociales en este país. En esto radica el poder de las 32 maquinas sumado al momento histórico por el que pasa el país. No dudo que las puedan usar, la duda seria haber tirado a la basura 84 millones de dólares, pero por ahora mientras el centro de la guerra esta en la confrontación con las guerrillas el miedo hacia los que protestamos y seguiremos protestando es necesario implantarlo y fortalecerlo antes de un clima de paz que permita evidenciar los cientos de conflictos socio-ambientales que tienen en crisis la ruralidad colombiana.
