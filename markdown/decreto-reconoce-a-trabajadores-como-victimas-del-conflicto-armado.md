Title: Decreto reconoce a trabajadores como Víctimas del Conflicto armado
Date: 2016-05-02 15:35
Category: Otra Mirada
Tags: Estigmatización, Reparación, SIndicatos, víctimas
Slug: decreto-reconoce-a-trabajadores-como-victimas-del-conflicto-armado
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/05/1-de-mayo-10-2016.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Gabriel Galindo] 

###### [02 Mayo 2016]

El Gobierno Nacional emitió el Decreto 624 de 2016 que reconoce que los trabajadores también han sido víctimas del conflicto armado y  merecen ser reparados a través de un mecanismo que será concretado entre voceros del sector de trabajadores y delegados de varios ministerios.

Según Fabio Arias, presidente de la Central Unitaria de Trabajadores (CUT),  este decreto llevaba dos años en un proceso de concertación con el gobierno y **reconoce la estigmatización y políticas de  exterminio hacía líderes obreros y organizaciones sindicales.** En los último 30 años han **asesinado a más de 3.000 dirigentes y activistas sindicales,  **a su vez se han recibido** más 18.000 acciones de violencia antisindical.**

La metodología que presentarán los sectores sindicales se conforma en 3 niveles: el primero es el movimiento obrero sindical en el que exigen que se deje de estigmatizar al mismo y se genere una **política pública del Estado en caminada a fomentar las libertades sindicales.** El segundo nivel se propone que c**ada sindicato exprese en que formas ha sido violentado** y finalmente el último nivel expone los derechos colectivos de los trabajadores.

A su vez el representante a la Cámara Alirio Uribe, asegura que este decreto es una ganancia del movimiento sindical y la reparación del mismo. El Representante dice que la estigmatización y persecución ha generado una **disminución en el ejercicio de las libertades sindicales que actualmente reporta solo un 5% de agremiación**.

<iframe src="http://co.ivoox.com/es/player_ej_11383452_2_1.html?data=kpagmpiYeZOhhpywj5WaaZS1lZeah5yncZOhhpywj5WRaZi3jpWah5yncafVw87cjabWrcLnjJKYpbq4cYarpJKw0dPYpcjd0JC%2Fw8nNs4yhhpywj5k%3D&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)]]
