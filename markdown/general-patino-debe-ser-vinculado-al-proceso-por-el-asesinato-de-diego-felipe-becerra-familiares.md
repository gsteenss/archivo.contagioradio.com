Title: General Patiño debe ser vinculado al proceso por el asesinato de Diego Felipe Becerra: Familiares
Date: 2018-01-18 17:17
Category: Uncategorized
Tags: “Trípido”, Diego Felipe Becerra, Policía Nacional
Slug: general-patino-debe-ser-vinculado-al-proceso-por-el-asesinato-de-diego-felipe-becerra-familiares
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/08/tripido6.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo Particular] 

###### [18 Ene 2018]

Tras seis años de búsqueda de justicia por el asesinato de Diego Felipe Becerra, a manos de la Policía Nacional y el encubrimiento del crimen por parte de oficiales y suboficiales, la Procuraduría General de la Nación, **sancionó con dos años de destitución a 6 integrantes de la institución por permitir la alteración de la escena del crimen, con el fin de ocultar el asesinato y la responsabilidad en los hechos**.

Se trata del Coronel José Nelson de Jesús Arévalo, el Teniente Rosemberg Madrid y el sub Intendente Juan Carlos Leal, los patrulleros Fredy Navarrete y Nelson Rodríguez, sin embargo, absolvió **al Coronel José Javier Vivas**, quien según la familia también es responsable dado que el material probatorio lo liga al proceso, debido a que estuvo en la escena del crimen, avaló la alteración y pretendió ocultar la evidencia en informes de prensa y contrainteligencia.

Para Gustavo Trejos, padre del joven de 16 años, esta decisión del ente de control es insuficiente puesto que, según él, no debió absolverse al Coronel Vivas, pero tampoco se tuvo en cuenta la participación de altos oficiales y suboficiales como el **General Francisco Patiño, Comandate de la Policía de Bogotá para el momento del asesinato**. El papá de Diego Felipe, también asegura que en los próximos pasos del proceso judicial tendría que vincularse a estos altos mandos.

Trejos aseguró que esta decisión, aunque incompleta es ejemplarizante y envía un mensaje tanto a las víctimas como a la Policía para que este tipo de situaciones no se repitan. “El mensaje que tenemos para las víctimas es que sigan trabajando para que la memoria de sus seres queridos no quede en la impunidad”. Además, reiteró que la tarea de las familias es demostrar que los policías **“también deben ser sancionados porque no pueden hacer uso del poder como lo han hecho”**.

** Diego Felipe Becerra fue asesinado el pasado 19 de agosto de 2011, cuando se encontraba realizando un grafiti en el puente de la av. Boyaca con calle 116**. En un primer momento se afirmó que el joven había participado en un robo a un bus que desencadenó la persecución, sin embargo, las versiones empezaron a trasformarse hasta que develaron el falso positivo.

Hay 13 personas implicadas, de las cuales tres son civiles y el resto policías. 9 de ellos han decidido contar la verdad incluyendo el testimonio del subintendente Giovanny Tovar quien el 18 de julio de 2016  reconoció que llevó el arma con la que pretendieron justificar el asesinato. (Le puede interesar: ["Justicia incompleta en el caso de Diego Felipe Becerra"](https://archivo.contagioradio.com/justicia-incompleta-caso-diego-felipe-becerra/))

<iframe id="audio_23243073" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_23243073_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
