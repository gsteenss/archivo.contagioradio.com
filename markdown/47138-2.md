Title: Organizaciones de víctimas y DDHH califican como positiva la elección de magistrados de la JEP
Date: 2017-09-26 12:13
Category: Nacional, Paz
Tags: Magistrados JEP, víctimas
Slug: 47138-2
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/FIRMA-DEL-ACUERDO-DE-PAZ-4-e1504831395382.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [26 Sept 2017] 

Luego de la publicación de los 51 nombres de personas elegidas como magistrados de la Jurisdicción Especial de Paz por parte del Comité de Escogencia, tanto organizaciones de Derechos Humanos, como de víctimas calificaron como positivo el listado y aseguraron que **es posible que se garanticen los derechos de las víctimas de la guerra, a pesar de las preocupaciones por la presencia de integrantes de la Justicia Penal Militar.**

<iframe src="https://co.ivoox.com/es/player_ek_21103921_2_1.html?data=k5aekpiddpKhhpywj5WbaZS1k52ah5yncZOhhpywj5WRaZi3jpWah5yncaLgw8rf1tSPncbkxt-SlKiPt9DW08qYzsaPqc3ZxMjWh6iXaaOnz5DRx5Cxpcjd1Nnfw8nTt4zVjNHOja-RaZi3jqjc0NnFq8rjjLfOxs7Tb46ZmKialg..&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

Alberto Yepez, integrante de la Coordinación Colombia Europa, plataforma que agrupa cerca de 200 organizaciones de DDHH en Colombia, Europa Estados Unidos; afirma que la elección ha sido plural "Es una lista plural, con personas de diferentes partes de la rama de la justicia" y agregó que **"La lista tuvo un importante avance respecto a la lista de los preseleccionados ". **(Le puede interesar: ["Las preocupaciones de las víctimas ante preseleccionados a la JEP"](https://archivo.contagioradio.com/organizaciones-sociales-cuestionan-listado-preseleccionado-para-la-jep/))

Por otra parte también señalan que **hay mucha esperanza en que la elección de los comisionados y comisionadas** de la verdad que serían elegidos la próxima semana, puesto que esa Comisión de la Verdad podría garantizar que se conozcan las versiones de las víctimas frente a la verdad oficial que ha sido predominante durante la guerra.

<iframe src="https://co.ivoox.com/es/player_ek_21104030_2_1.html?data=k5aekpmUd5Ghhpywj5WaaZS1k5Wah5yncZOhhpywj5WRaZi3jpWah5yncaLgxt3O0MnWpYy70NPnh6iXaaKlzcrnh5enb9qfzcaYy9LUs9PowtPQy8aPqMafzdTgjbLFq8rn1dfOxtSRaZi3jqjc0NnFq8rjjLfOxs7Tb46ZmKialg..&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

Acerca de la designación de Luz Marina Monzón como directora de la Unidad de Búsqueda de Personas desaparecidas, tanto Yepez como Alexandra González del MOVICE aseguraron que la postulación fue de todas las organizaciones por los tanto se podría rescatar que **el trabajo de esa oficina será acorde a la garantía de los derechos de las víctimas de más de 60 mil personas que se calculan son víctimas** de desaparición forzada.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
