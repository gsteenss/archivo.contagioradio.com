Title: Indígenas NASA continúan con la liberación de la Madre Tierra
Date: 2016-10-19 13:29
Category: DDHH, Nacional
Tags: ESMAD, Indigenas Nasa
Slug: indigenas-nasa-continuan-con-la-liberacion-de-la-madre-tierra
Status: published

###### [Foto: NasaComunidad] 

###### [19 de Oct 2016] 

El 14 de octubre, indígenas NASA se dirigieron hacia la hacienda Emperatriz para continuar con su ejercicio de liberación de la Madre Tierra, posteriormente disponibles del ESMAD llegaron al lugar para desalojarlos, **hecho que produjo un enfrentamiento en el que murió un agente del ESMAD y resultaron heridos varios indígenas.**

De acuerdo con la Consejera NASA, Luz Eida, **los indígenas no poseían ningún arma de fuego que generara heridas como las ocasionadas al policía del ESMAD**, sin embargo asegura que las autoridades NASA ya dieron inicio a la investigación para esclarecer cómo murió este agente.

“De igual manera hemos llegado a la conclusión  que **hay personas que se encuentran en contra de este proceso de liberación de la Madre Tierra e incluso hemos denunciado que miembros de la Fuerza Pública se infiltran** en los ejercicios de la movilización NASA, con el objetivo de deslegitimar el proceso que venimos realizando” afirmo la consejera.

En este mismo sentido, los indígenas denuncian que **se araron 30 hectáreas de cultivos de maíz y frijol en la Hacienda Emperatriz, que ya estaban listos para cosechar, con tractores después del desalojo por parte del ESMAD** y señalan que esta no es la primera vez en que la Fuerza Pública acaba con sus alimentos.

Estas tomas, que realizan los indígenas, hacen parte de la recuperación de sus territorios ancestrales y se lleva a cabo en zonas del Cauca como Corinto, Santander de Quilichao, López Adentro, Las Delicias y La Esperanza. Durante estos ejercicios miembros de la comunidad ingresan a las fincas, trabajan la tierra y una vez aparece la fuerza pública, los indígenas se retiran del lugar para evitar confrontaciones. Lea también: ["La resistencia de los Pueblos Indígenas apunta hacia la paz"](https://archivo.contagioradio.com/la-resistencia-de-los-pueblos-indigenas-apunta-hacia-la-paz/)

Sin embargo, estas tomas siempre **han significado un riesgo para la vida de los indígenas, en 1991, se llevó a cabo la masacre del Nilo**, hecho en que militares en connivencia de paramilitares asesinaros a más de 20 indígenas que se encontraban retomando sus territorios ancestrales, como acto reparador **el entonces presidente Samper, ofreció entregar a la comunidad NASA previos, entre ellos la Hacienda La Emperatriz. **Lea también: ["Indígenas y Campesinos del país exigen participación directa en pacto político Nacional"](https://archivo.contagioradio.com/victimas-del-pais-exigen-participacion-directa-en-pacto-politico-nacional/)

###### Reciba toda la información de Contagio Radio en su correo [[bit.ly/1nvAO4u]
