Title: Catarsis Recíproca 
Date: 2019-09-13 16:05
Author: Deborah
Category: Columnistas, Opinion
Tags: conflicto armado, Opinión, víctimas, Víctimas conflicto armado
Slug: catarsis-reciproca
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/09/WhatsApp-Image-2019-09-13-at-14.46.20.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

Hay una línea muy delgada para establecer los límites en cuanto a las opiniones que se generan sobre víctimas y victimarios en el conflicto armado en Colombia, esta línea tiende a ser delgada por que es muy fácil opinar sin tener el conocimiento o la experiencia que legitimen esa opinión.

Que montón de opiniones diversas las que existen en este rebaño llamado Colombia. Cualquiera diría que es libre de decir lo que quiera considerando su percepción, pero no hay que olvidarse del hecho de que opinar sobre la violencia ejercida en otras personas no deja de ser un tema bien complejo porque involucra sentimientos y emociones que pueden llegar a resquebrajar la piel y hasta los huesos mismos, pasar del plano interno que algunos socavan para no mostrar y por consiguiente abrir heridas que los afectados han sanado o están en proceso de hacerlo .

Créanme, no hay nada más difícil que atravesar por un trauma de la violencia y sentir que al otro al que se le cuenta sobre el hecho, reacciona con opiniones que están lejos del respeto o incluso que rayan en la indiferencia abrumadora y silenciosa, caracterizada por el desinterés. Eso es una bofetada certera que incrementa la agonía de no ser entendido y lo deja a uno sumido en una profunda depresión porque preferimos no abrir nuestro corazón por el temor a ser juzgados por esas "opiniones" de alguien que jamás ha vivido alguna experiencia de este tipo en su vida.

El primer paso para lograr un dialogo directo con cualquier víctima de violencia, sea cual sea el índole de la misma, radica en escuchar, en suavizar el oído para digerir cada palabra con la integridad e importancia que le da quien cuenta. El dialogo, muchas veces, puede no ser mutuo, claramente la víctima necesita desahogarse del sinsabor para expresar lo que siente y tal vez lo único que necesita es ser escuchada, entendida y por qué no... valorada por su lucha incesante; el arte de escuchar consiste en dejar que fluyan las emociones y abrir la mente para tratar de entender, de ver, incluso de sentir y ponerse en los zapatos del otro para lograr experimentar así sea mentalmente el dolor que el otro ha sentido.

¿Quién no logra sobrellevar sus cargas cuando tiene la ayuda de quienes mas aprecia? exactamente eso es lo que debemos hacer con las personas que sufren la ausencia de alguien arrancado por la arbitrariedad de la violencia; ayudarles a alivianar su carga por lo menos aportando herramientas emocionales basadas en el apoyo moral que ellos tanto necesitan en este mar de indiferencia. Que diferencia es mirar a los ojos, escuchar más allá de oír, abrazar en lugar de un simple apretón de manos, salir a marchar por la causa de sus familias en vez de quedarnos en casa y mirar, que diferente es compartir visibilizando y sensibilizando a nuestro entorno, en redes sociales, en donde sea, en lugar de ser indiferentes.

La raíz de todo cambio comienza por nosotros mismos, por eso creo que solidarizarnos con quienes necesitan nuestro apoyo emocional es el primer paso para cambiar nuestra sociedad colombiana que tanto se ha acostumbrado a rentar su consciencia y alejarse de la realidad cerrando los ojos al dolor.

La próxima vez que nos enfrentemos ante un diálogo con una de las víctimas... abramos el corazón de par en par.

Que sea bienvenida la bondad... y que se quede.
