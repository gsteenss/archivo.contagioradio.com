Title: "Van por nuestras tierras": Informe entregado a la JEP sobre despojos en Urabá
Date: 2018-12-11 16:00
Author: AdminContagio
Category: Nacional, Paz
Tags: Chocó, Comisión de Justicia y Paz, despojo de tierras, Informe a la JEP, JEP, uraba
Slug: van-por-nuestras-tierras-informe
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/12/Captura-de-pantalla-2018-12-11-a-las-3.49.37-p.m.-770x400-1.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: @Justiciaypazcol] 

###### [11 Dic 2018] 

Con esperanza de verdad y justicia, comunidades del Bajo Atrato y el Eje Bananero, entregaron el informe **"Van por nuestras tierras", en el que dan cuenta de operaciones militares y paramilitares desarrolladas en la región de Urabá entre 1995 y 2016**, que provocaron desplazamientos masivos, desapariciones, y el posterior ingreso de grandes empresas a los territorios. (Le puede interesar: ["Cinco casos por los que la JEP pone la lupa a crímenes en Urabá y Bajo Atrato chocoano"](https://archivo.contagioradio.com/cinco-casos-los-la-jep-pone-la-lupa-crimenes-uraba-atrato-chocoano/))

Las comunidades entregaron a la **Jurisdicción Especial para la Paz (JEP)**, un informe en el que denuncian actuaciones de terceros y agentes del Estado que estarían involucrados en crímenes de lesa humanidad; con el apoyo de la Fundación Forjando Futuros, el Instituto Popular de Capacitación, la Corporación Jurídica Libertad, la Comisión Intereclesial de Justicia y Paz, y gracias a la asesoría del Centro Internacional para la Justicia Transicional.

**Danilo Rueda, integrante de la Comisión Intereclesial de Justicia y Paz,** manifestó que el informe fue preparado con base en el acompañamiento que hace esta Institución desde hace más de 20 años en el Bajo Atrato. Rueda señaló que en la zona se encuentran comunidades negras, indígenas y campesinas, que manifiestan que la constante en la **zona son los desplazamientos forzados, provocados por operaciones coordinadas entre la fuerza pública y estructuras paramilitares**.

De acuerdo al documento, el número de tierras despojadas estaría cercano al millón de hectáreas, indicando que **el 70% de los casos ha sido producto del accionar paramilitar**. Sin embargo, lejos de ser un problema del pasado, líderes de las comunidades que llegaron a Bogotá para entregar el informe a la JEP, denuncian que el peligro se mantiene, siendo el asesinato y amenaza contra los integrantes de procesos organizativos, algunos de los problemas que enfrentan.

Según estos líderes, tanto el Chocó como la zona bananera de Antioquía, son territorios disputados  por su riqueza natural, que además de oro y coltán, son adecuadas para la siembra de Palma de Aceite y Banano, así como para la ganadería. Esta situación ha causado que **tras las operaciones militares y paramilitares, empresas ingresen a los territorios, como lo ha evidenciado la Fiscalía, para hacer uso de estos terrenos**.

El uso de la tierra para monocultivos y ganadería extensiva significan un riesgo mismo para el territorio, en tanto estas formas de uso de la tierra **provoca erosión, así como el secado de fuentes hídricas**. Adicionalmente, ambos procesos se suman a la deforestación, necesaria para liberar tierras con el fin de poder desarrollar estas actividades, y por esa vía, a la perdida de biodiversidad en especies animales y vegetales.

Como lo resaltaron los líderes de las comunidades que entregaron el informe, con el documento también quieren mostrar que Urabá "tiene todas las instituciones", pero no tienen garantizado el acceso a la justicia, adicionalmente en la región hay problemas para implementar la Ley 1448 de Víctimas,  la Ley 975 de Justicia y Paz, así como el mismo Acuerdo de Paz.  (Le puede interesar: ["Palmera Poligrow consume 1'700.000 litros de agua diarios en cada plantación en Mapiripán"](https://archivo.contagioradio.com/audiencia-en-congreso-de-la-republica-por-accion-de-poligrow-en-mapiripan/))

### **"Dada la impunidad estructural, esperamos que las víctimas puedan ser escuchadas por la JEP"** 

Danilo Rueda dijo que dada la impunidad estructural que impide el acceso a la justicia por parte de las víctimas, esperan ser escuchados ante la JEP en el marco del auto 040 con el que se abrió el caso 004, "por hechos constitutivos de graves violaciones del derecho internacional de los derechos humanos y del Derecho Internacional Humanitario en la región de Urabá entre el primero de enero de 1986 y el primero de diciembre de 2016.

Rueda manifestó que además de los relatos de las víctimas, **el informe contiene los indicios y elementos de juicio para que la JEP llame a personas, que como agentes del Estado, "participaron en la comisión de graves crímenes de lesa humanidad y crímenes de guerra"**, De tal forma que esta Justicia tendrá que escucharlos, abocar conocimiento del caso y tomar una decisión en derecho. (Le puede interesar: ["Colombia registró más de 90 mil desplazamientos forzados en 2017"](https://archivo.contagioradio.com/colombia-registro-mas-de-90-mil-desplazamientos-forzados-en-2017/))

> Acá en Bogotá víctimas entregaban informe a [@JEP\_Colombia](https://twitter.com/JEP_Colombia?ref_src=twsrc%5Etfw) sobre desplazamiento y despojo 1995- 2016, en sus territorios Bajo Atrato y Eje Bananero se unían en una misma alma: "Tenemos esperanza habrá verdad" [@ipcoficial](https://twitter.com/ipcoficial?ref_src=twsrc%5Etfw) [@forjandofuturos](https://twitter.com/forjandofuturos?ref_src=twsrc%5Etfw) [@CorpoJuridicaLi](https://twitter.com/CorpoJuridicaLi?ref_src=twsrc%5Etfw) [@Justiciaypazcol](https://twitter.com/Justiciaypazcol?ref_src=twsrc%5Etfw) [@el\_ICTJ](https://twitter.com/el_ICTJ?ref_src=twsrc%5Etfw) [pic.twitter.com/AhnKVOibAm](https://t.co/AhnKVOibAm)
>
> — Comisión Justicia y Paz (@Justiciaypazcol) [11 de diciembre de 2018](https://twitter.com/Justiciaypazcol/status/1072340914880856064?ref_src=twsrc%5Etfw)

<p>
<script src="https://platform.twitter.com/widgets.js" async charset="utf-8"></script>
</p>
###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
