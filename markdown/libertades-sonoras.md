Title: Libertades Sonoras
Date: 2017-03-14 11:43
Author: AdminContagio
Slug: libertades-sonoras
Status: published

### LIBERTADES SONORAS

[![¿Por qué las mujeres nos deberíamos sentir orgullosas si nos dicen brujas?](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/11/brujas-e1511982423890.jpg "¿Por qué las mujeres nos deberíamos sentir orgullosas si nos dicen brujas?"){width="756" height="500" sizes="(max-width: 756px) 100vw, 756px" srcset="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/11/brujas-e1511982423890.jpg 756w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/11/brujas-e1511982423890-300x198.jpg 300w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/11/brujas-e1511982423890-370x245.jpg 370w"}](https://archivo.contagioradio.com/mujeres_brujas_inquisicion/)

###### [¿Por qué las mujeres nos deberíamos sentir orgullosas si nos dicen brujas?](https://archivo.contagioradio.com/mujeres_brujas_inquisicion/)

[<time title="2017-11-29T14:09:36+00:00" datetime="2017-11-29T14:09:36+00:00">noviembre 29, 2017</time>](https://archivo.contagioradio.com/2017/11/29/)Hablamos sobre la época de la inquisición para reivindicar la memoria de las nueve millones de mujeres que fueron asesinadas al ser tildadas de brujas[LEER MÁS](https://archivo.contagioradio.com/mujeres_brujas_inquisicion/)  
[![¿Qué es el cyberfeminismo?](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/11/cyberfeminismo-897x550.png "¿Qué es el cyberfeminismo?"){width="897" height="550"}](https://archivo.contagioradio.com/que_es_cyberfeminismo/)

###### [¿Qué es el cyberfeminismo?](https://archivo.contagioradio.com/que_es_cyberfeminismo/)

[<time title="2017-11-15T07:47:38+00:00" datetime="2017-11-15T07:47:38+00:00">noviembre 15, 2017</time>](https://archivo.contagioradio.com/2017/11/15/)¿Qué es eso del cyberfeminismo y la seguridad digital? ese fue el tema que hablamos en \#LibertadesSonoras[LEER MÁS](https://archivo.contagioradio.com/que_es_cyberfeminismo/)  
[![El papel de las mujeres en la Jurisdicción Especial para la paz](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/09/Monzon-JEP-e1506709847691.jpg "El papel de las mujeres en la Jurisdicción Especial para la paz"){width="600" height="500" sizes="(max-width: 600px) 100vw, 600px" srcset="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/09/Monzon-JEP-e1506709847691.jpg 600w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/09/Monzon-JEP-e1506709847691-300x250.jpg 300w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/09/Monzon-JEP-e1506709847691-370x308.jpg 370w"}](https://archivo.contagioradio.com/mujeres-jep/)

###### [El papel de las mujeres en la Jurisdicción Especial para la paz](https://archivo.contagioradio.com/mujeres-jep/)

[<time title="2017-10-29T18:00:57+00:00" datetime="2017-10-29T18:00:57+00:00">octubre 29, 2017</time>](https://archivo.contagioradio.com/2017/10/29/)Foto: The International Center for Transitional Justice 26 Oct 2017 Las mujeres escogidas como magistradas en los tribunales de la Jurisdicción Especial para la Paz, representan el 53% de las y los magistrados seleccionados por el Comité de Escogencia, justamente ese papel de las mujeres en la JEP así como también en la construcción de la...[LEER MÁS](https://archivo.contagioradio.com/mujeres-jep/)  
[![Las resistencias en América Latina desde la interrupción voluntaria del embarazo](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/09/aborto.jpg "Las resistencias en América Latina desde la interrupción voluntaria del embarazo"){width="800" height="530" sizes="(max-width: 800px) 100vw, 800px" srcset="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/09/aborto.jpg 800w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/09/aborto-300x199.jpg 300w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/09/aborto-768x509.jpg 768w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/09/aborto-370x245.jpg 370w"}](https://archivo.contagioradio.com/resistencias_america_latina_aborto/)

###### [Las resistencias en América Latina desde la interrupción voluntaria del embarazo](https://archivo.contagioradio.com/resistencias_america_latina_aborto/)

[<time title="2017-09-27T21:00:42+00:00" datetime="2017-09-27T21:00:42+00:00">septiembre 27, 2017</time>](https://archivo.contagioradio.com/2017/09/27/)En el marco del Día de Acción Global por un Aborto Legal, Seguro y Gratuito, en \#LibertadesSonoras hablamos sobre la situación de la interrupción voluntaria del embarazo en América Latina[LEER MÁS](https://archivo.contagioradio.com/resistencias_america_latina_aborto/)  
[![ABC de la despenalización del aborto en Colombia](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/09/Aborto-1920x550.jpg "ABC de la despenalización del aborto en Colombia"){width="1170" height="335"}](https://archivo.contagioradio.com/despenalizacion-aborto-en-colombia/)

###### [ABC de la despenalización del aborto en Colombia](https://archivo.contagioradio.com/despenalizacion-aborto-en-colombia/)

[<time title="2017-09-20T17:15:45+00:00" datetime="2017-09-20T17:15:45+00:00">septiembre 20, 2017</time>](https://archivo.contagioradio.com/2017/09/20/)En \#LibertadesSonoras hablamos con dos invitadas sobre las sentencias, los obstáculos y jurisprudencia en el país.[LEER MÁS](https://archivo.contagioradio.com/despenalizacion-aborto-en-colombia/)  
[![Mujeres Indígenas del mundo siguen luchando por sus derechos](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/09/5-de-septiembre-1420x550.jpg "Mujeres Indígenas del mundo siguen luchando por sus derechos"){width="1170" height="453"}](https://archivo.contagioradio.com/mujeres-indigenas-luchan-por-sus-derechos/)

###### [Mujeres Indígenas del mundo siguen luchando por sus derechos](https://archivo.contagioradio.com/mujeres-indigenas-luchan-por-sus-derechos/)

[<time title="2017-09-13T16:58:49+00:00" datetime="2017-09-13T16:58:49+00:00">septiembre 13, 2017</time>](https://archivo.contagioradio.com/2017/09/13/)En \#LibertadesSonoras quisimos hacer memoria de las luchas y rendir un homenaje a todas las mujeres indígenas del mundo[LEER MÁS](https://archivo.contagioradio.com/mujeres-indigenas-luchan-por-sus-derechos/)
