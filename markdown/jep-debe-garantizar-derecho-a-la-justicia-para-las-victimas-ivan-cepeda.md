Title: Modificaciones en la JEP violarían el Acuerdo de Paz
Date: 2017-08-02 15:06
Category: Entrevistas, Paz
Tags: acuerdos de paz, Jurisdicción Espacial de Paz
Slug: jep-debe-garantizar-derecho-a-la-justicia-para-las-victimas-ivan-cepeda
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/10/vigilia-por-la-paz.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [02 Ago 2017] 

Congresitas han denunciado que el proyecto presentado por el gobierno para la Jurisdicción Especial para la Paz, tendría diferentes micos que **disminuiría las sanciones a terceros responsables en el conflicto armado y debilitaría** la cadena de mando al interior de las Fuerzas Militares, dejando de lado la posibilidad de obtener justicia para las víctimas del conflicto armado.

De acuerdo con el senador Iván Cepeda, esta no es la primera vez que alertan sobre las modificaciones que podría sufrir la JEP, “nosotros creemos que es imprescindible que la Jurisdicción Especial para la Paz obedezca al espíritu con la que fue creada y **eso es una jurisdicción al servicio de las víctimas, al servicio de la verdad, al servicio de la justicia en Colombia**”. (Le puede interesar:["Víctimas se entierran para exigir búsqueda de los desaparecidos"](https://archivo.contagioradio.com/victimas-se-enterraran-en-bogota-para-exigir-la-busqueda-de-los-desaparecidos/))

Entre los cambios que más preocupan al senador del Polo Democrático se encuentra el debilitamiento de la cadena de mando de las Fuerzas Militares, la responsabilidad de terceros en el conflicto armado, **la participación de las víctimas en todas las instancias de la jurisdicción y en consecuencia que no haya derecho a la verdad, justicia y reparación**.

En cuanto a los tiempos en los que podría ser aprobada la JEP, el senador señaló que tendría que ser lo más pronto posible, esto debido a que el proceso para escoger a los magistrados que harán parte del Tribunal de paz ya inició, **“los magistrados de la jurisdicción deben entrar a trabajar cuanto antes, así lo están reclamando millones de víctimas en Colombia”** afirmó Cepeda. (Le puede interesar: ["En septiembre se conocerán los nombres de los magistrados del Tribunal de Paz"](https://archivo.contagioradio.com/en-septiembre-se-conoceran-los-nombres-de-magistrados-del-tribunal-de-paz/))

El senador manifestó que las víctimas del país tienen muy claro que sus derechos deben “conquistarse” y que el Estado solamente opera en defensa y protección de los mismos, cuando las personas se movilizan y organizan en el reclamo de sus derechos, **razón por la cual las víctimas se encuentran en un permanente proceso de exigencia y reclamo para que se cumpla la necesidad de ser reparadas**.

<iframe id="audio_20135663" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_20135663_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
