Title: Demandan reforma tributaria por una menstruación con 0% impuestos
Date: 2017-05-18 13:37
Category: Libertades Sonoras, Mujer
Tags: Menstruación, mujeres, Reforma tributaria
Slug: demandan-reforma-tributaria-por-una-menstruacion-con-0-impuestos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/12/iva-a-toallas-higienicas.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [18 mayo 2017]

Luego de que el Equipo de Género y Justicia Económica de la Red por la Justicia Tributaria, lograra reducir 14 puntos del IVA sobre los productos de primera necesidad como toallas higiénicas, tampones y productos de aseo personal, **realizarán una demanda por inconstitucionalidad a la reforma tributaria para lograr que el impuesto sea del 0%.**

### **"Pagar impuestos por ser mujeres y menstruar, nos parece regresivo y sexista”** 

Natalia Moreno, investigadora, aseguró que la demanda está pensada para ser radicada el 15 de junio. Con el apoyo de la Corte Constitucional, el Equipo de Género espera que “se puedan materializar las luchas que hemos hecho para evitar que sigamos pagando más de 100 mil millones en impuestos por ser mujeres”.  **Para la investigadora lo más importante es que las mujeres se movilicen en torno a esta acción jurídica.** Le puede interesar: ["Por una menstruación libre de impuestos no a la reforma tributaria"](https://archivo.contagioradio.com/por-una-menstruacion-libre-de-impuestos-no-a-la-reforma-tributaria/)

De igual modo manifestó que al hecho de que la mujeres tengan que pagar impuestos por estos productos, se suma a que **“estamos en un país donde la brecha salarial entre hombres y mujeres es del 20%,** donde las mujeres trabajan más de 13 horas al día en labores remuneradas y no remuneradas y donde el desempleo femenino supera 13%”.

### **Los costos de ser mujer** 

En promedio, una mujer usa 5 toallas en un día durante 5 días que dura aproximadamente la menstruación. Esto serían 25 toallas cada 13 periodos que tienen las mujeres al año, es decir 325 toallas que a \$500 cada una, **da un total de \$162.500.** En Colombia hay 13 millones de mujeres en el período reproductivo que demandan dichos artículos.

Este 18 de mayo, las mujeres que están impulsando la campaña “logramos el 5% vamos por el 0%”, han extendido la invitación a la presentación de la nueva etapa de “Menstruación libre de impuestos”. **El encuentro se llevará a cabo en Bogotá desde las 5:30 pm en las instalaciones de FESCOL.**

<iframe id="audio_18774964" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_18774964_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
