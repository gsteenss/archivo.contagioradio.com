Title: El racismo permanece en las instituciones del Estado: CRIC
Date: 2020-05-21 19:57
Author: CtgAdm
Category: Actualidad, DDHH
Tags: Cauca, CRIC, racismo
Slug: el-racismo-permanece-en-las-instituciones-del-estado-cric
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/asesinato-de-lider-indigena-en-tacueyó.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: CRIC

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

El **Consejo Regional de Indígenas del Cauca, (CRIC)**, dio a conocer un audio al cierre de una conversación que sostenían contratistas y del Ministerio de las TIC con integrantes del equipo de comunicaciones del CRIC, los funcionarios del Gobierno quienes sin saberlo, dejaron los micrófonos abiertos refiriéndose a los pueblos ancestrales con insultos, frases discriminatorias y racistas, hechos que para los grupos étnicos revelan la naturaleza de la administración Duque.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

"*Ellos nunca van a cambiar y van a ser miserables toda su vida (…). “Que tal esos hijue... solo quieren plata y siempre van a ser unos miserables y brutos. No vamos a ceder ni en un 20% de sus pretensiones. En este momento ya me importa un culo, quieren casas, se las hago pues, con esos \$300 mil lo que van a hacer es comprar unas flechas y unas tarjetas como dotación por punto de resguardo*", son algunas de las intervenciones que se escuchan en la grabación.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Joe Sauca, coordinador de DD.HH. del [CRIC](https://twitter.com/CRIC_Cauca)se refirió a la situación señalando que es una denigrante conversación y un reflejo de cómo piensan funcionarios que son parte del gobierno del presidente Iván Duque, "sabemos que es un tema histórico que no ha desaparecido y que permanece en las instituciones del Estado pues ese trato también se representa en la desatención de muchas de las instituciones". [(Lea también: Indígenas denuncian que Ejército no ha sido solución, sino parte del problema en Cauca)](https://archivo.contagioradio.com/indigenas-denuncian-que-ejercito-no-ha-sido-solucion-sino-parte-del-problema-en-cauca/)

<!-- /wp:paragraph -->

<!-- wp:core-embed/facebook {"url":"https://www.facebook.com/contagioradio/videos/615880669009986/?__xts__[0]=68.ARA3Z1Qc2g08sfGc-Zjag9rX-kqUNyr1sQlp1BCklKo8ZyARj2blMnZCqCQ2j1iO_wsjt9Of7KyVjDcJqAw6XdVKPRFI3BvuaoGVZ3aKroD73jqIF2JlDqBBnBgEJuUJ9nwKikfjT1jExHilZfyVOhp_mh8onpNhgfFHmUeIuTqEWFX33dGWOuWirhPI6uaxFJjRwkodX8UsYf5xc6VdKEQk_hEJpxmi4stZ1YB5ww2skl-tTTthSZ-iNer6IzKj6dru2oQpfD7HsN9LX-fA6_5X86y-qvCGSLK4tsZzmjp7-0207y6XgMF5AyvLzDQpT7kyeFIaKch10JYcQGgQA3Wrilz3_Fz0\u0026__tn__=-R","type":"video","providerNameSlug":"facebook","className":""} -->

<figure class="wp-block-embed-facebook wp-block-embed is-type-video is-provider-facebook">
<div class="wp-block-embed__wrapper">

https://www.facebook.com/contagioradio/videos/615880669009986/?\_\_xts\_\_\[0\]=68.ARA3Z1Qc2g08sfGc-Zjag9rX-kqUNyr1sQlp1BCklKo8ZyARj2blMnZCqCQ2j1iO\_wsjt9Of7KyVjDcJqAw6XdVKPRFI3BvuaoGVZ3aKroD73jqIF2JlDqBBnBgEJuUJ9nwKikfjT1jExHilZfyVOhp\_mh8onpNhgfFHmUeIuTqEWFX33dGWOuWirhPI6uaxFJjRwkodX8UsYf5xc6VdKEQk\_hEJpxmi4stZ1YB5ww2skl-tTTthSZ-iNer6IzKj6dru2oQpfD7HsN9LX-fA6\_5X86y-qvCGSLK4tsZzmjp7-0207y6XgMF5AyvLzDQpT7kyeFIaKch10JYcQGgQA3Wrilz3\_Fz0&\_\_tn\_\_=-R

</div>

</figure>
<!-- /wp:core-embed/facebook -->

<!-- wp:paragraph {"align":"justify"} -->

Los hechos se dieron en medio de un plan de contingencia que presentaba el CRIC para atender la situación actual frente al Covid-19, que "viene de un esfuerzo de las autoridades indígenas, la guardia y los equipos de salud de tener menos de 70 casos, ha sido el aislamiento pero hoy nos encontramos en inminente riesgo y ese plan se presentó con un presupuesto que implica que el Gobierno garantice la política de salud. [(Lea también: Tortura contra coordinador indígena José Camayo retrata la aguda crisis del Cauca)](https://archivo.contagioradio.com/tortura-contra-coordinador-indigena-jose-camayo-retrata-la-aguda-crisis-del-cauca/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

"Esta es la respuesta más clara a las mingas que se han realizado y a los acuerdos incumplidos por el Gobierno" se juega con la voluntad con los compromisos, la palabra y la voluntad del pueblo, si eso pasa con nosotros, qué decir de otros sectores de Colombia, sindicalistas, campesinos, afros y diferentes gremios que siempre han reclamado sus derechos frente a la desigualdad que vive el país. [(Le puede interesar: ONIC: 40 años de resistencia histórica)](https://archivo.contagioradio.com/onic-40-anos-trabando-por-la-vida-y-los-territorios-de-los-pueblos-indigenas/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

De las tres voces que se escuchan en el audio, se han conocido los nombres de dos funcionarios, Juan Carlos Pulido Fernández **y Alejandro Plata Peña de quien otros medios señalan tuvo contratos por \$54 millones en 2017, de \$108 millones en 2018 y de \$101 millones en 2019, y quien habría** renunciado tras los hostigamientos.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Desde el Ministerio del Interior se pidió una copia de la grabación para investigar los hechos, el viceministro para la Participación e Igualdad de Derechos del Ministerio del Interior, Carlos Baena, expresó que denunciará la situación ante la Procuraduría, pese a ello, lo sucedido, señala Sauca ha quebrado la confianza que existía.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

**"La discriminación y el racismo son formas de violencia y están prohibidas por la declaración de Derechos Universales y la Constitución"**, afirma el coordinador, quien señala que no basta con que renuncien los funcionarios sino que el Gobierno debe debe pedir perdón y excusas ante el país y las comunidades ancestrales. [(Le recomendamos leer: Nombramiento de Jorge Tovar, una burla a la dignidad y memoria de las víctimas)](https://archivo.contagioradio.com/nombramiento-de-jorge-tovar-una-burla-a-la-dignidad-y-memoria-de-las-victimas/)

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### El CRIC continúa tomando medidas contra el Covid-19

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

En medio de la pandemia del Covid-19 que ha llevado al país a registrar más de 17.000 casos, Sauca destaca la labor de las comunidades indígenas del Cauca que han logrado, gracias al aislamiento y las medidas tomadas, reducir los contagios a menos de 70, además de compartir más de 5.000 kits para que las localidades más vulnerables tengan acceso a alimentos.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

"Partiendo de lo que dicen estos funcionarios opinan sobre las propuestas que planteamos uno pensaría que es mejor estar solo porque hay temas preocupantes en comunidades del Chocó, Vaupes y Amazonas, l**a exigencia es que el Gobierno tome decisiones o nos toca a nosotros, o nos mata el hambre o nos mata el virus pero el Gobierno debe responder de alguna forma",** concluye Sauca. [(Lea también: Covid-19 amenaza con extinguir la sabiduría indígena ancestral del Amazonas)](https://archivo.contagioradio.com/covid-19-amenaza-con-extinguir-la-sabiduria-indigena-ancestral-del-amazonas/)

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
