Title: Corte tiene en sus manos el futuro de 4mil mineros artesanales de Marmato
Date: 2017-02-28 13:19
Category: DDHH, Nacional
Tags: Gran Colombia Gold, Marmato Caldas, mineros artesanales
Slug: corte-tiene-en-sus-manos-el-futuro-de-4mil-mineros-artesanales-de-marmato
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/02/MinerosArtesanales.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Alcaldía Marmato] 

###### [28 Feb 2017] 

Las comunidades de Marmato Caldas denunciaron que a pesar de los 78 amparos administrativos que los defienden como mineros artesanales, **la multinacional canadiense Gran Colombia Energy insiste en desalojar a 4.000 mineros del territorio**, la multinacional anunció que demandará al Estado Colombiano por US\$700 millones, debido a que la Alcaldía municipal ha incumplido al no hacer efectivos los desalojos.

La Asociación de Mineros Artesanales de Marmato, manifestó que el título minero y las actividades de la canadiense, van en contravía de sus tradiciones y señalan que las autoridades correspondientes nunca hicieron Consulta Previa con ellos para dicha aprobación y otorgamiento de licencia, **"acá en Marmato no nos explican nada, sólo nos amenazan con que tenemos que irnos"**, puntualizó Mario Teguerife, presidente de la Asociación.

También señaló que desde hace 9 años, cuando fue otorgado el título minero, la comunidad ha tenido que enfrentarse a una constante incertidumbre por las negligencias estatales, pues **pese a que cuentan con algunas herramientas jurídicas que les favorece, aún el Estado colombiano no ha definido su situación.** Indicó que ahora el futuro de más de 4mil personas que habitan el territorio, está en manos de la Corte Constitucional, quien deberá estudiar una ponencia propuesta por el magistrado Luis Ernesto Vargas.

Dicha ponencia, está orientada hacia la solicitud de una Consulta Previa con las comunidades del municipio de Marmato, **para que “se permita la explotación minera en la región por parte de la multinacional canadiense”**. Las directivas de la Gran Colombia Gold, instauraron en las ultimas horas, una demanda contra el Estado colombiano “para hacer efectivos sus derechos”, argumentan que aunque cuentan con licencia para la exploración y explotación de oro** **en esa zona mediante el título CGH-081 del 2008, no han “podido realizar sus labores” porque las familias ocupan la zona.

###  

Así pues, la Corte Constitucional deberá estudiar los amparos administrativos que favorecen a los pequeños mineros, los estudios de impacto ambiental de la multinacional y los testimonios de la comunidad, quien asegura nunca les fue consultado ni socializado el proyecto, **"si la Corte Constitucional permite que sean las multinacionales las que exploten, serán desplazados 4 mil mineros"** advirtió Tenguerife, quien también resaltó que “daremos la pelea, porque ¿a dónde nos vamos a ir si sólo sabemos hacer minería?”.

Por su parte, la Confederación Nacional de Mineros de Colombia –CONALMINERCOL–aseguró que el Gobierno Nacional “no ha promovido la concertación entre la multinacional y los mineros artesanales”. Además, argumentó que el lío jurídico y el limbo en el que se encuentran las familias “es debido a un mal proceder del Gobierno, **porque otorgaron licencias justo en el lugar donde estos mineros han hecho presencia de manera histórica”.**

Por último, Mario Tenguerife, dijo que las familias estarán a la espera de la decisión de la Corte y que además de ello, de no obtener una respuesta o solución efectiva a la problemática **“nos movilizaremos, para defender nuestros derechos, no vamos a desalojar”.**

###### Reciba toda la información de Contagio Radio en [[su correo]
