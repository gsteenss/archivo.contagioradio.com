Title: ''Mi hijo para mí es un gran héroe'': María Sanabria, una de las madres de Soacha
Date: 2018-02-06 16:55
Category: DDHH, Sin Olvido
Tags: Alvaro Uribe, Ejecuciones Extrajudiciales, falsos positivos, madres de soacha
Slug: maria_sanabria_madres_soacha_falsos_positivos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/02/1494541802_870832_1494542301_noticia_normal_recorte1-e1517953898189.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:El Espectador] 

###### [6 Feb 2018] 

"Hace 10 años a las 11 del día se llevaron a mi niño", recuerda entre lágrimas María Sanabria, madre de **Jaime Estiven Valencia Sanabria, a quien a sus 16 años,** la Brigada XV del Ejército Nacional engañó, secuestró, asesinó y luego hizo pasar como guerrillero muerto en combate en la época de la seguridad democrática del gobierno de Álvaro Uribe Vélez.

Sanabria hoy no solo llora el asesinato de su hijo, sino **la impunidad reinante en el caso de la ejecución extrajudicial de Jaime Estiven y la de miles de jóvenes** que perdieron la vida por los intereses económicos de los militares, en marcados en la política de seguridad de ese entonces, mediante la cual se premiaba monetariamente a los militares que presentaran mayor número de guerrilleros muertos en combate.

''Como madres es muy difícil. Por recibir unas monedas vendieron a mi hijo. Personas que no tienen sentimientos se lo llevaron con engaños sabiendo lo que iba a pasar con ellos por recibir unas cuantas monedas por vender a estos muchachos. Me parece tan triste y desgarrador que trafiquen con la humanidad, y se los hayan entregado directamente al la Brigada Móvil XV para mostrar sus grandes resultados'', cuenta Sanabria, y agrega ''Mi niño nunca conoció las armas, nunca tuvo un arma en sus manos el estaba era estudiando''.

### **Las constantes dilaciones ** 

En estos años de espera y dilaciones las madres se han tenido que enfrentar **amenazas, falta de acompañamiento e incumplimiento de promesas por parte del Estado Colombiano**. Asimismo, en repetidas ocasiones las madres de los jóvenes han denunciado lo lenta que ha actuado la justicia y el incumplimiento de los acusados a las audiencias citadas.

**Solo en 3 de los 19 procesos adelantados por** [**ejecuciones extrajudiciales en Soacha,**]**  los implicados han recibido condenas,** ''Llevamos 10 años y el caso se encuentra en la completa impunidad'', expresa la madre de Jaime Estiven. (Le puede: [Continúan las dilaciones en casos de ejecuciones extrajudiciales de Soacha)](https://archivo.contagioradio.com/siguen-las-dilaciones-en-ejecuciones-extrajudiciales-de-soacha/)

Además han** **manifestado su inconformidad frente a la propuesta que había presentado el Senador y expresidente **Álvaro Uribe Vélez**, con la que se buscaba otorgar **beneficios judiciales a militares procesados y condenados** por delitos en el marco del conflicto armado. ''Han hechos miles de tramas para impedir que el caso llegue a la justicia'', dice Sanabria.

De acuerdo con informes del Centro de Investigación y Educación Popular (CINEP), un total de **5.265 ejecuciones extrajudiciales fueron cometidas en Colombia entre los años 2002 y 2010**. En 2015, el Centro Nacional de Memoria Histórica registraba un porcentaje de **impunidad del 95% en los casos de ejecuciones extrajudiciales en el pais**.

''Mi niño era un menor de edad, por eso debió haber más rapidez para que haya justicia, pero por más lucha que he dado no ha pasado nada, seguimos en la impunidad, porque con la plata se ha callado a jueces, fiscales y funcionarios públicos'', expresa en medio del dolor María Sanabria, quien afirma no tener ningún tipo de esperanza en la Justicia Especial para la Paz, pues para ella las ejecuciones extrajudiciales ''nos son hechos de guerra, son crímenes de lesa humanidad. En la JEP todo quedará en la impunidad''.

### **Más de 5.000 casos de ejecuciones extrajudiciales** 

Un informe del 2015 de Human Right Watch, habla de evidencias que sugieren que numerosos generales y coroneles tenían conocimiento de varios casos de ''falsos positivos'' y que en algunas ocasiones fueron ellos mismos quienes ordenaron o en su defecto, hicieron posible que se cometieran los asesinatos.

Cerca de ** **3.000 casos son investigados actualmente por la Fiscalía General de la Nación. ** **De acuerdo a los análisis de **Human Rights Watch** **los fiscales han identificado más de 180 batallones que cometieron ejecuciones extrajudiciales** entre 2002 y 2008, por lo que comandantes y unidades responsables de estos hechos podrían ser penalmente imputables por su capacidad de mando. (Le puede interesar: [180 batallones cometieron ejecuciones extrajudiciales)](https://archivo.contagioradio.com/human-rights-watch-expone-su-ultimo-informe-sobre-falsos-positivos-en-colombia/)

### **La esperanza y la lucha en medio del dolor** 

En medio de los 10 años de lucha, la salud de María, y de todas las madres víctimas de los mal llamados ''falsos positivos'', la salud se ha deteriorado, sin embargo, su tarea de dar a conocer al mundo lo que pasó en la época de la seguridad democrática se mantiene firme.

''Mi hijo para mí es un gran héroe, porque por medio de él he podido cuidar más vidas porque he ido a otros lugares a contar lo que está sucediendo en el país''', dice Sanabria, y agrega que de la mano de otras personas que han **logrado impedir que otras ''mamitas les pase lo que nos pasó a nosotras'', manifiesta.**

Precisamente, estas madres han convertido su dolor y su memoria en poesía, en grito poético de denuncia y demanda de justicia. Su presencia en la escena como poetas y actrices de su propia tragedia le concede una estremecedora verdad a la obra 'Antígonas, tribunal de mujeres', con el único anhelo de que, **"así no haya justicia al menos nos encargamos de que la gente se entere de lo que ha pasado''.**

<iframe id="audio_23590310" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_23590310_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio]{.s1}](http://bit.ly/1ICYhVU)[.]{.s1}
