Title: ¿Qué nos dice el estudio más reciente sobre medios digitales en América Latina?
Date: 2018-07-09 09:15
Author: ContagioRadio
Category: Carolina, Opinion
Tags: medios alternativos
Slug: que-nos-dice-el-estudio-mas-reciente-sobre-medios-digitales-en-america-latina
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/07/estudio-medios.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Cuda/CreativeCommons 

#### **Por: [Carolina Garzón Díaz](https://archivo.contagioradio.com/carolina-garzon-diaz/)** 

###### 9 Jul 2018 

[Para avanzar en este proyecto,][[Trascender el cerco de los convencidos]](https://www.facebook.com/TrascenderElCerco/)[, es necesario conocer el estado del arte en cuanto a las investigaciones y estudios que se han hecho recientemente en América Latina. Navegando por la red entre las bases de datos en línea y los repositorios digitales se encuentran cientos de estudios realizados en uno o varios de los 20 países de América Latina.]

[He querido poner mi atención en el más reciente, publicado hace un año exactamente. Se trata del informe][*[Punto de Inflexión]*[:]*[ impacto, amenazas y sustentabilidad. Estudio de emprendedores de medios digitales latinoamericanos]*](http://data.sembramedia.org/?lang=es) [construido por SembraMedia con el apoyo de Omidyar Network. Un documento que recoge datos de 100 medios digitales en Argentina, Brasil, Colombia y México, 25 en cada país. Entre sus hallazgos más importantes se destacan:]

-   **Impacto:** [Los medios digitales han logrado investigar y cubrir historias relacionadas con la realidad política, económica y social de sus propios países. Dicho trabajo es replicado posteriormente por medios masivos nacionales e internacionales. En muchas ocasiones este trabajo periodístico ha logrado renuncias de funcionarios públicos implicados en escándalos, hechos de corrupción o crímenes, también creación de protocolos contra el acoso sexual en Universidades y acciones a favor de víctimas de violaciones de derechos humanos.]

<!-- -->

-   **Vulnerabilidad:**[ Gran número de los periodistas y personas que hacen parte de los medios digitales tienen una fuerte convicción por la búsqueda de la verdad y el cambio social. En esta labor, se convierten en objetivo de grupos poderosos legales e ilegales que quieren silenciarlos. Según este informe 45% de los medios digitales sufrieron violencias o amenazas por su trabajo y un 49% fueron víctimas de ataques cibernéticos.]

<!-- -->

-   **Sustentabilidad:**[ El informe también dedicó buena parte de su investigación y análisis a los modelos de negocio de los medios digitales, clasificando a los medios en cinco grupos: El 12% se ubicaron en el grupo de]*[Destacados]*[(con ingresos de más de USD 500.000), el 17% en]*[A paso firme]*[ (USD 100.000 - 499.999), el tercer grupo llamado]*[Sobrevivientes]*[con un 23% (USD 20.000 - 99.000), el mayor porcentaje se encuentra en]*[Principiantes y estancados]*[ (USD 100 - 19.000) con un 32% y un 17% de medios digitales no reciben ingreso alguno.]

[Finalmente en este apartado, mencionan la importancia de diversificar las fuentes de ingresos, los desafíos editoriales, las alternativas a la publicidad y otras opciones de financiamiento que garanticen la subsistencia y la independencia de estos medios digitales.]

![grafico](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/07/grafico-599x518.jpg){.size-medium .wp-image-54573 .aligncenter width="599" height="518"}

[Este es el gráfico con las cifras más importantes del informe a nivel regional (Argentina, Brasil, Colombia y México):]

**Retos de sustentabilidad y crecimiento**

[Después del lanzamiento del informe, el director para América Latina de Omidyar Network, Felipe Estefan, dio una][[entrevista en el medio digital alternativo Contagio Radio]](https://archivo.contagioradio.com/medios-independientes-ganan-espacio-y-credibilidad-en-america-latina-segun-estudio/)[ en la que enfatizó que este es el informe más profundo y completo sobre los medios independientes en América Latina. Además, mencionó varios de los retos que enfrentan estos medios, en los cuales la independencia económica es sinónimo de independencia editorial. ¿Cómo subsistir y crecer? De acuerdo con las pautas de Estefan y tras leer el informe, les comparto cuatro pautas:]

1.  [Los medios de comunicación juegan un papel fundamental en la]**construcción de una comunidad**[ con sus oyentes, sus espectadores y sus lectores. Es necesario permitirles a quienes integran esta comunidad hacer parte del medio y mantener un diálogo constante con ellos.]
2.  [Es importante continuar]**involucrando a las audiencias en la creación de contenidos**[. Posicionando la idea de “prosumidores”, es necesario que los medios digitales puedan articular en sus propuestas editoriales la información y otros insumos producidos por las audiencias.]
3.  [En términos editoriales, consolidar un grupo más diverso de periodistas que puedan compartir sus perspectivas de los temas que investigan y cubren. Ampliar las voces y posibilidades de análisis.]
4.  [Resulta fundamental pensar nuevas formas de generación de recursos y nuevos modelos de negocios.]

**Una mirada para \#TrascenderElCerco**

[A su manera y de forma más consciente o más intuitiva, cada uno de los medios digitales que participaron en el estudio de SembraMedia ha desarrollado estrategias para llegar a nuevos públicos con el objetivo de ampliar el debate y posicionar en la agenda pública los temas que cubren.]

[En el caso de los medios alternativos y comunitarios, hayan nacido o no como medios digitales, su labor tiene como meta llevar realidades sociales y políticas a personas que no han accedido a dicha información. Es decir, romper la burbuja de las audiencias a quienes estos temas no les interesan o no les llegan, y trascender las audiencias conformadas por quienes conocen y son sensibles a estos temas (por ejemplo sobre vulneraciones a los derechos humanos) para generar mayor conciencia y acción a favor de las comunidades históricamente discriminadas.]

[Como lo mencioné en un artículo anterior titulado “][[Trascender el cerco de los convencidos]](https://trascenderelcerco.wordpress.com/2018/05/26/trascender-el-cerco-de-los-convencidos/)[”, es clave que los medios de comunicación alternativos y comunitarios documenten y compartan sus experiencias, para que esta información valiosa, como la recolectada por SembraMedia sea pública y genere una red en la que diversas experiencias de comunicación puedan dialogar.]

[Es una invitación a avanzar en esta experiencia introspectiva y profesionalizar el trabajo comprometido que se realiza día a día con tanta convicción; a clarificar o vislumbrar las estrategias de comunicación y los planes de acción pertinentes, así como compartirlos con los colegas de la región.Es una oportunidad de crecimiento y expansión desde la solidaridad y no desde la competencia.]

###### *[\*Artículo escrito por Carolina Garzón Díaz, y publicado en Contagio Radio, en el marco del Diplomado en Gestión de la Comunicación digital para el bien público de la Facultad Latinoamericana de Ciencias Sociales (FLACSO - Uruguay). Julio de 2018. Más información:]*[*[www.trascenderelcerco.wordpress.com]*](http://www.trascenderelcerco.wordpress.com) 
