Title: Pérdidas en Reficar equiparables al costo del metro para Bogotá
Date: 2016-05-04 16:22
Category: Economía, Nacional
Tags: corrupcion reficar, reficar, refineria cartagena
Slug: perdidas-en-reficar-equiparables-al-costo-del-metro-para-bogota
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/05/Reficar_.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Pedro Durán ] 

###### [4 Mayo 2016 ]

En el debate de control político llevado a cabo este martes en el Congreso, se logró probar que los **sobrecostos en la ampliación de la Refinería de Cartagena (Reficar) son consecuencia de la corrupción,** y no sólo de la incapacidad administrativa y malas decisiones, como están argumentando las directivas de Ecopetrol.

De acuerdo con Jorge Robledo, Senador del Polo Democrático, esta inversión pública, que terminó costando el doble de lo establecido inicialmente, **"nació torcida desde el momento en el que se privatizo Reficar"**. Entes como la Contraloría han aseverado que ésta es[ la obra más cara en toda la historia de Colombia, sus pérdidas son equiparables con lo que costó construir el Canal de Panamá o lo que cuesta el metro para Bogotá. ]

Frente a los sobrecostos hay dos tipo de responsabilidades, una penal, cuya sanción depende de la Contraloría y la Procuraduría, y otra política, en la que **es evidente que el 50% compete al gobierno Uribe y el restante, a Santos**, afirma el Senador.

Robledo asegura que contra la transnacional CB&I cursa una demanda por corrupción interpuesta por Reficar ante un tribunal de Nueva York, en la que se **reclama la cancelación de por lo menos US\$2 mil millones**.

Actualmente 30 funcionarios de la Contraloría trabajan en la recolección de información y auditoría del caso, para presentar un informe final en septiembre de este año, en el que se probará con más detalle el [[nivel de corrupción](https://archivo.contagioradio.com/sobrecostos-por-us-4-mil-millones-en-reficar-van-a-debate-de-control-politico/)] y **se señalaran las responsabilidades individuales en las que se verán comprometidos altos funcionarios del Estado colombiano**.

<iframe src="http://co.ivoox.com/es/player_ej_11412040_2_1.html?data=kpahk5eUeJGhhpywj5WbaZS1lZ6ah5yncZOhhpywj5WRaZi3jpWah5yncavj08zSjbfTps3ZxdSYj5C0s83jjKnSz9THtoa3lIquk9nNp9Chhpywj6jTstXVyM7cjbfFqMrjjJKSmaiReA%3D%3D&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)]] 
