Title: El asesinato de Jaime Garzón "es un crimen de Lesa Humanidad"
Date: 2015-08-13 15:22
Category: DDHH, Entrevistas, Judicial
Tags: CCJ, Don Berna, General Rito Alejo del Río, Jaime Garzon, Jorge Enrique Mora Rangel, marisol garzon, Mauricio Santoyo
Slug: el-asesinato-de-jaime-garzon-es-un-crimen-de-lesa-humanidad
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/08/Mariso_garzon_Jaime_garzon_contagioradio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: archivo familia Garzón 

##### <iframe src="http://www.ivoox.com/player_ek_6660504_2_1.html?data=l5ujkpqUeI6ZmKiakp6Jd6KklpKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRic2fwtjS1c7SpdXjjMnSja%2FFrc7ZjKzO1N%2BJh5SZo5jbjYqWdsbnjNrbjcjWrc7Zz5DRx5CwqdTVjK3iz5KJe6ShpNTb1sbLrdCfs8bRy9SPcYarpJKh&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>  
[Marisol Garzón, hermana del periodísta] 

###### [13 Ago 2015] 

Hoy 13 de agosto se conmemoran 16 años del asesinato del periodista y humorista Jaime Garzón Forero en la ciudad de Bogotá a manos de dos sicarios, que según las últimas informaciones entregadas por "Don Berna" contaron con el apoyo logístico de altos mandos militares.

**Marisol Garzón**, hermana del periodista recuerda que para ella, esta pérdida ha sido muy dura *“**por la mentira, por la  falsedad, por la calumnia, por tener los teléfonos chuzados, los correos***” y por “un país que funciona al revés”,  sin embargo Marisol recalca que “*hay que mantener la esperanza…Jaime fue un hombre que nos invitó a nosotros a creer*”.

Frente a las declaraciones de “Don Berna” durante la última semana, y que involucra al general Mauricio Santoyo, Rito Alejo del río y Jorge Enrique Mora Rangelen el asesinato de Garzón, Marisol afirma que es muy interesante puesto que "***Don Berna, no está esperando nada a cambio, está hablando sencillamente por dar datos, que para nosotros son muy valiosos demostrarle a la fiscalía que el caso de Jaime sí es de lesa humanidad***”.

Lo que dice Don Berna es importante no solo por que contribuye en el esclarecimiento del crimen contra Jaime, sino también por otros hechos relacionados con el exterminio de los integrantes de "La Terraza" “*Es vergonzoso vivir en un país que llevamos 16 años y no ha pasado nada*” afirma Marisol

El día de hoy con la participación de los colectivos Kinorama y Dexpierte  se realizaran distintas actividades en homenaje a Jaime Garzón en su estatua ubicada en la calle 26. Marisol rendirá homenaje a su hermano con el lanzamiento del libro del libro “**Jaime Garzón: Lea pa´que hablemos**” en la Universidad Nacional  a las seis de la tarde.
