Title: La Guajira necesita que se declare el estado de excepción
Date: 2017-02-16 16:11
Category: Comunidad, Nacional
Tags: Comunidades Wayúu, La Guajira
Slug: santos-y-su-error-de-no-declarar-en-estado-de-excepcion-a-la-guajira
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/05/muerte-de-ninos-wayuu.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [16 Feb 2017] 

**"Es un error no haber declarado el "estado de excepción"** en la Guajira, solo así se hubiese podido sentar las bases de una solución estructural al problema que atraviesan las comunidades Wayuu en ese departamento. Carolina Sáchica, abogada, señaló también que pretender solucionar la crisis con **"medidas ordinarias" es otro error dado que la propia CIDH ha intervenido con medidas cautelares que no han tenido respuesta".**

Sin embargo, el argumento del gobierno Santos es que para **poder declarar el estado de excepción se tenían en cuenta la existencia de hechos que amenacen el orden económico, social o ecológico,** concluyendo que el gobierno puede acudir a todas las medidas ordinarias para superar la crisis de La Guajira. Le puede interesar: ["Comunidades Wayúu vuelven a bloquera vía férrea del Cerrejón"](https://archivo.contagioradio.com/comunidades-wayuu-exigen-derecho-a-la-autonomia-regional/)

Para Sáchica esta decisión del presidente deja sin respaldo ni garantías a las comunidades indígenas de La Guajira **“uno se pregunta qué tiene que suceder para entender que las medidas ordinarias no han funcionado y se han quedado cortas ante los daños que ha generado el olvido del Estado”. **

Las medidas cautelares fueron ordenadas por la **Comisión Interamericana de Derechos Humanos desde el 11 de diciembre de 2015 para la protección de la primera infancia** y reiteradas el 2 de agosto de 2016, este año la CIDH ordeno que las medidas se ampliaran para proteger a las mujeres de esta comunidad, sin embargo, de acuerdo con la abogada Carolina Sáchica, a pesar de que el gobierno diga que presenta avances, las condiciones en las que permanecen las comunidades continúan siendo deplorables. Le puede interesar: ["CIDH cobija a 9 mil mujeres Wayúu con medidas cautelares"](https://archivo.contagioradio.com/cidh-cobija-a-9-mil-madres-gestantes-y-lactantes-con-medidas-cautelares/)

**Además de la hambruna, falta de agua y la alta tasa de mortandad,** las comunidades afrontan la violación de derechos humanos producto del Cerrejón, Sáchica indicó que pese a que las regalías que obtiene esta empresa del país son multimillonarias, “en la práctica no se ve la inversión ni los recursos, sino el hecho de que los niños se siguen muriendo y a los sobrevivientes llevando  una mala vida”. Le puede interesar: ["Robos, golpes e insultos a indígenas Wayúu en La Guajira"](https://archivo.contagioradio.com/robos-golpes-e-insultos-de-esmad-a-indigenas-wayuu-en-guajira/)

<iframe id="audio_17059976" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_17059976_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)
