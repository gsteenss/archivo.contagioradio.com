Title: Garantizar un buen sistema de justicia es vital para la paz: Jim McGovern
Date: 2015-08-19 16:53
Category: El mundo, Entrevistas, Paz, Política
Slug: garantizar-un-buen-sistema-de-justicia-es-vital-para-la-paz-jim-mcgovern
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/08/James-McGovern.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: [voces.huffingtonpost.com]

<iframe src="http://www.ivoox.com/player_ek_7139957_2_1.html?data=mJagm56Ze46ZmKialJyJd6Kpl5KSmaiRdo6ZmKiakpKJe6ShkZKSmaiRic%2Fo08rjy9jYpYy%2BwtLS1ZCxp6jj18rf0IqWh4zmxtXfx9jJstXVz9nSjcnJsYa3lIqvlcjWpdXVjKqbt5KJe6ShpNTb1sbLrdCfs8bRy9SPcYarpJKh&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Jim McGovern, Congresista Partido Demócrata] 

###### [19 Ago 2015 ] 

Jim McGovern, congresista de EEUU por el partido demócrata conversó con Contagio Radio sobre las apuestas, las responsabilidades y las deudas de ese país con el proceso de paz en Colombia. Temas como la impunidad, el paramilitarismo, la extradición y los derechos de las víctimas, así como la construcción de paz con justicia social son algunos de los temas en los que el congresista hace énfasis.

**Contagio Radio:** ¿Cuáles son los factores que han propiciado la impunidad en Colombia?

**James Mac Govern:** “Creo que la impunidad sigue siendo un problema en Colombia y el propósito de nuestra carta era urgir a nuestro gobierno a insistir al gobierno de Colombia y a las FARC en la Mesa de Negociaciones que este es un tema importante y que se deben tomar en cuenta las víctimas de conflicto, porque es importante para que la paz sea sostenible que lo que le paso a las víctimas sea reconocido y sea tratado para el futuro y entonces en la carta que fue escrita al gobierno y a las FARC quisiéramos, como miembros del Congreso, asegurar que las negociaciones sean incluyentes entonces hay que tener en cuenta a las víctimas del conflicto.”

‘’Creo que uno de los factores ha sido la falta de voluntad política para identificar la responsabilidad de quienes han sido responsables  para hacer las atrocidades. Hay un sinnúmero de casos de desplazamiento, de homicidio, de tortura de miembros de sindicatos, de miembros de organizaciones de derechos humanos, miembros de la sociedad civil y las víctimas nunca han encontrado justicia.

Hay una cultura en las instituciones de gobierno, incluyendo en la Fuerza Armada, que ha permitido que miembros de estas instituciones, incluyendo miembros de la Fuerzas Armada, queden protegidos aun cuando hayan sido responsables en la violación de derechos humanos. Igual hay una cultura de la impunidad en las FARC. También muchos colombianos inocentes quienes han sido asesinados o han sido secuestrados les han exigido pagos para soltarles y de parte de las FARC no ha habido justicia para ellos tampoco.

Y la razón por la que mandamos la carta era para hacer la solicitud para insistir a las dos partes que es necesario poner fin a esta cultura de la impunidad en Colombia para la paz. Gente que comete diferentes crímenes debe pagar por sus acciones independientemente de su ideología e independiente de sus antepasados. ’’

**CR:** ¿Cree que el gobierno de Estados Unidos podría estar de acuerdo con un tipo de justicia transicional que contemple elementos de justicia restaurativa y reparativa para un posible acuerdo de paz o tendrían que ser penas carcelarias para considerar que las víctimas han reestablecido sus derechos?

**JM:** ‘’Yo creo que lo que es más importante es que quienes han sufrido de violaciones sientan que los resultados del acuerdo son apropiados para sus situaciones y sus experiencias y creo también que es importante que en un tema tan sensible que decide Colombia. La preocupación de varios de nosotros en el Congreso es que no se ponga de lado el tema de la justicia para las víctimas.’’

**CR:** ¿Qué se ha pensado desde el Congreso estadounidense sobre el tema de la extradición, teniendo en cuenta que las víctimas afirman que ésta ha generado impunidad?

**JM:** ‘’Para empezar es importante decir que es el Gobierno de Colombia quien decide sí extradita a una persona o no. El Gobierno de Colombia hoy en día tiene la posibilidad de negar una solicitud de extradición si quiere hacerlo. La mayoría de los casos de extradición a los Estados Unidos han tenido que ver con delitos de narcotráfico no con violaciones de derechos humanos. Lo que creo es que es importante y es necesario que los colombianos decidan que quieren hacer en cuanto al tema de la impunidad en cuanto a cómo luchar contra la impunidad y ahí será necesario que nosotros lo tengamos en cuenta y lo revisemos y trabajemos con Colombia. Y creo que es importante que los Estados Unidos sea flexible, porque el objetivo es lograr un acuerdo de paz que sea sostenible. ’’

**CR:** ¿Cree que Estados Unidos estaría dispuesto a abrir los archivos sobre su contribución a la cadena de impunidad en Colombia con casos como el de los militares norteamericanos en una base militar colombiana?

**JM:** ‘’Su respuesta a la pregunta es si, gente que trabaja para el Gobierno estadounidense, ya sean diplomáticos o militares no están por encima de la ley. Sé que ha habido acusaciones, no cuento con toda la información, no conozco todos esos hechos, pero si hay ciudadanos estadounidenses que han sido responsables de acciones así que debieran enfrentar la justicia y deben tener que responder.’’

**CR:** ¿Estados Unidos abrirá los archivos sobre las operaciones militares que han orientado y que han contribuido a la guerra en Colombia, tal como ha insistido las FARC, para que se conozca la verdad sobre las responsabilidades en la guerra en general?

**JM:** Yo creo en la transparencia, también soy susceptible al hecho de que algunos de los archivos puedan contener por ejemplo nombres de personas que podrían encontrarse en peligro que simplemente se abran pero en términos generales creo en la transparencia y apoyaría el esfuerzo de maximizarla en ese sentido.

**CR:** ¿Cómo podría contribuir Estados Unidos al desmonte del paramilitarismo?

**JM:** ‘’Al fin de cuentas el desmonte del paramilitarismo depende del gobierno colombiano. Nosotros como estadounidenses podemos compartir la inteligencia, podemos compartir la asistencia técnica, podemos ofrecer personal para investigación, pero al fin del día la responsabilidad para la judicialización de los paramilitares es propiedad del gobierno colombiano.

Y como dije al principio y como es el punto de la carta que mandamos como congresistas, entender la preocupación de las víctimas, entender la persistencia de la impunidad lo que se debe tomar en cuenta en el proceso de paz y que se debe tratar como parte para que haya paz.

Agregaría una cosa más, por mucho tiempo he sido crítico de la política estadounidense hacia Colombia creo que hay demasiado énfasis en la asistencia a las Fuerzas Armadas y lo suficiente es fortalecer a las instituciones civiles. Pero asegurar que en Colombia haya un sistema judicial que asegura el Estado de Derecho es vital. ’’

**CR:** ¿Cómo va el tema del plan de acción laboral en el marco del TLC Colombia Estados Unidos, pues se afirma que en varios sentidos de esto depende que haya paz real y que haya garantía de derechos para las víctimas?

**JM:** ‘’Me opuse al Tratado de Libre Comercio con Colombia, pero una vez que fue aprobado he hecho lo que he podido hacer para asegurar que el Plan de Acción Laboral sea implementado plenamente y completamente, no creo que ese Plan ha sido implementado todavía tengo grandes preocupaciones frente al tratamiento de quienes organizan sindicatos y tengo grandes preocupaciones sobre la existencia de justicias ingerentes, estructurales en el sistema económico en Colombia y le he dicho directamente a oficiales del Gobierno colombiano y mientras aprecio sus declaraciones, sus comentarios de que si se está implementando el Plan de Acción Laboral y sus declaraciones en cuanto a un plan de restitución de tierras, lo que he visto cuando he estado en Colombia y lo que muchos colombianos me han dicho, es que esos planes no se han implementado como debe ser y mientras el Gobierno central está diciendo lo que debe ser, está diciendo lo correcto en todo el país, estas iniciativas no se están implementando de la forma que debe ser y eso es un problema y parte de un buen acuerdo de paz debe insistir en que estos acuerdos se implementen y Colombia debe solicitar apoyo de la comunidad internacional incluyendo de los Estados Unidos para ayudar a implementar estas iniciativas.’’

**CR:** ¿Qué piensa usted del papel que ha cumplido hasta el momento el delegado de los Estados Unidos al proceso de paz?

**JM:** ‘’Me parece importante que tengamos un delegado, yo trabaje con el señor Aronson en el proceso de paz del Salvador, hubo algunos errores durante el proceso en los acuerdos y debemos aprender y entender las lecciones de estos errores. Pero el delegado Aronson es conocedor y está tomando muy enserio su papel y yo estoy muy agradecido por eso. Quisiera decir una cosa más, yo tengo esperanza, yo creo que se están dando importantes discusiones en La Habana y yo creo que el pueblo colombiano ha sufrido suficiente, Colombia es un país bello con mucho potencial y lo que lo obstaculiza es ese conflicto y esta cultura de impunidad que todavía se tiene que trascender.

Esas negociaciones de paz están dando a Colombia la posibilidad de encontrarse en mejor día y yo quiero que tengan éxito. Pero si tengo una palabra de precaución, el Gobierno de Obama está apoyando fuertemente estas negociaciones de paz, hay muchos miembros del Congreso que también apoyan el proceso y las negociaciones, pero habrá una elección en los Estados Unidos en noviembre de 2016, no tengo idea quien será el próximo presidente de los Estados Unidos, yo no sé si a mí me van a reelegir, el punto es que hay un grupo de gente en el Gobierno actual de los Estados Unidos que apoyaría los recursos necesarios para implementar la paz.

Entonces este es el momento para que los negociadores concluyan y terminen con los acuerdos positivos, todas las estrellas están alineadas hoy, pueden no estar alineadas el 9 de noviembre, este es el momento y esa es la razón por la que miembros del congreso están mandando cartas a La Mesa, al Gobierno y a las FARC, urgiéndoles a terminar la negociación.

Queremos ayudar y yo en particular quiero ayudar a quienes más han sufrido, a los campesinos, a los afrocolombianos, a los defensores de derechos humanos, a los sindicalistas, a los desplazados y a quienes han perdido sus seres queridos por acción de uno u otro lado. Nosotros deseamos un muy buen acuerdo de paz y yo haré todo lo posible para ayudar. ’’
