Title: Camilo Torres ejemplo de lucha para Latinoamérica
Date: 2016-02-04 13:25
Category: eventos, Nacional
Tags: 50 años Camilo Torres, camilo torres
Slug: camilo-torres-ejemplo-de-lucha-para-latinoamerica
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/02/Camilo-Torres_.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Astelarra ] 

<iframe src="http://www.ivoox.com/player_ek_10316966_2_1.html?data=kpWgk5udepehhpywj5WbaZS1kZeah5yncZOhhpywj5WRaZi3jpWah5yncaTVzs7Z0ZC4s9PmxtiYx8%2FJsdHg0JDRx5DQucTcwpDdw9fFb63V1c7b0cbRaaSnhqam1M7HpY6ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Prof. Germán Roncancio] 

###### [4 Feb 2016. ] 

En el marco de la conmemoración de los cincuenta años de la muerte de Camilo Torres, diferentes organizaciones sociales preparan **jornadas académicas y culturales en distintas ciudades del país** con las que buscan rendir homenaje a la vida y obra del sacerdote, teniendo en cuenta su influencia en el devenir político latinoamericano.

De acuerdo con el profesor Germán Roncancio, Camilo Torres ha representado para Colombia y Latinoamérica **“un  ejemplo de lucha que se ha mantenido durante cinco décadas”**, por su carácter integral, humanista y sentipensante, que lo llevo a promover la construcción de otro mundo a partir del pensamiento crítico.

El profesor insiste en la necesidad de hacer de estas jornadas conmemorativas un llamado para que se **conozca la agenda pactada durante estos tres años entre las delegaciones del Gobierno y del ELN**, y en esa vía sea formalizada la mesa de diálogos para que la sociedad civil organizada pueda participar activamente.

La jornada de conmemoración inicia el viernes 12 de febrero en la Universidad Industrial de Santander, con la instalación del **Foro ‘Camilo Sacerdote Humanista y Revolucionario’**, y el lanzamiento del **documental ‘Rastros de Camilo’**. Continúan el sábado 13 de febrero con un concierto en Barrancabermeja, y un **acto ecuménico, místico y cultural**, el domingo 14 en Patio Cemento.

###### [Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU) ]] 
