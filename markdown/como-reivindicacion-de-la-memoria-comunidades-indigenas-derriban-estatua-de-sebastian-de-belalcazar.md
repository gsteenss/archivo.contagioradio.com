Title: Como reivindicación de la memoria, comunidades indígenas derriban estatua de Sebastián de Belalcázar
Date: 2020-09-17 17:33
Author: AdminContagio
Category: Actualidad, DDHH
Tags: Black Lives Matter, Cauca, comunidades indígenas, Sebastián de Belalcázar
Slug: como-reivindicacion-de-la-memoria-comunidades-indigenas-derriban-estatua-de-sebastian-de-belalcazar
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/09/Estatua.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: @marthaperaltae

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

En horas de la tarde del pasado 16 de septiembre, comunidades Misak, Nasa y Pijao que se movilizaban en rechazo de la violencia que se vive en el departamento del Cauca y en el país, ataron cuerdas alrededor de la estatua de Sebastían de Belalcázar ubicada en Popayán para luego derribarla como forma de "reivindicar la memoria de sus ancestros asesinados y esclavizados".

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Para e**l Movimiento de Autoridades Indígenas del Suroccidente Colombiano (AISO),** de Belalcázar cometió delitos como «genocidio, despojo y acaparamiento de tierras, desaparición física y cultural" de los pueblos que habitaban dicha región durante la época de la conquista. Dicha evidencia está sustentada no solo en crónicas, expedientes de archivos sino en la misma tradición oral delos pueblos ancestrales Misak.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

La estatua fue erigida por el artista español Victorio Macho y ubicada en el Morro de Tulcán en Popayán desde 1937, en honor a Sebastián de Belalcázar, conquistador que fundó Quitó el 6 de diciembre de 1534, así como Cali y Popayán y a quien se le atribuye el asesinato de 800.000 personas desde 1514 a 1524.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/FelicianoValen/status/1306358093635354625","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/FelicianoValen/status/1306358093635354625

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph {"align":"justify"} -->

Desde la [Organización Nacional Indígena de Colombia (ONIC),](https://twitter.com/ONIC_Colombia)conglomerado que expresó su respaldo al acto libertario, su consejero mayor, Luis Fernando Arias, expresó que se trató de "una deuda pendiente durante siglos atrás y que ha sido saldada pero también es un mensaje para la lucha ética, moral política e histórica de pueblo colombiano".

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Del Black Lives Matter a la resignificación histórica de una estatua

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

El hecho ocurrido con la estatua en Popayán emula lo ocurrido meses atrás en Estados Unidos tras el asesinato del afroamericano George Floyd a manos de un policía blanco, hecho que derivó **en el movimiento Black Lives Matter** y en el derrumbe de numerosos monumentos alrededor del mundo en honor a líderes políticos, figuras del viejo continente y exploradores de la “conquista” de América como Cristobal Colón o Miguel de Cervantes. [(Así es el movimiento antirracista que conocimos después del asesinato de George Floyd)](https://archivo.contagioradio.com/george-floyd-antirracismo-mundial/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Durante el mes de junio, en Estados Unidos figuras de George Washington y Thomas Jefferson también fueron derribas mientras en Reino Unido han sido pintadas las estatuas de la reina Victoria y Winston Churchill, con expresiones como "racistas", por su parte la del comerciante de esclavos Edward Colston fue arrancada en Bristol y arrojada al agua.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/spencercompton/status/1279579382793875459","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/spencercompton/status/1279579382793875459

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph {"align":"justify"} -->

Mientras historiadores especialistas en América Latina señalan que dichos actos son una forma de descolonizar el pensamiento y reconstruir la historia del país, otros argumentan que "no se puede analizar con las lentes del siglo XXI actuaciones de hace 500 años", sin embargo aclaran que es valido resignificar las esculturas; no así mismo con las diferentes figuras erigidas a dictaduras contemporáneas, cuando la defensa de los DD.HH. ya había sido establecida. [(Le puede interesar: "En Colombia la historia se debe contar con las voces de la comunidad negra")](https://archivo.contagioradio.com/en-colombia-la-historia-hay-que-contarla-como-es-con-las-voces-de-la-comunidad-negra/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Esto a propósito del derribo de otras estatuas de figuras contemporáneas como las de Iósif Stalin en Gori, Vladimir Lenin en Járkov e incluso la de Saddam Hussein en Bagdad en 2005. Pese al acto de reivindicación, desde la Alcaldía de Popayán han señalado que la escultura será sometida a restauración para luego ser colocada nuevamente en el lugar.

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->
