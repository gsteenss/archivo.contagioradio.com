Title: Plan de Acción Laboral, otra promesa incumplida del TLC Colombia-EEUU
Date: 2015-05-26 12:57
Category: DDHH, Nacional
Tags: alirio uribe, derechos laborales en colombia, Impactos del TLC en colombia, Plan de Acción Laboral, Polo Democrático Alternativo, TLC, TLC con Estados Unidos
Slug: se-debatira-sobre-incumplimiento-de-plan-de-accion-laboral-suscrito-con-eeuu
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/05/Sin-título2.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: www.aliriouribe.com 

<iframe src="http://www.ivoox.com/player_ek_4552269_2_1.html?data=lZqilJeafY6ZmKiakp2Jd6KmlZKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRl8afxcrPw9nNtoa3lIquk5DXs8PmxpDW0MjZsdHgytLWx9PYs4zYxpC9zsbSb8XZjKbQxc6Jh5SZo5jbjbHFcYarpJKw0dPYpcjd0JC%2Fw8nNs4yhhpywj5k%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Alirio Uribe, representate a la cámara del Polo Democrático Alternativo] 

Este miércoles 27 de mayo, en la Comisión Segunda de Cámara de Representantes, se llevará a cabo el debate sobre el **incumplimiento del Plan de Acción Laboral** suscrito con EEUU, en el marco del Tratado de Libre Comercio con Colombia.

Alirio Uribe, representante a la Cámara del Polo Democrático Alternativo, quien convocó el debate, informó que se discutirá sobre las afectaciones  que ha generado el  TLC con EEUU, ya que ha traído **consecuencias negativas para los derechos de los trabajadores colombianos,** además de otros impactos a la economía nacional, la formalización del empleo, y a la industria y producción agrícola en el país.

“**El Tratado de Libre Comercio con Estados Unidos no ha resultado beneficioso para Colombia**. Nos están inundando de mercancías y ajustes institucionales, y ni siquiera los sectores que se pensaba iban a resultar favorecidos, han encontrado beneficios pasados más de dos años de la entrada en vigencia. Y frente al Plan de Acción, el Estado colombiano le está haciendo conejo a su cumplimiento”, señaló Uribe Muñoz.

Desde el 2006 se había fijado la realización de un Plan de Acción Laboral que contenía 37 temas sobre los cuales ambos gobiernos se comprometían a garantizar la mejora de los DDHH de los trabajadores, los derechos laborales y sindicales, sin embargo, a pesar de ese Plan de Acción, se sigue vulnerando los derechos de los trabajadores, ya que hasta la fecha se han cometido por lo menos **1900 violaciones contra sindicalistas**, según afirma el representante a la cámara.

Por otro lado, se va a debatir sobre la formalización laboral, que implica que los trabajadores colombianos tengan contratos estables entre el empleador y empleado, pero en Colombia, la gran mayoría de las personas cuentan con un contrato de trabajo mediado por cooperativas y otras entidades, lo que genera que **no se cumpla con los derechos de los empleados**, sin que el Ministerio de Trabajo tome cartas sobre el asunto.

El objetivo final del evento es hacer un llamado al gobierno sobre la intermediación laboral y proponer un seguimiento al Plan de Acción, poniendo en evidencia estos impactos que han sufrido los trabajadores. Es por eso que se contará con la participación del Ministro de Trabajo, el director de la Unidad Nacional de Protección, la Procuraduría General y el Fiscal General,  además se esperan invitados de Estados Unidos.

Cabe resaltar que el debate fue citado por la preocupación existente entre **centrales sindicales, Coljusticia, la Escuela Nacional Sindical,** el Congresista Alirio Uribe Muñoz, entre otros.

El debate de podrá ver en vivo este miércoles a partir de las 9:30 am a través de contagioradio.com
