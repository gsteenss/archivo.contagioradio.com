Title: "Próximo rector de la Universidad Nacional no debería ser designado por el gobierno"
Date: 2015-03-04 22:15
Author: CtgAdm
Category: DDHH, Nacional
Tags: estudiante, mane, plinio teheran, profesor, rector, Universidad Nacional
Slug: proximo-rector-de-la-universidad-nacional-no-deberia-ser-designado-por-el-gobierno
Status: published

###### Foto: Periferia 

<iframe src="http://www.ivoox.com/player_ek_4166281_2_1.html?data=lZajmJecdY6ZmKiak5mJd6Kok5KSmaiRdo6ZmKiakpKJe6ShkZKSmaiRic3ZxMjWh6iXaaOnz5DRx5DWqcTo0NeYt7ORaZi3jqjc0NnFq8rjjLfOxs7Tb46ZmKialg%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Plinio Teherán, Representante de los profesores ante el Consejo Académico de la UN] 

En las próximas semanas, la **Universidad Nacional de Colombia** llevará a cabo la elección del nuevo rector, la persona encargada de orientar la administración de la institución educativa en los próximos años. Sin embargo, la comunidad universitaria está denunciando que el proceso es una pantomima de democracia, según lo califica **Plinio Teherán, representante de los profesores.**

<div>

El proceso de elección del rector de la universidad nacional no es menor. La Universidad es una de las más importantes del país, no sólo porque da cupo a más de 40 mil estudiantes de todo el país; sino por generar investigación y extensión de altísima pertinencia y calidad, posicionando a la Universidad Nacional como una de las más importantes de América Latina. El rector de la Universidad, además, tiene una participación importante en órganos de dirección y definición de políticas públicas para la educación superior como el **Consejo Nacional de Educación Superior, CESU.**

Y es precisamente por la responsabilidad que descansa en los hombros de este personaje, que la **comunidad universitaria de la Universidad Nacional exige tener voz y voto** en la decisión sobre quién ocupará el importante cargo.

Según explica el representante de los profesores ante el Consejo Académico de la Universidad Nacional, Plinio Teheran, la inconformidad reside en que existe un proceso de consulta para preguntarle a estudiantes y profesores quien debería ser el próximo rector, pero **los resultados de esa consulta no son vinculantes**. No solo se ignora la opinión del personal administrativo de la universidad, que no es consultada, sino que la decisión final la toma el Consejo Superior Universitario, CSU.

La inconformidad es mayor si se tiene en cuenta que dicho **Consejo Superior está compuesto por 8 personas, 6 de las cuales son externas a la universidad y representan de manera directa los intereses del gobierno nacional**: la ministra de educación, dos personas designadas por el presidente, un miembro del CESU, un representante de los gremios económicos, y un ex-rector (elegido, en años anteriores seguramente por la misma bancada de composición gobiernista). El CSU sólo cuenta con dos personas que representan a la comunidad universitaria: **el representante de los estudiantes, y el representante de los profesores**.

Para el profesor Teheran, el rector elegido por la **CSU** difícilmente es la **persona con mejores cualidades académicas**, o que presente una propuesta sobre administración universitaria acorde a las necesidades de la institución, sino personas que resultan "convenientes a las propuestas del gobierno".

En el marco de la presentación del Acuerdo por lo Superior 20-34 y el **Plan Nacional de Desarrollo**, seguramente el futuro rector de la Universidad Nacional será una persona designada para mantener el modelo de autofinanciación, que desligue al Estado de sus responsabilidad con la universidad; modelo de Universidad-empresa que ha llevado a la universidad a la asfixia presupuestal que vive actualmente, evidenciado en las últimas semanas especialmente en la Facultad de Ciencias Humanas y en la Sede Palmira de la Universidad, según expone el representante de los profesores.

Profesores, estudiantes y trabajadores de la Universidad Nacional de Colombia espera transformar el mecanismo de elección del rector que actualmente existe; y en el corto plazo, que el Consejo Superior Universitario respete los resultados de la **consulta interna que se llevará a cabo el próximo** **18 de marzo** vía electrónica en todas las sedes de la Universidad Nacional de Colombia.

</div>
