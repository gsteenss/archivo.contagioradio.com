Title: ¿Cuál es la realidad que se vive actualmente en Putumayo?
Date: 2015-06-11 11:53
Category: Comunidad, Nacional
Tags: Alexis Arroyo, Antonio Márquez, Derechos Humanos, Fiscalía, José Tapiero, Militarización, Persecución Judicial, plan Colombia, Putumayo, Yule Anzueta
Slug: cual-es-la-realidad-que-se-vive-actualmente-en-putumayo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/06/putumayo-1-e1434052286920.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: radiomacondo.fm 

##### <iframe src="http://www.ivoox.com/player_ek_4623968_2_1.html?data=lZuflZ6afI6ZmKialJiJd6KnmJKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRndbgxpCu0N%2FZqdXVjoqkpZKns8%2FowszW0ZC2pcXd0JCah5yncZU%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>  
[Yule Anzueta] 

##### [11 Jun 2015] 

El departamento de **Putumayo** vive actualmente una situación crítica en materia de **derechos humanos**, en la que se conoce una **persecución judicial** a las organizaciones sociales de forma masiva, fumigaciones que acaban con los cultivos de los campesinos, y empresas privadas que buscan la extracción petrolera.

Las organizaciones sociales aseguran que los intereses económicos están sobre los derechos humanos de los habitantes del departamento. **Yule Anzueta**, de la mesa de organizaciones sociales afirma que los desmovilizados dan testimonios falsos en contra de los campesinos acusándolos de pertenecer a las estructuras de las **FARC** y a raíz de esto se llevan a cabo capturas masivas.

La información que se conoce por parte de abogados y la **Fiscalía** es que hay 25 personas por capturar, en este momento hay **48 personas privadas de la libertad**, pero es una cifra inestable porque se liberan y se capturan constantemente, entre ellos están **José Tapiero, Alexis Arroyo y Antonio Marquez** que fueron condenados a **54 años** de cárcel.

Los intereses económicos que hay en el **Putumayo** hace que se violen los derechos humanos, este departamento es una zona donde se va a presentar la explotación petrolera, y llega la fumigación que acaba con los cultivos acompañada de la militarización, **Anzueta** afirma que al gobierno no le interesa defender los derechos de las comunidades, el interés está enfocado en las empresas sin importar que se generen desplazamientos de la población.

Estas situaciones suceden desde **2004** cuando comienza el **plan Colombia**, ante esto, la mesa de organizaciones sociales ha protestado en cuatro ocasiones por ser el sector más afectado por las detenciones masivas. Desde que se instaló la mesa de negociación con presencia de cuatro ministros, uno de los temas es el de derechos humanos y se ha venido denunciando la situación actual del movimiento social.

"La persecución es permanente en todos los lugares donde hacen presencia las organizaciones campesinas, no se dan garantías para ejercer el derecho a la defensa del territorio" enfatiza **Yule Anzueta**.
