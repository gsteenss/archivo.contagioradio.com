Title: Indígenas hondureños defienden el río Gualcarque de construcción de represa
Date: 2015-10-21 14:28
Author: AdminContagio
Category: El mundo, Resistencias
Tags: Copinh, Departamento de Santa Bárbara, Empresa DESA, Gobierno Hondureño, honduras, indígenas Lencas, represas en Honduras, río Gualarque, Tigres
Slug: indigenas-hondurenos-defienden-el-rio-gualcarque-de-construccion-de-represa
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/10/copinh-honduras-berta-caceres-indigenous-lenca-resistance-722x406.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: [fpif.org]

###### [21 Oct 2015]

Desde el pasado 13 de octubre, **la comunidad indígena Lenca realiza un plantón con el objetivo de demostrar su rechazo frente a la construcción de una represa de la empresa DESA (Desarrollos Energéticos)**, que afectaría el río Gualcarque en el departamento de Santa Bárbara.

**“El río no se toca”** es el señalamiento que hacen los indígenas organizados en el Consejo Cívico de Organizaciones Populares e Indígenas de Honduras, COPINH, quienes afirman que  por las **actuaciones militares de la Tropa de Investigación y Grupo de Respuesta Especial de Seguridad** ha sido difícil llevar a cabo la protesta pacífica.

Mediante un comunicado, la organización indica que el sábado 17 de octubre, **los militares  dispararon a una de las personas que hacen parte del plantón dejándolo herido**, y en varias ocasiones , la seguridad privada de la empresa y la fuerza pública ha utilizando la violencia contra los indígenas Lencas desde el primer día, de manera que hacen responsable a DESA y al gobierno Hondureño de lo que pueda pasar a la comunidad.

**DESA alega que ese territorio no pertenece a los indígenas,** sin embargo, la comunidad asegura que los Lencas y otros pueblos han permanecido ancestralmente en esas tierras, cuidando el río Gualcarque.

Es por eso que los indígenas han manifestado que la acción continuará pese a las actuaciones militares, pues **no piensan abandonar el río hasta que DESA “abandone el lugar y dejen al río Gualcarque en paz”**, como se asegura desde el COPINH, quienes añaden que “ni los militares Tigres, policías ni guardias privados podrán impedir la digna lucha”.
