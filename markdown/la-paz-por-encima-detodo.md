Title: La paz por encima de todo
Date: 2016-10-02 09:57
Category: Javier Ruiz, Opinion
Slug: la-paz-por-encima-detodo
Status: published

#### Por: **[Javier Ruiz](https://archivo.contagioradio.com/javier-ruiz/) - [@contragodarria](https://twitter.com/ContraGodarria)** 

###### 2 Oct 2016 

[“La guerra en Colombia es un andar de llano a montes, de monte a montículos, sin hallar al enemigo jamás” dijo alguna vez Porfirio Barba Jacob y efectivamente el enemigo nunca se halló porque en esta guerra de más de 60 años nos matábamos entre hermanos y entre pobres colombianos que vieron en la guerra una vía de escape ante la falta de oportunidades, ante el abandono estatal o porque el mismo Estado colombiano impedía que existieran espacios más democráticos para los sectores más excluidos y si aquellos sectores se manifestaban eran reprimidos, perseguidos o asesinados.]

La guerra en Colombia solo ha traído las peores desgracias y el surgimiento de posturas extremistas que justificaban la violencia como la única manera de solucionar los problemas existentes del país. Por supuesto, se buscaron formas de lograr la paz porque ya se sabía que la guerra no iba a conducir nada bueno pero la intransigencia de los bandos en guerra impidieron que se logrará una paz negociada y así siguió el derramamiento de sangre.

Todos los actores del conflicto cometieron abusos y provocaron mucho dolor  generando más violencia y más rencor entre nosotros. Tanto así que entre 2002 al 2010 de forma unánime se nos dijo que la única manera de lograr la paz era con más guerra y que no se aceptaba otra forma de conseguirla. La gran parte de la población colombiana aceptó sin discutir esta tesis y en ese periodo se vieron los peores abusos en contra de las personas inocentes que estaban en medio del conflicto como los “falsos positivos” o la infame Operación Orión en Medellín. El fin justificaba los medios y tocaba mostrar resultados según los promotores de la guerra y quienes decían que la forma de lograr la paz era la negociada para evitar más muertes eran acusados de ser “auxiliadores del terrorismo”.

Pero resulta que los tiempos cambian y ahora toca dejar de lado la postura guerrerista. Esa postura guerrerista que justificaba masacres, desplazamientos forzados, exilios, estigmatizaciones y persecuciones a quienes se oponían a una solución militar. 60 años y demasiados muertos fue la espera para saber que si seguíamos por la postura de la victoria militar esta guerra nunca iba a terminar y que tocaba buscar la paz de forma negociada.

Con las negociaciones de paz con las FARC en La Habana fue posible demostrar que con el diálogo se puede avanzar y demostrar que todos cabemos en el mismo país. Que nuestras diferencias deben ser resueltas de formas democráticas más no de formas violentas que ha sido la regla en el país. Para lograr la paz duradera es necesario solucionar los grandes problemas del país que precisamente originaron el conflicto para que no se repitan los horrores de la guerra y podamos saber que es vivir en un país en paz.

Por desgracia hay un sector que no apoya la paz porque sin ella perderían todo su poder e influencia. El sector liderado por Álvaro Uribe ha hecho una campaña sucia y plagada de mentiras para confundir a los votantes. Además, metiendo miedo a los colombianos, ha dicho un sin fin de falsedades dejando ver que los argumentos nunca los ha tenido y así pretende generar debate cuando ya lo tiene perdido. No es serio decir que con la paz el país se volverá comunista, Castro-Chavista o en una dictadura gay para despertar el miedo irracional de algunas personas. El miedo de Uribe es que sin guerra no es poderoso, no tendría influencia y no tendría votos. Por algo se dice que Uribe sin las FARC es nada absoluta.

El 2 de octubre está en nuestra manos encaminar al país en un rumbo de paz estable y duradera. Ya no queremos más muertos, no más dolor y no más desgracias de diversa índole por culpa de la guerra. Es ahora o nunca que de forma unánime digamos SÍ a la paz y con ella empezar a construir mejor donde todos tengamos cabida.

Ante la pregunta: ¿Apoya usted el acuerdo final para terminar el conflicto y construir una paz estable y duradera? Yo digo SÍ como homenaje a las víctimas del conflicto, por esos seres brillantes que anhelaron una Colombia en paz y por un mejor futuro para los que siempre vivimos en la guerra. También para los que van a votar NO vivan en paz y vean los beneficios.

La paz por encima de todo.
