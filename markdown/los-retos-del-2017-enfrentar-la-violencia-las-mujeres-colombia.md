Title: Los retos del 2017 para enfrentar la violencia contra las mujeres en Colombia
Date: 2017-03-07 21:09
Category: infografia, Mujer, Nacional
Tags: 8 de marzo, Día de la mujer, mujeres, Violencia de género
Slug: los-retos-del-2017-enfrentar-la-violencia-las-mujeres-colombia
Status: published

###### Foto: Carmela María 

###### [7 Mar 2017] 

Un hombre de 38 años, adinerado, profesional, de familia reconocida a la que incluso se le ha investigado por asesorar  empresas que se quedaron irregularmente con terrenos baldíos en Colombia, ** hace tres meses abusó sexualmente, torturó y asesinó a una niña de 7 años, indígena y víctima de desplazamiento forzado**.

**Esa es la radiografía de un país que ha vivido más de medio siglo en conflicto armado**, donde los cuerpos de las mujeres han sido botín de guerra, pero también han sido violentados en medio de una dinámica de una sociedad absorbida por el patriarcado que actualmente provoca que 7 de cada 10 mujeres sean víctimas de algún tipo de agresión.

Yuliana Samboní, es solo una de ellas, pero es tal vez, uno de los casos que más ha indignado a los colombianos por el contexto social y la sevicia con la que sucedieron los trágicos hechos. Un crimen, que pasará a la historia como el de **Rosa Elvira Cely, quien ya cumple 4 años de haber sido violada, empalada y asesinada**, y en cuyo nombre se apoya hoy la Ley del feminicidio o la Ley 1761 del 2015, por la cual es juzgado el arquitecto Rafael Uribe Noguera, asesino de la pequeña Yuliana, quien actualmente enfrenta cuatro cargos: feminicidio agravado, secuestro simple, acceso carnal violento y tortura. [(Le puede interesar: Rosa Elvira Cely, 6 veces victimizada)](https://archivo.contagioradio.com/rosa-elvira-cely-6-veces-victimizada/)

![Caso de Yuliana](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/12/4ae58a0845a6694836872611c1511e21.jpg){.size-full .wp-image-33378 .aligncenter width="727" height="484"}

“Yo tenía mi vida nueva, mis sueños intactos…mi cuerpo inmaculado, mis manitas quietas. Mis ojos no habían visto el horror. Mis pies no habían corrido con miedo. Mi sonrisa iluminaba al mundo. Mi garganta estaba sin desgarrar. Hasta que llegó el cobarde, el abusador y asesino de niñas y me cortó las alas para siempre. Es de mejor apellido que el mío, la prensa y los jueces lo taparán, dirán de él…que estaba loco y de mí… que qué hacía en la calle. Lo llevarán a una cárcel de lujo y a mí a un cementerio de pobres”, ese es uno de los cientos de mensajes que hoy decoran la tumba de Yuliana, **un escrito que no solo evidencia la indignación de muchos colombianos, sino también, una realidad.**

Carlos Eduardo Valdés, director del Instituto Nacional de Medicina Legal y Ciencias Forenses de Colombia, ha sido testigo del aumento de la violencia contra las mujeres en el país, y muchas veces es de quien se esperan respuestas para entender por qué hombres como Rafael Uribe terminan cometiendo semejantes crímenes. La respuesta que da desde su análisis no es nada nueva. La sociedad colombiana ha interiorizada la violencia, y eso se ha inculcado en mayor grado en los varones. “Cuando digo que hemos interiorizado la violencia, es que la interiorizamos en el yo, es decir, lo primero para mí soy yo: primero yo, segundo yo, tercero yo. Rafael Uribe es el producto de esta sociedad enferma que, en la construcción de su psiquis, de su yo, y del ello, encuentra satisfacción en una forma de violencia. La violencia sexual es en la que aprendemos a tener mayores satisfacciones de niños, la que nos va a proveer más satisfacciones. Pero es una satisfacción egoísta. Uribe Noguera es el resultado de sus relaciones sociales”.

### **¿Por qué se sigue perpetuando la violencia contra las mujeres?** 

Las cifras son alarmantes. Según el Instituto de Medicina Legal, en 2016 hubo más de 27 mil casos de abuso sexual contra las mujeres en Colombia y se presentaron 731 feminicidios. **La Encuesta Nacional de Demografía y Salud, señala que la violencia de género ha afectado al 74% de las colombianas.** Cerca del 26% afirman haber sido maltratadas verbalmente en algún momento de su vida, es decir fueron objeto de intimidación, manipulación, amenaza, humillación, aislamiento o algún tipo de conducta que implica daño a la salud psicológica. Adicionalmente el 37% de las mujeres aseguran haber sufrido algún tipo de violencia física.

En Colombia, no solo son los hombres directamente agresores quienes violentan a las mujeres, con el argumento “si no es mía no es de nadie”. Ese tipo de violencia, se sustenta por una que es estructural. Una que se da en la cotidianidad de una sociedad que impide a las mujeres su derecho a decidir. A decidir, ser madres o no, esposas o no, heterosexuales o lesbianas.

![rosacely-110506 (1 de 1)](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/05/rosacely-110506-1-de-1.jpg){.size-full .wp-image-24029 .aligncenter width="1000" height="667"}

El informe Datos y Cifras Claves Para La Superación De La Violencia Contra Las Mujeres de la Corporación Humanas de Colombia en 2016, concluye que el origen de la violencia cometida contra las mujeres se debe a las relaciones desiguales de carácter histórico y estructural entre hombres y mujeres.  Según el estudio, esto se da porque hay una persistencia de imaginarios culturales  que naturalizan y normalizan la violencia basada en el género. Por ejemplo, en una encuesta se observó que el 37% de las personas consideran que las mujeres que se visten de forma provocativa se exponen a que las violen y un 45% cree que las mujeres que siguen con sus parejas después de ser golpeadas es porque les gusta. Son datos que reflejan cómo la misma sociedad justifica ese tipo de actos.

“Colombia es un país capitalista y patriarcal. En esos modelos se encuentra es un desvalor el trabajo de las mujeres, un ver a las mujeres como propiedad de los hombres, y esos dos aspectos, hacen un caldo de cultivo muy propicio para que haya ese tipo de violencias”, explica Adriana Benjumea, directora de la Corporación Humanas. ([Le puede interesar: la violencia contra las mujeres no cesa)](https://archivo.contagioradio.com/la-violencia-contra-las-mujeresen-colombia-no-cesa/)

### **¿Y las leyes? ¿y los tratados internacionales?** 

Como se constata desde ONU Mujeres, Colombia ha ratificado todos los tratados internacionales vigentes sobre derechos humanos de las mujeres, y ha progresado significativamente en el desarrollo de leyes para promover la igualdad de género y con ello, combatir las violencias. Sin embargo, los crímenes contra las mujeres continúan ocupando los titulares de los medios del país.

En los últimos años, se han logrado leyes como 1257 del 2008, sobre no violencias contra las mujeres, y en el 2015, se tipificó el delito de feminicidio. Pero esto no ha sido suficiente, y organizaciones como Sisma Mujer, advierten que **los índices de violencia van en aumento.**

Lizbeth Cortés Mora, abogada de Sisma Mujer, indica que la situación es muy preocupante porque no hay una mejora a pesar de que la normatividad es muy robusta. La abogada advierte que el Estado debe investigar por qué se presenta este fenómeno y tomar las medidas necesarias interviniendo de manera oportuna en el fenómeno de las violencias contra las mujeres en ámbitos domésticos, laborales y educativos.

Tras el asesinato de Yuliana Samboní, la Red Nacional De Mujeres ‘Porque Vivas Nos Queremos’, evidenció **los obstáculos que afrontan las mujeres para dejar de ser violentadas**: el desconocimiento de la normativa, la falta de acceso a la justicia, la re-victimización de las mujeres por parte de los funcionarios, la legitimación social y naturalización de las violencias, la falta de acciones concretas por parte del Estado, entre otros.

\[caption id="attachment\_37065" align="aligncenter" width="2048"\]![Paro de Mujeres](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/03/parodemujeres.jpg){.size-full .wp-image-37065 width="2048" height="1365"} Paro de Mujeres - Proyecto 341\[/caption\]

### **Los retos** 

De acuerdo con los datos emitidos en 2015 por la Fiscalía General de la Nación, en los últimos 10 años en Colombia se han abierto 34.571 procesos relacionados con feminicidio, de los cuáles sólo en 3.658 casos se han presentado condenas, lo que indica un porcentaje de **impunidad del 90%.**

El reto para este año se alinea con la firma del acuerdo de paz en Colombia, acompañado de un contexto en el que los países del continente americano parecen retroceder en materia derechos de las mujeres. **“Discursos misóginos como el Donald Trump  pueden retrasar los derechos de las mujeres, pero no se puede permitir perder el terreno que se ha ganado”,** manifiesta la abogada de Sisma Mujer.

Cortés afirma que es importante “avanzar desde el discurso de la sensibilización en enfoque de género, para llegar a la idea de que no se trata de un favor, sino de un cumplimiento de deberes constitucionales, y por tanto, un deber de los funcionarios del gobierno aplicar el enfoque de género”.

Para Adriana Benjumea, **el 2017 pone una agenda para el movimiento de mujeres y feminista muy amplio,** enmarcado fundamentalmente en la implementación de los acuerdos de paz, pero además en las transformaciones sociales y políticas que se requieren para erradicar las violencias contra las mujeres. “Queremos poner en el centro del debate, la participación política de las mujeres, no solo desde lo que impone el acuerdo sino también desde la deuda pendiente que tiene el país con la participación de las mujeres.  Se debe  avanzar en propuestas que vayan más allá de las cuotas y que se hable más de la paridad política, no solo en los escenarios institucionales sino en la nueva contienda electoral que ya empiezan a plantearse desde 2017”.

Comprender el papel de las mujeres como sujetas políticas, está directamente relacionado con las violencias de género pues, desde la Corporación Humanas, se considera que si se **garantiza la participación de las mujeres en igualdad de condiciones con los varones,** es posible establecer políticas públicas para erradicar las violencias.

El otro tema es el presupuestal. Los temas de las mujeres son generalmente, los que menos tienen recursos en todos los niveles gubernamentales. “S**e debe impactar los recursos públicos, especialmente en lo que tiene que ver con temas de justicia, derechos sexuales y reproductivos”.** Pero además, debe brindarse las oportunidades laborales y educativas para las mujeres, ya que “quienes no son autónomas económicamente pueden permanecer fácilmente en círculos de violencia”, expresa Benjumea.

Finalmente, y no menos importante, **debe reconocerse las violencias invisibles.** Los micromachismos, y la presión por parte de ciertos grupos representan obstáculos impregnados en la sociedad colombiana, que hoy, en medio de su incapacidad de reconocer esos problemas, reconoce que es inconcebible que se sigan contando víctimas del sistema patriarcal.

![RETOS\_DESIGUALDAD](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/03/RETOS_DESIGUALDAD.png){.wp-image-37537 .aligncenter width="569" height="854"}

###### Reciba toda la información de Contagio Radio en [[su correo]
