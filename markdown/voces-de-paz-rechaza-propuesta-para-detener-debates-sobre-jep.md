Title: Voces de Paz rechaza propuesta para detener debates sobre JEP
Date: 2017-01-26 14:02
Category: Entrevistas, Paz
Tags: FARC-EP, Jurisdicción Espacial de Paz, voces de paz
Slug: voces-de-paz-rechaza-propuesta-para-detener-debates-sobre-jep
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/12/voces-de-paz1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Europa Press] 

###### [26 Ene 2017] 

La vocera Judith Maldonado, de Voces de Paz, rechazó las propuestas que hicieron la senadora Claudia López y el Centro Democrático, sobre detener los debates de la Jurisdicción Especial para la Paz, hasta que las FARC-EP entreguen a los menores de edad que hacían parte de sus filas, ya que considera que no tiene sentido “**que para promover lo acordado se vaya a incumplir lo pactado” sin exigirle de la misma forma al gobierno que cumpla con su parte.**

Además, Maldonado señaló que “el Estado institucionalmente no está capacitado para recibir a los niños”, debido a que  los programas del Instituto de Bienestar Familiar tratan ciertas realidades de maltrato y violencia intrafamiliar que **no acogen las problemáticas particulares del conflicto armado con la que vienen  los menores de edad,** y agregó que el ICBF está imposibilitando el encuentro entre las familias y los niños.

Sin embargo, de acuerdo con Paula Gaviria, alta consejera presidencial para los Derechos Humanos, el gobierno Nacional se encuentra preparado para recibir a los menores de edad desde mayo del año pasado y brindarles todas las garantías de bienestar. A su vez indicó que **“todos los niños que salieron tienen contacto con su familia**”.

El pasado 15 de mayo de 2016, tanto el gobierno Nacional como las FARC-EP establecieron un protocolo para la salida de los menores en donde acordaron que **serían considerados como víctimas del conflicto armado y tendrían un acompañamiento integral** de reintegración, con enfoque comunitario y familiar. Le puede interesar: ["Se acabó la guerra para los menores de las FARC-EP"](https://archivo.contagioradio.com/se-acabo-la-guerra-para-menores-de-las-farc-ep/)

### **Voces de Paz y su responsabilidad con las organizaciones sociales** 

Frente a las alarmas que han encendido diferentes organizaciones sociales y defensoras de derechos humanos sobre la falta de inclusión de las víctimas en los debates y las modificaciones frente a puntos como la amnistía para agentes estatales, Judith Maldonado indicó que tiene que dársele vigilancia a los mecanismos de seguimiento e implementación que existen  y agregó que “**deben buscarse los canales para fortalecer el diálogo y construcción con la ciudadanía y las organizaciones sociales**” a fin de incluir todas las propuestas que propendan por alcanzar la paz.

 De igual forma asevero que se están pensando metodologías como mesas de trabajo para incluir las 24 propuestas que tienen desde la sociedad. Le puede interesar: ["Organizaciones sociales exigen Justicia para todos en implementación de JEP"](https://archivo.contagioradio.com/organizaciones-sociales-exigen-justicia-para-todos-en-implementacion-de-jep/)

### **El sueldo de los voceros de Voces de Paz** 

Otro debate que se ha generado en los medios de información tiene que ver con el sueldo que recibirían los voceros de Voces de Paz y sí sería igual de alto al que reciben los congresistas. Frente a ello Judit Maldonado dijo que **“está absolutamente de acuerdo”** en que sus sueldo no sean tan exorbitantes como el de los congresistas.

“Nosotros como voceros de Voces de **Paz no estamos aspirando a esos sueldos**, y yo, en particular, rechazaría una propuesta de ese tipo, lo que sí está en el acuerdo, es que el gobierno Nacional debe garantizar unas condiciones mínimas para que podamos desarrollar el trabajo”.

Y añadió  “Yo vivo fuera de Bogotá, hemos hecho al menos unos 6 viajes en función del trabajo y **no hemos recibido el más mínimo apoyo**. Estando en el Congreso no tenemos ni siquiera un espacio para trabajar. Esto es anecdótico, en las sesiones del año pasado tuve que imprimir y me tocó buscar favores y ver que secretaria podía imprimir”

Los representantes de Voceros de Paz, **solo participarán en los debates sobre actos legislativos de paz y no tiene competencias para proponer leyes o actos legislativos**.

<iframe id="audio_16670927" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_16670927_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
