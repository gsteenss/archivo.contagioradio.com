Title: Congreso virtual para legislar y hacer control político al gobierno de Duque
Date: 2020-03-18 21:24
Author: AdminContagio
Category: Actualidad, Política
Tags: Congresistas, Congreso, Coronavirus, Iván Duque
Slug: congreso-virtual-para-legislar-y-hacer-control-politico-al-gobierno-de-duque
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/03/Congreso-Virtual.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:heading {"align":"right","level":6,"textColor":"cyan-bluish-gray"} -->

###### Foto: Senado de la República {#foto-senado-de-la-república .has-cyan-bluish-gray-color .has-text-color .has-text-align-right}

<!-- /wp:heading -->

<!-- wp:paragraph -->

Más de 60 congresistas anunciaron este miércoles la creación del congreso virtual, una forma de acatar las recomendaciones para evitar nuevos casos de Covid-19 pero manteniendo las funciones de una de las ramas del poder en Colombia: legislar y hacer control político. Los congresistas estarían aún ajustando los detalles del funcionamiento de esta propuesta, para iniciar su funcionamiento en pleno.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### **El Congreso no se ha disuelto, estamos en una democracia: Iván Cepeda**

<!-- /wp:heading -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/wilsonariasc/status/1240090350468857857","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/wilsonariasc/status/1240090350468857857

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

El senador Iván Cepeda sostiene que como congresistas han manifestado su intención de querer trabajar de manera conjunta y solidaria para superar el enorme desafío que representa el Coronavirus, y tienen voluntad para trabajar de la mano de todas las instituciones de la sociedad colombiana para que ello ocurra.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Al tiempo, manifiesta que el Congreso es una entidad que representa a la ciudadanía y tiene deberes constitucionales que son necesarios, incluso en situaciones excepcionales como la que atraviesa el país. En ese sentido, declara que tienen que seguir cumpliendo con la labor de hacer control político a las decisiones que toma el poder ejecutivo, así como apoyar aquellas decisiones que buscan el bien común.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### **De discusiones de procedimiento a otras de carácter más profundo**

<!-- /wp:heading -->

<!-- wp:paragraph -->

Cepeda señala que los congresistas tienen su estatus válido, y por ahora está discutiendo sobre temas operativos para que el Congreso virtual trabaje en pleno, "puede ser que a través de un decreto o una norma podamos sesionar". No obstante, añade que ya existen grupos de WhatsApp en los que los parlamentarios discuten temas de fondo para ejercer su labor.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por ejemplo, en dichos grupos los políticos han asegurado su preocupación sobre situaciones que les resultan preocupantes, como el que el presidente Iván Duque no haya apelado a un aislamiento riguroso del país para evitar nuevos casos de Covid-19, o que se mantenga abierta la frontera aérea del país.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Para Cepeda, sin tomar decisiones en el sentido de aislar definitivamente a Colombia "se están perdiendo oportunidades para evitar el colapso del sistema de salud", y en dado caso que dichas medidas continúen, el papel del [congreso virtual](https://www.ivancepedacastro.com/se-crea-el-congreso-virtual/) sería clave para corregir el rumbo y proteger vidas. (Le puede interesar: ["Eduación virtual por Covid-19, una decisión para la que no estamos preparados"](https://archivo.contagioradio.com/eduacion-virtual-por-covid-19-una-decision-para-la-que-no-estamos-preparados/))

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78980} /-->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->
