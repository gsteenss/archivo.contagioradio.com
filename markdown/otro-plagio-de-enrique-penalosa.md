Title: Emergencia sanitaria de Enrique Peñalosa está basada en hechos falsos
Date: 2016-11-11 13:46
Category: Nacional, Política
Tags: Alcaldía Enrique Peñalosa, Estado de Emergencia, Plagio de Peñalosa
Slug: otro-plagio-de-enrique-penalosa
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/03/penalosa-100-dias.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo] 

###### [11 Nov de 2016] 

El congresista Alirio Uribe, instauró una denuncia penal en la Fiscalía contra Enrique Peñalosa y el secretario de Salud Luis Gonzalo Morales, por delitos de prevaricato, acción y falsedad ideológica en documento público, al **decretar estado de emergencia sanitaria en la capital basado en una tesis de grado de especialización** de un estudiante y no en un estudio de la Secretaría de Salud como había afirmado el alcalde.

El estudiante, quien para la fecha de expedición del decreto era contratista de la Secretaría de Salud, afirma que su trabajo de grado “es un estudio enfocado a la **capacidad de respuesta de la red pública de urgencia hospitalaria para eventos con** **múltiples víctimas o catastróficos, tipo terremoto o inundaciones, no a otras circunstancias”.**

Mediante documentos obtenidos por derecho de petición, el congresista demostró inconsistencias en las cifras presentadas de la sobreocupación, según Peñalosa se trataba de 250% en la red pública y del 300% en red privada. Las cifras de la Secretaría de Salud, por el contrario, hablan de **84.77% para el año 2015 y de 73.93%, para enero del 2016, un mes antes de ser expedido el decreto.**

Según el representante del Polo Democrático, Enrique Peñalosa emitió dicho decreto sólo con el propósito de **“obtener prerrogativas especiales como contratación directa o la adquisición de empréstitos de manera directa,** que en condiciones ordinarias habían de ser objeto de debate y consecuentemente control en el Concejo de Bogotá como expresión del principio democrático”.

En palabras del congresista, dicha** **crisis sanitaria se hizo para tomar medidas en un estado de emergencia inexistente, “lo que permite concluir que **se querían hacer contrataciones directas sin pasar por la Ley 80 de 1993** la cual expide el Estatuto General de Contratación de la Administración Pública”. Le puede interesar: [Fiscalía abre investigación preliminar contra alcalde](https://archivo.contagioradio.com/fiscalia-abre-investigacion-preliminar-a-enrique-penalosa/)Enrique Peñalosa.

###### Reciba toda la información de Contagio Radio en su correo [[bit.ly/1nvAO4u]
