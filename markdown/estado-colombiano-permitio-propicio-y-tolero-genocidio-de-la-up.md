Title: "Estado colombiano permitió, propició y toleró genocidio de la UP": Reiniciar
Date: 2016-09-15 14:45
Category: DDHH, Nacional
Tags: Aída Abella, Comisión Interamericana de Derechos Humanos, Genocidio UP, Unión Patriótica
Slug: estado-colombiano-permitio-propicio-y-tolero-genocidio-de-la-up
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/09/thousands-murdered-union-patriotica-e1473968718811.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: MIKES BOGOTÁ ] 

###### [15 Sept 2016 ] 

Este jueves en el Palacio de Nariño, el Estado colombiano, en cabeza del presidente Juan Manuel Santos, **reconocerá públicamente su responsabilidad en el exterminio de la Unión Patriótica**, en cumplimiento a los acuerdos pactados en La Habana con la guerrilla de las FARC-EP. Un hecho importante que si bien no resuelve responsabilidades penales individuales por los crímenes cometidos, aporta a las garantías de No Repetición, como asegura la Corporación Reiniciar.

De acuerdo con la abogada Luz Stella Aponte, este reconocimiento **no tiene el alcance para resolver el caso que se tramita ante la Comisión Interamericana de Derechos Humanos**, ni está dirigido a definir la manera en la que se repararía a las víctimas de la Unión Patriótica; sin embargo es un paso importante que se espera pueda revertirse, no sólo en clave del proceso de paz, sino del reconocimiento de la responsabilidad internacional en el marco del litigio en la CIDH.

La base para una reparación satisfactoria de las víctimas de la Unión Patriótica es que el Estado reconozca plenamente su responsabilidad en tanto **permitió, propició, toleró, y de alguna manera auspició el genocidio**, según afirma la abogada, quién agrega que además de mantener estos crímenes en la impunidad, en su momento, no adoptó las medidas necesarias para prevenirlos y tiempo después, no castigó efectivamente a los agentes estatales que desde instancias militares y civiles los determinaron o ejecutaron.

"Es una [[contribución que hacen las víctimas de la Unión Patriótica al proceso de paz](https://archivo.contagioradio.com/la-up-desde-hace-30-anos-dando-el-si-por-la-paz/)]con la esperanza de que el país entre en un proceso de reconocimiento y de pluralismo político que pueda contribuir a la construcción efectiva de la paz" asevera la abogada e insiste en que tendrán que haber medidas de [[reparación individuales, colectivas, y políticas](https://archivo.contagioradio.com/union-patriotica-ha-recuperado-mas-de-4-mil-votos-y-aspira-a-curul-en-concejo-de-bogota/)] pues han sido más de **20 años de litigio en la Comisión Interamericana en los que han sido constantes las dilaciones y la falta de compromiso del Estado** colombiano.

<iframe id="audio_12922018" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_12922018_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)] ] 
