Title: Congresistas denuncian que DEA violó la soberanía colombiana en caso Santrich
Date: 2019-06-06 16:36
Author: CtgAdm
Category: Judicial, Política
Tags: DEA, JEP, Nestor Humberto Martínez, Senadores
Slug: denuncian-dea-violo-soberania-santrich
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/Diseño-sin-título-2.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio  
] 

El pasado miércoles, los senadores Iván Cepeda y Antonio Sanguino denunciaron al exfiscal Néstor Humberto Martínez, así como a agentes de la Administración para el control de Drogas (DEA por sus siglas en inglés) de Estados Unidos, por acciones cometidas en el caso de Zeuxis Pausias Hernández Solarte, más conocido como Jesús Santrich. Los senadores entablaron la acción judicial contra el fiscal por encubrir pruebas a la Jurisdicción Especial para la Paz (JEP) sobre Santrich, al tiempo que señalaron irregularidades cometidas por la DEA en operaciones desarrolladas en Colombia.

### **¿La DEA actúa de forma aútonoma en Colombia?**

El senador **Iván Cepeda** denunció en el debate de control político "entrampamiento a la justicia transicional y derecho a la verdad para las víctimas" que se realizó el miércoles, que desde hace dos años se han venido realizando operaciones sin autorización, y desconociento la Constitución del país por parte de agentes extranjeros para realizar entrampamientos. Una modalidad, en la que **se usa sujetos en cubierto para incitar a personas a cometer delitos;** forma de proceder que se considera ilegal en la justicia colombiana pero que es ampliamente utilizada en Estados Unidos.

A la denuncia de ambas ilegalidades (agentes extranjeros operando autonomaménte, y acudiendo al entrampamiento) se sumó la denuncia por los presuntos delitos de fraude a resolución procesal y ocultamiento de pruebas a cargo del exfiscal Néstor Humberto Martínez en el caso de Jesús Santrich. Como recordó Cepeda, Martínez no informó a la JEP sobre todas las pruebas que había contra Santrich, hasta que la Jurisdicción otorgara la garantía de no extradición al procesado.

El vídeo del que habla el Senador apareció después que la JEP tomara esta determinación, y allí se observa a unos agentes encubiertos de la DEA hablando sobre un negocio, que no se puede determinar como ilegal; pero que según la Ficalía, incluiría el envío de 10 toneladas de Cocaina a los Estados Unidos. Por esta razón, tanto Sanguino como Cepeda, denunciaron también a los agentes encubiertos que participaron en la operación. (Le puede interesar: ["¿Qué viene para Santrich tras recuperar su libertad?"](https://archivo.contagioradio.com/que-viene-para-santrich-libertad/))

### **Las preguntas que quedan tras el debate**

Los senadores señalaron que se trataría de una serie de operaciones, entre las que estaría la desarrollada entre junio de 2017 y abril de 2018 que terminó en la captura de Santrich, así como la que permitió la captura de Carlos Bermeo, fiscal de la sección de investigación y acusación de la JEP. Pero **llamaron la atención sobre la ausencia de respuestas sobre el número total de operaciones, los agentes implicados en ellas y su trascendencia**.

De esta manera, plantearon varias preguntas que esperan ser resueltas: **¿Cómo se organizó la operación en la que terminó capturado Santrich? ¿Quiénes eran los agentes comprometidos de la DEA y cuántos eran?** ¿Por qué no se legalizó esa información? ¿Por qué se demoró en salir el último vídeo que publicó la Fiscalía sobre el caso? Y, ¿por qué se cuestionó en otro de los vídeos sobre magistrados de la JEP?

### **La DEA actúa como si tuviera autonomía plena**

Para finalizar, Cepeda anunció que se desarrollarían otros debates mientras esperan que la denuncia 'prospere', para aclarar la situación; no obstante, concluyó que en su opinión, parece que **"la DEA actúa sin ninguna clase de controles, como si tuviera autonomía plena", en medio de una violación de nuestra soberanía**. (Le puede interesar: ["La extradición, una violación a la soberanía de Colombia"](https://archivo.contagioradio.com/extradicion-violacion-soberania-colombia/))

<iframe id="audio_36782398" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_36782398_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
