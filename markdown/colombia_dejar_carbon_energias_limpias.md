Title: Colombia tiene el camino abierto para dejar el carbón y transitar hacia las energías limpias
Date: 2017-11-18 01:17
Category: Ambiente, Voces de la Tierra
Tags: Alemania, Bonn, COP 23, Cumbre del Clima, Drummond, El Cerrejón
Slug: colombia_dejar_carbon_energias_limpias
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/04/IMG_2023.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Censat Agua Viva] 

###### [17 Nov 2017] 

[Aunque el gobierno colombiano se mantiene en la convicción de señalar que la extracción de carbón es necesaria para la economía colombiana, el país deberá despedirse del carbón. En el marco de la Cumbre del Clima, en Bonn, Alemania, más de veinte países anunciaron que dejarán de usar dicho mineral como combustible para generar energía eléctrica, pues **el objetivo es que el 80 % de las reservas globales fósiles se queden bajo tierra.**]

[Para el economista y experto en la materia, Alvaro Pardo, se trata de un anuncio que se veía venir desde hace dos o tres décadas, cuando el mundo empezó a tener la necesidad de iniciar la sustitución de combustibles fósiles. Sin embargo, el gobierno parece haberse olvidado de la preocupación universal sobre el tema del cambio climático, y en cambio ha promovido el otorgamiento de licencias ambientales para extracción de minerales, como una de las principales políticas del gobierno.]

### **Desventajas económicas frente al carbón** 

[Aunque se tiene previsto que para el 2025 más de 20 países dejarán de comprar carbón colombiano, el economista incluso señala que hace apenas un par de semanas una comisión encabezada por el Ministerio de Minas y Energía, junto a Silvana Habib ,   presidenta de la Agencia Nacional de Minería, y los presidentes de Carbones El Cerrejón, Drummond y Prodeco estuvieron en Europa, “**El gobierno fungió como agente comercial para hacer lobby con Holanda y Alemania, para que siguieran comprando carbón** con el argumento de que si dejaban de comprar carbón,  Colombia caería en una crisis profunda”, denuncia.]

[No obstante, si bien en Colombia hay importantes exportaciones de carbón a 34 países, Pardo explica que dada la caída de los precios del carbón desde el 2002 en adelante, este sector se ha convertido en uno de los menos significativos para el PIB y la generación de regalías. **En 2016, el sector minero apenas contribuyó en un  2,1% del PIB.**]

**Una puerta abierta para las energías limpias**

[Según el experto, debido a los beneficios tributarios, el aporte del sector minero y puntualmente el de carbón es muy bajo. Así gobierno siga insistiendo en vender carbón “**sino hay compradores no hay ventas”.** En cambio existen otros sectores con mayor potencial de los cuales se puede sacar provecho económico. ]

[Teniendo en cuenta dicho panorama, es claro que gradualmente los países van a dejar de comprar carbón, por lo cual se abre una puerta para que las energías limpias ocupen un papel preponderante en Colombia. Además, **por la posición de Colombia en el trópico, es posible desarrollar energías limpias, como la solar, y la eólica,** gracias a los avances tecnológicos eólicos. “Es obvio que las empresas no quieren dejar enterradas sus utilidades, pero necesitamos avanzar conforme a las preocupaciones universales”, concluye el analista.]

<iframe id="audio_22142571" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_22142571_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
