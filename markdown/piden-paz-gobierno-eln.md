Title: Organizaciones Sociales piden gestos de paz al Gobierno y el ELN
Date: 2018-09-17 15:19
Author: AdminContagio
Category: Nacional, Paz
Tags: Comandante Uriel, ELN, Gobierno, negociación, paz
Slug: piden-paz-gobierno-eln
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/07/paz-e1468867771732.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivos Contagio Radio] 

###### [17 Sep 2018] 

Organizaciones y plataformas sociales agrupadas en el movimiento **"Por la Paz"**, piden al Gobierno Nacional **que sea más flexible en sus posturas sobre la mesa de diálogo con el ELN** para que cese la violencia en los territorios, y al grupo armado,  que ofrezca nuevos gestos de paz para que las conversaciones continúen.

Desde ese Movimiento, reclaman que se cree un canal de comunicación directo entre las partes, y **que haya disposición del Gobierno y la guerrilla para crear un ambiente favorable hacia la paz**, el respeto por la vida y por los movimientos sociales. (Le puede interesar: ["ONU saluda liberación de las personas retenidas"](https://archivo.contagioradio.com/onu-saluda-liberacion-eln/))

### **"Que se mantenga la negociación y que se garantice la participación de la ciudadanía en el proceso"** 

**Marylen Serna, vocera de Congreso de los Pueblos e integrante de Por la Paz**, pidió al Gobierno al ELN que tengan más muestras de paz para que las negociaciones continúen. Igualmente, pidió **que se incluya la participación de la ciudadanía en la Mesa** para escuchar al movimiento popular como a las comunidades que sufren la violencia, y piden construir un ambiente de apertura democrática, diálogo y paz.

Adicionalmente, Serna sostuvo que **es desafortunada la retención de una menor de edad por parte del Frente de Guerra Occidental Omar Gómez, en Chocó.** Hecho que se suma a la llegada de los padres de la menor al campamento guerrillero pero que, según el ELN, no están retenidos por el Frente que comanda el "Comandante Uriel". (Le puede interesar: ["Duque, no le pare bolas a los que le hablan de la guerra sino a los que le hablamos de la paz"](https://archivo.contagioradio.com/duque-no-guerra-sino-paz/))

A través de su cuenta de Twitter, el grupo armado confirmó que la captura de la menor se produjo en razón de que las fuerzas armadas la involucraron en el conflicto armado, retención que para la activista por la paz resulta contradictoria, pues el ELN había respondido a un "clamor general" de liberar personas, con los procesos unilaterales que se habían realizado en Arauca y Chocó.

> [@UrielComandante](https://twitter.com/UrielComandante?ref_src=twsrc%5Etfw) ELN entrega audio sobre el caso de niña usada por el [@COL\_EJERCITO](https://twitter.com/COL_EJERCITO?ref_src=twsrc%5Etfw) como informante en el Chocó. [@IvanDuque](https://twitter.com/IvanDuque?ref_src=twsrc%5Etfw) podés pararte de cabeza, hacer pucheros, pero si seguís usando niños en la guerra, el ELN te denunciará, manganzón. 👉 vía: [@PerseoRomani](https://twitter.com/PerseoRomani?ref_src=twsrc%5Etfw)
>
> — ELN Colombia (@ELN\_RANPAL\_COL) [16 de septiembre de 2018](https://twitter.com/ELN_RANPAL_COL/status/1041444846345625601?ref_src=twsrc%5Etfw)

<p>
<script src="https://platform.twitter.com/widgets.js" async charset="utf-8"></script>
</p>
Entre tanto, el Presidente ha sostenido en repetidas ocasiones, que mientras el ELN no haya liberado a todas las personas retenidas, no continuará la mesa de diálogos en La Habana. No obstante, **Duque ha sido criticado por el enfoque con el que está asumiendo el conflicto y las negociaciones de paz.** (Le puede interesar: ["Por ahora, Duque no quiere la negociación sino la rendición: Victor de Currea"](https://archivo.contagioradio.com/duque-quiere-rendicion-eln-currea/))

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
