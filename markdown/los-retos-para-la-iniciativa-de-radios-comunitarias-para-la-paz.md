Title: Los retos para la iniciativa de Radios Comunitarias para la Paz
Date: 2017-02-12 23:09
Category: Nacional, Paz
Tags: Implementación de los Acuerdos de paz, Prensa Rural, Radios Comunitarias, Radios comunitarias para la Paz y la Convivencia
Slug: los-retos-para-la-iniciativa-de-radios-comunitarias-para-la-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/02/radios_comunitarias.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: ACIN] 

###### [12 Feb 2017] 

El Ministerio de Cultura dio inicio al proyecto Radios Comunitarias para la Paz y la Convivencia, que favorecerá a 450 emisoras comunitarias del país, sin embargo distintas organizaciones comunitarias han señalado que sólo **promueve actividades de capacitación y financiación para aquellas emisoras que ya cuentan con licencia de funcionamiento.**

El proyecto al que se destinaran **\$1.200 millones de pesos, otorgados por la Unión Europea y que serán ejecutados por la Red Cooperativa de Medios de Comunicación Comunitarios de Santander** –RESANDER–, tiene como objetivo promover el trabajo sobre los acuerdos de La Habana y la “pedagogía para el posconflicto", según el Alto Comisionado para la Paz, Sergio Jaramillo, la Ministra de Cultura Mariana Garcés Córdoba y el Viceministro de las TIC Juan Sebastián Rozo.

### La iniciativa se queda corta 

Organizaciones que tienen trabajos con comunidades en distintas regiones alrededor de la construcción de paz territorial, han manifestado que aunque el proyecto busca el fomento de las emisoras comunitarias en las regiones, **“se queda corta esta iniciativa puesto que los requerimientos propuestos, pocas emisoras los pueden cumplir”.**

Han indicado que es necesario que el Ministerio de Cultura haga llegar la iniciativa a “cientos de proyectos de comunicaciones en los más apartados rincones del país” que han esperado bastante tiempo para este tipo de convocatorias , y es “responsabilidad del Estado que se **facilite el proceso de otorgar licencias de funcionamiento para sus radios comunitarias”. **([Le puede interesar: Radios comunitarias se encuentran en la construcción de vida digna](https://archivo.contagioradio.com/radios-comunitarias-se-encuentran-en-la-construccion-de-vida-digna/))

Distintos proyectos de organizaciones campesinas y movimientos sociales que le han apostado a la construcción de paz y “que esperan operar desde la legalidad con los pocos recursos con los que cuentan” reciben la convocatoria como un espacio cerrado que **“en momentos de paz deberían ser más abiertos a la comunidad”.**

Por último, el Ministerio de Cultura ha dicho que paralelo a al proyecto de 'Radios Comunitarias', ya se abrió la convocatoria pública ‘Así suena la paz en las regiones’, que estará vigente hasta el próximo 10 de marzo y destinará recursos de **2 millones de euros durante** **18 meses a 50 emisoras comunitarias,** "que servirá para infraestructura tecnológica, producción y transmisión de programas con contenidos sobre paz y convivencia".

<iframe id="audio_16996637" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_16996637_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
