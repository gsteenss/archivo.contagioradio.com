Title: Principales victimarios de violencia sexual han sido militares y paramilitares
Date: 2015-08-04 16:39
Category: Mujer, Otra Mirada
Tags: comsión de la verdad, conflicto armado, FARC, habana, mujeres, paz, proceso de paz, Subcomisión de género, víctimas de violencia sexual, violencia sexual
Slug: principales-victimarios-de-violencia-sexual-han-sido-militares-y-paramilitares
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/08/cuba1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: [cb24.tv] 

<iframe src="http://www.ivoox.com/player_ek_6006128_2_1.html?data=l5WdmJaWfI6ZmKiak5yJd6KnmpKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRiqLGpJDd1NTUs8%2FZjMjfx8bWb8jm1tXcjdbZqYzdz9vS1dnNq9bZjM3Sxc3Tt4zYxpDjy9TQqY6ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Ángela María Robledo, representante a la cámara] 

La delegación de paz de la guerrilla de las FARC propuso la creación de una comisión que investigue los hechos de violencia sexual cometidos en el marco del conflicto armado en Colombia.

“Me parece muy bien que se exija conocer cuáles han sido los principales victimarios que han usado el cuerpo de la mujer como instrumento de guerra”, afirma la representante a la cámara Ángela María Robledo, quien agrega que de acuerdo a los datos e investigaciones de Medicina legal y la Casa de la Mujer, aunque la guerrilla si ha cometido ese tipo de crímenes, **los militares y los paramilitares han sido los grupos que más han usado el cuerpo de la mujer en el conflicto armado y además cuentan con impunidad del 98%.**

De acuerdo con la congresista, aunque Vivian Morales había dejado constituida una unidad especial para investigar la violencia sexual contra las mujeres, los resultados de esa unidad de trabajo son prácticamente nulos, “los militares y paramilitares tienen aproximadamente **60 denuncias y apenas se habla de que va haber dos condenas por violencia sexual”.**

En ese sentido, en un comunicado de prensa las FARC proponen “la conformación de un equipo técnico-investigativo… Pedimos una investigación seria e independiente que deberá describir todo el universo de las víctimas de violencia sexual en Colombia y descubrir los hechos de modo, tiempo y lugar en que se dieron", dijo Pablo Catatumbo.

La representante asegura que es necesario que se desarrolle un componente psicosocial para atender a las mujeres víctimas de violencia sexual, “**las mujeres están dispuestas a exigir una justicia restaurativa, lo que quieren las mujeres es reconstruir su vida”**.

La comisión de verdad y esclarecimiento van a ser nueve personas las nominadas, seis nacionales y tres internacionales y ellas nominaran a otras diez personas que tengan criterios de honorabilidad y reconocimiento para que hagan parte de esta comisión. "Esperamos que el 50% de esta comisión esté conformado por mujeres" dice la congresista.

Según un estudio de ICTJ, International Center for Transitional Justice, **de 39.546 delitos confesados por paramilitares solo 96 de referían a la violencia sexual.** Para Robledo, lo importante para esclarecer los hechos que tienen que ver con la violencia sexual, es que los operadores de justicia sean formados para atender estos casos, ya que estos han naturalizado que las mujeres sean botín de guerra.
