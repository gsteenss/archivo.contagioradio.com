Title: "El arte puede sanar y esa es la apuesta hacia el perdón y la esperanza” Marta Gómez
Date: 2016-09-08 14:59
Category: Mujer, Nacional
Tags: Cultura para la paz, Marta Gómez, Paz al parque
Slug: el-arte-puede-sanar-y-esa-es-la-apuesta-hacia-el-perdon-y-la-esperanza-marta-gomez
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/09/semana.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Semana] 

###### [08 Sept 2016] 

La artista Marta Gómez será una de las voces del concierto Paz al Parque, que se realizará este 9 de septiembre en la Plaza de Bolívar, como parte de las iniciativas que buscan apoyar la votación por el sí al plebiscito por la paz. La cantautora de “Para la guerra nada” **dejo en firme su apoyo y construcción hacia la paz desde la cultura y la música.**

Después del anuncio del inició de las campañas del plebiscito por la paz, la creatividad y las diferentes expresiones culturales han empezado a desempeñar un papel fundamental, esto según la artista se debe a que “[el arte es una bonita manera de sentir al otro y vernos reflejados en el otro](https://archivo.contagioradio.com/mas-de-600-ninos-y-ninas-en-ciudad-bolivar-cabalgan-por-la-paz/), ese mismo sentimiento lo vamos a intentar transmitir en este proceso político”.

Oriunda de Cali, Marta Gómez relata que creció escuchando la trova cubana, al igual que a Mercedes Sosa, Piero y León Gieco, grandes influencias que la llevaron a comprender desde muy pequeña que **la música debía ser un canal para hablar y darle voz a quienes no pueden catar porque se les niega el derecho o porque no tienen la posibilidad**. Motivo por el cual ahora siente la responsabilidad de contar las historias de quienes no tienen un micrófono en frente.

Frente a la situación actual que afronta Colombia, la cantautora afirma que **“el país se está dando cuenta que el arte puede sanar y que esa es la apuesta, hacía el perdón y la esperanza”.** La artista afirma que la música siempre ha sido su mejor herramienta para hacer la paz, para cantas sobre los que tienen menos o de las personas que sufren, pero también ha sido su forma de llevar esperanza.

El evento iniciará a partir de la una de la tarde y contará con la participación de otros artistas como León Gieco, Maía, Mesieur Perine, Doctor Krápula, entre otros. Cabe resalta que el concierto hace parte de una iniciativa del artista italiano [Piero, que recorrerá otros rincones de Colombia como parte de su aporte a la construcción de paz](https://archivo.contagioradio.com/piero-se-une-a-la-celebracion-de-la-paz-en-colombia/) y de las actividades realizadas nacionalmente en la semana que conmemora la paz en Colombia.

<iframe src="https://co.ivoox.com/es/player_ej_12831994_2_1.html?data=kpellZadfZWhhpywj5WZaZS1lpaah5yncZOhhpywj5WRaZi3jpWah5ynca7V09nVw5CraaSnhqegz8reaZO3jKjO0NnFudXj08aah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ..&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 

 
