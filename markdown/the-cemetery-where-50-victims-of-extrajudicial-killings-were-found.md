Title: The cemetery where 50 victims of extrajudicial killings were found
Date: 2019-12-18 12:29
Author: CtgAdm
Category: English
Tags: extrajudicial killings, false positives, Las Mercedes, Special Peace Jurisdiction
Slug: the-cemetery-where-50-victims-of-extrajudicial-killings-were-found
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/Cementerio-de-Dabeiba.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Photo: @JEP\_Colombia] 

 

**Follow us on Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

[This weekend, the Special Peace Jurisdiction (JEP) announced the discovery of a mass grave where at least 50 victims of extrajudicial killings were found in the Las Mercedes Cemetery in Dabeiba, Antioquia. In an interview with Contagio Radio, Adriana Arboleda, director of the Judicial Liberty Corporation and spokesperson for the Movement of Victims of State Crimes (Movice), explained how this discovery came about and why these cemeteries must be protected in order to prevent the destruction of evidence.]

### **Contagio Radio: How was the JEP able to intervene the cemetery in Dabeiba?**

[Adriana Arboleda: It was two things: First, the JEP opened Case 003, which involves states agents responsible for “Deaths illegitimately presented as combat kills by agents of the state.” Then, MOVICE requested precautionary measures on August 30, 2018 that we then asked be amplified this year to include cemeteries located in the municipality of Dabeiba, in western Antioquia. Both things came together because within Case 003, one of the state agents that appeared before the JEP said he had participated in an extrajudicial killing and that many of the victims had been buried in the cemetery in Dabeiba, which coincided with information that MOVICE had presented. This is what caused the JEP to intervene in this place to corroborate the information and to advance in the identification of the cases. ]

### **CR: In interviews with communities in Dabeiba, they have said that there are other areas where there also disappeared buried. Has MOVICE identified these areas?**

**AA:** [What they say is completely true. In fact, in the request we presented we are talking about other cemeteries. In the town, near the Catholic cemetery Las Mercedes, is a cemetery administered by the Presbyterian Church that we don’t know if it could have been used, due to its proximity, by the military men. There’s also the cemetery in the rural area of El Salado and above all else, there are three areas, Camparrusia, San José de Urama and Balsita, that we’re interested in.]

[The Judicial Liberty Corporation, the MOVICE and the Interchurch Justice and Peace Commission have denounced on various occasions that these places were areas where people were murdered and presented as combat kills or people were disappeared. That’s why we believe that these three areas are fundamental to intervene.]

[These places need to be protected to prevent that they be intervened or manipulated, which appears to be the case in the cemetery of Las Mercedes. We don’t think it’s just the cemetery located in the town, but also the Cañón de La Llorona, even other nearby municipalities, because this was a corridor in which paramilitaries operated. In fact, when we went to the cemetery in Las Mercedes there were testimonies of paramilitary members between 1996 and 1998 that said that they buried their victims there. But different battalions have also operated in this area, such as the First Division, the Seventh Division of the 17th Brigade.]

[In this area, there have been more than 300 victims of forced disappearances, so it’s very important what the locals indicate that the intervention must be integral, and this cemetery should be looked at as well as a series of other places.]

### **CR: What does the findings in the cemetery in Las Mercedes say about how the false positive cases were carried out in the country?**

[AA: There are different types of victims. There are direct victims of forced disappearances, that is, people of the region who were detained arbitrarily, who were subtracted from the protection of the law, hidden, assassinated and surely, taken to these cemeteries. There are also victims of combatants, who were executed when they were defenseless and were taken to these places, that is, that they were no attacked during combat but rather when they were killed once they were captured without a trial nor any protection, which is a clear violation of international humanitarian law. Lastly, there are cases of extrajudicial killings that according to the witnesses, and at least the military man that is offering information on Las Mercedes, correspond to people who were recruited in Medellín, were taken and assassinated in the rural areas of Dabeiba and buried there. ]

But we also have cases of people who are from the region, such as La Balsita, San José de Urama and Santa Rita, who were assassinated and also presented as combat kills. We have a series of victims, which are important to look into and to implicate the responsibility of paramilitary groups that acted in collusion with the Armed Forces, and acts committed directly by state agents. In this case, executions.

### **CR: There are other places in Colombia, such as Ituango and the Comuna 13 where people say there are disappeared. Which places should be examined to find the disappeared?**

[AA: MOVICE has presented a request to search 17 places in five provinces. In the case of Antioquia, there is the Comuna 13, the Universal Cemetery, the cemeteries that are being affected by Hidroituango, the cemetery of Puerto Berrío. Recently, we left a hearing on the cemeteries of San Onofre and Rincón del Mar in Sucre. We have requested that cemeteries in Caldas and in Santander be searched. But I think it's important now for the Search Unit for Disappeared Persons, along with the JEP, should design a model of intervention in the cemeteries in the areas where the armed conflict existed or in the areas where we have reported places where there are many victims of forced disappearances or extrajudicial killings.]

It's important to cross-check information and to guarantee the protection of these areas. There is now a "boom" over the cemetery of Dabeiba, but there is an urgent need to design a plan of action to prevent that these places where there are victims of extrajudicial killings and forced disappeareances be altered because the evidence that we have in the cemetery of Dabeiba was altered, possibly by military men. Consequently, the National Search Plan, designed by the Search Unit for Disappearead Persons, must define a protection protocol for all these places.

### CR: What do you mean when you say military men possibly altered this cemetery?

[AA: Last week when we were at judicial hearings, the same military man who offered information said when he arrived at the cemetery, it wasn't the same way he last saw it in 2006 and 2007. Apparently, the crosses were facing another direction because when he was last there they faced south and now they faced east. We realized that the crosses were painted white in the same handwriting. All appeared as deaths from the years 1972 to 1974 and all the crosses in these places are in the same place or close to where the military man said the victims had been buried or places where there is information that paramilitaries buried victims.]

Later, when we inquired, we were told that two years ago military men arrived to organize the cemetery. That they had fixed it up, that they had painted it. We think that is an issue that should be investigated because we think that the military could have removed bodies as a way to guarantee impunity.

### **CR: How many people could be buried in the places MOVICE requested be protected and searched?**

It's difficult to say, but in the Comuna 13 we have a list of close to 300 victims. In Dabeiba, we have close to 100 victims reported. It's very complicated and difficult to commit to one number. That is, we have more than 80,000 victims of forced disappearances in this country, and what we need to do now is to focus on the cross-checking of information. I insist that it is important to see how the Search Unit coordinates this action because one of the realities in Colombia is that we don't have a consensus yet on the number of the victims.

Even in the case of Dabeiba, or that of San Onofre or other places, many of the cases appear as unidentified people, so the real challenge is enormous, but what is encouraging is that we have started to take steps and to clarify certain things that will allow  us to guarantee the rights of the victims to know the truth and that at some moment, return the bodies of the disappeared to their families.

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
