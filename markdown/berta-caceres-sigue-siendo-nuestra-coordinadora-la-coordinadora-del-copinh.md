Title: "Berta Cáceres sigue siendo nuestra coordinadora, la coordinadora del COPINH"
Date: 2017-03-02 13:02
Category: DDHH, El mundo
Tags: asesinato Berta Cáceres, Berta Cáceres, Copinh
Slug: berta-caceres-sigue-siendo-nuestra-coordinadora-la-coordinadora-del-copinh
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/03/BertaCaceres.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Univisión] 

###### [2 Mar 2017] 

Ha pasado un año desde el asesinato de la defensora ambiental y de derechos de los pueblos indígenas, Berta Cáceres, y aún el crimen no se ha resuelto, aunque hasta la fecha hay 8 personas capturadas por su participación material, **todavía las autoridades hondureñas no llegan a los autores intelectuales.** El Consejo Cívico de Organizaciones Populares e Indígenas, del que Berta era coordinadora general, afirmó que continuaran movilizándose y exigiendo verdad y justicia en el caso de la líder hondureña.

Tomás Membreño, también integrante del COPINH, señaló que desde lo sucedido el Movimiento no ha descansado en la lucha por la verdad en el caso de la lideresa "Berta no ha muerto, **Berta se ha multiplicado (...) hay que defender la vida, a los defensores de la tierra** (...) tenemos que seguir multiplicando el ejemplo de Berta Cáceres". ([Le puede interesar: “Asesinato de Berta Cáceres demuestra el deplorable estado de los DD.HH En Honduras”](https://archivo.contagioradio.com/berta_caceres_asesinato_un_ano/))

También comentó que Berta "era una luchadora integral, no sólo era ambientalista, también era feminista, estaba en contra del sistema", asegura que "a 1 año del asesinato de Berta, el Estado no ha respondido porque involucraría al poder político"   y resalta que su trabajo seguirá siendo rememorado, **"Berta Cáceres sigue siendo nuestra coordinadora, la coordinadora del COPINH". **

###  

En recientes declaraciones a medios locales, Olivia Zúñiga, hija de Berta, hizo fuertes criticas a los mecanismos de investigativos y de justicia del Estado hondureño, además hizo referencia al último informe de Global Witnees, que documenta los crímenes contra los ecologistas en el mundo y **señala que a Honduras como uno de los lugares "más peligroso para defender el mundo".** En su declaración, resaltó la necesidad de proteger el legado de su madre “en medio de un ambiente hostil como el que se vive en Honduras”.

El periódico británico The Guardian, reveló que los acusados del asesinato de Berta Cáceres, **“recibieron entrenamiento contrainsurgente en Estados Unidos y pertenecían a las fuerzas de inteligencia militar de Honduras”**, de acuerdo a lo reflejado en el expediente jurídico del caso. ([Le puede interesar: Gerente de DESA directamente vinculado con asesinato de Berta Cáceres en Honduras](https://archivo.contagioradio.com/desa-asesinato-de-berta-caceres-honduras/))

Tiempo atrás, el único testigo de la muerte de Cáceres, Gustavo Castro, señaló al Estado hondureño como responsable del crimen, además, las evidencias contenidas en el expediente **apuntan a la hipótesis de que la muerte de la activista indígena fue una ejecución extrajudicial.**

###  

Durante años, la lideresa indígena recibió múltiples amenazas de muerte por su labor en defensa de la tierra del pueblo Lenca y por oponerse férreamente al proyecto hidroeléctrico de Agua Zarca en Río Blanco. Olivia, también apuntó que el país centroamericano **“es uno de los primeros en cuanto a feminicidios, y es sumamente peligroso para la defensa de los derechos humanos”**, por lo que hizo un llamado a distintas organizaciones y entidades internacionales, presionar al Gobierno hondureño y “estar vigilantes con las investigaciones”.

Por el caso de Berta hay ocho personas detenidas en proceso de ser judicializadas, pero sus familiares y compañeros de lucha, **exigen la captura de los autores intelectuales del asesinato** que ha causado conmoción en América Latina y otros rincones del mundo.

<iframe id="audio_17321495" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_17321495_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
