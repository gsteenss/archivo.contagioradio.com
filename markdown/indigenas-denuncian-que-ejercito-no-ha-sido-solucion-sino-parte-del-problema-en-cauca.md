Title: Indígenas denuncian que Ejército no ha sido solución, sino parte del problema en Cauca
Date: 2020-01-24 17:01
Author: CtgAdm
Category: Nacional
Slug: indigenas-denuncian-que-ejercito-no-ha-sido-solucion-sino-parte-del-problema-en-cauca
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/01/Ejército-intenta-retener-a-Yesid-Conda.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:heading {"align":"right","level":6,"textColor":"cyan-bluish-gray"} -->

###### Foto: @Marianiniecheve {#foto-marianiniecheve .has-cyan-bluish-gray-color .has-text-color .has-text-align-right}

<!-- /wp:heading -->

<!-- wp:audio -->

<figure class="wp-block-audio">
<audio controls src="https://co.ivoox.com/es/joe-zauca-sobre-indigena-herido-medio-de_md_47057499_wp_1.mp3">
</audio>
  

<figcaption>
Entrevista &gt; Joe Zauca | Coordinador DDHH (CRIC)

</figcaption>
</figure>
<!-- /wp:audio -->

<!-- wp:paragraph -->

El pasado jueves 23 de enero y este viernes habitantes de Corinto, al norte de Cauca han denunciado los enfrentamientos que se están dando entre la Fuerza Pública e integrantes de un grupo armado ilegal, que dejó como saldo un indígena herido. Los enfrentamientos, así como otra serie de hechos ponen nuevamente al Ejército como protagonista del conflicto en el territorio.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Organizaciones indígenas en alerta temprana, ante emergencia humanitaria

<!-- /wp:heading -->

<!-- wp:paragraph -->

El pasado 20 de enero, el Consejo Regional Indígena del Cauca (CRIC) emitió un comunicado en su página en la que se declaran en alerta humanitaria, ante la **llegada a la instalaciones de la Vigésima Novena Brigada del Ejército en Popayán un contingente de 2.500 uniformados.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

La organización explicaba que la militarización del territorio, "con la justificación que da el paradigma de 'legalidad' del actual Gobierno" los convertiría en "carne de cañón" en la confrontación entre armados. [Advertencia](https://www.cric-colombia.org/portal/comunicado-estado-de-alerta-de-los-pueblos-indigenas-del-cauca/) que se habría consumado durante los recientes combates en Corinto, donde un indígena fue impactado con una bala supuestamente perdida, mientras estaba recogiendo arena.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Joe Sauca, coordinador de derechos humanos del CRIC recordó que, además de este hecho, vale la pena traer a la memoria el intento de retención que realizaron fuerzas especiales de la Cuarta Brigada al exgobernador de Pitayó, Yesid Conda. (Le puede interesar: ["No convencen explicaciones del Ejército tras intento de retención a líder Nasa"](https://archivo.contagioradio.com/no-convencen-explicaciones-del-ejercito-tras-intento-de-retencion-a-lider-nasa/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Sauca también denunció la retención de documentos de identidad a guardias indígenas por parte de la Fuerza Pública, y el intento de retención al Coordinador zonal de norte de Cauca, a quien obligaron a parar con su vehículo de la Unidad Nacional de Protección (UNP), alegando que estaba secuestrado.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En un hecho reciente, el Coordinador de DD.HH. señaló que uniformados intentaron detener y trasladar a comuneros de la comunidad del Guanábano, en Corinto, por medio de actuaciones irregulares. Hecho que fue evitado por la autoridad tradicional de la zona y los vecinos. (Le puede interesar:["Comunidades del Cauca responsabilizan al Ejército del asesinato de Flower Jair Trompeta"](https://archivo.contagioradio.com/comunidades-del-cauca-responsabilizan-al-ejercito-del-asesinato-de-flower-jair-trompeta/))

<!-- /wp:paragraph -->

<!-- wp:html -->  
<iframe src="https://www.facebook.com/plugins/post.php?href=https%3A%2F%2Fwww.facebook.com%2Fcric.colombia%2Fposts%2F2185846141517759&amp;width=500" width="500" height="568" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>  
<!-- /wp:html -->

<!-- wp:paragraph -->

Por otra parte, manifestó que en la zona aparecieron varios panfletos con señalamientos a la guardia indígena y el partido MAIS, firmados por las disidencias de la Nueva Marquetalia. Sin embargo, Sauca sostuvo que **se trata de generar temor por parte de las mismas estructuras armadas que operan en la zona.**

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### **Llegada de más Ejército no es la salida, ¿entonces qué lo es?**

<!-- /wp:heading -->

<!-- wp:paragraph -->

Tras la masacre de Tacueyó, último evento grave que marcó la escalada de violencia en Cauca, el presidente Duque anunció que enviaría más tropas al departamento para brindar seguridad a las poblaciones. Sin embargo, tal como lo reitera la reciente alerta del CRIC, las comunidades han señalado que las medidas militares se han mostrado insuficientes para solucionar la violencia en el territorio.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En ese sentido, Sauca declaró que la Defensoría del Pueblo y la Gobernación han estado intentando generar espacios de diálogo con las autoridades indígenas, departamentales y del Ejército para establecer un acuerdo de entendimiento y respeto mutuo. (Le puede interesar:["Autoridades indígenas responsabilizan a Duque de masacre en Tacueyó"](https://archivo.contagioradio.com/autoridades-indigenas-responsabilizan-a-duque-de-masacre-en-tacueyo/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En consecuencia, aseguró que es momento de tomar decisiones y alcanzar **niveles de respeto que reconozca la autonomía de los pueblos indígenas de su territorio,** al tiempo que garantice la protección de la vida por parte de la Fuerza Pública. (Le puede interesar:["Militarizar más al Cauca es una propuesta "desatinada y arrogante": ACIN"](https://archivo.contagioradio.com/militarizar-mas-al-cauca-es-una-propuesta-desatinada-y-arrogante-acin/))

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### **JEP reconoce como víctimas a más de 100 mil indígenas en Norte de Cauca y sur de Valle del Cauca**

<!-- /wp:heading -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/JEP_Colombia/status/1219675706214535171","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/JEP\_Colombia/status/1219675706214535171

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

En medio de la situación de violencia en el Departamento, la Sala de Reconocimiento de la **Jurisdicción Especial para la Paz (JEP) acreditó a más 100 mil personas** en 31 resguardos y cabildos como víctimas, en el marco del caso 05 "sobre la situación territorial en el norte del Cauca y sur de Valle del Cauca".

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En un informe elaborado por la Organización Nacional Indígena de Colombia (ONIC) en 2018 sobre la situación para los pueblos indígenas en materia de derechos humanos se referencia que Chocó (2..253 casos), **Cauca (1.110)**, Valle del Cauca (447) y Antioquia (79) son los departamentos donde se presentan mayor número de infracciones al Derecho Internacional Humanitario contra los pueblos originarios.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

A propósito del reconocimiento como víctimas, Sauca declaró que era un reconocimiento de la JEP al trabajo de más de dos años que han realizado para recopilar la información, así como a las autoridades indígenas que han buscado armonizar el territorio. (Le puede interesar: ["66% de los pueblos indígenas está a punto de desaparecer: ONIC"](https://archivo.contagioradio.com/pueblos-indigenas-a-punto-de-desaparecer-onic/))

<!-- /wp:paragraph -->
