Title: Estado pide perdón por desplazamiento Forzado de comunidad de Inaia Sue, Tenjo
Date: 2018-04-13 14:38
Category: DDHH, Nacional
Tags: das, Desplazamiento forzado, Inaia Sue, Tenjo
Slug: inaia-sue
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/09/acuerdos-de-paz-e1506448818344.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: La Prensa. HN ] 

###### [13 Abr 2018]

Este 14 de abril, se llevará a cabo el acto de responsabilidad y pedido de perdón por parte del Ministerio de Defensa y la Policía Nacional, a la comunidad de Inaia Sue, en Tenjo, Cundinarmarca, por el asesinato de Leonardo Tibiquirá y **el desplazamiento de 32 familias**, producto de acciones violentas del extinto DAS, cometidas en el mes de noviembre de 1997.

### **En 1997 el DAS acusó a la comunidad de ser testaferros de la guerrilla** 

En octubre de 1997 el  director general del antiguo DAS, Luis Enrique Montenegro, acusó a las personas que conformaban la Cooperativa Multiactiva Vecinos y Amigos, que estaba adelantando un proyecto de vivienda Inaia Sue, de pertenecer a la guerrilla. **Hecho por el cual la Fiscalía General de la Nación abrió una investigación** y se produjo un gran despliegue de medios que estigmatizó a la comunidad.

En su defensa los habitantes e integrantes de este proyecto, realizaron comunicados de prensa en donde insistían en su inocencia y pedían que cesaran las agresiones en su contra. Sin embargo, el 16 de noviembre de 1997, **un grupo de personas con armas automáticas, llegaron al conjunto residencial de Inaia Sue y dispararon contra los inmuebles, asesinando a Leonardo Tibaquirá**.

Luego de este acto, lo agresores dejaron en el lugar panfletos y declararon objetivo militar a los residentes de la urbanización, que además, abandonaron sus viviendas en el menor tiempo posible, provocando el desplazamiento de 32 familias. (Le puede interesar: ["Familia Quilcue admiré perdón de Min Defensa, por asesinado de Edwin Legarda"](https://archivo.contagioradio.com/familia-quilcue-admite-perdon-de-ministeriod-e-defensa-por-asesinato-de-edwin-legarda/))

### **La Justicia para Inaia Sue**

En 1998, la investigación penal que se realizaba contra la comunidad dio como resultado que no existían pruebas para las acusaciones hechas por el DAS. El 1 de agosto de 2016, el Consejo de Estado declaró la responsabilidad administrativa del Estado y del entonces Departamento Administrativo de Seguridad (DAS), **y se ordenaron medidas de reparación integrales. **

Entre las medidas, puntualmente el Consejo ordenó una indemnización por daños y perjuicios, un acto de reconocimiento de responsabilidad y pedido de perdón público, una noticia en un medio escrito de amplia circulación y en otro televisivo de amplia emisión en el orden nacional, así como el erigir un monumento en memoria de las víctimas.

### **Un paso a la reparación de la comunidad Inaia Sue** 

Este 14 de abril se dará paso con uno de esos tipos de reparación, con el acto de responsabilidad y pedido de perdón por parte del Ministerio de Defensa y la Policía Nacional, el acto se llevará acabo en el Centro Cultural de Tenjo a las 10:00 am.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 

 
