Title: "Si nos quitan el territorio, nos quitan la vida" campesinos de La Macarena
Date: 2015-09-29 12:42
Category: DDHH, Entrevistas
Tags: Caquetá, Ejército Nacional, Emerland Energy, FARC, La Macarena, Ley de Restitución de Tierras, Meta, predios baldíos, proceso de paz, San Vicente del Caguán
Slug: gobierno-busca-expropiar-tierras-de-campesinos-e-indigenas-en-el-meta
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/Colombia-Meta-Serrania-de-la-Macarena-58_Santiago-Echeverri.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: [www.experienciacolombia.com]

<iframe src="http://www.ivoox.com/player_ek_8686254_2_1.html?data=mZulmJeZeI6ZmKiak5mJd6KmmZKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRi9DWysrf0NSPptbnxMaYx93UttDkysbfjdnNqdPmwtiYxsqPp8Lh0crgy9PTt4zZjM7bxoqnd4a1pcyah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Luz Mery Panche, autoridad indígena en el Caguán] 

**Cerca de seis mil familias campesinas y varios resguardos indígenas, podrían quedar sin tierras** luego de que el gobierno asegurara que recuperaría “tierras de las FARC”, que en realidad pertenecerían a comunidades que llevan más 50 años habitando **277 mil hectáreas** de los departamentos de Caquetá, municipio San Vicente del Caguán, y Meta, municipio de La Macarena.

De acuerdo con Luz Mery Panche, autoridad tradicional de los pueblos indígenas de San Vicente del Caguán, ese anuncio del gobierno frente a la “recuperación de tierras”, se trata de “una estrategia para quitarnos la tierra y dárselas a los grandes terratenientes y a las petroleras”, asegurando que  **gran parte de la región está concesionada a la exploración y explotación de petróleo por parte de la empresa Emerland Energy.**

En el marco de esa situación, desde hoy hasta el 30 de septiembre se llevará a cabo la **"Audiencia Pública por la Tierra, el Territorio y la Paz, en la Macarena**", con el fin de evidenciar que los predios que se piensa expropiar pertenecen a campesinos e indígenas que tienen legalmente constituidos sus resguardos.

**“Vamos a decirle al gobierno que no somos ilegales, que no es territorio de las FARC, el Estado ha sido testigo a través de las personerías jurídicas, el Estado sabe que existimos, que pagamos impuestos por las tierras y que hemos estado acompañados con presencia del Ejército Nacional”**, señala la líderesa indígena, quien añade que son familias que han sido víctimas del conflicto armado.

Por ser territorios baldíos no hay títulos, sin embargo, las familias han hecho todos los trámites necesarios para que se reconozca que los campesinos son los dueños de esas tierras, pero el gobierno es el que no ha permitido que se concrete ese trámite.

Panche, indica que **“lo que quiere el gobierno (por medio del Incoder), es repartir las tierras y aplicar la restitución de tierras sin tocar las tierras de los grandes terratenientes”** a través de la estigmatización de la comunidad que habita ese territorio.

Finalmente, la lideresa indígena, asegura que esas actuaciones del gobierno no son congruentes con el discurso de paz donde supuestamente, las víctimas son el eje central de los diálogos en la Habana. **“Si nos quitan el territorio, nos quitan la vida”**, expresa Panche.

[![audiencia\_publica\_maca](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/audiencia_publica_maca.jpg){.aligncenter .size-full .wp-image-14935 width="468" height="334"}](https://archivo.contagioradio.com/gobierno-busca-expropiar-tierras-de-campesinos-e-indigenas-en-el-meta/audiencia_publica_maca/)
