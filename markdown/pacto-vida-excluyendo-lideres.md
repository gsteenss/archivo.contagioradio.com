Title: Pacto por la vida se firmó excluyendo a líderes que están en riesgo
Date: 2018-08-24 13:07
Category: DDHH, Nacional
Tags: Fernando Carrillo, Iván Duque, lideres sociales, Mesa por la Vida
Slug: pacto-vida-excluyendo-lideres
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/08/Captura-de-pantalla-2018-08-24-a-las-1.00.01-p.m..png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: @IvanDuque] 

###### [24 Ago 2018] 

Aunque el Gobierno nacional, encabezado por los directores de las instituciones encargadas de velar por la protección de los líderes sociales firmó un pacto para la protección y defensa de su vida; a la reunión en Apartadó, Antioquia, en la que se firmó tal compromiso **no se permitió la entrada de los líderes de restitución de tierras, sustitución de cultivos, campesinos, afro e indígenas que están siendo amenazados en la región.**

La 'segunda Mesa por Protección a la Vida', como se denominó la reunión, fue propiciada por el Procurador General, **Fernando Carrillo**, acompañado por el presidente **Iván Duque**, la Vicefiscal General **Paulina Riveros** y el Defensor del Pueblo **Alfonso Negret**; quienes firmaron un pacto de [11 puntos](https://es.scribd.com/document/386978238/Compromisos-Del-Pacto)en el que se comprometen entre otras cosas a crear **"una política pública de prevención y protección integral, con enfoque diferencial, de equidad, étnico y territorial"**, garantizar la vida y seguridad de defensores de derechos humanos, e investigar pronta y efectivamente las amenazas y atentados en su contra.

Por su parte, Duque afirmó que en las próximas semanas presentará una política en la que todas las entidades del Estado defiendan "estructuralmente" la vida de los líderes sociales, periodistas y ciudadanos. (Le puede interesar:["Uriel Rodríguez: Primer líder asesinado en Gobierno Duque"](https://archivo.contagioradio.com/primer-lider-asesinado-gobierno-duque/))

### **Líderes amenazados fueron excluidos de la 'Mesa por la Vida'** 

Sin embargo, **del evento fueron marginados los líderes e integrantes de comunidades afrodescendientes, indígenas y desplazadas** quienes están sufriendo las consecuencias del lento avance en la implementación de los Acuerdos de Paz, la presencia de diferentes actores armados en la zona y el avance de las denominadas **Autodefensas Gaitanistas de Colombia (AGC)** que hacen presencia en la región del Bajo Atrato y el Urabá Antioqueño.

En una carta dirigida al Procurador General, las comunidades le recordaron que aunque "su voz no es la de los representantes legales", son agrupaciones con opinión propia, que forman parte de Consejos Comunitarios; Zonas Humanitarias y de Biodiversidad; Eco Aldeas de Paz; y Resguardos Indígenas reconocidos por la Comisión y la Corte Interamericana de Derechos Humanos.

Así mismo, sostuvieron que desde la última reunión adelantada con el Procurador, en diciembre del año pasado, hay nuevas familias desplazadas, se ha consolidado el despojo empresarial, un incremento en el número de líderes amenazados y  el asesinato de los líderes Mario Castaño y Hernán Bedoya. (Le puede interesar: ["Asesinan a Mario Castaño, líder reclamante de tierras en el Chocó"](https://archivo.contagioradio.com/asesinan-a-mario-castano-lider-reclamante-de-tierras-en-el-choco/))

Por estas razones, las comunidades piden a la Procuraduría una **nueva reunión** en su despacho, en vista de su exclusión de la segunda 'Mesa por la Protección a la Vida'. (Le puede interesar: ["En más del 90% de casos de asesinatos de líderes sociales aún no se alcanza justicia"](https://archivo.contagioradio.com/en-casos-de-lideres-sociales-no-se-alcanza-justicia/))

[Carta al Procurador General Fernando Carrillo](https://www.scribd.com/document/386978312/Carta-al-Procurador-General-Fernando-Carrillo#from_embed "View Carta al Procurador General Fernando Carrillo on Scribd") by [Contagioradio](https://www.scribd.com/user/276652802/Contagioradio#from_embed "View Contagioradio's profile on Scribd") on Scribd

<iframe id="doc_58330" class="scribd_iframe_embed" title="Carta al Procurador General Fernando Carrillo " src="https://www.scribd.com/embeds/386978312/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-Z4fKXl8VRd8J3YD1Tcgg&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="0.7068965517241379"></iframe>

###### [Reciba toda la información de Contagio Radio en ][su correo](http://bit.ly/1nvAO4u) [o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por ][Contagio Radio](http://bit.ly/1ICYhVU) 
