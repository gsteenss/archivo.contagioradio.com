Title: Abucheado Peñalosa durante instalación de Cumbre de premios Nobel de paz
Date: 2017-02-02 11:48
Category: Movilización, Nacional
Tags: abucheado, Bogotá, Peñalosa, revocatoria
Slug: penalosa-revocatoria-bogota
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/02/3373268_n_vir3.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### Foto: Colprensa 

##### 2 Feb 2017 

Durante el discurso de instalación de la Cumbre Mundial de Premios Nobel de Paz, que arrancó la mañana de este jueves en Bogotá, el alcalde de la ciudad, **Enrique Peñalosa resultó abucheado por la multitud que dejó ver su inconformismo con la política del burgomaestre** que a su parecer ha dejado mucho que desear en materia de movilidad, seguridad e inclusión social.

Justo cuando se refería  al tema de las prioridades de la ciudad en cuanto a movilidad, la construcción del metro y la sustentabilidad ambiental, **más de un centenar de personas que asistieron al evento respondieron con chiflidos y gritos de revocatoria**. El alcalde no interrumpió su discurso y por el contrario subió el tono de la voz para intentar opacar el bullicio del público.

Otra de las situaciones que ha molestado a muchos ciudadanos, es la implementación de **medidas de fuerza contra los vendedores ambulantes**. Circulan varios videos en los que se evidencia que la policía ha bloqueado varias rutas de Transmilenio en medio de los operativos en que se **decomisan los productos a vendedores** que trabajan sobre el eje ambiental en el centro de la ciudad.

Adicionalmente, han arreciado las críticas contra el alcalde por sus declaraciones en torno a la temporada taurina y la reapertura de la plaza de toros. Animalistas de todas las corrientes coincidieron en afirmar que no se podía impedir la protesta y que **el distrito está invirtiendo más seguridad en custodiar a quienes “gozan con la tortura” que en proteger a los ciudadanos y ciudadanas** de una serie de criminalidad que está llegando también al sistema de Transmileno y al SITP.

Así las cosas, muchos han afirmado que **la revocatoria sigue tomando fuerza y que podrá tener un impulso cuando el distrito comunique el alza en el costo del transporte público** en Bogotá. Le pude interesar: [¿Cómo califica la administración de Enrique Peñalosa?](https://archivo.contagioradio.com/como-califica-la-administracion-de-enrique-penalosa-frente-a-la-alcaldia-de-bogota/)

<div class="fb-video" data-href="https://www.facebook.com/UnidosRevocamos/videos/375194706191705/" data-width="500" data-show-text="false">

> [Peñalosa es abucheado en cumbre mundial de nobeles de paz](https://www.facebook.com/UnidosRevocamos/videos/375194706191705/)Al "gerente" la gente no le come cuento. Hoy en la cumbre mundial de nobeles de paz fue nuevamente abucheado.  
> \#RevocatoriaPeñalosa \#OtroDíaSinMetro  
> Posted by [Unidos Revocamos a Peñalosa](https://www.facebook.com/UnidosRevocamos/) on jueves, 2 de febrero de 2017

</div>

###### Reciba toda la información de Contagio Radio en [[su correo]
