Title: Tras atentando en Jamundí, comunidades indígenas reiteran que la paz no ha llegado a los territorios
Date: 2020-07-23 21:41
Author: AdminContagio
Category: Actualidad, DDHH
Tags: Atentados contra comunidades indígenas, ORIVAC
Slug: tras-atentando-en-jamundi-comunidades-indigenas-reiteran-que-la-paz-no-ha-llegado-a-los-territorios
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/07/Edijk7GXkAEbRwc.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"fontSize":"small"} -->

Foto: Atetando en Jamundí / Feliciano Valencia

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Comunidades indígenas han manifestado su preocupación y rechazo contra el atentado ocurrido en horas de la mañana del pasado 22 de julio en contra del vehículo de la Unidad Nacional de Protección que usaba como transporte el gobernador del resguardo Kwe’sx Kiwe Nasa de Jamundí Cristian Camilo Toconás, reconocido líder quien tanto él como sus escoltas resultó ileso, pues ninguno se encontraba al interior del vehículo al momento del ataque.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Los hechos ocurrieron mientras el gobernador del resguardo indígena se encontraban en la sede Kwe'sx Kiwe Nasa, cuando desconocidos arrojaron una granada de fragmentación al vehículo, a ello se suma una amenaza de bomba contra la sede de la [Organización Regional Indígena del Valle del Cauca (ORIVAC)](https://twitter.com/Orivac_Colombia) ubicada en el barrio Alameda, la organización indígena además denunció que diferentes líderes indígenas habían sido amenazados en las ultimas horas.[(Lea también: Cinco personas asesinadas y varios heridos deja atentado contra comunidad indígena de Tacueyó)](https://archivo.contagioradio.com/dos-personas-asesinadas-y-varios-heridos-deja-atentado-contra-comunidad-indigena-de-tacueyo/)

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Amenazas contra lideres y organizaciones indígenas han sido reiteradas

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Según **Alex Lúlico, consejero de DD.HH, Paz y Guardia Indígena de la ORIVAC** relata que son varias las amenazas en contra de autoridades y líderes indígenas en el departamento atribuidas a disidencias de las FARC, las Águilas Negras y estructuras que se hacen pasar por el EPL o Pelusos, sin embargo no se ha podido confirmar su autenticidad.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Advierte que el 14 de agosto de 2019 también se presentó un episodio similar cuando fue dejada una granada de fragmentación dentro de la sede del resguardo en Jamundí, sin embargo este no es el único municipio que ha sido víctima del temor que generan los artefactos explosivos, otros municipios como Florida, Dagua, Pradera y en Bolívar e incluso en la sede regional ubicada en la ciudad de Cali.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

"Hasta el momento no hemos tenido respuestas, queremos hacer un llamado a las instituciones y al Gobierno que sean contundentes contra la situación que se está presentando en las cabeceras municipales", afirma el defensor de DD.HH. quien indica que las amenazas son materia de investigación [(Le puede interesar: Autoridades indígenas responsabilizan a Duque de masacre en Tacueyó)](https://archivo.contagioradio.com/autoridades-indigenas-responsabilizan-a-duque-de-masacre-en-tacueyo/)

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### El control territorial no le gusta a los actores armados presentes en la región

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

"Siempre ha existido una situación de disputa entre grupos armados por el narcotráfico, hay corredores que durante años les ha servido a grupos criminales y creemos que esa es la razón de lo que viene ocurriendo, las comunidades hacen el ejercicio de control territorial y eso hace que les empiecen a causar zozobra para que se de el paso a estos grupos", explica **Alex Lúlico**.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

El más reciente informe del Instituto de Estudios para el Desarrollo y la Paz reveló que el número más alto de homicidios en contra de liderazgos sociales se encuentra en el sector campesino, con 342 casos; seguido por los indígenas con 250 homicidios ocurridos contra los pueblos ancestrales desde la firma del Acuerdo de Paz. Tan solo en el 2020 han sido asesinados 47 líderes indígenas mientras que en 2019 se registraron 84 homicidios. [(Lea también: 971 defensores de DD.HH. y líderes han sido asesinados desde la firma del Acuerdo de paz)](https://archivo.contagioradio.com/971-defensores-de-dd-hh-y-lideres-han-sido-asesinados-desde-la-firma-del-acuerdo-de-paz/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Al respecto el consejero señala que incluso después de la firma del Acuerdo la realidad en los territorios del Valle del Cauca no ha cambiado, "siempre ha existido la persecución, el señalamiento y los homicidios contra defensores campesinos, indígenas y defensores de DD.HH, no ha llegado la paz".

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->
