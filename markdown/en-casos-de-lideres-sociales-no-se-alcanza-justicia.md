Title: En más del 90% de casos de asesinatos de líderes sociales aún no se alcanza justicia
Date: 2018-08-01 16:41
Category: DDHH, Nacional
Tags: Fiscalía, impunidad, lideres sociales, Somos defensores
Slug: en-casos-de-lideres-sociales-no-se-alcanza-justicia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/07/sin-olvido-curvarado-6.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [1 Jul 2018]

De acuerdo con cifras del programa Somos Defensores, entre 2009 y 2017 fueron asesinados 564 líderes sociales. Según los datos obtenidos desde la Fiscalía por esa organización, hay un alcance efectivo de justicia en 49 de los casos (ejecución de pena o sentencia de absolución), lo que significa que en el 91,5% de esos procesos “aún no se alcanza justicia”.

Según el programa se ha avanzado en la investigación de 155 casos, lo que significa que existe impunidad en 409 de ellos, que equivale al 72,5%, y aunque 362 procesos todavía se encuentran en etapa de Indagación, es poco probable que concluyan en razón al tiempo desde que ocurrió el crimen, situación que reduce las posibilidades de encontrar elementos de prueba que permitan resolver los homicidios.

Somos Defensores concluye que hay alcance efectivo de justicia en 49 casos, es decir, procedimientos en los que se produjo sentencia. Sin embargo, 45 asesinatos de líderes sociales no fueron investigados siquiera, y por lo tanto no aparecen en el sistema de la Fiscalía, razón por la que no se sabrá nunca quién lo asesinó y los motivos para hacerlo. (Le puede interesar: ["Dos líderes sociales asesinados durante el 20 de julio"](https://archivo.contagioradio.com/lideres-asesinados-el-20-de-julio/))

Una de las recomendaciones hechas por la Corte Interamericana de Derechos Humanos (Corte IDH) para proteger a los líderes sociales, es que se creara una política pública integral que, además de proteger a defensores de Derechos Humanos; investigue los autores de amenazas en su contra; y cree políticas que permitan su labor, investigue la proveniencia de los ataques y sancione “a los autores materiales e intelectuales de dichos ataques, combatiendo en consecuencia la impunidad”.

Sin embargo, como lo demuestran las cifras de la Fiscalía presentadas por Somos Defensores, estas recomendaciones se están pasando por alto, evitando combatir un flagelo que tiene a Colombia en una situación difícil en materia de Derechos Humanos. Mientras tanto, organizaciones sociales, partidos políticos y líderes piden que haya celeridad en las investigaciones y se capturen tanto autores materiales como intelectuales de los asesinatos y amenazas. (Le puede interesar: ["Los asesinatos de líderes sociales son sistemáticos: Ángela María Robledo"](https://archivo.contagioradio.com/asesinatos-de-lideres-sociales-son-sistematicos-angela-robledo/) )

Vale la pena agregar que estas cifras no incluyen ninguno de los asesinatos del 2018, que podrían ser más de 70 casos hasta Julio.

\[caption id="attachment\_55218" align="aligncenter" width="477"\]![Casos de Líderes Sociales investigados por la Fiscalía](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/07/Captura-de-pantalla-2018-07-30-a-las-5.54.39-p.m.-477x183.png){.wp-image-55218 .size-medium width="477" height="183"} Estas cifras no incluyen ninguno de los asesinatos del 2018, que podrían ser más de 70 casos hasta Julio.\[/caption\]

**Fiscalía dice que ha aclarado el 50% de casos que involucran a líderes asesinados**

Según un comunicado de la Fiscalía emitido el 31 de julio, la puesta en marcha de un nuevo modelo de investigación en el marco del plan estratégico 2016-2020, ha significado "un porcentaje histórico en el esclarecimiento de homicidios contra defensores verificados por las Naciones Unidas que alcanza el 49,18% a julio de 2018"; es decir, casos en los que hay causas definidas y, autores materiales e intelectuales capturados y judicializados.

Sin embargo, el comunicado no aclara el periodo de estudio sobre el que se realiza la estadística, ni la cifra que toman de las Naciones Unidas, que para el 2017 fue de 171 líderes asesinados. El documento tampoco, aclara la razón de utilizar las cifras verificadas por Naciones Unidas y no por el ente territorial encargado de realizar esta confirmación que es la Defensoría del Pueblo, cuya cifra es de 311 líderes asesinados entre enero de 2016 y junio de 2018.

[Comunicado de Prensa de la Fiscalía](https://www.scribd.com/document/385142759/Comunicado-de-Prensa-de-la-Fiscalia#from_embed "View Comunicado de Prensa de la Fiscalía on Scribd") by [Contagioradio](https://es.scribd.com/user/276652802/Contagioradio#from_embed "View Contagioradio's profile on Scribd") on Scribd

<iframe id="doc_80595" class="scribd_iframe_embed" title="Comunicado de Prensa de la Fiscalía" src="https://www.scribd.com/embeds/385142759/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-sW3xqDwiRGOjF7SLBLaX&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="0.7729220222793488"></iframe>

###### [Reciba toda la información de Contagio Radio en] [su correo](http://bit.ly/1nvAO4u) [o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por] [Contagio Radio](http://bit.ly/1ICYhVU) 

<div class="osd-sms-wrapper">

</div>
