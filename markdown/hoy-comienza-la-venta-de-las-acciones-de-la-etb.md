Title: Comenzó la venta de la ETB "La gallina de los huevos de oro"
Date: 2017-05-09 13:33
Category: Movilización, Nacional
Tags: Enrique Peñalosa, ETB, Movilización
Slug: hoy-comienza-la-venta-de-las-acciones-de-la-etb
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/05/etb-1-contagioradio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: CNN en español] 

###### [09 May 2017] 

Como absurdo han calificado los integrantes de los Sindicatos de Trabajadores de la ETB que arranque la venta de las acciones de la empresa. No se puede vender una empresa que invirtió 2.3 billones de pesos, esperando conseguir 2.1 billones de pesos. La venta se realizará en dos etapas y cada acción tendrá un valor de 617 pesos.

Leonardo Arguello, directivo de Sintrateléfonos, aseguró que aunque la venta de las acciones ya comenzó, eso no significa que se vaya a terminar el proceso e hizo una llamado a los ciudadanos y ciudadanas a defender la empresa que es un patrimonio público, "no es tan fácil que el gobierno privatice y venda el patrimonio público de los colombianos"

### **Las cifras detrás de la venta de la ETB** 

El precio de venta esperado por la administración de Enrique Peñalosa es cercano a los 2.13 billones de pesos.

El monto de la inversión en fibra óptica, televisión por subscripción y telefonía móvil entre 2013 y 2015 fue de \$2,1 billones.

Se estima que el poder de control de precios y las utilidades comiencen a verse en el segundo semestre de 2017 según los sindicatos.

Sintrateléfonos afirma que los últimos estados financieros reportan utilidades superiores a los 370 mil millones de  pesos, crecimiento exponencial del patrimonio de 25.64% y un billón de pesos de ganancias para el distrito. Patrimonio que no es tenido en cuenta a la hora de poner en venta uno de los activos más importantes del Distrito Capital.

La ETB le ha **representado a la Universidad Distrital ingresos de entre 7 mil y 10 mil millones de pesos**, así como la posibilidad de contar con el servicio de internet gratuito en las bibliotecas y colegios públicos, siendo la única empresa que oferta internet de 150 megas.

El plan estratégico que definió el plan de inversiones diseñado a corto, mediano y largo plazo, se espera que generen en 2022 utilidades de entre 2.5 y 3 billones de pesos. ([Lea también: La Empresa de Telecomunicaciones de Bogotá aporta cerca de un billón de pesos anuales](https://archivo.contagioradio.com/etb-le-aporta-al-distrito-cerca-de-un-billon-de-pesos/))

Aunque las utilidades se han mantenido constantes desde 2009 con una capitalización de 2 billones de pesos, la empresa puede convertirse en la más competitiva y rentable en el espectro de las telecomunicaciones en Colombia.

El recurso por la venta de la ETB sería usada en 30 colegios y cerca de 13 jardines infantiles, inversión necesaria estaría en riesgo puesto que la desfinanciación de la eduación alcanza un déficit de 600 mil millones de pesos para el segundo semestre de 2017 según indicó FECODE y el gobierno nacional reconoció.

En cuentas alegres un comprador privado podría hacerse a la empresa con una inversión cercana a los 1.5 billones de pesos, capital suficiente para obtener más del 50% de las acciones, capitalizarla con 2 billones y recuperar la inversión en 5 años.

La ETB tiene una historia de 133 años, muchos economistas señalan que solo la corrupción y las malas administraciones pueden explicar que la empresa no se haya valorizado más que la competencia privada que completaría apenas 2 décadas en el país.

### **La defensa de la ETB continúa a pesar del incio de la venta** 

Una de las primeras acciones a las que convocan los sindicatos es este martes **desde las 5 de la tarde en una movilización** que pretende oponerse al modelo de privatización y venta de la ETB por parte del gobierno de Enrique Peñalosa, impulsar la revocatoria e ir sumando fuerzas en contra de la culminación del proceso de vente de la empresa. Le puede interesar:["Administración Peñalosa hace oídos sordos para detener la venta de la ETB"](https://archivo.contagioradio.com/administracion-penalosa-hace-oidos-sordos-para-detener-la-venta-de-la-etb/)

Si bien la venta de las acciones es inminente, Arguello afirma que "esto no quiere decir que la empresa se haya vendido en su totalidad" debido a que se están ofreciendo en venta únicamente el 86% de las acciones y el proceso se realiza en dos etapas que deben ser consecutivas, una primera para trabajadores, sindicatos y cooperativas; y otra para los inversionistas mayoritarios. Le puede interesar:["No claudicará la defensa de la ETB: Sindicatos"](https://archivo.contagioradio.com/alcaldia-de-penalosa-no-escucha-a-los-ciudadanos-atelca/)

<iframe id="audio_18587338" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_18587338_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
