Title: Derechos sexuales y reproductivos de las mujeres en las Cárceles de Colombia
Date: 2018-08-13 12:17
Category: Expreso Libertad
Tags: derechos sexuales, Prisioneras políticas
Slug: derechos-reproductivos-mujeres-carceles
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/08/MUJEWR-CARCEL.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Pro-presas Madrid 

###### 31 Jul 2018 

"No se permite planificar, nadie habla de enfermedades de transmisión sexual, no hay acompañamiento en periodos de gestación, los derechos reproductivos en las cárceles de Colombia, no existen" así lo asegura Valentina Beltrán, ex prisionera política que estuvo recluida en la Cárcel El Buen Pastor.

En este programa del **Expreso Libertad** conozca las situaciones de vulneración de derechos sexuales y reproductivos a las que son expuestas las mujeres en las cárceles del país.

<iframe id="audio_27779640" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_27779640_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Encuentre más información sobre Prisioneros políticos en[ Expreso libertad ](https://archivo.contagioradio.com/programas/expreso-libertad/)y escúchelo los martes a partir de las 11 a.m. en [Contagio Radio](https://archivo.contagioradio.com/alaire.html) 
