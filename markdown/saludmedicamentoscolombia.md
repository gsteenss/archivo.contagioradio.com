Title: Victoria reprobable del comercio contra la salud
Date: 2017-05-08 11:51
Category: Mision Salud, Opinion
Tags: Imatinib en Colombia, Medicamentos, Salud
Slug: saludmedicamentoscolombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/05/iStock_000008303859Small.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto:Aarp 

###### 08 May 2017 

##### [[Germán Holguín- Misión Salud](https://archivo.contagioradio.com/opinion/mision-salud/)] 

Tras un engorroso proceso de 25 meses, caracterizado por las presiones de las multinacionales farmacéuticas, su gremio Afidro, el gobierno suizo y otros actores nacionales e internacionales, el año pasado el Ministerio de Salud resolvió favorablemente la solicitud de Misión Salud, la Fundación Ifarma y el CIMUN de la Universidad Nacional de emitir una Declaratoria de Interés Público (DIP) en favor del imatinib, medicamento contra el cáncer que mantiene con vida a miles de personas, y estableció un sistema de control de precios para medicamentos con DIP, que abrió la puerta a la reducción del precio de medicamentos con precios exorbitantes. Acto seguido el gobierno redujo el precio del Imatinib 44%, lo que significa bajar el costo del tratamiento en 24 millones de pesos y un ahorro para el sistema de salud superior a 14.000 millones de pesos anuales. En ambos casos actuó basado en normas de la OMC y en recomendaciones de la OMS.

Estas decisiones alarmaron a las multinacionales farmacéuticas porque podían afectar las altísimas utilidades que obtienen en el país gracias a los altos precios de sus medicinas, por lo que impusieron a Afidro la tarea de presionar al gobierno hasta lograr su revocatoria, utilizando todos los recursos disponibles, entre ellos el anuncio de demandas internacionales y la amenaza de bloquear el ingreso de Colombia a la OCDE, organismo conocido como “el club de los países ricos”.

Como resultado de tales presiones, el 25 de abril pasado la Ministra de Comercio, contrariando la voluntad del Ministro de Salud e ignorando las objeciones de la sociedad civil, expidió el Decreto 670, que: I) Elimina las DIP con fines de control de precios, lo que permitirá a las multinacionales seguir cobrando precios escandalosos, con el consiguiente impacto sobre el acceso a los medicamentos por parte de la población, especialmente de las personas en condiciones de vulnerabilidad, y sobre la sostenibilidad del sistema de salud, y II) Hace casi imposible la emisión de licencias obligatorias, esto es, el ejercicio del derecho de autorizar a terceros para producir y comercializar versiones genéricas de un medicamento patentado, derecho que la normativa internacional ordena ejercer plenamente siempre que sea necesario para asegurar a todos el acceso a medicinas necesarias para la salud y la vida.

Esto último lo logra el Decreto incorporando al Comité Técnico encargado de conceptuar sobre la pertinencia de una DIP con fines de licencia obligatoria -hoy conformado íntegramente por expertos del Ministerio de Salud- al Ministerio de Comercio y a Planeación Nacional, ambos organismos con vocación a proteger la propiedad intelectual y el comercio por encima del derecho fundamental a la salud, lo que es un grave error dada la categoría superior de este último.

Como si lo anterior fuera poco, el Decreto fue gestado en reuniones cerradas en las que participaron el Secretario General de la Presidencia de la República, la representante de Colombia ante la OCDE y Afidro, a espaldas del Ministro de Salud, quien no participó en su redacción y se opuso a su expedición, y sin la suficiente concurrencia de la academia y de la sociedad civil, que se enteraron del proceso tardíamente a través de terceros, lo que denota una total falta de transparencia.

En consecuencia, en el futuro no habrá nuevas DIP de medicamentos y seguramente todos tendremos que aumentar los aportes al sistema de salud para que pueda seguir comprando medicamentos con precios exorbitantes. Todo para el enriquecimiento de unas pocas multinacionales, a costa del bolsillo, la salud y la vida de los colombianos.

Una victoria de las grandes multinacionales farmacéuticas y su protectora la Ministra Lacouture contra el derecho fundamental a la Salud y su defensor el Ministro Gaviria. Una victoria del Comercio contra la Salud genuinamente reprobable por haber sido lograda a un costo demasiado alto para el bienestar de los colombianos.

Falta saber qué piensa y que hará el Presidente Santos, quien hasta ahora ha permanecido en silencio. Aplaudimos su entusiasmo en favor de la paz pero demandamos coherencia con su obligación de respetar, proteger y cumplir el derecho a la salud. De lo contrario en el futuro los colombianos no morirán en el campo de batalla sino esperando las vacunas y los medicamentos en los hospitales y en sus casas. Sin salud no hay paz.

El derecho a la salud y la vida, el primero y más preciado de los derechos humanos, está por encima de los derechos de propiedad intelectual, el lucro y los intereses comerciales, por importantes que éstos sean. Le puede interesar: [El precio de entrar a la OECD](https://archivo.contagioradio.com/el-precio-de-entrar-a-la-oecd/)
