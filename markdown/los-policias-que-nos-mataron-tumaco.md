Title: Entre los policías que atacaron la Misión Humanitaria "estaban los que nos mataron"
Date: 2017-10-10 08:40
Category: DDHH, Nacional
Slug: los-policias-que-nos-mataron-tumaco
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/10/tumaco-vicepresidente.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: contagio radio] 

###### [09 Oct 2017] 

Durante el encuentro que sostuvieron en Tumaco los integrantes de la Misión Humanitaria que fue atacada por la policía el pasado domingo en el Tandil con el vicepresidente y el comandante de la policía antinarcóticos, una de las líderes de la comunidad expresó que dentro del grupo de policías que atacó a la misión se encontraban varios de los que dispararon contra los campesinos el pasado 5 de Octubre.

### **Policía habría rematado a campesino herido** 

Además en el encuentro se compartieron los testimonios en torno a que efectivos policiales dispararon contra un campesino que estaba herido y que les pidió ayuda. En ese sentido varios integrantes de la misión aseguraron que escucharon los testimonios de campesinos que testificaron este escandaloso hecho.

De acuerdo con las declaraciones de vicepresidente, "Está claro que hubo un comportamiento irregular de la policía nacional. Se ha tenido una reunión (con las Organizaciones víctimas del hecho), y tienen observaciones y reproches sobre el comunicado que sacó la policía". [Lea también Así se desarrollo la Masacre de campesinos en El Tandil](https://archivo.contagioradio.com/9-personas-erradicacion-forzada-tumaco/)

Según se conoció en esa reunión el vicepresidente pudo escuchar los testimonios de quienes fueron atacados, pero además se habló de las causas estructurales por las cuales el pasado jueves varios campesinos e indígenas resultaron muertos y heridos, según se ha podido establecer, a manos de la policía.

### **Se esperan mas sanciones disciplinarias contra la policía** 

"Entendemos que iban en una misión humanitaria que ha debido recibir otro trato”, dijo  Naranjo, al escuchar los testimonios. Informado que ya fueron sancionados cuatro policías que participaron en el ataque, y otros 30 agentes han sido trasladados mientras se avanza en la investigación de los hechos del 5 y 8 de octubre. [Lea también Policía ataca a Misión Humanitaria en el Tandil](https://archivo.contagioradio.com/policia-ataca-mision-humanitaria-47729/)

"Avanza en principio una suspensión disciplinaria contra varios uniformados, que tiene como propósito dar garantías de que la investigación no será afectada. Y por el otro lado, avanza la investigación penal, que corre a cargo de la Fiscalía. También hemos recibido el concurso de la Procuraduría a través de la delegada para la policía nacional. Lo que podemos decir es que el Gobierno ha dado instrucciones a la Policía Nacional para que esta investigación este rodeada de garantías y para que en ningún caso haya una sombra de duda y permitamos que la Fiscalía determine la responsabilidades individuales si hubiere el caso", indicó el vicepresidente.

### **Comunidad continúa atemorizada** 

En ese sentido, se habló de la situación frente al incumplimiento del punto 4 del acuerdo de paz sobre sustitución de cultivos de uso ilícito, y sobre la reincorporación de los excombatientes de las FARC, así como también del permanente abandono estatal, que tiene a los campesinos e indígenas en condiciones indignas en salud y educación, como lo ha manifestado ASOMINUMA.

Por otra parte, este lunes en la mañana se conoce que la comunidad asegura que se ha encontrado el cuerpo de un menor de edad, que habría sido víctima de los hechos del pasado jueves. Sin embargo, no se ha confirmado la situación por parte de Fiscalía ni la Medicina Legal, aunque se cree que este martes el CTI, irá al Tandil, donde ocurrió la masacre.

Mientras tanto, las comunidades se encuentran atemorizadas, ante la permanencia de la policía antinarcóticos, y piden al gobierno que sea desmontada esa unidad, que de acuerdo con los  campesinos, está revictimizandolos pues estos serían quienes atacaron y asesinaron a los miembros de las comunidades.

"Señor policía usted dice que sacó de ahí a 32 de los hombres que estaban el día de la masacre, ¿por qué dejó ahí a los que nos mataron? Ayer estaban ahí los que nos mataron", expresó en medio de llanto una líder campesina cocalera en la reunión con el vicepresidente Naranjo.

###### Reciba toda la información de Contagio Radio en [[su correo]
