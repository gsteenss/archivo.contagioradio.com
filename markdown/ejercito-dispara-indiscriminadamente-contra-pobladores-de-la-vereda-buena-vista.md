Title: Ejército dispara indiscriminadamente contra pobladores de la vereda Buenavista, Ituango
Date: 2015-11-17 11:50
Category: Movilización, Nacional
Tags: Agresiones por parte del ejército, Hidroituango, Isabel Cristina Zuleta, Movimiento Ríos Vivos, Vereda Buenavista
Slug: ejercito-dispara-indiscriminadamente-contra-pobladores-de-la-vereda-buena-vista
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/11/unnamed.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto:boletindeprensacompromiso.blogspot.com 

<iframe src="http://www.ivoox.com/player_ek_9417059_2_1.html?data=mpmemZWZfY6ZmKiakpqJd6KlmpKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRiMbi1tPQy8bSb8Xd1NXO1NTXb8XZzZDSzIqnd4a1mtfQy9nTb8Tjz9nfw5DUs8PgwsjWh6iXaaOnz5DS0JDQcYarpJKw0dPYpcjd0JC%2Fw8nNs4yhhpywj5k%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Isabel Cristina Zuleta, Movimiento Ríos Vivos] 

###### [17 Nov 2015] 

El movimiento Ríos Vivos, denunció este martes que en la vía que conduce a Ituango, en intermediaciones a la vereda de Buenavista, **el batallón Bajes de la cuarta brigada del ejército en el departamento de Antioquía,  disparó indiscriminadamente contra aproximadamente 30 personas** que exigían el respeto al debido proceso por la captura de algunos de los miembros de la comunidad en horas de la mañana.

Isabel Crisitna Zuleta, integrante del Movimiento, asegura que **el día jueves se presentaron combates entre las FARC y el Ejército, quedando la población civil en medio del fuego cruzado.** El ejército siguió a la guerrilla hacia las montañas, lo cual provocó enfrentamientos durante una hora.

Zuleta indica que “**en la vereda hay una situación muy crítica**”, el día de hoy las comunidades iban a expresar sus **quejas y reclamos ante la Autoridad Nacional de Licencias Ambientales, contra Hidroituango y el Ejército impidió que las comunidades asistieran,** justificando su acción en los enfrentamientos contra la guerrilla.

La comunidad de Buenavista está organizada y pertenece al Movimiento Ríos Vivos, son 200 las personas afectadas, quienes protestan además contra otras empresas que contaminan las fuentes hídricas de esta zona. Isabel Cristina Zuleta también denuncia que en días anteriores se presentó el encerramiento en una de las tanquetas por parte del ejército a un niño, en una clara violación a los derechos humanos.

**Exigencias al Gobierno Nacional:**

-   Se respete debido proceso a todos las comunidades.
-   Cese a los hostigamientos contra las comunidades
-   Que se explique por qué se dispara contra las comunidades
-   Que se aclare porqué hay enfrentamientos si se está en un proceso de diálogo en la Habana.

 
