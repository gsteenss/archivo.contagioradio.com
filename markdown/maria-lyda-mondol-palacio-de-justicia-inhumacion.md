Title: 30 años después entregan restos de víctima del Palacio de Justicia
Date: 2016-01-15 12:08
Category: Sin Olvido
Tags: entrega de restos víctima palacio de justicia, Palacio de Justicia
Slug: maria-lyda-mondol-palacio-de-justicia-inhumacion
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/01/María-Lyda-Mondol-de-Palacios.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

<iframe src="http://www.ivoox.com/player_ek_10087547_2_1.html?data=kpWdmpyZeJihhpywj5aVaZS1lp6ah5yncZOhhpywj5WRaZi3jpWah5yncZSkjMaSpZiJhpLj1JDRx9jUuYa3lIqum9iPqc%2Fo08rUw9OPtsbn1dTgjcnJb9eZpJiSo6nHuMrhwpDRx9GPlMLgwsiah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Liliana Palacios] 

###### [15 Ene 2016 ] 

[Familiares de María Lyda Mondol de Palacios, quien era auxiliar del magistrado Mauricio Gaona Cruz cuando fue asesinada durante la retoma del Palacio de Justicia, **inhumaron los restos óseos identificados por unidades de la Fiscalía en la tumba de la magistrada Blanca Inés Ramírez**, en Octubre de 2015 en el Cementerio Jardines de Paz.]

[Como una agonía, define Liliana Palacios, una de las 4 hijas de María Lyda, estos 30 años en los que mantuvieron la idea de que su madre estuviera con vida, “**siempre conservamos la esperanza de que ella estaba viva en algún lugar** y de que ella iba a aparecer viva en cualquier momento, nunca descartamos la posibilidad de que ella fuera a aparecer viva”.]

[Entre las exigencias que la familia Mondol Palacios hace al Estado Colombiano es que se **esclarezca la verdad y se haga justicia** para que los responsables de los asesinatos de quienes salieron con vida del Palacio de Justicia y luego fueron reportados como muertos sean sancionados, pues “**todo lo que nosotras padecimos económica y psicológicamente no tiene precio**”, como asegura Liliana Palacios.]

[El acto contó con una ceremonia religiosa en la que también participaron familiares de algunas de las víctimas de los hechos ocurridos durante la retoma del Palacio, liderada por las Fuerzas Militares y a quienes Liliana Palacios envió un **mensaje de esperanza y gratitud en el que les invitó a continuar apelando a la justicia**.]
