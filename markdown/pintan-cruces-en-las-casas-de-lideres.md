Title: Con cruces negras en las casas amenazan a líderes en Puerto López
Date: 2016-11-23 15:19
Category: DDHH, Nacional
Tags: Amenazas, Graftti, Lideres, Pintar casa
Slug: pintan-cruces-en-las-casas-de-lideres
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/Campesinos-San-Jose-2.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Instituto Popular de Capacitación] 

###### [23 Nov. 2016] 

El pasado 22 de noviembre, en el corregimiento de Puerto López en el municipio del Bagre, Antioquia, **las casas de diversos líderes comunitarios, comerciantes y personas que han estado recibiendo amenazas de paramilitares, amanecieron pintadas con grandes cruces negras**, en lo que según la comunidad ha considerado una amenaza expresa del paramilitarismo en su contra.

**Dada la gravedad de los hechos y los antecedentes, en los cuales han sido amenazados integrantes de la Asociación de Hermandades Agroecológicas y Mineras AHERAMIGUA de Guamocó AHERAMIGUA**, dicha organización ha exigido al Gobierno nacional garantizar la seguridad de estos líderes y entre tanto pueda dar con los responsables de estas amenazas y actos intimidatorios a sus hogares.

De igual manera, han solicitado a los organismos internacionales intervenir y hacer presencia en la zona, para de tal manera evitar cualquier tipo de tragedia en el lugar.

Por su parte, han manifestado que **el Estado colombiano es el “único responsable de la garantía y protección de los derechos humanos**” y a han dicho que la Alcaldía de El Bagre como ente territorial, debe velar por la protección de la comunidad. Le puede interesar: [Organizaciones del campamento humanitario en el Bagre se reunirán con el gobierno](https://archivo.contagioradio.com/organizaciones-del-campamento-humanitario-en-el-bagre-se-reuniran-con-el-gobierno/)

Finalmente han asegurado que cualquier situación que pueda derivarse en contra de la integridad y la vida de los líderes comunitarios e integrantes de AHERAMIGUA, luego de dada a conocer está denuncia, recaerá en las autoridades nacionales, departamentales y locales. Le puede interesar: [Según comunidad, ejército sí habría violado cese bilateral al fuego](https://archivo.contagioradio.com/segun-comunidad-ejercito-si-habria-violado-cese-bilateral-al-fuego/)

###### Reciba toda la información de Contagio Radio en [[su correo]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio]{.s1}](http://bit.ly/1ICYhVU)

<div class="ssba ssba-wrap">

</div>
