Title: ¿Por qué Mocoa sufre por desabastecimiento de agua constantemente?
Date: 2019-02-27 16:30
Category: Comunidad, DDHH
Tags: Acueducto, Agua, Mocoa, Putumayo
Slug: mocoa-desabastecimiento-agua
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/02/Mocoa-sin-agua.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: arnoldo1956] 

###### [27 Feb 2019] 

La capital de Putumayo completa once días con el **80% de la población sin el abastecimiento de agua** y aunque según las fuentes oficiales, la causa de esta situación son tres derrumbes que se presentan en la cuenca alta del **Río Mulato,** lo que imposibilita el funcionamiento de la bocatoma para el acueducto; para los mocoanos, las responsables son las administraciones municipales y locales, así como en el Gobierno Nacional, que desde 2014 saben que este tipo de emergencias ocurren y no hacen nada para prevenirlas.

**Manuel Ramos Muñoz, integrante de Dignidad por Mocoa y de la Mesa Única de Participación para la reconstrucción de Mocoa**, afirma qu**e no se debe culpar al territorio de Putumayo por la falta de abastecimiento del vital líquido**, porque en 2014 y 2015 ocurrieron eventos parecidos; adicionalmente, en 2017 ocurrió la avalancha que pudo evidenciar aún más la problemática.

Para Ramos, estas emergencias debieron advertir a la empresa pública que presta el servicio de agua en Mocoa para que creara un Plan de Contingencia en este tipo de situaciones, al tiempo que debió impulsarla a buscar alternativas para la captación de agua distintas al Río Mulato. (Le puede interesar: ["Sin soluciones concretas, vendrían nuevas calamidades para la Capital de Putumayo"](https://archivo.contagioradio.com/sin-soluciones-concretas-para-mocoa/))

### **¿Falla natural o desinterés administrativo?** 

Ramos explica que la cuenca del Río Mulato tiene problemas de estabilidad geológica porque tiene entre seis y ocho fallas cerca a su cauce, lo que se traduce en una alta sismicidad para la zona, y la alta probabilidad de que se presenten fenómenos como el taponamiento que se vive actualmente. (Le puede interesar: ["Gobierno Nacional se olvidó de los damnificados en M."](https://archivo.contagioradio.com/gobierno-nacional-se-olvido-de-los-damnificados-en-mocoa/))

El habitante de Mocoa recuerda que antes de la avalancha de 2017 había un Plan Maestro de Acueducto para ese municipio, dicho Plan tenía un costo estimado de 17 mil millones de pesos y debía encontrar una solución definitiva para garantizar el abastecimiento de agua; sin embargo, después del desastre, el Gobierno Nacional apropió recursos por 28 mil millones de pesos para reconstruir el actual acueducto.

En ese momento, los habitantes advirtieron el error  pero no fueron escuchados, y de acuerdo a Ramos, hasta el momento se han invertido **24 mil millones en la reconstrucción del acueducto, lo que ha significado avances en las obras en un 84%;** entre las que se encuentran la reparación de la bocatoma del Río Mulato, y la adecuación de redes internas de agua y alcantarillado, mientras **"el problema es la fuente captadora"**.

### **La actual situación que se vive en Mocoa** 

Ramos dice que en este momento **solo el 20% de la población de Mocoa (estimada en cerca de 50 mil personas) cuenta con servicio de agua** porque obtienen el líquido de otras fuentes; un 30% adicional está recibiendo el servicio mediante carrotanques, pero estos están enfocados en colegios, el comercio y algunas zonas determinadas como importantes. (Le puede interesar: ["Corrupción también tuvo su parte de responsabilidad en avalancha de Putumayo"](https://archivo.contagioradio.com/corrupciontambienfueculpabledeavalnchademocoa/))

Por su parte, el resto de la comunidad de la Capital de Putumayo debe recurrir a 2 quebradas cercanas al Municipio de las cuales obtienen el líquido necesario para satisfacer sus necesidades básicas. Mientras las autoridades esperan que la lluvia limpie el Río Mulato y se destapone su cauce, **los mocoanos esperan que se solucione el problema de fondo, porque de otra forma, esta situación se seguiría presentando**.

<iframe id="audio_32898862" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_32898862_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
