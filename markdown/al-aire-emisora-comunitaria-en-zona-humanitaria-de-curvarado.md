Title: "Al aire" emisora comunitaria en Zona Humanitaria de Curvaradó
Date: 2016-12-10 08:00
Category: Comunidad, Nacional
Tags: Comunicadores CONPAZ, Conpaz, Radio comunitaria
Slug: al-aire-emisora-comunitaria-en-zona-humanitaria-de-curvarado
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/12/radio-comunitaria-7.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Comunicadores CONPAZ 

###### 10 Dic 2016

En la **Zona Humanitaria, Camelias**, ubicada en la cuenca del río Curvaradó, Chocó, nació la radio emisora  “**Aflicoc Estéreo**”. Una voz de esperanza que pretende permitirle a esa comunidad y a quienes se han resistido al despojo en esa región del Bajo Atrato, alzar su voz, hacer sus propuestas y decirle al mundo que en ese pedazo de tierra están dispuestos a construir paz.

A pesar del férreo control paramilitar las Ondas de paz, resonaron el pasado 7 de diciembre en la casa de la memoria, que cobró más vida con la instalación de los equipos y las actividades de los y las jóvenes que participaron en el segundo t**aller de comunicación de CONPAZ,** orientado por el equipo de Contagio Radio.

\[embed\]https://youtu.be/IHKRVRmXh98\[/embed\]

Así la memoria sigue cobrando vida y se fortalece  en los procesos comunitarios que siguen negándose a la usurpación de los territorios y a la usurpación de la vida por parte de los paramilitares.

**14 jóvenes provenientes de Chocó, Putumayo, Cauca y Valle del Cauca, contribuyeron con cuñas radiales, voces, entrevistas, promos y música del primer programa** que salió al aire en este espacio. Los jóvenes narraron su experiencia y la de sus comunidades, enriquecieron a la comunidad en el encuentro de otras realidades atravesadas por la guerra en otros territorios pero con las mismas víctimas.

La tienda comunitaria, la líder **Maria Ligia Chaverra**, y la visibilización internacional al proceso de resistencia, así como los logros judiciales en la defensa del territorio, hicieron parte del espacio de lanzamiento. Un espacio que piensa permanecer y crecer en el marco de la implementación de los acuerdos de paz, en la que una exigencia concreta es que las comunidades puedan tener sus propios medios de comunicación y que sean financiados por el Estado. Esa es una de las apuestas del equipo de “Aflicoc Estéreo”.

Para **José Francisco**, uno de los encargados del espacio, esta emisora es una oportunidad para que las apuestas de la comunidad y sus propuestas para defender la tierra y la vida se escuchen. Además se podrá fortalecer el proceso comunitario porque tanto las discusiones internas como las propuestas van a tener un espacio de reflexión.

Así, con **“Aflicoc Estéreo”**, el proceso de comunicación sigue avanzando y se convierte en una posibilidad real de participación, de discusión y de construcción.

\
