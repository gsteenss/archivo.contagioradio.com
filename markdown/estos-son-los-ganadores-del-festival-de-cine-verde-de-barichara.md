Title: Estos son los ganadores del Festival de Cine Verde de Barichara
Date: 2015-09-21 15:58
Author: AdminContagio
Category: 24 Cuadros, Cultura
Tags: César Acevedo, Denis Delestrac, Festival Cine Verde Barichara, Gabo Kerlegand, Ganadores Festiver 2015, ‪Karney Hatch, Norida Rodríguez, Toto Vega
Slug: estos-son-los-ganadores-del-festival-de-cine-verde-de-barichara
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/festiver-e1442868654661.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [21 Sep 2015] 

Con la proyección de la cinta documental francesa "[Banking Nature"‬]{._58cm} de[‪l director  ]{._58cl}[Denis Delestrac‬, se realizó la clausura y premiación de la V edición del **Festival de cine Verde de Barichara**, evento que año tras año acoge cientos de visitantes en la colonial villa santandereana entorno al cine con enfoque medio ambiental "**Por un mundo más consciente y más consecuente**".]{._58cm}

**60 producciones**, entre documentales, cortos,  largometrajes y animaciones, hicieron parte de la competencia oficial del Festival, provenientes de 20 países como **Francia, Estados Unidos, México, Bélgica, Cuba, Rusia, Colombia** entre otros, en busca de ganar más de 70 mil US en efectivo, apoyo técnico para producción y postproducción y becas para diplomados.

La programación del evento, desarrollada entre el 16 y el 20 de septiembre, presentó en su apertura la cinta sobre Agricultura Urbana [[‎](https://www.facebook.com/hashtag/platthismovie?source=feed_text&story_id=1064202376925593){._58cn}"]{._58cl}[**Plat This Movie**"‬]{._58cm} del director norteamericano [‪]{._58cl}[**Karney Hatch**‬, mientras que]{._58cm} "**La Tierra y la Sombra**" de C**ésar Acevedo**, ganadora de la Cámara de Oro en la más reciente edición del Festival de Cine de Cannes, participó en la sección no competitiva de la muestra.

Exposiciones artísticas, talleres de producción y gestión, sonido y música, fotografía y dirección, conversartorios ambientales y clases magistrales con **Toño Folguera, Jacaranda Correa, Alejandro Solar y Gabo Kerlegand**, y las conferencias impartidas por expertos del medio provenientes de Colombia, España y México, demostraron que la concienciación y sensibilización en lo ambiental, tiene en el Festival el mejor de los espacios.

En su 5ta edición, Festiver continúa presentando su "**Mercado verde**" con productos orgánicos saludables, además de la inciativa ‘**La ruta verde**’, muestra itinerante que viaja a las veredas cercanas llevando cine a grandes y chicos de la región.

A continuación los ganadores por Categorías de Festiver 2015.

**Ganadores por Categorías 5to Festiver 2015.**

**Categoría guión inedito de cortometraje:** "Cuando Llovía" de Jennifer Ramírez

**Categoría Cogollo Verde, nuevos realizadores colombianos**: "El regreso del Oso" (2015), Ficción /Colombia/ Ángela Echeverry

Desde su futuro como fotógrafo de naturaleza, Camilo recuerda el día de su lejana infancia en que tuvo la primera noticia de que en su país había osos de verdad. A esta revelación que le hiciera su abuelo, se sumó la desazón de saber que estos animales habían desaparecido casi por completo de los Andes colombianos. Sin embargo, abuelo y nieto emprenden una fantástica aventura en busca del oso de anteojos

**Categoría animación**, "Gea" (2014),  Animación / España/ Jaime Maestro

Una guerra entre dos grandes potencias está arrasando un planeta. Ambos bandos mantienen a sus respectivos satélites de comunicación orbitando en los límites de la atmósfera. Sus dos tripulantes se cruzan cada día a la misma hora desde sus respectivas naves, y toda la rivalidad del planeta no es suficiente.

**Categoría cortometraje,** "Bienvenidos" (2014) , Ficción/ España/  Javier Fesser

A una escuela perdida en el corazón de los Andes peruanos llega una asombrosa novedad que revoluciona la vida de toda la comunidad: la conexión a internet.

**Categoría largometraje**, "Cuando respiro" (2015), Documental/ Chile/ Coti Donoso

Santiago de Chile, una gran ciudad que ha crecido a costa del aire que respiramos. Un dirigente social y un abogado intentan generar cambios, pero se enfrentan a una metrópoli que se expande sin medida frente al fantasma de la especulación inmobiliaria. Las autoridades hacen vista gorda al problema, y un poeta escribe ácidas críticas a la corrupción y la pobreza. Mientras, una ciudad sin aire, se mueve bajo una feroz nube gris.

**Categoría mejor película regional**, "El embudo" (2014), Ficción/Colombia/ Crisanto García y Jhon Chaparro

Un hombre vuelve a la montaña donde creció porque su padre ha muerto y comienza una especie de ajuste de cuentas con el pasado. Enterrar al padre también significa cerrar las heridas y para hacerlo es necesario transitar un arduo camino que le ayude a exorcizar todos los fantasmas que ha acumulado en su vida. Aquí, los personajes evocan el mundo de Juan Rulfo, seres que se debaten entre la aridez del mundo de los vivos y lo inexplicable del mundo de los muertos.
