Title: 25.000 personas serían beneficiadas con iniciativa de MinTrabajo
Date: 2016-11-18 17:59
Category: DDHH, Paz
Tags: justicia y reparación integral, Ministerio de Trabajo, Reparación a las víctimas
Slug: 25-000-personas-serian-beneficiadas-iniciativa-mintrabajo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/trabajo.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Fresota] 

###### [18 Nov 2016] 

El Ministerio de trabajo presentó en Bogotá el proyecto denominado **‘Ruta de empleabilidad para las víctimas’** el cuál plantea la estabilización socio-económica y reparación de víctimas, se trata de **25.000 personas que serían beneficiadas durante el 2017 y cuenta con un presupuesto de 34.320 millones** de pesos.

En Colombia según el registro único de víctimas son más de **8.230.860 las personas que se han visto directamente afectadas por la guerra**, y si se tiene en cuenta el número de personas que aún no están registradas, la cifra es alarmante. Le puede interesar: [Víctimas de desplazamiento exigen reparación integral.](https://archivo.contagioradio.com/victimas-de-desplazamiento-exigen-reparacion-integral/)

Si bien esta iniciativa representa un esfuerzo de la cartera estatal, **las 3.050 personas beneficiadas hasta el momento, son sólo una pequeña parte de la población** **colombiana a la que se debe restituir y garantizar** sus derechos a la vida, la tierra, a la vivienda, a la salud, a educación, al trabajo, a la reparación, a la verdad y a la no repetición de los hechos victimizantes.

La Ministra de Trabajo Clara López Obregón aseguró que “el conflicto armado colombiano afectó especialmente a campesinos, afrodescendientes, indígenas y sindicalistas, desestructuró las relaciones laborales y los confinó al desamparo**, a esas personas queremos llegar, buscamos que las víctimas recuperen sus derechos”.**

### **¿Quienes construyen el proyecto?** 

Matilde Caicedo una de las víctimas beneficiadas por la iniciativa, manifestó que para quienes han sufrido las consecuencias del conflicto es fundamental contar con el **acompañamiento por parte de la sociedad civil en general, pero sobre todo, el respaldo por parte del Estado** “para que poco a poco quienes hemos llegado a las ciudades por el desplazamiento **podamos perder el miedo y volvernos a sentir parte de la sociedad, sentir que volvemos a aportar”.**

Vladimir Rodríguez Coordinador de Reparación Colectiva de la Unidad de Víctimas también hizo referencia a la importancia de **reparar a los más de 7.000 sindicatos de base que contaban con unas 900 mil personas afiliadas,** quienes sufrieron directamente los fenómenos de desaparición, desplazamiento y asesinato selectivo, con mayor intensidad **durante el periodo presidencial del 2002 al 2010.**

Por último la Ministra Clara López señaló que el proyecto que integra a otras iniciativas que ya están en funcionamiento como Somos rurales, Sumando paz, Emprende pacífico Formación para el trabajo y Escalando sueños, siga creciendo con el apoyo de sectores como la **cooperación internacional de Suecia, España, Suiza, Corea, Francia, Reino Unido y de organizaciones internacionales** como USAID y el PNUD.

### **¿Cuáles son los pasos para que las víctimas accedan a la ruta de empleabilidad?** 

Quienes deseen acceder a las ofertas de formación y ofertas laborales en confección, áreas técnicas, apicultivos y cultivos de café deberán realizar los siguientes pasos:

1.  La persona debe hacer la declaración de los hechos victimizantes para entrar al Registro Único de Víctimas.
2.  Pasa al acceso de la oferta institucional en fase de ayuda humanitaria, en relación a temas de salud, educación, vivienda y seguridad alimentaria.
3.  MinTrabajo hace la conexión con las entidades debidamente certificadas.
4.  La entidad realiza la inscripción en el Servicio Público de Empleo, donde las hojas de vida de las víctimas tienen prioridad.

###### Reciba toda la información de Contagio Radio en [[su correo]
