Title: Durante sepelio de Alejandro Llinas amenazan a sus amigos y familiares
Date: 2020-04-30 20:48
Author: CtgAdm
Category: Actualidad, DDHH
Tags: #AljandroLlinas, #Líder, #SantaMarta, #SierraNevada
Slug: durante-sepelio-de-alejandro-llinas-amenazan-a-sus-amigos-y-familiares
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/04/llinas-1.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/04/alejandro_llinas.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/04/WhatsApp-Image-2020-04-30-at-8.07.14-PM.jpeg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/04/WhatsApp-Image-2020-04-30-at-8.07.14-PM-1.jpeg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

Familiares y amigos de líder social Alejandro Llinas, asesinado el pasado 24 de abril, denuncian que mientras se encontraban en la funeraria para iniciar el sepelio recibieron una llamada en donde los amenazaban de muerte, además les habrían asegurado que personas extrañas estarían en las honras fúnebres haciendo registro fotográfico de quienes participaran.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Según una de las personas víctima de los hechos, detrás de estos hostigamientos estarían los mismos intereses de quienes asesinaron al líder. Asimismo, considera que hay una intención de evitar que se conozca **"la verdad sobre todo el orden de terror que hay en la Sierra Nevada de Santa Marta"**. (Le puede interesar: "[Las prácticas turísticas que más dañan los ecosistemas según Pueblos Indígenas de la Sierra](https://archivo.contagioradio.com/las-practicas-turisticas-que-mas-danan-los-ecosistemas-segun-pueblos-indigenas-de-la-sierra/)")

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Fuentes de la gobernación del Magdalena aseguraron que sí hubo acompañamiento de la policía desde el momento en que se conocieron las amenazas y que han estado acompañando el caso, pues conocen la situación a la que se enfrentan líderes como Llinas y no son ajenos al control que sobre esos territorios pretenden ejercer los grupos armados ilegales.

<!-- /wp:paragraph -->

<!-- wp:heading -->

Las irregularidades en el caso de Alejandro Llinas
--------------------------------------------------

<!-- /wp:heading -->

<!-- wp:paragraph -->

De acuerdo con otros líderes, el hijo de Alejandro Llinas, residente en Estados Unidos, habría enviado una comunicación a la gobernación del Magdalena. En ella denunciaría el abandono del caso, debido a la falta de acciones desde Fiscalía y Medicina Legal.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Sin embargo fuentes de la gobernación aseguran que no han recibido una comunicación en ese sentido, pero a pesar de ello han estado activando todos los medios a su alcance para que este crimen no quede en la impunidad, además de buscar mecanismos de protección para las personas amenazadas por los grupos de tipo paramilitar que operan en la región ("[Los líderes sociales colombianos abogan por sus comunidades, pero luchan con amenazas de violencia y un gobierno apático](https://www.justiciaypazcolombia.com/los-lideres-sociales-colombianos-abogan-por-sus-comunidades-pero-luchan-con-amenazas-de-violencia-y-un-gobierno-apatico/)")

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En cuanto al grupo responsable del asesinato, en redes sociales se conoció un comunicado firmado por las **Autodefensas Conquistadores de la Sierra Nevada de Santa Marta.** De acuerdo con el texto ellos se atribuyen la muerte del líder social y aseveran que no está relacionada con el carácter político de Llinas, sino que señalan que se debería a actos de acoso sexual.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

No obstante, varios defensores de DDHH rechazaron el comunicado, aclarando que no se han conocido denuncias en ese sentido, al mismo tiempo que resaltaron las denuncias de Llinas sobre las dinámicas de violencia en ese territorio que han coptado Juntas de Acción Comunal, territorios de comunidades indígenas, parques naturales y que han provocado la muerte de ambientalistas como [Natalia Jiménez y Rodrigo Monsalve el pasado mes de Diciembre.](https://archivo.contagioradio.com/las-pistas-tras-el-asesinato-de-la-ecologista-nathalia-jimenez-y-su-esposo-rodrigo-monsalve/)

<!-- /wp:paragraph -->
