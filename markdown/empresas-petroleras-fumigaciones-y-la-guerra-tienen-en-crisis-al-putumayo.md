Title: Empresas petroleras, fumigaciones y la guerra tienen en crisis al Putumayo
Date: 2015-08-14 23:00
Category: Movilización, Nacional
Tags: ADISPA, alirio uribe, Asociación Nacional de Zonas de Reserva Campesina., Comisión de Justicia y Paz, fumigaciones con glifosato, Iván Cepeda, Jany Silva, Mesa Regional de Organizaciones sociales del departamento del Putumayo, Paramilitarismo, Pueblo Nasa del Putumayo, Putumayo, Zona de reserva Campesina del Putumayo
Slug: empresas-petroleras-fumigaciones-y-la-guerra-tienen-en-crisis-al-putumayo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/06/PUTUMAYO.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: archivo 

<iframe src="http://www.ivoox.com/player_ek_6741211_2_1.html?data=l5yhk5eVdY6ZmKiakpWJd6KkkZKSmaiRdI6ZmKiakpKJe6ShkZKSmaiRic7k08rgw9iPtMbo09TZx9fFt4ampJDT19LNq8LXytTbx9iPvYzgwpDU18rWtsKf1c7S0MrSb8bijoqkpZKns8%2FowszW0ZC2pcXd0JCah5yncZU%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Jani Silva, presidenta de ADISPA] 

###### [14 Ago 2015] 

Este 15 de Agosto se realizará la **audiencia senatorial en el municipio de Puerto Asís**, en la que se espera que comparezcan la Agencia Nacional de Licencias Ambientales, ANLA y el INCODER, así como los demás entes municipales y nacionales competentes en materia de legislación ambiental. En la audiencia se realizarán denuncias y se presentarán casos que la comunidad viene preparando desde hace cerca de un año.

**Jany Silva, presidenta de ADISPA**, refiere que son grandes las expectativas de las comunidades por la Audiencia de este sábado, dada la difícil situación que se vive actualmente en relación con las problemáticas ambientales y sociales en el Putumayo por cuenta del accionar de las **empresas petroleras, la persistencia de la guerra y la falta de posibilidades económicas de dignidad.**

<iframe src="http://www.ivoox.com/player_ek_6744288_2_1.html?data=l5yhlpecfI6ZmKiakpWJd6KkkZKSmaiRdI6ZmKiakpKJe6ShkZKSmaiRic7k08rgw9iPtMbo09TZx9fFt4ampJDT19LNq8LXytTbx9iPvYzgwpDU18rWtsKf1c7S0MrSb8bijoqkpZKns8%2FowszW0ZC2pcXd0JCah5yncZU%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Oscar Piso, líder indígena Nasa] 

**Oscar Piso, lider indígena del pueblo Nasa,** comenta que la presencia de las multinacionales petroleras, así como las intenciones gubernamentales de transnacionalizar los territorios **han agudizado la violación de los derechos colectivos, así como el conflicto armado en la región**. Comenta además, que ante las amenazas, muertes y estigmatizaciones por parte de las empresas el gobierno no se ha manifestado de forma responsable, lo que sigue poniendo en peligro sus vidas.

<iframe src="http://www.ivoox.com/player_ek_6744668_2_1.html?data=l5yhlpuafI6ZmKiakpiJd6KlmJKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRic7k08rgw9iPtMbo09TZx9fFt4ampJDT19LNq8LXytTbx9iPvYzgwpDU18rWtsKf1c7S0MrSb8bijoqkpZKns8%2FowszW0ZC2pcXd0JCah5yncZU%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Danilo Rueda, Comisión de Justicia y Paz] 

Danilo Rueda, defensor de derechos humanos, integrante de la Comisión de Justicia y Paz, quienes acompañan desde hace una década a estas comunidades, declara que los **intereses de los sectores empresariales en la región coinciden con la aplicación de la violencia paramilitar**, así como con la violación de derechos humanos y la militarización de los territorios, afectando las Zonas de Reserva Campesina y territorios colectivos indígenas.

<iframe src="http://www.ivoox.com/player_ek_6745008_2_1.html?data=l5yhl5WUfI6ZmKiak5WJd6KklZKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRic7k08rgw9iPtMbo09TZx9fFt4ampJDT19LNq8LXytTbx9iPvYzgwpDU18rWtsKf1c7S0MrSb8bijoqkpZKns8%2FowszW0ZC2pcXd0JCah5yncZU%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Alirio Uribe, representante a la Cámara] 

Por parte del congreso de la república asistirán el Senador Iván Cepeda y el representante a la cámara Alirio Uribe quien afirma que esta audiencia puede arrojar resultados en el corto y mediano plazo que generen respuestas a las exigencias de las comunidades.
