Title: Indultado de las FARC es amenazado de muerte por paramilitares en Envigado
Date: 2016-02-05 16:45
Category: Nacional, Paz
Tags: FARC, guerrilleros indultados FARC, Juan Manuel Santos, Proceso de conversaciones de paz
Slug: indultado-de-las-farc-es-amenazado-de-muerte-por-paramilitares-en-envigado
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/02/indultados_contagioradio.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: primeronoticias.] 

###### [05 Feb 2016]

Según la información publicada por el medio “Análisis Urbano” y la organización CORPADES dirigida por Fernando Quijano, el señor Wilson Antonio López, uno de los **integrantes del grupo de las FARC [indultado por el gobierno nacional](https://archivo.contagioradio.com/el-indulto-a-guerrilleros-de-las-farc/)**, recibió amenazas de muerte que lo obligaron a desplazarse de su lugar de residencia en medio de su delicado estado de salud.

La denuncia hace referencia a que el pasado 1 de Febrero López fue abordado por varios **hombres que se identificaron como integrantes de “la oficina”** quienes le indicaron que ya sabían quién era, a qué se dedicaba y de dónde venía, y le adivirtieron "mire que va a hacer". El 3 de febrero recibió una llamada telefónica en la recalcaban que lo tenían identificado.

"*Los hechos se presentaron el 1 de febrero, Wilson Antonio López se encontraba en su hogar cuando  varios individuos quienes se identificaron como miembros de la Oficina, se acercaron a decirle que a través de noticias que aparecieron en los medios de comunicación se habían dado cuenta de quien era él, a qué se dedicaba y de dónde venía, que se lo decían “para que mire a ver qué va a hacer”.*

*Posteriormente el 3 de febrero, Wilson Antonio López Tamayo recibió una llamada amenazante donde le anunciaron: “Casi que no nos damos cuenta de quién es usted, lo vimos en televisión, se lo tenía muy bien guardado. Sabemos dónde vive y dónde está usted”.*

Hasta el momento se desconoce si las autoridades han realizado algún tipo de acción para la protección de la vida de uno de los primeros integrantes de las FARC que retorna a la vida civil en medio de las decisiones que se han tomado en el proceso de conversaciones de paz.
