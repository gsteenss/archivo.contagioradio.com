Title: El día que Orson Welles paralizó a los EEUU
Date: 2017-02-20 16:39
Author: AdminContagio
Category: 24 Cuadros
Tags: Cine, Guerra de los mundos, Orson Welles, radio
Slug: orson-welles-guerra-mundos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/02/16901544_757627511059108_1553146993_n-e1487626203289.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: CBS 

###### 20 Feb 2017 

Es imposible hablar de la relación entre el cine y la radio sin tener en cuenta la figura de **Orson Welles**, uno de los directores más importantes de la historia del séptimo arte, quien antes de adentrarse en el mundo del cine, ya era una figura importante en el teatro y la radio estadounidense.

En 1938, su adaptación de **“La Guerra de los Mundos”** pasaría a la historia como la ocasión en la que una emisión de radio sembraría pánico en miles de oyentes convencidos de que Estados Unidos estaba siendo invadido por extraterrestres. Le puede interesar: ["Chavela", ovacionada en Berlín](https://archivo.contagioradio.com/chavela-ovacionada-berlin/).

Aquel incidente llamó la atención de la compañía cinematográfica RKO Pictures, la cual ofreció un contrato a Welles en 1939 con plena libertad de escribir, dirigir y producir dos películas. Lo demás es historia. Entre la filmografía de Orson Welles podemos encontrar gran cantidad de obras maestra como Citizen Kane, Touch of Evil y Macbeth.

En el cine flashback de 24 cuadros, hablamos de este importante suceso. ¡Viva el cine y viva la radio!

<iframe id="audio_17117371" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_17117371_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>
