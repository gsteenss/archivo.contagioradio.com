Title: Crisis agropecuaria vuelve a movilizar campesinos colombianos
Date: 2015-06-22 17:07
Category: Movilización, Nacional
Tags: Alonso Osorio, Azúcar, Café, Coca Cola, Comunidad Andina de Naciones, Dignidad Agropecuaria, Dignidad Cafetera, Panela
Slug: crisis-agropecuaria-e-incumplimiento-del-gobierno-generan-nueva-movilizacion-campesina
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/06/dignidad-agropecuaria-contagioradio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: lapatria 

###### [22 Jun 2015] 

Según los productores de café, **producir un kilo de café cuesta más de \$5300, pero se está vendiendo máximo a \$5000**. El tema de las deudas no se solucionó y no hay control real para los 5 monopolios importadores que funcionan en el país. Los precios de los fertilizantes no se han controlado y tampoco se hace un control sobre los 6 o 7 monopolios de importación que funcionan, afirma Alonso Osorio, integrante de la junta directiva de **Dignidad Cafetera**.

A esa serie de incumplimientos se suma el hecho de que la revaluación, aunque controlada, está **afectando directamente a las 560 mil familias cafeteras**. Según Osorio, el problema del sector es estructural y requiere salidas estructurales. Uno de los puntos neurálgicos de la crisis son las deudas de los pequeños cafeteros que ascienden a 20 mil millones de pesos. Según Osorio, aunque el gobierno pagó el subsidio en 2013, no hay recursos para atender la crisis y las deudas de 2014.

Además hay una crisis en la cadena del dulce, por ejemplo, **producir un kilo de panela puede costar \$1800 y se vende en solamente \$1000**, y producir un **litro de leche cuesta \$700 y se vende en los mismos \$700**, y el gobierno amenaza  con desmontar los precios mínimos de este producto.

**En el caso del Azucar y la Panela sobran 1 millón de toneladas al año y el gobierno, a través de los TLC autoriza la importación de cerca de 320 mil toneladas**, lo que aumenta el sobrante, que sirve para bajar los precios nacionales.

Los integrantes de Dignidad Agropecuaria denuncian también que para el dulce hay un precio establecido por la **Comunidad Andina de Naciones** y la empresa **Coca Cola está exigiendo que el gobierno desmonte esa regulación** para poder comprar el azúcar por fuera de la producción nacional, lo cual aumentaría los sobrantes y afecta directamente a los pequeños productores, tanto de azúcar como de panela.

Así las cosas, se estudia que para los próximos días se reúnan los líderes de los movimientos de **Dignidad Agropecuaria** para estudiar una nueva jornada de movilización a nivel nacional.
