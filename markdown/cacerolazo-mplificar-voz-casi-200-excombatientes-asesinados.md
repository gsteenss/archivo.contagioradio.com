Title: Un cacerolazo para amplificar la voz de casi 200 excombatientes asesinados
Date: 2020-02-24 17:21
Author: CtgAdm
Category: DDHH, eventos
Tags: asesinato de excombatientes, cacerolazo, FARC
Slug: cacerolazo-mplificar-voz-casi-200-excombatientes-asesinados
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/02/Excombatientes.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/02/ERj7C2JXUAEPF_k.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto excombatientes: FARC

<!-- /wp:paragraph -->

<!-- wp:html -->

<figure class="wp-block-audio">
<audio controls src="https://co.ivoox.com/es/victoria-sandino-sobre-cacelorazo-garantia-de_md_48223251_wp_1.mp3&quot;">
</audio>
  

<figcaption>
Entrevista &gt; Victoria Sandino | Senadora partido FARC

</figcaption>
</figure>
<!-- /wp:html -->

<!-- wp:paragraph {"align":"justify"} -->

Este 25 de febrero, el partido Fuerza Alternativa Revolucionaria del Común convocó en la ciudad de Bogotá, desde las 6:00 pm, un cacerolazo en el Parque de las periodistas que incluirá una serie de expresiones culturales artísticas y simbólicas en respuesta al silencio del Estado ante la muerte de excombatientes, 12 de los cuales han sido asesinados en lo corrido del 2020, **alertó el último reporte de la Misión de Verificación de la Organización de las Naciones Unidas.**

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

La escalada de la violencia contra firmantes de la paz se ha hecho más evidente tras los hechos ocurridos entre el viernes 21 y el sábado 22 cuando fueron asesinados Esder Pineda en Algeciras, Huila donde residen otras 53 personas en reincorporación; Winston Moreno, también excombatiente, y Kevin Cuesta, de 17 años de edad, hijastro de otro exmiembro de FARC en Quibdó, Chocó. Frente a esta situación y aunque el colectivo ha solicitado una reunión con la ahora ministra del ministra del Interior, Alicia Arango, no han tenido respuesta, denuncia el partido.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

**La senadora Victoria Sandino** explica que no solo se está abordando el cacerolazo como alternativa, a su vez, desde las regiones se están buscando otras alternativas como convertir los Espacios Territoriales de Capacitación y Reincorporación (ETCR) en campamentos de paz, acciones que serán dirigidas para visibilizar el incumplimiento y la falta de garantías para excombatientes, "esperamos que los colombianos y colombianas se sensibilicen porque es algo que nos debe interesar a todos", manifestó la congresista. [(Le puede interesar: Asesinan a Daniel Jimenez, excombatiente de las FARC en Puerto Guzmán)](https://archivo.contagioradio.com/asesinan-a-daniel-jimenez-excombatiente-de-las-farc-en-puerto-guzman/)

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Excombatientes contemplan el asilo

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Otros senadores de la bancada como Carlos Lozada han denunciado que ante los cada vez más frecuentes asesinatos contra excombatientes, el Ministerio del Interior no se ha pronunciado por lo que se requieren otro tipo de medidas. **"No es solo que trate de implementar esquemas de seguridad, sino que pare este accionar, porque ya muchos están pensando en pedir exilio para que otros países les garanticen su vida"**, explicó la senadora ante otras alternativas que garanticen la vida de las y los reincorporados. [(Lea también: Garantías de vida y seguridad para excombatientes se quedaron en el papel)](https://archivo.contagioradio.com/garantias-de-vida-y-seguridad-para-excombatientes-se-quedaron-en-el-papel/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Aunque el [partido FARC](https://twitter.com/PartidoFARC/status/1231614737194143744) hizo referencia a 190 excombatientes asesinados, otras fuentes como el Instituto de Estudios para el Desarrollo y la Paz (Indepaz) señalan que los homicidios superan los 200. Más allá de la cifra, Sandino es enfática en que el Gobierno debe tomar medidas efectivas para detener las agresiones contra quienes creyeron en una paz y apuestan por ella. .

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->
