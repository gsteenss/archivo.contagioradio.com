Title: ¡Golpe de Estado!  
Date: 2018-09-25 12:13
Author: JOHAN MENDOZA TORRES
Category: Johan Mendoza, Opinion
Tags: Golpe de Estado, golpe de Opinión
Slug: golpe-de-estado
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/09/golpe-de-estado-770x400-1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Contagio Radio 

###### [**Por: [Johan Mendoza Torres](https://archivo.contagioradio.com/johan-mendoza-torres/)**] 

###### 25 Sep 2018 

[Menudo problema atraviesa la democracia colombiana, un tremendo lío que nace en el seno mismo de la representatividad, un problema que niega cualquier oportunidad política al voto, un problema que niega a la democracia misma, leída esta, como una participación efectiva de los actores sociales en el ejercicio del poder político; hay un problema que niega ese carácter vinculante que debería tener el estatus de ciudadano con pleno derecho ¡no a que lo encuesten! ¡no a votar por alguien! ¡sino derecho a decidir algo!... ese problema, es que la “democracia” colombiana funciona bajo el modelo de la cooptación.]

[La cooptación funciona de tal manera que aquellos politiqueros que se encuentran en los cargos más lucrativos y poderosos del Estado, tienen el dinero (público), la potestad para designar y decidir quiénes ocuparán otros cargos en el Estado (unos más poderosos que otros) que componen en conjunto, la red clientelar que no permite el ejercicio del poder político por parte de los actores sociales, auténticos dueños de ese poder.]

[Es una obviedad en la democracia representativa, que uno vote para que alguien lo represente, no solo como sujeto, sino como parte constitutiva de una sociedad. Pero no es normal que uno vote, para que la representatividad política (con fin colectivo) se convierta en una cooptación, que claramente termina favoreciendo intereses privados y económicos particulares.]

[Mientras el ejercicio del poder político se haya desfigurado en una cooptación, entonces así votáramos todos los domingos, aquí no habría democracia nunca. ¿Qué queda entonces? Claro, es fácil criticar, pero qué difícil es proponer… vista la situación, lo que se denomina comúnmente como Golpe de Estado, es una madrugada esperanzadora que promete convertir en recuerdo esta noche tan oscura que cubre a Colombia.]

[¡Claro! uno no es Arizmendi como para promover Golpes de Estado con total tranquilidad por los medios masivos, por eso, antes de meterme en líos, debo aclarar el sentido de este artículo.  Hay tipologías del Golpe de Estado. Está el]**Cuartelzo**[, es decir el de los militares, el que tenemos como idea común de Golpe de Estado, a ese échele tierrita, porque aquí los manuales del ejército y la policía son norteamericanos, y mientras esa médula no se cambie, o no se erradiquen las primas del silencio o dejen de pedirle la baja al coronel que comienza a leer un poco más, el sistema se mantendrá estable.]

[Está el]**Palaciego**[. Se trata de ese golpe que podrían dar los ministros al presidente… ¡ja! La sola idea da risa, precisamente debido a la cooptación. Los ministros, como gran parte de los cargos más poderosos, por más de que sean legales, no son cargos democráticos, sino tiránicos (por ser amiguistas) y los amigos del presidente jamás levantarían un dedo contra el presidente.]

[¿Qué queda? El]**Golpe de Opinión,** [parece una utopía, pero no está tan lejana como para dejar de cultivarla. Este consiste en que la gente, la ciudadanía, con plena consciencia no solo del engaño al que está sujeta, sino con plena consciencia de lo que en verdad aspira políticamente, se constituye en un grupo de presión capaz de bloquear y desestabilizar la dinámica y la tranquilidad del sistema, con un principio ideológico general (para no entrar en discusiones deliciosamente largas) que sería el de la]*[desobediencia civil]*[, logrando así, derrocar al gobierno que se opusiera.]

[Cabe aclarar que, en el golpe de opinión, los medios masivos de comunicación juegan el papel de escuderos de la tiranía, “amigo televidente” no… esos canales NO están de su lado. De hecho, para que un golpe de opinión tenga probabilidad, hay una lucha que se debe librar en el campo del signo, del símbolo, de la cultura y que no se puede mediar, ni conciliar. Hay que ganarla.]

[Se trata pues, de hacer frente a la hegemonía cultural que la clase dominante conserva mediante la cooptación de los medios con los cuales nos informamos o nos entretenemos, la cooptación de la imposición de las formas en las que nos tenemos que expresar políticamente, la cooptación de las instituciones en las cuales nos educamos, y la cooptación de la libertad de elegir… aunque suene absurdo, hay que luchar para dejar de elegir lo que ya se ha decidido, y eso paradójicamente significa que necesitamos preparar un gran rechazo a “La Libertad”.]

[¿Cómo planificar ese golpe de Estado? La pregunta ya de por sí es sediciosa. No obstante, muchos lo han dicho y lo han escrito, cuando un gobierno constituye una tiranía, la resistencia es un derecho natural; claramente, las condiciones de esa resistencia, por lo general las impone la tiranía y no quien se resiste. En el campo andan matando a la gente a bala, así que, si la gente decide defenderse a bala, pues que no se nos haga raro y que no se riegue la espuma del discurso pacifista de sillón, ya que sería lo mínimo que podría generar esa represión sistemática contra gente que nunca quiso la guerra.]

[En la ciudad la cosa no opera así (aún) por eso es necesario comprender que los mecanismos de opresión parecen invisibles, no duelen, es más, todo indica que son dulces y placenteros, ligeros y brillantes, y de este modo acaparan más idiotas.]

[¿Pero cuál tiranía? Se preguntarán muchos que se escudan en la institucionalidad colombiana como la figura que soporta el trámite legal e infraestructural de un Estado… A esos se les responde muy fácilmente, y es que, si esa institucionalidad está basada en la cooptación, entonces no hay democracia, por tanto, la institucionalidad basada en un andamiaje de cargos colocados por amistad, en edificios construidos por contratación amiguista por razones familiares o intereses particulares y no políticos (es decir no-colectivos), es una institucionalidad ilegítima.  ]

[No hay democracia si la cooptación es el modus operandi del sistema, lo que hay es tiranía.  La cooptación como mecanismo funcional del clientelismo en Colombia, constituye una tiranía. Es una tesis defendible, sobre todo aquí, en Colombia.]

[Por ahora, la opinión, es decir, grupos de ciudadanos conscientes de las problemáticas, continúan hoy constituyéndose, educándose, protestando, conspirando y poco a poco, configuran una fuerza que un día será capaz de dar un golpe certero. Ese día ya habita en nuestros sueños, pero no solo allí, también habita en las calles, en las ciudades y en el campo, existe en nuestras protestas, virtuales o presenciales contra todo lo que ocurre en el país, por tanto, ya ese día ya dio el primer paso: existir.]

#### [**Leer más columnas de Johan Mendoza Torres**](https://archivo.contagioradio.com/johan-mendoza-torres/)

------------------------------------------------------------------------

###### Las opiniones expresadas en este artículo son de exclusiva responsabilidad del autor y no necesariamente representan la opinión de Contagio Radio.
