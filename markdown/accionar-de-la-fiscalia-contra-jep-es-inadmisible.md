Title: Accionar de la Fiscalía contra JEP es inadmisible: Comisión Colombiana de Juristas
Date: 2018-10-05 12:46
Author: AdminContagio
Category: Nacional, Paz
Tags: Fiscalía, jurisdicción especial para la paz, Nestor Humberto Martínez, Patricia Linares
Slug: accionar-de-la-fiscalia-contra-jep-es-inadmisible
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/10/DTtnaeTX4AEqkHJ-770x400-1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Twitter Jep] 

###### [05 Oct 2018] 

El director de la Comisión Colombiana de Juristas, Gustavo Gallón, rechazó la intromisión de la Fiscalía al llevarse un archivo de la Jurisdicción Especial para la Paz, y afirmó que, aunque la autoridad judicial haya señalado que revocó la orden para obtener una copia de un informe que reposaba en el Tribunal,**  este hecho es un mensaje muy grave y una acción "inadmisible" contra una autoridad de la República.**

Gallón manifestó que en reiteradas ocasiones la Fiscalía se ha entrometido en la funciones de la JEP, sin embargo con el hecho más reciente, "habría pasado la raya" de autoridad sobre el Tribunal, al atreverse a realizar una inspección judicial y sacar copias de un expediente, que en su mayoría había sido construido con los aportes del ente judicial sobre casos de secuestros cometidos en el marco del conflicto armado por la FARC.

Asimismo, señaló que este hecho abre muchas preguntas frente a los intereses de la Fiscalía sobre el trabajo que debe realizar la JEP, una de ellas, se refiere al respaldo que el presidente Iván Duque le dio a la autoridad juficial, y se cuestiona si esta acción fue concertada entre la presidencia, el ejecutivo y la Fiscalía. Otra, tendría que ver con **¿qué podía pretender conocer la Fiscalía, al hacer una inspección judicial sobre el expediente que tiene que ver con los secuestros atribuidos a la FARC?**

Adicionalmente, Gallón expresó que si bien la Fiscalía revocó la orden de esta diligencia, esa acción se hizo en el marco de una investigación que adelanta la autoridad por irregularidades en la JEP. (Le puede interesar[: "Plena disposición de la JEP para que se esclarezcan investigaciones de la Fiscalía"](https://archivo.contagioradio.com/plena-disposicion-de-la-jep-para-que-se-esclarezcan-investigaciones-desde-la-fiscalia/))

### **Hay que investigar a la Fiscalía** 

Para el director de la Comisión Colombiana de Juristas, el paso siguiente debe ser que se realice una investigación desde la Procuraduría en contra de la Fiscalía, para esclarecer si esta **"actividad se hizo en desarrollo de facultades legales, correctas o por el contrario fue un exceso y abuso de poder".**

Asimismo afirmó que se requiere una acción de rechazo por parte de la ciudadanía, frente a esta "persistente actitud en contra del desarrollo de la valiosa competencia que le ha sido atribuida a la JEP, para que se haga justicia en relación con los graves crímenes cometidos en el conflicto".

<iframe id="audio_29121477" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_29121477_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
