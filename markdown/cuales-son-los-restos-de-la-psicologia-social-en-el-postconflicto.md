Title: ¿Cuáles son los retos de la psicología social en el postconflicto?
Date: 2016-05-27 16:17
Category: eventos, Nacional
Tags: 'I Encuentro Nacional Postconflicto Salud Mental y Acciones Psicosociales Hacia la Paz', Cátedra Martín-Baro, Posconflicto en Colombia
Slug: cuales-son-los-restos-de-la-psicologia-social-en-el-postconflicto
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/05/Víctimas.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Universal ] 

###### [27 Mayo 2016 ]

Desde este jueves y hasta el próximo domingo se lleva a cabo el 'I Encuentro Nacional Postconflicto Salud Mental y Acciones Psicosociales Hacia la Paz', con el cual se busca contribuir a los planes, políticas y programas para la intervención en comunidades en un eventual escenario de postacuerdo; con el fin de consolidar procesos de construcción de paz con garantías de sostenibilidad social, política, económica y cultural.

De acuerdo con la psicóloga Johana Rodríguez, integrante de la Comisión Intereclesial de Justicia y Paz, este Encuentro se realiza en el marco de la Cátedra Martín-Baro, que con 14 años de trayectoria ha aportado al desarrollo acciones psicosociales e intervenciones en comunidades en el marco del conflicto armado, y que ahora se plantea fortalecer esta acción en el marco del postconflicto.

El eventual escenario de postconflcito plantea para la psicología social en Colombia retos y desafíos, teniendo en cuenta que el conflicto armado interno generó afectaciones, miedos, y desesperanzas, y que con la firma del acuerdo no se garantiza la superación de las causas estructurales que originaron el conflicto, pues aún persiste el despojo y los intereses sobre los territorios, asegura la psicóloga.

[![Afiche Oficial Congreso Posconflicto](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/05/Afiche-Oficial-Congreso-Posconflicto.jpg){.aligncenter .size-full .wp-image-24455 width="900" height="1245"}](https://archivo.contagioradio.com/cuales-son-los-restos-de-la-psicologia-social-en-el-postconflicto/afiche-oficial-congreso-posconflicto/)

<iframe src="http://co.ivoox.com/es/player_ej_11691868_2_1.html?data=kpajm5acepmhhpywj5WcaZS1kZaah5yncZOhhpywj5WRaZi3jpWah5yncavjycbbw5C2s8Xmhqigh6aoq9bZ25Caja%2FZt9XdxM7Ojd6PlMLujoqkpZKns8%2FowszW0ZC2pcXd0JCah5yncZU%3D&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU) ]] 
