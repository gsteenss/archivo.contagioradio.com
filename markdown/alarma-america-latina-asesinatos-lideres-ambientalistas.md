Title: Alarma en América Latina por asesinatos de líderes ambientalistas
Date: 2017-01-24 17:27
Category: Ambiente, Voces de la Tierra
Tags: goldman, Isidro Baldenegro, mexico, ONU
Slug: alarma-america-latina-asesinatos-lideres-ambientalistas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/01/Isidro-e1485297004711.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: [Goldman Environmental Prize]

###### [24 Ene 2017] 

Por defender la vida, su comunidad y los bosques, otro premio Goldman fue asesinado el pasado 15 de enero. Esta vez, se trata del **líder indígena y ambientalista, Isidro Baldenegro López, quien llevaba una lucha de más de 30 años,** contra empresarios aliados con narcotraficantes y madereros, que talaban ilegalmente los árboles ancestrales del pueblo Rarámuri, en México.

De acuerdo con las autoridades, ya se ha logrado dar con la identificación del hombre que habría asesinado al ambientalista. Se trataría de **un joven de 25 años de nombre Romeo R.M. y de la comunidad de Las Papas,** ubicada a 80 kilómetros del lugar donde fue asesinado Baldenegro, quien perdió la vida tras recibir seis impactos de bala en el pueblo Coloradas de la Virgen.

### La lucha de Isidro Baldenegro 

El ganador del Premio Goldman del año 2005, fue galardonado, al destacarse en “su trabajo implacable organizando protestas pacíficas contra la tala ilegal en las montañas de la Sierra Madre ayudando a proteger los bosques, las tierras y los derechos de su pueblo”, como se describió en la ceremonia de premiación. [(Le puede interesar: Estado, bancos y empresas estarían detrás del asesinato de Berta Cáceres: COPINH)](https://archivo.contagioradio.com/estado-bancos-y-empresas-estarian-detras-del-asesinato-de-berta-caceres-copinh/)

**Su lucha data desde 1980** cuando se empezaron las primeras protestas en contra de los taladores. Luego iniciaron todos los trámites legales para que las tierras de los indígenas no fueran despojadas y con ello lograran legalizar la tala.  Sin embargo, aunque la comunidad ganó la pugna, los empresarios continuaron con la actividad ilegal, violentando los derechos de las comunidades y generando graves impactos ambientales en esa zona de México.

Fue entonces cuando más se fortaleció la lucha del pueblo Rarámuri, guiada por Baldenegro, quien lideró una ONG en 1993 para combatir la deforestación. Además, organizó decenas de movilizaciones de su comunidad que lograron el cierre temporal de la tala por parte del gobierno en el año 2002. Un año después, realizó una protesta con la que logró la prohibición de la tala por decisión judicial, pero ese mismo año, el ambientalista fue judicializado y tras **15 meses en prisión fue absuelto de todos los cargos.**

### Aumento de asesinatos de líderes ambientalistas 

Hoy la preocupación por los líderes ambientalistas del mundo es cada vez mayor, pues cabe recordar que según el más reciente informe de la ONG, [Global Witness al menos 185 activistas fueron asesinados durante el 2015](https://archivo.contagioradio.com/durante-2015-fueron-asesinados-26-ambientalistas-en-colombia/), lo que quiere decir, tres por semana, de los cuales el 66% de las víctimas son latinoamericanas.

Frente a ese panorama, Erik Solheim, director Ejecutivo de ONU Medio Ambiente, se mostró preocupado por la situación de indefensión de este tipo de líderes, y aseguró que el asesinato de Isidro "destaca trágicamente los peligros mortales que enfrentan los defensores ambientales en América Latina y el Caribe y en todo el planeta, y **los vínculos entre el crimen organizado y la destrucción del medio ambiente. Nadie debería morir por proteger la naturaleza”.**

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
