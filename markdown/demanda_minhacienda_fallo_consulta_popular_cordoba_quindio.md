Title: Denuncian a MinHacienda por impedir consulta popular en Córdoba, Quindío
Date: 2018-01-11 17:15
Category: Ambiente, Nacional
Slug: demanda_minhacienda_fallo_consulta_popular_cordoba_quindio
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/01/5a1c8a2770ed6.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Laura Sepúlveda] 

###### [11 Ene 2018] 

Pese a que el Tribunal Administrativo del Quindío había dado la orden al Ministerio de Hacienda para tramitar los recursos necesarios para desarrollar la consulta popular en el municipio de Córdoba, Quindío, esa cartera ha hecho caso omiso a dicha decisión. Por ello el comité promotor de la consulta popular en Córdoba anunció que interpondrá una denuncia penal en contra del ministro de Hacienda, Mauricio Cárdenas, por desacato.

**“El fallo es muy claro al ordenar a la Registraduría y a Minhacienda que adelanten las actividades necesarias para garantizar la realización de la consulta. **La Registraduría lo hizo y el ministerio nunca se comunicó con la alcaldía y no respondió los requerimientos que se le hicieron para que participará en los dos comités de seguimiento electoral en diciembre”,  señala  Carlos Andrés Gómez, vocero del Comité a Crónica del Quindío.

**El líder agrega que si bien la Registraduría ya ha acatado la orden y estipuló la fecha para el próximo 21 de enero, el Ministro se ha burlado** de la orden del Tribunal y se ha pasado por la ‘faja’ la decisión de los magistrados".

La denuncia está basada en el artículo 454 del Código Penal Colombiano, donde  se establece **el delito de fraude a resolución judicial. De ser así, este funcionario público podría pagar hasta 6 meses de prisión, además de una multa entre 5 y 50 salarios mínimos legales vigentes.**

### **Habitantes se alistan para la consulta ** 

Mientras tanto ya se adelantan los preparativos del evento electoral y el Comité se ha reunido con las autoridades pertinentes como el registrador local, el alcalde,** **los secretarios de Gobierno y Planeación y el vocero del comité promotor.**  Además se adelantan actividades pedagógicas, aunque aseguran que la población es consiente de la importancia de participar de la consulta.**

“Hay que dejar en claro que el municipio, desde un principio, dijo que tenía el dinero para hacer la consulta, pero fue el mismo ministerio el que dijo que no se podía.** Ellos cambiaron de manera abrupta las reglas y eso no lo pueden hacer. **Esto es una burla al sistema de participación democrática de los ciudadanos, no solo de Córdoba, sino de otros 10 municipios que están en la misma situación”, dice Gómez.

Cabe recordar que en Quindío también el consejo municipal de Salento, le dio el visto bueno a la posibilidad de que a través de una consulta popular puedan decidir si quieren o no el desarrollo de actividades mineras en su territorio, de manera que se protejan riquezas naturales del país como el Valle del Cocora.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
