Title: 20 años de justicia agridulce para las víctimas de la Masacre de Mapiripán
Date: 2017-07-15 11:30
Category: DDHH, Entrevistas
Tags: General Uscátegui, mapiripan, Masacre de Mapiripan, paramilitares
Slug: 20-anos-de-justicia-agridulce-para-las-victimas-de-la-masacre-de-mapiripan
Status: published

###### [Foto: Archivo] 

###### [14 Jul. 2017] 

Según los juristas que han acompañado el caso, la **masacre de Mapiripán fue una de las estrategias utilizadas por el Ejército en los llanos orientales para comenzar la expansión paramilitar.** Comenzó en Mapiripán, y se abrió paso en diversas veredas y corregimientos en donde sembró el terror y el dolor. La Cooperativa, Charra, Puerto Alvira fueron algunas de esas poblaciones que fueron testigas de la masacre y del dolor en la historia reciente en Colombia.

**Luis Guillermo Pérez del Colectivo de Abogados José Alvear Restrepo** quienes acompañan jurídicamente a las víctimas recuerda que “llegaron a Mapiripán saliendo de un aeropuerto militarizado y controlado por la Brigada XVII a otro aeropuerto en San José de Guaviare controlado por la Brigada VII, luego los paramilitares fueron acompañados por la Brigada Móvil Nº 2 del Ejército que dirigía el entonces Coronel Lino Sánchez que estaba en curso de ascenso a General”.

### **20 años de justicia agridulce** 

A pesar de las decisiones tomadas en materia jurídica por organizaciones internacionales como la Corte Interamericana de Derechos Humanos, CorteIDH, que condenó al Estado por violación a los derechos de los habitantes de Mapirirán y decisiones como las de la Corte Suprema de Justicia en la que halló **culpable el General Jaime Uscategui por el delito de comisión por omisión, secuestro simple y homicidio,** entre otros el abogado de las víctimas manifiesta que sigue habiendo un sabor agridulce.

“Luego de 20 años es un sabor muy agridulce, **tenemos una sentencia de la CorteIDH que ha sido muy importante, pero hay una estrategia de manipulación,** de querer hacerle daño a las víctimas, a nuestra imagen de credibilidad como defensores de derechos humanos, que no va a parar, menos ahora con la JEP” asevera Pérez.

Otras sanciones penales son las del Coronel Hernán Orozco Castro a 37 años de prisión, quien era el comandante encargado del Batallón Joaquín París y quien fue el que denunció al General Uscátegui por haber impedido que protegieran a la población de Mapiripán.

“Pero no se ha investigado ni sancionado a los determinadores principales que por encima de los paramilitares y de estos militares fueron miembros de la Cuarta División del Ejército, de **la cúpula de las fuerzas armadas de la época que hicieron parte del diseño de esa estrategia”** recalcó Pérez.

### **Uscátegui podría ser beneficiado por la JEP** 

Organizaciones sociales y víctimas de la masacre de Mapiripán han rechazado de manera categórica la decisión tomada el 5 de mayo, a propósito de la Ley 1820 de 2016 de amnistía e indulto, de revisar la condena del General Uscátegui en la Jurisdicción Especial de Paz - JEP – y por ende de dejarlo en libertad con el compromiso de comparecer ante la JEP.

**“En este contexto de paz y de la JEP el General Uscátegui pretende favorecerse mintiendo y revicitmizando a las víctimas** sin ningún compromiso real con la verdad y con la justicia, pues no puede ser acreedor de unos beneficios por los que está gozando de libertad. Esperamos que esa decisión sea revocada” dice Pérez.

Por esta razón el Colectivo de Abogados espera que la decisión del juzgado 21 de Ejecución de Penas de Bogotá sea echada abajo y una respuesta por parte de la Fiscalía a la solicitud de declarar esta masacre como crimen de lesa humanidad.

### **¿Qué otras acciones pueden hacerse en la vía legal?** 

Asegura el abogado que **van a seguir trabajando en la Fiscalía General para que se logre la imputación y acusación** de los demás generales responsables de estos hechos, así como preparándose para la JEP, donde tendrán que volver a litigar el caso.

“Le diremos a la JEP que **lejos de que el General Uscátegui sea inocente fue corresponsable de esa estrategia de terror**, que él con su omisión faltó a su deber de garante y que en todas sus exculpaciones en torno que no tenía que responder porque no era el comandante efectivo de este Batallón pues eso ya quedó demostrado que él es responsable” relata Pérez.

### **¿Cuándo llegará la justicia y la verdad?** 

“No sabemos cuándo vamos a terminar de litigar este proceso porque seguramente la JEP va a considerar esta masacre como un crimen de lesa humanidad, por lo tanto no hay prescripción y seguramente pasaremos varios años litigando de nuevo este proceso” cuenta el abogado. Le puede interesar: [Coronel (r) Plazas Acevedo será procesado por presunta responsabilidad en la Masacre de Mapiripán](https://archivo.contagioradio.com/coronel-r-plazas-acevedo-sera-procesado-por-presunta-responsabilidad-en-la-masacre-de-mapiripan/)

Así mismo, dice que **seguirán reclamando que se cumpla la sentencia de la CorteIDH,** porque si bien hay una serie de compromisos por parte de la Cancillería como atención en salud a las víctimas, la identificación de todas las víctimas, sanción a los responsables y la construcción de una casa de la memoria, entre otras, estas no han sido realizadas

### **El exilio por defender a las víctimas de Mapiripán** 

Recuerda Pérez que este ha sido un proceso difícil, por el que **tuvo que exiliarse con su familia, recibir amenazas,** razón por la cual su familia no ha podido regresar a Colombia “el trauma que le ocasionó a la que era mi esposa y a mis hijos, pues los llevó a decir que nunca iban a volver a este país. Buena parte de mi familia se encuentra en el exilio”.

Pese a estas situaciones personales, sumado al desprestigio que tuvieron que vivir por el hecho conocido como “las falsas víctimas de Mapiripán”, un escándalo suscitado en Octubre de 2011 sobre unas personas que no eran víctimas y se beneficiaron no solo de los servicios jurídicos del Colectivo de Abogados José Alvear Restrepo, sino también de las reparaciones del sistema interamericano de derechos humanos, **el abogado manifiesta que seguirá luchando contra la impunidad y por los derechos de las víctimas en este proceso.**

Concluye reconociendo el trabajo incesante que han hecho las víctimas en la búsqueda de la verdad “y **a ellas se debe nuestro trabajo y rendimos homenaje a todas las que fueron sacrificadas** pero también a todas aquellas que con valor siguen soportando amenazas por adelantar sus reclamos de justicia”. Le puede interesar: [Mujeres de Mapiripán resisten al olvido tras 20 años de la masacre](https://archivo.contagioradio.com/masacre-de-mapiripan-obligo-a-las-mujeres-a-reconstruirse/)

<iframe id="audio_19807443" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_19807443_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU).

###### 
