Title: Crónica de una clase de autonomía universitaria con Miguel Ángel Beltrán
Date: 2016-01-22 17:13
Category: Opinion
Tags: Profesor Miguel ángel Beltrán, Universidad Nacional
Slug: cronica-de-una-clase-de-autonomia-universitaria-con-miguel-angel-beltran
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/01/miguel-angel-beltran.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

#### [**Por Itayosara Rojas**] 

###### [22 Ene 2016] 

*[“Si no existe una vinculación espiritual entre el que enseña y el que aprende, toda enseñanza es hostil y por consiguiente infecunda. Toda la educación es una larga obra de amor a los que aprenden” Manifiesto de Córdoba.]*

En la Universidad Nacional de Colombia para optar al título de socióloga es necesario cursar la asignatura de “Pensamiento Sociológico Latinoamericano”; según el más reciente plan de estudios es una asignatura obligatoria.  Cuando Traté de ver esta asignatura no pude tomar todas las lecciones ya que se presentaron un par de vicisitudes con el docente a cargo de la materia.

Recuerdo puntualmente que la unidad que estábamos estudiando era “América Latina en los años 20”, y el debate propuesto para la clase fue entonces la actualidad de la Reforma Universitaria. La clase era estricta, el profesor realizaba en algunas sesiones, y sin previo aviso, controles de lectura para evaluar que tanto leíamos. Para entonces un respiro,  Nos salvamos y no hubo control de lectura, empezó la exposición magistral del profesor. En un principio caracterizó el contexto político para América Latina en los años veinte, la llegada de las ideas socialistas  y su influencia en los movimientos estudiantiles de la época

De allí aprendí de memoria el núcleo central de lo que los estudiantes de hace más de noventa años exigían como reforma universitaria: Democracia universitaria, Renovación del profesorado y función social de la universidad. Me sentí cautivada por el lenguaje de los textos, leer cada uno de los manifiestos de los congresos de estudiantes parecía algo romántico. Que varias y varios estudiantes escribieran con tanta pasión  me parecía sumamente novelesco, pues en una academia “neutral” como la que conocemos leer un texto apasionado resulta en lo personal un verdadero placer.

El espíritu de la juventud alentado por la democracia no moría en las aulas, por el contrario para esa generación la universidad tenía un papel relevante en la democratización de la sociedad y el Estado. Fue una generación de una radicalidad liberal sorprendente, que también se oponía a las viejas castas religiosas que habían negado el debate al interior de las aulas, silenciado a la universidad y que dirigían los Estados Latinoamericanos. El profesor explicaba el alcance de este movimiento social, varias de mis compañeras y algunos de mis compañeros intervinieron, todos fueron escuchados y el profesor contrastaba sus argumentos con las ideas generales de los textos.

Esa clase en particular me ha parecido una de las clases más conmovedoras que he recibido durante mi proceso de formación como socióloga de una de las mejores universidades de Colombia. El docente que daba la asignatura era nada menos que Miguel Ángel Beltrán Villegas, quien para ese momento estaba siendo destituido por la procuraduría general de la nación de su puesto como profesor en la Universidad Nacional. Su propio caso encaraba de una forma increíble cada uno de los debates que se desarrollaron en esa clase. La posibilidad de ejercer  autonomía universitaria, la urgencia de la democratización de la universidad y hasta la libertad de catedra, todos eran temas que se ponían en juego en la destitución del profesor.  Al finalizar la clase pude sentir el “vinculo espiritual entre el que enseña y el que aprende” parafraseando a los estudiantes argentinos de 1918.

Muchos acontecimientos pasaron tras esa sesión, el profesor fue notificado oficialmente de su sanción, buscaron un nuevo profesor para la asignatura, varios estudiantes inconformes protestaron por el acatamiento del fallo de la procuraduría. Y hasta se presentaron amenazas de muerte contra estudiantes que habían participado en las protestas, no  sólo en defensa de Miguel Ángel, sino por el significado de ese fallo para la autonomía universitaria, la libertad de catedra  y  la democracia en las aulas.

Realizamos un programa de radio sobre el tema,  gracias a las protestas que se desarrollaron, y se abrió un gran debate en la Universidad que se expresaba en artículos académicos por parte de profesores, foros en las facultades, programas de radio y hasta como tema de discusión en varias clases de la facultad de ciencias humanas. Y al final, lamentablemente se dictó una orden de captura contra el profesor Beltrán por hechos de los que ya había sido absuelto y fue tomado en custodia.

El próximo 25 de enero  a las  4: 30 pm se realizará  en el palacio de justicia en la sala de audiencias del primer piso la audiencia de casación del profesor Beltrán. El fallo que de allí se desprenda no solo podrá dejar en libertad al profesor que se encuentra ahora recluido en la cárcel, si no que guarda un especial significado para lo que representa Miguel Ángel, que a final de cuentas,  resulta ser un ejemplo incesante por defender la autonomía universitaria, la urgencia de la democratización de todos los escenarios de la vida social, entre ellos la universidad, y la libertad de catedra.

Todo  lo que he aprendido en mi formación como socióloga no se compara con el vínculo espiritual que sentí al ser estudiante del profesor Miguel Ángel y no solo defender la justicia en su caso sino los principios de la autonomía universitaria y la democracia. Los magistrados no solo deberán considerar el tecnicismo jurídico propio del caso, sino también el significado del fallo para estos dos pilares en la universidad latinoamericana.
