Title: En Colombia hay cinco conflictos armados: CICR
Date: 2018-12-27 17:34
Author: AdminContagio
Category: DDHH, Nacional
Tags: Catatumbo, CIRC, Conflictos Armados, DIH, violencia
Slug: colombia-conflictos-armados-cicr
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/12/dih_armas_colombia_dih_cicr-e1563722438301.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: CIRC] 

###### [27 Dic 2018] 

Para el Comité Internacional de la Cruz Roja (CICR), en Colombia existen 5 conflictos armados internos en los cuales aplicaría el Derecho Internacional Humanitario (DIH); además, en 4 ciudades del país se presentan situaciones de violencia regidas por el derecho interno y el derecho internacional de los derechos humanos.

Según el Comité el vacío de poder generado por **la salida de las FARC de los territorios, ha provocado disputas entre grupos armados, quienes luchan por tomar el control en diferentes zonas.** Esta situación ha obligado al CICR a hacer una evaluación sobre los nuevos conflictos armados no internacionales (CANI's) que se presentan en el país, y están afectando a la población, así como la propia labor del Comité.

La identificación de estos conflictos resulta útil para aplicar el DIH, en tanto t**iene implicaciones sobre el uso de la fuerza que se emplea para combatir a los grupos alzados en armas, así como las normas para respetar a la población civil** y miembros de Organizaciones No Gubernamentales (ONG's). (Le puede interesar: ["Los retos humanitarios para 2018 según el CICR"](https://archivo.contagioradio.com/los-retos-humanitarios-para-2018-segun-el-cicr/))

En el caso de la CICR, los criterios que deben cumplirse para que se reconozca la existencia de un CANI son: Que "las hostilidades alcanzan un nivel mínimo de intensidad", lo que incluye una duración, número de personas que participan en los enfrentamientos y armas utilizadas, entre otras; y que los grupos armados que "participan en los actos de violencia están suficientemente organizados", es decir, que tienen una estructura jerárquica, con cadena de mando capaz de "planificar, coordinar y llevar a cabo operaciones militares".

### **En cuatro CANI's está involucrado el Estado ** 

Para determinar si una acción violenta obedece a un CANI no es necesario que el grupo perpetrador del ataque tenga motivaciones políticas, económicas, étnicas o religiosas; por esta razón, el Comité de la Cruz Roja considera que **hay 4 conflictos "entre el Gobierno del país y el Ejército de Liberación Nacional (ELN), el Ejército Popular de Liberación (EPL), las Autodefensas Gaitanistas de Colombia (AGC) y las antiguas estructuras del Bloque Oriental de las FARC-EP** que no se acogieron al proceso de paz".

Según el informe sobre "Conflictos armados focalizados" 2017-2018, del Instituto de Estudios para el Desarrollo y la Paz (INDEPAZ), las acciones del ELN afectaron durante el primer semestre de 2018 a 136 municipios, las del EPL a 9, las de las AGC a 225, y las del Bloque Sur-Oriental a 20. (Le puede interesar: ["18 grupos narcoparamilitares operan en Colombia"](https://archivo.contagioradio.com/18-grupos-narcoparamilitares/))

El informe de INDEPAZ utiliza el término de "Grupos Posdesarme de las FARC", para agrupar a las disidencias, grupos Rearmados para Negocios Ilegales (RNI) y Grupos de Seguridad del Narcotráfico y Mafias (GSNM); de allí la diferencia en denominaciones respecto al **CICR, que por su parte, identifica únicamente al Bloque Oriental, en sus Frentes 1,7 y 40, como parte de un CANI**.

### **El Conflicto Armado No Internacional en el que no interviene el Estado** 

En la región del Catatumbo, ubicada en Norte de Santander y limítrofe con Venezuela, tiene la presencia de al menos 3 actores armados (ELN, EPL y Fuerza Pública), presenta realidades de desplazamiento (interno y externo desde Venezuela), situaciones que se suman a la presencia de cultivos de uso ilícito y desigualdad percibida por sus habitantes, en términos de acceso a oportunidades de salud, vivienda y educación.

Es en esa región que **el Comité de la Cruz Roja ha identificado un quinto CANI entre el ELN y el EPL, o los denominados Pelusos**. Estructuras que se enfrentan para lograr el control territorial de esta importante región, para lograr el dominio sobre los negocios ilegales que operan en la zona. (Le puede interesar:["Asesinan al líder social José Antonio Navas en Catatumbo"](https://archivo.contagioradio.com/asesinan-lider-social-jose-antonio-navas-catatumbo/))

Adicionalmente, la CICR menciona otras situaciones de violencia "que no alcanzan el umbral de un conflicto armado", pero en las cuales tiene competencia la acción humanitaria que realiza, entre ellas, destaca las **situaciones de violencia urbana que sufren Medellín, Buenaventura, Cali y Tumaco,** "así como el uso de la fuerza durante protestas sociales, como en el caso de paros agrarios o manifestaciones que se tornan violentas" en el país.

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
