Title: Fuerza Aérea habría bombardeado resguardo indígena en el Litoral San Juan, Chocó
Date: 2018-01-30 13:30
Category: DDHH, Nacional
Tags: Chocó, Defensoría del Pueblo
Slug: fuerza_aerea_bombardeo_comunidad_indigena
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/01/colombia-e1517336965550.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:  TN.com] 

###### [30 Ene 2018] 

Comunidades del Chocó denuncian que por un bombardeo de la Fuerza Pública colombiana, ubicado en el municipio del Litoral del San Juan, se presenta un número indeterminado de personas muertas y heridas, entre las que se encontraría una menor de 16 años que habría perdido un brazo debido a la explosión.

Así lo ha denunciado también la Defensoría del Pueblo en una entrevista a RCN Radio, en donde ha señalado que el hecho se habría dado en el marco de un operativo de la Fuerza Aérea contra una estructura criminal ubicada en esa zona donde vive la comunidad indígena. De acuerdo con Aida Quilcué, encargada de Derechos Humanos de la ONIC, la acción se dio dentro de dos resguardos indígenas, uno de ellos, **Chagpien Tordó.**

### **Habría sido un bombardeo contra el ELN** 

El bombardeo sucedió el 29 de enero y se trataría de una acción dirigida a combatir al ELN. Aunque no se ha logrado conocer mayores detalles del hecho, ni de las víctimas debido a que en la zona no hay internet ni señal móvil, Aida Quilcué, señala que desde la ONIC se ha emitido u[na acción urgente tras conocer el hecho, y este miércoles se desplaza una comisión para verificar la situación.]

De acuerdo con las autoridades indígenas del departamento, la acción fue cometida en pleno** resguardo, por lo que niños, niñas, mujeres y adultos mayores se encontrarían entre las víctimas. ** La menor de edad herida, sería integrante de la comunidad indígena, pero también sería integrante del ELN, aunque se trata de una información que no se ha confirmado.

### **La versión de las Fuerzas Armadas** 

Por su parte, la Fuerza Aérea, asegura que el bombardeo se realizó en un acto legítimo de las Fuerzas Militares buscando** atacar a miembros del Frente Che Guevara del ELN que se encontraba en el sector,** con el fin de capturar a  alias ‘Estacio’ o ‘Anda que anda’ y alias ‘Anderson Tierra’, cabecillas de comisión de este frente.

El comunicado dado a conocer este martes explica que "La operación militar fue planeada y ejecutada conforme a los procesos establecidos para tal fin, fundamentada en las normas del **Derecho Internacional de los Derechos Humanos y el Derecho Internacional Humanitario".**

Además, aseguran que capturaron a varios integrantes de la guerrilla. Entre ellos, a alias ‘Duver’ y alias ‘Natalia’, quienes se encuentra heridos y reciben atención médica. Asimismo, la aprehensión de dos sujetos a quienes se les está verificando su identidad.

Ante la gravedad de los hechos, se adelantará un consejo de seguridad departamental para esclarecer lo que ocurrido. Por su parte, la** Defensoría del Pueblo exigió que el Ministerio de Defensa aclare lo sucedido** y las organizaciones defensoras de Derechos Humanos y la comunidad piden de manera urgente que cesen los actos que afectan las comunidades, la verificación y acompañamiento humanitario de las comunidades.

Cabe recordar que durante el 2017, esa comunidad fue víctima, en repetidas ocasiones de desplazamientos forzados en febrero debido a las acciones armadas y de reclutamiento de menores de edad que se presentaron en la zona, tanto por parte de la guerrilla del ELN, como las fuerzas militares.

En desarrollo...
