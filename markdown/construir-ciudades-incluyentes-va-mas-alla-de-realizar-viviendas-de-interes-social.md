Title: Construír ciudades incluyentes va más alla de hacer viviendas de interés social
Date: 2015-11-11 13:44
Category: Hablemos alguito
Tags: Carlos Torres Universidad nacional, Carlos Torres y Julio Abel Sánchez Univesidad nacional, Ciudades incluyentes, Plaza de la hoja, Programa de gobierno Bogotá humana, Vivienda de interes social en Bogotá
Slug: construir-ciudades-incluyentes-va-mas-alla-de-realizar-viviendas-de-interes-social
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/11/Vivienda-P-549x366-e1447267477601.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### Foto: Lina Rozo 

<iframe src="http://www.ivoox.com/player_ek_9353966_2_1.html?data=mpiilZ6aeo6ZmKiakpaJd6Kml4qgo5macYarpJKfj4qbh46kjoqkpZKUcYarpJKw0dPXuNPphqigh6aotozXytrRw8nJt4zdz8jZ197JstXZ1JDjw5DRaaSnhqae1ZDFsM3VjMnSjdfJpc3d28aah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [11 nov 2015]

"Plaza La Hoja" se ha convertido en el proyecto insigne del Plan de Desarrollo “Bogotá Humana”. Ubicado en el centro ampliado de la ciudad de Bogotá, este conjunto alberga cerca de 450 apartamentos para un número igual de familias que desde principios de este año reiniciaron sus vidas en este lugar. La mayoría de propietarios son mujeres cabeza de hogar.

Una ciudad que lucha contra la segregación y la discriminación –eje N°1 del plan de desarrollo-, fue el espíritu que animo la concreción de este proyecto que aún está en fase de adecuación, entendiendo que la vivienda no es sólo cuestión de cemento.

Vivir en un sector u otro de la ciudad, desde la perspectiva de la antisegregación y la antidiscriminación, implica el encuentro de sectores poblacionales hasta ahora alejados por fronteras socioeconómicas que parecen insalvables. Los pobres al sur, los ricos al norte dicta el “sentido común” de los bogotanos, pero ese “sentido común” inducido por los poderes económicos que construyen la ciudad, empieza a encontrar sus límites ecológicos y sociales. La Ciudad no puede seguir expandiéndose, necesitamos redensificar y ello supone el encuentro entre diferentes, la mezcla social.

"Plaza La Hoja", con todos los cuestionamientos que haya podido tener, es y será para la posteridad el único proyecto que en Bogotá, Colombia y Latinoamérica le ha apostado a la inclusión de una población que siendo víctima del conflicto social y armado que vive el país, se ha visto abocada a vivir en la ciudad. Una ciudad en la que han sentido el triple rigor de ser víctimas de desplazamiento forzado, muchos de ellos pobres y hasta hace pocos años, pobladores de las zonas rurales colombianas.

Contagio Radio adelantó una conversación con un residente de la Plaza La Hoja y los profesores de la Universidad Nacional-Facultad de Artes, Carlos Torres y Julio Abel Sánchez. Un encuentro en el que se introducen algunas reflexiones sobre los múltiples significados de adquiere la construcción de ciudad al día de hoy. ¿Es democrática y pública, es privatista? ¿Es sólo cuestión de casas, calles e infraestructura? ¿responde la ciudad a las necesidades de esta población o por el contrario la revictimiza? El escenario por ahora está salpicado de muchos claros y oscuros, porque la vivienda en Colombia por otro lado se ha convertido en instrumento de la politiquería.

Producción: Pilar Suárez y Contagio Radio
