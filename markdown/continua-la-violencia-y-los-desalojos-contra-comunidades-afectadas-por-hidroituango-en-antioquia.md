Title: Continúa la violencia y los desalojos contra comunidades afectadas por Hidroituango en Antioquia
Date: 2018-04-13 15:12
Category: Ambiente, DDHH, Nacional
Tags: Antioquia, desalojo de comunidades, EPM, Hidroituango, Policía Nacional, Sabanalarga
Slug: continua-la-violencia-y-los-desalojos-contra-comunidades-afectadas-por-hidroituango-en-antioquia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/02/DV1R3HZXkAA8Y8A-e1523649866886.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Movimiento Ríos Vivos] 

###### [13 Abr 2018] 

El Movimiento Ríos Vivos indicó que, en medio de **un proceso de desalojo** por parte de la Policía Nacional en Sabanalarga, Antioquia, los afectados por el proyecto hidroeléctrico Hidroituango fueron atacados con balas de goma y gases lacrimógenos a la vez que fueron señalados de "guerrilleros". Manifestaron que 6 personas resultaron heridas y no hubo presencia de las autoridades.

Las comunidades afectadas se encontraban realizando una **protesta pacífica** en la playa el Arenal durante las horas de la mañana del 12 de abril. Una vez allí, fueron atacados por los integrantes de la Policía Nacional quienes “desaseguraron sus fusiles”. Argumentó el Movimiento de defensa del territorio que “es reprochable que en una acción de desalojo hayan armas de fuego y se amenace a la población víctima del conflicto armado y del proyecto Hidroituango”.

### **Desalojos de comunidades son recurrentes** 

En repetidas ocasiones, Ríos Vivos ha denunciado los procesos de desalojo que se han hecho **“de forma violenta”** contra las comunidades que se encuentran en los territorios afectados por el proyecto de la hidroeléctrica. Desde que el proyecto empezó a andar a mediados de 2009, más de 500 familias han sido desplazadas y cientos de hectáreas de bosque seco tropical han sido taladas para poder realizar la hisroeléctrica por parte de la empresa EPM.

En esta ocasión, el Movimiento aseguró que “se les pidió diálogo con el comandante del operativo para conocer la orden de desalojo, **pero nadie dio respuesta a la comunidad**”. Afirmaron que “mientras se mantiene el cerco policial y militar sobre la comunidad”, se han registrado 6 personas heridas y al lugar no hizo presencia la Personería, la Procuraduría o la Defensoría del Pueblo. (Le puede interesar:["Memoria y resistencia en el Cañón del Río Cauca"](https://archivo.contagioradio.com/memoria-y-resistencia-en-el-canon-del-rio-cauca/))

### **Agresión se presentó en medio de los incumplimientos de las autoridades** 

Ríos Vivos argumentó que la agresión se presentó en el marco del incumplimiento de los acuerdos de la mesa de garantías de Antioquia pues **esperaban la visita de la Fiscalía**, la Procuraduría y a la Gobernación de Antioquia que nunca ocurrió. Afirmaron que estaba previsto un recorrido por el Cañón del Río Cauca para exponer las afectaciones al territorio como la situación de los desaparecidos.

Adicionalmente, las comunidades estaban esperando realizar “un diálogo de alto nivel con el gobernador Luis Pérez y gerente de EPM, que el mismo gobernador propuso en marzo de 2016 al Movimiento Ríos Vivos”. Alertaron **sobre la grave situación** en la que están las comunidades afectadas y solicitaron a las autoridades que “cese la represión y se brinden garantías para la protesta”.

Finalmente recordaron que a las víctimas del conflicto armado que han estado presente en las movilizaciones contra Hidroituango **“no se les está respetando sus derechos** y tampoco hay garantías de no repetición”. Por esto pidieron una intervención del Gobierno Nacional para que paren los desplazamientos y la revictimización.

\

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 

<div class="osd-sms-wrapper" style="text-align: justify;">

</div>
