Title: 6.219 mujeres actualmente en peligro de ser asesinadas por sus parejas
Date: 2015-11-30 15:31
Category: Mujer, Nacional
Tags: Casa de la Mujer, Corporación Casa de La Mujer, dia de la no violencia contra la mujer, Violencia contra las mujeres, violencias contra las mujeres en Colombia
Slug: 6-219-mujeres-actualmente-en-peligro-de-ser-asesinadas-por-sus-parejas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/11/Día-NO-Violencia.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Casa de La Mujer 

   
<iframe src="http://www.ivoox.com/player_ek_9554572_2_1.html?data=mpqilpqbdo6ZmKiak5uJd6KllZKSmaiRdo6ZmKiakpKJe6ShkZKSmaiReo%2Bmkp6Yz9rOqdPZ1JDOxdnZpc3hxtPhx5DJsozkxtHWydfTb8XZjNjS1JDFt8bnytPOxsbXb46ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [25 Nov 2015 ] {#nov-2015 .ecxMsoNormal}

[La Casa de La Mujer lidera una campaña principalmente en redes sociales, como parte de una estrategia política y comunicativa que busca **presionar al Gobierno nacional para que declare en crisis humanitaria la actual situación de las mujeres en Colombia**, con base en el reconocimiento de que el aumento exacerbado del registro de casos de violencia contra la mujer es muestra de la degradación a que ésta ha llegado y de que es el **Estado quien debe brindar las principales garantías de protección y soluciones efectivas**.]

[Ana María Salamanca psicóloga de la Casa de La Mujer, asegura que esta organización lleva desde 1982 “trabajando por un mundo a la medida de las mujeres” pues al analizar su **cotidianidad se hacen evidentes múltiples formas de violencia** en distintos espacios como la escuela, la casa o el trabajo, “discriminaciones en razón a las diferencias entre hombres y mujeres que han sido construidas socialmente” y que se manifiestan en el diario vivir “en **relaciones de opresión y subordinación**, que dificultan el reconocimiento de las mujeres como ciudadanas plenas y como sujetos de derechos”.]

[No sólo en Colombia las mujeres sufren discriminaciones específicas a la hora de acceder a derechos fundamentales, a espacios laborales, o a derechos en el marco del conflicto armado, afirma Salamanca, agregando que esta problemática hace parte de la **configuración estructural de nuestra sociedad**, que lleva a cifras alarmantes de violencia contra las mujeres en Colombia, por lo que se deben encaminar acciones que exhorten al Estado acerca de sus **responsabilidad en las garantías para que las mujeres vivan una vida libre de todo tipo de violencias**.]

[En la recolección de las **6.219** firmas propuestas como meta para la campaña, correspondientes, de acuerdo con la Defensoría del Pueblo, con el número de **mujeres que actualmente se encuentran en peligro de ser asesinadas por sus parejas**, se han vinculado diversas organizaciones del territorio nacional con las que La Casa trabaja, así como lideresas políticas y artistas con el fin de llegar a toda la población.  ]

 
