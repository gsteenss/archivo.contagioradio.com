Title: Proyecto de ley 133 pretende legalizar Baldíos para empresarios y no para campesinos
Date: 2014-12-26 16:00
Author: CtgAdm
Category: Otra Mirada
Slug: proyecto-de-ley-133-pretende-legalizar-baldios-para-empresarios-y-no-para-campesinos
Status: published

Otra Mirada ha estado hoy en la rueda de prensa convocada por distintas organizaciones sociales y ONG’S como Oxfam, Planeta Paz, Codhes, Cinep, Mesa de Incidencia Política de Mujeres Rurales Colombianas, Comisión Colombiana de Juristas, Dignidad Agropecuaria y la Cumbre Agraria, Campesina, Étnica y Popular; para analizar los **riesgos y alcances del proyecto de ley 133 , o como han calificado las organizaciones sociales y congresistas de izquierdas “Ley Urrutia-Lizarralde**” en referencia a los beneficiarios de la misma.  
Para entender bien lo que supondría su aplicación y las consecuencias sobre los pequeños productores y las comunidades indígenas hemos entrevistado a la presidenta de Oxfam Colombia Aída Pesquera y a Oscar Gutiérrez de Dignidad agropecuaria .Ellos nos explican como esta ley sería un paso más en la concentración de tierras en el propio país, de cómo sería un detrimento en la democratización del reparto de tierras, creando una situación en la que los terrenos baldíos que deberían pasar al Fondo de tierras estatal pasarían a manos de empresarios y de multinacionales que las gestionarían obligando a los campesinos a supeditarse a estos mismos.
