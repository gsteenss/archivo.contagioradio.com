Title: Ping pong, cuando la violencia de género se camufla en lo cotidiano
Date: 2018-07-13 12:51
Category: eventos
Tags: teatro, violencia género
Slug: ping-pong-teatro-violencia-genero
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/07/IMG_2954.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Caso Teatro 

###### 13 Jul 2018 

¿Cómo saber cuando el amor se convierte en una forma de maltrato? es la reflexión de la que parte **"Ping Pong", ópera prima de la directora Andrea Parra y su compañía Caso Teatro**, que utilizando técnicas como el teatro foro y el teatro de los oprimidos, busca abordar temas de interés social, de manera entretenida y con el plus de permitir la participación activa de los espectadores.

Además de ser actriz y dramaturga, Parra es psicóloga y fue durante sus estudios de postgrado que encontró en esas técnicas teatrales, herramientas idóneas para representar la violencia de género, que en su experiencia de trabajo en entidades como la secretaria de la mujer, le permitió identificar que **en la cotidianidad suelen naturalizarse ciertos comportamientos y actitudes que la ocasionan, sin limitarse necesariamente a las agresiones físicas**.

Catalina (Daniela Botero) y su novio José (Juan Esteban Quintero), **encarnan el ideal de pareja joven, que en el trascurso de la obra se va transformando en un pequeño infierno**, donde los detalles de amor se convierten en control, los reclamos terminan en discusiones y el termómetro de violencia va subiendo.

¿Para qué quieres trabajar?, Yo sería incapaz de tocarte un pelo, ¿Qué haces cuando estás sola o cuando hablas por Facebook o WhatsApp, ¿A qué hora vine a meterme con una vieja como tú ?, ¡Mira cómo te vistes! Todo lo que hago es para que estés bien, porque me preocupo por ti, porque te quiero, son frases que hacen parte del día a día de esta pareja y de muchas más, que no son capaces de reconocer la violencia en situaciones cotidianas.

Por tratarse de Teatro Foro, los espectadores se encontrarán en un primer momento con la directora, quien asumiendo el rol de moderadora por su conocimiento experto frente al tema, media entre el público y los actores, advirtiendo de antemano a los asistentes sobre la posibilidad que tienen para **intervenir en el escenario y plantear soluciones a las situaciones que se plantean en la obra** una vez esta concluya. De ahí que la directora asegura que en su obra "el espectador es más importante que los actores".

Adicionalmente, durante la puesta en escena de Ping-pong, **el público podrá disfrutar de la música compuesta por la actriz principal**, quien acompañada por el ukelele interpreta las canciones que hacen parte del montaje en voz de su personaje. Una muestra del talento y versatilidad de los jóvenes actores egresados de la Casa del Teatro Nacional.

"Van a sentirse muy identificados tanto hombres como mujeres con las situaciones que se plantean, porque **están escritas desde historias reales y todo lo que van a ver en escena es a parir de vivencias de personas que conocemos**, y los va ayudar a ver diferente la violencia de género, que vemos en el día a día pero no somos conscientes" afirma la directora.

La obra se presenta en temporada de estreno del **13 al 28 de julio en Teatro Hombre Mono** ubicado en la Cra. 25 \#39-74 (Parkway, Bogotá), con funciones los viernes y sábados a las 8:00 p.m. Bono de APOYO: \$ 30.000. Aforo 100 personas.
