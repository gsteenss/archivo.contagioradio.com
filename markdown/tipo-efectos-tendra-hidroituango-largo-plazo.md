Title: ¿Qué tipo de efectos tendrá Hidroituango a largo plazo?
Date: 2019-02-11 18:31
Author: AdminContagio
Category: Ambiente, Nacional
Tags: defensa del ambiente, Empresas Públicas de Medellín, Hidroituango, Movimiento Ríos Vivos
Slug: tipo-efectos-tendra-hidroituango-largo-plazo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/02/rios-vivos-770x400.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Svenswikipedia] 

###### [11 Feb 2019] 

El viernes pasado, Empresas Públicas de Medellín  (EPM) abrió tres compuertas de Hidroituango para ponerle fin a la emergencia ambiental que produjo la última operación del vertedero de la represa. Mientras que el río regresa a un caudal adecuado, se ha generado un debate entre las comunidades, los oficiales de EPM y algunos académicos sobre los efectos ambientales que se darán a largo plazo a causa de las sequías y la perdida del río.

Al respeto, el gerente de EPM, Jorge Londoño, afirmó que los daños ambientales son reversibles, declaraciones que contrastan con las voces de Movimiento Ríos Vivos que calificaron los hechos como actos de "ecocidio." En este debate, también se manifestaron expertos como Gabriel Pinilla Agudela, especialista en Biología de aguas y profesor de la Universidad Nacional, que sostuvo el río podrá regresar a la normalidad, pero con ciertas condiciones.

Dado que la perdida del río Cauca no duró más de una semana, es muy probable que el afluente se recupere, sin embargo no volverá a las mismas condiciones de antes. El Biólogo recordó que el impacto causado a la población de peces, invertebrados y reptiles por la gran cantidades de muertes en estas especies es irreversible.

[Actualmente van más de 57 mil peces muertos y podrían morir aun más cuando comience a fluir de nuevo el río. Como lo explicó José Iván Mojica, biólogo y profesor de la Universidad Nacional, al regresar el caudal los peces pueden quedar atrapados nuevamente en "sopas de barro" donde seguramente morirán. (Le puede interesar: Sequía y mortandad de peces alcanzan niveles críticos por operación de Hidroituango")]

### **"Represas tienen unos efectos bastante dañinos": Gabriel Pinilla ** 

El desastre ambiental ocasionado por la compuerta uno de Hidroituango llamó otra vez la atención del país  a los daños que puede ocasionar este megaproyecto. Sin embargo, estas últimas afectaciones solo se sumen a una lista de actos por parte de EPM que han resultado con el deterioro de ecosistemas acuáticos y aledaños al río.

Tal como lo había denunciado la organización Movimiento Ríos Vivos - Antioquia, la masa orgánica que resultó de la tala de árboles en preparación para la construcción de la represa terminó en el embalse, donde ahí se degrade el material lentamente. Esta organización ha reiterado en varias instancias que la agua estancada en el embalse está tan contaminada que los peces huyen de ahí.

Pinilla sostuvo que la cantidad de biomasa producida a talar más de 4.000 árboles en la área implica un consumo del oxígeno y la generación de gases tóxicos como métano y ácido hídrico en el afluente, que en el momento que la represa comienza a funcionar, fluirán a aguas abajas. Estas condiciones del agua "[son nada apropiadas para el desarrollo de los peces y otros organismos de aguas de fondo, especialmente del embalse," explicó el Biologo.]

Esto se suma a la erosión que podría ocasionar las aguas que filtra la represa. Según Pinilla, "el embalse tiene un efecto de retención de sedimentos" pues los minerales se hunden cuando el agua pierde su velocidad. Esto ocasiona más problemas para el río porque al correr nuevamente en aguas abajas, estas aguas "tienen más capacidad de arrastrar sedimentos" y pueden causar erosión del cauce.

[Adicionalmente, algunos expertos como Modesto Portilla, Doctor en Ingeniería del Terreno y profesor de la Universidad Nacional, advierten del desastre socioambiental que crearía el macizo rocoso al fallar. Al derrumbarse el macizo rocoso donde se sostiene la represa, se abriría un canal de agua por donde fluiría un "caudal inmenso" que podría llegar a velocidades de 263.000 metros cúbicos por segundos y a una altura de 100 metros.]

[Esta pared de agua podría llegar hasta el río Magdalena y generaría inundaciones de agua, sedimentos y lodo por los municipios aledaños al río, como Puerto Valdivia, Valdivia, El Doce, El Quince, Puerto Antoquia, Cáceres, Tarazá, Caucasia y Nechí. ]

Las afectaciones son alarmantes, sin embargo nada nueves [pues Pinilla aseguró que se suelen dar en otras represas. Sin embargo, los constructores, como en este caso, muchas veces no tienen en cuenta las medidas preventivas que pueden tomar para mitigar los "efectos bastante dañinos" que suelen ocasionar este tipo de hidroeléctricas.]

Frente estos hechos, Movimiento Ríos Vivos - Antioquia manifestó la semana pasada que "la única manera de proteger realmente la vida en el cañón del río Cauca, el Bajo Cauca y de salvar el río es que desmantelen Hidroituango." Hasta ahora, no hay ningunas indicaciones que EPM tenga planes de suspender el proyecto y por lo tanto, los expertos piden que EPM tome en serio las medidas para prevenir más daños ambientales al río, los animales y las comunidades que dependen en él para la supervivencia.

<iframe id="audio_32448654" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_32448654_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
