Title: Usted puede pasar a la historia como el general de la paz: Víctimas a Rito Alejo del Río
Date: 2020-12-01 19:05
Author: AdminContagio
Category: Actualidad, Comunidad
Tags: cacarica, comunidades, Operacion genesis, rito alejo del rio
Slug: general-rito-alejo-del-rio-general-de-la-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/12/General-Rito-Alejo-del-Rio-Cacarica.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right"} -->

[Foto: GS Noticias]{.has-inline-color .has-cyan-bluish-gray-color}

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En una comunicación más de 110 comunidades, organizaciones y procesos comunitarios invitan al general retirado Rito Alejo del Río a convertirse en el general de la paz dado que **en los próximos días rendirá versión ante la jurisdicción especial de Paz**. Según la carta el deseo de las víctimas es “animarlo asumir ese paso de la verdad en la JEP. Este es un momento muy importante para usted y para miles de víctimas, y el futuro de un país distinto”

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Las comunidades y organizaciones en un llamado sentido le recordaron al general Rito Alejo del Río que actuó convencido del amor a la patria, a la democracia, pero resaltan que “esos eran tiempos de otra guerra” [Vea la carta completa aquí.](https://www.justiciaypazcolombia.com/usted-puede-ser-el-general-de-la-paz-victimas-de-rito-alejo-del-rio/)

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### “Hoy estamos en tiempos donde la guerra puede seguir o la podemos parar si nos encontramos en la verdad”

<!-- /wp:heading -->

<!-- wp:paragraph -->

Las comunidades también aseguran que esperan que el aporte del general a la JEP sea para el bienestar de su familia, pero también del país y reafirman que del Río podría contar con ellos y con sus territorios sin violencia para lograr una verdadera paz. Lea también [General (r) Rito Alejo del Río comparecerá ante la JEP en diciembre – Contagio Radio](https://archivo.contagioradio.com/general-r-rito-alejo-del-rio-comparecera-ante-la-jep-en-noviembre/)

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Según ellos y ellas, si el general decide guardar silencio seguirán pidiendo a “Dios y al universo” para que un día allá verdad plena. “En honor de su amor a la patria y en este tiempo que Dios y la historia nos concede para las verdades, esperamos que su corazón hable de esas verdades para ser el general de la paz”

<!-- /wp:paragraph -->

<!-- wp:quote -->

> “En honor de su amor a la patria y en este tiempo que Dios y la historia nos concede para las verdades, esperamos que su corazón hable de esas verdades para ser el general de la paz”
>
> <cite>Carta de comunidades a General Rito Alejo del Río</cite>

<!-- /wp:quote -->

<!-- wp:paragraph -->

Estas mismas comunidades han enviado una serie de [18 comunicaciones](https://www.justiciaypazcolombia.com/cartabierta-18-seguimos-creciendo-en-el-acuerdo-humanitario-global/) al presidente Duque, a las guerrillas y a los grupos armados en las que claman por un acuerdo humanitario que alivie los efectos nocivos que ha tenido la pandemia en sus territorios y que siente un precedente para la posibilidad de conversaciones de paz que acaben con las múltiples violencias que sufren.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Rito Alejo del Río y sus verdades

<!-- /wp:heading -->

<!-- wp:paragraph -->

Desde el proceso judicial en el que el general Rito Alejo del Río fue condenado por la tortura y el asesinato de Marino López afrodescendiente de las comunidades del Cacarica, en el marco de la Operación [Génesis](https://archivo.contagioradio.com/en-medio-del-asedio-paramilitar-victimas-conmemoran-somosgenesis/), las personas de estas comunidades han clamado porque se sepa la verdad de estos hechos y se tomen las medida necesarias para evitar la repetición y se brinden las garantías de una reparación colectiva para ellas y ellos **la comparecencia de Rito Alejo del Río ante la JEP es una oportunidad para que se atiendan esas peticiones.    **

<!-- /wp:paragraph -->
