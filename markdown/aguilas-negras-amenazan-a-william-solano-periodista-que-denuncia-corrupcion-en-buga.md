Title: Águilas Negras amenazan a William Solano, periodista que denuncia corrupción en Buga
Date: 2019-02-05 17:43
Author: AdminContagio
Category: DDHH, Entrevistas
Tags: Amenazas a periodistas, Buga, Libertad de Prensa, Valle del Cauca
Slug: aguilas-negras-amenazan-a-william-solano-periodista-que-denuncia-corrupcion-en-buga
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/02/10444668_898617840176342_2306331343939005187_n-770x400.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Facebook 

###### 5 Feb 2019

[Después de 18 años de ejercer el periodismo en el municipio de Buga, Valle del Cauca, es la segunda vez que el periodista independiente William Vianney Solano ha recibido amenazas contra su vida, la más reciente ocurrió el pasado 1 de febrero cuando fue intimidado a través de un sufragio adjudicado a las autodenominadas Águilas Negras.]

El periodista explica que  su padre recibió un paquete que era para él y en **"el interior venía un sufragio con un fuerte olor a sangre y con unas letras recortadas en papel periódico que decía que había sido declarado objetivo militar de las Águilas Negras”** agrega  Vianney Solano que las amenazas en su contra nacen a raíz de su trabajo de investigación y de denuncia contra “los actos de corrupción, carteles de contratación, y el mal manejo de los recursos” al interior de la Alcaldía de Buga.

Solano, quien han recibido llamadas amenazantes afirma que está sitiado en un municipio que parece “ser el negocio en particular de esta persona que se ha incrustado en el poder,  que funge como si fuera un pequeño jefe de estado” pero que lo más le preocupa de la situación es el silencio absoluto de las autoridades locales y de los entes de control.

[A su vez indica que su caso ha sido asignado a la Fiscalía 32, encabezada por el fiscal Álvaro Duque, quien es esposo de la juez Escalante y a la vez son padre de un contratista del gobierno municipal quien es precisamente una de las personas a las que su medio de comunicación ha hecho seguimiento y ha investigado.]

### **"Hacer denuncias en provincias como Guadalajara de Buga tiene un mérito muy grande"** 

Respecto a su seguridad, el periodista explica que hace un par de años le fue asignado una unidad de protección y que a pesar de ello él continúa movilizándose a pie, lo que ha valido para que haya sido interceptado por hombres armados en al menos dos ocasiones en las que su escolta ha actuado con rapidez,  “en los últimos días me he tenido que encerrar, pienso mucho antes de salir” admite.

[Aunque no ha sido contactado por algún ente del Gobierno, Vianney Solano informó que la Secretaría de Seguridad Ciudadana del Valle del Cauca le ha pedido llenar un formulario para la Unidad Nacional de Protección y poder reforzar su esquema de protección, mientras las copias son compulsadas, el periodista afirma que continuará denunciando pues “permanecer en silencio es aceptar lo que está sucediendo”.]

<iframe id="audio_32297006" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_32297006_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]

 
