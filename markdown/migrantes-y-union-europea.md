Title: "Migrantes" y Unión Europea
Date: 2015-09-16 06:00
Category: Cesar, Opinion
Tags: César Torres Del Río, Emigrantes, Europa, francia, Hungria, La troika, Unión europea, Yanus Varufakis
Slug: migrantes-y-union-europea
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/inmigrantes-cayucos.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Ilka Oliva] 

#### [**[Cesar Torres del Río](https://archivo.contagioradio.com/cesar-torres-del-rio/) - [~~@~~logicoanalista2](https://twitter.com/logicoanalista2) 

###### [16 de Sep 2015] 

La Unión  Europea ha excluido de su léxico político-diplomático la palabra “refugiado”; es más, ha prohibido que se la utilice para referirse a los que llegan de África o de Asia. El argumento es que “migrante” tiene una connotación “neutral”, en medio de la guerra claro … La categoría de “refugiado” conlleva a reconfigurar el derecho de asilo, y esto para la U.E. implicaría rediseñar la “zona schengen” y modificar la concepción misma de “unidad”, tal y como lo han manifestado hace pocos días dirigentes políticos y ministros. La batalla de los que huyen de la guerra siendo perseguidos por razones políticas, sociales y étnicas también se está librando en el terreno del lenguaje.

[La  “zona schengen”, comprende una veintena de países con diferencias notables en sus economías, articulaciones políticas y tradiciones. Debemos entenderla tal y como se nos presenta en la actualidad: Estados capitalistas europeos “democráticos” (?), algunos con despreciables monarquías (España, Bélgica, Países Bajos, Inglaterra), con una modalidad particular de asociación imperialista (U.E.), cuyo epicentro es Alemania, y militar (OTAN), cuya sede se encuentra en Bélgica. Tales Estados - por su misma naturaleza capitalista -  no han eliminado la posibilidad de guerra entre ellos (aunque sus intelectuales, profesores, ministros y políticos estén convencidos de lo contrario); sus burguesías continúan siendo depredadoras y acumuladoras de capital, incluso por desposesión; sus formas de existencia estatal construyen distintos canales para garantizar la apropiación del capital por parte de las multinacionales; sus mecanismos incluyen planes de austeridad para imponer a los Estados débiles (Grecia 2015); sus “democracias” levantan muros infames (dentro o fuera de Europa) contra los refugiados y fomentan el racismo y la xenofobia (Hungria, Francia) o se constituyen en “filtros” para contener a los que huyen de la guerra y de sus tragedias internas (Italia).]

[Pero los intelectuales del “viejo mundo” no quieren entender éste ABC de la política, la economía y la guerra social. Hace pocos días lanzaron públicamente un  “Plan B” para Europa; entre quienes firman está Yanus Varufakis, el ex ministro griego de hacienda del gobierno Tsipras. Establecen en la dirigencia europea un sector “reaccionario y antidemocrático” (la Troika) lo que por deducción obliga a pensar en sectores liberales y democráticos en quienes podrían apoyarse las mayorías para luchar contra la austeridad; defendiendo a la democracia  liberal y a la Europa de sus amores afirman con ingenuidad y dulzura angelical  que “Si el euro no puede ser democratizado, si persisten en utilizarlo para estrangular a los pueblos, nos levantaremos, les miraremos y les diremos: ]*["¡Inténtenlo! Sus amenazas no nos dan miedo. Encontraremos un medio de dar a los europeos un sistema monetario que funcione con ellos, y no a sus expensas" (Por un Plan B en Europa,]*[*[www.vientosur.info]*](http://www.vientosur.info)*[).]*[ Terminan con una proclamación de gran solidaridad humana: “Nuestra visión es internacionalista” …]

[De modo que lo que los refugiados enfrentan inmediatamente a su llegada a las costas es una Europa en crisis, decadente e infame. Nada para ofrecer tienen los ministrillos, a no ser escapes por la vía individual. Con todo, las manifestaciones de solidaridad en Alemania y Portugal nos reconfortan, las apoyamos y las tomamos como ejemplo para futuras y presentes formas de resistencia frente a la ignominia.]

[Europa ha generado la guerra y creado las condiciones internas para la miseria, la explotación y la esclavitud; de los criminales como Bashar Al Assad y los regímenes árabes, de los bombardeos a la nación kurda, de las pandillas mafiosas norteafricanas, de los cómplices y amigos de Europa que fueron armados durante décadas en Asia y África, de tal trágica realidad huyen los]*[Aylan Kurdi]*[, refugiados de nacimiento pero “migrantes” por imposición de la Unión Europea.]
