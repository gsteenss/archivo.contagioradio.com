Title: Se quemó Guaidó
Date: 2019-05-02 12:45
Author: JOHAN MENDOZA TORRES
Category: Johan Mendoza, Opinion
Tags: soberanía, Venezuela
Slug: guaido-se-quemo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/D5kHMpUWkAAi0Vu.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

Hablar de Venezuela en la orilla del cuestionamiento a esa “bondadosísima” comunidad internacional, entidad del bajo astral que tiene rostro de Almagro, cuerpo de Trump y espíritu liberal-vasallo de Vargas Llosa, se ha convertido casi en un pecado. Cuestionar una supuesta comunidad internacional que con tal de bajar a un presidente que no favorezca sus negocios es capaz de celebrar un bloqueo económico, el robo de oro, dinero y hasta de atentar contra el sistema eléctrico de un país para demostrar su amor a la libertad, no es un acto crítico sino ético.

Esto no es de si “te gusta o no te gusta” el gobierno de… esto se trata de la inminencia de una nueva guerra en el continente americano. Para los que tienen la cabeza taaaaan abierta como un twitter quizá no estén conscientes de que, si están “aburridos” con los migrantes económicos, sabrán lo que es una tragedia cuando conozcan refugiados de una guerra.

El discurso anti injerencista es el que hay que rescatar, defender y argumentar el derecho a la soberanía, es una necesidad que debería ser charlada así sea en las íntimas comidas. El discurso anti injerencista no se agita en defensa de la figura personalizada de un gobierno, sino que significa negar esa sangrienta y antigua forma en que la política exterior estadounidense se ha manifestado contra los pueblos latinoamericanos que quieren surgir, con equivocaciones y aciertos ¡pero auténticos y soberanos!

¿Acaso todos los de esa izquierda caviar que hoy critican a Cuba y a Venezuela, creen realmente en la autodeterminación o están esperando que la ética del discurso sea una lección que algún día emerja de los gerentes de las compañías petroleras estadounidenses, del uribismo, de los terratenientes, de los paramilitares o de los narcotraficantes?

Hoy se ha vuelto literalmente un pecado salvaje defender la integridad (con sus errores y aciertos) de un país. Pero me curo en salud afirmando que lo que están haciendo con Venezuela supera por mucho el odio visceral contra Maduro; así no son las cosas, y contra la sonora, coordinada y hegemónica idea que transita día a día sobre Venezuela en todos los rincones del mundo, es necesario que se levante un manto de crítica anti hegemónico.

No será informándonos como lleguemos a comprenderlo, hace falta algo más mecánico que leer, necesitamos sentir, necesitamos pensar latinaomericanamente para darle fuerza a la crítica anti hegemónica.

¿Cómo?... una mariposa jamás tomará agua de un charco, ella necesita el lodo para saber desde qué tierra surge el agua que se está bebiendo.

Guaidó se quemó, nada que hacer, es un mamarracho. Y esto no es un agravio, es una literatura de nuestra realidad, atravesada por un hombre que no sabe hablar, que no se mete en el papel que le dieron, que no lee bien ni el libreto, que no encarna el performance de un hombre de Estado, sino que parece más bien eso: un mamarracho.

El reemplazo de un tipo como Guaidó, ya lo deben estar pensado los gerentes de todas las empresas petroleras que están impacientes por hacer negocios en Venezuela y que finalmente son las que están impulsando toda esta barbarie; yo creo que definirán a Miguel Bosé, pero un manto de duda me cubre, porque al no tener la ciudadanía venezolana podría autoproclamarse presidente, pero su acento lo delataría.

Guaidó se quemó, su última misión era sacar a López, tan decente y agresivo, tan burgués y comprometido, tan sonriente como lo eran los 43 muertos de las guarimbas. Esa era la última tarea de Guaidó, que bien ahora podrá regresar a Colombia a dar clase a una universidad, como lo hizo Carmona en el 2002.

Lo recordamos y no lo creemos, pero sí. Aquí la ficción no hay que buscarla, aquí el surrealismo se derrite ante nuestro sol caribeño de real ensamblaje de los hechos, aquí lo único que los estadounidenses tienen por opción es la guerra, ya no tienen más opción. Triste marco, en el que siempre pierda más Latinoamérica.  Toquemos madera, porque así dicen que se van las malas vibras.

------------------------------------------------------------------------

###### Las opiniones expresadas en este artículo son de exclusiva responsabilidad del autor y no necesariamente representan la opinión de Contagio Radio. 
