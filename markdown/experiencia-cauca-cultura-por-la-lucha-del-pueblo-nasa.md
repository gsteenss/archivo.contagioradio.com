Title: "Experiencia Cauca" cultura por la lucha del pueblo NASA
Date: 2015-08-15 09:00
Category: Cultura, Movilización
Tags: Con cultura apoyan comunidad NASA, Dj Kike el criollo, Experiencia Cauca, Maiti Roots Argentina, Reincidentes Bogotá, TEJIDO DE COMUNICACION
Slug: experiencia-cauca-cultura-por-la-lucha-del-pueblo-nasa
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/08/11825006_848883218481732_8307135109735707908_n-e1439494068299.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [15 Ago 2015] 

Este sábado 15 de agosto la cita es en La Redada (calle 17 No. 2-51) para compartir las distintas experiencias de la lucha del pueblo NASA en el Norte Del Cauca. El objetivo principal es recaudar víveres, herramientas y recursos para enviar como apoyo a la guardia indígena de este territorio. Habrá jornada de estampado, muestra fotográfica, proyecciones, conversatorio con autoridades indígenas y miembros de la guardia indígena, música en vivo y feria artística.

Esta es la programación de la jornada.

**4:00 a 7:00 pm:** Jornada de estampado (Set musical de Maiti Roots , Argentina)

**5:00 a 6:00 pm:** Proyección Audiovisuales:

- **TIERRAS TOMADAS** (Documental) // Amandine D´Elia

<iframe src="https://player.vimeo.com/video/130777824" width="500" height="281" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

- **NASA SIGNIFICA HUMANO** (Cortometraje) // Entrelazando

<iframe src="https://player.vimeo.com/video/136112042" width="500" height="281" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

- **SAAKHELU 2014** (Reportaje) // Dieciseis9

<object width="500" height="281" classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000"><param name="movie" value="http://www.nasaacin.org/components/com_allvideoshare/player.swf?random=1049781855"></param><param name="allowfullscreen" value="true"></param><param name="allowscriptaccess" value="always"></param><param name="flashvars" value="base=http://www.nasaacin.org/&amp;vid=97&amp;pid=1"></param><object type="application/x-shockwave-flash" data="http://www.nasaacin.org/components/com_allvideoshare/player.swf?random=1049781855" width="700" height="400"><param name="movie" value="http://www.nasaacin.org/components/com_allvideoshare/player.swf?random=1049781855"></param><param name="allowfullscreen" value="true"></param><param name="allowscriptaccess" value="always"></param><param name="flashvars" value="base=http://www.nasaacin.org/&amp;vid=97&amp;pid=1"></param><iframe title="YouTube video player" width="700" height="400" src="http://www.youtube.com/embed/vP-6QE4X2DA" frameborder="0" allowfullscreen="allowfullscreen"></iframe></object></object>

**6:00 a 7:00 pm:** Círculo de palabra problemáticas del Cauca: Liberación de la Madre Tierra,Guardia Indígena y su proceso de resistencia - Diálogo de la cosmovisión de los Nasa -procesos que se adelantan en Bogotá con el Cabildo Nasa y planteamiento sobre la justicia propia.

**8:00 a 11:00 pm**: Intervención musical:

**REINCIDENTES BTA**

<iframe src="https://www.youtube.com/embed/YcI7MX17Uhs" width="500" height="281" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

**DJ KIKE "EL CRIOLLO"**

<iframe src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/playlists/1180361&amp;auto_play=false&amp;hide_related=false&amp;show_comments=true&amp;show_user=true&amp;show_reposts=false&amp;visual=true" width="500" height="281" frameborder="no" scrolling="no"></iframe>

Vista: [QueQuieresHacerHoy](https://www.facebook.com/yoquierohacer?fref=ts) y encuentra más Eventos
