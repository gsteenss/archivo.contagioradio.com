Title: Inicia en Villavicencio el 3er Festival Internacional de Cine
Date: 2015-06-23 12:30
Author: AdminContagio
Category: 24 Cuadros
Tags: Bladimir Sánchez Espitia, Cinemateca Rodante del Meta, documental Operación Pacífic Rubiales, Ella película colombiana, Escuela de cine popular &amp; comunitario “Gabriel García Márquez”, Festival Internacional de Cine de Villavicencio - FICVI, Fundación Alejandro Viana, Libia Stella Goméz
Slug: 3er-ficvi-en-villavicencio-eres-cine
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/06/soyficvi.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

######  <iframe src="http://www.ivoox.com/player_ek_4677963_2_1.html?data=lZukmZ6ad46ZmKiak5WJd6Kml5KSmaiRdo6ZmKiakpKJe6ShkZKSmaiRhc3Zy8bbxtfTb7fdwtPOh5enb9Hm0MnixdnTtozYxtGYlcrWb6fZ1NnW2MbQb8XZjKjW0MqPqMafjoqkpZKns8%2FowszW0ZC2pcXd0JCah5yncZU%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Alejandro Viana, Productor General del FICVI] 

###### [23 jun 2015] 

Del **23 al 27 de junio** del 2015 llega en su tercera edición el **Festival Internacional de Cine de Villavicencio - FICVI** **"Eres Cine"**, el evento cinematográfico más importante de los llanos orientales de Colombia, una iniciativa de la **Fundación Alejandro Viana**, resultante de la convocatoria “IV Taller de Festivales de cine” organizado por Encuentros Cartagena, el Ministerio de Cultura y el Festival Internacional de Cine de Cartagena de Indias.

<iframe src="https://www.youtube.com/embed/8lCxaKKM07w" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

Más de **80 producciones** fìlmicas de 21 paises hacen parte de la selección audiovisual que tendrá en su apertura el martes 23 de junio, en el auditorio Mauricio Dieres Monplaisir de la biblioteca pública “Germán Arciniegas”, con la presentación de **"Ella"** segundo largometraje argumental de la directora santandereana **Libia Stella Gómez**, producción recientemente estrenada en la salas del país (Lea "[Ella, cuando la dignidad supera la indolencia](https://archivo.contagioradio.com/ella-cuando-la-dignidad-supera-a-la-indolencia/)).

Dentro de la programación del festival, el día 24 de Junio se adelantará el **conversatorio sobre problemáticas petroleras en el Meta** a la luz del documental **“Operación Pacific Rubiales”** del director **Bladimir Sánchez Espitia**, un relato donde las comunidades cuentan como son perseguidos, amenazados y despojados de sus tierras y territorios por parte de la empresa y como el estado garantiza su seguridad con el propio ejército colombiano.

<iframe src="https://www.youtube.com/embed/cHf1XTE2-bU" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

###### [Trailer de  “Operación Pacific Rubiales”] 

Cineastas nacionales de gran trayectoria entre los que se encuentran, **Bladimir Sánchez Espitia**, **Hugo Andres**, **Paola Chavarro**, **Ronald Escobar** y **Fabio Enrique Medellín Vargas** - Cineasta y catedrático, participarán en el evento, que tendrá como escenarios de proyección la Casa de la Cultura, y en barrios como Porfía, El Olímpico, Catumare y Playa Rica entre otras.

Para la edición 2015, la organización del Festival replanteó su estructura organizativa, nombrando al Cineasta Llanero, Jaime Ernesto Sandoval Nieves como su nuevo Director General, a la Cineasta y Artista Plástica Llanera, Sandra Monroy como Directora de Arte y al Comunicador Social, Periodista y Cineasta Bogotano, Alejandro Viana como Productor General.

Los organizadores de la **propuesta sociocultural** que representa el FICVI, son los mismos encargados de los proyectos: **Escuela de cine popular & comunitario “Gabriel García Márquez”** la cual cuenta con dos rodajes de cine en proceso y **Cinemateca Rodante del Meta**, una apuesta itinerante de cine social independiente que ya ha logrado copar varios espacios de la ciudad.

Los seguidores en redes sociales del Festival, podrán interactuar utilizando la etiqueta \#SoyFICVI

 Encuentre [AQUI](https://ficvifestival.wordpress.com/2015/06/20/programacionoficialficvi2015/) la progamación completa del Festival
