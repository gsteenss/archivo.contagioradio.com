Title: Ante la CIDH, comunidades indígenas expondrán fallas en acatamiento de medidas cautelares
Date: 2019-05-07 16:47
Author: CtgAdm
Category: Comunidad, Nacional
Tags: ACIN, CIDH, Comisión Interamericana de Derechos Humanos, CRIC
Slug: ante-la-cidh-comunidades-indigenas-expondran-fallas-en-acatamiento-de-medidas-cautelares
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/Indígenas-CRIC.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: CRIC] 

El próximo 9 de mayo, delegados de las comunidades indígenas del Chocó, Amazonas, Cauca y Putumayo se presentarán  en Kingston, Jamaica ante la **Comisión Interamericana de Derechos Humanos (CIDH)** para evaluar el estado de las medidas cautelares otorgadas sobre sus territorios y  el retroceso que han evidenciado en términos de derechos humanos.

**Edwin Capaz, coordinador de derechos humanos de la Asociación de Cabildos Indígenas del Norte del Cauca (ACIN),** expresa que junto a la Organización Nacional Indígena de Colombia (ONIC), evaluarán las medidas de protección que desde el 2011 cobijan a los territorios indígenas de Toribio, Tacueyó, San Francisco y Jambaló, además de las medidas del Consejo Regional Indígena del Cauca (CRIC) otorgadas por la Comisión a favor de 36 líderes indígenas.

Aunque en 2011 la CIDH había solicitado al Estado garantizar el bienestar de los pobladores de los resguardos, estas medidas no han sido suficientes para protegerlos debido al contexto de conflicto armado que se evidencia en el departamento del Cauca, razón por la que se debatirá cómo puede la institucionalidad brindar mayores garantías.  [(Lea también: Recuperar su territorio le está costando la vida a indígenas en Cauca)](https://archivo.contagioradio.com/nos-estan-asesinando-llamado-de-comunidades-indigenas-del-cauca/)

También se discutirán sobre los ataques que se han hecho contra la protesta social antes, durante y después de la Minga del suroccidente tales como señalamientos, estigmatización y lo que "ahora se ve traducido en atentados contra líderes defensores de derechos humanos como los ataques contra los líderes de las comunidades negras" manifestó el coordinador. [( Le puede interesar: 14 hechos de violencia contra los DD.HH. de la Minga en Cauca)](https://archivo.contagioradio.com/como-avanza-la-minga-en-la-defensa-de-dd-hh-de-los-pueblos-indigenas/)

**"La protección solo se le está dejando a una institución como la Unidad Nacional de Protección, y debe ir más allá, debe ser integral y verse desde el colectivo"**, afirma Capaz quien indica que esta será una de las perspectivas que reflejaran ante la CIDH y así el  Estado pueda aplicar estas medidas las comunidades indígenas.

<iframe id="audio_35506466" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_35506466_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
