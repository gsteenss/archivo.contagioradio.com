Title: Tercer capítulo Partículas de Libertad: Nuestras Cárceles
Date: 2020-09-21 17:38
Author: CtgAdm
Category: Expreso Libertad
Tags: #Montajesjudiciales, #Nuestrascárceles, #Partículasdelibertad, #Radionovela
Slug: tercer-capitulo-particulas-de-libertad-nuestras-carceles
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/09/Presentacion-de-Publicidad-Amarillo-y-Azul.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

Esta 15 de Septiembre, el **Expreso Libertad** lazó el tercer capítulo de su radionovela **«Partículas de Libertad»**  titulado:** **Nuestras Cárceles.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En este tercer capítulo los 5 jóvenes se enfrentarán la realidad con la comprobación de la pertenencia de Raúl a la Fuerza Pública y su verdadera identidad, también serán víctimas de la Fiscalía, institución que estaría detrás del montaje judicial que se teje en su contra.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Mientras tanto, cada uno de los personajes luchará por asumir su experiencia en la cárcel, sin hacerse a la idea de salir algún día de allí. Para escuchar los dos capítulos anteriores puedes ingresar a los siguientes links:

<!-- /wp:paragraph -->

<!-- wp:html -->  
<iframe id="audio_56875818" frameborder="0" allowfullscreen scrolling="no" height="200" style="border:1px solid #EEE; box-sizing:border-box; width:100%;" src="https://co.ivoox.com/es/player_ej_56875818_4_1.html?c1=ff6600"></iframe>  
<!-- /wp:html -->

<!-- wp:paragraph -->

[Partículas de libertad, una radionovela sobre los montajes judiciales en Colombia](https://archivo.contagioradio.com/particulas-de-libertad-una-radionovela-sobre-los-montajes-judiciales-en-colombia/)

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

[**Segundo capitulo Partículas de Libertad**: Las cadenas de la inocencia](https://archivo.contagioradio.com/segundo-capitulo-particulas-de-libertad/)

<!-- /wp:paragraph -->
