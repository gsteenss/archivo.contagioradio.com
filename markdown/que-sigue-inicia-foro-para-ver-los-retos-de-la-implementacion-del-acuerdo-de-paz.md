Title: "¿Qué sigue?" foro sobre los retos de la implementación del Acuerdo de Paz
Date: 2017-12-11 15:15
Category: Nacional, Paz
Tags: acuerdos de paz, implementación de los acuerdos, paz, Viva la ciudadanía
Slug: que-sigue-inicia-foro-para-ver-los-retos-de-la-implementacion-del-acuerdo-de-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/12/DQx3WrqU8AYg1vS-e1513017063991.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Viva la Ciudadanía] 

###### [11 Dic 2017] 

Teniendo en cuenta que se ha cumplido un año de la implementación de lo acordado entre el Gobierno Nacional y las FARC en la Habana, Viva la Ciudadanía, en conjunto con 76 organizaciones sociales realizarán un foro de dos días donde discutirán los retos que vienen para la implementación de los Acuerdos De Paz.  En 24 paneles, los participantes dialogarán teniendo en cuenta que la implementación **continúa su recorrido así se haya terminado el Fast Track** de la mano de múltiples iniciativas que han hecho un acompañamiento sostenido a los Acuerdos de Paz.

En el foro se discutirán temas como **el papel de los medios de comunicación en el posacuerdo**, el rol de la comunidad internacional en la implementación, temas asociados a la reincorporación de los ex combatientes, el acceso y el uso de la tierra, la implementación con enfoque de género, entre otros. Todos los paneles contarán con la asistencia de diferentes expertos que han estado realizando un seguimiento a la implementación desde los movimientos sociales y políticos.

Según Antonio Madariaga, director de Viva la Ciudadanía, “la imagen que se ha construido es que la implementación acaba con el Fast Track y resulta que no es así, el Acuerdo Final trae una gran cantidad de instrumentos de transformación que no dependen de la agenda normativa”. Manifestó además que, hay que reconocer que los procesos que se han desarrollado en los territorios, **“están siendo seguidos por muchísima gente** que está haciendo observación y veeduría de la implementación”.

Con base a estas experiencias, **Viva la Ciudadanía realizará un balance** que responda y de cuenta de los retos que se vienen en esta etapa del posacuerdo. Indicó que allí se presentará un análisis por parte de la CSIVI, el Instituto Kroc y habrá un conjunto de paneles donde se tocarán diferentes temas que hacen parte de los Acuerdos de Paz para poner prioridades que impulsen la materialización de lo acordado en este año que empieza. (Le puede interesar: ["La falta de implementación de los Acuerdos impide la paz territorial"](https://archivo.contagioradio.com/la-falta-de-implementacion-de-los-acuerdos-de-paz-impide-la-paz-territorial/))

### **Campaña electoral es una oportunidad para poner en la agenda pública la implementación** 

Madariaga enfatizó en que la campaña electoral que inicia en el país se debe aprovechar **para poner en la mesa y en el debate público** el tema de la implementación de los Acuerdos de Paz. Indicó que la movilización de las organizaciones sociales debe ser constante y así exigir “un parlamento comprometido con la paz”.

Así mismo, recalcó que la **participación política de los territorios** no puede limitarse a, por ejemplo, las 16 circunscripciones especiales de paz, en la medida que “pueden participar en las elecciones abiertas y lo más importante es que puede identificar los candidatos comprometidos con la implementación del Acuerdo de Paz”. (Le puede interesar: ["Implementación de los acuerdos no va por buen camino"](https://archivo.contagioradio.com/implementacion-de-los-acuerdos-no-va-por-buen-camino/))

Finalmente, Viva la Ciudadanía invitó al foro de dos días que se llevará a cabo entre el **12 y 13 de diciembre** en la Cámara de Comercio de Bogotá sobre la calle 26. Si quiere participar de los paneles, debe hacerlo a través de una inscripción en la página web www.viva.org.co.

![programacion 12 dic viva](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/12/programacion-12-dic-viva.jpeg){.alignnone .size-full .wp-image-49887 width="1280" height="1249"}

![Cronograma viva la ciudadanía](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/12/DQxsJm0U8AU0G-1.jpg){.alignnone .size-full .wp-image-49886 width="1200" height="1171"}

<iframe id="audio_22587652" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_22587652_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]

<div class="osd-sms-wrapper">

</div>
