Title: Cerca de 1500 líderes sociales podrían recuperar su libertad si se aprueba la amnistía
Date: 2016-12-21 12:33
Category: Entrevistas, Paz
Tags: implementación acuerdos de paz, Ley de Amnistia, lideres sociales, Presos políticos en Colombia
Slug: cerca-de-1500-lideres-sociales-podrian-recuperar-su-libertad-si-se-aprueba-la-amnistia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/12/CARCEL-WEB.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Barrancopolis] 

###### [21 Dic 2016] 

Por lo menos 300 líderes sociales privados de la libertad y acusados de delitos como rebelión o detenidos y procesados en el marco de la Ley de Seguridad Ciudadana podrían ser objeto de amnistía cuando esta sea aprobada.

La ley de Amnistía que continúa su trámite en el congreso por la vía del “Fast Track” o el mecanismo abreviado, que se aprobó para la implementación de los acuerdos de paz entre el gobierno y la guerrilla de las FARC. Según Diego Martínez, abogado asesor de la mesa de conversaciones, **los líderes sociales acusados de rebelión o detenidos en el marco de la ley de seguridad ciudadana podrán recuperar su libertad.**

Martínez, quien también es director del Comité Permanente por la Defensa de los Derechos Humanos, aseguró que el acuerdo es claro y que directamente podrían ser objeto de amnistía **unas 300 personas detenidas en el ejercicio del derecho a la protesta y acusados de crímenes menores.** Además de que estas personas podrían recobrar su libertad, las personas beneficiadas y que están en esas condiciones podría ascender a 1500.

Además, el acuerdo también contempla la **“Amnistía de iure” por los delitos políticos de rebelión, sedición, asonada, conspiración y seducción, usurpación y retención ilegal de mando** y los delitos que son conexos con estos de conformidad con esta ley.

### **Los delitos conexos** 

Son delitos conexos las muertes en combate compatibles con el Derecho Internacional Humanitario y la aprehensión de combatientes efectuada en operaciones militares”; los delitos en los cuales el sujeto pasivo de la conducta es el Estado y **las conductas dirigidas a facilitar, apoyar, financiar u ocultar el desarrollo de la rebelión. Todo ello establecido en el artículo 22 de la ley de amnistía.**

Todos ellos podrían ser objeto indulto, pero hay delitos de Lesa Humanidad que no serán objeto de la amnistía, sin embargo podrían acceder a penas alternativas a la prisión porque expresamente **manifiestan su voluntad de someterse a la Jurisdicción Especial de Paz, en medio de las condiciones que esta exija. **Le puede interesar: [“Amnistías en Colombia deben respetar estándares internacionales”.](https://archivo.contagioradio.com/amnistias-en-colombia-deben-respetar-estandares-internacionales-tom-howland/)

<iframe id="audio_15266171" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_15266171_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
