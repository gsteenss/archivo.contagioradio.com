Title: Lideresa social y 7 personas más son masacradas en Antioquia
Date: 2020-12-15 08:32
Author: CtgAdm
Category: Actualidad, Líderes sociales
Slug: lideresa-social-y-7-personas-mas-son-masacradas-en-antioquia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/12/diseños-para-notas-5.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

Este fin de semana se registraron nuevos hechos violentos que vulneraron la vida de **7 personas en el departamento de Antioquia y juntos a ellos y ellas el asesinato de una lideresa social**, hechos que se dan en medio de las alertas tempranas y las exigencias de vida por parte de la comunidad al Gobierno Nacional.

<!-- /wp:paragraph -->

<!-- wp:embed {"url":"https://twitter.com/Indepaz/status/1338178247142633472","type":"rich","providerNameSlug":"twitter","responsive":true,"className":""} -->

<figure class="wp-block-embed is-type-rich is-provider-twitter wp-block-embed-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/Indepaz/status/1338178247142633472

</div>

</figure>
<!-- /wp:embed -->

<!-- wp:paragraph -->

Este sábado 13 de diciembre se registró una intervención armada en donde **tres personas fueron asesinadas, cinco resultaron heridas y una fue reportada como desaparecida en el corregimiento rural de Cuturú**, en el municipio de Caucasia (Antioquia).

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Según reportó a medios locales el comandante de la Policía de ese departamento, Jorge Cabra, el hecho fue perpetrado por *"**hombres que vestían uniformes de la Fuerza Pública y que ingresaron al territorio en en lanchas**, por el río Nechí"*.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

**La masacre en Cuturú, se da luego de que el viernes 11 de diciembre cuatro hombres fueran asesinados en el municipio El Bagre**, a unos cincuenta kilómetros de Cuturú.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Según el Instituto de Estudios para el Desarrollo y la Paz (Indepaz), **esta es la tercera masacre perpetrada en el mes de diciembre**, de estás 72 personas han perdido su vida en 19 masacres durante solo en Antioquia ([CIDH está al tanto de las barreras que pone el Estado para acceder a información sobre la verdad del conflicto](https://archivo.contagioradio.com/cidh-barreras-impuestas-por-instituciones-del-estado-para-que-conozca-verdad-del-conflicto/))

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### **Elizabeth Betancur , lideresa social de Yolombo, Antioquia** es asesinada

<!-- /wp:heading -->

<!-- wp:embed {"url":"https://twitter.com/Justiciaypazcol/status/1337752213368791041","type":"rich","providerNameSlug":"twitter","responsive":true,"className":""} -->

<figure class="wp-block-embed is-type-rich is-provider-twitter wp-block-embed-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/Justiciaypazcol/status/1337752213368791041

</div>

</figure>
<!-- /wp:embed -->

<!-- wp:paragraph -->

Elizabeth Betancur, ejercía su labor en la comunidad como Capacitadora de la Red Nacional de Mujeres Comunales de Colombia capítulo Yolombo, en el departamento de Antioquia. Con ella son 291 líderes, lideresas y defensores de Derechos Humanos asesinados en 2020.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Según las autoridades locales,  la lideresa fue asesinada con arma de fuego en el sector El Atajo de la vereda Las Margaritas, ubicada a 40 minutos del casco urbano del municipio. 

<!-- /wp:paragraph -->

<!-- wp:image {"id":94157,"sizeSlug":"large","linkDestination":"none"} -->

<figure class="wp-block-image size-large">
![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/12/diseños-para-notas-5.jpg){.wp-image-94157}

</figure>
<!-- /wp:image -->

<!-- wp:paragraph -->

Al la fecha al menos 131 mujeres han sido asesinadas en Colombia tras la firma del Acuerdo de Paz, la mayoría de estos casos están relacionados con el liderazgo social que han desarrollado en sus comunidades y territorios.

<!-- /wp:paragraph -->
