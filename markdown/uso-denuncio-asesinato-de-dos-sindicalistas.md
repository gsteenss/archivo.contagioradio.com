Title: USO denunció asesinato de dos sindicalistas
Date: 2018-01-25 15:13
Category: DDHH, Nacional
Tags: Asesinatos, lideres sociales, Sindicalistas, union sindical obrera
Slug: uso-denuncio-asesinato-de-dos-sindicalistas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/01/moir.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Moir] 

###### [25 Ene 2018] 

La Unión Sindical Obrera denunció que el pasado **21 de enero fue asesinado Víctor Manuel Barrera**, y el **22 de este mismo mes, fue asesinado Wuilmer Angarita**. La USO, manifestó que rechaza los actos de violencia en los que fueron asesinadas ambas personas e hizo un llamado a que se brinden garantías para la construcción de paz en el país.

**Víctor Manuel Barrera era electricista de la empresa contratista COFIPETROL**, desde hace 6 años, y llevaba afiliado a la USO 27 años, su muerte se produjo durante un intercambio de disparos entre un grupo armado no identificado y escoltas de la Unidad Nacional de Protección que protegía una comisión de la FARC, en la vereda El Oasis, en el municipio de Arauquita, en Arauca. Según el comunicado Henry Pérez y Juan Torres, integrantes de la FARC contra quienes iba dirigido el atentado, resultaron ilesos.

El asesinato de Wilmer Angarita, **se produjo en la vía Tame –Cucutá, mientras él se movilizaba en su vehículo**, el hecho ya se encuentra en investigación, sin embargo, aún no hay móviles del homicidio. Angarita también hacía parte de la USO. (Le puede interesar: ["Incursión Paramilitar deja tres muertos y nueve heridos en Argelia, Cauca](https://archivo.contagioradio.com/incursion_armada_paramilitar_argelia_cauca/)")

Sumado a estos hechos de violencia, la USO denuncio que dos dirigentes indígenas, de la comunidad Julieros, en el municipio de Tame, Arauca, que murieron en hecho confusos, el pasado 18 de enero, aumentan la incertidumbre que existe sobre la implementación de los Acuerdos de Paz y **“ánima a los promotores de la guerra, para que Colombia regrese a la inhumana confrontación armada”**.

En ese sentido la USO hizo un llamado a la sociedad civil para que no se desista en los esfuerzos de paz, que ya se encuentran avanzado, pese a los números obstáculos y reitero que el país no puede retornar a una “horrible noche”.

[Comunicado No Renunciar a La Construccion de La Paz Enero 23 de 2018](https://www.scribd.com/document/369992770/Comunicado-No-Renunciar-a-La-Construccion-de-La-Paz-Enero-23-de-2018#from_embed "View Comunicado No Renunciar a La Construccion de La Paz Enero 23 de 2018 on Scribd") by [Contagioradio](https://www.scribd.com/user/276652802/Contagioradio#from_embed "View Contagioradio's profile on Scribd") on Scribd

<iframe id="doc_41664" class="scribd_iframe_embed" title="Comunicado No Renunciar a La Construccion de La Paz Enero 23 de 2018" src="https://www.scribd.com/embeds/369992770/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-8uO04eqzvTnZ54qPlTNG&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="0.7729220222793488"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
