Title: En operativo de desalojo del ESMAD y Policía es asesinado Duván Aldana
Date: 2020-06-27 12:16
Author: AdminContagio
Category: Actualidad
Slug: en-operativo-de-desalojo-del-esmad-y-policia-es-asesinado-duvan-aldana
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/06/Duvan-Aldana.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

La Red Popular de Derechos Humanos de Bogotá, denunció en horas de la noche de este viernes 25 de Junio que el ESMAD habría asesinado a **Duván Mateo Aldana**, joven de 15 años de edad. El hecho se presentó en medio de un operativo de desalojo realizado por ese escuadrón en el municipio de Soacha, concretamente en el barrio Ciudadela Sucre.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Según la denuncia en los últimos días un grupo de 800 personas tuvieron que abandonar sus lugares de habitación ya que no pudieron pagar las deudas de arrendamientos en medio de la pandemia y decidieron ocupar un lote baldío mientras lograban entablar diálogos con la alcaldía del municipio para buscar salidas prontas a su crisis económica.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Sin embargo, estos acercamientos nunca se presentaron y por el contrario desde el pasado 24 de Junio se tuvieron que enfrentar al Escuadrón Móvil Antidisturbios que arribó al lugar para desalojar a las personas que se habían resguardado con plásticos y tablas para soportar las inclemencias del tiempo.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Según la **Red de DDHH** en el momento del desalojo, efectivos de la policía, de fuerza disponible y el ESMAD dispararon en varias ocasiones en contra de las personas que intentaban evitar que los desalojaran, uno de esos disparos impactó en el cuello de Mateo quien fue conducido a un centro asistencial ese mismo día, lamentablemente el joven murió en horas de la tarde de ese 25 de Junio.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Según la madre del menor, citada en la denuncia, Mateo era estudiante de octavo grado en el colegio Buenos Aires de Soacha y era el único apoyo, ya que ella había perdido su empleo.

<!-- /wp:paragraph -->

<!-- wp:heading -->

**Testigos y videos corroboran que la policía disparó contra la gente**
-----------------------------------------------------------------------

<!-- /wp:heading -->

<!-- wp:paragraph -->

Según la abogada Isabel Fajardo, defensora de DDHH que ha acompañado a las familias, aunque no se ha podido establecer cuáles fueron los agentes que dispararon contra ****Duván Aldana****, hay varios testigos que evidenciaron el hecho y están dispuestos a denunciar a los efectivos de la Fuerza Pública.

<!-- /wp:paragraph -->

<!-- wp:html -->  
<iframe src="https://www.facebook.com/plugins/video.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2Fvideos%2F735978790497451%2F&amp;show_text=false&amp;width=734&amp;height=1335&amp;appId" width="734" height="1335" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowtransparency="true" allow="encrypted-media" allowfullscreen="true"></iframe>  
<!-- /wp:html -->

<!-- wp:paragraph -->

Adicionalmente, la jurista informó que hasta que se de un dictamen oficial de Medicina Legal y el cuerpo de **Duván Aldana** pueda ser entregado se entablará la denuncia formal. Entre tanto la comunidad está buscando la manera de reunir los recursos para el sepelio del joven que no ha sido entregado a sus familiares pues no cuentan con un servicio funerario, requisito de Medicina Legal que tras la emergencia sanitaria.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

La Red de Derechos Humanos también alertó por el grave riesgo que corren las personas que se han asentado temporalmente en la comuna ocho de Soacha, puesto que la situación en medio de la pandemia se hace más compleja pues tendrán que hacinarse en sitios en los que se les preste solidaridad sin contar con los elementos de bioseguridad necesarios para su protección.

<!-- /wp:paragraph -->

<!-- wp:heading -->

**ESMAD tiene un historial de crímenes e impunidad**
----------------------------------------------------

<!-- /wp:heading -->

<!-- wp:paragraph -->

Otros de los casos de menores de edad asesinados por el ESMAD son los de **Nicolás Neira** y más recientemente **[Dilan Cruz](https://archivo.contagioradio.com/las-razones-para-que-asesinato-de-dylan-cruz-sea-tomado-por-la-justicia-ordinaria/),** ambos casos siguen en la impunidad y la preocupación de las víctimas y sus representantes es que este escuadrón sigue funcionando y los crímenes continúan en la impunidad

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Aunque el caso de Mateo y de Dilan son los más recientes hay un historial de por lo menos 23 jóvenes que han sido asesinados por ese escuadrón desde que fue creado. Varios organismos nacionales e internacionales se han manifestado por la necesidad de regular la práctica del ESMAD, regular las armas y regular los protocolos de los gobernantes para afrontar las manifestaciones sociales, sin embargo no se han aplicado las medidas necesarias para frenar estos crímenes, evitar que se repitan y castigar a los responsables de hechos anteriores.

<!-- /wp:paragraph -->

<!-- wp:heading -->

Desalojo en medio de la pandemia
--------------------------------

<!-- /wp:heading -->

<!-- wp:paragraph -->

Este es el segundo desalojo que se ha realizado en contra de familias altamente vulnerables en Bogotá y sus alrededores. Uno de ellos se realizó varios días en la localidad de Ciudad Bolívar en Bogotá en donde cerca de cincuenta familias fueron dejados a la intemperie porque estaban ocupando un predio que no les pertenecía. En esa ocasión la Alcaldía de Bogotá ordenó el desalojo.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Aunque algunas de las familias fueron ubicadas temporalmente en sitios de la localidad, la mayoría de ellas aún ve incierto el futuro pues muchos de ellos son víctimas de la guerra, víctimas de desplazamiento forzado y no cuentan con los recursos necesarios, muchos de ellos y ellas están desempleados y no han recibido ninguna atención eficaz y suficiente por parte de los gobiernos.

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
