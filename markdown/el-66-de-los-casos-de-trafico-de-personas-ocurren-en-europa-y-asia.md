Title: El 66% de los casos de tráfico de personas ocurren en Europa y Asia
Date: 2016-09-23 14:55
Category: DDHH, El mundo
Tags: derechos de las mujeres, Día de no Violencia contra la mujer, maltrato a las mujeres, víctimas de violencia sexual
Slug: el-66-de-los-casos-de-trafico-de-personas-ocurren-en-europa-y-asia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/09/Trata-de-personas.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: iecah org] 

###### [23 Sept 2016] 

Desde 1999, el 23 de septiembre se conmemora el Día Internacional en Contra la Explotación Sexual y el Tráfico de Mujeres, Niñas y Niños; definida como: "la captación, el transporte, el traslado, la acogida o la recepción de personas, recurriendo a la amenaza o al uso de la fuerza u otras formas de coacción, al rapto, al fraude, al engaño, al abuso de poder o de una situación de vulnerabilidad o a la concesión o recepción de pagos o beneficios para obtener el consentimiento de una persona que tenga autoridad sobre otra, con fines de **explotación sexual, trabajos o servicios forzados, esclavitud o prácticas análogas a la esclavitud**, la servidumbre o la extracción de órganos".

Según el reporte bienal presentado por la ONU en 2014 sobre el tráfico de personas, en el mundo el porcentaje de la población que enfrenta este tipo de flagelo, con fines de diversa índole como explotación sexual, trabajos forzados y tráfico de órganos, corresponden un 49% a mujeres, 18% a hombres, 21% a niñas y el 12% a niños. **Frente a la explotación sexual Europa y Asia Central ocupan el primer lugar con un 66% de casos**; le sigue África y Medio Oriente con 53%, el continente americano con 48% y finalmente el Este y sur de Asia, con un porcentaje del 26%.

De acuerdo con la ONU, actualmente más del 90% de los países tienen una legislación en la que se criminaliza el tráfico de personas, de manera especial, el tráfico de personas para la explotación sexual, la cual es catalogada como delito de la delincuencia organizada transnacional. Por su parte, la Comisión Interamericana de Mujeres sobre el Tráfico de Personas y Explotación Sexual, asegura que este accionar constituye una **grave violación a los derechos humanos y los derechos de la mujer**, que les niega la salud, la libertad de vivir sin violencia y el no ser sometidas a esclavitud o servidumbre.

Este reporte bienal de la ONU también evidencia que las **desigualdades de género son un elemento transversal** en este tipo de problemáticas, por lo que aspectos como los altos niveles de desempleo femenino, todas las formas de violencia de género, la discriminación contra las mujeres y la mercantilización de los cuerpos, son algunas de las situaciones que deben transformarse para erradicar el tráfico y la explotación.

Pese a que en 2005, Colombia ratificó los acuerdos de la Convención de la Naciones Unidas Contra la Delincuencia Organizada Transnacional, el más reciente informe sobre el tráfico de personas señala que la nación cumple de forma mínima con los estándares propuestos para la eliminación del flagelo, en gran parte porque **no han habido avances en la identificación, enjuiciamiento y condena de los traficantes**. Según afirmó la ONU, en Colombia es alarmante la cantidad de casos de trata de menores y adolescentes para explotarlos sexualmente, siendo el turismo sexual, los matrimonios serviles y la prostitución con fines de comercialización pornográfica, los casos más frecuentes.

Organizaciones no gubernamentales y la sociedad civil **aúnan esfuerzos en la lucha por la restitución de los derechos de las personas víctimas**, es el caso de la Corporación Espacios de Mujer que insta al Gobierno a fortalecer su aparato institucional en las medidas y acciones para la prevención y atención de estos casos.

Vea también: [[Grupos paramilitares ahora se financian con la trata de personas](https://archivo.contagioradio.com/grupos-paramilitares-ahora-se-financian-con-la-trata-de-personas/)]

###### [Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)]] 
