Title: Gendarmería rural “no es una propuesta tan descabellada” Alfredo Molano
Date: 2015-02-02 18:27
Author: CtgAdm
Category: Paz, Política
Tags: CMHV, Juan Manuel Santos, paz
Slug: gendarmeria-rural-no-es-una-propuesta-tan-descabellada-alfredo-molano
Status: published

###### Foto: youtube.com 

##### [Entrevista a Alfredo Molano]<iframe src="http://www.ivoox.com/player_ek_4029063_2_1.html?data=lZWfm5Wad46ZmKiak5WJd6KmlpKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRic%2Fo08rjy9jYpYzVjKbZyNfJqNCfrtTZw9PTcYarpJKw0dPYpcjd0JC%2Fw8nNs4yhhpywj5k%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe> 

Si se habla de paz, de dejación de armas y de integración a la vida civil de los guerrilleros, **se les deben abrir todos los espacios, incluso los de las FFMM, afirma Alfredo Molano,** sociólogo, historiador e integrante de la CMHV, convocada por la mesa de conversaciones con las FARC. Para Molano, lo que hace el presidente Juan Manuel Santos al **lanzar propuestas como la de la “gendarmería rural” es lanzar un “globo de sondeo” a la opinión pública**.

Lo preocupante, según Molano, no es la propuesta en sí, que no es tan descabellada, sino la **reacción por parte de los sectores de derecha, reflejados en las afirmaciones del Procurador Ordóñez, el Ministro de Defensa y el senador Álvaro Uribe**, que están generando un ambiente que no será favorable para la aplicación de los acuerdos que se alcancen en La Habana.

Frente al inicio de un nuevo ciclo de conversaciones entre el gobierno y la guerrilla, Molano asegura que es el momento de **establecer unas condiciones de no retorno**, por ello deben fijarse metas para el **desescalamiento del conflicto y las condiciones del cese bilateral de fuego.**

Por otra parte afirma que las **guerrillas han estado sometidas a un proceso de satanización desde sus propios orígenes**, sin embargo, hay sectores de la sociedad que están asumiendo la necesidad de bajar los niveles de confrontación para llegar a un ambiente adecuado.

El próximo **10 de Febrero, la Comisión de Memoria del Conflcito y sus víctimas, presentará el informe** en el que viene trabajando desde el pasado mes de Septiembre, y arrojará luces para establecer las responsabilidades de los diferentes actores, como parte de los insumos necesario para la comisión de la verdad que sería un trabajo posterior a los eventuales acuerdos.
