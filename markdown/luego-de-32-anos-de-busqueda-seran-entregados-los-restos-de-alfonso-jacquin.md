Title: Luego de 32 años de búsqueda serán entregados los restos de Alfonso Jacquin
Date: 2018-07-25 17:14
Category: DDHH, Paz
Tags: Alfonso Jacquin, Palacio de Justicia, toma y retoma del palacio de justicia
Slug: luego-de-32-anos-de-busqueda-seran-entregados-los-restos-de-alfonso-jacquin
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/07/alfonso-jacquin1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Pintura de Josefina Jacquin] 

###### [25 Jul 2018] 

Luego de 32 años de la búsqueda silenciosa de Alfonso Jacquin, integrante del M19 que participó en la toma del Palacio de Justicia en 1985, sus restos serán entregados a su familia el próximo jueves 26 de julio. Un hecho que, para Lilia, su hermana, cierra el capítulo de tener los restos de su hermano, **pero abre otro con dudas y preguntas sobre los hechos ocurridos ese 6 y 7 de noviembre**.

### **¿Quién era Alfonso Jacquin?** 

Oriundo de Santa Marta, Jacquin se gradúo de la Universidad del Atlántico en Derecho Constitucional. Sus familiares y amigos afirman que **tenía una excelente capacidad de oratoria, que era un buen cantante y disfrutaba tocar guitarra**. Ingresó a la Unión Revolucionaría Socialista y fue a través de la academia que conoció al M19.

Uno de los recuerdos que hay en la memoria colombiana es la voz de Jacquin, al interior del Palacio de Justicia, **pidiendo un alto al fuego por parte de la Fuerza Pública** para que no se acabara con la vida de los magistrados de la Corte Suprema de Justicia que se encontraban recluidos con él y otros integrantes del M19 en el piso 4.

Lo que han establecido investigaciones, es que Jacquin era la persona encargada de entablar un diálogo con el gobierno que permitiera la salida de la **mayor cantidad de rehenes y militantes del M19 con vida del Palacio de Justicia** y lograr presionar al Estado en el cumplimiento de los acuerdos de paz que había firmado con esta guerrilla.

En marzo de este año, la Fiscalía General de la Nación confirmó que en la realización de muestras de ADN de la exhumación del cuerpo de Libardo Duran, escolta del magistrado Reyes Echandía, se encontraban los restos de Alfonso Jacquin. (Le puede interesar: ["A 33 años, no hay justicia para las víctimas de tortura del Palacio de Justicia"](https://archivo.contagioradio.com/a-33-anos-aun-no-hay-justicia-para-las-victimas-de-tortura-del-palacio-de-justicia/))

### **La estigmatización de quienes son familiares de un guerrillero** 

Esos 32 años son descritos por Lilia como una **búsqueda silenciosa debido a la estigmatización y seguimientos de los que fue víctima su familia** al intentar encontrar a Alfonso, “pasamos por todos los lugares que teníamos conocimiento que nos podían definir la demanda de búsqueda de desaparecidos, los consultorios jurídicos de las universidades se negaron a recibir el caso”.

“Ha sido doloroso, después de que oímos su voz el 6 y 7 de noviembr**e y la prensa se encargó de decir que salía vivo,** nos tocó hacer de tripas corazón para continuar con nuestras vidas y con la memoria de que él era un hombre alegre” afirmó Lilia.

Asimismo, recuerda que Jacquin era un hombre servicial y atento con su familia, que mantuvo en secreto su clandestinidad alejándose por periodos de ese núcleo familiar, razón por la cual para ellos fue sorpresivo enterarse del ingreso de Alfonso al M19.

### **Las dudas que rondan la muerte de Jacquin** 

Con la entrega de restos de Jacquin, que se realizará el 26 de julio, se abre un nuevo capítulo en la búsqueda de verdad que para **Lilia se traduce en conocer qué pasó durante la toma y retoma del Palacio de Justicia y de qué forma murió su hermano**.

El abogado Eduardo Carreño, del Colectivo de Abogados José Alvear Restrepo que lleva este caso, manifestó que quedan dudas frente a por qué el cuerpo de Jacquin es entregado como el de Duran, por qué si hay un audio de un integrante de la Fuerza Pública afirmando que Jacquin se encontraba entre los militantes del M19 muertos, sus restos aparecen calcinados. (Le puede interesar: "[¿Quién responde por las entregas equivocadas del holocausto del Palacio de Justicia?](https://archivo.contagioradio.com/quien-responde-por-las-entregas-equivocadas-del-holocausto-palacio-de-justicia/)")

Estas preguntas se suman a las ya hechas por las demás familias de las víctimas del Palacio de Justicia, que luego de 32 años de búsqueda de sus seres queridos continúan exigiendo conocer la verdad. El próximo 27 de julio, en las instalaciones de la Corporación Colombiana de Teatro, se hará un evento de homenaje a la memoria de Alfonso Jacquin.

<iframe id="audio_27238431" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_27238431_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
