Title: Aplazan audiencia de imputación de cargos contra oficiales responsables de la masacre de Tumaco
Date: 2018-01-17 18:34
Category: DDHH, Nacional
Tags: campesinos asesinados en Tumaco, El Tandil, Tumaco
Slug: tumaco_masacre_policia_campesinos_asesinados
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/10/tumaco.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:  Sonia Cifuentes - Asociación Minga] 

###### [17 Ene 2018]

Aunque este miércoles se esperaba la audiencia de imputación de cargos contra **Javier Enrique Soto García,** **comandante del ‘Núcleo Delta’ de la Policía y  **Jorge Niño León,** comandante del Pelotón Dinamarca I del Ejército,** por la masacre del pasado 5 de octubre en la vereda El Tandil, Tumaco, los familiares de las siete víctimas mortales, deberán esperan un mes, ya que la defensa pidió que fuera aplazada la diligencia.

Así lo señala Diana Montilla, vocera de  la Asociación de Juntas de Acción comunal de los ríos Nulpe y Mataje, ASOMINUMA, quien si bien celebra que la fiscalía haya documentado rápidamente el caso y que por eso ya se hable de una audiencia de imputación de cargos, avalando la versión de los campesinos e indígenas, la comunidad sigue insistiendo en que "este hecho sea conocido por una fiscalía encargada de Derechos Humanos y que se asuma de esa manera hasta el final de la investigación".

### **El material recolectado** 

Los delitos que se le imputarán a los oficiales, según había informado la vicefiscal María Paulina Riveros son: **homicidio agravado y homicidio agravado tentado en calidad de autores, por posición de garantes**. Este último teniendo en cuenta que al ser miembros de la Fuerza Pública, su obligación era salvaguardar la integridad y vida de los ciudadanos.

Fueron cerca de 70 funcionarios e inevstigadores, los que fueron al Tandil donde centraron su peritazgo en las necropsias, dictámenes médico-legales y en el informe que daba cuenta de la trayectorias de proyectiles. Asimismo, fueron analizados los explosivos que se encontraron en el lugar de los hechos, y además se tuvieron en cuenta las 400 declaraciones juradas y entrevistas.

“El grupo de fiscales encontró mérito para solicitar la audiencia de formulación de imputación a dos oficiales de la Policía Nacional y del Ejército Nacional”, había afirmado la vicefiscal. (Le puede interesar: [Los testimonios que comprometen a la fuerza pública con la masacre en El Tandil)](https://archivo.contagioradio.com/los-testimonios-que-comprometen-a-la-fuerza-publica-en-masacre-de-tumaco/)

En ese marco, de acuerdo con Montilla **es importante que se adelante esta audiencia de imputación de cargos, pues se trata de personas que conocen cómo se desplegaron esas acciones.** No obstante, afirma que es necesario que se amplíe la vinculación a quienes participaron en los hechos independientemente de su rango, pues la versión de la comunidad apunta a que fueron varios los uniformados que accionaron sus armas de forma indiscriminada.

### **Continúan la erradicación forzada y los incumplimientos del gobierno** 

Mientras tanto, el panorama de las familias que viven en el territorio de Alto Mira y Frontera, no ha cambiado, pese a lo que en su momento habían sido las promesas del vicepresidente Óscar Naranjo. Según denuncia la defensora de DDHH, el acompañamiento del Estado ha sido ineficaz. Hasta el momento no ha habido atención psicosocial para las víctimas, no se ha atendido la difícil situación económica de los habitantes, las problemáticas ambientales continúan y sigue siendo precaria la educación y el servicio de salud.

**"Esta es la fecha en que la personería de Tumaco no ha realizado una jornada para recepcionar las declaraciones de esos hechos que fueron tan graves** para la comunidad de El Tandil y para otras veredas que se vieron afectados por la masacre", dice Montilla.

Simado a lo anterior, pese a la disposición de las familias campesinas de adherirse a los planes de sustitución, las labores de erradicación forzada continúan sin que haya ningún tipo de concertación entre las autoridades y los campesinos. Por el momento, **se espera que las mesas de negociación continúen la próxima semana** y se logre un acuerdo que impida que se siga vulnerando la única salida económica con la que cuentan las comunidades.

"Mientras no exista una voluntad seria y real para acompañar a las víctmas, el panorama va a seguir siendo el mismo. Va a ser muy difícil lograr la paz, pues en Tumaco se sigue viviendo la violencia y el olvido estatal", concluye Diana Montilla. (Le pude interesar: [Tumaco llora una tragedia en tiempos de paz)](https://archivo.contagioradio.com/tumaco-llora-una-masacre-en-tiempos-de-paz/)

<iframe id="audio_23213203" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_23213203_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio [en su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio ](http://bit.ly/1ICYhVU)
