Title: En medio de gases y lágrimas ConocoPhillips entra maquinaria a San Martín
Date: 2016-10-20 12:15
Category: Ambiente, Nacional
Tags: Abuso de fuerza ESMAD, No más Fracking, Paro cívico
Slug: en-medio-de-gases-y-lagrimas-conocophillips-logra-entrar-maquinaria-a-san-martin
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/10/IMG-20161019-WA0011-e1476994049883.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Samuel Arregocés] 

###### [20 Oct 2016] 

En medio de llantos, gases lacrimógenos, bombas de aturdimiento y recalzadas, los habitantes de la vereda Cuatro Bocas de San Martín en Cesar, **vieron impotentes como el ESMAD facilitó la entrada de la maquinaria de ConocoPhillpis a sus territorios,** una situación que, según ellos, acabará con el agua y los recursos naturales que les han permitido vivir en ese territorio que muchos quieren proteger.

Sin embargo, desde las 5 de la tarde del miércoles, una Asamblea Campesina definió que a partir de la mañana de este jueves **decretaban un Paro Cívico para seguir sentando su voz de protesta frente a la realización del Fracking en sus territorios.** En esa misma asamblea se pudo establecer que el número de personas heridas ascendía a 12 y que fueron emitidas 15 órdenes de captura contra las personas que defendían la finca a la que llegó la maquinaria. Le puede interesar: [Fuerte represión del ESMAD contra comunidad de San Martín que se opone al Fracking.](https://archivo.contagioradio.com/fuerte-represion-del-esmad-contra-comunidad-de-san-martin-que-se-opone-al-fracking/)

Samuel Arregocés, testigo de los hechos, reveló que la violación de Derechos Humanos es alarmante, hasta el punto que **“la abogada Julia Figueroa del Colectivo de Abogados Luis Carlos Pérez, fue alcanzada por los gases lacrimogenos". **

Arregocés, añade que a pesar de la resistencia de la comunidad, al final de la jornada “la maquinaria logra entrar, **la gente lloraba porque su tierra va a ser saqueada, sienten impotencia ante la fuerza bruta del Estado”.**

Por otra parte, concluyó mencionando que las comunidades siguen en pie, oponiéndose a este tipo de extracción en sus territorios, y muestra de ello fue una marcha espontánea ocurrida en horas de la noche del miércoles, en la que **“unas mil personas estaban por las calles en contra del fracking y apoyando a la comunidad de Cuatro Bocas”. **Le puede interesar: [En fotos: marcha carnaval no al Fracking, San Martín, Cesar.](https://archivo.contagioradio.com/en-fotos-marcha-carnaval-no-al-fracking-san-martin-cesar/)

<iframe id="audio_13404448" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_13404448_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en su correo [[bit.ly/1nvAO4u]
