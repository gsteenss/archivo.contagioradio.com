Title: Desde el feminismo campesino trabajan más de 300 mujeres de las ZRC
Date: 2020-01-23 18:41
Author: CtgAdm
Category: eventos, Mujer
Tags: Mujeres Campesinas
Slug: feminismo-campesino-trabajan-mujeres
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/01/Feminismo-campesino.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: ANZORC

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Durante el 22 y el 23 de enero, más de 300 mujeres de todo el país se dieron cita en Bogotá en el **Tercer Encuentro de Mujeres de Zonas de Reserva Campesina** (ZRC), un espacio para debatir y deliberar sobre las agendas programáticas que tienen las mujeres en los territorios y las apuestas a nivel nacional para incidir políticamente en todos los escenarios.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Un feminismo campesino

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

**Yurani Cuellar, líderesa social y vocera de la Asociación Campesina del Valle del Cimitarra** **e integrante de la Coordinadora Nacional de Mujeres de las ZRC** señala que este encuentro ha permitido establecer cómo desde sus miradas de mujer se puede incidir "en la defensa de nuestros derechos". a través de ejes temáticos como la participación política, la paz en los territorios, la construcción del feminismo campesino, los derechos de las mujeres campesinas, la soberanía alimentaria y el fomentar las Zonas de Reserva Campesinas como territorios libres de violencias.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

En un país como Colombia donde 5.442.241 de las mujeres que conforman el 51% de la población habitan en zonas rurales, 37% viven en condiciones de pobreza y 4.85% trabaja en la producción agrícola, según datos aportados por la Agencia de Desarrollo Rural, el trabajo que viene realizando la **Coordinadora Nacional de Mujeres de las ZRC** y sus encuentros resultan esenciales para luchar contra la desigualdad de género que se evidencia en el campo y en general en todo el territorio nacional [(Lea también: Mujeres Campesinas "rebeldes pero con causa")](https://archivo.contagioradio.com/lmujeres-campesinas-rebeldes-pero-con-causa/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Es por ello que Yurani afirma que desde las zonas de reserva han decidido trabajar por un feminismo campesino que les represente y que a su vez les permita trabajar "de la mano con nuestros compañeros" y que permita una transformación real, una transformación con igualdad donde tanto hombres como mujeres tengan las mismas garantías. [(Le recomendamos leer: Duque olvidó el compromiso del Acuerdo de Paz con las mujeres rurales)](https://archivo.contagioradio.com/duque-olvido-el-compromiso-del-acuerdo-de-paz-con-las-mujeres-rurales/)

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### **Es necesaria una representación de las mujeres campesina**s

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Yurani manifestó que a través de estas iniciativas buscan "[reivindicar sus derechos](https://twitter.com/ANZORC_OFICIAL/status/1220415225020801024) y la participación política que incluya la toma de decisiones, autonomía sobre sus derechos sexuales y reproductivos, el acceso a la tierra que históricamente les ha sido negado es lo que las mujeres hoy exigen. que se han debatido en los territorios porque hay la necesidad de entender que necesitamos participación política, y hacer sinergia con la defensa de la paz y el territorio y poder garantizar nuestra permanencia en este último. [(Le puede interesar: Las barreras para el desarrollo de la mujer rural en América Latina)](https://archivo.contagioradio.com/las-barreras-para-el-desarrollo-de-la-mujer-rural-en-america-latina/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

"Estamos alzando las voces, buscando la forma que la institucionalidad nos escuche y conozca nuestras propuestas e iniciativas y venir desde los territorios más lejanos es una movilización", explica Yurani refiriéndose a la forma en que desde el campesinado se ha asumido el rol de la mujer en contextos como el Paro Nacional como una forma de exigir al Gobierno **una representación de la mujer para decidir sobre el rumbo y las transformaciones de este país.** [(Lea también: Cinco propuestas de mujeres que le apuestan a la paz)](https://archivo.contagioradio.com/cinco-propuestas-de-mujeres-que-le-apuestan-a-la-paz/)

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Soberanía alimentaria e importancia de semillas nativas

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Otro punto clave de este encuentro es el rescate de lo tradicional liderado por las mujeres "quienes han sido las cuidadoras de la cultura ancestral" por lo que se busca poder reafirmar y trabajar en la defensa de las semillas nativas, "si no hay semilla, no hay comida para la sociedad", expresa la lideresa. Para promover esta iniciativa, durante el encuentro se han realizado trueques de semilla de un lugar a otro para poder garantizar su siembre en todos los territorios, **"no queremos las semilas transgénicas, estamos cansadas que se incorporen en los territorios, queremos que reconozcan el rol de la mujer en la defensa de las semillas nativas**".

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Entre el maíz, el fríjol, el plátano y el arroz, las más de 300 asistentes pudieron comprobar que aún existe una serie de semillas en su tierra y que su permanencia debe ser garantizada por el Estado colombiano. Dicha iniciativa también ha sido llevada a promover la defensa de la vida en las poblaciones, "queremos un territorio libre de violencia y garantías para poder trabajar y que de paso las mujeres podamos tener esa participación que históricamente hemos perseguido", concluye destacando la importancia de afianzar el feminismo y recuperar la autonomía sobre sus vidas y cuerpos y un acceso equitativo a educación, a salud, trabajo y actividades cotidianas.

<!-- /wp:paragraph -->

<!-- wp:html -->  
<iframe id="audio_46925801" frameborder="0" allowfullscreen scrolling="no" height="200" style="border:1px solid #EEE; box-sizing:border-box; width:100%;" src="https://co.ivoox.com/es/player_ej_46925801_4_1.html?c1=ff6600"></iframe>  
<!-- /wp:html -->
