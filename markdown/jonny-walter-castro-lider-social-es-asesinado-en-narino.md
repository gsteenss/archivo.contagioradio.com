Title: Jonny Walter Castro, líder social es asesinado en  Nariño
Date: 2020-11-22 11:37
Author: AdminContagio
Category: Actualidad, Líderes sociales
Slug: jonny-walter-castro-lider-social-es-asesinado-en-narino
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/muerte-de-lideres-sociales.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

Este 20 de noviembre **en horas de la mañana fue asesinado el líder social Jonny Walter Castro**, integrante de la Mesa de Víctimas, en el corregimiento de San Francisco de Linares en el departamento de Nariño.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/fundepaz/status/1329845232222007296","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/fundepaz/status/1329845232222007296

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

Según integrantes de la comunidad un hombre armado ingreso a la vivienda de Jonny Walter Castro, **luego de llamarlo por su nombre lo asesinó con arma de fuego**.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/ONUHumanRights/status/1329967091714285568","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/ONUHumanRights/status/1329967091714285568

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

Según la **Fundación de Desarrollo y Paz** - FundePaz-, el líder social tenía 40 años de edad, y no solamente e**ra reconocido por su liderazgo, sino también por impulsar las causas sociales** en beneficio de las personas en condiciones de pobreza.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por otro lado la **[Unidad para las Víctimas](https://www.unidadvictimas.gov.co/es/institucional/la-unidad-para-las-victimas-rechaza-el-asesinato-de-jhonny-walter-castro-lider-de) rechazó el asesinato**, indicando que Johnny Walter Castro además de ser representante de la Mesa de Participación Efectiva de las Víctimas por el enfoque discapacidad en Nariño, **era reconocido por sus compañeros *"como un hombre trabajador, que lo dio todo por las víctimas, líder entregado por su comunidad".***

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Según el **Instituto de Estudios para el Desarrollo y la Paz -Indepaz**- , con Castro son **255 los líderes y defensores de Derechos Humanos asesinados en el 2020**, 27 de estos cometidos en el departamento de Nariño. ([971 defensores de DD.HH. y líderes han sido asesinados desde la firma del Acuerdo de paz](https://archivo.contagioradio.com/971-defensores-de-dd-hh-y-lideres-han-sido-asesinados-desde-la-firma-del-acuerdo-de-paz/))

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
