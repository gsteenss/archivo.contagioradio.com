Title: Con el Arroyo Bruno serían 27 las fuentes de agua que El Cerrejón ha secado en La Guajira
Date: 2016-04-14 18:30
Category: Ambiente, DDHH, Nacional
Tags: arroyo Bruno, El Cerrejón, La Guajira
Slug: con-el-arroyo-bruno-serian-27-las-fuentes-de-agua-que-el-cerrejon-ha-secado-en-la-guajira
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/04/IMG_2023.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Exposición Censat Agua Viva 

###### [14 Abr 2016]

Desde la campaña “**La Guajira le habla al país”,** líderes de las comunidades y organizaciones que apoyan los procesos organizativos de la población afectada por la actividad minera de la multinacional El Cerrejón,  convocaron una rueda de prensa donde dieron a conocer la realidad de ese departamento y las verdaderas causas de la crisis humanitaria y ambiental que actualmente atraviesan las familias indígenas y afro.

Según datos de la UNICEF en los últimos **6 años han muerto 5 mil niños del pueblo Wayúu por desnutrición**, y sumado a eso, de acuerdo con las comunidades 26 fuentes de agua se han secado. Desde el gobierno esa situación diariamente es atribuida al fenómeno de El Niño y a la corrupción, sin embargo, lo único cierto para los pobladores es que esa realidad tiene una causa principal, la mina a cielo abierto de El Cerrejón.

Para las comunidades y organizaciones el tema del **fenómeno de El Niño es una “falacia” que ha servido como cortina de humo para tapar las irregularidades que han existido**, y a través de las cuales la multinacional ha logrado obtener los permisos para saquear el agua con la que se abastecen las familias, deforestar cerca de 12 mil hectáreas de bosque seco tropical, desaparecer lagunas y arroyos, desechar residuos de la mina a las fuentes hídricas, engañar a las familias, acabar con tradiciones culturales, entre otras vulneraciones a los derechos humanos y del ambiente, como lo denunciaron las organizaciones Censat Agua Viva - Amigos de la Tierra Colombia, junto a los delegados comunitarios Yoe Arregocés y Rogelio Ustate.

Esa situación de hambre y sed, se agrava ante la inminente desviación del **Arroyo Bruno, afluente principal del Río Ranchería y fuente de agua de la que dependen comunidades wayúu, afrodescendientes y cabeceras municipales como las de Albania y Maicao**. La desviación hace parte del proyecto de expansión del proyecto minero de El Cerrejón, que ha sido denunciado por la población que ve en riesgo su permanencia territorial.

La empresa logró obtener el permiso para intervenir el Arroyo Bruno, gracias a que en noviembre de 2014, la Autoridad Nacional de Licencias Ambientales, ANLA, permitió al Cerrejón la modificación del plan de manejo ambiental. Un procedimiento para el cual CORPOGUAJIRA se declaró sin posibilidad de emitir permisos por incapacidad técnica, por lo que la ANLA, que ya había avalado el desvío, fue quien terminó realizando **“estudios amañados”, que dieron paso a la ampliación de la mina en el Tajo La Puente,** es decir, que fue juez y parte, como asegura Danilo Urrea.

Pero no se trata únicamente de los 3,6 kilómetros que se le desviarían al arroyo Bruno. Se trata de los 9,3 km que también se piensa desviar a esta misma fuente para el 2020. Pero además, se tiene planeado intervenir al arroyo Cerrejón, Tabaco y el río Palomino.

Mientras la mina consume **durante todo el proceso más de 30 millones de litros de agua diarios, como denuncian las comunidades**, según el PNUD, cada persona en la Guajira apenas puede consumir 0,7 Lts de agua diariamente. Frente a lo que cabe resaltar que la Organización de las Naciones Unidas, una persona debe consumir en promedio 50 Lts diarios.

“Cuando la tierra grita, el hombre llora, pero cuando la tierra llora, el hombre grita”, dice Rogelio Ustate, representante legal de Consejo Comunitario de Negros ancestrales de Tabaco, quien enumera las fuentes de agua que se han secado, entre ellas, los arroyos: Aguas Blancas,  La Puente, Cerrejoncito, Araña de Gato, Bartolito,  Morrocon, la chercha, el Sequión, La Trampa, Pupurema, Tabaco, La Puente, Taurina, Chivo feliz, Palomino, La Quebrada, Ceino, Puente Negro.

Asimismo, se han secado las lagunas del Co, Buzu, Fermin, Roche, Garrapatero, El Chivato, El Burro y El Ejemplo, muchas de ellas eran lugares sagrados pues no solo abastecían de agua a las familias, sino que además, alrededor de ellas las comunidades se congregaban para tomar decisiones o realizar rituales ancestrales.

En total según lo que denuncian las comunidades s**erían 26 fuentes hídricas que se habrían secado, y podrían ser más.** Debido a esas experiencias, los habitantes rechazan la desviación del arroyo bruno, cuya intervención significaría que el río ranchería dejaría de recibir 11 millones de litros de agua mensuales.

Finalmente, cabe resaltar que esa intervención del arroyo iría en contradicción con el plan de manejo y ordenamiento de la cuenca del río Ranchería, pues en 2011 se había declarado que en esa zona era prohibido realizar actividades mineras por ser un área de bosque seco tropical, del cual a Colombia solo le queda un 1.5%. En ese sentido se había propuesto que el arroyo y sus alrededores fueran clasificados como parque natural regional.

Es decir que ese **permiso que se concede al Cerrejón va en contravía de las apuestas regionales por la conservación  del bosque seco tropical y las estrategias para enfrentar la desertificación,** pero también está violando los derechos de los pueblos afro e indígenas, pues no se ha aplicado el derecho a la consulta previa como rezan las leyes colombianas.

#### *"Hemos vivido amantándonos de la tierra, pero hoy nuestra madre tierra sufre un deterioro por la multinacional". Rogelio Ustate*

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
