Title: Paramilitares asesinan a un joven mientras jugaba fútbol
Date: 2017-07-29 12:53
Category: DDHH, Nacional
Tags: belén de bajirá, Chocó, paramilitares
Slug: paramilitares-asesinan-a-un-joven-mientras-jugaba-futbol
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/07/cancha-belen.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:] 

###### [29 Jul. 2017] 

Según una denuncia entregada por la Organización Comisión Intereclesial de Justicia y Paz este 27 de julio sobre las 4:50 pm en Belén de Bajirá asesinaron  a un joven de la comunidad llamado **Luis Enor Murillo Chaverra, de 21 años de edad** mientras jugaba un partido de fútbol en la cancha de la comunidad.

De acuerdo con la denuncia los responsables habrían sido hombres autodenominados como **integrantes de las Autodefensas Gaitanistas de Colombia (AGC).** Le puede interesar: [Paramilitares irrumpen en Zona Humanitaria Nueva Esperanza en Jiguamiandó, Chocó](https://archivo.contagioradio.com/paramilitares-jiguamiando-choco/)

Algunos testigos de los hechos señalaron que hombres llegaron en una moto que dejaron a la entrada de la vía que conduce a la cancha de fútbol, minutos después irrumpieron en el partido, **identificaron a Luis Enor, se le acercaron por la espalda** y le dispararon dos veces, acabando con su humanidad.

De igual forma, algunas versiones han manifestado que las autodenominadas Autodefensas Gaitanistas de Colombia (AGC) habrían estado siguiendo a Luis para que realizara algunas “vueltas”, que el joven se negó a llevar a cabo. Le puede interesar:[ 74 municipios dejados por las FARC han sido copados por paramilitares](https://archivo.contagioradio.com/74-municipios-dejados-por-las-farc-han-sido-copados-por-estructuras-paramilitares/)

Las comunidades del Bajo Atrato han denunciado en diferentes ocasiones el accionar y control de las AGC en su territorio y manifiestan que que **hasta el momento no ha existido alguna respuesta por parte de las autoridades**, a pesar de que tienen pleno conocimiento de esta situación.

######  Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
