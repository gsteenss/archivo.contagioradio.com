Title: Congreso de Guatemala retiró inmunidad a Otto Perez Molina
Date: 2015-09-01 17:56
Category: El mundo, Movilización, Política
Tags: CICIG, Guatemala, La Línea, Movilización Social en Guatemala, Otto Pérez Molina, Paro Nacional, Superintendencia de Administración Tributaria
Slug: congreso-de-guatemala-retiro-inmunidad-a-otto-perez-molina
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/guatemala_3_contagioradio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: soy502guatemala 

###### [1 Sep 2015]

El congreso de **Guatemala decidió esta tarde levantar la imunidad que cobijaba al presidente de ese país Otto Pérez Molina**, quien será investigado por los escándalos de corrupción conocidos como "la línea". Por primera vez en la historia de Guatemala el **Congreso le retira la inmunidad a un Presidente** electo por voto popular para que la justicia lo investigue.

El Congreso con 132 votos le retiró  la inmunidad al presidente Pérez Molina, ahora el **Ministerio Público y la Comisión internacional Contra la Impunidad en Guatemala tiene vía libre para investigar** al mandatario por su presunta participación en la red de defraudación denominada La Línea.

Por el mismo caso ya se encuentra detenida la ex vicepresidenta Rosanna Baldetti y otros 12 funcionarios del alto gobierno. Recientemente han renunciado 7 ministros y se prevee que se abran investigaciones formales y procesos penales contra 20 integrantes del congreso de ese país.

En la situación actual, si el Ministerio Público y el CICIG así lo resuelven podría emitirse una orden de captura en contra del presidente. Cabe recordar que según las investigaciones preliminares y sus resultados tanto Pérez Molina como Baldetti son responsables de altos niveles de defraudación en la **Superintendencia de Administración Tributaria.**

Vea también [Miles de personas exigen la renuncia de Otto Perez Molina](https://archivo.contagioradio.com/a-una-sola-voz-guatemala-exige-la-renuncia-de-otto-perez-molina/)
