Title: ¿Ya hay una decisión de la CAR sobre la reserva Van Der Hammen?
Date: 2018-10-01 16:31
Author: AdminContagio
Category: Ambiente, Nacional
Tags: CAR, Mercedes Maldonado, Reserva Thomas Van der Hammen
Slug: ya-hay-una-decision-de-la-car-sobre-la-reserva-van-der-hammen
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/02/Reserva-Thomas-Van-der-Hammen-e1455219288852.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo Partícular] 

###### [01 Oct 2018]

Ambientalistas rechazaron el comunicado de prensa que de la C**orporación Autónoma Regional** en la que invitan a la jornada de participación para abordar los procesos de realinderación, recategorización y sustracción de la reserva Thomas Van Der Hammen, y advirtieron que podría significar que **ya hay una decisión desde la dirección de la CAR que daría vía libre a la construcción del complejo Lagos de Torca. **

\[caption id="attachment\_57202" align="alignnone" width="479"\]![Twitter: Manuel Rodríguez](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/10/car-479x390.jpg){.size-medium .wp-image-57202 width="479" height="390"} Twitter:  @manuel\_rodb\[/caption\]

Según María Mercedes Maldonado, integrante de las veedurías ciudadanas, la dirección de la CAR  ya tendría una decisión, y con esta invitación a la audiencia solo estaría  adelantándose al mecanismo de participación ciudadana de una manera irregular,** **ya que solo hay un proceso que puede ponerse en marcha en este momento, la sustracción, mientras que los otros dos, aún afrontan vacíos jurídicos.

Además, existe una** constancia en la que se afirma que aún no se encuentran los requisitos para hacer la realinderación**. (Le puede interesar:["¿Qué le espera a la reserva Thomas Van Der Hammen?](https://archivo.contagioradio.com/reserva-thomas-van-der-hammen-y-su-futuro/)")

De igual forma, Maldonado señaló que aún no se conocen los términos para ejecutar una realinderación por exclusión, debido a que el Ministerio de Ambiente no ha expedido la reglamentación de carácter legal que lo define y **sin este concepto la CAR tampoco podría empezar a realizar este proceso en la Reserva. **

La ambientalista expresó que este anunció les llegó por sorpresa, debido a que esta información no esta publicada en ningún periódico, dificultando la inscripción de la ciudadanía que quiera participar en las audiencias. (Le puede interesar: ["Cabildo indígena Muisca asegura que Van Der Hammen es territorio sagrado"](https://archivo.contagioradio.com/van-der-hammen-terreno-indigena/))

<iframe id="audio_29007181" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_29007181_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en] [[su correo] ][[Contagio Radio](http://bit.ly/1ICYhVU)]
