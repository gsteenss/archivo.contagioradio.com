Title: Continúan construcciones en zonas de conservación de Cerros Orientales
Date: 2016-10-31 12:39
Category: Ambiente, Nacional
Tags: cerros orientales, Constructoras
Slug: cerro-de-los-alpes-golpe-al-ecosistema-de-cerros-orientales
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/10/CERROS-DE-LOS-ALPES-ARQUITECTURA-Y-CONCRETO-6-e1477934986558.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: ImaginaBogota] 

###### [31 Oct 2016] 

Pese a que hay diferentes restricciones para construir en los Cerros Orientales por el daño ambiental que esto genera a diferentes ecosistemas, **el Ministerio Ambiente, Vivienda y Desarrollo permitió la construcción de un complejo de apartamentos** llamado Cerro de los Alpes, en la zona de Usaquén que invade el territorio destinado para proteger y conservar.

La construcción habría obtenido las licencias bajo una orden del **Ministerio de Ambiente, Vivienda y Desarrollo que determinó que los previos en donde se construirían los edificios seguían siendo zonas urbanas**. Además habría solicitado a  la Oficina de Registro e Instrumentos Públicos la cancelación de la afectación de la reserva, para que se diera paso a la construcción.

La edificación fue hecha por la Constructora Arquitectura y Concreto, detrás del Centro Comercial Paseo Real, en la calle 121 \#3A. En principio los edificios estaban en el límite entre los Cerros Orientales y el perímetro de urbanización, sin embargo en los últimos meses aparecieron **dos torres más que se pasan ese límite y se encuentran edificadas en la zona de protección de los Cerros Orientales.**

María Mercedes Maldonado, ex secretaria de Hábitat y Vivienda, denuncia que esto se dio bajo la segunda administración del ex presidente Uribe y que lo más importante es la **la afectación en términos de una ciudad con menos áreas que disfrutar y la perdida de las montañas. **

“Debe haber un control ciudadano y una vigilancia, nosotros actuaremos como veeduría y finalmente **una movilización social, porque de lo contrario vamos a perder los Cerros, la reserva Van Der Hammen, los Humedales, no perdamos de vista estos temas ambientales**” afirmó Maldonado.

No obstante, aunque hay autoridades como el Tribunal Administrativo de Cundinamarca que dejo en firme la decisión de recuperar para la conservación ambiental en su totalidad de los límites originales de la reserva de los Cerros Orientales, y lo reitero el Consejo de Estado en el año 2013, a través de una sentencia, siguen avanzando construcciones en los Cerros como es el caso de Bagazal Chapinero y este nuevo caso de Cerro de los Alpes sin que se ponga un freno o impedimento para continuar con las edificaciones. Le puede interesar: ["Construcción de cinco viviendas de lujo tiene en riesgo Cerros Orientales de Bogotá"](https://archivo.contagioradio.com/construccion-de-cinco-viviendas-de-lujo-tienen-en-riesgo-cerros-orientales-de-bogota/)

<iframe id="audio_13555284" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_13555284_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en su correo [[bit.ly/1nvAO4u]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por Contagio Radio [[bit.ly/1ICYhVU]{.s1}](http://bit.ly/1ICYhVU)
