Title: 74 municipios dejados por las FARC han sido copados por paramilitares
Date: 2017-07-18 17:27
Category: Paz, Política
Tags: acuerdos de paz, Cese al fuego, Dejación de armas, FARC, implementación de los acuerdos, Unión por la Paz
Slug: 74-municipios-dejados-por-las-farc-han-sido-copados-por-estructuras-paramilitares
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/08/paramilitares-e1472682962275.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Zona Cero] 

###### [18 Jul. 2017]

Según el informe de la inciativa Unión por la Paz, son 74 municipios, en los que existían las FARC-EP que han sido copados por estructuras de tipo paramilitar. Afirman también que el 90% del acuerdo del gobierno y esa guerrilla beneficia a la población colombiana en general y ese porcentaje no se ha implementado.

**En la entrega de su segundo informe ¿Cómo va la paz?**,  se analizaron los principales avances, problemas y retos que se han identificado en el último trimestre a propósito de la fase de implementación de los Acuerdos de Paz firmados entre el Gobierno Nacional y la guerrilla de las FARC.

**El informe de 142 páginas arrojó 7 conclusiones** en las que se destacan importantes avances. Una de ellas que incluye como un acontecimiento enorme y positivo del trimestre la dejación de 7.132 armas por parte de 6.800 guerrilleros a la Organización de las Naciones Unidas – ONU -.

Según León Valencia, Director de la Fundación Paz y Reconciliación manifestó **"es importante reconocer que lo que se les pide a las guerrillas es que dejen las armas** y eso lo tuvimos este trimestre".

En ese sentido, el ex presidente Ernesto Samper, integrante de la organización, aseguró que es importante reconocer que “las FARC han cumplido su palabra de una manera absolutamente admirable”.

1.  ### **La dejación de armas es un hito para la historia de Colombia**

Según el informe **el hecho más destacado en el trimestre fue la finalización del proceso de dejación de armas** de la dotación individual de los miembros de las Farc el cual se dio el 27 de junio y en el que las Naciones Unidas certificó la dejación de 7.132 armas entregadas por 6.800 guerrilleros.

Una de las conclusiones a las que se llegó es que **hay “más armas que guerrilleros, un hecho sin precedentes en todos los procesos de paz** que se han llevado a cabo en el mundo”.

Y ponen como ejemplo a Afganistán, del cual se tienen cifras de entrega de armas durante el proceso de paz de un total de 63.000 desmovilizados y 47.575 armas, es decir, 0.76 armas por desmovilizado, por lo que manifiestan que “de ahí que la dejación de armas de las FARC sea un hito porque la relación entre guerrillero y arma es de uno a uno, cifra que seguramente va a aumentar una vez termine el proceso de recuperación de las armas ubicadas en las caletas”.

2.  ### **Cese al fuego durante el último trimestre**

La evaluación realizada en cuanto al cese bilateral al fuego manifiesta el documento “es sumamente positivo” dado que **desde mediados de 2016 Colombia ha salvado más de tres mil personas de morir** o quedar heridas en acciones relacionadas con el conflicto armado”.

Por lo que destacan la reducción de la violencia en 2016 dado que en los 281 municipios priorizados para el posconflicto se pasó de 3.507 homicidios a 3157 y los secuestros descendieron hasta llegar a cero. Le puede interesar: [CIDH realiza audiencia sobre Jurisdicción Especial de Paz y "terceros en la guerra"](https://archivo.contagioradio.com/empresas-financiacion-paramilitares-colombia/)

3.  ### **Preocupación por cooptación de zonas dejadas por las FARC**

Aunque el informe muestra la reducción de los homicidios en algunas regiones, el panorama pareciera no aclarar y por el contrario preocupar a Unión por la Paz, puesto que **las zonas donde antes operaban las FARC han comenzado a ser copadas por “organizaciones criminales,** la guerrilla del ELN o sencillamente se presenta una situación de anarquía criminal”.

Destacan municipios como Tumaco en el departamento de Nariño, donde se ha elevado la violencia homicida por la **falta de presencia institucional en los 242 municipios que dejaron las FARC.**

Estos lugares del país que han sido conocidos como zonas postfarc, son ocupados por la guerrilla del ELN (12 municipios), por Grupos Armados Organizados (74 municipios) de los cuales 18 de ellos han sido producto de expansión debido al repliegue de las Farc.

Por otro lado, se encuentran las 16 zonas donde hacen presencia “disidencias” de las FARC y asevera el informe los demás lugares son “Zonas de anarquía criminal (delincuencia común, violencia intrafamiliar, violencia de género, entre otras) y Zonas en proceso de consolidación estatal (es decir, donde hará presencia institucional el Estado)”. Le puede interesar: [Organizaciones sociales realizan balance de la implementación de los acuerdos de paz](https://archivo.contagioradio.com/organizaciones-sociales-realizan-balance-de-la-implementacion-de-los-acuerdos-de-paz/)

4.  ### **Situación de los defensores y defensoras de derechos humanos sigue siendo crítica**

Para este segundo informe, se sigue dando cuenta de la **grave situación de vulnerabilidad a la que se ven expuestos líderes y defensores y defensoras** de derechos humanos en Colombia manifestando que hay un comportamiento con grados altos de sistematicidad.

“Las motivaciones de estas acciones estarían encaminadas en limitar la participación de líderes sociales en política, entorpecer procesos de construcción de verdad, restitución de tierras y defensa del medio ambiente” reza el informe

De acuerdo a la Fundación Paz y Reconciliación y al monitoreo realizado a través de su observatorio de violencia política, **desde noviembre de 2016 y en lo que va del 2017 se han registrado un total de 181 hechos** victimizantes contra líderes sociales y defensores de derechos humanos. Entre estos hechos 55 homicidios, es decir, que cada cuatro días se comete un homicidio contra un líder social en Colombia y cada dos, hay uno amenazado.![Infografia](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/07/Infografia.png){.alignnone .size-full .wp-image-43891 .aligncenter width="539" height="683"}

5.  ### **En riesgo la implementación normativa de los Acuerdos de Paz**

Dice León Valencia que se hace necesaria la conformación de los tribunales de la Jurisdicción Especial de Paz – **JEP- “porque es un tema clave para la reconciliación,** puesto que la mayoría de leyes y decretos promulgados hasta el momento favorecen a los actores directos del conflicto”.

Añade el documento que las leyes de amnistía, la Jurisdicción Especial de Paz, la reincorporación política y los voceros de paz que se pactaron, están encaminadas a favorecer directamente a los actores centrales del conflicto. Le puede interesar: ["Investigación destaca aporte de las mujeres en el proceso de paz de la Habana"](https://archivo.contagioradio.com/?p=43151&preview=true)

Así mismo la organización manifiesta que el debate sobre Circunscripciones Especiales de Paz “mostró que **los partidos pueden retrasar o incluso hundir las leyes para presionar al gobierno y preparar sus campañas electorales del 2018.** La próxima legislatura será determinante para que se aprueben leyes que están en trámite como las de innovación agropecuaria, adecuación de tierras y CTEP. Sin embargo, la cercanía de las elecciones y las posiciones aparentemente ambiguas de partidos como Cambio Radical y el Conservador sobre lo que queda por aprobar suponen un riesgo para la implementación normativa”.

6.  ### **Avances en materia de seguridad**

Se comienzan a ver avances institucionales en seguridad, que según el informe se ve reflejado en mayor despliegue de Fuerzas Militares como la Policía Nacional en las zonas donde estaban las FARC.

El despliegue se ha hecho a través de efectivos y de transformaciones internas para crear comandos que puedan trabajar para desmantelar a las organizaciones criminales responsables de homicidios y masacres, que vienen atentando contra defensores de los derechos humanos y líderes sociales y políticos.

Sin embargo, pese a las cifras que entrega el informe, **los cambios no se han visto en las regiones, donde hasta la fecha hay 52 defensores y defensoras asesinadas** sin que hasta la fecha existan investigaciones que hayan dado con los responsables materiales e intelectuales de esos hechos.

“No es suficiente reforzar la fuerza pública, es la oportunidad para construir una institucionalidad local más robusta, que genere confianza y pueda reconocerse como autoridad para el trámite de los conflictos”.

7.  ### **Apenas va el 10% de las acciones a emprender del Acuerdo de Paz**

Para Unión por la Paz los acuerdos de La Habana se pueden dividir en dos grupos, en la que un 10% hace referencia a los actores de la guerra y el otro 90% han sido pensados para beneficiar a la sociedad en general. A la fecha se ha alcanzado solo un 10%.

Por lo que sugieren que **el restante 90% debe estar enfocado a trabajar programas, planes y acciones para superar las condiciones estructurales de la violencia** “aún hace falta trabajar de manera más rigurosa para que las expectativas de transformación de los territorios más olvidados puedan materializarse en acciones de política pública concretas”.

Por último, recuerdan que para trabajar por la transformación de los territorios se debe contar con presupuesto, capacidad institucional administrativa, jurídica y técnica en los territorios, y voluntad política. Le puede interesar: [En Septiembre se conocerán los nombres de magistrados del Tribunal de Paz](https://archivo.contagioradio.com/en-septiembre-se-conoceran-los-nombres-de-magistrados-del-tribunal-de-paz/).

### **Los pasivos ambientales del posconflicto** 

Aunque este tema se mencionó en el evento y no aparece detallado en el informe de la organización, Ariel Avila afirmó que si **existe una preocupación por la incapacidad del Estado para proteger los recursos naturales que las FARC protegieron** durante años en las zonas en que ejercieron control.

Uno de los casos mencionados es en el departamento del Guaviare, donde se denuncia que cai 7 mil hectáreas de bosques han sido tumbadas por cuadrillas de colonos ante la vista impotente de los habitantes de la región y sin que el Estado o las FFMM apliquen las medidas para evitar un desastre ambiental anunciado. El año pasado, según el IDEAM, **el país perdió 178.597 hectáreas de bosque, lo que representa un aumento del 44 % en comparación al 2015.**

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU).

<div class="osd-sms-wrapper">

</div>

[Informe Cómo va la Paz - Unión por la Paz](https://www.scribd.com/document/354108929/Informe-Como-va-la-Paz-Union-por-la-Paz#from_embed "View Informe Cómo va la Paz - Unión por la Paz on Scribd") by [Contagioradio](https://www.scribd.com/user/276652802/Contagioradio#from_embed "View Contagioradio's profile on Scribd") on Scribd

<iframe id="doc_79145" class="scribd_iframe_embed" src="https://www.scribd.com/embeds/354108929/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-1GgZ7t3R4LLJUxr8wAKB&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="0.7729220222793488"></iframe>
