Title: Arte urbano, un espejo crítico del contexto histórico en Latinoamérica
Date: 2020-12-04 17:58
Author: CtgAdm
Category: Actualidad, Cultura
Tags: arte, Arte Urbano, Muralismo, Quien dio la orden
Slug: arte-urbano-espejo-critico-del-contexto-historico-en-latinoamerica
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/12/Diseno-sin-titulo-6.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","fontSize":"small"} -->

Foto: Arte @tavogaravato

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Colectivos y artistas buscan transmitir su lectura de la realidad en medio de un momento en la historia en el que la movilización social ha cobrado fuerza en toda Latinoamérica y en que en particular en el contexto nacional, las violaciones a los DD.HH. continúan y estas buscan ser denunciadas a través de expresiones como el muralismo, el cartelismo y en general el arte urbano.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### La galería de arte más importante es la calle

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Desde el Colectivo Beligerarte, un proceso que surge a partir de estudiantes de sociología, ciencias políticas y humanas que vieron la necesidad de tomarse las calles con la intención de hacer del arte callejero una herramienta comunicativa frente a una realidad compleja [(Lea también: Mural ¿quién dió la órden? le pertenece a la gente: MOVICE)](https://archivo.contagioradio.com/mural-quien-dio-la-orden-le-pertenece-a-la-gente-movice/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Para el colectivo, la calle termina siendo "el único espacio que nos queda para apropiarnos, es evidente que no hay una posibilidad de representación política ni en los grandes medios de comunicación y en ese sentido la calle termina siendo el último lugar al que nos han empujado", un lugar en el que señala, pueden exponer su lectura y pensamientos frente a la realidad que acontece.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Por su parte, **Gustavo Bernal, uno de los promotores de La Causa Latinoamericana,** una exposición de cartelismo y activismo gráfico que aborda el tema de las protestas sociales en América Latina señala que la idea es replicar esta idea a diferentes ciudades del continente siempre con la intención de exigir el fin de la corrupción, el acceso a la educación y la salud, y derechos fundamentales.

<!-- /wp:paragraph -->

<!-- wp:core-embed/instagram {"url":"https://www.instagram.com/p/CAicPzKJhos/","type":"rich","providerNameSlug":"instagram","className":""} -->

<figure class="wp-block-embed-instagram wp-block-embed is-type-rich is-provider-instagram">
<div class="wp-block-embed__wrapper">

https://www.instagram.com/p/CAicPzKJhos/

</div>

</figure>
<!-- /wp:core-embed/instagram -->

<!-- wp:paragraph {"align":"justify"} -->

Otros colectivos, como **[Brandalismo](https://www.instagram.com/brandalismocol/)**, una idea que surge en 2011 en Londres como respuesta a las agencias de la publicidad, y que llegó a Colombia en 2019, afirman que su trabajo es un espejo crítico que busca llevar un mensaje mediante el sabotaje de marcas nacionales e internacionales a través de la estética.

<!-- /wp:paragraph -->

<!-- wp:quote -->

> "Si el arte no tiene un mínimo de mensaje social es tan solo decoración"

<!-- /wp:quote -->

<!-- wp:paragraph {"align":"justify"} -->

Del mismo modo, el **Colectivo Guarichas** que trabaja desde México, Argentina y Colombia busca denunciar la violación de DD.HH. que ocurre en Colombia en el marco de una continuidad del conflicto armado a otras regiones como una apuesta de resistencia popular el demasiado más grande resulta condensar en una pieza un mensaje y ha sido el gran reto de lo colectivo de lo que implica hacer piezas colectivas y cómo compaginar la denuncia con la esperanza.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### El riesgo de llevar un mensaje a través del arte

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Desde el colectivo Guaricha afirman que se trata de entender el espacio público y poner a disposición de todas las consecuencias privadas de la guerra y la huella que esta deja en los cuerpos y en las mentes, "en este momento en Colombia, el riesgo es respirar, la continuidad de la guerra nos está poniendo a todos en riesgos en cualquier escenario" afirma la integrante del colectivo resaltando el cómo se ha usado el aparato político para perseguir la protesta y la movilización.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Pese a que en la actualidad pesa sobre el mural de 'Quién dio la Orden' - que expone a los miembros de la Fuerza Pública vinculados a las ejecuciones extrajudiciales - un fallo de la Corte Constitucional que prohíbe su reproducción a raíz de las tutelas interpuestas por miembros del Ejército Nacional, sus creadores señalan que lo interesante de lo ocurrido es que la imagen ya pertenece y es patrimonio de la sociedad pese al intento de censura.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

"Hay un punto en el que era inevitable la reproducción masiva de la imagen" expresa el artista respecto a la respuesta de apropiación de la imagen que permitió que tuvieran un impacto sobre su mayor difusión [(Le puede interesar: Con tutelas, altos mandos militares quieren censurar mural sobre 'falsos positivos')](https://archivo.contagioradio.com/con-demandas-altos-mandos-militares-quieren-censurar-mural-sobre-falsos-positivos/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

**"En los barrios, en las comunidades, en los territorios donde el conflicto armado ha sido más fuerte el riesgo es, en realidad el riesgo que asumimos en una apuesta como la de 'Quién dio la orden' viene siendo un riesgo más"** expresa Beligerarte, agregando que a su vez existe la posibilidad de construir imágenes en las "que no solo expresamos lo que sentimos sino logramos ciertos niveles de presentación en sectores de víctimas y en grandes masas de la población apostando a la transformación social".

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->
