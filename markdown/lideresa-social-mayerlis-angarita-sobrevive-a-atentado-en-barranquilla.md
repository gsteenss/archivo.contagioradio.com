Title: Lideresa social Mayerlis Angarita sobrevive a atentado en Barranquilla
Date: 2019-05-19 18:40
Author: CtgAdm
Category: Líderes sociales, Nacional
Tags: Atentado contra líderes sociales, Mayerlis Angarita, montes de maría
Slug: lideresa-social-mayerlis-angarita-sobrevive-a-atentado-en-barranquilla
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/Mayelis-Angarita.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Narrar para vivir] 

**Mayerlis Angarita, directora del colectivo Narrar para Vivir**,  organización que trabaja junto a 840 mujeres víctimas del conflicto armado en Montes de María fue víctima de un ataque mientras se desplazaba a bordo de su esquema de seguridad en la ciudad de Barranquilla. La activista, quien está fuera de peligro había denunciado recientemente amenazas en su contra.

Los hechos ocurrieron el pasado sábado 18 mientras la lideresa se movilizaba en su esquema de seguridad junto a sus dos hijas y su sobrino cuando hombres armados dispararon contra el vehículo que se desplazaba por el corredor portuario de Barranquilla con destino a **San Juan Nepomuceno, Bolívar, lugar del que es origininaria y donde actualmente vive.**

Sobre el ataque, la **Unidad Nacional de Protección, (UNP)** no descartó que se podría tratar de un intento de atraco pero manifestó que son necesarias tomar medidas para esclarecer el hecho. Mayerlis Angarita ha sufrido dos atentados contra su vida en 2012 y 2015.  [(Lea también: Visibilizar para proteger: la estrategia de líderes sociales)](https://archivo.contagioradio.com/visibilizar-para-proteger-la-estrategia-de-lideres-sociales/)

Uno de los desconocidos que atacó el vehículo fue herido por uno de los escoltas de Mayerlis y fue capturado, siendo trasladado a un centro asistencial donde actualmente es vigilado.

Narrar Para Vivir nace como una respuesta al conflicto armado en Montes de María,  generando resistencia civil entre cientos de mujeres: promoviendo la palabra, el acompañamiento psicosocial y reconstruyendo el tejido social como gestoras de paz.

Noticia en desarrollo...

 

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
