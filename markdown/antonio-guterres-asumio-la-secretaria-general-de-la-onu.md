Title: Antonio Guterres asumió la Secretaría General de la ONU
Date: 2017-01-01 19:46
Category: DDHH, Nacional
Tags: Antonio Guterres, ONU
Slug: antonio-guterres-asumio-la-secretaria-general-de-la-onu
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/01/antonio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: América TV] 

###### [1 Ene 2017] 

A partir del pasado primero de enero, Antonio Guterres asumió la secretaría general de la Organización de las Naciones Unidas. En su primer discurso Guterres hizo un llamado para que todos los países del mundo conviertan este 2017 en un año de paz. De igual forma afirmó que "**de las guerras nadie sale vencedor, todos pierden" haciendo un llamado a la reflexión sobre los más afectados de los conflictos: la población civil.**

Durante su alocución, el secretario reiteró que su labor se enfocará en la paz, su desarrollo y los derechos humanos y aseguro que “**el Secretario General no es el líder del mundo, su misión es de facilitador, el trabajo fundamental corresponde a los Estados miembros, y yo estaré aquí para apoyarlos”. **

Guterres asume este cargo en medio de múltiples conflictos armados que continúan en el mundo, como es el caso de Siria o Palestina e Israel, y la veeduría en la implementación de acuerdos de paz como en Colombia, en donde asumió una importante labor **en la vigilancia del cumplimiento de los acuerdos entre el gobierno y la guerrilla de las FARC-EP. **[Le puede interesar: "ONU declará ilegales las colonias Israelíes en territorio Palestino"](https://archivo.contagioradio.com/onu-declara-ilegales-las-colonias-israelies-territorio-palestino/)

El diplomático que será el predecesor del surcoreano Ban Ki-Moon, es oriundo de Portugal, miembro del Partido Socialista de este país, fue presidente de turno del Consejo Europeo y estuvo en frente de la Internacional Socialista entre los años 1999 al 2006. A partir del año 2005, Guterres se desempeñó al interior de la ONU como Alto Comisionado para los refugiados.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio.](http://bit.ly/1ICYhVU) 
