Title: Se mantiene condena contra ganaderos por Asesinato de Jesús María Valle
Date: 2018-08-17 13:19
Author: ContagioRadio
Category: Política, Sin Olvido
Tags: Alvaro Uribe, Jesús María Valle
Slug: crimen-jesus-maria-valle
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/08/14200imagen6.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: [[www.iejesusmariavallejaramillo.edu.co]

###### 17 Ago 2018 

Pese a las solicitudes presentadas por su defensa, la condena de treinta años de prisión contra los **hermanos Francisco y Jaime Angulo Osorio**, ratificada en 2017 por su vinculación con las masacres de La Granja, de El Aro, y como determinadores **del asesinato del defensor de los derechos humanos Jesús María Valle**, seguirá en firme.

A los ganaderos, el Tribunal Superior de Medellín les atribuyó **responsabilidad 'no exclusiva' **en caso del asesinato de Valle; decisión que sus abogados pretendieron detener este año, **aludiendo que este ya había sido juzgado y determinado**; petición que la Corte Suprema de Justicia negó, ratificando la condena por homicidio agravado y concierto para delinquir.

**Sobre el caso**

En el año 2001 los hermanos Angúlo fueron absueltos, posteriormente **en 2006 el Estado colombiano fue condenado por parte de la Corte Interamericana de Derechos humanos (CIDH) por las dos masacres,** lo que obligo a que se hiciera revisión del caso y por ende del asesinato del abogado relacionado directamente con el mismo proceso.

Allí se encontraron vínculos no solo con los reconocidos comerciantes y ganaderos de Ituango, **también con miembros de la fuerza pública y aun en indagación al senador y ex presidente Álvaro Uribe Vélez**. Hasta 2011 estuvo vigente la supervisión y el cumplimiento de la sentencia dictaminada por la CIDH, al Estado. En 2017 los hechos fueron reconocidos como Crímenes de Lesa Humanidad.

**Una vida por los derechos humanos**

Jesús María Valle, fue un firme defensor de los derechos humanos, un líder polifacético comprometido con su trabajo por la búsqueda de la justicia y la paz. **Denuncio acciones del paramilitarismo cometidas especialmente en el municipio de Ituango como fueron las masacres de La Granja y El aro, en 1997.**

El 27 de febrero de 1998, hombres armados en la ciudad de Medellín lo asesinaron en su oficina. **Hoy persiste la lucha por la verdad, la justicia y la memoria de Jesús María Valle** (Le puede interesar: [Jesús María Valle, El apóstol de los Derechos Humano](https://archivo.contagioradio.com/la-lucha-contra-la-impunidad-del-asesinato-de-jesus-maria-valle/)s).

###### Reciba toda la información de Contagio Radio en [[su correo]
