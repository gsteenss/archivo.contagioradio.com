Title: Segundo capitulo Partículas de Libertad
Date: 2020-08-12 11:36
Author: AdminContagio
Category: Expreso Libertad, Programas
Tags: Expreso Libertad, montaje judicial, montajes judiciales, radionovela
Slug: segundo-capitulo-particulas-de-libertad
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/08/Presentación-de-Publicidad-Amarillo-y-Azul.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/08/Presentación-de-Publicidad-Amarillo-y-Azul-1.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

Esta 11 de Agosto, el **Expreso Libertad** lazó el segundo capítulo de su radionovela **«Partículas de Libertad»**  titulado:** Las cadenas de la inocencia.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En este segundo capítulo los 5 jóvenes son trasladados a un juzgado en donde se les realizará la imputación de cargos por un delito que no cometieron. Suman y restan los años de su posible condena, con un abogado que les insisten en afirmar que son culpables y aceptar la pena.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Mientras tanto, las y los jóvenes, siguen atando cabos sobre el posible montaje creado por su compañero Raúl, ¿estará detrás de estos hechos? y de ser así ¿por qué lo hizo?

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Para escuchar el anterior capítulo puedes ingresar al siguiente link: [Partículas de libertad, una radionovela sobre los montajes judiciales en Colombia](https://archivo.contagioradio.com/particulas-de-libertad-una-radionovela-sobre-los-montajes-judiciales-en-colombia/)

<!-- /wp:paragraph -->

<!-- wp:html -->  
<iframe id="audio_55217023" frameborder="0" allowfullscreen scrolling="no" height="200" style="border:1px solid #EEE; box-sizing:border-box; width:100%;" src="https://co.ivoox.com/es/player_ej_55217023_4_1.html?c1=ff6600"></iframe>  
<!-- /wp:html -->

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/video.php?href=https%3A%2F%2Fwww.facebook.com%2Fwatch%2F%3Fv%3D2697968650479438&amp;show_text=false&amp;width=734&amp;height=413&amp;appId" width="734" height="413" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>
