Title: Asesinan abogado de la Defensoría del Pueblo William García
Date: 2016-09-16 12:43
Category: DDHH, Nacional
Tags: Asesinatos, FARC, lideres sociales, Plebiscito
Slug: asesinan-abogado-de-la-defensoria-del-pueblo-william-garcia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/09/defensoría-del-pueblo-e1474047687809.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Defensoría del Pueblo 

###### 16 Sep 2016

Este jueves fue asesinado por sicarios el abogado de la **Defensoría del Pueblo William García de Segovia, Antioquia.** Según la información el hecho se perpetró en el barrio Santa Fe de Medellín, a manos de hombres que usaban armas con silenciador.

Rubén Darío Gómez, secretario de la Confederación de Mineros de Colombia y amigo del defensor de derechos humanos asesinado, afirma que García era un hombre comprometido con los derechos de las comunidades, además **acompañaba a víctimas de grupos paramilitares**, estaba al servicio de la Defensoría del Pueblo y defendía la informalidad de las personas que viven de la minería artesanal en los municipios de Segovia y Remedios.

De acuerdo con Gómez, **desde 1997 William García estaba siendo amenazado, intimidado y perseguido por grupos paramilitares** de la zona, a causa de su trabajo con comunidades víctimas. Además, hace algunos meses desde la Mesa Minera se había denunciado otras amenazas no solo contra él, sino contra líderes de la región que actualmente se encuentran haciendo campaña para el Sí en el plebiscito por la paz del próximo 2 de octubre.

“No descartamos que ese sea el origen del asesinato”, dice Rubén Darío, quien agrega que, “El tipo de operativo para acabar con la vida de William, muestra que **estaría relacionado con grupos al margen de la ley**”.

Los amigos y familiares del abogado reclaman a las autoridades una investigación que pueda dar con el paradero de los responsables de este homicidio, pues aseguran que con los últimos hechos de [asesinatos contra 11 líderes sociales en todo el país](https://archivo.contagioradio.com/en-menos-de-un-mes-han-sido-asesinados-11-lideres-sociales-en-colombia/), se fragmenta el entorno de confianza frente al posconflicto, pues cabe resaltar que la **vereda Carrizal, cercana al sitio del asesinato de García, hace parte de una de los zonas veredales transitorias** de los combatientes de las FARC.

Por el momento las autoridades no se han manifestado, aunque se espera que haya un pronunciamiento por parte del gobierno departamental y nacional, además, según cuenta Gómez, “Ni la Defensoría (donde trabajaba el líder) ha dicho algo sobre el hecho”.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
