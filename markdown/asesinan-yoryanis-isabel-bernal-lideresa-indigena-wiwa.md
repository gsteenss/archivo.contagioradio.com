Title: Asesinan a Yoryanis Isabel Bernal, lideresa indígena Wiwa
Date: 2017-01-30 17:07
Category: DDHH, Nacional
Tags: lideres sociales asesinados, proceso de paz
Slug: asesinan-yoryanis-isabel-bernal-lideresa-indigena-wiwa
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/01/mujer-asesinada-en-valledupar-era-lider-wiwa-defensora-de-derechos-humanos.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: El Heraldo] 

###### [30 Ene 2017] 

Los asesinatos contra mujeres defensoras de derechos humanos no cesan, luego de que hace 15 días apareciera sin vida el cuerpo de una lideresa de CONPAZ, esta vez, las comunidades de la Sierra Nevada de Santa Marta, lloran el asesinato de **Yoryanis Isabel Bernal Varela, defensora de los derechos de la mujer Wiwa**, e integrante de esa misma comunidad indígena.

De acuerdo con las versiones de algunos testigos, el pasado jueves en cercanías al barrio Lorenzo en Valledupar, la lideresa habría sido abordada por personas que se movilizaban en una motocicleta quienes la intimidaron y con arma de fuego le dispararon en la cabeza.

Según la organización Wiwa Golkuche del reguardo Kowi, Malayo y Arhuaco, Yoryanis Isabel fue acompañante en todos los procesos de la etnia Wiwa en temas de **derechos de las mujeres indígenas**. La organización, también señala que las comunidades vienen siendo objeto de amenazas e intimidaciones.

José de los Santos Sauna, gobernador Arhuaco, Kogui y Wiwa, expresó **“Nos quitaron a una gran líder y cuando esto sucede nuestra cultura se baja** porque no hay mucha gente valiente para enfrentar nuestros problemas de orden público que siempre son peligrosos".

Con el asesinato de esta lideresa indígena, se cuentan 9 en lo que va del 2017. **Lo que da un total de 119 homicidios contra defensores de derechos humanos en el país,** desde que se firmó el acuerdo de paz con la guerrilla de las FARC. Mientras tanto, el Ministro de Defensa, Luis Carlos Villegas, continúa asegurando que los asesinatos son hechos aislados y sigue negando la vigencia del paramilitarismo en el país.[(Le puede interesar: asesinana a lideresa del CONPAZ en Buenaventura)](https://archivo.contagioradio.com/asesinan-a-lideresa-de-conpaz-y-a-su-companero-en-buenaventura/)

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
