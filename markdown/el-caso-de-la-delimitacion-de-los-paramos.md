Title: La participación ciudadana no puede seguir siendo un mero formalismo: el caso de la delimitación de los páramos
Date: 2019-08-26 15:02
Author: Foro Opina
Category: Opinion
Tags: delimitación de los páramos, Ministerio de Ambiente, Páramo de Santurbán
Slug: el-caso-de-la-delimitacion-de-los-paramos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/11/DOr8Qg0XkAEgTOw-e1510857171884.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: ADN Bucaramanga 

Hizo bien la C**orte Constitucional al reconocer que se vulneró el derecho a la participación en la delimitación del páramo de Santurbán** cuando el Ministerio de Ambiente y Desarrollo Sostenible expidió la Resolución 2090 de 2014 y, al señalar que se incumplió la garantía a una participación ciudadana eficaz relacionada con el acceso a la información, a la deliberación pública y a la construcción de consensos para la sanción de la mencionada resolución.

La Sentencia T-361 de 2017 obligó al Ministerio a hacer un alto en el camino que no sin dificultades había emprendido para cumplir con el mandato de delimitar formalmente los páramos del país. El problema de fondo radica en que ni el Ministerio de Ambiente ni la mayoría de órganos del Estado han comprendido qué significa dar voz a la ciudadanía y hasta dónde llegar para alcanzar ese complejo balance entre la democracia participativa y la representativa. Por mucho que se avance en la normatividad sobre la importancia de la participación no es suficiente si se la continúa abordando como un asunto residual o con temor y desconocimiento sobre cómo hacerla efectiva para la toma de decisiones públicas.

Dicha providencia estableció una ruta específica para la delimitación del páramo de Santurbán en el marco de un procedimiento previo, amplio, participativo, eficaz y deliberativo. Para la Corte la eficacia y eficiencia de la participación aumenta al implementar etapas de convocatoria amplia, información, consulta e iniciativa, concertación, decisión, gestión y fiscalización.

Con este precedente, algunos trabajadores mineros del municipio de Socha (Boyacá) instauraron una tutela al Ministerio de Ambiente bajo el argumento de que no se les había tomado en cuenta en el trámite de delimitación del Páramo de Pisba, tal y como lo había planteado la Corte en el 2017. El Tribunal de Boyacá les dio la razón (que ya había sido dada en primera instancia por un Juzgado de Duitama) y determinó que era plenamente aplicable esa Sentencia, para lo cual dio plazo de un año al Ministerio para hacer la delimitación participativa y además declaró a dicho páramo como sujeto de derechos titular de protección, conservación, mantenimiento y restauración.

Con este mandato el Ministerio reinició la delimitación en abril para implementar las distintas fases que adoptó la Corte. Avanzaron con dos, de acercamiento e información, pero, lamentablemente, el efecto fue justo el contrario: se generó gran preocupación entre las comunidades, pujas al interior de ellas y se instaló un entorno de desconfianza principalmente por los procedimientos y métodos utilizados por el Ministerio ya que hubo denuncias por parte de algunos de haber sido excluidos de notificaciones. (Ver:[Grupos ambientales buscan prorrogar delimitación del Páramo de Santurbán](https://archivo.contagioradio.com/grupos-ambientales-buscan-prorrogar-delimitacion-de-paramo-de-santurban/))

Y en la fase informativa, el Ministerio asumió que con la realización de una sola audiencia pública por municipio lo daba por cumplido y pasaba a la siguiente fase de concertación, en la cual las comunidades deberían presentar ¡propuestas específicas para el plan de manejo, las actividades de cierre minero, sustitución o reubicación y sobre la definición de actividades productivas de alto y bajo impacto!

En estas fases no quedó despejado qué se podía negociar y qué no, por lo que no se abordaron las expectativas que la ciudadanía podía tener sobre este proceso. El título de sujeto de derechos no relució, y no se puede dejar de lado y asumirlo como un proceso similar al de Santurbán –que tampoco ha sido exitoso-. Pretender que lleguen con propuestas sin un proceso previo de formación y acompañamiento sobre los temas técnicos es una vulneración a la garantía de información suficiente y a la misma participación. En resumen, en la delimitación de Pisba se continuó operando bajo la concepción de la participación como un ejercicio sin mucho sentido que debe hacerse como requisito por cumplir.

Y para completar, recientemente fue expedido el auto 393 de 2019, de la Corte Constitucional, que declaró la nulidad de todas las actuaciones adelantadas hasta el momento y ordenó devolverlas al Juzgado Segundo de Duitama para rehacerlas, pero esta vez teniendo en cuenta a la **Agencia de Minería** quien había solicitado la revisión de la tutela por violación al debido proceso al no haberla incluido.

Esta situación se puede ver desde dos ópticas: en primer lugar, la cuestión del tiempo. ¿Cuánto más tendrá que tomarse el gobierno en la delimitación formal del páramo de Pisba y con ello se pueda resolver la incertidumbre sobre el futuro de sus habitantes y la tensión entre ellos? El plazo de un año se vencía en este mes de agosto, no obstante, estaba bastante lejos de lograrlo. En segundo lugar, a pesar del tiempo perdido, se podría advertir como una oportunidad para barajar nuevamente las cartas y apuntar a un proceso real participativo. La pregunta es: ¿está dispuesto el Ministerio de Ambiente a hacerlo? ¿Realmente le interesa?

Para que la participación deje de ser un “molesto” requisito y cumpla con su propósito, se requiere que los funcionarios la asuman como parte inherente de su labor, que al final de cuentas contribuye a una gobernanza democrática y al bienestar de la ciudadanía.

La prevención de la conflictividad socioambiental y la garantía de los derechos de los habitantes en la delimitación de los páramos se alcanzan, necesariamente, con la participación de la ciudadanía desde el principio para que tengan la oportunidad de familiarizarse, aceptar las repercusiones y los cambios que les implicará, brindando un entorno de confianza y credibilidad con el Estado.

Todos los actores deben sentirse “parte de”, en igualdad de condiciones y no como agentes receptores pasivos. Los procesos técnicos deben ser socializados y discutidos con ellos. Con tan solo este cambio de abordaje, los resultados se verían en menor tiempo, las poblaciones estarían más satisfechas y seguramente sí se avanzaría en una delimitación participativa de los páramos.

###### Ver más columnas de opinión de [La Fundación Foro Nacional Por Colombia](https://archivo.contagioradio.com/author/foro-opina/) 

#####  
