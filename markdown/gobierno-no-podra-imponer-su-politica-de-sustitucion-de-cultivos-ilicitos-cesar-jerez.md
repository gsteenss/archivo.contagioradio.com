Title: "Gobierno no podrá imponer su política de sustitución de cultivos ilícitos" César Jerez
Date: 2016-11-16 15:29
Category: DDHH, Nacional
Tags: #AcuerdosYA, cultivos ilícitos en Colombia
Slug: gobierno-no-podra-imponer-su-politica-de-sustitucion-de-cultivos-ilicitos-cesar-jerez
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/08/cultivos_de_coca_-e1470250623934.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Zona Cero] 

###### [16 Nov 2016] 

Diferentes analistas han expresado su preocupación frente al nuevo acuerdo de paz y el tercer punto sobre erradicación y sustitución de cultivos ilícitos, ya que podría ser un retroceso en la búsqueda de paz. Sin embargo hay otra parte que afirma que no hay cambios sustanciales en este punto sino una **"interpretación unilateral del gobierno" como lo afirma César Jerez,**

Para el analista Víctor De Currea “en el acuerdo se están dando unos retrocesos que no tienen que ver con el afán de justicia por ejemplo: **un cultivador de coca no puede acceder a los subsidios hasta que no haya radicado el 100% de las matas**, esto no sería un proceso progresivo de restitución de cultivos,  y quedo en nada el punto 3 de cultivos ilícitos. Este es un mensaje confuso”. Le puede interesar: ["Las condiciones de sustitución de cultivos ilícitos según los campesinos"](https://archivo.contagioradio.com/las-condiciones-de-sustitucion-de-cultivos-ilicitos-segun-los-campesinos/)

De acuerdo con Cesar Jerez vocero de ANZORC, la preocupación es más sobre la interpretación que el gobierno viene dando al tercer punto de los acuerdos de paz de La Habana, “**el gobierno está proponiendo en las regiones el pago de doce salarios mínimos en 6 cuotas e imponiendo en la práctica la erradicación como punto de partida** en el proceso de sustitución”, condiciones que no se encuentran en los acuerdos y que no hacen parte del proceso de sustitución.

Jerez agregó que el hecho que se busque volver a la fumigación atiende a unas presiones muy puntuales. A su vez el vocero de ANZORC afirmó que existe la resolución **3080 del Ministerio de Defensa expedida este año, que en la práctica es un plan de erradicación a 5 años, que no tiene la misma intención del acuerdo de paz.**

En la actualidad brigadas móviles están llevando acabo erradicaciones en zonas como Tibú, mientras que en otros lugares del Putumayo como el municipio Valle del Guamuez, San Miguel, la vereda Jordán Guisia, Los Llanos y Betania pobladores denunciaron “que **después de un operativo militar aparecieron panfletos en el que se imponen multas y normas de convivencia** y que se encuentran firmados por el grupo autodenominado Comando Sur de Colombia de la Fuerza Aérea y el Comando de las Fuerzas de Dios Israelitas”. Los sitios a donde llegaron estos panfletos hacen parte del área de construcción del programa de sustitución de cultivos de uso ilícito.

La primera semana de diciembre se llevará a cabo el Quinto **Encuentro de Zonas de Reserva Campesinas, uno de los puntos que se tratarán se la creación de la coordinadora Nacional de Cultivadores de Coca, Marihuana y Amapola,** que servirá como interlocutor en ese proceso de implementación de los acuerdos y que tendrá como parte de sus tareas evidenciar las problemáticas e incumplimientos por parte del Estado a los campesinos frente a la sustitución de cultivos ilícitos.

###### Reciba toda la información de Contagio Radio en su correo [[bit.ly/1nvAO4u]
