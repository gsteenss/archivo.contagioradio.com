Title: Mineros Artesanales de Marmato le ganan el pulso a la Gran Colombia Gold
Date: 2017-03-01 14:26
Category: Ambiente, Nacional
Tags: Gran Colombia Gold, Marmato Caldas, mineros artesanales
Slug: mineros-artesanales-de-marmato-le-ganan-el-pulso-a-la-gran-colombia-gold
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/03/mineros_de_marmato.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: La Patria] 

###### [1 Mar 2017] 

La incertidumbre en la que estaban las familias mineras artesanales de Marmato Caldas desde hace 10 años, terminó. **La Corte Constitucional ordenó tutelar los derechos de los mineros artesanales y suspender la actividad de la multinacional canadiense en el cerro** El Burro, hasta que sea realizada una Consulta Previa, sin embargo el proceso de demanda por \$700 millones iniciado por la Gran Colombia Gold contra el Estado colombiano, continúa.

La decisión de la Corte, también deja sin efectos la resolución 751 de 2010, que ordenaba a la Alcaldía municipal efectuar el desalojo de más de 4.000 mineros tradicionales. Con una votación de 5 contra 3, buena parte de los magistrados coincidieron en que el título minero en poder de la filial de Gran Colombia Gold, Mineros de Occidente, **“pone en riesgo la subsistencia económica de los mineros artesanales y de las comunidades negras de la zona”.**

El fallo dispone que el Ministerio de Interior, la Gobernación de Caldas, la Procuraduría, la Personería y la Agencia Nacional de Minería, **deberán organizar y participar en el proceso de Consulta con las comunidades sobre la cesión del título minero, los impactos socio-ambientales** de la extracción a gran escala por parte de la multinacional y la forma en que explotaran el recurso las familias mineras de la zona.

###  

El alto tribunal también determinó que para el otorgamiento del título minero el título CGH-081 del 2008 a Gran Colombia Gold, **“no se tuvieron en cuenta los reales impactos sociales y económicos para los pobladores de Marmato”**, y obliga a las autoridades competentes que en esta ocasión sean tenidos en cuenta dichos impactos y la decisión de las comunidades. ([Le puede interesar: Corte tiene en sus manos el futuro de 4mil mineros artesanales de Marmato](https://archivo.contagioradio.com/corte-tiene-en-sus-manos-el-futuro-de-4mil-mineros-artesanales-de-marmato/))

Mario Tenguerife, presidente de la Asociación de Mineros de Marmato, aseguró que reciben con alegría la noticia y que estarán siguiendo de cerca el proceso, pues **“el Gobierno siempre ha querido favorecer a las multinacionales (...) tenemos que estar pendientes para que la Consulta se haga”.**

Por último, Tenguerife señaló que el paso a seguir es concertar con las familias mineras la metodología de la Consulta Previa y la fecha en que se va a realizar, e hizo un llamado a organizaciones ambientales, defensoras de derechos y medios de comunicación, para que **apoyen a los mineros de Marmato en el seguimiento y veeduría al fallo de la Corte.**

<iframe id="audio_17301966" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_17301966_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
