Title: Contra el sexismo: Cuentos Antiprincesas
Date: 2015-09-18 16:00
Category: Hablemos alguito
Tags: Chirimbote editorial, Cuentos Antiprincesas, Frida Kahlo, Nadia Fink periodísta, Pitu Álvarez ilustrador, Violeta Parra
Slug: contra-el-sexismo-cuentos-antiprincesas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/antiprincesas.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [<iframe src="http://www.ivoox.com/player_ek_8439616_2_1.html?data=mZmgm5uVeo6ZmKial5yJd6KllJKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRh9Di1dfOjcrQb9TZ2c7gz9SJd6KfpNrS0NnTt4y1z9nW0tfNssTZ1MbgjZKJe6ShpNTb1sbLrdCfs8bRy9SPcYarpJKh&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>] 

###### 17 Sep 2015

Nadia Fink, periodísta argentina, es la mente y corazón detrás de los "**Cuentos Antiprincesas**", una creación literaria de [Editorial Chirimbote](http://www.chirimbote.com.ar/), que apuesta por una forma diferente de construír las historias, que por décadas, han ayudado a perpetuar estereotipos que definen algunos comportamientos sociales de chicos y chicas de América Latina y del mundo.

De acuerdo con Fink, la idea original de los Cuentos Antiprincesas "nace como un juego de oposición a la figuras bellas y estáticas, plagadas de estereotipos con las que se van criando nuestras niñas". Inicialmente con dos publicaciones inspíradas en la vida de mujeres reales, latinoamericanas y de armas tomar: **Frida Kahl**o y **Violeta Parra**.

El cuento sobre la pintora mexicana, es un relato lineal de su vida mientras que el de la cantautora chilena, se centra en el periodo comprendido entre su nacimiento hasta la edad de 37 años, tiempo en el que recorre su país recopilando canciones de la cultura popular chilena, el objetivo "estimular a los chicos y chicas de pequeños, estimular su fantasía y que conozcan la historia de nuestra Latinoamérica".

[![Frida- Cuentos Antiprincesas](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/chirimbote.jpg){.aligncenter .wp-image-14259 width="386" height="386"}](https://archivo.contagioradio.com/contra-el-sexismo-cuentos-antiprincesas/chirimbote/)

Las ilustraciones del dibujante barcelonés **Pitu Álvarez**, complementan de manera excepcional el trabajo de escritura, por su "sensibilidad para interpretar la vida de estas mujeres, con un lenguaje propio, complementando el relato y en ocasiones armando cosas nuevas" refieriéndose la periodísta a cambios o adiciones surgidas en ella al observar las imágenes que acompañarían sus textos.

La gran acogida que han tenido los cuentos en países como Mexico, Perú, Chile y por su puesto Argentina tiene una formula que la escritora asegura "parte de la honestidad del trabajo, es laburar cada cuento de una manera nueva, no una formula industrial, cada historia tiene sus particularidades, y hay que encontrar el como vamos a contarla".

   
 
