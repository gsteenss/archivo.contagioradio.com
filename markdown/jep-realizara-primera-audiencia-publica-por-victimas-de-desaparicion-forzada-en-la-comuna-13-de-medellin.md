Title: JEP realizará primera audiencia pública por víctimas de desaparición forzada en la Comuna 13 de Medellín
Date: 2019-07-15 18:22
Author: CtgAdm
Category: Judicial, Paz
Tags: Audiencia de la JEP, Comuna 13, Desaparición forzada Colombia, Unidad de Búsqueda de personas dadas por desaparecidas
Slug: jep-realizara-primera-audiencia-publica-por-victimas-de-desaparicion-forzada-en-la-comuna-13-de-medellin
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/07/escombrera-contagioradio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

Los próximos 17 y 18 de julio, la Jurisdicción Especial para la Paz (JEP) llevará a cabo la audiencia pública de la Escombrera y la Arenera, en la Comuna 13 de Medellín, uno de los 16 lugares que el Movimiento de Víctimas de Crímenes de Estado (MOVICE) solicitó proteger con medidas cautelares para conservar el terreno en el que se presume se encuentran cerca de 200 restos de personas víctimas de desaparición forzada.

Según Adriana Arboleda, directora de la Corporación Jurídica Libertad, esta es la primera de una serie de audiencias públicas que se pretenden realizar en los departamentos de **Antioquia, Caldas, Cesar, Santander y Sucre,** en pro de garantizar los derechos de las víctimas.

Arboleda, además destacó que la audiencia será de carácter técnico, y ante todo busca establecer medidas de protección, preservación y cuidado que se deben generar para garantizar una búsqueda efectiva de las personas en estos lugares.

La Unidad de Búsqueda de Personas dadas por Desparecidas, quien encabeza esta labor, se encuentra estructurando un plan de búsqueda; precisamente en función de garantizar una documentación ‘fiable’ de todas las víctimas. **“En Colombia hay más de 86.000 casos de desaparición forzada”**, señaló Arboleda. (Le puede interesar:[En 2019 Unidad de Búsqueda de Desaparecidos llegará a 17 territorios)](https://archivo.contagioradio.com/unidad-de-busqueda-de-desaparecidos/)

En este sentido, la directora afirmó que en esta audiencia, la primera en su caso, las víctimas tiene un papel protagónico,  tal como se estableció en los Acuerdos de Paz. Un hecho que también es posible debido a la labor de  diferentes organizaciones que han trabajado por la inclusión de los principales afectados.

Tras las audiencias públicas se espera, por parte de la JEP, medidas específicas de protección a desarrollar en cada sector. La solicitud en concreto para la Comuna 13 es el cierre temporal o definitivo de la Escombrera y la Arenera hasta el diseño de un plan que garantice la búsqueda de víctimas en el sector.

 

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
