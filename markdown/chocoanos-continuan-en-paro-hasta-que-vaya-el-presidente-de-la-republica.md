Title: Chocoanos afirman que no están "dialogando con un gobierno serio"
Date: 2017-05-23 12:45
Category: Movilización, Nacional
Tags: Abandono estatal Chocó, Chocó, Movilizaciones, Paro Chocó
Slug: chocoanos-continuan-en-paro-hasta-que-vaya-el-presidente-de-la-republica
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/05/4Q0A4288-e1495560900283.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Colombia Plural] 

###### [23 may 2017]

Hoy más de medio país se encuentra realizando manifestaciones y el gobierno parece hacer oídos sordos. Debido a los incumplimientos sistemáticos de los acuerdos con el pueblo chocoano en temas de infraestructura, salud y educación, el paro completa 10 días con manifestaciones pacíficas. **Le han pedido al presidente Juan Manuel Santos que sea el encargado de dialogar con ellos.**

Las manifestaciones se fundamentan en el pliego de peticiones que completa un año de incumplimientos. **Este incluye la creación de comisiones de evaluación y seguimientos de los compromisos,** la inversión en saneamiento del hospital de segundo Nivel San Franciso de Asís y la asignación del presupuesto a las vías de comunican al Chocó con el resto del país. Le puede interesar: ["Gobierno hace oídos sordos al paro en Chocó"](https://archivo.contagioradio.com/gobierno-hace-oidos-sordos-al-paro-en-choco/)

Manuel Romaña, miembro del Foro Interetnico Solidaridad Chocó Fish, afirma que “llevamos reclamando nuestros derechos sobre unos acuerdos que ya se hicieron con el gobierno y que no se han cumplido”. **Es por esto que la semana pasada se levantaron de la mesa de diálogo con el gobierno.** Los chocoanos y chocoanas han manifestado en repetidas ocasiones que las delegaciones del gobierno que han ido al departamento no tienen capacidad de resolver los problemas.

Igualmente, han dicho que “la comisión que fue a Buenaventura, fue la misma que vino acá y esto no ayuda a nada”. Por esto **le han pedido a Santos que vaya personalmente** a dialogar con ellos para definir las peticiones que hace el pueblo chocoano. Según Manuel Romaña, “No estamos negociando con un gobierno serio, estamos exigiendo el cumplimiento de un pliego de peticiones que ya fueron pactadas y el gobierno ya le había asignado recursos al tema de la salud y la educación y no ha pasado nada”. Le puede interesar: ["Comunidades culpan a Santos por compromisos incumplidos en Chocó"](https://archivo.contagioradio.com/el-presidente-santos-es-el-responsable-de-los-compromisos-asumidos-e-incumplidos-en-el-choco-emilio-pertuz/)

**ACTIVIDADES DEL PARO EN CHOCÓ**

Los chocoanos han manifestado que el paro va a continuar y mantendrán las vías terrestres y acuáticas cerradas. Las movilizaciones y protestas van a continuar hoy todo el día y harán acompañamiento de actividades lúdicas y culturales. Además **le han pedido a los comerciantes que mantengan los negocios cerrados.** Le puede interesar: ["1500 indígenas se sumarán a paro de desplazados en Río Sucio, Chocó"](https://archivo.contagioradio.com/1500-indigenas-mas-se-sumaran-a-paro-en-riosucio-choco/)

El gobierno ha manifestado que la situación en el Chocó es grave pero que cerrar las vías no es conveniente. Sin embargo, para Manuel Romaña, "la constitución le ha dado al pueblo afrodescendiente la autonomía de realizar este tipo de actividades." Romaña afirma que “las vías hacia Pereira y Medellín son un cementerio más y por esto estamos cerrando las vías”. **El pueblo del Chocó espera que se inviertan los 720 millones de pesos que ya fueron asignados para tener carretas dignas.**

<iframe id="audio_18856088" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_18856088_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU).
