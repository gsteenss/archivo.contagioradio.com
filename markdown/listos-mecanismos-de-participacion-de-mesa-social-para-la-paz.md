Title: Listos mecanismos de participación de Mesa Social para la Paz
Date: 2016-11-04 12:45
Category: Nacional, Paz
Tags: diálogos de paz con el ELN, Mesa Social para
Slug: listos-mecanismos-de-participacion-de-mesa-social-para-la-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/mesa-social-por-la-paz.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio ] 

###### [4 de Nov de 2o16] 

La Mesa Social para la Paz hizo su lanzamiento oficial, una iniciativa que pretende vincular a las comunidades y la ciudadanía en general, a comprometerse en la construcción de un escenario de debate que logre compilar propuestas para llevar a la mesa de conversaciones en Quito. **Esta iniciativa tendrá como primordiales escenario de participación los cabildos y consejos.**

La metodología para este escenario consistirá en construir espacios de participación desde las instancias más pequeñas que existan en las comunidades, sean cabildos, juntas de acción o consejos que puedan ir recogiendo las diferentes propuestas e inquietudes de las comunidades, **para posteriormente pasar a escenario más grandes de carácter departamental o regional, para finalmente llegar a la construcción de una mesa nacional.**

Una vez se logre conformar este espacio, **se pretenderá generar una participación en la mesa de diálogos en Quito, bajo los parámetros que se establezcan entre el ELN  y el gobierno nacional** y de esta forma tener una agenda mucho más rica en propuestas para transformar el país.

De acuerdo con Marylen Serna, una de las impulsadoras de esta iniciativa, “las denominaciones no importan, sino que la ciudadanía se empodere de la participación en sus territorios”. La lideresa Aida Quilcue, vocera del Consejo Regional Indígena del Cauca, afirmó que **“este espacio que se legitima refleja la real participación de la sociedad civil”.**

El equipo de delegados por parte del ELN, a través de un vídeo, también expresó su respaldo a esta iniciativa de participación social, en cabeza de Pablo Beltrán la organización afirmó que "**el gran propósito que buscamos con  estos diálogos  para una solución política al conflicto es avanzar en una democratización de la sociedad**, que incluye decir en donde están las falencias de esta democracia".

En el evento políticos como el senador Alberto Castilla, Iván Cepeda, los representantes a la cámara Alirio Uribe, Víctor Correa, Ángela María Robledo, el alcalde Guillermo Jaramillo, entre otras figuras, respaldaron la propuesta y expresaron su compromiso para ayudar en la construcción de la mesa. Le puede interesar:["Arranca Mesa Social para la Paz"](https://archivo.contagioradio.com/arranca-la-mesa-social-para-la-paz/)

De igual forma organizaciones como Marcha Patriótica, la Central Unitaria de Trabajadores, la Coordinadora Nacional Agraria, el Proceso de Comunidades Negra, la Organización Nacional de Indígenas, entre otras, anunciaron su plena disposición para participar de los diferentes espacios locales que se instalen. Le puede interesar:["Cabildos abiertos podrían ser propuesta desde la Mesa Social para la Paz"](https://archivo.contagioradio.com/cabildos-abiertos-propuesta-mesa-social/)

###### Reciba toda la información de Contagio Radio en su correo [[bit.ly/1nvAO4u]
