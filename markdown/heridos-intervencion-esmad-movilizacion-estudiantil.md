Title: Más de 25 heridos tras la intervención del ESMAD en la movilización estudiantil
Date: 2019-10-11 15:46
Author: CtgAdm
Category: Movilización, Nacional
Tags: Campaña Defender la Libertad, ESMAD, estudiantes, heridos, marcha estudiantil, violencia
Slug: heridos-intervencion-esmad-movilizacion-estudiantil
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/Captura-de-pantalla-2019-10-11-a-las-14.55.00.png" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/Captura-de-pantalla-2019-10-11-a-las-14.58.12.png" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:  @hernanalaya\_] 

Las manifestaciones, convocadas por el colectivo estudiantil el pasado jueves 10 de octubre, transcurrieron de manera pacífica durante las primeras horas de la movilización. Sin embargo, en diferentes lugares de Colombia la actuación policial encabezada por el Escuadrón Movil Antidisturbios - ESMAD dejó un saldo de 25 personas heridas y 89 detenidas. Según reportó el movimiento estudiantil y las organizaciones de defensa de los Derechos Humanos, los agentes policiales llevaron a cabo actuaciones “arbitrarias, desmedidas e ilegales”.

**Alexandra González, coordinadora de la campaña Defender la Libertad**, alerta del impacto que tienen estas actuaciones por parte de la Fuerza Pública en la sociedad: “Esta respuesta violenta por parte del ESMAD difunde un temor generalizado en la población, lo cual se traduce en un miedo a protestar y esto afecta, por tanto, a la democracia”.

### **¿Por qué los estudiantes toman las calles nuevamente?** 

Los estudiantes de las universidades públicas y privadas se han venido movilizado desde el pasado mes de septiembre por el incumplimiento del Gobierno Nacional en materia presupuestal que sitúa el sistema educativo en una condición de precariedad, así como la corrupción por desfalco que ha salpicado la administración de algunas universidades, como la Distrital. (Le puede interesar "[Estudiantes de la Universidad Distrital tienen razones de sobra para protestar](https://archivo.contagioradio.com/estudiantes-de-la-universidad-distrital-tienen-razones-de-sobra-para-protestar/)")

Asimismo, estas movilizaciones tienen un nuevo componente respecto a las anteriores: reclamar el derecho a la protesta, contemplado en la Constitución. Esto surge tras la represión que sufrió el movimiento estudiantil en las anteriores marchas, en las cuales el ESMAD llegó a acceder a los recintos universitarios, algo que desde los colectivos y las propias instituciones categorizan de “violación de derechos humanos y de la autonomía universitaria”. (Le puede interesar "[En las calles por la autonomía universitaria](https://archivo.contagioradio.com/en-las-calles-por-la-autonomia-universitaria/)")

 

> ? [\#URGENTE](https://twitter.com/hashtag/URGENTE?src=hash&ref_src=twsrc%5Etfw) ESMAD agrede a Defensora de Derechos Humanos disparando marcadora directamente al rostro. [@PoliciaColombia](https://twitter.com/PoliciaColombia?ref_src=twsrc%5Etfw) hace evacuación de la plaza de Bolívar en Bogotá de manera violenta. [\#marchaestudiantil](https://twitter.com/hashtag/marchaestudiantil?src=hash&ref_src=twsrc%5Etfw) [pic.twitter.com/rcQWk12YIc](https://t.co/rcQWk12YIc)
>
> — Campaña Defender la libertad (@DefenderLiberta) [October 10, 2019](https://twitter.com/DefenderLiberta/status/1182424258783465473?ref_src=twsrc%5Etfw)

<p>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
</p>
### **La represión del ESMAD en varias ciudades ** 

En **Bogotá**, en la marcha que culminó en la Plaza Simón Bolívar, se reportó un uso desmedido de la fuerza por parte del ESMAD y posteriores detenciones arbitrarias: más de 61 personas fueron detenidas y 46 fueron trasladadas a la estación de policía de La Candelaria, dos de ellas con fines de judicialización.

“Al finalizar la marcha se produjeron algunos incidentes violentos puntuales, sin embargo la actuación del ESMAD no fue dirigida a dispersar a esos sujetos, sino que aprovecharon la situación para atacar y dispersar a toda la movilización. Su intervención fue totalmente desmedida, desproporcional y arbitraria”, denuncia Alexandra. (Le puede interesar: "[Nuevamente el ESMAD olvida que tiene que cumplir protocolos de intervención](https://archivo.contagioradio.com/esmad-olvida-que-tiene-protocolos-intervencion/)")

Al menos doce personas fueron heridas, dos de ellas defensoras de derechos humanos. “Se evidenció un ataque por parte de la policía y del ESMAD hacia personas defensoras de DDHH pese a portar un chaleco identificativo. Se nos agredió física y verbalmente”, relata la coordinadora.

En **Medellín** también se presentaron hechos violentos cuando los estudiantes ingresaron a la Universidad de Antioquia. Dos personas fueron detenidas, una de ellas con fines de judicialización, mientras que en **Cali** se intervino en la Universidad del Valle, en la que se encontraban también menores de edad, quienes se vieron afectados por gases lacrimógenos, 15 personas resultaron heridas y un menor fue detenido.

En **Bucaramanga**, la movilización, catalogada de pacífica, fue alterada por el ESMAD mediante el uso de armas de letalidad reducida de manera contraria a lo establecido en el protocolo. Tres estudiantes fueron heridos y dos detenidos, mientras en **Tunja**, tras finalizar la marcha, a los estudiantes agrupados frente al campus universitario se les lanzó gases y granadas aturdidoras, lo cuál provocó que 4 estudiantes fueran heridos y se prosiguió a la detención de 20 personas.

En **Ibagué**, el ESMAD también hizo uso de la violencia para disolver a los manifestantes que se encontraban en la Universidad de Tolima usando gases lacrimógenos y cinco personas fueron detenidas para posteriormente ser puestos en libertad.

### **Campaña Defender la Libertad** 

La campaña Defender la Libertad tiene como objetivo hacer un acompañamiento en las denuncias de las víctimas de las detenciones arbitrarias, la persecución judicial y la criminalización de la protesta social en Colombia.

La organización ha convocado una **reunión para el viernes 18 de octubre a las 9:00 a.m.en las oficinas del Colectivo de Abogados José Alvear Restrepo** para reunir a los testimonios y víctimas para interponer acciones penales contra las actuaciones del ESMAD y la Policía.

“Vamos a iniciar acciones penales y estaremos trabajando con los organismos de control, la Procuraduría, la Defensoría del Pueblo para que también inicien las investigaciones correspondientes frente al uso arbitrario y abusivo del poder que se presentó el día de ayer y en las movilizaciones pasadas”, afirma Alexandra González.

 

> Mientras [@IvanDuque](https://twitter.com/IvanDuque?ref_src=twsrc%5Etfw) defendía a estudiantes que se manifestaban en Venezuela, aquí en Colombia permite el uso desmedido de la fuerza por parte del [\#EMAD](https://twitter.com/hashtag/EMAD?src=hash&ref_src=twsrc%5Etfw) provocando graves heridas a estudiantes. Denunciamos públicamente al Estado Colombiano. [\#EsmadAsesinos](https://twitter.com/hashtag/EsmadAsesinos?src=hash&ref_src=twsrc%5Etfw) [pic.twitter.com/045q6Enw2A](https://t.co/045q6Enw2A)
>
> — MAPA DH ANTIOQUIA (@DhAntioquia) [10 de octubre de 2019](https://twitter.com/DhAntioquia/status/1182428616396853250?ref_src=twsrc%5Etfw)

<p>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
</p>
 

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>  
<iframe id="audio_43148670" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_43148670_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
