Title: Minería podría estar detrás de la emergencia de inundaciones en Chocó
Date: 2019-02-25 13:02
Category: Ambiente, Comunidad
Slug: mineria-podria-estar-detras-la-emergencia-inundaciones-choco
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/02/Captura-de-pantalla-28.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Afromedios] 

###### [25 Feb 2019] 

De acuerdo con Antonio Andrade, integrante del Comité Cívico por la Salvación y Dignidad del Chocó, las inundaciones en seis municipios del departamento que dejaron a más de 4.000 familias damnificadas este fin de semana, podrían ser producto del** deterioro de los ríos a causa de la minería, tanto ilegal como legal,** en la región.

Los municipios bajo agua, es decir, **Cértegui, Itsmina, Andagoya, Condoto, Tadó y Medio San Juan, son las zonas más afectadas por la explotación de oro** de aluvión y de maquinaria según [información](http://www.biesimci.org/Documentos/Documentos_files/Evoa_2016.pdf) de las Naciones Unidas. Además, Andrade sostiene que estas actividades de explotación alteran los cauces del río, víctimas de igual forma, de la deforestación ocasionada por las industrias madederas. "Todo esto altera los ecosistemas que cuando llegan las lluvias lo que sucede ya son tragedias," dijo el líder.

Desde que se desbordaron cinco ríos el sábado en el suroccidente del Chocó, los ciudadanos han manifestado la perdida de sus enseres y en algunos casos de sus viviendas, además de comerciantes que perdieron su mercancia.

En las zonas rurales de Itsmina, las comunidades proyectan que las afectaciones serán de gran magnitud dado su cercanía al río San Juan. Adicionalmente, reportan que en estas zonas alejadas, aún no ha llegado la ayuda humanitaria y actualmente se realiza un censo de las afectaciones.

Según Andrade, la situación humanitaria es "bastante dramática" y requiere acciones más urgentes que sobrepasen la ayuda humanitaria. Para prevenir que esta emergencia se de nuevamente, las comunidades piden una política pública de control sobre la minería mecanizada, la cual genera los daños más nocivos al ambiente. (Le puede interesar: "[Minería inconsulta afecta a comunidades y al río Apartadocito en Chocó](https://archivo.contagioradio.com/mineria-inconsulta-afecta-comunidades-al-rio-apartadocito-choco/)")

<iframe id="audio_32860344" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_32860344_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
