Title: En veinte años el ESMAD ha asesinado a 34 personas
Date: 2019-12-06 09:32
Author: CtgAdm
Category: Movilización, Paro Nacional
Tags: Asesinatos del estado, Desmonte Ya, Escuadrón Antidisturbios, ESMAD, estado, Gobierno, ONG, Temblores
Slug: en-veinte-anos-el-esmad-ha-asesinado-a-34-personas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/10/ESMAD.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Ernesto Che M Jones] 

[La ONG Temblores ha lanzado, en el marco del Paro Nacional, su informe sobre el historial de crímenes en los que está involucrado el Escuadrón Móvil Antidisturbios (ESMAD), titulado “Pa’ fuera, pa’ la calle. Silencio Oficial: Un aturdido grito de justicia por los 20 años del ESMAD”.]

[Dentro del informe se presenta la historia del Escuadrón, sus transformaciones a partir de los distintos gobiernos —desde 1999 hasta 2019—, el marco legal donde se desenvuelve, las víctimas letales de su accionar, las agresiones registradas a distintas poblaciones y la importancia de su desmonte.]

[La necesidad de **una “literatura crítica” sobre el tema, cumplidos 20 años de la creación del Escuadrón**, se gesta de manera precisa como un trabajo de memoria, una revisión concreta de los casos de asesinatos y abusos, que pone de manifiesto los costos de su gestión y la cuestionabilidad de sus actuaciones. ]

[Para esto, ONG Temblores dividió el informe en 4 capítulos concretos, cada uno centrado en un ítem  específico que busca dar respuesta al silencio estatal frente a las agresiones de los miembros de la fuerza pública y” romper el ocultamiento histórico de los delitos de homicidio y violencia física cometidos por parte del Esmad en contra de la ciudadanía”.][(Lea también: Nace en Colombia la Primera Línea, defensa pacífica contra la represión del ESMAD)](https://archivo.contagioradio.com/nace-en-colombia-la-primera-linea-defensa-pacifica-contra-la-represion-del-esmad/)

### **El ESMAD y la Protesta Social en la Línea de Fuego** 

[Al dar por terminada la medida del «Estado de Sitio», utilizada por los gobiernos anteriores a la Constitución de 1991 —que permitía una represión violenta— , la fuerza pública se dio a la tarea de regular una fuerza que hiciera parte de la Policía Nacional, encargada de replegar cualquier tipo de concentración o manifestación masiva de ciudadanos.]

[Al institucionalizar las formas efectivas de represión, amparadas bajo la figura de la Policía Nacional, se entiende el origen del ESMAD como “parte de un proceso de modernización de la fuerza pública promovido por el Plan Colombia desarrollado en el gobierno de Andrés Pastrana”. ]

[Con 6 oficiales, 8 suboficiales y 200 patrulleros comenzó a funcionar el ESMAD. Bajo el mandato del presidente Pastrana, los fracasados diálogos de Paz del Caguán llevaron a un Gobierno de mano dura, que se intensificaría en los dos períodos presidenciales siguientes, de la mano de Álvaro Uribe.]

[Para el 2006 el ESMAD ya contaba con 1.352 integrantes. Su crecimiento se entiende a partir de lo siguiente: “a medida que los protestantes fueron encontrando en la movilización una vía para tramitar sus demandas, estas se hicieron más álgidas, y a la par, fue aumentando la fuerza usada por el gobierno para aplacarlas”.]

[Desde ese momento, el Escuadrón solamente ha continuado “creciendo desmedidamente **hasta contar en el 2018 con un personal de 3.328 y un presupuesto de poco menos de tres mil millones de pesos”.**]

### **Los estudiantes, los campesinos y los indígenas** 

[Con la llegada de Juan Manuel Santos (2010-2018), la movilización social llegó a un pico de violencia y represión. En el 2013, el Paro Agrario estalló y fueron 902 las víctimas de agresiones físicas. De igual forma, hubo múltiples accionares violentos en zonas rurales, como en la región del Catatumbo y el departamento del Cauca.]

[Además de su participación en marchas y movilizaciones, el ESMAD fue el encargado de manejar el desalojo del Bronx, conocido también como la L, en Bogotá. Tanto en esa intervención, como en el desalojo del antiguo Cartucho —donde un habitante de calle murió por culpa de una recalzada— hubo represión violenta por parte del Escuadrón. ]

### **Límites Lacrimógenos de la Norma** 

[El ESMAD está regulado legalmente dentro del artículo 218 de la Constitución de 1991, el cual establece que “su finalidad será el mantenimiento de las condiciones necesarias para el ejercicio de los derechos y libertades públicas, y para asegurar que los habitantes de Colombia convivan en paz”.]

[El Escuadrón se encuentra regulado por el “Manual para el servicio en manifestaciones y control de disturbios para la Policía Nacional” donde se ejemplifican los tres escenarios de acción: Previo a la movilización, Durante la movilización y Posterior a la Movilización. ]

[Si bien las conductas del ESMAD cumplen —dentro de su marco legal— con la misión de salvaguardar la convivencia y el orden público en estos tres escenarios, las “presuntas desapariciones y homicidios de los manifestantes han generado un sentimiento colectivo de reproche público y desconfianza hacia esta institución.][(Le puede interesar: Las razones para pedir el desmonte del ESMAD a 20 años de su creación)](https://archivo.contagioradio.com/las-razones-para-pedir-el-desmonte-del-esmad-a-20-anos-de-su-creacion/)

[Los agentes del Escuadrón parecen olvidar que, dentro del marco internacional de los Derechos Humanos, el ESMAD está suscrito al «Código de Conducta para Funcionarios Encargados de Hacer Cumplir la Ley» y «Los Principios Básicos sobre el Empleo de la Fuerza y de Armas de Fuego», documentos que ratifican el carácter conciliador y protector que deberían ejercer los uniformados como parte de la fuerza pública. ]

### **Muertes Aturdidoras** 

[En trochas, calles y carreteras han sucedido los 34 asesinatos realizados por miembros del Escuadrón en contra de personas que se encontraban ejerciendo el derecho a la protesta social o simplemente transitando cerca de los sucesos. ]

[**Los estudiantes han sido los más afectados por la violencia perpetrada por el Escuadrón.** Nueve estudiantes han muerto en medio de los mal llamados “desmanes”, cinco de ellos como consecuencia del impacto de bala de un arma de fuego y otros tres por otro tipo de armas utilizadas por el ESMAD —además de las famosas recalzadas y las golpizas físicas—. ]

[La primera de estas muertes fue la de Carlos Giovanni Blanco (2001), asesinado por arma de fuego dentro de una movilización en contra de la guerra en Afganistán. La última, la del estudiante de secundaria, Dylan Cruz (2019), en el marco de las movilizaciones del Paro Nacional. ]

[Los estudiantes comparten características similares: todos tenían entre 19-22 años, estaban ejerciendo su derecho a la protesta social, o pasando cerca de los sucesos, cuando sucedieron los hechos y ninguno ha recibido justicia por parte de las instituciones del Estado. Muy recordado es el caso del menor de edad, y estudiante de secundaria, Nicolas Neira, asesinado en el 2005 dentro de las marchas del primero de mayo. ]

[Por su parte, campesinos e indígenas también han sufrido abusos de fuerza por parte del Escuadrón. “La desigual distribución de la tierra” y la “marginalización histórica de comunidades” son los motivos principales de la movilización en las áreas rurales del país. Han sido nueve los campesinos que han fallecido a manos del ESMAD, siendo el disparo por arma de fuego la causa de muerte más frecuente. ]

[Respecto a los indígenas, “se registraron 9 casos de violencia homicida en contra de la población indígena, siete de los cuales ocurrieron en el departamento del Cauca”. Es lamentable el caso de Miriam Bainama Guatiquí (2007), una niña de seis meses que falleció por culpa de los gases lacrimógenos en medio de manifestaciones que llevaban a cabo los indígenas Emberas en el departamento del Chocó. ]

### **Entre Gritos y Silencio** 

[Si algo caracteriza a todos los casos de violencia por parte del ESMAD es la impunidad. Como lo muestra el informe, “hasta la fecha, ninguno de los agentes perpetradores de los atroces homicidios ha sido condenado penalmente por su accionar”.]

[Dentro de las consecuencias a segundos, familiares y amigos de las víctimas, se encuentran intentos de homicidio, amenazas, violencia sexual y exilio.  Por ejemplo, Yuri Neira —padre del menor asesinado, Nicolás Neira— sufrió un intento de homicidio. Logró sobrevivir y actualmente se encuentra exiliado.]

### **No hay ningún miembro del ESMAD condenado por los asesinatos cometidos** 

[Desde 1999 hasta 2019 solo existen dos procesos penales en la Fiscalía en contra de agentes del ESMAD. Ambos aparecen en el  Sistema de información del Sistema Penal Oral Acusatorio (SPO) y en el Sistema de información de los procesos judiciales adelantados en el marco de la ley 600 de 2000 (SIUJF) bajo el título de “homicidios donde el presunto implicado es un miembro del Esmad”.]

[El primero, que se encuentra activo, “ocurrió en el año 2005 en la ciudad de Cali, en el Valle del Cauca. El otro, por su parte, se encuentra inactivo y ocurrió en el año 2006, en Villavicencio, en el departamento del Meta”. ]

[De esta forma, vemos el alcance que tienen las acciones violentas del ESMAD en contra de grupos de personas específicas —estudiantes, campesinos, indígenas— que murieron en circunstancias abruptas, algunas ejerciendo su derecho constitucional a la protesta y otras en situaciones poco claras. Lo que sí deja en claro el informe de la ONG Temblores es el historial nefasto de una Escuadrón que cumple veinte años en medio de un Paro Nacional y con una prontuario violento que va en aumento. ]

[Por último, **la ONG Temblores ha interpuesto una Solicitud de Medidas Cautelares ante la Comisión Interamericana de Derechos Humanos,** solicitando a la organización que vele por la protección de “los grupos de estudiantes, indígenas y campesinos que se movilizarán durante las próximas dos semanas en Colombia”.]

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
