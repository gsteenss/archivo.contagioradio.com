Title: Balance ambiental: La locomotora arrolló los ecosistemas en el país
Date: 2015-12-31 12:48
Category: Ambiente, Otra Mirada
Tags: Agua, extractivismo, Mineria
Slug: balance-ambiental-la-locomotora-arrollo-los-ecosistemas-en-el-pais
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/12/AMBIENTE-.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### 30 Dic 2015

<iframe src="http://www.ivoox.com/player_ek_9927304_2_1.html?data=mp6fmZiUeI6ZmKialJ2Jd6Knl5KSmaiRdo6ZmKiakpKJe6ShkZKSmaiRhsLgwtPQx5DFscPdxtPhw9GJd6KfrcaYztTHs87j1dTfw5DFttPjzdGSpZiJhpSfzdTgjcrHs9Td1NnSz5KJe6ShpNTb1sbLrdCfs8bRy9SPcYarpJKh&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

En 2015, el territorio colombiano en toda su extensión vivió un año complejo en materia ambiental. La desenfrenada apuesta gubernamental por explotar de las entrañas de la tierra los recursos como combustible imprescindible para sostener las principales “locomotoras” de su modelo minero energético, ha traído consigo, además de múltiples impactos al ambiente, desarraigo, desplazamiento, violencia y muerte.

Desde el año anterior, la promoción estatal de utilizar procedimientos como el fracking para la extracción de hidrocarburos como parte de la financiación del proceso de paz, encendió las alarmas al interior de las organizaciones que trabajan en la defensa del ambiente, quienes desde entonces han levantado su voz por los impactos irreversibles que esta técnica generaría para el agua, los ecosistemas y para la salud humana. ([Leer nota](https://archivo.contagioradio.com/astrid-puentes-aida-es-peligroso-que-se-vincule-el-fracking-a-la-financiacion-del-proceso-de-paz/))

La lucha por frenar el uso de la fractura hidráulica en el mundo se ha dado por su capacidad de producir serias consecuencias como la contaminación de acuíferos, afecciones de la salud y emisiones de gases de efecto invernadero como el metano que es 87 veces más activo que el dióxido de carbón, lo que provoca mayor calentamiento global, y su implementación en latinoamérica no cuenta con los estudios adecuados sobre sus impactos.

La denominada locomotora minero-energética, se materializa a todas luces en el Plan Nacional de Desarrollo 2014-2018, aprobado de manera arbitraria durante el año, donde claramente se apuesta por la construcción de vías 4G, que a su paso atravesaran bosques, acabando con la vida silvestre y el recurso más importante: el agua, utilizando los ríos para las represas y los océanos para el petróleo, los campos y las montañas para la explotación minera. ([Leer nota](https://archivo.contagioradio.com/plan-nacional-de-desarrollo-mercantilizacion-de-la-naturaleza-y-desastre-ambiental-anunciado-1er-parte/))

Por medio del artículo 177 que permite la minería en páramos, y el 183 que avala el otorgamiento de las licencias exprés, el modelo extractivista quedó incluído dentro del Plan Nacional de Desarrollo, gracias a los impulsos que les dieron los ministros de minas y de hacienda, según denunciaron varios congresistas, entre ellos, la representante de la Alianza Verde, Ángela María Robledo. ([Leer nota](https://archivo.contagioradio.com/pnd-no-es-un-plan-de-desarrollo-sino-de-negocio-angela-maria-robledo/))

En las 191 páginas de la encíclica "Laudato sí" el Pápa Francisco acusó a las multinacionales y los gobiernos de ser causantes del cambio climático, debido al “el uso desproporcionado de los recursos naturales”, un duro análisis a las actividades del ser humano en la tierra, que finalmente es lo que ha llevado a que se estén viviendo tiempos difíciles en materia ambiental.

El sumo potífice abordó temas como la contaminación y el cambio climático, la mala gestión del agua, la pérdida de la biodiversidad, la gran desigualdad entre regiones ricas y pobres o la debilidad de las reacciones políticas ante los daños ambientales, donde señala directamente a los culpables, llamando la atención también de los ciudadanos del mundo, por su aporte al "consumismo inmoral" que ha llevado a la sociedad a un comportamiento que permite la degradación continua de la naturaleza. ([Leer nota](https://archivo.contagioradio.com/empresas-y-gobiernos-son-los-causantes-del-cambio-climatico-segun-el-papa/))

**Fumigaciones y petróleo**

Algunas de las comunidades que durante el 2015, recibieron los fuertes impactos de las políticas gubernamentales sobre sus territorios y sus personas, fueron sin lugar a duda, las que habitan en el departamento de Putumayo. ([Leer nota](https://archivo.contagioradio.com/empresas-petroleras-fumigaciones-y-la-guerra-tienen-en-crisis-al-putumayo/))

Por cuenta de las fumigaciones con glifosato que el Gobierno colombiano se había comprometido a suspender, niños y adultos de distintos corregimientos de Putumayo experimentaron graves afectaciones a su salud durante este año, como brotes y alergias particularmente en menores de edad. frente a la situación de emergencia y pese a las denuncias de las comunidades las autoridades no mostraron respuestas efectivas. ([Leer nota](https://archivo.contagioradio.com/la-aspersion-con-glifosato-esta-enfermando-a-los-ninos-en-putumayo/))

Otro de los impactos sociales y ambientales registrados en este departamento tuvo que ver con la contaminación de por lo menos 2 litros de agua por segundo por la acción de las 8 empresas petroleras en ecosistemas estratégicos como humedales y bosques de selva tropical. Multinacionales que además se han visto involucradas en múltiples violaciones a derechos humanos entre ellas amenazas, muertes, estigmatizaciones y persecuciones que han proferido contra pobladores que se han opuesto a sus actividades extractivas y de transnacionalización del territorio. Fenómeno que se agudiza por la influencia militar y paramilitar en la región. ([Leer nota](https://archivo.contagioradio.com/petroleras-contaminan-2-litros-de-agua-por-segundo-en-putumayo/))

Los medios de información mostraron hasta la saciedad, la tragedia ambiental por el vertimiento del crudo en las aguas del río Putumayo por parte de la guerrilla de las FARCEP, acción cuestionada y rechazada por ambientalistas y defensores de ddhh; nadie, sin embargo, ni entidades gubernamentales ni los medios, han hecho resonancia de la tragedia, de la crisis ambiental generada por el vertimiento de aguas contaminadas, luego de ser usadas para la extracción de barriles de petróleo, ni a la deforestación, ni las mutaciones, ni la mortandan de animales, ni al riesgo de supervivencia de muchas especies vivas, de flora y fauna; y mucho menos de las graves violaciones de derechos humanos y las responsabilidades empresariales de las petroleras.

### **Resistencia, represión y estigmatización** 

Los Movimientos que hacen resistencia en los territorios a la nociva invasión por parte de las empresas, han sido constantemente víctimas de la represión estatal y la estigmatización que les ha acarreado amenazas contra su vida, lo que han convertido a Colombia en el segundo país de Latinoamérica donde es más peligrosa la tarea de defender el ambiente por detrás de Honduras, de acuerdo a un estudio de la organización británica Global Witness. ([Leer nota](https://archivo.contagioradio.com/colombia-tercer-pais-mas-peligrosos-para-defender-el-ambiente/))

Algunas de las organizaciones que han recibido amenazas, provenientea en su mayoría de grupos paramilitares, Liberación de la Madre Tierra, la Movilización de Mujeres Negras del Norte del Cauca por el Cuidado de la Vida y la Defensa de los Territorios Ancestrales, en oposición a los megaproyectos extractivos.

La construcción de las represas La Salvajina en el Cauca, Urrá en Córdoba, Hidrosogamoso en el departamento de Santander, El Quimbo en el Huila e Hidroituango en Antioquia, están dejando a su paso el desplazamiento de comunidades que se han ido quedando sin tierras donde llevar adelante su proyecto de vida, pero también la desaparición forzada de un líder, el asesinato de cinco en Santander, y 51 a nivel nacional, las amenazas contra 31 líderes y lideresas de Ríos Vivos, la judicialización infundada de más de 25, la tortura de dos e intento de secuestro de dos más, así como el desplazamiento de 100.000 personas a nivel nacional por causa de las represas. ([Leer nota](https://archivo.contagioradio.com/vida-dignidad-y-naturaleza-la-lucha-de-las-comunidades-contra-hidroituango/))

Finalizando el año, la multinacional Emgesa, propietaria del Proyecto Hidroeléctrico El Quimbo en el Huila, suspendió la prestación del servicio de energía en virtud a una sentencia dictada por la Corte Constitucional, que anuló el decreto 1759 del 6 de octubre de 2015, Decisión reafirmada por el juzgado tercero penal de Neiva negando la reactivación de las turbinas de la hidroeléctrica, que había sido solicitada por parte del Ministerio de Minas y el Director General de la Autoridad Nacional de Acuicultura y pesca (Aunad).

Por su parte, La comunidad teme que Emgesa, , tome medidas jurídicas para que se reverse esta decisión, pues la construcción de la hidroeléctrica, ha dejado más de 12 mil familias desplazadas, no ha reparado a los pobladores que se quedaron sin tierras, y de las 2.900 hectáreas de terrenos que debían darse a los campesinos no se ha entregado ninguna. ([Leer nota](https://archivo.contagioradio.com/suspension-de-hidroelectrica-el-quimbo-debe-ser-definitiva/))

### **Cambio climático** 

La posición gubernamental de trabajar por contrarrestar la generación de gases de efecto invernadero, participación en la COP 21, contrasta con su propio modelo político y económico, puesto en evidencia durante todo el año, con la puesta en marcha de mega proyectos consignados en el Plan Nacional de Desarrollo. ([Leer nota](https://archivo.contagioradio.com/cop-21-mas-emisiones-menos-compromisos/))

Los discursos y los compromisos expuestos en la cumbre de Paris contra el cambio climático, resultan ser un saludo a la bandera, por la falta de medidas reales que enfrenten este fenómeno, y la opinión pública se divide entre aquellos que consideran los acuerdos alcanzados como un primer paso que compromete a las naciones en su rol para frenar el crecimiento de la temperatura en la tierra y sus consecuencias y quienes están preocupados por tanta autocomplacencia entorno a estos. ([Leer nota](https://archivo.contagioradio.com/acuerdo-de-la-cop21-es-inhumano-enganoso-y-esquizofrenico/))

### **Ambiente y paz** 

De acuerdo con Briggitte Baptiste “El problema real en los próximos 25 años, no será ni siquiera la guerra sino el cambio climático”, El gobierno colombiano y los agentes del conflicto en negociación, deben comprender que para alcanzar la paz en Colombia, se necesita garantizar el respeto por el ambiente y todas las formas de vida.

Desde la posición de los y las defensoras del ambiente, es necesario y urgente que estos temas sean abordados en la mesa de negociaciones en la habana, y que los acuerdos alcanzados propendan por la protección de los ecosistemas, que podrían enfrentar nuevas amenazas con la llegada de la paz, la defensa de  la vida y la dignidad de las comunidades y de aquellos que las defienden, con una respuesta distinta por parte del estado a lo hasta ahora expuesto con su modelo extractivista.

Aunque varias han sido las victorias para la defensa del ambiente durante el año que termina, más grandes son los retos que vienen para el que comienza, en un posible escenario de postconflicto y postacuerdo en el que se juega el destino de nuestros recursos naturales en una lucha constante de intereses empresariales con el favor de la política y fuerza del estado, y los desafío que implica el afrontar los cambios en el comportamiento climático, sin un compromiso definitivo por parte de aquellos que deberían asumirlos.
