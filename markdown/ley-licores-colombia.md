Title: Nueva ley de Licores, ¿buena o mala?
Date: 2016-10-10 11:35
Category: El Eclectico
Tags: colombia, ley de licores, Reforma tributaria
Slug: ley-licores-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/12/lanuevaleydelicoresdirector-e1481647416537.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### Foto: fnd.org 

##### 10 Oct 2016 

Sin duda un tema que toca las fibras cotidianas del colombiano promedio, pero que pocos saben cómo funciona en cuestión tributaria y cuáles serían los efectos positivos o negativos de la nueva Ley de Licores que se tramita en el Congreso de la República. Le puede interesar: [Reforma tributaria, un golpe a los contribuyentes más débiles](https://archivo.contagioradio.com/reforma-tributaria-un-golpe-a-los-contribuyentes-mas-debiles-hector-moncayo/).

Para acercar a nuestros oyentes a este Proyecto de Ley nos acompañaron columnistas de El Ecléctico de distintas tendencias y otros invitados como Santiago Rodríguez quien desde la línea del Partido de la U (que avala el Proyecto) y además desde una zona del país muy influyente en la industria licorera como es Caldas explicó por qué encontraba en la ley algo favorable.

¿Serán los licores más costosos de ahora en adelante? ¿Cómo funciona hoy el aspecto tributario de los licores y cómo funcionará si se aprueba esta ley en el Congreso? ¿Qué pasará con el monopolio rentístico del que gozan las entidades territoriales en cuestión de licores? ¿Beneficia este Proyecto de Ley a la industria nacional sobre la multinacional, o al revés? Éstas y más respuestas hoy en nuestro debate de El Ecléctico.

<iframe id="audio_14934776" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_14934776_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>
