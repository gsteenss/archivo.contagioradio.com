Title: Los rostros de la adopción igualitaria a través de la fotografía
Date: 2015-05-11 07:38
Author: CtgAdm
Category: LGBTI, Nacional
Tags: Adopción igualitaria, Familias homoparentales, Homosexualidad, Juan Manuel Barrero, LGBTI, Retratos de Familia
Slug: los-rostros-de-la-adopcion-igualitaria-a-traves-de-la-fotografia
Status: published

##### Fotografía: [[egocitymgz.com]

La situación de las familias homoparentales en Colombia es de completa invisibilidad al estar condicionadas por la ley de adopción consentida, en la que se aclara que parejas del mismo sexo pueden adoptar siempre y cuando el menor sea hijo biológico de alguna de las dos partes y lleven conviviendo tres años o más, por lo que las familias acuden a llevar una vida por su propia cuenta y lejos de términos legales, arriesgándose a perder varios de los derechos de los que deberían gozar independientemente de su orientación sexual.

En respuesta a lo anterior, el fotógrafo documental Juan Manuel Barrero propone un trabajo visual llamado "Retratos de Familia" en el que se reunirán fotografías de la forma de vida de las familias homoparentales en sus hogares, el objetivo es mostrar la situación actual de las familias compuestas por personas del mismo sexo en un país como Colombia.

La idea central es que sean estas mismas familias quienes aporten desde su cotidianidad un elemento visual, que pueda aportar a las necesidades de la comunidad LGBTI, las personas que estén interesadas y dispuestas a abrir las puertas de su casa para poder realizar este ensayo fotográfico solo deben escribir al correo <infojmbb@gmail.com>, se acordará la cita y los detalles propios del trabajo.

El fin último de este proyecto es hacer una publicación en papel (libro) y en medios electrónicos.

<div class="yj6qo ajU" style="text-align: justify;">

   
\  
 

</div>
