Title: Fiscalía estratifica las víctimas en Colombia: El caso de Carlos Pizarro
Date: 2015-09-20 00:18
Category: DDHH, Nacional
Tags: Carlos Pizarro, Cartel de Medellín, Comisión Colombiana de Juristas, das, Fiscalía, M-19, Pablo Escobar
Slug: fiscalia-impide-que-se-interrogue-a-testigo-clave-en-asesinato-de-carlos-pizarro
Status: published

###### Foto: Vice.com 

###### <iframe src="http://www.ivoox.com/player_ek_8463355_2_1.html?data=mZmjlZiZeY6ZmKiak5yJd6KkmJKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRb7DW1Nnf18jHrdDixtiYzNrIrcTdwtHS1ZDJsozXwtjcjcnJsIzhwszby8jNqMrjjKjO1NHTt46ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>  
[ Alejandro Malambo, Comisión Colombiana de Juristas.] 

<iframe src="http://www.ivoox.com/player_ek_8463344_2_1.html?data=mZmjlZiYeI6ZmKiak5iJd6KolZKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRk8Pn1dfixcjNs8%2FZ1JDX18nNp8rVzcrgjcrSb8TV1NSYxsrQb87VyNPWxc7IrdCfpMbfztTXb46ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [María José Pizarro, hija Carlos Pizarro] 

###### [19 Sept 2015] 

La Comisión Colombiana de Juristas presentó una carta a la Dirección Nacional de Fiscalías de Análisis y Contexto (Dinac), denunciando que la **Fiscalía estaría estratificando a las víctimas del conflicto armado,** luego de que el director de la Dinac, Juan Pablo Hinestrosa, negara el desplazamiento de una comisión a España que le tomaría declaración a un **testigo clave en el caso del asesinato de Carlos Pizarro.**

Se trata de **Carlos Mario Alzate Urquijo, alias “el Arete”,** una de las personas más allegadas al cartel de Medellín de Pablo escobar en los años 90.  Su testimonio podría señalar la responsabilidad de grupos paramilitares en el homicidio contra el dirigente de izquierda, desmintiendo lo que en este momento aseguró el DAS. Por otro lado, podría dar información importante sobre los asesinatos de otros dirigentes de izquierda.

En la carta de la Comisión Colombiana de Juristas, se manifiesta que esas actuaciones en el caso de Carlos Pizarro son graves y discriminatorias, ya que **la Dinac estaría estratificando a las víctimas de la violencia sociopolítica de Colombia, “**pues no existe el mismo proceder ni facilitación de la actuación del fiscal en la práctica de pruebas en el extranjero cuando se trata de un líder de izquierda”, como lo afirma Alejandro Malambo,  abogado de la Comisión Colombiana de Juristas.

Ante esta situación, María José Pizarro, hija del asesinado dirigente del Movimiento M-19, asegura que estos 25 años han sido de “larga lucha”, a ella le produce **“muchísima indignación que en el caso Galán se cuente con recursos investigativos y en el caso nuestro tengamos que estar con las uñas, exigiéndole al estado un derecho que es nuestro”.**

Así mismo, María José, expresó que este **“mensaje que se lanza por parte de la Fiscalía es nefasto para los diálogos de paz que se adelantan en la Habana”,** teniendo en cuenta que se trata de ejemplo por medio del cual se le podría estar mostrando a las FARC si existen o no garantías de no repetición una vez los dirigentes esa guerrilla y empiecen una vida política.
