Title: Organizaciones ambientalistas preparan movilización contra la minería contaminante y el fracking
Date: 2019-06-06 10:41
Author: CtgAdm
Category: Ambiente, yoreporto
Tags: Ambiente, fracking, Marcha carnaval
Slug: organizaciones-ambientalistas-preparan-movilizacion-contra-la-mineria-contaminante-y-el-fracking
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/Imagen-1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Comité ambiental 

\#YoReporto por: Valentina Camacho Montealegre- Comité Ambiental en Defensa de la Vida - @valentinacmpm

Las diferentes organizaciones sociales y ambientales del país están promoviendo una **movilización nacional para este viernes 7 de junio en favor del agua y en contra la minería contaminante y el fracking.** Asimismo, se espera que \#CarnavalesPorElAgua sea tendencia en redes sociales.

El Movimiento Nacional Ambiental y la Alianza Colombia Libre de Fracking, plataformas ambientalistas convocantes, **esperan movilizar en total 300.000 personas en aproximadamente 100 municipios en donde sus organizaciones tienen presencia.**

**La iniciativa de Marcha Carnaval nació en el año 2011, en Ibagué,** como una propuesta de movilización no violenta promovida por el Comité Ambiental en Defensa de la Vida contra el proyecto minero La Colosa. Desde entonces, los ciudadanos se manifiestan cada año en el marco del Día Internacional del Medio Ambiente, de manera pacífica, alegre y artística exigiendo al estado garantizar el derecho al ambiente sano.

“Este año las exigencias tienen el objetivo de defender las bases del estado social de derecho, la autonomía territorial, el cumplimiento de las consultas populares y los acuerdos municipales, que deben hacerse efectivos a través de **la anulación de los Títulos Mineros y contratos de exploración y explotación en los municipios donde se han prohibido las actividades de minería e hidrocarburos**”, explicó Renzo García, vocero del Movimiento Nacional Ambiental.

Y agregó: “De igual forma salimos a exigir la prohibición del fracking en todo el territorio nacional, por principio de precaución. **Los colombianos no podemos permitir que nos impongan una dictadura mineroenergética** que se roba el derecho colectivo al ambiente sano de nuestra niñez y familias”.

Es de resaltar, que Ibagué, Bogotá, Barrancabermeja, Medellín, Cali y 69 municipios más en Tolima, Quindío, Caldas, Boyacá, Pereira, César, Caquetá, Bolívar, Huila, Meta, Risaralda, Santander, Antioquia, Cundinamarca, Valle, Nariño, Santa Martha y Norte de Santander ya confirmaron movilización. También, se realizarán actos simbólicos internacionales en Paris (Francia), Mendoza (Argentina) y Roma (Italia).

Haga click [aquí](http://www.bit.ly/CarnavalesPorElAgua) para ver la información de todas las movilizaciones.

 
