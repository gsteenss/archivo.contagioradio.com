Title: Crímenes contra la comunidad LGBT: el reto es vencer el prejuicio
Date: 2015-06-16 10:50
Category: Carolina, Opinion
Tags: Colombia Diversa, informe derechos humanos, LGBTI
Slug: crimenes-contra-la-comunidad-lgbt-el-reto-es-vencer-el-prejuicio
Status: published

###### Foto: Contagio Radio 

#### [**[Carolina Garzón Díaz](https://archivo.contagioradio.com/carolina-garzon-diaz/)** - @E\_vinna] 

###### [16 Jun 2015] 

La organización Colombia Diversa presentó hace algunos días su informe “Cuando la guerra se va, la vida toma su lugar. Informe de derechos humanos de LGBT en Colombia, 2013-2014”. Un documento que da cuenta de las principales vulneraciones a los derechos de las personas lesbianas, gay, bisexuales y transgeneristas en el país. El informe revela que la violencia contra esta población es constante y se expresa en homicidios, violencia policial, amenazas, hostigamientos y obstáculos en el acceso a la justicia.

Para tener un panorama más claro podemos evocar algunas de las cifras del informe: entre 2013 y 2014 se registraron 164 homicidios contra personas de la comunidad LGBT, 222 casos de violencia policial, 54 amenazas y únicamente 5 condenas en los 440 casos de violencia denunciados. Cada uno de estos números es una historia de vida y es el relato de una humanidad que ha sido lastimada o exterminada por ser alterna. Lo más complejo de esta información es que, según Colombia Diversa, estas cifras podrían ser la punta del iceberg pues “persisten problemas en el registro de información, el acceso a mecanismos de protección y las garantías para denunciar estas formas de violencia”.

De acuerdo con el informe, un concepto clave para comprender la dimensión de la violencia contra la población LGBTI es el prejuicio. Según María Mercedes Gómez, coordinadora para Latinoamérica y el Caribe de la International Gay and Lesbian Human Rights Commission, el prejuicio es una primera impresión que se considera como un conocimiento verídico y justificado, aunque en realidad no tenga fundamento. Es decir, “es una percepción que no pasó necesariamente por un proceso racional”. Para Gómez el problema no es el prejuicio en sí, pues todos los tenemos en muchos aspectos de la vida, la dificultad radica en que ese prejuicio puede justificar e incluso llegar a incitar la violencia contra el otro, en este caso, contra las personas lesbianas, gay, bisexuales y transgeneristas.

Vencer el prejuicio es necesario para prevenir la violencia contra la comunidad LGBT, pero también para acelerar las investigaciones en los casos que han sido denunciados y garantizar que estos hechos no se repitan. Además de las cifras, el informe documenta los testimonios de varias personas víctimas, por ejemplo, los brutales asesinatos de hombres homosexuales, la tortura a mujeres trans, los hostigamientos a mujeres lesbianas y las amenazas y homicidios contra defensores y defensoras de los derechos de esta comunidad. Para avanzar en la superación de la impunidad en estos casos es indispensable que las instituciones responsables se quiten el prejuicio de los ojos, de los oídos y del corazón.

La construcción de la paz se basa en el respecto a los derechos humanos y a la diversidad. Hoy Colombia necesita, más que nunca, que los prejuicios contra lo que consideramos diferente, alterno o diverso sean superados. La educación juega un papel definitivo, pero no sólo en la escuela, sino una pedagogía del respeto y contra el prejuicio que inicie en la familia, pase por los medios de comunicación, permee las instituciones del Estado y se posicione en el debate público.

Se espera que este informe presentado por Colombia Diversa, esta columna y todas las notas de prensa que han reseñado la grave situación de derechos humanos que sufre la comunidad LGBT en Colombia sea un llamado al respeto de todos los derechos de las personas lesbianas, gay, bisexuales y transgeneristas del país; así como una invitación a las instituciones del Estado colombiano para cumplir su deber constitucional, porque no es suficiente pedir confianza en las instituciones sí estas, en lugar de proteger, victimizan a quienes han sufrido la violencia.
