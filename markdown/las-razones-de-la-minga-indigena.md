Title: Violación a los DD.HH. e incumplimiento del Gobierno: Las razones de la Minga indígena
Date: 2018-11-13 12:36
Author: AdminContagio
Category: DDHH, Nacional
Tags: Chocó, indígenas, Minga, ONIC
Slug: las-razones-de-la-minga-indigena
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/08/minga-indigena.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: @ONIC\_Colombia] 

###### [13 Nov 2018] 

Más de **450 indígenas** integrantes de las comunidades Embera Dóbida, Katío y Wounaan, **provenientes del Chocó, llegaron este fin de semana a Bogotá en el marco de la Minga por la Vida**, denunciando la crisis humanitaria que se vive en el departamento, y reclamando el cumplimiento de los Acuerdos pactados entre el Gobierno Nacional y la Minga de 2017.

De acuerdo con los indígenas, **sus comunidades han sido víctimas de confinamientos, producto de las minas antipersona que hay en el territorio, y  de los enfrentamientos** entre el ELN, paramilitares y grupos residuales provenientes de las FARC. También denunciaron que se están dando reclutamientos forzados de menores de edad y desplazamientos masivos. (Le puede interesar: ["Minga por la Vida anuncia acuerdos con el Gobierno"](https://archivo.contagioradio.com/48847/))

Según **Isabela Membaché, vocera de la Minga**, a pesar de que las denuncias sobre estos hechos se han tramitado anteriormente, no han recibido respuesta; por esta razón, **se vieron obligados a emprender un viaje a Bogotá para pedirle en persona al presidente Iván Duque, condiciones que garanticen la vida de sus comunidades**. Adicionalmente, piden que se brinde amparo para su estadía en Bogotá, y respaldo para su retorno.

### **Minga fue detenida a las afueras de Bogotá, intentando evitar su ingreso a la Capital** 

Este fin de semana la **Organización Nacional Indígena de Colombia (ONIC)**, denunció que uniformados detuvieron en las afueras de Bogotá a los 10 buses en los que se trasladaban cerca de 450 indígenas que participan en la Minga, evitando su ingreso a la Capital. Según, Membaché, **la detención se dio porque el Gobierno Nacional no había dado permiso para su ingreso a la Ciudad**.

Esta situación los obligó a pernoctar a las afueras de Bogotá, expuestos al frío y sin poder comer, puesto que no había lugares cerca para comprar comida. Actualmente, la Vocera de la Minga indicó que los indígenas están en la oficina de la ONIC, **esperando los resultados de la reuniones que se adelantan con representantes del Gobierno**. (Le puede interesar: ["Se agudiza crisis humanitaria en Chocó"](https://archivo.contagioradio.com/se-agudiza-crisis-humanitaria-en-choco/))

### **Minga seguirá hasta alcanzar su objetivo** 

Tras la primera jornada de negociaciones se levantó la mesa por falta de garantías para la negociación, sin embargo, los integrantes de la Minga señalan que seguirán exigiendo sus derechos, que les permitan un retorno digno a su territorio. Mientras tanto, han pedido ropa, alimentos no perecederos y medicamentos en la oficina de la ONIC (Calle 12b ·4-38) para palear las difíciles condiciones que enfrentan las 450 personas que están en Bogotá.

 

> [\#MingaPorLaVida](https://twitter.com/hashtag/MingaPorLaVida?src=hash&ref_src=twsrc%5Etfw)| ¿Estas son las garantías del Gobierno de [@IvanDuque](https://twitter.com/IvanDuque?ref_src=twsrc%5Etfw) para los pueblos que salen huyendo del Chocó a causa del conflicto? Mingueros amanecieron adentro de [@ONIC\_Colombia](https://twitter.com/ONIC_Colombia?ref_src=twsrc%5Etfw) y quienes no cupieron en el suelo de pasillos les tocó dormir en la calle 12b con 4ta. [pic.twitter.com/LKOIgDPUgI](https://t.co/LKOIgDPUgI)
>
> — ONIC (@ONIC\_Colombia) [14 de noviembre de 2018](https://twitter.com/ONIC_Colombia/status/1062686718997000192?ref_src=twsrc%5Etfw)

<p>
<script src="https://platform.twitter.com/widgets.js" async charset="utf-8"></script>
</p>
###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
