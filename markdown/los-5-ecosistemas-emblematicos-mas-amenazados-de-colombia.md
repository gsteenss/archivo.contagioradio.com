Title: Los 5 ecosistemas emblemáticos turísticos de Colombia más amenazados
Date: 2016-12-26 11:10
Category: Ambiente, Voces de la Tierra
Tags: Amazonía, Ciénaga grande Santa Marta, La Guajira, palma de cera, Valle dal Cocora
Slug: los-5-ecosistemas-emblematicos-mas-amenazados-de-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/05/maravillas-naturales.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [26 Dic 2017] 

El modelo extractivista en el que se basa la economía tiene en riesgo las riquezas naturales más importantes de Colombia, que a su vez son importantes lugares turísticos. Pese a que el país es el segundo más biodiverso del mundo, y por tanto, tiene una responsabilidad frente a la conservación de los diversos ecosistemas, actualmente dichos paisajes exclusivos se encuentran en riesgo por concesiones mineras y petroleras de las que están detrás diferentes multinacionales, poniendo **en riesgo ríos, quebradas, caños y lagunas, por el desgaste irreversible de estas fuentes hídricas ocasionado por las actividades de las empresas petroleras, mineras y de monocultivos.**

Aquí les mostramos los ecosistemas en mayor riesgo:

### **1. Valle del Cocora** 

Tras la negativa de los habitantes de Salento, Quindío, y de diversos sectores de la población civil frente a la posibilidad de que se realice actividad minera en el Valle del Cocora, la Agencia Nacional de Minería, aseguró que no se otorgará el título minero para la explotación de oro en esa zona del país. Sin embargo, los habitantes esperan que ese anuncio se materialice con resoluciones y actos administrativos, teniendo en cuenta que aún existen 14 solicitudes de concesiones mineras en trámite, sin tener en cuenta que el **98% de Salento tiene alguna declaratoria de protección ambiental, y en contraste con ello, más del 50% de ese territorio estaría titulado o en trámite para otorgar licencias.**

El riesgo es inminente, pues no la palma de cera, árbol nacional de Colombia estaría en riesgo, sino también especies animales como **el Loro orejiamarillo, que actualmente es una de las especies de loros más amenazadas del mundo** debido principalmente a la destrucción de su hábitat y la tala de la palma de cera donde construye sus nidos y de la cual se alimenta.

### **2. La Macarena** 

Gracias a presión nacional en internacional, la Agencia Nacional de Licencias Ambientales se vio obligada revocar la licencia ambiental en esta zona. Pese a ello, continúa la amenaza debido a que **aún existen 12 licencias ambientales más para extraer petróleo y realizar actividades mineras.**

Apenas se revocó una licencia de un bloque petrolero. Todavía hay cuatro bloques más que pertenecerían a las empresas **Hupecol, PetroNova, Hocol y Petrominerales. Así mismo, existen 7 títulos mineros para Arenas Bituminosas y otros dos títulos mineros para extraer oro**, que se encontrarían en zona de amortiguamiento de las reservas naturales de esa parte del país, donde se encuentra uno de los lugares más emblemáticos que enmarca un patrimonio ambiental del planeta, [Caño Cristales.](https://archivo.contagioradio.com/no-es-posible-hacer-explotacion-petrolera-sin-danar-los-ecosistemas/)

### **3. La Amazonía** 

Los municipios de Doncello, Puerto Rico, Paujil y San Vicente, hacen parte del departamento del Caquetá y **representan el 18.67% de la región Amazónica**colombiana, un área que requiere altos estándares de protección ambiental, pero que se encuentra amenazada por 43 bloques petroleros.

Según la Agencia Nacional de Hidrocarburos, ANH, en Caquetá se planea la puesta en marcha de estos bloques estarías dispuestos para las empresas, **Meta Petroleum, Pacific Stratus, Canacol, Emerald Energy, Monterico, Hupecol, C&C Energy, Optima Range y Ecopetrol, **son las empresas interesadas en realizar actividad petrolera en este departamento

### **4. La Guajira** 

El Tribunal Contencioso Administrativo de La Guajira, resolvió un fallo de tutela, con el que se ordenó la suspensión durante un mes de los actos administrativos que autorizaban a la empresa minera El Cerrejón la intervención del [arroyo Bruno, ](https://archivo.contagioradio.com/con-el-arroyo-bruno-serian-27-las-fuentes-de-agua-que-el-cerrejon-ha-secado-en-la-guajira/)**[afluente principal del Río Ranchería](https://archivo.contagioradio.com/con-el-arroyo-bruno-serian-27-las-fuentes-de-agua-que-el-cerrejon-ha-secado-en-la-guajira/) y fuente de agua de la que dependen comunidades wayúu, afrodescendientes y cabeceras municipales como las de Albania y Maicao**.

Sin embargo, las comunidades continúan luchando para que esa decisión sea definitiva, pues aunque La Guajira es uno de los departamentos más visitados de Colombia, actualmente por cuenta de la explotación minera se han secado cerca de 27 fuentes de agua, lo que ha puesto en riesgo comunidades ancestrales, y una importante área de bosque seco tropical, del cual a Colombia solo le queda un 1.5%, y por lo cual el arroyo bruno había sido propuesto como parque natural regional.

Es decir que ese **permiso que se concede al Cerrejón va en contravía de las apuestas regionales por la conservación  del bosque seco tropical y las estrategias para enfrentar la desertificación.**

### **5. Ciénaga grande de Santa Marta** 

El bajo nivel de las aguas del Río Magdalena y el uso de los ríos que nacen en la Sierra Nevada para implementación de distritos de riego destinados a monocultivos de palma y banano, sumado a los efectos de la construcción de la Vía de la Prosperidad y a la ineficiencia de las obras hidráulicas de la vía entre Barranquilla y Santa Marta, tiene en alto riesgo de desaparición al complejo de humedales de la **Ciénaga Grande de la Magdalena, Reserva de Biósfera y Humedal Ramsar de importancia internacional.**

**"La situación de la Ciénaga es crónica y delicada, estamos entrando al cuarto año de un déficit hídrico,** de una sequía muy prolonga que ha magnificado los efectos de la falta de agua dulce", asegura Sandra Vilardy, profesora de la Universidad del Magdalena, denunciando el mal manejo del recurso hídrico y la irresponsabilidad ambiental de los proyectos agroindustriales**.**

Este complejo de humedales se alimenta del Río Magdalena y de los ríos que bajan de la Sierra Nevada, pero **éstos están siendo usados casi de manera exclusiva para los distritos de riego y los cultivos de palma y banano**, como lo han denunciado los campesinos de la zona, sin dejar caudales ecológicos que lleguen a la Ciénaga.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
