Title: Por fumigaciones con glifosato 800 familias de la Perla Amazónica tendrían que desplazarse
Date: 2015-08-12 15:06
Category: DDHH, Nacional
Tags: ADISPA, Asociación de Desarrollo Integral Sostenible de La Perla Amazónica, Audiencia Pública por la Defensa de Nuestros Territorios, fumigaciones, Glifosato, Juan Manuel Santos, paz, Perla Amazónica, Puerto Asís, Putumayo, zonas de reserva campesina
Slug: por-fumigaciones-con-glifosato-800-familias-de-la-perla-amazonica-tendrian-que-desplazarse
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/08/glifosatoColombia.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: [doralnewsonline.com]

<iframe src="http://www.ivoox.com/player_ek_6611574_2_1.html?data=l5uek5qbeI6ZmKiak5iJd6KllpKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRlNDmjMviz87LpcTd0NPS1ZDHs8%2BfyNHWyNTXpdXjjJ2dkpDKpc7dzc7O1ZDIqYzgwpC9x9fQpY6ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Jani Silva, Asociación de Desarrollo Integral Sostenible de La Perla Amazónica] 

###### [12 Ago 2015]

Alrededor de **800 familias** de la Zona de Reserva Campesina, Perla Amazónica del departamento del Putumayo, han sido **víctimas en los últimos días de las aspersiones aéreas con glifosato,** que están afectando los cultivos de pancoger, los ríos y a la población.

De acuerdo con Jani Silva, lideresa de la Perla Amazónica y representante de la Asociación de Desarrollo Integral Sostenible de La Perla Amazónica, ADISPA, los días **3 y 9 de agosto, las 25 veredas** que hacen parte de esta región del país, fueron roseadas con el químico que cayó sobre un menor de edad y otras dos personas más que **terminaron con mareos y vómitos durante la noche.**

Debido a esa situación **cuatro familias debieron salir de sus fincas y el resto de personas se encuentran en riesgo de desplazamiento** hacia el casco urbano, ya que “la comida que sembrada y los proyectos agrícolas se dañaron por el glifosato", dice Jani Silva, quien agrega que las personas que se vieron obligadas a irse, “no tenían ni plátano que comer”.

**“Es algo criminal lo que están haciendo, rociando con glifosato los ríos",** expresa la lideresa de la Perla Amazónica, ya que cuenta que “están fumigando con glifosato en partes donde ni siquiera hay coca”.

Ante estas acciones del gobierno, las comunidades se cuestionan sobre el tipo de paz que busca el presidente Juan Manuel Santos, y aseguran que al gobierno, “No les interesa acabar las fumigaciones,** se ensañaron con la ZRC Perla Amazónica,** quieren desocupar el Putumayo", denuncia la representante de ADISPA.

El próximo 15 de agosto se realizará una **audiencia sobre explotación petrolera y fumigaciones en el municipio de Puerto Asís,** allí las comunidades esperan que se visibilice la situación que viven para que se adopten las medidas con el fin de enfrentar estas actuaciones incongruentes con el discurso de paz del gobierno.

[![petrolerasredes](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/08/petrolerasredes.jpg){.aligncenter .wp-image-12100 width="499" height="794"}](https://archivo.contagioradio.com/por-fumigaciones-con-glifosato-800-familias-de-la-perla-amazonica-tendrian-que-desplazarse/petrolerasredes/)
