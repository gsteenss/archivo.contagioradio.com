Title: Arhuacos ganan batalla contra empresa de hidrocarburos Azabache
Date: 2018-02-21 12:14
Category: DDHH, Nacional
Tags: arhuacos, Consulta Previa, empresa Azabache, Indígenas arhuacos, Sierra Nevada de Santa Marta
Slug: arhuacos-ganan-batalla-contra-empresa-de-hidrocarburos-azabache
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/11/arhuacos-indigenas-e1511801126214.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Aventure Colombia] 

###### [21 Feb 2018] 

El pueblo indígena Arhuaco **ganó una tutela interpuesta** contra la multinacional canadiense de hidrocarburos Azabache. Se tendrá que realizar una consulta previa para preguntarle a los indígenas si están de acuerdo o no con las actividades de extracción petrolera en sus territorios que hacen parte de la Sierra Nevada de Santa Marta.

De acuerdo con el abogado y asesor de los pueblos indígenas en temas territoriales y ambientales, Edward Álvarez, el pueblo Arhuaco se ha movilizado en varias ocasiones “contra **cientos de títulos mineros** y de hidrocarburos que hay en la Sierra Nevada de Santa Marta”.

### **Presidente autorizó extracción petrolera en cuencas vitales para la Sierra Nevada** 

Informó que cuando se estaba desarrollando la movilización, el presidente Juan Manuel Santos “autorizó a la empresa canadiense Azabache, **que tiene problemas con otros pueblos indígenas** de América Latina, para que realizara actividades de extracción petrolera en la cuenca hidrográfica de Aracataca”.

Esta cuenca, que es de origen glacial, es una de las **35 cuencas hidrográficas** que hacen parte de la Sierra Nevada. Esa cuenca de Aracataca, hace parte del territorio ancestral del pueblo indígena y la empresa “empezó a hacer perforación petrolera sin que se le informara a la comunidad indígena Arhuaca”. (Le puede interesar:["Arhuacos exigen que la Sierra Nevada sea libre de minería"](https://archivo.contagioradio.com/arhuacos-exigen-que-la-sierra-nevada-sea-zona-libre-de-mineria/))

### **Juez ordenó realizar consulta previa** 

Cuando esto sucedió, los indígenas revisaron el proceso y constataron que **se desconoció el derecho a la consulta previa**. Por esto, presentaron el caso ante el Juzgado Civil del Circuito de Santa Marta y este ordenó a la Autoridad Nacional de Licencias Ambientales y a la dirección de consulta previa del Ministerio del Interior “iniciar de manera inmediata el proceso de consulta previa”.

Además, los indígenas van a exigir que esta consulta se haga con los **estándares internacionales** “para que la comunidad pueda objetar el proyecto”. Álvarez manifestó que las diferentes instituciones ambientales “le dijeron al pueblo Arhuaco de manera descarada que no era cierto el proceso de perforación petrolera en la Sierra Nevada cuando sí era cierto”.

### **Actividades de la empresa generó afectaciones ambientales y culturales** 

Según el abogado, la Corporación Ambiental Regional del Magdalena, “con base en la licencia aprobada por la ANLA, otorgó el permiso de **sustraer agua de la cuenca del río Aracataca**”. Esto generó daños ambientales de un recurso que ha sido defendido de manera permanente por los indígenas de la Sierra Nevada.

Además, manifestó que ha habido afectaciones a los derechos culturales de los pueblos indígenas en la medida en que “**están limitados para subir** hacia la parte alta de la Sierra Nevada de Santa Marta por los controles que ejerce la empresa canadiense”. Estos impactos deberán ser revisados con el mecanismo de consulta previa del cual tienen derecho las comunidades indígenas. (Le puede interesar:["Para el 2040 la Sierra dejará de ser Nevada"](https://archivo.contagioradio.com/nevados-de-la-sierra-nevada-de-santa-marta-desapareceria-en-el-2040/))

### **Responsabilidad es del Estado** 

En repetidas ocasiones, los pueblos Arhuacos han manifestado que, más allá de ser un problema de la empresa, **hay una responsabilidad significativa del Estado** porque “es el Estado, a través de la dirección de consulta previa y la ANLA, quienes deben prever los impactos y convocar a las partes a desarrollar la consulta”.

Álvarez enfatizó en que el Estado tiene un discurso de protección ambiental frente a la comunidad internacional que **no se traduce en las acciones que realiza en el país**. Dijo que “por primera vez en la historia de Colombia, se hace perforación petrolera en la zona de la Sierra Nevada de Santa Marta”.

Finalmente, el abogado indicó que “los pueblos indígenas **no van a permitir** que esa empresa siga haciendo exploración petrolera en sus territorios”. Manifestó que la Corte Constitucional ha emitido innumerables sentencias para proteger la Sierra Nevada “pero los directores de consulta previa y la ANLA son sujetos que tienen como propósito acabar los ecosistemas”.

<iframe id="audio_23953439" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_23953439_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]

<div class="osd-sms-wrapper">

</div>
