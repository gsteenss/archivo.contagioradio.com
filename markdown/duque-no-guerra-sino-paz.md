Title: "Duque, no le pare bolas a los que hablan de la guerra sino a los que le hablamos de la paz"
Date: 2018-09-10 13:34
Author: AdminContagio
Category: Nacional, Paz
Tags: ELN, Olimpo Cárdenas, Victor de CurreaLugo
Slug: duque-no-guerra-sino-paz
Status: published

###### [Foto: @IvanDuque] 

###### [10 Sept 2018] 

Tras cumplirse el plazo dado por el presidente I**ván Duque** para evaluar los diálogos de paz con el **ELN,** y que reiterara la condición de liberar a todas las personas retenidas por esta guerrilla para iniciar un nuevo ciclo de conversaciones, diferentes sectores de la sociedad pidieron al Gobierno continuar con la mesa y crear una estrategia de negociación que no limite la agenda.

Por una parte, el analista **Victor De CurreaLugo,** aseguró que con el enfoque que Iván Duque le está dando a la mesa, se está volviendo a un proceso de Desarme, Desmovilización y Reinserción **(DDR),** modelo que ha fracasado en muchas partes del mundo, y que no corresponde con el pasado cercano de los procesos de paz en Colombia. (Le puede interesar: ["Por ahora, Duque no quiere la negociación sino la rendición del ELN: Victor de Currea"](https://archivo.contagioradio.com/duque-quiere-rendicion-eln-currea/))

Según de Currea, con este enfoque se desconoce lo avanzado en el proceso de paz con las FARC, que a diferencia de otros, buscó negociar asuntos más sustanciales que la dejación de las armas y la reincorporación a la vida civil. Adicionalmente, el analista afirmó que reducir la negociación con el ELN a la liberación de personas retenidas es secuestrar la mesa de conversaciones, y desconocer la desigualdad, la pobreza, el difícil acceso a tierras y todos los problemas fundamentales que condujeron al conflicto armado.

Por otra parte, el integrante de la organización social "Por la Paz", **Olimpo Cárdenas,** sostuvo que el Gobierno no debería desconocer lo adelantado en el proceso con el ELN porque "no se empieza de cero; ya hay dos años y medio de trabajo; hay una agenda de 6 puntos acordada; y ya hubo importantes acuerdos entre las partes". (Le puede interesar: ["Las condiciones para continuar la mesa de diálogos con el ELN"](https://archivo.contagioradio.com/las-condiciones-para-continuar-la-mesa-de-dialogos-con-el-eln/))

### **"Lo importante no es si se mantiene la mesa, sino para qué"** 

De Currea ha manifestado en múltiples ocasiones que de continuar el proceso, **debería contar con una estrategia de negociación cuyos objetivos, permitan determinar lo que es fundamental y aquello que se puede negociar.** En ese orden de ideas, "lo importante no es si se mantiene la mesa, sino para qué", y recalcó que hay temas que están pasando desapercibidos de la agenda de negociación como el asesinato de líderes sociales.

Opinión que compartió Cárdenas, quien señaló la necesidad de dar continuidad a la negociación para que **desarrollar efectivamente un proceso de pedagogía de paz,** terminar el "genocidio de los miembros de organizaciones sociales", y generar las transformaciones importantes en términos de desigualdad y pobreza que requiere el país.

### **Duque dijo que no haría "trizas los Acuerdos"** 

Recientemente, cerca de 1.200 organizaciones sociales enviaron una misiva al presidente Duque, recordando las frases que había mencionado en su posesión, entre ellas su compromiso con la paz de Colombia, y la garantía de que **no haría "trizas los acuerdos".** Para Cárdenas, esta carta fue también una muestra de acompañamiento al proceso y una buena señal para el mismo.

Otra señal emitida por el Gobierno fue el anuncio del Comisionado de Paz, Miguel Ceballos, quien sostuvo que las ordenes de captura sobre los negociadores del ELN seguirán congeladas, sumado al gesto de voluntad de negociación expresado por esa guerrilla al liberar los uniformados retenidos en Arauca. (Le puede interesar:["Así transcurrieron los 4 meses de cese bilateral al fuego entre el ELN y el Gobierno"](https://archivo.contagioradio.com/asi-transcurrieron-los-4-meses-del-cese-bilateral-al-fuego-entre-el-eln-y-el-gobierno/))

Finalmente, Cárdenas reiteró que las 1200 organizaciones que componen "Por la Paz", están para rodear al gobierno y no sólo para criticarlo, e hizo un llamado al mandatario para que **"no le pare bolas a los que hablan de la guerra sino a los que le hablamos de la paz".**

<iframe id="audio_28459785" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_28459785_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en] [su correo](http://bit.ly/1nvAO4u)[ o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por] [Contagio Radio](http://bit.ly/1ICYhVU). 
