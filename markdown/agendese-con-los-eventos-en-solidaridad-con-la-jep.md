Title: Agéndese con los eventos en defensa a la JEP
Date: 2019-03-11 11:35
Author: CtgAdm
Category: Nacional, Paz
Tags: Iván Duque, jurisdicción especial para la paz, Ley estatutaria
Slug: agendese-con-los-eventos-en-solidaridad-con-la-jep
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/03/53492937_770340310019819_4159139208667070464_n.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Jóvenes por Santander] 

###### [11 Mar 2019] 

Ante la decisión del Presidente Iván Duque de objetar seis puntos de la Ley Estatutaria de la Jurisdicción Especial para la Paz (JEP), la sociedad civil ha programado los siguientes eventos hasta la fecha para demostrar su solidaridad con el mecanismo de justicia transicional.

### **Cundinamarca** 

En **Bogotá** desde este 11 de marzo inician las actividades. A las 12 del medio día, organizaciones sociales están convocando un cacerolazo, en la Plaza Simón Bolívar, en donde se espera que se logren coordinar más actividades, durante la semana, que apoyen la defensa de la Jurisdicción Especial para la paz.

El 12 de marzo en la Universidad Naciona, se realizará la charla sobre el balance a un año de la implementación de la JEP, en la que participará su presidente Patricia Linares, el evento se llevará a cabo en el auditorio Camilo Torres, de la Facultad de Ciencias Políticas.

Finalmente, el 13 de marzo se esta convocando una movilización que partiría desde el Centro Nacional de Memoria Histórica y llegará hasta la Plaza de Bolívar, en donde se espera una gran participación ciudadana en defensa del Sistema Integral de Verdad, Justicia y Reparación.

En el municipio de **Chía** las personas también se encuentran convocando a un plantón que se llevará a cabo el día 13 de marzo en el Parque Principal Santander a las 4 de la tarde.

### **Medellín** 

El 13 de marzo también se realizará una movilización en esta ciudad, el lugar de encuentro será el Parque de los Deseos a las 6 de la tarde.

### **Baranquilla**: 

El 13 de marzo también se realizará una movilización en esta ciudad, la cita será en el Parque Monumento a la Bandera (Carrera 54 con Calle 58) a las 4 de la tarde.

### **Cali**: 

En esta ciudad, se está convocando a un platón que se realizará en la Plazoleta San Francisco, hacia las 5 de la tarde.

### **Bucaramanga**: 

El miércoles 13 de marzo, se llevará a cabo el plantón  "Yo defiendo la JEP", que tendrá como hora de encuentro las 6:30 pm y partirá desde el Parque Guillermo Sorzano, San Pio. Allí se espera que las personas participen con velas blancas, en defensa de la paz.

### **Ipiales -Nariño** 

En el municipio de Ipiales, se está convocando a un plantón, el próximo miércoles 13 de marzo, a las 6 de la tarde, bajo la consigna "que no nos quiten el derecho a la paz", en el parque 20 de Julio.

### **Putumayo** 

En Mocoa, la ciudadanía se encontrará hacia las 4 de la tarde en la Bomba Las Villas, para realizar un plantón.

### **Risaralda** 

En la ciudad de Pereira, las personas se reunirán en la Plaza de Bolívar, hacia las 4 de la tarde para llevar a cabo un plantón.

### **Meta** 

En Villavicencio el plantón se llevará acabo en el Parque Central, a las 5:00pm.

Noticia en desarrollo...

######  
