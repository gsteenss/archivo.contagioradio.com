Title: 5 grandes daños de la minería a gran escala
Date: 2016-10-13 16:17
Category: Ambiente, Nacional
Tags: afectaciones, Agua, conservación del ambiente, contaminación, defensa del medio ambiente, Defensores ambiente, Gran Minería en Colombia, Mineria
Slug: 5-grandes-danos-de-la-mineria-a-gran-escala
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/10/Mineria-e1476393062932.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: VeoVerde 

Aunque diversas organizaciones sociales y ambientalistas han denunciado a través del tiempo los daños que representa para sus territorios y su vida la minería a cielo abierto, el otorgamiento de licencias para realizar este tipo de actividad en varios territorios del país continúa. Según datos del Instituto Nacional de los Recursos Naturales (Inderena) la minería tiene graves afectaciones para el ambiente. Las modificaciones inciden en los recursos hídricos, geológicos, biológicos, de la atmósfera y adicionalmente en la cultura de los territorios que rodean los lugares en donde se lleva a cabo minería.

1.  **Daños a las fuentes hídricas.**

Cuando se realiza minería a cielo abierto, el agua se ve seriamente comprometida debido  a los residuos sólidos que son vertidos durante la actividad minera. Los resultados de esto se traducen en un mayor contenido de sedimentos, lo que causa que los ríos o los afluentes de agua sean desviados de sus cauces naturales generando inundaciones y cambios en el paisaje, así como afectaciones a comunidades, quienes algunas veces se ven expuestas a perder sus cultivos de pan coger y por el nivel de contaminación no pueden consumir estos afluentes de agua. Le puede interesar [Empresa Cerrejón tiene al borde de la muerte al río ranchería](https://archivo.contagioradio.com/empresa-cerrejon-tiene-al-borde-de-la-muerte-al-rio-rancheria/).

2.  **Cambios en la topografía y geomorfología**

Para realizar la minería a gran escala es necesario hacer la remoción de capas superficiales de la tierra. Al hacer estas modificaciones se dejan las rocas descubiertas, lo que puede ocasionar erosión. Así mismo, se producen procesos de resquebrajamiento de las rocas, que si bien es un proceso natural, al ser afectadas de esta manera se ven expuestas a un proceso acelerado, esto debido al uso de dinamita en las zonas. Lea también [Minería en Colombia ocupa 5 millones de hectáreas y la agricultura solo 4 millones](https://archivo.contagioradio.com/mineria-en-colombia-ocupa-5-millones-de-hectareas-y-la-agricultura-solo-4-millones/)

3.  **Impacto sobre bosques**

Otro de los daños que genera la minería a cielo abierto es precisamente a los bosques en los cuales en algunos casos se hace necesaria la tala o para los que el agua deja de llega debido a las modificaciones en los afluentes o en las corrientes de los ríos. Puede leer: [Minería en Páramo de Santurbán no contó con estudios sobre impactos socio-ambientales](https://archivo.contagioradio.com/mineria-en-paramo-de-santurban-no-conto-con-estudios-sobre-impactos-socio-ambientales/)

4.  **Afectaciones sociales**

En cualquier lugar donde se lleve a cabo la explotación minera el aumento de las migraciones de población, los cambios en las costumbres y en las actividades de la economía cambian sustancialmente. Así mismo, el deterioro en la salud y en los cuerpos de las personas que viven en estos territorios no se hace esperar. Leer más [Niños indígenas y campesinos dibujan los efectos de la minería en sus cuerpos](https://archivo.contagioradio.com/ninos-indigenas-dibujan-los-efectos-de-la-mineria-en-sus-cuerpos/)

5.  **Otro tipo de contaminaciones**

Para poder sacar del territorio los desechos y los productos explotados se realiza la construcción de vías y oleoductos, esto también genera alteraciones de diverso tipo al suelo y a la fauna y flora que se encuentra en la zona y por donde pasan los vehículos. También pueden presentarse derrames y emisiones de gases o del polvillo en el caso del carbón, esto último también afecta sustancialmente la salud de los pobladores.

Le recomendamos [Cinco documentales que relatan el desastre de la gran minería](https://archivo.contagioradio.com/cinco-documentales-que-relatan-el-desastre-de-la-gran-mineria/)

###### Reciba toda la información de Contagio Radio en su correo [[bit.ly/1nvAO4u]
