Title: Entre 5 y 30 años de prisión pagarán campesinos víctimas de la Masacre de Curuguaty
Date: 2016-07-11 15:38
Category: DDHH, El mundo
Tags: condenas contra campesinos por masacre de curuguaty, destitución fernando lugo, golpe de estado paraguay, masacre de curuguaty
Slug: entre-5-y-40-anos-de-prision-pagaran-campesinos-victimas-de-la-masacre-de-curuguaty
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/07/Comision-de-Victimas-de-la-Masacre-de-Marina-Kue.-Curuguaty.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: EA] 

###### [11 Julio 2016 ]

El 15 de junio de 2012 decenas de familias campesinas de Curuguaty intentaron ocupar las tierras que estaban siendo apropiadas por empresarios, cuando integrantes de la Fuerza de Operaciones de la Policía Especializada perpetraron una masacre en su contra, que dejó como resultado **once campesinos asesinados, seis policías muertos** y la destitución parlamentaria de Fernando Lugo.** **Este lunes y martes se conocerá la sentencia en el marco del proceso judicial que se adelanta y en el que se pretende condenar a los campesinos hasta a 30 años de prisión.

Según Carlos Goncaldes, del 'Centro de Producción Radiofónica Info Paraguay', no habían más de 50 campesinos, pero el contingente policial enviado fue de por lo menos trecientos efectivos de la Policía Nacional quienes llegaron al municipio tres semanas después de la ocupación de las familias campesinas y provocaron la masacre, de la que culpan a los campesinos, quienes son procesados por los delitos de asociación criminal, invasión de inmueble ajeno y homicidio. Sin embargo, las tierras que ocuparon eran de propiedad del Estado y **la Fiscalía no ha comprobado que los campesinos se asociaron criminalmente, ni que hayan cometido un homicidio**.

4 años han pasado desde que ocurrió la masacre y uno del proceso en el que se investiga la muerte de los seis uniformados, sin incluir la de los once campesinos, y hay serias dudas por parte de organizaciones sociales sobre la transparencia de la investigación, teniendo en cuenta que se ha evidenciado que hay un montaje para culpar a quienes son inocentes, asegura el periodista, e insiste en que en Paraguay hay una distribución muy desigual de la tierra, el **2% de la población total maneja el 60% de la tierra apta para el cultivo, mientras que más de 300 mil familias campesinas no acceden a ella.**

Según Colgandes, ni los fiscales, ni los jueces, ni los magistrados "han tenido las agallas suficientes para recuperar las tierras mal habidas" que fueron apropiadas por políticos, empresarios y militares, y debían ser destinadas a una reforma agraria, por lo que **esperan que la justicia internacional actúe para esclarecer los hechos que rodearon la masacre**. Sí los campesinos son condenados, la defensa contempla apelar a la sana rebeldía para exigir una sentencia justa.

<iframe src="http://co.ivoox.com/es/player_ej_12189684_2_1.html?data=kpeemp6afJWhhpywj5abaZS1kp2ah5yncZOhhpywj5WRaZi3jpWah5yncaTV09Hc1ZCrs8_XwtHRx9iJdqSf0crfy9TIrdTowpC20MvTb7HV08bU18bdcYarpJKw0dPYpcjd0JC_w8nNs4yhhpywj5k.&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en [[Otra Mirada](http://bit.ly/1ICYhVU)] por Contagio Radio] 
