Title: ¿Cómo proteger a los líderes sociales y defensores de DD.HH en Colombia?
Date: 2018-06-13 16:23
Category: DDHH, Nacional
Tags: defensores de derechos humanos, IODHACO, Talua por Colombia
Slug: como-proteger-a-los-lideres-sociales-y-defensores-de-dd-hh-en-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/06/DSC1803.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [13 Jun 2018] 

El más reciente informe de las organizaciones OIDHACO y Taula Catalana por la paz reveló que si bien es cierto que en Colombia hay una disminución en la violencia y víctimas producto de la firma del acuerdo de paz, el riesgo para las y los defensores de derechos humanos va en aumento, **sin que existan las medidas necesarias que garanticen la protección de sus vidas**.

### **282 homicidios de defensores de DD.HH desde la firma del Acuerdo de paz** 

El informe titulado “Cómo protegemos a quienes defienden los derechos humanos en Colombia”, evidenció que desde el primero de enero de 2016 hasta el 27 de marzo de 2018, se han registrado en Colombia **282 homicidios,** según los reportes de la Defensoría del Pueblo, en donde el 74% de las acciones violentas se cometieron contra hombres y el 26% contra mujeres.

En ese sentido, los departamentos con mayores tazas de asesinatos a defensores de DD. HH son **Cauca, Antioquia, Valle del Cauca, Nariño y Chocó**. A su vez, el documento denuncia que la gran mayoría de agresiones y homicidios se ha presentado contra líderes comunales. (Le puede interesar:["Asesinan a tres líderes sociales en menos de una semana en Colombia"](https://archivo.contagioradio.com/asesinan-a-3-lideres-sociales-en-menos-de-una-semana/))

Frente a los responsables de estos hechos, el informe señala que si bien existen investigaciones contra los autores materiales, en la mayoría de casos se desconocen tanto los autores materiales como los intelectuales. **El 90% de los asesinatos se cometieron bajo patrones sicariales, de hombres que apuntan con armas y se desplazan en motos.**

En los casos en los que se conoce al autor, el documento señaló que los principales responsables son grupos armados sucesores del paramilitarismo. En ese sentido el Indepaz, identificó que en el primer semestre de 2017 **esos grupos registraron actividad en al menos 274 municipios de 28 departamentos, es decir el 70% del territorio del país**. Además, se estableció que destacan las menciones de acciones de los grupos Autodefensas Gaitanistas de Colombia (AGC), también denominadas Clan del Golfo, Los Rastrojos, Las Águilas Negras, los Puntilleros y La Constru, entre muchos otros.

### **Los mecanismos para proteger a los defensores** 

Frente a las acciones que toman las instituciones y autoridades gubernamentales para garantizar la protección de los defensores de derechos humanos, Hendrine Rotthier, integrante de OIDHACO, manifestó que algunas de esas medidas han sido efectivas, sin embargo, hay deficiencias, "no todas las medidas que se dan son adecuadas para los grupos y es necesario **generar medidas con enfoques dirigidos a los diferentes grupos poblacionales que tengan mayor riesgo**".

 En segundo lugar, recomiendan que se revisen los mecanismos de protección, ejemplo de ello serían las medidas colectivas, que son mucho más eficiente que las medidas individuales, debido a que en muchas ocasiones no es suficiente la protección a una persona en una comunidad, sino que el riesgo es colectivo.

Asimismo, el documento evidencia la importancia de realizar más esfuerzos en términos de la prevención de actos de violencia contra defensores y defensoras de derechos humanos. **Uno de ellos debe ser identificar las causas estructurales detrás de los asesinatos**.

### **Las acciones primordiales para proteger a las y los defensores** 

Dentro de las recomendaciones, las organizaciones solicitaron de forma urgente que se adelanten investigaciones y resultados frente a los responsables de estos asesinatos, debido a que de esta forma se podría configurar de una manera más precisa las intensiones que rodean estos actos de violencia y de prevenir otras situaciones similares.

De igual forma se encuentra la necesidad de que el Estado ponga en marcha políticas que permitan el desmantelamiento de las estructuras paramilitares que hay en el país, debido a que la mayoría de los asesinatos y agresiones han sido cometidos por estos grupos. (Le puede interesar:["Congresistas de EE.UU piden fortalecer protección a líderes sociales en Colombia"](https://archivo.contagioradio.com/congresistas-de-ee-uu-piden-fortalecer-proteccion-a-lideres-colombianos/))

El documento reveló,  la necesidad de tomar medidas urgentes para garantizar la vida de las y los defensores de derechos humanos en el país. En ese sentido en primera instancia le piden a la Unión Europea continuar con el acompañamiento y seguimiento tanto a la implementación de los Acuerdos de Paz, como a el cumplimiento y vigilancia de las labores que realizan empresas de estos países en Colombia. D**ebido a que algunos de los asesinatos de líderes están relacionados con las actividades que esas organizaciones realizan en el país. **

<iframe id="audio_26521785" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_26521785_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
