Title: Mujeres en Defensa de la Paz: la importancia de la mujer en el conflicto armado
Date: 2018-09-05 13:18
Author: AdminContagio
Category: Mujer, Nacional, Paz
Tags: conflicto, Cumbre Nacional de Mujeres y Paz, mujer, paz
Slug: importancia-de-mujeres-en-conflicto-armado
Status: published

###### [Foto: Colprensa] 

###### [05 Sept 2018]

La Cumbre Nacional de Mujeres y Paz,  dará inicio a los **Diálogos: Mujeres en defensa de la Paz**, donde se busca informar a los asistentes sobre la participación, los logros y los retos de las mujeres en los procesos de mediación con grupos armados, evento que tendrá lugar en 3 de las principales universidades de Bogotá.

La participación significativa de las mujeres en los diálogos con las FARC-EP y actuales negociaciones con el ELN, es desconocida para algunos sectores de la sociedad, por tal motivo, entre las temáticas del evento se narrarán **experiencias adquiridas por mujeres en el Acuerdo Final de Paz y las expectativas de las mismas en los diálogos con el ELN.** Adicional a esto, se hablará  de la seguridad para líderes y lideresas sociales en Colombia. (Le puede interesar:[El papel de las mujeres en la Jurisdicción Especial para la Paz](https://archivo.contagioradio.com/mujeres-jep/))

**Datos del evento**

Los diálogos iniciaran desde el Jueves 6 de Septiembre, durante el desarrollo de la Semana por la Paz, y la primera locación será la Pontificia Universidad Javeriana. Contarán con la participación de **Socorro Ramirez, defensora de los derechos de las mujeres y la paz y 3 lideresas** provenientes de los territorios más afectados por el ELN actualmente.

El evento hace parte de la campaña ¡A Defender la Paz!, y se replicará el 19 de Septiembre en la Universidad Distrital, el 2 de Octubre en la Universidad Nacional, y el 9 de octubre en la Universidad Jorge Tadeo Lozano. **Para participar, se debe diligenciar el formato de inscripción disponible en la página** [http://bit.ly/DIALOGOSCIUDADANOS](https://docs.google.com/forms/d/e/1FAIpQLSfU5y9ROpk1AfvWJ02cMzekCF6h22LxJIi2LSOVEKfeAwJT5w/viewform)

###### [Reciba toda la información de Contagio Radio en [[su correo] 
