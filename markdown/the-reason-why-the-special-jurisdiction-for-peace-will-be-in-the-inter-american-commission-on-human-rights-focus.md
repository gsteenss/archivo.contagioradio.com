Title: Colombia's Transitional Justice System Goes To The Inter-American Commission on Human Rights
Date: 2019-05-08 22:17
Author: CtgAdm
Category: English
Tags: Inter-American Commission on Human Rights, Special Jurisdiction for Peace
Slug: the-reason-why-the-special-jurisdiction-for-peace-will-be-in-the-inter-american-commission-on-human-rights-focus
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/Diseño-sin-título-1.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

The Inter-American Commission on Human Rights (IACHR) will hold a hearing today on the challenges the Special Peace Jurisdiction (JEP, by its Spanish initials) faces as the country strives to implement a historic peace deal between the government and FARC rebels.

According to Maria Camila Moreno, director of the International Center for Transitional Justice (ICTJ), this hearing is an opportunity for the Comission to follow up on the peace process and for "the Colombian State to reaffirm its support for the JEP and its commitment to the victims.”

The director of ICTJ states that the Comission has expressed interest in hearing from representatives of Colombia's transtitional justice system about the state of the implementation of the peace deal, considering President Iván Duque's recent veto of key points in the Statutory Law and Congress' decision to reduce the JEP's budget.

### **The National Development Plan: Less resources for the JEP** 

One of the main complaints about Congress' recent approval of the National Development Plan, a set of guidelines for economic growth in the country, revolves around the resources allocated for the functioning of the transitional justice system. In particular, critics have attacked the inclusion of article 148, proposed by **House Representative Édgar Gómez**, which allows for these resources to now be managed by two different departments of the JEP, the Executive Secretariat and the Investigation and Indictment Unit (UIA, in Spanish).

According to Moreno, granting two departments authority over the management of these resources could entail serious consequences. “**The Jurisdiction is one system and should be guaranteed internal coherence and a well-coordinated administration of  its resources**”, she says. In other words, the governing body of this institution should decide over the distribution of its resources for the entire organization, taking into consideration the needs of each department.

Congress' decision to split this task between two departments means these budgets will be handled differently. Additionally, Giovani Álvarez Santoyo, director of the Investigation and Indictment Unit who lobbied for the inclusion of this article in the Development Plan, will be granted the powers to appoint and remove personnel in each area of ​​work.

**Duque's objections to the JEP no longer face judicial limbo**

Last week, the president's objections to the JEP Statutory Law were up for discussion in the Senate, during which 47 legislators voted against the head of state's decision. The President of the Senate Ernesto Macias stated a majority vote had not been reached which prompted senators to attempt a second vote that was finally never realized.

The results of the vote left several doubts about the legal status of the Statutory Law. **Still, the Moreno claims that the Constitutional Court has made it clear that the high court would decide on the constitutionality of the Law after it made its way through Congress.** Taking into account that the Court already ruled in favor of the Law last year, it is expected that the transitional justice system will have an unaltered Statutory Law by July 21.

 

> ?|| Carta de la presidenta de la [@JEP\_Colombia](https://twitter.com/JEP_Colombia?ref_src=twsrc%5Etfw), Patricia Linares, al canciller Carlos Holmes Trujillo sobre audiencia en la [@CIDH](https://twitter.com/CIDH?ref_src=twsrc%5Etfw). [pic.twitter.com/yWIJL0riue](https://t.co/yWIJL0riue)
>
> — Jurisdicción Especial para la Paz (@JEP\_Colombia) [5 de mayo de 2019](https://twitter.com/JEP_Colombia/status/1125109062708936706?ref_src=twsrc%5Etfw)

<p>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
</p>

