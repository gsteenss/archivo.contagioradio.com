Title: FARC-EP anuncian que no reclutarán más menores de edad
Date: 2016-02-11 14:21
Category: Nacional, Paz
Tags: Delegación de paz de FARC, Diálogos de La Habana, reclutamiento infantil
Slug: farc-ep-anuncian-que-no-reclutaran-mas-menores-de-edad
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/02/Reclutamiento-infantil.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Infobae  ] 

###### [11 Feb 2016 ]

A través de un comunicado público la delegación de paz de las FARC-EP anunció su decisión de **poner fin a la incorporación de menores de 18 años a sus filas**, buscando avanzar lo más rápido posible hacia el fin del conflicto armado. Así mismo, solicitó al gobierno nacional acabar con la “**vinculación masiva de jóvenes a las Fuerzas Militares**”, con el objetivo de proteger y garantizar los derechos de los niños, niñas y adolescentes colombianos.

En el comunicado las FARC-EP aseveraron que la infancia y la juventud en Colombia además de "sufrir la inclemencia de la guerra, son también las **principales víctimas del incumplimiento del deber de protección integral por parte del Estado y sus instituciones**", por lo que se hace necesario llegar a un acuerdo humanitario que permita la garantía de sus derechos y "**poner fin a las condiciones indignas y de exclusión que padecen**".

La delegación extendió la invitación al presidente Juan Manuel Santos a "hacer realidad su promesa electoral y **finalizar de inmediato el Servicio Militar Obligatorio y las prácticas de reclutamiento de menores**", teniendo en cuenta que los mayores afectados son los jóvenes de las familias más empobrecidas del país que son reclutados "para **proteger empresas multinacionales en zonas con alto nivel de confrontación**".

###### Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)] 
