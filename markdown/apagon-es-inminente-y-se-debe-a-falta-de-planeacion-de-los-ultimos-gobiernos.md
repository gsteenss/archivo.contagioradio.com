Title: Apagón es inminente y sería por falta de planeación de los últimos gobiernos
Date: 2016-03-09 13:44
Category: Entrevistas, Otra Mirada
Tags: Alvaro Uribe, Crisis energética, política minero energética
Slug: apagon-es-inminente-y-se-debe-a-falta-de-planeacion-de-los-ultimos-gobiernos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/03/crisis-energética-e1457548401723.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Política Rock 

<iframe src="http://co.ivoox.com/es/player_ek_10737298_2_1.html?data=kpWklZyWfZmhhpywj5aYaZS1lZyah5yncZOhhpywj5WRaZi3jpWah5yncaLkwsySpZiJhpTijMrgjc7SscrixtPhx5Ddb9TZ04qwlYqliMKf0dTfjcvFsNXVjMnSjdXQpc%2FZwsjWh6iXaaOnz5Cah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Óscar Vanegas] 

###### [9 Mar 2016] 

Durante los últimos días en Colombia se ha debatido la posibilidad de que exista o no un apagón en el país debido a las sequías que se han presentado. Ante esa posibilidad, el presidente Juan Manuel Santos, anunció una estrategia de ‘Garrote y zanahoria’ para derrochadores y ahorradores de energía, sin embargo, para el profesor y experto en el tema, Óscar Vanegas, **este ahorro en los hogares colombianos resulta insuficiente para evitar el apagón que es una situación inminente,** que se ha producido por la falta de planificación energética nacional.

“**El ahorro apenas ha llegado al 1.5% y se necesita que sea un ahorro de más del 5%”**, dice Vanegas. Para él la Comisión de Regulación de Energía y Gas, CREG, no es la responsable de la actual crisis energética, como lo ha venido asegurando el Centro Democrático, sino que se trata de la incapacidad de planeación de los ministros y funcionarios que no pensaron a largo plazo en el soporte de la seguridad energética del país.

“El CREG solo regula lo que ya está instalado, quienes si tienen que renunciar es el director de la UNE (Unidad Nacional energética) y todo su equipo científico pues han hecho una mala planeación... que empezó desde el  gobierno de Andrés Pastrana hasta el actual, han sido nefastos en política minero-energética, no han asegurado el abastecimiento interno de ningún tipo de energía pues **se ha entregado a las multinacionales el 85%, y el Estado solo tiene el 15%”, señala el experto.**

Lo anterior, se suma a que durante el gobierno de Álvaro Uribe se le quitó la administración del subsuelo a Ecopetrol, para crear la Agencia Nacional de Hidrocarburos, **lo que significó que del 50% del crudo que estaba en poder los colombianos, solo quedó un 8% para el país, “**es decir que estamos regalando los hidrocarburos”.

Para Óscar Venegas, Colombia es un país lleno de contradicciones en materia minero-energética, ya que por ejemplo, aunque en la Costa Caribe se ha descubierto fuentes de gas, la región ya se está quedando sin ese recurso por que las empresas que lo descubrieron, como **Canacol, Pacific y Geoproduction, lo vendieron para Centro América, en cambio se espera que en pocos meses el país deba importar gas.**

Así mismo, sucede con el carbón. De acuerdo con cifras del experto, Colombia produce y exporta 90 millones de toneladas de carbón para generar energía, mientras que en el país únicamente existen dos termo-eléctricas que producen energía con carbón, **“Una de las políticas debería ser obligar a las multinacionales a que haya un suministro interno de energía**”, propone el profesor.

Finalmente concluye, “La generación de energía está mal planeada y descuadernada, debe haber un giro de 180° en el sector energético, el problema es estructural y es de mediano y largo plazo, los racionamientos van a ser inminentes”.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)
