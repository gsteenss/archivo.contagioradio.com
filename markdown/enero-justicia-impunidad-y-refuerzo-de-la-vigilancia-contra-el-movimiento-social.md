Title: Enero: justicia, impunidad y refuerzo de la vigilancia contra el movimiento social
Date: 2020-12-28 20:36
Author: CtgAdm
Category: Actualidad, Especiales Contagio Radio, Resumen del año
Tags: Derechos Humanos, impunidad en Colombia, justicia, Movilización social
Slug: enero-justicia-impunidad-y-refuerzo-de-la-vigilancia-contra-el-movimiento-social
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/12/disenos-para-notas-2.png" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/12/11.png" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

En el resumen del año, el primer mes de 2020 ya estaba marcando una ruta de lo que estaba por venir. Por un lado se consolidó **la impunidad para el proceso por el asesinato de Dimar Torres**, un caso emblemático con responsabilidad directa e innegable por parte de las FFMM en la región del Catatumbo.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

[El silencio cómplice de las Fuerzas Militares en caso Dimar Torres - Contagio Radio](https://archivo.contagioradio.com/el-silencio-complice-de-las-fuerzas-militares-en-caso-dimar-torres/)

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por otra parte, mientras el clima de la protesta social en contra de las medidas del gobierno continuaba agitado, **la vigilancia con drones en contra de sedes de organizaciones sociales y de DDHH se estaba incrementando**, casi que poniendo de moda. Un elemento que se entendía como la agudización de las medidas de vigilancia y represivas en contra de **la movilización social** que venía creciendo después de fuertes jornadas de paro nacional en los últimos meses del 2019.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

[Organizaciones de DDHH denuncian vigilancia y hostigamiento con drones - Contagio Radio](https://archivo.contagioradio.com/organizaciones-de-ddhh-denuncian-vigilancia-y-hostigamiento-con-drones/)

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

[La Jurisdicción Especial para la Paz](https://www.jep.gov.co/JEP/Paginas/Jurisdiccion-Especial-para-la-Paz.aspx) fue una de las grandes protagonistas del año, no solamente por la esperanza que significa para importantes grupos de víctimas sino porque también fue blanco de ataques en múltiples ocasiones. Sin embargo **el respaldo que en el mes de Enero le entregó la Corte Penal Internacional significó un aliciente para que se continuara en el trabajo de intentar acabar con la impunidad en Colombia**.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

[Visita del CPI en Colombia respalda labor de la JEP- Contagio Radio](https://archivo.contagioradio.com/visita-del-cpi-en-colombia-respalda-labor-de-la-jep/)

<!-- /wp:paragraph -->
