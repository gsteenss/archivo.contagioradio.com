Title: Comités de revocatoria a Peñalosa prometen entregar un millón de firmas
Date: 2017-05-02 12:59
Category: Movilización, Nacional
Tags: Enrique Peñalosa, Firmas revocatoria, Unidos revocaremos a Peñalosa
Slug: comites-de-revocatoria-buscan-recolectar-un-millon-de-firmas-contra-penalos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/03/Enrique_Peñalosa-e1488922778412.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo Particular] 

###### [02 May 2017] 

Hoy se entregaron las 660 mil firmas recogidas por los comités de la revocatoria a Peñalosa, sin embargo, de acuerdo con Carlos Carrillo, vocero de Unidos revocaremos a Peñalosa, está tan solo es la primera entrega, **porque la meta final será reunir un millón firmas y llevarlas a la Registraduría Nacional.**

Para la entrega final, que se realizará en Julio, esperan recoger otras **400 mil firmas, y completar un millón de firmas**, de esta forma, la cantidad de firmas anuladas dejaría de ser un factor para detener el proceso de referendo para la destitución del burgomaestre. “Estamos muy contentos de anunciarle a la ciudadanía que estamos entregando 660 mil firmas de 660 mil ciudadanos que nos han dado su apoyo” afirmó el vocero.

No obstante, Carrillo, expresó que diferentes sectores del movimiento social, se encuentran preocupados por una “marrulla jurídica” que se estaría generando al interior del Consejo Electoral y bajo los intereses del gobierno, con la intensión de que el CNE **“suprima los mecanismos de participación ciudadana”**, razón por la cual invitó a la ciudadanía para que se movilice y salgan a defender sus firmas y sus derechos.

Una vez se entreguen todas las firmas, la Registraduría tendrá un **plazo de 45 días para revisar la valides de las mismas y convocar a un referendo**, sí la decisión de las mayorías es retirar a Peñalosa de su puesto, se nombrará a un alcalde encargado, mientras que se presentan los nuevos candidatos y se realiza la respectiva campaña y elección. Le puede interesar: ["Ya están listas las firmas para revocar a Peñalosa"](https://archivo.contagioradio.com/listas-las-firmas-para-revocar-a-penalosa/)

Durante este periodo de tiempo, Unidos revocaremos a Peñalosa, señaló que harán actividades de campaña para invitar a la ciudadanía a votar en el referendo y tendrá un equipo jurídico **que estará dedicado a que el mecanismo de referendo se respete, y no haya ninguna posibilidad de perturbar el proceso**. Le puede interesar: ["Enrique Peñalosa desconoce reservas en los cerros orientales"](https://archivo.contagioradio.com/enrique-penalosa-sigue-desconociendo-reservas-forestales-en-los-cerros-orientales/)

<iframe id="audio_18459627" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_18459627_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
