Title: Terreno para Orquesta Filarmónica de Bogotá se convierte en parqueadero
Date: 2017-07-07 12:09
Category: Cultura, Nacional
Tags: Alcaldía, Bogotá, orquesta filarmónica de bogotá, Peñalosa
Slug: alcaldia-de-bogota-hara-parqueaderos-donde-iba-a-ser-sede-de-orquesta-filarmonica
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/07/OFB-e1499447324498.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Revista Metrónomo] 

###### [07 Jul 2017] 

Durante la administración anterior de Bogotá, la Orquesta Filarmónica había adquirido un terreno de 10 mil metros cuadrados, al lado del Coliseo el Campín, para **la construcción de una sede adecuada para la música clásica y el fomento de un sistema de orquestas de la ciudad**. Hoy la administración de Peñalosa decidió cancelar este proyecto y va a construir parqueaderos para las personas que asistan al Coliseo que está en remodelación.

Según David García, ex director general de la Orquesta Filarmónica de Bogotá, "el terreno fue entregado por el IDRD durante la administración pasada a la Filarmónica para su uso y administración". Ahora, con Peñalosa en la alcaldía, el lugar fue devuelto al IDRD y García manifestó que, **“lo adhirieron a la alianza público privada que está renovando el Coliseo**, sin que hubiera ninguna contraprestación al distrito, regalaron el terreno a la alianza público privada”. (Le puede interesar: ["Presupuesto para cultura se redujo en 32 millones para 2017"](https://archivo.contagioradio.com/presupuesto-para-cultura-se-redujo-en-32-mil-millones-para-2017/))

La sede donde iba a ser construido este lugar, contaba con estudios realizados por la Universidad Nacional quienes habían diseñado **190 planos para construir un auditorio de música con salas de ensayo y estudios de producción musical**. Para García, “el terreno que se tenía iba a permitir que Bogotá respondiera a la necesidad de construcción de infraestructura cultural que tiene la capital”.

### **La Orquesta Filarmónica de Bogotá cumple 50 años sin sede propia** 

Este año la Orquesta cumple 50 años de existencia y aún no tiene un lugar propio donde la ciudadanía pueda disfrutar de sus presentaciones. García afirmó que “ya estábamos concretando un proyecto que no costaba mucho dinero y **la actual alcaldía ha decidido priorizar cosas que la ciudadanía no quiere** como el Transmilenio por la séptima”. (Le puede interesar: ["Nace Independencia Récords una productora para la paz"](https://archivo.contagioradio.com/independencia-records-paz/))

Con esto en mente, **Bogotá se queda atrasada en la construcción de escenarios que respondan a las necesidades de arraigo cultural** de sus habitantes pues según García, “las grandes ciudades que tienen orquestas han construido sedes propias”. Él afirmó que “es lamentable que por sectarismos políticos se haya truncado este proyecto y esto le va a significar a la ciudad un costo en tiempo y además se corta un proceso de profundización cultural de la ciudadanía”.

<iframe id="audio_19681267" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_19681267_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU).

###### 
