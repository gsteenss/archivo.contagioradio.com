Title: Impunidad en casos de tortura en Colombia es superior al 90%
Date: 2018-06-26 17:58
Category: DDHH, Nacional
Tags: Comisión Colombiana de, Derechos Humanos, impunidad, ONU, tortura
Slug: impunidad-en-casos-de-tortura-en-colombia-es-superior-al-90-ana-maria-rodriguez
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/06/DIA-CONTRA-LA-TORTURA.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Gabriel Galindo] 

###### [26 Jun 2018] 

En el día Día Internacional de Apoyo a las Víctimas de la Tortura en el mundo, **Colombia registra más del 90% de impunidad en casos relacionados con ese fenómeno**, y  no está adecuadamente reconocido, en parte por la exacerbada violencia que se vive hace décadas; así lo afirma Ana María Rodríguez, integrante de la Comisión Colombiana de Juristas y de la Coalición Contra la Tortura.

Este delito es uno de los crímenes con mayor visibilidad internacional y de los más reconocidos desde hace décadas; sin embargo, l**a situación de justicia en Colombia sobre este flagelo tiene fallas en tanto la extrema violencia que ha vivido el país ha impedido que se visibilice y se evite naturalizar esa transgresión a los Derechos Humanos.**

### **En Colombia hay 2 dimensiones para entender la tortura** 

De acuerdo con Rodríguez, la tortura en Colombia está determinada en 2 dimensiones: la que se da en el marco del conflicto armado y la que no. En el marco del conflicto armado no se investiga específicamente la tortura, debido a que quienes hacen las indagaciones judiciales, comúnmente perfilan la tortura como un delito menor respecto a otros crímenes.

Por esta razón, casos como el de las ejecuciones extra judiciales emiten sentencias por delitos como asesinato y no por tortura, aunque se haya probado que se cometieron ambos crímenes. (Le puede interesar: ["Reclutador de 'Falsos Positivos' de Soacha se compromete con la verdad"](https://archivo.contagioradio.com/falsos_positivos_preacuerdo_madres_soacha/))

Para los casos en que la tortura no ocurrió en el marco del conflicto convergen dos situaciones que son alarmantes: la primera, que hay muchos casos de tortura en los centros carcelarios, pero “no pueden ser denunciados porque las personas privadas de la libertad no cuentan con los mecanismos para manifestar los casos de tortura y malos tratos”; y la segunda que, **el Instituto de Medicina Legal y Ciencias Forenses (IMLCF) “no cuenta con la capacidad y fortaleza de aplicar los procedimientos necesarios para determinar si hay casos de tortura, sobre todo en regiones apartadas del país”**, como añade Ana Rodríguez.

### **Colombia se rehúsa a ratificar el Protocolo Facultativo contra la tortura** 

Aunque no todo maltrato es tortura, el Derecho Internacional reconoce **tratos que alcanzan el grado de crueles, inhumanos y degradantes que constituyen violaciones a los Derechos Humanos y no necesariamente son ejercidos con violencia física.** Ejemplo de ellos, son los tratos que sufren las personas que están en las cárceles, “sin acceso a agua o salud y con altos índices de hacinamiento”, argumenta la integrante de la Comisión Colombiana de Juristas.

Precisamente sobre las torturas que ocurren en los centros de detención intramurales, la ONU tiene un Protocolo, en el que una comisión interdisciplinaria de expertos visita todo el sistema penitenciario para formular recomendaciones sobre cómo evitar la tortura en las cárceles, y cuáles son los mecanismos adecuados para denunciar este flagelo.

No obstante Colombia rechazó las sugerencias de ratificar este Protocolo, hechas por diferentes países pertenecientes a la ONU, en la presentación de su tercer examen periódico universal. De hecho, como lo resalta Rodríguez, “es la tercera vez que Colombia se niega a reconocer dicho protocolo”.

### **Exigencias para el nuevo Gobierno** 

De cara a la llegada del gobierno de Iván Duque, Rodríguez señala que aunque la tortura es un tema que ha sido relegado por parte de los gobiernos de Colombia, espera que las autoridades judiciales "mantengan su independencia, que las autoridades penales y carcelarias creen los mecanismos necesarios para denunciar casos de tortura en las cárceles y que el IMLCF sea fortalecido" para que pueda investigar adecuadamente este delito.

De igual forma, **Ana María Rodríguez desea que "los mecanismos creados por el acuerdo de paz se pongan en marcha sin ningún contratiempo"** puesto que son la Jurisdicción Especial de Paz, la Unidad de Búsqueda de Personas Desaparecidas y la Comisión de la Verdad las entidades llamadas a investigar y resolver los casos de tortura que se dieron en el país, en el marco del conflicto armado. ** **

<iframe id="audio_26746870" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_26746870_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en [[su correo] 
