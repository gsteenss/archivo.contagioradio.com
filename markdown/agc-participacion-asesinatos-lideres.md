Title: AGC niegan su participación en asesinatos de líderes sociales y excombatientes
Date: 2019-06-26 15:43
Author: CtgAdm
Category: Líderes sociales, Nacional
Tags: AGC, Comunicado, excombatientes, lideres sociales
Slug: agc-participacion-asesinatos-lideres
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/AGC-amenazan-en-territorios.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Foto: @LaPrensaWeb  
] 

En un comunicado publicado en la página de las llamadas **Autodefesas Gaitanistas de Colombia (AGC)**, esa organización al margen de la ley, **negó su participación en crímenes cometidos en contra de líderes sociales** y manifestaron que si continúa el asesinato de ex combatientes de FARC se los está empujando a volver a las armas como ha sucedido “en otros procesos de paz”.

El comunicado fechado este 26 de Junio también hace referencia a qu**e no tienen ningún interés en atacar el proceso de reintegración que adelantan los integrantes de FARC** e hicieron un llamado a que las autoridades investiguen la serie de asesinatos de personas de esa colectividad. (Le puede interesar: ["Con asesinato de Maria del Pilar Hurtado, paramilitare siguen cumpliendo amenazas en Córdoba"](https://archivo.contagioradio.com/con-asesinato-de-maria-del-pilar-hurtado-paramilitares-siguen-cumpliendo-amenazas-en-cordoba/))

Este [comunicado](http://autodefensasgaitanistasdecolombia.org/agc2/2019/06/25/comunicado-junio-26-de-2019/) **contrasta con las informaciones recientes en torno a la posible responsabilidad de las AGC en asesinatos de líderes sociales.** El más reciente es el caso de María del Pilar Hurtado en Tierralta quien días antes de su asesinato había recibido amenazas de muerte por medio de un panfleto firmado por esa organización. (Le puede interesar: ["Paramilitares de las Autodefensas Gaitanistas de Colombia sitian a 800 habitantes de Juradó, Chocó"](https://archivo.contagioradio.com/paramilitares-de-las-agc-sitian-a-800-habitantes-de-jurado-choco/))

Sin embargo, las versiones y denuncias en torno a que las amenazas llegaron luego de que un grupo de familias ocuparan un lote de propiedad de familiares del alcalde Otero siembran dudas en torno a la responsabilidad material e intelectual del crimen de Hurtado por cuanto ella era una de las lideresas del grupo de familias que exigían vivienda digna. (Le puede interesar:["Paramilitares intentan asesinar a líder en Zona Humanitaria de  Jiguamiandó, Chocó"](https://archivo.contagioradio.com/neoparamilitares-intentan-asesinar/))

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
