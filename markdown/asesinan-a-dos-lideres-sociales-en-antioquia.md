Title: Asesinan a dos líderes sociales en Antioquia
Date: 2018-03-28 12:39
Category: DDHH, Nacional
Tags: Antioquia, asesinato, lideres sociales
Slug: asesinan-a-dos-lideres-sociales-en-antioquia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/12/líderes-rueda-de-prensa-2-.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [28 de Mar 2018] 

Dos líderes comunitarios fueron asesinados el pasado domingo 25 de marzo en Antioquia, se trata de **Víctor Alfonoso Zabala**, líder comunitario de la vereda El Rizo que fue asesinado por desconocidos y **Jorge Polanco**, quien horas después, fue ajusticiado por hombres en la vereda, Caño Prieto, zona rural de Caucasia, en el mismo departamento.

Zabala lideraba el proyecto de la ruta de construcción de los Programas de Desarrollo con Enfoque Territorial, en su territorio, al igual que Polanco. Con el asesinato de estos líderes yo son más de 34 las víctimas de homicidios en el país por actuar en defensa de su territorio y protección de los derechos humanos. (Le puede interesar:["Enero un mes trágico para los líderes y reclamantes de tierras"](https://archivo.contagioradio.com/enero_asesinatos_lideres_sociales/))

La Agencia de Renovación del Territorio, que trabajaba con ambos líderes comunitarios, condenó y lamento los hechos **“el continuo asesinato de líderes es una clara demostración de sabotaje a un proceso participativo** de transformación de país que se está dando desde los territorios. Solo con la paz en el campo colombiano lograremos la reconciliación”, aseguró Mariana Escobar, directora de la Agencia.

De igual forma, la Agencia de Renovación al Territorio, en un comunicado de prensa, hizo un llamado a las autoridades “para reforzar las medidas de seguridad para todos aquellos que trabajan por la defensa de las comunidades más vulnerables, así como para dar con el paradero de los autores de este crimen que enluta a la construcción de la paz en Antioquia”. (Le puede interesar: ["Empieza febrero y ya se registran dos asesinatos de lideresas sociales")](https://archivo.contagioradio.com/febrero_asesinatos_lideres_sociales/)

###### Reciba toda la información de Contagio Radio en [[su correo]
