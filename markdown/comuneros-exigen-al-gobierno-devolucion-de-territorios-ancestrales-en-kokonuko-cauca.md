Title: Ya no estamos para nuevos acuerdos, queremos hechos: Indígenas de Kokonuko
Date: 2017-10-11 12:52
Category: Entrevistas, Movilización
Tags: liberación de la madre tierra, Resguardo de Kokonuko
Slug: comuneros-exigen-al-gobierno-devolucion-de-territorios-ancestrales-en-kokonuko-cauca
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/10/WhatsApp-Image-2017-10-09-at-4.04.26-PM-e1507584144426.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: CRIC] 

###### [11 Oct 2017] 

Los indígenas del Resguardo de Kokonuko continúan en el predio de Agua Tibia a la espera de acciones urgentes por parte del gobierno, para hacer efectiva la devolución de sus tierras, el día de ayer se realizaron los actos fúnebres de Efigenia Vásquez y hoy se realizó la siembra de su cuerpo, de igual forma **ya fueron liberados los comuneros que eran retenidos por la Fuerza Pública y el intengrante del ESMAD que se encontraba en manos de los indígenas. **

De acuerdo con Viviana Ipia, vice gobernadora del resguardo de Kokonuko, ha hecho presencia en el territorio Horacio Guerrero, como delegado del Ministerio del Interior para establecer que ruta se podría seguir, sin embargo, la comunidad decidió no recibirlo, hasta que no haya acciones efectivas de la devolución del predio, “**se conversará siempre y cuando sea contundente el diálogo**, porque si es para generar nuevamente acuerdos, ya no estamos para esos tiempos”.

 En horas de la noche, los indígenas denunciaron que llegaron más efectivos del ESMAD al territorio, situación que para la vice gobernadora Ipia demuestra la falta de garantías para la comunidad y los ponen en alerta máxima. (Le puede interesar: ["Por ataque del ESMAD muere comunicadora indígena en Kokonuko, Cauca"](https://archivo.contagioradio.com/por-ataque-del-esmad-muere-comunicadora-indigena-en-kokonuko-cauca/))

Con respecto al avance de la investigación sobre el asesinato de la comunicadora indígena Efigenia Vásquez, la vicegobernadora expresó que la comunidad tiene miedo por la forma en la que se  ha realizado la recolección de información “entes policiales nos manifestan que los uniformados que estaban en el  área** fueron retirados el mismo día, para hacer lo de la revisión de las armas**, entonces no se hizo en el resguardo ni el sitio de los hechos” señaló Ipia.

De igual forma, Viviana señaló que han llegado muchas muestras de apoyo y solidariad a la familia de Efigenia, en respuesta a la labor que realizaba la comunicadora en torno a la defensa de los derechos de su comunidad.

<iframe id="audio_21408888" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_21408888_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
