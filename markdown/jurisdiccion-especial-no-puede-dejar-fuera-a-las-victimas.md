Title: La Jurisdicción Especial no puede dejar fuera a las víctimas
Date: 2017-02-02 16:03
Category: Nacional, Paz
Tags: CINEP, Implementación de los Acuerdos de paz, Jurisdicción Espacial de Paz
Slug: jurisdiccion-especial-no-puede-dejar-fuera-a-las-victimas
Status: published

###### [Foto: Contagio Radio] 

###### [2 Feb 2017] 

La necesidad de mayor participación de las víctimas en la Jurisdicción Especial de Paz, el riesgo de pactar impunidades y los obstáculos que supone mantener el fuero constitucional, son algunos de los puntos que se trabajaron en el diálogo **‘La justicia en la implementación de los acuerdos’**, realizado por el Centro de Investigación y Educación Popular –CINEP–, en el marco del Programa por la Paz.

Judith Maldonado, Rodrigo Uprimny y John Jairo Montoya fueron los panelistas invitados, quienes dieron respuesta a interrogantes como ¿cuál ha sido el rol de las víctimas en el trámite legislativo? ¿Cómo armonizar las penas privativas de libertad contempladas por el Estatuto de Roma para crímenes de lesa humanidad y de guerra, con las sanciones alternativas que plantea el proyecto de la Jurisdicción Especial para la Paz? y ¿cómo se prepara la Corte Constitucional para atender las demandas de la JEP?.

### Las víctimas y la Jurisdicción Especial 

La vocera del Movimiento Voces de Paz, Judith Maldonado, señaló que si bien algunas víctimas han participado en el proceso de paz, no ha sido suficiente dicha participación, **“es fundamental contar con las voces de quienes realmente se han visto afectados por el conflicto”**, resaltó que sobre todo en los debates y trámites de la JEP han estado ausentes las voces de las víctimas, “preocupa porque en eso, más que en otra cosa deben estar presentes, pues es lo que va a permitir que las víctimas conozcan la verdad, puedan perdonar y sean reparadas”.

La vocera de Voces de Paz, manifestó que muchas veces **el Estado toma decisiones “sin saber el contexto de los territorios”**, y explicó que allí radica la preocupación que el proyecto de la JEP no tenga en cuenta a todas las víctimas, y se hagan ajustes sesgados que terminen por “afectar a las víctimas y beneficiando a los victimarios”. (Le puede interesar: [Jurisdicción Especial de Paz incluirá a terceros implicados en la guerra](https://archivo.contagioradio.com/jep-colombia-debate-paz/))

### Algunos buscan impunidad 

Frente al tema del riesgo de impunidades en el trámite de la iniciativa legislativa, los panelistas comentaron que algunas de las sugerencias hechas por partidos como el Centro Democrático, “buscaban que se cambiaran aspectos que obligan a los **altos mandos militares y dirigentes políticos a responder ante la justicia**”. (Le puede interesar: [En debate de jurisdicción especial para la paz algunos buscan impunidad](https://archivo.contagioradio.com/jurisdiccion-especial-para-la-paz-2/))

El ex magistrado Rodrigo Uprimny dijo que con la JEP no se busca “hacer una cacería de brujas” contra los jefes militares, se trata de “castigar a aquellos que incurrieron en omisiones graves, que permitieron que sus subalternos cometieran atrocidades”, agregó que se debe aplicar adecuadamente esta responsabilidad del mando, **“tanto frente a la guerrilla como frente a agentes estatales, es fundamental”.**

En el conversatorio, los panelistas advirtieron que durante el segundo debate de la JEP en el Congreso, el Centro Democrático hizo vehementes esfuerzos por hundir el proyecto, sin embargo, “la sensatez logró sacar avante el proyecto”, puntualizaron.

Por último, los panelistas hicieron un llamado a la responsabilidad que tiene la sociedad en general respecto a la participación y la veeduría, de la fase de implementación de los acuerdos y de los 2 debates que faltan para aprobar la JEP.

<iframe id="audio_16799338" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_16799338_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en [[su correo] 
