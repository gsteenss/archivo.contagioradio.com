Title: La niña Guardiana de los Ríos, con amor y respeto para Berta Cáceres
Date: 2016-03-24 13:20
Category: Andrea, Opinion
Tags: Berta Cáceres, Copinh, honduras
Slug: la-nina-guardiana-de-los-rios-con-amor-y-respeto-para-berta-caceres
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/03/2015_bertacaceres_061_9d18d516335c5373bba7d3f6c588347f.nbcnews-ux-2880-1000.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Tim Russo 

#### [Andrea Ixchíu Hernández](https://archivo.contagioradio.com/andrea-ixchiu-hernandez/) 

###### 24  Mar 2016 

### Berta Cáceres, mujer lenca, guerrera valiente, guardiana del agua y los bosques, una de las tantas mujeres corazón de la lucha indígena por la vida, tejedora de resistencia y organización. 

[Según los cálculos de mis abuelos K’iche, ella nació en Honduras, no casualmente en una comunidad llamada La Esperanza, Intibucá, un 4 de marzo de 1973 en un día jun Ak’ab’al, un amanecer, y como buena aurora ella llevaba lucha, esperanza, luz y fuego a dónde fuera. Nos fue arrebatada en la madrugada de su cumpleaños número 43, entre el 3 y 4 de marzo del 2016, con la energía del oxib Toj, la energía del pago de la ofrenda que debemos dar de vuelta a la tierra por el uso que hacemos de ella, por el sufrimiento que le causamos. Es el pago que hace la humanidad por el equilibrio de la justicia, el amor, la comunicación y la organización. Hemos pagado una ofrenda demasiado valiosa.]

[Berta fue asesinada a balazos, como bien enuncia Francesca Gargallo por: “la máquina productiva capitalista que no tiene límite alguno, ni el ecológico-ambiental, ni el respeto a la vida humana, que necesita seguir produciendo y por ende necesita extraer lo que sea: carbón, oro, petróleo, diamante, manganeso y cualquier otra cosa esté en las rocas, la tierra, el agua, las arenas, el cuerpo humano, la flora, la fauna, para generar ganancias”.]

[Recuerdo a Berta, su voz, la ternura de sus palabras cada vez que describía la lucha Lenca por el agua, su cosmovisión y profunda espiritualidad, ella podía comunicarse con el agua, ella era una niña guardiana de los ríos.]

[Su fuerza al hablar del COPINH y de los compas que junto a ella defendían la vida inspiraban a organizarte, sus reflexiones desde el feminismo comunitario nutre la construcción de nuevas formas de organizarnos. La recuerdo sentada junto a otras grandes mujeres enseñando a nosotras las más jóvenes sobre la importancia de sacar las violencias de nuestros cuerpos y luchas. Su sabiduría y convicción le hacían destacar en cualquier espacio. Era una mujer de consejos, que sabía concretizar muy bien sus planes de lucha. Ella misma era un conjunto de luchas, ella era comunidad, era insurgencia que buscaba poner fin a años de opresión, esclavitud, expolio y muerte; frente a este sistema colonizador de mentes, cuerpos y territorios.]

[Berta logró proteger el sagrado rio Gualcarque, poniendo freno a una represa, a una empresa China y a los empresarios hondureños de DESA, quienes sin pensar que el daño que hacen a la tierra hoy mañana lo cosecharan sus propios hijos, venden y transan el agua y los bosques, sin importarles el mañana. Eso y la complicidad de gobiernos formados por funcionarios corruptos, que protegen las ganancias por sobre la vida, tienen hoy a Honduras sumido en una tragedia.]

[4 días después de este terrible asesinato y luego de haber sembrado a Berta en la tierra, en este 8 de marzo, día en que se conmemora mundialmente la lucha de las mujeres por ser reconocidas como sujetas de derecho y no como objetos sexuales y domésticos, se reafirma la urgente necesidad de seguir trabajando y articulando por avanzar en la construcción de sociedades distintas, en dónde lo más importante sea la dignidad, el amor, el respeto, y no la acumulación.]

[¡Enterramos tu cuerpo Berta, pero tu lucha es semilla de rebeldía que germinará y será millones!]
