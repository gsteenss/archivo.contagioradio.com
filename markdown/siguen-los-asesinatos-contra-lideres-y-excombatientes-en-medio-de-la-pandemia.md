Title: Siguen los asesinatos contra líderes y excombatientes en medio de la pandemia
Date: 2020-07-13 22:20
Author: PracticasCR
Category: Actualidad
Tags: Excombatientes de FARC, Fredy Fajardo Ávila, Líderes Sociales Asesinados, Wilson Eduardo Baicue
Slug: siguen-los-asesinatos-contra-lideres-y-excombatientes-en-medio-de-la-pandemia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/07/Líderes-y-excombatientes-asesinados.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

**Este fin de semana se presentaron los asesinatos de un líder indígena y un excombatiente de FARC**, los cuales  se suman a la cruda oleada de violencia que se vive en contra de líderes y excombatientes en los territorios. (Lea también: [Sigue la muerte, durante el fin de semana asesinan tres líderes y excombatientes](https://archivo.contagioradio.com/sigue-la-muerte-durante-el-fin-de-sesmana-asesinan-tres-lideres-y-excombatientes/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El día sábado fue denunciado **el asesinato de Fredy Fajardo Ávila, un excombatiente de FARC**, en el municipio de Uribe, Meta. La víctima tenía 32 años, se encontraba en proceso de reincorporación y ya había sido víctima de amenazas por parte de grupos armados.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por otra parte, ayer domingo, **fue asesinado el joven Wilson Eduardo Baicue Quiguanas de 24 años, comunero del cabildo Nasa**, quien fue sacado a la fuerza de su vivienda por dos hombres armados y horas después fue encontrado sin vida  con cuatro impactos de bala sobre su cuerpo. (Lea también: [Rodrigo Salazar, reconocido líder del Pueblo Awá fue asesinado en Llorente](https://archivo.contagioradio.com/rodrigo-salazar-reconocido-lider-del-pueblo-awa-fue-asesinado-en-llorente/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Wilson fue asesinado en la inspección Arizona, municipio de Puerto Caicedo, Putumayo, **territorio de disputa entre el** **Frente Carolina Ramírez de las FARC-EP y la estructura criminal conocida como La Mafia.**

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Líderes y excombatientes bajo «exterminio sistemático»

<!-- /wp:heading -->

<!-- wp:paragraph -->

Con el asesinato de Fredy Fajardo Ávila, ya son **217 los excombatientes asesinados después de la firma del Acuerdo de Paz** según señala el Partido FARC.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/PartidoFARC/status/1282066020400857088","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/PartidoFARC/status/1282066020400857088

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

**139 de estos asesinatos se presentaron bajo el mandato de Iván Duque en la Presidencia y solo en 2020 se habrían presentado 33 homicidios en contra de los firmantes de paz,** lo que ha llevado a FARC a señalar que están siendo víctimas de un **«exterminio sistemático»**. (Le puede interesar: [Partido FARC solicitará formalmente medidas cautelares de la CIDH a favor de excombatientes](https://archivo.contagioradio.com/partido-farc-solicitara-formalmente-medidas-cautelares-de-la-cidh-a-favor-de-excombatientes/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por su parte, el desolador panorama se extiende también a los liderazgos sociales, los cuales están siendo amenazados y violentados aún en medio de la pandemia y el confinamiento.  Según Indepaz en lo que va del presente año al menos [164](http://www.indepaz.org.co/paz-al-liderazgo-social/) líderes, lideresas y personas defensoras de derechos humanos han sido asesinados.

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->
