Title: A 5 años de la Ley de restitución el despojo de tierras continúa
Date: 2016-12-06 15:50
Category: Entrevistas, Paz
Tags: CINEP, concentración de tierras, Desplazamiento forzado, despojo de tierras
Slug: a-5-anos-de-la-ley-de-restitucion-el-despojo-de-tierras-continua
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/12/despojo-de-tierras.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Protectionline] 

###### [6 Dic 2016] 

El Centro de Investigación y Educación Popular CINEP realizó en la capital un foro para analizar el desarrollo de los 5 años de funcionamiento de la Ley 1448 de víctimas y Restitución de Tierras. Los resultados revelaron que no han sido sustanciales los avances y por el contrario, **avanza el despojo de tierras, su legalización y la persecución a reclamantes de tierras.**

El foro que buscaba hacer un balance de la implementación de la Ley y en particular, de los decretos reglamentarios étnicos 4633 y 4635, aprobados en diciembre el 2011, reveló que las garantías de no repetición aún no son una realidad, “todavía subsiste la altísima impunidad del delito de desplazamiento, del abandono forzado y despojo de territorios étnicos, las autoridades **no hacen trabajos investigativos certeros para dar respuesta a las comunidades”. **

Además, el CINEP resalta que “es a causa de la lentitud en los procesos de restitución de tierras y la imposibilidad de implementar la política de restitución en el plano local, que **amenazan a las comunidades y ponen en riesgo la vida de sus líderes, lideresas y todos sus integrantes, los reclamantes siguen siendo asesinados,** amenazados, estigmatizados y desplazados”.

### **Parece que las leyes son sólo para algunos** 

El CINEP es enfático en decir que “el fenómeno del despojo responde a grandes intereses económicos legales detrás de los territorios colectivos étnicos, y por eso **las empresas pueden violar derechos humanos, consultas previas y estudios de impacto ambiental”.** Le puede interesar: [‘Desterrados’ un panorama sobre la concentración de tierras.](https://archivo.contagioradio.com/desterrados-panorama-la-concentracion-tierras/)

El foro dio cuenta de la existencia de muchos obstáculos jurídicos al momento de implementar la ley lo que impide que los procesos avancen en favor de las comunidades, **“la política de restitución se ha encontrado con una enorme complejidad debido a la no formalización de los territorios étnicos** lo que hace que un mismo territorio existan segundos y terceros ocupantes”.

Además, los expertos del CINEP comentaron que los actores armados, legales e ilegales, “han logrado el control territorial y disposición de los territorios para actividades extractivas e ilícitas, **en alianza con agentes públicos y privados locales y regionales”.**

Durante el foro, el CINEP hizo un llamado urgente a las instituciones estatales y organismos internacionales **“para que garanticen la protección colectiva e integral de las comunidades reclamantes de tierras, así como la seguridad de sus territorios”.**

Por último, los exponentes manifestaron que si bien la restitución de tierras es un aporte a la construcción de la paz territorial, “estamos viendo la puesta en marcha de procesos de retorno **sin las mínimas garantías que exige la ley, no se están construyendo carreteras, puestos de salud ni escuelas”.**

###### Reciba toda la información de Contagio Radio en [[su correo]
