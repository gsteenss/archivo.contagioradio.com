Title: Nueva cúpula militar: ¿vuelve la seguridad democrática?
Date: 2018-12-12 13:01
Author: AdminContagio
Category: Entrevistas, Nacional
Tags: cúpula militar, Seguridad Democrática
Slug: nueva-cupula-militar-vuelve-la-seguridad-democratica
Status: published

###### Foto: MinDefensa 

###### 12 Dic 2018 

El pasado 11 de Noviembre, el presidente Iván Duque anunció quienes conformarían la nueva cúpula de las fuerzas militares y policiales que estarán a cargo de la defensa del territorio colombiano, sin embargo la selección de algunos de sus integrantes ha generado dudas pues al menos dos de ellos han estado **involucrados en casos de ejecuciones extrajudiciales.**

La cúpula esta compuesta por el almirante Evelio Ramírez en la armada nacional, el general Ramsés Rueda en la  Fuerza Aérea Colombiana y Óscar Atehortúa en la dirección de la Policía Nacional, mientras que el comandante de las Fuerzas Militares, general, Luis Fernando Navarro Jiménez y el general Nicacio de Jesús Martínez, estos dos últimos **vinculados a la política  de seguridad democrática e**stablecida durante el gobierno de Álvaro Uribe.

El jurista Alberto Yepes señala que la selección de esta nueva cúpula militar simboliza **un enorme retroceso en las aspiraciones del pueblo colombiano de avanzar hacia la paz** y que temas como el posconflicto y la implementación de los acuerdos, serán dejados de lado pues garantizan un retorno a  un plan de guerra y por ende a la violación a los derechos humanos.

Ante la posibilidad de una salida negociada al conflicto, en particular con el ELN, Yepes augura pocas oportunidades de avanzar por esa vía, pues estos nombramientos “despiertan el entusiasmo de las personas más guerreristas del Centro Democrático" a lo que le suma las recientes decisiones del Congreso de "hacer conejo a la negociación", lo que  advierte, podría empujar a una parte de miembros del ELN a que se sientan traicionados con este tipo de medidas. [(Le puede interesar Uribismo impulsa modificación de Ley para emitir ordenes de captura contra ex-integrantes de las FARC) ](https://bit.ly/2SD4I6o)

### **La nueva cúpula militar, una semilla de la Seguridad Democrática** 

El abogado indicó que los mayores cuestionamientos de la nueva cúpula militar recaen sobre el general **Nicacio de Jesús Martínez**, quien se desempeñó en unidades militares entre 2004 y 2005, periodo durante el cual fueron documentados **cerca de  de 76 casos de ejecuciones extrajudiciales cometidas** en su mayoría por el grupo de caballería mecanizada Juan José Rondon bajo su mando.

Martínez Espinel también fue investigado por la justicia penal militar por un desvío de recursos cercanos a los mil quinientos millones de pesos en contratos del fondo interno de la Cuarta Brigada relacionados a la provisión, almacenamiento y transporte con escoltas de explosivos hacia empresas sin fondos que desaparecieron entre 2003  y 2004. Las acciones del general, como resalta el jurista no han sido investigadas debido a una “complacencia de la Fiscalía” y hoy se encuentran en la totalidad impunidad.

Por su parte, el general **Fernando Navarro** fue denunciado con anterioridad por haber filtrado información y manipulado investigaciones sobre los falsos positivos, además la Fuerza de Despliegue Rápido, cuerpo del Ejército que bajo la dirección de Navarro, dejó a su paso por el suroccidente del país cerca de cinco o seis cementerios donde fueron hallados civiles asesinados en operaciones militares.

### **La lucha contra el narcotráfico, una cortina de humo** 

Yepes indicó que la apuesta del Gobierno de combatir el narcotráfico con la que se justificó la denominación de la nueva cúpula, es tan solo un pretexto para continuar con las políticas de seguridad democrática, lo que da a entender que “el país está siendo manejado desde el Uberrimo o desde el escritorio del senador Uribe”, percepción que argumenta con la imposición del Centro Democrático en las decisiones políticas más importantes del Estado las cuales “se oponen  a la consolidación de la paz y a  las reformas sociales y económicas que se esperaban con la firma de la paz”. Y que para el jurista demuestran la falta de liderazgo del presidente Duque.

<iframe id="audio_30761160" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_30761160_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

######  Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)**  
** 
