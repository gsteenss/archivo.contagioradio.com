Title: Terminos
Date: 2015-01-26 16:55
Author: AdminContagio
Slug: terminos
Status: published

**Términos de uso ** 
--------------------

######  

1.  1.  Contagio radio, medio de comunicación alternativa, pone a la disposición de nuestros oyentes y lectores una sección para subir su contenido digital, (columnas, reportajes, fotografías, denuncias etc) para acceder a estas secciones la persona acepta los siguientes puntos que se enmarca en la ética de Contagio Radio.
    2.   El Usuario acepta TOTALMENTE la responsabilidad del contenido generado por el (ella) en [contagioradio.com](https://archivo.contagioradio.com/)
    3.  El Usuario acepta las condiciones establecidas por [contagioradio.com](https://archivo.contagioradio.com/) y se hace responsable por cualquier acción o conducta violatoria de las mismas y acepta las consecuencias legales por la violación de las mismas.
    4.  El usuario es libre de elegir la temática de su nota, pero ella no podrá contener contenidos que promueva la pornografía, el racismo o la xenofobia,  que  incite a la violencia o que contenga términos obscenos
    5.  El Usuario no podrá colocar dentro de sus opiniones contenido que viole los derechos de autor, de propiedad, copyright, marca, patente o cualquier propiedad intelectual o derecho adquirido por cualquier tercero.
    6.  El Usuario asume totalmente la responsabilidad en caso de que sus opiniones o comentarios generen alguna acción jurídica contra [contagioradio.com](https://archivo.contagioradio.com/) y se compromete a generar una rectificación si su contenido de derive.
    7.  Las publicaciones de nuestros usuarios están supeditada a la decisión editorial de Contagioradio.com que evaluara la  pertinencia del contenido digital.
    8.  El usuario no publicara datos personales o sensibles en los términos de la Ley 1581 de 2012, sin contar con la autorización previa del titular de esta información.
    9.  Contagioradio.com podrá cambiar los títulos de las notas para que estas tengan mayor audiencia e igualmente podrá corregir la ortografía dentro del contenido digital
    10. No se publicaran contenidos que hagan directa  o indirectamente publicidad a productos o marcas.
    11. [contagioradio.com](https://archivo.contagioradio.com/) podrá despublicar contenidos anteriores sin tener que avisar previamente al creador o creadora del contenido digital.
    12. Los usuarios son los titulares de los derechos de autor de los contenidos que elaboren y en virtud de esta aceptación, conceden a [contagioradio.com](https://archivo.contagioradio.com/) en forma permanente, una licencia de uso gratuita, no exclusiva, para la reproducción, adaptación, compilación, almacenamiento y distribución de los contenidos por él suministrados a través de esta página de Internet. El contenido de nuestros usuarios siempre será reconocido bajo los derechos de autor.


