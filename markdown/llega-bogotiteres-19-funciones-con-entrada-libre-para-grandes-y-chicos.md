Title: Llega Bogotíteres, 19 funciones con entrada libre para grandes y chicos
Date: 2015-09-19 17:05
Category: Cultura, eventos
Tags: Actividades culturales, Biblored, Bogotá, Bogotíteres 2015, Fundación Hilos Mágicos, IDARTES, Secretaria de Cultura Recreación y Deporte, Teatro con entrada libre, Títeres
Slug: llega-bogotiteres-19-funciones-con-entrada-libre-para-grandes-y-chicos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/Bogotíteres.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: IDARTES ] 

###### [19 Sept 2015 ] 

[Desde el 20 de septiembre y hasta el 4 de octubre se llevara a cabo “Bogotiteres 2015: Encuentro de títeres y ciudad”. **19 funciones de teatro de títeres para grandes y chicos**, programadas en distintos escenarios de la ciudad y con entrada libre.  ]

[Este Encuentro es organizado por la Gerencia de Arte Dramático del Instituto Distrital de las Artes, IDARTES y la Fundación Cultural Hilos Mágicos. **El lanzamiento será este domingo 20 de septiembre a las 11 a.m. en el Teatro El parque con la obra El circo de las pulgas, del grupo El castillo del gato**.]

[IDARTES, la Fundación Hilos Mágico, la Red Capital de Bibliotecas Públicas de Bogotá, BibloRed, la Secretaría de Cultura, Recreación y Deporte y el sector de títeres de Bogotá, extienden la **invitación al público capitalino para que asista a esta muestra que contará con obras de diversas técnicas como teatro negro, de sombras, de hilos o marionetas, de guante, de varillas, bunraku y muchas más**.]

Conozca la programación:

[![Sábana de programación Bogotíteres3copia](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/Programación-Bogotíteres.jpg){.aligncenter .size-full .wp-image-14305 width="2539" height="1962"}](https://archivo.contagioradio.com/llega-bogotiteres-19-funciones-con-entrada-libre-para-grandes-y-chicos/sabana-de-programacion-bogotiteres3copia-2/)

[  
]
