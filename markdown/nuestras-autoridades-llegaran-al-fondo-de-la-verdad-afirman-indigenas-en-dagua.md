Title: Así habría sido la muerte de ocho indígenas según la Minga en Dagua, Valle del Cauca
Date: 2019-03-22 17:53
Author: CtgAdm
Category: Comunidad, Nacional
Tags: Cabildos Indígenas, Dagua. Valle del Cauca, Organizaciones Indígenas de colombia
Slug: nuestras-autoridades-llegaran-al-fondo-de-la-verdad-afirman-indigenas-en-dagua
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/03/5c9442d0754dd.r_1553235083593.0-264-3000-1764.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: @cucutaenlamira] 

###### [22 Mar 2019] 

La tarde del 21 de marzo en el **cabildo Indígena Cañón del Río Pepitas, ubicado en el corregimiento de Cisneros, Dagua, Valle del Cauca** se registró un estallido al interior de una de las viviendas del lugar que deja hasta el momento 8 personas fallecidas y 15 heridos. Aunque los sucesos continúan en investigación y aún no han sido esclarecidos, las comunidades denuncian que un objeto fue lanzado dentro de la construcción previo a la detonación, por lo que podría tratarse de un atentado.

**Los hechos**

Tal como está previsto para el 24 de marzo se realizaría una Minga indígena en **La Delfina, vía Buenaventura**, sin embargo como el lugar era tan pequeño para los cerca de 5.000 indígenas que arribarían al lugar, se decidió adecuar un nuevo lugar en el municipio de Dagua específicamente en el corregimiento de Juntas, resguardo indígena de Pepitas.

El 21 de marzo, guardias indígenas de dicho resguardo fueron designados como responsables de la adecuación de la movilización, realizaron el trabajo logístico y una vez finalizaron su labor permanecieron al interior de una de las viviendas cuando cerca de las 3:00 pm se escuchó una detonación al interior de la casa.

**Arelis Cortés, consejera del Pueblo Embera Chami** quien se encuentra en el lugar de los hechos afirma que las investigaciones deben realizarse de manera autónoma pues cuentan con la capacidad para poder llevar el caso, "las versiones que hay en la población es que se dio una explosión, lo único que podemos declarar es que uno de los heridos trasladado al hospital alcanzó a decir que tiraron algo dentro del lugar y explotó, creemos que se trata de un atentado contra nuestra población".

**Investigación**

Frente al suceso y tal como explicó la consejera, **la Organización Regional Indígena del Valle del Cauca (ORIVAC) en el marco de la Jurisdicción Especial Indígena** ha comenzado la investigación de lo sucedido en coordinación con autoridades judiciales y a petición de las comunidades con un acompañamiento de organismos humanitarios que les ofrecieran garantías ante la desconfianza que existe hacia la Fiscalía.

"Estamos pasando por la pérdida de jóvenes llenos de vida que hacían su guardia indígena y que lo único que hacían eran defender su territorio y levantar sus bastones de mando para defender el derecho a la paz y tranquilidad" agrega Arelis Cortés quien aseguró que **a pesar del dolor la Minga sigue en pie con fortaleza y pie de lucha.**

Mientras las personas que resultaron heridas durante el suceso son atendidas en **Dagua y otros tres, debido a sus heridas de gravedad en el Hospital Universitario del Valle  y** permanecen en estado reservado.

**Declaraciones posteriores**

La consejera del Pueblo Embera rechazó las versiones de diversos medios de comunicación y las declaraciones del **ministro de Defensa, Guillermo Botero** quien  declaró que las comunidades indígenas habían alterado el lugar de los hechos, lo que implicaba una "conducta delincuencial" y haría mucho más difícil trabajo de los entes de investigación.

El senador Feliciano Valencia por su parte indicó que debido a que existen muchas versiones "hasta que no escuche la versión de la comisión de investigaciones de las comunidades indígenas y de Medicina Legal no se puede emitir un juicio sobre la realidad de lo que sucedió".

De igual forma la ONIC han manifestado su solidaridad con los Pueblos Embera y Nasa del Valle del Cauca, pidiendo a su vez a todos los organismos competentes, acompañar la investigación,  esclarecimiento  y judicialización de los hechos y han llamado a las diversas organizaciones y movimientos étnicos del país para que continúen fortaleciendo la **Minga Nacional por la Vida. **

<iframe id="audio_33630770" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_33630770_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
