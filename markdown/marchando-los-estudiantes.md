Title: ¿Por qué están marchando los estudiantes?
Date: 2018-09-11 14:42
Author: AdminContagio
Category: Educación, Movilización
Tags: Educación Superior, ENES, estudiantes, marcha, Ministerio de Educación
Slug: marchando-los-estudiantes
Status: published

###### [Foto: @PCatatumbo\_FARC] 

###### [11 Sept 2018] 

El déficit de las universidades públicas que ya supera los **16 billones de pesos**, la construcción de una alternativa al programa **Ser Pilo Paga** y la situación laboral de los profesores universitarios, son algunas de las razones que están denunciando los estudiantes con las marchas que se vienen gestando desde el año pasado.

### **Pero, ¿por qué protestan los estudiantes?** 

El pasado **5 de septiembre,** estudiantes de varias universidades se movilizaron en diferentes ciudades del país para invitar al **Encuentro Nacional por la Educación Superior (ENES)**, y en respaldo al debate por los líderes sociales. Hoy la Universidad Pedagógica saldrá a la carrera 7ª, hasta encontrarse con la U Nacional a la altura de la Calle 45, desde allí  en compañía de las demás universidades de la zona se desplazarán hacia el Planetario Distrital, para culminar la marcha en la Plaza de Bolivar.

Según **Ana María Nates,** integrante de la Asociación Colombiana de Estudiantes Universitarios **(ACEU)** y estudiante de la U Distrital Francisco José de Caldas (UD), en esta ocasión marcharán para visibilizar el **ENES que se llevará a cabo del 14 al 16 de este mes en Florencia, Caquetá;** y para rechazar la desfinanciación de la que está siendo objeto la educación superior pública. (Le puede interesar: ["El próximo 5 de octubre 'pasarán al tablero' autoridades que protegen líderes: Iván Cepeda"](https://archivo.contagioradio.com/5-octubre-lideres-cepeda/))

### **Las Universidades Públicas tienen un déficit financiero de 16 billones ** 

En la marcha, los estudiantes apoyarán a los profesores universitarios, quienes están denunciando el incumplimiento de los acuerdos a los que llegaron con el **Ministerio de Educación Nacional (MEN)** el año pasado, así como el aumento de los impuestos que deben pagar los docentes por el aumento de la base gravable en la declaración de renta.

Nates aseguró que **"es la crisis de la educación superior la que nos lleva a movilizarnos",** porque los jóvenes están reclamando el pago del déficit de las universidades públicas que asciende a 16 billones de pesos, para lo cual, pedirán una reunión con el **MEN** en la que se llegue a acuerdos de pagos que permitan subsanar esta deuda. (Le puede interesar: ["El Encuentro Nacional de Estudiantes por la Educación Superior se realizará en Marzo"](https://archivo.contagioradio.com/el-encuentro-nacional-de-estudiantes-por-la-educacion-superior-se-realizara-en-marzo/))

### **\#VamosPalEnes** 

La estudiante recordó que el primer ENES se realizó en marzo de este año en la Universidad Nacional sede Bogotá, y fue un primer momento de articulación; pero con el segundo encuentro que se realizará en la capital de Caquetá, esperan retomar el proceso y la experiencia del 2011 con la **MANE,** para construir una propuesta de educación alternativa.

En esta ocasión, los estudiantes plantearán alternativas para el programa Ser Pilo Paga, "que ha desfinanciado la educación superior", y buscarán generar propuestas para establecer una regla fiscal distinta que cree un presupuesto acorde a las necesidades del sector, **"porque la Ley 30 plantea un aumento de acuerdo al Indice de Precios al Consumidor (IPC), cuando los costos se incrementan de forma desmesurada"**.

<iframe id="audio_28492599" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_28492599_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en] [[su correo] [[Contagio Radio]
