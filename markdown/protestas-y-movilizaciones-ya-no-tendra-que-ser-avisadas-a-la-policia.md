Title: Se va cayendo a pedazos el nuevo Código de Policía
Date: 2017-04-21 14:50
Category: DDHH, Entrevistas
Tags: código de policía, Corte Constitucional
Slug: protestas-y-movilizaciones-ya-no-tendra-que-ser-avisadas-a-la-policia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/04/policia-1200x600.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Publimetro] 

###### [21 Abr 2017] 

Un nuevo falló de la Corte Constitucional se suma a los recortes de los “súper poderes” que tenía el Código de Policía, en esta ocasión se trata de las normas que regulaban cómo se debían organizar las movilizaciones y aglomeraciones públicas, y que la **Corte tumbó por considerar que, al afectar un derecho fundamental, debían tramitarse vía estatutaria y no ordinaría.**

De acuerdo con el representante a la Cámara, Alirio Uribe, estos fallos son necesarios y atienden a las advertencias que se habrían hecho sobre un Código de Policía **“inconstitucional”** que atentaba contra los derechos fundamentales de la ciudadanía. Le puede interesar:["Corte Constitucional restringe los "súper poderes" del Código de Policía"](https://archivo.contagioradio.com/corte-constitucional-restringe-los-super-poderes-del-codigo-de-policia/)

Sin embargo, la Corte dispuso que el Congreso de la República tendrá dos años para regular este tema, tiempo en el que para el representante continuaría en vigencia la normativa anterior sobre las protestas. **Es decir, las personas no estarían obligas a informarle a la Policía sobre la fecha, hora y lugar de una protesta.**

Los anteriores dos fallos de la Corte se refirieron a las posibilidades que tenía la **Policía de ingresar a las viviendas de las personas sin una orden judicial y la existencia de condiciones que permitan ese ingreso** y aclaró las normas sobre** la invasión al espacio público y los vendedores ambulantes.**

Sin embargo, quedan aún 24 demandas más, que deberá analizar la Corte sobre temas como el control en los espacios públicos, los vendedores ambulantes, las detenciones, entre otros, razón por la cual el represente señaló que **“la Corte le va a meter una buena peluquiada al Código”. **Le puede interesar: ["Nuevo Código de Policía otorga poderes extraordinarios a los uniformados"](https://archivo.contagioradio.com/nuevo-codigo-de-policia-otorga-poderes-exorbitantes-a-los-uniformados/)

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
