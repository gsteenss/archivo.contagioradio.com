Title: "La vida no vale nada..."
Date: 2015-01-29 16:50
Author: CtgAdm
Category: Laura D, Opinion
Tags: libertad de conciencia, Libertad de expresión
Slug: la-vida-no-vale-nada
Status: published

Por **[Laura Duque.]**

Citaré las palabras de Pablo Milanés: “*La vida no vale nada cuando otros se están matando y yo sigo aquí cantando cual si no pasara nada*”. En mi caso, y de manera proporcional, yo sigo aquí escribiendo cual si no pasara nada. Pero pasa, pasa en nuestra Colombia y en el mundo entero; asesinan por dinero, por diferencias ideológicas, por fanatismos, por apoderarse de la tierra, por intolerancia… porque sí, porque no. Las personas también matan y se hacen matar por defender una verdad, como si fuera absoluta.

“No vemos el mundo como es. Vemos el mundo como somos”. Así que no existe una verdad con mayúscula, sino múltiples perspectivas, como un caleidoscopio. Y entre ese conjunto diverso y cambiante encontramos posturas que rayan con la locura; ¿o es que es muy racional el atentado a la revista satírica Charlie Hebdo? No obstante, también es una locura alimentar el racismo y los odios en una sociedad propensa a la violencia como la sociedad humana; y a mi humilde consideración, la revista en cuestión se ha caracterizado por sobrepasar el límite de la libertad de expresión hasta el punto de promover la estigmatización y la fobia hacia todo un pueblo.

Mi reproche se dirige a los extremistas. Siendo así, mi crítica es tanto para los que silencian con fusiles como para quienes instigan a la violencia con lápices. ¡No desestimemos el poder del lenguaje ni de los medios! O si no recordemos el genocidio en Ruanda y cómo la difusión de calificativos como “cucaracha” para referirse a los tutsis cosificó a toda una población y llevó a que los hutus los asesinaran a machetazos, sin importar si eran vecinos o familiares, pues en su imaginario sólo se trataba de matar a simples animales rastreros. No olvidemos que las palabras también son acciones y como tales acarrean consecuencias. Podemos ser libres, pero esa libertad está sujeta al respeto de las demás libertades.

Ante todo debemos  defender la vida y la libertad de conciencia, y no caer en el engaño esnobista que dicta que por el elogio a la libertad de expresión podemos decir todo lo que nos dé la gana, irrespetando a diestra y siniestra todo aquello que no entendemos ni compartimos. Porque hay extremistas que no empuñan las armas, sin embargo alimentan activamente las ideologías guerreristas muchas veces legitimadas por la institucionalidad.

La manera en que los periodistas de la revista francesa ejercen el oficio se acerca más al formato de propaganda, pues su estilo es instigador y en extremo ofensivo; lejos del periodismo responsable pensado para el  bien público –entendiendo lo público como un concepto inconmensurable—. También hay quienes alimentan las locomotoras del entretenimiento, elogian la mediocridad o realizan un cubrimiento superficial, como si sólo de llenar espacio al aire se tratara, muchas veces sujetos a intereses propios y ajenos. En otras palabras, actúan como el circo que entretiene a los ciudadanos mientras algunas empresas periodísticas socavan la información de interés para el público.

Es un hecho que la ética es inherente al periodismo, y esa, es una relación que no se debe profanar, ni en Francia ni en Colombia.  Pero por desgracia muchas veces cuando se cumple su deber ser los extremistas hacen eco.

Acá en Colombia también funciona la máquina de la locura, esa que mata por doquier, la que pone bombas, la que amenaza, la que exilia… la que silencia la libre expresión. Sólo en el año que recién pasó, la [Fundación para la Libertad de Prensa –FLIP](http://flip.org.co/es/cifras-indicadores)— registró 162 víctimas colombianas.  Los ojos mediáticos se concentran en lo sucedido en Charlie Hebdo, y está bien, pero… ¿quiénes hacen hincapié en la violencia que se comete en contra del periodismo colombiano? Parece que para algunos sus vidas no valen nada, o valen muy poco, parece que para muchos son la piedra en el zapato, así como lo fue el inolvidable Jaime Garzón, la voz de la conciencia. Parece que sus muertes valen eso que se esconde tras un silencio incesante.

@lavadupe
