Title: Teatro, grafitti y radio en un mismo lugar
Date: 2017-06-08 17:08
Category: eventos
Tags: Bogotá, Radio Grafitti al dial, teatro
Slug: teatro-grafitti-radio
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/06/18839754_1457801744314775_5268602371596437893_o.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: CC de Teatro 

###### 07 Jun 2017 

**Radio Grafitti al dial**, es una combinación inusual entre el stand-up, el sit down comedy el grafitti contado y la radio en vivo, que proviene de la mente creativa de **Luis Lievano Quimbay (KESHAVA)**, que se presenta en Bogotá **del 8 al 10 de junio**.

La propuesta rescata de manera interactiva el género de la **radio revista** en la que el DJ, **relata con juegos de palabras y una fina ironía, las vivencias del grafitero** con un complemento de loops musicales y visuales como son algunos comerciales, clasificados y concursos en vivo sobre la cultura de la ciudad.

Keshava, un cuentero, grafitero y realizador de radio comunitaria y universitaria, con una trayectoría de más de 25 años, **utiliza la oralidad como hilo conductor para unir sus diferentes experiencias artísticas** y contar lo que "dicen los muros, lo que cuentan los grafittis, como desde tiempos inmemoriales la gente ha intentado rayar una corteza de un árbol, una roca y más recientemente las paredes".

Cerca del final de cada función, el público podrá participar respondiendo algunas preguntas sobre la ciudad, sus personajes y lugares históricos que la representan. Adicionalmente, el **los espectadores podrán interactuar en un pequeño radio foro** con sus apreciaciones y opiniones sobre el grafitti y las implicaciones sociales que carga.

La idea del artista es presentar la obra en algunas salas de Bogotá, llevarla a ciudades como Cali, Barranquilla, Medellín y Pasto, y luego viajar a Chile y Argentina con su propuesta, entendiendo que **el grafitti y el muralismo urbano es un fenómeno cultural que trasciende las fronteras geográficas**. Le puede interesar: [La muestra internacional documental de Bogotá abre convocatorias](https://archivo.contagioradio.com/midboconvocatoria/)

Adicional a la temporada antes de la última función, el día **Sábado 10 de Junio a las 5:00P.M** se realizará la proyección del documental **"Kontra la pared"** realizado por Keshava para Telesur, y un video foro sobre el Graffiti, la libertad de expresión y la construcción de una cultura de paz. . Este será de aporte voluntario.

 

[HORA: 7:30 P.M de Jueves a Sábado]

[LUGAR: Sala Seki Sano / Calle 12 \# 2- 65]

[BOLETERÍA: Estudiantes: \$12.000 / General: \$20.000]

<iframe id="audio_19161347" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_19161347_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>
