Title: Lessons from Chile's massive protests
Date: 2019-11-13 17:06
Author: CtgAdm
Category: English
Tags: Chile, protests, Sebastián Piñera
Slug: lessons-from-chiles-massive-protests
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/Fotos-editadas-2.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Photo: @CIDH] 

 

**Follow us on Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

[After more than four weeks of continuous protests, social organizations in Chile called for a national strike, demanding profound social reforms be made to guarantee access to health, pension, education and water. Student leaders have said that the key to change has been the transformative power of the people and the collective struggle. ]

[Despite criticism from the right that say the protests have no clear position, the citizen movements have established three demands: the creation of a new constitution, reforms of public services and a public declaration from the government to take responsibility for the violence committed by armed forces.]

[Nicole Rodríguez Aranda, vice president of the Student Federation of the University of Chile, said that the new constitution would address some of the limitations of the present Carta Magna that was written during the military dictatorship in 1980. What the protesters ask for is that a national referendum be called to determine whether a new constitution be created and how. President Sebastián Piñera has already announced some indications in favor of this proposition.]

[Citizens have also asked for modifications to the privatized pension, health and education system, which currently blocks poor citizens from accessing these basic services. Rodríguez added that access to water must also be democratized because “Chile is the only country in the world that has privatized its water sources.”]

[The student leader assured that the current system has “stripped us of our fundamental rights,” in reference to the government repression of citizens’ right to protest. For this reason, social movements have insisted that the Piñera administration acknowledge the human rights violations that the military and the police have committed during 20 days of protests.]

### **“We don’t trust the government”**

[After initially declaring the country was in the midst of a war, the Chilean President Piñera backtracked and said his administration would implement a policy of social reforms. Still, these declarations were not met with immediate support from protestors, who criticized the government for increasing financial support to the armed forces despite allegations of excessive force. ]

[These contradictions in the government’s public discourse has made it clear to protestors that President Piñera is operating as an authoritarian figure, said Rodríguez. “It’s clear that the right has not learned from the military dictatorship and is using criminal mechanisms,” she said. According to the National Institute of Human Rights, at least 197 people have suffered eye injuries during protests and another five people have died as a direct result of the action of state agents.]

[Rodríguez also stated that the police has started to intimidate some movement leaders by registering the locations of their homes. “We don’t trust in the capacities of the government to manage this situation with democracy,” the student leader said.]

### Lessons from the Chilean social protests

[For Rodríguez, the “social burst” of these protests is the result of years of organizing. “It has to do with the resistance that we have organized since the transition to democracy. We have insisted time and time again for or rights. This happened in 2006 with the penguin revolution, in 2011 with the student revolution, in 2018 with the feminist mobilization and again this year with the socio-environmental revolution.”]

[Although the previous protests failed to produce any short-term results, they changed popular perceptions of the economic system, which leads to long-term solutions. For this reason, she insisted that the collective struggle is what the government has tried to undermine and added that in recent protests, demonstrators have opted to protect each other.]

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
