Title: Consejo Nacional Electoral tiende trapa a ciudadanía: Unidos Revocaremos a Peñalosa
Date: 2017-05-15 16:04
Category: Movilización, Nacional
Tags: Enrique Peñalosa, revocatoria
Slug: consejo-nacional-electoral-tiende-trapa-a-ciudadania-unidos-revocaremos-a-penalosa
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/05/Consejo-Nacional-Electoral-Colprensa-e1494882049276.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Radio Red ] 

###### [15 May 2017]

El Comité Unidos Revocaremos a Peñalosa, señaló que si se han cumplido las condiciones para que se dé el proceso de revocatoria al alcalde Enrique Peñalosa y que el Consejo **Nacional Electoral estaría colocándole una trampa a la ciudadanía con este cambio en las reglas.**

Carlos Carrillo, promotor e integrante de este comité expresó que “la ley es muy clara al afirmar que las causales de revocatoria son, **tanto el incumplimiento del mandato como la insatisfacción generalizada**”, razón por la cual señala que el CNE está interpretado lo que la ley no dice. Le puede interesar: ["Con movilización y acciones legales defenderán revocatoria de Enrique Peñalosa"](https://archivo.contagioradio.com/mas-de-600-mil-firmas-expresan-insatisfaccion-hacia-gobierno-de-penalosa/)

De igual modo agregó que los motivos que sustentan está revocatoria son “serios” y que el problema radica en “quién es el que decide si esos motivos son o no válidos”, que para el comité no debe ser una autoridad como el **CNE, porque se estaría otorgando unas competencias que no tiene, sino la ciudadanía en las urnas.**

Carrillo manifestó que esta la regulación de los mecanismos de participación ciudadana, le corresponden a una ley estatutaria, a través de requerimientos muy puntuales como un estudio de constitucional previo, **lo que haría de este intento de cambio de reglas para la revocatoria, de acuerdo con Carrillo, una acción “burda para intentar salvar a Peñalosa”**. Le puede interesar:["Mandatarios no deberían tenerle miedo a la revocatoria"](https://archivo.contagioradio.com/mandatarios-no-deberian-tenerle-miedo-a-las-revocatorias/)

El Comité ha señalado que de llegar a ser aprobada la ponencia de Alexander Vega, presidente del Consejo Nacional Electoral, se estaría cayendo en prevaricato, sin embargo, Carrillo afirmó que desde ya se están estudiando todas las acciones **jurídicas para defender el proceso que ha recolectado más de 600 mil firmas en la ciudad.**

###### Reciba toda la información de Contagio Radio en [[su correo]
