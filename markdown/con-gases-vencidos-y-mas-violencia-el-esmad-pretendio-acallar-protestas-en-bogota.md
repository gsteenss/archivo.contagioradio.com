Title: Con gases vencidos y más violencia, el ESMAD pretendió acallar protestas en Bogotá
Date: 2019-12-20 18:55
Author: CtgAdm
Category: Movilización, Nacional
Tags: Escudos Azules, ESMAD, Ojo, Primera Línea
Slug: con-gases-vencidos-y-mas-violencia-el-esmad-pretendio-acallar-protestas-en-bogota
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/EMKyqGmXkAA7o2z.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/Escudos-e1576885750574.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: @Escudos\_Azules  
] 

El pasado jueves 19 de diciembre se presentó una nueva movilización que terminó en agresión por parte del Escuadrón Móvil Antidisturbios (ESMAD) en la Calle 72 con Carrera 7ma en Bogotá, que dejó a tres personas heridas y varias más detenidas. Adicionalmente, según participantes de la protesta, se hizo evidente el uso de gases lacrimógenos caducados, y no se tuvo en cuenta la presencia de niños en la zona. (Le puede interesar:["Caso de Dilan Cruz es enviado a la Justicia Penal Militar"](https://archivo.contagioradio.com/caso-de-dilan-cruz-es-enviado-a-la-justicia-penal-militar/))

### **La Universidad Nacional le dió la espalda a la movilización** 

Según explicó Nao, integrante de los Escudos Azules, desde la semana pasada pensaron en convocar una marcha para que las personas siguieran hablando del paro, y para que "se vea que todavía hay razones y personas que están luchando por lo que está pasando", es decir, que hay razones legítimas para salir a la calle a manifestarse. La movilización saldría de la Plaza Ché de la Universidad Nacional, pero el pasado miércoles, un día antes que se realizará la misma, salió un comunicado oficial de la institución señalando que se cerraría el campus hasta enero.

Al conocerse la decisión, los convocantes a la marcha decidieron trasladar el punto de encuentro al Parque conocido como Hippies, ubicado en la Carrera 7ma con Calle 60, donde llegó un número considerable de personas. La marcha salió desde este lugar hasta el Parque de la Calle 93, donde se realizaron distintos actos, entre ellos, lanzar agua roja en simbología por la sangre que se ha derramado en estos 30 días de Paro Nacional, pero los funcionarios de derechos humanos dijeron que se retirarían del lugar por esta acción.

Posteriormente, los manifestantes regresaron por la Carrera 7ma al sur, para concentrarse nuevamente en el Parque de los Hippies. Mientras se desplazaban, hacían plantones en algunos lugares, y sobre la Calle 72 hicieron un plantón, pero personas en la movilización alertaron por una gran presencia del ESMAD en su lugar de destino. En ese momento, personas que iban en la movilización pincharon las llantas de un bus del SITP y según el relato de Nao, "en 15 minutos llegaron cerca de 30 policías en moto, y en cuestión de 5 minutos nos sacaron".

### **Dos ojos heridos y un tobillo fracturado, el saldo de la intervención del ESMAD  
** 

> Los del ESMAD gasearon una niña.
>
> ¿A cuantos tienen que atropellar para desmontar este escuadrón de la muerte?
>
> Por favor compartamos estás imágenes, el gobierno Colombiano está violando los derechos humanos de su pueblo. [pic.twitter.com/Ph8nUbKSnq](https://t.co/Ph8nUbKSnq)
>
> — David. (@DonIzquierdo\_) [December 20, 2019](https://twitter.com/DonIzquierdo_/status/1207839025337376769?ref_src=twsrc%5Etfw)

<p>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
</p>
Según el integrante de los Escudos Azules, el pasado jueves se usaron gases vencidos que son más tóxicos, "cuando un gas se vence genera arsénico, que es tóxico", y se puede ver que no es apto para el uso porque el humo que emana es de color amarillo. También señaló que aunque había niños en la zona lanzaron los gases, y "usaron salvas contra las cabezas, les pegaron con bolillos a las personas". (Le puede interesar: ["Desde EE.UU. piden moratoria de armas y reformas dramáticas del ESMAD"](https://archivo.contagioradio.com/desde-ee-uu-piden-moratoria-de-armas-y-reformas-dramaticas-del-esmad/))

En medio de la huída, las personas corrieron a refugiarse en la Universidad Pedagógica, ubicada a escasos metros del lugar, y allí, se reportó que uno de los manifestantes perdió uno de sus ojos pese a que contaba con gafas de seguridad. De igual forma, otra persona tiene en riesgo su ojo, a una mujer le fracturaron el tobillo y fueron capturados algunos integrantes de los Escudos Azules. (Le puede interesar: ["El primer paso para desmontar el ESMAD es modificar la doctrina de seguridad"](https://archivo.contagioradio.com/el-primer-paso-para-desmontar-el-esmad-es-modificar-la-doctrina-de-seguridad/))

### **"Necesitamos que este ente deje de existir"** 

Para finalizar, Nao aseguró que físicamente los manifestantes no pueden defenderse del ESMAD, y "si nos ponemos una armadura, sacarán armas de fuego". Por esa razón, concluyó que es necesario el apoyo internacional para denunciar el actuar del ESMAD, así como que el ente deje de existir y haya organismos de control que hagan presencia durante todas las movilizaciones para que se garantice efectivamente el derecho a la protesta. (Le puede interesar: ["Agresiones del ESMAD son solo «daños colaterales» para el Gobierno"](https://archivo.contagioradio.com/agresiones-del-esmad-son-solo-danos-colaterales-para-el-gobierno/))

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe><iframe id="audio_45775872" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_45775872_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
