Title: La lucha de Lina Jiménez, víctima de montaje judicial de caso Andino, por su libertad
Date: 2020-11-04 11:35
Author: CtgAdm
Category: Expreso Libertad
Tags: #CasoAndino, #Expresolibertad, #LinaJiménez, #Montajesjudiciales
Slug: la-lucha-de-lina-jimenez-victima-de-montaje-judicial-de-caso-andino-por-su-libertad
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/05/lina.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

Luego de que el pasado 14 de octubre un juez ordenara la libertad de Lina Jiménez por vencimiento de términos, la recaptura de la mujer se realizó el 16 de octubre, sin que ni siquiera se cumpliera la orden del juez y según su familia, sin garantías para el proceso judicial.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En este programa del Expreso Libertad, Laura Jiménez, hermana de la víctima del montaje judicial y Lina Torres, amiga e integrante del colectivo Libres e Inocentes, recalcaron las difíciles situaciones que ha tenido que atravesar Jiménez y la violación al derecho al debido proceso. (Le puede interesar:["Colectivo Libres e inocentes"](https://www.facebook.com/LibreseInocentes))

<!-- /wp:paragraph -->

<!-- wp:html -->  
<iframe id="audio_59871770" frameborder="0" allowfullscreen scrolling="no" height="200" style="border:1px solid #EEE; box-sizing:border-box; width:100%;" src="https://co.ivoox.com/es/player_ej_59871770_4_1.html?c1=ff6600"></iframe>  
<!-- /wp:html -->

<!-- wp:paragraph -->

De acuerdo con ambas mujeres, la reciente orden de captura que se le imputó a Lina, está relacionada con hechos del 2018 y fue autorizada por un juez municipal. Sin embargo, es importante recalcar que es el tercer proceso en contra de esta mujer y que en los dos anteriores la Fiscalía no demostró la participación de Jiménez en los hechos en cuestión. (Le puede interesar: ["En busca de la libertad: Los grises del caso Andino"](https://www.youtube.com/watch?v=M27RlQHVqxw))

<!-- /wp:paragraph -->
