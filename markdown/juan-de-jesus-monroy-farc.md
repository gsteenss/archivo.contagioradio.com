Title: Asesinado Juan de Jesús Monroy reconocido lider de reincorporación de FARC
Date: 2020-10-17 15:41
Author: CtgAdm
Category: Actualidad, Nacional
Slug: juan-de-jesus-monroy-farc
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/10/Juan-de-jesus-Monroy.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

  
Este viernes, se conoció que fue asesinado Juan de Jesús Monroy, un líder de reincorporación en el céntrico departamento del Meta, junto a su escolta, Luis Alexander Largo, también desmovilizado de las FARC-EP.  
El partido Fuerza Alternativa Revolucionaria del Común (FARC) afirmo que los firmantes de la paz fueron acribillados en zona rural del municipio de Uribe.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El asesinato de Juan de Jesús Monroy es un golpe al corazón de la reincorporación de FARC. Son múltiples las expresiones de rechazo a su asesinato por el papel que había jugado al interior de la guerrilla antes de la dejación de armas y ahora en el proceso de reincorporación.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Estas muertes se suman a los 234 casos de excombatientes de las FARC-EP asesinados desde que se firmo el Acuerdo de Paz en la Habana. Por lo que el partido FARC afirma que "Exigimos garantías para los firmantes del acuerdo, quienes trabajan todos los días por la paz de Colombia"

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Carlos Ruiz Massieu, jefe de la Misión Verificación de las Naciones Unidas en Colombia, condenó el asesinato de Monroy: "su sensible pérdida es un trágico recordatorio de necesidad de reforzar protección de quienes dejaron las armas y apuestan por la paz en Colombia". De igual manera, la embajada británica manifestó:

<!-- /wp:paragraph -->

<!-- wp:quote -->

> "Lamentamos el asesinato de Jesús Monroy, comprometido con la reincorporación y la paz. Enviamos condolencias a sus familiares. Y recordamos la importancia de seguir trabajando por la protección y seguridad de los excombatientes. Nuestro compromiso con la reincorporación continúa”
>
> <cite>Carlos Ruiz Massieu</cite>

<!-- /wp:quote -->

<!-- wp:heading {"level":3} -->

### Excombatientes se movilizarán junto a la Minga para exigir que frene la ola de asesinatos

<!-- /wp:heading -->

<!-- wp:paragraph -->

Como respuesta a los hechos de violencia que han derivado en el asesinato de 49 excombatientes a lo largo del 2020, una comisión conformada por lideres y lideresas regionales del partido FARC en las diversas regiones del país adelanta los detalles de lo que [**será una gran movilización para el mes de noviembre** ](https://archivo.contagioradio.com/excombatientes-farc-movilizaran-rechazo-asesinatos/)en rechazo de lo que han denominado un extermino sistemático contra los firmantes de paz.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Aunque en un principio se aseguró que sería una movilización única por parte de FARC, en las últimas horas se ha conocido que se unirán a la Minga Indígena y respaldarán sus exigencias y además buscarán respaldo para frenar lo que han llamado un genocidio político en contra de las personas en proceso de reincorporación.

<!-- /wp:paragraph -->
