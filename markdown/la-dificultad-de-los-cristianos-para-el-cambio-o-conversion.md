Title: La dificultad de los cristianos para el cambio o conversión.
Date: 2019-11-26 14:14
Author: A quien corresponde
Category: A quien corresponda, Opinion
Tags: cristianismo., Cristianos, Iglesias Cristianas, Religión
Slug: la-dificultad-de-los-cristianos-para-el-cambio-o-conversion
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/carretera.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

Examínenlo todo y quédense con lo bueno  
1(1Tesalinisenses 5,21)

Bogotá, 30 de Octubre del 2019

*Diferenciar entre información y opinión*  
*es necesario para una buena decisión.*

Estimado

Hermano en la fe  
Cristianos, cristianas, personas interesadas

Cordial saludo,

Entre las dificultades para entendernos, hay dos preguntas, que dependiendo de la forma como se respondan se impulsará o limitará el cambio personal, social o eclesial, que en lenguaje cristiano,  
es la conversión predicada por Jesús de Nazaret: *“El tiempo se ha cumplido, el Reino de Dios está cerca, conviértanse y crean en la Buena Noticia”* (Marcos 1,15).

Le puede interesar:( [Mirar solo una cara de la realidad, actitud poco cristiana)](https://archivo.contagioradio.com/mirar-solo-una-cara-de-la-realidad-actitud-poco-cristiana/)

#### Las preguntas son: 

*¿En la relación con Dios, debemos tener en cuenta o no la realidad que nos rodea?*

*¿Hay elementos o aspectos de la fe cristiana que pueden cambiarse?*

**En cuanto a la primera pregunta:**

según tu manera de pensar y vivir la fe cristiana (y la de personas religiosas cercanas a ti), la relación con Dios es una relación “intimista", sin conexión con la realidad histórica, más aún, considerando al mundo cómo un obstáculo para encontrarnos con El, como algo que nos distrae o nos saca de la espiritualidad. Hay otra manera de pensar y vivir la fe cristiana, como una relación personal con Jesucristo que acontece en la historia concreta de cada persona, en medio de su realidad social, política y económica; en esta comprensión, el mundo, la historia y la vida diaria son los lugares donde Dios está encarnado y desde los cuales habla y llama a actuar a los creyentes, con momentos especiales para orar, celebrar y profundizar la fe y lugares adecuados para hacerlo, facilitando descubrir lo que Dios quiere que hagamos en esa realidad y cómo quiere que seamos en la vida, personalmente me identifico con esta segunda comprensión de la fe. Según tu manera de ver y vivir, las y los cristianos deben ocuparse solo de las *“cosas de Dios”*, del “*culto cristiano”*, de la *“espiritualidad”* y de la oración, alejada de la realidades concretas de los seres humanos y del planeta, sin relación con la vida diaria de las personas (especialmente las marginadas por cualquier razón, las víctimas de las violencias políticas y los empobrecidos por el sistema socio-económico actual) y de los pueblos (especialmente los sometidos por diversas poderes), sin relación con la naturaleza, con la casa común que habitamos los seres vivos; casa que está en riesgo por acciones humanas como la contaminación del agua, del aire, de la atmósfera y  de los alimentos como consecuencia de la idea de “progreso” y “desarrollo” que está orientando la humanidad con el uso de “mensajes cristianos”.

**En cuanto a la segunda pregunta:**

dicen que nada se puede cambiar de la fe cristiana. En los diálogos que hemos tenido, veo que identifican aspectos centrales de la fe (que no se pueden cambiar), con elementos históricos, culturales o disciplinarios en los que se expresa la fe cristiana y que podrían cambiar, porque son formas de vivir, de entender y celebrar la fe asumidas en diversos momentos de la historia, que obedecieron a culturas diferentes o que buscaron resolver problemas concretos, que se convirtieron en modos de vivir y expresar la fe respondiendo a momentos del pasado y dicen poco al presente, que respondieron culturas lejanas a la nuestra y por eso no conectan la fe con la vida de hoy, que resolvieron problemas del pasado pero no resuelven los de hoy.

##### Unos hechos pueden ayudarnos a entender mejor: 

**Primer ejemplo:** Durante siglos para la Iglesia católica el latín fue la lengua universal y sagrada. Esto cambió con el *Concilio vaticano II* (años 1962-1965). El latín, ha sido valioso y está bien que se usara en la Iglesia; el problema fue imponerlo a todos los cristianos católicos, en todas las regiones y para todos los ritos, negando el uso de las lenguas originarias por considerarlas indignas de  
expresar lo divino; en la actualidad hay personas que quieran imponerlo de nuevo, desconociendo que Jesús de Nazaret habló y predicó en arameo, leyó la Biblia (el antiguo testamento) y oró en  
legua hebrea; que los evangelios y todo el nuevo testamento se escribieron en griego (el lenguaje que la mayoría de personas letradas utilizaban) y que siglos más tarde, cuando todas las colonias del imperio romano hablaron latín y la Iglesia se expandió por todo el imperó romano y también habló latín. Está bien que el cristianismo se haya traducido de una lengua a otra y que asumiera las diversas culturas, el problema fue imponer esa lengua y esa cultura, impidiendo que *“todo el mundo”* pudiera oír en su propia lengua *“las maravillas de Dios”* (cf. Hechos 2,6.8.11).

**Segundo ejemplo:** Hace unos años, entré a una librería “cristiana” en Cali, mire libros y biblias. Me llamo la atención una biblia “en lenguaje actual”, traducida de los originales griego y hebreo, con información muy sólida y la compré. Con diccionarios griego y hebreo comparé la traducción de varios textos y constate que eran fieles al sentido original de los textos y que lo traducían bien al lenguaje de hoy. Tiempo después volví a la librería y hablé con el administrador, le pregunté por la aceptación de esa biblia entre los creyentes y me dijo que “muchos compraron la biblia y volvieron luego a reclamarle porque no encontraban las palabras con las cuales discutían con los creyentes de otras iglesias”. Está traducción de la biblia era más fiel a los textos originales y trasmitía mejor el mensaje para nuestro días, pero ellos estaban acostumbrados a las palabras y no a su sentido.

**El tercer ejemplo:** Hoy se está discutiendo en la iglesia católica la ordenación de hombres casados. Quienes están en contra, afirman que no pueden aceptarlo porque así estableció Nuestro Señor Jesucristo. No tienen en cuenta que *San Pedro* era casado (Los evangelios de Marcos 1, 30 y Lucas 4,38 hablan de su suegra), que al comienzo de la iglesia había obispos casados (1Timoteo 3,2), que en la iglesia católica oriental hay sacerdotes célibes y casados. Por supuesto que el celibato seguira siendo una opción valida, como lo presenta San Pablo: “Quiero que estén libre de preocupaciones; mientras el soltero se preocupa de los asuntos del Señor y procura agradar al Señor, el casado se preocupada de *"los asuntos del mundo y de cómo agradar a su mujer”* (1Corintios 7,32-34) pero lo hace como una recomendación no como una imposición.

Estos tres ejemplos muestran que el cristianismo ha tenido cambios importantes en la historia, que los puede tener hoy. Por esto es válido preguntarse: *¿Por qué tanto miedo de las iglesias a los*  
*cambios?* El miedo ha sido usado y estimulado por sectores políticos y económicos para conservar sus intereses en nombre de Dios, manipulando la religión. El miedo de los cristianos a los cambios, a lo desconocido, a lo diferente y a lo “otro” es contradictorio con el mensaje de Jesús de Nazaret, a quien su relación con Dios lo lleva a nuevas maneras de relacionarse con los demás, con la realidad social y con la naturaleza, haciendo realidad sus palabras *“A vino nuevo vasijas nuevas*” (Mateo 9,17), cumpliendo la voluntad de Dios: *“Yo hago nuevas todas las cosas”* (Apocalipsis 21,5)

Fraternalmente,  
P. Alberto Franco, CSsR, J&amp;P  
francoalberto9@gmail.com
