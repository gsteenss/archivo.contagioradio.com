Title: “Arrancamos un año en firme por la paz”
Date: 2016-01-21 11:13
Category: Nacional, Paz
Tags: Conversacioines de paz en Colombia, ELN, FARC, Frente Amplio por la PAz, Juan Manuel Santos
Slug: arrancamos-un-ano-en-firme-por-la-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/01/angela_robledo_contagioradio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: confidencialcolombia. 

<iframe src="http://www.ivoox.com/player_ek_10153403_2_1.html?data=kpWel5iYdJShhpywj5WUaZS1kZWah5yncZGhhpywj5WRaZi3jpWah5yncYa5k4qlkoqdh6Lm08bbxcbRs9Sf1tOYw4qnd4a2ktSYx9OPqsrmzsqY0tTWb83VjNXO3IqpdoaskYqmppKJe6ShpNTb1sbLrdCfs8bRy9SPcYarpJKh&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Angela Robledo, Alianza Verde] 

###### [[(]21 Ene 2016[)]] 

La Representanta Angela María Robledo afirma que desde el principio creían que iba a llegarse a este punto definitivo, y que la posibilidad de cese bilateral con un tercero veedor es un paso muy importante. **Además la presencia de la [ONU y la CELAC](https://archivo.contagioradio.com/?s=onu+) le da tranquilidad a quienes no han creído en el proceso de paz.**

También anunció que las organizaciones de la sociedad civil que han venido haciendo un trabajo de veeduría se están preparando para contribuir en esa nueva etapa, puesto que la presencia de la sociedad civil siempre ha sido una garantía de transparencia en las acciones emprendidas en ese sentido, tanto **[DiPaz](https://archivo.contagioradio.com/?s=DiPaz) como [Constituyentes por la Paz](https://archivo.contagioradio.com/?s=constituyentes+por+la+paz) y el [Frente Amplio por la paz](https://archivo.contagioradio.com/?s=frente+amplio+por+la+paz) realizarían veeduría social.**

En cuanto a la posibilidad del inicio de la fase pública de las conversaciones de paz con el ELN, Robledo resalta que siguen a la espera del anuncio pero que seguramente todas las fuerzas que han respaldado el proceso con las FARC van a respaldar las conversaciones con el ELN.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u)  o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](https://archivo.contagioradio.com/).
