Title: Juan Kamoru en Sonidos Urbanos
Date: 2016-07-08 14:56
Category: Sonidos Urbanos
Tags: Cultura, Juan Kamoru, Música, sonidos urbanos
Slug: juan-kamoru-en-sonidos-urbanos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/07/juan-kamoru.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Kamoru 

Acompañado por su guitarra y con letras inspiradas en las vivencias adquiridas en su contacto con comunidades latinoamericanas, el músico y compositor colombiano Juan Kamoru visitó **Sonidos Urbanos.**

Durante una hora hablamos de su música y de la creación del rock mestizo, una mezcla que destaca la identidad de las comunidades indígenas, afros y campesinas de los pueblos originarios. Destrezas que le han merecido reconocimientos como el "Jóvenes talentos" de la Fundación Gilberto Alzate Avendaño para el Festival Centro edición 2016.

Con un nombre de origen africano y antropólogo de profesión, Kamoru ha utilizado su trabajo como investigador  para producir de manera independiente nueva música basada en raíces ancestrales y con una mezcla de blues, rock, reggae y jazz que él califica como una "expresión del alma".

Dale play a este programa y conoce el recorrido de la música y vida de **Juan Kamoru.**

<iframe src="http://co.ivoox.com/es/player_ej_12163923_2_1.html?data=kpeemJiddpShhpywj5WVaZS1k5ySlaaWd46ZmKialJKJe6ShkZKSmaiRdI6ZmKiarNrFsoy_wtLc1NqPqc-ftNTby8nTt4zJ08fO0NTXcYarpJKw0dPYpcjd0JC_w8nNs4yhhpywj5k.&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>
