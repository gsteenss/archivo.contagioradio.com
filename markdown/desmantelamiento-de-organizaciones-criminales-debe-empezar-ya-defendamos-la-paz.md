Title: “Desmantelamiento de organizaciones criminales debe empezar ya” Defendamos La Paz
Date: 2020-01-08 12:52
Author: CtgAdm
Category: Nacional, Paz
Tags: asesinato de excombatientes, Duque, lideres sociales, proceso de paz, violencia
Slug: desmantelamiento-de-organizaciones-criminales-debe-empezar-ya-defendamos-la-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/DefendamosPaz.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: antoniosanguino.co] 

El movimiento Defendamos La Paz conformada por más de 600 personas y que ha liderado escenarios entorno a los Acuerdos de Paz con Farc, en distintas instancias internacionales, le exigió al presidente Iván Duque por medio de un comunicado, respetar el mandato de la Comisión Nacional de Garantías de Seguridad ante los reiterados hechos de violencia contra el liderazgo social en todo el territorio nacional, especialmente en territorios como el Cauca, Chocó y Putumayo. (Le puede interesar: [Entre julio y septiembre de 2019 aumentaron un 43% las agresiones a defensores de DD.HH. respecto al año pasado](https://archivo.contagioradio.com/entre-julio-y-septiembre-de-2019-se-amenazo-a-casi-3-defensores-de-dd-hh-por-dia/))

A pesar de que en mayo de 2019 el Gobierno de Duque convocó a la Mesa Nacional de Garantías a representas de las sociedad civil y ONGs que hacen parte de ese espacio, ellos aseguran que no es mucho lo que se ha podido avanzar, además el Gobierno desestimó su propuesta sobre la creación de una política pública para el desmonte de las organizaciones criminales previsto en el acuerdo.

Asimismo en el comunicado Defendamos la Paz afirma que la próxima reunión de la Comisión Nacional de Garantías “Debe dar inicio a la ruta de construcción de la política pública, como lo establece el acuerdo de paz, sin más dilación”. (Le puede interesar: [Colombia: El país con más asesinatos de defensores en el mundo](https://archivo.contagioradio.com/colombia-pais-mas-asesinatos-defensores-mundo/))

Y es que según el último informe semestral de Somos Defensores, de enero a noviembre del 2019 los asesinatos de mujeres defensoras de derechos humanos se incrementaron 44% en relación al último año pasando de 102 agresiones individuales contra ellas en el primer semestre del 2018 a 171 en el mismo periodo de 2019; además se registraron 250 asesinatos a líderes sociales en 2019, destacando a Cauca, Antioquia y Nariño como los departamentos con las cifras más altas de violencia a defensores y defensoras, asimismo destacaron los 36 asesinatos a excombatientes.

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo](http://bit.ly/1nvAO4u)][[Contagio Radio](http://bit.ly/1ICYhVU).]
