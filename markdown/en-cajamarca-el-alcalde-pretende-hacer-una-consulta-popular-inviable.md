Title: Alcalde le hace trampa a la Consulta Popular en Cajamarca
Date: 2016-11-22 12:11
Category: Ambiente, Nacional
Tags: Cajamarca, consulta popular, La Colosa, La Colosa Cajamarca, Mineria
Slug: en-cajamarca-el-alcalde-pretende-hacer-una-consulta-popular-inviable
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/Cajamarca.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: RDSColombia] 

###### [22 Nov. 2016] 

**A través de un decreto, la Alcaldía de Cajamarca ha dado a conocer la fecha de citación para las votaciones a la Consulta Popular Minera** la cual buscará frenar el proyecto de explotación de oro La Colosa desarrollado por la multinacional Anglo Gold Ashanti y que cuenta con 25 títulos mineros. **Esta se realizaría, según dicho decreto este domingo 27 de noviembre.**

Dicha decisión ha generado la respuesta por parte de la Registraduría y el Comité Ambiental, quienes han asegurado que **en esos 6 días sería casi imposible preparar las votaciones.**

Según Robinson Mejía, integrante del comité promotor de la Consulta Popular **“la decisión que se tomó es darle la espalda a la población. Hacer una consulta en 6 días es entorpecerla”.**

Así mismo agrega que en la actualidad la Alcaldía no permite hacer pregoneo acerca de la consulta y asevera **“sabíamos que el Alcalde estaba en contra de la Consulta Popular pero no pensamos que fuera a hacer esto que hizo con este decreto”.**

Frente a la Registraduría y su papel en esta Consulta Popular, Robinson manifestó **"esperamos que la Registraduría no entre en el juego maquiavelico del Alcalde, porque con esos tiempos no hay tiempo para hacer Consulta Popular".**

**Según las organizaciones todo parece ser una estrategia para entorpecer el proceso democrático y beneficiar a la Anglo Gold.** Le puede interesar: [Consulta Popular en Cajamarca pondría fin a La Colosa](https://archivo.contagioradio.com/consulta-popular-en-cajamarca-pondria-fin-colosa/)

**¿Trabas a la Consulta Popular Minera?**

Esta no es la primera vez que se intenta llevar a cabo una consulta minera en Cajamarca. Según el Comité Ambiental y Campesino de Cajamarca y Anime, **el año pasado en febrero la iniciativa que nació de la comunidad fue negada por el concejo del municipio.**

Sin embargo, en julio de 2015 con la Ley 1757, sobre mecanismos de participación ciudadana se pudo convocar nuevamente a una consulta popular gracias a la recolección de casi 5 mil firmas aunque solo se necesitaba recoger 1600. Le puede interesar: [Consulta minera busca frenar proyecto La Colosa en Cajamarca, Tolima.](https://archivo.contagioradio.com/consulta-minera-busca-frenar-proyecto-la-colosa-en-cajamarca-tolima/)

[Comunicado Público Del Comité Promotor de La Consulta Popular Sobre Minería en Cajamarca](https://www.scribd.com/document/331990764/Comunicado-Publico-Del-Comite-Promotor-de-La-Consulta-Popular-Sobre-Mineria-en-Cajamarca#from_embed "View Comunicado Público Del Comité Promotor de La Consulta Popular Sobre Minería en Cajamarca on Scribd") by [Contagioradio](https://www.scribd.com/user/276652802/Contagioradio#from_embed "View Contagioradio's profile on Scribd") on Scribd

<iframe id="doc_34909" class="scribd_iframe_embed" src="https://www.scribd.com/embeds/331990764/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-8YyiHbcUN4DQAKTEg4Bc&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="0.7729220222793488"></iframe>  
<iframe id="audio_13872632" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_13872632_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio]{.s1}](http://bit.ly/1ICYhVU) {#reciba-toda-la-información-de-contagio-radio-en-su-correo-o-escúchela-de-lunes-a-viernes-de-8-a-10-de-la-mañana-en-otra-mirada-por-contagio-radio .p1}

<div class="ssba ssba-wrap">

</div>
