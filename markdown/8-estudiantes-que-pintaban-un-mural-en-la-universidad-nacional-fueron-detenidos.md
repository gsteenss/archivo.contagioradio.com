Title: Detienen a 8 estudiantes en la Universidad Nacional
Date: 2016-03-17 15:33
Category: DDHH, Nacional
Tags: detenciones arbitrarias, Policía Nacional, Universidad Nacional
Slug: 8-estudiantes-que-pintaban-un-mural-en-la-universidad-nacional-fueron-detenidos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/03/universidad-nacional-contagioradio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: skyscrapercity] 

###### 

<iframe src="http://co.ivoox.com/es/player_ek_10844317_2_1.html?data=kpWllpmXdZihhpywj5aVaZS1kpyah5yncZOhhpywj5WRaZi3jpWah5yncZmfxtjh18nNpc%2FoxtiY09rJb9Hdz9nOxMbSb9bijNLi1MbQb8bijNHOjbrSrdfZ09jWxsbIb6%2Bhhpywj6jTstXVyM7cjbfFqMrjjJKSmaiReA%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### **Entrevista Susana Robayo** 

###### [17 Mar 2016]

Integrantes de la policía nacional y de la SIJIN ingresaron en 4 camionetas hasta la plaza central de la Universidad Nacional y luego de registrar las pertenencias de **17 estudiantes capturaron a 8 de ellos sin ningún tipo de acusación y se llevaron algunas de sus bicicletas. **

Según Susana Robayo, una de las estudiantes agredidas, el grupo de jóvenes estuvo trabajando en un mural del padre Camilo Torres en las paredes exteriores de la biblioteca desde tempranas horas de la madrugada, allí  **fueron sorprendidos por la policía e integrantes del equipo de seguridad de la Universidad.**

Robayo afirma que **dos de las camionetas no tenían ninguna identificación de los cuerpos de seguridad del Estado** y que una vez realizaron las requisas a las pertenencias procedieron a subir allí a los estudiantes, dos grupos de 4 en las camionetas sin identificación. Hacia las 10 de la mañana ya se pudo establecer que los jóvenes fueron conducidos a la UPJ y se espera que en el transcurso del día sean dejados en libertad pues no hay ningún cargo en su contra.

Este tipo de acciones por parte de las autoridades de policía se consideran irregulares, puesto que la entrada de la fuerza pública debe estar antecedida de una orden judicial o un permiso expreso de la institución y por otra parte el decomiso de las bicicletas y la captura de los estudiantes, **no tendría justificación en los procedimientos policiales dado que no se encontró ningún tipo de material que pueda representar peligro para la sociedad,** argumenta Susana Robayo.** **

##### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](https://archivo.contagioradio.com/)]
