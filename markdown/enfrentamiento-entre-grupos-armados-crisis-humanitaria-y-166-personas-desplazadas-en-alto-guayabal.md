Title: Indígenas de Alto Guayabal desplazados y confinados por grupos armados
Date: 2019-03-13 15:14
Author: ContagioRadio
Category: Comunidad, Nacional
Tags: AGC, comunidades indígenas, Crisis humanitaria, ELN
Slug: enfrentamiento-entre-grupos-armados-crisis-humanitaria-y-166-personas-desplazadas-en-alto-guayabal
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/03/Alto-Guayabal-45.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Contagio Radio 

###### 13 Mar 2019 

Como consecuencia del confinamiento y el desplazamiento al que han sido sometidos los habitantes de Alto Guayabal en Chocó, por parte de enfrentamientos entre las **autodenominadas Autodefensas Gaitanistas de Colombia (AGC) y el ELN,** desde el de 19 de enero hasta el 12 de marzo han muerto** siete niños de Alto Guayabal, Uradá y Bidoquiera quienes no han podido ser tratados a tiempo**. Otras 20 personas presentan síntomas de paludismo como fiebre, dolor de cabeza, diarrea y escalofríos.

**Argemiro Bailarín, líder indígena del resguardo Uradá Jiguamiando** indica que los actores armados han prohibido la caza, la pesca y otras actividades que realizan las comunidades, además de restringir el ingreso de víveres en diversos municipios como **Alto Baudó, Ríosucio y Carmen del Darién**. [(Lea también Paramilitares están retomando el control del Bajo Atrato)](https://archivo.contagioradio.com/paramilitares-estan-retomando-el-control-del-bajo-atrato/)

El líder indígena resalta las dificultades que existen para que los miembros de la comunidad puedan salir hasta la cabecera municipal más cercana en Mutatá, Antioquia pues además de ser un municipio distante, se requiere un gran esfuerzo económico que implica el uso de combustible y  motores a los que no tienen acceso.

En el lugar únicamente ha hecho presencia la comisión de la Defensoría del Pueblo, sin embargo no han recibido más apoyo humanitario, por lo que Bailarín pide que el Ministerio y la Secretaria de Salud haga presencia en los resguardos indígenas para resolver la precaria situación que viven las comunidades en temas de orden público, salud y alimentación.

###### Reciba toda la información de Contagio Radio en su correo o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por Contagio Radio. 
