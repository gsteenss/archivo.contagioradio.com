Title: La gratuidad de la educación chilena no es totalmente cierta
Date: 2016-01-19 13:49
Category: Educación, El mundo
Tags: Chile, educacion gratuita, estudiantes, ley de gratuidad chile, Movimiento estudiantil
Slug: la-gratuidad-de-la-educacion-chilena-no-es-totalmente-cierta
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/01/Movimiento-estudiantil-Chile.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: UNE Concepción 

<iframe src="http://www.ivoox.com/player_ek_10127962_2_1.html?data=kpWelJydepOhhpywj5eVaZS1lpeah5yncZOhhpywj5WRaZi3jpWah5ynca3VjMzfw9nZrcXVxZDRx5DQpYzZxdrQw8jNaaSnhp6g0JDHrMrgxtPOjdPTb8bnjNnc1sbQscbi1cqYj4qbh4630NPhw8zNs4zGwsnW0ZCRaZi3jpk%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Sebastián Reyes] 

###### [19 Enero 2016 ] 

[El pasado 23 de diciembre el Congreso chileno aprobó una ley corta para garantizar la gratuidad en la educación superior al 50% de la población estudiantil más vulnerable de las instituciones adscritas al ‘Consejo de Rectores de Universidades Chilenas’, una medida que ha generado serias críticas en los estudiantes quienes denuncian que **varias de las demandas que presentaron en las mesas de concertación no fueron tenidas en cuenta para la consolidación de la ley**.]

[Sebastián Reyes, presidente de la ‘Federación de Estudiantes de la Universidad de Concepción’ e integrante de la ‘Unión Nacional Estudiantil de Chile’, asegura que “**es una ley que no cumple con las expectativas del movimiento estudiantil**. Terminó el proceso legislativo y ninguna de las demandas transversales que nosotros planteamos fueron tomadas”. Lo que a su juicio muestra que **los procesos de concertación que plantea el Gobierno con distintos sectores sociales “son pura pantalla**”.    ]

[Reyes asevera que esta ley no asegura la gratuidad a largo plazo, pues es un plan piloto que **deja a estudiantes provenientes de familias de muy escasos recursos, quienes requieren de ayudas para poder mantenerse en el sistema educativo, sin becas de manutención**. Por un lado el gobierno está dando gratuidad a algunos estudiantes, pero por el otro le está quitando beneficios a otro porcentaje, agrega.]

[Otra de las críticas a la ley por parte del movimiento estudiantil tiene que ver con la falta de intervención en la financiación con dineros públicos a instituciones universitarias vinculadas con prácticas antidemocráticas y de corrupción, “**no está regulándose el sistema de financiación por parte del Estado, se están financiando universidades con graves problemas de corrupción**, asegura Reyes.]

[Pese a que los estudiantes reconocen que esta ley representa un pequeño avance en su trayectoria de movilización contra las lógicas de mercado que han imperado en la educación superior chilena,  insisten en que **no existe voluntad política para implementar las transformaciones que ellos y sus familias han demandado al Gobierno desde hace varios años**, por lo que como actores sociales deben copar todos los espacios de participación política para continuar impulsando sus demandas.]

[Reciba ésta y toda la información de Contagio Radio en su correo [<http://bit.ly/1nvAO4u>]]
