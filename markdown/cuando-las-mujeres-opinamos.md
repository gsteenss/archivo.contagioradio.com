Title: Cuando las mujeres opinamos
Date: 2017-06-08 08:34
Category: Columnistas invitados, Opinion
Tags: Carolina Sanín, Machismo, mujeres
Slug: cuando-las-mujeres-opinamos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/06/Cuando-las-mujeres-opinamos.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Grafiti de Mujeres Creando 

#### [Por: Angela Galvis Ardila] 

Leía hace unos días, en una de esas entrevistas ligeras que se le hacen a personajes destacados en cualquier campo cultural, que en su sentir –es un escritor colombiano- la igualdad de géneros le resultaba una utopía. En principio y casi que por instinto de mujer le respondí que no, rotundamente no, pues es obvio que el peso de esa utopía nos toca cargarlo a nosotras. Segundos o minutos después y ya con mi ego tranquilo, aunque golpeado, me di cuenta de lo palpable de esta dura afirmación.

En una sociedad que gira alrededor del universo masculino, el hecho de nacer mujer es nacer con un coeficiente simbólico negativo. Aquí no me voy a detener en señalar las instituciones misóginas que tenemos, ni en la desigualdad salarial entre hombre y mujer, ni en muchas o muchísimas otras circunstancias que respaldan lo dicho. Solamente voy a hablar de algo mucho más elemental: cuando las mujeres opinamos.

Desconozco racionalmente el porqué de la reacción de algunos hombres, afortunadamente no todos, que frente a un comentario de alguna mujer lo primero que hacen es dibujar -¿o desdibujar?- una risa socarrona, para luego dar paso a la peor de las defensas de su vanidad varonil: la palmadita en el hombro en señal de una admiración no sentida y de una indiferencia que les llega a las entrañas.

 Pero si ese mismo comentario lo dice alguien sin formas femeninas, no hay palmadita, hay discusión y seria, sin ninguna clase de miramiento; como por ejemplo, descalificarlo porque sea bonito o feo, casado o soltero, gordo o flaco. No. A él se le trata como a un interlocutor válido, refutándole o avalándole su postura intelectual y brindándole un trato de pares, sin hacerle sentir mediante un trato tierno, que es el peor de los disfraces de la conmiseración, lo mucho que le alegra su proceso de evolución. El debate –cualquiera que éste sea- es un escenario en donde hombres y mujeres conformamos un grupo homogéneo y por ello debemos gozar de un trato equitativo.

Carolina Sanín es solo un ejemplo de lo que aquí estoy hablando. Ella ha sido castigada socialmente por usar expresiones que, según dicen, son ya pasadas de tono, y he visto a más de uno (hombres y mujeres) rasgarse las vestiduras y abrir los ojos preguntándose cómo es posible que una mujer de su nivel intelectual y socioeconómico y de su rol como docente de una de las universidades más prestigiosas de nuestro país, recurra a esos insultos escatológicos, “ella que es bonita hasta fea se ve”.

 ¿Era este verdaderamente el centro de la discusión sobre lo sucedido con Carolina Sanín? No. ¿Era su expresión “parido por el ano” lo que debía estar en primer lugar cuando se hablara de ella? No. ¿Importaba que Carolina Sanín fuera mujer para descalificarla por vulgar? No. ¿Era necesario en las entrevistas empezar por describirla físicamente? No. Esta sociedad es tan hipócrita que fueron pocos quienes se detuvieron a pensar en lo que realmente importaba en este caso, es decir, en la base estructural de lo que ella ha denunciado desde sus redes sociales, que no ha sido otra cosa que una crítica constante del statu quo. Es más, su salida de la Universidad de los Andes se dio luego de que publicara un artículo en donde censuraba, en tono fuerte y directo, al programa Ser Pilo Paga, a la Universidad de los Andes por percibir recursos públicos y sus condiciones de hacinamiento que llegó a comparar con una cárcel, además de no estar de acuerdo con las políticas del Rector. Pero qué importan estas denuncias si esta mujer ha cometido el gran sacrilegio de decir “parido por el ano” y ser, como algunos con tono de asco dicen: una histérica sobreactuada, pues, mientras las mujeres somos exageradas y emotivas cuando defendemos derechos, los hombres que hacen lo mismo, son respetados pues eso demuestra gallardía y en este país lo que se necesita es mano dura, obvio, siempre y cuando sea una mano varonil y si es blanca mucho mejor.

 Y entonces, mientras que a Carolina Sanín se le pone en la picota pública, a hombres que en cada frase recurren a expresiones insultantes, se les aplaude, se les invita a aperturas de eventos, se les conceden títulos honoríficos, o si no, miren a Fernando Vallejo, a quien, valga decir, admiro muchísimo, pero definitivamente el rasero con que se les mide a los dos, es totalmente distinto, así como lo es con el que nos miden a los hombres y a las mujeres.

Cito el caso de Carolina Sanín (como podría citar el de Claudia López o el de Piedad Córdoba) por no hablar de lo que vivimos día a día las mujeres de a pie, de las que nuestros nombres y apellidos no dicen mayor cosa, las que no recibimos el mismo trato cuando estamos ejerciendo nuestra profesión, las que a pesar de gozar de privilegios no nos hemos logrado apartar de sufrir en carne propia los vejámenes de una sociedad desigual en términos de género, desigualdad que por fortuna se ve contrarrestada cuando encontramos espacios en donde podemos contar con hombres inteligentes y cómplices de nuestro universo, los que, para alegría nuestra, cada vez son más, como también cada vez somos más las mujeres conscientes de la responsabilidad que tenemos para escribir una nueva y mejor historia de nuestro papel en estos tiempos que nos tocó vivir.

------------------------------------------------------------------------

###### Las opiniones expresadas en este artículo son de exclusiva responsabilidad del autor y no necesariamente representan la opinión de Contagio Radio. 
