Title: En Colombia han sido asesinados 170 excombatientes tras la firma del Acuerdo de Paz
Date: 2019-11-06 12:46
Author: CtgAdm
Category: Expreso Libertad
Tags: excombatientes, Expreso Libertad, FARC
Slug: en-colombia-han-sido-asesinados-170-excombatientes-tras-la-firma-del-acuerdo-de-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/partido_farc_afp_12_0.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

Isabela Sanroque, integrante del partido político **FARC,** estuvo en los micrófonos del Expreso Libertad para hablar sobre la difícil situación que tienen que afrontar las y los excombatientes en los espacios territoriales de capacitación y reincorporación y en las diversas ciudades en donde han sido asesinados 160 integrantes de esta organización y 30 familiares.

De acuerdo con Sanroque, esta situación es producto de la negligencia por parte del gobierno que se niega a reconocer el asesinato sistemático contra estas personas y de la falta de compromiso con la implementación de los Acuerdos de Paz, sin embargo aseguró que tanto los excombatientes como las comunidades han insistido en su voluntad por construir paz en Colombia.

le puede interesar:([La participación política de FARC no puede ser asesinada)](https://archivo.contagioradio.com/la-participacion-politica-de-farc-no-puede-ser-asesinada/)

 

\[embed\]https://youtu.be/sGfR7WOa2jc\[/embed\]

[De guerrillera en las FARC-EP a edilesa en Teusaquillo](http://partidofarc.com.co/es/actualidad/de-guerrillera-en-las-farc-ep-edilesa-en-teusaquillo-515)

 
