Title: "Desconociendo protocolo, Gobierno cierra puertas a futuros procesos de paz"
Date: 2019-01-25 15:15
Author: AdminContagio
Category: Entrevistas, Paz
Tags: ELN, Organizaciones defensoras de DDHH, paz, proceso de paz, Protocolo
Slug: protocolo-procesos-de-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/01/PPROCESOS-DE-PAZ-770x400.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [25 Ene 2019] 

El Movimiento Nacional de Víctimas de Crímenes de Estado (MOVICE), la Coordinación Colombia-Estados Unidos- Europa (COEUROPA) y otras organizaciones defensoras de derechos humanas, emitieron un comunicado en el que señalan la **gravedad de desconocer los protocolos pactados por el Estado colombiano en conjunto con los países garantes en la mesa de negociación con el ELN**, con los que se establecieron las acciones siguientes a una ruptura de las conversaciones.

En el documento, las organizaciones afirman que pedir a los países garantes faltar al papel que asumieron al inicio de las conversaciones es un agravio internacional y afecta las buenas relaciones recíprocas que se han tenido con países como Cuba o Venezuela que también acompañaron el proceso de paz con las FARC. (Le puede interesar: ["Respuesta de Duque a Noruega atenta contra la legalidad internacional"](https://archivo.contagioradio.com/respuesta-duque-noruega/))

Alfonso Castillo, integrante del MOVICE, señaló que con esta acción además **se busca el respeto de la política de Estado en búsqueda de la paz,** lo que implica el respeto por las instituciones creadas por el Acuerdo con las FARC, y el apoyo a la implementación del mismo. De otra parte, las organizaciones aclaran que **el cumplimiento del protocolo no exime al Estado colombiano de su compromiso con la determinación de la verdad sobre el atentado a la Escuela General Santander, ni a que los responsables de los hechos sean investigados, individualizados y llevados ante la justicia**.

Adicionalmente, las organizaciones advirtieron que el desconocimiento de este protocolo afecta el futuro desarrollo de procesos de paz, en tanto **se estaría afectando la credibilidad institucional y la legalidad de cualquier pacto firmado entre el Estado** colombiano y cualquier grupo insurgente al margen de la Ley. (Le puede interesar: ["Violar protocolo establecido con ELN sería un irrespeto a la comunidad internacional"](https://archivo.contagioradio.com/protocolo-eln/))

> [@Movicecol](https://twitter.com/Movicecol?ref_src=twsrc%5Etfw) y [@coeuropa](https://twitter.com/coeuropa?ref_src=twsrc%5Etfw): “Desconociendo los protocolos de ruptura de la negociación el gobierno colombiano cierra las puertas a futuros procesos de paz y vulnera el derecho internacional”: [pic.twitter.com/18lD08SmCk](https://t.co/18lD08SmCk)
>
> — Iván Cepeda Castro (@IvanCepedaCast) [24 de enero de 2019](https://twitter.com/IvanCepedaCast/status/1088461231344648192?ref_src=twsrc%5Etfw)
>
> <iframe id="audio_31791724" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_31791724_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

<iframe id="audio_31791724" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_31791724_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
