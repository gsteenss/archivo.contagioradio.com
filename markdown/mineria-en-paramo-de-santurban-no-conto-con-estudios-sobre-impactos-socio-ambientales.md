Title: Minería en páramo de Santurbán no contó con estudios sobre impactos socio-ambientales
Date: 2016-09-01 14:09
Category: Ambiente, Nacional
Tags: Ambiente, Banco Mundial, Páramo de Santubán
Slug: mineria-en-paramo-de-santurban-no-conto-con-estudios-sobre-impactos-socio-ambientales
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/09/Paramo-de-Santurban-e1472756604235.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Radio Macondo 

###### [1 Sep 2016] 

La Corporación Financiera Internacional del Banco Mundial (CFI), que invierte en el proyecto minero de oro Angostura en Santurbán de la multinacional **[Eco Oro Minerals](https://archivo.contagioradio.com/empresa-canadiense-demandaria-a-colombia-por-fallo-que-protege-paramo-de-santurban/), incumplió con la evaluación de los impactos sociales y ambientales necesarios** para llevar a cabo actividades mineras que tienen en riesgo 293 especies de fauna y 457 variedades de plantas, además de la producción de agua que se provee a municipios de Santander y norte de Santander, incluido la ciudad de Bucaramanga.

Así lo señala la Oficina del Asesor en Cumplimiento Ombudsman, CAO, (un mecanismo independiente de rendición de cuentas del Banco Mundial), en un informe donde se asegura que **la CFI no realizó los estudios necesarios para justificar su inversión respecto a temas como los impactos ambientales y sociales del proyect**o, así como la evaluación de la biodiversidad y del hábitat, por lo que la CAO indicó que la CFI debería retirar su inversión del proyecto minero.

Ese hecho fue denunciado por el Comité por la Defensa del Agua y el Páramo de Santurbán, con el apoyo del Centro para el Derecho Internacional Ambiental (CIEL), el Centro para la Investigación de Corporaciones Multinacionales (SOMO), la Asociación Interamericana para la Defensa del Ambiente (AIDA) y MiningWatch Canadá.

"**La biodiversidad existente en Santurbán es crítica para la provisión de agua**. Por tanto, cualquier amenaza sobre la misma afecta el suministro del líquido en el área metropolitana de Bucaramanga", afirma Alix Mancilla, del Comité de Santurbán.

La CAO determinó que "uno de los propósitos declarados de la inversión de la CFI era desarrollar los informes necesarios para determinar si el proyecto podría cumplir con los estándares" como lo es la realización  estudios frente a los impactos que pueda generar la actividad minera, sin embargo  la empresa Eco Oro Minerals no los realizó y pese a ello la CFI hizo la inversión.

"Si el propósito de la inversión era evaluar la viabilidad del proyecto, **no hay razón que justifique la falta de estudios necesarios para dicha valoración.** No es posible dar luz verde a un proyecto en una región tan crítica para los habitantes de Santurbán sin valorar sus consecuencias reales", declara Carla García Zendejas, de CIEL.

Por su parte, Carlos Lozano Acosta, abogado de AIDA, dice que **"el proyecto es ilegal, por eso la licencia fue negada en 2011 y la Corte Constitucional ratificó la prohibición de minería en páramos”**, ay agrega, “Nos preocupa también que la CFI invirtió en una empresa cuyo proyecto desde el comienzo era inviable y que demandó internacionalmente al Estado colombiano, que es parte del Banco Mundial."

Ante ese incumplimiento el informe de la CAO, concluye que  la CFI debe retirar la inversión en la empresa Eco Oro, Es hora que **la CFI retire su inversión de Eco Oro** de manera que “deje de estimular las inversiones en empresas mineras junior, tal como ha venido haciendo en Colombia y otros países, sabiendo los serios daños socioambientales que esto implica y el contexto de impunidad en que operan las empresas", como lo afirma Jen Moore, de MiningWatch.

Cabe recordar que la Corte Constitucional de Colombia estudia una acción de tutela interpuesta por el Comité de Santurbán, con el apoyo de AIDA, sobre la falta de participación ciudadana en la delimitación del páramo, cuya exploración y explotación de oro, **[afecta directamente al total de la población de Bucaramanga](https://archivo.contagioradio.com/por-derecho-al-agua-se-interpone-tutela-para-defender-paramo-de-santurban/), es decir más de 500 mil personas, ya que las sustancias que se usan para la minería contaminan el agua,** pues este ecosistema representa la fuente de agua dulce más importante de la región.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u)  o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](https://archivo.contagioradio.com/). 
