Title: Fumigaciones sobre resguardo indígena afectan a la comunidad
Date: 2015-01-05 14:29
Author: CtgAdm
Category: Comunidad
Slug: fumigaciones-sobre-resguardo-indigena-afectan-a-la-comunidad
Status: published

###### Fotografía: Comisión Intereclesial de Justicia y Paz 

<div>

###### [Descargar Audio](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/01/fsvQPmy8.mp3) 

A un mes de que el gobierno iniciara fumigaciones ilegales sobre las zonas de cultivo de la comunidad indígena del Resguardo de Guayacán, Santa Rosa, se han empezado a presentar problemas de salud, con síntomas como alergias en el cuerpo, mareos y vómitos. En estos momentos la comunidad no cuenta con un puesto de enfermería, y el puesto de salud más cercano se encuentra a más de 4 horas, en Buenaventura, Valle del Cauca.

</div>
