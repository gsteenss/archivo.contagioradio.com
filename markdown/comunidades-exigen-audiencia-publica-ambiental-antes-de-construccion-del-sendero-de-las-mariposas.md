Title: Comunidades exigen Audiencia Pública Ambiental antes de construcción del Sendero de las Mariposas
Date: 2020-03-03 17:25
Author: AdminContagio
Category: Ambiente, yoreporto
Tags: Ambientalismo, CAR, comuidades, sendero de las mariposas
Slug: comunidades-exigen-audiencia-publica-ambiental-antes-de-construccion-del-sendero-de-las-mariposas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/03/5febre_bogota2ph01_20190204030758.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:list -->

-   Organizaciones sociales citan rueda de prensa ante anuncios de la Alcaldesa de construir Sendero de las Mariposas, desconociendo compromisos de campaña adquiridos y sentencia del Consejo de Estado
-   El Proyecto Sendero de las Mariposas continúa con irregularidades y se pretende nuevamente violar el derecho ciudadano a la participación y el acceso oportuno a la información.

<!-- /wp:list -->

<!-- wp:paragraph -->

*Comunidades  
articuladas en la denominada “Mesa Cerros Orientales” citarán a rueda de prensa  
el* *próximo 4 de marzo a las 3 pm en las  
oficinas de Planeta Paz Calle 30 A No 6 – 22 Of. para ampliar la  
argumentación en torno al rechazo a la construcción del Sendero de las  
Mariposas.*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

*Bogotá, Marzo 3 de 2020* : Nuevamente vuelve a sonar la polémica del denominado “Sendero de las Mariposas” que había propuesto Enrique Peñalosa a raíz de los pronunciamientos realizados por Claudia López, Ricardo Lozano el pasado 26 de febrero de este año.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

**“El Sendero de las Mariposas será una realidad”** afirmó el ministro Lozano tras la visita realizada el pasado 26 de febrero junto a la alcaldesa Claudia López. Situación que generó nuevamente polémica en las comunidades que se verán afectadas por el proyecto.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Es por eso que mediante un comunicado público la organización **“Mesa de Cerros Orientales”** ha manifestado su inconformidad con Claudia López, el Ministro de Ambiente y la **CAR**, pues afirman que:

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

“dicho proyecto, impulsado por la anterior administración distrital de Enrique Peñalosa, pone en riesgo a los ecosistemas de los Cerros Orientales y a las distintas comunidades que históricamente los han habitado”.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En el documento recuerdan que el Consejo de Estado en 2013 se ordenó  
la consolidación de la Franja de Adecuación, donde se contempla una zona de  
consolidación de borde urbano para la contención de la expansión de la ciudad;  
y una zona de Aprovechamiento Público Prioritario cuyo suelo es de carácter  
rural y donde se propone desde el Fallo, que existan zonas de parques, de  
restauración ecológica, avistamiento de aves, huertas agroecológicas, entre  
otras, que sean de acceso público. Situación que el proyecto construido y  
publicitado por Enrique Peñalosa en su administración pasada, desconoce.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Según la organización, los funcionarios y la alcaldesa están desconociendo el proceso de licenciamiento ambiental ante la **ANLA**, cuyo trámite no puede avanzar sin que antes se realice la Audiencia Pública Ambiental que desde el 2019 solicitaron las comunidades. También señalan que por medio de la tutela T-003333 de 2019, el Juzgado Primero Administrativo Oral del Circuito Judicial de Bogotá y el Tribunal Administrativo de Cundinamarca reconoció que en el proceso de construcción del proyecto turístico y comercial se “violó el derecho al acceso a la información, en conexidad con el derecho a la participación ciudadana, y que para realizar la mencionada audiencia se requiere que las comunidades tengan acceso a la información en los tiempos oportunos”.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Finalmente, las personas articuladas en la Mesa Cerros Orientales denuncian que Claudia López está pasando por encima del Compromiso Ambiental por Bogotá que ella misma firmó donde se compromete a cumplir con el fallo del Consejo de Estado desarrollando la Franja de Adecuación y el Sendero de las Mariposas.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Le puede interesar: [Aspersión aérea con glifosato, una estrategia fallida que se repite](https://archivo.contagioradio.com/la-aspersion-aerea-con-glifosato-no-es-una-estrategia-efectiva/)

<!-- /wp:paragraph -->
