Title: Reajuste en costo de matrículas motiva asamblea permanente en la UPTC
Date: 2017-09-26 11:38
Category: Educación, Nacional
Tags: Movimiento estudiantil, UPTC
Slug: reajuste-en-costo-de-matriculas-motiva-asamblea-permanente-en-la-uptc
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/09/uptca-e1506445017720.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:El Diario] 

###### [25 Sept de 2017]

Estudiantes de la Universidad Pedagógica y Tecnológica de Colombia, UPTC, se encuentran en paro de actividades, mientras que realizan una asamblea permanente, para exigirle a las directivas del centro educativo que no aumente el costo en las matriculas, para que estudiantes de estratos **1,2 y 3, mayoría poblacional en esta institución, pueda ingresar a la universidad.**

De acuerdo a las directivas, el alza de la matricula se debería a la necesidad de no afectar la sostenibilidad financiera del centro educativo, sin embargo, Camilo Tavera, estudiante de Ciencias Sociales, afirmó que al ser una institución **Pública esta sostenibilidad debe ser garantizada por el Estado y no que recaiga en el bolsillo de los estudiantes. **(Le puede interesar: ["Universidad Nacional necesita presupuesto para seguir siendo pública"](https://archivo.contagioradio.com/presupuesto-un-regalo-urgente-para-la-u-nacional-en-sus-150-anos/))

De igual forma en un comunicado de presa manifestó que estas modificaciones ya habían sido compartidas al estudiantado en 7 diferentes ocasiones y que solo empezará a ser aplicado hasta el primer semestre de 2018, para los nuevos alumnos.

### **El alza a las matriculas** 

Tavera señaló que el sistema que se impartirá para el cobro de la matricula se basará en un sistema de puntos y variables que determinan cuanto debe **pagar el estudiante. Son 15 variables y cada una de ellas tendría un valor de 21.294 pesos**. Algunas de esas variables dependen del estrato socioeconómico, el número de familiares, si vive en vivienda propia o arrendada, si tiene hijos, entre otras.

[![WhatsApp Image 2017-09-25 at 5.02.33 PM](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/09/WhatsApp-Image-2017-09-25-at-5.02.33-PM.jpeg){.alignnone .size-full .wp-image-47130 width="480" height="650"}](UPTC)

Sumado a ello habría una base de 200.000 mil pesos que tendrían que pagar todos los estudiantes, más \$51.000 pesos de costo de carné y papelería. Es decir que si se hace este ejercicio con un estudiante que haya sacado 2 puntos en ingreso mensual, 20 en número de hijos, 20 en número de hermanos y 10 en vivienda, deberá pagar en total: **\$1.107.288 de puntos, más \$200.000 mil pesos de la base de pago y \$51.000 pesos de micelania, total: \$1.358.288.**

Para el caso de Tavera, actualmente su semestre cuesta \$300.000 pesos la tercera parte de lo que tendrán que pagar los nuevos alumnos que ingresen a la Universidad Pedagógica y Tecnológica de Colombia. Además, Tavera expresó que la gran mayoría de oferta para esta Universidad, proviene de **estratos 1,2 y 3, dejándolos sin la posibilidad de estudiar**.

“Hay carreras que se han *elitisado*, medicina cuesta 4 salarios mínimos, solamente las personas de buena familia entrarían ahí, nosotros exigimos que todas las carreras tengan el mismo costo de ingreso” afirmó. (Le puede interesar:["Colombia le cierra las puertas a la investigación y a la ciencia"](https://archivo.contagioradio.com/recorte-a-ciencia/))

Los estudiantes han expresado, que hasta el momento no se ha conocido la propuesta final por parte del Consejo Superior y si finalmente sería ese el aumento o podría elevarse aún más. **Frente a esta situación, se mantendrá la asamblea permanente hasta que el Consejo Superior frente la decisión de aumentar el costo de las matrículas.**

<iframe id="audio_21084576" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_21084576_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio]{.s1}](http://bit.ly/1ICYhVU)[.]{.s1}
