Title: Así fue el asesinato de Guillermo Pavi por parte de las FFMM
Date: 2015-04-15 13:26
Author: CtgAdm
Category: Movilización, Nacional
Tags: Cabildo López Adentro, ESMAD, Guillermo Paví, liberación de la madre tierra, Movimiento Juvenil Álvaro Ulcué, Norte del Cauca
Slug: asi-fue-el-asesinato-de-guillermo-pavi-por-parte-de-las-ffmm
Status: published

El **cabildo indígena “Lopez Adentro”** al que pertenecía **Guillermo Paví**, joven indígena de 19 años de edad relata los últimos momentos de la vida del comunero indígena que participaba desde el viernes 10 de Abril en el proceso de liberación de la Madre Tierra y se vinculó al grupo de jóvenes de la Guardia Indígena en la finca “La Emperatriz”, la más asediada por las FFMM y el ESMAD por ser colindante con la carretera panamericana según explican los líderes de las comunidades.

En el comunicado el resguardo indígena resalta las calidades personales de Guillermo y las razones por las cuales se vinculó al grupo que participa en la recuperación de tierras en el Norte del Cauca, como uno de los delegados de su comunidad. Según las autoridades indígenas, **Guillermo tenía 19 años de edad** y tenía un hijo para el cual, decía que recuperaría la tierra.

#### *"Guillermo Paví Ramos hizo parte del Movimiento Juvenil Álvaro Ulcué y de la Guarda indígena. Tenía 19 años. En la comunidad nunca faltaba a encuentros, reuniones, marchas, juego de fútbol, festivales. Se comprometió con María Alejandra y tuvieron un hijo, Deiner. Por las casas de la comunidad pasaba haciendo bromas, sacándole chiste a todo mundo. A su tío le decía: “qué hubo tío, vamos a liberar la madre tierra o qué” y su tío le respondía: “para qué querés tierra, ¿para dejarla enmontar?”. “Es que esa es la idea”, le respondía Guillermo, “hay que liberarla para dejarla descansar porque ahora está como esclava”. Pongan cuidado y verán que llenándose de monte es como la tierra va liberándose. El viernes se despidió de su familia, marchó de El Pílamo a La emperatriz, uno de los sitios de liberación de la madre tierra. Cuando se despidió de su familia le dijo:“Me voy para La Emperatriz a recuperar tierra porque necesito tierra para mi hijo”. María Alejandra le dio la bendición.* 

#### *Llegando a La Emperatriz se encontró con sus amigos y se ubicaron a unos metros de la trinchera que montó el ejército y el ESMAD, no había confrontación. Sonaron disparos, bajito, no eran de fusil, eran armas especiales de asalto, nos enteramos después. Dos balas entraron en su vientre pero él siguió caminando, solo dijo “me dieron”. Salió caminando hasta Bodega Alta, la vereda cercana. “Tengo sed, me están dando calambres”, les dijo a sus amigos. A la hora de llevarlo al hospital el ESMAD bloqueó la vía con la tanqueta. Cuando finalmente pudo ser llevado al hospital ya era tarde. Por eso decimos que Guillermo fue mandado a asesinar, porque primero le dispararon sin haber confrontación y después no lo dejaron sacar a tiempo para ser atendido. Lo mataron por liberar la madre tierra."* 

*Guillermo: buen viaje de retorno al hogar desde donde un día viniste a acompañarnos y darnos tu compromiso y tu alegría. Allá, en el territorio de los espíritus, estarás ahora haciendo chistes con tu papá, con Lorenzo, con los 20 de El Nilo, con Aldemar, con Marden, con Marleny. Allá estarás con tu bastón en firme, con tus ideas intactas y con una linterna alumbrándonos el camino. Por aquí vamos juntos, compañero.*

*Cabildo indígena de López Adentro*

Anexamos Biografía de Guillermo Pavi realizada por el Resguardo Indígena López Adentro

[Vida de Guillermo](https://es.scribd.com/doc/261971227/Vida-de-Guillermo "View Vida de Guillermo on Scribd") by [Contagioradio](https://www.scribd.com/contagioradio "View Contagioradio's profile on Scribd")

<iframe id="doc_47141" class="scribd_iframe_embed" src="https://www.scribd.com/embeds/261971227/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-lMNvSRvSuRRRnupKGwcs&amp;show_recommendations=true" width="60%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="0.7729220222793488"></iframe>
