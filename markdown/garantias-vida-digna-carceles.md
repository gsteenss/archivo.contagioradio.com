Title: No hay garantías para la vida digna en las cárceles de Colombia
Date: 2019-02-24 14:47
Category: Expreso Libertad
Tags: crisis carcelaria, hacinamiento, prisioneros
Slug: garantias-vida-digna-carceles
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/02/cárcel.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: AFP 

###### 21 Feb 2019 

**Gloria Silva**, abogada del Equipo Jurídico Pueblos, participó en este capítulo del Expreso Libertad, en el que **denunció la normalización de la violencia y la tortura al interior de las cárceles colombianas**, que continúan dando tratos inhumanos a quienes se encuentran en ellas.

**Desde requisas que violentan y amedrantan a los prisioneros, hasta el uso excesivo de la fuerza por parte de las autoridades**, son algunas de las denuncias constantes que han hecho los internos, sin que se tomen las medidas suficientes para garantizar una vida digna al interior de los penales.

<iframe id="audio_32741800" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_32741800_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Encuentre más información sobre Prisioneros políticos en[ Expreso libertad ](https://archivo.contagioradio.com/programas/expreso-libertad/)y escúchelo los martes a partir de las 11 a.m. en [Contagio Radio](https://archivo.contagioradio.com/alaire.html) 
