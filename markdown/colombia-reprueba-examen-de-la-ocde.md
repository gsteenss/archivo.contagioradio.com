Title: Colombia reprueba examen de la OCDE
Date: 2016-02-10 17:41
Category: Educación, Nacional
Slug: colombia-reprueba-examen-de-la-ocde
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/02/Educación-Colombia.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Noticias Colegios ] 

###### [10 Feb 2016 ]

De acuerdo con el más reciente informe de la OCDE, por lo menos uno de cada cuatro alumnos de 15 años de los 64 países que hicieron parte de las pruebas PISA 2012 **"no han alcanzado un nivel básico de conocimientos y habilidades en lectura, matemáticas y ciencia"**, es decir, no cuentan con capacidades para leer y utilizar lo leído para aprender, ni comprender conceptos matemáticos básicos.

Según indica la OCDE en Colombia el 73% de los estudiantes de 15 años tiene bajos niveles en matemáticas, 51% en lectura y 56% en ciencias. Teniendo en cuenta los factores que han determinado estos mínimos rendimientos, nuestro país cuenta con el** 88% de su población escolar viviendo en mínimas condiciones socio-económicas y con el 83% de éstos que no contaron con educación preescolar**. A ello se le suman los estereotipos de género que aún imperan en las prácticas educativas y la desigualdad entre la educación impartida en los ambientes citadinos y los rurales.

Como lo asevera el informe estos bajos niveles educativos generan implicaciones tanto para los individuos como para las naciones, "los alumnos con un rendimiento bajo a los 15  años tienen más riesgo de abandonar completamente sus estudios; y **cuando una gran proporción de la población carece de habilidades básicas, el crecimiento económico de un país a largo plazo se ve amenazado**".

Este informe concluye asegurando que alcanzar buenos niveles de rendimiento en estas asignaturas básicas depende de la construcción de políticas educativas que contemplen **programas especiales para los estudiantes con bajos rendimientos, para aquellos que vivan en zonas rurales, sean inmigrantes o pertenezcan a minorías lingüísticas**, así como de las acciones que se emprendan para eliminar estereotipos de género y desigualdades en el acceso a la educación temprana.
