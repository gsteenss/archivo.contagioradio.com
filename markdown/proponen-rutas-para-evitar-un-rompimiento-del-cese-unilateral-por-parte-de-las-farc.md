Title: Organizaciones presentarán propuesta que evite ruptura de cese unilateral
Date: 2015-11-03 17:24
Category: Nacional, Paz
Tags: Aumento militar en colombia, conflicto armado, Conpaz, Copamiento militar, Derechos Humanos, Desescalamiento del conflcito, Desescalamiento del conflicto en Colombia, DIPAZ, Justicia y Paz, paramilitarismo en Colombia, proceso de paz, Proceso de paz en Colombia, Radio derechos Humanos, Víctimas de colombia
Slug: proponen-rutas-para-evitar-un-rompimiento-del-cese-unilateral-por-parte-de-las-farc
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/11/fRENTE-AMPLIO.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto:contagioradio.com 

<iframe src="http://www.ivoox.com/player_ek_9261740_2_1.html?data=mpejk5yYdI6ZmKiakp2Jd6KmlJKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRlNPj0dTbx9OPttbowtiY0sbWpYzZ187hw9ePttbk1drfw5DIqc2fpMrgx5C5ssrgwtnS1MbQb46ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [María Eugenia Mosquera, CONPAZ] 

<iframe src="http://www.ivoox.com/player_ek_9261714_2_1.html?data=mpejk5yVeI6ZmKiakp2Jd6Klk5KSmaiRdo6ZmKiakpKJe6ShkZKSmaiRlNPj0dTbx9OPttbowtiY0sbWpYzZ187hw9ePttbk1drfw5DIqc2fpMrgx5C5ssrgwtnS1MbQb46ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Angela Robledo, Alianza Verde] 

###### [3 Nov 2015] 

El comunicado de las FARC que recoge las **denuncias de distintos frentes de esta organización por hostigamientos y ataques por parte de las fuerzas armadas,** afirma que el Cese al Fuego está en riesgo, lo cual alarma a distintos sectores del país.

**Frente Amplio por la Paz se reunió para abordar esta situación, allí ** lograron definir una propuesta que presentarán a los delegados de paz de las FARC y al Gobierno nacional, esta salida pretende evitar que termine el cese al fuego unilaterial definido por esta guerrilla desde el 20 de julio.

En dicha reunión se definió una comisión que viajará a la Habana a acordar el tramite, en donde los puntos fundamentales serán:

**1-** Comisión que viajará a la Habana a acordar una tramitología y una propuesta para que “el desescalamiento se lleve a los territorios en concreto” y además que se puedan “propiciar diálogos entre comandantes de las FARC y comandantes de las brigadas del ejército”, para que en vez de un copamiento militar, se de un “**copamiento humanitario**”.

**2-** Llevar “subcomisiones al Choco, al Caquetá, al Catatumbo y al Cauca que es donde se presenta el mayor número de denuncias y de alertas”

**3-** Una reunión con el ministro de defensa.

**4-**“Solicitar una ruta clara de la comisión de negociación” en la que buscan solicitarle a la mesa técnica encabezada por el general Flórez por parte del ejercito nacional y del comandante Antonio Lozada por parte de las FARC “celeridad, señalando que el camino no puede ser este de provocación y de muerte”, esto en cuanto al cese bilateral del fuego.

Ángela Robledo indica que hasta ahora “no ha habido respuesta”, pero que esperan que el día de hoy la mesa técnica retomará actividades y mientras tanto en los territorios con ayuda de veedurías territoriales **se preparará la visita a la Habana**, con apoyo de ConPaz, DiPaz y distintos representantes de la iglesia católica, que están todos dispuestos a “proteger el cese unilateral al fuego”, ya que ha significado la “protección de vidas”.

### **Comunidades quieren "Voluntad real del gobierno":** 

Al día siguiente del comunicado que detalla los hostigamientos y ataques contra las FARC, también presentaron un comunicado en donde cuestionan el aumento paralelo del paramilitarismo con estas acciones por parte de las fuerzas militares, el cual dice  “**reverdece el paramilitarismo**, continúa el asesinato selectivo, reaparecen los retenes, siempre cerca de las guarniciones militares y no se diga ahora que son las BACRIM, son expresiones diferentes, en ambas está demostrada con claridad la connivencia con las fuerzas estatales, con el establecimiento”.

Así María Eugenia Mosquera, integrante de CONPAZ, indica que este anuncio de las FARC despierta **“gran preocupación”** por el “copamiento de las estructuras paramilitares y que el gobierno realmente no hace nada” y reafirma “**lo valioso que ha sido el cese unilateral**” y la “voluntad que ha tenido las FARC para mantenerse en este cese".

Hoy la presencia militar está en “regiones donde nunca han estado antes los militares”, pero llegan a estos territorios con “**estigmatizaciones a los líderes**” y “violaciones a los derechos humanos”, pero “los paramilitares siguen afianzándose cada vez más con el control territorial”, afirma Mosquera, por tanto esta presencia militar qué papel cumple en la zona.

El copamiento militar implica para los habitantes que “no podemos movernos tranquilamente en el territorio”, ya que se ven sujetos a “tener que pedir permiso para ir a pescar, pedir permiso para ir a cazar”, un ejemplo es la situación del Río Naya donde los militares entraron desde el 22 d99e octubre a esta comunidad y aún hoy no se van y “**comienzan a señalar a la gente**” de colaborar con la guerrilla.

Así las comunidades exigen una “verificación a los territorios” con compañía del Frente Amplio, para que el gobierno tome la decisión que “agilice un cese bilateral y definitivo”, porque sino este proceso “cada vez será más complicado”, además que estas denuncias de las FARC las comunidades también las han hecho, la presencia paramilitar “no es mentira” y el gobierno “es el único que no quiere reconocer la presencia paramilitar”, evidenciándose así una falta de “voluntad real del gobierno”.
