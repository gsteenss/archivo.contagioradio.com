Title: ¿Qué viene para Santrich tras recuperar su libertad?
Date: 2019-05-30 16:22
Author: CtgAdm
Category: Judicial, Paz
Tags: Corte Constitucional, Corte Suprema de Justicia, procuraduria, santrich
Slug: que-viene-para-santrich-libertad
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/Santrich-en-libertar.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: @SantrichLibre  
] 

El pasado miércoles la Corte Suprema de Justicia (CSJ) determinó su idoneidad para juzgar a **Seuxis Paucias Hernández Solarte**, más conocido como Jesús Santrich, por considerar que es un aforado, y es este Tribunal el encargado de juzgarlo. En ese sentido, el caso Santrich pasará a la Sala de Instrucción de la Corte Suprema, mientras él recupera su libertad, con la que afrontará el proceso.

**Alirio Uribe Muñóz, abogado y exrepresentante a la cámara,** señaló que tras la salida de Santrich del Búnker de la Fiscalía, la Corte lo investigará, y si encuentra delitos lo vinculará y caputará; pero aclaró que esa situación podría tardar meses "porque la Corte no actúa en caliente", y tampoco procede como la Fiscalía "entonces, si emite una orden de captura, no lo hará por televisión".

El Abogado también criticó la decisión del procurador Fernando Carrillo, de solicitar la captura de Santrich, según el ministerio público, para que rinda indagatoria "por los presuntos delitos de concierto para delinquir con fines de narcotráfico y tráfico, fabricación o porte de estupefacientes agravado"; maniobra que Uribe Muñóz **señaló como parte de un show medíatico.** (Le puede interesar: ["JEP niega extradición de Hernández Solarte y ordena que sea liberado"](https://archivo.contagioradio.com/jep-niega-extradicion-de-jesus-santrich-y-ordena-que-sea-liberado/))

> Procuraduría ([@PGN\_COL](https://twitter.com/PGN_COL?ref_src=twsrc%5Etfw)) solicita a sala de instrucción de la Corte ([@CorteSupremaJ](https://twitter.com/CorteSupremaJ?ref_src=twsrc%5Etfw)) captura de 'Jesús Santrich' para que rinda indagatoria
>
> ? | <https://t.co/V4aFEXnXtj> [pic.twitter.com/dAFDm1YDPb](https://t.co/dAFDm1YDPb)
>
> — Procuraduría Colombia (@PGN\_COL) [29 de mayo de 2019](https://twitter.com/PGN_COL/status/1133817841776570371?ref_src=twsrc%5Etfw)

<p>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
</p>
Iván Cepeda, senador y quien estuvo pendiente del proceso de libertad de Santrich dijo que el integrante del partido político FARC, reiterará su compromiso con el Acuerdo de Paz. Por su parte, Uribe Muñóz concluyó que es probable que el martes de la próxima semana tome posesión Hernández Solarte como representante a la cámara frente a Alejandro Chacón, presidente de esa corporación.

> Se acaba de radicar en [@CorteSupremaJ](https://twitter.com/CorteSupremaJ?ref_src=twsrc%5Etfw) la manifestación de voluntad de Jesús Santrich, firmada por el mismo, de comparecer a los llamados que le haga esta Corte, donde esperamos sea esclarecido el montaje en su contra y se devele quienes son sus determinadores.[\#SantrichLibre](https://twitter.com/hashtag/SantrichLibre?src=hash&ref_src=twsrc%5Etfw) [pic.twitter.com/HXiUZEUUZr](https://t.co/HXiUZEUUZr)
>
> — \#SantrichLibre (@SantrichLibre) [30 de mayo de 2019](https://twitter.com/SantrichLibre/status/1134129545114718208?ref_src=twsrc%5Etfw)

<p>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
</p>
###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
