Title: Temor por intervención de Policía en protesta pacífica de estudiantes en la UIS
Date: 2020-04-30 11:41
Author: AdminContagio
Category: Actualidad, Movilización
Tags: Fuerza Pública, uis
Slug: temor-por-intervencion-de-policia-en-protesta-pacifica-de-estudiantes-en-la-uis
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/04/Estudiantes-en-toma-de-la-UIS.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:heading {"align":"right","level":6,"textColor":"cyan-bluish-gray"} -->

###### Foto: UNEES {#foto-unees .has-cyan-bluish-gray-color .has-text-color .has-text-align-right}

<!-- /wp:heading -->

<!-- wp:paragraph -->

Estudiantes de la Universidad Industrial de Santander (UIS) temen que la Fuerza Pública intervenga en la protesta pacífica que iniciaron desde el pasado martes 28 de abril en la sede central de la Institución, para pedir que se suspenda el semestre académico en este lugar, y se aplace el inicio del mismo en las sedes regionales hasta lograr condiciones adecuadas para el reinicio de las clases.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

María Aguilera, estudiante de la Universidad explica que luego de casi un mes de insistir en hablar con la administración de la Institución, dos estudiantes decidieron encadenarse el lunes en las rejas de la sede en Bucaramanga de la UIS. Posteriormente, el martes, otros jóvenes ingresaron al campus para apoyar la protesta y formar un campamento que presione a las directivas a hablar con el estamento estudiantil.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

No obstante, la Unión Nacional de Estudiantes de Educación Superior ([UNEES](https://www.facebook.com/photo/?fbid=522688751737052&set=a.145142972824967)) denunció que la administración respondió con la evacuación del personal de la Universidad, y la llegada de efectivos de Policía a la zona. Adicionalmente, Aguilera afirma que a las personas que hablaron con los uniformados les indicaron que las posibilidades de ingresar al campus eran altas dado que no había ninguna intención de parte de la administración de escuchar a los estudiantes.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Las exigencias del movimiento estudiantil en la UIS

<!-- /wp:heading -->

<!-- wp:paragraph -->

Aguilera señala que la principal petición del movimiento estudiantil tiene que ver con que se den las condiciones para que todos los estudiantes puedan continuar sus clases, es decir, mediante la gratuidad en las matrículas dado que por la contingencia del Covid-19, algunos tienen que decidir entre "comer o pagar el semestre". (Le puede interesar:["Eduación virtual por Covid-19, una decisión para la que no estamos preparados"](https://archivo.contagioradio.com/eduacion-virtual-por-covid-19-una-decision-para-la-que-no-estamos-preparados/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Para garantizar esto, proponen que se aplace el inicio del semestre en la sede de Bucaramanga y se suspenda en las sedes regionales, considerando que el inicio del semestre se daría por medios virtuales, y no está garantizado que toda la comunidad universitaria pueda participar de este tipo de clases por temas de acceso a internet.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Asimismo, los jóvenes han señalado la necesidad de responder a temas académicos que tienen que ver con el ejercicio de clases presenciales para materias que son fundamentalmente prácticas, y en el acompañamiento para realizar tesis, entre otros. Por estas razones, piden que sus voces sean atendidas y no se haga uso de la fuerza por sus reclamos.

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->
