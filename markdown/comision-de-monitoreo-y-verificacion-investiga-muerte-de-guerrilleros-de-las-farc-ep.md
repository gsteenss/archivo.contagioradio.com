Title: Comisión de verificación del cese al fuego investiga muerte de guerrilleros
Date: 2016-11-17 18:23
Category: Nacional, Paz
Tags: #AcuerdosYA, Cese al fuego bilateral
Slug: comision-de-monitoreo-y-verificacion-investiga-muerte-de-guerrilleros-de-las-farc-ep
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/comision-de-verificacion.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [17 Nov 2016] 

Luego de que dos guerrilleros miembros de las FARC-EP fueron dados de baja por la Fuerza Pública, la **Comisión de Verificiación y Monitoreo del cese al fuego bilateral informo que ya se encuentra realizando la investigación pertienenta para aclarar las circunstancias** de la  muerte de estos guerrilleros.

Para el analista “**este es un hecho preocupante pero previsible en este tipo de situaciones, cuando aún a pesar del cese al fuego bilateral están las estructuras integradas**, tanto en lo que tiene que ver con las FARC-EP  como los operativos de la Fuerza Pública”. Le puede interesar:["ONU y CELAC participarán en la verificación del cese al fuego y dejación de armas"](https://archivo.contagioradio.com/onu-y-celac-verificaran-cese-al-fuego-y-dejacion-de-armas/)

A su vez, Lozano afirma que el procedimiento a seguir es que se habrá la investigación por parte de la Comisión de Verificación del cese al fuego que posteriormente rinda el informe público de qué fue lo que sucedió, sobre todo por las explicaciones que da el comandante del Ejército **“No parece muy convincente que dos miembros de la guerrilla se vayan a armar un combate con una patrulla del ejército**, y ahora quieren adornar la situación diciendo que encontraron  información de inteligencia que no se mostrará”.

Por su parte las FARC-EP expresó en su cuenta de Twitter que se tienen que **“los mecanismos de monitoreo y verificación tiene que actuar con prontitud para establecer las muertes de los dos guerrilleros e impedir hechos similares”.**

Mientras que otras organizaciones como Aheramigua que lleva 10 años trabajando justamente en el lugar en donde se dieron los hechos, hizo un llamado y las partes que conforman la mesa de conversación y a los medios de información a esperar el esclarecimiento de los hechos, **en aras de propiciar un ambiente positivo para la implementación de los acuerdos de paz y la construcción de una paz estable y duradera.**

De igual forma las Fuerzas Armadas en un comunicado de prensa indicaron que estas acciones se desarrollaron en medio de operaciones, en donde tenían información que miembros de la guerrilla del ELN estaban realizando actividades ilícitas. En el momento en que dan de baja a los dos guerrilleros uno más logra entregarse y les informa que hace parte del frente 37 de las FARC-EP. Los hechos ocurrieron a 68 kilómetros de la zona de pre concentración establecida por el gobierno Nacional.

La Comisión de Monitoreo y Verificación del cese bilateral, también expresó que d**esde que se supo de esta situación activaron los mecanismos e inicio las labores de investigación y se encuentra en proceso de análisis detallado del incidente** y las circunstancias que lo rodearon. Finalmente habrá que esperar los resultados de esta investigación.

<iframe id="audio_13805248" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_13805248_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
