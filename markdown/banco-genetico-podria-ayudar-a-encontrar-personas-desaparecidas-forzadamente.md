Title: Banco Genético ha sido fundamental para las víctimas de desaparición forzada
Date: 2017-04-06 16:54
Category: Entrevistas, Nacional
Tags: banco genético, crimenes, desaparecidos, Desaparición forzada, víctimas
Slug: banco-genetico-podria-ayudar-a-encontrar-personas-desaparecidas-forzadamente
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/08/desaparecidos-7.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo] 

###### [06 Abr. 2017] 

Una de las grandes preguntas que cientos de familiares de víctimas de desaparición forzada se hacen es **¿cómo hacer para identificar de manera eficaz a su familiar? ¿cómo tener la certeza que los restos o el cuerpo que me entregan es efectivamente su familiar?** Ante estas interrogantes, hay países como Guatemala que han logrado tener avances significativos que ayudan a que el proceso de búsqueda de desaparecidos sea más acertado.

**Nancy Valdez, integrante de la Fundación de Antropología Forense de Guatemala** y quien desarrolló el **Banco Genético Nacional de Familiares y Víctimas**, estuvo acompañado el primer encuentro de la Red Latinoaméricana sobre Desaparición Forzada y desde su país escucharon las iniciativas creadas en Colombia, pero también aportaron sus conocimientos con ideas para ayudar a las familias que aún buscan a sus desaparecidos. Le puede interesar: [Víctimas de la Desaparición Forzada de Latinoamérica desarrollan encuentro en Colombia](https://archivo.contagioradio.com/el-primer-encuentro-de-la/)

“Nosotros en Guatemala tomamos el Banco Genético Nacional de Familiares y Víctimas y ya hemos logrado recuperar 8 mil víctimas del conflicto armado” contó Nancy. Le puede interesar: [El Estado colombiano es el responsable del 84% de las desapariciones forzadas](https://archivo.contagioradio.com/el-estado-colombiano-es-el-responsable-del-84-de-las-desapariciones-forzadas/)

**En Guatemala los cuerpos de las personas que eran detenidas fueron tirados o dejados a la intemperie** en cualquier lugar de ese país y además los militares usaban los centros de detención y lugares deshabitados para dejarlos, lo que dificultó el reconocimiento de las personas.

“Los familiares se han estado movilizando por años, solicitando la identificación de sus familiares y fue realmente el Banco Genético lo que vino a complementar y poder dar esta posibilidad a los familiares. **El Banco Genético no solo es la esperanza de los familiares, sino que también es un resultado técnico- científico comprobado”** añadió Nancy.

Este Banco ha dado la certeza a las familias de que la persona entregada es su familiar, lo que redunda en que sus esperanzas se mantengan latentes. Según la integrante de la Fundación Antropóloga Forense, la cifra de personas que han sido halladas y reconocidas en Guatemala suman las 3 mil, resultado que se logra luego de años de trabajo e incidencia.

Otro de los temas en los que Nancy asegura que continúan insistiendo es en el de la justicia, es decir, **no solamente es necesario reconocer e identificar a la persona que fue detenida y desaparecida, sino que también que los casos lleguen al sistema de justicia** y que sean juzgados y que se puedan involucrar a las personas que cometieron los hechos. Le puede interesar: [Fiscalía ha suspendido 35 mil investigaciones sobre desaparición forzada](https://archivo.contagioradio.com/fiscalia-ha-suspendido-35-mil-investigaciones-sobre-desaparicion-forzada/)

Para Nancy, los errores y la experiencia que se ha tenido en Guatemala deben ser útiles para los colombianos “nosotros ya llevamos un proceso recorrido, tuvimos una comisión de esclarecimiento, hemos trabajado varios casos de implicación de personas y **hemos puesto de forma central a los familiares de las víctimas, cosas que no vemos reflejadas en Colombia”** dijo.

Finalmente, Nancy dice que los familiares deben seguir organizándose, uniéndose y de esa manera encontrar la fuerza para **continuar en la búsqueda de sus seres queridos y de la verdad de lo que pasó en sus casos**. Le puede interesar: [99% de los casos de desaparición forzada están en la impunidad](https://archivo.contagioradio.com/99-de-los-casos-de-desaparicion-forzada-estan-en-la-impunidad/)

<iframe id="audio_18008670" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_18008670_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 

 
