Title: Madres Comunitarias denuncian presiones por parte del ICBF
Date: 2017-04-17 12:40
Category: DDHH, Nacional
Tags: Cristina Plazas, ICBF, Madres Comunitarias
Slug: en-2016-han-fallecido-50-madres-comunitarias-por-estres-y-falta-de-garantias-laborales
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/01/madres_comunitarias.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Zona Cero ] 

###### [17 Abr. 2017] 

Las madres comunitarias de todo el país se encuentran a la espera de la **ratificación por parte de la Corte Constitucional de la sentencia T-48 de 2016** que reconoce sus derechos laborales después de más de 30 años de trabajar para el Instituto Colombiano de Bienestar Familiar ICBF. Mientras eso sucede, las **denuncias de posibles presiones contra estas madres y el fallecimiento de varias no se han hecho esperar.**

“Lo que estamos exigiendo ahí no es sino un cumplimiento, una deuda social que tiene el país con las madres comunitarias. **Más de 30 años trabajándole a un programa sin nada.** Yo no entiendo como dicen que se va a quebrar el ICBF, eso es mentira” relató Olinda García líder nacional de las Madres Comunitarias

Según García, en lo que va corrido del 2017, 50 madres comunitarias han muerto en todo el país “anoche nos falleció una mujer joven y la razón es el estrés y por qué estrés, porque **el ICBF nos está presionando a todas porque estamos exigiendo nuestros derechos. El estrés nos está enfermando”.**

Relata la lideresa que en la actualidad el ICBF está realizando visitas todos los días a los hogares, exige adecuaciones a las casas de un día para otro **“cómo es posible que yo le prestó la casa al ICBF y me estén exigiendo que tengo que remodelarla** porque ya no les gusta después de 30 años de trabajar con el programa”.

Dicen las madres que como trabajadoras el ICBF debe garantizarles todos los elementos para poder trabajar de manera adecuada, **“es estresante, el volumen y la jornada laboral.** Las madres entran a las 7 y salen a las 4 de la tarde y a veces no tienen ni el tiempo para almorzar” recalca García. Adicionalmente, les están exigiendo cada vez más documentación. Le puede interesar: [80 mil madres comunitarias a la espera de que Corte Constitucional ratifique sus derechos](https://archivo.contagioradio.com/38523/)

### **¿Cristina Plazas les mintió a las madres comunitarias?** 

Dice García que, en el año 2016, más exactamente el 14 de abril, Cristina Plazas, directora del ICBF firmó un acta en la que se comprometía con las madres comunitarias a dar solución a sus demandas, pero a la fecha no ha cumplido los acuerdos.

“En la cara me lo dijo, **yo no voy a cumplir con esta acta porque yo la firmé para que levantaran el paro.** Eso es una falta de respeto con las mujeres. Yo no la entiendo a ella, es una mujer igual que nosotras, no entiendo de qué está hecha. Seguimos pidiendo que la saquen” aseveró García.

### **Por cada niño el presupuesto es de 2700 pesos para 3 raciones diarias** 

Manifiestan las madres comunitarias, que la ración que está dispuesta para cada niño o niña cuidado por ellas no alcanza. Son **2.700 pesos los que ha destinado el ICBF para dos refrigerios y un almuerzo.**

“Eso no alcanza. Ahora con ese aumento del 19% del IVA eso no alcanza ninguna plata” aseguró García. Le puede interesar: [Acuerdo con ICBF no satisface demandas de las madres comunitarias](https://archivo.contagioradio.com/acuerdo-de-icbf-no-satisface-demandas-de-las-madres-comunitarias/)

En caso de que la Corte Constitucional deje en firme esta sentencia se **vería en principio beneficiadas 106 madres comunitarias y las demás deberán entutelar** para poder acceder al beneficio de los derechos laborales.

De no dejar en firme la sentencia T-48, las madres comunitarias anunciaron que tomarán otras medidas más "drásticas" encaminadas a garantizar los derechos laborales de más de 63.000 madres comunitarias que hay en todo el país y en diferentes programas del ICBF. Le puede interesar: [106 Madres Comunitarias serían indemnizadas](https://archivo.contagioradio.com/106-madres-comunitarias-serian-indemnizadas/)

<iframe id="audio_18181620" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_18181620_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio]{.s1}](http://bit.ly/1ICYhVU)[.]{.s1} {#reciba-toda-la-información-de-contagio-radio-en-su-correo-o-escúchela-de-lunes-a-viernes-de-8-a-10-de-la-mañana-en-otra-mirada-por-contagio-radio. .p1}

<div class="ssba ssba-wrap">

</div>
