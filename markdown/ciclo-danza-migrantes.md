Title: La danza contemporánea retrata la realidad de los Migrantes
Date: 2018-06-07 11:00
Category: eventos
Tags: Bogotá, danza, migración
Slug: ciclo-danza-migrantes
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/06/danza.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Las Buenas Maneras/ Cortocinesis 

###### 06 Jun 2018 

Durante el mes de junio, la Factoría L´Expose presenta su ciclo de danza contemporánea "Migrantes" compuesta por las piezas "Phobia" de la compañía Cortocinesis y "Hermana República" de Buenas maneras, expresiones que abordan realidades sociales proponiendo una reflexión que trasciende a la técnica y al movimiento.

El ciclo inicia el 7 hasta el 16 con "Phobia", una creación con la dramaturgia de Johan Velandia y la dirección del coreógrafo y bailarín Yovanny Martínez, una experiencia para ver, una reflexión poética sobre el señalamiento y la discriminación, como prácticas que fragmentan la sociedad.

"Phobia" narra múltiples historias que acusan y desnudan la individualidad de los interpretes que en escena que se ven asfixiados en su constante tarea de ocultar y señalar. Con una apuesta escénica física, donde la atmósfera claustrofóbica que llega a crearse en buena parte de la obra, así como la uniformidad de los vestuarios y el continuo juego con la luz, generan la sensación de que pese a que estamos frente a un escenario dinámico, que dialoga con los cuerpos, funciona como un organismo que encierra y constriñe.

Acto seguido, del 21 al 30 de junio llega la obra "Hermana República, bajo la dirección de Catalina del Castillo, que comparte escenario con las bailarinas Isabel Barrios, Romina Guarisma e Isabel Story. Cuenta con la música original Santiago y Sergio Mejía Rocca creadores y directores de la Orquesta La 33, quienes han desarrollado una propuesta estética arraigada en las sonoridades del caribe y los llanos orientales.

"Hermana República" surge de una indagación entre artistas de diferentes nacionalidades para abordar, desde el cuerpo, las huellas de la migración forzada. El instinto del hogar, la poética del viaje, las dinámicas de encuentro y desencuentro, los choques culturales… son temas que dan vida a esta creación. (Le puede interesar: [Colis suspect: una mirada documental al cerrojo de la UE a los migrantes](https://archivo.contagioradio.com/colis-suspect-documental/))

El costo del bono de apoyo es de \$30.000 general y \$25.00 estudiantes y tercera edad, en la Factoría L’EXPLOSE Carrera 25 \# 50-34 Bogotá.
