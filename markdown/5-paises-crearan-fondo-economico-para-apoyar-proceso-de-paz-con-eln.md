Title: 5 países crearán fondo económico para apoyar proceso de paz con ELN
Date: 2017-05-30 14:51
Category: Nacional, Paz
Tags: dialogos de paz, ELN, Gobierno, Mesa de diálogo
Slug: 5-paises-crearan-fondo-economico-para-apoyar-proceso-de-paz-con-eln
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/02/proceso-de-paz-eln.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo] 

###### [30 May. 2017] 

A propósito del inicio de la tercera semana de diálogos entre el gobierno nacional y la guerrilla del ELN, se conoció del **apoyo económico que realizarán Alemania, Italia, Suiza, Suecia y Holanda para que la mesa** y los gastos que pueda tener el ELN sean cubiertos y se garantice la continuidad de las conversaciones.

Estas ayudas humanitarias serán solamente destinadas al ELN y no al Gobierno nacional, dado que el ejecutivo es quien sostiene a los integrantes del gobierno.

En la actualidad los y las integrantes de **la delegación de paz del ELN se encuentran revisando los protocolos internacionales,** ya que al ser un grupo que en la actualidad no ha dejado las armas, la financiación internacional no puede hacer entrega de dineros. Le puede interesar: [Diálogos con el ELN fortalecen  esfuerzos hechos en La Habana con las Farc](https://archivo.contagioradio.com/dialogos-con-el-eln-fortalecen-esfuerzos-hechos-en-la-habana-con-las-farc/)

A través de su cuenta de Twitter, **el ELN aseguró que se encuentra realizando reuniones con diversas personalidades e instituciones internacionales** como los delegados de la Unión Europea.

> [\#DelegaciónELN](https://twitter.com/hashtag/Delegaci%C3%B3nELN?src=hash) En horas de la tarde nos reuniremos con el delegado de la Unión Europea.
>
> — ELN-PAZ (@ELN\_Paz) [29 de mayo de 2017](https://twitter.com/ELN_Paz/status/869208460629618688)

<p>
<script src="//platform.twitter.com/widgets.js" async charset="utf-8"></script>
  
 

</p>
La mesa de conversaciones fue instalada en Ecuador, y los países en los cuales se realizarán las diversas reuniones serán Ecuador, Chile, Brasil, Venezuela, y Cuba quienes junto a Noruega son los garantes del proceso. Le puede interesar: ["Extrema derecha no reversará impulso de la paz" FARC y ELN](https://archivo.contagioradio.com/extrema-derecha-no-reversara-la-paz-farc-y-eln/)

### **¿Cómo está conformada la mesa de diálogos?** 

Cabe recordar que **cada una de las delegaciones cuenta con la participación de 30 representantes** y en las reuniones participan cerca de 10 personas, 5 que se encuentran como principales y 5 más que cumplen la función de suplentes.

Así mismo, cada delegación – ELN y Gobierno – podrá recibir asesoría de personas externas, las cuales considera importantes para avanzar en las negociaciones. Le puede interesar: [Video | Entrevista con Aureliano Carbonell, delegado de la mesa del ELN en Ecuador](https://archivo.contagioradio.com/entrevista-con-aureliano-carbonell-delegado-de-la-mesa-del-eln-en-quito/)

###### Reciba toda la información de Contagio Radio en [[su correo]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio]{.s1}](http://bit.ly/1ICYhVU)[.]{.s1}

<div class="ssba ssba-wrap">

</div>
