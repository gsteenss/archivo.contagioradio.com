Title: Brecha de género también está en el acceso a tecnología
Date: 2016-12-19 16:35
Category: Mujer, Nacional
Tags: Brecha de género, mujeres, tecnologia
Slug: brecha-de-genero-tambien-esta-en-el-acceso-a-tecnologia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/12/Mujeres-y-tecnologia.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Chicas en Tecnología] 

###### [19 Dic. 2016] 

El tema del empoderamiento de las mujeres hace cada vez más parte de los titulares de prensa, pero además de las agendas de gobiernos. De igual modo, cada día más mujeres se reconocen como agentes importantes de cambios para sus países, por lo que el nivel de reconocimiento de sus derechos ha ido en aumento.

Sin embargo, el camino que hace falta por recorrer es largo, no solamente en tópicos como el acceso al trabajo, salarios igualitarios, cargos altos, acceso a la tierra, sino que incluso el tema de la tecnología se ve mediado por la desigualdad.

Joanna Prieto, co-fundadora de **Geek Girls Latam asegura que esta iniciativa es una comunidad que empodera a las mujeres como agentes de cambio, por medio de la tecnología** de modo que ellas puedan apropiarse y crear tecnología para transformar de manera positiva su entorno, de  modo que al final puedan contribuir a construir una sociedad más inclusiva.

**Para Prieto la brecha que existe en el sector de la tecnología es muy amplia. En cifras, ni siquiera un 15% hace parte de la industria, es decir hay un déficit cercano a los 100 mil profesionales **“lo que queremos es visibilizar, hay un problema, hay una brecha, pero entre todos la podemos trabajar. Queremos que más niñas y adolescentes empiecen a interactuar con la tecnología” y añade “**en una escala de 1 a 10, Colombia está en un 5 en acceso a tecnología para las mujeres”.**

Una de las grandes metas que se ha planteado Geek Girls Latam es que las mujeres hagan parte del sector tecnológico y que puedan ser ellas quienes creen la tecnología “con la mirada de mujer, hacia una sociedad que necesita cada vez más el tema de inclusión” agrega Johana.

**Este problema no es exclusivo de Colombia, según esta organización el problema se presenta en el Mundo pero es América Latina la que muestra “un rezago impresionante”.**

 “La tecnología no puede ser solamente de unas personas que tienen el privilegio de adquirirla, porque es costosa o porque está pensada para ciertos niveles, **la tecnología no conoce de género, de status y en eso estaremos trabajando” concluyó Joanna.**

<iframe id="audio_15182541" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_15182541_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio]{.s1}](http://bit.ly/1ICYhVU)[.]{.s1} {#reciba-toda-la-información-de-contagio-radio-en-su-correo-o-escúchela-de-lunes-a-viernes-de-8-a-10-de-la-mañana-en-otra-mirada-por-contagio-radio. .p1}
