Title: Cese Unilateral del ELN completa 18 días con balance positivo
Date: 2020-04-18 17:45
Author: CtgAdm
Category: Actualidad, Nacional
Tags: cese bila, ELN, MOE
Slug: cese-unilateral-del-eln-completa-18-dias-con-balance-positivo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/03/Cese-al-fuego-unilateral-por-parte-del-ELN.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

El pasado 29 de marzo el ELN anuncio que realizaría un cese unilateral activo durante un mes, a partir del 1 al 30 de abril, *"como un gesto humanitario al pueblo colombiano que padece la devastación del coronavirus*", ante esto la Misión de Observación Electoral (MOE), dio a conocer **un balance del [cese al fuego del ELN](https://archivo.contagioradio.com/55-anos-eln-quiere-salida-conflicto/)durante del 1 al 17 de abril**.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Según la MOE desde que el ELN anuncio el cese, **su actividad disminuyó un 83% con respecto a marzo,** a su vez destaca cuatro hechos importantes atribuidos al grupo armado en los últimos 17 días.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El primero se registró el 7 de abril en Samaniego, Nariño donde la **población civil denunció amenazas por medio de graffitis firmados por el ELN**. Junto a esto se suman diferentes panfletos que aparecieron en el sur de Bolívar, donde al igual que en Nariño, el mensaje era para que la comunidad se quedan encerradas o *"recibir castigos al no cumplir con el aislamiento"*.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Posteriormente el 14 de marzo se registraron enfrentamientos y el asesinato de 8 disidentes, en la vereda El Encanto y la Playa en Argelia, Cauca; entre disidencias de las FARC y el autodenominado frente Carlos Patiño. Grupo armado ilegal que según la MOE, se podría vincular al frente José María Becerra del ELN.

<!-- /wp:paragraph -->

<!-- wp:quote -->

> *"Los enfrentamientos con el ELN ha dejado como resultado líderes sociales y excombatientes de FARC amenazados, desplazamiento forzado y así como la desaparición de cuatro campesinos".*
>
> <cite>MOE</cite>

<!-- /wp:quote -->

<!-- wp:paragraph -->

A pesar de que aún no se ha identificado a los responsables del **ataque que sufrió una misión médica el 4 de abril en Tumaco, Nariño** donde murieron dos personas; la disidencia de FARC por medio de un comunicado negó la autoria de los hechos, mientras que a la fecha el [ELN](https://www.justiciaypazcolombia.com/defendamos-la-paz-organizaciones-y-personas-proponen-el-nombramiento-de-juan-carlos-cuellar-como-gestor-de-paz/) no ha hecho ningún pronunciamiento.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Finalmente y algo que la MOE denominó como*" incumplimiento al cese de hostilidades*", es los hechos que vive la comunidad indigena Embera del resguardo Pichicora Chicucué Punto Alegre, quien quedó en medio de los enfrentamientos entre el Eln y las autodenominadas Autodefensas Gaitanistas de Colombia.

<!-- /wp:paragraph -->

<!-- wp:quote -->

> " La comunidad fue usada como un escudo humano quedando en confinamiento y siendo obligados a desplazarse para buscar refugio"
>
> <cite>MOE</cite>

<!-- /wp:quote -->

<!-- wp:paragraph -->

A este hecho se suma el **desplazamiento de al menos 193 indígenas pertenecientes a la vereda Nuevo Jerusalén en Bojayá** *"la comunidad anunció que los grupos ilegales les inmovilizaron los barcos y que no les permitían ir a buscar sus alimentos".*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Otro informe que también analiza esta situación es el del Centro de Recursos para el Análisis del Conflicto (CERAC), quienes el 15 de abril presentaron, "El monitor del cese unilateral del ELN", un reporte que incluye información desde el 9 de abril al 15 de marzo.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

CERAC señaló que *"no han registrado acciones ofensivas atribuidas al ELN y en consecuencia no hay reporte de víctimas asociadas a la violencia de este grupo guerrillero".*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

CERAC evidenció que desde hace **33 días no se registran operaciones militares ofensivas en contra del grupo guerrillero**; el reporte señala que **la última se desarrolló el 12 de marzo cuando el ejército realizó una operación en zona rural de San Pablo, Bolívar** *"la cual finalizó con un combate en el que murió un guerrillero, y un soldado resultó herido"*.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Durante el cese se han registrado 4 acciones violentas que no tuvieron atribución a un grupo armado específico pero que ocurrieron en zonas en las que el ELN sostiene disputas violentas con otros grupos armados en esta ocasión murieron tres personas y dos fueron secuestradas .

<!-- /wp:paragraph -->

<!-- wp:quote -->

> "*Es claro que la falta de presencia del Estado en muchas regiones del país ha permitido qué **grupos al margen de la ley estén aprovechando esta época de confinamiento obligatorio para mantener el control sobre las ayudas que puedan llegar** y el manejo de los recursos de ayuda humanitaria".*
>
> <cite>MOE</cite>

<!-- /wp:quote -->

<!-- wp:paragraph -->

Finalmente, la MOE agregó que el Estado debe garantizar la seguridad de los ciudadanos en el marco de esta emergencia producto de la pandemia, proponiendo incrementar la presencia estatal en las zonas donde se registran hechos violentos por parte de grupos armados ilegales.

<!-- /wp:paragraph -->
