Title: Solidaridad continental del ALBA se hace presente en Bogotá
Date: 2016-05-27 15:40
Category: El mundo, Movilización
Tags: alba, colombia, Movimientos sociales
Slug: solidaridad-continental-del-alba-se-hace-presente-en-bogota
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/05/ALBA-contagioradio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: albamovimientos] 

###### [27 May 2016]

La solidaridad, la alegría y los aprendizajes de más de 300 organizaciones sociales integrantes de los movimientos del ALBA, se dan cita este fin de semana en Bogotá para analizar las alternativas de articulación en cuanto a la **movilización, la comunicación y los derechos alcanzados** en los últimos 16 años en los países de América Latina.

Uno de los primeros pasos será el análisis de la situación que atraviesan los movimientos sociales y políticos en el continente y buscar **alternativas de trabajo en las que se logren defender las conquistas sociales que durante los últimos años se han alcanzado** con algunos gobiernos como el de Brasil, Argentina, Ecuador, Bolivia, Uruguay y Venezuela.

Según los voceros de los movimientos sociales del ALBA  una de las primeras tareas es expresar la solidaridad con los pueblos de América Latina que viven una “arremetida” de los sectores tradicionales de poder y poderes económicos en varios países. **Además aseguran que es un acto de respaldo al Paro Agrario que arranca el próximo 30 de Mayo en Colombia.**

Los voceros afirman que en el continente estamos en un momento en que las organizaciones sociales tienen una oportunidad, por una parte porque hay una serie de derechos alcanzados en los gobiernos de varios países y en los últimos 16 años. Es decir, que **la gente ya conoce los derechos que tiene** y es diferente volver atrás con los nuevos gobiernos como en Argentina o Brasil y que se están gestando o reorganizando en Ecuador y Bolivia.

 Para Andrés Gil, uno de los voceros por Colombia, este encuentro expresa un movimiento de solidaridad constante que se manifiesta en la solidez de la búsqueda de la paz en Colombia en el proceso con las FARC y con el ELN. Además señala que **ni Macri en Argentina, ni Temer en Brasil o la oposición en Venezuela van a lograr desmantelar la conciencia del pueblo de América Latina que está aprendiendo de los errores.**

Por último señalan como importante la articulación de comunicacional, puesto que lo que venden los medios de información masivos no es la realidad y por ello, lo que se logre desde los **medios de información nacidos de la sociedad, seguirá cobrando** fuerza y creciendo como lo están haciendo hasta el momento.
