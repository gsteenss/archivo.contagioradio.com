Title: Ejército dispara ráfagas de fusil contra viviendas en Catatumbo
Date: 2018-10-31 17:19
Author: AdminContagio
Category: DDHH, Entrevistas
Tags: Catatumbo, Disparos, ejercito, Pie de Fuerza, Teorama
Slug: ejercito-dispara-contra-catatumbo
Status: published

###### [Foto: Contagio Radio] 

###### [19 Oct 2018] 

Habitantes del corregimiento de **San Pablo en Teorama, Norte de Santander, denunciaron que en la noche del pasado martes, un helicóptero del Ejército abrió fuego contra algunas viviendas del lugar** indiscriminadamente, sin que existiera presencia de grupos armados ilegales en la región. Los hechos ocurrieron apenas dos días después de la presencia del presidente Duque y de su decisión de enviar mas pie de fuerza al Catatumbo.

**Robinson Salazar, presidente de la Junta de Acción Local (JAL) del Corregimiento e integrante de la Comisión por la Vida del Catatumbo,** afirmó que los disparos contra las viviendas de civiles se realizaron sin que se presentara un enfrentamiento armado; también agregó que tras las ráfagas no hubo respuesta armada en contra de la aeronave, lo que hace pensar que no había presencia del ELN o el EPL, grupos que se disputan el control territorial.

Salazar señaló que si bien es cierto que en la región del Catatumbo hacen presencia muchos actores armados ilegales, las personas que se encontraban en el Corregimiento eran civiles y no conocen ni tienen relación con las guerrillas. Por esta razón, **la acción del ejército fue irresponsable y pone en peligro a las más de 4 mil personas que habitan en el en San Pablo.**

El Presidente de la JAL,  recordó que en otra ocasión, la guerrilla había realizado un ataque con ráfagas de fusil contra la fuerza pública que se encontraba en las afueras del casco urbano del Corregimiento, como debe hacerlo según protocolos internacionales; de tal forma que esas acciones bélicas cercanas a población civil podrían causar una tragedia como la de Bojayá, en Chocó.

### **"Sufrimos la visita del presidente Duque y ahora vivimos esta situación"** 

Salazar señaló que la sensación de los habitantes del corregimiento es de preocupación, pues **luego de "sufrir" la visita del presidente Iván Duque tienen que vivir estos hechos,** y la intensificación de los enfrentamientos entre ELN y EPL. (Le puede interesar: ["Aumentar pie de fuerza en el Catatumbo no resolverá crisis humanitaria: ASCAMCAT"](https://archivo.contagioradio.com/aumentar-pie-de-fuerza-el-catatumbo-no-resolvera-crisis-humanitaria-ascamcat/))

Aunque la situación ya fue denunciada ante la Defensoría del Pueblo y la Comisión de Derechos humanos de la ONU, el integrante de la Mesa por la Vida sostuvo que de no encontrar soluciones prontas a la crisis, **también habrían desplazamientos masivos de personas**. (Le puede interesar: ["¿Dónde estaba el Ejército durante la masacre de El Tarra?"](https://archivo.contagioradio.com/donde-estaba-ejercito-durante-masacre/))

El líder se refirió específicamente al anunció que hizo el Presidente, sobre la llegada de 3 mil hombres más al Catatumbo, criticando que **no se invertía en educación, salud, vías o dinero para el agro, pero sí en fuerzas militares**; decisión que pondría en mayor riesgo a los habitantes de la región, en tanto se prevé un incremento en los combates, así **"si el ejército entra al casco urbano, nos tendremos que desplazar para proteger nuestras vidas"**.

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
