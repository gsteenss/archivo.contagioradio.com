Title: Lecciones cristianas, olvidadas por los cristianos I
Date: 2020-05-26 07:19
Author: A quien corresponde
Category: A quien corresponda, Opinion
Tags: Biblia, cristianismo, cristianismo., Cristianos, historia, Iglesias Cristianas, Política colombiana, religion
Slug: lecciones-cristiana-olvidadas-por-los-cristianos-i
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/carretera.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:heading {"align":"right","level":5} -->

##### Examínenlo todo y quédense con lo bueno *1(1Tesalinisenses 5,21)* {#examínenlo-todo-y-quédense-con-lo-bueno-11tesalinisenses-521 .has-text-align-right}

<!-- /wp:heading -->

<!-- wp:paragraph -->

*Diferenciar entre información y opinión es necesario para una buena decisión*.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":5} -->

##### Estimado  
**Hermano en la fe, Cristianos, cristianas, personas interesadas**  
Cordial saludo,

<!-- /wp:heading -->

<!-- wp:paragraph -->

La manera de comprender y vivir la fe de las millones de personas en el mundo y las cercanas a ti, está determinada por una corriente cristiana, tradicionalista religiosa y política, que tiene, entre otras, las siguientes características:

<!-- /wp:paragraph -->

<!-- wp:list {"ordered":true} -->

1.  **La convicción de:** tener toda la verdad y considerar equivocados, malos o perversos a quienes comprenden de otras manera lo religioso y lo político; estamos llamados y obligados a “convertir” a todo el mundo a su manera de comprender a Dios y vivir la fe y quien no lo hacen están endemoniado o condenado; no admitir cambios o actualizaciones de la comprensión y la práctica religiosa. Son una versión moderna de fariseos, maestros de la ley y sumos sacerdotes que persiguieron a Jesús de Nazaret.
2.  **El uso mensajes religiosos** y frases bíblicas desligadas del texto al que pertenecen, que repiten sin comprender bien lo que dicen, para atacar y defender, como arma contra “los otros”.
3.  **Escuchar “formalmente”** a los otros para atraerlos y “convertirlos”, sino no lo logran, los descalifican y se alejan, porque solo se relacionan bien con quienes comparten sus convicciones.
4.  **Le creen a sus líderes por encima de todo**, incluido de Dios, porque “ellos son los verdaderos interpretes” de la palabra y la voluntad Dios. A ellos les han entregado su voluntad y discernimiento y cualquier cuestionamiento lo consideran un ataque a su fe.

<!-- /wp:list -->

<!-- wp:paragraph -->

Esta corriente cristiana mundial, promueve fundamentalismos e integrismos religiosos, ideas racistas, xenófobas, clasistas o de extrema derecha; es producida, reproducida y controlada por sectores del poder económico, que han colocado el dinero, la ganancia y el poder por encima de todo, incluso de Dios; creen que la riqueza y el “éxito”, por cualquier medio, es una bendición de Dios.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":5} -->

##### Estas ideas religiosas son vividas y difundidas por gente buena y bien intencionada, que no sabe lo que se mueve en el fondo. Son pocos los que conocen sus intereses “ocultos”.

<!-- /wp:heading -->

<!-- wp:paragraph -->

No reconocen que los libros de la Biblia cuentan que Dios se revela y manifiesta al pueblo en situaciones históricas concretas y por medio de personas de carne y hueso y en situaciones históricas específicas, y que es necesario “estudiar” para reconocer la voluntad de Dios en ese tiempo y relacionarla con nuestro tiempo; por esto se les dificulta entender y vivir la fe en el tiempo presente y se refugian en el pasado.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Esta corriente usa una tendencia muy humana: ver la realidad desde nuestro lugar, desconociendo que esa misma realidad puede ser vista desde otros lugares, generando comprensiones diversas de la misma realidad. Cuando creemos que nuestra manera de ver y comprender tiene toda la razón, es la mejor y la verdadera, sin analizarlas, se generan los conflictos, porque desconocemos que la realidad se ve del color de los lentes que llevamos puestos, si son anaranjados así veremos realidad.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":5} -->

##### Hay lentes de muchos colores y el color de la realidad tiene muchos matices.

<!-- /wp:heading -->

<!-- wp:paragraph -->

Esta corriente dice, “de labios para afuera”, que la fe cristiana no tiene relación con el mundo social, político o económico. Desconoce que su teología fue elaborada por los teólogos de “los señores feudales” y del poder económico, de ayer y de hoy; por eso es lógico que digan que la voluntad de Dios es que los seres humanos sufran, se sacrifiquen, consideran las cosas del “mundo” como malas y materialista y las desprecien para poder gozar de la otra vida, para “llegar al cielo”, mientras ellos (los dueños del poder) se adueñan y disfrutan de todo lo creado por Dios para todos sus hijos e hijas; por eso el culto y la espiritualidad que predican y practican esta  
desligado de la vida y de la historia humana, silenciado la exigencia bíblica de justicia y hermandad, entendida como eliminación de la pobreza y como consecuencia de la Alianza entre Dios y el pueblo: **”no habrá pobres entre ustedes” (Dt 15,4)**.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":5} -->

##### La lección olvidada por cristianos y cristianas

<!-- /wp:heading -->

<!-- wp:paragraph -->

Los profetas y escritores bíblicos analizaban con profundidad la realidad humana, social y natural, lo mismo que la actuación de los dirigentes del  
pueblo. Luego confrontaban la realidad analizada para ver si estaba de acuerdo con la justicia, el derecho, la equidad y reparto de la tierra, establecidos por Dios al llegar a la tierra prometida.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":5} -->

#####   
En la biblia, la fidelidad a Dios es obrar justamente, la justicia es entendida como justicia social.

<!-- /wp:heading -->

<!-- wp:paragraph -->

Los escritores sagrados estudiaban los hechos concretos de la historia para descubrir, si el pueblo dirigido por sus autoridades, había actuado bien o mal, para entender lo que Dios quería en el presente y tomar decisiones más acertadas.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

**Veamos un hecho concreto:** los Hechos de los Apóstoles (5,17-42), cuentan que Pedro y Juan estaban anunciando públicamente a Jesús de Nazaret, lo que generó la persecución de las autoridades religiosas de Jerusalén; que los llevaron ante el sumo sacerdote, los miembros del partido de los saduceos, el sanedrín y el senado del pueblo; que el sumo sacerdote, les preguntó por qué estaban incumpliendo la prohibición predicar esas enseñanzas. Los apóstoles respondieron que “debían obedecer a Dios antes que a los hombres” y que por eso hablaban de Jesús de Nazaret, a “quien ustedes ejecutaron colgándolo de un madero” y a quien Dios “levantó y  
nombró jefe y salvador”.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Al oírlos, los asistentes “se indignaron y quería matarlos”. Entonces  
Gamaliel, un fariseo, doctor de la ley y muy estimado de todo el pueblo, mando a los apóstoles a que salieran y pidió que los escucharan, hizo una lectura de la historia y les recordó el caso de Teudas que se había hecho pasar por gran personaje pero que todo acabó cuando lo mataron, y el caso de Judas el Galileo, con el que pasó lo mismo.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Luego les aconsejó “dejar a esos hombres en paz porque si esa idea o esa obra es ellos fracasará pero si es de Dios nos podrán destruirlos”. Los dirigentes del pueblo acogieron su consejo.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":5} -->

##### Son muchas las enseñanzas o lecciones del texto bíblico, destaco tres:

<!-- /wp:heading -->

<!-- wp:list -->

-   **Analizar la realidad y dar la razón a quien la tiene, una actitud sabia.**

<!-- /wp:list -->

<!-- wp:paragraph -->

Gamaliel era fariseo y doctor de la ley (letrado o escriba), la persecución de los doctores de la ley y los fariseos contra Jesús fue implacable, no cesó hasta condenarlo a muerte. Las palabras más duras de Jesús en los evangelios fue contra ellos. En el capítulo 23 1-36 de Mateo, 7 veces los llama hipócritas y describe sus actuaciones con palabras muy duras.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

A pesar de ello, Jesús fue a casa de algunos fariseos y ahora, un fariseo y doctor de la ley, con su análisis de la historia, libra a los dos discípulos de la muerte.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

**La lección:** analizar los hechos del presente con una mirada histórica, “objetiva” y desapasionada, separando los hechos de los intereses y visiones del grupo social o religioso al que se pertenece y dar la razón a quien la tiene, independiente si es de los míos o está a mi favor, es una actitud sabia que evita graves injusticias y equivocaciones.

<!-- /wp:paragraph -->

<!-- wp:list -->

-   **Estudiar la historia para no repetirla.**

<!-- /wp:list -->

<!-- wp:paragraph -->

Gamaliel, en el momento que estaban condenando a muerte a dos hombres, recurre a la historia, la analiza y aprende de ella para iluminar el conflicto que vivían y proponer una decisión asertiva.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":5} -->

##### La historia es maestra sabia poco escuchada.

<!-- /wp:heading -->

<!-- wp:paragraph -->

¿Cuántas tragedias, injusticia y equivocaciones habríamos evitado si como personas, como iglesias, como país y como humanidad hubiésemos estudiado la historia y la realidad, conociendo mejor los intereses e intenciones de quienes dirigían las iglesias, los países y el mundo? ¿Cuántas equivocaciones cometidas por falta de análisis objetivo y desapasionado de la historia personal, familia, eclesial y social que explica por qué pensamos  
como pensamos y actuamos como actuamos; análisis que ayudaría superar lo mal hecho y a cultivar lo bueno?

<!-- /wp:paragraph -->

<!-- wp:list -->

-   **Mirar “más allá” de la confrontación en el momento de los conflictos.**

<!-- /wp:list -->

<!-- wp:paragraph -->

Las autoridades político-religiosas habían prohibido a los discípulos hablar en público de Jesús, los habían encarcelado por incumplir esta prohibición. Todas las autoridades se habían reunido para juzgarlos, cuando les preguntan por qué habían desobedecido, dijeron que debían obedecer a  
Dios antes que a ellos y les recordaron su responsabilidad en la muerte de Jesús, por eso querían condenarlos a muerte.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Gamaliel, sabiamente se coloca por encima de la confrontación y del momento, manda sacar a los discípulos y hace un análisis de casos parecidos en el pasado.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":5} -->

##### Esta mirada le permite ver con otra perspectiva el hecho actual, lo ilumina y lo lleva a hacer una sugerencia que aceptada permite salir bien del conflicto.

<!-- /wp:heading -->

<!-- wp:paragraph -->

Cuando abordamos una discusión o pelea con la lógica de siempre, difícilmente ganamos. Si asumimos esta lección bíblica podemos ganar cristiana, ética y humanamente.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Fraternalmente,  
P. Alberto Franco, CSsR, J&P  
francoalberto9@gmail.com

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

[Mas columnas de opinión](https://archivo.contagioradio.com/author/a-quien-corresponde/)

<!-- /wp:paragraph -->
