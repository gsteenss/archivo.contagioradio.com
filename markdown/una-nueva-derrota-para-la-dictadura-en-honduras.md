Title: Una nueva derrota para la dictadura en Honduras
Date: 2015-01-26 19:01
Author: CtgAdm
Category: Opinion
Tags: honduras, Juan Orlando Hernández, militares, policia
Slug: una-nueva-derrota-para-la-dictadura-en-honduras
Status: published

###### Foto: Conexhion 

#### **Gilberto Ríos Munguía**

El pasado sábado 24 de enero se discutía en el Congreso Nacional una controversial ley que pretendía poner bajo órdenes directas del Presidente de la República Juan Orlando Hernández, a más de 5,000 Policías Militares. El argumento era tener mayor control por parte del Poder Ejecutivo de una fuerza de seguridad que respondiera de manera más efectiva ante la grave crisis que atraviesa el país en esa materia.

Luego de los movimientos políticos recientes del Partido de Gobierno para cambiar artículos de la Constitución de la República en los que se habla de la posibilidad de la reelección presidencial -uno de los supuestos motivos que llevó a el país al Golpe de Estado Militar del 28 de junio de 2009 contra el Presidente José Manuel Zelaya Rosales-, el debate que mantuvo a la opinión pública agitada durante tres semanas, concluyó en la unidad de todas las fuerzas de oposición política de diferentes tendencias ideológicas al interno del Congreso, logrando una manifestación clara en contra de la propuesta de ley y dejando una de las derrotas políticas más notorias del gobierno del Partido Nacional.

La oposición obtuvo 67 votos contra 61 del oficialismo que necesitaba la mayoría calificada de 86 para que la ley fuera aprobada. En una sesión en la que se esperaba la tradicional aplanadora de los partidos tradicionales, las aspiración reeleccionista del mandatario y el peligro de dotar al mismo de una poderosa fuerza militar, crearon un ambiente de rechazo en la mayoría de la población y también el temor de regresar a acontecimientos como los del 2009, ruptura que ha significado -entre otras cosas- una profunda crisis económica, social y de seguridad pública como nunca antes en la historia del país.

El mismo día el Presidente Juan Orlando Hernández visitó el Congreso Nacional y haciendo uso de sus facultades constitucionales, propuso que la misma decisión que había sido rechazada contundentemente por la mayoría simple en el Congreso Nacional, pudiera ser votada en un Plebiscito, que para ser aprobado igualmente necesitará 86 votos de esta cámara.

El Presidente apuesta doble o nada en un arriesgado momento en el que su gestión es reprobada por los altos niveles de inseguridad y el desempleo que vive la población.

**[ @grillo779]**
