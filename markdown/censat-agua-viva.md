Title: Censat Agua Viva
Date: 2015-07-09 12:22
Author: AdminContagio
Slug: censat-agua-viva
Status: published

### CENSAT AGUA VIVA

[![Sentidos ambientales de la participación](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/09/Mercaderes-2-370x260.jpg){width="370" height="260"}](https://archivo.contagioradio.com/sentidos-ambientales-de-la-participacion/)  

#### [Sentidos ambientales de la participación](https://archivo.contagioradio.com/sentidos-ambientales-de-la-participacion/)

Posted by [Censat Agua Viva](https://archivo.contagioradio.com/author/censat/)[<time datetime="2019-09-20T10:00:37-05:00" title="2019-09-20T10:00:37-05:00">septiembre 20, 2019</time>](https://archivo.contagioradio.com/2019/09/20/)  
[![Memoria ambiental – un paso más hacia la reconciliación](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/08/memoria-ambiental-y-reconciliación-370x260.png){width="370" height="260"}](https://archivo.contagioradio.com/memoria-ambiental-un-paso-mas-hacia-la-reconciliacion/)  

#### [Memoria ambiental – un paso más hacia la reconciliación](https://archivo.contagioradio.com/memoria-ambiental-un-paso-mas-hacia-la-reconciliacion/)

Posted by [Censat Agua Viva](https://archivo.contagioradio.com/author/censat/)[<time datetime="2019-08-15T12:25:38-05:00" title="2019-08-15T12:25:38-05:00">agosto 15, 2019</time>](https://archivo.contagioradio.com/2019/08/15/)  
[![Alertando sobre la extracción de cobre](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/Alertando-sobre-la-extracción-de-cobre-370x260.jpg){width="370" height="260"}](https://archivo.contagioradio.com/alertando-sobre-la-extraccion-de-cobre/)  

#### [Alertando sobre la extracción de cobre](https://archivo.contagioradio.com/alertando-sobre-la-extraccion-de-cobre/)

Posted by [Censat Agua Viva](https://archivo.contagioradio.com/author/censat/)[<time datetime="2019-07-22T10:24:56-05:00" title="2019-07-22T10:24:56-05:00">julio 22, 2019</time>](https://archivo.contagioradio.com/2019/07/22/)  
[![La lucha contra el fracking, la defensa de la vida](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/fdfdfdf-370x260.jpg){width="370" height="260"}](https://archivo.contagioradio.com/lucha-contra-fracking-defensa-vida/)  

#### [La lucha contra el fracking, la defensa de la vida](https://archivo.contagioradio.com/lucha-contra-fracking-defensa-vida/)

Posted by [Censat Agua Viva](https://archivo.contagioradio.com/author/censat/)[<time datetime="2019-06-21T09:44:16-05:00" title="2019-06-21T09:44:16-05:00">junio 21, 2019</time>](https://archivo.contagioradio.com/2019/06/21/)  
[![Cumbre climática en Colombia con la deforestación en su máxima cumbre](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/censat--370x260.jpg){width="370" height="260"}](https://archivo.contagioradio.com/cumbre-climatica-en-colombia-con-la-deforestacion-en-su-maxima-cumbre/)  

#### [Cumbre climática en Colombia con la deforestación en su máxima cumbre](https://archivo.contagioradio.com/cumbre-climatica-en-colombia-con-la-deforestacion-en-su-maxima-cumbre/)

Posted by [Censat Agua Viva](https://archivo.contagioradio.com/author/censat/)[<time datetime="2019-05-18T18:22:45-05:00" title="2019-05-18T18:22:45-05:00">mayo 18, 2019</time>](https://archivo.contagioradio.com/2019/05/18/)  
[![¡Ríos Libres para cosechar la Paz!](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/03/censat-370x260.jpg){width="370" height="260"}](https://archivo.contagioradio.com/rios-libres-para-cosechar-la-paz/)  

#### [¡Ríos Libres para cosechar la Paz!](https://archivo.contagioradio.com/rios-libres-para-cosechar-la-paz/)

Posted by [](https://archivo.contagioradio.com/author/)[<time datetime="2019-03-30T06:00:30-05:00" title="2019-03-30T06:00:30-05:00">marzo 30, 2019</time>](https://archivo.contagioradio.com/2019/03/30/)  
[![La Guajira, volver a soñar un territorio libre y vivo](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/03/Foto-demanda-cerrejon-370x260.jpg){width="370" height="260"}](https://archivo.contagioradio.com/la-guajira-volver-a-sonar-un-territorio-libre-y-vivo/)  

#### [La Guajira, volver a soñar un territorio libre y vivo](https://archivo.contagioradio.com/la-guajira-volver-a-sonar-un-territorio-libre-y-vivo/)

Posted by [Contagio Radio](https://archivo.contagioradio.com/author/contagioradio/)[<time datetime="2019-03-11T08:21:36-05:00" title="2019-03-11T08:21:36-05:00">marzo 11, 2019</time>](https://archivo.contagioradio.com/2019/03/11/)  
[![2018: Un año de movilizaciones sociales](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/12/movilización-censat-770x400-370x260.jpg){width="370" height="260"}](https://archivo.contagioradio.com/2018-un-ano-de-movilizaciones-sociales/)  

#### [2018: Un año de movilizaciones sociales](https://archivo.contagioradio.com/2018-un-ano-de-movilizaciones-sociales/)

Posted by [Censat Agua Viva](https://archivo.contagioradio.com/author/censat/)[<time datetime="2018-12-23T11:55:27-05:00" title="2018-12-23T11:55:27-05:00">diciembre 23, 2018</time>](https://archivo.contagioradio.com/2018/12/23/)  
[![De 1994 a 2018 tejiendo defensa territorial](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/09/67920077_2249258751790002_2861589769903669248_n-370x260.jpg){width="370" height="260"}](https://archivo.contagioradio.com/de-1994-a-2018-tejiendo-defensa-territorial/)  

#### [De 1994 a 2018 tejiendo defensa territorial](https://archivo.contagioradio.com/de-1994-a-2018-tejiendo-defensa-territorial/)

Posted by [Contagio Radio](https://archivo.contagioradio.com/author/admin/)[<time datetime="2018-09-28T07:00:31-05:00" title="2018-09-28T07:00:31-05:00">septiembre 28, 2018</time>](https://archivo.contagioradio.com/2018/09/28/)  
[![\#TerritoriosLibresDeFracking](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/WhatsApp-Image-2019-06-07-at-5.57.15-PM-370x260.jpeg){width="370" height="260"}](https://archivo.contagioradio.com/territorios-libres-de-fracking/)  

#### [\#TerritoriosLibresDeFracking](https://archivo.contagioradio.com/territorios-libres-de-fracking/)

Posted by [Censat Agua Viva](https://archivo.contagioradio.com/author/censat/)[<time datetime="2018-09-10T08:54:40-05:00" title="2018-09-10T08:54:40-05:00">septiembre 10, 2018</time>](https://archivo.contagioradio.com/2018/09/10/)  
[![Superar la retórica ambiental](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/08/censat-370x260.jpg){width="370" height="260"}](https://archivo.contagioradio.com/superar-la-retorica-ambiental/)  

#### [Superar la retórica ambiental](https://archivo.contagioradio.com/superar-la-retorica-ambiental/)

Posted by [](https://archivo.contagioradio.com/author/)[<time datetime="2018-08-31T06:00:20-05:00" title="2018-08-31T06:00:20-05:00">agosto 31, 2018</time>](https://archivo.contagioradio.com/2018/08/31/)  
[![No necesitas a Hidroituango para conectar tu iPhone](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/07/Ituango-370x260.jpg){width="370" height="260"}](https://archivo.contagioradio.com/no-necesitas-a-hidroituango-para-conectar-tu-iphone/)  

#### [No necesitas a Hidroituango para conectar tu iPhone](https://archivo.contagioradio.com/no-necesitas-a-hidroituango-para-conectar-tu-iphone/)

Posted by [](https://archivo.contagioradio.com/author/)[<time datetime="2018-07-23T06:00:28-05:00" title="2018-07-23T06:00:28-05:00">julio 23, 2018</time>](https://archivo.contagioradio.com/2018/07/23/)  
[![La transición energética en Colombia: un asunto que compete a todos los sectores](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/05/energias-colombia-370x260.jpg){width="370" height="260"}](https://archivo.contagioradio.com/la-transicion-energetica-en-colombia-un-asunto-que-compete-a-todos-los-sectores/)  

#### [La transición energética en Colombia: un asunto que compete a todos los sectores](https://archivo.contagioradio.com/la-transicion-energetica-en-colombia-un-asunto-que-compete-a-todos-los-sectores/)

Posted by [](https://archivo.contagioradio.com/author/)[<time datetime="2018-05-04T12:17:41-05:00" title="2018-05-04T12:17:41-05:00">mayo 4, 2018</time>](https://archivo.contagioradio.com/2018/05/04/)  
[![14 de marzo, un día para reclamar la liberación de nuestros ríos](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/03/rios-vivos-370x260.jpg){width="370" height="260"}](https://archivo.contagioradio.com/14-de-marzo-un-dia-para-reclamar-la-liberacion-de-nuestros-rios/)  

#### [14 de marzo, un día para reclamar la liberación de nuestros ríos](https://archivo.contagioradio.com/14-de-marzo-un-dia-para-reclamar-la-liberacion-de-nuestros-rios/)

Posted by [](https://archivo.contagioradio.com/author/)[<time datetime="2018-03-15T14:32:00-05:00" title="2018-03-15T14:32:00-05:00">marzo 15, 2018</time>](https://archivo.contagioradio.com/2018/03/15/)  
[![En memoria de Berta Cáceres y todas las rebeldías](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/03/mujeres-defensoras-370x260.jpg){width="370" height="260"}](https://archivo.contagioradio.com/en-memoria-de-berta-caceres-y-todas-las-rebeldias/)  

#### [En memoria de Berta Cáceres y todas las rebeldías](https://archivo.contagioradio.com/en-memoria-de-berta-caceres-y-todas-las-rebeldias/)

Posted by [](https://archivo.contagioradio.com/author/)[<time datetime="2018-03-08T10:01:35-05:00" title="2018-03-08T10:01:35-05:00">marzo 8, 2018</time>](https://archivo.contagioradio.com/2018/03/08/)  
[![Aguas arriba, de cómo seguir alimentando nuestras luchas ambientalistas](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/02/censat-370x260.jpg){width="370" height="260"}](https://archivo.contagioradio.com/aguas-arriba-de-como-seguir-alimentando-nuestras-luchas-ambientalistas/)  

#### [Aguas arriba, de cómo seguir alimentando nuestras luchas ambientalistas](https://archivo.contagioradio.com/aguas-arriba-de-como-seguir-alimentando-nuestras-luchas-ambientalistas/)

Posted by [Contagio Radio](https://archivo.contagioradio.com/author/contagioradio/)[<time datetime="2018-02-15T06:00:45-05:00" title="2018-02-15T06:00:45-05:00">febrero 15, 2018</time>](https://archivo.contagioradio.com/2018/02/15/)  
[![¿PDET – Enfoque territorial de quién?](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/12/imagen-edit_PDET-370x260.png){width="370" height="260"}](https://archivo.contagioradio.com/pdet-enfoque-territorial-de-quien/)  

#### [¿PDET – Enfoque territorial de quién?](https://archivo.contagioradio.com/pdet-enfoque-territorial-de-quien/)

Posted by [](https://archivo.contagioradio.com/author/)[<time datetime="2017-12-21T11:11:09-05:00" title="2017-12-21T11:11:09-05:00">diciembre 21, 2017</time>](https://archivo.contagioradio.com/2017/12/21/)  
[![El disfraz democrático del avance minero-energético](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/12/avance-minero-energético-370x260.jpg){width="370" height="260"}](https://archivo.contagioradio.com/el-disfraz-democratico-del-avance-minero-energetico/)  

#### [El disfraz democrático del avance minero-energético](https://archivo.contagioradio.com/el-disfraz-democratico-del-avance-minero-energetico/)

Posted by [](https://archivo.contagioradio.com/author/)[<time datetime="2017-12-06T06:00:09-05:00" title="2017-12-06T06:00:09-05:00">diciembre 6, 2017</time>](https://archivo.contagioradio.com/2017/12/06/)  
[![Tres desafíos para defender el páramo de Santurbán](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/11/paramo-de-santurban-370x260.jpg){width="370" height="260"}](https://archivo.contagioradio.com/tres-desafios-para-defender-el-paramo-de-santurban/)  

#### [Tres desafíos para defender el páramo de Santurbán](https://archivo.contagioradio.com/tres-desafios-para-defender-el-paramo-de-santurban/)

Posted by [](https://archivo.contagioradio.com/author/)[<time datetime="2017-11-24T09:53:02-05:00" title="2017-11-24T09:53:02-05:00">noviembre 24, 2017</time>](https://archivo.contagioradio.com/2017/11/24/)  
[![Memoria ambiental: el desafío de enunciar la vida](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/10/censat-370x260.jpg){width="370" height="260"}](https://archivo.contagioradio.com/memoria-ambiental-el-desafio-de-enunciar-la-vida/)  

#### [Memoria ambiental: el desafío de enunciar la vida](https://archivo.contagioradio.com/memoria-ambiental-el-desafio-de-enunciar-la-vida/)

Posted by [](https://archivo.contagioradio.com/author/)[<time datetime="2017-10-11T06:00:42-05:00" title="2017-10-11T06:00:42-05:00">octubre 11, 2017</time>](https://archivo.contagioradio.com/2017/10/11/)  
[![¡Dejen el carbón en las entrañas de la tierra!](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/09/paremos-la-mina-370x260.jpg){width="370" height="260"}](https://archivo.contagioradio.com/dejen-el-carbon-en-las-entranas-de-la-tierra/)  

#### [¡Dejen el carbón en las entrañas de la tierra!](https://archivo.contagioradio.com/dejen-el-carbon-en-las-entranas-de-la-tierra/)

Posted by [](https://archivo.contagioradio.com/author/)[<time datetime="2017-09-22T06:00:14-05:00" title="2017-09-22T06:00:14-05:00">septiembre 22, 2017</time>](https://archivo.contagioradio.com/2017/09/22/)  
[![Pago por servicios ambientales: Algunos riesgos de mercantilizar todo](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/09/servicios-ambientales-370x260.jpg){width="370" height="260"}](https://archivo.contagioradio.com/pago-por-servicios-ambientales-algunos-riesgos-de-mercantilizar-todo/)  

#### [Pago por servicios ambientales: Algunos riesgos de mercantilizar todo](https://archivo.contagioradio.com/pago-por-servicios-ambientales-algunos-riesgos-de-mercantilizar-todo/)

Posted by [](https://archivo.contagioradio.com/author/)[<time datetime="2017-09-11T10:07:52-05:00" title="2017-09-11T10:07:52-05:00">septiembre 11, 2017</time>](https://archivo.contagioradio.com/2017/09/11/)  
[![Paz y participación en Colombia](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/08/minga-indigena-370x260.jpg){width="370" height="260"}](https://archivo.contagioradio.com/paz-y-participacion-en-colombia/)  

#### [Paz y participación en Colombia](https://archivo.contagioradio.com/paz-y-participacion-en-colombia/)

Posted by [](https://archivo.contagioradio.com/author/)[<time datetime="2017-07-25T10:56:58-05:00" title="2017-07-25T10:56:58-05:00">julio 25, 2017</time>](https://archivo.contagioradio.com/2017/07/25/)  
[![Así camina la lucha ambiental en Colombia](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/08/Foto-editorial-Censat-370x260.jpg){width="370" height="260"}](https://archivo.contagioradio.com/asi-camina-la-lucha-ambiental-en-colombia/)  

#### [Así camina la lucha ambiental en Colombia](https://archivo.contagioradio.com/asi-camina-la-lucha-ambiental-en-colombia/)

Posted by [](https://archivo.contagioradio.com/author/)[<time datetime="2017-06-09T10:50:33-05:00" title="2017-06-09T10:50:33-05:00">junio 9, 2017</time>](https://archivo.contagioradio.com/2017/06/09/)  
[![Ecos de la consulta popular de Cajamarca](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/05/consulta-popular-370x260.jpg){width="370" height="260"}](https://archivo.contagioradio.com/ecos-de-la-consulta-popular-de-cajamarca/)  

#### [Ecos de la consulta popular de Cajamarca](https://archivo.contagioradio.com/ecos-de-la-consulta-popular-de-cajamarca/)

Posted by [](https://archivo.contagioradio.com/author/)[<time datetime="2017-05-26T11:03:39-05:00" title="2017-05-26T11:03:39-05:00">mayo 26, 2017</time>](https://archivo.contagioradio.com/2017/05/26/)  
[![¿Quién dijo miedo?](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/05/Cumaral-370x260.jpg){width="370" height="260"}](https://archivo.contagioradio.com/quien-dijo-miedo/)  

#### [¿Quién dijo miedo?](https://archivo.contagioradio.com/quien-dijo-miedo/)

Posted by [](https://archivo.contagioradio.com/author/)[<time datetime="2017-05-10T11:58:03-05:00" title="2017-05-10T11:58:03-05:00">mayo 10, 2017</time>](https://archivo.contagioradio.com/2017/05/10/)  
[![Desde las entrañas de la tierra y junto a los gritos del agua, Cajamarca dijo NO a la minería.](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/03/cajamarca-370x260.jpg){width="370" height="260"}](https://archivo.contagioradio.com/cajamarca-consulta-minera/)  

#### [Desde las entrañas de la tierra y junto a los gritos del agua, Cajamarca dijo NO a la minería.](https://archivo.contagioradio.com/cajamarca-consulta-minera/)

Posted by [](https://archivo.contagioradio.com/author/)[<time datetime="2017-03-31T14:15:07-05:00" title="2017-03-31T14:15:07-05:00">marzo 31, 2017</time>](https://archivo.contagioradio.com/2017/03/31/)  
[![Crece la movilización nacional contra la construcción de proyectos hidroeléctricos y mineros](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/03/unnamed-2-370x260.jpg){width="370" height="260"}](https://archivo.contagioradio.com/movilizacion-hidroelectricos-mineros/)  

#### [Crece la movilización nacional contra la construcción de proyectos hidroeléctricos y mineros](https://archivo.contagioradio.com/movilizacion-hidroelectricos-mineros/)

Posted by [](https://archivo.contagioradio.com/author/)[<time datetime="2017-03-24T09:10:33-05:00" title="2017-03-24T09:10:33-05:00">marzo 24, 2017</time>](https://archivo.contagioradio.com/2017/03/24/)  
[![La participación de las mujeres: una clave para la construcción de paz con justicia social y ambiental](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/03/mujeres-el-ambiente-y-la-paz-370x260.jpg){width="370" height="260"}](https://archivo.contagioradio.com/la-participacion-de-las-mujeres-una-clave-para-la-construccion-de-paz-con-justicia-social-y-ambiental/)  

#### [La participación de las mujeres: una clave para la construcción de paz con justicia social y ambiental](https://archivo.contagioradio.com/la-participacion-de-las-mujeres-una-clave-para-la-construccion-de-paz-con-justicia-social-y-ambiental/)

Posted by [](https://archivo.contagioradio.com/author/)[<time datetime="2017-03-03T08:31:47-05:00" title="2017-03-03T08:31:47-05:00">marzo 3, 2017</time>](https://archivo.contagioradio.com/2017/03/03/)  
[![Agua: Más allá del Derecho Fundamental](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/02/agua-censat-370x260.jpg){width="370" height="260"}](https://archivo.contagioradio.com/36440-2/)  

#### [Agua: Más allá del Derecho Fundamental](https://archivo.contagioradio.com/36440-2/)

Posted by [](https://archivo.contagioradio.com/author/)[<time datetime="2017-02-18T05:00:47-05:00" title="2017-02-18T05:00:47-05:00">febrero 18, 2017</time>](https://archivo.contagioradio.com/2017/02/18/)

Artículos recientes:
--------------------

<article id="post-73933">
<figure>
[![Sentidos ambientales de la participación](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/09/Mercaderes-2.jpg){width="720" height="480"}](https://archivo.contagioradio.com/sentidos-ambientales-de-la-participacion/)

</figure>
<!-- .post-thumbnail -->  
<!-- .posts-list__item-media -->  
[CENSAT](https://archivo.contagioradio.com/categoria/opinion/censat/) [Opinion](https://archivo.contagioradio.com/categoria/opinion/)  
<!-- .entry-meta -->

<header>
##### [Sentidos ambientales de la participación](https://archivo.contagioradio.com/sentidos-ambientales-de-la-participacion/)

</header>
<!-- .entry-header -->  
[Censat Agua Viva](https://archivo.contagioradio.com/author/censat/)[<time datetime="2019-09-20T10:00:37-05:00">5 min ago</time>](https://archivo.contagioradio.com/2019/09/20/)0<!-- .entry-meta -->

El importante uso de los mecanismos de participación ciudadana en la defensa de los bienes comunes ha intensificado la disputa por el verdadero sentido de la democracia durante los últimos…

<!-- .entry-content -->  
[Cauca](https://archivo.contagioradio.com/tag/cauca/), [Corte Constitucional](https://archivo.contagioradio.com/tag/corte-constitucional/), [extractivismo](https://archivo.contagioradio.com/tag/extractivismo/), [Mercaderes](https://archivo.contagioradio.com/tag/mercaderes/), [Suroeste Antioqueño](https://archivo.contagioradio.com/tag/suroeste-antioqueno/)<!-- .entry-meta -->

<footer>
[Leer más](https://archivo.contagioradio.com/sentidos-ambientales-de-la-participacion/)  

</footer>
<p>
<!-- .entry-footer -->  
<!-- .posts-list__item-content -->  
<!-- .posts-list__item-inner -->  

</article>
<!-- #post-## -->

<article id="post-73939">
<figure>
[![Transitional justice system grants protective measures for ethnic communities](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/09/JEP-MEDIDAS-1.jpg){width="805" height="588"}](https://archivo.contagioradio.com/transitional-justice-system-grants-protective-measures-for-ethnic-communities/)

</figure>
<!-- .post-thumbnail -->  
<!-- .posts-list__item-media -->  
[English](https://archivo.contagioradio.com/categoria/english/)  
<!-- .entry-meta -->

<header>
##### [Transitional justice system grants protective measures for ethnic communities](https://archivo.contagioradio.com/transitional-justice-system-grants-protective-measures-for-ethnic-communities/)

</header>
<!-- .entry-header -->  
[Contagioradio](https://archivo.contagioradio.com/author/ctgadm/)[<time datetime="2019-09-20T09:33:25-05:00">32 min ago</time>](https://archivo.contagioradio.com/2019/09/20/)0<!-- .entry-meta -->

These are the first precautionary measures granted by the Special Peace Jurisdiction that collectively protect ethnic communities and help them strengthen their self-defense mechanisms.

<!-- .entry-content -->  
[Bajo Atrato](https://archivo.contagioradio.com/tag/bajo-atrato/), [Chocó](https://archivo.contagioradio.com/tag/choco/), [Justice and Peace Commission](https://archivo.contagioradio.com/tag/justice-and-peace-commission/), [Special Peace Jurisdiction](https://archivo.contagioradio.com/tag/special-peace-jurisdiction/)<!-- .entry-meta -->

<footer>
[Leer más](https://archivo.contagioradio.com/transitional-justice-system-grants-protective-measures-for-ethnic-communities/)  

</footer>
<p>
<!-- .entry-footer -->  
<!-- .posts-list__item-content -->  
<!-- .posts-list__item-inner -->  

</article>
<!-- #post-## -->

<article id="post-73890">
<figure>
[![Diálogo por la Verdad: un alto a la violencia en Córdoba](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/09/No-Repetición.jpg){width="800" height="533"}](https://archivo.contagioradio.com/dialogo-por-la-verdad-un-alto-a-la-violencia-en-cordoba/)

</figure>
<!-- .post-thumbnail -->  
<!-- .posts-list__item-media -->  
[Actualidad](https://archivo.contagioradio.com/categoria/actualidad/) [Paz](https://archivo.contagioradio.com/categoria/ddhh/paz/)  
<!-- .entry-meta -->

<header>
##### [Diálogo por la Verdad: un alto a la violencia en Córdoba](https://archivo.contagioradio.com/dialogo-por-la-verdad-un-alto-a-la-violencia-en-cordoba/)

</header>
<!-- .entry-header -->  
[Contagioradio](https://archivo.contagioradio.com/author/ctgadm/)[<time datetime="2019-09-19T18:29:43-05:00">16 horas ago</time>](https://archivo.contagioradio.com/2019/09/19/)0<!-- .entry-meta -->

Diferentes actores sociales e institucionales de Córdoba plantearon junto a la Comisión de la Verdad, alternativas para la no repetición de la violencia

<!-- .entry-content -->  
[asesinato de líderes sociales](https://archivo.contagioradio.com/tag/asesinato-de-lideres-sociales/), [comision de la verdad](https://archivo.contagioradio.com/tag/comision-de-la-verdad/), [cordoba](https://archivo.contagioradio.com/tag/cordoba/), [Diálogos para la No Repetición](https://archivo.contagioradio.com/tag/dialogos-para-la-no-repeticion/)<!-- .entry-meta -->

<footer>
[Leer más](https://archivo.contagioradio.com/dialogo-por-la-verdad-un-alto-a-la-violencia-en-cordoba/)  

</footer>
<p>
<!-- .entry-footer -->  
<!-- .posts-list__item-content -->  
<!-- .posts-list__item-inner -->  

</article>
<!-- #post-## -->

<article id="post-73858">
<figure>
[![La memoria histórica del país no se puede crear desde la ambigüedad de Darío Acevedo](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/09/Dario-Acevedo.png){width="810" height="450"}](https://archivo.contagioradio.com/la-memoria-historica-del-pais-no-se-puede-crear-desde-la-ambiguedad-de-dario-acevedo/)

</figure>
<!-- .post-thumbnail -->  
<!-- .posts-list__item-media -->  
[Actualidad](https://archivo.contagioradio.com/categoria/actualidad/) [DDHH](https://archivo.contagioradio.com/categoria/ddhh/)  
<!-- .entry-meta -->

<header>
##### [La memoria histórica del país no se puede crear desde la ambigüedad de Darío Acevedo](https://archivo.contagioradio.com/la-memoria-historica-del-pais-no-se-puede-crear-desde-la-ambiguedad-de-dario-acevedo/)

</header>
<!-- .entry-header -->  
[Contagioradio](https://archivo.contagioradio.com/author/ctgadm/)[<time datetime="2019-09-19T17:13:04-05:00">17 horas ago</time>](https://archivo.contagioradio.com/2019/09/19/)0<!-- .entry-meta -->

El senador Iván Cepeda ha citado a debate de control político al director del Centro Nacional de Memoria Histórica, Dario Acevedo

<!-- .entry-content -->  
[Centro Nacional de Memoria Histórica](https://archivo.contagioradio.com/tag/centro-nacional-de-memoria-historica/), [conflicto armado](https://archivo.contagioradio.com/tag/conflicto-armado/), [Dario Acevedo](https://archivo.contagioradio.com/tag/dario-acevedo/), [Iván Cepeda](https://archivo.contagioradio.com/tag/ivan-cepeda/)<!-- .entry-meta -->

<footer>
[Leer más](https://archivo.contagioradio.com/la-memoria-historica-del-pais-no-se-puede-crear-desde-la-ambiguedad-de-dario-acevedo/)  

</footer>
<p>
<!-- .entry-footer -->  
<!-- .posts-list__item-content -->  
<!-- .posts-list__item-inner -->  

</article>
<!-- #post-## -->  
<!-- .posts-list -->
