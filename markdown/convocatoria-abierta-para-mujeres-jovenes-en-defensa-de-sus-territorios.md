Title: Convocatoria abierta para mujeres jóvenes en defensa de sus territorios
Date: 2020-08-20 17:29
Author: CtgAdm
Category: yoreporto
Slug: convocatoria-abierta-para-mujeres-jovenes-en-defensa-de-sus-territorios
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/08/WhatsApp-Image-2020-08-05-at-5.09.14-PM.jpeg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/08/convocatoria-mujeres.png" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

Con el fin de fortalecer los liderazgos de las mujeres jóvenes, está abierta hasta este 21 de agosto la convocatoria “Mujeres Jóvenes Defendiendo Territorios” de Fondo Lunaria, con el apoyo de la Agencia Catalana de Cooperación para el Desarrollo (ACCD).

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Fondo Lunaria es un fondo feminista que moviliza recursos para apoyar a organizaciones y colectivas de mujeres jóvenes en toda Colombia. Esta vez, la convocatoria, busca apoyar a 10 organizaciones o colectivas de mujeres jóvenes feministas, LBTIQ+, campesinas, afrocolombianas, indígenas, urbanas, con diversidad funcional para desarrollar proyectos sobre defensa del territorio y el medio ambiente.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

La pandemia del COVID – 19 sacó a la luz, el impacto que nuestra forma de vida tiene sobre la naturaleza y el ambiente. En varios países del mundo se han visto cambios positivos como la mejora en la calidad del aire o la recuperación de la naturaleza debido a la disminución de las actividades económicas y de la movilidad de la población. Sin embargo, en otros, como en Colombia, han aflorado problemáticas ya latentes como la actividad extractivista que sigue depredando bienes comunes y el asesinato sistemático de defensoras y defensores del medio ambiente y el territorio.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En este escenario, cobra cada vez más importancia plantear y fortalecer iniciativas ambientales y territoriales lideradas por mujeres. En la convocatoria “Mujeres jóvenes defendiendo territorios” las organizaciones o colectivas deben plantear un proyecto de 6 meses de ejecución y presupuestado en 10 millones de pesos, relacionado con la defensa del territorio y el medio ambiente. No es un requisito ser una organización constituida legalmente y se dará prioridad a aquellas con menos acceso a recursos.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Los temas en los que pueden ir enmarcados las propuestas son soberanía alimentaria, resistencia a daños ambientales y al modelo extractivista, derecho al medio ambiente sano, al agua, a la tierra y al territorio, entre otros. Así mismo, las estrategias pueden ser tan creativas e innovadoras como las participantes quieran, entre las posibles ideas está hacer uso de la comunicación alternativa, llevar a cabo acciones de incidencia e incluso utilizar el arte y del activismo para movilizar esta causa, entre otras.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Daniela Barón, de la colectiva Tejiendo Territorios, colectivo que ha sido apoyado por Fondo Lunaria en la línea de territorios, asegura que este apoyo les permitió expandir su radio de acción y fortalecerse como organización para seguir adelante en momentos en los que no cuentan con recursos externos. Además, lograron conectarse con otras organizaciones y colectivas de su territorio, Suacha, Cundinamarca: “Fue muy bonito encontrar muchas más mujeres y muchas más comunidades con las que podíamos discutir sobre la defensa del territorio y el hábitat. Invitamos a todas las organizaciones a participar en esta nueva convocatoria”.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Las organizaciones o colectivas interesadas pueden consultar los términos de referencia y los formularios de aplicación en esta página <https://fondolunaria.org/convocatorias/#convocatoria_territorios>.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"center","textColor":"cyan-bluish-gray","fontSize":"small"} -->

***La sección Yo Reporto es un espacio de expresión libre en donde se refleja exclusivamente los puntos de vista de los autores y no compromete el pensamiento, la opinión, ni la linea editorial de Contagio Rad*io**

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
