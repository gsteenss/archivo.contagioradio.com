Title: La respuesta del ELN a señalamientos de vínculos con Gobierno de Venezuela
Date: 2019-05-15 16:10
Author: CtgAdm
Category: Nacional, Paz
Tags: ELN, Venezuela
Slug: la-respuesta-del-eln-sobre-su-vinculo-con-venezuela
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/pablo-beltrán-ELN.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Fotograma del video 

En una entrevista a **Pablo Beltrán, comandante del Ejército de Liberación Nacional (ELN),** el ex negociador de paz responde a las preguntas sobre los supuestos vínculos de esa guerrilla con el gobierno de Venezuela. Entre sus respuestas asegura que esa afirmación carece de veracidad y expone algunos ejemplos, uno de ellos es la muerte de altos mandos guerrilleros por acciones del ejército del vecino país y afirma que se ha elaborado una campaña de guerra psicológica en la que tienen de "trompo de poner" al ELN.
