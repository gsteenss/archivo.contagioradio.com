Title: Así está Colombia en materia de protección animal
Date: 2016-10-04 13:08
Category: Animales, Nacional
Tags: Ambiente, camio climático, día mundial de los animales, Maltrato animal, paz
Slug: asi-esta-colombia-en-materia-de-derechos-de-los-animales
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/10/marcha-animales.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Juan Camilo Giorgi 

###### [4 Oct 2016]

Cada 4 de octubre se conmemora el Día Mundial de los Animales. Una fecha que se aprovecha para evidenciar la situación de las especies animales que factores como el **cambio climático, las actividades petroleras y mineras, la ganadería extensiva, el tráfico de especies**, entre otras, que han generado que muchos animales estén en peligro.

Y es que **[una tercera parte de las especies animales](https://archivo.contagioradio.com/la-tercera-parte-de-las-especies-animales-en-el-mundo-se-extinguiran-por-el-cambio-climatico/)del planeta podrían extinguirse **si no se actúa desde ya para frenar el cambio climático impidiendo que la temperatura aumente entre 1,5 y 2 grados, como lo ha advertido la organización World Wildlife Fund for Nature, la WWF.

Según esa organización uno de los impactos más importantes tiene que ver con el deshielo del Ártico, lo que genera, por ejemplo, que las crías de las focas estén en riesgo de morir debido a que el hielo se rompe y mueren ahogados, también los osos polares tendrán cada vez más complicaciones para conseguir su alimento.

En el caso colombiano el cambio climático también representa una grave amenaza, siendo este el país con el segundo lugar en biodiversidad del planeta, como lo explica Susana Vélez, Especialista en Política Forestal y Cambio Climático de WWF.

**“Todos los rincones de Colombia son vulnerables y están expuestos al cambio,** pero tenemos algunos mucho más vulnerables que son muchas veces las regiones más pobres, las que tiene menos capacidad de respuesta ante los fenómenos extremos. El Chocó, la Amazonía, algunas partes de los Andes están más expuestas que otras a estos eventos extremos y tiene menos capacidad de respuesta para atender la emergencias y prepararse a la ocurrencia de estos fenómenos”, lugares en los que se encuentran la gran mayoría de especies, muchas de las cuales solo se encuentran en Colombia.

En el país los animales más amenazados son el armadillo, la guacamaya bandera, el manatí del Caribe, el oso perezoso, la rana dorada, el mono tití del Caquetá, el mono tití cabeza blanca, la rana cornuda del amazonas, el oso hormiguero, el oso de anteojos, entre otros, para **un total son 359 especies silvestres están amenaza. Lo que deja a Colombia **entre los ocho países responsables de la mitad del deterioro del planeta, según un informe de la Unión Internacional para la Conservación de la Naturaleza, Uicn y la ONU.

Por su parte, según el Ministerio de Ambiente y Desarrollo Sostenible, señaló hace algunos meses que  en [Colombia existen 275 especies de aves migratorias](https://archivo.contagioradio.com/en-colombia-10-especies-de-aves-migratorias-estan-en-peligro-de-extincion/), de las cuales **10 están en peligro de extinción clasificadas por la Unión Internacional para la Conservación** de la Naturaleza, UICN. Esto debido al deterioro y desaparición de los hábitats naturales, el cambio climático, la rápida urbanización, la contaminación y el uso insostenible de los espacios naturales.

De las 10.000 especies de aves en la tierra, **Colombia posee 1.905 de las cuales, 90 únicamente son del país, además,** Bogotá es la capital del país con mayor diversidad de aves con 200 especies, gracias a la cantidad de humedales y páramos que hay, que son los ecosistemas más usados por estos animales, por lo que 32,8% de las aves acuáticas son especies migratorias que tienen un área total de ocupación de humedal para el interior del país es de 202.525 Km2.

### Avances en Colombia 

Frente a esa grave situación en la que se encuentran miles de especies en el mundo, en Colombia en los últimos años se ha avanzado en la implementación de medidas que protejan a los animales y además prevengan el maltrato animal.

En enero de este años los defensores de los derechos de los animales celebraron que el presidente Juan Manuel Santos haya firmado [el proyecto de Ley que penaliza el maltrato animal,](https://archivo.contagioradio.com/hasta-60-salarios-minimos-pagara-quien-maltrate-a-un-animal/)una propuesta de organizaciones animalistas que se apoyaron en el representante Juan Carlos Losada y luego en el senador Juan Manuel Galán.

Con la aprobación del proyecto de **Ley 172 se tipifica el delito de maltrato animal, con sanciones de 12 a 36 meses de cárcel**, con unos agravantes, cuando se comete el maltrato frente de menores de edad, cuando el maltrato es de tipo sexual, conocido como zoofilia, o cuando se trate de un funcionario público. Además  genera multas de hasta 60 salarios mínimos mensuales vigentes para quienes maltraten un animal, así mismo abandonar a un animal será una conducta que será castigada con multas entre dos y 20 salarios mínimos.

Por otra parte, por iniciativa del senador del Partido Liberal, Guillermo García Realpe de la mano de la organización animalista Plataforma ALTO, por primera vez la protección animal fue incluida en el Plan Nacional de Desarrollo “Todos por un nuevo país 2014 - 2018”.

De esta manera se incluyó el artículo 252, que busca promover una política pública en defensa de los animales, que debe ser responsabilidad de las entidades territoriales, quienes deberán vigilar, controlar y fomentar el respeto por los animales y con ellos su cuidado e integridad.

Este año en Bogotá, el 25 de septiembre se realizó la IX marcha animalista organizada por  organizaciones ambientalistas y animalistas que teniendo en cuenta la coyuntura, salieron a las calles para que la paz también sea para los animales y la naturaleza. **“Queremos poner sobre la mesa esas víctimas que históricamente han sido invisibilizadas durante la guerra en Colombia. Toda la riqueza en biodiversidad del país se ha visto afectada por el conflicto armado”**, dijo Catalina Reyes, integrante de la Plataforma ALTO.

En el marco de este 4 de octubre, en Bogotá se celebra por cuarto año consecutivo, la **Semana de Bienestar Animal;** en Medellín se realiza el Foro Seres Sintientes y en otras ciudades se hacen varias actividades en el transcurso de estos días.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
