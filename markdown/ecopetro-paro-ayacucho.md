Title: Denuncian hostigamientos de policía y ejército a participantes del paro contra Ecopetrol
Date: 2017-07-09 10:53
Category: yoreporto
Tags: Ayacucho, Ecopetrol, paro
Slug: ecopetro-paro-ayacucho
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/07/IMG_5024.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Derecho del pueblo 

###### 8 Jul 2017 

Se cumplen [once días de paro](https://archivo.contagioradio.com/defensor-de-dd-hh-denuncia-torturas-del-esmad-durante-movilizacion/) y persisten los señalamientos y hostigamientos por parte de la Policía y el Ejército Nacional. Se evidencia la falta de voluntad de parte de la administración de Ecopetrol para darle una salida negociada al conflicto social y laboral. Toda la institucionalidad ha tenido o avalado una respuesta militar contra la comunidad que acude como última instancia al derecho de la protesta social. A continuación nuestro informe en materia de derechos humanos.

Militarización. Se mantiene la presencia policial de efectivos de la estación de La Mata y Aguachica; además constante patrullaje de miembros del BAEEV 3 de Ejercito Nacional, quienes continúan tomando fotografías a quienes protestan pacíficamente e intimidando con sus armas de fuego.

Actos de hostigamientos y difamaciones. El 7 de julio, un sargento de la policía de apellido Pérez se acercó al punto de concentración indicando que se seguía incurriendo en el delito de obstrucción a vías y que si no se dispersaban llamarían de nuevo al ESMAD.

De igual manera, responsabilizó al Equipo Jurídico Pueblos de lo que ocurriera, pues según el agente, es la organización responsable del presunto bloqueo; situación que fue objeto de aclaración por parte de los defensores acompañantes, sin que desistiera el uniformado de su señalamiento.

Este mismo funcionario, manifestó también, que la comunidad estaba deteniendo vehículos para identificar quién se movilizaba en ellos, situación que no es cierta, por el contrario, muchos vecinos del lugar arriban en sus automotores con el propósito de visitar y saludar a las quienes se encuentran en el paro. También indicó que los manifestantes han lanzando piedras u objetos contundentes, a los camiones cisterna que transportan el crudo a la subestación. En ese mismo sentido, uno de los miembros del ejército indicó que la comunidad ha amenazado con quemar los carro-tanques, señalamiento al que han hecho eco miembros del corporativo de seguridad de Ecopetrol.

Tales acusaciones son falsas y generan preocupación en la medida que van orientas a orquestar una judicialización masiva de los participantes del paro contra Ecopetrol.

Despido de trabajadores. En los once días que lleva de paro la comunidad, Ecopetrol ha despedido a cinco personas, esto se evidencia como un hecho de retaliación de la empresa en contra de la comunidad por realizar el paro.

Persecución. DCF de 17 años de edad y miembro de la comunidad de Ayacucho, ha denunciado la constante persecución de la policía en su contra, indica que donde lo ven es perseguido y hostigado por lo que tiene ya temor de salir de su lugar de residencia. “El domingo 2 de julio en horas de la mañana sobre las 7 de la mañana más o menos, me dirigía de Ayacucho a la mata para llevar a mi hermana, y en el punto de mata vieja en la Kazeta la isla, se encontraba la policía, apenas pasamos comenzaron a perseguirnos hasta la mata, dejé a mi hermana y me di la vuelta por la bomba de gasolina para llegar a mata vieja. En horas de la tarde, como a las 2 de la tarde me encontraba en la quebrada El Cuaré, al dirigirme hacia mi casa la policía llega a Ayacucho y me ven y nuevamente me persiguen, me les perdí y guardé la moto… En este día no volví a salir más, solo hasta por la noche cuando salí con mi mamá… pero tengo temor de salir…”

Montajes judiciales. Por fuente de alta confiabilidad se ha conocido que agentes de policía de La Mata, Sijin y miembros del Corporativo de Seguridad de ECOPETROL intentan presentar evidencia falsa para la judicalización del líder comunitario Jorge Eliecer Alonso Vergel. Los uniformados están realizando seguimientos en su contra y se sabe que esperan encontrarlo solo para detenerlo.

Preocupa la pasividad del Personero Municipal de la Gloria, Eliecer Rangel y del Alcalde de este municipio FERMÍN CRUZ, quienes ignoran las peticiones justas de las comunidades y por contrario avalaron el violento atropello del Esmad ocurrido el sábado 1 de julio de 2017. Exigimos el cese de todo acto de hostigamiento contra los pobladores de Ayacucho; además de investigar y castigar a los funcionarios que por acción y omisión incurrieron en actos de brutalidad policial, tortura y tratos crueles e inhumanos contra quienes acuden a la protesta social.

\#Yoreporto por:

MOVIMIENTO DE TRABAJADORXS, CAMPESINXS Y COMUNIDADES DEL CESAR - MTCC

ASOCIACIÓN DE TRABAJADORES DEL ÁREA DE INFLUENCIA - ASOTRAIN

FEDERACIÓN UNITARIA DE TRABAJADORES MINEROS, ENERGÉTICOS, METALÚRGICOS Y QUÍMICOS DE LAS INDUSTRIAS EXTRACTIVAS TRANSPORTADORAS Y SIMILARES DE COLOMBIA - FUNTRAMIEXCO

SINDICATO NACIONAL DE TRABAJADORES DEL SISTEMA AGROALIMENTARIO - SINALTRAINAL

MOVIMIENTO NACIONAL DE VÍCTIMAS DE CORPORACIONES MULTINACIONALES

EQUIPO JURÍDICO PUEBLOS - EJP

CONGRESO DE LOS PUEBLOS

CORPORACIÓN SEMBRAR

FEDERACIÓN AGROMINERA DEL SUR DE BOLIVAR - FEDEAGROMISBOL

UNIÓN SINDICAL OBRERA - USO
