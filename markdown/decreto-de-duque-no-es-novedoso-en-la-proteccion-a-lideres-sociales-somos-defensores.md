Title: Decreto de Duque no es novedoso en la protección a líderes sociales: Somos Defensores
Date: 2018-11-21 13:17
Author: AdminContagio
Category: DDHH, Nacional
Tags: Diana Sánchez, Iván Duque, Líderes y lideresas sociales, ministerio de defensa
Slug: decreto-de-duque-no-es-novedoso-en-la-proteccion-a-lideres-sociales-somos-defensores
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/07/eurodiputados-lideres-sociales1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [21 Nov 2018] 

El nuevo decreto firmado por el presidente Duque, para garantizar la protección a líderes y lideresas sociales **"no tiene nada novedoso"** según la directora de la organización Somos Defensores, Diana Sánchez, quien además señaló que para garantizar la vida de estas personas, Colombia necesita una política integral que profundice en las problemáticas detrás de las agresiones a los defensores.

Sánchez expresó que lo diferente que trae este decreto es que "finalmente este gobierno terminó reaccionando  positivamente en que tenía que afrontar la situación a través de una política pública" para garantizar la vida de las y los líderes del país. Sin embargo, manifestó que se limita a generar una comisión interinstitucional para coordinar de mejor manera la cantidad de escenarios que existen frente al tema de protección a líderes, **hecho que ya se venía gestando durante el gobierno de Santos.**

Además, el decreto es de poco alcance debido a que solo tiene una cobertura de carácter ejecutivo y, para Sánchez, tampoco da un salto cualitativo en la protección a las y los líderes, porque no trasciende de las acciones de defensa física, dejando de lado el **contexto en el que están las personas, las amenazas y la presencia de grupos armados** o intereses políticos, económicos y sociales, para atentar en contra de sus vidas.

### **Necesitamos una Política de Garantías** 

Sánchez afirmó que para avanzar en la protección a líderes y líderesas, en Colombia se necesita una política de Estado que en primer lugar genere investigaciones mucho más amplias frente a quiénes **son los autores intelectuales de las agresiones a los defensores** y resaltó que si bien hay progresos en las acciones de la Fiscalía por esclarecer estos hechos, el universo de casos aún es muy grande. (Le puede interesar: ["Crímenes contra líderes sociales en 2016 y 2017 son superiores a las cifras registradas"](https://archivo.contagioradio.com/crimenes-lideres-sociales/))

De igual forma señaló que debe haber desde la institucionalidad un reconocimiento de la labor de las y los defensores de derechos humanos, "porque de nada sirve un decreto como este, y hacer campañas para la no estigmatización, **si desde altos funcionarios como el Ministro de Defensa se esta señalando y diciendo que detrás de las movilizaciones sociales hay una financiación desde el narcotráfico**".

### **¿Qué pasó con las instancias que ya se tenían para la protección de líderes?** 

Frente a las instancias que ya se habían establecido de interlocución para la protección a líderes y líderesas, Sánchez aseguró que este gobierno no las está dinamizando, como lo es la  Comisión de Garantías de Seguridad, creada a partir de los Acuerdos de Paz con FARC, que tiene como mandato el desmonte de las estructuras paramilitares.

Sobre el decreto 660 firmado en abril de este año, Sánchez afirmó que **no se esta implementando, pese a que tiene mucho más alcance del actualmente firmado** por la ministra del interior Nancy Patricia Gutiérrez y toda una estructura de funcionamiento interinstitucional.

Razón por la cual Sánchez también hace un llamado para que se coordinen esfuerzos y no se saquen más espacios ni agendas desde cada institución, porque se genera más dispersión. (Le puede interesar: "Cuáles son los mecanismos de autoprotección de

<iframe id="audio_30246231" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_30246231_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
