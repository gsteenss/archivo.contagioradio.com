Title: La diversidad del cine llega con el Ciclo Rosa 2018
Date: 2018-06-20 03:30
Author: AdminContagio
Category: 24 Cuadros
Tags: Ciclo rosa, Cine, lgtbi
Slug: ciclo-rosa-2018
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/06/ciclo-rosa-e1529081707245.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Cinemateca Distrital 

###### 15 jun 2018 

Si es amante del cine y los festivales, prepárese porque llega  El Ciclo Rosa 2018, un evento que celebra la diversidad desde la producción audiovisual que ya completa 17 años de existencia y que en esta oportunidad se verá en Bogotá, Medellín y Cali.

El Ciclo estará compuesto por una muestra internacional: Selección Queer de la Berlinale 2018, realizada por Michael Stütz, curador de la sección panorama; una retrospectiva del Festival Asterisco de la Argentina por su productora Violeta Uman y una selección del cine nacional.

Como es habitual el ciclo presentará una exposición fotográfica en el corredor de la Cinemateca Distrital, en esta ocasión los asistentes podaran ver fragmentos de textos, imágenes, memorias, vestuarios, rompecabezas, plantas Kuir y ranas de madera que hacen parte de la ardua investigación y proceso creativo de la película "El susurro del jaguar", ganadora a Mejor Director en la reciente edición FICCI 58 en la competencia oficial cine colombiano.

Esta pieza audiovisual, que dará apertura al ciclo, entra en dialogo con referencias no solo cinematográficas, sino también de otros campos artísticos como la literatura, el performance, el arte sonoro, la música techno y kuir punk, así como de la filosofía política y la biología.

<iframe src="https://www.youtube.com/embed/vEyLx0P6adE" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

El Ciclo Rosa que cuenta con el apoyo de Bogoshorts Sessions y las Embajadas de Argentina y Brasil en Colombia, se realizara del 28 de junio al 10 de julio en la capital, mientras en Medellín del 4 al 9 de julio en el Centro Colombo Americano y en Cali estará en el Museo La Tertulia.

Este año el evento contará con un componente académico que congrega a personajes del sector; así como talleres de fanzine ‘Fanzinografías de una Bogotá Diversa’ y encuentros literarios. Si desea saber más información de la programación, horas, costo de la boletería y demás puede ingresar a la página oficial de la [Cinemateca Distrital](https://www.cinematecadistrital.gov.co/noticias/dos-semanas-de-ciclo-rosa-para-celebrar-la-diversidad).
