Title: En el Bajo Baudó fue asesinado el profesor Rubilio Papelito
Date: 2020-07-05 19:16
Author: CtgAdm
Category: Actualidad, Líderes sociales
Tags: asesinato de líderes sociales, Asesinato de profesores, Bajo Baudó
Slug: en-el-bajo-baudo-fue-asesinado-el-profesor-rubilio-papelito
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/07/Obidio-Papelito.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","fontSize":"small"} -->

Foto: Rubilio Papelito/ @AsociacionOrewa

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El pasado 4 de julio fue asesinado el líder social y educador Rubilio Papelito en la comunidad de Santa María Birrinchao, en el municipio de Bajo Baudó en el Chocó, lugar donde se desempeñaba como profesor. En menos de una semana han sido asesinados al menos 10 líderes sociales y comunitarios.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Rubilio había sido designado docente por la Secretaría de Educación Departamental del Chocó y venía penía prestando su servicio como profesor de aula en la sede principal del Centro Educativo Santa María Birrinchao, en el municipio de Bajo Baudó - Chocó. [(Lea también: En menos de 48 horas se registran 8 asesinatos a líderes sociales)](https://archivo.contagioradio.com/en-menos-de-24-horas-se-registran-ocho-asesinatos-a-lideres-sociales/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Según las versiones preliminares, hombres armados habrían llegado hasta su vivienda y dispararon contra su humanidad, no se conocen los responsables del asesinato, sin embargo se trata de una zona en la que hay presencias de las autodenominadas Autodefensas Gaitanistas de Colombia (AGC) y el ELN.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Hace tan solo ocho días, la [Organización Nacional Indígena de Colombia (ONIC)](https://twitter.com/ONIC_Colombia)denunció el asesinato del gobernador indígena de Agua Clara en el Bajo Chocó, esto luego de que el dirigente hubiese desaparecido el pasado 25 de junio.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

De acuerdo lideres indígenas aún no se realizan los procesos de inspección y levantamiento del cuerpo del profesor Rubilio Papelito, razón por la que piden la presencia de las autoridades en dicha comunidad. [(Le recomendamos leer: Paramilitares asesinan a líder indígena en Medio Baudó)](https://archivo.contagioradio.com/paramilitares-asesinan-delante-de-la-comunidad-a-lider-indigena-ezquivel-manyoma/)

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Noticia en desarrollo....

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
