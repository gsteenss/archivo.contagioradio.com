Title: 350 Internos de todo el país se unen a huelga de hambre por crisis carcelaria
Date: 2016-02-08 16:27
Category: DDHH, Nacional
Tags: crisis carcelaria, FARC, INPEC
Slug: internos-se-unen-a-crisis-carcelaria
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/12/crisis-carcelaria1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Ecos 1360 

<iframe src="http://www.ivoox.com/player_ek_10361227_2_1.html?data=kpWgmJaWdpihhpywj5aWaZS1kp6ah5yncZOhhpywj5WRaZi3jpWah5yncZSpkZC20NnJts%2Fj1JDRx5DYs8XjjMrZjdXFaaSnhqax1ZDXqYzpz8rbjcaPrNbZzczOjcnJb8nVzsffj4qbh4630NPhw8zNs4zGwsnW0ZCRaZi3jpk%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [8 Feb 2016] 

350 prisioneros sociales y políticos de diversas cárceles del país, se unen a la Huelga Nacional y Protesta Pacífica indefinida que ya lleva 20 días, y que inició en el centro penitenciario Eron Picota.

Hombres y mujeres, de cárceles de Caquetá, Cúcuta, Meta, Arauca y Boyacá se unen a la huelga debido a las múltiples violaciones de derechos humanos a las que son sometidos los presos de las cárceles, como lo denuncia el profesor Miguel Ángel Beltrán, preso político recluido en la Picota **“Contrario a darnos una solución, hay un agudizamiento y agravamiento de la situación de los presos políticos, ha habido roces con la guardia, agresiones, no hay valoraciones medicas e incluso hay sectores del INPEC que están interesados en crear un clima de tensión, nos llaman sapos”.**

Así mismo, la huelga se intensifica en rechazo a la muerte de Jhon Jairo Moreno Hernández, Ramón Mallarino en Combita, Jaime Aroca en la cárcel de Villavicencio y Gilberto García en Eron Picota, que han fallecido por cuenta de la falta de atención en salud en las cárceles.

Los presos han decidido enviar una carta a las partes que participan del proceso de paz haciendo un llamado a que se cumplan los acuerdos logrados en la Habana, frente a las brigadas de salud especializadas, y la concentraciones en cárceles y patios determinados.

Así mismo, el pasado sábado, los voceros de los presos se reunieron con el senador Iván Cepeda, para evidenciarle la situación en la que se encuentran. El congresista se comprometió  a buscar una reunión con el Ministerio de Justicia para hablar sobre la salud y la concentración de los presos políticos. Por su parte, **los prisioneros aseguran que buscan generalizar la jornada de desobediencia en todo el país**.

### Una de las denuncias 

El 30 de enero de 2015 la Corte Interamericana de Derechos Humanos, le exigió al Estado colombiano, a proteger y garantizar la vida del prisionero político de las FARC EP, José Ángel Parra, quien sufre de **Leusemia Meloide Crónica y ya se encuentra en etapa terminal.**

Pese al fallo de la CIDH, José, denuncia que **ya cumple 10 días sin recibir sus 600 miligramos de  Imatinib  necesarios para conllevar su estado de salud.** Razón por la que el interno, en este momento tiene fiebre, desaliento y hematomas en todo el cuerpo.

El INPEC le ha respondido a Parra que no existen los recursos financieros para comprar la droga, por lo que José asegura que se trata de una negligencia de la institución para dejarlo morir como ya ha sucedido con otros prisioneros.

El interno, hace un llamado al INPEC, Procuraduría y al Estado en general para que le den su medicamento y la enfermedad no siga agudizándose. Finalmente concluye, que “las cárceles en Colombia no se diferencian de los campos de concentración nazis, **El INPEC es un nido de corrupción que no soluciona nada, su único interés es que no nos volemos de la cárcel**".
