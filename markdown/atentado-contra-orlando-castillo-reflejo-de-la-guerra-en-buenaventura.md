Title: Atentado contra Orlando Castillo, reflejo de la guerra en Buenaventura
Date: 2020-02-24 16:25
Author: CtgAdm
Category: DDHH, Entrevistas
Tags: Atentados contra líderes sociales, buenaventura, Pueblo Naya
Slug: atentado-contra-orlando-castillo-reflejo-de-la-guerra-en-buenaventura
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/02/Orlando-Castillo.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto Buenaventura: Comisión de Justicia y Paz

<!-- /wp:paragraph -->

<!-- wp:html -->

<figure class="wp-block-audio">
<audio controls src="https://co.ivoox.com/es/orlando-castillo-sobre-atentado-su-contra-en_md_48223020_wp_1.mp3&quot;">
</audio>
  

<figcaption>
Entrevista &gt; Orlando Castillo | Líder Social - Defensor de DD.HH - cofundador del Espacio Humanitario Puente Nayero en Buenaventura

</figcaption>
</figure>
<!-- /wp:html -->

<!-- wp:paragraph {"align":"justify"} -->

El pasado 23 de febrero, el líder social Orlando Castillo sufrió un atentado contra su vida en el barrio Kennedy en el municipio de Buenaventura, Valle del Cauca. Orlando es líder social, defensor de DD.HH. coordinador **y cofundador del Espacio Humanitario Puente Nayero en Buenaventura** y ha sido una de las personas que viene denunciando la crisis social y de violencia que vive el puerto más importante de Colombia por el que ingresan el 60% de las importaciones pero que sigue bajo la sombra de los herederos del paramilitarismo, altos niveles de pobreza y una tasa de 18,4% de desempleo según datos del DANE.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Según relata el líder social, quien salió ileso del atentando, los hechos ocurrieron cuando se desplazaba a la oficina de la Corporación Haciendo y Pensando el Pacífico, (CORHAPEP) en el esquema de protección asignado por la UNP, fue interceptado por diez hombres que se movilizaban en cinco motocicletas. [(Lea también: Las enseñanzas de 'don Temis' son símbolo de resistencia en el barrio Isla de la Paz)](https://archivo.contagioradio.com/las-ensenanzas-de-don-temis-son-simbolo-de-resistencia-en-el-barrio-isla-de-la-paz/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Los hombres rodearon el vehículo obligando al conductor a reducir la velocidad, al llegar a un semáforo, uno de los hombres disparó, según el relato los escoltas del líder reaccionaron a tiempo y esquivaron a los atacantes. [(Le recomendamos leer: Atentan contra Carlos Tobar, integrante del Comité del Paro Cívico por Buenaventura)](https://archivo.contagioradio.com/atentan-contra-carlos-tobar-integrante-del-comite-del-paro-civico-por-buenaventura/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

"Hemos venido denunciando los múltiples hechos que se presentan en la comuna 4 que sigue siendo afectada por actores que generan terror y muerte", expresa Orlando Castillo ante la problemática de inseguridad, homicidios, microtráfico, extorsiones y demás delitos que vive el municipio en el que para enero de 2019, **ya la Defensoría había lanzado la Alerta Temprana Número 007-19, de Inminencia sobre la vulneración de los derechos de la población civil del casco urbano de Buenaventura, en particular para líderes comunales.**

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Buenaventura aún no es escuchada

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

En mayo de 2019 en Buenaventura, los homicidios aumentaron un 75% al igual que hurto callejero, además de la continúa disputa de grupos por el manejo del microtráfico y la extorsión en los barrios entre grupos como **La Empresa y el Clan del Golfo, disidencias de las FARC y otros como La Gente del Orden,** presente en las **comunas 10 y 12** y la La Local que hace presencia en las comunas **3, 4 y 9** . [(Le puede interesar: Misión humanitaria constata violación de derechos en zona rural de Buenaventura)](https://archivo.contagioradio.com/mision-humanitaria-constata-violacion-de-derechos-en-zona-rural-de-buenaventura/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Aunque en 2014 durante el gobierno Santos se anunció la llegada de 1.500 uniformados a la ciudad portuaria y en 2018, el presidente Duque se refirió a integrar un contingente de 1.200 soldados con más 35 efectivos especiales para combatir el microtráfico, el incremento en el número de pie de fuerza no equivale a garantías para la vida e integridad de líderes sociales, comunidades y comunas. [(Le puede interesar: Del paro cívico a gobernar Buenaventura, los retos que asume Hugo Vidal)](https://archivo.contagioradio.com/del-paro-civico-a-gobernar-buenaventura-los-retos-que-asume-hugo-vidal/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

**"Creemos que la situación en Buenaventura está como en todo el país, hay que recordar que el Acuerdo de Paz no se han cumplido**" se trata de un exterminio contra líderes y líderes en toda Colombia y "Buenaventura no es la excepción". Frente a lo sucedido también se expresó la [**Comisión Interétnica de la Verdad del Pacífico**](https://twitter.com/VerdadPacifico/status/1231959906443563008)exigiendo la inmediata investigación y captura de quienes serían los autores del atentado e instaron al Ministerio del Interior a cumplir con un plan de protección efectivo para los y las líderes de la región.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

 

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->
