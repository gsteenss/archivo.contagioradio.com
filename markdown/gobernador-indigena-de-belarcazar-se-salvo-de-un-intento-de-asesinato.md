Title: Gobernador Indígena de Belarcázar se salvó de intento de asesinato
Date: 2017-10-13 12:47
Category: DDHH, Nacional
Tags: atentado contra indígenas, autoridades indígenas, Cauca, CRIC, grupos paramilitares, indígenas
Slug: gobernador-indigena-de-belarcazar-se-salvo-de-un-intento-de-asesinato
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/01/Nasa-e1502385039236.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: [Ana Karina Delgado Diaz ](https://www.vice.com/es_mx/article/wd35xq/estuvimos-en-el-juicio-nasa-a-los-guerrilleros-de-las-farc)] 

###### [13 Oct 2017] 

Desde el Consejo Regional Indígena del Cauca, **denunciaron el intento de asesinato** del cual fue víctima en la madrugada de hoy el gobernador y autoridad tradicional del resguardo Belalcázar, Hermes Pete. De acuerdo con la información, Pete recibió tres disparos de los cuales uno impactó en su hombro izquierdo y tuvo que ser atendido en un hospital de la Plata, Huila.

El CRIC indicó que **los hechos ocurrieron a las 2:00 am del 13 de octubre** cuando el gobernador indígena se trasladaba hacia Popayán por el sector de la Mesa de Belalcázar. “Cuando dos hombres en moto le hicieron el pare, Pete hizo caso omiso y los hombres le propiciaron tres disparos, uno de ellos le impactó en el brazo”. (Le puede interesar: ["ESMAD retiene a 15 comuneros del resguardo Kokonuco, Cauca: CRIC"](https://archivo.contagioradio.com/esmad-retiene-a-5-comuneros-del-resguardo-kokonuko-cauca-cric/))

Afirmaron que la autoridad tradicional **ha sido víctima de múltiples amenazas desde el 2016** por parte del grupo paramilitar “Águilas Negras” quienes le han dado un ultimátum para que salga del territorio. Argumentaron que Hermes Pete se ha dedicado a defender el territorio ancestral “y esto lo ha llevado a sufrir estos señalamientos”.

### **La Autoridad indígena ha solicitado medidas de protección** 

De acuerdo con José Hildo Pete, ex consejero del Consejo Regional Indígena del Cauca, **ya se había denunciado ante las autoridades del país la situación de riesgo** de Hermes Pete. Sin embargo, “la respuesta a la solicitud de protección fue negativa”.

Para las comunidades indígenas, es preocupante que, en un escenario de paz, se presenten estos casos que **ponen en riesgo la vida de los indígenas** y de los colombianos en general. Pete indicó que “si no fuera porque hay un escenario de paz, podríamos decir que la guerra continúa”. (Le puede interesar: ["Se agrava situación en el Cauca por asesinato de etno educador"](https://archivo.contagioradio.com/se-agrava-situacion-en-el-cauca-por-asesinato-de-etno-educador/))

Pete manifestó que **la presencia de grupos armados en el territorio es constante**, “hay presencia de paramilitares y disidentes de la guerrilla”. Sin embargo, los indígenas aún no han podido establecer qué grupo es el que está ocasionando este tipo de hechos. Ante esta situación, le han exigido al Gobierno que garantice el derecho a la vida y brinde la seguridad necesaria para la supervivencia de los grupos indígenas en el territorio.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 

<div class="osd-sms-wrapper">

</div>
