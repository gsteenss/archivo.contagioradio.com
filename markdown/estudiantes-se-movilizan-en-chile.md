Title: 90 mil estudiantes se movilizan contra reforma educativa en Chile
Date: 2017-04-11 15:29
Category: El mundo, Otra Mirada
Tags: Chile, educacion, Movilización, universitarios
Slug: estudiantes-se-movilizan-en-chile
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/04/chile.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### Foto:Ecuavisa 

##### 11 Abr 2017

Cerca de **90 mil personas**, en su mayoría estudiantes universitarios, marcharon desde las 11 de la mañana hora de Chile, para manifestar **su descontento con la reforma que el gobierno pretende hacer a la educación superior en el país austral**.

La movilización que recorrió las principales calles del centro de Santiago partiendo de la Plaza Italia por la Alameda hasta la calle Echaurren, esta sustentada por el reclamo que hacen organizaciones como la Confederación de Estudiantes Chilenos a los **parlamentarios para que no aprueben la polémica medida**.

La Reforma propuesta por el gobierno de Michel Bachelet en su segundo mandato, propone **varias medidas que, según los estudiantes han tomado un camino equivocado que cambiaría el rumbo del modelo de educación en el país**.

Una serie de manifestaciones paralelas se realizaron en diversas ciudades de la ciudad, todas con una gran convocatoria, en la que se presentaron según informes policiales **algunos incidentes aislados**. Le puede interesar: [Mujeres exigen legalización del aborto en Chile](https://archivo.contagioradio.com/mujeres-exigen-legalizacion-del-aborto-en-chile/).

Con críticas desde diferentes sectores políticos de movimientos como Revolución Democrática y el Frente amplio,  y organizaciones estudiantiles, hoy se definirá en votación por parte de la Cámara de Diputados si pasa o no la reforma a la educación superior.

El presidente de la FECh, Daniel Andrade, manifestó que "si los estudiantes están en las calles es porque nuestro movimiento sigue vigente. Los parlamentarios deben escuchar" enviando un mensaje para "Que el Gobierno deje su sordera: No habrá reforma sin nuestras propuestas".

 
