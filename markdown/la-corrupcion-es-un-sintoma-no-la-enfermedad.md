Title: La corrupción es un síntoma, no la enfermedad
Date: 2018-08-27 08:00
Author: JOHAN MENDOZA TORRES
Category: Johan Mendoza, Opinion
Tags: colombia, Consulta anticorrupción, corrupción
Slug: la-corrupcion-es-un-sintoma-no-la-enfermedad
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/08/corrupcion.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: mpresa.prensa.com 

#### **Por: [Johan Mendoza Torres](https://archivo.contagioradio.com/johan-mendoza-torres/)** 

###### 27 Ago 2018 

[Actualmente, la cultura de la imagen, el marketing político, los slogans, las frases genéricas sin contenido definido, se hallan adaptadas al vaivén de emociones tan tremendo en que se ha constituido ese clima simbólico-político de la sociedad.]

[La actitud burlona que emergía cuando se les preguntaba a las reinas de belleza “]*[¿Qué le regalaría a la humanidad?”]*[ y al unísono contestaban “]*[la paz mundial”,]*[bien puede ser una actitud en la que pueda degenerar el tema de la corrupción o la frase “lucharemos contra la corrupción”, roguemos para que no se constituya en un cliché.]

[Por supuesto no hay demerito en la consulta, de hecho, ésta sorprendió a muchos, (en los que me incluyo) por su abultada masa de votantes. A las 12 del medio día del 26 de agosto, nadie daba un peso por la consulta, Uribe celebraba su poder de convocatoria twitero atacando la consulta, y algunos simplemente hacían cuentas alegres o algo resentidas frente a las posibilidades de resultados.]

[Al final algo más de 11 millones de votos dejaron boquiabiertos hasta a los “nimierdistas” más radicales. La votación fue abultada, la apoyó una parte de la derecha, la apoyó el autodenominado “centro” que avanza de la mano del populismo de Daniel Samper, la apoyó la izquierda así muchos le hicieran el feo a las fotos con Rodrigo Londoño, la apoyaron los movimientos no alineados a ningún partido, apoyaron los que apoyaron a Claudia López, los que la odian (por simple moralismo barato o por diferencias ideológicas), los de aquí y los de allá, al final esa vaina tomó fuerza sin caudillo alguno, sin maquinaria, y rugió tan fuerte como un grito desesperado. De eso, no hay ninguna duda.]

[¿Quién no detesta la corrupción? Imagino que todos… todos los que se colan, los que hacen chancucos con el contador, los que le piden rebaja a los campesinos pero pagan sin decir ni “mu” en el supermercado de cadena, los que incumplen y aparte de todo se molestan, los que cuentan los actos o fantasías traquetas de su vida mientras denigran de “los políticos” o de “la política”, los que denigran de la política mientras deliran con llegar a ser lo lobos de Wall Street, los que son unos “chachos” cuando la hacen, pero se “emputan” cuando se las hacen. ¡¡claro contra la corrupción todos!!! en esa misma bolsa, incluso los reclamantes de tierras, los líderes sociales, los familiares de asesinados que buscan justicia y no la encuentran porque no tienen cómo pagar un abogado con influencias, a la gente que la desdicha a pesar de ser tan grande le luce diminuta frente a su humanidad, frente a su humildad, claro… ¿quién no detesta la corrupción? Le podría preguntar a cualquiera, del estrato que sea, y la sola pregunta tan neutra, tan indefinida, conduciría a la misma respuesta, constituyendo una obviedad peligrosa para el rumbo político que debería tomar.  ]

[Al finalizar la jornada, unos se emberracaron, otros maldijeron al país, y se tomaron la cuestión tan personal que se olvidaron de que lo político va más allá de cualquier opinión individual. Otros fueron más optimistas y quedaron con la esperanza de que en lo simbólico-político, algo está cambiando, lentamente, pero está cambiando.]

[No obstante, como hecho curioso, Duque, presidente y uribista que debe más favores políticos que Peñalosa, se abanderó el triunfo y anunció su paquete de leyes contra la corrupción. Claudia López, una destacada antiuribista y promotora de la consulta, también habló y ordenó con su característica beligerancia discursiva y con gran razón que no se podían ignorar 11 millones de votos. Algunos movimientos sociales convocaron a marchar hacia Bogotá como un plan de contingencia, y así, así, así, todos aplaudieron los comicios, pues la consulta anticorrupción, dejó clara una cosa:]**en Colombia hay un sentimiento muy grande de rechazo contra la corrupción, pero al parecer es más grande el sentimiento, que la comprensión sobre qué es o quién o qué provoca la corrupción.  **

[Cuando una consulta anti corrupción es celebrada por todos los sectores, incluso aquellos que están empantanados con la corrupción, simplemente algo no está claro.]

[Todos dicen “lucharemos contra los corruptos” … ¿contra los corruptos o contra la corrupción?  “¡ambos!” Dirán. Sí, pero al corrupto lo encarcelas, ¿y a la corrupción? ¿llamamos al padre Chucho para que la espante de nuestras almas? Primera cosa: es necesario distinguir entre corruptos y corrupción. Pues en el juego populista las están equiparando y eso es un error… o en el peor de los casos una estrategia.  ]

[Si hubiesen dado certificados electorales, de seguro se alcanza el umbral, de eso no hay duda, es decir, en el fondo un pensamiento corrupto como “solo voto por el medio día libre” hubiera dado estatus aprobatorio a la consulta anticorrupción. Segunda cosa: para muchos el problema realmente no es la corrupción, sino la represión de un deseo: ganarse la vida fácil, como los corruptos. De allí que subliman ese deseo vociferando “¡¡malditos corruptos!!!” pero piensan solapadamente “jum yo que no haría si me dieran ese papayaso”.]

[¿Qué puede ocurrir cuando una masa de 11 millones está más emocionada que consciente? A cualquiera que le diga lo que quiere escuchar, lo abraza, lo aplaude. Vota si si si si si si si, pero no tiene ni idea que eso era para que las preguntas pasaran a serios debates en el congreso. Vota si si si si si si si si si, porque el reggaetón estaba sabroso y porque lo hizo Daniel Samper, (pues si a Wally se le hubiera ocurrido, esa mandana de congresistas no le hubieran caminado a vestirse así). Gente que no se pone de acuerdo para votar un plebiscito, que no le gusta consensar nada con nadie, que quiere imponer su punto de vista bajo la ficción de “lo tuyo vale lo mío también” (así una de las dos partes constituya una mentira o una barbarie), gente que no tienen ni idea de los temas pero opina lo que sea (así sea una burrada) amparado en que la “libertad de expresión” le ofrezca criterio por sí sola, gente que no tiene facultades democráticas ni para debatir en la asamblea del conjunto residencial, sí, incluso gente así puede salir fervorosa a votar contra la corrupción…. ¿para qué?]

[¿Qué puede ocurrir cuando la gente odia algo pero no sabe quién o qué lo provoca más allá de “los políticos hijueputas”? ¿será cierto eso? ¿son “los políticos”? Pero en un sentido muy fidedigno al concepto, los líderes sociales asesinados también eran políticos. ¿ah, pero esos no eran “hijueputas”? ¿Entonces?... Ya empezó hace rato el zaperoco en twiter entre el centro democrático, los youtubers y los verdes, todos apoyaron la consulta, unos con más criterio ético que otros por supuesto, pero ahí están. Ya comenzaron las columnas de opinión como esta y luego entraremos en el debate sobre si Navarro, Claudia o Pachito para la alcaldía de Bogotá, mientras el “lucharemos contra la corrupción” se transforma en el nuevo “queremos la paz mundial”.]

[No se puede cerrar tan fácil un tema de estos… pero visto lo visto, lo que sí es una premisa para la continuación del debate es que con “la corrupción” constituida como ese gran demonio responsable de todo lo que ocurre, pasará lo mismo que pasó con la guerrilla. Al final nos daremos cuenta de que no era el problema del país, sino tan solo un síntoma de una enfermedad más grave… ¿pero cuál?.]

#### [**Leer más columnas de Johan Mendoza Torres**](https://archivo.contagioradio.com/johan-mendoza-torres/)

------------------------------------------------------------------------

###### Las opiniones expresadas en este artículo son de exclusiva responsabilidad del autor y no necesariamente representan la opinión de Contagio Radio.
