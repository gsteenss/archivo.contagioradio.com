Title: San Romero: una lección para las iglesias en vísperas de elecciones parlamentarias  en Colombia
Date: 2018-03-07 12:11
Category: Abilio, Opinion
Tags: cristianismo., Iglesia, Monseñor Romero, Teologia, teologia de la liberación
Slug: san-romero-una-leccion-para-las-iglesias-en-visperas-de-elecciones-parlamentarias-en-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/03/romero-santo.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Cancillería del Ecuador 

#### Por **[Abilio Peña ](https://archivo.contagioradio.com/abilio-pena-buendia/) - [~~@~~Abiliopena](https://twitter.com/Abiliopena)

###### 7  Mar 2018 

[Como un bálsamo en medio de la utilización del cristianismo en la política electoral colombiana, se conoció la noticia de [un decreto del Papa Francisco que reconoce un milagro de Monseñor Oscar Arnulfo Romero](https://www.aciprensa.com/noticias/la-iglesia-proclamara-santo-a-mons-oscar-romero-30778) . Ya había sido beatificado y reconocido como mártir de la iglesia, modelo de Seguimiento de Jesús,  luego de haber sido asesinado en San Salvador por grupos paramilitares dirigidos por las fuerzas militares de El Salvador.]

[Esta noticia  ha generado gran movilización en la comunidad ecuménica de América Latina y del mundo, por el profundo significado que para la vida de los pueblos tiene un hombre que en el dolor de la gente, pudo encontrar los fundamentos de su fe cristiana y obró en consecuencia, siendo capaz de denunciar con nombres propios a los responsables de la pobreza, de la corrupción, de las violaciones a los derechos humanos: dirigentes políticos y económicos, fuerzas militares, Gobierno de los Estados Unidos.]

[En Colombia los políticos tradicionales, los que se negaron a aceptar la propuesta  fruto de los acuerdos gobierno FARC de la comisión de expertos que recomendó una reforma política que no fue discutida, siquiera en el congreso, han hecho cuentas del número de fieles   de las iglesias cristianas que pueden sumar votos para sus causas y han hecho acuerdos políticos en la lógica tradicional donde es quien tiene dinero, sin importar cómo lo consiguió, el que termina ganando las elecciones.  También han sucedido los pactos entre algunos líderes de iglesias que han convertido sus feligresías en fortines políticos, construyendo movimientos en busca de curules en el congreso de la república.]

[Monseñor Romero entendió la política como búsqueda del bien común,  como la denuncia de la violencia institucionalizada, como la defensa de los derechos humanos, como  la opción preferencial, no excluyente, por los empobrecidos e invitó a una toma de partido desde el  Evangelio. Esta es su propia voz:]

1.  [«A la Iglesia no le interesan los intereses políticos o económicos, sino en cuanto tienen relación con el ser humano, para hacerlo más humano y para no hacerlo idólatra del dinero, idólatra del poder; o desde el poder, hacerlos opresores; o desde el dinero, hacer marginados. Lo que interesa a la Iglesia es que estos bienes que Dios ha puesto en las manos de los seres humanos -la política, la materia, el dinero, los bienes- sirvan para que el ser humano realice su vocación» (Homilía de Monseñor Romero, 17 de julio de 1977)]
2.  [ «La dimensión política de la fe no es otra cosa que la respuesta de la Iglesia a las exigencias del mundo real socio-político en que vive la Iglesia. Se trata de la opción por los pobres, de encarnarse en su mundo, de anunciarles una buena noticia, de darles una esperanza, de animarles a una praxis liberadora, de defender su causa y de participar en su destino. Esta opción de la Iglesia por los pobres es la que explica la dimensión política de su fe en sus raíces y rasgos más fundamentales. Porque ha optado por los pobres reales y no ficticios, porque ha optado por los realmente oprimidos y reprimidos, la Iglesia vive en el mundo de lo político y se realiza como Iglesia también a través de lo político. No puede ser de otra manera si es que, como Jesús, se dirige a los pobres»... (Mons. Romero, ]*[La dimensión política de la fe desde la opción por los pobres]*[,  discurso en Lovaina al asumir como doctor ]*[honoris causa 2 de febrero de 1980]*[. ][[www.servicioskoinonia.org/romero]](http://www.servicioskoinonia.org/romero)[)]

Esta dimensión política de la fe ligada a los intereses de las mayorías empobrecidas, está muy lejos de los discursos de  los movimientos políticos que se dicen inspirados en la fe cristiana al contrario, han hecho causa común con los sectores de poder más ligados con la corrupción, las violaciones a los derechos humanos, la perversión de la política a favor del sostenimiento del status quo.

[Las palabras de Romero, por las que fue consecuente hasta la muerte, siguen resonando en la conciencia de cristianas y cristianos, máxime hoy cuando su santidad será reconocida por la misma iglesia católica, que en algunos  momentos como institución, ha pactado, también con los poderes que han reproducido tanta miseria y crimen en nuestro país. Como Romero, muchas y muchos miembros de la iglesia, han sabido, también, levantar su voz profética para denuncia perversión de la política, corriendo no pocas veces,  la misma suerte de Romero.]

[  En  lógica de  San Romero,   dentro de la contienda electoral actual de Colombia,  parecen más cristianos los movimientos políticos minoritarios, no tradicionales,  que afirman su derecho a la participación electoral en medio del aislamiento mediático, las campañas de desprestigio,  la escases de recursos, que quienes a todas voces se proclaman cristianos.]

#### **[Leer más columnas de Abilio Peña](https://archivo.contagioradio.com/abilio-pena-buendia/)**

------------------------------------------------------------------------

###### Las opiniones expresadas en este artículo son de exclusiva responsabilidad del autor y no necesariamente representan la opinión de Contagio Radio
