Title: Piden cancelar audiencia contra Santrich en la Corte Suprema de Justicia
Date: 2019-07-08 14:33
Author: CtgAdm
Category: Entrevistas, Judicial
Tags: Corte Suprema de Justicia, JEP, santrich
Slug: piden-cancelar-audiencia-contra-santrich-en-la-corte-suprema-de-justicia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/Corte-Suprema-de-Justicia-y-Santrich.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: W radio  
] 

**La defensa de Jesús Santrich solicitó que la diligencia que se citó para el próximo 9 de julio en la Corte Suprema de Justicia no se realice**; debido a que este proceso debe esperar a que la sección de apelaciones de la Jurisdicción Especial para la Paz (JEP), tome su definición en segunda instancia conforme a la garantía de no extradición que se decretó por la sección de revisión. Una vez se efectúe este trámite la Corte tendría que continuar con el caso.

El **abogado Gustavo Gallardo** afirmó que también se encuentran a la espera del fallo de la Corte Constitucional, por el conflicto de competencias que planteo la defensa de Santrich frente a que sería la JEP y no la Corte Suprema de Justicia, la que debería continuar con el caso. (Le puede interesar: ["Actuamos administrando justicia impacial, objetiva y tansparente: Altas Cortes y JEP"](https://archivo.contagioradio.com/justicia-imparcial-objetiva-transparente-altas-cortes-jep/))

Gallardo informó que este trámite se hizo hace más de 20 días y que esperan que el pronunciamiento de la Corte Constitucional se de pronto, ya que el Magistrado a cargo de caso **ya habría radicado su ponencia el pasado 2 de julio**. (Le puede interesar:["El Fiscal buscará la forma para que Santrich permanezca en la cárcel: Gustavo Gallardo"](https://archivo.contagioradio.com/fiscal-santrich-carcel/))

**"Lo que está sucediendo en este caso de Santrich, es que se están llevado dos procesos: uno en la Corte y otro en la JEP, por los mismos hechos con las mismas pruebas**. Lo que quiere decir que hay una violación clara al "*non bis* in idem", es decir que nadie puede ser investigado dos veces por el mismo hecho" señaló Gallardo y afirmó que en todo caso, la defensa cuenta con todas las pruebas para demostrar la inocencia de Hernández Solarte.

### **¿Qué se sabe del paradero de Santrich? ** 

El abogado defensor manifestó que la familia de Santrich continúa sosteniendo la tesis de un posible secuestro, hecho que tampoco ha sido descartado por las autoridades Colombianas. Sin embargo hasta el momento no se tiene mayor información sobre esta situación. (Le puede interesar: ["La desaparición de Santrich no puede ser un impedimento para buscar la paz"](https://archivo.contagioradio.com/la-desaparicion-de-santrich-no-puede-ser-un-impedimento-para-buscar-la-paz/))

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>  
<iframe id="audio_38153308" frameborder="0" allowfullscreen scrolling="no" height="200" style="border:1px solid #EEE; box-sizing:border-box; width:100%;" src="https://co.ivoox.com/es/player_ej_38153308_4_1.html?c1=ff6600"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
