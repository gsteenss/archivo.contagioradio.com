Title: 15 heridos deja el uso excesivo de la fuerza por parte del ESMAD
Date: 2015-11-09 17:36
Category: Educación, Nacional
Tags: ESMAD, Ministerio de Educación, Movilización estudiantil, Movimiento estudiantil, Protesta social, represión estatal en colombia, Universidad Pedagógica Nacional de Colombia
Slug: comunidad-estudiantil-de-la-upn-rechaza-uso-excesivo-de-la-fuerza-por-parte-del-esmad
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/11/Universitarios-rechazan-el-uso-excesivo-por-parte-del-ESMAD-e-inconformidad-de-la-educación.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### foto:[emancipacionobrera.blogspot.com]

<iframe src="http://www.ivoox.com/player_ek_9330913_2_1.html?data=mpigkp6Vd46ZmKiakpWJd6KkkZKSmaiRdI6ZmKiakpKJe6ShkZKSmaiRh9Dh1tPWxsbIb8bn1drRy8bSuMrgjMnSjdHFb7bEr5Dfx8jMpdvVjNrg0ZDJvMTZ1M7j0ZDIqY6ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Bibiana Tellez, representante de pregrado al consejo académico] 

###### [9 nov 2015]

A través de un comunicado, el Consejo Académico de la Universidad Pedagógica Nacional rechaza el uso excesivo de la fuerza por parte del Escuadrón Móvil Anti Disturbios (ESMAD), luego que el pasado 5 de noviembre, atacaran a un grupo de estudiantes que adelantaban una movilización pacífica y de caracter pedagógico en la calle 72 en la ciudad de Bogotá.

La manifestación estudiantil se produjo luego de que el Ministerio de Educación rompiera con uno de los puntos acordados con la comunidad estudiantil con el que  se aseguraba que no se realizaría ningún tipo de borrador de una resolución que no fuera pactada y que pudiera **afectar la autonomía universitaria, según explica** Bibiana Téllez representante del Concejo Académico.

Fue por eso que los estudiantes decidieron salir a las calles a realizar una protesta pacífica en la que además se le explicaba a las personas porqué se marchaba y cómo se estaba vulnerando el derecho a la educación con las medidas del Ministerio.

Durante la marcha pacifica el ESMAD llega a disolver la movilización usando gases lacrimógenos hacia a estudiantes y profesores, pero además hacia niños, niñas, mujeres embarazadas e incluso personas en condición de discapacidad, dejando un total de 15 personas heridas, denuncia Tellez.

Así mismo, agentes de la fuerza pública intentaron ingresar a las instalaciones de institución universitaria, y bloqueó el ingreso y salida de la universidad por las puertas de la calle 72 y 73, vulnerando la protesta social, la autonomía universitaria, la libre expresión y el derecho a la educación, según señala el comunicado.

Ante esta situación, la representante al Consejo Académico, asegura que el movimiento estudiantil "**seguirá en la lucha y estarán dispuestos a llegar hasta las últimas consecuencias para lograr una educación de calidad".**

[![comunicado consejo académico UPN](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/11/comunicado-consejo-académico-UPN.jpg){.aligncenter .size-full .wp-image-16922 width="600" height="760"}](https://archivo.contagioradio.com/comunidad-estudiantil-de-la-upn-rechaza-uso-excesivo-de-la-fuerza-por-parte-del-esmad/comunicado-consejo-academico-upn/)
