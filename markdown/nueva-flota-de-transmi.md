Title: Frenar el cambio climático en Bogotá no será posible con la nueva flota de Transmilenio
Date: 2018-11-06 15:40
Author: AdminContagio
Category: Ambiente, Nacional
Tags: Bogotá, Euro 5, Peñalosa, Transmilenio
Slug: nueva-flota-de-transmi
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/08/csm_AgenciaNoticias-20170614-03_05_9e474c1549.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Agencia de Noticias UN] 

###### [06 Nov 2018] 

**Con el cambio de flota de Transmilenio se apaga la posibilidad de que Bogotá tenga un avance significativo en la toma de acciones en contra del cambio climático,** pero por lo menos se mejorará un poco la calidad de aire para los capitalinos, así lo aseguro Hugo Saenz, integrante de la organización Respira Bogotá.

"Desafortunadamente la voluntad de la Administración no estuvo a la altura de lo que la ciudad necesitaba y el resultado final es que seguirán operando una gran cantidad de buses diésel. Aún así se logró que en varios de los patios operen buses a gas natural, pero **dejamos aplazada para los próximos años la migración de cambio de sistema**" afirmó Saenz.

Para el vocero de Respira Bogotá, el cambio de flota pudo haberse hecho de una mejor manera, debido a que de los 1.133 vehículos que ingresaran a Transmilenio**, el 59% de ellos son Euro 5, que funcionan con** [**diésel**]** y han sido vetados en países europeos y Estados Unidos** debido a su nivel de contaminación, mientras que el resto de los buses funcionan a gas y podrían dar un paso en la mejora de la calidad el aire de la ciudad, debido a que estos vehículos emiten 90% menos de material partículado que los que funcionan con diésel.

Aún así, esta flota operará en la ciudad durante 10 años, un periodo de tiempo en el que si no se toman medidas urgentes, el cambio climático será irreversible y que de acuerdo con Saenz, además incumplirá los pactos ambientales a los que se ha acogido Colombia, como los Acuerdos de París, para frenar el calentamiento global y la crisis ambiental. (Le puede interesar: ["La cercanía entre contaminación y Transmilenio"](https://archivo.contagioradio.com/contaminacion-transmilenio/))

Frente a esta situación Saenz afirmó que hay dos grandes retos en términos de salud, el primero de ellos es que se garantice que los articulados Euro 5 cuenten con los filtros especiales que reducen el material partículado y el segundo es que la ciudadanía **pueda hacer parte de un proceso de fiscalización que garantice que los buses** [**diésel**]** operen de la forma correcta. **

<iframe id="audio_29874248" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_29874248_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]

<div class="osd-sms-wrapper">

</div>
