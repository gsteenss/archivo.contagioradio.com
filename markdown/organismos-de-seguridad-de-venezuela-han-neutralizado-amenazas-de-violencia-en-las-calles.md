Title: Organismos de seguridad de Venezuela han neutralizado amenazas de violencia en las calles
Date: 2016-09-02 17:35
Category: El mundo, Política
Tags: oposición en Venezuela, presidente Maduro, Venezuela
Slug: organismos-de-seguridad-de-venezuela-han-neutralizado-amenazas-de-violencia-en-las-calles
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/09/Marcha-Venezuela.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:eldia.com] 

###### [2 Sept de 2016] 

[**En paz y sin mayores alteraciones al orden público transcurrieron el día de ayer las movilizaciones en Venezuela**. Pese a que ambas marchas, tanto la del oficialismo como la de la oposición tuvieron diferentes rutas y una masiva participación, los intentos de violencia fueron rápidamente controlados.]

[La jornada se **transformó en un escenario para medir fuerzas entre la oposición que salió a las calles a exigir el referendo revocatorio** en contra del presidente Maduro y [el Chavismo que expresó su defensa por la democracia](https://archivo.contagioradio.com/jornada-intensa-de-movilizacion-en-venezuela-de-sectores-oficiales-y-de-oposicion/). Igualmente, el presidente Maduro informó que dos días antes de la marcha **92 personas fueron capturadas en un presunto campamento paramilitar, cerca del Palacio de Miraflores**. ]

[Para Hernán Vargas, miembro del ALBA Movimientos, es importante evidenciar la correlación de fuerzas que se dio durante el día de ayer y resaltó el hecho de que después de 14 años **la derecha haya convocado una movilización masiva, solo posible en el marco de la democracia**, de igual forma manifestó que sin la fuerza del Chavismo, posiblemente el plan de desestabilización habría sido llevado a cabo.]

[Por otro lado, Vargas considera que lo que vendría para la oposición es un debate interno frente a las próximas elecciones, **ya que sus máximos líderes estarían compitiendo por la candidatura** y  el programa general de este sector, por ahora solo expone como máxima reivindicación el refrendo revocatorio y no se refiere a otras propuestas. ]

[De igual forma afirmó que para el Chavismo también se viene una etapa de análisis en **busca de recomponer las fuerzas internas** y evaluar las estrategias para no ser una minoría electoral y disputar en las urnas la continuidad del proyecto Bolivariano en Venezuela. ]

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
