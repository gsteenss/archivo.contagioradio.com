Title: Alemania bloquea la prorroga de la ayuda financiera para Grecia
Date: 2015-02-19 20:25
Author: CtgAdm
Category: Economía, El mundo
Tags: Alemania bloque ayuda Grecia, Alemania impide prorroga crédito financiero Grecia, Ángela Merkel, Rescate Grecia, Syriza
Slug: alemania-bloquea-la-prorroga-de-la-ayuda-financiera-para-grecia
Status: published

###### Foto: Lahaine.org 

**El 28 de febrero expira la ayuda** de los socios europeos a modo de rescate financiero que hasta hora estaba recibiendo el país heleno.

Debido a la imposibilidad de pagar la deuda y de reflotar la economía **el ministro de finanzas Yanis Varoufakis ha realizado la petición formal al Eurogrupo de prorrogar las ayudas durante seis meses más**, renegociando las condiciones del mismo. A lo que con celeridad Alemania ha respondido rechazando las nuevas condiciones, bloqueando así la prorroga.

La nueva administración griega ha realizado un exahustivo documento donde detalla las **nuevas condiciones requeridas para negociar el rescate**, entre las que incluirían el **rechazo** a casi todas las **medidas de austeridad** impuestas anteriormente, como los recortes laborales y sociales.

También ha sido entregado conjuntamente al documento, un informe donde explica como va a pagar escrupulosamente a sus acreedores o donde valora la actual ayuda calificándola de camino a seguir. Entre las **concesiones más importantes está la de colaborar con el FMI o la de permitir al BCE colocar su deuda en bancos como el de Frankfurt, a cambio de liquidez.**

Después de la entrega del documento **Alemania ha rechazado dichas condiciones por no ser las acordadas el lunes entre los miembros del Eurogrupo**. A dichas declaraciones a respondido el ministro de finanzas que ha prometido evitar "acciones fiscales unilaterales".
