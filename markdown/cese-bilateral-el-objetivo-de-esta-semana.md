Title: Cese bilateral es la garantía para la mesa de La Habana: Carlos Velandia
Date: 2018-06-14 10:09
Category: Nacional, Política
Tags: Elecciones presidenciales, ELN, La Habana, proceso de paz
Slug: cese-bilateral-el-objetivo-de-esta-semana
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/05/eln.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Portal ELN] 

###### [14 Jun 2018] 

Lograr un cese bilateral y un Acuerdo Marco, son los objetivos que tienen Roy Barreras, senador del Partido Liberal y María Ángela Holguin, canciller de Colombia, para garantizar que la Mesa de Diálogos continúe después de la segunda vuelta electoral presidencial, sin que el resultado, sea Petro o Duque, **sean un obstáculo para el desarrollo de la misma.**

De acuerdo con el gestor de paz Carlos Velandia, de cumplirse esta meta, el siguiente paso que debe darse es gestar un acercamiento con el que será el próximo presidente de Colombia, **y su equipo de trabajo, para dar a conocer el estado actual de la mesa y los avances que pueden darse**.

“Se necesita crear un clima mucho más estable, duradero y prolongado de cese bilateral, **donde las dos partes estén comprometidas en mantener un alto al fuego**, dar alivios humanitarios y que, con ese contexto, el nuevo presidente de la República asuma los destinos del país” afirmó Velandia.

Frente al Acuerdo Marco, **el gestor de paz manifestó que es una posibilidad de darle continuidad a todo el trabajo que se ha desarrollado hasta la fecha**, y que además, servirá como hoja de ruta para el nuevo presidente. Asimismo, afirmó que ese Acuerdo debe ir mucho más allá de la agenda que se pactó el 30 de marzo de 2016 y debe plantear de forma más contundente la dejación de armas y el propósito para ponerle punto final a la confrontación armada entre ambas partes.

### **El proceso de paz entre el nuevo Gobierno y el ELN** 

Una de las preocupaciones más fuertes que se había gestado sobre la continuidad de la mesa radicaba en la candidatura de Iván Duque, que de llegar a la presidencia había manifestado su intención de no seguir con el proceso. Sin embargo, para Velandia el contexto actual es otro. (Le puede interesar: ["Futuro de la negociación con el ELN dependerá del nuevo presidente: Víctor de Currea"](https://archivo.contagioradio.com/futuro-de-la-negociacion-con-el-eln-dependera-del-nuevo-presidente-victor-de-currea/))

“**Una cosa son las preocupaciones y el lenguaje de un candidato a la presidencia y otra cosa son los problemas** y las preocupaciones de un jefe de Estado” expresó el anilista y aseguró que, sea quien sea el próximo presidente de Colombia, le dará continuidad al proceso y le pondrá su propio ritmo.

<iframe id="audio_26538517" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_26538517_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
