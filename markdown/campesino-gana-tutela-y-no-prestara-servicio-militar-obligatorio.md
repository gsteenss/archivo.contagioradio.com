Title: Campesino gana tutela y no prestará servicio militar obligatorio
Date: 2015-04-01 18:46
Author: CtgAdm
Category: Nacional, Resistencias
Tags: Cauca, conciencia, ejercito, libreta militar, nicolas correa, objetores, servicio militar
Slug: campesino-gana-tutela-y-no-prestara-servicio-militar-obligatorio
Status: published

<div>

######  

##### <iframe src="http://www.ivoox.com/player_ek_4290878_2_1.html?data=lZemkp2bfI6ZmKiakpqJd6Kpl5KSmaiRdo6ZmKiakpKJe6ShkZKSmaiRh8Lh0crgy9PTb8jVz8aY1trYqc3VjN6Y0NSPtNPZ1NnO1Iqnd4a1kpDgx9farcTd0JDay9HNuMLmjNSah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>[ ] 

##### [Nicolás Correa, campesino] 

El viernes 27 de marzo, **Nicolás Correa** recibió respuesta a la tutela interpuesta contra el Ejercito, que solicitaba respetar sus derechos fundamentales, el debido proceso y el **descuartelamiento inmediato para no prestar el servicio militar obligatorio**.

</div>

Nicolás es un joven campesino se Sucre, Cauca, que fue reclutado forzadamente por el Ejercito en el terminal de transporte de Pitalito, Huila, en noviembre del 2014. Desde el momento en que fue reclutado, Nicolás Correa manifestó que las pruebas médicas lo habían declarado no-apto para prestar el **Servicio Militar**, y que contaba con una unión marital de hecho, causales para no ser reclutado. Pero sus argumentos fueron ignorados sistemáticamente por **Batallón de Infantería N° 27 Magdalena**, adscrito a la Brigada IX del Ejército Nacional, donde permaneció por 5 meses.

En marzo del 2015, una vez terminado el proceso de entrenamiento habitual del Ejercito, Nicolás recibió un permiso de 15 días para visitar a su familia.Tan pronto volvió a su casa, en Sucre, buscó ayuda e interpuso la acción de tutela que le resultó favorable.

A pesar de no haber recibido aún su libreta militar, los trámites para adquirla están en proceso. **Nicolás Correa manifiesta estar feliz, "trabajando en la finquita"**, que ya se estaba echando a perder por su ausencia.
