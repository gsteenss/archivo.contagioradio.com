Title: Una vez más suspenden audiencia contra Santiago Uribe
Date: 2019-05-27 18:19
Author: CtgAdm
Category: Judicial, Política
Tags: 12 apostoles, Santiago Uribe Vélez
Slug: una-vez-mas-suspenden-audiencia-contra-santiago-uribe
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/03/Santiago-Uribe.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo] 

La audiencia, que debió realizarse en principio el pasado 4 de abril, fue suspendida una vez más debido a que la  defensa de **Santiago Uribe Vélez, acusado de financiar y hacer parte del grupo paramilitar Los 12 Apóstoles y el homicidio de Camilo Barrientos,** solicitó realizar nuevas pruebas para determinar el estado de salud mental del testigo Eunicio Pineda.  Tal solicitud quedó en manos del Tribunal Superior de Antioquia.

El abogado Daniel Prado señaló que tal objeción ya había sido aceptada por el Juez por lo que, en esa medida las diligencias no se pueden seguir adelantando hasta tanto no se termine el procedimiento de esa objeción sobre el dictamen, "una vez acabe ese procedimiento se harán las alegaciones finales", explico.

**"Para nosotros es una maniobra dilatoria para ganar tiempo esperando que hayan cambios en la administración de la justicia"** señaló el abogado.

Según el apoderado, esperan que la administración de justicia comprenda la importancia y delicadeza del proceso sobre los hechos por los que se acusa a Santiago Uribe para que se le de un trámite urgente. [(Le puede interesar: Defensa de Santiago Uribe es más mediática que jurídica: Daniel Prado)](https://archivo.contagioradio.com/defensa-de-santiago-uribe-es-mas-mediatica-que-juridica-daniel-prado/)

<iframe id="audio_36376767" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_36376767_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
