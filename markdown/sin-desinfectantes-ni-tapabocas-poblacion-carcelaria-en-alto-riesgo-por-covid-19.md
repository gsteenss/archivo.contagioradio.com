Title: Sin desinfectantes ni tapabocas. Población carcelaria en alto riesgo por COVID 19
Date: 2020-03-17 09:13
Author: CtgAdm
Category: Actualidad, DDHH
Tags: #CrisisHumanitaria, #MovimientoCarcelario, carcel, carceles, Covid-19
Slug: sin-desinfectantes-ni-tapabocas-poblacion-carcelaria-en-alto-riesgo-por-covid-19
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/6.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

El pasado 16 de marzo, el movimiento carcelario, alertó que se encuentran sin desinfectantes ni tapabocas, razón por la cual se encuentran en alto riesgo de contraer COVID 19**. A ello se suma que algunos patios aún estarían recibiendo visitas, aumentando el** peligro, pese a las ordenes del presidente Iván Duque de restringir el ingreso de personas a estos lugares.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Según el comunicado, el pasado domingo 15 de marzo, "el patio de los delincuentes de cuello blanco en la cárcel Picota de Bogotá" tuvo visitas familiares sin ninguna restricción. Mientras que "de las otras medidas no se ha visto nada, **las áreas de sanidad internas siguen colapsadas por la tuberculosis, gripa, diarrea,** que afecta a la población reclusa". (Le puede interesar: ["Prisionero político es víctima de torturas"](https://archivo.contagioradio.com/prisionero-politico-es-victima-de-torturas/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Las medidas sobre las prisiones, fueron anunciadas por el gobierno nacional el 12 de marzo*, **“**Lo primero que vamos a hacer, por un periodo prudencial, es suspender visitas en los próximos días hasta adaptar protocolos que permitan tener todas las medidas de salud, a partir de las cuales personas con síntomas no puedan hacer visitas y donde garanticemos que los reclusos también tengan medidas de protección”, *

<!-- /wp:paragraph -->

<!-- wp:heading -->

No tenemos nada para protegernos
--------------------------------

<!-- /wp:heading -->

<!-- wp:paragraph -->

Los reclusos también señalaron que esta situación de propagación del virus, se potencia con la ya crisis humanitaria que viven los centros penitenciarios que desde 1.998, que ha generado **un 48% de hacinamiento y constantes violaciones a derechos humanos.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

"No tenemos acceso a tapabocas, jabón antibactetial, jabón en polvo, creolina, ni cloro para el lavado de patios, pasillos, celdas y calabozos, es decir, la capacidad de respuesta del Estado escasamente alcanzó para suspender nuestros derechos a la visita" afirmaron los prisioneros. (Le puede interesar: ["El Movimiento Nacional Carcelario exige que se tomen medidas ante el COVID-19"](https://www.justiciaypazcolombia.com/comunicado-el-movimiento-nacional-carcelario-exige-que-se-tomen-medidas-ante-el-covid-19-y-el-riesgo-sobre-la-poblacion-privada-de-la-libertad/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En ese sentido, el movimiento Carcelario el pide al presidente Duque que decrete la emergencia carcelaria y reconozca la crisis humanitaria y social en el sistema penitenciario colombiano; adopté un plan inmediato de descongestión carcelaria y Adicione un fondo especial para el INPEC a fin de garantizar la higiene y salubridad de las cárceles y Penitenciarias.

<!-- /wp:paragraph -->
