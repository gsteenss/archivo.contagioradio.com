Title: Tras 21 años Estado deberá reparar a las víctimas de la Esperanza en Antioquia
Date: 2017-12-01 16:03
Category: DDHH, Nacional
Tags: CIDH, Desplazamiento forzado, Estado Colombiano, la esperanza, víctimas, víctimas de crímenes de Estado
Slug: tras-21-anos-estado-debera-reparar-a-las-victimas-de-la-esperanza-en-antioquia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/12/vereda-esperanza.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: CEJIL] 

###### [01 Dic 2017] 

Las víctimas del desplazamiento forzado, que ocurrió en 1996 en la vereda La Esperanza en Carmen de Viboral en el departamento de Antioquia, celebraron la decisión de la Corte Interamericana de Derechos Humanos (CIDH), quien **condenó al Estado colombiano por estos hechos**. Los paramilitares en conjunto con las fuerzas del Estado, desaparecieron a 19 personas entre el 21 de junio y el 27 de diciembre de ese año.

De acuerdo con la organización Internacional Brigadas de Paz (PBI), “las Autodefensas Campesinas del Magdalena Medio **desaparecieron a 19 personas**, incluyendo tres menores de edad señaladas de ser simpatizantes o colaboradoras de grupos al margen de la ley que operaban en la zona”.

Señalan que “el exjefe paramilitar Ramón Isaza Arango, alias ‘El Viejo’, confesó que estos hechos fueron ordenados por el fallecido **general Alfonso Manosalva Flórez**, comandante, en la época, de la Brigada IV del Ejército, con sede en Medellín y perpetrados por dos personas también fallecidas: su hijo, Omar Isaza alias ‘Teniente’ y el mayor David Hernández”. (Le puede interesar: ["CIDH admite demanda por tortura a 7 mujeres en Putumayo"](https://archivo.contagioradio.com/cidh-admite-demanda-por-tortura-a-7-mujeres-en-putumayo/))

### **Víctimas llevan 20 años exigiendo verdad y reparación** 

Flor Gallego, lideresa de la vereda la Esperanza, indicó que al proceso de reparación se acogieron familias de 12 de los desaparecidos y entre ellos, **Andrés Suárez Cordero** que hoy tiene 20 años y al momento de los hechos fue abandonado cuando desaparecieron a sus padres. Por esto, las víctimas fueron a San José de Costa Rica en 2016 para pedirle a la CIDH que, por medio de esta, el Estado cumpla con su labor de reparación colectiva.

En esta ocasión, la Corte, **le ordenó al Estado continuar con las investigaciones y los procesos judiciales** a la vez que deberá realizar un acto público de reconocimiento de responsabilidad y también deberá garantizar que se brinde tratamiento médico y psicológico a las víctimas de la Esperanza. Esto, lo llevan exigiendo las víctimas como medida de reparación sin que hubiesen recibido ayuda alguna. Indicó que “el Estado sólo nos hizo acompañamiento psicosocial cuando nos citaron a justicia y paz”.

Flor Gallego recordó que en la vereda la Esperanza han realizado diferentes actos de conmemoración cada que se cumple un aniversario de los hechos. Con el fallo, **se reunirá la comunidad** de la vereda para socializar la decisión y dijo que “seguiremos caminando para que el Estado nos responda con la verdad, esto es lo más importante del proceso”. (Le puede interesar: ["La impunidad en crímenes de Estado es del 90%: Alejandra Gaviria"](https://archivo.contagioradio.com/la-impunidad-en-crimenes-de-estado-es-del-90-alejandra-gaviria/))

Finalmente, las víctimas indicaron que el fallo de la CIDH es un paso más para conseguir **“esa verdad tan anhelada que es un derecho de nosotros”**. Manifestó que “la desaparición forzada duele, es de los crímenes más fuertes de lesa humanidad, eso lo hemos tenido que vivir y ha sido una lucha constante para saber qué pasó con nuestros familiares”.

<iframe id="audio_22403968" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_22403968_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio]{.s1}](http://bit.ly/1ICYhVU)[.]{.s1}
