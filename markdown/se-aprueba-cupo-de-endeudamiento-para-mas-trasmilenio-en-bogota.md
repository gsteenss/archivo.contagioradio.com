Title: Se aprueba cupo de endeudamiento para más Transmilenio en Bogotá
Date: 2017-09-07 13:16
Category: Nacional, Política
Tags: Alcaldía de Bogotá, Peñalosa
Slug: se-aprueba-cupo-de-endeudamiento-para-mas-trasmilenio-en-bogota
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/09/secretaria-ambiental.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Secretaria Distrital de Ambiente] 

###### [07 Sept 2017] 

La Comisión de Hacienda del Consejo de Bogotá, aprobó con 11 votos a favor y tres en contra, la ampliación del cupo de endeudamiento a **6.9 billones de pesos para realizar la construcción de Transmilenio por la carrera séptima**, obra a la que los ciudadanos se han opuesto, manifestado que podría aumentar la inseguridad deterioraría las tres localidades por las que atraviesa.

Esta troncal se construiría desde la calle 32 hasta la calle 200, por las localidades de Teusaquillo, Chapinero y Usaquén, de acuerdo con la **Alcaldía Distrital la obra estaría lista en el año 2019**. De igual forma el Peñalosa, alcalde de Bogotá, expreso que también se priorizaría la construcción de colegios públicos y la recuperación del Bronx.

### **Distrito tampoco presentó estudios para Transmilenio por la séptima** 

Sin embargo, la mayor crítica que realizaron concejales de bancadas como las del Polo democrático y Progresistas, fue **la falta de presentación de estudios** por parte de la Alcaldía de Bogotá, Transmilenio y la Secretaría de Hacienda, para sustentar la importancia, financiación y viabilidad de esta obra por una de las calles más importantes de la capital. (Le puede interesar: ["No hay excusa para seguir dilatando la revocatoria: Unidos Revocaremos a Peñalosa"](https://archivo.contagioradio.com/no-hay-excusa-para-dilatar-revocatoria/))

Uno de los cabildantes que votó en contra de esta propuesta fue Celio Nieves, del Polo Democrático, quien a través de su cuenta de twitter señaló **“sin haber utilizado cupo de endeudamiento 2016, Peñalosa pide \$2.4 billones más para proyectos sin estudios**”. Xinia Navarro, del Polo y Gloria Stella Diaz, del partido MIRA, también votaron en contra.

### **Los concejales que votaron a favor** 

De otro lado los concejales que votaron a favor del cupo de endeudamiento fueron: Yefer Vega, Roberto Hinestrosa y Julio Acosta, de Cambio Radical; Jorge Torres y Hosman Martínez de la Alianza Verde; Ángela Garzón y Daniel Palacios del Centro Democrático; David Belles y Patricia Mosquera del Partido de la U; Germán García del Partido Liberal y Roger Carrillo del partido Conservador. (Le puede interesar: ["Revocatoria a Enrique Peñalosa podría realizarse en Noviembre"](https://archivo.contagioradio.com/revocatoria-a-enrique-penalosa-podria-realizarse-en-noviembre/))

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
