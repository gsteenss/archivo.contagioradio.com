Title: 430 familias serían desalojadas para explotar oro en Miraflores, Risaralda
Date: 2017-07-06 17:07
Category: Ambiente, Nacional
Tags: explotación minera, Miraflores
Slug: 430-familias-serian-desalojadas-para-explotar-mercurio-en-miraflores-quindio
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/07/mineria-de-mercurio-.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Tejido Territorial] 

###### [06 Jul 2017] 

430 familias que viven de la minería artesanal, serían desalojadas de sus hogares en Miraflores, Risaralda, debido a que, la **Alcaldía del municipio emitió un diagnóstico de riesgo de deslizamiento para justificar el proceso de desalojo de la vereda**  para permitir que la empresa Miraflores Compañía Minera y filial de la Anglogold Ashanti realice la explotación de oro a gran escala  en este lugar.

De acuerdo con Esau Mora, presidente de la Asociación de Mineros Tradicionales y Artesanales de Miraflores, el martes 4 de Julio, la Alcaldía presentó un diagnóstico de riesgo sin justificación técnica evidenciando su intención de hacer efectivo el desalojo del cerro próximamente, aludiendo a que las personas vivían en zona de riesgo, sin embargo, los campesinos señalaron que en realidad es para permitir **la ejecución de un título minero a cargo de la Empresa Miraflores Compañía Minera, que tiene otros títulos en el territorio del municipio con la AngloGold Ashanti.**

Aunque el alcalde aseguró que la explotación está aprobada hasta el 2018, le manifesto a los mineros que el uso que realizaban del mercurio afectaba al ambiente, sin embargo solo ay 270 mineros explotando en la cantera que produce **15 toneladas de oro al día.** Con la explotación de la operadora la explotación será aún mayor y producirá un impacto ambiental más nocivo. (Le puede interesar:["Comunidades de Quinchía en Risaralda le dicen No a la megaminería"](https://archivo.contagioradio.com/protesta-mineria-quinchia/))

“Si la multinacional entra a trabajar nos han hablado que van a sacar 1.400 toneladas al día” señaló Esau Mora y agregó que ya se han realizado perforaciones en el **territorio, sin reconocer la tradición minera de las personas**. (Le puede interesar:["77 familias Embera Karambá serían desplazadas por la Anglogold Ashanti"](https://archivo.contagioradio.com/77-familias-embera-karamba-serian-desplazadas-por-la-anglo-gold-ashanti/))

En total serían 2900 personas las que saldrían afectadas de efectuarse el desalojo que tendría el objetivo expulsar a los habitantes del sector e impedir la explotación por parte de mineros tradicionales y artesanales. Frente a estos hechos la comunidad expresó que ya han activado todos los mecanismos con **la Defensoría del Pueblo y esperan que se respeten sus derechos a la vivienda y el trabajo**.

<iframe id="audio_19666850" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_19666850_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
