Title: "Impunidad en casos de desaparición forzada es casi del 100%": ASFADDES
Date: 2017-08-30 13:22
Category: DDHH, Nacional
Tags: desaparecidos, despariciones forzadas, Día Internacional del detenido desaparecido, Unidad de Busqueda
Slug: impunidad-en-casos-de-desaparicion-forzada-es-casi-del-100-asfaddes
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/08/desaparecidos-dia-e1504117350695.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Movice Antioquia] 

###### [30 Ago 2017] 

En medio de la conmemoración del día Internacional de los Desaparecidos, familiares y organizaciones sociales en Colombia indicaron que **Colombia es el país con mayor índice de impunidad en desaparición forzada** y la Fiscalía General de la Nación es la institución que menos resultados ha dado en materia de investigaciones. Según Medicina Legal, desde 1938 hasta 2017 han desaparecido más de 124 mil personas en el país.

Según Gloria Gómez, directora de la Asociación de Familiares de Detenidos Desaparecidos (ASFADDES), en Colombia la conmemoración de este día está reconocida por la ley obligando a entidades a hacer actividades para recordar a estas personas en centros educativos. Ella afirmó que **“ha habido un avance significativo en los procesos de divulgación a una sociedad que continúa siendo indiferente** ante los casos de desaparecidos”.

Sin embargo, Gómez indicó que, en materia de avances en la búsqueda, en las investigaciones y los resultados de las mismas, “Colombia sigue siendo el país con mayores índices de impunidad”. Dijo además que en **casos de desaparición forzada la impunidad es casi del 100%** en la medida que “las instituciones del Estado han fallado con el cumplimiento de sus obligaciones”. (Le puede interesar:["Las recomendaciones de las víctimas para la Unidad de Búsqueda de Desaparecidos"](https://archivo.contagioradio.com/las-recomendaciones-de-las-victimas-para-el-funcionamiento-eficaz-de-la-uniddad-de-busqueda-de-desparecidos/))

Gómez fue enfática en manifestar que, desde el Estado, **no ha habido una atención adecuada para las personas que están buscando a sus seres queridos** y no ha habido una responsabilidad del Estado en cuanto a que “ha cometido actos de omisión por no buscar y salvar las vidas de aquellos que se encuentran desaparecidos, encontrarlos vivos es siempre la mayor esperanza”.

**Mecanismos de búsqueda avanzan a paso lento**

Los familiares de las personas desaparecidas, lograron **por medio de la asociación y el trabajo constante que se creara la Unidad de Búsqueda de Personas Desaparecidas** en el marco de los acuerdos de paz entre el Gobierno Nacional y las FARC.  Sin embargo, han manifestado que la implementación de esta Unidad ocurre a paso lento pues aún están esperando que haya un reconocimiento jurídico por parte de la Corte Constitucional para su entrada en funcionamiento. (Le puede interesar: ["El 99% de los casos de desaparición forzada están en la impunidad"](https://archivo.contagioradio.com/99-de-los-casos-de-desaparicion-forzada-estan-en-la-impunidad/))

Gómez indicó que **“la Unidad de Búsqueda de Desaparecidos necesita rodearse de capacidades y recursos para iniciar la labor de búsqueda”**. Según ella, “debe haber un fortalecimiento del banco de perfiles genéticos que es muy débil, no hay la capacidad suficiente en el universo de los desaparecidos y Medicina Legal necesita fortalecerse para continuar siendo un apoyo en los procesos de identificación de personas”.

Gómez además estableció que la **Fiscalía General de la Nación es la entidad que menos resultados ha dado en materia de investigaciones** de casos de desapariciones forzadas, “insistimos en que la Fiscalía debe cumplir con sus deberes para que en Colombia no sigan ocurriendo desapariciones, ellos tienen que lograr establecer cómo buscar a los desparecidos de más de 30 años y a la par las desapariciones recientes”.

**Familiares no reciben la atención sicosocial necesaria**

La directora de ASFADDES indicó que la desaparición forzada **es un flagelo que destruye el núcleo familiar** y social del desparecido por lo que en la familia el impacto de la desaparición de un ser querido es devastador. Sin embargo, las familias han manifestado que la labor de acompañamiento de la Unidad de Víctimas es de poca calidad y el Ministerio de Salud debería crear políticas dedicadas a la atención de las víctimas.

Dijo también que los **familiares han tenido que crear procesos de acompañamiento mutuo** “para transitar en medio del dolor, de la búsqueda y de la resistencia”. Para ella, la organización entre familias ha resultado “terapéutica para continuar ante la indiferencia y la falta de avances que hay en el país para encontrar a nuestros seres queridos”.

<iframe id="audio_20608139" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_20608139_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)

######  
