Title: Fallo Histórico: Corte Internacional condena al Estado por Falsos Positivos
Date: 2019-01-17 09:34
Author: AdminContagio
Category: DDHH, Nacional
Tags: Corte Interamericana de Derechos Humanos, Ejecuciones Extrajudiciales, falsos positivos
Slug: fallo-historico-corte-internacional-condena-al-estado-por-falsos-positivos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/08/militares-jep-falsos-positivos.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo Particular] 

###### [16 Ene 2019] 

La Corte Interamericana de Derechos Humanos condenó al Estado Colombiano por la ejecución extrajudicial de seis jóvenes presentados falsamente por el Ejército como “bajas en combate”. Este fallo es el primero de un tribunal internacional en el que **se reconoce que existió un patrón de comisión de los mal llamados “falsos positivos”**.

Los jóvenes víctimas de estos hechos fueron Wilfredo Quiñones Bárcenas, José Gregorio Romero Reyes, Albeiro Ramírez Jorge, Gustavo Giraldo, Elio Gelves Carrillo y Carlos Arturo Uva, asesinados en los departamentos de Arauca, Santander y Casanare entre los años 1992 y 1997.

Estos casos habían sido presentados de forma independiente ante la Comisión Interamericana, posteriormente fueron acumulados en la fase de fondo antes de llegar a la Corte, hecho que permitió analizar el fenómeno de las ejecuciones extrajudiciales a lo largo de la decana de los noventa y que la Corte emitiera un fallo histórico frente a “una política que impulsaba o al menos toleraba, la comisión de ejecuciones sin procesos”.

Razón por la cual la Corte ordenó al Estado colombiano la investigación, juzgamiento y sanción de los responsables, brindar atención adecuada en materia de salud a las víctimas, y la realización de un acto público de reconocimiento de responsabilidad. (Le puede interesar:["Queremos saber quién ordenó los falsos positivos: Madres de Soacha"](https://archivo.contagioradio.com/queremos-saber-quien-ordeno-los-mal-llamados-falsos-positivos-madres-de-suacha/))

### **Los patrones detrás de las ejecuciones extrajudiciales** 

De acuerdo con la investigación se pudo establecer la existencia de una práctica que se agudizó y generalizó a partir del año 2002, con base en que ““se incentivó con diversos beneficios la eliminación de supuestos subversivos, lo que desató una nueva serie de ejecuciones sin proceso sobre población civil indefensa, **con la perversa finalidad de obtener los beneficios ofrecidos valiéndose de este letal fraude**”.

En ese sentido la Corte condenó al Estado colombiano por violaciones a los derechos a la vida, a la integridad personal y a la libertad personal. Además, el Tribunal encontró falencias y obstáculos de las investigaciones en el marco de jurisdicción penal militar.

De igual forma, la Corte encontró que el Estado vulneró los artículos 1, 6 y 8 de la Convención Interamericana para **Prevenir y Sancionar la Tortura en los casos de Wilfredo Quiñónez Bárcenas, José Gregorio Romero Reyes y Albeiro Ramírez Jorge,** al no investigar posibles actos de tortura en contra de estos.

Asimismo, la Corte condenó al Estado por violar el derecho a la honra y dignidad de las familias de las víctimas de ejecuciones extrajudiciales, debido a que en algunos casos señalaron que las personas asesinadas eran integrantes de grupos subversivos generando estigmas sociales sobre ellos y daños sicosociales.

###### Reciba toda la información de Contagio Radio en [[su correo]
