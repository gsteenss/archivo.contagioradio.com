Title: Fumigaciones indiscriminadas con glifosato queman 150 hectáreas de cultivos campesinos
Date: 2015-04-22 19:01
Author: CtgAdm
Category: Comunidad, Nacional
Tags: 2013, Anorí, antinarcóticos, Antioquia, Asesina a dos campesinos en la ZRC del Putumayo, Asociación Campesina del Norte de Antioquia, campesinos, fumigaciones indiscriminadas, Glifosato
Slug: fumigaciones-indiscriminadas-con-glifosato-queman-150-hectareas-de-cultivos-campesinos
Status: published

##### Foto: [americaeconomia.com]

<iframe src="http://www.ivoox.com/player_ek_4394135_2_1.html?data=lZimlpaXeY6ZmKiakpyJd6KomZKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRitbhyszOxc7TssbnjM7bxs7Xp9Pdzs7bw8nFt4zX0NOYydHNqtDnwtncjdvZqc3qxtOYw9GPso6ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Víctor Tobón, Asociación Campesina del Norte de Antioquia] 

La Asociación Campesina del Norte de Antioquia ASCNA, denuncia que el pasado 20 de abril **una avioneta y cuatro helicópteros de antinarcóticos realizaron fumigaciones indiscriminadas** en el municipio de Anorí de Antioquia, razón por la que en las veredas Tacamocho y los Trozos terminaron afectados **150 hectáreas de cultivos campesinos.**

De acuerdo a Víctor Tobón, quien hace parte de la Asociación Campesina del Norte de Antioquia, **la fumigación con glifosato quema el suelo y los cultivos** y además aumentan el PH del subsuelo, lo que generó un retroceso en los procesos que viene adelantando las comunidades campesinas respecto a la soberanía alimentaria.

Lo más “irónico’’ según expresa Tobón, es que  “l**a mata de coca es la que menos sufre con el glifosato,** en cambio, la yuca, el plátano, caña y arroz son los cultivos más perjudicados’’, perdidas que significan un gran impacto económico y social para la población.

El vocero de la ASCNA, afirma que la comunidad desconoce el por qué se reactivaron estas fumigaciones, ya que desde el paro agrario del 2013, se habían detenido, conociendo que hay avances en las posibilidades de sustitución de este tipo de fumigaciones.

Estos actos, sumado a las constantes agresiones de las Fuerzas militares, hace pensar a los campesinos, que **la fuerza pública intenta desplazar a los pobladores.**  “No sabemos qué intereses hay detrás de esto, pero nos quieren sacar bajo la intimidación militar o el hambre generada por la fumigación con glifosato”, denuncia Víctor Tobón.

Por el momento, la comunidad campesina realiza la recolección de evidencia fotográfica y muestreos del subsuelo como pruebas que pide la Dirección de Antinarcóticos para elaborar la denuncia, sin embargo, asegura Tobón, esos trámites nunca sirven.
