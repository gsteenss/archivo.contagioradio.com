Title: Habitantes de San Martín, Cesar denuncian contaminación en el agua tras acciones de fracking
Date: 2018-02-13 12:08
Category: Ambiente, Nacional
Tags: cesar, conoco phillips, fracking, pozos petroleros, san martin
Slug: habitantes-de-san-martin-cesar-denuncian-contaminacion-en-el-agua-tras-acciones-de-fracking
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/04/fracking-e1460586646301.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Burgos Digital] 

###### [13 Feb 2018] 

Los habitantes del municipio de San Martín en el Cesar han denunciado que, tras el inicio de los **trabajos de fracking por parte de la empresa Conoco Phillips**, el agua que abastece a las viviendas está llegando con aceite, afectando la salud de las personas. Allí buscarán que se realicen pruebas de laboratorio para ver qué relación tiene esto con la actividad petrolera.

Esta denuncia la hicieron visible los habitantes por medio de un video en donde se observa una **sustancia espesa** que contamina el agua potable de la cual hacen uso las personas que viven específicamente en la vereda Pita Limón. Han dicho que esa situación no se había presentado antes y que puede estar relacionada con las actividades de exploración para hacer fracking en un pozo que queda a 2 km de la vereda.

### **Agua está saliendo con una “nata” de aceite** 

De acuerdo con Carlos Andrés Santiago, líder contra el fracking de ese municipio, el agua contaminada está saliendo en la vereda Pita Limón que hace parte del área de influencia del proyecto petrolero que desarrolla la multinacional Conoco Phillips. Esta vereda es el casco urbano más cercano al **pozo Pico Plata 1**, “que es el pozo piloto de fracking en Colombia donde ya se desarrolló la primera fase de exploración de yacimientos no convencionales”.

Específicamente los habitantes han dicho que “el agua que sale del pozo de donde se abastecen de agua empezó a salir con una nata o **mancha de aceite** que es parecida a la que se produce cuando hay un derrame de hidrocarburos”. Por esto, han lanzado una advertencia a las autoridades ambientales para que tomen cartas en el asunto. (Le puede interesar:["Conoco Phillips no cumple requisitos de licencia ambiental para fracking en San Martín"](https://archivo.contagioradio.com/conoco-phillips-no-cumple-requisitos-de-licencia-ambiental-para-fracking-en-san-martin/))

Santiago reiteró que la fase de exploración en ese pozo “se hizo sin las licencias ambientales que se requieren para los yacimientos no convencionales y se hizo de manera ilegal”. Por esto, en varias ocasiones le han pedido al Gobierno Nacional que se aplique el **principio de precaución** “porque hay evidencia científica que muestra contaminaciones al agua y a la salud pública por estas prácticas”.

### **Cordatec está buscando hacer análisis de laboratorio de las muestras de agua** 

Para clarificar lo que está sucediendo con el agua en ese municipio, la Corporación Defensora del Agua, Territorio y Ecosistemas (Cordatec), está contactando laboratorios que puedan llegar al municipio para **tomar la muestra** “con todos los estándares y la cadena de custodia necesaria” para que sean enviadas a un laboratorio del IDEAM.

Santiago afirmó que “sería irresponsable en este momento afirmar con certeza la **relación de la contaminación con el fracking**” en la medida en que aún no tiene las conclusiones pertinentes. Sin embargo, están seguros de que hay una comunidad que “tiene un pozo de agua profundo, que nunca había tenido ese problema y misteriosamente cuando termina la fase uno de exploración de fracking empiezan a experimentar agua contaminada”.

### **Gobierno Nacional no ha tenido contacto con las comunidades** 

A esta problemática se suma el hecho de que las instituciones gubernamentales no han realizado **ningún acercamiento con las comunidades afectadas**. Santiago manifestó que Corpocesar ha sido el único que se ha manifestado y van a enviar un representante al municipio para evaluar lo que está sucediendo. (Le puede interesar:["Gases y aturdidoras del ESMAD contra pobladores de San Martín"](https://archivo.contagioradio.com/gases-aturdidoras-del-esmad-pobladores-san-martin/))

Recordó que desde hace tiempo “el Gobierno Nacional viene ejecutando una **cadena de irregularidades**"  para beneficiar a la multinacional Conoco Phillips”. Por esto, han dicho que hay pocas esperanzas que desde el Gobierno se realice cualquier tipo de acción, más cuando en repetidas ocasiones le han advertido de las consecuencias que trae esta actividad de extracción petrolera.

\[KGVID\]https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/02/contaminacion-agua-san-martin.mp4\[/KGVID\]

<iframe id="audio_23735977" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_23735977_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
