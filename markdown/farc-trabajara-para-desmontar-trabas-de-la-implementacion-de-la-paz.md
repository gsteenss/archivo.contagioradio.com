Title: FARC trabajará para desmontar trabas de la implementación de la paz
Date: 2018-01-09 11:51
Category: Nacional, Paz
Tags: acuerdo de paz, FARC, partido político FARC, Rodrigo Lara, Rodrigo Londoño
Slug: farc-trabajara-para-desmontar-trabas-de-la-implementacion-de-la-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/05/timochenko.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: FARC-EP] 

###### [09 Ene 2018] 

A través de un escrito, Rodrigo Londoño, candidato presidencial por el partido FARC, recordó la **crisis por la que está atravesando el país** teniendo en cuenta la falta de credibilidad de la sociedad en el Estado y sus instituciones. Argumentó que Colombia está en un momento de transformaciones que se deben aprovechar y culpó al presidente de la Cámara de Representantes de “corromper el hilo de la historia”.

Indicó que el 2018 será para Colombia un año de **profundas transformaciones** porque, “todo tenemos que hacerlo de nuevo porque como está no funciona”. Afirmó que hay que despertar la organización de la sociedad y “aprovechar que hoy en Colombia no hay rincón donde no se debatan los problemas más graves de la sociedad: la salud, el trabajo, la vivienda, la corrupción, la educación, la cultura”.

### **Colombia ha aprendido el valor de la democracia y la justicia social** 

Londoño recordó que hace más de medio siglo, el país perdió la oportunidad “de ir por la senda de la inclusión, la paz y el progreso” debido a las condiciones de la **confrontación armada** que se generaron por “las campañas de los latifundistas que destacaban la hostilidad de la elite hacia cualquier intervención del Estado relacionada con la tierra”. (Le puede interesar: ["Aprendizajes y retos para las FARC tras un año del Acuerdo de Paz"](https://archivo.contagioradio.com/el-aprendizaje-y-los-restos-de-la-farc-tras-un-ano-de-la-implementacion/))

Sin embargo, tras más de 50 años de confrontación bélica, se logró firmar un acuerdo de paz entre el Estado y la guerrilla de las FARC “que fue recibido y acogido por la inmensa mayoría del pueblo colombiano”. Esto para el nuevo partido político significó una **oportunidad de ampliar los valores democráticos** y luchar por la justicia social que le ha fallado a miles de campesinos que fueron arrinconados y despojados de sus territorios.

Así lo retrata Londoño pues indicó que “a golpe de fusil, en Colombia habíamos aprendido el **valor de la democracia, de la justicia social** y el costo del atraso económico. Aprendimos y cambiamos”. Por esto recordó que el partido FARC se la jugará por las reivindicaciones políticas, sociales y económicas en medio de las oportunidades de cambio que van surgiendo.

### **Rodrigo Lara busca repetir posiciones obsoletas en el país** 

Al mismo tiempo que expresó los retos del partido FARC para 2018, manifestó que la clase política de la actualidad ha venido **cerrando las puertas para la construcción de la paz**. Argumenta que el presidente de la Cámara de Representantes se ha opuesto a la creación de las curules en el Congreso para las víctimas del conflicto armado yendo en contra de lo acordado para alcanzar la paz. (Le puede interesar: ["Rodrigo Lara actúa contra la ética": Alirio Uribe"](https://archivo.contagioradio.com/rodrigo-lara-actua-contra-la-etica-alirio-uribe/))

En ese mismo sentido, le recordó a Lara que su padre, Rodrigo Lara Bonilla, fue “tal vez el último ministro de justicia que **hizo honor a su cargo**” y allí mismo, donde el congresista ha puesto miles de trabas para avanzar con la implementación del acuerdo de paz, su padre afirmó que “la democracia no se defiende atropellando los derechos humanos”.

Con esto en mente, Rodrigo Londoño indicó que en este nuevo año, el partido FARC trabajará por **desmontar las trabas de la implementación** de las reformas a la paz de la mano de millones de jóvenes, campesinos, académico e intelectuales, las Fuerzas Armadas y la Policía que “están al servicio de la patria y no de latifundistas mafiosos y corruptos”.

###### Reciba toda la información de Contagio Radio en [[su correo]

<div class="osd-sms-wrapper" style="text-align: justify;">

</div>
