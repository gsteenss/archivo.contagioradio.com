Title: El dolor más grande
Date: 2015-07-21 20:54
Category: Carolina, Opinion
Tags: Ángel Mina, Christian Aragón jóven de Buenaventura asesinado, Espacio Humanitario, Jóvenes asesinados por paramilitares en Buenaventura
Slug: el-dolor-mas-grande
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/07/Espacio-humanitario155.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Gabriel G - Contagio Radio] 

##### [**[Carolina Garzón Díaz](https://archivo.contagioradio.com/carolina-garzon-diaz/) (**[**@E\_vinna**](https://twitter.com/E_Vinna)**)**]

###### 22, Julio, 2015

La noche del domingo 19 de julio dos jóvenes, dos niños menores de edad, fueron asesinados por la espalda por paramilitares que operan y mandan en las calles de Buenaventura. No puedo imaginar dolor más grande que el de una madre o un padre que debe tomar en sus brazos el cuerpo sin vida de su hijo que acaba de ser baleado por la espalda cuando trataba de huir de los hombres armados que controlan su comuna.

Christian Aragón era un joven de 15 años de edad, a quien los paramilitares se intentaron llevar a la fuerza el pasado domingo. Cuando él se opuso y corrió para huir, estos hombres le dispararon fríamente por la espalda. Dos de los disparos acabaron con su vida y en el mismo ataque fue herido el niño Sol Ángel Mina, quien murió pocas horas después en la Clínica Santa Sofía. La comunidad se estremeció y la noche fue una de las más oscuras para los pobladores del sector.

Estremece el alma que la vida de estos jóvenes sea cegada por la espalda únicamente por decir NO a las armas y la violencia. Las palabras no son suficientes para describir el hondo dolor que causan sus muertes, porque son los jóvenes y los niños quienes más sufren en esta absurda guerra que permanece inmune en el puerto más importante del suroccidente de Colombia. Bien lo dice el Centro Nacional de Memoria Histórica en su reciente informe “Buenaventura, un puerto sin comunidad” cuando afirma que son los niños y jóvenes quienes están más expuestos a las diversas formas de violencia como el reclutamiento forzado, los asesinatos, la tortura, las desapariciones forzadas y el microtráfico de drogas ilícitas.

¿Cómo se construye paz? ¿Cómo se construye futuro? ¿Cuál es la voluntad del Estado? ¿Por qué se ha abandonado a su suerte a la población de Buenaventura? Esas y más preguntas rondan luego de este doloroso hecho. Queda claro, una vez más, que la construcción de la paz va más allá de los acuerdos y las desmovilizaciones; y queda claro, otra vez, que está en manos del Estado la confrontación a los grupos paramilitares que permanecen en los territorios. No será posible construir un futuro sin violencia si seguimos sembrando el dolor, la impunidad, el desconsuelo y la ira en los corazones y mentes de nuestros niños y jóvenes.

Las muertes de Christian Aragón y de Ángel Mina nos confrontan con la persistencia de una guerra que lastima a los más débiles y vulnerables; pero al mismo tiempo nos muestran la resistencia de los pueblos que anteponen la vida, la esperanza, la memoria, la justicia y la firmeza en lugar de la muerte y la venganza. Ante estos cruentos asesinatos la respuesta de la comunidad bonaerense es la vida y la lucha pacífica para evitar que estos lamentables acontecimientos vuelvan a ocurrir.

Desde otros lugares de Colombia y del mundo nuestro deber es exigir justicia. Insistir para que los asesinatos de Christian y Ángel no se pierdan en la impunidad y para que la respuesta del gobierno de Colombia no sea aumentar la exacerbada militarización, sino brindar protección integral a una comunidad que merece tener una vida digna, una juventud plena y una infancia feliz.

Desde el fondo de mi alma, mis más sentidas condolencias a las familias, amigos, acompañantes y vecinos de Christian Aragón y de Ángel Mina. No están solos.
