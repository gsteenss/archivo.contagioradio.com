Title: Actualidad
Date: 2014-11-21 15:03
Author: AdminContagio
Slug: actualidad
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/07/INICIO.png" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/03/voces-de-la-tierra.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

Hoy
---

[![Presidente Duque seguimos esperando su llegada: Minga del Cauca](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/03/D1eGwlrUwAAcjVp-770x260.jpg "Presidente Duque seguimos esperando su llegada: Minga del Cauca"){width="770" height="260"}](https://archivo.contagioradio.com/presidente-duque-seguimos-en-minga-esperando-su-llegada-comunidades-indigenas-del-cauca/)  

###### [Presidente Duque seguimos esperando su llegada: Minga del Cauca](https://archivo.contagioradio.com/presidente-duque-seguimos-en-minga-esperando-su-llegada-comunidades-indigenas-del-cauca/)

[<time datetime="2019-03-14T14:59:16+00:00" title="2019-03-14T14:59:16+00:00">marzo 14, 2019</time>](https://archivo.contagioradio.com/2019/03/14/)Las comunidades indígenas exigen que se especifique dentro del Plan Nacional de Desarrollo las garantías presupuestales que fueron acordadas previamente[Leer más](https://archivo.contagioradio.com/presidente-duque-seguimos-en-minga-esperando-su-llegada-comunidades-indigenas-del-cauca/)  
[![Comisión de la Verdad sigue firme en su respaldo a la JEP](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/03/D1k0PZeXQAAjnZW-770x260.jpg "Comisión de la Verdad sigue firme en su respaldo a la JEP"){width="770" height="260"}](https://archivo.contagioradio.com/comision-de-la-verdad-deja-en-firme-su-respaldo-a-la-jep/)  

###### [Comisión de la Verdad sigue firme en su respaldo a la JEP](https://archivo.contagioradio.com/comision-de-la-verdad-deja-en-firme-su-respaldo-a-la-jep/)

[<time datetime="2019-03-14T14:23:53+00:00" title="2019-03-14T14:23:53+00:00">marzo 14, 2019</time>](https://archivo.contagioradio.com/2019/03/14/)Su dirigente, Francisco de Roux y varios de los comisiones presentes expresaron su apoyo al mecanismo de justicia y a su presidenta Patricia Linares[Leer más](https://archivo.contagioradio.com/comision-de-la-verdad-deja-en-firme-su-respaldo-a-la-jep/)  
[![Dos razones del bajo funcionamiento en la Comisión de Paz del Congreso](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/03/D1i6nGsXcAALe_g-770x260.jpg "Dos razones del bajo funcionamiento en la Comisión de Paz del Congreso"){width="770" height="260"}](https://archivo.contagioradio.com/dos-razones-por-las-que-la-comision-de-paz-del-congreso-esta-dejando-de-ser-de-paz/)  

###### [Dos razones del bajo funcionamiento en la Comisión de Paz del Congreso](https://archivo.contagioradio.com/dos-razones-por-las-que-la-comision-de-paz-del-congreso-esta-dejando-de-ser-de-paz/)

[<time datetime="2019-03-14T14:23:19+00:00" title="2019-03-14T14:23:19+00:00">marzo 14, 2019</time>](https://archivo.contagioradio.com/2019/03/14/)Aunque este escenario debería propender por la paz por el contrario, este se ha convertido en un nuevo foco de polarización.[Leer más](https://archivo.contagioradio.com/dos-razones-por-las-que-la-comision-de-paz-del-congreso-esta-dejando-de-ser-de-paz/)  
[![Declaran inconstitucionales las modificaciones del Centro Democrático a la JEP](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/05/03-02-2015justice_gavel-e1527700435872-770x260.jpg "Declaran inconstitucionales las modificaciones del Centro Democrático a la JEP"){width="770" height="260"}](https://archivo.contagioradio.com/declaran-inconstitucionales-las-modificaciones-del-centro-democratico-la-jep/)  

###### [Declaran inconstitucionales las modificaciones del Centro Democrático a la JEP](https://archivo.contagioradio.com/declaran-inconstitucionales-las-modificaciones-del-centro-democratico-la-jep/)

[<time datetime="2019-03-13T21:40:04+00:00" title="2019-03-13T21:40:04+00:00">marzo 13, 2019</time>](https://archivo.contagioradio.com/2019/03/13/)La Corte Constitucional falló a favor de organizaciones de Derechos Humanos y declaró inconstitucionales las modificaciones que el Centro Democrático le hizo a la ley de procedimento de la JEP[Leer más](https://archivo.contagioradio.com/declaran-inconstitucionales-las-modificaciones-del-centro-democratico-la-jep/)  
[![Duro reclamo de víctimas tras objeciones de gobierno Duque contra la JEP](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/03/mujeres-rurales-770x260.jpg "Duro reclamo de víctimas tras objeciones de gobierno Duque contra la JEP"){width="770" height="260"}](https://archivo.contagioradio.com/dura-carta-victimas-tras-objeciones-gobierno-duque-la-jep/)  

###### [Duro reclamo de víctimas tras objeciones de gobierno Duque contra la JEP](https://archivo.contagioradio.com/dura-carta-victimas-tras-objeciones-gobierno-duque-la-jep/)

[<time datetime="2019-03-13T20:26:24+00:00" title="2019-03-13T20:26:24+00:00">marzo 13, 2019</time>](https://archivo.contagioradio.com/2019/03/13/)En una dura carta más de 100 comunidades de víctimas aseguran que las objeciones del gobierno Duque contra la JEP son una burla al derecho a la paz y desconocen la voz de las víctimas.[Leer más](https://archivo.contagioradio.com/dura-carta-victimas-tras-objeciones-gobierno-duque-la-jep/)  
[![“Estamos más unidos que nunca para defender la paz “: Oposición](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/03/Captura-de-pantalla-2019-03-13-a-las-3.27.44-p.m.-770x260.png "“Estamos más unidos que nunca para defender la paz “: Oposición"){width="770" height="260"}](https://archivo.contagioradio.com/estamos-mas-unidos-nunca-defender-la-paz-oposicion/)  

###### [“Estamos más unidos que nunca para defender la paz “: Oposición](https://archivo.contagioradio.com/estamos-mas-unidos-nunca-defender-la-paz-oposicion/)

[<time datetime="2019-03-13T16:17:01+00:00" title="2019-03-13T16:17:01+00:00">marzo 13, 2019</time>](https://archivo.contagioradio.com/2019/03/13/)Un mensaje quedó claro tras la réplica de la oposición al presidente Duque: Están unidos, y su enfoque central es la defensa de la paz[Leer más](https://archivo.contagioradio.com/estamos-mas-unidos-nunca-defender-la-paz-oposicion/)
