Title: Líder social Frederman Quintero se encuentran entre las víctimas de la masacre en el Tarra
Date: 2018-07-31 09:31
Category: DDHH, Nacional
Tags: Catatumbo, ELN, EPL, Tarra
Slug: lider-social-frederman-quintero-se-encuentran-entre-las-victimas-de-la-masacre-en-el-tarra
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/07/FredemanQuintero.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: ASCAMCAT] 

###### [31 Jul 2018] 

La Asociación Campesina del Catatumbo repudió la masacre de 9 personas, cometida el pasado 30 de julio, en el barrio La Esperanza. De igual forma, confirmaron que entre las víctimas se encontraba el líder social **Frederman Quintero, presidente de la Junta de Acción Comunal del kilómetro 84 y coordinador del comité veredal de ASCAMCAT**. Además, se estaría investigando si otras dos víctimas harían parte del proceso de reincorporación de FARC.

Los hechos se presentaron sobre las 2:30 de la tarde, cuando las personas se encontraban departiendo en un billar del barrio La Esperanza. Posteriormente un grupo de hombres armados, llegaron en motos hasta este lugar, en donde abrieron fuego contra quienes se encontraban allí. (Le puede interesar: ["Siete personas son asesinadas en el Tarra, Norte de Santander"](https://archivo.contagioradio.com/siete-personas-son-asesinadas-en-billar-de-tarra-nortesantander/))

De acuerdo con el comunicado de prensa de ASCAMCAT, Frederman Quintero se desempeñaba **actualmente como líder comunitario, era el presidente de la Junta de Acción Comunal de la vereda Kilómetro 84, en el Tarra**. y había sido delegado como el representante de su comunidad a la Audiencia Popular Regional, que se celebraría los días 3 y 4 de agosto, en el municipio de San Calixto.

ASCAMCAT señaló que estos hechos **"los hace rememorar los difíciles años del paramilitarismo"** y los llena de profundo dolor. Asimismo, exigieron a las autoridades el esclarecimiento de estos hechos e hicieron un llamado a todas los grupos armadas que hacen presencia en el territorio, para que se respeten las garantías a la vida de quienes habitan allí.

### **ELN y EPL rechazan masacre en el Tarra** 

A través de comunicados de prensa, tanto la guerrilla del Ejército de Liberación Nacional, como el Ejército Popular de Liberación, **rechazaron la masacre cometida en el barrio La Esperanza y manifestaron que no son responsables de este hecho**.

En el comunicado del ELN, dado a conocer en su cuenta de Twitter, expresaron que "condenan pública y categóricamente" los actos de violencia. De la misma manera, se conoció un comunicado emitido por parte del EPL, en el que aclaran que esta guerrilla no tiene nada que ver con este atentado.

\[caption id="attachment\_55229" align="alignnone" width="600"\]![comunicado de prensa EPL](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/07/WhatsApp-Image-2018-07-31-at-8.02.10-AM-600x800.jpeg){.size-medium .wp-image-55229 width="600" height="800"} comunicado de prensa EPL\[/caption\]

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
