Title: No queremos entrar en un júbilo ingenuo: Iván Cepeda
Date: 2020-08-07 10:11
Author: AdminContagio
Category: Actualidad, Nacional
Tags: Álvaro Uribe, Caso Uribe, corte suprema, Iván Cepeda, Reinaldo Villalba
Slug: no-queremos-entrar-en-un-jubilo-ingenuo-ivan-cepeda
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/Captura-de-pantalla-79.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

Luego de conocerse la medida de aseguramiento impuesta por la Corte Suprema de Justicia en contra del expresidente Álvaro Uribe en el proceso que cursa en su contra por los presuntos delitos de fraude procesal y soborno; el senador Iván Cepeda quien funge como víctima en dicho proceso señaló que recibe con beneplácito la decisión de la Corte pero que no quiere entrar en un «*jubilo ingenuo*».

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

[Cepeda](https://twitter.com/IvanCepedaCast) señaló que es importante darle dimensión a la decisión de la Corte pero guardando serenidad con miras a la decisión de fondo del proceso. Además, afirmó que **el proceso por manipulación de testigos no es un caso aislado sino que está inmerso en un conjunto de hechos que espera se esclarezcan a partir de esta primera decisión**; refiriendo al caso original donde se resuelven los presuntos vínculos del expresidente Uribe con la creación de grupos paramilitares. (Le puede interesar: [“Responsabilidad de Álvaro Uribe en el caso de Diego Cadena es innegable” - Iván Cepeda](https://archivo.contagioradio.com/alvaro-uribe-tiene-responsabilidad-innegable-en-caso-diego-cadena/))

<!-- /wp:paragraph -->

<!-- wp:quote -->

> **La decisión de la Corte, no me atañe solo a mí y a mis abogados sino que compromete a todo el país**
>
> <cite>Senador Iván Cepeda, víctima en el caso Uribe</cite>

<!-- /wp:quote -->

<!-- wp:paragraph -->

Por su parte Reinaldo Villalba, abogado de Iván Cepeda en el caso, señaló que la decisión de **la Corte se tomó con base en «*un acervo probatorio voluminoso, contundente y sólido*» por lo que la medida de aseguramiento está justificada en derecho.** También, que es un buen antecedente para que empiecen a avanzar otros procesos, **no solo en relación con Uribe, sino frente a «*todos los violadores de derechos humanos*»**.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Adicionalmente, el abogado señaló que ese acervo probatorio contiene, entre otras cosas, los pagos que se realizaron a los testigos y aseguró que **«*es difícil  pensar que un auxilio humanitario supere los 7 millones de pesos, sólo hablando de aquellos pagos que tienen respaldo documental*»**, es decir, sin contar aquellos que la Fiscalía en el caso de Cadena dice que superan los 40 millones. (Lea también: [Razones por las que Diego Cadena, abogado de Álvaro Uribe, será imputado por la Fiscalía](https://archivo.contagioradio.com/razones-por-las-que-diego-cadena-abogado-de-alvaro-uribe-sera-imputado-por-la-fiscalia/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

**También, fue enfático en señalar que ni el senador Cepeda, ni él como su apoderado, llevaron ni un solo testigo al caso;** sino que toda su actuación se ha limitado a desmentir los falsos testimonios de aquellas personas que la defensa de Uribe ha llevado al proceso.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Las reacciones del uribismo y la independencia de la Justicia

<!-- /wp:heading -->

<!-- wp:paragraph -->

Iván Cepeda, señaló que **el presidente Iván Duque y su Partido Centro Democrático han realizado una** «***intromisión grosera***» **en los asuntos de la Justicia** ante la opinión pública a través de alocuciones presidenciales e  intervenciones en medios. (Lea también: [«Duque y su Partido buscan presionar a la Corte Suprema en caso de Álvaro Uribe»](https://archivo.contagioradio.com/duque-y-su-partido-buscan-presionar-a-la-corte-suprema-en-caso-de-alvaro-uribe/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Frente a este tema, Reinaldo Villalba, aseguró que el Presidente tiene que comportarse como servidor público y separar sus opiniones personales de su voz institucional. **«*Iván Duque siempre que abre la boca está hablando como Presidente, no podemos tener un presidente que ataque a la Corte Suprema de Justicia y diga que está dando una opinión como ciudadano*»**, dijo Villalba.

<!-- /wp:paragraph -->

<!-- wp:quote -->

> **No le es dable, como presidente, a Iván Duque, hacer ese tipo de críticas en relación a la decisión de la Corte, menos aun cuando estoy absolutamente convencido que no conoce ni la caratula del expediente»**
>
> <cite>Reinaldo Villalba, abogado de Iván Cepeda en el caso Uribe</cite>

<!-- /wp:quote -->

<!-- wp:paragraph -->

Por ello, hizo un reconocimiento a la postura de la Corte para no dejarse «*atemorizar y amedrentar después de toda la campaña de presión que se dio durante todo el proceso y especialmente en las últimas semanas*» por parte del presidente Duque y su Partido.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Iván Cepeda y Reinaldo Villalba hablaron frente al futuro del caso y la esperanza de cambio en el país

<!-- /wp:heading -->

<!-- wp:paragraph -->

Tanto el senador como el abogado, coincidieron en señalar que como parte civil en el proceso harán un seguimiento estricto para que **las restricciones que impone la medida de aseguramiento en contra de Álvaro Uribe se cumplan y así se garantice la salvaguardia del proceso y las pruebas.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Villalba afirmó que una vez superada la etapa de imputación, el proceso tiene que transcurrir con mayor prontitud y no puede ser objeto de más dilaciones. Cepeda, por su parte, agregó que la Corte ni el poder judicial pueden ser sometidos a presiones, ya que existe una **«*campaña mediática para ir con todo contra la Corte Suprema*»** y que con la exigencia de una Asamblea Constituyente por parte del Centro Democrático «***se quiere destruir al poder judicial, acabando con las Altas Cortes y la JEP*»**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

No obstante, Cepeda quiso dar un mensaje de esperanza y afirmó que «*sabemos dar luchas a largo plazo que exigen constancia serenidad y perseverancia*».  Por su lado, Villalba señaló que **hay que ser «*gotas que agrietan la roca*»** donde «*la roca es la impunidad y el poder monolítico y las gotas la sociedad que está trabajando por el cambio*».

<!-- /wp:paragraph -->

<!-- wp:quote -->

> **Hace unos años era impensable que lo que está pasando (detención de Uribe) hoy pudiese ser posible»**
>
> <cite>Senador Iván Cepeda, víctima en el caso Uribe</cite>

<!-- /wp:quote -->

<!-- wp:core-embed/facebook {"url":"https://www.facebook.com/watch/?v=288687955795715"} -->

<figure class="wp-block-embed-facebook wp-block-embed">
<div class="wp-block-embed__wrapper">

https://www.facebook.com/watch/?v=288687955795715

</div>

</figure>
<!-- /wp:core-embed/facebook -->

<!-- wp:block {"ref":78955} /-->
