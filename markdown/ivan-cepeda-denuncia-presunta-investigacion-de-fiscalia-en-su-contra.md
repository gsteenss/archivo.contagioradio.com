Title: Iván Cepeda denuncia presunta investigación de Fiscalía en su contra
Date: 2019-04-26 13:49
Author: CtgAdm
Category: Judicial, Política
Tags: Fiscalía General de la Nación, Iván Cepeda, jurisdicción especial para la paz, Nestor Humberto Martínez
Slug: ivan-cepeda-denuncia-presunta-investigacion-de-fiscalia-en-su-contra
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/10/Ivan-Cepeda.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo] 

El senador Iván Cepeda radicó un derecho de petición este viernes, por medio del cual solicita a la Fiscalía General de la Nación la entrega de información que sustente la afirmación del Fiscal Nestor Humberto Martínez, en la que asegura que el congresista del Polo Democrático incluyó un artículo al texto legislativo de la Ley Estatutaria de la Jurisdicción Especial para la Paz (JEP), buscando garantizar la no extradición de narcotraficantes.

En una rueda de prensa al frente del bunker de la Fiscalía, el senador señaló que el Fiscal estaría desarrollando una investigación que "busca **desenmascarar** **a algunos congresistas que supuestamente habían tramitado, de una manera subrepticia, ese artículo y lo habrían insertado en el texto legislativo”.**

Como parte de esta investigación, el Fiscal tendría bajo la lupa una reunión entre el senador Cepeda y voceros del Clan del Golfo, durante el gobierno del expresidente Juan Manuel Santos, a pesar de que este acercamiento fuese aprobado por el ejecutivo y conocido por el Fiscal. (Le puede interesar: "[Senador Cepeda denuncia complot para vincular su esposa en caso de corrupción](https://archivo.contagioradio.com/senador-cepeda-denuncia-complot-para-vincular-su-esposa-en-caso-de-corrupcion/)")

Según Cepeda, esta investigación buscaría sindicar al congresista como el intermediario entre la banda criminal y el Congreso para lograr que el **artículo 156 de la ley** estatutaria fuese incluida, la cual establece que: "No se concederá la extradición de otras personas que estén ofreciendo verdad ante el Sistema Integral de Verdad, Justicia, Reparación y No repetición (SIVJRNR), antes de que terminen de ofrecer verdad".

Este artículo fue uno de las seis puntos objetadas por el Presidente Iván Duque y luego criticada por el Fiscal Humberto Martínez en audiencia pública del Senado porque estaría permitiendo a narcotraficantes y otros terceros del conflicto armado evadir la extradición.

Sin embargo, la Corte Constitucional avaló el artículo y en su sentencia determinó que los beneficiarios serán "particulares o agentes del Estado, y miembros de la Fuerza Pública que estén ofreciendo verdad plena en el SIVJRNR". (Le puede interesar: "[Presidente Iván Duque objeta seis puntos de la Ley Estatutaria de la JEP](https://archivo.contagioradio.com/presidente-ivan-duque-objeta-seis-puntos-la-ley-estatutaria-la-jep/)")

Frente a tales intenciones, el senador rechazó enfáticamente las acusaciones del Fiscal e indicó **tendrían el efecto  de intimidar a los senadores para votar favorablemente el próximo lunes las objeciones del Presidente Duque a la ley estatutaria de la JEP.**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/video.php?href=https%3A%2F%2Fwww.facebook.com%2FIvanCepedaCastro%2Fvideos%2F636535993476580%2F&amp;show_text=0&amp;width=560" width="560" height="315" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
