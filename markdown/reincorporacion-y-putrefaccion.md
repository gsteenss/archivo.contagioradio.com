Title: Reincorporación y putrefacción
Date: 2017-09-01 16:23
Category: Camilo, Opinion
Tags: acuerdo de paz, democracia, FARC, miliares
Slug: reincorporacion-y-putrefaccion
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/09/acuerdo.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Contagio Radio 

#### **Por[ Camilo de Las Casas](https://archivo.contagioradio.com/camilo-de-las-casas/)** 

###### **1 Sep 2017** 

¡Esa  es la reincorporación, involucrarse, relacionarse y reconocer una sociedad y en ella proponer y construir un proyecto de país.

Hay que evitar ser esquemáticos pero  los rasgos dominantes de la sociedad colombiana arrojan a una trágica realidad: un cadáver insepulto sobre el que nos divertimos

Los colombianos, según encuesta del club de la OCDE, somos mayoritariamente distantes de los asuntos de los poderes institucionales o de la política. Somos parte de esa tendencia mundial indiferente sobre lo público, hastiados quizás de que en ese escenario las demandas sentidas no se resuelven,  entonces mejor es cada quien haga por lo suyo. El bien común sentido de la politica ha derivado en la satisfacción de los apetitos individualistas.

Ese cádaver se revuelve de cuando en vez con el escándalo  suscitando una reacción fogosa o la simple anuencia ante la criminalidad que los poderes han creado y desatado, y que por sus pugnas internas, a veces nos  es posible conocer. Ese tipo de criminalidad se expresa en la violencia estatal, paraestatal y neoparaestatal, por nombrar algunas. O en dinámicas de corrupcción de sectores que ofician el poder ejecutivo, legislativo, judicial, militar, policial y eclesiástico con actores privados legales e ilegales. Ambos casos muestran la banalización y la mercantilización del bien común.

Esos tipos de criminalidad de los que es responsable desde épocas inmemoriales la clase dirigente, inoculan y conllevan a los ciudadanos al miedo o a una actitud medicante, a la indiferencia, a lo individual. Las voluntades huidizas en amores ciegos e infantilizados, cuando algo se politiza, lleva al caudillo o caudilla, a un pastor o pastora, a un abogado, a un periodistas o deportista. Los miedos a la libertad van disponiendo a nuevas formas de opresión a nombre de dios y de la democracia, del fútbol y de la vida fitness, en un entramado del complejo de pirámide y de una vida reality que asume la historia flasheo.

La negativa para apoyar un Acto Legislativo para proscribir el uso privado de las armas para asegurar lo que es propio de un Estado de Derecho como es el monopolio de la fuerza, refleja el miedo de sectores económicos y políticos de reconocer la verdad sobre el origen y motivaciones inconfesables del paramilitarismo. Tal actitud refleja la incapacidad de reconocer la responsabilidad en la violencia y subrepticiamente, el seguir legitimado un mecanismo violento para aplicar nuevamente, si sus intereses se vieran en peligro.

Es lo mismo que de refleja en el miedo  militar, entre ellos el general Rodríguez y el cuestionadísimo Mora Rangel, a la visita de la Fiscal de la CPI Fatuo

Bensuda. Temor a asumir su responsabilidades con operaciones de ilegalidad como autoridad y las que se derivan de la cadena de mando en violaciones graves y sistemáticas de derechos humanos.

Sus presiones para sustraer del Acuerdo del Teatro Colón el artículo 28 del Estatuto de Roma, como lo expresaron ACORE y otros trae sus consecuencias. Creer que con su falsa alharaca de la guerra juridica o del comunismo o su honor mancillado por ser equiparados con su adversario militar les genera  inmunidades es una falsedad que se les desmorona.

<div dir="auto">

Seguir esperando que ellos reconozcan y asuman es el asunto de ellos, el nuestro es reconstruir y construir lo nuevo, sin esperar de ellos ni del Estado putrefacto que han originado.

<div dir="auto">

La sociedad politizada por esas tensiones esquizoides, paranoicas, yoicas de las izquierdas, tampoco asumió la paz positiva como avance hacia un Estado Social Ambiental de Derecho. Así está una FARC peleando a veces en solitario con su Acuerdo, también gracias a su excesivos celos; y un ELN, aún con su apuesta de participación apalancada con los mismos de siempre, y en una engorrosa celotipia en aislamiento.

Reincorporarse hoy es ver esa tozuda realidad masiva de un cádaver, para construir un nuevo sentido de lo político que enamore el país, a la sociedad.

Más allá de las heróicas y significativas epopéyicas de base y de sectores de clases medias que se expresan en contracorriente, la reincorporación de las FARC y las conversaciones con el ELN se encuentran en ese escenario social y de cultura política que está por transformar. Reconocer el cadáver existente que hiede a putrefacto es ver en el presente lo qué es, no lo que imaginamos qué es, y desde ese presente reincorporar un alma nueva.

Sin ese realismo que rompe el espejismo nacido de la ideologización, lo que sigue será prolongar la agonía de una clase dirigente y su proyecto de país, hundiéndonos a todos y todo. El cadáver insepulto que pasa enfrente de nosotros, haciéndonos felizmente dominados, será el sentido de nuestro país.

Reincorporarse a lo existente es morirse con lo putrefacto. Reincorporarse en el presente es construir todo de un modo diferente, lo contrario es la pax neoliberal del taxi y la beca, la del negacionismo y la de la fantasía. El cádaver es cádaver así se muestre como vivo, la reincorporación será la creación social de otra democracia, no la de las FARC si con ellos y con el ELN y todos aquellos que renueven el alma, al ver la realidad, ¡tal cómo es!.

#### [**Leer más columnas de opinión de  Camilo de las Casas**](https://archivo.contagioradio.com/camilo-de-las-casas/)

------------------------------------------------------------------------

######  Las opiniones expresadas en este artículo son de exclusiva responsabilidad del autor y no necesariamente representan la opinión de Contagio Radio.

</div>

</div>
