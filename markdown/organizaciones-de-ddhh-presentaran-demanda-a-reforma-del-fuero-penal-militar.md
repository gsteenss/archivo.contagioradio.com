Title: Organizaciones de DDHH presentarán demanda contra reformas del Fuero Penal Militar
Date: 2015-07-09 14:01
Category: Nacional, Política
Tags: 9 de Abril día de las víctimas Colombia, Alberto Yepes, Coordinación Colombia- Europa-Estados Unidos, fuero penal militar, impunidad, proceso de paz, reforma constitucional
Slug: organizaciones-de-ddhh-presentaran-demanda-a-reforma-del-fuero-penal-militar
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/07/Militares.-Falsos-positivos.-Comunidades-reclaman-justicia.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: [notimundo2.blogspot.com.es]

<iframe src="http://www.ivoox.com/player_ek_4748763_2_1.html?data=lZyhmpyad46ZmKiakpqJd6KomZKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRk9PbwtPW3MbHrdDixtiYxsqPiKW8qZDd1MrXqc%2FowteSpZiJhZLijMnSz8bSqMKfwpDfx8vTts7VjMiah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Alberto Yepes, Coordinación Colombia-Europa-Estados Unidos] 

###### [9 de Julio de 2015]

Organizaciones defensoras de derechos humanos, congresistas y representantes de víctimas, **preparan una demanda en contra de las reformas al fuero penal militar**, que avanzan en el Congreso de República.

Alberto Yepes de la Coordinación Colombia – Europa- Estados Unidos, afirma que la demanda se presentará fundamentalmente en contra del  proyecto de Ley que pretende reformar la Constitución Política, mediante el cual “**se consagraría la exclusión del derecho internacional de los derechos humanos,** como pauta de investigación y juzgamiento de militares que incurran en violaciones a DDHH en el marco del conflicto armado”.

Para Yepes, este es el modo como el **Estado colombiano busca “deshacerse de los tratados internacionales”**, y generar formas para que los militares que han cometido crímenes, pueden ser absueltos aplicándose una versión del Derecho Internacional Humanitario, “que supuestamente permite matar civiles siempre y cuando se entiendan como muertes en operaciones policiales o militares”, explica.

Pese a que la acción está dirigida contra el proyecto que modificaría la constitución, las demás reformas al fuero penal militar también serán demandadas. Según el defensor de derechos humanos, **el primer paso es la creación de inconstitucionalidad del acto legislativo que soportaría los demás proyectos.**

**La demanda será presentada a comienzos de la próxima semana,** se espera contar con el respaldo de parte de la comunidad internacional, quienes "han observado que el gobierno no tiene voluntad política para investigar y esclarecer casos como los falsos positivos", dice Alberto Yepes.

##### 
