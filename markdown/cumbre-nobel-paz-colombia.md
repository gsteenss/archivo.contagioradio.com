Title: Las esperanzas de los premios Nobel de Paz puestas en Colombia
Date: 2017-02-02 14:45
Category: Nacional, Paz
Tags: colombia, cumbre, Nobel de Paz, paz
Slug: cumbre-nobel-paz-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/02/Cumbre-mundial-de-nobel-de-paz.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### Foto: Contagio Radio 

##### 2 Feb 2017 

“**Me dijeron que Bogotá estaba ubicada a 2600 metros más cerca de las estrellas, yo creo que esta 2600 metros más cerca de la paz**” con esta frase Ekaterina Zagladina, presidenta del secretariado de la Cumbre de los Premio Nobel de Paz, saludo el proceso de paz que se viene adelantando en el país. Al igual que ella, premios Nobel de Paz como, Oscar Arias, José Ramos Horta y Tawakkol Karman, expresaron su apoyo, desde la experiencia de paz, a las transformaciones que vive y vivirá Colombia para implementar los Acuerdos de Paz.

Para José Ramos Horta, laureado en 1996 por su trabajo para buscar una solución justa y pacífica del conflicto en Timor Oriental, “**se necesita coraje iniciar una guerra, pero se necesita aún más coraje para hacer la paz**”. [Le puede interesar: Premio Nobel es un espaldarazo al proceso de paz](https://archivo.contagioradio.com/premio-nobel-es-un-espaldarazo-al-proceso-de-paz/).

Ramos compartió la experiencia de superar el dolor del conflicto en su país, un genocidio que cobró la vida de más de 200.000 personas “**se cuán difícil es perdonar y reconciliarse, pero cuanto más tiempo dure un conflicto, más hondas son las cicatrices**” y agregó “las armas han callado la paz ahora es una posibilidad real, hermanos colombianos son ustedes los que van a construir su propio modelos de reconciliación y de justicia. **La paz tiene que ser construida paso a paso, en el hogar en comunidad, en las escuelas y en toda la Nación**”

Tawakkol Karman, laureda en el año 2011, por su lucha no violenta por la seguridad y el derecho de las mujeres a participar en la construcción de paz, se refirió hacia la importancia del as víctimas y de que sean escuchadas en este momento de la implementación de los acuerdos de paz, además expresó que c**ada una de las personas víctimas del conflicto armado son héroes** que permiten la construcción “a futuro desde su sacrificio” de la esperanza.

Para finalizar Oscar Arias, ex presidente de Costa Rica y premio Nobel de Paz en el año 1987, indicó que los pasos que da Colombia en medio de una “**era en donde hay gobiernos que pretenden construir muros y hacer de la ignorancia una fuerza**” son luz para el mundo. “Colombia ha demostrado que contra todos los pronósticos está a punto de consolidar la paz” que hará parte de la historia y que escribe un nuevo capítulo en el mundo.

Durante estos tres días también se espera la intervención de otros Nobel de Paz como Rigoberta Menchu, Jody William, Lech Walesa, entre otros, que principalmente tratarán la temática de la paz atravesada por los ejes que la componen como la educación, la democracia, la participación de las entidades privadas en su construcción, el ambiente, entre otros.

###### Reciba toda la información de Contagio Radio en [[su correo]
