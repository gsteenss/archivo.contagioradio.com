Title: 5 retos para la implementación de los acuerdos de paz entre gobierno y FARC
Date: 2016-08-24 18:15
Category: Nacional, Paz
Tags: conflicto armado, FARC, Gobierno, paz, proceso de paz
Slug: 4-retos-a-superar-para-lograr-una-paz-estable-y-duradera
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/02/casas-de-paz1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Casas de Paz] 

###### [24 Ago 2016] 

Luego del anuncio sobre la terminación del texto final de los acuerdos, queda pendiente los colombianos y colombianas voten el próximo 2 de octubre si aprueban el texto del acuerdo final. Una vez se dé la votación se iniciará la concentración de los integrantes de las FARC-EP en las zonas veredales transitorias que ya han sido acordadas.

Para diversos sectores **este nuevo escenario plantea una serie de retos** que deberán ser superados para lograr la consolidación de una paz estable y duradera.

### **PARAMILITARISMO** 

Uno de los principales retos que debe enfrentar el Estado colombiano es el de brindar las **garantías necesarias para la integridad de los guerrilleros** que se reintegren a la vida civil. En ese sentido es urgente que se sumen los esfuerzos necesarios para el desmonte real y efectivo de los grupos paramilitares que hacen [[presencia en 22 departamentos](https://archivo.contagioradio.com/paramilitares-hacen-presencia-en-149-municipios-de-colombia-indepaz/)] de Colombia y que ubican en [[situación de riesgo extremo](https://archivo.contagioradio.com/para-el-posconflicto-88-municipios-estan-en-riesgo-extremo-por-presencia-paramilitar/)] a 88 municipios, teniendo en cuenta que durante el 2015 cometieron [[1064 violaciones a los derechos humanos](https://archivo.contagioradio.com/es-fantasioso-asegurar-que-el-paramilitarismo-no-sigue-vigente/)], entre amenazas, atentados, ejecuciones, torturas y violaciones sexuales.

Así las cosas queda claro que el paramilitarismo no es solamente una amenaza para los y las integrantes de las FARC, sino para defensores de derechos humanos que temen que se pueda presentar una situación similar al genocidio de la Unión Patriótica, así mismo que continúen las amenazas contra defensores y defensoras, sino contra quienes denuncian su persistencia, quieren recuperar las tierras o simplemente trabajan por la paz.

### **CUMPLIMIENTO DE LOS ACUERDOS** 

Otro de los retos a superar es que las partes cumplan los acuerdos que han pactado, en especial por parte del **Gobierno colombiano que ha incumplido el 87% de los acuerdos** a los que ha llegado con diversos sectores sociales luego de paros en exigencia de condiciones dignas de salud, educación, trabajo y desarrollo agrario, principalmente. Prueba de ello fue el paro que diversos sectores congregados en la Cumbre Agraria protagonizaron durante 13 días debido al [[incumplimiento de los compromisos que se pactaron tras el paro de 2014](https://archivo.contagioradio.com/vuelve-el-paro-nacional-el-30-de-mayo-tras-dos-anos-de-incumplimientos-del-gobierno/)].

### **PEDAGOGÍA PARA ENTENDER LOS ACUERDOS** 

**La paz debe convertirse en una ola que llegue a toda la ciudadanía tanto en las zonas rurales como en las urbanas**, incluidos aquellos sectores que han sido víctimas directas del conflicto armado así como a los más escépticos frente al proceso de paz y que como asegura el profesor Carlos Medina, son los que han vivido la guerra a través de las pantallas de sus televisores. En este intento de sensibilizar en torno a la importancia del proceso de paz en Colombia, diversos sectores sociales han sumado sus esfuerzos para emprender [[actividades pedagógicas y culturales en distintas regiones](https://archivo.contagioradio.com/?s=pedagogia+de+paz+)] del país.

### **APROBACIÓN EN EL PLEBISCITO** 

Aunque las últimas encuestas han reflejado que crece la aprobación del plebiscito, todavía existe el riesgo de que gane el NO. León Valencia dice que en las consultas populares no todo está dicho y un ejemplo de ello es la decisión de los habitantes de Inglaterra que aprobaron, sin información, la salida de la Unión Europea, situación que pronto dio la vuelta y terminó afectando la economía de la clases baja y media.

Por situaciones como estas es que no se debería bajar la guardia y se debería aplicar de manera inmediata una estrategia para la explicación de los acuerdos. Según explican algunos analistas consultados por www.contagioradio.com la mayoría de las personas están tomando decisiones basándose en información de medios de comunicación y no en una lectura juiciosa de los acuerdos, situación que sería fácilmente superable si la gente que votará se da cuenta de que los acuerdos están diseñados en **un 90% para abordar necesidades básicas y no para resolver los problemas de los integrantes de las guerrillas.**

### **SOLUCIÓN DE OTROS CONFLICTOS** 

El país está en la etapa final del conflicto armado, este nuevo momento permitirá que los conflictos sociales, económicos y políticos relacionados con la falta de garantías en salud, educación, inversión social, empleo y servicios públicos, que se manifiestan en la cotidianidad de muchas comunidades a lo largo y ancho del territorio nacional sean resueltos, como lo han demandado desde [[Chocó](https://archivo.contagioradio.com/levantaremos-el-paro-cuando-hayan-compromisos-serios-pueblo-chocoano/)], [[Putumayo](https://archivo.contagioradio.com/33-dias-de-movilizacion-pacifica-24-personas-heridas-por-fuerza-publica-en-putumayo/)], [[Cauca](https://archivo.contagioradio.com/1800-docentes-del-cauca-viajan-a-bogota-para-continuar-paro-indefinido/)] y [[Caquetá](https://archivo.contagioradio.com/12-heridos-y-9-detenidos-deja-represion-de-la-fuerza-publica-en-caqueta/)] en las movilizaciones que sus poblaciones han protagonizado durante las últimas semanas.

###### [Reciba toda la información de Contagio Radio en[ [su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)] ] 
