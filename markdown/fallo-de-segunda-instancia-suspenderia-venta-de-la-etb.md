Title: Fallo de segunda instancia suspendería venta de la ETB
Date: 2016-08-12 13:04
Category: Economía, Nacional
Tags: Venta de la ETB
Slug: fallo-de-segunda-instancia-suspenderia-venta-de-la-etb
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/08/consejo-bogo.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: 360radio] 

###### [12 de Agost]

El pasado 8 de agosto se emitió el fallo de la segunda instancia que favorece la tutela interpuesta por Alejandra Wilches, presidenta de ATELCA, en donde **reclama la violación al debido proceso administrativo hecha por el Consejo de Bogotá** al no dar escenarios de debate fuertes frente a las implicaciones de la venta de la ETB, Este **podría ser el sustento legal para suspender la venta de la misma. **

Este fallo es resultado de un primer derecho de petición interpuesto por la Asociación Nacional de Técnicos en Telefonía y a fines (ATELCA), en el que argumentaban que **no se hizo el debido proceso con estudios para llevar el proyecto de la venta de la ETB al Consejo de Bogotá**, adicionalmente solicitaban que se diera un debate amplio y transparente en torno a la propuesta, teniendo en cuenta la calidad de patrimonio público de la ETB.

Sin embargo, el Consejo de Bogotá, negó en primera instancia los cronogramas, reuniones y fueros para debatir el proyecto violando el debido proceso administrativo, posteriormente aprobaron la venta de la ETB y ATELCA instauró una primera tutela que [fue fallada a favor del Consejo de Bogotáy que luego de ser impugnada por la asociación falla en favor de esta.](https://archivo.contagioradio.com/pese-a-la-oposicion-se-concretan-los-primeros-negocios-para-vender-la-etb/)

Paralelamente ATELCA esta llevando un proceso jurídico por nulidad a la venta de la ETB, en donde este fallo sirve como insumo para soportar esa nulidad. De igual modo, instauraron una **denuncia penal en contra de los 31 concejales y el alcalde mayor de Bogotá, por un posible prevaricato** al aprobar la venta de la Empresa Pública de Telefonía.

De acuerdo con Camilo Nieto, secretario general de ATELCA están a la espera de que el Consejo de Bogotá reacciones a este fallo, sin embargo afirma que **"es posible que con el proceso de nulidad se detenga la venta de la ETB".**

[![fallo ETB](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/08/fallo-ETB.jpg){.aligncenter .size-full .wp-image-27806 width="1200" height="1221"}](https://archivo.contagioradio.com/fallo-de-segunda-instancia-suspenderia-venta-de-la-etb/fallo-etb/)

<iframe src="http://co.ivoox.com/es/player_ej_12527064_2_1.html?data=kpeilJyUepWhhpywj5aUaZS1lpaah5yncZOhhpywj5WRaZi3jpWah5yncaTVzs7Z0ZCyrcbo0IqfpZClmKbApKaah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ..&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
