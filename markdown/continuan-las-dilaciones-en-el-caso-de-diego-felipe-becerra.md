Title: Continúan las dilaciones en el caso de Diego Felipe Becerra
Date: 2016-08-23 22:40
Category: DDHH, Nacional
Tags: crímenes de estado, Diego Felipe Becerra, Ejecuciones Extrajudiciales, procuraduria
Slug: continuan-las-dilaciones-en-el-caso-de-diego-felipe-becerra
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/08/tripido8.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Archivo 

###### [23 Ago 2016]

<div class="noticia_apertura">

[El juzgado 43 de Conocimiento emitió fallo condenatorio contra el patrullero Wilmer Alarcón, por el homicidio agravado del joven grafitero de 16 años, Diego Felipe Becerra. Pese a que Alarcón, deberá pagar entre 33 y 50 años de prisión, el patrullero se encuentra prófugo de la justicia, luego de que el pasado 18 de agosto el juez 47 de conocimiento **ordenara su libertad.**]

El juez había dado tomado esa decisión justo el mismo día en que la Fiscalía y la Procuraduría habían solicitado que se emitiera el fallo con el que se esperaba dictar condena contra Alarcón por el asesinato del joven. Aunque a que **existían los elementos materiales probatorios suficientes para evidenciar la responsabilidad** **del patrullero,** el juez consideró que hubo vencimiento de términos en el proceso que se adelanta contra Wilmer.

Pese a la noticia, la familia de Diego Felipe asegura que continuarán en la lucha de búsqueda de verdad y justicia. "no nos esperábamos esas decisiones del juez, es un proceso en el que llevamos 5 años, un proceso que ha presentado dilaciones de todo tipo, es una noticia triste pero seguiremos con fortaleza y buscando que los coroneles implicados también sigan siendo investigados", asegura Gustavo Trejos, padre de Diego Felipe Becerra.

Cabe recordar que la Procuraduría destituyó e inhabilitó por 12 años a subintendente y dos patrulleros. Los uniformados fueron sancionados discipliniariamente por participar en la manipulación de la escena de los hechos registrados en la noche del 19 de agosto de 2011, sin embargo, en **la decisión se absolvieron a un coronel, un teniente coronel y un subintendente.**

**"Después de tener 13 personas capturadas, ahora todas están en libertad, solo hay dos patrulleros detenidos,** mientras los coroneles están trabajando como si no hubiera pasado nada", dice Trejos.

El próximo **27 y 28 de agosto se realizará un evento donde habrá distintas actividades culturales en el puente de la calle 116 con Av. Boyacá** donde se homenajeará la memoria del joven víctima de este crimen que ya cumple 5 años en la impunidad.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) {#reciba-toda-la-información-de-contagio-radio-en-su-correo-o-escúchela-de-lunes-a-viernes-de-8-a-10-de-la-mañana-en-otra-mirada-por-contagio-radio .titulo}

</div>
