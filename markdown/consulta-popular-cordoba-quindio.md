Title: Impulsan consulta popular para salvar el páramo de Chilí en Quindio
Date: 2017-09-15 16:27
Category: Ambiente, Voces de la Tierra
Tags: consulta popular, cordoba, defensa del territorio, Mineria, Quindío
Slug: consulta-popular-cordoba-quindio
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/09/chili.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: @nautilusmaker] 

###### [15 Sept 2017] 

Las personas que promueven la consulta popular en el municipio de Córdoba del departamento de Quindío, **lograron consolidar la iniciativa de consulta popular por vía de concertación administrativa.** Esperan que en noviembre, los cordobeses sean los que decidan si quieren o no actividades de extracción de minerales en su territorio.

Según Carlos Gómez, abogado promotor de la consulta, “Córdoba es el primer municipio en el país que ha consolidado esta iniciativa **a través del liderazgo del alcalde**”. Esto ha hecho que la consulta se haga por iniciativa administrativa reduciendo los tiempos en trámites como la aprobación de la pregunta en el consejo municipal.

La pregunta de la consulta la hicieron **tomando como referencia la que se hizo en Pijao “**porque sabíamos que íbamos a encontrar poca o ninguna resistencia por parte del tribunal administrativo en el control de legalidad y constitucionalidad”. Los cordobeses votarán dando respuesta a si están o no de acuerdo con la actividad minera de metales.

### **Las montañas de Córdoba surten de agua al 40% de la población colombiana** 

El objetivo con la realización de la consulta popular, es defender los recursos hídricos de un municipio que se abastece del agua que producen por ejemplo, las 6.192 hectáreas del páramo de Chilí y el agua del Río Verde. De afluentes como estos, según Gómez, “se nutre el 40% de la población nacional con agua potable, es decir **más de 18 millones de habitantes**”. (Le puede interesar: ["Puerto Leguízamo, Putumayo, arranca proceso de consulta popular"](https://archivo.contagioradio.com/puerto-leguizamo-putumayo-arranca-proceso-de-consulta-popular/))

Con esto en mente, los cordobeses **ya tienen listo el comité promotor de la consulta popular** “que reúne sectores cívicos, ambientalistas, culturales y políticos que han manifestado la intención de pelear por la defensa del territorio”.

### **El 94% de la extensión del territorio tiene títulos mineros** 

Gómez manifestó que entre los títulos otorgados y en espera de otorgamiento, se **encuentra comprometido casi el total de la extensión territorial**. Dijo que estos títulos no han respetado la ley de páramos y afectan a las zonas que deberían ser protegidas por su riqueza y diversidad. (Le puede interesar: ["Con una marcha festival en Pasca dicen "la consulta popular si va"](https://archivo.contagioradio.com/pasca-consulta-petrolera/))

Las empresas mineras con mayor presencia en el territorio quindiano **son Anglo Gold Ashanti y Oro Barracuda**. Estas, “han supuestamente renunciado a algunos títulos, sin embargo, son estrategias mineras para desviar la atención sobre la actividad que realizan”.

Dijo que los títulos que dejan de aceptar, **son solicitados por empresas mineras más pequeñas** y “en últimas tienen los mismos intereses de la Anglo Gold Ashanti, muy posiblemente son ellos los propietarios”.

Finalmente, Gómez manifestó que es posible que la consulta **se realice el 5 de noviembre de este año.** No hay una fecha exacta en la medida que hace falta que el alcalde del municipio firme y presente el decreto a la Registraduría, quien finalmente determinará en concreto con el municipio, la fecha de las votaciones.

<iframe id="audio_20907059" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_20907059_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)
