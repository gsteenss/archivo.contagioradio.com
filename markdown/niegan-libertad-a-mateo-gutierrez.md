Title: Niegan libertad a Mateo Gutiérrez
Date: 2017-04-25 12:32
Category: DDHH, Judicial
Tags: Falsos Positivos Judiciales, Mateo Gutierrez
Slug: niegan-libertad-a-mateo-gutierrez
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/03/mateo-gutierrez-1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo Familiar] 

###### [25 Abr 2017] 

La Juez 42 del circuito penal negó la libertad para Meteo Gutiérrez. De acuerdo con el abogado Eduardo Matías, esperaban que en la audiencia  el estudiante de sociología, quedará **en libertad por falta de pruebas que lo señalen como autor de actos terroristas y otros delitos que ameriten una medida de aseguramiento carcelario.**

Además, agregó que los hechos por los cuales se acusa a Gutiérrez, no constituyen a la luz del derecho penal colombiano ningún delito y, por el contrario, el abogado señala que demuestra una **“cacería de brujas” en momentos en que era necesario dar resultados sobre las explosiones ocurridas en la Macarena,** en Bogotá, el 20 de febrero de este año. Le puede interesar: ["Mateo Gutiérrez  León sería víctima de otro falso positivo judicial"](https://archivo.contagioradio.com/mateo-gutierrez-leon-seria-victima-de-falso-positivo-judicial/)

Sin embargo, los actos de los que es acusado Mateo Gutiérrez tienen que ver con la explosión de un artefacto panfletario el pasado 18 de septiembre de 2015, no obstante, la persona testigo que estaba en el apartamento cuando se puso la panfletaria, **señaló a un sujeto con características diferentes a las del estudiante. **

A su vez, Matías señaló que la lentitud con la que ha actuado la Fiscalía ha dificultado el proceder de la defensa, ya que a la fecha, el juez de conocimiento no tiene el proceso del estudiante debido a que **la Fiscalía no ha radicado el escrito de acusación y no se ha realizado una valorización de las pruebas.** Le puede interesar: ["Se conforma red de solidaridad en defensa de Mateo Gutiérrez"](https://archivo.contagioradio.com/red-solidaridad-defensa-mateo-gutierrez/)

<iframe id="audio_18339083" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_18339083_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
