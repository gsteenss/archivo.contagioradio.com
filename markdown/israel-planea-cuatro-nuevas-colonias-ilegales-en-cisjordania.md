Title: Israel planea cuatro nuevas colonias ilegales en Cisjordania
Date: 2015-02-10 20:42
Author: CtgAdm
Category: DDHH, El mundo
Tags: Cisjordania, Colonos israelíes, Gaza, Israel, Palestina
Slug: israel-planea-cuatro-nuevas-colonias-ilegales-en-cisjordania
Status: published

###### Foto:Palestinalibre.org 

Israel continua con su política **de asentamientos ilegales en Gaza y Cisjordania como parte del apartheid contra el pueblo palestino**, esta vez se trata de **cuatro** **nuevas colonias en Cisjordania**, territorio soberano desde los Acuerdos de Oslo en 1993, por los cuales Cisjordania y Gaza se convertían en territorios legítimos de Palestina.

Las **ocupaciones ilegales se planean sobre Kedumim, Vered Yericho, Neve Tzuf y Emanuel en una área de un total de 374 hectáreas.**

Estos son asentamientos ilegales ya existentes que Israel busca ampliar, desde que retomó su política de ocupación de territorios en 2011. Es importante recordar que actualmente viven solo en Cisjordania **531.00 colonos judíos**.

Las **colonias** ilegales principalmente se extienden por Cisjordania y los Altos del Golán territorios ocupados desde 1967, y que, en consonancia **con el derecho internacional, constituyen un crimen de guerra.**

Como han denunciado organizaciones de derechos humanos como B'Tselem estos territorios están militarizados y solo pueden acceder judíos y turistas de todo el mundo pero a los que no se les permite tener acceso o contacto con la población Palestina.
