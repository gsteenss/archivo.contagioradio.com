Title: Debate en el Ecléctico: Acuerdo sobre participación política
Date: 2016-09-12 16:07
Category: El Eclectico
Tags: Acuerdo participación en política, colombia, FARC, paz, Porceso de paz
Slug: debate-en-el-eclectico-acuerdo-sobre-participacion-politica
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/09/foto_0000000820131106181050-e1473713980726.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### [Foto: La Nación] 

<iframe id="audio_12877371" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_12877371_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

El senador **Iván Cepeda** afirmó que las FARC no podrán presentar candidatos para las 16 circunscripciones especiales para la paz que se crearán luego de la entrada en vigencia del acuerdo de paz. Ese pronunciamiento lo hizo durante el debate de El Ecléctico en el cual **Miguel Cetina**, estudiante de derecho de la Universidad Libre y miembro del Partido Conservador afirmó que las FARC podrían llegar a tener 26 curules. Cepeda fue enfático en señalar que esas son mentiras creadas desde la campaña del NO para desinformar a los colombianos.

En este debate también estuvieron presentes **Nicolás Ordóñez**, estudiante de Gobierno y Relaciones Internacionales de la Universidad Externado y miembro de la UTL del senador Álvaro Uribe, quien mostró su preocupación por el hecho de que las FARC hagan parte de la comisión encargada de diseñar los lineamientos del nuevo estatuto de la oposición.

Al respecto, **Nicolás Ramírez**, estudiante de Economía de la Javeriana y director del periódico EJE, y **Mateo Bermúdez**, estudiante de Ciencia Política del Rosario y cofundador del movimiento Hablemos de Paz UR, le contestaron a Ordóñez que es apenas lógico que las Farc tengan representación en ese organismo, pues su deber será garantizar que no haya persecución por razones de oposición política.

 
