Title: La lucha feminista sigue por la despenalización  del aborto en Colombia
Date: 2020-03-03 17:19
Author: AdminContagio
Category: DDHH
Slug: la-lucha-feminista-sigue-por-la-despenalizacion-del-aborto-en-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/03/ERpNHnhWAAYLjfA.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

*Foto: [@mesaporlavida](https://twitter.com/mesaporlavida)  
*

<!-- /wp:paragraph -->

<!-- wp:html -->  
<iframe id="audio_48621994" frameborder="0" allowfullscreen scrolling="no" height="200" style="border:1px solid #EEE; box-sizing:border-box; width:100%;" src="https://co.ivoox.com/es/player_ej_48621994_4_1.html?c1=ff6600"></iframe>  
<!-- /wp:html -->

<!-- wp:paragraph -->

El aborto y la despenalización total de este ha dividido al país desde inicio de 2020 , este 2 de marzo y luego de varios debates la Corte Suprema decidió este 4 de marzo mantener la legislación de las tres causales tal y como está.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Ante ello los movimiento feminitas en defensa de la Interripción Voluntaria del Embarazo (IVE) afirmaron que es necesario seguir avanzando en materia de derechos y evitar la interposición de barreras que obstaculicen este servicio. (Le puede interesar: <https://archivo.contagioradio.com/ive-el-derecho-a-decidir-en-paz/>)

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

*"Las organizaciones celebramos que se mantenga la decisión, sin embargo el siguiente paso es que el IVE sea un derecho de todas, y libre de cualquier causal"*, afirmó Ana María Méndez Jaramillo, integrante de La Mesa por la Vida y la Salud de las Mujeres.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El decreto se mantuvo luego de que Sala Plena presenciara una votación de seis contra tres, ante la ponencia presentada por el magistrado Alejandro Linares, quien solicitaba despenalizar totalmente el aborto hasta la semana 16 de gestación, esto en contra de la posición de la abogada Natalia Bernal quien solicitó penalizarlo en todas sus modalidades.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Ante ello la Corte Suprema se declaró la Corte se declaró inhibida , y afirmó que las demandas que originaron esta discusión no contaban con los requisitos mínimos, por lo que no podían hacer un nuevo pronunciamiento, manteniendo así el derecho de IVE bajo las tres causales ya existentes en el país.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Jaramillo afirmó que seguirán trabajando por la implementación efectiva de un marco legal con otras entidades como la Fiscalía, el Ministerio de Salud, entre otros, *"cuando se abre este debate hay mucha desinformación, nuestro trabajo sigue y está en difundir información para que las mujeres, sepan que esto es un derecho al que puedan acceder de manera legal y segura".*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Asimismo destacó que las barreras a este derecho se seguirán presentando, y a pesar de las tres causales actuales *"aún hay mujeres que sufren de violencia e incluso son denunciadas por los mismos prestadores de salud, en el marco de este proceso la Fiscalía dijo que **él 30% las mujeres penalizadas por aborto son víctimas de violencia previa**".*

<!-- /wp:paragraph -->
