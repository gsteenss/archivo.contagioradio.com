Title: What we should remember about Jaime Garzón 20 years after his death
Date: 2019-08-18 23:25
Author: CtgAdm
Category: English
Slug: what-we-should-remember-about-jaime-garzon-20-years-after-his-death
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/08/Jaime-Garzón.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Photo: @utcentroknight] 

[The 20th anniversary of Colombian humorist and journalist Jaime Garzón’s death was commemorated this past Tuesday with an event at the National University in Bogotá, during which Garzón’s sister, Marisol Garzón, called on everyone to keep his memory and his teachings alive. She also denounced that his death remains in impunity.]

### **“Let Jaime be the pretext to love this country again”**

[In an interview with Contagio Radio, Marisol Garzón said that her brother’s speeches from a conference at the Autonomous University in Cali to understand Jaime Garzón’s way of thinking. For example, Marisol Garzón said that her brother spoke of using dialogue to get through differences in opinions. “If dialogue is lost, you need to sit down again and talk,” she said.]

[One of the phrases that Jaime Garzón routinely used throughout his life was the indigenous Wayuu people’s translation of Article 12 of the Constitution into their language. It reads: “Nobody can walk over anyone’s heart nor hurt another even if they think or speak differently.” For Jaime Garzón, this phrase “would save the country.” Marisol Garzón said that her brother “needs to become a pretext to love this country again, to commit oneself to this country.”]

**What shouldn’t we forget about Jaime’s case?**

[The lawyer Alirio Uribe Muñoz, who has taken on Jaime Garzón’s case, said that the humorist’s murder should be remembered as a crime of the State. “This isn’t speculation,” Uribe Muñoz said. The lawyer recalled that the assassination of Jaime Garzón was part of an operation to silence his voice, organized by “far-right political groups in coordination with paramilitaries and specifically with Carlos Castaño.”]

[The lawyer underlined that Castaño, the former paramilitary leader, and José Miguel Narvaéz, the former government intelligence director, was already convicted for the crime. “This was an agreement between the military leadership and the \[United Self-Defense Forces of Colombia\], that is, there were orders from the generals to this paramilitary group to carry out joint operation of extermination and annihilation.” According to the Uribe Muñoz, there are militaries implicated in the case that have yet to be investigated.]

[Uribe Muñoz also said that Jaime Garzón should be remembered for his humanitarian acts, for example, his numerous interventions to help liberate hostages from guerrilla anti-kidnapping legislation took the hostage’s assets from the market to avoid them being commercialized and penalized anyone who helped arrange the hostage’s liberation.”]

 

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
