Title: Las FARC-EP transitarán a la vida civil a través del deporte, la cultura y la educación
Date: 2017-03-31 15:38
Category: Nacional, Paz
Tags: FARC-EP, implementación acuerdos de paz
Slug: las-farc-ep-transitaran-a-la-vida-civil-a-traves-del-deporte-la-cultura-y-la-educacion
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/09/X-Conferencia-FARC-EP.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [31 Mar 2017]

Podría haber un equipo de las FARC-EP en la **categoría B del futbol colombiano, los integrantes de esa guerrilla correrán en la media maratón de Bogotá en le mes de Julio** y los guerrilleros que quieran estudiar medicina podrán hacerlo si cumplen los requisitos, luego de realizado el censo socioeconómico que determinará las posibilidades de cada uno de ellos, todo ello como parte de los Acuerdos de Paz en el tránsito y reincorporación a la vida civil.

Esto sería posible como parte de los p**rocesos de reincorporación de los integrantes de esta guerrilla, pactados con urgencia en el concilio de Cartagena** en donde entraron otros temas como las garantías de seguridad, la urgencia del desarrollo legislativo,el tránsito a la legalidad y la mejoría de las condiciones en las que se encuentran las zonas veredales. Le puede interesar: ["Abril es el plazo para las tareas pendientes en implementación de acuerdos de paz"](https://archivo.contagioradio.com/abril-el-mes-para-completar-la-implementacion-de-los-acuerdos-de-paz/)

En materia de tránsito a la legalidad, las FARC se comprometió **a entregar el listado de todas las personas que conforman esta guerrilla, de manera que el Gobierno deba dar todo el apoyo para llevar a cabo las amnistías**, muchas de ellas retrasadas hasta la fecha e iniciar con los procesos pedagógicos que darán paso a la reincorporación de los guerrilleros a la sociedad civil.

En este sentido, 300 integrantes de las FARC-EP empezaran su capacitación para ser escoltas, **mientras la Universidad Nacional avanza en la estructuración del censo socioeconómico que determinará que profesiones** adoptaran los integrantes de esta guerrilla y cómo se podrá dar esa formación académica, que ha sido apoyada por las universidades públicas del país. Le puede interesar: ["Abraza la paz, una iniciativa de los estudiantes de Colombia para la reconciliación"](https://archivo.contagioradio.com/abraza-la-paz-una-iniciativa-de-los-estudiantes-de-colombia-para-la-reconciliacion/)

De igual forma en el tránsito a la legalidad las FARC-EP deberá decidir la forma que adoptará su partido, **consenso al que se llegará en el congreso constitutivo que se realizará en el mes de agosto**, frente a este tema Jesús Santrich, integrante del Secretariado General de las FARC, señaló que ese es un “proceso complejo en donde la organización entra a hacer la construcción de un partido democrático, que necesita escuchar muchas voces”.

De otro lado Santrich, afirmó frente a los riesgos que aún rondan la implementación de los acuerdos de paz que, “**el tránsito de la guerra a la paz, es un salto histórico que debe tener problemas, porque siempre cabe la posibilidad de que se presenten dificultades**”, sin embargo, al mismo tiempo considera que el problema que más les afana es el fenómeno del paramilitarismo y la puesta en marcha de la Unidad del desmonte de este tipo de grupos armados.

###### Reciba toda la información de Contagio Radio en [[su correo]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio]{.s1}](http://bit.ly/1ICYhVU)[.]{.s1}

<div class="ssba ssba-wrap">

</div>
