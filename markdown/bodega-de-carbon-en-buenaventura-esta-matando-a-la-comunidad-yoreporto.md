Title: Bodegas de carbón en Buenaventura "están matando a la comunidad" #YoReporto
Date: 2017-08-07 10:05
Category: Nacional, yoreporto
Tags: afecciones de salud, buenaventura, carbón, mina de carbón, mina de carbón a cielo abierto
Slug: bodega-de-carbon-en-buenaventura-esta-matando-a-la-comunidad-yoreporto
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/08/IMG_9427.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Alfonso Ríos ] 

###### [04 Ago. 2017]

Desde la llegada de las bodegas de almacenamiento de carbón a Buenaventura, las afectaciones a la salud de los habitantes de los barrios Nayita y Mayolo, los más cercanos a las bodegas, siguen en aumento. Problemas respiratorios como la tuberculosis afectan cada día a más personas, fundamentalmente a niños y mayores.

La bodega de almacenamiento de carbón ubicada junto a estos dos barrios se encuentra a cielo abierto, un incumplimiento legal de la empresa gestora de la planta de carbón, Inatlantic, que debería haber cubierto el espacio para impedir que el polvo llegue a las casas de los habitantes de la zona.

### **"Esto nos está matando poco a poco" Líderes del sector** 

Cleómenes Germán Matacea, vicepresidente de la Junta de Acción Comunal de Nayita nos acompañó en una visita al barrio para conocer las afecciones más importantes que sufren los vecinos en la planta. “Esto nos está matando poco a poco. Hay un aumento de enfermedades respiratorias, decesos en niños, problemas con el cáncer. Sobre todo están surgiendo problemas en mayores y pequeños”, asegura el líder local.

Según reportaba [El país](http://www.elpais.com.co/valle/cvc-suspende-almacenamiento-de-carbon-en-bodegas-de-buenaventura.html), en 2014, la Corporación Autónoma Regional del Valle del Cauca- CVC- suspendió el almacenamiento de carbón en las bodegas por la vulneración del derecho a la salud. Sin embargo, el carbón sigue acumulándose en el mismo lugar y de la misma manera.

El barrio no desiste y sigue reclamando la atención de los medios para visibilizar su situación. “Hemos acudido tres veces a la prensa para denunciar y no emiten nada. No quieren que la gente se dé cuenta de lo que está pasando aquí”, afirma Matacea. “Las empresas multinacionales del carbón nos están opacando y no permiten que demos cuenta de esto que está pasando con nosotros”.

### **"Las empresas comprometidas y los compromisos incumplidos"** 

El carbón está gestionado por una de las empresas del Grupo Portuario, Inatlantic, que también es parte de Ventura Group. Según aseguran en su propia página web ofrecen servicio integral para sus clientes del sector minero. La Fundación Etikaverde, creada por Ventura Group como entidad de Responsabilidad Social Empresarial -RSE-, asegura en una de sus publicaciones que van a establecer ayudas para las comunidades de Nayita y Mayolo, pero ninguna mención al cubrimiento de la bodega. “Nosotros tenemos entendido que el carbón no puede estar tan cerca de la comunidad y así en estas condiciones”, confirma el líder de la comunidad.

El barrio ya estaba asentado mucho antes de que llegara el carbón, por lo que los vecinos piden que se cambie de ubicación. “Desde que llegó hemos estado peleando con el carbón. La comunidad ha puesto una demanda colectiva contra la empresa para que retiren el carbón.”

La única respuesta de las multinacionales ha sido minimizar el impacto con un arreglo en la pared que separa la bodega de las casas, añadiendo una malla más alta que supone un peligro porque corre el riesgo de caer.

Tampoco los representantes políticos toman cartas en el asunto a pesar de la gravedad: “La alcaldía hace oídos sordos ante nuestras denuncias. Ellos arriendan el terreno y les da igual que los vecinos estemos aquí”, afirma Matacea.

Inicialmente, cuando llegó el carbón, las empresas que gestionan la bodega ofrecieron empleo a muchos de los vecinos del barrio, quienes estuvieron trabajando durante algún tiempo. Después han ido dejando esos empleos por los perjuicios que genera en el barrio. “Las partículas de carbón están en todos los lados: en la ropa, la piel, el agua… Estamos cocinando y comiendo carbón”.

###  **Múltiples afecciones para la salud** 

Los habitantes de Nayita y el barrio colindante Mayolo son los más afectados por el carbón ya que son los más cercanos a la bodega.

“El carbón bota sus partículas diminutas y se van a los pulmones de las personas. Trajeron unas máquinas para moler el carbón y esas máquinas soltaban más polvo que se iba a los pulmones de los niños, hay muchos niños afectados de la respiración. El cuero le pica a uno porque la ropa tiene carbón”, nos asegura una de las vecinas del barrio de Mayolo. Y continúa: “la asfixia no se le quita a los muchachos. La gripa tampoco se quita. Nosotros estamos al pie del carbón”.

Otra vecina del barrio de Mayolo explica que una de las principales afecciones que están sufriendo es la tuberculosis. Los médicos han alertado ya a los vecinos de numerosos casos en la zona y les están ofreciendo algunos consejos para evitar el contagio. “Aunque pongan vallas o muros más fuertes ahora no va a haber solución a ese problema. El carbón siempre va a buscar su salida”, explica.

Pero la retirada del carbón no es la única petición de la comunidad. Después de años de presencia de la bodega a cielo abierto, las afecciones son muchas y exigen una intervención mayor: “Además de la demanda de retirar el carbón, creemos que se debe hacer una reparación en la salud de las personas por los daños sufridos. El Gobierno y la Alcaldía no han ofrecido una campaña de prevención a la comunidad”.

Sumado a las afectaciones directas que tiene el carbón sobre la salud de las personas, el movimiento del carbón dentro de la bodega ha llegado a causar también el bloqueo del sistema de alcantarillado, que se suma a la difícil situación que viven los vecinos.

### **El carbón, un motivo más del paro cívico en Buenaventura** 

El de Buenaventura es el principal puerto del país, por ahí entra el 70% de las exportaciones. “Supuestamente somos Distrito Especial, pero no entendemos lo que significa. Estamos abandonados, por ejemplo tenemos agua una o dos horas al día como máximo.”, afirma Cleómenes Germán Matacea.

El problema con las bodegas de carbón a cielo abierto junto a los dos barrios bonaverenses se convirtió también en elemento central de las comunidades durante el paro cívico.

“El carbón era uno de los asuntos básicos del paro. Creemos que deben enviar el carbón a otra bodega lejos de donde estén las familias y los niños. Creemos que legalmente el carbón no debe estar almacenado sin cubrimiento porque las micropartículas pasan.”

 Rodrigo Dominguez/Contagio Radio
