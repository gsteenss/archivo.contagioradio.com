Title: La 'brillante' idea del Gobierno: cortar la luz para reducir los cultivos de marihuana
Date: 2019-07-03 14:58
Author: CtgAdm
Category: Comunidad, Nacional
Tags: Cauca, coca, Marihuana, PNIS
Slug: cortar-la-luz-cultivos-marihuana
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/Periódico-La-última.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Periodico La Última  
] 

A partir del 2 de julio inció el proceso para cortar la luz, anunciado por la Fiscalía en Miranda, Toribío, Caloto y Corinto (norte de Cauca), para afectar los cultivos de marihuana en ese territorio. No obstante, **cultivadores y pobladores de la región han advertido que la medida será inútil** y afectará a los pobladores de la zona que no son cultivadores de esta planta.

**Leider Valencia, vocero nacional de la Coordinadora Nacional de Cultivadores de Coca, Amapola y marihuana (COCCAM),** recordó que los cortes se realizarán en 18 veredas de los cuatro municipios mencionados al norte del Cauca; y la Compañía Energética de Occidente (CEO), empresa encargada de suminsitrar la energía a estos sectores, ha anunciado que en caso de no lograr ingresar a los territorios para cortar la luz a los invernaderos, tendrá que hacer desconexiones generales.

### **"El Gobierno dice que la solución es cortar la luz, pero no"**

**La idea de cortar la luz a los invernaderos que producen marihuana la presentó el exfiscal Néstor Humberto Martínez en los meses finales de 2018,** como una forma de controlar estos cultivos, que en muchos casos tienen conexiones ilegales al servicio de electricidad . Como explicó Valencia, la luz se usa durante unos 20 días para que la planta crezca "porque esto no es marihuana como la de antes, sino que son semillas patentes y necesitan energía para su crecimiento".

Valencia señaló que en otros momentos también hubo cortes de luz con la misma intención expresada por Martínez Neira, pero los cultivadores acudieron a las plantas con gasolina para seguir teniendo energía eléctrica. En cambio, los cortes sí generaron problemas para los comerciantes, por ejemplo, aquellos que tienen que hacer uso de las neveras para mantener productos refrigerados como la carne . (Le puede interesar: ["Cultivadores de Coca llevan seis meses esperando una reunión con el Gobierno"](https://archivo.contagioradio.com/cultivadores-coca-gobierno/))

Como lo declaró el Vocero de la COCCAM, "el Gobierno dice que la solución es cortar la energía, pero no, eso empeora la situación, porque la gente sale a exigir sus derechos y terminan las cosas en vías de hechos". Otra propuesta del Exfiscal fue limitar la cantidad de combustible que se vende en algunos departamentos como Cauca o Putumayo, para evitar el funcionamiento de plantas eléctricas que funcionan con gasolina, pero los cultivadores alegan que se necesitan soluciones de fondo para resolver la cuestión de los cultivos de uso ilícito.

### **Que se implemente el Acuerdo de Paz y lo consignado en el PNIS** 

Valencia indicó que en Cauca los cultivadores han buscado entrar al **Plan Nacional Integral de Sustitución (PNIS)**, "pero el Gobierno en este momento no tiene una ruta establecida para los cultivadores de marihuana", así, aunque ellos quieran cambiar los cultivos, no tienen otra alternativa de subsitencia económica para su familias. En ese sentido, el integrante de COCCAM pidió que se implemente el Acuerdo de Paz, específicamente en el punto cuatro ("Solución al problema de las drogas ilícitas") y se avance en el desarrollo del PNIS para cultivadores de marihuana.

### **En reunión, la CEO dijo que ellos solo querían que les pagaran por la energía**

La medida de suspensión de electricidad fue una sugerencia hecha por la Fiscalía a CEO, que decidió aceptar la propuesta. Fue así como inició reuniones con integrantes de las Juntas de Acción Comunal de los cuatro municipios mencionados al norte de Cauca para socializar la idea; en una de esas reuniones, quienes asistieron, le comentaron a Valencia que la Compañia le dijo a la comunidad que lo que les interesa es que se pongan al día y paguen la energía: "ellos han dicho que no les interesa para qué utilizan la energía, sino que paguen", concluyó.

Hasta el momento, Valencia aseguró que a principios de junio se presentaron algunos cortes, pero desde que entró en operación la medida (julio 2) no se han vuelto a presentar y el servicio se está prestando con normalidad; **situación que esperan que se mantenga, para evitar vulneraciones a los derechos de las comunidades.** (Le puede interesar: ["El regreso del Glifosato, una amenaza para campesinos en zonas cocaleras"](https://archivo.contagioradio.com/el-regreso-del-glifosato-una-amenaza-para-campesinos-en-zonas-cocaleras/))

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

<iframe id="audio_37975891" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_37975891_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
