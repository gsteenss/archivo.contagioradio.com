Title: Lideres indígenas, blanco de asesinatos y amenazas
Date: 2016-12-03 12:34
Category: DDHH
Tags: Amenazas, lideres sociales, macha patriótica
Slug: lider-indigena-del-cauca-amenzado-por-los-urabenos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/05/Indígenas-del-Norte-del-Cauca-e1464380469118.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Los Mundos de Hachero, blog 

###### [3 Dic 2016] 

Los líderes indígenas de diferentes partes de Colombia continúan siendo víctimas de [asesinatos y amenazas](https://archivo.contagioradio.com/tras-firma-del-acuerdo-se-han-agudizado-actos-violentos-contra-lideres/) en su contra. De acuerdo con las denuncias por parte de organismos defensores de derechos humanos, este jueves **dos líderes indígenas han sido víctimas de actos violentos. Uno fue asesinado, y otro amenazado.**

El Gobierno Mayor Indígena Nasa en el sur del Tolima, denunció el asesinato del **Mayor Nasa Diomedes Perdomo**, quien fue encontrado en su domicilio por una comunera que fue a su casa. El cabildo y la comunidad manifiesta que se desconoce los autores de los hechos y no se conocen detalles sobre la forma cómo pudo haber sido asesinado.

### **Continúan amenazas contra Marcha Patriótica** 

Por otra parte, en el departamento del Cauca, **Asnoraldo Corpus, líder indígena y Presidente de la Junta de Acción Comunal de la vereda La Mina,** fue víctima de una amenaza este miércoles, por parte del grupo paramilitar autodenominado “Los Urabeños” en inmediaciones a uno de los puntos de Preagrupamiento Temporal de integrantes de la guerrilla de las FARC ubicado en la vereda Monterredondo.

De acuerdo con una denuncia de la Red de Derechos Humanos del Suroccidente Colombiano “Francisco Isaías Cifuentes”, esta estructura paramilitar ha intimidado, extorsionado y amenazado al vocero indígena y a otros habitantes de zona rural, algunos de ellos integrantes de la Asociación Pro Constitución de Zona de Reserva Campesina del Municipio de Miranda.

Estas situaciones se producen en inmediación de una base Militar ubicada en la Vereda Calandaima y de áreas de **operaciones Militares de la Brigada Móvil No. 17 y del batallón de alta montaña No. 8** “Coronel José María Vezga” adscritos operacionalmente a la Fuerza de Tarea Conjunta Apolo y la III División del Ejército Nacional.

A” las 12:35 de la tarde, cuando el señor Asnoraldo Corpus se encontraba trabajando recibe una llamada telefónica desde el número celular 3127310149. Al contestar la llamada un hombre le dijo que “este pendiente porque vamos a hacer una reunión en la zona y necesitaban que usted nos ayude”. Por lo cual Asnoraldo les pregunto: con quien hablo y le respondieron “**Es de parte de los Urabeños.** Nosotros vamos a operar en la zona de Florida, Pradera, Padilla, Corinto y Miranda. Como ahora no va a haber guerrilla nosotros necesitamos un apoyo de 20 camuflados y el costo de cada uno es de \$ 100.000”. Sabemos que en la zona hay ladrones, informantes y mucho colaborador de la guerrilla, por lo cual en estos días vamos a hacer un plan pistola y si usted no colabora tiene que desalojar la zona”.

Finalmente, le dijeron “ya sabemos quién es usted y quien opera por ahí”. Luego le pidieron que no dijera nada “si ha visto algo no diga nada, vamos a estar por ahí y nos vamos a estar comunicando”.  Ante la amenaza, la organización puntualmente señalan la responsabilidad de el Brigadier General Luis Fernando Rojas Espinoza** **Comandante de la III División del Ejército Nacional, al Coronel** **Leonardo Valderrama Ortiz** **comandante (E) de Fuerza de Tarea Conjunta Apolo del Ejército y al Coronel Edgar Orlando Rodríguez Castrillón Comandante del Departamento de Policía Cauca.

Ante estos hechos las organizaciones sociales hacen el llamado al gobierno y a la comunidad internacional para que se tomen las acciones necesarias de protección de la vida de este líder social y del resto de los integrantes de la organización.

###### Reciba toda la información de Contagio Radio en [[su correo]
