Title: Primer ciclo cinematográfico MOTETE
Date: 2020-10-21 16:11
Author: AdminContagio
Category: eventos
Tags: ciclo de cine, Cine, corporacion cultural, eventos, motete
Slug: primer-ciclo-cinematografico-motete
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/10/C8wu-acXUAA5GxZ.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/10/tumblr_inline_oo6hruq6fh1tmqv1d_1280.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/10/tumblr_inline_oo6hruq6fh1tmqv1d_1280-1.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/10/C8wu-acXUAA5GxZ-1.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:heading {"level":4} -->

#### Iguales y diversos

<!-- /wp:heading -->

<!-- wp:paragraph -->

La [Corporación Educativa y Cultural Motete](https://nuestromotete.com/), organización Chocoana sin ánimo de lucro dedicada desde su nacimiento en mayo de 2016 a promover la lectura, la escritura y en general la cultura y las artes como herramientas esenciales para desarrollar el pensamiento autónomo y crítico en el Chocó, organiza la primera edición del Motete Cinematográfico, un Ciclo de Cine Afrocolombiano que estará albergado digitalmente en la plataforma [Mowies](https://www.mowies.com/categories) durante un mes, iniciando el próximo jueves 22 de octubre.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":5} -->

#####   
En esta ocasión la temática del ciclo será **Iguales y Diversos** y se exhibirán 9 obras de diferentes formatos, que rompen con la uniformidad de la narrativa tradicional, mostrando miradas cruzadas que presentan las sutiles diferencias entre los pueblos afrocolombianos, sus identidades, discursos y tradiciones.

<!-- /wp:heading -->

<!-- wp:paragraph -->

  
La selección está compuesta por los **documentales biográficos**

<!-- /wp:paragraph -->

<!-- wp:list -->

-   **Busca por Dentro** -sobre Jairo Varela, fundador del Grupo Niche.

<!-- /wp:list -->

<!-- wp:list -->

-   **El Hombre Universal** -sobre el gran escritor chocoano, Arnoldo Palacios.

<!-- /wp:list -->

<!-- wp:paragraph -->

También por historias como:

<!-- /wp:paragraph -->

<!-- wp:list -->

-   **Keyla** -el primer largometraje realizado en la Isla de Providencia

<!-- /wp:list -->

<!-- wp:list -->

-   **Divinas Melodías** -Un hermoso homenaje a la marimba de chonta, la música y la sabiduría del Chocó

<!-- /wp:list -->

<!-- wp:list -->

-   **Polifonía** -un emotivo documental que junta las voces y tradiciones de mujeres jóvenes y mayoras del Norte del Cauca.

<!-- /wp:list -->

<!-- wp:paragraph -->

Por los cortometrajes:

<!-- /wp:paragraph -->

<!-- wp:list -->

-   **Tumaco** -la historia de dos pescadores discapacitados-

<!-- /wp:list -->

<!-- wp:list -->

-   **Dulce** -un bellísimo retrato de la maternidad y la relación con la naturaleza en la costa pacífica nariñense-

<!-- /wp:list -->

<!-- wp:list -->

-   **Palenque** -un viaje al primer pueblo libre de América Latina

<!-- /wp:list -->

<!-- wp:list -->

-   **Carreras en el Aire** -el primer trabajo del talentoso director David Paredes, parte del colectivo Puerto Creativo de Buenaventura.

<!-- /wp:list -->

<!-- wp:paragraph -->

Para Motete y el equipo curatorial de Motete Cinematográfico, el cine tiene un lugar muy importante como herramienta de transformación cultural y social, por esto, se han puesto en la tarea de impulsarlo como una forma de expresión independiente, que cruza fronteras étnicas, geográficas, políticas, culturales y sociales, abriendo así nuevos canales de expresión, reflexión y conversación. Por esto, la selección reúne obras que abordan temáticas importantes para las discusiones actuales en torno al racismo.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":5} -->

##### Aportando a esa transformación desde el reconocimiento de las formas de mirar y ser visto, y la exploración de la cotidianidad, la estética y la representación afrocolombiana.

<!-- /wp:heading -->

<!-- wp:paragraph -->

El Motete Cinematográfico tendrá un valor de **\$40.000** pesos y se presenta como una oportunidad para que el público se ponga en contacto con nuevas historias que le permitan reconocer la diferencia, aportando para que realizadores, protagonistas y comunidades partícipes de las diferentes películas moneticen sus creaciones y aseguren un ingreso económico que les permita continuar con su labor en medio del difícil contexto actual.

<!-- /wp:paragraph -->

<!-- wp:image {"align":"left","id":91783,"width":241,"height":86,"sizeSlug":"large","className":"is-style-default"} -->

<div class="wp-block-image is-style-default">

<figure class="alignleft size-large is-resized">
![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/10/tumblr_inline_oo6hruq6fh1tmqv1d_1280-1-1024x366.jpg){.wp-image-91783 width="241" height="86"}
</figure>

</div>

<!-- /wp:image -->

<!-- wp:paragraph -->

[Vea mas eventos culturales aquí](https://archivo.contagioradio.com/categoria/eventos/)

<!-- /wp:paragraph -->
