Title: "La paz en Colombia pende de un hilo" Caravana Internacional de Juristas
Date: 2018-09-07 14:09
Author: AdminContagio
Category: DDHH, Judicial, Nacional, Paz
Tags: Caravana Internacional de Juristas, colombia, JEP, paz
Slug: paz-en-colombia-pende-de-un-hilo
Status: published

###### [Foto:] 

###### [6 Sept 2018] 

La Caravana Internacional de Juristas visitó esta semana 7 departamentos en una misión investigativa donde recopilaron datos sobre denuncias hechas por diversas comunidades en cuanto al aumento de la violencia y los asesinatos de líderes sociales, concluyendo que "la paz parece pender de un hilo" en el país.

### **Los departamentos siguen en alerta roja por aumento de la **[**violencia**] 

La caravana visito los departamentos de Antioquia, Nariño, Santander, Norte de Santander, Valle del Cauca y el Caribe en donde se reunieron con diversas organizaciones defensoras y colectivos de los territorios para tratar el tema de la efectividad del Acuerdo de Paz en cuanto al aumento de la violencia y el asesinato de líderes sociales.

En Antioquia, uno de los departamentos más afectados por el conflicto armado, la Caravana observó que uno de los mayores inconvenientes para la implementación de lo pactado en el Acuerdo de Paz es la **falta de recursos asignados por el Gobierno**. Adicional a esto, tampoco se poseen los fondos suficientes el correcto desarrollo de la Unidad Nacional de Protección, organismo designado a apoyar a defensores y abogados en situación de riesgo. ( Le puede interesar: [Caravana de Juristas constata 54 asesinatos en Norte de Santander y Cauca en los últimos meses](https://archivo.contagioradio.com/continua-riesgo-para-defensores-de-derechos-humanos-y-victimas-en-colombia-caravana-de-juristas/))

Además,  los líderes de esta región están en mayor riesgo debido a la falta de presencia del Estado en muchas zonas, que actualmente se encuentran aisladas y se convierten en **campos de batalla para grupos armados** que se disputan el control territorial.

En el Caribe, la Caravana aseguró que la mayoría de las amenazas son dirigidas a abogados, jueces y líderes que toman los casos de **restitución de tierras**. De otro lado, en Nariño, se vio el notable crecimiento y **multiplicación de los grupos armados** como consecuencia de la falta de presencia Estatal y la desmovilización de la entonces guerrilla FARC-EP, que tenía mayor presencia en este territorio.

En Santander y Norte Santander los defensores de derechos manifestaron su sensación de **abandono sistemático por parte del Gobierno**. Valeria Verdolini, participante en la Caravana, se mostró preocupada por la situación en estas regiones “deben respetarse todos los derechos y la reducción de la desigualdad, esto es lo que falta en los contextos rurales como el Catatumbo, donde la violencia aumento de forma considerable con el notable aumento de presencia de grupos paramilitares en la zona. (Le puede interesar: [Aumentar pie de fuerza en el Catatumbo no resolverá crisis humanitaria: ASCAMCAT](https://archivo.contagioradio.com/aumentar-pie-de-fuerza-el-catatumbo-no-resolvera-crisis-humanitaria-ascamcat/))

Finalmente en el Valle del Cauca la población afirmó que para su comunidad **el conflicto no ha acabado, simplemente ha entrado a nueva etapa,** en donde el terror es la presencia de grupos armados mucho más amenazantes y sin temor a asesinar a quienes se interpongan en sus planes de control.

Con todos los datos recolectados, este grupo realizará un informe que se presentará a la comunidad internacional, y en el que se evidenciarán temáticas como el **cambio que se denota en el tratamiento de los casos de amenazas y muerte** de defensores de derechos en el gobierno entrante.

**¿Hubo** [**beneficios**]** para estas comunidades luego de la firma del Acuerdo?**

Charlotte Gill, presidenta del proyecto, afirma que ha sido difícil ver cómo ha empeorado la situación con respecto al 2016, “hace dos años fuimos a Tumaco, pero este año nuestras guías colombianas no quisieron ir por que les preocupaba nuestra seguridad, esto demuestra cómo **ha aumentado en sobre medida el conflicto después del Tratado de Paz**”. (Le puede interesar: [Ni conflicto ni paz forman parte del lenguaje del Gobierno de Duque](https://archivo.contagioradio.com/ni-conflicto-ni-paz-forman-parte-del-lenguaje-del-gobierno-duque-antonio-madariaga/))

Por otro lado, Valeria Verdolini, investigadora en sociología jurídica de la Universidad de Milán, afirma que el proceso de paz efectivamente se está concretando, pero aún falta mucha atención por parte del Estado, tomando en cuenta que no se está realizando un acompañamiento a las comunidades en este periodo de transición "**El tratado es un proceso, no va a cambiar de un día para otro**, pero para las comunidades, la frustración que les causa comparar entre perspectiva y realidad es muy fuerte”.

**Expectativas de la Caravana**

Los abogados estuvieron de acuerdo en que se debe** **plantear una política de prevención para evitar que las muertes de líderes sociales sigan ocurriendo, y que brindarán apoyo Al Gobierno en el desarrollo de un proyecto que incluya la **protección del líder en todo el proceso, y no solo después de su asesinato.**

Sara Chandler, ex presidenta de la Federación de los Colegios de Abogados de Europa manifestó si preocupación y se refirió a la importancia de la creación de una política preventiva “**No podemos esperar a las medidas de protección de 3 meses por que están amenazando y matando a los líderes en 3 días**, hay que tener un enfoque que de verdad pueda proteger a las comunidades; quiero escuchar del Gobierno que ellos tienen la voluntad para proteger un estado que merece la paz. Debemos empezar a **buscar a los autores de las amenazas, en vez de posteriormente tener que buscar a los actores de los homicidios**”.

Finalmente, Gustavo Salas Rodríguez, de la Unión Internacional de Abogados, opinó que, aunque el Gobierno de Duque no tenga el mismo nivel de apoyo que el ex Presidente Santos en temas de dialogo y paz, **igualmente están en deber de cumplir el Acuerdo,** e hizo un llamado a las grandes ciudades del país para crear conciencia sobre la gravedad de lo que está sucediendo en el país, “**en muchas regiones el conflicto está aumentando, afuera de los centros urbanos, hay un conflicto que sigue matando a la gente”.**

###### [Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU).] 
