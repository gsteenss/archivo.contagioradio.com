Title: Animales, los más afectados con el derrame de petróleo en Barrancabermeja
Date: 2018-03-26 13:24
Category: Ambiente, Nacional
Tags: barranacabermeja, derrame de petróleo, Ecopetrol, jaguar, manatíes, pozo lizama 158
Slug: animales-los-mas-afectados-con-el-derrame-de-petroleo-en-barrancabermeja
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/03/WhatsApp-Image-2018-03-26-at-12.01.58-PM-e1522083861866.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [26 Mar 2018] 

[Luego de 24 días de emergencia ambiental en Barrancabermeja, debido al derrame de petróleo que ocasionó el pozo Lizama 158 de Ecopetrol, defensores del ambiente y habitantes del territorio argumentaron que las afectaciones a la fauna y la flora son incalculables, tal y como lo reflejan las imagines que han sido publicadas a través de redes sociales en donde se observan especies muertas o cubiertas por completo en crudo. ]

[De acuerdo con Oscar Sampayo, defensor de los derechos del ambiente y habitante de Barrancabermeja, luego de hacer un recorrido por los diferentes puestos de control de Ecopetrol “evidenciamos que la situación continúa y aún persiste la gran mancha de petróleo”. Dijo que en el último puesto de control, cercano a la desembocadura del río Sogamoso “la situación sigue igual al primer día de emergencia y el afloramiento de crudo persiste”.]

### **Jaguar americano y manatíes antillanos están en riesgo** 

[Afirmó que el área entre San Vicente y Barrancabermeja “hace parte del corredor del Jaguar Americano”. Por esto, es probable que la quebrada la Lizama donde ocurrió el derrame, sea uno de los lugares por donde ésta especie transitaba. Para los ambientalistas, ya no será posible volver a ver a este mamífero transitar por allí.]

[Advirtió que el ecosistema en el que habita el Jaguar Americano, que es una especie insignia del continente, se ha debilitado por las actividades de extracción petrolera y minera por lo que **“preocupa que esta tragedia pueda acelerar la desaparición del ecosistema y de la especie”**. Indicó que se debe hacer todo lo posible por proteger a estas especies que se encuentran en peligro.  (Le puede interesar:["Afectaciones del derrame de petróleo podrían durar 30 años"](https://archivo.contagioradio.com/afectaciones-del-derrame-de-petroleo-en-santander-podrian-durar-30-anos/))]

[De igual manera, hay afectaciones en las especies de manatíes que habitan en la Ciénaga del Llanito teniendo en cuenta que el río Sogamoso llega allí. El ambientalista recalcó que “la familia de 8 manatíes antillanos está en vía de extinción y en los últimos dos años se han muerto dos ejemplares”. Los campesinos han manifestado que en la ciénaga hay una familia de alrededor de 30 manatíes pero no existe un inventario de cuántos viven en el Magdalena Medio.]

### **Afectaciones al río Magdalena deben ser evaluadas** 

Además afirmó que las acciones de Ecopetrol no han sido suficientes para detener el afloramiento de crudo. La destrucción de los ecosistemas con petróleo que, se desprenden del cuerpo hídrico de la quebrada la Lizama y que pasa por Caño Muerto y  el río Sogamoso, está también afectando y llegando a la desembocadura del río Magdalena.

Por esto, los ambientalistas están realizando un inventario de las afectaciones de los ecosistemas teniendo en cuenta que **“hay afectaciones en el Magdalena que a simple vista no se ven**”. En este sentido, se han tenido que acercar a las playas para revisar las afectaciones pues “las contenciones paran la mancha pero los tóxicos y contaminantes llegan al Magdalena”. (Le puede interesar:["En San Martín se contaminó el agua luego del inicio del Fracking"](https://archivo.contagioradio.com/en-san-martin-se-contamino-el-agua-luego-del-inicio-del-fracking/))

Sampayo recordó que la zona donde ocurrió el desastre,hace parte de una área protegida que es el Distrito Regional de Manejo Integrado de la Ciénaga de San Silvestre “que es la zona de amortiguación de la cual los barranqueños consumimos agua”.

[Finalmente, Sampayo indicó que las afectaciones a la flora también son incalculables y en poco tiempo el ecosistema que rodea el sitio de la afectación va a sufrir consecuencias irreparables. Frente a los pronunciamientos de Ecopetrol, quien informó que en dos semanas se sellaría el hueco por donde se está presentando el afloramiento de crudo, el ambientalista afirmó que se debe tratar la emergencia como un desastre nacional y que la espera sólo agrava la emergencia.]

<iframe id="audio_24812723" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_24812723_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 

<div class="osd-sms-wrapper">

</div>
