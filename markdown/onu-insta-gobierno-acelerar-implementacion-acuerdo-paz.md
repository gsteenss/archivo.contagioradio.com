Title: ONU insta a que Gobierno acelere implementación de los Acuerdos
Date: 2019-03-19 17:46
Category: DDHH, Paz
Tags: Alto Comisionado de las Naciones Unidas para los Derechos Humanos, asesinato de líderes sociales, impunidad, Naciones Unidas
Slug: onu-insta-gobierno-acelerar-implementacion-acuerdo-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/12/derechos-humanos.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Caraota Digital] 

###### [15 Mar 2019] 

[El informe anual del Alto Comisionado de las Naciones Unidas para los Derechos Humanos (ACNUDH) presentó un panorama complicado para el avance de la democracia, la seguridad, el desarrollo, la participación cívica y la paz en Colombia. Si bien se destaca un incremento en la participación política y progreso para la lucha en contra de la pobreza, los altos índices de** impunidad, desigualdad, corrupción y violencia** fueron señalados como impedimentos para el goce efectivo de los derechos humanos en el país.]

En particular, el Alto Comisionado expresó preocupación por el aumento de agresiones en contra de los líderes sociales. En el 2018, el organismo internacional registró el asesinato de **110 defensores de derechos humanos** en 24 departamentos del país. Las regiones más afectadas son las zonas rurales, particularmente en **Antioquia, Cauca y Norte de Santander**, donde fueron registrados 37 % del total de casos.

Los presuntos autores de estos delitos fueron principalmente miembros de organizaciones criminales, entre cuyos integrantes se encuentran posiblemente antiguos miembros de grupos paramilitares desmovilizados (40%), individuos no afiliados a ningún grupo criminal o armado ilegal (18%), integrantes del ELN (8%), integrantes del EPL (4%), miembros de la fuerza pública (5%), antiguos miembros de las FARC-EP que no se acogieron al proceso de paz (8%) y personas por determinar (17%).

En los mismos departamentos donde se concentra el asesinato de líderes sociales, también se registró el mayor número de masacres en el país. En 2018, las **masacres incrementaron en un 164%** respecto a 2017. Además, **en casi el 50 % de los municipios aumentó la tasa de homicidios**. Al respecto, Naciones Unidas (ONU) indica que la gran mayoría de estos crímenes quedan sin resolver pues la tasa de impunidad en los casos de homicidios dolosos oscila entre el 86,58% y el 94,30%.

Frente la inseguridad que se vive en las zonas rurales, el ACUNDH sostiene que el Estado debe identificar las causas estructurales que propician los conflictos y la violencia. Por lo tanto, instó al Gobierno a acelerar el ritmo de implementación de los puntos del Acuerdo de Paz relacionados con **la reforma rural integral, la reincorporación de las extintas FARC y la sustitución de cultivos de uso ilícito** e impulsar soluciones duraderas para el desarrollo, la reducción de la pobreza y la disminución de las economías ilícitas.

[Finalmente, el Alto Comisionado hizo un llamado al Gobierno y al Ejército de Liberación Nacional para que respeten el derecho internacional humanitario y convocó a las partes **a acordar un cese al fuego bilateral** que permita seguir construyendo confianza para la reanudación de los diálogos de paz.]

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
