Title: Discurso histórico Juan Manuel Santos y Timoleón Jímenez "Timochenko"
Date: 2015-09-23 19:09
Category: Nacional, Paz
Tags: acuerdo de justicia firmado en la habana, acuerdo de paz, Derechos Humanos, discurso juan manuel santos, discurso timochenko, Juan Manuel Santos, paz, paz en colombia, Timochenko, vamos por la paz
Slug: discurso-juan-manuel-santos-y-timo-leon-jimeneztimochenko
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/Timochenko-y-Santos.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

Discurso del presidente Juan Manuel Santos y Timoleón Jiménez luego de realizar la firma del histórico acuerdo de justicia en La Habana

**Juan Manuel Santos, Presidente de Colombia**

\[audio mp3="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/juan-manuel-santos-.mp3"\]\[/audio\]

**Timoleón Jiménez - Jefe Máximo de las FARC-EP**

\[audio mp3="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/Timo-leon-jimenez.mp3"\]\[/audio\]  
Leer [Comunicado conjunto: Acuerdo de creación de una Jurisdicción Especial para la Paz](https://archivo.contagioradio.com/sistema-integral-de-verdad-justicia-reparacion-y-no-repeticion/)  
Leer [Claves del acuerdo del Sistema Integral de Verdad, Justicia, Reparación y No Repetición](https://archivo.contagioradio.com/sistema-integral-de-verdad-justicia-reparacion-y-no-repeticion/)
