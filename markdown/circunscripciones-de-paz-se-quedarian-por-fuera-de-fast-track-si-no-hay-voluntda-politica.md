Title: Circunscripciones de paz se quedarían por fuera de Fast Track si no hay voluntad política
Date: 2017-08-28 15:30
Category: Entrevistas, Paz
Tags: acuerdos de paz, Fast Track
Slug: circunscripciones-de-paz-se-quedarian-por-fuera-de-fast-track-si-no-hay-voluntda-politica
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/01/CONGRESO-e1483381822139.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo Particular] 

###### [28 Ago 2017] 

A paso lento avanza la Jurisdicción de Paz en el Congreso. La reforma electoral afrontará su último debate en la primera comisión de la Cámara de Representantes esta semana, y de acuerdo con la representante Angélica Lozano, puntos como las 16 circunscripciones de paz ni siquiera tienen aún ponente, mientras que el debate sobre la JEP, continúa a la espera de que se finalicen los demás debates, dejando los acuerdos de paz en **“cuidados intensivos”**.

De acuerdo con la congresista del partido Alianza Verde, los intereses electorales otras prioridades son los que han mediado en la premura de los debates **“el año electoral está en alza, todos están armando su campaña a congreso”** afirmó Lozano. (Le puede interesar: ["Reconciliación será el centro de la semana por la Paz")](https://archivo.contagioradio.com/semana-por-la-paz/)

Una de las situaciones que refleja la falta de voluntad política es la actitud de cambio radical que, aunque ante los medios de comunicación dicen que respaldan el acuerdo, siempre han votado negativamente frenando los avances que puedan darse, frente a esta situación Lozano afirmó que **"Cambio Radical ha sido frentero, dio una ponencia negativa, que hace innecesario otras jugadas**".

En concreto, sobre el proyecto legislativo de las 16 circunscripciones de paz, la representante agregó que lo ve **“más hundido que evacuado”** y que esto sí sería un grave incumplimiento a los Acuerdos de Paz, que tendría intenciones de fondo, la principal sería que sean aprobadas muy tarde para que no alcancen al calendario electoral y se queden por fuera.

Por lo tanto**, hizo un llamado a la ciudadanía para que haga una veeduría y control mucho más fuerte** sobre los debates de los acuerdos de paz y para que vote de una mejor manera en las elecciones del 2018. (Le puede interesar: ["ONU extrajo últimos contenedores de las FARC"](https://archivo.contagioradio.com/dejacion_armas_farc_onu/))

### **La propuesta de los Verdes y el Polo para legitimar Comisión de Acusaciones** 

Una de las iniciativas que actualmente está impulsando la representante Angélica Lozano junto a otros congresistas de bancadas como el Polo, es revivir la reforma al equilibrio de poderes para que se logre que Comisiones como la de **Acusaciones puedan tener herramientas suficientes y juzguen a magistrados o al Fiscal de turno**.

Esta reforma fue aprobada hace dos años en el Congreso de la República, sin embargo, muchos de sus puntos fueron modificados o se dejaron por fuera en la Corte Constitucional, por lo tanto, Lozano dice que con la salida de **5 de los 9 magistrados que se opusieron a esta reforma, hay un “panorama optimista”** sobre la aprobación de esta reforma.

<iframe id="audio_20563510" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_20563510_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
