Title: Homicidios contra defensoras de DD.HH alcanzan el 91% de impunidad.
Date: 2020-09-20 19:27
Author: AdminContagio
Category: Actualidad, Mujer
Tags: Defensores de DD.HH., Gobierno Duque, implementación del Acuerdo de Paz
Slug: casos-homicidios-contra-defensoras-de-dd-hh-se-encuentran-en-un-91-de-impunidad
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/09/2020-09-16-7.png" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/09/2020-09-18.png" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/09/2020-09-18-1.png" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: Defensoras Voces de vida y resistencia

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Desde la plataforma de DD.HH, Somos Defensores, en alianza con la Cumbre Nacional de Mujeres y Paz, Sisma Mujer – GPAZ: Grupo de Trabajo Género en la Paz y la Liga Internacional de Mujeres por la Paz y la Libertad se dio a reconocer el nuevo informe [**"Defensoras Voces de vida y resistencia"**](https://drive.google.com/file/d/1ztkaVm3AHLHQsRf3w4UBiMI_mMCIDjc9/view). Dicha investigación da cuenta del aumento en las agresiones contra mujeres que defienden la vida y el territorio en medio de riesgos ligados a las brechas de género y que trascienden del contexto armado y de violencia a otras dinámicas.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

El informe, dividido en tres capítulos, aborda el **cómo interpretar las violencias que enfrentan las defensoras de DD.HH. en medio de una brecha. de discriminación en razón del género,** las respuesta estatales en cuanto a las garantías de esta labor, sus avances y bloqueos y un tercer capítulo que recoge las agresiones cometidas entre 2013 y 2019 registrando 1.338 hechos violentos, siendo la amenaza la forma de violencia más utilizada contra las defensoras.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

A través del Programa Integral de Garantías para Lideresas y Defensoras señala que condiciones como edad, orientación sexual, situación económica y condición étnica, son factores que agudizan la vulenerabilidad de género. En este marco, **mujeres indígenas y afrocolombianas están en mayor riesgo debido a la discriminación, pobreza y marginalidad histórica a las que han estado expuestas**, además del impacto del conflicto armado en sus territorios. [(Lea también: Las barreras para el desarrollo de la mujer rural en América Latina)](https://archivo.contagioradio.com/las-barreras-para-el-desarrollo-de-la-mujer-rural-en-america-latina/)

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Agresión contra defensoras incrementó un 58,3% en el último año

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Esto se ve reflejado en las cifras registradas por el Programa Somos De- fensores, según las cuales entre 2013 y 2019 ocurrieron 1.338 agresiones individuales contra defensoras de derechos humanos, cometidas por una gran diversidad de actores. El contenido del informe revela que las agresiones a las defensoras de derechos humanos se han incrementado con el tiempo, especialmente en los últimos años y en el marco de la implementación del Acuerdo de Paz. [(Lea también: Violencia contra la mujer durante la pandemia)](https://archivo.contagioradio.com/violencia-contra-la-mujer-durante-la-pandemia/)

<!-- /wp:paragraph -->

<!-- wp:image {"id":90122,"sizeSlug":"large"} -->

<figure class="wp-block-image size-large">
![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/09/2020-09-18.png){.wp-image-90122}

</figure>
<!-- /wp:image -->

<!-- wp:paragraph {"align":"justify"} -->

Además, se pudo compilar que, según la Oficina de la Alta Comisionada de Naciones Unidas para los DD.HH. para 2019, los asesinatos contra defensoras de derechos humanos se incrementaron en cerca del 50% en comparación con el 2018. Alertas a las que se han sumado la Defensoría del Pueblo, revelando que si bien de forma global se redujo el número de asesinatos **entre 2018 y 2019, sí aumento el número de defensoras asesinadas pasando de 12 a 19, es decir un incremento del 58,3%.** [(Lea también: Desde el feminismo campesino trabajan más de 300 mujeres de las ZRC)](https://archivo.contagioradio.com/feminismo-campesino-trabajan-mujeres/)

<!-- /wp:paragraph -->

<!-- wp:quote -->

> En términos de cifras, el Programa Somos Defensores tiene registrados 84 asesinatos de defensoras desde el año 2013 hasta el 2019"

<!-- /wp:quote -->

<!-- wp:paragraph {"align":"justify"} -->

Otros datos confirman que lo**s índices más altos de agresiones se han cometido contra lideresas indígenas, con 118 casos; seguido por lideresas comunitarias con 75 casos;** 68 casos contra lideresas campesinas y el mis- mo número en contra de lideresas de víctimas; seguidos de 66 casos contra lideresas afrodescendientes y 63 casos en contra de lideresas comunales, lo que lleva a concluir que la defensa por la tierra y el territorio es la más vulnerada.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Con respecto a los presuntos responsables de las agresiones se han identificado que 902 casos responden a grupos paramilitares, 331 casos se desconoce el presunto autor de los hechos, 56 señalan a instituciones del Estado, 34 casos a disidencias de las FARC y al ELN en 13 casos.

<!-- /wp:paragraph -->

<!-- wp:image {"id":90123,"sizeSlug":"large"} -->

<figure class="wp-block-image size-large">
![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/09/2020-09-18-1.png){.wp-image-90123}  

<figcaption>
Agresiones por departamento según el informe, fuente Somos Defensores

</figcaption>
</figure>
<!-- /wp:image -->

<!-- wp:heading {"level":3} -->

### La importancia de las mujeres defensoras de DD.HH.

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

El informe resalta además cómo el ejercicio de defensa de derechos al ser asumido por mujeres, es más factible superar las causantes anteriormente citadas de discriminación étnica, de género y subordinación, pues desde su labor emprender transformaciones en los roles y discursos que a lo largo de la historia han causado desigualdad.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

De igual forma se destaca el rol de las mujeres en los procesos de protección de los derechos y su importancia en la visibilización de situaciones de "injusticia social, promover las agendas de paz e impulsar los procesos democráticos alrededor del mundo". Pese a ello, las organizaciones sociales expresan que en la actualidad aún no existen entornos favorables que garanticen a las mujeres su derecho a defender derechos humanos.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Ausencia de garantías para las mujeres

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Según la Defensoría del Pueblo, las limitaciones que existen para priorizar la seguridad de las mujeres y defensoras de DD.HH. en el país están ligadas a una falta de claridad en cuanto a las competencias de las entidades te- rritoriales en materia de prevención y protección. [(Lea también: Tribunal ordena a Duque responder a comunidades que exigen cese al fuego en sus territorios)](https://archivo.contagioradio.com/tribunal-ordena-a-duque-responder-a-comunidades-que-exigen-cese-al-fuego-en-sus-territorios/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

De igual forma, advierten no se tiene en cuenta un enfoque diferencial y los tiempos de respuestas frente a las amenazas que se enfrentan en los territorios impide que se atienda oportunamente la situación de riesgo. Asímismo, desde instituciones encargadas de la judicialización y la investigación, no hay avances en identificar los orígenes de las amenazas.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Otras dificultades se presentan ante defensoras de pueblos indígenas, comunidades afrocolombianas y procesos colectivos al solicitar medidas de protección, se les han desconocido sus formas tradicionales de autogobierno, la cosmogonía y la concepción ancestral y colectiva de protección.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### La impunidad continúa

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

El Programa Somos Defensores ha expresado con anterioridad el limitado avance en entes como la Fiscalía frente a los casos de asesinatos de líderes sociales, y en el caso de los homicidios de mujeres defensoras de DD. HH, señalan, la mayoría "se encuentran en etapa de indagación", mientras los casos que se encuentran en imputación, juicio, ejecución de penas y condena están en un 9%, es decir existe un 91% de impunidad.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Mientras la Fiscalía sostiene que un 50% de casos han sido esclarecidos, "a partir de la respuesta entregada a Sisma Mujer, **solo el 14,1% de 78 casos de defensoras y lideresas asesinadas elevados por Sisma se encontraban con sentencia".**

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Plataformas de DD.HH. agregan que dicha ausencia de garantías se debo no solo a la ausencia de una respuesta acertada de protección sino a la "falta de voluntad política por parte del Gobierno Nacional en atender las situaciones de violencia". Pese a ello, organizaciones de mujeres continúan en la interolución con las instituciones para la consecución de dichas garantías [(Le puede interesar: La sororidad, una estrategia de defensoras de DD.HH para transformar el sistema judicial en Colombia)](https://archivo.contagioradio.com/la-sororidad-una-estrategia-de-defensoras-de-dd-hh-para-transformar-el-sistema-judicial-en-colombia/)

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
