Title: El teatro como herramienta de resocialización en las cárceles
Date: 2018-10-16 13:08
Author: AdminContagio
Category: Expreso Libertad
Tags: carceles, Prisioneros políticos en Colombia, teatro
Slug: teatro-en-las-carceles
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/10/30652525_174214809902329_420616111621406720_n-370x270-1.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Defiant Reallity 

###### 9 Oct 2018 

**Defiant Reallity** es un colectivo de Teatro que ha logrado a través de la expresión corporal, generar un proceso de re socialización al interior de algunas cárceles en Colombia, evidenciando la importancia de establecer procesos efectivos que le permitan a la población reclusa tener oportunidades reales de retorno a la sociedad de forma digna.

En este **Expreso Libertad,** conoceremos esta experiencia que próximamente estrenará su más reciente trabajo, basado en radio teatro y creado conjuntamente con la población reclusa de la cárcel de Villahermosa.

<iframe id="audio_29360546" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_29360546_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Encuentre más información sobre Prisioneros políticos en[ Expreso libertad ](https://archivo.contagioradio.com/programas/expreso-libertad/)y escúchelo los martes a partir de las 11 a.m. en [Contagio Radio](https://archivo.contagioradio.com/alaire.html) 
