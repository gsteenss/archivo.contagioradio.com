Title: INPEC iniciaría cobro por transacciones a internos en Cárcel de Girón
Date: 2017-09-07 17:20
Category: DDHH, Nacional
Tags: carceles, INPEC, presos, Situación caarcelaria
Slug: 46373-2
Status: published

###### [Foto: Archivo] 

###### [07 Sept. 2017] 

En un comunicado los internos de la cárcel Palogordo en Girón-Santander, aseguraron que **están siendo sometidos a una serie de abusos y represión por parte de algunos funcionarios del Instituto Penitenciario y Carcelario – INPEC** – y de José Meneses Moreno, Director de Gestión Corporativa quienes estarían imponiendo un cobro a los familiares que realizan transacciones para los internos.

“Impone un cobro por operación bancaria que nuestros familiares realicen ante el Banco Popular en la cuenta única matriz de internos, por l**a suma de \$ 2.000 pesos a partir de octubre del 2017”** dice el comunicado. Le puede interesar: [En las cárceles de Colombia existe pico y placa para poder dormir](https://archivo.contagioradio.com/en-las-carceles-de-colombia-existe-pico-y-placa-para-poder-dormir/)

Así mismo manifiestan que en la actualidad, como fruto de un recorte presupuestal realizado por la subdirectora de desarrollo de actividades productivas, **los internos vienen siendo víctimas del mal servicio prestado por el establecimiento** que al interior de la cárcel vende productos de primera necesidad a los reclusos. Le puede interesar: [En Colombia la población en cárceles aumentó 141%  desde el año 2000](https://archivo.contagioradio.com/en-colombia-la-poblacion-en-carceles-aumento-141-desde-el-ano-2000/)

“Producto de un recorte presupuestal por parte de la señora mayor Johana Andrea Montoya Cifuentes, subdirectora de desarrollo de actividades productivas y encargada de gestionar el presupuesto y pautas para el manejo y venta de los productos” concluyen. Le puede interesar: ["Condiciones en las cárceles Colombianas son preocupantes" Alan Duncan](https://archivo.contagioradio.com/condiciones-en-las-carceles-colombianas-son-preocupantes-alan-duncan/)

###### Reciba toda la información de Contagio Radio en [[su correo]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio]{.s1}](http://bit.ly/1ICYhVU)[.]{.s1}
