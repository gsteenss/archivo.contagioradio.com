Title: El Sínodo de la Amazonía. Una buena noticia para la biodiversidad, para el planeta.
Date: 2020-02-19 12:38
Author: A quien corresponde
Category: A quien corresponda, Opinion
Tags: amazonas, Iglesias Cristianas, Papa Francisco, Religión, sínodo
Slug: el-sinodo-de-la-amazonia-una-buena-noticia-para-la-biodiversidad-para-el-planeta
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/carretera.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

"[*Criaturas*] ***del Señor, bendigan al Señor***" (Daniel 3, 57)

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Bogotá,  13 febrero del 2020

<!-- /wp:paragraph -->

<!-- wp:heading {"level":4} -->

#### Reconocer nuestra dependencia de la naturaleza, nos hace más responsables con ella y con la vida.   

<!-- /wp:heading -->

<!-- wp:paragraph -->

Estimado 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

**Ministro ordenado,** Cristianos, cristiana, personas interesadas.  
Cordial saludo,  

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En la carta de agosto pasado te decía, entre otras cosas, que respetar y cuidar la creación era cuidar la vida; que el llamado “progreso y desarrollo” la estaba destruyendo y esta actitud negativa era justificada por algunos mensajes “cristianos” y que era urgente *pasar del antropocentrismo al biocentrismo*. En estos meses, las alarmas por las consecuencias del calentamiento global se incrementaron, especialmente con el fracaso de la **COP 25** en Madrid, con el “enloquecimiento” del clima en diversas partes del mundo, los incendios en Australia y la Amazonía…

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

La carta de 11.000 científicos hablando del “sufrimiento incalculable” por el cambio climático si no se introducen cambios dramáticos, es digna de tenerse en cuenta. Por esto sigo escribiéndote para buscar caminos convergentes desde nuestras miradas divergentes de la fe.  

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Es un hecho que está creciendo la conciencia social frente a la magnitud del riesgo para la humanidad por los daños ambientales y frente a las profundas injusticias mundiales. Creyentes e iglesias van superando la separación entre la fe y la vida personal y social, entre la fe y el compromiso con la defensa de la vida del planeta y de los seres humanos y aumenta la comprensión que *“Para la Iglesia, la defensa de la vida, la comunidad, la tierra y los derechos de los pueblos indígenas es un principio evangélico”* (Conclusiones Sínodo Amazonía 47).   

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Un acontecimiento relevante, desde la perspectiva social y de fe, fue el Sínodo de la Amazonia que escuchó el grito de la tierra y el grito de los pueblos indígenas, y que reconoció las equivocaciones del pasado, señalando la necesidad de una profunda conversión y marcando el camino hacia el compromiso socioambiental de los creyentes. Es la concreción en una región especial del llamado a la conversión ecológica integral de la carta encíclica Laudato Si. 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

  
El Sínodo comenzó con el anuncio, enero del 2018 en Puerto Maldonado Perú; continuó con la realización de múltiples encuentros con pueblos indígenas, expertos y expertas, miembros de la Iglesia Católica e iglesias hermanas.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":4} -->

#### El resultado fue el documento de trabajo.

<!-- /wp:heading -->

<!-- wp:paragraph -->

Este documento fue publicado, discutido, enriquecido y ampliado en foros, encuentros, diálogos y conferencias a lo largo y ancho de la Amazonía y fuera de ella, y las conclusiones las recogió el *Instrumento Laboris* (instrumento de trabajo para los sinodales) que fue publicado en junio del 2019, más de 3 meses antes de la etapa presencial, para la discusión y enriquecimiento por parte del público en general y la asimilación de quienes estarían en las aulas sinodales.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

La etapa presencial se realizó en Roma, del 6 al 27 de octubre de 2019 con la participación de alrededor de **252 “sinodales”** de la Amazonía y del resto del mundo (obispos, indígenas, religiosas, religiosos, expertos y expertas en temas relacionados, laicos, laicas, cardenales y el Papa), quienes escucharon, reflexionaron y discutieron, elaboraron y reelaboran textos hasta llegar al documento aprobado con el título de *Amazonía: nuevos caminos para la Iglesia y para una ecología integral.* Las y los sinodales eligieron una comisión para la implementación de sus decisiones.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":4} -->

#### El Papa Francisco anunció la publicación de un documento post-sinodal. 

<!-- /wp:heading -->

<!-- wp:paragraph -->

Te cuento que este documento fue muy esperado por quienes están a favor y en contra del Papa Francisco; por quienes consideran que la Iglesia debe comprometerse en la defensa de la Amazonía y del planeta, con el cuidado de la casa común y por quienes consideran que no debe hacer ni decir nada; por quienes apuestan por la renovación de la Iglesia y por quienes la quieren en el pasado, al servicio de poderes políticos y económicos, alejados del mensaje de Jesús.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Con esta expectativa, me acerqué a la Exhortación, minutos después de su promulgación, en el vuelo de Madrid a Bogotá, es decir, por los aires. En la introducción, me sorprendieron expresiones novedosas para un documento papal, como: *“Quiero expresar las resonancias que ha provocado en mí este camino de diálogo y discernimiento. No desarrollaré aquí todas las cuestiones abundantemente expuestas en el Documento conclusivo. No pretendo ni reemplazarlo ni repetirlo.* ***Sólo deseo aportar un breve marco de reflexión****… que* ***ayude y oriente*** *a una armoniosa, creativa y fructífera recepción de todo el camino sinodal…* ***Quiero presentar oficialmente ese Documento****… Dios quiera que los pastores, consagrados, consagradas y fieles laicos de la Amazonia* ***se empeñen en su aplicación****, y que pueda inspirar de algún modo a todas las personas de buena voluntad”.*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Percibí algo nuevo, magisterialmente hablando: el Papa no reemplazaba ni repetía el resultado del sínodo, ofrecía un marco de reflexión, “presentaba oficialmente” las conclusiones del Sínodo. ¿Era una Exhortación Apostólica para “presentar” el documento final, es decir, para respaldar el largo y profundo proceso sinodal y las conclusiones aprobadas? ¿Quedaban incluidos el documento *“Amazonía: nuevos caminos para la Iglesia y para una ecología integral vigente”* y las propuestas polémicas? ¿Estaba el Papa Francisco, “delegando” su magisterio en el sínodo o reconociendo, la Iglesia en discernimiento, como fuente del magisterio? La respuesta afirmativa, con los hechos, a estas preguntas, muestra que este proceso sinodal, constituye la puesta en práctica de la conversión a la sinodalidad eclesial mediante la Constitución Apostólica *“Episcopalis Communio”* que estructura el caminar juntos (Instrumentum laboris 5), que debe marcar “el ser y hacer” de la Iglesia en el mundo.  

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Esta conclusión, ayuda a ver mejor la novedad que encierra el proceso sinodal. Para las y los participantes en el Sínodo, más que las conclusiones, lo fundamental era el proceso participativo: la creación, puesta en marcha y desarrollo de la Red Eclesial Panamazónica –**REPAM**, que concretizó el compromiso con la ecología integral de la Carta Encíclica Laudato Sii en la Amazonía, y que facilitó la multitudinaria participación en el proceso sinodal, la especial participación de los pueblos indígenas en todo el proceso, el aporte de expertos y expertas de todos los estamentos implicados en la defensa de la Amazonía, la publicación anticipada del “Instrumentum Laboris”, la publicación de los aportes de los 12 grupos de trabajo durante el sínodo, y la publicación inmediata de la conclusiones del Sínodo con las votación respectiva de cada numeral.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":5} -->

##### Todo muestra una nueva manera de, ser y hacer iglesia, al servicio de la sociedad y del planeta.

<!-- /wp:heading -->

<!-- wp:paragraph -->

En la Red Iglesias y Minería, veíamos que el Sínodo podía llevar a la Iglesia a “retomar lo fundamental del cristianismo primitivo para afrontar los desafíos del presente, para entrar en el proceso de cambio, de conversión que esta hora de la historia y el Espíritu de Jesús le exigen”. Personalmente creo que los hechos la están llevando por este camino, con pasos profundos y firmes.    

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Las 145 notas de pie de página de la Exhortación, ratifican la opción magisterial del Papa Francisco: la llamada a la conversión ecológica integral de la Encíclica Laudato Sii, la conversión pastoral de la Exhortación Apostólica Evangelii Gaudium, la conversión a la sinodalidad citando reiteradamente el Instrumento, el llamado a la santidad en el mundo actual de la Exhortación Apostólica “Gaudete et Exsultate”, la vuelta al Vaticano II y al magisterio de la Iglesia latinoamericana, y la “comunión  episcopal” al citar los episcopados de los distintos países amazónicos. Además, reconoce que el Espíritu habla más allá del mundo religioso, especialmente por los poetas, cita diez de ellos. 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Al compartir con el “pueblo de Dios y con todas las personas de buena voluntad” los cuatro sueños para la Amazonía y los cuatro sueños que articulan la Exhortación (el social, el cultural, el ecológico y el eclesial) invita a “Ampliar el horizonte más allá de los conflictos” y a *expandir la mirada para evitar reducir nuestra comprensión de la Iglesia a estructuras funcionales*”(QA 100), a superar las discusiones que nos distraen de lo fundamental del Evangelio y a restar importancia a la emergencia ambiental, al empobrecimiento de millones de personas, al escándalo del hambre y a la lógica que genera un mundo profundamente injusto. 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Te comparto esta reflexión por el gran desafío que tenemos: asumir estos sueños, asimilar la profundidad y las repercusiones de esta “nueva-vieja” manera de ser iglesia, comprender y poner en práctica la conversión pastoral, la conversión cultural, la conversión ecológica y la conversión a la sinodal de las conclusiones del Sínodo. Por ahora, te invito a leer despacio la Exhortación. 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Fraternalmente, tu hermano en la fe,  

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

P. Alberto Franco, CSsR, J&P. [francoalberto9@gmail.com]{}

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

[Vea mas columnas](https://archivo.contagioradio.com/author/a-quien-corresponde/)

<!-- /wp:paragraph -->
