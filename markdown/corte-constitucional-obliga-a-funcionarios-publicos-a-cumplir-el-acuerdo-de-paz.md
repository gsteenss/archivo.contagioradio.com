Title: Corte Constitucional obliga a funcionarios públicos a cumplir el Acuerdo de Paz
Date: 2017-10-12 14:02
Category: Nacional, Paz
Tags: acuerdos de paz, blindaje jurídico de los acuerdos de paz, Corte Constitucional
Slug: corte-constitucional-obliga-a-funcionarios-publicos-a-cumplir-el-acuerdo-de-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/08/corte_constitucional-Fuero_Penal_Militar_contagioradio-e1485870742527.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: CMI] 

###### [12 Oct 2017] 

La Corte Constitucional, en una decisión unánime, **avaló el blindaje jurídico del Acuerdo de Paz** pactados entre el Gobierno Nacional y las FARC. Como consecuencia, ningún Gobierno, durante los próximos 12 años, podrá modificar lo acordado en la Habana haciendo de los Acuerdos una política de Estado.

En el fallo, los magistrados de la Corte Constitucional, aclararon que **los próximos tres gobiernos están en la obligación de respetar el Acuerdo de Paz** por lo que las autoridades y las diferentes instituciones que hacen parte del Gobierno, deben cumplir con las disposiciones que se acordaron. (Le puede interesar: ["Implementación de los acuerdos de paz, no va por buen camino"](https://archivo.contagioradio.com/implementacion-de-los-acuerdos-no-va-por-buen-camino/))

De igual manera, enfatizó la Corte que al bloque de constitucionalidad **entrarán los temas relacionados con el Derecho Internacional Humanitario** y que son fundamentales en la Constitución como la participación política, la justicia y la reincorporación a la sociedad civil de los ex combatientes de las FARC. Sin embrago, especificó que no todo lo que está acordado hará parte de la Constitución automáticamente.

De acuerdo con el senador Iván Cepeda, el fallo de la Corte **resuelve la discusión sobre el alcance jurídico, legal y constitucional que tiene el Acuerdo de Paz**. Dijo que la Corte fue clara en su decisión “en torno al carácter de obligatorio cumplimiento que tienen los acuerdos”.

Así, quedó establecido que el acuerdo “es un parámetro de interpretación y al mismo tiempo es un referente para el **cumplimiento de las normas que tienen que ver con la paz**”. El senador indicó que el fallo compromete al Estado en el desarrollo del proceso por 12 años, blindando y protegiendo la estabilidad de los Acuerdos de Paz.

### **Funcionarios del Estado están obligados a cumplir el Acuerdo** 

Cepeda indicó que la aprobación de normas, con carácter de reforma constitucional, han introducido aspectos del acuerdo, a través de actos legislativos por parte del Gobierno y el Congreso, a la Constitución. Además, “la Corte está diciendo que **no puede ser objeto de renegociación**, de modificación o cambios por vías legislativas lo que tiene que ver con el fondo del Acuerdo”.

Se refirió a que, cuando la Corte dice que el Acuerdo de Paz atañe una política de Estado, “está diciendo que todos los funcionarios de las 3 ramas del poder público, **están obligados a cumplir con el acuerdo de buena fe** y de manera estricta”. (Le puede interesar: ["Implementación de los requiere movilización social: Iván Cepeda"](https://archivo.contagioradio.com/implementacion-de-los-acuerdos-requiere-movilizacion-social-ivan-cepeda/))

### **Realidad vigente hoy es la decisión de la Corte Constitucional** 

Ante los anuncios de miembros de la oposición, como del Centro Democrático, quienes afirmaron que **harán todo lo posible por tumbar el fallo**, Cepeda manifestó que la orden que empieza a regir a partir de hoy, y por los próximos años, es la que fue emitida por la Corte Constitucional.

Si bien es posible que se establezcan mecanismos de refrendación o que por la vía popular se puedan modificar algunas cosas, **“la realidad hoy vigente en el país es la decisión que la Corte Constitucional ha tomado”**. Es por esto, que los Gobiernos futuros no pueden introducir cambios que distorsionen el fondo del Acuerdo de Paz. (Le puede interesar: ["Del Fast Track al Slow Track"](https://archivo.contagioradio.com/del-fast-track-al-slow-track-en-la-implemetacion-de-acuerdos-de-paz/))

Finalmente, Cepeda manifestó que, como política de Estado, las leyes futuras y decisiones políticas **“deben corresponder al espíritu de lo que está vigente** y que atañe a la paz”. Así, de acuerdo con la Corte,  se deberá cumplir con la determinación de incorporar el Acuerdo de paz al ordenamiento jurídico “exigiendo su implementación normativa por los órganos competentes y de conformidad con los procedimientos previstos en la Constitución”.

###### Reciba toda la información de Contagio Radio en [[su correo]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio]{.s1}](http://bit.ly/1ICYhVU)[.]{.s1}

 
