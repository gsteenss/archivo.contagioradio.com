Title: Agresiones contra líderes sociales incrementaron un 49% en el primer semestre del 2019
Date: 2019-10-11 17:28
Author: CtgAdm
Category: DDHH, Nacional
Slug: agresiones-contra-lideres-sociales-incrementaron-un-49-en-el-primer-semestre-del-2019
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/Líderes-Somos-Defensoers.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/Agresiones-2019.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/Tipos-de-agresiones.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Somos Defensores] 

El más reciente informe **'Defensores ¿El Juego Final?**', el cual recopila las agresiones semestrales cometidas contra líderes, lideresas, evidencia que entre enero y junio de 2019  se han registrado 591 agresiones: 477 amenazas, 59 asesinatos, 27 atentados, 22 detenciones arbitrarias y 6 robos de información que afecta a defensores y defensoras de DD.HH, denotando un  aumento del 49% con relación al 2018 y una respuesta poco efectiva por parte de entidades estatales frente a estas

Aunque el informe señala que existe una reducción del 23% en el número de asesinatos de líderes y lideresas, advierten que esta tendencia podría variar para el segundo semestre del 2019, en medio de un contexto electoral regional que podría desatar mayores disputas por el territorio, algo que se ha visto reflejado con el asesinato contra candidatos de diversos partidos. [(Lea también: Agresiones contra defensores de DD.HH. aumentaron más de 40% en 2018)](https://archivo.contagioradio.com/en-2018-se-presentaron-mas-de-dos-agresiones-diarias-contra-defensores-de-dd-hh/)

Señalan que otro elemento que ha permitido la reducción de asesinatos es el temor que existe hacia ciertos actores que se han apoderado del territorio, que ha llevado a que las y los líderes guarden silencio, reduciéndose la violencia, física y directa, lo que ha dado lugar al incremento de intimidaciones y amenazas, que pasaron de 272 en 2018 a 477.  **Resalta el informe también el incremente en detenciones arbitrarias en un 450%,  lo que denota el rol que ha cumplido la Fuerza Pública de cara a quienes defienden los DDHH.**

\[caption id="attachment\_74940" align="aligncenter" width="568"\]![Agresiones contra líderes 2019](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/Tipos-de-agresiones.jpg){.size-full .wp-image-74940 width="568" height="259"} Foto: SIADDHH - Programa Somos Defensores 2019\[/caption\]

De igual forma se pudo establecer que de las 591 personas defensoras agredidas en este lapso,  el 71% fueron hombres y el 29% mujeres, lo que evidencia un incremento en las agresiones contra mujeres en relación con el primer semestre 2018, pasando de un 26% a un 29%.

### ¿Quiénes atacan a líderes y lideresas? 

Tal como en informes anteriores, los presuntos irresponsables en su mayoría, **continúan siendo grupos paramilitares quienes serían los autores del 53% de las agresiones**, pasando de 187 agresiones en el 2018 a 314 en el 2019, mientras en segundo lugar, con un 28% se ubican autores desconocidos, un 9% de las disidencias de las FARC, un 6% del ELN y en 4% de la Fuerza Pública.

\[caption id="attachment\_74924" align="alignleft" width="561"\]![Agresiones líderes sociales 2019](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/Agresiones-2019.jpg){.size-full .wp-image-74924 width="561" height="735"} Foto: Somos Defensores\[/caption\]

Al respecto. Diana Sánchez, directora de Somos Defensores, señala que en los territorios se ha dado un re acomodamiento de actores armados lo que afecta directamente a los liderazgos sociales, impidiendo que en ocasiones se pueda identificar un actor en particular, especialmente en regiones como el Bajo Cauca Antioqueño, el Sur de Córdoba, el Urabá, y el Pacífico, por mencionar algunas regiones

Parte de esta situación se refleja en Cauca, que continúa siendo el departamento más peligroso para el liderazgo social y el territorio con mayor aumento en las agresiones contra los defensores de DDHH, registrando 115 casos en el 2019.  [(Le puede interesar: ¿Qué está pasando en Cauca? análisis del líder campesino Óscar Salazar)](https://archivo.contagioradio.com/que-esta-pasando-en-el-cauca-un-analisis-del-lider-campesino-oscar-salazar/)

### Lectura insuficiente por parte del Gobierno

Sánchez señala que este informe va mucho más allá de la estadística, por que además resalta la responsabilidad del Estado por acción u omisión, limitando las agresiones contra líderes y lideresas únicamente a asesinatos, restando universalidad a todo los tipos de agresiones de los que son víctimas quienes defienden los derechos de su comunidad.

'Defensores ¿El Juego Final?' realiza un detallado análisis al Gobierno, señalando que este desconoce la realidad de los territorios al dejar de lado lo pactado en el Acuerdo de Paz y obstaculizando las iniciativas que desde la sociedad civil se ofrecen para garantizar su seguridad mientras que las acciones de entidades como la Fiscalía o la Procuraduría y la misma Fuerza Pública resultan ineficaces frente a estas agresiones.

Mientras del el Gobierno se planteó una reestructuración a la Unidad Nacional de Protección que en la actualidad cobija a 7434 personas,  **limitándose a ofrecer dispositivos que resguardar la integridad física del o de la protegida sin ofrecer verdaderas soluciones a las causas del problema.**

Otras entidades como la Procuraduría únicamente han adelantado 31 investigaciones, de las que 23 se encuentran en indagación preliminar, 5 en investigación y 3 en evaluación de la queja. Por su parte, desde la Fiscalía afirman que se ha esclarecido entre un 58% y 60% de los 289 casos investigados entre el 1 de enero de 2016 y el 3 de julio de 2019, sin embargo la realidad es otra.

De estos 289 casos, 33 casos tienen sentencia de las que 4 corresponden a la  jurisdicción especial indígena, mientras 55 casos están en juicio, 43 casos permanecen con imputación de cargo, otros 40 casos están en indagación con orden de captura, 3 casos precluidos: por razones de muerte de los presuntos implicados mientras otros 115 casos se encuentran en etapa de indagación.

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
