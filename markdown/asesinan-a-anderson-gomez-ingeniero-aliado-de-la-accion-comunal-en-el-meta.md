Title: Asesinan a Ánderson Gómez, ingeniero aliado de la Acción Comunal en el Meta
Date: 2019-04-12 18:14
Author: CtgAdm
Category: Líderes sociales, Nacional
Tags: Asesinato de líderes comunales, lideres sociales, Macarena, Meta
Slug: asesinan-a-anderson-gomez-ingeniero-aliado-de-la-accion-comunal-en-el-meta
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/41939_1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:] 

El pasado 11 de abril en La Macarena, Meta, camino de regreso al municipio, fue asesinado con disparos de arma de fuego **el  ingeniero y líder comunal Ánderson Ramiro Gómez Herrera,** quien trabajaba desde hace seis años en la formulación y presentación de proyectos junto a las Juntas de Acción Comunal de las veredas aledañas.

Según relata Delio Franco, presidente de la Asociación de Juntas de Acción Comunal (Asojuntas), el ingeniero regresaba al pueblo tras apoyar un proceso de convenios solidarios en las veredas Campo hermoso y Las Delicias cuando encontró la vía bloqueada por un palo atravesado en el camino, Ánderson detiene su vehículo y al retirar el obstáculo le sale al paso una persona con pasa montañas que dispara contra él. [(Lea también: ¿Quién fue Policarpo Guzmán? El histórico líder social de Argelia, Cauca que fue asesinado) ](https://archivo.contagioradio.com/quien-fue-policarpo-guzman-el-historico-lider-social-de-argelia-cauca-que-fue-asesinado/?fbclid=IwAR3gHi6AQSlgXIK8wsM4ElhWXC4241adDsNJvDofpEK8QtHU5yP1T-TccPI)

No se tiene conocimiento que existieran amenazas en su contra, pero tal como indica Delio Franco, el ingeniero era bien conocido en el sector pues hacía recorridos por toda la zona rural; el asesinato ya es materia de investigación y hast el momento se desconocen los autores intelectuales y materiales del suceso y aunque en La Macarena hay presencia de disidencias, el Presidente de Asojuntas afirma que no se pueden señalar a responsables hasta que se no haya un pronunciamiento oficial de las autoridades.

### **La mano derecha de las Juntas de Acción Comunal ** 

A pesar que el ingeniero no estaba afiliado a ninguna junta en particular, era su mano derecha y "por esa razón las personas lo conocían como el ingeniero de las Juntas de Acción Comunal", dentro de los proyectos que adelantó  destacan varias ejecuciones de obras en la vía de La Macarena hacia San José del Guaviare, "que eran ejecutados por las mismas comunidades  mientras el las supervisaba", además de promover otros proyectos con las JAC en Las Delicias y en los Llanos del Yarí.

Franco señala que las Juntas de Acción Comunal están gestionando y consiguiendo los recursos de la despedida y los actos fúnebres  para Ánderson,"**los líderes de las Juntas están consternados porque se ha perdido una persona que era muy cercana, pidieron que no se le enterrara en otra parte"** explica el presidente quien señala que el ingeniero era oriundo de Santander pero por decisión de la población se pidió que fuera enterrado en La Macarena  a donde llegará el resto de su familia para acompañar a la esposa e hija del fallecido líder comunal.

<iframe id="audio_34409589" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_34409589_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
