Title: Cerrejón demolió escuela de 38 niños y niñas de la Guajira
Date: 2015-05-06 14:27
Author: CtgAdm
Category: Educación, Entrevistas
Tags: Cerrejón, Derecho a la eduación, educacion, Guajira, Mineria, niños
Slug: cerrejon-demolio-escuela-de-38-ninos-y-ninas-de-la-guajira
Status: published

##### Foto: [informedeladocencia.blogspot.com]

<iframe src="http://www.ivoox.com/player_ek_4456175_2_1.html?data=lZmimJabeY6ZmKiak5eJd6KnkpKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRh8bm08rXh6iXaaOnz5DRx9jYttbtxpDS1cjZqc3VjMnSjZicb8%2Fdhqigh6eVs9Sf2pDby4qnd4a2ksbgjcrSb83VjJKSmaiRh9Di1cbUy9SPlsLYytSYj4qbh46o&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Wilman Palmezano, Consejo Comunitario de Negros Ancestrales de Chancleta] 

Comunidades de Patilla y Chancleta apelaron una tutela que ordenó transporte en lugar de la reconstrucción de la escuela de **38 niños y niñas de estas comunidades afrodescendientes en la Guajira que perdieron su colegio**, debido a que la empresa **El Cerrejón** y la Alcaldía de Barrancas la destruyeron, según denuncia Wilman Palmezano, quien hace parte del Consejo Comunitario de Negros Ancestrales en Chancleta.

Wilman asegura que **la destrucción de la escuela hace parte un plan de la empresa para ejercer presión sobre las familias que decidieron quedarse**, pese a la propuesta de reasentamiento que les había hecho el Cerrejón, ya que la comunidad no vio un "buen futuro" con esa propuesta, como lo dice el lider comunitario.

Es por eso que los pobladores decidieron usar su derecho a la tutela, con el objetivo principal de que les reconstruyeran el colegio, sin embargo, el fallo de tutela del 16 de abril del Juzgado Promiscuo Municipal de Barrancas, consideró que la manera de amparar el derecho a la educación de las y los niños de Patilla y Chancleta era ordenando que se establecieran rutas para el acceso a la escuela más cercana, por ende la juez “**solo se inclino por el transporte, siendo que el tema central era la demolición del colegio** y la juez dijo que el colegio no era competencia de ella”, señala Wiman.

Según Palmezano, se dieron de 48 horas a 10 días para que el transporte de los niños esté listo, y aun así no ha sucedido nada, “sentimos que todo va a quedar impune, no sabemos si ese fallo valla a funcionar”.

La hija del líder comunitario de 6 años, lleva año y medio sin estudiar, y esa es la situación de los otros niños y niñas de la comunidad.

Así mismo, los pobladores de Chancleta y Patilla **exigen que aparezca el responsable de la demolición de la escuela,** aunque Wilman asegura que la destrucción del colegio solo le conviene al Cerrejón.

Actualmente, **los padres deben entre 600 mil pesos y un millón de pesos ya que ellos mismos habían decidido pagar el transporte para el estudio**, pero no pudieron continuar haciéndolo debido a que no tienen empleo, por lo que los niños y niñas continúan sin que se les garantice su derecho a la educación.
