Title: ¿Qué es el cyberfeminismo?
Date: 2017-11-15 07:47
Category: Libertades Sonoras
Tags: JEP, Libertades Sonoras, Mujeres y Paz
Slug: que_es_cyberfeminismo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/11/cyberfeminismo.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Twitter] 

###### [13 Nov 2017] 

[¿Qué es eso del cyberfeminismo y la seguridad digital? ese fue el tema que hablamos en \#LibertadesSonoras, en donde dialogamos con expertas en el tema y se explicó cuál ha sido la importancia del activismo en las redes sociales, pero también los cuidados que debe haber a la hora de usar las redes sociales o bajar cualquier tipo de aplicación en el celular.]

<iframe id="audio_22072577" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_22072577_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio]{.s1}](http://bit.ly/1ICYhVU)[.]{.s1}

 
