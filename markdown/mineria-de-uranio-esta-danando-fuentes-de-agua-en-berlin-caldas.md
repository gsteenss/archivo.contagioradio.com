Title: Minería de Uranio está dañando fuentes de agua en Berlin, Caldas
Date: 2017-06-13 14:18
Category: Ambiente, Nacional
Tags: Caldas, defensa del medio ambiente, medio ambiente
Slug: mineria-de-uranio-esta-danando-fuentes-de-agua-en-berlin-caldas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/06/marmato_mineros_medoro_a39-e1497381473274.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: La Patria.com] 

###### [13 Jun 2017]

La empresa Gaia Energy tiene 5 títulos mineros para averiguar el contenido real de uranio sobre 10 mil 700 hectáreas en Caldas. Los pobladores de la vereda Alto San Juan en Berlín en ese departamento han denunciado que **el residuo ocasionado por la exploración del mineral está afectando las fuentes hídricas** de la quebrada Santa Marta que surte del líquido vital a la comunidad.

El uranio se sabe que es un mineral radioactivo peligroso. La empresa minera Gaia Energy ha hecho socializaciones con las comunidades para decirle a la población que la extracción de uranio no es peligrosa. Sin embargo, la explotación minera no ha comenzado y ya **hay fuentes de agua con olores fétidos**, llenas de residuos de uranio que perjudican la salud de las personas y las condiciones medioambientales, indica la comunidad.

Según los campesinos de la vereda Alto San Juan en Berlín, Caldas, “la empresa dejó residuos almacenados en una bodega a 100 metros de una escuela y queremos saber esto como va a afectar a las personas que están cerca de allí”. Los pobladores han dicho que hace 4 años la empresa minera “**rompió una vena de la quebrada en la parte alta de la montaña cambiando el curso de la misma** y afectando a las poblaciones que utilizan esa agua.”

**Explotación de uranio es agresiva con el medio ambiente y afecta la salud de las personas**

El uranio es un mineral que puede ser utilizado como combustible para generar energía nuclear y **puede transformarse en una fuente inagotable y barata de energía**. Sin embargo, según estudios del portal nuclear.5,  su extracción, como cualquier otra, genera residuos y gasta otros recursos.

La extracción de uranio, al ser un mineral radioactivo, es una de las más contaminantes porque saca del medio ambiente el producto utilizando otros componentes que generan residuos tóxicos. Al igual que otros minerales, su extracción se puede hacer a cielo abierto o bajo tierra. De igual forma, la **exposición de los humanos a grandes cantidades de uranio puede generar cáncer** y otras enfermedades que se evidencian en la reproducción.

De la agresividad de la extracción se deriva la preocupación de los pobladores quienes han acudido a instituciones como Corpocaldas para encontrar soluciones a **sus dudas sobre los daños ambientales y humanos que puede tener la extracción del uranio**. Según los pobladores, “estamos muy preocupados porque las aguas fétidas llegan a la comunidad a través de la quebrada y no sabemos que daños se pueden ocasionar”.

Hasta el momento se desconoce un pronunciamiento oficial por parte de las autoridades ambientales. Lo cierto es que, tanto el desvío de las fuentes hídiricas o quebradas, como la contaminación de los entornos de vida de la comunidad por los olores **tienen en zozobra a la comunidad de este municipio.**

<iframe id="audio_19244806" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_19244806_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU).
