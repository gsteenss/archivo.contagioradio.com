Title: "Iven" música para hermanar pueblos
Date: 2015-10-11 11:40
Category: Cultura, Hablemos alguito
Tags: 31 Festival Voz, Grupo Iven Venezuela, Nueva canción Latinoamericana, semanario Voz
Slug: iven-musica-para-hermanar-pueblos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/10/Grupo-Iven-pag-91.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<iframe src="http://www.ivoox.com/player_ek_8891456_2_1.html?data=mZ2mk5mZeo6ZmKiakpaJd6KllIqgo5ebcYarpJKfj4qbh46kjoqkpZKUcYarpJKyzpDLttbk0JDjx9PJvtDgwtPcja7aqc%2Bf187gy9nFb6Tjz9nOyc7Tb9PVxc7cj4qbh4630NPhw8zNs4zGwsnW0ZCRaZi3jpk%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Grupo "Iven" en Hablemos alguito] 

###### [11 Oct 2015] 

Son más de 25 años de viaje para el grupo "Iven", desde Mérida a toda Venezuela y luego por el mundo. Un recorrido que a través de la palabra, la música y la poesía, ha llevado un mensaje de reivindicación de la lucha por la identidad y hermandad de los pueblos latinoamericanos.

"Iven" suena a trova, a canción social y folclor latinoamericano, con un marcado acento de revolución e idealismo bolivariano; sentimientos comunes entre los integrantes de la agrupación desde sus años como estudiantes de la Universidad de Los Andes, a pesar del difícil panorama político y social que reinaba en Venezuela por esos años.

[![grupo Iven](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/10/IMG_0046-e1444563408467.jpg){.aligncenter .size-full .wp-image-15630 width="885" height="532"}](https://archivo.contagioradio.com/iven-musica-para-hermanar-pueblos/img_0046/)

A la fecha, Salbatore Grosso, director y pianista, los hermanos Víctor Manuel y Gerardo "Lalo" Moreno, en la voz, el cajón y la guitarra respectivamente, Luis Alejandro Pereira en el bajo, Humberto Rivas, percusión menor, Yerimoth Barrios en la guitarra eléctrica y el poeta Carlos Angúlo, han lanzado 3 producciones de estudio.

"Mientras tanto","Por una angustia de amor" y "Alas sobre el delirio", son el fruto del esfuerzo y constancia de la agrupación, por las que han recibido múltiples reconocimientos a nivel venezolano e internacional, abriendo las puertas a la realización de colaboraciones con artistas de la talla de Piero y Pablo Milanés entre otros.

"Iven", vuelve a Colombia en el marco del "31 Festival Voz", organizado por el Semanario Voz, subiendo a tarima el 11 de Octubre a las 3:00 p.m. en el Club de Pensionados de los Ferrocarriles en Bogotá.

Aprovechando su paso por la ciudad conversamos con ellos en "Hablemos Alguito" sobre su historia, pensamiento y legado, desde la cultura y la música hacia la paz y reconstrucción del tejido social en su país y el continente.
