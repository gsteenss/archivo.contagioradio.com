Title: No tiene por qué haber inflación de precios, si Gobierno toma medidas urgentes ante Covid 19
Date: 2020-03-19 16:19
Author: CtgAdm
Category: Actualidad, DDHH
Tags: abastecimiento, alza de precios, Canasta familiar, Covid-19, especulación, usura
Slug: no-tiene-por-que-haber-inflacion-de-precios-si-gobierno-toma-medidas-urgentes-ante-covid-19
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/12/mercado.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

Ciudadanos denuncian en Bogotá las alzas de precios en productos de la canasta familiar, en medio del abastecimiento que buscan hacer impulsados por el miedo y la desinformación. Sin embargo, Jairo Bautista, docente de la Universidad Nacional, afirma que a pesar de la demanda **"no tiene por qué haber inflación de precios"** pues hay suficiente stock en los almacenes y hace un llamado a que los vendedores no especulen justo ahora debe primar la solidaridad.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

De acuerdo con el docente, la demanda compulsiva que se ha venido generando en los últimos cinco días, es producto del pánico de las personas que no ven una orientación clara en materia de cómo prevenir y controlar la crisis del Covid-19.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

"Esto mismo está pasando en Estados Unidos, pero no pasó en otros países. No pasó en China, no pasó en Korea, no pasó **en Italia en donde las condiciones de la epidemia son bastante graves"** señala el docente.

<!-- /wp:paragraph -->

<!-- wp:heading -->

"No tiene por qué haber inflación de precios"
---------------------------------------------

<!-- /wp:heading -->

<!-- wp:paragraph -->

Bautista asegura que en este momento no hay un problema de abastecimiento, sino una demanda compulsiva que va a tender a agravarse, razón por la cual hace un llamado urgente a que el gobierno establezca controles de precios y a que la Superintendencia de Industria y Comercio, emita regulaciones sobre la cantidad de productos que puede adquirir una persona según su núcleo familiar.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

También, expone que ante la ausencia de directrices del gobierno Nacional, la Secretaría de Desarrollo Económico del Distrito Capital podría sancionar a los establecimiento comerciales, sobretodo a los grandes que estén jugando con la inflación de precios.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

"No tiene porque haber inflación de precios. En el mercado de abastecimiento de vienes hay suficiente stock para responder a una demanda de **mínimo 4 meses y eso ya ha sido establecido por las autoridades económicas**" afirma el docente.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En ese sentido, Bautista destaca que algunos elementos como tapabocas, jabones antibacteriales o el alcohol, han tenido un aumento del 10.000%, mientras que otros han visto en esta situación una forma de sacar provecho revendiendo los productos.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

"Esta es una cuarentena que va a durar entre 35 a 40 días, de tal manera que lo que se pide es que se provean de alimentos para ese tiempo, no para seis meses ni para un año". (Le puede interesar: ["El neoliberalismo del coronavirus"](https://archivo.contagioradio.com/el-neoliberalismo-del-coronavirus/))

<!-- /wp:paragraph -->

<!-- wp:heading -->

La solidaridad nos corresponde a todes
--------------------------------------

<!-- /wp:heading -->

<!-- wp:paragraph -->

"Debemos aislarnos, debemos reducir el número de salidas a las calles, no compremos en masa porque desabastecemos los mercados y particularmente, estamos dejando sin abastecimiento a las poblaciones más vulnerables de la ciudad. Ellos son los que **llegarán a la tienda y encontraran los precios superinflados y no van a tener como comprar"** expresa Bautista frente al comportamiento que debe tener la ciudadanía en esta crisis.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por último, el mensaje a los medianos y pequeños empresarios, por parte del docente, es que no se especule con los precios "es tu vecino, es tu amigo, es la gente con las personas con las que nos relacionamos todos los días, no tiene porqué haber esta especulación". (Le puede interesar: ["Seguimiento y Recomendaciones actuales del coronavirus en Colombia"](https://coronaviruscolombia.gov.co/Covid19/index.html))

<!-- /wp:paragraph -->
