Title: Otra Mirada: Se reactiva la movilización social en Colombia
Date: 2020-09-22 19:59
Author: PracticasCR
Category: Nacional, Otra Mirada, Programas
Tags: 21s, Fuerza Pública, Movilización social
Slug: otra-mirada-se-reactiva-la-movilizacion-social-en-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/09/paro.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: Contagio Radio /Andrés Zea

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Este 21 de septiembre varios sectores del país salieron a las calles nuevamente a movilizarse y protestar en contra del abuso por parte de las fuerzas públicas y por los diferentes sucesos que han venido azotando al país; el asesinato de líderes sociales, las masacres, la falta de implementación del Acuerdo de Paz y la deficiente gestión del Gobierno durante la pandemia. (Le puede interesar: [Duque expresó «el desprecio que siente por la ciudadanía», Ángela Robledo](https://archivo.contagioradio.com/duque-expreso-el-desprecio-que-siente-por-la-ciudadania-angela-robledo/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En el programa de Otra Mirada sobre la reactivación de las movilizaciones y protestas en el país estuvieron Daniel Libreros, analista y profesor de la Universidad Nacional, Francisco Marín, defensor de DD.HH. e integrante de Juventud Rebelde y Óscar Ramírez, abogado de CSPP e integrante de Defender la Libertad.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Durante el conversatorio, los invitados hicieron un balance de la jornada a nivel nacional y compartieron cómo se vivió la situación en Bogotá y Nariño. **Por otra parte, a pesar de que se anunció que la fuerza pública no usaría armas de fuego, 13 personas resultaron heridas, por lo que los invitados señalan lo preocupante de las actuaciones, prácticas y procedimientos de la fuerza pública durante las protestas.** 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Además, comentan que a pesar de que se sabe que el estallido social va a ser más fuerte este año, es preocupante que el gobierno esté “diseñando un entramado de fortalecer el terror de estado”.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En materia de acciones, señalan qué se debe hacer desde los diferentes ámbitos de la sociedad y comparten la importancia de salir a marchar teniendo unos argumentos claros.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Finalmente, Daniel Libreros, habla de las falencias del actual Código de Policía y la necesidad de una reforma estructural de la Policía y en general de las Fuerzas Militares. ([Si desea escuchar el programa del 18 de septiembre: Cine en resistencia](https://www.facebook.com/contagioradio/videos/360486468668861))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Si desea escuchar el análisis completo: [Haga click aquí](https://www.facebook.com/contagioradio/videos/2793255737663648)

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
