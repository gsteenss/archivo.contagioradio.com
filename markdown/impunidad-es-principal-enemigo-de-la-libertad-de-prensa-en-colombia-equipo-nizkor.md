Title: Entre 1986 y 2017 se registraron 673 casos de agresiones contra periodistas en colombia
Date: 2017-08-09 13:58
Category: DDHH, Nacional
Tags: Amenazas a periodistas, equipo nizcor, Fiscalía General de la Nación, Periodistas
Slug: impunidad-es-principal-enemigo-de-la-libertad-de-prensa-en-colombia-equipo-nizkor
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/prensa1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:  Archivo] 

###### [09 Ago 2017] 

El organismo español especializado de derechos humanos, Equipo Nizkor, presentó el informe “El silenciamiento de los periodistas y la estruendosa impunidad en Colombia”. Allí, revela cifras de Fiscalía General de la Nación que indican que, en el país, **las investigaciones por violencia contra los periodistas son nulas.** Advierte el organismo que de las investigaciones realizadas entre 1986 y abril de 2017, 2 de cada 10 casos llegaron a juicio final o tuvieron condena.

Según las cifras de la propia Fiscalía y presentadas por el organismo de derechos humanos, en un tiempo de 31 años, **solo 12 procesos, de 673 casos registrados, llegaron a juicio.** De igual forma, 8 de ellos obtuvieron condena y “3 de esas sentencias podrían no tener relación con la profesión de la víctima”, lo que indica que son menos las sentencias ejecutadas en virtud de las agresiones a los periodistas.

El Equipo Nizkon asegura que **“la impunidad no sólo es rampante sino complementaria a la desinformación y el desorden** en los datos de la Fiscalía sobre los ataques contra periodistas”. Esto en virtud de que, según ellos, el ente investigativo se tomó más de 4 meses para suministrar las cifras que hacen parte del informe y que fueron entregadas como respuesta a una acción de tutela “instaurada en su contra por vulnerar el derecho a la información y la libertad de prensa”. (Le puede interesar: ["CIDH hace un llamado a proteger a periodistas en Colombia"](https://archivo.contagioradio.com/llamado-de-la-relatoria-de-libertad-de-expresion-al-estado-a-prevenir-violencia-contra-la-prensa/))

Adicional a esto, desde 1986 hasta hoy **“sólo un caso de amenazas de un total de 304 investigados logró sentencia condenatoria**, igual sucedió con 3 de los 105 casos de homicidios”. Para el organismo, esto envía un “delicado mensaje a los perpetradores desde las instancias judiciales”.

El informe agrega que “pese a que todas las entidades de prensa dieron por hecho la creación de una sub unidad de crímenes contra periodistas, compromiso realizado por la Fiscalía General de la Nación desde 1999, **ésta nunca fue creada**”. Igualmente, “hubo crímenes que nuca se investigaron, la quinta parte de 134 homicidios quedaron de tajo en la impunidad o no fueron investigados como crímenes por razón de oficio”.

**Amenazas contra periodistas quedan en total impunidad**

En lo que respecta a las amenazas, **el 60% de las denuncias no fueron investigadas**. “En Colombia sólo son investigadas 4 de cada 10.” El informe indica que “entre los años 2006 a 2014, la Fiscalía General sólo abrió indagaciones por 251 de las 629 amenazas que se registraron en el país”. Fue en este periodo que se logró la única condena por amenazas contra periodistas mientras que 247 casos se quedaron en etapa de indagación por lo que no tuvieron investigación formal. (Le puede interesar: "[Crisis laboral y agresiones contra periodistas](https://archivo.contagioradio.com/crisis-laboral-y-agresiones-contra-periodistas/)")

De igual forma, el Equipo manifiesta que en muchas ocasiones **los periodistas no instauran la denuncia de las amenazas lo que puede significar un aumento en las cifras**. Destacan lo dicho por la Federación Colombiana de Periodistas FECOLPER, quienes manifiestan que “si bien no todas las amenazas terminan en asesinatos, sí alteran la vida y el trabajo de los periodistas, que se ven obligados a invertir su tiempo en buscar formas de prevenir eventuales ataques y en ocasiones a cambiar su enfoque de trabajo para disminuir las presiones”.

Finalmente, el informe hace alusión a las cifras que maneja por ejemplo Reporteros Sin Fronteras que indica que en 2016 Colombia ocupaba el puesto **124 de 180 países de los más peligrosos para ejercer el periodismo.** De igual manera, FECOLPER, entre enero y junio de 2017 136 casos de agresiones contra la prensa.
