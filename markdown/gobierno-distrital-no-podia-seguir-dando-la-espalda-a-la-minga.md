Title: Gobierno distrital no podía seguir dando la espalda a la Minga
Date: 2020-10-17 17:19
Author: AdminContagio
Category: Actualidad, Nacional
Slug: gobierno-distrital-no-podia-seguir-dando-la-espalda-a-la-minga
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/10/gobierno-distrital-palacio-de-los-deportes.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right"} -->

[Foto: bogotá.gov.co]{.has-inline-color .has-cyan-bluish-gray-color}

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

  
La minga continua su recorrido hacia la ciudad de Bogotá, luego de iniciar movilización desde Cali el pasado 14 de octubre, con el fin de al llegar a la ciudad sostener un diálogo con el presidente Iván Duque, sobre medidas concretas para mitigar y acabar con la violencia, el despojo de tierras, desplazamiento forzado a los que han sido sometidos los indígenas campesinos de las comunidades del Cauca y el pleno cumplimiento del acuerdo de paz, firmado en la Habana.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Luis Ernesto Gómez, secretario de Gobierno de Bogotá, aseguró que los integrantes de la minga se comprometieron a protestar pacíficamente y cumplir las normas sanitarias una vez lleguen a la capital. Lea también [Minga avanza contra viento y marea](https://archivo.contagioradio.com/7-000-indigenas-de-la-minga-buscan-un-dialogo-con-duque-en-bogota/).

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Esta decisión es casi que una obligación a la que se indujo al gobierno distrital luego de las múltiples críticas por la actitud de la Alcaldía frente a la actuación del ESMAD en contra de la protesta social.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Gobierno Distrital no podía seguir quedando mal a la movilización social

<!-- /wp:heading -->

<!-- wp:paragraph -->

Por ello, se confirmo que el lugar que recibirá a la minga indígena una vez arribe a Bogotá, será el Palacio de los Deportes. 7000 personas se espera que lleguen a la ciudad este domingo 18 de octubre.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El Distrito aseguro que ya hay funcionarios de la Alcaldía adecuando el espacio en el palacio con el fin de albergar a los indígenas, cumpliendo todas las medidas de bioseguridad, teniendo en cuenta la situación de salubridad por la pandemia. [Lea también: Minga es expresión de unidad](https://archivo.contagioradio.com/la-minga-es-una-expresion-de-unidad-en-medio-de-la-violencia/)

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Cabe recordar que la administración distrital es la encargada de organizar la llegada de la minga, pues el Gobierno se negó a gestionar la llegada de la población.

<!-- /wp:paragraph -->
