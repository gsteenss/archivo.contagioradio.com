Title: Comunidad Arhuaca continúa sin garantías en Pueblo Bello, Cesar
Date: 2017-07-17 16:09
Category: DDHH, Nacional
Tags: Comunidad Arhuaca, Sierra Nevada
Slug: comunidad-arhuaca-continua-sin-garantias-en-pueblo-bello-cesar
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/07/desalojo-pueblo-bello-cesar-3.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Pueblo Arhuaco] 

###### [17 Jul 2017] 

Las 180 familias Arhuacas que habían sido desalojadas continúan en el parque central de Pueblo Bello, en del departamento del Cesar, sin ser reubicadas y sin lograr establecer algún acuerdo con las instituciones que garanticen el cumplimiento de sus derechos y el retorno a su territorio ancestral, **así mismo tampoco han recibido ninguna clase de ayuda humanitaria de emergencia.**

Gunnawia Chaparro indígena integrante de esta comunidad denunció que ha existido un descuido por parte de las autoridades competentes **y recordó que en estas familias hay más de 170 niños y niñas, además personas de la tercera edad**. (Le puede interesar:["180 familias del Pueblo Arhuaco serían desalojadas en Pueblo Bello, Cesar"](https://archivo.contagioradio.com/pueblo-arhuaco-desalojado-pueblo-bello/))

Una comisión de indígenas se encuentra en Bogotá reuniéndose con diferentes entidades como la Defensoría del Pueblo, para **frenar el desalojo por parte del ESMAD y plantear la falta de garantías por las que atraviesa esta comunidad Arhuaca, que con este completa su tercer desalojo.**

De igual forma en el transcurso del día se espera que se desarrolle una reunión con el Ministerio del Interior, **quien previamente había manifestado que este desalojo no estaba permitido**. (Le puede interesar:["Colombia ha perdido el 92% de la Sierra Nevada de Santa Marta"](https://archivo.contagioradio.com/colombia_pierde_sierra_nevada_santa_marta/))

El 12 de marzo de este año la comunidad indígena Arhuaca, decidió ocupar el predio que fue asignado, de manera insculta, por parte del INCORA a un tercero denominado Tikumukin, a pesar de que este se ubicaba entre los límites del territorio ancestral indígena. **El 31 de marzo el ESMAD arremetió por primera vez, obligándolos a abandonar momentáneamente este lugar.**

<iframe id="audio_19846203" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_19846203_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
