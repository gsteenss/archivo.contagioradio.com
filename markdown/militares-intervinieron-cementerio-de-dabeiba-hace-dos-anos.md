Title: Militares intervinieron cementerio de Dabeiba hace dos años
Date: 2019-12-16 14:52
Author: CtgAdm
Category: Comunidad, Entrevistas
Tags: Cementerio, Dabeiba, desaparecidos, falsos positivos
Slug: militares-intervinieron-cementerio-de-dabeiba-hace-dos-anos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/Cementerio-de-Dabeiba.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: @JEP\_Colombia  
] 

Este fín de semana se hizo público el descubrimiento, por parte de la Juridicción Especial para la Paz (JEP), de una fosa común que tendría al menos 50 víctimas de ejecuciones extrajudiciales en el cementerio Las Mercedes de Dabeiba, Antioquia. En entrevista con Contagio Radio, **Adriana Arboleda, directora de la Corporación Jurídica Libertad y vocera del MOVICE sobre este tema**, explicó cómo fue posible hacer este descrubrimiento, y exlicó que se debe proteger este cementerio, y otros que están en zonas donde hubo conflicto, para evitar que se los intervenga y desaparezcan las evidencias.

### **Contagio Radio: ¿Cómo fue posible que la JEP interviniera el cementerio de Dabeiba?** 

Adriana Arboleda: Fueron dos hechos: Por un lado, en el marco del Caso 003 que lleva la JEP, e involucra la responsabilidad de agentes estatales en “Muertes ilegítimamente presentadas como bajas en combate por agentes del Estado”; y por otro lado, la solicitud de medidas cautelares que el Movimiento Nacional de Víctimas de Crímenes de Estado (MOVICE) presentó el 30 de agosto de 2018, y que este año pedimos que se ampliara a los cementerios ubicados en el municipio de Dabeiba, en el occidente antioqueño. Entonces se juntaron ambas cosas, porque en el marco del Caso 003, uno de los comparecientes señaló que él había participado en ejecuciones extrajudiciales y que varias de esas víctimas estaban inhumadas en el cementerio de Dabeiba, lo que coincidía con la información que desde el MOVICE habíamos presentado, eso fue lo que generó que la JEP decidiera intervenir ese lugar para corroborar la información, y avanzar en la identificación de los casos.

### **CR: En entrevistas con comunidades de Dabeiba han señalado que hay otras zonas donde también hay desaparecidos, ¿ustedes tienen identificadas esas zonas?** 

**AA:** Lo que dicen los campesinos y habitantes de la zona es totalmente cierto. De hecho, en la solicitud que nosotros presentamos estamos hablando de otros cementerios. En el mismo casco urbano, cerca del Cementerio católico Las Mercedes, está el cementerio administrado por la iglesia presbiteriana que no sabemos si pudo haber sido utilizado, dada la cercanía, por los militares. También está el cementerio de la vereda El Salado, y sobre todo hay tres corregimientos que a nosotros nos interesan mucho que son el de Camparrusia, el de San José de Urama y el de la Balsita.

Tanto la Corporación Jurídica Libertad, el MOVICE como la Comisión Intereclesial de Justicia y Paz han denunciado en varias ocasiones que estos lugares fueron zonas donde pobladores fueron asesinados y presentados como muertos en combate, o los pobladores fueron desaparecidos. Entonces nosotros pensamos que estos tres lugares son fundamentales de intervenir. (Le puede interesar: ["La IV edición Festival de las Memorias tendrá lugar en La Balsita, Dabeiba"](https://archivo.contagioradio.com/festival-de-las-memorias-balsita-dabeiba/))

Hoy en concreto se requiere protección para evitar que puedan ser intervenidos, manipulados como al parecer ocurrió con el cementerio de Las Mercedes. Creemos que el problema no es solamente el cementerio en el casco urbano sino que hay una realidad en el Cañón de La Llorona, incluso en otros municipios cercanos, porque ese fue un corredor en el que operaron los paramilitares, y de hecho, cuando fuimos al cementerio de Las Mercedes había testimonios de paramilitares entre 1996 y 1998 diciendo que a ese cementerio llevaron sus víctimas. Pero también han operado distintos batallones de la Primera División, de la Séptima Divisón, de la Brigada 17.

Es una zona que reporta más de 300 víctimas de desaparición forzada. Entonces es muy importante eso que señalan los pobladores de que la intervención debe ser integral, y debe mirarse este cementerio así como otra serie de lugares de la región. (Le puede interesar: ["En cementerio de Tumaco podrían haber más de 150 personas sin identificar"](https://archivo.contagioradio.com/en-cementerio-de-tumaco-podrian-haber-mas-de-150-personas-sin-identificar/))

### **CR: ¿Qué dice lo revelado en el cementerio de Las Mercedes sobre cómo se dieron los casos de 'falsos positivos' en el país?** 

AA: Hay diferentes tipos de víctimas: Hay víctimas directas de desapariciones forzadas, es decir, personas de la región que fueron detenidas arbitrariamente, que fueron sustraídas del amparo de la Ley, ocultadas y asesinadas, seguramente, llevadas a estos cementerios; hay también víctimas de combatientes, que fueron ejecutadas cuando estaban de forma indefensa, y que fueron llevadas a estos lugares, osea, que no los atacaron en combate sino que al ser capturados fueron dados de baja sin que mediara ningún juicio ni ninguna protección y en una clara violación al Derecho Internacional Humanitario (DIH); y en tercer lugar está el caso de ejecuciones extrajudiciales que según los mismos testimonios, por los menos del militar que está dando información sobre Las Mercedes, **se trata de personas que fueron reclutadas en la ciudad de Medellín, fueron llevadas y asesinadas en las zonas rurales de Dabeiba e inhumadas en estos lugares.**

Pero también tenemos casos de, como decía, pobladores de la región: De La Balsita, de San José de Urama, de Santa Rita, que fueron asesinados y también fueron presentados como muertes en combate. Entonces ahí tenemos una serie de víctimas por las que es importante que se indague y que compromete la responsabilidad de grupos paramilitares que actuaron en connivencia con la Fuerza Pública, y acciones cometidas directamente por agentes estatales, como en este caso, la ejecución.

### **CR: Hay otros sitios en Colombia, el caso de Ituango o la Comuna 13 donde se dice que hay desaparecidos, ¿a qué lugares se debe voltear a mirar para encontrar a los desaparecidos?** 

AA: El MOVICE ha presentado solicitud sobre 17 lugares en 5 departamentos, en el caso de Antioquia está lo de La Comuna 13, el Cementerio Universal está relacionado con ese caso, los cementerios que están siendo afectados por el Proyecto Hidroituango, el cementerio de Puerto Berrío... recientemente salimos de una audiencia sobre los cementerios de San Onofre y Rincón del Mar en Sucre. Hemos solicitado que se revisen en algunos cementerios en el departamento de Caldas, en Barrancabermeja, Santander... pero yo creería que **es importante ahora que la Unidad de Búsqueda de Personas dadas por Desaparecidas (UBPD), en articulación con la JEP, diseñen un modelo de intervención en los cementerios en las zonas donde se desarrolló el conflicto armado** o en las zonas donde tenemos reportadas como lugares donde hay muchas víctimas de desapariciones forzadas o ejecuciones extrajudiciales.

Es importante hacer esos cruces de información y garantizar la protección de esos lugares, ahora está el 'boom' sobre el cementerio de Dabeiba, pero creo que se requiere urgentemente diseñar un plan de acción para evitar que esos lugares donde hay víctimas de ejecuciones extrajudiciales o desapariciones forzadas puedan ser alterados, porque la evidencia que tenemos es que este cementerio de Dabeiba fue alterado, posiblemente por los militares. Entonces, **dentro del Plan Nacional de Búsqueda que está adelantando la UBPD es fundamental definir una ruta de protección para todos estos lugares.**

### **CR: Cuando usted dice que los militares posiblemente alteraron el cementerio, ¿a qué se refiere?** 

AA: La semana pasada cuando estuvimos en las diligencias, el mismo militar que señaló la información fue claro en decir al momento de llegar al cementerio que ese lugar no estaba como cuando él estuvo, que fue en el año 2006-2007. Al parecer las cruces estaban mirando a otra dirección, pues cuando él estaba miraban hacia el sur y ahora miran hacia el oriente; nosotros nos dimos cuenta que todas las cruces estaban pintadas de blanco con una misma caligrafía, todas aparecían como muertes en el año 1972-1974 y esas cruces en esos lugares están en los mismos sitios o cerca de los que el militar señaló como donde habían inhumado las víctimas. O sitios donde se tiene información donde se tiene información que paramilitares inhumaron víctimas.

Luego, indagando, nos dijeron que **hace 2 años los militares llegaron a organizar el cementerio. Que lo habían puesto bonito, que lo habían pintado.** Nosotros creemos que ese es un tema que sí debe ser investigado, porque lo que pensamos es que **los militares pudieron remover cuerpos como una forma de garantizar impunidad sobre los hechos.**

### **CR: ¿Cuántas personas se cree que estarían en los 17 lugares donde el MOVICE pidió protección?** 

Es muy difícil determinarlo, por decir algo, en la Comuna 13 tenemos un listado de cerca de 300 víctimas, en Dabeiba cerca de 100 víctimas reportadas... es muy complicado y difícil comprometerse con un número. Es decir, tenemos en este país más de 80 mil víctimas de desaparición, y lo que necesitamos ahora es enfocarnos en el cruce de información. Yo insistiría en que ahí es importante mirar en cómo desde la UBPD se coordina toda esta acción, porque una de las realidades que tenemos en Colombia es que aún hoy no tenemos un censo único que nos permita tener claridad de cuántas víctimas y qué víctimas estamos hablando sobre desaparición.

Porque incluso en el caso de Dabeiba, como en San Onofre y en otros lugares, muchos de los casos aparecen como personas no identificadas, entonces el desafío realmente es enorme pero lo esperanzador es que ya empezamos a caminar y empezamos a tener unas claridades que nos van a permitir garantizar el derecho de las víctimas a saber la verdad y a que en algún momento se les pueda devolver a sus familiares. (Le puede interesar:["Desaparecidos podrían estar sepultadas en cementerios como personas no identificadas"](https://archivo.contagioradio.com/personas-desaparecidas-podrian-estar-sepultadas-como-nn/))

**Síganos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe><iframe id="audio_45516915" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_45516915_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
