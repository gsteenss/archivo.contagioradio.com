Title: Aguas: Democracia y Paz
Date: 2016-03-22 15:05
Category: CENSAT, Opinion
Tags: Agua, Ambiente, Mineria
Slug: confluencia-nacional-por-el-agua
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/03/rio-colombia.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Alejandro Arango 

#### **[Censat Agua Viva](https://archivo.contagioradio.com/censat-agua-viva/) - [~~@~~CensatAguaViva](https://twitter.com/CensatAguaViva)

###### [22 Mar 2016] 

### **Por una Confluencia Nacional por el Agua** 

En la actualidad, las luchas legítimas por la defensa del agua no sólo tienen que ver con el cuidado y la conservación del ambiente, sino sobre la construcción de un país para todos, en donde la decisión del manejo y uso de las aguas se conviertan en escenarios más democráticos y equitativos. Precisamente, en la crisis ambiental que atraviesa el país, el agua ha estado presente en las demandas y las denuncias de la población. Sin duda el agua será definitiva en el pos-acuerdo, puesto que tener aguas con calidad y cantidad suficiente es fundamental para la construcción de la paz en los territorios. Agua y democracia son determinantes para erigir un país con mayor justicia social y ambiental.

En Colombia el agua es un bien público, es decir que el Estado tiene la potestad de administrarlo. En términos de su uso, puede ser tanto público como privado, pero el Estado tiene el control y la vigilancia sobre ambos tipos de empleos. En este sentido, las concesiones de agua son el modo de adquirir los derechos de uso del agua en el país, siendo las autoridades ambientales, Autoridad Nacional de Licencias Ambientales y Corporaciones Autónomas Regionales, las encargadas de otorgarlas. Al respecto, en una reciente investigación realizada por María Cecilia Roa y Sandra Brown, basada en el estudio de la totalidad de los derechos de agua, se afirma que en el país la asignación de agua es extremadamente inequitativa. Según este estudio, el 1.1% de los dueños de concesiones de agua tienen el 62% del volumen de agua otorgado, y al aplicar el coeficiente GINI, instrumento para medir la desigualdad, se observa que es igual a 0.92 para todas las concesiones de aguas registradas. Sin lugar a dudas, una cifra realmente preocupante que atestigua un sistemático acaparamiento de aguas por medio del cual se concentra el liquido vital en manos de corporaciones e intereses privados funcionales a las actividades extractivas, y siendo aún peores que los registrados en términos de tierra.

No obstante, la defensa del agua y los territorios ha puesto en jaque en diversas ocasiones una política minero-energética que promueve un uso corporativo de las aguas, que no se preocupa por la disponibilidad de agua para garantizar el líquido vital, los alimentos y la energía a la población, sino para garantizar la rentabilidad de ciertas economías y compañías que venden a los mercados globales y se abastecen de los bienes comunes para sus actividades lucrativas. No cabe duda, que la defensa del agua y los territorios representa una disputa por la democracia y por la interpretación de los bienes comunes en un país con tan alto índice de injusticia social y ambiental. Ahora que la Corte Constitucional de Colombia con su sentencia 035/16, notificada en el mes del febrero, respalda la importancia de las decisiones de las autoridades locales en sus planes de ordenamiento territorial frente a los proyectos extractivos, las consultas populares, como las realizadas en el 2013 en Piedras y Tauramena, toman nuevamente relevancia en sus justas proporciones, y sobre todo la próxima a realizarse en Ibague – Tolima a favor del agua y las actividades productivas propias de su región.

[Hoy son diversas las expresiones y articulaciones en defensa del agua. Es así, que diversas organizaciones sociales, étnicas, comunitarias, movimientos sociales, procesos rurales y urbanos que desde su trabajo cotidiano defienden los territorios del agua hemos convocado a sumarse a una Confluencia Nacional por el Agua, en la perspectiva de construcción de paz en nuestro país. En sus propias palabras la Confluencia convoca] [a construir caminos de articulación para la acción, para la recuperación de saberes tradicionales y ancestrales del agua, para la creación de nuevas narrativas, nuevos lenguajes, y renovadas sensibilidades que promuevan el viaje, la fiesta, el encuentro y la minga. Permitir que el]***agua toque***[ a la sociedad en general con acciones puntuales y articuladas con la participación de diversos sujetos sociales que en su cotidianidad han defendido y defienden las aguas como condición necesaria para dignificar la vida.]

Sus principios se basan en el agua como un bien común, cuyo manejo debe estar en manos de las comunidades que trabajan para garantizar la salud en los territorios, exigiendo la responsabilidad del estado de garantizar su derecho a las poblaciones, reivindicando la movilización por el derecho fundamental al agua como una acción legítima, y que se levanta en contra de toda forma de privatización, mercantilización y acaparamiento de agua en los territorios.

En definitiva, la Confluencia Nacional por el Agua es una invitación para que la acción concreta se convierta en el horizonte de trabajo de todos y todas los colombianos, y que los territorios de agua sea el lugar de encuentro para construir paz en el país.
