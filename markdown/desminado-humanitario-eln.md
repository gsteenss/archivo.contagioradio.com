Title: Gobierno y ELN logran acuerdo de plan piloto de desminado humanitario
Date: 2017-04-06 12:22
Category: Nacional, Paz
Tags: acuerdos, ELN, Gobierno, proceso de paz
Slug: desminado-humanitario-eln
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/10/eln-y-gobierno.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto] 

###### [06 Abr 2017] 

**El marco de referencia que especificaría un programa piloto de desminado humanitario** que se abordará en el próximo ciclo de conversaciones de paz es uno de los priemeros acuerdos que anuncia la mesa de conversaciones entre el gobierno de Colombia y el Ejército de Liberación Nacional.

Las delegaciones de paz del ELN y el gobierno lograron este acuerdo en medio de la discusión sobre medidas de protección a la población civil y los combatientes que es uno de los puntos de la agenda acordada y que definió que en un primer momento trabajará las **medidas de desescalamiento y la participación de la sociedad.**

El comunicado de 4 puntos también señala que se continuará discutiendo el punto 1 de participación de la sociedad y también anuncia un acuerdo en los **términos de referencia para el Grupo de Países de Apoyo Acompañamiento y Cooperación al Proceso de Paz,** que en este momento recibe el respaldo de los países en que se desarrollarán las conversaciones pero también 5 países que son garantes. ([Le puede interesar: Los retos de las conversaciones gobierno y ELN](https://archivo.contagioradio.com/los-retos-de-las-conversaciones-de-paz-gobierno-eln/))

### **Acuerdo de la mesa demostraría que si se está avanzando ** 

Al mismo tiempo del anuncio sobre el plan de desminado las delegaciones agradecieron la hospitalidad del gobierno de Rafaél Correa y el acompañamiento de la comunidad internacional y por último anunciaron que el próximo ciclo de conversaciones continuaría en Ecuador y se iniciará el próximo 3 de Mayo. [(Lea también: Gobierno y ELN no pueden seguir escalando la guerra](https://archivo.contagioradio.com/gobierno-y-eln-no-pueden-seguir-escalando-la-guerra/))

Este primer acuerdo es calificado como positivo, puesto que se han presentado diferentes momentos de tensión dada la contnuidad de las acciones militares, tanto de las Fuerzas Militares como del ELN. Por estas razones es que varias organizaciones se han pronunciado sobre la necesidad de un **cese de hostilidades bilateral** para evitar que la guerra afecte lo que se trabaja en la mesa.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
