Title: La ciudadanía deberá ser veedora de la Jurisdicción Especial de Paz
Date: 2017-03-15 16:14
Category: Nacional, Paz
Tags: Comisión de Esclarecimiento de la Verdad, Jurisdicción Especial de Paz, Unidad de Busqueda
Slug: la-ciudadania-debera-ser-veedora-de-la-jurisdiccion-especial-de-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/03/víctimas.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: La Radio del Sur] 

###### [15 Mar 2017] 

De acuerdo con Diego Martínez, asesor jurídico en la mesa de La Habana e integrante del CPDH, si bien la estructura esencial de la Jurisdicción Especial de Paz se mantuvo con el Sistema de Verdad, Justicia, Reparación y no Repetición, la Comisión de Esclarecimiento de la Verdad y la Unidad de Búsqueda de Personas Desaparecidas, existen **algunas “antinomias” o contradicciones frente al tema de la participación de terceros en el conflicto y las cadenas de mando** de los agentes estatales, que los jueces deberán gestionar cuando entre en funcionamiento la JEP.

Martínez, advirtió que para resolver dichas divergencias es fundamental la participación, veeduría y seguimiento por parte de la sociedad civil en todo lo concerniente a la JEP, para que lo esencial, **“que es la reparación y garantías para las víctimas” no quede por fuera de ninguno de los engranajes.** ([Le puede interesar: Jurisdicción de Paz será una herramienta para las víctimas](https://archivo.contagioradio.com/jurisdiccion-de-paz-sera-una-herramienta-para-las-victimas/))

Señaló que las modificaciones que ha hecho el Congreso “no afectan la estructura y el funcionamiento de la JEP”, sin embargo, aseguró que **hay algunos sectores económicos y políticos que buscan “hacer de esta otra justicia, otro espacio de impunidad”,** por lo que la veeduría ciudadana es la herramienta con la que se cuenta para asegurar una implementación efectiva de dicha iniciativa legislativa.

Comenta que la tarea siguiente es para los jueces, quienes deberán elaborar el reglamento para la posterior aplicación de la JEP y quienes **“coordinarán los desarrollos mediante ley estatutaria”.** ([Le puede interesar: ¿Qué viene luego de la aprobación de la Jurisdicción Especial para la Paz?](http://¿Qué%20viene%20luego%20de%20la%20aprobación%20de%20la%20jurisdicción%20especial%20para%20la%20paz?))

Por último, resaltó que la movilización social, las jornadas de seguimiento con iniciativas como ‘Ojo a la Paz’ y la pasada ‘Operación Tortuga’, **son iniciativas que manifiestan el poder de la sociedad civil frente a las instituciones del estado** y “ayudan a ejercer presión para que se dé celeridad a los trámites.

<iframe id="audio_17570445" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_17570445_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
