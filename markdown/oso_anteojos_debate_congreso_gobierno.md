Title: Colombia se pone los anteojos por el oso andino
Date: 2018-02-21 13:18
Category: Animales, Voces de la Tierra
Tags: oso de anteojos
Slug: oso_anteojos_debate_congreso_gobierno
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/06/plan_conservacion_mongabay-1-e1496706484617.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Mongabay] 

###### [ 21 Feb 2018] 

Este miércoles se celebra a nivel mundial el día Mundial por la protección de los osos. El objetivo es promover la protección el oso frontino, oso andino, oso sudamericano, ucumari y jukumari, u oso de anteojos, que **desde el año 2001 pasó de una especie vulnerable a especie altamente vulnerable a extinción** según la declaratoria de la Unión Internacional para la Conservación de la Naturaleza (UICN).

Se trata de un mamífero que habita en 22 de las 59 áreas protegidas en Colombia, un animal muy importante ya que es un regulador y modificador de las condiciones de los bosques donde habita, no obstante es una especie amenazada por múltiples motivos, como son la ganadería, la captura para su comercio o uso como mascotas, la presencia de especies invasivas y la cacería ilegal.

Los dos últimos aspectos se dan debido a que los **lugares que habita están siendo invadidos por agricultores, extractores forestales, cazadores y otras formas de actividad humana como el desarrollo de infraestructura vial,** el uso indiscriminado de pesticidas, el crecimiento acelerado de la minería y la explotación petrolera. [(Le puede interesar: Anteojos rotos)](https://archivo.contagioradio.com/oso-anteojos-extincion/)

### **Los avances en la protección del oso andino** 

De acuerdo con Daniel Rodríguez, integrante de la Fundación Wii, esa situación ha generado el asesinato de osos de anteojos especialmente en departamentos de Nariño, Cundinamarca y Cauca, "debido a la llegada de la ganadería cuando se tumbaron los bosques para que vivieran las vacas".

Rodríguez explica que "El área que está protegida para el oso es sólo el 23% de su hábitat, y son las corporaciones regionales son las encargadas de proteger a las especies por conflictos humanos". De acuerdo con los defensores del osos, es necesario que haya una mayor articulación interinstitucional y se aumente presupuesto que se asigna para proteger a esta especie.

No obstante, desde la Fundación Wii reconoce que Colombia es el único país que le destina recursos a la protección de ese animal. En 2016, Parques Nacionales destinó \$1660 millones a **programas de conservación de éste mamífero, lo que representó un 2.9% del total asignado para esa institución. **"Colombia ha invertido dinero en la conservación del oso. Hemos logrado sacar al oso del sótano, se está visibilizando", concluye. Daniel Rodriguez. [(Le puede interesar: Por estas razones se debe proteger al oso de anteojos)](https://archivo.contagioradio.com/por-que-defender-el-oso-de-anteojos/)

<iframe id="audio_23955829" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_23955829_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u)  o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](https://archivo.contagioradio.com/).
