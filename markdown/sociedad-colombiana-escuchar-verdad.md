Title: ¿Está lista la sociedad colombiana para escuchar la verdad?
Date: 2019-03-28 16:56
Category: Nacional, Paz
Tags: Álvaro Leyva, presos, Reclusos, verdad
Slug: sociedad-colombiana-escuchar-verdad
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/03/Alvaro-Leyva.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: La Otra Cara] 

###### [27 Mar 2019] 

El pasado miércoles se conoció una carta con la que más de 200 prisioneros recluidos en La Picota, y agrupados en el Comité de Presos por la Verdad de Colombia expresaron su deseo de contar la verdad sobre 4 tipos de delitos cometidos en 45 territorios del país por 24 grupos armados ilegales. (Le puede interesar: ["Histórico: Por primera vez presos están dispuestos a revelar verdades sobre el conflicto"](https://archivo.contagioradio.com/historico-por-primera-vez-presos-estan-dispuestos-a-revelar-verdades-sobre-el-conflicto/))

Sobre esta información, Contagio Radio conversó con Álvaro Leyva, quién ha participado activamente en varios procesos de paz y en la creación del Comité.

### **Contagio Radio: ¿Por qué no se le está dando relevancia a esta comunicación?** 

**Alvaro Leyva:** Yo creo que se va abriendo campo porque es un tema que asusta. La verdad tiene que ver con los episodios más horrorosos de la historia de Colombia, y pone los pelos de punta que la gente que ha participado en actos de violencia manifieste que quiere contar qué sucedió y quiénes participaron en dichos actos.

La verdad es el éxito de un proceso que tiene amplios rechazos, de tal manera que muchos prefieren la cárcel que la verdad, pero esto llega un momento en que no lo ataja nadie. Por lo tanto, me siento muy agradecido de haber hecho parte de esta iniciativa. Estamos en un proceso que es muy doloroso, difícil y va a tener muchos obstáculos, porque a nadie le gusta que le 'quiten la cobija' y tenga que aceptar que hizo parte del conflicto directa o indirectamente no solo como combatiente, sino como auspiciador.

### **CR: Los presos decidieron hablar porque creen en el Sistema Integral de Verdad, Justicia, Reparación y No Repetición, ¿por qué se da esta confianza?** 

**AL:**El éxito del proceso colombiano estriba en que es algo que no tiene antecedentes, es que si uno se pone a ver la carta, hablan de temas relacionados con el conflicto y los mencionan: **asesinatos, desapariciones o el magnicidio del doctor Álvaro Gómez Hurtado;** quieren hablar sobre la autoría, quienes fueron los cómplices, las causas, las circunstancias. Aquí lo que ellos manifiestan es que darán a conocer nombres de personas que hasta el momento nadie sabe quiénes son ni dónde están.

Algunos de los que firman está carta se acogieron a la Ley Justicia y Paz, pero la favorabilidad señalada en esa norma les permite acudir a la justicia transicional actual, es decir que aquí se les puede abrir la puerta. (Le puede interesar: ["Es más fácil vincular la verdad con la paz, que la paz con la justicia: David Rieff"](https://archivo.contagioradio.com/es-mas-facil-vincular-la-verdad-con-la-paz-que-la-paz-con-la-justicia-david-rieff/))

Además señalan su deseo de entrar a la Jurisdicción Especial para la Paz. Aquí hay gente dispuesta a decir incluso dónde hay fosas comunes, de tal forma que así se quiera tapar el sol con las manos llega un momento en que el sol es superior, y **vamos a tener que ver y ser testigos de algo que no se ha visto en el mundo.**

### **CR: ¿La sociedad Colombiana está lista para escuchar la verdad?** 

**AL:** En un país donde hubo hornos crematorios, las grandes ciudades no se enteraron de lo que estaba ocurriendo; parte del problema es que la gente no conocía, no sabía o es incrédula, pero la carta comienza con algo que es espeluznante: **La historia de Colombia está en la cárcel, entonces démosle la oportunidad a los que están en la cárcel a que contribuyan**.

El texto señala cosas que son muy significativas para el proceso de hoy: el Comité está en la Modelo, pero ha recibido comunicaciones de personas en la clandestinidad y de presos en el extranjero que quieren aportar a la verdad y no quieren llevarse a la tumba secretos sobre la verdad del conflicto. (Le puede interesar: ["Antioquia y Eje Cafetero, listos para trabajar junto a la Comisión de la Verdad"](https://archivo.contagioradio.com/antioquia-y-eje-cafetero-listos-para-aportar-sus-relatos-a-la-comision-de-la-verdad/))

Se están abriendo también capítulos en otras cárceles del país, y están dispuestos a acogerse a la Jurisdicción para aportar verdad plena sobre actividades o conductas cometidas en desarrollo del conflicto, y las circunstancias de su comisión; y **también para brindar información necesaria y suficiente para atribuir responsabilidades, de tal forma que esto es con nombres propios**.

Se ha dicho que los civiles no asisten obligatoriamente a la JEP, puede que así sea, pero cuando trasciendan sus delitos y se sepa que son de lesa humanidad tendrán que escoger entre la jurisdicción ordinaria o la Jurisdicción. Entonces, que no crean que la realidad no los va a envolver y comprometer. (Le puede interesar: ["Militares respaldan a la JEP y mantienen su compromiso con la verdad"](https://archivo.contagioradio.com/militares-respaldan-jep/))

Y en la Comisión de la Verdad no se habla solo de las FARC o los militares, sino mucho más allá; la verdad toca a paramilitares y otras agrupaciones, de tal manera que cuando uno ve la lista que aparece en la Carta, parece increíble que hayan tantas agrupaciones y el país no se haya enterado. **Estamos en las vísperas de conocer una Colombia diferente para construir un país diferente**, si no, no tendría sentido el proceso de paz.

### **CR: Las personas se preguntan: ¿por qué creerle a unos delincuentes?** 

**AL:** Delincuentes son todos no solo los de la cárcel, hay delincuentes en oficinas y en las juntas directivas; y se debe creerles porque el Sistema que se está buscando es de contraste: con mecanismos de sistematización se van oyendo las diversas versiones y se van cruzando, esta no es la verdad de uno solo, aquí no hay principios de oportunidad, ni hay falsos testimonios.

Cuando hay cruces de información, se puede establecer la mentira, y el que haya incurrido en una mentira pierde los beneficios y puede llegar a purgar penas de hasta 20 años en prisión. El que vaya a acceder a esto tiene que pensarlo mucho, porque su verdad será contrastada con otro.

Tenemos que prepararnos y ver si hay inexactitudes que aflore, para que con el cruce y contrastación de la verdad, aflore la realidad de todo lo que sucedió en estas décadas de conflicto interno.  (Le puede interesar: ["Comisión de la Verdad sigue firme en su respaldo a la JEP"](https://archivo.contagioradio.com/comision-de-la-verdad-deja-en-firme-su-respaldo-a-la-jep/))

### **CR: ¿Cómo están trabajando con el Sistema Integral de Verdad, Justicia, Reparación y No Repetición?** 

**AL:** Se han dado pasos importantes, los comisionados están regionalizados, han venido recibiendo información en las regiones del país y eso ha permitido comenzar a cruzar información, así que cuando llegue alguien de una cárcel a hablar sobre cualquier tema, inmediatamente el caso al que alude aparece. La Comisión no ha perdido el tiempo, este no es un sistema penal, es un sistema constitucional en el que prima el interés de las víctimas, la verdad, la reparación y la no repetición.

Entonces ha habido mucho trabajo de la JEP, de la Comisión y de la Unidad (de Búsqueda para Personas Dadas por Desaparecidas). La gran sorpresa va a ser que a pesar de las objeciones, el Congreso y demás, se va a imponer la necesidad de que esta sociedad conozca la verdad gustele o no. Y es sobre la verdad como función de Gobierno; porque uno no puede gobernar sobre un país que no es.

El día que se conozca la verdad, toda la función pública tiene que adecuarse a la verdad y al hecho de que se debe superarla. Estamos en vísperas de un cambio fundamental que no se lograría con medidas menores, ahí es donde vale la fuerza de la Corte Constitucional y por eso debemos protegerla: porque allí está el funcionamiento de un mecanismo que es fundamental y constitucional.

Aquí hay enemigos para todo por un gran desconocimiento, aquí se piensa que se está viviendo en un momento de felicidad colectiva, pero para llegar a esa felicidad colectiva tenemos que destapar una olla podrida. (Le puede interesar: ["En la Orinoquía empresas expropian a 13 comunidades indígenas"](https://archivo.contagioradio.com/empresas-expropian-13-comunidades-denuncian-indigenas-orinoquia/))

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
