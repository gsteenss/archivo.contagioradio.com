Title: El pueblo palestino enfrenta la colonización bailando
Date: 2017-07-14 11:56
Category: Onda Palestina
Tags: Apartheid, BDS, Cultura, Israel, Palestina
Slug: palestino-cultura-dabke
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/07/baile-pales.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: taysideforjusticeinpalestine.org 

###### 14 Jul 2017 

A pesar de la fuerte militarización en territorios palestinos debido a la ocupación israelí, siguen manteniendo vivas sus tradiciones como forma de sobrevivir a la colonización que sufren. Uno de las mejores expresiones de esto es **el Dabke**, un baile que viene de la tradición de zapatear el barro para que sea manejable y se pueda moldear de acuerdo a las necesidades en las construcciones tradicionales.

Este baile, que también hace parte de la identidad cultural de otros pueblos de la región como son el **Líbano, Siria, Jordania e Iraq**, tiene en Palestina un especial arraigo, esto gracias a grupos culturales como la compañía **El-Funoun Palestina Popular Dance Troupe**, una organización que es reconocida por su resistencia a la ocupación a través de la reivindicación de las canciones y danzas populares de Palestina.

El-Funoun fue fundada en 1979 y es la primera compañía de artes populares mixtas de Cisjordania creada después de la ocupación de 1967. Con los **cerca de 80 bailarines y al menos 40 voluntarios más, han logrado mostrar sus espectáculos a nivel local, regional e internacional**, incluyendo campos de refugiados palestinos.

En varias declaraciones sus co-fundadores **Mohammad Yaacoub, Mohammed Atta y Wassim al-Kurdi**, han explicado que el interés común fue preservar el folklore palestino, seguros de que esto hace parte de la resistencia cultural del pueblo palestino a la ocupación israelí. En una declaración, por ejemplo, Yaacoub afirmó que la compañía se creó para “hacer parte del cambio, porque creen que **el arte es una parte del movimiento revolucionario y popular contra la ocupación**”.

<iframe id="audio_19809202" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_19809202_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Escuche esta y más información sobre la resistencia del pueblo palestino en [Onda Palestina](https://archivo.contagioradio.com/category/programas/onda-palestina/), todos los Martes de 7 a 8 de la noche en [Contagio Radio](http://bit.ly/1ICYhVU). 
