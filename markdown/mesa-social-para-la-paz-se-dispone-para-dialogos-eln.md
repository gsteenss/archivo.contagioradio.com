Title: Mesa Social para la paz se dispone para diálogos con ELN
Date: 2016-10-24 16:56
Category: Nacional, Paz
Tags: Diálogos ELN, ELN, paz
Slug: mesa-social-para-la-paz-se-dispone-para-dialogos-eln
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/10/DialogosELN1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: ] 

###### [24 de Oct 2016] 

En su última alocución el presidente Santos anunció el nombramiento de Juan Camilo Restrepo como el jefe negociador y reitero que este jueves 27 de octubre se instalaría la mesa  **“si se dan las condiciones”** afirmación que **pone en duda el inicio de las conversaciones** y que ajusta los tiempos para establecer  la metodología de participación que tendrá la sociedad civil en la Mesa Social por la Paz.

Por su parte el ELN, aunque le dio la bienvenida al anuncio de Santos, señalo en su cuenta de Twitter que **“las declaraciones de J.C Restrepo torpedean acciones mutuas que faltan antes del 27 de octubre”**, esto debido a  que el jefe negociador exhortó a la guerrilla a que libere al ex congresista Odín Sánchez Montes de Oca, para que comiencen los diálogos.

Sin embargo, una de las definiciones que debe tomarse y que podría estar debatiendo el gobierno y la guerrilla del ELN, es **el mecanismo de selección de personas que tendrían la posibilidad de participar muy cerca del escenario de las negociaciones**, los contenidos y las formas de llevar a cabo esa participación.

Este escenario de participación se constituye por componentes diversos, por un lado la comunidad internacional, ya que se realizara en países como Venezuela, Ecuador y Brasil, y por otro lado están los escenarios regionales y locales. Le puede interesar: ["Conversaciones con el ELN apuntan a democratizar Colombia: Pablo Beltrán"](https://archivo.contagioradio.com/conversaciones-con-el-eln-apuntan-a-democratizar-a-colombia-pablo-beltran/)

### Mesa Social para la Paz 

Frente a los avances que hay en la Mesa Social para la paz, Marylen Serna, vocera de Congreso de los Pueblos y miembro del comité de impulso de la mesa, afirmó que por el momento se **está construyendo la agenda con los puntos que más dudas generar al interior de las comunidades**, por otro lado indicó que se están estableciendo diálogos con otros sectores de la sociedad para garantizar una amplia participación, sin embargo aclaró que esta Mesa Social estará supeditada a los mecanismos y dinámicas que se den en la mesa de Quito.

**El próximo 3 de noviembre se llevará a cabo el lanzamiento de la Mesa Social para la Paz** en donde estarán los sectores que por el momento han decidido aunarse a este escenario. “Estamos aprestándonos para que el tejido social que se forme sea fuerte y con acuerdos entre la diversidad y la diferencia para que cuando llegue esa oportunidad de la participación, podamos tener acuerdos como parte de la sociedad colombiana” afirmó Serna. Le puede interesar: ["Organizaciones Sociales porponen diálogo nacional en mesa ELN-Gobierno"](https://archivo.contagioradio.com/organizaciones-sociales-proponen-dialogo-nacional-en-mesa-eln-gobierno/)

<iframe id="audio_13458834" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_13458834_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>
