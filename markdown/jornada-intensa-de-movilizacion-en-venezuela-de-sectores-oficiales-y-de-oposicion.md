Title: Jornada intensa de movilización en Venezuela de sectores oficiales y de oposición
Date: 2016-09-01 13:36
Category: El mundo, Política
Tags: oposición en Venezuela, Venezuela
Slug: jornada-intensa-de-movilizacion-en-venezuela-de-sectores-oficiales-y-de-oposicion
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/09/telesur.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Telesur] 

###### [1 Sep 2016] 

**Se llevó a cabo en Venezuela una jornada intensa de movilización** por parte de los sectores opositores que convocaron a la “Toma de Caracas” y el sector oficialista que salió a las calles del país en defensa de las políticas del gobierno del presidente Maduro y de la estabilidad democrática.

Las dos movilizaciones se desarrollaron en diferentes ciudades del país, siendo la avenida Bolívar en el centro de Caracas, el punto más álgido, sin embargo, la ruta de movilización de la oposición no fue fijada desde un principio y tomó otras vías de la ciudad, en consecuencia, **el gobierno hizo un llamado para evitar la confrontación y acciones violentas.**

Carlos Ramírez, encargado continental del ALBA Movimientos, asegura que el g**obierno venezolano estaría tomando medidas para garantizar la estabilidad y los derechos de la ciudadanía** durante las protestas, ante la posibilidad que estas se prolonguen por algunos días más.

Estas acciones hacen parte de un plan estatal para impedir la desestabilización del país, entre las que se cuentan el **garantizar el abastecimiento y asegurar las condiciones básicas para que la vida** continúe, mantener en funcionamiento el sistema de servicios, generar un despliegue de inteligencia militar y de acciones policiales que han tenido diferentes  frutos como[detenciones de personas que portaban aparatos explosivos](https://archivo.contagioradio.com/vicepresindete-de-venezuelaa-alerta-plan-desestabilizacion-en-marcha-toma-de-caracas/)y finalmente hacer un llamado al pueblo para que estén alerta y mantengan los ánimos sin entrar en confrontaciones.

**“El pueblo en la calle es lo que ha garantizado la resistencia y la continuidad de este proceso**, por eso desde diferentes organizaciones se está en acciones masivas con el fin de seguir y hacer presencia para evitar cualquier acto de avance de la oposición” afirmo Ramírez.

Por su parte, la oposición esta convocando a un cacerolazo para "celebrar el éxito de la toma de Caracas y reclamar el revocatorio", además el próximo miércoles 7 de septiembre, se harán movilizaciones hacía las sedes de cada Consejo Nacional Electoral y el 14 del mismo mes, se llevarán a cabo protestas en todas las capitales por 12 horas.

<iframe src="http://co.ivoox.com/es/player_ej_12744860_2_1.html?data=kpeklpmcepGhhpywj5WcaZS1lJmah5yncZOhhpywj5WRaZi3jpWah5yncaTV09Hc1ZC2pc6ZpJiSo6nWqduZk6iYo7GmhYzB0NvWz87JstXj1JKSmaiRh9Di1cbUy9SPlsLYytSYj4qbh46o&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
