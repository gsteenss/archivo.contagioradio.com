Title: Indígenas denuncian agresiones con arma de fuego por parte del ESMAD
Date: 2017-11-01 16:56
Category: DDHH, Nacional
Tags: Cauca, ESMAD, indígenas, Minga por la vida
Slug: indigenas-denuncian-agresiones-con-arma-de-fuego-por-parte-del-esmad
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/11/Resguardo-Kokonuko.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:] 

###### [01 Nov 2017] 

Indígenas que se encuentran movilizándose en la Minga, sobre la vía Buenaventura – Cali, señalaron que son falsas las acusaciones que se han hecho por parte de la Policía, en donde afirman que los manifestantes los han atacado con artefactos explosivos y expresaron tener pruebas de ataques con armas de fuego a los manifestantes. **Hasta el momento hay un reporte de 7 personas heridas en este punto conocido como La Delfina y 5 uniformados**.

De acuerdo con el último reporte de la ONIC una de las personas heridas es de gravedad y estaría a punto de perder su ojo, además Carlos Quiro, vocero de la movilización, afirmó que los indígenas han recogido municiones de escopeta que comprueban que la Fuerza Pública **ha usado armas de fuego y granadas durante los enfrentamientos contra la comunidad y que estas pruebas ya fueron puestas en conocimiento de la Fiscalía**.

Sumado a estas violaciones a los derechos humanos, los indígenas también denunciaron la **estigmatización por parte de la Fuerza Pública, la desaparición de líderes y amenaza gobernantes**, que se vienen dando desde hace más de un año en los diferentes territorios indígenas. (Le puede interesar:["Desde firma del acuerdo de paz reportaron 40 indígenas asesinados: OPIAC"](https://archivo.contagioradio.com/indigenas-asesinados-acuerdo-paz/))

El día de hoy se mantendrá el cierre de la vía y de acuerdo con Angelino, líder de la movilización indígena, habría posibilidades para que los transportadores también frenaras sus actividades y se unieran a la Minga, **debido a que tampoco ha existido cumplimiento en los acuerdos pactados con la población de Buenaventura**.

De acuerdo con el último informe nacional de la ONIC, el día de hoy se ha registrado dos heridos de gravedad, uno con un impacto de bala en el pie y el otro en la mano, que se suman al reporte de ayer. **De igual forma indígenas en Caldas, Risaralda, Quindío y Chocó, denunciaron el accionar desmedido por parte de la Fuerza Pública**. (Le puede interesar: ["Las razones de la Minga Indígena en cifras"](https://archivo.contagioradio.com/las-razones-de-la-minga-indigena-en-cifras/))

<iframe id="audio_21827959" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_21827959_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
