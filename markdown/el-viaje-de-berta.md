Title: El viaje de Berta
Date: 2016-09-19 16:34
Category: Eleuterio, Opinion
Tags: Berta Cáceres, Copinh, Defensores ambiente, honduras
Slug: el-viaje-de-berta
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/09/berta.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Medico International 

#### Por **[Eleuterio Gabón ](https://archivo.contagioradio.com/eleuterio-gabon/)** 

###### 12 Ago 2016 

Habían matado a su madre. Tal vez, las prolongadas amenazas y la certeza física del peligro, le habían hecho asumir de alguna forma lo inasumible, el fatal asesinato de su mami. El caso es que su rostro cansado trasmitía una gran fuerza. Era la fuerza de la convicción, del espíritu de resistencia y la tradición de lucha por la supervivencia que tienen quienes pertenecen a pueblos maltratados pero irreductibles. De modo que la joven hizo su pequeño equipaje y junto a una buena amiga de su mamá, cruzó el océano para hablar a la cara con los poderosos que gobiernan el destino de los pueblos. ¿Se imaginan? Dos mujeres indígenas recorriendo Europa para denunciar a gobiernos y bancos por su apoyo y su financiamiento a los crímenes que suceden en Centroamérica. Así fue.

Berta Zuñiga Cáceres y Rosalina Domínguez, indígenas lencas de Honduras, hija y compañera de lucha respectivamente de Berta Cáceres, asesinada por su defensa del medio ambiente y de los derechos humanos en su país, hicieron ese viaje. Durante tres semanas recorrieron Holanda, Alemania, Finlandia, Bélgica y España. Se entrevistaron con altos dirigentes, como los del banco de desarrollo holandés FMO, el finlandés Finnfund y las empresas alemanas Siemens y Voith-Hydro; también con alcaldes y políticos de ciudades como Barcelona, Zaragoza, Madrid y Valencia.

Les dijeron que dejaran de financiar a las empresas que construyen, las hidroeléctricas y minas que destruyen los territorios indígenas y sus formas de vida y de respaldar al gobierno hondureño, hijo de un golpe de estado, que lo permite. Denunciaron a esos bancos y grandes corporaciones que se dicen impulsores del desarrollo sostenible, de la energía limpia y del respeto a los derechos humanos y apoyan con sus inversiones a las empresas que no dudan en asesinar a quienes rechazan y se oponen a sus negocios de saqueo de la riqueza del país.

Berta Cáceres fue asesinada por defender el derecho de las comunidades indígenas a decidir sobre sus propios territorios, por oponerse a los megaproyectos que destruyen la vida allá donde aparecen, por denunciar la impunidad y la vulneración de los derechos humanos que vive su país.

“Su asesinato pone en evidencia lo que viene sucediendo en nuestro país desde hace varios años. Pero también señala las condiciones tan difíciles en que los movimientos sociales vienen luchando para defender la tierra, la vida y la dignidad humana.” Todo esto recordaron y denunciaron en su viaje… Lanzaron su mensaje y demandaron la solidaridad internacional.

“Se han hecho muchas movilizaciones, consignas, mensajes, grafitis en las calles con su cara… Muchos que no la conocían ahora se solidarizan. Debe ser sostenida en el tiempo la solidaridad con todas las luchas que compartimos, más allá de los cambios de tema de los medios de comunicación inmediatistas. No nos vamos a parar, tenemos un legado digno y resistente, no nos hemos quedado sin voz, tenemos muchas voces. Ella nos ha enseñado cómo defender nuestra comunidad. Berta no ha muerto, se ha multiplicado.”
