Title: Integrantes de las FARC asesinados en Nariño no habrían muerto en combates
Date: 2017-10-19 14:49
Category: DDHH, Nacional
Tags: excombatientes, FARC, nariño, Tumaco
Slug: integrantes-de-las-farc-asesinados-en-narino-estaban-en-sus-casas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/10/aldemar-garcia.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Resis Tiza Ananda Thandavam] 

###### [18 Oct 2017] 

Luego de conocerse la noticia del asesinato de seis integrantes de la Fuerza Alternativa Revolucionaria del Común en el sector de San José del Tapaje, en el punto Isupí, Municipio de El Charco, el pasado 15 de octubre, se pudo establecer que varios de los ex combatientes pertenecían anteriormente al frente 29, salieron del Espacio de reincorporación Aldemar Galán y estaban en sus casas, saludando a sus familias.

El comunicado emitido por el Espacio Territorial Aldemar Galán, señala a una banda encabezada por dos hombres; Robinson Alirio Cuero Obando (Alvaro Galán) y  Eliecer García Estupiñan (Marcos Arteaga) como los responsables de los hechos que acabaron la vida de los excombatientes,

En comunicación con **Ramiro Cortes,** miembro de FARC y de la dirección del espacio territorial, explicó que varios de los excombatientes asesinados salieron a sus casas luego de cumplir con el proceso establecido para la reincorporación. Sobre cómo sucedieron los hechos no se conoce mayor información debido a la incomunicación en el lugar.

Igualmente, Cortés aseguró que **se puede desmentir que se presentaron combates o enfrentamientos** dado que quienes suministraron las primeras informaciones no hablaron de ningún tipo de acciones armadas.

El Espacio Territorial Aldemar Aldana rechazó que se presente a estos grupos como "disidentes" y piden que se investigue si hay participación de alguna instancia de poder o del estado  detrás de las acciones contra los excombatientes de las FARC.

Por otra parte el comunicado de la zona de reincorporación  señala que habían denunciado ante el Mecanismo de Monitoreo y Verificación hechos que venían sucediendo y que serian una señal del crimen contra los integrantes de las FARC.

### **Nombres de los excombatientes:** 

-   José Miller Estupiñan Toloza ( Alexis Estupiñan)
-   Carlos Sinisterra (Kevin Gonzales)
-   Edinson Martínez Ordóñez ( Carlos "Pescadito" Perea)
-   Duber Alberto Obando Vallencilla - (Junior Velásquez)
-   José Alfredo García Estupiñan (Bruno Suarez)
-   Johan (No se conoce su nombre completo)

### **La situación del excombatientes en la Zona de Reincorporación**

Según Ramiro Cortes muchos de los ex combatientes que hacen parte de ese espacio territorial han salido de allí porque no se ha cumplido completamente la instalación de la infraestructura necesaria para el desarrollo de los proyectos productivos. Reiteró también que hay mucha inconformidad por situaciones de inseguridad que el Estado no ha atendido.

<iframe id="audio_21564592" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_21564592_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
