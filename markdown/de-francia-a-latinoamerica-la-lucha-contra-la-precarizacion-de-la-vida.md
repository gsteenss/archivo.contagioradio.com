Title: De Francia a Latinoamérica, la lucha contra la precarización de la vida
Date: 2019-12-09 17:04
Author: CtgAdm
Category: El mundo, Movilización
Tags: francia, Huelga, Paro Nacional, Reforma Pensional
Slug: de-francia-a-latinoamerica-la-lucha-contra-la-precarizacion-de-la-vida
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/Protestas-en-Francia.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: @Graphenes1  
] 

Francia completa cinco días en protestas contra la reforma pensional que reduciría los ingresos de los pensionados. Sin embargo este proceso de movilización habría comenzado hace más de un año con las conocidas protestas de los chalecos amarillos, incluso, desde 1995 cuando las calles se colmaron de huelguistas que lograron evitar la reforma pensional propuesta por el gobierno de Jacques Chirac. (Le puede interesar: ["Con un 65% de votos Emmanuel Macron es el nuevo presidente de Francia"](https://archivo.contagioradio.com/emmanuel-macron-nuevo-presidente-de-francia/))

### **Las razones de las manifestaciones: Mucho más que una reforma** 

Según expresó Pablo, profesor en economía y doctor en sociología política, las manifestaciones en Francia iniciaron hace un año con los chalecos amarillos, que aparecieron luego del aumento de la gasolina, sin embargo, **"lo que sucede es más profundo que el aumento del precio de un elemento en particular o una reforma"**. Igual que en [Chile](https://archivo.contagioradio.com/chile-los-efectos-de-un-modelo-economico-fallido/), las protestas responden a unas degradación en las condiciones de vida: Si bien Francia es la sexta potencia económica mundial, cuenta con más de 9 millones de personas que viven bajo la línea de la pobreza, entre las cuales, 5 millones viven con menos de 450 euros al mes (lo que representa un 30% del salario mínimo del país, establecido en 1.521 euros).

A esta tensión se agregó la de un Proyecto de Reforma Jubilatoria, de la cual solo hay orientaciones. Dicha reforma incluiría la cotización al sistema por puntos (haciendo que las personas paguen por dichos puntos), y aunque el Gobierno ha asegurado que el valor del punto no se moverá, según señala el Docente, la inversión del Estado en las pensiones se mantendrá igual pese a que cada vez más personas se jubilarán**, lo que hará que necesariamente el valor del punto baje, y se reduzca la pensión de quienes actualmente estén jubilados.**

### **No se veía una movilización como la de hoy, desde los 90's** 

Pablo sostuvo que desde 1995 no se veía una movilización tan fuerte como la que está viviendo actualmente Francia, cuando el gobierno de Alain Juppé, primer ministro de Jacques Chirac intentó hacer una reforma jubilatoria. En ese momento, se presentó una huelga general y el Gobierno tuvo que retroceder en su intensión, "pero no por la presión en las calles sino porque **la huelga general le hizo perder mucha plata a los empresarios y fueron ellos quienes presionaron para que retirara la reforma"**, afirmó el Profesor.

### **La guerra de relación de fuerzas: Macron vs sindicatos** 

Para Pablo, lo que se juega en este momento con la protesta es una batalla de tiempo para saber quién aguanta más: Los empresarios (que están divididos entre el sector financiero que vería fortalecidos los fondos privados y los que no se ven beneficiados por la reforma) o los trabajadores que han ido a huelga. En ese sentido, concluyó que no es en las manifestaciones en calle, sino en la huelga general que se juega todo, porque si no hay sueldos, los trabajadores se cansan y la huelga fracasa.

En consecuencia, el Académico manifestó que hoy el movimiento obrero "no tiene muchos cartuchos", pues la tasa de sindicalización en el país galo es baja, y **si no se establecen cajas de solidaridad y se hacen paros en sectores claves como el de transportes, la huelga será difícil de sostener en el tiempo**. (Le puede interesar: ["La unión del pueblo y la persistencia: claves en la movilización de Chile"](https://archivo.contagioradio.com/la-union-del-pueblo-y-la-persistencia-claves-en-la-movilizacion-de-chile/))

Por otra parte, Pablo dijo que el Gobierno ha respondido en dos vías: El plano comunicacional, que usa los canales de televisión con mayor audiencia para decir que las personas no entendieron la reforma pensional; y la represión, pues hace un año "la Policía está cometiendo acciones ilegales contra manifestantes, la mayoría de veces pacíficos". Respecto al accionar de las fuerzas de Policía, recordó que hasta el momento 25 personas perdieron un ojo por cuenta de los disparos con balas de goma, y una persona murió en su propia casa.

Mientras tanto, en las calles también se mantiene una tensión, porque los manifestantes en la calle rechazan los partidos políticos y los sindicatos evitando que se generen liderazgos que controlen el paro, pero al tiempo, esa carencia de liderazgos se lee como una falta de organización, y como una de las razones por las cuales no se adelantan acciones de mayor envergadura. (Le puede interesar:["Lecciones históricas de la movilización social colombiana, por Mauricio Archila"](https://archivo.contagioradio.com/lecciones-historicas-de-la-movilizacion-social-colombiana-por-mauricio-archila/))

Con este contexto, la tensión de Macron vs La Huelga se mantiene en las calles sabiendo que al movimiento sindical le falta fuerza y organización, mientras el Gobierno tiene el tiempo de su lado. Sin embargo, Pablo resaltó que las elecciones de 2022 podrían tener incidencia, si Macron se interesa por reelegirse, pues otra fuerza podría llegar a disputarle el poder en la primera vuelta.

### **Violencia o no violencia, una discusión que también se da en Francia** 

En Colombia, en el marco del Paro Nacional se han escuchado voces que apoyan radicalmente la protesta pacífica y rechazan de plano cualquier forma de protesta que no se enmarque en la no violencia, así, han llegado a pedir incluso que no exista la [primera línea](https://archivo.contagioradio.com/nace-en-colombia-la-primera-linea-defensa-pacifica-contra-la-represion-del-esmad/). El Profesor en Economía sostuvo que en Francia se tiene el mismo debate sobre el uso o no de la violencia y las posiciones al respecto "están bastantes polarizadas".

Por una parte, quienes creen en las acciones no violentas señalan que esta es una forma más eficaz porque hace que las marchas sean multitudinarias, y de otra parte, hay quienes dicen que las marchas no son suficientemente concurridas, y por eso se debe presionar rompiendo cosas específicas, atacando objetos y no personas. (Le puede interesar: ["¿Por qué Ecuador se vuelca a las calles?"](https://archivo.contagioradio.com/ecuador-tras-protestas/))

"Los manifestantes no han atacado personas, salvo en algún enfrentamiento con la Policía que casi siempre pierden los manifestantes. Mi opinión es que ninguna de las dos parece ser eficaz, por ejemplo, si se acude a las luchas armadas, muy pocos casos en la historia demostraron ser victoriosas, entonces, si bien es verdad que la manifestación pacífica no molesta tanto, la acción violenta no es tan eficaz", añadió.

Evaluando la encrucijada, Pablo apuntó que **se deberían usar métodos de lucha en sectores estratégicos para presionar a los empresarios,** y a su vez, al Gobierno, jugando con la correlación de fuerzas que se tienen y hacerla favorable para quienes están contra la reforma pensional. (Le puede interesar: ["¿Levantar el paquetazo, o buscar la salida de Lenín Moreno en Ecuador?"](https://archivo.contagioradio.com/levantar-el-paquetazo-o-buscar-la-salida-de-lenin-moreno-en-ecuador/))

### **¿Lecciones de Francia para América Latina?** 

Para cerrar, el doctor en sociología política, sostuvo que no era Francia la que debía enseñar a Latinoamérica como protestar sino al revés: Europa tiene que aprender de latinoamérica, en términos de protesta y de movimiento obrero. No es que seamos más inteligentes los latinos que los europeos, es que sabemos más de crisis porque las vivimos todas. En ese sentido, tenemos un repertorio de acción colectiva, mientras en Europa tenemos una baja tasa de sindicalización que hace que las huelgas se pierdan.

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe><iframe id="audio_45286651" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_45286651_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
