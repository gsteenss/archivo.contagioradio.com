Title: Falleció el director de "El lado Oscuro del corazón"
Date: 2016-12-26 08:23
Author: AdminContagio
Category: 24 Cuadros
Tags: argentino, Cine, El lado oscuro del corazón, Eliseo Subiela
Slug: eliseo-subiela-cine-argentino
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/12/510707_1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### Foto: Semana 

##### 26 Dic 2016 

El director y guionista argentino **Eliseo Subiela, recordado por su película "El lado Oscuro del corazón", falleció la madrugada de este domingo** a los 71 años de edad. Aunque el cineasta había sufrido un ataque al corazón hace 3 meses, aún no han sido confirmadas las causas puntuales de su deceso.

La noticia de su fallecimiento fue confirmada por el cineasta Gabriel Arbós, directivo de la asociación Directores Argentinos Cinematográficos y amigo de Subiela, con un mensaje a través de su cuenta en la red social Facebook. "**Fue en vida un gran director de cine, nos deja muchas obras cinematográficas por las que ganó tanto el apoyo del público como de la crítica**", escribió el dirigente.

Su trabajo en "**Hombre mirando al sudeste**", le valió el premio como mejor ópera prima del Festival de Cine de San Sebastián y el Premio de la Crítica del Festival de Cine de Toronto en el año 1986, Subiela se encontraba trabajando en la actualidad en un nuevo proyecto cuyo título provisional era Corte Final, que tendría como protagonistas a Miguel Angel Solá y Selva Alemán.

Con "El lado oscuro del corazón" de 1992, película considerada de culto, alcanzó el reconocimiento y admiración de toda una generación, por **su capacidad para combinar la estética visual cinematográfica con la poesía de grandes escritores latinoamericanos**, en un relato de corte surrealista.

<iframe src="https://www.youtube.com/embed/QYgs-wymnwA" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

En la actualidad, Subiela alternaba su trabajo de guionista y director con la docencia en su propia escuela de cine. Durante los últimos años se había dedicado a realizar producciones gracias a la tecnología digital. Sus restos serán velados este lunes durante una hora por disposición explícita de su familia. Le pueden interesar: [El cine necesita de nuevas plataformas para ser visible](https://archivo.contagioradio.com/cine-plataformas-vod-qubit/)
