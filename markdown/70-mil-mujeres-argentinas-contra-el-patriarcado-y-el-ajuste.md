Title: 70 mil mujeres argentinas contra el patriarcado y el ajuste
Date: 2016-10-10 17:41
Category: El mundo, Otra Mirada
Tags: 70 mil mujeres, Argentina, derechos de la mujer, mujeres, Rosario
Slug: 70-mil-mujeres-argentinas-contra-el-patriarcado-y-el-ajuste
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/10/unnamed-2.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Paula Di Carlo 

###### 10 Oct 2016 

[En la ciudad de Rosario, Argentina **más de 70 mil mujeres realizar**on su **Encuentro nacional número 31**, en el que durante 3 días participantes llegadas desde distintos puntos del territorio nacional y latinoamericano, participaron en 69 talleres temáticos para debatir los temas que más les preocupan.]

[El encuentro, que se realiza todos los años en una ciudad diferente, **es un espacio de participación federal, horizontal, igualitario, participativo, autoconvocado, autorganizado y autofinanciado por las propias mujeres.** Como todos los años el domingo, segundo día del encuentro, se realizó una multitudinaria movilización que recorrió las calles de la ciudad sede.]

[Ante el contexto actual que vive el pais, del cual no son ajenas las mujeres, en el encuentro s**e elevaron fuertes críticas al neoliberalismo, al ajuste económico, al aumento de la pobreza provocado por el gobierno de Mauricio Macri** y se reiteró el reclamo contra la violencia machista y por **el derecho al aborto legal, seguro y gratuito**.]

Aldana Martino, integrante del Movimiento Proyecto Popular que estuvo presente en este encuentro, aseguró que hace 31 años que se dan cita muchas mujeres para dialogar en talleres de diversas temáticas diferentes problemáticas “Los problemas de los feminicidios, la violencia obstétrica, el aborto legal seguro y gratuito, los micro machismos son algunos temas de los cuales se habla” aseguró.

[Hacia el final de las movilizaciones se presentaron algunos incidentes. Un pequeño grupo de **provocadores ultracatólicos se enfrentó con un grupo de mujeres frente a la Catedral de Rosario**, lo que devino en la intervención de la policía local, quienes protegieron el templo y reprimieron a las manifestantes dejando algunos heridos, incluyendo periodistas y fotógrafos.]

[Sin embargo, el hecho represivo no empañó lo positivo del encuentro que logró congregar más de 70 mil mujeres, quienes dejaron en claro que es**tán dispuestas a seguir luchando por un país y un mundo con mayor igualdad, sin violencia machista, sin femicidios y con derecho a elegir sobre sus cuerpos**.]

Por último, manifestó que las conclusiones a las que llegaron en este encuentro se conocerán en los próximos días para que estas puedan ser retomadas en diversos espacios del mundo y cada vez más mujeres se sumen a la búsqueda  de igualdad para las mujeres.

[La próxima sede del encuentro,  que tendrá lugar **en octubre de 2017, será la provincia de Chaco**, una de las más pobres y marginadas del país.]

<iframe id="audio_13277633" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_13277633_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Con información de FARCO (Red Argentina de Radios Comunitarias) 
