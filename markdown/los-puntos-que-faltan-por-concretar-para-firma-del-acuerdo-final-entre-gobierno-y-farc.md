Title: Los puntos que faltan por concretar para firma del acuerdo final entre gobierno y FARC
Date: 2016-08-22 15:23
Category: Paz
Tags: acuerdos de paz, FARC, paz
Slug: los-puntos-que-faltan-por-concretar-para-firma-del-acuerdo-final-entre-gobierno-y-farc
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/03/Terrepaz-e1457126046305.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: telesur] 

###### [22 Ago 2016]

[Aunque se espera que en los próximos días se pueda llegar a un acuerdo final, dada la decisión de trabajar de manera constante en las conversaciones de paz entre el gobierno y las FARC, aún quedan por concretar algunos puntos que son cruciales. **Según lo que ha podido establecer Contagio Radio son por lo menos 3 puntos en que aún no se alcanzan consensos**.]

[Al primero de esos temas se refirió Iván Márquez en las últimas semanas y tiene que ver con la amnistía para los integrantes de las FARC y las personas que están detenidas y acusadas del delito de rebelión con hechos que están supuestamente relacionados con esa guerrilla. **La amnistía debe darse el mismo día D** cuando iniciarán las conversaciones y las [personas que sean cobijadas por la medida quedarían a la espera de la entrada en vigencia de la Jurisdicción Especial para la Paz](https://archivo.contagioradio.com/terrepaz-podria-ser-la-solucion-integral-para-el-conflicto-en-colombia/).]

[Otro de los puntos **es la verificación tripartita, tanto del cese bilateral como de la implementación de los acuerdos de paz**. Uno de los puntos críticos tiene que ver con la duración de las misiones de verificación. Mientras que para algunos sectores el tiempo de permanencia de las misiones es de 1 año, para otros sería necesario que la verificación debería permanecer por los menos 5 años más después del llamado “día D”.]

[Por otra parte la discusión también ha girado alrededor del “día D”. Aunque al parecer uno de los acuerdos era que el día “D” sería el día de la firma del acuerdo final, hay ciertas disposiciones que tienen que ver con las **garantías de seguridad para la movilización de las FARC hacia las [Zonas Veredales Transitorias](https://archivo.contagioradio.com/las-visitas-de-verificacion-en-las-zonas-veredales-desde-las-comunidades/) de Normalización** y la manera en la que actuarán los organismos internacionales, es decir, los papeles que van a cumplir en dichas zonas.]

[Además, al parecer, uno de los temas centrales de la discusión tiene que ver con la financiación tanto de lo que se necesita en las zonas de concentración, como de la seguridad para los integrantes de las FARC que se moverán a nivel regional y nacional, así como el presupuesto para poner a funcionar el sistema de **[Jurisdicción Especial de Paz](https://archivo.contagioradio.com/asi-seran-seleccionados-las-y-los-jueces-para-la-jurisdiccion-especial-de-paz/) y la Comisión de la Verdad, entre otras.**]

[Sin embargo, a través de la cuenta en Twitter de la delegación de paz de las FARC se afirmó que se está trabajando de manera constante, sin embargo **parece descartarse que el acuerdo final se firme esta semana**. También se revelaron los nombres de los integrantes de las FARC que harán parte de la misión de verificación a nivel nacional en Bogotá y a nivel regional. A continuación compartimos el listado…]

[Equipo de Monitoreo y verificación de las FARC-EP. A nivel nacional sede Bogotá: Marco Calarcá, Matías Aldecoa, Rubín Morro, Olga Marín, María Soler, Sofía Nariño, Gonzalo Porras, Damaris Acosta, Lucas Urueta, Ezequiel Martínez.]

[A nivel regional: Lucas T (Valledupar), Erika Montero (Medellín), Ezequiel Martínez (Quibdó), Francisco Gonzáles (Popayán), Kunta Kinte (Villavicencio), Víctor Tirado (San José del Guaviare), Hernán Benítez (Florencia), Gloria Martínez (Bucaramanga).]
