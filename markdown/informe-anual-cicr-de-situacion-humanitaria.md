Title: Cuatro retos humanitarios para Colombia en el 2016 según el CICR
Date: 2016-03-11 18:03
Category: DDHH, Nacional
Tags: CICR, Conversaciones de paz de la habana, Paramilitarismo
Slug: informe-anual-cicr-de-situacion-humanitaria
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/03/cicr-informe.2016.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Fotos: CICR 

###### **[10 Mar 2016]** 

La desaparición forzada, las minas y los artefactos sin explotar, la violencia por fuera del conflicto armado y al aguda crisis carcelaria, son los retos humanitarios que Colombia debería afrontar de manera prioritaria en el 2016, según el último informe del Comité Internacional de la Cruz Roja (CICR) "Los retos humanitarios de Colombia en 2016".

### **Desaparición Forzada: Cifras que superan la realidad de cualquier país** 

Según el informe, a la fecha, continúan **79.000 personas desaparecidas por conflicto armado** y otras circunstancias, y se estima que alrededor de **45.000 han sido víctimas directas**. “En Colombia, se documenta la desaparición de una persona cada hora y solo se reporta la aparición de una persona cada tres horas, ya sea viva o muerta”. Ante esta situación, el Comité afirma que "las dimensiones de esta tragedia humanitaria **superan las de cualquier otro país del continente** y las de la mayoría de conflictos armados recientes en el mundo".

### **Crisis carcelaria insostenible** 

La situación en las cárceles de Colombia está en crisis humanitaria con el **aumento de hacinamiento de los prisioneros en el 2016**, y el deterioro de los servicios de salud. El CICR asegura que hay **43.000 reclusos de más en las cárceles**, y que la atención médica únicamente para ser examinados, se convierte en una odisea. "La **falta de una política criminal eficiente** y concertada entre las diferentes instituciones del Estado (...) ha llevado a un **nivel insostenible de hacinamiento**".

### **Artefactos explosivos, minas antipersonal** 

El CICR manifiesta que la descontaminación de artefactos explosivos y restos de guerra  es otro de los retos humanitarios para solventar, especialmente en las zonas rurales. A pesar de la reducción del conflicto armado, prevalecen artilugios como municiones sin estallar o abandonadas, armas trampa, armas cortas y ligeras, y minas antipersonal, que han dejado a **108 víctimas mutiladas**, deserción escolar, aislamiento de las comunidades por temor, acceso limitado a los cultivos, entre otras. **"Después de Afganistán, Colombia es el segundo país con más víctimas de minas antipersonal".**

### **Violencia armada** 

Aunque no se nombra el paramilitarismo, la violencia armada por fuera del conflicto armado es otro detonante de la grave situación humanitaria. Según el CICR, el **mayor número de casos registrados se han presentado en Tumaco, Medellín y Buenaventura**. "Durante 2015, los entornos urbanos fueron el escenario de **124 posibles violaciones del DIH** y otras normas humanitarias, una cuarta parte de todas las registradas por el CICR el año pasado".  El accionar de bandas organizadas, y pandillas por el "control territorial, extorsiones y desplazamientos intraurbanos" ha provocado **amenazas, violencia sexual, asesinatos y el desplazamiento** de las comunidades.

El CICR agrega que los cuatro retos planteados para este año 2016 deben continuar después de los diálogos de paz entre el gobierno y las FARC-EP en Cuba, y que han trabajado en conjunto los familiares de las víctimas, las instituciones del Estado correspondientes, el CIRC, las FARC-EP y el gobierno colombiano para encontrar soluciones definitivas.

[Informe Colombia Retos Humanitarios 2016 Cicr](https://es.scribd.com/doc/303995596/Informe-Colombia-Retos-Humanitarios-2016-Cicr "View Informe Colombia Retos Humanitarios 2016 Cicr on Scribd")

<iframe id="doc_65384" class="scribd_iframe_embed" src="https://www.scribd.com/embeds/303995596/content?start_page=1&amp;view_mode=scroll&amp;show_recommendations=true" width="75%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="undefined"></iframe>
