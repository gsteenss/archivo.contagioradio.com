Title: Avances de diálogos con el ELN deberán ser consignados en la ONU
Date: 2019-05-04 11:41
Author: CtgAdm
Category: Nacional, Paz
Tags: ELN, Mesa de negociación, proceso de paz
Slug: avances-de-dialogos-con-el-eln-deberan-ser-consignados-en-la-onu
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/06/ivan-duque-y-eln.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Archivo 

El tribunal administrativo de Cundinamarca, **ordenó al presidente Iván Duque responder "de fondo" al derecho de petición** presentado por Álvaro Leyva Durán y el Senador Iván Cepeda, el 14 de marzo del presente año, y envíe al Consejo de Seguridad de la ONU y los países garantes los **documentos que contengan los avances y acuerdos a los que se llegaron durante los años de negociación y seis ciclos de diálogos entre Colombia y el Ejército de Liberación Nacional** (ELN).

La decisión del tribunal con fecha del 3 de mayo, ordena que el mandatario responda en el trascurso de 5 días hábiles tras la notificación del fallo de la acción presentada. Esto en razón que **hasta la fecha las respuestas obtenidas,** primero por parte del comisionado de paz Miguel Ceballos (febrero 11) y luego por la secretaría jurídica de la presidencia Clara María Gonzalez (abril 5), **se limitan a exponer que el presidente tiene las facultades** para "Dirigir las relaciones internacionales"; "conservar en todo el territorio el orden público y restablecerlo donde fuere turbado" así como la dirección de todo proceso de paz y que "quienes a nombre del gobierno participen en los diálogos y acuerdos de paz lo harán de conformidad con las instrucciones que él les imparta".

Para los tutelantes, ambas respuestas que aducen fueron redactadas por la misma funcionaria, no responden de fondo a su solicitud, por lo que acudieron directamente al Presidente como cabeza del gobierno nacional para que **ordene a quien corresponda informar sobre la información requerida al Consejo de seguridad de las Naciones Unidas, y a las Repúblicas de Brasil, Chile, Cuba, Ecuador y al Reino de Noruega como países garantes,** y que en su calidad de jefe de gobierno "proceda a su pronta implementación para satisfacer derechos surgidos de dichos compromisos".Tutelar el derecho de petición.

##### Respuesta a petición no es "congruente y precisa" 

El fallo del tribunal en cabeza del magistrado ponente Alberto Espinosa Bolaños de tutelar el amparo solicitado por los demandantes, determina que la respuesta obtenida por parte de la presidencia **"no cumple con los requisitos legales y jurisprudenciales para entender satisfecho el derecho de petición, puesto que la contestación no es acorde con lo peticionado"**, porque enuncia las normas que facultan al Presidente de la República pero "no da explicaciones o motivaciones de fondo del asunto" concluyendo que si existe una vulneración del derecho invocado.

La decisión contempla que de no ser impugnada, **dentro del término de los 3 días siguientes a la notificación deberá ser remitida a la Corte Constitucional para su revisión al día siguiente**.

##### 

<iframe id="doc_55663" class="scribd_iframe_embed" title="Fallo tribunal de Cundinamarca Cepeda y Leyva/ Iván Duque" src="https://es.scribd.com/embeds/408653612/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-W8ZzNgWgkoQAFb2pWW1x&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="0.6498559077809798"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]

 

 
