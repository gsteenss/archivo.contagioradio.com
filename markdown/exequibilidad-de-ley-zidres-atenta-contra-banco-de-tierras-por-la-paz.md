Title: Exequibilidad de Ley ZIDRES atenta contra Banco de Tierras por la Paz
Date: 2017-02-09 12:33
Category: DDHH, Nacional
Tags: Banco de Tierras por la Paz, Ley Zidres
Slug: exequibilidad-de-ley-zidres-atenta-contra-banco-de-tierras-por-la-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/05/regresan-a-las-tierras.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [9 Feb 2017] 

La Corte Constitucional falló en la defensa de la **Ley ZIDRES que atenta contra la conformación del Banco de Tierras para la Paz** y que solo deja como mecanismo de participación, por parte de la población, los **Consejos Municipales**, que serían consultados previamente antes de que se realice la declaratoria de la ley.

El Banco de Tierras para la Paz dispone de la extinción judicial de dominio de predios ilegales, de las tierras provenientes de la delimitación y actualización  de las reservas forestales,  las tierras que puedan adquirirse vía expropiación y por último las **tierras baldías de la nación**, que no implicaban la redistribución de la tierra sino la titulación de la misma y que ahora **podrán ser adquiridas por empresas y multinacionales bajo la Ley ZIDRES.**

Para el ex Representante a la Cámara Wilson Arias “La adjudicación de tierras baldías que era el principal factor para este Banco, en estos momentos están desechas de un plumazo porque **las trasnacionales y las grandes corporaciones tienen sus ojos en ellas**, esto es el grupo Ardila Lule, el grupo Santodomingo, esto es Luis Carlos Sarmiento Angulo, estos son acaparadores internacionales como Poligrow … de modo que muy poco futuro le vemos al Banco de Tierras para la Paz” Le puede interesar: ["Ley ZIDRES para acaparadores de tierra afecta territorios colectivos"](https://archivo.contagioradio.com/ley-zidres-para-acaparadores-de-tierras-afecta-territorio-colectivos/)

De otro lado, la Corte Constitucional también especificó que los c**oncejos Municipales serán una instancia de consulta previa**, antes de que se realice la declaratoria de ZIDRES, sin embargo, Arias considera que estos escenarios dependen demasiado de la **capacidad de incidencia y participación que tengan los campesinos,** pero también de las alianzas que se dan entre empresas y actores del narco paramilitarismo.

“En los espacios del Ingenio Manuelita con 17.000 hectáreas, se encuentra que las tierras pasaron por las manos de Elver Oicá Samorales y Oscar de Jesús López Cadavid, personas que aparecen en los organigramas de la DEA como actores del narco paramilitarismo y que **causaron miles de desplazados y cientos de víctimas, por eso tengo dudas de que en esos concejos municipales se logre revertir esta ley”. **

Dentro de los próximos 15 o 20 días se dará un **nuevo falló frente a otra demanda sobre la ley ZIDRES** y sus afectaciones al campesinado colombiano. Pese a que el optimismo de Arias no es grande afirmó que es necesario que la población se organice y sobretodo el campesinado del país: “**el sujeto político que debe batallar es el campesino directamente, pero todos los colombianos debemos apoyar** porque nos afecta, nos alimentamos de la tierra campesina”. Le puede interesar:["Radican demanda contra Ley ZIDRES"](https://archivo.contagioradio.com/polo-partido-verde-y-cumbre-agraria-radican-demanda-contra-zidres/)

<iframe id="audio_16922684" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_16922684_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
