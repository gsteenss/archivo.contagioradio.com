Title: 20 años de lucha para lograr titulación en el sur del Cesar
Date: 2016-05-13 16:03
Category: DDHH, Nacional
Tags: ASOCADAR, hacienda bellacruz, Restitución de tierras
Slug: 20-anos-de-lucha-para-lograr-titulacion-en-el-sur-del-cesar
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/05/regresan-a-las-tierras.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [13 Mayo 2016 ]

Después de 20 años de lucha y resistencia, 120 familias del sur del Cesar, organizadas en la Asociación Campesina de Desplazados al Retorno ASOCADAR, lograron que la **Corte Constitucional ordenara la titulación de 2 mil hectáreas**, correspondientes a la Hacienda Bellacruz, en la que actualmente funciona la firma palmera M.R Inversiones y Cía. Ltda, de propiedad de Germán Efromovich, gerente de Avianca.

"Por fin se ha hecho justicia", dice Adelfo Rodríguez, representante legal de ASOCADAR, e insiste en que esperan que las autoridades departamentales, municipales y nacionales brinden las garantías necesarias para su permanencia en el territorio, teniendo en cuenta que han sido **víctimas de hostigamiento por parte de la Policía y el ESMAD, quienes los desalojaron violentamente** cuando decidieron regresar por sus propios medios.

El líder asevera que **las autoridades "se han arrodillado" ante el gran poder económico y político**, poniéndose a disposición de la empresa de Efromovich, en la que la familia del excanciller Carlos Arturo Marulanda, también tiene acciones. "Ellos son los ocupantes que originaron [[toda la violencia en la región](https://archivo.contagioradio.com/?s=hacienda+bellacruz+)] desde 1936, matando campesinos hasta lograr obtener siete mil hectáreas de tierra", agrega.

En las próximas semanas las familias se reunirán para acordar la ruta para la titulación, entretanto extienden su llamado a las autoridades para que brinden el acompañamiento y garanticen que se fortalezcan los frentes de producción **para su permanencia digna en el territorio**.

<iframe src="http://co.ivoox.com/es/player_ej_11523533_2_1.html?data=kpailJiZd5Shhpywj5WcaZS1k5aah5yncZOhhpywj5WRaZi3jpWah5yncaLYxtHT0ZC2s8Xmhqigh6aoq9bZ25Cajaa3k6S1paa%2Fj4qbh4630NPhw8zNs4zGwsnW0ZCRaZi3jpk%3D&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)]] 
