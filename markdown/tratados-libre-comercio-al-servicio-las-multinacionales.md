Title: Tratados de libre comercio al servicio de las multinacionales
Date: 2016-11-22 14:14
Category: Economía, El mundo
Tags: Eurodiputados, TLC, TPP, Tratados de Libre Comercio, TTIP
Slug: tratados-libre-comercio-al-servicio-las-multinacionales
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/ceta-y-ttip-e1479841153118.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: [L'Expansion]

###### [22 Nov 2016] 

El Tribunal de Justicia Europeo revisará este miércoles el texto del **[Acuerdo Económico y Comercial Global, CETA](https://archivo.contagioradio.com/los-secretos-del-tlc-de-la-union-europea-con-espana/),** (gestionado entre Unión Europea y Canadá) de acuerdo con la moción de 89 eurodiputados que solicitaron dicho examen asegurando que este convenio pretende implementar nuevos mecanismos de blindaje para las multinacionales, afectando los derechos de las comunidades y generando daños ambientales irreparables.

Tras ese examen, el 13 de diciembre será cuando la Eurocámara vote de manera definitiva sobre el CETA, con el que se estaría protegiendo al sector privado, como lo ha denunciado la **campaña \#NoalTTIP.** Según esta iniciativa el acuerdo permitirá a **las empresas interponer millonarias demandas a los Estados ante tribunales de arbitraje** cuando consideren que sus inversiones se han visto perjudicadas por cambios normativos ejecutados por esas naciones.

Y es que este no es el primer tratado de libre comercio que termina beneficiando a las multinacionales y que además les brinda herramientas para demandar a los Estados. Según la campaña contra el TTIP, Estados Unidos y la Unión Europea han propuesto incluir en [El Tratado Transatlántico de Comercio e Inversiones, TTIP](https://archivo.contagioradio.com/partidos-de-izquierda-espanoles-y-europeos-se-alian-en-contra-del-ttip/), un capítulo sobre la protección de las inversiones, que incluso es muy similar a **los más de 1200 acuerdos de protección de las inversiones que ya han firmado los países miembro de la UE.**

Se trata del **Arbitraje de Diferencias Estado-Inversor,** ISDS. Un mecanismo por el cual los inversores extranjeros pueden eludir la justicia de cada nación y además pueden demandar al país, sin que ellos puedan ser denunciados.

### Países demandados por multinacionales 

En ese sentido se puede resaltar casos como el de Canadá, a quien en 2013 la empresa Lone Pine Resources, demandó amparándose en el Tratado de Libre Comercio de América del Norte, NAFTA, reclamado 250 millones de dólares luego de que el estado de Quebec declarara la moratoria para el fracking.

La empresa productora de cigarrillos, Philip Morris denunció a Uruguay y a Australia por sus leyes antitabaco. En el caso de Uruguay se basó en el tratado bilateral de inversión entre Suiza y Uruguay, y en el caso de Australia, lo hizo alegando que se interfería con el Tratado sobre Inversiones Bilaterales de Australia con Hong Kong.

Por otra parte en 2009, la empresa petrolera [Chevron Corporation, inició una demanda contra Ecuador](https://archivo.contagioradio.com/tribunal-de-ecuador-debe-embargar-bienes-de-chevron-para-reparar-los-danos-en-la-amazonia/) bajo el TBI entre Estados Unidos y Ecuador. Con esta logró que ese país le  pagara 112 millones de dólares, y además pudo eludir el pago de una indemnización multimillonaria ordenada a la empresa por la justicia ordinaria, debido a los daños de la selva amazónica causados por la contaminación petrolera.

Respecto a Colombia, luego de que la Corte Constitucional prohibiera realizar actividades mineras en zonas de páramo, la empresa canadiense Eco Oro Minerals Corp anunció que demandará al Estado Colombiano ante instancias internacionales buscando explotar la mina Angostura en el páramo de Santurbán. Esa compañía anunció un posible arbitraje internacional por las medidas adoptadas por el país para proteger los páramos, argumentando que es posible demandar al Estado de acuerdo con el capítulo de inversión del[Tratado de Libre Comercio entre Canadá y Colombia.](https://archivo.contagioradio.com/que-le-ha-dejado-a-colombia-5-anos-de-tlc-con-canada/)

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
