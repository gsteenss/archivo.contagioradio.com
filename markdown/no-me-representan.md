Title: No me representan
Date: 2015-10-16 12:44
Category: invitado, Opinion, superandianda
Tags: Claudia López, elecciones colombia, encuestas mienten, María Fernanda Cabal
Slug: no-me-representan
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/10/elecciones.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

#### **[[Superadianda](https://archivo.contagioradio.com/superandianda/) - [~~@~~Superandianda](https://twitter.com/Superandianda)** 

###### [16 Oct 2015] 

[Esta democracia no me representa.]

[No me siento representada en un estado donde expresarse diferente es pecado, progresar es ser indiferente al otro y participar sin roscas es amenaza.]

[Ni sus gobernantes,  ni sus partidos y mucho menos sus candidatos me representan. Guarden sus encuestas, sus debates, sus promesas, porque me niego rotundamente a ser representada por una mayoría que es la minoría que opina por corrupción o por ignorancia. Su sentido de patria no es el mío, su amor a la tierra no es el mismo que yo siento y quien habla por mí no es a quien le daría mi palabra.]

[Me representa el campesino, el obrero, el estudiante, el hombre y la mujer que rechaza este sistema de mafias, los que han callado, estigmatizado, discriminado e ignorado, los que no pertenecen a dinastías ni partidos políticos, ni a familias con canales de televisión privados, los que nadie les pregunto su opinión para una encuesta.]

[Solo existe una corriente política en Colombia que no reconoce derecha, izquierda o centro: la ignorancia. Su sentir es sin ideales, sin convicciones, sin criterio, sin vergüenza por la miseria, su opinión es fabricada por Ipsos-Napoleón Franco y Cía S.A,  Datexco Company o cualquiera de las otras 39 firmas que tienen la opinión estúpida de un país estúpido en sus manos.]

[Quiero dejar muy claro que no me representa como colombiana los tamales de las elecciones, los millonarios contratos, los que dicen ser activistas por repartir volantes en los semáforos, los candidatos que se cambian de bando, acomodando sus intereses según sea la máquina de turno del partido. No me digan que en Colombia existen partidos con ideología; porque no me representa el partido de izquierda de color amarillo -el mio es rojo sin temor y sin vergüenza- tampoco el que se dice liberal con políticas de derecha, y menos  el de color verde de centro jalado para el neoliberalismo "alternativo". No estoy de acuerdo con todos los que critiquen a Uribe, porque existe una critica que juzga pero otra que se alimenta para estar calentando puesto en el congreso indignado.]

[Que varias mujeres estén en el congreso no me representan como mujer,  como ciudadana. Que tengamos el mismo sexo y hablemos contra Uribe no quiere decir que tenga que apoyar  incoherentes políticas que van de aquí para allá con el que mejor les convenga, es cierto que nunca me representarian uribistas enfermas como Paloma Valencia y Maria Fernanda Cabal, pero tampoco lo haría el centro mentiroso de Claudia López.  ]

[Yo prefiero miles de críticos, no criticones, que no sumen votos sino ideas, que sus opiniones  no sean  las "ganadoras"  en las encuestas, ni en los noticieros, que entiendan que la política no es un partido de fútbol. En una democracia verdadera no hay ganadores ni perdedores porque todos somos constructores,  su opinión o mi opinión cuenta y si no estamos de acuerdo se habla sin temor, con respeto y firmeza.]

[Pero "nuestros sueños no caben en sus urnas"]
