Title: Comité del Paro Sur-Tunjuelo denuncia estigmatización a líderes
Date: 2017-09-26 15:38
Category: Movilización, Nacional
Tags: Basurero doña juana, Movilización social
Slug: comite-del-paro-sur-tujuelo-denuncia-estigmatizacion-a-lideres
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/08/relleno-doña-juana1.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Comunidad] 

###### [26 Sept 2017] 

Voceros del paro cívico que se están convocando para mañana en el Sur de Bogotá, denunciaron que jefe de seguridad del consorcio CGR que está a cargo del Relleno Sanitario Doña Juana, habría señalado ante medios de información que las autoridades **tendrían 15 órdenes de captura para detener a líderes de esta movilización.**

Oscar Barón, uno de los voceros, señaló que “esta mañana en el canal Canal Uno, el dispositivo de seguridad que está dentro del Basurero Doña Juana afirma que se tienen 15 órdenes de capturas contra los líderes del paro, **supuestamente sobre una mentira, de que tienen que ver con la quema de unos carros, hace un mes**”. (Le puede interesar:["Distrito ampliaría hasta 2070 la existencia del Relleno Doña Juana"](https://archivo.contagioradio.com/distrito-ampliaria-hasta-2070-la-existencia-del-relleno-sanitario-dona-juana/))

### **Las acusaciones** 

Según lo que se logró establecer a partir de las declaraciones de los integrantes de la empresa, las acusaciones tendrían que ver con la supuesta quema de unos carros en las protestas del mes anterior, **hechas por Darwin Poloche, jefe de seguridad del Consorcio CGR.** Barón que  asegura que la comunidad nada tiene que ver en la acusación y que la quema se produjo porque la empresa no ha realizado el mantenimiento de las tuberías de gas.

Además, agregó que esto es muestra de un Estado que es incapaz de debatir en público y que prefiere seguir reprimiendo a la gente. De igual forma la comunidad ha denunciado la alta presencia de la Fuerza Pública que hay en el territorio. “Han generado mucho temor en la población, **porque el territorio está militarizado por la Policía Militar, la SIJIN, el GOES y el ESMAD**” afirmó Barón.

### **Paro Sur-Tunjuelo por la dignidad** 

El paro tiene como fin, exigirle a la Alcaldía Distrital medidas urgentes para finiquitar diferentes problemáticas que aquejan a más de 130 barrios del sur de la capital, y que se vienen presentando desde hace más de **30 años como consecuencia del Relleno Sanitario Doña Juana**. Se espera que en el paro participen ciudadanos y campesinos del área rural de Bogotá. (Le puede interesar: ["ESMAD también arremetió contra manifestantes en el Relleno Doña Juana"](https://archivo.contagioradio.com/esmad-continua-arremetida-contra-manifestantes-en-el-relleno-dona-juana/))

Los habitantes han manifestado que con esta movilización pretenden que la isntitucionalidad distrital, la gobernación de Cundinamarca, la CAR y las empresas privadas lleguen a las asambleas comunitarias y allí se establezcan las condiciones, los métodos, los tiempos y los ritmos para abordar cada uno de los conflictos que hay en los territorios, **cómo el tema del basurero, de la licitación pública de aseo y de la salud**.

Los puntos de concentración serán el Portal Usme desde las 7:00 am, el Portal Tunal desde las 8:00 am, el Portal de las Américas desde las 6:00 am, Protabaco desde las 7:30 am y se espera que en las diferentes universidades públicas de la capital los estudiantes se reúnan para acompañar la movilización, todas las marchas se encontraran en la Autopista Sur. **La segunda movilización recorrerá diferentes veredas de Bogotá en su ruralidad de Ciudad Bolívar**.

[Denuncia Publica Paro 26 de Sep](https://www.scribd.com/document/360078158/Denuncia-Publica-Paro-26-de-Sep#from_embed "View Denuncia Publica Paro 26 de Sep on Scribd") by [Contagioradio](https://es.scribd.com/user/276652802/Contagioradio#from_embed "View Contagioradio's profile on Scribd") on Scribd

<iframe id="doc_32883" class="scribd_iframe_embed" title="Denuncia Publica Paro 26 de Sep" src="https://www.scribd.com/embeds/360078158/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-rDL0hcybFilyxHSkFIJT&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="0.7068965517241379"></iframe>  
<iframe id="audio_21108457" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_21108457_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
