Title: Tras la verdad, hay un sector que no quiere perder el poder del control ideológico
Date: 2020-10-08 21:55
Author: AdminContagio
Category: Actualidad, Paz
Tags: Álvaro Gómez Hurtado, Comisión de la Verdad, Ejército Nacional, FARC, jesús abad colorado, Sistema Integral de Justicia
Slug: sector-teme-verdad-no-quiere-perder-poder-control-ideologico
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/10/Verdad.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: Comisión de la verdad/ premioggm

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

En medio de una conversación entre **el comisionado de la Verdad, Saúl Franco, el fotoperiodista Jesús Abad Colorado y el politólogo Álvaro Villarraga** a la luz de las revelaciones hechas por el Partido FARC sobre el asesinato de Álvaro Gómez Hurtado, la creciente pérdida de la esperanza de las víctimas que exigen a la JEP resultados y el difícil acceso que se ha tenido a la información por parte de la [Comisión de la Verdad,](https://comisiondelaverdad.co/) los tres panelistas coinciden en que el debate no se centra en una pugna por la verdad, sino por el control ideológico de la historia del país y el temor de la élites e instituciones a que les sea arrebatado dicho poder.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### "Para un país que ha vivido en la oscuridad, la verdad es una luz que duele"

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Al respecto, el comisionado de la Verdad, Saúl Franco aseguró que es necesario hablar del 'Poder' como eje del conflicto que ha llevado a una disputa por los poderes locales, regionales y nacionales y que hoy parte del miedo a que se conozca la verdad por parte de algunos sectores, **corresponde al temor que existe de perder el control ideológico, político y económico del país.**

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Por su parte Álvaro Villarraga, expresa que ante el alto nivel de insatisfacción que han demostrado las víctimas con relación a los procedimientos de la JEP, es necesario que se tomen las medidas "para que la condicionalidad de los beneficios transicionales dé resultados, por lo que cree que el único escenario posible es que nuevas verdades tengan que salir a la luz desde el sector de la Fuerza Pública, llamando en especial la atención sobre el hecho de que existen **más de 203 informes recibidos por la Comisión de la Verdad,** incluidos al menos 4.000 folios de instituciones como el Ejército pero hasta ahora ninguno, habla de reconocimientos propios.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

El comisionado Saul resalta que la verdad es un remedio, que si bien cura, "por sí sola es impotente", resaltando que esta es tan solo es el camino para alcanzar el perdón o la justicia. Agrega que a lo largo del trabajo hecho por la Comisión, han descubierto que muchas veces, **esta no favorece ni resuelve la situación de quienes buscan respuestas e incluso puede producir dolor.**

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Esto, a propósito de las verdades establecidas en la memoria histórica de Colombia como el propio asesinato de Gómez Hurtado atribuido al Estado y que junto a otras verdades suma más de 20 o 25 años y resultan inamovibles para sectores de la sociedad, por lo que, para que las nuevas verdades comprobadas que surjan de este Acuerdo sean suplantadas, **"se requerirá un procesamiento interior y enfrentar estas resistencias"**, reiterando que se trata de un campo de lucha" donde a menudo se cuestionan las verdades que favorecen o perjudican a ciertos sectores.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Para el fotógrafo, una sociedad que ha vivido en la oscuridad en la que muchos actores armados y políticos han actuado bajo la impunidad, una nueva verdad se convierte en una luz que duele a un país "que está acostumbrado a señalar a unos \[...\] y en el que hay muchos otros actores armados que tendrían que estar diciendo nosotros también fuimos". Aunque hay verdades que van a sorprender, el fotógrafo recordó que existen otras verdades que para las y los campesinos están claras, "la gente en el campo sabe quién actuó y con complicidad de quién".

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Lo que se puede concluir del paso que dio FARC

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Para Álvaro Villarraga, con la firma del Acuerdo de Paz, el Partido FARC dio pasos importantes tanto al inicio como al final de la negociación, avanzando en los reconocimientos de responsabilidad en casos como el CLub el Nogal, las masacres de Bojayá la Chinita y el asesinato de los diputados del Valle, sin embargo señala que ante el detrimento del Acuerdo con la llegada del gobierno de Iván Duque esa iniciativa decayó, haciendo referencia en particular a las respuestas ambiguas que se han dado sobre el reclutamiento de menores de edad.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Sin embargo habla de un tercer momento que ha vivido el nuevo partido político y que se dio a raíz de su reconocimiento de responsabilidad ante el homicidio de Álvaro Gómez Hurtado, **describiéndolo como una decisión política acertada y que no es gratuita, sino que es una respuesta a todas las nterpelaciones y debates que ya se estaban originando ante la resistencia que presentaban al hablar de otros temas de difícil abordaje** y circunstancias que se han cumulado que llevaron al partido FARC a tomar esa decisión. [(Lea también: FARC- EP reconoce ante la JEP la verdad sobre 6 homicidios, entre ellos el de Álvaro Gómez Hurtado)](https://archivo.contagioradio.com/farc-ep-reconoce-ante-la-jep-la-verdad-sobre-6-homicidios-entre-ellos-el-de-alvaro-gomez-hurtado/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Con no solo la mención de su responsabilidad en el asesinato de Gómez Hurtado sino el reconocimiento de otros actos como el asesinato del representante a la Cámara, Pablo Emilio Guarín, Hernando Pizarro León Gómez, el general (r) Fernando Landazabal Reyes, el exguerrillero José Pedro Rey y el economista y pensador, Jesús Antonio Bejarano; el analista advierte que se trata de una acción que el exsecretariado de las FARC retoma la iniciativa que habían evidenciado al inicio del proceso.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Para el analista, este hecho se va a convertir en una "bola de nieve" que se va a haber reflejado no solo en otros hechos de reconocimientos, como el realizado por Pastor Alape ante la Asamblea de Antioquia por el asesinato del exgobernador de Antioquia Guillermo Gaviria y su asesor Gilberto Echeverri, sino en otros que dinamicen el ejercicio de la justicia transicional.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Una verdad que construya país y no siembre más divisiones

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Al respecto Colorado expresó su preocupación frente a la polarización que vive el país en un momento que "desde el odio o el odio de clase" busca que solo se conozca la verdad de un sector. Y aunque, si bien es necesario reconocer la responsabilidad de élites políticas que "propiciaron la guerra porque no tuvieron la capacidad de entender que el ejercicio de poder busca lograr el bienestar de una sociedad" y no de unas cuantas familias; también es necesario reconocer como sociedad "que también tenemos una culpa frente a eso que sucedió y que no quisimos ver o escuchar y que es necesario afrontar".

<!-- /wp:paragraph -->

<!-- wp:quote -->

> Siento que a veces se acusa desde un lado o desde el otro pero si no nos ponemos de acuerdo vamos a seguir fomentando esos odios, venganzas e iras que no nos permiten mirar al otro como un sujeto que tiene responsabilidades, lo que hay que aprender es a ver con otros ojos menos ideologizados y más desde los afectos y la solidaridad
>
> <cite>Jesús Abad Colorado - fotoperiodista</cite>

<!-- /wp:quote -->

<!-- wp:paragraph {"align":"justify"} -->

De igual forma resaltó que en un país en el que medios de comunicación y líderes políticos buscan manipular la verdad para lograr réditos, al periodista le corresponde aprender a tener **"un ojo clínico para tomar el pulso a una sociedad"**, por lo que le corresponde a los medios, no informar desde "un proyecto o militancia política" sino acercarse a esa verdad que el país necesita".

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Por su parte, ante las múltiples versiones que existen de un mismo hecho, el comisionado afirma que el verdadero desafío es partir de dichas versiones y encontrar la forma de **"sedimentar aquello que sustenta toda la realidad que es lo que llamamos verdades",** explicando que en todas las visiones las del testigo, las del victimario o de la víctima; hay elementos de la realidad, pero al mismos tiempo casi ninguno tiene la totalidad del hecho, por lo que es urgente confrontarlas, verificarlas y lo más importante desprenderse de una única interpretación de un hecho".

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Reitera que **desde la Comisión de la Verdad, pese a las dificultades que han surgido del difícil acceso a la información ante la pandemia que ha imposibilitado los encuentros presenciales y el no suministro de datos por parte de algunas instituciones, su compromiso es realizar una escucha universal** con la mayor imparcialidad posible con el objetivo de "ofrecer a la sociedad algunos pilares para la construcción de una verdad histórica". [(Le puede interesar: Información de inteligencia y contrainteligencia no ha sido entregada a la Comisión de la Verdad)](https://archivo.contagioradio.com/comision-verdad-denuncia-obstaculos-para-acceder-a-informacion-reservada/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

El comisionado concluye de forma positiva, que con las recientes revelaciones conocidas por parte del Partido FARC, "está llegando la hora de la verdad concreta" al país, por lo que es una oportunidad no para seguir fragmentando sino para invitar al conjunto de la sociedad a ser capaces como sociedad de hacer caer los velos que ocultan la verdadera historia del conflicto armado en Colombia. [(Lea también: Rodrigo Londoño se comprometió con el «Movimiento Nacional por la Verdad»)](https://archivo.contagioradio.com/rodrigo-londono-se-comprometio-con-el-movimiento-nacional-por-la-verdad/)

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
