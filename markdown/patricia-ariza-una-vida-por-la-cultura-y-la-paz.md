Title: Patricia Ariza, una vida por la cultura y la paz
Date: 2015-07-31 11:37
Category: Cultura, Hablemos alguito
Tags: Bogotá, Camilo, Corporación Colombiana de Teatro, Festival Mujeres en escena por la paz, Hablemos Alguito, Patricia Ariza, teatro, teatro en bogota, Tramaluna
Slug: patricia-ariza-una-vida-por-la-cultura-y-la-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/07/patricia-ariza-contagio-radio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Gabriel G - Contagio Radio] 

###### [Jul 31 2015]

\[audio mp3="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/07/Patricia-Ariza.mp3"\]\[/audio\]

Patricia Ariza, directora de la Corporación Colombiana de Teatro y  defensora de Derechos Humanos, pasó por los micrófonos de 'Hablemos Alguito'. Dialogamos sobre su infancia, su juventud en el movimiento nadaista, y su recorrido en las artes. Entre otros temas nos relata el nacimiento del teatro La Candelaria, su militancia comunista y en el movimiento de artistas que existía en ese momento.

Para Patricia, el teatro "nace de la necesidad de expresar un malestar que había en la cultura, una rebeldía", que hoy luego de sus largos años de trabajo todavía siguen vigentes.

Manifiesta la necesidad de apostarle a la paz desde todos los sectores y en especial desde los medios de comunicación, y propone un proceso de paz trilateral que incluya a las partes que actualmente dialogan, y a la sociedad en general.

Para finalizar este interesante diálogo sobre cultura de paz, hablamos del papel que tiene el teatro y todo tipo de arte en un camino hacia la paz en Colombia y cómo las mujeres han venido aportando en él. Ejemplo de ello es el [Festival Internacional de Mujeres en Escena por la Paz](https://archivo.contagioradio.com/teatro-por-la-reivindicacion-de-la-mujer-y-su-papel-de-cara-a-la-paz/) que iniciará el próximo 1 de agosto en la ciudad de Bogotá, y al cual la directora nos invita.
