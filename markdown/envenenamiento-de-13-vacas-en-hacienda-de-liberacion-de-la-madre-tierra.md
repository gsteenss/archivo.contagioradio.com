Title: Envenenamiento de 13 vacas en hacienda de liberación de la Madre Tierra
Date: 2020-04-28 19:56
Author: CtgAdm
Category: Nacional
Tags: Cauca
Slug: envenenamiento-de-13-vacas-en-hacienda-de-liberacion-de-la-madre-tierra
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/04/Vacas.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: Liberación de la madre tierra/ CRIC

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

El pasado 25 de abril en medio del ejercicio de [Liberación de la Madre Tierra](https://liberaciondelamadretierra.org/), realizado por la comunidad indígena Nasa en la hacienda Canaima, en La Emperatriz al norte del Cauca, además de ser hostigados por la Fuerza Pública, 13 vacas liberadoras fueron masacradas por personas que harían parte del ingenio azucarero Incauca, según relatos de los comuneros.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Denuncian que trabajadores al servicio de Incauca, ingenio productor de azúcar y agrocombustibles, **habrían entrado en la hacienda Canaima, donde se habrían regado bolas de veneno en un gran lote de potrero. En la mañana, de las 19 vacas que consumieron el heno, 13 murieron y 6 lograron ser salvadas.** [(Le puede interesar: Marcha de la Comida, una apuesta del pueblo Nasa por la defensa del territorio)](https://archivo.contagioradio.com/marcha-de-la-comida-una-apuesta-del-pueblo-nasa-por-la-defensa-del-territorio/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Uno de los comuneros que hacía parte de la conmemoración señala que a través del Instituto Colombiano Agropecuario (ICA) se han recolectado algunas pruebas que han dicho qué tipo de químico se usó para atentar contra las reses. [(Lea también: Continúan agresiones del ESMAD contra resguardos indígenas en el Cauca)](https://archivo.contagioradio.com/continuan-agresiones-del-esmad-contra-resguardos-indigenas-en-el-cauca/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

"Esta vez la emprendieron contra los animales que no tienen nada que ver en este ejercicio por el mismo hecho de ser animales, pero es el afán de desmotivar la fuerza que tiene el proceso y de causarle daño a la comunidad".

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Relata que mientras se revisaban los cuerpos de las vacas que fueron envenenadas fueron hostigados con disparos hechos por la Policía presentes en la Hacienda La Emperatriz, agrega que se redujo a los integrantes de la Fuerza Pública y se les pidió respetar el ejercicio de liberación. Según el comunero son más de 350 intentos de desalojo los que se han hecho contra los liberadores por parte de diferentes integrantes de la Fuerza Pública. [(Le puede interesar: Desalojos del ESMAD dejan cuatro indígenas heridos en el Norte del Cauca)](https://archivo.contagioradio.com/desalojos-del-esmad-dejan-cuatro-indigenas-heridos-en-el-norte-del-cauca/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Los comuneros ya habían advertido desde mediados de marzo que el Ejército aumentó el pie de fuerza en las haciendas en proceso de liberación en Corinto, Quebrada Seca y Miraflores. La haciendas Vista Hermosa y La Emperatriz, en Caloto, permanecen militarizadas. [(Le puede interesar: Liberadores de la madre tierra denuncian ataque del ejército en Cauca)](https://archivo.contagioradio.com/liberadores-corinto-ejercito/)

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Liberadores llevan comida a las comunidades en medio de la pandemia

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

El ejercicio de liberación de la madre tierra incluye una marcha de la comida que reparte recursos en las poblaciones, en este caso de Aguablanca, Valle del Cauca; una acción que en medio de la pandemia del Covid-19 resulta más valiosa que nunca.

<!-- /wp:paragraph -->

<!-- wp:quote -->

> "Pensamos en un acto de hermandad, solidario que transmita la sabiduria del pueblo Nasa"

<!-- /wp:quote -->

<!-- wp:paragraph {"align":"justify"} -->

**"Vemos que quienes hoy pueden estar tranquilos pasando la cuarentena son los que tiene poder, pero la clase trabajadora hoy está aguantando hambre"** afirma el comunero, al agregar que no podían permitir que la producción del pancoger se detuviera incluso en épocas de aislamiento.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Respecto a los sucesos cercanos en los que un mayordomo de una de las haciendas fue asesinado, hecho del que algunos medios acusan a la comunidad indígena, los liberados y comuneros han sido claros en que no están vinculados de ninguna forma con el suceso.

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
