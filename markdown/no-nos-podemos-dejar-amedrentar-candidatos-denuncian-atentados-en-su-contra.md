Title: ¡No nos podemos dejar amedrentar! Candidatos denuncian atentados en su contra
Date: 2019-10-22 13:40
Author: CtgAdm
Category: Líderes sociales, Nacional
Tags: Agresiones contra defensores de DDHH, Atentados contra líderes sociales, candidatos a la alcaldía
Slug: no-nos-podemos-dejar-amedrentar-candidatos-denuncian-atentados-en-su-contra
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/Diseño-sin-título.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio  Radio] 

Este 21 de octubre se registraron dos hechos de violencia dirigidos a candidatos a cargos públicos: uno en contra del aspirante al Consejo del municipio La Vega, Cauca  y actual concejal, **Wilmer Jiménez **y otro contra el candidato a la alcaldía del municipio de Convención en Norte de Santander, **Alexander Botello**.

**Los atentados en contra de los candidatos**

Personas aún sin identificar, que al parecer tenían prendas alusivas a las disidencias del Frente 33 de las FARC, quemaron el vehículo de Alexander Botello, candidato por el Partido Verde a la alcaldía de Convención, en la vía que comunica este municipio con Balcones.

El hecho ocurrió mientras el candidato se desplazaba hacia el corregimiento de Miraflores del mismo municipio, luego de retenerlo en el sector conocido como Casa Blanca, dejándolo marcharse después de algunos minutos.

En referencia a Wilmer Jiménez, el candidato estaba reunido con algunos líderes del municipio La Vega, cuando llegaron hombres desconocidos armados con fusiles y comenzaron a disparar en el medio de la reunión. Posteriormente, el Batallón de Alta Montaña del Ejercito Nacional, hizo presencia en el lugar para hacer el respectivo acompañamiento a Jimenez y trasladarlo a un punto seguro.

### **Las campañas electorales siguen siendo inseguras** 

Jiménez declaró que, a pesar de que la situación de orden público se encuentra complicada en La Vega y en los municipios cercanos, nunca había recibido amenazas. Al mismo tiempo el concejal admitió que “**defender el tema del territorio, de la minería y de los DD.HH. hace que los lideres se encuentren bajo los lentes**” de grupos armados que están operando en la zona.

Ambos candidatos hicieron un llamado a las autoridades para que sigan trabajando por la seguridad de concejales, candidatos y políticos defensores de DD.HH. que están trabajando por mantener su territorio. (Le puede interesar: [Paramilitares amenazan candidatos a la Alcaldía en Ovejas, Sucre](https://archivo.contagioradio.com/paramilitares-amenazan-candidatos-a-la-alcaldia-en-sucre/))

**«No nos podemos dejar amedrentar, es necesario que se haga publico lo que nos pasa, que verdaderamente nos miren a nosotros, los lideres, y que nos protejan, porque estas son situaciones que ya se pasan a mayores»** denunció Jiménez.

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe><iframe id="audio_43513177" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_43513177_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe><iframe id="audio_43513177" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_43513177_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
