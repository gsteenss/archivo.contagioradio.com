Title: La ausencia
Date: 2019-07-14 15:58
Author: Deborah
Category: Opinion
Tags: Desaparición forzada, Palacio de Justicia
Slug: la-ausencia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/ausencia.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

Me raspé la rodilla y me sangró profusamente sin detenerse por algo más de 5 minutos, experimenté la angustia del dolor físico esa tarde en el colegio, nos habíamos caído con mi amiga María Andrea, juntas terminamos aporreadas en el piso de la cancha de basketball  del colegio, teníamos 4 años, lo recuerdo bien por q fue mi primera caída fuerte; pero algo que me marcó significativamente ese día, mi abuela y la mamá de mi amiga llegaron a las pocas horas y lavadas en llanto corrimos a abrazarlas, pero yo, sentí otro dolor, el de la ausencia de mi mamá Norma, vi como la madre de mi amiga la abrazaba con un matiz diferente al que mi abuela lo hacía conmigo y entendí que mi abuela no era mi madre, que ella estaba muerta, en el cielo como me decían, pero con pocos detalles.

Fue la primera vez que sentí su ausencia por que de allí en adelante jamás he dejado de sentir ese vacío incontrolable que me recuerda que nunca estuvo allí y que fue arrancada del mundo por las garras de la injusticia desatada por la guerra.

Pero hablar de desaparición forzada desde el ámbito y la experiencia propia es una catarsis constante que me ha permitido ver todas las facetas habidas y por haber del dolor; he aprendido y desaprendido sobre mis propios traumas y gracias a ese sinsabor de no saber que sentir o cómo actuar para controlar la ausencia, he forjado un destino basado en los sueños empíricos de mi mamá, esos que, paradójicamente no le permitieron estar a mi lado y compartir una “vida”

Cuando alguien se va de esa forma de nuestras vidas, aludiendo a esa forma desastrosa de ni siquiera encontrar un cuerpo, ni saber cómo murió o no saber ni siquiera que pensó en sus últimos minutos antes de que otros, con la facultad que no les compete, le quitaran su último aliento y desaparecer su rastro. Cuando eso pasa puedo compararlo con el hambre, todos los días llegaba esa sensación insaciable de hambre de verdad, que debía saciar con múltiples pensamientos cargados de incertidumbre, ¿por qué no había respuestas, no había verdad?

Con el tiempo vi que la ansiedad y los malos pensamientos, al igual que los sentimientos póstumos a la desaparición como el odio, el resentimiento, la ira, el rencor, la depresión, la tristeza desmedida, la frustración, el aislamiento propio y el más destructivo de todos, la venganza, eran tan sumamente poderosos que opacaban los sueños y metas que mi madre tenía para mí. Eran tan corrosivos e invasivos que dominaban mi mente y llenaban mi corazón de oscuros panoramas que me dejaban a ciegas para perseguir la luz que ella habría anhelado para mi vida.

Cuando entendí esto, pude liberarme de esta energía negativa y concentrarme en cumplir los sueños que mi madre habría querido para mí, me dediqué a levantarme todos los días no pensando en la tragedia o en buscar venganza, sino pensando en ser feliz como ella quería que fuera.

Esta experiencia de vida se traslada irremediablemente a ponerme en los zapatos de las otras víctimas del conflicto armado en Colombia, personas luchadoras que más que exigir venganza, quieren que los horrores de la guerra no se repitan en las vidas de los colombianos. Cuando hablas con cualquier víctima te encuentras con la nobleza pura, personas llenas de esperanza, paz y reconciliación; esta es la llave que trasciende para abrir la puerta a la paz real.

A pesar de mi dolor decidí trasformar la ausencia de mi madre en una oportunidad para ayudar a otros y hacerles ver que la solución a la guerra desmedida y a las ansias de venganza no son el camino correcto, esto solo generará un boomerang de emociones macabras que se traducen en la idea siniestra de cobrar las faltas de forma violenta, para expiar las culpas, para saciar la venganza.

La guerra no va a parar con más guerra y violencia, la guerra solo para cuando dejas de pelear y decides dialogar, este poder llamado reconciliación nos traerá un silencio fuerte que nos dejará escuchar por fin nuestra propia voz, la que muchos han tratado de opacar con el ruido de los fusiles.
