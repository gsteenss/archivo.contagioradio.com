Title: Samuel David, hijo de excombatientes fue víctima de la guerra con tan solo siete meses
Date: 2019-04-15 18:46
Author: CtgAdm
Category: Comunidad, Nacional
Tags: Asesinatos contra excombatientes, espacios de reincorporación
Slug: samuel-david-hijo-de-excombatientes-fue-victima-de-la-guerra-con-tan-solo-siete-meses
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/victima-de-la-guerra.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo] 

En horas de la noche del pasado sábado 13 de abril, **Carlos Enrique González y Sandra Pushaina** dos excombatientes de las Farc pertenecientes a la étnia Wayúu y su hijo **Samuel David de siete meses**, fueron a visitar a sus familiares al corregimiento Montelara, de Maicao, en La Guajira cuando hombres armados irrumpieron en la vivienda en la que se hospedaban y dispararon contra la familia, asesinando al pequeño, uno de los primeros en nacer en los **Espacios Territoriales de Capacitación y Reincorporación (ETCR). **

Carolina Vargas, una de las lideresas del **ETCR de Tierra Grata en La Paz**, **Cesar ** donde habita esta familia, relata que el bebé fue llevado a donde la abuela de Carlos, para que esta lo conociera, sin embargo, en horas de la madrugada, hombres con armas de fuego largas y de los que aún se desconoce su identidad o motivaciones para atacar a la familia, dispararon en diferentes direcciones  hiriendo de gravedad a Samuel, a  Carlos en el abdomen y  a Sandra en las piernas.

"No pudieron salir a tiempo del área" indica Carolina quien señala que el niño a pesar de luchar por su vida,  murió desangrado camino hacia un centro hospitalario en Maicao a media hora del lugar de los hechos. A pesar que los padres ya están fuera de peligro señala que la pérdida de su hijo ha significado un golpe muy duro tanto para ellos como para la comunidad, "queremos que se finalice de raíz este conflicto armado y lastimosamente los niños son los que pagan la mayor parte", agrega.

Carlos, padre de Samuel, ha pedido investigar los hechos mientras la comunidad del ETCR Simón Trinidad expresó su dolor a través de un comunicado "con su corta vida **Samuel nos deja reflexiones sobre las contradicciones de estos tiempos. Nacer como símbolo de esperanza de la construcción de Paz y morir asesinado para recordarnos que las condiciones de convivencia están distantes** aún en medio de un orden social en plena decadencia".

<iframe id="audio_34519301" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_34519301_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
