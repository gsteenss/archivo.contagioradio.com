Title: Dilema ético para el Consejo de Estado
Date: 2017-10-27 11:40
Category: Mision Salud, Opinion
Tags: Acceso a los medicamentos, Afidro, Consejo de Estado
Slug: dilema-etico-para-el-consejo-de-estado
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/10/salud-medicamentos.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Contagio Radio 

#### **Por: [María Imelda Moreno MD Esp. para Misión Salud](https://archivo.contagioradio.com/opinion/mision-salud/)** 

[¿Debe primar el interés económico exagerado de las multinacionales farmacéuticas (monopolio de comercialización de medicamentos, más allá de los veinte años de patente), sobre el bien común de la viabilidad del Sistema de Salud Colombiano?]

[Muchos profesionales del mundo de la salud en nuestro país llevamos años luchando contra la codicia desenfrenada de unos pocos que lleva a la pérdida de la vida (más de diez millones anuales en el mundo) o a la disminución de la salud de muchas personas por no poder acceder a los medicamentos o tratamientos necesarios. Lo que para nosotros no significa dilema ético, ya que tenemos la certeza de la primacía del bien común sobre el bien particular, curiosamente, puede ser motivo de confusión a nivel de los legisladores o de los tribunales de justicia. Fuimos testigos de la dificultad que acompañó definir la salud como un derecho fundamental, de las innumerables reuniones para conseguir que la atención de Cuidado Paliativo sea un derecho, y de los múltiples debates para lograr el respeto de la autonomía médica por encima del negocio de la salud, entre otras conquistas.]

[Ante la solicitud de Afidro,  de derogar el artículo 9 relacionado con la regulación de los medicamentos biogenéricos (resultado de un trabajo colaborativo de representantes de los diferentes actores comprometidos en el tema, incluido Afidro), no hay que dejarse confundir, esta no es una confrontación técnica, es sin duda, una solicitud movida por el gasto y la distribución de los recursos del sistema de salud, tal como queda evidenciado en el artículo anterior de este blog que muestra cifras concretas. Cada día de retraso en la aparición de medicamentos de la competencia implica una continuación de la ganancia desenfrenada de los monopolios y esta es la pretensión de Afidro, disfrazándola de preocupación por la seguridad de la salud de los colombianos, pidiendo que se exijan estudios además de innecesarios, antiéticos y muy costosos.]

[Tenemos claro que lo deseable es que la investigación y el desarrollo de medicamentos, los procedimientos y las técnicas para mejorar la salud de los pueblos no estén ligados a intereses monetarios particulares; sin embargo por ahora quedaríamos agradecidos con las multinacionales farmacéuticas si no codiciaran mayores ganancias de las que obtienen actualmente, que sin duda compensan los esfuerzos innovadores con creces. **(Leer: [Cada hora mueren en el mundo 1200 personas por falta de acceso a medicamentos](https://archivo.contagioradio.com/cada-hora-mueren-en-el-mundo-1200-personas-por-falta-de-acceso-a-medicamentos/))**]

[Está en manos del Consejo de Estado defender a los pacientes, que gracias al artículo 9 que Afidro busca eliminar, podrán recibir en el futuro tratamientos  necesarios, seguros y eficaces con un costo alto para el Sistema pero bastante menor al actual y por tanto con una posibilidad mayor para muchos, dado que la disminución de los precios podrá aumentar la cobertura con la misma inversión.]

[Confiamos en que el Consejo de Estado al resolver la demanda de Afidro dará prioridad a las conveniencias del Sistema de Salud y de las personas enfermas sobre los intereses comerciales de un puñado de grandes corporaciones multinacionales.]

[Los profesionales de la salud seguiremos abogando por el cumplimiento del derecho a la salud para todos los seres humanos, por el respeto de los recursos económicos asignados a la salud y por la equidad en su distribución, por la investigación  y el desarrollo que beneficien al mayor número de la población en el mundo desde la prevención, la promoción y el tratamiento oportuno y eficaz de las personas con enfermedades, desde el humanismo propio de nuestra labor.]

#### **[Leer más columnas de Misión Salud](https://archivo.contagioradio.com/opinion/mision-salud/)** 

------------------------------------------------------------------------

###### Las opiniones expresadas en este artículo son de exclusiva responsabilidad del autor y no necesariamente representan la opinión de Contagio Radio.
