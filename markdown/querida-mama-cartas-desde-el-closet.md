Title: Querida mamá - Cartas desde el Closet
Date: 2017-07-27 09:22
Category: Cartas desde el Closet
Tags: LGBTI
Slug: querida-mama-cartas-desde-el-closet
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/09/fotos-de-madres-hans.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: *Hans* 

###### [Julio 2017] 

He decidido escribir esta carta porque no sé cómo hablarte.

[No quería que te enteraras de esa manera que soy homosexual. Fue un momento muy triste para mí sentir la humillación de toda la familia y ver tu indiferencia mientras me iba de la casa. ]

[No espere nunca que mi ex novia fuera capaz de mandar esas fotos a mi muro de Facebook y que te enteraras por tus amigos del grupo de oración, quienes te contaron y reprocharon cuando te las mostraron.]

[Nunca haría nada para hacerte daño y por eso no fui capaz de contarte, no quería lastimarte ni avergonzarte.]

[Ser homosexual no es una venganza, ni es una consecuencia de tener un papá ausente.  Nunca te lo he reprochado, aunque te sientas culpable. Simplemente es lo que soy.]

[Irme de casa fue duro, salir huyendo sin tener a donde ir, como si hubiera hecho algo horrible. Espero que el grupo en el que estás cuide de ti con amor y te aliente a ser mejor cada día, no a odiar en nombre de Dios.]

[Aún me pregunto: ¿Crees que de verdad nuestra casa estaba contaminada por mi presencia como lesbiana? ]

[Te quiero decir que te amo profundamente y lamento que nuestra comunicación este cortada porque prefieras tener una hija muerta que lesbiana.]

[Siempre serás mi mamá y yo tu hija.]

**Parche por la vida, 4.**

------------------------------------------------------------------------

###### [**Este texto hace parte de Cartas desde el Closet, escritos anónimos de personas con orientaciones de género diversas que no han podido compartir públicamente sus pensamientos e ideas por su orientación sexual. Parche por la Vida **] 
