Title: "La paz territorial, no se siente en los territorios": Misión internacional
Date: 2017-11-03 15:40
Category: Nacional, Paz
Tags: Actores armados ilegales, acuerdos de paz, colombia, comunidades, Misión de Verificación, Violencia contra las mujeres
Slug: la-paz-territorial-no-se-siente-en-los-territorios-mision-internacional
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/11/misión-internacional-e1509734915478.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio ] 

###### [03 Nov 2017] 

Miembros de diferentes organizaciones internacionales, entregaron las conclusiones y recomendaciones preliminares de la misión internacional de verificación de los acuerdos de paz que realizaron en diferentes territorios del país. En rueda de prensa indicaron que **“el tiempo pasa y la paz aún no ha llegado a los territorios”.** Además, constataron el temor de las comunidades ante la presencia de grupos armados y la falta de avances en la implementación de los acuerdos.

La organización española Mundubat, Brigadas Internacionales de Paz, y la Oficina de Derechos Humanos Acción Colombia, **resaltaron la importancia de la consecución de un Acuerdo de Paz** que “puso punto final a más de 4 años de negociación (…) y que despertó muchas expectativas en toda la ciudadanía”.

Sin embrago, en su proceso de veeduría, constataron con las comunidades “una situación de **alto riesgo por la presencia de grupos armados** y su afán de control territorial”. Además, resaltaron la preocupación de la falta de presencia estatal en los territorios que dejaron las FARC por lo que manifiestan que el Estado “ha abandonado a las comunidades”. (Le puede interesar:["Sí hay paramilitares en el Catatumbo"](https://archivo.contagioradio.com/comision-de-verificacion-en-catatumbo-verifico-denuncias-sobre-presencia-paramilitar/))

### **Principales preocupaciones del grupo de verificación** 

<iframe src="https://co.ivoox.com/es/player_ek_21868857_2_1.html?data=k5almJ2ceZihhpywj5WXaZS1k5qah5yncZOhhpywj5WRaZi3jpWah5yncbPpw8rbjbLFstXZxNTbjcnJb83VjNTfycbSrdvVxM6SpZiJhpTijKffy8zFqMLnjMnSjbXFvozdz9nSj4qbh4630NPhw8zNs4zGwsnW0ZCRaZi3jpk.&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

De acuerdo con Rubén Mantencón, integrante de la organización Brigadas de Paz  de España, la principal preocupación de la misión es que en las comunidades “hay **un miedo permanente por la reconfiguración de los actores armados** en los territorios donde estaban las FARC”. Dijo que, como resultado de esto, las amenazas, agresiones y asesinatos a miembros de las comunidades han aumentado.

Adicionalmente, indicó que existe una preocupación por la **falta de garantías que tienen las y los líderes sociales** “para ejercer la defensa de los derechos humanos y territoriales de forma legal”. Fue enfático en recalcar que las personas en los diferentes territorios no pueden desarrollar sus derechos económicos, sociales, culturales y ambientales en la medida que están bajo amenaza.

De igual forma, la misión insistió en que en hay una **incertidumbre profunda por parte de los ex combatientes** que se encuentran en los Espacios Territoriales de Capacitación y Reincorporación debido a la inseguridad jurídica y económica “debido a la falta de implementación del acuerdo, lo que dificulta la reincorporación colectiva por la que las FARC apuestan”. (Le puede interesar: ["Proceso de implementación afronta serias dificultades: Misión de Verificación"](https://archivo.contagioradio.com/mision-internacional-verificacion-implementacion/))

Una preocupación adicional es la situación de estancamiento en el Congreso de la República **de la Jurisdicción Especial para La Paz**. Garbine Biurru, magistrada del Tribunal Superior del País Vasco, argumentó que “la justicia y la seguridad jurídica es fundamental para la estabilidad del proceso de paz, la incertidumbre y la impunidad sólo pueden conllevar a la repetición de los hechos”.

### **En los territorios continua la victimización de las Mujeres** 

<iframe src="https://co.ivoox.com/es/player_ek_21868886_2_1.html?data=k5almJ2cfJehhpywj5WbaZS1kpuah5yncZOhhpywj5WRaZi3jpWah5yncaTV09LS0JCxpcjVzdGSpZiJhpTihpewjbHNq8KfqtPhx9fSpcTd0NPOzpDIqYzB1s_S1MrXb9Hj05DZw5CRaZi3jqjc0NnFq8rjjLfOxs7Tb46ZmKialg..&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

De acuerdo con Carmen Magallón, presidenta de la sección española de la Liga Internacional de Mujeres por la Paz y la Libertad, en los territorios aún persisten violencias que caracterizan la victimización contra las mujeres que **“son asaltadas y violadas por todos los actores armados”.** Sin embrago, y a pesar de esto, indicó que evidenció la “resiliencia y capacidad para organizarse de las mujeres colombianas”.

Recordó que en Tumaco hay una organización de 40 mujeres “que han sido violadas y que no pueden salir de su comunidad por lo que han pedido un centro de salud”. Esto, para ella, **es una acción de voluntad de las mismas comunidades** para mejorar sus condiciones de vida. Sin embargo, dijo que hay un miedo constante de las mujeres por la presencia de nuevos grupos armados, “ellas se sienten vulnerables ante la posibilidad de que las violen y las dejen embarazadas”. (Le puede interesar: ["Mujeres ex combatientes en la implementación de los Acuerdos de Paz"](https://archivo.contagioradio.com/el-papel-de-las-mujeres-excombatientes-en-la-paz/))

Ante esto, Magallón hizo énfasis en que es necesario **proteger a estas mujeres y brindarles garantías** para que puedan realizar sus proyectos productivos y de vida. Indicó que en las comunidades “los hombres se comprometen poco en la crianza de los niños, que sigue recayendo en las mujeres y esto dificulta la participación política”.

### **Recomendaciones de la misión internacional** 

Finalmente, la misión internacional reafirmó su disposición para seguir realizando el **acompañamiento a las diferentes comunidades**. Indicaron que el Gobierno Nacional debe hacer todo lo posible por cumplir con sus compromisos y debe implementar los mecanismos necesarios para “hacer realidad la construcción de paz desde los territorios y para garantizar la participación política”.

Así mismo, manifestaron que las **Fuerzas Armadas deben estar al servicio de la protección de las comunidades,** que el Estado debe investigar los asesinatos y las agresiones a los líderes sociales a la vez que debe brindar garantías de seguridad para las comunidades. Reiteraron que “la paz no debe ser centralizada, y la paz será sentida si llega a los territorios”.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)

######  
