Title: 117 líderes fueron asesinados durante 2016
Date: 2017-01-24 16:59
Category: DDHH, Nacional
Tags: asesinatos de líderes sociales, Desaparición forzada, paramilitarismo en Colombia, Presencia Paramilitar
Slug: 117-lideres-fueron-asesinados-durante-2016
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/01/lideres-asesinados-20116.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [24 Ene 2017] 

El Instituto de Estudios para el Desarrollo y la Paz –INDEPAZ– elaboró un informe en el cual señala que durante 2016 se presentaron **117 homicidios de líderes sociales y defensores de derechos humanos, más de 350 amenazas, 46 atentados y 5 casos de desaparición forzada.**

En el informe, se destaca que más de la mitad de los hechos victimizantes sucedieron en el sur occidente colombiano en los departamentos de Valle, Cauca y Nariño, donde **fueron asesinados 57 líderes sociales, de los cuales 43 fueron en el Cauca, 9 en Nariño y 5 en el Valle.**

Más allá de las cifras, el documento llama la atención sobre la **importancia de analizar cada uno de los casos como un conjunto, no como hechos desligados uno del otro**, puesto que “las situaciones de amenazas, los asesinatos, atentados, las diferentes modalidades de ataque a las comunidades, todo forma parte de una misma situación”, puntualiza Leonardo González, coordinador de la unidad investigativa de INDEPAZ.

González, manifiesta que se logró determinar el aumento de la presencia de grupos paramilitares, especialmente en las zonas donde hacía presencia las FARC y revela que una de las preocupaciones es que **los homicidios cometidos contra líderes, han sido en el marco la primera fase de la transición hacia la paz,** teniendo como finalidad “el desplazamiento de las comunidades, la apropiación de territorios, la defensa a megaproyectos y el control político en las regiones”.

### ¿Qué tienen en común las amenazas? 

El informe también advierte sobre **“patrones comunes de amenaza” en 15 departamentos de Colombia**, todas han sido a través de panfletos, “donde indican nombres de personas pertenecientes a organizaciones, líderes que son acusados de guerrilleros encubiertos, señalados y condenados a muerte o conminados a desalojar las regiones”.

Frente a ello, INDEPAZ resalta que “todo este aparato que genera una mentalidad contrainsurgente no se ha desactivado”, que por el contrario se trata de una ideología que articula redes e individuos que recurren a estos procedimientos y que **“se exacerban ahora que se viene una coyuntura electoral de disputa por poderes locales”.**

Gonzáles señala que “hay una realidad dramática que tiene que ser atendida” y por ello se hace necesaria una voluntad de cambio representativa por parte de la sociedad colombiana, sin embargo afirma que de no actuar de manera apropiada y urgente sobre la **situación de los asesinatos de líderes y las amenazas a organizaciones sociales, “será imposible que podamos hablar de paz en nuestro país”.**

A modo de conclusión, INDEPAZ hace un llamado urgente “para que se apliquen las medidas efectivas para la protección del derecho a la vida”, pues actualmente más de 30 organizaciones son perseguidas de manera sistemática, **“es un llamado para que el Estado reconozca el fenómeno paramilitar existente y que les garantice la vida a los líderes** y defensores de derechos humanos”.

[Informe Final Anual 2016 INDEPAZ](https://www.scribd.com/document/337470449/Informe-Final-Anual-2016-INDEPAZ#from_embed "View Informe Final Anual 2016 INDEPAZ on Scribd") by [Contagioradio](https://es.scribd.com/user/276652802/Contagioradio#from_embed "View Contagioradio's profile on Scribd") on Scribd

<iframe id="doc_27777" class="scribd_iframe_embed" src="https://www.scribd.com/embeds/337470449/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-iyUHMT3eZqhRu0CDWjzz&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="1.2941176470588236"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
