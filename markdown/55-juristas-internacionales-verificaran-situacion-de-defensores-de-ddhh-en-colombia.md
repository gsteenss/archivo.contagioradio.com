Title: Caravana Internacional de Juristas llegan a Colombia a verificarán situación de DDHH
Date: 2016-08-22 12:24
Category: Nacional
Tags: colombia, Derechos Humanos, proceso de paz
Slug: 55-juristas-internacionales-verificaran-situacion-de-defensores-de-ddhh-en-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/08/Juristas1.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Caravana Internacional de Juristas 

###### [22 Ago 2016] 

Del 22 al 24 de agosto se llevará acabo la V Caravana Internacional de Juristas, que contará con la participación de abogados de Gran Bretaña, Irlanda, Australia, Estados Unidos, España, Canadá y Sur América, con el objetivo principal de conocer la situación de DDHH y las oportunidades frente al proceso de paz en Colombia.

La Caravana visitará **Barranquilla, Bucaramanga, Cali, Cartagena, Tumaco, Medellín y Cúcuta**, para constatar las condiciones de riesgo en las que los juristas y defensores de derechos humanos realizan su labor, teniendo en cuenta que pese a los avances en el proceso de paz, las agresiones, amenazas y asesinatos contra los defensores y defensoras han aumentado. Según el más reciente informe del programa Somos Defensores entre enero y junio de 2016, **[35 defensores de derechos humanos fueron asesinados](https://archivo.contagioradio.com/aumentan-los-riesgos-para-defensores-de-ddhh-en-colombia/)en Colombia y 279 sufrieron algún tipo de agresión.**

Teniendo en cuenta esas alarmantes cifras y denuncias frente a las deficientes garantías que brinda el gobierno para la protección de derechos humanos, esta misión elaborará un informe con recomendaciones a las autoridades colombianas y plantearán las acciones necesarias de acuerdo con las  peticiones y denuncias identificadas para que se ataquen los problemas estructurales que ponen en riesgo la vida de los defensores.

“Buscamos la protección e independencia de la labor jurídica, para que la profesión pueda ejercerse en libertad, sin obstáculos y amenazas, y aquí en Colombia hay muchas dificultades al respecto. **Queremos escuchar a los defensores y juristas sobre cómo está la situación para el escenario de posconflicto”**, dice Marina Biergman, una de las juristas que hace parte de la Caravana.

Fundada en 2008, la caravana está organizada en cuatro delegaciones de alto nivel. La primera  constaba de 70 abogados procedentes de Europa y América, luego en septiembre de 2014 más de 200 delegados habían visitado Colombia como parte de la Caravana, y esta vez **55 delegados de 15 países irán a 6 departamentos** para verificar la labor de la defensa de DDHH en Colombia.

Durante la visita, se prevé el acompañamiento a las personas en riesgo, así como entrevistas principalmente con víctimas, organizaciones de la sociedad civil y organismos públicos. De las visitas se pretende **elaborar un marco con las y los abogados colombianos para una futura colaboración, apoyo e intercambio.**

<iframe src="http://co.ivoox.com/es/player_ej_12626173_2_1.html?data=kpejlJuVe5Shhpywj5aVaZS1lp2ah5yncZOhhpywj5WRaZi3jpWah5yncaLgxt3O0MnJtozB0NPhw4qnd4a2ksaYj5CxpdPdz8aYpM7WscLihpewjajFtsLqwtPOja7SuMbmz8bQy9SRaZi3jqjc0NnFq8rjjLfOxs7Tb46ZmKialg..&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
