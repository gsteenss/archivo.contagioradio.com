Title: Así avanza la (in)justicia en el caso de Nicolás Neira
Date: 2019-06-05 14:37
Author: CtgAdm
Category: DDHH, Judicial
Tags: ESMAD, Fiscalía, Juez, nicolas neira, Yuti Neira
Slug: asi-avanza-la-injusticia-en-el-caso-de-nicolas-neira
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/Nicolás-Neira-e1559756420285.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: paredesquecomunican.com  
] 

Este miércoles se llevó a cabo un nuevo procedimiento judicial en el caso de Nicolás Neira, el joven de 15 años asesinado el 1 de mayo de 2005 por el oficial Néstor Rodríguez Rúa, del Escuadrón Móvil Antidisturbios (ESMAD). En la audiencia **se esperaba que el uniformado pidiera perdón por el asesinato de Nicolás**, en el marco del preacuerdo alcanzado entre la Fiscalía y el acusado para lograr una conciliación del caso; **sin embargo la disculpa no se dió.**

### **La familia de Nicolás Neira ha esperado 14 años y 1 mes para que se haga justicia** 

Han pasado 14 años desde que ocurrieron los hechos en los que **Nicolás fue asesinado por un proyectil lanzado por el agente del ESMAD durante la marcha de los trabajadores en 2005;** y esta audiencia sería la ocasión para que el uniformado se disculpara con la familia, aceptando su responsabilidad por utilizar armamento no letal, de forma fatal contra el joven que participaba en la marcha. (Le puede interesaR: ["Imputado Coronel de la Policía por asesinato de Nicolás N."](https://archivo.contagioradio.com/imputado-coronel-de-la-policia-por-asesinato-de-nicolas-neira/))

No obstante, la defensa de Nicolás, encabezada por [Yuri Neira](https://archivo.contagioradio.com/yuri-neira-una-historia-de-amor-y-resistencia/) (padre del joven), había señalado los errores en el preacuerdo que finalmente se pactaría hoy: la modificación del delito que le sería imputado a Rodríguez Rúa, pasando de homicidio doloso a culposo, lo que elimina la intención de hacer daño con que actúo el uniformado, y **la imposibilidad señalada por la Corte Constitucional de alcanzar acuerdos en casos con víctimas menores de edad.**

Pese a ambas advertencias, l**a familia de Nicolás esperaba las disculpas del oficial, lo que no ocurrió en razón de una nueva modificación realizada por la Fiscalía y el juez que lleva el caso.** (Le puede interesar: ["Medida de aseguramiento contra agente del ESMAD Rodríguez Rúa por asesinato de Nicolás N."](https://archivo.contagioradio.com/esmad_nicolas_neira_rodriguez_rua/))

**"Vemos una acción entre la Policía, Fiscalía y la justicia para que todo continúe en la impunidad"**

Como explicó Yuri Neira, la modificación del preacuerdo "se tranzó a puerta cerrada entre el nefasto fiscal, Juan Carlos Molina Oliveros" y el juez que lleva el caso horas antes que se adelantara la audiencia. Según Neira, allí **acordaron eliminar el pedido de perdón del agente sin contar con la participación siquiera del acusado,** "entonces vemos una acción entre la Policía, la Fiscalía y la justicia para que todo continúe en la impunidad", concluyó Neira.

Ante tal modificación, el delegado de la Procuraduría manifestó su negativa a la modificación, mientras que la defensa de Nicolás interpuso un recurso de reposición, búscando anular el preacuerdo adelantado entre el Fiscal y el acusado; y anunció que insistirá en que el caso sea revisado por la **Comisión Interamericana de Derechos Humanos (CIDH),** intentando que la presión internacional y el Sistema Interamericano de Derechos Humanos revela la verdad sobre los hechos al rededor del asesinato de Nicolás, y brinde justicia para su memoria.

> María Alejandra Garzón, abogada del caso explica lo sucedido en la audiencia de hoy en la que Yuri Neira, padre de Nicolás, salió del recinto después que la jueza aceptara retirar la cláusula que indicaba que Néstor Julio Rodriguez Rua debía pedir perdón público. [pic.twitter.com/6SGcZ72Qyy](https://t.co/6SGcZ72Qyy)
>
> — Movice (@Movicecol) [5 de junio de 2019](https://twitter.com/Movicecol/status/1136296383781228545?ref_src=twsrc%5Etfw)

<p>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
<iframe id="audio_36741185" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_36741185_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen">
</iframe>
</p>
###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
