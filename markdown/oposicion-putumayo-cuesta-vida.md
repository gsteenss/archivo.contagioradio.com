Title: Hacer oposición en Putumayo nos cuesta la vida: diputado UP
Date: 2020-01-24 16:50
Author: CtgAdm
Category: DDHH, Política
Tags: amenazas contra líderes sociales, extractivismo, Mocoa, Puerto Guzmán, Putumayo, Unión Patriótica
Slug: oposicion-putumayo-cuesta-vida
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/01/EKWb2rWWoAEu9hJ.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: Putumayo @CancimanceL

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

**Andrés Cancimance, diputado por la Unión Patriótica** de la Asamblea Departamental del Putumayo denuncia las amenazas de las que ha sido objeto por su labor en defensa del Acuerdo de Paz y su pertenencia a la comunidad LGBTI. Frente a ello, el funcionario relata lo que significa hacer oposición al sur de Colombia en un departamento donde, sin finalizar el primer mes del año, han sido asesinados seis líderes sociales.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

El pasado 21 de enero, el [esquema de protección](https://twitter.com/CancimanceL/status/1219681413668200450) que le fue asignado a Andrés Cancimance desde las elecciones del pasado 27 de octubre le fue retirado. Días después las amenazas en su contra se intensificaron a través de **panfletos en los que el Cartel de Sinaloa, lo declara objetivo militar, ofreciendo 30 millones de pesos por su vida.**

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Afirma que al ser integrante de un partido que fue víctima de un exterminio como la Unión Patriótica, las frecuentes denuncias que ha hecho contra el asesinato de líderes sociales y campesinos en la región, su defensa por la implementación del Acuerdo en todas sus dimensiones, las críticas que ha realizado a la fumigación con glifosato, la extracción minera y petrolera son las que lo han convertido en un blanco de personas y grupos que se oponen a la paz, **"la situación es muy crítica, quedarme sin un esquema de protección es un gran riesgo, principalmente porque estoy en el territorio recorriendo los 13 municipios de este departamento"**. ["Hay un plan para asesinarme y se va a ejecutar" advierte líder Ingrid Vergara](https://archivo.contagioradio.com/hay-un-plan-para-asesinarme-y-se-va-a-ejecutar-advierte-lider-ingrid-vergara/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

A su vez señala que al hacer parte de la comunidad LGBTI su orientación sexual podría ser otro de los factores de rechazo y de violencia en su contra, "es una comunidad que sigue estando amenazada, no es fácil asumir esta identidad, hay mucho temor, el 90% de los casos de maltrato no se reportan porque lo que lo origina es la condición sexual". [(Le puede interesar: Por primera vez, informes de población LGBTI llegan a una Comisión de la Verdad)](https://archivo.contagioradio.com/por-primera-vez-informes-de-poblacion-lgbti-llegan-a-una-comision-de-la-verdad/)

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Puerto Guzmán, un reflejo de la violencia

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

El diputado se refirió a la violencia en Puerto Guzmán, municipio fronterizo con el Caquetá, donde han sido asesinados al menos tres líderes en lo corrido del año. En la zona, existió un dominio del frente 32 de las FARC por más de 20 años y a su vez hizo permanencia el bloque sur Putumayo de las Autodefensas Unidas de Colombia (AUC) lo que históricamente lo ha convertido en un territorio en disputa.[(Lea también: Con asesinato del líder Yordan Tovar, continúa la violencia en Puerto Guzmán, Putumayo)](https://archivo.contagioradio.com/asesinato-lider-yordan-tovar-puerto-guzman-putumayo/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

A su vez, destaca que la ola de violencia se ha intensificado contra quienes promueven el plan de sustitución de cultivos de uso ilícito y el narcotráfico pues los perfiles de los asesinatos son de personas que trabajan con el PNIS, como fue el caso de Gloria Ocampo, secretaria de la Junta de Acción Comunal y quien además promovía la sustitución voluntaria.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### "La historia del Putumayo ha sido la historia del extractivismo"

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

"Si en otra época había una correlación entre desplazamiento y cultivos de palma a través de multinacional, aquí estamos viendo asesinatos, amenazas y desplazamientos directamente relacionados con la entrada de megaproyectos" expresa el diputado quien atribuye estos factores estructurales a las continuas disputas entre las disidencias de las FARC y agrupaciones armadas que se denominan como el Cartel de Sinaloa o La Constru. [(Lea también: Denuncian que Plan Nacional de Desarrollo dejaría políticas LGBT+ 'en el closet')](https://archivo.contagioradio.com/plan-politicas-lgbt-closet/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Cancimance concluye que otra particularidad es el temor que se ha propagado entre la población que teme denunciar agresiones en su contra, "la mayoría de los casos no pasan por las rutas institucionales de denuncia" afirma el diputado quien es enfático en que los ataques no solo están enfocados en líderes sociales, **"es un mensaje claro, te pueden matar así no ejerzas un liderazgo, en otros tiempos la condición de liderazgo explicaba por qué las muertes violentas, ahora están mezcladas con ataques a campesinos, ese es un mensaje poderoso de la vía armada".**

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->
