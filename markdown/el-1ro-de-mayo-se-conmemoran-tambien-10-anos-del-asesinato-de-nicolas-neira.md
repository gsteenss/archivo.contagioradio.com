Title: El 1ro de mayo se conmemoran también 10 años del asesinato de Nicolás Neira
Date: 2015-04-30 12:24
Author: CtgAdm
Category: Otra Mirada, Resistencias
Tags: angel molano, brutalidad policial, Derechos Humanos, ESMAD, nicolas neira, yuri neira
Slug: el-1ro-de-mayo-se-conmemoran-tambien-10-anos-del-asesinato-de-nicolas-neira
Status: published

###### Foto: U. Pública Resiste 

<iframe src="http://www.ivoox.com/player_ek_4430484_2_1.html?data=lZmgkpmceI6ZmKiakpuJd6KolpKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRic2fktfcjcnJb87V2tSY1cqPp9Dizsra0dfFsozowtLPy4qnd4a1mtOYk5WPpYa3lIqvk9TXb8XZzZDO1crXcYarpJKw0dPYpcjd0JC%2Fw8nNs4yhhpywj5k%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Ángel Molano, Fundación Nicolás Neira] 

En un país en el que los crímenes cometidos por el ESMAD se ven cobijados por la impunidad y el olvido, la memoria es un acto de resistencia. Mujeres y hombres que luchan en Colombia contra la brutalidad policial y la militarización de la vida en Colombia, hacen un llamado a fortalecer los lazos para la vida y tejer memoria, en el marco de la conmemoración de los 10 años del asesinato del joven Nicolás Neira.

Cabe recordar que durante las manifestaciones por el día internacional de los trabajadores y las trabajadoras, el 1ro de mayo del año 2005, el ESMAD golpeó brutalmente al joven Nicolás, de 15 años de edad, quien falleció el 6 de mayo del mismo año como consecuencia de la muerte cerebral y las heridas en el cuerpo que le dejó el ataque por parte del Escuadrón Móvil Anti Disturbios.

<div>

A pesar de que el Estado colombiano recibió una condena por este caso, y que los miembros del ESMAD implicados fueron retirados de sus cargos; los casos de abuso de fuerza y crímenes cometidos por agentes del Estado aumenta cada día en Colombia, sin que se tomen las medidas pertinentes para evitarlos. Por el contrario, posturas como la del procurador general, Alejandro Ordoñez, quien profirió un fallo para restituir en función a los miembros del ESMAD implicados en el asesinato de Nicolás, son muestras de la falta de voluntad política para acabar con los abusos por parte de los agentes estatales.

<div>

Según denuncia la Fundación Nicolás Neira, en los últimos años, se ha fortalecido al ESMAD, que "continúa asesinando, torturando, y deteniendo masiva y arbitrariamente a jóvenes, indígenas, campesinos y campesinas que luchan por sus derechos", declaró Ángel Molano, de la Fundación.

En una apuesta por la reconstrucción de la memoria, y el fortalecimiento de la lucha contra la brutalidad policial en Colombia, se conmemorarán los 10 años del asesinato de Nicolás Neira, el 1ro de mayo del 2015 en la carrera 7ma con calle 28, en el mismo lugar en el que fuera golpeado por el Escuadrón Antidisturbios.

Desde las 8 de la mañana habrá actos culturales, galerías de la memoria y palabras para rechazar el olvido con el que se cobijan los crímenes del Estado.

</div>

</div>
