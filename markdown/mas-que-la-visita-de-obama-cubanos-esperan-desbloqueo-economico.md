Title: Visita de Barack Obama a Cuba debería ser un paso más al fin del bloqueo
Date: 2016-03-19 14:00
Category: El mundo, Política
Tags: Barck Obama, Cuba, Estados Unidos, Raul Castro
Slug: mas-que-la-visita-de-obama-cubanos-esperan-desbloqueo-economico
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/03/Cuba-y-Estados-Unidos.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Diario Contexto 

<iframe src="http://co.ivoox.com/es/player_ek_10881741_2_1.html?data=kpWlmpabeJKhhpywj5WcaZS1kpmah5yncZOhhpywj5WRaZi3jpWah5yncbfd1M7hw5DIqYy2wtfOxdCPk8PVzsaYw5CnucPVjMnSxMrWaaSnhqaxw5DXqdOf1tOY0sbXs4zhhqigh6aVt46ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Felix Albisu, Prensa Latina] 

###### [18 Mar 2016] 

Este domingo el presidente de Estados Unidos, Barck Obama, visita Cuba en medio de la persistencia del bloqueo económico, comercial, financiero, así como otras leyes discriminatorias que afectan a la población cubana.

Frente a la visita de Obama, el canciller Bruno Rodríguez afirmó que recibirán al presidente de Estados Unidos con "la hospitalidad que nos caracteriza", sin embargo señaló diversas situaciones que podrían resolverse con la dinámica del avance del restablecimiento de las relaciones diplomáticas.

Por ejemplo Rodríguez señala que "La medida anunciada con relación al uso por parte de Cuba del dólar estadounidense, **no significa... que se hayan normalizado las relaciones bancarias entre Cuba y Estados Unidos**", o las acciones que restringen y condicionan la práctica del turismo para ciudadanos de EEUU porque invita "promover la independencia de los cubanos" lo cual puede interpretarse como una desaprobación de un modelo económico.

Aquí el discurso completo

[Intervención de Canciller de Cuba Bruno Rodríguez de Cara a La Visita de Presidente de EEUU Barack Obama](https://es.scribd.com/doc/305334659/Intervencion-de-Canciller-de-Cuba-Bruno-Rodriguez-de-Cara-a-La-Visita-de-Presidente-de-EEUU-Barack-Obama "View Intervención de Canciller de Cuba Bruno Rodríguez de Cara a La Visita de Presidente de EEUU Barack Obama on Scribd")

<iframe id="doc_85406" class="scribd_iframe_embed" src="https://www.scribd.com/embeds/305334659/content?start_page=1&amp;view_mode=scroll&amp;show_recommendations=true" width="70%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="undefined"></iframe>  
Felix Albisu, director de **Prensa Latina** para Colombia, asegura que las expectativas e interrogantes son muchos, pero califica la visita como “muy positiva” pues “**se rompen un lapso muy largo de enemistad y confrontación”**, teniendo en cuenta que hace 80 años un mandatario estadounidense no visitaba la isla.

Pese a que la visita de Barack es un paso importante para avanzar en las relaciones entre Cuba y Estados Unidos, **“lo fundamental es el levantamiento del bloqueo económico comercial y financiero a Cuba,** que incide sobre la vida cotidiana de los ciudadanos de la isla. Sino se resuelve eso, será muy difícil continuar un proceso de normalización”, explica Albisu, quien señala que en las relaciones entre estas dos naciones hay “muchos obstáculos creados por los Estados Unidos”.

De acuerdo con el periodista de Prensa Latina, el presidente Obama ha tomado otra dirección y es partidario de levantar el bloqueo “que lo tiene maniatado por el propio Congreso de EEUU”, pero a su vez, esto debe leerse como un cambio de estrategia en relación con la revolución cubana, “porque **por otros caminos se intentará subvertir el orden socialista de Cuba”,** algo que “los cubanos tienen claro, aunque saben que siempre será mejor vivir en un ambiente de entendimiento y amistad con los vecinos y no en confrontación de la guerra fría”.

La **[población cubana se encuentra a la expectativa de la visita y lo que se pueda dar después de ella](https://archivo.contagioradio.com/rene-gonzalez-heroe-cubano-comparte-su-testimonio-de-vida-hoy-en-bogota/)**, aunque se desconoce hasta donde lleguen las intenciones de Estados Unidos, teniendo en cuenta el cambio de gobierno. Según lo que se ha prometido en campaña, tanto Donald Trump como Hillary Clinton continuaría fortaleciendo la normalización de las relaciones bilaterales entre los dos países, aunque **habría mayor seguridad si la candidata demócrata fuera la presidenta de Estados Unidos debido a que sus proyecciones son muy afines a las de Obama**, como lo explica Albisu.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
