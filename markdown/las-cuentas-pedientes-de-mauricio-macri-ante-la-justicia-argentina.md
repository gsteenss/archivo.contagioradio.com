Title: Las cuentas pedientes de Mauricio Macri ante la justicia Argentina
Date: 2015-11-24 10:52
Category: El mundo, Judicial
Tags: Argenitna, Elecciones en Argenitna, investigaciones contra Macri, Kishnerismo, Madres de la Plaza de Mayo, Mauricio Macri
Slug: las-cuentas-pedientes-de-mauricio-macri-ante-la-justicia-argentina
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/11/Macri_contagioradio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: minutouno 

###### [24 Oct 2015] 

Desde que Macri asumió como alcalde en la ciudad de Buenos Aires ha venido sumando denuncias por  estafa y asociación ilícita, abuso de autoridad y violación de deberes de funcionario público, enriquecimiento ilícito, falsificación de documentos públicos, amenazas y abandono de personas, entre otras denuncias. Así lo han señalado diversos medios tanto locales como internacionales, que sumarían por lo menos 214 denuncias.

Por ejemplo, la Sala I de la Cámara Federal adelanta un proceso en contra de Macri por el delito de asociación ilícita dedicada al espionaje ilegal. Las víctimas serían dos familiares de víctimas de la AMIA a las que Macri y su familia habrían espiado ilegalmente con la colaboración del jefe de policía de Buenos Aires que también ha declarado en el proceso y aunque la causa se abrió en 2010 diversas maniobras dilatorias han evitado un fallo definitivo.

Otra de las denuncias más sonadas tiene que ver con hechos de violencia contra las mujeres. El recién electo presidente de Argentina en Octubre de 2012 reveló datos de una mujer a la que se le adelantaría un proceso de aborto legal, la dejó al descubierto y generó una serie de denuncias contra la mujer y evidenció la oposición conservadora al aborto legal por parte del macrismo.

Además Macri ha sido denunciado porque en la campaña de 2011 para su reelección como gobernante de Buenos Aires fue financiado por Martins Coggiola, prófugo de la justicia y líder de una red de proxentismo y esclavitud sexual que trafica con mujeres de Brasil y de República Dominicana según lo ha denunciado la organización La Alameda.

La denuncia es refrendada por la propia hija de Martins que denunció que el puente entre Macri y Coggiola lo hace Gabriel Conde, propietario del prostíbulo Shampoo en México en el que el electo presidente habría sostenido varios encuentros con quién también ejerció la vice presidencia del club deportivo Boca Juniors cuando Macri fue presidente.

\[caption id="attachment\_17724" align="aligncenter" width="407"\][![foto ong la Alameda](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/11/macri-conde.jpg){.wp-image-17724 width="407" height="232"}](https://archivo.contagioradio.com/las-cuentas-pedientes-de-mauricio-macri-ante-la-justicia-argentina/macri-conde/) foto ong la Alameda. Macri con Conde en supuesto prostíbulo mexicano\[/caption\]

Hasta el momento ninguna de las causas ha tenido una condena y se encuentran en etapa de investigación, sin embargo Macri se convierte en el político de alto nivel más investigado de la nación suramericana.
