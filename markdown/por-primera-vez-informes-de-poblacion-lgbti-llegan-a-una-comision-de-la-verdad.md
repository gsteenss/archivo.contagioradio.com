Title: Por primera vez, informes de población LGBTI llegan a una Comisión de la Verdad
Date: 2019-09-20 18:11
Author: CtgAdm
Category: LGBTI, Memoria
Tags: comision de la verdad, Comunidad LGBTI
Slug: por-primera-vez-informes-de-poblacion-lgbti-llegan-a-una-comision-de-la-verdad
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/09/Comisión-Verdad-LGBTI.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/09/Comisión-LGBTI.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:ComisióndelaVerdad] 

La organización Caribe Afirmativo entregó el informe 'Nosotras Resistimos" que recopila los actos de violencia cometidos contra la población LGBTI en el marco del conflicto armado, este resulta ser el primero de su naturaleza entregado a una Comisión de la Verdad en el mundo y se espera sea un aporte a la reconstrucción de memoria del país.

Según, Wilson Castañeda, director de Caribe Afirmativo, la entrega de este informe contó con la participación directa de las víctimas y hace énfasis en cuatro puntos estratégicos, el primero resalta que existen 53 casos documentados de personas afectadas de la comunidad LGBTI en los departamentos de **Antioquia, Córdoba, Sucre, Bolívar y Chocó** en el marco del conflicto armado, mientras en segundo lugar realiza una diferenciación de los hechos de violencia según sus perpetradores, variando entre FARC-EP, ELN, paramilitares y fuerzas del Estado.

En tercer lugar el informe da cuenta de algunas prácticas de resistencia en los territorios y de cómo las víctimas construyeron estrategias de supervivencia **"que deberían ser valoradas como tácticas de construcción de paz en el territorio**", finalmente el informe da cuenta de propuestas construidas con las víctimas que buscan una reparación colectiva que involucre a diferentes actores para mandar un mensaje de reconocimiento, reparación y no repetición.

\[caption id="attachment\_73992" align="aligncenter" width="800"\]![Comisión LGBTI](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/09/Comisión-LGBTI.jpg){.size-full .wp-image-73992 width="800" height="533"} Foto: Comisión de la Verdad\[/caption\]

### Expectativas de la población LGBTI frente a la Comisión de la Verdad

El director de Caribe Afirmativo resalta la importancia de este primer informe de la comunidad LGBTI ante una organización que pertenece al Sistema Integral de Justicia, haciendo una salvedad de casos en Perú y Sudáfrica. **"Nuestra expectativa es que efectivamente en su macro informe dé una mirada desde la realidad de las personas LGBTI y que la Comisión genere puentes y rutas de atención para las víctimas"**, agregó.

> *La Comisión de  la Verdad deber ser la entidad que lidere en Colombia un gran paso de reconciliación de la sociedad civil con la población LGBTI para desterrar prácticas de discriminación y que el respeto a la diversidad sexual sea un valor prioritario*

Finalmente, Castañeda señala que este Acuerdo de Paz, tiene la responsabilidad de marcar una rutas para otros países que puedan conformar una comisión de la verdad y que esta no deje por fuera a las personas LGBTI y de paso, generar espacios en los territorios para que puedan formar a las personas en este temática. [(Le puede interesar: Que Duque no nos obligue a volver al clóset: Caribe Afirmativo)](https://archivo.contagioradio.com/duque-no-nos-obligue-a-volver-al-closet-caribe-afirmativo/)

<iframe id="audio_41901049" frameborder="0" allowfullscreen scrolling="no" height="200" style="border:1px solid #EEE; box-sizing:border-box; width:100%;" src="https://co.ivoox.com/es/player_ej_41901049_4_1.html?c1=ff6600"></iframe>  
**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
