Title: Juez ordena desalojar a 36 familias indígenas Inga damnificados por avalancha en Mocoa
Date: 2019-12-09 17:46
Author: CtgAdm
Category: Comunidad, Judicial
Tags: avalancha, Desplazamiento, Inígenas, Mocoa, Putumayo
Slug: juez-ordena-desalojar-a-36-familias-indigenas-inga-damnificados-por-avalancha-en-mocoa
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/EK4KBPqXsAAaTUI.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/WhatsApp-Video-2019-12-09-at-6.26.21-AM.mp4" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:Marino Peña] 

[Este lunes 9 de diciembre finalizan los 5 días de plazo, para el desalojo de 36 familias pertenecientes al cabildo indígena Inga Musu Runakuna, comunidad que habitaba en  la vereda San Antonio, destruida en la avalancha en 2017. La orden de despojo se da luego de 32 meses, tiempo en el que este territorio estuvo en arriendo, y a la espera de una confirmación por parte de Agencia  Nacional de Tierras (ANT) para su compra. (Le puede interesar:[Sin soluciones concretas, vendrían nuevas calamidades para Mocoa](https://archivo.contagioradio.com/sin-soluciones-concretas-para-mocoa/))]{.css-901oao .css-16my406 .r-1qd0xha .r-ad9z0x .r-bcqeeo .r-qvutc0}

La comunidad Inga se instaló en este predio, según Marino Peña, exgobernador del Cabildo, para procurar la normalidad de la vida comunitaria que está en riesgo por la falta de cumplimiento por parte del Gobierno; *"Desde que sucedió la avalancha estamos pidiendo atención para nuestras familias,  tomamos en arriendo unos predios pero desde el 2017 hasta hoy, y aún habiendo actas de compromiso donde la ANT se responsabilizaba a comprar los predios, que el propietario voluntariamente había ofertado, este es el momento que nos están sacando por su falta de pago"*, señaló Peña.

De igual forma el exgobernador resaltó, *"Los líderes del cabildo condenamos este acto y responsabilizaron de negligencia al Ministerio de Interior y a la directora de la Agencia Nacional de Tierras de Colombia, por no cumplir con los fallos judiciales y por el tercer desplazamiento forzado que pueden generar en la comunidad Musu Runakuna que se encuentran  todavía  sin territorio";* y destacó que es necesario  que se emita la orden de compra inmediata del territorio para asegurar las vidas de las 36 familias.

### ¿Qué ha pasado con la comunidad desde la avalancha de Mocoa en 2017?

La comunidad indígena [ Inga Musu Runakuna,]{.css-901oao .css-16my406 .r-1qd0xha .r-ad9z0x .r-bcqeeo .r-qvutc0} está ubicada en Mocoa, Putumayo, y se ha visto vulnerada por la afectación  del conflicto armado, y especialmente el fuerte desplazamiento de tierra del 31 de marzo y nuevamente el primero de abril de 2017; desastre climático qué generó la pérdida total del territorio, y con el sus viviendas, y más de 2.500 zonas de vegetación, que suplía su alimentación y medicina. (Le puede interesar:[Gobierno Nacional se olvidó de los damnificados en Mocoa](https://archivo.contagioradio.com/gobierno-nacional-se-olvido-de-los-damnificados-en-mocoa/))

Posteriormente tomaron en arriendo los predios en Bella Vista y Villa Hermosa, territorios de propiedad privada, ubicados en la vereda el Pepino. *"Este predio se arrendó por la comunidad con opción de compra, en razón a que el propietario ofertó estas tierras a la gente, no obstante la falta de respuesta estatal y el incumplimiento en el pago, generó que el dueño pidiera el desalojo; acción que se da después de 2 años y medio de ocurrida la catástrofe, en la que el Gobierno no han resuelto de fondo la solicitud legítima de la comunidad, que es tener sus propias tierras de nuevo"*, indicó Peña.

Los recursos destinados para la reconstrucción de Mocoa sumaban más de 2 billones de pesos, de los cuales \$5.500 millones fueron designados para la compra de predios; *"No sabemos porqué pensé a que el documento Compes 1904, designado para la reconstrucción de nuestro territorio, señala que hay un dinero designado a la compra  de nuevos espacios territoriales, la institucionalidad  no ha asumido el papel y no ha adquirido los predios que nosotros estamos demandando",* agregó Peña.

Los representantes del Cabildo han llevado este caso a diferentes organizaciones, entre ellos el Alto Comisionado de las Naciones Unidas para los Refugiados (ACNUR),  que ya ha tomado cartas en el asunto; la Defensoría del Pueblo y la Alcaldía Municipal. Asimismo el exgobernador señaló que se esta siguiendo una directriz, *"Nosotros habíamos escuchando que el senador Alvaro Uribe,  dijo que no se le diera ni una hectárea mas para los indígenas, entonces no sab*emos si el señor alcald*e y los responsables de la reconstrucción de Mocoa están siguiendo esta directriz".*

\[video width="640" height="352" mp4="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/WhatsApp-Video-2019-12-09-at-6.26.21-AM.mp4"\]\[/video\]

 

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
