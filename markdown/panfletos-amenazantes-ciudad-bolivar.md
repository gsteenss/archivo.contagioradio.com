Title: Con panfletos amenazantes advierten "toque de queda" en Ciudad Bolívar
Date: 2019-05-27 19:04
Author: CtgAdm
Category: Comunidad, Nacional
Tags: amenaza, ciudad bolivar, Limpieza social, Panfleto
Slug: panfletos-amenazantes-ciudad-bolivar
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/08/ciudad-bolivar.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo] 

Habitantes denunciaron la aparición de panfletos en la localidad de Ciudad Bolívar, Bogotá, en los que se amenazaba con realizar una 'limpieza social', especialmente contra personas provenientes de Venezuela y "viciosos". El panfleto se comenzó a difundir desde el pasado martes 21 de mayo; y **según el edil de la localidad, Christian Robayo,** esta es otra evidencia de los intereses de los grupos ilegales que hacen presencia en este territorio. (Le puede interesar: ["Vida de jóvenes en Ciudad B. es amenazada por control de bandas criminales"](https://archivo.contagioradio.com/vida-de-jovenes-de-ciudad-bolivar-amenazada-por-control-de-bandas-criminales/))

> Este es el panfleto que circula en [\#CiudadBolívar](https://twitter.com/hashtag/CiudadBol%C3%ADvar?src=hash&ref_src=twsrc%5Etfw) [\#Bogotá](https://twitter.com/hashtag/Bogot%C3%A1?src=hash&ref_src=twsrc%5Etfw)  
> Amenanza con asesinar a jóvenes y a venezolanos. [pic.twitter.com/UMBNMz6s3M](https://t.co/UMBNMz6s3M)
>
> — Daniel Bejarano (@DanielBejarano\_) [26 de mayo de 2019](https://twitter.com/DanielBejarano_/status/1132789871796473858?ref_src=twsrc%5Etfw)

<p>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
</p>
Como lo explicó Robayo, integrante del Polo Democrático, en Ciudad Bolívar la situación es crítica desde hace tiempo. Razón por la cual la Defensoría del Pueblo emitió una alerta temprana el año pasado señalando la presencia de grupos armados ilegales en esta zona y otros territorios como Soacha, Usme y Kennedy. En la alerta se manifestaba la preocupación ante el aumento de muertes violentas, y otros problemas asociados con el microtráfico.

El Edil indicó que en el sur de Bogotá, especialmente en las salidas de la Capital hacia Villavicencio y Sibaté, hacen presencia grupos **"reductos del paramilitarismo",** que se camuflan con negocios lícitos y manejan el microtráfico y la extorsión. El líder político afirmó que incluso se ha advertido sobre la presencia de enviados de las autodenominadas Autodefensas Gaitanistas de Colombia (AGC), que están buscando alianzas con los grupos ya establecidos en el territorio.

### **En 2018 se presentaron 249 homicidios en Ciudad **Bolívar

Robayo también manifestó la preocupación ante la cantidad de muertes violentas, pues "de enero a diciembre de 2018 se presentaron 249 homicidios" y **en lo corrido del año van 59**; situación que debería ser atendida de forma especial por la Administración Distrital. En consecuencia, el Edil pidió a la Fiscalía que se investiguen los posibles autores del panfleto, a la Fuerza Pública que proteja los derechos de los ciudadanos, y que se tomen medidas urgentes desde la Alcaldía tanto de la localidad como del Distrito.

<iframe id="audio_36376418" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_36376418_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
