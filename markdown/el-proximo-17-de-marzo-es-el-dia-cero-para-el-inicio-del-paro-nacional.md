Title: Paro Nacional inicia el próximo 17 de marzo
Date: 2016-02-15 13:36
Category: Movilización, Nacional
Tags: Cumbre Agraria, Paro Nacional
Slug: el-proximo-17-de-marzo-es-el-dia-cero-para-el-inicio-del-paro-nacional
Status: published

###### [Foto: El Pais] 

###### [15 Feb 2016]

En una reunión llevada a cabo este lunes en la ciudad de Bogotá, el Comando Unitario Nacional, conformado por plataformas de organizaciones sociales y sindicales, definieron que **el próximo 17 de Marzo se inician las jornadas de movilización que confluirán en un gran paro nacional indefinido**, en respuesta al incumplimiento del Gobierno frente a los acuerdos pactados en el paro de 2013 con la Cumbre Agraria Campesina, Étnica y Popular, según afirma Ricardo Herrera, vocero del Coordinador Nacional Agrario.

En esta primera jornada entraran en paro por 24 horas centrales sindicales y obreras, que serán apoyadas por las **más de cien organizaciones articuladas desde sus particularidades y "desde la diversidad para trabajar por lo común" y manifestar la "gran inconformidad generalizada"** por las políticas regresivas del actual Gobierno, como asevera Herrera, quien agrega que le exigirán al Presidente que no se siente a negociar con cada sector por separado en una mesa, como ha hecho en paros anteriores, sino que cumpla integralmente con todas las demandas.

De acuerdo con Herrera, los **incumplimientos de la Presidencia llevaron a que el paro de 2013 se retomara en 2014**, año en el que se acordó un pliego único de negociación con puntos sobre derechos humanos, territorio, proyectos de infraestructura y seguridad alimentaria, principalmente, frente al que no se han logrado mayores avances. "Hemos avanzado en un 10% en un fondo de fomento agropecuario para proyectos de inversión", **\$250 mil millones que no han sido desembolsados a los campesinos debido "a todas las trabas" que el Gobierno les ha puesto**, como asegura el vocero.

A las exigencias del sector agrario, se suman las demandas de centrales sindicales frente al exiguo aumento del salario mínimo, de los transportadores en relación con las tarifas de los fletes, y las de distintos sectores sociales que consideran **regresivas las políticas económicas actuales, para la materialización de la paz en un escenario de posconflicto**, por lo que acuerdan programar un gran paro nacional indefinido "hasta que el Gobierno de respuestas concretas frente a las necesidades demandadas", como la derogación de la [[Ley de Zidres](https://archivo.contagioradio.com/zidres-legalizan-el-acaparamiento-de-tierras-en-casanare/)].

###### Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)] 
