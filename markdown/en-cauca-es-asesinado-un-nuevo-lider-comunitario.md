Title: En Cauca es asesinado un nuevo líder campesino
Date: 2016-12-26 07:05
Category: DDHH, Nacional
Tags: asesinato, Cauca, Derechos Humanos, lider comunitario
Slug: en-cauca-es-asesinado-un-nuevo-lider-comunitario
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/03/cauca-.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Contagio Radio 

###### 25 Dic 2016 

El 25 de diciembre hacia las 9:30 de la mañana en la vía que comunica del Plateado a Argelia, Cauca, en el Punto llamado el Chamuscado, vereda Deziderio Zapata, fue asesinado el líder comunitario **Anuar José Álvarez Armero, socio de ASCAMTA, organización que integra el movimiento Marcha Patriótica**, líder campesino de la vereda Mirolindo y perteneciente al Comité cocalero de esa vereda. Le puede interesar: [No cesan las agresiones contra integrantes de Marcha Patriótica](https://archivo.contagioradio.com/?p=32972)

Según la denuncia de la Red de Derechos Humanos del suroccidente colombiano “Francisco Isaías Cifuentes”, el líder se transportaba en una motocicleta eco delux, desde el Plateado hasta su casa en la vereda Mirolindo, en el trayecto fue alcanzado por dos sujetos que se movilizaban en una motocicleta Honda color azul, quienes dispararon en cuatro ocasiones causándole heridas de gravedad en el pecho, el dorso, y el brazo. Pese a estos disparos Anuar pudo llegar al hospital de Argelia con ayuda de pobladores pero horas después debido a la gravedad de las heridas falleció.

El asesinato del líder campesino se da en medio de amenazas e intimidaciones de grupos paramilitares en la región, durante los últimos días se han conocido panfletos firmados por el grupo "Autodefensas Unidas de Colombia" en diferentes municipios de este departamento ubicado al suroccidente del país.

Ver **[Asesinatos de líderes sociales son práctica sistemática: Somos Defensores](https://archivo.contagioradio.com/asesinatos-de-lideres-son-practica-sistematica-33507/).**
