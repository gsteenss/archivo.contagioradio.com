Title: Música en Solaridad con los 13
Date: 2015-08-20 17:01
Category: eventos, Movilización
Tags: carlos lugo, Concierto solidaridad 13, Desarme banda, El man del bajo, Latino power, Leonardo (Pasajeros), Reincidentes Bogotá
Slug: musica-en-solaridad-con-los-13
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/08/solidaridad-13-e1440108453190.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [20, ago, 2015]

Un llamado a la solidaridad a través de la música se realizará este viernes desde las 9 de la noche en "Latino Power". 5 agrupaciones subirán a tarima con el propósito de apoyar, desde la música, a los 13 jóvenes detenidos y procesados por las explosiones en la ciudad de Bogotá, y en su nombre a todos los presos políticos que existen en el país.

Bandas invitadas:

Desarme

<iframe src="https://www.youtube.com/embed/uuZwsWA7zw0" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

Reincidentes

<iframe src="https://www.youtube.com/embed/VubgGI93gqg" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

Carlos Lugo

<iframe src="https://www.youtube.com/embed/Esl1aYvxyC4" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

El man del bajo

<iframe src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/205687310&amp;auto_play=false&amp;hide_related=false&amp;show_comments=true&amp;show_user=true&amp;show_reposts=false&amp;visual=true" width="560" height="315" frameborder="no" scrolling="no"></iframe>

Leonardo (Pasajeros)

<iframe src="https://www.youtube.com/embed/I6bcfjykKhY" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

[**Lugar:** Latino Power, calle 58 \#13- 88]{.eModal-3}

[**Hora:** 9 p.m. ]{.eModal-3}

[**Cover:** \$10.000 (Mitad de precio hasta las 11:00 pm)]{.eModal-3}
