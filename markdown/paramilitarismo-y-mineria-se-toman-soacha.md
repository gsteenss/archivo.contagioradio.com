Title: Paramilitarismo y minería se toman Soacha
Date: 2015-03-06 23:36
Author: CtgAdm
Category: DDHH, Nacional
Tags: Alcaldía de Bogotá, Altos de Cazucá, Minería en Soacha, Paramilitarismo en Soacha
Slug: paramilitarismo-y-mineria-se-toman-soacha
Status: published

###### Foto: [servewlove.wordpress.com]

<iframe src="http://www.ivoox.com/player_ek_4178565_2_1.html?data=lZakmpqaeY6ZmKiakp6Jd6KnlpKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRjMbdz8rfjazFrdWZpJiSo5bSaZO3jLfSxpCuudfZz87ZjcnJb7TjwsjVw5KJe6ShpNTb1sbLrdCfs8bRy9SPcYarpJKh&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Heiner Gaitán, Red Juvenil de Soacha] 

Según la circular roja emitida por la Alcaldía de Bogotá, el paramilitarismo tiene especial auge en la comuna 4 - Altos de Cazuca, Comuna 6 - Altos de Florida y la comuna 5 - San Mateo. Esta sería la primera entidad oficial en reconocer la existencia de paramilitares en esa zona colindante con Bogotá. Ya hace más de dos años, la misma alcaldía, como la defensoría del pueblo se han pronunciado en ese sentido, sin que hasta el momento las FFMM y de Policía reconozcan esta realidad que es inocultable para los habitantes del sector.

Heiner Gaitán, de la Red Juvenil de Soacha, afirma que la presencia paramilitar en el municipio no es nueva, lo único novedoso o diferente es que un actor institucional menciona el tema. En 2014 la Fundación Paz y Reconciliación hizo un mapeo de la presencia de los paramilitares. Además resalta que hay también un auge de los intereses empresariales en la zona.

En Soacha hay varios elementos que rodean el fenómeno del paramilitarismo. Por un lado el desplazamiento forzado que tiene a la zona de Soacha como receptora de familias de todo el país, cerca de 150 mil habitantes, entre los que se mezclan todos los actores de la guerra. Además al sector han llegado también los intereses empresariales que instalan allí sus oficinas, afirma Gaitán.

Por parte de las autoridades militares y de policía, Gaitán afirma que persiste la negación de la existencia del paramilitarismo pero paradójicamente, se ha establecido la construcción de un batallón en el sector, lo cual contrasta con esa misma negación. Si no hay paramilitarismo no se justifica la instalación de la base militar en altos de la florida, comuna 6. Situación que violaría normas internacionales puesto que en ese terreno hay población civil.

Como mecanismo de defensa los jóvenes de la Red, han iniciado la escuela de formación para defensores y defensoras de DDHH. En principio la escuela será itinerante y hará presencia en los sectores donde se señala está el paramilitarismo. El próximo 11 de Abril será el lanzamiento y se espera que esta alternativa logre convocar el respaldo de sectores políticos y sociales del país.
