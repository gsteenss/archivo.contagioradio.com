Title: ELN muestra voluntad para un Acuerdo Humanitario en el Catatumbo
Date: 2020-11-18 18:29
Author: AdminContagio
Category: Actualidad, Nacional
Tags: Acuerdo Humanitario para el Catatumbo, Catatumbo, ELN
Slug: eln-muestra-voluntad-para-un-acuerdo-humanitario-en-el-catatumbo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/11/ELN-acuerdo-humanitario-Catatumbo.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

El Ejército de Liberación Nacional -ELN- dio a conocer por medio de un comunicado su intención de **acogerse a la propuesta** elevada por comunidades y organizaciones para establecer un **Acuerdo Humanitario para el Catatumbo** que permita encontrar una salida negociada al conflicto que se libra entre los diversos actores armados que hacen presencia en ese territorio. (Lea también: [ELN está preparado para negociar un cese al fuego bilateral](https://archivo.contagioradio.com/eln-esta-preparado-para-negociar-un-cese-al-fuego-bilateral/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En su comunicado el ELN expresó que “*se reafirma en la voluntad de allanar caminos para buscar una salida política al conflicto político, social y armado*” y agregó que “*el futuro de la Nación no puede seguir siendo la Guerra*”. (Le puede interesar: [Es necesario negociar un cese bilateral: ELN](https://archivo.contagioradio.com/en-necesario-negociar-un-cese-bilateral-eln/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

No obstante, el grupo armado expresó la poca voluntad de paz que ha tenido Iván Duque desde que asumiera la Presidencia, señalando que “*mientras la región anhela la Paz, el Gobierno aumenta la militarización de los territorios, fortalece su aparato represivo y promueve la expansión del narcoparamilitartimo*”.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El comunicado del ELN se da como respuesta a la [carta abierta](https://www.vivamoshumanos.com/carta-abierta-a-partir-de-mesa-humanitaria-del-catatumbo/) elevada por varios congresistas y organizaciones sociales en el sentido de exigir un **Acuerdo para establecer unos mínimos humanitarios** como el cese del reclutamiento forzado de menores, la instalación de minas antipersona, la erradicación forzada de cultivos de uso ilícito y los ataques contra la infraestructura social, así como  la implementación del acuerdo final de paz

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Algunas organizaciones sociales como la Asociación Campesina del Catatumbo -[ASCAMCAT](https://twitter.com/AscamcatOficia)- y [Vivamos Humanos](https://twitter.com/VivamosHumanos/status/1328820142013358081), promotoras del Acuerdo Humanitario para el Catatumbo, saludaron el anuncio del ELN, en el que además se compromete a excluir a la población civil del conflicto.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/AscamcatOficia/status/1328874798819266562","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/AscamcatOficia/status/1328874798819266562

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

Por su parte, **las comunidades del Catatumbo siguen pidiendo al Gobierno Nacional, mostrar voluntad de paz para buscar la salida negociada al conflicto** con los grupos armados que operan en el territorio como el Ejército Popular de Liberación -EPL- entre otros; y para que reanude los diálogos de paz con el ELN, iniciados en el mandato de Juan Manuel Santos, los cuales suspendió el presidente Duque desde el pasado mes de enero de 2019. (Le puede interesar: [ELN propone cese bilateral y el Gobierno cierra la puerta a esa posibilidad](https://archivo.contagioradio.com/eln-propone-cese-bilateral-y-el-gobierno-cierra-la-puerta-a-esa-posibildad/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Sin embargo, la tendencia del Gobierno sigue siendo la salida militar, cerrando la posibilidad a un cese al fuego bilateral que ha sido propuesto por el ELN en al menos tres ocasiones, y arreciando en las operaciones de las Fuerzas Militares, aumentando así la confrontación armada en los territorios. (Lea también: [Con operativo contra alias “Uriel”, Gobierno cierra más la puerta a salida negociada con el ELN](https://archivo.contagioradio.com/con-operativo-contra-alias-uriel-gobierno-cierra-mas-la-puerta-a-salida-negociada-con-el-eln/))

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
