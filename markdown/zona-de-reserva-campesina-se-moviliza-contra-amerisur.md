Title: 600 personas se movilizan en Putumayo contra petrolera Amerisur
Date: 2016-02-24 15:56
Category: DDHH, Nacional
Tags: Amerisur, Putumayo, zonas de reserva campesina
Slug: zona-de-reserva-campesina-se-moviliza-contra-amerisur
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/02/Perla-Amzónica.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Contagio Radio 

<iframe src="http://co.ivoox.com/es/player_ek_10560820_2_1.html?data=kpWimJWcdpGhhpywj5WdaZS1k56ah5yncZOhhpywj5WRaZi3jpWah5yncZekkZDdx9fXs8%2FV1JDgx5DRs9fdzc7nw9OPqc%2Bfsdrh19LFvdCfxNTb1tfFb9HZ1dfczsrWpYyhhpywj6jTstXVyM7cjbfFqMrjjJKSmaiReA%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Sandra Lagos, Adispa] 

###### [24 Feb 2016] 

**600 personas de la Comunidad de la Zona de Reserva Campesina Perla Amazónica y veredas Aledañas se movilizan en el  sector Alea - la Rosa** manifestando y reiterando su rechazo la actividad petrolera de la empresa Amerisur en Putumayo que ha causado diferentes afectaciones sociales y ambientales.

La población de la ZRC asegura que la presencia de las petroleras representa una amenaza para la vida y la permanencia de las comunidades en sus territorios, **debido a que sus fuentes de agua están secas y “la poca agua que tienen está contaminada”**, dice Sandra Lagos, integrante de ADISPA, Asociación de Desarrollo Integral Sostenible Perla Amazónica, quien afirma que la empresa petrolera bota agua industrial a los caños con los que se abastece la comunidad, ocasionando que las personas deban caminar 8 kilómetros, para poder tomar agua de la única fuente del sector que  se ha contaminado.

Las comunidades se han visto mayormente afectadas desde que se inició la construcción del oleoducto (línea de transferencia) que conectaría y permitirá transportar el petróleo, lo que preocupa a los pobladores que pues el **Río Putumayo podría verse afectado por este tipo de intervenciones.  **

Además según denuncia la vocera de la Zona de Reserva Campesina, **la explotación de crudo se realiza en medio de un humedal que es el representa el 50% de la zona de reserva campesina.**

La comunidad espera que con la manifestación se suspendan todas las actividades que afecten el entorno en el que viven adultos, jóvenes y sobre todo ancianos y niños cuya salud se ha visto afectada por la contaminación que ha generado la actividad petrolera. Según Lagos, esa contaminación, ha causado que cinco personas de la región presenten cáncer y otras enfermedades como dolores de cabeza y sarpullidos en la piel.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u)  o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](https://archivo.contagioradio.com/). 
