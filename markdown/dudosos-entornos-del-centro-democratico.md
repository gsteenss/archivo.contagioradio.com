Title: Los dudosos entornos del Centro Democrático
Date: 2018-07-04 15:26
Category: Nacional, Política
Tags: Cambio Radical, Centro Democrático, Corte Suprema de Justicia, investigaciones, parapolítica
Slug: dudosos-entornos-del-centro-democratico
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/07/centro_democratico-1004x670.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Fundación Paz y Reconciliación] 

###### [4 Jul 2018] 

De 19 congresistas que salieron electos por el Centro Democrático, 5 provienen de entornos con historiales delictivos; si bien sus entornos no necesariamente los condenan si marcan un derrotero sobre el comportamiento de algunos integrantes de la bancada de gobierno que tendrá que legislar durante los próximos 4 años en temas fundamentales como el acuerdo de paz con las FARC, el asesinato de líderes sociales (y el desmonte de estructuras militares ligadas a economías ilegales) y la corrupción.

Teniendo a familiares acusados, investigados o condenados por violaciones a los derechos humanos y actos en detrimento de las arcas públicas, seguramente habrá una gran cantidad de impedimentos para votar proyectos de ley, o habrá un aplazamiento de los mismos, intentando encubrir a los suyos, los cinco congresistas son:

### María del Rosario Guerra de La Espriella 

La senadora del Centro Democrático es hija de José Guerra Tulena, quién fue congresista y gobernador de Sucre. Es además sobrina de Julio César Guerra Tulena, quien fue representante a la cámara, senador y gobernador de Sucre; actualmente está siendo investigado por fraude durante su gobernación relacionado con el cartel de enfermos mentales.

María del Rosario tiene 3 hermanos: Manira Guerra de la Espriella, quién fue secretaria de hacienda de Santa Marta; José ‘Joselito’, quién fue condenado por enriquecimiento ilícito, estafa y falsedad agravada por cuenta del Proceso 8.000; y Antonio, quien salió electo senador de Cambio Radical para el periodo 2018-2022, contra él cursa una investigación en la sala penal de la Corte Constitucional por corrupción en el caso Odebrecht.

En su entorno también está su primo Miguel Alfonso de la Espriella ‘Miguelito’, quién fue condenado a 43 meses de prisión por ser firmante del Pacto de Ralito, en el que más de 50 políticos se propusieron re-fundar el Estado Colombiano en conjunto con Paramilitares.

### Santiago Valencia González 

Su padre es Fabio Valencia Cossio, quien fue ministro de Interior y actualmente es investigado por la Sala de Justicia y Paz del Tribunal Superior de Bogotá por presuntos nexos con paramilitares. Su tío, Guillermo León Valencia Cossio fue condenado por la Corte Suprema de Justicia por proteger de investigaciones al narcotraficante Juan Carlos Abadía, alias ‘Chupeta’, todo mientras fue director de Fiscalías en Medellín.

### Ruby Helena Chagüi Spath 

Esta comunicadora de profesión tiene familia con tradición política también: es nieta de Alfonso Spath Spath, quien fue alcalde de Cereté, e Hija de Elber Chagüi, actual alcalde de ese municipio en Córdoba.

Recientemente, Jesús Henao, quién es testigo del caso sobre el asesinato de Jairo Zappa (ex-director de regalías de Córdoba), señalo a Elber Chagüi como su autor intelectual, así como del caso de corrupción contra el magisterio de Córdoba.

### Honorio Miguel Enríquez Pinedo 

Pinedo, senador del Centro Democrático por segunda vez, es primo del electo representante a la cámara por Cambio Radical, José Luis Pinedo Campo. Ambos pertenecen a la casa política Pinedo en Magdalena, de la cual también hace parte Miguel Pinedo Vidal, padre de Luis Pinedo y tío de Honorio Enríquez, quien fue condenado por parapolítica por la Corte Suprema de Justicia.

### Ciro Alejandro Ramírez Cortés 

Hijo de Ciro Ramírez Pinzón, un poderoso cacique político de Boyacá que comenzó su carrera en 1986. Tras varios periodos en el ámbito de la política fue creciendo en apoyos y aumentando su caudal electoral, lo que lo llevó a lograr una curul en el senado en 2006. Sin embargo, en febrero de ese año fue capturado por sus vínculos con el Bloque Central Bolívar y sentenciado por la Corte Suprema de Justicia a purgar 90 meses de prisión por parapolítica.

### Los salientes senadores del Centro Democrático 

Entre los senadores que también tienen entornos con acusaciones y no resultaron electos están: Thania Vega de Plazas, quien es esposa de Alfonso Plazas Vega, coronel condenado por la desaparición de dos personas luego de la toma del palacio de justicia y posteriormente absuelto por la Corte Suprema de Justicia; y Alfredo Ramos Maya, de quien recientemente habló la opinión pública por cuenta del insulto dicho en contra de la senadora del partido verde, Claudia López, además es hijo de Luis Alfredo Ramos,  quien esta siendo investigado por la Corte Suprema de Justicia por presuntos nexos con paramilitares. (Le puede interesar: ["Ejército deberá responder por torturas a 11 personas durante retoma del Palacio de Justicia"](https://archivo.contagioradio.com/torturas_palacio_justicia_retoma/))

### Las acusaciones en carne propia 

También hay congresistas que estarán en la próxima legislatura e inician sus periodos con investigaciones a nombre propio, entre ellos se encuentran: Maria Fernanda Cabal, acusada por el Fiscal Nestor Humberto Martínez por irregularidades en su campaña para Senado; Margarita Restrepo, igualmente acusada por el fiscal por ofrecer beneficios a sus votantes durante su campaña a la Cámara de Representantes por Bogotá; y el prontuario de investigaciones que existe contra Álvaro Uribe Vélez. (Le puede interesar:["El historial de investigaciones contra Álvaro Uribe Vélez"](https://archivo.contagioradio.com/el-historial-de-investigaciones-contra-alvaro-uribe-velez/))

###### [Reciba toda la información de Contagio Radio en] [su correo](http://bit.ly/1nvAO4u) [o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por] [Contagio Radio](http://bit.ly/1ICYhVU). 
