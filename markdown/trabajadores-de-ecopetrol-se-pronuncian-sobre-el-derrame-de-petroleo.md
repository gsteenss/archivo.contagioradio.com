Title: Trabajadores de Ecopetrol se pronuncian sobre el derrame de petróleo
Date: 2015-06-12 12:12
Category: Nacional, Paz
Tags: Ecopetrol, FARC, paz, petroleo, Putumayo, union sindical obrera, USO
Slug: trabajadores-de-ecopetrol-se-pronuncian-sobre-el-derrame-de-petroleo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/06/petroleo1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Contagio Radio 

###### [12 jun 2015] 

Trabajadores de la USO emitieron un comunicado el viernes 12 de junio en el cual rechazan el derramamiento de petróleo por las afectaciones que genera sobre la población civil y el ambiente, mientras aseguran que como consecuencia del conflicto armado colombiano "muchas instalaciones petroleras y comunidades parecen trincheras de guerra, en las que los trabajadores deben laborar en medio de soldados, fusiles, granadas y otro tipo de armamento, violándose con esto el Derecho Internacional Humanitario."

Para la USO, las acciones de la guerrilla de las FARC en contra de la infraestructura de oleoductos es una respuesta ante la ofensiva de las Fuerzas Armadas, que mediante bombardeos en los departamentos del Chocó, Cauca, Putumayo y Norte de Santander, acabaron con la vida de decenas de guerrilleros.

"La USO lamenta profundamente que la ambigüedad del gobierno entre la guerra y la paz se haga en función de calmar los ánimos de la extrema derecha y en cambio, se sacrifique la posibilidad de una tregua bilateral", reza el comunicado.

Finalmente, la Unión Sindical Obrera insiste en que "el conflicto armado se debe resolver mediante el diálogo y la solución política negociada", y llama a las partes a cesar las hostilidades para ponerle fin al conflicto e iniciar el proceso de transformaciones políticas, económicas y sociales que nos conduzca a una paz estable y duradera.

A continuación compartimos el comunicado completo:

<div>

<div>

#### SACAR EL PETRÓLEO DE LA GUERRA Y PACTAR UNA TREGUA BILATERAL 

#### Hace dos semanas la opinión pública conoció de la gigantesca ofensiva de las Fuerzas Armadas que mediante bombardeos sobre zonas del Choco, Cauca, Putumayo y el Catatumbo; muchas de ellas zonas pobladas. Dicho accionar buscaba golpear columnas insurgentes. La respuesta no se hizo esperar y estos últimos días la prensa informa de una oleada de sabotajes y atentados de la guerrilla a la infraestructura petrolera, esta vez en el departamento del Tibu, Tumaco y Putumayo, que deja no solo cuantiosas pérdidas económicas sino lamentables afectaciones ambientales. 

#### La Unión Sindical Obrera de la Industria del Petróleo –USO- Rechaza estos hechos que además de afectar a la población civil y el medio ambiente, representan un escalamiento del conflicto armado que distancia las posibilidades de una Paz duradera, que fue el mandato del pueblo Colombiano al actual gobierno. 

#### La militarización de las instalaciones y campos petroleros, la reactivación del paramilitarismo y el escalamiento del conflicto, convierten a los trabajadores petroleros y la población de esas regiones en objetivo militar. Muchas instalaciones petroleras y comunidades parecen trincheras de guerra, en las que los trabajadores deben laborar en medio de soldados, fusiles, granadas y otro tipo de armamento, violándose con esto el Derecho Internacional Humanitario. 

#### La USO lamenta profundamente que la ambigüedad del gobierno entre la guerra y la paz se haga en función de calmar los ánimos de la extrema derecha y en cambio, se sacrifique la posibilidad de una tregua bilateral, con lo cual se aliviaría la situación de incertidumbre que viven centenares de familias colombianas y trabajadores que viven el riesgo en medio de las hostilidades. Negociar la paz en medio del conflicto está resultando una decisión absurda, costosa e inconveniente para el país. No compartimos el daño a las instalaciones de nuestra empresa Ecopetrol y mucho menos el daño al medio ambiente y los medios de subsistencia de las comunidades petroleras; como tampoco compartimos los exagerados bombardeos y hostigamientos de las fuerzas militares donde exponen a trabajadores y comunidades en medio del fuego. 

#### Insistimos en que el conflicto armado se debe resolver mediante el diálogo y la solución política negociada. En este sentido hemos Venido promoviendo la realización de la Segunda Asamblea por la Paz, que esperamos sea un espacio de construcción y confluencia de propuestas para Sacar el petróleo de la guerra y convertir este sector en un catalizador del proceso de paz y el desarrollo del país. 

#### Los colombianos y colombianas exigimos a las partes cesar las hostilidades para ponerle fin al conflicto e iniciar el proceso de transformaciones políticas, económicas y sociales que nos conduzca a una paz estable y duradera. 

#### Unión Sindical Obrera de la Industria del Petróleo –USO  
Junio 2015 

</div>

</div>
