Title: Se acaba el tiempo del Fast Track y toda la presión va al congreso
Date: 2017-10-31 12:31
Category: Nacional, Paz
Tags: acuerdo de paz, Reforma Política
Slug: se-acaba-el-tiempo-del-fast-track-y-toda-la-presion-va-al-congreso
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/03/ausentismo-camara.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Noticias Económicas] 

###### [31 Oct 2017] 

Esta semana será crucial para la Reforma Política que estará en plenaria de Cámara de Representantes, **hoy se debería aprobar el artículo de coaliciones y quedarían 3 artículos pendientes: el de financiación estatal y dos referidos a la configuración de movimientos** y partidos políticos, sin embargo, representantes como Ángela María Robledo hicieron un llamado a evitar la dilatación de este trámite en el Congreso.

Para la congresista del partido Alianza Verde, las posibilidades de que el proyecto sea aprobado son altas, teniendo en cuenta la presión que se ha ejercido durante los últimos días sobre el presidente de la Cámara y los representantes “si bien aquí hay una actuación arbitraria de Rodrigo Lara, **con apariencia de democracia, esperemos que los compañeros de la Cámara y todos los partidos respondan** y hoy podamos sacar adelante la reforma”.

Adicionalmente varias organizaciones sociales entre las que se encuentra el Movimiento de Víctimas de Crímenes de Estado, están impulsando una campaña de veeduría en la que **exigen a los congresistas asistir a los debates y votar favorablemente a las normas** que buscan garantizas los derechos de las víctimas como el \#NoFalteAlDebate.

### **Las coaliciones son una espada de doble filo** 

De acuerdo con Robledo esta reforma ha sufrido modificaciones, una de ellas tiene que ver con la conformación de las coaliciones para elecciones **“el espíritu de las coaliciones se había hecho para fortalecer la democracia** y preservar la participación de partidos minoritarios en el congreso, pero por una maniobra del representante Chacón, del partido Liberal se cayó”

En ese sentido, la representante afirmó que los partidos minoritarios quedaron en el peor escenario, sin embargo, los sectores minoritarios y de la izquierda democrática deberán afrontar, en lo regional y lo nacional, coaliciones de distinto orden, razón por la cual la tarea **es conformar una lista única, diversa y plural que logre recoger las diferentes iniciativas que hay.**

Otra de las propuestas que no pasó los debates fue el Tribunal de Aforados, que, según la Representante, **ya no se podrá revivir**, pero si cabría la posibilidad de buscar una entidad que cumpla un papel similar. (Le puede interesar: ["Proceso de paz afronta serias dificultades: Misión Internacional de verificación"](https://archivo.contagioradio.com/mision-internacional-verificacion-implementacion/))

Sobre los puntos que están quedando por fuera del Acuerdo de Paz y que ya no entrarán vía Fast Track, Robledo señaló que desde la presidencia se tienen herramientas que se pueden **activar para proteger el Acuerdo como lo sería declarar el estado de conmoción interior**.

Con este panorama la congresista hizo un llamado a la ciudadanía para que ejerza su derecho al voto consiente, organice veedurías ciudadanas y esté al tanto de la política para generar transformaciones en el congreso 2018. (Le puede interesar: ["Estado de conmoción podría salvar Acuerdos de Paz: Iván Cepeda"](https://archivo.contagioradio.com/48477/))

<iframe id="audio_21797966" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_21797966_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)
