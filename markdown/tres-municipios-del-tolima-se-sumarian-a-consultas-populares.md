Title: Tres municipios del Tolima se sumarían a consultas populares
Date: 2017-07-11 15:14
Category: Ambiente, Nacional
Tags: Ambiente, consultas populares, encuentro ambiental, minas, Tolima
Slug: tres-municipios-del-tolima-se-sumarian-a-consultas-populares
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/07/consulta-ibague.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: La2Orillas] 

###### [11 Jul 2017] 

Ambeima, Ortega y Ataco,  municipios del Tolima debaten en el marco del Encuentro Ambiental sobre los Derechos, el Territorio y la Gobernanza del Agua que se realiza en Chaparral  la **posibilidad de realizar consultas populares** para impedir la construcción de hidroeléctricas, la extracción de hidrocarburos y la minería de oro en sus territorios.

Para Elio Campos, integrante del Comité Dinamizador Ambiental Campesino del Sur Oriente del Tolima, “en este espacio se va a dar una ilustración a las comunidades campesinas y a algunas organizaciones sociales sobre la importancia de estar informados con respecto a los proyectos y **las amenazas contra el territorio y las mismas comunidades por los procesos de la minería** y la extracción de hidrocarburos”. (Le puede interesar: ["La propuesta de Pijao como alternativa a la minería tras consulta popular"](https://archivo.contagioradio.com/la-propuesta-de-pijao-como-alternativa-a-la-mineria-tras-consulta-popular/))

Ante la postura del Ministerio de Minas, que manifestó que existe un nuevo momento en la relaciones con las comunidades, Campos enfatizó que, “en ocasiones el Gobierno Nacional **ha tratado de desvirtuar las decisiones que han tomado las comunidades** para quedar bien con las multinacionales que buscan lograr sus objetivos económicos”. Por esto puso de ejemplo la llegada de la empresa de energía de los Andes a Ambeima en donde no socializaron los proyectos con las comunidades y no les brindaron la información necesaria para el desarrollo de proyectos mineros.

Igualmente afirmó que **“cuando la comunidad sale a defender su territorio, vienen las amenazas, los desplazamientos y las intimidaciones contra los líderes sociales”.** Esto en virtud de que “cuando las decisiones de la comunidad son adversas a los intereses del gobierno y de las grandes empresas mineras, ellos intimidan a la sociedad diciendo que van a poner nuevos impuestos”. (Le puede interesar: ["Producción agrícola se salva con la victoria del No en consulta popular de Arbeláez"](https://archivo.contagioradio.com/43359/))

**La gobernanza del agua como decisiones colectivas sobre su uso**

En el Encuentro Ambiental, los participantes han dejado claro que los **proyectos de la mega minería llevan a la disminución y contaminación del agua**. Se han centrado en debatir los aspectos de la gobernanza del agua teniendo en cuenta los usos que se le ha dado y el cambio que han experimentado los territorios por afectaciones en los recursos hídricos.

Es por esto que, para Elio Campos, “es importante socializar las experiencias que tenemos sobre los **daños que se han generado en los afluentes de los ríos Ambeima y Saldaña**, que surten de agua a 102 millones de habitantes, por ejemplo”. De igual forma, manifestó que se van a discutir los usos del agua para la agricultura, las industrias, los riegos y el uso de las comunidades a través de decisiones colectivas. (Le puede interesar: ["Sibaté prepara consulta popular contra la minería"](https://archivo.contagioradio.com/sibate_consulta_popular_mineria/))

Cabe resaltar que los asistentes están discutiendo el **impacto de proyectos como las hidroeléctricas a filo de agua en la cuenca del Alto Saldaña**, la reanudación de campos petroleros en Ortega, la exploración de oro en Ataco y los temas relacionados con la tierra en el desarrollo de los Planes de Desarrollo con Enfoque Territorial PDET, que están dispuestos en los acuerdos de paz de la Habana.

<iframe id="audio_19748427" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_19748427_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU).
