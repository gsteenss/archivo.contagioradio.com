Title: Educación
Date: 2017-03-14 11:34
Author: AdminContagio
Slug: educacion-2
Status: published

EDUCACIÓN
---------

 

###### [Con panfletos y carteles amenazan a estudiantes de la U. de Antioquia](https://archivo.contagioradio.com/con-panfletos-y-volantes-amenazan-a-estudiantes-de-la-u-de-antioquia/)

[<time title="2019-01-23T17:10:27+00:00" datetime="2019-01-23T17:10:27+00:00">enero 23, 2019</time>](https://archivo.contagioradio.com/2019/01/23/)Los mensajes aparecieron este miércoles en inmediaciones de la Universidad firmados por un grupo que se hace llamar Brigada de seguridad Anticomunista[Leer más](https://archivo.contagioradio.com/con-panfletos-y-volantes-amenazan-a-estudiantes-de-la-u-de-antioquia/)

###### [Suspenden diplomado financiado por Anglo Gold Ashanti en la Universidad de Antioquia](https://archivo.contagioradio.com/suspenden-diplomado-anglo-gold-ashanti-la-universidad-antioquia/)

[<time title="2019-01-23T17:05:02+00:00" datetime="2019-01-23T17:05:02+00:00">enero 23, 2019</time>](https://archivo.contagioradio.com/2019/01/23/)La multinacional habría financiado el curso sobre sostenibilidad y liderazgo en la Universidad de Antioquia para brindar legitimidad a un proyecto minero[Leer más](https://archivo.contagioradio.com/suspenden-diplomado-anglo-gold-ashanti-la-universidad-antioquia/)

###### [1.000 km por la educación: Llegan los Caminantes de la Sierra a Bogotá](https://archivo.contagioradio.com/caminantes-de-la-sierra/)

[<time title="2019-01-23T15:42:18+00:00" datetime="2019-01-23T15:42:18+00:00">enero 23, 2019</time>](https://archivo.contagioradio.com/2019/01/23/)Luego de 48 días y 1.198 km de recorrido, llegan los Caminantes de la Sierra a la Capital; una braza para avivar el fuego de la lucha estudiantil[Leer más](https://archivo.contagioradio.com/caminantes-de-la-sierra/)

###### [Estudiantes vuelven a las calles por el desmonte del ESMAD](https://archivo.contagioradio.com/desmonte-del-esmad/)

[<time title="2019-01-21T13:04:16+00:00" datetime="2019-01-21T13:04:16+00:00">enero 21, 2019</time>](https://archivo.contagioradio.com/2019/01/21/)Para respetar el derecho a la vida de los manifestantes, jóvenes exigen que se desmonte el ESMAD y se brinden garantías para el derecho a la protesta[Leer más](https://archivo.contagioradio.com/desmonte-del-esmad/)
