Title: Alter Eddie, más que caricaturista me considero un pedagogo
Date: 2019-04-18 08:00
Author: CtgAdm
Category: Cultura, DDHH
Tags: Alter Eddie, Historia del Conflicto Armado, paz, redes sociales
Slug: alter-eddie-mas-que-caricaturista-me-considero-un-pedagogo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/MG_5185-e1555541671228.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/MG_5238-e1555541630651.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/MG_5188.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/MG_5202.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

Llegar a la casa del caricaturista y animador ‘Alter Eddie’ es como revivir en cierto modo aquella época en la que los amigos del barrio aguardaban a que uno más de su grupo saliera de su casa para ir a jugar;   plantados frente a la puerta presionamos un botón en el citófono y aguardamos con paciencia en la normalidad de un vecindario del occidente de Bogotá a que alguien responda.

La puerta se abre magnéticamente y caminamos a través de un pasillo hasta pararnos frente a otra puerta en particular, golpeamos y esta se abre casi de inmediato; una señora ya entrada en años se asoma mientras evalúa a los dos visitantes quienes pareciera que en cualquier momento van a preguntar “buenos días, ¿puede Humberto salir a jugar?”.

La mujer, quien como más tarde confirmamos se trata de la mamá del artista,  nos invita a pasar, la seguimos de forma discreta, cruzamos la sala y subimos unas escaleras para luego ingresar a una habitación donde Edgar Humberto Vélez, más conocido como ‘Alter Eddie’, el creador de la iniciativa ‘Se lo explico con plastilina’  aguarda sentado frente a su computador.

\[caption id="attachment\_65084" align="alignnone" width="1000"\]![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/MG_5238-e1555541630651.jpg){.wp-image-65084 .size-full width="1000" height="667"} Mientras hablábamos con Alter Eddie hacia este personaje /Contagio Radio\[/caption\]

Se trata de una estancia colorida en la que cuelgan patinetas de las paredes con diseños creados por el animador; desde estantes, gavetas y diversos muebles nos saludan toda clase de réplicas en plastilina de  Pepe Mujica, Betty la Fea, Jaime Garzón, Donald Trump y todo personaje de la coyuntura nacional que pueda acudir a su imaginación, hacen parte de esta habitación, emulando a la colección de juguetes de un privilegiado niño.

> [empecé desde muy pequeño trabajando con plastilina, algo que me gustaba mucho era esa mezcla de volumen y color a la vez...]{.s1}

[Alguna vez, fue precisamente ese el sueño del pequeño Edgar Humberto, igual que  muchos otros niños deseaba poder dar vida y movimiento a sus figuras, un sueño que pudo hacer realidad al descubrir la plastilina y que hoy años más tarde con la animación de su lado hizo realidad, “empecé desde muy pequeño trabajando con plastilina, algo que me gustaba mucho era esa mezcla de volumen y color a la vez”, recuerda.]{.s1}

Mientras la mayoría pensaría en modelar figuras de acción y personajes de ciencia ficción, Edgar recuerda que sentía una predilección por el modelado de campesinos, personajes de nuestra realidad colombiana y a que su forma son los verdaderos héroes de esta tierra, “siempre me ha gustado mucho, me he identificado con las víctimas y con aquellos que siento pueden estar en una posición de desventaja”, asegura Vélez.

\[caption id="attachment\_65081" align="alignnone" width="1024"\]![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/MG_5185-1024x683.jpg){.size-large .wp-image-65081 width="1024" height="683"} Familia de campesinos/Alter Eddie. Foto: Contagio Radio\[/caption\]

> “No es una obligación, dentro del arte hay personas que se inspiran en lo personal, otras en el amor, desde pequeño tuve en la cabeza esas ideas que tenían que ver con la justicia”

Y ‘Alter Eddie’ no miente, dentro de los cientos de personajes que están escondidos entre los cajones y cajones de esa habitación, la mayoría son campesinos que de alguna forma y junto a la educación que recibió en su casa por parte de su mamá, labraron una especie de compromiso social en la forma de trabajar de este bogotano.

“No es una obligación, dentro del arte hay personas que se inspiran en lo personal, otras en el amor, desde pequeño tuve en la cabeza esas ideas que tenían que ver con la justicia”,  un valor que ha buscado plasmar en sus creaciones a lo largo de su carrera y sobretodo como una forma de cuestionar a las personas y de no imponer las ideas sino de acudir al diálogo y a la pedagogía.

Es precisamente por ello que viene desarrollando un proyecto de memoria enfocado en narrar la historia del conflicto armado a través de la plastilina, “tenemos que hablar de historia, tde política, de religión y esto hay que construirlo entre todos, no existen verdades absolutas”  afirma con fuerza quien confiesa que para este proyecto en particular ha creado más de 300 personajes, “Imagínese, estamos contando la historia del conflicto” añade.

https://www.youtube.com/watch?v=3tFBcfmDTGg

> “la plastilina es un material que le llega a todo el mundo, esa suavidad y fllexibilidad no solo se transmite a nivel del material sino también cuando se mandan mensajes”.

A la habitación entra de forma cautelosa una gata blanca que al igual que la mamá de Humberto, inspecciona a los dos extraños que han llegado a su casa, para luego avanzar indiferente por el resto de la habitación, ‘Alter Eddie’ la saluda y continúa su reflexión, “la plastilina es un material que le llega a todo el mundo, esa suavidad y fllexibilidad no solo se transmite a nivel del material sino también cuando se mandan mensajes”.

Edgar, a quien debido a sus creaciones acuden toda clase de personas, desde maestros y niños hasta abuelos y seguidores de la izquierda y derecha de esta país asegura que solo cuando se comprende que el mundo no es solo de blancos y negro y que existe toda una serie de matices es cuando la gente se da cuenta que el diálogo es la salida a la polarización y agresividad que ha vivido el país a lo largo de su historia.

Algo irónico, piensa Edgar Humberto quien recuerda que pasó seis años en Estados Unidos y que fue en ese lugar donde descubrió su colombianidad, “mientras trabajaba en su agencia de publicidad en el exterior, campesinos del Huila utilizaban imágenes de sus creaciones en manifestaciones” algo que para el creador significó una fusión completa de su trabajo con la realidad del país.

\[caption id="attachment\_65085" align="alignnone" width="1024"\]![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/MG_5188-1024x683.jpg){.size-large .wp-image-65085 width="1024" height="683"} Simón Bolivar y Colombia, personajes de las historias de Alter Eddie/ Contagio Radio\[/caption\]

Mientras conversa, Edgar abre una de las gavetas que están a su alcance y saca algunas barras de plastilina de diversos colores y comienza, como un artesano a modelar la que será su próxima creación, las que, a pesar de conservar la perfección en sus rasgos y características, asegura no guarda pues de hacerlo no le cabrían en su casa.

“Yo trabajo muy rápido”, argumenta mientras amasa un par de bolitas de plastilina blanca que serán los ojos del personaje que está creando, “tengo un gran banco de imágenes y otra ventaja y es que no trabajo para ningún medio y bueno, la práctica y la técnica también ayudan”, afirma confiado.

“El placer de esto está en construir” afirma mientras finaliza la creación de un pequeño personaje que casi con vida propia permanece sobre la mesa, mirando sus ojos de plástico a la nada, ‘Alter Eddie’  como un dios que admira su obra, lo revisa por última vez y pasa su mirada a través de la plastilina que ha dado forma a los campesinos, víctimas, políticos, paramilitares, guerrilleros, ciudadanos y soldados que han hecho parte de la convulsa historia de este país y que a través de su plastilina buscan narrar una cotidianidad y sobre todo dejar una enseñanza,  “me considero más un pedagogo que un caricaturista”, declara.

 
