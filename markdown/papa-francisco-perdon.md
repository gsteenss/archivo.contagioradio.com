Title: Francisco pidió perdón por escándalos de la iglesia
Date: 2017-04-16 08:58
Category: El mundo, Otra Mirada
Tags: iglesia católica, Papa Francisco, Perdon
Slug: papa-francisco-perdon
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/04/semana-santa-2178162w640.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### Foto: Reuters 

##### 16 Abr 2017 

Durante los tradicionales actos religiosos del viernes Santo, **el Papa Francisco pidió perdón ante Dios por los diferentes escándalos en los que se han visto inmersos integrantes de la iglesia Católica**. El Sumo Pontífice reflexionó además sobre la **vergonzosa indolencia de la humanidad**, habituada a las escenas de terror que se vienen presentando en todo el mundo.

Cerca de **20 mil personas escucharon en el Coliseo romano las reflexiones críticas del Jerarca**, mientras encabezaba por quinta ocasión el viacrucis católico, celebración que conmemora los últimos momentos en la vida de Jesús de Nazaret; actos que en esta oportunidad se desarrollaron bajo fuertes dispositivos de seguridad.

Francisco lamentó “las veces que nosotros, obispos, sacerdotes, consagrados y consagradas, **hemos escandalizado y herido tu cuerpo y hemos olvidado nuestro primer amor**, nuestro primer entusiasmo y nuestra total disponibilidad” recordando los diferentes episodios en que los prelados han incurrido en faltas contra su fe y la ley.

Durante el recorrido de 14 estaciones, que lleva la cruz desde el interior del Coliseo hasta la colina de Palatino,  el Papa se refirió al "**silencio ante las injusticias**" y denunció “las manos perezosas en el dar, pero ávidas a la hora de arrebatar y conquistar” o los “pies veloces en la vía del mal y paralizados en la del bien”. Le puede interesar: [Católicos pidieron perdón por crímenes de la iglesia](https://archivo.contagioradio.com/catolicos-pidieron-perdon-por-crimenes-de-la-iglesia/).

El jueves santo en una entrevista a la prensa, Francisco abogó por la paz y por que se "**detenga a los señores de la guerra**" refiriéndose a quienes comercian con armas y se benefician con el desarrollo de los conflictos bélicos y con "la sangre de hombres y mujeres inocentes". Ese mismo día, visitó la cárcel de Paliano, en Italia, donde **lavó los pies a doce de los detenidos**, animando a los reclusos a que ayuden a sus compañeros de la cárcel si necesitan algún tipo de asistencia.

Las reflexiones del Santo Padre durante la Semana Mayor, apuntaron a la **búsqueda de solidaridad en los corazones duros de la humanidad**, invocando a la cruz para transformarlos en "corazones de carne" que sean capaces de soñar, perdonar y amar. Una petición que ayude a vencer el egoísmo y "romper las cadenas" que nos mantienen en una "ceguera voluntaria y en la banalidad de nuestros cálculos mundanos"
