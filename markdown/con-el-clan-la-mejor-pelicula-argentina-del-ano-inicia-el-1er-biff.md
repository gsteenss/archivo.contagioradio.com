Title: Con "El Clan" la mejor película Argentina de 2015 inicia el BIFF
Date: 2015-10-08 14:02
Author: AdminContagio
Category: 24 Cuadros
Tags: Cinemateca Distrital Octubre, El Clan película Argentina, Pablo Trapero en Bogotá, Programación BIFF 2015
Slug: con-el-clan-la-mejor-pelicula-argentina-del-ano-inicia-el-1er-biff
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/10/55d703df36f5f.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Fotograma de "El Clan" 

###### [08 Oct 2015] 

Con la proyección de más de 50 películas provenientes de diferentes rincones del planeta, arranca la primera edición del "**Bogotá International Film Festival, BIFF**", un evento que, según sus organizadores, promete estar a la altura de los Festivales de cine más importantes del mundo.

Del 8 al 16 de octubre, 4 salas de Cine Colombia y la Cinemateca Distrital de Bogotá, prestarán sus pantallas a las producciones realizadas en países de Africa, Asia, Europa y América, de las cuales 20 tendrán su estreno en latinoamerica y 2 su premier mundial.

La película ‘**El Clan**’, del director Pablo Trapero, escogida para representar a Argentina en los premios Oscar y Goya, será la encargada de dar apertura al evento, seguida por una muestra dividida en las secciones **"Francotiradores", "Crisis de Amor", "Realidades", "Miradas Paralelas", "Expreso de Medianoche", "Espíritu Joven y Niños".**

<iframe src="https://www.youtube.com/embed/n7kpI79cPBk" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

##### Trailer de "El Clan" 

El cine nacional tiene su propio espacio en la sección "**Colombia Viva**", donde se destacan los estrenos mundiales de ‘Violencia’, de Jorge Forero, y ‘Sabogal’ de Juan José Lozano. Otras producciones como ‘Suave el aliento’, de Augusto Sandino, ‘Malos Días’ de Andrés Beltrán, ‘Carta a los marcianos’ de Agustín Godoy y el documental ‘Aislados’, de Marcela Lizcano, representan el aporte local al evento.

Durante BIFF, se realizará una retrospectiva al trabajo del director uruguayo Rodrigo Plá, con una muestra compuesta por sus películas ‘La Zona’, ‘El Desierto’, ‘La Demora’, ‘El monstruo de mil cabezas’ y los cortos ‘Novia Mía’ y ‘El Ojo en la Nuca’.

Cerca de 15 invitados internacionales, entre directores, productores, editores y guionistas, aportaran con sus conocimientos y experiencia, acercandose a una visualización y comprensión más completa de cada una de las producciones que hacen parte de la programación de exhibicion.

Adicionalmente, del 11 hasta el 15 de octubre se adelantará la programación académica, totalmente gratuita, compuesta por *Conversaciones arco iris, Disección de un cineasta, La fuerza del resentimiento, Con la paz en la puerta, Una crisis de fe, Tomar riesgos para hacer una película y Actuar para la animación y animar para la actuación*.

[Programacion Biff 2015](https://es.scribd.com/doc/284091955/Programacion-Biff-2015 "View Programacion Biff 2015 on Scribd")

<iframe id="doc_97320" class="scribd_iframe_embed" src="https://www.scribd.com/embeds/284091955/content?start_page=1&amp;view_mode=scroll&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="undefined"></iframe>

La curaduría del Festival, estuvo a cargo de Rebeca Conget, Javier Martin, Raymond Pathanavirangoon y Andrés Bayona, reconocidos por hacer parte de otros eventos similares a nivel internacional, seleccionando títulos "arriesgados" que reflejan las tendencias fílmicas mundiales.

Para el equipo organizador, las películas que se verán en esta primera edición del BIFF son "en esencia modernas, pensadas para un público al que le gusta descubrir y que hoy piden nuevas alternativas audiovisuales, que los reten a conocer más sobre cine".
