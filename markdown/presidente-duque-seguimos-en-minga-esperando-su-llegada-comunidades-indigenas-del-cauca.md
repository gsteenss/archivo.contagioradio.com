Title: Presidente Duque seguimos esperando su llegada: Minga del Cauca
Date: 2019-03-14 14:59
Author: ContagioRadio
Category: Comunidad, Nacional
Tags: Acuerdos con comunidades indígenas, Minga Indígena
Slug: presidente-duque-seguimos-en-minga-esperando-su-llegada-comunidades-indigenas-del-cauca
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/03/D1eGwlrUwAAcjVp.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/03/D01Ij0FWsAIIqf7.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: CRIC 

###### 14 Mar 2019 

Desde el pasado 10 de marzo, líderes y comunidades indígenas del Cauca se concentran  en la vía Panamericana en ejercicio de Minga, exigiendo la presencia del **presidente Iván Duque** para restablecer el diálogo y encontrar soluciones a las exigencias planteadas en particular para el Plan Nacional de Desarrollo. El mandatario en su lugar envió a la ministra del Interior Nancy Patricia Gutiérrez con quien no se llegó a ningún acuerdo.

> "el ataque frontal que se está haciendo a la JEP desde el Gobierno para dejarla fuera de circulación"

Según explica el **senador Feliciano Valencia**, la Minga quiere dejar una posición muy clara manifestándose en contra de la activación del fracking en el territorio nacional, el incumplimiento de los acuerdos de paz, "el ataque frontal que se está haciendo a la JEP desde el Gobierno para dejarla fuera de circulación" y que se respete la vida de líderes y lideresas sociales así como los derechos de los pueblos indígenas. [(Lea también La Minga indígena que reclama a Duque cumplir los acuerdos)](https://archivo.contagioradio.com/minga-indigena-duque/)

De igual forma exigen como comunidad indígena que se especifiquen dentro del **Plan Nacional de Desarrollo (PND)** las garantías presupuestales que las regiones necesitan para solucionar sus problemas, particularmente en el Cauca, donde están evaluando todos los compromisos pactados desde 1966 y que no se han cumplido, "si bien es cierto que el Gobierno anunció que existen 10 billones de pesos para pueblos indígenas, esa incorporación no está expresada en las líneas del PND".

El senador también se refirió a las alertas tempranas que han realizado ante la represión de la Fuerza Pública, denunciando que hasta el momento han resultado heridas 14 personas, las cuales están siendo atendidas en su mayoría en las inmediaciones de la vía Panamericana pues el **ESMAD** no ha permitido el ingreso de los servicios médicos para su evacuación.

<iframe id="audio_33381122" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_33381122_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
