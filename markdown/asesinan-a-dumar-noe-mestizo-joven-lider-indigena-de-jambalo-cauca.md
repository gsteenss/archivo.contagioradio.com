Title: Asesinan a Dumar Noe Mestizo, joven líder indígena de Jambaló, Cauca
Date: 2019-10-05 17:56
Author: CtgAdm
Category: Líderes sociales, Nacional
Tags: Cauca, Líder Indígena, nariño, Toribío
Slug: asesinan-a-dumar-noe-mestizo-joven-lider-indigena-de-jambalo-cauca
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/Fotos-editadas.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

**Este viernes en horas de la noche el Consejo Regional Indígena del Cauca (CRIC) denunció el asesinato de Dumar Noe Mestizo**, integrante del movimiento juvenil de Jambaló, de la vereda La Esperanza en Cauca. Dumar Noe habría sido asesinado en la vereda la Despensa del municipio de Toribío, al norte del departamento, donde esta misma semana se presentó el secuestro y tortura del coordinador y guardia indígena José Albeiro Camayo. (Le puede interesar: ["Tortura contra coordinador indígena José Camayo retrata la aguda crisis del Cauca"](https://archivo.contagioradio.com/tortura-contra-coordinador-indigena-jose-camayo-retrata-la-aguda-crisis-del-cauca/))

Dumar era hijo de Marino Mestizo, líder indígena Nasa a quien la violencia le arrebató la vida en 2009, mientras ejercía el cargo de jurídico en el mismo resguardo indígena de Jambaló al que pertenecía Dumar. Contagio Radio habló en horas de la mañana del viernes con Hermes Pete, consejero mayor del CRIC, quien expresó su preocupación ante los asesinatos sufridos por los pueblos indígenas en el Cauca, y la poca acción que ha tenido el Estado para evitarlos, teniendo en cuenta que en el territorio hacen presencia "más de 2 mil soldados". (Le puede interesar: ["CRIC no cederá ante amenazas contra indígenas"](https://archivo.contagioradio.com/cric-no-cedera-ante-amenazas-contra-indigenas/))

> Maldita violencia, asesinan a joven Indígena, era un ARTISTA Dumar Mestizo de la Vereda de la Esperanza, resguardo indígena de Jambalo, hacía parte del grupo de jóvenes, están asesinando nuestras generaciones, están matando la cultura, ?[@DefensoriaCol](https://twitter.com/DefensoriaCol?ref_src=twsrc%5Etfw) [@SomosDef](https://twitter.com/SomosDef?ref_src=twsrc%5Etfw) [@ONUHumanRights](https://twitter.com/ONUHumanRights?ref_src=twsrc%5Etfw) [pic.twitter.com/s0jJWuvtDp](https://t.co/s0jJWuvtDp)
>
> — Defensa de la Vida y los DDHH CRIC (@CricDdhh) [October 5, 2019](https://twitter.com/CricDdhh/status/1180289219215929344?ref_src=twsrc%5Etfw)

<p>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
</p>
### **El mismo día fue asesinado Antonio Angulo, ex combatiente en proceso de reincorporación**

**El mismo viernes se denunció el asesinato del ex combatiente de las FARC, Santos Antonio Angulo Cabezas en el municipio Iscuande de Nariño.** Antonio Angulo habría sido víctima de un ataque con arma de fuego, y su cuerpo fue hallado en el muelle de Iscuande. (Le puede interesar: ["Continúan los asesinatos a excombatientes de las FARC"](https://archivo.contagioradio.com/nuevo-asesinanto-excombatiente-farc/))

Según las primeras informaciones, Angulo habría sido víctima de amenazas por parte del Ejército de Liberación Nacional (ELN) y había denunciado las mismas ante la Fiscalía, que le hizo una serie de recomendaciones de autocuidado, mientras la Unidad Nacional de Protección (UNP) estaba tramitando su evaluación de nivel de riesgo. Según cifras de organizaciones sociales y el mismo partido Fuerza Alternativa Revolucionaria del Común (FARC), después de la firma del Acuerdo de Paz hasta la fecha, ya han sido asesinados más de 150 excombatientes que le apostaron a la paz.

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
