Title: "Los despavoridos" de Arthur Rimbaud
Date: 2015-10-20 18:13
Category: Viaje Literario
Tags: Aniversario Rimbaud, Arthur Rimbaud, Los despavoridos poema Rimbaud, Poetas malditos
Slug: los-despavoridos-de-arthur-rimbaud
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/10/rimbaud-par-ernestpignonernest-1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### <iframe src="http://www.ivoox.com/player_ek_9102132_2_1.html?data=mpadlJaXdo6ZmKiakpiJd6Kpl5KSmaiRdo6ZmKiakpKJe6ShkZKSmaiRaZOmrdTgjcnJt9HV19Tfy8nTt4amk5Cu1NnMudOfs87axMbZqI6ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe> 

##### [20, Oct 2015] 

Arthur Rimbaud es un poeta francés nacido en Charleville  el 20 de octubre de 1854, desde pequeño demostró un gran talento para la literatura. Muy joven se trasladó a Paris donde trabó amistad con importantes poetas de la época, especialmente con Paul Verlaine con quien sostuvo una tormentosa relación amorosa que terminó dos años después a raíz de serias disputas entre ambos.

De esta época datan las primeras publicaciones "El barco borracho"en 1871 y "Una temporada en el infierno" en 1873. Su obra, de marcado tono simbolista, está profundamente influida por Charles Baudelaire, y su interés en el ocultismo, en la religión y en la exploración sobre el subconsciente individual.

La vida licenciosa lo obligó a dejar por algún tiempo la poesía, viajó por Europa, se dedicó al comercio en el Norte de África y a su regreso a Paris en 1891 ya había sido publicada su obra "Iluminaciones" . Rimbaud Falleció en noviembre de 1891. Compartimos con ustedes su poema "Los despavoridos"
