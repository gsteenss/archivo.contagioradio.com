Title: Las mujeres se preparan para la implementación de los acuerdos
Date: 2016-09-20 14:11
Category: Otra Mirada, Paz
Tags: Acuerdos de paz en Colombia, Corporación Casa de La Mujer, Defensa de derechos de las mujeres, enfoque de género en los acuerdos de paz
Slug: las-mujeres-se-preparan-para-la-implementacion-de-los-acuerdos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/09/Cumbre-Mujeres-y-Paz.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:  Cumbre Mujeres y Paz] 

###### [20 Sept 2016] 

Este lunes inició la Segunda Cumbre de Mujeres y Paz, en la que participan por lo menos 500 mujeres de distintas regiones de Colombia quienes se han congregado para concretar las líneas de trabajo que permitirán verificar que la **implementación de los acuerdos de paz se haga en perspectiva de la garantía de los derechos de las mujeres**. En este encuentro también han participado el Alto Comisionado de Paz, delegados de la ONU, algunos miembros de la Subcomisión de género y líderes de procesos de paz culminado en otras naciones.

De acuerdo con María Eugenia Sánchez, quien es la encargada de incidencia política en la Corporación Casa de la Mujer, **las propuestas que saldrán del encuentro tienen como base lo que se ha pactado en La Habana**, y se espera lograr que cada uno de los seis puntos acordados permita el cumplimiento en la garantía de los derechos de las mujeres, por lo que buscarán el impulso de aspectos como la formalización de la propiedad de la tierra, la consolidación de proyectos productivos y fortalecimiento de la participación política en espacios decisorios.

La experiencia de los diferentes procesos de paz en el mundo ha demostrado que fases como la implementación las mujeres son relegadas y subordinadas, en **desconocimiento de sus capacidades políticas para aportar en aspectos como verdad y justicia**, asegura Sánchez e insiste en que debe haber un monitoreo constante para que los derechos de las mujeres no sean vulnerados en el marco del posconflicto, pues lo que se busca es [[que los acuerdos se vean reflejados en mayores garantías para su vid]a](https://archivo.contagioradio.com/las-mujeres-y-la-defensa-de-los-derechos-humanos-en-colombia/).

<iframe id="audio_12984166" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_12984166_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)]] 

 
