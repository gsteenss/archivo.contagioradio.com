Title: Familias afectadas por Hidroituango exigen la suspensión del proyecto
Date: 2017-02-10 16:32
Category: Ambiente, Nacional
Tags: Desplazamiento HidroItuango, Hidroituango, Movimiento Ríos Vivos
Slug: familias-afectadas-hidroituango-exigen-la-suspension-del-proyecto
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/02/Hidroituango-e1486762164538.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Las 2 Orillas] 

###### [10 Feb 2017 ] 

Inició el campamento en protesta por los desalojos que ha venido promoviendo Hidroituango, el Movimiento Ríos Vivos señaló que **es necesario frenar la “estrategia para desalojar el cañón del río Cauca”**, que hasta la fecha ha desalojado a 500 familias, también advierten que las autoridades locales han ignorado las peticiones de las familias barequeras y “han cedido a las presiones de las Empresas Públicas de Medellín –EPM–”.

Isabel Zuleta, integrante del Movimiento Ríos Vivos comentó que el objetivo de la jornada pacífica es demostrar la existencia de las comunidades en el cañón, puesto que **“lo que ha querido EPM es ocultar que allí habitan las familias,** **incluso en el expediente de Hidroituango las comunidades no aparecen”** a pesar que estas familias han ocupado ancestralmente dichos territorios.

Zuleta manifiesta que “campesinos, barequeros y arrieros” de la región, quieren resignificar el papel que juegan en el departamento y la importancia de sus labores para el ámbito económico y cultural. ([Le puede interesar: Desplazamiento forzado instrumento del desarrollo](https://archivo.contagioradio.com/desplazamiento-forzado-instrumento-del-desarrollo/))

### ¿Qué dicen las autoridades? 

La EPM ha dicho a las comunidades que tienen 1 año para desalojar totalmente la zona, pues el proyecto hidroeléctrico entraría a funcionar en 2018, frente a ello, Zuleta advierte que **“el territorio estaba habitado por las familias mucho antes de que llegará Hidroituango”**, por lo que se hace necesario “la suspensión inmediata del proyecto” puntualiza la defensora.

Zuleta señala que otro de los objetivos es entablar un diálogo con el Alcalde de Toledo, Johny Alberto Marín, y la administración del municipio de Valdivia, que por el momento “es el único que ha suspendido los desalojos”, indicó que **esperan continuar con las gestiones en los municipios de Briceño, Sabanalarga, Peque, Liborina** y los demás afectados con el proyecto.

La defensora y ambientalista, resalta que hasta el momento no hay presencia de policía en cercanías del campamento y que el alcalde ha dicho que **se encuentra fuera del municipio por lo que “no hará presencia el día de hoy”**. ([Le puede interesar: Ya son 500 las familias desalojadas por](https://archivo.contagioradio.com/ya-son-500-las-familias-desalojadas-por-hidroituango/)Hidroituango)

Por último, aseguró que las familias afectadas, organizaciones ambientalistas y de derechos humanos, planean una **gran movilización el 14 de Marzo día internacional en contra de las represas.**

<iframe id="audio_16945946" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_16945946_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
