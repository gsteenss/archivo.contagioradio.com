Title: Comunidades le piden a Duque retomar diálogos con ELN y llevar paz a los territorios
Date: 2019-01-04 12:05
Author: AdminContagio
Category: DDHH, Paz
Tags: acuerdos de paz, comunidades, ELN, Iván Duque, paz
Slug: comunidades-le-piden-a-duque
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/01/marcha-de-las-flores101316_160.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [04 En 2019] 

A través de un comunicado de prensa la guerrilla la del ELN informaron el fin del cese unilateral que había iniciado desde el pasado 23 de diciembre de 2018. En ese sentido comunidades de paz, le enviaron una carta al presidente Iván Duque en la que le piden reanudar los diálogos **para posibilitar la paz en las zonas más golpeadas por el conflicto armado.**

Asimismo denunció que mientras sus tropas estuvieron al margen de acciones bélicas, el **Ejército Nacional adelanto operaciones el pasado 25 de diciembre en donde, según el comunicado, el Ejército bombardeó el campamento de la guerrilla** ubicado en el sur de Bolívar. Además rechazaron el asesinato de 9 líderes sociales en todo el país, que se produjo mientras se realizó esta tregua de dos semanas.

En ese sentido, comunidades de paz, en todo el país, enviaron una carta al primer mandatario Iván Duque, en el que lo invitan a retomar los diálogos de paz con el ELN y sentarse en la mesa, **y manifestaron su comprensión a ambas partes, sin que eso signifique la perdida de voluntad por construir paz**. (Le puede interesar:["¿Por qué gobierno ni avanza, ni rompe conversaciones con el ELN?"](https://archivo.contagioradio.com/conversaciones-de-paz-eln/))

"Comprendemos que se ejerzan exigencias a ellos y que se ejerza la fuerza del Estado, la que debe regirse por las normas de derecho internacional. Y también les comprendemos a ellos que se ejerza el derecho a la rebelión armada justificada en la corrupción, la injusticia tributaria, la injusticia social, y también ella debe regirse por el respeto a lo humanitario. Escribir esto, lejos de justificar nos permite decirle que comprender el uno y el otro, es empezar a buscar salidas"

Asimismo, las comunidades instaron a la guerrilla del ELN a que persista "en estar sentado a la espera de su interlocutor" y apelaron a Iván Duque, en su condición de padre de familia, para que comprenda **"a quiénes padecemos la continuidad del conflicto armado, los adultos mayores, quienes escribimos por nuestros hijos y nietos,** para que nos escuche, y se siente a dialogar buscando salidas a los obstáculos".

### **Los temores de las comunidades** 

De igual modo, las comunidades expresaron que están siendo víctimas de la violencia, tras la firma del Acuerdo de paz, en territorios en donde ha aumentado la militarización, que en ocasiones ha permitido la violación de derechos humanos y denunciaron la aparición y el control de grupos armados ilegales.

Uno de los hechos más recientes de violencia, ocurrió el pasado 31 de diciembre, cuando dos neoparamilitares que se autoidentificaron como integrantes de las Autodefensas Gaitanistas de Colombia, entraron al resguardo de Alto Guayabal, en Jiguamiandó y retuvieron al joven Carlos Rubiano, de 24 años.

Las personas, que vestían pasamontañas y camisetas negras, increparon al muchacho preguntándoles por la ubicación de la guerrilla del ELN. El joven manifestó que ni el ni su comunidad tenían conocimiento, media hora después lo dejaron libre.

Además expresaron su incertidumbre frente al cumplimiento de los Acuerdos de Paz, en donde las comunidades estarían siendo tomadas en cuenta por el Estado, **"los planes de desarrollo con enfoque territorial nos desconocen, entre otras cosas,** debido a una institucionalidad que ignora las regiones, y las decenas de propuestas que durante más de 20 años hemos formulado". (Le puede interesar:["Desplazamiento forzoso en Colombia aumento en un 106% en el 2018")](https://archivo.contagioradio.com/desplazamiento-forzoso-en-colombia-incremento-un-106-en-2018/)

Finalmente le manifestaron al presidente Duque que la legalidad es legitima si se ampara en la justicia, "y la búsqueda del bien común, sin exclusión. Lo contrario es prolongar la agonía de millones de colombianos que vemos como poco podemos vivir en libertad en nuestras tierras".

###### Reciba toda la información de Contagio Radio en [[su correo]
