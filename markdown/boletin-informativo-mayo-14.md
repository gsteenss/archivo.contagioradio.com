Title: Boletín informativo Mayo 14
Date: 2015-05-14 16:27
Author: CtgAdm
Category: datos
Tags: Actualidad Contagio Radio, Noticias del día
Slug: boletin-informativo-mayo-14
Status: published

*[Noticias del Día:]*

<iframe src="http://www.ivoox.com/player_ek_4497475_2_1.html?data=lZmmmZmbeY6ZmKiakpeJd6KpmZKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRhtDgxtmSpZiJhaXijK7byNTWscLoytvcjbLFvdCfkpmah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

-El abogado **Jorge Lizarazo**, representante de una de las **niñas victimas de violación** por parte de **militares de Estados Unidos** en **Melgar Colombia**, denuncia que la menor fue drogada y conducida a la **base militar de tolemaida**, lugar en el que fue abusada y que además el crimen se encuentra en absoluta impunidad puesto que los militares son cobijados por un acuerdo con el gobierno colombiano y por la justicia penal militar de Estados Unidos.

-Las organizaciones que hacen parte de la **Cumbre Agraria** concluyeron ayer su asamblea nacional ante la crisis por **incumplimiento por parte del gobierno** colombiano en por lo menos el **80% de los acuerdos**. El paso a seguir son asambleas departamentales por espacio de 2 meses para definir una nueva jornada de movilización. **Cesar Jeréz**, vocero de la **Asociación Nacional de Zonas de Reserva Campesina** explica la hoja de ruta.

-Las comunidades indígenas y la **Fundación ProSierra**, aseguran que debido al acelerado descongelamiento de los nevados de la **Sierra Nevada de Santa Martha**, es posible que para el año 2040 hayan desaparecido, lo que generaría que los caudales de 30 ríos se vean afectados o podrían llegar a desaparecer.

-El próximo 17 de mayo, se celebra el **Día Internacional del Reciclaje**, es por eso que este lunes festivo, en Bogotá se realizan diversas actividades por parte de la organización **Basura Cero Colombia**, con el objetivo de que la ciudadanía se acerque cada vez más al reciclaje. Habla **Luisa Fernanda León**, de la organización convocante.
