Title: Insoportable es la situación de los habitantes en Mochuelo Alto por Relleno Doña Juana
Date: 2017-12-06 08:19
Category: DDHH, Movilización, Nacional
Tags: Basurero doña juana, Mochuelo, Moscas
Slug: insoportable-es-la-situacion-de-los-habitantes-en-mochuelo-alto-por-relleno-dona-juana
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/10/DHNq2J2W0AAAkkI.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Paro del Sur] 

###### [05 Dic 2017] 

Los habitantes de la vereda Mochuelo Alto, en la localidad de Ciudad Bolívar, denunciaron que, aunque ya han pasado dos meses del paro desde el Sur, en donde las comunidades exigían garantías para la digna vida, las condiciones se han agravado en los alrededores del Relleno Sanitario Doña Juana, **provocando el aumento de plagas como las moscas, ratas, gusanos y los malos olores**.

De acuerdo con Wilmer Albornoz, líder social de esta localidad, desde hace 30 años los campesinos que viven en este lugar han habitado en la misma precariedad “una vez más, se incrementan lo que los ingenieros y técnicos llaman vectores, **es decir el número de moscas, ratones, babosas y demás se incrementan**”.

En el caso particular de las moscas Albornoz, afirmó que se ven enjambres negros en las calles, así como “manchas negras en las paredes, ventanas y techos de casas, en las tiendas, los restaurantes y las panaderías”, además Albornoz expresó que la situación no solo es preocupante por las plagas sino por el **riesgo en la salud de los niños, ancianos y los habitantes de alrededor del Relleno, debido a la propagación de bacterias**.

Durante el último paro, las instituciones se habían comprometido a estar pendientes de la operación del Basurero y generar las sanciones, sin embargo, de acuerdo con Albornoz, las sanciones no están siendo aplicadas y los compromisos del operador no se están viendo reflejados, ya que **solo se aplicaron los primeros días después del paro**.

“El recubrimiento como lo obliga la ley, lo están haciendo a medias o no lo hacen y eso permite el incremento de los vectores, y lo hacen cuando les parece” afirmó Albornoz y agregó que el manejo ha sido irresponsable. (Le puede interesar: ["Aumentaron las moscas y malos olores en zonas aledañas a Basurero Doña Juana"](https://archivo.contagioradio.com/aumentaron-moscas-y-malos-olores-en-zonas-aledanas-al-basurero-de-dona-juana/))

### **30 años viviendo sin atención desde la Alcaldía** 

Albornoz señaló que si bien es cierto que los habitantes llevan 30 años viviendo en condiciones indignas que atentan contra su salud, se han venido aumentando las afectaciones debido al mal manejo por parte del operador del basurero, el **Consorcio Centro de Gerenciamiento de Residuos y a la falta de atención por parte de la Alcaldía y el gobierno nacional**. (Le pude interesar: ["Habitantes de más de 100 barrios del Sur de Bogotá protestan contra Relleno Doña Juana"](https://archivo.contagioradio.com/habitantes-de-mas-de-100-barrios-del-sur-de-bogota-protestan-contra-relleno-sanitario-dona-juana/))

Frente a esta situación los habitantes están convocando a un plantón hoy, en donde se establecerá una mesa de diálogo en la que se espera participe el director del CGR, Carlos Vega. De igual forma, **Albornoz afirmó que se está pensando retomar la movilización del paro del Sur, para que haya un cumplimiento a lo pactado**.

<iframe id="audio_22482308" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_22482308_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
