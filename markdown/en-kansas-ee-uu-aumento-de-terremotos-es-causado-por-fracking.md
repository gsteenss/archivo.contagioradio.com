Title: En Kansas, EE.UU, aumento de terremotos es causado por fracking
Date: 2015-01-22 22:48
Author: CtgAdm
Category: Ambiente, Entrevistas
Tags: AIDA, fracking, Kansas, medio ambiente, Proceso de
Slug: en-kansas-ee-uu-aumento-de-terremotos-es-causado-por-fracking
Status: published

#### Foto: Robert Saucier 

**[Entrevista Astrid Puentes:]**<iframe src="http://www.ivoox.com/player_ek_3985944_2_1.html?data=lJ6ll56YeI6ZmKiakp6Jd6KkmZKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRhdTo087RjbXZqc%2Foxtiah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

Según el Servicio Geológico de Kansas **el fracking está directamente relacionado con el aumento en la actividad sísmica** de ese Estado.

La entidad, vincula los terremotos ocurridos en la zona con la inyección de aguas residuales del fracking. Se denuncia que el **Estado registró más de 120 terremotos el año pasado frente a ninguno en el 2012.**

El fracking en Colombia y a nivel internacional ha sido una actividad bastante polémica, que ha generado diversas discusiones entre sectores ambientales y económicos, debido a que los ecologistas afirman que esta actividad es perjudicial para el ambiente y la salud. (Lea también: [La implementación del fracking en Colombia es una mala noticia para el país y la región](https://archivo.contagioradio.com/index.php?option=com_content&view=article&id=8123:la-implementaci%C3%B3n-del-fracking-en-colombia-es-una-mala-noticia-para-el-pa%C3%ADs-y-la-regi%C3%B3n&catid=8:otra-mirada&Itemid=2))

Se trata de una forma de explotación de hidrocarburos en la que se realiza un fracturación hidráulica, se bombea en el suelo una mezcla de agua y sustancias químicas para poder romper formaciones rocosas y liberar el petróleo y el gas.

Astrid Puentes, Co-Directora Ejecutiva de la Asociación Interamericana para la Defensa del Ambiente, AIDA, afirma que la situación en Kansas es otro ejemplo del impacto que genera el fracking, además de otros daños irreversibles en la salud y el medio ambiente.

**“El impacto del fracking en Kansas es otra evidencia para que en Colombia se logre una moratoria como un principio de precaución para que se tome una buena decisión”**, señala Astrid. Así mismo, la Co- Directora de la AIDA, dice estar preocupada debido a que no existe suficiente información en las comunidades colombianas sobre el impacto del fracking.

Desde el Ministerio de Minas y Energía, se afirma que esta actividad es necesaria para financiar parte del proceso de paz que se lleva a cabo en la Habana. Sin embargo, la Unión Sindical Obrera, USO, recuerda que según analistas, un pos-conflicto en Colombia, costaría cerca de 187  billones de pesos, quiere decir, que con lo que recibiría el Estado por impuestos a multinacionales de hidrocarburos, tendría como financiarse el proceso de paz hasta 10 veces.

Además, Astrid considera que “**es bastante peligroso que se vincule el fracking o cualquier otra actividad extractiva con el proceso de paz,** aun cuando los ambientalistas apoyan una salida pacífica del conflicto”.

“**El problema del fracking es que al final pueden ser muchos más los costos para el país que los beneficios** (…) se pide al gobierno que haga una moratoria y que se asegure que no va a haber impactos, esto no es un escándalo de ambientalistas, son decisiones que se han tomado en diferentes países de manera seria y documentada”.

España, Inglaterra, Alemania, Francia y New York, son solo algunos de los ejemplos para que el Gobierno colombiano considere la relación costo beneficio de la realización del fracking, ya que para la AIDA, el afán de promover el desarrollo puede causar otro impacto mucho peor para el país.
