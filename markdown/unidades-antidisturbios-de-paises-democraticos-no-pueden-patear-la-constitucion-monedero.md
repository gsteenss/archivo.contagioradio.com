Title: Unidades antidisturbios de países democráticos no pueden patear la constitución: Monedero
Date: 2019-10-03 22:31
Author: CtgAdm
Category: DDHH, Nacional
Tags: Antidisturbios, ESMAD, podemos, Universidades
Slug: unidades-antidisturbios-de-paises-democraticos-no-pueden-patear-la-constitucion-monedero
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/8105828779_b88bd1f2c6_c.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: El Turbión] 

Juan Carlos Monedero, fundador del partido Podemos, uno de los más importantes en el Estado Español estuvo presente en la rueda de prensa en que universitarios colombianos denunciaron el actuar del ESMAD durante las protestas estudiantiles de las últimas semanas. Monedero aseguró que la solidaridad y la empatía son las claves en la defensa de la democracia, y denunció que **es necesario que se revalúe el trabajo de las unidades antidisturbios.**

En una comparación con la represión en contra de las protestas ciudadanas en Cataluña, el líder de Podemos manifestó que la revaluación de los escuadrones antidisturbios deberían basarse en el respeto de la constitución y no patearla. Según él, esas actuaciones no son aplicables en países democráticos, pues pareciera que dichos escuadrones están hechos para acallar a las voces críticas en todo el mundo. (Le puede interesar: ["Saldo catastrófico de operación del ESMAD contra protestas y universidades"](https://archivo.contagioradio.com/saldo-catastrofico-de-operacion-del-esmad-contra-protestas-y-universidades/))

Monedero también resaltó que **son los propios Estados los que deben ponerse al frente de ese rediseño,** en clara contradicción con lo manifestado por el ministro de defensa Guillermo Botero, quien ante las cientos de denuncias por las agresiones del ESMAD, y la pregunta en torno a la regulación de esta fuerza, afirmó tajantemente que no es hora de rediseñar ese cuerpo de seguridad que habría dejado por lo menos tres heridos de gravedad y cientos de afectados durante las movilizaciones de las semanas anteriores en Colombia.

### [Solidaridad y empatía son las claves para defender la democracia en un mundo global] 

Aunque Monedero afirmó que "es difícil defender la democracia en un mundo globalizado" en que los poderosos acuden al G7, al G20 o el Foro Económico Mundial para defender sus intereses, los estudiantes tienen la capacidad de unirse en torno a la solidaridad. Por lo tanto, **se debe acudir a la solidaridad y la empatía** para entender que "defender desde España a los estudiantes en Colombia es también defendernos a nosotros mismos". (Le puede interesar: ["Marcha estudiantil del viernes termina en nuevas agresiones por parte del ESMAD"](https://archivo.contagioradio.com/marcha-estudiantil-agresiones-esmad/))

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
