Title: Inicia el Foro Social Mundial en Túnez
Date: 2015-03-26 17:43
Author: CtgAdm
Category: Movilización, Otra Mirada
Tags: Ecologistas en Acción, Foro Social Mundial Túnez, Tom Kurchaz
Slug: inicia-el-foro-social-mundial-en-tunez-con-homenaje-a-victimas-del-atentado-del-ei
Status: published

###### Foto:ApdhaCadiz.wordpress.com 

##### **Entrevista con [Tom Kurchaz, [integrante de la organización social española Ecologistas en Acción]][:]** 

<iframe src="http://www.ivoox.com/player_ek_4268649_2_1.html?data=lZejmpuYfY6ZmKiakp6Jd6KkkZKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRitDm0JDA0cjNpc2frtrbxs7FsIzIhqigh6elssbujoqkpZKns8%2FowszW0ZC2pcXd0JCah5yncZU%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

El martes 26 de Marzo quedó** inaugurado el Foro Social Mundial en Túnez**, con una gran marcha desde la rotonda de Bab Saadoun, cerca de donde se va a realizar el foro, hasta el **museo del Bardo**, lugar en que se produjo el **atentado terrorista que costó la vida a más de 20 personas.**

Con el objetivo de solidarizarse con las víctimas y de repudiar actos terroristas como el del museo del Bardo, la marcha inaugural cambió el recorrido para poder pasar por el lugar donde se cometió la masacre. Los lemas más escuchados fueron los de **“Otro mundo es posible” y todo tipo de eslóganes feministas**, debido a la fuerte presencia de mujeres.

Según **Tom Kurchaz**, participante del FSM y miembro de **Ecologistas en Acción**, estos foros llevan realizándose desde **hace 14 años**, cuando organizaciones sociales de todo el mundo decidieron hacer frente al foro que se hace todos los meses de enero en **Davos, donde se reúnen el FMI, el BM, banqueros y élites financieras** mundiales con el objetivo de establecer las políticas **neoliberales** de corte capitalista para cada año.

Conscientes de las **graves consecuencias estructurales de las políticas neoliberales**, como la desigualdad, la pobreza, la violencia política y la represión, y el expolio, las organizaciones sociales crearon los **FSM para poder defenderse y resistir** a dichas políticas creando un modelo alternativo.

El antecedente a este foro es el que ya se realizó en **2013** en el mismo país para poder apoyar las luchas de los movimientos sociales en las protestas conocidas como **“Primavera árabe”, en países como Túnez, Egipto, Bahrein o Siria.**

Tom Kurchaz afirma que este año hay **temas destacados** para debatir como las causas estructurales de la violencia política de grupos extremistas como el **Estado Islámico o el propio patriarcado.** Razones como el expolio o las políticas de ajuste del FMI, causan desigualdad social que es el caldo de cultivo perfecto para el florecimiento de grupos como el EI.

En el transcurso del foro se realizan **debates** entre las miles de organizaciones sociales que participan. Al final se decidirán las **agendas de lucha** con respecto a temáticas como los campesinos sin tierra, los movimientos sociales feministas, el cambio climático, la lucha contra las grandes corporaciones, la privatización del agua y de los recursos naturales o la defensa de los movimientos sociales en el mundo árabe.

Kurchaz, finaliza diciendo que no solo se discuten y preparan las **políticas alternativas a el capitalismo globalizado** sino que también se **fortalecen las redes creadas en el foro de 2013**, con presencia de organizaciones sociales de todos los puntos del planeta.
