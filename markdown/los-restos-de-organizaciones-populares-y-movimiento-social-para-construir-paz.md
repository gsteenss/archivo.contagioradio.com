Title: Organizaciones sociales ponen lupa a recursos públicos para la paz
Date: 2017-02-13 16:37
Category: Movilización, Nacional
Tags: Agenda Común para la Paz, Movimiento social
Slug: los-restos-de-organizaciones-populares-y-movimiento-social-para-construir-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/05/movilizacio-paz-contagioradio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo  Particular ] 

###### [13 Feb 2017] 

**El clientelismo y la coptación de recursos públicos, la participación política, la reorganización de los territorios**, son algunos de los retos  y desafíos que de acuerdo con la Agenda Común de paz, deberán asumir las organizaciones populares y el movimiento social en medio de los cambios que vive Colombia hacia la paz.

La Agenda Común plantea que el papel que deben ocupar tanto las organizaciones sociales populares como el movimiento social, no puede suscribirse a una idea de paz funcional acorde al modelo vigente; y ya que la gran mayoría de estos procesos se encuentran en las regiones deberán **"tener cuidado con riesgos como quedar inmersos en un control de interés económicos y juegos políticos". **Le puede interesar: ["Organizaciones Sociales exigen pronta implementación de Acuerdos de Páz"](https://archivo.contagioradio.com/organizaciones-sociales-exigen-pronta-implementacion-de-acuerdos-de-paz/)

**De igual forma, esta agenda evidencia que sectores como partidos políticos o el mismo gobierno intentarán moldear algunos de los puntos pactados en La Habana** que garantizaban avances para el campesinado o las víctimas, a través de ejecución de proyectos o políticas clientelares, lo que demuestra la necesidad de fortalecer  procesos de base en las regiones y las alianzas populares “que den respuesta a la recomposición del régimen político, al modelo de desarrollo y a la implementación de los Acuerdos de Paz”.

La Agenda Común de Paz, se construye en un momento de tránsito para el país que de acuerdo con las organizaciones que lo componen, genera diversos escenarios para la recomposición de las regiones, la **mayor autonomía que podrían ganar procesos y apuestas populares, la reducción del narcotráfico** y una apuesta por integrar más aceradamente tanto al campesinado como a las víctimas del conflicto armado a políticas públicas.

En esta iniciativa **participaron 338 mujeres y 427 hombres de más de doscientos cincuenta organizaciones sociales populares,** en 19 departamentos del país, en regiones como el Suroccidente, Centro, Eje Cafetero, Nororiente y Caribe y fue respaldada por las organizaciones OXFAM y Planeta Paz. Le puede interesar: ["Organizaciones sociales exigen justicia para todos en implementación de JEP"](https://archivo.contagioradio.com/organizaciones-sociales-exigen-justicia-para-todos-en-implementacion-de-jep/)

###### Reciba toda la información de Contagio Radio en [[su correo]
