Title: Trabajo Social apunto de extinguirse como profesión dentro de las Ciencias Sociales
Date: 2016-10-31 20:39
Category: Educación, Nacional
Tags: Acuerdo 2034, Movimiento estudiantil Colombia, Privatización Educación
Slug: trabajo-social-apunto-de-extinguirse-como-profesion-dentro-de-las-ciencias-sociales
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/10/trabajo-social1-e1477963157978.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: El Hermano Mayor] 

###### [31 de Oct 2016] 

De acuerdo con la  resolución 776, del DANE y bajo el amparo del Ministerio de Educación la carrera de trabajo social que se encontraba al interior de las ciencias sociales, **pasará a ser parte de las ciencias de la salud y únicamente tendrá como campo el Bienestar**, esta decisión afecta aproximadamente 40 departamentos de Trabajo Social, el campo de acción laboral no solo de los egresados, sino de los estudiantes y la malla curricular de la carrera que tendrá que replantearse.

Frente a esta situación tanto estudiantes como directivas de diferentes universidades ya se han pronunciado. Para las Directivas del Colegio Mayor de Cundinamarca, esta decisión en primer lugar es imprecisa **“debido a ley 53 de 1977 que ubica a esta profesión en el áreas de las Ciencias Sociales y que cumple actividades relacionadas con las políticas de bienestar y desarrollo social”**

De otro lado las Directivas también señalan que este hecho **desconoce la trayectoria histórica de la profesión y la incidencia que esta ha tenido en la solución de problemas sociales en el país**. Además indican que se limitan los campos de acción de la profesión que se han desarrollado incluso en la investigación de fenómenos sociales.

Para Natalia Cansino, estudiante de Trabajo Social de la Universidad Nacional, la resolución **pretende cumplir unos parámetros establecidos por la UNESCO,** en donde se deben reubicar algunas profesiones entre ellas Trabajo Social.

Sobre el futuro de los estudiantes y egresados, Cansino ejemplifico que en el momento que se de paso a la construcción de un pos acuerdo, ellos quedarían por fuera, **“ya no habría una intervención políticas o a víctimas de nuestra parte, o una intervención en derechos humanos que es lo que estamos haciendo los trabajadores sociales”.**

Estudiantes, egresados y algunas instituciones de Educación Superior, le están pidiendo al Ministerio de Educación que modifique esta resolución, **“nuestro campo de intervención integral se desarrolla desde las Ciencias Sociales** y no podemos permitir que limiten la intervención que tiene nuestra profesión” afirmó Cansino.  A su vez, se están realizando asambleas a nivel distrital y nacional, para establecer una agenda que determinará las próximas acciones de los estudiantes. Le puede interesar: ["Estudiantes colombianos se movilizan en contra de la 2034"](https://archivo.contagioradio.com/estudiantes-colombianos-se-movilizan-contra-el-acuerdo-2034/)

<iframe id="audio_13566096" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_13566096_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en su correo [[bit.ly/1nvAO4u]
