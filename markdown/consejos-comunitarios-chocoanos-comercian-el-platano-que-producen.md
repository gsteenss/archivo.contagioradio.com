Title: Consejos Comunitarios chocoanos comercian el platano que producen
Date: 2015-01-05 14:32
Author: CtgAdm
Category: Comunidad
Slug: consejos-comunitarios-chocoanos-comercian-el-platano-que-producen
Status: published

###### Fotografía: ICA 

###### [Descargar Audio](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/01/fslxeteJ.mp3) 

La tarde del 19 de julio del 2014 la asociación de familias pertenecientes a los consejos comunitarios de Curbarado, Jiguamiandó, Pedeguita y Mancilla, y Vigía Curbaradó (AFLICOC) envió el primer viaje de plátano, de la cuenca del Rio Curbaradó hacia la plaza de Cartagena en busca de un comercio justo para nosotras las comunidades ya que han venido sufriendo un bloqueo económico empresarial liderado por las empresas bananeras Banacol, Banur, Uniban entre otras que están dentro de los territorios sometiendo a las familias a que trabajen para ellas, en condiciones de desigualdad y desventaja. Para los Consejos Comunitarios es un logro y un desafío, por eso seguirán buscando comercio solidario para los demás productos agrícolas que producimos las comunidades.
