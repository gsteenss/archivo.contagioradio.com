Title: Estados Unidos interviene directamente en Ucrania con 300 efectivos militares
Date: 2015-04-17 17:39
Author: CtgAdm
Category: El mundo, Otra Mirada
Tags: 300 militares de EEUU entrenaran fuerzas Ucranianas, Diputado asesinado Ucrania, Diputado del partido de Víctor Yanukovich asesinado, Periodista asesinado Ucrania, Periodista proruso Ucrania
Slug: estados-unidos-interviene-directamente-en-ucrania-con-300-efectivos-militares
Status: published

###### Foto:HD.Clarín.com 

En los últimos días se han cometido **dos asesinatos políticos**, el primero el de un periodista proruso y el segundo el de un diputado del parido del ex-presidente Víctor Yanukovich, estos dos asesinatos se suman a **los nueve acometidos en el último mes**.

El miércoles en la noche **abatieron al ex diputado Oleg Kaláshnikov** del partido del **ex presidente Víctor Yanukovich** huido a Rusia tras las protestas de Maidan en 2014.

El jueves fue **asesinado** en la capital por dos sicarios el **periodista Oles Buzina** perteneciente a un **pequeño partido político proruso** que había conseguido presentarse a las últimas elecciones

El (PR) Partido de las Regiones, del anterior mandatario es el partido que más asesinatos ha padecido en el último mes, creando así un clima de inestabilidad para el país.

Ambos estaban **vinculados a la oposición al movimiento político conocido como Maidan** que inició las protestas en 2014 contra el gobierno electo.

También los dos asesinados pertenecían al **círculo del oligarca del este Rinat Ajmétov**, de hecho el periodista trabajaba para uno de sus diarios y el ex diputado para una de sus empresas de “holding”.

Según la **UNED y organizaciones sociales de defensa de los DDHH**, las fuerzas de seguridad ucranianas **no se encuentran preparadas para llevar a cabo la investigación de los asesinatos** políticos así como de masacres como la de Maidan o la de Odessa.

Para el presidente del país Petro **Poroshenko los crímenes “…recaen en las manos de sus enemigos..”** y ha ordenado una investigación clara y transparente. Para el presidente ruso Vladimir **Putin la situación en Ucrania ha estallado y la persecución contra periodistas prorusos** es una prueba de lo que está sucediendo actualmente, también ha acusado al régimen ucraniano de permisividad y de no impedir dichos asesinatos.

Después de la noticia de los asesinatos selectivos hoy han **desembarcado 300 militares de EEUU con el objetivo de entrenar a las fuerzas de seguridad del país**. Este gesto ha sido considerado por la oposición y por las repúblicas separatistas **como injerencia de EEUU y de la OTAN, considerándolo un paso más para la inestabilidad y el complejo proceso de paz.**

A un año del inicio del conflicto armado, no existe una mesa de conversaciones de paz y según Amnistía Internacional no se han cumplido los compromisos del cese al fuego por las dos partes.
