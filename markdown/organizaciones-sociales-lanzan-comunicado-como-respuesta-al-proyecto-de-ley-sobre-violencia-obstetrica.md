Title: Organizaciones sociales lanzan comunicado como respuesta al Proyecto de Ley sobre Violencia Obstétrica
Date: 2018-05-17 14:12
Category: Nacional, yoreporto
Tags: Embarazos, Violencia Obstétrica
Slug: organizaciones-sociales-lanzan-comunicado-como-respuesta-al-proyecto-de-ley-sobre-violencia-obstetrica
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/05/portada-3-e1526583454928.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Radio Universidad Chile] 

### [\#YoReporto] 

###### [17 May 2018] 

18 organizaciones sociales y 21 personas de manera independiente que trabajan asuntos relacionados con la salud sexual y reproductiva, han dado a conocer su posición a la opinión pública, frente al proyecto de ley que la Comisión Séptima del Senado de la República aprobó a finales de abril **"por medio del cual dictan medidas para prevenir y sancionar la violencia obstétrica"**.

Desde una postura claramente crítica, han hecho énfasis en que antes de proponer dicho proyecto de ley, se hace necesario el diálogo, debate y discusión de manera amplia entre los actores involucrados. Aseguran, entre otras cosas, que la justificación del proyecto no tiene en cuenta **ninguna evidencia científica hecha en Colombia, que dé cuenta de la mortalidad materna como de la morbilidad a causa de este tipo de violencia de género**.

 Reconocen que efectivamente, **existen prácticas médicas y administrativas individuales o grupales que son manifestaciones claras de violencia ginecobstétrica**,  pero que no se debe cargar la responsabilidad totalmente a los profesionales y/o administrativos del sector salud, pues existen otro tipo de factores influyentes en esta problemática, como las prácticas pedagógicas y currículos desde las facultades de ciencias de la salud, las condiciones laborales del sector salud y la remuneración por su experiencia, trabajo y nivel académico, la lógica mercantil del sistema de salud colombiano y la falta de Estado en algunas partes del país.

 Especifican que la violencia ginecobstétrica  **no solo se presente en los partos, sino que también sucede en abortos espontáneos e interrupciones voluntarias del embarazo,** y que pueden dar evidencia de ello, gracias a su trabajo de acompañamiento y asesoría a mujeres gestantes, así como en investigaciones propias de sus colectivos.

Como propuesta sugieren fortalecer la Ley 1257 **"Por la cual se dictan normas de sensibilización, prevención y sanción de formas de violencia y discriminación contra las mujeres”** y que sea definida en ésta misma ley el concepto de violencia ginecobstétrica para que sea la herramienta efectiva y suficiente para la prevención y sanción de todo tipo de violencia de género, incluyendo la violencia ginecobstétrica .

Entre estas organizaciones sociales hay inconformidad frente a la presentación de este proyecto de ley, por no tener en cuenta su trabajo, ni mencionar sus investigaciones publicadas, aportes académicos y de activismo social y político. De igual manera, declaran que se desconoce las labores de acompañamiento en diferentes momentos del ciclo sexual y reproductivo de mujeres en diferentes partes del país, el cual debe ser reconocido y valorado, ya que de esta manera las organizaciones firmantes antes de dicho proyecto de ley han “posicionado el tema en la agenda social en diversos escenarios, en pro de la prevención de las violencias contra las mujeres” y reafirman entonces la necesidad de debate sobre esta ponencia y “garantizando su participación con voz y voto”, como las agentes de cambio y líderes sociales que son.

.  
 
