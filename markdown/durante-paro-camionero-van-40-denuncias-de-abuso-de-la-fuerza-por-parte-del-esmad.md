Title: Durante Paro Camionero van 40 denuncias de abuso de la fuerza por parte del ESMAD
Date: 2016-07-15 16:01
Category: DDHH, Nacional
Tags: Abuso de fuerza ESMAD, agresiones esmad
Slug: durante-paro-camionero-van-40-denuncias-de-abuso-de-la-fuerza-por-parte-del-esmad
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/07/esmad-marcha.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio radio] 

###### [15 Julio 2016 ] 

La Comisión de Derechos Humanos que hizo presencia en la movilización Camionera, informó que se han reportado **aproximadamente 40 denuncias por parte de civiles en donde se acusa al ESMAD** de tener un abuso de fuerza y autoridad hacía la comunidad durante el Paro. 21 detenciones arbitrarias, amenazas, heridas y asesinatos son las que se destacan en la verificación realizada.

**El asesinato de Luis Orlando Sainz**, no es el único uso desmedido de autoridad que se ha presentando a lo largo de los 39 días que lleva el Paro Camionero. De acuerdo con Adriana Pico, miembro de la comisión de derechos humanos, las personas han [denunciado que mientras estaban en sus casas fueron agredidos por petardos](https://archivo.contagioradio.com/gobierno-quiere-criminalizar-paro-camionero/), bombas de aturdimiento y las demás armas usadas por  el ESMAD para controlar las manifestaciones.

A su vez, la defensora de derechos humanos también expone que se han reportado **capturas ilegales por parte de la Policía** en donde retienen a las personas, les piden el documento de identificación, los trasladan a las estaciones y posteriormente son acusados de pertenecer al movimiento del Paro Camionero. **Hasta el momento se denuncian 13 personas en esta situación**.

Otro de los abusos que pudo constatar la comisión de derechos humanos es que en los 100 puntos de concentración que tienen los camioneros **hay daños a viviendas y cuadras que se encuentran completamente afectadas** por los dispositivos usados por parte de la Fuerza Pública, además constataron que los enfrentamiento que se han dado entre los manifestantes y el ESMAD, han sido desmedidos por el nivel de violencia que se ha usado en los mismos.

**"Hay gente golpeada,** **hay un señor hospitalizado en Duitama con serías lesiones en su cráneo**, han utilizado estas bolas de goma que han impactado en los ojos de las personas, hay personas adultas que solo por el hecho de estar comprando lo del desayuno fueron golpeadas, [golpearon a diestra y siniestra todas las personas que estuvieran en la calle](https://archivo.contagioradio.com/crece-represion-del-gobierno-hacia-el-paro-camionero/)" afirmo Adriana Pico, miembro de la comisión de verificación de Derechos Humanos en el Paro Camionero.

Las conclusiones de este primer informe son:

 

1\. Personas heridas con artefactos lanzados por el Esmad, que causaron hematomas y laceraciones en rostro, cuerpo y cabeza de varias personas.

2\. Personas heridas producto de golpizas proferidas por parte del ESMAD con objetos contundentes.

3\. Personas detenidas arbitrariamente en varios puntos, entre estas 13 en el municipio de Cajicá, 4 en Venta Quemada y 4 que fueron retenidas temporalmente en Paipa.

4\. Uso indiscriminado de gases, en sitios cerrados contra los pobladores de las veredas y barrios cercanos a los puntos de concentración; quienes manifiestan que les han sido lanzadas capsulas de gases lacrimógenos al interior de sus casas, causando asfixia y problemas respiratorios principalmente a niños y adultos mayores. Dichas familias se encuentran atemorizadas.

5\. El señalamiento y amenazas por parte de la fuerza pública contra los manifestantes, y habitantes de este departamento, especialmente a líderes y miembros de juntas de acción comunal, mediante comentarios en los que se señala la intención de eliminarlos.

6\. La ocupación de al menos una institución educativa por el ESMAD en el municipio de Paipa.

7\. El asesinato de Luis Orlando Saiz en la ciudad de Duitama, el cual según testimonios de manifestantes y pobladores que se encontraban en el lugar de los hechos, el joven fue impactado por una granada de gas lacrimógeno lanzada directamente a su cabeza por parte del Esmad el día 12 de Julio del presente año, cuando se trasladaba camino a su casa después de la jornada laboral en el sitio conocido como Higueras. Versión que coincide con el dictamen emitido el día 14 de julio de 2016, por el Instituto Nacional deMedicina Legal y Ciencias Forenses.

8\. La utilización de ambulancias para el transporte de armas y miembros del ESMAD en el municipio de Paipa.

9\. Militarización y medidas propias de Estados de excepción, tales como toque de queda en ciudades como Duitama.

10\. Varios de los manifestantes señalaron que luego de los ataques, las personas heridas se quejaron de la atención inadecuada por parte del personal del hospital de Duitama, señalando que por su presunta participación en la manifestación no podrían seratendidos.

[  
](https://archivo.contagioradio.com/con-una-instalacion-en-los-heroes-inicia-indiebo-2016/indiebo/)<iframe src="http://co.ivoox.com/es/player_ej_12236983_2_1.html?data=kpeflZudfJShhpywj5aXaZS1lJuah5yncZOhhpywj5WRaZi3jpWah5yncaLY087O0MaPlMrX0IqfpZDHs87d1M6SpZiJhpTijMnSjcnJtsbXydTgjc3ZscLi0Niah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ..&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>
