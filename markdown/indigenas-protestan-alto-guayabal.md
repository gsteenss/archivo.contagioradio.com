Title: Indígenas protestan por militarización y paramilitarización en Alto Guayabal
Date: 2019-08-28 08:14
Author: CtgAdm
Category: Movilización, Nacional
Tags: AGC, Alto Guayabla, Chocó, Jiguamiandó, Mineria
Slug: indigenas-protestan-alto-guayabal
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/08/WhatsApp-Image-2019-08-26-at-5.48.12-PM.jpeg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/08/WhatsApp-Image-2019-08-26-at-5.48.11-PM1.jpeg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/08/WhatsApp-Image-2019-08-26-at-5.48.11-PM.jpeg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/08/WhatsApp-Video-2019-08-26-at-5.48.11-PM.mp4" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio  
] 

Desde el pasado 24 de agosto, **comunidades indígenas de Alto Guayabal, Chocó, se han manifestado reclamando la protección de sus vidas, y contra las actividades armadas en su territorio.** De las marchas participan más de 8 comunidades, con un total de más de 170 personas, que exigen el respeto a los pueblos originarios, su cosmovisión y su lucha por la defensa de la vida y el territorio.

![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/08/WhatsApp-Image-2019-08-26-at-5.48.12-PM-1024x682.jpeg){.aligncenter .size-large .wp-image-72622 width="1024" height="682"}

**Estefania Abello, defensora de derechos humanos e integrante de la Comisión Intereclesial de Justicia y Paz (J&P),** afirmó que la movilización que se ha dado en Jiguamiandó, Chocó, es de carácter indígena y tiene distintas reivindicaciones, exigencias y denuncias. Estas últimas, relacionadas con las "continuas acciones de violencia contra la población civil, y el no respeto a sus territorios", señaló Abello.

En ese sentido, las comunidades **rechazan las amenazas que se han presentado contra líderes, quienes han sido citados y señalados por las autodenominadas Autodefensas Gaitanistas de Colombia (AGC)**, que hacen presencia en la zona. Asimismo, las comunidades en protesta rechazan las operaciones empresariales ilegales, y los ataques a la guardia ambiental, encargada del cuidado y defensa del territorio.

### **En contexto: Explotación de minerales en el Cerro Cara de Perro, Alto Guayabal  
** 

Según la Comisión (J&P), desde 1996 intereses económicos buscaron asentarse en el territorio para el desarrollo de agroindustria de palma, banano, teka, yuca, piña y ganadería. Posteriormente, entre 2008 y 2009, la empresa **Muriel Mining  Corporation (MMC)** inició una exploración que no se consultó con las comunidades en el Cerro Cara de Perro para la explotación de oro, cobre y molibdeno. Este cerro tiene importancia para comunidades afro e indígenas de la zona, pues tiene un carácter sagrado y espiritual en su cosmovisión.

\[caption id="attachment\_72628" align="aligncenter" width="1024"\]![Comunidades protestan en Alto Guayabal](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/08/WhatsApp-Image-2019-08-26-at-5.48.11-PM-1024x682.jpeg){.size-large .wp-image-72628 width="1024" height="682"} Foto: Contagio Radio\[/caption\]

Por esta razón, las comunidades realizaron en 2009 la primer consulta popular interétnica, en la que expresaron su rechazo a la intervención minera a cielo abierto. Tras la consulta, y la decisión de la Corte Constitucional en 2010 de amparar el derecho de las comunidades a la Consulta Previa, Libre e Informada; MMC cesó sus actividades, y desarrolló un acuerdo con la empresa Río Tinto. (Le puede interesar:["Presencia paramilitar y minería, una amenaza constante para comunidades afro e indígenas de A. Guayabal"](https://archivo.contagioradio.com/presencia-paramilitar-y-mineria-una-amenaza-consante-para-las-comunidades-indigenas-de-alto-guayabal/))

**A partir de este año los habitantes de la zona han denunciado nuevamente el regreso del interés por la explotación del Cerro,** lo que ha significado un aumento de la presencia militar para permitir el ingreso de personal de la empresa minera (aún desconocida), y el reinicio de sobrevuelos por parte de aeronaves privadas, como el que se realizó el pasado 14 de agosto y aterrizó en el Cerro. (Le puede interesar: ["Indígenas de A. Guayabal desplazados y confinados por grupos armados"](https://archivo.contagioradio.com/enfrentamiento-entre-grupos-armados-crisis-humanitaria-y-166-personas-desplazadas-en-alto-guayabal/))

\[video width="640" height="352" mp4="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/08/WhatsApp-Video-2019-08-26-at-5.48.11-PM.mp4"\]\[/video\]

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
