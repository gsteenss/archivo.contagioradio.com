Title: Aplazan traslado de Feliciano Valencia a centro de armonización
Date: 2015-10-14 14:43
Category: DDHH, Nacional
Tags: Derechos Humanos, Feliciano Valencia, Líder Indígena Feliciano Valencia, Lugares de armonización, ONIC, Radio derechos Humanos
Slug: aplazan-traslado-de-feliciano-valencia-a-centro-de-armonizacion
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/10/feliciano-valencia-734.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto:Elpais.com.co 

###### 14 oct 2015

Después de que el pasado 13 de octubre se ordenara el traslado del líder indígena Feliciano Valencia por parte de un juez especializado en Popayán, **el día de hoy una apelación de última hora aplazó el dictamen** y la situación será resuelta ahora por el Tribunal Superior.

Esta situación se presentó después de que el abogado del cabo Jairo Danilo Chaparral, a quien se le aplicó el ritual de castigo por infiltrar la minga indígena del 2008, **reclamara que el resguardo indígena al que sería trasladado Valencia no cumple con los requisitos de la normatividad vigente** para cumplir la condena a 16 años de cárcel.

Frente a estos hechos Miller Hormiga, en un comunicado de la Organización Indígena de Colombia (ONIC) indicó que “la apelación por parte de representante del militar e integrante del ministerio de defensa es una apelación sin argumentos y lo que busca es dilatar y demorar este proceso”, ya que “todos los requisitos y las pruebas se presentaron”.

Hormiga afirmó que esperan que “el Tribunal **revise los soportes y los documentos** aportados, y que **tenga en cuenta la posición del Ministerio Público** y la no apelación por parte de la Fiscalía, además el fallo de primera instancia y en virtud de esto tome la decisión que reafirme la decisión del juez”.

Así mismo la ONIC indica que distintos **medios empresariales** están “desinformando sobre el sitio de armonización” y aclara sobre estos lugares que:

“Para los **pueblos indígenas los lugares de armonización**, son los sitios que tienen una connotación espiritual para los Nasa, es el **lugar físico donde se remedia los  actos de desarmonía que se cometen en las comunidades**. Allí el sabedor espiritual o medico tradicional juega un papel muy importante para ayudar a curar esa enfermedad que le ha llevado a causar  el delito, así poderlo devolver a su familia, a su comunidad a sus territorio. La persona después de la armonización espiritual realizada por el medico tradicional inicia un proceso de formación político organizativo que permita curar el pensamiento, es reubicar a la persona en el proceso organizativo. Desarrollar la vivencia espiritual, volver a los usos y costumbres como indígenas. Una de la parte fundamental es encontrarse consigo mismo”.
