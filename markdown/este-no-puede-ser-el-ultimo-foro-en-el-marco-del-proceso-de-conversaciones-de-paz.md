Title: “Este no puede ser el último foro” en el marco del proceso de conversaciones de paz
Date: 2016-02-09 12:13
Category: Nacional, Paz
Tags: ELN, FARC, Foro Fin Conflicto, Juan Manuel Santos, Plebiscito, Proceso de conversaciones de paz, Universidad Nacional
Slug: este-no-puede-ser-el-ultimo-foro-en-el-marco-del-proceso-de-conversaciones-de-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/01/foro-universidad-nacional-paz.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: UNAL] 

<iframe src="http://www.ivoox.com/player_ek_10372932_2_1.html?data=kpWgmZedd5Ohhpywj5aYaZS1k5iah5yncZOhhpywj5WRaZi3jpWah5yncYa5k4qlkoqdh6bn1cqY0NSPtNbZxcqY1crWb8bgjIqwlYqmhc3oytLcjcvTttCZppeSmpWJfaWfxtOYx9GPscLmxNSYxsrQcYarpJKw0dPYpcjd0JC%2Fw8nNs4yhhpywj5k%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Abilio Peña, Comisión de Justicia y Paz] 

###### [09 Feb 2016]

Uno de los grandes consensos que se han elaborado hasta el momento en lo que va corrido del [Foro Fin del Conflicto y mecanismos de refrendación](https://archivo.contagioradio.com/arranca-ultimo-foro-sobre-el-fin-del-conflicto-e-implementacion-de-acuerdos/) es que, en cuanto a esos mecanismos, deben promoverse espacios de participación más amplia que un plebiscito como lo plantea el gobierno nacional Según el defensor de DDHH Abilio Peña hay un sentimiento generalizado en que no se ha podido participar de manera más efectiva.

### **Se requiere un escenario distinto al plebiscito** 

El plebiscito es muy cerrado, muy puntual y se necesita que la gente participe ampliamente, por ello se cree que la constituyente puede ser un mecanismo en el que se discuta desde las regiones. “Mucha gente no se siente en un proceso de paz” por eso, más allá de los foros deben darse espacios reales donde la gente “pueda sentarse a discutir” señala Peña, integrante de la Comisión de Justicia y Paz.

### **Se necesita un trabajo pedagógico del proceso de Paz** 

Más allá de la información que se conoce, según el defensor de DDHH, hay “desinformación” de los contenidos de los acuerdos. Sin un trabajo pedagógico no se puede hablar de participación de la ciudadanía. El gobierno es el responsable de abrir espacios democráticos y amplios. “El problema es que el gobierno está ensillando la bestia sin haberla amarrado” por eso sería indispensable ese proceso pedagógico. “Hay mucha desinformación en las discusiones” pero la responsabilidad no es de las personas, es responsabilidad del gobierno, “ni siquiera las personas designadas para moderar la discusión en las mesas tiene la información completa”.

### **Se necesita otro espacio para discutir la doctrina de seguridad** 

Según el defensor de DDHH, para hablar del paramilitarismo “no hace falta leerse un informe” porque la gente lo siente a diario, “han salido con nombres propios”, las denuncias en el sentido de la permanencia de ese fenómeno, no como “bandas emergentes” sino como paramilitares.

Algunas de las propuestas de los asistentes surgen en torno a que en los lugares en donde se han comprobado por testimonios u otros medios se haga “remoción”  de las estructuras policiales y militares que actúan en la región. Así mismo es necesario que la tecnología se ponga al servicio del esclarecimiento de la proveniencia de las amenazas “si se aportan las placas, los número de teléfono” es necesario que a gente sepa de dónde provienen las amenazas y se obligue a las FFMM y de policía a actuar en contra de las estructuras paramilitares que las profieren.

Abilio Peña agrega que, si no se hace una revisión de la doctrina de seguridad que manejan las fuerzas militares, es muy difícil que se construyan garantías reales. El problema de la doctrina es que habla de la necesidad de trabajar en conjunto entre las FFMM y los ciudadanos y esa es la base del paramilitarismo, por ello debe propiciarse un espacio amplio para esa reflexión.

Durante el segundo día del foro se discuten en mesas los mecanismos de refrendación y hacia el final de la tarde se espera una socialización de algunas de las propuestas que se harán llegar a la mesa de conversaciones de la Habana.
