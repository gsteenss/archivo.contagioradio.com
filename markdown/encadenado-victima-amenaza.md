Title: Encadenado víctima exige protección del Estado
Date: 2017-09-30 08:31
Category: Nacional, yoreporto
Tags: Bogotá, Paramilitarismo, víctimas
Slug: encadenado-victima-amenaza
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/09/atado-e1506775875122.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto:CAJAR 

###### 30 Sep 2017 

\#YoReporto

José Joaquín Martínez, se encuentra encadenado a manera de plantón desde el 26 de Septiembre, frente al edificio Avianca, donde funciona la Unidad para la Atención y Reparación Integral a las Víctimas. Este ciudadano se encuentra incluido dentro del registro único de víctimas, desde el 20 de agosto de 2014.

Martínez manifestó que trabajó en el puerto de Buenaventura, donde días atrás se había incautado un cargamento de narcóticos perteneciente a uno de los dos grupos paramilitares dominantes de la zona “los urabeños” y “la empresa”. Motivo por el cual fue amenazado por esta última organización, quien lo señaló de ser informante de la policía, dándole 15 minutos para abandonar la ciudad.

El ciudadano se trasladó de ciudad en ciudad hasta llegar a la capital, donde fue partícipe de la toma de la plaza de Bolívar, lugar en el cual, el gobierno manifestó ayudar a la población desplazada, llegando a un acuerdo de ayuda en proyectos productivos, viviendas y la respectiva indemnización.

Como consecuencia de las amenazas migró forzadamente a la República del Ecuador, donde tras haber sido víctima de un atentado se vio obligado a retornar a Bogotá y al no encontrar ninguna respuesta por parte de las entidades, respecto del incumplimiento de lo convenido en el 2014 con la presidencia de la república, decidió encadenarse manifestando su desconcierto ante la incapacidad institucional y la negligencia burocrática para dar solución a la problemática.

\[KGVID\]https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/09/IMG\_5290.mp4\[/KGVID\]
