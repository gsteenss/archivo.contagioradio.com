Title: Animales en Colombia, objetos y no sujetos de derechos
Date: 2020-01-24 14:25
Author: CtgAdm
Category: Animales, Entrevistas
Tags: Animales, Barranquilla, Chucho, osos de anteojos
Slug: animales-en-colombia-objetos-y-no-sujetos-de-derechos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/01/0c2991e1-967e-4d5e-9314-e76333a7ed8b.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/01/8c15e044-669c-4917-b49f-d904c854e542.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/01/05-feb2018-osezcnoantioquia.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/01/EPDzwD9WsAE4SdH.png" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: @Roberto\_Trobajo

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

La Sala Plena del alto tribunal denegó este 23 de enero la ponencia presentada por la magistrada Diana Fajardo en la que solicitaba el *habeas corpus* a animales, a lo que la corte respondió con 7 votos en contra y solo 2 a favor, aclarando así que **los animales son objetos de derecho y no sujetos**.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

La solicitud se hace luego de que el abogado Luis Domingo Gómez presentó el caso de un oso de anteojos Andino del zoológico de Barranquilla, más conocido como ***"Chucho"* del cual se pedía la liberación mediante el *habeas corpu*s** (tendrás tu cuerpo libre)**, acción que se aplica en los casos de detención ilegal a personas**.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Procedimiento judicial que no pudo ser aplicado al caso de Chucho por no considerarse dentro del sistema jurídico como sujeto de derecho, el cual solo aplica legalmente para personas; teniendo en cuenta que ante la ley el oso es un objeto de derechos, no un sujeto consciente de lo que representa esa libertad.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### "A Chucho se le condenó a estar en cautiverio solo con por el delito de vivir"

<!-- /wp:heading -->

<!-- wp:paragraph -->

Chucho es un oso de anteojos que nace en 1994 en la reserva natural La Planada, junto a otros tres ozesnos, donde cuatro años después fue donado junto a su hermana a la Reserva Ecológica Río Blanco, con el fin de aumentar el indice de natalidad de la especie. (Le puede interesar: <https://archivo.contagioradio.com/narino-promueven-la-proteccion-animal/>)

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Tiempo después su hermana muere, y desde allí según el abogado Luis Domingo ***"empieza un descuido hacia chucho"***. En 2017 es entregado por Corpocaldas al zoológico de Barranquilla nuevamente con propósitos reproductivos; ante ellos y por **la avanzada edad del oso los científicos afirman que su nivel de fertilidad no es optimo**.

<!-- /wp:paragraph -->

<!-- wp:quote -->

> *"Llega a compartir una celda de 200 metros cuadrados con una osa que lleva ahí muchos años, a Chucho se le condenó a vivir en cautiverio solo con por el delito de vivir, por eso defendí su libertad" .*
>
> <cite> Luis Domingo Gómez </cite>

<!-- /wp:quote -->

<!-- wp:image {"id":79714,"sizeSlug":"large"} -->

<figure class="wp-block-image size-large">
![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/01/8c15e044-669c-4917-b49f-d904c854e542-1024x498.jpg){.wp-image-79714}  

<figcaption>
Foto : Luis Domingo Gómez

</figcaption>
</figure>
<!-- /wp:image -->

<!-- wp:heading {"level":3} -->

### La decisión de la Corte hacia los animales deja un mensaje de desesperanza

<!-- /wp:heading -->

<!-- wp:paragraph -->

Según Gómez hay que ser conscientes que el modelo de protección animal en Colombia resulta muchas veces deficiente con respecto a las situaciones de algunas especies, ***"estamos hablando de un ser sensible, consciente de su libertad,** es ahí cuando uno se pregunta ¿Cuál es el papel de la Corte frente a los derechos de los animales?.*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por último declaró que el estado de salud de chucho actualmente es bueno, *"ha habido una mejoría respecto a como se encontraba en la reserva, hay mejoría física, pero emocionalmente claro que no está bien".* (Le puede interesar: <https://archivo.contagioradio.com/las-practias-turisticas-que-mas-danan-los-ecosistemas-segun-pueblos-indigenas-de-la-sierra/>)

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Así mismo el abogado reconoció que *"hubo una decisión por parte de la Corte, pero no hubo justicia*", y afirmó que es consciente de que el modo en el que fue criado Chucho sumado a sus 25 años de edad le impedirían vivir en un ambiente salvaje. *"yo no pido una liberación a campo abierto, soy consciente que el creció acostumbrado a comer de la mano del hombre, solo pido unas condiciones de vida diferentes".*

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### El caso del ozesno Remedios

<!-- /wp:heading -->

<!-- wp:paragraph -->

Remedios es un ozesno de anteojos de aproximadamente 2 años, nacido en los bosques del municipio de remedios en Antioquia, y quien fue recogido por campesinos a los 3 meses, cuando se alejó de su familia luego de ser asustada por el sonido producto de la tala de árboles.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El mismo día el 23 de diciembre del 2017 fue rescatado por funcionarios de la Corporación Autónoma Regional de Antioquia (Corantioquia), y llevado por orden del Director al zoológico de Santa Fé. (Le puede interesar: <https://www.justiciaypazcolombia.com/el-infierno-en-la-tierra-australia-se-quema-de-costa-a-costa/> )

<!-- /wp:paragraph -->

<!-- wp:image {"id":79715,"sizeSlug":"large"} -->

<figure class="wp-block-image size-large">
![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/01/05-feb2018-osezcnoantioquia.jpg){.wp-image-79715}  

<figcaption>
Foto: MinAmbiente

</figcaption>
</figure>
<!-- /wp:image -->

<!-- wp:paragraph -->

Tres días después se desarrolló un consenso con diferentes entidades entre ellas Corantioquia, personal técnico del zoológico y especialistas de la Universidad Corporación para Estudios en la Salud, quienes debatieron sobre la posibilidad de realizar la readaptación de Remedios para regresar a su habitad.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Acción que según la tutela requería de una adecuación de infraestructura, seguimiento, monitoreo y tiempo que debía ser asumido por el Zoológico; *"esto ocurrió hace 2 años, tiempo en el que ha sido sometido a exhibición en el zoológico, prefieren mantenerlo en cautiverio antes que en brindarle un adecuado proceso de rehabilitación e incorporación".*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por último la tutela solicitó la revisión a la acción que niega el *Habeas Corpus* a favor de Remedios, de cara a que el osezno debió ser rehabilitado de manera inmediata, *"entre más se demore la administración en realizar dicha rehabilitación menos será la posibilidad de regresar a su hábitat".*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Ante estas situaciones el abogado concluyó con que es momento para reinventarse los zoológicos *"sin someter a prisión a tantos seres vivos , que su único delito fue en muchos casos caer en las manos de algún traficante"*.

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
