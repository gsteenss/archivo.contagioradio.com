Title: Lo nuevo entre las incertidumbres
Date: 2018-01-15 08:30
Category: Camilo, Opinion
Tags: diálogos con el ELN, ELN
Slug: lo-nuevo-entre-las-incertidumbres
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/FIRMA-DEL-ACUERDO-DE-PAZ-4-e1504831395382.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

######  

<div dir="auto">

###### Foto: Contagio Radio 

<div dir="auto" style="text-align: justify;">

#### **Por [Camilo De Las Casas](https://archivo.contagioradio.com/camilo-de-las-casas/)**

###### 15 Ene 2018

Estamos en el ocaso de Santos y en la oportunidad de crecer en lo nuevo. Los tiempos de incertidumbre se abren como oasis entre la guerra militar y el aceleramiento de la crisis del cambio climático, y la fase del modelo neoliberal extractivista en consolidación .  
Un poder conciente ciudadano se urde entre la ceguera y la estrechez de los ideologismos y pragmatismos existentes, entre un congreso mayoritariamente de lentejos o enmermelados por Santos, Vargas Lleras, y Uribe, y unas expresiones distintas o alternativas apegadas a egos y espejismos.  
Es poco el tiempo de aprobación de leyes hacia bases de paz con justicia ambiental suscitando la incertidumbre sobre el presente futuro.  
En el congreso habrá más esguinces a la paz con las FARC. Allí de la militarización política del ELN se validaran restricciones de derechos y discursos de guerra. Los trámites de leyes de urgencia evitaran unas AGC enunciando la verdad y sometida al silencio conveniente del establecimiento.  
Y finalmente, como borregos del corporativismo, ese congreso legislara medidas para liberar de condicionamientos ambientales al poder corporativo, consolidando las decisiones financieras ya aprobadas.  
Hoy el escenario electoral es de incertidumbres. Las fragmentaciones de la derecha y las de lo demócrata alterno, liberalismo social e izquierda. Es muy aventurado saber quién de la derecha será elegido como presidente, y si habrá coalición entre las egolatrías liberales y de izquierda.  
El país nacional ve un poco más y en estas semanas duda de la derecha retrógrada, lo que podría limitar la certeza de asegurar sus mayorías en el congreso.  
La incertidumbre se alimenta por una paz acordada con las FARC con posibilidades, para  que en medio de la pax neoliberal, se logre avanzar en  ciudadanos concientes de la protección de derechos rurales y participación politica un poco más amplia que están en el Acuerdo  
Esa misma incertidumbre en posibilidad de profundización crítica de lo nuevo se alimentaría, si se reabre un cese de fuego bilateral entre el gobierno y el ELN, intentando lograr en algún punto un Acuerdo de fondo, intentando asegurar algún mecanismo de continuidad con el nuevo gobierno, por ejemplo con el Consejo Nacional de Paz. Reabrir esa Mesa para deslegitimar la guerra militar abierta y encubierta expresada en el escenario electoral a favor de la derecha es obrar en coherencia con la ética de la vida.  
En ambos casos, FARC y ELN, es necesaria  la autocrítica a la estrategia de comunicación y a la ausencia de apertura a otros actores sociales que podrían oxigenar prácticas y planteamientos que puede ser ciertos,  pero que desconocen la vida de otros de a pie, distintas de sus mundos de referencia ideológica militante.  
En la incertidumbre hay que superar la ingenuidad y extrañeza. Desde Bolívar hasta hoy, la clase dirigente ha incumplido, ha mentido y ha traicionado lo acordado.  
Romper con el complejo de éste tipo de Estado colombiano, él que existe, es un reto en el escenario abierto por el Acuerdo del Teatro Colón y por la continuidad de la guerra militar por diversos medios por el establecimiento y el ELN.  
Así asumimos de una vez por todas que lo nuevo nace en la concreción de los derechos por construcción propia.  
Este tipo de Estado existente por quiénes lo regentan y dirigen sigue siendo para unos y estos a su vez para las corporaciones. Este Estado solamente se relaciona con los otros, con las mayorías con represión múltiple y con seducción.  
Estaba previsto al desarme de las FARC, trampas y obstáculos. El conjunto del establecimiento está acostumbrado a ciertas concesiones,  son eso concesiones, para sostener lo que existe. Eso mismo es el cese de fuego con el ELN, una conquista echada a menos, por seguir creyendo en el Estado Es tiempo de cumplir lo que corresponde y crear lo que nos merecemos, lo nuevo.  
Seguir llamándonos a creer que cumplen a cabalidad es parte de los espejismos. Ha pasado con las víctimas organizadas y desorganizadas, con las diversas expresiones del movimiento social. Reconocer ese tipo de Estado posibilita la generación de conciencia de lo nuevo, dialogar, pactar, exigir sin esperar, es aportar hacia el desmoronamiento de lo viejo. Poder conciente  
El poder conciente  asume que exige al Estado, firma con el Estado, este cumple algo (concesión), enmaraña lo acordado, miente y continúa en lo suyo. El poder conciente transformante es de todo lo excluido, asumiendo que  la realidad del alma de ese establecimiento es así que mata, que miente, que incumple.  
Y solo cuando lo nuevo sea real, no el ideologismo o las narrativas alternativas, sino los micropoderes transiten de la palabrería a concreción de derechos en medio del modelo global, vendrá la paz.  
Lo nuevo que posibilita las incertidumbres es que lo desconectado se una,  que se supera la esquizofrenia ciudadana dicha en su Estado actual en otro Estado, otro tipo de gobierno, hay sentido para avanzar, aquí y ahora.  
Esta sabiduría del poder conciente es la superación del trauma esquizoide creado por la clase dirigente, asumido por todos los demás, incluso la izquierda.  
Sin construir lo nuevo en el alma, los estados de agobio y de fragmentación, nos llevaran a seguir creyendo que el Estado de Derecho existente, resolvera lo que me y nos corresponde.  
Lo nuevo es creación. Así, lo expresó claramente el maestro Eduardo Umaña Mendoza, asesinado por ese Estado de Derecho, en abril próximo 20 años de su presencia de otro modo, la salida está en el pueblo organizado, no hay más. Y ese pueblo es todo lo excluido que desde el deseo de la inclusión se abre entre las incertidumbres, siendo reconociendo, asumiendo su lugar en la historia.

</div>

<div dir="auto" style="text-align: justify;">

#### [Leer más columnas de opinión de  Camilo de las Casas](https://archivo.contagioradio.com/camilo-de-las-casas/)

------------------------------------------------------------------------

######  Las opiniones expresadas en este artículo son de exclusiva responsabilidad del autor y no necesariamente representan la opinión de Contagio Radio.

</div>

</div>
