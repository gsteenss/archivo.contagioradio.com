Title: Los saldos pendientes con las víctimas de la Masacre de Bojayá
Date: 2017-05-05 16:18
Category: DDHH, Nacional
Tags: Acuerdos de La Habana, Masacre de Bojayá
Slug: los-saldos-pendientes-con-las-victimas-de-bojaya
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/05/bojaya-entrega-Cristo-Negro-117.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [05 May 2017] 

Cada dos de mayo, las víctimas de la masacre de Bojayá se reúnen para conmemorar, con actos culturales y ancestrales, la memoria de las personas que murieron allí, y continuar con la lucha que **los sobrevivientes han emprendido para exigirle al Estado, paramilitares y las FARC-EP, justicia, verdad y reparación**.

Leider Palacios, víctima de la masacre de Bojayá, señaló que aún hay deudas pendientes que saldar en la búsqueda de la verdad. Una de ellas tiene que ver con la inoperancia del Estado, **que 15 años después abre los procesos para iniciar la exhumación de cuerpos de posibles víctimas de la masacre que terminaron enterradas en municipios aledaños.**

La masacre de Bojaya, se dio en medio de enfrentamientos entre la guerrilla de las FARC-EP y Paramilitares, con el conocimiento de las Fuerzas Militares, no obstante, Palacios expresó que la justicia no ha actuado ingualmente con los 3 responsables “**hoy conocemos que han existido fallos en relación a la condena de algunos guerrilleros, de niveles razos**” sin embargo, frente a las responsabilidades del Estado y de los grupos paramilitares no hay ningún avance.

Frente a la reparación, Leiner afirmó que aún están a la espera de las indemnizaciones y denunció que la atención a las víctimas tampoco ha sido adecuada ya que todavía hay 158 personas que no han sido atendidas por el Gobierno Nacional “las filas de la miseria en Quibdo son tristes, **se siguen asesinando a los jóvenes, hijos de las personas muertas en las masacres desde el 97**”.

De otro lado, las FARC-EP entregó la reconstrucción de un Cristo negro, que se encuentra ubicado en La Loma, muy cerca de Bojaya, como parte de los compromisos de reparación y perdón al daño que ocasionaron a esta población y **como garantía de cumplimiento con el resto de acciones reparadoras que implementarán. **Le puede interesar: ["El Cristo Negro una muestra de perdón, esperanza y paz en Bojayá"](https://archivo.contagioradio.com/el-cristo-negro-bojaya/)

### **El camino de la reconstrucción de Bojayá** 

Para Leider Palacios víctima de la masacre y líder de la comunidad, la conmemoración de la masacre de Bojaya, siempre es triste porque se le vienen a la memoria “las tragedias vividas”, sin embargo, afirmó que también significa un año más para con **“ánimo, fuerza, seguir caminando por los derechos de las personas”**.

Luego del desplazamiento, Palacios indicó que la comunidad de Bojaya ha hecho diferentes esfuerzos por recuperar la dignidad y no olvidar lo sucedido, **con actos de memoria que se realizan cada dos de mayo** y acompañamiento a las familias que perdieron a sus seres queridos.

De igual forma, Palacios señaló que se ha iniciado un trabajo por reconstruir los lazos sociales ya que luego de estos 15 años, **algunas familias han decidido retornar a Bojaya**, además agregó que tras el fenómeno de la guerra las indíenas y afro han podido compartir mucho más e iniciar intercambios culturales. Le puede interesar:["Víctimas de Bojayá y la Chinita exigen participación en pacto por la paz"](https://archivo.contagioradio.com/victimas-de-bojaya-y-la-chinita-exigen-participacion-en-pacto-por-la-paz/)

Actualmente el Comité por los Derechos de las Víctimas de Bojaya ha realizado una serie de asambleas comunitarias, en donde **las víctimas diseñan los planes a partir de las necesidades más importantes que afronten para trabajar en ellas**, labor que ha contribuido a que personas que estaban en condiciones de desplazamiento, retornen al territorio.

Finalmente Leider Palacios, hizo un llamado al gobierno Nacional, para que preste una atención urgente al Bajo Atrato debido a la inseguridad, **producto del accionar del paramilitarismo, que ha dejado como consecuencia el desplazamiento de comunidades**. Le puede interesar: ["Un mes y medio completan 400 familias desplazadas del San Juan del Litoral"](https://archivo.contagioradio.com/un-mes-y-medio-completan-400-familias-desplazadas-del-san-juan-del-litoral/)

<iframe id="audio_18526340" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_18526340_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
