Title: Movimientos sociales de Alemania exigen parar asesinatos de líderes en Colombia
Date: 2017-10-24 14:55
Category: El mundo, Movilización, Nacional
Tags: Alemania, asesinato de defensores, colombianos en alemania, Derechos Humanos, lideres sociales
Slug: grupos-sociales-de-colombianos-en-alemania-exigen-respeto-por-la-vida-en-el-pais
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/10/colombainos-alemania-e1508872749732.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: El Espectador] 

###### [24 Oct 2017] 

Colombianos y grupos de solidaridad con Colombia que residen en Alemania, enviaron un comunicado al Gobierno Nacional y a la opinión pública rechazando** los asesinatos de campesinos y líderes sociales en Colombia**. Manifestaron su preocupación ante la falta de medidas del Estado para garantizar la seguridad y la vida de los ciudadanos.

En el comunicado, expresaron que **es preocupante que se persiga, criminalice y estigmatice** a los líderes sociales de un lugar que “está en lucha por la construcción de una sociedad en democracia y paz”. Igualmente, indicaron que “resulta aún más preocupante que, en algunos casos, los hechos pudieran haber sido perpetrados por las mismas fuerzas de seguridad del Estado o, bajo la complicidad de éstas”. (Le puede interesar: ["Colombia sin garantías en la defensa de los derechos humanos"](https://archivo.contagioradio.com/colombia-sin-garantias-en-la-defensa-de-los-derechos-humanos/))

De acuerdo con Adriana Yee, miembro de uno de los grupos de solidaridad con Colombia, en Alemania hay colombianos que han tenido que salir en busca de oportunidades o exiliados pero que **no han dejado de hacer un seguimiento** a la situación en el país. Allí, han generado iniciativas de participación para poner en evidencia los problemas que afronta Colombia en la actualidad.

### **En Octubre han sido asesinados más de 20 campesinos, líderes sociales y excombatientes de las FARC** 

Ante el panorama que está viviendo Colombia, pues **sólo en octubre han sido asesinadas más de 20 personas**, éstos grupos le han exigido al Gobierno Nacional que “se investiguen los asesinatos y las masacres que han ocurrido y que se sancione a los responsables para que no queden en la impunidad”. Además, le han exigido que se respete el ejercicio de oposición y se brinden garantías para el respeto de la vida.

También, solicitaron que se reconozca la **sistematicidad en las violaciones a los derechos de humanos** de los diferentes líderes pues consideran que, a pesar del acuerdo y la desmovilización de diferentes grupos armados, “los asesinatos continúan de forma generalizada y reiterada”. (Le puede interesar: ["Delegación asturiana denuncia la reparamilitarización del territorio colombiano"](https://archivo.contagioradio.com/delegacion-asturiana-denuncia-la-reparamilitarizacion-del-territorio-colombiano/))

### **Partidos políticos y parlamentarios alemanes están participando de las iniciativas de visibilización del problema** 

Alemania, como país garante y acompañante del proceso de paz, ha hecho pronunciamientos **“explícitamente sobre la situación de derechos humanos** y de asesinatos a líderes y lideresas en Colombia”. Por esto, los grupos sociales allá han venido trabajando de la mano del gobierno alemán “para que ejerza presión al Gobierno colombiano”.

Finalmente, aseguraron que la violación a los derechos humanos de los colombianos “pone en evidencia **el incumplimiento del Acuerdo de Paz**, en relación a las garantías para ejercer la oposición y la protesta para el respeto de los territorios en búsqueda de una ruralidad nueva, justa y respetuosa de las comunidades e individuos que la habitan”.

[Comunicado Asesinatos - Uxpaz y Red (1)](https://www.scribd.com/document/362503828/Comunicado-Asesinatos-Uxpaz-y-Red-1#from_embed "View Comunicado Asesinatos - Uxpaz y Red (1) on Scribd") by [Contagioradio](https://www.scribd.com/user/276652802/Contagioradio#from_embed "View Contagioradio's profile on Scribd") on Scribd

<iframe id="doc_49088" class="scribd_iframe_embed" title="Comunicado Asesinatos - Uxpaz y Red (1)" src="https://www.scribd.com/embeds/362503828/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-UszvfqWwahvkAmE70H75&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="0.7080062794348508"></iframe><iframe id="audio_21660970" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_21660970_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### **Rec**iba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)

<div class="osd-sms-wrapper">

</div>
