Title: ¿Qué es ser afro en Colombia y en América?
Date: 2015-05-30 15:26
Category: DDHH, Nacional
Tags: Poder Ciudadano
Slug: ser-afrodescendiente-en-colombia-y-en-america-latina
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/05/no_mas_al_acismo.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: eluniversal 

<iframe src="http://www.ivoox.com/player_ek_4570761_2_1.html?data=lZqkkpyadY6ZmKialJqJd6KplJKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRaaSmhqezs9qJh5SZop6Yx9iPt8bmjMbT1NSPqc%2BfpNTZ0dLGrcKZk6iY25DJsoy1zoqwlYqlfdPdxMaYrsbYrc%2FVhpizj4qbh4630NPhw8zNs4zGwsnW0ZCRaZi3jpk%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Piedad Córdoba, Movimiento Poder Ciudadano] 

###### [29 may 2015] 

En el marco del inicio del Decenio Afro en Colombia, que tendrá lugar entre el 2015 y el 2025, se llevará acabo el Foro Ciudadanía, Etnia y Territorio los días 30 y 32 de mayo, en el Centro de Memoria, Paz y Reconciliación en Bogotá.

Según sus organizadores, el objetivo del Foro es crear un espacio de encuentro de la población afro, para tratar los temas, dinámicas, problemáticas e ideas de acción que les conciernen, ¿Qué es ser afro en Colombia?,  ¿Qué significa ser afro en América Latina?

Para la defensora de Derechos Humanos, Piedad Córdoba, el decenio afro y el reconocimiento de uno de los grupos poblacionales más importantes de Colombia debe implicar también la concreción de hechos y acciones para su bienestar, y la participación real en escenarios decisorios sobre los destinos del país.

"El racismo es estructural, porque se ejerce desde el Estado", afirmó Córdoba, quien recordó que en Colombia un gran porcentaje de población desplazada por la violencia es afro; y que, aunque existen 2 curules afros para el Senado, actualmente son ocupados por personas no-afrodecendientes.

El evento contará con la participación de James Counts Early, Director de Estudios Culturales y Comunicación en Washington; Jesus Escobar, Presidente del Instituto Nacional contra la Discriminación Racial en Venezuela; El Secretario General de la UNASUR, Ernesto Samper, entre otros.

\[caption id="attachment\_9430" align="aligncenter" width="1316"\][![Foro: Ciudadanía, Etnia y Territorio](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/05/20150529_131941-1.jpg){.size-full .wp-image-9430 width="1316" height="2062"}](https://archivo.contagioradio.com/ser-afrodescendiente-en-colombia-y-en-america-latina/20150529_131941-1/) Foro: Ciudadanía, Etnia y Territorio\[/caption\]
