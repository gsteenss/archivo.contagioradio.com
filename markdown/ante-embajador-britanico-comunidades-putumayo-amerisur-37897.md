Title: Ante embajador británico comunidades denuncian afectaciones de petrolera Amerisur
Date: 2017-03-17 14:30
Category: Ambiente, Nacional
Tags: Amerisur, comunidades indígenas, Putumayo
Slug: ante-embajador-britanico-comunidades-putumayo-amerisur-37897
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/03/reunion-putumayo.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Comisión de Justicia y Paz] 

###### [18 Mar. 2017] 

Ante una delegación de la Embajada Británica encabezada por **el embajador Peter Tibber,** comunidades indígenas y campesinas del departamento de Putumayo denunciaron las **graves afectaciones socioculturales y ambientales que están siendo generadas por la empresa Amerisur,** con la exploración sísmica y la explotación petrolera llevada a cabo en sus territorios.

Durante varias horas, **el embajador Tibber escucho de voz de las comunidades la importancia de la tierra y los ecosistemas** que existen en sus territorios y que son vistos como fuente de vida. De igual modo, le contaron cómo el territorio es concebido para ellos y ellas como un todo en el que el subsuelo, el suelo y el espacio cosmogónico hace parte de el.

**Mario Erazo, Gobernador de la comunidad Siona de Putumayo** relató “la empresa **Amerisur continúa en su labor y el ruido afecta mucho al territorio, al agua, al espacio y muy profundamente a nuestra espiritualidad”**.

Según relata Erazo, el embajador mostró todo su respeto frente a las aseveraciones entregadas por las comunidades “le dijimos que **como pueblo siona no queremos ninguna intervención de ninguna clase de multinacional** en nuestros territorios porque hemos sufrido impactos fuertes”.

El Gobernador Erazo aseguró que todas las inquietudes y sugerencias fueron entregadas al embajador británico “**no solamente somos los pueblos indígenas los que nos vemos afectados, sino también los pueblos afrodescendientes y campesinos.** Todos manejando el mismo lineamiento de la protección a los territorios, porque la afectación es muy dura para nuestra vida”. Le puede interesar: [Amerisur pone en riesgo la pervivencia del pueblo ZioBain](https://archivo.contagioradio.com/amerisur-pone-riesgo-la-pervivencia-del-pueblo-ziobain/)

Por su parte, el embajador británico le hizo un llamado a las comunidades a llevar a cabo un diálogo constante con los representantes de la multinacional y con el Gobierno nacional, por lo que Erazo agregó “nosotros estamos siendo muy claros, y hemos dicho que **nosotros no estamos diciendo en este momento sentémonos y dialoguemos para acordar algo, porque eso ya lo hemos realizado dentro de una consulta previa**”. Le puede interesar: [Comunidad indígena en Putumayo dijo NO a actividad petrolera de Amerisur](https://archivo.contagioradio.com/comunidad-indigena-en-putumayo-dijo-no-a-actividad-petrolera-de-amerisur/)

Consulta que se realizó en el Resguardo Buenavista pero que no fue amparada por el Gobierno nacional dado que los entes que debían hacer presencia en el lugar “nunca llegaron, **esa consulta la hicimos solos y bajo consentimiento nuestro y dijimos no a la sísmica.** Ahora lo que estamos haciendo es ratificando bajo todos los comunicados que ya dialogamos, acordamos y concretamos que no. Ahora exigimos que nos respeten el derecho a la vida y el territorio” añadió Erazo.

Para las comunidades, la visita de la delegación británica fue “algo muy valioso e importante porque es primera vez que un embajador llega a esos lugares donde está la empresa y las afectaciones. **Sentimos que el embajador con nuestra presencia puso mucho cuidado y sentimos que el puede hacer algo por nosotros** (…) le pedimos que no se olvide más de nosotros, pase lo que pase siempre este más cerca” concluyó Erazo.

Desde hace más de 10 años Amerisur hace presencia en el territorio de Putumayo y realiza actividades alrededor de varios lugares donde habitan comunidades indígenas, afrodescendientes y mestizas, generando afectaciones como contaminación de fuentes hídricas, perdida de fauna y flora, fractura a los territorios y contaminación de ecosistemas de humedales, entre otros. Le puede interesar: [Amenaza ambiental por perforación de petrolera Amerisur en Putumayo](https://archivo.contagioradio.com/empresas-petroleras-y-exctractivistas-en-colombia-ponen-en-riesgo-el-ambiente/)

[Comunicado Visita Embajador Británico](https://www.scribd.com/document/342214058/Comunicado-Visita-Embajador-Britanico#from_embed "View Comunicado Visita Embajador Británico on Scribd") by [Contagioradio](https://es.scribd.com/user/276652802/Contagioradio#from_embed "View Contagioradio's profile on Scribd") on Scribd

<iframe id="doc_26028" class="scribd_iframe_embed" src="https://www.scribd.com/embeds/342214058/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-fyr0jW4l6rPK551PHrw1&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="0.7729220222793488"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio]{.s1}](http://bit.ly/1ICYhVU)[.]{.s1}

<div class="ssba ssba-wrap">

</div>
