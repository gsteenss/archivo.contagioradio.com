Title: Estudiantes de Uni. Cauca denuncian posible desalojo violento por parte del ESMAD
Date: 2018-11-22 12:10
Author: AdminContagio
Category: DDHH, Educación
Tags: Alerta temprana, Desalojo, ESMAD, estudiantes, Popayán
Slug: estudiantes-de-la-uni-cauca-denuncian-un-posible-desalojo-violento-por-parte-del-esmad
Status: published

###### [Foto:UNEES Popayán] 

###### [22 Nov 2018] 

Estudiantes de la Universidad del Cauca enviaron una alerta temprana por el posible desalojo del campamento en el que se encuentran en la sede la Institución en Popayán, debido a la alta presencia de integrantes del **Escuadrón Móvil Anti Disturbios, Policías uniformados y de civil, además de ambulancias.**

Edison Valencia, estudiante de la institución, aseguró que esta orden porviene desde la Alcaldía municipal y pone en riesgo la seguridad de **más de 200 estudiantes, docentes y **[**administrativos**] que se encuentran actualmente acampando en sus instalaciones.

"En este momento, hemos interlocutando con el comandante Falla de la Policía Metropolitana de Popayán y manifiesta que hay orden de desalojo para las vías en las intermediaciones de la Facultad de Humanidades. Ya hizo presencia Personería municipal y Defensoría del Pueblo" señaló Valencia, objetando** que no confían en ellas porque fueron quienes autorizaron el desalojo del pasado 8 de noviembre del que resultaron estudiantes heridos**.

Valencia aseguró que en el pliego local de exigencias para levantar el paro, una de las peticiones es la presencia del Alcalde y el Gobernador para resolver la financiación de la institución, **pero hasta el momento solo habrían enviado a personas que no tienen capacidad de decisión**. (Le puede interesar: ["Las 3 condiciones de los estudiantes para levantar el paro"](https://archivo.contagioradio.com/condiciones-levantar-paro-nacional/))

### **Persecución al Movimiento Estudiantil** 

La comisión de derechos humanos de la Universidad del Cauca, denunció que desde el inicio del paro estudiantil han detectado la presencia de policías infiltrados, que "inclusive han entrado al lugar en donde esta el campamento", **y la persecución a varios de los estudiantes que conforman esa instancia.**

Frente a la situación de las y los estudiantes que fueron agredidos por el ESMAD, durante a movilización del 8 de noviembre, Valencia aseguró que una de las personas, que sufrió una fractura de tabique, no ha recibido atención oportuna, **hay otro estudiante que ya fue intervenido quirúrgicamente y los demás se recupera de forma estable.**

<iframe id="audio_30264262" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_30264262_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
