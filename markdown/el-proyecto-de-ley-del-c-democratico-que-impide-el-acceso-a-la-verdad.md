Title: El proyecto de ley del C.Democrático que impediría el acceso a la verdad
Date: 2018-08-17 16:09
Category: Paz, Política
Tags: Alfredo Beltran, Centro Democrático, JEP, Jurisdicción Espacial de Paz, Patricia Linares
Slug: el-proyecto-de-ley-del-c-democratico-que-impide-el-acceso-a-la-verdad
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/08/heraldo.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: El Heraldo: El Heraldo] 

###### [17 Ago 2018] 

El Centro Democrático radicó un proyecto de ley en el que **prohíbe a los organismos del Sistema de verdad, justicia, reparación y no repetición, que soliciten información que sea considerada de interés estatal sobre operaciones militares**, integrantes de la Fuerza Pública, entre otros. Además, propone sancionar a quienes entreguen dichas informaciones reservadas.

Los motivos, según el Centro Democrático, para hacer esta prohibición, es que según ellos esas solicitudes podrían arriesgar la seguridad nacional o de cualquiera de sus agentes. (Le puede interesar:["Los dudosos entornos del Centro Democrático"](https://archivo.contagioradio.com/dudosos-entornos-del-centro-democratico/))

<iframe src="https://co.ivoox.com/es/player_ek_27906867_2_1.html?data=k5ymkpucepihhpywj5aVaZS1kpeah5yncZOhhpywj5WRaZi3jpWah5yncbHV1dfWxc7Fb63dz8bfx9iJdqSfsdfS1c7Iqc_owpDRx5DQpYy-prWah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ..&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

Para Patricia Linares, presidenta de la Jurisdicción Especial para la paz, esa propuesta de acto legislativo, para que este escenario pueda desarrollar su accionar “obviamente” tiene que acceder a todo tipo de información, respetando y garantizando su manejo y agregó que **“a ningún juez de la República se le puede negar el acceso a la información que requiera para el cumplimiento de sus funciones”**, ya que este hecho podría atentar contra la autonomía de los jueces.

<iframe src="https://co.ivoox.com/es/player_ek_27906888_2_1.html?data=k5ymkpucfJmhhpywj5WcaZS1kZaah5yncZOhhpywj5WRaZi3jpWah5yncaLa08rR0ZCmqc3o04qwlYqldc-Zk6iYx93UtsbnysnS0NnJb8XZjNHOjajTttXZjKjc0NjYrdXpxM7c0MaRaZi3jqjc0NnFq8rjjLfOxs7Tb46ZmKialg..&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

De igual forma, el expresidente de la Corte Constitucional, Alfredo Beltrán, señaló que el proyecto del Centro Democrático, no solo está en contravía del acto legislativo 001 de 2017, que crea la JEP, sino que, además, **para lograr su aplicación sería necesaria una reforma a la constitución**. Sin embargo, manifestó que esto solo sería posible en un contexto en el que el proyecto se apruebe.

Asimismo, Beltrán recordó que el acto legislativo 001 de 2017 también ordena que los próximos 3 gobiernos deben someterse a dar estabilidad a los Acuerdos de paz porque el pacto involucra al gobierno.

![Proyecto de ley](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/08/WhatsApp-Image-2018-08-17-at-3.28.37-PM-549x800.jpeg){.alignnone .size-medium .wp-image-55805 width="549" height="800"}

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
