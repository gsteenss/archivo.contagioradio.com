Title: Gobierno Duque está en absoluta ignorancia sobre la política de drogas
Date: 2018-09-05 08:35
Author: AdminContagio
Category: DDHH, Nacional
Tags: Dosis Mínima, Iván Duque, TNI
Slug: gobierno-duque-ignorancia-drogas
Status: published

###### [Foto: @GloriaMBorrero] 

###### [4 Sept 2018] 

**Ricardo Vargas, investigador asociado al Transnational Institute (TNI)** y experto en materia de drogas afirmó que las recientes declaraciones de miembros del Gabinete Duque sobre la fumigación de cultivos de uso ilícito con glifosato y la regulación de la dosis mínima, prueban que la actual administración **"está en una absoluta ignorancia sobre el tema de drogas".**

Para Vargas, volver a **la fumigación aérea con glifosato sería un retroceso en materia de política de drogas,** "porque desde diferentes perspectivas se ha demostrado que no es el mecanismo óptimo para reducir áreas de cultivo", y si acaso tiene unos alcances temporales, los efectos son altamente nocivos para el ambiente, la sociedad y la economía de las regiones.

El experto afirmó que **no está demostrado que la fumigación aérea reduzca el área de cultivos de uso ilícito sembrados**, porque hay periodos de intensa fumigación en que hay mayor crecimiento del número de hectáreas sembradas; como paso cuando la Coca se movió de Putumayo a Nariño, y "entre más fumigaban, más crecía el número de cultivos".

Por otra parte, hay ocasiones en que no se están realizando fumigaciones y se presenta una reducción en las plantaciones, por lo tanto, para el investigador es fundamental hacer un "balance más riguroso", que permita entender el problema en toda su dimensión y actuar adecuadamente para enfrentarlo. (Le puede interesar:["Aspersión terrestre con glifosato es absurda: Camilo Gonzalez Posso"](https://archivo.contagioradio.com/aspersion-terrestre-con-glifosato-es-absurda-camilo-gonzalez-posso/))

### **"Debería construirse un acuerdo de confianza con las comunidades"** 

Sin embargo, Vargas reconoció que el actual Gobierno tiene la razón al señalar que el modelo de sustitución voluntaria que se acordó en la Habana es inútil, costoso e inocuo, porque ha demostrado que no sirve. En lugar de ello, debería construirse un acuerdo de confianza con las comunidades, para **que haya un proceso de inversión con enfoque territorial y étnico, que les permita encontrar otros medios de subsistencia.**

Adicionalmente, los enfoques para asumir el tema de drogas desde la política pública no han tenido en cuenta la protección que se debe brindar a las comunidades interesadas en reducir las áreas de siembra, ni a las grandes estructuras del narcótrafico que se mueven en el mercado internacional, que "es realmente el que sostiene el negocio de la droga".

### **"Si la persona demuestra que es un adicto, se le devuelve la droga": Gloria Borrero.** 

Vargas aseguró que se han creado unas narrativas absurdas sobre la cuestión de la droga que no responden a la realidad del problema, por ejemplo, **"hay una corriente liderada por  la Fiscalía que cree que el incremento de la producción de coca en Colombia, ligado a un incremento en las incautaciones hechas en el país, implica que esta droga se está quedando en el territorio nacional".** (Le puede interesar:["Fumigaciones no deberían ser el tema de la Fiscalía sino mecanismos de combate al narcotráfico"](https://archivo.contagioradio.com/fumigaciones-no-deberian-ser-el-tema-de-la-fiscalia-sino-mecanismos-de-combate-al-narcotrafico/))

Ante el anunció de la ministra de justicia, Gloria Borrero sobre el decreto con el que se buscará regular la dosis mínima, según el cual "si la persona demuestra que es un adicto, que lo puede hacer con el testimonio de los papás o una opinión médica, se le devuelve la droga"; el experto dijo que es un enfoque equivocado para tratar a los consumidores.

**"Eso supone que todo el que consuma psicoactivos está enfermo, es decir, hay gente que consume de forma recreativa y eso no es una enfermedad.** Enfermo es el que se vuelve dependiente y necesita casi que fisiologicamente la dosis", sostuvo Vargas. Además, el investigador recordó que la Corte Constitucional ya se ha referido con claridad sobre la fumigación aérea con Glifosato y la dosis personal, y cambiar tales determinaciones sería inconstitucional.

###### [Reciba toda la información de Contagio Radio en] [su correo](http://bit.ly/1nvAO4u) [o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por] [Contagio Radio](http://bit.ly/1ICYhVU). 
