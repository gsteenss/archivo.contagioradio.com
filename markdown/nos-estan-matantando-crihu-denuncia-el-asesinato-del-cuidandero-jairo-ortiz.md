Title: "Nos están matantando", CRIHU denuncia el asesinato del Cuidandero Jairo Ortiz
Date: 2019-12-23 12:50
Author: CtgAdm
Category: Comunidad, Nacional
Tags: asesinato, guardia indígena, Huila, Líder Social Asesinado
Slug: nos-estan-matantando-crihu-denuncia-el-asesinato-del-cuidandero-jairo-ortiz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/Captura-de-Pantalla-2019-12-23-a-las-11.33.24-a.-m..png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:ArchivoPersonal] 

Este domingo 22 de diciembre, por medio de un comunicado, el Consejo Regional Indígena del Huila (CRIHU), denunció el asesinato del Cuidandero Jairo Ortiz, quien protegía y residía en el Resguardo Nasa de Huila Rionegro. (Le puede interesar: [Líder indígena, Genaro Quiguanas fue asesinado en Santander de Quilichao](https://archivo.contagioradio.com/lider-indigena-genaro-quiguanas-fue-asesinado-en-santander-de-quilichao/))

El hecho se registró a las 5:30 de la madrugada de este domingo cuando Ortiz salía de su lugar de trabajo en el centro de Rionegro, y fue seguido por hombres armados que se desplazaban  en una moto; el Cuidandero quien conducía una camioneta intentó huir, pero el vehículo fue interceptado en la estación de gasolina, donde los atacantes dispararon en reiteradas ocasiones hacia el carro; dos de estos proyectiles impactaron en la cabeza del indígena causando su muerte.

Los disparos a tan temprana hora, generaron conmoción en la comunidad, quienes anunciaron que  tomarán acciones para encontrar a los perpetradores de este crimen; asímismo mientras integrantes del CRIHU se desplazaban hacia la estación de gasolina, lugar donde quedó la camioneta del Cuidandedor; se registraron dos heridos con arma de fuego, uno de ellos el Coordinador Regional de Cuidadores, Laureano Yandi quien se movilizaba en su vehículo, y Orlando Finscue Perdomo quien conducía el vehículo de CRIHU. Hasta el momento se desconoce quienes son los atacantes.

> [\#SOS](https://twitter.com/hashtag/SOS?src=hash&ref_src=twsrc%5Etfw) nos están matando cuidandero del territorio [\#CRIHU](https://twitter.com/hashtag/CRIHU?src=hash&ref_src=twsrc%5Etfw) <https://t.co/usFSnsBPdX> [pic.twitter.com/kbqkr3yYPs](https://t.co/kbqkr3yYPs)
>
> — CRIC Colombia Cauca (@CRIC\_Cauca) [December 22, 2019](https://twitter.com/CRIC_Cauca/status/1208777920769970176?ref_src=twsrc%5Etfw)

<p>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
</p>
Mediante el comunicado la comunidad denunció que la vida del pueblo Nasa está en riesgo y que su tranquilidad se ha visto fracturada por el hostigamiento continuo de grupos armados en su territorio, "*Hacemos el llamado a los territorios indígenas del Departamento del Huila a coordinar acciones en defensa de la vida y del territorio"*, declararon.

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
