Title: Respuesta de Mineducación no es nada solidaria con las víctimas: Lorena Beltrán
Date: 2016-07-14 16:09
Category: Mujer, Nacional
Tags: Brasil, cirugía plástica, Lorena Beltrán
Slug: respuesta-de-mineducacion-no-es-nada-solidaria-con-las-victimas-lorena-beltran
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/07/Lorena-Beltran-e1468516756158.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: You Tube 

###### [14 Jul 2016] 

Luego de que en el Congreso se hundiera el proyecto de Ley que buscaba garantizar la seguridad en las cirugías plásticas, las víctimas de este tipo de procedimientos mal hechos, sufren un nuevo golpe.

Esta vez se trata la Ministra de Educación, Gina Parody quien no reconoció el error en las convalidaciones de cirujanos plásticos que tramita el Ministerio de Educación, y que ha dejado cientos de víctimas que confían en estos doctores avalados por el propio gobierno. Para la ministra el problema que se ha venido presentando con las cirugías** “no tiene que ver con convalidaciones sino con la integridad y ética médica”.**

“**La respuesta de Gina Parody no es nada solidaria con las víctimas y además es insuficiente** y aunque nos sentimos abandonadas por el Estado vamos a continuar en nuestra lucha”, expresa Lorena Beltrán, periodista y líder de la campaña \#CirugíaSeguraYa, "El Ministerio de Educación no puede escudarse en un simple examen de legalidad, sino que debe debe exigir un examen de idoneidad que debería ser avalado por el Ministerio de salud, **pido una respuesta más integral para las víctimas**".

Durante la rueda de prensa donde se reveló el resultado de una investigación sobre la convalidación de títulos de cirugía plástica a médicos que tomaron cursos en Brasil,  Álvaro Flores, de la Comisión Nacional Intersectorial de Aseguramiento de la Calidad de la Educación Superior, Conaces, y quien lideró la investigación concluyó que los títulos que el **Ministerio de Educación le otorgó a los médicos que llevaron a cabo cursos en la Universidad Veiga de Almeida en Brasil si eran convalidables**.

Lo anterior, pese a que la Asociación Colombiana de Sociedades Científicas, la Sociedad Colombiana de Cirugía Plástica, la Universidad Nacional, la Universidad de Antioquia y las asociaciones médicas brasileñas han insistido en que **“esos cursos express no son equiparables a estudios profesionales, poniéndose es un riesgo la salud de los pacientes colombianos”**, explica Lorena Beltrán, “¿Usted se dejaría operar por un cirujano que hizo un curso semipresencial?” y añade “Si un médico profesional me hubiera atendido no me habrían puesto gelatina sin sabor en mis heridas”.

Además, cabe recordar que los cirujanos que han realizado estos cursos en Brasil, han dicho que no vivían en Brasil mientras estudiaban, sino que viajaban a tomar cursos.

Por el momento, las víctimas como Lorena, aseguran que la lucha continúa, y que ahora seguirán acompañando el **proyecto de Ley que el Ministerio de Salud busca revivir para que haya cirugía segura**, aunque no están de acuerdo que sea la misma propuesta que, por contar con algunos vacíos, fue tumbada en el congreso.

### El caso de Lorena Beltrán 

Lorena, fue víctima del doctor Francisco Sales Puccini y del Estado Colombiano luego de haberse realizado una mamoplastia de reducción en julio de 2014, confiando en la convalidación de título del cirujano proferida por el Ministerio de Educación. Tras su procedimiento,  la periodista empezó a ver uno de sus  pezones completamente negro y el tejido empezó a necrosar.

Para corregir esa situación, Puccini le recomendó a Beltrán realizarse un segundo procedimiento en 2015, pero eso solo agravó su situación pues perdió la sensibilidad en gran parte de ambos senos. **Como las heridas no cerraban  y se abrían huecos en varios puntos la solución de Puccini le aplicó gelatina sin sabor en la herida,** empeorando sus cicatrices, por lo que hoy desafortunadamente Lorena no podría amamantar por el daño que sufrió en sus senos, sin hablar de las cicatrices que quedarán en su alma luego de lo que puedo significar esto para su vida personal.

<iframe src="http://co.ivoox.com/es/player_ej_12225946_2_1.html?data=kpeflJqdeJehhpywj5WbaZS1k56ah5yncZOhhpywj5WRaZi3jpWah5ynca3j08rbw5Cmqc3o04qwlYqldc-Zk6iYssrWrdDYhqigh6aot9XVjoqkpZKns8_owszW0ZC2pcXd0JCah5yncZU.&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>
