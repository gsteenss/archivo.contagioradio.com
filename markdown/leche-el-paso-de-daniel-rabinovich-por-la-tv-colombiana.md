Title: “Leche” el paso de Daniel Rabinovich por la Tv colombiana
Date: 2015-08-21 17:35
Author: AdminContagio
Category: 24 Cuadros, Cultura
Tags: Bernardo Romero Pereiro, Daniel Rabinovich, Daniel Samper Pizano, Jorge Maronna, Leche serie Caracol, Les luthiers
Slug: leche-el-paso-de-daniel-rabinovich-por-la-tv-colombiana
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/08/Rabinovich-e1440194794989.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Fotograma: "Leche", Caracol Televisión 

###### [21, Ago, 2015] 

Entre el año 1995 y 1996, eran muchos de colombianos que se sentaban frente a la pantalla del televisor para cumplir la cita semanal con “Leche”, la miniserie de Caracol Televisión, nacida de la mente y pluma de Daniel Samper Pizano, Bernardo Romero Pereiro y Jorge Maronna, bajo la dirección de Victor Mallarino.

Una historia de vacas humanizadas, mineralianos, de castos e inocentes amores, donde buenos y bandidos entonaban canciones y rimas en una suerte de musical televisivo, con la imprenta indiscutible de Maronna, resultante de su larga y prolífica trayectoría con el grupo argentino de humor "Les Luthiers".

Dentro de la amplia baraja de personajes célebres, (la niña Susana, su papito Fermín, Zoroastro el "Mineraliano", la fiel Dispepsia, Alejandro y Acercandro, etc, etc...), "Leche" presentó, durante algunos episodios, a Oscár (con acento al final), un matón que vestido al mejor estilo "gangster" del cine policiaco, se encargó de hacer el trabajo sucio de Jorge, némesis de los protagonistas de la serie.

En los capítulos que apareció en pantalla Oscár (Daniel Rabinovich), logró robarle protagonismo a los personajes centrales de la historia (interpretados por Flora Martínez y Juan Carlos Vargas), con su marcado acento, astucia malévola y por su habilidad para el canto y el baile.

<iframe src="https://www.youtube.com/embed/-10NHbTrvmk" width="420" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

El paso de Maronna y de Rabinovich por nuestra pantalla chica, vivirá en la memoria de aquellos que pudieron disfrutar de ese experimento audiovisual que fue "Leche", un proyecto imposible de homologar, y que con el paso del tiempo se ha convertido en serie de culto para una generación.
