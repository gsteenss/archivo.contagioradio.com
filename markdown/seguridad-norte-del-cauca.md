Title: Preocupante situación de seguridad en Suárez, Norte del Cauca
Date: 2019-09-01 18:45
Author: CtgAdm
Category: Comunidad, Nacional
Tags: Cauca, Norte Cauca, seguridad, víctimas
Slug: seguridad-norte-del-cauca
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/03/cauca-.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:] 

En las últimas horas al Norte del Cauca, en el municipio de Suárez, habitantes de la vereda El Amparo, reportan la aparición tres víctimas fatales, dos mujeres y un hombre los cuales según los testigos fueron atacados con arma de fuego.(Le puede interesar:[Líderes del Cauca esperan que ONU se lleve una visión real de lo que sucede a nivel territorial ](https://archivo.contagioradio.com/lideres-del-cauca-esperan-que-onu-se-lleve-una-vision-real-de-lo-que-sucede-a-nivel-territorial/)

Los habitantes de Suárez afirman que las víctimas no son residentes del sector, por esta misma razón aún se desconoce su identidad o la razón del ataque. Las autoridades de la zona ya están encargados de las investigaciones, por ahora esta es toda la información.

Noticia en desarrollo

 

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
