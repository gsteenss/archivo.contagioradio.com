Title: Juez declara ilegal captura de Lina Jiménez, victima de montaje judicial
Date: 2020-11-24 13:28
Author: AdminContagio
Category: Actualidad, DDHH
Tags: #CasoAndino, #Falsospositivosjudiciales, #Fiscalía, #LinaJiménez, #Montajesjudiciales
Slug: juez-declara-ilegal-captura-de-lina-jimenez-victima-de-montaje-judicial
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/05/lina.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

Juez de segunda instancia declaró ilegal la captura de Lina Jiménez Numpaque, quien fue recapturada el pasado 16 de octubre. El hecho se dio luego de que otro juez ordenara su libertad por vencimiento de términos. En esta ocasión la **libertad inmediata tendría que ser efectiva** producto del fallo a favor de la joven.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Jiménez hace parte del grupo de personas judicializadas en el caso conocido como caso Andino, en donde a un conjunto de 10 personas se les acusó de ser los autores del atentado perpetrado en el año 2016 al centro comercial Andino. Sin embargo, tres años después la Fiscalía continúa sin sustentar su teoría acerca de la responsabilidad de los acusados. (Le puede interesar: ["Colectivo Libres e inocentes"](https://www.facebook.com/LibreseInocentes))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por esta situación 7 de las personas quedaron en libertad por vencimiento de términos. En el caso de Lina Jiménez e Iván Ramírez, esa orden se generó el pasado 16 de octubre; pero luego de **un conjunto de acciones arbitrarias por parte del INPEC**, según denunciaron los familiares, un juez ordenó la recaptura de ambas personas

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El fallo de este 24 de noviembre en el caso Lina Jiménez, solicita la libertad inmediata para la mujer por declarar ilegal la captura. Se espera que en el transcurso de las próximas horas, ese hecho se haga efectivo. (Le peude interesar: "[La lucha de Lina Jiménez, víctima de montaje judicial de caso Andino, por su libertad](https://archivo.contagioradio.com/la-lucha-de-lina-jimenez-victima-de-montaje-judicial-de-caso-andino-por-su-libertad/)")

<!-- /wp:paragraph -->
