Title: Sin atención en salud internos de 19 cárceles de Antioquia
Date: 2015-08-05 15:31
Category: DDHH, Nacional
Tags: Antioquia, Cárceles en Colombia, Comité de Solidaridad con los Presos Políticos, Defensoría del Pueblo, INPEC
Slug: sin-atencion-en-salud-internos-de-19-carceles-de-antioquia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/08/carcel-colombia-contagioradio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### Foto: cronistadiario 

###### [5 Agosto 2015]

Según un informe de la Delegada para la política criminal y penitenciaria de la Defensoría del Pueblo, **desde el pasado 27 de Julio no hay atención en salud para los internos de 19 establecimientos penitenciarios del departamento de Antioquia**. Según la Defensoría, el problema radicaría en inconvenientes con los contratos para el personal médico que atiende a los internos.

La situación tiende a agravarse por la presencia de 4 casos de Tuberculosis en la cárcel de Itaguí y 3 en la cárcel de Bellavista que no han recibido ninguna atención médica por parte de la empresa Caprecom y que podrían propagar una epidemia según indica la entidad estatal.

La defensoría también constató el precario estado de salud en el que se encuentra un interno que ingresó con tutor externo por herida de proyectil en una de sus piernas, sin que le hayan hecho curaciones ni se haya retirado el implante 4 años después de realizado el procedimiento. También hay varios pacientes con úlceras en su piel, las cuales presentan claros síntomas de infección, sin ningún tipo de tratamiento.

### **Hacinamiento asciende al 300% en algunos establecimientos penitenciarios** 

Por su parte el Comité de Solidaridad con los Presos Políticos denunció que hay 3 casos graves en la Carcel “Tramacua” de Valledupar. Uno de ellos José Gregorio Jiménez Hoyos, quien sufre de pólipos nasales, Jonathan Rodríguez Valencia denunció que hace 18 días no recibe atención médica por un acceso que le salió en la cara y Víctor Hugo Correa Ortiz decidió entrar en huelga de hambre desde el 17 de julio pasado, cociéndose la boca.

La crisis carcelaria ha sido denunciada por diversas organizaciones de DDHH desde hace más de 2 años y hasta el momento no se han tomado las medidas necesarias para mitigar los efectos del hacinamiento que, en algunos casos, alcanza el 300%, la atención en salud y la falta de agilidad en los procesos judiciales en contra de las personas detenidas.
