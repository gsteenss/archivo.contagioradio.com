Title: “Palabras que matan” la serie documental sobre defensores de DD.HH. en Colombia
Date: 2017-04-07 16:28
Category: DDHH, Nacional
Tags: defensores de derechos humanos, Derechos Humanos, Somos defensores, víctimas
Slug: palabras-que-matan-la-serie-documental-sobre-defensores-de-dd-hh-en-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/04/Sin-título.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: New Media Advocacy Project] 

###### [07 Abr. 2017]

<iframe src="https://co.ivoox.com/es/player_ek_18028406_2_1.html?data=kp2dlJ2YdJehhpywj5aUaZS1kZaah5yncZOhhpywj5WRaZi3jpWah5yncazV08rbja3JtsbYysaSlKiPksbrjLLSxs7Fb6LY19TQw8jdb7Hm0M_SxdmPcYarpJKw0dPYpcjd0JC_w8nNs4yhhpywj5k.&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

Este 9 de abril, se estará realizando a través de redes sociales **el lanzamiento de “Palabras que matan”, una serie documental que cuenta con 4 capítulos que narran la historia de 3 organizaciones sociales** y varios defensores y defensoras de derechos humanos, así como víctimas que han sido estigmatizadas por su labor en pro de la defensa y la garantía de los derechos humanos.

Karen Heredia, productora de esta serie e integrante de New Media Advocacy Project aseguró que “esto fue **un trabajo que duró aproximadamente 6 meses**, de entrevistas y trabajo de investigación". Le puede interesar: [Gobierno debe garantizar seguridad para líderes y defensores de DDHH](https://archivo.contagioradio.com/gobierno-debe-garantizar-seguridad-para-lideres-y-defensores-de-ddhh/)

En esta serie **se cuentan las historias del Colectivo de Abogados José Alvear Restrepo, la Corporación Jurídica Yira Castro y la Comisión Intereclesial de Justicia y Paz**, tres organizaciones que han sido atacadas por su trabajo con las víctimas en el país.

“Estas organizaciones hacen una labor muy valerosa, legitima y legal. Manejan muchos de los casos más importantes de derechos humanos en Colombia, incluyendo casos notorios de atrocidades cometidas por agentes del Estado como los militares, pero **también son algunas de las organizaciones de DD.HH. más perseguidas en el país”** aseveró Karen.

El objetivo de esta serie es mostrar y recalcar que, **las acusaciones y difamaciones hacia defensores son parte de una táctica estratégica del Estado** para desprestigiar el trabajo de la defensa de derechos humanos “por estas situaciones los más afectados son las víctimas que representan, por eso queremos informar al público y compartir las experiencias” añadió Karen. Le puede interesar: [Defensores de derechos humanos "Contra las cuerdas"](https://archivo.contagioradio.com/disminuyeron-las-amenazas-pero-aumentaron-los-asesinatos-somos-defensores/)

Con el lanzamiento de esta serie se estará impulsando la etiqueta \#YoCreoEnSuTrabajo para mostrar el **trabajo legítimo que realizan los defensores y defensoras de derechos humanos** con el objetivo de minimizar los ataques que ocurren en las redes y en los medios.

"Le diría al público que cuando hay noticias o ataques a defensores eso se convierte rápidamente en amenazas reales, entonces entre más apoyo tengan los defensores mejor” puntualizó. Le puede interesar: [120 defensores de DDHH asesinados en 14 meses en Colombia: Defensoría](https://archivo.contagioradio.com/120-defensores-asesinados-defensoria/)

### **Introducción de la Serie Documental.** 

<iframe src="https://www.youtube.com/embed/UPb1rO-HnO0" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>  
Ver más [aquí](https://www.youtube.com/channel/UCOJCW6E5Pzj2iIE2xwkfEgQ).

### 

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)

<div class="ssba ssba-wrap">

</div>
