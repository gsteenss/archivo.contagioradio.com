Title: 34 años del Palacio de Justicia: Para nunca olvidar, para no repetir
Date: 2019-11-07 15:57
Author: CtgAdm
Category: eventos, Memoria
Tags: conmemoración, desaparecidos, Palacio de Justicia, víctimas
Slug: 34-anos-del-palacio-de-justicia-para-nunca-olvidar-para-no-repetir
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/WhatsApp-Image-2019-11-07-at-2.56.46-PM.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio  
] 

El pasado miércoles 6 de noviembre el Consejo de Estado hizo un homenaje a las víctimas de la toma y retoma del Palacio de Justicia, ocurrido entre el 6 y 7 de noviembre de 1985. Durante el evento se encontraron 2 sobrevivientes de los hechos y 2 familiares víctimas de desaparición, que a lo largo de una hora hablaron sobre cómo vivieron esas 28 horas que duró la toma y retoma, y cúal es la verdad que han logrado reconstruir 34 años después de los hechos.

### **Una serie de desafortunadas coincidencias** 

En el evento participaron Carlos Betancur Jaramillo, consejero de Estado durante 21 años (1977 a 1997); Nubia González Cerón, relatora del Consejo de Estado; Johana Patricia Angulo, hija de Blanca Inés Ramírez Suárez, quien ejercía como auxiliar integrante del Consejo de Estado y murió en el Palacio; y René Guarín, hermano de Cristina Guarín, asesinada y desaparecida. (Le puede interesar: ["Fiscalía demuestra su falta de compromiso con los desaparecidos del Palacio de Justicia"](https://archivo.contagioradio.com/fiscalia-desaparecidos-palacio-de-justicia/))

En los relatos sobre lo que pasó ese 6 de noviembre de 1985 se encuentra un factor común: las coincidencias que llevaron a cada una de las personas a estar en el Palacio de Justicia ese día. Nulia González explicó que como relatora, en ocasiones visitaba la cárcel La Picota para revisar los anales del Consejo y ese 6 de noviembre había decidido hacerlo. Aunque usualmente ella se tomaba todo el día para estar en la cárcel cumpliendo con esta labor, debido a lo dispendioso que resulta entrar al penal, ese miércoles optó por finalizar su trabajo poco antes del medio día y regresó al Consejo 30 minutos antes que se produjera la toma.

Por su parte, René Guarín relató que su hermana era una mujer de 27 años, que hablaba y leía en francés y tenía la intención de viajar a España en enero de 1986 para estudiar en la Universidad Complutense de Madrid. Para cuando ocurrió la toma, Cristina Guarín había trabajado apenas 36 días en la cafetería del Palacio como cajera, porque hacía el reemplazo de la esposa de Carlos Rodríguez (administrador de la cafetería), que estaba en licencia de maternidad.

Carlos Betancur Jaramillo narró que como presidente del Consejo de Estado participó en una reunión con la policía el 17 de octubre, en que la que les informaron que se había detectado un plan para tomar el Palacio. En ese momento, se discutió la posibilidad de poner vidrios de seguridad en todo el edificio, instalar circuitos cerrados de televisión y ubicar barreras eléctricas, sin embargo, consciente de las limitantes presupuestales para comprar estos elementos, pidió que se aumentara el pie de fuerza que custodiaba la edificación.

El abogado relató que así se hizo, pero ese pie de fuerza fue retirado coincidencialmente tres días antes de la toma, al punto que se especula si la guardia se retiró para permitir la entrada del M-19 al edificio. Otra coincidencia que señaló Betancur es que el Palacio fue tomado media hora antes que se discutiera en la Sala Constitucional el tema de la Ley de Extradición, lo que ha motivado la teoría, creada por Jhon Jairo Velásquez alias 'popeye', de que Pablo Escobar dio dinero al grupo guerrillero para tomar el Palacio y evitar que se aprobara esta normativa.

### **Certezas en el caso 34 años después de la toma y retoma del Palacio de Justicia  
** 

Johana Ángulo, hija de Blanca Inés Ramírez Suárez, quien murió en 1985, señaló que gracias a la sentencia de la Corte Interamericana de Derechos Humanos (Corte IDH) que ordenó, entre otras cosas, exhumar los cuerpos de las personas asesinadas en el Palacio, ella y su familia se dieron cuenta que los restos que les fueron entregados no eran los de su mamá sino los de otra persona. Es decir, que Blanca Inés estuvo desaparecida durante 31 años, hasta que gracias a la Corte IDH fue encontrada en 2016 en una fosa común del Cementerio del Sur. (Le puede interesar: ["Corte Interamericana de DDHH condenó al Estado colombiano por desaparecidos del Palacio de Justicia"](https://archivo.contagioradio.com/corte-interamericana-de-ddhh-condeno-al-estado-colombiano-por-desaparecidos-del-palacio-de-justicia-2/))

En medio de una situación similar, la familia del magistrado auxiliar Jorge Alberto Echeverry Correa se enteró en septiembre de 2017 que los restos que les fueron entregados pertenecían en realidad a Bernardo Beltrán Hernández, mesero de la cafetería. Mientras la familia del magistrado auxiliar Julio César Andrade recibió en junio de 2017 la misma información, al conocerse que los restos que les fueron entregados pertenecían en realidad a los de Héctor Jaime Beltrán Fuentes, también mesero de la cafetería. (Le puede interesar: ["Condena a general (r) Árias Cabrales confirma que sí hubo desapariciones en el Palacio de Justicia"](https://archivo.contagioradio.com/condena-a-general-r-arias-cabrales-confirma-que-si-hubo-desapariciones-en-el-palacio-de-justicia/))

Rene Guarín afirmó que es un hecho que integrantes de la inteligencia militar tomaron el Instituto de Medicina Legal y desvirtuaron algunos de los hechos ocurridos. También recordó que se filtraron grabaciones de militares diciendo que "si aparece la manga que no aparezca el chaleco", refiriéndose a las personas desaparecidas, orden que para él se ha cumplido, porque "nos han entregado pedazos" de familiares. (Le puede interesar: ["Don Héctor abraza hoy a su hijo Héctor Jaime Beltrán, víctima de toma y retoma del Palacio de Justicia"](https://archivo.contagioradio.com/don-hector-abraza-hoy-a-su-hijo-hector-jaime-beltran-victima-de-toma-y-retoma-del-palacio-de-justicia/))

Y por último, el hermano de Cristina Guarín afirmó que el Estado se casó con la teoría del error en la entrega de restos, lo que los ha llevado incluso a asegurar que no hay desaparecidos, pero es un hecho probado por la justicia internacional que sí hay desaparecidos en el Palacio de Justicia. (Le puede interesar: ["¡Sí existen desaparecidos! familiares de las víctimas del Palacio de Justicia"](https://archivo.contagioradio.com/existen-desaparecidos-l-palacio-de-justicia/))

### **"La mejor forma de ayudar a los responsables es propiciar el olvido"** 

Betancur concluyó diciendo que "la mejor forma de ayudar a los responsables es propiciar el olvido y eso hemos hecho en Colombia", por eso, recordó algunas de las preguntas que no han sido respondidas, por ejemplo, por qué la policía fue al anfiteatro el jueves 7 de noviembre de 1985 a recoger los cuerpos de las personas no identificadas, y luego dichos cuerpos aparecieron en el Cementerio del Sur. (Le puede interesar: ["Gobierno incumple reparación en el caso del Palacio de Justicia"](https://archivo.contagioradio.com/gobierno-incumple-reparacion-en-el-caso-del-palacio-de-justicia/))

<header class="entry-header">
</header>
Guarín, desde otra perspectiva señaló que los familiares de los desaparecidos sí quieren perdonar y sí quieren reconciliarse, pero no saben a quién disculpar ni por qué hechos, dado que aún no se conoce la verdad de lo ocurrido. En consecuencia, sostuvo que seguirán buscando la verdad de lo ocurrido y buscando a los desaparecidos. (Le puede interesar:["¿Quién responde por las entregas equivocadas del holocausto palacio de justicia?"](https://archivo.contagioradio.com/quien-responde-por-las-entregas-equivocadas-del-holocausto-palacio-de-justicia/))

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
