Title: Valle del Cauca de luto por el asesinato del líder social Luis Carlos Valencia
Date: 2019-06-15 16:30
Author: CtgAdm
Category: Líderes sociales, Nacional
Tags: asesinato de líderes sociales, Valle del Cauca
Slug: valle-del-cauca-de-luto-por-el-asesinato-del-lider-social-luis-carlos-valencia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/Luis-Carlos-Valencia.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo] 

En horas de la tarde de este 15 de junio se conoció el asesinato de **Luis Carlos  Valencia, líder social de 52 años** del departamento del Valle del Cauca, información preliminar apunta a que su cuerpo fue hallado con cinco heridas ocasionadas por arma de fuego en la **vía Restrepo, zona rural de Agua Mona**, se desconocen los autores y los móviles del crimen.

Asesinado lider social Luis Carlos Valencia de Restrepo, Valle. Mientras Gobierno y uribismo concentran sus esfuerzos en hacerle daño al proceso de paz, continúan matando a los líderes sociales. [pic.twitter.com/SauJnzNr45](https://t.co/SauJnzNr45)

> — Iván Cepeda Castro (@IvanCepedaCast) [15 de junio de 2019](https://twitter.com/IvanCepedaCast/status/1139975277059158017?ref_src=twsrc%5Etfw)

<p>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
</p>
**Fe de errata: **Tras indagar sobre lo ocurrido con el señor Luis Carlos Valencia y comunicarnos con la Alcaldía y Personería del municipio de Restrepo, Valle del Cauca se pudo establecer, hasta donde conocen las entidades, que el hombre asesinado no ejercía labores vinculadas con la defensa de los derechos humanos y que su trabajo, según la comunidad estaba vinculado al préstamo de dinero.

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
