Title: "Extrema derecha no reversará impulso de la paz" FARC y ELN
Date: 2017-05-11 13:25
Category: Nacional, Paz
Tags: dialogos de paz, ELN, FARC, La Habana, paz
Slug: extrema-derecha-no-reversara-la-paz-farc-y-eln
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/05/eln-y-farc.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: El Heraldo  
] 

###### [10 May. 2017]

Luego de dos días de reuniones en La Habana entre **los máximos jefes de la guerrilla de las FARC y el ELN, en los que compartieron los avances** que se han tenido en cada una de las mesas de diálogos de paz, así como las demoras y dificultades que se han presentado en la fase de implementación, a través de un comunicado aseguraron que “estarán unidos por la solución política”.

En el breve comunicado manifiestan que, aunque han encontrado disparidades en los avances de las dos mesas de conversaciones entre la insurgencia y el gobierno del presidente Santos **“mantenemos objetivos comunes, con caminos diversos pero complementarios;** como el de buscar que la sociedad tenga una función protagónica en el logro de la paz”. Le puede interesar: [Proceso de refrendación de acuerdos debe ser conjunto para FARC y ELN](https://archivo.contagioradio.com/proceso-de-refrendacion-de-acuerdos-debe-ser-conjunto-para-farc-y-eln/)

Así mismo, manifiestan que luego de siete décadas de conflicto armado y la intención de alcanzar la paz para Colombia, **seguirán manteniendo a las víctimas en el centro de sus propuestas y debates** con miras a encontrar la verdad plena de tantos años de guerra.

En ese orden de ideas, aseveraron que **asumirán sus responsabilidades en el conflicto,** pero además exigirán a los Gobiernos y a las “clases dominantes” que hagan lo mismo.  Le puede interesar:[ "Sectores de Ultraderecha y empresarios le temen a la JEP" Iván Cepeda](http://Reciba%20toda%20la%20información%20de%20Contagio%20Radio%20en%20su%20correo%20o%20escúchela%20de%20lunes%20a%20viernes%20de%208%20a%2010%20de%20la%20mañana%20en%20Otra%20Mirada%20por%20Contagio%20Radio.)

Añaden que durante la reunión dialogaron sobre **la importancia de exigir que se “saque la violencia de la lucha política”**, para que de tal manera puedan verse avances y resultados bilaterales de lo firmado en los acuerdos. Le puede interesar: [Diálogos con el ELN fortalecen  esfuerzos hechos en La Habana con las Farc](https://archivo.contagioradio.com/dialogos-con-el-eln-fortalecen-esfuerzos-hechos-en-la-habana-con-las-farc/)

Para ello, dicen que trabajarán para que los partidos políticos y sus cabildantes se comprometan con la paz, a propósito de las elecciones que tendrán lugar en 2018 “trataremos de evitar que los llamados a la guerra, que hace la extrema derecha **no hagan reversar este impulso por el logro de un nuevo país con equidad**” manifiestan en el comunicado conjunto. Le puede interesar: [Las FARC-EP y sus posibles coaliciones para las elecciones 2018](https://archivo.contagioradio.com/las-farc-ep-y-sus-posibles-coaliciones-para-las-elecciones-2018/)

Hicieron un reconocimiento a la labor que diversas comunidades hacen en “defensa de la vida y el territorio” y llamaron la atención a la persecución que viven líderes y lideresas en el país **“invitamos a no dejar prosperar la persecución, que pretende cerrar el paso a las fuerzas políticas alternativas,** comprometidas con la paz, la justicia social y la dignidad de todas y todos” manifestaron.

Por último, agradecieron a la comunidad internacional y al Gobierno nacional por facilitar este encuentro, que dijeron si se mantiene como un mecanismo permanente podría aportar para el diálogo y coordinación conjunta.

[Comunicado Conjunto FARC y ELN](https://www.scribd.com/document/348067478/Comunicado-Conjunto-FARC-y-ELN#from_embed "View Comunicado Conjunto FARC y ELN on Scribd") by [Contagioradio](https://es.scribd.com/user/276652802/Contagioradio#from_embed "View Contagioradio's profile on Scribd") on Scribd

<iframe id="doc_54386" class="scribd_iframe_embed" src="https://www.scribd.com/embeds/348067478/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-qJ3W8HxEp0zwYYJkPB31&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="0.7729220222793488"></iframe>
