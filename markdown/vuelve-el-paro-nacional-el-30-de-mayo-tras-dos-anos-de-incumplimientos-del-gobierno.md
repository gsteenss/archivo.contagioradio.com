Title: Vuelve el paro nacional tras dos años de incumplimientos del gobierno
Date: 2016-05-14 13:45
Category: Movilización, Nacional
Tags: 30 de mayo, Cumbre Agraria, Paro Nacional
Slug: vuelve-el-paro-nacional-el-30-de-mayo-tras-dos-anos-de-incumplimientos-del-gobierno
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/05/10-días-para-evita-r-Paro-Agrario..jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: La Razón.co] 

###### [13 May 2016]

El próximo 30 de mayo será la hora cero para el Paro Nacional Minga Agraria, Étnica y Popular, con la finalidad de exigirle al gobierno del presidente Santos el cumplimiento de los acuerdos pactados en negociaciones anteriores y acciones inmediatas frente a las políticas económicas que afectan a las comunidades y territorios.

Este Paro surge como respuesta a los diferentes incumplimientos por parte de Santos a los [pliegos de exigencias pactados anteriormente](https://archivo.contagioradio.com/el-pliego-de-peticiones-del-paro-nacional/) con el movimiento social y a la **profundización de las políticas económicas en los diferentes territorios del país** y se ha construido a partir del diálogo entre las comunidades y organizaciones que hacen parte de la Cumbre Agraria Étnica y Popular.

Estos son los 8 puntos que el gobierno Nacional ha incumplido:

**Tierras, territorios y ordenamiento territorial:** En este punto las comunidades le han exigido al gobierno que sean ellas las que [redefinan como se organiza el territorio, que se reconozca al campesinado como sujeto de derechos,](https://archivo.contagioradio.com/proyecto-de-ley-busca-garantizar-derechos-del-campesinado-en-colombia/)que se reconozcan las zonas de reserva campesina, los territorios indígenas y ancestrales, las zonas agroalimentarias, los territorios afrocolombianos.  Territorios que se ha visto afectados por la implementación de un modelo agroindustrial y mineroenergético que esta generando despojo en los territorios.

**La economía propia contra el despojo:  **Las diferentes organizaciones y comunidades exigen al gobierno que se derogue la ley [ZIDRES que ha promovido inversión por parte de multinacionales y agroindustriales, afectando a los pequeños productores campesinos, las comunidades rurales y sus prácticas culturales](https://archivo.contagioradio.com/zidres-legalizaria-mecanismos-de-acumulacion-de-tierras/). A su vez, plantean la defensa de la soberanía y seguridad alimentaria a través de proyectos institucionales que estén hechos a partir de la realidad de las comunidades. De otro lado expresan que las comunidades también deben participar de la construcción de lo que sería una Reforma Tributaría que incluya las problemáticas de los sectores populares.

**Minería, Energía y Ruralidad:** Las últimas licencias otorgadas en diferentes partes del país a multinacionales para la explotación del subsuelo [han incumplido con los acuerdos sobre la moratoria minera, la obligación de las sentencias de consulta previa a las comunidades](https://archivo.contagioradio.com/existen-14-solicitudes-de-mineria-que-aun-tienen-en-riesgo-a-la-palma-de-cera/) para los proyectos de extracción y explotación del subsuelo, y el replanteamiento de una política minero energética más amigable con la tierra.

**Derechos políticos, garantías, víctimas y justicia:** Las comunidades y organizaciones exigen al gobierno la derogación de la ley de Seguridad Ciudadana, [garantías y respeto a los derechos humanos para las comunidades, debido a las situaciones de hostigamientos que se han presentado en diferentes territorios](https://archivo.contagioradio.com/panfletos-amenazantes-aguilas-negras/) , a su vez exigen que se establezca una política real que confronte [al paramilitarismo que se encuentra activo y amenaza a las comunidades diariamente.](https://archivo.contagioradio.com/audiencia-persistencia-del-paramilitarismo-en-colombia/)

**Paz, Justicia Social y Solución Política: **Las comunidades y organizaciones aplauden y apoyan los avances en el proceso de paz entre el gobierno y la guerrilla de las FARC-EP, sin embargo hacen un llamado a la mesa de diálogos para que se involucre al movimiento social y popular en la discusión del [proceso para construir una paz vinculante y participativa.](https://archivo.contagioradio.com/cuatro-millones-quinientos-mil-deberan-votar-si-en-plebiscito-para-la-paz/)

**Derechos Sociales: **Diferentes sectores del movimiento social han expresado la vulneración a derechos como la educación, salud, trabajo, vivienda digna y recreación en la medida de que no se garantizan a toda la población colombiana y [se profundiza cada vez más la crisis de desfinanciación estatal a hospitales y universidades públicas.](https://archivo.contagioradio.com/hospital-universitario-del-valle-no-atendera-urgencias-por-falta-de-recursos/)

**Relación Campo - Ciudad: **Las comunidades le exigen al gobierno nacional  la defensa del agua como un derecho fundamental y la protección a las fuentes hídircas afectadas por la otorgación de títulos mineros en diferentes regiones donde se encuentran. Además demandan[frenar la venta de empresas públicas como la ETB.](https://archivo.contagioradio.com/asi-fue-el-cacerolazo-no-a-la-venta-de-la-etb/)

**Cultivos de Coca, Marihuana y Amapola: **Las comunidades insisten en que las políticas antidrogas del gobierno Nacional son un fracaso, y decisiones como la [fumigación manual con glifosato solo demuestran la arbitrariedad e improvisación](https://archivo.contagioradio.com/fumigaciones-terrestres-con-glifosato-una-decision-paradojica-y-negligente/)que el mismo tiene frente a la visión y tratamiento de los cultivos de coca, marihuana y amapola. Por ende exigen la creación de programas de sustitución gradual, concertada, estructural y ambiental de estos cultivos.

De acuerdo con Gimmy Moreno, vocero de Congreso de los Pueblos y de la Cumbre Agraria, Étnica y Popular, el **30 de mayo es la hora cero para dar inicio al Paro**, pero la duración de este dependerá de la **voluntad de sentarse a negociar por parte del gobierno y la negociación se hará en caliente, es decir en el marco de la movilización de las comunidades.**

"Como Cumbre Agraria llevamos ya casi 2 años con una mesa de negociación y **el gobierno resume las soluciones en pequeños proyectos** pero en el marco de avanzar en temas más concretos el gobiernos cierra las puertas. La movilización es producto del incumplimiento por parte del gobierno nacional en el tema de la negociación de los 8 puntos del pliego"

[El Paro es convocado por todos los sectores que se agrupan en la plataforma Cumbre Agraria, Étnica y Popular,](https://archivo.contagioradio.com/en-asamblea-general-la-cumbre-agraria-decide-si-hay-nuevo-paro-nacional/) además participarán diversos sectores del movimiento social como las organizaciones estudiantiles. Se espera que el 30 de mayo se realicen movilizaciones en diferentes ciudades del país como **Bogotá, Medellín, Cali, Cucúta, Popayán, entre otras ciudades y regiones del país.**

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
