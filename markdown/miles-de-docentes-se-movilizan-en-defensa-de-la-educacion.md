Title: Miles de docentes se movilizan en defensa de la educación
Date: 2017-02-28 13:04
Category: Educación, Nacional
Tags: educacion, Paro de docentes
Slug: miles-de-docentes-se-movilizan-en-defensa-de-la-educacion
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/02/barrio-adentro.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Noti Barrio Adentro] 

###### [28 Feb 2017] 

Miles de docentes marcha hoy en Bogotá debido al incumplimiento, por parte del gobierno con el pliego pactado durante el 2015 que incluye, pago de deudas retrasadas que han desmejorado el salario, la ausencia de planes integrales para la atención en salud y **el hacinamiento que en muchos colegios ha producido la instalación de la jornada única.**

Profesores y profesoras de todo el país denuncian que las problemáticas continúan y agregan que las políticas que vienen impartiendo el Ministerio de Educación como lo son los convenios, las concesiones o la implementación de la jornada única, **han ampliado el cupo de estudiantes en los diferentes cursos, sin las condiciones suficientes para hacerlo, disminuyendo la calidad de la educación.**

De igual forma, denuncian que los colegios tampoco están adecuados estructuralmente para responder a las necesidades de una jornada única, situación que se ve reflejada en el **hacinamiento de estudiantes, la falta de refrigerios y de comida caliente para el estudiantado.** Le puede interesar: ["Estas son las razones de la movilización en Bogotá"](https://archivo.contagioradio.com/razones-de-la-movilizacion-en-bogota/)

A su vez, rechazan la resolución 1293 de 2016, que de acuerdo con la Asociación Distrital de Educadores, **acabo con el grado prejardín, la educación de Necesidades Especiales y viene acabando paulatinamente con la jornada de la tarde, la educación rural y la de fines de semana. **

En esta ocasión, los docentes radicaran un nuevo pliego de peticiones con 7 puntos: **Política educativa, Carrera Docente, Economía, Bienestar, Salud y Prestacional** e Institucional, que consideran deben ser atendidos de forma inmediata por el Ministerio para garantizar el derecho a la educación. Le puede interesar: ["1800 docentes del Cauca viajan a Bogotá para continuar paro indefinido"](https://archivo.contagioradio.com/1800-docentes-del-cauca-viajan-a-bogota-para-continuar-paro-indefinido/)

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
