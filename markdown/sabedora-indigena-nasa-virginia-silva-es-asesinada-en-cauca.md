Title: Sabedora indígena Nasa Virginia Silva es asesinada en Cauca
Date: 2020-01-10 17:02
Author: CtgAdm
Category: DDHH, Nacional
Tags: asesinato, Cauca, ejercito, grupos armados, indígenas, lideres sociales
Slug: sabedora-indigena-nasa-virginia-silva-es-asesinada-en-cauca
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/01/Nasa-e1502385039236.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:Vice] 

La Asociación indígena de Cabildos Nasa Cxha cxha del Cauca denunció el asesinato de la señora Virginia Silva de 71 años de edad el pasado 7 de enero en la vereda El Canelo Resguardo Indígena de Belalcazar-Páez. (Le puede interesar: [Líder indígena, Genaro Quiguanas fue asesinado en Santander de Quilichao](https://archivo.contagioradio.com/lider-indigena-genaro-quiguanas-fue-asesinado-en-santander-de-quilichao/))

Virginia fue asesinada en presencia de su esposo Jesús Vargas, por hombres armados que ingresaron a su  finca y le dispararon. *"Llegaron hasta la casa donde habitaba la víctima, sujetos desconocidos sin mediar palabras le propinaron 2 tiros en la parte frontal del cráneo y otro impacto en el ante brazo. Provocándole la muerte de manera inmediata*". Señaló Sonia Hurtado Ducue, presidenta de la Asociación de Cabildos Nasa Cxha cxha

Según Hurtado, Virginia Silva había recibido mensajes de amenaza de muerte por parte de grupos armados, el hecho es atribuido por las autoridades a la disidencia de Farc*, "vemos hombres armados que quieren imponer su ley y hacer justicia sobre acciones que creen incorrectas en nuestro territorio". : [No cesa violencia contra resguardos indígenas en Buenaventura](https://archivo.contagioradio.com/violencia-contra-resguardos-indigenas-en-buenaventura/)) *

Asímismo, la lideresa lamentó que los grupos armados justificaran el asesinato de Virginia por sus acciones como sabedora, *“ella era una mujer mayor de la comunidad indígena y tenia saberes sobre rituales espirituales que normalmente se usan para mantener la estabilidad y paz en la comunidad, sin embargo al parecer realizaba rituales que causaban desarmonía familiar y territorial en el resguardo, cosa que no le pareció a los violentos".*

### "Hacemos un llamado a la comunidad internacional para que continué su misión en territorio indígena"

Por medio de un comunicado la Asociación de **Cabildos Nasa Cxha cxha**,  hizo un llamado a la comunidad internacional para que continúe con su misión de observación y verificación, tanto de los acuerdos, como de la crisis humanitaria en  Colombia, y rechazaron las acciones violentas que han venido en aumento en zonas como Tierradentro  en el Municipio de Páez.

Y agregaron,*"Seguimos siendo víctimas de éste atropello que provocan los grupos armados quienes desarmonizan la tranquilidad de los habitantes, además del conflicto y la ola de violencia que se está reviviendo en el país y que el gobierno nacional poca atención le ha puesto"* .  (Le puede interesar: [Ingreso de Ejército a resguardos pone en riesgo vida de indígenas en Tumaco, Nariño](https://archivo.contagioradio.com/ingreso-de-ejercito-a-resguardos-pone-en-riesgo-vida-de-indigenas-en-tumaco-narino/))

### "Los problemas en nuestra comunidad no los soluciona ni el Ejército ni la disidencia, y menos la violencia"

Por otro lado la presidenta de la Asociación denunció la ocupación de un lugar ancestral por parte de integrantes del Ejército, *"La estancia de personal del Ejército ha causado tala de varios árboles y acumulación de basura en este lugar que es sagrado para nosotros, donde nos reunimos para agradecer al sol y hacer rituales de protección y armonía para la comunidad". *

La lideresa explicó que a través de una resolución creada hace dos años pedirán al ejercito abandone el lugar sagrado,  **"*****Hace unos años se había presentado una situación similar, pero fue la misma comunidad quien se encargó de sacarlos, sin embargo, esta vez nos acogemos bajo esta resolución y vamos a presentarla ante la personería"**,* y agregó que hay otras rutas que estos pueden frecuentar, sin atentar con la naturaleza.

Por último, agregó que los problemas de la comunidad *"No los soluciona ni el ejército ni la disidencia"*,  y que ellos tienen protocolos de solución a acciones como las que desarrollaba Silva,  y destacó que *"El verdadero conflicto lo causan ellos con su acciones violentas que atentan contra la población y la naturaleza". *

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
