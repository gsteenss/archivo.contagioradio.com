Title: ‘Hombre Irracional’  la moralidad no graciosa de Woody Allen
Date: 2015-09-05 11:00
Author: AdminContagio
Category: 24 Cuadros
Tags: Cine Colombia, Emma Stone, Irrational man, Joaquin Phoenix, Woody Allen
Slug: hombre-irracional-la-moralidad-no-graciosa-de-woody-allen
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/unnamed-4.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [3, Sep 2015] 

A partir de este 3 de septiembre, llega a las salas de cine del país, la nueva cinta del aclamado director neoyorkino Woody Allen, quien desde hace décadas tiene acostumbrado al público a  estrenar una película cada año.

[![Irrational man](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/unnamed-3.jpg){.aligncenter .wp-image-13348 width="248" height="368"}](https://archivo.contagioradio.com/?attachment_id=13348)

En el 2015, el turno es para ‘Hombre Irracional’, una producción estelarizada por Joaquin Phoenix (Her, Inherent Vice) y Emma Stone (Birdman, The Amazing Spiderman), que cuenta la historia de Abe Lucas, un profesor de filosofía alcohólico, depresivo y autodestructivo, que entabla una relación amorosa con una colega (Parkey Posey) y con su mejor estudiante (Emma Stone).

A partir de allí, una serie de acontecimientos, harán que el profesor Abe vuelva a sentirse vivo por una decisión radical que toma, aunque este hecho pueda tener consecuencias devastadoras.

<iframe src="https://www.youtube.com/embed/2F8HURXLXqc" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

Hombre irracional se proyectó inicialmente durante la más reciente edición del festival de cine de Cannes. En salas comerciales estadounidense su estreno fue en julio y desde este 3 de septiembre el público colombiano podrá disfrutar de la cinta donde "La moralidad no es graciosa".

24 Cuadros de Contagio Radio, estuvo en el pre-estreno de ‘Hombre Irracional’. Compartimos algunos comentarios de los asistentes.

<iframe src="http://www.ivoox.com/player_ek_7823950_2_1.html?data=mJ2flZ6ZdI6ZmKiakpiJd6KllJKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRaZOrqtffw9nNs8%2FVzZDaw9OJdpifzdSY0NrJutCfxcqYudTTqNqfotHZx9ORaZi3jqjc0NnFq8rjjLfOxs7Tb46ZmKialg%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>
