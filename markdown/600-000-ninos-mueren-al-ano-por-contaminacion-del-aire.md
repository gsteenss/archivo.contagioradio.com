Title: 600.000 niños mueren al año por Contaminación del Aire
Date: 2016-11-01 15:27
Category: Ambiente, El mundo
Tags: Contaminación del Aire, muerte infantil, Unicef
Slug: 600-000-ninos-mueren-al-ano-por-contaminacion-del-aire
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/polution-e1478025638346.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Share América] 

###### [1 Nov de 2016] 

El pasado 31 de Octubre el Fondo de Naciones Unidas para la Infancia UNICEF, publicó un informe en el que alerta sobre la situación de 300 millones de niños que viven en lugares donde la contaminación atmosférica supera los limites establecidos por la OMS. **La contaminación del aire, contribuye de forma decisiva a la** **muerte anual de unos 600.000 menores de cinco años en todo el mundo.**

El informe sobre el estudio basado en imágenes satelitales, develó que alrededor de **2.000 millones de niños viven en áreas contaminadas por las emisiones de vehículos, el uso desmedido de combustibles fósiles y actividades relacionadas al extractivismo**. Le puede interesar: [“Día sin carro y sin moto” redujo en un 27% la contaminación en el aire.](https://archivo.contagioradio.com/con-dia-sin-carro-y-sin-moto-se-redujeron-en-un-27-las-emisiones-de-gases/)

UNICEF concluye que los más afectados son los niños del sur de **Asia, Oriente Medio y África, y en el Pacífico.** Sin embargo, ciudades como **Santiago, Lima y Buenos Aires,** son algunas de las que presentan mayor contaminación **en América Latina. **Le puede interesar:[ 2015 año con la tasa más alta de CO2 en la historia.](https://archivo.contagioradio.com/2015-ano-la-taza-mas-alta-co2-la-historia/)

Este estudio, también llama la atención sobre **el daño que la polución hace a los no nacidos,** pues se relaciona directamente con un aumento de la tasa de abortos, nacimientos prematuros, bajas tallas y peso de los bebés y una **considerable reducción de la capacidad pulmonar a un 20%** de los niños que han crecido en estos entornos.

Anthony Lake, director general de la UNICEF, manifestó que "La contaminación atmosférica es el principal factor que contribuye a la mortalidad de niños menores de cinco años anualmente” e instó a que presidentes de todo el mundo asistan a la **COP22 que se celebrará en Marruecos del 7 al 18 de Noviembre** y “empiecen a **implementar medidas efectivas que mejoren la calidad de los ecosistemas y protejan la vida de los niños”.**

###### Reciba toda la información de Contagio Radio en su correo [[bit.ly/1nvAO4u]
