Title: En libertad Iván Ramírez, uno de los detenidos por los atentados en Centro Andino
Date: 2017-06-26 21:33
Category: DDHH, Nacional
Tags: Centro Andino, Falsos Positivos Judiciales, Fiscalía
Slug: ivan_ramirez_libertad_atentados_centro_andino
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/06/capturados_andino-e1498530335509.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Mauricio Alvarado 

###### [26 Jun 2017] 

El joven Iván Darío Ramírez, quien había sido detenido por la explosión en el Centro Comercial Andino, fue dejado en libertad debido a que su captura se dio en el marco de irregularidades y vulneraciones al debido proceso.

De acuerdo con el relato de los hechos, Ramírez fue detenido arbitrariamente a las 3:50 de la tarde el pasado sábado mientras se movilizaba en un bus de transporte público. Luego, fue trasladado a la estación de policía del barrio Monteblanco, localidad de Usme, en razón a la correspondencia de su apariencia física con la de un presunto ladrón, según manifestaron los policiales a sus familiares. Posteriormente, **fue trasladado a una patrulla sin que la familia fuera informada del lugar del traslado.**

Sobre las 7:30 de la noche, fue llevado a su casa custodiado por 2 policías del cuadrante y 5 miembros de Policía Judicial de la SIJIN. Estos manifestaron tener una orden de allanamiento, impidiendo que la familia la leyera. Durante la diligencia llegaron a la residencia familiar 3 vehículos más y sus ocupantes entraron a la casa, sin identificarse, y sin presencia de Ministerio Público. **Aproximadamente a las 2:30 de la madrugada del domingo el joven fue formalmente capturado, pese a haber estado privado de la libertad por casi 11 horas.**

### Inconsistencias 

El relato asegura que en el Informe de Registro y Allanamiento exhibido por la Fiscalía en la Audiencia de legalización de captura, se señala que los policías habrían llegado a las 8:00 de la noche a la casa de Iván y lo habrían encontrado allí, sin embargo unos videos evidencian que Ramírez llegó a la casa ya estando privado de su libertad y custodiado por los policiales.

Se denuncia que la **Fiscalía General de la Nación desconoció los principios del debido proceso al sostener que el joven fue capturado en flagrancia** del delito de Concierto para Delinquir, pues se afirmaba que fue sorprendido con una USB durante el allanamiento. Frente a esa anomalía procesal, y debido a que la defensa del joven lo logró demostrar, la Juez de garantías ordenó la libertad inmediata de Iván.

**La situación del joven sociólogo de la Universidad Nacional** le preocupa a su familia y amigos, debido a que los señalamientos en medios de comunicación del fiscal, quien dio por hecho la culpabilidad de Iván en  los  hechos del Centro Comercial Andino, ponen en peligro la vida de Iván. Debido a eso, solicitan que se respete su "derecho al debido proceso y el cese de los montajes judiciales incitados por la presión mediática, así como **la adopción inmediata de una veeduría de las Naciones Unidas - Derechos Humanos sobre este proceso judicial".**

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 

 
