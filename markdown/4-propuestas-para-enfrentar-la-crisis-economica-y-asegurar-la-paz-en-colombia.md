Title: 4 propuestas para enfrentar la crisis económica y asegurar la paz en Colombia
Date: 2016-01-20 17:52
Category: Economía, Nacional
Tags: economia, Impuestos en Colombia, Reforma tributaria, salario minimo
Slug: 4-propuestas-para-enfrentar-la-crisis-economica-y-asegurar-la-paz-en-colombia
Status: published

###### Foto: contagioradio. 

<iframe src="http://www.ivoox.com/player_ek_10143733_2_1.html?data=kpWelpibd5Shhpywj5aaaZS1lpqah5yncZOhhpywj5WRaZi3jpWah5yncaLnxtjW0MbYs4zYxtGY0srWrdDYytjhw5C2rcTV08ncjanZtoa3lIquk9OPrMLW04qwlYqliMKf1M7R0ZCJdpPkjoqkpZKns8%2FowszW0ZC2pcXd0JCah5yncZU%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Enrique Daza, Justicia Tributaria] 

###### [20 Ene 2016.]

La crisis de los precios del petróleo, el aumento de los precios del dólar que controlan el peso de la deuda externa, el aumento de los impuestos indirectos como el IVA y la aplicación de los tratados de libre comercio, son algunas de las claves de la crisis. Ante ello, desde sectores de organizaciones sociales hacen 4 propuestas.

Establecer un control de cambio al mercado del dólar, frenar la política del libre comercio representada en los TLC’s, remodelar la base arancelaria y aumentar el gravamen de impuestos directos a las industrias extractivas serían las claves para enfrentar la crisis económica. **¿Cómo funcionarían?**

### **La crisis de los precios del Petroleo** 

Desde hace varios gobierno Colombia se montó en la llamada “bonanza petrolera” señala Enrique Daza, integrante de la organización Justicia Tributaria. Según el analista varios gobiernos se dedicaron a dilapidar esos recursos y no se hizo nada para garantizar que los dineros que entraban se convirtieran en inversión productiva. Esta crisis de los precios del petróleo podrían seguir disminuyendo por la entrada al mercado de cerca de 1 millón de barriles diarios.

Así las cosas los dineros en Colombia no van a aumentar por cuenta de la exportación del petróleo y por el contrario, las proyecciones del gobierno para estos años esperaban que los precios internacionales del crudo no disminuyeran por debajo de 80 dólares el barril.

Daza señala que la apertura económica y los TLC no tuvieron nada que ver con el petróleo y tampoco se apostó por la diversificación de la capacidad exportadora del país. “estamos pagando las consecuencias” de las malas políticas económicas de los 4 últimos gobiernos.

### **Precios del dólar seguirán en aumento** 

El precio del dólar es una consecuencia directa de la disminución de los precios del petróleo, pero también de la política económica de los Estados Unidos. Según Daza el precio de la divisa es manejado políticamente por el emisor que es el administrador de la reserva de ese país “tiene una manipulación directa del dólar y nosotros estamos en manos de ellos”.

Por otra parte crece de una manera desmedida el peso de la deuda externa. Colombia tiene su deuda en dólares y los rectores de los mercados internacionales como el FMI y el BM continúan exigiendo el pago de los intereses de la deuda de manera puntual y además emiten medidas económicas para que la austeridad de los gobierno hacia sus ciudadanos permita dicha puntualidad.

### **Las medidas para enfrentar la recesión son medidas recesivas** 

Los remedios que el gobierno está aplicando fomentan más recesión, por ejemplo, una reforma tributaria que aumenta el valor del IVA y su base gravable, afecta la capacidad de consumo de las clases medias y bajas lo cual disminuye el espectro y las capacidades del mercado interno. Esta situación directamente ligada al aumento de salario mínimo agudizará los factores de crisis.

### **Las propuestas** 

1\. Según Daza, si hubiera voluntad política lo que se debería hacer sería establecer un control de cambio del dólar para que no dependiera de las fluctuaciones del mercado porque no es un mercado libre, sino atado a las manipulaciones de EEUU.

2\. Con respecto a la reforma tributaria, Daza afirma que deberían incrementarse los impuestos directos, es decir que se debería hacer una revisión de los beneficios tributarios que, a su juicio, son inmensos.

3\. Otro de los grupos es de los empresarios que mantienen sus dineros en paraísos fiscales, Daza señala que mucha de la inversión extranjera en Colombia proviene de Panamá y muchas veces no son de ese país sino de empresarios nacionales que se hacen acreedores a los beneficios tributarios. Así las cosas la revisión de los aranceles significaría un aumento inmenso de los activos del país.

4\. Frenar los Tratados de Libre Comercio y renegociar los existentes aumentaría las capacidades productivas del campo colombiano y obligarían a un rediseño de la política agraria para que no esté sujeta a las exigencias coyunturales del mercado sino que tenga una aplicación a largo plazo.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u)  o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](https://archivo.contagioradio.com/).
