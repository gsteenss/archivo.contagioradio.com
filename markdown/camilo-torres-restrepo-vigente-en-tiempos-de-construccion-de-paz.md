Title: Camilo Torres Restrepo vigente en tiempos de construcción de paz
Date: 2017-02-15 15:20
Category: Cultura, Nacional
Tags: camilo torres, Carlos Medina, Javier Giraldo
Slug: camilo-torres-restrepo-vigente-en-tiempos-de-construccion-de-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/06/CT-IMG.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo Particular] 

###### [15 Feb 2017] 

En conmemoración a Camilo Torres, el padre Javier Giraldo y el profesor Carlos Medina lanzarán dos libros que recopilan el pensamiento de este teólogo de la liberación. A las 5 de la tarde en la Capilla que lleva su nombre, en la Universidad Nacional, el padre Javier Giraldo en compañía de otros sacerdotes, **realizarán una misa en memoria de Camilo Torres**. Le puede interesar: ["Camilo Torres, símbolo de unidad y amor eficaz" ](https://archivo.contagioradio.com/camilo-torres-amor-eficaz/)

Sobre las 6 de la tarde en el auditorio Camilo Torres, de la Facultad de Ciencias Políticas y Derecho, en la misma universidad, se llevará a cabo el acto conmemorativo **“Camilo Vive” que tendrá como panelistas a Vladimir Zabala, los docentes Daniel Libreros y Carlos Medina, y al padre Javier Giraldo** y en donde se conversará sobre el pensamiento de Camilo Torres a partir de sus diferentes momentos en la historia.

A su vez, el padre Javier Giraldo lanzará el libro **“Camilo, entonces y ahora frente a creyentes y agnósticos”** que se venderá a la salida del conversatorio, a un precio módico para que sea exequible para todos. El profesor Carlos Medina también lanzará su libro **“Camilo Torres Restrepo: La sonrisa de la esperanza**”, este libro se entregará de forma gratuita al finalizar el evento. Le puede interesar: ["ELN revela detalles de la muerte de Camilo Torres"](https://archivo.contagioradio.com/eln-revela-detalles-de-la-muerte-de-camilo-torres/)

De acuerdo con el profesor Carlos Medina, la vigencia del pensamiento de Camilo Torres se encuentra en su trabajo con la clase popular **“el pensamiento de Camilo contribuyó a la perspectiva de construcción de paz que vivimos actualmente**, el proceso que se inicia con el ELN y el Gobierno pueden tomar a consideración imaginarios de este pensador pa la creación de escenarios de democracia”, temas como este serán abordados en el libro que además contó con la mirada de otros analistas.

<iframe id="audio_17040226" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_17040226_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
