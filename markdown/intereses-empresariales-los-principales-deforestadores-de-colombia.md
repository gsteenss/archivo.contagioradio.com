Title: Interéses empresariales, los principales deforestadores de Colombia
Date: 2019-05-21 19:45
Author: CtgAdm
Category: Voces de la Tierra
Tags: Artemisa, colombia, deforestación, Iván Duque
Slug: intereses-empresariales-los-principales-deforestadores-de-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/07/tuparro-bosques-secos.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: 

Global Forest Watch, el más reciente informe del Instituto de Recursos Mundiales, reveló que Colombia ocupa el cuarto lugar en el mundo de los países que más bosques **han deforestado durante el año 2018, con 176.977** hectáreas. Una alarmante situación que genera varias preocupaciones entre las organizaciones defensoras del Ambiente. Entre ellas se encuentran, la gran perdida de ecosistemas para la fauna y flora, la falta de cumplimiento de acuerdos internacionales en la protección de la tierra y por supuesto la ampliación de la meta de deforestación en el gobierno Duque a 1.100.000 hectáreas que se acabaran en 4 años.

### **¿Quienes son los deforestadores en el país?** 

Juan Manuel Rengifo, investigador del Observatorio de Conflictos Ambientales de la Universidad Nacional, afirmó que en Colombia los grandes motores de deforestación son, en primer lugar, el conflicto armado que a partir de dinámicas de violencia, como el desplazamiento forzado o el despojo, **han provocado el acaparmiento de tierras que dan paso a los demás motores de deforestación como la ganadería, los monocultivos y la extracción minera**.

De igual froma, la ambientalista Veruska Nieto, aseguró que la deforestación en el país no solo se da al interior de parques Naturales, sino en zonas donde hay intereses particulares. En ese sentido, señaló que recientemente se pudo demostrar que en el área de manejo de la Macarena, que incluye 4 parques naturales, se han reportado  9.300 focos de calor o incendios, de los cuales **5.700 se encontraban dentro de bloques petroleros, con licencias otorgadas antes de la firma de los Acuerdos de Paz, en zonas selváticas**.

"Desde hace tres años hemos visto que los incendios en la parte sur del Meta, San José de Guaviare y la parte norte del Caquetá comienzan a prenderse a partir de la famosa ruta 65, que atraviesa tres países y alrededor de los planes de un oleoducto tapir que sacaría el petróleo de la cuenca sedimentaria Putumayo-Caguan", afirmó Nieto. ([Le puede interesar: "Deforestación sigue aumentando en la Amazonía Colombiana")](https://archivo.contagioradio.com/deforestacion-siguen-aumentando-la-amazonia-colombiana/)

### **El plan Artemisa no combate a los grandes deforestadores** 

Recientemente el gobierno nacional en cabeza de Iván Duque anunció la puesta en marcha de la Operación Artemisa, que tiene como finalidad la protección de parques naturales y frenar las actividades ilegales que atenten contra el ambiente.[(Le puede interesar: "El Ambiente otra víctima del Plan Nacional de Desarrollo")](https://archivo.contagioradio.com/el-ambiente-otra-victima-del-plan-nacional-de-desarrollo/)

Para Andrea Echeverri, integrante de Censat Agua Viva, la operación junto con "los discursos militaristas, están eludiendo la responsabilidad política del problema de la deforestación" y agregó que esta estrategia "solo sería una pantalla política para que la opinión pública sienta que se está haciendo algo, **cuando en realidad se mantienen intactas las bases sociopolíticas que permiten profundizar, no solo la deforestación, sino la crisis ambiental"**.

Por su parte Veruska Nieto expresó que hay varias preocupaciones entorno a esta Operación. La primera de ellas es que es una estrategia similar a la que se dio en el Congo con seis mil soldados, sin embargo actualmente ese es el país con mayor deforestación en el mundo, evidenciando para ella, la poca efectividad de esa iniciativa.

Otra de las preocupaciones de la activista tiene que ver con el Comandante del Ejército de Colombia **Nicacio Martínez**, que según la organización Human Right Watch, se encuentra en una lista de **9 altos mandos de la Fuerza Pública involucrados en casos de ejecuciones extrajudiciales.**

### **Los Falsos positivos ambientales en Colombia** 

El pasado 25 de abril, las familias campesinas que viven en el Parque Nacional de Chibiriquete, denunciaron la captura ilegal de 11 personas entre las que se encontraban cuatro menores de edad acusados de cometer delitos ambientales, que estarían siendo **víctimas de un falso positivo ambiental.** Sin embargo, el Gobierno señaló que tales hechos hacían parte de la Operación Artemisa.

Las personas capturadas fueron sometidas a interrogatorios y de acuerdo al comunicado de las familias campesinas, "ante la presión de estar en instalaciones militares aceptaron cargos relacionados con delitos ambientales, les dieron un plazo de tres días para recoger sus pertenencias y desocupar sus predios, a su regreso las familias se encontraron con la quema de las viviendas **y herramientas de trabajo, el ejército también dinamitó el puente que la comunidad construyó para pasar un caño**”.

Para Nieto estas situaciones podrían hacer parte de un gran plan que ya se evidenció en el Congo, y que tendrían como primera fase generar desplazamientos forzados de campesinos que han habitado y protegido su territorio durante muchos años, para luego ocasionar **una especulación con el valor de las tierras, para ser finalmente adquiridas por grandes terratenientes o multinacionales.**

Mientras que Andrea Echeverri manifestó que en el Guaviare se ha venido gestando una dinámica denominada burbuja contra la deforestación, que durante el año 2018 tuvo 72 capturas, todas personas campesinas, que tan solo representan el 0,72% de los responsables de la deforestación en el país, que al parecer podría ser subsidiada, es decir paga por otros, "es muy difícil que un campesino, que a duras penas tiene para sostener a su familia, pueda internarse tres semanas seguidas a tumbar 500 hectáreas de monte, con una moto sierra nueva, gasolina y remesa".

 <iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/video.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2Fvideos%2F926915641033543%2F&amp;show_text=0&amp;width=560" width="560" height="315" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Encuentre más información ambiental en[ Voces de la tierra ](https://archivo.contagioradio.com/programas/onda-animalista/)y escúchelo los jueves a partir de las 11 a.m. en Contagio Radio 
