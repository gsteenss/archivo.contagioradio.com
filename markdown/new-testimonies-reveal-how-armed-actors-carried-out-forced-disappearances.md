Title: New testimonies reveal how armed actors carried out forced disappearances
Date: 2019-09-26 12:25
Author: CtgAdm
Category: English
Tags: AUC, Colombian military, FARC, forced disappearances, Truth Commission
Slug: new-testimonies-reveal-how-armed-actors-carried-out-forced-disappearances
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/09/Desapariciones-HH.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Photo: Truth Commission] 

 

**Follow us on Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

[The Truth Commission released the testimonies of armed actors, including members of the Revolutionary Armed Forces of Colombia (FARC), United Self-Defense Forces of Colombia (AUC) and the Colombian military, that admitted to taking part in acts of forced disappearances. For the first time, a former military official has acknowledged responsibility in these crimes. ]

[Among the former combatants that testified were José Benítez Ramírez, former commander of the FARC’s Southern Bloque; José Ever Veloza, an AUC ex-combatant known as HH; and Gustavo Enrique Soto Bracamonte, a retired major general who described the methods used to prevent the identification and discovery of the victims. ]

[The testimonies that have been offered voluntarily and come from highly-ranked actors have been prioritized. According to the commissioner Carlos Berinstain, the main challenges armed actors face to contribute to the truth is a “pact of silence among the responsible” and a “practice of insensitivity,” which has allowed for these crimes to remain unresolved.]

[In his testimony, Soto Bracamonte, who was the commander of the military unit Gaula in the Casanare province from 2006 to 2007, said that he allowed assassinations and forced disappearances to occur under his watch. He added that his subordinates disappeared the victims’ documents to make it harder to identify them and assured that his testimony would especially benefit the family members of victims. ]

> [“Because of the statistics that the military managed, I always tried to be in the top 10 of the units and this caused many problems for the Colombian people and in particular, for the victims,” the retired general said.]

[Jose Éver Veloza, a former paramilitary commander of the Calima and Bananeros Bloques that operated in the Urabá region, said in his testimony that the assassinations were committed openly until the military asked them "not to leave the dead on the road and to instead, disappear them because the presence of bodies increased the homicide rates and that was damaging for them."]

[According to commissioner Alejandra Miller, these confessions have been obtained thanks to the influence of the family members who continue to look for the disappeared. This was confirmed by the testimony of the former FARC commander who is working with the Search Unit for Disappeared Persons.]

"I saw that the mothers arrived in Caguán to look for their loved ones and there, I felt that it was necessary to begin to give them answers," Benítez said.

[With these testimonies, ]Benítez Ramírez, Ever Veloza and Soto Bracamonte affirmed their confidence in the Truth Comission and their commitment to clarifying what happened during the armed conflict.

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
