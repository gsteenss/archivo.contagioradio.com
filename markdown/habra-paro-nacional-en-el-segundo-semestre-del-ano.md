Title: Habrá Paro cívico Nacional en el segundo semestre del año
Date: 2016-07-08 12:40
Category: Movilización, Nacional
Tags: Paro Nacional
Slug: habra-paro-nacional-en-el-segundo-semestre-del-ano
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/07/kaosenlared.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Kaosenlared.net] 

###### [8 jul 2016] 

El  8 y 9 de julio se llevará a cabo en Bogotá el encuentro de organizaciones políticas y sociales, para analizar el contexto que afronta el país bajo el mandato del actual presidente Juan Manuel Santos y posiblemente se convocaría a un **paro cívico nacional que se desarrollaría en el segundo semestre del año**.

El encuentro tendría como objetivo revisar los **acuerdos que el gobierno a pactado** con los diferentes sectores de la sociedad. Por otro lado, el pasado 17 de marzo se lanzó un [pliego de exigencias conformado por 15](https://archivo.contagioradio.com/el-proximo-17-de-marzo-es-el-dia-cero-para-el-inicio-del-paro-nacional/) puntos del paro Nacional, pliego que no ha obtenido ninguna respuesta por parte de los diferentes ministerios y en donde se mencionaron las problemáticas que afrontan en **este momento el sector agrario y camionero del país.**

De acuerdo con Luís Alejandro Pedraza, presidente de la Central Unitaria de Trabajo, el encuentro tiene como fin analizar y evaluar los eventos que se han desarrollado este año y años anteriores, con todos los sectores del movimiento social con quienes el gobierno ha generado acuerdos **para exigir en conjunto el cumplimiento de los mismos**.

En el espacio participarán representantes de los tres sectores de las centrales sindicales, las dos confederaciones de pensionados, la Cumbre Agraria Étnica Popular y Campesina, Congreso de los Pueblos, organizaciones indígenas, organizaciones afros, organizaciones de víctimas y[organizaciones del movimiento estudiantil](https://archivo.contagioradio.com/300-mil-maestros-de-colombia-inician-paro-nacional-por-incumplimientos-del-gobierno/).

<iframe src="http://co.ivoox.com/es/player_ej_12163059_2_1.html?data=kpeemJiUeZqhhpywj5WaaZS1lJiah5yncZOhhpywj5WRaZi3jpWah5ynca3pytiYo5OPlMbY08bnw4qWh4y3trmah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ..&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)] 
