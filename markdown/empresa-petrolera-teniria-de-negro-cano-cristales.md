Title: Empresa petrolera teñiría de negro las aguas de Caño Cristales
Date: 2016-04-13 19:34
Category: Ambiente, Nacional
Tags: Caño Cristales, Meta, petroleo, zonas de reserva campesina
Slug: empresa-petrolera-teniria-de-negro-cano-cristales
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/04/Caño-Cristales-e1460593392319.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Caño Cristales 

###### [13 Abr 2016]

Mientras que el gobierno ha negado el acceso a la tierra de miles de familias campesinas que llevan años solicitando que se declare legal la Zona de Reserva Campesina Losada – Guayabero y Macarena, si se concedió licencia ambiental para explorar y explotar 150 pozos petroleros en medio de tres parques naturales entre los cuales se encuentra **Caño Cristales, el río de los cinco colores, conocido como 'el más hermoso del mundo’.**

La empresa Hupecol, mediante la resolución 0286 del 18 de marzo de 2016 queda autorizada para la explotación de petróleo en gran parte de la Macarena, Meta, una región en la que se encuentran **los parques naturales, Picachos, Tininua, y Sierra de la Macarena**.

Pese a que desde el gobierno y las autoridades ambientales se argumenta que los pozos petroleros estarían ubicados por fuera de los parques, lo cierto es que estarán en zona de amortiguamiento de estas reservas naturales y además **un área colonizada por 6 mil  familias campesinas e indígenas víctimas de desplazamiento por el conflicto armado.**

“La contradicción es que se viene negando el derecho a la tierra de los campesinos, en cambio con un estudio ambiental bastante precario, se da la posibilidad a una empresa de perforar **150 pozos, construir  38 carreteras y centenares de locaciones**, sumado a eso, se producirían toneladas de residuos que afectará las reservas ambientales”, asegura César Jeréz, vocero de la Asociación Nacional de Zonas de Reserva Campesina, ANZORC.

Jeréz denuncia que Cormacarena y la Agencia Nacional de Licencias Ambientales, ANLA son las  instituciones que están permitiendo que se afecte una zona que es productora de agua. “Hay una contradicción total con el papel de las entidades ambientales. **Son muy agresivas con los campesinos pero permisivas para que una empresa llegue a la región”,** señala.

Se trataría entonces de un **nuevo modelo de despojo**, afirma el vocero de ANZORC, pues se criminaliza a los pobladores que incluso estaban antes de que se establecieran los parques naturales, y se logra el establecimiento de un modelo de economía extractivista, desplazando nuevamente a esas comunidades campesinas e indígenas.

Es por eso que las familias y líderes sociales deberán configurar un bloque de resistencia para impedir que esta explotación se logre, sostiene Cesar Jeréz, quien indica que existen bastantes irregularidades en el proceso de licitación.

<iframe src="http://co.ivoox.com/es/player_ej_11163097_2_1.html?data=kpaemJiUfZihhpywj5aUaZS1lpaah5yncZOhhpywj5WRaZi3jpWah5yncaSZpJiSo57XpdOfq8rfx9%2BPcYy1r7%2B8tKiRaZi3jqjc0NnFq8rjjLfOxs7Tb46ZmKialg%3D%3D&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
