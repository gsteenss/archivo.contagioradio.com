Title: Controversia por placa que honra militares de la retoma del Palacio de Justicia
Date: 2017-04-27 18:10
Category: DDHH
Tags: Concejo de Bogotá, desaparecidos, Palacio de Justicia, pilar navarrete, Placa
Slug: concejo-de-bogota-deberia-reconocer-la-memoria-de-las-victimas-del-palacio
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/01/Palacio-de-Justicia.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Sin Olvido] 

###### [27 Abr. 2017] 

“¿Quiénes asesinaron a la mayoría de los magistrados? Los militares y los agentes del Estado con su extensión de fuego, de poder y con todos los excesos que se cometieron en la toma del Palacio de Justicia”, así lo manifestó Pilar Navarrete, esposa de Héctor Jaime Beltrán uno de los desaparecidos del Palacio de Justicia ante la posibilidad de **colocar una placa que conmemore a los uniformados presentes en la toma del Palacio de Justicia. **

Esta iniciativa había sido radicada desde noviembre de 2016 por el concejal del Centro Democrático Daniel Palacios, quien aseguró que el propósito de esta es exaltar y honrar la memoria de los integrantes de la Fuerza Pública que perdieron la vida en la toma del Palacio de Justicia. Le puede interesar: [Coronel (r) Edilberto Sánchez pagará 40 años de prisión por desaparecidos del Palacio de Justicia](https://archivo.contagioradio.com/coronel-edilberto-sanche-condenado-por-palacio-de-justicia/)

Sin embargo, Navarrete manifiesta que “memoria es contar una historia, **memoria es que escuchen a las víctimas, no solo a los desaparecidos, muchísimas víctimas que tienen que contar todo lo que sufrieron** y lo que han sufrido estos 32 años”.

Para Navarrete, el **Estado, el Concejo, los concejales y los magistrados que han estado de turno han querido siempre enaltecer a la gente que tuvo más que ver** que los mismos guerrilleros, haciendo referencia a los militares, razón por la que asegura que está iniciativa no le sorprende. Le puede interesar: [Alcaldía de Bogotá no quiso escuchar peticiones de víctimas del Palacio de Justicia](https://archivo.contagioradio.com/alcaldia-de-bogota-no-quiso-escuchar-peticiones-de-las-victimas-del-palacio-de-justicia/)

### **El Estado le incumplió a las víctimas.** 

Los familiares del Palacio de Justicia han recordado que la sentencia de la CIDH en la que se ordena al Estado colombiano indemnizarlos, garantizarles la salud, entre otros temas, no ha sido cumplida. Le puede interesar: [Gobierno incumple reparación en el caso del Palacio de Justicia](https://archivo.contagioradio.com/gobierno-incumple-reparacion-en-el-caso-del-palacio-de-justicia/)

**“Yo estoy denunciando en este momento el incumplimiento de la sentencia y la dilatación que han hecho para cumplir todo lo que la Corte Interamericana han dicho**. Por lo que tuvo que volverse a pedir a la Corte que le recordara al Estado cómo se cumplía una sentencia y aun así a 31 años y 6 meses no hemos comenzado” relata Navarrete. Le puede interesar: [Familiares de las víctimas del Palacio de Justicia seguirán exigiendo verdad y justicia](https://archivo.contagioradio.com/familiares-de-las-victimas-del-palacio-de-justicia-seguiran-exigiendo-justicia/)

[Carta de familiares del Palacio de Justicia](https://www.scribd.com/document/346605637/Carta-de-familiares-del-Palacio-de-Justicia#from_embed "View Carta de familiares del Palacio de Justicia on Scribd") by [Contagioradio](https://es.scribd.com/user/276652802/Contagioradio#from_embed "View Contagioradio's profile on Scribd") on Scribd

<iframe id="doc_65266" class="scribd_iframe_embed" src="https://www.scribd.com/embeds/346605637/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-8oIYa1zunCZGLbiyEhDX&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="0.7068965517241379"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)
