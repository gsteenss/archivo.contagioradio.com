Title: Derogar Ley de Garantías es “aceitar la maquinaria” del gobierno para elecciones de Octubre
Date: 2015-03-31 17:42
Author: CtgAdm
Category: Nacional, Política
Tags: Corte Constitucional, Ley de garantías., Magistrado Prettelt, Rodolfo Arango, tutela
Slug: derogar-ley-de-garantias-es-aceitar-la-maquinaria-del-gobierno-para-elecciones-de-octubre
Status: published

###### Foto: youtube.com 

<iframe src="http://www.ivoox.com/player_ek_4290497_2_1.html?data=lZemkpmde46ZmKiak5aJd6Kkk5KSmaiRdo6ZmKiakpKJe6ShkZKSmaiRiMbm0MzO1JCwqdqfxcqYqcbWpc%2Fohqigh6aopdSfxtiYh6qWaZmkhp6ww8jJrdXV05DZw5DRpdLpytPO1M7Faaamhp2dj4qbh4630NPhw8zNs4zGwsnW0ZCRaZi3jpk%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Rodolfo Arango, Ex Magistrado Aux de Corte Constitucional] 

Para nadie es un secreto que Colombia es un país tremendamente presidencialista y centralista, por ello, aunque el gobierno salga a decir que no hay de qué preocuparse por favorecer ciertos intereses políticos porque todos serán beneficiarios, esta propuesta le sirve al gobierno para aceitar la maquinaria de cara a las elecciones de octubre, afirma el Ex magistrado auxiliar de la Corte Constitucional Rodolfo Arango.

Es muy fácil prever que habrá inversión pronta y ágil para algunas regiones dependiendo de los intereses electorales del gobierno en ciertos departamentos. Arango agrega que seguramente habrá inversión pronta en unos sitios y demoras en otros. Además esto será terriblemente dañino para la oposición, porque primarán los intereses electorales favorables a la coalición de gobierno.

Por otra parte, frente a la crisis actual de la Corte Constitucional, Arango se sitúa en el grupo de las personas que creen que el dedo debe apuntar no hacia la Corte sino hacia las personas. Para Arango el caso Pretelt está siendo usado de manera política por parte de grupos que no están de acuerdo con la propia Corte y sus decisiones y que están en contra de avances como la tutela. Por ejemplo, “ya se habla de que no haya tutela contra sentencias de altas Cortes", resalta Arango para dimensionar las intenciones que hay detrás de este tipo de propuestas.

Frente a la propuesta del gobierno nacional Arango afirma que no está de acuerdo con tribunales de aforados; para él, las soluciones se pueden encontrar si hay un diseño institucional acorde con las necesidades del país. En lugar de un tribunal de aforados que significa justicia especial para algunos, se puede integrar una comisión técnica, rotativa, de magistrados en la Corte Suprema, que evalúe las denuncias contra altos funcionarios y decida si hay causal para suspenderlos.

Para el ex magistrado auxiliar, el comportamiento del magistrado Pretelt es indigno de su cargo y tendría que renunciar, teniendo en cuenta el daño que le está haciendo a las instituciones del país.

.
