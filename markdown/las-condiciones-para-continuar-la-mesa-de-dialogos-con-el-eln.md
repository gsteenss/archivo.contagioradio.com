Title: Las condiciones para continuar la mesa de diálogos con el ELN
Date: 2018-06-19 15:08
Category: Paz, Política
Tags: ELN, Gobierno Nacional, Iván Duque, proceso de paz
Slug: las-condiciones-para-continuar-la-mesa-de-dialogos-con-el-eln
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/06/Captura-de-pantalla-2018-06-19-a-las-3.03.08-p.m..png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: ELN] 

###### [19 Jun 2018] 

######  

**Iván Duque afirmó en las ultimas horas que la única forma de continuar con el proceso de paz "debe ser la suspensión de todas las actividades criminales" por parte del ELN.** Según Luis Eduardo Celis, asesor de la Fundación Paz y Reconciliación,  lo fundamental para que se garanticen las conversaciones es que Juan Manuel Santos, Iván Duque y el ELN encarnen la idea de construcción de paz que hay en la sociedad colombiana.

Celis señaló que los comicios muestran que hay 8 millones de personas de acuerdo con buscar una salida negociada al conflicto armado colombiano. Por esta razón, lo que espera como mínimo es que “Duque y el ELN se sienten a charlar y busquen un camino.”

En consecuencia, para que Iván Duque continúe con la mesa de negociación de la Habana, Celis considera que deben existir unas circunstancias que permitan la negociación. **Estos requisitos son: el pacto de un nuevo cese bilateral y, la disposición tanto de Duque como del ELN para sentarse a la mesa con argumentos a dialogar.** (Le puede interesar: ["Cese bilateral y participación de la sociedad: Temas claves entre Gobierno y ELN"](https://archivo.contagioradio.com/cese_bilateral_gobierno_eln_mesa_quito/))

### **El primer paso lo dio el ELN** 

En un comunicado emitido por el ELN, **el grupo armado afirmó que sigue en la Mesa, e indicó que está dispuesto a conversar con “el futuro gobierno” con el fin de consolidar la búsqueda de una paz** que permita salir de la violencia definitivamente y acabar con las razones de fondo del conflicto Colombiano.

Por su parte, el nuevo presidente, fijó sus condiciones para continuar con la mesa de diálogo: Que el ELN se concentre para su desmovilización, supervisada por una veeduría internacional y que se suspendan "todas las actividades criminales".

De acuerdo con el comunicado, **será el entrante presidente quien definirá si se recorre el camino de la salida negociada al conflicto** o, como lo señala Celis "el camino de la guerra, de las imposiciones que ya conocemos, y que nos ha dejado tantos dolores".

[Comunicado ELN](https://www.scribd.com/document/382114055/Comunicado-ELN#from_embed "View Comunicado ELN on Scribd") by [Contagioradio](https://www.scribd.com/user/276652802/Contagioradio#from_embed "View Contagioradio's profile on Scribd") on Scribd

<iframe id="doc_70025" class="scribd_iframe_embed" title="Comunicado ELN" src="https://www.scribd.com/embeds/382114055/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-Vz4jLMsIogy3wKDb3n84&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="0.7729220222793488"></iframe><iframe id="audio_26621772" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_26621772_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
