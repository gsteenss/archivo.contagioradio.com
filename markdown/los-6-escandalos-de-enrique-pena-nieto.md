Title: Los 6 escándalos más sonados de Enrique Peña Nieto
Date: 2017-01-23 17:59
Category: El mundo, Política
Tags: Ayotzinapa, Enrique Peña Nieto, mexico
Slug: los-6-escandalos-de-enrique-pena-nieto
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/01/pena-nieto.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: RuizHealyTimez.com] 

###### [23 Ene 2017]

Desde comienzos de enero, México vive una de las crisis más fuertes por el alza denominada “gazolinaso”, que ha sido cuestionada por la gran mayoría de ciudadanos debido a que no según subsana la escases del combustible, razón que dio Nieto para realizarla, y sí aumenta los costos de vida. Sin embargo, este no es el único hecho que ha cuestionado el periodo de gobierno: **desapariciones forzadas, masacres y narcotráfico, han sido algunas de las situaciones que más impacto han generado en los mexicanos.**

### **La desaparición de los 43 normalistas de Ayotzinapa** 

El 26 de septiembre del año 2014, 43 estudiantes de la Normal Isidro Burgos de Ayotzinapa desaparecieron en el municipio de Iguala, mientras que 25 más resultaron heridos y tres fallecieron. Los hechos ocurrieron cuando los normalistas se transportaban en un bus que fue interceptado por la Policía que disparo en su contra.

A pesar de las múltiples evidencias que involucran la participación de agentes del Estado Mexicano en los crímenes cometidos,  la **falta de respuestas por parte del gobierno Peña Nieto** y la complicidad de las instituciones encargadas de investigar y garantizar el cumplimiento de la justicia, han permitido que hasta la fecha el caso continué en total impunidad. Le puede interesar: [“Ayotzinapa 2 años sin verdad ni justicia”](https://archivo.contagioradio.com/ayotzinapa-2-anos-sin-verdad-ni-justicia/)

De igual forma, la CIDH denunció que en este caso se ha vivido una **obstrucción a las investigaciones por parte de las autoridades**, luego de que el Grupo Interdeisiplinario de Expertos Independientes determinará en un informe duras observaciones a las mecánicas de trabajo de la Procuraduría General y  la limitación para obtener información por parte de detenidos y miembros de la Fuerza Pública

### **Masacre de Tlataya** 

El 30 de Junio de 2014, ocho militares pertenecientes al  102 Batallón de Infantería reportaron que habían dado de baja a 21 hombres y una mujer menor de edad. Las autoridades expresaron que los cuerpos correspondían a delincuentes y que murieron en un intercambio de fuego. Sin embargo, esta versión cambió cuando sobrevivientes a estos hechos lograron hablar.

Una de ellas es Clara Gómez, madre de Erika, la menor de edad hallada en el lugar.  Gómez afirmó que, pese a que algunas de las personas que se encontraban en el lugar si eran delincuentes se rindieron durante el tiroteo. Posteriormente relató que los militares entraron al lugar y sin reparaciones acribillaron a cada una de las personas que se encontraban aún con vida.

La Comisión Nacional de Derechos Humanos logró establecer que la escena del crimen fue alterada, además Clara Gómez fue retenida durante 6 días y denunció tratos vejatorios. Hasta el momento solo se ha dictaminado una condena, a su vez que Peña Nieto ha señalado que se “debe superar esta etapa”.

### **La fuga de Juaquin Guzmán alias el “Chapo”** 

El 11 de julio de 2015 el “Chapo” Guzmán, líder del cartel de Sinaloa o del Pacífico, escapó de una de las cárceles de máxima seguridad de México, hecho que **puso en tela de juicio al gobierno de este país y su capacidad para frenar las actividades ilegales de uno de los máximos jefes del narcotráfico.**

El Chapo se dio a la fuga a través de unos túneles que contaban con luz y ventilación, construido por sus colaboradores, y que llevaban desde su celda a las afueras de la cárcel. El capo fue finalmente capturado el 8 de enero de 2016.

### **El Muro de Donald Trump entre Estados Unidos y México** 

Pese a que en las propuestas de gobierno del entonces candidato Donald Trump, estuviese la construcción de un muro entre Estados Unidos y México, y aunque durante la gran mayoría de sus discursos se haya referido a la **comunidad latina como ladrones y principales artífices del problema del narcotráfico**, este visitó a México, por invitación del presidente Peña Nieto el pasado primero de septiembre.

Para diferentes analistas este fue un “hecho histórico” que ratifico las acciones que Trump puede cometer en contra de los mexicanos y la poca oposición que encontrará en Peña Nieto. En declaraciones dadas después del infructuoso encuentro Peña nieto afirmó **“Quizá nos apresuramos, quizá nos faltó ponderar la reacción social que se iba a presentar en México”**, bajando su popularidad, en donde solo 2 de cada 10 mexicanos aprueban su gestión. Le puede interesar:["El muro no va a ser en la frontera sino en México entero"](https://archivo.contagioradio.com/muro-no-va-a-ser-en-la-frontera-34924/)

### **Casa Blanca y corrupción** 

El 9 de noviembre de 2014, se dio a conocer el escándalo de la Casa Blanca, un previo avaluado en **7.5 millones de dólares y construido por el grupo Higa**, contratista del gobierno, que pertenecía a Angélica Rivera, esposa del presidente. Rivera explicaría que la casa fue adquirida en 53 millones de dólares y que sería el fruto de su trabajo.

Sin embargo, otros miembros del gabinete como Luis Videgaray, también adquirió una casa del grupo Higa por **532 mil dólares**. En este punto. De igual forma se reveló que durante el año 2013 las empresas Bancomext y Banobras dependientes de Videgray entregaron contratos millonarios a Videgray.

Finalmente, Virgilio Andrade, juez a cargo de la investigación, exoneraría a Enrique Peña Nieto, Angélica Rivera y Luis Videgaray afirmando que no existía tal conflicto

### ** “Gasolinazo”** 

Desde el primero de enero de este año, entro a regir el “Gasolinazo”, medida impuesta por el presidente Enrique Peña Nieto que pretende solucionar la crisis que afronta México frente al combustible. No obstante, esta decisión ha **generado estragos y por el contrario ha aumentado el precio de servicios básicos como el transporte público**. Le puede interesar: ["Gasolinazo una medida regresiva para Mexico"](https://archivo.contagioradio.com/gasolinazo-una-medida-regresiva-para-mexico/)

De igual forma, el que el alza no se aplique en un precio estándar sino que aumente dependiendo del estado, ha significado para los mexicanos un favoritismo a **empresas mexicanas dueñas de estaciones de gasolina**. En reacción a este hecho, **multitudinarias protestas se han registrado en todos los estados del país**, que le exigen al presidente quitar la medida y dar verdaderas soluciones a la problemática del combustible.

Finalmente organizaciones defensoras en derechos humanos, como el Centro de Estudios Ecuménicos han expresado su preocupación frente al **aumento de grupos paramilitares en diferentes estados de México**, que pretenden desarticular al movimiento social  e inhibir la participación de grupos sociales en defensa del territorio y la vida.

###### Reciba toda la información de Contagio Radio en [[su correo]
