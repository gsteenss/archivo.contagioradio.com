Title: Paro Nacional
Date: 2019-04-24 17:53
Author: CtgAdm
Slug: paro-nacional-6
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/paro-nacional.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

\#PARONACIONAL
==============

[  
Facebook  
](https://www.facebook.com/contagioradio)  
[  
Twitter  
](https://twitter.com/contagioradio1)  
[  
Instagram  
](https://www.instagram.com/contagioradio/)

**FECHA DE INICIO 25 DE ABRIL **

#### ¿POR QUÉ EL PARO NACIONAL?

![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/paro-nacional-3.jpg){width="1000" height="667" sizes="(max-width: 1000px) 100vw, 1000px" srcset="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/paro-nacional-3.jpg 1000w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/paro-nacional-3-300x200.jpg 300w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/paro-nacional-3-768x512.jpg 768w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/paro-nacional-3-370x247.jpg 370w"}

##### Tienes preguntas sobre la movilización del 25 de abril, aquí las respondemos. 

[¿Por qué un paro nacional?]{}

Los diferentes sectores sociales saldrán a las calles a exigir el cumplimiento de los acuerdos, rechazo al asesinato de líderes y lideresas sociales, el ajuste del Plan Nacional de Desarrollo, defensa de la educación pública.

[¿Quiénes participan?]{}

A la movilización se han sumado sindicatos, movimientos políticos, sociales, campesinos, indígenas, afros, estudiantiles

[Durante cuántos días será el paro nacional]{}

La actividad nacional iniciará el 25 de abril en diferentes regiones de Colombia,  la movilización podrá extenderá por más días.

[¿En qué ciudades habrán movilizaciones?]{}

**Bogotá**, marcha desde CAD hasta la Plaza de Bolivar 9 a.m.

U. Pedagógica: Plaza Dario 8 a.m.

Parque Nacional  8:30 a.m.

**Medellín:**

Parque de los Deseos 9 a.m.

**Yopal:**

Parque El Resurgimiento 8 a.m.

**Bucaramanga:**

Parque San Pio, 9 a.m.

**Popayán:**

Claustro de Santo Domingo 9 a.m

**Santa Marta:**

Parque San Miguel 8 a.m

**Cucuta: **

Central de Transportes 8 a.m

**Cali:**

Glorieta de Sameco  9 a.m

¿Cómo va el paro nacional?
--------------------------

","nextArrow":"","autoplay":false,"autoplaySpeed":5000,"rtl":false}' dir="ltr"&gt;  
<time title="2019-04-24T18:07:47-05:00" datetime="2019-04-24T18:07:47-05:00">abril 24, 2019</time> Cinco razones de peso para respaldar el Paro Nacional, ¿con cuál se identifica?

<time title="2019-04-24T16:59:48-05:00" datetime="2019-04-24T16:59:48-05:00">abril 24, 2019</time> Las rutas de la movilización que paralizará el país este 25 de abril

<time title="2019-04-23T17:46:36-05:00" datetime="2019-04-23T17:46:36-05:00">abril 23, 2019</time> Asociación campesina del Catatumbo sin garantías para participar en paro nacional

<time title="2019-04-23T16:30:19-05:00" datetime="2019-04-23T16:30:19-05:00">abril 23, 2019</time> El 28 de abril abre las puertas el Refugio Humanitario por la vida de los...

<time title="2019-04-23T14:33:00-05:00" datetime="2019-04-23T14:33:00-05:00">abril 23, 2019</time> Estudiantes se suman a paro nacional del 25 de abril

<time title="2019-04-10T21:17:34-05:00" datetime="2019-04-10T21:17:34-05:00">abril 10, 2019</time> Duque tendrá que hablar con la Minga Nacional del 25 de abril: CRIC

<time title="2019-04-09T23:02:50-05:00" datetime="2019-04-09T23:02:50-05:00">abril 9, 2019</time> Miércoles 24 de abril: cafeteros marchan en Armenia por crisis del sector

<time title="2019-04-02T17:47:50-05:00" datetime="2019-04-02T17:47:50-05:00">abril 2, 2019</time> 25 abril paro nacional por la paz y contra el Plan Nacional de Desarrollo

Pintando el Paro Nacional
-------------------------

<figure>
[![ASPU](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/Captura-de-pantalla-2019-04-23-a-las-20.30.51-370x260.png)](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/Captura-de-pantalla-2019-04-23-a-las-20.30.51.png)

</figure>
<figure>
[![Panfletario](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/D4zUzlCWAAE7NX4-370x260.jpg)](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/D4zUzlCWAAE7NX4.jpg)

</figure>
<figure>
[![Refugio humanitario](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/Captura-de-pantalla-2019-04-23-a-las-20.28.03-370x260.png)](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/Captura-de-pantalla-2019-04-23-a-las-20.28.03.png)

</figure>
" Lo que importa son las incontables hazañas de la gente común, ellos sientan la base de los sucesos importantes que pasan en la historia "  
Howard Zinn  
Historiador
