Title: Pueblo indígena Kokonuko denuncia el asesinato de dos mujeres de su comunidad
Date: 2016-12-02 12:55
Category: DDHH, Nacional
Tags: Asesinatos de indígenas en Colombia, feminicidios, Pueblo indígena Kokonuko, Violencia contra las mujeres en Colombia
Slug: pueblo-indigena-kokonuko-denuncia-el-asesinato-de-dos-mujeres-de-su-comunidad
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/12/coconuco.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Pueblo Kokonuko] 

###### [2 Dic 2016] 

El pasado 29 de noviembre, dos mujeres del pueblo indígena Kokonuko zona del Puracé en el Cauca, Marly Yuliet Gómez y Shirley Dayana Lozada Ramírez ambas de 23 años edad, fueron **asesinadas en el kilómetro 31 de la carretera que comunica a este departamento con el Huila.**

A través de un comunicado las autoridades tradicionales y la guardia indígena manifestaron que **“es lamentable que en el marco del proceso de paz, sigan ocurriendo hechos de violencia y pérdida de vidas humanas,** hacemos un llamado y exigimos a las autoridades y los organismos de control y organizaciones nacionales e internacionales que velan por los Derechos Humanos, para que pronto se esclarezcan los hechos”.

En la misiva las autoridades también se refieren a “la **desarmonización y desestabilización que estos hechos de dolor y violencia generan a las familias, la vida comunitaria y nuestro territorio”. **Le puede interesar: [Gobernador indígena sale ileso de atentado con explosivo en Nariño.](https://archivo.contagioradio.com/gobernador-indigena-sale-ileso-de-atentado-con-explosivo-en-narino/)

Por último, hacen un llamado a los pueblos indígenas, organizaciones campesinas, afros y otros sectores de la sociedad civil a que estén “atentos, alertas y en pie de lucha ante las arremetidas violentas de grupos armados de todo orden, para que todos juntos **rechacemos estos actos y denunciemos ante la opinión pública, las afectaciones y vulneraciones de nuestros derechos fundamentales”.**

###### Reciba toda la información de Contagio Radio en [[su correo]
