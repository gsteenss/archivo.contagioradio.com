Title: CAR aprueba licencia para hacer minería en el río Magdalena
Date: 2020-02-20 14:27
Author: CtgAdm
Category: Ambiente, Nacional
Slug: car-aprueba-licencia-para-hacer-mineria-en-el-rio-magdalena
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/02/b3cae1d2-ff26-468e-84e1-9b0ecefbab77.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/02/WhatsApp-Image-2020-02-19-at-11.52.02-AM.jpeg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/02/8319e3dc-67f9-4817-9456-6b2d84b43fd9.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:html -->

<figure class="wp-block-audio">
<audio controls src="https://co.ivoox.com/es/antonio-sanguino-sobre-politica-exterior-duque_md_48221583_wp_1.mp3&quot;">
</audio>
  

<figcaption>
Entrevista &gt; Julian Huertas | Consejal de Girardot

</figcaption>
</figure>
<!-- /wp:html -->

<!-- wp:paragraph -->

La Corporación Autónoma Regional de Cundinamarca (CAR) aprobó en 2019 una licencia para explotar arenas, gravas, tierra y minerales en el río Magdalena; esto a pesar que desde el 2015 la comunidad de Girardot y Coello,Tolima, denunciaran y protestaran por los impacto que causaría este proyecto en la cuenca del río.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Movilizaciones que según **Julian Huertas Concejal de Girardot** han sido ignorada por la Corporación, *"los líderes del movimiento social en contra de la adjudicación de la licencia, han recibido amenazas de muerte, trato despectivo e instigaciones verbales"*.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Impacto ambiental en el río Magdalena

<!-- /wp:heading -->

<!-- wp:paragraph -->

La licencia ambiental que que confiere la CAR es para el Proyectos de explotación de las empresas Ingeniería y Geología Colombiana **PROINGECOL S.A.S.** y a la empresa **Proacol** en la inmediatez del río amazonas en su paso por los departamentos de Tolima y Cundinamarca.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Dentro de los impactos ambientales resalta no solo los residuos de material tóxico en el agua usada por los habitantes en su diario y especies nativas para su supervivivencia, también la afectación de las únicas playas del río Magdalena, utilizadas con fines turísticos y recreativos por los pobladores de la zona. (Le puede interesar: <https://archivo.contagioradio.com/la-temporada-de-la-mala-calidad-de-aire-en-bogota-hasta-ahora-comienza/>)  
 

<!-- /wp:paragraph -->

<!-- wp:quote -->

> *“Girardot alberga una gran variedad de especies de aves y peces, siendo un hábitat bio-diverso de gran importancia ambiental. El movimiento quiere que Colombia y el mundo disfruten de un río Magdalena limpio e insignia de la protección ambiental pacífica”*.  
>
> <cite> Julián Huertas - Concejal Girardot </cite>

<!-- /wp:quote -->

<!-- wp:paragraph -->

Dentro de los procesos por parte de la comunidad para evitar esta acción de la CAR, destaca la proposición firmada por los concejales de Girardot quienes solicitaron al **Luis Fernando Sanabria, nuevo director de la CA**R, la revocación de la decisión tomada por su antecesor.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Los concejales denunciaron que con esta medida* “**se están vulnerando los derechos fundamentales a un ambiente sano**, al agua, a la vida y a la salud de los girardoteños, y se está poniendo en riesgo la integridad ecosistémica”.*  

<!-- /wp:paragraph -->

<!-- wp:image {"id":81018,"sizeSlug":"large"} -->

<figure class="wp-block-image size-large">
![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/02/8319e3dc-67f9-4817-9456-6b2d84b43fd9-1024x339.jpg){.wp-image-81018}

</figure>
<!-- /wp:image -->

<!-- wp:heading {"level":3} -->

### **Un proceso marcado por inconsistencias**

<!-- /wp:heading -->

<!-- wp:paragraph -->

  
Según Huertas con la autorización de la actividad minera en el cauce del río Magdalena se configuró también una vulneración al debido proceso, "*el proyecto **cambió sustancialmente entre la propuesta inicial y la que finalmente fue aprobada***, temas como coordenadas del punto de extracción, *costos de la actividad, ventas proyectadas, y Evaluación del Impacto Ambiental (EIA) son algunos de los temas modificados luego de la aprobación"*.  
   
Y afirmó que este proyecto tiene múltiples falencias frente a los manuales de estudios ambientales y que además, *"se realizó sin la participación real y efectiva de las poblaciones locales, **desconociendo también a la comunidad indígena** pijao en Coello **y a los campesino**s de Girardot".*

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### **¿Qué pasará si el proyecto de Proacol se hace realidad** en el Magdalena?

<!-- /wp:heading -->

<!-- wp:paragraph -->

Según el Concejal las afectaciones que acarrearía la activación del proyecto son varias, entre ellas alteración de la dinámica natural del río Magdalena. Pérdida de especies únicas de aves y peces, propias de la biodiversidad del río.Afectación de la pesca, actividad de la que viven los indígenas Pijao asentados en el municipio de Coello y los campesinos de Girardot.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Afectaciones de los daños para la salud de los campesinos e indígenas de las zonas aledañas al río Magdalena, ***"hay múltiples casos que evidencias que la minería siempre trae problemas a los territorios**, además de los cambios en las dinámicas socioeconómicas de los habitantes de la zona, al perder la posibilidad de desarrollar la pesca, su única actividad extractiva."*.

<!-- /wp:paragraph -->

<!-- wp:quote -->

> *"Los pescadores denuncian que, en lo que va del 2020, su actividad ha caído en 60%, afectando seriamente los ingresos de los hogares locales".*
>
> <cite>Julian Huertas </cite>

<!-- /wp:quote -->

<!-- wp:heading {"level":3} -->

### "Protejamos el río Magdalena"

<!-- /wp:heading -->

<!-- wp:image {"id":80986,"sizeSlug":"large"} -->

<figure class="wp-block-image size-large">
![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/02/b3cae1d2-ff26-468e-84e1-9b0ecefbab77-1024x576.jpg){.wp-image-80986}

</figure>
<!-- /wp:image -->

<!-- wp:paragraph -->

La lucha de las comunidades de campesinos, pescadores e indígenas en contra del desarrollo de actividades extractivas comenzó en 2015, "*las comunidades se han opuesto por la inminente destrucción ambiental del río Magdalena y por las consecuencias negativas de esta explotación para la pesca, de la que dependen cientos de familias en la zona, se han planteado acuerdos y reuniones con las empresas y la CAR, pero esto parece ser echado en saco roto"*, afirmó Huertas.  
  
Y recordó que esta adjudicación va en contravía de las peticiones de la comunidad, teniendo en cuenta que meses atrás habían presentando una carta con 6.759 firmas solicitando la detención de esta licencia. *"pescadores, campesinos y habitantes ribereños de Girardot y Coello protestan pacíficamente con la esperanza de ser escuchados, haciendo valer justamente sus derechos al trabajo, al ambiente sano y la salud"*.

<!-- /wp:paragraph -->
