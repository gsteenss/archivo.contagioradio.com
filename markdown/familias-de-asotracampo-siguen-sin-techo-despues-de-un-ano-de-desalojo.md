Title: Familias de Asotracampo siguen sin techo después de un año de desalojo
Date: 2016-12-13 17:37
Category: DDHH, Nacional
Tags: Desalojo El Tamarindo, despojo de tierras, Ley de Restitución de Tierras
Slug: familias-de-asotracampo-siguen-sin-techo-despues-de-un-ano-de-desalojo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/12/16998490526_b783c47073_o.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Peacepresence] 

###### [13 Dic 2016] 

Después de un año del desalojo de la finca El Tamarindo en Barranquilla, donde habitaban más de 120 familias desplazadas a causa del accionar paramilitar en el Cesar, Magdalena, Urabá y Bolívar, la Asociación de Trabajadores del Campo ASOTRACAMPO, conformada en 2012 por estas familias, **denuncia a través de un comunicado que hasta el momento no han recibido las garantías de restitución prometidas por el Estado.**

La Asociación manifiesta que hasta la fecha ninguna de las instituciones estatales responsables como la Unidad para la Atención y Reparación Integral a las Víctimas, la Agencia Nacional de Tierras y la Consejería Presidencial para los Derechos Humanos, han asumido su responsabilidad en el caso, **“no se han articulado de manera efectiva, exponiendo las 62 familias miembros de ASOTRACAMPO**, 52 de ellas registradas como víctimas del conflicto armado, **a condiciones de completa vulnerabilidad”.**

La organización campesina señala que en tiempos de transición hacia una paz duradera, sienten una profunda indignación porque “las víctimas de múltiples desplazamientos por el conflicto armado y campesinos en condiciones vulnerables por las causas estructurales del conflicto de la distribución desigual de la tierra, **son nuevamente víctimas de un despojo por intereses económicos, sin ninguna garantía de protección de sus derechos básicos por parte del Estado en todos sus niveles”.**

Por ultimo, en el comunicado organizaciones defensoras de derechos humanos y las familias afectadas, exigen al Gobierno Santos, a la Alta consejería para los Derechos Humanos, la Unidad para la Atención y Reparación Integral a las Víctimas, a la defensoría del pueblo y a organizamos internacionales, **atención, seguimiento y respuesta efectiva frente a la situación de crisis humanitaria que atraviesan.**

[Carta](https://www.scribd.com/document/334128065/Carta-Asotracampo#from_embed "View Carta Asotracampo on Scribd")Asotracampo by [Contagioradio](https://www.scribd.com/user/276652802/Contagioradio#from_embed "View Contagioradio's profile on Scribd") on Scribd

<iframe id="doc_94500" class="scribd_iframe_embed" src="https://www.scribd.com/embeds/334128065/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-1IMrqcIJb9Sob11IpmuU&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="0.7729220222793488"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
