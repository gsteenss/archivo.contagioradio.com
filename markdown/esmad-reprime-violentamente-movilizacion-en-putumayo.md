Title: ESMAD reprime violentamente movilización en Putumayo
Date: 2016-08-16 14:04
Category: DDHH, Nacional
Tags: Amerisur en Putumayo, erradicación cultivos ilícitos, programas de sustitución de cultivos, Represión contra movilización en Putumayo
Slug: esmad-reprime-violentamente-movilizacion-en-putumayo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/08/Represión-comunidades-Putumayo.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Juventud Rebelde ] 

###### [16 Ago 2016 ] 

Durante el fin de semana los pobladores que se movilizaban contra el accionar de la petrolera Amerisur y la insistencia del Gobierno de continuar con la erradicación forzada de cultivos de uso ilícito en Putumayo, fueron **violentamente reprimidos por miembros del ESMAD** y el Ejército Nacional, según afirma, desde Puerto Caicedo el poblador Germán Carache, miembro de la 'Mesa Regional de Organizaciones Sociales del Putumayo'.

En los puntos de concentración, como Yarumo, en el municipio de Orito, San Pedro en Caicedo, y San Miguel, las comunidades fueron hostigadas por miembros del ESMAD que **lanzaron gases lacrimógenos y recalzadas**; mientras que integrantes del Ejército hicieron señalamientos contra varios de los líderes y anunciaron judicializaciones contra algunos de los participantes de la movilización.

De acuerdo con Carache, pese a que las comunidades han presentado las propuestas de sustitución de cultivos ilícitos que vienen construyendo desde 1996, el **Gobierno nacional insiste en la erradicación forzada** y [[en imponer programas](https://archivo.contagioradio.com/gobierno-no-quiere-sustitucion-concertada-de-cultivos-de-uso-ilicito-en-putumayo/)] como el Plan Colombia o el Plan Consolidación o el programa Familias Guardabosques, que ya han sido implementados en la región, sin mayores resultados.

"Nunca hemos dicho que estamos defendiendo la mata de coca (...) estamos buscando una solución a la problemática social", afirma el poblador y agrega que **22 meses de discusión con el Gobierno no han posibilitado avances** en las cuatro exigencias que plantearon frente a la implementación de un programa de sustitución concertado con las comunidades; la discusión de la política mineroenergética; las garantías de derechos humanos para líderes y mayor inversión social.

Pese a la represión, la **movilización continúa en siete puntos del departamento**, entre ellos en los municipios de Puerto Caicedo, Puerto Asís y San Miguel, en los que más de 700 pobladores se concentran para exigir espacios de discusión con funcionarios del Gobierno con capacidad de decisión, mientras que el Ejército les [[anunció citaciones judiciales por obstruir las vías](https://archivo.contagioradio.com/comunidades-denuncian-judicializacion-injusta-contra-lideresa-jani-silva/)].

<iframe src="https://co.ivoox.com/es/player_ej_12565522_2_1.html?data=kpeimJqZdpOhhpywj5aXaZS1k5aah5yncZOhhpywj5WRaZi3jpWah5yncajZ09LO0JCnpdPVxM3Sh5enb67Z1MaYtMrLrdDiwtGYxsqPk9PbwtPW3MbHrdDixtiYtdTHrcLgxpKSmaiRh9Di1cbUy9SPlsLYytSYj4qbh46o&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)] ] 
