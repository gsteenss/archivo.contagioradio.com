Title: Los informes ocultos de Hidroituango
Date: 2020-09-24 18:17
Author: PracticasCR
Category: Columnistas invitados, Opinion
Tags: opinion
Slug: los-informes-ocultos-de-hidroituango
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/09/Hidroituango.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

*Un informe presentado por el alcalde Daniel Quintero sobre Hidroituango dejó al descubierto que Federico Gutiérrez y exmiembros de la Junta de Empresas Públicas de Medellín -[EPM](https://www.epm.com.co/site/)- ocultaron información. ¿A quién protegían?*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

La semana pasada fue presentado por el alcalde de Medellín, Daniel Quintero, ante la opinión pública y las autoridades competentes (Fiscalía y Contraloría), un revelador informe sobre el proyecto Hidroituango, el cual, fue elaborado por expertos en geología y mecánica de suelos, contratados por EPM y  MAPFRE, la reaseguradora del proyecto.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El informe indica, que la suma de varios problemas no resueltos en relación al diseño y la supervisión del proyecto, fueron las causas del colapso de los túneles de desviación y posteriormente del proyecto hidroeléctrico más importante del país. (Lea también: [Lo que hay detrás de los exmiembros de la Junta Directiva de EPM](https://archivo.contagioradio.com/lo-que-hay-detras-de-los-exmiembros-de-la-junta-directiva-de-epm/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Además, detalla que algunos materiales que fueron utilizados para la elaboración de la obra, eran de menor calidad a los que debieron ser usados para un proyecto de esta envergadura.  El informe que fue entregado por Quintero a la Fiscalía dejó sin aire a quienes desvirtúan todas las acciones tomadas por el Alcalde, con el fin de esclarecer las causas reales y los culpables de los sobrecostos del proyecto, que hoy superan los 9. 9 billones de pesos.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Las conclusiones del estudio, que en palabras del mandatario de los medellinenses es “*demoledor*”  fechado el 2 de agosto del 2019, deja muy mal parado al exalcalde Federico Gutiérrez y a la Junta Directiva de EPM saliente, la cual había renunciado de manera masiva, al conocer la demanda que iniciaría el alcalde Daniel Quintero contra los diseñadores y constructores de la obra; hecho que también fue utilizado por sectores políticos del “Uribismo” y el “Fajardismo” para desprestigiar a Quintero y hasta proponer la revocatoria de su mandato. (Lea también: [El complot de los que perdieron con Daniel Quintero](https://archivo.contagioradio.com/el-complot-de-los-que-perdieron-con-daniel-quintero/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Sin embargo, ante las revelaciones presentadas en el informe de Hidroituango, en diferentes medios de comunicación, ninguna explicación han dado; se han limitado a decir que nunca conocieron la existencia de ese documento, excusa poco creíble y hasta risible teniendo en cuenta que dicho informe fue elaborado por expertos contratados por la reaseguradora de la empresa que ellos mismos dirigían.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

No cabe duda que Daniel Quintero se convirtió en la piedra en el zapato para sectores políticos y económicos que han gobernado Medellín, los mismos que engrosaron sus cuentas bancarias a cuestas de EPM. No obstante, a pesar de toda la presión que le han ejercido, Quintero ha continuado firme en su postura de avanzar en la demanda contra los constructores y diseñadores, del proyecto hidroeléctrico, quienes al parecer serían los responsables de las  fallas que han colocado en riesgo la vida de muchas personas, y que le han costado a la empresa miles de millones de pesos.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El Alcalde dejó en conocimiento de la Contraloría General de la Nación y de la Fiscalía, los hallazgos detallados en este revelador informe esperando que las autoridades competentes aclaren si es verdad que el ex alcalde Gutiérrez y su Junta Directiva nunca conocieron el informe, o si por el contrario guardaron silencio con la intención de proteger sus propios intereses o proteger a alguien más. (Lea también: [Los conflictos de intereses entre la exdirectiva de EPM y el GEA](https://archivo.contagioradio.com/los-conflictos-de-intereses-entre-la-exdirectiva-de-epm-y-el-gea/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

**Por: Robinson López @RobinsonLopezA - Robinlopez3213@gmail.com**

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
