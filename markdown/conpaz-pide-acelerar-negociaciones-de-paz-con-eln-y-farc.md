Title: CONPAZ propone cese bilateral para iniciar conversaciones de paz con el ELN
Date: 2016-03-22 12:51
Category: Nacional, Paz
Tags: Conpaz, FARC ELN, proceso de paz
Slug: conpaz-pide-acelerar-negociaciones-de-paz-con-eln-y-farc
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/03/CONPAZ.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: CONPAZ 

<iframe src="http://co.ivoox.com/es/player_ek_10897540_2_1.html?data=kpWlm5yZeJGhhpywj5WUaZS1kZWah5yncZGhhpywj5WRaZi3jpWah5yncaTDr7WuvJDUrcXZjMbQx9HJtsLmjNPSydTHrcLXytTbx9iPqMaf0cbnjcjTsoy5rbOY25CqhbOhhpywj6jTstXVyM7cjbfFqMrjjJKSmaiReA%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Juan Martínez, CONPAZ] 

###### [22 Mar 2016] 

Este fin de semana la Red, CONPAZ, Comunidades Construyendo Paz en los Territorios [ envió un comunicado a las delegaciones de paz del gobierno y las FARC](https://comunidadesconpaz.wordpress.com/2016/03/20/iniciativas-humanitarias-a-iniciativas-territoriales-de-paz/)que están en la Habana y otra a [la guerrilla del ELN](https://comunidadesconpaz.wordpress.com/2016/03/20/es-importante-iniciar-la-mesa-para-ambientar-la-solucion-del-conflicto-carta-conpaz-al-eln/)también dirigida al gobierno para que se aceleren las conversaciones de paz en Cuba, y los diálogos con el ELN pasen a su fase pública.

De acuerdo con Juan Martínez, vocero de red, las comunidades que han vivido la guerra en sus territorios se encuentran preocupadas por los estancamientos en ambas mesas, y aseguran seguir firmes en que sus territorios sean el lugar donde guerrilleros tanto de las FARC como del ELN lleven a cabo sus trabajos de reparación de las víctimas y proyectos productivos.

Así mismo, por medio de los comunicados ponen a disposición sus iniciativas de territorios de paz  que son reconocidos por otros gobiernos, por la Corte Interamericana de Derechos Humanos, las Naciones Unidas y oros organismos internacionales.

“Pensamos que si se firman los acuerdos con las FARC y no con el ELN quedaría incompleto el punto del fin del conflicto”, dice Martínez, asegurando que para que se pueda construir la paz en Colombia debe tenerse en cuenta a las dos guerrillas para forjar una nueva democracia.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)
