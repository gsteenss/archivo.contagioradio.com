Title: Informe da cuenta de victimización LGBTI en el conflicto armado colombiano
Date: 2015-12-10 12:16
Category: eventos, LGBTI
Tags: 'Aniquilar la diferencia’, Centro de Memoria Histórica, Comunidad LGBTI, LGTBI en el conflicto colombiano
Slug: informe-da-cuenta-de-victimizacion-lgbti-en-el-conflicto-armado-colombiano
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/12/gaygimp_12.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### [10 Dic 2015]

El Centro Nacional de Memoria Histórica presenta el informe:**'Aniquilar la diferencia’: Lesbianas, gays, bisexuales y transgeneristas en el marco del conflicto armado colombiano**, publicación resultante del trabajo de investigación adelantado durante dos años en los departamentos de  Antioquia, Bolívar, Cundinamarca y Nariño.

El estudio desarrollado con **63 víctimas del conflicto armado pertenecientes a la población LGTBI**, da cuenta de la grave situación de la victimización de quienes han sufrido por la eliminación material con los asesinatos selectivos y los desplazamientos forzados, y la eliminación simbólica de quienes han logrado sobrevivir borrando su identidad sexual por completo.

Además de hacer un llamado a la memoria y al reconocimiento de lo padecido por la comunidad LGTBI por actores armados involucrados en el conflicto nacional, el trabajo destaca la resistencia y fuerza de las víctimas por sobrevivir y transformar su entorno, apoyado en cifras y estadísticas que sustentan su razón de ser.

El informe esta dividido en cuatro capítulos temáticos, en los que se analizan las condiciones que permitieron la violencia, las acciones e identificación de las víctimas, las afectaciones, individuales y colectivas provocadas y las formas de resistencias surgidas por partede las víctimas, cerrando con algunas conclusiones y recomendaciones propuestas a partir del trabajo.

**'Aniquilar la diferencia’** será presentado a las 5:30 de la tarde de este jueves 10 de diciembre, en el marco del Día Internacional de los Derechos Humanos, conto con el apoyo de la Agencia de Estados Unidos para el Desarrollo (USAID) y la Organización Internacional para las Migraciones (OIM). El lugar establecido para su socialización es auditorio Rogelio Salmona del Centro Cultural Gabriel García Márquez, en Bogotá, con entrada libre hasta completar el aforo del escenario.
