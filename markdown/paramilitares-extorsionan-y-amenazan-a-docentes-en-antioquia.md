Title: Paramilitares extorsionan y amenazan a docentes en Antioquia
Date: 2016-05-02 12:19
Category: DDHH, Nacional
Tags: docentes amenazados, paramilitares, vigia del fuerte
Slug: paramilitares-extorsionan-y-amenazan-a-docentes-en-antioquia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/05/Paramilitares....png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Chocó ORG ] 

###### [2 Mayo 2016]

El pasado viernes, docentes de dos instituciones educativas de Vigía del Fuerte, Antioquia, fueron amenazados y extorsionados con una llamada telefónica de quien se identificó como el Comandante 'Ricardo López' del 'Clan Úsuga', en la que exigió "una colaboración" de entre \$4 y \$20 millones que de no llegar a la estructura paramilitar, llevaría a la **muerte y el desplazamiento forzado de los maestros**.

La primera llamada fue a un docente de la 'Institución Educativa Embera Atrato Medio', a quien el Comandante le exigió \$20 millones, amenazándolo con que podrían asesinar a uno de sus compañeros de no cumplir "la orden". A los maestros de la 'Institución Educativa Vigía del Fuerte', el Comandante les anunció que tenían **"24 horas para abandonar el pueblo" y que debían dar \$4 millones**.

El Comandante arremetió con tono amenazante, insultos y comentarios que insistían en **hacer efectivas sus amenazas de muerte y desplazamiento forzado**. El hecho se da en el marco de la presencia y control territorial que ejerce el grupo paramilitar [['Clan Úsuga'](https://archivo.contagioradio.com/?s=clan+usuga+)], en la cabecera municipal de Vigía del Fuerte.

Los docentes exigen que las autoridades locales y nacionales emitan alertas pertinentes, actúen eficientemente y **brinden las garantías necesarias para el cumplimientos de sus derechos fundamentales**.

###### [Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)]][ ] 
