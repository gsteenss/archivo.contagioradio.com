Title: Vacancia de PPK en Perú sería una oportunidad para los movimientos sociales
Date: 2017-12-21 10:04
Category: El mundo, Política
Tags: Fujimori, Odebrecht, Pedro Pablo Kuczinsky, Perú
Slug: vacancia-ppk-peru
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/12/ppk-mano-5.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Exitosa 

###### 20 Dic 2018 

Tras la aprobación en el Congreso peruano de debatir este jueves la “vacancia” que destituiría de su cargo al actual presidente Pedro Pablo Kuczynski (PPK) por sus presuntos vínculos con el caso Odebrecht, desde las organizaciones sociales aseguran que el panorama político en el país andino se torna cada vez más complejo.

La solicitud de destitución por “incapacidad moral permanente” se da a partir de las relaciones que la empresa brasilera plasmó en un documento en el que se especifica que Westfield Capital, empresa unitaria de PPK, recibió dineros en tiempos en que se desempeñaba como ministro de economía y primer ministro de 2001 a 2006, vínculos negados reiteradamente por el mandatario.

**Una tormenta perfecta**

Rocio Silva Santiesteban, defensora de DDHH en Perú, que la confluencia de situaciones es similar a “una tormenta perfecta” que el fujimorismo esta aprovechando para desviar la atención sobre las cada vez más fuertes versiones que apuntarían que Keiko Fujimori, ex candidata presidencial de Fuerza popular, estaría implicada en el caso Odebrecht.

En ese sentido, la mayoría parlamentaria que representa el fujimorismo estaría pretendiendo cuestionar al fiscal de la nación, entidad que la semana anterior, en cabeza del fiscal José Domingo Pérez, allano dos locales del movimiento buscando evidencia que compruebe los indicios que apuntan  a que habría recibido dineros para su campaña política.

Otro de los movimientos mencionados por la activista, están relacionados con la demanda presentada por la esa bancada ante la subcomisión de acusaciones contra 4 jueces del tribunal constitucional por el caso de la cárcel “El Frontón” durante el primer gobierno de Alan García al considerar que había un “error material” en el conteo de votos que corregir.

En su consideración, al poner en cuestión al poder judicial, el tribunal constitucional y a la Fiscalía de la nación, permitiría al fujimorismo  coptar al tribunal constitucional  y de paso restar credibilidad a las investigaciones que puedan adelantarse en contra de su líder.

**Corrupción y crisis política.**

La defensora de DDHH asegura que las revelaciones que han salido a la luz tras el caso de corrupción de Odebrecht han permitido vislumbrar “toda la podredumbre interna que hay entre ese vínculo de las grandes empresas que contratan con el Estado y los políticos que gobiernan”.

Con la solicitud de vacancia a PPK e investigación a Keiko Fujimori, la crisis política  suma nuevos nombres a los ya vinculados de alto nivel: dos presidentes presos Alberto Fujimori y Ollanta Humala (prisión preventiva), Alejandro Toledo con solicitud de prisión en trámite de extradición y Alan García por el mismo caso.

Adicionalmente el 4 de diciembre se ordenó medida de prisión preventiva por 18 meses, contra los 4 gerentes de las 4 más grandes empresas constructoras del Perú,  por presuntamente haber coordinado sobornos en conjunto al expresidente Alejandro Toledo para obtener la licitación de una vía que une a Brasil y Perú.

**Movimientos sociales frente al cambio**

La indignación ante lo que viene ocurriendo con la clase política peruana y particularmente las pretensiones de Fuerza Popular en el Congreso, ha logrado convocar movilizaciones particularmente. El pasado sábado 16 de diciembre cerca de 2000 personas se encontraron en la Plaza San Martín en lo que denominaron "marcha contra el golpe fujimorista"

Silva asegura que los peruanos y peruanas "tenemos que movilizarnos y manifestarnos los peruanos, por que no podemos permitir que las mafias gobiernen" así mismo llama a las organizaciones sociales a "tener cordura y exigir que se respete el estado de derecho, más que un lema político es un lema cívico y ético, porque estamos ante el derrumbe absoluto de la clase política en el Perú"

"El congreso está cuestionado, el poder ejecutivo está cuestionado en este momento lo único que tenemos es el poder judicial" mismo que asegura estuvo cuestionado en años anteriores por estar coptado por el fujimorismo. "Lo que debemos hacer es respaldar a los jueces y fiscales probos por que es el único elemento institucional que está más o menos libre" manifiesta.

A pesar que tienen sus propias crisis internas, los movimiento sociales cuando se hace una llamada a la ciudadanía sale a las calles y se moviliza, en parte por que como asegura Silva “si hay una fuerza importante en el Perú es el anti-fujimorismo” y que por una lógica de “el mal menor” llevó a la presidencia a PPK y a Ollanta Humala, misma que hoy está en crisis y que tiene a los peruanos exigiendo “que se vayan todos”

En un escenario de vacancia presidencial y de darse la renuncia del vicepresidente Martin Vizcarra, quien asumiría el primer cargo el presidente del Congreso Luis Galarreta del fujimotista Fuerza Popular, que en tal caso debería convocar a nuevas elecciones y en la que a criterio de Silva abriría el camino para que el Fujimorismo copte todas las instituciones del Estado como en 1992, temor que ante el cual Silva reflexiona “Que se vayan todos pero ¿Cómo?”

Para este miércoles diferentes organizaciones están convocando a una gran Marcha contra la corrupción y en defensa de la democracia en las diferentes provincias del Perú, solventada en 4 ejes centrales: La corrupción infestada en la política peruana, que la solicitud de vacancia que enfrenta **PPK **se realice en la forma debida y se respete el debido proceso, la amenaza golpista del **fujimorismo** y la responsabilidad de los partidos políticos y gobernantes corruptos en la grave crisis en la que se encuentra el Perú.

<iframe id="audio_22782252" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_22782252_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio]{.s1}](http://bit.ly/1ICYhVU)[.]{.s1}
