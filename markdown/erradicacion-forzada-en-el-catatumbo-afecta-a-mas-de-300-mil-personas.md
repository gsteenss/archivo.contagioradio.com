Title: Erradicación forzada en el Catatumbo afecta a más de 300 mil personas
Date: 2017-09-14 15:24
Category: DDHH, Nacional
Tags: Catatumbo, sustitución de cultivos ilícitos
Slug: erradicacion-forzada-en-el-catatumbo-afecta-a-mas-de-300-mil-personas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/09/hoja-de-coca-1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Jujuy al día] 

###### [14 Sept 2017] 

Campesinos del Catatumbo, en Norte de Santander están denunciando los incumplimientos por parte del **Gobierno Nacional, que habría dado la orden para que se erradicaran forzadamente 25 mil hectáreas**, pertenecientes a campesinos que ya habían manifestado su voluntad de pertenecer a los planes de sustitución de cultivos de uso ilícito. Situación que de acuerdo con Juan Carlos Quintero podría gestar un proceso de movilización y paro.

De acuerdo con Quintero, vocero de ASCAMCAT, esto es un incumplimiento a los acuerdos de paz que desgasta la confianza de los campesinos hacia el gobierno “nos sorprendió que el **Ministro de defensa anunciara que van a erradicar de manera forzosa 25 mil hectáreas de cultivos de coca en la zona del Catatumbo”**. (Le puede interesar:["Organizaciones piden que no se militarice lucha contra cultivos de uso ilícito"](https://archivo.contagioradio.com/ong-nacionales-e-internacionales-preocupados-por-erradicacion-forzada-de-cultivos-en-colombia/))

Quintero manifestó que en el punto 4 de los acuerdos se establece que serán los campesinos voluntariamente los que se acojan a los planes de sustitución que, además serán concertados, en este sentido el vocero de ASCAMCAT señaló que tres días antes se había firmado un acuerdo de voluntades en el municipio de Tibú, **con 6 asociaciones de juntas de acción comunal, que representan a 15 mil personas, que actualmente continúan con sus cultivos ilícitos de coca**.

### **Riesgos de enfrentamiento** 

Quintero denuncio que ayer en horas de la tarde, desembarcaron hombres del Ejercito Nacional y del ESMAD en la vereda Corinto, en el municipio de Tibú, que provocó enfrentamientos entre campesinos y Fuerza Pública, **otra de las veredas que serían erradicadas de cultivos ilícitos es La Treinta, ubicada en el municipio de Sardinata**.

Además, los campesinos han manifestado que el Programa de Desarrollo con Enfoque Territorial, acobijaría no solo a Tibú, sino también Sardinata, Acarí, San Calixto, El Tarra, Teorama, Convención y El Carmen, razón por la cual, no comprenden porque se continúan con la erradicación forzada. **Con estas erradicaciones Quintero manifestó que se esta afectando la supervivencia de 300 mil personas. **(Le puede interesar: ["Campesinos del Putumayo logran acuerdos de sustitución"](https://archivo.contagioradio.com/campesinos-del-putumayo-logran-acuerdos-de-sustitucion-de-cultivos/))

Frente a esta situación los campesinos han manifestado que agotaran todas las instancias pacíficas de resolución de conflictos, sin embargo, ya que esta no es la primera vez en que se incumplen los acuerdos de paz y al campesinado, el día de hoy diferentes organizaciones y **representaciones de los campesinos se reunirán para determinar si el siguiente paso es convocar a un paro**, para garantizar cumplimientos en los acuerdos de paz.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
