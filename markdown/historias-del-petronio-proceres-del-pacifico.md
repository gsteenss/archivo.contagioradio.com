Title: Historias del Petronio: Próceres del Pacífico
Date: 2019-08-16 10:14
Author: CtgAdm
Category: Comunidad, Cultura
Tags: historia, pacífico, Petronio Álvarez
Slug: historias-del-petronio-proceres-del-pacifico
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/08/Próceres-Petronio.png" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/08/Sinisterra.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: viviendocali] 

**Célimo Sinisterra, natural de Timbiquí** e integrante del **Comité Académico del Festival de Música del Pacífico**, Petronio Martínez, recorre los espacios del festival, compartiendo con los visitantes, todo su conocimiento sobre la diáspora africana y la ancestralidad que caracterizan a los departamentos que hacen parte del Pacífico colombiano.

Sinisterra quien viene trabajando en el Quilombo Pedagógico enseña a quienes llegan al Petronio sobre algunas de las figuras más emblemáticas de la cultura afro como **Barûle, importante afrodescendiente, vendido de Jamaica a Chocó o Benkos Biohó**, considerado al palenque más importante de Colombia quien lideró la rebelión de los cimarrones hasta su captura y ejecución en  1621.

Dentro de las anécdotas que narra Sinisterra también está la de **Luis Antonio Robles**, primer congresista negro de Colombia, quien ante el desprecio que mostraron en el Congreso a su llegada respondió, "no tengo la culpa de ser negro, la noche imprimió su manto en mi epidermis pero allá en las bovedas de Cartagena aún blanquean los huesos de mis antepasados para darle libertad a personas blancas de consciencia negra como ustedes".

También pasó a la historia **Casilda Cundumí Dembelé**, nacida en Malí, Africa y quien llegó esclavizada a Palmira donde trabajo en el ingenio Manuelita como esclava y quien tras obtener su libertad procedió con su proceso de abolición liberando Cerrito, Guacarí y a los corregimientos aledaños a Palmira.  [(Le puede interesar: Historias del Petronio: 25 años retratando el poder, la riqueza y el color del Pacífico) ](https://archivo.contagioradio.com/historias-del-petronio-25-anos-retratando-el-poder-la-riqueza-y-el-color-del-pacifico/)

\[caption id="attachment\_72230" align="aligncenter" width="1024"\]![Petronio Célimo](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/08/Sinisterra-1024x683.jpg){.size-large .wp-image-72230 width="1024" height="683"} Foto: Contagio Radio\[/caption\]

Otro de aquellos personajes fue **Manuel Saturio Valencia, primer abogado negro de la Universidad del Cauca,** quien según Sinisterra, además de ser el escritor de cuatro novelas y múltiples ensayos, fue el último fusilado en el departamento del Chocó, inculpado de un incendio ocurrido en el centro de la ciudad, dicen, como venganza por enamorar a Deyanira Castro, una joven blanca perteneciente a las élites de Quibdó.

Sinisterra hace alusión finalmente a **Juan José Nieto Gil,** un personaje que sigue oculto para un sector de la historia de Colombia pero que sirvió como presidente de la nación en 1861, 10 años después de la abolición de la esclavitud, además fue el autor de Ingermina o la Hija de Calamar, obra que sigue invisibilizada según el integrante del Comité Académico por su origen negro.

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
