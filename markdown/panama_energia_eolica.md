Title: Panamá aumentará su producción de energía eólica un 63%
Date: 2017-05-26 18:15
Category: Ambiente, El mundo
Tags: energía renovable, Panama
Slug: panama_energia_eolica
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/05/Energía-eolica-sostenible-verde-ambiente-e1495840773225.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: AFP 

###### [May 26 de 2017] 

Panamá da pasos en la lucha contra el cambio climático. El gobierno panameño anunció que para el 2018 incrementará su producción de energía eólica en un 63%, en el marco de su Plan Energético Nacional,  que **como objetivo busca que para 2050 el 30% de su energía sea renovable.**

El proyecto contempla que las operaciones del ambicioso plan inicien sus operaciones en el primer trimestre de 2018, con una capacidad de 100 megavatios de energía renovable. En en total del año se espera que la producción  del **Sistema Interconectado Nacional en ese tipo de energía  aumente en más de 170 megavatios.**

Con esta inversión Panamá se convierte en uno de los principales países latinoamericanos productores de energía eólica. De hecho ese país tiene el mayor parque eólico de Centroamérica y el Caribe. Se trata del **Penonomé, que durante el 2016, sus 108 turbinas generaron el 7 %** de toda la electricidad que se consumió la nación.

El nuevo proyecto estará en la provincia de Coclé donde habrá un 63% adicional de energía eólica respecto a la capacidad anterior. Los otros dos iniciarán en diciembre, ya que se pretende que dicha producción sea usada para la temporada seca del 2019.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
