Title: Luego del paro en Remedios y Segovia han sido detenidos 20 mineros
Date: 2017-10-03 12:54
Category: Movilización, Nacional
Tags: mineros artesanales, mineros de remedios y segovia, Paro de mineros, Remedios y Segovia
Slug: luego-del-paro-de-segovia-y-remedios-han-sido-detenidos-40-mineros
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/08/Planton-por-Mineros-Segovia-14.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Instituto Popular de Capacitación] 

###### [3 Oct 2017] 

A pesar de los acuerdos que se pactaron, los mineros de Remedios y Segovia en Antioquia, **denunciaron que han sido víctimas de persecución** por parte de la empresa Gran Colombia Gold y de la Policía Nacional. La Mesa Minera indicó que ha habido detenciones arbitrarias y sus miembros han sido amenazados.

De acuerdo con Jaime Gallego, integrante de la Mesa Minera, tras haber logrado acuerdos para que se establezca la formalización del trabajo de los mineros, **“hay gente interesada en que no se cumpla con lo acordado** por intereses políticos y propios, el Gobierno Nacional y a la multinacional, quedaron con un malestar luego de los acuerdos”.

Dijo además que algunas personas han sido detenidas y **“hay rumores de que existe una lista para detener** a los miembros de la Mesa Minera”. Indicó que han sido 20 los mineros detenidos, “aún hay más de 60 órdenes de captura” y los integrantes de la mesa han sido amenazados de muerte en varias ocasiones. (Le puede interesar: ["Estos fueron los acuerdos que levantaron la protesta de mineros en Remedios y Segovia"](https://archivo.contagioradio.com/mineros-de-remedios-y-segovia-logran-formalizacion-de-sus-actividades/))

### **Las razones de las capturas** 

Gallego explica que la situación para los mineros artesanales **no ha cambiado en nada**. Una de las razones de la protesta es que se está impidiendo la compra del oro a mineros artesanales y, la Policía y la empresa, a pesar de los acuerdos siguió con esa práctica. Es decir que en este punto de sobrevivencia también se incumplió.

Esta situación los empuja  a la ilegalidad puesto que **deben vender el oro así sea solamente para recuperar la inversión.** Sin embargo, al intentar venderlo en el mercado formal, el mineral no es recibido por lo que acuden al mercado ilegal, esto los convierte en ilegales y por ello se estarían presentando las capturas. (Le puede interesar: ["Informe ratifica grave situación de DDHH en Remedios y Segovia"](https://archivo.contagioradio.com/situacion-de-ddhh-en-segovia-y-remedios/))

### **Acuerdos con los mineros no se han cumplido** 

Gallego informó que, por el lado de los acuerdos con la empresa, “lo único que ha hecho la multinacional es llamar personalmente a cada mina para **ofrecer el mismo contrato que tenían antes** de la movilización”.  Por el lado del Gobierno Nacional, ha habido reuniones con el ministerio de Minas para discutir las políticas estatales de mineros ancestrales. Los mineros han presentado alternativas para cambiar la política minera en el país.

Ellos le han pedido al Gobierno que, mientras ocurre el proceso de formalización que fue acordado, **“desbloqueen la comercialización de oro** para que los mineros lo puedan vender”. Con el bloqueo, “los mineros no tienen otra opción que vender el oro de manera ilegal y ahí es cuando sucede la criminalización, queremos vender el oro de manera legal pero no dejan, ahí ¿qué hacemos?”.

Frente a esta situación, **le han pedido al gobierno que cesen las persecuciones** y que se solucione la comercialización porque “todos los mineros del país están aguantando hambre”. Piden que se siga adelante con la formalización equitativa, que no sea impuesta y que se agilicen los cumplimientos de lo acordado en meses pasados. (Le puede interesar:["Policía usa francotiradores para reprimir mineros en Remedios y Segovia"](https://archivo.contagioradio.com/mineros-de-remedios-y-segovia-denuncian-presencia-de-francotiradores-en-el-paro/))

Finalmente, Gallego manifestó que si continúan las actitudes de la multinacional, que está incumpliendo con los acuerdos, la comunidad de Remedios y Segovia **podría tomar la decisión de retomar las movilizaciones** de manera pacífica.

<iframe id="audio_21240179" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_21240179_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 

<div class="osd-sms-wrapper">

</div>
