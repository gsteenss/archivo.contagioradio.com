Title: Propuesta de Reforma a la Justicia de C.Democrático busca impunidad sobre investigaciones a Uribe
Date: 2018-09-26 16:13
Author: AdminContagio
Category: Nacional, Política
Tags: alvaro uribe velez, Centro Democrático, Iván Cepeda, Reforma a la Justicia
Slug: propuesta-de-reforma-a-la-justicia-de-c-democratico-busca-impunidad-sobre-investigaciones-a-uribe
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/02/uribe-1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: El Heraldo] 

###### [26 Sept 2018] 

Un nuevo proyecto de reforma a la justicia fue presentado por la senadora Paloma Valencia del Centro Democrático, con el que proponen la unificación de las altas Cortes, crear el Tribunal Supremo Constitucional y eliminar el sistema de aforados. Iniciativa que para el senador Iván Cepeda, del Polo Democrático, **solo busca generar impunidad en torno a las investigaciones adelantadas en contra del ex presidente Álvaro Uribe Vélez.**

Para Cepeda, las intensiones detrás de esta propuesta son varias; una de ellas es hacer "mucho más difícil" el proceso de esclarecimiento de graves crímenes de lesa humanidad y de otro tipo de conductas que actualmente examina la Corte Suprema de Justicia, en las que esta relacionado Uribe Vélez. (Le puede interesar: ["Los dudosos entornos del Centro Democrático"](https://archivo.contagioradio.com/dudosos-entornos-del-centro-democratico/))

"Como se sabe en la Corte Suprema están haciendo curso, al menos, una veintena de procesos en los cuales se acusa a Uribe de distintos tipos de delitos, incluso unos muy graves como la conformación de grupos paramilitares, **concierto para delinquir, el haber determinado la perpetración de masacres y otra clase de hechos**" afirmó Cepeda.

Asimismo, el senador del Polo Democrático aseguró que la propuesta de retirar los fueros a los congresistas buscaría que la competencia sobre la investigación de delitos graves que se le imputen a altos funcionarios, queden en manos de fiscales **"comunes y corrientes"**, que tienen una alta vulnerabilidad a cualquier clase de presiones que se ejerzan desde esos lugares de poder.

Finalmente frente a la propuesta del Centro Democrático de unificar las Altas Cortes, Cepeda expresó que "causaría un grave daño a la independencia de la Justicia" y a todo el proceso jurisprudencial que se ha desarrollado en defensa de los derechos de la ciudadanía, y agregó que con la eliminación de la **Corte Constitucional, también se suprimirían competencias como la revisión de tutelas.**

### **Las propuestas de reforma a la justicia de Iván Duque y el Centro Democrático** 

Cepeda aseguró que tanto la iniciativa de reforma a la justicia que se radicó desde el gobierno Nacional como la que viene del Centro Democrático, intentan promover una serie de objetivos similares, sin embargo ninguna de las dos pretendería "democratizar la justicia, hacerla más cercana a la ciudadanía o resolver las problemáticas de falta de garantías en los procesos judiciales".

En esa medida manifestó que la bancada de Oposición ya estaría estudiando otra propuesta de reforma a la justicia y política, que tendrían como principal tarea **"reforzar y construir vías para la independencia de la justicia que la desligue del poder político"**.

<iframe id="audio_28914997" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_28914997_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
