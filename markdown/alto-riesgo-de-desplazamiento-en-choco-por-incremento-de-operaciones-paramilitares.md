Title: Alto riesgo de desplazamiento en Chocó por incremento de operaciones paramilitares
Date: 2016-01-15 11:12
Category: DDHH, Nacional
Tags: asesinato de campesinos en Salaquí, Autodefensas Gaitanistas de Colombia, Bajo Atrato chocoano, cacarica, Chocó, paramilitares, Salaquí
Slug: alto-riesgo-de-desplazamiento-en-choco-por-incremento-de-operaciones-paramilitares
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/01/gaitanistas_contagioradio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: radiomacondo 

<iframe src="http://www.ivoox.com/player_ek_10085821_2_1.html?data=kpWdmpqcdpKhhpywj5abaZS1k52ah5yncZOhhpywj5WRaZi3jpWah5yncaLg1dSY1M7Jt8jjjMnSjcnJt9Hgwt%2FOz87JstXjjMrbjajMs8SZpJiSpJiPtNDmjM7bxdfJscbi1dSYj4qbh4630NPhw8zNs4zGwsnW0ZCRaZi3jpk%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [15 Ene 2016] 

Pobladores de la región del Bajo Atrato Chocoano continúan denunciando la presencia de por lo menos 300 paramilitares, que asesinaron a 5 personas a principios de esta semana y que habrían sido responsables de la **desaparición de un número indeterminado de afrodescendientes**. También se afirmó que aumenta el riesgo de un desplazamiento interno de algunas comunidades o de todos los poblados de las dos cuencas de Salaquí y Cacarica.

Las comunidades denunciaron que las fuerzas **paramilitares se ubican en los poblados de "El rodadero" en cuenca del río "Salaquí" y se estarían movilizando hacia la comunidad de Truandó en la cuenca del rio "Cacarica**". Adicionalmente las unidades de las FFMM que controlan las bocas de los dos ríos estarían ubicadas a 10 minutos de los integrantes de las Autodefensas Gaitanistas de Colombia.

Según la denuncia de uno de los pobladores, la presencia paramilitar obedecería a la retoma del control militar y también  al **control de las rutas del narcotráfico que implican el puerto de Turbo en Antioquia y las vías de transporte entre esa ciudad y el municipio de Unguia.**

De acuerdo con la denuncia de la ‘Comisión Intereclesial de Justicia y Paz’, el pasado 10 de enero integrantes de grupos[**paramilitares asesinaron a 5  afrodescendientes**, en el trayecto entre ‘Caño Seco’ y ‘Playa Bonita](https://archivo.contagioradio.com/5-campesinos-asesinados-por-paramilitares-en-el-bajo-atrato-chocoano/)’, río Salaquí, Bajo Atrato chocoano, acusándolos de ser colaboradores de la guerrilla.

Esta noticia y más información en**[Otra Mirada](https://archivo.contagioradio.com/alaire.html)**, todos los días de 8 a 10 Am
