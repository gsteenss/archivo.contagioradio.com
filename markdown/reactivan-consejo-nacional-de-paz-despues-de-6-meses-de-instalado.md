Title: Reactivan Consejo Nacional de Paz después de 6 meses de instalado
Date: 2015-04-21 17:26
Author: CtgAdm
Category: Nacional, Paz
Tags: Consejo Nacional de Paz, Cumbre Agraria, Juan Manuel Santos
Slug: reactivan-consejo-nacional-de-paz-despues-de-6-meses-de-instalado
Status: published

###### Foto: emisoraatlantico.com 

Ante los últimos hechos relacionados con la guerra y que han afectado las conversaciones de paz que se desarrollan en La Habana, Cuba, el presidente **Juan Manuel Santos** anunció este martes 21 de Abril que se reactiva el **Consejo Nacional de Paz** con la idea de aportar para el avance certero de las conversaciones. Según el presidente Santos, este es un espacio de amplia participación y que representa a varios sectores de la sociedad colombiana.

Pasaron más de 10 años desde la última vez que el Consejo Nacional de Paz sesionó. Lo convocó el entonces presidente Álvaro Uribe en julio de 2004, tras el fallo de una acción de cumplimiento. Sin embargo, el instrumento nunca pudo ejercer sus labores, pues no pasó de ser un requisito formal.

Según el presidente Santos, es necesario que el Consejo Nacional de Paz, acelere, por una la pedagogía del proceso, y lo describe como "*un órgano asesor y consultor, y es ante todo  un escenario de participación ciudadana. Y sin duda el más representativo y plural para tratar el tema de la paz*.”

El presidente también señala que este espacio tiene dos **funciones,** por una parte **llevar a la presidencia las exigencias y reclamos de las comunidades que están allí representadas,** pero por otra parte **llevar a las comunidades** los acuerdos y las estrategias de aplicación de los avances que tenga la mesa de conversaciones de paz. Según el mandatario “*es fundamental para lograr cuatro elementos: articulación, incidencia, preparación y movilización*.”

**A pesar de esta decisión persisten las críticas**, puesto que el gobierno no le ha apostado de manera contundente a la construcción de la paz, primero porque no se declara un cese bilateral de fuego, como lo reclaman diversas organizaciones y lo reiteran en diferentes escenarios, y también porque muchos de los espacios de interlocución como la **Cumbre Agraria** no han resultado satisfactorias porque según el propio gobierno se están esperando los acuerdos de paz de la Habana, relegando las exigencias de la sociedad civil a los acuerdos a los que se llegue.

Para muchos analistas, la decisión de reactivar el Consejo Nacional de Paz responde más a una necesidad de impulso política, pero sin cambios de fondo, para enfrentar a sectores de opinión que han logrado movilizarse y manifestarse en contra del proceso de paz.

\[embed\]https://www.youtube.com/watch?v=hWusYDgtTaw&feature=youtu.be\[/embed\]
