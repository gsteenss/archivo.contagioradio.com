Title: Cese bilateral entre ELN y gobierno Nacional es Histórico: Álvaro Villarraga
Date: 2017-09-06 17:08
Category: Entrevistas, Paz
Tags: Diálogos en Quito, ELN
Slug: cese-bilateral-entre-eln-y-gobierno-nacional-es-hstorico-alvaro-villarraga
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/09/DI46Q4uXcAAZH69.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: ELN] 

###### [06 Sept 2017] 

De acuerdo con el profesor y analista Álvaro Villaraga, el cese bilateral pactado entre el Ejército de Liberación Nacional y el Gobierno, es histórico y permite evidenciar cambios en las posturas del equipo liderado por Juan Camilo Restrepo, **ya que nunca antes se había pactado un cese bilateral hasta que no se hubiesen terminado las conversaciones.**

“En lo relativo al tema militar del cese al fuego **no había existido un compromiso como el que se suscribió en Quito, ni uno entre el ELN y el gobierno**” afirmó Villaraga yseñaló que durante la presidencia de Betancur, fue uno de los pocos momentos en donde se intentó generar un cese bilateral con otras insurgencias como el M-19, el EPL y , para ese entonces, las FARC-EP, que fracasó debido a la falta de acogida por parte de la Fuerza pública y al asesinato de dirigentes políticos.

Otra de las características específicas de este cese bilateral, según Villarraga, es que demuestra una evolución en la postura del gobierno y hace cambiar su política nacional de paz, al firmarlo sin que se haya concluido el conjunto de la negociación. (Le puede interesar: ["Iglesia y ONU serán veedoras del cese bilateral entre el gobierno y el ELN"](https://archivo.contagioradio.com/la-iglesia-seria-veedora-del-cese-bilateral-entre-el-gobierno-y-el-eln/))

Referente a que este cese bilateral pueda ser prorrogado luego de estos primeros cuatro meses, Villarraga manifestó que existe la posibilidad de que una vez en este periodo se avance en el punto 1 de participación social y el 5.f de **acciones humanitarias para finalizar el conflicto, ambos equipos podrían contemplar un plazo más para el cese.**

De igual forma, manifestó que este proceso de paz tiene la particularidad de desde un principio permitir la participación de la sociedad de forma abierta, singularidad que para el analista no se ha dado en ningún otro proceso de paz en Colombia “**a diferencia de la mesa de la Habana esta es una mesa permanentemente abierta a la participación**”. (Le puede interesar: ["COALICO insta a ELN y gobierno a incluir protección a menores de edad en agenda de diálogo"](https://archivo.contagioradio.com/coalico-insta-a-eln-y-gobierno-a-incluir-proteccion-de-menores-de-edad-en-agenda-de-dialogo/))

<iframe id="audio_20746717" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_20746717_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
