Title: No voy a permitir que me silencien, es un tema de principios: Claudia Julieta Duque
Date: 2019-07-31 17:33
Author: CtgAdm
Category: DDHH, Nacional
Tags: Censura, Libertad de expresión, Libertad de Prensa
Slug: no-voy-a-permitir-que-me-silencien-es-un-tema-de-principios-claudia-julieta-duque
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/Claudia-Julieta-Duque.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: FLIP] 

[La periodista e investigadora **Claudia Julieta Duque** calificó como una revictimización y un ataque a la libertad de expresión por parte del]Juzgado Segundo Especializado de Bogotá, [la orden que recibió de]no opinar ni manifestarse sobre el proceso que se adelanta contra varios ex funcionarios del extinto **Departamento Administrativo de Seguridad (DAS)** del que fue víctima por tortura y persecución en 2001.

### Lo que no quieren que se conozca 

Según relata la periodista, la decisión tomada por el Juzgado Segundo parte de una serie de tuits y declaraciones que son tomados "fuera de contexto" por **la jueza Nidia Angélica Carrero,** argumentando que "vulneran los derechos de los acusados"  y "ponen en ridículo a la justicia". Para Duque, este fallo hace parte de la persecución de la que ha sido blanco con el fin de "acallar la voz de mis opiniones, mi pensamiento y mis investigaciones".

Pese aunque siente que es una forma de **"revivir los dolores, temores, ansiedades y angustias" que le han conllevado su trabajo, considera que lejos de silenciarla, le permite revitalizarse y "tener una mayor fortaleza para continuar luchando".**

> Por este tuit la juez Nidia Angélica Carrero Torres dijo que yo hacía quedar en ridículo a la administración de justicia. Me refería al hecho de ser la única sobreviviente de los crímenes de Estado cometidos entre 1997 - 2005. ME REAFIRMO: [pic.twitter.com/MDjCgFAyjw](https://t.co/MDjCgFAyjw)
>
> — ClaudiaJulietaDuque (@JulieDuque1) [July 30, 2019](https://twitter.com/JulieDuque1/status/1156191730007511041?ref_src=twsrc%5Etfw)

<p>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
</p>
> Por este tuit la juez me prohibió publicar fotografías y dizque vulnerar los derechos de los acusados. Se le olvida a la juez que los he señalado con nombre propio desde el día 0, que he investigado mi propio caso y si no fuera por ello JAMÁS habría habido avances. ME REAFIRMO: [pic.twitter.com/Oc7I8KSOZS](https://t.co/Oc7I8KSOZS)
>
> — ClaudiaJulietaDuque (@JulieDuque1) [July 30, 2019](https://twitter.com/JulieDuque1/status/1156191317778665474?ref_src=twsrc%5Etfw)

<p>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
</p>
> Estas fueron mis declaraciones del 31 de enero a [@elespectador](https://twitter.com/elespectador?ref_src=twsrc%5Etfw) que molestaron a Nidia Angélica Carrero Torres.  
> Mi delito: opinar. Se olvida la juez además de que la mía es una opinión calificada. Hace más de 20 años soy investigadora en temas de impunidad y ddhh. ME REAFIRMO. [pic.twitter.com/VjXTsXm2dj](https://t.co/VjXTsXm2dj)
>
> — ClaudiaJulietaDuque (@JulieDuque1) [July 30, 2019](https://twitter.com/JulieDuque1/status/1156190655921737730?ref_src=twsrc%5Etfw)

<p>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
</p>
### **Un retroceso enorme y preocupante**

[Sobre la forma en que se manejan los procesos judiciales y penales, la periodista expresa que esta no es la primera vez que impiden a los periodistas estar en las audiencias pese a que son públicas, pero advierte que sí es inédita la decisión de un juez de "censurar la opinión y pensamiento de una periodista que en esta ocasión además es la víctima de graves violaciones a los derechos humanos" a través de un fallo.  [(Le puede interesar: Se incrementan hostigamientos, seguimientos y tortura contra Claudia Julieta Duque y su familia)](https://archivo.contagioradio.com/se-incrementan-hostigamientos-seguimientos-y-tortura-contra-claudia-julieta-duque-y-su-familia/)]

[La investigadora agrega que al no existir precedentes al respecto y no existir ninguna jurisprudencia para justificar la censura, la juez "se basó en dos libros de ultraconservadores jurídicos para manipular y descontextualizar expresiones que yo había hecho de manera previa". ]

### Lejos de silenciarme lo que hacen es fortalecerme

[Ante estas medidas, Duque señala que está "decidida a llegar hasta donde sea necesario, si eso implica que la víctima sea detenida y condenada por denunciar la impunidad y la libertad por vencimiento de términos de los sindicados pues hasta allá vamos a llegar".]

Al respecto, ya solicitó[ extensión de medidas cautelares a la Comisión Interamericana de Derechos Humanas y aunque sigue a la espera de esta petición apunta que antes de acudir a instancias internacionales se puede apelar a la acción de tutela o presentar un recurso de reposición que está en suspenso pues también se usó un recurso de recusación contra la juez, que debería apartarse de ese caso y de otros tres que están en su despacho. ]

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe><iframe id="audio_39329282" frameborder="0" allowfullscreen scrolling="no" height="200" style="border:1px solid #EEE; box-sizing:border-box; width:100%;" src="https://co.ivoox.com/es/player_ej_39329282_4_1.html?c1=ff6600"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
