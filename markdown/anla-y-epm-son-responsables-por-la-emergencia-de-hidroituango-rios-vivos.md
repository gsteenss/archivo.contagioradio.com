Title: ANLA y EPM responsables por la emergencia de Hidroituango: Ríos Vivos
Date: 2018-05-15 12:31
Category: Ambiente, DDHH
Tags: bajo cauca antioqueño, crisis en Hidroituango, Hidroituango, Movimiento Ríos Vivos
Slug: anla-y-epm-son-responsables-por-la-emergencia-de-hidroituango-rios-vivos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/05/DdFZ9MIX4AYEomP-e1526402570611.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Movimiento Ríos Vivos] 

###### [15 May 2018] 

Tras la emergencia producida el fin de semana en el Bajo Cauca Antioqueño, producto del des taponamiento de los túneles de llenado de la hidroeléctrica Hidroituango, que hasta el momento deja 600 personas damnificadas, el Movimiento Ríos Vivos afirmó que **aún hay alerta por la creciente del río** que amenaza con nuevas inundaciones y culpó a EPM y la ANLA por lo sucedido.

De acuerdo con Isabel Cristina Zuleta, integrante del Movimiento Ríos Vivos,  la avalancha que tiene a varios municipios incomunicados, “la habíamos previsto y las comunidades están pidiendo que **sean reubicadas** y nadie habla de esto”. Afirmó que “sólo se ha dicho que hay dinero para reparar las viviendas, pero no todo lo que ha sucedido con el megaproyecto”.

Indicó que “aún no hay claridad de lo que pueda pasar con el puente Palestina que comunicaba a Briceño con Ituango”. Adicionalmente, el deslizamiento de tierra y los residuos vegetales han hecho que la **creciente continúe en aumento** generando la mortandad de los peces. Dijo que solamente en Sabanalarga se han registrado **300 personas afectadas que no han tenido la ayuda humanitaria correspondiente.**

**Responsabilidad es de EPM y la ANLA**

En reiteradas ocasiones, tanto Ríos Vivos como las comunidades, han denunciado que la crisis que se está viviendo **“es producto de la irresponsabilidad de EPM** y su falta de estudios rigurosos”. Zuleta reiteró que la empresa tuvo un desconocimiento “de los saberes ancestrales en el Cañón y ellos sabían de las fallas geológicas existentes”.

Ante esto, afirmó que “lo que ha venido ocurriendo es única y exclusivamente responsabilidad de la **Autoridad Nacional de Licencias Ambientales**, que debió revisar los estudios técnicos y las falencias que tenían”. Recordó que la zona donde se construyó el muro de contención de la hidroeléctrica “es un lugar de alta pendiente donde ha habido derrumbes y deslizamientos que habían sido advertidos por las comunidades”.

### **Sitios identificados con cuerpos de desaparecidos quedaron bajo el agua** 

A la crítica situación se suma el hecho de que, los lugares que las comunidades en conjunto con Ríos Vivos habían identificado como fosas comunes que albergan los cuerpos de cientos de desaparecidos **“quedaron bajo el agua”**. Esto ha representado “un gran dolor para las familias que tenían alguna esperanza con nuestra participación en la CIDH”. (Le puede interesar:["Hidroituango, una bomba de tiempo"](https://archivo.contagioradio.com/hidroituango-una-bomba-de-tiempo/))

Cabe resaltar que Ríos Vivos, desde que inició el proyecto en 2010, pidió garantías para desarrollar las labores de búsqueda de los desaparecidos en los **12 municipios antioqueños** en los que tiene influencia el proyecto y donde, en el marco de conflicto armado, se presentaron 62 masacres. Sin embargo, hasta la fecha y según la Fiscalía sólo ha sido posible encontrar 154 cuerpos de más de 600 desaparecidos que ha registrado el Centro Nacional de Memoria Histórica.

### **Albergues en Puerto Valdivia dispuestos para atender la emergencia no son suficientes** 

De igual forma, Zuleta denunció que ha habido **discriminación en la atención de las personas damnificadas** que hacen parte del Movimiento Ríos Vivos, en la medida en que “no se les ha entregado con la misma rapidez las ayudas que al resto de los habitantes”. Esta situación también se vive en los sitios de albergue del municipio de Puerto Valdivia que “no son suficientes”.

Según Ríos Vivos, uno de los tres albergues es de carácter privado y **“es propiedad del alcalde de Valdivia** y él quiso desalojar a la gente de ese sitio”. Afirmó que ese albergue es el único que tiene las condiciones de seguridad suficientes teniendo en cuenta que los demás sitios “están sobre el nivel del río y ante el riesgo de una nueva avalancha”. (Le puede interesar:["Es indignante posición de EPM ante tragedia de Hidroituango: Ríos Vivos"](https://archivo.contagioradio.com/es-indignante-posicion-de-epm-frente-a-tragedia-por-hidroituango-rios-vivos/))

### **Exigen que se retire licencia ambiental a Hidroituango** 

Debido a esta situación, el Movimiento Ríos Vivos ha exigido que se retire la licencia ambiental a Hidroituango teniendo en cuenta que “no tienen capacidad para manejar la situación y quedó demostrado que, cuando dio los partes de tranquilidad, no tiene el **conocimiento suficiente** sobre la gravedad de lo que está sucediendo”.

Para las comunidades “es humillante que digan que la **responsabilidad es de la naturaleza** cuando ha sido el hombre, que, con sus túneles, intervino una montaña que no debió haber tocado nunca y por eso se está desmoronando”. (Le puede interesar:["Búsqueda de desaparecidos en Hidroituango debe incluir a las víctimas"](https://archivo.contagioradio.com/busqueda-de-desaparecidos-en-hidroituango-debe-incluir-a-las-victimas-rios-vivos/))

Frente al número de personas afectadas hasta el momento, Zuleta manifestó que la Unidad para la Gestión del Riesgo, “estuvo en Puerto Valdivia y dio un reporte del censo que tienen”. Afirmó que se han presentado **108 familias damnificadas en Puerto Valdivia** pero aún no hay un reporte del número de personas afectadas en los municipios de Ituango, Toledo y San Andrés de Cuerquia.

Finalmente, Zuleta indicó que el agua sigue aumentando **en la parte alta del río** y esto ha causado inundaciones “en tierras que no son de EPM”. Desde ayer, los municipios de Buriticá y Peque quedaron incomunicados por la vía de Sabanalarga por lo que “más de mil personas se encuentran aisladas e incomunicadas”.

###### Reciba toda la información de Contagio Radio en [[su correo]
