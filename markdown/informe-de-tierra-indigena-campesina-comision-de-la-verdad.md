Title: Informe "Tierra y despojo en los Llanos" será entregado a la Comisión de la Verdad
Date: 2019-09-20 15:30
Author: CtgAdm
Category: Paz
Tags: campesinos, comision de la verdad colombia, indígenas, Meta, paz
Slug: informe-de-tierra-indigena-campesina-comision-de-la-verdad
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/vigilia-castillo-meta-14.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:ContagioRadio] 

 

Del 20 al 22 de septiembre,  se estará llevando a cabo en el **Asentamiento Ancestral  Indígena *Aseinpome*, en el municipio de Puerto Gaitán** en El Porvenir Meta, el s**egundo encuentro de pueblos indígenas y campesinos** de los departamentos de Meta, Casanare y Vichada, donde será entregado  el informe "Tierra y despojo en los llanos" a la Comisión de la Verdad, en el que se refleja el trabajo de las comunidades por la defensa del territorio y los intereses empresariales por el mismo. (Le puede interesar:[ESMAD agrede a campesinos que se oponen a multinacional petrolera en Caquetá](https://archivo.contagioradio.com/esmad-agrede-a-campesinos-que-se-oponen-a-multinacional-petrolera-en-caqueta/))

Juan David Espinal, Secretario Técnico de la **Corporación Claretiana Norman Pérez**, señaló que en el informe se reflejan los daños de  actores externos  que se han involucrado en la apropiación de esta tierra,  “**se puede notar el interés por parte de empresarios y políticos por apropiarse de estos territorios a toda costa**”. Igualmente, el documento señala los proceso de las comunidades por el respeto a la naturaleza, la importancia de defender el territorio y la esperanza de vivir dignamente y en paz.

### Este informe sobre la tierra cuenta con varios casos donde:

Pérez, afirma que desde **la Corporación han pensado en la posibilidad de presentar este informe ante otras organizaciones como la JEP**, “esto implicaría darle otro enfoque al escrito, pero también sería la forma de visibilizar el trabajo de los pueblos,  y poder mostrar esa responsabilidad de los terceros que se han beneficiado los despojos de la tierra”. (Le puede interesar:[Gobierno incumple a campesinos e indígenas del sur de Córdoba](https://archivo.contagioradio.com/gobierno-incumple-a-campesinos-e-indigenas-del-sur-de-cordoba/))

En el encuentro se contará con la participación del Consejo Regional del Cauca, la Comisión de la Verdad, entidades internacionales y alrededor de catorce movimientos indígenas y campesinos de tres departamentos, según Espinal, “hace 3 años logramos que las comunidades indígenas pudieran regresar a este territorio y conciliar con los campesinos, **ahora no solo buscamos una sana convivencia en los territorios, también encontrar a los responsables de los crímenes que afectaron a estas comunidades”**

**Este evento se realiza con la finalidad de seguir afianzando una ruta conjunta en defensa de la vida y el territorio**, reafirmando un trabajo que han venido desarrollando desde la Corporación Claretiana Norman Pérez Bello para concienciar a los grupos campesinos, sobre **la propiedad ancestral que une a los indígenas con estos territorios**, al igual que la labor de visibilizarían trabajos en  salud, educación, territorio, economía propia, solidaridad y reconocimiento de víctimas, temas  que serán señaladas en un informe que será entregado a la Comisión de la Verdad.

<iframe id="audio_41958915" frameborder="0" allowfullscreen scrolling="no" height="200" style="border:1px solid #EEE; box-sizing:border-box; width:100%;" src="https://co.ivoox.com/es/player_ej_41958915_4_1.html?c1=ff6600"></iframe>  
**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo] 
