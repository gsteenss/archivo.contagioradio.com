Title: Muere Arley Velasco, líder del Putumayo víctima de atentado el 5 de Diciembre
Date: 2017-12-26 14:39
Category: DDHH, Nacional
Tags: Arley Velasco, Putumayo
Slug: asesinado-arley-velasco-lider-putumayo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/12/arley-velasco-corredor-pto-vega-tty.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: recpsur] 

###### [26 Dic 2017]

Según la información compartida por las organizaciones del Putumayo, **este 26 de Diciembre hacia las 9:40 de la mañana, murió Arley Velasco, quién fue víctima de un atentado el pasado 5 de diciembre en la vereda “Carmelita”** del corredor Puerto Vega – Teteyé. En ese momento los sicarios le propinaron 9 disparos dejándolo gravemente herido.

El 5 de diciembre, luego del atentado Velasco fue trasladado al hospital de Puerto Asís en donde se recuperaba de las heridas propinadas por los desconocidos, **sin embargo, luego de 21 días de luchar por su vida y después de haber sido ingresado a la Unidad de Cuidados** Intensivos del hospital, murió en la mañana de este 26 de Diciembre. [Lea también: Atentan contra líder del Putumayo](https://archivo.contagioradio.com/atentan-contra-arley-velasco-lider-social-del-putumayo/)

El campesino era habitante de la vereda “Carmelita” lugar en que se encuentra ubicado el Espacio Territorial de Educación y Reincorporación de integrantes de las FARC. Velasco también era afiliado al Sindicato de Trabajadores Campesinos del cordón fronterizo del Putumayo, SINCAFROMAYO, de la mesa de Organizaciones Sociales del Putumayo que a su vez es fialial del Sindicato de Trabajadores Agrícolas, FENSUAGRO. (Lea también: [guerra contra los líderes de sustitución se está tomando al Putumayo](https://archivo.contagioradio.com/lideres_sociales_putumayo_puerto_asis_asesinatos/))

En su momento los líderes sociales de la región manifestaron que era urgente que se dieran las garantías de seguridad pues, tanto solo en los primero días del mes de Diciembre se había reportado el asesinato de 3 personas, entre las que se encontraba también el presidente de la Junta de Acción Comunal de la vereda la Brasilia, **Luis Alfonso Giraldo. **(Le puede interesar: ["Asesinan a Luis Alfonso Giraldo, líder social de la Carmelita en Putumayo"](https://archivo.contagioradio.com/asesinan_luis_alfonso_giraldo_camelita_putumayo/))

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
