Title: U de Pamplona otra víctima de la desfinanciación de la educación Pública
Date: 2017-03-02 16:28
Category: Educación, Nacional
Tags: Crisis de la Educación, Universidad de Pamplona
Slug: universidad-de-pamplona-en-asamblea-permanente
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/03/Pamplona.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Mapio.net] 

###### [2 Mar 2017] 

Estudiantes de la Universidad de Pamplona se declararon en asamblea permanente, debido a la falta de garantías para estudiar. La comunidad denuncia que **hay problemas con los horarios de clases, falta de planta docente, malas condiciones y falta de insumos en los laboratorios y un incremento en las matrículas de la universidad.**

Para Carlos Solarte, representante ante el Consejo Superior de la Universidad de Pamplona, **es “indignante” que ya haya trascurrido dos semanas de clase y aún existan materias en diferentes facultades que no tengan un docente**, retrasando el horario de clases. A su vez, expone que este es un fenómeno que se ha hecho reiterativo en la universidad y que evidencia una falta de control sobre los profesores.

Los estudiantes explican que la gran mayoría de personas que ingresan a esta universidad son de estratos bajos, razón por la cual trabajan o estudian y seleccionan de esa manera su horario. Sin embargo, denuncian que la disposición de las clases se está dando de forma desorganizada, **provocando que estudiantes que se han inscrito en solo jornadas diurnas vean clase en las noches, o que a veces tengan bloques de 8 horas continúas de clase.** Le puede interesar. ["Reforma estudiantil se pasaría vía Fast Track"](https://archivo.contagioradio.com/reforma-al-sistema-de-educacion-se-pasaria-via-fast-track/)

Otra de las problemáticas que llevo a los estudiantes a suspender la actividad de clases, es la falta de elementos e insumos en los laboratorios, que ha generado que muchos de ellos deban comprarlos para realizar sus prácticas de investigación en clase. Insumos que deben ser proporcionados por la universidad y que de acuerdo con Sergio Solarte **“en medio de esta realidad se hace muy complicado que el estudiante pueda además de la matrícula comprar los insumos”**

Referente a las matrículas y los descuentos, Solarte explica que desde hace algunos años han logrado generar un descuento para los estudiantes de **\$100.000 pesos**, que si bien es cierto no es mucho, ayuda a sopesar el costo semestral de la matrícula, **al mismo tiempo los estudiantes lograron que no se aumente el costo de la misma**; ambos acuerdos finalizan este año.

“Nuestra preocupación es que la tabla de matrículas es muy alta, así como hay carreras de **\$700.000 mil pesos hay otras que llegan a costar hasta tres millones**, por ende exigimos que se mantengan los costos, siendo universidad pública, para bajar los precios”. Le puede interesar: ["Ser pilo paga ha gastado más de 350 mil millones del presupuesto de universidades públicas"](https://archivo.contagioradio.com/ser-pilo-paga-se-llevo-373-290-470-719-del-presupuesto-de-educacion/)

Debido a estas situaciones, los estudiantes desde ayer se declararon en paro, e**l 2 y 3 de marzo tendrán mesas de trabajo que expongan los pliegos de exigencias** para cada uno de estos puntos y finalmente dialogar con las decanaturas y rectoría de la Universidad, posteriormente esperan que las directivas se comprometan a mejorar estas problemáticas y de esta manera retomar la normalidad académica.

###### Reciba toda la información de Contagio Radio en [[su correo]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio]{.s1}](http://bit.ly/1ICYhVU)[.]{.s1}
