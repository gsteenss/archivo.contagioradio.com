Title: "Sucesos como éste sólo se dan cada diez años en la historia": Roberto Carlos Vidal
Date: 2016-06-24 13:45
Category: Entrevistas, Nacional, Paz
Tags: acuerdo cese al fuego farc y gobierno, Cese bilateral al fuego, Mesa de conversaciones de paz de la habana, proceso de paz Colombia
Slug: sucesos-como-este-solo-se-dan-cada-diez-anos-en-la-historia-roberto-carlos-vidal
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/05/viene-la-paz-contagioradio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio ] 

###### [24 Junio 2016 ] 

El investigador Roberto Carlos Vidal, integrante del Instituto Pensar de la Universidad Javeriana, asegura que el acuerdo publicado en La Habana "tiene una relevancia radical para el país, porque es el corazón de toda la negociación y del esfuerzo del acercamiento entre el Gobierno y las FARC", teniendo en cuenta que el cese bilateral marca el fin de la guerra. Expertos internacionales en paz aseguran que **sucesos como éste sólo se dan cada diez años en la historia** y son comparables con el fin del conflicto armado en Irlanda, con el apartheid en Sudáfrica o con la caída del Muro de Berlín.

Los **mecanismos que se han acordado en este proceso de paz provienen de las experiencias internacionales en las negociaciones más exitosas de los últimos 30 años** y reflejan lo que la humanidad sabe que es lo mejor para lograr el cumplimiento de lo pactado, asegura el investigador, e insiste en que elementos positivos como el que las armas sean entregadas en 180 días, llenan de optimismo a la sociedad colombiana, pues en procesos como el de África o Irlanda del Norte la entrega de armas tomó 20 años.

"Sin duda el éxito de los mecanismos va a depender de lo que en Colombia podamos hacer (...) los acuerdos van a tener que estar rodeados de un consenso muy grande de parte de la sociedad civil", que ya no es tan indiferente ni está en contra del proceso como se dice en muchos medios, pues **tanto la información independiente como la del Estado evidencia el apoyo de la población**, particularmente de quienes han sido afectados directamente por el conflicto, entre ellos víctimas, militares y excombatientes, quienes han vivido la guerra en sus cuerpos y territorios, afirma Vidal.

"Todos sabemos que los únicos bandos enfrentados en Colombia no son las FARC y el Gobierno, y que de hecho tenemos amenazas muy fuertes de los viejos y nuevos grupos paramilitares, de los narcotraficantes y de varias fuerzas que están en los territorios. Este es un proceso de paz que se va a implementar dentro de un contexto de guerra que persiste, de disturbios internos entre el Estado y otras fuerzas" agrega el investigador. Una situación que preocupa a las **comunidades y organizaciones sociales que reiteran el llamado al Gobierno y al ELN a que inicien conversaciones**, teniendo en cuenta que el 80% del proceso con FARC puede ser aplicable a la negociación con esta guerrilla.

Vea también: [[Éste es el texto de acuerdo sobre el cese bilateral firmado entre Gobierno y FARC](https://archivo.contagioradio.com/este-es-el-texto-del-acuerdo-sobre-cese-bilateral-firmado-entre-gobierno-y-farc/)]

<iframe id="audio_12018911" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_12018911_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)]]
