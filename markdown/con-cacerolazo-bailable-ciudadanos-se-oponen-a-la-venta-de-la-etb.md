Title: Con 'cacerolazo bailable' ciudadanos se oponen a la venta de la ETB
Date: 2016-05-06 11:06
Category: Economía, Movilización
Tags: ETB, Sintrateléfonos, Universidad Distrital, Venta ETB
Slug: con-cacerolazo-bailable-ciudadanos-se-oponen-a-la-venta-de-la-etb
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/05/cacerolazo.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Gabriel Galindo ] 

###### [6 Mayo 2016 ]

Estudiantes de la Universidad Distrital al percatarse de que la privatización de la Empresa de Teléfonos de Bogotá ETB implica que la institución **no reciba los \$7 mil millones con los que el Distrito la financia**, lideran iniciativas de movilización para que la ciudadanía se oponga a la venta de esta compañía de telecomunicaciones.

De acuerdo con William Sierra, presidente de Sintratelefonos, este viernes desde las 5 de la tarde, **sindicatos distritales, estudiantes y ciudadanos marcharán** desde la Plazoleta Eduardo Umaña Mendoza hasta la Plaza de Bolívar en la que a ritmo de música, ollas y cacerolas, manifestaran su negativa a la [[pretensión del actual alcalde de la capital](https://archivo.contagioradio.com/enrique-penaloza-anunciala-venta-de-la-empresa-publica-etb/)].

La venta de la ETB implicaría que en 2019 el Distrito **dejara de recibir por lo menos \$2 billones para inversión social**, por lo que varios sindicatos de la ciudad entre ellos la ADE y Sintratelefonos, sostienen reuniones en las que evalúan la posibilidad de promover un paro distrital.

<iframe src="http://co.ivoox.com/es/player_ej_11434969_2_1.html?data=kpahlZmdepqhhpywj5WYaZS1lJeah5yncZOhhpywj5WRaZi3jpWah5yncbjdzdHWw9KPl8rZ09fOjZKPl6rCtbeutqqwiafDr7TAj4qbh4630NPhw8zNs4zGwsnW0ZCRaZi3jpk%3D&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>  
[![ETB Cacerolazo](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/05/ETB-Cacerolazo.jpg){.aligncenter .size-full .wp-image-23692 width="630" height="470"}](https://archivo.contagioradio.com/con-cacerolazo-bailable-ciudadanos-se-oponen-a-la-venta-de-la-etb/etb-cacerolazo/)  
 
