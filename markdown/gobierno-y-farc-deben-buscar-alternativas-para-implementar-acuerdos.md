Title: Gobierno y Farc deben buscar alternativas para implementar acuerdos
Date: 2016-10-18 16:45
Category: Nacional, Paz
Tags: Carlos Lozano, FARC, Gobierno, Juan Manuel Santos, paz
Slug: gobierno-y-farc-deben-buscar-alternativas-para-implementar-acuerdos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/06/ff3c51e6bbe6718a88c978d30111002f.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: AFP] 

###### [18 de Oct 2016] 

Luego de ser conocidos los resultados del plebiscito, la delegación de Paz del gobierno de Juan Manuel Santos viajará a La Habana para reunirse con las Farc y conversar las propuestas que han sido entregadas por diversos sectores que promueven el NO.

A propósito del tema, Carlos Lozano director del Semanario Voz, aseguró que no se puede olvidar que el acuerdo aún existe y que **lo que se perdió fue la posibilidad de implementarlo inmediatamente** después del plebiscito pero “el acuerdo existe y se ha trabajado durante 4 años” aseguró.

Por su parte, indicó que debido al resultado del No las Farc y el Gobierno manifestaron la posibilidad de dialogar con todas las partes y los sectores sociales y políticos con el fin de encontrar las precisiones que son necesarias hacer en el acuerdo “lo que van a ver es qué se puede hacer a manera de anotaciones, que sirvan para que quede más claro todo lo que se hizo en La Habana” y agregó **“hay que dejar claro que no se ha hablado de abrir el debate sobre el conjunto del acuerdo”.**

Dentro de las propuestas entregadas por el No a la delegación de Paz del Gobierno, y que serán llevadas a la Habana, se encuentran entre otros los puntos de justicia y el tema agrario. Frente a este Carlos Lozano manifestó “hay que recordar que hasta el Presidente (Juan Manuel Santos) ha manifestado que no se hagan propuestas que no sean realistas y propuestas imposibles, **pretender cambiar la justicia transicional por otra como la que plantean los del No, es un imposible”.**

En materia de implementación de los acuerdos, Lozano aseguró que pueden buscarse otras variantes legales para hacerlo **“Ya le corresponderá al gobierno buscar la alternativa constitucional de acuerdo al fallo de la Corte para poder sacar adelante el acuerdo**… se pueden buscar otras variantes, legales, constitucionales y es lo que tienen que hacer en La Habana asesorados por los juristas”.

Aseguró que la campaña del No se está aprovechando de temas como la reforma tributaria y agregó **“al señor Uribe le importa cinco el tema de la reforma tributaria, es decir si ésta reforma no toca los bolsillos de los empresarios de los monopolio**, de las transacionales, de los poderosos que detentan el poder, el señor Uribe no tiene problema, sin embargo el No ha aprovechado esos temas”. Le puede interesar: [“Propuestas de Uribe apuntan a la impunidad” Carlos Lozano](https://archivo.contagioradio.com/propuestas-de-uribe-apuntan-a-la-impunidad-carlos-lozano/)

Por último hizo un llamado a los sectores sociales y manifestó **“por ahora debemos concentrarnos en defender la paz, a exigir paz ya, de una forma inmediata**, que es el clamor de la sociedad civil. No será la movilización de Uribe, pero si las del sector popular” concluyó.

<iframe id="audio_13383979" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_13383979_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en su correo [[bit.ly/1nvAO4u]
