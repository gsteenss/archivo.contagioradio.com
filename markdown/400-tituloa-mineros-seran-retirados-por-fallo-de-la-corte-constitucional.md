Title: Cerca de 400 títulos mineros en zonas de páramos deberán ser retirados
Date: 2016-02-23 19:29
Category: Ambiente, Otra Mirada
Tags: Corte Constitucional, delimitación de páramos, Plan Nacional de Desarrollo
Slug: 400-tituloa-mineros-seran-retirados-por-fallo-de-la-corte-constitucional
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/02/Páramo1-e1456273290780.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Agencia Noticias Unal 

<iframe src="http://co.ivoox.com/es/player_ek_10556395_2_1.html?data=kpWil5uXfZahhpywj5WdaZS1kZeah5yncZOhhpywj5WRaZi3jpWah5yncaTZ08jOjcnJb5WkkZDhh6iXaaK41drZ0diPscrixtfc1ZDJsozu0NPO1ZDIqYzkhqigh6aVtsLh0NiYxsrGqY6ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Natalia Gómez, Ambiente y Sociedad] 

###### [23 Feb 2016] 

Tras el fallo de la Corte Constitucional, a favor de la demanda que interpusieron varios congresistas del Polo Democrático Alternativo, en contra de los artículos del Plan Nacional de Desarrollo que permitían la permanencia de títulos mineros en zonas de páramo. **El reto ahora, para el Estado y específicamente para el Ministerio de Ambiente será implementar la decisión pues se tendrá que retirar cerca de 400 títulos mineros.**

Este año el Ministerio de Ambiente anunció que se iniciará **el proceso de delimitación de 25 páramos**, para lo que el ministerio deberá tener en cuenta  los conflictos socio-ambientales y por lo tanto la opinión y relación de las comunidades con el ecosistema, de manera que existan garantías reales para la protección de los páramos, como lo explica la abogada Natalia Gómez, de la Asociación Ambiente y Sociedad.

“Hubo un parágrafo que no se demandó, donde se decía que el Ministerio de Ambiente tiene la obligación de hacer el proceso de limitación de los páramos”, dice la abogada quien pone en cuestión que MinAmbiente acepte los requisitos del Instituto Von Humbolt.

Aunque Colombia cuenta con el mayor porcentaje de páramos de América, **el único páramo que se encuentra delimitado es el de Santurbán,** en Santander, cuya delimitación fue bastante polémica pues que no estuvo acorde a las necesidades de la comunidad ni de la propia protección de ese ecosistema.

Entre los años 2002 y 2009, el gobierno entregó títulos mineros sin obligar a las empresas nacionales y multinacionales a cumplir mayores requisitos. Durante esos años se pasó de 1,1 a 8,4 millones de hectáreas tituladas, entregándose **391 títulos mineros sobre páramos en cerca de 108.000 hectáreas.** Pero sacarlos no será tan fácil como fueron entregados.

De acuerdo con la abogada Gómez, el gobierno actual habría perdido con la decisión de la Corte, pues las compañías invierten millones de dólares en estos proyectos mineros. Pero ahora, el Estado deberá cumplir la sentencia de  la Corte Constitucional, además deberá tener en cuenta que la delimitación no solo se trata del páramo, sino de la interconectividad ecológica, la cartografía y los criterios de consultas sociales.
