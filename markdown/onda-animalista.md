Title: Voces de la Tierra
Date: 2015-01-16 16:52
Author: AdminContagio
Slug: onda-animalista
Status: published

### VOCES DE LA TIERRA

[![Arborización y contaminación del aire en Bogotá](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/03/fuego-detras-paloquemao1-e1551983121421-1000x550.jpg "Arborización y contaminación del aire en Bogotá"){width="1000" height="550"}](https://archivo.contagioradio.com/arborizacion-aire-bogota/)  

###### [Arborización y contaminación del aire en Bogotá](https://archivo.contagioradio.com/arborizacion-aire-bogota/)

[<time datetime="2019-03-07T13:30:03+00:00" title="2019-03-07T13:30:03+00:00">marzo 7, 2019</time>](https://archivo.contagioradio.com/2019/03/07/)En este programa se aborda la realidad ambiental sobre la calidad del aire en Bogotá, y la relación con el proceso de arborización en la ciudad[LEER MÁS](https://archivo.contagioradio.com/arborizacion-aire-bogota/)  
[![Desastre en Hidroituango: ¿Y ahora qué?](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/02/Captura-de-pantalla-2018-09-27-a-las-12.23.04-p.m..png "Desastre en Hidroituango: ¿Y ahora qué?"){width="718" height="549" sizes="(max-width: 718px) 100vw, 718px" srcset="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/02/Captura-de-pantalla-2018-09-27-a-las-12.23.04-p.m..png 718w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/02/Captura-de-pantalla-2018-09-27-a-las-12.23.04-p.m.-300x229.png 300w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/02/Captura-de-pantalla-2018-09-27-a-las-12.23.04-p.m.-370x283.png 370w"}](https://archivo.contagioradio.com/desastre-hidroituango/)  

###### [Desastre en Hidroituango: ¿Y ahora qué?](https://archivo.contagioradio.com/desastre-hidroituango/)

[<time datetime="2019-02-20T14:46:26+00:00" title="2019-02-20T14:46:26+00:00">febrero 20, 2019</time>](https://archivo.contagioradio.com/2019/02/20/)Expertos y representantes de la comunidad analizan los posibles escenarios que surgen tras el más reciente desastre provocado por Hidroituango[LEER MÁS](https://archivo.contagioradio.com/desastre-hidroituango/)  
[![¿Qué significa la concesión por 20 años más a la carbonera Drummond?](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/02/Mineria-770x400.jpg "¿Qué significa la concesión por 20 años más a la carbonera Drummond?"){width="770" height="400" sizes="(max-width: 770px) 100vw, 770px" srcset="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/02/Mineria-770x400.jpg 770w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/02/Mineria-770x400-300x156.jpg 300w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/02/Mineria-770x400-768x399.jpg 768w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/02/Mineria-770x400-370x192.jpg 370w"}](https://archivo.contagioradio.com/20-anos-concesion-drummond-carbon-cesar/)  

###### [¿Qué significa la concesión por 20 años más a la carbonera Drummond?](https://archivo.contagioradio.com/20-anos-concesion-drummond-carbon-cesar/)

[<time datetime="2019-02-13T12:00:15+00:00" title="2019-02-13T12:00:15+00:00">febrero 13, 2019</time>](https://archivo.contagioradio.com/2019/02/13/)¿Son suficientes las compensaciones económicas para reparar los daños ambientales que ocasionaran dos décadas más de extraer carbón por parte de Drummond?[LEER MÁS](https://archivo.contagioradio.com/20-anos-concesion-drummond-carbon-cesar/)  
[![Aguacate hass ¿una amenaza para el paisaje cafetero?](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/02/avocado-hass-770x400.jpg "Aguacate hass ¿una amenaza para el paisaje cafetero?"){width="770" height="400" sizes="(max-width: 770px) 100vw, 770px" srcset="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/02/avocado-hass-770x400.jpg 770w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/02/avocado-hass-770x400-300x156.jpg 300w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/02/avocado-hass-770x400-768x399.jpg 768w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/02/avocado-hass-770x400-370x192.jpg 370w"}](https://archivo.contagioradio.com/aguacate-hass-amenaza-paisaje-cafetero/)  

###### [Aguacate hass ¿una amenaza para el paisaje cafetero?](https://archivo.contagioradio.com/aguacate-hass-amenaza-paisaje-cafetero/)

[<time datetime="2019-02-05T17:29:50+00:00" title="2019-02-05T17:29:50+00:00">febrero 5, 2019</time>](https://archivo.contagioradio.com/2019/02/05/)Tras la popularidad del aguacate hass pocos conocen el fuerte impacto ambiental que puede ocasionar, algunas consecuencias ya se ven en el Quindio[LEER MÁS](https://archivo.contagioradio.com/aguacate-hass-amenaza-paisaje-cafetero/)  
[![La fijación de Peñalosa con los árboles de Bogotá](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/01/DxtOQ2XWsAE12pe-770x400.jpg "La fijación de Peñalosa con los árboles de Bogotá"){width="770" height="400" sizes="(max-width: 770px) 100vw, 770px" srcset="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/01/DxtOQ2XWsAE12pe-770x400.jpg 770w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/01/DxtOQ2XWsAE12pe-770x400-300x156.jpg 300w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/01/DxtOQ2XWsAE12pe-770x400-768x399.jpg 768w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/01/DxtOQ2XWsAE12pe-770x400-370x192.jpg 370w"}](https://archivo.contagioradio.com/penalosa-arboles-de-bogota/)  

###### [La fijación de Peñalosa con los árboles de Bogotá](https://archivo.contagioradio.com/penalosa-arboles-de-bogota/)

[<time datetime="2019-01-29T15:36:26+00:00" title="2019-01-29T15:36:26+00:00">enero 29, 2019</time>](https://archivo.contagioradio.com/2019/01/29/)Mientras el Distrito asegura que ha sembrado 200 mil árboles, los grupos ambientalistas aseguran que la administración Peñalosa planea talar 34 mil árboles[LEER MÁS](https://archivo.contagioradio.com/penalosa-arboles-de-bogota/)  
[](https://archivo.contagioradio.com/consumo-responsable-navidad/)  

###### [Consumo responsable en Navidad](https://archivo.contagioradio.com/consumo-responsable-navidad/)

[<time datetime="2018-12-23T11:00:26+00:00" title="2018-12-23T11:00:26+00:00">diciembre 23, 2018</time>](https://archivo.contagioradio.com/2018/12/23/)Una influencer en estilo de vida y un chef especializado en comida integral aportan recomendaciones para consumir responsablemente en navidad[LEER MÁS](https://archivo.contagioradio.com/consumo-responsable-navidad/)
