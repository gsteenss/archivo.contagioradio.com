Title: Así se movilizará Colombia este 21S
Date: 2020-09-21 09:07
Author: CtgAdm
Category: Actualidad, Movilización
Tags: 21s, Barranquilla, Bogotá, Cali, Cúcuta, Ibagué, Movilización social
Slug: asi-se-movilizara-colombia-este-21s
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/09/EibxHiMXYAEnyUp.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: Twitter

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Por el derecho a una vida digna, para manifestarse en contra de la brutalidad policial, la agudización de la violencia que ha derivado **en 60 masacres y 213 líderes sociales asesinados registrados a lo largo del año** y exigiendo respuestas al Gobierno de Iván Duque a la crisis que vive el país, centrales obreras, maestros, profesionales de la salud, liderazgos sociales, estudiantes y la ciudadanía se movilizarán pacíficamente en Colombia este 21S.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

En Bogotá se presentarán diversas movilizaciones en varios puntos de la ciudad comenzando con la **'Jornada Nacional por la Vida’** un recorrido hacia la sede del Ministerio de Trabajo en la Calle 100 al norte de la ciudad desde los Portales de la Calle 80 y el Portal del Norte. De igual forma desde el Sena de la avenida 1.º de mayo a las 2.00 p. m. y posteriormente una concentración e el Centro Comercial Mercurio a las 4:00 pm.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

De igual forma la jornada comenzará en la sede Porvenir de la Universidad Distrital cerca del Portal de las Américas por la Calle 42 Sur hasta la Avenida de las Américas, para llegar a la calles sexta hasta el centro de la ciudad

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Otras concentraciones y expresiones de movilización comenzarán desde la 1:00 pm en **el Parque de los hippies y en la carrera 13 con calle 36**; en **el parque San Luis y** a las 2:00 p.m. en **el Centro de Memoria Histórica **en la calle 26; otros puntos de concentración serán la Avenida Villavicencio con avenida 1.º de Mayo, a las 10:00 a. m, la Autopista Sur desde la entrada a Bogotá en La Sevillana a partir de las 9:00 a. m.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/cutcolombia/status/1307717040233426945","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/cutcolombia/status/1307717040233426945

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:heading {"level":3} -->

### 21S en otras ciudades del país

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

En la ciudad de Cali habrá dos marchas, una en la mañana y otra en la tarde. la primera comenzará a las 9:00 am cuando las personas se concentren en el Puente del Comercio donde comenzarán el recorrido a través de la ciudad hasta llegar hasta el Ministerio del Trabajo, explicaron desde la [Central Unitaria de Trabajadore](https://twitter.com/cutcolombia)s. Mientras, en la tarde, la movilización está prevista para las 5:00 p.m. en C**omfandi El Prado y se dirigirá hasta el Parque de las Banderas.**

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Mientras, en Antioquia, las protestas comenzarán desde las 9:00 a.m. en **el parque Débora Arango en Envigado**, con un recorrido que llegará hasta **el parque El Artista en Itagüí** y luego hacia el parque Obrero. Las marchas seguirán por la avenida Guayabal hacia la sede de la empresa Noel, la planta de Haceb y **la avenida Guayabal hacia el Centro Empresarial Olaya Herrera**. El punto de concentración será el Parque de las Luces hacia las 4:00 pm, donde también llegará **la marcha estudiantil que partirá desde la Comuna 7 **al noroccidente de Medellín. Finalmente, el punto final será la avenida Oriental, hacia el Parque de los Deseos sobre las 5:00 p.m.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

A las 9:30 AM en la torre del cable en Manizales las personas se sumarán a la movilización en contra de la brutalidad policial mientras en Pereira será desde las 2:00 pm en la Iglesia El Misericordioso, otras ciudades como Sogamoso el encuentro será en la Plaza de la Villa a la misma hora donde habrá una marcha de antorchas, biblioteca popular y diferentes expresiones artísticas. [(Lea también: "Si no salimos nos van a masacrar a todos... volveremos a las calles")](https://archivo.contagioradio.com/si-no-salimos-nos-van-a-masacrar-a-todos-volveremos-a-las-calles/)

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/AquinoTicias1/status/1307878032632356865","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/AquinoTicias1/status/1307878032632356865

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph {"align":"justify"} -->

Este 21S, la defensa por la vida en Popayán, tendrá su punto de concentración en la Glorieta Chirimía a las 9:00 am y la sede del SENA Norte a partir desde las 8:00 am mientras en el departamento de Tolima, en **Ibagué el punto de concentración será el Parque de los Venados desde las 9:00 am** y en Neiva a partir de las 8:00 a.m. comenzarán las movilizaciones en la Universidad Surcolombiana y el parque Leesburg hasta llear a la Gobernación del Huila.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

En Barranquilla, donde se espera que a partir de las 2:00 pm se concentren más de 70 organizaciones sindicales, organizaciones sociales, estudiantiles y ciudadanía en general, el punto de encuentro será la **Universidad Autónoma del Caribe,** la movilización irá hasta el hospital Metropolitano en la 76, para unirse con los trabajadores de esa institución y de SintraCarbón.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Mientras la segunda marcha se moverá a través de la Circunvalar y subirá hasta la carrera 38 para encontrarse en la 76 con el primer grupo para bajar por la carrera 53 y finalmente llegar hasta la Plaza de la Paz. [(Le puede interesar: Organizaciones sociales convocaron a un Pacto por la Vida y la Paz)](https://archivo.contagioradio.com/organizaciones-sociales-convocaron-a-un-pacto-por-la-vida-y-la-paz/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

En Bucaramanga la movilización comenzará a partir de las 9:00 am, los marchantes podrán encontrarse en el Caballo de Bolívar de la Universidad Industrial de Santander, de igual forma en Saravena, Arauca la movilización comenzará a las 9:00 am y en Yopal, Casanare posteriormente también se realizará un Plantón Cultural por la vida en el Km1 vía Aguazul junto al Vivero desde las 4:30 pm.

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
