Title: Empresa inglesa estaría detrás de la prohibición del ingreso de palestinos a mezquita en Jerusalén
Date: 2017-08-02 15:30
Category: Onda Palestina
Slug: empresa-inglesa-estaria-detras-la-prohibicion-del-ingreso-palestinos-mezquita-jerusalen
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/08/g4s.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: [[bbci.co.uk]](https://ichef.bbci.co.uk/news/624/cpsprodpb/C900/production/_97065415_ac7fda0a-3eb2-4af2-bba1-07593380a839.jpg) 

###### 08 Ago 2017

La empresa de seguridad **G4S suministró los sistemas de seguridad utilizados por el gobierno israelí para restringir el libre acceso a la mezquita de al-Aqsa**. FST Biometrics, la compañía que espera suministrar las cámaras de vigilancia inteligentes para ser instaladas en las puertas de al-Aqsa (ya instalada en la Puerta de Mughrabi), es socio de AMAG Technology, una compañía de propiedad de G4S.

Esto lo ha informado el Comité Nacional Palestino (CNP) que dirige la campaña de Boicot Desinversiones y Sanciones, quien ha hecho **un llamado a no olvidar la campaña de boicot a G4S.** Este boicot ha generado tal presión internacional que ha llevado a esta empresa a anunciar su retiro de de Palestina. Sin embargo, esta nueva información demuestra que **aún permanece proveyendo servicios a Israel**.

El CNP informó también que **Policity**, otra compañía perteneciente y operada por G4S, es **responsable de por lo menos el 40% de la instrucción de entrenamiento de la policía israelí**, que asciende a unas 40.000 horas en el contrato actual. La formación impartida por la Academia Nacional de Policía incluye **control de multitudes, allanamientos, técnicas de interrogatorio, tiro al blanco y operaciones encubiertas**, tácticas de represión **dirigidas exclusivamente a los palestinos**.

###### Escuche esta y más información sobre la resistencia del pueblo palestino en [Onda Palestina](https://archivo.contagioradio.com/programas/onda-palestina/), todos los Martes de 7 a 8 de la noche en Contagio Radio. 
