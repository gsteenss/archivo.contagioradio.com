Title: Uruguay: breve estado de situación y perspectivas
Date: 2015-03-03 12:32
Author: CtgAdm
Category: Enrique, Opinion
Tags: Ciberseguridad, Ley de Software Libre, software libre, Uruguay
Slug: uruguay-breve-estado-de-situacion-y-perspectivas
Status: published

#### Por [Enrique Amestoy](https://archivo.contagioradio.com/enrique-amestoy/) {#por-enrique-amestoy align="JUSTIFY"}

*Sin intentar hacer un raconto 100% acabado de los avances hacia la defensa de la Soberanía Tecnológica en el Uruguay, presentaré algunos de los avances, desafíos, fortalezas y debilidades que encuentra el Uruguay en este momento. Justo a 10 años del triunfo electoral, por primera vez en la historia, del Frente Amplio[i](#sdendnote1sym){#sdendnote1anc}[ii](#sdendnote2sym){#sdendnote2anc}, fuerza que agrupa a la mayoría de los sectores de centro-izquierda e izquierda, con 44 años de existencia. En el día de hoy comienza el tercer período de gobierno del Frente Amplio y parece importante pasar balance del legado de los últimos 10 años en general y de los últimos cinco, de José “Pepe” Mujica en particular y la proyección en los próximos 5 años de gobierno de Tabaré Vazquez.*

El 23 de diciembre de 2013 el Parlamento uruguayo votó, con la voluntad política de los legisladores del partido de gobierno, la **Ley de Software Libre y Formatos Abiertos en el Estado** N°19.179 [iii](#sdendnote3sym){#sdendnote3anc}

El texto de la Ley 19.179 consta de cinco artículos:

**ARTÍCULO 1º**. Los Poderes Ejecutivo, Legislativo y Judicial, los entes autónomos, los organismos descentralizados, las empresas donde el Estado posea mayoría accionaria, los Gobiernos Departamentales, las Juntas Departamentales, el Tribunal de lo Contencioso Administrativo, la Corte Electoral y los organismos de contralor del Estado, deberán distribuir toda información en al menos un formato abierto, estándar y libre. Todo pedido de información deberá ser aceptado en al menos un formato abierto y estándar.

**ARTÍCULO 2º**. En las instituciones y dependencias del Estado mencionadas en el artículo 1º, cuando se contraten licencias de software se dará preferencia a licenciamientos de software libre. En caso que se opte por software privativo se deberá fundamentar la razón.

En caso de que el Estado contrate o desarrolle software, el mismo al ser distribuido, se licenciará como software libre. El intercambio de información realizado con el Estado, a través de Internet, deberá ser posible en, al menos, un programa licenciado como software libre.

**ARTÍCULO 3º**. Se considera de interés general que el sistema educativo proceda a promover el uso de software libre.

**ARTÍCULO 4º**. El Poder Ejecutivo reglamentará en un plazo de 180 (ciento ochenta) días las condiciones, tiempos y formas en que se efectuará la transición de la situación actual a una que satisfaga las condiciones de la presente ley y orientará, en tal sentido, las licitaciones y contrataciones futuras de programas de computación (software) realizadas a cualquier título.

**ARTÍCULO 5º**. Definiciones a los efectos de la presente ley :

A\) El software libre es el que está licenciado de forma que cumpla simultáneamente las siguientes condiciones

-   Pueda ser usado para cualquier propósito.
-   Tenga acceso a su código fuente de forma que pueda ser estudiado y cambiado para adaptarlo a las necesidades.
-   Pueda ser copiado y distribuido.
-   Sea posible la mejora del programa y la liberación de dichas mejoras a la ciudadanía.

B\) El software privativo es todo software que prive de alguna de las cuatro condiciones o libertades inherentes al software libre.

C\) Los formatos abiertos son formas de manejo y almacenamiento de los datos en los que se conoce su estructura y se permite su modificación y acceso no imponiéndose ninguna restricción para su uso. Los datos almacenados en formatos abiertos no requieren de software privativo para ser utilizados.

D\) Formatos estándar son los que han sido aprobados por una entidad internacional de certificación de estándares.

Si bien la ley no obliga a la migración al Software Libre, marca precedente de que de aquí en mas, cada vez que se desarrollen o compren soluciones de software se deberá dar preferencia a las basadas en Software Libre. Herramienta indispensable, aunque no excluyente, para caminar hacia la Soberanía Tecnológica y la independencia de trasnacionales del software y la tecnología.

El 7 de abril de 2014 el Poder Ejecutivo y su Consejo de Ministros dieron otro importantísimo paso: firmaron el Decreto de Ciberseguridad [iv](#sdendnote4sym){#sdendnote4anc} que, entre otras cosas, obliga a todos los ministerios a alojar sus servidores de correo electrónico en territorio nacional, con control soberano de los mismos. Por otra parte obliga al Estado a la utilización exclusiva de dominios .gub.uy o .mil.uy en la misma dirección de dar garantía de soberanía y aplicación de la legislación local.

El 29 de abril el Poder Ejecutivo y el Consejo de Ministros firman el decreto de Política de Defensa Nacional[v](#sdendnote5sym){#sdendnote5anc} , asesorado por el CODENA (Consejo de Defensa Nacional, integrado por varios ministerios). Es un proyecto de largo aliento, sobre el que deberá legislarse, que analiza el escenario estratégico, escenario futuro, obstáculos que se podrían enfrentar y define lineamientos estratégicos. Dejo [en este link](http://www.mateamargo.org.uy/index.php?pagina=notas&seccion=el_fogon&edicion=21) un análisis que hiciera en el momento sobre el citado decreto.

En el discurso de toma de mando del día de ayer de Tabaré Vázquez[vi](#sdendnote6sym){#sdendnote6anc} menciona que se trabajará en este nuevo período en “políticas y estrategias relativas a la misma \[competitividad\] y a la transformación productiva, incluyendo lo que refiere al desarrollo económico, productivo, a la ciencia, a la tecnología,” Mas adelante define que “la inversión en ciencia y tecnologías de la información que alcanzará el 1 % del producto bruto interno (PIB) “ y que se destinarán e incrementarán los “fondos para becas de formación y capacitación de los investigadores e incorporaremos la investigación científica y tecnológica en las empresas públicas.“

De estas primeras declaraciones no se desprende que todo lo anterior vaya por el camino que se comenzara en el período de Pepe Mujica aunque, en tanto el Frente Amplio gana nuevamente las elecciones con un programa común y de continuidad y profundización, es esperable que la gestión Vazquez no se desvíe de lo avanzado hasta el momento y que, por el contrario, profundice y avance hacia modelos mas justos de distribución, mas fuertes en materia de soberanía e integración.

Un breve estado de situación y perspectiva, asociado a los avances en materia tecnologica en lo que a la informática y la seguridad refiere, que debe complementarse, indudablemente, con los avances en materia de generación de energía soberana de la mano de parques eólicos, biomasa, etc. Desde 2008 y al momento se estima que el 25% de la electricidad consumida en Uruguay es de origen eólico[vii](#sdendnote7sym){#sdendnote7anc}. Una de las últimas actividades de Pepe Mujica como Presidente de la República fue la inauguración el 28 de febrero, último día de mandato, junto con la presidenta del Brasil Dilma Rouseff, de un parque eólico que aportará 65Mw a la red actual[viii](#sdendnote8sym){#sdendnote8anc}.

Un partido en el gobierno desde hace 10 años, que se ha tomado en serio la importancia estatégica de la Soberanía Tecnológica y Energética así como la integración regional como pilar fundamental de cooperación y complementación en matería política, económica, educativa y tecnológica.

<div id="sdendnote1">

###### [[i](#sdendnote1anc) 

</div>

###### [[ii](#sdendnote2anc) 

###### [[iii](#sdendnote3anc) 

###### [[iv](#sdendnote4anc) 

###### [[v](#sdendnote5anc) 

###### [[vi](#sdendnote6anc) 

###### [[vii](#sdendnote7anc) 

<div id="sdendnote8">

###### [[viii](#sdendnote8anc) 

</div>
