Title: Campesinos de Sibaté se oponen a la destrucción del Río Muña por parte de Comin S.A
Date: 2015-10-08 14:30
Category: Ambiente, Movilización
Tags: CAR, Derechos Humanos, Fusagasuga, Mineria, Minería en Cundinamarca, Minería en SIbaté, Movilización ciudadana, Radio derechos Humanos, Sibate
Slug: campesinos-de-sibate-se-oponen-a-la-destruccion-del-rio-muna-por-parte-de-comin-s-a
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/10/Sibate.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: sibatecundinamarca 

<iframe src="http://www.ivoox.com/player_ek_8867100_2_1.html?data=mZ2jmZaUdI6ZmKiakpWJd6KkkZKSmaiRdI6ZmKiakpKJe6ShkZKSmaiRh8Lh0crgy9PTt4zYxpDAy8fFuIa3lIqum5DXqYzj0dTbx9OPpYzgwpDRx9jYttbXxM6SpZiJhpTijMnSzpC2cYarpJKw0dPYpcjd0JC%2Fw8nNs4yhhpywj5k%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Giovanni castillo, Líder Campesino] 

###### [8 oct 2015] 

[En Sibate (Cundinamarca) la comunidad denuncia que la empresa italiana Comind S.A lleva cuatro años de explotación en esta región y está **causando graves afecciones medio ambientales y estructurales en distintos municipios aledaños**, sin que las autoridades tomen acción frente a esto y responda únicamente con la presencia de la fuerza pública.]

Debido a su cercanía con el río Muña, las **fuentes hídricas se están viendo seriamente afectadas** *“no tenemos el agua suficiente que teníamos hace cinco seis años, ha disminuido muchísimo”*, además “del polvo que hay en el aire”, que afecta a los pobladores por la extracción de la mina, indica el líder campesino, Giovanni castillo. Quien agrega que no solamente la población de Sibaté es la afectada sino que también Fusagasugá y otros municipios aledaños.

Otro de los problemas ha sido **en la carretera** la cual esta hecha para resistir camiones con carga de 15 Toneladas y por estas están pasando “25 volquetas diarias” con 35 Toneladas diarias.

La **CAR y la Agencia Nacional de Minería** dieron el permiso a la empresa Comind S.A “sabiendo que en una fuente de esas donde hay páramos, hay fuentes hídricas, no se pueden explotar porque hay muchos recursos naturales”, indica Castillo, pero “no se sabe porqué dieron el permiso”.

Frente a la difícil situación con esta empresa y la ausencia del Estado para resolver “el daño medio ambiental que nos están haciendo a nosotros, como afirma el líder campesino, **la comunidad se ha movilizado pacíficamente** impidiendo el paso de las volquetas frente a lo que las autoridades han optado por enviar el ESMAD y la Policía, amedrentando a los pacíficos manifestantes.
