Title: América Latina retrocede en la garantía de derechos para las mujeres
Date: 2016-09-29 13:12
Category: Mujer, Nacional
Tags: cuerpo de la mujeres, Derechos reproductivos de las mujeres, Foro ‘Aborto legal en Colombia: Presente y Futuro’
Slug: america-latina-retrocede-en-la-garantia-de-derechos-para-las-mujeres
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/09/Despenalización-aborto.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Crónica De Sociales Org ] 

###### [29 Sept 2016] 

En América Latina se está viviendo un retroceso en la garantía de los derechos sexuales y reproductivos de las mujeres, asegura Anaís Córdova Páez, integrante de la Colectiva Salud Mujeres de Ecuador, quien pone como ejemplo que **en Brasil fueron eliminadas las causales que permitían abortar en casos de violación** o que en Ecuador fue prohibida la educación sexual en los colegios con la aprobación del 'Plan Familia', mientras durante los últimos tres años se han encarcelado a 156 mujeres por haber abortado, la mayoría de ellas de forma espontánea.

De acuerdo con Anaís, las mujeres más empobrecidas son las más criminalizadas pues son quienes tienen menos información acertada sobre el aborto, realizan el procedimiento de forma insegura y se ven obligadas a asistir a centros hospitalarios en los que la mayoría de veces "llega más rápido un policía que un médico". La activista agrega que hay casos en los que **las mujeres se desangran mientras los policías hacen el papeleo para capturarlas** o que son llevadas a la Fiscalía antes de recibir atención médica.

Cuando se criminaliza el aborto, se impide el acceso a información oportuna y acertada, expresa Anís e insiste en que las más afectadas son las mujeres más pobres, pues las que tienen mayores beneficios económicos pueden acceder a **abortos seguros, que en las clínicas ecuatorianas pueden llegar a costar 4 mil dólares**. Y es que la posición estatal tampoco favorece la situación, tras el terremoto ocurrido en Ecuador ha proliferado el contagio del virus del zika en mujeres embarazadas, que se debaten entre el desconocimiento del Gobierno y la posibilidad de dar a luz bebes con microcefalia.

Frente a esta problemática, la Colectiva Salud Mujeres decidió crear la primera línea de información aborto seguro (0998301317), que desde 2008 ha logrado que **muchas mujeres se informen sobre la interrupción voluntaria de un embarazo o métodos de anticoncepción**, como una apuesta que busca salirse de las esferas estatales para promover que haya también una despenalización moral del aborto, a través de actividades pedagógicas en comunidades, dirigidas no solamente a la población femenina.

Para la organización es fundamental llevar [[información acertada](http://media.wix.com/ugd/c5eb38_eccd350ba7754c88b11b7cb66a520482.pdf)] sobre la sexualidad a los lugares a los que no llega el Estado, pues la mayoría de los abortos son realizados de formas inseguras que ponen en peligro la vida de las mujeres, ante la estigamatización que ha pesado sobre este procedimiento y que ha llevado a que **quienes han elegido abortar hayan sido violentadas física y psicológicamente**. Por lo que para las activistas es necesario que se consoliden políticas públicas que contemplen todos los contextos que enfrentan las mujeres embarazadas y que a su vez todas tejan redes de hermandad entre ellas mismas.

Vea también: [[organizaciones y activistas conmemoraron el día internacional por la despenalización del aborto](https://archivo.contagioradio.com/organizaciones-y-activistas-conmemoraron-el-dia-por-la-despenalizacion-del-aborto/)]

<iframe id="audio_13114804" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_13114804_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)]] 

 
