Title: Plan pistola en marcha contra líderes indígenas: ACIN
Date: 2019-08-05 17:34
Author: CtgAdm
Category: DDHH, Líderes sociales
Tags: autoridades indígenas, guardia indígena, Líderes y lideresas sociales
Slug: plan-pistola-en-marcha-contra-lideres-indigenas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/08/WhatsApp-Image-2019-08-05-at-5.35.51-PM.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:Acin\_Cauca] 

**Enrrique Guejia, integrante de la comunidad indígena del Resguardo de Tacueyó, Toribio fue asesinado el sábado 4 de agosto en la vereda La Luz,** Guejia es el tercer líder asesinado en menos de una semana en el departamento del Cauca. El guardia y defensor de las comunidades indígenas tenía aproximadamente 70 años, era médico tradicional y Cambuwesx (alguacil) o autoridad indígena del resguardo.

La Asociación de Cabildos Indígenas del norte del Cauca (ACIN) confirmó la muerte de Enrrique, **su cuerpo fue hallado sin vida en la vereda La Luz, y según testimonios fue ultimado por dos hombres que se movilizaban en moto**. (Le puede interesar: **[Guardia Indígena es atacada en la vía Caloto-Toribio, Cauca](https://archivo.contagioradio.com/guardia-indigena-es-atacada-en-la-via-caloto-toribio-cauca/)**)

Al respecto, la Guardia Indígena realizó un rastreo por la zona y logró capturar a los presuntos autores del crimen. Estos hechos ponen en evidencia la grave situación que enfrenta el suroccidente de Colombia en cuanto a protección y defensa de las y los líderes sociales, en especial de las comunidades indígenas. (Le puede interesar: [José Eduardo Tumbo líder campesino asesinado en Caloto)](https://archivo.contagioradio.com/jose-eduardo-tumbo-lider-campesino-asesinado-en-caloto/)

### **Plan pistola contra comunidades indígenas** 

A su vez, la ACIN denunció vía Twitter que existe ** "un *plan pistola* contra las comunidades indígenas"**. Este acontecimiento sucede tres días después del asesinato del líder campesino José Eduardo Tumbo el sábado 3 de agosto, y de Gersaín Yatacué, coordinador de la guardia indígena de San Julián, Toribío. (Le puede interesar: **[Asesinan a Gersain Yatacue, coordinador y guardia indígena de Toribio, Cauca](https://archivo.contagioradio.com/asesinan-a-gersain-yatacue-coordinador-y-guardia-indigena-de-toribio-cauca/)**)

Para Hermes Pete, consejero y representante legal del  Consejo Regional Indígena del Cauca (CRIC), esta creciente ola de violencia corresponde a las amenazas emitidas por  grupos como las Águilas Negras, disidencias de las Farc y en los últimos días el cartel de Sinaloa a través de panfletos, en estos ,“se nos advierte que nos iban a asesinar a guardias, gobernadores, comuneros y habitantes que porten insignias de la Autoridad”, aseguró Pete.

### **A un año del Gobierno de Duque las comunidades no sienten garantías de protección** 

Asimismo, para el consejero estos sucesos obedecen a las dinámicas de narcotráfico y la violencia que se ha generado en el “postconflicto”, como resultado de la falta de "garantías estatales" que deberían proporcionar el Estado en medio de un proceso de implementación, tal como señaló Pete.

Al cumplirse un año de mandato del presidente Iván Duque, las comunidades indígenas ven un  desinterés del Gobierno por el proceso de paz, en donde los principales afectados son las habitantes de los territorios. “Tras un año lo que podemos ver es más guerra y violencia, lo que se puede ver es un país más desestabilizado”.

“En esta época estamos perseguidos por los criminales y por el Estado", señaló  el consejero quien se refirió a diversos ataques de diferentes actores armados contra la población indígena, a las que se  suman  enfrentamientos con la Fuerza Pública, como sucede en el municipio de Piendamó con la comunidad indígena  Misak y el Ejército Nacional.

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][<iframe id="audio_39648596" frameborder="0" allowfullscreen scrolling="no" height="200" style="border:1px solid #EEE; box-sizing:border-box; width:100%;" src="https://co.ivoox.com/es/player_ej_39648596_4_1.html?c1=ff6600"></iframe>[su correo][[Contagio Radio]
