Title: Unidad Nacional de Protección adjudicó contratos a empresas endeudadas con sus trabajadores
Date: 2016-02-25 15:49
Category: DDHH, Nacional
Tags: Agresiones contra defensores de DDHH, Analtraseg, Sinproseg, Unidad Nacional de protección
Slug: unidad-nacional-de-proteccion-adjudico-contratos-a-empresas-endeudadas-con-sus-trabajadores
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/02/sinproseg-contagioradio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: sinedian] 

<iframe src="http://co.ivoox.com/es/player_ek_10574843_2_1.html?data=kpWimZmceJShhpywj5WcaZS1lpiah5yncZOhhpywj5WRaZi3jpWah5yncbbiysnOxpCypcTd0NPOzpDIqYzE09Thx8jHrYa3lIqvldOPpcXe1snWxYqnd4a2lJDQ0dPYtsLo0NiYw5DJsY6ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Ramiro Leiton, integrante de SINPROSEG] 

###### [25 Feb 2016] 

Desde muy tempranas horas de la mañana cerca de 300 personas, todos ellos **contratistas de empresas de seguridad e integrantes de ANALTRASEG y SINPROSEG**, se dieron cita frente a las instalaciones de la Unidad Nacional de Protección para protestar contra las empresas adjudicatarias que **adeudan a los trabajadores cerca de 12 mil millones en el caso de “Guardianes”** que, con nuevo nombre, vuelve a ganar la licitación.

Según la denuncia de los escoltas, a través de Ramiro Leiton, integrante de SINPROSEG, uno de los principales problemas es que **desde hace más de 6 meses varias empresas adeudan los viáticos de los escoltas**, que deciden viajar junto a sus protegido porque de ello depende su seguridad, sin embargo, a pesar de justificar con antelación los viajes, las empresas se niegan a autorizar los pagos “hay gente a la que le deben hasta 8 millones de pesos” relatan.

Otra de las situaciones es la **falsificación de las firmas de las Cajas de Compensación** Familiar, que niegan los servicios cuando son requeridos por los escoltas alegando que no se encuentran inscritos, sin embargo, la empresa, al inicio del contrato entrega papeles en los que consta la inscripción.

Lo paradójico del caso es que la Unidad Nacional de Protección conoce los problemas que las empresas tienen con sus trabajadores y no hace nada. Incluso, durante la audiencia de adjudicación de contratos **se presentaron las mismas empresas con diferentes nombres y ganaron**. El problema es que cuando se acaba una empresa hay que liquidar a los trabajadores y eso no se hace. **A algunos escoltas les adeudan dos liquidaciones de empresas anteriores pero a la hora de reclamar no hay donde porque “la empresa ya no existe”**

Diego Mora, director de la **UNP afirmó que para iniciar los contratos las empresas debían ponerse al día con las deudas**, sin embargo no se firmó ningún documento con el que los trabajadores puedan reclamar sus derechos, lo cual podría significar que hay  niveles, por lo menos, de complicidad entre la UNP y las empresas a las que se adjudican los contratos para la [protección de miles de personas que ven amenazada su integridad.](https://archivo.contagioradio.com/de-156-defensores-de-ddhh-asesinados-en-2015-en-el-mundo-51-eran-colombianos-segun-informe-de-ai/)
