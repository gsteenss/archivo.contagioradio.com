Title: Siria denuncia a Israel por un bombardeo en el Golán
Date: 2015-01-19 20:57
Author: CtgAdm
Category: El mundo
Tags: Israel guerra Hezbollah, Noticias Derechos Humanos, Siria
Slug: siria-denuncia-a-israel-por-un-bombardeo-en-el-golan
Status: published

###### Fuente:Rtctv 

El gobierno sirio afirma que Israel incursionó en la ciudad conocida como Quneitra bombardeando con drones . También lo confirma el Observatorio Sirio de Derechos Humanos, organización del Reino Unido que monitorea todo el conflicto en el país.

Existen distintas versiones pero todas apuntan a que Israel atacó la conducción de Hezbollah, asesinando a 4 combatientes de la milicia, entre ellos el hijo de un conocido líder buscado por EEUU e Israel.

Qunetria es la zona fronteriza de Israel con Siria, situada en el valle de los altos del Golán, tradicionalmente ha sido una área de disputa entre Siria, Líbano e Israel, pasando a formar parte de este último como parte de la ocupación en guerras como el conflicto árabe-israelí o las guerras de los seis días o la del Yom Kimpur.

Actualmente destruida y en manos de la administración Siria, apenas está habitada y su población vive exclusivamente de prestar servicios al personal de Naciones Unidas. Siria rehúsa repoblarla y constituye un símbolo de la resistencia contra la ocupación israelí.

Por su ubicación no es posible entrar por la parte israelí, quedando la parte occidental de la propia ciudad como un corredor militar o “tierra de nadie” entre ambas fronteras. Hoy en día se ha convertido en blanco de ataques contra soldados israelíes por parte de las milicias libanesas y pro palestinas, a los cuales el Tzáhal responde bombardeando posiciones de la guerrilla o del propio ejército sirio.

Por otro lado el líder de Hezbollah Nasrallah había advertido el viernes pasado en un discurso televisivo, que la milicia contaba con misiles iraníes capaces de cubrir todo Israel, y que en caso de continuar los ataques contra Siria y Líbano responderían.

Tanto Hezbollah como la guerrilla Amal (prosiria y aliada de Hezbollah), nacen de procesos de resistencia de carácter islamista contra Israel, son las máximas expresiones de la comunidad Chií, mayoritaria en Líbano, financiadas por Irán y ambas están consideradas como organizaciones terroristas por parte de la UE y de EEUU.
