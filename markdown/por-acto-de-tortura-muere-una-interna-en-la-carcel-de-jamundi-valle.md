Title: Por acto de tortura muere una interna en la cárcel de Jamundí, Valle
Date: 2015-10-23 15:12
Category: DDHH, Nacional
Tags: actos de tortura en las cárceles, Adriana Bernal, Cárceles en Colombia, Fundación Comite de Solidaridad con Presos Políticos, INPEC, la cárcel de Jamundí, presos politicos, valle
Slug: por-acto-de-tortura-muere-una-interna-en-la-carcel-de-jamundi-valle
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/10/Presas-en-Colombia-e1472247144495.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: [www.noticiasggl.com]

###### [23 Oct 2015] 

De acuerdo a una denuncia del Comité de Solidaridad con los Presos Políticos - Seccional Valle del Cauca, ** la prisionera** **Adriana Bernal**  recluida en la cárcel de Jamundí, murió tras recibir tratos crueles tanto físicos como mentales. **Bernal, era una paciente psiquiátrica que por prescripción** médica le eran suministrados continuamente medicamentos psiquiátricos para controlar su enfermedad mental.

Según la denuncia, Adriana fue trasladada a un lugar al aire libre que “**consiste, al mejor estilo de los campos de concentración nazi, en un encerramiento de alambre y acero”, se trata de“La jaula o La perrera** que son indiscutiblemente lugares de tortura física y mental que son utilizados con fines de control, retaliación o castigo extremo por parte del INPEC contra la población reclusa”.

Estando en ese lugar la interna **debió soportar una noche con un fuerte aguacero, teniendo que soportar bajas temperaturas**. Las reclusas que se dieron cuenta de la situación empezaron a llamarla sin obtener respuesta de la interna que se encontraba en el piso en posición fetal y tras ver que no respondía,  llamaron a la guardia y avisaron.

Comité de Solidaridad con los Presos Políticos, relata que la guardia de la Cárcel de Jamundí se dirigió al lugar donde estaba Bernal, **allí en compañía del  médico de apellido Tafur la revisaron y se dieron cuenta que estaba muerta.** Una hora más tarde realizaron el levantamiento del cadáver.

“Algunas internas dicen haber escuchado al médico Tafur diciendo que la probable causa de la muerte de Adriana fue por hipotermia, resultado de haber estado a la intemperie”dice el comunicado del Comité, por lo que se solicitó a **la Fiscalía iniciar el proceso de investigación para conocer las causas de la muerte. Así mismo en el comunicado se rechaza este tipo de actos de tortura de parte del INPEC contra los reclusos.**
