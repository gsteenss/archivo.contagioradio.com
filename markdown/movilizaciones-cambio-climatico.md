Title: Así fueron las movilizaciones por el cambio climático
Date: 2019-09-20 17:49
Author: CtgAdm
Category: Ambiente, Movilización
Tags: cambio climatico, ClimaStrikeFest, ClimateStrike, FridaysForFuture, Movilización
Slug: movilizaciones-cambio-climatico
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/09/EE8HmaEWkAIXZ16.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:@PaolaGuzmanPhD] 

Desde este viernes hasta el próximo 27 de septiembre, más de 150 países se han sumado al movimiento por el cambio climático \#ClimateStrike, convocado por la organización **Fridays For Future**, que plantea la "defensa del futuro, de un planeta vivo y de un mundo justo”. Este paro coincide con la cumbre sobre la crisis climática que comienza el lunes 23 en la sede neoyorquina de las Naciones Unidas.

### **Movilizaciones en Colombia** 

A las 4:00 p.m. de este 20 de septiembre hubo una concentración en la Plaza de Bolívar, en Bogotá, organizada por “Pacto por el clima”. Asimismo, otras ciudades se han sumado a la iniciativa, como Medellín, con una movilización en el Parque de las Luces y Cali, en la plaza Caicedo. Este tipo de iniciativas se mantendrán del 20 al 27 de septiembre  a propósito de la Semana por el Clima.

Sílvia Gómez, directora de Greenpeace, afirma que “es una manera de exigirle al gobierno, al presidente y a las empresas privadas que hagan una transformación en sus políticas” puesto que consideran que “en Colombia hay unas políticas muy incoherentes respecto a la imagen que mostramos en los ámbitos internacionales y las decisiones en nuestras políticas domésticas. "

> A nuestro gobierno y empresas les falta demasiada responsabilidad y demasiado compromiso para entender la magnitud del problema”.

**Programación**

En Bogotá, la movilización que se dio en la Plaza Bolívar fue el inicio de una secuencia de actividades para el medio ambiente. En la página GlobalClimateStrike.net se pueden consultar las movilizaciones convocadas a nivel mundial. y aquí pueden consultar la programación para la ciudad de Bogotá. [(Le puede interesar: Con la Amazonía está en juego la supervivencia de la humanidad)](https://archivo.contagioradio.com/amazonia-supervivencia-humanidad/)

20 de septiembre: Plaza de Bolívar, Huelga General Global Por Emergencia Climática. 4:00 p.m.

21 de septiembre: Día de los Niños (KidsStrike), Plaza de Usaquén. Familias Por el Clima. 10:00 a.m.

22 de septiembre: Día Mundial Sin Carro y Colombia Sin Plásticos

23 de septiembre: Summit NY(ONU). Apertura de Campamentos de Emergencia. Plaza de Bolívar

23, 24, 25 y 26 de septiembre: Mesas Ciudadanas y Cabildos Abiertos por Emergencia Climática.

27 de septiembre: Acción O Extinción. Marcha Global por el Clima. Desde las 12:00 a.m.

ClimateStrikeFest /AmazonasSOS en el Parque de los Hippies. Desde las 4:00 p.m.

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
