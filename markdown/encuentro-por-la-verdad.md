Title: ¿Dónde están? el grito que retumbó en el Encuentro por la Verdad
Date: 2019-08-28 14:30
Author: CtgAdm
Category: DDHH, Nacional
Tags: comision de la verdad, desaparecidos, Encuentro por la Verdad, MOVICE, Unidad de Búsqueda de personas dadas por desaparecidas
Slug: encuentro-por-la-verdad
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/08/Encuentro-por-la-verdad.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/08/WhatsApp-Image-2019-08-28-at-10.26.33-AM.jpeg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/08/verdad.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/08/verdad-1.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/08/desaparecidos.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: ContagioRadio] 

La ciudad de Pasto, Nariño fue el lugar donde se llevó a cabo el **Segundo Encuentro por la Verdad** en el marco del  reconocimiento a las víctimas del conflicto armado realizado por la Comisión para el Esclarecimiento de la Verdad (CEV) y la Unidad de Búsqueda de  Personas dadas por Desaparecidas, este espacio no es solo un evento,es un dialogo social  donde diferentes familias nacionales e internacionales de desapariciones, podrán compartir sus experiencias entorno a la búsqueda de sus familiares.(Le puede interesar:[Segundo encuentro por la Verdad: un reconocimiento a familias de personas desaparecidas](https://archivo.contagioradio.com/encuentro-verdad-desaparecidas/))

![Encuentro por la Verdad](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/08/WhatsApp-Image-2019-08-28-at-10.26.33-AM-1024x505.jpeg){.aligncenter .size-large .wp-image-72742 width="1024" height="505"}

[Foto:@ComisionVerdadC]

Para Martha Ceballos, integrante del Movimiento Nacional de Víctimas de Crímenes de Estado (MOVICE), este es un aporte que engrandece el **intercambio de experiencias**, y así mismo permite esclarecer los registros que tienen estas entidades frente a las víctimas y los nombres de los desaparecidos, unificando las bases de datos que tienen cada una de las organizaciones colombianas.

Son más de 12 procesos de familias que están en pie de lucha, así como la concertación de métodos de búsqueda, presentados en 4 puntos, de los que Ceballos destaca dos, la prevención y la búsqueda, “Para eso tenemos toda una línea de trabajo que hemos venido dialogando con la Comisión de la Verdad y la Unidad de Búsqueda de Personas dadas por desaparecidas”.

### Experiencias que aportan desde el exterior a la búsqueda de personas desaparecidas

Así mismo los diálogos con invitados internacionales de México, Guatemala, Malí y Argentina, han sido valiosos para el enriquecimiento de los procesos de búsqueda en Colombia, y al mismo tiempo generan soluciones a las  diferentes problemáticas que tienen que afrontar las madres y familias buscadoras en el mundo, “en El Salvador, como en muchos países, el Estado sigue siendo indolente con las madres de los desaparecidos”, señala **Eduardo García de la Asociación Pro-Búsqueda de Niñas y Niños. **(Le puede interesar: [Proyecto de Centro Democrático pone en riesgo la verdad de los Colombianos )](https://archivo.contagioradio.com/centro-democratico-riesgo-verdad/)

El encuentro ha dado lugar a una serie de eventos culturales entre ellos bailes, dramatizados, cantos y ´*rituales de sanación´,* que brindan serenidad y fortaleza a los familiares y al mismo tiempo, una despedida simbólica al alma de las víctimas desaparecidas.

> "la búsqueda es creer en lo posible y en lo imposible, es un ejercicio de esperanza"

En el auditorio Javeriano se llevó a cabo el último día de este encuentro, donde los diferentes representantes departamentales, líderes de las asociaciones y comisionados hicieron un intercambio de símbolos como reconocimiento al trabajo realizado, "la búsqueda es creer en lo posible y en lo imposible, es un ejercicio de esperanza” declaró el comisionado Alejandro Valencia.

![Reconocimiento](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/08/verdad-1-1024x682.jpg)

La obra "corazón abierto" fue la estatuilla que se otorgó a  las mujeres y familiares que desde cada territorio participan activamente en la búsqueda, para Pedro Ruíz, artista encargado de hacerla, [el arte siempre ha sido un elemento de consuelo y de transformación, por eso inspiró su obra en el amor como arma para sobrevivir, “Hemos creado formas de sanarnos y acompañarnos. Hemos contado nuestra historia una y otra vez para seguir viviendo”, concluyeron las mujeres buscadoras.]{.css-901oao .css-16my406 .r-1qd0xha .r-ad9z0x .r-bcqeeo .r-qvutc0}

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
