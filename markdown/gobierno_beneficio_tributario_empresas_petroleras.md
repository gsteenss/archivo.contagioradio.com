Title: Gobierno dará nuevo beneficio tributario a petroleras en medio de profundo déficit fiscal
Date: 2017-09-26 17:53
Category: Economía, Entrevistas
Tags: Certificado de Reembolso Tributario, empresas petroleras, Ministerio de hacienda
Slug: gobierno_beneficio_tributario_empresas_petroleras
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/07/petroleo1-e1468449932490.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Confidencial Colombia] 

###### [26 Sep 2017] 

[En medio de las exigencias de las comunidades de que no se continúe desarrollando proyectos petroleros que atentan contra el ambiente y la soberanía de los territorios, las empresa petroleras  se verán beneficiadas por un incentivo tributario con el cual **el Gobierno pretende acelerar la búsqueda de crudo en el subsuelo colombiano**]**.**

Se trata de una medida que otorgaría US\$0,15 por cada dólar invertido a las compañías petroleras, siempre y cuando el precio del barril de crudo esté por debajo de US\$60. Un beneficio al que podrán aplicar las empresas mediante un proceso de precalificación de los proyectos de inversión.

“Estamos convencidos de que el sector necesita un impulso para hacer más inversión que nos permita no solo aumentar las reservas, sino evitar una caída en la producción de petróleo”, dice el ministro. Sin embargo, para Álvaro Pardo, economista y director del centro de estudios mineros Colombia Punto Medio, **"El gobierno es *manisuelto* a la hora de entregar más beneficios".**

Según explica el economista, la DIAN señala que las empresas en Colombia cuentan con 253 beneficios tributarios distintos para reducir pagos por impuestos, de los cuales, son las empresas del sector minero y petrolero las que más pueden sacar provecho.

### **El origen del nuevo beneficio tributario**

Pardo asegura que el gobierno avanza de acuerdo a la agenda de las empresas. Ese último beneficio nació en el congreso de la Asociación Colombiana de Petroleros, con una lista de doce peticiones  de los que resaltan dos puntos. Uno que se refieren a que a las empresas de crudo se les permita que **el 50% de los impuestos los paguen realizando obras en los lugares donde trabajan, es decir que al gobierno solo le llegará realmente el 50% de los impuestos.**

Esa última medida es copia de una en Perú, con la que se han evidenciado problemas, como que las empresas solo han construido vías que ellos necesitan para su operación, que pagan con impuestos y realmente no se trata de obras que contribuyan al progreso de la comunidad.

El segundo es **el restablecimiento de Certificado de Reembolso Tributario**, que solo podían usar  empresas nacionales, sin embargo, el gobierno nacional ha decidido dar ese privilegio a las empresas petroleras de cualquier orden. Esto quiere decir que **por cada por cada 100 dólares que las compañías petroleras inviertan, el gobierno les va a devolver 15 dólares,** que pueden usar para pagar impuestos en los próximo 100 años, lo que profundiza el déficit fiscal que ya asciende a unos 37 mil millones de pesos.

"Durante la época que duró el boom de precios, recurrentemente el gobierno gastó más de lo que ingresó, y encima se le hace nuevas deducciones a las empresas petroleras. Es decir que **la única forma de recaudar recursos será con impuestos a la ciudadanía",** considera Pardo.

El economista destaca lo que ha sido la actuación de las comunidades que además de hacer frente a la locomotora minero-energética con las consultas populares, han tomado la decisión de no impedir la minería en un 100% pues saben que se necesitan ciertos minerales e hidrocarburos, y en cambio piden que se modifique radicalmente modelo minero y la política económica para que realmente sirva a los colombianos.

<iframe id="audio_21128564" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_21128564_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)
