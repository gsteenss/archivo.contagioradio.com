Title: Presupuesto Nacional 2021: Antesala de reforma tributaria y ataque a la paz
Date: 2020-10-23 12:40
Author: AdminContagio
Category: Actualidad, Nacional
Tags: PGN, Presupuesto, Presupuesto General de la Nación, Reforma tributaria
Slug: presupuesto-nacional-2021-antesala-de-reforma-tributaria-y-ataque-a-la-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/10/Presupuesto-2021-antesala-de-una-reforma-tributaria.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

La reciente aprobación del Presupuesto General de la Nación -PGN- para el año 2021 ha suscitado críticas y preocupaciones en varios sectores, sobre la destinación de los recursos y los sectores sociales y económicos más afectados con la estructuración de cada rubro. (Lea también: [Duras críticas generó aprobación del Presupuesto de la Nación para 2021](https://archivo.contagioradio.com/duras-criticas-genero-aprobacion-del-presupuesto-de-la-nacion-para-2021/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Si bien el Gobierno y las mayorías parlamentarias destinaron una cifra de 313,9 billones de pesos como monto global del presupuesto, al momento de desagregar los rubros se advierte que **solo en pago de deuda pública (cuyos benefactores son los bancos) y en Policía y Defensa están concentrados el 34% de todo el presupuesto**, lo cual según varios sectores opera en perjuicio de la inversión social.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El senador Wilson Arias, uno de los principales críticos al PGN del año 2021, señaló que este sigue obedeciendo a una lógica propia de la “*ortodoxia neoliberal*” en la que la liquidez se le da a los bancos  y los recursos de la salud se canalizan a través de las EPS´s.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El congresista advirtió que en el presupuesto se previó un incremento en el recaudo del Impuesto al Valor Agregado -IVA- cercano a un 42% lo cual “*hará mucho más regresiva la estructura tributaria*” y aun así, las sumas que se esperan recibir no van a ser suficientes para solucionar los asuntos que se tienen que financiar con el presupuesto.

<!-- /wp:paragraph -->

<!-- wp:quote -->

> **"Tenemos uno de los presupuestos más reaccionarios de la historia de Colombia"**
>
> <cite>Wilson Arias, Senador de la República</cite>

<!-- /wp:quote -->

<!-- wp:paragraph -->

Frente al tema del aumento en el recaudo del IVA, Jairo Bautista, magister en administración pública, señaló que en el presupuesto hay cuentas absolutamente absurdas, pues según el analista, la única forma de recaudar la cifra estimada por el Gobierno a través de ese impuesto, es que hubiese una “*megarecuperación económica*”, lo cual no se prevé para un escenario de pospandamia; y la otra sería a través de una reforma tributaria para el año entrante, la cual según el experto, ha venido siendo anunciada implícitamente por el Gobierno a través del Ministro de Hacienda, Alberto Carrasquilla.

<!-- /wp:paragraph -->

<!-- wp:quote -->

> **“Posiblemente estemos enfrentado una reforma tributaria en el primer trimestre del 2021”**
>
> <cite>Jairo Bautista, Magister en Administración Pública</cite>

<!-- /wp:quote -->

<!-- wp:paragraph -->

Bautista anticipó que para alcanzar esos montos de recaudo, la reforma venidera podría llegar a gravar incluso los bienes hasta ahora exentos, como el caso de los productos de la canasta familiar, aquellos alimentos esenciales con los cuales se abastece la ciudadanía para su alimentación básica.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En similar sentido, se expresó Diego Carrero, economista y magister en estudio políticos, quien señaló que había un desfase en las estimaciones del Gobierno para financiar el presupuesto, pues se amparaba en un hipotético crecimiento económico superior al 6%; lo cual se contradice con las proyecciones realizadas por diferentes organismos como el Fondo Monetario Internacional, el Banco Mundial, la CEPAL, la OCDE y hasta el mismo MinHacienda, los cuales, apuntan a que en Colombia “*vamos a tener una recesión este año que va a girar entre el 5 y el 7%*”, esto es, “*la peor crisis en la historia republicana de Colombia*”.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Frente a esto, agregó que la única manera que utilizaría el Gobierno para cubrir el hueco fiscal que vendría luego de que no se consolidara el crecimiento económico esperado, sería justificar la necesidad de una reforma, como históricamente lo ha venido haciendo, la cual afectaría “*los bolsillos de personas de estratos medios y bajos*”.

<!-- /wp:paragraph -->

<!-- wp:quote -->

> **"Lo que se acaba de aprobar en el Congreso como Presupuesto Nacional es la antesala de una nueva reforma Tributaria"**
>
> <cite>Diego Carrero, Economista y Magister en Estudio Politícos</cite>

<!-- /wp:quote -->

<!-- wp:heading {"level":3} -->

### **El desfinanciamiento de la Paz**

<!-- /wp:heading -->

<!-- wp:paragraph -->

Otro de los puntos que generó críticas fue la no aprobación por las mayorías del Congreso de una partida presupuestal para la Jurisdicción Especial para la Paz -[JEP](https://www.jep.gov.co/Paginas/Inicio.aspx)-, la cual estaría llamada a atender temas de protección a víctimas y testigos; la representación y asesoría jurídica de las víctimas y la inversión en tecnología para agilizar y acercar la justicia a los ciudadanos.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El senador Wilson Arias expresó frente a este tema que ya que “*el Gobierno no había podido hacer trizas los Acuerdos de Paz por la acción política de algunos sectores y la ciudadanía que se ha agrupado y manifestado en su defensa*” buscaba hacerlo ahora por la vía presupuestal afectando su financiación. (Lea también: [Presupuesto del 2021 se puede convertir en un nuevo ataque a la JEP](https://archivo.contagioradio.com/presupuesto-del-2021-se-puede-convertir-en-un-nuevo-ataque-a-la-jep/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Asimismo, Jairo Bautista, señaló que era sabido que la Paz iba a quedar de última en el orden de prioridad del Gobierno, no sólo por su desinterés en implementar el Acuerdo sino porque desfinanciando la JEP conseguiría debilitar el Acuerdo de Paz, evitando costear la inversión que implica la operación y funcionamiento del Sistema Integral de Verdad, Justicia, Reparación y No Repetición -SIVJRNR-.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### La acción ciudadana en la conformación del Presupuesto

<!-- /wp:heading -->

<!-- wp:paragraph -->

Diego Carrero, expresó que en Colombia tiene que haber un cambio político, porque mientras sean los mismos sectores de siempre los que estén representados en el Gobierno y en el Congreso; los resultados siempre serán los mismos, por lo cual advirtió que “*se necesita una ciudadanía empoderada e involucrada en estos temas para exigir cambios estructurales en la proyección presupuestal que se realiza cada año*”.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por su parte, Jairo Bautista coincidió en que tiene que existir un involucramiento de los sectores ciudadanos en la conformación del presupuesto, exigiendo masivamente la disposición de unas partidas acordes con las necesidades del país, agregó que la ciudadanía tiene que movilizarse en torno al debate del presupuesto e insistió en que hay que descomplejizar el asunto del presupuesto para que sea más digerible para la ciudadanía en lo cual podían contribuir los medios alternativos.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Adicionalmente, Bautista señaló que la gente tiene que comprender que el presupuesto se construye con el pago de los impuestos que paga  cada ciudadano y que solo en esa medida se podría apropiar del tema para exigir una idónea destinación de los recursos.

<!-- /wp:paragraph -->

<!-- wp:core-embed/facebook {"url":"https://www.facebook.com/contagioradio/videos/4799645393408836","type":"rich","providerNameSlug":"embed-handler","className":""} -->

<figure class="wp-block-embed-facebook wp-block-embed is-type-rich is-provider-embed-handler">
<div class="wp-block-embed__wrapper">

https://www.facebook.com/contagioradio/videos/4799645393408836

</div>

</figure>
<!-- /wp:core-embed/facebook -->

<!-- wp:block {"ref":78955} /-->
