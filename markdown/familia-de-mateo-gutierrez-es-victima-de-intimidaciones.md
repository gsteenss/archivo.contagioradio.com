Title: Familia de Mateo Gutiérrez es víctima de intimidaciones
Date: 2020-02-05 17:50
Author: CtgAdm
Category: DDHH, Judicial
Tags: falso positivo judicial, intimidaciones, Mateo Gutierrez, Prisionero Político, proyectil
Slug: familia-de-mateo-gutierrez-es-victima-de-intimidaciones
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/Mateo-gutierrez.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

La familia de Mateo Gutiérrez, denunció que fue víctima de intimidaciones el pasado 4 de febrero, cuando en la casa de Aracely León, madre del ex prisionero político, se encontró un proyectil de arma de fuego. Tras el hallazgo la familia se percató de un impacto de fuego en la ventana del hogar que da hacía una vía principal.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Según el reporte de la seguirdad del conjunto en donde está ubicada la vivienda, hasta el pasado 3 de febrero el inmueble se encontraba en perfecto estado. Asimismo la familia aseguró que "en indagaciones con la Policía del cuadrante, no se presentaron alteraciones del orden público que involucraran el uso de armas de fuego en el sector".

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

La familia también resaltó que estos hecho se presentan 15 días después de que el Tribunal Superior de Bogotá confirmará la absolución a Mateo Gutiérez, proferida tras un proceso judicial que lo tuvo privado de su libertad durante 21 meses. (Le puede interesar:["A un año de la Libertad de Mateo Gutiérrez ¿qué ha pasado?"](https://archivo.contagioradio.com/a-un-ano-de-la-libertad-de-mateo-gutierrez-que-ha-pasado/))

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Persisten las intimidaciones en contra de la Familia

<!-- /wp:heading -->

<!-- wp:paragraph -->

Desde que Mateo Gutiérrez fue privado de su libertad, por ser víctima de un falso positivo judicial, su familia inició una campaña para defender su inocencia y retornarlo a libertad. Sin embargo, al mismo tiempo comenzaron a ser objeto de intimidaciones y hostigamientos.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Aracely León, madre de Mateo, ha denunciado en diversas ocasiones, las persecuciones de las que ha sido objeto, al igual que las interceptaciones a sus líneas telefónicas.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En ese sentido, la familia manifestó que "este tipo de acciones se han convertido en un común denominador contra las familias, procesos y personas sometidas a la detención arbitraria y los montajes judiciales". Razón por la cuál exigen al Estado colombiano poner fin a estas prácticas que desconocen los derechos humanos y las garantías constitucionales.

<!-- /wp:paragraph -->
