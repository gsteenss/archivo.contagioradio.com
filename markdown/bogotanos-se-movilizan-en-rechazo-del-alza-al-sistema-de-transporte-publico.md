Title: Bogotanos se movilizan en rechazo al alza del sistema de transporte público
Date: 2017-04-03 13:33
Category: Movilización, Nacional
Tags: Alcaldía de Bogotá, Enrique Peñalosa, Transmilenio
Slug: bogotanos-se-movilizan-en-rechazo-del-alza-al-sistema-de-transporte-publico
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/04/pulzo.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Pulzo] 

###### [03 Abril 2017] 

Desde que el Alcalde de Bogotá Enrique Peñalosa dio a conocer el alza del 11% sobre el sistema de transporte público de la Capital, los ciudadanos han expresado su inconformismo a través de **bloqueos, colatones y movilizaciones, hoy estudiantes de diferentes universidades, convocaron una marcha para rachazar y exigir que se disminuya el precio del pasaje.**

**El punto de encuentro de este lunes, será el Planetario distrital a las 6:00pm y se dirigirá al Palacio del Lievano**. Se espera que también se produzcan movilizaciones desde la Universidad Nacional y la Universidad Pedagógica.

**Bogotanos toman acciones legales contra el incremento de la tarifa de transporte**

Este es el aumento más alto que se realiza en 8 años en Bogotá y que, además, como lo han manifestado los ciudadanos, **está por encima del reajuste que se hizo al salario mínimo que fue del 7%, afectando así el mínimo vital de las personas**, razón por la cual ya se radicó una acción popular que expone que esta medida no solo es injustificada, sino que vulnera los derechos de los ciudadanos.

En la misma se pide al juzgado que mientras se realiza la investigación a fondo sobre el alza al pasaje, **se impongan medidas cautelares que frenen el incremento**. Otras iniciativas como demandar el decreto de aumento ante el Tribunal de Bogotá ya fueron instauradas por el Centro Democrático. Le puede interesar: ["Aumento del transporte público de Bogotá el más alto en 8 años"](https://archivo.contagioradio.com/aumento-en-transporte-publico-en-bogota-el-mas-alto-en-8-anos/)

\[caption id="attachment\_38748" align="alignnone" width="638"\]![Infografía El Espectador](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/04/WhatsApp-Image-2017-04-03-at-8.34.38-AM-1.jpeg){.size-full .wp-image-38748 width="638" height="356"} Infografía El Espectador\[/caption\]

De igual modo, desde la semana pasada, estudiantes de la Universidad Pedagógica Nacional han desarrollado bloqueos en la estación de la calle 72, en diferentes horarios del día, para exigir que el alcalde de Bogotá eche para atrás esta medida, manifestaciones que han sido reprimidas por agentes del ESMAD y que, de acuerdo con estudiantes, **han hecho un uso excesivo de su fuerza provocando diferentes heridas a personas que participaban de los bloqueos.**

Las quejas de los usuarios frente a este aumento radican en tres puntos: el primero es la falta de rutas y la disminución en los tiempos de espera, el segundo es la aglomeración que hay en las estaciones y portales de Transmilenio y el tercero es el mal estado de los articulados de este transporte, que han tenido serios incidentes como el del **pasado 2 de abril cuando uno de estos buses se incendió en la estación de Coliseo en la NQS.** Le puede interesar: ["29 de marzo jornada de movilización contra Transmilenio"](https://archivo.contagioradio.com/29-marzo-jornada-movilizacion-transmilenio/)

###### Reciba toda la información de Contagio Radio en [[su correo]
