Title: Más de un millón de mujeres multiplican sus apuestas de paz
Date: 2016-03-09 17:34
Category: Nacional, Paz
Tags: un millon de mujeres de paz
Slug: mas-de-un-millon-de-mujeres-multiplican-sus-apuestas-de-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/03/Un-millón-de-mujeres.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio ] 

###### [9 Mar 2016] 

En el marco de la conmemoración del Día Internacional de la Mujer, el pasado martes se reunieron en el Centro de Memoria Histórica [['Un Millón de Mujeres de Paz'](https://archivo.contagioradio.com/nosotras-nos-merecemos-vivir-en-un-pais-en-paz/) ]para compartir con organizaciones provenientes de distintas regiones del país lo que para ellas significa la concreción de la paz en los territorios, buscando que los acuerdos de paz que se pacten sean implementados desde una perspectiva territorial y de género, que permita paridad en la participación institucional y política, así como en el mecanismo de verificación del cese al fuego definitivo y la entrega de armas.

<iframe src="http://co.ivoox.com/es/player_ek_10740087_2_1.html?data=kpWklpWUfJihhpywj5WYaZS1lpiah5yncZOhhpywj5WRaZi3jpWah5ynca6ZpJiSo5bXb8XZjNrbjdLNsM2ZpJiSpJjSb8XZjNLizMrWqdSfztrZ1s7UsMrXwtOY1drXb8Lk1srg1sbXb46ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Angela Robledo] 

La representante Angela María Robledo, aseveró que nuestro país necesita una segunda oportunidad de la que son semilla las mujeres, pues en el escenario de solución política al conflicto armado el primer territorio a recuperar y reinventar es el cuerpo femenino como primer territorio apropiado por la guerra, que ha tenido que huir del despojo pero que también se ha politizado para recuperar la voz, "un cuerpo en el que reencarnara la paz". De acuerdo con Robledo la paz implica una revolución desde los hogares que requiere de hombres a cargo de las actividades que demandan un hogar y de un Estado que reconozca las implicaciones de esta labor.

<iframe src="http://co.ivoox.com/es/player_ek_10740113_2_1.html?data=kpWklpWVdZShhpywj5WXaZS1lpmah5yncZOhhpywj5WRaZi3jpWah5ynca6ZpJiSo5bXb8XZjNrbjdLNsM2ZpJiSpJjSb8XZjNLizMrWqdSfztrZ1s7UsMrXwtOY1drXb8Lk1srg1sbXb46ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Viviane Morales] 

Por su parte la senadora Viviane Morales, afirmó que no puede permitirse a los altos comisionados de Gobierno continuar "secuestrando" el proceso de paz y haciéndole "conejo" a la ciudadanía, mucho menos a las mujeres, pues de su veeduría depende la efectiva implementación de los acuerdos de paz, una tarea que tiene que comenzar con nuevos planes de ordenamiento territorial que impidan que los recursos públicos queden en manos de las estructuras paramilitares de distintos municipios del país, en convivencia con sectores políticos.

<iframe src="http://co.ivoox.com/es/player_ek_10740139_2_1.html?data=kpWklpWVd5qhhpywj5WYaZS1lp6ah5yncZOhhpywj5WRaZi3jpWah5ynca6ZpJiSo5bXb8XZjNrbjdLNsM2ZpJiSpJjSb8XZjNLizMrWqdSfztrZ1s7UsMrXwtOY1drXb8Lk1srg1sbXb46ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Cecilia López] 

La activista política Cecilia López, refirió la necesidad de que el trabajo no remunerado que las mujeres realizan día a día en los hogares conocido como 'economía del cuidado' sea medido en el Producto Interno Bruto, teniendo en cuenta que su contribución es de 19%, frente al 6% que aporta la agricultura, el 11% de la industria y el 18% del sector financiero. Así mismo afirmó la necesidad de implementar acciones que permitan a las mujeres lograr independencia económica, como "principio de reducción de la violencia", y que también incluya el que las mujeres puedan ser Presidentas, Consejeras de Paz o Ministras de Hacienda.

<iframe src="http://co.ivoox.com/es/player_ek_10740207_2_1.html?data=kpWklpWWdJihhpywj5WYaZS1k56ah5yncZOhhpywj5WRaZi3jpWah5ynca6ZpJiSo5bXb8XZjNrbjdLNsM2ZpJiSpJjSb8XZjNLizMrWqdSfztrZ1s7UsMrXwtOY1drXb8Lk1srg1sbXb46ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

Desde el sector de mujeres campesinas habitantes de Sumapaz, la construcción de paz tiene que ver con la desmilitarización de los territorios, así como el reconocimiento de la propiedad, uso y dominio de la tierra por parte de las comunidades, "tiene que haber una transformación a favor nuestro porque sino con diálogo o sin diálogo, con acuerdo o sin acuerdo, sí las condiciones de nosotras las mujeres campesinas y rurales en el país no se transforman, nosotras seguiremos en lo mismo otro 8 de marzo en el que se repetiran las mismas cosas".

Otras de las mujeres ponentes en este panel dirigido por Piedad Córdoba fueron María Mercedes Maldonado, Aida Avella, Gloria Cuartas y Orsinia Polanco quienes aseguraron que la paz implica transformaciones estructurales que permitan a todos los colombianos, pero en especial a las mujeres mayor democracia, inclusión, participación y garantía de derechos, en clave de la defensa de lo público, de los bienes naturales y de las apuestas de paz que distintas comunidades han construido desde los territorios en los que la guerra han minado los tejidos sociales y violentado sexualmente a las mujeres.

##### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
