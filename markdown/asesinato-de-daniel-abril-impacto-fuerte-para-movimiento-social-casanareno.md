Title: Asesinato de Daniel Abril: impacto fuerte para movimiento social casanareño
Date: 2015-11-17 11:44
Category: DDHH, Nacional
Tags: Asesinato Daniel Abril, Asesinato en Trinidad, Casanare, Comité Cívico por los Derechos Humanos del Meta, congreso de los pueblos, Corporación Claretiana Norman Pérez Bello, COSPACC, MOVICE, Trinidad
Slug: asesinato-de-daniel-abril-impacto-fuerte-para-movimiento-social-casanareno
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/11/Daniel-Abril.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Las 2 Orillas 

<iframe src="http://www.ivoox.com/player_ek_9417542_2_1.html?data=mpmemZqYdo6ZmKiak5WJd6KmlZKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRhdTZ1M7bw9nTb8XZjKnO0M7JsIy1w9fWzoqXhYzdztXOxdnTb8fpxtfhx5DUpdPVjNLc2M7RrcbijoqkpZKns8%2FowszW0ZC2pcXd0JCah5yncZU%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Jaime León, Corporación Claretiana NPBE] 

###### [17 Nov 2015 ]

[El pasado viernes sobre las 6:40 de la tarde en el municipio de Trinidad, Casanare fue **asesinado el líder comunitario, ecologista, defensor de derechos humanos** y padre de un menor de 12 años, **Daniel Abril**, de 38 años de edad, quien se encontraba sentado en una panadería de la zona, cuando hombres aún desconocidos le propinaron tres disparos que lo dejaron sin vida.   ]

[Jaime León miembro de la Corporación Claretiana NPBE y quien trabajó al lado de Abril, señala que el líder campesino se destacó por su **lucha contra los intereses de las multinacionales petroleras presentes en Casanare**, participando activamente de las audiencias públicas de 2014 y protestas adelantadas en 2010 y 2011, con el fin de denunciar los graves impactos de su accionar para la región. Así mismo, **denunció sin reparos a funcionarios públicos inmiscuidos en líos de corrupción** en el departamento.  ]

[Desde el año pasado, junto con organizaciones defensoras de derechos humanos, adelantaba una **campaña de denuncia contra Corporinoquía por su inoperancia y complacencia con las multinacionales petroleras** que arrasan con el agua y el territorio del Casanare. "Al ser una corporación que abarca 5 departamentos de la Orinoquía, con una gran responsabilidad ambiental, **no sanciona, no investiga o archiva las investigaciones**" por lo que se exige la destitución de su directora Martha Plazas, tal como indica León.  ]

[Daniel Abril también hacia parte de plataformas de movilización social como la Voz de la Tierra, el MOVICE, el Congreso de los Pueblos, el Comité Cívico por los Derechos Humanos del Meta y trabajaba articuladamente con la Corporación COSPACC y la Corporación Claretiana Norman Pérez Bello.]

[Según indica León, Daniel Abril fue **víctima de constante persecución**,** **en 2005 sufrió un **intento de desaparición forzada por parte de la Policía y miembros del Ejército**, de la que sobrevivió siendo enjuiciado injustamente, así mismo hace dos meses habría tenido un incidente con la Brigada XVI del Ejército Nacional con sede en Yopal.]

[Las distintas organizaciones sociales con las que Daniel Abril trabajaba, así como sus familiares exigen al Estado que la Fiscalía General de la Nación investigue seriamente los móviles y responsables de su asesinato, pues según los últimos informes los **vídeos de una cámara de seguridad** que habría registrado el hecho **no han sido incluidos en la investigación que ya se inició**.]

[Jaime León concluye llamando la atención sobre los **más de 10 líderes sociales** que en el municipio de Trinidad se encuentran **amenazados**, por lo que esta semana esperan la visita de Naciones Unidas en la que las organizaciones defensoras de derechos humanos de la región harán la denuncia formal del hecho. ]
