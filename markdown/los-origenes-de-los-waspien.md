Title: El origen de los Waspien
Date: 2016-09-06 09:09
Category: Comunidad
Tags: comunidades, Cultura, historia
Slug: los-origenes-de-los-waspien
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/09/Waspien-1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: noctumar 

###### 6 Sep 2016 

Los[ mayores cuentan que los Waspien vienen existiendo desde la creación del hombre. Los Waspien son invisibles, viven en los bosques primarios, en algunos afluentes del río San Juan y en otras quebradas donde habitan los Wounaan.]

[Los Waspien fueron Wounaan y como costumbre realizan rituales acompañados con la danza, ataviados con diseños corporales en forma de culebrilla. Ellos son espíritus semidesnudos, el color de su piel es más claro que el de los Wounaan y tanto hombres como mujeres van cubiertos con damagua (Materia prima para artesanías extraída de una palma) decorada  desde la cadera hasta la mitad del muslo. Los hombres tienen los cabellos largos hasta la nuca y llevan un collar de semillas de colores café y blanco. Las mujeres tienen el cabello largo hasta la cintura, con collares hechos en semillas que le llegan hasta el ombligo y se pintan con jagua, al igual que los hombres. Tienen un lenguaje autóctono o nativo sin intervención de otros, con palabras como thakᵾᵾr (chicha).]

Ewandam (Dios)  dejo a los Waspien invisibles en esta tierra y con poderes espirituales (chi mie), los mando a vivir en otros espacios de este mundo y en este mundo visible dejo a los Wounaan si el chi mie. Los espíritus Waspien son poderosos, pueden trasladarse a otros mundos, saben el manejo de todos los espíritus de la flora y la fauna y para ayudar comenzaron a guiar en algunos casos la vida de los benkhᵾᵾn.

Cuentan los mayores que los Waspien viven, por lo general, en las cabeceras de las quebradas, a las orillas de las lagunillas, en las partes bajas de la loma, en los charcos y en las riberas de los ríos y del mar. Algunas veces se escuchan voces, murmullos, y se ven en algunos sitios específicos y esteros, como el Félix y el Bagrero, que son afluentes del río San Juan, también por las playas o los diferentes lugares donde se encuentran los Wounaan. Están cerca a los Wounaan y protegen los peces y los bosques, por eso hay que pedir ayuda e invocar a los Waspien para cazar, cultivar, pescar y realizar algunas actividades en los territorios.

#### [![Saul Chamarra](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/09/Saul-Chamarra--150x150.jpg)

###### *Como parte del trabajo de formación de Contagio Radio con comunidades afros, indígenas, mestizas y campesinas en toda Colombia, un grupo de comunicadores realizó una serie de trabajos periodísticos y relatos que recoge las historias de las comunidades. Nos alegra compartir sus trabajos con nuestros y nuestras lectoras.* 

 
