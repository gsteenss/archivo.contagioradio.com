Title: Una semana para vivir el cine colombiano
Date: 2016-08-18 12:34
Author: AdminContagio
Category: 24 Cuadros
Tags: Cine Colombiano, Gente de bien, Ruido Rosa, Semana del Cine colombiano
Slug: una-semana-para-vivir-el-cine-colombiano
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/08/semana-de-cine.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Proimágenes Colombia 

###### 17 Agos 2016

Del 18 al 24 de Agosto en 197 municipios de 32 departamentos del país, se llevará a cabo la sexta edición de la Semana del Cine Colombiano, en la que se proyectarán 55 películas colombianas estrenadas entre el 2014 y el 2016.

La muestra liderada por el Ministerio de Cultura, con el apoyo de Proimágenes, en alianza con el diario El Tiempo, Señal Colombia y Retina Latina; busca que estas películas puedan ser vistas en lugares del país que no han estado en los circuitos de exhibición y en municipios en los que el acceso al cine es limitado.

[![Web](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/08/El-cine-que-somos-e1471455417156.jpg){.wp-image-27934 width="564" height="320"}](https://archivo.contagioradio.com/?attachment_id=27934)

Entre las películas que participarán en esta semana, se encuentran producciones nacionales premiadas en distintos festivales de cine en el mundo, como: ‘Gente de Bien’ del director Franco Lolli o ‘Ruido Rosa’, de Roberto Flores.

Bajo el lema “El Cine Que Somos”, la sexta edición de la semana del Cine Colombiano, pretende acercar y ampliar la cantidad de espectadores del cine nacional, promoviendo las producciones y fortaleciendo la circulación cinematográfica.

Muchas de las películas también serán emitidas de forma gratuita a través de la plataforma de cine latinoamericano: “Retina latina” y del canal Señal Colombia en su franja: ‘En cine nos vemos’.

<iframe src="https://www.youtube.com/embed/rSmLZ69wj_8" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>
