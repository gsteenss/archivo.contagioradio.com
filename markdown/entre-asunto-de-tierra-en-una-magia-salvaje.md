Title: Entre Asunto de Tierra en una Magia Salvaje
Date: 2015-09-14 12:27
Category: Camilo, Opinion
Tags: biodiversidad, camilo de las casas, despojo, Magia Salvaje, Mike Slee, Un asunto de tierras, violencia
Slug: entre-asunto-de-tierra-en-una-magia-salvaje
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/por.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

#### [**Por [Camilo De Las Casas ](https://archivo.contagioradio.com/camilo-de-las-casas/) - [~~@~~CamilodCasas](https://twitter.com/CamilodCasas) 

###### [14  sep 2015] 

La ley y las estrategias verdes son una ilusión frente a asuntos de fondo que se enmascaran o que en realidad no se quieren enfrentar. Partimos de la tragedia y volvemos a la profundización de la misma, por inercia, por ausencia de transformación, por insensibilidad subconciente, porque no se enganchan esos mundos reales con decisiones políticas, y tal vez, porque el arte, se ha vuelto como el deshecho: se consume y se tira, porque no logra conmovernos, más allá de unos instantes.

Un asunto de Tierras de Patricia Ayala, es la  narrativa en el 7mo Arte,  sobre una ley, la de víctimas y restitución publicitada en el mundo como la más avanzada y con una realidad de sin sentidos. Una constatación de la actitud de la mayoría de los políticos ante el despojo, ante la violencia y los derechos de las víctimas. La ley como la eficacia simbólica y retórica; la burocratización como mecanismo para dilatar devolver la tierra a sus legales y legítimos propietarios. Es la observación que constata el discurso institucional de los dos demonios responsables del desplazamiento y el miedo de las victimas sobrevivientes o la recreación de su tragedia en la esperanza. Es la creatividad, la persistencia y hasta la inocencia de los sobrevivientes de la violencia, la mayoría de ellos, de la tercera edad, con una generación pequeña que continuará las exigencias de sus derechos. Es el cuadro del poder sacro santo de la ley, bendecida por la iglesia católica, sin ser esta un sujeto activamente crítico.

Colombia Magia Salvaje, dirigida por Mike Slee, la expresión fílmica de un país, el segundo de mayor biodiversidad en el mundo con cuadros inimaginados de especies en riesgo, de parajes bellos y que nos posibilitan nuestra supervivencia. Nos acerca a una realidad cercana pero al mismo tiempo distante, ese mundo del que somos parte, esas vidas con las que interactuamos pero de las que desconocemos su origen y su relación con nosotros. Son la flora, la fauna, los bosques, los páramos, las aguas  y las más de 40 áreas declaradas de protección especial. Unas imágenes de contraste con una apelación que quedan en el consumo urbano cotidiano con pinceladas sobre la responsabilidad empresarial privada minera, agroindustrial, y las prácticas cotidianas de algunos habitantes rurales en lo que realmente es una situación apocalíptica, un poco distante de la voz de Julio Sánchez Cristo, que tal vez, por la percepción de un manipulador focalizado con algunos de sus entrevistados en la radio de la mañana, que no logra conmover, es posible que sea mi prejuicio.

Ambas producciones son un espejo de lo que hoy somos como proyecto de país. Un país atado a déficits territoriales históricos que no han sido resueltos. Un país de sobrevivientes que son tratados como mendigos; con mentiras, con engaños; un país ambiental perdido y enmarañado con prebendas a multinacionales que hoy prolongan el saqueo que se inició desde la colonia y en que las fuentes de producción de la riqueza para unos pasan por el arrasamiento de las fuentes de vida, agua, páramos, bosques. Un país de ocultamientos, como se refleja en el documental. Una negación del Atrato ¿por qué será? Y mientras tanto en una especie de subasta hay cerca de 400 millones del BID para el pacifico  
¿Qué hay detrás?

Un Asunto de Tierra y Magia Salvaje, dos rostros de nuestra misma tragedia. La violencia y la de una política de derechos que son una retórica para los sobrevivientes, que está desconectada del reconocimiento de los daños ambientales causados y que demuestran la desarticulación de las políticas públicas que dimensionen la transversalidad, de lo que significa la restitución.

Se pueden llenar las salas de cine con ese mundo desconocido de nuestra riqueza ambiental en riesgo, o por la ley de devolución de tierras, sin que logremos comprender, que más allá de asumir una actitud protectora cotidiana o de exigencia a la aplicación de la ley de víctimas, con el manejo de basuras, por ejemplo, o la devolución de la tierra, se requieren transformaciones de mentalidades, de sensibilidades y la gestación de una voluntad política ecosocial. Sensibilidad, mentalidad y gestación que está muy distante de los contenidos y de la aplicación de la pomposa ley de víctimas y de restitución, y de las disposiciones ambientales existentes en la “prosperidad” santista.

Como expresa Saskia  Sassen en Expulsiones: “Hoy nuestras instituciones y nuestros supuestos están cada vez más al servicio del crecimiento económico corporativo. Esa es la nueva lógica sistémica. Suficientes corporaciones han buscado liberarse de toda constricción, incluidas las de interés público local, que interfiera con su búsqueda de lucro. Cualquier cosa o persona, ya sea una ley o un esfuerzo cívico, que dificulte el lucro,corre el riesgo de que la hagan a un lado, que la expulsen”

No ser conscientes de esa dimensión del Asunto de Tierras o de Colombia Magia Salvaje, nos conducirán a caminar hacia lo mismo, en una actitud caritativa y ya.

Lo que implicitamente se encuentra en el Asunto de Tierras, lo claramente oculto en la ley de restitución, es lo mismo que se esconde en la maravillosa publicidad verde, que encubre el factor fundamental de riesgo y de destrucción vigente de las fuentes de vida: el modelo de desarrollo a fin con el mercado privado.

<div class="yj6qo ajU" style="text-align: justify;">

</div>
