Title: Respeto a los derechos humanos es garantía para la Paz
Date: 2016-12-19 16:23
Category: DDHH, Nacional
Tags: Consejería de Derechos Humanos de la presidencia, Derechos Humanos, Reparación a las víctimas, reparación y no repetición
Slug: respeto-los-derechos-humanos-garantia-la-paz
Status: published

###### [Foto: Contagio Radio] 

###### [19 Dic 2016] 

En un país donde no se proteja los derechos humanos y la integridad de sus habitantes, no es posible la construcción de una paz íntegra, estable y duradera, es la premisa que confirma el último informe **Colombia más allá del conflicto armado: Derechos humanos y tránsito a la paz**, elaborado por la Consejería Presidencial para los Derechos Humanos.

El informe conformado por 8 capítulos, pretende mostrar un panorama general de la situación que atraviesa Colombia en materia de derechos humanos, **reparación a las víctimas, niñez dentro del conflicto armado, la desaparición forzada, el papel de las mujeres dentro de la guerra y el tema de la corrupción.**

### **Derechos Humanos** 

El informe revela que para el año 2013 se registraron más de **260.000 víctimas del conflicto, **para las cuales se crearon estructuras anexas como el Sistema Integral de Verdad, Justicia, Reparación y No Repetición y el Sistema Nacional de Derechos Humanos.

Este capítulo resalta la importancia de garantizar el reconocimiento de las víctimas como sujetos de derechos y la responsabilidad del Estado para** que toda la población conozca la verdad plena sobre lo ocurrido durante el conflicto. **Le puede interesar: [100% de violaciones a](https://archivo.contagioradio.com/100-violaciones-derechos-humanos-estan-impunes-colombia/)Derechos Humanos[están impunes en Colombia.](https://archivo.contagioradio.com/100-violaciones-derechos-humanos-estan-impunes-colombia/)

Llama la atención sobre la cifra alarmante de más **de 8 mil víctimas del conflicto armado registradas ante la Unidad de Víctimas, y la tendencia al aumento de la cantidad de población afectada por la guerra.**

Señala también, el desarrollo de la Ley de Víctimas y una nueva política pública que busca lograr el componente de generación de ingresos y de empleo para ayudar a superar la situación de vulnerabilidad de la población desplazada en cuanto** a estabilización socio económica y restablecimiento de sus capacidades laborales.**

### **Los niños en la guerra** 

El documento aporta cifras sobre reclutamiento y desvinculación de menores de edad, y el avance de la Comisión Intersectorial para la **prevención del reclutamiento, utilización y violencia sexual contra niños, niñas y adolescentes.**

Menciona que entre 2010 y octubre de 2016, han sido atendidos a través del **Programa Especializado para la Atención del ICBF un total de 1.909 niños, niñas y adolescentes.**

También el informe, resalta el desarrollo de la **Ley 1098 de 2006, la cual contiene las normas sustanciales y procesales para la protección de los derechos de la niñez y la adolescencia,** así como las “obligaciones de la familia, la comunidad y el Estado frente a su reconocimiento como sujetos de derechos, el deber de prevenir, garantizar, atender y restablecer sus derechos, con el fin de asegurar el desarrollo pleno y armonioso de esta población”.

Además, señala que con la Ley de Víctimas y Restitución de Tierras, **se considera a los niños, niñas y adolescentes víctimas de reclutamiento como sujetos de reparación integral,** quienes pueden acceder a medidas de indemnización, rehabilitación, satisfacción, restitución y garantías de no repetición.

### **Desaparición Forzada** 

En esta materia, el documento devela que **entre 1938 y 2015 se denunciaron 110.833 casos de desaparición, de los cuales 23.441 se reportaron como presunta desapari­ción forzada,** correspondiente el 84% a hombres y el 11% a mujeres.

Incluye también el trabajo de la **Comisión Nacional de Búsqueda de Personas Desaparecidas**, la cual desde la publicación del comunicado 062, en Octubre del 2015 hasta noviembre de 2016, **ha realizado la exhumación de 621 cuerpos y ha entregado otros 65 a sus familiares.**

\[caption id="attachment\_33904" align="alignnone" width="1048"\]![Cartografía Desaparición Forzada](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/12/Desaparición_Forzada.png){.size-full .wp-image-33904 width="1048" height="557"} Cartografía de Desaparición Forzada\[/caption\]

### **Las mujeres en medio del conflicto** 

El informe presta especial atención al tema de violencias basadas en género en el marco del conflicto armado, y revela que los municipios más afectados por este delito son **Buenaventura, Tumaco y Riohacha**. Sin embargo, señala que principalmente las más afectadas son ciudades capitales como **Bogotá, Medellín, Cali y Barranqui­lla.**

En **92 de cada 100 casos de violencia sexual, las víctimas son mujeres, así como lo son 85 de cada 100 víctimas de presunto delito sexual** en escenarios diferentes al conflicto armado y en casos de trata de personas en el marco del conflicto, se registraron 31.777 casos, puntualiza este capítulo del informe.

### **Amenazas y Hostigamientos** 

Las cifras contenidas en el documento, demuestran que entre Enero de 2012 y Noviembre de 2016, los 10 municipios donde se han presentado más amenazas son Medellín con 15.598 casos, Buenaventura con 10.275, San Andrés de Tumaco con 3.703, San Vicente del Caguán con 3.538, Cartagena del Chairá con 3.538, Cali con 2.511, Florencia con 1.190, Puerto Rico con 1.933, Quibdó con 1.911 y Turbo con 1.840 casos.

Por otra parte, el Observatorio del Delito de la DIJIN muestra que entre 2012 y 2015 hubo **un incremento signifi­cativo de amenazas, que llevó el número de registros a 38.007 casos.**

\[caption id="attachment\_33905" align="alignnone" width="940"\]![Cartografía Amenazas](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/12/Amenazas.png){.size-full .wp-image-33905 width="940" height="508"} Cartografía de Amenazas\[/caption\]

### **Corrupción** 

En el último capítulo, que se refiere al tema de la corrupción y su tratamiento en el marco del postconflicto, mencionan la creación de la Secretaría de Transparencia de la Presidencia de la República, que tiene la misión de asesorar y apoyar al Presidente en el diseño de una **Política Integral de Transparencia y Lucha Contra la Corrupción, de la cual hasta el momento han sido pocos los avances** en temas de salud, educación, extractivismo, entre otros.

### **Recomendaciones** 

En la última parte del documento, se hacen algunas sugerencias a las instituciones del Estado y a las organizaciones, **para que sean veedoras y garantes del cumplimiento de los Acuerdos en su fase de Implementación. **

Concluye que la implementación del Acuerdo requiere de la apropiación y la participación de toda la sociedad, aunque las causas del conflicto y sus consecuencias hayan afectado de manera diferente a los territorios y sus habitantes, **“construir una paz estable y duradera ame­rita la participación de todos, pues es lo que permitirá el ejercicio de los derechos en condiciones de igualdad”.**

[Informe Final - Colombia más allá del conflicto armado:](https://www.scribd.com/document/334705387/Informe-Final-Colombia-mas-alla-del-conflicto-armado-Derechos-humanos-y-transito-a-la-paz#from_embed "View Informe Final - Colombia más allá del conflicto armado: Derechos humanos y tránsito a la paz on Scribd")Derechos humanos[y tránsito a la paz](https://www.scribd.com/document/334705387/Informe-Final-Colombia-mas-alla-del-conflicto-armado-Derechos-humanos-y-transito-a-la-paz#from_embed "View Informe Final - Colombia más allá del conflicto armado: Derechos humanos y tránsito a la paz on Scribd") by [Contagioradio](https://es.scribd.com/user/276652802/Contagioradio#from_embed "View Contagioradio's profile on Scribd") on Scribd

<iframe id="doc_58548" class="scribd_iframe_embed" src="https://www.scribd.com/embeds/334705387/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-lhFuaZSxwXRroD6zd8Xm&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="0.7729220222793488"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
