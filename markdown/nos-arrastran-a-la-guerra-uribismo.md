Title: Nos arrastran a la guerra
Date: 2019-07-26 09:06
Author: JOHAN MENDOZA TORRES
Category: Johan Mendoza, Opinion
Tags: corrupción, fanatismo, guerra, uribismo
Slug: nos-arrastran-a-la-guerra-uribismo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/6f6e9d14-congreso-de-colombia.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: RT 

**El descaro del uribismo,** lo ha convertido en un régimen que se burla en la cara de todos los colombianos y colombianas; esto ya no parece un gobierno sino un régimen.

El uribismo, a sabiendas de que los únicos que lo desafiaron alguna vez, hoy se encuentran desmovilizados y afiliados a un partido muerto, convirtió lo de la jugadita en una leguleyada, lo de Odebrecht en una deuda que pagaremos los colombianos, lo de la muerte de personas por razones políticas en una estadística que ofrece reducciones porcentuales para evitar considerar la gravedad del asunto, apoyaron con fidelidad al ladrón Andrés Felipe Arias, y así con cuanta infamia surge… ellos se burlan, porque hoy, tienen la certeza de que nadie se atreve a desafiarlos más que con cosas que les resbalan, como esta columna, como marchas que no sirven, como ciertos vídeos de indolentes youtubers o con esos eventos académicos que no pasan del aplauso.

Cáncer del siglo XXI, mal habida razón de su ideología política autoritaria ¡pro falangista! uribismo paupérrimo intelectualmente pero tan enquistado en nuestra sociedad que hoy ya parece la razón de ser de la derecha en Colombia.

I**ncansable duelo que vivimos con el asesinato de tantas personas**; duelo que no se abre como una flor ni ante los soles más fuertes de Colombia, el día ya parece la noche y la noche se ha vuelto eterna. Así vive Colombia, de la zozobra a la certeza que solo les ofrece a los conformistas una próxima serie televisiva. Ya muchos no buscan el vacío, porque su pesadez los sobrecoge en medio de todos los entierros que han tenido lugar y los que vienen.

El paisaje más bello ha perdido todos sus olores, en medio de la risa nos llegó el llanto, no quisieron verlo aquellos que confiaron ciegamente en las promesas de la marioneta. Duque: hay que admitirlo, hablas mejor que Santos, pero eres igual de mentiroso.

El país sin cabeza, el país descuartizado, volvió a tocar a nuestra puerta. La tierra es nuestra vida y nuestra maldición; espejismo de piedras preciosas que fueron acaparadas por las buenas costumbres de escurrir sangre, ahora nos hablan de capitalismo responsable.

La necedad convirtió la tierra en un tesoro opaco y negro, un tesoro en el fondo de nuestro mar de lágrimas. Lágrimas que se arrojan al final de cada cordillera, lágrimas que descienden por los manglares de nuestros dos mares, lágrimas sobre las que navegan todos aquellos que alguna vez denominamos “nuestros deseos”.

Bicentenario extraño que recordamos en medio de las barbaries y el descaro infinito del régimen uribista, que arroja el pérfido ejemplo de que la ley no sirve, de que el ladrón debe ser venerado, de que los muertos vienen por categorías, de que estudiar no sirve si se tienen un buen padrino político, de que se puede ocultar la fuerza de nuestro trabajo con teorías de papel naranja mientras los recursos públicos terminan en manos de banqueros y multinacionales; siguen y seguirán dando el ejemplo de que mentir paga muy bien, mientras la sociedad conozca mejor la serie de narcos que el barrial que está pisando… ¡bastardos!

Vorágine interminable, porque las barbaries se niegan a desaparecer… Colombia es una vez más el país sin cabeza, en el que se decapita gente desde hace más de 200 años. La desdicha anda por las calles dando tumbos entre la miseria y la indiferencia adinerada. La ironía se alimenta de nuestros cuerpos que de tanto sentir ya no diferencian las sensaciones, pues como los muertos ya no importan, placer y dolor parece que hoy dan lo mismo.

Somos el hacha y el árbol, el agua y el estiércol, la luz y la oscuridad, los carceleros y los libertarios, somos los dueños de nuestra riqueza y de nuestra ruina, estamos condenados a la libertad de matarnos en perfecto orden, estamos condenados a bailar como un youtuber en medio de los entierros.

> **Nos arrastran a una nueva guerra, y los culpables son los que nos hacen jugaditas desde el gobierno**

**Nos arrastran a una nueva guerra, y los culpables son los que nos hacen jugaditas desde el gobierno**, son ellos, que no vengan a inventar pendejadas; nos arrastran a una guerra con su soberbia, con su descaro, con las riquezas que nos roban, con la tierra que no nos quieren devolver, con el país que están saqueando, nos arrastran a la guerra.

Nos arrastran a una guerra, no hay duda, pronto vendrán de nuevo, el desespero por la victoria, la derrota, el alivio y el sollozo. Ya sabemos que 200 años no son nada, si fuimos capaces con España, pero inútiles con nuestra espada.

##### [Leer más columnas de opinión de Johan Mendoza](https://archivo.contagioradio.com/johan-mendoza-torres/) 
