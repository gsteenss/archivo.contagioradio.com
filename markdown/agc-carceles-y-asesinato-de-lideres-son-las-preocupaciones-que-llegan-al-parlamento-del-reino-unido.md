Title: AGC, cárceles y asesinato de líderes son las preocupaciones que  llegan al parlamento del Reino Unido
Date: 2020-05-15 01:09
Author: AdminContagio
Category: Actualidad, El mundo
Tags: Parlamento Británico, paz, Reino Unido
Slug: agc-carceles-y-asesinato-de-lideres-son-las-preocupaciones-que-llegan-al-parlamento-del-reino-unido
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/05/Clive-Efford-parlamentario-del-Reino-Unido-by-@Sly023-e1589370970743.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:heading {"align":"right","level":6,"textColor":"cyan-bluish-gray"} -->

###### Foto: Contexto, @Sly023 {#foto-contexto-sly023 .has-cyan-bluish-gray-color .has-text-color .has-text-align-right}

<!-- /wp:heading -->

<!-- wp:paragraph -->

El pasado lunes 4 de mayo en el parlamento británico, el diputado Clive Efford interrogó a la subministra de asuntos exteriores de Reino Unido, Wendy Morton, sobre asuntos concernientes al papel que está jugando el país respecto a la situación de violencia que se vive en el departamento del Chocó, así como el apoyo al Acuerdo de Paz, específicamente en lo relacionado con la justicia transicional.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### En contexto: La visita a Colombia que permitió formular algunas preocupaciones en el parlamento británico

<!-- /wp:heading -->

<!-- wp:paragraph -->

Entre el 16 y 21 de febrero se llevó a cabo la [cuarta delegación internaciona](https://colombiapeacemonitor.org/2020/02/22/584/)l del Monitoreo de Paz de Justice for Colombia, en la que se revisó el estado de implementación del Acuerdo de Paz. La delegación estuvo compuesta por parlamentarios británicos (entre los que se encontraba Efford) y españoles, así como líderes sindicales del Reino Unido, Irlanda, Italia y Dinamarca.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Durante su estancia en Colombia, la delegación visitó territorios del Chocó y Antioquia, y tuvo la oportunidad de encontrarse con comunidades, organizaciones defensoras de derechos humanos así como instituciones encargadas de desarrollar partes del Acuerdo de Paz, como las que componen el Sistema Integral de Verdad, Justicia, Reparación y No Repetición (SIVJRNR).

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Lo invitamos a consultar: [Comisión parlamentaria monitorea DDHH en Colombia](https://archivo.contagioradio.com/comision-parlamentaria-monitorea-ddhh-en-colombia/).

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Los cuestionamientos sobre la situación humanitaria y de paz en Colombia

<!-- /wp:heading -->

<!-- wp:paragraph -->

Con el conocimiento de la situación en Colombia, el parlamentario laborista Clive Efford preguntó sobre la evaluación por parte del Gobierno Británico respecto a la situación de conflicto que vive el Chocó, considerando especialmente las actividades de las autodenominadas Autodefensas Gaitanistas de Colombia (AGC) y la respuesta a sus acciones por parte de las fuerzas del Estado.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Organizaciones defensoras de DD.HH. como la[Comisión de Justicia y Paz](https://www.justiciaypazcolombia.com/control-paramilitar-de-agc-se-extiende-desde-frontera-con-panama-bajo-y-medio-atrato/)denunciaron, en los primeros meses de este año, el creciente poder territorial de las AGC en el medio y bajo Atrato, afectando a comunidades **como las de Cacarica, Curbaradó, La Larga, Pedeguita Mancilla y Jiguamiandó**. El parlamentario precisamente visitó la zona de biodiversidad de **La Madre Unión**, en el territorio colectivo de **La Larga Tumaradó**.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Ante la pregunta, Morton manifestó la preocupación del Gobierno britanico respecto a las operaciones de estos grupos armados ilegales y su impacto en las comunidades, señaló que la embajada del país en Colombia hacía seguimiento de la situación y recordó que este tema es objeto de discusiones en instancias internacionales como el Consejo de Seguridad de las Naciones Unidas.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El parlamentario también preguntó sobre el apoyo del Reino Unido al sistema de justicia transicional en Colombia. Frente a ello, la subministra reiteró el apoyo al SIVJRNR, celebró la promulgación de la Ley Estatutaria de la Jurisdicción Especial para la Paz (JEP) y concluyó [recordando] que el país ha contribuído con más de 32 millones de dólares a la justicia transicional y las víctimas desde 2016.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/JFColombia/status/1260505731884777473","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/JFColombia/status/1260505731884777473

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:heading {"level":3} -->

### Las otras preocupaciones en el Reino Unido sobre la situación en Colombia

<!-- /wp:heading -->

<!-- wp:paragraph -->

En otra ocasión, también en el parlamento británico, el [diputado](https://twitter.com/JFColombia/status/1258067227334459392) ha cuestionado al gobierno de su país sobre la posición que ha asumido frente a los informes que presenta el Estado colombiano sobre asesinato de excombatientes que se acogieron al Acuerdo de Paz, así como la situación de defensores de derechos humanos.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por otra parte, el sindicato[POA](https://twitter.com/JFColombia/status/1256245579182747648) escribió a la Ministra de Justicia de Colombia, Margarita Cabello, para plantear su preocupación sobre la situación sanitaria a la que se enfrentan las cárceles en Colombia en el marco de la pandemia del Covid-19, pidiendo que se tomen medidas para garantizar la vida de todas las personas que están en los penales.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

También lo invitamos a leer: [Emergencia por COVID llegó al INPEC y tampoco hay protocolo](https://archivo.contagioradio.com/emergencia-por-covid-llego-al-inpec-y-tampoco-hay-protocolo/).

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->
