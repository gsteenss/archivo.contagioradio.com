Title: Carbones del Cerrejón incumple sentencia que protege a comunidades y al Arroyo Bruno
Date: 2020-02-01 12:10
Author: AdminContagio
Category: Ambiente, Judicial
Tags: arroyo Bruno, Carbones del Cerrejón, Cerrejón, La Guajira, Wayuu
Slug: carbones-del-cerrejon-incumple-sentencia-que-protege-a-comunidades-y-al-arroyo-bruno
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/01/Mapa2.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/01/Mapa2-1.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: Achivo

<!-- /wp:paragraph -->

<!-- wp:html -->

<figure class="wp-block-audio">
<audio controls src="https://co.ivoox.com/es/rosa-maria-mateus-integrante-cajar-sobre-desviacion-del_md_47284095_wp_1.mp3">
</audio>
  

<figcaption>
Entrevista &gt; Maria Mateuz | Abogada CAJAR

</figcaption>
</figure>
<!-- /wp:html -->

<!-- wp:paragraph -->

El 31 de enero se llevó a acabo una audiencia en el corregimiento **El Paradero,** ubicado en **La Guajira** entre representantes del Gobierno, el Cerrejon y Comunidades Wayúu, el tema fue el destaponamiento del Arroyo Bruno y las consecuencias ambientales, pero especialmente las mentiras e inclumientos que se han dado a la comunidad.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### El arroyo Bruno

<!-- /wp:heading -->

<!-- wp:paragraph -->

Esta corriente natural de agua nace a 420 metros de altura en la reserva natural de los Montes de Oca, limita con los municipios de Albania y Maicao, y provee agua a comunidades afrodescendientes y a 34 comunidades wayúu; según la organización Censat Agua Viva. (Le puede interesar: <https://colectivodeabogados.org/?Corte-Constitucional-ordena-debatir-con-las-comunidades-el-desvio-del-arroyo> )

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Su caudal lo convierte en uno de los afluentes más importantes de la Guajira, *abasteciendo al Río Rancheria*. A pesar de ello, en 2016 **Corpoguajira** otorgó a la empresa **Carbones del Cerrejón Limited** tres **permisos para la desviación del Arroyo Bruno**, procedimiento que permitió ampliar la frontera extractiva a la empresa. (Le puede interesar: <https://archivo.contagioradio.com/la-desviacion-del-arroyo-bruno-llamemos-las-cosas-por-su-nombre/> )  

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### La desviación del Arroyo y sus impacto

<!-- /wp:heading -->

<!-- wp:paragraph -->

Los daños ambientales y sociales que ha generado la desviación se han podido constatar de varias formas según la abogada Rosa Maria Mateus integrante del Colectivo de Abogados José Alvear Restrepo (Cajar), a través de imágenes satélites de Google Earth evidenciaron como desde 2017 se ha presentado una considerable disminución del bosque trópical.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/AlirioUribeMuoz/status/1223242005964304384","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/AlirioUribeMuoz/status/1223242005964304384

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

De manera similar la Corte Constitucional realizó una inspección en el terreno haciendo un recorrido por el cauce natural así como por el artificial en el que corroboraron lo que se había denunciado por las comunidades, de allí la decisión **de suspender la desviación del arroyo Bruno en La Guajira,** a través de la sentencia 698 de 2017.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

La orden se dio en protección de los derechos fundamentales al agua, a la seguridad alimentaria y a la salud, ante la amenaza de vulneración ocasionada por el proyecto de modificación parcial del cauce.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

La abogada tambien denunció la *"falta de ética y honestidad* *por parte de la empresa"*, reflejada en la comunicación externa que pone en contraste la palabra de la empresa contra la de la comunidad. Ejemplo de esto son los vídeos que ha publicado Carbones del Cerrejón evidenciando el arroyo artificial y sus logros, *“en los vídeos no se sabe de qué parte del arroyo están hablando porque las imágenes no son para nada cercanas a la realidad que se vive allí”*.  (Le puede interesar: <https://archivo.contagioradio.com/necesario-actuar-salvar-arroyo-bruno-guajira/> )

<!-- /wp:paragraph -->

<!-- wp:image {"id":80131,"sizeSlug":"large"} -->

<figure class="wp-block-image size-large">
![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/01/Mapa2-1-1024x614.jpg){.wp-image-80131}  

<figcaption>
Foto: [Censat Agua Viva](http://censat.org/)

</figcaption>
</figure>
<!-- /wp:image -->

<!-- wp:paragraph -->

La realidad según Mateus es que *"hay una cantidad de comunidades sin agua que durante años se han sostenido con el  recurso brindado por el arroyo, **las comunidades señalan que no hay pescados, que hay animales muertos, y que el agua se evapora"***, además la perdida del agua en el cause artificial se presenta porque, *"al cauce natural lo rodeaba una gran cantidad de árboles que lo protegían del sol de La Guajira, éste no tiene nada, lo que lo deja más expuesto a las altas temperaturas". *

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### "No son acciones judiciales sino éticas las que se necesitan "

<!-- /wp:heading -->

<!-- wp:paragraph -->

Finalmente, la invitación que hacen las comunidades **es a recuperar los sentidos**, ***"poder ver, oler sentir y hacer visible cómo ocurre esta tragedia en plena crisis climática en un departamento semidesértico"*** agrego la jurista.

<!-- /wp:paragraph -->

<!-- wp:quote -->

> *"Vengan, conozcan y solidarisense con ellos, evidenciando que es un tema ético, el cual jurídicamente ya se pudo comprobar con más de 16 fallos en contra de Carbones Cerrejón y que han sido ignorados por la empresa"*
>
> <cite> Rosa Maria Mateus - Abogada Cajar </cite>

<!-- /wp:quote -->

<!-- wp:core-embed/youtube {"url":"https://youtu.be/y5i8cq0D7ws","type":"video","providerNameSlug":"youtube","className":"wp-embed-aspect-16-9 wp-has-aspect-ratio"} -->

<figure class="wp-block-embed-youtube wp-block-embed is-type-video is-provider-youtube wp-embed-aspect-16-9 wp-has-aspect-ratio">
<div class="wp-block-embed__wrapper">

https://youtu.be/y5i8cq0D7ws

</div>

<figcaption>
Por el Arroyo Bruno Resistencia/Censat

</figcaption>
</figure>
<!-- /wp:core-embed/youtube -->

<!-- wp:paragraph -->

Ante ello la pregunta que surge en la comunidad es *“¿qué más quieren?”*, cuando han presentado todos los argumentos y evidencias para generar una acción concreta, *“La Guajira ha sido la zona de sacrificio de este país, no solo por la explotación de carbón, sino de gas, energía eólica y miles de cosas; hasta las empresas de gaseosas vinieron a experimentar sus productos con los niños, por eso los acompañamos para que se respete esta cultura milenaria que tenemos* *allí aún"*, concluyó la abogada.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

A pesar de que Cerrejón estima poder seguir explotando el carbón en el tiempo que resta, la Organización de las Naciones Unidas exige acciones prontas y ha generado campañas para que los inversores internacionales se sumen al proceso de descarbonización de la economía, alentando la desinversión en combustibles fósiles. La coyuntura mundial nos llama a frenar la explotación de carbón y, en esa medida, se cuestiona la viabilidad de desviar los arroyos, pues se sabe que a futuro, en un escenario de cambio climático agudizado, será más importante el agua que el mineral negro.

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
