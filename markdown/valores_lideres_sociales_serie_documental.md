Title: Valores de los líderes sociales de Colombia son retratados en una serie documental
Date: 2018-04-02 17:00
Category: DDHH, Nacional
Tags: defensores de ddhh, lideres sociales, Somos defensores
Slug: valores_lideres_sociales_serie_documental
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/04/somos-defensores.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:] 

###### [2 Abr 2018] 

[La organización Somos defensores que recoge las cifras de ataques en contra de defensores y defensoras de DDHH en Colombia, lanza esta semana **la serie documental “Positiva”** que resalta los valores y el trabajo constante de los líderes sociales a pesar de la adversidad y los contextos en los que están envueltos.]

En la serie se retratan c**inco historias de defensores de derechos humanos de Quibdó, el Urabá, Amazonas, Aracataca y el Páramo de Sumapaz,** en las que se evidencia que a pesar de las constantes amenazas a su vida y los contextos complejos en medio de los que viven, siguen trabajando en la búsqueda de un país diferente.

"Buscamos la cara positiva siempre recordando los contextos difíciles", dice Carlos Guevara,  coordinador del Programa Somos Defensores. El trabajo se realizó de la mano de 'David Alba Producciones'. En los cinco documentales, se representan los valores de la alegría, el arraigo, la persistencia, el espíritu y la esperanza con la que a diario viven los líderes sociales en todo el país.

**¿Dónde se pueden ver los documentales?**

"Tratamos de identificar territorios donde no se hubiese hablado de ciertos temas y que se cruzaran con complejidades del contexto", explica Guevara, quien agrega, que en cada documental se buscó un valor agregado o una voz distinta que pudiera dar un ejemplo y una voz de aliento a todo los líderes sociales de Colombia.

**La serie de documentales será publicada esta primera semana de abril, de lunes a viernes a las 9:00 am en el canal de YouTube del Programa Somos Defensores.** "Lo importante es que la gente en general, se de cuenta que los líderes sociales no son solo víctimas, son actores determinantes en la construcción de democracia y una paz estable y duradera del país", concluye Carlos Guevara.

https://www.youtube.com/watch?v=0Zn24ATzmOA  
<iframe id="audio_25009146" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_25009146_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)

 
