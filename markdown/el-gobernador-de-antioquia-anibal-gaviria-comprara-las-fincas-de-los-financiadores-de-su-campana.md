Title: ¿El Gobernador de Antioquia, comprará las fincas de los financiadores de su campaña?
Date: 2020-04-06 15:14
Author: Carlos Montoya
Category: Opinion
Tags: Agrochiguiros, Agropecuaria grupo 20, Aníbal Gaviria, gobernador de antioquia, Hacienda Velaba
Slug: el-gobernador-de-antioquia-anibal-gaviria-comprara-las-fincas-de-los-financiadores-de-su-campana
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/04/Captura-de-pantalla-2020-04-06-a-las-12.54.28.png" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/04/Captura-de-pantalla-2020-04-06-a-las-12.56.33.png" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/04/Aníbal-Gaviria-posesión.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

**Por: Carlos Montoya**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En la columna anterior había presentado que el 33% de la financiación de la campaña de Aníbal Gaviria a la Gobernación de Antioquia venía de [familiares, personas y empresas que se beneficiaban con la construcción de Puerto Antioquia.](https://archivo.contagioradio.com/anibal-gaviria-el-gobernador-de-antioquia-o-el-mandatario-que-tiene-la-democracia-empenada/)

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Este negocio no terminaría solo con esa primera acción, ahora resulta que los financiadores también serán beneficiados con la compra de sus predios, probablemente por la Gobernación de Antioquia, como lo demuestran documentos públicos firmados por el exgobernador Luis Pérez Gutiérrez. 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Los documentos solicitan la inscripción de *“declaratoria de utilidad pública”* de 74 bienes inmuebles para la construcción y operación de Puerto Antioquia. Esto significa la compra de los predios o su aportación a la sociedad como lo define el artículo 16 de la Ley 1 de 1991 que citan en los mismos oficios públicos.

<!-- /wp:paragraph -->

<!-- wp:quote -->

> *ARTÍCULO 16. Expropiación y aporte de terrenos aledaños. ****Se declara de interés público la adquisición de los predios de propiedad privada necesarios para establecer puertos****. Si la sociedad a la que se otorga una* ***concesión portuaria no es dueña de tales predios, deberá iniciar conversaciones con las personas que aparezcan como titulares de derechos reales sobre ellos, para conseguir que voluntariamente los vendan o aporten a la sociedad****. Transcurridos treinta días a partir del momento en el que se comunicó a los titulares de derechos reales la intención de negociar con ellos, si la negociación no ha sido posible, se considerará fracasada y la Nación, a través del Superintendente General de Puertos, o cualquier entidad pública capacitada legalmente para ser socia de una sociedad portuaria, podrá expedir un acto administrativo y ordenar la expropiación.*
>
> <cite>Artículo 16</cite>

<!-- /wp:quote -->

<!-- wp:paragraph -->

Al cruzar la información entre los aportantes a la campaña, los predios declarados en utilidad pública y sus dueños, resulta que el 27% de estos son propiedad de familiares, personas y empresas que financiaron a Aníbal Gaviria durante su campaña electoral a la gobernación de Antioquia.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En total, son 20 de los 74 predios declarados en utilidad pública agrupados en siete empresas: Agrchiguiros, C.I BANACOL, Agrícola El Retiro, Agrícola Cerdeña, Hacienda Velaba, Agropecuaria Grupo 20 y Agrícola Sara Palma. 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El siguiente organigrama muestra cuantos predios le comprarían a cada empresa:

<!-- /wp:paragraph -->

<!-- wp:image {"id":82836,"sizeSlug":"large"} -->

<figure class="wp-block-image size-large">
![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/04/Captura-de-pantalla-2020-04-06-a-las-12.54.28.png){.wp-image-82836}

</figure>
<!-- /wp:image -->

<!-- wp:paragraph -->

Ahora, la relación de estos 20 predios declarados en utilidad pública y los aportantes a la campaña de la Gobernación de Aníbal Gaviria, se presenta por medio de las juntas directivas de las empresas. Varios de sus integrantes participan en distintas compañías dando la sensación de ser los mismos con las mismas, pero desde distinta figura jurídica. 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En total también son 20 personas que integran entre dos o hasta seis juntas directivas permitiendo pasar de financiador a beneficiado por el hecho de estar en la estructura organizativa de otra empresa. Presentamos los casos más cuestionables:  

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

**Familia Gaviria Correa**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Los hermanos Jorge Julián Gaviria y Irene Gaviria participan en otras juntas directivas relacionadas con el proyecto Puerto Antioquia y la financiación de la campaña de su hermano:

<!-- /wp:paragraph -->

<!-- wp:list -->

-   **Jorge Julián Gaviria**

<!-- /wp:list -->

<!-- wp:paragraph -->

Participa en seis juntas directivas de empresas: Agrícola Sara Palma, Promotora Bananera - PROBAN-, Nueva Plantación, Uberaba, Agropecuaria Grupo 20 y la Asociación de Bananeros de Colombia –AUGURA-   

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Entre las que financiaron a Aníbal Gaviria están AUGURA y PROBAN; y entre las beneficiadas con la posible compra de predios están Agrícola Sara Palma y Agropecuaria Grupo 20.   

<!-- /wp:paragraph -->

<!-- wp:list -->

-   **Irene Gaviria Correa**

<!-- /wp:list -->

<!-- wp:paragraph -->

Participa en 3 juntas directivas. Entre las que financiaron a Aníbal Gaviria está PROBAN y las beneficiadas directamente con la posible compra de predios están la Agropecuaria Grupo 20 y UNIBAN, la cual también controla a la Agrícola Sara Palma.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Hay que recordar que familia Gaviria Correa financió el 29% de la campaña de Aníbal Gaviria y por este motivo serían de las grandes beneficiadas porque les comprarían tres propiedades en la zona de influencia de Puerto Antioquia. Son las fincas: las Dunas, el Oasis y Guadalupe controlados por la empresa familiar, la Agropecuaria Grupo 20 S.A. Estos tres predios tienen 176 hectáreas y un valor catastral de alrededor de 934 millones de pesos. 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Una persona que sabe de avalúos por la zona, afirmaba que los predios tendrían un valor comercial 60 veces su valor catastral. Es decir, que las tres fincas de las familias Gaviria Correa podrían ser compradas por 56 mil millones de pesos.  

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

**Juan Esteban Álvarez Bermúdez**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Juan Estaban es el representante legal de la empresa de la familia Gaviria Correa- Agropecuaria Grupo 20-. Hombre de confianza de la familia, quien el mismo Aníbal lo considera como “hermano de la vida” y según esta investigación, hace parte de cinco juntas directivas de empresas bananeras (Promotora Bananera –PROBAN-, Agropecuaria Sara Palma, Nueva Plantación S.A., Agrícola Uberaba S.A y AUGURA) y representante legal en dos empresas (UNIBAN y Agropecuaria Grupo 20). Varias de ellas están relacionadas con Puerto Antioquia como el caso de UNIBAN y la financiación de la campaña de Aníbal Gaviria, AUGURA y la Promotora Bananera –PROBAN-

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Esto permite interpretar, que Juan Estaban Álvarez es el representante de la familia Gaviria Correa en varios espacios empresariales y políticos, ya que los Certificados de Cámara de Comercio de las empresas mencionadas y artículos de prensa, muestran que fue asesor durante la primera Gobernación de Aníbal, pero también fue considerado un “Rasputín” durante el periodo en la Alcaldía de Medellín.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

**Jaime Enríquez Gallo **

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Jaime Enríquez Gallo es integrante de las juntas directivas de varias empresas, entre ellas, UNIBAN, socia del proyecto Puerto Antioquia, pero también de AUGURA y PROBAN, ambas aportantes a la campaña del nuevo mandatario departamental.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Adicionalmente, el pasado 5 de diciembre del 2019, antes de posesionarse como nuevo mandatario departamental, Aníbal Gaviria se reunió con empresarios involucrados en el proyecto Puerto Antioquia con el fin de “*tratar los temas del sistema portuario de Urabá…”*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Uno de las personas que participó del encuentro fue Jaime Enríquez Gallo y para esa fecha él y Aníbal Gaviria, debían estar enterados de la declaración de utilidad pública de varios de sus predios ya que el Decreto Departamental firmado por exgobernador Luis Pérez Gutiérrez tenía fecha del 20 de septiembre del 2019.

<!-- /wp:paragraph -->

<!-- wp:image {"id":82837,"sizeSlug":"large"} -->

<figure class="wp-block-image size-large">
![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/04/Captura-de-pantalla-2020-04-06-a-las-12.56.33.png){.wp-image-82837}  

<figcaption>
  
En la foto están: el nuevo Gobernador de Antioquia Aníbal Gaviria, Daniel Quintero, el Alcalde de Medellín y el empresario bananero Enrique Gallo integrante de las juntas directivas de UNIBAN (socia de Puerto Antioquia) y de AUGURA y Promotora Bananera –PROBAN- (Empresas que aportaron dinero a la campaña de Aníbal Gaviria). Foto 5 de diciembre de 2019

</figcaption>
</figure>
<!-- /wp:image -->

<!-- wp:paragraph -->

**Víctor Manuel Henríquez Restrepo**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Víctor Manuel es el presidente de la Comercializadora Internacional de BANACOL y de la Asociación de Bananeros de Colombia –AUGURA-. Esta empresa posiblemente le compraran 9 predios. Cinco directos a la comercializadora y otros cuatro de la Agrícola El Retiro, empresa que controla según la Cámara de Comercio.  

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

**Sandra María Estrada Yepes**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Sandra María hace parte de seis juntas directivas Fundación BANAFRUT, Otraparte, Hacienda Velaba, Agrícola Cerdeña, C.I BANAFRUT y la Asociación de Bananeros de Colombia –AUGURA-.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Ella sería beneficiada por medios de las empresas Hacienda Velaba y Agrícola Cerdeña.  

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por estos hechos, el Gobernador Aníbal Gaviria y el marco de ser transparente con la ciudadanía, debería mostrar a la opinión pública todos los detalles del proyecto Puerto Antioquia, declararse impedido para seguir gestionándolo e invitar a la sociedad civil a conformar una veeduría especial de seguimiento para su implementación.

<!-- /wp:paragraph -->

<!-- wp:table {"hasFixedLayout":true,"className":"is-style-stripes"} -->

<figure class="wp-block-table is-style-stripes">
  -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- -------------------------------
  **Nombre del predio**                                                                                                                                                                                    **Dirección**
  Agrícola Cerdeña S.A                                                                                                                                                                                     Rio Grande
  AGROCHIGUIROS S.A                                                                                                                                                                                        Indira
  AGROCHIGUIROS S.A                                                                                                                                                                                        Los Cámbulos
  AGROCHIGUIROS S.A                                                                                                                                                                                        Las muchachas
  Agropecuaria Santa Ana LTDA                                                                                                                                                                              Santa Anita
  Agropecuaria Santa Ana LTDA                                                                                                                                                                              Santa Ana
  Agropecuaria Yaconda LTDA                                                                                                                                                                                Las Dunas
  Comercial Punta Arenas S.A                                                                                                                                                                               Bodegas
  Comercial Punta Arenas S.A                                                                                                                                                                               Costa Rica
  Cultivos Riogrande S.A                                                                                                                                                                                   Las Margaritas
  Cultivos Santa Cruz S.A                                                                                                                                                                                  Providencia
  El Edén LTDA Agrícola                                                                                                                                                                                    La Escupa
  Grupo 20 S.A Agropecuaria                                                                                                                                                                                Guadalupe
  Hacienda Velaba Morris y CIA.                                                                                                                                                                            La Esperanza
  Hacienda Velaba y CIA.                                                                                                                                                                                   Velaba \#2
  Inversiones Mapana LTDA                                                                                                                                                                                  Mapana
  Inversiones Ucrania LTDA                                                                                                                                                                                 Ucrania
  La Nación                                                                                                                                                                                                Baldíos Nacionales
  Luis Arcesio Gómez Montoya                                                                                                                                                                               La Esperanza
  Teodoro Solorzano González                                                                                                                                                                               Buena vida
  Alicia Acevedo Montoya                                                                                                                                                                                   El encanto
  Libia Roso Rojas Arrieta                                                                                                                                                                                 La mejor esquina de Urabá
  Puerto Bahía Colombia de Urabá S.A                                                                                                                                                                       Futuro Nuevo Puerto Antioquia
  Jesús Avirama                                                                                                                                                                                            Puerto
  Michael Correa Henríquez                                                                                                                                                                                 La Cuña
  Inversiones Sorzano S.A.S                                                                                                                                                                                La Islita
  John Alexander Espitia                                                                                                                                                                                   Caserío el canal
  Puerto Bahía Colombia de Urabá S.A                                                                                                                                                                       Vallan viendo
  Diego Javier Silva González                                                                                                                                                                              Vayan viendo
  Inversiones AICAIR S.A                                                                                                                                                                                   Las Camelias
  Inversiones AICAIR S.A                                                                                                                                                                                   Las Camelias \#2
  Inversiones AICAIR S.A                                                                                                                                                                                   Santa Marta
  Comercializadora V.A S.A.S                                                                                                                                                                               La Liliana Morelo
  C.I BANACOL S.A                                                                                                                                                                                          Finca la Fortuna
  C.I BANACOL S.A                                                                                                                                                                                          Carretera
  C.I BANACOL S.A                                                                                                                                                                                          Carretera
  C.I BANACOL S.A                                                                                                                                                                                          Lote de terreno
  Vidal Granados                                                                                                                                                                                           Finca
  C.I BANACOL S.A                                                                                                                                                                                          La Suerte
  Agrícola el Retiro                                                                                                                                                                                       Sonsoles Limitada
  Limitada Agropecuaria Bambu                                                                                                                                                                              Bambu
  S/I                                                                                                                                                                                                      S/I
  Evangelista Muñoz Martínez                                                                                                                                                                               El Rosal
  Luis Arcesio Gómez Montoya                                                                                                                                                                               La Malagueña
  Daniel Jorge Vahos García                                                                                                                                                                                Genova
  Jorge Enrique Pulido                                                                                                                                                                                     Sin dirección
  Juana Evangelista                                                                                                                                                                                        La Fortuna
  Luis Arcesio Gómez Montoya                                                                                                                                                                               Pamplonita
  Luis Arcesio Gómez Montoya                                                                                                                                                                               Pamplona \#1
  Cultivos Rancho Alegre S.A                                                                                                                                                                               María Consuelo
  Promotora Plantaciones el Darién S.A                                                                                                                                                                     La Unión LT\#1
  Agrícola El Retiro S.A                                                                                                                                                                                   Candas
  Agrícola El Retiro S.A                                                                                                                                                                                   San Jacinto
  CIBELES LTDA. FINCA                                                                                                                                                                                      Montecarlo LT 2
  Agrícola El Retiro S.A                                                                                                                                                                                   Villa Alicia 2
  S.A C.I Promotora Bananera                                                                                                                                                                               San Ramón
  Yerbazal LTDA. Agropecuaria                                                                                                                                                                              Sin dirección
  Agrícola Sara Palma                                                                                                                                                                                      Los Cativos
  Agropecuaria Yoconda LTDA.                                                                                                                                                                               Los Oasis
  Aida Isabel Lara  de García - Cruz María Lara Julio                                                                                                                                                      La Necesidad
  Hacienda Velaba LTDA                                                                                                                                                                                     Alcala
  Hernando Manuel Hernández Ramos                                                                                                                                                                          Sin dirección
  Inversiones Cruz Cardona y CIA S.EN.C                                                                                                                                                                    Ana Gladis
  José Arnulfo Manco López                                                                                                                                                                                 Lote de terreno
  José Arnulfo Manco López                                                                                                                                                                                 El Barbasco
  José Arnulfo Manco López                                                                                                                                                                                 Lote la IUNION
  Luis Arcesio Gómez Montoya                                                                                                                                                                               Pamplona
  Luis Arcesio Gómez Montoya                                                                                                                                                                               La Pamplonesa
  Luz Elena Gómez Echeverry - Bethy Gómez Echeverry -  Luis Enrique Gómez Gómez - Pablo Emilio Gómez Montoya - Carlos Arturo Gómez Echeverry - Blanca Ligia Gómez Echeverry - Lucia Echeverri de Gómez -   El Recreo
  Marcela Eugenia Morales                                                                                                                                                                                  Altazor
  Pablo Emilio Gómez Montoya                                                                                                                                                                               Las Dacotas
  Sara Cruz Pérez Martínez                                                                                                                                                                                 Las Acasias
  Teodoro Solorzano González                                                                                                                                                                               Miriam Amparo
  Tilly Wunderlich Escobar                                                                                                                                                                                 Villa Aida
  -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- -------------------------------

</figure>
<!-- /wp:table -->

<!-- wp:paragraph {"fontSize":"small"} -->

Fuente: Construcción propia con base la información del Decreto Departamental del 20 de septiembre del 2019 y el Oficio del 18 de diciembre del mismo año del director de asunto legales de la Gobernación de Antioquia.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

  

<!-- /wp:paragraph -->
