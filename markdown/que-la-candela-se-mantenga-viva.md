Title: ¡Que la candela se mantenga viva!
Date: 2019-12-06 16:08
Author: CtgAdm
Category: Nacional, Paro Nacional, Uncategorized
Tags: Cali, medios libres cali, Paro Nacional
Slug: que-la-candela-se-mantenga-viva
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/WhatsApp-Image-2019-12-06-at-15.04.56-2.jpeg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/WhatsApp-Image-2019-12-06-at-15.04.56-1.jpeg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/WhatsApp-Image-2019-12-06-at-15.04.56.jpeg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/WhatsApp-Image-2019-12-06-at-15.04.55-2.jpeg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/WhatsApp-Image-2019-12-06-at-15.04.55-1.jpeg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/WhatsApp-Image-2019-12-06-at-15.04.55.jpeg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/WhatsApp-Image-2019-12-06-at-15.04.54-2.jpeg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/WhatsApp-Image-2019-12-06-at-15.04.54-1.jpeg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/WhatsApp-Image-2019-12-06-at-15.04.54.jpeg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/WhatsApp-Image-2019-12-06-at-15.04.55-3.jpeg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/la-candela-viva-medios-libres-cali.png" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### QUE LA CANDELA SE MANTENGA VIVA

##### Una infografía y nota de Medios Libres Cali

¡Nadie dijo que cambiar un país sería fácil! Muchas personas empiezan a sentirse cansadxs o con incertidumbre en torno a lo que está por venir, sin embargo, aquí seguimos convencidxs de que la calle es el escenario de encuentro, reconocimiento y exigencia social. 

Proponemos algunas ideas que nos pueden ayudar a cuidarnos y a mantener la candela viva.

##### [Únete a Medios Libres Cali en telegram](https://t.me/medioslibrescali)

<figure>
![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/WhatsApp-Image-2019-12-06-at-15.04.54.jpeg){width="1000" height="1000" sizes="(max-width: 1000px) 100vw, 1000px" srcset="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/WhatsApp-Image-2019-12-06-at-15.04.54.jpeg 1000w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/WhatsApp-Image-2019-12-06-at-15.04.54-150x150.jpeg 150w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/WhatsApp-Image-2019-12-06-at-15.04.54-300x300.jpeg 300w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/WhatsApp-Image-2019-12-06-at-15.04.54-768x768.jpeg 768w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/WhatsApp-Image-2019-12-06-at-15.04.54-370x370.jpeg 370w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/WhatsApp-Image-2019-12-06-at-15.04.54-512x512.jpeg 512w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/WhatsApp-Image-2019-12-06-at-15.04.54-310x310.jpeg 310w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/WhatsApp-Image-2019-12-06-at-15.04.54-230x230.jpeg 230w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/WhatsApp-Image-2019-12-06-at-15.04.54-360x360.jpeg 360w"}

</figure>
Para resistir hasta que el sol se apague

<figure>
![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/WhatsApp-Image-2019-12-06-at-15.04.54-1.jpeg){width="1000" height="1000" sizes="(max-width: 1000px) 100vw, 1000px" srcset="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/WhatsApp-Image-2019-12-06-at-15.04.54-1.jpeg 1000w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/WhatsApp-Image-2019-12-06-at-15.04.54-1-150x150.jpeg 150w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/WhatsApp-Image-2019-12-06-at-15.04.54-1-300x300.jpeg 300w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/WhatsApp-Image-2019-12-06-at-15.04.54-1-768x768.jpeg 768w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/WhatsApp-Image-2019-12-06-at-15.04.54-1-370x370.jpeg 370w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/WhatsApp-Image-2019-12-06-at-15.04.54-1-512x512.jpeg 512w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/WhatsApp-Image-2019-12-06-at-15.04.54-1-310x310.jpeg 310w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/WhatsApp-Image-2019-12-06-at-15.04.54-1-230x230.jpeg 230w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/WhatsApp-Image-2019-12-06-at-15.04.54-1-360x360.jpeg 360w"}

</figure>
mide tus fuerzas

<figure>
![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/WhatsApp-Image-2019-12-06-at-15.04.54-2.jpeg){width="1000" height="1000" sizes="(max-width: 1000px) 100vw, 1000px" srcset="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/WhatsApp-Image-2019-12-06-at-15.04.54-2.jpeg 1000w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/WhatsApp-Image-2019-12-06-at-15.04.54-2-150x150.jpeg 150w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/WhatsApp-Image-2019-12-06-at-15.04.54-2-300x300.jpeg 300w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/WhatsApp-Image-2019-12-06-at-15.04.54-2-768x768.jpeg 768w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/WhatsApp-Image-2019-12-06-at-15.04.54-2-370x370.jpeg 370w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/WhatsApp-Image-2019-12-06-at-15.04.54-2-512x512.jpeg 512w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/WhatsApp-Image-2019-12-06-at-15.04.54-2-310x310.jpeg 310w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/WhatsApp-Image-2019-12-06-at-15.04.54-2-230x230.jpeg 230w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/WhatsApp-Image-2019-12-06-at-15.04.54-2-360x360.jpeg 360w"}

</figure>
Estrategia de relevos

<figure>
![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/WhatsApp-Image-2019-12-06-at-15.04.55-3.jpeg){width="1000" height="1000" sizes="(max-width: 1000px) 100vw, 1000px" srcset="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/WhatsApp-Image-2019-12-06-at-15.04.55-3.jpeg 1000w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/WhatsApp-Image-2019-12-06-at-15.04.55-3-150x150.jpeg 150w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/WhatsApp-Image-2019-12-06-at-15.04.55-3-300x300.jpeg 300w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/WhatsApp-Image-2019-12-06-at-15.04.55-3-768x768.jpeg 768w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/WhatsApp-Image-2019-12-06-at-15.04.55-3-370x370.jpeg 370w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/WhatsApp-Image-2019-12-06-at-15.04.55-3-512x512.jpeg 512w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/WhatsApp-Image-2019-12-06-at-15.04.55-3-310x310.jpeg 310w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/WhatsApp-Image-2019-12-06-at-15.04.55-3-230x230.jpeg 230w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/WhatsApp-Image-2019-12-06-at-15.04.55-3-360x360.jpeg 360w"}

</figure>
alimento para el cuerpo

<figure>
![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/WhatsApp-Image-2019-12-06-at-15.04.55-2.jpeg){width="1000" height="1000" sizes="(max-width: 1000px) 100vw, 1000px" srcset="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/WhatsApp-Image-2019-12-06-at-15.04.55-2.jpeg 1000w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/WhatsApp-Image-2019-12-06-at-15.04.55-2-150x150.jpeg 150w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/WhatsApp-Image-2019-12-06-at-15.04.55-2-300x300.jpeg 300w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/WhatsApp-Image-2019-12-06-at-15.04.55-2-768x768.jpeg 768w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/WhatsApp-Image-2019-12-06-at-15.04.55-2-370x370.jpeg 370w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/WhatsApp-Image-2019-12-06-at-15.04.55-2-512x512.jpeg 512w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/WhatsApp-Image-2019-12-06-at-15.04.55-2-310x310.jpeg 310w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/WhatsApp-Image-2019-12-06-at-15.04.55-2-230x230.jpeg 230w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/WhatsApp-Image-2019-12-06-at-15.04.55-2-360x360.jpeg 360w"}

</figure>
algunas herramientas

<figure>
![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/WhatsApp-Image-2019-12-06-at-15.04.56.jpeg){width="1000" height="1000" sizes="(max-width: 1000px) 100vw, 1000px" srcset="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/WhatsApp-Image-2019-12-06-at-15.04.56.jpeg 1000w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/WhatsApp-Image-2019-12-06-at-15.04.56-150x150.jpeg 150w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/WhatsApp-Image-2019-12-06-at-15.04.56-300x300.jpeg 300w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/WhatsApp-Image-2019-12-06-at-15.04.56-768x768.jpeg 768w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/WhatsApp-Image-2019-12-06-at-15.04.56-370x370.jpeg 370w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/WhatsApp-Image-2019-12-06-at-15.04.56-512x512.jpeg 512w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/WhatsApp-Image-2019-12-06-at-15.04.56-310x310.jpeg 310w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/WhatsApp-Image-2019-12-06-at-15.04.56-230x230.jpeg 230w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/WhatsApp-Image-2019-12-06-at-15.04.56-360x360.jpeg 360w"}

</figure>
¿cómo nos informamos?

<figure>
![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/WhatsApp-Image-2019-12-06-at-15.04.56-1.jpeg){width="1000" height="1000" sizes="(max-width: 1000px) 100vw, 1000px" srcset="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/WhatsApp-Image-2019-12-06-at-15.04.56-1.jpeg 1000w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/WhatsApp-Image-2019-12-06-at-15.04.56-1-150x150.jpeg 150w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/WhatsApp-Image-2019-12-06-at-15.04.56-1-300x300.jpeg 300w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/WhatsApp-Image-2019-12-06-at-15.04.56-1-768x768.jpeg 768w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/WhatsApp-Image-2019-12-06-at-15.04.56-1-370x370.jpeg 370w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/WhatsApp-Image-2019-12-06-at-15.04.56-1-512x512.jpeg 512w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/WhatsApp-Image-2019-12-06-at-15.04.56-1-310x310.jpeg 310w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/WhatsApp-Image-2019-12-06-at-15.04.56-1-230x230.jpeg 230w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/WhatsApp-Image-2019-12-06-at-15.04.56-1-360x360.jpeg 360w"}

</figure>
consejos a la hora de hacer la foto

<figure>
![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/WhatsApp-Image-2019-12-06-at-15.04.56-2.jpeg){width="1000" height="1000" sizes="(max-width: 1000px) 100vw, 1000px" srcset="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/WhatsApp-Image-2019-12-06-at-15.04.56-2.jpeg 1000w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/WhatsApp-Image-2019-12-06-at-15.04.56-2-150x150.jpeg 150w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/WhatsApp-Image-2019-12-06-at-15.04.56-2-300x300.jpeg 300w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/WhatsApp-Image-2019-12-06-at-15.04.56-2-768x768.jpeg 768w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/WhatsApp-Image-2019-12-06-at-15.04.56-2-370x370.jpeg 370w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/WhatsApp-Image-2019-12-06-at-15.04.56-2-512x512.jpeg 512w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/WhatsApp-Image-2019-12-06-at-15.04.56-2-310x310.jpeg 310w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/WhatsApp-Image-2019-12-06-at-15.04.56-2-230x230.jpeg 230w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/WhatsApp-Image-2019-12-06-at-15.04.56-2-360x360.jpeg 360w"}

</figure>
el miedo cambió
