Title: Aprueban consulta popular sobre actividades petroleras en Pasca, Cundinamarca
Date: 2017-05-23 17:00
Category: Ambiente, Voces de la Tierra
Tags: consulta popular, Sumapaz
Slug: aprueban_consulta_popular_pasca_petroleo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/05/pasca.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Blogspot Pasca 

###### [23 May 2017] 

[En el municipio de Pasca, en la región del Sumapaz, la comunidad también prepara la posibilidad de que a través de las urnas la población pueda decidir sobre proyectos petroleros en su territorio, frente a lo cual el **Tribunal Administrativo de Cundinamarca** **ya le dio el aval de constitucionalidad a dicha inicitiva.**]

[Se trata de una propuesta gestada desde la] **Corporación en Pro de la Defensa del río Cuja, **[Corpocuja, que ve como una amenaza la puesta en marcha de actividades de exploración y explotación de crudo, por la concesión petrolera asignada a la compañía]**internacional Canacol Energy,**[ **conocida como Cor 4.** Un proyecto que a su vez se busca implementar en el municipio de Arbeláez, donde el próximo 9 de julio se tiene estipulada la realización de una consulta popular.]

[“**Si o no están de acuerdo con que se ejecuten actividades exploratorias, sísmicas, perforaciones, explotaciones, producción y transporte de hidrocarburos en el municipio de Pasca**”, es la pregunta que aprobó el Tribunal, tras el trabajo de más de 6 meses de los ambientalistas, quienes este viernes se reunirán con la alcaldía para estudiar una posible fecha de realización de la consulta.]

[Cabe recordar que el pasado 16 de septiembre, las comunidades del Sumapaz presentaron al presidente Juan Manuel Santos un derecho de petición firmado por 25.000 personas que exigen la rescisión de los contratos de exploración y explotación petrolera y minera adjudicados en los **municipios de Fusagasugá, Pasca, Pandi, Silvania, San Bernardo, Tibacuy, cabrera, granada, Arbeláez, Sibaté, Venecia.**]

[Las comunidades encuentran que estos proyectos de extracción atentan contra el agua, la soberanía alimentaria y afectarían el ecosistema. Aseguran que no se detendrán en su lucha, pues el objetivo "es garantizar el 13% de la alimentación de la capital del país y las comunidades aledañas”.]

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
