Title: Reclusos de cárcel La Modelo tienen miedo de denunciar masacre del 21 de marzo
Date: 2020-12-02 15:38
Author: CtgAdm
Category: Expreso Libertad
Tags: #Cárcel, #INPEC, #LaModelo, #Masacre
Slug: reclusos-de-carcel-la-modelo-tienen-miedo-de-denunciar-masacre-del-21-de-marzo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/03/modelo.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

Organizaciones defensoras de derechos humanos denuncian que tras la masacre cometida el pasado 21 de marzo en la cárcel La Modelo, la población reclusa tiene miedo de denunciar el conjunto de acciones violatorias a derechos humanos de las que fueron víctimas. (Le puede interesar: "[Muertes de los detenidos en "La Modelo" a manos del INPEC fueron intencionales: HRW](https://archivo.contagioradio.com/muertes-de-los-detenidos-en-la-modelo-a-manos-del-inpec-fueron-intencionales-hrw/)")

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

De acuerdo con Óscar Ramírez, integrante del Comité de Solidaridad con Prisioneros Políticos, a la fecha no se ha logrado establecer un diálogo con la población reclusa en torno a los hechos ocurridos durante ese día. Además señala que tampoco se ha podido establecer comunicación con las familias de las **24 personas que fueron asesinadas ese 21 de marzo.**

<!-- /wp:paragraph -->

<!-- wp:html -->  
<iframe id="audio_61294189" frameborder="0" allowfullscreen scrolling="no" height="200" style="border:1px solid #EEE; box-sizing:border-box; width:100%;" src="https://co.ivoox.com/es/player_ej_61294189_4_1.html?c1=ff6600"></iframe>  
<!-- /wp:html -->

<!-- wp:paragraph -->

Por otro lado el abogado Uldarico Flores, integrante de la Brigada Eduardo Umañana asegura que la misma guardia del INPEC que habría cometido los abusos de poder y el asesinato de los reclusos, continúa al interior del penal. Ello porque no se ha ordenado ninguna suspensión, retiro del cargo o cambio de guardia al interior del penal.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En este Expreso Libertad escuche las denuncias de estas organizaciones en contra del accionar del INPEC, el Ministerio de Justicia y las distintas instituciones gubernamentales en torno a **la impunidad que existe sobre la masacre** del 21 de marzo en la cárcel La Modelo.

<!-- /wp:paragraph -->
