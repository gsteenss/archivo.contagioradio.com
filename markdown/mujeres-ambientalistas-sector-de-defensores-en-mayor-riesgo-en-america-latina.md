Title: Mujeres defensoras del ambiente las más amenazadas en América latina
Date: 2016-06-27 15:30
Category: DDHH, Nacional
Tags: Agresiones contra defensoras, Defensores del medio ambiente en América Latina, Fondo de Acción Urgente
Slug: mujeres-ambientalistas-sector-de-defensores-en-mayor-riesgo-en-america-latina
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/06/Mujeres-ambientalistas.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: DW] 

###### [27 Junio 2016 ]

De acuerdo con el más reciente informe del Fondo de Acción Urgente, las defensoras del ambiente, las mujeres defensoras de derechos humanos, y las defensoras de derechos de las mujeres y cuestiones relativas al género, son el grupo de defensoras en mayor riesgo actualmente en América Latina, siendo **las mujeres ambientalistas el sector poblacional que reúne todas estas características y que por tanto está en mayor situación de riesgo**.

Frente a la criminalización, las mujeres ambientalistas están en un riesgo particular por su condición de género. La mayoría de las agresiones de las que son víctimas presentan alto contenido sexual, asevera la investigadora Laura Carvajal, integrante del Fondo. En este contexto de criminalización son comunes las amenazas, intimidaciones, hostigamientos, expulsiones, torturas, detenciones, **ataques a oficinas y hogares, destrucción de cultivos, estigmatizaciones por parte de funcionarios públicos**, campañas de desprestigio en redes, e intentos de asesinato.

"Hay una mayor parcialidad por parte de los operadores de justicia que parte de una lógica patriarcal en la que **las mujeres por ser mujeres son revictimizadas, no se les cree, su testimonios son deslegitimados** y subestimados, y por esto hay un mayor riesgo de que las agresiones en su contra queden en la impunidad". La criminalización se enmarca en una estructura de violencia basada en género en la que el extractivismo, por su lógica patriarcal, exacerba todos los tipos de violencias y desigualdades, agrega Carvajal.

Pese a que las mujeres son las que producen la mayor parte de los alimentos, son el sector con **más limitaciones para el acceso efectivo a la tierra y para participar en las consultas previas**, que resultan ser "una farsa, porque los Estados tienen una serie de mecanismos fraudulentos para suplantar el consentimiento de las comunidades", según afirma Carvajal, por lo que se llama a los Gobiernos para que tomen medidas efectivas que garanticen la participación de las mujeres, su vida digna y acción en defensa del medio ambiente.

Conozca las recomendaciones que esta organización emite a los Estados para la protección de las ambientalistas en la siguiente entrevista.

<iframe src="http://co.ivoox.com/es/player_ej_12044081_2_1.html?data=kpedlpmUfJKhhpywj5aXaZS1lZmah5yncZOhhpywj5WRaZi3jpWah5ynca3V1tfOjajFttfVy8bZjZKPitDixdSYxsqPhcTXyoqwlYqmd8-fttfUx9PYqY6ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)]] 
