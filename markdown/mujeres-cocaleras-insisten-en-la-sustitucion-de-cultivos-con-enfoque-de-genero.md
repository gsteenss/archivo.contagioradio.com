Title: Mujeres cocaleras insisten en la sustitución de cultivos con enfoque de género
Date: 2017-03-22 12:50
Category: Mujer, Nacional
Tags: cultivos ilícitos, hoja de coca, mujeres cocaleras, Putumayo
Slug: mujeres-cocaleras-insisten-en-la-sustitucion-de-cultivos-con-enfoque-de-genero
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/03/7d15abe6213b9e29ccb9bb6d04f6143e_0.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: El Espectador] 

###### [22 Mar. 2017 ]

**Más de 200 mujeres cocaleras de varias regiones del país** reunidas en el departamento de Putumayo, que conocieron las realidades que acontecen en varias regiones del país y realizaron un intercambio de propuestas en torno al tema de los cultivos de uso ilícito, **le dijeron sí a la sustitución, pero con plenas garantías para seguir trabajando.**

En el encuentro que pretendía intercambiar realidades además con invitadas internacionales provenientes de Bolivia se logró **conocer las necesidades y el trabajo de cada una de las mujeres** que se han dedicado por diversas labores al trabajo en este tipo de cultivos. Le puede interesar: [Mujeres cocaleras de todo el país se reúnen para trabajar propuestas de sustitución](https://archivo.contagioradio.com/mujeres-cocaleras-de-todo-el-pais-se-reunen-para-trabajar-propuestas-de-sustitucion/)

Según Leidy Maigual integrante de Alianza de Mujeres del Putumayo y participante de este evento “contar con la presencia de tantas mujeres tan diversas de Colombia y de Bolivia, nos hizo comprender que podemos aprender de otras experiencias. **Hablamos de lo agropecuario, de lo cultural, de los cultivos** y fue interesante escuchar que en Bolivia se han organizado en diversos grupos que encontraron en varios cultivos oportunidades de generación de ingresos para las familias”.

El departamento del Putumayo en la actualidad, tiene **la capacidad de generar producción de tipo avícola, agropecuaria y pisicultura**, lo que permitiría que las mujeres puedan abrirse espacio en otro tipo de mercado diferente a los cultivos de coca.

“A nosotras la invitada de Bolivia nos decía que debemos variar de cultivos y así nos podemos dividir las generaciones de ingresos, porque si saturamos el comercio pues va a haber un abaratamiento del producto” agregó Maigual.

Una de las grandes conclusiones de este encuentro fue que las mujeres reconocieron las **posibilidades que pueden encontrar en otro tipo de cultivos que “no nos generan tanto esfuerzo**, menos trabajo, no nos sacrificamos tanto y tampoco nos arriesgamos con el tema de ir a una cárcel o de poner tantos conflictos en nuestras familias” añadió Maigual. Le puede interesar: [Erradicación de cultivos de uso ilicito van en contra de Acuerdo de Paz](https://archivo.contagioradio.com/erradicacion-de-cultivos-hecha-por-el-gobierno-iria-en-contra-de-acuerdo-de-paz/)

Sin embargo, para realizar estos cambios, las mujeres reconocieron que hay que **continuar exigiendo al Gobierno nacional sean implementados los acuerdos de paz** firmados en La Habana y puntualmente el tema de erradicación de cultivos de uso ilícito. Le puede interesar:["Comunidades del Putumayo insisten en plan de sustitución a pesar de erradicación forzada"](https://archivo.contagioradio.com/comunidades-del-putumayo-insisten-en-plan-de-sustitucion-a-pesar-de-erradicacion-forzad/)

“El Gobierno nacional está combatiendo los cultivos ilícitos de raíz y no se está dando cuenta que de estos se están beneficiando sean de manera directa o indirecta muchas comunidades. **Las erradicaciones de hoja de coca se están haciendo sin llegar a las comunidades con ayudas que prometieron”** relató la líder putumayense.

La gran pregunta que les dejó este encuentro de cara a la erradicación forzada de la que están siendo víctimas las comunidades es **¿de qué vamos a vivir si no hay sustitución, no hay vías para sacar nuestros productos?** “Necesitamos, entre otras cosas, vías de acceso. Necesitamos que el Gobierno se reporte de manera urgente” denunció Maigual. Le puede interesar: [Familias Cocaleras ya tienen Plan de Sustitución de Cultivos](https://archivo.contagioradio.com/familias-cocaleras-ya-tienen-plan-de-sustitucion-de-cultivos/)

Por último, Maigual dijo que no se puede desconocer el trabajo que muchas mujeres han hecho en cuanto al tema de los cultivos ilícitos “muchas veces somos invisibles ante el Gobierno porque somos mujeres y dicen que no trabajamos estos cultivos, cuando muchas de nosotras somos raspachinas, trabajamos en laboratorios, o en cocinar. **Exigimos implementación de los acuerdos con enfoque de género para el tema de sustitución de cultivos, para generar nuestros propios ingresos”.**

###### Reciba toda la información de Contagio Radio en [[su correo]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio]{.s1}](http://bit.ly/1ICYhVU)[.]{.s1}
