Title: "Concesión de títulos mineros tiene fallas de naturaleza estructural": Corte Constitucional
Date: 2016-08-05 17:40
Category: Ambiente, Nacional
Tags: Corte Constitucional, Mineria, paz, titulos mineros
Slug: concesion-de-titulos-mineros-tiene-fallas-de-naturaleza-estructural-corte-constitucional
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/08/Corte-constitucional-e1470436473989.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: TNN Políticas 

###### [5 Ago 2016] 

Al considerar que los procesos de titulación minera en Colombia tienen profundas contradicciones con la Constitución Política, los abogados Laura Santacoloma y Rodrigo Negret,  interpusieron una demanda ante la Corte Constitucional, cuya instancia les dio la razón afirmando que efectivamente **“La concesión de títulos mineros tiene fallas de naturaleza estructural que, por su complejidad, deben ser resueltas de manera integral por el Congreso de la República”,** como dice la sentencia.

De acuerdo con Laura Santacoloma, esos errores se derivan principalmente porque el Estado colombiano no garantiza la participación de las comunidades, teniendo en cuenta su derecho a la consulta previa libre e informada; así mismo argumenta que una vez se otorga un título minero  tampoco se ha pensado en el derecho de las generaciones futuras a un ambiente sano.

Lo anterior debido a que, según la Corte solo se dio importancia a un aspecto formal, es decir “Los requisitos (para obtener un título minero), que básicamente tienen que ver con exigencias como la cédula de ciudadanía, los anexos técnicos y el mapa, **pero no se tiene ninguna cualificación de los mineros, no hay aspectos sobre participación ciudadana y tampoco se verifican requisitos como los antecedentes ambientales y técnicos”.**

La corte dice entonces que por medio de la concesión de estos títulos mineros, se están violentando los derechos fundamentales por una interpretación formal de las normas, explica la abogada, y agrega que actualmente el otorgamiento de un título minero es una decisión eminentemente estatal, donde no se consulta a las personas sobre las cuales recaen los efectos de la titulación, pues los pobladores generalmente no tienen el derecho de oponerse a una explotación que pueda poner en peligro su integridad cultural y los demás derechos de la comunidad.

Pese a que no se conoce el cuerpo de la sentencia, cabe recordar que la Corte Constitucional ya ha instado al congreso a sacar un nuevo Código Minero, no solo a través de este fallo, sino por medio de otros cinco donde se dice que **“el modelo minero como está estructurado no está funcionando, ni está siendo acorde a la constitución”**, expresa Laura Santacoloma, quien espera que este sea un paso esencial para que en la etapa del posconflicto no se hable solo de una paz con las guerrillas sino también con el ambiente.

<iframe src="http://co.ivoox.com/es/player_ej_12455474_2_1.html?data=kpehl5qYe5Whhpywj5aaaZS1lZmah5yncZOhhpywj5WRaZi3jpWah5ynca3V1tfOjbjFstXVxNTZ0dLFaZO3jKbP0czFqMKhhpywj6jTstXVyM7cjbfFqMrjjJKSmaiReA..&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)
