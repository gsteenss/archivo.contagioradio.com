Title: Caso Santiago Uribe no es cuestión de pruebas sino de poder
Date: 2016-10-24 15:49
Category: Judicial, Nacional
Tags: Alvaro Uribe, Comisión de Justicia y Paz, Daniel Prado, Finca La Carolina, Los 12 apostoles, Santiago uribe
Slug: verdades-de-los-12-apostoles-en-juicio-santiago-uribe
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/10/DANIEL-PRADO.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [24 de Oct 2016]

Luego de darse por concluida la etapa de investigación a **Santiago Uribe**, por sus posibles **vínculos con el grupo paramilitar llamado “los  12 apóstoles”**, el Fiscal 10 Delegado ante la Corte Suprema de Justicia decidió **llamarlo a juicio.**

Después de conocida la citación contra el hermano menor del senador Álvaro Uribe, **Daniel Prado, abogado de la Comisión de Justicia y Paz aseguró que se espera que este juicio pueda llegar a buen término** y manifestó que no se pueden desconocer las pruebas que han hecho que el caso llegue a este momento.

“Durante los meses que llevamos de instrucción no ha cambiado nada, lo que si se ha reafirmado a través de testimonios y que es un hecho incontrovertible es que el grupo paramilitar “los 12 apóstoles” operaba en la región y que **es causante de aproximadamente 900 homicidios** y se ha develado el funcionamiento y la estructura de mando, en la que Santiago Uribe es fundamental.

Durante el tiempo que lleva la investigación, la Fiscalía ha decretado pruebas de oficio que ha pedido a la defensa y a las víctimas para poder identificar cuál era el andamiaje paramilitar de la región y qué otras personas estaban implicadas en el caso “**otros hechos que la Fiscalía tendrá en cuenta en su momento es que se hacían campañas de limpieza social por parte de miembros de la familia Uribe Vélez**… Aquí han surgido pruebas que vuelven a darle vida a este proceso y de las cuales le solicitamos a la Fiscalía tome cartas en el asunto”. Le puede interesar: [Nuevo testigo reafirma vínculos entre familia Uribe Vélez y ‘los 12 apóstoles’](https://archivo.contagioradio.com/nuevo-testigo-reafirma-vinculos-entre-familia-uribe-velez-y-los-12-apostoles/)

Por su parte, el defensor de Uibe, Jaime Granados , manifiesta que los testigos son mentirosos y locos. Sin embargo, la defensa de las víctimas, en cabeza de Prado manifestó **“me parece muy irrespetuoso que el señor Granados siga mintiendo, él está engañando al país.** Lamentablemente la **incapacidad jurídica del doctor Granados** no le ha permitido convencer a los fiscales de la inocencia de su defendido**”.**

Así mismo, **Alberto Franco, representante legal de la Comisión de Justicia y Paz aseguró que saludan esta decisión que a todas luces es importante porque “enaltece a las víctimas y es una decisión en honor a ellas”**. Sin embargo, replico que ésta se da muy tarde “se habrían podido evitar muchos asesinatos, mucho dolor y mucho sufrimiento si se hubiera tomado a tiempo la decisión de actuar en derecho”.

Por su parte, Santiago Uribe afirma que lo que se encuentra detrás de este tema es una persecución política. Contrario a ello, Prado asegura que **“lo que se pretende es tender una cortina de humo para que la gente no entienda cuál era la actividad criminal que llevaban a cabo algunos miembros de la familia Uribe Vélez** en la región antioqueña”.

De no haber justicia en este caso, el abogado asegura que **las víctimas han manifestado que la opción es irse a instancias internacionales,** para buscar garantías en derecho que deban ser tomadas en el proceso, “el tema aquí es que **esto es una pelea contra el poder no un problema probatorio (...)** lo que hay que esperar es si el gran poder que maneja no hará que este caso quede en la impunidad” concluye.

\[embed\]https://www.youtube.com/watch?v=mhi96lHHOpQ\[/embed\]

### **20 años de investigaciones ** 

**Santiago Uribe**, es acusado por los **delitos de concierto para delinquir agravado y homicidio agravado** a propósito de la constitución del grupo paramilitar "los 12 apóstoles" que cumplía operaciones en Yarumal, Antioquia.

**Esta investigación comienza en el año 2010 cuando se decidió reabrir el proceso.** Para esa época, se retoman las pruebas que ya habían sido practicadas con anterioridad y se comienzan a realizar otras que dan cuenta de los hechos acontecidos en 1994 en Yarumal.

**Santiago, hermano del senador del Centro Democrático Álvaro Uribe, fue capturado el 29 de febrero de 2016 en Medellín** y se mantiene desde marzo privado de su libertad en una guarnición militar en Ríonegro (Antioquia). Le puede interesar:  [Captura de Santiago develaría más delitos de familia Uribe Vélez](https://archivo.contagioradio.com/captura-de-santiago-uribe-develaria-mas-delitos-de-familia-uribe-velez/)

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
