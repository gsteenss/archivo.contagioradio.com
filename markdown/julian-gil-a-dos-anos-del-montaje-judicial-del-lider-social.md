Title: Julián Gil: A dos años del montaje judicial del líder social
Date: 2020-06-04 14:06
Author: AdminContagio
Category: Expreso Libertad, Nacional, Programas
Tags: congreso de los pueblos, defensores de derechos humanos, Derechos Humanos, Expreso Libertad, Lider social, montaje judicial
Slug: julian-gil-a-dos-anos-del-montaje-judicial-del-lider-social
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/06/Portada_Juli.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:heading {"align":"right","level":6,"textColor":"cyan-bluish-gray"} -->

###### Foto de: Colombia Informa {#foto-de-colombia-informa .has-cyan-bluish-gray-color .has-text-color .has-text-align-right}

<!-- /wp:heading -->

<!-- wp:paragraph -->

Esta semana se cumplen dos años del montaje judicial en contra del líder social Julián Gil, integrante de la plataforma política [Congreso de los Pueblos](https://www.congresodelospueblos.org/). En este **Expreso Libertad**, conversamos con él, su abogado defensor Raymundo Vasquéz y Alejandra Toloza, del proceso Quinua, sobre la violencia tras un montaje judicial.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Para Julián, una de las principales razones tras este tipo de hechos, es la criminalización a la defensa de los derechos humanos y el trabajo que hacen las y los líderes sociales en Colombia.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Asimismo Vásquez afirma, que al igual que en otros montajes judiciales, lo que se ha hecho en contra de Gil, es dilatar el proceso, sin evidenciar pruebas o testigos en su contra.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Sin embargo, Alejandra Toloza asegura que tanto las comunidades que acompañaba Julián, como sus círculos más cercanos, continúan a la espera de su libertad y en defensa de su inocencia.

<!-- /wp:paragraph -->

<!-- wp:core-embed/facebook {"url":"https://www.facebook.com/contagioradio/videos/275126653895143/","type":"video","providerNameSlug":"facebook","className":""} -->

<figure class="wp-block-embed-facebook wp-block-embed is-type-video is-provider-facebook">
<div class="wp-block-embed__wrapper">

https://www.facebook.com/contagioradio/videos/275126653895143/

</div>

</figure>
<!-- /wp:core-embed/facebook -->

<!-- wp:paragraph -->

[Mas programas de Expreso Libertad](https://archivo.contagioradio.com/programas/expreso-libertad/)

<!-- /wp:paragraph -->
