Title: Búsqueda de desaparecidos en "La Escombrera" debe ser una labor integral
Date: 2015-07-17 07:30
Category: DDHH, Entrevistas, Judicial, Nacional
Tags: Adriana Arboleda, Corporación Jurídica Libertad, Desaparición forzada en Colombia, Fiscalía, Fiscalía General de la Nación, La escombrera
Slug: busqueda-de-desaparecidos-en-la-escombrera-debe-ser-una-labor-integral
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/07/escombrera-contagioradio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: documentalamarillo.blogspot 

###### [17 Jul 2015] 

Este 27 de julio iniciarán las excavaciones en el sector de "la escombrera" en Medellín para buscar los cuerpos de víctimas de desaparición forzada. Con un acto de memoria y con el lema **“Escarbando la verdad, desenterrando la justicia”**, iniciarán las excavaciones en La Arenera, uno de los 8 sectores de la escombrera que, a pesar de las evidencias, sigue funcionando.

La **Corporación Jurídica Libertad**, CJL, organización defensora de los DDHH, afirma que lastimosamente la escombrera es lo que es ahora, una montaña, porque no se ha escuchado a las víctimas, que desde hace varios años vienen solicitando el cierre del sector pero que no ha dejado de funcionar como sitio de depósito de escombros de toda la ciudad.

Adriana Arboleda, abogafda de la Corporación, afirma que se debe poner en marcha un Plan Integral de Búsqueda. Según ella, existen alrededor de **100 víctimas de desaparición forzada en la Comuna 13**, tema que no ha contado con la investigación pertinente de parte del Estado colombiano. Agrega que esta comuna ha sido un escenario permanente de violación a derechos humanos.

De igual forma, la CJL exige a la Fiscalía realizar procesos de investigación preliminar que den cuenta de lo sucedido con la víctimas para lograr una intervención eficiente, pues el esclarecimiento de la verdad de las desapariciones es una premisa para dar garantías de no repetición teniendo en cuenta que el paramilitarismo persiste, “*no puede ser posible que avancemos en estos procesos de búsqueda pero sigamos en medio de un control total de parte de estas estructuras ilegales"*.

El Movimiento de Víctimas de Crímenes de Estado MOVICE apoyará en el asesoramiento técnico de antropología forense. El Alto Comisionado de Naciones Unidas para los DDHH apoyará en la veeduría para que la búsqueda sea llevada a cabo técnicamente y exigen que la Fiscalía se mantenga en pie . Buscan que sea un proceso que garantice el respecto de las victimas, la Fiscalía anunció que esta comprometida y acepta que los abogados tengan asesoría para el tema.

 
