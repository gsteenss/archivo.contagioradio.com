Title: Exintegrantes de actores armados piden perdón a menores víctimas del conflicto
Date: 2019-11-25 17:40
Author: CtgAdm
Category: Nacional, Paz
Tags: comision de la verdad, Convención de Derechos del Niño
Slug: exintegrantes-de-actores-armados-piden-perdon-a-menores-victimas-del-conflicto
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/Verdad-Niños.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/EKBDuREXUAAHEAF.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/Verdad.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: ComisionVerdadC] 

Como parte de su mandato, los pasados 22 y 23 de noviembre en la ciudad de Medellín, la Comisión de la Verdad celebró un nuevo Encuentro por la Verdad, un espacio para reconocer los diversos impactos, responsables y afectaciones que han sufrido niños, niñas y adolescentes en el marco del conflicto. Según el Registro Único de Víctimas los menores de edad **representan un 30% de las 8.874.110 víctimas de la guerra** **en Colombia**, una estadística que va en ascenso con la agudización del conflicto en el país.

Para **Sinthya Rubio, coordinadora del Enfoque de Vida de la Comisión de la Verdad,** este encuentro que se suma a los reconocimientos realizados a las víctimas de violencia sexual y desaparición forzada durante el conflicto, permitió sentar las bases para continuar desde la Comisión un trabajo de diálogo social y gestión del conocimiento alrededor de una participación activa de niños, niñas y adolescentes que representan un 30% de la población nacional.

"Estamos consolidando espacios en los que se pueda dialogar, hacer reflexiones profundas en cuanto a lo que sucedió, no solo que tengan una voz sino también cuestionarse qué es lo que ha pasado y comprender esos impactos a largo plazo" manifestó la coordinadora con relación a los **más de 2.312.707  menores que han sido víctimas del conflicto armado en Colombia.**

“Es importante reconocer y amplificar la voz de quienes sufrieron la violencia en la primera etapa de su vida” manifestó la comisionada de la Verdad, Lucía González, durante el evento. [(Le puede interesar: Mueren niños por desnutrición en Reserva Indígena de Caño Mochuelo, Casanare)](https://archivo.contagioradio.com/muerte-por-desnutricion-ninos-cano-mochuelo/)

> “Ofrezco mi arrepentimiento y mi compromiso de no repetición. Ojalá esta intervención sirva para que Colombia entienda que los niños no deben hacer parte de la guerra. Gracias y de nuevo pido perdón”: Daladier Rivera, mayor del [@COL\_EJERCITO](https://twitter.com/COL_EJERCITO?ref_src=twsrc%5Etfw) ► <https://t.co/5Kkc6q26Ea> [pic.twitter.com/MeZQViJz4f](https://t.co/MeZQViJz4f)
>
> — Comisión de la Verdad (@ComisionVerdadC) [November 22, 2019](https://twitter.com/ComisionVerdadC/status/1197997790393196552?ref_src=twsrc%5Etfw)

<p>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
</p>
### No solo se trata del reclutamiento contra niños, niñas y adolescentes

Como parte del encuentro, la Coalición Contra la Vinculación de niños, niñas y jóvenes al Conflicto Armado en Colombia (Coalico) hizo entrega a la Comisión del informe “Sueños y vidas truncadas por una guerra que no es nuestra: Niños, niñas y adolescentes en el conflicto armado en Colombia” que detalla las modalidades de vinculación  directa o indirecta de menores en el conflicto por parte de todos los actores armados.

Al respecto, antiguos integrantes de grupos en el marco del conflicto armado que participaron del encuentro, como F**reddy Rendón, excomandante del bloque Elmer Cárdenas de las Autodefensas Unidas de Colombia,** reconoció que miles de niños y niñas fueron reclutados, asesinados, desaparecidos y sometidos a condiciones inhumanas, bajo su mando, "reconocer las acciones con las que hice tanto daño no es fácil. No me enorgullece haber reclutado a cientos de niños, muchos de ellos sufrieron heridas y afectaciones psicológicas que los toda la vida y también a sus familias".

A su voz, se sumó la de **Rodrigo Londoño, último comandante en jefe de las FARC-EP y** actual presidente del partido Farc , quien expresó que su intención "con sinceridad, es reivindicar y dignificar a los niños y niñas que se vieron afectados por las operaciones militares”.

Aunque el reclutamiento es probablemente una de los tipos de violencia más comunes en el conflicto, siendo 16.879 los menores de edad forzados a ingresar al conflicto desde 1960 hasta 2016, Sinthya señala que la afectación no solo se reduce a este delito, pues otros tipos de violaciones de DD.HH. como desplazamiento, hostigamiento, homicidio, desaparición forzada, combates, delitos contra la libertad y la integridad sexual,  abandono, secuestro y tortura han impedido su libre desarrollo históricamente en contextos de guerra.  [(Lea también: Comunidades rurales trabajan por el goce efectivo de los derechos de las niñas y niños)](https://archivo.contagioradio.com/comunidades-rurales-trabajan-por-el-goce-efectivo-de-los-derechos-de-las-ninas-y-ninos/)

Ante esta realidad, la Comisión hizo un énfasis en cómo a través de estas violencias se ataca la  transmisión de conocimiento e identidad cultural de los menores e insistió en continuar fomentando  la reflexión y promover un trabajo integral para mitigar su impacto.

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
