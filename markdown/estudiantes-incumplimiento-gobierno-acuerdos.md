Title: Estudiantes retomarán acciones ante incumplimiento del Gobierno a los acuerdos de 2018
Date: 2019-07-09 15:16
Author: CtgAdm
Category: Estudiantes, Movilización
Tags: acuerdos, educación pública, estudiantes, Gobierno
Slug: estudiantes-incumplimiento-gobierno-acuerdos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/estudiantes.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

[Más de seis meses han pasado desde que el presidente Iván Duque y las mesas estudiantiles llegaron acuerdos presupuestales y estructurales, sin embargo, el acuerdo no ha sido cumplido, ya que el gobierno todavía adeuda alrededor de 300.000 millones de pesos. (Le puede interesar: ["En el congreso y en la calle estudiantes defienden la lucha por la educación"](https://archivo.contagioradio.com/congreso-calle-estudiantes-lucha-educacion/))]

[Jennifer Pedraza, representante estudiantil expresa que “el Gobierno destinaría alrededor de 300.000 millones de pesos anuales, durante los próximos cuatro años, dispuestos para la ciencia y la tecnología", no obstante **el dinero no se ha dado bajo el argumento que "este año se debe ahorrar y qué invertir en ciencia no es tan importante.**” (Le puede interesar: ["¿Por qué los estudiantes se seguirán movilizando en 2019?"](https://archivo.contagioradio.com/estudiantes-movilizando-2019-2/))]

### **La agenda de los estudiantes** 

[El plan de acción de los estudiantes debe ser acelerado, por ello “se buscará llamar la atención de la opinión pública y realizar foros para que los estudiantes se informen y así la comunidad universitaria conozca los logros y las fallas del Gobierno”; esto debido a que el presupuesto para el 2020 se está definiendo en este momento.]

[Para la líder estudiantil las acciones y preocupaciones giran en torno a aumentar el rubro para las universidades públicas, la garantía de  la autonomía y la democracia universitarias, y disponer de un monto específico para las instituciones técnicas y tecnológicas. Razón por la cual, según los estudiantes, es prioridad reformar  el ICETEX y los artículos 86 y 87 de la Ley 30 de la educación superior los cuales disponen: ]

 

*[“Artículo 86. Los presupuestos de las universidades nacionales, departamentales y municipales estarán constituidos por aportes del presupuesto Nacional para funcionamiento e inversión, por los aportes de los entes territoriales, por los recursos y rentas propias de cada institución. Las universidades estatales u oficiales recibirán anualmente aportes de los presupuestos nacional y de las entidades territoriales, que signifiquen siempre un incremento en pesos constantes, tomando como base los presupuestos de rentas y gastos, vigentes a partir de 1993”]*

*[“Artículo 87. A partir del sexto año de la vigencia de la presente ley, el Gobierno Nacional]*

*[incrementará sus aportes para las universidades estatales u oficiales, en un porcentaje no inferior al 30% del incremento real del Producto Interno Bruto. Este incremento se efectuará en conformidad con los objetivos previstos para el Sistema de Universidades estatales u oficiales y en razón al mejoramiento de la calidad de las instituciones que lo integran. Parágrafo. El incremento al que se refiere el presente artículo se hará para los sistemas que se creen en desarrollo de los  artículos 81 y 82 y los dineros serán distribuidos por el Consejo Nacional de Educación Superior (CESU), previa reglamentación del Gobierno Nacional.”]*

### **Los avances** 

[Gracias a la gran movilización del año pasado las mesas estudiantiles lograron que el pasado 27 de mayo se le diera a las universidades públicas 107.000  y para las instituciones técnicas y tecnológicas 5400 millones de pesos, sin embargo, estas acciones no aportan lo suficiente para que se garantice el derecho a la educación gratuita y de calidad en todo el país y por ello se avanzará en nuevas movilizaciones asegura Pedraza. ]

> Desde la UNEES llamamos a las IES a realizar las discusiones pertinentes frente al panorama actual, que nos permitan como plataforma, proyectar horizontes políticos en el marco de la defensa de la educación. [pic.twitter.com/NX59dQTUT2](https://t.co/NX59dQTUT2)
>
> — UNEES Colombia (@UneesCol) [7 de julio de 2019](https://twitter.com/UneesCol/status/1147953478100279296?ref_src=twsrc%5Etfw)

<p>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
</p>
**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

<iframe id="audio_38153346" frameborder="0" allowfullscreen scrolling="no" height="200" style="border:1px solid #EEE; box-sizing:border-box; width:100%;" src="https://co.ivoox.com/es/player_ej_38153346_4_1.html?c1=ff6600"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
