Title: Nuevo ataque contra defensores de DDHH en Putumayo
Date: 2017-03-20 17:50
Category: DDHH, Nacional
Tags: Amerisur, Comisión Intereclesial de Justicia y Paz, Defensores DDHH, Putumayo
Slug: ataque-defensores-ddhh-putumayo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/03/atentado-e1490049932522.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: El Universal 

###### 20 Mar 2017 

El pasado sábado 18 de marzo, se registró un nuevo ataque en contra de dos defensores de derechos humanos de la Comisión Intereclesial de Justicia y Paz, que acompañan y apoyan procesos en la Zona de Reserva Campesina Perla Amazónica y al Pueblo Nasa de Putumayo, departamento en el que se han presentado nuevo modos de operaciones de tipo paramilitar.

De acuerdo con el reporte de esa organización, a las 2:20 de la tarde **los defensores Viviana Martínez y Carlos Fernández** se encontraban cerca de llegar a la ciudad de Mocoa, capital del Departamento, desde Puerto Asís, junto al esquema de seguridad asignado por la Unidad Nacional de Protección, cuando f**ueron sorprendidos por dos sujetos que se movilizaban en una motocicleta blanca de alto cilindraje** que no portaba matrícula.

Antes de acercarse al vehículo en que se transportaban, los **motorizados impactaron el espejo retrovisor del costado izquierdo**, que corresponde al escolta, para luego huir en dirección contraria, en una maniobra rápida que no permitió la reacción del agente de seguridad. Le puede interesar:[Ante embajador Británico comunidades denuncian afectaciones de petrolera Amerisur](https://archivo.contagioradio.com/ante-embajador-britanico-comunidades-putumayo-amerisur-37897/)

De acuerdo con la denuncia, recientemente **los dos defensores habían participado en una visita internacional, realizada con el propósito de verificar los daños ambientales generados por las operaciones petroleras de la empresa Amerisur**, y documentaron casos de abusos de autoridad que desconocen lo alcanzado en el punto cuatro del Acuerdo de paz.
