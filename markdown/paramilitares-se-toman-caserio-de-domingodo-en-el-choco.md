Title: Paramilitares se toman caserío de Domingodó en el Chocó
Date: 2017-03-07 12:50
Category: DDHH, Nacional
Tags: Aumento de Paramilitares, Chocó, Domingodó, paramilitares
Slug: paramilitares-se-toman-caserio-de-domingodo-en-el-choco
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/03/documento-choco.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [07 Mar. 2017] 

Según una denuncia dada a conocer por la Comisión Intereclesial de Justicia y Paz, **un grupo indeterminado de paramilitares,** autodenominados como integrantes de las Autodefensas Gaitanistas de Colombia, **ingresaron desde la madrugada de este martes al caserío de Domingodó, en el departamento del Chocó.**

De acuerdo con la información entregada por algunos pobladores, e**ntre los hombres que ingresaron, algunos estaban vestidos de camuflado y de civil, y uno más que estaba encapuchado**, buscaba personas en algunas de las casas del lugar. Le puede interesar: [Paramilitares extorsionan y amenazan a pobladores de Murindó, Chocó](https://archivo.contagioradio.com/paramilitares_amenazan_extorsionan_pobladores_choco/)

Los paramilitares amenazaron a algunos pobladores que estaban presentes, por lo que varios de ellos optaron por huir del caserío. Dice la denuncia que además, **otras personas se encuentran escondidas en parajes cercanos por miedo y como mecanismo para salvaguardar sus vidas**. Le puede interesar: [Paramilitares arremeten en diferentes regiones del país](https://archivo.contagioradio.com/paramilitares-arremeten-contra-diferentes-regiones-del-pais/)

Hasta las 8 de la mañana los paramilitares llevaban cerca de tres horas en el lugar. Le puede interesar: [476 indígenas Embera desplazados por bombardeos del Ejército en Chocó](https://archivo.contagioradio.com/476-indigenas-embera-desplazados-por-bombardeos-del-ejercito-en-choco/)

**Este hecho se suma al desplazamiento de por lo menos 700 personas, habitantes de la cuenca de Peña Azul en el municipio del Alto Baudó, Chocó,**  luego la incursión paramilitar en sus territorios. Le puede interesar: [Cerca de 700 desplazados deja incursión paramilitar de AGC en Alto Baudó](https://archivo.contagioradio.com/cerca-de-700-desplazados-deja-incursion-paramilitar-en-alto-baudo-choco/)

Noticia en desarrollo…

###### Reciba toda la información de Contagio Radio en [[su correo]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio]{.s1}](http://bit.ly/1ICYhVU)[.]{.s1}

<div class="ssba ssba-wrap">

</div>
