Title: Se disparó el interés de militares y policías en retiro por la política
Date: 2020-07-19 22:29
Author: CtgAdm
Category: Columnistas invitados, Opinion
Slug: militares-aspiraciones-politicas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/07/‎Militares-política.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right"} -->

Por Mayor (R) Cesar Maldonado\*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Aunque cada vez que se sacude el toldo electoral surgen miembros de la Fuerza Pública en retiro apoyando candidaturas - y uno que otro con aspiraciones propias - recientemente se ha incrementado el número de sus pretendientes a los cargos de elección popular. ¿La pregunta es por qué?

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Durante los cuatro años del primer período de gobierno del presidente Uribe se destacó la aplicación de la política de seguridad gubernamental conocida con el nombre de Plan Patriota. Fue una ofensiva militar permanente contra los grupos terroristas, con énfasis en las FARC, en ese entonces el grupo con mayor capacidad de daño.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Uribe llegó al poder con una imagen mesiánica, poniendo el tema de la seguridad en el primer plano de los problemas nacionales, lo cual era cierto, al menos durante las dos últimas décadas. Los militares abrigaron la esperanza de que por fin tendrían las herramientas políticas y jurídicas para combatir en igualdad de condiciones a un enemigo que hábilmente se cobijaba bajo la estrategia de la *“combinación de todas las formas de lucha”.*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Cuan equivocados estaban. Uribe encauzó todos sus esfuerzos al más costoso - en términos humanitarios y económicos – de los elementos de la guerra de guerrillas: la lucha armada. Pero el menos eficaz. A pesar de que logró diezmar ostensiblemente el poder militar de las FARC, mientras tanto perdía en los estrados judiciales una batalla para la cual los militares no fueron entrenados. Una justicia ordinaria totalmente permeada, masacró a más soldados de los que las guerrillas habían podido soñar.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El desenamoramiento comienza cuando Uribe decide acuñar el término “manzanas podridas” para referirse a los militares encartados por sus excesos en la guerra. La imagen del “mesías” empezaba a desmoronarse, pues por primera vez los militares sintieron tan cerca la traición. Fueron abandonados por quien no tuvo la capacidad para diseñar una estrategia adecuada y controlar las veleidades de mando militar. No pudo haber sido peor, los militares desvanecían su honor en los calabozos de cárceles comunes, compartidas con delincuentes de carrera, mientras que su líder mesiánico solo atina a responder *“dejen que brille la justicia, muchachos”,* cuando ni siquiera a él, en su condición, le fueron leales las Altas Cortes.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Como ya sabemos, al perro no lo capan una sola vez; ya en retiro, esos militares volvieron a atender el canto de la misma sirena, acompañaron sus aspiraciones políticas con la ilusión de que *“el hombre que más ama a los soldados”* los incluyera en un eventual gobierno de su partido. Varios Generales entregaron su lealtad, trabajo y esfuerzos con miras a ocupar la cartera de defensa. Nuevamente los capó y los desechó como fusibles viejos.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

De otra parte, el tratamiento posacuerdo que se está dando a los militares que creyeron en el proceso de paz, en particular los comparecientes a la JEP, no es equitativo con la suerte de sus adversarios que gozan de un programa de reincorporación con muy buenas segundas oportunidades. No le tienen fe a la JEP y demás componentes del Sistema Integral de Justicia que se creó en el marco del acuerdo de paz, pues adolece de desconfianzas por la trayectoria ideológica de sus miembros.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Tampoco ayuda el hecho de que una parte significativa de las antiguas FARC se haya quedado en la disidencia, y que otros - entre ellos reconocidos cabecillas - desertaran de los espacios territoriales de capacitación y reincorporación, para reorganizarse en la ilegalidad con ayuda y asilo de Venezuela. Las sospechas de alianzas secretas entre el hoy partido político Fuerza Alternativa Revolucionaria del Común y las disidencias, no dejan de maquinarse en la cabeza de militares que nunca han creído en la voluntad de paz del grupo subversivo. Y consideran que está maniobra obedece a un plan estratégicamente diseñado.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Para rematar, los inoportunos Senadores Iván Cepeda y Antonio Sanguino, reconocidos y declarados contradictores de la Fuerza Pública, anunciaron la pasada semana que, en el periodo legislativo que hoy comienza radicaran dos proyectos de ley proponiendo reformas en las Fuerzas Militares. Lo cual es demasiado provocador, pues aún en retiro, los militares son muy celosos de su institución.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Estas podrían ser unas de las tantas causas que responden el interrogante inicial. Definitivamente a los militares se les destiñó su ídolo, y no creen apostar a otro. El presidente Duque en su afán de desmarcarse del Uribismo, terminó dándole la razón a sus críticos para crear una zona de confort político. Les asusta un futuro cercano en manos de Petro o Fajardo, ambos con maquinarias capaces de conquistar la Casa de Nariño. Consideran que el país va por mal rumbo y que ellos tienen la brújula que marcará el azimut correcto.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Hasta luego,

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

**(\*) Presidente Fundación Comité de Reconciliación**

<!-- /wp:paragraph -->

<!-- wp:separator -->

------------------------------------------------------------------------

<!-- /wp:separator -->

</p>
<!-- wp:paragraph {"fontSize":"small"} -->

Las opiniones expresadas en este artículo son de exclusiva responsabilidad del autor y no necesariamente representan la opinión de Contagio Radio.

<!-- /wp:paragraph -->
