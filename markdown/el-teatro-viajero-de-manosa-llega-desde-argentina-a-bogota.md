Title: El teatro viajero de "Mañosa" llega desde Argentina a Bogotá
Date: 2016-04-13 16:04
Category: Hablemos alguito
Tags: Mañosa Banda de Teatro, Teatro Argentino, Teatro la mama
Slug: el-teatro-viajero-de-manosa-llega-desde-argentina-a-bogota
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/04/teatro.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### Foto: Mañosa Banda de Teatro 

##### [13 Abr 2016] 

Luego de realizar teatro comunitario por 10 años en Parque Patricio, Brenda Rimberg, Otti Salas y Ayelen García, tres jóvenes argentinos apasionados por el arte, la dramaturgia y la música crean en 2014 "Mañosa banda de teatro", un colectivo viajero que apuesta por el intercambio cultural, a través de producciones artísticas y proyectos pedagógicos.

En su recorrido por Suramérica, el trío de amigos y compañeros estuvieron de paso por Ecuador y Perú, presentándose con su primer trabajo colectivo "´La Fábrica de Papel", una propuesta que combina el lenguaje corporal, narración y la música en vivo para contar una historia de amor entre dos obreros que desafían sus soledades.

Desde hace 4 meses "Mañosa" se encuentra en Colombia, viviendo la escena teatral del país y haciendo nuevos amigos, entre ellos el Teatro La Mama, espacio en el que presentarán su producción desde hoy y hasta el próximo 23 de abril, en funciones de miércoles a  sábado a las 7:30 de la noche.

Ayelen García, co creadora de la banda teatral y violinista de profesión, compartió en 'Hablemos Alguito' las diferentes experiencias adquiridas a nivel personal y por la agrupación desde sus inicios, lo aprendido con cada uno de los viajes y algunos detalles de su presentación en la ciudad de Bogotá.

<iframe src="http://co.ivoox.com/es/player_ej_11152440_2_1.html?data=kpael5eYeJGhhpywj5iWaZS1kZmah5yncZOhhpywj5WRaZi3jpWah5yncaLtxtHS0JCrpdPXhqigh6aopYyhjLLOh6iXaaOl0NjOjafFssXVjMnSjbnJpdXm0JKSmaiRh9Di1cbUy9SPlsLYytSYj4qbh46o&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

**Lugar:** Teatro La Mama, Calle 63 N° 9-60

**Fecha:** Miércoles 13 a Sábado 23 de Abril

**Precios:** 25.000 General, Estudiantes 15.000, Miércoles 2x1
