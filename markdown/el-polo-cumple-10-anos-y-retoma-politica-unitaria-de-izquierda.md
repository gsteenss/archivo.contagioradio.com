Title: El Polo retoma política unitaria de izquierda
Date: 2015-05-19 12:29
Author: CtgAdm
Category: Entrevistas, Política
Tags: alirio uribe, Clara López, Congreso, congreso de los pueblos, izquierda, marcha patriotica, Partido Comunista, paz, polo, unidad, Unión Patriótica, vamos por los derechos
Slug: el-polo-cumple-10-anos-y-retoma-politica-unitaria-de-izquierda
Status: published

###### Foto: Polo Democrático 

<iframe src="http://www.ivoox.com/player_ek_4517567_2_1.html?data=lZqemZqae46ZmKiak5aJd6KmkZKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRjbefpNTbydfJt9CfsdTZ0YqWh4y3zcbfw5Cws9HZ25KSmaiRh9Di1cbUy9SPlsLYytSYj4qbh46o&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Clara Lopez, Presidenta PDA] 

#####  

<iframe src="http://www.ivoox.com/player_ek_4517537_2_1.html?data=lZqemZqXe46ZmKiak5eJd6KllZKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRh9DiyNfS1dSPqMbgjLXcztSJdqSfotHW1M7Tb7bmysfSj4qbh4630NPhw8zNs4zGwsnW0ZCRaZi3jpk%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Alirio Uribe, Vamos por los Derechos] 

###### [19 may 2015] 

"H*ay un espíritu de unidad en los diferentes sectores, y estamos seguros de que tenemos que encontrar esos caminos. Sin embargo, **la unidad se hace por partes**, y en los próximos meses va a tener que discutirse la forma que tome esa unidad*" afirma Clara López, ratificada como presidenta del Polo Democrático Alternativo.

De esta manera, se consolida la idea de **presentar una candidatura única para aspirar a la alcaldía de Bogotá.** "*Tenemos una voluntad férrea para que se siga profundizando ese cambio social en la capital de la república*", asegura Clara López.

Otro de los temas medulares que logró consensuarse durante el Congreso del Polo fue la política de paz, que adquiere trascendencia y peso mediante la creación de una vicepresidencia de Paz y Derechos Humanos, que se encargaría de coordinar iniciativas y políticas de paz del partido. No sólo el apoyo a las mesas de diálogo que entablan gobierno e insurgencias. "*Nos vamos a poner la camiseta de la refrendación. **Vamos a trabajar por un acuerdo que nos se quede en el papel***", asegura la presidenta del partido.

Aunque no fueron pocos los analistas que auguraron la ruptura del Polo durante su 4to Congreso, como consecuencia de discusiones que se dieron a conocer en los últimos meses a través de declaraciones públicas y cartas abiertas, el Partido asegura haber **cerrado su máximo espacio de decisión política de manera unitaria y exitosa.**

Para las vocerías de los diferentes movimientos que integran el POLO, ha sido importante resaltar que ese es el **único partido político en Colombia que escoge a sus directivas locales, regionales y nacionales de manera democrática.** Las 776 personas que asistieron al Congreso los días 15 y 16 de mayo, fueron elegidas mediante consulta interna el pasado 19 de abril; la mesa directiva, la presidencia y vicepresidencia del Partido se eligieron -o ratificaron- con la participación decisiva de las vocerías elegidas.

Otro es el panorama que plantea hoy el Partido, que en años anteriores, durante su 2do y 3er Congreso, se enlodara al momento de hablar sobre alianzas y paz. Cabe recordar que estos puntos neurálgicos fueron los que antecedieron la expulsión de las filas del Polo del Partido Comunista, y el deslinde de otros movimientos que lo conformaban. Parecía entonces que la unidad de las izquierdas no podía superar el momento táctico electoral que llevó al Polo a tener más de 2 millones de votos en las elecciones presidenciales del 2006; pues a la hora de abordar discusiones estratégicas las diferencias se convertían en fracturas profundas.

Sin embargo, el pasado Congreso, el Polo no solo recibió el saludo del **Partido Comunista, la Marcha Patriótica y el Congreso de los Pueblos**; también definió claramente entablar espacios de diálogo que tengan como fin avanzar en la construcción de escenarios unitarios con "*organizaciones que sean políticamente afines, con los que potencialmente se puedan hacer cambios*", según afirma **Alirio Uribe,** del movimiento Vamos por los Derechos. "Aliados estratégicos" según lo definido por el Congreso, que posibiliten ganar el poder local en los comicios de octubre, no para sí mismos, sino en favor de la paz, "*lo que implica que el Polo no solo participe en las elecciones con candidatos propio, sino también con candidatos del abanico de la izquierda*", afirmó.
