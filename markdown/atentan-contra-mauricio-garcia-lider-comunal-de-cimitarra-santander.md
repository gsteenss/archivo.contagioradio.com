Title: Atentan contra Mauricio García, líder comunal de Cimitarra, Santander
Date: 2019-08-22 16:20
Author: CtgAdm
Category: Líderes sociales, Nacional
Tags: Atentados contra líderes sociales, Puerto Olaya, Santander
Slug: atentan-contra-mauricio-garcia-lider-comunal-de-cimitarra-santander
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/08/Mauricio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: archivo particular ] 

 El pasado 20 de agosto a las 10:15 de la noche, **Mauricio García González, integrante de la Junta de Acción Comunal de Carretera Vieja Puerto Olaya** y de la Asociación de Pescadores de esta población fue atacado en un intento de asesinato y herido en una de sus piernas, el líder que fue trasladado a un centro médico y se encuentra estable. Este es el segundo atentado contra líderes sociales en este corregimiento del municipio de Cimitarra.

Según información proporcionada por Jorge Iván Atuesta, personero de este municipio, los hechos ocurrieron cuando el dirigente comunal se disponía a entrar a su lugar de residencia y fue abordado por un hombre que lo esperaba afuera de su casa. [(Le puede interesar: En una semana amenazan dos veces la organización Fuerza Mujeres Wayúu)](https://archivo.contagioradio.com/amenazan-dos-veces-fuerza-mujeres-wayuu/)

Mauricio quien llegaba en su motocicleta acompañado de su esposa y su hija reaccionó a tiempo y alertó a la comunidad, sin embargo fue herido por dos disparos en su pierna izquierda antes de que el hombre armado huyera, mientras su familia resulto ilesa, Mauricio fue remitido al hospital de Puerto Berrío en Antioquia. [(Lea también: Lideresa social Mayerlis Angarita sobrevive a atentado en Barranquilla)](https://archivo.contagioradio.com/lideresa-social-mayerlis-angarita-sobrevive-a-atentado-en-barranquilla/)

Aunque las causas del ataque son motivo de investigación por parte de las autoridades, el personero señaló que podrían estar relacionadas con el conflicto por el empleo que hay en Puerto Olaya donde se proyecta construir para 2020 la primera refinería de petróleo privada del país y que generaría entre 350 y 400 empleos diarios.

Noticia en desarrollo...

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
