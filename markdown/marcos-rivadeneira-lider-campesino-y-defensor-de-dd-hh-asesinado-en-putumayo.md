Title: Marco Rivadeneira, líder campesino y defensor de DD.HH. asesinado en Putumayo
Date: 2020-03-19 22:45
Author: AdminContagio
Category: Nacional
Slug: marcos-rivadeneira-lider-campesino-y-defensor-de-dd-hh-asesinado-en-putumayo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/03/Líder-Marcos-Rivadeneira-1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:heading {"align":"right","level":6,"textColor":"cyan-bluish-gray"} -->

###### Foto: @camiloencisov {#foto-camiloencisov .has-cyan-bluish-gray-color .has-text-color .has-text-align-right}

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Este jueves se denunció el asesinato del histórico líder del Putumayo y defensor de derechos humanos Marcos Rivadeneira, que fue secuestrado por hombres armados mientras sostenía una reunión y minutos después, se informó de su homicidio cerca al lugar en el que estaba reunido. (Le puede interesar: ["En audiencia senatorial examinarán impactos del extractivismo en Putumayo"](https://archivo.contagioradio.com/en-audiencia-senatorial-examinaran-impactos-del-extractivismo-en-putumayo/))

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### **Marcos Rivadeneira, reconocido líder campesino y defensor de DD.HH.**

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

La Red de Derechos Humanos del Putumayo denunció que sobre las 2:30 de la tarde, mientras estaba en una reunión con campesinos de la vereda Nueva Granada en el corredor Puerto Vega - Teteyé- Puerto Asís, tres hombres vestidos de civil armados sacaron del lugar a Marcos Rivadeneira. Posteriormente se supo que había sido asesinado, y su cuerpo dejado cerca al lugar.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Marcos Rivadeneira era un reconocido dirigente político y social integrante del Congreso de los Pueblos, y presidente de la Asociación Campesina del Puerto Asís; asimismo, era vocero nacional de la Plataforma Colombia, Europa, Estados Unidos y miembro del Coordinador Nacional Agrario. (Le puede interesar: ["Se agudizan agresiones contra campesinos que se acogieron a plan de sustitución en Putumayo"](https://archivo.contagioradio.com/se-agudizan-agresiones-contra-campesinos-que-se-acogieron-a-plan-de-sustitucion-en-putumayo/))

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Contagio Radio se había comunicado con el líder en diferentes ocasiones, la útlima de ellas, en junio de 2019 para hablar sobre el Plan Nacional Integral de Sustitución (PNIS) y las pocas alternativas que tienen los campesinos en la zona, así como el incumplimiento del Plan por parte del Gobierno. (Le puede interesar:["La sustitución es cambiar una mata por otra, pero el Estado no lo está haciendo: campesinos de Putumayo "](https://archivo.contagioradio.com/la-sustitucion-es-cambiar-una-mata-por-otra-pero-el-estado-no-lo-esta-haciendo-campesinos-de-putumayo/))

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### **Putumayo, sin garantías para la vida en un territorio militarizado**

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Recientemente, la [Comisión Intereclesial de Justicia y Paz](https://www.justiciaypazcolombia.com/asesinatos-en-medio-de-la-presencia-militar-en-putumayo/) había denunciado el asesinato de un hombre en el municipio de Puerto Caicedo, presuntamente por hombres de la estructura armada conocida como 'La Mafia'. La organización señala que previamente se registraron otros asesinatos que involucraría a esta organización y al Frente Carolina Ramírez de FARC-EP.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Situaciones que ocurren en un territorio en el que hay una "fuerte presencia militar de la Brigada XXVII de Selva, de policía y la fuerza naval del sur". Razón por la que afirma que la respuesta del Estado está resultado ineficaz para garantizar la integridad física y psicológica de las comunidades rurales en Putumayo.

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78980} /-->
