Title: Lo represión del ESMAD marcó la jornada de movilizaciones estudiantiles
Date: 2019-10-26 08:47
Author: CtgAdm
Category: Estudiantes, Nacional
Tags: Educación Superior, ESMAD, universidad, Universidad del Atlántico
Slug: lo-represion-del-esmad-marco-la-jornada-de-movilizaciones-estudiantiles
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/WhatsApp-Image-2019-10-25-at-18.09.46-1.jpeg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/WhatsApp-Image-2019-10-25-at-18.09.42-2.jpeg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/WhatsApp-Image-2019-10-25-at-18.09.43-1-1.jpeg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/WhatsApp-Image-2019-10-25-at-18.09.43-3.jpeg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/WhatsApp-Image-2019-10-25-at-18.09.45-1.jpeg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/WhatsApp-Image-2019-10-25-at-18.09.45-2.jpeg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/WhatsApp-Image-2019-10-25-at-18.09.46-2.jpeg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:Aleyo Gómez] 

El pasado jueves, en el marco de la Semana por la Indignación, diferentes universidades desarrollaron movilizaciones en las principales capitales del país, no obstante, nuevamente la violencia ejercida por parte de efectivos del Escuadrón Móvil Antidisturbios (ESMAD) marcaron la jornada, dejando a una estudiante herida en uno de sus ojos y más de 60 detenidos en todo el país.

### **"Se mantiene el uso de material no convencional y el uso de recalzadas"** 

Valentina Ávila, integrante de la Red Distrital Universitaria de Derechos Humanos, señaló que diferentes universidades en Bogotá iniciaron su convocatoria a la movilización sobre las 11 de la mañana y en horas de la tarde se desplazaron hasta la sede principal de la Universidad Nacional de Colombia. Los jóvenes se manifestaban contra la violencia ejercida por parte de la Fuerza Pública la semana pasada contra el movimiento estudiantil, así como contra el artículo 44 del Presupuesto General de la Nación. Ver: [Movimiento estudiantil se alista para la semana por la indignación](https://archivo.contagioradio.com/movimiento-estudiantil-se-alista-para-la-semana-por-la-indignacion/)

Luego que la marcha llegara a la Universidad, realizaron un plantón sobre la Calle 26 y en ese momento inició la intervención del ESMAD. Según Ávila, integrantes de la red pudieron evidenciar que "se mantiene el uso de material no convencional y el uso de recalzadas", de igual forma, los defensores de DD.HH., denunciaron la presencia del Grupo de Operaciones Especiales (GOES) de la Policía, un cuerpo armado que no tiene por objetivo intervenir en el marco de la protesta social.

La estudiante manifestó que desde la Red no habían podido consolidar información sobre las personas detenidas, ni heridas, debido a que aún no hay un consolidado de los equipos de la Red que estuvieron haciendo presencia, y por la cantidad de información falsa o sin verificar que se transmite a través de redes sociales.

\[caption id="attachment\_75558" align="aligncenter" width="1024"\]![Aleyo Gómez - Bogotá U.Nacional](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/WhatsApp-Image-2019-10-25-at-18.09.42-2-1024x683.jpeg){.wp-image-75558 .size-large width="1024" height="683"} Aleyo Gómez -U.Nacional Bogotá\[/caption\]

### **Universidad del Atlántico: Más de 50 heridos, 53 estudiantes detenidos** 

El pasado jueves en la Universidad del Atlántico se conmemoraban 13 años del asesinato de 4 estudiantes de la Universidad, a causa de una explosión que ocurrió en las canchas de la Universidad, en medio de hechos aún sin esclarecer. El acto conmemorativo incluía una ceremonia religiosa y un plantón pacífico, pero cuando culminó el primer acto, un incendio en el 'Bloque D' de la universidad interrumpió la normalidad. Ver: [ESMAD usó material no convencional y violó protocolos en movilizaciones estudiantiles](https://archivo.contagioradio.com/esmad-uso-material-no-convencional-y-violo-protocolos-en-movilizaciones-estudiantiles/)

Según Fabián Salcedo, estudiante de la Universidad, los jóvenes intentaron ingresar al edificio para apagar las llamas, pero encontraron las puertas cerradas, y de acuerdo a testigos del hecho, personas externas a la Institución podrían ser las causantes de la conflagración. Salcedo señaló que luego que fueron apagadas las llamas, los estudiantes encontraron mensajes amenazantes escritos en los tableros de los edificios contra la comunidad universitaria.

Luego que se presentaran estos hechos, algunos estudiantes buscaron realizar el plantón, pero fueron reprimidos por el ESMAD y obligados a regresar al interior del campus, lo que provocó una confrontación que se mantuvo hasta cerca de las 5 de la tarde. En ese momento, el rector de la Universidad, Carlos Prasca, ordenó el ingreso de los cuerpos de Policía a la Institución, argumentando que se estaban generando destrozos.

Los estudiantes por su parte señalaron que al interior de la Institución habían personas infiltradas, y denunciaron que tras el ingreso de la Policía fueron detenidas 53 personas, algunas de ellas, que no estaban participando de los actos. Uno de los casos denunciados fue el de la joven Camila García, diagnosticada con hipertensión pulmonar primaria, lo que la obliga a necesitar oxígeno constantemente, pero fue detenida sin justa causa, y sin las debidas medidas para proteger su salud, pese a expresar su condición médica.

Otras organizaciones estudiantiles señalaron que en la Universidad se presentaron cerca de 30 personas heridas por la acción de la fuerza pública, por esa razón, este viernes se está desarrollando en la Institución una asamblea universitaria en la que se evaluará lo ocurrido y se hará un llamado a paro, exigiendo la salida del rector Prasca de la Institución.

\[caption id="attachment\_75561" align="aligncenter" width="1024"\]![Aleyo Gómez -U.Nacional Bogotá ](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/WhatsApp-Image-2019-10-25-at-18.09.45-1-1024x682.jpeg){.wp-image-75561 .size-large width="1024" height="682"} Aleyo Gómez -U.Nacional Bogotá\[/caption\]

### **Universidad de Nariño y la Universidad Pedagógica y Tecnológica de Colombia** 

En la Universidad de Nariño, sede Panamericana, los jóvenes se manifestaron el jueves en consonancia con lo planteado por el movimiento estudiantil en otras partes del país. Los estudiantes de la Institución denunciaron que allí, por acción del ESMAD, una joven resultó herida en un ojo. En hechos similares, organizaciones estudiantiles denunciaron que tras la movilización de estudiantes de la Universidad Pedagógica y Tecnológica de Colombia (UPTC), 5 personas resultaron detenidas y 1 más, herida.

\[caption id="attachment\_75557" align="aligncenter" width="1024"\]![Aleyo Gómez -U.Nacional Bogotá ](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/WhatsApp-Image-2019-10-25-at-18.09.46-1-1024x682.jpeg){.wp-image-75557 .size-large width="1024" height="682"} Aleyo Gómez -U.Nacional Bogotá\[/caption\]

### **Universidad Industrial de Santander: Proponiendo banderas para el movimiento estudiantil** 

Anderson Arboleda, representante estudiantil de la Universidad Industrial de Santander (UIS), institución que impulsó la propuesta de la Semana por la Indignación explicó que la misma tenía como sentido poner en alerta máxima al movimiento estudiantil respecto al Artículo 44 del Presupuesto General de la Nación, que obligaría a las universidades públicas a pagar las demandas que pierda la Nación. Así mismo, invitando a pensar las banderas que puedan volver a unir a todo el movimiento estudiantil.

Para Arboleda, dichas banderas deberían estar orientadas a una "transformación profunda de la política del sistema educativo", para que se reformule la Ley de educación superior y se financie la oferta y no la demanda, es decir "que el presupuesto de Generación E y el ICETEX pase a las universidades públicas. Adicionalmente, el representante declaró que también esperan generar, en la asamblea que se desarrollará este viernes, una propuesta de articulación para el movimiento estudiantil que defina una plataforma desde la cual puedan realizarse acciones conjuntas.

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe><iframe id="audio_44015969" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_44015969_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
