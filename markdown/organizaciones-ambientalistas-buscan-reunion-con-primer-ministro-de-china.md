Title: Las amenazas de la inversión de China en Colombia
Date: 2015-05-25 17:06
Category: Ambiente, Economía
Tags: ASOQUIMBO, Cormagdalena, hidroeléctricas en Colombia, Hydrochina, Plan de Aprovechamiento del Río Magdalena, Privatización del Río Magdalena
Slug: organizaciones-ambientalistas-buscan-reunion-con-primer-ministro-de-china
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/05/arton15762.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### Foto: [www.prensarural.org]

<iframe src="http://www.ivoox.com/player_ek_4546780_2_1.html?data=lZqhmJycdI6ZmKiak5iJd6KllJKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRk9PbwtPW3MbHrdDixtiYw9LGrcbi1cbZy9jYpdSfxtPjh6iXaaK4wtOYxcbWuMKfwpC91M7RqdOfrs6ah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Tatiana Roa, Coordinadora Censat Agua Viva] 

Varias organizaciones ambientalistas colombianas que trabajan en torno a la problemática del Río Magdalena, enviaron una carta al Primer Ministro de la República Popular de China, Li Keqiang, con el objetivo de buscar una reunión para hablar con él sobre el proyecto de formulación del **Plan Maestro de Aprovechamiento del Rio Magdalena,** teniendo en cuenta  que  el riesgo de este proyecto es "cómo se está entendiendo el desarrollo del país", dice Tatiana Roa, Coordinadora general de Censat Agua Viva.

“**Queremos conocer más entorno a las inversiones que ellos hacen en Colombia como Hydrochina;**  también sobre las microcentrales hidroeléctricas que quieren construir en la cuenca alta del Río Magdalena y cómo se está planeando el tema de navegación del río, que es un proyecto en una asociación público privada” dice Roa, quien agrega que la carta no ha sido respondida.

Particularmente a las organizaciones les interesa la postura del **Primer Ministro, ya que ha planteado una crítica frente al modelo de desarrollo** que ha generado altos niveles de  contaminación ambiental, por lo que desean hablar sobre su perspectiva de cómo entiende la relación ambiente, comunidad y desarrollo.

Este tipo de proyectos en nombre del desarrollo generan diferentes riesgos para el país, ya que se está pensado el Río Magdalena como una mercancía y un canal para la navegabilidad, por lo que se pretende volver una autopista donde se movería el 80% de la carga que se mueve en los puertos. **Carbón, caña aceitera y petróleo, serían los materiales que se movilizan en el río,** de manera que si se presenta algún tipo de accidente, se pondría en riesgo el ambiente, y además se generan efectos sociales y económicos para las comunidades que viven alrededor del río.

Cabe recordar que el Plan Maestro de Aprovechamiento del Río Magdalena planea la construcción de **19 represas para el año 2020, las cuales implicarían la reubicación de varias comunidades indígenas, campesinas y pesqueras. **Es por eso, que las comunidades continúan movilizándose en resistencia al Plan, por lo que la siguiente manifestación empezará en agosto en La Dorada (Caldas) y concluirá en Barranquilla el 12 de octubre. [(Ver nota relacionada)](https://archivo.contagioradio.com/plan-maestro-de-aprovechamiento-del-rio-magdalena-cuando-se-hizo-y-por-quienes/)

[![Carta-Primer-ministro-China (1)](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/05/Carta-Primer-ministro-China-1.png){.aligncenter .size-full .wp-image-9129 width="757" height="2000"}](https://archivo.contagioradio.com/organizaciones-ambientalistas-buscan-reunion-con-primer-ministro-de-china/carta-primer-ministro-china-1/)
