Title: Se preparan 8 capturas más por el crimen de Diego Felipe Becerra
Date: 2015-08-13 16:35
Category: DDHH, Judicial
Tags: Diego Felipe Becerra, grafitero, Hector Hernando Ruiz, INPEC, Jaime Gutierrez, Policía Colombiana
Slug: se-preparan-8-capturas-mas-por-el-crimen-de-diego-felipe-becerra
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/08/2nd-saturday-073.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: [jacquelinemhadel.wordpress.com]

###### [13 Ago 2015] 

Se cumplieron 4 años del crimen del joven, **Diego Felipe Becerra**, conocido como "el Grafitero", asesinado el 10 de Agosto de 2011, homicidio que  trató de encubrirse por parte de la policía alterando la escena del crimen y ajustando falsas denuncias que mostraban a Diego Felipe como una persona que había atracado un bus y que luego le habría disparado a los policías que lo perseguían.

**Hector Hernando Ruiz, abogado del policía implicado** en el caso de Diego Felipe Becerra, denunció que fue violentado por un agente del INPEC que lo estaba trasladando a su celda después de tener una audiencia en la Fiscalía e indicó que deben ser capturadas aproximadamente cuarenta personas “*discriminadas entre personal de vigilancia, investigación criminal y personal de inteligencia al interior de la Policía Nacional*”. Después de estas declaraciones ocurrió la agresión.

En este caso hasta ahora 13 personas han sido implicadas y capturadas, entre ellos miembros de la fuerza pública. La abogada de victimas quien maneja el caso, declaró que  “con las declaraciones del abogado se evidencia la participación de más uniformados en el crimen del graffitero”, por lo cual podrían presentarse 8 nuevas capturas en los próximos días.

### **El 31 de Agosto se iniciaría juicio contra patrullero Wilmer Alarcón** 

Después de 31 meses de audiencia preparatoria se espera que se inicie el juicio contra el patrullero Alarcón acusado de ser la persona que disparó contra Diego Felipe. Según la familia del grafitero hay muchas dilaciones en el caso.

La audiencia de acusación inició el 7 de Mayo de 2012 y terminó 6 meses después el 5 de Octubre, la audiencia preparatoria desde Diciembre de 2012 hasta Junio de 2015. Fueron 17 audiencias preparatorias, 2 apelaciones, 7 aplazamientos solicitados por la defensa de Alarcón, 67 inasistencias del abogado Jaime Gutierrez hasta el punto que el juez ordenó el arresto por 48 horas contra el jurista por dilaciones injustificadas. Dentro del caso también se estarían preparando 8 capturas más.
