Title: ‘Breaking News’ y el caso Uribe en medios tradicionales
Date: 2020-08-20 16:10
Author: AdminContagio
Category: Actualidad, Política
Tags: Álvaro Uribe, Corte Suprema de Justicia, Medios tradicionales
Slug: breaking-news-y-el-caso-uribe-en-medios-tradicionales
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/08/Diseno-sin-titulo-4.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: @Alvaro Uribe Vel / @VickyDavilaH

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

La entrevista realizada hace unos días por **Vicky Dávila y María Isabel Rueda para Semana** al exsenador Álvaro Uribe, detenido de manera preventiva mientras la Corte Suprema de Justicia avanza en la investigación que lo vincula a supuesta manipulación de testigos, dejó, para muchos, entrever no solo afirmaciones del expresidente que no corresponden a la información recopilada por el auto de la Corte, sino la posición sesgada que han asumido grupos editoriales y periodistas frente al caso, pese a su compromiso con la información.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

**Fabiola León, analista de medios y defensora de la libertad de prensa,** advierte que en medio del diálogo usado en dicha entrevista, no existen preguntas concretas sobre el hecho del que se acusa a Uribe, mientras el exsenador “responde siempre hablando de otras personas, las periodistas nunca le insisten en su responsabilidad o su actuación en el caso concreto”, que hoy investiga la Corte Suprema.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### "Como usted quiera presidente. Yo no tengo problema con eso"

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

A su juicio, el segmento de casi tres horas, “terminó siendo una conversación, alejándose del género periodístico”,  parten de unas posturas concretas frente al Gobierno y la "inflexión de la voz sugiere que hay una indignación hacia quienes critican al gobierno Duque" incluida la Corte, además, señala hay demasiadas preguntas de opinión.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Respecto a si la entrevista se apegó a la ley, explica que solo un juez podría restringir el uso de redes o de entrevistas, sumado a que las condiciones dentro de un centro penitenciario son diferentes a las que existen en la modalidad de casa por cárcel por lo que al INPEC por ahora solo se le ha ordenado una reseña y visitas periódicas.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

“Semana, especialmente en este año fue clara en su apuesta comercial de periodismo. El grupo Gillinski invirtió miles de millones en el canal digital para el cual fue pertinente la contratación de Victoria Dávila por su estilo y afinidades políticas, con la misma idea vincula a María Isabel Rueda”, interpreta León  **al agregar que medios como Semana vienen priorizando el ‘Breaking News’ al periodismo de investigación**.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/CarlosCortes/status/1295549719876374528","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/CarlosCortes/status/1295549719876374528

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph {"align":"justify"} -->

La postura asumida por Semana y el Grupo Gilinkski propietario del 50% del grupo editorial no solo ha quedado en evidencia en la entrevista al expresidente, columnas de opinión de Vicky Dávila como 'Álvaro Uribe libre' la salida definitiva del también columnista Daniel Coronell y la suspensión de las publicaciones impresas de Arcadia, dan cuenta de ello, además de la vinculación de otros periodistas y analistas como Salud Hernández, Luis Carlos Vélez y afines al partido de Gobierno.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Al respecto, Daniel Coronell ha advertido sobre la posición de **Sandra Suarez, quien está al frente de la gerencia de la Revista Semana y además formó parte del equipo para la campaña presidencial de Álvaro Uribe** para luego ser ministra de Ambiente y Vivienda y quien fue una de las firmantes de carta en respaldo al exsenador y líder del Centro Democrático.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/DCoronell/status/1290380605885231112","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/DCoronell/status/1290380605885231112

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:heading {"level":3} -->

### La cercanía de la fuente y el periodista

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Hace unas semanas fue dado a conocer el Auto de la Corte Suprema de Justicia que definió la situación jurídica del exsenador, en su interior fueron citadas conversaciones entre, al menos, cinco periodistas y sus fuentes, incluido Julio Sánchez Cristo, director de La W Radio.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Al respecto, **[la Fundación para la Libertad de Prensa (FLIP)](https://flip.org.co/index.php/es/informacion/pronunciamientos/item/2561-consideraciones-de-la-flip-sobre-la-exposicion-indebida-de-periodistas-y-sus-fuentes)**resaltó que ante la falta de claridad en la información conocida hasta ahora por la Corte se sugirió la existencia de "elementos de complicidad de los periodistas con las conductas investigadas" por lo que invitaron a la Corte a evaluar y hacer una aclaración sobre las menciones de periodistas en su decisión. [(Lea también: La estrategia mediática de los implicados en el caso Uribe Vélez)](https://archivo.contagioradio.com/la-estrategia-mediatica-de-los-implicados-en-el-caso-uribe/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Aunque la **Corte Constitucional ha reconocido que un elemento fundamental de la libertad de prensa es la libertad de fuentes** y en el sistema constitucional colombiano no hay fuentes prohibidas, como resalta la FLIP, se abre el debate sobre la relación entre la fuente y el periodista y el rol del periodismo en la manipulación de la investigación y su interés público.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

“Democratizar la información abarca la inclusión de lo plural y lo diverso. La diversidad dice varias orillas, no basta con entrevistar 20 fuentes sino con tener 5 con diferente postura”, afirma León, quien resalta que pese a ello un periodista o un medio tienen la libertad de elegir a sus entrevistados, salvedad hecha ante la petición que se ha hecho en redes de brindar el mismo espacio a Iván Cepeda, contraparte en el caso que vincula al exsenador Uribe.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### El Estado de Opinión, un viejo conocido de Álvaro Uribe

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Durante su intervención, Álvaro Uribe afirmó "estar secuestrado" por las altas cortes y propuso un referendo para modificar la justicia, la propuesta , que buscaría desmontar la rama judicial y finalizar de paso con la Jurisdicción Especial para la Paz (JEP) creando una única corte bajo la influencia del ejecutivo; viene de la mano desde hace meses vinculada a legitimar un ‘Estado de Opinión’, un término usado durante su Gobierno por el actual senador.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Al respecto, exmagistrados como Alfredo Beltrán han manifestado sobre el Estado de Opinión que de plantearse este referendo las cortes serían «suprimidas de tajo para ser reemplazadas por una que tendría profunda influencia del Ejecutivo», lo que dista de ser un Estado de derecho. [(Referendo uribista va más allá de una simple venganza contra el sistema judicial: Beltrán)](https://archivo.contagioradio.com/referendo-uribista-va-mas-alla-de-una-simple-venganza-contra-el-sistema-judicial-beltran/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

“Uribe apeló durante su Gobierno al llamado Estado de Opinión, con el que justificó cualquier acto de su vida pública o privada con sus niveles de popularidad; pretendió ponerse más allá de la ley  y la justicia” resalta la analista de medios. [(Le puede interesar: Uribe ha elegido la vía de la presión mediática y política contra el poder judicial: Iván Cepeda)](https://archivo.contagioradio.com/uribe-ha-elegido-la-via-de-la-presion-mediatica-y-politica-contra-el-poder-judicial-ivan-cepeda/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

En medio de su análisis, Fabiola León advierte que parte de la estrategia que el exsenador y su partido utilizan es una mención a un Chavismo “peligroso” y a relacionarlo con la izquierda colombiana, de lo que no existe evidencia. **"Es difícil saber cuál será la última carta cuando una persona acostumbrada a contar con tanto poder, se ve cercada por asuntos judiciales. En el caso concreto de Uribe, siempre ha jugado un rol importante el manejo del discurso**". [(Le puede interesar: La estrategia del uribismo que busca lavar la imagen de Andrés Felipe Arias)](https://archivo.contagioradio.com/uribismo-andres-felipe-arias/)

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
