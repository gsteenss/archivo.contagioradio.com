Title: ¿Cómo evitar que Ministerio de Ciencia, Tecnología e Innovación se convierta en fortín político?
Date: 2018-12-18 17:00
Author: AdminContagio
Category: Educación, Entrevistas
Tags: Colciencias, Investigación, Ministerio de Ciencia y Tecnología, politica
Slug: como-evitar-que-ministerio-de-ciencia-tecnologia-e-innovacion-se-convierta-en-fortin-politico
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/03/investigación-e1503513943790.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Protocolo Foreign Affairs & Lifestyle] 

###### [18 Dic 2018] 

Con una votación de  109 los congresistas en plenaria del senado aprobaron la creación del Ministerio de Ciencia, Tecnología e Innovación que entrará a suplir las actividades de Colciencias. Sin embargo, las dudas sobre el funcionamiento de este Ministerio y **si será una cuota política, son apenas algunas de las preguntas que rodean a esta nueva entidad.**

Gabriela Delgada, PhD en Ciencias Farmacéuticas, docente del departamento de Farmacia de la  Universidad Nacional y ex directora de fomento e investigación Colciencias, manifestó que **esta iniciativa de crear un Ministerio de Ciencia, Tecnología e Innovación, se había planteado desde hace 5 años**, pero no había contado con el respaldo de los congresistas. (Le puede interesar: ["Recursos de Ciencia y Tecnología no se están invirtiendo en investigación"](https://archivo.contagioradio.com/fondo-de-ciencia-y-tecnologia/))

Asimismo indicó que la importancia de este cambio radicaba en la necesidad de garantizar un presupuesto nacional para la investigación, que en los últimos años ha venido siendo más reducido, **garantizar un papel  fundamental de la Ciencia en la gobernabilidad del país y ser el principal garante de la construcción de políticas estatales.**

### **Min. Ciencia Tecnología e Innovación no puede ser un fortín político** 

Finalmente ahora que ha sido avalada la creación de esta entidad, Delgado afirmó que las expectativas son altas, "transformar a Colciencias en Ministerio no nos garantiza tener una mejor institucionalidad para el país y mucho menos tener un sistema de Ciencia y Tecnología como el que el país necesita" y **agregó que para que este escenario no se convierta en un fortín político deberá tener un funcionamiento magistral**.

Delgado resaltó la importancia de que quienes lo conformen, sean personas que tengan una amplia trayectoria en el campo de la investigación y que comprendan la labor de la Ciencia para generar nuevo conocimiento. Razón por la cual el sindicato de Colciencias propuso que la **escogencia de las personas que trabajarán en el Ministerio se realice a través de carrera laboral o meritocracia**.

"Necesitamos que este sea un Ministerio ejemplar, que no sea el de más cargos burocráticos, que no sea otro fortín politiquero, que ya lo habíamos visto desde que Colciencias se conformó en un departamento administrativos, esperamos que allá lleguen personas por merito suficiente, que sea líder en las políticas de Estado y que tenga recursos" afirmó la docente.

<iframe id="audio_30886678" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_30886678_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
