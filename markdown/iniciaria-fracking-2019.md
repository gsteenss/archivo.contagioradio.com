Title: En tres departamentos de Colombia iniciaría el fracking en 2019
Date: 2018-12-12 17:45
Author: AdminContagio
Category: Ambiente, Entrevistas
Tags: Duque, fracking, GEAM, petroleo
Slug: iniciaria-fracking-2019
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/06/WhatsApp-Image-2018-03-26-at-12.01.58-PM-1-e1544654159516.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [12 Dic 2018] 

**Solo faltaría la autorización del Gobierno Duque para iniciar pilotos de fracking en 3 departamentos**; aunque el Consejo de Estado suspendió los actos administrativos que fijaron los términos para realizar exploración y explotación de Yacimientos No Convencionales (YNC), ambientalistas advierten que la medida solo sirve para dar tiempo a la comisión de expertos creada para analizar el tema que avalaría la práctica.

Luis Miguel Morelli, director de la Agencia Nacional de Hidrocarburos (ANH), dijo recientemente que **solo faltaba la aprobación de la comisión de expertos creada por los Ministerios de Minas y Energía, y de Ambiente, para dar vía libre a pilotos de fracking**, o fracturamiento hidráulico en La Guajira, Cesar y Santander, afirmación ha causado revuelo entre los grupos ambientalistas que se oponen al uso de esta técnica para la extracción de hidrocarburos.

Para el **integrante del Grupo de Estudios Extractivos, Ambientales y Sociales del Magdalena Medio (GEAM) Óscar Samapayo**, esta noticia es lamentable, porque en un país como Colombia, que tiene una “incalculable riqueza de fauna y flora, de biodiversidad e hídrica, que se use está técnica generará una afectación evidente a la salud humana y el ambiente”. (Le puede interesar: ["14 municipios de Boyacá ganaron la pelea contra el Fracking"](https://archivo.contagioradio.com/boyaca-contra-fracking-sismica/))

Sampayo recordó que durante 100 años, en el país se han extraído hidrocarburos de rocas almacenadoras, que son de fácil acceso porque se perforan verticalmente a profundidades de 2 km; ahora con el fracturamiento hidráulico se perforarán YNC de rocas generadoras a profundidades entre 3 y 6 km. **El ambientalista aseguró que es probable que de estos pilotos se encuentre petróleo, porque “si hay roca almacenadora, es porque abajo hay roca generadora”**.

El integrante del GEAM sostuvo que esta técnica es intensiva, razón por la que se deben performar miles de pozos, para que sea posible crear yacimientos horizontales que permitan la extracción del crudo. Sampayo explicó que dicha técnica se realizará en la formación La Luna, que se extiende por el Magdalena Medio; así como en el Cesar y La Guajira, donde se realizará en mantos de carbón de los que se espera obtener gas.

### **El petróleo que se extraerá en Santander, no se puede refinar en Barrancabermeja** 

Sampayo expresó que en Santander se espera obtener petróleo, pero dadas las condiciones geológicas de la zona, sería de bajo octanaje. Este tipo de crudo, como explica el ambientalista, no sirve para producir combustible como diésel o combustibles para jet. Adicionalmente, la refinería de Barrancabermeja se reconvirtió para procesar petróleos semipesados o pesados, **razón por la que el material que se explote en la zona, probablemente sería exportado**.

A ello se suma la posible afectación ambiental causada por la exploración y explotación minera, pues como lo han evidenciado en el GEAM, **la extracción de hidrocarburos en yacimientos convencionales causa “impactos incalculables**, sabemos que la recuperación secundaria y terciaria inyecta ingentes cantidades de agua con químicos para estimular estos yacimientos, y eso ha producido destrucción y desastre”.

Aunque en Colombia el fracturamiento hidráulico no ha sido causante de problemas porque aún no se ha implementado, la extracción en **yacimientos convencionales ya ha causado afloramientos de residuos en la zona del Magadalena Medio**, por lo que Sampayó asegura que no quieren esperar a que llegue el fracking para ver si incrementa la contaminación.

### **Decisiones técnicas que son más bien políticas** 

Haciendo un pronóstico futuro de lo que podría pasar con las medidas jurídicas y legales que se están tomando para evaluar los efectos que podría conllevar la explotación de YNC, Sampayo aseveró que “el show del fracking iniciará en Marzo o Abril de 2019”, porque la decisión del Consejo de Estado de suspender el Decreto 3004 de 2013 y la Resolución Nº 90341 de 2014 que viabilizaban la técnica, **fue solo un tiempo de espera para que el Gobierno tomara la decisión**.

Sería el resultado de la investigación de la comisión de expertos la que determinaría la elección del Gobierno, pero Sampayo señaló que de los **14 técnicos contratados, 9 tienen conflictos de interés porque han trabajado o tienen relaciones con multinacionales que extraen hidrocarburos**. Además, porque dicha Comisión fue activada en noviembre y entregará resultados en enero, es decir en solo 3 meses.

Sampayo anuncio que desde la Alianza Colombia Libre de fracking, también se generarán conceptos de geólogos, toxicólogos, ingenieros de petróleos e hidrogeólos, que conocen muy bien el territorio y encuentran que **realizar fracking en Santander, “segundo nido sísmico más activo del planeta”, es una decisión política con posibles consecuencias negativas**.

### **La avanzada minero-energética no se detiene, los ambientalistas tampoco** 

El activista añadió que otro tema relevante es el proyecto de Ley que busca prohibir el fracking, y que fue aplazado para marzo, hecho que contrasta con el avance que según Sampayo, buscarían hacer las empresas extractivistas a zonas como el Catatumbo, Tolima, Huila, y finalmente al Putumayo. (Le puede interesar:["Aplazada discusión del Proyecto de Ley que prohibe el fracking en Colombia"](https://archivo.contagioradio.com/aplazada-discusion-proyecto-de-ley-prohibe-fracking-en-colombia/))

No obstante, el integrante del GEAM también anticipó que formularan una queja ante la Procuraduría y la Agencia Nacional de Licencias Ambientales (ANLA), por hacer “desarrollos de fracking sin piso jurídico”, a cargo de empresas como Ecopetrol, Parex Resources, Exxon Movil, Conoco Philips y Drummond. (Le puede interesar: ["No es posible hacer fracking de forma responsable"](https://archivo.contagioradio.com/no-es-fracking-responsable/))

<iframe id="audio_30760495" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_30760495_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
