Title: La presunción de inocencia no aplica con integrantes del Movimiento social
Date: 2020-08-07 08:37
Author: AdminContagio
Category: Nacional
Tags: Alvaro Uribe, David Ravelo, Iván Duque, miguel angel beltran
Slug: la-presuncion-de-inocencia-no-aplica-con-integrantes-del-movimiento-social
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/08/Álvaro-Uribe-y-Duque.jpeg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/08/Miguel-Angel-Beltran.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/08/David-Ravelo-Crespo.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

Mientras el presidente Iván Duque y varios funcionarios públicos del partido de gobierno exigen presunción de inocencia a favor de Álvaro Uribe, miles de colombianos y colombianas padecen de la estigmatización y no gozan de dicha presunción de inocencia. Algunos en las cárceles, sin juicio y sin condena, y otros ya en libertad después de haber logrado demostrar las falsas acusaciones. Les compartimos dos ejemplos.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/IvanDuque/status/1290755832330813442","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/IvanDuque/status/1290755832330813442

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

No se escucharon voces con similar ahínco por parte de la institucionalidad representada en el Gobierno, frente a las capturas y juicios injustos de los que denunciaron haber sido víctimas, por ejemplo el defensor de derechos humanos David Ravelo Crespo o el docente universitario Miguel Ángel Beltrán; en cuyos casos ha sido comprobado en instancias superiores de la justicia que fueron mal enjuiciados e injustamente condenados. **Por el contrario, según denuncian los dos su persecución fue promovida desde los gobiernos del entonces presidente Uribe.**

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### El caso de David Ravelo inició con una denuncia a Álvaro Uribe

<!-- /wp:heading -->

<!-- wp:image {"id":87977,"sizeSlug":"large"} -->

<figure class="wp-block-image size-large">
![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/08/David-Ravelo-Crespo-1024x568.jpg){.wp-image-87977}  

<figcaption>
David Ravelo Foto: PBI

</figcaption>
</figure>
<!-- /wp:image -->

<!-- wp:paragraph -->

David Ravelo Crespo, por ejemplo, aseguró que su persecución por parte del Estado inició en junio de 2007 cuando divulgó ante los medios de comunicación **un video en el que aparecía Álvaro Uribe Vélez, reunido en Puerto Berrio, Antioquia con varios paramilitares de Barrancabermeja, Santander.** A partir de ahí, según aseguró Ravelo, fue víctima inicialmente de un plan paramilitar para acabar con su vida y posteriormente de un montaje judicial en el que buscaron incriminarlo por nexos con insurgencias de las FARC.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

**David Ravelo señaló que todo su proceso fue un «*mar de arbitrariedades*»**,  que fue condenado por dos falsos testimonios, uno de ellos el del exparamilitar Mario Jaimes Mejía, alias El Panadero; que el Fiscal de su caso, William Gildardo Pacheco Granados,  fue posteriormente condenado por el delito de desaparición forzada; que la juez que asumió su caso fue remplazada intempestivamente en medio del proceso; y que tras toda una investigación, **el Juzgado 9° Penal del Circuito de Bucaramanga acogió la imputación que posteriormente hizo la Fiscalía en contra del declarante alias El Panadero por fraude procesal en concurso con falso testimonio. Pese a lo cual, su fallo condenatorio no fue revocado y se vio obligado a pagar siete años en prisión.** (Lea también: ["Seguiré trabajando por demostrar que fui víctima de un falso positivo judicial": David Ravelo](https://archivo.contagioradio.com/seguire-trabajando-por-demostrar-que-fui-victima-de-un-falso-positivo-judicial-david-ravelo/))

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/IvanCepedaCast/status/404250992763756544","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/IvanCepedaCast/status/404250992763756544

</div>

<figcaption>
El senador [Iván Cepeda](https://twitter.com/IvanCepedaCast) se pronunció frente al caso en 2013

</figcaption>
</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:heading {"level":3} -->

### El caso de Miguel Ángel Beltrán

<!-- /wp:heading -->

<!-- wp:image {"id":87976,"sizeSlug":"large"} -->

<figure class="wp-block-image size-large">
![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/08/Miguel-Angel-Beltran.jpg){.wp-image-87976}  

<figcaption>
Miguel Angel Beltrán Foto: Sin Fronteras

</figcaption>
</figure>
<!-- /wp:image -->

<!-- wp:paragraph -->

Con una génesis similar, en la que estuvo presente como protagonista Álvaro Uribe quien el 23 de mayo de 2009  anunció la captura de Miguel Ángel Bernal reseñándolo como “*uno de los terroristas más peligrosos de la organización narcoterrorista de las FARC*” y atribuyéndole el alias de «*Cienfuegos*».

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Según Bernal, luego de haber sido objeto de un allanamiento ilegal en su oficina de la Universidad Nacional de Colombia, decidió hacer un posdoctorado en la Universidad Nacional de México, pero allí **había un acuerdo, del que no sabía pero después lo llegó a conocer entre el gobierno de Felipe Calderón y el de Álvaro Uribe para hacer un operativo en el que le dieron captura presentándolo ante los medios de comunicación como un «*peligroso terrorista*».** (Lea también: **[En Colombia se encarcela el pensamiento crítico](https://archivo.contagioradio.com/pensamiento-critico/)**)

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En su momento la Fiscalía General lo acusó de ser alias Jaime Cienfuegos, presunto coautor de los delitos de administración de recursos relacionados con el terrorismo, concierto para delinquir agravado y rebelión. Inicialmente, fue detenido por dos años mientras transcurría el proceso, en primera instancia fue absuelto por el Juzgado 4° Penal del Circuito de Bogotá; pero la Fiscalía impugnó la decisión, y el Tribunal Superior de Bogotá revocó parcialmente la sentencia absolutoria en octubre de 2014, hecho por el que fue capturado nuevamente.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

**En el año 2016 fue absuelto definitivamente por la Corte Suprema quien argumentó que todo el proceso que llevó a su captura y condena había sido ilegal.** No obstante, **Miguel Angel, pasó en total cuatro años recluido en prisión**, además de ser destituido de su cargo como profesor universitario por orden de la Procuraduría como sanción disciplinaria ante la condena impuesta.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Miguel Ángel Bernal, pese a ser un servidor público ejemplar no se le permitió defenderse en libertad, pero ninguna voz institucional abogó por él; según señala, fue todo lo contrario. (Lea también: **[Docentes de Universidades Públicas denuncian montaje judicial](https://archivo.contagioradio.com/docentes-de-universidades-publicas-denuncian-montaje-judicial/)**)

<!-- /wp:paragraph -->

<!-- wp:quote -->

> **«Las causas por las cuales terminé en reclusión en una cárcel de máxima seguridad fue como parte de la persecución que se intensificó durante el gobierno del entonces presidente Álvaro Uribe Vélez»**
>
> <cite>Miguel Ángel Bernal, docente universitario </cite>

<!-- /wp:quote -->

<!-- wp:core-embed/youtube {"url":"https://www.youtube.com/watch?v=Ozfkuxvu46w","type":"video","providerNameSlug":"youtube","className":"wp-embed-aspect-16-9 wp-has-aspect-ratio"} -->

<figure class="wp-block-embed-youtube wp-block-embed is-type-video is-provider-youtube wp-embed-aspect-16-9 wp-has-aspect-ratio">
<div class="wp-block-embed__wrapper">

https://www.youtube.com/watch?v=Ozfkuxvu46w

</div>

</figure>
<!-- /wp:core-embed/youtube -->

<!-- wp:block {"ref":78955} /-->
