Title: "Se ha perdido el objetivo central y es erradicar el matoneo en los colegios" ONU
Date: 2016-08-19 17:51
Category: Entrevistas
Tags: discriminación en Colombia
Slug: hay-que-sumar-esfuerzos-para-atacar-la-discriminacion-de-raiz-onu
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/08/notiagen.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Notiagen] 

###### [19 Ago 2016] 

Después de la polémica  generada por las cartillas que serían una herramienta para implementar la educación sexual en los colegios, como lo dispuso un fallo de la Corte Constitucional, aún no se ha pronunciado el Ministerio de Educación sobre cuál será la forma como se acatará la decisión de la Corte, sin embargo[la desinformación que rodea este tema generó que se perdiera de vista el objetivo central que era erradicar el matoneo en los colegios](https://archivo.contagioradio.com/lo-que-hay-es-un-miedo-profundo-a-pensarse-la-sociedad-mas-alla-de-lo-establecido/), como lo asegura Guillermo Fernández, el representante adjunto de la comisión de Derechos Humanos de Naciones Unidas para Colombia.

Una de las organizaciones que estuvo en este debate fue la ONU, como organismo que hizo parte del convenio suscrito con el Ministerio y Colombia Diversa, con el fin de **generar una cartilla que tendría preguntas orientadoras para establecer si los manuales de convivencia promueven o no la discriminación en los colegios.**

De acuerdo con Guillermo Fernández, [se perdió el tema de fondo que es la agresión en contra de los niños y niñas en los colegios](https://archivo.contagioradio.com/la-movilizacion-de-ayer-se-baso-en-mentiras-y-manipulaciones-fernando-giraldo/)no solo por su orientación sexual sino por razones socio-económicas. Una situación que ha generado que el índice de suicidios en los menores de edad siga en aumento, y por lo tanto, compete al Estado ponerle lupa al tema e implementar estrategias para frenar estas acciones discriminatorias.

**"No hubo una imposición hacia los colegios ni tampoco un ataque a la familia", explica Fernández, quien añade que ahora lo** importante es que se sumen esfuerzos para atacar el problema de raíz. En ese sentido es esencial la contribución de la sociedad civil, pues usualmente "las acciones más violentas provienen de la desinformación", como sucedió el pasado miércoles con la marcha impulsada por sectores conservadores donde fue evidente la discriminación hacia la comunidad LGBTI.

De igual modo, insistió en que la discriminación de género es un problema mundial, sin embargo, manifiesta que en el caso de [Colombia no se puede combinar el dogma con el Estado laico](https://archivo.contagioradio.com/sociedad-colombiana-no-esta-preparada-para-afrontar-la-diversidad-sexual-marcela-sanchez/)y tiene que ser un proceso participativo en el que la sociedad aprenda a entender al otro, y así " garantizar una vida libre de violencias a la infancia y a la adolescencia".

<iframe src="http://co.ivoox.com/es/player_ej_12601355_2_1.html?data=kpejkpaXeZahhpywj5aXaZS1lZ6ah5yncZOhhpywj5WRaZi3jpWah5yncajpytHZx9fRs4y6xtfbh6iXaaKlz8nS3IqWh4y30NLW1c6Jh5SZo5jbjcnJb6W4qa2YsbO5b8bijKjcztTRpsrVjoqkpZKns8_owszW0ZC2pcXd0JCah5yncZU.&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
