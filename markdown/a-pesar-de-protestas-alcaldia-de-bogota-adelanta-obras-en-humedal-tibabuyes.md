Title: A pesar de protestas, Alcaldía adelanta obras en Humedal Tibabuyes
Date: 2019-05-24 08:30
Author: CtgAdm
Category: Ambiente, Nacional
Tags: Ciudadela Colsubsidio, Engativá, Humedal Tibabuyes., Suba
Slug: a-pesar-de-protestas-alcaldia-de-bogota-adelanta-obras-en-humedal-tibabuyes
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/Captura-de-pantalla-104.png" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/Captura-de-pantalla-106.png" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: ] 

A pesar de la protesta de los residentes, la administración del Alcalde de Bogotá Enrique Peñalosa llegó esta mañana al barrio Ciudadela Colsubsidio en la localidad de Engativá para aislar una zona de amortiguación del Humedal Tibabuyes en anticipación de la construcción del parque Juan Amarillo.

Según la denuncia, funcionarios del Distrito empezaron la intervención de esta zona verde de 18 hectáreas, conocida como Siete Canchas, a las 4:30 de la mañana. Posteriormente los vecinos llegaron dos horas más tarde para frenar el cercamiento de esta parcela para impedir la construcción de seis canchas de fútbol sintéticas y cuatro canchas de baloncesto. (Le puede interesar: "[Habitantes denuncian construcción de complejo deportivo sobre Humedal de Tibabuyes](https://archivo.contagioradio.com/habitantes-denuncian-construccion-decomplejo-deportivo-sobre-humedal-de-tibabuyes/)")

Tal como lo afirmó Yised Arango, vecina de la zona, las comunidades de Engativá y Suba, lugar donde se propone construir este parque de suelo duro, se oponen a la realización de este proyecto por las afectaciones a la flora y fauna que ocasionaría en el Humedal Tibabuyes. Cabe resaltar que esta zona de amortiguación  protege al humedal de las adversidades y la intervención del hombre.

"Como el parque es tan grande, empezaron cerrando siete frentes. Trajeron mucho personal para trabajar en la obra, pero hay gente suficiente para pararla de nuevo", relató Arango. La líder comunal también denunció que el ESMAD arremetió en contra de los ciudadanos. Incluso, unos vídeos que fueron compartidos por redes sociales demuestran a manifestantes con heridas de balas de gomas.

### **Los retos legales al proyecto**

[El Instituto Distrital de Recreación y Deporte (IDRD) adelanta el desarrollo de una intervención, en las localidades de Suba y Engativá, de **86.000 metros cuadrados de terreno, avalado en el decreto 565 de 2017 para realizar la construcción del Parque Juan Amarillo**. El proyecto incluiría] seis campos deportivos de fútbol, zona de gimnasios al aire libre, tres canchas de tenis, juegos infantiles, dos canchas múltiples, dos canchas de voleibol, cuatro canchas de baloncesto, pista pequeña de ciclo montañismo, pista de trote, módulos de baño, vestier, senderos peatonales, senderos de bicicletas, iluminación y administración.

Sin embargo, el Juez Cuarto Administrativo de Bogotá, Lalo Enrique Olarte Rincón, le ordenó el año pasado al Distrito suspender provisionalmente los efectos del decreto 565 por la falta de participación ciudadana en la modificación de la Política de Humedales. El pasado 29 de abril, el mismo juez abrió un incidente de desacato en contra del Alcalde, aún así no estableció que la administración había incumplido las medidas cautelares otorgadas.

Según un comunicado de la Concejala, María Fernanda Rojas, el Juez Olarte luego decidió no suspender las obras adelantadas, ya que cursa en el mismo juzgado un incidente de desacato ,y se está a la espera de que el IDRD y la Empresa de Acueducto y Alcantarillado de Bogotá hagan llegar todos los documentos técnicos. Estos estudios previos ayudaran al juez a determinar si las obras están afectando los ecosistemas o no. (Le puede interesar: "[Bogotá ha perdido el 90% de sus humedales en 40 años](https://archivo.contagioradio.com/bogota-ha-perdido-el-90-de-sus-humedales-en-40-anos/)")

> Hasta cuando esta Alcaldía va pasar por encima de la comunidad. NO QUEREMOS CEMENTO EN EL HUMEDAL TIBABUYES [\#SOSTIBABUYES](https://twitter.com/hashtag/SOSTIBABUYES?src=hash&ref_src=twsrc%5Etfw) [\#ciudadelacolsubsidio](https://twitter.com/hashtag/ciudadelacolsubsidio?src=hash&ref_src=twsrc%5Etfw) [\#ElAlcaldemiente](https://twitter.com/hashtag/ElAlcaldemiente?src=hash&ref_src=twsrc%5Etfw) [@EnriquePenalosa](https://twitter.com/EnriquePenalosa?ref_src=twsrc%5Etfw) [@humedalesbogota](https://twitter.com/humedalesbogota?ref_src=twsrc%5Etfw) [@fhumedales](https://twitter.com/fhumedales?ref_src=twsrc%5Etfw) [@humedaltibabuye](https://twitter.com/humedaltibabuye?ref_src=twsrc%5Etfw) [pic.twitter.com/4YqfIpmk1I](https://t.co/4YqfIpmk1I)
>
> — Laura V. (@lapiveki) [23 de mayo de 2019](https://twitter.com/lapiveki/status/1131609858078314497?ref_src=twsrc%5Etfw)

<p>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
</p>
Frente estos hechos, Arango afirmó que las comunidades esperan que las obras sean suspendidas hasta que la Procuraduría General llegue a una conclusión en las investigaciones que realiza sobre el impacto ambiental de la construcción de este proyecto en los humedales.

<iframe id="audio_36282161" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_36282161_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
