Title: Aumenta el control paramilitar en el departamento de Antioquia
Date: 2018-08-21 16:16
Category: DDHH, Nacional
Tags: Antioquia, COEUROPA, lideres sociales, paramilitares
Slug: aumenta-el-control-paramilitar-en-antioquia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/Paramilitares1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Carlos García] 

###### [21 Ago 2018] 

Dos líderes sociales fueron asesinados en el departamento de Antioquia, se trata de José Vicente García, líder de la mesa de víctima de Valdivia y Luis Alverto Rivas, líder juvenil de Turbo. De acuerdo con Oscar Zapata, integrante de la Coordinación Colombia Europa Estados Unidos, estos hechos se suman a las denuncias que han realizado **las comunidades sobre el aumento del control paramilitar en el territorio, la presencia de grupos armados y la falta de acciones desde la Fuerza Pública.**

En total, en el departamento de Antioquia han sido asesinados **24 líderes sociales en lo corrido del 2018, cifra que según Zapata, enciende todas las alarmas debido a que, durante el 2017** se registraron 21 asesinatos.

Las estructuras paramilitares que estarían operando en el territorio, según el nodo de la Coordinación Colombia Europa Estados Unidos, son las Autodefensas Gaitanistas de Colombia y los Caparrapos. De igual forma, habría presencia de disidentes de las FARC, de bandas criminales relacionadas a carteles del narcotráfico mexicanos y de grupos del ELN.

Asimismo, la organización manifestó que de 125 municipios de Antioquia, **121 tiene al menos un grupo paramilitar que ejerce el control territorial, económico, social y político**, y  que uno de los casos más alarmantes sería el del Valle de Aburrá que contaría con más de 350 bandas criminales, funcionales al paramilitarismo.

"Estamos asistiendo a que, sí el Estado colombiano no garantiza la soberanía de las armas en cabeza del propio Estado, estaremos asistiendo a una nuea ola de violencia que se va a desatar, no solo en contra de los defensores y defensoras de derechos humanos, sino contra la población en general. Incluso contra la misma Fuerza Pública" afirmó Zapata.

### **Los líderes asesinados** 

**José Vicente García** fue asesinado el pasado 15 de agosto, a las 7:00pm cuando hombres llegaron a su vivienda y le dispararon al líder social de 61 años. De acuerdo con la información dada por sus familiares, García no había recibido amenazas.

El otro líder asesinado es **Luis Alberto Rivas, de 22 año de edad, que se había desempeñado como líder juveni**l en el corregimiento como Bocas del Toro, perteneciente al Proceso de Comunidades Negras y lideraba iniciativas culturales. (Le puede interesar: ["110 Líderes sociales han renunciado a sus labores en Antioquia por amenazas")](https://archivo.contagioradio.com/110-lideres-sociales-han-renunciado/)

<iframe id="audio_27983159" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_27983159_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
