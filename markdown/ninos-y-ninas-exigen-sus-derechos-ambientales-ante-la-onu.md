Title: Niñas y niños exigen sus derechos ambientales ante la  ONU
Date: 2020-07-01 16:04
Author: AdminContagio
Category: Actualidad, Ambiente
Tags: derecho ambientales, ONU
Slug: ninos-y-ninas-exigen-sus-derechos-ambientales-ante-la-onu
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/07/1039ffd3-9765-4759-a114-e525c76d680e.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

*Foto: Juan Camilo mantilla*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Este 1° de julio intervino en la 44° sesión del **Consejo de la Organización de Naciones Unidas (ONU), Juliana Torres Marín, de 15 años** **de edad**, quien habló en representación de *«las luchas, resistencias e inconformidades de la niñez y la juventud latinoamericana»* haciendo un llamado para el reconocimiento de los derechos ambientales a esta población, los cuales, según dijo en su intervención ***«han sido históricamente vulnerados y desconocidos».*** (Le puede interesar: [Comunidades Wayúu acuden a la ONU para frenar la actividad del Cerrejón en medio de la pandemia](https://archivo.contagioradio.com/comunidades-wayuu-acuden-a-la-onu-para-frenar-la-actividad-del-cerrejon-en-medio-de-la-pandemia/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Las exigencias de los niños, niñas y jóvenes ante la ONU giraron en torno a:

<!-- /wp:paragraph -->

<!-- wp:list -->

-   La adopción de medidas urgentes que permitan conformar espacios de representación y participación con diversidad de edad y de género, reconociendo a la niñez y juventud como actor político en la toma de decisiones.

<!-- /wp:list -->

<!-- wp:list -->

-   El cumplimiento de acuerdos nacionales e internacionales que buscan la erradicación de la deforestación en  América Latina.

<!-- /wp:list -->

<!-- wp:list -->

-   El agua como derecho fundamental

<!-- /wp:list -->

<!-- wp:list -->

-   La implementación de mecanismos de protección y seguridad que garanticen la vida e integridad física y socioemocional de  líderes y lideresas sociales y ambientales, que han sido violentados y asesinados.

<!-- /wp:list -->

<!-- wp:core-embed/facebook {"url":"https:\/\/www.facebook.com\/SomosPolinizadores\/videos\/901009377046885\/?v=901009377046885","type":"video","providerNameSlug":"facebook","align":"center","className":""} -->

<figure class="wp-block-embed-facebook aligncenter wp-block-embed is-type-video is-provider-facebook">
<div class="wp-block-embed__wrapper">

https://www.facebook.com/SomosPolinizadores/videos/901009377046885/?v=901009377046885

</div>

<figcaption>
44° Sesión de DD.HH de la Organización de Naciones Unidas (ONU)

</figcaption>
</figure>
<!-- /wp:core-embed/facebook -->

<!-- wp:heading {"level":3} -->

### Causas que vulneran sus derechos ambientales según los niños, niñas y jóvenes

<!-- /wp:heading -->

<!-- wp:paragraph -->

En su intervención, Juliana, cuestionó el accionar de algunos líderes mundiales quienes «han  priorizado una idea depredadora de la naturaleza sobre el Buen Vivir de los pueblos».

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Juliana, quien también representa a la organización ambiental [Somos Polinizadores](https://www.facebook.com/SomosPolinizadores/?eid=ARAt0Ydnv2YHC3oiZplg9cvh32ycl_h-wVF1bRUgZwH4UM5DZBZ65cKFCLE2dU4kcJDP0qGbBxJS9MYA) y al Cinturón Occidental Ambiental  de Antioquia, señaló en su intervención ante la ONU que **los conflictos en sus territorios son generados principalmente por proyectos extractivistas, minero-energéticos y agroindustriales,** los cuales afectan la tradición campesina, indígena y los recursos naturales. (Lea también: [Modo de vida campesino Vs. extractivismo](https://archivo.contagioradio.com/modo-de-vida-campesino-vs-extractivismo/))

<!-- /wp:paragraph -->

<!-- wp:quote -->

> **El consumismo desenfrenado, la idea de que somos dueños de la naturaleza, la imposición de las ideas de los adultos, la falta de educación crítica, el extractivismo y el actuar de las empresas, conducen a la violación de los derechos de los niños y niñas y generan problemáticas  ambientales como la deforestación, el aumento de especies en vía de extinción y  el cambio climático que nos han llevado a la actual crisis ambiental»**
>
> <cite>Juliana Torres Marín, integrante de Somos Polinizadores, ante el Consejo de Naciones Unidas  
> </cite>

<!-- /wp:quote -->

<!-- wp:core-embed/facebook {"url":"https:\/\/www.facebook.com\/SomosPolinizadores\/videos\/307327700395747\/?v=307327700395747","type":"video","providerNameSlug":"facebook","className":""} -->

<figure class="wp-block-embed-facebook wp-block-embed is-type-video is-provider-facebook">
<div class="wp-block-embed__wrapper">

https://www.facebook.com/SomosPolinizadores/videos/307327700395747/?v=307327700395747

</div>

</figure>
<!-- /wp:core-embed/facebook -->

<!-- wp:paragraph -->

A la intervención de Juliana Torres, se sumaron las voces de más niñas y niños de Latinoamérica, realizando sus aportes, reclamos y observaciones para que sean reconocidos y protegidos sus derechos ambientales y con ellos, el rol fundamental que juegan las poblaciones más jóvenes, no solamente para ser escuchados sino para hacer parte de la decisión y transformación en la construcción de un ambiente sano y responsable.

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
