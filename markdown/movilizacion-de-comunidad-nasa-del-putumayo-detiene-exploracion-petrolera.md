Title: Movilización de comunidad Nasa del Putumayo detiene exploración petrolera
Date: 2015-02-03 21:28
Author: CtgAdm
Category: Ambiente, Economía
Tags: Ambiente, Corpoamazonía, indígenas, Nasa, Putumayo
Slug: movilizacion-de-comunidad-nasa-del-putumayo-detiene-exploracion-petrolera
Status: published

##### [Foto: www.colombia.com]

<iframe src="http://www.ivoox.com/player_ek_4034482_2_1.html?data=lZWglpmcdo6ZmKiakpyJd6Kol5KSmaiRdo6ZmKiakpKJe6ShkZKSmaiRh8LmzdTgjafFsNXV28bfj4qbh4630NPhw8zNs4zGwsnW0ZCRaZi3jpk%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Carlos Baltazar] 

La movilización de los campesinos e  indígenas logró detener actividades de explotación  y la exploración petrolera en Putumayo. En resolución del 20 de enero, **Corpoamazonía decidió expedir dos resoluciones: e**n la primera se suspende la licencia de aguas que había otorgado a Gas Tierra para desarrollar un proyecto sísmico en Orito, Putumayo;  y la segunda resolución considera ese territorio como una zona de protección.

Carlos Baltazar, líder de la comunidad Nasa, afirma que **la medida protege el territorio por 180 días en los que estará suspendido el proyecto**, y durante ese tiempo Corpoamazonía realizará un estudio sobre los impactos negativos que generan este tipo obras.

**“Como comunidades indígenas estamos exigiendo que se respete los territorios indígenas porque el Estado debe protegerlos y no adelantar proyectos cuando no haya una consulta previa”**, Baltazar señala que en esa zona del Putumayo, hay yacimientos de agua y una gran variedad de especies que estarían en riesgo con el desarrollo de este proyecto sísmico.

**El líder indígena también denunció que muchas veces las empresas adelantan consultas con una sola comunidad** desconociendo que existen otras que también tiene derecho a decidir. Además aseguró que l**as empresas suelen realizar ciertos ofrecimientos a las comunidades indígenas**, como los Awá, para que la población no se vuelva un impedimento para la realización de estos proyectos.

Las labores de la obra se suspendieron, y aunque eso no significa que en un futuro no continúen, la comunidad Nasa mantiene una posición firme con el fin de garantizar la preservación de la fauna, la flora y los recursos hídricos del Putumayo.
