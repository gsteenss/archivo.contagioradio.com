Title: España decide entre partidos tradicionales y nuevas alternativas
Date: 2015-12-18 16:00
Category: El mundo
Tags: Elecciones España, españa, Partido popular, podemos, PP, PSOE
Slug: elecciones-en-espana-entre-nuevas-alternativas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/12/elecciones-españa.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<iframe src="http://www.ivoox.com/player_ek_9770897_2_1.html?data=mpykkp2de46ZmKiak5WJd6KmmJKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRic2fx9rh19fTb8XZjKrg0saJh5SZo5bOjdjJb8XZxM7Rx5DFb8jjzdXSjcnJb9fj1dSYx9jYqYzY0NKah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### 17 Dic 2015

El supuesto debate entre **Mariano Rajoy, actual Presidente del Gobierno de España** y candidato a la reelección por el Partido Popular (PP), y Pedro Sánchez, candidato por el Partido Socialista (PSOE), realizado esta semana, a unos días de las elecciones presidenciales que se celebran en España, estuvo cargado de descalificaciones, echadas en cara y pocas, muy pocas propuestas.

El cara a cara por parte de los dos líderes de la vieja derecha e izquierda española ha estado en la parrilla mediática durante toda la semana, a solo unos días de los comicios generales que se celebrarán el domingo 20 de diciembre y donde se decide el futuro del país.

Cuatro años le han servido al **Partido Popular** para ser odiado por unos o querido por otros, sin medias tintas. Y es que las políticas de recortes junto a los numerosos casos de corrupción sacados a la palestra y que vienen de antaño en el partido, no es para menos. Hasta tal punto que, en la noche del miércoles, un joven de 17 años atizó un puñetazo a Mariano Rajoy en plena calle de Pontevedra mientras el líder del PP hacía los actos rutinarios de la campaña electoral.

La gente está cansada, al menos una parte de la ciudadanía y, sin justificar ningún tipo de violencia, es un ejemplo de la hartura de un pueblo, o al menos de quienes se sitúan en las clases medias y bajas españolas.

La vicepresidenta del Gobierno y miembro del PP, Soraya Sáenz Santa María, ratificaba por televisión en el debate a cuatro realizado por Atresmedia el pasado 7 de diciembre, lo bien que el Gobierno lo ha hecho estos cuatro años, en especial en lo relativo a la economía y al bienestar social. “Hemos creado empleo, los jóvenes españoles son los jóvenes europeos que mejor encuentran trabajo, ahora tratamos de incentivar la estabilidad en el empleo”, decía intentando convencer en una intervención seca, demasiado formal y sin credibilidad ninguna.

Mientras, los nuevos partidos como **Podemos y Ciudadanos**, creados a raíz de la crisis económica que ha llevado a España a altos niveles de desempleo, precariedad y pérdida de derechos, no lo ven así.

“Se ha gobernado para la minoría. Hay que reducir la deuda. Buena parte de nuestros jóvenes han tenido que emigrar. El PP se ha gastado el 45% de las pensiones del país. Los contratos son temporales, la mayoría dura menos de un mes. En este país la situación es alarmante, hay gente en el paro sin cobrar ninguna prestación”, fueron algunas de las intervenciones por parte de Pablo Iglesias, de Podemos, y de Albert Ribera, de Ciudadanos, las dos alternativas de izquierda y derecha, respectivamente, en las que está en juego el devenir español, según las últimas encuestas.

Por su parte, **Unidad Popular - Izquierda Unida** tampoco lo ve así. A pesar de haber sido discriminado en el famoso debate a cuatro emitido por Atresmedia el pasado lunes, Alberto garzón, su líder, no ha dejado de desilusionarse por unos comicios que no le auspician muchos éxitos. Aún así, allá donde va arrasa, como en el debate realizado esta semana en el barrio madrileño de La Latina, donde tuvo que salir a la calle a saludar a quienes no pudieron entrar al acto debido a su abarrotamiento.

Pero centremos la atención en las nuevas alternativas: 
------------------------------------------------------

El **partido Ciudadanos** se presenta como una derecha moderada y juvenil. Formada por mucha gente que antes militaba en el partido popular pero que, como explicaba la alcaldesa de Barcelona, Ada Colau, de Podemos, durante un mitin en Madrid, son muchas las personas que se han cambiado de bando cuando el gobierno ha estado de capa caída; es decir, que antes militaban en el PP pero que cuando nace Ciudadanos, se cambian de bando y de chapa en la solapa simulando ser otra cosa con apuestas distintas.

Sus propuestas políticas son de centro derecha y las causas de su boom son varias: un apoyo empresarial importante, la simpatía del votante moderado y una gran habilidad con los medios de comunicación Quizás por ello, hoy tiene a más de 15.000 afiliados y 100.000 simpatizantes, por lo que el partido naranja (por los colores que utiliza) podría convertirse en la cuarta fuerza política de España y ser clave de cara a futuros pactos de gobierno en muchas administraciones.

Por su parte, Podemos, que nace en las plazas durante el famoso 15-M del año 2011 con el grito ¡SÍ SE PUEDE!, se muestra más interesado en apoyar las causas sociales y estar con la gente de abajo. Quiere acabar con los desahucios, la corrupción, las privatizaciones de los servicios sociales que se han venido haciendo durante estos cuatro años…

También desea implantar un sistema de empleo digno para todos y todas, garantizar un sistema de pensiones de calidad y crear medidas para la igualdad de género y la integridad laboral. Apuesta por cambios desde la gente y un gobierno responsable, solvente, independiente y comprometido con quienes desean un futuro mejor, como explica Tania Sánchez, candidata de Podemos por Madrid.

Pero a pesar de las muchas opciones políticas para este domingo, porque detrás de los principales partidos mayoritarios, hay otros muchos minoritarios y regionalista, las encuestas vaticinan que volverá a ganar el PP, seguido del PSOE, Podemos y Ciudadanos.

Y… ¿Cómo es posible que las políticas aplicadas, a pesar de haberse demostrado reiteradamente injustas e ineficaces, sigan presentándose como medidas ‘serias’ y ‘responsables’?, se pregunta el sociólogo español Jorge Moruno.

La respuesta es que “la crisis ha permitido que los mismos actores que condujeron a la caída de España salgan reforzados y sus beneficios aumenten. Las lógicas que han disparado la espiral de la deuda en las últimas décadas se presentan como defensoras de la ‘austeridad’ contra el gasto. ¡Y esto parece el mundo al revés!”

“PP, PSOE y Ciudadanos comparten una misma estrategia usando tácticas distintas, al tiempo que las propuestas para salir del bucle del empobrecimiento, la austeridad y la desigualdad son duramente cuestionadas”.

Pero parece que, según las encuestas, la ciudadanía olvida rápido o tiene miedo a los nuevos aires. Ante tanta incertidumbre, tan sólo nos queda esperar a conocer los resultados en las urnas del próximo domingo. Lugar donde verdaderamente se tendrá que decidir si romper con lo viejo y empezar algo nuevo, o quedarse como hasta ahora.

El futuro está por ver en España.

#### **Por: Silvia Arjona** 
