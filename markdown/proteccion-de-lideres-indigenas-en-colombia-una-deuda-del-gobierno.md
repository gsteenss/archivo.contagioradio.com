Title: Organizaciones indígenas insisten en el diálogo pese a histórico incumplimiento
Date: 2019-01-22 12:52
Author: AdminContagio
Category: DDHH, Nacional
Tags: Asociación de Cabildos Indígenas, comision de la verdad
Slug: proteccion-de-lideres-indigenas-en-colombia-una-deuda-del-gobierno
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/01/acuerdo-indigenas-01_77504473197a12a5da4c5e6be20426bb-770x400.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Comisión de la verdad 

###### 22 Ene 2019 

Con el propósito de garantizar su participación en el esclarecimiento de la verdad del conflicto armado en Colombia, los representantes de los 104 pueblos indígenas que hacen parte de la Mesa Permanente de Concertación y las entidades del Sistema Integral de Verdad, Justicia, Reparación y No Repetición (SIVJRNR) protocolizaron acuerdos relacionados con el proceso de consulta previa y la participación que tendrán las comunidades y autoridades en este proceso.

Uno de los principales acuerdos alcanzados es la creación de una **Dirección de Pueblos Étnicos junto a la Comisión de la Verdad**, dicho espacio permitirá coordinar acciones, institucionales con los pueblos indígenas y fortalecer sus derechos colectivos; entre los acuerdos también es de resaltar la creación de macro regionales de Orinoquía y Amazonía y acoger los criterios de priorización propuestos por los pueblos indígenas.

### **Continúa la concertación técnica del Plan Nacional de Desarrollo** 

Según **Aida Quilcué, consejera de Derechos de los Pueblos Indígenas**, dicho protocolo continúa siendo perfeccionado junto a la Mesa Permanente y el Gobierno Nacional a través de un Plan Nacional de Desarrollo el cual plasmaría temas como territorio, infraestructura, derechos humanos, paz y la protección individual y colectiva de los pueblos indígenas, iniciativas que se han propuesto al Gobierno con anterioridad en más de 1.500 acuerdos de los que tan solo se han implementado un 6%.[(Le puede interesar Gobierno ha incumplido 1300 acuerdos a la minga indígena)](https://archivo.contagioradio.com/inicia-minga-indigena-en-colombia/)

La anterior cifra se suma a la proporcionada por el informe **"¿Cuáles son los patrones? Asesinatos de Líderes Sociales en el Post Acuerdo"**, señala que entre el 24 de noviembre de 2016 y el 31 de julio de 2018 se registró **el asesinato de 31 líderes de pueblos indígenas representando un 12,06% de los asesinatos cometidos contra líderes sociales en el país, **estadística que incremento a finales del 2018 y amenaza con seguir su ascenso este 2019.

### **Fiscalía no avanza en investigaciones de autores intelectuales** 

Frente a los procesos de esclarecimiento de los casos de asesinatos de líderes indígenas,  la Fiscalía ha dicho a la comunidad indígena que únicamente se han investigado a los autores materiales, sin embargo no existen investigaciones sobre los autores intelectuales de amenazas o asesinatos con excepción de uno u otro caso, un tema que la líder considera debe ser tratado con “mayor eficacia y celeridad” pues **la vulneración sistemática de los derechos humanos y la violencia en los territorios sigue incrementando.**

La defensora de derechos humanos reconoce que incluso con la voluntad política que existió durante el gobierno Santos, no se cumplieron en la práctica la mayoría de dichos acuerdos regionales,  experiencia que reflejada con el gabinete actual llevaría a pensar que están “muy lejos los acuerdos a los que se podría llegar con este Gobierno”, sin embargo insistió en que como principio del pueblo indígena seguirán afianzado los diálogos y no renunciarán a la construcción de propuestas.

Aida Quilcué contrasta esta posición con la mesa de negociación en el Cauca donde Gobierno y resguardos no llegaron a mayores acuerdos, **un indicador de que en algunas regiones no existe un avance en la protección de las comunidades** y que como resalta la lideresa continúa suscitando “el olvido y el no reconocimiento de los derechos fundamentales a los que deben tener acceso los pueblos indígenas”.

<iframe id="audio_31685281" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_31685281_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
