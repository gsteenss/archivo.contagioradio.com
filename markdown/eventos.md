Title: Eventos
Date: 2014-11-25 15:50
Author: AdminContagio
Slug: eventos
Status: published

### EVENTOS

[](https://archivo.contagioradio.com/hay-festival-para-las-comunidades/)  

###### [También “Hay Festival” para las comunidades y barrios](https://archivo.contagioradio.com/hay-festival-para-las-comunidades/)

[<time datetime="2019-01-25T11:44:27+00:00" title="2019-01-25T11:44:27+00:00">enero 25, 2019</time>](https://archivo.contagioradio.com/2019/01/25/)La sección comunitaria del Hay Festival 2019 trae actividades enfocadas en la afrocolombianidad, género y la importancia de la imaginación en la infancia[LEER MÁS](https://archivo.contagioradio.com/hay-festival-para-las-comunidades/)  
[](https://archivo.contagioradio.com/el-aire-que-respiro-bogota/)  

###### [Con documental ciudadanos exponen su preocupación por calidad del aire en Bogotá](https://archivo.contagioradio.com/el-aire-que-respiro-bogota/)

[<time datetime="2018-12-13T13:32:18+00:00" title="2018-12-13T13:32:18+00:00">diciembre 13, 2018</time>](https://archivo.contagioradio.com/2018/12/13/)“El Aire que Respiro, Caso Bogotá” una producción que busca sensibilizar sobre los factores que inciden en el deterioro de la calidad del aire[LEER MÁS](https://archivo.contagioradio.com/el-aire-que-respiro-bogota/)  
[](https://archivo.contagioradio.com/dia-no-violencia-mujeres/)  

###### [Arte y cultura en el día de no violencia contra las mujeres](https://archivo.contagioradio.com/dia-no-violencia-mujeres/)

[<time datetime="2018-11-23T14:03:11+00:00" title="2018-11-23T14:03:11+00:00">noviembre 23, 2018</time>](https://archivo.contagioradio.com/2018/11/23/)En el marco del día de la no violencia contra las mujeres, varios colectivos adelantarán diversas actividades artísticas y culturales[LEER MÁS](https://archivo.contagioradio.com/dia-no-violencia-mujeres/)  
[](https://archivo.contagioradio.com/teatro-calidoso-bogota/)  

###### [Una obra recuerda a Calidoso y a los habi-tantes de calle](https://archivo.contagioradio.com/teatro-calidoso-bogota/)

[<time datetime="2018-10-17T11:21:52+00:00" title="2018-10-17T11:21:52+00:00">octubre 17, 2018</time>](https://archivo.contagioradio.com/2018/10/17/)HABI-TANTES, una obra de teatro inspirada en las historias de cientos de habitantes de calle como Calidoso que viven tras un muro construido de indiferencia[LEER MÁS](https://archivo.contagioradio.com/teatro-calidoso-bogota/)
