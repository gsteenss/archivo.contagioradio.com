Title: Comunidad universitaria va a paro en Argentina
Date: 2017-05-08 12:58
Category: El mundo, Otra Mirada
Tags: Argentina, Macri, paro, universitario
Slug: parouniversitarioargentina
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/05/conadu.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Prensa CONADU 

##### 08 May 2017 

Desde este lunes 8 de mayo, docentes universitarios de Argentina entrarán a paro en todo el país. Durante una semana la comunidad educativa se movilizara **por la exigencia de sus derechos entre los que se encuentran un aumento salarial del 35%**, mayor presupuesto para ciencia y tecnología y unas paritarias libres y sin techo.

El reclamo de los docentes radica en la actitud del gobierno Macri, que durante la sexta reunión paritaria del jueves pasado "sólo informó que, de manera unilateral, **adelantará un cuatro por ciento por los meses de marzo y abril, un 2% menos de lo que había ofrecido en la reunión anterior**.” un pago que según los sindicatos obedece a cuenta de la propuesta anterior que incluía un 6% a marzo, adelantando menos de lo que ofertaron, lo que ratifica su consideración de que **el gobierno no quiere superar el 18 0 20% de incremento** para este año.

A la indignación de los maestros por los incumplimientos en materia salarial, se suman los **ataques y el desprestigio** que desde el gobierno se realizan hacia la docencia, el derecho a reclamar pacíficamente y **el desfinanciamiento hacia la Universidad Pública en general y la Ciencia y Tecnología en particular**, razones que motivan lo que aseguran sera **el mayor paro universitario de los últimos 12 años**.

Las manifestaciones que van **hasta el próximo 12 de mayo**, son convocadas entre otras por los gremios docentes de universidades públicas como la Universidad de Buenos Aires (UBA) y el Síndicato de docentes de la misma Institución (FEDUBA), quienes anunciaron que **el martes 16 de mayo se realizará una gran Marcha Nacional Universitaria** donde continuarán insistiendo en sus demandas.

Tal movilización tendrá como destino el Congreso Nacional, lugar donde se encontrarán con los afliliados a  la **Confederación Nacional de Docentes Universitarios (Conadu)**, su par histórica y las **Federaciones Argentina de Trabajadores de las Universidades Nacionales (Fatun), de Docentes del sector (Fedun) y el Sindicato Argentino de Docentes Privados (Sadop)**.

Durante la para de actividades, **los estudiantes brindarán clases públicas y talleres en varios puntos del país**, y respaldan el llamado de las diversas organizaciones sociales y de derechos humanos que convocan a la **movilización "contra la impunidad a genocidas"** que se adelantará el miércoles 17 de mayo. Le puede interesar: ["Ningún genocida suelto" la consigna contra la ley del 2x1 en Argentina](https://archivo.contagioradio.com/argentina-dictadura-ley/).

###### Reciba toda la información de Contagio Radio en [[su correo]
