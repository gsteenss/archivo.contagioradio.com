Title: Ríos Vivos denuncia proselitismo de Centro Democrático en medio de crisis de Hidroituango
Date: 2018-05-25 16:15
Category: Ambiente, Nacional
Tags: Antioquia, Hidroituango, Rios Vivos
Slug: rios-vivos-denuncia-proselitismo-de-centro-democratico-en-medio-de-crisis-de-hidroituango
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/05/Dd78LgQVQAE7YTi.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Ríos Vivos] 

###### [25 May 2018]

Las comunidades aledañas al río Cauca continúan manifestando su preocupación frente a la posibilidad de una avalancha. De acuerdo con el Movimiento Ríos Vivos aguas arriba de la presa, el estancamiento del agua está provocando malos olores y la descomposición de material orgánico y que partidos como **el Centro Democrático están haciendo proselitismo con los damnificados, aprovechándose de su situación**.

El gobernador de Antioquia, Luis Pérez, se comprometió con el Movimiento Ríos Vivos a darles un espacio en el puesto de mando unificado y de esta forma, tener información de primera mano y poderla comunicar a las comunidades, como también darle trámite a las inquietudes de las personas. Frente al llenado anticipado que se realizó, **Zuleta afirmó que ya se tiene una respuesta en la que se afirmó que fue una decisión arbitraría**.

### **Agúas arriba se estanca el río**

Otra de las situaciones que se encuentra denunciando Ríos Vivos, es que aguas arriba de la represa ya se nota una gran mancha verde sobre el agua, además Zuleta informó que desde el puente de Pescadero, **los habitantes están alertando sobre olores putrefactos que esta generando gas metano, perjudicial para las comunidades aledañas.**

Asimismo, las comunidades de Sabanalarga, el Peque y Buriticá, señalaron que el puente La Garrucha que los conectaba, de acuerdo con los ingenieros, no será construido de nuevo porque no se asegura su estabilidad en el terreno, por más de 5 años. (Le puede interesar:["Alerta naranja no reduce riesgo de una avalancha en Hidroituango: Ríos Vivos"](https://archivo.contagioradio.com/alerta-naranja-no-reduce-el-riesgo-de-una-avalancha-en-hidroituango-rios-vivos/))

### **Las elecciones y sus estragos** 

Ríos Vivos denunció que "de manera inescrupulosa" algunos candidatos a la presidencia de la república han ido a llevar mercados a la población damnificada para conseguir votos. Según Zuleta, específicamente la campaña de Iván Duque por el Centro Democrático, estuvo en Sabanalarga, haciendo actividades proselitistas. **"No nos parece que sea ético que se use la emergencia para hacer campañas políticas".**

Las elecciones se llevarán a cabo en las comunidades, los puestos de votación serán trasladados a Puerto Valdivia, en la Plaza Principal. Ríos Vivos ha insistido en que debe haber seguridad para las personas que quieren participar.

###### Reciba toda la información de Contagio Radio en [[su correo]
