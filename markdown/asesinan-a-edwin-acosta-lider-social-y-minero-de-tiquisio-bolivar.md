Title: Asesinan a Edwin Acosta, líder social y minero de Tiquisio, Bolívar
Date: 2020-05-27 11:11
Author: CtgAdm
Category: Actualidad, Líderes sociales
Tags: Bolívar
Slug: asesinan-a-edwin-acosta-lider-social-y-minero-de-tiquisio-bolivar
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/05/Líder-Edwin-Emiro-Acosta.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: líder Edwin Acosta/ @parescolombia

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

El pasado 26 de mayo en horas de la tarde, la Comisión de Interlocución del Sur de Bolívar, Centro y Sur del Cesar denunció el asesinato del líder social y minero **Edwin Emiro Acosta Ochoa en el corregimiento de Mina Seca en Tiquisio, Bolívar, según señala la comunidad por accionar paramilitar.**

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Versiones preliminares señalan que **Edwin, integrante de la Asociación Agrominera de Tiquisio fue abordado en su casa por tres hombres con armas cortas que habrían preguntado por su identidad**, al responder de forma afirmativa, los hombres dispararon en cuatro ocasiones hiriendo su cabeza y espalda. [(Le puede interesar: Asesinan a líder Aramis Arenas Bayona en Becerril, Cesar)](https://archivo.contagioradio.com/asesinan-a-lider-aramis-arenas-bayona-en-becerril-cesar/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Edwin, esposo y padre de tres hijos también era reconocido como integrante de la Comisión de Interlocución del Sur de Bolívar. Las comunidades advierten que pese a la militarización que existe en el departamento, lideres y lideresas del sur de Bolívar siguen siendo vulnerados sin que existan acciones de fondo de las autoridades e instituciones. [(Le puede interesar: 2019 fue el año más agresivo contra defensores de DDHH en toda la década: Somos Defensores)](https://archivo.contagioradio.com/el-ano-pasado-fue-el-mas-agresivo-para-defensores-de-dd-hh-de-la-decada-somos-defensores/)

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Ser líder social, una labor de riesgo en Bolívar

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Hace menos de una semana, la Junta de Acción Comunal del corregimiento de Vallecito al sur del Bolívar, denunció la desaparición forzada de María Rocío Silva Caballero de 52 años, integrante de la Junta y defensora de los proyectos de sustitución voluntaria de cultivos de uso ilícito. [(Le puede interesar: Denuncian desaparición forzada de lideresa Rocío Silva en el Sur de Bolívar)](https://archivo.contagioradio.com/denuncian-desaparicion-forzada-de-lideresa-rocio-silva-en-el-sur-de-bolivar/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Según el [Programa Somos Defensores](https://twitter.com/SomosDef), el 2019 concluyó con 844 hechos violentos contra liderazgos sociales en Colombia siendo el año con mayor registros de casos en la última década, siendo Bolívar uno de los 11 departamentos donde más afectaciones contra los derechos de defensores de DD.HH. se presentaron siendo un total de 27 agresiones. Por su parte el **Instituto de Estudios para el Desarrollo y la Paz (Indepaz) señala que ya son 100 los líderes asesinados en la primera mitad del 2020.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Noticia en desarrollo....

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
