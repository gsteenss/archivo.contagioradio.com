Title: Paramilitarismo y secuestro los puntos álgidos de la mesa en Quito
Date: 2017-06-12 13:00
Category: Nacional, Paz
Tags: ELN, Juan Carlos Restrepo, Mesa de diálogos en Quito
Slug: eln-y-gobierno-deben-superar-tensiones-para-alcanzar-cese-bilateral
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/02/PAZ-ELN-CANCILLERIA-Ecuador-2.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Cancillería de Ecuador] 

###### [12 Jun 2017] 

A pesar de que se habla de una crisis en la Mesa de Conversaciones de Paz entre el Gobierno y el ELN que habría sido suscitada por desacuerdos en torno al tema del secuestro y el combate al paramilitarismo, **el senado Iván Cepeda da un parte de tranquilidad pero afirma que es necesario que se insista el cese al fuego bilateral**.

El senador Iván Cepeda manifestó que **las tensiones de la mesa de diálogos en Quito entre el Ejército de Liberación Nacional y el Gobierno, se dan en todos los procesos de paz**, y no afecta los avances que se han alcanzado como la creación de un equipo de comunicación, el mecanismo de apoyo internacional y los detalles de los asuntos de la participación social y ciudadana. Le puede interesar: ["ELN y Gobierno incluyen cese al fuego en agenda de conversaciones"](https://archivo.contagioradio.com/eln-y-gobierno-incluyen-el-cese-al-fuego-en-la-mesa-de-conversaciones/)

La decisión del gobierno de “congelar” los acuerdos que hasta el momento se han realizado con el ELN, de acuerdo con Cepeda, hace parte de un momento álgido del proceso que necesitará del trabajo conjunto tanto de la delegación del gobierno Nacional como la del ELN “son momentos en los cuales las dificultades de determinados temas se expresan aveces en situaciones de no entendimiento, **pero no es lo que prima en la mesa de conversaciones en Quito**” afirmó Cepeda.

De igual forma, el senador expresó que espera que se avance en las conversaciones sobre el cese bilateral al fuego, sin embargo, aseguró que temas como este requieren de un debate muy intenso y de un diseño que le permita solidez a la hora de aplicarlo, además, **agregó que ya las delegaciones han dado muestras de querer establecer el cese**. Le puede interesar:["ELN y Gobierno alcanzarán punto de no retorno en diciembre: Carlos Velandia"](https://archivo.contagioradio.com/aun-no-hay-condiciones-para-pactar-cese-al-fuego-entre-eln-y-gobierno-carlos-velnadia/)

Asimismo, estableció que la importancia del cese bilateral es garantizar la construcción de paz en territorios del país que todavía se ven afectados por las dinámicas del conflicto armado y que han hecho un llamado, **junto con organizaciones defensoras de derechos humanos y el movimiento social a que este paso se dé lo más pronto posible**.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU).

<div class="osd-sms-wrapper">

</div>
