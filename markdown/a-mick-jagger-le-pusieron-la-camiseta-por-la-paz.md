Title: A "Mick Jagger" le pusieron la camiseta por la paz
Date: 2016-03-10 16:31
Category: Nacional, Paz
Tags: Concierto Rolling Stones, Mick Jagger Bogotá
Slug: a-mick-jagger-le-pusieron-la-camiseta-por-la-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/03/IMG-20160310-WA0021-e1457645394998.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### <iframe src="http://co.ivoox.com/es/player_ek_10754422_2_1.html?data=kpWkl5mYdpOhhpywj5WYaZS1lZmah5yncZOhhpywj5WRaZi3jpWah5yncaKfhpefr87Hr4y%2BwszUx9eJdpOfzcqY0trXrcbm0NOYzsaPp8LhytjS1saPtNDmjNHOjdXFvo6ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe> 

###### [Alexander Mendez] 

###### 10 Mar 2016 

A pocas horas de iniciar el concierto de la banda británica The Rolling Stones en Bogotá, un grupo de ciudadanos se acercó a las inmediaciones del Estadio 'El Campin' portando una imagen tamaño real del vocalista y líder Mick Jagger, con el mensaje "Gana Colombia, se acaba la guerra".

Alexander Mendez, uno de los promotores de la iniciativa, asegura que la intención es "acercar el mensaje a las personas y aclararles que lo que se esta negociando en la Habana no es la paz de Santos, es la paz de Colombia por el bien de todos los colombianos", ante la mala imagen que una parte de la población tiene respecto a los diálogos con las FARC- EP.

La imagen del emblemático artista, quien apoya abiertamente la paz desde los tiempos de la guerra de Vietnam, ha llamado la atención de quienes esperan ingresar al escenario. "La mayoría de las personas quieren tomarse la foto con nosotros, se rien, se divierten y nos dan su mensaje de paz" y sin importar lo que digan las encuestas "las personas si quieren la paz, si quieren que se acabe la guerra"  afirma Mendez.

Y aunque difícilmente lleguen a ser vistos por el mismo Jagger, Keith Richards, Charlie Watts o Ronnie Wood, el grupo de ciudadanos es optimista y algunos de ellos entraran al concierto portando pancartas y camisetas con mensajes que puedan compartir los artistas en la tarima.

\

   
   
 
