Title: Pago por servicios ambientales: Algunos riesgos de mercantilizar todo
Date: 2017-09-11 10:07
Category: CENSAT, Opinion
Tags: Ambientalismo, despojo de tierras, Economía Verde
Slug: pago-por-servicios-ambientales-algunos-riesgos-de-mercantilizar-todo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/09/servicios-ambientales.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Censat 

###### 10 Sep 2017 

#### **[Por: CENSAT Agua Viva - Amigos de la Tierra – Colombia](https://archivo.contagioradio.com/censat-agua-viva/)** 

[La división internacional del trabajo se ha expresado en los países de América Latina en una concepción colonial de los pueblos y de la naturaleza que ha permeado las políticas públicas del país desde distintos escenarios en, al menos, los últimos sesenta años. Lo anterior se ha expresado claramente en el extractivismo, pero una cara menos conocida de esta relación, las formas de conservación ambiental agenciadas por agentes corporativos, avanza con fórmulas técnicas y económicas en el terreno del cuidado ecológico, donde tantas variables e imprevistos entran a jugar. Las figuras de protección que se crean bajo estas premisas parten del supuesto de que la toma de decisiones individuales se guía esencialmente por motivaciones económicas.]

[Colombia ha venido avanzando en la legislación e implementación de mecanismos territoriales en esta vía, amparados en el difuso imaginario de la Economía Verde que incluyen programas de familias guardapáramos o guardabosques, Mecanismos de Desarrollo Limpio (MDL), Pago por Servicios Ambientales (PSA), programas de Reducción de Emisiones por Deforestación y Degradación forestal (REDD)- denominados Iniciativas Tempranas-,Certificado de Incentivo Forestal, impuesto al carbono, compensaciones por pérdida de biodiversidad, oficina de negocios verdes en el Ministerio de Ambiente y Desarrollo Sostenible (MADS) y otros similares.]

[Al hacer una cronología de estos instrumentos sería posible identificar su implementación de forma marcada desde la primera década del 2000, sin embargo, 2017 ha sido un año excepcional en la regulación de la Economía Verde en el país. Muchas de esas iniciativas giran en torno al logro de una paz estable y duradera y el resarcimiento de los derechos de las víctimas. Una de las normas con fuerza de ley expedidas en este período legislativo especial es el decreto 870 de 2017 “]*[Por el cual se establece el Pago por Servicios Ambientales y otros incentivos a la conservación”,]*

[Este decreto se presenta como la medida para “]*[prevenir e impulsar soluciones a los conflictos entre la vocación de la tierra y su uso real”]*[-Como aparece en el punto 1 del Acuerdo Final-. El diagnóstico de dicha norma parte de la pérdida de ecosistemas por la expansión de cultivos de uso ilícito y por la deforestación producto de la búsqueda de sustento económico de los pobladores de “]*[ecosistemas estratégicos”.]*

[La propuesta para resolver estas tensiones consiste en una remuneración individual que se constituya en una alternativa económica para los habitantes de estos lugares. Sin embargo, el decreto parece posponer cuestiones de fondo. Dentro de estas la concentración de tierras y aguas en Colombia, la cual ha sido una de las condiciones básicas para la ampliación de la frontera agraria en el país. De otro lado, se asume al campesino como el actor sobre el cual deben centrarse las medidas contra la deforestación, soslayando, por ejemplo, el impacto ambiental de las industrias extractivas.]

[Al igual que otros proyectos enmarcados en la Economía Verde más que atacar los problemas ambientales y económicos estructurales, el decreto lo que hace es adoptar medidas cosméticas, y mercantilizar las relaciones entre las culturas y los ecosistemas otorgando un incentivo económico o en especies por modificar las relaciones con la naturaleza. En este caso, si bien no se pierde la titularidad de los predios sí se empeña la soberanía sobre los mismos al condicionar las actividades permitidas allí.]

[Si de motivaciones y alternativas económicas para permanecer en los territorios se trata, este mecanismo es, cuando menos, insuficiente, pues, aparte de romper las identidades campesinas con la tierra y el territorio, este incentivo, -cuyos montos no se especifican en el decreto y que parten de una valoración discrecional de acuerdo con el costo de oportunidad de las actividades perdidas- no logra garantizar la reproducción comunitaria de la vida campesina en sus aspectos sociales, culturales y económicos.]

[En ese sentido sería más acertado promover otro tipo de programas que reconozcan las amenazas a las que históricamente ha resistido el campesinado: las dificultades en el acceso a tierras y aguas, y la pérdida de sus costumbres. Aquí sería preciso pensar opciones sustentables acordes con los medios y modos de vida campesinos y no iniciativas que consistan en una resignificación de los reclamos populares de justicia social, económica y ambiental para imponer más de lo mismo, esto es, ponerle precio a todo: a las relaciones de cuidado del campesinado, a los saberes populares y a los ciclos naturales, a la vez que se desconocen las identidades campesinas desde y con los territorios.]

[Adicionalmente, con la lógica de compensar los daños ambientales en determinado territorio, preservando otros que ya existen se amplía la escala de la apropiación territorial. La financiación privada de la iniciativa deja entrever que los]*[“Interesados en Servicios Ambientales”]*[ pueden corresponder a empresas vinculadas al sector extractivo, quienes en cumplimiento de]*[obligaciones derivadas de autorizaciones ambientales]*[ logran mejorar su imagen corporativa en términos sociales y ambientales, permitiendo así ampliar su accionar en otros territorios, perjudicando a la población campesina de los mismos.]

[El momento actual de implementación de Acuerdos de Paz es una oportunidad para cambiar el contrato natural hegemónico y para reconciliarnos con la naturaleza, con toda la naturaleza; humana y no humana, lo cual implica, entre muchos otros asuntos, respetar las construcciones sociales campesinas y evaluar las condiciones de posibilidad que lo han ubicado en una posición subyugada en Colombia. Con este fin, necesariamente se deben garantizar las condiciones materiales para una vida digna, que ineludiblemente pasan por el cuidado ambiental, pero no con medidas paliativas, sino desde las transformaciones urgentes que se necesitan para hacer viable a futuro, no solo la vida campesina, sino todas las formas de vida, amenazadas por la actual crisis ambiental.]

#### [**Leer más columnas de CENSAT Agua Viva - Amigos de la Tierra – Colombia**](https://archivo.contagioradio.com/censat-agua-viva/)

------------------------------------------------------------------------

###### Las opiniones expresadas en este artículo son de exclusiva responsabilidad del autor y no necesariamente representan la opinión de Contagio Radio.
