Title: Víctimas piden que comunidad internacional se pronuncie ante crisis de la JEP
Date: 2017-11-20 12:50
Category: Entrevistas, Paz
Tags: acuerdos de paz, comunidad intrenacional, conflicto armado, Conpaz, JEP, vítimas
Slug: victimas-piden-que-comunidad-internacional-se-pronuncie-ante-crisis-que-vive-la-jep
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/11/carta-conpaz-comunidad-int.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: CONPAZ] 

###### [20 Nov 2017] 

Las Comunidades Construyendo Paz en los Territorios, CONPAZ, le enviaron una carta al papa Francisco y a la comunidad internacional en representación de las Naciones Unidas, donde manifiestan **su preocupación por las actuaciones del Congreso de la República y la Corte Constitucional**, quienes “han obrado en contra del Acuerdo del Teatro Colón”. Argumentaron que las víctimas necesitan conocer la verdad y nunca han buscado “venganza”.

En la carta, las víctimas indican que **“se está inaugurando una nueva era de impunidad bajo el nombre de la paz**, una nueva era de mentira”. Dicen además que los partidos políticos tradicionales se han “burlado” de las víctimas y de las personas que respaldaron el Acuerdo de Paz. También le hacen saber a la comunidad internacional que el presidente Santos los defraudó y “fue incapaz de estar a la altura del momento”.

Por esto, afirmaron que “a pesar del respaldo del Papa Francisco, de dos Secretarios Generales de Naciones Unidas, del Consejo de Seguridad de Naciones Unidas, y de ser elegido por el Consejo de los Premio Nobel como Nobel de Paz 2016, y obviamente de nosotras las víctimas, **está ausente para enfrentar esta crisis**, una de las etapas más importantes de este proceso, el de los derechos de las víctimas.” (Le puede interesar: ["FARC pide reunión con fiscal de la Corte Penal Internacional ante situación de la JEP"](https://archivo.contagioradio.com/farc_corte_penal_internacional_jep/))

### **“Esa JEP no es la del Acuerdo”** 

De acuerdo con Rodrigo Castillo, integrante de CONPAZ y víctima del conflicto armado, una de las preocupaciones de las víctimas frente a la Justicia Especial de Paz, **gira en torno a la decisión de la Corte Constitucional y de los congresistas**. Argumentó que les preocupa lo establecido en cuanto a la participación de terceros en el conflicto armado y que ahora se someterían de manera voluntaria a la JEP.

También indicó que las víctimas saben que hubo actores civiles que **“aportaron condiciones para que la guerra se recrudeciera en los territorios** y las decisiones de hoy los deja por fuera de cualquier responsabilidad”. También dijo que les preocupa la victimización de los defensores de derechos humanos “cuando se plantea que ningún defensor puede participar en defensa de los derechos de las víctimas en la Jurisdicción Especial de Paz”. (Le puede interesar:["No quieren dejar que la justicia fluya": Víctimas al Congreso tras modificaciones a la JEP"](https://archivo.contagioradio.com/victimas-jep-congreso/))

### **Es urgente un pronunciamiento internacional** 

Para realizar actividades de presión al Gobierno Nacional y para que en la Cámara de Representantes **se actúe en concordancia con el Acuerdo pactado**, las víctimas optaron por dirigirse a la comunidad internacional y al mismo tiempo le piden a los congresistas que reverse las decisiones tomadas por el Senado.

Además, convocaron a las organizaciones de defensores de derechos humanos y de víctimas **para que hagan parte de esta iniciativa**. “Señor Papa Francisco, Señores Secretario y Ex Secretario de Naciones Unidas, Señores Países del Consejo de Seguridad de Naciones Unidas, Señores Consejo del Premio Nobel, ustedes pueden evitar este naufragio, para que a salvo de este momento de crisis de la paz, podamos encausar el tiempo hacia la paz total”, escriben en la carta.

Finalmente, recuerdan que **es derecho de las víctimas poder conocer la verdad** de todo lo sucedido para que haya una reparación efectiva y no vuelvan a suceder actos de barbarie en las comunidades más alejadas y abandonas del país.

<iframe id="audio_22180445" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_22180445_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio]{.s1}](http://bit.ly/1ICYhVU)[.]{.s1}
