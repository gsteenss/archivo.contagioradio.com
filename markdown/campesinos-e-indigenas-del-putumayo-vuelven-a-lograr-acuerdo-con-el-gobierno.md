Title: Campesinos e indígenas del Putumayo vuelven a lograr acuerdo con el gobierno
Date: 2016-09-05 14:56
Category: Ambiente, Nacional
Tags: Amerisur, Amerisur en Putumayo, Putumayo, Yuri Quintero
Slug: campesinos-e-indigenas-del-putumayo-vuelven-a-lograr-acuerdo-con-el-gobierno
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/05/Putumayo-Nasa-e1463785355735.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo Contagio Radio ] 

###### [5 Ago 2016]

Luego de más de 40 días de protestas los y las campesinas, así como las comunidades indígenas del departamento lograron llegar a un acuerdo con el gobierno en el que se definen **4 mesas de trabajo en las que se pretenden resolver las exigencias de los sectores sociales** para impedir más daños ambientales producidos por el accionar de las empresas petroleras, la sustitución de cultivos de uso ilícito, la problemática de DDHH y la inversión social.

Con la presencia del Ministro de Ambiente, el Viceministro del interior, Guillermo Rivera, la directora de la Agencia Nacional de Licencias Ambientales y el director de la Agencia Nacional de Hidrocarburos, así como representantes de las comunidades que han mantenido la protesta en 5 puntos del departamento, se logró firmar el acta en la que se da inicio a las **mesas de trabajo que deberán presentar sus resultados en los próximos 15 días**.

Para la diputada departamental Yuri Quintero las mesas de trabajo representarán un avance y la posibilidad de concretar acciones puntuales para resolver las exigencias pero dependerá de la **fuerza de las organizaciones sociales que realmente se cumplan tanto los cronogramas como los acuerdos** a los que se llegue. Según Quintero las organizaciones van a continuar haciendo seguimiento del cumplimiento de los acuerdos.

### Mesa de revisión de afectaciones ambientales por la actividad petrolera 

Aprovechando una serie de estudios que se han realizado en la región en los que se da clara cuenta del impacto ambiental nocivo de las multinacionales petroleras tanto en las comunidades campesinas como en los territorios indígenas, se elaborará un informe que se entregará en 15 días y que tendrá por objetivo frenar el impacto ambiental e impedir que nuevas concesiones petroleras causen más daños.

### Mesa de estudio de sustitución de cultivos de uso ilícito 

En ella se evalúan los impactos de la erradicación forzada, así como las aspersiones con glifosato. Se espera que una vez más sean estudiadas y viabilizadas las propuestas de los campesinos para los plantes de sustitución de cultivos pero también para el desarrollo de acciones integrales que permitan el desarrollo de la economía campesina respetando la tradición de las comunidades y las costumbres asumidas. Esta propuesta entrará en vigencia en el mes de Noviembre, previa evaluación de las comunidades.

### Mesa de Derechos Humanos y Derecho Internacional Humanitario 

En esta mesa se revisará la situación de Derechos Humanos en el departamento que está atravesada no solamente por las detenciones de campesinos y campesinas acusadas de supuestos nexos con grupos guerrilleros, sino por la persistencia del paramilitarismo que ha producido víctimas durante mucho tiempo, pero sobre todo este año cuando se anunció un supuesto plan de “limpieza social”. Adicionalmente los señalamientos por parte de integrantes de las fuerzas militares y los hechos de violencia contra integrantes de las comunidades evidencian una serie de graves infracciones al Derecho Internacional Humanitario.

### Mesa de seguimiento a proyectos de inversión social 

Según Yuri Quintero hay varios proyectos de desarrollo que parecen en las estadísticas de los organismos estatales como el Ministerio del Interior, pero que no aparecen en los sitios en donde supuestamente están ubicados los parques, los colegios, los coliseos, entre otros. Las organizaciones sociales compararán el presupuesto asignado con el ejecutado para poner en marcha dichos desarrollos de infraestructura, respetando los planes de ordenamiento territorial propios de los campesinos e indígenas.

###### [Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU) ]] 
