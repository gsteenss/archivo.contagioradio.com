Title: Estudiantes, profesores y trabajadores de la Universidad Nacional anuncian movilización
Date: 2015-04-17 12:09
Author: CtgAdm
Category: Educación, Nacional
Tags: estudiante, fecode, feu, Ignacio Mantilla, mane, oscar londoño, paro, Plan Nacional de Desarrollo, profesor, Universidad Nacional
Slug: asamblea-de-estudiantes-profesores-y-trabajadores-de-la-universidad-nacional-anuncia-jornadas-de-movilizacion
Status: published

###### Foto: Identidad Estudiantil 

<iframe src="http://www.ivoox.com/player_ek_4369299_2_1.html?data=lZijm5edfY6ZmKiakpyJd6Kml5KSmaiRdo6ZmKiakpKJe6ShkZKSmaiRhdTVzsfZx8aPqMafptjh18nNpc%2FoxtiSlKiPtNPjx8rg0dfJt4ztjNnfw8fFrsLY0NfS1ZDIqYzgjoqkpZKns8%2FowszW0ZC2pcXd0JCah5yncZU%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Oscar Londoño, Representante Estudiantil Universidad Nacional] 

Estudiantes, profesores y trabajadores de la **Universidad Nacional de Colombia** realizaron una asamblea el jueves 16 de abril, para discutir lo problemas que afectan a los 3 estamentos, y proponer hojas de ruta para su solución.

El representante de las y los estudiantes ante el Consejo de la Sede Bogotá, Oscar Londoño, le contó a Contagio Radio que el auditorio León de Greiff, en el cuál se llevó a cabo la asamblea, estuvo completamente lleno.

Los trabajadores manifestaron que la **administración de la Universidad ha incumplido** los acuerdos a los cuales se había llegado en los últimos años: por ejemplo, no se hizo  traslado de 140 cargos que actualmente son de libre remoción y nombramiento (que los trabajadores consideran cargos de "elección a dedo"), para que fuesen cargos de nombramiento por concurso, que respondan a la carrera administrativa y los méritos laborales. Además de que se excluyera a una franja de trabajadores de un aumento salarial pre-acordado.

Los profesores y estudiantes **presentaron un pliego en el que se evidenció el déficit de 150 mil millones de pesos que sufre la universidad**, que se vería agravado por la deuda de 200 mil millones de pesos que adquiriría la institución con el Findeter.

\[caption id="attachment\_7459" align="alignleft" width="327"\][![asamblea UN](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/04/asamblae-UN-2.jpg){.wp-image-7459 width="327" height="218"}](https://archivo.contagioradio.com/asamblea-de-estudiantes-profesores-y-trabajadores-de-la-universidad-nacional-anuncia-jornadas-de-movilizacion/asamblae-un-2/) asamblea UN\[/caption\]

El viernes 17 de abril, el representante estudiantil presentó ante el **Consejo de Sede** una propuesta para que se escuchara a los representante de los trabajadores, y se buscaran salidas al paro que actualmente se desarrolla en la Universidad. Sin embargo, se le negó esta posibilidad. Para Londoño, "dado que la administración cierra las vías de diálogo, no nos queda otra salida que el ejercicio de la movilización de los estamentos".

Es por este motivo que trabajadores, estudiantes y profesores convocan a jornadas de reunión y movilización para la semana del 20 de abril, en la que se sumarán a la jornada convocada por **FECODE** contra el **Plan Nacional de Desarrollo** y sus efectos en la educación superior, así como una nueva asamblea para el día 23 de abril en el que decidirán si profesores y estudiantes se suman al paro de las y los trabajadores de la Universidad Nacional.

Finalmente, el representante estudiantil extendió "la invitación a que la sociedad conozca esta problemática, y se sume a la defensa de la Universidad Nacional y de la educación pública".
