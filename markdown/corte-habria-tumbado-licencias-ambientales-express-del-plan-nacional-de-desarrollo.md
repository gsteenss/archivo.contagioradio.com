Title: Corte habría tumbado licencias ambientales “express” del Plan Nacional de Desarrollo
Date: 2016-02-08 15:00
Category: Economía, Nacional
Tags: ANLA, ANM, Mineria, páramos, Plan Nacional de Desarrollo
Slug: corte-habria-tumbado-licencias-ambientales-express-del-plan-nacional-de-desarrollo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/08/mujer_victima_mineria_contagioradio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: redlatinoamericanademujeres 

###### 08 Feb 2016 

Según la información conocida hasta el momento, con una votación de 5 a 3, la Corte Constitucional habría tumbado 3 de los 6 artículos incluidos en el **[Plan Nacional de Desarrollo](https://archivo.contagioradio.com/plan-nacional-de-desarrollo-2015-2018-supone-retroceso-de-100-anos-en-ddhh/)** que **fueron demandados por algunos congresistas, una vez probado el articulado**. La demanda que sigue en estudio fue interpuesta por congresistas del Polo Democrático Alternativo y el Partido Verde.

Según el portal La Silla Vacía, y otras fuentes consultadas por Contagio Radio, en la decisión la Corte Constitucional argumentó que quitarle las funciones y la autonomía a las **Corporaciones Autónomas Regionales**, CAR, para dejar todo en manos de la Agencia Nacional de Licencias Ambientales, es inconstitucional. Este artículo fue uno de los más criticados del PND y en él radicó un gran volumen de críticas.

Otro de los artículos que habría tumbado la corte, es el que tiene que ver con que la **[Autoridad Minera Nacional](https://archivo.contagioradio.com/?s=miner%C3%ADa)** y que reservaba para ellos **la posibilidad de reservar unas áreas especiales para la minería**, según la ponencia, no se puede concentrar en esa institución el ordenamiento para el uso del suelo, cuando los intervinientes en ese tipo de decisiones deben tener autonomía.

El otro aspecto tiene que ver con que en manos de la **[ANLA](https://archivo.contagioradio.com/?s=anla) y la ANM** tendría que quedar la decisión de darle prelación a los proyectos mineros por encima de los intereses de las víctimas que se acogen a la restitución de tierras, cuando coincidieran los terrenos para la aplicación de este tipo de intereses.

Para algunos analistas esta decisión de la Corte **[se atraviesa directamente sobre los intereses del ahora vicepresidente Germán Vargas Lleras](https://archivo.contagioradio.com/plan-nacional-de-desarrollo-empezo-cojo-jorge-robledo/)**, del cual dependen en gran medida tanto la ANLA como la ANM. Si estos tres puntos y otros que están en estudio quedan por fuera del PND tanto comunidades como ambientalistas podrían **avanzar en la defensa de sus territorios.**
