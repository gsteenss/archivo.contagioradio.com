Title: Pueblo Nasa del Putumayo tendrá Consulta Previa
Date: 2017-03-08 17:09
Category: Ambiente, Nacional
Tags: Fracking en Colombia, Orito, Pueblo Nasa del Putumayo
Slug: pueblo-nasa-del-putumayo-tendra-consulta-previa
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/03/NASA1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Las2Orillas] 

###### [8 Mar 2017] 

El pueblo Nasa que habita en el departamento del Putumayo, fue favorecido por un fallo de la Corte Constitucional, el alto tribunal ordenó la suspensión de actividades de exploración sísmica que adelanta la multinacional canadiense Gran Tierra Energy, desde finales de 2014. Además **ordenó que la empresa debe realizar la Consulta Previa, que hasta el momento no ha realizado, deberá realizar nuevos estudios de impacto ambiental y reconocer de manera formal la presencia de comunidades** étnicas en la zona solicitada.

Mediante la sentencia T-630/16, la Corte aprobó en primera y segunda instancia, las más de **11 tutelas presentadas entre 2013 y 2015 por la asociación del Pueblo Nasa Kwesx Ksxaw y Consejos Comunitarios afro, solicitando consultas previas** e indemnizaciones por los impactos ambientales y culturales genera dos por las empresas operadoras y de transporte de hidrocarburos.

En 2015 las comunidades se pronunciaron a través de estas tutelas, pues el **Ministerio del Interior entregó informes en los que se negaba la presencia de comunidades étnicas en estos territorios**, por lo cual la empresa pudo acceder al título minero y proceder a realizar las actividades de sísmica.

###  

Por otra parte, según lo dispuesto en el auto 4152 de la ANLA, las coordenadas del área escogida para la exploración del subsuelo, se cruzan con territorios legalmente titulados de los Resguardos Indígenas del Alto de Orito y Simorna, lo que fortaleció los argumentos de las comunidades étnicas. ([Le puede interesar: Comunidades de Orito Putumayo le ganan la pelea a proyecto petrolero](https://archivo.contagioradio.com/comunidades-de-orito-putumayo-ganan-pelea-proyecto-petrolero/))

James Baltazar, Consejero de la Asociación del Pueblo Nasa Kwesx Ksxaw, manifestó que las comunidades reciben con gran alegría, señala que **"es una muestra del esfuerzo y lucha del pueblo Nasa por la defensa del territorio y la permanencia",** explica que con la medida los municipios de Orito y Villa Garzón ya no tendrán que "entregar las más de 4.000 hectáreas que pretendía explotar Gran Tierra".

Con el proyecto, Gran Tierra Energy pretendía la adecuación y construcción de tres plataformas multipozo, denominadas **Garza 2, Garza 3 y Garza 4, que contarían con tres pozos cada una. Además, buscaba la construcción de cuatro helipuertos** y un centro de producción, ubicados todos en la vereda El Caldero, cercana al área de perforación exploratoria Garza.

Por último, el consejero aseguró que aunque la noticia fue recibida el pasado 7 de Marzo, los Consejos Comunitarios y Resguardos Indígenas, **ya se encuentran trabajando en la planeación de actividades para la Consulta Previa.**

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio.](http://bit.ly/1ICYhVU) 
