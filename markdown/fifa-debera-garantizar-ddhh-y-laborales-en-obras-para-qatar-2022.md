Title: FIFA deberá garantizar DDHH y laborales en obras para Qatar 2022
Date: 2016-04-15 15:42
Category: El mundo, Otra Mirada
Tags: Derechos laborales obreros Qatar, FIFA, Mudial Qatar 2022
Slug: fifa-debera-garantizar-ddhh-y-laborales-en-obras-para-qatar-2022
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/04/qatar.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### Foto: Onlyagame.org 

##### [15 Abr 2016] 

Luego de las revelaciones de Amnistía Internacional sobre algunas prácticas laborales abusivas que se estarían dando en los trabajos preparativos para la Copa del Mundo, y los reportes periodísticos sobre el maltrato a los obreros de construcción migrantes Qatar publicados en el diario 'The Guardian', la Federación Internacional de Fútbol Asociado FIFA, deberá comprometerse a garantizar los Derechos Humanos y Sindicales de los trabajadores.

Para tal fin, la organización tendrá que seguir la agenda establecida en el informe presentado por el profesor John Ruggie, experto internacional en la materia, en el que se establece que deberá "respetar el paquete completo de derechos sin excepción alguna y en todos y cada uno de los ámbitos de operaciones de la entidad" que no se limitan al desarrollo de la Copa Mundial de fútbol exclusivamente e incluye las operaciones de marketing y patrocinios.

Las sugerencias presentadas en el informe, han sido celebradas desde organizaciones como la Confederación Sindical Internacional (CSI), que a través de su secretaria general Sharan Burrow manifestó que "Si la FIFA es incapaz de reducir los graves impactos a los derechos utilizando su influencia, debería plantearse suspender o terminar la correspondiente relación".

El trabajo realizado por profesor Ruggie, por encargo del mismo ente deportivo, representaría con su cumplimiento una oportunidad para restablecer parte de su maltrecha reputación y se establecería como un requisito global para todas las organizaciones deportivas, tanto en el desarrollo de eventos importantes como de sus actividades regulares.

Para el CSI, la FIFA debe "actuar de manera decisiva" de la misma manera que Qatar, como nación organizadora del evento, debe comprometerse a realizar "una reforma exhaustiva" a sus leyes laborales, y en su consideración deberá hacerlo antes del Foro de las Naciones Unidas sobre Empresas y Derechos Humanos que tendrá lugar la semana que viene en esa nación asiática, para mantener así el derecho de ser anfitrión para la Copa del Mundo 2022.
