Title: Ahora sí, recursos de Ciencia e Investigación serán destinados a Vías terciarias
Date: 2017-03-26 20:04
Category: Opinion, Paola
Tags: ciencia, educacion, tecnologia, vias terciarias
Slug: recursos-de-ciencia-e-investigacion-seran-destinados-a-vias-terciarias
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/03/tecnologia-y-educacion-a-vias-terciarias.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: contextoganadero 

Por: [Paola Galindo](https://archivo.contagioradio.com/paola-galindo/)

###### 26 Mar 2017 

Hace algunas semanas el presidente Juan Manuel Santos anunciaba su intención de reasignar parte del presupuesto del Fondo de Ciencia, Tecnología e Innovación (FCTI) del Sistema General de Regalías (SGR) para la construcción de cerca de 2.500 kilómetros de vías terciarias. Esta asignación, en caso de realizarse, no cubriría ni siquiera el 2% del total de este tipo de infraestructura, la cual, se ha dicho, es fundamental para garantizar algún nivel de justicia económica con la población campesina del país ubicada en áreas donde el conflicto y la ausencia del estado han sido inclementes.

Pese a la preocupación de las comunidades educativas y académicas frente a esta intencionalidad de extraer recursos de un sector ya desfinanciado –como lo es el de ciencia e investigación-, para invertir en proyectos de infraestructura local y regional; el presidente, haciendo caso omiso a las objeciones, siguió adelante con su propuesta que hoy está a cuatro debates en el Congreso de volverse una realidad.

El ministro de Hacienda y Crédito Público, Mauricio Cárdenas, radicó en la Cámara de Representantes bajo el Procedimiento Legislativo Especial para la Paz o Fast Track, el proyecto de Acto Legislativo 010 de 2017 mediante el cual se modifica el artículo 361 de la Constitución Política, aquel que se refiere a la destinación de los ingresos del SGR. Recordemos que las regalías existen como un mecanismo de retribución o compensación a las entidades territoriales  por los efectos ambientales y sociales de las actividades productivas extractivas, de las cuales se ven beneficiadas, en teoría, áreas productoras de  petróleo o con presencia de enclaves mineros, por ejemplo.

Esta modificación crea una medida transitoria que destina un 7% del total de los ingresos del SGR para la implementación del Acuerdo de Paz durante los próximos 20 años. Lo anterior no guarda ninguna dificultad, antes bien, es una garantía parcial para la realización efectiva –si acaso eso llega a ocurrir- de lo consignado en el Acuerdo Final. Lo que sí representa una dificultad es el Artículo 8° transitorio de ese proyecto de Acto Legislativo, en el que se ordena mediante decreto con fuerza de ley, el traslado de una parte de los recursos del FCTI que al 31 de diciembre de 2016 no hayan sido destinados a la financiación de proyectos en esta materia, para el desarrollo del proceso de implementación.

Entre 2012 y 2016 le fueron asignados al FCTI \$3,8 billones de pesos; a diciembre 31 de 2016, sólo se habían aprobado proyectos de inversión por valor de \$2,3 billones, quedando sin ejecutar \$1,5 billones, el monto en cuestión al cual se le daría una nueva destinación: vías terciarias.

Bajo el argumento, soportado por la opinión del Contralor Edgardo Maya, de que este dinero no fue invertido por los factores de riesgo existentes en el sistema, entre ellos la alta dependencia a las gobernaciones para la formulación de proyectos con pertinencia regional, la laxitud de la definición misma de “proyecto regional” e, incluso, situaciones de corrupción, es que se justifica este traslado de recursos.

En el diagnóstico del gobierno y de algunos entes de control frente al funcionamiento del FCTI prima entonces como situación problemática la lentitud en la aprobación de proyectos y la incapacidad de formulación de los mismos por parte de las entidades territoriales. Esto pone de presente que es el diseño institucional existente para el manejo de los recursos de regalías el que presenta fuertes limitantes, que impiden, a su vez, el cumplimiento del propósito del FCTI, que es el de incrementar la capacidad científica, tecnológica y de innovación en las regiones.

Ahora bien, podemos concordar e incluso validar el diagnóstico, pero no su conclusión.

No se puede castigar a la investigación y a la ciencia en Colombia por un asunto de forma. La solución si se quiere, es preservar los recursos y su destinación, aumentar la inversión dirigida a la producción de conocimiento desde lo local y lo  regional modificando para ello los mecanismos de asignación y ejecución de estos presupuestos, de modo tal que se garantice su finalidad social y su transparencia operativa.

------------------------------------------------------------------------

------------------------------------------------------------------------

###### Las opiniones expresadas en este artículo son de exclusiva responsabilidad del autor y no necesariamente representan la opinión de Contagio Radio.
