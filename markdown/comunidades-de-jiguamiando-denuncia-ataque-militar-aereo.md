Title: Comunidad de Jiguamiandó denuncia ataque militar aéreo
Date: 2018-12-07 17:49
Author: AdminContagio
Category: Comunidad, Nacional
Tags: Bella Flor Remacho, Jiguamiandó, Zona humanitaria
Slug: comunidades-de-jiguamiando-denuncia-ataque-militar-aereo
Status: published

###### Foto: 

###### 07 Dic 2018 

En horas de la noche del pasado 6 de diciembre, la comunidad de Bella Flor Remacho en el departamento del Chocó, denunció continuos ametrallamientos y bombardeos por parte del Ejercito a tan solo 20 minutos de la Zona Humanitaria de Nueva Esperanza, una de las áreas establecidas para refugiar a la población civil de Jiguamiandó y Curvaradó.

Según Benjamin Sierra, líder de la zona, la operación aérea que se extendió **desde las 9:35  de la noche hasta las 2:00 a.m, del 7 de diciembre,** obligó a los habitantes del lugar a prender hogueras y montar antorchas para evitar ser blanco de algunas de las nueve bombas disparadas en las cercanías del territorio y que desplazaron a varios de los pobladores a la zona humanitaria.

En horas de la mañana la parroquia local de Río Sucio y la **Comisión de Justicia y Paz** realizaron la debida misión humanitaria de verificación, a pesar que no fueron encontradas  personas heridas por el hecho, Sierra indicó que la situación en la comunidad es de zozobra pues desconocen si podrían repetirse los sucesos de la noche pasada, sin embargo han dejado en claro que por ahora no piensan abandonar la zona humanitaria.

Por su parte el Gobierno asegura que la acción militar se dio contra un blanco hasta el momento desconocido, la duda persiste pues, como advierte Sierra no existen grupos al margen de la ley alrededor de la zona y el lugar del ataque fue un sitio que los habitantes destinaron para realizar únicamente tareas manuales.

###### Reciba toda la información de Contagio Radio en [[su correo]
