Title: Haz parte de esta campaña para decirle no al sacrificio de perros en China
Date: 2015-06-19 13:55
Category: Animales, Voces de la Tierra
Tags: Change.org, China, Maltrato animal, Onda Animalista, tortura animal, tradiciones culturales con animales, Yulin
Slug: haz-parte-de-esta-campana-para-decirle-no-al-sacrificio-de-perros-en-china
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/06/dog_meat.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto:[www.onegreenplanet.org]

En Yulin, China, cada año se celebra una cruel tradición. Se trata del  [Festival “Come Carne de Perro”](http://www.vice.com/es/video/el-festival-de-la-carne-de-perro-de-yulin-628)  que este año se **celebra el 21 de junio, donde las personas torturan, queman, matan y comen caninos.**

Durante el "Festival", los asistentes se divierten mientras sacrifican miles de vidas de perros callejeros, secuestrados y animales que son criados en granjas únicamente con ese fin. Estos caninos, serán víctimas este domingo de la crueldad humana, ya que se golpea y descuartiza a los perros para luego ser transformados en comida.

Desde Onda Animalista, invitamos a las personas a hacer parte de una campaña que desarrolla la plataforma **Change.org, donde se están recogiendo firmas para que se prohiba el Festival “Come Carne de Perro” de Yulin,** en China, que se ha permitido y reproducido durante años.

Change.org invita a que se unan las voces que defienden la vida en todas sus formas, “para mantener la esperanza en la humanidad, y tener un mundo más compasivo donde todos luchemos a favor de la vida de seres inocentes como los animales, y no solo la de la humanidad. **Si ignoramos el dolor, y el llanto de estos cachorros, significa que realmente hemos dejado de ser "una especie humana".**

[Aquí puedes hacer parte de esta campaña.](https://www.change.org/p/detengan-el-festival-come-carne-de-perro-de-yulin-yulindogmeatfestival?recruiter=256827151&utm_source=share_petition&utm_medium=twitter&utm_campaign=share_twitter_responsive)
