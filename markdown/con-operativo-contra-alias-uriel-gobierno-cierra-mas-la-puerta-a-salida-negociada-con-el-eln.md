Title: Con operativo contra alias “Uriel”, Gobierno cierra más la puerta a salida negociada con el ELN
Date: 2020-10-26 19:52
Author: AdminContagio
Category: Actualidad, Nacional
Tags: Alias &quot;Uriel&quot;, Chocó, ELN, Nóvita
Slug: con-operativo-contra-alias-uriel-gobierno-cierra-mas-la-puerta-a-salida-negociada-con-el-eln
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/10/alias-Uriel-ELN.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

Este domingo el presidente Iván Duque y el ministro de Defensa, Carlos Holmes Trujillo, confirmaron **la muerte de** **Andrés Felipe Vanegas Londoño, alias “Uriel” jefe político del Frente Occidental del Ejército de Liberación Nacional -ELN-**; en medio de un operativo militar adelantando por las Fuerzas Armadas en Nóvita, Chocó denominado “Operación Odín”.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Luego de la operación, **se alertó sobre la posible presencia de menores en el campamento donde fue abatido el alto mando del ELN**; lo cual fue confirmado por el comandante del Ejército Nacional, Luis Fernando Navarro; quien señaló que en las labores de inteligencia se pudo determinar que había dos menores de edad en dicho campamento que no hacían parte del grupo armado.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/RoyBarreras/status/1320496942846271493","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/RoyBarreras/status/1320496942846271493

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/ArielAnaliza/status/1320497952889229317","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/ArielAnaliza/status/1320497952889229317

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

Aunque el comandante Navarro, afirmó que una vez realizado el levantamiento no había ningún indicio de que los niños hubieran resultado afectados en la operación militar; **no es la primera vez que las Fuerzas Armadas adelantan una incursión militar, aun conscientes de la presencia de menores en la zona en la que se despliegan los operativos.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Frente a esto, el ministro Holmes Trujillo justificó este proceder, asegurando en una rueda de prensa concedida en la mañana de este lunes que “*se pueden realizar operaciones militares cuando hay menores*”. (Lea también: [Presidente Duque y altos mandos militares sabían de los menores y autorizaron bombardeo](https://archivo.contagioradio.com/presidente-duque-y-altos-mandos-militares-sabian-de-los-menores-y-autorizaron-bombardeo/))

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Comunidades en el Chocó piden pacto humanitario y cese a la violencia

<!-- /wp:heading -->

<!-- wp:paragraph -->

Aunque la muerte de alias “Uriel” es catalogada por parte del Gobierno como un “*un paso muy importante en la creación de condiciones de seguridad*”; comunidades del Chocó junto con organizaciones sociales que adelantan su labor en la zona, coinciden en señalar que **la única manera de garantizar el cese de la violencia y la seguridad de los pobladores del territorio es la suscripción de un Pacto Humanitario** que involucre, no solo a los actores armados al margen de la ley, sino a las Fuerzas Oficiales y al Gobierno que las lidera; para evitar así que las comunidades sigan quedando en medio del fuego cruzado. (Le puede interesar: [Organizaciones sociales convocaron a un Pacto por la Vida y la Paz](https://archivo.contagioradio.com/organizaciones-sociales-convocaron-a-un-pacto-por-la-vida-y-la-paz/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En relación con esto, **la guerrilla del ELN le ha planteado en varias ocasiones al Gobierno un cese al fuego bilateral,** atendiendo también el llamado del Consejo de Seguridad de las [Naciones Unidas](https://www.un.org/es/) quien llamó a un “*alto al fuego humanitario global*” en medio del escenario de pandemia. Asimismo, el grupo armado ha instado al presidente Duque para reactivar los diálogos de paz que se habían iniciado en el Gobierno Santos, los cuales fueron suspendidos por el actual mandatario.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Pese a esos llamados, el Gobierno Duque se ha mostrado reacio frente a una salida negociada del conflicto con el ELN; primero suspendiendo, en enero de 2019, los diálogos de paz iniciados por el anterior Gobierno y segundo cerrando la puerta a la posibilidad de retomar los mismos. (Le puede interesar: [ELN propone cese bilateral y el Gobierno cierra la puerta a esa posibilidad](https://archivo.contagioradio.com/eln-propone-cese-bilateral-y-el-gobierno-cierra-la-puerta-a-esa-posibildad/))

<!-- /wp:paragraph -->

<!-- wp:quote -->

> “Durante 50 años de conflicto hemos visto titulares con “bajas de los cabecillas” y la guerra sigue. A un mando lo reemplaza otro. Al final, sólo la solución negociada a los conflictos y los cambios estructurales en las sociedades logran parar las guerras”
>
> <cite>Roy Barreras, Senador de la República</cite>

<!-- /wp:quote -->

<!-- wp:heading {"level":3} -->

### Chocó sitiado también por paramilitares

<!-- /wp:heading -->

<!-- wp:paragraph -->

El Chocó no solo sufre el accionar del ELN sino también un intenso accionar de grupos paramilitares. Organizaciones sociales han denunciado el aumento en el control de las autodenominadas Autodefensas Gaitanistas de Colombia -AGC- en las áreas rurales del Bajo Atrato y Bojayá. El accionar de este grupo se extiende a varias zonas entre las que se encuentran, Cacarica, Curbaradó, La Larga, Pedeguita Mancilla y Jiguamiandó donde pobladores se han visto obligados a desplazarse ante las amenazas de los paramilitares y el control territorial que estos ejercen. (Le puede interesar: [Control paramilitar de AGC se extiende desde frontera con Panamá, Bajo y Medio Atrato](https://archivo.contagioradio.com/no-es-solo-bojaya-bajo-atrato-y-dabeiba-blanco-de-operaciones-paramilitares/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Ante este panorama, la operación militar que conllevó la muerte de alias “Uriel”, ha generado que se exija al Gobierno y las Fuerzas Militares la misma efectividad y contundencia en contra de los grupos paramilitares, frente a los cuales el Estado no parece actuar con la misma determinación; llegando incluso a denunciarse por parte de las comunidades, la connivencia existente entre fuerzas oficiales y paramilitares.

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
