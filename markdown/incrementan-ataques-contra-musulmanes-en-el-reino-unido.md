Title: Aumentan las agresiones contra Musulmanes en Reino Unido
Date: 2015-11-18 14:09
Category: El mundo, Otra Mirada
Tags: Comunidad musulmana, islamofibia, La Comisión Islámica de Derechos Humanos, La Comisión Islámica de Derechos Humanos (CIDH) informó que los ataques islamófobos contra la comunidad musulmana en el Reino Unido, Reino Unido
Slug: incrementan-ataques-contra-musulmanes-en-el-reino-unido
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/11/2050bf07cb647c50c4f7c107cf33f9bd_article.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto:actualidad.rt.com 

###### [19 nov 2015 ]

La Comisión Islámica de Derechos Humanos (CIDH), informó que  los ataques islamófobos contra la comunidad musulmana en el Reino Unido van en aumento. El abuso, la discriminación y la amenaza de asalto violento se han convertido en una **"experiencia normal"**.

El informe revela que las políticas del Gobierno Británico están transmitiendo un ambiente de odio contra los musulmanes que viven en el Reino Unido; a su vez registra un incremento en el número de personas que reportaron abuso verbal y el número de ataques físicos desde la última encuesta realizada en 2010.

Las cifras publicadas en los últimos meses por la Policía Metropolitana  indican un alto número de crímenes de odio reportados contra los musulmanes; en Londres, se registraron 818 incidentes en el año hasta septiembre de 2015, comparado con el año pasado, fueron 499. Se establece que se dio un aumento del 64 % en estos tipos de hechos.

**"Los musulmanes en el Reino Unido se sienten controlados por los medios de comunicación y las instituciones políticas contribuyendo hacia un clima de deterioro de miedo, un aumento de los grupos de extrema derecha y un aumento del racismo anti-musulmán. Actualmente la mayoría de los musulmanes  sienten que son odiados”** menciona el informe.

Tras los ataques terroristas  ocurridos en Francia el pasado viernes 13 de noviembre, El Concejo Musulmán de Gran Bretaña pidió incluír participación de musulmanes en la creación de una nueva legislación antiterrorista.

Así mismo El Concejo Musulmán de Gran Bretaña afirmo que  “**Los musulmanes británicos deben estar en el corazón de la creación de una nueva legislación antiterroristas si se quiere que sea eficaz”**. Adicionalmente Migdaad Versi Secretario General de MCB aseguró que las autoridades estaban cometiendo un grave error al confundir puntos de vista conservadores con extremismo violento.
