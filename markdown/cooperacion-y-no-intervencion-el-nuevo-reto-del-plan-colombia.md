Title: Cooperación y no intervención: el nuevo reto del Plan Colombia
Date: 2016-02-04 11:41
Category: Otra Mirada, Paz
Tags: plan Colombia, Plan Colombia segunda etapa
Slug: cooperacion-y-no-intervencion-el-nuevo-reto-del-plan-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/02/Militares-EEUU-en-Colombia.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Webinfomil.] 

###### [4 Feb 2016. ]

A propósito del balance sobre la implementación del Plan Colombia durante estos 15 años y una posible ejecución de su segunda etapa, diversas organizaciones defensoras de los derechos humanos insisten en que debido a las [[nocivas consecuencias](https://archivo.contagioradio.com/plan-colombia-no-ha-significado-mejoria-para-las-comunidades-conpaz-al-presidente-obama/)] de esta primera fase es necesario que se construya **un proyecto completamente distinto en contenido y desarrollo**, que más allá del intervencionismo fortalezca la cooperación internacional para las transformaciones que requiere el posconflicto.

Reinaldo Villalba, vicepresidente del ‘Colectivo de Abogados José Alvear Restrepo’ CAJAR, asevera que **una segunda fase del Plan Colombia no puede orientarse a “la militarización de la sociedad colombiana”**, como ha ocurrido durante estos 15 años, sino que debe posibilitar la eliminación de las causas estructurales que han motivado el conflicto social y armado en nuestro país, y en esa medida **los recursos deben disponerse para el fortalecimiento de la inversión social**, teniendo en cuenta a las comunidades para la implementación de los programas que se acuerden.

"Sí el acuerdo se da en La Habana, hay que construir paz, democracia y país, y **la comunidad internacional debe participar en términos de cooperación y no de intervención**", afirma Villalba e insiste en que la reestructuración que requiere el Plan Colombia “debe respetar ante todo la soberanía del estado colombiano”, sin promover “el intervencionismo para la imposición de modelos y políticas”, y **compete a la sociedad civil movilizarse para garantizar la voluntad política de los Gobiernos** de Colombia y Estados Unidos en los cambios para el posconflicto.

###### Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u) ]o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU) ] 
