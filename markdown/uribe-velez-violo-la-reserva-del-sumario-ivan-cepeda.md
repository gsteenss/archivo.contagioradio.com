Title: Uribe Vélez violó la reserva del sumario: Iván Cepeda
Date: 2019-10-09 13:26
Author: CtgAdm
Category: Judicial, Política
Tags: Alvaro Uribe, Corte Suprema de Justicia, Iván Cepeda, Sumario
Slug: uribe-velez-violo-la-reserva-del-sumario-ivan-cepeda
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/Uribe.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Video Álvaro Uribe Vélez  
] 

El pasado martes se cumplió el llamado a indagatoria de la **Corte Suprema de Justicia (CSJ)** al expresidente y senador Álvaro Uribe Vélez, en el marco del proceso por la presunta compra de testigos. Para el también senador Iván Cepeda, contraparte, la jornada transcurrió con normalidad y significó un momento trascendental en la historia del país. Sin embargo, Cepeda afirmó que un hecho 'marcó' el final de este día, en medio de un acto político electoral y de intervención sobre el proceso judicial.

### **Un día trascendental en la historia del país** 

Desde tempranas horas de la mañana se produjeron concentraciones de ciudadanos que apoyaban a Uribe y de quienes lo cuestionaban, el Senador Uribe llegó al Palacio de Justicia sobre las 8 de la mañana, y junto a sus abogados ingresaron al recinto para responder el cuestionario que adelantaría el magistrado César Augusto Reyes de la sala de instrucción de la Corte Suprema de Justicia. Según Iván Cepeda, las manifestaciones que se generaron alrededor del proceso en general se expresaron de forma pacífica y civilizada.

Cepeda calificó la jornada como "trascendental para la historia del país", porque el máximo tribunal de justicia penal en Colombia (la CSJ) llamó a indagatoria a una persona que ostenta una condición de ex mandatario y debe rendir explicaciones. Ello significa "un nuevo punto de lo que ha sido el desarrollo por la verdad y la justicia en Colombia porque se instaura un principio de cualquier cultura democrática: que cualquier ciudadano, independiente de su condición (...) del poder que ostente o al influencia en la opinión pública, debe rendir cuentas ante la justicia", explicó el senador.

Otro de los elementos que resaltó el senador por el Polo fue que **"si la Corte Suprema toma decisiones como las que ha tomado no es el fin del mundo"**, es decir, que contrario a lo que se ha expresado en redes sociales y algunos medios de comunicación empresariales, "acá no hay ningún peligro de que por que se desarrolle este proceso judicial se genere una situación caótica en el país". (Le puede interesar:["Piden respeto a la Corte Suprema por indagatoria a Álvaro Uribe"](https://archivo.contagioradio.com/piden-respeto-a-la-corte-suprema-por-indagatoria-a-alvaro-uribe/))

No obstante, Cepeda manifestó que percibió respecto al Presidente de la República, la Ministra de Interior y el Embajador de Colombia en Estados Unidos una intervención "a una sola voz" en defensa del senador Uribe "de una manera impúdica, y recordó que **"la rama ejecutiva debe guardar silencio sobre lo que está decidiendo la Corte, y no intentar influir para que la opinión pública crea tal o cual cosa"**. (Le puede interesar: ["Uribe ha elegido la vía de la presión mediática y política contra el poder judicial: Iván Cepeda"](https://archivo.contagioradio.com/uribe-ha-elegido-la-via-de-la-presion-mediatica-y-politica-contra-el-poder-judicial-ivan-cepeda/))

### **La mancha del día de la indagatoria** 

Una vez terminada la indagatoria el expresidente y senador se dirigió a la sede del Centro Democrático, allí se refirió a la jornada, así como a algunas de las pruebas presentadas en el proceso, y finalizó con un llamado a votar por Miguel Uribe Turbay. Para Cepeda, la mancha de este día fue la intervención del senador Uribe ante sus seguidores en medio de un acto que fue político electoral y de intervención sobre el proceso, en el que Uribe Vélez "**violó la reserva del sumario**, e hizo una alusión pormenorizada a todos los puntos del proceso, diciendo toda clase de mentiras".

Según Cepeda, todos los asuntos del proceso no deben ser manifestados públicamente porque tienen dicha reserva, lo que significa que públicamente no se puede hacer referencia a ellos. Para él, este acto público fue un "intento por encubrir los problemas que enfrenta el proceso con el discurso electoral, porque lo que es evidente es que Uribe y su defensa están en aprietos para dar explicaciones coherentes sobre lo que han sido todos los hechos que la opinión pública ha conocido poco a poco".

### **¿Qué viene para el proceso luego de la indagatoria?** 

Cepeda aseguró que lo que corresponde es informar a la Corte sobre la violación del sumario, y además, esperar la decisión que tomará el magistrado de la sala de instrucción sobre el actual momento procesal: "Como se sabe, ayer quedó vinculado de manera formal el senador Uribe al proceso, y **ahora se decidirá si esa vinculación también debe estar acompañada de una medida de aseguramiento**". Posteriormente, el senador señaló que probablemente se practicarán otras pruebas, y después de un tiempo que aún no se conoce, se tomará la decisión sobre si se archiva el proceso or por el contrario, se llama a juicio.

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
