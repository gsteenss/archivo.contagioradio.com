Title: Más de 170 regiones europeas han prohibido los cultivos transgénicos
Date: 2015-06-03 14:42
Category: El mundo, Otra Mirada
Tags: cultivos transgénicos, Ecologistas en Acción, Ecologistas en Acción publica un cuaderno explicativo ZONAS LIBRES DE TRANSGÉNICOS, españa, Monsanto, Organismos Modificados Genéticamente, Unión europea
Slug: mas-de-170-regiones-de-europa-han-prohibido-los-cultivos-transgenicos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/06/Monsa-4.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: [nicaraguaymasespanol.blogspot.com]

<iframe src="http://www.ivoox.com/player_ek_4591040_2_1.html?data=lZqmk5WYdI6ZmKiakpmJd6KklpKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRicTjzdTUy9jYpdSfxtOYo8jHrYa3lIqvldOPtNbWzc7Qw5DZsozX1sbRx9fSs4zZ2dXZy8jFuMrq0JCah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Gabriela Vásquez, Ecologistas en Acción ] 

Ecologistas en Acción publica un cuaderno explicativo sobre las zonas libres de transgénicos, donde se trata de despejar algunos mitos en torno a los **Organismos Modificados Genéticamente (OMG), que son una amenaza para el ambiente y la agricultura a pequeña escala.**

Los países que han acogido estas zonas libres de transgénicos pretenden proteger a las poblaciones locales, debido a que existen muchos **mitos alrededor de estos cultivos transgénicos,** como por ejemplo, que son totalmente seguros para la salud humana. Frente a esto se ha demostrado que no existen los estudios necesarios y verídicos para que las personas puedan consumir alimentos producidos a base de semillas transgénicas, como lo afirma Gabriela Vásquez, integrante de Ecologistas en Acción.

Debido a la falta de exámenes de toxicidad a largo plazo y otros estudios adecuados, se ha comprobado el daño que generan los transgénicos no solo para la salud, sino también para el ambiente y el campesinado. Es por eso que **más de 170  regiones europeas han decidido prohibir este tipo de cultivos como una medida de precaución. **

Actualmente, las zonas libres de transgénicos, es una medida que se ha extendido en varias partes del continente europeo, por ejemplo, en 3 provincias de Finlandia, 9 estados de Alemania, 21 regiones de Croacia y 60 áreas del Reino Unido.

Así mismo, países como **Austria, Francia y Bulgaria, han prohibido el cultivo de maíz transgénico que en España pretende presentarse como inofensivo.** Por otra parte, “en Alemania se ha impuesto un etiquetado especial para productos de animales alimentados con piensos libres de OMG y en Finlandia hay restricciones específicas para los comedores públicos”, señala Ecologistas en Acción.

El colectivo ecologista publica este cuaderno explicativo, donde demuestra algunos de los riesgos cuando se permite los cultivos transgénicos y se evidencian casos específicos sobre el daño que puede llegar a causar  el cultivo de OMG, así mismo contribuye con la explicación sobre la falta de estudios científicos.

[Zonas Libres Transgenicos](https://es.scribd.com/doc/267582470/Zonas-Libres-Transgenicos "View Zonas Libres Transgenicos on Scribd") by [Contagioradio](https://www.scribd.com/contagioradio "View Contagioradio's profile on Scribd")

<iframe id="doc_32267" class="scribd_iframe_embed" src="https://www.scribd.com/embeds/267582470/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-4ojzkdwj8q3x9bGiO6Ak&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="0.7044566067240031"></iframe>
