Title: Boletín informativo abril 21
Date: 2015-04-21 17:18
Author: CtgAdm
Category: datos
Tags: Conpaz, fecode, FLIP, Santos
Slug: boletin-informativo-abril-21
Status: published

[**Noticias del día:**]

<iframe src="http://www.ivoox.com/player_ek_4388383_2_1.html?data=lZilmpicd46ZmKiakpyJd6KlmZKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRhtDgxtmSpZiJhaXijK7byNTWscLoytvcjabGtsrgjJeej4qbh4630NPhw8zNs4zGwsnW0ZCRaZi3jpk%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

- El Presidente Juan Manuel Santos anuncia la reactivación del Consejo Nacional de Paz.

- De acuerdo a la organización británica Global Witness, Latinoamérica es la región más peligrosa para defender el ambiente. Colombia es el segundo país más peligroso para los ambientalistas, habla Tatiana Roa, Coordinadora General de Censat.

- La ANLA y la Dirección de Consulta Previa del Ministerio del Interior, adelantaron procedimientos ilegales de consulta previa en las comunidades indígenas que habitan en el Parque el Tayrona, con el objetivo de llevar a cabo el proyecto hotelero los ciruelos. Alejandro Arias, abogado de la confederación indígena tayrona, habla sobre porqué se sanciona a estas dos instituciones.

- Nuevamente los defensores y defensoras del humedal la conejera, están siendo hostigados por el ESMAD, y acompañados de funcionarios de la constructora Fontanar del rio, intentan ingresar material de construcción al humedal, Gina Díaz, quien hace parte del campamento que defiende la conejera denuncia esta situación.

- El Presidente Juan Manuel Santos, decidió proponer un cronograma a las conversaciones de paz que se llevan a cabo en la Habana, Luis Eduardo Celis de la Corporación Nuevo arcoíris, analiza esta decisión del Presidente.

- La red CONPAZ y el Movimiento de Víctimas de crímenes de Estado se manifiestan a favor de un cese bilateral del fuego y una verificación sobre los últimos hechos donde 11 militares murieron al norte del Cauca, habla Rodrigo Castillo, representante de CONPAZ.

- La Federación de Trabajadores de la Educación (FECODE), Central Unitaria de Trabajadores de Colombia (CUT) y la Unión Sindical Obrera (USO), anunciaron que mañana 22 de abril empezarán una jornada de paro nacional, Elias Fonseca, ejecutivo de la CUT habla sobre las razones del paro.

- La Fundación para la Libertad de Prensa, la FLIP, denuncia que algunos periodistas que tratan el tema de la restitución de tierras en Antioquia, están siendo presionados por accionistas del periódico El Colombiano, quienes además son propietarios de tierras en el Urabá, que son objeto de reclamaciones a través de la ley de restitución de tierras, Pedro Vaca, director de la FLIP habla sobre la denuncia.
