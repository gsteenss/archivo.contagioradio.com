Title: Indígenas Wounaan Nonam exigen el cumplimiento de sus derechos
Date: 2015-01-05 14:11
Author: CtgAdm
Category: Comunidad
Slug: indigenas-wounaan-nonam-exigen-el-cumplimiento-de-sus-derechos
Status: published

###### Foto&gt; Comisión Intereclesial de Justicia y Paz 

[Audio](http://ctgaudio.megadatesystem.com/upload/0/fsW9eA1e.mp3)

Las comunidades indígenas Wounaan Nonam, del reguardo indígena de Guayacán, en el Valle del Cauca, denuncian que a pesar de que la ONIC ha estado en disposición de trabajar con el gobierno en la creación de un decreto con el cual las comunidades indígenas puedan reclamar sus derechos; el gobierno a través de Juan Carlos Monroy de la Unidad de Víctimas, a llegado al Resguardo Indígena a socializar el decreto ley 46-33 del 2011, en la que no han participado las comunidades afectadas.
