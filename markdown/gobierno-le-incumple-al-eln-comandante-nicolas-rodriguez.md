Title: "Gobierno le incumple al ELN" Nicolás Rodríguez
Date: 2016-11-09 15:22
Category: Nacional, Paz
Tags: ELN, Mesa en Quito
Slug: gobierno-le-incumple-al-eln-comandante-nicolas-rodriguez
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/09/Nicolás-Rodríguez-ELN-.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: ELN] 

###### [9 de Nov 2016] 

El  Comandante del ELN, Nicolás Rodríguez Bautista, afirmó en una alocución que el **gobierno Nacional incumplió los puntos pactados el pasado 6 de octubre** frente a la liberación de los secuestrados en poder de esta guerrilla y la exigencia de liberar a Odín Sánchez, que de acuerdo con el Comandante “es uno de los 10 funcionarios más corruptos y depredadores del presupuesto del Chocó, e implicado con el paramilitarismo en esta región del país”.

Rodríguez, señaló que los **incumplimientos se dan en los mecanismos que se habían pactado para que se diera el inicio a la mesa de conversaciones en Quito**, en donde se indicó que el ELN liberaría a dos personas antes del 27 de octubre y una en el transcurso de la primera ronda de díalogo. De igual forma expresó que el gobierno tampoco ha nombrado a los dos gestores de paz para que se dé inicio a la mesa.

Frente a la liberación del tercer secuestrado, que sería Odín Sánchez, Rodríguez afirmó: “**hemos empeñado la palabra y cumpliremos siempre y cuando el gobierno también cumpla lo que le corresponde.** Así lo establecen los acuerdos firmados entre las delegaciones del ELN y el gobierno” además añadió que una vez se den estos cumplimientos se activarán los protocolos de liberación en coordinación con el gobierno y los organismos humanitarios.

De igual modo Nicolás Rodríguez expreso que no cree que haya existido una doble interpretación de los acuerdos porque “s**on perfectamente claros y sin margen para las dudas, confusiones o dobles interpretaciones**, tampoco creo que la delegación del gobierno haya mal informado al presidente”.

En la alocución el primer Comandante también asegura que no existe la división interna en las filas del ELN y que **todas las estructuras acatan las orientaciones de la Dirección Nacional y el Comando Central.** A su vez se refirió a otros temas como la Reforma Tributaria e indicó que “es un vulgar atraco a los sectores medios y empobrecidos de Colombia”. Finalmente Rodríguez hizo un llamado a  “plantarse alternativas donde los cambios vayan en la dirección de un gobierno de Nación, Paz y Equidad”. Le puede interesar:["ELN y Gobierno deben cumplir su palabra: Carlos Velnadia"](https://archivo.contagioradio.com/eln-y-gobierno-deben-cumplir-su-palabra-carlos-velandia/)

###### Reciba toda la información de Contagio Radio en su correo [[bit.ly/1nvAO4u]
