Title: ¿Por qué Semana y la Silla Vacía no se rectifican con Renán Vega?
Date: 2015-05-20 15:02
Author: CtgAdm
Category: Nacional, Política
Tags: comision, Comision historica, habana, paz, proceso de paz, renan vega
Slug: por-que-semana-y-la-silla-vacia-no-se-rectifican-con-renan-vega
Status: published

###### Foto: Anarikismo.net 

 

A raíz de la “columna de opinión” que apareció en la versión virtual de Semana el 9 de mayo, con el título “Más rigor, por favor”, escrita por Alfonso Cuéllar, le envíe al Director de Semana la réplica que ahora hago pública. Ni siquiera se acusó recibo de la misma. No me extraña, porque en febrero de este año, luego de la publicación del Informe de la Comisión Histórica, en la edición impresa de Semana se tergiversaron mis palabras. En esa ocasión dirigí una comunicación en la que exigía una rectificación y esta nunca fue publicada. Como abrebocas al escrito de respuesta a las infamias de A. Cuéllar, doy a conocer el texto enviado el 20 de febrero.

Renan Vega  
 

**NO MIRES LA PAJA EN EL OJO AJENO...**

*Respuesta a una columna de Alfonso Cuéllar publicada en la Revista Semana.*

###### *“\[Que\] peligroso... puede ser un micrófono en las manos de un cretino,* 

###### *cuando el tal cretino goza de total impunidad”.* 

###### *Alfonso Sastre, La batalla de los intelectuales,* 

###### *CLACSO, Buenos Aires, 2005, p. 57.* 

En la edición virtual de la Revista Semana apareció un artículo de opinión de Alfonso Cuéllar, con el título Más rigor, por favor, el día 9 de mayo de 2015 y dicho artículo permaneció colgado en un lugar visible de esa edición virtual por lo menos durante cuatro días (Nota al pie 1).

En este artículo se hacen unos señalamientos graves, irresponsables y sin el menor fundamento a mí, con nombre propio, y al informe que escribí para la Comisión Histórica del Conflicto Armado y sus Víctimas, continuando con la cadena de mentiras e infundios que iniciaron Adriaan Alsema de Colombia Reports y la Silla Vacía, con el fin de desvirtuar los resultados que presenta mi ensayo sobre la injerencia de Estados Unidos en Colombia y su directa participación en el conflicto armado que soportamos desde hace más de medio siglo. El mencionado “periodista” al iniciar su columna sostiene: “Todo parece indicar que la presunta violación de 53 menores por gringos en Melgar no tiene fundamento. Grave precedente”. A partir de tan poca seguridad, que se confirma con las palabras “todo parece indicar”, el autor sostiene, eso si ya sin ninguna duda, que “no hay evidencia alguna de que lo dicho por la Comisión y en particular, Vega sea cierto” afirmación recurre a una supuesta “investigación” de Colombia Reports y al artículo escrito por Miguel Rueda, de Fusión.

En mi respuesta a las mentiras divulgadas por La Silla Vacía, que son las mismas que repite Cuéllar, comprobé que sí existen evidencias de los abusos de militares de los Estados Unidos y la cifra de 53 casos yo no me la inventé ni mucho menos, sino que proviene del escrito académico de la investigadora Anna Kucia de 2008 sobre la influencia de militares de los Estados Unidos en algunos aspectos de la sociedad colombiana (Nota al pie 2).

Cualquiera que tenga dos dedos de frente puede detectar fácil y rápidamente que el artículo de Adriaan Alsema, en Colombia Reports, carece de cualquier fundamento serio, si tenemos en cuenta que su autor reconoce que no lee (porque supuestamente en el mundo moderno estamos muy ocupados), descalifica el informe porque no fue sometido a pares académicos (acaso los periodistas tienen pares académicos cuando escriben y difunden sus mentiras), le pide al rector de la universidad donde laboró que me obligue a hablar con los periodistas, descalifica mis méritos intelectuales por escribir en periódicos y publicaciones de izquierda, asegura que las cosas que yo digo en mi Informe no son ciertas porque son producto de un “antiamericanismo primario” y, lo más llamativo y cínico, que no han existido ni abusos sexuales ni violaciones por parte de militares y mercenarios de los Estados Unidos sino que esa es una invención propia de un simple “mito urbano” (Nota al pie 3) juntillas en este tipo de sandeces si indica la falta de rigor del señor Alfonso Cuellar y su altura investigativa y su seriedad a la hora de mentir y difamar. Por qué no comparó, con calma y rigor, las estupideces que se encuentran en el artículo de Colombia Reports con mi informe de 60 páginas, que se constituye en una explicación estructural y de largo plazo sobre la presencia de Estados Unidos en Colombia.

La afirmación más irresponsable, y carente de cualquier rigor, de Cuellar sostiene que soy “un académico que no le pareció necesario investigar antes de publicar; violando un fundamento de su profesión”(Nota al pie 4). Pues, quien no investigó y se limitó a reproducir los disparates de Colombia Reports y las frases descontextualizadas de Fusión fue el seudoperiodista Alfonso Cuéllar, quien parece que no leyó el Informe que yo escribí, el cual tiene un amplio respaldo documental, y donde además se exploran una diversidad de aspectos sobre la injerencia de Estados Unidos en Colombia, tales como su apoyo a la formación de grupos paramilitares, el patrocinio de la contrainsurgencia y el terrorismo de Estado, su formación doctrinaria a militares involucrados en los asesinatos de Estado (mal llamados “falsos positivos”), su patrocinio al uso del glifosato contra campesinos e indígenas, su alianza con grupos de asesinos (como los PEPES en Medellín) y un largo etcétera.

Carece por completo de cualquier rigor y seriedad un individuo que goza del privilegio de escribir en un medio informativo de los dueños de Colombia, para señalarme como irresponsable en mi labor investigativa, basándose en las “habladurías escritas” de Colombia Reports, sin preocuparse por indagar lo que allí se dice.

Además, a Cuéllar le preocupa que supuestamente haya impuesto una “verdad absoluta para la opinión pública”, que no aclara cuál es, pero que parece referirse al hecho de que son responsabilizados como causantes y generadores de violencia el Estado colombiano, las clases dominantes –así como sus medios de comunicación tradicionales, como la Revista Semana-. Esto es lo que le incomoda a nuestro “riguroso” opinólogo, quien no necesita ni siquiera leer, sino que deja que otros lean por él, y luego, con la impunidad que le da el poder de figurar en un poderoso medio de desinformación, calumnie y descalifique a quienes no están de acuerdo con las falsas verdades de las clases dominantes de Colombia. Porque, hay que subrayarlo con claridad, no estamos ante una simple opinión de Alfonso Cuéllar sino que él miente, desinforma y tergiversa en forma malintencionada, al decir, sin ningún respaldo, que yo no tengo evidencia documental sobre los abusos sexuales de militares de los Estados Unidos.

Porque, en últimas, lo que le preocupa a Cuéllar radica en que se sepa la verdad acerca de la responsabilidad de los poderosos de este país –incluyendo a sus sostenedores externos, es decir, los Estados Unidos–, lo cual lo reafirma al terminar sus “habladurías escritas”: “Temo que la ligereza con que se ha tratado esta historia sea apenas el preaviso de lo que nos espera si se constituye la tan mentada comisión de la verdad. Y lejos de tener una mayor claridad sobre qué pasó en estas difíciles décadas de guerra, terminemos ahogados en acusaciones sin fundamento. Y de ser así, que no cese la horrible noche (Nota al pie 5).

Claro, lo que le asusta a los dueños de Colombia –de los que Cuéllar se presenta como uno de sus voceros– es que se sepa la verdad de la responsabilidad directa de industriales, grandes comerciantes, bancos, terratenientes, periodistas y dueños de medios de comunicación, empresas multinacionales, de los máximos responsable del Estado durante los últimos 70 años y, por supuesto, de los Estados Unidos, en el patrocinio del terrorismo de Estado, con sus múltiples crímenes. Para el “riguroso” columnista Cuéllar deben ser acusaciones sin fundamento los 5.000 asesinatos de los “falsos positivos” durante los gobiernos de Álvaro Uribe Vélez, el patrocinio de la Chiquita Brands a los paramilitares en el Urabá (que mataron a miles de personas) y por lo cual esa empresa fue sancionada en Estados Unidos con 25 millones de dólares de multa, los desaparecidos del Palacio de Justicia, el bombardeo a Santo Domingo (Arauca) en 1998, cuando fueron masacradas 17 personas, por bombas que fueron lanzadas desde aviones de la FAC y con participación directa de mercenarios de los Estados Unidos, las torturas generalizadas durante el gobierno de Julio César Turbay Ayala... y miles de acontecimientos similares, propios del Terrorismo de Estado a la colombiana. Estas son las “verdades sin fundamento” que aterran al señor Alfonso Cuéllar y él, con su enorme rigor informativo, no quiere que se conozcan.

 

**Bogotá, febrero 20 de 2015**

**Director Revista Semana:**

En la edición No. 1711 –del 15 al 22 de febrero del 2015–, en el artículo “¿Cuándo empezó esta guerra?” comentan los informes de la Comisión Histórica del Conflicto Armado y sus Víctimas (CHCAV). En la página 63, en un recuadro, aparece entre comillas una afirmación que se me atribuye y, se supone, ha sido extraída en forma textual del Informe elaborado por mí o del resumen. Allí se dice: RENAN VEGA: “La guerra colombiana nace de la dependencia y adhesión de Colombia a los Estados Unidos, y se ha prolongado porque esta relación beneficia a las clases dominantes del país. El imperialismo ha favorecido la contrainsurgencia y el terrorismo de Estado”. Tengo que manifestar de manera categórica que esa afirmación no es mía. Es un resumen libre y ligero que hace el redactor del artículo y, aunque capta algunos de los aspectos centrales de mi Informe, se equivoca gravemente cuando dice que la “guerra colombiana nace de la dependencia y adhesión de Colombia a los Estados Unidos”. ¿Quién dice esto leyó de verdad y entendió el Informe?

En ningún lugar yo he dicho que la guerra se originó por acción o iniciativa de los Estados Unidos, puesto que hablo de una contrainsurgencia nativa que opera en Colombia desde la década de 1920, antes del surgimiento de la contrainsurgencia moderna, y las clases dominantes del país y el Estado libran una guerra contra la población colombiana, que comenzaron por sus propios medios desde mediados de la década de 1940, cuando se aniquiló al gaitanismo a sangre y fuego. En el camino, el anticomunismo de las clases dominantes se identifica con la política exterior de los Estados Unidos, cuya acción prolonga el conflicto y alimenta la contrainsurgencia y el terrorismo de Estado, desde la Guerra de Corea (1950-1953). Por esa circunstancia, defino la intromisión de los Estados Unidos como propia de de una “intervención por invitación”.

A la hora de hablar del conflicto armado en Colombia, los periodistas deben ser serios, responsables y veraces -máxime cuando se están glosando los ensayos de un estudio histórico- y no presentar una información sesgada y tergiversar el pensamiento de uno de sus autores. Por todo lo anterior, solicito la correspondiente aclaración.

**NOTAS AL PIE**

1\. Alfonso Cuéllar, Más rigor, por favor, disponible en http://www.semana.com/opinion/articulo/alfonso-  
cuellar-mas-rigor-por-favor/427074-3 . Alfonso Cuéllar, loc. cit.

2\. Ver Anna Kucia, The Complex Relationship Between Private Military and Security Companies and the Security of Civilians: Insights from Colombia. Disponible en http://www.google.com/urlsa=t&rct=j&q=&esrc=s&source=web&cd=1&ved=0CCEQFjAA&url=http%3A%2F%2Fwww.havenscenter.org%2Ffiles%2FKucia%2520PS%2520draft\_0.doc&ei=9otPVZbUO8aXNtKJgbAC&usg=AFQjCNHiEtDrESc5b5s6sG3Cu-1FQCtP6Q&bvm=bv.92885102,d.eXY

3\. Adriaan Alsema, How I helped a pseudo-scholar spread anti-American propaganda, disponible en http://colombiareports.co/how-i-helped-a-pseudo-scholar-spread-anti-american-propaganda/

4\. Alfonso Cuéllar, loc. cit. (Énfasis mío).

5\. Alfonso Cuéllar, loc cit.
