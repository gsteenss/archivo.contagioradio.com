Title: CIDH ordena a Colombia atender a adultos mayores indígenas Wayúu
Date: 2017-12-05 13:20
Category: DDHH, Nacional
Tags: La Guajira, Wayuu
Slug: cidh_estado_colombiano_indiegnas_mayores_wayu_guajira
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/12/la-lucha-wayuu-por-bahia-portete-body-image-1422200351-e1512495974737.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Ana Karina Delgado] 

###### [5 Dic 2017] 

[La Comisión Interamericana de Derechos Humanos, CIDH, concedió medidas cautelares a favor de los adultos mayores de la Comunidad Indígena Wayúu. El **Estado colombiano deberá garantizar la salud, la alimentación y el agua potable** a esta población vulnerable con el fin de proteger sus derechos a la vida e integridad personal. ]

Así lo señala ese organismo mediante la Resolución 51 de 2017, gracias a la solicitud y acompañamiento de la  abogada Carolina Sáchica Moreno que ha venido interponiendo una serie de herramientas jurídicas con las que se busca que el gobierno de respuesta a la situación de crisis humanitaria en la que vive la comunidad indígena víctima del **abandono estatal y de las actividades mineras de la empresa Carbones El Cerrejón.**

[Se trata de la tercera decisión de medidas cautelares que concede la CIDH a favor de la etnia; la primera fue en diciembre de 2015 siendo beneficiarios los niños, niñas y adolescentes y posteriormente fueron ampliadas a las madres gestantes y lactantes; y ahora los adultos mayores también deberán ser objeto de especial protección por parte del Estado colombiano.]

### **Hace falta voluntad política para cumplir a la CIDH** 

[No obstante, de acuerdo con la abogada, pese a lo que se ha dicho de parte de la CIDH, **en los últimos dos años han muerto más de cien beneficiarios de las medidas cautelares** a causa del incumplimiento por parte del gobierno, por ello esta vez se espera que el Estado actúe de manera eficiente para atender la situación de los adultos mayores.]

[En esa línea, se espera que el gobierno incluya a esa comunidad en los programas para atender las necesidades básicas de dicha población y así cumpla con los establecido desde la CIDH. **"El Estado ha adquirido un compromiso con el Sistema Interamericano de Derechos Humanos, y al ser Colombia parte de ese sistema, debe acatar esos requerimientos**", manifiesta Sáchica.]

[Para la abogada el paso a seguir debería ser un censo de la comunidad Wayúu de manera que pueda ser debidamente identificada e intervenida. Todo ello, "acompañado de la voluntad política" necesaria para atender a los adultos mayores, niños y mujeres que han venido siendo víctimas de los incumplimientos gubernamentales.]

<iframe id="audio_22479671" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_22479671_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
