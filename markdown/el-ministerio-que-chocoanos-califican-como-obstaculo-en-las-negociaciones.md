Title: "No estamos dispuestos a hacer una negociación mal hecha” Líder en Chocó
Date: 2017-05-26 13:47
Category: Movilización, Nacional
Tags: Chocó, Paro en Chocó
Slug: el-ministerio-que-chocoanos-califican-como-obstaculo-en-las-negociaciones
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/05/chocoparo-e1495824396688.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Colombia Plural] 

###### [26 may 2017]

Chocoanos afirman que el **Ministerio de Hacienda es el gran obstáculo para la toma de decisiones**. La poca disposición de crear mecanismos que financien y mejoren  infraestructura, vías y condiciones de vida de la población, ha mantenido al departamento en un olvido histórico.

Para Emilio Pertuz, integrante del comité de interlocución del paro, “**el gobierno ha mostrado poca claridad ante el cumplimiento de los acuerdos pactados”**. Por esto, los chocoanos continúan hoy las movilizaciones con actividades centradas en la participación de los jóvenes. Como el Día J o día dela juventud, han categorizado las movilizaciones que saldrán hoy por la tarde, desde el parque bicentenario en Quibdó. Le puede interesar: ["Chocoanos afirman que no están "dialogando con un gobierno serio"](https://archivo.contagioradio.com/chocoanos-continuan-en-paro-hasta-que-vaya-el-presidente-de-la-republica/)

Igualmente, en las horas de la tarde, una comisión delegada de la presidencia se reunirá con los pobladores para tratar de plantear una forma de financiar la salud y la construcción de vías de comunicación. Según Pertuz, **“el presidente ha asegurado que las comisiones que llegan a Chocó tienen poder de decisión.** Esperamos que sea así y que las decisiones tengan un sustento administrativo”.

El fin de semana, delegaciones del paro de chocó se reunirán con los encargados de las movilizaciones en Buenaventura para crear una postura de la región del pacífico. Pertuz afirmó que “en Chocó estamos preparados para seguir con el paro. **No estamos dispuestos a hacer una negociación mal hecha”.** De igual manera aseguró que de ser necesario, las movilizaciones llegarán a Bogotá para exigir que se respeten sus derechos fundamentales. Le puede interesar: ["Gobierno hace oídos sordos al paro en Chocó"](https://archivo.contagioradio.com/gobierno-hace-oidos-sordos-al-paro-en-choco/)

Los pobladores de esta región reconocieron y agradecieron la solidaridad de colombianos y ciudadanos de diferentes partes del mundo. Pertuz aseguró que **“estas demostraciones de afecto nos demuestran que no hay razones para desfallecer porque las reclamaciones que hacemos son justas”.**

<iframe id="audio_18922427" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_18922427_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU).
