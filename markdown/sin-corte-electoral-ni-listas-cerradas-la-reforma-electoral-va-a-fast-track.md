Title: Sin Corte Electoral ni listas cerradas la Reforma Electoral va a Fast Track
Date: 2017-05-17 13:55
Category: Nacional, Paz
Tags: Corte Electoral, Reforma Electoral
Slug: sin-corte-electoral-ni-listas-cerradas-la-reforma-electoral-va-a-fast-track
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/07/CONGRESO.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:] 

###### [17 May 2017] 

Cada propuesta sobre la Reforma Electoral "pisaba algún cayo" MOE

Ya se conocen las modificaciones a la propuesta de reforma electoral hecha por la Misión Electoral Especial (MEE), que **no contará ni con Corte Electoral, ni con listas cerradas para las próximas elecciones**, ambos puntos tenían como finalidad establecer una mayor veeduría y control sobre los partidos políticos y la corrupción.

**Alejandra Barrios, directora de la Misión de Observación Electora** e integrante de la MEE, expresó que el desechar la creación de la Corte Electoral evidencia el interés de los partidos políticos que no querían una reforma electoral estructural. Para ella la oposición estuvo, desde el principio, en cabeza de Mauricio Lizcano “cuando uno es presidente del Congreso de la República, claramente l**o que a uno le gusta o no le gusta tiene un peso específico**” señaló barrios.

Y agregó “**cada propuesta que tiene este documento toca algún cayo y eso significa que si la que presentó la MEE levantó todo ese debate**, no me alcanzo a imaginar los que va a levantar la propuesta que presentará el Gobierno Nacional, porque de alguna manera recoge algunas de nuestras recomendaciones y otras que hicieron las organizaciones políticas”. Le puede interesar:["Hay sectores a los que no les interesa una reforma electoral: MOE"](https://archivo.contagioradio.com/hay-sectores-que-no-les-interesa-reforma-electoral-moe/)

### **Consejo Electoral Colombiano no tendrá los mismos "dientes" de una Corte Electoral** 

Uno de los puntos claves de las recomendaciones de la Misión Electoral Especial era la posibilidad de despolitizar los trámites por denuncias contra los partidos políticos, puesto que el actual Concejo está constituido por cuotas políticas. Sin embargo, esta propuesta levantó ampollas y los partidos decidieron seguir nombrando a **sus propios jueces en el órgano que se llamará Consejo Electoral Colombiano, CEC.**

Con la desaparición de la Corte Electoral, queda aún la propuesta sobre la reestructuración del CNE, que se transformaría en el CEC, un órgano que estará conformado en principio por un cuerpo colegiado de 9 personas designadas por cada una de las Cortes y el Presidente de la República, y que una vez cumplido su periodo de 8 años, **serán ellos mismos quienes designen a sus sucesores. **Le puede interesar: ["Estas son las recomendaciones de la Misión  Electoral para la Reforma Política"](https://archivo.contagioradio.com/estas-son-las-recomendaciones-de-la-mision-electoral-para-la-reforma-politica/)

En cuanto a las funciones de esta institución, una de las modificaciones que se habrían realizado, es que este no solo tenga la posibilidad de toma de decisiones de carácter administrativo, sino también juridiccionales, es decir resolver problemas que se presenten al interior de los partidos políticos o de los candidatos antes de las elecciones, **funciones que actualmente tiene el Consejo de Estado**.

### **La reforma no alcanzaría a estar lista para las elecciones de 2018** 

Aunque se espera que a mediados de agosto, del próximo año, entre a regir el Consejo Electoral Colombiano, no se estaría hablando de las listas cerradas, sino semi bloqueadas, que apoyarían el orden que designe el partido. **Otras medidas para el control de la financiación de las campañas se implementarían después de 2018**.

Alejandra Barrios asegura, que es posible que la reforma electoral no pase a trámite en el Fast Track sino que sea una ley ordinaria lo que demoraría su aplicación, incluso, algunas medidas que se podrían aprobar de manera inmediata quedarían supeditadas a su paso por el congreso. Le puede interesar:["Sin Corte Electoral no estamos haciendo mucho"](https://archivo.contagioradio.com/sin-corte-electoral-no-estamos-haciendo-mucho-moe/)

###### Reciba toda la información de Contagio Radio en [[su correo]
