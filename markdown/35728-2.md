Title: Católicos piden perdón por implicación de la iglesia en conflicto armado colombiano
Date: 2017-02-03 12:40
Category: DDHH, Nacional
Tags: Iglesia, paz, Perdon, víctimas
Slug: 35728-2
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/02/CATOLICOS-PIDEN-PERDON1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [3 Ene. 2017] 

En una carta divulgada este viernes, más de 400 integrantes de la iglesia católica pidieron perdón por la participación de esa iglesia en el conflicto en Colombia. Además exigieron que la jerarquía eclesial también pida perdón, haga manifiestos públicos como ejemplo de reconciliación y aseguran que **pedirán al Papa Francisco que se cierre la diócesis castrense que funciona en Colombia**. Le puede interesar:[ Informe: Casos de implicación de la iglesia en la violencia en Colombia](https://archivo.contagioradio.com/una-confesion-misericordiosa-de-la-iglesia-colombiana/)

Hilda Giraldo, una de las promotoras de la iniciativa argumenta que la **fe católica debe dar ejemplo de humildad, de perdón y reconciliación en el contexto de construcción de paz** en Colombia y por eso, ellos y ellas decidieron tomar esa iniciativa, pero aseguró que es necesario que la jerarquía también de ejemplo y contribuya a la reconciliación de los colombianos y colombianas, puesto que la guerra está inmersa en la cotidianidad**. **Le puede interesar: [Iglesias y organizaciones de fe de DiPaz acompañarán las Zonas Veredales Transitorias](https://archivo.contagioradio.com/iglesias-y-organizaciones-de-fe-de-dipaz-acompanaran-las-zonas-veredales-transitorias/)

A manera de ejemplo Hilda Quiroga, asegura que su esposo, Jacinto Quiroga, un cristiano comprometido con los valores de la justicia y la dignidad para todos y todas, fue víctima de torturas y hostigamientos por más de 10 años hasta que fue asesinado en un operativo militar de la 5ta brigada en 1990, según el relato de Hilda, en medio de las torturas se daba cuenta de que los militares que lo torturaban recibían la comunión y el bautismo en las parroquias castrenses.

**“Mi esposo sufrió la persecución, la tortura, el encarcelamiento a conocimiento de ellos, porque tienen una vicaría castrense** o una diócesis castrense la cual tienen sus capellanes en las brigadas y mi esposo contaba que en los días de la tortura al otro día les ofrecían misa y le daban comunión a sus torturadores en presencia de ellos, (…) y el sacerdote se prestaba para todo eso (…). Eso nos parece una humillación terrible ver cara a cara como le daban la comunión a los criminales” contó Hilda.

“Mi esposo siempre decía ¿**cómo estos señores** que han recibido el bautizo igual que nosotros, que estamos presos, **pueden llegar a sus casas con sus familias con las manos tan manchadas de horrendos crímenes y vejámenes** hechos contra los prisiones y las mujeres?” relata la mujer.

Sin embargo, la situación de Jacinto Quiroga no fue la única, un reciente estudio reveló que en varios lugares de Colombia, se promovía la posibilidad de acabar con el “comunismo” como expresión diabólica, situación que generó centenares de hechos de violencia y de violación de Derechos Humanos, como en el caso del grupo “Los doce apóstoles” nombrados así por la participación del párroco de Yarumal, entre otros ejemplos.

Son varias las solicitudes que tiene esta carta de 4 páginas, que pretende en un contexto de paz ayudar a que las víctimas puedan perdonar las complicidades y los silencios de la Iglesia.

### **Invitación a realizar un acto simbólico** 

En la carta **se invita a la Conferencia Episcopal de la Iglesia católica a realizar un acto simbólico de carácter nacional en el que se pida perdón** y en el que sean invitados representantes de movimientos políticos liberales y comunistas porque según los y las firmantes **“la Iglesia contribuyó a la persecución de ellos”.**

Así mismo, solicitan que en todas las parroquias del país se haga lectura en medio de la Cuaresma de 2017 del texto de petición de perdón “que en la semana santa puedan hacerlo, que en sus sermones den a conocer esta carta para que el pueblo se entere y **ofrezcan un perdón convocando a organizaciones sociales, líderes, comunistas, liberales”** dijo Hilda.

### **Sacar restos mortales de Gonzalo Jiménez de Quesada** 

Según los católicos, se hace necesario sacar del recinto de la Catedral Primada los restos del conquistador Gonzalo Jiménez de Quesada y entregarlos a la Alcaldía de Bogotá para que les asigne otro espacio pues **“este hombre fue uno de los torturadores y asesinos de todo el pueblo indígena de Colombia**, que con la espada y la cruz amedrentaban a su pueblo y los masacraron por robarles sus tierras y el oro” añadió Hilda.

### **Cierre de la Diócesis Castrense** 

Quienes firman esta misiva se han comprometido en **solicitar al Papa Francisco cierre en Colombia la Diócesis Castrense** que se ha encargado desde su origen del cuidado pastoral y la administración de los sacramentos de las fuerzas militares en Colombia.

“Esta solicitud la hacemos porque en cada brigada tiene sus sacerdotes que **bendicen las armas de los militares para que vayan a masacrar al pueblo y eso es inhumano**, entonces por eso firmamos esa carta y le queremos hacer la solicitud al santo padre (…) ya que estamos en las puertas de una paz” añade Hilda.

Hasta el momento la Iglesia no se ha manifestado a propósito de esta solicitud, pero los firmantes en voz de Hilda aseguraron que continuarán trabajando para sus solicitudes sean escuchadas “seguiremos trabajando por la memoria de nuestras víctimas para un buscar un mejor vivir. Pero además quienes quieran formar la carta pueden escribirnos al correo [pedidoperdón@gmail.com](mailto:pedidoperdón@gmail.com) y se los enviaremos. Le puede interesar: ["Postura de la iglesia católica frente al género es contraria a las enseñanzas de Jesús"](https://archivo.contagioradio.com/postura-de-la-iglesia-catolica-frente-al-genero-es-contraria-a-las-ensenanzas-de-jesus/)

<iframe id="audio_16814510" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_16814510_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>  
<iframe id="audio_16818704" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_16818704_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio]{.s1}](http://bit.ly/1ICYhVU)[.]{.s1}

<div class="ssba ssba-wrap">

</div>
