Title: "Ayotzinapa una vuelta al sol" Erik Arellana
Date: 2015-09-26 10:00
Category: DDHH, Viaje Literario
Tags: 43 ayotzinapa poema, 43 estudiantes desaparecidos en Ayotzinapa, Erik Arellana, Masacre Iguala México
Slug: ayotzinapa-una-vuelta-al-sol-erik-arellana
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/ayotzinapa-8-2.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Ilustración: manuelhborbolla.wordpress.com 

###### <iframe src="https://www.youtube.com/embed/FDcuv12VHSA" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe> 

###### [26 Sep 2015] 

[Los estudiantes nos enseñan]  
[que debemos exigir respuestas]  
[y no aceptar informes oficiales]  
[que protegen criminales.]  
[Han creado un movimiento]  
[que en todo el planeta reclama:]  
[justicia y verdad]  
[desde su prolongada ausencia.]  
[Nos seguimos preguntando]  
[dónde están esos maestros]  
[que desde hace un año ya]  
[fueron raptados.]  
[El mal gobierno tiene miedo]  
[de que sean hallados]  
[insinúan con mentiras]  
[que los han incinerado]  
[y pretenden que con eso nos quedaremos callados.]  
[Cuarenta y tres mil veces gritaremos los nombres]  
[de quienes se han llevado]  
[una noche de septiembre]  
[del estado de Guerrero.]  
[No han podido borrarlos]  
[ni con terror, ni amenazas,]  
[ni mentiras bien planeadas,]  
[ni con sus guerras]  
[y valores de papel,]  
[ni con su muerte hecha ley,]  
[ni con asesinos en cartel,]  
[detendrán el movimiento hasta encontrarlos.]
