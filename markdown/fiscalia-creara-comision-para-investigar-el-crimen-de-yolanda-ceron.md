Title: Fiscalía creará comisión para investigar asesinato de Yolanda Cerón
Date: 2016-10-28 17:50
Category: DDHH, Nacional
Tags: colombia, nariño, paramilitares, Tumaco, Yolanda Cerón
Slug: fiscalia-creara-comision-para-investigar-el-crimen-de-yolanda-ceron
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/10/10-yolanda-ceron.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### Foto: Semana 

##### [28 Oct 2016]

Nuevas investigaciones que dan cuenta de la **participación de funcionarios del Estado y representantes de sectores económicos de la región**, fueron presentadas en el caso de la hermana Yolanda Cerón durante una nueva audiencia adelantada en el juzgado de ejecución de penas del  tribunal de Justicia y Paz. La religiosa fue **asesinada en el año 2001 por grupos paramilitares del bloque sur de las AUC**; a causa de su trabajo por el derecho a la titulación de la tierra a las comunidades afrodescendientes e indígenas del puerto de **Tumaco, Nariño**.

Durante la audiencia, realizada en doble jornada los días 25 y 26 de octubre, se hizo un seguimiento a la **sentencia de la Sala de Justicia y Paz del Tribunal Superior de Bogotá del 29 de septiembre de 2014, contra Guillermo Pérez Alzate, alias ‘Pablo Sevillano’**, ex jefe del bloque sur libertadores de las AUC, y varios de los hombres que estuvieron bajo su mando por su responsabilidad en varios crímenes ocurridos en la región desde el año 2000, haciendo énfasis en el asesinato de la religiosa y la importancia que tenia su labor para esas comunidades.

Alexander Montana, representante de las víctimas, asegura que uno de los exhortos que hace el tribunal de Justicia y paz es que "**la Fiscalía General de la Nación debe crear una comisión especializada que permita unificar todas las investigaciones sobre la actuación de grupos paramilitares y la participación en estos hechos de agentes del estado, sectores económicos de la región y de particulares**".

De acuerdo con el abogado, dicha determinación es importante por que la Fiscalía, a través de la oficina de compulsa de copias, **ha abierto varias investigaciones penales contra alcaldes de los municipios de Santa Cruz, Rosario, Barbacoas, San Pablo, San Bernardo, Pasto y Tumaco**. Así mismo, vincula a comerciantes de Chichagüí y a FEDEGAN como auspiciadores del bloque de las AUC que opera en la zona.

En concepto del defensor, el llamado a la Fiscalía es para que con la unificación de las investigaciones en la justicia ordinaria "no existan justificaciones para frenar las investigaciones y que por el contrario avancen" asegurando además que, **si el proceso es trasladado ante la eventualidad de la Jurisdicción Especial de Paz (JEP), "sería una gran oportunidad para que el Estado pueda avanzar y mostrar resultados**".

[Resultados y homenajes a Yolanda Cerón] 
-----------------------------------------------------------------

Montana asegura que la sentencia contempla que la comisión de la Fiscalía **deba rendir cuentas del resultado de su trabajo a la comunidad, organizaciones sociales, defensores de los derechos humanos y las demás victimas de este accionar criminal**. Al mismo tiempo, señala que se debe realizar u**n acto de conmemoración el próximo 31 de marzo** con participación de organizaciones de DDHH, donde las autoridades locales deben **hacer un informe y pedir perdón **por los crímenes que ocurrieron en la zona, invitando además al **Centro Nacional de Memoria Histórica  a realizar un informe acerca del trabajo y testimonio de 42 víctimas**, con especial énfasis en la vida y obra de la hermana Yolanda Cerón.

<iframe id="audio_13554496" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_13554496_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>
