Title: La violencia neoparamilitar en la frontera sur de Bogotá
Date: 2019-07-29 17:22
Author: CtgAdm
Category: DDHH, Nacional
Tags: Bogotá, Casa de Pique, Cuerpos Desmembrados, soacha
Slug: neoparamilitar-frontera-bogota
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/Cazucá.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Diario del Cauca  
] 

En días recientes fue encontrada una 'Casa de Pique' en Soacha, Cundinamarca; según líderes del lugar, la violencia en la frontera sur de Bogotá se debe a que es un lugar estratégico para ir al sur occidente del país, y allí tienen intereses grupos neoparamilitares que se vieron beneficiados por el desalojo del Bronx. De acuerdo a información de la Policía, la Casa está ubicada en altos de Cazucá, uno de los sectores donde se observa mayor cantidad de violencias y necesidades insatisfechas. (Le puede interesar:["¿Qué está detrás de los hechos de violencia al occidente de Bogotá?"](https://archivo.contagioradio.com/violencia-occidente-de-bogota/))

### **Violencia en la frontera de Bogotá y Soacha** 

**Heiner Gaitán, líder juvenil y candidato al concejo de Soacha** declaró que el sur de Bogotá y Soacha es un corredor estratégico porque es la ruta que conduce del centro del país al suroccidente; "esto hace que estructuras armadas hagan presencia en el territorio", explicó. A propósito de esta presencia, en los últimos dos años la Defensoría del Pueblo ha emitido cuatro alertas tempranas que cobijan localidades como Bosa, Ciudad Bolívar, Kennedy y Rafael Uribe advirtiendo sobre presencia de distintos grupos armados.

Según Gaitán, hay dos sectores especialmente donde se percibe la violencia: **La María, que limita con Bosa; y el sector de Altos de Cazucá;** "en los dos puntos operan mafias que han sido fortalecidas luego de la intervención del Bronx", añadió. De hecho, el Líder resaltó que en febrero explotaron dos granadas en La María; mientras en esa misma frontera entre Bogotá y Soacha el año pasado fue descubierto un laboratorio de procesamiento de Cocaína.

A ello se suma que en el vecino Municipio al sur de Bogotá se han encontrado cuerpos desmembrados desde 2016, lo que "nos deja muy preocupados porque se evidencia una realidad que ha sido denunciada por el movimiento social", según referenció Gaitán. Para él, el hallazgo de la 'Casa de Pique' debería motivar a las administraciones locales a investigar y dar seguimiento a las alertas tempranas de la Defensoría, así como tomar acciones contundentes para proteger la vida de la población.

### **¿Paramilitar, neoparamilitar, banda criminal o para policía?**

En una entrevista, el candidato al Concejo de Bogotá[David Flórez](https://archivo.contagioradio.com/paramilitares-asesinan-al-joven-indigena-nelson-domico-en-mutata-antioquia/) señaló que el fenómeno de violencia al sur de Bogotá se debe a grupos paramilitares con presencia en la Ciudad; por su parte, el profesor Carlos Medina Gallego manifestó que el cuerpo desmembrado que fue encontrado en Engativá respondía a una lógica parapolicial, de control por parte de fuerzas de seguridad del Estado contra aquellos que consideran que atentan contra la seguridad: prostitutas, jóvenes y consumidores de sustancias.

Para Gaitán, los panfletos que han aparecido en Soacha amenazando activistas y líderes están firmados por grupos que se pueden considerar parte del **fenónomo neoparamilitar: Águilas Negras y las autodenominadas Autodefensas Gaitanistas de Colombia (AGC)**; para el candidato al Concejo de ese Municipio, la presencia de estos grupos "obedece a una lógica de copamiento y sometimiento de la población gracias al narcotráfico y microtráfico". (Le puede interesar: ["Peñalosa y el fracaso de la política para atender a los habitantes de calle"](https://archivo.contagioradio.com/penalosa-fracaso-habitantes-de-calle/))

El término de neoparamilitar responde a un cambio de dichos grupos respecto del pasado, según lo explicó Gaitán, "estamos viendo que el paramilitarismo funciona como una red que aglutina pandillas, ladrones, de celulares, narcotraficantes, diferentes versiones de la economía irregular y que la organiza de diferentes formas". Eso significa que **estos grupos no están articulados de forma central ni jerarquizada**, porque tienen una fuerza aglutinante de acuerdo a los negocios irregulares que los mantienen como 'aliados'.

### **¿Qué hacer ante esta situación?**

Gaitán afirmó que en los dos sectores mencionados de Soacha se evidencia "usualmente la característica de no tener facilidad de acceso a derechos", mientras tanto, "las organizaciones criminales echan mano de la pobreza y miseria para contar con sus propósitos delictivos con personas que tienen necesidades". En ese sentido, concluyó que para sus propósitos no distinguen nacionalidad o género porque lo transversal "es que el crimen va a persuadir con mayor contundencia a quienes tienen necesidades con dinero y seguridad económica".

En cambio, criticó que ante estas situaciones las autoridades siempre se quedan con "la foto" de las capturas a los autores materiales de los hechos, **lo que no les permite identificar las lógicas y patrones a los que responde la violencia, y las formas de acabarla.** (Le puede interesar: ["Prácticas paramilitares del pasado resurgen en Sucre"](https://archivo.contagioradio.com/practicas-paramilitares-del-pasado-resurgen-en-sucre/))

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>  
<iframe id="audio_39200138" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_39200138_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
