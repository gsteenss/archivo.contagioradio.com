Title: Don Héctor abraza hoy a su hijo Héctor Jaime Beltrán, víctima de toma y retoma del Palacio de Justicia
Date: 2018-05-07 15:54
Category: DDHH, Nacional
Tags: Héctor Beltrán, Hector Jaime Beltrán, Palacio de Justicia, toma y retoma del palacio de justicia
Slug: don-hector-abraza-hoy-a-su-hijo-hector-jaime-beltran-victima-de-toma-y-retoma-del-palacio-de-justicia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/05/DcmJBQ-XkAAobMp.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [07 May 2018] 

**“Es el sueño que se le está cumpliendo, abrazarse nuevamente con su hijo”** esa es la frase con la que Pilar Navarrete, compañera de Héctor Jaime Beltrán, describe el encuentro entre padre e hijo. Entre don Héctor Beltrán, que desde el primer día de la desaparición de su hijo inició una larga lucha de más de 30 años por encontrarlo, y Héctor Jaime Beltrán, una de las víctimas de la toma y retoma del Palacio de Justicia.

Héctor Beltrán falleció el día de ayer, con la tranquilidad de haber encontrado los restos de su hijo, pero a la espera de que el Estado le contará qué pasó con el.El “suplicio de la larga espera” como el propio Héctor lo describe en su libro, que lleva el mismo nombre, inició el 6 de noviembre de 1985, ese día Jaime se encontraba en la cafetería del Palacio de Justicia, trabajando como mesero.

Pilar Navarrete afirmó que desde el primer momento en que se supo la desaparición de Jaime, su padre fue una pieza fundamental para continuar la búsqueda, “don Héctor trabajando, al mismo tiempo que buscando, dejo gran parte de sus obligaciones por esta búsqueda que nos llevó media vida”.

### **El encuentro con los restos físico de Jaime, pero no con la verdad ** 

El pasado 2 de junio de 2017, la Fiscalía anunció que los restos de Héctor Jaime habían sido hallados en la tumba del ex magistrado Julio César Andrade, en Barranquilla. Con esta identificación Beltrán se convertía en el 4 cuerpo entregado a las familias de las víctimas de la toma y retoma del Palacio de Justicia.

El 19 de septiembre, luego de 33 años, Héctor Jaime volvió a ser abrazado por su familia. Sin embargo, el tiempo ya había hecho de las suyas. “La desaparición hace mella en la vida, en la totalidad de la vida, en todo el tiempo en que existimos, se asume como algo adherido al cuerpo”, así describía don Héctor la soledad y el largo camino que tienen que padecer quienes buscan a sus seres queridos en un país de desaparecidos.

Este hecho, de todas formas, no significó el encuentro con la verdad de lo que le había ocurrido a Héctor Jaime esos 6 y 7 de noviembre de 1985 en el Palacio de Justicia, y por el contrario dejó más preguntas que respuestas, por ejemple **¿Por qué a parecen los restos de Héctor en la tumba del ex magistrado Andrade? **(Le puede interesar: ["Héctor Jaime Beltrán, desaparecido del Palacio de Justicia, abrió sus brazos para volar"](https://archivo.contagioradio.com/46816/))

### **El patriarca Héctor** 

Esos 33 años de búsqueda de seres queridos y de familias que se juntan bajo el mismo drama de la desaparición forzada, forjaron en las familias de las víctimas de la toma y retoma del Palacio de Justicia, lazos de fraternidad y compañerismo, que en el caso particular de don Héctor se tradujeron en liderazgo.

“**A él le decían el patriarca, así lo recuerda todo el mundo**. Era padre de 7 hijos a los que crío con dureza y responsabilidad. En la lucha, desde el momento en que se entera de la desaparición de Héctor Jaime, no dejó de buscar, no dejó de exigir y viajó por todos los lugares por donde pudo hablar” señaló Pilar Navarrete.

### **El Estado debe más que la verdad a las familias de las víctimas del Palacio de Justicia** 

Las familias de las víctimas del Palacio de Justicia han exigido desde el principio la verdad sobre sus desaparecidos y junto con ella, una reparación integral, que de acuerdo con el fallo emitido por la Comisión Interamericana de Derechos Humanos, también resguardaba los derechos de las familias a una salud digna y de calidad. No obstante, Don Héctor solo fue beneficiario de ese fallo al final de sus días, aunque padeció de varias complicaciones durante su vida, producto de la ardua búsqueda.

(Le puede interesar:["32 años después la Fiscalía sigue negando a los desaparecidos del Palacio de Justicia"](https://archivo.contagioradio.com/32-anos-despues-fiscalia-sigue-negando-a-los-desaparecidos-del-palacio-de-justicia/))

### **El día del abrazo llegó** 

Tras más de 30 años de búsqueda de la verdad, Pilar Navarrete aseguró que, por fin, Don Héctor está conociendo lo que realmente sucedió en voz de su propio hijo, “don Héctor va a saber la verdad, y tal vez, por qué no, si algún día encontraremos justicia”.

“La impunidad no es perfecta, porque no sabe prever la totalidad de la conducta humana y los escenarios límites en que los seres humanos no aguantan y deciden hablar” Héctor Beltrán.

<iframe id="audio_25839422" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_25839422_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
