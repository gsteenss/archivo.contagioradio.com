Title: Indígenas y afros se unen para presentar propuestas de paz en La Habana
Date: 2016-03-07 16:53
Category: Nacional, Paz
Tags: afro, Comisión Interétnica, indígenas, proceso de paz
Slug: indigena-y-afro-se-unen-para-presentar-propuestas-frente-al-proceso-de-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/03/proceso-de-paz-e1457387315878.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: El Pilón 

###### [7 Mar 2016] 

Este lunes se presenta la **Comisión Interétnica en la que organizaciones indígenas y afrodescendientes se unifican** para exigir que la mesa de diálogos de la Habana los atienda para explicarles los acuerdos alcanzados, pero también para que las delegaciones de paz de ambas partes tengan en cuenta sus preocupaciones y propuestas.

Esta Comisión es el resultado por una preocupación territorial, teniendo en cuenta **que cerca del 15% del total de la población colombiana está conformada por indígenas y afrocolombianos,** sin embargo,  esta población asegura que no han tenido participación en el proceso de paz y tampoco han sido consultados a pesar de los reiterados requerimientos que han hecho a la Mesa de Negociación de La Habana.

En ese sentido, los líderes de las comunidades negras e indígenas viajarán a La Habana, Cuba, para exigir la inclusión de su población en el proceso de paz, allí se presentarán las inquietudes y propuestas frente a lo acordado en La Habana, para que estas sean incluidas en el acuerdo final que está próximo a firmase entre el gobierno nacional y las FARC.

“Llego la hora de garantizar participación real y efectiva, no habrá Paz en Colombia, si no hay paz en los territorios étnicos”, asegura en un comunicado el Consejo Nacional de Paz Afrocolombiano, donde sostienen que **se quiere “evitar posibles conflictos en el posconflicto”,** pues señalan que si no se tiene en cuenta la mirada de las comunidades étnicas del país, es posible que se viva una nueva época de conflictos de manera que una de las propuestas va dirigida a garantizar la propiedad de la tierras para las comunidades y el reconocimiento del etnocidio.

En el Auditorio Kimy Pernía Domicó de la ONIC, se dio a conocer las apuestas, estrategias y rutas frente a los derechos territoriales y étnicos de los pueblos afro e indígenas, que **en próximos días serán presentados en la Habana.**

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
