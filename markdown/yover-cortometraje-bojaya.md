Title: "Yover"el cortometraje de Bojayá que se estrenará en la Berlinale
Date: 2018-01-19 08:00
Author: AdminContagio
Category: 24 Cuadros
Tags: Bojaya, Cine, colombiano
Slug: yover-cortometraje-bojaya
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/01/3_SC_2389-1-e1516226080183.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Ayer Película 

###### 17 Ene 2018 

Con una perspectiva diferente a la que en el imaginario colectivo se ha construido sobre Bojayá, el director manizalita Edison Sánchez llevará a la edición 68 de la Berlinale su cortometraje "Yover" como parte de la sección Generation categoría Kplus del Festival.

Yover narra la historia de un niño de doce años que debe trabajar. Todos los días recorre en su bici-carreta las calles del nuevo Bojayá, un pueblo sobreviviente a la más cruenta masacre de la guerra en Colombia, y subsiste, robándole a la realidad, minutos de fantasía y juego propios del mundo infantil.

“Esta película tiene como intención mostrar una perspectiva diferente a la del Bojayá que retratan los medios de comunicación tradicionales, que llaman al pasado para provocar tristeza de una forma visceral y contar así una única historia, sin matices, de la comunidad. Yover es reflejo de esa nueva configuración donde por encima de todo prima la amistad y la vida en comunidad. Ahí, en la selva, encontramos un poderoso mensaje para un planeta cada vez más convulsionado y dividido”, asegura su director.

Yover, encarna el perdón de un pasado que se revela a través de él y una fe por seguir adelante hacia un futuro de sencillas recompensas. Una historia sobre el trabajo infantil, el juego y la amistad. (Le puede interesar: [Víctimas de Bojayá piden respeto al duelo](https://archivo.contagioradio.com/victimas-de-bojaya-piden-respeto-de-su-intimidad-para-procesos-de-duelo/))

La banda sonora de la cinta cuenta con el tema "El Camino de Tutunendo" , interpretado por las Alabaoras de Bojayá, un grupo de mujeres que se han convertido en símbolo de perdón y reconciliación para el país, sus canciones hicieron parte de la firma del Acuerdo de Paz realizado en Cartagena el pasado 26 de septiembre de 2016 entre el Gobierno Nacional y las FARC.

La sección competitiva Generation, dedicada al cine protagonizado por niños y adolescentes, presentará entre el 15 y el 25 de febrero 65 películas de 39 países, entre largometrajes y cortometrajes en sus distintas categorías. El jurado compuesto por once niños y siete adolescentes decidirá que producciones se llevarán el Oso de Cristal.

De igual forma, dos jurados internacionales entregarán el Gran Premio del Jurado. Este año el Festival recibió más de 2.000 mil aplicaciones para Generation, de las cuales fueron escogidas 65 películas entre largometrajes y cortometrajes, representando a 39 países.
