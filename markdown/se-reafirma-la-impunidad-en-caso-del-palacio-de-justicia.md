Title: Se reafirma la impunidad con absolución de Plazas Vega por caso del Palacio de Justicia
Date: 2015-12-16 18:00
Category: Judicial, Nacional
Tags: Corte Interamericana de Derechos Humanos, Palacio de Justicia, plazas vega
Slug: se-reafirma-la-impunidad-en-caso-del-palacio-de-justicia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/12/medi131105alfonso-plazas-vega.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: semana 

###### [16 Dic 2015]

Se conoció que la sala penal de la Corte Suprema de Justicia acogió la ponencia del magistrado Luis Guillermo Salazar, que absolvería al Coronel ® Alfonso Plazas Vega, condenado en primera y segunda instancia por la desaparición de Carlos Rodríguez e Irma Franco a 30 años de prisión. Según la información que se ha conocido hasta el momento la votación habría sido 5 votos a favor y 2 en contra.

Recientemente el presidente Juan Manuel Santos realizó un acto de perdón en el marco del cumplimiento a la sentencia de la Corte Interamericana de DDHH que ordenó al Estado colombiano reparar a los familiares de las víctimas y orientar todas las acciones necesarias para esclarecer las desapariciones forzadas y llevar a los tribunales a los responsables, sin embargo la gran mayoría de las exigencias de ese tribunal siguen sin cumplirse.

Según la abogada de las víctimas, Liliana Avila, de la Comisión de Justicia y Paz, Plazas Vega comandó la operación de retoma del [Palacio de Justicia](https://archivo.contagioradio.com/page/2/?s=palacio+de+justicia) y tenía conocimiento de las órdenes que conllevaron a la desaparición forzada de** Carlos Rodríguez e Irma Franco**, [“como se ha demostrado en los tribunales”](https://archivo.contagioradio.com/plazas-vega-quedaria-en-libertad-palacio-justicia/)  que han estudiado el caso. Además Ávila afirma que conocen de la presión política a la que ha estado sometido el caso que se ha considerado de interés internacional.

En reiteradas entrevistas y actos públicos los [familiares de los desaparecidos han afirmado que esto representa una ofensa](https://archivo.contagioradio.com/que-se-cumpla-la-condena-de-plazas-vega/), en tanto pretende ignorarse la capacidad de mando que tenía el Coronel durante las operaciones de retoma del Palacio de Justicia, pasando por encima de las condenas proferidas en Colombia y en la Corte Interamericana de Derechos Humanos, mientras insisten en la necesidad de que los responsables revelen qué hicieron con las personas desaparecidas, y dónde se encuentran sus restos.
