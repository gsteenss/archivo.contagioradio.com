Title: Hacia la conmemoración de los 50 años de la muerte de Camilo Torres
Date: 2015-06-17 16:17
Category: Educación, eventos
Tags: 50 años, camilo torres, teologia de la liberación, Universidad Nacional
Slug: hacia-la-conmemoracion-de-los-50-anos-de-la-muerte-de-camilo-torres
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/06/CT-IMG.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Semanario Frente Unido 

<iframe src="http://www.ivoox.com/player_ek_4654662_2_1.html?data=lZuilpuado6ZmKiak5iJd6Kok5KSmaiRdo6ZmKiakpKJe6ShkZKSmaiRjMLXysaYzsaPp9Dizsra0dfFp8qZpJiSpJjSb8XZjNHc1ZCZdIzVhqigh6eVs9SfxcqYzsaPsdbZ09nSjcnJcYarpJKw0dPYpcjd0JC%2Fw8nNs4yhhpywj5k%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Ricardo Sánchez, Decano de Ciencias Humanas, Universidad Nacional] 

#####  

###### [17 jun 2015] 

La Universidad Nacional de Colombia se prepara para conmemorar los 50 años de la muerte de Camilo Torres, y para ello ha conformado una Comisión que de cuenta de sus aportes en diversos campos de la sociedad colombiana y latinoamericana.

Para el abogado e historiador Ricardo Sánchez, decano de la Facultad de Ciencias Humanas, la mejor conmemoración que puede hacerse a Camilo Torres es releyendo sus propuestas, evaluando su tiempo y entendiendo su significado para Colombia y el mundo.

Camilo Torres fue un importante artífice de la Teología de la Liberación, como corriente rebelde de la iglesia que abogó por el papel de la religión en la lucha por los derechos sociales; Fue uno de los fundadores de la entonces Facultad de Sociología en la Universidad Nacional de Colombia, junto al reconocido investigador Orlando Fals Borda; Fundó el movimiento social y político Frente Unido, que buscó unir a la izquierda colombiana acuñando el término de "clase popular" que denotara la amplitud y las características socio-económicas de la clase social, luchando contra el sectarismo político de la época, y en ese camino provocó escenarios de debate entre las diversas corrientes de izquierda; logró, en palabras de Ricardo Sánchez, "acercarse al corazón mismo de las gentes más necesitadas"; finalmente, Camilo Torres haría parte de la lucha armada en una de las guerrillas más antiguas de Colombia y América Latina, el ELN.

La Universidad se propone compilar y publicar la producción escrita de Camilo Torres, que incluye monografías, pronunciamientos, cartas y comunicados. "La universidad tiene que cumplirle a la memoria de Camilo Torres teniendo un producto académico certifico que permita estudiar en serio, de manera sistemática y ordenada, la complejidad de su pensamiento, y a partir de ahí leer el significado que él tiene en la vida nacional", afirma Sánchez.

La Comisión, conformada por los profesores Ester Perez, Alejo Vargas, Carlos Medina, Ramón Fallad, Genaro Sánchez, la profesora Rocío Londoño y el Padre Alfonso Rincón, hará público el segundo semestre del 2015 el cronograma de la conmemoración, que tendrá lugar entre febrero y junio del año 2016.
