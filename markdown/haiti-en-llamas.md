Title: Haití arde mientras el mundo lo ignora
Date: 2019-02-13 17:37
Author: AdminContagio
Category: El mundo, invitado, Opinion
Tags: Haiti, Jovenel, Movilizaciones, PetroCaribe
Slug: haiti-en-llamas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/02/Captura-de-pantalla-21-1.png" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/02/haiti-arde-770x400.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: [[[Johnson Sabin](https://www.facebook.com/johnson.sabin?__tn__=%2CdC-R-R&eid=ARBVhBNSYtSlSwHK4dbl13Zrnh4MnX1VSRHZKYlQvyB91toI11D-NBmidHfrMRUBRWYqupesE2Yd-PlC&hc_ref=ARSjITI3H6dhvAc6pfR7-6xPR87qZsPTJr2UVm5pFLJBeV9n6MY_Sd4rp2LH0Mo2qJU&fref=nf) ]

###### 13 Feb 2019

#### **[Por: Karen Varon Rojas\*]** 

**Haití** es conocido mundialmente por ser el país con los índices de desigualdad más altos de América Latina y el Caribe; también ha sido tema de conversación los últimos días debido a las publicaciones en prensa internacional y a las reacciones en redes sociales producto de las fuertes manifestaciones que vienen desarrollándose desde el pasado jueves en la capital y en distintas zonas del país.

Si nos proponemos por dos minutos tejer algo de memoria sobre este lugar, que parece ajeno para muchos en el mundo, podemos mencionar que fue la primera y única nación de esclavos negros que logró liberarse; que el kreyol o criollo haitiano (lengua materna y herencia de la revolución) es uno de sus idiomas oficiales además del francés, y aproximadamente un 99% de la población lo conoce y/o habla.

También podemos decir, que históricamente su lectura de la religión, de la espiritualidad, del arte, de la música y de la cultura han sido señaladas, estigmatizadas y juzgadas debido a que configuran una cosmovisión del mundo distinta a las convencionales y/o a las occidentales, por involucrar y reconocer el medio ambiente, la música, los tambores y los orígenes en la ancestralidad africana.

### **Un poco de contexto necesario - Peyi a lock** 

El 7 de febrero de 1986, J**ean Claude Duvalier** dictador haitiano conocido como “Baby Doc” fue derrocado por una revuelta popular dando fin a su atroz dictadura, a las constantes violaciones de derechos humanos y a los numerosos casos de corrupción que se vivieron durante su mandato. Paradójicamente, en esta misma fecha en 2017, Jovenel Moise, se posicionó como presidente de Haití.

2 años más tarde, es decir el pasado jueves 7 de febrero, iniciaron las fuertes manifestaciones en Puerto Príncipe y en distintas zonas del país reclamando la renuncia del mandatario, luego de que el Tribunal Superior de Cuentas emitiera un informe de auditoría que evidencia una infinidad de irregularidades, la terrible gestión de recursos y las posibles desviaciones de fondos prestados por Venezuela en 2008 para ayudar y potenciar el desarrollo económico y social de Haití con el programa de PetroCaribe.

El informe revela además la participación en este grave escándalo de corrupción de 15 exministros y altos funcionarios del gobierno, entre ellos el actual presidente Jovenel Moise, quien apareció como responsable de una empresa que se benefició de dichos fondos para la construcción de una carretera, por medio de un proyecto en el que no se encontraron contratos o procesos legales oficiales, y quien además siempre había defendido su inocencia en declaraciones pasadas cuando se referían a este caso.

Es importante señalar, que esta situación sale a la luz pública en un momento de tensión, ya que el pasado 5 de febrero, el Gobierno declaró el país en urgencia económica, lo que se ha visto traducido en la devaluación de la moneda local frente al dólar de manera exponencial las últimas semanas, una inflación cercana al 15% acumulada en 2 años, la escasez de combustible en el país que también resulta en una de crisis de electricidad y en general la imposibilidad de garantizar el acceso a alimentos básicos para suplir una canasta familiar.

### **7 días de fuertes manifestaciones - Peyi a lock** 

![Haiti](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/02/51919778_10217650776968857_851734238620286976_o-800x533.jpg){.alignnone .size-medium .wp-image-61718 width="800" height="533"}

Hoy las calles amanecen con un ambiente de incertidumbre en el séptimo día de manifestaciones convocadas por la oposición y diferentes sectores sociales reclamando la renuncia inmediata de Jovenel y el gobierno aún permanece en silencio; el único pronunciamiento lo hizo el secretario de gobierno Eddy Jackson Alexis el lunes 11 de febrero a través de un comunicado de prensa, en el cual rechaza la violencia y llama al diálogo entre la oposición y el gabinete del actual mandatario. La comunidad internacional y el sector económico nacional también emitieron un comunicado a través del Core Group llamando al diálogo entre ambas partes, no obstante, las protestas continúan en Puerto Príncipe y en el resto del país.

La situación es de tal urgencia que el día de ayer al menos 78 prisioneros de la cárcel civil en la comunidad de Aquin, escaparon en medio de las manifestaciones; la embajada estadounidense recomendó a mujeres, niños y personas no esenciales abandonar el país, y se percibe un ambiente de tensión e incertidumbre por una posible crisis migratoria.  
\_  
Ahora veamos en qué lugar tiene los ojos el mundo, veamos en donde centra su dolor selectivo, pues en este país, el Estado además de estar absolutamente ausente, también es represor y violento con los manifestantes: desde que iniciaron las protestas el número de muertos supera los 16 y hasta el día de hoy, según reportes no oficiales, la cantidad de heridos es desconocida (el reporte oficial de la PNH es de 4 muertos).

![Haiti](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/02/52602162_10217671069236151_5666038487447502848_o-3-800x532.jpg){.alignnone .size-medium .wp-image-61720 width="800" height="532"}

Veamos si su nivel de indignación permanece intacto cuando muchos de los muertos han sido consecuencia de la violencia policial y la imposibilidad del Estado por responder a las demandas de los manifestantes; o porque el acceso a salud y a educación es limitado y casi nulo; o en general, por las condiciones de vida en las que viven la mayoría de los haitianos que no suplen muchas de sus necesidades básicas.

En Haití no hay petróleo, y Estados Unidos ya vino “a salvarlo”, o mejor a intervenirlo (siempre luego de algún momento de desestabilidad política o algún fenómeno natural como el terremoto de 2010 o el Huracán Jeann en 2004), a través de la “donación” de casi 60 mil sacos de semillas híbridas de maíz y otros vegetales provenientes de MONSANTO, alterando la agricultura local y afectando la semilla nativa, porque nunca se explicaron los efectos futuros sobre el suelo y las posibles consecuencias de su uso en el medio ambiente y en la salud de las personas.

Organismos como la ONU ya se han pronunciado y la comunidad internacional también, de hecho, su presencia en el territorio haitiano ya tiene varios años; sin embargo, es de vital importancia señalar que la descomunal ayuda humanitaria y los mecanismos de control eran y/o siguen siendo el motor del fenómeno de corrupción que agobia este país. Un ejemplo de ello fue el despliegue militar que hubo con los llamados cascos azules que vinieron a “impartir orden y a traer la paz a las calles haitianas” en 2004 a través de la llamada Misión de Estabilización de las Naciones Unidas en Haití (MINUSTAH), no obstante, olvidaron mencionar que fueron dichos cuerpos de seguridad quienes también trajeron el cólera, violaron y dejaron a muchas mujeres haitianas en embarazo antes de retornar a sus países, entre otras graves vulneraciones a los derechos humanos.

El daño que le ha hecho la “ayuda humanitaria” a Haití, la sobre intervención de organizaciones no gubernamentales, los altos montos de dinero que le pagan a extranjeros en las organizaciones de “expertos” cuando en la realidad ni siquiera se les exige hablar criollo haitiano o hacer contacto con la gente en la cotidianidad, o con la cultura local. El complejo modelo de Estado, la centralización del poder en Puerto Príncipe y a su vez la gobernabilidad desdibujada ha resultado en la opción de desarrollo del país a manos de organismos internacionales sin una adecuada regulación por el Estado haitiano.

Lo anterior, es sólo una opinión que me permito construir luego de vivir dos años en este país, y trabajar con comunidades; es un llamado a analizar y a reflexionar cómo EEUU salva los países, con qué criterios, con qué objetivos, y sobre todo a repensar hacia dónde están nuestras preocupaciones, nuestra indignación, nuestro dolor y también nuestra indiferencia.

##### \*Socióloga colombiana, radicada en Haití desde marzo del 2017, donde trabaja con 3 comunidades en la periferia de Puerto Príncipe, en un proyecto educativo utilizando el fútbol y el juego como estrategias para desarrollar habilidades y fortalezas en niñas, niños y jóvenes, a través de una malla de resiliencia. 
