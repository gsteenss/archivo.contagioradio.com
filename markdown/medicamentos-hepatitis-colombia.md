Title: Farmacéuticas se oponen a declarar de interés público medicamentos contra la hepatitis C
Date: 2018-01-22 17:26
Category: Nacional
Tags: Acceso a medicamentos, colombia, hepatitis C, Misión Salud
Slug: medicamentos-hepatitis-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/01/1024x1024.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: SF Gate 

###### 22 Ene 2018 

La posibilidad de que el gobierno **declare como de interés público** los medicamentos de última generación desarrollados para combatir la hepatitis C en busca de **obtener las licencias obligatorias**, permitiría a los pacientes infectados acceder a tratamientos que en la actualidad pueden superar los cien millones de pesos; oportunidad que las multinacionales farmacéuticas buscan frenar a toda costa.

El 94% de efectividad que han alcanzado antivirales como **Harvoni, Daklinza y Sovaldi,** contrasta con el alto costo que representa adquirirlos. En el mercado estadounidense una dosis (pastilla) vale 1000 USD, lo que multiplicado por las doce semanas que demanda el tratamiento representa 80.000 USD por paciente. (Le puede interesar: [1200 personas mueren cada hora en el mundo por falta de acceso a medicamentos](https://archivo.contagioradio.com/cada-hora-mueren-en-el-mundo-1200-personas-por-falta-de-acceso-a-medicamentos/))

**Germán Holguín**, director de la organización Misión salud, asegura que a pesar del sistema de compra centralizada que el gobierno realizó en julio de 2017 a través de un fondo de la organización panamericana de la salud **para bajar el precio de los medicamentos en un 80%**, aún es demasiado elevado y únicamente personas con altos ingresos pueden costearlos.

Por ejemplo el tratamiento combinado de Daklinza y Sovaldi bajó de 137,2 millones de pesos por paciente a 29 millones, y el tratamiento con Harvoni, de 114,3 millones a 23,5 millones, ante lo cual el doctor Holguín asegura que en países como la India, se pueden pagar por sumas cercanas a los 300 dólares, de ahí la importancia de recurrir al mecanismo de las licencias obligatorias que ha demostrado ser "efectivo para reducir los precios de los medicamentos y mejorar el acceso"

De acuerdo con el Ministerio de Salud en Colombia **son cerca de 400 mil personas infectadas con hepatitis C** cuyo tratamiento costaría para el sistema de salud, con el costo regular, 40 billones de pesos, una cifra que desborda la cobertura del sistema de colombiano que **tiene un presupuesto total de 24,6 billones de pesos**.

"Es necesario tomar todas las medidas que estén al alcance del gobierno para asegurar que estos medicamentos de última generación lleguen a todas las personas que lo requieren" afirma Holguín resaltando que con la petición realizada al gobierno por la sociedad civil a través de la fundación Ifarma de declarar los medicamentos como de interés público "tendríamos un precio no de 24 millones sino de 2 millones o algo similar"

**La oposición de las** [**farmacéuticas**]** **

En carta enviada al gobierno nacional el gremio de mundial de PhRMA (Investigadores y Productores Farmacéuticos de América) solicitó la revocatoria de la resolución 5246 y el inicio de esta acción administrativa para evaluar si se requiere una declaración de interés público para garantizar el acceso a los antivirales de acción directa para el tratamiento de la hepatitis C.

Para la organización de la que hacen parte laboratorios como Bayer, Sanofi, Pfizer, Novartis y Merck,  la declaratoria sobre estos medicamentos "carece de fundamento" argumentando que ya existe competencia en este segmento comercial, que el costo ya ha sido reducido en el país y que no hay indicios de que exista una emergencia relacionada con la salud con respecto a la hepatitis C en Colombia.

La presión de PhRMA incluye una advertencia al gobierno sobre las consecuencias que tendría solicitar una declaración de interés público "no parece consistente con el deseo de Colombia de entrar a la OCDE", amenazas que desde el Ministerio de salud se reciben con prudencia pero con muchos cuestionamientos sobre el sustento de las  mismas.

La hepatitis C es una enfermedad contagiosa que cuando adquiere características permanentes puede tener complicaciones hepáticas mortales como cáncer de hígado y cirrosis y por consiguiente es una enfermedad que hay que atender con urgencia. La OMS estima que en la región de las Américas 0.7 por ciento de la población estaría infectada.

<iframe id="audio_23292991" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_23292991_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en su correo o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por Contagio Radio
