Title: Crímenes de Estado: el reto principal para la Fiscalía General de la Nación
Date: 2016-08-03 12:58
Category: Nacional
Tags: crímenes de estado, falsos positivos, Juan Manuel Santos, paz, posconflicto
Slug: crimenes-de-estado-el-reto-prinicipal-para-la-fiscalia-general-de-la-nacion
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/08/Fiscalía-General-e1470246650610.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Risaralda hoy 

###### [3 Ago 2016] 

Este lunes se posesionó como nuevo Fiscal General de la Nación, Néstor Humberto Martínez, junto a la abogada penalista María Paulina Riveros Dueñas como nueva Vicefiscal y Fabio Espitia Garzón director de la Unidad de Fiscales Delegados, un equipo que, de acuerdo con Alberto Yepes, integrante de la Coordinación Colombia Europa Estados Unidos, tendrá el gran reto de **esclarecer las responsabilidades de la criminalidad estatal para que en el posconflicto la Jurisdicción Especial para la Paz sea realmente funcional.**

En los discursos de Martínez y el presidente Juan Manuel Santos, el tema principal fue la corrupción más allá del papel de la fiscalía para el posconflicto. En ese sentido, Yepes, espera que **las promesas de afectar la corrupción generen resultados contra los grandes conglomerados económicos**, que se han lucrado de ese delito afectando los derechos humanos de los colombianos, por ejemplo con el robo de los 4 billones de pesos que pertenecía al sector de la salud.

“La corrupción ha servido como un tema taquillero para ocultar la falta de eficiencia en la lucha contra la impunidad, un tema que cuenta con un gran vacío frente al papel de la Fiscalía en la investigación de los crímenes de Estado", explica el vocero de la Coordinación Colombia Europa Estados Unidos.

Según él, **la Fiscalía debe saldar cuentas con el esclarecimiento de la criminalidad estatal,** por ejemplo sobre las más de 50 mil personas desaparecidas forzosamente sin prácticamente ningún responsable, cuando este delito obedeció a las actuaciones de grupos paramilitares de la mano de agentes estatales. Así mismo sucede con  las más de 6 mil ejecuciones extrajudiciales sin que los grandes responsables hayan pagado por esos crímenes, o las más de 5 mil compulsas hacia políticos y empresarios en el marco de la Ley de Justicia y Paz, sobre los cuales tampoco hay alguna condena.

Aunque desde distintos sectores se ha afirmado que **difícilmente el nuevo fiscal, que está tan vinculado a los grandes poderes económicos tenga la voluntad de llegar a los máximos responsables de la criminalidad estatal,** para la etapa de posconflicto es fundamental que la fiscalía abandone la paralización que ha tenido en sus funciones, pues la Jurisdicción Especial para la Paz deberá trabajar con base en los expedientes que entregue ese organismo, de ahí la importancia que el ente acusador se ponga al  día con el esclarecimiento sobre los crímenes de Estado, para que así “**la Fiscalía pueda convertirse en una entidad creíble para consolidar la paz y la democracia en el país”.**

<iframe src="http://co.ivoox.com/es/player_ej_12418947_2_1.html?data=kpehk52deJihhpywj5aXaZS1kpqah5yncZOhhpywj5WRaZi3jpWah5yncaLgw8rf1tSPncbkxtiSlKiPh9Dj08mbjajTsNDhw87OjarZttDkwpCy1dnFqNDnjLrby8nTt46ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)
