Title: Para el 2040 la Sierra dejaría de ser nevada
Date: 2015-05-14 13:51
Author: CtgAdm
Category: Ambiente, Entrevistas
Tags: arhuacos, Calentamiento global, cambio climatico, IDEAM, indígenas, Instituto de Hidrología, kankuamas, kogis, la Universidad Nacional, Meteorología y Estudios Ambientales de Colombia, Sierra Nevada de Santa Marta, Unesco, wiwas
Slug: nevados-de-la-sierra-nevada-de-santa-marta-desapareceria-en-el-2040
Status: published

###### Foto: [www.flickr.com]

Las comunidades indígenas y la Fundación ProSierra, aseguran que debido al acelerado descongelamiento de los nevados de la Sierra Nevada de Santa Marta, es posible que  para el año 2040 hayan desaparecido, lo que generaría que **los caudales de 30 ríos se vean afectados y puedan desaparecer.**

La fábrica de agua de **la Sierra Nevada pierde anualmente 1,3%  de hielo,** según un estudio del Instituto de Hidrología, Meteorología y Estudios Ambientales de Colombia, IDEAM, y la Universidad Nacional, donde también se registró que entre 1986 y 2002, la pérdida de hielo fue de 3,67% y si continúa así, para el 2040 ya no existirá hielo en la Sierra.

Lo preocupante, es que en de acuerdo a ProSierra los siete páramos que tiene Colombia constituyen 1.661.400 hectáreas, de esas, 85 mil hectáreas que representan el 5,1 por ciento, pertenecen a la Sierra Nevada de Santa Marta, lo que quiere decir que las fuentes de agua que **proveen a aproximadamente a 1,5 millones de personas, tendrían una reducción sustancial en la disponibilidad y calidad del agua,** ya que la desaparición de la cobertura  glaciar producirá la desaparición de muchos ríos.

Wiwas, arhuacos, kogi y kankuamos, aseguran que el mayor problema lo han generado los seres humanos que en nombre del desarrollo han emprendido proyectos que no son positivos en materia ambiental y en cambio generan  gran explotación de recursos naturales a partir de la minería y la  tala de árboles, entre otras actividades, que aceleran el calentamiento global.

Así mismo, el descongelamiento de los nevados afectaría **el río Palomino, que recibe el 25 % de la nieve; el Guatapurí que tiene el  8%, y el Aracataca que sería el más afectado al recibir el 67% de la nieve.** Lo que traerá como consecuencias la evaporación del agua, disminución de tierras húmedas, afectaciones en zonas de bosque y graves pérdidas de riqueza en fauna y flora, teniendo en cuenta que por su valor ambiental la Sierra Nevada de Santa Marta fue considerada por la Unesco en **1980 como Reserva del Hombre y la Biosfera.**

Este lugar **también es el hogar de aproximadamente 100.000 indígenas, arhuacos, wiwas, kogis y kankuamas,** que consideran sagrado su territorio. Ellos también vivirán las consecuencias del cambio climático ocasionado por el "hombre blanco", pues actualmente tienen problemas para conseguir y producir su comida, ya que han ido desapareciendo diferentes alimentos que producían como es el caso del aguacate.

Es por todo esto que los indígenas le hacen un llamado al “hombre blanco” para que replantee el significado de la palabra desarrollo y no siga ocasionando terribles efectos en el ambiente como ya se ve en la Sierra y en muchos otros lugares de grandes riquezas ecológicas para el país y el mundo.
