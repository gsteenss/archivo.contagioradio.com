Title: Comunidades de la Guajira demandaron a Cerrejón y piden anular licencia ambiental
Date: 2019-02-23 19:25
Author: ContagioRadio
Category: Ambiente, Comunidad
Tags: colectivo de Abogados José Alvear Restrepo, Comunidades Wayúu, Consejo de Estado, Guajira
Slug: comunidades-la-guajira-presentan-demanda-cerrejon-estado
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/11/el-cerrejon.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: laurismu] 

Comunidades de la Guajira, junto con organizaciones de derechos humanos, presentaron ante el Consejo de Estado una demanda en contra de Carbones del Cerrejón, pidiendo que el tribunal **anule la licencia ambiental otorgada al proyecto minero y que impida trámites para ampliar su actividad de explotación carbonífera**.

Según el comunicado, los demandantes sostienen que El Cerrejón violó normas ambientales y derechos de las poblaciones locales al modificar su licencia en el 2005 sin primero realizar un Estudio de Impacto Ambiental. Sostienen que la multinacional **aprovechó de sus recursos legales para evitar este trámite y pasar por encima de los estudios anteriores** que comprobaban los impactos a la salud y el ambiente ocasionados por este megaproyecto.

La demanda de 300 páginas "va dirigida a establecer la responsabilidad de la mina Cerrejón por la crisis humanitaria, la pérdida de seguridad alimentaria y la escasez de agua que ha ocasionado la muerte de 5.000 niños y la desnutrición de 40.000 más" en la Guajira. Ahí, presenta los resultados de estudios científicos sobre la contaminación, afectaciones en la salud e impactos negativos en el ambiente por la explotación de carbón, así como los relatos de las comunidades afectadas. (Le puede interesar: "[Cerrejón ha contribuido a la vulneración del derecho al ambiente sano en la Guajira: Indepaz](https://archivo.contagioradio.com/cerrejon-ha-contribuido-a-la-vulneracion-del-derecho-al-ambiente-sano-en-la-guajira-indepaz/)")

La acción legal también incluye a las autoridades del Estado, como el Ministerio del Medio Ambiente y Corpoguajira, **los cuales fallaron en realizar un control y seguimiento del megaproyecto y permitieron que El Cerrejón continuara expandiendo sus actividades** a través de 60 modificaciones de su licencia ambiental. Según Alirio Uribe Muñoz, abogado y defensor de derechos humanos, la demanda explica que con cada modificación, El Cerrejón "iba desapareciendo más arroyos, desplazando más comunidades y violando más normas".

Al respecto, la Corte Constitucional, la Corte Interamericana de Derechos Humanos y otros tribunales y organismos de control reconocieron en varias decisiones que El Cerrejón se encontraba responsable de violar los derechos de las comunidades indígenas y afrodescendientes de la Guajira a un ambiente sano, la salud y la participación.

En consecuencia, ordenaron a la multinacional de reducir sus niveles de contaminación y amparar los derechos de las comunidades, pero para los demandantes, El Cerrejón no les ha cumplido. Según el abogado Uribe Muñoz, esto se debe a que "El Cerrejón es una empresa que a nivel local tiene cooptado todo el poder judicial y las entidades de control."

Por tal razón, la demanda le pide al Consejo de Estado que ordene medidas cautelares "que suspendan cualquier trámite de modificación de la licencia, es decir, una expansión de la actividad de explotación de carbón, hasta que las autoridades ambientales no constaten la protección de los derechos colectivos e individuales afectados con esta autorización." Ademas, busca que se establece un plan de cierre para este megaproyecto.

<iframe id="audio_32770177" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_32770177_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
