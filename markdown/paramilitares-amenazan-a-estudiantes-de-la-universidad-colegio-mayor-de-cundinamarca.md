Title: Paramilitares amenazan a estudiantes de la Universidad Colegio Mayor de Cundinamarca
Date: 2015-05-30 10:06
Category: Nacional
Tags: Aguilas Negras, Jefferson Tunjano, Lorena Neira, Luz Ramírez, Nicolás Tamayo, Unidad Nacional de protección, Universidad Colegio Mayor de Cundinamarca
Slug: paramilitares-amenazan-a-estudiantes-de-la-universidad-colegio-mayor-de-cundinamarca
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/05/colegio-mayor-cundinamarca-contagioradio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: upcmcresiste.blogspot 

###### [29May2015]

Según la denuncia de los estudiantes, el pasado 27 de Mayo aparecieron en las instalaciones de la **Universidad Colegio Mayor de Cundinamarca**, una serie de panfletos amenazantes, firmado por las **Aguilas Negras**, en contra de algunos líderes estudiantiles que han denunciado los malos manejos administrativos, la corrupción y la exigencia de la** salida del Decano de la facultad de Derecho, Ricardo Martínez Quintero.**

[![amenaza-upcmc](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/05/unnamed1.jpg){.wp-image-9461 .alignleft width="278" height="464"}](https://archivo.contagioradio.com/paramilitares-amenazan-a-estudiantes-de-la-universidad-colegio-mayor-de-cundinamarca/unnamed-6/)La amenaza señala a los estudiantes **Luz Ramírez, Nicolás Tamayo, Jefferson Tunjano y Lorena Neira** y les advierte “*se callan y dejan de joder, o los callamos*”. Para los estudiantes estas amenazas tienen que ver con el **proceso de movilización del semestre pasado**, y la estigmatización y persecución a la que están siendo sometidos, incluso con procesos disciplinarios vigentes en la universidad.

Hasta el momento se desconocen las acciones de investigación y protección que las autoridades de policía o la Unidad Nacional de Protección brinden a los estudiantes, y por tal razón persiste la preocupación en la comunidad universitaria por este tipo de acciones que violan los derechos a la vida, a la protesta y a la asociación, señalan los estudiantes.
