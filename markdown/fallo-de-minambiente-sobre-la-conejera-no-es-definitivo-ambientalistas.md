Title: "Fallo de Minambiente sobre 'La Conejera' no es definitivo" Ambientalistas
Date: 2015-03-13 21:06
Author: CtgAdm
Category: Ambiente, Nacional
Tags: Alcaldía de Bogotá, Ambiente, Humedal, La Conejera, Ministerio de Ambiente, Plan de Manejo Ambiental del Humedal La Conejera, Suba, Tribunal Administrativo de Cundinamarca
Slug: fallo-de-minambiente-sobre-la-conejera-no-es-definitivo-ambientalistas
Status: published

##### Foto: Diario ADN 

<iframe src="http://www.ivoox.com/player_ek_4212210_2_1.html?data=lZeelJeVdI6ZmKiak5mJd6Kmk5KSmaiRdo6ZmKiakpKJe6ShkZKSmaiRaZOmr9TgjcrSp9Di1dfOz9TXb8Tjz5Di0MaPq9PVz5DcztHFb9HjxdfWxsaPqc%2BfxtGYxs7XuNPdjoqkpZKns8%2FowszW0ZC2pcXd0JCah5yncZU%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Gina Díaz, defensora del humedal 'La Conejera'] 

**“Seguiremos buscando organizaciones de apoyo, esto  no es una definitiva”**, es la reacción de Gina Díaz, quien hace parte de la comunidad que defiende el humedal La Conejera, frente a la decisión del Alcalde de Bogotá Ad-hoc y Ministro de Ambiente, Gabriel Vallejo, que aprobó la construcción de apartamentos en La Conejera, adelantado por la constructora Fontanar del Río.

**“Nos encontramos con una gran olla podrida en el distrito”,** dice la defensora del humedal, quien añade que **“nuestra reacción es un profundo descontento frente a las mentiras que están haciendo públicas”**, sin embargo, afirma que con esa aprobación, solo se agotó el mecanismo administrativo, ya que hace falta conocer el fallo del Tribunal Administrativo de Cundinamarca, respecto a la acción popular interpuesta por el personero y la comunidad.

Pese a que el veredicto de las autoridades, afirma que  el proyecto Fontanar del Río, etapa 8, cumple con los requisitos y tiene todas las licencias al día, de acuerdo a Gina Díaz, hay varias **irregularidades en la licencia de construcción, además, de otras en la licencia de urbanismo, donde  “hay inconsistencias aun mayores”.**

Según la ambientalista, las denuncias de construcción no han sido tenidas en cuenta y el ministro Vallejo dijo que no podía reconocer las denuncias sobre la licencia de urbanismo porque estaba por fuera de sus funciones.

Además de las evidentes faltas en las licencia de construcción y urbanística, la comunidad y la personería argumentan que:

-   No se tuvo en cuenta el Plan de Manejo Ambiental del Humedal La Conejera, aprobado en enero de este año,  donde están soportadas las denuncias hechas por los defensores del ambiente. Allí, se confirma que de  acuerdo a ese documento, no se deben permitir construcciones cerca a La Conejera por el daño ambiental y por ser una zona vulnerable a inundaciones. Pero según Díaz, “no se han tomado la molestia de revisar ese plan”.
-   No se cuenta con el concepto que permita la demolición de la Hacienda Fontanar del Río, y se desconoció el decreto que protege esa construcción.
-   No hay claridad frente a los límites del humedal,  hay contraposición de varias normas y acuerdos.
-   El Canal Afidro que recolecta aguas lluvias hace varios años, está reconocido por el plan de manejo ambiental.
-   No se sabe por dónde van a ingresar el material pesado, debido a que no existe ninguna  vía que permita ingresar volquetas.
-   Pese a que el Ministro Vallejo dice que no hay consecuencias ambientales, lo cierto, es que “la afectación ambiental es inminente, el humedal es transito de aves migratorias… la luz, el polvo, el tráfico de personas y el ruido, contaminan el ecosistema”, dice Gina Díaz.

**La comunidad también denuncia que la fuerza pública está hostigando a los defensores y defensoras del humedal**, ya que según las autoridades, el campamento que los manifestantes tienen frente a la construcción,  genera daños a la naturaleza, “pero en cambio una construcción no…”, expresa irónicamente Gina.

La ciudadanía seguirá firme para proteger ese ecosistema, y **mañana 14 de marzo desde las  9 am, realizarán una concentración en el “campamento de resistencia”** ubicado frente a la constructora en protesta del fallo del distrito.
