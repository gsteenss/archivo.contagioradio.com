Title: Comunidades del Bajo Atrato discuten agenda de paz
Date: 2016-08-04 12:58
Category: Comunidad, Nacional
Tags: acuerdos de paz, Curvarado, Diálogos de paz en Colombia, DIPAZ, Red CONPAZ
Slug: comunidades-del-bajo-atrato-discuten-agenda-de-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/08/e709f9d2-12e4-4971-8d57-20281f414d33.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Comunicadores CONPAZ ] 

###### [4 Ago 2016]

En la Zona Humanitaria Las Camelias, cuenca del río Curbaradó municipio Carmen del darien Chocó, este martes con el objetivo de proponer las **casas de veeduría al proceso de paz**, la organización DiPaz estuvo visitando las comunidades para socializar los seis puntos del acuerdo que firmará el Gobierno colombiano y las FARC-EP en la mesa de negociación de La Habana Cuba.

Asistieron al taller las comunidades de las cuencas de los ríos Curbaradó, Jiguamiandó, La Larga Tumaradó, Vigia del Curbaradó, los estudiantes del Colegio de la Asociación de familias del Curbaradó,  Jiguamiandó,  Pedeguita y Mansilla, La Larga Tumaradó y Vigia del Curbaradó y seis delegados de DiPaz.

[![Cuvaradó](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/08/Cuvaradó.jpg){.aligncenter .size-full .wp-image-27377 width="1535" height="1269"}](https://archivo.contagioradio.com/comunidades-del-bajo-atrato-discuten-agenda-de-paz/cuvarado/)

Allí las comunidades miraron la **preocupación que hay con la presencia de los paramilitares y empresarios ocupantes de mala fe**, así como el incumplimiento por parte del Gobierno de la ley que cobija los territorios colectivos afros, indígenas y afromestizos

Fue muy enriquecedor el taller para las personas que asistieron porque pudieron darse cuenta que es muy diferente lo que están hablando el gobierno en La Habana, Cuba, con la realidad que  están viviendo en los territorios. Las comunidades participaron con poesías, y cantos de hip-hop en los que hablaron de la **vulneración de derechos de las comunidades afromestizas** por parte del gobierno, en el Curbaradó. Sobre las 4 de la tarde todas las comunidades partieron a sus Lugares  de origen.

José Francisco Álvarez - Jaider Acosta

Comunicadores Populares de la Red CONPAZ   ⁠⁠⁠⁠

<iframe src="http://co.ivoox.com/es/player_ej_12441200_2_1.html?data=kpehlpaWdJGhhpywj5WdaZS1lpiah5yncZOhhpywj5WRaZi3jpWah5yncavVysnS1JClp9Dn1caSlKiPh9Dh1tPWxcbIs9OfpLS7sqa-cYarpJKw0dPYpcjd0JC_w8nNs4yhhpywj5k.&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)]] 
