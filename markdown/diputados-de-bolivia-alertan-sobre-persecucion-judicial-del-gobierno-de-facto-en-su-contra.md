Title: Diputados de Bolivia alertan sobre persecución judicial del Gobierno de facto en su contra
Date: 2019-11-18 19:23
Author: CtgAdm
Category: El mundo, Política
Tags: Bolivia
Slug: diputados-de-bolivia-alertan-sobre-persecucion-judicial-del-gobierno-de-facto-en-su-contra
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/Bolivia.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:] 

**Diputados del Movimiento Al Socialismo (MAS)** en Bolivia han alertado sobre una persecución judicial en su contra, tras la creación de un "aparato especial" promovido por el Gobierno de facto, tal recurso estará a cargo de detener a congresistas afines a este partido acusándoles de cometer actos subversivos pese a que vienen denunciando las violaciones de DD.HH. en el país suramericano.

Sonia Brito, diputada del MAS asegura que este accionar se está cometiendo contra cerca de 30 parlamentarios que son las cabezas visibles del partido, han sido aquellos quienes han sido voceros o han denunciado la crisis que se vive en el país,  **"están plantando supuestas pruebas para decir que nosotros estamos en actividades irregulares"**, afirma. [(Futuro incierto en Bolivia tras la renuncia de Evo Morales)](https://archivo.contagioradio.com/futuro-incierto-en-bolivia-tras-la-renuncia-de-evo-morales/)

"A partir del lunes voy a ordenar, ya tengo listas que los mismos dirigentes de varias zonas me están pasando, los van a empezar a detener con órdenes fiscales", manifestó el actual ministro interino del Gobierno, Arturo Murillo quien desde su posicionamiento expresó que iría a la cacería de diferentes funcionarios, entre ellos  Juan Ramón Quintana, ministro de Presidencia y  Raúl García Linera, hermano del exvicepresidente Álvaro García Linera.

> "Queremos una salida institucional pero no se está permitiendo, hay toda una campaña mediática en contra nuestra", denuncia.

A su vez, Sonia Brito resalta que el Gobierno de facto, liderado por la autoproclamada presidenta Jeanine Áñez, busca **"llamar a unas elecciones pero bajo sus condiciones y sin tomar en cuenta su Constitución, quieren imponer sus candidatos para el nuevo órgano electoral y están metiendo a la cárcel al anterior"**. [(Le puede interesar: Durante gobierno de facto en Bolivia policía habría asesinado 24 personas)](https://archivo.contagioradio.com/durante-gobierno-de-facto-en-bolivia-policia-habria-asesinado-24-personas/)

> En lugar de pacificación, ordenan difamación y represión contra hermanos del campo que denuncian el golpe de Estado. Después masacrar a 24 indígenas, ahora preparan un Estado de Sitio. Sería la confirmación de que pidiendo democracia instalaron una dictadura.
>
> — Evo Morales Ayma (@evoespueblo) [November 18, 2019](https://twitter.com/evoespueblo/status/1196411824922599424?ref_src=twsrc%5Etfw)

<p>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
</p>
### "Nos están arrinconando" 

Los hechos se dan después de que legisladores del MAS anunciaron que presentarán un recurso de inconstitucionalidad ante el Tribunal Constitucional contra **el decreto que exime a las Fuerzas Militares de su responsabilidad en la represión registrada en los último días,** decreto que también ha sido rechazado por la Comisión Interamericana de Derechos Humanos (CIDH) pues considera, estimula la violencia.

Agrega la diputada que se encuentra realizando un informe sobre la situación de DD.HH. en Bolivia, pues más allá de su situación, es necesario resaltar el hecho de que el Ejército continúa en las calles. Dicho informe, espera poder entregarlo a Jean Arnault, quien fuera jefe de la Misión de Verificación de la ONU en Colombia y es ahora enviado personal del secretario general de Naciones Unidas para mediar en esa crisis.

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
