Title: “Las mentiras de la Silla Vacía” respuesta de profesor Renán Vega a difamaciones
Date: 2015-05-13 18:25
Author: CtgAdm
Category: Nacional, Paz
Tags: "La injerencia de Estados Unidos, Comisión de Memoria Histórica del Conflicto y sus víctimas, contrainsurgencia y terrorismo de Estado", Renán Veg
Slug: las-mentiras-de-la-silla-vacia-respuesta-de-profesor-renan-vega-a-difamaciones
Status: published

###### Foto: anrquismo.net 

El profesor **Renán Veg**a, integrante de la **Comisión de Memoria Histórica del conflicto y sus víctimas,** ha emitido un comunicado en el que aclara ante la opinión pública, los datos concretos en los cuales está consignada la información que aporto en su ensayo **"*La injerencia de Estados Unidos, contrainsurgencia y terrorismo de Estado*"**. En el comunicado el académico aclara que el caso cuestionado si tiene una fuente clara y concisa en el que documenta la denuncia de explotación sexual a 53 menores de edad por parte de militares de Estados Unidos.

Vega afirma que medios de información como **"la Silla Vacía"** pretenden desinformar a la opinión pública y aclara las fuentes de la información que consignó en su informe así como los periodistas que la suministraron y a los cuales él referencia. El comunicado de 9 páginas contiene diversos apartes como "Los 53 casos de abuso sexual, el origen de la cifra", "porno marines en Colombia", explica la lógica del investigador y la lógica del periodista, y afirma que hay una lógica de desinformación aplicada por medios en Colombia y en el mundo.

A manera de conclusión el historiador presenta afirma que "*A propósito de esta salida en falso de la Silla Vacía, debo recordar que, en cuanto a la Comisión Histórica sobre el Conflicto Armado y sus Víctimas (CHCAV), no es la primera vez que lo hace, porque antes ya lo había hecho en forma irresponsable. Recordemos que la Silla Vacía desde el momento en que se configuró la CHCAV en agosto de 2014 procedió a elaborar un diagrama en el que clasificaba a cada uno de sus miembros de la “extrema izquierda” a la “extrema derecha” y, por supuesto, yo aparecí en la extrema izquierda15*

*Apenas se dio a conocer el Informe final, otra vez La Silla Vacía procedió a señalar, sin medir las consecuencias que eso puede tener un país como Colombia, a cada uno de los miembros de la Comisión, adscribiéndonos o a las FARC o al gobierno16 antecedentes no sorprende que La Silla Vacía haya incurrido en la pifia que comentamos en este artículo y que enloda mi buen nombre y mi prestigio intelectual, lo cual es propio por lo demás de la mayor parte de los medios de desinformación de masas, cuya catadura obliga a estar alerta, como bien lo advirtió Malcon X hace más de medio siglo: “Si no estáisprevenido ante los Medios de Comunicación, os harán amar al opresor y odiar al oprimido*”.

A continuación el texto completo...

[Las Mentiras de La Silla Vacia](https://es.scribd.com/doc/265251788/Las-Mentiras-de-La-Silla-Vacia "View Las Mentiras de La Silla Vacia on Scribd")

<iframe id="doc_8803" class="scribd_iframe_embed" src="https://www.scribd.com/embeds/265251788/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-8dlbBntO83QUOnKo0oav&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="0.7729220222793488"></iframe>
