Title: Con Oneida Epiayú y Constantino Ramírez son más de 115 indígenas asesinados en el último año
Date: 2019-10-18 12:53
Author: CtgAdm
Category: Líderes sociales, Nacional
Tags: Asesinato de indígenas, Cauca, La Guajira, Quindío
Slug: con-oneida-epiayu-y-constantino-ramirez-son-mas-de-115-indigenas-asesinados-en-el-ultimo-ano
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/Indigena-Constantino-Ramírez.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:] 

Este 17 de octubre se atentó nuevamente contra dirigentes de los pueblos indígenas del país. Sobre el medio día se informó sobre el intento de asesinato contra el coordinador [**Eberto Tumiña** quien sobrevivió al ataque ocurrido en el departamento de Cauca.]{.css-901oao .css-16my406 .r-1qd0xha .r-ad9z0x .r-bcqeeo .r-qvutc0} Horas más tarde se alertó sobre el homicidio de la lideresa Wayúu **Oneida Epiayú, en Riohacha,** La Guajira, mientras que en la noche en Calarcá, Quindio, fue asesinado el líder ambiental del pueblo Embera, **Constantino Valencia**. Con sus muertes, los líderes y lideresas sociales asesinados en Colombia durante el Gobierno de Iván Duque ascienden a 234.

### Oneida Epiayú

Los hechos ocurrieron mientras Oneida, lideresa de la Alta Guajira almorzaba junto a su familia en un restaurante popular de la Avenida Los Estudiantes de Riohacha, cuando dos sujetos a bordo de una motocicleta negra, ingresaron al establecimiento acercándose a la mujer.

Según información de los testigos de los hechos, los autores materiales tenían sus cascos puestos al ingresar al restaurante lo que impidió su reconocimiento, acto seguido dispararon contra la cabeza y el tórax de la lideresa causando su muerte. En medio de los actos de violencia, cuatro personas resultaron heridas incluidas el esposo de Oneida y un menor de edad.

La lideresa, oriunda de Manaure, **era reconocida por su activismo social y por las denuncias que venía realizando en contra de la corrupción que se presenta al interior del Instituto Colombiano de Bienestar Familiar en La Guajira**. [(Le puede interesar:Asesinato del palabrero José Manuel Pana Epieyú en Maicao, un golpe a la reconciliación) ](https://archivo.contagioradio.com/asesinato-del-palabrero-jose-manuel-pana-epieyu-en-maicao-un-golpe-a-la-reconciliacion/)

### Constantino Ramírez

En horas de la noche de ese mismo día, mientras se desplazaba en motocicleta por la vía que conduce de Calarcá, Quindio, al Resguardo Indígena Dachi Agore. fue asesinado el líder indígena Constantino Ramírez Bedoya, considerado uno de los máximos dirigentes  del pueblo Embera del departamento.

Constantino f**ue ambientalista, miembro de la Junta Directiva de la Corporación Autónoma del Quindio, en la que trabajó desde hace más de 20 años, a su vez fue fundador de la Organización Regional Indígena del Quindío (ORIQUIN)**,  en el que fungió como Consejero Mayor en varias ocasiones y como representante del cabildo de adultos mayores.

### Un nuevo atentado contra autoridades indígenas en Cauca

Así mismo, el Consejo Regional Indígena del Cauca (CRIC) informó sobre un nuevo atentado registrado en la zona entre [Caloto y Toribío]{.css-901oao .css-16my406 .r-1qd0xha .r-ad9z0x .r-bcqeeo .r-qvutc0} donde el vehículo de protección del coordinador [indígena Eberto Tumiña fue impactado con arma de fuego.]{.css-901oao .css-16my406 .r-1qd0xha .r-ad9z0x .r-bcqeeo .r-qvutc0}

Al conocerse los hechos, la guardia zonal y la comunidad aledaña acudieron a la vía que comunica El Palo y El Tierrero, donde encontraron al coordinador [que se encuentra fuera de peligro.  [(Lea también:  Lilia García, secretaria del Cabildo Awá fue asesinada en Nariño)](https://archivo.contagioradio.com/lilia-garcia-secretaria-del-cabildo-awa-fue-asesinada-en-narino/)]{.css-901oao .css-16my406 .r-1qd0xha .r-ad9z0x .r-bcqeeo .r-qvutc0}

> [@UNPColombia](https://twitter.com/UNPColombia?ref_src=twsrc%5Etfw) [@utunpnacional](https://twitter.com/utunpnacional?ref_src=twsrc%5Etfw) [@PoliciaColombia](https://twitter.com/PoliciaColombia?ref_src=twsrc%5Etfw) [@MinInterior](https://twitter.com/MinInterior?ref_src=twsrc%5Etfw) Reprochamos atentado contra Líder Indígena EBERTO TUMIÑA en el norte del Cauca día a día los HOMBRES DE PROTECCION demostrando su profesionalismo. [pic.twitter.com/D7Drmw0Gea](https://t.co/D7Drmw0Gea)
>
> — U.T-U.N.P. Popayan (@utunppopayan) [October 17, 2019](https://twitter.com/utunppopayan/status/1184910691972632576?ref_src=twsrc%5Etfw)

Ante estos sucesos, la Organización Nacional Indígena de Colombia (ONIC) ha solicitado la visita al país, de la  relatora especial sobre los derechos humanos Victoria Tauli-Corpuz, para que realice una misión de verificación de cara a los múltiples asesinos contra  los pueblos indígenas, llegando la cifra de homicidios a 115 en el último año, según la organización indígena.

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
