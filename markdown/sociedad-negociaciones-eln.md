Title: La sociedad tiene que presionar para que sigan las negociaciones con el ELN
Date: 2018-09-20 17:46
Author: AdminContagio
Category: Nacional, Paz
Tags: Duque, ELN, Equipo Negociador, Negociación de Paz
Slug: sociedad-negociaciones-eln
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/FIRMA-DEL-ACUERDO-DE-PAZ-10.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo Contagio Radio] 

###### [20 Sep 2018] 

Con el fin del periodo del equipo negociador del Gobierno con el ELN, diferentes visiones encontradas se han expuesto sobre la forma en que el presidente **Iván Duque** seguirá asumiendo los diálogos de paz. Por una parte, hay quienes creen que es una muestra más de que el Presidente quiere 'quebrar' la mesa de diálogo y por otra, quienes lo ven como un asunto administrativo.

<iframe src="https://co.ivoox.com/es/player_ek_28756512_2_1.html?data=k52kl5uZdZOhhpywj5WcaZS1k5Wah5yncZOhhpywj5WRaZi3jpWah5yncbfdxNnc1JDIqYy31tffx8aRkNbb0IqfpZDXs8PmxpDSzpDKrc-fxcrZjdXJtsrjxdSYxsrQb6blj5KSmaiRh9Di1cbUy9SPlsLYytSYj4qbh46o&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

Para el profesor y analista **Víctor de Currea-Lugo,** este anuncio es consecuente con la posición de Duque, quien antes de ser presidente, planteó que el ELN tenía que concentrarse con verificación internacional; después, propuso un modelo de negociación basado en el Desarme, Desmovilización y Reinserción (DDR) y afirmó que en 30 días evaluaría el proceso. Pero **44 días después no ha resuelto nada,** y tampoco ha tenido en cuenta los mensajes de paz que envió la guerrilla con la liberación de personas retenidas.

En ese sentido, para el analista es claro que el Gobierno no sabe que pasará con las negociaciones, mientras la lógica que impera por parte de Duque es "que **el ELN tiene que dejar de ser ELN para negociar",** mientras de parte de la guerrilla se está asumiendo que están hablando con el Duque que ellos quisieran y no con el presidente que está. (Le puede interesar: ["ONU saluda liberación de personas retenidas por guerrilla"](https://archivo.contagioradio.com/onu-saluda-liberacion-eln/))

Sin embargo, el integrante de la Comisión de Paz del Senado **Iván Cepeda,** aseguró que en una reunión realizada con **Miguel Ceballos,** el comisionado anunció que la terminación de contrato de Gustavo Bell, y el resto del equipo negociador **es un tramite administrativo normal**, que ocurre cada que llega un Gobierno nuevo. (Le puede interesar:[ "Organizaciones Sociales piden gestos de paz al Gobierno y la guerrilla"](https://archivo.contagioradio.com/piden-paz-gobierno-eln/))

<iframe src="https://co.ivoox.com/es/player_ek_28756526_2_1.html?data=k52kl5uZdpehhpywj5WbaZS1kZWah5yncZOhhpywj5WRaZi3jpWah5yncarqhqigh6aVsoy3xtXSxsaJdqSf1NTP1MqPsMKf08ri0M6Jh5SZo5jbjcjTsozByszix9GPh8bWwtHZ0diJdqSfpNSah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ..&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

Aunque el Senador reconoció que el contexto de la situación es complicado y las condiciones que pide el Gobierno para continuar con la Mesa de Negociaciones son difíciles, recordó que **Ceballos expresó la intención de Duque de continuar el proceso de paz con el ELN y la implementación del Acuerdo de Paz con las FARC.** De igual forma, Cepeda reconoció que hay pactos que deben hacerse de forma bilateral, como el cese de hostilidades, para construir un proceso de confianza que vaya ganando fuerza.

Mientras De Currea piensa que el Gobierno no tiene claro para dónde va con el Proceso, porque hay fuerzas al interior del Gobierno pujando para seguir la guerra y otras en pro de la negociación; Cepeda consideró que parece haber un compromiso en continuar los diálogos, por lo tanto, **"hay que presionar desde la sociedad civil para superar este difícil momento que vive el proceso y poderlo destrabar".**

###### [Reciba toda la información de Contagio Radio en] [[su correo] [[Contagio Radio](http://bit.ly/1ICYhVU)]
