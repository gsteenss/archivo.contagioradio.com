Title: Asesinato de Gobernador y líder indígena enluta al Pueblo Awá
Date: 2018-12-03 11:09
Author: AdminContagio
Category: DDHH, Entrevistas
Tags: asesinato, Awá, lideres sociales, masacre
Slug: awa-luto-asesinato-de-lideres
Status: published

###### [Foto: Contagio Radio] 

###### [3 Dic 2018] 

Durante el fin de semana el Pueblo Awá denunció el ataque de hombres armados contra integrantes de la comunidad, hecho en el que resultaron **4 personas heridas y 2 líderes asesinados**. La agresión se produjo contra el **Resguardo Palmar Imbi Medio** en Ricaurte, Nariño, el sábado 1 de diciembre en horas de la madrugada.

Según **Jaime Caicedo Guanga,** asesor jurídico del Cabildo Mayor Awá de Ricaurte (CAMAWARI), el atentado se produjo cerca de las 12:15 am del sábado, cuando hombres armados llegaron a la vivienda **Braulio Arturo García Guanga**, gobernador indígena electo para 2019, y dispararon contra él y su padre, **Hector Ramiro García**, líder y fundador de la organización CAMAYARI, así como de la Unidad Indígena del Pueblo Awá (UNIPA).

Posteriormente, los atacantes agredieron a **Miguel García,** hijo de Hector Ramiro y coordinador de la guardia indígena; **Gilberto Nastacuas y Gerardo Nastacuas, guardias indígenas, y al líder Juvenal Torres**. Este atentado se produjo en momentos en que la comunidad se encontraba en medio de una celebración. (Le puede interesar: ["La grave crisis humanitaria que afronta el pueblo Awá los obligó a venir a Bogotá"](https://archivo.contagioradio.com/crisis-humanitaria-awa/))

Caicedo señaló que tras el proceso de paz, la comunidad estaba feliz porque sentían la posibilidad de vivir tranquilos en sus territorios, no obstante, la salida de las FARC de los territorios significó el ingreso del ELN, así como de grupos reincidentes de la ex-guerrilla. (Le puede interesar: ["Comunidad indígena denuncia amenaza a sus líderes"](https://archivo.contagioradio.com/unidad-indigena-del-pueblo-awa-denuncia-amenazas-a-sus-lideres/))

En este contexto, los líderes trabajaban defendiendo el proceso organizativo Awá, "luchando por la autonomía y haciendo control territorial en los resguardos", razón por la que el integrante de CAMAWARI pidió a las entidades encargadas se investigar los hechos celeridad en sus procedimientos. (Le puede interesar: ["Los elementos de la crisis de asesinatos de indígenas en Nariño"](https://archivo.contagioradio.com/indigenas-awa-denuncian-asesinatos-y-hostigamientos-de-estructuras-armadas-contra-su-comunidad/))

### **"No fue por una riña familiar"** 

Organizaciones como la Asociación Minga, o la Defensoría del Pueblo condenaron la agresión, el Ejército Nacional, representado por el teniente coronel Oscar Zalas, manifestó que los asesinatos fueron producto de una riña familiar, declaraciones que fueron amplificadas por varios de comunicación, lo que los integrantes del Pueblo Awá rechazaron, indicando que esa información es completamente falsa. (Le puede interesar: ["ONU preocupada por descalificación de Gobierno a asesinatos de defensores de DD.HH."](https://archivo.contagioradio.com/onu-preocupada-por-descalificacion-de-gobierno-a-asesinatos-de-defensores-de-ddhh/))

> Repudiamos asesinato de Arturo García, gobernador del Resguardo Indígena Awa El Palmar en municipio Ricaurte (Nariño), y de su padre Héctor García, fundador de CAMAWARI. [@DefensoriaCol](https://twitter.com/DefensoriaCol?ref_src=twsrc%5Etfw) manifiesta sus condolencias a familiares y exige acelerar las investigaciones. [pic.twitter.com/TWKMtingWX](https://t.co/TWKMtingWX)
>
> — Defensoría delPueblo (@DefensoriaCol) [2 de diciembre de 2018](https://twitter.com/DefensoriaCol/status/1069268722186100738?ref_src=twsrc%5Etfw)

<p>
<script src="https://platform.twitter.com/widgets.js" async charset="utf-8"></script>
<iframe id="audio_30506800" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_30506800_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen">
</iframe>
</p>
###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
