Title: ¿Coordinaron Gobierno colombiano y Rastrojos ingreso de Juan Guaidó a la frontera?
Date: 2019-09-13 18:02
Author: CtgAdm
Category: Nacional, Política
Tags: Frontera entre Colombia y Venezuela, Juan Guaidó, Los Rastrojos
Slug: coordinaron-gobierno-colombiano-y-rastrojos-ingreso-de-juan-guaido-a-la-frontera
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/09/Guaidó.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:  @wilcan91] 

Las fotografías en las que Juan Guaidó, líder de la oposición en Venezuela, aparece junto a **Albeiro Lobo Quintero, alias 'Brother'  y John Jairo Durán Contreras alias 'Menor', líderes del grupo paramilitar Los Rastrojos,**  llevan a cuestionarse qué tipo de vínculo podría existir entre la oposición venezolana y el grupo armado y cuál fue el rol del gobierno colombiano como facilitador de la estancia de Guaidó, quien cruzó la frontera en el marco del concierto celebrado el 22 de febrero de 2019 en Cúcuta pese a que este tenía prohibida la salida del país vecino.

La fotografía con los líderes de Los Rastrojos, cuya identidad fue confirmada por la Policía de Cúcuta, proviene de **Wilfredo Cañizares, director de la Fundación Progresar en Norte de Santander**, quien señala que el ingreso de Guaidó a Cucutá fue coordinado con integrantes de este grupo, quienes habrían obligado a los habitantes a encerrarse en sus casas hasta que el líder de la oposición fuese recogido por funcionarios del Gobierno, que como señala Wilfredo, "debe ser quien aclare quiénes participaron en este operativo y quién coordino con Los Rastrojos el ingreso del señor Guaidó a nuestro país".

"Quienes lo recogen a él en la cancha de fútbol del corregimiento de Aguaclara, es personal de la Presidencia, pero no hay miembros de la Fuerza Pública uniformados, todos son personas de civil, afirma Cañizares  quien agrega que "Los Rastrojos no querían que hubiera fotos ni se documentara y evitar que la gente se diera cuenta que estaba sucediendo", argumenta WIlfredo. Por su parte Guaidó afirma que los dos hombres con los que aparece en la fotografía no estaban prestando seguridad sino que se encontraban en la vía y que desconocía que se tratase de integrantes del grupo armado.

### No podemos seguir construyendo una verdad sobre Venezuela a partir de Juan Guaidó

Para Víctor De Currea-Lugo, profesor y analista, la aparición de estas fotos son la prueba de que la oposición venezolana tiene vínculos directos con grupos armados que funcionan a ambos lados de la frontera, además plantea que la verdadera pregunta de fondo que hay que hacerse es cómo cruzó Juan Guaidó la frontera entre Colombia y Venezuela. [(Le puede interesar: \#5000TroopsToColombia: ¿Agresión mediática contra Venezuela?)](https://archivo.contagioradio.com/5000troopstocolombia-venezuela/)

Para el analista existen dos teorías: o Juan Guaidó cruzó con ayuda de Los Rastrojos, lo que obedecería a a una coordinación entre paramilitarismo y la oposición facilitando el transporte hasta territorio colombiano o el líder de la oposición fue apoyado por el gobierno de Colombia en cuyo caso "pudo haber vulnerado la soberanía de Venezuela",  algo que  ya se ha visto con anterioridad, como en enero de 2008 cuando se incursionó en territorio ecuatoriano para capturar a Raúl Reyes.

Independientemente del Estado y su política exterior,  Víctor De Currea-Lugo sostiene que existe una doble moral en un sector de  la población a la que le es indiferente la evidencia de los vínculos entre el actual presidente de la Asamblea Nacional de Venezuela y paramilitares colombianos pero que condena a Maduro y los posibles vínculos que tendría con las FARC o el ELN  sin que existan pruebas contundente, **"hay discursos en los que sentarse con un grupo paramilitar es plausible y miramos para otro lado mientras que otros vínculos no tienen pruebas pero se dan por ciertos"**.

### ¿Cómo operan Los Rastrojos en la frontera?

Según el director de la Fundación Progresar, el Norte de Santander  un departamento de más de 621.000 km2 es una zona en la que conviven el EPL, las autodenominadas Autodefensas Gaitanistas de Colombia, El ELN, los Urabeños y las disidencias de las FARC, sin embargo es esta la frontera donde existe un mayor control de Los Rastrojos, quienes dominan la minería ilegal y el narcotráfico entre Colombia y Venezuela. "controlan las vías, la economía, la renta ilegal, el narcotráfico, controlan la vida y la muerte", relata.

Con relación a alias 'El Menor' y alias 'Brother', señalados de ser responsables de más de 100 desapariciones forzadas y 150 asesinatos en Puerto Santander, se encuentran en poder de las autoridades después de entregarse a  la Policía, esto debido a la persecución realizada en su contra por Wilfredo de Jesús Torres Gómez, alias 'Necoclí', antiguo líder de la organización, capturado en Valencia, Venezuela y quien los estaría buscando debido a un enfrentamiento al interior de la organización. "No sé si tendrán la voluntad de decir la verdad, pero su fotografía o garantizar el acceso y el cruce de la frontera al señor Guaidó sería irrelevante ante sus crímenes cometidos".

<iframe id="audio_41571299" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_41571299_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>  
**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
