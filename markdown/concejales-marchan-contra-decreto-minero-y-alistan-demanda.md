Title: Concejales marchan contra decreto minero y alistan demanda
Date: 2015-03-24 17:07
Author: CtgAdm
Category: Movilización, Nacional
Tags: Alberto Castilla, Ambiente, Decreto, decreto minero, Guaviare, Meta, Mineria, Ministerio de Agricultura, Ministerio de Ambiente, Ministerio de Minas y Energía, Ministerio del Interior, Movilización, Polo Democrático Alternativo, soberanía
Slug: concejales-marchan-contra-decreto-minero-y-alistan-demanda
Status: published

##### Foto: Alberto Castilla 

<iframe src="http://www.ivoox.com/player_ek_4257944_2_1.html?data=lZeimZ6YeI6ZmKiakp6Jd6KlkpKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRh9Di1MrXw9HJt4zhwtfQysbSb8Tjz9nfw5DIqcTmxtncjcnNssbm0JDmjcbQrdTowtOYxsrRpY6ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Alberto Castilla, senador Polo Democrático Alternativo] 

Desde este sábado 21 de marzo, aproximadamente **100 concejales** y concejalas del Meta y Guaviare, iniciaron una marcha desde Villavicencio hacia Bogotá, con el objetivo de mostrar su rechazo al Decreto 2691 de 2014, **el “decreto navideño”, firmado por el Ministerio de Minas y Energía, del Interior, de Ambiente y de Agricultura, el pasado 23 de diciembre,** por el cual se deja a las comunidades sin potestad sobre sus territorios explotados por diferentes empresas mineras.

“Esperamos que el gobierno nacional escuche la movilización y **proceda a derogar el decreto, de lo contrario se estará profundizando en la extracción minera,** tal y como lo plantea el Plan Nacional de Desarrollo (2014-2018) que es contrario a la aspiración de paz que hoy defiende la sociedad colombiana”, asegura el senador del Polo Democrático Alternativo, Alberto Castilla quien acompaña la movilización.

De acuerdo al senador, el Decreto 2691 de 2014, fue **“un regalo de navidad para las empresas mineras** y una sustracción de la autonomía que tienen los entes territoriales para decidir sobre la minería”, lo que ha generado daños al ambiente y a las actividades cotidianas de los pobladores, es por eso que los  concejales expresan su solidaridad y acompañamiento, con el fin de reclamar la autonomía municipal.

A las 11:00  de la mañana del 24 de marzo, los concejales y concejalas llegarían a la Plaza de Bolívar para socializar la demanda que anule el decreto minero que pretenden **“reivindicar la defensa de la autonomía territorial** y los principios de coordinación, concurrencia y subsidiariedad entre el nivel central y entes territoriales; la defensa de la democracia representativa y participativa, debido a que el Decreto limita la actuación de la ciudadanía, en cambio si se promueve la participación de empresarios, pero no de la gente que sufrirá los impactos de la minería en sus territorios”, asegura el comunicado de la marcha.

Así mismo, se considera que **el Decreto viola el principio constitucional de buena fe, privilegia la industria minera sobre la protección del ambiente, del agua y la producción de alimentos.**

La movilización también ha sido acompañada por la doctora Clara López, integrantes el Instituto Latinoamericano para un Derecho y una Sociedad Alternativos (ILSA), concejales de Nariño, Cauca, Putumayo, Cundinamarca, Bogotá y los Santanderes y aproximadamente 600 ciudadanos que rechazan el “decreto navideño”.
