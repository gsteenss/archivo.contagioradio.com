Title: En Caucasia fue asesinada hija de reincorporados
Date: 2020-08-12 11:00
Author: CtgAdm
Category: Actualidad, DDHH
Tags: Antioquia, asesinato de excombatientes, FARC
Slug: en-caucasia-fue-asesinada-hija-de-reincorporados
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/08/Diseño-sin-título-15.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

Este 12 de agosto se conoció el asesinato de **Michell Andreína Gómez,** hija de una pareja de reincorporados de FARC, **en la vereda La Uribe del municipio de Caucasia**, localizado en la subregión del [Bajo Cauca](https://archivo.contagioradio.com/atentado-contra-alcaldia-de-buenaventura-no-es-una-casualidad/), departamento de Antioquia.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/PartidoFARC/status/1293522795188686848","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/PartidoFARC/status/1293522795188686848

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

La joven fue encontrada sin vida desde el lunes 10 de julio en las parcelas de La Uribe ubicadas en cercanías del Río Cauca, por **habitantes de la zona quienes además reportaron que tenía múltiples laceraciones de arma blanca en su cuerpo.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Al no portar ningún documento personal o elemento de filiación fue imposible identificar de quien se trataba, **así que al ser reportado el acto a las autoridades de la zona fue procesada como N.N** - *nomen nescio*- .

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Posteriormente fue trasladada a las instalaciones de la morgue del municipio de Caucasia, donde fue identificada al poco tiempo como **Michell Andreína Gómez Calvete, hija de Sandra Calvete Cárcamo y Raúl Méndez González, ambos en proceso de reincorporación.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Según informó el equipo coordinador del Espacio Territorial de Capacitación y Reincorporación ([ETCR)](https://archivo.contagioradio.com/crimenes-contra-excombatientes-de-farc-son-producto-de-la-accion-y-omision-del-estado/), Juan Carlos Castañeda. **Sandra Calvete, madre de la joven había recibido amenazas de muerte en días anteriores**, cuando habitaban el municipio de Caucasia, razón que los llevó a desplazarse hacia el ETCR como medida para proteger su vida.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Debido a que el cuerpo de Míchell fue hallado con 12 puñaladas y según señalan voceros de FARC, **"fue degollada en una acción de sevicia empleada por los enemigos de la paz en Colombia de los firmantes y sus familias"** , los restos se encuentran en las instalaciones de Medicina Legal en Montería, en donde se realizan investigaciones ante el asesinato.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

A través de redes sociales el partidos FARC rechazó el hecho, señalando que el asesinato de la joven fue "una acción de absoluta brutalidad contra la mujer y los familiares de las y los firmantes de paz".

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El asesinato de Michell, según el reporte más reciente de [Indepaz](https://archivo.contagioradio.com/971-defensores-de-dd-hh-y-lideres-han-sido-asesinados-desde-la-firma-del-acuerdo-de-paz/), se suma a la cifra de más de 200 reincorporados asesinatos desde la firma del Acuerdo de Paz en el 2016, de los cuales 20 fueron hacia algún miembro de su núcleo familiar; **a su vez esté se convierte en el caso 52 de asesinatos registrados en Caucasia y 169 homicidios en el Bajo Cauca en lo corrido de este año**.

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
