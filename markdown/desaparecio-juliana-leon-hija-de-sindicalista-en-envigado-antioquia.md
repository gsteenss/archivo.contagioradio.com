Title: Desapareció Juliana León, hija de sindicalista en Envigado, Antioquia
Date: 2015-09-09 14:16
Category: DDHH, Mujer, Nacional
Tags: Antioquia, desaparición, Envigado, Hildebrando León, Juliana León Pulgarín, Sindicalismo en Colombia, UNEB
Slug: desaparecio-juliana-leon-hija-de-sindicalista-en-envigado-antioquia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/Juliana-Uneb.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Cut] 

<iframe src="http://www.ivoox.com/player_ek_8245308_2_1.html?data=mZehl5iUfI6ZmKiakpuJd6KnlJKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRiMbnwtXO1MrHrYa3lIqvlZCuuc3dwtPOjbHJaaSnhqeg0IqWh4zcys%2FOjcnJb9Tdz8nWxcbQrdTowpDS0JCpstehhpywj6jTstXVyM7cjbfFqMrjjJKSmaiReA%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Hildebrando León, padre de la menor] 

###### [9 Sept 2015] 

Desde el pasado 6 de septiembre en el municipio de Envigado, se encuentra desaparecida, Juliana León de 18 años, hija de Hildebrando León, secretario general de la Unión Nacional de Empleados Bancarios UNEB.

[Hildebrando León, padre de la menor y actual secretario general de la Unión Nacional de Empleados Bancarios UNEB, afirma que la joven estudiante de ingeniería ambiental de la Universidad de Medellín, s**alió de su casa en el barrio El Dorado, el pasado domingo sobre las 8 de la mañana hacia El Poblado, pero no llegó a su destino y desde entonces no se sabe nada sobre su paradero**.]

[Hildebrando, recientemente negociador en la Convención Colectiva de Trabajo del Banco de Bogotá, **se abstiene de conectar la desaparición de su hija con sus actividades sindicales**, y llama la atención de las autoridades competentes para que den lo más pronto posible con el paradero de Juliana y adelanten las investigaciones pertinentes para encontrar y sancionar a los responsables.   ]

[Este hecho enciende las alarmas frente a las **garantías para el ejercicio sindical en Colombia**, los antecedentes en la materia son repudiables, familiares de sindicalistas constantemente han estado involucrados en acciones que atentan contra su integridad y derechos fundamentales, como amenazas, hostigamientos y desapariciones.  ]

 [![juliana](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/juliana1.jpg){.aligncenter .wp-image-13671 width="421" height="535"}](https://archivo.contagioradio.com/desaparecio-juliana-leon-hija-de-sindicalista-en-envigado-antioquia/juliana-2/)
