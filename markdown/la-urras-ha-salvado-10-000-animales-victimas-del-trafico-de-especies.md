Title: La URRAS ha salvado 10.000 animales víctimas del tráfico de especies
Date: 2015-01-21 14:05
Author: CtgAdm
Category: Ambiente, Voces de la Tierra
Tags: Animales, Fauna silvestre, Universidad Nacional, URRAS
Slug: la-urras-ha-salvado-10-000-animales-victimas-del-trafico-de-especies
Status: published

#### Foto por: Agencia UN 

Alrededor de 10.000 animales víctimas del tráfico de fauna silvestre o maltratados por el ser humano, han sido rescatados por la Unidad de Rescate y Rehabilitación de Animales Silvestres (URRAS) de la Universidad Nacional.

Esta entidad pertenece a la Facultad de Medicina Veterinaria y Zootecnia de la U.N., y tras sus 20 años de funcionamiento ha rehabilitado animales silvestres, a través de su labor de capacitación de estudiantes de esa facultad con el objetivo de lograr rescatar especies y reubicarlos en entornos adecuados para su bienestar.

De acuerdo a Claudia Brieva, profesora de la facultad de Medicina Veterinaria y de Zootecnia, muchos animales comercializados terminan muriendo en cautiverio, así mismo señaló que la URRAS intenta trabajar siempre en zonas donde la cacería o degradación del ecosistema han sido la causa de la desaparición de fauna silvestre.

Colombia es reconocida como la cuarta nación en biodiversidad mundial, la primera nación en anfibios y aves, la tercera en reptiles y quinta en mamíferos,  según datos de la Secretaría Distrital de Ambiente.

Cifras oficiales resaltan que en la primera mitad de año del 2014 en el país se incrementó un 26% el número de especies víctimas del tráfico de animales. Las guacamayas, loros, monos, tortugas, aves y serpientes son los animales que más se usan para comercializar, un negocio que después del tráfico de drogas y armas, es la tercera actividad criminal  más lucrativa en el mundo.

Cabe recordar que generalmente se piensa que el único responsable del comercio ilegal de animales es la persona que trafica, pero también es culpable, y así mismo será judicializado quien consuma o participe de esta actividad.
