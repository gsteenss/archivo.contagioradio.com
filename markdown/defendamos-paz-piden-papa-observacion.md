Title: Defendamos la Paz pide al Papa seguimiento y observación para que avance la paz
Date: 2019-09-09 15:59
Author: CtgAdm
Category: Nacional, Paz
Tags: acuerdo de paz con las FARC, Defendamos la Paz, Derechos Humanos, Pacto de la Habana, Papa
Slug: defendamos-paz-piden-papa-observacion
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/03/papa-en-colombia.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:] 

Después de dos años de la visita del Papa Francisco en Colombia, el movimiento Defendamos la Paz se dirigió por carta al Pontífice expresando su preocupación ante los asesinatos de líderes sociales y los problemas que enfrenta la implementación del acuerdo de paz firmado con FARC.

En la comunicación, el movimiento resaltó que la visita del Papa Francisco en 2017, la cual tuvo como motivo respaldar el Acuerdo de Paz, fue significativa para gran parte de la sociedad colombiana y la reconciliación del país.  No obstante, la violencia sigue presente en el territorio a través del asesinato de líderes y lideresas, candidatos, exguerrilleros y defensores de los Derechos Humanos.

Es por ello que en la misiva piden “velar por el Acuerdo y el proceso de paz, amenazados hoy desde distintos flancos, su observación del cumplimiento o incumplimiento de la implementación de lo pactado, su seguimiento a las labores de protección de las personas que lideran la defensa de la Paz y los derechos humanos en los territorios, así como de quienes dejaron las armas”.

En esa misma línea, Guillermo Rivera, ex ministro del Interior que trabaja para Defendamos la Paz, dijo a Contagio Radio que “el Papa es un líder universal de la reconciliación y por eso nosotros creemos que él puede jugar un papel muy importante en ese propósito que tenemos los colombianos”.

El exministro también resaltó que para el Papa la presencia de los jóvenes en el proceso de paz fue fundamental así como su rol en las movilizaciones. Asimismo, afirma que hay que continuar trabajando para entusiasmar a los jóvenes con la paz.

**La observación del pontífice puede ser determinante para el cumplimiento del Pacto de la Habana.**

“No podemos decaer en nuestro propósito de alcanzar la paz. Nuestra organización seguirá trabajando (…) en ese propósito de alcanzar la paz total y seguiremos en ese empeño. Tenemos la convicción de que este es el más noble esfuerzo que se puede hacer por la sociedad colombiana, sobre todo pensando en las nuevas generaciones” afirma Guillermo Rivera.

   
<iframe id="audio_41569255" frameborder="0" allowfullscreen scrolling="no" height="200" style="border:1px solid #EEE; box-sizing:border-box; width:100%;" src="https://co.ivoox.com/es/player_ej_41569255_4_1.html?c1=ff6600"></iframe>  
**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
