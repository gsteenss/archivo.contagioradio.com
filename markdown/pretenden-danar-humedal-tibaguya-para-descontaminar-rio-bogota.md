Title: Descontaminación de río Bogotá afectaría Humedal Tibaguya
Date: 2016-04-20 12:37
Category: Ambiente, Nacional
Tags: Alcaldía de Bogotá, Comunidad, Humedales
Slug: pretenden-danar-humedal-tibaguya-para-descontaminar-rio-bogota
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/04/humedal-el-Cortijo.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### Foto: El país 

##### [20 Abr 2016]

Las comunidades aledañas a la Planta de Aguas Residuales del Salitre, denuncian que la ampliación de esta, afectaría el ecosistema del Humedal Cortijo Tibaguya y la salud de apróximadamente 160.ooo mil familias que habitan a tan solo 100 metros del lugar en que se planifica la ampliación.

La Planta de Aguas Residuales se encuentra ubicada cerca a los barrios Ciudadela Colsubsidio, Quintas de Santa Ana, Los Eucalipto y El Cortijo, y dentro de poco la CAR planea realizar la ampliación de esta planta hacía el humedal Cortijo -Tibaguya reserva ambiental de 35 hectáreas de bosque y 50 de pastizales, que cuenta con cerca de 60 especies de aves, mamíferos e insectos.

De acuerdo con Camilo Moreno, miembro de la Mesa Ciudadana de El Cortijo, para realizar la planta se secaron dos humendales en el año 1998, además el funcionamiento de la misma es ineficiente debido a que no trata los dos causes del río Juan Amarillo, trata únicamente las aguas del rió Box Culvert Salitre, que desemboca más adelante en el río Bogotá junto con las aguas contaminadas del río Juan Amarillo.

A su vez, la planta estaría ubicada a tan solo 60 metros de los conjuntos residenciales, generando problemas de salud a la población ubicada allí debido a los malos olores, los químicos que se usan para el tratamiento de las aguas y la propagación del zancudo.

Diferentes organizaciones como la Mesa Ciudadana de El Cortijo en apoyo con la comunidad han realizado algunas propuestas a la CAR y la Alcaldía de la capital, para evitar la ampliación de la planta, entre ellas se encuentran generar un plan de interceptores en los ríos de Bogotá, además proponen que la planta que se construya pueda  recibir no solo las aguas de la capital sino también de las zonas urbanas aledañas.

El próximo domingo 24 de abril se realizará un recorrido de sensibilización al Humedal Tibaguya para que la comunidad conozca la fauna y flora de este lugar.

<iframe src="http://co.ivoox.com/es/player_ej_11239277_2_1.html?data=kpaflZ6We5ihhpywj5aVaZS1k5qah5yncZOhhpywj5WRaZi3jpWah5yncaTVzs7Z0ZCxs9PZz9SajbLJt8KfxM7ixsbIpc%2FVjKrZjajTttXdy9Sah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ%3D%3D&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)] 
