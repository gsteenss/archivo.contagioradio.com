Title: The harsh realities rural women face in Colombia
Date: 2019-10-18 10:28
Author: CtgAdm
Category: English
Tags: discrimination, Inter-American Commission on Human Rights, rural women
Slug: the-harsh-realities-rural-women-face-in-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/mujer-rural-cacarica.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Photo: Contagio Radio] 

 

**Follow us on Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

[In Colombia, the 5,442,241 women who live in rural areas face a host of challenges in comparison to their male counterparts and women in the city. About 37% live in conditions of poverty and only 4.85% work in agricultural production, according to the Rural Development Agency. The Inter-American Commission on Human Rights (IACHR) has demanded that violence and discrimination against rural women be eradicated — two of the biggest problems they face to access their full rights.]

[For Ana María Restrepo of the Center for Investigation and Popular Education (CINEP, for its Spanish initials), other, less obvious issues also affect rural women’s quality of life, such as the health consequences of extractivist projects, the persecution and criminalization of human rights activists, and an uptick in right-wing rhetoric that rejects the participation of women in politics, particularly in rural areas. ]

[Given the circumstances, the IACHR warns that women face a greater risk of falling victim to violence, such as forced displacement, assassinations and sexual abuse. Victims also rarely find any sort of justice. The Commission also reports that women in the countryside have a harder time accessing land, natural resources, education and health services.]

[“It’s very important to continue seeking land rights guarantees for rural women,” said Restrepo, in reference to the challenges women face to gain property titles, credits or to take a leading, decision-making role in rural areas. ]

[In most cases, it is rural women who carry our most of the unpaid workload in households — even when they are formally employed. Restrepo attributes this to the gendered discrimination women face even when they do the same work as men. “If the man farms then he is a producer, but if a woman farms she is an economic domestic. It’s important to start a conversation on the role of women as producers,” says Restrepo.]

### Diversity of women in the countryside

[Hada Marlén Alfonso, National Planning Counselor for the Rural Women Sector, says that these difficulties "are realities that have not diminished as expected despite the government’s role to guarantee rights.” Still, she highlights that various rural unions have become increasingly involved in the government to influence policy. ]

[“In rural regions, we work on the empowerment of Afro-Colombian and indigenous women. We emphasize that the government has to be assertive to guide and to make effective their gender and ethnic focuses,” says Alfonso.]

[According to Restrepo, the government must address this problem by consolidating data that will allow the situation of women in the Colombian countryside — and the diversity within the population — to be known. “It is not only about inhabiting a space but the identity that forms around a particular way of life," says Restrepo.]

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
