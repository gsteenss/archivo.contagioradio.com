Title: Después de advertencias de EEUU sobre Israel, Netanyahu afirma que aceptaría dos Estados
Date: 2015-03-20 17:31
Author: CtgAdm
Category: El mundo, Política
Tags: 42 palestinos heridos en cisjordania, Netanyahu se desdice y ahora dice que quiere dos estados, Represión Israel
Slug: despues-de-advertencias-de-eeuu-sobre-israel-netanyahu-afirma-que-habran-dos-estados
Status: published

###### Foto:Jdmuriana.wordpress.com 

##### <iframe src="http://www.ivoox.com/player_ek_4242266_2_1.html?data=lZehlJeaeo6ZmKiakpWJd6KkkZKSmaiRdI6ZmKiakpKJe6ShkZKSmaiRksbowtPmw83Zb9TZjNfS1tfFp9XVjN6Yxs7HqYzVydTfw5DVucaf1M6Y09rNqdPZjMnc1ZDJt46ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe> 

##### [Entrevista con **Alexander Montero, asesor de la embajada Palestina en Colombia**] 

**El primer ministro de Israel, Benjamin Netanyahu se retractó** sobre sus declaraciones de **rechazo a la creación de un estado palestino**, "...No quiero una solución con un solo Estado, quiero una con dos Estados, pero que sea sostenible, aunque las circunstancias aún tienen que cambiar para que eso sea posible...", esto lo declaro en la primera entrevista luego de ser reelegido para gobernar por cuatros años más Israel.

Estas palabras se dan luego que EEUU, advirtiera a Israel, de que si rechazaba los diálogos con los palestinos y la futura creación de un estado Palestino retiraría su apoyo en la ONU.

En palabras del portavoz de la **Casa Blanca Josh Earnest** sobre Netanyahu "...Ha dado marcha atrás en los compromisos asumidos previamente por Israel, lo que ha provocado que Estados Unidos evalúe cuál es el camino a seguir..."

Según **Alexander Montero, asesor de la embajada Palestina en Colombia**, para que Netanyahu pueda gobernar en Israel tiene que **alcanzar los 61 escaños de los 120** que tiene el parlamento, votación que ningún partido ha conseguido nunca, por ello es **necesario pactar con partidos pequeños de ultraderecha.**

En ese sentido Netanyahu hizo  **promesas electorales** como la de no reconocer el estado palestino, promesa para ganar puntos con  los **votantes radicales o colonos judíos**, con el objetivo de contentarles y atraer su voto. De hecho así ha sucedido, ya que en la recta final de las elecciones, y cuando las encuestas vaticinaban la subida del centroizquierda, ha habido un gran giro derechista.

Alexander Montero en entrevista para Otra Mirada afirma que las ultimas declaraciones del presidente de Israel responden a las presiones de EEUU, para que reconozca los dos estados. Las posiciones actuales de Netanyahu **suponen un desgaste político para EEUU, ante la comunidad internacional y ante sus propios socios.**

En palabras del asesor de la embajada Palestina en Colombia ".**..Que Netanyahu haya dicho que reconocerá los dos estados depende de EEUU y no de Palestina.."**, aunque es poco probable que haga el reconocimiento según el analista.

Todo esto sucede mientras continúan  jornadas represivas por parte de Israel en la que han sido **heridos 42 palestinos en Cisjordania en protestas por la construcción de más colonias ilegales.**
