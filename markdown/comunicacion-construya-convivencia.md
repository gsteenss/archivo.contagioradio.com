Title: Hay que apostarle a una comunicación que construya escenarios de convivencia
Date: 2019-07-18 13:20
Author: CtgAdm
Category: eventos, Paz
Tags: comunicacion, Justapaz, paz, periodismo
Slug: comunicacion-construya-convivencia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/Foro-Nacional-Comunicación-y-Paz.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: @Justapaz\_Col  
] 

En el marco del Foro Nacional Comunicación y Paz, diferentes analistas e integrantes de medios de comunicación alternativos, comuntiarios o independientes manifestaron algunos de los problemas y propósitos que se presentan en su labor de comunicar para la paz. A propósito de esta discusión, **la profesora Fabiola León** señaló el camino que debían recorrer los medios para aportar a una sociedad en convivencia.

### **Critica a los medios comunitarios y corporativos** 

Una de las críticas destacadas de la profesora León a los medios fue la naturalización del centralismo, es decir, que los medios se limitan a contar lo que pasa en las ciudades más grandes y poco más que eso. Así mismo, dijo que en su trabajo había evidenciado que a medida que las emisoras se alejaban de los centros urbanos, tenían mejor producción. (Le puede interesar: ["La importancia de la ética en medios públicos, una lección de Javier Darío Restrepo"](https://archivo.contagioradio.com/la-importancia-de-la-etica-en-los-medios-publicos-una-leccion-de-javier-dario-restrepo/))

Para la Profesora, en cuanto a la producción de información, en los medios comunitarios se observa un 'unifuentismo' (es decir que se acude a una sola fuente para producir información); se acude poco a las fuentes comunitarias y se mezcla información con opinión. Adicionalmente, criticó que los medios no tienen producción propia, y en su lugar, están copiando lo que se encuentra en la web. (Le puede interesar: ["Unir la comunicación con la movilización social: la clave para defender el territorio"](https://archivo.contagioradio.com/unir-la-comunicacion-con-la-movilizacion-social-la-clave-para-defender-el-territorio/))

En cuanto a los problemas que enfrentan estos medios, la profesora indicó que "no hay suficiente personal en las emisoras comunitarias"; a ello se suma que se asignan grandes responsabilidades a personas que no están preparadas para asumirlas, sobre este aspecto, la Académica cuestionó el papel de las universidades que gradúan comunicadores, porque privilegian una formación en términos técnicos e instrumentales, respecto a la preparación en conocimientos sobre los contenidos que manejarán.

### **¿Qué piensan las emisoras comunitarias sobre el trabajo de los medios?** 

**Libardo Valdez Dorado, integrante de la emisora Ocaina Stéreo** que, a su vez, hace parte de la red CANTOYACO de emisoras del Putumayo, estuvo de acuerdo en la crítica de León sobre el centralismo del periodismo. Valdez dijo que el periodista en la ciudad "mira desde el pupitre" y se limita a describir su propia realidad; en ese sentido, invitó a salir de la zona de confort, y buscar historias de las regiones para tener una mejor idea de país.

En adición, aseguró que el ejercicio periodístico en muchas ocasiones "quiere pasar a la novelería", es decir, que cuenta una historia larga que parece no tener conclusión y "es como chismoso". Por esa razón, expresó la necesidad de acudir a un periodismo que mire las potencialidades de la región, que sirva para la memoria y que dignifique a las personas; en resumen, **"debemos tener un periodismo humanizante".**

### **Por una comunicación que construya convivencia** 

Ante la actual coyuntura de incremento en la actividad por parte de grupos armados ilegales, y el reconocimiento que hacen instituciones como el Comité Internacional de la Cruz Roja (CICR) de la persistencia de conflictos armados no internacionales en el país, León declaró que los medios debemos "apostarle a una comunicación que construya escenarios de convivencia". (Le puede interesar:["En Colombia hay cinco conflictos armados: CICR"](https://archivo.contagioradio.com/colombia-conflictos-armados-cicr/))

Es decir, que los medios deben entender que hay una situación de conflicto armado y conflicto urbano, lo que **"nos tiene que hacer un llamado a incorporar nuevamente el Derecho Internacional Humanitario (DIH) y una reivindicación de los derechos humanos"** para enfrentar la violencia de los armados; pero todo ello, con la meta de seguir buscando una comunicación para la convivencia.

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
