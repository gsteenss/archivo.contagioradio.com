Title: De la criminalización del ambientalismo popular
Date: 2016-05-06 14:32
Category: CENSAT, Opinion
Tags: Ambientalismo, Defensores de los DDHH, protesta
Slug: de-la-criminalizacion-del-ambientalismo-popular
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/05/de-la-criminalizacion-del-ambientalismo-.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Censat Agua Viva 

#### **Por [CENSAT Agua Viva](https://archivo.contagioradio.com/censat-agua-viva/) - [~~@~~CensatAguaViva](https://twitter.com/CensatAguaViva)

###### 6 May 2016 

En años recientes se ha visto una intensificación de la violencia hacia defensores del territorio. El último informe de Global Witness sentencia que en el año 2014 hubo 116 casos de asesinatos de defensores del ambiente y los territorios en el mundo.  Desafortunadamente, Colombia se posiciona en el segundo lugar de esta escala, después de Brasil, con un total de 25 asesinatos, 15 de ellos indígenas. Un reciente informe de Somos Defensores revela que entre enero y marzo de 2016 han habido 113 agresiones contra defensores y defensoras de derechos humanos. Paralelo a esta lamentable situación, desde algunos medios de comunicación se ha desatado la desacreditación de las personas, organizaciones y comunidades que protegen los bienes comunes.

La crisis ambiental a la que nos enfrentamos va más allá de la degradación de determinados ecosistemas o los impactos del modelo de desarrollo. Las afectaciones que generan las actividades extractivas se expresan también en rupturas de los tejidos sociales y culturales de las comunidades,  en complicidad con la visión gubernamental enfocada en garantizar la seguridad jurídica y territorial a inversionistas corporativos.

En contraste, el ambientalismo popular construye la defensa del ambiente, partiendo de  una estrecha relación sociedad-naturaleza.  Parte del compromiso político y es sinónimo de participación, concebida como la generación de poder desde lo colectivo; donde el uso de mecanismos de participación ha impulsado debates democráticos acerca de la utilización de los bienes comunes y la capacidad de las comunidades de decidir acerca de cada situación, de ejercer su autonomía.

Desde hace décadas, el ambientalismo popular ha construido y potenciado propuestas desde la agroecología, con la promoción de mercados locales, la participación comunitaria del manejo de las aguas y con la implementación de energías alternativas y uso de la bicicleta. Se cimienta sobre la defensa de los territorios, más allá del aprovechamiento de los bienes comunes, pues incluye otras miradas del mundo, otras formas de vida armoniosas y en asocio con la naturaleza, que contribuyen a pensar otros caminos para hacer frente a la crisis ambiental, tanto en escenarios nacionales como internacionales.

Las iniciativas de comunidades, organizaciones, procesos y movimientos en lucha por su autonomía, se han visto enfrentadas a discursos con pretensión técnico - científica para justificar las actividades extractivas. Empresas, funcionarios del gobierno y algunos medios de (des)información  han tratado de enfocar todas las discusiones a meros datos científicos o cargas de la prueba que sin un contexto social y cultural, y sin el diálogo de saberes necesario en la definición de afectaciones y realidades concretas territoriales, confunden más de lo que iluminan, pero legitiman falazmente los proyectos desde la anulación de los interlocutores.

Esta situación  evidencia la precariedad funcional en las instituciones gubernamentales delegadas para la defensa del ambiente, entre otras, quienes se escudan en su incapacidad de realizar estudios que permitan determinar la pertinencia de actividades extractivas. De esta forma se abre paso a que se entreguen permisos y licencias a empresas que, a partir de su accionar, destruyen la naturaleza y con ello la vida de muchas comunidades y saberes populares milenarios.

Es responsabilidad del Estado colombiano satisfacer las necesidades de sus habitantes y garantizar el respeto de los derechos, cuestiones que no pueden obviar la soberanía territorial. Estas responsabilidades pasan por el respeto a todas las formas específicas de relación, práctica y significado de las personas y la naturaleza. Casos como el del departamento del Cauca, que en semanas pasadas arroja más de 8 personas agredidas por su lucha en defensa del territorio, los recientes asesinatos de personas que desempeñaban dicha labor, dan cuenta de la intensificación de un modelo neoliberal que va más allá de fronteras, que agrede, lesiona y justifica sus acciones violentas a partir de discursos de desarrollo.

Proteger el ambiente hoy se percibe como un riesgo en un contexto cambiante en el que se intensifican señalamientos nocivos hacia los y las defensoras territoriales.

Hacemos un llamado a construir un  debate donde se deje de lado la estigmatización y criminalización, sin volcar exclusivamente a lo técnico-científico un debate que es evidentemente político  e interpela la necesidad de validar otros saberes y pŕacticas en las narrativas de los conflictos ambientales.

[\*\*\*]

[Terminando de escribir este texto fue asesinado Manuel Dolores Pino Perafán, secretario general de la Junta comunal del Corregimiento Fondas, del municipio Tambo (Cauca), reconocido por la lucha contra la minería ilegal en su territorio.]

Amigos de la Tierra – Colombia

------------------------------------------------------------------------

###### Las opiniones expresadas en este artículo son de exclusiva responsabilidad del autor y no necesariamente representan la opinión de la Contagio Radio. 
