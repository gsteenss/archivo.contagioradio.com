Title: Campesino resulta herido por represión de la fuerza pública en Valparaíso, Caquetá
Date: 2016-08-16 13:53
Category: DDHH, Nacional
Tags: Caquetá, Emerald Energy, ESMAD, petroleo
Slug: campesino-resulta-herido-por-represion-de-la-fuerza-publica-en-valparaiso-caqueta
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/08/Caqueta.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Caracol Radio 

###### [16 Ago 2016] 

Una persona resultó herida en medio de la represión por parte del ESMAD y el Ejército Nacional hacia la comunidad de Valparaíso que sigue firme en no permitir que las empresas petroleras ingresen al territorio del departamento del **Caquetá donde ya existen [43 bloques petroleros que tienen en peligro parte de la Amazonía colombiana.](https://archivo.contagioradio.com/43-bloques-petroleros-en-caqueta-amenazan-la-amazonia-colombiana/)**

De acuerdo con Martín Trujillo, coordinador de la Mesa de la defensa del territorio y la vida municipio de Paujil,  **un campesino salió herido por una bala de fusil proveniente del Ejército, ** por lo que tuvo que ser intervenido quirúrgicamente ante la afectación en su sistema digestivo.

El hecho se dio en medio de la represión por parte de la fuerza pública, que dice seguir órdenes de la Presidencia de la República, contra las comunidades que intentan frenar la materialización del contrato de exploración de hidrocarburos que existe entre el gobierno y la empresa Emerald Energy.

Una represión que se fortaleció luego de que los pobladores llevaran a cabo diferentes movilizaciones pacíficas en contra de la actividad petrolera y por lo cual la fuerza pública optó por poner tanquetas del ESMAD para impedir el paso de los campesinos. **"Estamos casi secuestrados en tres áreas que cubren parte de Milán y la Montañita".**

Los pobladores del  municipio de Valparaíso, adelantan procesos de resistencia desde el año 2014. La Mesa Departamental por la Defensa del agua y el Territorio, trabaja para sensibilizar a la población sobre este tema, para lo que se ha implementado una **comisión accidental de hidrocarburos** que trabajará con los Consejos municipales con el fin de socializar en las  juntas de acción comunal las consecuencias de la extracción y explotación petrolera. “**Nosotros sabemos que la exploración sísmica de petróleo, causa mayores daños ambientales**, se fragmenta la roca donde permanece el petróleo y se contaminan las fuentes hídricas, se trata de daños irreversibles” expresa Trujillo.

Meses atrás la comunidad había advertido de la presencia del ESMAD justo después de que llegó la maquinaria para empezar la actividad petrolera, así mismo, habían denunciado que a causa de la llegada de las multinacionales se vive un conflicto socio-ambiental que ha generado amenazas contra algunos concejales y líderes que se oponen a estos proyectos minero-energéticos.

**"La función de esta región debe ser producir agua, las fuentes hídricas empezaron a disminuir sin haber entrado la locomotora minero-energética**, cuando ésta ingrese con toda su fuerza, esta condición de productor de agua cambiará y perderemos la cantidad de agua que se produce", es la preocupación que evidencia la docente Mercedes Mejía, integrante de la Mesa Departamental por la Defensa del Agua y el Territorio.

Pese a las denuncias frente a la **Defensoría del Pueblo y por parte de las organizaciones de Derechos Humanos,** Martín Trujillo asegura que la Gobernación de Caquetá amenaza a municipios con quitar regalías si no aceptan la actividad petrolera, que ya tiene  licencias en 16 municipios para exploración pese al rechazo de comunidades que desde ya gestionan una consulta popular para decirle no a la entrada de petroleras al departamento.

<iframe src="http://co.ivoox.com/es/player_ej_12564523_2_1.html?data=kpeimJmZdpShhpywj5adaZS1k5Wah5yncZOhhpywj5WRaZi3jpWah5ynca7V09nW0JC4ttbeytHZ0YqWh4zBxtjOjcnJb83VjMnSyMrSt8KfxcrZjdnJttPd1dTfy9SPvYzgwpKSmaiRh9Di1cbUy9SPlsLYytSYj4qbh46o&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
