Title: Las falacias del fiscal Néstor Humberto Martínez para objetar la JEP
Date: 2019-02-19 16:41
Author: AdminContagio
Category: Paz, Política
Tags: Fiscal Néstor Humberto Martínez, JEP, Ley estatutaria
Slug: falacias-para-objetar-jep
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/02/Captura-de-pantalla-2019-02-19-a-las-4.00.42-p.m.-e1550610229588-770x400.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Fiscalía General de la N] 

###### [19 Feb 2019] 

[Este lunes el fiscal Néstor Humberto Martínez señaló en una misiva al presidente Duque, cuatro “preocupaciones” sobre la Ley Estatutaria de la Jurisdicción Especial para la Paz (JEP), que espera su sanción presidencial. Según lo dicho por el Fiscal en diferentes entrevistas, **lo que pretende con la carta es que estos puntos sean objetados, y que el Proyecto de Ley haga nuevamente trámite por el Congreso para reformularlos**.]

[Sin embargo, analistas han señalado que en caso de regresar al Congreso, el Proyecto afectaría el funcionamiento de la JEP, retrasando todos sus procesos; adicionalmente **el resultado de este retorno al legislativo sería un documento similar al que actualmente espera la sanción presidencial** para convertirse en Ley de la República. (Le puede interesar: ["Presidente Duque sancione la Ley Estatutaria de la JEP: Organizaciones"](https://archivo.contagioradio.com/duque-ley-estatutaria-jep/))]

### **Conductas de ejecución permanente serán juzgadas por la JEP** 

[Este tipo de delitos son aquellos que se cometen a lo largo del tiempo y no se circunscriben en un solo acto a diferencia del homicidio o el robo, por ejemplo. El Fiscal señala que es inconveniente que este tipo de delitos, cometidos después del 1 de diciembre de 2016, no puedan ser investigados por la justicia ordinaria y sigan en competencia de la JEP. (Le puede interesar: ["¿Firmará Duque la Ley Estatutaria de la JEP?"](https://archivo.contagioradio.com/ley-estatutaria-jep/))]

[Tal como lo aclara el **abogado y defensor de derechos humanos Alirio Uribe Muñoz,** la Jurisdicción debe tener la potestad para evaluar este tipo de conductas de ejecución permanente, pues en ellas se verían casos como que excombatientes de las FARC tuvieran municiones o armas (delito de porte ilegal de armas) aún después de firmado el Acuerdo, por los tiempos mismos que le tomó a la Misión de Naciones Unidas recoger el material bélico.]

[Pero este sería un caso exepcional, en ese sentido, la Corte Constitucional fue clara en que los delitos iniciados antes de diciembre de 2016 serán juzgados por la JEP, y **en caso que miembros de las extintas FARC cometieran delitos con posterioridad a esa fecha, la Jurisdicción abriría un incidente de incumplimiento** y serían investigados por la justicia ordinaria.]

### **Reincidencia en ciertos delitos implica perder beneficios de la JEP** 

[En su carta, el Fiscal señala que quienes reinciden en delitos como narcotráfico, homicidio o secuestro conservarían los beneficios otorgados por parte de la Justicia transicional. No obstante, la Corte Constitucional decidió que **la Jurisdicción reducirá gradualmente los beneficios a quienes no cumplen las condiciones para hacer parte de este mecanismo** **de justicia**, entre ellas, la verdad, la reparación y las garantías de no repetición.]

[Adicionalmente, se aplica lo mencionado en el punto anterior, pues **la JEP quitará los beneficios a quienes cometan delitos posteriores a la firma del Acuerdo de Paz**, abriendo un incidente de incumplimiento y trasladando el caso a la jurisdicción ordinaria. (Le puede interesar: ["Modificaciones a la JEP serían inconstitucionales: Gustavo Gallón"](https://archivo.contagioradio.com/modificacion-a-tribunales-de-la-jep-es-inconstitucional-gustavo-gallon/)) ]

> La Corte Constitucional ya resolvió qué le pasa a quienes reinciden:
>
> 1\. La JEP le quita los beneficios a quienes vuelven a delinquir, no dicen la verdad y no reparan; y
>
> 2\. La JEP le reduce gradualmente los beneficios a quienes solo cumplen algunas de las condiciones. [pic.twitter.com/50aXOFsJJt](https://t.co/50aXOFsJJt)
>
> — Juanita Goebertus (@JuanitaGoe) [18 de febrero de 2019](https://twitter.com/JuanitaGoe/status/1097643743237324800?ref_src=twsrc%5Etfw)

### **Fiscalía sí puede seguir investigando** 

[El Fiscal señala que en la Ley, los entes investigadores quedan atados de manos para continuar desarrollando procesos sobre delitos cometidos antes de la firma del Acuerdo, esto es así porque la Fiscalía General de la Nación no puede realizar capturas, determinar responsabilidades ni citar a diligencias jurídicas. (Le puede interesar: ["JEP preservará los archivos del DAS"](https://archivo.contagioradio.com/jep-preservara-los-archivos-del-das/)) ]

[No obstante, la Fiscalía si puede continuar investigando los casos y decretar pruebas hasta que el Tribunal de Paz de la JEP anuncie su resolución de conclusiones. La pregunta entonces es sobre el interés de la Fiscalía por avanzar en los casos, pues como lo recuerda Uribe Muñoz, **“hace dos años el Fiscal dio la orden de no mover procesos” referentes a militares involucrados en casos de ejecuciones extrajudiciales**.]

### **No es cierto que haya impunidad en la JEP** 

[El Fiscal señala que Colombia está cediendo su posibilidad a investigar y sancionar a todos los actores en conflicto que no se consideren grandes responsables por crímenes de lesa humanidad, situación que conllevaría la actuación de la Corte Penal Internacional (CPI). Pero el Abogado y defensor de Derechos Humanos, resalta que lo que ocurrirá en la JEP será una priorización de casos; lo que no se traduce en impunidad sino en el reconocimiento de grandes patrones de acción.]

[A ello se suma que la CPI avaló el Marco Jurídico para la Paz, y en cualquier caso, **los crímenes de lesa humanidad** cometidos por agentes del Estado como por integrantes de las desmovilziadas FARC, **serán juzgados en la medida en que la JEP pueda avanzar en el desarrollo de sus funciones**. (Le puede interesar: ["Las razones de las víctimas para oponerse a modificaciones de la JEP"](https://archivo.contagioradio.com/las-razones-de-las-victimas-para-oponerse-a-modificacion-de-la-jep/)) ]

### **Lo inconveniente es que la JEP no funcione con todo su andamiaje jurídico** 

[En el principio y final de la carta, el Fiscal señala su preocupación ante una intervención de la CPI por una supuesta impunidad, pero como lo asegura Uribe Muñoz, el Fiscal desde el trámite del Proyecto de Ley Estatutaria de la JEP intentó obstruirla, quitarle competencia y generar conflictos con la justicia ordinaria; todo ello, evitando que esta Institución cuente con un marco legal completo para su actuación, mientras la CPI ha mantenido su decisión de respetar este mecanismo transicional, y las posibles acciones que desarrolle.]

[En ese sentido, Uribe Muñoz concluye que el Proyecto de Ley Estatutaria de la JEP no tiene inconveniencias, sino que **lo inconveniente es no permitir que la Jurisdicción cuente con todo el andamiaje jurídico para operar**, y evitar ahí sí, una acción de la CPI contra presuntos violadores de derechos humanos. (Le puede interesar: ["Cuatro preocupaciones de la Corte Penal Internacional sobre cambios a la JEP"](https://archivo.contagioradio.com/cuatro-preocupaciones-corte-penal-internacional-cambios-jep/))]

<iframe id="audio_32688576" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_32688576_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
