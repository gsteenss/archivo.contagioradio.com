Title: Situación de derechos humanos en Colombia "una preocupación mundial"
Date: 2016-11-07 10:50
Category: DDHH, Nacional
Tags: Acumulacion de tierras, Equidad de género
Slug: situacion-de-derechos-humanos-una-preocupacion-mundial
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/misión-Parlamentarias-e1478285522944.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [4 de Nov 2016] 

La Coordinación Colombia Bélgica desarrolló una misión de información e intercambió sobre temas de agricultura, comercio y derechos humanos en Colombia que dio como resultado una serie de observaciones **graves frente a la crítica situación del despojo y la restitución de tierras, la falta de garantías para víctimas y organizaciones sociales y la afectación de tratados de libre comercio sobre las comunidades del país.**

En la misión conformada por parlamentarias de países como Bélgica, Austria y España, pudo evidenciar situaciones como la **falta de financiación por parte de las instituciones a organizaciones defensoras en derechos humanos, la falta de estrategias y políticas públicas sobre género y  la acumulación de tierras que persiste en el país, pese al proceso de restitución.**

Tania González Peña, parlamentaria y representante por el partido popular Podemos, visitó la región del Cauca y se refirió a la acumulación de tierras en el país “encontramos una clara deficiencia en el proceso de restitución de las tierras, **son muy pocas las tierras que se han restituido frente a las despojadas, elemento fundamental a la hora de construir la paz en Colombia**”. Le puede interesar: ["20 empresas deberán devoler 53.821 hectáreas de tierras despojadas"](https://archivo.contagioradio.com/20-empresas-deberan-devolver-53-821-hectareas-de-tierras-despojadas/)

Otro de los problemas que encontraron son las diferentes dificultades a las que se enfrentan los campesinos cuando retornan a sus tierras, una de ellas es la **falta de acompañamiento institucional debido a la lentitud de estos procesos**. Además informaron que aún **hay presencia de grupos armados ilegales que continúan amenazando a las comunidades**. Frente a este panorama dos de las recomendaciones de la misión son por una parte agilizar el proceso de restitución de tierras y por el otro combatir a estos grupos armados.

Frente al modelo extractivita y agroindustrial que se ha profundizado con los tratados de libre comercio, González indicó que encontraron que están fuertemente relacionados con la violación de derechos humanos, “**generando inequidad entre los pequeños productores y las grandes multinacionales, daños ambientales y desplazamiento de poblaciones**”. Situación que podrá tener seguimiento y control por parte de la Unión Europea.

Sobre la equidad de género, la misión encontró que hace falta demasiado apoyo a estas organizaciones y señalaron que **el enfoque de género es un paso necesario** “el enfoque de género no se trata de una dictadura o de hechos para poner a las mujeres por encima de alguien, sino se trata de equilibrar la situación” afirmó González.

La misión estuvo conformada por las parlamentarias Gwenaelle Grovonius, Olga Zrihen, Helen Rickmans, Petra Bayr y Tanía González, que visitaron 3 departamentos del país: Cauca, Valle del Cauca y Antioquía y que tuvieron la posibilidad de reunirse con diferentes sectores políticos y organizaciones defensoras de derechos humanos.

<iframe id="audio_13619152" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_13619152_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en su correo [[bit.ly/1nvAO4u]
