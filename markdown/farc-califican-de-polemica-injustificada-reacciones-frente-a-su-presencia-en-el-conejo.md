Title: Farc califican de "polémica injustificada" reacciones frente a su presencia en el Conejo
Date: 2016-02-19 18:03
Category: Nacional, Paz
Tags: Conversaciones de paz de la habana, FARC, Juan Manuel Santos, proceso de paz
Slug: farc-califican-de-polemica-injustificada-reacciones-frente-a-su-presencia-en-el-conejo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/01/FARC-e1453154641281.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: AFP] 

###### [19 Feb 2016]

En el marco del tercer encuentro de [pedagogía de paz](https://archivo.contagioradio.com/continua-el-foro-dialogos-de-paz-y-libertad-de-prensa/) en el corregimiento de Conejo, municipio de Fonseca, Guajira, miembros de la Delegación de Paz de las FARC-EP arribaron al territorio el pasado jueves 18 de febrero con el fin de participar en tal evento organizado por [“El movimiento de Artistas y Académicos por la Paz”.](https://archivo.contagioradio.com/patricia-ariza-una-vida-por-la-cultura-y-la-paz/)

La visita de tres de los integrantes de la delegación de paz de las FARC, Iván Márquez, Joaquín Gómez y Jesús Santrich, orientada a la socialización de los avances del proceso con los guerrilleros y la población civil, ha desatado una serie de reacciones en contra por parte de diversas figuras de la política nacional.

Frente a la polémica, las FARC-EP emitieron un comunicado en el que aseguran,** **que los **responsables de generar tal discusión son los "[voceros de la derecha guerrerista](https://archivo.contagioradio.com/extrema-derecha-teme-asumir-acuerdo-de-justicia/)",** "quienes pregonan la continuación de la guerra y argumentan un supuesto proselitismo político de la guerrilla con el fin de imponer el aislamiento de la delegación y prohibir su interacción con los distintos sectores de la sociedad interesados en conocer[los desarrollos del proceso.](https://archivo.contagioradio.com/2015-un-ano-entre-la-paz-la-guerra-y-la-esperanza/)"

De igual forma, el comunicado aclara que **la presencia de los voceros de las FARC en la Guajira tuvo "la intensión de llevar un mensaje de paz y reconciliación** a uno de los departamentos más castigados por el olvido y[la corrupción de las élites gobernantes](https://archivo.contagioradio.com/empresas-y-estado-responsables-de-la-situacion-en-la-guajira/)", respetando los protocolos acordados por las partes referente al traslado de los delegados de las FARC al país [para hacer pedagogía de paz.](https://archivo.contagioradio.com/convocar-a-la-sociedad-civil-legitima-el-proceso-de-paz/)

Finalmente, la guerrilla insta al gobierno nacional a mantener en firme la decisión de sortear cualquier obstáculo que pueda presentarse en el camino de la solución política y a trabajar conjuntamente para superar este impase.

Por su parte el gobierno nacional instó a acelerar las conversaciones de paz y prohibió el traslado de los integrantes de la delegación de paz de las FARC a Colombia para hacer pedagogía para la paz. La decisión fue anunciada por el jefe de la delegación de paz del gobierno y reafirmada por el presidente Juan Manuel Santos, quién además ratificó que el 23 de Marzo es la fecha establecida para la firma del acuerdo.
