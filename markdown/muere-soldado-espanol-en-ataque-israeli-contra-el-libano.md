Title: Muere soldado español en ataque israelí contra el Líbano
Date: 2015-01-28 21:34
Author: CtgAdm
Category: DDHH, El mundo
Tags: FINUL, Israel, Líbano, ONU, soldado español
Slug: muere-soldado-espanol-en-ataque-israeli-contra-el-libano
Status: published

###### **Foto: Gatomilitaria.blogspot** 

El cabo Soria se encontraba en la Blue Line o zona fronteriza, donde se posicionan las fuerzas de naciones unidas **(FINUL),** conocidas como **cascos azules, a las cuales pertenecía el soldado fallecido**. Dichas posiciones también han sido alcanzadas por los bombardeos israelíes.

Anteriormente la milicia de Hezbollah había lanzado un ataque con misiles antitanques contra el ejercito israelí causando dos muertos y siete heridos, al cual las Fuerzas de Defensa  han respondiendo **bombardeando posiciones del sur del Líbano.**

El contingente Este de la misión está liderado por las tropas españolas, que cuentan con base propia en el terreno. El batallón está en la zona desde 2006, cuando se acordó establecer la misión de paz por parte de las Naciones Unidas.

El gobierno español ha expresado su pesar por la muerte del soldado  y ha **exigido una investigación exhaustiva por parte de la ONU** con el objetivo de esclarecer los hechos.

La primera reacción por parte del gobierno israelí ha sido del representante de la embajada en España que ha lamentado la muerte de cualquier miembro de la misión de las naciones unidas FINUL.
