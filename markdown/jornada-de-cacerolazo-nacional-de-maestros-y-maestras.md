Title: Jornada de cacerolazo nacional de maestros y maestras
Date: 2015-04-29 15:59
Author: CtgAdm
Category: Educación, Nacional
Tags: educacion, fecode, gina parody, Juan Manuel Santos, magisterio, Ministerio de Educación, Paro Nacional
Slug: jornada-de-cacerolazo-nacional-de-maestros-y-maestras
Status: published

##### Foto: Contagio Radio 

Continúa el paro y las manifestaciones a nivel nacional de los maestros y maestras  que exigen una solución a sus exigencias, es por eso que han rechazado la propuesta de la Ministra de Educación, Gina Parody, que pidió suspender el paro por cinco días para iniciar una mesa de negociaciones. Ante la propuesta la Federación Colombiana de Trabajadores de la Educación , Fecode, decidió entablar un diálogo directo con el Presidente Juan Manuel Santos, ya que piensan que **hace falta voluntad política por parte de la ministra**, quien en días anteriores había dicho que se trataba de un "paro injustificado".

El presidente de Fecode, Luis Alberto Grubert, asegura que **“el presidente ya debería haberles prestado atención”** sobre los cuatro puntos en los que se basan sus reclamos: la nivelación salarial, evaluación docente, protección para profesores y profesoras amenazados o que ejercen en zonas de alta peligrosidad, y Mejoramiento en el servicio de salud. [(Ver nota relacionada).](https://archivo.contagioradio.com/334-000-profesores-se-suman-al-paro-nacional-de-fecode/)

Lo anterior teniendo en cuenta que se han **asesinados alrededor de 1000 profesores** y además no se han aprobado los traslados de los docentes amenazados. Respecto a la parte salarial, según la Federación **un profesor de educación pública gana en promedio 1'200.000 pesos y un docente que tenga máster puede recibir máximo de 2'300.000** pesos.

Ante la falta de disposición del gobierno, el magisterio ha decidido mantener el paro hasta que no exista un compromiso definitivo de parte del gobierno por medio de la instalación de una mesa de conversaciones.

Este el miércoles el paro estárá acompañado por una **jornada de cacerolazos por todo el país frente a las sedes del Ministerio de Educación.**
