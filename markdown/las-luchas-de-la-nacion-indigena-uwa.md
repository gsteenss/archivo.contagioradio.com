Title: Las luchas de la Nación indígena U'wa
Date: 2016-06-07 22:13
Category: Hablemos alguito
Tags: Corte Constitucional, indígenas de Colombia, Nevado del Cocuy, Uwa
Slug: las-luchas-de-la-nacion-indigena-uwa
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/06/Uwa.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Y[aku.eu]

###### [7 Jun 2016] 

La Nación indígena U'wa ubicada en los departamentos de Boyacá, Arauca, Norte de Santander, Casanare, Santander son una de las comunidades indígenas en peligro de desaparecer según la propia Corte Constitucional. Los indígenas han debido enfrentarse a diferentes amenazas entre las que se encuentra el modelo extractivista y el ecoturismo.

El pueblo U'wa tiene **una larga historia de lucha contra las empresas multinacionales.** Los indígenas se oponen públicamente a la explotación petrolera, que es para ellos sinónimo de etnocidio. Denuncian la negligencia del Estado para resolver la situación de los territorios que la ley les reconoce como resguardos, y aunque en 1991 se aprobó la nueva Constitución que reconoce y protege la diversidad étnica y cultural ese mismo año se firmó  un contrato entre Ecopetrol y empresas petroleras en tierras U’was.

La afectación ambiental en el Nevado del Cocuy se debe principalmente a que las autoridades ambientales no han hecho cumplir la normativa en materia de protección y conservación, y por lo tanto, no ha habido un buen manejo de las aguas negras como lo asegura Corpoboyacá, **contaminando las fuentes hídricas de los más de 7 mil indígenas asentados en la zona.**

En Hablemos Alguito, hablamos con Aura Tegría, integrante y asesora jurídica de la Nación U'wa, quien habló sobre estas problemáticas y el cómo los indígenas han resistido pese al abandono estatal.

<iframe src="http://co.ivoox.com/es/player_ej_11827219_2_1.html?data=kpallJyWdZqhhpywj5mcaZS1kZ2ah5yncZOhhpywj5WRaZi3jpWah5ynca3V1JDZ18jMpdSfxcqYzsaPksLXyoqwlYqmd8-fytPRh6iXaaK4yMrbw5C5aZOr2MaYj5CspcPgxtLc1ZDFsMjpjoqkpZKns8_owszW0ZC2pcXd0JCah5yncZU.&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>
