Title: Eurocine, Festival de Cine Europeo en Colombia presentó su 21 edición
Date: 2015-03-19 21:44
Author: CtgAdm
Category: 24 Cuadros, El mundo
Tags: Eurocine
Slug: eurocine-festival-de-cine-europeo-en-colombia-presento-su-21-edicion
Status: published

###### Foto: eurocine 

#### *Medios de comunicación en Bogotá presenciaron el lanzamiento del Festival 2015.*

Con la proyección de la comedia alemana *Die kirche bleibt im dorf*, 2012 (La iglesia permanece en el pueblo) dirigida por Ulrike Grote, fue presentada oficialmente para medios de comunicación la edición número 21 de Eurocine, Festival de cine europeo en Colombia.

Teresa Hope, directora y curadora del evento conversó con 24 Cuadros de Contagio Radio en relación con lo que encontraran en las salas a partir del 15 de Abril “*Vamos a encontrar muchas películas que son de pronto muy comerciales en sus países de origen pero que tal vez no han salido de sus fronteras, que me parece tienen una trascendencia más allá del país de donde vienen, sin embargo también se ven ahí muy plasmados temas culturales que es un poco lo que Eurocine quiere: hacer puentes, crear comunicación y también incentivar el cómo nos podemos imaginar Europa porque son muchos países y son muy diferentes entre ellos y a través de la cinematografía nos podemos enterar un poco que está pasando por allá*”.

Diferentes medios de comunicación hicieron presencia en la sala de Cine Colombia Avenida Chile, lugar el lanzamiento que contó además con la participación de Lars Steen Nielsen, embajador de Dinamarca en Colombia, en representación del país invitado de honor al Festival quien manifestó al respecto “*Estamos muy orgullos que Dinamarca sea el invitado a este Festival de Cine, tenemos ocho películas, he visto algunas y son bastante buenas. Dinamarca tiene una tradición de películas de alto nivel, empezamos hace como 100 años atrás y en los últimos 28 años ha ganado 3 Oscar para mejor película extranjera y varias Palmas de Oro en Cannes. Dinamarca no es un país muy grande pero tenemos cineastas muy famosos, incluso algunos que han hecho su propio camino como Lars Von Trier y Thomas Vinterberg entre ellos, como reacción a las mega producciones norteamericanas de alto presupuesto*”.

<iframe src="https://www.youtube.com/embed/Vmjbv-VeZe4" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

Los asistentes recibieron además el afiche oficial del evento, con la imagen diseñada por Ricardo Tribin quien describió en detalle los elementos que hacen parte de su creación “*La idea del afiche este año era mostrar un poco que Europa nos está visitando a nosotros en el trópico, por eso tenemos las matas y esa mata que ven es una muy tropical que se llama el Balazo o la Monstera deliciosa que es muy típica del trópico, que está en la casa de todas las abuelas, en la de la mía estaba, la Guacamaya, representando todos los animales y la biodiversidad y los colores que nos llevan a ese sentimiento de calidez del trópico*”.

Bogotá (15 al 26 de Abril), Medellín (30 de Abril al 7 de Mayo), Cali (8 al 2 de Mayo), Manizales (12 al 17 de Mayo) y otras diez ciudades hacen parte del recorrido del festival que inicia en el Gimnasio moderno de Bogotá y que rodará por las salas y espacios alternativos incluidos en la propuesta del Eurocine comunitario, en busca de atraer nuevas audiencias, uno de los objetivos en este 2015 según su directora “*Queremos ampliar el público, este año logramos negociar más producciones por lo cual podemos tener más salas, podemos tener más funciones por tanto tener más personas en las películas que tenemos y nuestro comunitario que va por toda Colombia, este año con nuevas ciudades pequeñas, por lo que espero alcanzar la misma cifra de asistentes del año pasado y ojala más*”.

La edición 21 de Eurocine llegará con producciones para todos los públicos en diferentes espacios para que los colombianos tengan la posibilidad de abrir una ventana al viejo continente y en particular a la cultura de los diferentes países que hacen parte de esta muestra audiovisual gracias a la selección realizada por la misma Teresa Hope “*La verdad ha sido muy divertido para mi hacer la curaduría este año, sé que hay muy buenas películas en esta versión de Eurocine y ojalá nos acompañen también en esta versión número 21, creo que este año hay muchas películas que también son aptas para un público un poco más amplio porque no son tan complicadas de entender, hemos tratado de traer cine para familias, de diversión, y algunas que, aunque traten temas tristes o complicados, siempre se vean desde un punto de vista de fe y de esperanza, entonces espero que nos acompañen, bien sea en las salas oficiales o en el comunitario de Eurocine 2015*”.
