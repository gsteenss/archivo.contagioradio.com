Title: Después de 23 años de lucha, a familia restituida solo le quedó la tierra y un techo
Date: 2019-10-01 00:14
Author: CtgAdm
Category: Entrevistas, Paz
Tags: Ley de Víctimas, Restitución de tierras, Techo, uraba
Slug: despues-de-23-anos-de-lucha-a-familia-restituida-solo-le-quedo-la-tierra-y-un-techo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/09/Restitución-de-Tierra.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: @GioPalacios5] 

Luego de 23 años de vivir el despojo, la familia de Manuel Oviedo pudo regresar a su tierra en Chigorodó, Antioquia, gracias al proceso de restitución. Sin embargo, durante la apropiación ilegal de este predio, el despojador destruyó la casa y todas las construcciones que había en el territorio, razón por la cual los que los vecinos de Oviedo decidieron regalarle un techo. (Le puede interesar:["En 25 años de Ley de tierras, más del 60% de fincas siguen sin derechos de propiedad"](https://archivo.contagioradio.com/ley-de-tierras-mas-fincas-siguen-sin-derechos-de-propiedad/))

### **La restitución: Un techo, la tierra y algunos incumplimientos** 

Según Manuel Oviedo, presidente de la Asociación, el proceso de restitución de la zona (denominada por los despojadores como Hacienda Monte Verde) fue posible gracias a la Ley 1448 o Ley de víctimas. En el lugar, 7 familias han sido beneficiadas con la restitución, incluída la de Manuel, **pero en total son 34 familias reclamantes de cerca de 7 mil hectáreas**. (Le puede interesar: ["Después de 20 años de lucha, campesinos regresan a sus tierras en Guacamayas, Urabá"](https://archivo.contagioradio.com/despues-de-20-anos-de-lucha-campesinos-regresan-a-sus-tierras-en-guacamayas/))

Antes que Manuel llegara a sus tierras, denunció que el despojador tumbó los corrales, la casa y las cercas que había en su terreno, según su relato, **"ahora que me restituyeron me dejaron puro pasto".** Entonces, un vecino le regaló el techo viejo que cargaron otras familias en proceso de restitución. (Le puede interesar:["En Colombia una comunidad campesina ha luchado más de 40 años por el derecho a la tierra"](https://archivo.contagioradio.com/el-garzal-una-comunidad-campesina-ha-luchado-mas-de-40-anos-por-el-derecho-a-la-tierra/))

Este no es el único caso, la familia de Laureano Gómez, vicepresidente de la Asociación, también fue restituida recientemente (el pasado 1 de abril), no obstante, **no ha podido asentarse en su territorio porque los despojadores "no lo han dejado ingresar y trabajar"**. (Le puede interesar: ["Los seis obstáculos a la restitución de tierras en Urabá"](https://archivo.contagioradio.com/obstaculos-restitucion-de-tierras/))

### **La seguridad para los restituyentes** 

Oviedo explicó que los despojadores "son empresarios ganaderos, bananeros y palmicultores" que en algunos casos compraban la tierra a bajo costo mientras las personas huían del paramilitarismo, y en otros casos como el suyo, se trató de una apropiación: "Asesinaron a mi hermano el 13 de septiembre de 1996, el 14 fuimos a enterrarlo y cuando regresamos del entierro a mí no me dejaron ya entrar a mi finca".

Con su tierra restituida, las amenazas para Oviedo no han cesado: "Nos mandan amenazas con personas, nos mandan razones... por medio de panfletos (...) un escolta que me cambiaron hace poco un hombre le dijo que yo estaba muy caliente". Manuel es protegido de la Unidad Nacional de Protección (UNP), por lo que tiene medidas blancas: Un hombre de a pie, un chaleco y un celular; razón por la que pidió que se refuerce su seguridad, para garantizar su derecho a una vida digna, así como la de las demás familias restituyentes.

<iframe id="audio_42418329" frameborder="0" allowfullscreen scrolling="no" height="200" style="border:1px solid #EEE; box-sizing:border-box; width:100%;" src="https://co.ivoox.com/es/player_ej_42418329_4_1.html?c1=ff6600"></iframe>  
**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
