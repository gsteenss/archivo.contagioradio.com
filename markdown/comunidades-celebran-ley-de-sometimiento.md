Title: Comunidades rurales celebran Ley de Sometimiento
Date: 2018-07-13 09:22
Category: Nacional, Paz
Tags: Autodefensas Gaitanistas de Colombia, Conpaz, Ley de Sometimiento de Bandas Criminales, paz
Slug: comunidades-celebran-ley-de-sometimiento
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/07/Captura-de-pantalla-2018-07-13-a-las-8.52.03-a.m..png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: @Conpaz\_] 

###### [13 Jul 2018]

La red de Comunidades Construyendo Paz en los Territorios CONPAZ, celebraron la oportunidad abierta por la Ley de Sometimiento a bandas criminales para que estas organizaciones se acojan a la justicia y de esa manera acceder a la verdad y a la reconciliación.

Por medio de un comunicado, la organización pidió **que se escuche su clamor de paz y cese la violencia en los territorios,** dado que ellos han sido afectados por "los daños que imponen ilegalmente empresarios al servicio de economías como el narcotráfico".

Igualmente,  **solicitó a las Autodefensas Gaitanistas de Colombia comprometerse con la verdad, la reparación a las víctimas y la construcción de la paz**, al reiterar su deseo de ver a los jóvenes que están en sus filas rompiendo el circulo de muerte y exclusión.

De otra parte, manifestaron su sentimiento de esperanza por cuenta de **este mecanismo legal que permite avanzar en la "terminación de otra expresión de la violencia"** que los afecta,  e instaron al Gobierno a garantizar el  cumplimiento del debido proceso.

Finalmente, CONPAZ señaló su promesa de hacer todo lo posible para "abrir camino a la reconciliación en la justicia socio ambiental, en el respeto a sus vidas y su dignidad y la dignidad de todas las víctimas sin exclusión alguna". (Le puede interesar: ["Nuevos respaldos a Ley de Sometimiento de bandas criminales"](https://archivo.contagioradio.com/apoyos-ley-de-sometimiento-bandas-criminales/))

\[caption id="attachment\_54746" align="aligncenter" width="677"\][![CONPAZ](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/07/Captura-de-pantalla-2018-07-13-a-las-9.14.12-a.m.-677x629.png){.size-medium .wp-image-54746 width="677" height="629"}](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/07/Captura-de-pantalla-2018-07-13-a-las-9.14.12-a.m..png) CONPAZ\[/caption\]  
 

###### [Reciba toda la información de Contagio Radio en] [[su correo] [[Contagio Radio]
