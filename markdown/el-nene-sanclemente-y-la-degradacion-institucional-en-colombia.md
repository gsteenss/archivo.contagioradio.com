Title: El 'Ñeñe', Sanclemente y la degradación institucional en Colombia
Date: 2020-03-15 16:53
Author: AdminContagio
Category: Actualidad, Política
Tags: Alvaro Uribe, Fernando Sanclemente, Ñeñe Hernández
Slug: el-nene-sanclemente-y-la-degradacion-institucional-en-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/03/Ñeñe-Hernandez-Sanclemente-y-Uribe.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:heading {"align":"right","level":6,"textColor":"cyan-bluish-gray"} -->

###### Foto: Contagio Radio {#foto-contagio-radio .has-cyan-bluish-gray-color .has-text-color .has-text-align-right}

<!-- /wp:heading -->

<!-- wp:paragraph -->

En los primeros días de marzo, el periodista Gonzalo Guillén publicó información obtenida de audios en poder de la Fiscalía que probarían la compra de votos por parte de **José Guillermo 'Ñeñe' Hernández Aponte,** un ganadero cercano al narcotraficante Marcos Figueroa en favor de la campaña de Iván Duque a la Presidencia, por orden de Álvaro Uribe. La de Guillén sería la segunda revelación del año que vincularía al mandatario a un delito electoral, tomando en cuenta las declaraciones de Aída Merlano.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Pese a lo delicado de las revelaciones y las pruebas dadas a conocer por Guillén, solo hasta ahora se ha visto una respuesta institucional que comienza a abrir paso a las investigaciones en entes como la Fiscalía, Comisión de Acusaciones de la Cámara de Representantes y la Corte Suprema de Justicia. Hecho que, para académicos y estudiosos como Daniel Libreros, muestran la débil institucionalidad que existe en Colombia.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### **En contexto: Un breve resúmen del caso Ñeñe**

<!-- /wp:heading -->

<!-- wp:paragraph -->

El periodista Gonzalo Guillén [reveló](https://lanuevaprensa.com.co/component/k2/interceptaciones-al-narcotraficante-nene-hernandez-destapan-compra-de-votos-para-duque-por-orden-de-uribe) que en una investigación de la Fiscalía sobre el fallecido Ñeñe (murió en 2018) en la que se recaudaban pruebas sobre su presunta participación en el asesinato de Óscar Rodríguez Pomar, el ente investigador halló audios en los que se habla de una compra de votos en favor de Duque, por orden del senador Álvaro Uribe. Julián Martínez, también periodista, se encargó de develar que la persona con la que hablaba sobre este tema el 'Ñeñe' era María Claudia Daza, asesora personal de Uribe.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Mientras el uribismo ha intentado señalar que el 'Ñeñe' era solo un ganadero del Caribe, la justicia y los periodistas anteriormente mencionados han mostrado que tiene vínculos con el condenado narcotraficante Marcos Figueroa. El Ñeñe fue asesinado en Brasil durante un robo en mayo del año pasado, antes de ello, para junio de 2018 la Fiscalía ya había aplicado la extinción de dominio en su contra por presuntamente apoyar económicamente a Figueroa.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### **La degradación del sistema político, y el control de las mafias electorales**

<!-- /wp:heading -->

<!-- wp:paragraph -->

Para el profesor Daniel Libreros, "lo del 'Ñeñe' expresa una continuidad de un ciclo de reproducción política, de utilización del sistema electoral por parte de las mafias locales" que acuden a dineros del narcotráfico, y muestran el nivel de corrupción en dicho sistema. (Le puede interesar: ["Las polémicas decisiones del CNE, ¿es necesario reformarlo?"](https://archivo.contagioradio.com/decisiones-cne-reformarlo/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Además del sistema electoral, el profesor recordó que se sabe que la Fiscalía, a cargo de Néstor Humberto Martínez,["engavetó esa investigación",](https://twitter.com/JulianG40747584/status/1237534755060498432) pese a que eran conocidos los audios en los que se presumía un delito electoral. Y a esa situación se añade que el candidato Duque usó vehículos que estaban en proceso de extinción de dominio, al tiempo que Hernández fue trasladado en un avión de la Fuerza Pública cuando ya era buscado por la justicia.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/HELIODOPTERO/status/1237411360834752517","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/HELIODOPTERO/status/1237411360834752517

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:heading {"level":3} -->

### **Del 'Ñeñe' Hernández a Fernando Sanclemente**

<!-- /wp:heading -->

<!-- wp:paragraph -->

Como señaló Libreros, "el asunto no solo es el Ñeñe", porque otro hecho que llamó la atención en este principio de año fue la destrucción de un laboratorio para procesar cocaína hallado en la finca personal de Fernando Sanclemente, embajador de Colombia ante Uruguay. Hasta el momento aún se discute si se hará extinción de dominio al predio, esperando una respuesta de la justicia que explique por qué el Gobierno lo ha mantenido en el cargo.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por el contrario una persona que no salió favorecida por el uribismo fue 'La Caya' Daza, persona que hablaba con Hernández sobre la compra de votos y que fue apartada de la Unidad de Trabajo Legislativo (UTL) del senador Uribe. Para Libreros, la salida de ella de su cargo obedece a una maniobra acostumbrada por Uribe: el enroque, que ha usado con su abogado Diego Cadena, entre otros.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### **Si la institucionalidad no resuelve las cosas, ¿entonces quién?**

<!-- /wp:heading -->

<!-- wp:paragraph -->

Con Uribe en su curul, Duque en la presidencia, Sanclemente en la embajada, [Aída Merlano](https://archivo.contagioradio.com/acusaciones-de-aida-merlano-deben-llegar-a-la-fiscalia/) en Venezuela y la hija de La 'Caya' Daza en un consulado de Estados Unidos (y ella misma fuera de Colombia) el país parece seguir su curso normal, solo distorsionada por el COVID-19. En sintonía con esta idea, Libreros dijo que no hay esperanza de que se vaya a resolver nada porque el fiscal no investigará al que es su amigo de muchos años.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

También es conocido que en el Congreso se llama informalmente a la Comisión de Absoluciones al ente encargado de investigar a altos funcionarios porque no da resultados, de igual forma se denunció que no contaba con presupuesto para su operación. Por otra parte, el periodista Gonzalo Guillén ha repetido que la Corte Suprema de Justicia rehuye a un juicio de Álvaro Uribe, por temor a las consecuencias negativas de un fallo contra el hoy senador.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Con ese panorama, el analista político invitó a cuestionar las instituciones para encontrar salidas distintas a las que se suelen ofrecer. "Tenemos que evaluar lo que está pasando con la movilización y esperar que la calle se ponga contra esa institucionalidad corrupta y las formas de reproducción política que ahora nos amenazan", declaró. (Le puede interesar: ["Ejército realizó "chuzadas" a congresistas, jueces, periodistas y defensores de DD.HH."](https://archivo.contagioradio.com/ejercito-realizo-chuzadas-a-congresistas-jueces-periodistas-y-defensores-de-dd-hh/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En ese sentido, sostuvo que es momento de organizar mejor y masivamente la potencia barrial y de resistencia para enfrentar la corrupción del sistema, así como los anuncios de los pilotos de fracking y las reformas pensional, laboral y tributaria que se esperan presentar. Para dicha organización, manifestó que Chile es un buen referente, que "nos está marcando la necesidad de dar una salida más fuerte a la crisis del neoliberalismo".

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### **Medios de Comunicación: El contrapoder o el cuarto poder**

<!-- /wp:heading -->

<!-- wp:paragraph -->

Para finalizar, Libreros aseguró que si las instituciones son una vergüenza también lo son "los grandes medios de comunicación que han decidido esconder esta tragedia social", puesto que desde su perspectiva, su función debería orientarse en discutir lo que está pasando y poner el problema institucional en la plaza pública.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Desde otra perspectiva, también indicó que por eso los medios de comunicación alternativos tienen que jugar un gran papel en este momento, como ya lo hicieron los periodistas Gonzalo Guillén y[Julián Martínez](https://archivo.contagioradio.com/alvaro-uribe-estigmatiza-a-periodismo-de-noticias-uno/), así como desde La Nueva Prensa, que por su labor han sido víctimas de seguimientos y estigmatización.   

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78980} /-->
