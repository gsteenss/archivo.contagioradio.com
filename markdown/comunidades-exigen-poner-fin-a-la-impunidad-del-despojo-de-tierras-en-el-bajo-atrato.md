Title: Comunidades exigen poner fin a la impunidad del despojo de tierras en el Bajo Atrato
Date: 2019-12-02 18:19
Author: CtgAdm
Category: DDHH, Nacional
Tags: Bajo Atrato, comision de la verdad, Empresas Bananeras, uraba
Slug: comunidades-exigen-poner-fin-a-la-impunidad-del-despojo-de-tierras-en-el-bajo-atrato
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/Despojo-de-tierras.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/Despojo-de-tierras-copia.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: @CorpoJuridicaLi] 

En Apartadó, Antioquia, en medio de la entrega a la Comisión para el Esclarecimiento de la Verdad de los informes  **'La mejor esquina de América: Territorios Despojados' y** '**Conflicto Armado y Violencia Sociopolítica en la implementación y desarrollo de un modelo de acumuluación por desposesión en la región de Urabá',** las víctimas y habitantes de las regiones afectadas en Urabá y Bajo Atrato expresaron la importancia de identificar a los terceros responsables de los sucesos que han vulnerado sus derechos desde hace 30 años.

Los informes realizados por la **Comisión Intereclesial de Justicia y Paz, la Corporación Jurídica Libertad, la Fundación Forjando Futuros y el Instituto Popular de Capacitación** identifican el despojo de 166.000 hectáreas en 11 municipios del Urabá entre 1995 y 1997, que obedecen al accionar de diversos actores armados para la apropiación y redistribución de la tierra, afectando a comunidades étnicoterritoriales y campesinas de esta zona de Colombia.

### "El despojo de tierras en el Bajo Atrato no fue improvisado, fue planificado" 

Martha Peña, del Instituto Popular de Capacitación señaló que este informe sobre la región de Urabá se fundamenta en el acompañamiento realizado por las organizaciones que han escuchado el testimonio de las víctimas, proceso que ha permitido conocer de primera mano las afectaciones que han sufrido las comunidades. [(Le puede interesar: Informes sobre despojo en el Urabá y Bajo Atrato llegarán a la Comisión de la Verdad)](https://archivo.contagioradio.com/informes-sobre-despojo-en-el-uraba-y-bajo-atrato-llegaran-a-la-comision-de-la-verdad/)

Los informes han permitido establecer que entre 1996 y 1997  se dio el desplazamiento forzoso de casi medio millón de personas, cerca de 1.200 casos de desaparición y más de 120 víctimas de masacres, hechos que obligaron a las poblaciones a abandonar su territorio al que empezaron a llegar compañías agroindustriales, de infraestructura, palmeras y bananeras a materializar ese despojo como una alianza entre empresarios y grupos paramilitares.

"Cuando dejaron a Urabá sin liderazgos y silenciados, organizaron otro plan para desocupar el territorio", explica Peña quien agrega que estos hechos continúan en impunidad debido a la permisividad del Estado. "No estamos hablando de un tema que no sea conocido por la opinión pública pero sí es un tema del que se habla de una negación y de impunidad, expresó.

### **"Desde el 97  necesitamos esclarecer esa verdad, pero van 22 años y no tenemos respuesta"**

La entrega del informe, que se da a tan solo unos días después que nueve líderes reclamantes de tierras de Turbo, Antioquia fueran capturados por la Policía en un caso que podría ser obedecer a un falso positivo judicial, fue el escenario que permitió hacer visible esta situación. [(Lea también: En Guacamayas se estaría configurando falso positivo judicial contra reclamantes de tierras)](https://archivo.contagioradio.com/en-guacamayas-se-estaria-configurando-falso-positivo-judicial-contra-reclamantes-de-tierras/)

**Ayineth Pérez de la organización Tierra y Paz** se dirigió a la Comisión de la Verdad para que se haga justicia con los afectados, "son campesinos que tienen la sentencia en sus manos y ni siquiera así se están respetando sus derechos",  a lo que Ana del Carmen Martínez, lideresa de Cavida, agregó que quiere "saber quiénes son los responsables del despojo y de nuestro desplazamiento forzado porque sabemos que detrás hay intereses de terceros". [(Le puede interesar: Denuncian captura de ocho líderes reclamantes de tierras en Guacamayas en el Urabá) ](https://archivo.contagioradio.com/denuncian-captura-de-ocho-lideres-reclamantes-de-tierras-en-guacamayas-en-el-uraba/)

La comisionada de la verdad, Ángela Salazar manifestó que los hechos de los que han sido víctimas campesinos del Urabá y el Bajo Atrato, con la entrega de los informes, **"hacen visible que ha sido un ejercicio sistemático la usurpación de tierras"**. Por su parte, el comisionado de la verdad, Alejandro Valencia  agregó que esta compilación será un tema central y capital frente al trabajo de la Comisión de la Verdad.

Por su parte, el delegado de la Sala de Reconocimiento de Verdad de la Jurisdicción Especial para la Paz expreso que la entidad avanza en el caso 004, con la emisión de las resoluciones de conclusiones que  que identificarían la responsabilidad de particulares, excombatientes y agentes del Estado como miembros de la Fuerza Pública en los delitos cometidos en los últimos 30 años en la región del Urabá.

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
