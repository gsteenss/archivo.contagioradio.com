Title: La paz y el periodismo, los retos para una nueva realidad
Date: 2017-02-09 17:05
Category: Entrevistas, Nacional
Tags: acuerdos de paz, periodismo
Slug: la-paz-y-el-periodismo-los-retos-para-una-nueva-realidad
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/02/periodistas1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Prensa Rural ] 

###### [9 Feb 2017]

Javier Darío Restrepo, maestro del periodismo en Colombia, ha dicho que el periodismo debe asumir el reto de producir **“noticias para la inteligencia”** y de esta forma avanzar en hacer un periodismo que sea de propuesta, **“debe haber un párrafo que diga por qué hubo una desgracia y el otro que comente que propuestas** hay porque de esta forma florece la esperanza”. Fabiola León, integrante de Reporteros sin Fronteras agrega a ellos, la inclusión, la democratización y la educación.

### **La relación entre las Empresas y los medios de información** 

Para Fabiola Leon un problema grave en Colombia, es que los oligopolios empresariales sean los que a su vez tienen el monopolio de la información **“mientras el interés económico este mediando se tendrán dificultades”**, debido a que esta situación no permite un  respeto al derecho a la información, ni un equilibrio democrático que permita que todos los medios estén en las mismas condiciones para existir,

### **Polarización** 

Esta división entre medios de información, según Fabiola León, **se ha hecho más evidente desde el inicio del proceso de paz con las FARC-EP,** demostrando desde la información que producen que no existe un interés por cubrir todas las fuentes de una noticia, sino que por el contrario se dedican a quedarse con ciertas fuentes.  **“Hay pocos asomos hacia una inclusión**, en cambio sí es evidente posturas que solo mostraran un lado y que no les interesa hablar con los otros” afirmó.

### **Educación**

Las Facultades de Comunicación Social y Periodismo del país, también tienen retos que asumir, de acuerdo con Fabiola León, el principal es dar un contexto desde las humanidades como la historia y democracia, para que los futuros periodistas **aborden las realidades con argumentos** y añadió que en la actualidad, los jóvenes periodistas y las dinámicas de la tecnología están generando una apatía por ir hasta las regiones y comunidades para cubrir los hechos, motivo por el cual debe generarse otras relaciones con las nuevas tecnologías que no dejen de lado el salir al territorio.

### **Otra informacióm , otros medios** 

En Colombia han venido surgiendo medios de información que no están ligados a grandes empresas y que se ha denominado como alternativos, comunitarios o populares, frente a ellos, Fabiola León considera que la crisis de representación democrática y la falta de credibilidad en los grandes medios ha **generado que la ciudadanía se volqué hacia otros escenarios en donde pueda encontrar otras formas de informarse.**

El panorama mundial no es muy diferente al de Colombia, Fabiola señala que existe una **crisis de la democracia representativa que se evidencia en la incredulidad hacia los medios** de información  “no solo son los medios los que entran a estar en una problemática de credibilidad sino que son las instituciones políticas”. Le puede interesar: ["Javier Darío Restrepo: El periodista y los medios de información en tiempos de paz"](https://archivo.contagioradio.com/javier-dario-restrepo-el-periodista-y-los-medios-en-tiempos-de-paz/)

<iframe id="audio_16927139" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_16927139_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
