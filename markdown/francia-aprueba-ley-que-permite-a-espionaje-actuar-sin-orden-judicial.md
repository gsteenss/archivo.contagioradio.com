Title: Francia aprueba ley que permite a fuerzas de seguridad espiar sin orden judicial
Date: 2015-05-05 17:39
Author: CtgAdm
Category: DDHH, El mundo
Tags: Espías franceses actuarán sin orden judicial, francia, Francia aprueba ley espionaje, Francia aprueba polemica ley espionaje, Francia regula servicios de espionaje, Le Monde
Slug: francia-aprueba-ley-que-permite-a-espionaje-actuar-sin-orden-judicial
Status: published

###### Foto:24h.cl 

El **parlamento francés ha aprobado** hoy la polémica ley que permite a los **servicios secretos espiar**, rastrear, y utilizar medios tecnológicos como cajas negras de seguimiento contra sospechosos **sin orden judicial.**

La normativa ha sido aprobada por **438 votos a favor y 86 en contra**, estos últimos de partidos de izquierda relacionados con movimientos sociales contrarios a la ley por considerarla un **"liberticidio**".

La **gran mayoría de partidos aprueba la ley** que ha sido realizada con el objetivo de poder combatir el terrorismo islámico. Por el contrario los movimientos sociales y asociaciones de carácter civil la han criticado por poner en riesgo la privacidad y el propio ejercicio de las libertades civiles.

La ley supone **legalizar prácticas que los servicios de espionaje ya estaban realizando** sin ningún tipo de cobertura legal. Además la ley permitirá a los servicios secretos espiar solo con la autorización del ministro de defensa y del interior, así como en casos de emergencia actuar sin autorización.

Con la propuesta del ministro del interior para **la actuación de los servicios de inteligencia será necesaria sola la autorización del ministro del interior** que podrá contar con la opinión de una Comisión Nacional de Control de Técnicas de Información compuesta por 4 parlamentarios, dos magistrados, dos miembros del consejo de estado y un experto en telecomunicaciones.

El anuncio coincidió con la **publicación en el periódico “Le Monde”** sobre un artículo que habla de que los servicios de inteligencia actualmente tienen una base de datos ilegal donde acumulan todo tipo de información privada** **sobre cientos de ciudadanos franceses.

La ley va enmarcada en las **políticas contra el yihadismo de la UE**, que justificándose en las amenazas terroristas, están poniendo en funcionamiento mecanismos legales que atentan contra la libertad de los ciudadanos y que según la ONU y numerosas organizaciones sociales de DDHH constituyen una clara violación de los DDHH.
