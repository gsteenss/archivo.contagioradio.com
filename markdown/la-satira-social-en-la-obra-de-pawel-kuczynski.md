Title: La sátira social en la obra de Pawel Kuczynski
Date: 2016-01-22 11:05
Category: Cultura, Galerias
Tags: Crítica social en el arte, Ilustradores Polacos, Pawel Kuczynki
Slug: la-satira-social-en-la-obra-de-pawel-kuczynski
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/01/pawel-kuczynski-the-strike.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

######  

###### [22 Ene 2016.] 

Miles de personas expresan a través del arte su forma de ver la vida, uno de ellos es el ilustrador**[Pawel Kuczynki](http://pawelkuczynski.com/),** su obra gráfica, popularizada en redes sociales,** **plasma de una manera satírica situaciones que hacen parte del cotidiano de nuestra sociedad.

**Kuczynski**, se graduó de bellas artes en la universidad de Poznan, especializado en ilustración, carrera que le ha permitido exponer su trabajo en diferentes lugares del mundo. En 2004 por recomendación de amigos se inscribió al primer concurso de ilustración, ganando el primer lugar, reconocimiento que le motivaria para seguir por el camino de la crítica social desde la plástica.

[![Crítica-social-36](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/01/Crítica-social-36.jpg){.aligncenter .size-full .wp-image-19518 width="500" height="354"}](https://archivo.contagioradio.com/la-satira-social-en-la-obra-de-pawel-kuczynski/critica-social-36/)

"La realidad es tan retorcida, loca y absurda, que es bastante difícil competir con ella... y esta realidad me da inspiración" dice el artista en una entrevista concedida a la revista [Flustermagazine](http://flustermagazine.com/2012/03/10/showcase-pawel-kuczynski/), en la que  manifiesta además que sus dibujos nacen de las observaciones que hace a la sociedad y que algunos surgen de sus sus viajes por el mundo.

Acuarelas, ecolines y  lápices de color son los insumos que utiliza el polaco para dar vida a sus obras que se encuentran por centenas en la red. Los personajes recurrentes son en su mayoría ratones y gatos, y los temas ilustrados son  la sátira económica, ambiental, social y política, "me gusta dibujar sobre estos temas porque son atemporales como el arte".

[![Crítica-social-31](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/01/Crítica-social-31.jpg){.aligncenter .wp-image-19519 width="500" height="375"}](https://archivo.contagioradio.com/la-satira-social-en-la-obra-de-pawel-kuczynski/critica-social-31/)

Desde el inicio de su carrera hasta el día de hoy el artista ha sido galardonado con 102 premios y distinciones. En 2005 recibió el premio "Eric" de la Asociación Polaca de Caricaturistas, por su récord de premios en concursos internacionales.

**Con ustedes la critica a nuestra sociedad por Pawel Kuczynki**

\

   
 
