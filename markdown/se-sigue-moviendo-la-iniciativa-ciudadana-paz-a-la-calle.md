Title: Se sigue moviendo la iniciativa ciudadana Paz a La Calle
Date: 2016-10-11 15:08
Category: Nacional, Paz
Tags: acuerdos de paz ya, Movilización para la paz, Paz a la Calle
Slug: se-sigue-moviendo-la-iniciativa-ciudadana-paz-a-la-calle
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/10/pazcalle.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Paz a La Calle] 

###### [11 Oct de 2016]

El pasado lunes se llevó a cabo, en el monumento del Almirante Padilla en el Park Way, la tercera Asamblea Ciudadana de Paz a La Calle, allí se ratificaron objetivos y surgieron otras acciones colectivas para continuar la **presión ciudadana que exige la implementación urgente de los Acuerdos de Paz.** Carolina Moreno, una de las integrantes manifestó que fueron centrales cuatro puntos durante la Asamblea.

1.  Legitimamos la mesa de La Habana como el espacio de diálogo y solución política al conflicto armado, no admitimos la solución militar al conflicto.
2.  Exigimos que el cese bilateral al fuego sea definitivo.
3.  Reivindicamos los 4 años de trabajo en los diálogos, creemos que no pueden botarse a la basura, y exigimos que cuanto antes inicie la discusión sobre la implementación. Respetamos si las delegaciones consideran que se debe añadir otros puntos, pero no aceptamos que se den pasos atrás.
4.  Denunciamos que la campaña por el NO estuvo enmarcada en mentiras, y decimos que la verdad no se negocia.

Lea También: [Paz a la Calle se sigue Movilizando en toda Colombia](http://Reciba%20toda%20la%20información%20de%20Contagio%20Radio%20en%20su%20correo%20bit.ly/1nvAO4u%20o%20escúchela%20de%20lunes%20a%20viernes%20de%208%20a%2010%20de%20la%20mañana%20en%20Otra%20Mirada%20por%20Contagio%20Radio%20bit.ly/1ICYhVU)

También mencionó que “lo mas importante es que paz a la calle no es protagonista, no es representante, es un llamado a que la paz se vaya hacia las calles, **que la paz no se quede entre Uribe y santos sino que vuelva a los y las ciudadanas**”. Surge como un espacio de discusión y construcción que tiene como objetivo visibilizar iniciativas, promover la movilización y actividades desde lo artístico y educativo.

Si bien es importante escuchar a los sectores que se opusieron a los Acuerdos, “los acuerdos son integrales y **en la medida en que se quite un punto queda cojo el acuerdo y por eso hay q defender la integralidad**” afirma Moreno. En la Asamblea se propuso acompañar las movilizaciones de las comunidades que el próximo miércoles y viernes saldrán a marchar en las calles de la capital y participar en el Plantón de este martes en horas de la tarde frente a RCN para exigir a este y otros medios respeto y transparencia en la información que brindan.

Señaló que desde este espacio ciudadano se quiere promover la realización de movilizaciones cada semana y extender estas asambleas a otras localidades. El próximo jueves en Kennedy se realizará la cuarta Asamblea y además informó que **el 19 de Octubre se realizará la movilización Paz a La Calle que será simultánea en otras ciudades del país.** La consigna será “\#297 razones para defender la paz”.

Lea También: [Paz a la Calle iniciativa de los jóvenes por los Acuerdos de Paz](https://archivo.contagioradio.com/no-somos-una-patria-boba-y-vamos-a-demostrarlo-pazalacalle/)

Frente al inicio de la fase pública de negociaciones con el ELN, Moreno resalta que desde la iniciativa lo saludan de manera positiva y agrega que se espera “no cesen los ánimos, que no se dilate el proceso, pues hay quienes dicen que la re-negociación podría durar más de un año, pero es peligroso para toda la población”.

<iframe id="audio_13276906" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_13276906_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en su correo [[bit.ly/1nvAO4u]
