Title: "Tapen, tapen" La estrategia de encubrimiento de responsables de 'Falsos positivos'
Date: 2015-08-20 11:20
Category: Judicial, Nacional
Tags: Ejecuciones Extrajudiciales, falsos posi, falsos positivos, Fuerzas militares
Slug: tapen-tapen-continua-la-impunidad-en-caso-de-falsos-positivos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/08/el-gobierno-promete-blindar-procesos-judiciales-de-falsos-positivos.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: www.uniderecho.com 

[<iframe src="http://www.ivoox.com/player_ek_7218202_2_1.html?data=mJeempeUdo6ZmKiakpWJd6KkkZKSmaiRdI6ZmKiakpKJe6ShkZKSmaiRaZa4hpefu9SPscbohqigh6aopYzg0NiYz8bSqdSf2pDi1dnJqMbnjNHc1ZDRpdXVw8bbh5eWcYarpJKw0dPYpcjd0JC%2Fw8nNs4yhhpywj5k%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>]

##### ["Yo metía los manes y ustedes los mataban"] 

<iframe src="http://www.ivoox.com/player_ek_7218278_2_1.html?data=mJeempebfI6ZmKiakpWJd6KkkZKSmaiRdI6ZmKiakpKJe6ShkZKSmaiRaZOmotbih6iXaaK4jNncxcaPqdSf0trSjdjJb9LpxsnSjcjFsM3VxdSYz8bWrcTVhpefj4qbh4630NPhw8zNs4zGwsnW0ZCRaZi3jpk%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### ["Aquí toca es que se quede callado marica"] 

##### <iframe src="http://www.ivoox.com/player_ek_7218368_2_1.html?data=mJeempiafI6ZmKiakpuJd6KlkZKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRaZOmr9SY0NTRptPZjMaYxdTWs8%2FZzcrgh5enb8%2FjjNPcz8fWqYzVjNPOxs7JaZOmjoqkpZKns8%2FowszW0ZC2pcXd0JCah5yncZU%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>  
["No nombre a coroneles, no nombre a nadie"] 

###### [19 Ago 2015]

Se han revelado audios en los que es explícita la intención de los militares detenidos por casos de ejecuciones extrajudiciales, de dilatar las investigaciones que se adelantan en su contra, así como encubrir la responsabilidad de altos mandos de las Fuerzas Militares, también implicados.

Los dos miembros del ejército que sostienen las conversaciones se encuentran recluidos, el primero José Torres desde hace cinco años, por el asesinato a un campesino que hizo pasar por integrante de las FARC, cuando pertenecía a la Brigada Cuarta de Medellín, y el segundo un suboficial no identificado, sindicado de 12 homicidios en persona protegida. Sobre Torres pesan además otros procesos por 32 muertos en unidades en las que estuvo.

En las conversaciones es evidente la responsabilidad en los actos de los que estos militares son acusados: “Yo vendía los manes y ustedes los mataban”, además de la pretensión de burlar los procesos judiciales: “Usted quedese callado…nosotros cuadramos la declaración”, para culpar a la menor cantidad posible de soldados “La solución es quedese callado” “No nombre a coroneles no nombre a nada”.

La situación resulta indignante sí se tiene en cuenta que desde 2007 hasta la fecha se han capturado cerca de 3.000 soldados implicados, de los que solo 815 han sido condenados, la mayoría suboficiales y soldados, de altos mandos sólo 10 coroneles, 5 condenados y los restantes hasta ahora capturados.
