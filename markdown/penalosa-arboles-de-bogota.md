Title: La fijación de Peñalosa con los árboles de Bogotá
Date: 2019-01-29 15:36
Author: AdminContagio
Category: Voces de la Tierra
Tags: Árboles, Peñalosa, tala
Slug: penalosa-arboles-de-bogota
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/01/DxtOQ2XWsAE12pe-770x400.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Las dos Orillas 

###### 24 Ene 2019 

El caso del parque Japón donde la administración distrital de Bogotá inició una tala pese a la oposición de los vecinos del sector, no es el primero en el que **la alcaldía de Peñalosa es cuestionada por las disposiciones que toma frente a los árboles en su proyecto de ciudad**, sumándose a lo que algunos ambientalistas consideran como un arboricidio urbano.

Mientras el Distrito se defiende y asegura que ha sembrado **200 mil árboles**, los grupos ambientalistas aseguran que la administración Peñalosa planea talar **34 mil árboles en la ciudad**, y que muchas de las especies que buscan plantar son seleccionadas bajo criterios inadecuados.

Para abordar el tema nos acompañaron en **Voces de la tierra,** **Liliana Castañeda**, Politóloga, integrante de Asovecinos Teusaquillo y asesora en temas ambientales del concejal Manuel Sarmiento; **Julián Triana** abogado y activista en el barrio Modelia; y **Sebastián Rojas**, Ingeniero, Director del periódico El Chapín Prensa y veedor ciudadano de la localidad de Chapinero.

<iframe id="audio_31938290" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_31938290_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Encuentre más información ambiental en[ Voces de la tierra ](https://archivo.contagioradio.com/programas/onda-animalista/)y escúchelo los jueves a partir de las 11 a.m. en [Contagio Radio](https://archivo.contagioradio.com/alaire.html) 
