Title: Preocupante: Se abren las puertas de nuevo para la minería en Jericó
Date: 2019-09-17 18:16
Author: CtgAdm
Category: Ambiente, Nacional
Tags: Ambiente, Anglo Gold Ashanti, Antioquia, Explotación, Jericó, Mineria
Slug: nuevo-mineria-en-jerico
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/09/Minería-de-Anglo-Gold-Ashanti.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

La decisión fue tomada por la Sala Tercera del Tribunal Administrativo de Antioquia, quien suspendió las acciones del acuerdo municipal 010 de 2018, que prohibía la minería metálica al igual que la exploración, construcción y explotación en mediana o gran escala minera en Jericó.

(Le puede interesar:[Nueva derrota en Jericó para AngloGold Ashanti](https://archivo.contagioradio.com/nueva-derrota-en-jerico-para-anglogold-ashanti/))

En el 2017 el Concejo de Jericó presentó un acuerdo donde suspendían la minera por generar desvíos de recursos hídricos que ocasionaban la disminución en el agua que llegaba a la comunidad. Este cese en la explotación se dio un año después de la presentación del primero acuerdo, y luego de lograr la aprobación del segundo en el 2018, se aceptó un acuerdo similar que prohibía la explotación minera en el municipio de Urrao; ambos acuerdos fueron suspendidos por orden Tribunal de Antioquia.

(Le puede interesar: [Debe parar Minería de AngloGold Ashanti en Jericó](https://archivo.contagioradio.com/continuan-actividades-de-mineria-en-jerico/))

Según Fernando Jaramillo Vocero de la Mesa Ambiental de Jericó, las intenciones detrás de esta reapertura las tiene la multinacional Anglo Gold Ashanti, que venía realizando explotaciones, “esta empresa no respeto en ningún momento este acuerdo y siguió explotando; solo dos veces y por presencia de los campesinos que defienden el territorio y el acompañamiento del alcalde se logró frenar por 10 días la explotación”.

La comunidad  de este municipio había manifestado su rechazo en contra de la presencia de Anglo Gold desde el 2011; en ese sentido, Jaramillo afirmó que “es preocupante saber que mientras nosotros damos 100 pasos para lograr un acuerdo que frene estas acciones, el gobierno con uno vuelve a abrir las puertas de la minería en Jericó”; y agregó que para los habitantes, esta reversa desde el Tribunal significa que en el territorio no son válidos los acuerdos municipales, la palabra del alcalde es pasada por alto cuando de acciones contra la petrolera se trata, y la consulta popular que actuaba como el único recurso regulador ahora es suspendida.

Jaramillo aseguró que la comunidad seguirá buscando recursos jurídicos y de movilización social  para que no se explote minería, por este motivo, **el jueves 24 de octubre** se dará una audiencia  en el Tribunal de Antioquia a las 2:00 de la tarde para decidir la nulidad de este acuerdo.

(Le puede interesar:[Jericó, Antioquia le dice ¡No! a la minería por segunda ocasión](https://archivo.contagioradio.com/jerico-antioquia-le-dice-no-a-la-mineria-por-segunda-ocasion/))

*“Estamos presentando un recurso de súplica para que se suspenda este auto, que nos niega el derecho para que se respeten los territorios, e igual seguiremos insistiendo en otros recursos jurídicos para que se respete la voluntad de los Jericuanos” - Jaramillo*  
<iframe id="audio_41721303" frameborder="0" allowfullscreen scrolling="no" height="200" style="border:1px solid #EEE; box-sizing:border-box; width:100%;" src="https://co.ivoox.com/es/player_ej_41721303_4_1.html?c1=ff6600"></iframe>  
 
