Title: Fe caminante en el paro nacional de Colombia
Date: 2019-12-05 12:46
Author: CtgAdm
Category: Opinion
Tags: #ParoyFe, dios, Fe, Iglesias, Paro Nacional
Slug: fe-caminante-en-el-paro-nacional-de-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/IMG_2297-2.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Jhon Martínez y Stella Colmenares con su hijo Juan, de la Iglesia Diálogos y Fe (M. Estupiñán)] 

**Autor: Miguel Estupiñán**

Tarde de domingo en Bogotá. 1° de diciembre.

A sus seis años, Juan hubiese preferido estar en casa, jugando Uno con sus papás. Pero está en la calle, entre una multitud que arenga.

Hace más de una semana que la rutina se trastocó. No solo en su familia, ni en su barrio, sino en toda Colombia, donde no hay garantía para que niños de su edad vivan en paz. Una entre muchas razones que durante estos días empujan de día y de noche a miles de personas, como sus papás, a salir a manifestarse de manera pública en favor de un cambio. Jornadas de protesta que se multiplicaron a partir del 21 de noviembre y que marcan un antes y un después en la historia reciente del país. En Colombia no se veía algo parecido desde el gran paro nacional de 1977.

Hay entre los marchantes quienes le exigen al Gobierno de Iván Duque que implemente los acuerdos a los cuales llegó el Estado colombiano con la antigua guerrilla de las FARC en 2016, para avanzar en una solución política del conflicto armado. Uno entre tantos problemas históricos del país, no del todo conjurados. La lista incluye inequidad y desempleo. Pero se suma a ello la incertidumbre ante posibles medidas económicas en camino que podrían hacer peor las cosas para la mayoría. La presidencia lo niega. Pero entre los marchantes hay quienes lo anticipan, quienes denuncian la inminencia del “paquetazo”. (Le puede interesar:[El tal paquetazo sí existe: Oposición](https://archivo.contagioradio.com/el-tal-paquetazo-si-existe-oposicion/))

Jhon Martínez y Stella Colmenares, los papás de Juan, son los pastores de la iglesia pentecostal Diálogos y Fe. Además de creer en la manifestación de los dones espirituales, la suya es una fe caminante que aborda la identidad religiosa desde una perspectiva de construcción de ciudadanía.

“Queremos que nuestras oraciones y plegarias vayan de la mano con acciones a nivel material”, dice Jhon. Por eso decidieron acompañar a los manifestantes reunidos en su barrio, en una de las principales avenidas de la localidad en que, semanalmente, la pareja y su hijo se congregan con otros cristianos para hablar de Dios, estudiar la Biblia y discernir la acción.

Mientras responden unas cuantas preguntas, se suman a la marcha decenas de indígenas. Entre banderas rojiverdes, los nasa del Norte del Cauca exigen que se detenga el baño de sangre en su territorio ancestral. Una zona del país bajo la presión de narcotraficantes, megaproyectos y formas de minería que atentan contra la autodeterminación comunitaria y el cuidado de la naturaleza. Escenario, como otras zonas del país, del exterminio impune de líderes sociales. (Le puede interesar:[Agresiones contra líderes sociales incrementaron un 49% en el primer semestre del 2019](https://archivo.contagioradio.com/agresiones-contra-lideres-sociales-incrementaron-un-49-en-el-primer-semestre-del-2019/))

—¿Qué razones de fe tienen ustedes para unirse a esta manifestación —les pregunto.

—Principalmente, la vida —contesta Jhon—. Hay gente que está siendo asesinada sistemáticamente. La fe nos obliga a pronunciarnos frente al homicidio.

—La Palabra de Dios dice que tenemos que levantar la voz por los oprimidos y por los excluidos —agrega Stella—. Eso estamos haciendo: saliendo a acompañar a la gente que está pidiendo justicia e igualdad, para que no se les vulneren más sus derechos a la vida, a la educación, a la paz y a la justicia.

—¿Cómo se articula la misión de su iglesia con ejercicios como este?

—Hemos procurado responder al nombre de la Iglesia: diálogo —contesta Jhon—. Cuando hacemos preguntas o reflexionamos acerca de la Biblia, hay un sentir de mantener esa actitud dialogante con el que piensa diferente. El señor Jesús nos mandó a ser testigos, a compartir la experiencia.

—En la iglesia, a las personas no se les impone que tengan un enfoque o un pensamiento como el de nosotros —agrega Stella—. Damos libertad. A las personas que piensan distintos las respetamos. Igual hacen parte de la comunidad.

### Diálogos de improbables {#diálogos-de-improbables dir="ltr"}

Cada tanto, Diálogos y Fe convoca a creyentes y no creyentes a conversaciones sobre fe e incidencia pública.

Con la metodología del profesor John Paul Lederach, de la Universidad de Notre Dame, meses atrás la invitación fue a dialogar sobre el papel de los cristianos en la implementación del acuerdo de paz con las FARC, sobre las condiciones de diálogo con la guerrilla del ELN y sobre el posicionamiento del Gobierno actual. Participaron en el panel el ex-guerrillero y senador de la República por el ahora partido político FARC Carlos Losada y la menonita Jenny Neme, de DIPAZ, una articulación de cristianos defensores de derechos humanos con sentido ecuménico.

Neme ha manifestado en varios escenarios que se deben visibilizar los motivos de fe que llevan al compromiso social a muchos actores religiosos, víctimas también, en muchos casos, de la violencia política en Colombia. En esa ocasión, particularmente, llamó la atención sobre las falencias en la implementación de lo acordado entre el Estado y las FARC, de las que DIPAZ ha sido testigo. Por parte del actual Gobierno, afirmó, ha faltado acompañamiento a procesos productivos de los excombatientes en su tránsito a la vida civil. Y ha habido ausencia de garantías para la legalización de la posesión de tierra en favor de los mismos. Situación que le impide acceso a proyectos de vivienda a las personas que dejaron las armas. Losada planteó que el fin de la guerra en Colombia depende de un tipo de participación política que propicie soluciones estructurales a los problemas sociales de fondo.

En parte, a ello están orientadas las demandas sociales en las marchas multitudinarias que suman ya más de una semana en distintas ciudades del país, con “cacerolazos” en los que no ha faltado la presencia de muchos cristianos. Pero a través de las redes sociales hay quienes difunden la idea de que dichos actos son cosa del diablo; una expresión de la influencia del ex-presidente Juan Manuel Santos o del senador Gustavo Petro, supuestos representantes del Foro de São Paulo.

―¿Qué lectura hacen ustedes de la actitud adversa de algunos sectores religiosos hacia las movilizaciones de estos días?

―Los sectores religiosos han sido muy manipulados ―dice Jhon―. Se han dejado instalar algunas interpretaciones, lecturas y noticias que se multiplican visceralmente. Una conciencia influida por algo que no es verdad ni tiene fundamento. Lamentablemente, la mayoría de personas no evalúan la información.

―Como creyentes, queremos practicar el evangelio de Jesús ―agrega Stella―; decirles a los sectores religiosos que salir a manifestarse no es una ideología ni es partidista. Salir a manifestarse, estar del lado de los pobres, de los excluidos, de los que necesitan justicia, es un mandato del evangelio. En la iglesia hoy estuvimos en un tiempo de ayuno.También estudiamos el texto de Isaías 51, que dice que el verdadero ayuno no solo es dejar de comer, apartarnos y orar, sino también va de la mano con una actitud práctica, con miras a la realidad en que vivimos. Debemos manifestarnos, buscar la justicia y desnudar esos poderes en contra de los principios de la vida, del perdón y la reconciliación. Poderes que a veces están en el sistema político, en el sistema religioso, en el sistema económico y en el sistema cultural. Y que han sabido manipular muy bien. El pueblo evangélico, cuando, a veces, legitima cosas que van en contra de la Biblia.

Ha hecho carrera una generalización en América Latina según la cual la incidencia política de los evangélicos amenaza la democracia. Jhon y Stella, caminando en el paro nacional de Colombia junto a su hijo, son la prueba de que la actitud de los cristianos frente a la construcción de ciudadanía es muy diversa. Y no necesariamente un problema.

 

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
