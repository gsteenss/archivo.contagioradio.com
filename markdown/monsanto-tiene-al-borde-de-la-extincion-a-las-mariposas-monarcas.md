Title: Monsanto tiene al borde de la extinción a las mariposas Monarcas   
Date: 2015-02-09 20:11
Author: CtgAdm
Category: Animales, El mundo
Tags: animales en vías de extinción, Estados Unidos, mariposas monarcas, Monsanto
Slug: monsanto-tiene-al-borde-de-la-extincion-a-las-mariposas-monarcas
Status: published

##### Foto: [info7.mx]

**La mariposa monarca, se encuentra al borde de la extinción debido al uso del herbicida Roundup de Monsanto,** que acaba con plantas de las que se alimenta esta especie. Además este herbicida, pone **en riesgo la vida de agricultores y jornaleros,** según un estudio de **Centro para la Seguridad Alimentaria**.

De acuerdo al informe publicado, la tecnología agraria que combina el uso del **glifosato**, la sustancia activa del herbicida, con el cultivo de plantas genéticamente modificadas ha diezmado la fuente principal de alimentos de esta especie de mariposas: el **algodoncillo y otras hierbas del género Asclepias.**

En el reporte del Centro para la Seguridad Alimentaria  se evidencia que en los últimos 20 años la población de la Monarca ha descendido excesivamente, **el herbicida ya eliminó un 99% de las Asclepias en los campos del Medio Oeste de Estados Unidos, el hábitat principal de estas mariposas.**

Por otro lado, según una investigación de la revista **Natural Society**, “los trabajadores expuestos a glifosato de soya sufren de daños genéticos en el ADN y muerte celular elevada. La adenina (A), citosina (C), guanina (G) y timina (T) son los componentes de ácido nucleico que componen el ADN y la biotecnología están dañando estas partes importantes de nuestra biología, que ninguna persona en su sano juicio volvería a experimentar”.

**“Dejar que la mariposa monarca desaparezca para permitirle a Monsanto vender su herbicida durante unos cuantos años más es simplemente vergonzoso”,** aseguró Andrew Kimbrel, uno de los investigadores.
