Title: Dora Lilia Gálvez, otra víctima de la violencia feminicida
Date: 2016-11-18 18:29
Category: Mujer, Paz
Tags: Día de no Violencia contra la mujer, feminicidio en Colombia, feminicidios
Slug: dora-liliana-galvez-otra-victima-de-la-violencia-feminicida
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/vivas.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: ToVoz.ar] 

###### [18 Nov 2016] 

En Buga Valle del Cauca, Dora Liliana Gálvez **fue víctima de un brutal ataque que en estos momentos la tiene en estado de coma.** Es alarmante que ni las autoridades ni los medios **dieron a conocer a tiempo este grave caso de intento de Feminicidio ocurrido el pasado 6 de Noviembre** y del que el país tuvo conocimiento hasta el día 17 de Noviembre, **gracias a las denuncias de familiares y personas cercanas.**

Frente a ello la **Mesa por el Derecho de las Mujeres a una Vida Libre de Violencias,** conformada gracias a la Ley 1257 de 2008, manifestó, a través de un comunicado público, su rechazo a las diversas violencias a las que fue sometida Dora Liliana Gálvez. **Exigen a las entidades correspondientes celeridad para el inicio de una investigación, acceso a la justicia y garantías para ella y su familia.**

El hecho de que no se diera a conocer a tiempo este grave caso, demuestra la intencionalidad de las instituciones y la sociedad en general, de **invisibilizar y naturalizar los casos de violencias contra las mujeres en Colombia y el mundo. **Le puede interesar: [Latinoamérica grita ¡Ni una menos!](https://archivo.contagioradio.com/latinoamerica-grita-ni-una-menos/)

### **¿Por qué siguen las violencias contra las mujeres en Colombia?** 

La Mesa llama la atención sobre “la persistencia e incremento de los niveles de violencias contra las mujeres en el país, el **incumplimiento de las principales disposiciones de la Ley 1257 de 2008, Ley de no violencias contra las mujeres y la Ley 1761 de 2015 de Feminicidio”.**

Denuncian que en Colombia hay una grave **negligencia frente a las garantías de las mujeres víctimas de violencias y eso a raíz de un “desconocimiento que sigue existiendo** frente a las disposiciones legales para la atención de casos de violencias de género y feminicidios”.

Distintas organizaciones de mujeres reunidas en la Mesa, hacen un llamado a la sociedad para que se exprese contra la violencia feminicida, rechace todos los tipos de violencia y denuncie públicamente cualquier caso de violencia. **“A las mujeres nos están asesinando y la respuesta de la sociedad no puede seguir siendo el silencio cómplice y la indiferencia”.**

Por último extienden la invitación a que todas y todos participen de la movilización nacional que lleva el nombre de **\#DéjameEnPaz** que será el próximo **25 de Noviembre, día internacional de la** **‘No violencia contra la mujer’**, a partir de la 1pm desde el Planetario Distrital hasta la Plaza de Bolívar.

<iframe id="audio_13821553" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_13821553_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
