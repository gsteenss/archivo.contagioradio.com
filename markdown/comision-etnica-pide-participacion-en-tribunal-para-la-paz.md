Title: Comisión Étnica pide participación en Tribunal para la Paz
Date: 2017-02-07 15:42
Category: Nacional, Paz
Tags: Comisión Étnica, Tribunal de Paz
Slug: comision-etnica-pide-participacion-en-tribunal-para-la-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/10/marcha-de-las-flores101316_94.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [7 Feb 2017] 

La Comisión Étnica para la Paz y la defensa de los Derechos Territoriales, **solicitó garantizar la elección de magistrados y magistradas afrodescendientes e indígenas**, en la Jurisdicción Especial para la Paz, con la finalidad de que el mecanismo de JEP refleje la diversidad de Colombia y el impacto del conflicto armado sobre el territorio.

La organización defensora de derechos humanos WOLA, que cumple el papel de acompañar y hacer seguimiento al proceso de paz en Colombia, respaldó esta solicitud y señaló que actos como estos **“dan cumplimiento a las garantías y salvaguardas, establecidas en el capítulo étnico del acuerdo de paz”**. Le puede interesar: ["Empresas serán investigadas por financiación de la guerra en el Tribunal Especial de Paz"](https://archivo.contagioradio.com/empresas-serian-investigadas-por-financiacion-de-la-guerra-en-el-tribunal-especial-de-paz/)

Además señala que tanto Afros como indígenas sean seleccionados para estos puestos significa que se **reconoce el impacto desproporcionado del conflicto armado sobre sus territorios y comunidades**, y el papel fundamental que han desempeñado en la construcción de la paz.

El proceso de selección del comité que escogerá a los magistrados inició su trabajo desde que se firmó el Acuerdo de Paz el pasado 24 de noviembre, **en este momento se encuentran en el proceso de selección**. A su vez estos **magistrados conformaran el Tribunal para la Paz**, que tendrá como labor, ser la última instancia decisoria, motivo por el cual ningún otro tribunal podrá revisar o re abrir los casos expuestos en el Tribunal de Paz. Le puede interesar: ["Tribunal Especial para la Paz tendrá 7 salas con 5 jueces cada una"](https://archivo.contagioradio.com/tribunal-especial-para-la-paz-tendra-7-salas-con-5-jueces-cada-una/)

El comité está conformado por el magistrado José Francisco Acuña, el presidente de la Corte Interamericana de Derechos Humanos, Diego García Sayán, María Camila Moreno directora del Centro Internacional de Justicia Transicional, un delegado por parte de la Corte Europea de Derechos Humanos y un delegado de la Comisión Permanente del Sistema Universitario del Estado.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)

<div class="ssba ssba-wrap">

</div>
