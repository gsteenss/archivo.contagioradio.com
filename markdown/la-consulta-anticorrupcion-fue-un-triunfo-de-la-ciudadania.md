Title: La consulta Anticorrupción fue un triunfo de la ciudadanía
Date: 2018-08-27 13:42
Category: Entrevistas, Política
Tags: Centro Democrático, Consulta anticorrupción, Iván Duque
Slug: la-consulta-anticorrupcion-fue-un-triunfo-de-la-ciudadania
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/08/consulta-med.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: La Silla Vacia] 

###### [27 Ago 2018] 

Pese a que la Consulta Anticorrupción no logró el umbral necesario para ser aprobada, los líderes de esta iniciativa han calificado como un **triunfo el avance de una ciudadanía politizada que envió un mensaje contundente en contra de los actos corruptos** por parte de congresistas, funcionarios públicos y la maquinaria electoral.

De acuerdo con Julian Triana, analista político, la jornada de ayer “es una victoria política” que debe ser leída y acogida por los congresistas, en donde hay un mandato por parte de los 11 millones de personas que se movilizaron con el voto. Asimismo, señaló que esta votación también **logró vencer las viejas tradiciones electorales al no ser respaldada por ninguna maquinaria electoral.**

“Las personas que salieron a votar ayer, sin un peso, sin certificado electoral, sin que se diera la media jornada electoral, son votos de opinión, y cuando un país comienza a votar así, significa que se acercan cambios en la política del país” señaló Triana.

### **Los mensajes de Duque** 

Una vez se dieron a conocer los resultados de la consulta, el presidente Iván Duque dio una alocución en la que celebraba la alta votación que se tuvo durante la jornada y se refería a los cuatro proyectos de ley que ya presentó ante el Congreso de la República, en donde se proponen algunos de los puntos de la consulta anticorrupción.

Sin embargo, Triana manifestó que es importante tener en cuenta que **el apoyo de Duque a la consulta solo se dio en los últimos días** y evidencia un interés, de parte del Centro Democrático, por captar a las personas de centro de una forma más “loable”. (Le puede interesar:["La propuesta del Centro Democrático que va en contra de la consulta anticorrupción"](https://archivo.contagioradio.com/centro-democratico-contra-anticorrupcion/))

Además, Triana señaló que de haber pasado la consulta anticorrupción uno de los partidos más afectados hubiese sido el Centro Democrático, ya que integrantes como **Uribe, habrían tenido que hacer pública su declaración de renta**, mientras que otros habrían estado en su último periodo electoral.

<iframe id="audio_28110032" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_28110032_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
