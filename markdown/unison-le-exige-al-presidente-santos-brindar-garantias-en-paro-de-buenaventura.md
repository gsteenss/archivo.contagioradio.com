Title: UNISON le exige al presidente Santos brindar garantías en paro de Buenaventura
Date: 2017-05-28 18:32
Category: Movilización, Nacional
Tags: agresiones esmad, Paro Buenaventura
Slug: unison-le-exige-al-presidente-santos-brindar-garantias-en-paro-de-buenaventura
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/05/paro-civico-marcha-buenaventura-perdidas-actos-vandalicos-22-05-2017-e1496014083254.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Noticiero 90 Minutos] 

###### [28 May 2017]

El sindicato de trabajadores de servicios públicos UNISON, uno de los más grandes en el Reino Unido, envió una carta al presidente Juan Manuel Santos, en rechazó de la v**iolencia usada por el Escuadrón Móvil Anti Disturbios contra la población de Buenaventura en el paro que se desarrolla allí y exigió garantías para la movilización.**

Los más de 1.3 millones de trabajadores le expresaron al presidente que lo exhortan para que atienda las exigencias del pueblo de Buenaventura y manifestaron su preocupación por los informes de organizaciones defensoras de derechos humanos que indican que “la protesta pacífica fue atacada por una enorme operación por tierra, aire y mar, involucrando unidades policiales, militares y navales, **con el uso indiscriminado de gas lacrimógeno, armas de fuego y balas de goma” hacia las personas que protestaban.**

Además, en la carta también señalan que los observadores de derechos humanos señalaron que **numerosas personas resultaron heridas en las manifestaciones, entre los que hay niños y mujeres**. Del mismo modo pronunciaron su preocupación frente a las más 200 detenciones que se han producido durante el paro. Le puede interesar:["Hasta los niños son víctimas del ESMAD en Buenaventura"](https://archivo.contagioradio.com/se-recrudece-la-represion-a-paro-civico-de-buenaventura/)

Frente a esta situación los trabajadores de UNISON, le pidieron al presidente Santos que respete el derecho a la protesta y se abstenga de cualquier nueva violencia hacia los participantes de las movilizaciones, que **realice las respectivas investigaciones y sanciones contra los agentes del Estado acusados de abusos durante el paro**.

De igual forma le pidieron a Santos que garantice la seguridad de las familias y los miembros que integran el comité impulsor del paro y finalmente que **actué rápidamente para implementar las demandas del pueblo bonaverence. **Le puede interesar:["Camioneros se unen al paro de Buenaventura"](https://archivo.contagioradio.com/camioneros-se-unen-al-paro-de-buenaventura/)

![WhatsApp Image 2017-05-28 at 3.53.55 PM](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/05/WhatsApp-Image-2017-05-28-at-3.53.55-PM.jpeg){.alignnone .size-full .wp-image-41287 width="907" height="1280"}

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
