Title: CIDH está al tanto de las barreras que pone el Estado para acceder a información sobre la verdad del conflicto
Date: 2020-12-09 20:02
Author: CtgAdm
Category: Actualidad, DDHH
Tags: CIDH, Comisión de la Verdad
Slug: cidh-barreras-impuestas-por-instituciones-del-estado-para-que-conozca-verdad-del-conflicto
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/09/48417385526_11ab065890_c.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: Comisión de la Verdad

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Ante la **Comisión Interamericana de Derechos Humanos (CIDH)** la Comisión de la Verdad, presentó un balance de su trabajo y reiteró las dificultades que han entorpecido el trabajo de investigación sobre lo ocurrido en el conflicto armado y que han impedido el acceso a información reservada por parte de entidades públicas.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

En medio de la rendición de cuentas de este año, el comisionado Alejandro Valencia ya había alertado s**obre la preocupación que existe al interior de la Comisión de la Verdad ante la demora de varias instituciones para atender las solicitudes de acceso a información**; dudas que también fueron expuestas ante la CIDH. [(Lea también: Información de inteligencia y contrainteligencia no ha sido entregada a la Comisión de la Verdad)](https://archivo.contagioradio.com/comision-verdad-denuncia-obstaculos-para-acceder-a-informacion-reservada/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

El comisionado alertó sobre el silencio de algunas entidades, **la falta de respuesta a las solicitudes, omisión, falta de congruencia de los documentos entregados e incluso destrucción de archivos,** en particular obstáculos que resaltan en sectores de la Fuerza Armada, "esa es una dificultad muy seria, a lo que no podamos acceder en los próximos tres meses no vamos a poder integrarlo en nuestro informe final", afirma el comisionado.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Resalta que si bien el Ejército y en general las Fuerzas Armadas han entregado por iniciativa un volumen de informes bastante grande e información puntual que se ha pedido, **hay otro tipo de información que no ha sido recibida y que podría ser considerada más sensible, en particular documentos de operaciones militares o hechos que han afectado a la población civil.**

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Agrega que no existe una generalidad en quienes se niegan a entregar información desde la Fuerza Pública, mientras algunos entregan, otros cuestionan o niegan tener dicha información en su poder, "nos indican mucho que la información no existe o no la encuentran, cuando uno pregunta por hechos muy notorios, masacres que han impactado al país, uno esperaría que la institucionalidad guarde esos archivos".

<!-- /wp:paragraph -->

<!-- wp:quote -->

> Con esa información o sin en esa información nosotros tenemos que sacar adelante nuestro informe final
>
> <cite>Comisionado Alejandro Valencia</cite>

<!-- /wp:quote -->

<!-- wp:paragraph {"align":"justify"} -->

Otras de las instituciones con las que han existido problemas son en su mayoría entidades públicas como la Unidad de Información de Análisis Financiero, Instituto Nacional Penitenciario y Carcelario y Medicina Legal, **"muchas de ellas son en la demora hay peticiones nuestras que llevan más de 6 meses y estas entidades ni nos dan respuesta, hemos tenido que ponerlo en conocimiento de la Procuraduría".**

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Valencia resaltó la facultad que tiene la [Comisión](https://comisiondelaverdad.co/) **para acceder a información pública y reservada, incluidos documentos de inteligencia y contrainteligencia sin que se le pueda oponer reserva alguna**, en especial a información que pueda encontrarse en instituciones con relación a posibles violaciones a los DD.HH, infracciones al Derecho Internacional Humanitario.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Frente a estas dificultades, el comisionado Valencia enumeró cinco peticiones puntuales a la CIDH, con las que se espera que en los 11 meses que restan para entregar el informe final se pueda acceder a la información que aún no ha sido recibida, dichas peticiones incluyen el reconocimiento por parte de la CIDH a la labor desarrollada por la Comisión de la Verdad por las víctimas del conflicto e incluir un balance de la situación expuesta en la audiencia en su informe anual.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

De igual forma se solicitó a la CIDH a exhortar al Estado para que presente antes de marzo de 2021 un informe detallado sobre el estado de las peticiones hechas por institución con relación al acceso de información, a garantizar la colaboración armónica con las funciones de la Comisión y más importante**, y a que abstenga al mismo Gobierno de promover iniciativas legislativas que busquen reformar el Sistema Integral de Justicia**.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/ComisionVerdadC/status/1336402090869075981","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/ComisionVerdadC/status/1336402090869075981

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph {"align":"justify"} -->

**Andrea Urrejola, comisionada de la CIDH, expresó** que están al tanto de las barreras para acceder a la información y afirmó que ya han manifestado esta problemática con preocupación al Gobierno Nacional. [(Lea también: Los desafíos de la Comisión de la Verdad al entrar en su tercer año)](https://archivo.contagioradio.com/los-desafios-de-la-comision-de-la-verdad-al-entrar-en-su-tercer-ano/)

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### "Ausencia del Gobierno es una verdad política"

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Para este encuentro se esperaba la participación del Gobierno, sin embargo, no hubo presencia de delegados del Ejecutivo. Al respecto el comisionado lamentó su ausencia, pues quería presentar un balance de lo avanzado y expresar las dificultades que existen. Pese a ello resalta que el espacio se aprovecho para plantear ante la CIDH sus puntos de vista.  
  
Para el director de la CEV, Francisco de Roux, la ausencia del Gobierno puso en "evidencia la verdad política que nosotros estamos viviendo en el país”. La CIDH de igual forma lamentó dicha ausencia pues esperaban "escuchar posiciones y queríamos hacer preguntas puntuales frente al trabajo de la Comisión de la Verdad, y por cuestiones del tiempo que le queda es una tarea urgente”, así lo expresó la comisionada de la CIDH, Esmeralda Arosemena. [(Le puede interesar: La Comisión de la Verdad le apuesta a un relato del conflicto que pueda reconciliar al país: Leyner Palacios)](https://archivo.contagioradio.com/la-comision-de-la-verdad-le-apuesta-a-un-relato-del-conflicto-que-pueda-reconciliar-el-pais/)

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### El legado que dejará la Comisión

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

El comisionado Valencia también dio algunos adelantos sobre la creación del Comité de Seguimiento que surgirá tras el final del mandato de la Comisión, y aunque hasta ahora se están estableciendo ciertos parámetros, expresa que su trabajo debe estar atado a las recomendaciones que establezcan y cuya función primordial será la de apropiarse del legado y el finrome final que surja a finales de 2021.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Valencia, quien ha trabajado con las comisiones de verdad de Ecuador, Paraguay, Perú y Guatemala señala que a partir de su experiencia ha identificado una dificultad en que la sociedad digiera la verdad, en particular la denominada verdad de los combatientes, pese a ello **"me atrevería a decir que esta comisión es de las que más ha avanzado, y la JEP nos ha permitido tener un número de testimonios muy importante de personas que participaron en el conflicto y que en otras comisiones no fue posible"**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

"En algunos países es bastante lento, como es el caso guatemalteco, todavía 20 años después de entregar el informe hay bastante dificultades en lo que concierne al acceso de información, uno aspiraría a que estas alturas, en pleno 2020, el acceso a la información sea un derecho", concluye.

<!-- /wp:paragraph -->

<!-- wp:html -->  
<iframe id="audio_61999840" frameborder="0" allowfullscreen scrolling="no" height="200" style="border:1px solid #EEE; box-sizing:border-box; width:100%;" src="https://co.ivoox.com/es/player_ej_61999840_4_1.html?c1=ff6600"></iframe>  
<!-- /wp:html -->

<!-- wp:paragraph -->

Comisión de la Verdad

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
