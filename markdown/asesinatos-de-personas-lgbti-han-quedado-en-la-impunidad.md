Title: Asesinatos de personas LGBTI han quedado en la impunidad
Date: 2017-01-30 21:40
Category: El mundo, LGBTI
Tags: Asesinatos a personas LGBTI, LGBTI en América Latina, Personas Trans en América Latina
Slug: asesinatos-de-personas-lgbti-han-quedado-en-la-impunidad
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/01/trans.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Ytimg] 

###### [30 Ene 2017] 

En el Día Nacional de la Visibilidad Trans en Brasil, se conmemoró a las 144 personas trans que fueron asesinadas durante 2016. Según la Comisión Interamericana de Derechos Humanos, los países en donde más se han presentado asesinatos de personas LGBTI son Brasil, México, Estados Unidos y Colombia.

La organización Colombia Diversa, advirtió a través de su informe *Cuerpos excluidos, Rostros de impunidad*, que 33 hombres y mujeres trans fueron asesinadas por su condición de orientación sexual e identidad de género, 91 personas LGBTI fueron afectadas por violencia policial, 110 personas LGBTI fueron asesinadas y el total de los casos en todo el país se encuentran en la impunidad.

### El día de la memoria Trans 

En varios lugares del mundo, se conmemora del día de la *memoria trans, *esta fecha tiene sus inicios en el año 1988, debido al asesinato de una mujer trans en Estados Unidos, que según activistas LGBTI fue una de “las primeras muestras de la discriminación y violencia por prejuicio”, con que se justifican los ataques a personas de orientación sexual e identidad de género diferente.

Algunos expertos y expertas como Tathiane Araújo, presidenta de la Red Trans Brasil, aseguran que el problema social de los prejuicios y la estigmatización, radican en el seno de la propia familia, “que en la mayoría de casos desaprueba que sus hijos no se identifiquen con el sexo con el que han nacido” y no les brindan el apoyo oportuno.

Por último, cientos de organizaciones y personas LGBTI han pedido mediante recolección de firmas en la plataforma Change.org, a la Organización Mundial de la Salud, la despatologización de la transexualidad y el fin de este estigma “para uno de los colectivos con mayor riesgo de exclusión social”.

###### Reciba toda la información de Contagio Radio en [[su correo]
