Title: El salario mínimo en Colombia es de sobrevivencia
Date: 2014-12-24 20:03
Author: CtgAdm
Category: Economía, Política
Tags: economia, salario minimo
Slug: el-salario-minimo-en-colombia-es-de-sobrevivencia
Status: published

###### Foto: grandesmedios.com 

Finaliza nueva reunión de la mesa de concertación del salario mínimo entre empresarios y trabajadores, sin que se llegue aún acuerdo entre el 4,2% propuesto por los primeros, y el 9% por las centrales sindicales.

El gobierno nacional deberá decidir ahora si convoca a una nueva reunión de la mesa, o fija el salario mínimo por decreto. Según expresa Fabio Arias, de la Central Unitaria de Trabajadores -CUT-, seguramente el incremento responderá, como todos los años, a los intereses de los trabajadores: "Un incremento de la mano de la inflación, del 3.65, no es en realidad un aumento. "El salario mínimo en Colombia es de sobrevivencia"

Los trabajadores esperan que se respeten los acuerdos en cuanto a exoneración del 8% de pensionados, y el restablecimiento de horas nocturnas.
