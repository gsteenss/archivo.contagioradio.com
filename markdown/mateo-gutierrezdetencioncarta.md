Title: Carta de Mateo Gutiérrez desde la prisión
Date: 2017-03-19 20:46
Category: DDHH, Nacional
Tags: Bogotá, Falsos Positivos Judiciales, Mateo Gutierrez
Slug: mateo-gutierrezdetencioncarta
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/02/mateo-gutierrez.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Archivo familiar 

###### 19 Mar 2017 

En una carta fechada el 8 de marzo difundida a través de redes sociales, el estudiante de sociología de la Universidad Nacional Mateo Gutiérrez, detenido por la policía el 23 de febrero con la acusación de ser supuestamente responsable de algunas explosiones ocurridas en la ciudad de Bogotá, plasmó algunos de los pensamientos que ha tenido durante su estancia en prisión. Le puede interesar: [Mateo Gutiérrez León sería víctima de otro falso positivo judicial](https://archivo.contagioradio.com/mateo-gutierrez-leon-seria-victima-de-falso-positivo-judicial/)

En su texto el estudiante aprovecha para saludar a las diferentes personas, familiares, amigos, compañeros y docentes que se han [solidarizado con su situación](https://archivo.contagioradio.com/departamento-de-sociologia-de-la-un-se-solidariza-con-familia-de-mateo-gutierrez/). Del mismo modo expresa que es consciente que su encarcelamiento obedece a motivaciones políticas, que atribuye a una necesidad del gobierno a legitimarse en lo que considera es la "descomposición interna del establecimiento, cuando el Estado veía amenazada su “fortaleza” en cuanto a la “seguridad” en las ciudades".

El estudiante hace una reflexión sobre la situación política que vive el país y da un panorama de lo que a su parecer viene ocurriendo con los ciudadanos, la clase dirigente, la Academia, que viven en una desconexión histórica, que no permite se den las transformaciones de fondo que a nivel social requiere Colombia.

Este es el texto del documento:

Un gran saludo a todos los que puedan leer estas palabras: familiares, amigos, compañeros y cualquiera que desee escucharme. Empiezo por agradecer el apoyo y solidaridad que se ha tejido entorno a mí y mi familia, no alcanzarán las palabras para expresar mi gratitud hacia mis amigos del colegio, entrañables compañeros con los que crecí, a los profesores del Liceo que han escrito artículos y columnas en la prensa, así como a los estudiantes y profesores de la Universidad: a todos los que me han escrito, especialmente al profesor Miguel Ángel Herrera, Luis Londoño y Mariana Valderrama, Daniel Schwartz, Juanita Méndez, Camilo Castro, Karin, Cata, a todos los llevo muy dentro de mis afectos. Igualmente, a todas esas personas que se han solidarizado conmigo, todos constituyen esa cadena de afectos que nos protege.

Tengo plena consciencia de que mi encarcelamiento es un acto completamente injusto y arbitrario. Sin embargo, he de insistir en que no es fortuito, ya que tiene una profunda motivación política. En un momento donde la crisis de legitimidad del gobierno mostraba la descomposición interna del establecimiento, cuando el Estado veía amenazada su “fortaleza” en cuanto a la “seguridad” en las ciudades; luego de decretar las más infames leyes para seguir sometiendo al pueblo a la miseria, la ignorancia, la pobreza, explotación y exclusión; se muestra de forma clara el talante autoritario de los que nos gobiernan.

Frecuentemente se dice que la izquierda en Colombia tiene un “discurso anacrónico y trasnochado”, pero escuchando al ministro de defensa en sus declaraciones frente a mi detención, parece más uno de los miembros de cualquier junta militar en tiempos de dictaduras que a uno de los ministros de la “democracia más antigua de América Latina”.  
Una vez más queda claro que la doctrina del enemigo interno es el agua de la que beben los que nos gobiernan, al igual que la caduca y atrasada clase política; viable solamente gracias a un modelo económico de despojo y un sistema político excluyente y aristocrático, al cual tienen la vergüenza de maquillar bajo el rótulo de “democracia”.

Estas palabras tienen como objeto interpelar esa mal llamada “democracia colombiana”, cuestionar que en el país todos los problemas políticos encuentran una respuesta militar por parte del establecimiento, un establecimiento responsable de la violencia que vive el país. Colombia sería un país menos violento si a la hora de un reclamo de los campesinos, de los trabajadores, de las mujeres, los indios, negros y de todos los sectores que conforman la gran pobresía colombiana, se respondiera con soluciones efectivas y sobre todo participación y protagonismo político, y no con policía, encarcelamiento, asesinato y represión.

Si Colombia quiere ostentar orgullosa el título de democracia debe solucionar una profunda contradicción. Democracia viene de la raíz griega demos que significa “pueblo” y kratia que se refiere al gobierno; demokratia o democracia traduce gobierno del pueblo, y ahí radica el problema fundamental: en nuestro país el pueblo no gobierna. Con tan sólo salir a la calle y ver cualquier rostro común y corriente, queda claro que no están expresados en los Santos, Lleras, Uribe o Pastrana el carácter, las costumbres, los modos y las vivencias de nuestra gente.

Hay una desconexión entre el pueblo y sus “dirigentes”, de ahí esa idea extendida en el sentido común de que no importa quien gobierne “todo va a seguir igual”, esa nefasta idea de que la política “no sirve para nada”.

El pueblo no ha encontrado su dirigencia, no ha podido hacerse aún dueño de la política, que no es otra cosa sino el problema del gobierno: del pueblo debe nacer la Democracia. Por ello nuestro deber es situarnos, insertarnos dentro de los reclamos y luchas de ese pueblo y hacerlo dueño de la política, ejercicio que no le corresponde a esos “profesionales” autoproclamados presidentes, senadores o ministros; por el contrario, ese ejercicio le corresponde al ciudadano del común: al eternamente excluido y negado, al de ruana, al indio patirrajado.

Para que todos esos sectores puedan ser gobierno deben encontrar un cauce, un caudal para navegar e irrumpir en el espectro político. Ese es uno de los temores más fuertes de la clase dirigente, por eso su respuesta represiva; saben bien que no pueden responder por los motivos de los problemas de Colombia, hacerlo implicaría aceptar su responsabilidad y darle más legitimidad a la justa lucha de los ciudadanos.

Aquellos que estamos comprometidos con esa lucha tenemos que marcar una pauta y ser ejemplo, ser una opción real que saque al país de la crisis en que lo han sumergido. Por eso debemos hablar de forma clara y en términos políticos. No partimos de un criterio oportunista respecto a la política, no seremos de esos que van a barrios, pueblos y veredas cada cuatro años pidiendo votos para que a la vuelta de otros cuatro años todo siga igual. Partimos de un criterio de principios en cuanto a la política, y sabemos que sólo esa clase de criterio es justo y adecuado, motivo por el cual estamos dispuestos a darlo todo: es justo luchar por tierra, salario, educación, salud, derechos plenos, democracia y libertad. Eso le debe quedar claro a todo el mundo: NO claudicamos, NO renunciamos a nuestros principios y nuestra forma de ver y entender el mundo, mucho menos a cómo lo pensamos transformar.

Hablo en plural, y con esto hago un llamado, porque sé que más de uno podrá recogerse en estas palabras, porque no es un problema mío o de mi situación, sino de miles de desposeídos en Colombia Nunca he sido adepto de los personalismos, por ello me considero tan sólo un preso más, un colombiano más, un ciudadano ordinario, y a mis semejantes les escribo: a todos los patriotas que aman este pedazo de tierra, especialmente a los intelectuales, letrados y profesionales.

Estando en la cárcel me he preguntado ¿Qué hacemos desde la academia por los presos del país? No sólo por los políticos, sino por todos los que viven en este sistema carcelario inhumano, que criminaliza la pobreza y hace del pobre el “enemigo” social. Los intelectuales deben comprometerse en todas las aristas de la vida social y política; no es momento de ceder, por el contrario, debemos insistir: el momento que atravesamos se presta para ahondar en la lucha, para abrir propuestas y alternativas democráticas, para crear desde abajo un nuevo país.

Justamente allí se encuentra nuestra libertad; en el desarrollo de esas alternativas y propuestas, en la conducción de los grandes asuntos del país, en el ejercicio del poder político y por ello no renunciamos a esa libertad, ya que trae consigo la dignidad. El momento del miedo ha pasado, ahora sólo nos debe preocupar la causa del pueblo colombiano. Mañana podrán ocurrir muchas cosas, es incierto qué nos depare el devenir, de lo que si hay certeza es que las necesidades del momento no son otras que movilizarse, organizarse y luchar por Colombia.

“Seamos libres, lo demás no importa”
