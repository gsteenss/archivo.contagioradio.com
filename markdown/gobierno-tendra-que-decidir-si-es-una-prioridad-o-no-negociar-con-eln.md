Title: Gobierno tendrá que decidir si es una prioridad o no negociar con ELN
Date: 2018-11-20 17:02
Author: AdminContagio
Category: Entrevistas, Política
Tags: diálogo con el ELN
Slug: gobierno-tendra-que-decidir-si-es-una-prioridad-o-no-negociar-con-eln
Status: published

###### Foto: @[EquipoPazGob]

###### 20  Nov 2018 

Tras conocerse la presencia de alias Gabino, jefe máximo del ELN en Cuba, el  **Alto Comisionado para la Paz, Miguel Ceballos** exigió al país insular entregar al dirigente guerrillero sobre quien pesan dos circulares rojas de Interpol por diferentes delitos; de igual forma pidió a Venezuela aclarar si hay jefes de ese grupo amado en su territorio y de ser así entregarlos a las autoridades, un reclamo que tensiona más el ya delicado asunto de sentarse a negociar un acuerdo de paz con el ELN.

El académico e investigador en temas de conflicto y paz, **Luis Eduardo Celis** indicó que este es el momento en que el presidente Iván Duque debe decidir si está interesado en negociar con el ELN o someter al grupo armado, que tras 54 años de conflicto "ha demostrado ser incontrolable pues no se le ha podido someter ni derrotar”. (Le puede interesar [Por ahora, Duque no quiere la negociación sino la rendición del ELN: Víctor de Currea](https://www.google.com.co/search?q=Por+ahora%2C+Duque+no+quiere+la+negociaci%C3%B3n+sino+la+rendici%C3%B3n+del+ELN%3A+V%C3%ADctor+de+Currea)&rlz=1C1GCEA_enCO824CO824&oq=Por+ahora%2C+Duque+no+quiere+la+negociaci%C3%B3n+sino+la+rendici%C3%B3n+del+ELN%3A+V%C3%ADctor+de+Currea)&aqs=chrome..69i57.316j0j7&sourceid=chrome&ie=UTF-8))

El también asesor de la Redprodepaz resaltó que la decisión que tome Iván Duque puede ser influida por “sectores de la política colombiana como el uribismo interesados en la permanencia del conflicto armado” pero que en caso de no estar interesado, el **Gobierno tendrá que asumir las responsabilidades de dicha decisión**, es decir “controlar al ELN, proteger a la población y asumir el costo de la continuidad del conflicto”.

Frente a las exigencias que realizó el Gobierno a Venezuela y Cuba para que entreguen información sobre la presencia de jefes del ELN en su territorio, Celis indicó que se trata de “una desatención y una falta de respeto total”, pues Cuba en particular, ha demostrado ser un aliado solidario en la resolución del conflicto armado en Colombia.

El académico añadió que la permanencia o no de Nicolás Rodríguez Bautista, alias Gabino en Cuba no debería ser un tema de mayor importancia para una agenda mediática y que el Gobierno debería centrarse en hablar sin condiciones con el ELN y avanzar en una negociación viable.

###### Reciba toda la información de Contagio Radio en [[su correo]
