Title: Familias se toman edificio en el centro de Bogotá
Date: 2017-02-13 13:00
Category: DDHH
Tags: Derecho a una vivienda digna, Enrique Peñalosa, Gobierno Santos, M-19
Slug: familias-se-toman-edificio-centro-bogota
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/02/WhatsApp-Image-2017-02-13-at-11.54.30-AM-e1487008798533.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [13 Feb 2017] 

Al menos unas 50 personas se encuentran protestando al interior de un edificio en el centro de Bogotá, ubicado en la calle 22 con Caracas. Quienes se manifiestan son familias víctimas del conflicto armado apoyadas por integrantes y ex integrantes del M19, quienes apoyan la justa reclamación de estas familias y exigen la presencia del Alcalde Enrique Peñalosa y funcionarios del Gobierno Nacional, para que **den cumplimiento al derecho a la vivienda dispuesto en la constitución del 91 y les “ofrezcan alguna alternativa”.**

Rubiel Antonio Zapata manifestante e integrante del M19, señala que se amparan en el **artículo 51 de la Constitución Política el cual dispone que todos los colombianos tienen derecho a vivienda digna**, el Estado fijará las condiciones necesarias para hacer efectivo este derecho y promoverá planes de vivienda de interés social, sistemas adecuados de financiación a largo plazo y formas asociativas de ejecución de estos programas de vivienda.

Desde tempranas horas de la mañana, el ESMAD hace presencia en el lugar y hasta el momento no se han presentado heridos. Además, Zapata a**dvirtió que no abandonarán las instalaciones del edificio hasta tanto tengan “una solución por parte de los gobiernos** Nacional y Distrital”.

<iframe id="audio_16981501" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_16981501_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
