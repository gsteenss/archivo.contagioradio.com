Title: Por presiones del Centro Democrático retiran programa de Antonio Morales
Date: 2016-03-18 09:40
Category: DDHH, Nacional
Tags: Antonio Morales, Canal Capital, Centro Democrático, El Café Colado
Slug: por-presiones-del-centro-democratico-retiran-programa-de-antonio-morales
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/03/antonio-morales-el-primer-café-e1458311980272.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Canal Capital 

###### [18 Mar 2016] 

El periodista Antonio Morales denunció que debido a la **persecución del Centro Democrático** hacia la generación de espacios informativos diferentes**, se cancela la emisión del programa 'El Café Colado'**, que se transmitía por el Canal del Congreso y que era la nueva versión del programa de análisis e información 'El Primer Café' que se transmitía en Canal Capital.

La **“política agresiva y sucia del Centro Democrático”,** como lo señala Morales, sería la que obligó al presidente del Congreso a retirar el programa que apenas llevaba una emisión. "Luis Fernando Velasco aceptó acabar con el programa agobiado por las presiones del Centro Democrático”, asegura el periodista, quien añade que se trata de una cuenta de cobro por su “actividad crítica”.

De acuerdo con Antonio Morales, el recurso que se usó para que se diera el cierre del programa, fue una denuncia contra el periodista por acceso carnal violento interpuesta por una mujer en Cali. Este hecho fue usado por la **represente María Fernanda Cabal para generar una campaña de desprestigio en contra de Morales**, que finalizó con presiones hacía el presidente del Congreso para que aceptara el cierre del espacio.

“**Es una falsa denuncia de una mujer en Cali, no hay imputación de cargos ni ninguna decisión de la fiscalía”**. Para el analista, es un complot impulsado por el Centro Democrático que usó Cabal para arremeter por Twitter contra él usando “fusiles y tanques de guerra”.

A través de un comunicado, el analista expresa: “Detrás de esto, evidentemente, hay una – en este caso- persecución contra mí por parte del Centro democrático, contra la libertad de expresión, el derecho a la información, el derecho a disentir, las libertades laborales, las voces críticas e independientes, negando la presunción de inocencia y con una pretensión de censura previa”.

El primer y único programa de 'El Café Colado', según Morales, habría logrado “resultados maravillosos de audiencia”, pero **“no nos permiten ni el 1% de voz,** quieren un silencio total”, sostiene el periodista concluyendo que **deben empezar a forjarse espacios políticos que garanticen la  democracia informativa.**

<iframe src="http://co.ivoox.com/es/player_ej_10853705_2_1.html?data=kpWll5ibdJahhpywj5WcaZS1lpiah5yncZOhhpywj5WRaZi3jpWah5yncbHj05Dd1MrXrdDixtiYxsrQb6TZz9nf0ZCoqc7jxNeSpZiJhZLoysjcjdfJuMrmwtOY0tfTq9PVzsaYj4qbh4630NPhw8zNs4zGwsnW0ZCRaZi3jpk%3D&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>  
Aquí el comunicado de Antonio Morales:  
[![Cd1d8-SWIAAGesw](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/03/Cd1d8-SWIAAGesw.jpg){.aligncenter .size-full .wp-image-21697 width="578" height="668"}](https://archivo.contagioradio.com/por-presiones-del-centro-democratico-retiran-programa-de-antonio-morales/cd1d8-swiaagesw/)

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)
