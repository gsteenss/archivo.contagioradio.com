Title: La amenaza petrolera sobre biodiversidad del Archipiélago de San Andrés
Date: 2016-11-23 18:38
Category: Ambiente, Otra Mirada
Tags: gobierno colombiano, Nicaragua, petroleo, San Andrés
Slug: la-amenaza-petrolera-biodiversidad-del-archipielago-san-andres
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/arrecife-e1479944018864.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Turismoysusrelaciones.blogspot 

###### [23 Nov 2016] 

[Más de **350 mil kilómetros cuadrados del Archipiélago de San Andrés y que son Reserva Mundial de la Biosfera declarada por la UNESCO**, se encuentran en amenaza por un proyecto de exploración petrolera y de explotación de gas, que además afectaría a los más de 85 mil habitantes de la región insular colombiana.]

[Eso es lo que revelan investigadores de la Universidad Nacional de Colombia Sede San Andrés, de acuerdo con su análisis sobre ‘Geopolítica del extractivismo en el Caribe’. Según ese estudio hace más de 6 años existe un proyecto de una coalición de grandes empresas que está interesada en la exploración petrolífera del Caribe occidental.]

[Se trata de un proyecto que busca triplicar las  reservas petroleras y que incluso pretende realizar explotación de gas. “**Desde 2012 se vienen estableciendo bases petroleras en costas del Caribe colombiano y desde 2011 existen documentos sobre ofertas realizadas por el Gobierno nacional** para la explotación dentro de la reserva”, explica para la Agencia Nacional de Noticias UN, Catalina Toro, quien hace parte del Grupo de Investigación sobre Derecho y Política Ambiental.]

[Lo que llama la atención, es que pese a que se trata de un proyecto tan antiguo y que el gobierno conocía, el Ministerio de Relaciones Exteriores de Colombia, impidió mediante una carta en el año 2011, que no se reforzara la protección de este territorio.]

[Para ese entonces se estaba buscando a través de otra figura internacional, que la totalidad del archipiélago entrara a hacer parte de la lista de **Patrimonio Natural de la Humanidad**, sin embargo, esto se consideró como “no pertinente” por parte del gobierno y se pidió “no avanzar en la presentación de esta candidatura”, como decía la carta firmada por Patti Londoño Jaramillo, actual vicecanciller.]

[Esa declaratoria representaba el mecanismo mediante el cual se apoyaba a los pobladores de la isla, quienes se han opuesto a la entrada de empresas petroleras a su región, a quienes Nicaragua les ha negado su derecho a la consulta previa. “**Detrás de la pretensión de Nicaragua de incluir otras 200 millas hacia la costa,** está el petróleo; en 2014, desoyendo los derechos de consulta que tienen las comunidades, este país desarrolló proyectos de sísmica para establecer si había potenciales recursos de hidrocarburos”.]

[De acuerdo con la docente, la comunidad también se están enfrentando a la llegada de pescadores industriales a zonas en las que esa actividad era netamente artesanal. Además ahora tienen que vivir con limitaciones para el abastecimiento de agua potable y la disposición de residuos, debido al descontrolado incremento de turistas.]

[Hoy la gran biodiversidad marina y costera de estas islas que **cuentan con más del 77% de las áreas coralinas someras de Colombia,** innumerables ecosistemas someros como manglares, praderas de fanerógamas marinas, fondos arenosos, playas y ecosistemas profundos, además variedad de especies clave, se encuentra bajo amenaza extractivista por cuenta de las actuaciones de los gobiernos de Colombia y Nicaragua.]

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
