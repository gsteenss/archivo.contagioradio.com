Title: 'Desterrados' un panorama sobre la concentración de tierras
Date: 2016-11-30 23:26
Category: Ambiente, DDHH
Tags: comunidades indígenas y campesinas, concentración de tierras, despojo de tierras, extractivismo, Mujeres y extractivismo, Oxfam
Slug: desterrados-panorama-la-concentracion-tierras
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/desterrados.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: OXFAM] 

###### [30 Nov 2016 ] 

En el Cono Sur el 1% de las fincas concentra más del 50% del total de las tierras productivas, **lo que da cuenta de la alta concentración de tierra en manos de unas cuantas minorías,** esa es una de las grandes conclusiones consignadas en  el informe Desterrados Tierra, poder y desigualdad en América Latina realizado por la organización OXFAM.

Para el caso de Colombia, el informe demuestra que sólo el **0,4% de las fincas concentran más del 67% de la tierra productiva** y que millones de familias campesinas deben convivir con mega plantaciones, en el documento resa por ejemplo en el caso de Argentina los monocultivos ocupan el equivalente a 44.000 campos de fútbol”.

Simon Ticehurst director de OXFAM en América Latina y el Caribe resaltó que es necesario **“combatir la desigualdad en América Latina a través de las transformaciones en la repartición y las formas de acceso a la tierra”.**

Dicha desigualdad ha provocado en el continente fuertes conflictos sociales y ambientales que **han desencadenado hostigamientos, persecución, asesinatos y desapariciones contra los defensores de los territorios y las comunidades.**

La sociedad debe girar su mirada sobre" las políticas públicas que los gobiernos hacen **a la medida de las empresas extractivas, permitiendo que unos cuantos grupos poderosos acumulen cada vez más riqueza** y mantengan la opresión sobre el campesinado" concluyó Ticehurst.

*Desterrados* ubica la raíz de la profunda desigualdad de América Latina en el sistema económico, político y social neoliberal, basado en la explotación a gran escala de bienes naturales, el otorgamiento indiscriminado de **concesiones mineras, petroleras, ganadería extensiva y monocultivos de soja, maderables, caña y palma de aceite.**

Varios de los especialistas invitados coincidieron en que un caso representativo de la concentración en términos de políticas públicas es la ley ZIDRES, la cual **“profundizaría la brecha de desigualdad y la exclusión que viven las mujeres y el campesinado colombiano”** precisaron los expertos.

### **Mujeres, comunidad y territorios** 

Uno de los ejes transversales del informe es la desigualdad de las mujeres en el acceso a la tierra y a su propiedad, frente a ello María Raquel Vásquez lideresa indígena de la comunidad Chiorti de Guatemala, coordinadora de la Red Centroamericana de Mujeres Rurales Indígenas y Campesinas, señaló que **“es necesaria la voluntad política de los Estados frente a nuestras demandas, porque mientras las mujeres no tengamos acceso a la tierra, no habrá paz”.**

La lideresa que a causa de su lucha por la defensa del territorio, la vida y los derechos de las mujeres estuvo exiliada 14 años en México, manifestó que "en la práctica nosotras somos especialmente marginadas, **en Guatemala las mujeres rurales sólo poseemos menos de un 8% del total del territorio, siendo parcelas pequeñas de baja calidad”.**

Por otra parte Guadalupe Esquivel mujer salvadoreña integrante de la Coordinadora Latinoamericana de Organizaciones del Campo CLOC-VÍA CAMPESINA, resaltó que si bien el panorama es desalentador, "las mujeres rurales latinoamericanas nos hemos caracterizado por **estar en la primera línea de lucha por la tierra a pesar de que sufrimos violencias como acoso sexual, amenazas y hostigamientos a nuestras familias**”.

Sobre ese tema, Esquivel  trajo a la memoria de los y las asistentes el caso de la defensora ambiental hondureña Berta Cáceres, quien por encabezar la resistencia a un proyecto hidroeléctrico fue víctima de un cruento suceso que **“expuso al mundo la extrema vulnerabilidad de las mujeres defensoras y la pasividad, cuando no complicidad, de gobiernos que incumplen sus obligaciones”** recalcó Esquivel.

### **Alternativas frente a la Desigualdad** 

Además de proporcionar un amplio panorama del tema de la concentración de tierras, **este informe también contiene propuestas desde las comunidades, organizaciones sociales y ambientales,** para la construcción de Paz desde los territorios latinoamericanos.

Gabriel Tobón profesor e investigador de la Universidad Javeriana y de la CLACSO, indicó que **“se deben fortalecer las redes latinoamericanas de movimientos sociales en pro de la defensa de los territorios y los derechos humanos,** pues es escalofriante saber que América Latina es el continente más violento contra quienes le apuestan a la defensa de la vida”.

Agregó que para el caso colombiano “no es gratis que asesinen a tantos reclamantes de tierras en los últimos meses y el gobierno de declaraciones tan tibias, sin organización la lucha pierde perspectiva, **se hace** **un llamado a la unidad popular, a luchar por el poder político desde los territorios”.**

María Vásquez comentó que "en Guatemala hace 20 años se hicieron acuerdos de paz, sin embargo no han habido cambios sustanciales porque los gobiernos no han tenido la suficiente disposición, **mientras el acceso a la tierra no sea igualitario la paz no será una realidad, ojalá en el caso de Colombia las comunidades logren hacer cambios".**

Por último tanto el informe como los invitados hicieron un llamado a las instituciones gubernamentales para que estos **cumplan sus funciones respecto a las garantías de derechos, labores investigativas en casos de crímenes, seguimiento, regulación y transformación de políticas** que favorecen al sector extractivo e industrial.

<iframe id="audio_14327145" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_14327145_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
