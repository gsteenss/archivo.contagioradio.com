Title: Octubre comienza con campaña paramilitar de las AGC
Date: 2020-10-02 10:28
Author: CtgAdm
Category: Actualidad, Comunidad
Tags: AGC, Antioquia, Caldas, Plan Pistola
Slug: campana-paramilitar-agc
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/10/EjRj9LDWoAAX7-8.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/10/WhatsApp-Image-2020-10-01-at-9.35.33-PM.jpeg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","fontSize":"small"} -->

Foto: Graffiti AGC / @andrealdana

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

A través de un plan coordinado, viviendas, vehículos y locales comerciales de municipios de **Córdoba, Sucre y Bolívar, Magdalena, La Guajira, Caldas, Chocó y Antioquia** amanecieron con grafitis y panfletos de las autodenominadas Autodefensas Gaitanistas de Colombia (AGC). La aparición de estos mensajes ha causado temor entre las comunidades ante el dominio territorial que se demostró con estas acciones.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Con el mensaje “AGC presente” escrito en paredes de tiendas, fachadas de casas y calles amanecieron municipios como San Pedro de Los Milagros, Medellín, Cocorná, Urabá, Ituango, Sabanalarga, en Antioquia, San Andrés islas, Mariquita en Tolima, Nuquí, Bahía Solano, Carmen del Darién y Acandí en Chocó.

<!-- /wp:paragraph -->

<!-- wp:image {"id":90821,"sizeSlug":"large"} -->

<figure class="wp-block-image size-large">
![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/10/WhatsApp-Image-2020-10-01-at-9.35.33-PM.jpeg){.wp-image-90821}  

<figcaption>
Sectores de Colombia donde ocurrieron los hechos/ Marcha Patriótica

</figcaption>
</figure>
<!-- /wp:image -->

<!-- wp:paragraph {"align":"justify"} -->

El suceso se repitió en La Dorada y Victoria al oriente de Caldas, Monte Líbano, Los Córdobas, Canalete, Puerto Libertador y zona rural de Montería en Córdoba. **A través de redes sociales se ha denunciado[el mismo accionar](https://twitter.com/AbadColorado/status/1311734098927513600)en los departamentos de Sucre y Bolívar**.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/elmerkaleth/status/1311675961637699584","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/elmerkaleth/status/1311675961637699584

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph {"align":"justify"} -->

Además de los graffitis, el grupo repartió un panfleto a lo largo de los municipios justificando su accionar, al hacer mención de "la desprotección institucional”, los ataques de la Fuerza Pública y responsabilizando al ELN y a las disidencias de las FARC de los sucesos de violencia ocurridos en las últimas semanas.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/andrealdana/status/1311726648866332672","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/andrealdana/status/1311726648866332672

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:heading {"level":3} -->

### ¿Cómo operan las AGC?

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Para 2018, según Paz y Reconciliación las autodenominadas AGC o ‘Clan del Golfo’, hacían presencia en 257 municipios del país con cerca de 4.000 integrantes, a través de una compleja red de nodos territoriales en la que sus mandos eran de fácil reemplazo. [(Lea también: Paramilitares afianzan presencia en San José de Apartadó)](https://archivo.contagioradio.com/paramilitares-afianzan-presencia-en-san-jose-de-apartado/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Según Análisis Urbano, el grupo cuenta con un flujo casi ilimitado de recursos derivado de sus economías ilegales como el narcotráfico, la extorsión y la minería ilegal, establecidas entre otros con políticos, empresarios e integrantes de la Fuerza Pública.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Fe de Erratas:

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En esta nota publicada el 2 de Octubre a las 10:28 se hace referencia a un Plan Pistola de las AGC en contra de la policía. Sin embargo la fuente a la que acudimos no tiene sustento para tal información en este momento (05/10/2020 12:47 PM) por lo tanto rectificamos la información publicada y ofrecemos excusas, pues no tenemos medios de prueba suficientes para afirmar **la existencia de un Plan Pistola de las AGC contra la policía nacional.**

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->
