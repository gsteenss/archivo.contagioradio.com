Title: Una paz quimérica
Date: 2016-03-02 16:04
Category: Opinion
Tags: acuerdo de paz, baile rojo, plan Colombia
Slug: una-paz-quimerica
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/03/paz-quimerica.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [foto: Bunkerglo] 

#### [**Mateo Córdoba - @DurkheimVive**] 

###### 2 Mar 2016 

El 29 de Agosto de 1885 Rafael Núñez se dirigía emotivamente al pueblo colombiano informando la rendición de las fuerzas rebeldes en Colombia tras la captura de la última flotilla disidente en el Río Magdalena.  Un año después, producto del triunfo ‘regenerador’ en la guerra, se instituía la Constitución del 86. Sin embargo, dicha constitución desde su brote fracasó como pretendido contrato social y nacional, sabiéndose anacrónica desde el primer momento sería nuestra carta magna durante más de un siglo. Tal vez nuestra historia nos dice a gritos que cuando no se sabe acabar la guerra se hacen contratos sociales cojos.

Hace unos días el gran analista político Ariel Ávila (coordinador de la Fundación Paz y Reconciliación) escribía una columna para el portal web de la Revista Semana en la cual reconocía la importancia de un ejercicio de reparación del Estado frente a la violencia política sufrida por sectores políticos de izquierda como la Unión Patriótica. Haciendo alusión a un episodio tan vergonzoso como el conocido ‘Baile Rojo’, Ávila apela a una gráfica que bien ilustra lo que significó el asesinato como arma política entre 1981 y 1991 (ver: Semana.com). Además, en dicha columna, se mencionan casos como el de Iván Márquez que tras la guerra sucia contra la UP tuvo que renunciar a su curul en la Cámara de Representantes y volver a las filas guerrilleras o el de Manuel Cepeda que sería vilmente asesinado con complicidades del Estado colombiano que han sido probadas. Tras un breve recorrido por las responsabilidades que tuvieron diferentes instituciones oficiales en la masacre a la Unión Patriótica, Ávila plantea –como un mínimo– la necesidad de garantizar a las FARC unas curules en el Congreso (al menos 10) que en su momento le fueron arrebatadas a la UP y sus aliados políticos a sangre y fuego. Si bien dicha propuesta debiera ser considerada por el gobierno como justa y necesaria, tal vez cuando se habla de reparación el Estado colombiano tendría que tomarse aún con mayor incumbencia el asunto como algo más solemne que la exclusiva restitución de los derechos políticos a individuos y colectivos. Es un deber del gobierno comprender la verdad histórica como eje de la reparación y el proyecto de un país sin conflicto armado.

Entendiendo que las reciprocidades lógicas han de ser eje de los procesos de reconciliación tendríamos que comprender la elasticidad de las disposiciones y voluntades en La Habana. Mientras la justicia transicional está siendo entendida por la insurgencia como la necesidad de un arbitraje especial que les juzga en perspectiva del fin del conflicto armado, el Estado ha de entender la justicia transicional como la garantía del ejercicio político no-violento ajustado al carácter político del grupo en proceso de desarme, y allí entra la propuesta de las curules fijas por unos años para las FARC, posibilidad que aún está en discusión. Por otro lado, la verdad ha sido el núcleo de la agenda que el país le ha exigido a las FARC y estas han demostrado franca voluntad con una verdad de “todos y para todos” mientras el gobierno supone que su trabajo con la paz es meramente procedimental, de marcos jurídicos, en últimas, de persuadir y llevar a los ‘violentos’ a la arena de la sana política. Ya quisiera el Estado colombiano huir de sus responsabilidades históricas que van mucho más allá de lo señalado por Ávila y muchos otros académicos en torno a los magnicidios de los ochentas y noventas.  Hay que decirlo y repetirlo cuantas veces sea necesario: El Estado colombiano siempre ha hecho la guerra negando la historia, así pues, es la historia un sujeto más de reparación, el más urgente y el menos mencionado.

Una vez terminada la guerra en 1885 y consumado el triunfo de la regeneración conservadora, Núñez afirmaba ante el país que el triunfo había sido gracias a la Divina Providencia que merecía todos los agradecimientos por su labor de “salvar a la República”. Núñez faltó a la historia, desconoció el carácter político del conflicto y denunció a los rebeldes como la auténtica amenaza al régimen político. Más de un siglo ha pasado, ahora la Divina Providencia es el Plan Colombia y las balas norteamericanas a quienes habrá que agradecerles por su labor de “salvar a la República”.  Santos y De la Calle lesionan la historia cuando salen campantes a decir que no creen en la “hipótesis” de las causas estructurales del conflicto –a pesar del documento producto de la Comisión Histórica– y denuncian a la insurgencia como la genuina amenaza al régimen político y la ciudadanía colombiana.

Tamaño error negar la historia patria cuando es ella quien nos grita que para un renovado y justo contrato social hay que saber terminar las guerras. Mientras episodios como el de Marquetalia o Villarrica sigan siendo sacados de contexto y entendidos como necedades en el tiempo por una de las contrapartes negociadoras, la historia –y con ella la paz– será la gran descalabrada. Ser justo con la historia del conflicto colombiano es hablar de hambre, del frente nacional, de resistencia campesina y de digna rabia, por eso el gobierno calla. El alcance reparador de la verdad histórica no se compara ni con 20 o 30 curules en el Congreso.

El reto no será sólo bajar la guardia y consagrar el silenciamiento de los fusiles. Hay un reto con la historia para que no nos sea arrebata en una paz quimérica, ya Bertolt Brecht lo advertía: “El que desconoce la verdad es un ignorante; pero el que la conoce y la desmiente, es un criminal.”

###### [Fuentes:] 

-   ###### [Las reparaciones del otro lado, Ariel Ávila. 16 de febrero 2016. Ver:][[http://www.semana.com/opinion/articulo/ariel-avila-las-reparaciones-del-otro-lado/460896]](http://www.semana.com/opinion/articulo/ariel-avila-las-reparaciones-del-otro-lado/460896) {#las-reparaciones-del-otro-lado-ariel-ávila.-16-de-febrero-2016.-ver-httpwww.semana.comopinionarticuloariel-avila-las-reparaciones-del-otro-lado460896}

-   ###### [Discurso Rafael Núñez 29 de agosto 1885, Bogotá. Ver: <http://www.banrepcultural.org/sites/default/files/brblaa1142848.pdf>]


