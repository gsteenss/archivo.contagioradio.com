Title: Colonos israelíes ocupan casa de la familia Abu Rajab, en Hebrón
Date: 2017-08-03 10:00
Category: Onda Palestina
Tags: Apartheid Israel, BDS, Hebrón, Palestina
Slug: israelies-ocupan-hebron
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/08/casa-hebron.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: *[Mamoun Wazwaz, Agencia Anadolu]* 

El 25 de julio, **cerca de 100 colonos israelíes se apoderaron a la fuerza de un hogar palestino en la ciudad palestina de Hebrón**. El hogar ocupado es de la familia palestina Abu Rajab, cuyos miembros han sido atacados por soldados, policías y colonos israelíes, según informa Issa Amro, director del grupo activista “Jóvenes contra los Asentamientos” basado en Hebrón.

Amro cuenta que **los colonos se tomaron la casa y sacaron a la familia luego de haberlos intimidado**, todo esto "justo enfrente de la policía y el ejército \[israelí\]". Además, recordó que el día anterior “**los colonos también atacaron a un niño palestino** que pasaba por allí, y arrojaron piedras a una familia", afirmando que esto es muy común en Hebrón.

Alrededor de **700 israelíes judíos viven en colonias en el corazón de Hebrón**, donde son protegidos por miles de soldados y policías israelíes. Por otro lado, alrededor de **37.000 palestinos viven en la misma zona y aguantan decenas de retenes militares** que limitan seriamente su movimiento.

Como ciudad ocupada, Hebrón está dividida en tres esferas de control: primero, las zonas administradas por la Autoridad Palestina, las que son administradas por la Autoridad Palestina junto con las fuerzas militares israelíes, y las de completo control israelí. Esto implica que como Estado ocupante, **Israel es responsable por garantizar la seguridad de los habitantes**.

A pesar de esta responsabilidad, **la ocupación de la casa de la familia Abu Rajab ha sido respaldada por el primer ministro israelí, Benjamín Netanyahu**, y por muchos otros altos funcionarios israelíes, quienes piden que el gobierno permita que los colonos se queden en el hogar ocupado indefinidamente.

###### Escuche esta y más información sobre la resistencia del pueblo palestino en [Onda Palestina](https://archivo.contagioradio.com/programas/onda-palestina/), todos los Martes de 7 a 8 de la noche en Contagio Radio. 
