Title: Movimientos sociales y populares trabajarán por la unidad política
Date: 2017-11-28 15:14
Category: Nacional, Política
Tags: Encuentro por la Unidad y la Paz, movimientos populares, Movimientos sociales
Slug: movimientos-sociales-y-populares-trabajaran-por-la-unidad-politica
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/11/unidad-politica.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio ] 

###### [28 Nov 2017] 

Diferentes organizaciones y movimientos sociales del país, han convocado un **Encuentro Nacional por la Unidad y la Paz** con el fin de “dar un salto cualitativo en el proceso de construcción de una perspectiva estratégica de unidad”. Esto, teniendo en cuenta la necesidad de “levantar un proyecto político alternativo en Colombia”.

De acuerdo con Omar Fernández, integrante de la Coalición de Movimientos Sociales de Colombia, COMOSOC, el encuentro buscará que se establezcan procesos para coordinar y fortalecer las convocatorias para **la unidad de los movimientos sociales.** Dijo que “hay una necesidad de dar un salto de calidad en la unidad del campo popular”.

Adicionalmente, afirmó que en la coyuntura actual que vive el país, se requiere que los movimientos populares tengan una capacidad de convocatoria más amplia **hacia sectores de la población que no está organizada**. Por esto, el encuentro “quiere arrojar nuevas pistas y estilos de trabajo en el quehacer político del campo popular”. (Le puede interesar: ["Mensaje a los movimientos sociales que protestan en todo el país"](https://archivo.contagioradio.com/mensaje-a-los-movimientos-sociales-que-protestan-por-todo-el-pais/))

### **Movimientos sociales buscarán superar la dinámica de fragmentación que existe** 

Si bien en el país es posible evidenciar una polarización política que se desliga de los sucesos que hacen parte del panorama nacional, para los movimientos sociales y populares es claro que hay también ciertas fracturas **que perjudican a la movilización social**.

Por esto, “vamos a trabajar el tema de la paz, de **la defensa de la vida** frente a la situación de persecución sistemática contra los líderes y las lideresas sociales y el tema de las elecciones en una clave de unidad”. Estos temas serán abordados con la misión de unir y trabajar por el fortalecimiento del movimiento social. (Le puede interesar: ["Política de miedo" de paramilitarismo debe ser afrontada con movilización social"](https://archivo.contagioradio.com/paramilitarismo-es-una-politica-del-miedo-impuesto/))

Finalmente, Fernández recordó que el Encuentro Nacional por la Unidad y la Paz se llevará a cabo en Bogotá el **30 de noviembre y el 1 de diciembre** del año en curso en el consultorio jurídico de la Universidad Autónoma en el barrio la Candelaria.

<iframe id="audio_22334421" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_22334421_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]

<div class="osd-sms-wrapper">

</div>
