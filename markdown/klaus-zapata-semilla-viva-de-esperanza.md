Title: Klaus Zapata semilla viva de esperanza
Date: 2017-03-05 17:08
Category: DDHH, Sin Olvido
Tags: Klaus Zapata, soacha
Slug: klaus-zapata-semilla-viva-de-esperanza
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/03/IMG_0147-e1488752503526.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [05 Mar 2017] 

El 6 de marzo de 2016 fue asesinado Klaus Zapata, joven de 20 años, comunicador popular y miembro de la Juventud Comunista. **Un año después, su familia, amigos y compañeros se reunieron en el salón comunal de Ciudad Latina**, a 10 minutos del lugar en donde le quitaron la vida mientras jugaba un partido de "micro", para recordarlo y mantener viva la memoria y el legado del trabajo popular de Klaus.

Entre mensajes, música, poesía y una eucaristía, cada uno de los asistentes al evento recordó sus anécdotas vívidas entre los pasillos de la universidad o las calles del barrio.  Le puede interesar:["Mil claveles blancos por la memoria de Klaus Zapata"](https://archivo.contagioradio.com/mil-claveles-blancos-por-la-memoria-de-klaus-zapata/)

Arturo Zapata, padre del estudiante, afirmó durante el homenaje que su hijo es **"semilla viva" y que ahora lo ve reflejado en cada uno de los jóvenes que trabaja por construir otro país**, sin embargo, señaló que el Estado no ha dado ningún avance frente a la investigación de la muerte de su hijo ni sobre los autores intelectuales del asesinato. Le puede interesar: ["Asesinan a Klaus Zapata integrante de la Juventud Comunista en Soacha"](https://archivo.contagioradio.com/asesinan-a-klaus-zapata-integrante-de-la-juventud-comunista/)

\

###### Reciba toda la información de Contagio Radio en [[su correo]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio]{.s1}](http://bit.ly/1ICYhVU)[.]{.s1}
