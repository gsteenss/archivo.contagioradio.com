Title: Por enfrentamientos armados en el Bajo Cauca han sido desplazadas más de 1.500 personas
Date: 2018-03-20 12:21
Category: DDHH, Nacional
Tags: Antioquia, asesinatos de defensores, Bajo Cauca, Desplazamiento forzado, grupos armados, paramilitares
Slug: por-enfrentamientos-armados-en-el-bajo-cauca-han-sido-desplazadas-mas-de-1-500-personas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/03/Desplazamiento.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:  Web Sur] 

###### [20 Mar 2018] 

[Diferentes organizaciones sociales han denunciado la crítica situación que se vive en el Bajo Cauca y Nordeste de Antioquia debido a los constantes enfrentamientos entre grupos armados ilegales. Los asesinatos y desplazamientos forzados suceden a diario y más de 1500 personas entre **indígenas y campesinos** han tenido que salir de sus hogares en municipios como Tarazá y Cáceres.]

[De acuerdo con Fernando Quijano, director de la Corporación para la Paz y el Desarrollo Social CORPADES, “en el Bajo Cauca **brotó la guerra** y la confrontación armada está desestabilizando no sólo al Bajo Cauca sino también al nordeste y podría extenderse a otras partes”, situación que asegura responde a la presencia de los grupos armados y la falta de presencia estatal que hay en la región.]

### **Enfrentamientos entre grupos armados han ocasionado un aumento en el número de asesinatos** 

[Quijano manifestó que se han registrado enfrentamientos entre los grupos de las Autodefensas Gaitanistas de Colombia denominados Clan del Golfo, “que están a la cabeza de alias “Gonzalito” cercano a alias “Otoniel” y se están enfrentando al grupo armado liderado por alias “Ratón”, confrontaciones que se estarían dando por el control territorial y actividades** relacionadas a la minería**, los cultivos de coca y sus corredores estratégicos. (Le puede interesar: ["Control paramilitar se afianza en el Bajo Atrato y Cauca"](https://archivo.contagioradio.com/control-paramilitar-se-afianza-atrato-cauca/))]

Además dijo que los enfrentamientos se han dado por temas de tierras en la medida en que “hay una gran cantidad de tierras muy costosas **que han sido de jefes poderosos** del crimen urbano y rural que también son antiguos jefes paramilitares”. Esto ha llevado a que sucedan asesinatos de manera constante al igual que desplazamientos forzado, "muy a pesar de que hay una fuerte presencia de la Policía y el Ejército Nacional".

### **Más de 500 familias han sido desplazadas** 

[Según CORPADES, sólamente en Cáceres se han registrado 1.500 personas desplazadas en cuatro grandes desplazamientos. En otras regiones, se han presentado desplazamientos **“que no se muestran y ocurren donde están los varequeros**  que han sido obligados a desplazarse al casco urbano de Tarazá”. Además, “cerca de 500 familias se han tenido que desplazar a Medellín”.]

[Como en el Bajo Atrato chocoano, esta situación se está presentando en varios municipios del departamento de Antioquia. En repetidas ocasiones, diferentes organizaciones sociales han manifestado que tanto los homicidios como los desplazamientos forzados se pueden presentar en otras regiones “cuando se creía que con el fin del conflicto con las FARC” habría **garantías de seguridad y paz** para las comunidades. (Le puede interesar:["En conmemoración de las víctimas del Río Cauca, Ríos Vivos rechaza violencia"](https://archivo.contagioradio.com/en-conmemoracion-de-las-victimas-del-canon-del-rio-cauca-rios-vivos-rechaza-la-violencia/))]

[Finalmente, Quijano indicó que **“los líderes sociales están siendo obligados a silenciarse”.** Dijo que “tienen prohibido a hablar con organizaciones como las Naciones Unidas o MAP-OEA”. El trabajo de las organizaciones no ha tenido un gran impacto en los territorios a la vez que “hay una fuerte desidia por parte de la Policía Nacional”. Por esto, hizo un llamado de atención por “el silencio extraño que ha tenido el Gobierno Nacional”.]

<iframe id="audio_24649670" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_24649670_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
