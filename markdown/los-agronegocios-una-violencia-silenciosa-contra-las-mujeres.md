Title: Los agronegocios una violencia silenciosa contra las mujeres
Date: 2018-12-20 08:46
Author: AdminContagio
Category: DDHH, Mujer
Tags: agronegocios, Monocultivos, mujeres, Vichada, violencia sexual
Slug: los-agronegocios-una-violencia-silenciosa-contra-las-mujeres
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/12/AGRONEGOCIOS-770x400.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:] 

###### [19 Dic 2018] 

Los agronegocios y monocultivos, que se vienen desarrollando en el departamento de Vichada, han generado un conjunto de v**iolencias sexuales, económicas y culturales hacia las mujeres** que no han tenido freno ni por parte de las multinacionales, que explotan el territorio, ni de las instituciones estatales; así lo denunció el más reciente informe de las organizaciones Ciase y OXFAM.

### **Agronegocios provocan violencias en contra de las mujeres en el Vichada** 

El texto "Una lapa en la avioneta: una mirada feminista sobre las visiones de futuro y los agronegocios en el Vichada, Colombia" señaló que en este departamento se han  desarrollado, incluso desde la colonización, **agronegocios como monocultivos de caucho, y más recientemente ha entrado la siembra a gran escala de maíz y soya**.

Sin embargo, el documento alerta sobre la expansión de la frontera agrícola que abarcaría 10 millones de hectáreas sobre suelo de sabana húmeda, sin tener en cuentas las afectaciones que estas actividades económicas ya han dejado en las mujeres. (Le puede interesar:["Mujeres realizan el primer tribunal a la justicia patrical en Colombia"](https://archivo.contagioradio.com/justicia-patriarcal-mujeres/))

Diana García, integrante de esta investigación manifestó que entre los daños que ocasionan los agronegocios a las mujeres, se encuentra la destrucción de los ecosistemas naturales, porque reduce la diversidad ecológica y genética que hay en los cultivos, y particularmente provoca **impactos en la salud de ellas, debido al uso de agroquímicos y pesticidas. **

Además aseguró que se logró recopilar información sobre 8 casos de violaciones sexuales cometidas por trabajadores de las empresas que están en el departamento. La mayoría de las víctimas son mujeres jóvenes indígenas, que para salir de sus resguardos o comunidades le pidieron a los transportadores que las acercaran a lugares específicos.

De igual forma, un alto porcentaje de **las mujeres no pudo denunciar porque solo hablaba su dialecto autóctono o no reconocían el lugar de trabajo de los hombres** quienes no está identificados con uniformes o sus vehículos carecen de distintivos.

Otra afectación contra las mujeres es la oferta laboral que llega a los territorios con los monocultivos, pues esta es de carácter masculino y en condiciones precarias, que están "muy ligadas a la idea de la fuerza". Según García esto ocasiona que las mujeres sean sometidas por sus parejas a malos tratos debido a la falta de independencia económica o se vean en la obligación de realizar trabajos mal remunerados como lavar la ropa de los trabajadores o ejercer como trabajadoras sexuales.

### **¿Quiénes son los responsables de estas violencias contra las mujeres?** 

García afirmó que en el informe se decidió no publicar los nombres de las empresas que son responsables de las violencias en contra de las mujeres, producto de sus agronegocios, debido a que **ni el Estado ni la comunidad internacional pueden garantizar la seguridad y protección de las comunidades.**

"Hemos detectado varios casos de comunidades que han denunciado o tienen procesos por estos hechos, y han sido amenazados de forma recurrente y sufrido hechos de violencia" manifestó García y agregó que además la alta presencia de estructuras armadas, sumado a la cantidad de empresas y de intereses que hay detrás de la tierra es difícil generar hechos más trascendentales.

Igualmente, García señaló que una de las complicaciones más fuertes a la hora de hacer denuncias, referente a violaciones al derecho laboral, tiene que ver con que las empresas están cambiando cada dos o tres años de razón social, **"es decir que cuando se va a iniciar un proceso, la empresa ya no existe".**

### **El Estado debe recordar que Vichada existe** 

Algunas de las recomendaciones que sale de este grupo de trabajo para superar la violencia en contra de las mujeres que se produce entorno a los agronegocios y monocultivos, tiene que ver con la presencia y accionar del Estado, **debido a que hay muy poca institucionalidad que garantice una vida digna a las comunidades**.

Asimismo, García aseveró que existe una gran responsabilidad de parte de las empresas en estos hechos, razón por la cual les sugieren generar diálogos con las comunidades para que comprendan las relaciones sociales que ya existen en el territorio con la naturaleza y los daños que causan las economías extensivas. (Le puede interesar: ["Casos de violencia sexual en el Catatumbo y Tumaco llegarán a la Comisión de la Verdad"](https://archivo.contagioradio.com/casos-de-violencia-sexual-en-catatumbo-y-tumaco-llegaran-a-comision-de-la-verdad/))

<iframe id="audio_30910951" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_30910951_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]

 
