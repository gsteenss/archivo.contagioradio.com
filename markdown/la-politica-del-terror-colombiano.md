Title: La política del terror colombiano
Date: 2015-03-18 16:41
Author: CtgAdm
Category: Opinion, superandianda
Tags: Alvaro Uribe, Paramilitarismo, superandianda
Slug: la-politica-del-terror-colombiano
Status: published

#### [**Por [Superandianda](https://archivo.contagioradio.com/superandianda/)- [~~@~~Superandianda](https://twitter.com/Superandianda) 

La política del terror colombiano es aquella que nos impide avanzar como sociedad unida, nos atemoriza a los grandes cambios sociales, y prevalece en la burocracia. Durante décadas el modelo del miedo ha servido para que sea la violencia la que sostenga la misma clase dirigente que se nutre económicamente de la guerra.

Una Colombia cansada de la violencia fue la que creyó que la guerra se resolvería con más guerra y que todos sus problemas sociales se terminarían con la mano firme que mantendría en el poder a Alvaro Uribe Velez durante 8 años.

El fundamento que haría del uribismo la nueva fuerza política más poderosa en Colombia tendría sentido mientras presentara resultados y demostrara lo efectiva que podría llegar a ser; de ahí llegarían las celebraciones por las bajas a guerrilleros, tal como si sus cabezas fueran trofeos y los grandes golpes militares que eran muy bien recompensados. Ya todos sabemos que esa competencia por cifras solo dejo miles de inocentes sacrificados sin justicia alguna hasta el día de hoy.

Ese periodo de sentirse orgulloso por la caída del enemigo, celebrar unas muertes pero indignarse por otras, dividirnos entre malos y buenos, las ganas de vengar la violencia con más violencia, es el periodo que le ha hecho igual o quizás más daño que 50 años de conflicto armado a nuestro país, no solo porque dejo en la impunidad crímenes de lesa humanidad y puso al estado como servidor del paramilitarismo, sino porque demostró que en 8 años las fuerzas militares con todo y resultados de bombardeos a campamentos no eliminaron a la guerrilla y mucho menos la lograron desmovilizar.

Aún con el fracaso de la seguridad democrática, la política del terror nos dice que tenemos que seguir manteniendo ese modelo porque de lo contrario estaremos desprotegidos, esa es la política que nos hace incapaces de creer y apostar por un verdadero escenario democrático donde trabajemos por la reparación social de las víctimas que nos dejó la guerra. Cuando señalo a las víctimas, no solo indico a las de la guerra armada sino a todo el pueblo colombiano que ha sido víctima de la guerra cultural que siembran los grandes medios de comunicación y el aparato burocrático, quienes parecen tener como único propósito perpetuar el miedo y la ignorancia a través de su poder legislativo y de mala información.

Somos el resultado de más de 50 años de conflicto armado y de todo un sistema diseñado para mantenerlo. Es entonces necesario sanar a un país herido culturalmente por la violencia y entender que los enemigos de la paz no solo son quienes llevan las armas sino los que mantienen la estructura social del miedo, la politica del terror al servicio del poder, a eso erróneamente lo llaman democracia.
