Title: Comunidades de Córdoba marchan contra asesinato de campesino a manos del ejército
Date: 2018-12-28 12:08
Author: AdminContagio
Category: DDHH, Nacional
Tags: Agresiones por parte del ejército, conflicto armado, lideres sociales, marcha pacifica
Slug: comunidades-de-cordoba-marchan-contra-asesinato-de-campesino-a-manos-del-ejercito
Status: published

###### [Foto: @QUELAPAZVIDA] 

###### 28 Dic 2018 

El pasado 27 de diciembre, las comunidades del corregimiento Juan José de Puerto Libertador, Córdoba se movilizaron para honrar la memoria de **Luis Eduardo Garay** presuntamente asesinado por miembros del Ejército y exigir justicia en este y en los muchos casos en los que los actores del conflicto armado han atentado contra la población civil.

Con banderas y prendas blancas, una concurrida marcha organizada por la **Asociación de Campesinos del sur de Córdoba** (ASCSUCOR) se desplazó para sepultar al campesino asesinado el pasado 24 diciembre quien mientras atravesaba un retén militar habría muerto a manos de integrantes del **Batallón Rifles, perteneciente a la Brigada Once.** [(Le puede interesar Denuncian asesinato de campesino a manos del ejército en Córdoba)](https://archivo.contagioradio.com/denuncian-asesinato-de-campesino-por-miembros-del-ejercito-en-cordoba/)

Una marcha similar ya se había realizado el 29 de junio de este año cuando la comunidad se movilizó en rechazo del asesinato de **Iván Lázaro, líder social** que murió cuando hombres armados ingresaron a su habitación y lo atacaron, Lázaro, integrante de ASCSUCOR, además era secretario de la Junta de Acción Comunal de la vereda Rogero y fomentaba procesos deportivos y culturales en su comunidad.

Ambos hechos permanecen en la memoria de las comunidades las cuales una vez más se unieron para expresar su total rechazo en contra de los continuos ataques y amenazas en la región por parte de los grupos al margen de la ley y  abusos de la fuerza pública, la cual como han destacado sus pobladores, pesar de hacer fuerte presencia en el municipio, no garantiza su seguridad.

###### Reciba toda la información de Contagio Radio en [[su correo]
