Title: ZIDRES legalizan el acaparamiento de tierras en Casanare
Date: 2016-01-28 18:05
Author: AdminContagio
Category: Nacional, Resistencias
Tags: Juan Manuel Santos, Zidres, ZIDRES Casanare
Slug: zidres-legalizan-el-acaparamiento-de-tierras-en-casanare
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/01/Acaparamiento-de-tierras.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

######  [Foto: Prosalus] 

<iframe src="http://www.ivoox.com/player_ek_10246297_2_1.html?data=kpWflpuWfZihhpywj5WUaZS1kZWah5yncZGhhpywj5WRaZi3jpWah5yncbu9pbeytZDQqcjVzc7nw9OPqc2fwsjO0sbWpc7dxtPh0ZDIqYzoysrf1MbXb8bijKjO1cbSpdOhhpywj6jTstXVyM7cjbfFqMrjjJKSmaiReA%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Alejandro López, Polo Democrático] 

###### [28 Ene 2016] 

Frente al acto de sanción protocolario del proyecto de Ley de ZIDRES anunciado por el presidente Juan Manuel Santos, que se llevará a cabo este viernes en el municipio de Orocué, campesinos de la ‘Asociación por la Defensa de Casanare’, denuncian los **impactos ambientales y sociales que esta ley traerá para sus territorios**.

De acuerdo con Alejandro López, concejal por el Polo Democrático del municipio de Paz de Ariporo e integrante de la Asociación, esta ley pretende **despojar al campesino casanareño de sus tierras a cambio de permitir el acaparamiento por parte de transnacionales**, a través del arrendamiento o la compra de extensiones de tierra hasta de 100 mil hectáreas para la implementación de proyectos agroindustriales de palma, sorgo y maíz, principalmente.

“No es que le tengamos miedo a que Colombia se la juegue por el agro, **lo que no compartimos ni aceptamos es que el Gobierno vaya a subsidiar grandes proyectos de multinacionales** y que [[no apoye al pequeño campesino](https://archivo.contagioradio.com/ley-zidres-para-acaparadores-de-tierras-afecta-territorio-colectivos/)], ni le dé el acceso a la tierra”, asevera el Concejal e insiste en que es un error pretender implementar las ZIDRES cuando en Casanare no se ha solucionado el problema de titulación de baldíos.

En los últimos 20 años al INCODER le han sido enviadas **7.983 solicitudes de adjudicación de baldíos sobre 1 millón 8oo mil hectáreas en el Casanare**, y el problema persiste pues son muchas las familias que aún esperan su titulación, por lo que causa indignación que “de la noche a la mañana metan grandes proyectos agroindustriales cuando **no se le soluciona al campesino el problema del acceso a la tierra**”, como asevera López.

Otra de las [[críticas de las familias campesinas](https://archivo.contagioradio.com/campesinos-de-casanare-defienden-el-agua-y-dicen-no-a-ley-zidres/)] al proyecto de ley de las ZIDRES tiene que ver con que mientras la ley 160 de 1994 reglamenta que la Unidad Agrícola Familiar UAF sobre la que cualquier campesino tiene derecho de propiedad debe oscilar entre 30 y 80 hectáreas, la ley de ZIDRES posibilita que los **grupos empresariales acaparen hasta más de 100 mil hectáreas para implementar monocultivos agroindustriales**.

Según indica este líder campesino la implementación de más proyectos agroindustriales en la región incrementaría las afectaciones ambientales y sociales, pues se **ponen en riesgo ecosistemas estratégicos para especies animales y vegetales**, así como las fuentes hídricas que son vitales para la supervivencia de las comunidades.

“No nos hicieron una consulta, nunca nos dijeron, nunca nos socializaron y sencillamente **de manera arbitraria radican el proyecto de ley que mañana se ufanaran de firmar protocolariamente como el mejor proyecto productivo para los llanos orientale**s”, asevera el Concejal quien concluye  que ante la necesidad de defender los derechos de los casanareños y sobre todo de los campesinos, líderes comunales y algunas fuerzas políticas de la región han conformado esta Asociación que cuenta con comités de trabajo para tratar de [[hacer frente a leyes tan nocivas](https://archivo.contagioradio.com/polo-partido-verde-y-cumbre-agraria-radican-demanda-contra-zidres/)] como la de las ZIDRES.

###### [Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)]] 
