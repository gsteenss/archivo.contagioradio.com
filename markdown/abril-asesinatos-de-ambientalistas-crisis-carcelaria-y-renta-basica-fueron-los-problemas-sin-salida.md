Title: Abril: Asesinatos de ambientalistas, crisis carcelaria y renta básica negada
Date: 2020-12-29 10:33
Author: CtgAdm
Category: Actualidad, Especiales Contagio Radio, Nacional, Resumen del año
Tags: abril, asesinatos de ambientalistas, asesinatos de líderes sociales, crisis carcelaria, Renta básica, Resumen de noticias
Slug: abril-asesinatos-de-ambientalistas-crisis-carcelaria-y-renta-basica-fueron-los-problemas-sin-salida
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/12/disenos-para-notas-3.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

El mes de Abril, estuvo atravesado por un **aislamiento estricto en casi todo el territorio nacional** lo que conllevó la angustia de millones de familias, cerca del 60% de la población. que sostienen su economía en la informalidad. **Desde el congreso, la llamada bancada de oposición, comenzó a impulsar la propuesta del Mínimo Vital o Renta Básica**. Propuesta que, aunque tuvo gran acogida en la ciudadanía que la respaldó desde el primero momento, encontró una fuerte oposición tanto en el gobierno como en sus bancadas en el congreso, reforzadas por la llegada de Cambio Radical a esta coalición.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

**El asesinato de Alejandro Llinas, reconocido ambientalista** de la Costa Caribe colombiana y del Parque Natural Tayrona, marcó el punto de inflexión que luego convirtió a **Colombia en uno de los países más peligroso para defensores ambientales según lo señaló el informe de Global Witness.** Sobre el parque Tayrona cabe recordar dos cosas: Para Abril se iniciaba un **conflicto con la multinacional Daabon por la invasión de esta reserva natural con cultivos de plátano** que luego era exportado como producto orgánico y ambientalmente amigable. Adicionalmente en este territorio sigue avanzando el control por parte de estructuras del narcotráfico y el paramilitarismo que no han encontrado oposición por parte de las fuerzas militares.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por si fuera poco, el desinterés y la incapacidad del gobierno se siguió viendo aumentada por **la crisis carcelaria que no tiene aún una solución de fondo**. Las internas de la cárcel del Buen Pastor en Bogotá, seguían denunciando que **no contaban con ninguna garantía de salubridad** y que la medida de prohibir las visitas no sirvió de nada en el objetivo de frenar el COVID 19, pues el personal del INPEC y de trabajo del centro de reclusión seguían entrando y saliendo sin mayor protección poniendo en riesgo la salud de la población reclusa.

<!-- /wp:paragraph -->

<!-- wp:heading -->

El Mínimo para la Vida que propone la bancada de oposición
----------------------------------------------------------

<!-- /wp:heading -->

<!-- wp:paragraph -->

Este martes 28 de abril, congresistas de la bancada de oposición presentaron su propuesta de «Mínimo para la vida» una propuesta dirigida a un sector de la ciudadanía en el marco de la pandemia, que consiste en aportar un mínimo vital del que serían beneficiados más de 30 millones de colombianos en condiciones de vulnerabilidad económica, mientras dure la emergencia sanitaria y por tres meses más.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Las bancadas proponen que el **Mínimo para la Vida **tomé como base el salario mínimo, es decir 877 mil pesos, y se agregue un «incremento marginal de acuerdo con la cantidad de personas que conforman el hogar, las variables de vulnerabilidad y un enfoque étnico y de género», llegando a un tope máximo de 2.200.000.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Ver más: ([El Mínimo para la Vida que propone la bancada de oposición - Contagio Radio](https://archivo.contagioradio.com/la-propuesta-del-minimo-para-la-vida-de-la-bancada-de-oposicion/))

<!-- /wp:paragraph -->

<!-- wp:heading -->

Durante sepelio de Alejandro Llinás amenazan a sus amigos y familiares
----------------------------------------------------------------------

<!-- /wp:heading -->

<!-- wp:paragraph -->

Familiares y amigos de líder social Alejandro Llinás, asesinado el pasado 24 de abril, denuncian que mientras se encontraban en la funeraria para iniciar el sepelio recibieron una llamada en donde los amenazaban de muerte, además les habrían asegurado que personas extrañas estarían en las honras fúnebres haciendo registro fotográfico de quienes participaran.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Según una de las personas víctima de los hechos, detrás de estos hostigamientos estarían los mismos intereses de quienes asesinaron al líder. Asimismo, considera que hay una intención de evitar que se conozca **«la verdad sobre todo el orden de terror que hay en la Sierra Nevada de Santa Marta»**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Ver más: ([Durante sepelio de Alejandro Llinas amenazan a sus amigos y familiares - Contagio Radio](https://archivo.contagioradio.com/durante-sepelio-de-alejandro-llinas-amenazan-a-sus-amigos-y-familiares/))

<!-- /wp:paragraph -->

<!-- wp:heading -->

Mujeres detenidas en cárcel Picaleña exigen protocolos contra Covid 19
----------------------------------------------------------------------

<!-- /wp:heading -->

<!-- wp:paragraph -->

Un grupo de once mujeres reclusas, que fueron trasladadas de forma arbitraria de la cárcel de Buen Pastor, en Bogotá, a la cárcel de Picaleña en Ibagué, alertan sobre las pésimas condiciones en las que se encuentran y el latente temor de un contagio en un centro penitenciario **que no cuenta con protocolos de protección ante el Covid 19.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

De acuerdo con las mujeres, el traslado se produjo el pasado el pasado 24 de marzo, cuando ya todo el país se encontraba en cuarentena. Además, en el vehículo usado para ese movimiento, también fueron transportados otros reclusos de la cárcel La Modelo y La Picota sin ninguna medida de Bioseguridad.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Ver más: ([Mujeres detenidas en cárcel Picaleña exigen protocolos contra Covid 19 - Contagio Radio](https://archivo.contagioradio.com/mujeres-detenidas-en-carcel-picalena-exigen-protocolos-contra-covid-19/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->
