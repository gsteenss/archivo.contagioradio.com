Title: Decreto de Duque para prohibir dosis mínima es una persecución a consumidores de drogas
Date: 2018-10-01 14:32
Author: AdminContagio
Category: DDHH, Movilización
Tags: Dosis Mínima, Fuerza Pública, Iván Duque, Prohibición
Slug: decreto-de-duque-para-prohibir-dosis-minima-es-una-persecucion-a-consumidores-de-drogas
Status: published

###### [Foto: Contagio Radio] 

###### [01 Oct 2018] 

Nada "novedoso" tiene el nuevo decreto del presidente Duque con el que se decomisaría la dosis mínima a los consumidores, así lo afirmaron organizaciones sociales que advirtieron que este trámite puede salir costoso, es una medida populista, **podría generar estigmatización hacia los usuarios y abusos de autoridad por parte de la Fuerza Pública**.

Felipe Cruz, integrante del grupo de investigación sobre drogas, en la organización Dejusticia, afirmó que "este decreto es una medida que tiene muchos problemas conceptuales y técnicos". En primer lugar, la medida respondería a una promesa de campaña por parte de **Duque que busca prohibir una actividad que está permitida constitucionalemente.**

Además, Cruz señaló que desde el 2016 en el Código de Policía ya se tenían facultades para decomisar sustancia, imponer multas a quienes la portaran en espacios públicos y en la medida legal, se establecen **cantidades para diferenciar a microtráficantes de consumidores**. Razón por la cual, este decreto no tiene nada de “novedoso”.

En segundo lugar, el especialista aseguró que Duque está enviando un mensaje a los agentes de Policía para re empoderarlos, hecho que podría generar abusos de poder y estigmatización hacia los consumidores, “cuando se da órdenes a la Policía y se le imponen objetivos ambiciosos, como controlar el microtráfico, y no hay una formación en derechos humanos, básicamente se envía un mensaje de represión, **que es peligroso, porque puede llegar a ser violación a los derechos fundamentales**”.

De igual forma, señaló que este decreto podría dar paso a una casería de brujas y a una persecución de consumidores de marihuana, cocaina y bazuco, impactando los derechos de los usuarios que no todos son adictos.

En tercer lugar, el decreto podría salir costoso debido a que se aumentarían los recursos para combatir a los consumidores. Actualmente la destinación a la política antidrogas corresponde al 4.1% del presupuesto general  y solamente un 1% de ese monto va a la prevención. (Le puede interesar:[ "Aspersión terrestre con glifosato es absurda: Camilo González Posso"](https://archivo.contagioradio.com/aspersion-terrestre-con-glifosato-es-absurda-camilo-gonzalez-posso/))

**NI PERMISO DE LOS PADRES NI DE LOS DOCENTES**

Asimismo, Cruz señaló que en el decreto que firmaría Duque, o por lo menos en el borrador, no se menciona la autorización por parte de padres de familia o docentes para garantizar que no se les decomise la droga a los usuarios y aseguró que una declaración extrajuicio de otra persona, autorizando el consumo de una sustancia sicoactiva, sitúa a quienes consumen como adictos o enfermos.

**¿QUÉ DE NOVEDOSO DEBERÍA TENER UNA POLÍTICA DE DROGAS EN COLOMBIA?**

Cruz afirmó que, sí el presidente Duque quisiera hacer algo diferente con la problemática de las drogas, debería crear un mecanismo especial o generar protocolos al interior de la Policía en el proceso de las incautaciones y no seguir el camino de la prohibición, debido a que esto solo provoca que** “el Estado se haga el de las gafas, no regule el mercado y en cambio este sea controlado por los actores ilegales o criminales”.**

Adicionalmete, informó que desde hace 5 años no se tiene información y datos frente a qué tipo de consumo se está generando en el país, cuál es el aumento de las personas consumidoras o cuál es la magnitud de esta situación. (Le puede interesar: ["Gobierno Duque esta en absoluta ignorancia sobre la política de drogas"](https://archivo.contagioradio.com/gobierno-duque-ignorancia-drogas/))

<iframe id="audio_29005961" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_29005961_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
