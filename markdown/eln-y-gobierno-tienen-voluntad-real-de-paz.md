Title: ELN y gobierno tienen voluntad real de paz
Date: 2016-02-08 13:23
Category: Entrevistas
Tags: ELN, negociaciones de paz
Slug: eln-y-gobierno-tienen-voluntad-real-de-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/02/ELN1-e1454955245567.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Notimérica 

<iframe src="http://www.ivoox.com/player_ek_10359559_2_1.html?data=kpWgl56ZeZqhhpywj5aUaZS1kZ6ah5yncZOhhpywj5WRaZi3jpWah5yncabAr5DmjczTpsrZ09PcjdnNqc%2FZz5Dj0dHZstXVxZDfx8bQb8XZjNXO3JKJe6ShpNTb1sbLrdCfs8bRy9SPcYarpJKh&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Víctor de Currea Lugo] 

###### [8 Feb 2016] 

**“De parte del gobierno si hay una voluntad real de paz y de parte del ELN también la hay”**, dice el profesor de la Universidad Nacional, Víctor de Currea Lugo, asegurando que pese a los últimos hechos entre el ELN y el gobierno nacional que tienen estancado el inicio de conversaciones en su fase pública, “no puede haber espacio para el pesimismo”.

Las tensiones se dan en medio de la retención del soldado Ejército Jair de Jesús Villar Ortiz, capturado el pasado miércoles, y tras las declaraciones del presidente Juan Manuel Santos quien señaló que las fuerzas militares van a actuar con “contundencia” y con “la ley” contra la guerrilla, añadiendo **“No vamos a bajar la guardia. Todo lo contrario: vamos a redoblarla”.** Tras esas declaraciones, la guerrilla del ELN, insistió el domingo a través de su cuenta en Twitter sobre la necesidad de iniciar un cese bilateral del fuego para propiciar un “clima favorable”.

Frente a esta situación, el profesor de la Universidad Nacional, hizo un llamado de atención sobre los **“problemas de comunicación que hay en el país”, pues su lectura es que la sociedad siente “que toda es culpa del ELN”**, pero este percepción de los colombianos se debe a que esa guerrilla “no ha sido capaz de informar a la sociedad sobre su propia agenda”.

Para Víctor de Currea, será clave la implementación de los acuerdos que se firmen en la Habana, para dar mayor seguridad al ELN, a su vez, sostiene que se trata de dos mesas de negociaciones y un solo proceso, pero “no se puede plantear que basta con maquillar dos cosas para que el ELN se suba a lo acordado con las FARC. **El ELN merece una mesa propia por su dinámica política”, expresa.**

Cabe recordar que por el momento se conoce que la agenda con el ELN estaría compuesta por los puntos ‘Participación de la sociedad’, ‘Democracia para la paz’, ‘Víctimas’, ‘Fin del conflicto armado e implementación de los  acuerdos y refrendación’ y ‘Transformaciones Sociales’.

Finalmente, el profesor concluye que pese a la coyuntura, este momento es clave para la paz de Colombia, y se tiene una oportunidad política que el país no puede desaprovechar, "Hay que jugársela por la paz" ya que **"La paz sin el ELN, es una paz incompleta y con riesgos".**

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
