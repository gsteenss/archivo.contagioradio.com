Title: 500 empresas y naciones son responsables del 70% de la deforestación mundial
Date: 2015-02-14 08:43
Author: CtgAdm
Category: Ambiente, Otra Mirada
Tags: Avon, cambio climatico, Caquetá, colombia, deforestación, Johnson &amp; Johnson, medio ambiente, Nestlé y Pepsico, ONG, Unilever
Slug: 500-empresas-y-naciones-son-responsables-del-70-de-la-deforestacion-mundial
Status: published

##### [Foto: www.ecoosfera.com]

**Nestlé y Pepsico, Johnson & Johnson, Avon, Unilever** y los bancos HSBC y Santander, entre otras compañías, hacen parte de la lista de las **500 empresas y países** que más contribuyen a la deforestación en el mundo.

Se trata de la iniciativa **Forest 500, propuesta por la ONG Global del Dosel** , donde se identifican a los actores responsables del 70% de la tala de árboles del planeta, con el objetivo de llamar la atención de las compañías y países, para que actúen desde ya a favor de la naturaleza.

**Los bosques cubren alrededor del 30% de las regiones del mundo,** sin embargo, de acuerdo la ONG,  factores como la ganadería, el sector maderero, la pasta de papel y los extensos cultivos de soya y aceite de palma, entre otros; se genera que al año se pierden cantidades tan grandes de bosques del tamaño de Panamá.

Colombia se encuentra entre la lista de los 500 responsables de la deforestación, especialmente por el caso del departamento del **Caquetá, donde se presenta con mayor frecuencia esta problemática, causado por la palma de aceite y los fines ganaderos.**

Diez  de las empresas peor calificadas son de **China, Rusia e Indonesia**, y las multinacionales con la máxima puntuación (cinco puntos) entre los 500 son: Nestlé, Unilever, Grupo Danone y Kao Corp. [(Ver la lista completa del Forest 500)](http://forest500.org/)
