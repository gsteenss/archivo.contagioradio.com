Title: Empresas y gobiernos son los causantes del cambio climático según el Papa
Date: 2015-06-18 13:38
Category: Ambiente, Entrevistas
Tags: cambio climatico, consumo, contaminación, encíclica, multinacionales, Papa Francisco, política mineroenergética, Vaticano
Slug: empresas-y-gobiernos-son-los-causantes-del-cambio-climatico-segun-el-papa
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/06/papa.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: [bancaynegocios.com]

###### [18 de junio 2015]

Este jueves se publicó la encíclica del Papa Francisco sobre la temática ambiental, donde **acusa a las multinacionales y los gobiernos de ser causantes del cambio climático**, debido al “el uso desproporcionado de los recursos naturales”.

191 páginas hacen parte de la encíclica del Papa, titulada **Laudato Si,** donde su primer capítulo llamado *Lo que está pasando en nuestra casa*, implica un duro análisis a las actividades del ser humano en la tierra, que finalmente es lo que ha llevado a que se estén viviendo tiempos difíciles en materia ambiental.

Francisco aborda temas como la contaminación y el cambio climático, la mala gestión del agua, la pérdida de la biodiversidad, la gran desigualdad entre regiones ricas y pobres o la debilidad de las reacciones políticas ante los daños ambientales, donde señala directamente a los culpables.

Pero el Papa no solo llamó la atención de los gobiernos y empresas, sino también de los ciudadanos del mundo, teniendo en cuenta que se refirió a conceptos como el **"consumismo inmoral"** que precisamente ha llevado a la sociedad a un comportamiento que permite la degradación continua de la naturaleza. Así mismo, asegura que **“la tecnología basada en combustibles fósiles muy contaminantes -sobre todo el carbón, pero aún el petróleo y, en menor medida, el gas- necesita ser reemplazada progresivamente y sin demora**" advirtió.

Por otro lado, habló sobre el tema de la desigualdad, donde resaltó que los países desarrollados o industrializados deben apoyar a las naciones más pobres del mundo, ya que son ellos quienes explotan los recursos naturales de esas naciones “tercermundistas”, lo que calificó como una estrategia por parte de los países del primer mundo como **"estructuralmente perversa".**

Finalmente, en su encíclica, el Papa propone regulaciones a nivel gubernamental para frenar el calentamiento global, y también instituciones eficientes y organizadas con la potestad de sancionar a quienes incumplan las normas.

Así mismo, hizo especial énfasis, en la necesidad de realizar un **cambio radical de estilo de vida de los habitantes del planeta**, con el objetivo de frenar la contaminación del ambiente, por lo que el sumo pontífice asegura que “**cada vez más (la tierra) es un inmenso depósito de porquería”**. Y rechazó el argumento de que solo a través del crecimiento económico se puede solucionar el hambre, la pobreza y el daño ambiental.

[LaudatoSi](https://es.scribd.com/doc/269004468/Laudato-Si-ES "View LaudatoSi on Scribd") by [Contagioradio](https://www.scribd.com/contagioradio "View Contagioradio's profile on Scribd")

<iframe id="doc_43612" class="scribd_iframe_embed" src="https://www.scribd.com/embeds/269126456/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-qX1zfv43AEXAjIseEMT2&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="0.5655994978028877"></iframe>
