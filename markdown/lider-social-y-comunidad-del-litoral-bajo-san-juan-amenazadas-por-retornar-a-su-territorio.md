Title: Líder Social y comunidad del Litoral Bajo San Juan amenazadas por retornar a su territorio
Date: 2018-02-06 15:17
Category: DDHH, Nacional
Tags: Litoral Bajo San Juan
Slug: lider-social-y-comunidad-del-litoral-bajo-san-juan-amenazadas-por-retornar-a-su-territorio
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/02/coliseo-770x400.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [06 Feb 2017] 

El líder social y defensor de derechos humanos, Dagoberto Pretel y la comunidad del litoral del Bajo San Juan, en Chocó, que actualmente se encuentra desplazada en el Coliseo de Buenaventura, fueron amenazados el pasado 4 de febrero, por un hombre que les afirmó **que tanto la vida del líder como la de la comunidad “corrían peligro” debido a sus intenciones de retornar al territorio.**

La amenaza se realizó vía telefónica al número de Pretel, quien manifestó que, pese a que intentó que el hombre le diera su nombre o que señalara si la intimidación venía por parte de un grupo armado, no se logró establecer quienes estarían detrás de este hecho. (Le puede interesar: ["Un mes y medio completan 400 familias desplazadas en el San Juan Litoral"](https://archivo.contagioradio.com/un-mes-y-medio-completan-400-familias-desplazadas-del-san-juan-del-litoral/))

### **En vilo el retorno** 

El retorno de la comunidad, que actualmente se encuentra en el coliseo de Buenaventura, se iba a realizar el próximo 15 de febrero, sin embargo, debido a esta amenaza, este martes se llevará a cabo un consejo comunitario en donde **se evaluará la seguridad y las garantías para las más de 400 familias que planean regresar a su territorio**.

“Nosotros nos encontramos indefensos, estamos rodeados de la Infanteria de Marina, pero eso no **quiere decir que estamos blindados, la seguridad que necesitamos es en el territorio**” afirmó Pretel, y añadió que esta situación está en conocimiento de todas las autoridades del país. (Le puede interesar: ["3.000 personas se encuentran confinadas en el Bajo San Juan Litoral"](https://archivo.contagioradio.com/3-000-personas-se-encuentran-confinadas-en-el-bajo-san-juan-litoral/))

Así mismo señaló que, aunque la Infantería de Marina, ha asegurado que hay normalidad en el territorio del Bajo San Juan, las amenazas demuestran que no es cierto, razón por la cual afirmó que la Fuerza Pública y las autoridades deben encontrar un protocolo de seguridad que parta del enfoque diferencial como pueblo afro e indígena, para garantizar la protección a la comunidad y los líderes sociales.

Desde el momento de la llegada de la comunidad a Buenaventura han recibido dos amenazas: la primera fue el año pasado, en donde también se nombró a Dagoberto Pretel. De igual forma la comunidad había denunciado la desaparición de Jefferson Pretel, miembro del consejo comunitario, y quien apareció tres días después muerto en una vereda en cercanías a la ciudad de Cali.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
