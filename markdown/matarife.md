Title: Denuncian censura contra la serie “Matarife”
Date: 2020-07-27 22:14
Author: PracticasCR
Category: Actualidad, Nacional
Tags: Álvaro Uribe, Censura, Daniel Mendoza, Matarife
Slug: matarife
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/07/Daniel-Mendoza-creador-de-Matarife.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"textColor":"cyan-bluish-gray","fontSize":"small"} -->

Daniel Mendoza creador de la seria "[Matarife](https://twitter.com/matarifeco?ref_src=twsrc%5Etfw%7Ctwcamp%5Etweetembed%7Ctwterm%5E1287024659750367234%7Ctwgr%5E&ref_url=https%3A%2F%2Fwww.telesurtv.net%2Fnews%2Fcolombia-redes-sociales-rechazo-posible-censura-matarife-20200725-0013.html)"

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Este fin de semana varios ciudadanos a través de redes sociales se pronunciaron en contra de lo que para ellos fue una censura de la serie "Matarife" creada por el abogado y periodista Daniel Mendoza, la cual retrata los escándalos que rodean la vida del senador y expresidente Álvaro Uribe Vélez, a partir de diversa documentación periodística y judicial recopilada. (Le puede interesar: [Razones por las que Diego Cadena, abogado de Álvaro Uribe, será imputado por la Fiscalía](https://archivo.contagioradio.com/razones-por-las-que-diego-cadena-abogado-de-alvaro-uribe-sera-imputado-por-la-fiscalia/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

La noche del viernes, espacio habitual en el que la serie se emite, se esperaba la publicación del capítulo séptimo a través de la plataforma Youtube pero este nunca salió al aire.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Frente a esto **los creadores de Matarife manifestaron que la emisión de dicho capítulo no se llevó a cabo porque según ellos fue censurado** y promovieron el  hashtag: \#CensurarMatarifeEs en la red social Twitter.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/ElQueLosDELATA/status/1286828298564243458","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/ElQueLosDELATA/status/1286828298564243458

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/HELIODOPTERO/status/1286858311241998343","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/HELIODOPTERO/status/1286858311241998343

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

Fue tal la acogida que tuvo el hashtag en Twitter que ***«\#CensurarMatarifeEs*» llegó a convertirse en primera tendencia a nivel mundial** en la noche del viernes.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### "Matarife" también se enfrenta a un litigio jurídico

<!-- /wp:heading -->

<!-- wp:paragraph -->

El expresidente Álvaro Uribe, presentó a través de su abogado Abelardo de la Espriella una tutela en contra de la serie "Matarife" y sus realizadores, alegando que dicha producción vulneraba sus derechos a la «*honra y el* *buen nombre*». No obstante, **el Juzgado 23 Civil de Bogotá que conoció la tutela en primera en primera instancia, decidió «*negar por improcedente*» la acción presentada por el hoy senador Uribe.**

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/migueldelrioabg/status/1280650501982470144","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/migueldelrioabg/status/1280650501982470144

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

Posteriormente, **el Tribunal Superior de Bogotá revocó la decisión tomada por el Juzgado 23, argumentando que este despacho no era el competente para decidir sobre la tutela**, es decir, invalidó el fallo por un aspecto formal pero no por el fondo mismo de la decisión.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Lo anterior, llevó a que el proceso se reiniciara con la remisión del expediente al J**uzgado 29 Civil Municipal de Bogotá quien será quien entrará a decidir nuevamente la acción presentada por el expresidente Uribe y su abogado.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Pros y Contras de impedir que "Matarife" sea emitida

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Frente a este debate están quienes afirman que la serie es una verdad necesaria que debe ser recogida para continuar y, de alguna manera, sistematizar el ejercicio de memoria que está haciendo el país de la mano de organizaciones de DDHH y de víctimas.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Sin embargo otros sectores han afirmado que aunque hay que hacer un trabajo para que se acabe la zozobra legal y la impunidad en torno a las acusaciones contra Alvaro Uribe Vélez, este tipo de acciones promueven la polarización y personalizan el debate cuando lo que se debe trabajar de fondo es un sistema que sigue estando presente y que con o sin la figura central ha venido funcionando en el país.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Ese sistema ha seguido produciendo víctimas, posibilitando el control territorial de estructuras familiares como los clanes en la costa y el valle, el control por parte de narcotraficantes y de tipo paramilitar de regiones olvidadas por el Estado pero aprovechadas por los gobiernos locales, incluso el sistema financiero del pais.

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
