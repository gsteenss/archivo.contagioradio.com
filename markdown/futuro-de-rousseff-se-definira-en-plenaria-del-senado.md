Title: Futuro de Rousseff se definirá en plenaria del Senado
Date: 2016-05-06 15:12
Category: El mundo, Política
Tags: Dilma Roussef impeachment, Golpe de estado Brasil
Slug: futuro-de-rousseff-se-definira-en-plenaria-del-senado
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/05/brazil-dilma-1024x682.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### Foto: Cmi 

##### [06 May 2016] 

Con un total de **15 votos a favor y 5 en contra**, la Comisión especial senatorial encargada de evaluar la solicitud de impeachment sobre la presidenta Dilma Rousseff, dio visto bueno al **informe presentado por el relator Antonio Anastasia**, recomendado destitución de la  mandataria brasileña.

Los únicos votos en desaprobación al proceso, correspondieron al Partido de los trabajadores (PT), al Partido Comunista de Brasil (PCdoB) y al Partido Democrático Laborista, los diputados de los partidos restantes, entre disidentes y opositores, votaron favorablemente.

Aunque consulta se realizó de manera electrónica, los líderes de los partidos tuvieron la posibilidad de manifestarse justificando su decisión, "Lo que está sucediendo aquí es increíble, es una pena. Ellos (la oposición) **defienden la destitución desde que la presidenta fue elegida**", aseguro el senador Lindbergh Farias por el Partido de los Trabajadores.

El representante califica el proceso como un intento de "**criminalizar una política anticíclica**", asegurando que su partido sale "con la cabeza en alto de esta votación, porque quiere condenar a una política que ha sacado a millones de la pobreza. Presidenta Dilma la historia la absolverá".

Por su parte, el líder del gobierno en el Senado Humberto Costa, crítico que la elección de los miembros de la comisión se hiciera "a dedo", asegurando que en plenaria el resultado será diferente.

La decisión pasa ahora a debate en plenaria el próximo **miércoles 11 de mayo**, donde requerirá de la mitad más uno de los congresistas (41 votos) para ser aprobada, de ser así Rousseff será apartada de su cargo durante 180, en los cuales Michel Temer la remplazará temporalmente.
