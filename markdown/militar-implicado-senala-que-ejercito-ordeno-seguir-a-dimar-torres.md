Title: Militar implicado señala que Ejército ordenó seguir a Dimar Torres
Date: 2019-05-03 17:09
Author: CtgAdm
Category: Comunidad, Nacional
Tags: Catatumbo, Dimar Torres, Ejecuciones Extrajudiciales, Ejército Nacional
Slug: militar-implicado-senala-que-ejercito-ordeno-seguir-a-dimar-torres
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/WhatsApp-Image-2019-05-03-at-5.27.59-PM.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: @BenedictoFarc] 

**Daniel Eduardo Gómez Robledo**, cabo señalado de ser el responsable del homicidio del excombatiente  de Farc, Dimar Torres, reveló durante la audiencia de imputación de cargos que tenía ordenes de realizar labores de seguimiento al campesino, tarea que venía realizando 15 días atrás y que desencadenó en el asesinato de Dimar el pasado 22 de abril en Catatumbo.

Durante la audiencia y como parte de la investigación se pudo establecer que  fue en el cumplimiento de esta función que el cabo Gómez interceptó y ordenó a Dimar detener su motocicleta de regreso a la vereda Campo Alegre para revisar sus objetos personales. [(Lea también: Señalan a Ejército Nacional de asesinar al excombatiente Dimar Torres)](https://archivo.contagioradio.com/excombatiente-dimar-torres-habria-sido-asesinado-por-ejercito-en-el-catatumbo/)

Por su parte, el comandante del Ejército, Nicasio Martínez, negó la versión de Gómez Robledo indicando que no tenía conocimiento que existiera alguna orden de seguimiento hacia Dimar y que será la Fiscalía la encargada de determinar la veracidad del hecho, agregando que los demás involucrados en el hecho han sido asignados a tareas administrativas mientras avanza el proceso. [(Lea también: nueva cúpula militar: ¿vuelve la seguridad democrática?)](https://archivo.contagioradio.com/nueva-cupula-militar-vuelve-la-seguridad-democratica/)

### **La familia de Dimar Torres pide justicia** 

Durante la audiencia de imputación de cargos a cargo del  Juzgado Segundo Penal de Garantías del circuito de Ocaña, **el padre de Dimar, José Manuel Torres de 74 años**, exhortó a que se hiciera justicia por la muerte de su hijo, exigiendo que al cabo Gómez le fuera impuesta una pena de al menos de 50 años, señalando que que con su muerte se ha causado gran daño a su  familia.

Dentro de la investigación que realiza la Fiscalía, también pudo establecerse, tal como ya se había manifestado, que Dimar actualmente se dedicaba a la actividad agrícola, sembrando yuca y plátano en un terreno que le había sido prestado por su hermano, una labor que desempeñaba como parte de su proceso de reintegración  y que pudo ser constatado por  la oficina del Alto Comisionado para la Paz.

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
