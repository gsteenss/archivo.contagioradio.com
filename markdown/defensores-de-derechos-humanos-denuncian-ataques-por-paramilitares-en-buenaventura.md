Title: Defensores de Derechos Humanos denuncian ataques por paramilitares en Buenaventura
Date: 2015-08-28 14:35
Category: DDHH, Nacional, Paz
Tags: buenaventura, defensores de derechos humanos, Denuncias, Justicia y Paz, Paramilitarismo
Slug: defensores-de-derechos-humanos-denuncian-ataques-por-paramilitares-en-buenaventura
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/06/buenaventura-gabriel.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Gabriel Galindo] 

###### [28 Ago 2015] 

María Eugenia Mosquera y Enrique Chimonja defensores de Derechos Humanos de la Comisión de Justicia y Paz, denunciaron que el día de ayer en Buenaventura fueron atacados por un hombre armado y otro hombre en una motocicleta, en el Barrio el Cristal donde hay presencia del grupo paramilitar “los Urabeños”.

El hecho se dio a conocer a la policía por parte del escolta de la Unidad Nacional de Protección.

Los integrantes de la Comisión de Justicia y Paz, acompañan a la comunidad indígena Woaunan, prestándoles asistencia humanitaria, jurídica y psicosocial, debido a que se encuentran en condición de desplazamiento hace 9 meses.
