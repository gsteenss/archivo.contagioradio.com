Title: “Todavía podemos parar la venta de ISAGEN”
Date: 2016-01-20 17:18
Category: Economía, Nacional
Tags: Brookfield, ISAGEN
Slug: todavia-podemos-parar-la-venta-de-isagen
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/01/protestas-Isagen.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [[Foto: E]l Pais.]

<iframe src="http://www.ivoox.com/player_ek_10143321_2_1.html?data=kpWelpiXdpKhhpywj5aYaZS1lZqah5yncZOhhpywj5WRaZi3jpWah5yncYa5k4qlkoqdh7Xjxcbjh6iXaaK4wpDd0cnJsdDnjNXO1MbWb83VjNvS0NnFb8XZjK7Ao6ypkoa5k4qlkoqdiI6ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Enrique Daza, Justicia Tributaria] 

###### [20 Ene 2016]

“Todavía podemos parar la venta de [ISAGEN](https://archivo.contagioradio.com/?s=isagen)”, aseguran los senadores **Jorge Enrique Robledo, Sofía Gaviria, Antonio Navarro, Viviane Morales, Iván Duque, junto a la Red de Justicia Tributaria, Sintraisagén** y otras organizaciones, que radicaron una acción popular que busca impedir la venta de ISAGEN, teniendo en cuenta que esta no se ha finiquitado ya que Brookfield no ha cancelado el valor por el que compró la empresa.

A la serie de demandas interpuestas que buscan suspender la venta de la empresa generadora de energía, se suma este documento de 37 páginas en las que se sostienen los argumentos por los cuales sería ilegal la venta de ISAGEN, cuyo  proceso de venta debería finalizarse el próximo 27 de enero, cuando la firma canadiense  desembolsaría los \$6,4 billones que ofreció.

Así mismo, de acuerdo con Enrique Daza, integrante de Justicia Tributaria, **“ha habido toda una manipulación y opacidad con respecto a la venta de ISAGEN”**, de manera que se interpondrá una nueva exigencia al Consejo de Estado para que se declare la nulidad de la venta, impidiéndose que Brookfield cancele el dinero.

Daza, dice que sería posible, que la compañía canadiense piense en retirarse del negocio, ya que “se encuentra en la mira de la opinión  pública, tiene una cantidad de irregularidades por lo que podría reconsiderar su participación dado el escándalo que se puede producir, y dada la forma como puede verse afectada su imagen y la estabilidad de sus inversores”.

[![6 razones que frenarian venta de Isagen](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/01/6-razones-que-frenarian-venta-de-Isagen.jpg){.aligncenter .size-full .wp-image-19403 width="1920" height="2505"}](https://archivo.contagioradio.com/todavia-podemos-parar-la-venta-de-isagen/6-razones-que-frenarian-venta-de-isagen/)

 [[Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u)  o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](https://archivo.contagioradio.com/).]]
