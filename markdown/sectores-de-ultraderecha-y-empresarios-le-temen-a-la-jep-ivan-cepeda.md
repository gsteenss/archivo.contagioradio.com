Title: "Sectores de Ultraderecha y empresarios le temen a la JEP" Iván Cepeda
Date: 2017-02-21 13:33
Category: Entrevistas, Judicial
Tags: Implementación de Acuerdos, jurisdicción especial para la paz
Slug: sectores-de-ultraderecha-y-empresarios-le-temen-a-la-jep-ivan-cepeda
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/11/Senado-colombiano.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo Particular ] 

###### [21 Feb 2017] 

**La corrupción, la falta de voluntad política y los intentos de partidos políticos como Cambio Radical y el Centro Democrático de frenar el avance de la Jurisdicción Especial para la paz**, son las nuevas trabas para la implementación de los Acuerdos de paz y para que prospere una paz sin impunidad en el país.

El senado Iván Cepeda, del Polo Democrático señaló que los flancos en los que se pretende modificar la Jurisdicción Especial para la Paz son claros: por un lado está el **intentar ocultar la participación de particulares en el conflicto armado** y por el otro, **diluir la responsabilidad de los militares que corresponderían por  la cadena de mando**.

Este último hecho ya ha recibido el llamado de atención por parte de la Corte Penal Internacional, sin embargo, sectores como la Asociación Colombiana de Oficiales del Estado en Retiro, ha sentado su **postura e inconformismo frente a la JEP y la cadena de mando.**[Le puede interesar: " Ley de Amnistía mazana de la discordia"](https://archivo.contagioradio.com/ley-de-amnistia-la-manzana-de-la-discordia/)

A su vez Cepeda indicó que el temor y la prevención hacia la Jurisdicción Especial para la Paz y a la Comisión de Esclarecimiento de la Verdad Histórica, radica en que **“se desate una importante cadena de investigaciones y de esclarecimiento con relación a los centenares de miles de hechos criminales que se han perpetrado** no solamente desde instancias estatales, sino también de acciones de paramilitares y con responsabilidad de particulares y empresariales”. Le puede interesar: ["Organizaciones sociales exigen justicia para todos en implementación de JEP"](https://archivo.contagioradio.com/organizaciones-sociales-exigen-justicia-para-todos-en-implementacion-de-jep/)

Temor que, para Cepeda, se funda en las solicitudes de condenados como Diego Palacio, ex Ministro de la Protección Social, el ex secretario de la Presidencia Alberto Velásquez y el ex Ministro Sabas Pretelt,  que ejercieron durante la presidencia de Álvaro Uribe Vélez y que están intentando entrar a ser juzgados en el Tribunal de Paz. **Los tres argumentarían que impulsar la reelección de Uribe era una política de Estado para acabar con las FARC.** Le puede interesar: ["Crece el riesgo para los acuerdos de paz"](https://archivo.contagioradio.com/crece-el-riesgo-para-los-acuerdos-de-paz/)

Con este panorama Cepeda insiste en que lo más importante de los debates sobre la Justicia Especial para la Paz es que se mantenga lo acordado en La Habana, **sin modificaciones que den paso a la impunidad.**

<iframe id="audio_17140185" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_17140185_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
