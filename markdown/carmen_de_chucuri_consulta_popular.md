Title: El Carmen de Chucurí seguirá en lucha para lograr consulta popular
Date: 2017-11-15 20:24
Category: Ambiente, Nacional
Tags: consulta popular, Ecopetrol, El Carmen de Chucurí
Slug: carmen_de_chucuri_consulta_popular
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/11/DSC_0008-e1510795323898.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: derechodelpueblo.blogspot.] 

###### [15 Nov 2017] 

Una vez más la supuesta falta de recursos, es el argumento que usa la Registraduría General de la Nación, para notificar al alcalde de El Carmen de Chucurí, de la suspensión de la consulta popular que **estaba programada para el próximo 10 de diciembre.**

Ante dicha situación, que nuevamente pone en jaque la participación de las comunidades, la comunidad del municipio, ha rechazado enfaticamente el anuncio de la la Registraduría, pues señalan que el mecanismo de participación  popular, superó todos los trámites jurídicos y políticos, ante el alcalde, el concejo municipal y el Tribunal Administrativo de Santander.

La pregunta en la consulta planteada era ¿está usted de acuerdo si o no que en la jurisdicción del municipio del Carmen de Chuchuri se realicen actividades de exploración y explotación mineras de carbón y de hidrocarburos?  Se trata de una pregunta que busca que no se repitan episodios como los que sucedieron en 2006, cuando un proyecto de **Ecopetrol ocasionó la destrucción de gran parte de la vereda Payoa a 22 kilometros del municipio Sabana de Torres,** debido a una gran explosión que derrumbó aproximadamente 20 viviendas.

Nini Johana Cardenas, integrante de uno de los comités promotores de la Consulta popular afirmó que los habitantes del Carmen de Chucurí están pensando en realizar una movilización pacífica para exigir el cumplimiento de sus derechos y reiteró que **"si el gobierno no nos da los recursos, el pueblo carmeleño pagará la Consulta popular".**

Ante la situación, las comunidades exigen que se brinden las garantías de igualdad para todas las consultas en el país , así como se hacen para que los partidos políticos escojan candidato. **De igual manera afirman que mantendrá los esfuerzos para seguir adelante con la consulta.**

<iframe id="audio_22120339" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_22120339_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
