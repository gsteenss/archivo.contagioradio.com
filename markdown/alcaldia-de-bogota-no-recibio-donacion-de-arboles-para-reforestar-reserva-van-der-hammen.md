Title: Alcaldía de Bogotá no recibió 2339 árboles para reserva Van Der Hammen
Date: 2017-08-08 13:10
Category: Ambiente, Nacional
Tags: Alcaldía de Bogotá, Enrique Peñalosa, reforestación reserva, Reserva Thomas Van der Hammen
Slug: alcaldia-de-bogota-no-recibio-donacion-de-arboles-para-reforestar-reserva-van-der-hammen
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/08/arboles-donados-3-e1502216025239.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Sembradores Van Der Hammen] 

###### [08 Ago 2017] 

En una jornada de donación de árboles para reforestar la reserva Thomas Van Der Hammen, **los bogotanos donaron más de 2339 árboles que no fueron recibidos por la administración de Enrique Peñalosa** para ser llevados y plantados a la reserva forestal en Bogotá. Ante ello la presión ciudadana recae sobre la Corporación Autónoma Regional de Cundinamarca- CAR que deberá recibir y sembrar las donaciones.

La jornada se realizó este Lunes **en el marco de la celebración del cumpleaños de Bogotá en la plaza de Bolívar**. Allí realizaron actividades pedagógicas para recibir los árboles, resolver las dudas de la ciudadanía frente al proceso de reforestación de la reserva e hicieron un reconocimiento de la flora nativa de la sabana de Bogotá. (Le puede interesar: ["Reserva Thomas Van Der Hammen debe mantenerse sin un centímetro de cemento"](https://archivo.contagioradio.com/reserva-thomas-van-der-hammen-debe-estar-sin-cemento/))

Sabina Rodríguez, integrante del Comité para la Defensa de la Reserva Thomas Van Der Hammen, manifestó que, desde **el 19 de julio le habían notificado a la Alcaldía de Bogotá sobre la realización de la jornada** para que ellos pudieran recibir los árboles en el cumpleaños de la capital. Sin embargo, Rodríguez fue enfática en manifestar que no obtuvieron respuesta alguna.

Ante esto, le hicieron la propuesta de recepción y traslado de los árboles a la CAR por se una entidad con jurisdicción sobre la reserva. **Aún están esperando la respuesta de la institución** y le han pedido a la ciudadanía que se sume a un ejercicio de apoyo a la solicitud por medio de las redes sociales y así lograr que los árboles sean plantados lo antes posible en la reserva. (Le puede interesar:["¿Qué le espera a la reserva Thomas Van Der Hammen?"](https://archivo.contagioradio.com/reserva-thomas-van-der-hammen-y-su-futuro/))

En este momento, el Comité se ha encargado de **mantener los árboles en las bolsas que fueron donados bajo constante supervisión y cuidado**. Rodríguez afirmó que “los árboles se encuentran en un patio grande donde están siendo regados y revisados por un grupo de voluntarios que han recibido tierra para volverlos a embolsar”.

Finalmente, desde el Comité han invitado a la **ciudadanía a que participe en la jornada de siembra que se va a realizar en la reserva**. “No se va a poder hacer una gran siembra pero con el compromiso de la ciudadanía no vamos a dejar que los árboles se queden en las bolsitas”.

<iframe id="audio_20229656" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_20229656_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
