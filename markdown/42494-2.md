Title: Para Voces de Paz el periodo legislativo fue "bueno pero insuficiente"
Date: 2017-06-21 13:04
Category: Entrevistas, Paz
Tags: Congreso, Fast Track, imelda daza, Legislación, voces de paz
Slug: 42494-2
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/05/Imelda-Daza.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo] 

###### [21 Jun. 2017] 

**"Bueno pero insuficiente" así calificó voces de paz al periodo legislativo que culminó** este martes en Colombia. Según voceros de esa agrupación ciudadana, si bien se lograron diversas aprobaciones de reformas constitucionales y de leyes vía ‘Fast Track’, el desinterés y la falta de compromiso de algunas bancadas para legislar por la paz impidió lograr un trabajo efectivo.

Para **Imelda Daza integrante del movimiento Voces de Paz,** el balance que puede hacerse es bueno, pero insuficiente para lo que se esperaba del Congreso “se avanzó sin duda y las leyes aprobadas, los actos legislativos aprobados son fundamentales para la implementación del Acuerdo de Paz”.

Daza aseguró que algunos avances fueron la ley de Aministía e Indulto para los integrantes de la guerrilla de las FARC, el Estatuto de Oposición, La JEP, la protección personal para líderes del nuevo partido político y el primer debate de las circunscripciones especiales para la paz “además del reconocimiento de Voces de Paz”.

### **Falta de interés del Gobierno en el Congreso** 

Tal y como lo denunciaron diversos movimientos de ciudadanos que se encargaron de la veeduría en las votaciones del Congreso y que dieron a conocer el abstencionismo por parte de los congresistas, falta de preparación para los debates, entre otros temas, Daza añadió que pudo **notar un “despotismo” de parte de la bancada del Gobierno.**

“Hubo desgano, quizás desinterés, poco esfuerzo por cumplir con la ley de amnistía e indulto” añadió Daza.

Y hace referencia a este tema, dado que por más que haya sido aprobada desde diciembre la ley que permitía la libertad de guerrilleros y guerrilleras en las cárceles de Colombia, en la actualidad no han alcanzado su libertad “ni la mitad de esos presos. **Hay desgano, desintereses, por cumplir, aplicar la ley de aministia e indulto** en la que una parte del sector judicial está interesado en que esto no avance” hincó Daza.

### **El Fiscal, una piedra en el zapato** 

Así calificó Daza a Néstor Humberto Martínez luego de conocer que el Fiscal asegura tener listas las órdenes de captura y de extradición para miembros de las FARC “es decir ¿qué se pretende con eso? ¿decirle a las FARC no den el paso, no avancen más, devuélvanse pal monte? se pregunta".

Y añade que "es inexplicable la falta de coherencia en la institucionalidad colombiana para manejar este proceso de paz. Unos van con la guerra y otros van con la paz y otros en la mitad que, si hacen, pero no hacen (sic.)” añadió. Le puede interesar: [Fiscal debe dejar de tratar a las FARC como si no hubieran firmado la Paz: Andrés Paris](https://archivo.contagioradio.com/farc/)

### **“Hay muchísima gente que prefiere seguir en la guerra”** 

Manifiesta la integrante de Voces de Paz que lo que ha podido comprender es que “hay muchísima gente que prefiere seguir en la guerra, porque es que ni sus hijos, ni sus sobrinos, ni sus amigos van a la guerra, van los hijos de los que no tienen poder, de los que no tienen contactos con medios de comunicación, esos son los que van a la guerra y **quienes no mandan sus hijos a la guerra son los que están torpedeando el proceso”.** Le puede interesar: [Víctimas exigen a Congreso mantener esencia de los Acuerdos de Paz](https://archivo.contagioradio.com/victimas-exigen-a-congreso-mantener-esencia-de-los-acuerdos-de-paz/)

### **Corte Constitucional sí interfirió en la implementación del Acuerdo ** 

“La Corte Constitucional nos dejó como en el primer día de las negociaciones” y se desdibujó el papel del Congreso frente a las aprobaciones vía ‘Fast Track’ dado que lo que debe hacer el legislativo es traducir el Acuerdo de Paz al lenguaje jurídico y convertirlos en leyes para que sea de obligatorio cumplimiento y no para debatir y poner sus posiciones en los debates.

“Se trataba de darle forma legal a un documento que ya cumplió los requisitos para ser aprobado. En esa medida la Corte Constitucional echó para atrás todo. **Quedamos como si nada se hubiera hecho y todo está por hacer”** manifestó. Le puede interesar: [Gobierno pidió anular decreto de Corte Constitucional que limitó el Fast Track](https://archivo.contagioradio.com/gobierno-corte-constitucional-fast-track/)

### **Lo que está pendiente** 

Asegura Daza que falta la discusión de temas vitales para poder continuar y garantizar la implementación de los Acuerdos de Paz tales como la ley de víctimas que “quedó solo para reabrir la posibilidad de ser reconocidas como víctimas las más de medio millón de personas exiliadas colombianas que están en el mundo”.

Y recalcó que es importante no olvidar que **a las víctimas no les interesa la justicia punitiva, sino la justicia restaurativa**, que garantice el acceso a la verdad y a las garantías de no repetición. Le puede interesar: [Más micos en la Fast Track: el caso del Sistema Nacional de Innovación Agropecuaria](https://archivo.contagioradio.com/mas-micos-en-la-fast-track-el-caso-del-sistema-nacional-de-innovacion-agropecuaria/)

Así mismo, **están pendientes la constitución de los tribunales de paz, la seguridad jurídica para las FARC** "todavía no está clara la situación jurídica de los integrantes de la guerrilla" recuerda Daza.

Quedan 6 meses del ‘Fast Track’ ampliado, tiempo que según Daza no será suficiente para aprobar todo lo que pueda estar pendiente, porque **luego de la disposición de la Corte de discutir y aprobar artículo por artículo se requerirá de cerca de 10 años** para lograrlo. Le puede interesar:[ Votación en bloque en Congreso esperanza para Acuerdo de Paz: Imelda Daza](https://archivo.contagioradio.com/votacion-en-bloque-en-congreso-esperanza-para-acuerdo-de-paz-imelda-daza/)

“Estamos dependiendo de la voluntad de las mayorías que respaldan al Gobierno, esas voluntades son variables. Han prometido esas mayoría que seguirán aprobando o debatiendo los proyectos de ley como un todo, en un bloque, no lo van a hacer por artículo si ellos cumplen avanzaremos y en 6 meses se terminará” concluyó Daza.

<iframe id="audio_19398134" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_19398134_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio]{.s1}](http://bit.ly/1ICYhVU)[.]{.s1}

<div class="osd-sms-wrapper">

</div>
