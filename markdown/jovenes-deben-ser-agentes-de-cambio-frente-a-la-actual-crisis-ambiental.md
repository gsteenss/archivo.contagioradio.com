Title: Jóvenes deben ser agentes de cambio frente a la actual crisis ambiental
Date: 2015-08-12 17:08
Category: Ambiente, Nacional
Tags: agentes de cambio, Ambiente, Ambiente y Sociedad, Calentamiento global, cambio climatico, daño ambiental, Día internacional de la juventud, Vanessa Torres
Slug: jovenes-deben-ser-agentes-de-cambio-frente-a-la-actual-crisis-ambiental
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/08/Colores-por-el-agua-2.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: [periodicoeloriente.com]

<iframe src="http://www.ivoox.com/player_ek_6610697_2_1.html?data=l5uekpude46ZmKiakp6Jd6KpkpKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRiMbnwsuSpZiJhaXj1JDOz8fNqc%2FowtHS1ZDIqYzgwpDUx9PJtsLXyoqwlYqmd8%2Bfwsjh18bQb8XZjM%2BSpZiJhpTqxpKSmaiRh9Di1cbUy9SPlsLYytSYj4qbh46o&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Vanessa Torres, Ambiente y Sociedad] 

###### [12 Ago 2015]

En el marco del Día Internacional de la Juventud, la Organización de las Naciones Unidas, ONU, ha destacado que **la generación de jóvenes representa a una de las poblaciones más importantes como agentes de cambio** frente a la crisis ambiental y específicamente el tema del cambio climático.

De acuerdo con la organización, en el mundo existen más de **1800 millones de personas entre los 18 y los 24 años, **generación que enfrenta los mayores desafíos ambientales, por lo que exige de estos una activa participación en las luchas relacionadas con el tema, teniendo en cuenta que durante 2015, los gobiernos tomarán decisiones que determinarán el futuro de las próximas generaciones.

En septiembre se adoptará formalmente la agenda de los Objetivos del Milenio para combatir la inequidad y proteger el ambiente; en diciembre, los Estados se reunirán en París para establecer un nuevo acuerdo frente al cambio climático, de manera que **es fundamental la creación de espacios de participación para que los jóvenes puedan aportar a estas discusiones de las que resultan medidas decisivas para enfrentar el calentamiento global.**

Desde Ambiente y Sociedad se lidera una campaña denominada [Acción 2015,](http://ambienteysociedad.org.co/accion2015/calendario/)para que los jóvenes se vuelvan actores importantes en la implementación de medidas que contrarresten los efectos del cambio climático.

Durante agosto, mes mundial de la juventud, se desarrollarán diversas actividades que impulsen a esta población a liderar iniciativas en este escenario, sin embargo, es necesario que **los gobiernos cambien la visión negativa que se tiene en torno a los movimientos juveniles** y reconozcan su importancia como agentes de cambio.
