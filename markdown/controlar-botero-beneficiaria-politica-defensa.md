Title: Controlar Ltda, la empresa de Botero que se beneficiaría con la política de Defensa y Seguridad
Date: 2019-06-12 15:31
Author: CtgAdm
Category: Nacional, Política
Tags: Congreso, Guillermo Botero, Ministro de defensa, Moción de Censura
Slug: controlar-botero-beneficiaria-politica-defensa
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/guillermo_botero3-1170x585-e1560362675727.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Colombia Reports  
] 

Esta semana inició en el Congreso el proceso para impulsar la moción de censura contra el **ministro de defensa, Guillermo Botero**, por su responsabilidad en el caso de Dimar Torres y en la directriz denunciada por el New York Times que podría revivir casos de ejecuciones extrajudiciales. A estas denuncias, se sumó el impedimiento que tendría Botero para administrar la cartera de defensa, por su participación accionaria en empresas bajo la vigilancia de su Ministerio; hechos por los que podría ser investigado por la Procuraduría.

El **senador Wilson Arias** explicó que el Ministro no se declaró impedido para participar en la "Política de Defensa y Seguridad para la Legalidad, el Emprendimiento y la Equidad", de este Gobierno para los próximos años, pese a tener conflicto de intereses, ya que **Botero es el socio mayoritario de Controlar LTDA., compañia que se beneficiaría de esta política.**

Aunque Botero sí declaró sus impedimentos en decisiones tomadas al interior de la Superintendencia de Vigilancia y Seguridad Privada, así como en la elección de quien preside esta entidad; el Senador afirmó que, su omisión para participar en la política pública de seguridad "constituye una falta gravísima". (Le puede interesar: ["Política de seguridad, un retroceso de 20 años en derechos humanos"](https://archivo.contagioradio.com/politica-de-seguridad/))

### **El Ministerio de Defensa no debería estar en manos de un comerciante**

Arias recordó que mientras Botero fue presidente de la Federación Nacional de Comerciantes (FENALCO), evidenció en diferentes entrevistas su afán por que se contratara la vigilancia con empresas conocidas por la Superintendencia de Seguridad, y que estas empresas tuvieran el sello de ética. (Le puede interesar: ["Ministro de Defensa no es una persona competente ni ética para ocupar ese cargo: David Racero"](https://archivo.contagioradio.com/botero-no-competente-ministro-defensa/))

Adicionalmente, el Senador indicó que el actual Ministro reclamaba con frecuencia **que se dieran más licencias a empresas de seguridad, se aumentara la vinculación entre las compañias de vigilancia y la Policía,** y se hicieran concesiones a empresas extranjeras de seguridad para participar en el sector nacional.

Todas estas acciones del expresidente de FENALCO demostrarían que era una persona muy activa en la búsqueda de negocios para compañías de seguridad; y brindarían algunas luces sobre la participación del ministro en la Política Pública de seguridad puesta en marcha por el Gobierno Duque. Ante esto, el Senador concluyó que "estos son temas que no deben quedar en manos de un interés particular, sino de un ministro de defensa que piense en el país".

> Acá está la “impecable" gestión del ministro Botero: ser socio capital de una empresa de seguridad que vigila la Superintendencia de Vigilancia que está a su cargo.
>
> Con una mano hace negocios y con la otra él mismo se “vigilia".
>
> Su gestión no es impecable ni transparente. [pic.twitter.com/Ep4IwMuZxM](https://t.co/Ep4IwMuZxM)
>
> — Ángela María Robledo (@angelamrobledo) [10 de junio de 2019](https://twitter.com/angelamrobledo/status/1138144657567821824?ref_src=twsrc%5Etfw)

<p>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
</p>
### **La posible influencia del Ministro en la política pública**

El Senador por el Polo expresó que en Colombia hay un debate fuerte, y anterior al ministro Botero, sobre el límite de empresas de seguridad privada que tienen licencia para funcionar, porque esta decisión afecta la seguridad y soberanía nacional, en tanto se trata de compañías con acceso a armamento. Es decir, **se discute el monopolio de la fuerza a cargo del Estado.**

Tomando esto en cuenta, Arias resaltó que hay voces que señalan que el Plan Nacional de Desarrollo (PND) en su redacción, **da la posibilidad de que empresas extranjeras compitan para alcanzar alianzas público privadas para la construcción de cárceles en el país.** (Le puede interesar: ["Nueva cúpula militar: ¿vuelve la seguridad democrática?"](https://archivo.contagioradio.com/nueva-cupula-militar-vuelve-la-seguridad-democratica/))**  
**

Sumado a ello, el Congresista dijo que **la política de seguridad de este Gobierno creó frentes de seguridad entre Policía y empresas privadas, también buscó impulsar un modelo nacional de vigilancia comunitaria** y reactivó las redes de participación cívica (o redes de informantes). "Esta política pública es posible que sea distinta en manos de un Ministro que no tuviera intereses particulares", concluyó Arias.

### **Procuraduría tendría en sus manos el caso de Botero**

En la formulación de queja disciplinaria radicada por Wilson Arias contra Botero, el Senador expuso: "En pocas palabras, el doctor GUILLERMO BOTERO, en ejercicio de sus funciones, decidió articular en diferentes niveles, vincular y favorecer por medio de la política de seguridad del Ministerio de Defensa al sector Seguridad Privada, del cual ha sido durante muchos años impulsor y defensor gremial y en el cual tiene interés particular, directo y concreto por ser socio de la empresa de vigilancia CONTROLAR LTDA. (...) **Crea la política que articula a quienes no debió articular y se reserva la supervisión esa articulación**".

El artículo 40 de la Ley 734 de 2002 ordena que "todo servidor público deberá declararse impedido para actuar en un asunto cuando tenga interés particular y directo en su regulación, gestión, control o decisión..."; en el artículo 48 de la misma Ley se califica como faltas gravísimas "no declararse impedido oportunamente, cuando exista la obligación de hacerlo...), razón por la que el Senador consideró que el ministro Botero debería ser investigado por la Procuraduría en este caso.

<iframe id="audio_37029295" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_37029295_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
