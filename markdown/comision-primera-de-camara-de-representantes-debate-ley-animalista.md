Title: Aprobado Proyecto de Ley animalista en primer debate
Date: 2015-03-24 17:52
Author: CtgAdm
Category: Animales, Nacional
Tags: Animales, Animalistas, Cámara de Representantes, Comisión primera, Congreso, Juan Carlos Losada, Plataforma Alto, Proyecto de Ley
Slug: comision-primera-de-camara-de-representantes-debate-ley-animalista
Status: published

##### Foto: Juan Carlos Losada 

Este martes 24 de marzo se debatió el proyecto de Ley de protección animal, propuesto por el representante a la cámara del Partido Liberal, Juan Carlos Losada, el cual fue **aprobado en su primer debate en la Comisión primera de Cámara de representantes.**

El debate, contó con la presencia de diversas organizaciones animalistas, que mostraron su apoyo al representante del Partido Liberal.

Cabe recordar que esta iniciativa del representante acompañado por varias organizaciones animalistas, se basa en dos ejes. El primero es que **los animales sean declarados seres sintientes, **es por eso que “**estamos reformando el código civil,** consideramos que los animales no pueden ser tratados como cosas”, expresa Losada.

El segundo eje, se refiere a la penalización del maltrato animal, por lo que se quiere fortalecer y modificar la Ley 84 de 1989, para que existan multas entre 5 y 60 salarios mínimos legales vigentes”.

[![tuit animalista](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/03/tuit-animalista.png){.wp-image-6390 .aligncenter width="562" height="281"}](https://archivo.contagioradio.com/comision-primera-de-camara-de-representantes-debate-ley-animalista/tuit-animalista/)
