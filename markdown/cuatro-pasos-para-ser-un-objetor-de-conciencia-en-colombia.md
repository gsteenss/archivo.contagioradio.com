Title: Cuatro pasos para ser un objetor de conciencia en Colombia
Date: 2018-11-22 15:57
Author: AdminContagio
Category: DDHH
Tags: objeción de conciencia
Slug: cuatro-pasos-para-ser-un-objetor-de-conciencia-en-colombia
Status: published

###### Foto: Justapaz 

###### 22 Nov 2018 

La ley 1861, que reglamenta el servicio de reclutamiento de las fuerzas armadas en Colombia también regula las posibles causales de exoneración del servicio militar, entre ellas la objeción de conciencia,  una causal instaurada en la Constitución  por orden de la Corte y que muchas veces enfrenta todo tipo dilataciones y obstáculos en el proceso.

Según datos aportados a Justapaz por el Comando de Reclutamiento y Control Reservas a partir de la vigencia de esta ley, **442 jóvenes se declararon objetores de conciencia, de los cuales tan solo 266 fueron reconocidos**, en muchas ocasiones por no recibir la asesoría necesaria para realizar el proceso.

Álvaro Peña, objetor de conciencia, abogado e integrante de Justapaz explica qué pasos debe seguir un joven colombiano quien por razones políticas, éticas o  humanitarias se oponga a prestar el servicio militar obligatorio y logre ser reconocido como objetor de conciencia por las Fuerzas Armadas sin que se le vulneren sus derechos.

**1. Documentación**

Como parte del proceso, los jóvenes  deben realizar una solicitud formal ante su distrito militar el cual les dará un plazo de 10 día para elaborar una carta de presentación personal y reunir documentos o testimonios de personas u organizaciones que ratifiquen cuáles son los principios que les impiden prestar el servicio militar obligatorio. [(Le puede interesar 379 solicitudes de objeción de conciencia en el prmer año de ley de reclutamiento)](https://archivo.contagioradio.com/foro-objecion-de-conciencia/)

**2. Paso por la comisión disciplinaria**

Al reunir la información, el joven será citado a una comisión disciplinaria en un plazo de 15 días hábiles donde se definirá su situación, dicha comisión está compuesta por cinco integrantes de los cuales cuatro son militares y tan solo uno es del ministerio público, lo que no garantizaría la objetividad en la decisión que se tome.

**3. Apelación**

Por lo general en aquella misma citación se le otorgará al joven una resolución que indicará si su objeción es reconocida o no.  De ser denegada tal petición, todo joven puede apelar o presentar un recurso de reposición para que se lleve el caso a una instancia de nivel nacional hasta el Comando de Reclutamiento y Control Reservas que resolverá si se mantiene la decisión o si se le reconoce como objetor de conciencia.

Según el abogado, **no existe un argumento o herramienta técnica o jurídica para no reconocer la petición de objeción de conciencia**, sin embargo, en caso que al joven se le quiera reclutar en aquel mismo momento, este puede acudir a una tutela que defienda su derecho fundamental.

**4. Asesoría**

Existen varias organizaciones como: **Justapaz, la Articulación Antimilitarista, La Tulpa o la Acción Colectiva de Objetores y Objetoras de Conciencia** que pueden brindar la ayuda necesaria para guiar al joven a través del proceso y otorgarle un acompañamiento jurídico, que sustentado con los argumentos precisos, podrá concederle al joven su reconocimiento como objetor de conciencia. (Le puede interesar: ["Así es el camino para graduarse sin libreta militar"](https://archivo.contagioradio.com/se-gradua-en-colombia-el-primer-objetor-de-conciencia-sin-libreta-militar/))

<iframe id="audio_30270245" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_30270245_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
