Title: 104 días en huelga duraron trabajadores de Goodyear en Yumbo, Valle
Date: 2016-01-29 17:55
Author: AdminContagio
Category: Nacional, Resistencias
Tags: Goodyear Colombia, Sintraincapla
Slug: huelga-de-trabajadores-de-goodyear-en-yumbo-valle
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/01/Sintraincapla.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Sintraincapla ] 

<iframe src="http://www.ivoox.com/player_ek_10252202_2_1.html?data=kpWfl5eWdJOhhpywj5WUaZS1kZWah5yncZGhhpywj5WRaZi3jpWah5yncbWZpJiSo57WscriwpDi0MaPqMafzcbgjc3Zqc3bwtiYz4qnd4a1ktiYzsbWq8LnjMnSzpDUpYa3lIquptiRaZi3jqjc0NnFq8rjjLfOxs7Tb46ZmKialg%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [29 Ene 2016 ] 

**Tras 104 días de huelga cerca de 180 trabajadores** de la planta de Goodyear del municipio de Yumbo, Valle del Cauca, **lograron llegar a un acuerdo con las directivas** que pretendían aumentar 6 días de trabajo, rebajar los permisos sindicales, atacar la estabilidad laboral, cambiar los modelos de contratación y reconocer un aumento salarial del 4%, mientras el sindicato demandaba un incremento del 9.5%.

Jaime Ninco, vicepresidente de Sintraincapla, asevera que el conflicto laboral con la multinacional venía desde agosto del año pasado cuando presentaron un pliego de peticiones a las directivas y éstas respondieron con un **contrapliego en el que anunciaban medidas regresivas**, por lo que el 13 de octubre decidieron entrar en huelga, para presionar un aumento salarial justo y mejoras en las condiciones laborales.

“La retaliación de la empresa fue clara, **en el mes de diciembre no canceló salarios, no nos reconoció la prima ni un bono que teníamos**. Desde que nos fuimos a huelga hasta el día que la terminamos recibimos cero pesos de parte de la compañía”, asegura el líder sindical e insiste en que pese a la represión de la empresa **sabían que sus reclamos eran justos y debían hacerlos valer**.

De acuerdo con el sindicato “el reajuste salarial quedó en 8,5% para el primer año y del IPC para los años siguientes, y los **demás beneficios económicos (educación y vivienda, entre otros) aumentaron entre el 8.5% y el 10%**. También logró un bono de 25 días de salario básico para cada trabajador por la firma de la convención”.

###### Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU) ] 
