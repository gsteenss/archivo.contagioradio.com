Title: Respuesta de Duque a Noruega atenta contra la legalidad internacional
Date: 2019-01-23 14:59
Author: AdminContagio
Category: Nacional, Política
Tags: Cuba, ELN, Noruega, Protocolo, Relaciones Internacionales
Slug: respuesta-duque-noruega
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/01/Captura-de-pantalla-2019-01-23-a-las-12.27.53-p.m.-e1548264512660-770x400.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Presidencia de la República] 

###### [23 Ene 2019] 

Tras la respuesta de Noruega, en calidad de país garante del proceso de paz con el ELN, a Colombia por la solicitud hecha a Cuba de romper el protocolo establecido en caso de ruptura de la mesa de conversaciones, el profesor y doctor de la Universidad de Pittsburh Fabio López de la Roche, sostiene que puede obedecer a **la internacionalización de la política de lucha contra el terrorismo del gobierno Duque**.

De la Roche recordó que cuando se volvió extra territorial la política de seguridad democrática del gobierno Uribe, se realizó la captura de Rodrigo Granada en Caracas para trasladarlo a Bogotá; situación con la que se violó evidentemente la soberanía venezolana. Similar situación ocurrió en 2008, cuando el Ejército Nacional atacó el campamento de Raul Reyes en territorio ecuatoriano, **hecho que causo un conflicto diplomático con el vecino país**.

En ese sentido, el profesor sugirió que **debería analizarse una linea de política exterior que podría estarse poniendo en curso**, y a la que debería llamarse la atención para que respete los tratados internacionales, los pactos generados por el Estado colombiano y los compromisos asumidos en la negociación con el ELN. (Le puede interesar: ["Violar protocolo establecido con ELN sería un irrespeto a la comunidad internacional"](https://archivo.contagioradio.com/protocolo-eln/))

Adicionalmente, el académico afirmó que deberían agradecerse a Fidel y Raúl Castro sus acciones para lograr la paz en Colombia; de tal forma que **el pedido hecho por el gobierno colombiano a Cuba en realidad afecta las relaciones diplomáticas con la isla, y con Noruega**. (Le puede interesar: ["Colombia no puede omitir el DIH ni violar los protocolos de la mesa con el ELN"](https://archivo.contagioradio.com/colombia-no-puede-omitir-el-d-i-h-ni-violar-los-protocolos-de-la-mesa-con-eln/))

Para finalizar, De la Roche consideró contradictorio que mientras uno de los pilares de campaña del actual Gobierno fuera la legalidad, se intente restar legitimidad a los pactos establecidos por el gobierno Santos, todo ello, tomando en cuenta que el actual gobierno continúo la mesa de conversaciones, aunque estuviera congelada, durante estos 5 meses. (Le puede interesar: ["Buscamos la paz con todas sus dificultades, u optamos por la guerra con todo su dolor"](https://archivo.contagioradio.com/paz-o-guerra-con-dolor/))

### **Duque respondió a Noruega pidiendo la violación del Protocolo** 

Este miércoles en la mañana el presidente Duque reiteró su pedido a Cuba para que entregue a los jefes negociadores del ELN; llamado al que se sumó el canciller **Carlos Holmes Trujillo, quien desde Estados Unidos manifestó el pasado martes que dicho protocolo no goza del reconocimiento por parte del Gobierno colombiano**.

> [\#Davos2019](https://twitter.com/hashtag/Davos2019?src=hash&ref_src=twsrc%5Etfw) Espero que la comunidad internacional nos siga rodeando con un solo propósito: el terrorismo es una amenaza global y necesita que la justicia se aplique con todo el peso y que haya sanciones ejemplarizantes. [\#WEF2019](https://twitter.com/hashtag/WEF2019?src=hash&ref_src=twsrc%5Etfw) [pic.twitter.com/3l9iFBE7hs](https://t.co/3l9iFBE7hs)
>
> — Iván Duque (@IvanDuque) [23 de enero de 2019](https://twitter.com/IvanDuque/status/1088062078508777483?ref_src=twsrc%5Etfw)

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
