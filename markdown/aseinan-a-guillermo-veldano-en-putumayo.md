Title: Asesinan a Guillermo Veldaño en Putumayo
Date: 2016-12-12 20:40
Category: DDHH, Nacional
Slug: aseinan-a-guillermo-veldano-en-putumayo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/12/Guillermo-Veldaño-contagioradio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Twitter Piedad Cordoba] 

###### [12 Dic 2016]

Según la información publicada en recientes horas, fue asesinado el señor **Guillermo Veldaño**, líder de la vereda Buenos Aires, de la cual era el presidente de la Junta de Acción Comunal. Además **era integrante del Sindicato de Trabajadores Campesinos Sincatfromayo, de Fensuagro y del movimiento social Marcha Patriótica.**

Según la información recaudada por la Red de Derechos Humanos del Putumayo, los hechos ocurrieron hacia el medio día de este 12 de Diciembre, cuando 2 hombres que se movilizaban en una moticicleta **dispararon en varias ocasiones contra la humanidad de Veldaño causando su muerte en el sitio.**

Este hecho enluta a la región del corredor puerto Vega Teteye, una región azotada por las operaciones paramilitares que se vienen incrementando en medio de **una fuerte militarización, así como las operaciones petroleras** de varias empresas que operan en el departamento y que han provocado fuertes daños en los ecosistemas, así como en las fuentes de agua que surten los acueductos veredales. (También le puede interesar [Situación crítica de derechos humanos en Putumayo](https://archivo.contagioradio.com/?s=putumayo))

Con el asesinato de Guillermo Veldaño ya son más de 140 los líderes de marcha patriótica, defensores de derechos humanos y reclamantes de tierras asesinados o amenazados durante el último semestre. Por estas situaciones diversos movimientos sociales y organizaciones de DDHH han enviado mensajes y [realizado acciones para que se implementen las medidas necesarias para desmantelar el paramilitarismo](https://archivo.contagioradio.com/proteccion-defensores-de-derechos-humanos/) y prevenir nuevos atentados contra la vida de líderes sociales.

###### [Reciba toda la información de Contagio Radio en][[su correo]](http://bit.ly/1nvAO4u)[ o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por][[Contagio Radio.]](http://bit.ly/1ICYhVU) 
