Title: Coronel (r) Edilberto Sánchez pagará 40 años de prisión por desaparecidos del Palacio de Justicia
Date: 2016-01-12 15:51
Category: Judicial
Tags: Desaparición forzada, militares, Palacio de Justicia
Slug: coronel-edilberto-sanche-condenado-por-palacio-de-justicia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/12/holocausto-palacio-de-justicia-contagioradio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Contagio Radio 

###### [12 Enero 2016]

El Coronel (r) Edilberto Sánchez Rubiano fue condenado a 40 años de prisión por las desapariciones de **Carlos Augusto Rodríguez y Bernardo Beltrán Hernández,** desaparecidos durante la retoma del [Palacio de Justicia.](https://archivo.contagioradio.com/?s=Palacio+de+Justicia)

El juez 52 de conocimiento de Bogotá ordenó la captura del coronel teniendo en cuenta que Sánchez Rubiano, **era el comandante del B-2**, y además es investigado por la desaparición de siete empleados de la cafetería, tres visitantes ocasionales y la guerrillera Irma Franco Pineda. **El coronel coordinaba el puesto ubicado en la Casa Museo del Florero donde se identificó y interrogó  a las personas que salían con vida del Palacio.**

Cabe recodar que en febrero el coronel Sánchez deberá rendir indagatoria por otro proceso frente a los excesos de uso de la fuerza de los militares que participaron en la retoma del del Palacio de Justicia. Los subalternos en el B2: los sargentos (r) Ferney Causaya Peña, Luis Fernando Nieto Velandia, y Antonio Rubay Jiménez Gómez. También fue llamado un exagente del B2, el mayor en retiro Óscar William Vásquez, también son investigados.

Así mismo, el mayor Óscar William Vásquez deberá cumplir 40 años en prisión  por las desapariciones de Irma Franco, Carlos Augusto Rodríguez y Bernardo Beltrán Hernández.

 
