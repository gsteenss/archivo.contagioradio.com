Title: Hay que enfocarse en las víctimas, ellas siguen aquí mientras los políticos vienen y van: David Crane
Date: 2019-05-20 12:25
Author: CtgAdm
Category: Entrevistas, Paz
Tags: comision de la verdad, David Crane, Sistema Integral de Justicia
Slug: hay-que-enfocarse-en-las-victimas-ellas-siguen-aqui-mientras-los-politicos-vienen-y-van-david-crane
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/DSC6990.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/DSC6987.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio ] 

**David Crane,** el académico y reconocido abogado en derecho penal internacional se refirió a los retos que enfrenta la Comisión de la Verdad en Colombia y los avances que puede aprovechar el país con relación al trabajo que realizó el profesor como  **jefe de la Corte Especial para Sierra Leona** en la década de los 90.

Crane trabajó por más de 30 años en el gobierno de Estados Unidos y posteriormente fue designado como jefe fundador de la Corte Especial para Sierra Leona, un tribunal que durante su mandato logró judicializar al expresidente de Liberia, Charles Taylor, primer jefe de Estado africano condenado por un tribunal internacional por crímenes de guerra y lesa humanidad.

Debido a sus logros en materia de seguimiento y judicialización a crímenes contra los DD.HH, este tribunal internacional fue nominado al **Premio Nobel de Paz, en 2010**.

**Usted fue enfático en la importancia de contar la verdad, algunos sectores de la sociedad no ve su importancia ¿Cómo hacer que las personas valoren el trabajo que está realizando la Comisión de la Verdad?**

Para lograr que los colombianos entiendan el trabajo de la Comisión y la importancia no solamente de su trabajo sino del mecanismo de justicia, hay que conversar con ellos. Es importante que se sientan integrados al proceso, porque, de lo contrario habrán muchos retos para cualquier de los mecanismos en el esquema de la verdad y la justicia en Colombia.

Lo mas clave es que todos entiendan el enfoque central de las victimas de la Comisión. Hay que oír a las víctimas, quieren ser escuchadas, y lo mas critico es que ellos entiendan que la Comisión quiere escucharlos. [(Le puede interesar: No es cuestión de olvidar y perdonar, es cuestión de reconocer y cambiar: Lederach)](https://archivo.contagioradio.com/no-es-cuestion-de-olvidar-y-perdonar-es-cuestion-de-reconocer-y-cambiar-lederach/)

**Basado en su experiencia en países como Sierra Leona, qué pasos debe seguir la Comisión de la Verdad y cuáles no debe seguir? ¿Qué funcionó y qué verdaderamente no?**

Deben lograr trabajar con actores con puntos de vista distintos en el proceso de Justicia Transicional y entenderlos. La Comisión tiene que trabajar con el Ejército, los políticos, los territorios y desarrollar formas para comunicar y dialogar con ellos, para así lograr  resolver desafíos, dificultades y preguntas cuando se presenten.

Otro aspecto importante para la Comisión es compartir el informe final a todos los colombianos. Es un informe de ellos y no de la Comisión. Lo olvidamos, pero es importante. Los comisionados y los que trabajan en la Comisión necesitan visitar las regiones, los territorios, escuchar y recoger la información de las víctimas, para que sientan que hay interés en sus relatos. Al final ellas quieren contar sus relatos.

\[caption id="attachment\_67367" align="aligncenter" width="1024"\]![David Crane](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/DSC6987-1024x683.jpg){.wp-image-67367 .size-large width="1024" height="683"} Foto: Contagio Radio\[/caption\]

**Es importante no perderse en el ruido de lo político, y seguir enfocado en las víctimas. Las víctimas seguirán, mientras los políticos vienen** y van, hay que superar los disturbios que se presentan en los procesos de justicia transicional. Eso no es particular en el caso de Colombia, le pasa a todo el mundo. Es un proceso pesado y a veces puede causar estrés y angustia, en particular a nivel político, pero es importante superarlo y no perder el camino.

**¿Qué cosas han cambiado desde la década de los noventa cuando ejercía su trabajo en África y que no tuvo la oportunidad de aprovechar y que ahora nuestro Sistema Integral de Verdad, Justicia, Reparación y No Repetición?**

La tecnología ha cambiado mucho. Hace diez años, no existía el iPhone, ni las redes sociales como Facebook. Ahora, tenemos que usar las redes sociales de manera positiva para asegurar que la historia llegue a todos. Todos tienen acceso a las redes, no es necesario usar los periódicos o la televisión. Es importante trinar y  tener presencia en las redes con regularidad o a diario para socializar el trabajo de la Comisión, y los mecanismos jurídicos porque de todas maneras alguien lo va a contar.

Hay que tenerlo en cuenta constantamente porque se puede asistir o destruir el esfuerzo hecho. Esa es la diferencia más grande de mi trabajo en Sierra Leona, eso no existió. La tecnología es una maldición y una bendición para la investigación de delitos. A veces, hay demasiado información y es como buscar una agujar en un pajar.

También existe un fenómeno nuevo: ese concepto loco de ‘fake news’, El problema radica en que pueden construir una narrativa fuera de Colombia, que puede destruirlo todo, si tienen la disposición. Es importante para el plan de medios y comunicación asegurarse que no existen narrativas falsas, construidas por los que están en contra del trabajo de la Comisión. [(Lea también: Vencer la apatía, el desafío más grande al buscar la verdad en Colombia: John Paul Lederach)](https://archivo.contagioradio.com/vencer-la-apatia-el-desafio-mas-grande-al-buscar-la-verdad-en-colombia/)

**Ver a Colombia desde el exterior le permite verla de un modo diferente, al hablar sobre reconciliación y perdón ¿considera que la sociedad colombiana está lista para eso?**

Hay que tener mucho cuidado con los conceptos como olvidar, perdonar e ir hacia el futuro, a los políticos les gusta usar esas expresiones para su provecho y en realidad hacer otras cosas. Es una narrativa falsa, una ‘cortina de humo’. Los colombianos necesitan contar su historia, no necesariamente perdonar, las víctimas necesitan justicia y que alguien asuma una responsabilidad, si no la sociedad fracasará.

Es mejor pensar en la reconciliación, donde se reúna a gente con objetivos, perspectivas y afectaciones distintas. Si logramos con la reconciliación ir hacia la verdad y la justicia, el país irá hacia adelante. Nunca se puede lograr que todos se pongan de acuerdo, no es posible ni Colombia ni en el mundo, si lo intentan solo encontrarán frustración. Hay que reconciliarse y no idealizar la paz en una era de oro.

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
