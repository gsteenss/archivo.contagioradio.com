Title: La ofensiva de los descerebrados: ataques, marchas e infamias
Date: 2016-04-13 09:41
Category: Javier Ruiz, Opinion
Tags: Alvaro Uribe, marcha, Uribistas
Slug: la-ofensiva-de-los-descerebrados-ataques-marchas-e-infamias
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/04/marcha-2-de-abril-uribistas.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Indepaz 

#### **Por [Javier Ruiz](https://archivo.contagioradio.com/javier-ruiz/)- [@contragodarria](https://twitter.com/ContraGodarria)** 

###### 13 Abr 2016 

El 2 de abril el uribismo quería demostrar su fuerza y expresar ante todo el país y ante todo el mundo su posición en contra de la paz. Vale decir, así los uribistas lo nieguen, que su enemigo mortal no es el presidente Santos sino la paz. Ellos dicen que están en contra de la forma en que se está negociando la paz con las FARC, pero el día de la marcha la realidad fue otra, fue explícito su apoyo a la continuidad de la guerra y al derramamiento de sangre.

Los uribistas querían ocultar el motivo de la marcha del 2 de abril, pero al ver quienes marchaban, de qué lugar del espectro político e ideológico provenían los protestantes y sus fines, se notaba que la marcha buscaba; además de atacar a la paz; que no se investigue a los delincuentes comunes del uribismo por casos de corrupción o por su vinculación con los grupos paramilitares, como es el caso del “apóstol” Santiago Uribe.

En algunos lugares del país, donde los paras despojaron de sus tierras a los campesinos, los latifundistas se ponían camisetas que decían estar en contra de la restitución de tierras y los grupos paramilitares, (que supuestamente se habían acabado en el gobierno Uribe), invitaban a marchar con panfletos amenazantes, si no lo hacían habrían consecuencias. Yo, hasta el momento, no he escuchado rechazo alguno por parte del uribismo sobre los panfletos de los paras.

Acá en Bogotá era “normal” ver a los denominados “uribistas de bien” marchando al lado de grupos neonazis pidiendo más muerte y guerra. Era “normal” verlos pedir que atentaran contra los líderes de izquierda porque, en su lógica demencial, aquellos líderes son “terroristas vestidos de civil”. Era “normal” verlos decir que la marcha era “apolítica” y que no tenía un “color” político en particular. Puede que no tenga color pero decir que fue apolítica es una gran falsedad, con ese argumento pretendían lograr unidad entre los colombianos para apoyar a la guerra.

Así lo niegue el uribismo la marcha fue un fracaso. No fue la cantidad de gente esperada y su apoyo a la guerra pierde adeptos. Los que marcharon pidiendo guerra no la han padecido y nunca han sabido lo que es estar en un cruel combate con la guerrilla. Estos que piden guerra lo hacen cómodamente desde sus redes sociales creyendo que un trino es una ráfaga mortal contra sus contendores. Para muchos uribistas, que son gente del común como usted o como yo, es más cómodo pedir más guerra vía Twitter, sobre todo para aquellos ídolos de barro uribistas que hacen fama a través de la red social anteriormente mencionada.

Quiero aprovechar este espacio para manifestar que los descerebrados me han visto como una persona incómoda y he recibido ataques personales y algunos muy bajos en Twitter, tanto que me han creado un fake (parodia) de mi cuenta en Twitter, cosa que me halaga mucho porque me quieren imitar. La tolerancia no es una virtud de los uribistas, pero si algo extraño sucede conmigo hago única y exclusivamente responsable al señor José Cuello Blanco que suele amenazar con calumnias y es el posible creador de mi fake o cuenta parodia para suplantarme.

Uribistas, les recuerdo que la calumnia y la injuria son delitos graves. Si con esto pretender callarme pues no lo van a lograr y lamento decirle a José Cuello que con esto no va a lograr su membresía a los grupos neonazis.

------------------------------------------------------------------------

###### Las opiniones expresadas en este artículo son de exclusiva responsabilidad del autor y no necesariamente representan la opinión de la Contagio Radio. 
