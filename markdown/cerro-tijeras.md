Title: Continúan embates de Fuerza Pública contra campamentos de la Minga
Date: 2019-04-05 16:36
Author: AdminContagio
Category: DDHH, Nacional
Tags: Abuso de fuerza ESMAD, Cauca, Cerro Tijeras, ESMAD, Minga Nacional
Slug: cerro-tijeras
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/cerro-tijeras.png" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/cerro2.png" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/cerro3.png" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/55622724_862195934121981_3613851200148996096_n.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:  Colombia Plural - Berta Camprubí] 

###### [5 Abr 2019] 

Comunidades pertenecientes a tres Resguardos y siete cabildos indígenas de la zona occidente del Cauca denuncian hostigamientos sin fundamento por parte de aproximadamente **30 uniformados del ESMAD e integrantes de la Policía** que el pasado 3 de abril irrumpieron en los campamentos establecidos en el marco de La Minga por la Defensa de la Vida destruyendo y quemando las carpas y pertenencias de las comunidades además de contaminar sus alimentos.

**Felipe Castiblanco, uno de los profesores del resguardo**, relata que en la mañana de ese día, vieron personal sospechoso vestido de civil y personal de la Policía en los alrededores, pero que fue hasta el medio día mientras los mingueros almorzaban que llegaron tres tanquetas hasta el sitio ubicado a cuatro kilómetros del casco urbano de Morales y arremetieron contra el campamento armados con gas lacrimógeno, papas bomba, recalzadas y balas convencionales.

Según el profesor, personas de la comunidad corrieron a ocultarse a las viviendas de un caserío aledaño sin embargo, resultaron más perjudicadas pues el gas lacrimógeno se se concentró en estos espacios cerrados, **"hubo personas desmayadas incluyendo niños, eso no les importó a los uniformados quienes destruyeron las banderas de las organizaciones, incendiaron las carpas y contaminaron parte del alimento"**, recuerda Castiblanco.

\

### **También hay presencia de grupos al margen de la ley** 

El profesor también se refirió a la situación del 25 de marzo cuando integrantes de grupos delincuenciales que operan en Cali y se refugian en Suárez irrumpieron en los campamentos y retuvieron a algunos comuneros que fueron liberados al día siguiente, Castiblanco señala que esta situación es comparable a lo sucedido el pasado miércoles cuando manifestantes arremetieron contra sedes del CRIC en Popayán con el argumento de estar cansados del bloqueo de la Vía Panamericana.

Conforme a estas circunstancias, **la comunidad de Cerro Tijeras ha contabilizado a lo largo de estos 25 días más de 40 personas heridas, de las cuales 4 fueron de gravedad** y tuvieron que ser trasladados a Cali debido a los golpes recibidos en su cabeza, uno de ellos ya había tenido antecedentes de trauma en su cráneo y perdió el conocimiento por varias horas. Los heridos actualmente se encuentran en estado estable. [(Lea también: 14 Hechos de violencia contras los DD.HH. de la Minga en Cauca) ](https://archivo.contagioradio.com/como-avanza-la-minga-en-la-defensa-de-dd-hh-de-los-pueblos-indigenas/)

Acompañados por el CRIC, se le indico al Resguardo a través de un simulacro cómo reaccionar a eventuales arremetidas, además se realizó un listado de las pérdidas materiales  y se espera que el Consejo Indígena  haga entrega de nuevos materiales para  reconstruir los campamentos. [(Le puede interesar: Estamos en capacidad de resistir, la Minga se revitaliza cada día )](http://ESTAMOS%20EN%20CAPACIDAD%20DE%20RESISTIR,%20LA%20MINGA%20SE%20REVITALIZA%20CADA%20DÍA)

<iframe id="audio_34130004" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_34130004_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
