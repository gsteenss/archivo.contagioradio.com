Title: Primer General del Ejército será llamado a juicio por "falsos positivos"
Date: 2017-01-26 17:29
Category: DDHH, Nacional
Tags: Asesinatos, Ejecuciones Extrajudiciales, falsos positivos, militares
Slug: general-del-ejercito-llamado-a-juicio35257
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/03/Falsos-positivos.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: ] 

###### [26 Ene. 2017] 

Existen suficientes pruebas para demostrar que el excomandante de la Brigada XVI del Ejército con sede en Yopal, general (r) **Henry Torres Escalante participó en el asesinato de dos personas en la zona rural del municipio de Aguazul en Casanare quienes fueron presentados como “falsos positivos”**, así lo aseguró la Fiscalía, entidad que el jueves iniciará de manera formal el juicio en contra de Torres, luego de casi 13 años de cometidos los asesinatos.

**Las dos personas asesinadas, eran campesinos, padre e hijo** y sus nombres eran Julio Torres de 38 años  y Daniel Torres Arciniegas de 16 años, ambos **fueron presentados como guerrilleros muertos en combate,** razón por la cual el ex general Torres tendrá que responder por el delito de homicidio en persona protegida.

Según la Fiscalía, el objetivo de asesinar a Julio y Daniel era poder silenciarlos ya que eran testigos claves en otro caso de ejecución extrajudicial o “falso positivo”. Le puede interesar:[ Siguen las dilaciones en casos de ejecuciones extrajudiciales de Soacha](https://archivo.contagioradio.com/siguen-las-dilaciones-en-ejecuciones-extrajudiciales-de-soacha/)

Asegura el ente acusador que el ex general Henry Torres tuvo conocimiento de cómo, cuándo y dónde sería realizado el operativo por parte de algunos hombres que estaban bajo su cargo en la Brigada XVI. Así mismo, dice que dichas muertes estuvieron planeadas con suficiente tiempo de anticipación.

Para ello se sostienen en dos tópicos, el primero es la realización de un censo por parte de 15 militares de la Brigada XVI en el lugar donde habitaban los campesinos Daniel y Julián. El segundo, es que días antes del asesinato de los dos campesinos, se realizó la captura de un guerrillero en la parcela donde ellos habitaban.

El escrito de acusación, que será presentado por la Fiscalía en el juzgado primero penal del circuito de Yopal, dice que en el caso del ex general Henry Torres no se trató solamente de impartir órdenes para que fueran cometidos los crímenes, sino que como **comandante de la Brigada fue quien confirmó la información de inteligencia que serviría para sustentar la misión táctica** que estaría a cargo del Grupo Especial Delta 6, pese a que la vereda donde vivían Daniel y Julián no estaba dentro de la jurisdicción de dicho grupo especial.

A pesar de que la orden de ejecutar la operación fue dada por el mayor Carlos Alirio Buitrago Bedoya, la Fiscalía dice que la orden fue de Torres Escalante, quien por ser comandante de la Brigada era el único para hacerlo.

**Por estos dos asesinatos ya fue condenado a 22 años de cárcel el teniente Fabián García, quien se convierte en testigo esencial en el proceso contra Torres. **Le puede interesar: [Militares implicados en “falsos positivos” no deben ser ascendidos: HRW](https://archivo.contagioradio.com/se-debe-revaluar-ascenso-de-militares-implicados-en-casos-de-falsos-positivos/)

Según la organización internacional Human Rights Watch **son 180 los batallones investigados por “falsos positivos”** y algunos altos mandos que aún se encuentran activos y en algunos casos condecorados también estarían implicados en algunos de estos casos.

La Fiscalía, por su parte ha dicho que investiga más de 3000 casos, en los cuales según víctimas, familiares y organizaciones sociales pocos han sido los avances. Le puede interesar: [Ejecuciones extrajudiciales persisten en Colombia](https://archivo.contagioradio.com/cada-6-dias-se-registro-una-ejecucion-extrajudicial-en-colombia-durante-2015/)

Por este escándalo de‘ falsos positivos’, que tuvo su auge entre 2002 y 2008, se está pendiente de la decisión de la Fiscalía en contra del excomandante del Ejército general Mario Montoya, quien rindió indagatoria con un fiscal delegado ante la Corte.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)
