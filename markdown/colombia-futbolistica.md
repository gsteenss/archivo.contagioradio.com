Title: Colombia futbolística
Date: 2016-04-04 09:53
Category: invitado, Opinion, superandianda
Tags: futbol
Slug: colombia-futbolistica
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/04/hinchas-colombia.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: FIFA 

#### **[Superandianda](https://archivo.contagioradio.com/superandianda/)- [@superandianda](https://twitter.com/Superandianda)** 

###### 3 Abr 2016 

[Cuando todos nos miremos y  nos preguntemos por qué llevamos el color de la bandera solo el día de un partido de fútbol, despertará la conciencia en Colombia.]

[Sucede algo especial alrededor de un balón: todo se detiene, nuestros problemas se colorean y encontramos la unión, yo diría, la falsa unión, la que por noventa minutos combate a la desigualdad  que existe en Colombia en lo que normalmente sería un absurdo: escuchar al intelectual, al obrero, al gerente, al empleado, al político y cualquier otro ciudadano,  en la sola voz que canta goles; es una rápida escena de igualdad, una fugaz unión. Todo esto ocurre solo mientras estemos en la cancha.]

[Entonces pienso que deberíamos cambiar a las calles por canchas, grandes canchas donde todos juguemos el mismo juego con la misma camiseta, de esa manera nos la jugaríamos por tantas cosas que siempre han esperado un equipo, diferente claro esta de campañas "dejen jugar al Moreno" o todos los goles del uribismo, jugaríamos por nosotros, los que nunca hemos jugado: estudiantes, maestros, prisioneros, ciudadanos, campesinos.]

[Cuando veo a la  Colombia futbolística, apasionada, unida y sentida en un solo equipo, puedo pensar que el hecho puede ser grotesco pero esperanzador: aún creemos en algo  y aún podemos unirnos. El problema es que no sabemos cómo unirnos del todo, cómo acabar con todas las Colombias que tenemos y formar una sola, cuando nombró las colombias es porque son tantas las cosas que nos identifican que al fin de cuentas terminamos con la identidad de la nada: sin un objetivo, sin una razón, sin trascendencia.]
