Title: Población de Argelia Cauca, en medio del horror de la guerra
Date: 2020-05-17 12:57
Author: AdminContagio
Category: Actualidad, DDHH
Tags: Disidencias
Slug: poblacion-de-argelia-cauca-en-medio-del-horror-de-la-guerra
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/05/Comunicado-disidencias-en-Argelia.jpeg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/05/Amrados-en-Argelia.png" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/05/Amrados-en-Argelia-1.png" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:heading {"align":"right","level":6,"textColor":"cyan-bluish-gray"} -->

###### Foto: @EEColombia2020 {#foto-eecolombia2020 .has-cyan-bluish-gray-color .has-text-color .has-text-align-right}

<!-- /wp:heading -->

<!-- wp:paragraph -->

Este sábado 16 mayo se conocieron una serie de denuncias de habitantes de en Argelia, Cauca, que señalaban la presencia de cerca de 30 hombres armados que se identificaron como integrantes de las llamadas disidencias del frente Carlos Patiño. Según se informó en medios, los hombres estarían buscando a excombatientes de FARC que sí se acogieron al proceso de paz.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Defensores de derechos humanos que trabajan en el territorio confirmaron a Contagio Radio la denuncia de la comunidad sobre un número indeterminado de sujetos armados que se desplazaron este sábado entre El Plateado a Puerto Rico y Sinaí, en Argelia. Además, señalaron que los hombres hicieron un retén en la vía de Plateado a Argelia, mientras la Fuerza Pública permanecía en la cabecera municipal.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Según la fuente, los hombres estarían adelantando reuniones con la comunidad, manifestando hacer control territorial y apoyar su organización así como sus proyectos. Lo dicho por los hombres también aparece en un panfleto conocido por Contagio Radio y firmado por el "Comando Coordinador de Occidente de las FARC-EP" divulgado el pasado viernes.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En el comunicado, el grupo señala que las extorsiones a campesinos en la zona a nombre del Frente Carlos Patiño y la Columna Móvil Jaime Martínez no hacen parte de su accionar; y señalan las acciones como parte de una estrategia que utilizan otros grupos armados. Esta situación aumenta la confusión pues no se sabe aciencia cierta de que grupo se trata.

<!-- /wp:paragraph -->

<!-- wp:image {"id":84331,"sizeSlug":"large"} -->

<figure class="wp-block-image size-large">
![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/05/Comunicado-disidencias-en-Argelia-797x1024.jpeg){.wp-image-84331}

</figure>
<!-- /wp:image -->

<!-- wp:heading -->

La situación humanitaria en Argelia
-----------------------------------

<!-- /wp:heading -->

<!-- wp:paragraph -->

El pasado 15 de abril, organizaciones sociales denunciaron que puerta a puerta, hombres armados incursionaron en zona rural de Argelia en búsqueda de líderes sociales y excombaitentes que habitan en la zona, ofreciendo recompensas por sus cabezas. (Le puede interesar: ["Puerta a puerta armados persiguen a excombatientes y líderes en Argelia, Cauca"](https://archivo.contagioradio.com/puerta-a-puerta-armados-persiguen-a-excombatientes-y-lideres-en-argelia-cauca/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Asimismo, vale la pena recordar que el pasado 22 de abril fueron [asesinadas 3 personas](https://twitter.com/sebaquiropa/status/1254892225475067906) en López de Micay que pertenecían al[Consejo Afro Renacer el Micay](https://archivo.contagioradio.com/micay-asesinato-tres-personas/). Integrantes de este Consejo señalaron que ante la escalada de violencia, su opción sería salir desplazados del territorio, [situación que ocurrió tres días después](https://archivo.contagioradio.com/desplazamientos-reten-ilegal-argelia-cauca/).

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

De acuerdo a organizaciones defensoras de DD.HH., Lopez de Micay, Argelia y El Tambo están en el centro de una disputa territorial entre distintos actores armados por tomar el Cañón del Micay, un espacio en el que persisten los cultivos de uso ilícito y sirve como ruta para salir hacia el mar pacífico. (Le puede interesar: ["Tres diferencias entre disidencias de FARC recorriendo las guerras en Colombia"](https://archivo.contagioradio.com/tres-diferencias-entre-disidencias-de-farc-recorriendo-las-guerras-en-colombia/))

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### ¿Y qué pasa con las Fuerzas Militares que copan el departamento?

<!-- /wp:heading -->

<!-- wp:paragraph -->

Según información de las propias Fuerzas Militares en el Cauca hacen presencia por lo menos 10500 hombres, 2500 de ellos pertenecientes a la Fuerza de Despliegue Rápido, FUDRA \# 4, que no han demostrado efectividad pues, esta situación, sumada al alto número de líderes sociales asesinados en el departamento y la persistencia del narcotráfico, dan cuenta de una labor ineficaz de las FFMM.

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->
