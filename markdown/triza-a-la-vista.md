Title: Triza a la vista  
Date: 2019-10-05 22:44
Author: JOHAN MENDOZA TORRES
Category: Johan Mendoza, Opinion
Tags: FARC, proceso de paz, uribismo
Slug: triza-a-la-vista
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/Johan.mp3" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

\[audio mp3="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/Johan.mp3"\]\[/audio\]

[Tal parece que la maldad camina alegre por estos días en Colombia. Alegre porque avanza con una dosis de anestesia lo suficientemente fuerte como para evitar observar que los guerreros y guerreras no quieren ser tratados con disimulo, sino que siempre preferirán ser amados profundamente en la guerra.]

[Para los estudiosos y cobardes, siempre habrá algo preocupante en la aceptación que solicita un sujeto diferente en el marco de esta sociedad estandarizada culturalmente. La política enciende debates en salones, redes sociales, salas y comedores; en medio de las mejores y peores familias. ]

[Incluso, aquellos que viven solos con sus gatos o sus perros, no les queda otra que humanizar al animal para cumplir discretamente la sentencia aristotélica sobre el lenguaje y la política. ]

[La política conserva y transforma el todo sin traicionar a la nada; hoy, una tendencia perversa se ha venido gestando y ha parido la figura aceptable (ya consumida por muchos) de ese excombatiente de las farc, ese comunista que ahora produce cosas y las vende en el mercado ¿Acaso ese fue el fin último del acuerdo de paz? Puede que muchos, bajo el absolutismo de las emociones, lleguen hasta esa conclusión, pero no. ]

[Los y las ex combatientes de las Farc eran miembros del Partido Comunista Clandestino, disuelto luego de la firma del acuerdo. Es decir, no eran sujetos esperando ser “sus propios jefes” amparados en la filantropía de algún millonario, o que tenían como fin último pasar de guerrilleros a empresarios. ]

[Por supuesto, como todo ser humano, producir y consumir, son ejes del desarrollo existencial que no dependen del modelo económico, es decir, forman parte de la naturaleza misma de la vida económica. La tendencia (totalmente bañada por el emocionalismo) consiste en que ya muchos se sienten “tranquilos” con el hecho de que “esa gente” que dejó las armas, por fin se puso a “hacer algo”.]

[No obstante, como diría un sabio, la felicidad nunca se sienta en un trono. Muchas personas ajenas al conflicto han podido perdonar lo que los excombatientes le hicieron a la sociedad; pero jamás pensarían si hay que perdonar lo que algunos excombatientes se están haciendo a sí mismos. Las cabezas llenas de paja no aportan a la consolidación del dilema.   ]

**¿Qué pasó con el acuerdo de paz? **

[Las trizas no son despojos sobre el suelo de nuestra historia. Son hechos reales con efectos reales; lo grave es que están cobrando vida propia; se están constituyendo en difíciles micro mundos desde los cuales las comunidades que viven en los territorios están clamando sobre nuevas formas de opresión.  ]

[Por ejemplo, se sabe que el Programa de Sustitución de Cultivos de uso Ilícito y los planes y programas orientados a la transformación estructural del campo han quedado subordinados a una estrategia de militarización de los territorios.  También, que los Programas de Desarrollo con Enfoque Territorial (PDET), se están subordinandos a las Zonas Estratégicas de Intervención Integral (ZEII) que tienen según lo enunciado por las comunidades, un enfoque militarista. ]

[Asimismo, la **Comisión Nacional de Garantías o el Sistema Integral de Seguridad para el Ejercicio de la Política** que tenía un claro enfoque de derechos humanos ha sido opacada por el Plan de Acción Oportuna (PAO) con enfoque militarista que ha propuesto el gobierno uribista. ]

[Ni hablar de lo que un organismo como el **Comité Internacional de la Cruz Roja** denuncia en su último informe sobre la expansión de cinco nuevos tipos de conflicto armado en Colombia. ]

[¡¡Ni hablar de Darío Acevedo que, con sus sabias y bien citadas palabras, justifica su posición negacionista del conflicto armado en el país!! Maltrechos y mentirosos los que no citamos, los que solo opinamos… yo solo puedo responder con sorna: dime qué objetividad buscas y te diré dónde citarla… ]

[No importa cuantos títulos o citas tengan los informes objetivamente propuestos por académicos del uribismo, lo importante es que nunca se confunda desmostar con derribar. Tampoco deberíamos olvidar que la sangre no es un argumento, que las fosas comunes aguardan el mismo tono ocre terracota por más largo que sea el informe.  ]

[No olvidemos que en medio del humo podríamos cuestionarnos ¿de qué sirve escuchar hablar sobre las cadenas que hemos hecho pedazos, si quienes hablamos, nunca hablamos del pensamiento que nos domina y en ocasiones nos convierte en esos encantadores miserables?]** **

##### Ver columna[ de opinión de Johan Mendoza](https://archivo.contagioradio.com/johan-mendoza-torres/)
