Title: Este es el comunicado de FECODE sobre Preacuerdo con gobierno nacional
Date: 2015-05-06 10:12
Author: CtgAdm
Category: Educación, Nacional
Tags: Asociación Distrital de Educadores, fecode, gina parody, Ministerio de Eduación, Paro del Mestros
Slug: este-es-el-comunicado-de-fecode-sobre-preacuerdo-con-gobierno-nacional
Status: published

###### Foto: Archivo 

<iframe src="http://www.ivoox.com/player_ek_4456276_2_1.html?data=lZmimJebeo6ZmKiak5yJd6KnmpKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRidToxpDS1ZDJsIzX0NLi0M7HpcXjjMnSjauph7C4ppDg0cfWqYzE08rOxdrJtsXjjMjc0JDLs46ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Tarsicio Mora, encargado de derechos humanos de Fecode] 

"Preacuerdos son benéficos a todo el magisterio

Hacia las 2:00 de la madrugada del día de hoy el Comité Ejecutivo de la Federación Colombiana de Trabajadores de la Educación llegó a un principio de acuerdo con el Gobierno Nacional.

En primera medida, la evaluación de competencias desaparecería definitivamente. En su reemplazo aparecería un mecanismo que permitiría el ascenso a los educadores del 1278, especialmente, a quienes no lo han hecho por la nefasta evaluación de competencias. De esta manera, se garantizaría movilidad en el escalafón para todos los docentes de este estatuto.

Ahora bien, Fecode ha manifestado que no le satisface el 12% en nivelación salarial pero planteó una fórmula al Gobierno para el cierre de la brecha salarial. En consecuencia, en dos años, la Federación y el Gobierno se sentarán nuevamente para retomar la discusión y establecer el alcance del correspondiente tope. Para los etnoeducadores hay un compromiso de un aumento en sus salarios y los maestros del grado 14 recibirían una bonificación anual. En cuanto a salud, cambiaría el sistema de selección de las entidades prestadoras del servicio médico-asistencial, de manera que se controle y aseguren los términos para su prestación.

De igual manera, se incluyó en el Plan Nacional de Desarrollo el artículo con el cual la Nación concurre con los entes territoriales para el pago de deudas laborales; así mismo, este garantiza la gratuidad educativa, dotación, alimentación e infraestructura para las escuelas.

El principio de acuerdo será socializado, analizado y discutido en Junta Nacional. El llamado al magisterio es a mantener la unidad y la fuerza en las acciones, y a no dejarse desinformar. Los logros alcanzados serán explicados por nuestros medios de comunicación en lo sucesivo y se publicará una cartilla donde se relacionen con cada uno de los puntos del pliego de peticiones y sus respectivos alcances."

[Tomado de página de Facebook de FECODE](https://www.facebook.com/fecode/posts/10153032541894930)
