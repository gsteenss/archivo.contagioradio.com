Title: Solo 149 adultos mayores en condición de calle logran dormir en albergues
Date: 2018-09-11 16:31
Author: AdminContagio
Category: Comunidad, DDHH
Tags: Adulto mayor, habitante de calle, secretaría distrital
Slug: adultos-mayores-sin-hogar
Status: published

###### [Foto: Milena Bernal] 

###### [11 Sept 2018] 

[Adultos mayores en condición de habitantes de calle se encuentran en una preocupante situación en la capital, según el Concejal Jorge Torres, solo están habilitados tres puntos de acogida nocturna o Centros Noche en la ciudad, donde **únicamente 149 personas logran conseguir albergue, mientras 632 pasan la noche sin un techo**.  ]

Según Torres, el déficit de puntos de acogida se debe a la “gestión lenta e indolente de la Secretaría de Integración Social”, quienes **sólo habrían comprometido el 30% de los \$3.406 millones de pesos del presupuesto** asignado en la conservación de estos centros. Esa misma institución señala que **dos adultos mayores son abandonados a diario en Bogotá, principalmente en hospitales y calles**.

[Además, de plantear la grave situación, que el Concejal calificó como **inhumana e inaudita, ** hizo referencia a la metodología que está usando la Secretaría de Integración Social para “decidir” quienes pueden acceder al refugio durante la noche “los abuelos tienen que llegar a las 5 de la tarde para recibir ficha, si llegan a las 5:20 quedan por fuera; a las 6 de la tarde hacen un sorteo entre todos, meten la mano a una bolsa con balotas y **el que saque la blanca no tiene derecho a pasar esa noche en el lugar.** si esto les pasa tienen dos opciones: buscar dinero para un lugar o dormir en la calle y quedar expuestos a robos y condiciones climáticas”.]

[El Concejal Torres hace un llamado urgente a la Secretaría Distrital para que realice la re apertura de los 4 centros que fueron clausurados durante el año y la creación de nuevos puntos de acogida par adultos mayores en áreas periféricas de Bogotá. (Le puede interesar: C[enso de habitantes de calle "debe verse como un drama y no como una cifra": Fray Gabriel Gutiérrez](https://archivo.contagioradio.com/censo-de-habitantes-de-calle-debe-verse-como-un-drama-y-no-una-cifra-fray-gabriel-gutierrez/))]

[ “Los puntos solo están en el centro de la ciudad, **hay abuelos que no pueden acceder a los servicios a menos que caminen 3 o 4 horas para llegar, y aún así se arriesgan a ser desfavorecidos por el sorteo**”, aseguró Torres y agregó  que citará a un debate de control político en donde se plantee la priorización de recursos “lo único de lo que se encarga el distrito es asegurarle a los ancianos que pasarán la noche en una cama y bajo un techo, y **si se tienen los recursos deberían estar cumpliendo”.**]

<iframe id="audio_28492947" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_28492947_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
