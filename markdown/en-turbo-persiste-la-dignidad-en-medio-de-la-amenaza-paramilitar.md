Title: En Turbo persiste la dignidad en medio de la amenaza paramilitar
Date: 2015-07-22 16:19
Category: Comunidad, Otra Mirada
Tags: cavida, CIDH, Comunicadores CONPAZ, Conpaz, Corte Interamericana de Derechos Humanos, Operacion genesis
Slug: en-turbo-persiste-la-dignidad-en-medio-de-la-amenaza-paramilitar
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/07/Clamores-alicia.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Gabriel G - contagioradio.com 

<iframe src="http://www.ivoox.com/player_ek_5131613_2_1.html?data=lpagk5uVd46ZmKiak5eJd6KplpKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRic%2Fo08rjy9jYpYzX0NOYo9HNp8rVhpewjcnJb6TgwtLc1MrXcoyhhpywj6jTstXVyM7cjbfFqMrjjJKSmaiReA%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Alicia Mosquera, Clamores] 

###### [22 Jul 2015] 

La Asociación de Mujeres de Rio Sucio Chocó, **CLAMORES, resiste en la ciudad de Turbo desde 1997**, año en que fueron desplazadas en el marco de la Operación Génesis. A pesar de la condena  de la Corte Interamericana de Derechos Humanos contra el Estado colombiano, las mujeres de CLAMORES denuncian que no han sido reparadas y que ni siquiera han recibido comunicación alguna por parte del Estado desde que se emitió la condena.

Las mujeres de CLAMORES, desde hace cerca de 15 años **decidieron que la memoria era un tema de dignidad, y por ello construyeron un monumento a las más de 90 víctimas** de la Operación Génesis, un espacio de memoria que ha resistido solamente por la voluntad de las mujeres y la solidaridad de algunas organizaciones que han aportado algunos recursos económicos para la pintura o materiales de construcción.

Alicia Mosquera, integrante de CLAMORES afirma que **el Estado colombiano nunca ha movido una mano para respaldar la construcción de memoria histórica o reconocer la memoria de las víctimas**, por ello exigen que la orden de la Corte Interamericana sea cumplida a cabalidad por el Estado, es decir, que se les  repare integralmente, se les respete su dignidad y se les restituyan los derechos vulnerados durante tanto tiempo.

Nos han amenazado, nos han señala, nos han ofendido, comenta Alicia, pero a pesar de todo ello se han mantenido y exigen, dentro de la reparación integral, las condiciones de vida digna se puedan dar también en la ciudad, exigen por ejemplo, una casa de 12 por 25 Metros, porque solo así podrán tener una vivienda digna que se parezca a la que los paramiltares y los militares les arrebataron en el 97.
