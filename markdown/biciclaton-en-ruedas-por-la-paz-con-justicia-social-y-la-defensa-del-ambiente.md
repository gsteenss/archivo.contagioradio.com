Title: Biciclatón “En ruedas por la paz con justicia social y la defensa del ambiente”
Date: 2015-06-12 14:43
Category: Ambiente, eventos
Tags: alternativo, arte, Biciclatón, Cultura, En ruedas por la paz, Paz de Colombia, sybil Sanabria
Slug: biciclaton-en-ruedas-por-la-paz-con-justicia-social-y-la-defensa-del-ambiente
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/06/Bicicletas2.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Lorenza Pérez 

<iframe src="http://www.ivoox.com/player_ek_4633313_2_1.html?data=lZuglZiVd46ZmKiakpmJd6KlmpKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRhsrXysjZw9mJh5SZo5jbjYqpdoaskYqmparSb9PpxsnO1ZDUs9OfzcaY0sbeb8Tjz5DX19jYrcTdwpDg0cjNpc2hhpywj6jTstXVyM7cjbfFqMrjjJKSmaiReA%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Sybil Sanabria, Estudiante U. Javeriana] 

###### [12 Jun 2015] 

Con la intención aportar a la construcción de la paz en Colombia, estudiantes  de universidades públicas y privadas convocan a todos y todas a participar en "Ruedas por la paz", este sábado 13 de Junio.

Con patines, patineta, tabla, bicicleta o cualquier medio de transporte alternativo, las personas interesadas podrán asistir a las 5:00 p.m al centro comercial Unicentro, lugar en el cual iniciará el recorrido, que tomará toda la carrera 15 hasta el Parque Nacional, donde se llevarán a cabo actos culturales y un concierto de cierre con diversas bandas alternativas.

"Las acciones por el ambiente también son acciones por la paz de Colombia", asegura el grupo de jóvenes convocante.  
[![Foto: En ruedas por el ambiente y la paz](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/06/bicicletada.jpg){.aligncenter .size-full .wp-image-10077 width="1336" height="2048"}](https://archivo.contagioradio.com/biciclaton-en-ruedas-por-la-paz-con-justicia-social-y-la-defensa-del-ambiente/bicicletada/)
