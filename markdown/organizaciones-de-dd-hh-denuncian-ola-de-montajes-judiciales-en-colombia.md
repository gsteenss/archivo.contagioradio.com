Title: Organizaciones de DD.HH denuncian ola de montajes judiciales en Colombia
Date: 2020-10-08 16:46
Author: CtgAdm
Category: Expreso Libertad
Tags: #Expresolibertad, #FuerzaPública, #Mediosdecomunicación, #Montajesjudiciales
Slug: organizaciones-de-dd-hh-denuncian-ola-de-montajes-judiciales-en-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/09/policias-e1506522939203.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

Tras las movilizaciones del 9 al 11 de septiembre en donde la ciudadanía expresó su indignación contra la brutalidad policial, organizaciones defensoras de derechos humanos denunciaron los montajes judiciales de los que han sido víctimas personas que **se encontraban en los lugares de las manifestaciones.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

La abogada Gloria Silva, afirmó en el Expreso Libertad, que estos montajes judiciales buscan estigmatizar el legitimo derecho a la protesta y que en el marco de lo que ha sucedido durante estas movilizaciones, tiene el agravante de las **torturas y tratos inhumanos y degradantes hechos por integrantes de la Fuerza Pública contra la ciudadanía.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Asimismo el abogado y docente Camilo Villegas Rondón, aseguró que en torno al papel que han jugado los medios de comunicación masivos, con noticias que afirman la tesis de disidencias de FARC-EP y la guerrilla del ELN tras la movilización, evidencian los **intereses políticos y económicos** que hay con esas construcciones de matrices mediáticas.

<!-- /wp:paragraph -->

<!-- wp:html -->  
<iframe id="audio_57670237" frameborder="0" allowfullscreen scrolling="no" height="200" style="border:1px solid #EEE; box-sizing:border-box; width:100%;" src="https://co.ivoox.com/es/player_ej_57670237_4_1.html?c1=ff6600"></iframe>  
<!-- /wp:html -->

<!-- wp:paragraph -->

Sintonice este programa del Expreso Libertad para escuchar las posturas de estos dos especialistas sobre los montajes judiciales y la responsabilidad de los medios de comunicación en los mismos.

<!-- /wp:paragraph -->
