Title: Lideresas sociales guardan dolores emocionales  que se deben atender: Limpal
Date: 2020-03-17 20:00
Author: CtgAdm
Category: Actualidad, DDHH
Tags: atencion, colombia, lideresas, mujeres
Slug: lideresas-sociales-guardan-dolores-emocionales-que-se-deben-atender-limpal
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/grito.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:heading {"level":3} -->

### 

<!-- /wp:heading -->

<!-- wp:paragraph -->

Pensando en las lideresas del país, el 12 de marzo la **Liga Internacional de Mujeres por la Paz y la Libertad Colombia** (Limpal) presentó el informe [**Sintonías Corporales**:](https://www.limpalcolombia.org/images/documentos/SINTONIAS_CORPORALES_DIC_19_1.pdf)memoria y resistencia de defensoras, un seguimiento a la Resolución 1325. (Le puede interesar: <https://archivo.contagioradio.com/las-carceles-y-su-doble-revictimizacion-a-las-mujeres/>)

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Un documento que identifica las afectaciones psicosociales que han tenido algunas lideresas y defensoras de derechos humanos en el desarrollo de su oficio; Limpal presenta **más allá de datos estadísticos un análisis de los factores de impacto en las [mujeres](https://archivo.contagioradio.com/la-lucha-feminista-sigue-por-la-despenalizacion-del-aborto-en-colombia/) que trabajan en la construcción de paz** en Colombia .

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Del mismo modo realizan recomendaciones a instituciones gubernamentales y privadas, para la **comprensión, acompañamiento y transformación de las realidades psicosociales de las defensoras.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

**Diana Salcedo, directora de Limpal** Colombia señaló que*"muchas entidades de salud, justicia y seguridad desconocen protocolos para hacer seguimiento y acompañamiento, lo que resulta en la revictimización de estas mujeres"*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El informe señala también que "*uno de los factores que ha incidido en que Colombia no tenga una política clara que responda a las necesidades en materia de salud mental y atención psicosocial es la falta de designación presupuestal para cumplir con esta obligación*".

<!-- /wp:paragraph -->

<!-- wp:quote -->

> "Da miedo*, yo soy defensora de derechos humanos en todos los espacios, porque yo no sé quién está de acuerdo con ser defensora de derechos humanos y eso mismo está pasando con esa estigmatización que desde la institucionalidad se ha venido haciendo*".
>
> <cite> Defensora, intervención en grupo focal, Meta, 2019. </cite>

<!-- /wp:quote -->

<!-- wp:paragraph -->

La anterior son declaraciones de una defensora presentadas en el informe, donde resaltan que las mujeres que ejercen liderazgos en el país tienen afecciones emocionales y sociales que el Estado no trata y relega a otras organizaciones, *"en medio de su ejercicio guardan traumas de su pasado, de sus vivencias y realidades rodeadas de machismos que no son atendidas y ellas van guardando"*.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Violencia de género, inseguridad y miedo son factores que afectan a las lideresas

<!-- /wp:heading -->

<!-- wp:paragraph -->

Salcedo señaló que aún muchas de las defensoras y lideresas enfrentan conductas patriarcales y hegemónicas que dificultan la garantía de sus derechos sociales, pero especialmente políticos.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Y destacó, *"los elevados niveles de violencia sociopolítica predominantes en los territorios del país son uno de los principales obstáculos para nuestras líderes, aún en muchos territorios su voz es relegada por las conductas machistas predominantes".*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Afirmando así que la lucha de las defensoras por eliminar las desigualdades en aspectos como política , y en especial las asimetrías en los procesos de paz, también han tenido que enfrentar múltiples obstáculos para posicionar sus estrategias de lucha y resistencia.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Además según la Directora, son conductas que se unen al conflicto territorial, *"las mujeres son las principales victimas del conflictos, sufriendo violencia física y sexual, además de tener que llorar, enterrar o buscar a sus familiar; lo cual representa muchas veces que quedan solas sosteniendo sus económi*as*, ante una sociedad que no tiene oferta laborar para mujeres del mismo modo que para hombres"*

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Hay grietas en los corazones de nuestras lideresas que es necesario tratar

<!-- /wp:heading -->

<!-- wp:paragraph -->

Diana Salcedo concluye diciendo que ante los eventos que se han venido viviendo y se seguirán presentando entorno a la verdad y a justicia, serán *"muchas las gritas que irán afectando a nuestras mujeres, por eso es necesario y ético que no las dejemos solas, que se les brinde una correcta atención y manejo psicosocial, porque son ellas la fuerza que mantiene las comunidades, son ellas la esperanza en el camino a la paz".*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Limpal hace también un llamado a las Naciones Unidas y a los Estados miembros de esta a ejecutar acciones como, garantizar la participación de las mujeres en los niveles decisorios de los procesos de paz; articular la perspectiva de género a la agenda de paz; crear medidas para proteger las mujeres en los conflictos armados y en las situaciones post bélicas, y hacer transversal el enfoque de género en la recolección de datos y sistemas de información de Naciones Unidas.

<!-- /wp:paragraph -->
