Title: Parlamento británico cuestiona el compromiso de Duque y la Fiscalía con la paz
Date: 2020-12-08 17:42
Author: CtgAdm
Category: Actualidad, Paz
Tags: acuerdo de paz, Cámara de Lores, Fiscalía, Néstor Humberto Martínez, Reino Unido
Slug: parlamento-britanico-cuestiona-el-compromiso-de-duque-y-la-fiscalia-con-la-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/12/Camara-de-los-Lores.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: Cámara de los Lores

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Hasan Dodwell, director de[Justice for Colombi](https://justiceforcolombia.org/)a, habló en exclusiva para Contagio Radio sobre el debate del pasado lunes, que tuvo lugar en la Cámara de los Lores, una de las dos cámaras del parlamento británico. Se habló de temas como la implementación del Acuerdo de Paz, las revelaciones en cuanto a las actuaciones de la Fiscalía de Néstor Humberto Martínez encaminadas a obstaculizar el trabajo de instituciones como la JEP en el caso de Jesús Santrich y las continuas violaciones a los DD.HH. que siguen ocurriendo en el territorio nacional

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Dicho debate, convocado por la Baronesa Christine Blower, contó con la participación de diez Miembros de la Cámara quienes hicieron sus preguntas al Lord Ahmad, ministro del gobierno británico con responsabilidad de Asuntos Exteriores, en la Cámara de los Lores como parte del Parlamento británico. [(Lea también: Colegio de abogados de New York advierte sobre presiones a la rama judicial en caso Uribe)](https://archivo.contagioradio.com/colegio-de-abogados-de-new-york-advierte-sobre-presiones-a-la-rama-judicial-en-caso-uribe/)

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

**CR: ¿Cómo se genera esta audiencia en el Parlamento británico y por qué allí es tan importante el tema del Acuerdo de Paz?**

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

HD: Los miembros de la cámara de los lores pueden proponer temas y la Baronesa Blower propuso el tema de Colombia porque, no solamente ella, hay muchos parlamentarios de varios partidos que, desde hace años, observan la situación de Colombia con mucha preocupación en el tema de derechos humanos y en términos de la implementación del Acuerdo de Paz. Es una muestra más general del interés de ambas cámaras en el Parlamento Británico frente a la situación en Colombia.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

HD: La estructura del debate de los lores, es que los miembros de la Cámara hacen preguntas al ministro representante en esa Cámara del gobierno británico y la baronesa Christine Blower fue quién expresó su preocupación por la situación y las revelaciones de los audios que salieron a la luz frente a la situación de la Fiscalía en el caso Santrich.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

**CR: ¿Cómo consideran que han actuado las distintas instituciones gubernamentales en torno al Acuerdo de Paz?**

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Hay una preocupación dentro del Parlamento británico y del movimiento sindical, donde también trabajábamos, frente al compromiso real del gobierno Duque de implementar el Acuerdo. Hay conocimiento de que en Colombia hay muchas fuerzas que no quieren el Acuerdo de Paz, ya sea por motivos personales, por no querer que exista un desmantelamiento de ciertos crímenes o por interés económico en que continúe la situación de guerra.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Hay muchas dudas de si hay personas intentando aprovecharse de la situación para atacar a la JEP y atacar al proceso de paz. Creo que para muchas personas, cuando salieron estos audios fue más bien una confirmación de lo que ya se había pensado, porque no se explica por qué se hace una operación de este nivel de inteligencia o de seguimiento para buscar a criminales.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

¿Por qué no se muestran estos audios a la JEP,? ¿por qué, después de pedirlo tanto, no se muestra todo?, eso fue lo que dijo la baronesa Blower, expresó que lo que salió a la luz fue que salieron 5 kilos de cocaína desde la oficina de la Fiscalía, pero eso no se dijo a la JEP, y en todo ese momento, hace dos años se usaba todo el caso Santrich para atacar duramente a la JEP, y estos ataques han seguido.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Lo que preguntó la Baronesa también fue importante porque preguntó al ministro si el embajador británico sabía de esta operación de la Fiscalía al momento de la detención de Santrich, porque sabemos que antes de la detención, en ese momento Santos con Néstor Humberto Martínez, llamaron a una reunión con los representantes diplomáticos de varios países para decir: "mira esto esta apunto de pasar, esta detención y vamos a mostrarles el por qué, aquí hay unas pruebas"

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Mostrándolo como algo sin poder debatir si había una acción criminal o no. Entonces, la pregunta es ¿también dijeron que había toda esta operación secreta? porque si no lo dijeron estaban mintiendo a todo este equipo de embajadores y diplomáticos.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

**CR: ¿Cómo evalúan también el rol del presidente Duque en torno a la veeduría e implementación del Acuerdo de paz, y luego de esta situación con Santrich?**

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

HD: Es clarísimo que si salen estos audios, para cualquier persona, sea presidente o una persona que no esta dentro de las esferas políticas, es algo preocupante. Se tendría que mirar bien qué ha pasado, qué hay en todos esos archivos, espiando a las personas, posiblemente al presidente, el vicepresidente, y lo repito, porque lo dije antes, ¿Por qué todo esto no fue compartido con la JEP, si realmente había un interés de una investigación criminal, si realmente había un interés de apoyar la JEP en su función?

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Reconociendo un momento muy difícil, estos archivos, estos audios se abrían compartido, y el hecho de que Duque no salga a decir cosas así, no sorprende desafortunadamente, pero creo que se puede ver como otra muestra más que no está poniéndose al lado de los que si apoyan al Acuerdo de Paz, a la JEP y la implementación.     

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

**CR: ¿Cuál es el llamado que se le hace a las disidencias de las FARC, quienes retornaron a las armas?**

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

HD: Creo que la mejor manera de crear el ambiente y las condiciones que conduzcan a un país en paz es que se implemente el acuerdo actual, si se muestra realmente un compromiso en implementarlo, yo creo que se puede generar un contexto social y político que favorezca las negociaciones y conversaciones con personas que crean que no tienen un espacio para ello.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Creo que se debe implementar el Acuerdo de paz, mejorar la situación de derechos humanos y abrir el espacio a conversaciones constructivas con los diferentes grupos armados, que en este momento están al margen de la ley.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

**CR: ¿Cuáles son los miedos desde el Parlamento británico y la comunidad internacional entorno al escalonamiento del conflicto armado en Colombia y cuáles son las afectaciones justamente a esa comunidad internacional?**

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

HD: Lo claro es que se esta viendo un aumento de violencia para los líderes sociales, se ve el aumento de inseguridad en el campo y el aumento de conflictos y de enfrentamientos entre los diferentes grupos armados.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

La violencia y la inseguridad en ciertas regiones de Colombia, el numero de denuncias que nos llegan a la organización Justice for Colombia de personas asesinadas, realmente le afecta a uno, me afecta a mi personalmente porque es muy constante. Y claro que hacemos lo que podamos desde Justice for Colombia, los diferentes parlamentarios con que tenemos relaciones y las personas en los movimientos sindicales.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Hay unas estructuras que ponen mucha atención a Colombia y muy comprometidos con apoyar a las organizaciones aliadas, trabajando en estos temas.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Muchas veces uno se siente muy triste y un poco frustrado, porque al final escribir cartas, hablar con los parlamentarios, presionarlos para que pongan debates y eso; todo eso sabemos que es bueno, pero enfrentando la extrema situación en la que estamos a veces se siente como muy poco. Nunca será suficiente hasta que haya realmente la libertad de vivir como uno debería vivir en Colombia.        

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

**CR: ¿Cuál será el papel de la comunidad internacional y puntualmente de Justice for Colombia entorno a la violencia en el país?**

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

HD: El Acuerdo de Paz, tiene en teoría 15 años de implementación, estamos en 4 años, es decir que todavía hay mucho tiempo en el que debemos mantener la atención en él; es decir, tenemos que seguir aumentando la voz de las organizaciones que están en Colombia. Hay muchas personas poniéndose en peligro para avanzar en la defensa de los derechos humanos, la paz, la justicia social, situaciones muy difíciles.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Creo que el papel de nosotros es seguir presionando a nuestros representantes políticos para que ellos presionen al gobierno colombiano, seguir invitando a las personas activistas en Colombia hablar con los representantes y las organizaciones sociales aquí para subir la voz de estas personas, creo que tenemos que seguir haciendo esto en términos generales.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

En términos específicos: seguir hablando y presionando para que haya una renovación de las negociaciones con el ELN, seguir hablando muy claro en defensa de los espacios de justicia transicional, en denuncia de las violencias de las fuerzas estatales e intentar seguir aumentando la red política que esta mirando a Colombia, porque las personas que sí están mirando en Gran Bretaña e Irlanda, están muy alarmadas por la situación actualmente.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Para ver el video del debate resumido haga clic [aquí](https://youtu.be/k-218GoKgAI)

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
