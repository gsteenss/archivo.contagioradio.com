Title: Pobladores y ambientalista rechazan construcción de base naval en Gorgona
Date: 2016-11-11 15:47
Category: Ambiente, Nacional
Slug: pobladores-ambientalista-rechazan-construccion-base-naval-gorgona
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/Gorgona.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: El Tiempo] 

###### [11 Nov. 2016] 

Diversas organizaciones ambientalistas y nativos de la región han manifestado su rechazo y preocupación ante la posibilidad de **construir una Estación de Guardacostas en el Parque Nacional Natural Gorgona. Esta sería ubicada muy cerca de corales únicos y en una zona que tiene vocación de conservación,** por lo que las posibles afectaciones ambientales que tendría para este territorio serían nefastas.

Este proyecto que lleva el nombre de 'Construcción, Operación, Abandono y Restauración de la Estación de Guardacostas en La Isla Gorgona y obras complementarias' logró su licencia el 31 de diciembre del 2015 y fue otorgada por la Autoridad Nacional de Licencias Ambientales (Anla).

**De realizarse esta construcción los pobladores y turistas se encontrarán con una torre metálica de 55 metros de altura y con unas instalaciones para que 28 personas de la Armada Nacional estén en plena zona de recuperación marina.**

Diversos grupos de ambientalistas han manifestado que se oponen a este proyecto y que en el, existe una falta de análisis de las corrientes oceánicas en relación con la obra, la cercanía de esta a corales únicos que se verían afectados por los sedimentos y la presión del transporte que se podría dar en esta área son otras preocupaciones del gremio.

**La Autoridad Nacional de Licencias Ambientales informó que Parques Nacionales Naturales consideró que el proyecto es viable y compatible con las necesidades de control** y vigilancia del área protegida y recomendó las obligaciones que debían imponerse para desarrollar la actividad.

**Organizaciones como Avaaz han emprendido acciones a través de su portal en el que invitan a la sociedad a firmar una petición dirigida al gobierno para que no continúe con este proyecto.** Así mismo han asegurado que “nos oponemos rotundamente a la puesta en marcha de este proyecto, pues hacerlo es poner en peligro 32 años de conservación y  recuperación natural, es exponer a un riesgo innecesario un frágil ecosistema que se ha venido adaptando al cambio climático ya presente y es afectar un lugar con un importante potencial turístico”.

Así mismo, la Academia Colombiana de Ciencias envío una carta al Presidente Juan Manuel Santos para que éste reconsidere la construcción de dicha base en la isla protegida. Y añadieron que realizan este proyecto generará “muy severos impactos ambientales a estos ecosistemas únicos” y que **esta decisión “no está en armonía con la política del Gobierno en materia de ampliación de áreas protegidas”.**

###### Reciba toda la información de Contagio Radio en su correo [[bit.ly/1nvAO4u]
