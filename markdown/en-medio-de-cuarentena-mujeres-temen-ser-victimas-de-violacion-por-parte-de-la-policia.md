Title: En cuarentena mujeres temen ser víctimas de violación por parte de la Policía
Date: 2020-04-04 10:32
Author: CtgAdm
Category: DDHH, Mujer
Tags: covid19, violacion
Slug: en-medio-de-cuarentena-mujeres-temen-ser-victimas-de-violacion-por-parte-de-la-policia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/04/image.png" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/04/image-1.png" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

El pasado 28 de marzo una mujer en la localidad de Bosa, Bogotá denunció haber sido víctima de violencia sexual por parte de un policía. Esta semana se han conocido dos casos más en donde mujeres denuncian ser víctimas de acoso y violencia sexual. Organizaciones feministas afirman que no puede ser que en medio de la cuarentena mujeres **teman ser víctimas de violación por parte de la Fuerza Pública.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El segundo caso de violencia sexual se habría presentando en el CAI de Laureles, también en la localidad de **Bosa**. De otro lado, los hechos de acoso sexual se presentaron en la ciudad de **Cali,** cuando una mujer sacaba a su mascota.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Para Adriana Benjumea, directora de la Corporación Humanas "es más grave que te viole un policía a que te viole un particular porque ese policía, no solo no podía cometer ese delito, sino que tenía la obligación de cuidarte de que alguien más lo cometiera". (Le pude interesar: ["Tres propuestas de universitarias para erradicar la violencia de género"](https://archivo.contagioradio.com/tres-propuestas-de-universitarias-para-erradicar-la-violencia-de-genero/))

<!-- /wp:paragraph -->

<!-- wp:heading -->

¿En dónde está el enfoque de género de la Policía Nacional?
-----------------------------------------------------------

<!-- /wp:heading -->

<!-- wp:paragraph -->

La directora de la organización enfatiza en que el abuso de la fuerza pública y la brutalidad policial son recurrentes y evidencian la deuda en la protección a los derechos de las mujeres. "No puede ser que las mujeres no tengamos solamente miedo a contagiarnos de covid-19, sino que además, **tengamos miedo de encontrarnos a un policía en a calle**" asegura.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Asimismo, expresa que frente al estado de excepción hay permisiones a la Fuerza Pública, que en ningún caso pueden ser violatorias de derechos humanos. Motivo por la cual cuestiona la formación que se ha dado a integrantes de la Policía y la ausencia de articulación entre entidades para establecer políticas de género.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

"Hay que hacer un trabajo coordinado con los entes vigilantes de derechos humanos y la Alcaldía distrital en este caso. Hay que permitir la discusión de cómo el deber de garantía de la Fuerza Pública le pone mayor exigibilidad en el respeto a los derechos humanos" asevera.

<!-- /wp:paragraph -->

<!-- wp:heading -->

A mi me cuidan mis amigas
-------------------------

<!-- /wp:heading -->

<!-- wp:paragraph -->

La defensora afirma la línea 123 o a la línea púrpura están recibiendo todo tipo de denuncias basadas en violencia de género. De igual forma manifiesta, que en medio de esta crisis, las organizaciones feministas continúan trabajando para garantizar los derechos de las mujeres.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

"Las organizaciones de mujeres y feministas estamos acompañando a las mujeres en muchas de sus denuncias y garantizando que las entidades públicas efectivamente respondan". (Le pude interesar: ["Pronunciamiento de la Corporación Humanas sobre cárceles"](https://humanas.org.co/alfa/10_454_Pronunciamiento-de-la-Corporacion-Humanas-sobre-la-grave-situacion-presentada-el-dia-21-de-marzo-de-2020-en-las-prisiones-de-Colombia.html))

<!-- /wp:paragraph -->
