Title: Dirección Nacional de Inteligencia estaría detrás de falsos positivos judiciales contra oposición
Date: 2019-07-06 17:16
Author: CtgAdm
Category: Nacional, Política
Tags: Barreras, Cepeda, DNI, Falsos Positivos Judiciales, oposición, Sanguino, Senadores
Slug: direccion-nacional-de-inteligencia-contra-oposicion
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/Fotos-editadas-1.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio  
] 

En una carta envíada al presidente Iván Duque, los senadores **Iván Cepeda, Roy Barreras y Antonio Sanguino,** denunciaron que estan siendo víctimas de seguimientos ilegales, con la intención de generar procesos judiciales con bases falsas. En la misiva, los congresistas dicen que los seguimientos estarían siendo ordenados por el almirante **Rodolfo Amaya Kerquelen, quien preside la Dirección Nacional de Inteligencia (DNI)**. (Le puede interesar:["Iván Cepeda reafirma que Fiscal preparaba investigación en su contra"](https://archivo.contagioradio.com/ivan-cepeda-reafirma-que-fiscal-preparaba-investigacion-en-su-contra/))

Los tres congresistas, que han sido claves en impulsar la implementación del Acuerdo firmado en el Teatro Colón e integran la plataforma Defendamos la Paz, dicen que gracias a fuentes cercanas se enteraron de los seguimientos ilegales. En el último hecho del cual tuvieron conocimiento, **una fuente comentó a los senadores que funcionarios del DNI se habrían reunido el pasado 30 de marzo en Bogotá, para adelantar acciones de vigilancia ordenados por el director de la Entidad, al parecer en la lista estarían diez personas, pero solo se conocen los nombres de los tres senadores. **

Sanguino aseguró que "Se espera que sea la presidencia quien conteste esta carta y no la DNI, a la presidencia le corresponde una labor de contra inteligencia, sobre la propia Dirección Nacional de Inteligencia para corroborar la información que le hemos hecho saber al presidente”

La finalidad de esta órden sería iniciar procesos judiciales con bases falsas, conocidos como falsos positivos judiciales. Ante estas informaciones, los líderes políticos **pidieron al Presidente que se retire de la Dirección Nacional de Inteligencia al almirante Amaya Kerquelen mientras se investigan las denuncias;** así mismo, pidieron que se respete el ejercicio de la oposición, pues este tipo de actos "son usuales en regímenes despóticos en los que se acalla a la oposición, pero no en regímenes democráticos".

También el senado adicionó que “esperamos es que al más alto nivel se ordene una investigación sobre estos hechos y se clarifique si efectivamente esto está ocurriendo desde un organismo como la DNI, que se creó precisamente para superar el episodio de las chuzadas y la persecución política”

### **Es grave que se persiga a la oposición, pero sería aún más grave si esa persecución viene del Estado**

No es la primera vez que congresistas de la oposición hacen una denuncia como esta, pues anteriormente Cepeda había dicho públicamente que se estarían realizando acciones tendientes a propiciar montajes judiciales en contra suya e integrantes de su familia. Sobre un hecho similar, hace apenas un mes, los senadores Cepeda y Sanguino realizarón un debate de control político en el que cuestionaban las operaciones de la Administración para el Control de Drogas (DEA por sus siglas en inglés) en Colombia.

Durante el evento, ambos congresistas sostuvieron que esta Agencia estaba haciendo operaciones de entrampamiento que eran ilegales en la Constitución colombiana, y adelantando otras acciones sin el debido proceso. De igual forma, indicaron que este tipo de ilegalidades se habrían utilizado contra Jesús Santrich y otros capturados, en operaciones conjuntas con la Fiscalía que dirigía Néstor Humberto Martínez. (Le puede interesar:["Congresistas denuncian que DEA violó la soberanía colombiana en caso Santrich"](https://archivo.contagioradio.com/denuncian-dea-violo-soberania-santrich/))

Por esta razón, en la carta los Congresistas concluyerón que **"grave resulta que fuerzas oscuras ilegales persigan e intimiden a contradictores políticos, pero más grave serían aún que en dichas maniobras participen altos agentes y agencias del Estado".  
**

Con esto dos casos aumenta a **19 el número de líderes sociales y defensores de Derechos Humanos, asesinados**  en lo corrido del año 2020 en Colombia.

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
