Title: Persecución a pobladores de La Macarena en desarrollo de Operación Artemisa
Date: 2020-02-24 19:36
Author: CtgAdm
Category: DDHH, Nacional
Tags: agresiones, campesinos, deforestación, Duque, La Macarena
Slug: persecucion-a-pobladores-de-la-macarena-en-desarrollo-de-operacion-artemisa
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/02/ERj7C2JXUAEPF_k.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

*Foto: Archivo de Contagio Radio*

<!-- /wp:paragraph -->

<!-- wp:html -->

<figure class="wp-block-audio">
<audio controls src="https://co.ivoox.com/es/sebastian-escobar-sobre-persecucion-a-pobladores-la_md_48223520_wp_1.mp3&quot;">
</audio>
  

<figcaption>
Entrevista &gt; Sebastian Escobar | Abogado integrante (CAJAR)

</figcaption>
</figure>
<!-- /wp:html -->

<!-- wp:paragraph -->

Desde hace dos semanas la serranía de La Macarena arde y no solo por las llamas que arrasan con casi 80 hectáreas de área silvestre protegida; también por las **múltiples agresiones por parte de integrantes del Ejército, la Policía y ESMAD hacia las personas que habitan** esta zona. (Le puede interesar: <https://archivo.contagioradio.com/plan-artemisa-habria-arrancado-con-desplazamiento-forzado-en-chiribiquete/>)

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Los habitantes de **El Rubí, Picachos y Tinigua en la vereda Caño San Juan y el Tapir** se han visto afectados durante los últimos días por la intervención de la Fuerza Pública, que busca sacarlos del territorio con la excusa de recuperar y proteger el Parque Nacional Natural Tinigua en **ejecución del Plan Artemisa**.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Durante el fin de semana **fueron capturadas 20 personas, 8 mujeres y 12 hombres, presuntamente por delitos ambient**ales, según la información de las fuerzas militares, argumentó que se contrasta con el Comite Cívico por los Derechos Humanos del Meta (CCDHM), quien afirma que con estas operaciones desconocen a los habitantes de estos territorios.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Ante ello el abogado Sebastian Escobar, integrante del Colectivo de Abogados José Alvear Restrepo (CAJAR) afirmó: *"vemos con preocupación la aplicación de la Operación Artemisa, donde se ven agresiones de la comunidad campesina, con una política de corte netamente militar"*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por otro lado Sebastian Gómez integrante de Centro de Alternativas al Desarrollo (Cealdes) destacó que esta no es una situación aislada y que corresponde a una serie de falencias en el territorio, *"desde 2016 se han incrementado los hechos violentos que obedecen a una serie de condiciones que el Gobierno Nacional termina obviando de las causas reales".*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Las agresiones de la Fuerza Púbica también ha afectado ***las viviendas, bienes y cultivos de pan coger** de los pobladores*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Según el CAJAR una comisión de verificación determinó en el 2019 que los predios habitados por los campesinos no estaban ubicados en zonas protegidas. (Le puede interesar: <https://www.coljuristas.org/nuestro_quehacer/item.php?id=282>

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/DefensoriaCol/status/1231378354516918272","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/DefensoriaCol/status/1231378354516918272

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:heading {"level":3} -->

### Retención a integrantes del Ejército en La Macarena

<!-- /wp:heading -->

<!-- wp:paragraph -->

Aceneth Castro lideresa del territorio declaró ante las retenciones por parte de la comunidad a integrantes del Ejército, que *"la comunidad retuvo a un cabo y un soldado del Ejercito, al verse completamente agredidos y vulnerados, **... ellos solo se defendían con palos, ninguno actuó con arma de fuego, su intención era defenderse no agredir a nadie**"*.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Ante el olvido estatal Gómez destacó que CEALDES lleva varios años haciendo análisis a las organizaciones presentes en el territorio, *"donde el Estado no ha hecho presencia, las comunidades realizaron acuerdos para reducir la deforestación, reglamentar el acceso al agua, gestionar la cacería, es decir, hay un andamiaje comunitario para evitar impactos ambientales"*.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Y concluyó diciendo que, "*con el ingreso de personas ajenas a la región se han generado nuevos conflictos; Por ejemplo, la falta de aplicación del PNIS, lo que ha representado la llegada de colonizadores de otras partes del país a el PNN Tinigua, al igual que el ingreso de capitales externos a la región que han adquirido una gran cantidad de tierras y empleado a campesinos por bajos costos para ejecutar sus acciones"*.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### ¿Qué es el Plan Artemisa?

<!-- /wp:heading -->

<!-- wp:paragraph -->

Esta operación fue presentada a finales de abril de 2019 como parte de las acciones ambientales del gobierno de Duque; una estrategia principalmente militar, que pretende por medio de la Fuerza Pública efrenar la deforestación en el país, recuperar la selva húmeda tropical y judicializar a quienes están detrás de la tala y quema de bosque.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

La primera etapa de esta Operación se implementó inicialmente en los Parques Chiribiquete, Macarena, Tinigua y Picachos, posteriormente se aplicará al resto del país donde se registra deforestación.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->
