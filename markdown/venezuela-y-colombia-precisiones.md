Title: Venezuela y Colombia: Precisiones
Date: 2015-08-31 12:05
Category: Cesar, Opinion
Tags: Crisis en la frontera Colombia Venezuela, Desplazamiento de colombianos a Venezuela, Paramilitarismo y contrabando en la frontera
Slug: venezuela-y-colombia-precisiones
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/08/bande.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [31 Ago 2015] 

#### **[César Torres Del Río](https://archivo.contagioradio.com/cesar-torres-del-rio/)** 

[La frontera, el común talón de Aquiles de los dos países, es hoy  territorio en el que se adelantan medidas biopolíticas de control social.  Tradicionalmente ha sido espacio de olvido, de inseguridad ciudadana, de corrupción politiquera y de enriquecimiento ilícito por parte de capas medias y elites asociadas al poder regional, de paramilitarismo y de prácticas sociales - contrabando y reventa de gasolina entre otras -  que en parte responden simplemente a la necesidad físico-mental de sobrevivir.]

[ Miles de colombianos se fueron a Venezuela porque aquí en Colombia no se les ofrecieron oportunidades básicas de empleo, de educación, de vivienda; allá obtuvieron mejores condiciones de vida. La causal directa y primaria de tal situación debe ser atribuida a la gestión irresponsable de los sectores dirigentes -  de lado y lado - siempre en permanente búsqueda de la ganancia y de la satisfacción individuales en medio de las virtudes “mágicas” del mercado, nunca cuestionado y por siempre idolatrado.]

[Ahora se llegó a un punto límite y  la crisis social y humanitaria es un hecho. La elite socioeconómica colombiana, que posa de representativa de nuestra pluralidad, ha levantado una cortina de humo sobre su propia responsabilidad y enfila baterías diplomáticas en UNASUR y la OEA fungiendo como víctima frente a Venezuela. El discurso patriotero de la Cancillería y la Presidencia, de Holguín y de Santos, con pretensiones de “nacional”, es parte de su escasa y débil utilería política y mediática.]

[Diferenciándonos del gobierno hemos manifestado nuestra solidaridad con los colombianos maltratados y expulsados por la Guardia Nacional de Venezuela; también hemos criticado y rechazado las marcaciones colocadas en las fachadas de las casas, de los ahora deportados, con la “D” de demolición y la “R” de revisión; nos distanciamos inmediatamente de una medida represiva como la del “Estado de Excepción” que, además, ha implicado la militarización de la frontera. En particular hemos dejado en claro que NO apoyamos al gobierno de Santos y no lo haremos en el futuro inmediato, y mucho menos para las elecciones de octubre, incluidas las alianzas con sectores de la izquierda, en las que se utilizará la retórica patriotera con fines politiqueros de afianzamiento regional y nacional.]

[En cuanto al gobierno de Nicolás Maduro digamos lo siguiente. Los métodos utilizados contra los colombianos, por tenebrosos que hayan sido, nada tienen de fascistas o de hitlerianos. Decirlo es el recurso lógico de la estulticia; “argumentarlo” es desconocer el sentido histórico de la modernidad. La tortura, el asesinato y el desplazamiento forzado fueron prácticas anteriores a Mussolini y Hitler; la tortura en Guantánamo a los presos “no personas” hoy se ve como acción de “héroes”; y los bombardeos israelitas, también con marcaciones, en la Franja de Gaza se encubren con la lucha contra el terrorismo y en nombre de la democracia y la religión. La solución final antisemita, la Shoah, se basó en el biologismo racial.]

[El régimen político venezolano, desde Hugo Chávez, es claramente nacionalista-popular, antimperialista en el discurso y en algunas acciones frente a los Estados Unidos (aunque errado en el tratamiento a ciertos regímenes, como el de Siria e Irán). Ejerce como gobierno en el marco de un Estado burgués, porque es la burguesía la que hoy le imprime su sello de clase y mantiene el control económico permitiéndole a las multinacionales compartir buena parte de la renta petrolera. Se apoya tanto en las Fuerzas Armadas como en la movilización popular para mover sus hilos en la telaraña política; según la coyuntura apela a aquellas  o a ésta, o a ambas como en el golpe del 2002.]

[Pero Nicolás Maduro no es representante o agente de esa burguesía; si así fuera no tendría a socialdemocracia y a la oposición de derecha interna (Leopoldos, Antonios, María Corinas y demás) e internacional  “cazándolo”. Pese al autoritarismo en curso, al remedo de participación directa del ciudadano,  al verticalismo del PSUV y su democracia desde arriba, el Chavismo, y con él Maduro, ha ampliado el marco de la movilización popular. Hay en curso una “emergencia plebeya” que se controla y evita desde el Estado y el gobierno; la denominada “transición” es tan sólo una categoría intelectual que no corresponde a la realidad contradictoria del proceso. Con todo, no pedimos la caída de Maduro.]

[Para terminar, nos parece que lo adecuado es la negociación político-diplomática para zanjar la crisis en la frontera. Las vías militares y la legislación de excepción son tan negativas como la retórica guerrerista y patriotera, la de ambos lados.]
