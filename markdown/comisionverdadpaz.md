Title: 160 organizaciones y personas respaldan la constitucionalidad de la Comisión de la verdad
Date: 2017-05-13 15:55
Category: Nacional, Paz
Tags: acuerdo de paz, colombia, comision, verdad
Slug: comisionverdadpaz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/05/C_qwdIsW0AAgLqA.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto:MOVICE 

###### 13 May 2017 

Organizaciones y miembros de la sociedad civil, dirigieron un comunicado a la opinión pública y a la Corte Constitucional en el que **expresan su respaldo a la Comisión para el esclarecimiento de la verdad CEV**, buscando que se declare la constitucionalidad de la figura creada por el Congreso mediante el artículo 2 del Acto Legislativo 01 de 2017 y organizada por el Decreto Ley 588 de 2017.

Las personas y organismos firmantes, varios de los cuales pertenecen a la veedora Mesa por la verdad, aseguran que su creación "**cumple con las expectativas y requerimientos de los diferentes movimientos sociales y de víctimas**, y muestra un progreso en el compromiso del Estado (...) de garantizar el derecho a la verdad de las víctimas del conflicto y la sociedad"

Recordaron que desde antes del inicio de las negociaciones de paz con las FARC EP, existió el reclamo por una institución independiente que, además de la búsqueda de verdad, **tenga como mandato "la convivencia y la no repetición, como elementos fundamentales para la construcción de un futuro en paz"**, destacando que es un "mecanismo urgente" para la sociedad y para la implementación del Acuerdo Final al formar parte del Sistema Integral de Verdad, Justicia, Reparación y No Repetición.

"La CEV es indispensable en la medida en que está enfocada en la **dignificación y reconocimiento de las víctimas, y en la garantía de los derechos de la sociedad colombiana** a la verdad, a la memoria y, en consecuencia, a la convivencia y la no repetición", afirman destacando la legitimidad que organizaciones civiles nacionales e internacionales, han dado con su apoyo a la propuesta.Le puede interesar: [Declarado crimen de lesa humanidad caso de Mario Calderón y Elsa Alvarado](https://archivo.contagioradio.com/declarado-crimen-de-de-lesa-humanidad-caso-de-mario-calderon-y-elsa-alvarado/).

El llamado de los firmantes es a que se consolide y se ponga en funcionamiento urgente la Comisión, avalada por la viabilidad que le otorga el Decreto Ley 588 de 2017, que ayuda además a garantizar la participación de diferentes sectores sociales,"con una perspectiva territorial, diferencial, de género y étnico", insistiendo en que "**sin la verdad la paz no será posible por ser esta la primera víctima de la guerra**".
