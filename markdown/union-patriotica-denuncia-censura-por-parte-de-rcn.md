Title: Unión Patriótica denuncia censura por parte de RCN
Date: 2015-05-06 17:51
Author: CtgAdm
Category: DDHH, Nacional, Política
Tags: Aida Avella, Amenazas, Canal RCN, Censura, cese bilateral del fuego, Unión Patriótica
Slug: union-patriotica-denuncia-censura-por-parte-de-rcn
Status: published

###### Foto: Contagio Radio 

La Unión Patriótica ha denunciado por medio de sus líderes al canal RCN por censurar un vídeo institucional que debía ser transmitido el día 5 de mayo de 2015 antes del noticiero de las 7 P.M. en una franja que es propiedad del Consejo Nacional Electoral.

La reacción inmediata por parte de la unión patriótica fue informar por medio de sus redes sociales que el vídeo no se había transmitido como se acordó desde un principio y publicarlo por su propia cuenta, los realizadores fueron los primeros en oponerse ante este hecho asegurando que habían sido censurados por el contenido del mismo.

Por su parte, RCN respondió a las acusaciones afirmando que por razones técnicas no les había sido posible transmitir el material pero que no existía tal censura. Sin embargo, La UP afirma que el canal ha tenido un cambio editorial por lo que exige que se les garantice que no serán censurados.

El vídeo es un corto promocional de divulgación política de la Unión Patriótica, que expone el conflicto interno del país y que se pronuncia a favor del cese bilateral del fuego.

\[embed\]https://www.youtube.com/watch?v=HqfyK5h1Bu8\[/embed\]  
 
