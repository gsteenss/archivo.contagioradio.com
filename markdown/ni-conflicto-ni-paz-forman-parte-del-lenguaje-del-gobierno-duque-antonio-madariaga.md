Title: Ni conflicto ni paz forman parte del lenguaje del Gobierno Duque
Date: 2018-08-15 17:29
Category: Paz, Política
Tags: Antonio Madariaga, Iván Duque, Seguridad Democrática, Viva la ciudadanía
Slug: ni-conflicto-ni-paz-forman-parte-del-lenguaje-del-gobierno-duque-antonio-madariaga
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/10/marcha-por-la-paz64.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [15 Ago 2018] 

**“Ni conflicto ni paz forman parte del lenguaje o referente del gobierno Duque”**, así lo manifestó Antonio Madariaga, asesor de Viva la Ciudadanía, luego de que se conocieran los cambios de denominación que se le dio a la Alta Consejería para el postconflicto, que ahora se llamará Alta Consejería para la Estabilización; del Alto Comisionado para la paz, que pasará a ser el Alto Comisionado para la legalidad; y de la Consejería Presidencial para los derechos humanos, a Consejería para las relaciones internacionales y Derechos Humanos.

Madariaga afirmó que mientras no haya un decreto que modifique la estructura de esas instituciones, las denominaciones no pueden ser cambiadas. Sin embargo, el analista señaló que desde lo simbólico, el mensaje que se está transmitiendo es que “**este no es el gobierno que puede ser mirado desde el lente de la paz”**, sino que estaría marcado por los conceptos de Justicia y seguridad desarrollados en la seguridad democrática.

Asimismo, señaló que también se estaría enviando un mensaje en términos administrativos y presupuestales en donde se podría entender que existirá un privilegio **“por aquellas acciones, programas y relación con la ciudadanía que este enmarcada por las necesidades del Estado y del gobierno”**, y no por la garantía de los derechos a la población.

Otro de los mensajes que estaría dirigido a todo el Sistema de Verdad Justicia Reparación y no Repetición, tendría que ver con la posible insuficiencia de recursos para “atender las necesidades o compromisos que se deriven de la implementación de los Acuerdos de Paz” y el uso de modificaciones, de carácter administrativo, que impongan cargas o trámites burocráticos que dificulten la acción de esa arquitectura institucional.

### **El mensaje al movimiento Social** 

A este escenario se suman las afirmaciones hechas por el ministro de defensa, Guillermo Botero que hizo un llamado a que las Fuerzas Militares reactividad su capacidad de combate de intervención, declaraciones que según Madariaga permiten establecer que, por ejemplo, la protesta social será tratada como un tema de orden público y no de exigencia y garantías a derechos humanos. (Le puede interesar: ["Colombia y sus dos presidentes: análisis de Fernando Giraldo")](https://archivo.contagioradio.com/colombia-y-sus-dos-presidentes-analisis-de-fernando-giraldo/)

Madariaga señaló que debe existir una “preocupación” desde todos los sectores debido al carácter de este gobierno y así evitar retrocesos no solo en la implementación de los Acuerdos de paz, sino en la defensa de los derechos humanos y agregó que en ese sentido es importante generar canales de comunicación con el gobierno Duque que reconozca al movimiento social y **a las organizaciones defensoras de derechos humanos como legítimos interlocutores, en un diálogo sobre el proyecto de país**.

<iframe id="audio_27880277" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_27880277_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
