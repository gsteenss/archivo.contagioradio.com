Title: Con firmatón solicitan a líderes mundiales la prohibición del Fracking
Date: 2015-11-13 17:05
Category: Ambiente, Nacional
Tags: Cumbre del clima en París, Ecologistas en Acción, españa, fracking, fractura hidráulica, La conferencia de las partes COP21, movimiento internacional Global Frackdown
Slug: con-firmaton-solicitan-a-lideres-mundiales-la-prohibicion-del-fracking
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/11/Fracking.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: canalcapital 

###### [13 nov 2015]

En el marco de la realización de la Conferencia de las Partes, la COP 21, que se desarrollará entre el 30 de noviembre al 11 de diciembre, **1250 organizaciones de 64 países han firmado una carta para pedir a los líderes mundiales que se llegue a un acuerdo sobre la prohibición del fracking.**

Al mandatario español Mariano Rajoy le han llegado peticiones de rechazo de parte de 74 organizaciones y plataformas que rechazan la fractura hidráulica y solicitan que se pacte un compromiso para dejar la mayor parte de los combustibles fósiles en el subsuelo, generando una transición hacia un futuro con energía 100% renovable para frenar el calentamiento global.

España se suma al movimiento internacional Global Frackdown debido que el presidente Rajoy ha impulsado proyectos de extracción de gas mediante fractura hidráulica en diferentes regiones de ese país.

Por esta razón se han desarrollado movilizaciones debido a que la mayoría de los ciudadanos rechazan esta técnica por las consecuencias que esa prática conlleva, tales como la ** contaminación de acuíferos, afecciones de la salud y emisiones de gases de efecto invernadero, entre otras.**
