Title: 42 familias que retornaron a Hacienda Bella Cruz  se encuentran en riesgo de desalojo
Date: 2015-08-28 16:52
Author: AdminContagio
Category: DDHH, Nacional, Resistencias
Tags: ASOCADAR, cesar, Desalojo, Desplazamiento forzado, ESMAD, recuperación de tierras, Sur de Bolivar
Slug: 42-familias-que-retornaron-a-hacienda-bella-cruz-se-encuentran-en-riesgo-de-desalojo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/08/desplazamiento.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: [angiemaya95.blogspot.com]

###### [28 Ago 2015]

Las **42 familias del municipio de Pelaya** y sus alrededores, en el departamento del Cesar, nuevamente **se encuentran en peligro de desalojo violento por parte de la policía,** tras haber realizado hace algunos meses la recuperación pacífica sus tierras  que les fueron arrebatadas a manos de grupos paramilitares en 1996.

A pesar de que el pasado 23 de julio, la Sala 5ta de Revisión de Tutelas de la Corte Constitucional ordenó “a las autoridades de Policía que se abstengan de llevar a cabo cualquier acto de hostigamiento en contra de tal población, y por el contrario, proteger la vida, integridad, debido proceso e intimidad de la población desplazada de la Hacienda Bellacruz”, la comunidad denuncia que **los amedrantamientos en su contra, continúan, en la noche del jueves las familias se atemorizaron  nuevamente tras escuchar disparos,** producto de la presencia del Ejército que se encuentra muy cerca a la comunidad.

Por otro lado,  las familias aseguran que las condiciones en las que están viviendo no son adecuadas y se les está acabando la comida, pese a que el Estado no les ha brindado ningún tipo de ayuda para poder vivir dignamente, los campesinos han empezado la realización de proyectos productivos capacitándose con el SENA, para poder alimentarse de manera autónoma.

Es por eso que las familias exigen que el Estado colombiano la ayuda necesaria para el acatamiento del fallo de **la Corte Constitucional, que obliga a las fuerza pública a detener sus acciones violentas**. Así mismo exigen que se les brinden los mecanismos establecidos por la ley, que ordenan brindar especial protección a la vida e integridad física de las personas víctimas de la violencia, que se hallan ahora re haciendo sus vidas.

Aunque las familias viven en constante riesgo de ser desalojadas a la fuerza, Adelfo Rodríguez, integrante de ASOCADAR, asegura que la comunidad no se irá del predio y **continuarán resistiendo defendiendo su derecho sobre esas tierras tierras.**
