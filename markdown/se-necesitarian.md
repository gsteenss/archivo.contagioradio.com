Title: Se necesitarían 45 votos para que la JEP sea aprobada en el próximo debate
Date: 2017-11-02 16:17
Category: Nacional, Paz
Tags: Acuerdos Paz, FARC, Jurisdicción Espacial de Paz
Slug: se-necesitarian
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/11/la-fm.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: La FM] 

###### [02 Nov 2017] 

El próximo martes se reanudará la plenaria del senado en donde se espera, que luego de 3 semanas de intentar aprobar la Jurisdicción Especial para la paz y de que ayer tampoco existiera quorum para llevar acabo el debate, por fin sea sancionada el marco jurídico que da soporte a los Acuerdos de paz de La Habana. **Bastarían 45 votos para que sea aprobada en el próximo debate.**

De acuerdo con Victoria Sandino, integrante del partido político FARC, lo que sucedió el día de ayer con la falta de voluntad política para llevar a cabo el trámite y torpedear la implementación del acuerdo de paz, **es preocupante porque continúa dilatando el tiempo de Fast Track**.

“El concejo político nacional se reunión el día martes y declaró alerta máxima por justamente lo que pasa en el congreso y la falta de voluntad de la clase política dirigente de este país, que **sin importar el fallo de la Corte Constitucional evade la responsabilidad que tiene**” afirmo Sandino. (Le puede interesar: [" Se acaba el tiempo del Fast Track y toda la presión va al Congreso"](https://archivo.contagioradio.com/se-acaba-el-tiempo-del-fast-track-y-toda-la-presion-va-al-congreso/))

Por su parte el senador Iván Cepeda del Polo Democrático afirmó que la sesión de ayer dedicó buena parte de la jornada a votar impedimentos debido a que hay **44 congresistas que se han declarado impedidos por tener familiares en distintos grados de consanguinidad y afinidad vinculados al paramilitarismo en los hechos conocidos en Colombia como parapolítica** o por ellos mismos estar siendo investigados.

De igual forma señaló que el presidente del Senado manifestó ayer haciendo alusión al artículo 134 de la Constitución, que conforme ha ido disminuyendo el número de personas por impedimentos que pueden votar y participar en esta ley estatutaria, también debe irse reconfigurando**, lo que significaría que hacen falta 45 votos para garantizar la votación.**

Finalmente, Cepeda señaló que estas tensiones y dilataciones hacen parte de un lado del temor que tienen sectores políticos por la Justicia para las víctimas y del otro, por la carrera electoral para la presidencia 2018 que ya comenzó. (Le puede interesar: ["Se está usando el soborno para que los congresistas hagan lo que deben hacer: Imelda Daza"](https://archivo.contagioradio.com/congresistas_jurisdiccipn_especial_paz_imelda_daza/))

<iframe id="audio_21847736" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_21847736_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
