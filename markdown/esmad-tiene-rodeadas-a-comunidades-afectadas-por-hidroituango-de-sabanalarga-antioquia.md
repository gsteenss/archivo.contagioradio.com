Title: ESMAD tiene intimidadas a comunidades afectadas por Hidroituango en Sabanalarga, Antioquia
Date: 2018-04-05 13:16
Category: DDHH, Nacional
Tags: amenazas a líderes sociales, Antioquia, ESMAD, Hidroituango, Movimiento Ríos Vivos, Sabanalarga
Slug: esmad-tiene-rodeadas-a-comunidades-afectadas-por-hidroituango-de-sabanalarga-antioquia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/04/hidroituango-e1522947209859.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Movimiento Ríos Vivos] 

###### [05 Abr 2018] 

Desde el Movimiento Ríos Vivos denunciaron que desde el 3 de abril se han presentado **operativos policiales** en la playa El Arenal en el municipio de Sabanalarga, Antioquia. Allí se viene desarrollando el proyecto hidroeléctrico Hidroituango, y justamente por eso, Ríos Vivos indica que el ESMAD tiene rodeadas a las comunidades que se han resistido a la tala de árboles y que han recibido amenazas por parte de la Gobernación de Antioquia.

De acuerdo con la denuncia, ha habido una **violación sistemática de los derechos humanos** de las comunidades que están siendo afectadas por Hidroituango. En esta fase del proyecto, las personas y la naturaleza han sido víctimas de las actividades de las empresas EPM y de Refocosta, la encargada de realizar la tala del bosque. Dichas compañías son las que han enviado al ESMAD al territorio.

En el comunicado de Ríos Vivos, afirman que el escuadrón antidisturbios “de manera abusiva pidió escrituras de las playas a los barequeros que las han habitado ancestralmente, impusieron a la gente que tenían que dejar que se cortaran los árboles que les rodean, y que el riesgo era culpa de las mismas comunidades por estar en predios ajenos y que no respondían por quien quedará herido”.

### **ESMAD está obligando a las comunidades a dejar talar árboles** 

Además, denuncian que la Fuerza Pública y las empresas contratista del proyecto hidroeléctrico **“dejaron si agua a la comunidad”** y entraron arbitrariamente a las viviendas de las personas. Por esto, indicaron que el derecho a la intimidad fue vulnerado y cualquier pérdida que sufran las comunidades será responsabilidad de la Policía”. (Le puede interesar:["Memoria y resistencia en el cañón del Río Cauca"](https://archivo.contagioradio.com/memoria-y-resistencia-en-el-canon-del-rio-cauca/))

De acuerdo con Isabel Cristina Zuleta, vocera del Movimiento Ríos Vivos, “las comunidades están viviendo una situación tensa con el ESMAD”, y agrega que, de realizarse la tala de árboles, las comunidades están en riesgo teniendo en cuenta que ya ha habido incidentes con los árboles **cayendo sobre las viviendas de las personas**.

### **Ríos Vivos denunció amenazas de un funcionario de la Gobernación de Antioquia** 

Zuleta afirmó que se había llegado a una negociación con la Gobernación de Antioquia “por medio de un acuerdo departamental y los acuerdos se han incumplido por parte de la Gobernación”. Afirmó que el funcionario de derechos humanos de Antioquia, Carlos Mario Vanegas, estuvo en la zona y “se dedicó a desprestigiar al Movimiento, a decir que no se va a lograr ninguna negociación e incluso dijo que **no iba a olvidar los rostros** de las personas que lo interpelaban”.

Al movimiento le preocupa que el funcionario “le expresó a una compañera del Movimiento Ríos Vivos **que la iba a ver enterrada** en las playas del Río Cauca porque iba a llegar la inundación y no iba a pasar absolutamente nada”. Para Zuleta, estas afirmaciones del funcionario son la evidencia de que “nadie responde por nuestras vidas y se confirma que lo que está haciendo EPM y la Gobernación es una encerrona en contra nuestra”, expresan.

### **Flora y Fauna está siendo gravemente afectada** 

El Movimiento afirmó que esa zona de Sabanalarga es el único lugar del bosque seco tropical al que acuden diferentes especies animales. En repetidas oportunidades han dicho que “los animales como **las guacamayas se están muriendo** de hambre porque han acabado con los árboles de los cuales ellos se alimentan”.

Asimismo, señalan que Hidroituango está dejando sin sitio a una especie tan importante como el jaguar debido a que **“lo está acorralando”**. Si bien Corantioquia ha evidenciado esta situación, no se lo ha hecho expreso a Hidroituango, pero además, “la empresa le ha dicho a las comunidades que son ellas las que deben proteger a los animales”, denuncia Zuleta. (Le puede interesar:["En zona de Hidroituango habría más de 2 mil personas desaparecidas"](https://archivo.contagioradio.com/en-el-canon-del-rio-cauca-afectado-por-hidroituango-hay-mas-de-2-mil-personas-desaparecidas/))

Finalmente, la líder ambientalista resaltó que las amenazas por parte de la Gobernación y de la empresa Refocosta son permanentes y **“ninguna autoridad ha dado respuesta”**. Hizo un llamado a las autoridades para que protejan a las comunidades e invitó a la ciudadanía a que firmen [la petición](https://secure.avaaz.org/es/petition/Juan_Manuel_Santos_Presidente_de_Colombia_Jorge_Londono_Gerente_de_EPM_Detengan_represa_Hidroituango_destruye_fosas_con_/) para que el Gobierno Nacional detenga la represa de Hidroituango “para encontrar a los desaparecidos”.

<iframe id="audio_25133107" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_25133107_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 

<div class="osd-sms-wrapper">

</div>

 
