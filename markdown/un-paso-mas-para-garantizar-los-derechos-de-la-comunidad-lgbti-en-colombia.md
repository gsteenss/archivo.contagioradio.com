Title: Un paso más para garantizar los derechos de la comunidad LGBTI en Colombia
Date: 2016-02-10 13:04
Category: LGBTI
Tags: comunidad gay, Decreto contra discriminación sexual, LGBTI Colombia
Slug: un-paso-mas-para-garantizar-los-derechos-de-la-comunidad-lgbti-en-colombia
Status: published

###### [Foto: Contagio Radio ] 

<iframe src="http://www.ivoox.com/player_ek_10386181_2_1.html?data=kpWgmpuVfJKhhpywj5WaaZS1lZ2ah5yncZOhhpywj5WRaZi3jpWah5yncbbijNXO1dSPsYa3lIquk9iPtMLmwpDUw9fFstXd28bfjdHTt4zYxtfSxc3Tt4zYxpDZw5DHs87pz87Rj4qbh4630NPhw8zNs4zGwsnW0ZCRaZi3jpk%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Germán Rincón Perfetti, Abogado] 

###### [10 Feb 2016.] 

La Consejería de Derechos Humanos y el Ministerio del Interior presentaron un borrador de decreto que busca que instituciones del Estado como **notarias, colegios, cárceles y centros de salud adopten un enfoque diferencial para garantizar los derechos de la población LGBTI**. De acuerdo con el abogado Germán Rincón Perfetti, este proyecto se construyó teniendo en cuenta los aportes de distintas organizaciones que participaron en encuentros realizados con la intención de construir una política pública que legisle en la materia.

Según afirma el abogado este decreto pretende aportar a la construcción de una política pública que en perspectiva de derechos se convierta en una herramienta para evitar actos de discriminación contra la población LGBTI, y a su vez posibilite la **transformación de la institucionalidad “para garantizar un estado social democrático y de derechos”**, como lo demanda la Constitución y las distintas sentencias emitidas por la Corte que fueron tenidas en cuenta para la elaboración del borrador presentado.

Este decreto ordenaría al Ministerio de Educación velar porque los colegios acojan como falta disciplinaria la discriminación por orientación sexual; a la Fuerza Pública y al INPEC **respetar las diferencias sexuales de la población y garantizar el ejercicio de sus derechos** en los centros penitenciarios; a la Superintendencia de Notariado y Registro sancionar a los notarios que retrasen los trámites para el cambio de sexo en la cédula de ciudadanía y a la Superintendencia de Salud garantizar una atención adecuada en los centros médicos.

Para asegurar que este decreto promocione los derechos de la población LGBTI, se estima que antes de ser sancionado por la Presidencia de la República pueda ser consultado para hacerle los ajustes correspondientes. Una vez el proyecto se convierta en decreto, las **instituciones públicas estarán en la obligación de acoger las medidas legales determinadas**; sin embargo, dependerá del control social el que las entidades cumplan, tal como asevera Rincón.

###### Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)] 
