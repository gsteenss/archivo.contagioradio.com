Title: Un homenaje teatral a Humberto Monroy y al grupo "Génesis"
Date: 2018-08-21 15:45
Category: Cultura
Tags: Corporación Teatral PFU, Grupo Génesis de Colombia, Humbero Monroy, rock folk colombiano, teatro la candelaria
Slug: homenaje-humberto-monroy-grupo-genesis
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/10/25300_110747498959812_3488603_n.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

#####  

##### [21 Ago 2018] 

Más de 20 años después de su prematura y penosa muerte, **Humberto Monroy**, emblemático músico y compositor bogotano recordado por liderar agrupaciones como "The Speakers", "Siglo Cero" y "Génesis" (el de Colombia), vuelve a la vida gracias al teatro.

Se trata de "Humo", producción de la Corporación Teatral PFU en collor, ganadora en 2014 de la Beca de "Creación Teatral" del programa de estímulos del Ministerio de Cultura, que busca contar la trágica historia de una banda de folk rock colombiano en el contexto político y social de finales de los 70 y principios de los 80.

![Humo obra de teatro](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/11/unnamed-11.jpg){.aligncenter .wp-image-16614 width="420" height="278"}

En esta oportunidad, con única función, el colectivo artístico bumangués presentará en el Teatro Petra de Bogotá la propuesta que busca a través del drama y la música homenajear y reivindicar a una generación de pioneros y "locos" que se atrevieron a fusionar el rock and roll con sonidos e instrumentos propios del folclor colombiano.

Desde su título "Humo" apelativo con el que se conocía a Monroy,  la puesta en escena toma como referente al músico y a su banda, para abordar problemáticas presentes durante el llamado "Estatuto de seguridad" del presidente Turbay, como la impunidad y la intolerancia, desafiándolas con canciones, sueños e ilusiones que muchas veces terminan en frustraciones y derrotas.

Con la dirección del maestro Lizardo Florez "Chalo" y la actuación de Ericsson Gómez, Liliana López Ibáñez, Susana Ortiz Córdoba, John Manrique Pabón, Elkin Moreno y William Montero, la obra promete devolver en el tiempo a quienes vivieron esos años de agitación y permitir a las nuevas generaciones conocer un poco más de la historia nacional desde la perspectiva de aquellos que coincibian al país de una manera diferente.[  
](https://archivo.contagioradio.com/un-homenaje-teatral-a-humberto-monroy-y-el-grupo-genesis/unnamed-11/)

<iframe src="https://www.youtube.com/embed/Shk-pC1vLlw" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

Humo, en esta oportunidad es invitada especial de la Semana Teatral ACA organizada por la Asociación Colombiana de Actores que se llevará a cabo del 27 de agosto al 2 de septiembre en varias salas de la ciudad y la cual destaca la labor creativa, calidad, representatividad y pluralidad de cada una de las obras invitadas.

“Estamos muy contentos de presentar esta pieza en el marco de la Semana Teatral ACA, ya que es una obra muy capitalina. Aunque los personajes nacieron, crecieron y vivieron en Bogotá DC, la obra se desarrolla en un contexto nacional. Es una pieza teatral llena de recuerdos, nostalgia de una generación; y para aquellos que no conocen la historia de Humo, les va a brindar un maravilloso recorrido por los inicios del rock en Colombia” afirma el director Lizardo “Chalo” Flórez.

**Sobre Génesis**

Humberto Monroy, Edgar Restrepo, y Federico Taborda formaron el Grupo Génesis de Colombia en 1972. El lider fue Humberto como compositor y cantante. Fué el primer grupo colombiano en fusionar ritmos e intrumentos folcloricos con el Rock. La batería estaba compuesta por tambores andinos y de las costas colombianas, interpretaban capadores, quenas y flautas traversas de caña.

**La Corporación Teatro PFU en Collor**

Nace en el año 1992, como necesidad artística de un grupo de artistas a quienes interesa el teatro como arte: lugar de laboratorio, investigación y experimentación permanente. A través de su trayectoria se ha alimentado de diversas culturas y estilos teatrales dentro de la tradición y algunas vertientes del teatro contemporáneo como Beckett, Meyerhold, Tadeusz Kantor, Dadá, Santiago García y el Teatro La Candelaria, Brecht, Karl Valentín, A.Strindberg, Miguel Borrás, Noureddine Aba, Sanchis Sinisterra, entre otros.

**Fecha:** 31 de agosto de 2018

**Lugar:** Teatro Petra Carrera 15 BIS \# 39-39

**Hora:**  8:00 p.m.
