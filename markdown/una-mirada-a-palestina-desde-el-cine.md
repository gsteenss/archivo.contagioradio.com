Title: Una mirada a Palestina desde el Cine
Date: 2016-03-18 13:41
Author: AdminContagio
Category: 24 Cuadros, eventos
Tags: Ciclo cine Palestina, Museo Nacional de Colombia, Palestina
Slug: una-mirada-a-palestina-desde-el-cine
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/03/28.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Fotograma: "Omar" 

###### [18 Mar 2016] 

El Museo Nacional de Colombia, junto a la misión diplomática de Palestina, el Festival de Cine de Bogotá y la Corporación Internacional de Cine, presentan del 19 al 26 de marzo el ciclo "Mas allá de Tierra Santa", compuesto por ocho películas que abordan desde la mirada de diferentes directores el conflicto palestino- israelí.

La muestra inicia este sábado 19 a las 11:oo de la mañana con la proyección de la cinta **"La sal de este mar"**, (Milh Hadha Al-Bahr), seguida por la ganadora del Globo de oro **"El paraíso ahora"** de Annemarie Jacir, que se presentará desde las 6:00 p.m.

<p>
<script>// <![CDATA[<br />
window.fbAsyncInit = function() { FB.init({ xfbml : true, version : 'v2.3' }); }; (function(d, s, id){ var js, fjs = d.getElementsByTagName(s)[0]; if (d.getElementById(id)) {return;} js = d.createElement(s); js.id = id; js.src = "//connect.facebook.net/en_US/sdk.js"; fjs.parentNode.insertBefore(js, fjs); }(document, 'script', 'facebook-jssdk'));<br />
// ]]></script>
</p>
<div class="fb-video" data-href="https://www.facebook.com/video.php?v=487212698153160" data-width="500" data-allowfullscreen="true">

</div>

La muestra continua el martes 22 con la proyección de **"El árbol de lima"** (Etz Limon), ganadora del 'Premio especial del público' en el Festival de Cine de Berlín, el miércoles 23 se presentará el documental **"Cinco cámaras rotas"** dirigida por Emad Burnat y Guy Davidi ambas a las 3:00 p.m y el jueves 24 a las 11:00 a.m. con el filme franco-palestino **"Intervención divina"** de Elia Suleiman y la cinta canadiense Inch’Allah a las 3:00 p.m.

El viernes 25 de marzo el ciclo presentará la nominada al Oscar **"Omar"** desde las 11:00 a.m. y la producción chilena **"La última luna"**, cerrando el sábado 26 con **"Amreeka"** cinta palestina dirigida por Najwa Najjar. Todas las proyecciones tendrán lugar en el auditorio "Teresa Cuervo Borda" del Museo Nacional de Colombia. Entrada Libre.

 
