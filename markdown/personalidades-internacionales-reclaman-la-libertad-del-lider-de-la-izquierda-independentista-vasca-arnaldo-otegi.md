Title: Personalidades internacionales reclaman la libertad del líder de la izquierda independentista vasca Arnaldo Otegi
Date: 2015-03-24 20:46
Author: CtgAdm
Category: El mundo, Otra Mirada
Tags: “Libertad para Arnaldo Otegi, #FreeOtegi, presos vascos a casa”
Slug: personalidades-internacionales-reclaman-la-libertad-del-lider-de-la-izquierda-independentista-vasca-arnaldo-otegi
Status: published

###### Foto:Basqueprocces.info 

Esta tarde el **parlamento europeo** ha acogido la iniciativa **“Libertad para Arnaldo Otegi, presos vascos a casa”**, firmada por **24 personalidades internacionales** de prestigio como el **premio nobel de la paz Desmond Tutu o el expresidente de Uruguay José Mujica.**

La iniciativa reclama la puesta en libertad del líder de la izquierda abertzale Arnaldo Otegi, encarcelado el 26 de mayo de 2005 bajo la acusación de colaboración con banda armada. También demanda la finalización de la dispersión de los presos políticos vascos y su regreso a penitenciarías vascas.

**Arnaldo Otegi, cuyo caso ha sido recurrido ante el Tribunal Europeo de Derechos Humanos**, se encuentra en una prisión española alejado de sus familiares y amigos. Así están también cerca de **500 presos vascos relacionados con este conflicto. Dispersados en prisiones lejanas al País Vasco**, frecuentemente incomunicados, esta realidad provoca, además, un castigo añadido a sus familiares, abocados a recorrer largas distancias para poder visitarlos.

El Premio Nobel de la Paz **Desmond Tutu señaló a Arnaldo Otegi, en su día portavoz de Batasuna y hoy secretario general de Sortu, como “el líder de este proceso de paz".** Efectivamente, Otegi fue el dirigente más destacado entre aquellos que propiciaron en las fuerzas independentistas el debate sobre la necesidad de apostar por la palabra para solucionar todo conflicto. Su apuesta por vías exclusivamente pacíficas y democráticas fue respondida con su arresto, en octubre de 2009, y con su posterior condena a más de seis años y medio de prisión por pertenecer a la organización política Batasuna, ilegalizada previamente por el Gobierno español.

Adjuntamos **Declaración internacional por la libertad de Arnaldo Otegi y la vuelta a casa de los presos vascos**:

[Declaración internacional por la libertad de Arnaldo Otegi y la vuelta a casa de los presos vascos](https://es.scribd.com/doc/259827948/Declaracion-internacional-por-la-libertad-de-Arnaldo-Otegi-y-la-vuelta-a-casa-de-los-presos-vascos "View Declaración internacional por la libertad de Arnaldo Otegi y la vuelta a casa de los presos vascos on Scribd") by [Contagioradio](https://www.scribd.com/contagioradio "View Contagioradio's profile on Scribd")

<iframe id="doc_39130" class="scribd_iframe_embed" src="https://www.scribd.com/embeds/259827948/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-x3x5Ybm42sq6iWSFI2Zn&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="0.7068965517241379"></iframe>
