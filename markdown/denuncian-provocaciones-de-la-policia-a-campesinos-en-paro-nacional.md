Title: Denuncian provocaciones de la policía a campesinos en Paro Nacional
Date: 2017-10-27 12:50
Category: Movilización, Nacional
Tags: campesinos, Fuerza Pública, movilización de campesinos, paro nacional indefinido
Slug: denuncian-provocaciones-de-la-policia-a-campesinos-en-paro-nacional
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/10/DNGLiNbWsAEoEId.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Marcha Patriótica] 

###### [27 Oct 2017] 

El día de hoy **se completa el quinto día de paro nacional indefinido** por parte del campesinado del país. Luego del primer acercamiento entre las organizaciones y el gobierno nacional se definió el estudio del mecanismo de diálogo y un encuentro para el próximo lunes. Además un acuerdo de no agresión, sin embargo los campesinos denuncian reiteradas provocaciones por parte de la policía y el ESMAD.

De acuerdo con Juan Carlos Quintero, integrante de la mesa de interlocusión MIA en el Catatumbo, luego de haber solicitado la mediación de la Comisión de Garantes de la Mesa de Interlocusión y Acuerdo, que está encabezada por el ex presidente **Ernesto Samper**, ambas partes lograron hacer un ejercicio de rendición de cuentas.

### **70% de los acuerdos de 2013 se han incumplido** 

Los objetivos de esta reunión se enmarcaron en **establecer los cumplimientos de los acuerdos** del paro de 2013 y el manejo de los recursos que debían invertirse en la región. Con esta discusión, los campesinos constataron que “el 70% de los acuerdos de 2013 se han incumplido” y los recursos que han llegado no los ha ejecutado la organización ASCAMCAT, como lo decían diferentes sectores.

Igualmente **discutieron la situación del paro nacional actual**. Quintero manifestó que los representantes del Gobierno “llegaron sin poder de decisión para abordar el pliego de peticiones”, por lo que argumentaron que no hay voluntad política para resolver la crisis de los campesinos del país. (Le puede interesar: ["Arranca diálogo de campesinos del paro nacional con el Gobierno"](https://archivo.contagioradio.com/continua-paro-nacional-indefinido-de-campesinos-en-el-pais/))

### **Fuerza Pública sigue intimidando a los manifestantes** 

Según Deivin Hurtado, integrante de la Red de Derechos Humanos del Sur Occidente Colombiano Francisco Isaías Cifuentes, **“la Fuerza Pública detuvo de forma arbitraria a un compañero** que se estaba movilizando”. Las comunidades reiteraron la necesidad q de que la protesta se lleve a cabo de manera pacífica y se consolide el diálogo con las autoridades.

Hurtado indicó que la persona detenida es un campesino de Cajibio, “él estaba en caminando cuando **dos sujetos en una motocicleta lo detienen**, lo agarran por a fuerza y le solicitan una requisa”. Dijo que estas personas no portaban uniforme lo que generó alerta en el campesino. (Le puede interesar: ["Gobierno a responder con hechos a campesinos en paro nacional indefinido"](https://archivo.contagioradio.com/comunidades-y-organizaciones-redactaron-pliego-de-peticiones-para-el-gobierno-nacional/))

El campesino **estuvo detenido por 5 horas** y esto significó un acto de intimidación para las comunidades. Por esto, han continuado con la manifestación pacífica en los puntos de concentración permitiendo la movilidad de los vehículos en la avenida Panamericana.

Finalmente, los campesinos indicaron que **la movilización y las actividades del paro se van a mantener** en busca de la concertación y el diálogo con las autoridades. Indicaron que a pesar de los hechos de intimidación, continuarán con la protesta pacífica de manera indefinida.

<iframe id="audio_21731789" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_21731789_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]

<div class="osd-sms-wrapper">

</div>
