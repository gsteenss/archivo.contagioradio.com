Title: Fuerte oposición al nombramiento de Néstor Humberto Martínez como embajador en España
Date: 2020-11-17 11:37
Author: CtgAdm
Category: Actualidad, DDHH
Tags: colombia, embajada de españa, Néstor Humberto Martínez, Presidente Duque
Slug: fuerte-oposicion-al-nombramiento-de-nestor-humberto-martinez-como-embajador-en-espana
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/07/nestor-humberto.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

Varios ciudadanos colombianos y organizaciones sociales entre ellos la Coordinación Colombia, Europa, Estados Unidos (Coeuropa), Plataforma de incidencia política internacional y nacional en materia de derechos humanos integrada por 281 organizaciones, se muestran en desacuerdo frente a la decisión del jefe de Estado Iván Duque.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/coeuropa/status/1327646059347275776","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/coeuropa/status/1327646059347275776

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

Ante la posibilidad de que **Néstor Humberto Martínez**, exfiscal general de la Nación se convertirse en el próximo embajador de España por pedido presidencial. Esto según se dio a conocer por caracol radio. Asegurando que una de las razones que motivaron al exfiscal Martínez a aceptar el ofrecimiento fue los inconvenientes de seguridad en el país, tras la polémica por las **pruebas no entregadas** por parte de la Fiscalía a la **Jurisdicción Especial para la Paz, JEP**, en el caso de** Jesús Santrich**, además de la insistencia por parte del **presidente de la República, Iván Duque Márquez**.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El exfiscal se encuentra, hoy, involucrado en el caso de Jesus Santrich puesto que, al parecer, la fiscalía, en cabeza de **Nestor Humberto Martínez, en ese momento, no compartió con la Jurisdicción Especial para la Paz una serie de audios que reposan en el expediente del exjefe guerrillero,** con que se habría podido tomar una decisión sobre su extradición a Estados Unidos por narcotráfico. Martínez presentó su renuncia irrevocable al cargo de Fiscal General el 15 de junio de 2019, a raíz de la decisión de la JEP de no extraditar al exguerrillero de las Farc, Jesús Santrich*.*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Es por esto, que algunos congresistas preparan un debate, cuya fecha no se ha definido aún, en el cual se preguntarán **“por qué la Fiscalía General de la Nación aportó los cinco kilos de cocaína para una operación encubierta de la DEA”** contra Santrich y *“qué fiscal dio la autorización de aportar la droga”*. Este debate será citado por los senadores [Gustavo Petro](https://www.pulzo.com/noticias/gustavo-petro), [Roy Barreras](https://www.pulzo.com/noticias/roy-barreras), [Iván Cepeda](https://www.pulzo.com/noticias/ivan-cepeda) y Antonio Sanguino, quienes igualmente indagarán a los altos funcionarios, con el fin de reconocer si el Gobierno sabía de las actuaciones y operaciones que estaba haciendo la agencia estadounidense antidrogas en el país y **cuestionarán a la Fiscalía por no entregar los audios del proceso a la JEP.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Coeuropa asegura que: *“Es muy grave que el señor Néstor Humberto Martínez, actuando como Fiscal General, haya ocultado y negado la entrega de las evidencias que le fueron requeridas por la Jurisdicción Especial para la Paz, JEP, y por el contrario, pretendió desarrollar un juicio paralelo ante los medios y la opinión pública, con pruebas manipuladas (videos silenciados), con los cuales presionó a dicho tribunal a la expedición sin pruebas de decisiones de extradición que, al no tener fundamento, fueron la base para el descrédito emprendido por este poderoso funcionario en contra de la Jurisdicción Especial para la Paz.”*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Además, La plataforma Coeuropa considera que es importante denunciar que Martinez *“bajo su gestión archivó importantes investigaciones por corrupción y graves crímenes de altos oficiales de la Fuerza Pública, como es el caso de la Operación Gavilán, e impidió el avance de las investigaciones de los oficiales mencionados en la Operación Bastón. De esta manera, frustró el proceso de depuración al interior de las Fuerzas Militares. A su vez, desde su llegada a la Fiscalía, congeló todos los casos de investigaciones por los llamados “falsos positivos” adelantados hasta entonces por la entidad*.”

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Se conoce que por algunos de estos delitos, en los que hubo participación del ex fiscal Néstor Humberto Martínez cursa ya denuncia penal por prevaricato por acción y por omisión, fraude a resolución judicial o administrativa, ocultamiento, alteración o destrucción de elemento material probatorio. Y por esto mismo, las organizaciones de la sociedad civil resaltan que Néstor Humberto Martínez debe ser procesado, enjuiciado y responder ante la justicia colombiana, o en su defecto, ante instancias de la justicia internacional que tienen el deber de perseguir a los perpetradores de graves crímenes internacionales.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Este caso se suma a los Andrés Felipe Arias, Luis Carlos Restrepo, María del Pilar Hurtado y María Claudia Daza en los cuales se repite el accionar del gobierno colombiano al nombrar como embajadores ante diferentes países a implicados en graves violaciones de los derechos humanos y crímenes contra la paz para evadir su comparecencia ante la justicia, así como facilitar su fuga antes de que se produzcan decisiones judiciales en su contra. Fortaleciendo de esta manera el patrón de impunidad que ha caracterizado al Estado colombiano.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Las organizaciones de la sociedad civil, hacen un fuerte llamado a Mandatario Iván Duque Márquez, para que se abstenga de nombrar al señor ex fiscal Néstor Humberto Martínez como Embajador en España. Así mismo, solicita a los organismos judiciales competentes acelerar las investigaciones sobre las conductas criminales que implican la responsabilidad de Néstor Humberto Martínez. De igual forma, solicita al Gobierno de España que se abstenga de conceder el beneplácito solicitado por el Gobierno colombiano para el señor Néstor Humberto Martínez, así como interponer sus diligencias para que las denuncias por ocultamiento de bienes y blanqueamiento de activos que implicarían al mencionado, sean debidamente investigados y sancionados.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Otra expresión que la ciudadanía ha creado es una una ‘firmatón’, a través de Change.org. liderada por un ciudadano que se identifica como César Augusto González, la cual lleva más de 13.000 firmas.  El hombre afirma que:  “Nuestro país debe dejar de promover como embajadores a políticos cuestionables, que afrontan o están vinculados a investigaciones por corrupción como el exfiscal [Néstor Humberto Martínez](https://www.pulzo.com/noticias/nestor-humberto-martinez)”.

<!-- /wp:paragraph -->
