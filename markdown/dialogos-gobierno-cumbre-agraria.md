Title: En noviembre se retoman diálogos entre Gobierno y Cumbre Agraria
Date: 2018-10-24 12:44
Author: AdminContagio
Category: DDHH, Nacional
Tags: COCCAM, Cumbre Agraria
Slug: dialogos-gobierno-cumbre-agraria
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/04/campesinos-boyaca.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Contagio Radio 

###### 24 oct 2018 

Organizaciones que hacen parte de la COCCAM y la Cumbre Agraria, hicieron un llamado al gobierno nacional para que se **retomen las conversaciones en torno a las garantías de DDHH** y se avance en los temas que se habían pactado con el gobierno anterior, tras las movilizaciones campesinas del año 2013 y las que le precedieron.

El vocero Cesár Jerez, asegura que a pesar de que el gobierno actual ha manifestado su intención de continuar con el diálogo como fórmula, **no se ha restablecido por ahora ningún canal** "hace aproximadamente mes y medio sostuvimos una reunión con la Ministra del Interior donde asumió un compromiso para **reactivar este mes ya las sesiones formales de la interlocución**"  que iniciaron durante el gobierno Santos.

"Ya hay compromiso, unas fechas para realizar inicialmente una mesa técnica y para mediados de noviembre ya **retomar una sesión formal de intercambio y negociación entre el gobierno Duque y la Cumbre Agraria**" asegura Jerez, añadiendo que falta que el gobierno asuma esos compromisos para **demostrar si su voluntad de diálogo es real**.

El también profesor manifestó además que propusieron al gobierno **reactivar las mesas que se venían trabajando por lo menos en 5 regiones del **[**país**], incluyendo la mesa del Catatumbo donde hace aproximadamente dos semanas hubo un para detener las erradicaciones violentas en el sector de El Zulia.

**Se esta construyendo una matriz mediática**

Frente a la criminalización de la protesta que se viene realizando en algunos medios en los que se señala que los campesinos atacan a la fuerza pública, Jeréz asegura que ser trata de **una matriz mediática construida sobre una supuesta sublevación del mundo rural** "Le queda muy mal a este gobierno entrar a justificar acciones contra la protesta social" .

"En el gobierno **lamentablemente es el Ministro de defensa el que viene ejerciendo esa labor de intoxicación pública mediática contra campesinos e indígenas**", lo que esperan se pueda solucionar con la re activación de las mesas regionales y nacionales de diálogo.

<iframe id="audio_29588716" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_29588716_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
