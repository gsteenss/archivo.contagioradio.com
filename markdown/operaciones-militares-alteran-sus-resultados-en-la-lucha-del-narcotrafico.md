Title: "Operaciones militares alteran sus resultados en la lucha del narcotráfico"
Date: 2020-07-31 11:30
Author: AdminContagio
Category: Actualidad, datos, DDHH
Slug: operaciones-militares-alteran-sus-resultados-en-la-lucha-del-narcotrafico
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/06/erradicación-forzada-guayabero.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

En la ciudad de Bogotá, una fuente que solicitó la reserva de su identidad, **informó sobre operaciones militares con resultados alterados en la lucha contra el narcotráfico.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Según la fuente en los municipios del medio y bajo Putumayo, donde se han intensificado las Operaciones militares contra el narcotráfico; las cifras presentadas en términos de erradicación de hectáreas de cultivos de coca y la destrucción de laboratorios para el procesamiento de cocaína e insumos, no corresponden con la realidad, y **están siendo alteradas de manera fraudulenta por algunas de los mandos militares presentes en los territorios.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Esto teniendo en cuenta que la Fuerza Pública dedicada a estas acciones, tiene que cumplir metas diarias. En los casos mencionados por la fuente, **por cada hectárea reportada, hay hasta un 80% de incremento que no corresponde con la realidad**, y que son legalizadas con fotografías y destrucción de pequeños elementos, que luego son presentados como positivos en las operaciones militares, contra los grandes responsables del narcotráfico en el departamento.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Estas nuevas denuncias, reiteran las falencias y complicidades existentes entre sectores militares y policiales con estructuras armadas dedicadas al narcotráfico como **La Mafia**. (Le puede interesar: [Otra Mirada: El Catatumbo entre fuego cruzado](https://archivo.contagioradio.com/otra-mirada-el-catatumbo-entre-fuego-cruzado/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En el último año, varios funcionarios, entre ellos **el coronel del Ejército Elkin Alfonso Argote Hidalgo,** jefe del Estado Mayor del Comando de Reclutamiento y Control Reservas, **capturado el 28 junio del 2019, mientras brindaba protección a uno de los delincuentes más buscados en la región, **Miguel Antonio Bastidas Bravo**, conocido con el alias de “Gargola”**,

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por otro lado, el 30 mayo de este año, el **comandante de la Sijín en el Valle del Guamuez, Diego Guenis**, quien llevaba 4 años laborando en la seccional de la entidad y 14 años de servicio en la Policía; **fue capturado mientras transportaba escondida en una de las llantas de su vehículo personal nueve kilos de cocaína, en la via Mocoa- Pitalito.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Según el informe ejecutivo Nº 20 de la **Oficina de las Naciones Unidas contra la Droga y el Delito** -UNDOC- , presentado en abril de 2020, sobre el seguimiento a la implementación del Programa Nacional de Sustitución de Cultivos de Uso Ilícito, reitera el cumplimiento en la erradicación voluntaria del 96% de las familias vinculadas al PNIS, así mismo **señala que se presenta solo un 0.4% de resiembra o rebrote de cultivos de uso ilícito**, mientras que en los proceso de erradicación forzada el nivel de resiembra en algunos casos supera el 50% de cultivos.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Es más efectiva la sustitución voluntaria de los Cultivos de Uso ilícito

<!-- /wp:heading -->

<!-- wp:paragraph -->

Así mismo, las [erradicaciones forzadas](https://archivo.contagioradio.com/la-jep-tiene-la-palabra-para-que-las-ffmm-recuperen-su-legitimidad/) implican un alto costo en violaciones de Derechos Humanos e infracciones al DIH, tan solo este año, se han producido **51 incidentes en medio de operaciones militares** **de erradicación de cultivos** y según cifras de la Acción Integral contra Minas Antipersonal (Aicma), desde 2006 y hasta el 31 de marzo de 2020, **457 personas que desarrollaban operaciones de erradicación manual fueron víctimas de minas antipersonales,** de estas 417 (el 91%) eran civiles. **De esos civiles, 43 murieron.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Además por medio de una rueda de prensa [Ascamcat](https://www.justiciaypazcolombia.com/asesinado-lider-social-de-ascamcat/)este viernes hizo un llamado de alerta ante el recrudecimiento de la violencia y el desplazamiento forzado en la región del Catatumbo, sumida en medio del conflicto, los cultivos ilícitos y el olvido estatal. Y señalaron que en tan solo en el primer semestre de este año, se registraron **cinco masacres, en la que perdieron la vida 30 perspnas, y el desplazamiento forzado de cerca de 800 campesinos por grupos armados.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Además durante el mes de julio, en el departamento de Putumayo, **se han presentado dos asesinados a manos de la Fuerza Pública**, en medio de operaciones de erradicación forzosa de cultivos de coca. (Le puede interesar: [Asesinan a José Oliver Maya, niño indígena Awá en operativos de erradicación forzada](https://archivo.contagioradio.com/asesinan-a-jose-oliver-maya-nino-indigena-awa-en-operativos-de-erradicacion-forzada/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Siendo así **la implementación integral del punto 4 del Acuerdo Final de Paz, se constituye como la mejor estrategia** para la sustitución y superación de la problemática de cultivos de uso ilícito en Colombia.

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
