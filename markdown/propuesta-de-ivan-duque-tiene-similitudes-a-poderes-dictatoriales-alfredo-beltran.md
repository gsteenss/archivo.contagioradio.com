Title: Propuesta de Iván Duque tiene similitudes a poderes dictatoriales: Alfredo Beltrán
Date: 2018-03-28 11:00
Category: Nacional, Política
Tags: Alfredo Beltran, Centro Democrático, Iván Duque
Slug: propuesta-de-ivan-duque-tiene-similitudes-a-poderes-dictatoriales-alfredo-beltran
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/03/Iván-Duque.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Publímetro] 

###### [28 Mar 2018] 

La propuesta que hizo el candidato del Centro Democrático, Iván Duque, sobre juntar los Altos Tribunales para crear una “súper corte”, es una iniciativa que en opinión del ex magistrado de la Corte Constitucional, Alfredo Beltrán, **tiene características de los poderes que se crean bajo sistemas dictatoriales o autoritarios** y además, estaría en contra de la historia del país por garantizar una democracia.

“En Colombia, la República adopto **el sistema de tener por separado la justicia ordinaria, de la justicia contenciosa administrativa**” señaló Beltrán y agregó que finalmente lo que lograría la propuesta es que de un lado se elimine esa justicia contenciosa y del otro que tanto la Corte Constitucional, como el Consejo de Estado y la Corte Suprema de Justicia se reúnan en una sola Corte.

Consecuencia de ello, todos los magistrados serían revocados y le darían paso a la nueva institución que plantean, modelo que de acuerdo con el jurista, se parecería al implementado en Venezuela en 1998, en donde existe una sola corte con salas especializadas,**“lo que se ha prestado para una situación bastante grave de concentración del poder ”**.

### **¿Por qué la iniciativa tendría características de un poder dictatorial?** 

“Es una propuesta profundamente autoritaria, profundamente contraria a la evolución de Colombia y de democrático, no tiene sino la discusión que ella origina” afirmó Beltrán y manifestó que esta idea se **“entrelaza” con la postura de Álvaro Uribe Vélez, cuando propuso que desaparecieran las Cortes**.

Hecho que implicaría que las funciones **especializadas de la Corte Constitucional, que tanto ha disgustado a algunos funcionarios,** según Beltrán, y que se ha caracterizado por garantizar el derecho a la tutela, quedaría a la deriva. (Le puede interesar: ["4 candidatos presidenciales reconocen las Consultas Populares"](https://archivo.contagioradio.com/fajardo-duque-petro-y-de-la-calle-reconocen-la-legitimidad-de-las-consultas-populares/))

Beltrán señaló que los cargos que conformen a esa súper corte, también serían designados por el máximo mandatario, que finalmente significa una concentración del poder que podría llevar a otras discusiones como restablecer la posibilidad de una re elección presidencial, aumentar el periodo de tiempo de gobierno y desaparecer a otras instituciones.

<iframe id="audio_24926449" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_24926449_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
