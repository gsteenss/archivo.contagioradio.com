Title: Nueva incursión de Autodefensas Gaitanistas en Truandó Chocó
Date: 2017-03-12 19:36
Category: DDHH, Nacional
Tags: Autodefensas Gaitanistas de Colombia, paramilitarismo en Colombia, Truandó
Slug: nueva-incursion-autodefensas-gaitanistas-truando-choco
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/03/Autodefensas_Gaitanistas.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: La Razón] 

###### [12 Mar 2017] 

El pasado 10 de marzo en la cuenca del río Truandó en Chocó, integrantes de las autodenominadas "Autodefensas Gaitanistas de Colombia", resultaron afectados por la activación de minas antipersona. Pobladores comunicaron que **2 fallecieron y otros 2 perdieron sus extremidades inferiores y se encuentran hospitalizados en Río sucio.**

Denunciaron que durante el fin de semana las presuntas Autodefensas Gaitanistas de Colombia “fortalecieron su presencia con personas de civil en Tumaradó, Puente América o **Travesía, Rio sucio, Turbo, Belén de Bajirá, Brisas de Curvaradó, Pavarandó y Mutatá**” y han ejecutado labores de control “sobre las personas que están denunciando sus diversas operaciones”.

###  

Algunos de los testigos, habitantes de Río Sucio y Truandó, afirmaron que se trató de enfrentamientos entre las AGC y el ELN en el caserío de la Pava. De acuerdo con fuentes de la Comisión Intereclesial de Justicia y Paz, desde el pasado sábado 4 de marzo hacia las 4:00 p.m. ingresaron hacia el territorio colectivo de Truandó 8 botes, **“cuatro de madera y cuatro de fibra con cerca de de 150 hombres con armas largas y cortas y vestidos de camuflado”.**

Señalaron que las operaciones “neo paramilitares en el Bajo Atrato”, se mantienen en **Cacarica, Truandó, Domingodó, Curvaradó y Jiguamiandó.** Incluso la escritora Alemana, Alexandra Huck, quien recientemente publicó un libro sobre el conflicto en el pacífico Colombiano, manifestó, "estuve de visita en la zona humanitaria de Nueva Esperanza en Dios y **la comunidad me contó sobre las nuevas incursiones neoparamilitares en el territorio, la situación es muy preocupante". **

###### Reciba toda la información de Contagio Radio en [[su correo]
