Title: Diciembre: Consumismo y maltrato animal
Date: 2015-01-25 20:41
Author: CtgAdm
Category: Voces de la Tierra
Tags: Animales, diciembre, Maltrato animal, vegetarianismo
Slug: diciembre-consumismo-y-maltrato-animal
Status: published

##### Foto: vimeo.com 

<iframe src="http://www.ivoox.com/player_ek_3994996_2_1.html?data=lJ6mlp6deo6ZmKial5yJd6KlmpKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRk8%2FYwpCu0M7Rpc3d1NnOjZKJe6ShpNTb1sbLrdCfs8bRy9SPcYarpJKh&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

Las fiestas de diciembre son el momento del año en el que el sistema capitalista en que se desarrolla la sociedad, genera que el consumo aumente en niveles absurdos.

Aunque para la mayoría de los seres humanos, diciembre representa una de las épocas más felices del año, los animales en cambio son víctimas de maltrato y crueldad y por eso, para ellos es tal vez el peor momento del año. **Vacas, pavos, pollos, cerdos, entre otros, son consumidos como comida.** Por otro lado, **perros, gatos y otros animales terminan siendo abandonados meses depués de haber sido objeto de regalos.**

En nuestro primer programa de Onda Animalista analizamos esta problemática, y te invitamos a cambiar algunos hábitos propios de las fiestas de diciembre en pro de la vida de los animales.
