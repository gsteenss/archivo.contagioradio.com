Title: Los 3 discursos históricos de Fidel Castro
Date: 2016-11-26 09:25
Category: El mundo, Política
Tags: Cuba, Fidel Castro
Slug: adios-a-fidel-castro-padre-de-la-revolucion-cubana
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/7.-La-estrella-de-Fidel-2010.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Roberto Chile] 

###### [26 Nov 2016] 

A través de un mensaje televisado, Raúl Castro, informó que sobre las 10:29 de la noche falleció Fidel Castro, líder y comandante en jefe de la revolución Cubana que estuvo en el poder de este país hasta hace 10 años, cuando por problemas de salud dejó el cargo que actualmente ocupa su hermano.  En su conmemoración Cuba tendrá nueve días de duelo.

Fidel Castro llegó al poder el 1 de enero de 1959, día en el que se proclama la victoria de la revolución Cubana y el 8 de enero entra a la Habana en donde pronuncia las siguientes palabras:

**“La tiranía ha sido derrocada. La alegría es inmensa. Y sin embargo, queda mucho por hacer todavía.** No nos engañamos creyendo que en lo adelante todo será fácil; quizás en lo adelante todo sea más difícil. Decir la verdad es el primer deber de todo revolucionario. Engañar al pueblo, despertarle engañosas ilusiones, siempre traería las peores consecuencias, y estimo que al pueblo hay que alertarlo contra el exceso de optimismo.”.

Durante 47 años, Fidel estuvo al frente de uno de lo gobierno que más tenciones ha generado en torno a la comunidad internacional. Abogado de profesión, uno de sus discursos más recordados fue el que pronunció en 1979 ante la ONU, en donde se refiere a la Guerra Fría “Las bombas podrán matar a los hambrientos, a los enfermos, a los ignorantes, pero **no pueden matar el hambre, las enfermedades, la ignorancia, no pueden tampoco matar la justa rebeldía de los pueblos”**.

En el mismo acto también hace un llamado a la desigualdad económica y a los peligros de la profundización de la misma “**Hablo en nombre de los enfermos que no tienen medicinas, hablo en nombre de aquellos que se les ha negado el derecho a la vida** y la dignidad humana, unos países poseen abundantes recursos, otros no poseen nada ¿cuál es el destino de estos? ¿Morirse de hambre?, ¿ser eternamente pobres?, ¿para qué sirve entonces la civilización, para qué sirve la conciencia del hombre, para qué sirven las Naciones Unidas?"

Sin embargo, con el tiempo estas apariciones fueron mermando, la salud del Comandante en jefe se fue deteriorando. Una de sus últimas a pariciones se dio este año durante el cierre del Congreso del Partido Comunista, está sería la última alocución pública de Fidel Castro “**Pronto seré ya como todos los demás, a todos nos llegará nuestro turno, pero quedarán las ideas de los comunistas cubanos**, a nuestros hermanos de América Latina y el mundo  debemos trasmitirles que  el pueblo cubano vencerá”.

“Por qué me hice socialista, más claramente por qué me convertí en comunista, esa palabra que expresa el concepto más distorsionado y calumniado de la historia, por parte de aquellos que tuvieron el privilegio de explotar a los pobres, despojados desde que fueron privados de todos los bienes materiales que provee el trabajo, el talento y la energía humana. Desde cuándo el hombre vive en ese dilema”

https://www.youtube.com/watch?v=RhJhT9jdhyA

Con 90 años muere Fidel Castro, seguramente uno de los personajes más amados y odiados en el mundo, pero convertida a partir de hoy, en una leyenda para la historia. Le puede interesar:["15 fotografías incónicas del líder de la Revolución: Fidel Castro"](https://archivo.contagioradio.com/?p=32909&preview=true)

######  Reciba toda la información de Contagio Radio en [[su correo]
