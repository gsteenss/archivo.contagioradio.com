Title: De 6 países que privatizaron su régimen pensional, 4 revirtieron la decisión: Wilson Arias
Date: 2019-10-15 17:41
Author: CtgAdm
Category: Economía, Entrevistas
Tags: Iván Duque, pensiones, Reforma Pensional, Wilson Arias
Slug: de-6-paises-que-privatizaron-su-regimen-pensional-4-revirtieron-la-desicion-wilson-arias
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/interna-119-e1571177951974.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: ElNacional.com  
] 

En medios de comunicación empresariales se difundieron las principales características de lo que sería la próxima reforma pensional que prepara el Gobierno para este año, dicha reforma incluiría la modificación del régimen de prima media y el aumento de la tasa de cotización. Según explicó el senador por el Polo Wilson Arias, la eliminación del régimen de prima media, manejado por COLPENSIONES, sería una decisión que no produce buenos resultados y respondería a intereses de privados.

### **¿Qué significa acabar con el régimen de prima media?** 

Arias afirmó que la idea de acabar con el régimen de prima media "es la respuesta del Gobierno a un pedido de Luis Carlos Sarmiento Angulo y el Fondo Monetario Internacional, que viene proponiendo paquetazos para enfrentar la crisis que está viviendo países como Argentina, Ecuador, Colombia...". El senador señaló que uno de los beneficiados por esta situación sería el grupo que pertenece a Sarmiento Angulo, porque **dos grupos económicos concentran más del 80% de las pensiones en Colombia: Porvenir, del grupo Aval; y Protección, del Grupo Empresarial Antioqueño.**

Sin embargo, la decisión afectaría económicamente a los pensionados, porque según relató Arias, quienes se pensionan actualmente con los fondos privados obtienen tasas bajas de reemplazo que llegan al 30%, mientras que en Colpensiones llega al 65%. "En contraste, es apenas normal que las personas prefieran estar en Colpensiones, y eso no le conviene a los grupos económicos mencionados", concluyó Arias. (Le puede interesar: ["¿Por qué Ecuador se vuelca a las calles?"](https://archivo.contagioradio.com/ecuador-tras-protestas/))

Mientras el argumento que respalda esta medida se centra en que el régimen de prima media resulta costoso de sostener porque no hay muchos jóvenes ingresando a la base de cotizantes, y por lo tanto, al Estado le cuesta cubrir esas pensiones, el senador del Polo recordó que académicos han señalado todo lo contrario: "Si acabáramos los fondos privados, y crearamos una especie de bolsa común con prima media, podríamos responder por todas las pensiones".

Por esta razón, Arias aseguró que de **"seis países que privatizaron absolutamente su régimen pensional, cuatro revirtieron esa decisión porque no produce pensionados y no produce remuneración pensional adecuada** para las personas que trabajaron toda su vida. (Le puede interesar: ["Sindicatos rechazan propuesta de contratación por horas"](https://archivo.contagioradio.com/sindicatos-rechazan-propuesta-de-contratacion-por-horas/))

### **Esperaríamos que la movilización sea similar a la de Ecuador** 

El líder político equiparó esta reforma pensional, y la propuesta de pagar a los jóvenes que ingresan al mercado laboral el 75% de un salario mínimo, con el 'paquetazo' de Ecuador, al señalar que está en juego la precarización del trabajo y por lo tanto, la capacidad de compra de los ciudadanos. Por esta razón, dijo que estas medidas debían ser explicadas a las personas, para que entiendan su gravedad, y se produzca una movilización como la que se tomó las calles del vecino país.

### **Actualización: Gobierno anunció que la reforma no tendría los parámetros señalados por la prensa**

Luego que medios de comunicación empresariales señalaran que tanto la eliminación del régimen de prima media como el aumento en el porcentaje de cotización estarían en la próxima reforma pensional, **el Ministerio de Hacienda publicó en su cuenta de Twitter que esta información no era cierta.** En el comunicado señalaron que no hay un Proyecto de Ley definido para tal propósito, asi como tampoco tienen la intención de aumentar la edad de pensión.

> .[@MinHacienda](https://twitter.com/MinHacienda?ref_src=twsrc%5Etfw), [@MintrabajoCol](https://twitter.com/MintrabajoCol?ref_src=twsrc%5Etfw) y [@DNP\_Colombia](https://twitter.com/DNP_Colombia?ref_src=twsrc%5Etfw) se permiten aclarar que los informes de prensa sobre el contenido de la reforma de [\#ProtecciónALaVejez](https://twitter.com/hashtag/Protecci%C3%B3nALaVejez?src=hash&ref_src=twsrc%5Etfw) en la que se ha venido trabajando, no son ciertos y no hay un proyecto de ley definido para presentar al Congreso de la República. [pic.twitter.com/CnDaNR1N6H](https://t.co/CnDaNR1N6H)
>
> — MinHacienda (@MinHacienda) [October 15, 2019](https://twitter.com/MinHacienda/status/1184143366507958272?ref_src=twsrc%5Etfw)

<p>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
  

<iframe id="audio_43149357" frameborder="0" allowfullscreen scrolling="no" height="200" style="border:1px solid #EEE; box-sizing:border-box; width:100%;" src="https://co.ivoox.com/es/player_ej_43149357_4_1.html?c1=ff6600">
</iframe>
  
**Síguenos en Facebook:**

</p>
<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
