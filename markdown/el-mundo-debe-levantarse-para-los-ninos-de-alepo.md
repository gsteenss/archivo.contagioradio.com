Title: "El mundo debe levantarse por los niños de Alepo"
Date: 2016-12-14 12:50
Category: Uncategorized
Tags: Alepo, Guerra en Siria, Niños en Siria, Unicef
Slug: el-mundo-debe-levantarse-para-los-ninos-de-alepo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/12/Captura-de-pantalla-2016-12-14-a-las-12.34.23.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: RTVE] 

###### [14 Dic 2016] 

Después de 4 años de fuertes enfrentamientos entre el Ejército Sirio y el Estado Islámico, el pasado 13 de Diciembre Bashar Al Asad aseguró que el 90% de la ciudad de Alepo ha sido recuperada, sin embargo, la ONU llamó la atención sobre la **grave situación de vulnerabilidad vivida por 5.831 niños que fueron evacuados junto a sus familias.**

El Centro Ruso para la Reconciliación en Siria reveló que después de la retoma de Alepo “13.346 civiles, incluidos 5.831 niños, fueron evacuados, desde los barrios que permanecen bajo el control de grupos armados, hacia zonas fronterizas”.

**“El mundo tiene que levantarse para los niños de Alepo y es el tiempo de parar esta pesadilla que viven mientras están vivos”**, fueron las palabras de Geert Cappelaere enviado de la UNICEF para dar acompañamiento a las familias evacuadas de Alepo.

Por otra parte, el portavoz del Alto Comisionado para los Derechos Humanos de las Naciones Unidas, Rupert Colville, dijo durante una conferencia en Ginebra que **“al parecer, tropas del gobierno sirio y sus aliados han ejecutado en los últimos días por lo menos a 82 civiles en el este de Alepo** (..) entre las víctimas se encuentran once mujeres y 13 niños de diferentes barrios de la ciudad”, lo que según varios medios locales, “no ha permitido que la población siria goce de total tranquilidad”.

En redes sociales, ciudadanos y ciudadanas sirias han hecho llamados a organismos internacionales para que garanticen la seguridad de estas familias y en particular a los niños. También buena parte de la población,  ha colgado en distintos medios, **vídeos con mensajes de despedida en los que muestran su temor e incertidumbre ante el avance de las fuerzas de Al Asad y sus aliados rusos**, así como la reacción de los rebeldes ante la derrota.

###### Reciba toda la información de Contagio Radio en [[su correo]
