Title: Así será la marcha en defensa del Páramo de Santurbán
Date: 2019-05-06 16:03
Author: CtgAdm
Category: Ambiente, Movilización
Tags: Autoridad Nacional de Licencias Ambientales, Comité para la Defensa del Agua y el Páramo de Santurbán, Minesa, Páramo de Santurbán
Slug: asi-sera-la-marcha-en-defensa-del-paramo-de-santurban
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/02/Páramo1-e1456273290780.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Agencia Noticias Unal ] 

[La ciudadanía de Bucaramanga, Santander saldrá a las calles este viernes, 10 de mayo, para manifestar su rechazo a la intención de la Sociedad Minera de Santander (Minesa) de poner en marcha una mina de oro en las montañas aledañas al Páramo de Santurbán, proyecto que contaminaría las fuentes hídricas que surten agua a más de 2,5 millones de colombianos.]

Activistas del [**Comité para la Defensa del Agua y el Páramo de Santurbán**, una de las organizaciones que convoca la movilización, esperan que la solicitud de licencia ambiental que radicó Minesa para iniciar la fase de explotación sea rechazada por las autoridades dadas las afectaciones ambientales que resultarían de la operación de la mina Soto Norte.]

"Ellos van a hacer una explotación subterránea y van a dañar a todas las fuentes hídricas como ya ha ocurrido. Eso puede contaminar las aguas de Bucaramanga porque nosotros recibimos el 70% del abastecimiento de agua del Río Suratá y ahí caerían las aguas sucias que salen de la mina", afirma Mario Flores, integrante del Comité. (Le puede interesar: "[Páramo de Santurbán nuevamente amenazado por solicitud de Minesa](https://archivo.contagioradio.com/solicitud-explotacion-oro-presenta-nuevas-amenaza-al-paramo-santurban/)")

### El video que revela las verdades de MINESA

Grabaciones de una reunión privada de Minesa también han alertado a opositores a este proyecto dado que muestra al presidente de la empresa, Santiago Urdinola, diciendo que estarán emprendiendo en una campaña de estigmatización de los manifestantes para deslegitimar el movimiento y así, mantener a las autoridades ambientales en Bogotá "tranquilos".

En estos videos, Urdinola también reveló que al menos de **50 familias campesinas serían desplazadas** por la construcción del proyecto aunque Flores indica que los afectados podrían ser muchos más. Por lo tanto, el Comité pide que los procedes de la empresa sean investigados por la Procuraduría General y la Fiscalía General. (Le puede interesar: "[Filtran video que evidencia campaña de estigmatización de Minesa contra ambientalistas](https://archivo.contagioradio.com/filtran-video-que-evidencia-campana-de-estigmatizacion-de-minesa-contra-ambientalistas/)")

### Defensa de Santurbán, una década de lucha

Tal como lo explica Flores, el Comité [para la Defensa del Agua y el Páramo de Santurbán es un ONG nacida en 2009 y que hoy en día, es conformado por 20 organizaciones, incluyendo sindicatos, organizaciones sociales y juntas de acción comunal. Además, es apoyado por partidos políticos, tanto como de izquierdo como derecha, que sin embargo, no ejercen vocería dentro de la agrupación.]

La marcha de este viernes, 10 de mayo, comenzará en el **Puerto del Sol en Bucaramanga a las 2 de la tarde**. También realizará un **plantón en Bogotá al mediodía en frente de la Autoridad Nacional de Licencias Ambientales**.

<iframe id="audio_35439708" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_35439708_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
