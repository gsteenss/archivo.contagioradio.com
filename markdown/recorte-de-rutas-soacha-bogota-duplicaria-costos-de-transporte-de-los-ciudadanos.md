Title: Recorte de rutas Soacha-Bogotá duplicaría costos de transporte de los usuarios
Date: 2016-05-02 14:17
Category: Nacional
Tags: Alcaldía de Bogotá, paro transportadores, soacha
Slug: recorte-de-rutas-soacha-bogota-duplicaria-costos-de-transporte-de-los-ciudadanos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/05/Soacha-e1462215882680.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Municipio de Soacha] 

###### [2 May 2016] 

Desde tempranas horas de la mañana, el gremio de los transportadores que cubren las rutas desde Soacha hasta Bogotá, **iniciaron una jornada de paro, debido a que la administración distrital tiene planeado recortar dichos servicios**, con lo que no solo se verían afectados los transportadores sino también los usuarios.

Según informó a Contagio Radio, Ariel González, director de Periodismo Público de Soacha, el paro se dio por la falta de comunicación entre los conductores y sus líderes pues ya se había acordado con el alcalde Eleázar González que el paro programado para este lunes 2 de mayo quedaba suspendido provisionalmente hasta que se adelanten nuevos encuentros con autoridades locales y distritales.

**La reunión entre los transportadores, las autoridades locales y la secretaría de movilidad de Bogotá, quedó agendada en dos semanas**. Allí se tiene planeado demostrar por qué no sería conveniente recortar estas rutas que van desde el municipio de Soacha hasta la Calle 80 con Avenida Boyacá,  Calle 80 con Avenida carrera 68 y hasta el 20 de Julio. De ser así, las personas que se movilizan usualmente en esos buses deberán pagar el doble en transportes, es decir **casi 70 mil pesos al mes sin contar los fines de semana.**

De acuerdo con la información de González, el Distrito comenzó a recortar varias rutas de este tipo tras la implementación del SITP, y sumado a eso, **la movilidad para los pobladores de Soacha se complicaría aún más teniendo en cuenta que tampoco cuentan con rutas alimentadoras**, aunque ya deberían haber entrado en funcionamiento desde hace 2 años.

Por el momento, los habitantes del municipio han tenido que recurrir al transporte informal para poder llegar hasta la estación de Transmilenio de San Mateo, que se encuentra más colapsada que lo usual. Desde las comunas de Soacha se reporta que no hay transporte y los habitantes se encuentran represados en sus barrios.

<iframe src="http://co.ivoox.com/es/player_ej_11383085_2_1.html?data=kpagmpiUfJahhpywj5WUaZS1kZWah5yncZGhhpywj5WRaZi3jpWah5yncaLmysrZjazTstuZpJiSo5bQqdSfjpC9x9fNs8WZpJiSo6nXuMKhhpywj6jTstXVyM7cjbfFqMrjjJKSmaiReA%3D%3D&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
