Title: El deber común (Mensaje a la fuerza pública)
Date: 2017-11-29 07:30
Author: JOHAN MENDOZA TORRES
Category: Johan Mendoza, Opinion
Tags: Fuerza Pública, Fuerzas militares
Slug: el-deber-comun-mensaje-a-la-fuerza-publica
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/11/fuerza-publica.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto:Nelson Cárdenas - SIG 

#### **Por: [Johan Mendoza Torres](https://archivo.contagioradio.com/johan-mendoza-torres/)** 

###### 29 Nov 2017 

Para nadie en este país es un secreto que una cosa es hablar de la guerra y otra estar en ella. No obstante, a pesar de que no es secreto, “algunos”, se atreven a subvalorar el esfuerzo tácito que implica desarrollar una guerra, un combate, un tiroteo, o un tropel; a esos “algunos”, se les suman los pacifistas que se la pasan citando a Gandhi, pero que no estarían dispuestos a salir a una calle a recibir ni siquiera un grito.  Gandhi no fue pacifista, fue un luchador pacífico, por algo murió como murió, y eso es algo que los pacifistas no logran diferenciar.

[Del mismo modo mucha gente no sabe identificar el carácter pragmático de una confrontación, donde la razón en ocasiones sucumbe al elemental instinto de supervivencia, impulsando a actuar de una forma que “vista desde afuera” no sería entendible, pues como se ha indicado durante siglos: el arte de la guerra es también el arte de la improvisación.]

[Colombia es un país convencionalmente desagradecido con la fuerza pública, porque olvida que los errores cometidos y que son explanados con detalle desde todos los sectores, son responsabilidad estructural de decisiones ejecutivas, de decisiones políticas en las que desde 1957, se arregló todo para que los militares y la policía, jamás volvieran a interferir y jamás volvieran a tener el control de una decisión político-administrativa.]

[El último gobierno militar que hubo en Colombia, hizo en 4 años lo que habían dejado de hacer los partidos políticos en más de 100 años. Carreteras, puentes, paz con guerrillas, centros de educación técnica, tecnológica, universidades, aeropuertos, voto a la mujer, introducción de la televisión al país, entrega de juguetes y alimentos a los más pobres, planes para fortalecer el mercado interno antes de abrirnos al mercado internacional, entre otras cosas.]

[Por supuesto faltó tiempo para decantar los errores típicos producto del encantamiento que causan las armas, sobre todo los fusilamientos de la calle 13 con carrera 7. No obstante, luego de que los militares fueran sacados del poder en el año 1957 ¿acaso los políticos no realizaron peores perversiones? ¿acaso no son ellos y no los militares los que están impresos en los billetes de la nación?  ¿acaso no son los políticos y sus familias, los que no pagan cárcel mientras sí lo hacen miembros de la fuerza pública de alto, mediano y bajo rango?]

[Mucha gente en Colombia conoce la composición de la fuerza pública, sabe que son familias de capas pobres o medias las que aportan hombres y mujeres a las filas del ejército o la policía; pero lo más extraño es que a pesar de conocer su composición, muchas personas no son conscientes de lo que eso significa.  Incluso, muchos socialistas o simplemente chicos rebeldes de salón, tienen un tío o un familiar que estuvo o está en la fuerza pública. Eso no es nada malo, es algo normal, pues la composición sociológica de la nación colombiana así lo denota. Además como la nación colombiana ha estado en tan poquitas manos y tan poquitos son los dueños del poder, que a pesar de que hayan hecho esfuerzos por dividirnos, por enemistarnos, al final… siempre fuerza pública y población civil, cuando no hay uniformes de por medio, estamos mezclados en la vida cotidiana.  ]

[Claro: nosotros peleando y perdiendo, y ellos ¡como siempre! fortaleciéndose y ganando.]

[La fuerza pública por naturaleza filosófico-política, es un elemento a merced de los poderes del Estado. No obstante, esos poderes no significan absolutamente nada, si nunca han demostrado ser dignos de la responsabilidad que cargan.   ]

[Algunos dicen, “]*[que los militares den un golpe de Estado y Colombia se arregla]*[”, pero no tienen cuidado para reconocer que eso resultaría en un mal tremendo si no se reconoce en primera instancia qué significa el honor militar, la dignidad del pueblo, y en todo sentido]**un deber común**[.]

[El problema no es un golpe de Estado, el problema es que nuevamente sean los políticos y las familias acaudaladas, las que promuevan el golpe, utilizando, instrumentalizando a la fuerza pública para cumplir sus propósitos particulares, y luego abandonarlos a la deriva en caso de que algo salga mal.]

[La fuerza pública no ha estado al servicio del Estado Colombiano, sino que, bajo la lógica de servir a la clase política que nunca ha conocido la guerra, la fuerza pública permitió que unos cuantos políticos se llevaran los votos, los aplausos, las pensiones, los honores, mientras que por otro lado pisotearon al país, utilizaron la fuerza pública para proteger sus intereses, pusieron a la gente en contra de la fuerza pública y viceversa, provocaron enfrentamientos evitables y a la cárcel, ¡¡a la cárcel!!! siempre fueron los mismos de siempre...   ]

[La fuerza pública tiene un deber común que cumplir. Tiene un Estado que proteger, pero no tiene por qué defender a una clase política que ha secuestrado al Estado y que cuando algo sale mal y la “comunidad internacional” exige resultados, esa misma clase política coge de chivos expiatorios a los militares, dejándolos en ridículo, subordinándolos por decisiones politiqueras a oficiales de bajo rango de otros países, que por ejemplo vienen a Tolemaida a dar órdenes donde no deberían darlas.]

[El deber común de la fuerza pública, pasa por reconocer que una sublevación militar no tiene como fin establecer lo bienes personales de una clase política, sino que tiene por fin establecer el rumbo de los intereses comunes de una sociedad. Los políticos se lo han tirado todo. Lo han arruinado. Y mantienen “el orden” (su orden) intacto a punta de “primas del silencio” que dan a un grupito pequeñito de generales, mientras a los demás los mantienen bajo lógicas administrativas absurdas, o incluso bajo lógicas injustas que han impulsado a militares retirados a protestar por sus pensiones, a cometer actos desesperados como el de la granada en Porvenir, o como el video de policías encapuchados comandados por el patrullero Rubén Darío Rozo Giraldo.]

[Rozo y sus hombres cometieron un error, y fue haberle puesto nombre propio a su protesta. El problema no es Santos, el problema no es Uribe, el problema es una clase política que alimenta bien a unos pocos generales para que no los bajen del poder, mientras mantienen a todos los demás en medio de la más penosa rapiña, miseria, odio, enemistad etc. ¿o acaso quién pagó por el tema de los falsos positivos? ¿Acaso Santos no era el ministro de defensa? ¿Acaso Uribe no era el comandante en jefe de las fuerzas armadas? … Ninguno pagó, Santos recibió un Nobel de Paz se irá a vivir a París, y Uribe se relajará con sus cientos de miles de hectáreas y su pensión vitalicia de 27 millones pesos que en comparación con los 800 mil pesos de pensión que le dan a un soldado profesional; ese tonto creyó que convirtiéndose en un “anti-Santos”, saldría bien librado.  ]

**Uribe y Santos son lo mismo, son clase política (oligarca y terrateniente) que utilizan a la fuerza pública para su propio fin y no el de la nación. Cuando se entienda esa simple afirmación el honor militar será reestablecido.**

[Cuando un oficial quiere estudiar para jefe de Estado, ¿por qué de manera amable es apartado de su cargo? ¿por qué por la retoma del palacio de justicia solo pagó Plazas, si el expresidente Belisario Betancour tenía que como mínimo haberle hecho compañía en la celda? Plazas quedó destruido, como buen militar hubiese preferido la muerte. Pero Belisario, sigue con pensión vitalicia, y hace poco pidió perdón, para limpiar su conciencia mientras a Plazas le tocó limpiar su celda.]

[Es que incluso, ¿por qué el monumento a los militares caídos es un miserable muro que no invita a ningún tipo de experiencia estética asociada con la gloria del Ejército? ¿por qué? …. ¿a quién no le interesa que la fuerza pública sea considerada “tanta cosa”?  ¡¡pues a la clase política!!]

[Si el honor militar se recuperara, si el deber común fuera cumplido, los políticos colombianos serían los que perderían. ¿Quiénes ganarían? ¡¡La gente hombre!!  ¡¡la gente!!   aquí esta vaina no la va a salvar un político, esta vaina no la va a salvar un empresario como esos que les ponen en el ministerio de defensa para que les den órdenes sobre cosas que ni siquiera entienden, gente que como Martha Lucía Ramírez, como Álvaro Uribe, como Juan Manuel Santos, como Luis Carlos Villegas, nunca]*[comieron mierda]*[ en la escuela.  ]

[El deber común no es buscar el apoyo de algún político que logre desarrollar el valor del honor militar. El deber común, es reconocer que el honor de la fuerza pública reside en ella misma, y es alimentado por los millones de personas que tenemos amigos, familiares y conocidos enlistados en la policía o en el ejército.]

[La clase política sigue sembrando odios, sin disparar una sola bala. Sigue conduciendo al país hacia un hoyo sin fondo producto de su codicia. La clase política enemista a pueblo contra pueblo, y le quedará muy berraco a una fuerza social detenerla a menos que ésta tenga el apoyo de la fuerza pública.]

[La alianza cívico-militar también está en la agenda del bien común. No es una alianza en la que politiqueros le endulzan el odio a los militares y policías. NO, ese sería el peor error. Eso es como los que odian a Santos pero aman a Uribe, solo son títeres incapaces de pensar con propiedad y determinación. Se trata más bien del despertar autónomo de la fuerza pública que debería responder al momento histórico y a la gloria que le han robado.]

#### [**Leer más columnas de Johan Mendoza Torres**](https://archivo.contagioradio.com/johan-mendoza-torres/) 

------------------------------------------------------------------------

###### Las opiniones expresadas en este artículo son de exclusiva responsabilidad del autor y no necesariamente representan la opinión de Contagio Radio. 
