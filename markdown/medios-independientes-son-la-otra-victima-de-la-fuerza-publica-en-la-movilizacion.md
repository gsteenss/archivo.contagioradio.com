Title: Medios independientes son la otra víctima de la Fuerza Pública en la movilización
Date: 2020-06-16 22:26
Author: AdminContagio
Category: Actualidad, DDHH
Tags: Libertad de Prensa
Slug: medios-independientes-son-la-otra-victima-de-la-fuerza-publica-en-la-movilizacion
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/06/c6eb466d-e90d-4390-8d4f-e55937b8a04d.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: Medios alternativos siendo golpeados por el ESMAD/ Contagio Radio

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

En el marco de las movilizaciones del pasado 15 de junio en diferentes ciudades de Colombia, reporteros gráficos que cubrían los hechos en ciudades como Medellín y Bogotá, incluído un integrante del equipo de Contagio Radio, fueron víctimas de detenciones arbitrarias y abusos de autoridad, golpes y tratos crueles que constituyen una violación a la libertad de prensa por parte de la Fuerza Pública

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Frente a la detención ilegal de trece reporteros gráficos que cubrían las manifestaciones del 15 de junio en Medellín, la Fundación para la Libertad de Presa (FLIP) rechazó lo acontecido e instó a investigar las actuaciones de los miembros de la Policía Nacional para que se tomen acciones que garanticen la libertad del ejercicio periodístico. [(Lea también: Seguimientos contra periodistas son propios de regímenes totalitarios: FLIP)](https://archivo.contagioradio.com/seguimientos-contra-periodistas-son-propios-de-regimenes-totalitarios-flip/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

La Policía detuvo a siete de los reporteros que pertenecen a los colectivos Periferia Prensa, AquiNoticias, Ab\_zurdo Colectivo,y los trasladó a diferentes estaciones de Policía y Unidades de Reacción Inmediata. Horas después, cuatro de ellos fueron dejados en libertad mientras los tres restantes: Harrison Agudelo, Juan Carlos Londoño y Juan Pablo Herrera fueron trasladados a las instalaciones de la Fiscalía General de la Nación en Medellín para iniciar el proceso de judicialización, 24 horas después y a la hora de cierre de esta nota, no se ha definido su situación jurídica.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

**José Luis Marín abogado y periodista de Aquinoticias de Medellín, uno de los periodistas afectados, relata que aunque se identificó como prensa e intentó mostrar su carné a la Policía fue detenido** y al interior del bunker de la Fiscalía se encontró con otros colegas que también habían sido capturados de forma arbitraria. [(Lea también: Más de 100 detenidos y 20 heridos deja la violencia policial contra movilización del 15 de Junio)](https://archivo.contagioradio.com/violencia-policial-15-junio/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

**"No nos habían leído los derechos ni los motivos por los que estábamos detenidos, todos los detenidos eran periodistas de medios alternativos",** relata Marín sobre las agresiones contra ocho de ellos, uno de ellos con un antebrazo fracturado, además señalan que durante el operativo también dañaron algunos de sus equipos periodísticos como cámaras y celulares.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Frente al episodio, la FLIP también se dirigió a la Alcaldía de Medellín, en cabeza de Daniel Quintero Calle a que brinde las garantías en la labor periodística en Medellín y a la Procuraduría para investigar a los funcionarios responsables de violar la libertad de prensa en la ciudad.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

De igual forma y según el balance hecho por la [FLIP](https://twitter.com/FLIP_org) en Bogotá, un reportero de prensa independiente resultó herido con posible fractura de craneo, tres fotógrafos fueron capturados, y múltiples periodistas fueron golpeados y censurados. **Durante las manifestaciones de 2020 la FLIP ha registrado siete ataques contra la prensa, sin incluir este más reciente episodio.**

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### "Es hora que en Colombia surjan medios con otras posiciones y que cuenten más verdades"

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

El periodista también hizo referencia a la ausencia de reconocimiento por parte de las autoridades hacia los medios independientes al comparar el trato con los medios empresariales, aunque en algunas ocasiones se ha denunciado abuso por parte de la policía contra estos medios, la mayoría de los casos se presentan contra periodistas de medios independientes o alternativos.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Ante dicha disparidad, el periodista señala la importancia de "tejer redes de apoyo" entre la ciudadanía y otros medios populares, "tenemos que evitar caer en la soledad por parte de los medios de comunicación alternativos, presionar y seguirnos ganando un espacio en la sociedad colombiana" afirma el periodista quien advierte que la Internet ha dado la posibilidad a quienes no tiene la oportunidad de hacer parte de los medios hegemónicos de lanzarse a la calle y hacer reportería.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

"Hay que demostrar que el monopolio de la verdad no le pertenece a los Santodomingo, al grupo Gilinski o a Ardila Lulle", afirma el periodista quien considera que las nuevas tecnologías permitirán tarde o temprano una mayo democratización de la información. [(Lea también: Asi es hacer periodismo en el Bajo Cauca Antioqueño)](https://archivo.contagioradio.com/asi-es-hacer-periodismo-en-el-bajo-cauca-antioqueno/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

En la actualidad, el Grupo Gilinski, dueños del banco GNB Sudameris, posee el 50% de la Revista Semana mietras tres grupos familiares en cabeza de Luis Carlos Sarmiento, Carlos Ardila Lulle y Alejandro Santodomingo controlan el 57% de la televisión, radio e internet en Colombia.

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
