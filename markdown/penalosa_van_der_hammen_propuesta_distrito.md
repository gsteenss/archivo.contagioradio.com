Title: Peñalosa desconoce institucionalidad en materia ambiental frente a la Van der Hammen
Date: 2018-04-03 16:51
Category: Ambiente, Nacional
Tags: Alcalde Enrique Peñalosa, Reserva Thomas Van der Hammen
Slug: penalosa_van_der_hammen_propuesta_distrito
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/04/Diseño-sin-título-1-e1522792225586.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Sec. Ambiente / La W] 

###### [3 Abr 2018] 

Aunque la alcaldía de Bogotá ha asegurado que su propuesta frente a la Reserva Thomas Van der Hammen se basa en volver a delimitarla para ampliar su área, cambiar su categoría de productora a protectora y que sea de uso público y no privado, el movimiento ambientalista asegura que este nuevo anuncio del distrito **no es más que una estrategia de comunicación que "de ninguna manera mejora la reserva".**

"Hay dudas frente a que sea una propuesta diferente a lo que se había dicho hace dos años. El nuevo tono de Enrique Peñalosa es una estrategia para despistar el debate. Vamos a estudiar la propuesta cuando nos la hagan llegar", dice Sabina Rodríguez, abogada y veedora de la reserva, quien agrega que si bien el alcalde ha dicho a los ambientalistas que antes de criticar deberían estudiar la propuesta, **lo cierto es que a ellos el distrito no les ha hecho llegar dicha propuesta.** [(Le puede interesar: Peñalosa continúa firme en sus proyectos depredadores) ](https://archivo.contagioradio.com/reserva_thomas_van_der_hammen_enrique_penalosa_cerros_orientales/)

### **Las posibles mentiras de la propuesta dela Alcaldía** 

De acuerdo con los ambientalistas, aunque el mapa que ha mostrado el Distrito aparentemente muestra que habría amplias zonas verdes en su proyecto de urbanización, para Sabina Rodríguez se trata de "una comparación desleal, porque ponen mucho verde en el mapa pero desconocen dos de las cuatro categorías que tiene la reserva y no le suman los ecosistemas colindantes como el humedal Torca-Guaymaral, pero además pintan de verde las vías", que no serían otra cosa más que cemento.

Asimismo, la ambientalista resalta que la alcaldía se ha caracterizado por decir que en el inconformismo de quienes defienden la Thomas Van der Hammen reina la desinformación. Sin embargo de lo que poco se ha hablado es que desde el año 2015, el plan de manejo ambiental de **la reserva cuenta con  90 mil millones que desde hace tres años están en la Secretaría de Ambiente**. Estos se deberían haberse destinado a la compra de predios para fortalecer la reserva, pero la actual administración no ha querido ejecutar dichos recursos, ya que no están de acuerdo a sus intereses en esa zona.

"Todo esto viene de la desinformación de la alcaldía. Dicen que nadie sabe con qué y cuánto se requiere para que la reserva sea una realidad, pero en realidad que **existe todo un plan de manejo ambiental que tiene obligaciones especificas para mantener la reserva",** explica Rodríguez, quien añade que esto evidencia que lo que está haciendo Peñalosa es desconocer la institucionalidad del país.

### **Veeduría ciudadana a la CAR** 

Frente al nuevo anuncio por parte de la administración, los ambientalistas están a la espera del fallo nuevamente de primera instancia sobre la acción de cumplimiento que habían interpuesto. (Le puede interesar: [Ambientalistas piden respuestas a Minambiente)](https://archivo.contagioradio.com/ambientalistas_respuesta_minambiente_reserva_thomas_van_der_hammen/)

"**No tenemos más acciones legales porque lo que estamos defendiendo es que se cumpla la Ley.** Estamos haciéndole seguimiento a que las autoridades cumplan lo que se debe cumplir", dice Sabina Rodríguez, e invita a que sea toda la ciudadanía la que haga la veeduría al proceso que afronta la reserva ante la CAR Cundinamarca.

<iframe id="audio_25054939" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_25054939_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)
