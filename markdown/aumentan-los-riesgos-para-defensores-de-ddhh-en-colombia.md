Title: 279 agresiones y 35 asesinatos contra defensores de DDHH en Colombia durante 2016
Date: 2016-08-17 17:02
Category: DDHH, Entrevistas
Tags: Derechos Humanos, paz, Somos defensores
Slug: aumentan-los-riesgos-para-defensores-de-ddhh-en-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/08/defesnsores-dh-.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [17 Ago 2016]

Pese a que cada vez Colombia se acerca más a un acuerdo final de paz entre el gobierno y la guerrilla de las FARC, los defensores de derechos humanos continúan siendo blanco de amenazas y asesinatos. Las cifras este año van en aumento en relación al mismo periodo de 2015, registrándose un total de 314 agresiones, entre ellas 36 asesinatos.

[De acuerdo con el más reciente ]informe de la ONG Somos Defensores, [entre enero y junio de 2016, 35 defensores de derechos humanos fueron asesinados en Colombia y 279 sufrieron algún tipo de agresión. De ellos, 2]32 fueron víctimas de amenazas, se cometieron 21 atentados contra la vida de los defensores de derechos humanos, 13 fueron víctimas de detención arbitraria, hay 9 casos con procesos de judicialización, 3 fueron víctimas de robos de información estratégica y un defensor está desaparecido en la zona del Catatumbo.

Frente a los responsables de estas agresiones, los grupos paramilitares tienen la responsabilidad del 68% de los casos, 22% actores desconocidos, y las fuerzas de seguridad del Estado tiene un aumento del 10% por origen de la policía, el ejército o la fiscalía. Por su parte, el grupo paramilitar Águilas Negras, es el responsable del 90% de las amenazas contra defensores en los últimos 5 años, un accionar que no cesó durante este primer semestre del año.

¿Este es el fin? es el título del informe que asegura que "A las puertas de firmar un acuerdo de paz en Colombia, los índices de asesinatos y agresiones contra defensores de derechos humanos siguen subiendo en contraste con el descenso acelerado de las acciones violentas derivadas del conflicto. ¿Qué está pasando? ¿Este será en verdad el fin del conflicto? ¿Con esto se acabará la violencia contra defensores? Y además de ello, si los defensores(as) son pieza clave en la construcción de la paz, ¿cómo los va a proteger este gobierno? Análisis de un reto sin precedentes".

[Este Es El Fin Informe Semestral 2016](https://www.scribd.com/document/321496925/Este-Es-El-Fin-Informe-Semestral-2016#from_embed "View Este Es El Fin Informe Semestral 2016 on Scribd") by [Contagioradio](https://www.scribd.com/user/276652802/Contagioradio#from_embed "View Contagioradio's profile on Scribd") on Scribd

<iframe id="doc_24616" class="scribd_iframe_embed" src="https://www.scribd.com/embeds/321496925/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-LYijTUImjeHpyMqDZlHZ&amp;show_recommendations=true" width="75%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="0.6872616323417239"></iframe>  
<iframe src="http://co.ivoox.com/es/player_ej_12577305_2_1.html?data=kpeimZyXdJahhpywj5aUaZS1lZ2ah5yncZOhhpywj5WRaZi3jpWah5yncaTV09Hc1ZCrucbqwtfOh5enb7TjztTgjanJqsbi1NTfx9iPcYarpJKw0dPYpcjd0JC_w8nNs4yhhpywj5k.&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
