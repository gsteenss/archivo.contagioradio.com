Title: ONU preocupada por descalificación de gobierno a asesinatos de defensores de DDHH
Date: 2017-12-21 12:44
Category: DDHH, Nacional
Tags: asesinato de líderes sociales, Derechos Humanos, lideres sociales, Naciones Unidas, ONU
Slug: onu-preocupada-por-descalificacion-de-gobierno-a-asesinatos-de-defensores-de-ddhh
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/12/LIDERES-SOCIALES.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [21 Dic 2017] 

La Oficina en Colombia del Alto Comisionado de las Naciones Unidas, alertó sobre la grave situación que viven los líderes sociales y defensores de derechos humanos en Colombia y, contrario a las cifras que dio el Ministro de Defensa, la ONU indicó que solo en el 2017 han ocurrido **105 homicidios** de líderes sociales.

De acuerdo con el comunicado que envió ONU Derechos Humanos, hay una preocupación por la **descalificación, el hostigamiento y la estigmatización** de las y los defensores de derechos humanos en el país. Por esto, reiteraron que “ser defensor es una labor legitima y necesaria para la consolidación de la democracia y la construcción de un paz estable y duradera”. (Le puede interesar: ["32 líderes sociales han sido asesinados durante 2017 en Cauca"](https://archivo.contagioradio.com/aumenta-preocupacion-por-asesinato-de-lideres-sociales-en-el-cauca/))

Además, las Naciones Unidas manifestaron que el desconocimiento de los magistrados elegidos para hacer parte de la Jurisdicción Especial de Paz, con el argumento de ser defensores de derechos humanos, **“muestra el irrespeto a su labor** y desconoce lo que significa un Estado de derecho”. También, ante las declaraciones del Ministro de Defensa, el organismo reiteró que decir que los asesinatos ocurren por líos de faldas, “es justificar asesinatos que en ningún caso son justificables”.

### **Asesinatos de líderes sociales afectan a la población más vulnerable del país** 

Naciones Unidas también indicó que los asesinatos están ocurriendo en zonas del país donde **hay un vacío de poder** que el Estado no ha tenido la capacidad para llenar. Cuando esto sucede, “las personas no tienen la capacidad de tener acceso a los derechos humanos” y por esto la labor de los defensores es vital. Por lo tanto, cuando ocurre un asesinato de un líder social “se afecta a la población más vulnerable del país”. (Le puede interesar: ["MinDefensa pone en riesgo la vida de líderes sociales: Somos Defensores"](https://archivo.contagioradio.com/min-defensa-expone-la-vida-de-lideres-sociales-somos-defensores/))

También, dice el comunicado que “las consecuencias del asesinato de una líder o un líder social son más complejas de lo que parecen interpretar algunos funcionarios del Gobierno”. De la misma forma, “la magnitud de las **repercusiones en el tejido social** y comunitario deben ser consideradas por las autoridades estatales en su conjunto”. Por esto, el organismo instó a que debe haber investigaciones, juzgamiento y sanciones efectivas de los responsables para así prevenir que sigan ocurriendo estos hechos.

###### Reciba toda la información de Contagio Radio en [[su correo]

<div class="osd-sms-wrapper">

</div>
