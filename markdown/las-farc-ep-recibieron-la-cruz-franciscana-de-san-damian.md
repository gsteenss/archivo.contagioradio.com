Title: Las Farc-Ep recibieron la cruz franciscana de San Damián
Date: 2017-06-29 05:00
Category: Abilio, Opinion
Tags: FARC, paz, teologia de la liberación
Slug: las-farc-ep-recibieron-la-cruz-franciscana-de-san-damian
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/06/la-cruz-franciscana-de-San-Damián.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

#### Por **[Abilio Peña ](https://archivo.contagioradio.com/abilio-pena-buendia/) - [~~@~~Abiliopena](https://twitter.com/Abiliopena){.twitter-atreply .pretty-link} **

###### 28 Jun 2017

[Un día antes de que se celebrara la entrega de más de siete mil armas de las Farc-Ep a  la Misión de la Organización de Naciones Unidas  un significativo grupos de creyentes de la Coalición Cristiana por  la Paz, integrada por delegaciones de la Mesa Ecuménica por la Paz y del Diálogo Intereclesial por la Paz –DiPaz- y la Iglesia Sueca, visitaron la Zona Veredal Antonio Nariño, vereda la  Fila, en Icononzo Tolima.]

[Integrantes de la delegación entregaron a delegados de  las Farc-Ep la Cruz de San Damián, de profundo significado  para San Francisco, quien en un momento  crucial de su vida,   ingresó a la pequeña capilla en que estaba colgada la colorida cruz   y suplicó al  Jesús crucificado: “ilumina las tinieblas  de mi corazón y dame fe recta, esperanza cierta y caridad perfecta para que cumpla tu santo mandamiento.” Como respuesta, dice la tradición, el crucificado le  habló: «Francisco, ¿no ves que mi casa se derrumba? Anda, pues, y repárala». Nada que ver con la cruz que acompañó las espadas de las cruzadas y del exterminio de nuestros pueblos  indígenas de América.]

[En el mes de diciembre del año anterior,  la comunidad Franciscana de Asís, le había entregado al presidente de Colombia Juan Manuel Santos la “Lámpara de la Paz de Francisco” optando por la parte más poderosa,  conocida, prestigiosa  y dejando de lado la otra, más incomprendida, estigmatizada, señalada, censurada. Varias reacciones escritas se conocieron de otros miembros de la familia franciscana en el mundo que no estuvieron de acuerdo con que se le entregara la lámpara solamente al presidente.]

[San Francisco no hubiese permitido que una sola de las partes recibiera su lámpara, pues un acuerdo se firma entre  dos. En su tiempo,  él mismo medió por un acuerdo de paz entre los católicos y musulmanes que se peleaban el control de Jerusalén. Para su desconsuelo encontró más eco en los musulmanes – erigidos en monstruos por los católicos- que en los miembros de su propia iglesia encargados de dirigir la cruzada, quienes luego de haber escuchado de Francisco la disposición del Sultán para un acuerdo de paz, ordenaron un ataque al campamento donde se encontraba.]

[El acto en que  las Farc-Ep recibieron la cruz de San Damián fue simple, sencillo, sin medios masivos de información,  sin notas de presa, algo tan “pobre” como el propio Francisco.  Estaban ahí, en su Zona Veredal  atendiendo la visita. Se les entregó primero dos frágiles   vasijas de barro que si se descuidan se pueden romper, como el acuerdo. Luego la cruz de San Damián acompañada de la explicación de su sentido. Francisco es el Santo de la Paz, de la opción por los empobrecidos. Ante esa cruz entendió su misión de “reconstruir la iglesia” en la perspectiva de la fidelidad al Evangelio de los humildes y sencillos de Jesús, en perspectiva de la justicia, sobre la base de la fraternidad. Muy lejos de las ruinas de una iglesia que se edificó desde el poder de la espada y de los pactos con los sectores dominantes de la sociedad, de los que Francisco se desprendió.]

[Se trataba de un acto de elemental justicia, que fue recibido con alegría y respeto por parte de los miembros de la guerrilla en tránsito a la vida civil, quienes expresaron su tranquilidad por haber cumplido con la entrega de armas y  sus miedos ante los  incumplimientos de los acuerdos por parte del Estado. Pidieron el acompañamiento de la sociedad, pues sobre ellos pesa la historia del genocidio de la Unión Patriótica que dio al traste con las aspiraciones de paz en la década de los ochenta.]

[Que la cruz  franciscana de San Damián aliente a estos hombres y mujeres  a recorrer las sendas de la paz y a seguir buscando la justicia desde la acción Noviolenta, ante los incumplimientos a los acuerdos que ya se conocen.]

#### **[Leer más columnas de Abilio Peña](https://archivo.contagioradio.com/abilio-pena-buendia/)**

------------------------------------------------------------------------

###### Las opiniones expresadas en este artículo son de exclusiva responsabilidad del autor y no necesariamente representan la opinión de Contagio Radio.
