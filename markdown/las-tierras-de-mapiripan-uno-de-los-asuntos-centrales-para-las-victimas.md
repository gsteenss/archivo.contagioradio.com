Title: Las tierras de Mapiripán, uno de los asuntos centrales para las víctimas
Date: 2017-07-12 16:06
Category: DDHH, Nacional
Tags: mapiripan, Meta, Reparación, restitución, Víctimas de Mapirián
Slug: las-tierras-de-mapiripan-uno-de-los-asuntos-centrales-para-las-victimas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/07/Poligrow.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [12 Jul. 2017] 

En el sur del Meta, donde se encuentra **Mapiripán, hasta diciembre de 2016 habían sido radicadas 2.272 reclamaciones en la Unidad Nacional de Restitución de Tierras** por parte de las víctimas de la masacre de Mapiripán que sueñan con regresar a sus territorios, despojados por paramilitares en la Masacre que tuvo lugar en ese municipio entre el 15 y el 20 de julio de 1997.

Según las víctimas, la restitución presenta dos grandes dificultades. Una es que las tierras reclamadas no han tenido un desarrollo satisfactorio, pues **no se han comenzado a realizar las microfocalizaciones y en otros casos hay presencia de multinacionales** que a través de la siembre de palma de aceite han asumido el control de algunos de los territorios pedidos por ellos y ellas. Esto además de la presencia paramilitar que impide, en algunos casos que las familias se sientan seguras para solicitar la restitución.

**María Cecilia Lozano integrante de la Asociación de Mujeres Desplazadas del Meta – ASOMUDEM**- asegura que hace 20 años estaban tranquilos en Mapiripán donde vivían y tenían sus propiedades y “disfrutábamos y ayudábamos a otras personas con trabajo porque generábamos empleo y vivíamos una vida digna, porque la mayoría éramos propietarios de tierras”.

A partir de ese 15 de julio las vidas de los habitantes de Mapiripán tomaron otro rumbo y como campesinos, manifiesta que nunca habían pensado que un día tendrían que salir de sus territorios “hasta que llega ese día tan horrible y macabro en donde asesinan muchas personas y tuvimos que desplazarnos” recuerda Lozano.

### **Mapiripán es el municipio que más solicitudes de restitución de tierras ha radicado** 

Muchas personas fueron desplazadas por esa masacre cometida por paramilitares con el apoyo del Ejército Nacional, exactamente **fueron 511 familias las que salieron de ese municipio sin que 20 años después hayan logrado recuperar sus tierras**, sino que deban vivir en condición de desplazamiento.

“Fue muy difícil nuestra situación, hay víctimas que no han sido reconocidas aún, cuando salimos de allá no sabíamos ni que éramos desplazados y han pasado 20 años en los cuales el Gobierno no nos ha dado respuesta ante la reclamación de tierras” cuenta Lozano. Le puede interesar: [Ordenan suspender operaciones de Poligrow Colombia por daños ambientales](https://archivo.contagioradio.com/suspenden-operaciones-de-poligrow-colombia-por-infracciones-ambiantales/)

**Mapiripán es el municipio que más solicitudes de restitución de tierras ha radicado hasta el momento, sin embargo, esto no ha sido suficiente** para que la institución de respuesta. Por lo cual, para Lozano, la ley “habla sobre el tema de restitución, pero cuando uno va a hacer exigencia de ese derecho no es tan real”.

En la actualidad las personas se encuentran desplazadas en diversos lugares del país como Villavicencio uno de los lugares donde hay más víctimas de Mapiripán, Guainía, Vaupés, Guaviare, Casanare y Vichada.

### **Mapiripán quedó como un jarrón roto en mil pedazos y lo estamos reconstruyendo** 

Dice Lozano que **ante tanto abandono estatal a las víctimas de Mapiripán que están desplazadas decidieron organizarse**, haciendo que la voz llegara a todos los rincones y pudieran juntarse para poder hablar y apoyarse en la reconstrucción del tejido social, pero también para que todas las familias puedan regresar a sus territorios.

“Sabemos que estamos aquí, aunque no deberíamos estar. Ha sido bastante difícil y cuando nosotros queremos hacer todo el tema de manera colectiva encontramos que hay otros factores externos como la falta de garantías, el interés en la tierra o que la comunidad que se quedó dice que no quiere que regresemos. Es muy difícil reconstruir el jarrón, pero toca a pedacitos” manifiesta Lozano. Le puede interesar: [La cara oculta de Poligrow, empresa palmera en Mapiripán](https://archivo.contagioradio.com/poligrow-palma-mapiripan/)

### **“No hemos podido regresar porque hay intereses en las tierras”** 

Lozano manifiesta que al regresar a Mapiripán para realizar un encuentro de víctimas notó cómo ha cambiado la tierra “me monté en el avión y me quedaba profundamente mirando cómo se veían grandes extensiones de tierra con puros cultivos de palma, mucha palma y caucho”. Eran** cerca de 14 mil hectáreas las que eran usadas por los campesinos para ganadería, cultivar maíz, sembrar yuca.**

“A uno le da tristeza, porque **para ellos – los empresarios – es el bienestar del pueblo, la economía y para nosotros fue el despojo** y esos son los resultados de ese despojo, porque nosotros estamos fuera del territorio no pudimos regresar mientras que hay una multinacional allá. A mí, me limitan para ir allá, tengo que tener escolta porque si no me amenazan, pero las multinacionales pueden andar como Pedro por su casa y nada pasa”

Lozano hace referencia a la multinacional **Poligrow de origen italiano a quien la Contraloría abrió investigación por acumulación de predios baldíos,** que según la entidad suman casi cerca de 5577 hectáreas, superando la Unidad Agrícola Familiar. Le puede interesar: [Informe sobre acumulación de baldíos se quedaría sin piso si se aprueba ley de tierras](https://archivo.contagioradio.com/123-mil-hectareas-de-tierras-habrian-sido-adquiridas-irregularmente-contraloria/)

Y añade que además de la multinacional, **en el territorio hay presencia de paramilitares y de otras fuerzas que nadie denuncia** “hay muchos grupos paramilitares allá, no es solo el bloque Guaviare, los Gaitanistas sino otra clase de grupos, todo el mundo sabe y nadie hace nada al respecto”.

### **Proceso de microfocalización no avanza por presencia paramilitar** 

Denuncia Lozano que en reiteradas ocasiones ha estado en reuniones con la Policia en las que aseguran que en el pueblo no ha pasado nada y el Ejército por el contrario asegura que hay algunos combates en algunas veredas, pero **no hay seguridad completa para que pueda darse la restitución.**

“Entonces lo que nos han dicho es que se harán progresivamente, pero uno no entiende si saben dónde están y quiénes son entonces porque el Gobierno no hace nada para que las víctimas si puedan reclamar sus tierras” afirma Lozano.

**Hasta el momento de los 29 municipios del departamento solo se han realizado 23 microfocalizaciones** para restituir luego las tierras de las cuales 12 tienen sentencias de restitución. Le puede interesar: [Palmera Poligrow consume 1'700.000 litros de agua diarios en cada plantación en Mapiripán](https://archivo.contagioradio.com/audiencia-en-congreso-de-la-republica-por-accion-de-poligrow-en-mapiripan/)

### **Que el Estado cumpla, exigen las víctimas** 

Luego de 20 años de luchar por intentar recuperar sus predios, dice Lozano que los planes de vida se rompieron y hoy en día siguen exigiendo que devuelvan sus tierras, pero además con garantías de seguridad para poder regresar.

“Hay mucha gente que está aguantando hambre, hay personas de avanzada edad porque han pasado 20 años, nuestros hijos ya crecieron, pero **no hay garantías del Estado para poder regresar por lo menos con el derecho a una verdadera reparación**, porque ni siquiera la reparación colectiva se ha podido hacer” dijo Lozano.

<iframe id="audio_19770927" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_19770927_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU).

<div class="osd-sms-wrapper">

</div>
