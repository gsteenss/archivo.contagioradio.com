Title: Fase pública entre gobierno y ELN sería a partir del 8 de febrero
Date: 2017-01-18 10:25
Category: Nacional
Tags: ecuador, ELN
Slug: fase-publica-gobierno-eln-seria-partir-del-8-febrero
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/10/eln-y-gobierno.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: contagioradio] 

###### [18 Ene 2016] 

El 7 de febrero se instalará la mesa de conversaciones entre el gobierno y el ELN y la mesa comenzará a sesionar el 8 de ese mismo mes. Adicionalmente se conoció que Odín Sánchez será liberado el 2 de febrero. El anuncio se hace luego de 5 días de trabajo entre las delegaciones y el estudio de las propuestas entregadas por algunos sectores sociales.

Antes de este anuncio las delegaciones tuvieron un receso para consultas desde el pasado mes de Diciembre, en el que se lograron definir mecanismos de distensión que permitiera abrir la fase pública, estancada desde el 27 de Octubre de 2016.

Durante estos 5 días se logró definir la comisión humanitaria para la liberación de Odín Sánchez y establecer los protocolos de seguridad del operativo que ya estaría en marcha. Igualmente el gobierno estaría definiendo los trámites para que dos integrantes del ELN puedan unirse a la delegación de paz de esa guerrilla.

Para resolver esa situación ya se habían presentado varias propuestas por parte de la sociedad civil representada en cerca de 100 intelectuales, también se había hecho pública la posibilidad de definir una comisión internacional para un mínimo acuerdo humanitario propuesta por Monseñor Darío Monsalve.

###### Reciba toda la información de Contagio Radio en [[su correo]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio]{.s1}](http://bit.ly/1ICYhVU)

<div class="ssba ssba-wrap">

</div>
