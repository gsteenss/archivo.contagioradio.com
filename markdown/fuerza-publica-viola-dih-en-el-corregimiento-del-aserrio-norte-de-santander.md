Title: Fuerza pública viola DIH en el corregimiento del Aserrío, Norte de Santander
Date: 2015-07-10 12:42
Category: DDHH, Nacional
Tags: ASCAMCAT, Catatumbo, Derecho Internacional Humanitario, Fuerza Pública, Juan Fernando Cristo, Ministro del Interior
Slug: fuerza-publica-viola-dih-en-el-corregimiento-del-aserrio-norte-de-santander
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/07/BACOA_CENTURION_INSERCION_OP__8_.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: [www.24horas.cl]

<iframe src="http://www.ivoox.com/player_ek_4761355_2_1.html?data=lZyjk5iZeY6ZmKiak5eJd6KlkpKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRitbZ09%2FOjbWJh5SZo6bPzs7HpYzqytTZw5CojamfxtOYx9GPp9Dm08rUy9LNqc%2Fo0JDRx9GPhdTZ09eah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Olga Quintero, ASCAMCAT] 

###### [10 de Junio de 2015] 

La Asociación Campesina del Catatumbo, ASCAMCAT, denuncia que la **Fuerza Pública nuevamente pone en riesgo a los habitantes del corregimiento "el Aserrío"** violando el Derecho Internacional Humanitario, luego de la caída de un helicópetero  del Ejército Nacional en el municipio de Teorama, Norte de Santander.

De acuerdo a la comunidad, **la Fiscalía, el CTI, Ejército y la Policía Nacional, están en el corregimiento y cargan un listado de nombres de personas y fotografías**, además van acompañados de tres personas encapuchadas desconocidas .

Esta situación preocupa a los pobladores, quienes creen que podría tratarse de un nuevo operativo **para capturar campesinos como “falsos positivos judiciales”,** como ya sucedió en el año 2010, cuando el Ejército entró al corregimiento y 3 meses después llegó con órdenes de captura de 17 personas, que finalmente fueron dejadas en libertad tras demostrarse su inocencia.

**Entre 1000 y 1200 agentes de la fuerza pública** hacen presencia dentro y alrededor del Aserrío, donde **habitan aproximadamente 1000 personas,** informa Olga Quintero, integrante de ASCAMCAT, quien añade que la situación ha ido empeorando ya que los policías y militares han ocupado las casas de los pobladores y se están realizando retenes, por lo que hay zozobra en la comunidad y muchos han debido desplazarse de sus hogares. Así mismo, continúan los enfrentamientos entre la guerrilla y la fuerza pública y se han allanado y saqueado las casas de los habitantes del Aserrío.

La integrante de ASCAMCAT, dice que el Ministro del Interior, Juan Fernando Cristo, anunció que se establecerían dos batallones más en el Catatumbo, “**si se está hablando de paz, no entendemos por qué se siguen militarizando los territorios**”, expresa Quintero.

ASCAMCAT, exige a la fuerza pública y al Gobierno Nacional el retiro inmediato de la policía y los militares que están ubicados en el parque principal, iglesia, casas y fincas del corregimiento del Aserrío, también se solicita que se deje de obligar a la población civil para que haga parte del conflicto armado.
