Title: De la peste profunda al salto cuántico
Date: 2020-04-15 17:00
Author: Camilo de las Casas
Category: Camilo, Opinion
Tags: Covid-19, Estado Colombiano, politica, salto cuántico
Slug: de-la-peste-profunda-al-salto-cuantico
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/04/jump_exercise_health_sun_mountain1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

Terminadas las excusas para modificar y despojarse de tantos privilegios, siguen presos de su ceguera.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Corporaciones bancarias, poderes empresariales privados, algunos gobernantes, unos herederos leales del neoliberalismo, otros cómplices facilistas de la aniquilación con militarizaciones o leales a sus narcas elecciones. Muchos tienen miedo. Miedo de morir por pandemia, ellos también lo tienen.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Sin excepciones a unos y otros, el COVID-19 puede contagiar sin resultados previsibles de vida o de muerte. Incluso, a aquellos que huyen de una ciudad a otra o buscando refugio en protectorados privados en donde se conciben inmunes. Ese puñado de privilegiados que con multimillonarias cuentas creen escapar. En New York, Mónaco, París. Ellos que huyen están siendo presos de su propio miedo, de su propia cobardía.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Hasta un mandatario llegó hasta sala de cuidados intensivos. Otro de linaje azul identificado con COVID-19. Un banquero internacional que murió por esta peste. Un reconocido jurista contagiado que salió de la clínica hace unas horas.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":5} -->

##### Ese miedo es posibilidad.

<!-- /wp:heading -->

<!-- wp:paragraph -->

Puede despertar la raíz profunda del egoísmo, de dolores profundos sin resolver vueltos la ley del más fuerte y las nuevas formas del fascismo.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

**¡A veces el miedo puede más!.** O el miedo que moviliza a morir en las mismas esclavitudes o el miedo que moviliza a las libertades que nacen en la solidaridad y el sentido del bien común, el sentido como especie humana, el sentido de la honestidad con el tiempo que nos ha sido concedido en la tierra.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Olvidamos que nadie se libra de la muerte. Ni los de arriba ni los de abajo ni los del medio. Algunos se libran o nos libramos de la peste.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":5} -->

##### Hoy el COVID-19 es el síntoma más temido de la peste.

<!-- /wp:heading -->

<!-- wp:paragraph -->

Una peste que mata rápidamente, distante de las limpiezas étnicas, de las masacres que pasan en un día, de los muertos por hambre, de los muertos y expoliados por el cambio climático, de los muertos silenciados por fiebre amarilla, dengue o por paludismo. El COVID-19 es el síntoma de la peste que se disfraza en cifras y en retórica populista para ocultar la profunda desigualdad, la acumulación de riqueza sin distribución alguna, la destrucción de las fuentes de supervivencia, la construcción de pensamiento flasheo que nos esclaviza haciéndonos creer que somos felices.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":4} -->

#### En tiempos de incertidumbre mundial se rompe el espejismo del país más feliz del mundo.

<!-- /wp:heading -->

<!-- wp:paragraph -->

Los cuentos fantaseados de héroes y villanos, de responsables que han hecho desde la institucional del Estado un aparato criminal, un aparato para diversos intereses fragmentados, nunca contradictorios. Una formal democracia que adoba la exclusión y que hoy a través de esta crisis pandémica va quedando al desnudo.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Ellos quedan al descubierto guardando silencio, orientado la opinión a otro foco de intereses o mintiendo con estrategias publicitarias donde vuelven héroes a hombres y mujeres que son víctimas de esas privatizaciones que han hecho del trabajo una mercancía, del derecho a la salud un negocio, y de la justicia una parafernalia para absolver a los poderosos y condenar a los empobrecidos a un fatal destino.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Los cortesanos nacionales de las decisiones de la banca mundial que desde los 80 fueron haciendo de lo público mercancía pasaron en la historia sin ser identificados como los responsables de impedir un modelo agrario y ambiental incluyente, con tierra para todos, con producción en circuitos de producción limpia y justa, de relación campo ciudad a través del **IDEMA**. O también, los privatizadores de los servicios de aseo, de transporte y luego de la salud y la educación. Ellos saquearon lo público e hicieron sus propias EPS, IPS, empresas de transporte, empresas inmobiliarias y de construcción, agronegocios (entre ellos el narcotráfico).

<!-- /wp:paragraph -->

<!-- wp:heading {"level":5} -->

##### Todo se hizo negocio.

<!-- /wp:heading -->

<!-- wp:paragraph -->

Los que privatizaron la salud pública a través de la ley 100 están ahí en ejercicio de gobierno. Desde ese momento empezó la pauperización de las garantías laborales de los médicos, de las enfermeras, de todos los trabajadores de la salud, y una informalidad que supera el 60% de los que aparecen en las encuestas como personas que reciben ingreso por una labor.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Todo se fue haciendo mercancía golpeando a sectores medios y medio altos viviendo del complejo de pirámide, sorteando el día a día, tratando de evitar volverse pobres. Ellos, los que hoy representa Duque, con la seguridad democrática pretendieron asegurar una fase superior de la seguridad inversionista con privilegios legales, y tributarios a las multinacionales, de las que ellos, reciben coimas.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En ese momento se logró la asunción del proyecto narco en la política en su totalidad. Allá en la *Casa de Nari* se concretó el pacto con el establecimiento empresarial para acelerar el modelo agroindustrial y el extractivismo internacional.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":5} -->

##### Ese pacto inconfesable sigue rigiendo los destinos del país.

<!-- /wp:heading -->

<!-- wp:paragraph -->

“Neñes”, “Fantasmas” están por doquier en esta nueva fase de la seguridad democrática. Y queda al descubierto en esta crisis que tendrá consecuencias económicas que ni la minería ni el petróleo generan capacidades reales de calidad de vida para el país. Ese sector hoy tienen tanto miedo de perder, que mientras al día muchos se debaten en lograr encontrar la comida para el día, ellos están promoviendo la anulación de la Consulta Previa para poder sin recato alguno realizar operaciones extractivas, porque consultar y buscar el consentimiento de las comunidades negras, indígenas, y campesinas en realidad es un obstáculo para ese "progreso" de unos contra todos.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Los muertos de hoy, los infectados de estas semanas, somos nosotros. Somos los cómplices de la peste, del país proyectado por una clase dirigente de egoísmos, de prejuicios, de arribismos, de mezquindad, de costumbres excluyentes, de inveterados falsos clasismos, de estéticas narcas, de maniqueísmo creyentes, de destrucción de las aguas, los bosques, los animales, los páramos a nombre del “desarrollo”, de ese desarrollo que es muerte. Sí, muerte en vida.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":4} -->

#### Ante esa muerte que respira, que consume, que acumula se está abriendo un salto cuántico.

<!-- /wp:heading -->

<!-- wp:paragraph -->

En el síntoma de la peste más profunda está la posibilidad de crecimiento de lo nuevo, de otro porvenir distinto a la feliz esclavitud, al sometimiento de todas las fuentes de vida para deleitar el consumismo.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Esta naciendo en comunidades de base que están más allá de la hambruna, en sectores medios que en la espera solidaria están respirando lo nuevo, que exigen del Estado sin esperar de ese Estado nada y que ejercen sus derechos en la creatividad de sus propias fuerzas o recursos. En sectores medio altos, y uno que otro empresario que comprende que ya estamos en un límite que solo se resuelve por un cambio de paradigma en solidaridad con los que ya ejercen la solidaridad..

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El salto cuántico de poderes conscientes, ciudadanos que están reconociendo el miedo hacia la libertad en la reinvención de la política como el arte de un bello existir. Ese salto cuántico que nace desde dentro y se expresa en la movilidad embriagante del amor a través de redes y de un agenciamiento transformante en una apuesta política ambiental y anti patriarcal, ante la muerte en vida es la respuesta que se abre paso entre el COVID-19.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Terminado el miedo a esa muerte extraña y dolorosa se abrirá paso a los estados de excepción, al autoritarismo y las formas de control, y al salto cuántico transformador.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Ver mas: [Nuestros columnistas](https://archivo.contagioradio.com/opinion/columnistas/)

<!-- /wp:paragraph -->
