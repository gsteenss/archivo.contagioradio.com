Title: Aumentan denuncias por abuso de autoridad en implementación del Código de Policía
Date: 2018-01-24 14:51
Category: DDHH, Nacional
Tags: abuso de autoridad, Bogotá, código de policía, policías
Slug: aumentan-denuncias-por-abuso-de-autoridad-en-bogota
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/08/policia-represion.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Youtube] 

###### [24 Ene 2017] 

De acuerdo con la Personería de Bogotá, en el último semestre **la entidad abrió 100 investigaciones** por exceso de uso de la fuerza y abuso de autoridad en diferentes localidades, esto tras la entrada en vigencia del código de policía. En la Procuraduría General de la Nación hay 150 casos más que están en investigación.

El representante a la Cámara por Bogotá, Alirio Uribe, anunció que el 31 de enero se llevará a cabo un debate en el Congreso **sobre el tema de los impactos del código de Policía** teniendo en cuenta las denuncias de la ciudadanía por abuso de autoridad de la Fuerza Pública.

### **“El código de Policía da facultades exorbitantes a la Fuerza Pública”** 

Uribe indicó que **“el código no tiene controles** ni medidas hacia la corrupción de los Policías y hacia los potenciales abusos que se cometen”, dijo que es evidente la forma en que la Policía “maltrata a los vendedores ambulantes y los habitantes de calle”.  Además, reiteró que la ciudadanía se ha quejado por la cantidad de multas que existen y esto está siendo utilizado por parte de la Fuerza Pública para extorsionar.

Sin embargo, enfatizó en que **no ha habido una pedagogía** que le brinde a la ciudadanía las herramientas suficientes para que conozcan los derechos y las normas. Esto se debe hacer para que se respete a la Policía “pero también para que haga valer sus derechos”. (Le puede interesar: ["¿Qué hacer si considera injusta alguna sanción o procedimiento del código de Policía"](https://archivo.contagioradio.com/que-hacer-si-considera-injusta-alguna-sancion-o-procedimiento-del-codigo-de-policia/))

### **En Bogotá se está viviendo una crisis** 

Teniendo en cuenta las investigaciones de la Personería, Uribe manifestó que esto es un reflejo de que **en Bogotá hay muchos problemas** que no se han solucionado y que por el contrario tienden a aumentar. “Hay problemas de seguridad, se ha aumentado el hurto de bicicletas, hay una reducción de programas sociales y esto nos pone de presente la necesidad de generar un monitoreo ciudadano a la aplicación del código de Policía”.

Adicional a esto, los problemas de abuso de autoridad se dan porque **“los Policías están mal formados** y consideran que como son autoridad pueden hacer lo que les venga en gana y la ciudadanía tiene que entender que un funcionario público debe hacer lo que dice la ley”. Sin embargo, “también es un problema de la ciudadanía que no conoce sus derechos y las normas de Policía”. (Le puede interesar: ["Las 26 demandas al Código de Policía que debería resolver la Corte Constitucional"](https://archivo.contagioradio.com/las-26-demandas-sobre-el-codigo-de-policia-que-deberia-resolver-la-corte-constitucional/))

### **Policía debe estar al servicio del ciudadano como servidor público** 

El representante recordó que es necesario que la Policía **cumpla su deber de proteger a los ciudadanos** en vez de agredirlos. Esto está enmarcado dentro de la filosofía del Código de Policía que incluye el garantizar los derechos y las libertades de todos los ciudadanos. Sin embargo, “hay ciudadanos que irrespetan la convivencia y se hace necesario que la Policía intervenga”.

Para que no se sigan presentando este tipo de problemas, Uribe resaltó la importancia de **la cultura ciudadana**. Dijo que “hay gente que se queja de que en Bogotá es la ciudad que tiene menos Policías en el país de acuerdo al número de habitantes y eso es un indicador positivo de que Bogotá puede ser una mejor ciudad, de que la gente se puede portar bien sin tener a un Policía detrás”.

<iframe id="audio_23344388" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_23344388_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 

<div class="osd-sms-wrapper">

</div>
