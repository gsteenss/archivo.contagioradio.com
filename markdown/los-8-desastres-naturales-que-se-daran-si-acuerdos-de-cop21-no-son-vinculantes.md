Title: Los 8 desastres naturales que se darán si acuerdos de COP21 no son vinculantes
Date: 2015-12-01 14:52
Category: Ambiente, Otra Mirada
Tags: Calentamiento global, cambio climatico, Clima, COP 21, Medioambiente, ONU
Slug: los-8-desastres-naturales-que-se-daran-si-acuerdos-de-cop21-no-son-vinculantes
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/12/osos-polares-peligro-contagioradio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Expoknews 

###### [1 Dic 2015] 

El cambio climático ha sido el tema principal en la agenda de los medios durante esta semana, debido a la COP 21 que se celebra en Francia con el propósito de crear políticas y acuerdos que permitan mitigar el deterioro del ambiente. Aunque muchos cuestionan la celebración y verdadero compromiso de los gobiernos en el evento, es importante tener en cuenta, cuáles son las repercusiones que el calentamiento global tiene sobre el planeta y que serán evidentes y pasará  si los gobiernos no se comprometen  con acuerdos vinculantes.

Estos son 8 de ellos.

**Deshielo de los polos.**

\[caption id="attachment\_18071" align="aligncenter" width="600"\][![Foto: El País](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/12/1.jpg){.wp-image-18071 width="600" height="399"}](https://archivo.contagioradio.com/los-8-desastres-naturales-que-se-daran-si-acuerdos-de-cop21-no-son-vinculantes/1-10/) Foto: El País\[/caption\]

La extensión de los casquetes polares y del territorio de hielo en el Ártico y la Antártida se han reducido considerbalemente desde los últimos 100 años, debido a las altas temperaturas que se registran. Las zonas gélidas disminuyen cada vez más como consecuencia de este fenómeno. De acuerdo A La Agencia Espacial Europea, entre 1991 y 2011, la capa de hielo de los lagos, se redujo un 22%. Además, es preocupante que desde 1979 hasta el 2014, la presencia de hielo marino en el mundo ha bajado en un 40%.

**Aumento del nivel del mar.**

\[caption id="attachment\_18072" align="aligncenter" width="533"\][![Foto: National Geographic](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/12/2486.600x450.jpg){.wp-image-18072 width="533" height="400"}](https://archivo.contagioradio.com/los-8-desastres-naturales-que-se-daran-si-acuerdos-de-cop21-no-son-vinculantes/2486-600x450/) Foto: National Geographic\[/caption\]

Cuando el hielo que llega de los polos y las zonas más frías del mundo se derrite, se dirije hacia el mar. Esa gran cantidad de agua hace que su nivel crezca. También, el aumento de temperaturas de las aguas oceánicas hace que su densidad sea menor, por lo que un aumento de la temperatura, aceleraría el aumento del nivel del mar. Según el Instituto Americano de Ciencias Biológicas, en los últimos 3000 años, el nivel del mar ha aumentado de 0.1 a 0.2 milímetros por año. Sin embargo, en el siglo XX, la tase fue de 1 a 2 milímetros cada 12 meses.

**Temperaturas más altas**.

\[caption id="attachment\_18073" align="aligncenter" width="600"\][![Foto: alaluzpublica](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/12/Altas-temperaturas-600x399.jpg){.wp-image-18073 .size-large width="600" height="399"}](https://archivo.contagioradio.com/los-8-desastres-naturales-que-se-daran-si-acuerdos-de-cop21-no-son-vinculantes/altas-temperaturas/) Foto: alaluzpublica\[/caption\]

El calentamiento y aumento de las temperaturas se aceleraría. En la época actual, se han visto sus consecuencias parciales que se traducen en olas de calor que se viven alrededor del mundo. Recientemente en el 2013, la India y gran parte de Europa se vieron afectadas. La ONU afirma que el coeficiente de aumento de la temperatura media de la superficie de la Tierra en los últimos 50 años prácticamente duplicó el de los últimos 100 años. Desde 1990 hasta la actualidad, se registraron los d{ias más calurosos, mientras que el hielo del océano ártico disminuye un 2,7% cada diez años.

**Suelos desérticos**.

\[caption id="attachment\_18074" align="aligncenter" width="520"\][![Foto: masivaecologica](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/12/9b94fe_Dia_mundial_en_contra_de_la_desertificacion_y_sequia-1.jpg){.wp-image-18074 width="520" height="416"}](https://archivo.contagioradio.com/los-8-desastres-naturales-que-se-daran-si-acuerdos-de-cop21-no-son-vinculantes/9b94fe_dia_mundial_en_contra_de_la_desertificacion_y_sequia-1/) Foto: masivaecologica\[/caption\]

Las transformaciones climáticas hacen que los suelos requieran un gran abastecimiento de nutrientes que no van a poderse suplir; por este motivo, las zonas productivas del planeta y los suelos fértiles pasarían por un proceso de desertificación, en donde la destrucción de la capa vegetal, la erosión y la falta de agua, harán que se pierdan estos territorios. De acuerdo al CRICYT, "la desertificación aumenta el albedo de la superficie terrestre y disminuye la tasa actual de evapotranspiración, modificando el equilibrio energético en la superficie y la temperatura del aire y añade polvo y dióxido de carbono (CO2) a la atmósfera".

**Extinción de especies**.

\[caption id="attachment\_18075" align="aligncenter" width="600"\][![Foto: noticiasvenezuela](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/12/todo-terreno-cambio-climatico.jpg){.wp-image-18075 .size-full width="600" height="400"}](https://archivo.contagioradio.com/los-8-desastres-naturales-que-se-daran-si-acuerdos-de-cop21-no-son-vinculantes/todo-terreno-cambio-climatico/) Foto: noticiasvenezuela\[/caption\]

El cambio climático será uno de los motores para la extinción de especies en todo el mundo, puesto que la transformación brusca de los ecosistemas hace que los animales no tengan el tiempo suficiente para adaptarse a los nuevos cambios del ambiente, ni para migrar a otras zonas. La Organización de las Naciones Unidas para la Alimentación y la Agricultura (FAO), asegura que Se calcula que entre 20 y 30 por ciento de las especies de plantas y animales enfrentarán un mayor riesgo de extinción debido al calentamiento global", además, una parte significativa de las especies endémicas se habrán extinguido en el 2050.

**Enfermedades**.

\[caption id="attachment\_18076" align="aligncenter" width="600"\][![Foto: elcalentamientodenuestroplaneta](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/12/calentamiento-global18.jpg){.wp-image-18076 width="600" height="400"}](https://archivo.contagioradio.com/los-8-desastres-naturales-que-se-daran-si-acuerdos-de-cop21-no-son-vinculantes/calentamiento-global18/) Foto: elcalentamientodenuestroplaneta\[/caption\]

La disminución de la calidad de las condiciones medioambientales y la cantidad de los recursos naturales, tendrá repercusión en la salud de las personas. Según la Organización Mundial de la Salud (OMS), entre 2030 y 2050, "el cambio climático causará unas 250.000 defunciones adicionales cada año, debido a la malnutrición, el paludismo, la diarrea y el estrés calórico". En ese orden de ideas, el costo de este problema para el sistema de salud oscilará entre los 2 mil y 4 mil millones de dólares.

**Incendios forestales**.

\[caption id="attachment\_18077" align="aligncenter" width="600"\][![Foto: guardabosqueusb](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/12/000_arp3638245-600x406.jpg){.wp-image-18077 .size-large width="600" height="406"}](https://archivo.contagioradio.com/los-8-desastres-naturales-que-se-daran-si-acuerdos-de-cop21-no-son-vinculantes/spain-fire/) Foto: guardabosqueusb\[/caption\]

Las olas de calor, el aumento de la desertización en las zonas verdes del planeta y los extremos cambios de temperatura hacen que ocurran incendios en las zonas boscosas, que se propagan gracias a la vegetación de esos lugares. El cambio climático incrementa su capacidad de alcance, debido a que permite que se den las condiciones para que ocurra una catástrofe ambiental. De acuerdo a Grrenpeace España, "el cambio climático no es una causa de incendio, sin embargo sí explica los cambios que se están produciendo en los “nuevos” incendios, empeorando sus condiciones de inicio y de propagación".

**Escasez de alimentos y agua potable**.

\[caption id="attachment\_18078" align="aligncenter" width="600"\][![Foto: corcobi](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/12/180608_sequia-600x400.jpg){.wp-image-18078 .size-large width="600" height="400"}](https://archivo.contagioradio.com/los-8-desastres-naturales-que-se-daran-si-acuerdos-de-cop21-no-son-vinculantes/180608_sequia/) Foto: corcobi\[/caption\]

A consecuencia del cambio climático, el acceso al agua potable será más limitado, lo que implicará que en principio, la calidad de los alimentos no sea la ideal. Al mismo tiempo, los suelos no tendrían los nutrientes necesarios para las cosechas. De acuerdo a un documento de la FAO, en todo el mundo, la agricultura representa un 70 por ciento del agua que se extrae. Pero la demanda de las zonas urbanas, aumenta la presión sobre la calidad y la cantidad de los recursos hídricos locales. El organismo afirma que "Para 2025, 1 800 millones de personas vivirán en países o en regiones donde habrá escasez absoluta de agua".
