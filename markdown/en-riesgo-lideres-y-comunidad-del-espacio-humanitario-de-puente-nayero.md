Title: En riesgo líderes y comunidad del Espacio Humanitario de Puente Nayero
Date: 2020-09-24 16:12
Author: AdminContagio
Category: Actualidad
Tags: Espacio Humanitario, Líderes Sociales, Paramilitarismo, puente nayero
Slug: en-riesgo-lideres-y-comunidad-del-espacio-humanitario-de-puente-nayero
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/04/puente-Nayero-e1491860371260.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

Este 24 de septiembre la Comisión de Justicia y Paz denunció **[el riesgo](https://www.justiciaypazcolombia.com/atentado-contra-habitante-del-espacio-humanitario-puente-nayero/) en el que se encuentran los líderes y la comunidad del espacio humanitario de Puente Nayero**, en Buenaventura, Valle del Cauca, luego del intento de homicidio que se presentó en el espacio humanitario a pesar de que en ambos extremos de la calle hay presencia policial.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/Justiciaypazcol/status/1308845712000593920","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/Justiciaypazcol/status/1308845712000593920

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

Según la organización de Derechos Humanos, ***"el control social y económico a través de la extorsión, cobro de vacunas y robos por estructuras armadas heredadas del paramilitarismo"***, que hacen presencia en las distintas comunas de [Buenaventura](https://archivo.contagioradio.com/atentado-contra-alcaldia-de-buenaventura-no-es-una-casualidad/) evidencia aún más el riesgo en el que se encuentran las comunidades y especialmente los lugares que se han señalado como vulnerables, como es el caso de Puente Nayero.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Al mismo tiempo denunciaron que el pasado 17 de septiembre a las 6:30 pm en la calle San Francisco del barrio La Playita, **Crispiano Ángulo, fue atacado con arma de fuego por hombres armados que le dispararon en el abdomen.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

**El hecho se dio mientras se desarrollaban actividades lideradas por los defensores de Derechos Humanos integrantes de la Comisión,** en dónde trabajaban junto con la comunidad temas de producción agrícola sostenible con mujeres y un taller de danza para los niños.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Crispiano vivía junto con su familia en el Espacio Humanitario en donde administraba una tienda. ***"Hasta hoy Crispiano continua recibiendo atención médica"***, indicó la ONG, al mismo tiempo destacó que este hecho violento se dio a pocos metros de las unidades policiales, *"las cuales no reaccionaron mientras los hombres armados caminaban hacia su víctima"* .

<!-- /wp:paragraph -->

<!-- wp:heading -->

La comunidad de Puente Nayero sigue desprotegida
------------------------------------------------

<!-- /wp:heading -->

<!-- wp:paragraph -->

La organización señala que a pesar de que los habitantes han realizado en el marco del seguimiento y concertación de las medidas cautelares otorgadas por la Comisión Interamericana de Derechos Humanos, exigencias para que se respete su vida, **aún persiste la vulnerabilidad y el riesgo de las comunidades que viven en este Espacio Humanitario**.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

  
Un llamado al que se sumaron el pasado 28 de agosto, 120 comunidades qué [exigieron al Gobierno garantías](https://archivo.contagioradio.com/tribunal-ordena-a-duque-responder-a-comunidades-que-exigen-cese-al-fuego-en-sus-territorios/) en los territorios y un cese al fuego, así como el **amparo de sus derechos fundamentales a la salud, la vida y la paz**.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

**Exigencia que fue respaldada por el Tribunal Administrativo de Cundinamarca,** quien resolvió en un plazo de 5 días que el presidente Iván Duque debía dar respuesta a dichas comunidades.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Sin embargo pese a esto, voceros de las comunidades reconocieron qué esto significó un pequeño logro, y más aún cuando **la respuesta del Gobierno no fue satisfactoria, señalando que está no cumplía con el requisito de ser suficiente, efectiva y congruente**, y que además no solucionaba de fondo los riesgos y necesidades que atravesaban las comunidades.

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->
