Title: Desde Cacarica llegó la Universidad de Paz a la Filbo
Date: 2019-04-27 18:08
Author: CtgAdm
Category: Comunidad, Cultura
Tags: cacarica, Chocó, feria del libro
Slug: desde-cacarica-llego-la-universidad-de-paz-a-la-filbo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/Cacarica-e1556406632890.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo] 

**Ledys Orejuela y Henry Ángulo** líderes sociales de Cacarica, Chocó, en compañía del director de la Comisión de Justicia y Paz, Danilo Rueda compartieron con los asistentes a la Feria Internacional del Libro de Bogotá cómo avanza  la Universidad de Paz,, una iniciativa inaugurada en esta región del país, buscando que los actores armados que atentaron contra su población durante la operación Genesis en febrero de 1997, reparen los daños  a través  de la la educación.

**"Queremos que sea un espacio acogedor, que exista un espacio de cultura, que haya un espacio de memoria y donde todos nos podamos incluir",** afirma Ledys Orejuela quien destaca la importancia de perdonar durante este proceso.  [(Lea también  Lanzan propuesta de Universidades de Paz) ](https://archivo.contagioradio.com/lanzan-propuesta-de-universidades-de-paz/)

### **La educación, esencial para el ser humano** 

Henry, integrante de las Comunidades de Autodeterminación, Vida y Dignidad del Cacarica (CAVIDA)  agrega que la propuesta de la Universidad de Paz, es un sueño que permitirá a las 25 comunidades de Cacarica -  incluidas las zonas humanitaria de Nueva Esperanza y Nueva Vida -  ** impulsar la educación, un derecho que consideran, "es el desarrollo fundamental del ser humano".**

"Nosotros lo hemos dicho siempre, el sueño de ese país nuevo, de esa mujer y hombres nuevos los construimos a raíz de la verdad, la justicia restaurativa, lo construimos dejando ese odio, dejando esa ira hacia el otro que ayer me hizo daño y que hoy sea perdonado, nosotros soñamos con un país donde todos y todas nos podamos encontrar abrazar y en el que fluya la solidaridad"  afirma Henry.

[English Version](https://archivo.contagioradio.com/international-book-fair-2019-from-cacarica-came-the-university-for-peace/)

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
