Title: Cinco claves para entender el informe de Michel Forst sobre la situación de DD.HH. en Colombia
Date: 2020-03-04 13:44
Author: CtgAdm
Category: Actualidad, DDHH
Tags: Asesinato de líderes comunales, asesinato defensores de derechos humanos, Empresas Bananeras, Michel Forst, ONU
Slug: cinco-claves-para-entender-el-informe-de-michel-forst-sobre-la-situacion-de-dd-hh-en-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/03/FORST.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: Michel Forst/ Contagio Radio

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

[Michel Forst,](https://twitter.com/ForstMichel) relator especial sobre la situación de los defensores de los derechos humanos de la ONU. dio a conocer el informe que constata el riesgo que viven líderes y lideresas sociales frente a los intereses de grupos criminales, grupos armados e ilegales, y a intereses de actores estatales y empresarios en un contexto de altos índices de impunidad que convierte a Colombia en el país con mayor número de personas defensoras asesinadas de América Latina. [(Le puede interesar: Iván Duque visita Apartadó y a tan solo kilómetros es asesinado el líder Amado Torres)](https://archivo.contagioradio.com/ivan-duque-visita-apartado-y-a-tan-solo-kilometros-es-asesinado-el-lider-amado-torres/)

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Son constantes asesinatos y otras violaciones a líderes y defensores de DD.HH.

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

El informe de Forst asegura que hasta el 31 de junio de 2019 fueron asesinados 486 líderes sociales o defensores de DD.HH según datos de la Defeesoría del Pueblo, sin embargo resalta también que la Oficina de ACNUDH en el mismo periodo se reportaron 324 asesinatos y el Programa Somos Defensores reportó 400.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Ante la diferencia de las cifras el relator aclaró que ello responde a diferentes metodología en la documentación de los casos, pero llamó la atención sobre la discusión entorno a esas diferencias **y lamentó que esa debate desvíe la atención y los esfuerzos de "las cuestiones claves para lograr un ambiente seguro y propicio para la defensa de derechos humanos en Colombia"**. Además destacó que el indice de asesinatos es muy elevado y esta situación se presenta por lo menos en 47 municipios del país. [(Lea también: Difamación contra defensores de DD.HH no será tolerada: Michel Forst)](https://archivo.contagioradio.com/difamacion-contra-defensores-de-dd-hh-no-sera-tolerada-michel-forst/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Otro de los puntos señalados es que indígenas, afrodescendientes o campesinos "siguen siendo los más castigados por la defensa de los DD.HH". y subrayó que en particular las víctimas del Programa Nacional Integral de Sustitución de cultivos de uso ilícito (PNIS) siguen siendo un blanco crucial de las acciones violentas.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### La impunidad es una invitación para seguir violentando afirma la ONU

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

En su informe, el relator especial de la ONU señala que la impunidad en casos de asesinatos a líderes y defensores de DD.HH supera el 89%, lo que sugiere un alto grado de preocupación pues este alto índice envía un mensaje de ausencia de reconocimiento de la labor en la sociedad de quienes son víctimas "y ello implica una invitación para seguir violentando sus derechos".

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Además este porcentaje de impunidad, obedece a los casos asumidos por la Fiscalía que son los reportados por el ACNUDH, es decir que solo se ha asumido una investigación en 302 casos, dejando por fuera más de 100 que son los documentados por otras organizaciones.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Estigmatización y criminalización: un ambiente poco seguro para defensores y líderes

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Según el informe, dirigentes políticos, funcionarios públicos, personas influyentes y líderes del sector privado estigmatizan como guerrilleros, terroristas anti desarrollo o informantes y señala concretamente el caso de Luis Pérez Gutiérrez, gobernador de Antioquia en 2018, en contra del Paro Minero en Segovia y Remedios y el del ministro de Defensa, Luis Carlos Villegas quien atribuyó la violencia contra líderes a "línderos, líos de faldas y rentas ilícitas", así mismo aseguró que en la protesta pública las mafias y el crimen organizado.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Otro de los problemas que agrava esta crítica situación es que en al menos 70 casos "se acusa, judicializa y detiene a defensoras y defensores" quienes son acusados de pertenecer a grupos ilegales. Sumado a ello, la criminalización de la defensa de la tierra y el medio ambiente puesto que al menos 202 defensores dicha causa han sido judicializados desde el 2012. Señala como ejemplo especial el de los ocho líderes de San Luis de Palenque quienes lideraron las protestas contra la empresa Frontera Energy que tiene, s**egún el relator una aparente conexión con la Brigada XVI del Ejercito expresada entre otras cosas con un convenio por 1.343.106 dólares para el que el Ejército proteja su actividad.**

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Forst también hizo énfasis en los casos de criminalización en el marco de la protesta social ligada a "detenciones arbitrarias y uso excesivo de la fuerza por parte de la Fuerza Pública", en movilizaciones del sector educativo o la Minga Indígena en la que hubo al menos 104 detenciones y 12 judicializaciones. Adicionalmente el relator especial expresó su preocupación por el proyecto de ley 281 del 2018 que contribuiría a la criminalización de la protesta social. [(Le puede interesar: Más medidas integrales y menos accionar policial y militar: ONU)](https://archivo.contagioradio.com/onu-mas-medidas-integrales-menos-accionar-policial-militar/)

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Defensores rurales, étnicos, ambientales y mujeres: los grandes blancos: Michel Forst

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

En una parte especial del informe, asegura que **por lo menos 167 líderes indígenas han sido asesinados desde la firma del Acuerdo** y cita un informe de la Comisión Colombiana de Juristas (CCJ) en el que se señala que el 23% de los asesinatos son dignatarios de Juntas de Acción Comunal, 20 líderes y lideresas comunales, 20% líderes étnicos, 5% defensores de las víctimas, 11% líderes y lideresas campesinos, 2% reclamantes de tierras y 2% abogados defensores de DD.HH.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Respecto a las mujeres, asegura que desde la firma del Acuerdo, el número de defensoras asesinadas ha ido en aumento y que estos casos representan cerca del 20% del total de los asesinatos e hizo un especial énfasis en la cultura patriarcal y la brecha en el disfrute de derechos humanos entre hombres y mujeres, sobre todo en las zonas rurales.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Así mismo aseguró el relator de la ONU que hay un aumento de las amenazas contra personas LGBTI y un índice bajo en la apertura de investigaciones en la Fiscalía General de la Nación por estas denuncias. Por último la situación de periodistas, estudiantes y sindicalistas también es preocupante, dado que hasta el 4 de octubre de 2019 se registraron 329 violaciones a la libertad de prensa, 79 de ellas contra mujeres, periodistas, 104 amenazas, 2 asesinatos y 43 acciones de judicialización.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Empresas públicas y privadas siguen aportando a la crisis de DD.HH.

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

El informe asegura que entre 2015 y 2018 se registran 115 incidentes relacionados con empresas que incluyen intimidación, estigmatización, criminalización, desplazamiento forzado y hasta asesinatos en zonas de actividad empresarial. El 30% de los ataques en zonas mineras, 28.5% agronegocios de aceite de palma, banano, caña de azúcar.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

A través de su informe Forst resaltó la importancia de la implementación efectiva del Acuerdo de Paz y la elaboración de una política integral para la defensa de los derechos humanos y el funcionamiento efectivo de la Comisión Nacional de Garantías de Seguridad que a su vez permita el desmantelamiento de organizaciones criminales y paramilitares.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Las recomendaciones de Forst incluyen otorgar autonomía al Sistema de Alerta Temprana de la Defensoría del Pueblo y una correspondiente respuesta articulada a la misma, **además de un plan de desarrollo nacional, de reforma agraria y restitución de tierras que incluya un enfoque étnico, de DD.HH. que esté alineado con los Objetivos de Desarrollo Sostenible de las Naciones Unidas.**

<!-- /wp:paragraph -->
