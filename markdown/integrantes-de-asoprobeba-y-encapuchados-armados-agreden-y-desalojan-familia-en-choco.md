Title: Integrantes de ASOPROBEBA y encapuchados armados agreden y desalojan familia en Chocó
Date: 2015-03-19 17:30
Author: CtgAdm
Category: DDHH, Nacional
Tags: ASOPROBEBA, Curvarado, Desalojo, Paramilitarismo
Slug: integrantes-de-asoprobeba-y-encapuchados-armados-agreden-y-desalojan-familia-en-choco
Status: published

###### Foto de archivo Justicia y Paz 

<iframe src="http://www.ivoox.com/player_ek_4237459_2_1.html?data=lZegmZmZfY6ZmKiak5qJd6KnlZKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRjc%2Foxszfw9PYqdSfxcqYo7izlLPDo6qvo5Ddb8bixMbd18jMpcXj1JDO1NLFqNDnjMbU1MrIqY6ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Víctima del desalojo] 

Según la denuncia de una de las habitantes del caserío Santa María en el consejo comunitario de **Pedeguita y Mansilla,** en el Bajo Atrato chocoano, hacia las 5:00 de la tarde de este 18 de Marzo, integrantes de la organización **ASOPROBEBA** junto con un grupo de personas encapuchadas y armadas con machetes y armas de fuego, irrumpieron en el caserío disparando, amenazando y torturando a 6 personas, entre ellos un menor de edad.

Cabe recordar que ASOPROBEBA es una de las organizaciones creadas por **Sor Teresa Gómez**, para apropiarse ilegalmente de tierras en el territorio colectivo Curvaradó.

Mientras que las personas de la Junta Directiva de ASOPROBEBA, llegaron con sus rostros descubiertos el grupo de hombres armados llevaba capuchas, gafas oscuras y gorras para evitar ser identificados. Una de las personas desalojadas identificó a varios de los integrantes de la organización mencionada y los denunció ante las autoridades.

Cuando las personas llegaron al caserío Santa María llegaron disparando, apresaron a 3 hombres que se encontraban trabajando en la finca de una de ellas, los amarraron, los golpearon y los agredieron con armas corto punzantes, en varias partes del cuerpo, al mismo tiempo disparaban hacia tres personas más.

[Foto de las personas heridas](https://archivo.contagioradio.com/ddhh/integrantes-de-asoprobeba-y-encapuchados-armados-agreden-y-desalojan-familia-en-choco/attachment/heridos/): ( imágenes fuertes)

Luego de las torturas, las personas armadas y los integrantes de ASOPROBEBA procedieron a quemar y destruir las casas, a subir a un camión a todos los propietarios del predio, camión que más adelante fue interceptado por la policía frente a quién se hizo la denuncia sin que hasta el momento se conozca las acciones que toma esa autoridad.

Dos de las personas heridas se encuentran siendo atendidas por personal médico y una vez se recuperen tienen la firme intensión de **regresar a sus tierras,** por ello exigen la solidaridad urgente y las garantías de seguridad por parte de las autoridades.

Este ataque se da 13 días después de que un juez de restitución de tierras negara una tutela a favor de los habitantes de Santa María, quienes fueron víctimas de desplazamiento forzado en 1997 y regresaron hace cerca de dos años en medio de las persistentes amenazas por parte de empresarios y paramilitares.
