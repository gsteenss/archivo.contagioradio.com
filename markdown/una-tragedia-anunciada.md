Title: ¡UNA TRAGEDIA ANUNCIADA!
Date: 2020-08-13 18:41
Author: CtgAdm
Category: yoreporto
Tags: Ejercito Nacional, Fuerza Pública, policia nacional
Slug: una-tragedia-anunciada
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/02/cárcel.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right"} -->

*Autor: [Fundación Comité de Reconciliación ONG](http://fundacionparalareconciliacion.org/)*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Reza el adagio popular: ***“No hay cuña que más apriete que la del mismo palo”***, refrán que expresa que, ninguno es peor enemigo que el que ha sido su amigo, o de la misma institución, oficio o familia. Etc.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

**Respetados Generales:**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El pasado 23 de abril, esta ONG en cumplimiento de sus funciones en defensa de los Derechos Humanos, l**os alertó PUBLICAMENTE de lo que era evidente por mera lógica, y que en la madrugada de hoy cobró la vida del SP. ® LUIS OCHOA MARTÍNEZ,** suboficial de la Policía Nacional que se encontraba recluido de manera irregular en una cárcel civil, específicamente en el patio ERE-1 de La Picota.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

La terquedad del BG. Norberto Mojica (director del INPEC) de perseverar en el error, hoy se traduce en el lamentablemente deceso del suboficial, dejando huérfana a una familia de la institución policial. **¿Quién va a responder por la tragedia de la muerte del Sargento?**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En general todas las cárceles colombianas están atiborradas de reclusos, con el hacinamiento más alto de la historia del país y en medio de una pandemia incontrolable, mientras que los 15 centros [carcelarios](https://archivo.contagioradio.com/el-sistema-penitenciario-de-colombia-una-violacion-a-dd-hh/) militares y policiales que existen en el país están a menos del 30% de su capacidad instalada. A diario se contagian centenares de reclusos en todos los establecimientos penitenciarios porque un espacio de 2 x 2 metros es compartido por varios hombres. **¿Qué está esperando el director del INPEC para trasladar a los militares y policías a las cárceles que les corresponde?**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El BG. Mojica además de testarudo y desleal con sus compañeros de armas, es irresponsable y una tragedia , pues a los militares y policías que purgan pena en los patios ERE-1 y ERE-2 de **La Picota que están enfermos por COVID-19 o que tienen sintomatología relacionada, el INPEC** les entregó pastillas de MORINGA como sustancia curativa milagrosa (lo cual no aprueba el Ministerio de Salud); de igual forma el laboratorio COLCAN contratado por FIDUPREVISORA, realizó algunas pruebas moleculares PCR, pero se negó a hacerlas a militares y policías enfermos porque estos dependen de sistemas de salud de las Fuerzas Armadas. Enfermos, esperan el fatal destino en una mazmorra.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

**¿Qué están esperando señores Generales, acaso que la tragedia se vuelva incontrolable?** Entendemos que el BG. Mojica se encuentra en comisión en el INPEC, ¿pero se les salió de las manos a ustedes? Es obvio que a estas alturas ya no puedan trasladarlos a los Centros Carcelarios Militares porque podrían multiplicarse los contagios en esos, pero si pueden sacarlos a Tolemaida o a Bello y crearles un pabellón especial con la atención correspondiente, de igual manera a la cárcel de Facatativá para miembros de la Policía Nacional, que prácticamente está desocupada.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Señores Generales, nunca olviden estas palabras: *“**El Covid-19 nos cogió calzones abajo a todos, por lo que las autoridades están muy ocupadas atajando la crisis;** lastimosamente se perdió la sensibilidad por la muerte, pero cuando todo esto pase, muy seguramente a ustedes - gozando del retiro -, esa justicia hoy ocupada, tendrá todo el tiempo para llamarlos a responder por las vidas que se perdieron en vuestras manos”.* No lo olviden.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Hacemos un llamado a todas las ONG defensoras de Derechos Humanos, Congreso de la República, Procuraduría y Defensoría del Pueblo, organizaciones sociales y **entidades responsables del tema, a que se pronuncien sobre esta TRAGEDIA ANUNCIADA.** También aprovechamos para hacer un llamado a todas las organizaciones de la Reserva Activa para que viren su mirada hacia la barbarie que están afrontando sus compañeros de armas hoy recluidos, dejen de preocuparse por el expresidente Uribe que, con toda seguridad, nada le sucederá en su celda de 1.400 hectáreas. 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

**Comandantes:** **si se mueren sus hombres en las cárceles civiles, no podrán decir a la historia que fue culpa de la “izquierda”**. Por ahora, urge que envíen sendas comisiones de sanidad, equipadas de medicamentos, elementos de bioseguridad y pruebas PCR para sus hombres recluidos en dichos patios de La Picota, obviamente, si el BG. Mojica les permite ingresar.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Con respetos,

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Mayor (r), **CÉSAR MALDONADO** |Presidente

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->

<!-- wp:paragraph {"textColor":"cyan-bluish-gray","fontSize":"small"} -->

***La sección Yo Reporto es un espacio de expresión libre en donde se refleja exclusivamente los puntos de vista de los autores y no compromete el pensamiento, la opinión, ni la linea editorial de Contagio Rad*io**

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
