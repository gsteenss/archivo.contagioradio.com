Title: Miguel Morales, sexto profesor asesinado en Cauca este año
Date: 2018-09-24 12:20
Author: AdminContagio
Category: DDHH, Nacional
Tags: ASOINCA, Cauca, lideres sociales, profesor
Slug: miguel-morales-profesor-asesinado
Status: published

###### [Foto: Contagio Radio] 

###### [24 Sep 2018] 

**Miguel Antonio Morales Calambas, miembro del pueblo Misak** y profesor en el municipio de **Morales, Cauca**, fue asesinado el viernes 21 de septiembre, según las autoridades, producto de un robo; sin embargo, desde la Asociación de Institutores y Trabajadores de la Educación del Cauca **(ASOINCA),** organización que Morales integraba, denuncian que su homicidio obedece a razones políticas.

**Tito Torres, integrante de ASOINCA,** indicó que Morales se desplazaba en compañía de su compañera por la vía que conduce a Suárez, cuando fue interceptado por hombres que, para la Policía, querían robar su motocicleta; no obstante, luego de asesinar a Morales, solo hurtaron unas pertenencias de la mujer mientras la moto y demás objetos de valor se encontraron en el lugar de los hechos. (Le puede interesar: ["32 líderes líderes indígenas han sido asesinados en el primer semestre de 2018"](https://archivo.contagioradio.com/32-lideres-indigenas-han-sido-asesinados-en-el-primer-semestre-del-2018/))

**6 docentes afiliados a ASOINCA han sido asesinados durante este año**

Para el docente, **en Cauca se está viendo la "arremetida del paramilitarismo",** mientras el Gobierno Nacional hace "oídos sordos" de los 6 profesores integrantes de la Asociación que han sido asesinados durante este año. Torres aseveró que las autoridades han pedido plazo para investigar tanto el crimen de Morales, como el de Hollman Mamian, Efrén Zúñiga y los otros 3 docentes, **pero hasta el momento no han tenido resultados. **

El hecho genera nuevas preocupaciones porque en el Departamento han aumentado el número de amenazas, así como de asesinatos a líderes sociales. De acuerdo con registros de la** Defensoría del Pueblo, Cauca es el departamento con mayor número de homicidios de estas personas**, seguido por Antioquia, Nariño y Chocó. (Le puede interesar: ["Fiscal General niega impunidad en asesinatos a líderes sociales"](https://archivo.contagioradio.com/fiscal-general-niega-impunidad-en-asesinatos-a-lideres-sociales/))

El director de ASOINCA sostuvo que en los próximos días se realizarán reuniones entre diferentes organizaciones sociales para realizar una **movilización del Suroccidente colombiano,** con el fin de reclamar por la defensa de las y los trabajadores del sector de la educación, los y las líderes así como los y las defensoras de Derechos Humanos del país. (Le puede interesar: ["El próximo 5 de Octubre 'pasarán' al tablero autoridades que protegen líderes: Iván Cepeda"](https://archivo.contagioradio.com/5-octubre-lideres-cepeda/))

<iframe id="audio_28822506" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_28822506_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en] [su correo](http://bit.ly/1nvAO4u) [o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por] [Contagio Radio](http://bit.ly/1ICYhVU). 
