Title: Desde las entrañas de la tierra y junto a los gritos del agua, Cajamarca dijo NO a la minería.
Date: 2017-03-31 14:15
Category: CENSAT, Opinion
Tags: Anglo Gold Ashanti, Cajamarca, Consulta minera, La Colosa
Slug: cajamarca-consulta-minera
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/03/cajamarca.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: CENSAT 

#### **[Por: CENSAT Agua Viva /Amigos de la Tierra Colombia](https://archivo.contagioradio.com/censat-agua-viva/)** 

###### 30 Mar 2017 

El día 26 de marzo asistimos a un hecho histórico que se convierte en un hito en la lucha de las comunidades populares colombianas frente a la gran minería del país. La contundente victoria de miles de voces venidas de las veredas y los rincones más recónditos de este municipio que durante años han resistido el embate de la gran minería de oro y han resguardado su vocación agrícola y campesina va más allá de un NO rotundo a la minería. Cajamarca ha dado una lección a Colombia acerca de la participación y la voluntad popular, participando ha mostrado que la paz se construye desde los territorios con ejercicios concretos y haciendo respetar su voluntad popular, ha desafiado la “dictadura minera” impuesta desde hace años en nuestro país, en donde para el sector y para los últimos gobiernos la gran minería lo justifica todo.

Las consultas populares han pasado por varias estaciones de discusión y disputa. Como todos los mecanismos de participación ciudadana están consignados en la Constitución desde 1991 pero repetidamente han sido reversados y restringidos como una manera de favorecer intereses particulares. En el caso de la minería, la participación en todos sus espectros ha sido reducida meramente a un trámite, ni siquiera las consultas previas para las comunidades étnicas han sido realizadas de buena fe y con las disposiciones requeridas. No obstante, las comunidades, en su autonomía han recuperado el valor de estos mecanismos de participación ciudadana como una forma legal y taxativa de exigir que su voluntad sea de obligatorio cumplimiento y además como un mecanismo de protección de la vocación del suelo y las formas tradicionales de habitar el territorio que son notablemente amenazadas por las actividades extractivas en el país.

Desconocer la importancia y los efectos vinculantes de las consultas populares, los cabildos abiertos, las iniciativas normativas y legislativas, entre otros en la política pública colombiana resulta anticonstitucional y antidemocrático además terriblemente desafortunado para el momento de post acuerdo que vivimos. No sólo porque desdibuja el derecho a participar, necesario para construir democracia sino porque además nos sigue hundiendo en el totalitarismo extractivo que no admite voces disidentes y que en complicidad con los gobiernos de turno, nos sigue imponiendo la muerte como el único camino a un desarrollo, que está lejos del significado de buen vivir que las comunidades rurales y populares defendemos.

Sin duda, la victoria de las y los habitantes de Cajamarca, que nace de las entrañas de la tierra, de la fuerza del volcán Machín y del grito de las aguas, es la victoria de todas las comunidades rurales y urbanas que durante años han advertido y sufrido en carne propia lo que significa la minería en sus vidas. No es necesario irse lejos para constatar que los argumentos de multinacionales como Anglo Gold Ashanti han sido repetidamente desmontados: la Guajira, el Cesar, Montelíbano son los ejemplos más cercanos que tenemos para anticipar lo que podría ser la Colosa y la sentencia de muerte que la gran minería lleva consigo.

Teniendo en perspectiva que los escenarios de relacionamiento entre el sector multinacional y las comunidades han sido extremamente desiguales, la consulta popular de Cajamarca nos muestra que más allá del dinero y el aparataje corrupto asociado al accionar de las empresas, el arraigo, el amor y la resistencia por el territorio prevalece en lo corazones de los campesinos y campesinas de nuestro país, quienes son los responsables de abastecer de alimento a las ciudades y además de ser los cuidadores del agua. Estos valores sin precio, han superado las artimañas mercantiles que las empresas han utilizado para comprar la dignidad y han mostrado que frente a la gran minería no existen puntos medios, ni “gana – gana” como fue expresado por la viceministra de minas y energías, sino una necesidad imperiosa de desmontar el modelo extractivo trasnacional que solo nos ha traído pérdidas y afectaciones irreparables e incompensables.

Por estas razones, nos sumamos a las exigencias de las comunidades de respetar a cabalidad y acatar la decisión del pueblo cajamarcuno de declarar su territorio libre de minería y del reconocimiento de los mecanismos de participación ciudadana dispuestos constitucionalmente.

El talante democrático de las campesinas y campesinos del municipio y del proceso del Comité ambiental en Defensa de la Vida del que hacen parte organizaciones de Cajamarca como COSAJUCA (Colectivo Socioambiental Juvenil de Cajamarca) , Conciencia Campesina, APACRA (Asociación de productores agroecológicos de Cajamarca), Asociación por la defensa de Chorros Blancos - Asocuencua Chorro Blancos, es una muestra de la autonomía y la soberanía de los pueblos y de la necesidad de una resistencia nacional por la de la defensa de las aguas.

Mientras aquí discutimos si es vinculante o no el resultado de la consulta, en El Salvador, dan un paso firme para avanzar hacia el posextractivismo avanzando en la prohibición de cualquier minería metálica. Le puede interesar: [El Salvador prohíbe la minería metálica](https://archivo.contagioradio.com/salvador-prohibe-la-mineria-metalica/).
