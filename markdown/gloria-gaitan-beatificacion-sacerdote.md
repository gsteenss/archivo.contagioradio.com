Title: La petición de Gloria Gaitán al Papa por beatificación de sacerdote
Date: 2017-07-08 18:55
Category: Nacional, Política
Tags: 9 de abril, Gloria Gaitán, iglesia católica
Slug: gloria-gaitan-beatificacion-sacerdote
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/07/gloria-e1499557340744.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: 

###### 8 Jul 2017 

Tras el anuncio por parte del Vaticano de su intención de beatificar a los sacerdotes colombianos Jesús Emilio Jaramillo Monsalve y Pedro María Ramírez Ramos, Gloria Gaitán, hija del dirigente liberal Jorge Eliécer Gaitán envió una carta al Nuncio apostólico del Papa Francisco cuestionando la decisión, particularmente en lo que a Ramírez se refiere.

De acuerdo con el decreto firmado por el vaticano, los dos sacerdotes pueden ser considerados "mártires" de la iglesia por haber haber sido asesinados por razones de fe, mismas que la hija de Gaitán cuestiona al recordar que en 2015 había solicitado a la Santa sede revisar una serie de informes que daban cuenta del accionar de algunos prelados de provincia contra su padre a quien "desde los púlpitos se le denigró y vilipendió calumniosamente, diciéndole a los fieles que era pecado votar por él”.

En su carta, Gloria Gaitán recuerda que al enterarse que Pedro María Ramírez fue linchado por una multitud en Armero, Tolima,  pocas horas después del asesinato de su padre, recordó las palabras de su madre cuando advertía al caudillo liberal que debía cuidarse de los curas de esa población, lo que a su consideración pondría la beatificación en tela de juicio, por no tratarse de alguien quien muriera por su fé católica sino por haber "denegado del mensaje de Cristo".

Gaitán, reitera su llamado al Papa Francisco a escuchar sus palabras, si es el propósito de la iglesia pedir perdón por su participación en los actos de violencia que generó el gobierno contra el pueblo gaitanista, destacando que su petición no es un gesto fácil "está a la altura de su valentía, demostrada en el empeño de su importante misión,tendrá un efecto crucial para el futuro de la paz en Colombia" (Le puede interesar: [Francisco pidió perdón por escándalos de la iglesia](https://archivo.contagioradio.com/papa-francisco-perdon/))

A continuación se encuentra el documento completo.

Bogotá, junio 8 de 2017  
Su Excelencia  
Ettore Balestrero  
Nuncio Apostólico de Su Santidad Papa Francisco  
Respetado Señor Nuncio,

Anoche me informé que el Papa Francisco beatificará al sacerdote Pedro María Ramírez, quien fue linchado por la multitud en Armero (Tolima) pocas horas después de que mi padre, Jorge Eliécer Gaitán, hubiera sido asesinado en el marco del genocidio al Movimiento Gaitanista que desató el gobierno del presidente de la época, Mariano Ospina Pérez.

Con esta beatificación se me volcó el recuerdo de mi madre que alertaba a mi padre para que se cuidara porque lo iban a asesinar, tal como venían haciendo con sus seguidores. Ella le repetía: “Cuídate de los curas de Armero”. Yo pensaba que la advertencia de mi madre era un refrán popular. Pero, cuando llegó la noticia del linchamiento, supe que se trataba de un sacerdote de carne y hueso.

¿Será posible que el sacerdote que invitaba al crimen sea Pedro María Ramírez? Porque del que yo tuve noticias en mi infancia no murió por su fe católica sino todo lo contrario. Fue lamentablemente linchado por haber denegado del mensaje de Cristo. ¿De ser la misma persona, será posible que esta beatificación sea una respuesta del Papa Francisco a mi solicitud del año 2015? En mi carta yo le decía:

“Mi padre, Jorge Eliécer Gaitán, también sufrió, en su condición de líder popular revolucionario, el embate de la Iglesia Católica. Me permito anexarle algunos informes de prensa sobre las actuaciones de algunos prelados de provincia, ya que desde los púlpitos se le denigró y vilipendió calumniosamente, diciéndole a los fieles que era pecado votar por él”.

“Si Su Santidad escucha mis palabras y si, así como la Iglesia ha pedido perdón por los delitos de los sacerdotes pedófilos, opta por pedir perdón a nombre de la Iglesia Católica por haber sido partícipe de los actos de violencia que generó el gobierno contra el pueblo gaitanista y antes de ello contra las huestes que seguían a su antecesor, el general Rafael Uribe Uribe, yo puedo hacerle llegar los documentos probatorios de lo dicho”.

“Estoy convencida que este gesto que le pido – que no es fácil, pero que está a la altura de su valentía demostrada en el desempeño de su importante misión – tendrá un efecto crucial para el futuro de la paz en Colombia”.

Espero que Su Excelencia aclare mis dudas para tranquilidad de mi espíritu y sujeción de la Iglesia Católica a la política de verdad, justicia y reparación. Con deferencia a su alta jerarquía, me suscribo de Ud. respetuosamente,

GLORIA GAITÁN JARAMILLO  
e-mail: gaitanjaramillogloria@yahoo.es
