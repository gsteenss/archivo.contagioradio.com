Title: “Para nosotros la justicia debe ser restauradora" Comunidades de Cacarica
Date: 2017-03-13 21:34
Category: DDHH, Nacional
Tags: cacarica, cavida, Chocó, Operacion genesis
Slug: asi-seria-condena-de-rito-alejo-del-rio-en-las-comunidades-victimas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/03/Soy-genesis-46.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio ] 

###### [13 Mar. 2017] 

Las comunidades de Cacarica, en el Chocó, dieron a conocer una sentida carta en la que hablan **e invitan a los perpetradores, a los planificadores y a los beneficiarios de la violencia a buscar “la libertad de la cárcel”**, y dicen que para ellos la justicia y la verdad va más allá de las rejas de la cárcel, razón por la que han propuesto otro tipo de sentencias, incluso como propuesta para ser pensada en la Jurisdicción Especial para la Paz. Le puede interesar: [Soy génesis, festival de la memoria en Cacarica - Chocó](https://archivo.contagioradio.com/soy-genesis-festival-de-la-memoria-en-cacarica-choco/)

“Para nosotros la justicia debe ser restauradora. Una propuesta que hemos venido haciendo desde hace 16 años, porque hemos visto que en Colombia el tema de la justicia ordinaria, de la cárcel, realmente no ha solucionado lo que nosotros queremos, que es saber la verdad” relata Jahaira Salazar, etnoeducadora e integrante de la Comunidad de Cacarica.

La propuesta que han trabajado desde las comunidades es que existan sanciones de tipo éticas, que permitan a las víctimas saber la verdad y a los victimarios, reconociendo que también son seres humanos, **regenerarse con la sociedad que oprimieron y violentaron, viviendo con ellas, siendo parte de sus días y resarciendo el daño causado.**

Esta carta fue dada a conocer **a propósito de la conmemoración de los 20 años de la Operación Génesis.** Luego de 20 años de haberse reconocido como parte de un tejido social, pero también como víctimas de esa violencia aterradora que llegó a sus territorios y en la que aseguran han sido **20 años de resistencia, de perdonar y de crear paz desde los territorios.**

En la misiva, las comunidades relatan cómo han sido estos 20 años de caminar el territorio y la memoria de los hechos, pero también de **recordar sin dolor y seguir exigiendo justicia y verdad en los crímenes contra las comunidades**. Le puede interesar: [In Memoriam Marino López a 20 años de la Operación Génesis](https://archivo.contagioradio.com/in-memoriam-marino-lopez-a-20-anos-de-la-operacion-genesis/)

Dice el documento que hace 20 años fueron “penetrados” por la violencia aterradora en el cuerpo, el alma, la mente, bajo el nombre bíblico de la creación, la llamada Operación Génesis y la Operación Cacarica, por la que “unos monstruos de hierro” que sobrevolaron sobre su territorio los obligaron a abandonar el territorio.

Jahaira, aseguró que la actividad que se llevó a cabo el pasado 27 de febrero en el que recordaron esa operación, **llenó sus espíritus de digna lucha y alegría.** Le puede interesar: [“Contra la Corriente de las Aguas Turbias, a 20 años de la operación Génesis”](https://archivo.contagioradio.com/la-corriente-las-aguas-turbias-20-anos-la-operacion-genesis/)

“Fue una caminata por la vida, fue una conmemoración para nosotros muy importante porque estamos en un nuevo momento, en el que **revaloramos la palabra génesis, porque ya no es el génesis que entró a las comunidades para llevar a cabo la operación paramilitar**, sino un génesis que nos permite reiniciar, reconstruir valores desde la dignidad y desde una propuesta de autonomía” recalcó.

**Revaloración que se logró luego de muchos años de trabajo** “de recorrido, de lucha, de formar primero que todo procesos. De explorar bien nuestra idea de retorno a nuestras tierras. Desde la defensa del medio ambiente, de vivir en armonía con la naturaleza, desarrollándonos como culturas autóctonas” añadió Jahaira.

Todas estas **estrategias de paz siguen siendo construidas y puestas en conocimiento de la sociedad pese a la incursión paramilitar** que se está dando en el Chocó.

“Estos días han sido un poco de tensión, de mirar a ver cómo podemos seguir desarrollando nuestra dinámica cotidiana para ejercer nuestras labores dentro del territorio. Aunque **sabemos que estamos en el marco de unos acuerdos, los paramilitares era algo que nos esperábamos".**

Además, en la carta se solicita al Estado que se desmonte el paramilitarismo de los territorios de Colombia y que se le permita vivir en paz. Le puede interesar: [Nueva incursión de Autodefensas Gaitanistas en Truandó Chocó](https://archivo.contagioradio.com/nueva-incursion-autodefensas-gaitanistas-truando-choco/)

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
