Title: "La tarea será construir desde la participación ciudadana una política de participación" Antonio Madariaga
Date: 2016-07-06 12:15
Category: Nacional, Política
Tags: participación ciudadana, proceso de paz
Slug: la-tarea-sera-construir-desde-la-participacion-ciudadana-una-politica-de-participacion-antonio-madariaga
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/07/movilizacic3b3n-por-la-paz-7.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto:Centro de Memoria, Paz y Reconciliación 

###### [Jul 06 2016]

La mesa de diálogos de la Habana dio luz verde sobre los acuerdos de promoción de la participación ciudadana. Para el desarrollo de este punto las organizaciones **Foro por Colombia**, **CINEP** y **Viva la Ciudadanía** tendrán como primera tarea presentar un documento señalando criterios y lineamientos para que se construya desde **la participación ciudadana una política de participación.**

Este escenario será un espacio de deliberación incluyente, plural y representativo que producirá un proyecto de ley que permita reforzar los [mecanismos para ampliar y fortalecer los procesos de participación ciudadana](https://archivo.contagioradio.com/este-es-el-texto-del-acuerdo-sobre-cese-bilateral-firmado-entre-gobierno-y-farc/). En el espacio podrán confluir las organizaciones y movimientos sociales que estén interesados en ser parte de la construcción del mismo.

De acuerdo con Antonio Madariaga, director de la organización Viva la Ciudadanía, una de las labores que deberán asumir será "evaluar los **mecanismos de participación institucionales** desde la experiencia, para saber si su funcionamiento es o no el adecuado, a su vez índica que también deberán establecer cómo se protegerán las expresiones pacíficas de los **mecanismos de participación no institucionales,** como las movilizaciones o paros, y para finalizar intentarán generar una acción del gobierno y de la sociedad dirigida a **fortalecer las organizaciones y movimientos sociales** para posteriormente dotar de instrumentos a las personas para su participación en la vida social y política del país."

La iniciativa será liderada por el Consejo Nacional de Participación con el apoyo de las organizaciones Foro por Colombia, CINEP y Viva la Ciudadanía y[ tienen dos semanas para presentar el archivo](https://archivo.contagioradio.com/gobierno-y-farc-saldan-puntos-pendientes-en-acuerdo-de-participacion-politica/). Después de la aprobación de la propuesta en la mesa de negociación de la Habana, el siguiente paso será la asesoría por parte de las organizaciones al proceso de participación.

<iframe src="http://co.ivoox.com/es/player_ej_12140009_2_1.html?data=kpeelpWUdJqhhpywj5WbaZS1k5yah5yncZOhhpywj5WRaZi3jpWah5yncaLi1dTby9SPkcLYwtfWw8zFaZO3jLvW2MaPsMKfxM7ixsbIpc-ZpJiSo6nFcYarpJKw0dPYpcjd0JC_w8nNs4yhhpywj5k.&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU).]

 
