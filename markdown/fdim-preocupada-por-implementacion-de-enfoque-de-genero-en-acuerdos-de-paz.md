Title: FDIM preocupada por implementación de Enfoque de género en Acuerdos de Paz
Date: 2018-11-24 11:29
Author: AdminContagio
Category: Mujer, Paz
Tags: acuerdos de paz, enfoque de género, Federación Democrática Internacional de Mujeres
Slug: fdim-preocupada-por-implementacion-de-enfoque-de-genero-en-acuerdos-de-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/02/mujeres-paz.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [23 Nov 2018] 

La Federación Democrática Internacional de Mujeres estuvo en Colombia para hacer una verificación del enfoque de género en la implementación de los Acuerdos de Paz. Sin embargo, de acuerdo con Luz Méndez, integrante de esta comitiva, una de las preocupaciones más grande que se llevan, tiene que ver con "**el proceso de formación, consientización y designación de recursos hacia todas las entidades que juegan un papel en la implementación de ese enfoque"**.

### **El enfoque de género débil en los Acuerdos de Paz** 

Méndez, presidenta de la Asociación de mujeres guatemaltecas y quien participó en el proceso de paz de su país en 1996, manifestó que en las diversas reuniones que mantuvieron con las mujeres, les señalaron que "si bien se han aprobado algunas normas como la entrega de tierras**, no se dice expresamente cuál será el mecanismo que se usará para la entrega de tierras a las mujeres**".

Frente al punto de reincorporación, Méndez señaló que hay muchas debilidades en la implementación de los acuerdos, particularmente en torno a las mujeres, que les permita llevar a acabo este proceso, enfatizando en que es el Estado y la sociedad quienes deben proveer las herramientas para que las personas se incorporen a la vida productiva, económica y política del país.

La participación política de las mujeres, tampoco ha avanzado como se esperaba. Según Méndez, lograron constatar que hay una **participación del 21% en el Congreso**, y si se diera una "adecuada implementación de los Acuerdos de Paz, tendría que dar un resultado en términos de una participación más equitativa, en harás de la igualdad".

### **La Jurisdicción Especial para la paz una gran preocupación** 

De acuerdo con Méndez, durante las diferentes audiencias que se sostuvieron con mujeres, ellas expresaron su preocupación frente a la Justicia Especial para la Paz, en ese sentido la recomendación que realizó la Federación Democrática de Mujeres Internacionales es que se de el espacio **para que la JEP cumpla con las funciones para las cuales fue creada**.

Asimismo, las mujeres también afirmaron la necesidad de mejorar los mecanismos para poder acceder a la JEP y garantizar que se les escuche de forma adecuada, "sin que haya algún obstáculo para este trabajo". (Le pude interesar: ["En Colombia no hay avances para detener la violencia en contra de las Mujeres"](https://archivo.contagioradio.com/en-colombia-no-hay-avances-para-detener-la-violencia-contra-las-mujeres/))

### **Las Recomendaciones de la FDIM** 

Entre las recomendaciones que la FDIM le hizo al gobierno de Colombia para cumplir con la implementación del enfoque de género en los Acuerdos de Paz, una de la más importantes consiste en dar garantías políticas y acciones concretas de protección a las lideresas y **al mismo tiempo proveer recursos financieros, técnicos y humanos para lograr la implementación**.

De igual forma, señalaron a importancia de que se tenga en cuenta el trabajo que ya han adelantado las **organizaciones defensoras de los derechos humanos de las mujere**s en este camino de la implementación para aportar a la construcción de paz en Colombia. (Le puede interesar:["Acoso sexual en las universidades, una violencia sexual contra las mujeres"](https://archivo.contagioradio.com/acoso-sexual-en-la-universidades/))

Para Méndez, uno de los componentes más trascendentales de los Acuerdos de Paz en Colombia es la integración del enfoque de género, porque "son muy pocos los acuerdos de paz en el mundo que tienen esta características, es decir que tienen una mirada específica sobre las condiciones de vida, demandas y propuestas de las mujeres". Mientras que en el caso particular de Colombia se introducen más de 100 acuerdos en favor de las mujeres y de la igualdad de género, razón por la cual hay que llevarlos a buen puerto.

###### Reciba toda la información de Contagio Radio en [[su correo]

######  
