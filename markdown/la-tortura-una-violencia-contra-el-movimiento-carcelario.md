Title: La tortura, una violencia contra el movimiento carcelario
Date: 2018-11-19 16:20
Author: AdminContagio
Category: Expreso Libertad
Tags: crisis carcelaria, hacinamiento, presos politicos
Slug: la-tortura-una-violencia-contra-el-movimiento-carcelario
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/11/descarga-3.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: El Espectador 

###### 13 Nov 2018 

**Hacinamiento, propagación de enfermedades sanitarias, problemas estructurales, son algunas de las situaciones que deben padecer las personas al interior de las cárceles de Colombia**, hechos que al denigrar y violar los derechos fundamentales de los mismos, se convierten en tortura.

Escuche en este **[Expreso]{.il} Libertad** el contexto de la población reclusa en el país y los llamados al gobierno para que tome medidas urgentes frente a estos hechos.![](https://ssl.gstatic.com/ui/v1/icons/mail/images/cleardot.gif){.ajT}

<iframe id="audio_30171424" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_30171424_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Encuentre más información sobre Prisioneros políticos en[ Expreso libertad ](https://archivo.contagioradio.com/programas/expreso-libertad/)y escúchelo los martes a partir de las 11 a.m. en [Contagio Radio](https://archivo.contagioradio.com/alaire.html) 

 
