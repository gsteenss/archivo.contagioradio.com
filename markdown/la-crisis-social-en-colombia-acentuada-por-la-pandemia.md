Title: La crisis social en Colombia acentuada por la pandemia
Date: 2020-10-21 18:09
Author: Foro Opina
Category: Columnistas invitados, Opinion
Tags: Coronavirus, Crisis social, Foro opina, opinion, pandemia, salud en colombia
Slug: la-crisis-social-en-colombia-acentuada-por-la-pandemia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/10/WhatsApp-Image-2020-10-19-at-6.34.36-PM.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:heading {"align":"right","level":6,"textColor":"cyan-bluish-gray"} -->

###### Foto por: Contagio radio {#foto-por-contagio-radio .has-text-align-right .has-cyan-bluish-gray-color .has-text-color}

<!-- /wp:heading -->

<!-- wp:heading {"level":5} -->

##### Colombia es oficialmente el quinto país del mundo más afectado por el COVID-19 con más de 818 mil  contagios y cerca de 26.000 muertes. Si a esto se suma los pronósticos negativos en materia de agudización de la pobreza y exclusión social nos aproximamos a una catástrofe inminente.

<!-- /wp:heading -->

<!-- wp:paragraph -->

Ante esta realidad, preocupa la actitud facilista del gobierno que culpa de todos los males al coronavirus. Sin embargo, la pandemia no ha hecho más que dejar en evidencia la enorme deuda del Estado en materia de derechos políticos, sociales y económicos con buena parte de la población. 

<!-- /wp:paragraph -->

<!-- wp:heading {"level":5} -->

##### En efecto, la crisis de la salud estalló con el coronavirus.

<!-- /wp:heading -->

<!-- wp:paragraph -->

Las EPS, concebidas como actores centrales en la atención integral, le han fallado al país en la detección y tratamiento del COVID-19. Los datos de la Procuraduría General de la Nación indican que a mediados de septiembre se contabilizaban cerca de 62 mil pruebas represadas con retrasos de hasta 30 días.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Además, los pacientes que buscan atención se enfrentan al desafío de conseguir un diagnóstico y tratamiento oportuno en medio de un sistema fragmentado, disperso e ineficiente. La emergencia también puso en evidencia los efectos del robo sistemático de los recursos, el desmantelamiento de los hospitales públicos y el deterioro de la red de aseguramiento territorial.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":5} -->

##### Esto ha generado dos consecuencias:

<!-- /wp:heading -->

<!-- wp:list -->

-   por un lado, la Federación Médica Colombiana señala que se perdieron cerca de 8.500 camas en los últimos 10 años, cifra que en las condiciones actuales contribuiría a disminuir la presión de la emergencia.

<!-- /wp:list -->

<!-- wp:list -->

-   por otro, miles de pacientes han tenido que ser trasladados a otras ciudades e incluso fallecen alejados de sus familiares, algo totalmente inaceptable en un Estado Social de Derecho.

<!-- /wp:list -->

<!-- wp:heading {"level":5} -->

##### Tampoco se ha cumplido con los subsidios y mejoras en la situación laboral del personal de la salud que enfrenta la pandemia.

<!-- /wp:heading -->

<!-- wp:paragraph -->

A mediados de septiembre se contabilizaban 67 trabajadores del servicio sanitario muertos y 7.500 contagiados prestando el servicio de atención a pacientes, según cifras del Instituto Nacional de Salud (INS). Además, la [Federación Médica Colombiana](https://federacionmedicacolombiana.com/) señala que el 80% de los médicos no cuentan con afiliación a aseguradoras de riesgos laborales, el 80% tiene dificultades contractuales, la carencia de insumos es evidente, y en no pocos casos enfrentan amenazas, constreñimientos para ejercer su labor y discriminación social.   

<!-- /wp:paragraph -->

<!-- wp:heading {"level":5} -->

##### Las imágenes de trapos rojos en las ventanas de casas y apartamentos demuestran el fracaso de la política social sustentada en subsidios asistencialistas.

<!-- /wp:heading -->

<!-- wp:paragraph -->

No se generó ningún avance en la reducción de la desigualdad: uno de cada siete hogares no tiene acceso a agua potable, apenas el 15% de los sectores más vulnerables cuenta con internet y uno de cada cuatro niños sufre de desnutrición. Los datos también muestran la inequidad territorial entre las capitales y los municipios periféricos.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En estas condiciones, era predecible que en medio de la pandemia se acelerara el aumento de la miseria del 10% a 14%, y la pobreza de 29% a 34% entre 2019 y 2020, según cifras de la [CEPAL](https://www.cepal.org/es). La mayor afectación será la de la clase media con una reducción estimada de 1,8 millones de personas. Por esta vía se perderá lo ganado en cobertura escolar. 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Al respecto, la [UNESCO](https://es.unesco.org/) calcula que la deserción aumentará en 28% para los grados de enseñanza primaria, secundaria y media y en 18% en educación profesional. Lamentablemente, la pobreza golpeará con mayor rudeza a los municipios afectados por la exclusión, la violencia y las economías ilegales, alejando sus posibilidades de avanzar hacia la paz. 

<!-- /wp:paragraph -->

<!-- wp:heading {"level":5} -->

##### La respuesta del gobierno nacional ante la crisis ha sido lenta y sin una hoja de ruta integral.

<!-- /wp:heading -->

<!-- wp:paragraph -->

Se pasó de un escenario de cuarentena a una estrategia de “nueva normalidad” con la apertura de las actividades productivas, el discurso de la corresponsabilidad ciudadana en la reducción de los contagios y la compra de ventiladores para aumentar la capacidad de camas UCI.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Luego de demoras en las negociaciones y las denuncias de la falta de claridad sobre la estrategia del gobierno para adquirir la vacuna contra el COVID-19, el presidente decidió incluir a Colombia en la alianza internacional COVAX para adquirir porcentajes específicos de este producto y firmó acuerdos con los laboratorios Pfizer y AstraZeneca. Mientras tanto, se discute la estrategia adoptada por Minsalud de disminuir el número de pruebas para detectar el virus: a mediados de agosto se aplicaban cerca de 35 mil pruebas y un mes después bajó a algo más de 24 mil, de acuerdo con los reportes del INS. Finalmente, preocupa el escenario de rebrote, tal como ha ocurrido en Europa. 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En el campo social, se han destinado 20 billones de pesos para ayudas a la población pobre, el aumento de la capacidad en el sistema de salud y el financiamiento del sector público. La estrategia estrella ha sido el   programa de ingreso solidario para cerca de 3 millones de personas con dos giros que sumados apenas llegan a la mitad del salario mínimo, acompañado de subsidios aislados y mercados distribuidos a lo largo del país con un saldo de 110 mil millones de pesos perdidos en la trampa de la corrupción.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

La unidad tampoco ha sido la regla, predominan las tensiones, disputas políticas y acusaciones mutuas del gobierno nacional y las entidades territoriales por las decisiones tomadas sin contar con los alcaldes y gobernadores, y la declaratoria de inconstitucionalidad de varios decretos, entre ellos, el alivio en el pago de los servicios públicos por la falta de firmas de todos los ministros, lo que demuestra el grado de improvisación que rige en el gobierno. 

<!-- /wp:paragraph -->

<!-- wp:heading {"level":5} -->

##### Ante esta realidad, Colombia enfrenta el desafío de promover alternativas al enfoque cortoplacista imperante. Varios temas aparecen en esta agenda de cambio.

<!-- /wp:heading -->

<!-- wp:list -->

-   En primer lugar, el coronavirus demostró la urgencia de cambiar el sistema de salud para hacerlo más equitativo, transparente, con mayor énfasis en la prevención, en cabeza del Estado, con más recursos para la investigación y con mayor autonomía médica.

<!-- /wp:list -->

<!-- wp:list -->

-   En segundo lugar, el país tiene la urgencia de reactivar la oferta de trabajo sin acudir a la fórmula simplista de recudir los derechos adquiridos. Si bien, el gobierno conformó una Misión de Empleo para avanzar en este propósito, con participación del sector privado, los gremios y los trabajadores, es incierto su alcance y su capacidad para promover un pacto colectivo sobre asuntos de importancia como la reforma laboral, el problema desbordado de la informalidad, la reglamentación del teletrabajo y los incentivos y subsidios a la nómina.

<!-- /wp:list -->

<!-- wp:paragraph -->

Tampoco se sabe cuál va a ser su autonomía para plantearle al país una agenda basada en el ingreso básico universal y un nuevo sistema de protección que promueva la promoción de oportunidades diferenciadas.

<!-- /wp:paragraph -->

<!-- wp:list -->

-   Por último, una discusión inaplazable es la configuración de un régimen tributario con tres principios orientadores: **la equidad** en la redistribución de la renta, **la inversión pública** hacia proyectos de innovación y de agregación de valor productivo diferentes a la minería y los hidrocarburos, y **la inclusión de los territorios** que históricamente han estado por fuera del desarrollo nacional.

<!-- /wp:list -->

<!-- wp:heading {"level":6} -->

###### **La Fundación Foro Nacional Por Colombia** Es un Organismo Civil no Gubernamental sin ánimo de lucro, creado en 1982, cuyos objetivos son contribuir al fortalecimiento de la democracia en Colombia. Desarrolla actividades de investigación, intervención social, divulgación y deliberación pública, asesoría e incidencia en campos como el fortalecimiento de organizaciones, redes y movimientos sociales, la participación ciudadana y política, la descentralización y la gestión pública, los derechos humanos, el conflicto, la paz y las relaciones de género en la perspectiva de una democracia incluyente y efectiva. Foro es una entidad descentralizada con sede en Bogotá y con tres capítulos regionales en Bogotá (Foro Región Central), Barranquilla (Foro Costa Atlántica) y Cali (Foro Suroccidente).

<!-- /wp:heading -->

<!-- wp:heading {"level":5} -->

##### **Contáctenos: 316 697 8026\_ 031282 2550 /Comunicaciones@foro.org.co**

<!-- /wp:heading -->

<!-- wp:paragraph -->

[Vea mas columnas de opinión](https://archivo.contagioradio.com/opinion/columnistas/)

<!-- /wp:paragraph -->
