Title: “Laudato si” y la  destrucción ambiental planetaria
Date: 2015-06-25 11:05
Category: Cesar, Opinion
Tags: cambio climatico, Emissions Gap, Enciclica Papal Medio ambiente, Naciones Unidas, OTAN, PNUMA
Slug: laudato-si-y-la-destruccion-ambiental-planetaria
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/06/logo9.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: neoteo ] 

#### **[Por [Cesar Torres del Río](https://archivo.contagioradio.com/cesar-torres-del-rio/) - [~~@~~logicoanalista2](https://twitter.com/logicoanalista2)** 

###### [25 Jun 2015] 

La primera encíclica papal sobre los graves problemas ecológicos a que está sometido el planeta es a la vez una denuncia del capitalismo salvaje pues critica a los poderes que han llevado a cabo un implacable modelo económico y social que se ha hecho “insostenible” para la humanidad y que sólo ha beneficiado a unas decenas de empresas nacionales y transnacionales y a unos cuantos Estados; el capitalismo ha convertido la tierra en un “inmenso depósito de porquería” El documento Vaticano es a la vez un llamamiento ecuménico a todos los creyentes para que contribuyan a detener la tragedia de la “casa común” que gira alrededor del astro solar.

**“Laudato si”**[ ]{.Apple-converted-space}aporta así argumentos para la discusión que se abordará en la Conferencia de Naciones Unidas sobre Cambio Climático 2015, en Paris a fin de este año. En criterio del Vaticano el encuentro preparatorio de Lima, en diciembre de 2014, fue decepcionante y “falto de coraje” en relación a todos los aspectos de la catástrofe ambiental mundial. Por eso llama a un “cambio radical en el comportamiento de la humanidad” ya que ["Nunca hemos maltratado y lastimado nuestra casa común como en los últimos dos siglos". En breve, el capitalismo es el responsable de la tragedia humana.]{.s3}

El encuentro de París, con todo, no alterará el curso depredador del extractivismo de las multinacionales ni la orientación suicida de los Estados imperialistas y sus agentes neocoloniales del Sur. Mantener el recalentamiento por debajo de los 2° C, como se dice han acordado los negociadores, es simplemente imposible; en el Informe Emissions Gap, del PNUMA (el Programa sobre el medio ambiente) y en el del Grupo Intergubernamental de Expertos sobre el Cambio Climático se[ ]{.Apple-converted-space}afirma que el tope mencionado se lograría sólo si la concentración de CO[2[ ]{.Apple-converted-space}]{.s4}no sobrepasara el umbral de 350-400 ppm (partes por millón en volumen), nivel que fue superado ya en el mes de marzo, y siempre y cuando el volumen de las emisiones mundiales se redujera entre 50-85%[ ]{.Apple-converted-space}para el año 2050.

La angustiante realidad también es un hecho en Colombia. El Plan Nacional de Desarrollo afirma sin ambages que recorremos un camino ambientalmente insostenible; pero eso poco importa a las empresas extractivas, a los piratas medioambientales que talan los árboles en los humedales o que drenan las ciénagas, a los empresarios de la minería ilegal, a los promotores del fracking. Mucho menos les interesa a los que contratan préstamos con el Fondo Monetario Internacional[ ]{.Apple-converted-space}para promover el “desarrollo” o a aquellos que se apropian de los terrenos baldíos y siembran miles de hectáreas con palma africana.

El planeta se encuentra en el abismo galáctico. De “común” la casa no tiene sino el nombre pues la controla y dirige la OTAN, el FMI, el Consejo de Seguridad, Estados Unidos y la Troika europea. Si de humanidad se trata su futuro dependerá de ella misma y no de las cruzadas en[ ]{.Apple-converted-space}su contra argumentando la defensa de la democracia para idolatrar al mercado; tampoco la especie se salvará mediante las “coartadas verdes” de los partidos o gracias a la “ecología de mercado”.[ ]{.Apple-converted-space}El mercado “divinizado” conduce directamente a la destrucción de toda dignidad humana y de sus más caros valores.

Requerimos una estrategia política ecosocialista, que apunte a todos aquellos aspectos que “Laudato si” ha planteado y que son los mínimos presupuestos a discutir y a resolver. Tal estrategia debe apuntar hacia la planificación democrática de todo el equilibrio ecológico del planeta [ ]{.Apple-converted-space}por parte de los trabajadores y marginados, creyentes y no creyentes, judíos, islámicos y cristianos. Hay que aplicar desde ahora el “freno de mano” del que hablara Walter Benjamin pues el peligro de incendio ya ha comenzado.
