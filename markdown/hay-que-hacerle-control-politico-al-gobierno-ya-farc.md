Title: Hay que hacerle control político al gobierno ya: FARC
Date: 2020-05-12 21:28
Author: AdminContagio
Category: Actualidad, Política
Tags: FARC, Gobierno
Slug: hay-que-hacerle-control-politico-al-gobierno-ya-farc
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/Congreso-curules.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:heading {"align":"right","level":6,"textColor":"cyan-bluish-gray"} -->

###### Foto: SenadoGovCo {#foto-senadogovco .has-cyan-bluish-gray-color .has-text-color .has-text-align-right}

<!-- /wp:heading -->

<!-- wp:paragraph -->

En una rueda de prensa que tuvo lugar este martes 12 de mayo, la bancada del partido Fuerza Revolucionaria del Común (FARC) presentó un [paquete](https://twitter.com/Sergio_FARC/status/1260309172857470976) de medidas para modificar, derogar y adicionar elementos a los decretos con fuerza de Ley que ha expedido el presidente Duque en el marco del estado de emergencia por la pandemia del Covid-19.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Las propuestas resultan determinantes en momentos en que congresistas de la oposición han señalado la necesidad de que el legislativo se active para cumplir con su función de control a las decisiones del ejecutivo, y además, en medio de denuncias por beneficios, vía decreto, a los grandes capitales en detrimento de las personas.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Los decretos que busca modificar FARC

<!-- /wp:heading -->

<!-- wp:paragraph -->

El senador Julian Gallo, más conocido como Carlos Lozada, explicó que el paquete legislativo surge como continuación al [Plan de Choque Social](https://archivo.contagioradio.com/la-propuesta-de-farc-para-salvar-la-economia-popular/)propuesto hace apenas unas semanas por la colectividad. De allí que algunos elementos respondan a avances legislativos sobre temas como salud, educación o gasto público que ya aparecían en el mencionado documento.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En primer lugar, FARC propone modificar el decreto de [excarcelación](https://archivo.contagioradio.com/decreto-presidencial-no-resuelve-hacinamiento-ni-emergencia-por-covid/) (Decreto Ley 546 de 2020), para que sean excluídos de este beneficio personas condenadas "por delitos que por su especial gravedad presenten un peligro para la sociedad y las víctimas". Así, responderían al llamado de cientos de internos en las cárceles que, incluso sin estar condenados, están expuestos a una difícil situación sanitaria en los penales.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En segundo lugar, FARC presentará una adición al Decreto Legislativo 533 de 2020, relacionado con el Plan de Alimentación Escolar (PAE), que dadas las cirunstancias de la pandemia, tuvo que modificarse para que estudiantes lo sigan recibiendo en su casa. En ese sentido, la adición contiene aportes anticorrupción, mediante el fortalecimiento de la denuncia ciudadana y la veeduría en contratación.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Un tema que se hizo presente en el Plan de Choque fue el tema sanitario. FARC presentó una reforma al Decreto Legislativo 538 de 2020, con la intención de aumentar la capacidad de atención en centros hospitalarios mediante equipos médicos, garantías para el personal relacionado con el derecho a la salud y la adquisición de tecnología para enfrentar la pandemia.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

También se incluyó una modificación al Decreto Legislativo 460 de 2020, relacionado con el funcionamiento de las comisarías de familia, para que los funcionarios de estas instituciones cuenten con mayores elementos de higiene y bioseguridad que les permitan seguir desempeñando sus funciones, en momentos en que la violencia contra la mujer, los niños, niñas y adolescentes presenta un pico.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por último, FARC presentó una modificación al Decreto Legislativo 553 de 2020 para que los adultos mayores, beneficiarios del programa Colombia Mayor reciban un aporte durante 6 meses, no se les reduzca este beneficio por ninguna razón y se autorice a entes territoriales a cofinanciar el subsidio, si así lo determinan para que esta población esté protegida de la crisis económica.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Los decretos que proponen derogar

<!-- /wp:heading -->

<!-- wp:paragraph -->

Organizaciones sociales y congresistas como el senador Wilson Arias han denunciado algunos decretos que consideran que no atienden a la población que sufre la pandemia, y en cambio sí benefician a los grandes capitales. Con una idea similar, FARC propuso la derogación de dos decretos que consideró relevantes en este momento.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El Decreto Legislativo 488 de 2020 que se refería al retiro de cesantias de fondos privados, el otorgamiento de vacaciones y de protección a las personas que pierden su trabajo se derogaría. La intención del partido es que se proteja a los trabajadores evitando despidos de manera unilateral, y se les brinde un beneficio económico a los trabajadores de Empresas de Servicio Temporal.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por último, FARC presentó la derogación del Decreto Legislativo 558, "toda vez que significa una reforma pensional en detrimento del fondo público de pensiones Colpensiones". El decreto permite hacer un aporte menor a la salud y pensión tanto de trabajadores del sector público y privado, así como aquellos que son independientes.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Todas las propuestas de FARC, que incluyen algunos ajustes a otros decretos y la renta básica, tendrán que ser discutidos por el Congreso de la República, en momentos en que algunos parlamentarios luchan porque se reinicien las sesiones semipresenciales, mientras otros impulsan la idea de mantener el congreso operando de manera virtual, y con funciones reducidas.

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->
