Title: Avalancha de impuestos y poca credibilidad del Estado
Date: 2016-06-23 06:00
Category: Opinion, Ricardo
Tags: Congreso de Colombia, educacion, Impuestos, Salud
Slug: avalancha-de-impuestos-y-poca-credibilidad-del-estado
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/06/Impuestos-Mitos.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: elminnesotadehoy 

#### **[Ricardo Ferrer Espinosa @ferrer\_espinos](https://archivo.contagioradio.com/ricardo-ferrer-espinosa/)** 

###### 23 Jun 2016

De vez en cuando el Estado justifica el cobro de impuestos con dos argumentos infalibles: serán para mejorar la salud y la educación. Supuestamente el ciudadano cree el argumento y tributa. Pero en Colombia estamos muy lejos de creer que las políticas tributarias tengan el destino correcto.

El IVA es el mejor ejemplo de la mentira: el recaudo se hace sobre cada producto vendido. Por el volumen de consumidores colombianos debería ser suficiente para brindar los mejores servicios de salud y deberíamos tener el mejor sistema escolar. Pero estos servicios siempre están en déficit, son precarios y no protegen a cada ciudadano.

#### [Los ciudadanos pagan impuestos para la]**salud**[, pero inexorablemente deben pagar:] 

1.  [Cuotas de afiliación a EPS, ARL y otros seguros.]
2.  [Cuando consulta, el paciente cubre un copago.]
3.  [Copagos cada vez que acceda a servicios como farmacia, laboratorio, especialistas y cirugías.]
4.  [Los programas de rehabilitación son deficientes.]
5.  [El paciente de alto costo es asesinado por las EPS que omiten atender traumas, cáncer y enfermedades degenerativas.]
6.  [Enfermedades raras, medicamentos y tratamientos no POS impiden el acceso real a la salud integral. Para tener salud completa, el ciudadano debe vivir haciendo filas eternas, presentando tutelas y derechos de petición.]

Lo verdaderamente triste es la pasividad del ciudadano ante el modelo de salud, que evidentemente ha fracasado.  La protesta de los trabajadores de la salud no recibe el suficiente respaldo de los pacientes y víctimas del sistema.

[Igual pasa con la]**educación**[, muy publicitada por el Estado en la televisión.]

1.  [Los costos de matrículas supuestamente son mínimos o gratuitos, pero en regiones enteras no se construye un entorno que facilite la inserción escolar. No existe un pensamiento proactivo que busque al niño y lo integre.]
2.  [El Estado no cubre el costo de útiles escolares. Las tabletas informáticas y las nuevas tecnologías son excepcionales y poco integradas a un sistema educativo continuo.]
3.  [El Estado no cubre costos de transporte.]
4.  [El Estado es errático en el suministro de alimentos escolares, que se convirtieron en el paradigma de la corrupción.]
5.  [El sistema educativo no está bien integrado con los sistemas de salud, especialmente los que más afectan el sistema escolar: salud oral, visual, auditiva.]
6.  [La atención psicosocial a la población escolar es mínima, si se compara con la realidad de la guerra y el desplazamiento forzado, donde la mayor parte de las víctimas son infantes, mujeres y adolescentes.  ]

La guerra es costosa, nos ha recordado el mandatario de turno. Costosa en términos de dinero, clima de inversión, daño a la infraestructura, dicen los políticos. Pero al pueblo colombiano nadie debería recordarle el costo de la guerra, porque hemos puesto los muertos, desplazados, la población traumatizada, y desposeída.

Los impuestos se fueron para las Fuerzas Armadas, que omitieron el deber constitucional de proteger a la población. Los impuestos se pierden en un congreso enorme que legisla con malignidad y omite juzgar a los grandes dueños del Estado, a los aforados que gozan de perpetua impunidad.

**Reducir a la mitad el congreso de Colombia y reducir a la mitad los sueldos de la “clase política” es una propuesta que debe ser tomada en serio**[. El parlamente colombiano es una vena rota en los presupuestos del Estado.]

Que haya un solo pobre en Colombia es absurdo, dada la colosal riqueza del territorio. Los niños muriendo de hambre sublevan por cinco minutos a los televidentes y luego pasamos a ver el partido de fútbol, que recibe todo el despliegue en las redes sociales.

**Por hambre se están muriendo miles de ancianos en Colombia, en mucha mayor proporción  que los niños**[, pero esta cruda realidad no ha tenido el despliegue correspondiente. Los viejos son desechables, no motivan la solidaridad ni generan trinos en Twitter.]

[  La credibilidad tributaria se siente cuando el Estado cubre las necesidades de la población más vulnerable. Pero esa mentalidad protectora está por construir en nuestra patria demente, que como Saturno, devora a sus hijos.]

------------------------------------------------------------------------

###### [Las opiniones expresadas en este artículo son de exclusiva responsabilidad del autor y no necesariamente representan la opinión de la Contagio Radio.] 
