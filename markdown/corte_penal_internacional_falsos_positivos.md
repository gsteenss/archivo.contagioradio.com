Title: CPI solicita que 29 generales y coroneles sean juzgados por ejecuciones extrajudiciales
Date: 2017-07-25 10:11
Category: DDHH, Nacional
Tags: Délitos de lesa humanidad, falsos positivos
Slug: corte_penal_internacional_falsos_positivos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/08/FALSOS-POSITIVOS-e1500995272586.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: El Pilón 

###### [25 Jul 2017] 

Un  total de 29 altos cargos del ejército de Colombia serían juzgados por delitos de lesa humanidad, por el caso específico de las ejecuciones extrajudiciales durante el gobierno de Álvaro Uribe. Así lo ha pedido la Corte Penal Internacional, que ha señalado que existen todas las condiciones para **calificar esos asesinatos como crímenes de lesa humanidad.**

Fatou Bensouda, Fiscal de la Corte Penal Internacional (CPI), le ha advertido al Gobierno colombiano que se garantice una investigación seria contra los** **altos militares retirados y activos implicados en estos tipos de crímenes. Entre ellos se encuentran el **General retirado Mario Montoya, General (r) Óscar González Peña, el General Jaime Lasprilla Villamizar y el General Juan Pablo Rodríguez Barragá,** actual comandante general de las Fuerzas Militares.

Además la CPI culpa al Gobierno Santos de no brindar las “pruebas tangibles de que se estén realizando indagaciones relevantes por parte de las autoridades colombianas”, es decir que podría existir una intervención de la Justicia Internacional, ante los obstáculos en la justicia colombiana, pues por el momento el único general encausado en estos delitos ha sido el General Torres Escalante.

### Delitos de lesa humanidad 

Desde la CPI se habla de 1228 de “falsos positivos” documentados, sin embargo cita también el que emitió en 2015 el representante en Colombia del Alto Comisariado para los Derechos Humanos de la ONU, Todd Howland, según el cual **las víctimas de "falsos positivos" superarían los 5000 civiles**.

Se trataría entonces de delitos de lesa humanidad pues "La mayoría de los asesinatos se cometieron para mostrar resultados en la lucha contra la guerrilla y las víctimas fueron seleccionadas especialmente pues residían en zonas remotas o pertenecían a sectores marginales de la población. **Para ubicarlas se usaron civiles, paramilitares, policías o militares que secuestraron o atrajeron a las víctimas bajo engaños**, como promesas de trabajo. Tras su asesinato, les colocaban armas o equipos al lado de sus cuerpos para simular combates que nunca tuvieron lugar", señala la fiscalía de la CPI.

Cabe recordar que, teniendo en cuenta que serían delitos de lesa humanidad al ser cometidos contra civiles que fueron engañados, y no dentro del conflicto directo entre guerrilleros y militares, **los 23 generales y 6 coroneles acusados por la CPI** no podrían acogerse a la **Justicia Especial de Paz **acordada en La Habana.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
