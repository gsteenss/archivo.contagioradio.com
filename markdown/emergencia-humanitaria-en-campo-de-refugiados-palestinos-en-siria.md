Title: Emergencia humanitaria en Campo de refugiados Palestinos de Siria
Date: 2015-04-12 09:54
Author: CtgAdm
Category: El mundo, Otra Mirada
Tags: Al Yarmuk, Al-Yarmuk atacado por ISIS, Campo de refugiados Palestinos en Siria, Emergencia humanitaria Yarmuk, Red de solidaridad con los Palestinos de Siria
Slug: emergencia-humanitaria-en-campo-de-refugiados-palestinos-en-siria
Status: published

###### Foto:Sumadiario.com 

###### **Entrevista con [[Muhammad]], activista de la Red de solidaridad con los Palestinos en Siria:** 

<iframe src="http://www.ivoox.com/player_ek_4335765_2_1.html?data=lZigl5yaeY6ZmKiak5eJd6KnlZKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRic7Z08zS0MjNpYzc1tLO0M7YpdPdwpDS0JDJsIzXwtLd0ZDIqYzmxsviyc7FqNDnjMnSjb7Fto6ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

Según **Muhammad, portavoz de la Red de solidaridad con los Palestinos en Siria**, el campo de refugiados Al-Yarmuk, se encuentra en una situación de **emergencia humanitaria** debido a la escasez de alimentos pero también a que el **régimen Sirio** les ha **cortado el agua y la luz**.

Muhammad afirma que el campo es objeto de numerosos ataques por parte de los grupos islamistas radicales como el **¨ISIS¨, pero también del régimen Sirio que no les perdona haberlos apoyado cuando se rebelaron contra su gobierno**.

Por otro lado también nos explica que esto hace parte de una **estrategia de Israel** para atacar a los refugiados fuera de las fronteras de Palestina, con el objetivo de **expulsarlos de Siria.**

En la entrevista nos relata también sobre la situación en **Gaza**, donde **no hay medicamentos ni instrumentos médicos** para los hospitales debido al **bloqueo** de **Israel**, que dura ya más de 8 años.

Esta situación es extremadamente peligrosa, ya que aún hay muchas personas , principalmente **niños, heridos en los hospitales por la invasión terrestre de Israel en Agosto de 2014.**

Desde la red de solidaridad culpan a la **ONU, a Siria y a la OLP, de haber dejado en una situación de abandono total el campo**, además añaden que en reiteradas ocasiones han solicitado la ayuda internacional y no han recibido respuesta.

El campo de refugiados palestino **Al Yarmuk** cercano a la capital de Siria fue creado en 1957 para albergar a miles de ciudadanos palestinos que huían de las políticas de despojo de tierras y expulsión por parte de Israel. Actualmente es blanco de **ataques de los grupos extremistas** de carácter islamista, Estado **Islámico y Frente Al Nusra**, provocando una **emergencia humanitaria**.

El miércoles **1 de abril**, miembros del EI, ocuparon violentamente el campo, dejando un saldo de **15 muertos y más de 70 heridos**. Está no ha sido la única incursión, ya que en **2012 fue ocupado y casi controlado por los extremistas.**

Actualmente el campo alberga a más de **18.000 palestinos** que no han podido regresar a sus tierras. Según organismos internacionales como la Umrwa (Agencia de la ONU para los refugiados en Palestina) la situación es muy precaria debido a la escasez de agua, medicinas y alimentos.

La ONU, y el gobierno sirio han denunciado públicamente la situación en el campo de refugiados exigiendo el cese de la cooperación de países como Arabia Saudí con los grupos extremistas y demandando ayuda a la comunidad internacional para la protección del mismo.

Distintos **portavoces palestinos** han denunciado también la situación calificándola de **“catastrófica”.**

**Jaled Abdul Mayid** representante del **Frente de luchas Palestinas** señaló que “Los **terroristas** del EI y del Frente al Nusra **secuestraron a decenas de vecinos** del campamento, cortaron las cabezas de cinco personas y mataron a tiros a otros dos”.

Para el partido político palestino **Hamas**, la situación es cada vez peor y también la considera como una **“catástrofe”.**

Según **Fayez Abu Eid**, portavoz del **Grupo de Acción para los Palestinos en Siria** “no hay agua ni comida y todavía quedan muchos civiles que no han podido huir”.

En total en lo que va de año el **EI ha asesinado en Siria a más de 2 mil personas**, los bombardeos de la coalición encabezada por EEUU no han conseguido frenarlo y su avance es debido en parte  la financiación de países como Arabia Saudí.

Según **Leila Nachawati, activista y analista hispanosiria**, el campo de refugiados de Yarmuk esta siendo **hostigado** por parte de los extremistas como camino para la toma de la capital, pero **también por el régimen sirio, ya que cuando el levantamiento los palestinos también apoyaron a los rebeldes**, de tal manera la emergencia humanitaria es mayo por la desatencion gubernamental y por la amenaza del EI.

 Entrevista con Leila Nachawati: http://bit.ly/1ycR1Qn
