Title: '12 Apóstoles' al mando de Santiago Uribe cometieron cerca de 570 crimenes
Date: 2017-10-13 08:59
Category: Judicial, Nacional
Tags: 12 apostoles, alvaro uribe velez, Paramilitarismo, Santiago uribe
Slug: santiago_uribe_homicidios_12_apostoles
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/03/Santiago-Uribe.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Caracol Radio] 

###### [12 Oct 2017] 

Este viernes es la primera audiencia preparatoria contra Santiago Uribe Vélez, hermano del senador Álvaro Uribe, por el asesinato del transportador Camilo Barrientos, en el marco de las acciones y masacres por parte del grupo paramilitar 'Los 12 Apóstoles'. Una primera fase del proceso contra Uribe, que se espera, sea la puerta para que se investigue unos 570  crímenes cometidos por ese grupo.

De acuerdo con el abogado Manuel Garzón y el abogado Daniel Prado, de la Comisión de Justicia y Paz, **"Todo el expediente del proceso evidencia la comisión de una serie sistemática de homicidios** que conducen a que la fiscalía declare que lo acontencido en la región antioqueña del país entre los 1990 y 1998, constituye una serie de crímenes de lesa humanidad".

Aunque la Fiscalía actualmente le imputa a Uribe el homicidio de Barrientos, el abogado Garzón explica que en el contexto hay todo un análisis por parte del ente investigador, que evidencia cómo el grupo paramilitar tuvo un papel determinante en la generación de la última ola de paramilitarismo en Antioquia. Se estiman cerca de unos 570 asesinatos, "por cuya responsabilidad presunta alguna y demostrada otra, como lo evidencian sentencias del Consejo de Estado, **se ha condenado al Estado por los crímenes de los 12 Apóstoles",** dice el abogado.

En este primer proceso, Santiago Uribe deberá responder por delitos como concierto para delinquir y homicidio agravado, pero como señala Manuel Garzón, estos, se cometieron bajo "un plan de macrocriminalidad y un contexto de crímenes sistemáticos, que conforman crímenes de lesa humanidad". En ese sentido, se resalta que el hermano del expresidente, "no era un aliado, ni un financiador. Era la cúspide de ese aparato criminal". De manera que por su responsabilidad en el mando podría ser investigado de una forma más profunda, según lo establecido procesalmente.

**Las víctimas de los 12 apóstoles**

Según datos de la resolución de acusación, los cerca de 570 asesinatos, ocurrieron en la región antioqueña, en lo municipios de **Angostura, Briceño, Campamento, Donmatías, San José de la Montaña, Santa Rosa de Osos, Valdivia y Yarumal.**

De las víctimas del grupo paramilitar, 246 asesinatos fue contra agricultores, mineros, forasteros, personas en estado de embriaguez, presuntos expendedores de drogas o adictos, estudiantes, informantes o testigos, amas de casa y personas dedicadas a oficios varios. Asimismo, hay 136 casos, entre los cuales había comerciantes, carniceros, ganaderos y transportadores, además de habitantes rurales, líderes o militares políticos y personas señaladas de ser presuntos integrantes o auxiliadores de la guerrilla.

Por otra parte se registran 81 homicidios contra personas señaladas de ser delincuentes, extorsionistas, secuestradores o integrantes de bandas. Según la misma resolución, habrían sido asesinadas 51 personas sobre las cuales se desconoce su profesión u ocupación. Un quinto sector contempla a 11 homicidios de integrantes de inspecciones de policía y miembros activos o retirados de la Fuerza Pública. También habría 5 casos de personas cuya ocupación no está definida y fallecieron y extrañas circunstancias, y finalmente se registran 3 casos de asesinatos contra personas presuntamente integrantes de 'Los Doce Apóstoles'.

### **La audiencia preparatoria** 

En esta audiencia, manifiesta Garzón, se van a debatir las pruebas que solicitó la fiscalía y la defensa, ya que estas pueden ser validadas durante el juicio, sin necesidad de que el testigo las reafirme durante la etapa procesal. **No obstante, considera el abogado, es posible que la defensa de Uribe, nuevamente llame a cerca de 100 testigos, que ya han dado su declaración, con el fin de dilatar el proceso.** "Este podría ser un juicio muy largo donde comparezcan decenas de testigos, aunque se espera que eso mismo sirva para develar la verdad", concluye.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
