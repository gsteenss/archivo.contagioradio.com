Title: "Hoy sembramos vida" acto de entrega de desaparecidos en Medellín del Ariari
Date: 2018-09-11 09:43
Author: AdminContagio
Category: Sin Olvido
Tags: desparecidos, medellin del ariari, restos
Slug: entrega-desaparecidos-medellin-ariari
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/09/WhatsApp-Image-2018-09-08-at-11.11.52-AM-770x400-1.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Comunidad 

###### 10 Sep 2018 

El sábado 8 de septiembre a las 8:30 de la mañana, en el Templo Santuario a las víctimas, en Medellín del Ariari (Municipo de El Castillo, Meta), se realizó la entrega digna de los restos mortales de Germán Herrera Peña, Yanira Acosta Maldonado y Ovirnes Sepúlveda Rodríguez, quienes habían sido desaparecidos y asesinados.

En una ceremonia dolorosa pero afectuosa, finalmente tres familias recibieron a sus parientes después de más de 18 años sin conocer su paradero, en procura de darles una sepultura digna y pasar por el proceso vital del luto y el duelo por su pérdida.

Durante el evento se evidencio que la búsqueda de las víctimas de desaparición es larga y la entrega de los cuerpos debe ser exhaustiva. La Fiscalía General de la Nación intervino agradeciendo a los familiares, indicando que gracias a ellos es posible que se materialice el esfuerzo para la entrega digna de las víctimas.

Durante la ceremonia se realizó una presentación simbólica con un mándala en el Templo; afirmando la frase “Hoy Sembramos Vida”, también los familiares y allegados, fueron invitados a escribir una palabra o frase que quisieran ofrecer a las víctimas y ubicarlas en las raíces de la mándala, en memoria y honor de las mismas. El homenaje y el actor religioso fue encabezado por el padre Henry Ramírez Soler.

Ocasionalmente, el padre ha indicado que recuperar el cuerpo de un ser querido significa iniciar un proceso de duelo, pero al mismo tiempo dar fin a una incertidumbre, porque formaliza mental, afectiva y físicamente el momento de la muerte, y que frente a este hecho de violencia, el Estado aún no tiene y no quiere pretender de la capacidad para encontrar a las personas desaparecida y brindar la verdad, la justicia y la reparación.

\
