Title: Las propuestas de los Wayúu para superar crisis humanitaria en La Guajira
Date: 2016-08-04 15:42
Category: Ambiente, Nacional
Tags: Gobierno Nacional, ICBF, indigenas wayuu, La Guajira
Slug: las-propuestas-de-los-wayuu-para-superar-crisis-humanitaria-en-la-guajira
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/08/La-Guajira-e1480359645641.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Defensoría del Pueblo 

###### [4 Ago 2016] 

Si bien, los niños y las niñas de la comunidad Wayúu de La Guajira están muriendo en muchos casos por desnutrición y la deficiencia de los programas de alimentación por parte del Instituto Colombiano de Bienestar Familiar, ICBF, se trata realmente de **una situación que padecen todos los indígenas por la falta de articulación de las instituciones estatales** para enfrentar las causas estructurales por las cuales también están muriendo adultos y ancianos, como lo denuncia Matilde López Arpushana, líder wayúu de Rioacha.

Teniendo en cuenta que las Autoridades Tradicionales no han  encontrado respuestas en sus territorios, decidieron desplazarse a Bogotá esta semana  con el objetivo de exigir acciones por parte del  Gobierno Nacional  para que se solucione la actual crisis humanitaria que atraviesa el pueblo indígena.

**“Es un problema estructural, el gobierno no le ha dado la importancia, ni ha dimensionado la magnitud de este problema que afecta a la mitad de la población de La Guajira**”, expresa Matilde López, quien agrega “Cuando no hay agua no hay nada, la soberanía alimentaria se afecta tanto para niños como adultos y ancianos”.

Las comunidades Wayúu insisten en que esta problemática no se puede reducir a una sola institución como lo es el ICBF, de lo que se trata es de garantizar la vida de la población de forma integral, garantizando primordialmente el agua en las cantidades necesarias para poder cultivar.

En ese sentido, proponen que se construyan al menos 10 reservorios del agua del Río Ranchería cuando este alcanza sus más altos niveles, pues actualmente se están perdiendo cantidades enormes de agua que finalmente están llegando al mar. Así mismo, creen esencial que se permita el paso del agua que se encuentra concentrada tras la construcción del proyecto de la **represa del río Ranchería, inaugurada a finales del 2010, y que únicamente le está sirviendo a los grandes arroceros y a la multinacional Carbones de El Cerrejón, dejando** **sin el líquido vital a las comunidades, como ya lo había denunciado en el 2014 la Procuraduría.**

Por otra parte, piden que haya vías de acceso pues esto ha dificultado que los padres puedan llevar al médico a sus niños y además no ha permitido el ingreso de ayudas a territorios Wayúu. También evidencian barreras lingüísticas cuando llegan a la ciudad por lo que exigen que el gobierno entienda las características de la población indígena por medio de un **enfoque diferencial.**

Al ver que sus propuestas no han sido atendidas por el gobierno, luego de haberse reunido este miércoles con el ICBF, los líderes indígenas buscan una reunión con el Ministerio del Interior **para que las acciones jurídicas a favor del pueblo Wayúu, como las  medidas cautelares y tutelas,** “se traduzcan en acciones programáticas reales y coherentes”, dicen.

“**Si los Wayúu nos tomamos la vía férrea es porque nos cansamos de la situación. Para las multinacionales es para lo que sirve nuestro departamento, para que lo saqueen y lo saqueen**”, expresa la líder Wayúu, y agrega  que de no ser un pueblo es pacífico, ya se hubieran tenido que ir las empresas.

<iframe src="http://co.ivoox.com/es/player_ej_12442753_2_1.html?data=kpehlpebeZShhpywj5eXaZS1k5Wah5yncZOhhpywj5WRaZi3jpWah5ynca7V1c7ZxsqPkIa3lIqvldXJvoy109Xi1c3FssKZk6iYzoqnd4a1pcnS1JDbpdqZpJiSpKbZb46ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
