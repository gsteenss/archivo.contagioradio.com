Title: Fuerte represión del ESMAD contra comunidad de San Martín que se opone al Fracking
Date: 2016-10-19 15:07
Category: Ambiente, Nacional
Tags: defensa ambiental, Fracking colombia, Represión del ESMAD
Slug: fuerte-represion-del-esmad-contra-comunidad-de-san-martin-que-se-opone-al-fracking
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/10/san_martin2.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Justicia Ambiental] 

###### [19 Oct 2016] 

Por lo menos 10 personas heridas y un centenar de afectados ha dejado la jornada de **represión del ESMAD contra la protesta de los habitantes de San Martín en Cesar, que se oponen a la entrada de maquinaria de la empresa Conoco Phillips en su territorio.** Desde tempranas horas de la mañana más de mil personas pretenden bloquear la entrada de maquinaria y el paso del ESMAD hacia la vereda "Cuatro Bocas".

Según la denuncia que han realizado los habitantes de San Martín a través de las redes sociales, l**a policía orientó su acción en contra de un grupo de estudiantes de secundaria que impedían el paso a la policía para evitar lesiones a las personas** que, en otro punto del municipio impedían la entrada de maquinaria. En ese primer momento el ESMAD provocó heridas en dos menores de edad.

Desde el pasado 14 de Octubre, habitantes de San Martín Cesar se encontraron la resolución 674, emitida por la Alcaldía municipal que **autorizó la presencia del ESMAD para[ facilitar el ]inicio de las actividades de exploración en el pozo Pico Plata 1**, que pretende adelantar la empresa Conoco Phillips Colombia Ventures Ltd. Sucursal Colombia. Le puede interesar: [San Martín se opone al fracking a pesar de presencia del ESMAD.](https://archivo.contagioradio.com/san-martin-se-opone-al-fraking-a-pesar-de-presencia-del-esmad/)

Julia Figueroa, abogada defensora de Derechos Humanos de la Corporación Colectivo de Abogados Luis Carlos Pérez, quien se encuentra acompañando a estas comunidades, denuncia que “el nivel de violación de Derechos Humanos es muy alto” además denunció que **la Comisaría de Familia amenazó a los estudiantes por manifestarse, “les dijeron que van a tener procesos disciplinarios en sus colegios”** y que los padres de familia también tendrían sanciones.

El colectivo denuncia que días anteriores, se decretó ley seca y toque de queda, además se anunció que “El Hospital Álvaro Ramírez Gonzales (...) se mantendrá en situación de alerta amarilla, con el fin de atender cualquier hecho, a partir de las 6:00 pm del día 17 de octubre de 2016, hasta el cumplimiento del procedimiento administrativo y policivo en cumplimiento de la Resolución 674”, llaman la atención en que** “la Alcaldía da por hecho que la vida e integridad física de las personas se pondrá en riesgo”.**

Figueroa manifiesta que aunque se han generado alertas y hay acompañamiento de defensoría del pueblo, no se ha emitido una decisión de suspensión de este proyecto, ni de la intervención del ESMAD. Añade que el 18 de Octubre “hubo hostigamientos, fotografías y filmaciones por parte de policías, para atemorizar a la comunidad”, además, se registró la **“presencia de hombres en motos sin placas y armados que no quisieron identificarse, esa ha sido la respuesta a la protesta pacifica de la comunidad”. **Le puede interesar: [El prontuario del ESMAD.](https://archivo.contagioradio.com/el-prontuario-del-esmad/)

Por otra parte, subraya que el colectivo ha realizado **“talleres rápidos de defensa de Derechos Humanos con la comunidad”** para que puedan tener respuesta oportuna ante estas violaciones por parte de la Fuerza Pública, y esperan que haya celeridad para “una decisión judicial, frente al acto administrativo, porque esta intervención es ilegal”.

###### Reciba toda la información de Contagio Radio en su correo [[bit.ly/1nvAO4u]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por Contagio Radio [[bit.ly/1ICYhVU]{.s1}](http://bit.ly/1ICYhVU) {#reciba-toda-la-información-de-contagio-radio-en-su-correo-bit.ly1nvao4u-o-escúchela-de-lunes-a-viernes-de-8-a-10-de-la-mañana-en-otra-mirada-por-contagio-radio-bit.ly1icyhvu .p1}
