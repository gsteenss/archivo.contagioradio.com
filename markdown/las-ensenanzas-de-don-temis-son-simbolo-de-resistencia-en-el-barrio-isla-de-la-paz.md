Title: Las enseñanzas de 'don Temis' son símbolo de resistencia en el barrio Isla de la Paz
Date: 2020-01-29 17:28
Author: CtgAdm
Category: Líderes sociales, Memoria
Tags: buenaventura, Paro Cívico de Buenaventura, Temístocles Machado
Slug: las-ensenanzas-de-don-temis-son-simbolo-de-resistencia-en-el-barrio-isla-de-la-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/01/don-temis.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:html -->

<figure class="wp-block-audio">
<audio controls src="https://co.ivoox.com/es/janer-panameno-habitante-isla-paz-y_md_47282655_wp_1.mp3">
</audio>
  

<figcaption>
Entrevista &gt; Janer Panameño | Habitante Isla paz - Amigo de Temistocles Machado

</figcaption>
</figure>
<!-- /wp:html -->

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: Temístocles Machado ONU

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify","textColor":"very-dark-gray","fontSize":"normal"} -->

Durante los pasado 27 y 28 de enero, la comunidad del[barrio Isla de la Paz](https://twitter.com/Movicecol/status/1221840822729158663) en Buenaventura, conmemoró dos años del asesinato de **Temístocles Machado, líder del paro cívico de Buenaventura** quien en vida emprendió una lucha en defensa del territorio y que hoy pese a no estar, se ha convertido en un símbolo de resistencia para quienes hoy continúan protegiendo su tierra de empresarios y grupos armados.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

A propósito del homenaje, Janer Panameño, habitante de Isla de Paz y amigo cercano al líder social, lo recuerda como un hombre apasionado con una vocación nata de defender los derechos de las comunidades y de ver cómo los jóvenes podían seguir construyendo en medio de la violencia y despojo territorial.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

A dos años de su homicidio y aunque Jorge Luis Jaramillo, autor material de los hechos fuera condenado a 17 años de prisión por los delitos de homicidio agravado, concierto para delinquir y porte ilegal de armas, la comunidad y quienes compartieron junto a Temístocles señalan que no se han dado a conocer los nombres de los autores intelectuales, lo que implica que la comunidad aún continúe en riesgo. [(Le puede interesar: Verdad en el asesinato de Temístocles Machado quedó a medias)](https://archivo.contagioradio.com/verdad-temistocles-machado-quedo-medias/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Janer afirma que la partida de 'Don Temis' ha sido un asunto doloroso, sin embargo "también nos conforta que toda esa lucha y esa entrega está en cada uno de nosotros, que su cuerpo no está pero que **sigue presente en cada uno de los habitantes del barrio La Paz y de toda Buenaventura**. [(Lea también: Familiares del líder Temístocles Machado continuarán con su legado)](https://archivo.contagioradio.com/familiares-de-temistocles-machado-lider-social-asesinado-continuaran-con-su-legado/)

<!-- /wp:paragraph -->

<!-- wp:quote -->

> "Fue un hombre incansable que tenía un sueño de seguir rescatando los valores que muchas por las dinámicas delincuenciales arrebatan a veces a muchos jóvenes"

<!-- /wp:quote -->

<!-- wp:heading {"level":3} -->

### Extrañamos a 'don Temis' pero valoramos su entrega

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

La jornada, que contó con proyecciones a la memoria del líder social y reunió a las personas alrededor de anécdotas sobre la vida de 'don Temis', fue un momento para valorar cómo por más de 35 años desempeño una lucha por el territorio y por la vida junto a diversas generaciones, demostrando una **"destreza de fortalecer vínculos. Su mensaje y sus enseñanzas de cómo seguir luchando, esas son las razones de seguir defendiendo el territorio y de quienes quieren despojarnos"**.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

A su vez, Janer destaca que aunque su vacío juega un papel importante su vacío, se ha vuelto una fortaleza en medio de la debilidad a través de su legado y sus enseñanzas. [(Le puede interesar: Amenazan a dos líderes sociales del paro Cívico de Buenaventura)](https://archivo.contagioradio.com/amenazan-a-dos-lideres-sociales-del-paro-civico-de-buenaventura/)

<!-- /wp:paragraph -->

<!-- wp:quote -->

> "Si él lo hizo por nosotros, toca continuar con lo que nos enseñó para seguir viviendo y resistiendo, porque si él no lo hubiera hecho no estaríamos aquí. A pesar que él no está, nos inyectó esa revolución de seguir defendiendo el territorio, ahí nos acordamos que Temis está con nosotros presente" - Janer Panameño.

<!-- /wp:quote -->

<!-- wp:heading {"level":3} -->

### "El territorio no se vende sino que se defiende"

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

En medio del continuo hostigamiento por parte de empresas como Inversiones TOCAM hacia la población, la comunidad relata que el 2019 fue derribada una casa de una familia el 31 de julio de 2019 en un intento de apoderarse el territorio, **"Por cada acción que hacemos como comunidad nos obstruyen el paso, pero a nosotros no nos importa, nos paramos en el espacio que quieren para la ampliación de su puerto y seguimos denunciando"**, relata.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Parte de esta resistencia, relata el habitante de Isla de Paz se concentra en realizar mingas comunitarias como respuesta a los embates de quienes quieren apoderarse de su tierra; "cuando derriban las canchas para convertirlas en bodegas, la comunidad sale para reconstruir, participan los niños, jóvenes, adolescentes, ancianos (...) reiterando que estamos vivos y que estamos presentes como nos enseño 'don Temis', fortaleciendo nuestros derechos colectivos e individuales.

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
