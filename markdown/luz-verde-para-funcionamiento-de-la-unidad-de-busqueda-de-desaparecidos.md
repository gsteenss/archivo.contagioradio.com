Title: Luz verde para funcionamiento de la Unidad de Búsqueda de Desaparecidos
Date: 2018-02-16 14:07
Category: Nacional, Paz
Tags: Luz Marina Monzon, Unidad de Busqueda, Unidad de Búsqueda de Desaparecidos
Slug: luz-verde-para-funcionamiento-de-la-unidad-de-busqueda-de-desaparecidos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/08/desaparecidos-dia-e1504117350695.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: MOVICE Antioquia] 

###### [16 Feb 2018] 

Ya fueron expedidos los decretos que hacían falta para que empiece a funcionar la **Unidad de Búsqueda de Personas dadas por Desaparecidas** en el contexto y en razón del conflicto armado. Su directora, Luz Marina Monzón, se posesionará el martes 20 de febrero y espera cumplir con las expectativas de las víctimas para encontrar a sus seres queridos.

De acuerdo con Monzón, el proceso para la creación y el funcionamiento de la Unidad “ha sido desafiante en la medida en que ha tenido unas lógicas con las que no estoy familiarizada”. Esto teniendo en cuenta los **tramites institucionales y burocráticos** en donde “no se entiende por qué no se pueden dar las respuestas que uno necesita en el tiempo que se solicitan”.

### **Decretos crean una planta de trabajo provisional** 

Sin embargo, reconoce que ha llegado el momento para lograr “salir adelante con la formulación de unos decretos que respondan a las **necesidades del tipo de institución** que se está creando”. Recordó que estos decretos se desarrollaron para la creación de una planta provisional de la Unidad que se acordó con el Gobierno Nacional.

Esto se hizo para no caer en ninguna equivocación “cuando se está buscando crear una institución que puede **quedar técnicamente muy bien**, pero a la hora de la operación llegue a ser ineficiente”. Por esto, desde la Unidad han pensado tener un equipo inicial de 14 personas que puedan crear diferentes estrategias para diseñar una institución definitiva. (Le puede interesar:["Las recomendaciones de las víctimas para la Unidad de Búsqueda de Desaparecidos"](https://archivo.contagioradio.com/las-recomendaciones-de-las-victimas-para-el-funcionamiento-eficaz-de-la-uniddad-de-busqueda-de-desparecidos/))

### **Para diseño de la Unidad se tendrá en cuenta a las víctimas ** 

En cuanto a la participación de las organizaciones sociales y de las víctimas en la Unidad de Búsqueda de Personas dadas por Desaparecidas, Monzón recalcó en que se deben tener en cuenta **sus recomendaciones**. Esto dado que ya hay unas propuestas que involucran las expectativas de las víctimas en cuanto a cómo debe ser la institución.

Además, enfatizó en la importancia de poder desarrollar reuniones de trabajo para “pensar cuáles son las mejores maneras de diseñar la institución”. Se espera que lo que se diseñe, corresponda con las **viabilidades institucionales** pues, “uno puede pensar que las cosas son de una manera, pero cuando llegan a la institucionalidad hay diferentes opiniones”. (Le puede interesar:["Unidad de Búsqueda tendrá que encontrar a más de 50.000 desaparecidos"](https://archivo.contagioradio.com/unidad-de-busqueda-tendra-que-encontrar-a-mas-de-50-000-desaparecidos-en-el-pais/))

Finalmente, en lo que tiene que ver con los recursos que tiene disponible la Unidad para su funcionamiento, la directora indicó que en el diseño se debe identificar el presupuesto que va a requerir la entidad y “en ese momento sabremos si es viable o no”. Hizo alusión a lo estipulado por la vice Ministra de Hacienda quien recordó que hay **22 mil millones** destinados para el funcionamiento de la Unidad para este año.

Sin embargo, Monzón recalcó que esa cifra no se ha visto en el presupuesto debido a que la entidad como tal, en términos institucionales, **no se ha creado.** Por esto, hasta que esto no ocurra no se va a ver reflejado el presupuesto.

<iframe id="audio_23829693" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_23829693_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]

<div class="osd-sms-wrapper">

</div>
