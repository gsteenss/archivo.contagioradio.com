Title: Acuerdo final de Paz entre gobierno y FARC
Date: 2016-08-25 12:15
Category: Otra Mirada, Paz
Tags: acuerdo final, conversaciones de paz, Cuba, FARC, Humberto de la Calle, Iván Márquez
Slug: acuerdo-final-de-paz-entre-gobierno-y-farc
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/08/saludo-jefes-delegaciones.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: vanguardia.cu] 

###### [25 Ago 2016] 

El acuerdo final, calificado como histórico, ya está publicado y seguramente será un texto de consulta, no solamente para la etapa de implementación, sino como parte de 4 años de trabajo que cambian la historia del país. El documento final cuenta con 297 páginas y consta de prólogo, introducción, acuerdos y firmas.

Aquí lo podrán consultar en su integridad...

[Acuerdo Final Entre Gobierno y FARC](https://es.scribd.com/document/322167223/Acuerdo-Final-Entre-Gobierno-y-FARC#from_embed "View Acuerdo Final Entre Gobierno y FARC on Scribd")

<iframe id="doc_84116" class="scribd_iframe_embed" src="https://www.scribd.com/embeds/322167223/content?start_page=1&amp;view_mode=scroll&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="undefined"></iframe>  
 
