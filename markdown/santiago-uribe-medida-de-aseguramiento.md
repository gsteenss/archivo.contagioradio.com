Title: Niegan solicitud de defensa de Santiago Uribe para que recobrara la libertad
Date: 2017-08-11 15:30
Category: Judicial, Nacional
Tags: 12 apostoles, Antioquia, Santiago uribe
Slug: santiago-uribe-medida-de-aseguramiento
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/06/Santiago-Uribe-Centro-Tampa-e1497306434666.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: centro tampa] 

###### [11 Ago 2017]

El Juzgado Primero Penal Especializado de Antioquia resolvió que no se puede sustituir la medida de aseguramiento en establecimiento penitenciario a favor de Santiago Uribe, dado que el proceso sigue su curso y **no se cumplen los causales por los cuales se podría sustituir la medida de aseguramiento.**

En una decisión conocida este 11 de Agosto, el juez Jaime Herrera Niño, rechazó la solicitud de Jaime Granados, defensor de Santiago Uribe. En la decisión el juzgado asegura que no se dan por cumplidos los presupuestos para dicha suspensión y además decidió prorrogar la medida por un año más.

Según la decisión los presupuestos para mantener la medida de aseguramiento contra Uribe Vélez se mantienen “indemnes” **por lo tanto no se puede remplazar por una medida sustitutiva**. En el documento se especifica que los causales para aplicar la medida de aseguramiento son que no se obstruya la justicia, que el imputado represente un peligro para la sociedad y prevenir que el imputado no se presente al proceso de juicio.

### **El proceso de juicio continúa con santiago Uribe detenido** 

Santiago Uribe, quien tiene medida de aseguramiento desde el 29 de Febrero de 2016, afronta un proceso de juicio por su participación y determinación en múltiples crímenes cometidos por el grupo paramilitar conocido como **“los doce apóstoles” que operó en varios municipios del departamento de Antioquia en la década de los 90’s.** [Lea tambien: Se abre posibilidad de justicia para las víctimas de los 12 apóstoles](https://archivo.contagioradio.com/se-abre-la-posibilidad-de-justicia-para-victimas-de-santiago-uribe/)

Así las cosas el proceso de juicio seguirá su curso y el imputado deberá seguir detenido mientras se surten las demás etapas, aunque la decisión del juzgado puede ser apelada por la defensa del procesado.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
