Title: ¿Se ha convertido la Fiscalía en una herramienta de poder para el Gobierno?
Date: 2020-06-09 21:34
Author: CtgAdm
Category: Actualidad, Judicial
Tags: Fiscalía, Francisco Barbosa
Slug: se-ha-convertido-la-fiscalia-en-una-herramienta-de-poder-para-el-gobierno
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/06/Barbosa.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: Fiscalía/ Presidencia

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

La semana pasada **se hicieron efectivas las capturas del mayor Yeferson Fabián Tocarruncho Parra y del investigador Wadith Miguel Velásquez García funcionarios de Policía Judicial que realizaron las interceptaciones a José Guillermo “El Ñeñe” Hernández** donde se hablaba de presuntos dineros ilícitos para la campaña del actual presidente Iván Duque, pues según la Fiscalía los dos investigadores habrían requerido que se incluyera en la misma solicitud de interceptación 5 teléfonos de miembros de la Policía que no estaban vinculados al caso.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

La decisión ha causado suspicacia en varios sectores, que aseguran que la Fiscalía se está utilizando como un instrumento de persecución. Cabe recordar que hace pocos días se produjo una orden de captura en contra del exgobernador de Antioquia, Anibal Gaviria y también que el Fiscal Francisco Barbosa anunció la apertura de una “beta investigativa” en contra de la campaña del excandidato presidencial, Gustavo Petro al ser supuestamente mencionado en uno de los audios del Neñe. Estos hechos han acentuado las dudas –especialmente por los momentos en que se dieron— sobre el ente investigador y particularmente sobre la persona del Fiscal Barbosa.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

En contraste con la proactividad en la que actuó en estos casos, **se cuestiona la inoperancia de la Fiscalía para capturar a Claudia “Cayita” Daza y por el contrario permitirle salir libremente del país días antes de que fuera vinculada al proceso** pese a estar directamente implicada en los audios del Ñeñe. [(Lea también: Decir que en La Guajira no se compran o venden votos es querer tapar el sol con un dedo)](https://archivo.contagioradio.com/la-guajira-compran-o-venden-votos-tapar-sol-con-un-dedo/)

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Conflictos de interés

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

A las denuncias de persecución, se suman las de conflicto de intereses que recaen sobre el fiscal Barbosa por haber participado supuestamente de la campaña del entonces candidato Iván Duque en la pasada elección presidencial y por también ser reseñado como amigo cercano del hoy mandatario. 

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Basados en estos argumentos, organizaciones como [Transparencia por Colombia y Dejusticia](https://transparenciacolombia.org.co/2020/03/16/fiscalindependiente/) **han solicitado al propio Fiscal Barbosa que se declare impedido para “supervisar la investigación penal relacionada con las denuncias de presuntas irregularidades en la campaña presidencial de Iván Duque”** y en el mismo orden a la Corte Suprema de Justicia para que sea recusado por el conflicto de intereses que según estas organizaciones se configura con la presencia de Barbosa en el caso.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

En el mismo sentido el abogado Miguel Ángel del Río, quien reveló los audios del "Ñeñe" Hernández y actualmente funge como representante de las víctimas en dicho caso, **pidió a los magistrados de la Corte Suprema que nombren un fiscal *ad hoc*, ya que Francisco Barbosa, como amigo personal del presidente Duque, no sería garantía de imparcialidad.** [(Le puede interesar: "Fiscal Barbosa está encubriendo responsables de masacre en cárcel La Modelo" Uldarico Florez)](https://archivo.contagioradio.com/fiscal-barbosa-esta-encubriendo-responsables-de-masacre-en-carcel-la-modelo-uldarico-florez/)

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### **El Antecedente en la Fiscalía con Néstor Humberto Martínez**

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

La actuación del Fiscal Barbosa al frente del[ente investigador](https://twitter.com/FiscaliaCol) rememora la de su antecesor Néstor Humberto Martínez Neira sobre quien también recayeron serios cuestionamientos. **Cabe recordar que el exfiscal Martínez tuvo que ser recusado por la Corte Suprema ante su negativa para apartarse del caso Odebrecht** sobre el que muchos sectores políticos y ciudadanos le atribuyeron un serio conflicto de interés. Esto llevó a la elección del fiscal *ad hoc*, Leonardo Espinosa quien estuvo al frente del caso hasta la renuncia de Martínez Neira en mayo del año pasado.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

En caso de prosperar la recusación que se ha instaurado en contra del actual Fiscal Barbosa, se estaría configurando exactamente la misma situación que con su antecesor, pues tendría que nombrarse un fiscal *ad hoc* para que conozca exclusivamente de los hechos relacionados con las presuntas irregularidades en la campaña presidencial de Iván Duque dadas a conocer a la opinión pública mediante las grabaciones del Ñeñe Hernández.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

**No obstante, cabe anotar que el fiscal «*ad hoc*» que eventualmente se llegara a nombrar, sería de una terna integrada por el mismo Presidente de la República** lo que podría no reslver los conflictos y cobijaría sus decisiones bajo el mismo manto de duda que hoy recae sobre Barbosa.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Por todo lo anterior, es que se plantea urgente el desarrollo de una reforma política estructural que evite esa relación inconveniente y nociva entre poderes que propicia el sistema actual, donde el Senado es quien elige al Fiscal General de una terna integrada por el Presidente de la República. Aunque valga decir que ello requeriría una reforma constitucional y la convergencia de una porción mayoritaria de los diversos sectores políticos presentes en el Congreso para su consolidación. [(Le puede interesar: En Colombia no hay postconflicto, en Colombia hay un Narcoestado Paramilitar)](https://archivo.contagioradio.com/en-colombia-hay-un-narcoestado-paramilitar/)

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->
