Title: Unidad de Investigación y Acusación de la JEP sobrepasa sus límites
Date: 2019-10-25 11:36
Author: CtgAdm
Category: Paz
Tags: FARC, JEP, Sonia, Unidad de investigación y acusación
Slug: unidad-de-investigacion-y-acusacion-de-la-jep-sobrepasa-sus-limites
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/Fotos-editadas1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: JEP  
] 

El pasado martes 22 de octubre, la **Unidad de Investigación y Acusación (UIA)** de la Jurisdicción Especial para la Paz (JEP) solicitó la expulsión de la excombatiente **Anayibe Rojas Valderrama, alias 'Sonia',** de este mecanismo de justicia transicional. Ese mismo día más tarde, la Jurisdicción decidió no expulsarla tomando en cuenta que no había pruebas de que Sonia haya regresado a las armas, lo que al tiempo, puso en cuestión el trabajo que viene desarrollando la Unidad de Investigación.

> ?|| La JEP se abstiene de abrir incidente contra Omaira Rojas, exintegrante de las Farc.  
> La Sala de Amnistía o Indulto asegura que el fiscal de la Unidad de Investigación y Acusación ([\#UIA](https://twitter.com/hashtag/UIA?src=hash&ref_src=twsrc%5Etfw)), que pidió abrir el incidente, no pudo demostrar indicios de graves incumplimientos. [pic.twitter.com/gSdoP7Lpnp](https://t.co/gSdoP7Lpnp)
>
> — Jurisdicción Especial para la Paz (@JEP\_Colombia) [October 22, 2019](https://twitter.com/JEP_Colombia/status/1186696303239204865?ref_src=twsrc%5Etfw)

<p>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
</p>
### **"La Unidad sobrepasa sus límites y competencias"** 

**Diego Martínez, abogado de Anayibe Rojas**, explicó que **la solicitud de expulsión ocurrió de manera "poco rigurosa"**, porque la UIA es una entidad dentro de la JEP que no es judicial y sin embargo, inició una investigación ordinaria contra Rojas, con la intención de probar que ella se encontraba participando en el alzamiento armado de Iván Márquez y un grupo de guerrilleros, que fue dado a conocer para los días finales de agosto.

Martínez aseguró que la actuación se tomó de manera arbitraria, "porque ellos no tienen funciones de investigación ordinaria", pero se tomaron "la atribución de investigar temas a cargo de la Fiscalía". Adicionalmente, cuestionó las deficiencias técnicas con que se decidió pedir la expulsión de su defendida, puesto que la misma se basó en un supuesto cotejo morfológico "que no es tal", y "con base en un informe de un investigador que determinó que Sonia" es una persona que aparece en el vídeo de Márquez, pero no tiene una semejanza siquiera del 20% para realizar tal afirmación.

En cambio, el Abogado señaló que Rojas Valderrama en este momento se encuentra culminando sus estudios de secundaria, y pagó 16 años de prisión en Estados Unidos, "por tanto su pena ya está cumplida"; también añadió que **está apoyando el proceso de reincorporación, porque sigue visitando a miembros de FARC que siguen en la cárcel y no han recobrado su libertad,** por lo que concluyó que la Unidad en este caso "sobrepasa sus límites y competencias".

### **¿Por qué la Unidad sobrepasó sus límites?** 

El abogado declaró que esta situación debía analizarse de una forma sistémica, y recordó que el Gobierno Nacional, "en cabeza del consejero para la estabilización, Emilio Archila y la ministra de interior, Nancy Patricia Gutiérrez solicitaron al partido FARC que expulsaran a Márquez y un grupo de personas que decían que estaban delinquiendo pero era una información falsa", por lo tanto, dijo que "hay intereses de sectores del partido de Gobierno en generar una matriz de incumplimiento por parte de integrantes de FARC". En opinión del Jurista, integrantes al interior de la UIA responderían a esos intereses.

Martínez también afirmó que la Unidad ha intentado en otras oportunidades proclamarse autónoma, lo que es "bastante grave, porque está supeditada al imperio de las decisiones de la Ley" y en aras de responder a la Jurisdicción. Dicha autonomía habría recibido un espaldarazo en el Proyecto de Ley del Plan Nacional de Desarrollo, que incluyó un artículo para darle autonomía administrativa. (Le puede interesar: ["Ante la JEP buscan justicia para casos de ejecuciones extrajudiciales en Arauca"](https://archivo.contagioradio.com/ante-la-jep-buscan-justicia-para-casos-de-ejecuciones-extrajudiciales-en-arauca/))

Otra irregularidad mencionada por el Abogado tiene que ver con la propia entrada en funcionamiento de la Unidad, pues se supondría que **debería empezar plenamente sus labores cuando hayan procesos de no reconocimiento de verdad en la Jurisdicción, situación que aún no ha ocurrido,** pero desde ya el fiscal que la dirige "ha solicitado un cuerpo de fiscales y policía judicial exorbitante". La preocupación ante todos los hechos mencionados, concluyó Martínez, es "que se quiera afectar los principios de la JEP".

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe><iframe id="audio_44015597" frameborder="0" allowfullscreen scrolling="no" height="200" style="border:1px solid #EEE; box-sizing:border-box; width:100%;" src="https://co.ivoox.com/es/player_ej_44015597_4_1.html?c1=ff6600"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
