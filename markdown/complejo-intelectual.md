Title: Complejo Intelectual
Date: 2017-09-21 07:48
Category: Opinion
Tags: Intelectual, izquierda colombiana
Slug: complejo-intelectual
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/09/INTELECTUAL.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### **Foto:freepik** 

#### **Por: Ángela Galvis Ardila** 

###### 21 Sep 2017 

Siempre asimilé la palabra pose a un asunto físico y sobre todo a la forma en que nos disponíamos a salir en una foto. Posudo era un calificativo muy cercano a creído, o, para usar una palabra coloquial, a escamoso. Pero el tiempo me fue enseñando que existe una pose menos mundana, por decirlo de alguna manera: la pose intelectual. Esa que nos hace tener una respuesta para todo con la mano en la barbilla y casi que mirando a lontananza. Esa que nos quita la posibilidad de equivocarnos, de ser frívolos, de decir no sé o no me importa; esa que nos conduce al vicio de sentirnos admirados, halagados y a revolcarnos cuando no tenemos el aplauso que creemos merecer y que es un beso directo al ego.

No sé en qué momento, o si siempre ha sido así, empezamos a creer que usar una jerga rebuscada o citar a autores que quizás tan solo hemos leído unos cuantos párrafos, nos hace intelectuales, aun cuando la referencia que traemos a colación no resulte significativa para el contexto de lo que queremos transmitir. Pero claro, eso no importa, lo verdaderamente importante es imprimirle un toque de clasismo a lo que se dice o se escribe, buscando una especie de distinción frente a quien escucha o frente a quien lee, lo cual no es otra cosa que la vanidad en su máxima expresión, vanidad que como a los bichos del estómago, es necesario purgar de vez en cuando.

Y no estoy diciendo que todo intelectual pose falsamente, no, ni más faltaba. O que porque alguien cite, por ejemplo, a Pushkin, ello signifique que es un farsante; tampoco. O que escuchar a Bach sea de por sí una impostura; jamás. Lo que digo o pretendo decir es mucho más básico: si cualquier comentario o actitud que digamos o hagamos responde a algo diferente que a nuestro querer genuino de transmitir un conocimiento o a un gusto auténtico, definitivamente estamos tomando una ilusoria pose de superioridad intelectual sobre los demás, sobre esos que solemos pensar no están a nuestra altura y por eso creemos que nuestra función en esta vida es “educarlos”, haciéndoles conocer la lista de libros, películas, viajes, lugares, música, comida, licores, etc, que recomendamos merced a nuestra vasta y privilegiada vida intelectual que hemos ido recopilando y que, al menor descuido, exponemos sin recato alguno y hasta forzando la conversación a ello, porque no importa el lugar, ni las circunstancias, al extremo que hasta un velorio puede convertirse en un espacio adecuado para sacar a relucir ese perverso sistema binario que en nuestro mundo elevado hemos ido construyendo: inteligente/bruto, culto/ignorante, exitoso/fracasado. Y ni hablar de cuando se escribe en un medio de comunicación o se aparece en televisión y nos hacemos fotografiar o grabar delante de una enorme biblioteca, como si la hubiéramos escrito o siquiera leído; de una infinita colección de música, como si la hubiéramos compuesto, o de una gigantesca cava de vinos, como si la hubiéramos producido.

El conocimiento, la cultura, la intelectualidad, la sabiduría, se aprecian mejor cuando se demuestran en tono y perfil bajos, sin aspavientos, cuando se respeta al otro y no se le mira por encima del hombro, cuando no se alardea de poseerlas, cuando se alejan de la arrogancia, cuando se expresan sin desdén por el que carece de ellas, en fin, cuando no tienen nada que ver con ese complejo intelectual que está al acecho como un fantasma para atacar y que fue justamente el que me hizo citar atrás a Pushkin sin siquiera haberlo leído.

#### **Leer "¿[Para Cuándo](https://archivo.contagioradio.com/para-cuando/)?" de  Ángela Galvis Ardila**

------------------------------------------------------------------------

###### Las opiniones expresadas en este artículo son de exclusiva responsabilidad del autor y no necesariamente representan la opinión de Contagio Radio.
