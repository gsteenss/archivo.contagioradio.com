Title: Consulta popular en Pasca sigue viva
Date: 2017-07-26 13:54
Category: Ambiente, Nacional
Tags: Consejo de Estado, consulta popular, hidrocarburos, pasca
Slug: consulta-popular-en-pasca-sigue-viva
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/07/pasca-e1501095252207.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Mapio.net] 

###### [26 Jul 2017] 

Las comunidades de Pasca en Cundinamarca recibieron con preocupación la decisión del Consejo de Estado, **quien decidió suspender el proceso de consulta popular programada para el 6 de agosto** de este año. Sin embargo, abogados han manifestado que esto hace parte de un litigio entre partes que defienden sus intereses.

La decisión del tribunal se dio como respuesta a una **acción de tutela interpuesta por la Asociación Colombiana de Ingenieros de petróleos (ACIPET).** En esta, la Asociación manifestó que la pregunta propuesta para la consulta dejaría al municipio sin la posibilidad de abastecerse “de los combustibles, gasolina y ACPM a través de carro tanques”. (Le puede interesar:["La Vega en Cauca lista para empezar proceso de consulta popular"](https://archivo.contagioradio.com/municipio-de-la-vega-en-el-cauca-mas-que-lista-para-consulta-popular/))

Es importante recordar que la pregunta era “Sí o no, están de acuerdo con que se ejecuten actividades exploratorias, sísmicas, perforaciones, explotaciones, producción y transporte de hidrocarburos en el municipio de Pasca”.

Ante esto, el abogado Rodrigo Negrete afirmó que **“hubo una interpretación errada por parte del consejo de estado y de la Asociación de Ingenieros** porque la pregunta hace referencia al transporte del crudo que sale de la producción de hidrocarburos más no a sus derivados”.

Es por esto que manifestó que el proceso de litigio **no compromete en su totalidad la realización de la consulta popular**, sino que por el contrario se debe continuar con el proceso judicial para defender los intereses de las comunidades por proteger sus territorios. Para él “es lastimosos que el Consejo de Estado haya hecho una mala interpretación de la pregunta y que haya desconocido la magnitud del asunto”. (Le puede interesar: ["Morelia Caquetá busca consulta popular"](https://archivo.contagioradio.com/morelia-caqueta-alista-proceso-para-consulta-popular-contra-hidrocarburos/))

Negrete manifestó que lo que sigue es que el Consejo de Estado **reciba la información que ha solicitado para dar claridad al tema** para poder decidir sobre la medida cautelar provisional. Mientras tanto, la consulta popular no se podrá llevar a cabo el 6 de agosto “pero esto hace parte del juego democrático que hacemos las personas que nos ceñimos a la ley y a la constitución”.

### **Ha habido una campaña de desinformación** 

Negrete manifestó que hay una campaña de desinformación por parte de los medios de comunicación. Él fue enfático en manifestar que **la consulta no ha sido cancelada del todo** pero las comunidades “han recibido información falsa con argumentos negativos que desinforman y no han recibido el mensaje real por parte de los medios”.

Finalmente, Negrete aseguró que seguirán trabajando para, si es el caso, **reformular la pregunta y poder continuar con la defensa del territorio**. “Se va a impugnar esta decisión para que pase a una segunda instancia y será el mismo Consejo de Estado quien defina el litigio”.

<iframe id="audio_20015593" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_20015593_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU).
