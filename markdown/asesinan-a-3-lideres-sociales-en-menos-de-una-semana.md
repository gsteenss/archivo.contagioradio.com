Title: Asesinan a 3 líderes sociales en menos de una semana
Date: 2018-06-13 12:46
Category: DDHH, Nacional
Tags: Antioquia, Cauca, lideres sociales, Putumayo
Slug: asesinan-a-3-lideres-sociales-en-menos-de-una-semana
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/12/lideres-sociales.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [13 Jun 2018] 

Tres asesinatos se reportaron hoy en Colombia. Se trata de Holman Mamian, integrante de ASOINCA, asesinado en el departamento del Cauca; Francisco José Guerra, líder de acción comunal de la vereda El Mandarino, en Ituango-Antioquia y Jeisson Ramírez, presidente de la junta de acción comunal de la vereda la Yet, en el Putumayo.

### **El Cauca en la mira de la violencia contra sindicalistas** 

**Holman Mamian era integrante de ASOINCA y docente de la institución educativa agropecuaria Santa Rita**, en el municipio de la Vega, Cauca. Los hechos se presentaron el pasado 12 de junio, cuando Mamian se dirigía hacia el lugar de trabajo, en donde fue abordado por hombres que posteriormente acabaron con su humanidad.

De acuerdo con Mario Daza, directivo de ASOINCA, Mamian se dedicaba a las labores sindicales que hacían parte de la organización, razón por la cual, considera que su asesinato puede estar relacionado con su activismo social. Además, señaló que estos hechos se han vuelto sistemáticos en el Cauca, **“vemos como se siguen asesinando a los dirigentes sociales y campesinos, sin que el gobierno nacional** o el gobierno departamental se inmuten”.

### **Continúan los asesinatos de líderes en Antioquia y Putumayo** 

De igual forma, las autoridades de Ituango, en Antioquia, confirmaron el asesinato del presidente de la Junta de Acción comunal de la vereda El Mandarino, Francisco Guerra. El hecho ocurrió **el pasado sábado 9 de junio cuando hombres llegaron a su vivienda y le dispararon en reiteradas ocasiones**.

Frente a estos hechos, diversas organizaciones defensoras de derechos humanos han exigido medidas urgentes en el departamento de Antioquia, debido a que con este homicidio, son 9 los defensores de derechos humanos asesinados en el territorio. (Les puede interesar:["Continúan los asesinatos de líderes sociales en Nariño"](https://archivo.contagioradio.com/nuevamente-un-lider-social-fue-asesinado-en-narino/))

Así mismo, el presidente de la Junta de Acción Comunal la Yet, **Jeisson Ramírez, fue asesinado en la zona rural del Valle del Guamez,** en el departamento del Putumayo, el pasado 10 de junio. Los hechos se presentaron cuando Ramírez se encontraba con su compañera en una tienda.

Los habitantes de la vereda manifestaron que este hecho podría estar relacionado con las diferentes acciones que realizaba Ramírez por la defensa del territorio. Sin embargo, las autoridades expresaron que están en proceso de investigación.

<iframe id="audio_26518003" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_26518003_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
