Title: Arranca el Foro Nacional de Paz de los Pueblos Indígenas
Date: 2015-02-11 21:47
Author: CtgAdm
Category: Movilización, Paz
Tags: conversaciones de paz, Conversaciones de paz en Colombia, Foro Nacional de Paz de los Pueblos Indígenas, ONIC, paz, Proceso de
Slug: arranca-el-foro-nacional-de-paz-de-los-pueblos-indigenas
Status: published

##### [Foto: uriel.mininterior.gov.co]

<iframe src="http://www.ivoox.com/player_ek_4070392_2_1.html?data=lZWkkpiddo6ZmKiakpqJd6KomZKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRitDm0JC7w8jNs8%2FVzZDRx5C0ucbWzdTgja7SqIa3lIqupszJssLnjoqkpZKns8%2FowszW0ZC2pcXd0JCah5yncZU%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Carmen Rosa Guerra] 

**Este 12 y 13 de febrero, se realizará el Foro Nacional de Paz de los Pueblos Indígenas,** convocado por la consejería de Derechos de los Pueblos Indígenas de la Organización Nacional Indígena de Colombia (ONIC).

Durante el foro, desde la perspectiva de “la Minga Social, popular, indígena, por la vida, el territorio, la autonomía y la soberanía”,  se hablará sobre el proceso de paz que se desarrolla en la Habana, esto con el objetivo de hacer **aportes concretos** desde la perspectiva y las necesidades de las **comunidades indígenas en Colombia**.

Para Carmen Rosa Guerra, indígena Kankuama y Coordinadora del Foro Nacional de Paz de los Pueblos Indígenas  "los pueblos indígenas hemos sido constructores de paz (...) e**s necesario consolidar este ejercicio desde nuestros pueblos en un documento que atraviese ejes estructurales que observan otros escenarios de paz desde la sociedad civil".**

Así mismo, la indígena Kankuama, afirma que **los pueblos aborígenes aplauden y apoyan los diálogos con las FARC y el ELN,** sin embargo, consideran que hay garantías que solo se pueden desarrollar **incluyendo a la sociedad civil**.

La ONIC, ha pedido que se **les garantice un espacio para presentar su agenda en la Habana**  y para además, esas propuestas de las comunidades indígenas sean tenidas en cuenta en las negociaciones de paz.

**"La paz no es absoluta y los planteamientos de los pueblos indígenas no se terminan aquí, vamos a seguir planteando posturas en torno a la paz",** aseguró la coordinadora del Foro Nacional de Paz de los Pueblos Indígenas. El evento que se llevará a cabo en el Centro Cultural Gabriel García Márquez en el Salón Rogelio Salmona.
