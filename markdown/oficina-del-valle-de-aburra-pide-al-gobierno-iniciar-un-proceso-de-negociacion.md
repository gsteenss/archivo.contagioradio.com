Title: 'Oficina del Valle de Aburra' pide al Gobierno iniciar un proceso de negociación
Date: 2016-03-29 17:31
Category: DDHH, Nacional
Tags: Medellin, oficina de envigado, Valle de Aburra
Slug: oficina-del-valle-de-aburra-pide-al-gobierno-iniciar-un-proceso-de-negociacion
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/03/Oficina-de-Envigado.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Agencia Noticias UNal ] 

###### [29 Mar 2016]

Dirigentes de la 'Oficina del Valle de Aburra' a la que pertenece la 'Oficina de Envigado' con presencia en por lo menos 10 municipios del nordeste y suroeste antioqueño, han enviado al Gobierno nacional una **solicitud para iniciar un proceso de negociación que lleve a su desmonte**.

De acuerdo con Luis Fernando Quijano, director de la ONG CORPADES, estas estructuras **deben someterse a la justicia si quieren acogerse a garantías que no impliquen impunidad**, teniendo en cuenta que su accionar violento y criminal ha ocasionado 7 mil homicidios, por lo menos 900 desapariciones, implementación de casas de tortura, desplazamiento forzado intraurbano y sometimiento de algunas organizaciones sociales.

La 'Oficina' ha enviado dos propuestas al Gobierno, en la primera, los jefes refieren un sometimiento a la justicia y en la segunda la 'dirección colegiada' plantea que debe otorgárseles reconocimiento político para que dejen las armas, proposición que "no tiene ni pies ni cabeza" porque busca dar un carácter político a una organización que no lo tiene, según afirma Quijano, por tanto es la primera propuesta la **que debería ser avalada por la Mesa de Diálogos de La Habana para que se consolide la paz en distintas ciudades del país**.

Esta divergencia en las propuestas tiene que ver con que quienes dirigen la 'Oficina' se dividen en tres vertientes, una que aboga por continuar con su trayectoria de violencia y criminalidad, otra que busca reconocimiento político para la dejación de armas y la última, que plantea sometimiento a la justicia, lo que muestra la **horizontalidad con diferencia de mandos e intereses en su conformación estructural**, como asevera Quijano.

Según Quijano los integrantes de la 'Oficina' podrán contar con garantías judiciales "siempre y cuando desmantelen la estructura tanto a nivel político como económico y militar", lo que implica que sus verdaderos jefes muestren el rostro y sean desmanteladas las **350 bandas que existen en Medellín y las más de 500 del Valle de Aburra, que completan un total de 13 mil personas** vinculadas que deben restaurar los derechos vulnerados durante sus más de 30 años de trayectoria, en la que han sido la fachada del 'Cartel de Medellín' y se han consolidado como una estructura que representa "familias, clanes y organizaciones mafiosas", vinculadas al paramilitarismo.

Teniendo en cuenta que "Colombia necesita la paz rural, necesita la paz urbana y que el posconflicto cobije a todos" Quijano concluye aseverando que el Gobierno nacional, en cabeza del Ministerio del Interior, del de Justicia y del del Posconflicto, junto a la Fiscalía y la Consejería Presidencial para los Derechos Humanos, debe ponerse en la tarea de **iniciar y consolidar los diálogos urbanos que ya se vienen desarrollando desde hace algunos años,** sin que participe la sociedad civil y sin que desde la Mesa de Diálogos haya pronunciamientos frente al tema.

#### [**Entrevista Fernando Quijano, Corpades**] 

<iframe src="http://co.ivoox.com/es/player_ej_10973513_2_1.html?data=kpWmmZiZdZShhpywj5adaZS1lJ2ah5yncZOhhpywj5WRaZi3jpWah5yncbbijNXf0cjJt9CfxcqY0MrLs8TdwsjWh6iXaaOnz5DQ0dOPsMKfhqqfh52UaZqssMvWxc7SpYzYxtGYuMbQsMafjoqkpZKns8%2FowszW0ZC2pcXd0JCah5yncZU%3D&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)]
