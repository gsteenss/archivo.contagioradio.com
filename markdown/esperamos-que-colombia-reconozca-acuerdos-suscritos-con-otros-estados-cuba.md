Title: Esperamos que Colombia reconozca acuerdos suscritos con otros Estados: Cuba
Date: 2020-07-09 21:11
Author: CtgAdm
Category: Actualidad, Política
Tags: Cuba, ELN, Gobierno Duque, Implementación de Acuerdo de paz
Slug: esperamos-que-colombia-reconozca-acuerdos-suscritos-con-otros-estados-cuba
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/07/Ecf1PanXsAg-tMi.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","fontSize":"small"} -->

Foto: Cancillería de Cuba

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Con su participación en la Comisión de Seguimiento, Impulso y Verificación, (CSIVI), Cuba anunció su decisión de mantener por el momento su condición de garante en la implementación del acuerdo de paz entre el gobierno de Colombia y las FARC-EP, alentados por peticiones de organizaciones en el país y de la comunidad internacional sobre la conveniencia de la continuidad en su rol por la paz del país, pese a las declaraciones y desconocimiento de acuerdos entre ambas naciones por parte del gobierno Duque.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Aunque el gobierno Cuba se había retirado temporalmente de la CSIVI a raíz de las más recientes tensiones entre ambas naciones, su Cancillería reiteró su apoyo a la implementación con la presencia del embajador cubano, José Luis Ponce. [(Lea también: Gobierno debe dialogar con Cuba para enfrentar la pandemia)](https://archivo.contagioradio.com/gobierno-debe-dialogar-con-cuba-para-enfrentar-la-pandemia/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Cabe recordar que el pasado 4 de junio, el Ministerio de Relaciones Exteriores de Cuba rechazó la actuación de la Cancillería colombiana, que facilitó la inclusión del país insular en la lista de Estados que no cooperan plenamente en los esfuerzos de Estados Unidos contra el terrorismo, calificando a Cuba como un "refugio seguro" precisamente por la presencia en Cuba de la delegación de paz del ELN. Un acto que además fue considerado un "espaldarazo" por funcionarios como Miguel Ceballos, Alto Comisionado para la paz. [(Le recomendamos leer: El Estado nos dio la espalda, Cuba acogió a nuestros hijos: Militares por la Reconciliación)](https://archivo.contagioradio.com/cuba-acogio-nuestros-hijos-militares-reconciliacion/)

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Pese a diferencias, Cuba expresa voluntad diplomática

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

"Esperamos que el Estado colombiano reconozca la vigencia de los acuerdos suscritos con otros estados y cumpla con esos compromisos, en particular con el protocolo de ruptura del diálogo con el ELN", expresó [Rogelio Sierra Díaz](https://twitter.com/RogelioSierraD), Viceministro de la Cancillería de Cuba. [(Lea también: "Incumplir acuerdos internacionales trae consecuencias": Eurodiputados sobre Colombia)](https://archivo.contagioradio.com/incumplir-acuerdos-internacionales-trae-consecuencias-eurodiputados-sobre-colombia/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Agregan desde la Cancillería que la delegación de paz del ELN "no viajó a Cuba huyendo. Viajó con las correspondientes autorizaciones del estado colombiano" por lo que el Gobiero al romper el diálogo con el ELN y desconocer un Protocolo previamente acordado para este caso, además de dictar órdenes de captura, acogió una "conducta contradictoria" que desconoce los acuerdos suscritos con el país en condición de garante, en el proceso de paz con el ELN. [(Lea también: Académicos del mundo piden a la ONU exigir cumplimiento de protocolos con el ELN)](https://archivo.contagioradio.com/academicos-del-mundo-piden-a-la-onu-exigir-cumplimiento-de-protocolos-con-el-eln/)

<!-- /wp:paragraph -->

<!-- wp:quote -->

> " Todos conocen que Cuba siempre ha actuado desde la condición de garante con imparcialidad, transparencia, discreción y a solicitud de ambas partes. Todo con el propósito de ayudar al anhelo compartido de una paz integral en Colombia"
>
> <cite>Rogelio Sierra Díaz, Viceministro de la Cancillería de Cuba.</cite>

<!-- /wp:quote -->

<!-- wp:paragraph {"align":"justify"} -->

En nombre del Gobierno, el consejero presidencial para la estabilización y la consolidación, Emilio Archila expresó que “nunca ha puesto en duda el papel de Cuba como país garante ni lo ha cuestionado, sino que, por el contrario, espera que siga ejerciendo esa función”. [(Le puede interesar: ELN propone cese bilateral y el Gobierno cierra la puerta a esa posibildad)](https://archivo.contagioradio.com/eln-propone-cese-bilateral-y-el-gobierno-cierra-la-puerta-a-esa-posibildad/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Aunque desde el Gobierno afirman que siempre han contado con Cuba como un aliado del país, cabe resaltar cómo, a través de informes, como el del senador Antonio Sanguino, se han detallado los primeros 18 meses de la política exterior del gobierno Duque, caracterizada por un rechazo al diálogo, centrando su atención más en la situación en Venezuela que en la prevalencia del Acuerdo de Paz, **lo que ha llevado a un distanciamiento con los países garantes como Cuba y Noruega, o con las Naciones Unidas.** [(Lea también: No se puede menosprecia el rol de Cuba en los procesos de paz del mundo)](https://archivo.contagioradio.com/no-se-puede-menosprecia-el-rol-de-cuba-en-los-procesos-de-paz-del-mundo/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Parte de esta conducta ha sido evidente a través de frecuentes cuestionamientos del presidente Duque e integrantes de su gabinete, quienes insisten en la solicitud de extradición desde La Habana a los negociadores del ELN – lo que violaría el protocolo establecido en caso de rompimiento de la mesa de diálogo o la abstinencia de Colombia en la votación para levantar el bloqueo económico de Estados Unidos a Cuba, cambiando históricamente su posición ante el mundo. [(Lea también: Política exterior de Colombia obedece a intereses de EEUU: Congresistas)](https://archivo.contagioradio.com/politica-exterior-de-colombia-obedece-a-intereses-de-eeuu-congresistas/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Dichos antecedentes han llamado la atención de varios académicos de distintas nacionalidades quienes han señalado que desde el año pasado se ha generado una relación tensa entre ambos países, donde la comunidad internacional está “pasivamente como espectadora” e incluso se le ha pedido a las Naciones Unidas (ONU) que intervenga dentro de la delicada situación diplomática que existe entre ambas naciones. [(Lea también: Miguel Ceballos no le cumple a la paz sino que «promueve el conflicto armado»: Cepeda)](https://archivo.contagioradio.com/miguel-ceballos-no-le-cumple-a-la-paz-sino-que-promueve-el-conflicto-armado-cepeda/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

En septiembre de 2019, tanto Noruega como Cuba, países garantes en la Mesa de Conversaciones entre el Gobierno de Colombia y las antiguas FARC- EP ya habían ratificado su apoyo a la implementación del acuerdo resaltando la importancia de blindar la Jurisdicción Especial para la Paz y promover la reforma rural integral, la sustitución de cultivos de uso ilícito y las garantías en la reincorporación de los excombatientes, temas que han sido dejados en un segundo plano durante esta administración. [(Le recomendamos leer: 2Cuba y Noruega reiteran su apoyo al Proceso de Paz)](https://archivo.contagioradio.com/cuba-y-noruega-reiteran-su-apoyo-al-proceso-de-paz/)

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->
