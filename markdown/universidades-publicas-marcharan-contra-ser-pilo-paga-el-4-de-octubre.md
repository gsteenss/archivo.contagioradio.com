Title: Universidades Públicas marcharán contra "Ser Pilo Paga" el 4 de Octubre
Date: 2017-09-29 15:35
Category: Movilización, Nacional
Tags: ser pilo paga
Slug: universidades-publicas-marcharan-contra-ser-pilo-paga-el-4-de-octubre
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/05/crisis-universidades-publicas.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [29 Sept 2017] 

Estudiantes de la Universidad Nacional la Universidad Pedagógica y la Distrital, han venido realizando actividades para pronunciarse **en contra de la desfinanciación de la educación superior** en el país. El 4 de octubre realizarán una movilización a nivel nacional para protestar en contra de políticas como “Ser Pilo Paga”, la falta de recursos para la investigación y la vulneración de los derechos de los estudiantes.

De acuerdo con Carlos Bautista, representante de los estudiantes de la facultad de Ciencia Política de la Universidad Nacional, **éste programa está poniendo en riesgo a la educación pública** en la medida que eleva los costos para el Estado y contribuye a su desfinanciación por el desvío de los recursos. (Le puede interesar: ["Ser Pilo Paga ha gastado más de 350 mil millones del presupuesto de educación"](https://archivo.contagioradio.com/ser-pilo-paga-se-llevo-373-290-470-719-del-presupuesto-de-educacion/))

Bautista manifestó que “Ser Pilo Paga es una política que ofrece créditos educativos condonables, que se presentan como becas, pero en realidad los **estudiantes están bajo amenaza de una deuda** que los puede perjudicar y además destruye la naturaleza de la educación superior pública y gratuita”.

Además, indicó que “muchos prefieren estudiar en una universidad privada por las facilidades que implica no tener que realizar un examen de admisión”. Dijo que el programa del Estado **no ayuda a que se aumente la población estudiantil** y, por el contrario, eleva los costos y gastos para el Estado .

### **Estado invierte un billón de pesos anual a “Ser pilo Paga”** 

Según el estudiante, **el programa le cuesta a la nación un billón de pesos** que podría ser invertido en mejorar las condiciones de la universidad pública. Además, el Gobierno Nacional ha venido “modificando ciertos fondos de la educación superior que iban a universidades públicas y ahora financian el programa Ser Pilo Paga”. (Le puede interesar: ["Ser Pilo Paga, otro golpe al derecho a la educación"](https://archivo.contagioradio.com/ser-pilo-paga-otra-mermelada-del-gobierno-nacional/))

Si bien los recursos de ésta política educativa no alcanzan para subsanar la deuda de 16 billones de pesos, que tiene el Estado con la educación superior, **ese dinero serviría para solventar la crisis** y permitiría que la universidad pública dejara de autofinanciarse. Sin embargo, Bautista recordó que es necesario que se realice una reestructuración de fondo del sistema financiero de la educación.

Finalmente, el estudiante invitó a la **movilización del 4 de octubre** a nivel nacional, donde la comunidad educativa pedirá que se establezca un aumento en el presupuesto de la financiación de la educación superior, que se detenga el recorte a la investigación y que no se vulneren los derechos de los estudiantes.

<iframe id="audio_21178547" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_21178547_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)
