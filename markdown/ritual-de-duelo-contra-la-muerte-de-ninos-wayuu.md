Title: Un ritual de duelo contra la muerte de niños Wayúu
Date: 2016-05-11 17:53
Category: Fotografia, Otra Mirada
Tags: Agua, indigenas wayuu, La Guajira
Slug: ritual-de-duelo-contra-la-muerte-de-ninos-wayuu
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/05/muerte-de-ninos-wayuu-2-1.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/05/muerte-de-ninos-wayuu-3-1.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/05/muerte-de-ninos-wayuu-4-1.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/05/muerte-de-ninos-wayuu-5-1.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/05/muerte-de-ninos-wayuu-9-1.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Fotos: Contagio Radio 

###### 11 May 2016 

"Cada muerte nos tiene que doler a todos, indígenas y no indígenas, blancos o afros, de Colombia y el mundo", este fragmento acompañaba uno de los textos que mujeres con mantos negros entregaban a las personas que caminaban en la Plaza de Bolivar, lugar al que llegaron integrantes de las comunidades Wayúu de la Media Guajira para denunciar y dar a conocer [el "genocidio" del que están siendo víctimas los niños y niñas](https://archivo.contagioradio.com/ninos-indigenas-de-colombia-estan-siendo-victimas-de-genocidio/) de esta parte del país.

Con baúles de cartón que llevaban el nombre de niños fallecidos durante los últimos 7 años, se realizó el ritual "Entre mantas y llantos", un acto simbólico que busca sensibilizar a las personas, para invitarlas a realizar acciones que terminen con esta situación, que pudo ser evitada.

Con la participación de autoridades indígenas, organizaciones, personas que han apoyado a la comunidad y transeúntes,  se exigió al Gobierno una pronta atención a los niños para detener la alta tasa de mortalidad infantil en el departamento de La Guajira. Igualmente se hizo un llamado para que los órganos de control implementen planes para salvaguardar la vida e integridad de todos los pueblos indígenas del país, especialmente del pueblo Wayúu, de manera que primen los derechos de las comunidades sobre los intereses económicos de las empresas.

 \

<figure>
[![muerte-de-ninos-wayuu-2](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/05/muerte-de-ninos-wayuu-2-1.jpg)](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/05/muerte-de-ninos-wayuu-2-1.jpg)

</figure>
<figure>
[![muerte-de-ninos-wayuu-3](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/05/muerte-de-ninos-wayuu-3-1.jpg)](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/05/muerte-de-ninos-wayuu-3-1.jpg)

</figure>
<figure>
[![muerte-de-ninos-wayuu-4](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/05/muerte-de-ninos-wayuu-4-1.jpg)](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/05/muerte-de-ninos-wayuu-4-1.jpg)

</figure>
<figure>
[![muerte-de-ninos-wayuu-5](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/05/muerte-de-ninos-wayuu-5-1.jpg)](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/05/muerte-de-ninos-wayuu-5-1.jpg)

</figure>
<figure>
[![muerte-de-ninos-wayuu-9](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/05/muerte-de-ninos-wayuu-9-1.jpg)](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/05/muerte-de-ninos-wayuu-9-1.jpg)

</figure>

