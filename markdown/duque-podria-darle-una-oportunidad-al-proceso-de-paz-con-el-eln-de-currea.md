Title: Duque podría darle una oportunidad al proceso de paz con el ELN: De Currea
Date: 2018-08-02 15:57
Category: Nacional, Política
Tags: ELN, Iván Duque, Mesa de Diálogos de La Habana
Slug: duque-podria-darle-una-oportunidad-al-proceso-de-paz-con-el-eln-de-currea
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/08/eln.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Twitter ELN ] 

###### [02 Agosto de 2018]

Sin cese bilateral, pero con un protocolo para continua la mesa de diálogo en La Habana, culminó el sexto ciclo de conversaciones entre el ELN y el Gobierno Nacional. Ambas partes expresaron en un comunicado de prensa que se alcanzaron avances en el punto 1 y 2 de la agenda de pactada, como el respaldo de la comunidad internacional. Sin embargo, a 5 días de la posesión de **Iván Duque como presidente la incertidumbre que recae sobre el proceso es la forma en la que continuará.**

Según Víctor de Currea, analista político y docente, el mensaje rígido con el que llegó Duque a la campaña electoral, no será el mismo que se vea cuando inicie su mandato, de hecho, de Currea afirmó que **"hay voces del Uribismo que están planteando la formulación de canales"**, algunos ya se abría abierto para evaluar en qué va el proceso de paz y cómo podría mantenerse. Además, expresó que el ELN ha sido claro en manifestar que continúan en la mesa de La Habana hasta que encuentren una respuesta sobre el curso del proceso.

El próximo paso para garantizar ese posible séptimo ciclo de conversaciones, de acuerdo con de Currea, tendría que consistir en encuentros entre el ELN y el equipo de negociaciones de Duque, que seguramente dará como resultado otra estrategia de diálogo.  (Le puede interesar:["Incertidumbre en la mesa ELN-Gobierno con la presidencia de Iván Duque")](https://archivo.contagioradio.com/se-sentara-duque-a-dialogar-en-la-mesa-con-eln/)

Además, el analista señaló que con Iván Duque al mando, perteneciendo al partido Centro Democrático, no se podrá alegar sobre **"enemigos de la paz"** como en el caso de Santos y que podría ser una oportunidad para encontrar diferentes salidas al conflicto armado que no tengan que ver con el sometimiento del otro.

###### <iframe id="audio_27550627" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_27550627_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
