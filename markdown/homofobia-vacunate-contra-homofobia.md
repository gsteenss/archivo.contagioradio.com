Title: Vacúnate contra la homofobia: aún estás a tiempo
Date: 2015-05-25 06:24
Category: Escritora, Opinion
Tags: colombia, Comunidad LGBTI, gais, gays, homofobia, vacunate
Slug: homofobia-vacunate-contra-homofobia
Status: published

#### Por [Escritora Encubierta](https://archivo.contagioradio.com/escritora-encubierta/)[ ] 

###### [21 May 2015] 

El pasado 17 de Mayo, se celebró en más de 100 países el Día Internacional Contra la Homofobia  y la Transfobia con el fin de conmemorar cuando, en el mismo día y mes pero muchos años atrás, en 1990, la Organización Mundial de la Salud (OMS) eliminó a la homosexualidad de la lista de trastornos mentales.

De acuerdo a la RAE, el término ''homofobia'' hace referencia a la aversión obsesiva hacia las personas homosexuales. Coloquialmente también suele referirse a las demás personas que integran la comunidad LGBT, como lo son los bisexuales y los transexuales (aunque éstos últimos suelen tener su propio término: transfobia), y a las personas que adoptan actitudes asociadas al sexo opuesto, como lo son los transexuales.

Contrario a muchas creencias, el rechazo hacia los homosexuales no siempre estuvo presente a lo largo de la historia. En la antigua Grecia y la antigua Roma, en ciertos casos, era considerado normal y estaba permitido socialmente que dos hombres tuvieran relaciones sexuales. El tema de la homosexualidad tuvo un lugar importante en la literatura de estas culturas. Y aún más curioso, la mayoría de los piratas rechazaba la heterosexualidad. A pesar de que en los puertos existía la posibilidad de establecer contacto con las mujeres, los piratas preferían tener relaciones con otros hombres, e incluso, constituyeron los primeros ''matrimonios'' o uniones homosexuales. Las mujeres que eran capturadas rara vez eran utilizadas como objeto sexual.

La homosexualidad no fue perseguida sino hasta la época de la inquisición; aquella misma en que de igual manera se castigaba, humillaba públicamente, mutilaba y torturaba hasta la muerte no sólo a personas, sino también a animales, por herejía, blasfemia y otros comportamientos que no se ajustaban a los parámetros de la Iglesia Católica.

En la época nazi, se consideraba que la homosexualidad era un defecto genético y que por lo tanto las personas que realizaban actos homosexuales eran inferiores. Se calcula que aproximadamente 100.000 hombres fueron arrestados, 15.000 enviados a campos de concentración donde más de la mitad murieron y sólo unos 4.000 sobrevivieron.

Actualmente, se podría decir que el mundo está dividido. A pesar de que en muchos países hay leyes que protegen y acobijan a los homosexuales, en algunos otros todavía se castiga a una persona por su orientación sexual. En 78 países se considera ilegal la homosexualidad y, peor aún, en 7 se condena con pena de muerte.

Incluso, en aquellos países que se jactan defender los derechos humanos se siguen viendo casos discriminación hacia el colectivo LGBT; tanto familiar, como escolar y laboralmente, entre otros. Tristemente muchos de esos casos llevan a las víctimas a buscar consuelo en la autoflagelación, el alcoholismo, la depresión, la drogadicción y el suicidio. Lo que usted podría considerar que es una simple palabra, un simple gesto, o un simple golpe podría ser el desencadenante de lo anteriormente mencionado.

No es cuestión de opiniones. Usted puede no estar de acuerdo con que dos personas del mismo sexo se amen, pero eso no le da derecho a agredir física o verbalmente a esa pareja. Si a usted no le gustaría que le hicieran eso, entonces no haga lo mismo con otras personas. Todos somos seres humanos, por ende tenemos los mismos derechos fundamentales (el derecho a la vida, a la libertad y seguridad, a la libertad religiosa e ideológica, a la libertad de expresión...) y ninguno vale más que otro.

Por esto, le invito a que tenga más respeto y tachar de su léxico las siguientes palabras al referirse a una persona homosexual: maricón, loca, enfermo, desviado, arepera, bota plumas, puto, machorra, mariquita, etc, y a que se abstenga de creer que puede golpear a alguien que no le ha hecho nada.

Frases célebres en contra de la homofobia

• Ser homofóbico es como pretender que nadie coma chocolate o pizza sólo porque tú estás a dieta.

• La homosexualidad está presente en más de 450 especies, la homofobia sólo en una. ¿Qué es lo antinatural ahora?

• Odiar a la comunidad LGBT es como odiar a alguien por ser surdo.

• La homosexualidad no es una enfermedad, la homofobia sí.

• Amarse entre iguales no es tan diferente.
