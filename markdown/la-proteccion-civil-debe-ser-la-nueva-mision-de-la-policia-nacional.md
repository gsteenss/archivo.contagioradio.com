Title: La protección civil debe ser la nueva misión de la Policía Nacional
Date: 2016-05-23 08:28
Category: Opinion, Ricardo
Tags: emisoras de la policía, ESMAD, policia
Slug: la-proteccion-civil-debe-ser-la-nueva-mision-de-la-policia-nacional
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/05/policia-reprime.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Gabriel Galindo 

#### **Por [Ricardo Ferrer Espinosa](https://archivo.contagioradio.com/ricardo-ferrer-espinosa/) - [@ferr\_es](https://twitter.com/ferr_es)** 

###### 23 May 2016 

#### [**La policía nacional de Colombia nunca debió ser militarizada**[.]] 

En Colombia, por degradación del conflicto, la policía pasó de ser un ente de protección ciudadana a un instrumento de la guerra, con funciones de policía política, cacería de líderes sociales, intimidación de periodistas y corrupción en toda su cadena de mando. \[1\]

En lo más crudo de la guerra reciente, la policía nacional actuó abiertamente del lado de los victimarios. Si el Estado es consecuente con el actual proceso de reducir el conflicto, la misión global de la policía debe cambiar.

[Los policías militarizados deben ser sustituido por oficiales formados en la] **protección civil. Es decir, que realmente cumplan el mandato de proteger vida, honra y bienes de los ciudadanos.**

Aporto algunos ejemplos de los cambios inmediatos que se deben dar en la misión institucional:

**1. Deben ceder las emisoras de la policía**[ (que son una clara aberración institucional) a las entidades de protección civil y a las comunidades. Mientras las comunidades perdieron sus emisoras comunitarias, el Estado traspasó frecuencias y equipos a la fuerza pública.]

Las emisoras comunitarias, tan perseguidas y criminalizadas en Colombia, son fundamentales para proteger a la población, orientarla y son pieza clave en la coordinación de la ayuda. Por el momento, las emisoras de la policía tienen una misión más centrada en la propaganda oficial, un tono bélico, un lenguaje muy pobre y envía pocos mensajes de unidad comunitaria. Recuperar estas emisoras, devolverlas a la comunidad, centrarlas en la protección civil, es tarea urgente, si en verdad el Estado quiere reparar el daño que ha hecho.

**2.** **Habitamos un país especialmente vulnerable ante los embates de la naturaleza:**

Cada año oscilamos entre ciclos climáticos de lluvias torrenciales y sequías. Además de las alertas tempranas sobre inundaciones, derrumbes en carreteras, evacuaciones de población, la policía puede aportar en campañas de mitigación de riesgos y saneamiento básico.

Estamos en el cinturón de fuego del pacífico y por lo tanto estamos expuestos a sismos, terremotos, fallas del terreno y actividad volcánica. La policía tiene un buen espacio de trabajo orientando a la población sobre planes de evacuación, sitios de encuentro, activación de alertas y atención de las contingencias.

El clima tropical y húmedo es perfecto para enfermedades con dengue, zika, chicunguña, malaria y otros procesos epidémicos. La educación ambiental, el manejo y control de aguas, son espacios donde la policía puede hacer grandes aportes, porque el agua es un bien común.

**3. Protección de infancia y adolescencia:**

[- Queremos ver a la policía en la calle, protegiendo a los menores, cuidando los entornos escolares,  cazando a  los depredadores sexuales, evitando el tráfico de drogas en las escuelas, rescatando niños de la mendicidad,]

**4. Debe desaparecer el organismo “Defensa Civil**[**”**, de tenebrosa historia y dudosa gestión.]

[En Colombia, el policía que conocemos corresponde a un actor de la violencia institucional. Si recobramos la función de]**Protección Civil**[ para la policía, los ciudadanos tendremos un verdadero apoyo ante urgencias, emergencias y desastres.]

El esfuerzo de cambiar la mentalidad pasa por modificar los programas en las escuelas de policía. Se deben recuperar las experiencias de países como Suiza, Cuba, Alemania, España y Rusia, que planifican sus sistemas de protección civil  a largo plazo, con visiones proactivas, con planes de mitigación y campañas educativas todos los días del año.

Pero la idea principal, es crear una nueva mentalidad del ciudadano, conocedor de sus derechos, gestor de su propia seguridad y sobre todo, ciudadano participante en la vida de la comunidad.

###### [\[1\]][[http://www.las2orillas.co/corrupcion-en-la-policia-esta-escandalosa-denuncia-puede-haber-sido-una-de-las-razones-de-la-renuncia-del-general-janio-leon-riano/]](http://www.las2orillas.co/corrupcion-en-la-policia-esta-escandalosa-denuncia-puede-haber-sido-una-de-las-razones-de-la-renuncia-del-general-janio-leon-riano/) 

------------------------------------------------------------------------

###### Las opiniones expresadas en este artículo son de exclusiva responsabilidad del autor y no necesariamente representan la opinión de la Contagio Radio.
