Title: Retos del MOVICE tras la celebración de sus 10 años
Date: 2015-08-03 16:14
Category: DDHH, Nacional
Tags: Antioquia, Derechos Humanos, Encuentro Mundial de Movimientos Populares, escombrera, MOVICE, Paramilitarismo, víctimas de crímenes de Estado
Slug: retos-del-movice-tras-la-celebracion-de-sus-10-anos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/08/MOVICE-Antioquia.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: [alianzademediosalternativos.blogspot.com]

##### <iframe src="http://www.ivoox.com/player_ek_5925635_2_1.html?data=lp6fl5uXeY6ZmKiak5yJd6KmmpKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRkbDKqqiyjcjFtIa3lIquptnZsNCfotPhy9TVucrVjMjiz9XQqYylkZDOh6iXaaOl0Niah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe> 

##### [Ingrid Vergara, MOVICE] 

###### [3 Agosto 2015]

El pasado 1 de agosto el El Movimiento de víctimas de crímenes de estado, MOVICE, capítulo Antioquia concluyó la Asamblea que se realizó **en el marco de los 10 años de lucha contra la impunidad del Movimiento.**

La asamblea se llevó a cabo en el Museo Casa de la Memoria, en la ciudad de Medellín, donde distintas organizaciones defensoras de derechos humanos abordaron temas sobre **esclarecimiento de la verdad, desaparición forzada, paz territorial y justicia**.

De acuerdo con Ingrid Vergara, integrante del MOVICE y quien participó durante la asamblea, el trabajo que se ha desarrollado desde Antioquia “ha fortalecido el Movice a nivel nacional permitiendo a las víctimas **romper los grandes muros de impunidad que existen en Colombia”.**

Antioquia es un escenario marcado por altos índices de violencia acompañado por el fenómeno del paramilitarismo, es por eso que el Urabá Antioqueño ha sido una de las regiones que más víctimas ha sumado y donde hay mayor despojo de tierras. Es a partir de ese panorama que “**el MOVICE Antioquia  ha logrado visibilizar la criminalidad del estado y sus víctimas**”, dice Vergara.

Durante la asamblea se propuso retomar el **tema de la justicia alternativa y desarrollar audiencias ciudadanas por la verdad,** donde sean las victimas las que cuenten los hechos victimizantes y no los victimarios como se ha hecho en Colombia.  Así mismo, se propuso la creación de un tribunal donde se hable de temas como el despojo de tierras, mujeres y desaparición forzada, también se acordó trabajar en una Ley que sancione y desmonte las estructuras paramilitares.

Finalmente se piensa crear **acciones de censura pública frente a los perpetradores de crímenes de lesa humanidad** y se acordó realizar reuniones con las comunidades indígenas y afro para que en época de elecciones de alcaldes y gobernadores  no se le dé votos a personas han tenidos vínculos con paramilitares.

 
