Title: En los últimos 4 años matrículas de Los Andes han aumentado $4 millones de pesos
Date: 2017-11-21 17:29
Category: Educación, Entrevistas
Tags: altos costos de las matriculas, ser pilo paga, Universidad de los Andes
Slug: en-los-ultimos-4-anos-matriculas-de-los-andes-han-aumentado-4-millones-de-pesos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/11/mininterior-u-andes-e1511297596267.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: MinInterior] 

###### [21 Nov 2017] 

[Estudiantes de la Universidad de los Andes, mantienen una jornada de actividades en protesta a los altos costos en las matrículas. En 2018, los estudiantes deberán pagar **\$16. 334.000 pesos** por cada semestre sin tener en cuenta a los estudiantes de medicina, quienes deberán pagar 23.254.000 pesos. ]

Según explica Laura Torres, integrante del grupo, la 'Universidad Más Pública del País', esa institución aumenta** el precio de la matrícula teniendo en cuenta la inflación más 2 puntos porcentuales adicionales**. En esa línea, se viene denunciando que se trata de una situación que responde a que la universidad no ha podido diversificar sus fuentes de ingresos. (Le puede interesar: [La nueva versión de "Ser Pilo Paga](https://archivo.contagioradio.com/48884/)")

### **Los problemas para los estudiantes** 

Torres señala que son las matrículas de estudiantes de pregrado las que financian a toda la universidad, y por ende, son exorbitantes los aumentos en las matrículas. Por ejemplo en el caso de Laura, **al iniciar su carrera en el 2013, empezó pagando cerca de 13 millones de pesos y hoy sus estudios semestrales ya están sobre los \$16 millones.** Esa también es la situación de las más de 14.000 personas matriculadas en esa institución.

"Cada vez somos más las personas que nos vemos en una situación muy apremiante para poder pagar la matrícula", expresa la estudiante. En el caso de medicina, por ejemplo, los estudiantes se ven obligados a pagar actualmente \$ 23 millones. Una situación que estaría generando deserciones de personas que ya no están en posibilidad de poder pagar una matrícula de esos costos.

Los estudiantes además sostienen que **desde que se implementó el programa 'Ser Pilo paga', ha aumentado exponencialmente el número de alumnos.** Si bien esto no es negativo, si contribuye a que aumente el costo de las matrículas. Torres además considera que la política gubernamental, "beneficia a las universidades privadas, y en cambio le quita recursos a las públicas". (Le puede interesar:[ Estudiantes de públicas y privadas listos para afrontar  política de 'Ser pilo paga')](http://bit.ly/2jKsgKA)

### **¿Qué están haciendo los estudiantes?** 

No es la primera vez que sucede esta situación. En ocasiones anteriores la comunidad universitaria se ha manifestado contra el alza de las matrículas como sucedió en el año 1997. Durante la última semana la comunidad universitaria desarrolló una asamblea a la que asistieron varios estudiantes y a la que se vio obligado a ir el rector de la universidad para responder una serie de preguntas de los alumnos.

De esa gran asamblea los estudiantes organizaron mesas de discusión para evidenciar las problemáticas que enfrenta cada departamento. A través de ellas **se desarrollan diferentes debates y actividades culturales que buscan generar presión para que se establezca un diálogo** serio con la universidad sobre dichos temas.

<iframe id="audio_22207783" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_22207783_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]

######  
