Title: FARC denuncia asesinato de 3 de sus integrantes en Nariño
Date: 2018-01-31 11:04
Category: DDHH, Nacional
Tags: ELN, FARC, nariño
Slug: farc-afirma-posible-asesinato-de-3-integrantes-por-estructura-del-eln
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/06/las-preocupaciones-de-las-farc-luego-de-la-entrega-de-armas.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: NC Noticias] 

###### [31 Ene 2018] 

La Fuerza Alternativa Revolucionaria del Común, FARC, informó en un comunicado de prensa que **Jhojan Silva, William Rivera y José Luis Cortes, integrantes de esta organización, fueron asesinados**, posiblemente por una estructura del ELN, así mismo señalaron que habría una persona menor de edad desaparecida e identificada con el nombre de David Rivera.

Los hechos ocurrieron el pasado 25 de enero, c**uando los integrantes de la FARC se encontraban en el resguardo Sande de Santa Cruz de Guachavez, en Nariño, realizando actividades del campo**. De acuerdo con el comunicado, la FARC dio un compás de espera para corroborar al 100% la situación. Hecho que se confirma en una reunión sostenida por delegados de las FARC en Pasto, Nariño esta mañana.

En ese sentido los delegados de la FARC aseguraron que la información la obtuvieron a través de “informantes” que le aseguraron que los cuerpos **“habían sido enterrados y que el ELN manifestó que no los fueran a buscar”**. (Le puede interesar: ["No cesan los asesinatos contra integrantes de la FARC"](https://archivo.contagioradio.com/asesinados-integrantes-de-las-farc-peque/))

En el comunicado de la FARC, la organización expresó que ya interpuso la demanda correspondiente ante la Fiscalía y que pretende acercarse a la Defensoría del Pueblo, para que, a través de esta, **la gobernación de Nariño solicite de inmediato un consejo de seguridad**.

Noticia en desarrollo...
