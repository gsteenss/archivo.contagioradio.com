Title: Fumigaciones manuales con glifosato: una decisión paradójica y negligente
Date: 2016-05-05 20:41
Category: DDHH, Entrevistas
Tags: Cultivos de uso ilícito, Glifosato, Monsanto, narcotrafico
Slug: fumigaciones-terrestres-con-glifosato-una-decision-paradojica-y-negligente
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/05/Glifosato.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: La Prensa Latina 

###### [5 May 2016] 

Como una decisión **“paradójica” y una “improvisación”,** señala César Jeréz, vocero nacional de la Asociación de Zonas de Reserva Campesina,  el anuncio de que el Consejo Nacional de Estupefacientes de Colombia autorizó nuevamente la fumigación terrestre de cultivos de uso ilícitos con el **herbicida glifosato, un agente químico cancerígeno, como lo advirtió la Organización Mundial de la Salud.**

“Es una decisión paradójica y negligente. El gobierno está improvisando. A pesar de sus discursos en escenarios internacionales donde asume una postura progresista del problema de los cultivos y el consumo de drogas, y a pesar de los  acuerdos firmados en la Habana, **se avoca a la violencia que representa fumigar de manera manual y terrestre”,** expresa Jeréz, quien añade que además se trata de una [“política fracasada que no ha dado ningún resultado”.](https://archivo.contagioradio.com/30-anos-de-lucha-en-contra-del-glifosato-en-colombia/)

Como lo han advertido del comunidades víctimas de estas aspersiones, [el glifosato causa múltiples impactos negativos,](https://archivo.contagioradio.com/no-solo-las-balas-matan-tambien-el-glifosato-y-la-indiferencia-de-los-gobernantes/)tanto en la salud, como en el ambiente pero además en la soberanía alimentaria de las poblaciones.

Diferentes estudios, entre ellos el de la OMS, plantean que las actividades de fumigación con glifosato **deterioran la calidad de vida de los habitantes, pues [causa problemas dermatológicos,](https://archivo.contagioradio.com/la-aspersion-con-glifosato-esta-enfermando-a-los-ninos-en-putumayo/) respiratorios, oculares, hormonales, e incluso anormalidades durante la gestación y abortos. Así mismo, generan graves daños en el ambiente pues este veneno puede llegar a las fuentes de agua de las que viven las comunidades.**

De acuerdo con César Jeréz, esta decisión del gobierno, se debe a presiones de los Estados Unidos y los intereses económicos que tienen algunas empresas. El libro ‘Costos económicos y sociales del conflicto en Colombia’ de la Universidad de Los Andes, revela que las campañas de aspersión aérea de herbicidas sobre los cultivos ilícitos **son generalmente ejecutadas por contratistas estadounidenses que trabajan para compañías como DynCorp, que a su vez trabaja con el químico Roundup, producido por la [compañía Monsanto](https://archivo.contagioradio.com/monsanto-de-los-mismos-creadores-del-glifosato-surgio-el-agente-naranja/)**, que desarrolló y patentó la molécula de glifosato en 1970 y comenzó a comercializarla en 1973.

Pese a que en los cultivadores de marihuana, coca y amapola, hay un consenso para sustituir sus cultivos y acogerse plenamente a los acuerdos alcanzados en las conversaciones de paz en La Habana, el gobierno continúa con estas medidas sin haber escuchado las **propuestas para sustitución que se han trabajado en mesas nacionales, regionales, municipales y locales,** como lo asegura el vocero de ANZORC.

Es por ello, que según el líder campesino, las comunidades continúan seguras de que la única opción por el momento es la movilización que se reflejará en un **[próximo paro nacional.](https://archivo.contagioradio.com/campesinos-del-catatumbo-irian-a-paro-si-se-reactivan-fumigaciones-con-glifosato/)** Pero además, continuarán resistiendo en sus territorios cuando lleguen las autoridades de estupefacientes a realizar este tipo de procedimientos, en los que mujeres, ancianos, niños, niñas y la naturaleza resultan ser los primeros afectados.

<iframe src="http://co.ivoox.com/es/player_ej_11435042_2_1.html?data=kpahlZqUeJOhhpywj5WbaZS1lZWah5yncZOhhpywj5WRaZi3jpWah5yncaTZ1Mbfja%2FJtsbujJKYo9jTp8rVxM6SpZiJhpTijMnSjb%2FTssLnjMnSjbfJt8bm18aYpcbRtMbnytPOj4qbh4630NPhw8zNs4zGwsnW0ZCRaZi3jpk%3D&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)
