Title: "Nuevo fiscal no es prenda de garantía para las víctimas": CONPAZ
Date: 2016-07-12 12:52
Category: DDHH, Nacional
Tags: Fiscalía, Nestor Humberto Martínez, paz, Violencia de género
Slug: nuevo-fiscal-no-es-prenda-de-garantia-para-las-victimas-conpaz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/07/Nestor-Humberto-Martinez-e1468345311281.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Primicidiario 

###### [12 Jul 2016]

Fuertes críticas recibe la Corte Suprema de Justicia tras la elección de Néstor Humberto Martínez como nuevo Fiscal General de la Nación. Las víctimas, organizaciones de derechos humanos y de género ven preocupante la [situación que se avecina para el postconflicto y la defensa de los derechos humanos](https://archivo.contagioradio.com/nestor-humberto-matinez-nuevo-fiscal-general-para-la-paz/) teniendo en cuenta los intereses políticos y económicos que rodean a la nueva cabeza del ente acusador cercano a Germán Vargas Lleras, **al gremio de los azucareros, los medios hegemónicos y empresarios como Luis Carlos Sarmiento Angulo.**

Juan Martínez, integrante de CONPAZ (Comunidades Construyendo Paz en los Territorios), asegura que “**la elección del nuevo fiscal no es prenda de garantía para las víctimas**”, por lo que preocupa que en lugar de encontrar, verdad, justicia y reparación, lo único que continúe sea la impunidad, debido a los vínculos que tiene el nuevo fiscal con los sectores empresariales más poderosos del país. En ese sentido, las víctimas exigen que Néstor Humberto Martínez sea imparcial en las investigaciones contra las empresas.

<iframe src="https://co.ivoox.com/es/player_ek_12203839_2_1.html?data=kpefkpicd5qhhpywj5WXaZS1lJuah5yncZOhhpywj5WRaZi3jpWah5yncavpwtOYr8bWuIa3lIquptPJvoampJCwsbO0hbuhhpywj6jTstXVyM7cjbfFqMrjjJKSmaiReA..&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

En materia de defensa de los derechos de las mujeres la situación también es preocupante teniendo en cuenta que Néstor Humberto había pedido a ante la Corte Suprema “revisar la criminalización de la violencia intrafamiliar”. Frente a ese nuevo panorama para la situación de la mujer, **Ángela María Robledo, representante a la cámara, señaló que la Corte Suprema ignoró las solicitudes de las Mujeres.**

“No hubo una respuesta a nuestras cartas, solicitando una audiencia para presentar nuestra visión sobre lo que significa la llegada de Néstor Humberto a la Fiscalía”, dice la congresista quien agrega que no sabe “de dónde se saca que el nuevo fiscal va a atender con idoneidad la violencia de género… **Cada día asesinan a 4 mujeres en Colombia necesitamos un Fiscal que nos proteja no que despenalice”.**

<iframe src="https://co.ivoox.com/es/player_ek_12203808_2_1.html?data=kpefkpicdJmhhpywj5aUaZS1k5iah5yncZOhhpywj5WRaZi3jpWah5yncaLiyMrZw5C2s8Pgxsnch5enb6Lgysbb3MaPmsbmxcqah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ..&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

Finalmente, las organizaciones defensoras de derechos humanos aseguran que “esta decisión de la Corte Suprema es un infortunio pues se han desconocido las demandas de la sociedad”, dice Danilo Rueda, Defensor de Derechos Humanos e integrante de la Comisión de Justicia y Paz, quien añade, “**La elección de la Corte genera una ausencia de credibilidad por parte de ciudadanía, en un ente que está justo para demostrar que estamos en un Estado Social de Derecho**”

Rueda, concluye indicando que las organizaciones  sociales deberán manifestar públicamente las acciones que deberá implementar el nuevo fiscal para demostrar independencia  en el ente acusador, teniendo en cuenta que **Martínez estará muy cuestionado frente en temas relacionados con la desarticular del paramilitarismo y sobre la toma de decisiones para la implementación de la Jurisdicción Especial para la Paz,** acordada en las conversaciones de La Habana.

<iframe src="https://co.ivoox.com/es/player_ek_12203826_2_1.html?data=kpefkpicdpehhpywj5WbaZS1lp2ah5yncZOhhpywj5WRaZi3jpWah5yncaXVz87Z0ZC2ucbYwoqfpZCns87d1M6SpZiJhpTijMnSja_Zt9XdxM7Ojd6PlMLujoqkpZKns8_owszW0ZC2pcXd0JCah5yncZU.&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
