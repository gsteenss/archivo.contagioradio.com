Title: Mientras niños y niñas mueren de sed, multimillonarios compran fuentes de agua
Date: 2015-04-27 18:12
Author: CtgAdm
Category: Ambiente, Nacional
Tags: Agua, mexico, multimillonarios, privatización del agua, sed
Slug: mientras-ninos-y-ninas-mueren-de-sed-multimillonarios-compran-fuentes-de-agua
Status: published

##### Foto: [www.revueltaverde.org]

[**Boone Pickens;el ex presidente George HW Bush y su familia; Li Ka-shing, de Hong Kong; Manuel V. Pangilinan,** y multimillonarios filipinos, entre otros, estarían comprando terrenos donde hay enormes e importantes fuentes de agua, según un informe de la revista digital Conexión México, lo que afirma cada vez más la idea de que el derecho al agua está dejando de existir, debido a su **privatización.**]

Así mismo el informe resalta que compañías bancarias como Goldman Sachs, JP Morgan Chase, Citigroup, UBS, Deutsche Bank, Credit Suisse, Macquarie Bank, Barclays Bank, Blackstone Group, Allianz y HSBC, están consolidando su control sobre el agua del mundo, por las compras de varias hectáreas de tierra, con el fin de aprovechar los lagos, lagunas y manantiales naturales para proyectos de **desalinización,  purificación y tecnologías de tratamiento, empresas de servicios públicos de saneamiento del agua, mantenimiento y construcción de la infraestructura de suministro, servicios de ingeniería del agua y el sector de agua al por menor, como los que participan en la producción y las ventas de agua embotellada,** de acuerdo al informe.

** **La revista, analiza la situación y critica el hecho de que actualmente millones de familias estén muriendo de sed, mientras estas compañías intentan acaparar las mayores cantidades de agua posibles, generando que solo unos pocos tengan acceso a este recurso vital para vivir.

El informe concluye que “el agua es y seguirá siendo indispensable para nuestra vida y es por esa razón que las élites económicas que controlan el mundo confluyen sus esfuerzos en el **control de este vital insumo**. Hace centenares de miles de años, el agua fluía libremente por valles y llanuras y todos los animales podían disfrutar libremente de ella, pues no era propiedad de nadie. Ahora, gracias al desarrollo de la civilización y a la ignorancia, inacción y falta de dignidad de la sociedad, el agua ya es propiedad de unos pocos y pronto deberemos suplicar por ella, como si fuéramos tristes esclavos. Sin duda, éste es el precio que debemos pagar por no haber actuado contundentemente cuando debíamos y tuvimos la oportunidad de hacerlo”.
