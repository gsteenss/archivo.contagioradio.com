Title: Acceso a medicamentos: Ginebra bajo la lupa
Date: 2017-05-26 06:00
Category: Mision Salud, Opinion
Tags: Acceso a los medicamentos, Derecho a la salud
Slug: acceso-a-medicamentos-ginebra-bajo-la-lupa
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/05/acceso-a-los-medicamentos.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: galilciapress 

###### 26 May 2017 

#### [**Por [Misión Salud](https://archivo.contagioradio.com/opinion/mision-salud/)**] 

[Esta semana y la siguiente (22 de mayo a 2 de junio) está en curso en Ginebra (Suiza) la 70° Asamblea Mundial de la Salud (AMS), el máximo órgano decisorio en cuestiones relacionadas con el derecho fundamental a la salud a nivel de Naciones Unidas, lo que significa un escenario con impacto en los 194 Estados Miembros de la Organización Mundial de la Salud (OMS).]

[Como es natural muchos temas están en la][[agenda]](http://apps.who.int/gb/ebwha/pdf_files/WHA70/A70_1Rev2-sp.pdf)[. Nosotros tenemos los ojos puestos en todo lo que tiene que ver con lo que consideramos la principal causa del problema de la falta de acceso a medicamentos en el mundo y, por supuesto, en Colombia: el abuso que se comete todos los días al cobrar inimaginables precios por medicamentos que, al estar protegidos por patentes, no tienen competencia a menor precio en el mercado.]

### [Veamos esta historia por partes:] 

[¿Cómo empezó? Puesto que tener medicamentos es un proceso costoso y riesgoso, desde hace no mucho (1994) a nivel global (Organización Mundial del Comercio) decidimos promover esta actividad diciéndole al inventor que podría patentar su descubrimiento. Esto lo pondría en condiciones exclusivas en el mercado (monopolio) por un determinado periodo de tiempo, y así podría cobrar precios que hicieran posible recuperar sus esfuerzos.]

[¿Qué pasó? En el camino (23 años) nos dimos cuenta de tres problemas de fondo que tiene esta decisión:]

[Los precios con los que se venden los medicamentos monopólicos son tan altos que hacen que las personas que los necesitan no los puedan comprar o no puedan hacerlo sin dejar de atender sus demás necesidades básicas (vivienda, alimentación, educación, otros gastos en salud, etc.). En el caso de países como Colombia,][[en donde el Estado es el que asume los gastos directos en salud mayoritariamente (15%)]](http://www.eltiempo.com/datos/precio-de-los-medicamentos-en-colombia-88298)[, pagar altos precios de medicamentos pone claramente en riesgo la atención de las diversas necesidades en salud que tenemos  los colombianos. ¿De qué sirve que exista un medicamento si cuando se necesita no se puede pagar?]

[Cosa clave: lo que antes era un problema de los países de medianos y bajos ingresos es ahora una crisis global. Por ejemplo, el][[sofosbuvir, medicamento utilizado para tratar la hepatitis C]](http://www.mision-salud.org/nuestras_acciones/accesoamedicamentos/el-nuevo-reto-en-el-acceso-medicamentos-en-colombia-hepatitis-c/)[. En Estados Unidos entró al mercado a US\$84.000 (tratamiento de 12 semanas para un paciente), equivalentes a COP\$250.000.000. Actualmente su precio oscila entre €50.426 (Alemania), €41.680 (Francia), €13.000 (España), €3.435 (Australia) entre otros países. Sin embargo, ¡puede lograrse un costo de producción del tratamiento completo de doce semanas a US\$62 incluyendo ya un margen de beneficio del 50%!]

La perspectiva de altos precios para recuperar la inversión enfoca la investigación en aquello que dará mayor rentabilidad y no en lo que la población necesariamente requiere. Ya en 1998 se destinaba solamente un [10% de la investigación al 90% de los problemas sanitarios del mundo](http://www.who.int/phi/CEWG_Report_ES.pdf?ua=1).

Los mecanismos que se dispusieron para proteger a la salud pública de cualquier abuso por parte de los titulares de patente (como los altos precios) son prácticamente imposibles de utilizar, especialmente por la fuerte oposición que ejercen las compañías farmacéuticas titulares de la patente y sus gobiernos. En [Perú](http://aisperu.org.pe/nuestro-trabajo/noticias/item/80-comision-de-salud-del-congreso-peruano-declarara-de-interes-publico-atazanavir-licencia-obligatoria-ya), [Colombia](http://www.mision-salud.org/nuestras_acciones/accesoamedicamentos/licencias_obligatorias/comunicado-de-sociedad-civil-sobre-decreto-670-de-mincomercio/) y [Tailandia](http://makemedicinesaffordable.org/es/civil-society-oppose-proposal-on-patent-law-amendment-4/), por ejemplo, estamos viviendo experiencias ahora o lo vivimos recientemente.

¿Qué pasa cuando la válvula de escape de un sistema de presión no funciona y éste se atora? ¡BOOM!

[¿Y entonces? Desde el 2003 viene avanzando al interior de la OMS tanto la investigación como la discusión política sobre la necesidad de encontrar modelos alternativos con los cuales, como sociedad, fomentemos que se investigue en medicamentos que efectivamente estén al alcance de la población (tanto de las personas como de los países) y que estén en línea con aquello que necesitamos, especialmente aquello que está mayoritariamente desatendido (nuevos antibióticos, enfermedades que aquejan a población sin capacidad de pago, etc.).]

[Llevamos 14 años en el debate y, más que eso, asumiendo silenciosamente la enfermedad y pérdida de seres queridos causada por las falencias del actual modelo (los tres puntos de atrás)… Por ello tenemos bajo la lupa lo que pase en Ginebra, urgiendo a que se den pasos concretos que reflejen que lo principal es la innovación y el acceso a medicamentos y no el modelo que beneficia de manera billonaria a un grupo de corporaciones multinacionales farmacéuticas y sus accionistas.]

------------------------------------------------------------------------

###### [Las opiniones expresadas en este artículo son de exclusiva responsabilidad del autor y no necesariamente representan la opinión de Contagio Radio.] 
