Title: Kevin Mestizo Coicué and Eugenio Tenorio die following attack on Indigenous Guard
Date: 2019-08-12 11:39
Author: CtgAdm
Category: English
Tags: Association of Indigenous Councils in Northern Cauca, Cauca, Indigenous Guard
Slug: kevin-mestizo-coicue-and-eugenio-tenorio-die-following-attack-on-indigenous-guard
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/08/68280269_1879128068856236_2228074753644560384_o.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Foto: CRIC] 

[According to the Association of Indigenous Councils of Northern Cauca, a new attack against the Indigenous Guard on Saturday in the southwestern province of Cauca resulted in the death of two of its members, Kevin Mestizo Coicué and Eugenio Tenorio. Another five, including a minor, were injured.]

[Members of the  Indigenous Guard were shot at while driving on the road from Toribío to Caloto in northern Cauca to attend the local Coffee Festival. According to the Regional Indigenous Council of Cauca, the people injured have been identified as Leonel Coicué, Sandra Milena Pacue, Aurelino Ñuscue Julicue, Julio Taquinás Pilcué and Edinson Eduardo Rivera, a 7-year-old.]

[This is the second attack on the Indigenous Guard in less than 24 hours and the third in less than a month. On Aug. 9, armed assailants fired at a vehicle belonging to the National Protection Unit in Caloto. On July 26, four members of the Indigenous Guard were attacked while they rode in a car that they recovered from alleged FARC dissidents.]

[In commemoration of the International Day of the World’s Indigenous Peoples, the National Organization of Indigenous Peoples called for an end to the genocide of these ethnic people in the country. Since the signing of the peace deal, 158 activists have been assassinated. Ninety-four of them were killed after President Iván Duque took office.]

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
