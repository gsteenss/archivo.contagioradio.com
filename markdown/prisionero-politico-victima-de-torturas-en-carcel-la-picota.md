Title: Prisionero político víctima de torturas en Cárcel La Picota
Date: 2020-02-05 16:12
Author: CtgAdm
Category: Expreso Libertad
Tags: Defensa de derechos humanos, La Picota, Prisionero Político
Slug: prisionero-politico-victima-de-torturas-en-carcel-la-picota
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/02/WhatsApp-Image-2020-02-05-at-3.57.13-PM.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

La Brigada Jurídica Eduardo Umaña Mendoza denunció que Aimer Serrano, prisionero político fue víctima de torturas en cárcel La Picota, por un grupo de guardias del INPEC, cuando este iba a ser trasladado a la Cárcel de Valledupar, conocida como La Tramacúa.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Según la denuncia, un grupo de guardias ingresaron al patio 9 para sacar al prisionero que iba a ser trasladado a la Cárcel de la Tramacua, en ese momento Serrano fue víctima de torturas.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El abogado Uldarico Flórez, defensor de derechos humanos e integrante de la Brigada, señaló que "hubo gravísimas violaciones a derechos humanos, por parte de guardia que no tenían ninguna distinción".

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Asimismo, la Brigada Eduardo Umaña Mendoza expresó que, los tratos crueles e inhumanos infringidos en contra de Serrano, se realizaron bajo ordenes del director general del centro Penitenciario, Norberto Mojica. ([Le puede interesar: "En Colombia se encarcela el pensamiento crítico")](https://archivo.contagioradio.com/pensamiento-critico/)

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Serrano había realizado distintas denuncias sobre la corrupción al interior del centro penitenciario La Picota y ejercía una defensa de los derechos humanos, razón por la cuál la Brigada señala que el traslado a la Tramacúa, haría parte de una retaliación en contra del trabajo adelantado por el prisionero político.

<!-- /wp:paragraph -->
