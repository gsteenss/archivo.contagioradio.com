Title: "Participación Social será el corazón del proceso de paz ELN-Gobierno" Carlos Velandia
Date: 2017-01-19 13:47
Category: Entrevistas, Paz
Tags: ELN, Mesa de conversación de Quito
Slug: participacion-social-sera-el-corazon-del-proceso-de-paz-eln-gobierno-carlos-velandia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/10/marcha-de-las-flores101316_274.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [19 Ene 2017 ] 

Tras el anuncio del inicio de la fase pública de la mesa de conversaciones entre el ELN y el gobierno el próximo 7 de febrero, quedaron resueltas las dudas referente a la liberación de Odín Sáchez de Oca, que se espera se **realice el dos de febrero, y el trámite de los indultos a los integrantes de esta guerrilla.**

Después de la reunión, ambas delegaciones llegaron a un acuerdo que permitirá retomar la agenda de paz que se pactó desde el 30 de marzo de 2016, y que gira en torno a la participación social. De acuerdo con Carlos Velandia, gestor de paz de estos diálogos, “la democracia representativa se ha quedado muy corta y no consulta las realidades”, **motivo por el cual las reivindicaciones que se han gestado desde la sociedad será el corazón de este proceso.**

Uno de esos escenarios que se ha propuesto abrir el debate a la ciudadanía es la Mesa Social para la Paz, que ha expresado a través de un comunicado de prensa que saluda la noticia de la instalación de la fase pública y que realizará **100 espacios de encuentro y  el próximo 4 de febrero con la finalidad de ir recogiendo más voces**, organizaciones, plataformas y propuestas que puedan ser escuchadas en la mesa de conversaciones. Le puede interesar: ["Listos mecanismos de participación para la Mesa Social para la paz](https://archivo.contagioradio.com/listos-mecanismos-de-participacion-de-mesa-social-para-la-paz/)"

Sobre la posibilidad de declarar el cese bilateral al fuego, Velandia señala que “este podría ser un punto de llegada” más no arrancar desde allí, sin embargo afirma que lo que sí es posible es que **ambas partes vayan desescalando el conflicto a través de la disminución de acciones hostiles**. Además considera que uno de los temas que se tratarán con prontitud en la mesa, será la retención de personas que aún estén en manos del ELN.

<iframe id="audio_16560668" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_16560668_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
