Title: Mientras conflicto en Colombia persista será muy difícil lograr avances en desminado
Date: 2019-04-04 17:09
Author: CtgAdm
Category: DDHH, Entrevistas
Tags: CICR, dia internacional para la sensibilización contra las minas antipersona
Slug: mientras-conflicto-en-colombia-persista-sera-muy-dificil-lograr-avances-en-desminado-2
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/08/minas-colombia.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo] 

###### [4 Abr 2019] 

En conmemoración del 4 de abril, Día Internacional para la sensibilización contra las minas antipersona, el **Comité Internacional de la Cruz Roja (CICR)** advierte que en Colombia esta es una problemática que viene incrementándose de manera exponencial pasando de tener un crecimiento del 2017 al 2018 del 300% en el número de víctimas heridas por este tipo de artefactos.

**José Yesid Carrillo, oficial de campo de contaminación por armas del CICR** señala que Colombia continúa ocupando el segundo lugar en el ranking de países más afectados por este flagelo, superado únicamente por Afganistán, agregando que si se tienen en cuenta los datos del 2018, sigue siendo el tercer país con mayor número de víctimas por debajo de países como Camboya y Afganistán.

Según Carrillo, si se realiza una proyección con las 59 víctimas documentadas de enero a marzo en diferentes accidentes en los departamentos de Bolívar, Norte de Santander, Arauca, Chocó, Cordoba y Nariño, revelaría una tendencia del 300% que podría mantenerse durante este 2019.

**Resurgimiento de un fenómeno**

A propósito de los recientes hechos de desplazamiento en el sur de Córdoba,  José Yesid Carrillo afirma que este departamento **"es un caso ilustrativo de la tendencia nacional, pues Córdoba no registro víctimas en el 2017 pero en  2018 registró cinco víctimas y en lo que va de 2019 ha registrado tres en los municipios de Puerto Libertador, en San José de Uré y Tierra Alta"** demostrando la realidad que crecer exponencialmente.

Según el integrante del CICR este incremento demuestra que ante la presencia y contaminación por este tipo de armamento, algunas poblaciones tienden a autoconfinarse, restringiendo su movilidad y "dejando  de realizar actividades cotidianas como cultivar o pescar lo que tiene un impacto en su vida nutricional y educativa".

Aunque se estableció que para el 31 de diciembre de 2021 Colombia debía ser declarada libre de sospecha de contaminación por armas,  Carrillo advierte que **"nos encontramos en una coyuntura problemática"** pues mientras el conflicto armado persista en el país serán muy pocos los avances en la erradicación.

<iframe id="audio_34099122" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_34099122_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
