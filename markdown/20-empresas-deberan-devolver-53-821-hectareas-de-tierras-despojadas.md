Title: 20 Empresas deberán devolver 53.821 hectáreas de tierras despojadas
Date: 2016-11-03 13:42
Category: DDHH, Nacional
Tags: despojo de tierras, Empresas palmeras, Paramilitarismo
Slug: 20-empresas-deberan-devolver-53-821-hectareas-de-tierras-despojadas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/08/tierras.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio ] 

###### [3 de Nov 2016] 

**Magistrados de Restitución de Tierras condenaron a 20 empresas** entre las que figuran Argos S.A., Aglogold Ashanti exploraciones Choco Colombia; Bancolombia; CI Banana S.A.; Sociedad las Palmas S.A.; Palmas de Bajirá; entre otras, a **devolver 53.821 hectáreas de tierras despojadas a campesinos** y en donde sucedieron actos como extorsiones, asesinatos, desaparición forzada y desplazamientos causados por paramilitares, agentes del Estado y guerrillas.

A su vez los Magistrados señalaron que las empresas “**hicieron caso omiso de la notoriedad y conocimiento público sobre la violencia generalizada** que padecían esas regiones” en donde fueron adquiridas las tierras. Las órdenes de restitución se dieron en 24 sentencias.

De igual modo, no se creyó conveniente una **compensación en dinero para las empresas debido a que no cumplieron con los requisitos que exige la ley sobre la buena fe,** que básicamente consiste en verificar que quién vende la tierra es el verdadero dueño, segundo que la compra se hace en un precio justo y tercero que no hayan existido hechos de violencia que motivaran la venta.

Referente a los títulos adquiridos por empresas mineras, los Magistrados ordenaron **que se suspendan los estudios y tramites de exploración y explotación minera hasta “que no se cumpla las consultas populares requeridas en territorios de comunidades indígenas”.**

Las empresas son:

**Argos S.A. (Fiducor S.A.)**

Municipio y ubicación del previo a devolver: Carmen de Bolívar – Bolívar

Hecho notorio de violencia: Masacre paramilitar de El Salado en el año 2000

**Continetal Gold Limited Aglogold, Ashanti Exploraciones Choco-Colombia**

Municipio y ubicación del previo a devolver: Bagadó - Chocó

Hecho notorio de violencia: Masacres, despojos, desplazamiento y bombardeos e incursión de guerrillas, ejército  y paramilitares de las AUC

**Bancolombia**

Municipio y ubicación del previo a devolver: Valncia – Córdoba

Hecho notorio de violencia: Contexto de violencia paramilitar bajo el control de los Castaño y alias “Don Berna”.

**CI Bananas S.A.**

Municipio y ubicación del previo a devolver: Mutatá - Antioquia

Hecho notorio de violencia: Bloque Bananero de las Autodefensas Campesinas de Córdoba y Urabá en el año 1996

**Sociedad Las Palmas Ltda**

Municipio y ubicación del previo a devolver: Sabana de Torres – Santander

Hecho notorio de violencia: Acciones y amenazas por parte de la guerrilla de las FARC-EP en el año 1992

**Palmas de Bajirá**

Municipio y ubicación del previo a devolver: Mutatá - Antioquia

Hecho notorio de violencia: Bloque Bananero de las Autodefensas Campesinas de Córdoba y Urabá en el año 1998

**Palmagan S.A**

Municipio y ubicación del previo a devolver: Mutatá - Antioquia

Hecho notorio de violencia: Bloque Bananero de las Autodefensas Campesinas de Córdoba y Urabá en el año 1997

**Todo Tiempo S.A**

Municipio y ubicación del previo a devolver: Mutatá - Antioquia

Hecho notorio de violencia: Bloque Bananero de las Autodefensas Campesinas de Córdoba y Urabá en el año 1996

**Sociedad Agropecuaria Carmen de Bolívar S.A**

Municipio y ubicación del previo a devolver: Carmen de Bolívar – Bolívar

Hecho notorio de violencia: Masacre Paramilitar El Salado del año 2000

**A.Palacios S.A.S**

Municipio y ubicación del previo a devolver: Turbo - Antioquia

Hecho notorio de violencia: Bloque Bananero de las Autodefensas Campesinas de Córdoba y Urabá en el año 1997

**Sociedad Montecarmelo**

Municipio y ubicación del previo a devolver: Agustín Codazzi - Cesar

Hecho notorio de violencia: Amenazas y enfrentamientos armados entre FARC-EP, ELN, Ejército y Paramilitares, a finales de los años 90 y principios de los años 2000

**Sociedad Agropecuaria W2 S.A.S**

Municipio y ubicación del previo a devolver: Puerto Gaitán - Meta

Hecho notorio de violencia: Control de Autodefensas del Meta y Vichada, desplazamientos provocados por las FARC-EP y bandas emergentes de las AUC

**Agroservicios San Simón S.A**

Municipio y ubicación del previo a devolver: Carmen de Bolívar - Bolívar

Hecho notorio de violencia: Masacre Paramilitar El Salado, en el año 2000

**Sociedad Jorge Herrera S.C.S**

Municipio y ubicación del previo a devolver: Carmen de Bolívar - Bolívar

Hecho notorio de violencia: Masacre Paramilitar El Salado, en el año 2000

**Agropecuario Tocaloa**

Municipio y ubicación del previo a devolver: Carmen de Bolívar - Bolívar

Hecho notorio de violencia: 1995- 1997 incursión Paramilitar, año 2000 Masacre de El Salado

**Urballanos Cía Ltda**

Municipio y ubicación del previo a devolver: Granada- Meta

Hecho notorio de violencia: Enfrentamientos entre FARC-EP, Ejército y grupos Paramilitares

######  Reciba toda la información de Contagio Radio en su correo [[bit.ly/1nvAO4u]
