Title: Así conmemora Colombia la semana del Detenido - desaparecido
Date: 2019-05-27 15:16
Author: CtgAdm
Category: DDHH, eventos
Tags: colombia, desaparecidos, Desaparición forzada, MOVICE
Slug: asi-conmemora-colombia-la-semana-del-detenido-desaparecido
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/08/movice-e1502993685776.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Contagio Radio 

Del 27 al 31 de mayo se conmemora internacionalmente la semana del Detenido-Desaparecido, que para el caso de Colombia y según el Movimiento de Víctimas de Crímenes de Estado**, ha dejado más de 82.998 víctimas en el país**. Razón por la cuál, diversas organizaciones sociales y defensoras de derechos humanos se reunirán, bajo la consigna "te seguimos buscando", en exigencia de respuesta a preguntas como ¿en dónde están?, ¿por qué los desaparecieron? y ¿quiénes lo hicieron?

Para esta ocasión, más de 4 departamentos contarán con diversas actividades en torno a la visibilización de este hecho de violencia. En **Bogotá**, durante toda esta se estará presentando en el Centro de Memoria, Paz y Reconciliación, la proyección de una vídeo instalación denominada **"Volver a pasar por el Corazón".** Igualmente, los días 29,30 y 31 de mayo, en la Universidad Distrital, sede La Macarena se llevará a cabo una una galería de la memoria, y allí mismo, el 30 de mayo se desarrollará una ponencia sobre memoria. Finalmente, en el Bunker de la Fiscalía, el 30 de mayo, a las 4 pm se realizará un plantón.

En el departamento de Caldas, se realizará una muestra artística en la ciudad de **Manizales**, en la Universidad Nacional, sede Palogrande, el 29 de mayo desde las 4 hasta las 6 pm. De igual forma en **Sincelejo-Sucre**, el 30 de mayo se hará un acto simbólico en el Parque Santander. (Le puede interesar:["En 2019 Unida de Búsqueda de desaparecidos llegará a 17 territorios"](https://archivo.contagioradio.com/unidad-de-busqueda-de-desaparecidos/))

Asimismo en **Barraquilla,** el día 29 de mayo, sobre las 2 de la tarde, se llevará a cabo el conversatorio y un acto de memoria, en el salón de la Asamblea Departamental, mientras que en el departamento de Antioquia, el día 30 de mayo se realizará un plantón por las Víctimas de Desaparición Forzada en en el Centro Administrativo Alpujarra.

Al sur del país, en el departamento del Valle del Cauca, se desarrollará el V Encuentro Departamental de Familiares de Personas Desaparecidas, en **Cali**. El evento se hará en la Universidad Icesi, los días 30 y 31 de mayo desde las 8:30 am.

Igualmente en el departamento del **Meta,** el día 29 de mayo en el Parque infantil de Villavicencio, a las 3pm, se hará un acto simbólico convocado por los familiares de las víctimas con la intención de dignificar a sus desaparecidos.

###### Reciba toda la información de Contagio Radio en su correo o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por Contagio Radio. 
