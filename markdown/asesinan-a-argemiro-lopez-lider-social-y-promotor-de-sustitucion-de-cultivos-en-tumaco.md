Title: Asesinan a Argemiro López, líder social y promotor de sustitución en Tumaco
Date: 2019-03-18 15:42
Author: CtgAdm
Category: Líderes sociales, Nacional
Tags: asesinato de líderes sociales, campesinos asesinados en Tumaco, Plan de Sustitución de Cultivos de Uso ilícito
Slug: asesinan-a-argemiro-lopez-lider-social-y-promotor-de-sustitucion-de-cultivos-en-tumaco
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/03/argemiro-lopez.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Contagio Radio 

###### 18 Mar 2019 

El pasado 17 de marzo a las 7:30 de la noche, hombres armados irrumpieron en la vivienda de **Argemiro López, líder social e integrante de la Junta de Acción Comunal**, disparando contra su humanidad y la de su esposa. López quien promovía la sustitución de cultivos de uso ilícito en el corregimiento de **La Guayacana en Tumaco** fue asesinado mientras su compañera continúa en delicado estado de salud.

Según Leder Rodríguez, **vicepresidente de la Federación de Acción Comunal de Nariño,** Argemiro ya había recibido amenazas previas vinculadas a su labor con la sustitución de cultivos "a él ya venían diciéndole que en cualquier momento podían matarlo y ya lo había manifestado en las reuniones que tenemos aquí" indica el líder tumaqueño.  [(Lea también: Lideresa Maritza Ramírez muere tras ser víctima de ataque en Tumaco) ](https://archivo.contagioradio.com/lideresa-maritza-ramirez-pierde-vida-tras-ser-victima-ataque-tumaco/)

Rodríguez  indica que entre los actores armados que operan en Tumaco destacan grupos como el **frente Oliver Sinisterra, perteneciente a las disidencias de las FARC, las Guerrillas unidas del Pacífico y  las Águilas Negras**, sin embargo a ninguno se le puede adjudicar la autoridad del crimen pues no hay suficiente información. "es un collage de amenazas en contra de los líderes"asegura el líder comunal mientras denuncia que la Fiscalía no ha esclarecido ninguno de los crímenes que se viven en este municipio de Nariño. [(Lea también Tumaco, municipio con mayor número de líderes asesinados desde 2016).](https://archivo.contagioradio.com/cauca-narino/)

El Vicepresidente de la organización reitera que se ha realizado un llamado al Gobierno para hacer un trabajo en conjunto y establecer una seguridad propia que venga desde las mismas Juntas de Acción Comunal, **"nosotros sabemos cómo operan estos grupos y sabemos cómo cuidarnos, pero si no tenemos las herramientas es imposible, hoy en día estamos en peligro todos"** señala.  [(Lea también: Alfonso Correa, ambientalista y líder comunal fue asesinado en Sácama, Casanare)](https://archivo.contagioradio.com/alfonso-correa-ambientalista-y-lider-comunal-fue-asesinado-en-sacama-casanare/)

<iframe id="audio_33480110" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_33480110_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
