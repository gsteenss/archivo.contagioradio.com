Title: Más de 4.000 indígenas se reúnen en el IX Congreso Nacional de la ONIC
Date: 2016-10-10 17:48
Category: Nacional, Política
Tags: IX Congreso ONIC, ONIC
Slug: mas-de-4-000-indigenas-se-reunen-en-el-ix-congreso-nacional-de-la-onic
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/10/ONIC.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:ONIC] 

###### [10 de Oct 2016]

Desde el 8 de octubre inició el IX Congreso Nacional de la ONIC,  evento en el que participan **más de 4.000 indígenas pertenecientes a las 47 filiales de esta organización y que pretender establecer las directrices** que proyectaran el trabajo y líneas de acción de cara al momento que atraviesa el país.

El evento se lleva a cabo en el colegio Claretiano, que se encuentra ubicado en la localidad de Bosa y que a su vez es territorio Mhuysca. La agenda del evento girará en torno **al fortalecimiento de las nociones de territorio, Unidad, Cultura y Autonomía que se desarrollarán a través de diferentes paneles.**

“El reconocimiento del Gobierno Indígena no lo otorgan las instituciones coloniales ni republicanas, [son aspectos formales para seguir controlando la vida jurídica y política de todos los pueblos del mundo](https://archivo.contagioradio.com/victimas-del-pais-exigen-participacion-directa-en-pacto-politico-nacional/)…” afirmo Fernando Arias, actual vocero de la organización Nacional de Indígenas de Colombia

Entre las actividades se desarrollará el **12 de octubre, la movilización en conmemoración al día de la resistencia de los Pueblos** y el 13 de octubre se realizará el acto de cierre con la elección del Consejo Mayor de Gobierno de la ONIC, evento al que el público tendrá acceso despúes de las 4:00pm
