Title: Alcaldía Distrital se niega a firmar pacto para proteger los Cerros Orientales
Date: 2017-08-09 15:00
Category: Ambiente, Nacional
Tags: Bogotá, cerros orientales
Slug: se-crea-pacto-para-proteger-los-cerros-orientales
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/08/Salitre_2007_8.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Wikipedia] 

###### [09 Ago 2017] 

**La Alcaldía de Bogotá y diferentes instituciones distritales, se negaron a firmar un pacto para proteger los Cerros Orientales** en la capital y tomar medidas frente a la protección y defensa de la biodiversidad de este espacio. Sin embargo, organizaciones sociales y congresistas manifestaron que continuarán en la vigilancia y amparo a este ecosistema amenazado por construcciones.

En el pacto, tanto la Alcaldía como las instituciones distritales se comprometían a evitar nuevas urbanizaciones en el área no ocupada de la franja de adecuación y cambios en el uso del suelo, manteniendo las **más de 500 hectáreas como rurales**. (Le puede interesar: ["Alcaldía de Bogotá no recibió 2339 árboles para la reserva Thomas Van Der Hammen"](https://archivo.contagioradio.com/alcaldia-de-bogota-no-recibio-donacion-de-arboles-para-reforestar-reserva-van-der-hammen/))

Además, se les recomendaba **destinar recursos suficientes para que se cumpla el fallo que protege los Cerros y limitar la edificación**, y así proteger el derecho al paisaje e invertir en programas para los barrios cercanos a los Cerros Orientales que propendan por las iniciativas de economía solidaria y popular, y reconocer a las comunidades campesinas que se encuentran en el territorio junto con su actividad agropecuaria, siempre y cuando sea sostenible con el medio ambiente.

Los congresistas como Ángela María Robledo de la Alianza Verde y Alirio Uribe, del Polo Democrático, promotores del pacto, manifestaron que, pese a que las instituciones distritales no se hayan adherido a este acuerdo, **realizaran acompañamiento a las comunidades y exigirán el cumplimiento del fallo**, de igual forma prometieron ejercer control en las entidades que tienen a su cargo la defensa y protección de los Cerros y respaldar los proyectos de ley que contribuyan a la protección de los mismos.

Por su parte las organizaciones que trabajan en la defensa de los Cerros y apoyan el pacto, comprometieron a realizar el seguimiento al cumplimiento del fallo, **hacer control social a las acciones del Estado** y la ejecución de los programas y proyectos que involucren la franja de adecuación y la reserva Thomas Van Der Hammen. (Le puede interesar:["505 hectáreas de los Cerros Orientales estarían en peligro"](https://archivo.contagioradio.com/bogotanos-van-a-defender-los-cerros-orientales/))

[Pacto Cerros Orientales 2017-08-03](https://www.scribd.com/document/355931189/Pacto-Cerros-Orientales-2017-08-03#from_embed "View Pacto Cerros Orientales 2017-08-03 on Scribd") by [Contagioradio](https://www.scribd.com/user/276652802/Contagioradio#from_embed "View Contagioradio's profile on Scribd") on Scribd

<iframe id="doc_73489" class="scribd_iframe_embed" src="https://www.scribd.com/embeds/355931189/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-8pnWbhfDO1gbv7rNwuZJ&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="0.7068965517241379"></iframe>

######  Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
