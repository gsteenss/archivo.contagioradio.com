Title: “Medios de comunicación deben reflejar la diversidad y la realidad de las mujeres”
Date: 2017-05-03 13:37
Category: Mujer, Nacional
Tags: dia mundial de la libertad de prensa, Libertad de Prensa, Mujees, Prensa
Slug: medios-de-comunicacion-deben-ser-reflejo-de-la-diversidad-de-mujeres
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/11/Mujeres-marchan-por-la-paz.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo] 

###### [03 May. 2017] 

Luego de 2 días de intenso trabajo en el Seminario Internacional: Las mujeres y los medios en los procesos de paz, el balance en términos generales ha sido positivo, y fue la oportunidad de convertir el espacio, en un diálogo donde **mujeres periodistas de Colombia y el mundo, mostraron su papel en el periodismo y en la comunicación en tiempos de paz.**

**Gema Granados integrante de la Red Colombiana de Periodistas con Visión de Género** aseguró “hemos tenido invitadas de Guatemala, El Salvador, Chile, Argentina, México y del Congo, hemos movilizado periodistas de varios territorios de Colombia que han compartido sus experiencias, sus logros y dificultades para ejercer el periodismo con esta perspectiva de la defensa de los derechos de las mujeres”.

Uno de los principales mensajes que pretende dejar este seminario es que **sin la participación de las mujeres y sin su visibilización como ciudadanas con derechos y activas no se podrá hacer la paz** “la paz tiene que venir con las mujeres y tiene que permitir a las mujeres espacios para desarrollarse en condiciones de igualdad, con respeto a los derechos y sin violencia” recalca Granados. Le puede interesar: [Comunicación para narrar el posconflicto, una mirada desde las mujeres](https://archivo.contagioradio.com/comunicacion_posconflicto_mujeres/)

Además, agrega la integrante de la Red, hay que contar las historias de las mujeres “la historia de Colombia no se puede contar sin las mujeres, la paz no se puede contar ni hacer sin las mujeres. **Cada vez que hagamos una noticia preguntémonos si en esa noticia también recojo las realidades de las mujeres,** sus historias, sus miradas, porque tendemos a contar la historia a través de la mirada masculina”. Le puede interesar: [Mujeres excombatientes en la implementación de los Acuerdos de Paz](https://archivo.contagioradio.com/el-papel-de-las-mujeres-excombatientes-en-la-paz/)

Asegura la Red Colombiana de Periodistas con Visión de Género que **entre el 2002 y el 2016, solo 179 noticias publicadas en los medios de comunicación hablaron de las mujeres en el proceso de paz** “eso frente a un mar de miles de miles de noticias sobre el tema (…) no puede haber libertad de prensa si las mujeres periodistas y ciudadanas no encuentran espacios en los medios de comunicación para contar lo que están haciendo” reitera Granados.

Quien también reconoce el papel primordial que ha tenido el movimiento social en el proceso de paz, quienes lograron incidir en los acuerdos, además de visibilizar a las mujeres como víctimas y sobre todo como sobrevivientes y constructoras de paz. Le puede interesar: [Foro Internacional busca la participación política de mujeres afro](https://archivo.contagioradio.com/foro-internacional-busca-la-participacion-politica-de-mujeres-afro/)

**“No puede haber libertad de prensa completa si algunos sujetos de la historia no tienen acceso a los medios** de comunicación para contar sus realidades y en este caso son las mujeres que son la mitad de la población de Colombia y el mundo, que no pueden quedar por fuera de las noticias” relata Granados.

Así mismo, dice Granados, el seminario logró mostrar las grandes tareas que tienen pendientes los medios, como por ejemplo **cómo contar las historias de las mujeres y visibilizarlas por fuera de los estereotipos,** de los roles tradicionales y no hacerlas caer en la victimización. Le puede interesar: [Las mujeres tejen paz desde su territorio.](https://archivo.contagioradio.com/las-mujeres-tejen-paz-desde-los-territorios/)

“Hay que visibilizar las mujeres en su diversidad, las mujeres están en lo público, construyendo la paz, generando procesos de desarrollo en el país. Hay mujeres que no son solamente son heterosexuales, hay mujeres que están en la diversidad sexual, hay mujeres indígenas, hay mujeres afro, jóvenes, adultas, adultas mayores y **los medios de comunicación deben ser el reflejo de la diversidad y la realidad de las mujeres”** indicó Granados.

<iframe id="audio_18484832" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_18484832_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

######  Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio.](http://bit.ly/1ICYhVU)

<div class="ssba ssba-wrap">

</div>
