Title: Nuevo comandante de la XIII Brigada estaría implicado en 26 falsos positivos
Date: 2019-01-22 18:20
Author: AdminContagio
Category: DDHH, Entrevistas
Tags: Caso de Falsos Positivos, colectivo de Abogados José Alvear Restrepo, jurisdicción especial para la paz, Marcos Evangelista Pinto Lizarazo, Sebastian Escobar
Slug: informe-presentado-la-jep-vincula-al-comandante-la-xiii-brigada-falsos-positivos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/01/Du4xhaBXQAAW_T2-770x400.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: @Ccajar 

###### 22 ene 2019 

[El Colectivo de Abogados José Alvear Restrepo (Cajar) presentó a la Jurisdicción Especial para la Paz (JEP) un informe que vincula **al brigadier general Marcos Evangelista Pinto Lizarazo a 26 ejecuciones extrajudiciales**, mal llamados “falsos positivos”, presuntamente cometidos por dos batallones bajo su mando entre el 2006 y 2009.]

[El colectivo señalo al brigadier general Pinto, actualmente el comandante de la XIII Brigada del Ejército, de participar en 26 ejecuciones extrajudiciales mientras se desempeñaba como comandante del Batallón “Atanasio Girardot” entre octubre de 2006 y abril de 2007 y como comandante del Batallón “Magdalena”, entre diciembre de 2007 y septiembre de 2009.]

[Según Sebastián Escobar, abogado de Cajar, **el brigadier general se encuentra bajo investigación por la Oficina de Paz y Derechos Humanos de la alcaldía de Neiva, Huila** por casos de ejecuciones extrajudiciales. Además, Cajar, junto a los familiares de las víctimas, ha pedido que la Fiscalía General de la Nación se encargue de investigar a Pinto; sin embargo, la **Fiscalía se ha declarado incompetente de asumir este caso** dado que el brigadier general goza de "fuero constitucional" por su rango militar. ]

El informe resaltó que el brigadier general se podría encontrar responsable por estos crímenes, de "práctica sistemática y generalizada," dado que los batallones bajo su mando pertenecen a unidades militares con el mayor número de casos de ejecuciones extrajudiciales, según Human Rights Watch y la Corte Penal Internacional.

Actualmente, la justicia ordinaria ha condenado a varios subalternos de estas dos brigadas, incluyendo un cabo, un sargento segundo y soldados profesionales. Además, el Sargento Segundo William Andrés Capera Vargas, condenado a 20 años de prisión por desaparición forzada y homicidio agravado, afirmó en interrogatorio que no comprendía como el entonces coronel Pinto no pudo darse "cuenta que en la unidad que lidera se estén realizando falsos positivos o que le muevan armas dentro de las oficinas."

Dado que estos implicados se han sometido a la justicia transicional, el Cajar, junto a los familiares de las victimas, entregaron este informe con la esperanza de que la JEP "establezca la verdad sobre toda la cadena de mando que operó detrás de los crímenes que involucran a este alto mando militar y que se reconozcan estos graves hechos como crímenes de Estado."

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
