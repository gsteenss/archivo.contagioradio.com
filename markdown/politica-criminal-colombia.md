Title: Política criminal en Colombia
Date: 2018-05-29 16:59
Category: Expreso Libertad
Tags: política criminal, presos politicos
Slug: politica-criminal-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/05/crisis-carcelaria1-e1463780527863.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto:ICRC 

###### 29 May 2018 

David Bonilla y Estefanía Herrera, abogados de la Corporación Solidaridad Jurídica, hablaron en los micrófonos del Expreso Libertad, sobre lo **que es una política criminal en el país, que busca generar un control social, enmarcado en el derecho penal, prohibiendo conductas que se consideran erradas**.

Hecho al que de acuerdo a los abogados **se suman los castigos que se han impuesto a quienes por pensar diferente debe permanecer "aislados" de la sociedad y en consecuencia reprendidos para que no vuelvan a cometer actos de rebelión**. Escuche en este Expreso Libertad la definición de una política criminal y sus implicaciones contra el pensamiento crítico.

<iframe id="audio_30797231" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_30797231_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Encuentre más información sobre Prisioneros políticos en[ Expreso libertad ](https://archivo.contagioradio.com/programas/expreso-libertad/)y escúchelo los martes a partir de las 11 a.m. en [Contagio Radio](https://archivo.contagioradio.com/alaire.html) 
