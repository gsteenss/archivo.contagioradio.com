Title: La memoria ambiental para el “retorno in situ”
Date: 2020-09-02 11:29
Author: Censat
Category: Columnistas, Opinion
Tags: CENSAT, Censat agua Viva, memoria de colombia
Slug: la-memoria-ambiental-para-el-retorno-in-situ
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/09/arbol-compadre-bototo.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:heading {"align":"right","level":6,"textColor":"cyan-bluish-gray"} -->

###### Foto por: [Censat](https://censat.org/) {#foto-por-censat .has-text-align-right .has-cyan-bluish-gray-color .has-text-color}

<!-- /wp:heading -->

<!-- wp:paragraph -->

Cuando hablamos de memoria ambiental en el marco del conflicto armado, no hablamos de un inventario de ecosistemas perdidos o afectados, aunque ciertamente hace falta contar con ese tipo de registros.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Para nosotros, la memoria ambiental es ante todo una práctica transformadora de compartir recuerdos y sentimientos relacionales sobre el impacto que genera la guerra en los territorios al destruir la naturaleza. Y también es una práctica de sanación para el territorio cuando permite que la gente se reencuentre con sus entornos naturales que han sido víctimas igual que ellos y ellas, resignificando sus relaciones con las aguas, montañas,  selvas y la tierra.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

La memoria ambiental ofrece la posibilidad de ampliar nuestro horizonte analítico en los trabajos de memoria, pues va más allá de indagar por los impactos causados por el conflicto armado entre humanos. Nos permite preguntarnos por

<!-- /wp:paragraph -->

<!-- wp:list -->

-   1 el daño que dejó el conflicto en la naturaleza por ser naturaleza

<!-- /wp:list -->

<!-- wp:list -->

-   2 las rupturas causadas en el relacionamiento material, emocional y simbólico entre seres humanos y naturaleza

<!-- /wp:list -->

<!-- wp:list -->

-   3 las secuelas en las identidades y marcas en los cuerpos como consecuencia de las prácticas cotidianas cambiadas a causa de la destrucción de la naturaleza.

<!-- /wp:list -->

<!-- wp:heading {"level":5} -->

##### En este escrito nos centraremos en las rupturas de los relacionamientos.

<!-- /wp:heading -->

<!-- wp:paragraph -->

El conjunto de relaciones entre naturaleza y seres humanos es fuertemente alterado en los lugares de la guerra cuando los ríos y lagunas son usados como botaderos de cuerpos, las selvas bombardeadas, la tierra minada.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":4} -->

#### Hay quienes entienden estas destrucciones y las fracturas que generan como un “desplazamiento *in situ*”.

<!-- /wp:heading -->

<!-- wp:paragraph -->

Un desplazamiento que sufren quienes logran resistir en sus territorios a pesar de las crueldades de la guerra o incluso quienes retornan a sus lugares desde donde fueron desplazados, pero donde las relaciones que conformaron esos territorios ya no son las mismas.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Ríos que se convirtieron en símbolo de la muerte y ya no permiten ni pesca ni paseo de olla. Bosques despedazados que ya no alojan las anécdotas de la infancia sino las esquirlas de las bombas y los recuerdos de quienes murieron allí. Tierra fértil convertida en un desafío perverso de una ruleta rusa donde el caminar puede activar una mina y costar la vida.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Existe un vacío en la comprensión del impacto que los daños en la naturaleza generan en las poblaciones, daños que significan rupturas en dimensiones no sólo materiales, sino simbólicas, espirituales y emocionales.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

La destrucción de entornos naturales puede ser traumática y llevar a depresiones y crisis identitarias profundas porque nuestras identidades se construyen y co-construyen con un referente histórico-geográfico y también a diario gracias al permanente intercambio con el entorno biofísico y social. El arrasamiento del entorno natural tiene por lo tanto impactos inmediatos y de largo alcance en la identidad y en las relaciones sociales provocando vacíos de sentido de pertenencia causando la vivencia de “desplazamiento *in situ*”.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":5} -->

##### La memoria ambiental no puede deshacer estas fracturas.

<!-- /wp:heading -->

<!-- wp:paragraph -->

Solo puede ayudar a reflexionar sobre ellas, a expresar los dolores que generan y a recurrir a los lugares donde ocurrieron estos hechos buscando resignificarlos.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Volver a los lugares desde una voluntad propia es permitirse un “retorno *in situ*” cuando se logra establecer nuevas relaciones con los entornos naturales que igual que las comunidades no fueron causantes sino víctimas del conflicto; un ejercicio de sanación entre iguales.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Reconectarse con la naturaleza sin dejar atrás lo vivido, incorporando los recuerdos y dolores en este nuevo encuentro entre viejos amigos es una tarea de años. De hecho, en muchos territorios, esta práctica del “retorno *in situ*” hace parte de la vida cotidiana tanto a nivel individual como comunitario:

<!-- /wp:paragraph -->

<!-- wp:heading {"level":5} -->

##### Un ritual con el río para regresar con él a pesar de los cuerpos hundidos, una peregrinación por la montaña para volver a sentir su fuerza y no la de las bombas caídas, una siembra en minga para recomponer la relación con la tierra.

<!-- /wp:heading -->

<!-- wp:paragraph -->

Estas y muchas otras prácticas de resignificar las relaciones con la naturaleza son caminos para sanar las fracturas generadas por la guerra, son formas de resistir como territorio.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Con la memoria ambiental solo le damos nombre a una dinámica social que transcurre a diario en Colombia como expresión de reencuentro y resistencia.

<!-- /wp:paragraph -->

<!-- wp:separator -->

------------------------------------------------------------------------

<!-- /wp:separator -->

</p>
<!-- wp:heading {"level":6} -->

###### Las opiniones expresadas en este artículo son de exclusiva responsabilidad del autor y no necesariamente representan la opinión de Contagio Radio.

<!-- /wp:heading -->

<!-- wp:paragraph -->

[Nuestros columnistas](https://archivo.contagioradio.com/opinion/columnistas/)

<!-- /wp:paragraph -->
