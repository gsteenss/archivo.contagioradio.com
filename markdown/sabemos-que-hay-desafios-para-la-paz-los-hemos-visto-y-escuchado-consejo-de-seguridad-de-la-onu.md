Title: Sabemos que hay desafíos para la paz, los hemos visto y escuchado: Consejo de Seguridad de la ONU
Date: 2019-07-14 21:26
Author: CtgAdm
Category: Paz, Política
Tags: Consejo de Seguridad de la ONU, Implementación del Acuerdo
Slug: sabemos-que-hay-desafios-para-la-paz-los-hemos-visto-y-escuchado-consejo-de-seguridad-de-la-onu
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/ONU-Caldono.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/ONU-Caldono-Seguridad.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: @MisionONUCol  
] 

Con un recorrido por el Espacio Territorial de Capacitación y Reincorporación (ETCR) de Caldono, Cauca, concluyó la visita de los 15 países miembros del **Consejo de Seguridad de la ONU** a Colombia. Tras su visita, la delegación  reafirmó su compromiso con la implementación del acuerdo, pero a su vez expresó su preocupación por el asesinato de defensores de DD.HH y excombatientes, a quienes tuvo la oportunidad de escuchar y reiterar su respaldo.

Junto al **Jefe de la Misión de Verificación de la ONU en Colombia, Carlos Ruiz Massieu**, representantes del Gobierno  autoridades locales y de FARC,  la delegación conoció de primera mano las preocupaciones e intereses de quienes se encuentran en terreno y cómo viene desarrollándose la implementación del acuerdo, además, recorrieron la zona y visitaron los proyectos productivos del sector.

**“Ellos nos han expresado su compromiso con el proceso de paz, pero también nos han hecho saber sus preocupaciones, en particular sobre los asesinatos que ha habido de líderes sociales y defensores de derechos humanos, preocupación que compartimos en el Consejo y que lo llevamos como un elemento a considerar”,** afirmó el  presidente de la Misión, el embajador peruano Gustavo Meza-Cuadra,

"Sabemos que hay desafíos y los hemos visto, escuchado, entre otros el tema de algunos asesinatos de líderes sociales y también la necesidad de un apoyo en términos de infraestructura social de parte del gobierno”, agregó Meza quien también señaló que el presidente Duque, por medio de una carta pidió la extensión de la Misión de la ONU un año más.

![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/ONU-Caldono-Seguridad-1024x683.jpg){.aligncenter .wp-image-70451 .size-large width="1024" height="683"}

La visita estuvo acompañada por el delegado de Consejo Nacional de Reincorporación, Pastor Alape, y el senador Pablo Catatumbo, quienes también dieron a conocer su preocupación y la del partido FARC por los asesinatos contra excombatientes y la importancia de tener garantías para una efectiva reincorporación y acceso a la tierra para quienes trabajan en zonas rurales.

> La presencia del Consejo de Seguridad de la ONU en el Cauca despertó esperanzas en las comunidades del departamento. Gobernador hace compromiso con alcalde de Caldono, comunidad del ETCR y cabildos del municipio en pro de la construcción de paz [pic.twitter.com/taikwQzZTv](https://t.co/taikwQzZTv)
>
> — Pastor Alape Lascarro (@AlapePastorFARC) [July 13, 2019](https://twitter.com/AlapePastorFARC/status/1150134190295388161?ref_src=twsrc%5Etfw)

<p style="text-align: justify;">
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
</p>
### Diálogos de la ONU con otros sectores de la sociedad 

Durante su visita al Cauca, los delegados también se reunieron con líderes y lideresas sociales, incluyendo a las comunidades étnicas, escucharon sus preocupaciones sobre la protección en los territorios y ratificaron su acompañamiento para garantizar garantizar la seguridad en esta zona.

El día anterior, también se reunieron con los congresistas que hacen parte de las Comisiones de Paz del Senado y la Cámara de Representantes, donde se destacó la importancia de avanzar en un diálogo nacional para avanzar en la implementación del Acuerdo. [(Lea también: Organizaciones sociales piden al Consejo de Seguridad de la ONU reanudar diálogos con el ELN)](https://archivo.contagioradio.com/organizaciones-sociales-piden-al-consejo-de-seguridad-de-la-onu-reanudar-dialogos-con-el-eln/)

Aquella misma tarde el Consejo se reunió con los representantes del Sistema Integral de Verdad, Justicia, Reparación y No Repetición quienes se refirieron a **la importancia de respetar la integralidad del Acuerdo, superar la polarización  y asegurar recursos para el desarrollo de su trabajo en territorio.**

La delegación que contó con diplomáticos de Perú, , África del Sur, Reino Unido, República Dominicana,  Kuwait, China, Guinea Ecuatorial, Alemania, Indonesia, Francia, Rusia, Estados Unidos, Bélgica, Costa de Marfil, y Polonia regresó a Nueva York este 14 de julio mientras que el 19 de julio, el jefe de la Misión de Verificación, Carlos Ruiz Massieu presentará su informe ante el Consejo de Seguridad.

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
