Title: Estado incumple acto de perdón ante familia de joven asesinado por militares
Date: 2016-04-19 15:53
Category: DDHH, Nacional
Tags: bolivar, CIDH, crímenes de estado, Omar Zuñiga
Slug: estado-incumple-acto-de-perdon-ante-familia-de-joven-asesinado-por-militares
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/04/Omar-Zuñiga.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Cajar 

###### [19 Abr 2016] 

Tras 24 años de impunidad en el caso de la desaparición forzada y ejecución extrajudicial de Omar Zuñiga, en San Jacinto, Bolívar. **El Estado colombiano había anunciado que este 21 de abril reconocería su responsabilidad en el crimen,** como primera acción de reparación suscrita ante la Comisión Interamericana Derechos Humanos, sin embargo, esta primera medida será incumplida por el gobierno, como lo denunció Carmen Zuñiga, hermana de la víctima.

De acuerdo con Carmen, el Estado “sigue atropellando a la familia y sigue incumpliendo con los compromisos internacionales”, pues el **Ministro de Interior, Juan Fernando Cristo, anunció que no podrá llegar a la cita pactada por otra serie de compromisos gubernamentales,** por lo cual dijo que el acto no se realizaría en horas de la mañana sino de la tarde, lo que para Carmen Zuñiga es una falta de respeto con las víctimas, de manera que no aceptarán el acto de reconocimiento del Estado.

Los hechos sucedieron el primero de junio de 1992. Omar de 24 años de edad, fue desaparecido y asesinado en San Jacinto por un grupo de **treinta hombres pertenecientes al Batallón de Fusileros de la Infantería de Marina de la I Brigada,** quienes ingresaron violentamente a la casa de Omar Zúñiga quien se encontraba con su madre. Al joven lo interrogaron sobre el paradero de guerrilleros y al no tener respuesta, los militares se lo llevaron junto a su madre, quien después de haber sido víctimas de tortura psicológica fue dejada en libertad, pero su hijo apareció muerto nueve días después con un disparo en el cráneo y la mandíbula fracturada.

Por ese crimen en 2011 fueron detenidos: Henry Mauricio Rodríguez Botero, coronel de la Infantería de Marina, Pedro José Yepes Guzmán, los sargentos viceprimero Carlos Adolfo Bermúdez Carmona y Misael Villabona López, y los infantes profesionales de Marina Oslavi Enrique de la Cruz Torres, Guillermo Próspero Castillo Valencia y Alfonso Coronel Ortiz. **Pero en 2012 el caso fue prescrito y los responsables quedaron en libertad.**

Frente a este hecho, el Estado se comprometió ante la CIDH, a hacer la revisión del fallo que declaró la prescripción del caso, también se  prometió vincular a la familia a programas de reparación integral y la realización de un acto público de reconocimiento de responsabilidad que es la primera medida que incumplirá el gobierno.

“No se ha cumplido nada, estamos a la espera del reconocimiento de la culpabilidad del Estado. Espero que se retomen las riendas y se vuelva al camino de la justicia, **nosotros añoramos justicia hace 24 años** y esperamos que se efectúe lo que se firmó ante la CIDH aunque ya empezó incumpliendo”, expresa Carmen Zuñiga.

<iframe src="http://co.ivoox.com/es/player_ej_11227345_2_1.html?data=kpaflJyXeJahhpywj5aWaZS1lJiah5yncZOhhpywj5WRaZi3jpWah5yncaTV09LS0JC%2BuYa3lIqvk87LpY6fqcrfz8bSpY6ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
