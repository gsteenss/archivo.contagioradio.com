Title: "El Centro Democrático no va a vacilar en métodos para frenar el Acuerdo De Paz" Iván Cepeda
Date: 2016-11-30 16:32
Category: Paz, Política
Tags: #AcuerdosYA, Implementación de Acuerdos
Slug: el-centro-democratico-no-va-a-vacilar-en-metodos-para-frenar-el-acuerdo-de-paz-ivan-cepeda
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/congreso-e1478108708937.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Laura Rico] 

###### [30 Nov 2016] 

Ayer el Senado refrendo el acuerdo de paz de La Habana con 75 votos a favor. El día de hoy  el debate sobre refrendación se llevará a cabo en el Congreso y de ser aprobado la Corte Constitucional será la que tenga que pronunciarse sobre el Fast track o procedimiento especial, para que se lleve a cabo la implementación de los acuerdos. **Aquí se sabrá si se realizará vía directa, es decir por refrendación popular  o vía indirecta por refrendación en el Congreso.**

No obstante, los ánimos en el Congreso estuvieron caldeados desde un primer momento, ya que la gran mayoría de senadores llego una hora tarde. Para el senador Iván Cepeda este **“fue un debate intenso”**. El orden del día comenzó con intervenciones de ponentes del sí y el no y continúo con el debate de los senadores, momento que según Cepeda visibilizo aún más los **argumentos tanto de la legitimidad que tiene el Congreso para refrendar los acuerdos como el contenido de los mismos. **

Punto en el que sectores del No habían hecho enfasis durante toda su exposición. Frente a las posturas radicales del Centro Democrático y su anunció de **“resistencia civil”**, Cepeda afirma que este hecho solo revela los intereses electorales que tiene este partido para el periodo electoral del 2018. “Hay una intención de evitar que se lleve a cabo el proceso, y que no va a vacilar para usar toda clase de métodos para conseguir este fin”

Sin embargo, mientras avanzan los debates en Cámara en Colombia siguen presentándose atentados y actos de violencia en contra de defensores de derechos humanos, líderes del movimiento social y sindicalistas, situación que para el Senador  significa **“un llamado para poner en funcionamiento los acuerdos y los mecanismos** que tienen que ver con el desarme del paramilitarismo” y la defensa de los líderes sociales.

De igual modo, para el representante Alirio Uribe “el desafío que se viene es grande” motivo por el cual el próximo miércoles en conjunto con el representante Víctor Correa citaron a la Audiencia denominada los **“Crímenes por la paz”**, además el representante añadió que los acuerdos no “van a frenar de inmediato el tren de la guerra y se tratará de sabotear el proceso de paz, por lo cual hay que estar en permanente movilización”. Durante todo el día de hoy se llevará a cabo la jornada en la Cámara de Representantes. Le puede interesar: ["Llego el momento de la refrendación de los acuerdos de paz"](https://archivo.contagioradio.com/llego-el-reto-de-la-implementacion-de-los-acuerdos-de-paz/)

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)

<div class="ssba ssba-wrap">

</div>
