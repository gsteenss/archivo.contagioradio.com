Title: Adolfo Pérez, Nobel de Paz celebró anuncio desde La Habana
Date: 2016-06-23 11:21
Category: Entrevistas, Paz
Tags: Adolfo Pérez Esquivel, FARC, Nobel de Paz, proceso de paz
Slug: adolfo-perez-nobel-de-paz-celebro-anuncio-desde-la-habana
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/06/Adolfo-Pérez-Esquivel-e1466692483611.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Wikipedia 

###### [23 Jun 2016]

El Premio Nobel de Paz, Adolfo Pérez Esquivel, celebró en los micrófonos de Contagio Radio la firma del acuerdo sobre el cese al fuego bilateral, asegurando que se trata de un paso importante para construir la paz de Colombia, “**No son los acuerdos ideales pero si los posibles, son pasos para poner fin a tantos años de lucha, beligerancias y desencuentros.** Son pasos importantes que pueden ir afirmando soluciones de paz para el pueblo colombiano”.

Para el Nobel, con la firma de este acuerdo se avecinan retos para la sociedad colombiana, el gobierno, la comunidad internacional y los medios de comunicación en torno a la persistencia de grupos armados como las estructuras paramilitares y otras guerrillas, así mismo, se debe desmantelar las redes del narcotráfico, generar garantías para los millones de desplazados, asegurar el derecho a la tierra, lograr posibilidades de desarrollo para los campesinos y comunidades étnicas y fortalecer las instancias democráticas.

Otro de los retos puntuales tienen que ver con la reconciliación, que para Pérez Esquivel, **“no se llega fácilmente, hay que dar pasos, de reconocimiento de las culpas, buscar caminos de verdad justicia y reparación** para poder llegar a una reconciliación, que no es un hecho vacío y tiene que estar en hechos concretos que depende de la firma del proceso de paz”.

Pero este día no solo es importante para Colombia, se trata de un momento histórico también para el mundo y que da ejemplo a otros países donde se viven conflictos bélicos, indica el Nobel, quien añade que a su vez la sociedad colombiana debe aprender de las experiencias en otros países donde también se han desarrollado negociaciones de paz para que no finalmente se logre **"desarmar las conciencias armadas para construir nuevas ciudadanías en el mundo"**.

<iframe id="audio_12003545" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_12003545_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
