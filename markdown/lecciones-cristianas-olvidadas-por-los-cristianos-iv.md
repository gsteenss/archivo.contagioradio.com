Title: Lecciones cristianas olvidadas por los cristianos IV: Usar la misma medida para medir los mismo hechos
Date: 2020-10-19 12:09
Author: CtgAdm
Category: A quien corresponda, Opinion
Tags: cristianas, cristianismo., Cristianos, reflexiones
Slug: lecciones-cristianas-olvidadas-por-los-cristianos-iv
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/carretera.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

***Por culpa de ustedes, el nombre de Dios es denigrado entre las naciones***. (Romanos 2,24)

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

*"Me gusta tu Cristo... No me gustan tus cristianos. Tus cristianos son muy diferentes a tu Cristo"* -Mahatma Gandhi.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Estimado

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

**Hermano en la fe**, Cristianos, cristianas, personas interesadas

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Cordial saludo,

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Una vez más, te planteo temas político-religiosos, no para cuestionar tus ideas políticas o que gente cercana a ti sea de extrema derecha. Ese no es el problema, aunque tengan argumentos discutibles por falta de solidez y realismo. Tampoco es problema que haya cristianos que sigan partidos políticos de extrema derecha, de derecha, de centro o de izquierda.  

<!-- /wp:paragraph -->

<!-- wp:heading {"level":5} -->

##### **El problema que planteo, es la forma de defender (desde diversos lados del espectro político nacional e internacional) sus posiciones y visiones, y la forma de atacar a sus contradictores o llamados enemigos.**

<!-- /wp:heading -->

<!-- wp:paragraph -->

Frecuentemente, ante la falta de argumentos y razones, recurren a descalificaciones y ataques personales, ordinariamente, de forma irracional e inmisericorde. Más aún, que lo hagan en nombre de Dios y con afirmaciones que poco tiene que ver con El, que utilicen la religión para afirmar “su verdad” y “su autoridad”, que alimenten el odio hacia “los otros”, hacia “el otro lado”; que confundan el pensar y el ser diferente con ser “mala gente” o carente de valor y dignidad.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":5} -->

##### **Y lo más contradictorio, que cristianos y cristianas usen mentiras para atacar o defender, reproduzcan el odio y afirmen que lo hacen defendiendo la verdadera religión cristiana.  **

<!-- /wp:heading -->

<!-- wp:paragraph -->

Entre las raíces de esta contradicción están: seguir, ciegamente y sin análisis, lo que dicen sus líderes y “predicadores”, de diversas iglesias,  por redes sociales y medios afines a sus intereses y visiones, dejando a un lado la búsqueda honesta de la verdad, el respeto a las actitudes básicas humanas y cristianas;  dar por cierto que la visión del mundo y de Dios de sus líderes, es la única y verdadera;  negar la posibilidad de que criticar sus ideas y visiones del mundo y de Dios; no admitir que sus “contradictores”  pueden tener parte de razón;  y sobre todo, dejar de lado actitudes fundamentales de que el cristianismo propone. De esta contradicción quiero que reflexionemos y que recordemos que el Señor Jesús dijo:

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

“*La medida que usen para medir la usarán con ustedes”* (Mt 7,2)

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

**Es un escándalo que los cristianos contradigan los mensajes centrales de la Biblia** (como hemos reflexionado en anteriores cartas) que, entre otras enseñanzas, dice: *“En esto conocerán todos que son mis discípulos, en el amor que se tengan unos a otros” (*Jn 13,35), “*Amen a sus enemigos*” (Mt 544), “*Vence el mal haciendo el bien*” (Rom 12,21), *“Busquen primero el reino de Dios y su justicia”* (Mt 6,33) y **olvidan que la medida que usemos para medir la usarán con nosotros** (Cf. Mt 7,2). El mensaje de Jesús de **utilizar la misma medida “para medir y que nos midan”,** poco lo practican los sectores de los que venimos hablando, cuando debería ser lo lógico, lo humano, lo ciudadano, lo legal y lo cristiano.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Para estos sectores socio-religiosos es normal ver, escuchar y pensar, ciegamente, de acuerdo lo que sus líderes piensan, ven y hablan, sin darse cuenta que detrás de sus “bellos discursos” están sus intereses, a veces oscuros. Sus seguidores creen ciegamente en sus líderes, porque a través de los medios de información y redes sociales, con diversas formas y en diversas voces, con apariencia de seriedad, de cientificidad y de verdad, los llevan ver una sola cara de la realidad: la que ellos deciden mostrar. Y olvidan o desconocen que la moneda tiene otra cara, que hay otros argumentos, otras formas de ver la misma realidad. Y todo para que no vean la inconsistencia de sus argumentos religiosos, político, sociales…

<!-- /wp:paragraph -->

<!-- wp:heading {"level":5} -->

##### Y así no se ve **que usan medidas distintas para medir los mismos hechos.**

<!-- /wp:heading -->

<!-- wp:paragraph -->

La cuarentena me permitió seguir varios hechos de actualidad (políticos, judiciales, de orden publico, de corrupción, del manejo económico de la pandemia…) por diversos medios de información y redes sociales, y pude ver el manejo que hacen ciertos líderes para esconder o maquillar sus visiones e intereses políticos y económicos, bastante torcidos, presentándose ante el mundo como personas honestas y honorables. Pude, además, contrastar fuentes y “argumentos”, y ver los niveles de engaño y manipulación a las que se llega.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Esta manipulación y engaño las he visto y vivido en diálogos personales y en redes sociales, principalmente de este sector del que venimos hablando, sector que repite lo que dicen sus líderes y “predicadores”, muchos de ellos politiqueros, corruptos y mentiros (hay líderes de otros sectores que hacen mismo).

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Lo escandaloso e inaceptable es que se presentan como “los buenos”, los defensores de los valores tradicionales, los que dicen verdad, los defensores de la religión y del nombre de Dios, los que dictan las normas de buena conducta social y que actúan como si tuvieran la “autoridad divina”; y que **jamás aceptarían que se usara con ellos la medida que usan con los demás**, más aún, se consideran exentos de ser medidos, incluso, con medidas mucho más pequeñas que las que usan con los demás.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":5} -->

##### Te comparto estas reflexiones porque, como te lo he dicho otras veces, eres honesto en tus posiciones y creo que podemos, paso a paso, **asumir los criterios del Evangelio y usar la misma medida para medir a todas las personas y los mismos hechos.  **

<!-- /wp:heading -->

<!-- wp:paragraph -->

**Tener una doble medida, una para nosotros y otra para los otros**, es una realidad aceptada por sectores religiosos que se creen con más derechos y autoridad que los demás. Esta realidad llevó a graves equivocaciones en el pasado. Equivocaciones que causaron mucho daño e hicieron quedar mal la religión cristiana (católica, evangélica, protestante) y llevó a gente buena y honesta a desconfiar de lo religioso. Esos sectores calificaron como un atentado a la fe, a la tradición, a la verdad revelada, las visiones, ideas, aportes y críticas de sectores sociales, religiosos, políticos o científicos y sociales críticos, realidades que hoy son aceptadas por todo el mundo, incluso por esos sectores que hoy hacen lo mismo con otras realidades.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

También es verdad que hay tendencias muy arraigadas en los seres humanos, entre ellas: el ver la realidad desde nuestro punto de vista y considerar nuestro punto de vista como el verdadero, el mejor y el único; el miedo al cambio y a lo nuevo, que nos obliga pensar a reformular las ideas y creencias; a creerle, ciegamente, a las personas cercanas y de confianza o a quienes consideramos mejores que las demás; a creer que los equivocados son los otros; a mirar los hechos de acuerdo a nuestro intereses. **No olvidemos que la memoria es tendenciosa y selectiva, y que eso vale para toda persona.** El problema es que nos cerremos a las evidencias y **usemos una doble medida**.  

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Te dije que había analizado hechos actuales, en otro momento escribiré de ellos, pero tomo como ejemplo, hechos del pasado:

<!-- /wp:paragraph -->

<!-- wp:list -->

-   Nuestros padres y abuelos nos contaron historias dolorosas de la forma como en la “época de la violencia” (así se conoce la violencia de los años 40 y 50) se mataban uno a otros, por ser liberales o conservadores, sin conocerse, sin saber y si eran buenas o malas personas, sin hacerse nada. Sólo por “ser del otro partido”.

<!-- /wp:list -->

<!-- wp:heading {"level":5} -->

##### **Había una doble medida, dependiendo de quién midiera.**

<!-- /wp:heading -->

<!-- wp:paragraph -->

Mientras la gente del pueblo se peleaba y se mataba, los jefes liberales y conservadores tomaban whisky en los clubes sociales, se casaban y hacían negocios entre ellos.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Hicieron pactos, “bajo la mesa” y a espaldas del país, para repartirse los recursos económicos y el poder, para negar derechos básicos o acabar, de diversas maneras la “oposición”. Ellos envenenaron al pueblo y lo llevaron a “odiar hasta quien fue su buen vecino”, como dice la canción. Incluso con la participación de la religión.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Esta estrategia fue muy eficaz para ellos, porque el odio impedía al pueblo ver, oír o pensar, y así, no se daba cuenta que mientras los seres humanos se peleaban o mataban, al pueblo le robaban todo. **Muy hábilmente usaban una doble medida.**

<!-- /wp:paragraph -->

<!-- wp:list -->

-   Hace unos 14 años, un hombre de tu tendencia política-religiosa, me atacó verbalmente y me insultó. Me acusaba por ser defensor derechos humanos y ambientales, por pedir justicia y paz.

<!-- /wp:list -->

<!-- wp:paragraph -->

Me dijo que esas eran cosas de ateos, de comunistas o castrochasvistas y puso en duda mi fe cristiana y mi fidelidad al ministerio, porque según él, defendía la ideología de género. Por fortuna, logré respirar y no dejarme sacar de casillas.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Y le pregunté: ¿En qué se parecen Hugo Chávez, Álvaro Uribe y Fidel Castro? Me miró desconcertado, pensó un momento y dijo que en nada. Luego le recordé que los tres rechazaban y descalifican vehementemente los informes de Amnistía Internacional y de Human Rights Wastch sobre las violaciones a los derechos humanos.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":5} -->

##### Esos informes mostraban que Colombia era el campeón mundial, y con gran ventaja, de violaciones a los derechos humanos.

<!-- /wp:heading -->

<!-- wp:paragraph -->

Le recordé que él y su gente solo hablaban de lo que pasaba allá (con informaciones sin contrastar) y no de lo que pasaba aquí. Lo que era un escándalo para todo el mundo. Muy hábiles para *“colar el mosco y tragarse el camello”* (Mt 23,24), como dice Jesús. **Usan muy bien la doble medida.**  

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Podría alargarme con ejemplos, pero tú puedes recordar otros, y caer en la cuenta que muchos creyentes olvidamos el mensaje de Jesús: “*Del mismo modo que ustedes juzguen, se les juzgará. La medida que usen para medir, la usarán con ustedes”* (Mt 7,2)

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Fraternalmente,

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

P. Alberto Franco, CSsR, J&P <francoalberto9@gmail.com>

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

[Lea mas de nuestros columnistas](https://archivo.contagioradio.com/opinion/columnistas/)

<!-- /wp:paragraph -->
