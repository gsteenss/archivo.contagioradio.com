Title: El Centro de Memoria, paz y reconciliación celebra tres años con música
Date: 2015-12-01 11:56
Category: eventos, Nacional
Tags: 3 años del centro de memoria paz y reconciliacion, Celebración 3 años apertura centro de memoria, Centro de Memoria Paz y reconciliación, la mojarra electrica, Paz y reconciliación
Slug: el-centro-de-memoria-paz-y-reconciliacion-celebra-tres-anos-con-musica
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/11/Centro-de-memoria.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: centrodememoria.gov.co 

###### [30  Nov 2015]

El Centro de Memoria, Paz y Reconciliación estará encabezando la celebración de los 3 años construcción de un espacio que ha recibido distintos eventos y distintas voces de víctimas, académicos y organziaciones civiles y sociales  que han denunciado injusticias y presentado propuestas para un mejor país , el evento que se llevará a cabo el 5 de diciembre se llama “3 años constuyendo memoria” y será diagonal a la estación de Transmilenio “Centro Memoria”.

El evento será encabezado por la agrupación de la Mojarra Elécrica  que tiene 10 años en la escena musical de los sonidos tradicionales afrocolombianos con un contenido social, que visibiliza y rescata el legado cultural del Pacifico y del Atlántico colombiano, este grupo es dirigido por Jacobo Velez y conducido por Alejandro Montaña, en su trayectoria tiene tres discos en el mercado:. Calle 19 en 2002, Raza en 2005 y Poder para la gente en 2011.
