Title: La Trocha, una cerveza para la paz
Date: 2020-04-29 19:16
Author: AdminContagio
Category: Expreso Libertad, Nacional, Programas
Tags: cerveza artesansal, cerveza La trocha, Expreso Libertad, exprisioneras politicas, exprisioneros políticos, FARC
Slug: la-trocha-una-cerveza-para-la-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/04/EMPRENDIMIENTO-EXCOMBATIENTES-RECONCILIACIÓN-FARC-4.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

La cerveza **La Trocha** es uno de los proyectos que han emprendido ex prisioneros políticos de **FARC** en el marco del proceso de reincorporación. Para ellos, esta iniciativa no solamente es una forma de subsistir económicamente, también se ha convertido en otro lugar desde donde apostarle a la paz.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

De acuerdo con Doris Suarez, la Trocha surge como una apuesta por recolectar las memorias de las extenuantes caminatas por las trochas de Colombia que hacían cuando estaban alzados en armas, y las trochas que hoy caminan para abonar un país con la esperanza.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

José Zamora también rescata que si bien en este proceso han recibido muy poco apoyo por parte del gobierno en el compromiso de la reincorporación, han tenido un gran respaldo por parte de la comunidad de Ubate, la sociedad civil y la Universidad Nacional.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En este **Expreso Libertad** escuche la historia de la Trocha, tomando el camino de la paz.

<!-- /wp:paragraph -->

<!-- wp:core-embed/facebook {"url":"https://www.facebook.com/contagioradio/videos/357450688546669/","type":"video","providerNameSlug":"facebook","className":""} -->

<figure class="wp-block-embed-facebook wp-block-embed is-type-video is-provider-facebook">
<div class="wp-block-embed__wrapper">

https://www.facebook.com/contagioradio/videos/357450688546669/

</div>

</figure>
<!-- /wp:core-embed/facebook -->

<!-- wp:paragraph -->

Ver mas: [Programas de Expreso Libertad](https://archivo.contagioradio.com/programas/expreso-libertad/)

<!-- /wp:paragraph -->
