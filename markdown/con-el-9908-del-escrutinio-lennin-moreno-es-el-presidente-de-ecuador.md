Title: Con el 99,08% del escrutinio Lennin Moreno es el presidente de Ecuador
Date: 2017-04-03 17:24
Category: El mundo
Tags: ecuador, Guillermo Lasso, Lennin Moreno
Slug: con-el-9908-del-escrutinio-lennin-moreno-es-el-presidente-de-ecuador
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/04/Lenin-Moreno.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Periódico Expectativa] 

###### [03 Abril 2017] 

Una fuerte jornada de votación se desarrolló ayer en Ecuador que lleva por ahora la contabilización del **99,08% de los votos y en donde Lennin Moreno, de Alianza País va a la cabeza con un total del 51,16%**. esta victoria permite que Ecuador sea uno de los poco países en Latinoamérica que da continuidad a los gobiernos progresistas.

Sin embargo, su contrincante Guillermo Lasso señaló que no reconoce las elecciones y que usará todas las vías políticas y jurídicas para evidenciar que existieron irregularidades en el proceso.

No obstante, esta segunda vuelta electoral contó con una de las mayores veedurías, **el 93% de las mesas de votación tuvieron representación de delegados de ambos partidos, además habían delegados de la UNASUR y la OEA**, de igual modo el CNE, apenas obtuvo las actas con el número de votos, fueron subidas a la web del CNE. Por ende, la petición que actualmente se la hace al candidato opositor es que explique a qué tipo de irregularidades se refiere para decir que “no se respetó la voluntad popular”.

Una de las posibles causas de la polarización que ahora se vive en Ecuador, frente al escrutinio de votos, tiene que ver con las encuestadoras que minutos antes de que iniciara el conteo de votos ya se contradecían y que de acuerdo con Rocío Guamancondor, periodista de Prensa Aler, crean una **“falsa verdad” y cuando salen los resultados del CNE “la gente considera que no reflejó lo que las encuestas decían”**.

Para evitar este tipo de situaciones Guamancondor, hace un llamado a las encuestadoras para que den **el resultado previo al conteo de votos del Consejo Nacional Electoral, con muestras mucho más certeras**, y de esta forma no poner en vilo a la ciudadanía. Le puede interesar: ["Lo que perdería Ecuador y Latinoamérica si pierde Alianza País"](https://archivo.contagioradio.com/lo-que-pierde-ecuador/)

El Consejo Nacional Electoral, dice que el resultado final se tendrá dentro de dos días, para hacer el reconteo de votos, sin embargo, s**e espera que la tendencia de votos hacia Lennin Moreno se mantenga**, tras existir una brecha tan corta entre los votos de ambos candidatos.

###### Reciba toda la información de Contagio Radio en [[su correo]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio]{.s1}](http://bit.ly/1ICYhVU)[.]{.s1}
