Title: 31 años después del Holocausto del Palacio de Justicia entregarán restos de Luz Mary Portela
Date: 2016-11-04 17:43
Category: DDHH, Otra Mirada
Tags: Homenaje a víctimas del palacio de justicia
Slug: entregaran-restos-de-luz-mary-portela
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/Luz-Mary-Portela.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio ] 

###### [4 de Nov 2016]

Este martes se espera que el Instituto Colombiano de Medicina Legal entregue los restos oseos de Luz Mary Portela de León, una de las 11 desaparecidas del Palacio de Justicia en 1985. 31 años después, y a pesar de diversos fallos judiciales **el Estado se niega a reconocer la responsabilidad de los militares en estos hechos y solamente se han hallado algunos restos de tres de los desaparecidos.**

Para Rosa Milena Cardona León, hija de Luz Mary Portela, lo que ha vivido durante estos 31 años es similar a estar en una montaña rusa y llenarse de razones y de valores para continuar,  **“hay momentos en donde ya no quiero saber nada, pero luego vuelvo a retomar la lucha, el amor y la perseverancia para seguir”.**

Los restos de Luz Mary fueron encontrados en el Cementerio del Norte, en una tumba con otro nombre y hasta el momento es el único cuerpo de las víctimas del Palacio de Justicia del cual se ha encontrado la estructura ósea casi completa. Aún se desconocen las causas de por las qué el cuerpo de Luz Mary fue enterrado en este lugar e incluso **por qué es encontrada dentro del Palacio de Justicia, cuando material de vídeo la registran saliendo con vida.**

“Me queda la amargura de no saber que paso con ellos, sobre todo después de saber que Luz Mary sale viva del Palacio de Justicia hacia la Casa del Florero y el 20 de octubre, el **Estado me la entrega muerta y sin una explicación de la causa de muerte y esa es toda la verdad que puedo saber”**

En la conmemoración de las víctima del Palacio de Justicia este 6 y 7 de noviembre se realizarán varias actividades en la Plaza de Bolívar, el **domingo 6 de noviembre a partir de la 1 de la tarde habrá un acto de memoria y la proyección de un vídeo sobre los momentos en donde los familiares de las víctimas han estado a punto de alcanzar la verdad y se ha esfumado. **Le puede interesar: ["Coronel Edilberto Sánchez pagará 40 años de prisión por desaparecidos del Palacio de Justicia"](https://archivo.contagioradio.com/coronel-edilberto-sanche-condenado-por-palacio-de-justicia/)

El 7 de noviembre se realizará la entrega técnica de los restos de Luz Mary, desde las 6:00pm se realizará una **vigilia frente a la Casa del Florero** y el 8 de noviembre será la entrega oficial y el sepelio, además se hará un viacrucis en homenaje a los padres y madres de las víctimas del Palacio de Justicia. Le puede interesar:["30 años después entregan restos de víctima de Palacio de Justicia"](https://archivo.contagioradio.com/maria-lyda-mondol-palacio-de-justicia-inhumacion/)
