Title: Corte Suprema de Panamá investigará a ex presidente Martinelli por corrupción y espionaje
Date: 2015-02-03 22:11
Author: CtgAdm
Category: El mundo, Judicial
Slug: corte-suprema-de-panama-investigara-a-ex-presidente-martinelli-por-corrupcion-y-espionaje
Status: published

###### Foto: laradiodelsur.com.ve 

Tanto el parlamento Centroamericano como el consejo electoral negaron la inmunidad a favor del expresidente, quién es acusado por múltiples **escándalos de interceptación de comunicaciones y corrupción durante su gobierno en Panamá**. Sin embargo la solicitud al tribunal electoral quedó en pausa mientras se notifica la apertura de la investigación en la Corte Suprema de Justicia.

Walyd Zayed, abogado defensor de Derechos Humanos y víctima de las interceptaciones ilegales afirma que **es necesario que la justicia actúe con celeridad una vez se levante la inmunidad electoral** pues la CSJ tendría un plazo de 90 días para concluir la parte inicial de la investigación formal. Esta figura, establecida por la ley, sería el principal obstáculo para el avance de las investigaciones.

La denuncia por corrupción en contra de Ricardo Martinelli, fue radicada el pasado miércoles tras las **denuncias públicas del exdirector del Programa de Ayuda Nacional (PAN) Giacomo Tamburrelli, quien vinculó al exmandatario en las irregularidades del contrato por \$45 millones** para la compra de comida deshidratada.
