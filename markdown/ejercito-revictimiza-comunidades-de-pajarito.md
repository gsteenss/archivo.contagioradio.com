Title: "Ejército revictimiza campesinos negando abusos" Comunidad de Pajarito,Boyacá
Date: 2017-01-09 12:05
Category: DDHH, Nacional
Slug: ejercito-revictimiza-comunidades-de-pajarito
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/01/1_ejercito_y_comunidad_sur_de_bolivar.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Getty] 

###### [9 Enero 2017] 

Luego de haber sido presentada la denuncia por parte de la comunidad de Pajarito, Boyacá en la que aseguraron haber sido víctimas de detenciones y amenazas por parte del Ejército de la zona, **las autoridades de dicho municipio realizaron un consejo de seguridad en el que según manifiestan las comunidades no se les invitó y fueron revictimizadas al no reconocer los hecho ocurridos.**

En un comunicado, los pobladores de esta región manifestaron que se enteraron de dicha reunión y de lo declarado por las autoridades a través de los medios de comunicación, y que ante la información proporcionada es necesario asegurar que “**ni las víctimas, ni las organizaciones denunciantes fueron informadas o convocadas a participar en dicho Consejo de Seguridad”. **

De igual modo, cuentan que ya han puesto en conocimiento de la Personería Municipal de Pajarito (Boyacá) los hechos ocurridos y que han ratificado “lo narrado por las organizaciones denunciantes”.

Para las comunidades los hechos que se denunciaron el pasado 2 de Enero en la vereda Sabanalarga sí sucedieron “reiteramos la ocurrencia de los hechos” aseveran en la comunicación, y agregan “**el Ejército Nacional intimidó a los habitantes, les restringió  el libre desplazamiento, registró sin orden judicial una vivienda y realizó señalamientos en contra de habitantes del sector”. **Le puede interesar: [Ejército detiene y amenaza a campesinos en Pajarito, Boyacá](https://archivo.contagioradio.com/ejercito-detiene-y-amenaza-a-campesinos-en-boyaca/).

Finalmente, afirman que **realizarán una misión de verificación y acompañamiento al lugar de los hechos** e invitaron a la Oficina del Alto Comisionado de las Naciones Unidas, al ministerio público, a medios de comunicación y organizaciones defensoras de Derechos Humanos nacionales e internacionales a hacer parte de dicha acción humanitaria. Le puede interesar: [En total impunidad crímenes cometidos por Ejército Nacional en 2004](https://archivo.contagioradio.com/en-total-impunidad-crimenes-cometidos-por-ejercito-nacional-en-2014/)

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio.](http://bit.ly/1ICYhVU)

<div class="ssba ssba-wrap">

</div>
