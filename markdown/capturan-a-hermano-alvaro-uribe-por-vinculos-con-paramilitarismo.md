Title: Capturan a hermano de Álvaro Uribe por vínculos con el paramilitarismo
Date: 2016-02-29 13:51
Category: Entrevistas, Judicial
Tags: Alvaro Uribe, Paramilitarismo, Santiago uribe
Slug: capturan-a-hermano-alvaro-uribe-por-vinculos-con-paramilitarismo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/02/Alvaro-Uribe.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Blu Radio 

###### [29 Feb 2016] 

Santiago Uribe, hermano del senador Álvaro Uribe Vélez, fue capturado en las últimas horas por agentes del CTI de la Fiscalía en Medellín, por las investigaciones que cursan en contra suya por presuntos vínculos con grupos paramilitares, concierto para delinquir y homicidio agravado.

La diligencia se desarrolló hacia la 1:00 pm, en el Barrio El Poblado de Medellín. El Cuerpo Técnico de Investigaciones condujo hacia el Bunker de la Fiscalía a Santiago Uribe y hasta el momento se encuentra allí recluido. La medida de aseguramiento no tiene beneficio de excarcelación según lo indicó la propia Fiscalía.

El hermano menor del expresidente de Colombia es investigado tras conocerse el testimonio del** mayor (r) de la Policía, Juan Carlos Meneses quien aseguró que Santiago Uribe** está relacionado con la creación del grupo paramilitar 'Los 12 apóstoles', que asesinaron a varias personas luego de haber sido señaladas como guerrilleros o colaboradores de la insurgencia.

El testimonio de Eunisio Pineda fue clave para la vinculación de Santiago Uribe a los más de 33 crímenes cometidos por el grupo paramilitar "Los 12 Apóstoles", Pineda, quien era trabajador de Santiago Uribe, afirma que el apodo con el que se conocía a Uribe era "El Abuelo" y reiteró que los que mandaban ahí eran Rodrigo Alzate, Julián Bolívar y Santiago Uribe.

\[embed\]https://www.youtube.com/watch?v=mhi96lHHOpQ\[/embed\]

Otro de los testimonios que refuerza la medida de aseguramiento es el "Don Berna" que desde una cárcel de EEUU declaró, el 26 de Febrero de 2015, que en varias ocasiones había escuchado de la relación de amistad de Vicente Castaño con Santiago Uribe. En esa misma diligencia "Don Berna" aseguró que alias "j", segundo al mando del bloque metro, habló en varias charlas sobre su relación cercana con Santiago Uribe.

También relacionó al hermano menor de los Uribe con el grupo "Los Escopeteros", uno de los primeros grupos paramilitares de Antioquia y que fue patrocinado por el ganadero Garcés Soto, que también hacía referencia permanente a su relación con Santiago Uribe.
