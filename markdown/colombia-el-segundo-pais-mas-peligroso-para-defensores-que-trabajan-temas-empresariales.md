Title: Colombia, el segundo país más peligroso para defensores que trabajan temas empresariales
Date: 2020-03-02 17:43
Author: CtgAdm
Category: Líderes sociales, Nacional
Tags: defensores de derechos humanos, empresas, Mineria, Naciones Unidas
Slug: colombia-el-segundo-pais-mas-peligroso-para-defensores-que-trabajan-temas-empresariales
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/03/Empresas-y-derechos-humanos.png" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/03/Empresas-y-defensores-de-derechos-humanos-1.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:heading {"align":"right","level":6,"textColor":"cyan-bluish-gray"} -->

###### Foto: @CIEDHes {#foto-ciedhes .has-cyan-bluish-gray-color .has-text-color .has-text-align-right}

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Este lunes el Centro de Información sobre Empresas y Derechos Humanos presentó un informe en el que se denuncia la relación existente entre empresas y afectaciones a los defensores de DD.HH. Según la investigación, Colombia es el segundo país más peligroso del mundo (después de Honduras) con relación a ataques contra defensores de DD.HH. que trabajan temas empresariales, "albergando el 9% de todos los casos a nivel mundial".

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El documento relata los hallazgos evidenciados tras observaciones hechas por el Centro entre 2015 y 2019, en el que se cuentan aquellos casos de ataques a defensores de derechos humanos en los que fue posible hacer un seguimiento en terreno. Por esta razón, en Colombia únicamente se documentan 181 casos de agresiones aunque podría existir un subregistro con un número mayor.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Sectores y empresas relacionados con ataques a defensores de DD.HH.

<!-- /wp:heading -->

<!-- wp:paragraph -->

Según explica el [comunicado](https://www.business-humanrights.org/en/node/204225) que acompaña el informe, la intención del mismo no es afirmar ni vincular directamente a las empresas con el hecho victimizante, sino ofrecer elementos de contexto para evaluar lo que está ocurriendo y cómo solucionarlo. Por lo tanto, la relación no implica que las empresas sean responsables de los ataques, pero sí que los defensores atacados han expresado su preocupación por sus actividades.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En el 90% de los ataques, los defensores se habían manifestado sobre los efectos negativos de sectores como minería, extracción de hidrocarburos, agronegocios y ganadería, y energía renovable. Por otra parte, "Las empresas que con mayor frecuencia (44% de los ataques) fueron objeto de la defensa de DD.HH. por parte de los defensores y que fueron atacados en respuesta a ella fueron [Anglo Gold Ashanti](https://archivo.contagioradio.com/anglo-gold-ashanti-bloquear-investigacion/), [Big Group Salinas](https://archivo.contagioradio.com/multinacionales-habrian-desplazado-productores-artesanales-de-sal-la-guajira/), [Cerrejón Coal](https://archivo.contagioradio.com/carbones-del-cerrejon-incumple-sentencia-que-protege-a-comunidades-y-al-arroyo-bruno/), [Ecopetrol](https://archivo.contagioradio.com/7-lideres-estigmatizados-ecopetrol/) y[EPM](https://archivo.contagioradio.com/casos-de-leishmaniasis-incrementan-en-zona-de-influencia-de-hidroituango/).

<!-- /wp:paragraph -->

<!-- wp:gallery {"ids":[81483]} -->

<figure class="wp-block-gallery columns-1 is-cropped">
-   <figure>
    ![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/03/Empresas-y-derechos-humanos.png){.wp-image-81483}
    </figure>

</figure>
<!-- /wp:gallery -->

<!-- wp:paragraph -->

El informe llama la atención porque el 76,5% de lo casos en los 4 sectores con mayor riesgo se registraron en zonas con alta inversión empresarial, "lo que indica que estos ataques no se producen en zonas olvidadas" y por lo tanto, deben ser objeto de las consideraciones empresariales como estatales.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Adicionalmente, se destaca que el tipo de ataque de mayor registro fueron los asesinatos, seguidos por amenazas de muertes y golpizas. En su mayoría, incidentes registrados entre 2016 y 2018, aunque aún faltan cifras por consolidar respecto a 2019. (Le puede interesar: ["Gobiernos del mundo indiferentes ante la violación de DD.HH. por parte de empresas"](https://archivo.contagioradio.com/gobiernos-del-mundo-indiferentes-ante-la-violacion-de-dd-hh-por-parte-de-empresas/))

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Las empresas deben prevenir las consecuencias negativas sobre los DD.HH. relacionadas con sus operaciones

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

La[Defensoría del Pueblo](http://www.indepaz.org.co/wp-content/uploads/2018/03/AT-N%C2%B0-026-18-Defensores.pdf) en su alerta temprana N° 026-18 sostiene que se han evidenciado situaciones de violencia que vinculan la participación de agentes privados que ven amenazados sus intereses económicos con los procesos de restitución de tierras, las protestas por la implementación de proyectos extractivos o denuncia de abusos contra comunidades y personas.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Por esta razón, el Centro de Información recuerda que los Principios Rectores de las Naciones Unidas sobre las empresas y los DD.HH. establecen que estas deben prevenir o mitigar las consecuencias negativas sobre los derechos humanos relacionadas con sus operaciones, productos o servicios, aún cuando hayan contribuido a generarlos.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En ese sentido, hizo un llamado a las empresas para que se manifiesten contra violaciones a los derechos humanos, así como que tomen acciones en el sentido de prevenir que sus empleados o contratistas (otras empresas) se vean afectados por este tipo de hechos. (Le puede interesar: ["Las 11 empresas que se oponen a la Restitución de Tierras según estudio de ONG"](https://archivo.contagioradio.com/las-11-empresas-que-se-oponen-a-la-restitucion-de-tierras-segun-estudio-de-forjando-futuros/))

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Por otra parte, se recomendó al Estado colombiano que incorpore una dimensión empresarial en el monitoreo de las sobre ataques a defensores de derechos humanos, investigar y esclarecer las razones detrás de los ataques, así como proteger eficazmente a las personas que defienden de muchas formas el territorio.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/CIEDHes/status/1234584587264823299","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/CIEDHes/status/1234584587264823299

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:block {"ref":78980} /-->
