Title: "Si el PND de Colombia no redistribuye la riqueza, no habrá paz" Víctimas que viajaron a La Habana
Date: 2015-02-20 22:17
Author: CtgAdm
Category: DDHH, Otra Mirada
Tags: Conversaciones de paz en Colombia, ELN, FARC, paz, víctimas, Víctimas que viajaron a la Habana
Slug: si-el-pnd-de-colombia-no-redistribuye-la-riqueza-no-habra-paz-victimas-que-viaron-a-la-habana
Status: published

###### Foto: Contagio Radio 

[<iframe src="http://www.ivoox.com/player_ek_4113396_2_1.html?data=lZaelZideo6ZmKiakpWJd6KkkZKSmaiRdI6ZmKiakpKJe6ShkZKSmaiRh8LhytHcjbvNsM3VjoqkpZKns8%2FowszW0ZC2pcXd0JCah5yncZU%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>]{.eModal-3}

##### [Camilo Villa, hijo de líder político asesinado] 

<iframe src="http://www.ivoox.com/player_ek_4113395_2_1.html?data=lZaelZideY6ZmKiakpaJd6KmlZKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRjtDnhqigh6adb6Li1cre18rWpY6ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [José Antequera, hijo de líder político asesinado] 

<iframe src="http://www.ivoox.com/player_ek_4113401_2_1.html?data=lZaelZmUdY6ZmKiakpWJd6KnlJKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRncbnysjOja3TvdDnjoqkpZKns8%2FowszW0ZC2pcXd0JCah5yncZU%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Yesica Hoyos, hija de sindicalista asesinado] 

##### <iframe src="http://www.ivoox.com/player_ek_4113398_2_1.html?data=lZaelZidfI6ZmKiakpaJd6KkkpKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRjtbVz5Cww9fQs9Sft87ZzsbRrdvV05KSmaiRh9Di1cbUy9SPlsLYytSYj4qbh46o&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe> 

##### [Juan Carlos Villamizar, hijo de líder político asesinado] 

En un comunicado de 8 puntos emitido por el grupo de 60 víctimas que en 5 delegaciones, viajaron a La Habana, expresan su **apoyo para que el objetivo de la mesa de diálogos** se concrete, pero también ponen de presente los retos que esto implica: la necesidad de que los **acuerdos alcanzados entre guerrillas y gobierno** tengan enfoques de género, edad y etnia; que se **eviten hechos re-victimizantes**, y que se inicie un proceso de **desescalamiento del conflicto con corresponsabilidad y reciprocidad.**

El grupo de 60 personas, se reunió el 18, 19 y 20 de febrero del 2015 en Bogotá, con el acompañamiento de la ONU, la Universidad Nacional de Colombia, la Conferencia Episcopal y delegaciones diplomáticas internacionales. El objetivo: **Encontrar la manera de continuar apoyando los diálogos de paz,** y la solución dialogada al conflicto social y armado.

La reunión, según el propio grupo, se desarrolló en un ambiente de diálogo cordial, en el que las personas pudieron reconocer y reconocerse como **víctimas de Estado, Ejercito, grupos paramilitares y guerrilla;** en general, como víctimas de un conflicto social y armado del que -todos coinciden- Colombia debe salir, a través de la vía del diálogo. Su propia capacidad de encontrar consensos, en medio de la diversidad generacional, regional y de situaciones victimizantes, los lleva a pensar que en Colombia es posible construir la paz.

La tarea que asumen, consideran, es fundamentalmente pedagógica y difusión de lo que significa la solución política del conflicto para alcanzar la paz, teniendo claro que en La Habana se encuentran dos de las partes en confrontación armada, pero que la paz no la construyen ellas, sino los y las colombianas en los espacios locales y regionales.

Por ese motivo, el grupo no se refirió a temas de opinión actuales, como la propuesta del ex-presidente Cesar Gaviria de extender la justicia transicional a empresarios; o la asignación de un delegado de los EEUU al proceso de paz. Pero por el contrario, si se refirieron a proyectos en curso en el Congreso que afectan a las víctimas, y a la paz en Colombia, como el Plan Nacional de Desarrollo, afirmando que sin unas políticas estatales que tengan como objetivo acabar con las condiciones de inequidad y pobreza, será imposible alcanzar un país en paz.

"Hacemos un llamado a la sociedad colombiana para reflexionar y trabajar por la paz, y generar una cultura incluyente, que propenda por la igualdad social, el respeto, y la tolerancia de las diferencias, que posibiliten nuestro futuro, dignifiquen nuestro presente, y rememoren nuestro pasado", concluyó el comunicado.

**Comunicado:**

\
