Title: Continúan agresiones contra la vida de miembros del COPINH en Honduras
Date: 2016-10-12 12:23
Category: DDHH, El mundo
Tags: asesinato Berta Cáceres, Asesinatos ambientalistas honduras, Berta Cáceres, Copinh, honduras
Slug: continuan-agresiones-contra-la-vida-de-miembros-del-copinh-en-honduras
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/10/BertaCopihn-e1476293551286.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: El Pais 

###### 12 Oct 2016 

El Consejo Cívico de Organizaciones Populares de Honduras **(COPINH)** organización de la que era parte **Berta Cáceres**, asesinada el pasado mes de marzo, dio a conocer a través de un comunicado, que su coordinador general Tomás Gómez Membreño y otro de sus integrantes como Alexander García fueron víctimas de atentados contra su vida.

Los hechos ocurrieron el pasado domingo cuando un desconocido atacó con arma de fuego el vehículo en el que se movilizaba Gómez, quien es la persona que sucedió en el cargo a Cáceres. Ante estos hechos el **(COPINH)** aseguró que los disparos fueron realizados con la intención de asesinar a los miembros de dicha organización.

Por su parte, el atentado en contra de García se dio en la vivienda del integrante de esta organización. Personas fuertemente armadas dispararon contra las ventanas y la puerta de su casa. Según datos del **COPINH**, este es el segundo atentado que sufre García, el anterior sucedió el 6 de mayo también a la salida de su lugar de residencia.

Así mismo la organización aseveró que estos sucesos se están presentando en contra de quienes defienden los derechos del pueblo lenca “"se sigue atentando contra la vida de quienes nos oponemos a la construcción de proyectos de muerte” agrega el comunicado.

Luego de seis meses de no lograr justicia en el caso de la líder ambientalista e indígena Berta Cáceres, ésta organización manifestó que “ninguna entidad del gobierno ni demás instituciones, han respondido a nuestras demandas de cancelación de proyectos inconsultos con las comunidades, la investigación independiente del asesinato de **Berta Cáceres**, la desmilitarización de los territorios lencas y el cese a la persecución y estigmatización hacia el **COPINH**". Lea también: [Dos militares están implicados en asesinato de Berta Cáceres en Honduras](https://archivo.contagioradio.com/militares-berta-caceres/)

Recientemente se conoció que el Poder Judicial de Honduras rechazó el recurso de apelación que se había impuesto contra los imputados por el crimen de la ambientalista **Berta Cáceres**. De manera que los 6 implicados en el caso serán procesados con prisión preventiva.

Lea también: [Gerente de DESA directamente vinculado con asesinato de Berta Cáceres en Honduras](https://archivo.contagioradio.com/desa-asesinato-de-berta-caceres-honduras/)

<iframe id="audio_13289067" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_13289067_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en su correo [[bit.ly/1nvAO4u]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por Contagio Radio [[bit.ly/1ICYhVU]{.s1}](http://bit.ly/1ICYhVU)
