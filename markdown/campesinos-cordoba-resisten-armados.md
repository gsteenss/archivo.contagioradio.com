Title: Campesinos de Córdoba resisten en el territorio pese amenazas de grupos armados
Date: 2019-03-29 17:25
Category: Comunidad, DDHH
Tags: cordoba, desplazados, Familias, San Jorge
Slug: campesinos-cordoba-resisten-armados
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/03/WhatsApp-Image-2019-03-29-at-3.27.56-PM-e1553898374315.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Andrés Chica] 

###### [29 Mar 2019] 

Luego de una semana de continuos desplazamientos en la región del San Jorge, al sur de Córdoba, más de 500 familias se han visto obligadas a refugiarse en el corregimiento de Juan José, municipio de Puerto Libertador, mientras esperan las condiciones para retornar a sus veredas. Esta situación viene afectando a cerca de 1.200 personas, quienes de acuerdo a testimonios de la población, no están recibiendo la atención necesaria para palear el drama de su desplazamiento.

Las amenazas de desplazamiento iniciaron el pasado 21 de marzo, cuando grupos armados que operan en el Alto San Jorge, con la excusa de combatir a sus contrincantes sin involucrar la población, exigieron a las comunidades salir de sus territorios. Esta situación se repitió desde el domingo 24 de marzo hasta el jueves con otras veredas cercanas al corregimiento de Juan José.

El integrante de la Asociación de Campesinos del Sur de Córdoba, Arnobis de Jesús Zapata, señaló que en la parte alta del territorio operan conjuntamente el Nuevo Frente 18 y Los Caparrapos, mientras que en la parte baja lo hacen el Clan del Golfo y las auto denominadas Autodefensas Gaitanistas de Colombia (AGC). (Le puede interesar: ["Comunidad Embera expulsó a cinco paramilitares de las AGC de su territorio en Chocó"](https://archivo.contagioradio.com/comunidad-embera-agc/))

Según Zapata, aunque la Defensoría del Pueblo advirtió sobre la confrontación entre armados ilegales en la región del San Jorge, y el Presidente envío 4 mil hombres para la Fuerza de Tarea Conjunta Aquiles, "hasta el día que se presentó el desplazamiento  no ha habido un solo soldado en el territorio"; por el contrario, la pasividad de las fuerzas militares ha permitido que los armados avancen en su control territorial y pareciera que "el ejército fuera consecuente con lo que hacen estos grupos" afirma Zapata.

Adicionalmente, la atención del Estado para atender la emergencia humanitaria ha sido insuficiente, pues se están tomando los planes de rutina, que se han visto desbordados en su capacidad departamental por el número de personas afectadas. Mientras tanto, las familias necesitan alimentos, medicina, agua potable, baños y un lugar donde dormir. (Le puede interesar: ["Más de 400 familias desplazadas por guerra entre grupos ilegales en la región del San Jorge"](https://archivo.contagioradio.com/familias-desplazadas-ilegales-cordoba/))

### **Las familias están en Juan José resistiendo para volver a su territorio** 

Los campesinos del Sur de Córdoba iniciaron el 28 de febrero una manifestación para exigir al Gobierno la continuación del Programa Nacional Integral de Sustitución de cultivos de uso ilícito (PNIS), puesto que en la zona erradicaron casi la totalidad de las 923 hectáreas cosechadas que había de Coca, pero aún están esperando la llegada de los proyectos productivos que les permitiera obtener su sustento económico de otras fuentes.

Sin embargo, por las amenazas de los armados tuvieron que terminar su protesta y esperar el cumplimiento de lo acordado con el Gobierno, mientras regresaban a sus zonas de origen para ayudar a sus familias a llegar a Juan José, corregimiento en el que se mantienen en un acto de resistencia esperando poder retornar a sus hogares. (Le puede interesar: ["Ante incumplimiento de PNIS, más de 3 mil familias del Alto Sinú se declaran en paro"](https://archivo.contagioradio.com/mas-de-3-000-familias-del-sinu-se-declaran-en-paro-campesino/))

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
