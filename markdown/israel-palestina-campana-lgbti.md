Title: Israel intenta utilizar población LGBTI en campaña contra Palestina
Date: 2017-07-24 14:39
Category: Onda Palestina
Tags: Apartheid Israel, Comunidad LGTBI, Palestina
Slug: israel-palestina-campana-lgbti
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/07/Lgtbi-palestina.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: [ QuAIA Seattle] 

###### 19 Jul 2017 

El “**pinkwashing**” **o lavado de rosa** es una campaña de propaganda utilizada por el Estado de Israel para convencer a las personas en los Estados Unidos, y en otras partes del mundo, que Israel es un paraíso de bienvenida y progreso mientras que sus vecinos árabes son homofóbicos y peligrosos para las personas LGBTI.

El lavado rosa es una de las estrategias más efectivas que utiliza Israel para ganar el apoyo de personas de la izquierda en los Estados Unidos, **tratando de usar la empatía que existe con las comunidades LGBTI para alcanzar sus objetivos**.

En su propósito, Israel ha tenido mucho éxito convenciendo al mundo de que los países musulmanes son muy homofóbicos. Sin embargo **existen organizaciones palestinas como Al-Qaws y Aswat, que están luchando por los derechos LGBTI en Palestina.**

Estas organizaciones coinciden en que **la lucha más importante para la población palestina LGBTI es detener el apartheid Israelí**. Sin acabar con el apartheid, todas las otras luchas por los derechos civiles son mucho más difíciles.

En Estados Unidos, hay organizaciones que luchan activamente contra el “Pinkwashing”. En las marchas del orgullo que se dieron en todo el país y el mundo, **varias personas decidieron pronunciarse en contra del apartheid israelí** para alejarse de esa campaña de propaganda.

Las acciones en contra del lavado rosa, también incluyen organizar eventos educativos para la comunidad LGBTI estadounidense para **concientizar sobre esta campaña israelí, y sobre las luchas de los palestinos LGBTI contra el Apartheid**.

<iframe id="audio_19898459" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_19898459_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Escuche esta y más información sobre la resistencia del pueblo palestino en [Onda Palestina](https://archivo.contagioradio.com/programas/onda-palestina/), todos los Martes de 7 a 8 de la noche en Contagio Radio. 
