Title: ¿Plan de Desarrollo Nacional de Duque se olvidó de la paz?
Date: 2019-02-08 10:42
Author: AdminContagio
Category: DDHH, Paz
Tags: acuerdo de paz, Iván Cepeda, Plan de Desarrollo
Slug: plan-de-desarrollo-olvido-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/02/duque-y-la-paz-770x400.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [7 Feb 2019] 

El Congreso fue citado a sesiones extraordinarias que se surtirán en los próximos días, las bancadas se han reunido y tanto la oposición como la bancada de Gobierno preparan sus argumentos para la discusión sobre el Plan de Desarrollo Nacional 2018-2022. El Gobierno ha dicho que es un Plan con enfoque en el gasto social; mientras víctimas, analistas y senadores de la oposición afirman que es un plan que ignora la implementación del Acuerdo de Paz entre otros asuntos.

El senador por el Polo Democrático, Iván Cepeda señaló que desde la oposición ya se está discutiendo el Plan, y en sus análisis encontraron que el plan está puesto en dirección de cerrar las puertas a la implementación del proceso de paz, y nada tiene que ver con los supuestos pilares del gobierno Duque: Equidad, Legalidad o Convivencia Pacífica. Adicionalmente, han extraído los que parecen ser los peores aspectos de este plan, y que deberían ser modificados en las discusiones del legislativo.

### **La implementación del proceso de paz:** 

El Senador del Polo sentenció que el Plan de Desarrollo planteado es inconstitucional, porque no incluye destinaciones concretas para la implementación del Acuerdo de Paz, pese a que la Corte Constitucional determinó que esos recursos debían garantizarse por 20 años, como también lo avaló el abogado e integrante de la Coordinación Colombia, Europa, Estados Unidos (COEUROPA), Alberto Yepes.

El integrante de COEUROPA profundizó su planteamiento al señalar que se redujo y condicionó el presupuesto que reciben las instituciones del Sistema Integral de Verdad, Justicia, Reparación y No Repetición (SIVJRNR), pues se quiere limitar el trabajo de la JEP, mientras se reduce un "40% del presupuesto para la Comisión de la Verdad" y se entregan recursos escasos a la Unidad de Búsqueda de Personas Dadas por Desaparecidas, para la realización de una labor tan compleja.

El Senador también señaló que la bancada de la oposición está preocupada por el nulo desarrollo del punto 1 del acuerdo de paz, en el que se incluyen la Reforma Rural Integral, el Catastro Multipropósito, la creación de un sistema de Salud para el campo y el desarrollo de una política en materia agraria. (Le puede interesar: ["Política de seguridad, un retroceso de 20 años en derechos humanos"](https://archivo.contagioradio.com/politica-de-seguridad/))

### **Ley de Víctimas y los discursos enfrentados** 

Uno de los puntos sobre los que organizaciones sociales habían advertido en el Plan de Desarrollo era el tratamiento que recibían las víctimas, al ser desconocidas como actor, y reducir el presupuesto para su reparación. Al respecto, Cepeda explicó que el Gobierno está manejando una  tesis según la cual hay una duplicación de instituciones responsables de responder a las víctimas: La Ley 1448 de 2011 y las provenientes del SIVJRNR.

En ese sentido, Duque estaría intentando hacer una especie de homologación de ambas instituciones para disminuir los recursos a la Ley, así como recortar la autonomía de la JEP, Comisión de la Verdad y Unidad de Búsqueda. A esta disputa se suma la ocurrida recientemente, con respecto a la Ley Estatutaria de la JEP, en la que un trámite de firma tuvo que ser verificado para que se aprobara este importante Proyecto Legislativo.

Y de otra parte se abre una disputa con el Gobierno por la Política de Defensa y Seguridad que "intenta retrotraernos a tiempos anteriores" al consolidar las redes de informantes, cuyo desenlace final fueron los mal llamados falsos positivos. Sin embargo, Cepeda sostuvo que la diferencia es que ahora tenemos la experiencia de lo ocurrido, así como los mecanismos y fuerzas necesarias para oponernos a estos proyectos.

### **Oposición en el Congreso y ciudadanía en las calles** 

Cepeda anunció que entre los otros elementos que enfrentará la oposición estarán la financiación para los Programas de Desarrollo con Enfoque Territorial (PDET), la política de desarrollo rural y la Reforma Rural Integral (RRI); para ello, la bancada se reunirá para preparar el debate parlamentario, y cómo ocurrió recientemente con la Ley de Financiación, presentarán argumentos y propuestas alternativas.

Sin embargo, el Senador dijo que así como con la educación, la ciudadanía tendrá que movilizarse para que se respeten los recursos de la implementación del proceso de paz, y se respalde el SIVJRNR. Por último, Cepeda señaló que la bancada de oposición está preparando una audiencia pública que se realizará en el Bajo Cauca Antioqueño para tratar el desastre provocado por Hidroituango. (Le puede interesar:["Actitud del Gobierno es un deterioro de la democracia"](https://archivo.contagioradio.com/deterioro-de-diplomacia/))

<iframe id="audio_32373122" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_32373122_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
