Title: A 16 años del Embrujo Autoritario llega Iván, el aprendiz
Date: 2019-09-13 11:02
Author: CtgAdm
Category: Nacional, Política
Tags: asesinato de excombatientes, Fumigaciones de Glifosato contra cultivos ilícitos, Gobierno Duque, Iván Duque, Plan Nacional de Desarrollo
Slug: a-16-anos-del-embrujo-autoritario-llega-ivan-el-aprendiz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/09/WhatsApp-Image-2019-09-19-at-5.04.08-PM-1.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo] 

'El Aprendiz del Embrujo', libro que será dado a conocer el próximo 25 de septiembre, recopila la investigación y participación de sectores académicos, organizaciones sociales e investigadores alrededor del primer año del presidente Iván Duque y expone los múltiples cuestionamientos y formas de obstaculizar la implementación del Acuerdo de un Gobierno que pese a ello maneja un discurso positivo sobre la paz.

Para el abogado y defensor de DD.HH, Alirio Uribe Muñoz se trata de un libro que será presentado en universidades y escenarios internacionales con la intención de hacer incidencia y de alguna manera el gobierno Duque no desemboque en cuatro años que trunquen desarrollo del país.

### Fingir la paz {#fingir-la-paz .entry-title}

Se espera que este informe, el cual reúne múltiples fuentes y citas permita develar si el Gobierno hace lo que dice ante la comunidad internacional, "en materia de paz, les dice que no se preocupen, que va implementar el acuerdo mientras el Gobierno y su partido obstaculizan la implementación".  El hundimiento de las curules de la paz, las  objeciones a la JEP y las directrices que vienen de la Fuerza Pública que colindan con un regreso de las ejecuciones extrajudiciales evidencian esta tendencia.   [(Le puede interesar: Gobierno Duque cumple un año obstaculizando la paz y la garantía de DDHH)](https://archivo.contagioradio.com/gobierno-duque-cumple-un-ano-obstaculizando-la-paz-y-la-garantia-de-ddhh/)

Uribe Muñoz también se refirió a las medidas propuestas del Gobierno para proteger a los líderes sociales que aunque las menciona y destaca a nivel internacional, es desde el propio partido de Gobierno que se estigmatiza y señala a los liderazgos, generando situaciones de riesgo, el caso mas reciente, de la senadora María Fernanda Cabal y su columna de desprestigio contra los indígena del Cauca.  [(Lea también: El CRIC tramitará denuncia formal contra María Fernanda Cabal)](https://archivo.contagioradio.com/cric-denuncia-contra-maria-fernanda-cabal/)

### Reinventar la guerra

Apartes del libro también abordan el Plan Nacional de Desarrollo, donde para Alirio Uribe son más evidentes las similitudes entre el plan de Gobierno actual y la época de la seguridad democrática y un enfoque que considera, va en contravía de la paz, "estamos empezando a ver un escenario de deterioro en materia de seguridad para las comunidades", expresa.

Los ataques al Sistema Integral de Justicia, el cese de las negociaciones con grupos como el ELN,  los nulos avances en el desmonte paramilitar y la impunidad característica frente a los asesinatos de excombatientes y defensores de derechos humanos permiten concluir al abogado que "este gobierno nos devuelve al pasado en términos de violencia y conflicto". [(Le recomendamos leer: No avanzan investigaciones sobre asesinatos de excombatientes)](https://archivo.contagioradio.com/no-avanzan-investigaciones-sobre-asesinatos-de-excombatientes/)

![Embrujo](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/09/WhatsApp-Image-2019-09-19-at-5.04.08-PM-1-1024x389.jpeg){.size-large .wp-image-73920 width="1024" height="389"}

### Privatiza lo público 

El libro realiza un análisis del Gobierno y su contexto internacional,  la tensión con Venezuela, el rol de la Unión Europea o Estados Unidos y de "cómo se acoge a imposiciones en temas como la políticas de drogas", lo que se ha evidenciado en su interés de regresar a la fumigación aérea con glifosato pese a los significativos avances por parte de las comunidades en la sustitución voluntaria. [(Lea también: Reducción de cultivos de uso ilícito es un mérito de las comunidades)](https://archivo.contagioradio.com/reduccion-de-cultivos-de-uso-ilicito-es-un-merito-de-las-comunidades/)

Además de abordar la fumigación áérea, 'El Aprendiz del Embrujo' también se acerca a otras políticas como el fracking y la deforestación, que a su vez "obedecen a políticas e intereses empresariales", Alirio Uribe aclara que no se trata de un recopilado que diga la verdad absoluta pero si está sustentado con información de personas que conocen los temas y la situación del país y que permiten conocer a fondo la forma en que funciona el Gobierno Duque.

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
