Title: Retos, oportunidades, fortalezas y amenazas del gabinete de Claudia López
Date: 2019-12-13 17:52
Author: CtgAdm
Category: Entrevistas, Política
Tags: ‘Bordando para reparar ausencias’, Bogotá, Claudia López, Secretaría
Slug: retos-oportunidades-fortalecas-y-amenazas-del-gabinete-de-claudia-lopez
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/Diseño-sin-título.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio  
] 

Poco a poco, la electa alcaldesa Claudia López ha revelado el equipo que la acompañará en las distintas secretarías de Bogotá a partir del 1 de enero de 2020, fecha en que inicia su periodo. En total, de las 15 secretarías que tiene el Distrito, 9 ya tienen a la persona asignada que estará a su cargo, en términos generales, 4 mujeres y 5 hombres que tienen experiencia en cargos públicos. (Le puede interesar: ["Los compromisos de Claudia López con la Universidad Distrital"](https://archivo.contagioradio.com/los-compromisos-de-claudia-lopez-con-la-universidad-distrital/))

Para hacer una evaluación sobre lo que pueden significar estos nombramientos, Contagio Radio entrevistó a Heidy Sanchez, concejala electa por lista de la Colombia Humana, Unión Patriótica y Mais, quien además recordó que López dijo que haría un concurso de méritos para elegir a los secretarios, "pero lo que se ha venido mostrando es que esto responde a cuotas políticas en concreto".

### **Luis Ernesto Gómez: El ESMAD vestido de blanco, la propuesta del nuevo Secretario de Gobierno** 

El primero en ser nombrado es, según Sánchez, también la primera de las cuotas políticas, "no solamente con esta cartera, sino con la inclusión de candidatos de Activista en la lista Verde". Ella destacó que Gómez es una persona "que llama al diálogo, con el que se puede conversar", y recordó que en 2018 acompañó la campaña de Gustavo Petro y Ángela María Robledo para la presidencia. (Le puede interesar:["Crimen organizado, violencia y narcotráfico acechan Bogotá1"](https://archivo.contagioradio.com/crimen-organizado-violencia-narcotrafico-bogota/))

Sin embargo, dijo estar preocupada ante las recientes declaraciones del nuevo secretario de Gobierno respecto al Escuadrón Móvil Antidisturbios (ESMAD), pues en entrevista con medios, ha señalado que es una fuerza que actúa con exceso de violencia y tal vez, que sus trajes fueran blancos los haría menos violentos. Por su parte, Sánchez sostuvo que el problema del Escuadrón es que aunque tiene protocolos y los conoce, no los cumple, por lo que se requieren cambios estructurales y profundos para este grupo.

### **Adriana Córdoba: Secretaría de Planeación, la cuota de Mockus** 

Luego del nombramiento de Gómez, el de Córdoba fue uno de los más mencionados en redes sociales, por una parte, por quienes cuestionaban que fuese escogida por ser la esposa del exalcalde de Bogotá y ahora senador Antanas Mockus, y por otra parte, por quienes la defendían diciendo que ella tiene suficiente mérito para llegar a esta posición. (Le puede interesar: ["Claudia López y Antanas Mockus se unen a Gustavo Petro"](https://archivo.contagioradio.com/claudia-lopez-y-antanas-mockus-se-unen-a-gustavo-petro/))

Para Sánchez, Córdoba es otra cuota saldada de López, que durante su campaña recibió el apoyo de Mockus. Pero alejándose del debate sobre su idoneidad, la Concejala aseguró que entre sus retos estarán proyectar el próximo Plan de Desarrollo de la Ciudad, así como impulsar el nuevo Plan de Ordenamiento Territorial (POT) en conjunto con la Secretaría de Ambiente.

### **Juan Mauricio Ramírez: La cuota del uribismo en el Gobierno López** 

Finalmente, el último que sería una cuota para Sánchez está a cargo de las finanzas del distrito: Mauricio Ramírez. La razón por la que señaló que era cuota del uribismo es porque él fue coordinador del Plan Nacional de Desarrollo (PND) del gobierno Duque, un plan que resultó contrario a la paz, y favorable a la extracción minero-energética. (Le puede interesar: ["Así fue aprobado el Plan Nacional de Desarrollo en Senado"](https://archivo.contagioradio.com/aprobado-plan-nacional-de-desarrollo-senado/))

### **Nicolás Montero: De las tablas, a la Secretaría de Cultura** 

Respecto al nombramiento de Nicolás Montero en la Secretaría de Cultura, Sánchez aseguró que muchas personas se podían sentir recogidas porque es una persona que ha estado en el área de la cultura (como actor y director), pero que representa los intereses del Teatro Nacional, "que es una élite que ha acaparado el arte y la cultura en Bogotá". No obstante, manifestó que aún es temprano para saber cómo será su gestión, tomando en cuenta que tiene grandes retos que asumir desde la Secretaría, como el manejo que se da a las salas concertadas de La Candelaria, entre otros programas.

### **Xinia Navarro: Del Concejo a la Secretaría de Integración Social** 

Navarro también hizo parte de la campaña a la alcaldía de López, en los últimos 4 años ocupó una curul en el Concejo de Bogotá por el Polo y ahora asumirá la secretaría de Integración Social. Para la Concejala, Navarro es una persona que ha trabajado hace años con los sectores sociales y populares de la ciudad, por lo tanto, se puede esperar una secretaría cercana a la ciudadanía e interesada porque se devuelva el presupuesto a programas de vejez juventud y niñez que se han recortado en los últimos años.

Respecto a los retos, además de los mencionados programas, Sánchez subrayó que se debe resolver el funcionamiento de los comedores comunitarios y jardines infantiles, para que vuelvan a operar en jornadas continuas, así como para que las profesoras que allí trabajan tengan condiciones dignas de empleo, pues tienen contratos de prestación de servicios a pesar de que cumplen un horario.

### **Hugo Acero: Seguridad, una secretaría que debe devolverle la confianza a la ciudadanía** 

Vamos a ver como se formulará la política de seguridad. De hecho, en el plan de Gobierno de Claudia ella plantea la persecución a las bandas criminales, pero también algunas líneas de seguridad humana, que tiene que ver con una formulación integral sugerida desde Naciones Unidas sobre el deber ser desde la seguridad. (Le puede interesar: ["La violencia neoparamilitar en la frontera sur de Bogotá"](https://archivo.contagioradio.com/neoparamilitar-frontera-bogota/))

Esta persona tiene que devolverle la confianza a la gente en materia de seguridad, porque esta fue una bandera importante de Peñalosa, pero la percepción de inseguridad ha aumentado. El hecho de su posesión, es un sociólogo, es experto en temas de seguridad y convivencia, vamos a ver qué puede presentar. Si se expresa en mediana parte lo expresado en el Plan de Claudia, será importante su participación allí.

### **Edna Bonilla, Alejandro Gómez y Carolina Urrutia** 

Respecto a Edna Bonilla, secretaria de educación, Sánchez dijo que ha escuchado buenas referencias provenientes del magisterio, pero destacó que está de acuerdo con los Colegios en concesión, hecho contra el que se ha opuesto en varias ocasiones el exalcalde y líder político de la Colombia Humana Gustavo Petro. Respecto a Alejandro Gómez y Carolina Urrutia, secretarios de salud y ambiente respectivamente, la Concejal no se atrevió a opinar, puesto que no conoce sus hojas de vida.

Lo relevante en tanto sus secretarías será el manejo que darán a puntos que han sido centrales en salud para la ciudad como la atención de los hospitales públicos, el manejo del sistema de ambulancias público y lo que pasará con el Hospital San Juan de Dios. Por otra parte, en cuanto a la secretaría de ambiente, tendrá que afrontar la puesta en marcha de un nuevo POT que respete la estructura ecológica central de Bogotá, y restaure territorios como los cerros ambientales y el Río Bogotá.

Aún falta por conocerse también algunas secretarías que son importantes en cuanto al tema que manejan y los recursos que disponen por lo son la de Planeación, Movilidad, Hábitat y Desarrollo Económico, así como la General, Jurídica y de la Mujer. Las líneas generales de lo que será la alcaldía de López, y por lo tanto de sus Secretarios, se evidenciara en el Plan de Desarrollo 2020-2023 que deberá presentar el próximo año, no obstante, hay un tema que la ciudadanía está pidiendo que se asuma en primer orden de importancia: El ESMAD.

### **La primera tarea para López en el marco del Paro Nacional: Desmonte del ESMAD** 

En razón de los abusos que se han registrado en Bogotá por parte del ESMAD, que incluyen el asesinato de Dilan Cruz, la ciudadanía le ha pedido a López que se refiera a lo que hará su administración con este cuerpo, pues finalmente ella es también la jefe de Policía en el Distrito Capital. A propósito de esta discusión, López dijo que pediría la remoción de Hoover Penilla, director de Policía en Bogotá, sin embargo, Sánchez cuestionó la medida: "Más que modificar la cúpula, que se puede modificar mil veces, se debe cambiar la concepción de cuál es la labor de la policía y cuál debe ser el tratamiento hacia la gente, pasará lo mismo".

En cambio, la Concejal aseguró que se debe desmontar el ESMAD, porque "es una fuerza que no debe existir en un país que la apuesta a la paz", y porque se ha demostrado que no respeta sus propios protocolos. (Le puede interesar:["Indignación en las calles por asesinato de Dilan Cruz"](https://archivo.contagioradio.com/indignacion-en-las-calles-por-asesinato-de-dilan-cruz/))

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe><iframe id="audio_45516804" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_45516804_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
