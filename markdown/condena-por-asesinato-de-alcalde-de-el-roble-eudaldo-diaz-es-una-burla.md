Title: Condena por asesinato de Alcalde de El Roble, Eudaldo Díaz "'es una burla"
Date: 2015-02-26 20:52
Author: CtgAdm
Category: Judicial, Nacional
Tags: condena a la nacion, eudaldo diaz, Paramilitarismo, tito diaz
Slug: condena-por-asesinato-de-alcalde-de-el-roble-eudaldo-diaz-es-una-burla
Status: published

###### Foto de la familia 

<iframe src="http://www.ivoox.com/player_ek_4139751_2_1.html?data=lZagm5yZdY6ZmKiak5qJd6Kkk5KSmaiRdo6ZmKiakpKJe6ShkZKSmaiRic%2Fo08rjy9jYpYzX0NOYrNrFsoy4wtvWxpCoaaSnhqaxw9%2BPcYarpJKw0dPYpcjd0JC%2Fw8nNs4yhhpywj5k%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [**Entrevista con  con Juan David Díaz, hijo de Eudaldo Díaz**] 

Luego de ser conocida la condena a la nación por el caso del ex Alcalde de El Roble Eudaldo Díaz, las criticas no se hicieron esperar debido a la falta condenas contra organismos del estado, entre ellos la presidencia de la república que tuvo responsabilidad en la medida que la denuncia de Eudaldo Díaz fue puesta en conocimiento frente al presidente de ese momento Álvaro Uribe.

**Según Juan David Díaz, hijo de el exalcalde** y quien a estado al frente del proceso,  la armada nacional también tuvo responsabilidad puesto que era la entidad encargada de la seguridad en la zona rural. El DAS hizo un informe en el que señalaba a Díaz de ser auxiliar de la guerrilla del ELN, informe que terminó en las manos de los paramilitares que operaban en la zona.

El hijo del alcalde del roble asegura que la directora de la cárcel donde se encontraban los sicarios permitió la salida de estos del establecimiento y está condenada por el asesinato, por ello no se entienden las razones por las cuales el tribunal administrativo falló sólo en contra de la policía y absolvió al resto de las entidades que tienen responsabilidad en el crimen.

Díaz asegura que, en su calidad de defensor de Derechos Humanos no van a permitir que las instituciones judiciales se burlen de las víctimas de esa manera y que buscarán ante los estrados internacionales y “donde sea necesario” que haya verdad, justicia y reparación integral de manera satisfactoria.

**Eudaldo “Tito” Díaz **se posesionó el 01 de Enero de 2002 como alcalde en el Municipio el Roble- Sucre; A finales de marzo del 2003 después de haber denunciado que lo iban a matar le fueron retirados un escolta de la Policía, dejándolo solo con su escolta particular que el mismo pagaba y que no utilizaba armas.

El 05 de Abril de 2003, hacia el mediodía “Tito” Díaz fue raptado por paramilitares al mando de **Rodrigo Antonio Mercado Peluffo**, conocido como “Cadena”, Tito arribó al sitio conocido como “El Mirador” ubicado sobre la vía que comunica a los municipios de Sincelejo y Tolú, luego de asistir a una reunión con varios políticos y funcionarios públicos del departamento de Sucre con el fin de agilizar su reintegración como Alcalde del municipio El Roble.

Tito había decidido ir porque el **Coronel Norman Leon Arango** iba a estar presente como le comunicó vía telefónica.

El 10 de Abril de 2003, es encontrado el cuerpo sin vida de “Tito” Díaz, en la vía conocida como “La Boca del Zorro”, que comunica Sincelejo a Sampués, a un Kilómetro de la finca de Said Isaac, (cuñado de Salvador Arana Sus, gobernador de Sucre).
