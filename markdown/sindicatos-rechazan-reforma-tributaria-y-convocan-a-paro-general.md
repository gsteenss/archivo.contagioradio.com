Title: Sindicatos rechazan reforma tributaria y convocan a paro general
Date: 2016-09-06 16:01
Category: Movilización, Nacional
Tags: CUT, Movilización, paro, Reforma tributaria
Slug: sindicatos-rechazan-reforma-tributaria-y-convocan-a-paro-general
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/09/CUT.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo Contagio Radio ] 

###### [6 Sept 2016] 

Después del próximo 3 de octubre se conocerá la fecha en la que la Central Unitaria de Trabajadores CUT, convocará a un paro general en rechazo a la reforma tributaria que el Gobierno Santos implementará este año y que es altamente regresiva con los colombianos de las clases más empobrecidas, pues plantea **exenciones tributarias para los empresarios y mayores impuestos sobre los salarios y alimentos de la canasta básica familiar**, según afirma Fabio Arias, presidente de la CUT.

De acuerdo con Arias, lo que hasta el momento se conoce de la reforma tributaria es que **mantendrá las exenciones tributarias a las empresas; eliminará el impuesto sobre la riqueza y aumentará los impuesto sobre los salarios**, haciendo que quienes ganen desde \$1.800.000 deban pagar retención en la fuente y que algunos de los alimentos de la canasta básica familiar paguen impuestos por el orden del 19%. Medidas que impactarán negativamente a toda la población, principalmente a los sectores medios y bajos.

La Central insiste en que además de manifestarse en contra de la reforma tributaria, con el cese de actividades exigirán al Gobierno nacional cumplir [[las demandas presentadas el pasado 17 de marzo](https://archivo.contagioradio.com/el-proximo-17-de-marzo-es-el-dia-cero-para-el-inicio-del-paro-nacional/)], en **un pliego de peticiones que no ha tenido respuesta más que el escrito enviado por el Ministerio de Trabajo** y que los sindicatos rechazan, pues exigen que se instale una comisión negociadora en la que se plantee con qué acciones puntuales se va a dar cumplimiento a [[lo que demandan las agremiaciones](https://archivo.contagioradio.com/habra-paro-nacional-en-el-segundo-semestre-del-ano/)].

Arias concluye asegurando que la **convocatoria a paro no implica que estén en contra de los acuerdos de paz**, pues "lo que más le sirve al país es que el [[plebiscito sea votado positivamente](https://archivo.contagioradio.com/sindicatos-en-colombia-votaran-si-en-el-plebiscito-por-la-paz/)] por la mayoría de los colombianos, porque de esa manera podemos aislar a los señores de la guerra, a los que motivan que sigamos otros cincuenta años en guerra, no estamos diciendo que estamos en contra de los acuerdos, porque son **más de 3.100 sindicalistas los que han sido asesinados en los últimos 30 años**".

<iframe src="https://co.ivoox.com/es/player_ej_12815356_2_1.html?data=kpelk5qXeZehhpywj5aXaZS1kZaah5yncZOhhpywj5WRaZi3jpWah5yncafVw87cjabWrcLnhpewjbXWqdTdxcrb1sqPh7bIjoqkpZKns8_owszW0ZC2pcXd0JCah5yncZU.&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en[ [su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)]] 
