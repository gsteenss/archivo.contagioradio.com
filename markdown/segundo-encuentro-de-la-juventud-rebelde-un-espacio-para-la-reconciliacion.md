Title: Segundo Encuentro de la Juventud Rebelde un espacio para la reconciliación
Date: 2017-07-25 13:13
Category: Nacional, Paz
Tags: Acuerdo de paz de la habana, juventud rebelde
Slug: segundo-encuentro-de-la-juventud-rebelde-un-espacio-para-la-reconciliacion
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/07/jrt.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Juventud Rebelde Tolima] 

###### [25 Jul 2017] 

Este próximo fin de semana se llevará a cabo el Segundo Congreso de la Juventud Rebelde, en la ciudad de Barranquilla, **un escenario que pretende proyectarse con miras al empoderamiento de las y los jóvenes del país para la construcción de paz**. El evento que iniciará desde el viernes 28 de julio y contará con más de 450 delegados de todo el país.

De acuerdo con Cristian Hurtado, vocero de la Juventud Rebelde, el trabajo que ha venido desarrollando esta plataforma consiste en dos líneas, la primera es generar espacios de reconciliación, no solamente con diferentes sectores políticos como lo son las juventudes de centro y derecha del país, sino también con **las y los jóvenes que actualmente hacen parte de la guerrilla de las FARC-EP y que se reincorporarán a la sociedad**. (Le puede interesar: ["Listo el censo socioeconómico de los integrantes de las FARC-EP"](https://archivo.contagioradio.com/43216/))

Su segunda línea de acción se ha desarrollado en el impulso, apoyo y defensa a los Acuerdos de Paz firmados en El Teatro Colón, “**estamos pensándonos desde lo juvenil la agenda de la implementación de los acuerdos de la Habana** y queremos jugar un papel fundamental en la finalización de la mesa de diálogos entre el Ejército de Liberación Nacional y el gobierno” afirmó Hurtado.

El evento se llevará a cabo hasta el próximo 30 de Julio y se desarrollará en diferentes espacios, algunos de ellos abiertos a la participación de toda la ciudadanía y otros cerrados que serán espacios de toma de decisión internos de la Juventud Rebelde, la clausura se realizará en la Plaza de Paz de Barranquilla con un concierto en el que participarán diferentes artistas, entre ellos **Julian Conrrando, BlackEstaban y Martín Batalla, intengrantes de las FARC-EP.**

<iframe id="audio_19991712" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_19991712_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
