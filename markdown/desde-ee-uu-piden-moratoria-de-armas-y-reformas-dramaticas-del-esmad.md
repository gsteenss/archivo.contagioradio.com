Title: Desde EE.UU. piden moratoria de armas y reformas dramáticas del ESMAD
Date: 2019-12-17 18:18
Author: CtgAdm
Category: Movilización, Nacional
Tags: armas, ESMAD, Estados Unidos, WOLA
Slug: desde-ee-uu-piden-moratoria-de-armas-y-reformas-dramaticas-del-esmad
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/27537460656_ed3d676360_c.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Chavo Censura  
] 

En una carta enviada al Congreso de Estados Unidos y al Departamento de Estado, 18 organizaciones sociales de ese país **solicitaron una moratoria para la venta de armas al Escuadrón Móvil Antidisturbios (ESMAD), hasta que dicho grupo no sea reemplazado o reformado.** Además, solicitaron al presidente Duque que "resuelva las protestas pacíficamente por medio de la negociación, incluyendo el amplio liderazgo de los múltiples sectores involucrados en las protestas que lleven a una resolución más profunda de los problemas que motivan este descontento social".

### **Las armas 'menos letales' del ESMAD han causado la muerte de, por lo menos, 34 personas  
** 

A lo largo de la carta, las organizaciones expresan que ha sido un error manejar la protesta social en Colombia como un evento influenciado por países como Cuba o Venezuela, que no tiene fundamentos o que corresponde a un ataque al Estado Social de Derecho. Esta posición explica que se hayan realizado 37[allanamientos previos al paro](https://archivo.contagioradio.com/allanamientos-a-colectivos-y-jovenes-promotores-del-paro-nacional/), aunque 21 fueron declarados como ilegales, y que luego del primer día del paro (21 de noviembre) se declarara toques de queda en Bogotá y Cali.

Adicionalmente, el texto recuerda que en sus 20 años de existencia el ESMAD ha sido causante de la muerte de por lo menos [34 personas](https://archivo.contagioradio.com/en-veinte-anos-el-esmad-ha-asesinado-a-34-personas/), siendo [Dylan Cruz](https://archivo.contagioradio.com/indignacion-en-las-calles-por-asesinato-de-dilan-cruz/), su más reciente víctima, por efecto de un proyectil tipo 'Bean Bag'. De igual forma, las organizaciones referencian los datos suministrados por la Campaña Defender la Libertad, que cuenta más de 400 casos de abusos ejercidos por el ESMAD y la Policía entre el 21 y 27 de noviembre, **"incluyendo 16 afectaciones a los ojos causado por munición de gas lacrimógenos y otros proyectiles".**

### **"Somos de donde se fabrican estas armas, por eso podemos pedir que dejen de venderse"** 

Gimena Sánchez, directora para los Andes de la Oficina en Washington para asuntos Latinoamericanos (WOLA), una de las organizaciones firmantes de la carta, afirmó que el ESMAD no está directamente patrocinado por el Gobierno de Estados Unidos; no obstante, las armas que usa el Escuadrón sí provienen de la empresa **Combined Systems, Inc,** establecida en Pennsylvania, Estados Unidos. (Le puede interesar:["El primer paso para desmontar el ESMAD es modificar la doctrina de seguridad"](https://archivo.contagioradio.com/el-primer-paso-para-desmontar-el-esmad-es-modificar-la-doctrina-de-seguridad/))

Por esa razón, y advirtiendo que el uso de esas armas no es el que hace el ESMAD, hacen un llamado al Departamento de Estado y al Congreso para que establezca una moratoria en la venta de estas armas hasta que no se investiguen los hechos alrededor de las agresiones anteriormente mencionadas. (Le puede interesar: ["Agresiones del ESMAD son solo «daños colaterales» para el Gobierno"](https://archivo.contagioradio.com/agresiones-del-esmad-son-solo-danos-colaterales-para-el-gobierno/))

En la carta, las organizaciones además piden **que estas armas no sean vendidas hasta que el "ESMAD haya sido reemplazado por una nueva fuerza o haya sido reformado para ser una entidad dramáticamente diferente**, con una cultura de respeto a los derechos, una doctrina de des-intensificación, respeto a la protesta pacífica, y el uso mínimo de la fuerza". (Le puede interesar: ["ESMAD usó material no convencional y violó protocolos en movilizaciones estudiantiles"](https://archivo.contagioradio.com/esmad-uso-material-no-convencional-y-violo-protocolos-en-movilizaciones-estudiantiles/))

Para finalizar, la integrante de WOLA sostuvo que "somos de dónde vienen estas armas, por eso podemos pedir que dejen de venderse", y concluyó que **si Colombia quiere mostrarse como una democracia seria y real, como un país en posconflicto, tiene que demostrar que la protesta social se respeta"**, y permitir que los manifestantes se expresen sin temor a ser asesinados o lesionados por la fuerza pública.

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe><iframe id="audio_45644878" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_45644878_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
