Title: En 25 años de Ley de tierras, más del 60% de fincas siguen sin derechos de propiedad
Date: 2019-09-12 14:44
Author: CtgAdm
Category: Comunidad, Nacional
Tags: campesinos, tierra, Unidad Agrícola Familiar, Zonas de Reserva Campesinas
Slug: ley-de-tierras-mas-fincas-siguen-sin-derechos-de-propiedad
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/09/Ley-de-tierras.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Verdad Abierta  
] 

Este jueves a las 5:30 pm se presentará  el libro "**La Unidad Agrícola Familiar (UAF) y el ordenamiento territorial rural en Colombia"**, una investigación que presenta tres estudios de caso en Casanare, Bolívar y Cundinamarca sobre los impactos que ha tenido la Ley de Tierras en el ordenamiento rural del país. El libro será presentado en el Café Pushkin de Bogotá, con la participación de los profesores Darío Fajardo y Carlos Duarte, así como del l**íder campesino César Jerez.**

### **Ley 160 de 1994, un tema de actualidad** 

Según explicó Jerez, el estudio lo lideró la Universidad Javeriana de Cali, Valle del Cauca, en el participaron diferentes investigadores liderados por Carlos Duarte. A propósito de los **25 años que cumple la Ley 160 de 1994,** los académicos quisieron mostrar tres casos tipo sobre lo que puede significar avanzar efectivamente en la implementación de la UAF. (Le puede interesar: ["En 2025 Colombia tendrá un inventario de sus tierras gracias al catastro multipropósito"](https://archivo.contagioradio.com/inventario-tierras-catastro-multiproposito/))

En ese sentido, Jerez afirmó que una adecuada implementación de esta Ley implicaría poner límite a la propiedad agrícola y permitir que sean las comunidades campesinas las que accedan a las tierras baldías de la nación. Así mismo, sería posible avanzar en la formalización de la tierra, porque "**en Colombia, más de un 60% de las fincas no tienen derechos de propiedad"**, porque los territorios están inmersos en conflictos territoriales que deberían ser subsanados mediante el establecimiento de las Zonas de Reserva Campesinas, y el establecimiento de la frontera agrícola.

### **¿Se debe modificar la Ley de Tierras para que funcione?** 

Para el líder campesino es claro que la Ley no se ha implementado en su totalidad por diferentes oposiciones que ha enfrentado entre actores políticos, sociales y económicos, pero la solución no estaría en crear una nueva Ley. "Hay es que exigir el cumplimiento a cabalidad de la Ley 160", aseguró, y añadió que es paradójico que los mismos sectores que intentan modificar la normativa, desvirtuarla o recortar sus alcances no han querido tampoco implementar las zonas de desarrollo territorial que incluía, y pretenden modificar, "por ejemplo, mediante la misma Ley ZIDRES".

En consecuencia, Jerez concluyó que **"se debe aplicar un límite a la extensión de la propiedad para que no se presente concentración de la tierra, la UAF significa eso, y por eso hay que defenderla".** (Le puede interesar: ["ZIDRES tendrían que ser consultadas con comunidades"](https://archivo.contagioradio.com/zidres-ponen-en-riesgo-la-labor-de-mas-de-13-millones-de-campesinos-en-colombia/))

<iframe id="audio_41570154" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_41570154_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>  
**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
