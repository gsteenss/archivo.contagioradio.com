Title: Víctimas de Sucre entregan informe sobre desapariciones forzadas
Date: 2020-05-27 19:00
Author: CtgAdm
Category: yoreporto
Tags: #Dondeestán, Desaparición forzada, Informe, Sucre
Slug: victimas-de-sucre-entregan-informe-sobre-desapariciones-forzadas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/05/EY8ibfLWkAAZlw0.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"textColor":"cyan-bluish-gray","fontSize":"small"} -->

***Por: Movice | CSPP***

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El informe busca visibilizar, **comprender y denunciar el fenómeno de la [desaparición forzada](https://archivo.contagioradio.com/semana-del-detenido-desaparecido-una-oportunidad-de-luchar-para-que-este-flagelo-no-se-repita/)en Sucre a partir de la caracterización de la trayectoria paramilitar en la región** y de su relación con el Estado,  
haciendo énfasis en la responsabilidad estatal en la sistematicidad, generalidad e impunidad de esta práctica.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/Movicecol/status/1265723635458867201","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/Movicecol/status/1265723635458867201

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

El 28 de mayo de 2020, en el marco de la Semana Internacional del Detenido Desaparecido, en la que se les rinde homenaje a las víctimas del grave flagelo de la desaparición forzada y las detenciones ilegales, **el Comité de Solidaridad con los Presos Políticos (CSPP) y el Movimiento Nacional de  
Víctimas de Crímenes de Estado (MOVICE) hacemos entrega de *“Exhumando Justicia y Verdad*".**

<!-- /wp:paragraph -->

<!-- wp:quote -->

> "*Informe sobre la desaparición forzada en Sucre 1988-2008” a los órganos del Sistema Integral de Verdad, Justicia, Reparación y No Repetición".*

<!-- /wp:quote -->

<!-- wp:paragraph -->

El documento **presenta 222 casos de 259 víctimas de desapariciones forzadas de la región y una caracterización de los lugares en donde se encuentran inhumados** **restos de personas dadas por desaparecidas**, con el objetivo de aportar información valiosa a los órganos del SIVJRNR para el cumplimiento de sus mandatos y funciones.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Este informe es producto de la **lucha constante y permanente de las víctimas de desaparición forzada del departamento de Sucre**, quienes a pesar del paso del tiempo no se rinden en la búsqueda de sus  
seres queridos, y en la exigencia de sus derechos a la verdad, la justicia, la reparación y las garantías de no repetición.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

**Durante años, las [víctimas](https://archivo.contagioradio.com/50-anos-de-promesas-fallidas-a-los-embera/) han exigido que el Estado colombiano lleve a cabo las acciones necesarias para establecer el paradero de las personas dadas por desaparecidas** en el marco del conflicto armado y que, en los casos de fallecimiento, se realicen todas las acciones necesarias para la ubicación, exhumación, identificación y entrega digna de los restos a los familiares, y con ello se contribuya a aliviar el sufrimiento ocasionado por la desaparición de su ser querido.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Especialmente, con el informe **buscamos contribuir al proceso que adelanta la JEP**, pues el 30 de agosto de 2018 el [Movice](https://www.justiciaypazcolombia.com/en-la-semana-internacional-del-detenido-desaparecido-comunicado-de-la-mesa-de-trabajo-sobre-desaparicion-forzada-las-desapariciones-forzadas-no-son-una-historia-del-pasado-son-una-realidad-del-presen/) le solicitó que tomara medidas urgentes para la protección de 17 lugares del país en los que se cree se encuentran restos de personas dadas por desaparecidas.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Entre estos **17 lugares se incluyen dos fincas y dos cementerios del departamento de Sucre**, que fueron utilizados por los paramilitares para enterrar a las personas que asesinaban. Es importante que estos lugares  
sean protegidos por la JEP, para que cuando la UBPD empiece la búsqueda, identificación y la entrega digna de los restos, estos no se hayan deteriorado o perdido.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### “Exhumando Justicia y Verdad” tiene nueve capítulos:

<!-- /wp:heading -->

<!-- wp:paragraph -->

i\) Una conceptualización del fenómeno de la desaparición forzada como un crimen de Estado y su relación con el conflicto armado

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

ii\) Las características políticas, económicas y sociales del departamento de Sucre y la dinámica de los grupos paramilitares en esa región

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

iii\) La caracterización de 15 fincas y 18 cementerios del departamento en los que se presume que se encuentran inhumados restos de personas dadas por desaparecidas y una evaluación de sus riesgos

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

iv\) La documentación de desaparición de 259 víctimas agrupadas en 222 casos de desaparición forzada, en donde se incluyó el perfil de la víctimas, las circunstancias de su desaparición, las labores de búsqueda de sus familiares y allegados y el avance del proceso judicial (casi siempre nulo).

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

v\) **Un análisis de los patrones que rodearon la comisión de desapariciones forzadas en el departamento de Sucre;** vi) Un relato que establece la lucha de los familiares de desaparición forzada, particularmente sus estrategias de búsqueda, resistencia y afrontamiento.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

vii\) Un análisis de la necesidad de investigar y judicializar los casos de desaparición forzada en el escenario de la justicia transicional.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

viii\) Algunas solicitudes y recomendaciones a los órganos del SIVJRNR.  
En todo este proceso de documentación y análisis se identificaron cinco patrones de la desaparición forzada en Sucre, relativos a que: i) esta conducta fue una práctica generalizada y sistemática.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

ii\) Fueuna conducta utilizada para favorecer el despojo de tierras; iii) se empleó como un instrumento de exterminio político y social;además, iv) fue un mecanismo de control social; que v) se perpetuó en el tiempo gracias a la ineficiencia judicial y administrativa del Estado al momento de investigarla.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

**En el informe se documenta cómo los paramilitares se valieron del control que ejercían en los territorios para realizar inhumaciones clandestinas con el fin de desaparecer los cuerpos de las personas que asesinaban** en: i) las fincas y haciendas del departamento que fueron despojadas para  
ser utilizadas como centros de operación paramilitar; y ii) los cementerios públicos municipales de los que se apropiaron utilizando el control político que tenían de algunas alcaldías.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Exigencias de las víctimas frente al SIVJRNR

<!-- /wp:heading -->

<!-- wp:paragraph -->

Con la entrega del Informe “Exhumando Justicia y Verdad. Informe sobre la desaparición forzada en Sucre 1988-2008”, se espera que la CEV, en su informe final, de cuenta de la magnitud de la desaparición forzada y de los nexos de grupos paramilitares y agentes estatales para cometer este y  
otros crímenes.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por otro lado, **se espera que UBPD avance en la construcción participativa de planes regionales de búsqueda para el departamento de Sucre** que permitan buscar a las personas dadas por desaparecidas.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Finalmente, **se espera que la JEP de apertura a un macro caso que permita la investigación, juzgamiento y sanción de la desaparición forzada como una grave violación a los derechos humanos** que generó múltiples consecuencias y que permanece en la impunidad a pesar del transcurso del  
tiempo.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

De igual forma, que avance en el proceso de medidas cautelares y tome acciones para la protección de estos lugares hasta que la UBPD realice las labores de exhumación e identificación de restos.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"center"} -->

***¡Somos semilla, somos memoria, somos el sol que renace ante la impunidad!***

<!-- /wp:paragraph -->
