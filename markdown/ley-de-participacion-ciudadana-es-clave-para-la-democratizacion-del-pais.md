Title: Ley de Participación ciudadana es clave para la democratización del país
Date: 2017-08-11 15:34
Category: Entrevistas, Paz
Tags: acuerdos de paz, Proyecto de Ley de Participación
Slug: ley-de-participacion-ciudadana-es-clave-para-la-democratizacion-del-pais
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/07/Participación-política-Colombia.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo Particular] 

###### [11 Ago 2017]

La ley Estaturia de Participación iniciará debates en el congreso, luego de la presión ejercida por la sociedad a través de las redes sociales, para que el gobierno la presentara al congreso. **Con este articulado los diferentes mecanismos de la democracia, incluidos en la Constitución de 1991, contará con un respaldo y reconocimiento legal**.

La ley estatutaria de Participación es un proyecto legislativo que fue aprobado en el 2015 y busca fortalecer la democracia a partir de tres formas: los mecanismos de participación ciudadana como el plebiscito, la revocatoria, la consulta popular, etc; la rendición de cuentas para ejercer un control social a lo público y **la coordinación para la participación ciudadana, a través del Consejo Nacional de Participación Ciudadana.**

Este Consejo está conformado por un representante del Ministerio del Interior, un representante del Departamento Nacional de Planeación, un representante de las gobernaciones, otro por las alcaldías y finalmente integrantes de plataformas ciudadanas. El proyecto de ley, además, fue el resultado de **6 foros nacionales en los que participaron organizaciones sociales y más de 4.000 personas**, de allí se escogieron 24 representantes que hacen parte de la Comisión Nacional de Diálogo y que discutirán este articulado.

### **Ley de Participación Ciudadana y Reforma Política van de la mano** 

De acuerdo con David Flórez, vocero de la plataforma política Marcha Patriótica, este articulado va de la mano con la Reforma Política pactada en los acuerdos de paz de La Habana, debido a que **es necesario que se fortalezcan todos los elementos que constituyen la democracia de un país**. (Le puede interesar: ["Partidos minoritarios logran acuerdo para impulsar reforma política"](https://archivo.contagioradio.com/partidos-minoritarios-buscan-fortalecer-su-incidencia-en-la-contienda-politica/))

“Se necesita, no solamente, que los partidos políticos tengan garantías para la participación electoral, que haya unas reglas de juego más democráticas, sino que también, **los ciudadanos que no necesariamente hacen parte de un partido político, sino que se manifiestan a través de organizaciones sociales**, tengan garantías para participar en Colombia” afirmó Flórez.

Sin embargo, será el congreso finalmente el que realice modificaciones al proyecto de ley, y de acuerdo con Flórez habrían partidos políticos que no estarían interesados en que este pase conforme a como se construyó, **no obstante considera que será la sociedad civil la que salga en la defensa del articulado**. (Le puede interesar: ["Modificaciones de la JEP violarían el Acuerdo de Paz"](https://archivo.contagioradio.com/jep-debe-garantizar-derecho-a-la-justicia-para-las-victimas-ivan-cepeda/))

<iframe id="audio_20289472" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_20289472_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
