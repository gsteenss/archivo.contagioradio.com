Title: No hay garantías para acompañar elecciones atípicas en La Guajira:MOE
Date: 2016-11-02 13:19
Category: Nacional, Política
Tags: colombia, elecciones, Elecciones atipicas, Falta de Garantias, Guajira, La Guajira, Misión de Observación Electoral, MOE, Votaciones
Slug: no-hay-garantias-para-acompanar-elecciones-guajira-moe
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/moe-elecciones-guajira.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: MOE ] 

###### [2 Nov. de 2016] 

**La Misión de Observación Electoral MOE** dio a conocer que **no llevará a cabo el ejercicio de observación electoral durante la jornada electoral atípica** que se llevará a cabo en el departamento de La Guajira y donde se espera poder elegir gobernador.

La MOE ha asegurado que ha tomado la decisión de salir de la Guajira, dado el **nivel de polarización y de amenazas entre las campañas, lo cual no genera las suficientes garantías de transparencia y seguridad para efectuar este ejercicio de control ciudadano.**

Adicionalmente, Alejandra Barrios directora de la MOE, agregó que esta disposición se toma porque “las acusaciones que se están haciendo entre las campañas están directamente relacionadas con compra de votos, participación en política de funcionarios públicos, una falta de credibilidad absoluta que tienen las campañas frente al trabajo que van a hacer los jurados de votación”.

**Dentro de los municipios en los cuales, según la MOE, se puede presentar fraude electoral el próximo domingo se encuentran Urumita, La Jagua del Pilar y El Molino**, por ello ésta organización ha realizado una petición especial a las autoridades a hacer un control exhaustivo de los votantes a través de la identificación biométrica.

Así mismo, Barrios afirmó que el acompañamiento también debe hacerse a los jurados dado que **en lugares como Urumita, Manaure, Riohacha, Albania y Uribia han existido unos altos niveles de votos nulos, que según la directora de la MOE, llegan casi al 4%, cifra que no se entiende, puesto que el promedio nacional es de 2% de votos nulos.**

“Eso puede significar que de manera intencional, los votantes de estos municipios anulan sus votos o se podría efectuar algún tipo de acción que termine favoreciendo algún candidato” confirmó Alejandra Barrios.

Los niveles de polarización política en La Guajira se han traducido entre otras en amenazas contra el Gobernador encargado Jorge Enrique Vélez y otros líderes políticos y sociales. Le puede interesar: [La crisis de La Guajira en voz de las comunidades](https://archivo.contagioradio.com/la-crisis-de-la-guajira-en-voz-de-las-comunidades/)

**Situación en La Guajira**

Para la directora de la MOE, la crisis política, económica, social  por la que atraviesa La Guajira, la relación entre la política, la ilegalidad y la corrupción que existe en este departamento y en otros del país, han obligado a dicha organización a tomar la decisión de no acompañar estas elecciones.

Y manifestó que **“la democracia se sustenta en los procesos electorales, pero no en cualquier tipo de proceso electoral. No es suficiente la presencia el día de elecciones o el seguimiento pre-electoral para poder avanzar hacia elecciones libres y transparentes,** es necesario el compromiso de la ciudadanía, de los candidatos y de las campañas que se presentan a las elecciones”. Le puede interesar: [Las propuestas de los Wayúu para superar crisis humanitaria en La Guajira](https://archivo.contagioradio.com/las-propuestas-de-los-wayuu-para-superar-crisis-humanitaria-en-la-guajira/)

Luego de conocida la noticia en la que la MOE no estará presente en dichas elecciones, La organización ha dado a conocer que para este proceso electoral estarán desarrollando una campaña que busca incentivar la denuncia de irregularidades electorales a través de la herramienta PILAS CON EL VOTO en distintos medios de comunicación.

**Que se suspendan las elecciones: Indígenas Wayúu**

Por su parte, en días pasados cerca de **900 comunidades indígenas que hacen parte de la Asociación de Autoridades Tradicionales Indígenas Shipia Wayúu, habían solicitado al presidente Juan Manuel Santos a través de un derecho de petición, suspender las elecciones atípicas a gobernador de La Guajira.**

En la misiva se afirmaba que “la situación social y política de La Guajira requiere atención especial de sumo cuidado, pues está atravesando una crisis humanitaria causada, entre otras, por la corrupción“. Sin embargo, **hasta la fecha no ha habido respuesta por parte del primer mandatario.**

<iframe id="audio_13586447" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_13586447_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>
