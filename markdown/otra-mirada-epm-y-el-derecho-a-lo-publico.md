Title: Otra Mirada: EPM y el derecho a lo público
Date: 2020-08-20 21:02
Author: AdminContagio
Category: Otra Mirada, Programas
Tags: Alcalde Daniel Quintero, comunidades afectadas por hidroituango, EPM
Slug: otra-mirada-epm-y-el-derecho-a-lo-publico
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/01/Hidroituango-EPM-e1484757457706.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right"} -->

[Foto: Hidrotuango EPM]{.has-inline-color .has-cyan-bluish-gray-color}

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El alcalde de Medellín, Daniel Quintero, anunció este lunes que Empresas Públicas de Medellín (EPM) demandará a los constructores, interventores, diseñadores y aseguradores de Hidroituango. 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Después de esto, ocho miembros de la junta directiva enviaron una carta renunciando a la Junta Directiva. Posteriormente el alcalde Quintero, reveló esta semana cinco de los ocho nuevo miembros.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Explicando qué le espera EPM estuvieron William de Jesús Gutiérrez, Asociación de mineros y pesqueros de Puerto Valdivia, Fernando Quijano presidente de la Corporación para la Paz y el Desarrollo Social (Corpades), Luz Marina Múnera, exconcejal de Medellín y Freddy Agudelo, abogado y liderato Grupo Jurídico especialista en gerencia de servicios.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Los panelistas también comentan cómo afecta la división que se ha generado dentro de EPM y cuál es la realidad no contada que está viviendo la empresa EPM actualmente.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Ahora, con esta nueva junta directiva explican qué perfiles deberían tener cada uno de los miembros y qué se puede hacer para que las empresas públicas no respondan a intereses de un gobierno transitorio, sino que por el contrario, se garantice el respeto y la autonomía de lo público.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Igualmente, representantes de las comunidades que más se han visto afectadas por el proyecto hidroeléctrico Hidroituango comparten cómo están viviendo las víctimas y qué acciones se están generando desde las mismas comunidades para seguir defendiendo la vida digna y cuidar el patrimonio público. (Le puede interesar:  [«Ituango en una encrucijada de pobreza y violencia»](https://archivo.contagioradio.com/ituango-en-una-encrucijada-de-pobreza-y-violencia/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Finalmente, comparten cuáles han sido las respuestas por parte de EPM, el Gobierno de Quintero y organismos internacionales a las comunidades y movimientos que siguen reclamando tanto por las afectaciones a la comunidad como al medio ambiente. 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Si desea escuchar el programa del 14 de agosto: [Las pandemias que enfrentan los pueblos indígenas](https://www.facebook.com/contagioradio/videos/297571844645251)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"textColor":"cyan-bluish-gray","fontSize":"small"} -->

Si desea escuchar el programa completo

<!-- /wp:paragraph -->

<!-- wp:html /-->
