Title: Parte hacia Arauca la caravana "Por una paz completa"
Date: 2015-12-09 17:41
Category: Nacional, Paz
Tags: Arauca, dialogos, dialogos de paz, ELN, FARC, Iglesia cristiana Menonita, Katherine Torres, Movilizaciones, paz, proceso de paz, Reconciliación
Slug: parte-hacia-arauca-la-caravana-por-una-paz-completa
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/12/Niños-araucanos-e14496950952851.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: porunapazcompleta 

###### [9 Dic 2015] 

En el marco de la campaña ‘Por una paz completa’, se va a desarrollar la caravana por Arauca, una movilización que se llevará a cabo en uno de los departamentos que más ha sentido las consecuencias del conflicto. Esta iniciativa tiene como propósito “formar desde la comunidad y las organizaciones el acompañamiento para formar una masa crítica favorable frente al proceso de paz entre las Farc, el ELN y el Gobierno colombiano”.

Frente a la disyuntiva coyuntural del proceso de paz y de la solución política del conflicto armado en Colombia, se han planteado mecanismos para acelerar los diálogos y refrendar los acuerdos que se pacten en La Habana entre el Gobierno Nacional y las FARC. Recientemente, sectores sociales proponen que el ELN se vincule a una misma Mesa de Conversaciones.

La caravana que recorrerá las poblaciones de Tame, Fortul, Arauquita, Saravena y la capital del departamento, busca visibilizar el apoyo de las comunidades hacia el proceso de paz y la voluntad de los habitantes de implementar y materializar la reconciliación en elementos de diálogo constructivo, pluralización y la superación de la polarización política y social que han dejado 50 años de guerra.

Una de esas organizaciones es la Iglesia Cristiana Menonita, que adelanta el programa ‘Puentes Para la Paz’, que viene trabajando en el acompañamiento y la constitución de las comisiones ciudadanas de reconciliación y paz en las zonas de Arauca, en la Costa Caribe y en el nororiente antioqueño.

Katherine Torres, quien integra esta organización, afirma que los diálogos en la Habana, deben alinearse con las conversaciones entre el ELN y el Gobierno, para lograr “una paz completa, en donde en dejar a un protagonista del conflicto no se repita en la historia”.

Torres, manifestó que grupos y líderes sociales del Casanare se vincularán a esta iniciativa, que busca llegar al Cuarto Foro Internacional de Horizontes para la Paz y la Reconciliación, respaldado por más de 30 organizaciones. “Hemos visto como esta campaña ha sido apoyada y diversos sectores han venido participando en una manera activa”, dijo.

La representante de la Iglesia Cristiana Menonita reiteró que la paz completa se alcanzará cuando se desmonte el paramilitarismo y será sostenible cuando los conflictos sociales y económicos de las comunidades se tengan en cuenta. Por ello expresó que “debe entablarse un diálogo integral para que la explotación de los recursos naturales y el tema de la economía extractiva, no perjudique a los habitantes de estas regiones”.

Finalmente, Torres afirmó que entre todos debemos llegar a la reconciliación nacional y esta se logra cuando se generen transformaciones que fomenten la inclusión social y el diálogo sincero entre las empresas, las instituciones y las comunidades.
