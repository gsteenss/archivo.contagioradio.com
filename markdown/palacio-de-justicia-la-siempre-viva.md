Title: "La siempre viva"
Date: 2015-11-04 09:47
Category: Opinion, Tatianna
Tags: 30 años palacio de justicia, Arias Cabrales, Corte Interamericana de Derechos Humanos, Cristina de pilar guarin, Palacio de Justicia, plazas vega
Slug: palacio-de-justicia-la-siempre-viva
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/11/palacio-de-justicia-contagio-radio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

#### **[Por [Tatianna Riveros](https://archivo.contagioradio.com/opinion/tatianna-riveros/)- [@[tatiannarive]** 

###### [4 Nov 2015] 

Veinte años tuvo que cumplir la obra estrenada en 1994 para que fuera a verla, la última función de una obra que sin pensarlo dejaría más preguntas que  respuestas.

La siempre viva cuenta la historia del antes y un poco del después de lo que vivió una de las familias de las  desaparecidas del Palacio de Justicia, que hoy  vuelve a ser noticia luego que la Fiscalía lograra identificar tres de los cuerpos que durante muchos  años reposaron en esta entidad (incluido el de ella, la protagonista de esta obra).

La historia es la de Julieta (en la vida real llamada Cristina Guarín), y se  desarrolla en una casa, donde diversos personajes conviven y aparecen en escena: una afro (por no decir negra y que chao racismo me contacte), un payaso (esposo de la afro, creía que su mujer era un objeto), el hermano de Julieta (que en la historia no hace nada), su mamá (la de Julieta) quien cree    que volverá a ver su hija, y el dueño del inquilinato. Cuentan como un suceso  nacional se vuelve una tragedia familiar, utilizan un radio como narrador, escalofriantes audios se escuchan durante la obra, las luces se apagan una y otra vez, el vestuario impecable, vestuario que en nuestros tiempos, sirvió para que familiares pudieran identificar a las tres mujeres de las que, no se sabe la  causa real de su muerte.

En el 2014, la Corte Interamericana de Derechos Humanos condenó al estado  colombiano, por las irregularidades cometidas por militares en lo que se llamó   la retoma del Palacio y donde desaparecieron diecisiete  personas, entre ellas   Cristina. A la fecha, el General Armando Arias Cabrales y el Coronel Alfonso Plazas Vega, fueron condenados por las actuaciones del 6 y el 7 de noviembre de 1985, aunque Plazas Vega, espera que la Corte Suprema decida sobre el recurso interpuesto. Investigado estaban el Coronel Luis Carlos Sadovnik (quien falleció en el 2009), y el Coronel Edilberto Sánchez Rubiano, sin embargo, son más las preguntas que respuestas porque no todas las familias de los desaparecidos ese día han tenido reparación, y en cambio ninguna ha tenido    verdad ni justicia. Y no, no porque no haya cárcel para los involucrados en la    toma y retoma del Palacio creo que no hay justicia, no hay justicia porque los   años siguen pasando y la brecha entre lo que sucedió y lo que se ha contado,  se hace más grande.

La siempre viva es una planta, una que tiene la habilidad de almacenar agua en sus hojas para vivir en lugares rocosos, son resistentes a condiciones difíciles  de crecimiento, crece varios años antes de poder florecer, lo que explicaría  (desde lo que yo creo) por qué se llama así una de las obras más importantes para la historia y el teatro.

Queda entonces el sinsabor de esta historia, en la obra Julieta visita a su mamá todas las noches y le pide que le lave el cabello, en esta vida, Cristina apareció muerta, en una fosa común, sin saber las causas de su muerte, pero  viva al salir del Palacio; La corte aún no decide sobre el recurso de Plazas Vega, que tras conocer la noticia de que la Fiscalía identificó  los cuerpos, salió en los medios asegurando que siempre ha dicho la verdad.  Si se perdió la obra, vea  la película, si se perdió la película, lea y deje de creerle a la esposa del Coronel condenado que hoy es senadora, al que le dio indulto al M-19, y a la entidad que tiene contratos por cuatro mil millones de pesos con la periodista que hace algoritmos.
