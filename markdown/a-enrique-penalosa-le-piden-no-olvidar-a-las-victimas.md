Title: A Enrique Peñalosa le piden no olvidar a las víctimas
Date: 2015-12-21 16:20
Category: DDHH, Nacional
Tags: Bogotá Humana, Enrique Peñalosa, víctimas
Slug: a-enrique-penalosa-le-piden-no-olvidar-a-las-victimas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/12/85084b1a94e12e07d38fb85381e6aca2.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: El Espectador 

###### [21 Dic 2015] 

Desde la Alta Consejera para los Derechos de las Víctimas, la Paz y la Reconciliación, se dio a conocer  una carta dirigida al alcalde electo de Bogotá, Enrique Peñalosa, donde se señalan los avances de la ciudad en materia de víctimas, pero también los retos que continúan teniendo en cuenta, que en el plan de gobierno de Peñalosa, **no existe una sección especial para las víctimas ni las refiere directamente en el documento.**

En la carta se le solicita al alcalde electo no abandonar el tema de las víctimas, **ya que a Bogotá han llegado 640 mil víctimas que han sido atendidas con los programas implementados por la Bogotá Humana,** que han sido aplaudidos por las Naciones Unidas y además por la Unidad de Víctimas que anunció la clasificación de alcaldías y gobernaciones según su gestión en el tema de víctimas, donde Bogotá ocupó los primeros lugares a nivel nacional.

De acuerdo con la Unidad de Víctimas, **la capital obtuvo el nivel alto sobresaliente,** de tal manera que Ana Bernal,  la Alta Consejera para los Derechos de las Víctimas, la Paz y la Reconciliación, solicita a través del documento“**profundizar lo que Bogotá Humana ha hecho, reconociendo los derechos de los más vulnerables”.**

En la carta se resaltan el Modelo Dignificar de Asistencia y Atención integral, la Creación del Sistema Distrital de Atención y Reparación a las víctimas SDARIV; la creación del Programa de acompañamiento legal a las víctimas para garantizar medidas de restitución de tierras, indemnizaciones, exoneración de pasivos, entre otros; la creación de un Protocolo propio de participación en la ciudad y la construcción del Centro de Memoria Paz y Reconciliación que ha logrado vincular directamente a miles de víctimas apoyando sus iniciativas y actividades.

Sin embargo, la Alta Consejera, asegura que aún queda mucho por hacer, “**causándonos gran preocupación la lentitud del proceso de indemnización** que de acuerdo a la velocidad con que se está asumiendo por parte de la nación, si congeláramos hoy el número de víctimas en la ciudad, no alcanzaría a pagarse en 60 años, indicativo de que son necesarios nuevos recursos y metodologías más eficientes para la reparación a las víctimas”, dice la carta.

Finalmente, se hace el llamado a que Enrique Peñalosa y su equipo no abandonen a las víctimas y en cambio fortalezcan los aportes hechos por el gobierno de la Bogotá Humana de acuerdo con los retos frente a la Paz y el posconflicto*.*

**Aquí la carta:**

*Asunto: Bogotá y la tarea por dignificar a las víctimas. Respetado*

*Dr. Peñalosa:*

*En virtud a que usted ha sido elegido para asumir el gobierno de Bogotá y teniendo en cuenta que estamos en una importante etapa de la vida nacional referida al tema de la paz, en la que se ha puesto en el centro de este propósito la condición de mas de 7.5 millones de víctimas que lamentablemente ha dejado el conflicto interno armado, me dirijo a usted con la responsabilidad de ponerlo en contexto sobre los avances y desafíos que deja el gobierno de la Bogotá Humana en materia de política pública para garantizar los derechos de esta población.*

*Bogotá ha sufrido los impactos del conflicto armado de diferentes formas, por una parte, ha sido escenario durante su historia de acontecimientos que estremecieron a la ciudadanía (grandes magnicidios, artefactos explosivos, atentados,etc); por otra, ha sido receptora de miles de víctimas provenientes de diversas regiones del país. Según registro oficial, más de 640,000 víctimas han declarado en nuestra ciudad, siendo ellas en su mayoría desplazadas.*

*Casi simultáneo con la puesta en marcha de la Ley de víctimas y restitución de tierras, inicia el gobierno de la “Bogotá Humana” que con vocación de garantizar los derechos de las víctimas crea mediante los decretos 059 de 2012 y 284 de 2012 la Alta Consejería para los Derechos de las víctimas, La Paz y la Reconciliación”.*

*De acuerdo con esta Ley, los entes territoriales tienen funciones y competencias en cuatro escenarios de la implementación de la política pública de víctimas a saber:*

1.  *Goce efectivo de derechos: entrega de Ayuda Humanitaria Inmediata; vinculación a los servicios del Estado; Garantía de derechos económicos sociales y culturales; y, ejercicio efectivo de la participación;*
2.  *Corresponsabilidad para la reparación integral: Estrategias para aportar a la reparación integral de las víctimas tanto individual como colectiva; Construcción de memoria histórica; reparación simbólica y medidas de satisfacción;*
3.  *Coordinación (Vertical y horizontal): ejercer la coordinación del Sistema Distrital de Atención y Reparación a las Víctimas del Conflicto Armado; Funcionamiento del Comité Distrital de Justicia Transicional y los Comités Locales de Justicia Trancisional; Coordinación y articulación con el Sistema Nacional de Atención y Reparación a Víctimas; coordinación y articulación con otros entes territoriales; funcionamiento de sistemas de información.*
4.  *Seguimiento y evaluación: Reportes sobre la implementación de la política pública de víctimas en el Distrito; seguimiento y monitoreo de la la política y las dinámicas asociadas al conflicto armado a través del observatorio de víctima.*

*Poniendo un plus en cada uno de estos aspectos, Bogotá asumió estas responsabilidades y logró un importante cumplimiento de sus metas en la mayoría de estos aspectos de la siguiente manera:*

-   *Modelo Dignificar de Asistencia y Atención integral: construcción, adecuación y puesta en marcha de 7 Centros de Atención en las localidades de Bosa, Ciudad Bolívar, Rafael Uribe Uribe, Suba, Barrios Unidos, Kennedy. En estos centros se concentra la oferta del Estado tanto nacional como distrital para atender todas las demandas de las victimas. contamos con 3 equipos de Unidades moviles para orientar y visitar a las victimas en terreno. en esta estrategia hemos beneficiado a 43.000 hogares.*
-   *Creación del Sistema Distrital de Atención y Reparación a las víctimas SDARIV, que logra la coordinación interinstitucional y la identificación de todos los recursos de las diferentes entidades de la ciudad aplicados a la población víctima del conflicto armado en Bogotá. Instalación del Comité de Justicia Transicional y aprobación y puesta en marcha del Plan de Acción Distrital. La totalidad del presupuesto invertido en víctimas en la ciudad hasta diciembre de 2015 es de de \$1.075.450.000.000.*
-   *En materia de Reparación Integral: creación del Programa de acompañamiento legal a las víctimas para garantizar medidas de restitución de tierras, indemnizaciones, exoneración de pasivos, entre otros. Programa del que se han beneficiado 5.700 núcleos familiares. En materia de Reparación Colectiva Bogotá está en proceso de reconocimiento e implementación de 5 Planes con diferentes sujetos de reparación colectiva,  entre los que se encuentran organizaciones de mujeres afectadas por la violencia, comunidades indígenas, organizaciones de familiares de desaparecidos forzados, y, ha propuesto procesos de reparación colectiva con enfoque territorial. Asimismo ha hecho un esfuerzo por acompañar los procesos de retornos y reubicaciones individuales y colectivos a mas de 4.000 familias desplazadas.*
-   *Consecuentes con el espíritu democrático de la participación invocado en la Ley de víctimas y considerando el tamaño y el número de víctimas en la ciudad impulsamos la creación de un Protocolo propio de participación en la ciudad cuyo resultado ha sido la existencia de 14 Mesas de participación de víctimas en las localidades, 1 Mesa Distrital y 3 Mesas autónomas en las que participan comunidades indígenas, población afrodescendiente y mujeres.*
-   *En materia de medidas de satisfacción, Bogotá es la primera ciudad que construye un Centro de Memoria Paz y Reconciliación que ha logrado vincular directamente a miles de víctimas apoyando sus iniciativas, motivándolas a participar activamente en la construcción de una memoria viva con experiencias como “Costurero de la Memoria”; potenciando la riqueza de la diversidad cultural a través de procesos como “sabores y saberes”, entre otras. El Centro de Memoria Paz y Reconciliación ha impulsado la construcción del Eje de la Memoria sobre la calle 26 donde está también ubicado el Parque de la Reconciliación y próximamente el Gobierno Nacional  construirá muy cerca, el Museo Nacional de la Memoria. Asimismo este Centro es hoy epicentro de reflexiones y de encuentros encaminados al análisis del proceso de paz y punto de encuentro de las grandes movilizaciones del 9 de abril Día Nacional de la Memoria y la Solidaridad con las víctimas del conflicto armado, vinculando de manera activa y permanente a 163,680 personas.*

*Gracias a todas estas acciones, y a la implementación de esta política, Bogotá ha sido certificada por la Unidad Nacional de Víctimas y el Ministerio del Interior con calificación Alta 96/100 en 2012; 98/100 en 2013 y Alta Sobresaliente 126/100 en 2014 por el compromiso que ha demostrado tener en la implementación de la Ley de víctimas y Restitución de Tierras.*

*No obstante hace falta mucho por hacer, causándonos gran preocupación la lentitud del proceso de indemnización que de acuerdo a la velocidad con que se está asumiendo por parte de la nación, si congeláramos hoy el número de víctimas en la ciudad, no alcanzaría a pagarse en 60 años. Indicativo de que son necesarios nuevos recursos y metodologías mas eficientes para la reparación a las víctimas. Los entes territoriales estamos a la espera de un mejor sistema de corresponsabilidad nación-territorio.*

*Asimismo, las diferencias de enfoque en la inclusión de las víctimas en los programas de vivienda y la poca diversidad en la oferta que no puede ser exclusiva de vivienda nueva, la necesidad de mayores recursos y garantías para una reparación integral, la eficiencia en los procesos de restitución de tierras y las garantías de seguridad para los retornos son problemáticas latentes que deben ser consideradas con voluntad política, mejor institucionalidad y músculo administrativo.*

*Hace unas 24 horas que Colombia y el mundo presenciaron un hecho trascendental para La Paz de nuestro país: el Gobierno Nacional y las FARC firmaron el acuerdo sobre Víctimas y Justicia para La Paz. Este acuerdo aún pone más en alto y en el centro a esta inmensa población que ha sufrido desgarradoramente la violencia, por eso le invito a que en su gobierno se profundice en los siguientes temas y acciones:*

1.  *Estrategia de reparación colectiva con enfoque territorial, político y diferencial en perspectiva de construcción de paz y como eje estructurador de ejercicios de transformación ciudadana para la inclusión social y la paz.*
2.  *Estrategia de representación de víctimas para acceder a la reparación integral con el objetivo de avanzar de manera eficaz en el goce efectivo de derechos, la inclusión social y la reparación integral.*
3.  *Centro de Memoria, Paz y Reconciliación como estrategia de transformación social y comunitaria para la paz y el empoderamiento de procesos organizativos.*
4.  *Procesos locales y distritales de participación para la incidencia en la formulación, implementación y seguimiento de la política pública de víctimas.*
5.  *Intervenciones territoriales en perspectiva de inclusión, paz y reconciliación para la superación de contextos de exclusión y violencia; lucha contra la discriminación y tratamiento Pacífico de conflictos.*

*Dr. Peñalosa, conscientes de que La Paz y los derechos de las víctimas trascienden los gobiernos y son ante todo políticas de Estado, lo insto a usted y a su equipo a fortalecer los aportes hechos por el gobierno de la Bogotá Humana y a considerar la necesidad de tener una institucionalidad más acorde con los retos de La Paz y el posconflcito, porque si bien se logra un acuerdo entre las partes enfrentadas, la tarea de la reconciliación nos compete a todos durante un largo tiempo, profundizando la democracia en todos los aspectos de la vida de la sociedad. Así tendremos una ciudad más amable en la que la Vida sea el disfrute pleno de los derechos ciudadanos.  *

*Cordialmente,*

*Ana Teresa Bernal Montañez Alta*

*Consejería para los Derechos de las Víctimas, la Paz y la Reconciliación.*
