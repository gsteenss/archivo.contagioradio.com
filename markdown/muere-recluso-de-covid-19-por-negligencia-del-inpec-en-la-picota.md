Title: Muere recluso de Covid 19 por negligencia del INPEC en la Picota
Date: 2020-08-06 15:07
Author: AdminContagio
Category: Actualidad, DDHH
Tags: #Cárcel, #covid19, #Crisis, #INPEC, #Picota
Slug: muere-recluso-de-covid-19-por-negligencia-del-inpec-en-la-picota
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/08/17-02-09-Colombia-prison-la-modelo-1.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

Prisioneros políticos de la Cárcel de la Picota, denunciaron el fallecimiento del recluso Néstor Evelio Forigua. El hombre presentó síntomas de Covid 19 desde hace más de una semana, s**in que se diera la atención oportuna por parte de la guardia del INPEC.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

De acuerdo con los prisioneros políticos, la atención médica que brindaron a Forigua fue proporcionada por sus compañeros de patio. Asimismo, fueron ellos quienes lo trasladaron de urgencia hasta **la enfermería de la reclusión para que recibiera primeros auxilios.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Actualmente, en este centro de reclusión han fallecido tres personas producto del Covid 19 y se estima que hay más de 300 personas contagiadas. A la fecha, se registran 3.196 contagios de la Covid-19, en donde 2.582 son reclusos, **178 miembros del cuerpo de custodia, 22 auxiliares y 14 funcionarios administrativos.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Organizaciones defensoras de derechos humanos, continúan exigiendo al presidente Iván Duque acciones urgentes para evitar muertes y contagios en las cárceles. También reiteran que los casos que se presenten en los centros de reclusión son responsabilidad del Estado y que irían en contravia de acuerdos y pactos internacionales. (Le puede interesar: [Más de 186 mujeres padecen Covid 19 en la Cárcel El Buen Pastor](https://archivo.contagioradio.com/mas-de-186-mujeres-padecen-covid-19-en-la-carcel-el-buen-pastor/)")

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Frente a esta situación, señalan que con el decreto de excarcelación no ha solventado la situación de hacinamiento en las cárceles. En ese sentido aseguran que tan solo el 10% de la población reclusa se ha beneficiado del mismo, aumento la grave crisis carcelaria. (Le puede interesar: "[Decreto 546 de 2020 de excarcelación para evitar contagio de covid-19 en cárceles](https://www.justiciaypazcolombia.com/decreto-546-de-2020-de-excarcelacion-para-evitar-contagio-de-covid-19-en-carceles/)")

<!-- /wp:paragraph -->
