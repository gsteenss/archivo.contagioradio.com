Title: Diana Fajardo podría garantizar respeto a la Constitución: Iván Cepeda
Date: 2017-06-02 16:46
Category: Paz, Política
Tags: acuerdo de paz, Ley Estatutarua de JEP
Slug: diana-fajardo-podria-garantizar-respeto-a-la-constitucion-ivan-cepeda
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/06/semana1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Semana] 

###### [02 Jun 2017] 

**Con la llegada de Diana Fajardo a la Corte Constitucional, se espera que haya un equilibrio al interior de las fuerzas de este escenario que garanticen el respeto a la Constitución**, así lo afirmó Iván Cepeda, senador del Polo Democrático.

Durante los debates en donde se analizó la candidatura de cada integrante de la terna para la Corte Constitucional, Cepeda manifestó que Diana Fajardo había señalado “**que el respeto por la Constitución pasa por el cumplimiento estricto de los Acuerdos de Paz** y que la manera de promover una democracia en el país, un estado de derecho y unas condiciones para que se acabe la inequidad social, también pasan por ese cumplimiento”, razón que llevó finalmente a que fuese elegida para ser la nueva integrante de la Corte Constitucional.

El senador señaló que la decisión se tomó en medio de un congreso sumamente polarizado en donde la diferencia fue apenas de 5 votos y en donde las bancadas del Uribismo y Conservadoras presionaron para impedir que Fajardo fuese elegida, a través de campañas y estrategias de desprestigio, como decir que era **“indigna” para hacer parte de este espacio. **Le puede interesar: ["Debe haber una Constitucionalismo par ala paz: C. Gonzáles"](https://archivo.contagioradio.com/debe-haber-un-constitucionalismo-para-la-paz-c-gonzalez/)

Sin embargo, Cepeda señaló que hay que estar pendiente de las próximas decisiones de la Corte Constitucional porque los siguientes **fallos serán sobre los decretos firmados por el presidente Santos que permitirían avances sustanciales en la implementación** de los Acuerdos de Paz. Le puede interesar: ["Elección de magistrado de la Corte Constitucional es clave para implementación de acuerdos de paz"](https://archivo.contagioradio.com/eleccion-de-magistrado-de-la-corte-constitucional-es-clave-para-implementacion-del-acuerdo/)

Al mismo tiempo la Ley Estatutaria de la Jurisdicción de Paz está en trámite en el Congreso y de acuerdo con Cepeda, los debates continuarán siendo tan divididos como ha pasado con los otros proyectos de ley sobre el Acuerdo de paz, “**este será un debate central, se va a desarrollar la Jurisdicción Especial para la Paz** y se sabe que hay sectores políticos que ven con mucha animadversión esta jurisdicción porque se trata de examinar la responsabilidad sobre los crímenes no amnistiarles”.

Frente a la movilización ciudadana Cepeda expresó que en debates anteriores la presión aplicada por la sociedad se ha reflejado en el cumplimiento de las tareas de los congresistas “se han activado algunas de las redes de paz y de los jóvenes que han trabajado en este proceso, y es fundamental su presencia en el debate legislativo”.

<iframe id="audio_19049447" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_19049447_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

######  Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
