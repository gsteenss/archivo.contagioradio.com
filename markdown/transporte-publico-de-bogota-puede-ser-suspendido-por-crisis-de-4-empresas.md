Title: Transporte Público de Bogotá puede ser suspendido por crisis de 4 empresas
Date: 2017-09-29 17:59
Category: DDHH, Nacional
Tags: Sistema Itegrado de Trasnporte, Transmilenio
Slug: transporte-publico-de-bogota-puede-ser-suspendido-por-crisis-de-4-empresas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/09/semana1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Publimetro] 

###### [29 Sept 2017] 

“Desde el 2016 advertimos sobre la crisis y quiebra del Sistema de transporte público en Bogotá” así lo advirtió el concejal Antonio Sanguino, por la Alianza Verde, quien además aseguro, que hay una **latente amenaza de que la ciudad se quede sin transporte público, debido a la quiebra de los operadores de este sistema que deben aproximadamente 3.7 billones de pesos.**

De acuerdo con Sanguino, desde hace un par de años dos operadores **Ecobus y Agrobus**, que agrupaban a los pequeños transportadores, se declararon en quiebra, mientras que recientemente hay 3 empresas de los 9 operadores, que han manifestado su situación de declaratoria de quiebra. Algunos de estos operarios son los mismos que estarían a cargo de los buses rojos de Transmilenio.

### **La inminente crisis** 

Según el concejal, la crisis se debe fundamentalmente a dos situaciones, la primera de ellas es el diseño del sistema y las licitaciones que se otorgaron en la alcaldía de Samuel Moreno, y la segunda, son las equivocaciones y los problemas gerenciales en la implementación del Sistema Integrado, que tienen que ver con **la responsabilidad de diferentes administraciones.**

“8 millones de personas se quedarían si poder movilizarse en Bogotá, es una situación especialmente grave” afirmó Sanguino y agregó que producto de las falencias es que actualmente ningún sistema de transporte se articule con el funcionamiento de otros, es decir que **no exista una relación entre las troncales de Transmilenio y el SITP, las ciclorutas y el transporte privado**.

“No superamos la existencia paralela del transporte público tradicional, hay problemas de información sobre el funcionamiento del SITP, los buses pasan vacíos y que sucede lo que paga el distrito por los buses vacios, generaron un hueco fiscal para la ciudad, que está por encima de los **2 billones de pesos y los operadores tampoco lograron cubrir el resto de la operación del sistema**”. (Le puede interesar:["Se aprueba cupo de endeudamiento para más Transmilenio en Bogotá"](https://archivo.contagioradio.com/se-aprueba-cupo-de-endeudamiento-para-mas-trasmilenio-en-bogota/))

### **El aumento del pasaje de Transmilenio** 

[Una de las estrategias que se usaron para superar esta crisis en la alcaldía de Peñalosa, fue el aumento del costo del pasaje de Transmilenio a \$2.200 pesos, sin embargo, para Sanguino esta decisión fue mucho más lesiva “esa fue una actitud poco responsable porque esto no se resuelve aumentando la tarifa y poniendo a los ciudadanos a pagar el déficit” y agregó que se necesita es hacer de nuevo el sistema integrado de transporte con otras condiciones y características. (Le puede interesar: ["Transmilenio desde el principio se baso en un acuerdo desventajoso"](https://archivo.contagioradio.com/transmilenio-desde-el-principio-se-baso-en-un-acuerdo-desventajoso/))]

### **El pacto del silencio** 

Victor Martínes, vocero de 7 de los 9 concesionarios del SITP, manifestó que las directivas de Transmilenio le habrían hecho firmar un documento de confidencialidad para no dar a conocer la problemática que afronta el sistema, para Sanguino **esto atenta contra el carácter público de la información sobre la administración del distrito**.

“A mí me ha tocado hacer labores de inteligencia y contra inteligencia para obtener la información financiera de las interventorías y supervisores sobre el déficit financiero del Sistema integral de Transporte y esto hay que develarlo”.

<iframe id="audio_21179564" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_21179564_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio]{.s1}](http://bit.ly/1ICYhVU)[.]{.s1}
