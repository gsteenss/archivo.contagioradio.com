Title: La comunidad del periodismo
Date: 2016-02-19 09:32
Category: invitado, Opinion, superandianda
Tags: la comunidad del Anillo, periodismo, Vicky Dávila
Slug: la-comunidad-del-periodismo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/02/la-fm.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: La FM 

#### **[Superandianda](https://archivo.contagioradio.com/superandianda/)- [@superandianda](https://twitter.com/Superandianda)** 

###### 19 Feb 2016 

La indignación que ha causado por estos días en los medios colombianos la llamada Comunidad del Anillo no ha sido precisamente por la red de prostitución y trata de personas al interior de la Policía Nacional, aunque así lo hayan manifestado, sino el hecho de que los llamados hombres machos y valientes que defienden la patria se vean envueltos en una condición adversa al orden que juran tener, además de muchas implicaciones políticas que han desplegado de manera escandalosa los abusos sexuales que involucran importantes cabezas de la institución.

[La situación es tan doble moral como bochornosa, si bien es cierto que cualquier acoso y trata de personas debe ser investigada y denunciada, ocurra en la Policía o en cualquier otra institución y escenario personal,  también es cierto que en Colombia han sucedido crímenes y violaciones a derechos humanos por parte de la fuerza pública que han sido irrelevantes en su momento para  la opinión y el periodismo nacional.]

[Este nuevo escándalo que hace persignar a Colombia y en especial al procurador, pone en tela de juicio la ética profesional con la que los medios colombianos manejan el material que cae en sus manos. La tarea  parece que no es la de  informar ni denunciar sino vender, y sin duda alguna lo que más se vende en Colombia es la doble moral, esa doble moral que nos impide ver más allá de la pantalla del televisor y solo nos permite escuchar opiniones sesgadas llenas de intenciones políticas.  Sin pretender justificar ningún abuso que haya ocurrido en cualquier institución, no creo que sea  la primera y última red de violación que ocurra en las filas del orden, no tendríamos  que ir tan lejos si quisiéramos indignarnos más, los abusos en el sistema militar, el cual considero  denigrante para cualquier persona, ha sido escenario de asesinatos y desapariciones que no han causado indignación pública alguna.]

[La denuncia de violaciones sexuales y proxenetismo, Comunidad del Anillo, es válida en la medida que sea investigada judicialmente, con esto no señalo que no posea valor hacerlo a través de los medios de comunicación, pero hay una línea muy grande que debe separar el periodismo que denuncia y el amarillismo que vende. Existe otra comunidad, debe ser la comunidad del periodismo, pero no del periodismo que prostituye su espacio y su ética al mejor postor para lograr fines políticos, sino el periodismo serio, creíble y que maneja la información objetivamente sin ser servil a la élite y despiadado destruyendo usando el  morbo.]

[Los valientes  del periodismo colombiano denuncian profesionalmente, muchos han terminado sin vida por hacerlo, creo que es una total ofensa comparar una vulgar denuncia de material morboso sin fundamento alguno con trabajos profesionales que en su momento fueron fuerte crítica  para el estado y auténtica verdad revelada para la ciudadanía; recordando a Héctor Abad Gómez.]

[La intención de Vicky Dávila en todo el escándalo que desato no fue la de informar sino la de vender y de la manera más fácil y baja –vender lo que tenía que vender- Lo peor de todo es escuchar opiniones donde admiran su valentía ¿cuál valentía? ¿La misma que tuvo en el gobierno de Álvaro Uribe maquillando la realidad criminal que vivía Colombia?]

[El cuarto poder son los medios de comunicación y en Colombia sí que lo han sabido manejar. Debe  saberse que el poder de dar información además de ser eso –poder- es una responsabilidad. Odio los juicios sobre ética y moral, pero de lo que estoy completamente convencida es que cualquiera que sea nuestra profesión es vocación entregada para quienes lo requieran y siempre quienes nos requieren son los más vulnerables, en ese caso entonces la información periodística de este país vendida a ciertos sectores políticos la hace proxeneta, prostituyendo el derecho del pueblo a ser libremente informados con respeto.]
