Title: De  títulos sin subtítulos
Date: 2016-04-27 07:50
Category: Camilo Alvarez, Opinion
Tags: Bogotá, ESMAD, Peñalosa
Slug: de-titulos-sin-subtitulos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/04/diplomas.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: [myhastensbed]

#### **[Camilo Álvarez Benítez](https://archivo.contagioradio.com/camilo-alvarez-benitez/)- [@camiloalvarezb](https://twitter.com/CamiloAlvarezB)** 

###### 27 Abr 2016

Madre no hay sino una, Doctor es cualquier Hp, parece haberse transformado el dicho; alentado por los debates no diplomáticos sobre diplomas; durante esa cruenta batalla, tomaron fotos de solapas de libros que tal vez nunca leyeron, buscaron hojas de vida en la web, folletos de seminarios, foros y acrósticos. Y es que, No es un tema menor; en Colombia, los títulos son de mucha relevancia, nos lo recuerdan los hinchas del América en la B y Monseñor Ordoñez y La FURIÉ en la costa Atlántica también.

[Desde el Márquez de San Jorge, único al que le alcanzaron los reales para comprar título nobiliario en la colonia, se ha vuelto práctica costumbrista alcanzar los que otros tienen, crecer con la mirada puesta en otros lares e intentar imitarlo, por eso nuestras cabezas viven en lugares distintos a los que caminan nuestros pies y el papel soportado en la pared da más confianza que el trabajo demostrado.  ]

[Pero no es una diatriba contra el estudio, del que al fin y al cabo habrá que resaltar a quien lo logre superar con el sistema educativo que nos circunscribe; es precisamente la separación entre forma y contenido donde las entrañas nos desencuentran; cierto es que hay que cualificar el conocimiento para regir los destinos públicos, como es igual de cierto que nos han robado más dinero los “doctores” que los obreros de a pie. Cierto es que la profesionalización es una manera de medir los logros individuales, como cierto es que la simpleza de los títulos también son una forma de evadir el trabajo colectivo. ¿A cuántos les ha tocado el jefe que si acaso supervisa y firma?]

[¿Sabemos sobre qué investigaciones o actuaciones están soportados los diplomas? una imagen vale más que mil palabras, pero en política una obra vale más que mil cartones, quien impulsó la construcción de más universidades en Brasil no culminó sus estudios superiores y quienes se autodenominan inteligencias superiores en nuestro país nos llevaron al borde del abismo y casi convencieron a los connacionales de dar un paso al frente.]

[Pero la lógica está puesta sobre la base la “cultura de opinión”; y esta, tiene poco tiempo para argumentos y quienes argumentan no tienen tiempo para entenderse con ella; así como los tiempos del capital tampoco dan compás de espera, que lo ratifique el tecnócrata que apela al secuestro alienígena para abducir petróleo o el alcalde que acude al potrero para construir condominios.]

[Hay títulos de títulos dicen esta semana en la feria del libro, que entre el gringo y escoja; al final nuestra atención sigue perdida entre el asombro de cambiarse el apellido de Guatibonza a Springler y no en el detrimento público de la fiscalía, en si Peñalosa estudio menos mientras contrata más con los mismos del relleno fluido, en si los hijos de Uribe son empresarios perseguidos mientras asesinan los testigos del cartel de la chatarra.]

[Así somos, un país en que se le pone más atención a la ortografía de los panfletos que a los paros armados paramilitares, un país donde los estudiantes quedan en coma o mueren en manos del ESMAD, mientras nuestros hijos van a la feria del libro a conocer la bibliotanqueta.]

------------------------------------------------------------------------

###### Las opiniones expresadas en este artículo son de exclusiva responsabilidad del autor y no necesariamente representan la opinión de la Contagio Radio. 
