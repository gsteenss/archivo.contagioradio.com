Title: En 7 meses han muerto dos bebes indígenas desplazadas en Buenaventura
Date: 2015-07-07 16:02
Category: Comunidad, Nacional
Tags: Alcaldía de Buenaventura, asesinatos en buenaventura, buenaventura, Comunidad Wounaan desplazada en Buenaventura, Parramilitarismo
Slug: en-7-meses-han-muerto-dos-bebes-indigenas-desplazadas-en-buenaventura
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/07/comuidad-indígena-buenaventura-contagioradio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: contagioradio.com 

###### [7 Jul 2015] 

Mujer indígena de la comunidad  Wuonam se encuentra en delicado estado de salud luego de haber dado a luz prematuramente a su bebé en el coliseo de Buenaventura y que **falleció dos días después de nacida en la clínica Santa Sofía**. Su padre, también comunero indígena relata que no recibieron ayuda de emergencia por parte de las instituciones de salud.

El estado de salud de la mujer es delicado ante las **graves condiciones de salubridad y problemas nutricionales** que ha presentado la **comunidad Wuonam** dentro del coliseo El Cristal de Buenaventura, luego de siete meses de ser desplazados. Según Enrique Membelache, padre de la bebé fallecida, su esposa ha sufrido de intensa fiebre, diarrea y dolor de cabeza.

**El paseo de la muerte...**

Luego de haber nacido la menor, Enrique y su compañera, acudieron a la ayuda de la Policía de la zona, quien desde el coliseo los llevó al hospital Niza Blanca. Ante la ausencia de una incubadora en el hospital  y la negación de brindar una ambulancia, el padre y la madre se dirigieron en la patrulla de la policía hacia el hospital Santa Sofía, a las 10:00 P.M.

"*No nos atendían mientras no tuviéramos todos los registros de nacida (de la bebé). Llegamos a urgencias; eran más o menos  las 2:00 A.M de la mañana. (...) Yo no tenía los documentos de nacida porque la niña nació en el coliseo; no en el hospital, no teníamos esos documentos (...) Vino la patrulla (de policía) para que nos hicieran atender; porque si no, ellos dijeron, si no era porque el caso era muy delicado, tenían que atender por obligación porque estos eran indígenas desplazados, y ahí nos atendieron*". Cuenta Membelache.

Su hija habría estado un día después en la incubadora, en buen estado de salud. Luego de haber recibido la orden de salida para su compañera, quien presentaba intensa fiebre y dolor de cabeza, **Enrique Membelache** en medio de las facturaciones y papeleos tendría la oportunidad de visitar a su hija desde las 4:00 P.M, hora de visitas. "*Yo pensaba que la niña estaba viva, pero cuando ya me dijeron y entré allá, me dijeron que la niña ya falleció porque tenía mucha fiebre alta*".

Además le negaron los medicamentos que el hospital le formuló a su esposa en el momento de reclamar. Según la clínica Santa Sofía, el carné de salud "Barrios Unidos", no era el correcto. Enrique Membelache hasta ahora no ha conseguido comprar los medicamentos, pues la sucursal de "Barrios unidos" no tiene convenio en el municipio de Buenaventura. **Hasta el momento de la entrevista, el único medicamento suministrado era acetaminofén.**

El **secretario de salud del municipio** no ha hecho presencia directa en el coliseo. La comunidad ha contado únicamente con la presencia de delegados de su oficina, de los cuales se espera una respuesta a la situación actual de los y las indígenas desplazados, y que respondan ante el caso de Membelache y su compañera.
