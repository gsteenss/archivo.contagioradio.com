Title: Impeachment "rompe la base de la democracia": Rousseff
Date: 2016-04-19 16:38
Category: El mundo
Tags: Dilma Roussef impeachment, Golpe de estado Brasil, Juicio político Rousseff
Slug: impeachment-rompe-la-base-de-la-democracia-rousseff
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/04/mvd6708086.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### Foto: El país 

##### [19 Abr 2016]

Mientras se adelanta este martes la conformación de la comisión senatorial encargada de evaluar el pedido de impeachment en su contra, la presidenta brasileña Dilma Rousseff insiste que el proceso que se adelanta se soporta en la “injusticia y fraude” y que no existe un “crimen de responsabilidad” que permita la activación de dicho juicio político.

La mandataria aseguró que tiene la conciencia tranquila, añadiendo que los actos por los que se le acusa “fueron practicados por otros presidentes antes de mí y no fueron categorizados como actos ilegales”, contrario a lo que se afirma desde los grandes medios y por los diputados, que, con argumentos débiles y algunos con investigaciones encima, justificaron su voto a favor del impeachment.

A pesar de su consideración de ilegalidad ", Rousseff ratificó que "no se trata de eliminar ninguno de los elementos que tenemos para defender la democracia, se trata de tener el derecho a la defensa" y que el juicio político "no traerá estabilidad al país porque rompe la democracia y acabaría con el crecimiento económico" de Brasil, séptima economía en el mundo en la actualidad.

Analistas políticos como Jorge Kreynnes, hablan de la falta de coherencia en el discurso de respeto a las instituciones “pero no se respete la voluntad popular” expresada en los 54 millones de votos con los que fue elegida presidenta Rousseff, actitud que responde a lo que denomina un “golpe suave” en el que se desprestigian los líderes populares a partir de la creación de matrices de opinión, centradas en acusaciones por corrupción, que les permita recurrir a “mecanismos institucionales”.

Adicionalmente, son varias las relaciones que indicarían una posible participación de los Estados Unidos, entre las que se incluye el hecho que Lilian Ayalde’s, actual embajadora americana en Brasil, ocupará el mismo cargo diplomático en Asunción durante el golpe parlamentario al entonces presidente Fernando Lugo y el viaje realizado por varios de los diputados a Norte América después de la elección.

Paola Estrada, integrante de Alba movimientos y el Frente Brasil Popular, asegura que la decisión de la Cámara Federal como una "derrota" no solo para el gobierno de Dilma Rousseff, también para el movimiento popular y el "ciclo de avances que tuvimos durante los últimos 10 o 15 años", ante lo cual aseguro que  para defenderlos "tenemos que ir a la calle para dialogar con la gente que no es militante y no esta en las organizaciones".

**Sobre la comisión**

La configuración del comité, que tiene lugar hoy en el Senado Federal, estará compuesta por 42 integrantes, quienes, a partir de una reunión entre los líderes de los partidos políticos, establecerán un calendario de trabajo para evaluar la solicitud de juicio político proferida por la Cámara en su votación del día domingo.
