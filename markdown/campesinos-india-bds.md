Title: 16 millones de campesinos boicotean a Israel en solidaridad con Palestina
Date: 2017-11-02 17:35
Category: Onda Palestina
Tags: Apartheid Israel, BDS, Palestina
Slug: campesinos-india-bds
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/11/bds.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto:ganashakti.com 

###### 1 Nov 2017

La organización más grande de campesinos y trabajadores agrícolas de la India, All India Kisan Sabha (AIKS), anunció que se adhería al movimiento BDS. Esta organización mantiene presencia en 21 de los 29 estados de la India y una afiliación superior a la de 16 millones de personas, lo que la convertiría en la organización más grande en el mundo que apoya el BDS.

Al adherirse la AIKS se comprometa a desarrollar Boicots e impulsar Desinversiones y Sanciones contra Israel hast que cumpla la ley internacional. Así mismo se compromete a resistir al avance de las compañías israelíes dentro del sector agrícola indio. Para esto se proponen documentar y denunciar los casos donde se vean involucradas empresas israelíes en el sector agrícola, e impulsar dentro de los campesinos indios la preocupación que las ganancias de las empresas Israelíes están financiando la ocupación militar y el apartheid en Palestina.

Apoorva, enlace del comité nacional palestino del BDS para el sudeste asiático, ha dicho lo siguiente: “La solidaridad india con el pueblo palestino no es nueva y tiene una larga historia. Estamos muy contentos de ver que re emerge esta solidaridad con la declaración de AIKS en apoyo al movimiento BDS y al pueblo palestino. Los movimientos populares en la India, en Palestina y en todo el mundo están trabajando para derrotar la oleada de políticos de derecha que están extendiéndose en el mundo hoy día. Al unirse al movimiento BDS AIKS le está diciendo no a las odiosas políticas del primer ministro Modi, Netanyahu y Trump, y se une a quienes queremos construir un mundo más libre, justo y equitativo”. Justamente este año en Julio el primer ministro de la India visitó a Israel donde ambos países firmaron convenios en el área de la agricultura, el agua y la tecnología espacial.

All India Kisan Sabha (AIKS) nació en 1936, antes que India se independizara del colonialismo británico. Ellos fueron parte fundamental en la movilización de los campesinos y trabajadores agrícolas que lograron ponerle fin al imperialismo británico en esas tierras. Más recientemente AIKS ha resistido activamente a el incremento de las corporaciones dentro de la agricultura india y a las dos décadas de políticas neoliberales que han llevado a un masivo endeudamiento y suicidio de campesinos.

En esta emisión de Onda Palestina además pueden sobre noticias de bombardeos a túneles clandestinos que han dejado a ocho personas muertas en Gaza, así como el Operativo en Cisjordania que va en 50 capturas ilegales; una entrevista con Daniel Devita, un rapero que lucha contra el poder y le canta al pueblo palestino en castellano; victorias del movimiento BDS en el Japón; una entrega de poesía palestina; y algunos puntos sobre la historia y actualidad de la lucha palestina por la liberación sexual, partiendo de su artículo del mismo título.

<iframe id="audio_21822482" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_21822482_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Escuche esta y más información sobre la resistencia del pueblo palestino en [Onda Palestina](https://archivo.contagioradio.com/programas/onda-palestina/), todos los Martes de 7 a 8 de la noche en Contagio Radio. 
