Title: Así se perdieron los votos de la Unión Patriótica
Date: 2015-11-03 12:59
Category: Entrevistas, Política
Tags: 25 de octubre, Aida Avella, Carlos Ariel Sánchez, Comisión de escrutinios, Concejo de Bogotá, Concejo Nacional Electoral, elecciones regionales, Jaime Hernando Suárez, Registraduría Nacional, Unión Patriótica
Slug: union-patriotica-ha-recuperado-mas-de-4-mil-votos-y-aspira-a-curul-en-concejo-de-bogota
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/11/aida1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: La Silla Vacía 

<iframe src="http://www.ivoox.com/player_ek_9258487_2_1.html?data=mpeimpmce46ZmKiak5eJd6KokpKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRmc%2Fdhqigh6eXsozEwtnfy4qnd4a2lNnWxcaPp9Di1c7bh6iXaaO1wpDS2s7LrcbixdSYx9GPtsbX0NPhx9SPqMaf15KSmaiRh9Di1cbUy9SPlsLYytSYj4qbh46o&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Aida Avella, Unión Patriótica] 

###### 3 Nov 2015 

Con el fin de alcanzar una curul en el Concejo de Bogotá y tras denunciarse las diversas fallas en el diseño de las elecciones regionales de este año, **la Unión Patriótica continúa exigiendo el reconteo de votos con lo que ha logrado recuperar más de cuatro mil** sin que ese procedimiento se haya realizado en todas las localidades de la capital.

**"Es un sistema caduco con muchas fallas, no hemos podido hablar con el registrador nacional, ni distrital que son los causantes del atropello** contras las listas cerradas al Concejo de Bogotá”, afirma Aida Avella, quien añade que entre esas fallas, la más grave fue que hubo un **“pésimo diseño” de los formularios** y además las cifras entre los votos depositados y los reportados son muy distintas, ya que se anularon miles de votos para la Unión Patriótica.

Frente a esas “fallas electorales”, el pasado jueves la UP interpuso ante la Fiscalía una acción de tutela  responsabilizando a la Comisión de Escrutinios, al registrador distrital, Jaime Hernando Suárez y al registrador nacional, Carlos Ariel Sánchez. **“Esto no se hace con un partido que ha sido sometido de un genocidio,** y que intenta participar en una supuesta democracia donde siempre ganan los mismos (…) con mucho esfuerzo logramos votos con dignidad para que nos los anulen de una manera tan fresca”.

Para Avella, "el **sistema electoral colombiano empieza a ser fuertemente cuestionado por estar en la edad de piedra**, estas cosas tienen que revelarse pero sobre todo corregirse (…) no estamos pidiendo nada distinto a que se cumpla la Ley y se cuenten nuestros votos, no estamos pidiendo que nos regalen absolutamente nada”, concluye la dirigente de la UP.
