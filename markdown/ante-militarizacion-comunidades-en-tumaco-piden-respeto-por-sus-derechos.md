Title: Ante militarización, comunidades en Tumaco piden respeto por sus derechos
Date: 2018-04-18 12:04
Category: DDHH, Paz
Tags: alias guacho, comunidades tumaco, Fuerza Pública, Militarización, Tumaco
Slug: ante-militarizacion-comunidades-en-tumaco-piden-respeto-por-sus-derechos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/10/masacre-tumaco.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Luis Alfonso Mena Sepúlveda] 

###### [18 Abr 2018] 

Tras el desplazamiento de las Fuerzas Militares a Tumaco Nariño por orden de Juan Manuel Santos, las comunidades que habitan allí **lanzaron una alerta sobre la situación de inseguridad** y zozobra en la que se encuentran estas personas. Le pidieron a los militares que respeten los derechos de los habitantes y que eviten realizar señalamientos que puedan poner en riesgo su vida.

De acuerdo con Diana Montilla, integrante de la Asociación de Juntas de Acción Comunal de los Ríos Nulpe y Mataje, ASOMINUMA, “es obvio que, en un territorio tan complejo como Alta Mira y Frontera, las comunidades **se encuentran preocupadas** y en alerta frente a la fuerte militarización que se ha venido presentando”.

### **Comunidades de Tumaco piden que se respeten sus derechos humanos** 

Afirmó que la comunidad ha sido quien ha tenido que vivir los efectos del conflicto armado que han conllevado a la violación de los derechos humanos. Por esto, han enviado una alerta a las organizaciones sociales, pero también a las autoridades y la Fuerza Pública “para que estén pendiente **ante cualquier hecho de violación** a los derechos humanos”. (Le puede interesar:["Vicepresidente Naranjo le incumplió a las víctimas de la masacre en el Tandil, Tumaco"](https://archivo.contagioradio.com/masacre_tumaco_impunidad_naranjo/))

Recordó que, debido a la búsqueda de **alias "Guacho" ** en esa parte del territorio nacional, “la situación en Tumaco ha estado tensa”. Sin embargo, dijo que los enfrentamientos que se han presentado “han dejando en medio a la población civil y la semana pasada 150 personas se ubicaron en un sector conocido como la "Y" para escapar y protegerse de los enfrentamientos”.

Ante esta situación, le han solicitado al Ejército que se respeten los derechos de las comunidades campesinas y que “en lo posible se evite generar perjuicios a las comunidades” teniendo en cuenta que la situación **se puede salir de control** cuando se presentan los enfrentamientos con los grupos armados ilegales.

###### Reciba toda la información de Contagio Radio en [[su correo]
