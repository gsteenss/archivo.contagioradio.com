Title: "Llueva o truene, el paro se mantiene"
Date: 2018-11-29 13:44
Author: AdminContagio
Category: Educación, Movilización
Tags: educacion, ENEES, estudiantes, marcha, UNEES
Slug: paro-se-mantiene
Status: published

###### [Foto: Contagio Radio] 

###### [29 Nov 2018] 

Luego de la jornada de movilización realizada el miércoles 28 de noviembre, **el balance de derechos humanos es positivo,** pese a que se registraron casos de agresiones y capturas irregulares por parte de la policía contra los estudiantes. Sin embargo, se han emitido alertas tempranas sobre hechos que involucran a la policía con la seguridad de los jóvenes que se trasladan a Bogotá para el **Encuentro Nacional de Estudiantes por la Educación Superior (ENEES)**.

Según el **delegado de[ derechos humanos] de la Unión Nacional de Estudiantes por la Educación Superior (UNEES) Cristian Guzmán**, dar un parte positivo es posible gracias al trabajo de las organizaciones sociales. Guzmán resaltó la instalación del **Puesto de Mando Unificado Popular (PMUP)**, así como el acompañamiento de diferentes sectores sociales como la guardia indígena, las Unidades Técnicas Legislativas del Congreso y la Organización de las Naciones Unidas.

A pesar de dichos acompañamientos, el integrante de la UNEES sostuvo que **sí se presentaron intentos de sabotaje contra la movilización mediante la infiltración de cuerpos de seguridad** en las marchas, que tenían como objetivo crear focos de violencia. Adicionalmente, en el Boletín 6 del PMUP, se dio aviso sobre 13 personas heridas y 12 detenidas por acción de la fuerza pública durante las movilizaciones.

[![Captura de pantalla 2018-11-29 a la(s) 11.18.47 a.m.](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/11/Captura-de-pantalla-2018-11-29-a-las-11.18.47-a.m.-800x388.png){.alignnone .size-medium .wp-image-58769 width="800" height="388"}](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/11/Captura-de-pantalla-2018-11-29-a-las-11.18.47-a.m..png)

> Me envían. Hay 10 personas vestidas de civil que están formando con la Policía junto al portal de la 80.
>
> Srs. [@PoliciaColombia](https://twitter.com/PoliciaColombia?ref_src=twsrc%5Etfw), ¿van a infiltrar la movilización para violentarla? [pic.twitter.com/CAA3WOd1Zj](https://t.co/CAA3WOd1Zj)
>
> — Ángela María Robledo (@angelamrobledo) [28 de noviembre de 2018](https://twitter.com/angelamrobledo/status/1067842606028210176?ref_src=twsrc%5Etfw)

<p>
<script src="https://platform.twitter.com/widgets.js" async charset="utf-8"></script>
</p>
### **“No sabemos si tenemos cuerpos de seguridad o de hostigamiento”** 

Pese al buen desarrollo de la jornada de movilización, Guzmán sostuvo que hay alertas graves sobre empadronamientos y ataques contra estudiantes que se desplazan hacía Bogotá, para participar en el **ENEES **de emergencia. (Le puede interesar: ["Nuestra estrategia será movilizarnos con alegría"](https://archivo.contagioradio.com/movilizarnos-unees/))

El joven denunció que 2 de los 4 buses en los que se desplazaban estudiantes de la regional Antioquia fueron atacados con piedras, situación que se presentó cerca al municipio de Marinilla, y de la que **resultaron 2 personas heridas**. Adicionalmente, quienes venían en los buses dijeron que había motos siguiéndolos; y cuando tuvieron el acompañamiento de la policía, **fueron agredidos verbalmente por los oficiales, y fueron fotografiados, al igual que sus documentos de identidad**.

Por estas situaciones, Guzmán manifestó que “no sabemos si tenemos cuerpos de seguridad o de hostigamiento”, y aseguró que la UNEES pidió a la policía que acompañe y garantice el tránsito de los estudiantes que se desplazan para llegar al ENEES, en Bogotá. (Le puede interesar: ["Las 3 condiciones de los estudiantes para levantar el cese nacional"](https://archivo.contagioradio.com/condiciones-levantar-paro-nacional/))

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/post.php?href=https%3A%2F%2Fwww.facebook.com%2FUNEES.COL%2Fposts%2F257173084955288&amp;width=500" width="500" height="738" frameborder="0" scrolling="no"></iframe>

### **Se espera la participación de 2.300 estudiantes en el ENEES** 

El de este fin de semana será el segundo ENEES de emergencia que realizarán los estudiantes, y según información de Guzmán, desde las 5:30 de la mañana de este jueves, han llegado algunas de las 51 Instituciones de Educación Superior (IES) públicas, que se espera, participen del encuentro. (Le puede interesar: ["Caravanas de estudiantes avanzan hacia gran movilización por la educación"](https://archivo.contagioradio.com/caravanas-de-estudiantes-avanzan-hacia-gran-movilizacion-por-la-educacion/))

En dicho evento, la UNEES hará un balance de la organización, sobre las formas de organización y su funcionamiento; posteriormente trabajarán sobre las propuestas del pliego nacional, así como de los pliegos locales y regionales; y finalmente, **discutirán sobre el posicionamiento del paro nacional, y su organización en términos de los objetivos políticos que trace el encuentro**.

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
