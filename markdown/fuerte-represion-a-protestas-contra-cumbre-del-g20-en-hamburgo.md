Title: Fuerte represión a protestas contra cumbre del G20 en Hamburgo
Date: 2017-07-07 16:17
Category: El mundo, Movilización
Tags: G20, Hamburgo, represión contra manifestantes, reunión G20
Slug: fuerte-represion-a-protestas-contra-cumbre-del-g20-en-hamburgo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/07/hamburgo-e1499459964801.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Noticiero Televisa] 

###### [07 Jul 2017] 

La represión contra los manifestantes en Hamburgo, quienes protestan contra las discusiones que se llevan a cabo en el marco de la reunión de los países que conforman el G20, se ha vuelto más intensa. **Hasta el momento 45 manifestantes han sido arrestados** y miles han sido reprimidos con tanques de agua.

La parlamentaria alemana Heikel Hansel aseguró que “**las personas que están manifestando están en desacuerdo con las políticas que establecen el orden económico** mundial y han llevado a mucha pobreza, guerras y explotación de recursos desmedida”. Por esto, miles de personas han rechazado la naturaleza de esta reunión y piden que se discutan temas como el cambio climático, la pobreza y las migraciones.

**Las represiones a  los manifestantes por parte de la policía se han vuelto cada vez más violentas** y según la parlamentaria “los manifestantes están protestando de forma pacífica y los choques con la policía han sido muy fuertes”. Según ella, los arrestos han sido arbitrarios y la policía ha utilizado tanquetas para rociar a la gente con agua y con gases lacrimógenos. (Le puede interesar: ["Trump tendrá que acatar norma que pone límites a la industria petrolera"](https://archivo.contagioradio.com/trump_limites_industria_petrolera/))

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/video.php?href=https%3A%2F%2Fwww.facebook.com%2Fheikehaenselmdb%2Fvideos%2F1521274681270333%2F&amp;show_text=0&amp;width=560" width="560" height="315" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Video Haike Hansel 

De igual forma, Hansel enfatizó en que **“al interior del G20 no hay un interés por debatir temas que están perjudicando a todas las naciones** porque sólo están discutiendo políticas neoliberales de globalización”. Además, ella afirmó que los acercamientos entre los gobiernos para resolver problemáticas asociadas con la crisis en Siria, por ejemplo, son muy pocos y no son temas que estén en la agenda de la reunión. (Le puede interesar:["La represión y el terror armas de control social de gobiernos latinoamericanos"](https://archivo.contagioradio.com/la-represion-y-el-terror-armas-de-control-social-de-gobiernos-latinoamericanos/))

Finalmente Hansel manifestó que “**la sociedad debe unirse para crear diálogos entre los países del norte y el sur** y crear así intercambios en materia de conocimiento y desarrollo para superar las crisis que hay en el mundo”. La cumbre del G20 no es un espacio para discutir aspectos como los tratados de libre comercio que, para Hansel, “le hacen mucho daño a las economías de los países del sur porque no permiten su desarrollo”.

<iframe id="audio_19684080" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_19684080_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU).

 
