Title: “Pasamos de invertir en ladrillo a invertir en la gente” Clara López
Date: 2015-01-29 19:42
Author: CtgAdm
Category: Entrevistas, Política
Tags: Alcaldía, Bogotá, Polo Democrático
Slug: pasamos-de-invertir-en-ladrillo-a-invertir-en-la-gente-clara-lopez-2
Status: published

###### Foto: polodemocratico 

##### [Clara López]:<iframe src="http://www.ivoox.com/player_ek_4015642_2_1.html?data=lZWel5uYdo6ZmKiakp6Jd6KplpKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRic%2Fo08rjy9jYpYzVjKjZw9fFb62ZpJiSpJjUqdufsMffx8yJh5SZo5jbh5enb8TVz8nWxsbYpYzVjNHOjabQp8Khhpywj6jTstXVyM7cjbfFqMrjjJKSmaiReA%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe> 

Respecto a la propuesta lanzada por David Luna del Partido Liberal, la candidata a la Alcaldía de Bogotá por el Polo Democrático Alternativo, asegura que **cada partido es libre de hacer las alianzas que más le convengan**, pero que está segura de que **la gente está cansada de la pugnacidad y el señalamiento y lo que quiere es escuchar propuestas de ciudad.**

Asegura que durante las alcaldías del **Polo Democrático** y los Progresistas se le ha cambiado el destino al recurso público, “**Dimos un giro en el gasto público, pasamos de invertir en ladrillo a invertir en la gente**”. López también resaltó que hace dos años se graduó la primera promoción de **estudiantes de secundaria gratuitos**, y afirma que ese tipo de políticas son las que se deben seguir fortaleciendo para seguir avanzando.

Por último señala que uno de los lunares en la Bogotá actual, es el de inversión en infraestructura de la ciudad y afirma que el tema de “**los huecos, cuestan los mismo que un metro**” por eso es necesario escuchar propuestas incluyentes y diversas para buscar las soluciones entre todos y todas.
