Title: En Bello pretenden destruir biblioteca para construir un supermercado
Date: 2015-09-12 11:18
Category: Movilización, Nacional
Tags: Antioquia, Bello, Biblioteca comunitaria Niquia, Desalojo en Bello, Desalojos, ESMAD, Sacerdote Jose Mario Acosta
Slug: en-bello-pretenden-destruir-biblioteca-para-construir-un-supermercado
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/Bello-3.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Biblioteca Comunitaria Niquia ] 

###### [11 Sept 2015] 

[En horas de la mañana un grupo de 50 personas entre jóvenes y adultos mayores, fueron hostigados por unidades del ESMAD, que pretendían **desalojarlos de la biblioteca comunitaria** Niquia en Bello, Antioquia, **para iniciar la construcción de un supermercado**.]

[Miguel Ángel Romero, defensor de la biblioteca, asegura que es el sacerdote Jorge Mario Acosta quien lidera la iniciativa de demoler este centro cultural, para construir un supermercado, alegando que los predios pertenecen a la parroquia y desconociendo que **el párroco de hace 41 años apoyó la iniciativa de la comunidad y cedió los lotes para construir con dineros recolectados y libros donados esta biblioteca**.]

[[![Bello (1)](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/Bello-1.jpg)

[**El argumento del sacerdote Acosta es que allí funciona un centro de formación de izquierda que debe ser desalojado** para en su lugar instalar un supermercado que contribuya a la dinámica comercial del municipio en la que, según Miguel Ángel, el sacerdote juega un papel central. "Sí se demuele la biblioteca se afectarían los jóvenes y adultos mayores que asisten a talleres de pintura, lectura y teatro que allí se llevan a cabo", agrega Miguel Ángel. ]

[![Bello (2)](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/Bello-2.jpg){.aligncenter .size-full .wp-image-13813 width="960" height="720"}](https://archivo.contagioradio.com/en-bello-pretenden-destruir-biblioteca-para-construir-un-supermercado/bello-2-2/)

[Se espera que las autoridades municipales y eclesiales se pronuncien al respecto, pues aún cuando fue hecha la denuncia formal de desalojo, **ninguna autoridad ha dado la cara**. ]
