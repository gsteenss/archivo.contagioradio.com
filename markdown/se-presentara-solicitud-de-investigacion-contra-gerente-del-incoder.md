Title: En el Senado solicitan investigación contra gerente de INCODER
Date: 2015-06-04 17:15
Category: Judicial, Otra Mirada, Política
Tags: Ariel Borbón, audiencia pública, Fiscalía, INCODER, Iván Cepeda, Ley de Restitución de Tierras, Polo Democrático Alternativo, Restitución de tierras
Slug: se-presentara-solicitud-de-investigacion-contra-gerente-del-incoder
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/06/image-2.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto:[diarioadn.co]

<iframe src="http://www.ivoox.com/player_ek_4596789_2_1.html?data=lZqmmJycfY6ZmKiakpWJd6KkkZKSmaiRdI6ZmKiakpKJe6ShkZKSmaiRl8af0dfS1crSuMLmhqigh6aVb9Tjzc7Qy9nZqIzYxpDW0NvJt9XdyMbQy4qnd4a2lNOYxdTSuNPVjMzS1MrScYarpJKw0dPYpcjd0JC%2Fw8nNs4yhhpywj5k%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Iván Cepeda, senador Polo Democrático Alternativo] 

Este viernes 5 de junio, **se realizará la audiencia pública: análisis del cumplimiento de las obligaciones del INCODER frente al acceso y recuperación de la tierra,** con el objetivo de estudiar los resultados en materia de acceso a tierras de los campesinos reclamantes.

Desde las 8 de la mañana, comunidades y autoridades gubernamentales, entre ellos el gerente del **INCODER, Ariel Borbó**n, estarán presentes en esta audiencia que se llevará a cabo en el Congreso de la República en el salón Luis Guillermo Vélez.

El senador del Polo Democrático, Iván Cepeda, es una de las personas que convoca a esta audiencia, quien viene siguiendo la política de restitución de tierras del gobierno desde hace 5 años. Él afirma que desde su equipo se ha advertido sobre lo **“tímida y débil” que es la Ley de Restitución de Tierras**, que presenta medidas muy frágiles para devolver las tierras a las víctimas de la violencia.

Así mismo, asegura que tras la administración de **Borbón, se ha tomado decisiones que tienden a disfrazar una supuesta entrega de tierras a los campesinos** con nuevos procesos de legalización para entregarlas a quienes las han despojado, como terratenientes y empresarios.

Es por eso que a partir de esa audiencia se analizará **“cuál es realmente el fondo de la política en materia de tierras de este gobierno”**, dice Cepeda, quien agrega que se ha promovido y favorecido la política extractivista y los Tratados de Libre Comercio.

Mañana **se presentará una solicitud de investigación a la Fiscalía General contra el gerente general del INCODER,** invitado a la audiencia, quien debe asumir responsabilidades ante la autoridades, según declaraciones del senador del Polo Democrático.
