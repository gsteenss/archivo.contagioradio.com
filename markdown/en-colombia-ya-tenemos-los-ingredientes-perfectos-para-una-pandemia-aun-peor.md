Title: Medidas de gobierno en Colombia agravarán los efectos del COVID 19
Date: 2020-05-06 20:06
Author: CtgAdm
Category: Actualidad, Entrevistas
Tags: Aislamiento, colombia
Slug: en-colombia-ya-tenemos-los-ingredientes-perfectos-para-una-pandemia-aun-peor
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/04/coronavirus-Chile-América-Latina-desigualdad-inequidad-covid-19-Bolivia-min.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

En Colombia en las últimas horas el Gobierno de Iván Duque anunció una prórroga de aislamiento obligatorio hasta el 25 de mayo, a pesar de ello dió luz verde a otros 11 sectores para retomar sus actividades económicas, decisión altamente cuestionada.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Algunos expertos y expertas tanto de economía como de salud, afirman que no es posible poner a más de la mitad de la población en las calles sin tener certeza del número contagiados en el país, su ubicación y sin tener aún las medidas de protección en los hospitales y clínicas de toda Colombia.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Ante ello en conversación en **[Otra Mirada](https://bit.ly/2SJpxQ6)**, **Jaime Eduardo Ordóñez** médico epidemiólogo, con maestría en economía de la salud, señaló que *"la suma de pocas pruebas, un subregistro de personas infectadas asintomáticas, y **el un aumento de personas en la calle da como resultado los ingredientes de la tormenta perfecta para una pandemia más intensificada en Colombia**".*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Al mismo tiempo añade que, justo este 5 de mayo cuando se anuncia el mayor pico de casos de contagio en el país, el Gobierno decide permitir la salida de mas de la mitad de la población **"yo no veo la lógica en esto y especialmente cuando esta comprobado que el Covid-19 genera bastantes casos de personas asintomáticas, lo que presenta un mayor riesgo a la población mas vulnerable"**.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Ordoñez señala también que es entendible que la economía tenga afán de activarse, pero como reconoce que estas medidas priman las exigencias de las grandes empresas, *"lo que puede pasar es que poco tiempo se genere un bloqueo en hospitales, y a su vez una cuarentena total de nuevo que alargue más la reincorporación de la industria".*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Y afirma que por ello era vital respetar las acciones adoptadas inicialmente, dando atención a los sectores mas vulnerables económicamente, así como aquellos que proporcionan víveres a todo el país, *"los sectores que fueron aprobados no se relacionan con las realidades; por ejemplo, la construcción de edificios apartamentos y casas, en este momento* ***nadie esta pensando en comprar finca raíz, no solamente porque la economía está quieta sino porque hay una incertidumbre y desconfianza de los datos y cifras que se están presentando".***

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Una responsabilidad descargada en la población

<!-- /wp:heading -->

<!-- wp:paragraph -->

"El número de pruebas no ha aumentado, pero se anunció la compra de más de 2.300 ventiladores para las Unidades de Cuidados Intensivos, la pregunta es, **¿cuántas pruebas de Covid-19 se hubieran podido comprar con el costo de un solo ventilador, con el cual sólo puede atender un paciente a la vez durante 2 o 3 semanas?**.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

A su vez analizó que se puede pensar que " los gestos de Gobiernos son un acto de buena fe, **lo que ocurre es que la buena fe no sirve en la salud** se requiere de cifras concretas dadas por las pruebas para realmente mitigar el riesgo".

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Asimismo explicó que las pandemias tienen cuatro fases, que inician con la preparación, luego la mitigación, contención y finaliza con la supresión, *"ahora estamos en la fase de contención, pero de esas cuatro fases la más importante es la primera porque da como resultado el plan de acción, en Colombia esto se omitió".*

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### ¿El riesgo de esta pandemia es que dejemos de hablar de ella y la normalicemos como un problema más de Colombia"

<!-- /wp:heading -->

<!-- wp:paragraph -->

Ante ello el doctor afirmó, que las pandemias a diferencia de las situaciones sociales no responden a una lógica económica, *"para sostener una guerra o un conflicto se necesita una operación económica, por tanto llegará un punto en el que **si no hay financiación puede que se detenga el conflicto, mientras que la pandemia no hay forma de tener la cantidad de muertos"***

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Y agrega que, *"un virus no responde a una lógica social, económica y política un virus ataca a todas las personas de manera desmedida y sólo hay una forma de detenerlo y es disminuyendo el contacto social, y tomando medidas de protección y cuidado a la población".* (Le puede interesar: <https://archivo.contagioradio.com/pandemias-y-guerras-diferencias-y-similitudes/>)

<!-- /wp:paragraph -->
