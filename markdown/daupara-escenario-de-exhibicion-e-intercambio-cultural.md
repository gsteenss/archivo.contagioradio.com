Title: Daupará, muestra de cine y vídeo Indígena
Date: 2015-11-17 15:27
Category: Cultura, eventos
Tags: Bakatá, La séptima versión Daupará, Muestra de cine y vídeo Indígena Daupará
Slug: daupara-escenario-de-exhibicion-e-intercambio-cultural
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/11/cine-indígena-600x407.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto:[ukhamawa.wordpress.org] 

###### [17 nov 2015 ]

Fue presentada en la ciudad de Bogotá la Séptima versión de la Muestra de Cine y Vídeo Indígena en Colombia Daupará, que se realizará en la ciudad de Bogotá (Bakatá) del 24 al 28 de noviembre próximos.

La Maloka del Jardín Botánico "José Celestino Mutis", fue el lugar escogido para lanzar el evento que contará con la participación de grupos indígenas, sectores rurales y comunitarios del país y comunidades indígenas internacionales provenientes de Ecuador, Venezuela y Chile.

<iframe src="https://www.youtube.com/embed/SIM0LoLf0Kg" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

El objetivo de Daupará 2015, esta enfocado en la memoria y apropiación del territorio indígena, propiciando el fortalecimiento de la memoria ancestral Muisca Bakatá **“mediante los trabajos  audiovisuales realizados por los indígenas que principalmente tienen que ver con la imagen, la historia  de los pueblos indígenas en Colombia”**  según afirma Felipe Martínez colaborador del evento.

Los participantes, seleccionados por convocatoria realizada a lo largo del presente, pertenecen a colectivos indígenas colombianos y otros como las CORPANP Corporación de Mujeres Audiovisuales de Ecuador,  ganador del festival Kikinyari de Ecuador y la muestra indígena de Venezuela.

<iframe src="https://www.youtube.com/embed/znAliQS5i3o" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>
