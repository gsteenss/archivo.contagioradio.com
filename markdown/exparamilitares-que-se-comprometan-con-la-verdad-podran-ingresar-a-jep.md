Title: Exparamilitares que se comprometan con  la verdad podrán ingresar a JEP
Date: 2017-12-28 10:30
Category: Nacional, Paz
Tags: JEP, Ley de justicia y paz, paramilitares
Slug: exparamilitares-que-se-comprometan-con-la-verdad-podran-ingresar-a-jep
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/02/Paramilitarismo1-e1459970325314.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo Partícular] 

###### [28 Dic 2017] 

Este 26 de diciembre se aprobó el decreto 2199, que permite que desmovilizados, que participaron en la ley de Justicia y Paz de 2005, puedan ingresar en el proceso de reincorporación, a través de la JEP, **si se comprometen a contribuir con la verdad y reparacióna las víctimas**, esto de acuerdo con el gobierno, para afianzar la construcción de paz en el país y garantizar acciones que hagan que esta sea “irreversible y duradera”.

Este decreto buscará que los desmovilizados también sean beneficiarios de la JEP, en ese sentido tendrán que contribuir a la verdad, justicia y reparación en las diferentes instancias en donde sean llamados para esclarecer los hechos de guerra y violencia, durante el conflicto armado, **para acceder a los beneficios económicos, como subsidios económicos que permitan suplir las necesidades básicas de los mismos**.

En esa medida en decreto señala que la Agencia para la Reincorporación y la Normalización será la encargada de revisar los procedimientos que permitan el acceso de estos excombatientes al proceso de reincorporación. (Le puede interesar: ["Furerzas Armadas abrirán sus archivos a la Comisión de la Verdad"](https://archivo.contagioradio.com/fuerzas-armadas-abriran-sus-archivos-a-la-comision-de-la-verdad/))

### **La puerta abierta de la ley de Justicia y paz** 

La posibilidad que se abre de que, desmovilizados del proceso de paz con los paramilitares en el año 2005, puedan hacer parte de la JEP, surge del artículo 63 de la ley de Justicia y Paz de 2005 **que establece que las personas que hayan sido investigadas y procesadas bajo esta ley podrán aplicar a cualquier otro régimen normativo que resulte favorable**.

De esta manera quienes tengan penas por 8 años, en la Ley de Justicia y Paz, tendrán acceso a otro tipo de castigos participando en la JEP, desde que estén en el Tribunal de Paz, y modificar su condena actual. (Le puede interesar:["Organizaciones sociales respaldan la Comisión de la Verdad"](https://archivo.contagioradio.com/comunidades-respaldan-y-apoyan-a-la-comision-de-la-verdad/))

Finalmente, el decreto indica que la intencionalidad es que las personas que fueron actores del conflicto armado “asuma conciencia de los hechos perpetrados (...) y por tanto, los beneficios económicos se constituyen en un medio que permite su comparecencia a los mecanismos judiciales y extrajudiciales de contribución a la verdad”.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
