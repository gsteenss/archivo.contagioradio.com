Title: Elecciones a rector de la Universidad Nacional sin garantías para la democracia universitaria
Date: 2018-02-20 16:04
Category: Educación, Nacional
Tags: Movimiento estudiantil, rector, Universidad Nacional
Slug: elecciones-rector-universidad-nacional
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/02/jorge-fernandez.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Jorge Fernández ] 

###### [20 Feb 2018] 

El próximo 22 de marzo se realizará la elección del nuevo rector de la Universidad Nacional, Sin embargo, para esta ocasión, el movimiento estudiantil ni las organizaciones sindicales de esta institución tienen a un representante en las elecciones, debido a que finalmente la decisión es tomada bajo los intereses del Consejo Superior, que es el que decide, **sin que se realice la votación con los estudiantes, el cuerpo docente y administrativo**.

El mecanismo de elección de rector de la Universidad Nacional se divide en dos momentos, el primero es el que se realizará el 15 de marzo, cuando la comunidad académica vota a los candidatos, de allí el Consejo Superior elije 5 candidato y posteriormente escoge al rector.

Para Andrés Salazar, representante de los estudiantes ante el Consejo Superior, este modelo de elección **beta la posibilidad de que sea tomado en cuenta el candidato que representa los intereses de la comunidad estudiantil**.

 Ejemplo de ello serían las dos elecciones anteriores con los profesores **Leopoldo Munera, que obtuvo alrededor de 5 mil votos, y Mario Hernández, que tuvo una votación histórica de más de 10 mil votos**, y en ambos casos pese a tener votaciones mayoritarias no fueron tomados en cuenta por el Consejo Superior.

“Se llegó a la conclusión de que si bien el gobierno nacional no elige a dedo quién es el candidato, si beta a algunas de las opciones y cuando se elige a un candidato de los estudiantes y profesores sindicalizados, se le beta” afirmó Salazar.

### **Las opciones de la comunidad académica** 

En vista de este panorama, en una reunión en la que participaron organizaciones estudiantiles y de docentes, se propuso apoyar la idea del 15 de marzo votar en blanco a modo de rechazo del sistema de elección de rector y exigiéndole al CSU que avance en una reforma que transforme el mecanismo de escogencia y que permita un ejercicio real de la democracia universitaria.

Entra de las opciones de que plantea Salazar, es que se logren establecer acuerdos con los candidatos en temas muy puntuales de la universidad, para lograr avances. (Le puede interesar:["Encuentro Nacional de estudiantes por la Educación Superior se realizará en marzo"](https://archivo.contagioradio.com/el-encuentro-nacional-de-estudiantes-por-la-educacion-superior-se-realizara-en-marzo/))

### **Los candidatos a la rectoría de la Universidad Nacional** 

Actualmente a este cargo aspiran 9 personas: el primero es el ex viceministro de Carlos Alberto Agudelo Calderon, luego está el actual vicerrector académico de la Universidad, Juan Manuel Tejeiro, también se encuentra Jorge Hernán Cardenas, economista y hermano del actual Ministro de Hacienda, Mauricio Cárdenas; Edna Bonilla, directora de extensión de la institución; Fred Gustavo Manrique, profesor de la Facultad de Enfermería de la Universidad; Fabian Zanabría, docente de la Facultad de Sociología, que ya había aspirado a este cargo en el 2015.

En la lista igualmente esta Jorge Bula Escobar, profesor de la Facultad de Ciencias Económicas; Dolly Montoya, docente del Instituto de Biotecnología de la insitución y Jhon William Branch, designado como vicerrector de la universidad Nacional sede Medellín. (Le puede interesar: "[¿Adios a las chazas en la Universidad Nacional?](https://archivo.contagioradio.com/adios-a-las-chazas-en-la-universidad-nacional/)"

<iframe id="audio_23953187" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_23953187_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
