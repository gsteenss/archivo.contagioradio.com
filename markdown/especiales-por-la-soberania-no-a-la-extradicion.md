Title: Especiales: Por la soberanía, No a la extradición
Date: 2015-08-13 10:19
Category: Hablemos alguito
Tags: Brigada jurídica Eduardo Umaña Mendoza, Comité de Solidaridad con presos pol, Mark Burton Abogado Simón Trinidad, Por la Soberanía No a la extradición, Victor Correa Representante a la Cámara Polo Democratico
Slug: especiales-por-la-soberania-no-a-la-extradicion
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/08/C.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<iframe src="http://www.ivoox.com/player_ek_6556225_2_1.html?data=l5qimJeWeY6ZmKial5yJd6KklZKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRh8Lh0caSpZiJhpLVjNXc1JDQpYzH0MfS1MbSaaSnhqaxw5CJdpPC0JDOjdHFb6bs1dfOxs7HrYa3lIqvldOJdpOfktfOjcqRaZi3jqjc0NnFq8rjjLfOxs7Tb46ZmKialg%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Por la Soberanía, No a la extradición (1ra entrega)] 

La campaña "Por la Soberanía No a la extradición", es una iniciativa promovida por diferentes organizaciones sociales, populares de Derechos humanos y sindicales, nacionales y extranjeras, que se oponen a la extradición de nacionales y en particular a la manera en que viene siendo aplicada en el caso colombiano.

Las abogadas Ingrid Saavedra, Gloria Silva y Liria Manrrique, promotoras de la campaña, ayudan a comprender, en esta primera entrega, las razones que motivan la iniciativa, principalmente  sustentadas en la manera que la aplicación de la figura, violenta de manera grave la soberanía del país.

La vulneración de los derechos de la víctimas de crímenes de lesa humanidad, la falta de garantías judiciales para las personas requeridas por países extranjeros, la desintegración familiar y la violación de derechos de niños y niñas, el aumento en el riesgo de tortura y el ser un obstáculo para las posibilidades de solución o acuerdo político del conflicto colombiano, son algunos de los argumentos que motivan la iniciativa de acuerdo con las invitadas.

El especial, contó con la participación de la esposa de un ciudadano extraditado, quien relata el caso de su pareja y la manera en que la situación afecta su núcleo familiar y su situación económica y emocional, así como el representante Víctor Correa, quién reclama que el sistema de justicia debe darle cabida a que los nacionales respondan por sus delitos y sean juzgados en Colombia, por lo cual trabajan en la presentación de un proyecto de ley para regular la extradición en el Congreso de la República.

<iframe src="http://www.ivoox.com/player_ek_6556702_2_1.html?data=l5qimJyUdo6ZmKiakpaJd6KklYqgo5aYcYarpJKfj4qbh46kjoqkpZKUcYarpJKww9LUpYa3lIqvk8aPtNDmjNHOjbjTpsbmwtOSpZiJhaXVjIqflLPTb8KfzcaYp93YtsLYysjWh6iXaaOnz4qflJCWqMKfxpKSmaiRh9Di1cbUy9SPlsLYytSYj4qbh46o&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Por la Soberanía, No a la extradición (2da entrega)] 

En la segunda entrega especial, Lilani Obando ex detenida política, Uldarico Florez Peña, presidente de la Brigada jurídica Eduardo Umaña Mendoza y la abogada Gloria Silva, del Comité de solidaridad con Presos Políticos, ponen en discusión el tema de la aplicación de la extradición como instrumento de violación de los Derechos Humanos y el trabajo realizado por las organizaciones que hacen parte de la campaña "Por la soberanía, No a la extradición".

En el contexto de una salida negociada al conflicto armado, sumado a la criminalización de la movilización social, es necesario aunar esfuerzos para que el sistema judicial actué impartiendo real justicia con garantías para todos y todas los ciudadanos en Colombia y los nacionales solicitados por procesos en el exterior.

Los promotores de la campaña han realizado hasta la fecha, varias actividades de solidaridad entre las que se encuentran plantones en Bogotá, Bucaramanga, Medellín, Cúcuta y Cali, entre otras, la presentación del informe "La situación de la extradición en Colombia" el próximo 21 de Agosto, en el Auditorio Luis Guillermo Velez del Congreso de la República y una audiencia pública el 9 de octubre también en el congreso.

Las voces del Representante a la Cámara por el Polo democrático Víctor Correa, ponente del proyecto de ley que busca modificar el trámite del proceso de extradición de nacionales, respaldado por los gestores de la campaña,  y de Mark Burton, abogado de Simón Trinidad, aportan al entendimiento de la actual situación que viven las personas solicitadas, procesadas o detenidas por la justicia de países diferentes al propio.
