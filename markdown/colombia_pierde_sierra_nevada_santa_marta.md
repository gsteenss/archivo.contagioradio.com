Title: Colombia ha perdido el 92% de la Sierra Nevada de Santa Marta
Date: 2017-03-28 20:06
Category: Ambiente, Nacional
Tags: cambio climatico, IDEAM
Slug: colombia_pierde_sierra_nevada_santa_marta
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/sierra-nevada-santa-marta1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: EFE 

###### [28 Mar 2017] 

El 92% del área glaciar de la Sierra Nevada ha desaparecido, lo que quiere decir que **dentro de unos 40 años ese ecosistema se habrá perdido por completo.** Así lo advierte un informe del Instituto de Hidrología, Meteorología y Estudios Ambientales, IDEAM, que emite una alerta sobre el deshielo en los glaciares colombianos.

Por medio del cálculo de imágenes satelitales, se realizó este estudio de la mano de otras entidades meteorológicas de Ecuador, Bolivia y Perú y el Banco Interamericano de Desarrollo, que concluye que debido a ese deshielo, al país **apenas le quedan 37 kilómetros cuadrados de glaciares.**

Pero la situación no solo es preocupante frente a la Sierra Nevada de Santa Marta, cada año desaparece el 5% de los nevados en Colombia, eso quiere decir que en 50 años hubo una disminución del 63% de los glaciares, y peor aún, en los últimos  seis años se perdió el 17% de la de **Santa Marta y el Cocuy,  y de los volcanes: El Ruiz, Santa Isabel, Tolima y Huila.**

La situación es preocupante, debido a que buena parte del agua que consumen los colombianos proviene de los glaciares, teniendo en cuenta que el agua se produce mediante toda una cadena ecológica que se vería gravemente afectada si se secan por completo los nevados.

Desde el IDEAM, se ha asegurado que debido al avance en el deshielo **“ya es muy tarde para revertir el daño que han sufrido los nevados,** como consecuencia del calentamiento global”. [(Le puede interesar: Para el 2040 la Sierra dejaría de ser nevada)](https://archivo.contagioradio.com/nevados-de-la-sierra-nevada-de-santa-marta-desapareceria-en-el-2040/)

###### Reciba toda la información de Contagio Radio en [[su correo]
