Title: Se hace viral video que critica a Sergio Fajardo
Date: 2020-12-02 18:40
Author: AdminContagio
Category: Cultura, Nacional
Slug: video-viral-contra-fajardo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/12/Sergio-fajardo.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

Un grupo de jóvenes hizo un video que se volvió viral en las últimas horas. El video es una canción de rap que hace fuertes críticas a la vida política del ex candidato presidencial Sergio Fajardo, quien, al parecer, también estaría interesado en participar de la contienda electoral el próximo 2022 enfrentándose a candidatos como Gustavo Petro o Tomás Uribe.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Para algunos analistas y sectores de la opinión como muchos estudiantes universitarios, la aspiración presidencial de Fajardo no es otra cosa que una estrategia del Uribismo para posicionarse ante un candidato fuerte como Gustavo Petro. [Lea también: Los conflictos e intereses de la anterior junta directiva de EPM](https://archivo.contagioradio.com/los-conflictos-de-intereses-entre-la-exdirectiva-de-epm-y-el-gea/)

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Sin embargo, otros sectores de la población consideran que el candidato de los verdes, quién ha respaldado a la Alcaldesa de Bogotá, Claudia López y que al parecer, estaría ocupando importantes cargos en la capital, es una alternativa real ante lapolarización.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Lo cierto del caso es que muchos jóvenes no le perdonan al "candidato profesor" que se haya ido a "mirar ballenas" dejando la contienda electoral en manos del voto en blanco que, para ellos favoreció la candidatura de Duque en las elecciones pasadas.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Este es el video completo

<!-- /wp:paragraph -->

<!-- wp:core-embed/facebook {"url":"https://www.facebook.com/1507280742905964/posts/2514143185553043/","type":"rich","providerNameSlug":"embed-handler","className":""} -->

<figure class="wp-block-embed-facebook wp-block-embed is-type-rich is-provider-embed-handler">
<div class="wp-block-embed__wrapper">

https://www.facebook.com/1507280742905964/posts/2514143185553043/

</div>

</figure>
<!-- /wp:core-embed/facebook -->
