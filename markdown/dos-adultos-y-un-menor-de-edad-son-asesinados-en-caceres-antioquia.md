Title: Dos adultos y un menor de edad son asesinados en Cáceres, Antioquia
Date: 2020-10-04 00:39
Author: CtgAdm
Category: Actualidad, Nacional
Tags: colombia, masacre
Slug: dos-adultos-y-un-menor-de-edad-son-asesinados-en-caceres-antioquia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/10/masacre-tumaco.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

Este sábado 3 de octubre se registró una nueva masacre, donde fueron asesinadas 3 personas, una de ellas es un menor de 15 años; **en zona rural de Cáceres, en límites con el municipio de San José de Uré,** al sur del departamento de Córdoba, en donde

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/Indepaz/status/1312569683527303170","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/Indepaz/status/1312569683527303170

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

El hecho se presentó en la finca El Breque, zona rural de Cáceres en límites con los municipios de [Tarazá y Cáceres, Antioquia,](https://archivo.contagioradio.com/la-violencia-se-reagrupa-y-reestructura-en-los-montes-de-maria/) allí fueron asesinados **Juan Carlos Rodríguez** de **30 años,** **Wilmer Eduardo López**, de **42 años**, y **Daivi Camilo Ochoa** de **15 años**, quienes según medios locales reportan heridas de arma blanca, así como varios impactos de arma de fuego, junto a ellos también se señala que fue herido u**n hombre identificado como Rafael Cabrera.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Adicional comunidades locales indicaron que las personas asesinadas era el propietario de la finca, El Breque, y su hijo, juntos a ellos también perdió la vida el conductor de un vehículo transportador de ganado. ([Otra Mirada: Guerra y abandono estatal, el yugo contra indígenas en Nariño](https://archivo.contagioradio.com/otra-mirada-guerra-y-abandono-estatal-el-yugo-contra-indigenas-en-narino/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Según el Instituto de Estudios para la Paz ([Indepaz](http://indepaz.org.co)), este hecho violento corresponde a la **masacre número 66 ocurrida durante el 2020**, y agregó que en esta zona operan grupos arma ilegal como las AGC, los Caparros, GAOR 18 Román Ruiz y el ELN.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

A este hecho se suma el del 13 de febrero en dónde fueron asesinados 4 campesinos también en zona rural de Cáceres, así como el **desplazamiento de 700 personas en el mismo municipio,** que tuvieron que abandonar de manera forzada sus viviendas debido a la presencia e intimidación de estos grupos armados.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Y más recientemente el 15 de septiembre también se registró una masacre donde fueron asesinadas 3 mineros en la vereda Los Azules, también en Cáceres, hechos que persisten pese a que el pasado 31 de agosto la Defensoría del Pueblo presentará una alerta temprana para seis municipios del Bajo Cauca antioqueño, señalando que la ciudadanía y los lideres y lideresas sociales se encontraban en un inminente riesgo.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/ascsucor_org/status/1312483440869076992","type":"rich","providerNameSlug":"twitter"} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/ascsucor\_org/status/1312483440869076992

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:block {"ref":78955} /-->
