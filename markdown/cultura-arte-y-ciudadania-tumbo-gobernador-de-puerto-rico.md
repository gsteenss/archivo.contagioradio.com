Title: Cultura, Arte y Ciudadanía: la tormenta perfecta que tumbó a Roselló
Date: 2019-07-25 19:36
Author: CtgAdm
Category: Cultura, El mundo
Tags: movilizaciones sociales, puerto rico, Ricardo Rosselló
Slug: cultura-arte-y-ciudadania-tumbo-gobernador-de-puerto-rico
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/Puerto-rico2.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/Puerto-Rico-1.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Joe Raedle/Getty Images] 

Tras varios días de protestas motivadas por el cruce de mensajes ofensivos, sexistas y homófobos, Ricardo Rosselló, gobernador de Puerto Rico anunció su dimisión al cargo. Situación que se convirtió en la cereza del pastel ante el descontento generalizado que vive la población de la Isla con la actual administración y los principales partidos políticos. (Le puede interesar: [Hijo de tigre... las cuestionadas acciones de Flávio Bolsonaro](https://archivo.contagioradio.com/relaciones-salpican-flavio-bolsonaro/))

El cruce de chats con algunos de sus colaboradores, colmo la paciencia de cientos de miles de puertorriqueños. Una mecha que se venía encendiendo por fenómenos como la corrupción, en un país que trata de salir a flote con una economía estancada, y los estragos producidos por el paso del Huracán María que devastó la isla en septiembre de 2017.

De manera no violenta pero muy decidida, indignados se tomaron las calles cantando y ondeando banderas unidos para exigir la renuncia del gobernador. La manifestación más grande fue el martes 23 de julio en San Juan, la capital. El día anterior, Rosselló aseguró que dimitiría como presidente del Partido Nuevo Progresista y no volvería a postularse en 2020, pero en principio se negó a renunciar como gobernador.

Posteriormente, Rosello trató disculparse pero los manifestantes no estaban dispuestos a conformarse con el compromiso de verlo en las próximas elecciones e insistieron con firmeza hasta lograr su destitución. «No es suficiente. Debe entregar el poder a los nuevos líderes», fueron algunos de los gritos que animaron las Calles de San Juan.

\[caption id="attachment\_71230" align="aligncenter" width="356"\]![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/Puerto-Rico-1-300x257.jpg){.wp-image-71230 width="356" height="305"} San Juan, Porto Rico, 22 julio  
(Angel Valentin/Getty Images)\[/caption\]

### Movimiento de artistas contribuye a tumbar al Presidente 

Grupos de artistas del país contribuyeron a liderar las protestas dando su aporte a la presión de los sindicatos y organizaciones sociales, algunos incluso dejaron pospuestas sus propias giras de conciertos. Luego de hacerse pública la renuncia de Rosselló, Benito Martínez conocido artísticamente como Bad Bunny declaró en Twitter: "Se siente cabrón hacer historia con mi gente, con mi pueblo, con mi Puerto Rico".

También el cantante Ricky Martin, durante las protestas además de sumarse a la lucha, comentó su dolor con estas palabras: "se mofaron de nuestros cadáveres, se mofaron de la mujer, se mofaron de la comunidad LGBT, se mofaron de toda la isla.  
Otros artistas internacionales como Daddy Yankee, Luis Fonsi, Marc Anthony, Lin-Manuel Miranda y Benicio del Toro respaldaron a sus colegas y los ciudadanos de Puerto Rico.

En su declaración, Rosselló afrimó que su renuncia entrará en vigencia a partir del 5 de agosto.  Su lugar será ocupado temporalmente por la secretaria de Justicia Wanda Vázquez, ex fiscal que había asumido anteriormente el cargo de Ministra por la igualdad de oportunidades. De esta forma, la movilización se convirtió en la más grande en más de 15 años desde que los habitantes lograron al exigir el fin de los ejercicios militares de la Armada estadounidense en la isla de Vieques.

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
