Title: Atentan contra la vida del líder Awá, Javier Cortés Guanga en Nariño
Date: 2020-08-11 17:32
Author: CtgAdm
Category: Actualidad, Líderes sociales
Slug: atentan-contra-la-vida-del-lider-awa-javier-cortes-guanga-en-narino
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/08/WhatsApp-Image-2020-08-11-at-2.09.27-PM-2.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

Este 11 de agosto la **Asociación Minga** denunció el atentado contra la vida del líder Awá, **Javier Cortés Guanga en el corregimiento de Llorente,** zona rural del puerto de Tumaco en [Nariño](https://archivo.contagioradio.com/otra-mirada-narino-resiste-ante-el-olvido-estatal/) . El líder indígena había denunciado en varias ocasiones **amenazas contra su vida y la de su familia.**

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/asociacionminga/status/1293254552288927744","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/asociacionminga/status/1293254552288927744

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

El hecho se presentó sobre las 12:00 del mediodía de este martes en el sector de La Vaquería a 2 km de la zona urbana de Llorente, **mientras salía de la casa de su madre rumbo a reunirse con gobernadores** del resguardo indígena Awá cercano, con el fin de dialogar sobre un proyecto de electrificación.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

La acción armada se presentó mientras el líder se movilizaba en su vehículo y fue abordado por seis hombres armados que dispararon en múltiples ocasiones en su contra, **pese a que el automóvil no es blindado el líder junto con sus escoltas lograron evitar el atentado y salir ilesos** del hecho, logrando resguardarse en un batallón militar cercano.

<!-- /wp:paragraph -->

<!-- wp:quote -->

> *"Afortunadamente Javier se encuentra bien estamos desde la asociación minga y el programa somos defensores acompañando todo el proceso para sacarlo del territorio y resguardar su vida"*
>
> <cite>Señala vocera de la Asociación Minga </cite>

<!-- /wp:quote -->

<!-- wp:paragraph -->

Pocos minutos luego de la agresión, el líder Awá alertó a la Asociación Minga, quiénes junto al **Programa Somos Defensores** activaron las acciones de protección a la vida de los liderazgos. Advierten que no es la primera vez que el líder Awá recibe amenazas contra su vida**, en el último mes ha sido receptor de panfletos** enviados por los distintos grupos armados que operan en la zona.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Los hechos se dan **a menos de 20 días del asesinato del gobernador suplente del Cabildo Piguambí Palangala, Rodrigo Salazar**. mientras las agresiones se han extendido además a la familia del líder Awá, **[Segundo Jaime Cortés Pa](https://asociacionminga.co/index.php/2020/07/27/continuan-amenazas-y-presiones-contra-lideres-del-pueblo-awa-en-narino/)i , padre de líder indígena y además Gobernador del resguardo Piguambi Palangala** y quién el pasado 24 de julio, recibió por medio de la página de Facebook del cabildo y del perfil de la docente **Milena Cortes Pai** hija del Gobernador, un mensaje de amenaza en su contra.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

*"Alguien que se identifica como del “Ejército del Pueblo GUP”, señala que **al gobernador lo tienen permanentemente vigilado, y que sus escoltas de poco le van a servir**, esto es, que ya han considerado atentar contra su vida"*, señaló el comunicado emitido por las Autoridades Tradicionales del Pueblo Indígena Awá el pasado 27 de julio.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/DianaDefensora/status/1293263893637332992","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/DianaDefensora/status/1293263893637332992

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

A su vez la Asociación Minga hizó un llamado al Gobierno para que se garantice la vida de las personas líderes en el departamento de Nariño en medio de la situación de militarización y asesinatos que afronta el territorio y que ha causado [el homicidio de 10 líderes](https://archivo.contagioradio.com/971-defensores-de-dd-hh-y-lideres-han-sido-asesinados-desde-la-firma-del-acuerdo-de-paz/) en tan solo el primer semestre de este 2020.

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
