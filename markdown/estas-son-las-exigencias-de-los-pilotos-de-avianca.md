Title: Estas son las exigencias de los pilotos de Avianca
Date: 2017-09-18 13:11
Category: Movilización, Nacional
Tags: Avianca, Ministerio de Trabajo, Pilotos, Pliego de Avianca
Slug: estas-son-las-exigencias-de-los-pilotos-de-avianca
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/09/avianca-taca.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: El Heraldo] 

###### [18 Sept. 2017] 

El 50% de los pilotos entrarían a paro este miércoles para **exigir se apruebe el pliego de peticiones que incluye mejoras en los salarios**, solución a los problemas de seguridad aérea y la eliminación de la discriminación de los pilotos colombianos frente a los demás aviadores de Avianca en la región. Además piden se respete la Convención Colectiva vigente y no se insista en el pacto Colectivo calificado como ilegal por el Ministerio de Trabajo.

**Jaime Hernández presidente de la Asociación Colombiana de Aviadores Civiles,** aseveró que al interior de la compañía no se están garantizando los derechos del gremio al tener que trabajar 12 horas 5 días a la semana, lo que se traduce en una fatiga excesiva de los trabajadores y representa un riesgo para quienes comandan los vuelos y viajan con dicha aerolínea.

**La huelga inicia este miércoles 20 y podría ir hasta el próximo 27 de septiembre**, de no llegar a un acuerdo con el Ministerio de Trabajo y los representantes de Avianca. Auxiliares de vuelo, de mantenimiento y servicios en tierra de Bogotá, Barranquilla, Bucaramanga, Cali y Medellín  apoyan la huelga iniciada.

### **¿Qué están pidiendo los pilotos al Gobierno y a la empresa?** 

Dentro de los 8 puntos del pliego de peticiones se encuentra el acatamiento de los fallos, que se cumpla la ley, que se cumpla la convención colectiva y **que se negocie el pliego de peticiones, que permitiría que Avianca sea una empresa eficiente**, más segura y sin discriminación y violación de los derechos humanos.

“La Corte Constitucional ya se pronunció y la administración de Avianca se empeña en no acatar (…) ya hay una orden de un fiscal en el que ordena cárcel de 1 día al representante legal por desacato de la sentencia T-069 de 2015”.

Además, **pedirán que quede por escrito las mejoras en las jornadas laborales que en algunos casos completan 12 horas durante 5 días seguidos**, lo que representa una fatiga excesiva para quienes conducen los aviones.

“Una persona que comienza a volar a las 5 a.m. está levantado desde las 2 a.m. y están terminando a las 5:30 p.m. y están llegando a su casa a las 7 p.m. para levantarse nuevamente a las 2 a.m. y así 5 días seguidos”. Le puede interesar: [Colombia se raja en Derechos Económicos, Sociales y Culturales ante la ONU](https://archivo.contagioradio.com/colombia-desigual-informe-sobre-la-situacion-de-los-derechos-economicos-sociales-y-culturales/)

### **¿Qué es el Pacto Colectivo?**

El Pacto Colectivo es una figura que se ha estado utilizando durante varios años, dice el Capitán Hernández, para acabar la negociación de las Asociaciones legales. **De este Pacto son parte los pilotos administrativos, quienes dialogan con la empresa las necesidades que hay** y cómo modificarlas, sin embargo, lo que aseguran los pilotos es que ellos no los representan y no conocen sus requerimientos.

“Nosotros representando a los pilotos nos sentamos con la empresa, tratando de buscar mejoras operacionales y ellos a través de los pactos colectivos lo que hacen es suplantar estos acercamientos y **quienes representan esos pactos son pilotos administrativos que suplantan las necesidades del piloto** y desde el punto de vista administrativo reproduce un pacto colectivo y buscan que sea el acuerdo, lo cual no es ético, legal y no está bien hecho”.

### **“La huelga es ilegal” Organización de Aviadores de Avianca** 

Relata Hernández que esa organización no es un sindicato, sino una agremiación sindical que “recluta forzosamente personas ya que los pilotos nuevos que ingresan a la compañía se ven obligados a firmar el Pacto Colectivo y si a él le preocupa que la huelga no es legal, pues le decimos no se preocupe por que es legal”.

<iframe id="audio_20944300" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_20944300_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
