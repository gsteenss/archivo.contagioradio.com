Title: No más Escuela de las Américas
Date: 2014-12-16 15:39
Author: CtgAdm
Category: Video
Slug: no-mas-escuela-de-las-americas
Status: published

\[embed\]https://www.youtube.com/watch?v=mmc0OGADXP8\[/embed\]  
Plantón realizado el viernes 21 de noviembre frente al Ministerio de Defensa en Colombia, para rechazar el envío de militares a entrenarse a la Escuela de las Américas, en EEUU.
