Title: Se crea primera biblioteca especializada en la construcción de Paz en Colombia
Date: 2016-08-05 13:45
Category: Nacional, Paz
Tags: FARC, Gobierno, paz, pedagogía de paz, proceso de paz
Slug: se-crea-primera-biblioteca-especializada-en-la-construccion-de-paz-en-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/03/Caminata-por-la-paz-60.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Gabriel Galindo Contagio Radio 

###### [5 Ago 2016] 

En el marco las actividades que se promueven desde distintos sectores para hacer pedagogía sobre la construcción de la paz en Colombia, la Red Nacional de Programas Regionales de Desarrollo y Paz,  **Redprodepaz, creó la primera biblioteca con material bibliográfico, audiovisual y digital** con contenidos sobre el actual proceso negociaciones que se desarrolla en La Habana, pero también sobre los procesos de construcción de paz que han desarrollado históricamente las comunidades desde sus territorios.

Reconciliación y convivencia; Desarrollo Integral; Territorio y ambiente; Educación y cultura; y Gobernanza, son los temas que podrá consultar la ciudadanía mediante un servicio abierto, democrático y gratuito, en la biblioteca **Alma Rosa Jaramillo L, que rinde homenaje a las y los** constructores de paz en Colombia.

**Esta lleva ese nombre rindiendo homenaje a una de las caras de lo que ha significado el conflicto armado en Colombia. Se trata de Alma Rosa,** una abogada y líder social que contribuyó a la defensa de los derechos de las organizaciones campesinas del sur de Bolívar y que, en ejercicio de su labor, fue asesinada el 29 de junio del año 2001 en el municipio de Morales.

Esta iniciativa, nace tras identificar “**la necesidad de organizar y capitalizar el saber hacer de 26 programas de Desarrollo y Paz** que, en medio de la guerra, lograron abrirle espacio a la vida de sus comunidades con espacios de diálogo y participación en los territorios, como se explica desde la Redprodepaz.

Un compendio que se logra gracias a las donaciones de  Entidades Facilitadoras, Entidades de Apoyo, Equipo de la Coordinación Nacional, que suman **un total de 25 organizaciones aliadas de la Redprodepaz,** de acuerdo con sus aportes a los procesos de transformación social y la consolidación de la paz territorial.

La biblioteca se podrá consultar también por vía virtual a través del sistema de información El Armadillo: [www.redprodepaz.net/Armadillo](http://www.redprodepaz.net/Armadillo) y en la sede de la Redprodepaz en su sede en Bogotá, ubicada en la Cra. 6 No. 35-49, donde se realizará este viernes la inauguración de esta iniciativa, de la manos de una conversación sobre esta apuesta para luego compartir una copa de vino con los asistentes.

<iframe src="http://co.ivoox.com/es/player_ej_12453572_2_1.html?data=kpehl5iZe5Ohhpywj5aUaZS1lJ6ah5yncZOhhpywj5WRaZi3jpWah5yncaLn1dfWxpCwaaSnhqeg0sreb46ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
