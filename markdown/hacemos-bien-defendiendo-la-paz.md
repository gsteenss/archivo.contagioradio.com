Title: ¿Hacemos bien defendiendo la paz?
Date: 2016-03-23 10:52
Author: JOHAN MENDOZA TORRES
Category: Johan Mendoza, Opinion
Tags: defensores de derechos humanos, Paramilitarismo, paz, proceso de paz
Slug: hacemos-bien-defendiendo-la-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/10/paramilitares.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Carlos García 

#### [Johan Mendoza Torres](https://archivo.contagioradio.com/johan-mendoza-torres/) 

###### 23 Mar 2016 

[William Castillo líder social asesinado en Antioquia, Klaus Zapata comunicador social asesinado en Bogotá, Maricela Tombe campesina asesinada en el Cauca, Milton Escobar campesino asesinado en Arauca, Alexander Oime líder asesinado en Popayán y en Putumayo asesinados nueve líderes de base…. Esto sólo por hablar del mes de marzo de 2016.]

[La tristeza me invade como académico, como colombiano, como ser humano, ¿es esto el camino hacia la paz? ¿es realmente este el país democrático ejemplar del que hablan algunos convencidos en las universidades y otros tantos, en medios informativos masivos cuando se burlan de otras naciones? ¿es realmente esto lo que merece la gente que piensa diferente en nuestra patria? ¿por qué no escuchamos ni vemos a los gurús de las grandes cadenas de medios indignados, apelando a su ideología día y noche, llamando a expertos de aquí o allá para explicarle al país qué está pasando? ¿por qué ni un solo twitter de Claudia Gurisatti indignada con la misma vehemencia con la que se indigna por lo que le sucede al “sedicioso” de Leopoldo López? ¿por qué?]

[¿Por qué las cosas tienen que ser así en un país al que muchos le apostamos día tras día, por la construcción y reconstrucción de escenarios donde si bien no haya unanimidad, por lo menos se configuren espacios en los cuales podamos dialogar sin matarnos, sin que nos maten por pensar diferente? ¿por qué no veo en Facebook una ola viral de filtros con la bandera de Colombia? ¿por qué esos que se quejan de que aquí no nos indignamos como la engalanada sociedad europea que sale en masa a marchar cuando la calamidad la azota, no están promoviendo, escribiendo o si acaso reflexionando con dolor de patria lo que ocurre en Colombia?  ]

[La paz es un tema filosófico, los acuerdos son la realidad que el gobierno y la guerrilla nos ofrecen. La muerte de colombianos inocentes bajo el plomo de siempre, es lo que el siniestro poder de terratenientes, multinacionales y corruptos en la administración pública ofrecen a nuestro pueblo. Hoy el paramilitarismo avanza nuevamente con firmeza sobre los territorios colombianos.]

[Hoy a esa tragedia se le suma la indolencia de los sínicos en las ciudades, esos a los que les preocupa un paro solo porque no pueden viajar rápido, se suma la indolencia de los sínicos de la opinión, que se la pasan hablando de otras naciones sin tocar las temáticas más vergonzosas de nuestro país simplemente porque no les importa y por tanto no se les da la gana hacerlo.]

[Hoy como académico siento cada vez más fuerte un vacío tremendo, cuando me pregunto ¿de qué está sirviendo rescatar mediante el estudio, el diálogo y la pedagogía, aquellas raíces endémicas del conflicto en nuestro pueblo cuando hay gente que le importa un bledo los muertos que no salen en las grandes cadenas de información? ¿Qué estoy haciendo como promotor de la paz ante un país que tiene políticos que protestan a favor del paramilitarismo, que tiene gente amante de corruptos a sabiendas de todos sus escándalos?]

[Así mismo, cuando se constituye una pedagogía para paz, es porque le apostamos a confiar en que pensemos lo que pensemos no tenemos derecho a morir de la manera como muere nuestra gente hoy. No es justo morir por ideas políticas; y mientras el asesinato por dicha causa siga tan macabramente vivo en nuestro país, no sé si será bueno defender “la paz”, no sé si será bueno apostarle a un posconflicto en donde un socialista y un neoliberal debaten con energía en una universidad, en una plaza, en algún lugar que no sea en la violencia... A veces creo que las posiciones intransigentes son aquellas que en verdad se empoderan y pueden enfrentar el poder (mas allá de las palabras) con un criterio beligerante… a veces siento que defender la paz bajo estas premisas aquí presentadas, es un acto poco ético, pues evidentemente, las fuerzas oscuras y poderosas de Colombia están dando un claro mensaje.]

[Quiero seguir promoviendo la paz, pero bajo qué condiciones ¿estás? ¿el asesinato? ¿El futuro irremediable de un balazo atravesando nuestro cuerpo por soñar un país diferente? ¿por pensar diferente? ... No sé, me invade el dolor de saber que en el mes de marzo han sido asesinados tantos líderes sociales… y con dolor uno no piensa bien ¿o sí?  ]
