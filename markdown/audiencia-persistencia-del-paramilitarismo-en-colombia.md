Title: Audiencia Persistencia del Paramilitarismo en Colombia
Date: 2016-04-15 09:29
Category: Nacional
Tags: paramilitares
Slug: audiencia-persistencia-del-paramilitarismo-en-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/04/paramili.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### [15 Abr 2016]

Siga en Vivo la audiencia sobre la vigencia del Paramilitarismo en Colombia.  
<iframe src="https://www.youtube.com/embed/qHbXwnJyOFI" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>
