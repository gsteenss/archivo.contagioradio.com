Title: Las 11 recomendaciones sobre Derechos Humanos de la ONU a Colombia
Date: 2015-03-18 11:05
Author: CtgAdm
Category: DDHH, Nacional
Tags: alto comisionado, Derechos Humanos en Colombia, ONU, todd howland
Slug: las-11-recomendaciones-sobre-derechos-humanos-de-la-onu-a-colombia
Status: published

###### Foto: Contagio Radio 

##### <iframe src="http://www.ivoox.com/player_ek_4228722_2_1.html?data=lZefmpyWdo6ZmKiakpaJd6KnlpKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRmNDYxZC10dzQpc%2FYhpewja7SqtDmzsqYycrSqdPVzZC8sLqRaZi3jqjc0NnFq8rjjLfOxs7Tb46ZmKialg%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>  
[ Todd Howland, Alto Comisionado de las Naciones Unidas] 

#####  

El lunes 16 de marzo, la oficina del Alto Comisionado de las Naciones Unidas para los Derechos Humanos en Colombia (ACNUDH) presentó su informe anual sobre la situación de Derechos Humanos, que incluye 11 recomendaciones para el gobierno nacional y las insurgencias de las FARC y el ELN.

Todd Howland recordó que la ONU y el gobierno colombiano renovaron el acuerdo establecido hasta el año 2016, y que la intensión de la oficina del Alto Comisionado es hacer observaciones y preparar el trabajo de acompañamiento con miras al proceso de paz y el eventual "postconflicto" colombiano.

En ese sentido, aunque la oficina planteó en términos generales que es positivo que se estén adelantando diálogos de paz, y que el gobierno y los movimientos sociales hayan establecido espacios de interlocución para la resolución de conflictos, existen retos importantes de afrontar. Por ejemplo, que Colombia sea un país con históricos indices de desigualdad, y que funcione políticamente como "aliados y enemigos" en que el gobierno "mira en otra dirección cuando los amigos están violando los DDHH". "La *no-repetición* no puede ser simbólica, debe implicar cambios estructurales que signifiquen que la violación a los DDHH no se van a repetir", indicó el Alto Comisionado.

En materia de restitución de tierras, el informe indica que ACNUDH entregó recomendaciones al gobierno en 5 oportunidades, en las que señala que la principal preocupación no puede enfocarse en cumplir un record de restitución, sino en garantizar la integralidad de la misma: que las comunidades tengan acceso a agua potable, salud, educación, trabajo o ingresos económicos, etc., pues la ausencia de garantías para la vida digna puede ser un hecho re-victimizante.

<iframe src="http://www.ivoox.com/player_ek_4228728_2_1.html?data=lZefmpyWfI6ZmKiakpaJd6Kkl5KSmaiRdo6ZmKiakpKJe6ShkZKSmaiRmNDYxZC10dzQpc%2FYhpewjbLTqMbg0JDSxdTSs87dxNSah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Modelo económico en Colombia. Todd Howland.] 

<div>

Señala la oficina del Alto Comisionado que "todavía estamos preocupados sobre las judicializaciones en contra de líderes sociales y comunales, y la insuficiencia de elementos para llevarlos a juicio". La situación de las y los defensores de Derechos Humanos se agravó en el último año, según indican, con amenazas y asesinatos incluso a personas que habían manifestado encontrarse en riesgo, personas con esquemas de seguridad o a quienes se les fue retirado.

<iframe src="http://www.ivoox.com/player_ek_4228735_2_1.html?data=lZefmpyXeY6ZmKiakpiJd6KolJKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRmNDYxZC10dzQpc%2FYhpewja%2FZt9XdxM7Oj4qbh4630NPhw8zNs4zGwsnW0ZCRaZi3jpk%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Justicia en Colombia. Todd Howland.] 

En términos de las Ejecuciones extrajudiciales, Todd Howland enfatizó que las instituciones no pueden excusarse en el desconocimiento o la omisión, pues la cantidad de casos, que según la oficina se acerca a los 5 mil, demuestra la responsabilidad por parte de altos mandos tanto del Ministerio de Defensa como de poder ejecutivo y legislativo.

 

#####  

**Recomendaciones:**

[![Recomendaciones 1](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/03/Recomendaciones-1-1024x516.jpg){.aligncenter .wp-image-6079 .size-large width="604" height="304"}](https://archivo.contagioradio.com/uncategorized/las-11-recomendaciones-sobre-derechos-humanos-de-la-onu-a-colombia/attachment/recomendaciones-1/) [![Recomendaciones 2](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/03/Recomendaciones-2-660x1024.jpg){.aligncenter .wp-image-6080 .size-large width="604" height="937"}](https://archivo.contagioradio.com/uncategorized/las-11-recomendaciones-sobre-derechos-humanos-de-la-onu-a-colombia/attachment/recomendaciones-2/) [![Recomendaciones 3](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/03/Recomendaciones-3-1024x406.jpg){.aligncenter .wp-image-6081 .size-large width="604" height="239"}](https://archivo.contagioradio.com/uncategorized/las-11-recomendaciones-sobre-derechos-humanos-de-la-onu-a-colombia/attachment/recomendaciones-3/)

</div>
