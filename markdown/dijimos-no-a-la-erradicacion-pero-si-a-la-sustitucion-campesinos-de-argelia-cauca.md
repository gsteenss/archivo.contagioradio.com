Title: "Dijimos no a la erradicación, pero sí a la sustitución" campesinos de Argelia, Cauca
Date: 2017-04-11 16:48
Category: DDHH, Nacional
Tags: Argelia Cauca, COCCAM, erradicación cultivos ilícitos
Slug: dijimos-no-a-la-erradicacion-pero-si-a-la-sustitucion-campesinos-de-argelia-cauca
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/04/coca.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: ] 

###### [11 Abr 2017] 

Campesinos en Argelia, Cauca, llegaron a un acuerdo con el Ejército y la Policía Nacional para que frenen la erradicación forzada en las veredas, dejen de hacer señalamientos a las comunidades de ser **“mentirosas y terroristas” y logren iniciar planes de sustitución de cultivos ilícitos acorde con sus necesidades**. Sin embargo continuarán en paro hasta no ver voluntades por parte del Estado.

De acuerdo con Orlando Bolaños, desde hace dos días el Ejército, de forma violenta ha venido amedrentando a los campesinos y erradicando sus cultivos, pese a que estos hayan expresado que quieren hacer parte de los planes de sustitución:“Ha sido un atropello por parte del ejercito hacia los campesinos de la cabecera hacía arriba, en donde **estuvieron radicando sin consultar con la comunidad, de una manera absurda quisieron encañonar a la gente**, somos una comunidad unida y dijimos no a la erradicación, pero si a la sustitución”.

Durante la mañana de hoy y en una asamblea, el **Ejército se comprometió a no continuar con la erradicación**, mientras que las autoridades administrativas, en representación del Alcalde, se comprometieron a gestionar una visita del mecanismo tripartito de verificación y monitorio para iniciar con los planes de sustitución. Le puede interesar:["Plan de sustitución no se está cumpliendo: COCCAM"](https://archivo.contagioradio.com/plan-de-sustitucion-voluntaria-no-se-esta-cumpliendococcam/)

Sin embargo, los campesinos señalaron que se mantendrán vigilantes del cumplimiento de esta orden y mantendrán el bloque a las calles de entrada y salida de Argelia, durante las próximas horas, hasta confirmar que el gobierno tiene la voluntad de construir planes conforme a las necesidades de los campesinos, **el Comité Municipal Cocalero, será el encargado de hacer la veeduría. **Le puede interesar: ["Campesinos de Tumaco y gobierno logran acuerdo sobre sustitución de cultivos ilícitos"](https://archivo.contagioradio.com/campesinos-de-tumaco-y-gobierno-logran-acuerdo-sobre-sustitucion-de-cultivos/)

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)
