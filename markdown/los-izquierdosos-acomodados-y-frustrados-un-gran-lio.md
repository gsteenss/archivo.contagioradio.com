Title: Los izquierdosos acomodados y frustrados, un gran lío
Date: 2016-04-27 06:36
Author: JOHAN MENDOZA TORRES
Category: Johan Mendoza, Opinion
Tags: izquierda colombiana
Slug: los-izquierdosos-acomodados-y-frustrados-un-gran-lio
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/04/change.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Imagen: strangepolitics] 

#### Por [Johan Mendoza Torres](https://archivo.contagioradio.com/johan-mendoza-torres/) 

Que lío tan tremendo para un país como Colombia tener tantas dudas sobre lo que es derecha o lo que es izquierda. Que lío tan tremendo es escuchar y leer a algunos ingenuos (o vivos) asegurando que el paramilitarismo se “reactivó”, como si alguna vez hubiese desaparecido. Es un problema bien grave, porque aquí en Colombia se le dice izquierdoso a Samuel Moreno o a Luis Eduardo Garzón, se le dice demócrata a Uribe, gran empresario a Nule y así…

[En Colombia esto trae muchos problemas ya que esas empresas llamadas partidos políticos, explotan el discurso liberal, pero lo dotan de matices sociales para conseguir más votos, o estatus de “opositores” en una democracia tan abierta en el papel, pero tan cerrada en su estructura. Aquí el procurador, es decir, quién procura que el poder esté al servicio de la gente y no a merced del funcionario, ese abierto derechista, habla en las plazas contra la restitución de tierras de la mano de José Félix Lafaurie quien tiene acusaciones en su contra por colaboración a paramilitares; sí, todo esto ocurre en mi querida patria, no obstante, la masa de individualistas crece, la indiferencia colma nuestras calles y como si fuera poco, la sociedad anda llena de izquierdosos frustrados e izquierdosos acomodados.]

[Creo yo, que hay un acuerdo sobre cómo piensa la derecha y quiénes son los déspotas del país. Pero con la izquierda hay un matiz enorme, no nos ponemos de acuerdo, dejando en duda cualquier intento de conclusión.]

[El izquierdoso frustrado viene de ese relato, o incluso en algunos casos meta-relato, donde muchos cuentan, cómo cuando jóvenes tiraron piedra, tenían ideales, eran “revolucionarios” pero que místicamente se dieron cuenta, con el trabajo, la vida “real” y las cosas, que era mejor dejar todo atrás. Luego esos sujetos que cuentan dicho relato, para evitar construir soluciones que respondieran a los cambios históricos y encarar el drama en el que vive Colombia, se disiparon en dos grupos: unos huyeron frustrados y resentidos a los brazos del liberalismo mental, cambiaron la estatuilla de la virgen o el cuadro de Lenin, por las palabras “éxito y libertad” que las repiten 5 veces] [frente al espejo antes de salir a trabajar y se dedicaron a considerar fracaso, todo lo que les recuerde la enorme frustración que sienten por haber abandonado sus ideales juveniles.]

[Otros, los izquierdosos acomodados, se quedaron anclados en discursos sociales y alternos, que rápidamente fueron reciclados por los grandes medios de radio y televisión, así como por la cultura popular, que vio en la rebeldía un mercado igual de lucrativo al de los defensores del statu quo. Esa izquierda acomodada es la que se caracteriza por su docilidad, por su falta de atrevimiento con consecuencias más allá del beneplácito individual… ¡¡es más!! estoy por pensar que, si un par de años atrás, las carreras hubiesen tenido 20 semestres, la revolución hoy sería un hecho en Colombia, pues crónicamente la gente que se graduaba, inmediatamente desechaba los ideales.  ]

[Sí, esos izquierdosos acomodados y sobretodo los frustrados, los incapaces, los que hoy se la montan a la guerrilla por su visión sobre cómo acabar ese martirio que debe ser la guerra cuando ¡en su vida! han disparado un solo tiro y no caminan más de 5 cuadras antes de pedir el taxi. Esa izquierda frustrada es un problema, porque siempre que alguien intenta hacer algo, llega para dar “cátedra” sobre cómo podría hacerlo mejor, o lo que es más común, llega a denigrar de aquellos que aún creen y sienten la ideología tan viva como la carne misma. Sí, la izquierda acomodada y sus neurosis, es la que incumple citas, la que no viajaría si no le pagan, la que no escribiría porque no le ve sentido, la que se volvió budista no porque comprendió a fondo los sūtras, sino a fin de cuentas porque así podía continuar su crítica a la iglesia y militar en un espectro occidental de una religión oriental donde pudiese, básicamente, no comprometerse con nadie más que consigo mismo…. Sí, esos izquierdosos frustrados y acomodados son un problema bastante serio, pues ya no son izquierda y valía la pena dedicarles unas letras.]

------------------------------------------------------------------------

###### Las opiniones expresadas en este artículo son de exclusiva responsabilidad del autor y no necesariamente representan la opinión de la Contagio Radio. 
