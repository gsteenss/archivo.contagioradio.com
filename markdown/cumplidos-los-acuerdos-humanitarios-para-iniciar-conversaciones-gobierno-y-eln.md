Title: ELN y gobierno cumplen acuerdos humanitarios
Date: 2017-02-02 12:12
Category: Nacional, Paz
Tags: ELN, Odín Sánchez, proceso de paz
Slug: cumplidos-los-acuerdos-humanitarios-para-iniciar-conversaciones-gobierno-y-eln
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/02/Nixon-Cobos-y-Leivis-Valero-y-odin-sanchez.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: ELN-el país] 

###### [02 Feb 2017]

Tanto el ELN como el gobierno cumplieron ya los acuerdos humanitarios alcanzados en Enero de este año luego de más de 2 meses de "atranques" al inicio de la fase pública de conversaciones. Durante la mañana de este 2 de Febrero tanto **Odín Sánchez como dos integrantes del ELN detenidos en la cárcel de “Palogordo” en Santander fueron excarcelados.**

Odín Sánchez fue entregado a una comisión de la Cruz Roja, los países garantes y delegados del gobierno nacional. Por otra parte ya está en Colombia una comisión de paz del ELN para recibir a dos detenidos liberados por **razones humanitarias** en Bucaramanga.

En torno a la liberación del político chocoano Odín Sánchez, se realizó en el departamento del Chocó, en el corregimiento de Pinta Nueva, en medio de una celebración acompañada por intervenciones artísticas de las comunidades indígenas de la región y varias intervenciones también por parte de **integrantes del ELN que resaltaron la voluntad de paz** y la decisión de llegar a la fase pública de conversaciones. ([Lea también Para negociar es necesario conversar](https://archivo.contagioradio.com/para-negociar-es-necesario-abrir-debates-y-para-eso-se-requiere-a-la-socieda/))

Por otra parte llegó a Bucaramanga la comisión humanitaria que recibirá la excarcelación de Nixon Cobos y Neivis Valero, integrantes del [ELN](https://archivo.contagioradio.com/?s=eln) que sufren graves afecciones de salud y se encontraban recluidos en la cárcel de “Palogordo” en esa ciudad. Según el comunicado \#4 de la delegación de paz del ELN **se trata del cumplimiento de los acuerdos humanitarios que ayudarán a des escalar la intensidad de la guerra** y abrirá las puertas a un debate sobre el cese bilateral de acciones armadas. ([Le puede interesar: Mesa social lista para aportar en la construcción de la paz](https://archivo.contagioradio.com/mesa-social-para-la-paz-busca-incidir-en-conversaciones-eln-gobierno/))

Así mismo esa delegación manifestó que se recibe con entusiasmo la posibilidad de iniciar las **fase pública de conversaciones el próximo 7 de Febrero** y aseguran que la comunidad internacional será una de las garantes del proceso y por ello convocaron a rodearlo para que se consolide la paz en todo el territorio “consolidar a latinoamérica y el caribe como territorio de paz, tal como lo han declarado la CELAC y la UNASUR”.

> Así fue la salida de la cárcel en Girón, Santander de Nixon Cabos y Levis Valero los dos guerrilleros indultados del [@ELN\_RANPAL](https://twitter.com/ELN_RANPAL) [@ELN\_Paz](https://twitter.com/ELN_Paz) [pic.twitter.com/2ViKjnSMgl](https://t.co/2ViKjnSMgl)
>
> — Camilo Chará Guevara (@camilochara) [2 de febrero de 2017](https://twitter.com/camilochara/status/827201561491288065)

<p>
<script src="//platform.twitter.com/widgets.js" async charset="utf-8"></script>
</p>
> [\#AccionesHumanitariasMutuas](https://twitter.com/hashtag/AccionesHumanitariasMutuas?src=hash) Primeras imágenes de nuestros bravos combatientes Nixón y Leivis, que ya están en libertad. ¡Avanza la Paz! [pic.twitter.com/5Sz4x5owNc](https://t.co/5Sz4x5owNc)
>
> — ELN RANPAL (@ELN\_RANPAL) [2 de febrero de 2017](https://twitter.com/ELN_RANPAL/status/827200326600110080)

<p>
<script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>
</p>
###### Reciba toda la información de Contagio Radio en [[su correo]
