Title: Comunidades Wayuu de la Guajira cierran pasos de vía férrea de empresa Cerrejón
Date: 2016-12-21 14:42
Category: Ambiente, Nacional
Tags: Cerrejón, desnutrición, indigenas wayuu
Slug: comunidades-wayuu-de-la-guajira-cierran-pasos-de-via-ferrea-de-empresa-cerrejon
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/12/wayuu.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Las 2Orillas ] 

###### [21 Dic 2016]

Desde las 3 de la tarde de este martes 20 de Diciembre, integrantes de las c**omunidades Wayuu de la Guajira bloquean por lo menos dos puntos de la vía férrea** que sirve a la empresa Cerrejón para el transporte de Carbón. También se han realizado bloqueos intermitentes en la troncal del caribe **exigiendo la investigación por las amenazas, planes de alimentación acordes con las costumbres de ellos y la aplicación de los decretos 2500 y 804.**

Los bloqueos se realizan sobre el Kilómetro 79 a la altura de la comunidad Katsariamana sobre la vía Férrea. **Más de 300 personas de diferentes comunidades se dieron cita en este punto para exigir la presencia del ICBF**. Adicionalmente la troncal del caribe estuvo bloqueada durante algunas horas hasta que se llegó a un acuerdo con la defensoría del pueblo que se comprometió a manifestarse y buscar una respuesta por parte de esa entidad. Le puede interesar: ["No solamente son niños, es todo el pueblo Wayuu el que se está mueriendo"](https://archivo.contagioradio.com/no-solamente-son-ninos-es-todo-el-pueblo-wayuu-el-que-se-esta-muriendo/)

Las comunidades exigen investigación y castigo por el asesinato del profesor indígena Rafael Lubo Aguilar del E'iruku Apshana, quién fue victimizado **“por expresar y desarrollar su intelecto para el fortalecimiento del SEIP**, en cada una de las comunidades educativas del centro etnoeducativo de Caracas Ruleya en el municipio de Manaure”, así como investigación y protección por las recientes amenazas de las que han sido víctimas varios de sus líderes.

También exigen la **puesta en marcha del decreto 2500 que les permite administrar la educación** para la construcción del Sistema de Educación Propio SEIP, y la aplicación del decreto 804 que les concede el derecho de nombramientos de docentes sin concurso.

Adicinalmente expresan que el **ICBF debe aplicar programas de alimentación acordes a las características de sus territorios**, puesto que muchos de los alimentos proporcionados por el Instituto no son acordes con las costumbres alimenticias de las comunidades indígenas o por las condiciones del terreno no se pueden conservar apropiadamente “Los tomates se dañan y los niños no están acostumbrados a comer papa” explica Valbuena.

Se espera que este 22 de Diciembre se realice una movilización a la altura del Kilómetro 12 que de Rioacha conduce a Maicao para reiterar las exigencias y hacer un homenaje a las víctimas de estas comunidades.

###### Reciba toda la información de Contagio Radio en [[su correo]
