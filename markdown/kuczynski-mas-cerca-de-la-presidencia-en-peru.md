Title: Kuczynski más cerca de la presidencia en Perú
Date: 2016-06-06 10:17
Category: El mundo, Política
Tags: Elecciones en Perú, Keiko Fujimori, Pedro Pablo Kuczinsky
Slug: kuczynski-mas-cerca-de-la-presidencia-en-peru
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/06/ONPE050601.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### Foto: ONPE 

##### 6 Jun 2016 

Con el 91% de las actas contabilizadas por la Oficina Nacional de Procesos Electorales ONPE, el candidato de Peruanos por el Kambio Pedro Pablo Kuczynski (PPK), lidera las elecciones con una diferencia ajustada sobre la candidata Keiko Fujimori.

Hasta la mañana de este lunes, las cifras oficiales presentaban un 50.32% de favorabilidad para Kuczynski, mientras que la candidata Fuerza Popular le sigue de cerca con un 49,68% de los votos contabilizados, que al momento representan la voluntad de poco más de 16 millones de peruanos de los 22 millones habilitados para votar.

De acuerdo con la ONPE, de las 77.307 actas en todo el país, faltan procesar unas 5.756 (7,446%), así mismo la entidad anunció que se irán publicando los avances a medida que los centros de cómputo de las oficinas descentralizadas de procesos electorales (ODPE) vayan procesando y contabilizando las actas de escrutinio.

Los resultados oficiales parciales, coinciden con los presentados a boca de urna por encuestadoras la tarde del domingo que daban como virtual ganador a PPK, con un margen reducido que refleja una decisión determinada por el "antifujimorismo" promovido desde diferentes frentes que no quieren se repitan las violaciones y delitos ocurridos durante los 10 años que gobernó el país.
