Title: Atentado contra autobús en Pakistán deja 45 muertos y 30 heridos
Date: 2015-05-14 16:15
Author: CtgAdm
Category: El mundo, Otra Mirada
Tags: Atentado contra autobús de minoría Chií deja 45 muertos en Pakistán, atentado en Pakistán deja 45 muertos y 30 heridos, Estado Islámico reivindica atentado en Pakistán contra Ismaelíes, Karachi, Karachi atentado contra autobús, Minoría Ismaelí Pakistán
Slug: atentado-contra-autobus-en-pakistan-deja-45-muertos-y-30-heridos
Status: published

###### Foto:Info7.mx 

El atentado ocurrió en la ciudad de **Karachi**, cuando más de **diez hombres en motocicleta dispararon contra el autobús** que transportaba miembros de la **minoría Chií ismaelita.**

**Estado Islámico, y otros grupos insurgentes suníes y talibanes** se han atribuido el atentado. Dichos grupos llevaban ya más de dos años amenazando a la minoría y más de una década a las autoridades Pakistaníes. Según **Ayesha Aly**, portavoz de la comunidad Ismaelí “… el autobús hacía su ruta habitual cuando ha sucedido el ataque. La **gente simplemente hacía su vida normal, yendo a ganarse la vida…”.**

La **primera reivindicación** del atentado ha sido del **Estado Islámico** en un comunicado afirmaba “…Gracias a Dios, 43 apóstatas han resultado muertos y casi otros 30 heridos en un ataque de los soldados del Estado Islámico contra un autobús que transportaba a gente de la rama chií ismaelí… en Karachi…”.

Más tarde el **Ejército de Dios (Jundullah**) también emitía un comunicado “…Los muertos eran Ismaelíes y les consideramos *kafir* (infieles). Hemos empleado cuatro atacantes. En los próximos días atacaremos a ismaelíes, chiíes y cristianos…”.

Este último grupo pertenece a la rama extremista suní y tiene vínculos con los Talibanes de Afganistán, las autoridades de Pakistán creen que **ha jurado lealtad al EI**, de tal manera podría ser un atentado coordinado conjuntamente.

**Karachi** es la ciudad **portuaria más popular de Pakistán**, con 20 millones de habitantes, es **blanco habitual de la violencia sectaria** entre distintas ramas islamistas y entre los partidos que se disputan el poder, desde la invasión de Afganistán la **violencia** se ha acrecentado llegando en los dos últimos años a más de **2000 personas muertas.**

La minoría **Ismaelí constituye el 20% de la población de Pakistán**, otras minorías como la cristiana o los sijs forman el 5%, todas ellas están siendo blanco de la **violencia sectaria** que azota al país.
