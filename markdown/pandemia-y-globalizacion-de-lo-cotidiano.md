Title: Pandemia y globalización de lo cotidiano
Date: 2020-07-21 20:17
Author: Mision Salud
Category: Columnistas invitados, Misión Salud, Opinion
Tags: covid19, dinero para pandemia, globalizacion, pandemia, pandemia Estados Unidos guerra Vietnam Colombia Covid 19
Slug: pandemia-y-globalizacion-de-lo-cotidiano
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/07/pandemia-vs-globalización-scaled-1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:heading {"align":"right","level":6,"textColor":"cyan-bluish-gray"} -->

###### Foto de AgendAR {#foto-de-agendar .has-cyan-bluish-gray-color .has-text-color .has-text-align-right}

<!-- /wp:heading -->

<!-- wp:heading {"level":6} -->

###### Por [Jennifer Bueno](https://www.linkedin.com/in/jennifer-bueno-35667135/) de Misión Salud

<!-- /wp:heading -->

<!-- wp:paragraph -->

Hoy más que nunca en la historia se tiene la oportunidad de evidenciar cómo la política global determina la forma en que las personas en todos los países del mundo transforman sus rutinas diarias y acciones cotidianas de acuerdo con las decisiones que se toman en instancias internacionales globales y regionales como las Naciones Unidas, la Organización Mundial de la Salud, la Organización mundial del Comercio y la Organización Panamericana de la Salud, entre otras.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Si bien esta situación sería la esperada bajo una dinámica global ideal que responda al espíritu de la creación de estas instancias\*, son de conocimiento general [las dificultades que atraviesan en términos de independencia y de intervención de poderes privados en ellas](https://www.mision-salud.org/actualidad/2709/). 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Esto implica que, todas las directrices que están siendo impuestas a todas las naciones del mundo de manera directa o indirecta no solo responden a los intereses colectivos de la humanidad, sino que en ellas están incrustados otros intereses particulares de individuos con el poder suficiente para hacerlo.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Con esta reflexión se pretende aclarar y resaltar las líneas que hasta hoy han permanecido borrosas a la mayoría de la población (por diferentes motivos) y que conectan los altos niveles comerciales y diplomáticos globales con la cotidianidad y realidad de una persona en Colombia, ya esté en el amazonas, los llanos o en la capital del país.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En nuestras siguientes entregas estaremos analizando la evolución de varios de estos fenómenos, por ejemplo el uso de tapabocas, la digitalización de la vida, la automedicación, etc.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### El caso de los tapabocas

<!-- /wp:heading -->

<!-- wp:paragraph -->

Cuando se anunció que la epidemia de COVID-19 había pasado al estatus de “pandemia” diversas recomendaciones sanitarias de autocuidado empezaron a generarse. Fuentes “oficiales” y “no oficiales” inundaron los medios de comunicación con las indicaciones que las personas de todo el planeta deberían seguir para prevenir el contagio y reducir la transmisión del virus SARS-CoV-2. En ese momento las voces oficiales insistían en que únicamente el personal sanitario requería uso de tapabocas o mascarillas. En un comunicado publicado el 29 de enero de 2020 por la Organización Mundial de la Salud, se compartían las directrices “provisionales” para el uso de tapabocas en el contexto COVID19. 

<!-- /wp:paragraph -->

<!-- wp:heading {"level":6} -->

###### ![](https://lh4.googleusercontent.com/rLlga4Ev1AjZLFWPYNPE3XVRRRbMJv8ykjD65hZQgTFB5S2xdGKkCpCreNioWFcs2_mpufId_aVUJMikQ81S7YIgYqGxLk8r-BTTD7tSpDuFdV7a-0yv7MsUJRUjNUMsxf4biSTUZ868dSWC9Q){width="572" height="182.9996621608734"}Tomado de [OMS, 2020](https://apps.who.int/iris/bitstream/handle/10665/330999/WHO-nCov-IPC_Masks-2020.1-spa.pdf)

<!-- /wp:heading -->

<!-- wp:paragraph -->

En estas directrices el término “provisionales” indica que aún la máxima autoridad en materia sanitaria en el mundo no tenía (ni tiene) claridad sobre los mecanismos de transmisión del virus y preveía transformaciones en las indicaciones (sigue haciéndolo). A estas directrices se alinearon (como debe ser) la Organización Panamericana de la Salud, y en Colombia el Ministerio de Salud y Protección Social, la Alcaldía Mayor de Bogotá.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":6} -->

###### ![](https://lh6.googleusercontent.com/0PGMku0WSNXosEiegRYZQdRL1BScl3eLRmZF4ARJtfsqcCsLNvXB4goSIQ-w7vfWfiC9CR6NpcY2D5c-CD6zCJb0zclUvvdNES5Ya2FSxDcTLWGnPmqbbt8NmEX0fj3XJYp_QtSUDPCeDVfjjQ){width="440" height="247"}

<!-- /wp:heading -->

<!-- wp:heading {"level":6} -->

###### Tomado de página de la [Alcaldía de Bogotá Marzo 10 , 2020](https://bogota.gov.co/mi-ciudad/salud/coronavirus-cuando-y-como-usar-tapabocas)

<!-- /wp:heading -->

<!-- wp:heading {"level":6} -->

###### ![](https://lh5.googleusercontent.com/_0vU1fXG8gEoByu9YUqYzo_nKcdDMAbbY4mrTR8yl4mYlFxcbseJBqxvjhqzSRDsnQqZMrWXovgtjIgSLJ2VlVUqbDI4rbhkBbK6qW6uz7-MzxIsegZJ5se3TnDnidPtIpjCW_DP7ewlF6N_lA){width="624" height="108"}Tomado de [Organización Panamericana de la Salud](https://www.paho.org/hq/index.php?option=com_content&view=article&id=15744:face-masks-during-outbreaks-who-when-where-and-how-to-use-them&Itemid=1926&lang=es), Febrero 28, 2020

<!-- /wp:heading -->

<!-- wp:heading {"level":6} -->

###### ![](https://lh6.googleusercontent.com/VQi0qQN3yIaK4uqWQyaFGzh-32ckvwIAnCvYt2vsFyjF3Fbjkvfro5gDY6a762_MJ4GbEaRSHxYRkcc8v0v7llh48acjHHJPCStJM-EFZUZdIW0P3BvJu3N3IHoqaiRhjRpzMgkESTyKr08G3Q){width="624" height="165"}Tomado de diario [El País (Uruguay), Marzo 31 de 2020](https://www.elpais.com.uy/mundo/dijo-oms-tapabocas-insiste-todos-usen.html)

<!-- /wp:heading -->

<!-- wp:paragraph -->

De forma paralela a estos anuncios ‘oficiales’ o ‘institucionales’, imágenes de China e Italia, donde toda la población usa mascarilla sin distinción, inundaban los medios de comunicación globales. Audios de WhatsApp y publicaciones en redes sociales (principalmente Facebook) desafiaban y/o desmentían la recomendación de que sólo el personal sanitario o aquellas personas con cuadros gripales o en contacto con ellas y a partir del miedo, recomendaban adquirir y usar tapabocas, ojalá los más especializados (por ejemplo mascarillas N95). Tanto impacto han tenido estas desinformaciones que las autoridades han tenido que solicitar a las personas no comprar mascarillas especializadas pues se podría causar el desabastecimiento para los profesionales de la salud que realmente las necesitan.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Mientras la población se dividía, confundida, entre quienes aceptaban las directrices y quienes no y quienes tenían los medios para adquirir estos insumos y los que no, entre finales de marzo e inicios de abril la evidencia volvía a transformar las recomendaciones y rápidamente el uso de las tapabocas [se volvió obligatorio](https://www.minsalud.gov.co/Paginas/El-uso-de-tapabocas-se-hace-obligatorio-en-el-sistema-de-transporte-publico.aspx) (o altamente recomendado) para personas sanas en prácticamente todo el planeta.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":6} -->

###### ![](https://lh6.googleusercontent.com/lHZqVoUWndaDlsBYJfPYLvZJB-e7gtJbUf_YL-LBIwZFSYcRXlCvO7o7KXFjWCogWoYWsQu5VwFoJCWWQMv8nodUSwbJy5n68ZqzIFoWd9MY5KH9ooeTAERIbcVmILZIGnYkzdHm2iUWYPY8xA){width="624" height="315"}[El Espectador (Colombia)](https://www.elespectador.com/coronavirus/oms-recomienda-el-uso-de-tapabocas-para-todas-las-personas-articulo-913002/), 4 de abril de 2020

<!-- /wp:heading -->

<!-- wp:heading {"level":6} -->

###### ![](https://docs.google.com/drawings/u/2/d/stAV_Rz7FR5yWUAbmN5rkHQ/image?w=534&h=489&rev=1&ac=1&parent=1u__eEL7YyGdXns1r02kHhuNd875Cr05jtGtdNQxNGA8){width="534" height="489"}

<!-- /wp:heading -->

<!-- wp:heading {"level":6} -->

###### Tomado de Asociación Colombiana de Infectología, [Abril 2020](https://www.acin.org/images/guias/coronavirus/Comunicado_8.pdf)

<!-- /wp:heading -->

<!-- wp:paragraph -->

![](https://docs.google.com/drawings/u/2/d/simZnziBdvcu1RaJse2piUA/image?w=544&h=294&rev=1&ac=1&parent=1u__eEL7YyGdXns1r02kHhuNd875Cr05jtGtdNQxNGA8){width="544" height="294"}

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

A la fecha organizaciones dedicadas al análisis de evidencia biomédica destacan que el uso de tapabocas puede prevenir la transmisión de coronavirus, y que [la protección ocular puede proporcionar beneficios adicionales.](https://evidenceaid.org/resource/face-masks-to-stop-the-transmission-of-respiratory-viruses-from-infected-people/) El Presidente Donald Trump opositor inicial del uso del tapabocas en Estados Unidos, realizó unas declaraciones usándolo por primera vez hace una semana, pero manifestó no estar de acuerdo con el uso obligatorio argumentando que la población [debe tener derecho a ciertas libertades.](https://www.bbc.com/news/world-us-canada-53453468)

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Mientras esto sucedía, todas las personas empezamos a ser bombardeados virtualmente con anuncios y publicidades de compra de insumos sanitarios como gel antibacterial, guantes y tapabocas. En el correo electrónico, notificaciones móviles, publicidades automáticas en las páginas de internet, en redes sociales. 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Y es ahí donde el mercado entra a transformar la cotidianidad. Un insumo que se declara como esencial para desacelerar la propagación de un patógeno que tiene colapsada a la sociedad, altamente contagioso y perjudicial para la salud humana se transforma en un producto de alto consumo, también en un accesorio, los hay de múltiples colores, marcas, materiales, de fabricación nacional, importados, etc.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":6} -->

###### ![](https://lh6.googleusercontent.com/fNcvfEpQNHXouqfIApg9pepEftJNEqmqPqOebme5HphsFpl-FQR0RtyHQ8LcB5GHAYHo9WnnMkL9b4F2KDbBI2etwtn6h_OYtOZMC3DuhsdWswEp2J61naQcL7tiNel57hsIaX2JQz2C9CPIxA){width="514" height="109"}

<!-- /wp:heading -->

<!-- wp:heading {"level":6} -->

###### \[Español: Chanel produce mascarillas para ayudar a combatir el coronavirus\] [Tomado de revista Bazaar, marzo 30 de 2020](https://www.harpersbazaar.com/uk/fashion/fashion-news/a31973471/chanel-launches-face-masks/)

<!-- /wp:heading -->

<!-- wp:paragraph -->

A nivel nacional e internacional el acceso y uso de los tapabocas está mutando a ser no solo un tema de salud pública sino también un problema de mercado. 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Una “fiebre del oro” alrededor de los implementos de protección se instauró en el país y no solo para aquellos que llevan años establecidos en dicho mercado sino también para pequeños comerciantes que ante la inminente quiebra de sus negocios también decidieron participar.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Dada la cantidad de productores en los mercados formal e informal, podría lanzarse la hipótesis de que en este momento no es posible para ninguna agencia sanitaria en el mundo garantizar que el tapabocas que se está usando cumple con los estándares necesarios para cumplir su función protectora. 

<!-- /wp:paragraph -->

<!-- wp:heading {"level":6} -->

###### ![](https://docs.google.com/drawings/u/2/d/sT4mugJyrgGJJer8HohR6EA/image?w=343&h=244&rev=1&ac=1&parent=1u__eEL7YyGdXns1r02kHhuNd875Cr05jtGtdNQxNGA8){width="343" height="244"}

<!-- /wp:heading -->

<!-- wp:heading {"level":6} -->

###### Tomado de diario [La República, abril 1 de 2020](https://www.larepublica.co/empresas/las-pyme-se-apersonan-del-negocio-de-los-tapabocas-en-el-pais-2986174)

<!-- /wp:heading -->

<!-- wp:paragraph -->

La situación es compleja. Sin embargo, tener claro qué es lo que sucede en el mercado de este tipo de implementos permite no solo tomar decisiones más seguras de compra sino también preguntarse por cómo es la dinámica de otros insumos para la salud que son aún más impactantes (medicamentos, respiradores, vacunas, etc.)

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por otro lado, es necesario que todas las personas hagamos conciencia de que esta situación desafía todas las instancias del saber humano. Tenemos en el mundo expertos que han estudiado durante años este tipo de virus, ellos mismos no tienen claridad sobre sus mecanismos y por lo mismo no tienen claridad sobre cómo abordarlo. Nos encontramos entonces en una carrera en terreno desconocido, si aquellas personas expertas están aterrizadas en la incertidumbre actual, el resto de nosotros deberíamos hacerlo. 

<!-- /wp:paragraph -->

<!-- wp:heading {"level":6} -->

###### \*Esto es donde dichas instancias sólo responden a los intereses de las naciones que las integran y son financiadas de manera equitativa por ellas, velando por el respeto de los derechos humanos y la dignidad de las personas independiente de su país de origen.

<!-- /wp:heading -->

<!-- wp:heading {"level":6,"textColor":"vivid-cyan-blue"} -->

###### [Vea mas: Nuestros Columnistas](https://archivo.contagioradio.com/opinion/columnistas/) {#vea-mas-nuestros-columnistas .has-vivid-cyan-blue-color .has-text-color}

<!-- /wp:heading -->

<!-- wp:separator -->

------------------------------------------------------------------------

<!-- /wp:separator -->

</p>
<!-- wp:heading {"level":6} -->

###### Las opiniones expresadas en este artículo son de exclusiva responsabilidad del autor y no necesariamente representan la opinión de Contagio Radio.

<!-- /wp:heading -->
