Title: Unidad de Búsqueda comienza piloto de identificación de 2.100 cuerpos en Norte de Santander y en Nariño
Date: 2019-07-16 11:34
Author: CtgAdm
Category: Nacional, Paz
Tags: Medicina Legal, nariño, Norte de Santander, Unidad de Búsqueda de personas dadas por desaparecidas
Slug: unidad-busqueda-piloto-narino-norte-santander
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/D_hlNnsW4AMQ6Yt.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:] 

En una labor conjunta, la Unidad de Búsqueda de Personas dadas por Desaparecidas (UBDP), Medicina Legal y la Fiscalía comenzarán a desarrollar un plan piloto que realizará la **identificación de 1.600 cuerpos en Norte de Santander y 500 en Nariño** ,con el objetivo de reconocer los problemas que podrían afectar el proceso de reconocimiento y preparar estrategias de trabajo que puedan ser aplicadas en este campo a futuro.

**Según Luz Marina Monzón, directora de la UBPD,** se han seleccionado a Norte de Santander y Nariño porque cuentan con una participación activa de organizaciones de víctimas que vienen trabajando en la búsqueda de desaparecidos y que pueden aportar mayor información en el proceso, lo que permitirá además una mayor participación de los familiares durante la búsqueda e identificación.

"No sabemos cuáles son los familiares de esos cuerpos, una vez identifiquemos los periodos, lugares y la situación, convocaremos a las organizaciones y familiares para determinar si podrían corresponder a esta búsqueda" señaló la directora de la UBPD, quien explicó que el piloto tendrá una duración de tres meses en Norte de Santander y dos en Nariño.

Parte del desafío de este plan piloto e**s recavar datos que permitan complementar información de cuerpos que datan de 1960 a 2006 de los que no se cuenta con suficiente información** para así agilizar la búsqueda, retomar procesos de búsqueda y responder a quienes continúan esperando nueva información sobre sus seres queridos.

La prueba estará conformada por equipos de antropólogos, odontólogos, genetistas y técnicos en criminalística que comenzarán a investigar los cuerpos de personas que posiblemente desaparecieron durante el conflicto armado y que pese a pasar por un proceso de necropsia aún no han sido identificados. [(Lea también: Pese a recortes presupuestales la Unidad de Búsqueda de Desaparecidos no se detiene)](https://archivo.contagioradio.com/pese-a-recortes-presupuestales-la-unidad-de-busqueda-de-desaparecidos-no-se-detiene/)

La prueba piloto arrojará varios resultados que determinarán la situación del proceso de identificación, para así pasar a una segunda fase que permitirá establecer estrategias a nivel nacional que deben ser implementadas para complementar la información recolectada.  [(Le puede interesar: En 2019 Unidad de Búsqueda de Desaparecidos llegará a 17 territorios)](https://archivo.contagioradio.com/unidad-de-busqueda-de-desaparecidos/)

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>  
<iframe id="audio_38538157" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_38538157_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
