Title: El Estado fue tomado por "corruptelas políticas"
Date: 2017-02-08 14:49
Category: Nacional, Política
Tags: corrupción, Juan Manuel Santos, Odebrecht, partidos politicos
Slug: corrupcion-penetro-a-todo-el-estado
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/02/corrupción-en-México.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: ndsnoticias] 

###### [8 Feb. 2017] 

Parece ser que nadie se salva de haber recibido dinero de Odebrecht, este martes el Fiscal General, Néstor Humberto Martínez dijo que la campaña presidencial de Juan Manuel Santos del año 2014 habría aceptado US\$ 1  millón de dólares. Sin embargo, **en un accionar contradictorio, este miércoles Martínez sorprendió con otra versión** en la que asegura que la “Fiscalía carece de pruebas que vinculen al primer mandatario con dineros de Odebrecht”.

En un principio la Fiscalía aseguraba que el ex senador Otto Bula, del Centro Democrático, les había confesado que él realizó la monetización de un millón de dólares en Colombia a través de una empresa, trabajo por el que recibió 200 millones de pesos, **dinero del cual se le pagó el 10 % a un empresario en Colombia que sería el donante a la campaña Santos**.

Para Carlos Velandia, gestor de paz, por estos escándalos de corrupción el país se encuentra ante una situación compleja y difícil “incluso mucho más grave que la misma confrontación bélica (…) **el fenómeno de la corrupción ha penetrado la totalidad del Estado, aquí no se salva nadie”. **Le puede interesar: [La corrupción, un obstáculo para la Paz en Colombia](https://archivo.contagioradio.com/la-corrupcion-un-obstaculo-para-la-paz-en-colombia/)

Y es que por el anuncio hecho por el Fiscal deberá responder Roberto Prieto, gerente de la campaña de esa época, quien ha asegurado que las declaraciones de Otto son “infundadas y tendenciosas” y añadió que durante su trabajo con Juan Manuel Santos fue uno de los promotores de no recibir financiación de empresas.

Asegura Velandia que “los últimos gobiernos han ingresado a sus campañas dineros calientes, dineros de la corrupción por la puerta de atrás y muy seguramente todas estas campañas y sus **candidatos terminarán diciendo que si ‘entraron, entraron a mis espaldas’**, como ocurrió con el proceso 8.000”.

Velandia no da muchas esperanzas frente a esta situación de corrupción en Colombia, y por el contrario manifiesta de manera contundente que nadie va a responder y que **más de uno dirá** “**no nos dimos cuenta, no vimos y terminarán echándole la culpa al gerente de la campaña”.**

Congresistas como Viviane Morales del Partido Liberal, Claudia López del Partido Verde, José Obdulio Gaviria del Centro Democrático y Alexander López del Polo Democrático, han insistido en afirmar que si se comprueba esa financiación a la campaña del Presidente, éste debería renunciar.

Algunos analistas señalan que las acusaciones podrían ser falsas y buscarían favorecer al actual vicepresidente **Germán Vargas Lleras,** que se ha perfilado como candidato a la presidencia y quien **no ha sido duramente juzgado por todos los escándalos de corrupción del partido político Cambio Radical. **Le puede interesar: [El riesgo de los acuerdos de paz en elecciones presidenciales 2018](https://archivo.contagioradio.com/el-riesgo-de-los-acuerdos-de-paz-en-elecciones-presidenciales-2018/)

**“Esto lo que demuestra es que estamos ante un Estado tomado por las corruptelas políticas** y eso es lo que el ciudadano del común tiene que examinar en el momento de tomar opción política por un partido, por un candidato, tiene que mirar su pasado ¿qué es lo que han venido haciendo y qué es lo que ofrecen?”.

Según Velandia **todos los partidos políticos e instituciones están contaminados y “son una tracamanada de ladrones**, son una cueva del Rolando esos partidos”. Le puede interesar:[ Los escándalos de corrupción que salpican al Centro Democrático](https://archivo.contagioradio.com/odebrecht-pago-por-servicios-de-publicista-para-la-campana-de-oscar-ivan-zuluaga/)

Además para el gestor de paz, la actitud de juzgarse “los mismos con los mismos” no habla muy bien de los partidos “por ejemplo **el Centro Democrático nombra una comisión de ética de bolsillo, que antes de constituirse ya exonero al candidato Óscar Iván Zuluaga** de tener que ver con los dinero de Odebrecht”.

Por último, Velandia hace hincapié en que el ciudadano no puede seguir cayendo en la **irresponsabilidad de vender su voto por un tamal, una teja o un bulto de cemento** “por eso estamos como estamos”. Le puede interesar: [Colombia tiene un sistema electoral que permite elegir criminales: MOE](https://archivo.contagioradio.com/sistema-electoral-elegir-criminales-35907/)

<iframe id="audio_16906470" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_16906470_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio]{.s1}](http://bit.ly/1ICYhVU)[.]{.s1} {#reciba-toda-la-información-de-contagio-radio-en-su-correo-o-escúchela-de-lunes-a-viernes-de-8-a-10-de-la-mañana-en-otra-mirada-por-contagio-radio. .p1}
