Title: Marcha Carnaval en Ibagué, en defensa de la vida y en contra de Anglo Gold Ashanti
Date: 2015-06-05 13:05
Category: Ambiente, Nacional
Tags: Ambiente, Anglo Gold, ecosistema, Ibagué, La Colosa, marcha, Movilización, Valencia
Slug: marcha-carnaval-en-ibague-en-defensa-del-ecosistema-y-en-contra-de-anglo-gold
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/06/11083647_10205384754354939_1008574892600074472_n.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### <iframe src="http://www.ivoox.com/player_ek_4601428_2_1.html?data=lZudk5mWfI6ZmKiakp2Jd6KnmpKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRkcLmxM3OjcjFts%2FV18bZj4qbh4630NPhw8zNs4zGwsnW0ZCRaZi3jpk%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe> 

##### [Renzo García, miembro del Comité ambiental del Tolima] 

Desde las 2:30 p.m del día de hoy se llevará a cabo la **séptima versión de la Marcha Carnaval en defensa del agua, la vida y el territorio,** y en rechazo directo al proyecto LA COLOSA, de la multinacional Anglo Gold Ashanti en Ibagué, Tolima. El Sena será el punto de encuentro; en la calle 42 con Av. Ferrocarril, para dirigirse al parque Murillo Toro, frente a la gobernación del Tolima.

Ante la **presencia** de la empresa **Anglo Gold Ashanti, en el territorio tolimense de Cajamarca**, por medio del **proyecto minero La Colosa;** campesinos, campesinas y organizaciones se movilizarán de manera pacífica en contra de este proyecto que ocasionaría un **impacto a la biodiversidad, al ecosistema y a dinámicas socioproductivas**, afirma Renzo García, miembro del comité ambiental del Tolima.

**Con cánticos y obras artísticas**, esta movilización recorrerá las calles de Ibagué como **crítica a las declaraciones que la senadora Paloma Valencia** quien aseguró por medio de su cuenta de twitter, que “las FARC están presionando a la ciudadanía para obligarlos a marchar en contra de La Colosa”.

García asegura que estas **estigmatizaciones ponen en riesgo la vida del campesinado que se oponen al proyecto**,  y de las cuales algunos medios de comunicación son partícipes, pues Anglo Gold habría financiado algunas de sus pautas publicitarias “cambiando el derecho a la información y convirtiéndose en propaganda unilateral que favorece el interés de la multinacional”.

De esta manera, hacen la **invitación a toda la población de Ibagué** y sectores aledaños a que **participen en defensa del agua, los recursos naturales y la tierra**, ya que el proyecto La Colosa realizaría una –**lixiliación**-, (mecanismo con el cual se necesitan grandes cantidades de agua mezcladas con cianuro) para atrapar el oro que se posa en las rocas, metodología que genera **graves daños ambientales.** “Las montañas desaparecerían y quedarían canteras a cielo abierto que tienen 1, 2, o 3 kilómetros de diámetro y unos 8.000 mts de profundidad”, enfatiza el ambientalista.
