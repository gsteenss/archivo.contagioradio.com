Title: Elecciones atípicas en Tumaco afrontan “Riesgo Extremo”
Date: 2017-04-22 16:54
Category: Política
Slug: elecciones-atipicas-tumaco
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/04/elecciones-tumaco.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: hbs] 

###### [22 Abr 2017]

La presencia de por lo menos 10 estructuras paramilitares en la región, de insurgencia del ELN, de bandas dedicadas al narcotráfico y la minería ilegal son algunas de las razones por las cuales la Misión de Observación Electoral, MOE, califica como **“riesgo extremo” para la realización de las elecciones atípicas de este 23 de Abril en Tumaco**, luego de la destitución en Enero de María Emilsen Angulo, alcaldesa electa en Octubre de 2016.

Según lo manifiesta la MOE, la elección de **Tumaco como jurisdicción especial y municipio piloto para la implementación del acuerdo de paz y la existencia de una Zona Veredal**, son dos de las características especiales de las votaciones atípicas y por ello también han decidido no hacer observación con personas locales solamente, sino que movilizarán allí observadores regionales para supervisar a situación. ([Lea también: Farc denuncian presencia de 10 grupos paramilitares en Tumaco](https://archivo.contagioradio.com/paramilitares-tumaco/))

Para esta jornada la Registraduría ha destinado 15 puestos de votación en la zona urbana y 384 mesas en las zonas rurales del municipio. Sin embargo, **las mesas ubicadas en Llorente,** epicentro de recientes protestas por los incumplimientos del acuerdo sobre sustitución de cultivos de uso ilícito, **son para la MOE, situaciones que merecen toda la atención** para la observación del trascurso de la jornada. ([Le puede interesar: campesinos persisten en protestas en zona rural de Tumaco](https://archivo.contagioradio.com/gobierno-prefiere-erradicacion-forzada-sobre-sustitucion-de-cultivos/))

### **Elecciones atípicas han costado 10.300 millones de pesos** 

Según informó la MOE se han realizado 29 elecciones atípicas desde las jornada electoral de Octubre de 2015, y le han costado al país cerca de 10.300 millones de pesos entre diciembre de 2015 y Abril de 2017, lo que implica un costo muy alto dado que situaciones como estas podrían prevenirse. ([Lea también: Colombia tiene un sistema electoral que permite elegir criminales](https://archivo.contagioradio.com/sistema-electoral-elegir-criminales-35907/))

En otra entrevista a Contagio Radio, Alejandra Barrios afirmó que este tipo de situaciones son las que dan cuenta de la necesidad de una reforma que ponga **más controles y sanciones a los partidos políticos que dan avales a candidatos y candidatas que tienen investigaciones abiertas** o que están acusados de delitos. Si no se permite ese aval no sería necesario realizar otro gasto en otras jornadas electorales.

###### Reciba toda la información de Contagio Radio en [[su correo]
