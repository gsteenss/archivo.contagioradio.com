Title: 3.200 pacientes con cáncer se beneficiarán con la reducción del costo del Imatinib
Date: 2016-06-10 13:27
Category: DDHH, Nacional
Tags: Imatinib en Colombia, Misión Salud, Reducción precio Imatinib en Colombia, Tratamiento del cáncer en Colombia
Slug: 3-200-pacientes-con-cancer-se-beneficiaran-con-la-reduccion-del-costo-del-imatinib
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/06/Imatinib.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Hipertextual ] 

###### [10 Junio 2016 ] 

Dieciocho meses estuvieron trabajando arduamente las organizaciones 'Misión Salud', IFARMA y el 'Centro de Información de Medicamentos de la Universidad Nacional' para que MinSalud declarara de interés público el medicamento 'Imatinib'. Pese a que junto a la medida también se solicitaba una Licencia Obligatoria que permita producir fármacos genéricos para tratar el cáncer, la decisión del Ministerio constituye un avance en la garantía de derechos de los **3.200 pacientes con leucemia en los que el sistema de salud debe invertir \$48 millones anuales por cada uno. **

Una vez el Ministerio de salud emita la resolución, será la 'Comisión Nacional de Precios de Medicamentos' la entidad que fijará autonoma y unilateralmente el precio máximo al que la multinacional Novartis podrá cobrar el medicamento. **Se espera que haya una reducción de por lo menos 50%**.

Por tratarse de una decisión que compromete una gran cantidad de dinero, durante estos meses **Novartis apeló a distintos argumentos para poner freno a la declaratoria de interés público**, de acuerdo con el doctor Germán Holguín, la multinacional buscó el apoyo de organizaciones con las que tiene relación comercial en Europa, Estados Unidos y Colombia, entre ellas AFIDRO; incluso logró que un funcionario del Senado estadounidense presionara a la embajada de Colombia en Washington, para que la financiación del proceso de paz dependiera de la decisión del Ministerio de Salud.

De acuerdo con el doctor, sí fuera otorgada la Licencia de Obligatoriedad, se estimularía la competencia para la producción de medicamentos genéricos para el tratamiento del cáncer a costos más accesibles y se podrían beneficiar más pacientes. Esta situación ocurre en la India en dónde negaron la patente del 'Glivec' por carecer de novedad y altura inventiva y hoy por hoy **el costo del tratamiento es veinte veces menor que en Colombia**.

Holguín asevera que la figura de interés púbico de los medicamentos, **es un derecho que tienen todos los países que hacen parte de la 'Organización Mundial del Comercio'** y que en nuestro país ha sido pocas veces utilizada. El primer intento hizo con el medicamento 'Caletra', que no fue declarado ni de interés público ni con Licencia de Obligatoriedad; sin embargo, logró que se estableciera un control de precio.

<iframe src="http://co.ivoox.com/es/player_ej_11857233_2_1.html?data=kpall5yWd5Shhpywj5aaaZS1kp2ah5yncZOhhpywj5WRaZi3jpWah5yncajZ09KSpZiJhZLijK3czszZaaSnhqax0JCRb67d1M6SpZiJhpTijLjOztrIcYarpJKw0dPYpcjd0JC_w8nNs4yhhpywj5k.&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)] ] 
