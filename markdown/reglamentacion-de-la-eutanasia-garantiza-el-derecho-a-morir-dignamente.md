Title: Reglamentación de la eutanasia garantiza el derecho a morir dignamente
Date: 2015-04-24 18:38
Author: CtgAdm
Category: DDHH, Nacional
Tags: Asociación de Ateos de Bogotá, Eutanasia en Colombia, Procurador General de la Nación
Slug: reglamentacion-de-la-eutanasia-garantiza-el-derecho-a-morir-dignamente
Status: published

###### Foto: [www.reporternuovo.it]

<iframe src="http://www.ivoox.com/player_ek_4403905_2_1.html?data=lZmdlZ6UeY6ZmKiakp6Jd6KnmJKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRic%2Fo08rjy9jYpYy6xtfbx96PltDY04qwlYqliMjpxt%2Bah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Ferney Rodríguez, Asociación de ateos de Bogotá] 

#### *Pesé a la oposición de la Iglesia y el Procurador, sigue en firme reglamentación de la eutanasia en Colombia.* 

Luego de 18 años de espera y de un sinnúmero debates jurídicos, ideológicos, políticos y éticos, el Ministerio de Salud colombiano reglamento la aplicación de la Eutanasia para dar “muerte digna” a pacientes con enfermedades terminales en el país, resolución aprobada por la Corte Constitucional en la **sentencia C239 de 1997**.

**¿ A quienes?**

- Pacientes certificados por especialistas médicos en etapa terminal de su enfermedad.

- Mayores de edad.

- Deben manifestar su voluntad expresa de someterse al procedimiento eutanásico.

**Sobre la eutanasia en Colombia:**

- Consinsiste en darle una droga al paciente terminal para que muera dignamente.

- Será prestada por la Entidades Prestadoras de Salud (EPS) **de manera gratuita**.

**-** El proceso **no aplicará para enfermedades degenerativas**.

**Procedimiento.**

- El paciente debe manifestar a su médico el deseo de aplicar al procedimiento de Eutanasia.

- El especialista deberá presentarle al enfermo **todas las opciones y alternativas terapéuticas posibles** a las que puede someterse para tratar su enfermedad.

- La decisión será determinada por un comité conformado por **un médico especialista, un psiquiatra o psicólogo clínico y un abogado**, en un término de 10 días o en un plazo establecido por el paciente de acuerdo con un protocolo establecido por declaración.

-Si el comité decide que se debe aplicar este derecho, nuevamente se le pregunta al paciente si está seguro; si la respuesta es afirmativa, el hospital tiene un plazo de **15 días** para efectuar el protocolo médico determinado por el Ministerio.

Aunque la resolución contempla la posibilidad de objeción de conciencia particular para los profesionales de la salud que no deseen aplicar el procedimiento, el Ministerio afirma que no existe objeción de conciencia institucional, contrario al llamado que se hace desde diferentes instituciones vinculadas con posturas religiosas.

A esto se suma la misiva enviada por el Procurador General de la Nación, Alejandro Ordoñez, en la que advertía al Ministro Alejandro Gaviria que si cumplía con el fallo “estaría actuando por fuera de sus competencias” y tocando un tema que le “corresponde exclusivamente al legislador”, intervención que, por el momento, no ha cumplido el cometido de frenar la decisión.

En entrevista con Contagio Radio, Ferney Rodríguez, vocero de la Asociación de Ateos de Bogotá afirma que **“Independiente que el Procurador tenga su religión no significa que él tiene que trabajar con el catecismo bajo el brazo** sino es con la Constitución entendiendo que Colombia es un estado de derecho y un estado laico y no un Estado Confesional**”** en relación con las “trabas” que el funcionario ha puesto en este y en otros temas que entran en contradicción con sus creencias religiosas.

Ante el llamado de Ordoñez y de la iglesia de anteponer el derecho a la vida, Rodríguez asegura que *“no estamos hablando que esto es un asesinato, entendiendo que la muerte es parte de todos nosotros, y es importante que cada uno pueda decidir si desea optar por este camino, no se trata simplemente de vivir por vivir sino de no hacerlo bajo unas condiciones muy lastimeras como se tienen en muchos casos de enfermedades terminales*”.

Teniendo en cuenta además que gran parte de la población colombiana profesa la religión católica, misma que ha puesto férrea oposición a la legislación en estos casos, la implementación de la eutanasia en el país requerirá de un proceso de asimilación “*es importante hacer pedagogía a esto, decirle a los ciudadanos que de eso se trata la laicidad estatal, el hecho que la política y la religión están separados***”** señala Rodríguez.

Algunos vacíos de la reglamentación relacionados con la aplicación del procedimiento en menores de edad en casos de extremo sufrimiento, o para aquellas personas que por alguna circunstancia no pudieron expresar su voluntad de morir y su familia desee intervenir para evitar un padecimiento mayor, deben ser considerados aún por la decisión ministerial, sin embargo el vocero de la Asociación de Ateos de Bogotá considera que “**esta reglamentación es un primer paso pero todavía falta un poco más”.**

**En Colombia 16.509 personas se han acercado y han firmado su “Testamento vital”** de acuerdo con información en el portal web de la Fundación Pro Derecho a Morir Dignamente, organización que cuenta con documentos que incluyen las instrucciones sobre los cuidados y tratamientos que se quieren o no recibir. La validación del “Testamento vital” requiere de la firma de dos testigos y puede incluir el deseo de recibir ayuda activa para morir.
