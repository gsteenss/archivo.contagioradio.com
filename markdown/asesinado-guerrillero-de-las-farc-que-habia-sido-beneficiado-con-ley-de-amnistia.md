Title: Asesinado guerrillero de las FARC que había sido beneficiado con ley de amnistía
Date: 2017-04-20 15:29
Category: Nacional, Paz
Tags: Amnistia, Amnistía de Iure, FARC, Indulto, paz, Zona Veredal
Slug: asesinado-guerrillero-de-las-farc-que-habia-sido-beneficiado-con-ley-de-amnistia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/04/Ariel-aldana.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Zona Veredal Transitoria de Normalización, Ariel Aldana] 

###### [20 Abr. 2017] 

**Luis Alberto Ortiz Cabezas integrante de las FARC**, quien era conocido como “Pepe”, **fue asesinado este 16 de abril a las 8 p.m. en la vereda la Guayacana**, Corregimiento de Llorente en Tumaco, en donde habitaba y hasta donde llegaron para atacarlo con arma de fuego. El guerrillero había sido beneficiario de la ley de amnistía e indulto y había recobrado su libertad hace 15 días, cuando salió de la cárcel de Vista Hermosa en Cali.

A través de un comunicado emitido por el comando de la Zona Veredal Transitoria de Normalización, Ariel Aldana, las **FARC expresaron su temor e indignación ante la falta de garantías para los integrantes de esa guerrilla** en el marco de la implementación de los Acuerdos de Paz.

Así mismo, manifestaron su preocupación por el **incremento de homicidios en todo el municipio donde se encuentra ubicada esta Zona Veredal** y de la cuál era parte Luis Alberto.

Según algunas versiones de los pobladores, el hecho habría sido cometido **“por un hombre apodado “Renol”, quien pertenece a un grupo armado** y hace presencia en la zona”.

Por su parte, los integrantes de la Zona Veredal afirman que, estas situaciones y los asesinatos de varios líderes sociales, les genera la inquietud sobre el trabajo que están realizando las autoridades en cuanto al cumplimiento de los compromisos.

> Estos hechos demuestran que el paramilitarismo persiste en los territorios y minan la confianza. [\#COMUNICADO](https://twitter.com/hashtag/COMUNICADO?src=hash) <https://t.co/JHXScukko5>
>
> — Carlos A. Lozada (@Carlozada\_FARC) [20 de abril de 2017](https://twitter.com/Carlozada_FARC/status/855114604392919041)

<p style="text-align: justify;">
<script src="//platform.twitter.com/widgets.js" async charset="utf-8"></script>
“En el sentido de **combatir las bandas paramilitares al servicio de las mafias que se constituyen en amenaza real para la ciudadanía, los guerrilleros** y los milicianos excarcelados, que no cuentan con protección alguna” agrega el comunicado. Le puede interesar: [Ley de Amnistía la manzana de la discordia](https://archivo.contagioradio.com/ley-de-amnistia-la-manzana-de-la-discordia/)   Para los guerrilleros, estas situaciones dan cuenta de cómo **se está pasando por alto la protección de la ciudadanía y de los excarcelados**, condición primordial para “garantizar una paz estable y duradera” con miras a “convertirnos en un partido político civil”. Le puede interesar: [Más de 100 organizaciones del mundo piden aplicación de amnistía a líderes sociales](https://archivo.contagioradio.com/amnistia-a-lideres-sociales-colombia/)  

<script src="//platform.twitter.com/widgets.js" async charset="utf-8"></script>
</p>
Cabe recordar que tras dos días de jornadas de trabajo conjunto entre el Gobierno Nacional y las FARC- EP, en el marco de la Comisión de Seguimiento, Impulso y Verificación a la Implementación– CSIVI, en el que se evaluaron los primeros 100 días de la implementación, el Gobierno se comprometió a activar las garantías de seguridad que fueran necesarias.

**“Agilizar la activación de los mecanismos e instancias establecidas en el Acuerdo Final,** incluyendo el diseño y puesta en marcha del Pacto Político Nacional que busca garantizar el efectivo compromiso de todos los colombianos para que nunca más se utilicen las armas para hacer política” dice el comunicado de esa fecha. Le puede interesar: [Oleada de amenazas y homicidios en Colombia es escalofriante: Amnistía Internacional](https://archivo.contagioradio.com/oleada-de-amenazas-y-homicidios-en-colombia-es-escalofriante-amnistia-internacional/)

Según cifras entregadas por las FARC, de 2.736 nombres que esa guerrilla ha entregado al gobierno este les entregó 1.080 certificaciones. **Hasta el momento han salido de las cárceles por amnistía de iure 132 guerrilleros, de los cuales 22 tienen libertad condicional. **Le puede interesar: [Hay más de 700 solicitudes de amnistía a integrantes de las FARC sin respuesta](https://archivo.contagioradio.com/700-solicitudes-de-amnistia-sin-respuesta/)

Las principales problemáticas identificadas ante estas situaciones, es que la Oficina del Alto Comisionado no ha hecho entrega de las certificaciones de la totalidad de prisioneros que han sido entregados en los listados, la secretaria ejecutiva de **la JEP está paralizada y no se han suscrito actas de compromiso desde hace 3 semanas** y por último los juzgados de Tunja, Bogotá, Cali e Ibagué siguen cerrados.

###### Reciba toda la información de Contagio Radio en [[su correo]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio]{.s1}](http://bit.ly/1ICYhVU)[.]{.s1} {#reciba-toda-la-información-de-contagio-radio-en-su-correo-o-escúchela-de-lunes-a-viernes-de-8-a-10-de-la-mañana-en-otra-mirada-por-contagio-radio. .p1}
