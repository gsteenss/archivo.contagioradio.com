Title: Foro de Mujeres Rurales, tejiendo lazos para florecer
Date: 2016-11-17 18:11
Category: Mujer, Nacional
Tags: Derechos, Igualdad, mujeres, mujeres rurales, Oxfam, paz, territorio
Slug: foro-de-mujeres-rurales-tejiendo-lazos-para-florecer
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/para-convocatoria.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Israel Aguado] 

###### [17 Nov. 2016] 

Este 17 de Noviembre se realizó en Bogotá, el Foro de Mujer Rural en el que **más de 200 organizaciones se dieron cita para hablar de temas importantes a propósito de la actual coyuntura por la que atraviesa el país.**

Dentro de los puntos trabajados en este evento, se habló del Fondo de Fomento para las Mujeres Rurales (FOMMUR), el empoderamiento económico de las mujeres rurales, la eliminación de todos los tipos de violencia en contra de las mujeres, la participación e institucionalidad, el acceso a tierras y el medio ambiente.

El evento que contó con un acto artístico de las mujeres rurales que llegaron de diferentes partes de Colombia, pretendió enviar un mensaje a todas las personas de la capital dado que las instituciones se encuentran en Bogotá.

“**Hacer el evento en el centro del país, genera un posibilidad de llegar a otros espacios para que sean las voces de las mujeres rurales las que compartan sus historias con las personas que de una forma u otra hacen parte de un gobierno y toman decisiones”** puntualizó Visitación Asprilla, integrante de la Red departamental de Mujeres Chocoanas.

Este foro también se realizó con el ánimo de tejer lazos y exigir en conjunto el cumplimiento de diversos acuerdos que han sido pactados entre el gobierno nacional y la red de mujeres “**siempre nos queda un sinsabor porque para estos eventos delegan a personas desde la institucionalidad pero que no tienen poder de decisión”** agregó Visitación.

**Cada año muchas mujeres afros, indígenas, mestizas llegan a este encuentro con el ánimo de compartir y realizar diversas exigencias al gobierno nacional.**

En esta ocasión enviaron un mensaje contundente, como lo contó Edilia Mendoza integrante de la Plataforma de Mujeres Rurales “la institucionalidad aún no ha entendido que una política o un programa es universal y no tiene en cuenta el enfoque territorial, de género ni tampoco el de justicia. Por eso pedimos que se nos escuche"

Por último, las mujeres de la plataforma de Mujeres Rurales manifestaron que han apoyado el proceso de paz desde sus inicios y en la actualidad **“nosotras las mujeres rurales le apostamos a la paz, estamos haciendo muchos esfuerzos,** pero invitamos al gobierno nacional a que recuerde que debe hacer la paz con el ELN y el EPL que aún existe”.

Y concluyó “**sobre todo pedimos al Gobierno que no niegue que existe paramilitarismo, que es el que más ha destruido la economía y las comunidades en este país. Si no se desmonta a los paramilitares, no podemos hablar de paz**”. Le puede interesar: [Solo el 24% de las mujeres rurales pueden tomar decisiones sobre sus tierras](https://archivo.contagioradio.com/solo-el-24-de-las-mujeres-rurales-pueden-tomar-decisiones-sobre-sus-tierras/)

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
