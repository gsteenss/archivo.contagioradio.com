Title: "Estado invisibiliza torturas que se presentan fuera del conflicto armado" Informe CCCT
Date: 2015-06-25 17:28
Category: Entrevistas, Judicial
Tags: ccct, Claudia Julieta Duque, código de policía, cspp, Desplazamiento, Discriminación, ejecucion, fuero penal, Gloria silva, julio arevalo, militar, paz, prisioneros, solidaridad, tortura
Slug: estado-invisibiliza-torturas-que-se-presentan-fuera-del-conflicto-armado-informe-ccct
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/06/20150625_090515-1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Contagio Radio 

#####  

<iframe src="http://www.ivoox.com/player_ek_4691396_2_1.html?data=lZumk5ideo6ZmKiakpiJd6KkkZKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRaZS6hqifh6elqdTowsncjc7SusrnysfWzs7epYzo0Nfh19fFt4zl1sqY1cqPtNPZ1Mrb1sbSb8fpxtfOjZKJe6ShpNTb1sbLrdCfs8bRy9SPcYarpJKh&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Gloria Silva, CSPP] 

###### [25 jun 2015] 

La jurisprudencia en Colombia invisibiliza los hechos de tortura, más aún cuando no se presentan en el marco del conflicto armado. Así lo determinó el informe presentado por la Comisión Colombiana Contra la Tortura, en el marco de la conmemoración del Día Internacional en apoyo a las víctimas de Tortura.

\[caption id="attachment\_10523" align="aligncenter" width="345"\][![Tortura en Colombia](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/06/20150626_085715.jpg){.wp-image-10523 width="345" height="207"}](https://archivo.contagioradio.com/estado-invisibiliza-torturas-que-se-presentan-fuera-del-conflicto-armado-informe-ccct/20150626_085715/) Tortura en Colombia\[/caption\]

Según arroja la investigación adelantada, los departamentos en los que se presentan mayor número de denuncias por tortura son Cauca, Nariño, Putumayo, Santander y Norte de Santander, aquellos en los que el conflicto armado se vive con fuerza cotidianamente. Sin embargo, asegura la CCCT, existe un sub-registro de casos que contempla aquellos en los que las víctimas no son conscientes de que se está cometiendo este delito, o cuando existe conexidad con otras violaciones a los Derechos Humanos y los organismos judiciales dan prelación a delitos considerados mayores, como desplazamiento forzado o asesinato.

\[caption id="attachment\_10525" align="aligncenter" width="494"\][![Departamentos con denuncias de Tortura](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/06/20150626_085732-1.jpg){.wp-image-10525 width="494" height="247"}](https://archivo.contagioradio.com/estado-invisibiliza-torturas-que-se-presentan-fuera-del-conflicto-armado-informe-ccct/20150626_085732-1/) Departamentos con denuncias de Tortura\[/caption\]

Y es que en el imaginario social colombiano existe la creencia de que la tortura se relaciona únicamente con acciones físicas contra las víctimas, sin embargo, el CCCT busca evidenciar que la tortura es "toda acción que inflija a una persona dolores o sufrimientos físicos o psiquicos, causados con el fin de obtener información o confesión de la víctima o de terceros, castigarla por un acto que haya cometido o se sospeche que ha cometido, o de intimidarla o coaccionarla por motivos basados en cualquier tipo de discriminación".

Contempla, entonces, acciones que rompen el tejido social, como la discriminación y otro tipo de ataques por motivos de género, raza, creencias religiosas, ideologías políticas, condición física y económica; incluye la detención, desplazamiento y desaparición forzado, maltrato, ejecución extrajudicial, violencia sexual y amenaza.

En informe demuestra que "en Colombia la tortura y los tratos o penas crueles e inhumanas o degradantes se practican de manera sistemática y generalizada. Entre 2009 y 2014 se han registrado 349 casos de tortura física y la Fuerza Pública es registrada como el principal perpetrador de estos actos, seguido de los grupos paramilitares posdemovilización".

Y aunque jurídicamente el Código Penal colombiano reconoce este tipo de delitos tanto en el marco del conflicto armado como fuera de él, en la práctica, actualmente pocos son los procesos judiciales que se adelantan por estos delitos, según la CCCT, porque en muchos casos las autoridades competentes entienden la tortura como un delito subsidiario de otros. Por ejemplo, en casos de las ejecuciones extrajudiciales no se contempla la tortura previa al asesinato; en casos del desplazamiento forzado no se investigan las acciones físicas o psicológicas que generan la acción individual o colectiva del desplazamiento; en casos de represión de la protesta social se privilegia la denuncia por "abuso de autoridad", y no la intensión de las fuerzas estatales de generar temor, zozobra y dinámicas de comportamiento ejemplificantes para grupos sociales; y en los relacionados con tortura a población carcelaria, el Estado se escuda en la omisión de la garantía a derechos sociales como la salud, para no reconocer que muchas de sus acciones son castigos intencionales contra los y las prisioneros que se organizan al interior de los centros penitenciarios.

<iframe src="http://www.ivoox.com/player_ek_4688708_2_1.html?data=lZulmpyUfI6ZmKiakpeJd6KplpKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRaZOmptjhw8nTb8ri187gy8fNsMruwpDh0dfYudPV1JDe18qPt8af0dfS1crSuMLijMvix9fFb8XZjoqkpZKns8%2FowszW0ZC2pcXd0JCah5yncZU%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Gloria Silva y Julio Arévalo, CSPP y CAPS respectivamente] 

 

El informe alerta que la expedición de un nuevo código de policía y la reforma al fuero penal militar generarían una disminución en la denuncia de los caso, no porque dejaran de presentarse, sino porque estas reformas buscan limitar las formas de denuncia por parte de las víctimas, y la acceso a la verdad, justicia y reparación.

Para la Comisión Contra la Tortura, es importante que en el marco del proceso de paz se garanticen nuevas formas de relacionamiento social que no lleven al Estado o a grupos poblacionales a resolver los conflictos sociales por medio de delitos como la tortura. Insisten también en que "en la medida en que haya garantías de participación para la sociedad en las decisiones que les afectan, se esta generando mecanismos de prevención de la tortura".
