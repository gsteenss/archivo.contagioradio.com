Title: Camilo de las Casas
Date: 2015-02-02 12:28
Author: AdminContagio
Slug: camilo-de-las-casas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/06/camilo-de-las-casas.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

### CAMILO DE LAS CASAS

[![Aterrizó el del 2022](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/Uribe-hoy-Magadalena-370x260.jpg){width="370" height="260"}](https://archivo.contagioradio.com/aterrizo-el-del-2022/)  

#### [Aterrizó el del 2022](https://archivo.contagioradio.com/aterrizo-el-del-2022/)

Posted by [Camilo De Las Casas](https://archivo.contagioradio.com/author/camilo-de-las-casas/)[<time datetime="2019-07-18T12:42:21-05:00" title="2019-07-18T12:42:21-05:00">julio 18, 2019</time>](https://archivo.contagioradio.com/2019/07/18/)Comments: [0](https://archivo.contagioradio.com/aterrizo-el-del-2022/#respond)El viernes aterrizó él de ÉL. Era el elegido, nunca Santos. Pero un turbio proceso judicial confeccionado por la Corte…[Leer más](https://archivo.contagioradio.com/aterrizo-el-del-2022/)  
[![Dos caras de la misma moneda](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/08/180807_06_PosesionPresidenteDuque-370x260.jpg){width="370" height="260"}](https://archivo.contagioradio.com/dos-caras-de-la-misma-moneda/)  

#### [Dos caras de la misma moneda](https://archivo.contagioradio.com/dos-caras-de-la-misma-moneda/)

Posted by [](https://archivo.contagioradio.com/author/)[<time datetime="2018-08-21T18:08:51-05:00" title="2018-08-21T18:08:51-05:00">agosto 21, 2018</time>](https://archivo.contagioradio.com/2018/08/21/)Comments: [0](https://archivo.contagioradio.com/dos-caras-de-la-misma-moneda/#respond)No es igual que en el 2002. La táctica es diferente, los propósitos estratégicos impunidad para él y su círculo…[Leer más](https://archivo.contagioradio.com/dos-caras-de-la-misma-moneda/)  
[![Unas velas, el poder de lo nuevo](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/07/velaton-bogota-370x260.jpg){width="370" height="260"}](https://archivo.contagioradio.com/unas-velas-el-poder-de-lo-nuevo/)  

#### [Unas velas, el poder de lo nuevo](https://archivo.contagioradio.com/unas-velas-el-poder-de-lo-nuevo/)

Posted by [](https://archivo.contagioradio.com/author/)[<time datetime="2018-07-07T17:42:37-05:00" title="2018-07-07T17:42:37-05:00">julio 7, 2018</time>](https://archivo.contagioradio.com/2018/07/07/)Comments: [0](https://archivo.contagioradio.com/unas-velas-el-poder-de-lo-nuevo/#respond)30 o 127, uno solo, un solo asesinado es un escándalo. La violencia multiforme que combina amenazas, estigmatizaciones y culmina…[Leer más](https://archivo.contagioradio.com/unas-velas-el-poder-de-lo-nuevo/)  
[![El Jefe es el Pre, aunque en apariencia es un senador, ex pre y ex gobernador](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/06/uribe-y-duque-370x260.jpg){width="370" height="260"}](https://archivo.contagioradio.com/el-jefe-es-el-pre-aunque-en-apariencia-es-un-senador-ex-pre-y-ex-gobernador/)  

#### [El Jefe es el Pre, aunque en apariencia es un senador, ex pre y ex gobernador](https://archivo.contagioradio.com/el-jefe-es-el-pre-aunque-en-apariencia-es-un-senador-ex-pre-y-ex-gobernador/)

Posted by [](https://archivo.contagioradio.com/author/)[<time datetime="2018-06-26T12:28:53-05:00" title="2018-06-26T12:28:53-05:00">junio 26, 2018</time>](https://archivo.contagioradio.com/2018/06/26/)Comments: [0](https://archivo.contagioradio.com/el-jefe-es-el-pre-aunque-en-apariencia-es-un-senador-ex-pre-y-ex-gobernador/#respond)Ya se observan los rasgos característicos de estos próximos cuatro años o mejor de la apuesta de un proyecto de…[Leer más](https://archivo.contagioradio.com/el-jefe-es-el-pre-aunque-en-apariencia-es-un-senador-ex-pre-y-ex-gobernador/)  
[![Pase lo que pase, lo nuevo está creciendo](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/02/marcha-de-las-flores101316_160-370x260.jpg){width="370" height="260"}](https://archivo.contagioradio.com/pase-lo-pase-lo-nuevo-esta-creciendo/)  

#### [Pase lo que pase, lo nuevo está creciendo](https://archivo.contagioradio.com/pase-lo-pase-lo-nuevo-esta-creciendo/)

Posted by [Contagio Radio](https://archivo.contagioradio.com/author/contagioradio/)[<time datetime="2018-06-15T20:07:41-05:00" title="2018-06-15T20:07:41-05:00">junio 15, 2018</time>](https://archivo.contagioradio.com/2018/06/15/)Comments: [0](https://archivo.contagioradio.com/pase-lo-pase-lo-nuevo-esta-creciendo/#respond)Junio 14 de 2018 Por Camilo De Las Casas La Colombia de hoy es otra, diferente a la de estos últimos…[Leer más](https://archivo.contagioradio.com/pase-lo-pase-lo-nuevo-esta-creciendo/)  
[![Con Petro y más allá de Petro.](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/02/marcha-de-las-flores101316_160-370x260.jpg){width="370" height="260"}](https://archivo.contagioradio.com/petro-mas-alla-petro/)  

#### [Con Petro y más allá de Petro.](https://archivo.contagioradio.com/petro-mas-alla-petro/)

Posted by [Contagio Radio](https://archivo.contagioradio.com/author/contagioradio/)[<time datetime="2018-05-20T09:41:38-05:00" title="2018-05-20T09:41:38-05:00">mayo 20, 2018</time>](https://archivo.contagioradio.com/2018/05/20/)Comments: [0](https://archivo.contagioradio.com/petro-mas-alla-petro/#respond)Por Camilo De Las Casas 18 de mayo de 2018 Lo nuevo se sigue expresando está en las calles reales…[Leer más](https://archivo.contagioradio.com/petro-mas-alla-petro/)  
[![Empezó el guayabo de ellos](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/04/elecciones-tumaco-370x260.jpg){width="370" height="260"}](https://archivo.contagioradio.com/52243-2/)  

#### [Empezó el guayabo de ellos](https://archivo.contagioradio.com/52243-2/)

Posted by [](https://archivo.contagioradio.com/author/)[<time datetime="2018-03-17T13:02:36-05:00" title="2018-03-17T13:02:36-05:00">marzo 17, 2018</time>](https://archivo.contagioradio.com/2018/03/17/)Comments: [0](https://archivo.contagioradio.com/52243-2/#respond)Foto: hbs noticias Por Camilo De Las Casas 15 mar 2018 El país es otro. Lo que se denomina el fenómeno…[Leer más](https://archivo.contagioradio.com/52243-2/)  
[![Siempre faltones, pero hay algo nuevo](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/02/marcha-por-la-paz-370x260.jpg){width="370" height="260"}](https://archivo.contagioradio.com/siempre-faltones-pero-hay-algo-nuevo/)  

#### [Siempre faltones, pero hay algo nuevo](https://archivo.contagioradio.com/siempre-faltones-pero-hay-algo-nuevo/)

Posted by [](https://archivo.contagioradio.com/author/)[<time datetime="2018-02-09T16:04:10-05:00" title="2018-02-09T16:04:10-05:00">febrero 9, 2018</time>](https://archivo.contagioradio.com/2018/02/09/)Comments: [0](https://archivo.contagioradio.com/siempre-faltones-pero-hay-algo-nuevo/#respond)El incumplimiento de la palabra de Estado en acuerdos con comunidades locales, organizaciones sociales y las FARC, y en la…[Leer más](https://archivo.contagioradio.com/siempre-faltones-pero-hay-algo-nuevo/)  
[![Entre los Morales y los Garzón](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/01/jaime-garzon-y-claudia-morales-370x260.jpg){width="370" height="260"}](https://archivo.contagioradio.com/entre-los-morales-y-los-garzon/)  

#### [Entre los Morales y los Garzón](https://archivo.contagioradio.com/entre-los-morales-y-los-garzon/)

Posted by [](https://archivo.contagioradio.com/author/)[<time datetime="2018-01-27T10:47:16-05:00" title="2018-01-27T10:47:16-05:00">enero 27, 2018</time>](https://archivo.contagioradio.com/2018/01/27/)Comments: [0](https://archivo.contagioradio.com/entre-los-morales-y-los-garzon/#respond)Está abriéndose en el debate de la memoria, el derecho al silencio y la distorsión de las memorias en lo…[Leer más](https://archivo.contagioradio.com/entre-los-morales-y-los-garzon/)
