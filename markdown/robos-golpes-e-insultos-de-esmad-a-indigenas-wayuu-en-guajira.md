Title: Robos, golpes e insultos de ESMAD a indígenas Wayúu en Guajira
Date: 2017-02-06 16:32
Category: DDHH, Nacional
Tags: ICBF, indigenas wayuu
Slug: robos-golpes-e-insultos-de-esmad-a-indigenas-wayuu-en-guajira
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/02/denuncian-ataque-desmedido-del-esmad-e1486403562891.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: El Heraldo] 

###### [6 Feb 2017] 

Indígenas Wayúu fueron desalojados de forma violenta por el ESMAD, mientras adelantaban una protesta a la altura del kilómetro 70 sobre la vía Férrea que sirve al Cerrejón, los indígenas también **denunciaron robo de sus pertenencias, abusos de autoridad y la captura de 7 autoridades tradicionales y un menor de edad que estarían siendo judicializados.**

El desalojo se produjo el sábado en horas de la noche en el territorio indígena de Katsaliamana, de acuerdo con Brandon González, integrante de la comunidad Wayúu, el ESMAD llegó  **“tirando gases lacrimógenos, saquearon las pertenencias de las mujeres, se robaron la comida y los chinchorros y capturaron a las 7 autoridades indígenas y el menor de edad”.**

De igual modo los, **indígenas señalaron que fueron amenazados en diferentes ocasiones**, la primera de ellas fue vía telefónica, la segunda a través de panfletos que llagaron a las viviendas de las personas que participaban de la movilización y la última se dio durante la protesta.

Previo a estos hechos, los indígenas sostuvieron una reunión con la contratista del Ministerio del Interior Edith Gómez y la defensoría del pueblo para evidenciar las amenazas de las que son víctimas, sin embargo, Brandon González denunció que les tomaron fotografías y todos los datos para llegar el sábado **con “lista en mano” a Katsaliamana y llevarse a las autoridades.**

Los indígenas anunciaron que seguirán protestando en esta vía hasta que **se respete la autonomía territorial, el derecho a la consulta previa** y la mejoría de los programas de alimentación para los niños indígenas. Le puede interesar: ["Comunidades indígenas vuelven a bloquear la vía férrea del Cerrejón"](https://archivo.contagioradio.com/comunidades-wayuu-exigen-derecho-a-la-autonomia-regional/)

<iframe id="audio_16852474" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_16852474_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
