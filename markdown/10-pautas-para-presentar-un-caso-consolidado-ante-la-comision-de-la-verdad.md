Title: 10 pautas para presentar un caso ante la Comisión para el Esclarecimiento de la Verdad
Date: 2019-01-18 15:25
Author: AdminContagio
Category: DDHH, Paz
Tags: comision de la verdad, conflicto armado, posconflicto
Slug: 10-pautas-para-presentar-un-caso-consolidado-ante-la-comision-de-la-verdad
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/01/Dwd-HmyWwAEfJ87-770x400.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: @ComisionVerdadC 

###### 17 Ene 2019 

Como parte de su labor de pedagogía, la Comisión de la Verdad, cuyo trabajo comenzó este 2019, ha dado a conocer una guía que explica cómo y qué tipo de información deben aportar todos aquellos quienes presentarán su caso ante el mecanismo, que junto a otros organismos como la Jurisdicción Especial para la Paz y la Unidad para la Búsqueda de Personas dadas por Desaparecidas, buscará sentar las bases de la no repetición, la reconciliación y la paz en el territorio colombiano.

**1 ¿Qué es un caso para la Comisión?**

[La Comisión de la Verdad considerará como caso los hechos que cuenten con documentos de soporte  y uno o varios testimonios que permitan identificar la violación de derechos humanos o infracciones al derecho internacional humanitario cometidos en el marco del conflicto armado interno.]

**2. ¿Qué no es un caso para la Comisión?**

La Comisión no considerará como caso la simple entrega de soportes documentales o expedientes judiciales adelantados por otras instituciones estatales (Unidad de Víctimas o Defensoría del Pueblo), pues los casos no tendrán una naturaleza judicial, no buscan llevar a juicio a los presuntos responsables, únicamente tiene como objetivo determinar las causas, las formas de ejercer violencia, consecuencias y las estrategias que usaron las víctimas para hacer frente a la violencia.

**3. ¿Quién puede presentar un caso ante la Comisión?**

Pueden acudir a este organismo personas, organizaciones, instituciones o grupos de cualquier sector de la sociedad que quieran aportar al esclarecimiento de la verdad del conflicto armado interno de Colombia. La Comisión preservará la confidencialidad de las personas que contribuyan a la investigación, sin embargo no se recibirán casos de forma anónima. [(Lea también Casos de violencia en el Catatumbo y Tumaco llegarán a la Comisión de la Verdad) ](https://archivo.contagioradio.com/casos-de-violencia-sexual-en-catatumbo-y-tumaco-llegaran-a-comision-de-la-verdad/)

**4. ¿Cómo puedo presentar un caso a la Comisión?**

Se puede llevar personalmente a la oficina central de la Comisión de la Verdad en Bogotá (Calle 77 \#11-19, piso 5) o a las Casas de la Verdad regionales o locales que se abrirán al público, por correo postal a la oficina central de la Comisión de la Verdad o a las Casas de la Verdad regionales o al correolectrónico: <informesycasos@comisiondelaverdad.co>. En cualquiera de las tres alternativas la información debe ir dirigida al Grupo de gestión documental de la Comisión de la Verdad con el asunto: "Entrega de caso a la Comisión de la Verdad".

**5. ¿Hay una fecha límite para presentar casos a la Comisión?**

Aunque no existe un plazo límite para presentar casos ante la Comisión, la entidad recomienda presentar los casos a lo largo del 2019, pues 2020 será un año en que se enfocará en el contraste de las fuentes de información y en 2021 se dará paso a la producción final de su informe y divulgación.

**6**[.]**¿Qué información debe incluir un caso presentado?  
  
**[El nombre debe contener información clave para identificar el caso, hacer referencia al hecho ocurrido (masacre XX), a las personas o grupos afectados a la fecha y el lugar de los hechos (región, municipio, corregimiento, vereda, localidad, consejo comunitario o resguardo).  
  
Quien presenta el caso debe incluir su nombre, edad, sexo, orientación sexual o identidad de género, dirección de contacto, correo electrónico de contacto, número telefónico de contacto; relación con la/s víctimas directas; y grupo u organización a la que pertenezca.   
  
El caso, además de describir los hechos y el tipo de violación de derechos humanos que se presentó, debe relatar de la forma más detallada posible todo lo acontecido víctimas, testigos, hechos, etc. También debe aportar información sobre los antecedentes, es decir si existieron ataques, amenazas o hechos significativos que tengan relación con lo sucedido o si se conocen hechos similares en la localidad.  
**  
7. Relevancia de la información sobre víctimas y responsables**]

[En el caso de las víctimas debe indicarse su nombre, edad, estado civil, quién hace parte de su familia, su orientación sexual, grupo étnico, oficio, profesión cómo vivía, a qué se dedicaba, su participación y rol en algún grupo o sector social  y toda la información que se considere relevante.]

[En cuanto a quienes cometieron los hechos, ya sean individuos o grupos armados ilegales o fuerzas armadas estatales. en lo posible debe suministrarse toda la información posible, identidad de los agresores si se conoce, características, pertenencia a algún grupo armado, detalles (bloque, frente, batallón, entre otros), cabe aclarar que la Comisión no puede determinar responsabilidades individuales sino colectivas.]

**8 ¿Qué tipo de documentación se puede presentar?** [  
  
Anexo a la información se pueden incluir testimonios, notas de prensa, denuncias penales de la Fiscalía, registros ante la Unidad para las Víctimas, de igual forma se puede poner a disposición una copia de los documentos, grabaciones, videos o fotos de alta importancia o aportes únicos o novedosos, tanto en papel como en digital o si es el caso entregar CD, USB u otros soportes.]

[La Comisión de la Verdad podrá recoger información de entidades del Estado o de organismos internacionales por lo que puede solicitar información si quien envía el caso adjunta el número, código o referencia a través de la cual se puede acceder a la información del ente indicado.]

**9. ¿La Comisión esclarecerá el caso específico que se presente?** [  
  
**La Comisión no puede esclarecer individualmente los casos presentados, esta debe enfocarse en identificar las lógicas, patrones, responsables colectivos, impactos y formas de afrontamiento del conflicto armado**, y no de la investigación y juzgamiento de hechos particulares relacionados con el conflicto armado, para dicha tarea está encargada la Jurisdicción Especial para la Paz.   
  
El mandato de tres años de la Comisión no es un tiempo suficiente para esclarecer cada caso de un conflicto armado que afectó la vida de aproximadamente nueve millones a lo largo de cincuenta años, sin embargo los testimonios tanto individuales y colectivos, además de los informes serán recopilados para contrastarlos entre sí y con otras fuentes para establecer de las dinámicas del conflicto en Colombia.  
]

**10 ¿Qué pasa si no se tiene toda la información solicitada?**[  
  
Los datos solicitados en la guía de la Comisión no son estrictos ni cerrados, sin el caso existen datos (impacto, antecedentes), la Comisión recibirá el caso en la forma en que la persona u organización quiera presentarlo, sin embargo, entre más específico y documentada sea la información es más probable que esta sea tenida en cuenta para que sea contrastada con otros testimonios.]

Puede leer toda la guía a continuación:

[Guia Presentacion Casos Comision Verdad](https://www.scribd.com/document/397751221/Guia-Presentacion-Casos-Comision-Verdad#from_embed "View Guia Presentacion Casos Comision Verdad on Scribd") by [Contagioradio](https://www.scribd.com/user/276652802/Contagioradio#from_embed "View Contagioradio's profile on Scribd") on Scribd

<iframe id="doc_86352" class="scribd_iframe_embed" title="Guia Presentacion Casos Comision Verdad" src="https://es.scribd.com/embeds/397751221/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-G3SMD1UdwiSefFkgF9p7&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="0.7729220222793488"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
