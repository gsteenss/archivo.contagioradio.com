Title: Ejército deberá responder por torturas a 11 personas durante retoma del Palacio de Justicia
Date: 2018-01-29 09:07
Category: DDHH, Nacional
Tags: Palacio de Justicia, Víctimas del Palacio de justicia
Slug: torturas_palacio_justicia_retoma
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/01/palacio-de-justicia-e1517241421149.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo] 

###### [27 Ene 2018] 

Un total de 14 militares tendrán que rendir indagatoria ante la Fiscalía General de la Nación por el delito de tortura, en el marco de los hechos que rodearon la toma y retoma del Palacio de Justicia el 6 y 7 de noviembre de 1985. Un hecho que fue demostrado en un fallo de la Corte Interamericana de Derechos Humanos.

Son once las víctimas reconocidas que denunciaron la forma cómo **desde la captura fueron torturados física y psicológicamente acusándolos de pertenecer al la guerrilla del M-19.** De acuerdo con Eduardo Carreño, abogado de Colectivo de Abogados José Alverar Restrepo las víctimas fueron de estudiantes que visitaban a profesores, trabajadores o visitantes ocasionales que acudían ese día a averigua por algún proceso.

"Las víctimas fueron torturados en **las instalaciones del B2 de la brigada o en le Batallón de inteligencia y contra inteligencia Charry Solano**", explica el abogado, entre los militares hay varios generales que deben responder por este tipo de hechos, entre ellos se nombra a **Rafael Hernández López, comandante del Batallón de artillería, también el Coronel del B2 Edilberto Sánchez Rubiano, o el Coronel Iván Ramírez.**

De acuerdo con el defensor de las víctima, se constató que ese delito nunca había sido investigado, por eso todos ellos luego ascendieron a cargos de generales. (Le puede interesar: [El pacto del silencio frente a los hechos del holocausto del Palacio de Justicia)](https://archivo.contagioradio.com/49372/)

### **Ejército sigue negando el delito tortura** 

"La postura por del Ejército hasta ahora ha sido de negar absolutamente todo y plantear que deben demostrar que ellos torturaron. teniendo en cuenta que ninguna de las víctimas los conocía, que no sabía sus nombres y además los tenían encapuchados", dice Carreño.

El abogado señala que se trata de un crimen que del que efectivamente hay pruebas de carácter técnico que demuestran que la tortura si se realizó. Además, **existen las declaraciones de las once víctimas, pero también de otras personas que fueron torturadas,** como es el caso de los estudiantes de la universidad Externado, Eduardo Matson y Yolanda Santodomingo, quienes constatan que fueron testigos de esos actos violentos por parte de militares. "La Fiscalía reconoce que las prueba de las torturas si existen", indica.

Se trata entonces de una luz en el camino espinoso que han tenido que atravesar las víctimas del Palacio, que en medio de los 32 años de impunidad, ven como un paso importante este hecho en el marco de la búsqueda de justicia en este proceso. Sin embargo, el abogado asegura que las víctimas tienen altos niveles de escepticismo en la administración de justicia, que no ha teniendo en cuenta que la toma "fue algo preparado por parte del ejército porque sabían lo que iba a pasar, y facilitaron el ingreso de la guerrilla al Palacio de Justicia.

### **Guerrilleros también fueron víctimas de tortura, ejecuciones y desapariciones** 

Tras esta decisión de la Fiscalía, se recibirán las declaraciones y luego se decidiría si se aplica una medida de detención. No obstante, el abogado de las víctimas explica que por efectos de la pena que tenía establecido el delito de tortura en la época, los militares no irían a detención física, pero después se iría a un posible juicio.

"Esperamos que al futuro haya condenas, sobre todo contra quienes direccionaron  en la las torturas", manifiesta el defensor de las víctimas. Además, se cree que con este avance es posible que **se abran otras investigaciones, por ejemplo los crímenes de guerra cometidos contra los guerrilleros.** "Muchos de ellos salieron vivos, fueron capturados y luego fueron ejecutados, o en algunos casos fueron desaparecidos. Por lo cual tendría que responder toda la cúpula del DAS y del Ejército", dice. (Le puede interesar: [Entregan restos de Mónica Molina, integrante del M-19)](https://archivo.contagioradio.com/monica-molina-integrante-del-m-19-retorno-a-los-brazos-de-su-familia-despues-de-32-anos-de-impunidad/)

<iframe id="audio_23419865" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_23419865_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
