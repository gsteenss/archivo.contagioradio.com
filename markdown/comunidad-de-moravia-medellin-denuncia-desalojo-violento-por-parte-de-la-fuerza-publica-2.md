Title: Comunidad de Moravia, Medellín denuncia desalojo violento por parte de la Fuerza Pública
Date: 2019-01-30 19:12
Author: AdminContagio
Category: Comunidad, Nacional
Tags: abusos Fuerza Pública, desalojo de comunidades, Moravia
Slug: comunidad-de-moravia-medellin-denuncia-desalojo-violento-por-parte-de-la-fuerza-publica-2
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/01/WhatsApp-Image-2019-01-29-at-11.43.40-AM-770x400.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Archivo 

###### 30 Ene 2019 

[El 29 de enero, los habitantes del barrio El Oasis, en Moravia, realizaron una toma de terreno exigiendo condiciones de vida digna, pues denuncian que la Alcaldía de Medellín no les ha ofrecido soluciones reales de vivienda luego de perder todo durante un incendio ocurrido en agosto de 2017 el cual afectó a 324 familias; como respuesta la fuerza pública realizó operaciones de desalojo de la población la cual habitaba en la rivera del Río Medellín.]

[**Santiago Valencia, presidente de la organización en defensa de derechos humanos, CORDEHCO** informó que el Esmad  y la Policía hicieron presencia en el lugar para desalojar a las 200 ]familias que se manifestaron y que esperan ser reubicadas o beneficiadas por un proyecto de vivienda de interés social, e incluso si es posible poder vivir  nuevamente debajo del puente donde han habitado los últimos cuatro meses.

["Las personas después de todo esto decidieron permanecer en uno de los albergues que tiene la ciudad para este tipo de casos, están bien de salud dentro de sus posibilidades” explica Valencia quien afirma, se encuentran buscando soluciones temporales a través de representantes a la cámara que se solidaricen con lo sucedido.  
  
Mientras  la Alcaldía de Medellín afirma que el sector no está habilitado para vivienda y que han brindado garantías a los habitantes de Moravia, la fuerza pública argumenta que con el desalojo se busca la recuperación del espacio público, **sin importar la argumentación jurídica del caso, no se han ofrecido respuestas reales a los habitantes del sector,** según Valencia, la administración municipal ha sido enfática en que cada caso es diferente por lo cual no pueden haber generalidades en la solución del problema.]

<iframe id="audio_32002930" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_32002930_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
