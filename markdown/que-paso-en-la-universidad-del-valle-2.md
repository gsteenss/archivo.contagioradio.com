Title: ¿Qué pasó en la Universidad del Valle?
Date: 2019-04-04 15:21
Author: ContagioRadio
Category: Movilización
Tags: disturbios, ESMAD, estudiantes, Universidad del Valle
Slug: que-paso-en-la-universidad-del-valle-2
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/Disturbios-en-la-Universidad-del-Valle-1.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: @athemayst] 

###### [4 Abr 2019] 

Durante el miércoles 3 de marzo se presentaron enfrentamientos en la Universidad del Valle entre encapuchados y el Escuadrón Móvil Anti Disturibios (ESMAD), en el marco de estas protestas, una persona resultó muerta y ocho más heridas (dos de ellas de gravedad) tras la explosión de un artefacto, causando gran conmoción en la comunidad universitaria, que aún se cuestiona las circunstancias en las que ocurrieron los hechos.

Cristian Guzmán, vocero de la Unión Nacional de Estudiantes de Educación Superior (UNEES), señaló que los voceros regionales y de la Universidad del Valle habían alertado desde horas de la mañana sobre los enfrentamientos, que se realizaban en apoyo a la Minga, y rechazando la muerte de Jhonatan Landines, estudiante de arquitectura que fue víctima de la explosión en Dagua, Valle del Cauca.

[Mientras la protesta se desarrollaba, las acciones violentas se fueron incrementando al punto en que se presentó una explosión al interior del campus universitario, que dejó como resultado una persona muerta y ocho heridas, dos de ellas de gravedad que fueron remitidas a la Fundación Valle del Lili. (Le puede interesar:["Así habría sido la muerte de ocho indígenas según la Minga en Dagua, Valle del Cauca"](https://archivo.contagioradio.com/nuestras-autoridades-llegaran-al-fondo-de-la-verdad-afirman-indigenas-en-dagua/))]

[El joven fallecido fue identificado como Jhonny Rodríguez, y de acuerdo al comunicado emitido por la Fundación, las dos personas heridas fueron intervenidas quirúrgicamente por afectaciones graves en sus extremidades superiores producto de la explosión. (Le puede interesar: ["Reportan un joven desaparecido tras procedimiento irregular de Policía en Cauca"](https://archivo.contagioradio.com/joven-desaparecido-policia-cauca/))]

Guzmán señaló que la UNEES está guardando prudencia, porque "nosotros tenemos audios e imágenes de un dron sobrevolando el lugar en que ocurrieron los hechos", razón por la que esperan investigaciones de fondo frente a cual fue la causa de la explosión. Por su parte, la Fuerza Pública después de ocurridos los hechos rápidamente señaló que se trataba de una 'cocina', o lugar en el que se preparaban papas bomba, y producto de una mala utilización de estos elementos, se produjo la explosión.

No obstante, el vocero recordó que en otras ocasiones en que se han presentado hechos similares, estudiantes y trabajadores han encontrado pruebas para creer que se trató de atentados por parte de la Fuerza Pública. Adicionalmente, está el precedente de lo ocurrido en Dagua, donde tras la explosión las autoridades señalaron que se trataba también de una Cocina, declaraciones que fueron refutadas por los indígenas.

### **¿Qué pasó?¿por qué siempre se ataca la protesta? y ¿cómo más se pueden abordar los tropeles?** 

Medios informativos nacionales aseguraron que Rodríguez no era estudiante activo de la Universidad; frente a lo cual Guzmán, indicó que estaban tomando distancia pues considera que no tiene tanta relevancia, como preguntarse por la forma en que el Gobierno trata la protesta: "tres personas de derechos humanos que fueron al hospital a confirmar quienes eran los heridos fueron capturados, porque decían que todos eran terroristas".

A ello se suma que inmediatamente después de ocurridos los hechos, muchas personas manifestaron su deseo de que la fuerza pública ingresara al Campus, situación que históricamente ha significado un riesgo para los estudiantes, pues se presumen excesos de fuerza contra todas las personas que allí se encuentran, sean o no participantes de la protesta. Para el estudiante, esta situación es grave en la medida en que siempre se intenta sacar la protesta de su contexto, y se le acusa de ser promovida por terroristas o grupos organizados, generando estigmatización contra la comunidad universitaria.

Guzmán recalcó que es delicado que no se esté hablando de la vida de las personas y la importancia de insistir en el dialogo como forma de superar los problemas, sino que se insista en las vías violentas para tratar estas situaciones. El Vocero de la UNEES concluyó que hoy se quiere enfrentar a las mismas personas con el mismo discurso de la seguridad que ha afectado la vida de diferentes personas, y que sigue causando temor entre los estudiantes.

<iframe id="audio_34097610" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_34097610_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
