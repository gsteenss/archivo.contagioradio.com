Title: Edis Care, líder comunitario del Chocó es asesinado
Date: 2020-08-25 11:03
Author: AdminContagio
Category: Actualidad, Líderes sociales
Slug: edis-care-lider-comunitario-del-choco-es-asesinado
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/03/Minas-antipersonales-y-artefactos-explosivos.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

Este 25 de agosto la Comisión Intereclesial de Justicia y Paz **denunció el asesinato de líder comunitario Edis Care**; activista y defensor de Derechos Humanos de la comunidad Villanueva en el departamento de Chocó en el territorio colectivo de [La Larga y Tumaradó](https://www.justiciaypazcolombia.com/el-consejo-comunitario-de-la-larga-tumarado-solicita-al-presidente-duque-apoyo-para-proyectos-productivos/) en el municipio de Río Sucio.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/Justiciaypazcol/status/1298234773853405186","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/Justiciaypazcol/status/1298234773853405186

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

Edis Care, **además hacia parte del Consejo Comunitario de la Cuenta de la Larga y Tumaradó (Cocalatu)**, organización que trabaja en contra de la [erradicación forzada](https://archivo.contagioradio.com/las-razones-que-explican-el-regreso-de-las-masacres-a-colombia/) de cultivos de coca y la garantía de vida digna para los habitantes de este municipio.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Care según el Concejo fue asesinado este lunes 24 de agosto, cuando hombres fuertemente armados ingresaron a la vivienda del líder **y le causaron la muerte propinándole varios impactos de arma de fuego.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

A esto se suma que **Edis Care había denuncia desde el mes de abrir amenazas en su contra** , las cuales se extiendendieron a los y las reclamantes de tierra de estos territorios quienes evidenciaron graves agresiones por parte de particulares el 3 de abril del 2020.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/CINEP_PPP/status/1246161532347613190","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/CINEP\_PPP/status/1246161532347613190

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

Día en el que ingresaron 6 hombres armados con sus rostros cubiertos al predio, Sevilla, y **amenazan de muerte a cuatro familias que hacían parte de COCALATU del dándoles 24 horas para desocupar el predio** antes de ser asesinados , esto como una medida para que los líderes y lideresas de estos territorios desalojaran sus predios.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

A las denuncias que venia haciendo el líder territorial desde inicio de año se suman a **hostigamientos, amenazas y restricciones a la movilidad de las personas del territorio, así como daños a los bienes** indispensables para la vida de la comunidad.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### El asesinato de Edis Care es un acto de revictimización

<!-- /wp:heading -->

<!-- wp:paragraph -->

COCALATU es integrada por 48 comunidades y 1.503 personas quién según el Centro de Investigación y Educación Popular (Cinep), *"han vivido entre las balas, los ataques de diversos actores armados y el intereses de empresas y Fuerza Pública"* , **exigiendo desde el 2019 al Gobierno que se comprometa con su protección y seguridad.**

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/CINEP_PPP/status/1298298497402273794","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/CINEP\_PPP/status/1298298497402273794

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

Hecho de violencia que fueron evidenciados el pasado 8 de mayo por la Sala de Reconocimiento de Responsabilidad y Determinación de los hechos y Conductas de l**a Jurisdicción Especial para la Paz, al acréditar en el caso [04 a los Consejo](https://www.cinep.org.co/Home2/component/k2/tag/COCOLATU.html) comunitario de La Larga y Tumaradó, junto a más de 20,000 víctimas afrocolombianas** en la región del Urabá víctimas.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En los hechos ocurridos entre 1996 y 1998 con el ingreso de las Autodefensas Unidas de Colombia (AUC), e**n presunción en una acción coordinada con miembros del Ejército en el que se presentaron múltiples violaciones a los Derechos Humanos de las comunidades afrocolombianas,** comodesplazamientos forzados, despojo de tierras, entre otras acciones violentas.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Finalmente hoy diferentes voceros del territorio denuncian la lentitud en la presencia de las autoridades, q**uienes aún no han llegado al territorio a realizar el proceso de levantamiento y registro del asesinato de Care.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

A su vez la **Comisión de Justicia y Paz, señaló estos hechos como "indignantes"**, y destacó que en el departamento del Chocó, la presencia de las Autodefensas Gaitanistas de Colombia, y el control que estás en los territorios se dan en medio de la presencia de las fuerzas armadas y la Policía.

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
