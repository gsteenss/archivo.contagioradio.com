Title: Finaliza el Foro Internacional Diálogos de paz y libertad de prensa
Date: 2015-05-08 20:08
Author: CtgAdm
Category: DDHH, eventos
Tags: AMARC Colombia, Diálogos de paz y libertad de prensa FLIP, FLIP Federación para la libertad de prensa Colombia, Radios Comunitarias Colombia, Regulación de frecuencias Colombia
Slug: finaliza-el-foro-internacional-dialogos-de-paz-y-libertad-de-prensa
Status: published

###### Foto:Contagioradio.com 

Hoy han finalizado el foro internacional **Diálogos de Paz y libertad de prensa** organizado por **FLIP** (Fundación para la libertad de prensa) con ponencias referidas a los medios comunitarios en Colombia, en concreto a las **radio comunitarias**, el espectro electromagnético Colombiano y a los **índices de libertad de expresión en el país.**

En la primera ponencia ¨Regulación de frecuencias y políticas de la comunicación¨ han participado Luis Eduardo Peña de la Agencia Nacional del Espectro **(ANE)** y Débora Pérez de la **dirección de comunicaciones del ministerio de Cultura**, ambos ponentes del gobierno.

Débora ha explicado la incidencia de los operadores en internet y sus configuraciones en Colombia indicando los distintos contratos , ha reconocido que en Colombia **en las regiones donde hay conflicto armado el acceso a internet es casi nulo.**

El delegado de la ANE relataba el espectro radioeléctrico colombiano y el futuro preacuerdo de los Diálogos de la Habana cuando ha **sido espetado por los distintos representantes de las organizaciones sociales de radios comunitarias** presentes por la gestión del gobierno con respecto a la financiación y a los insumos inexistentes para las radios. Además ha sido invitado por el público en repetidas ocasiones ha participar en el conversatorio sobre radios comunitarias.

**Peña** ha puntualizado que no está en sus manos sino en las del MINTIC y ha reconocido la **escasa financiación estatal y las dificultades con el excesivo canon que se les cobra a las radios comunitarias**, siendo estas asociaciones sin ánimo de lucro, pero ha dicho que no podía participar en el panel sobre radios comunitarias.

Sin duda el panel más esperado era el de **Medios comunitarios en Colombia:herramientas para la paz**, donde han participado SIPAZ, AMARC-Colombia, Kamsaa y distintos expertos en radios comunitarias de Colombia.

**Mauricio Beltrán de SIPAZ** ha realizado una ponencia sobre los retos de los **medios comunitarios en un futuro escenario de preacuerdo**, Miguel Córdoba de **AMARC** ha explicado las debilidades derivadas del conflicto armado para las radios comunitarias en su trabajo diario refiriendose a amenazas, y **dificultad en el ejercicio periodístico en el ámbito de las regiones.**

**María Jacinta**, experta en radios comunitarias ha informado sobre el modelo que se aplicó en **Bogotá para implementar las 7 radios comunitarias** existentes, recalcando la necesidad que hubo de tener que interponer tutelas y la necesidad futura de financiación para dichos medios.

A través de **Jaime Conrado** hemos podido escuchar la voz de la **emisoras indígenas,** el portavoz del Putumayo ha explicado que en su caso es necesario un **enfoque diferencial** próximo a la cultura de los pueblos ancestrales a la hora de gestionar desde lo público las radios.

Por último **Jeanine El Gazi** ha contextualizado la **historia legal de las radios comunitarias en el país. **

Las dos últimas conferencias se han referido a los **índices de libertad de expresión en Colombia**, en las que ha participado **Edison Lanza relator de la CIDH** para la libertad de expresión en Colombia, quién ha alertado del riesgo de la profesión periodística en Colombia y de los **asesinatos de periodistas** que según los informes de la CIDH han **quedado en la impunidad.**

Con esta última jornada ha **finalizado el foro** con gran asistencia de público y de ponentes tanto nacionales como internacionales, en los que se han tratado temáticas como los medios de comunicación comunitarios y radios, su papel en el conflicto y en un futuro escenario de preacuerdo, la incidencia de las pautas publicitarias en Colombia, la situación de la libertad de expresión en el país.

También hemos podido escuchar distintos **ejemplos de regulaciones de los medios comunitarios a nivel internacional** y experiencias de medios comunitarios en conflictos como **Sudáfrica o en transiciones como la Chilena** con el objetivo de poder enriquecer el debate para Colombia.

Entrevista con **Edison Lanza**, relator de la CIDH para la libertad de expresión en Colombia:

<iframe src="http://www.ivoox.com/player_ek_4468846_2_1.html?data=lZmjmp2Yeo6ZmKiakpuJd6KpmJKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRic%2Fo08rjy9jYpYzX0NOYp8nNt9DijLHO0N%2FFb8XZjNHOjaitiKmfzc7Px9fYpcWfxcqYx93Uto6ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

Entrevista con **Jeanine El Gazi**, antropóloga especialista en comunicación:

<iframe src="http://www.ivoox.com/player_ek_4468835_2_1.html?data=lZmjmp2XeY6ZmKiakpqJd6KolJKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRic%2Fo08rjy9jYpYzX0NOYrMrFssrixpCyzpCrpdvdjKbb1tfTtIa3lIqvldHTq8KfxtOYxdTRuc%2FdxMaah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

Entrevista con **Miguel Córdoba**, de AMARC-Colombia (Asociación mundial de radios comunitarias):

<iframe src="http://www.ivoox.com/player_ek_4468841_2_1.html?data=lZmjmp2YdY6ZmKiakpuJd6KmmJKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRic%2Fo08rjy9jYpYzX0NOYr87LucbgjKiSpZiJhpTmxdTPw5DIqYy1rqa%2FpZKJe6ShpNTb1sbLrdCfs8bRy9SPcYarpJKh&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>
