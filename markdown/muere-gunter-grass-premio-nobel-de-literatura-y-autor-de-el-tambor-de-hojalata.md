Title: Muere Günter Grass , premio nobel de literatura y autor de "El Tambor de Hojalata"
Date: 2015-04-13 15:20
Author: CtgAdm
Category: El mundo, Otra Mirada
Tags: "El Tambor de Hojalata", Günter Grass contra políticas Bush, Günter Grass miembro de las SS nazis, Muere Günter Grass
Slug: muere-gunter-grass-premio-nobel-de-literatura-y-autor-de-el-tambor-de-hojalata
Status: published

###### Foto:Badische-Zeitung.de 

A los 87 años ha fallecido el **premio nobel de literatura** **Günter[ ](http://es.wikipedia.org/wiki/G%C3%BCnter_Grass)Grass** en la localidad de Lübeck (Alemania). Dejando como principal legado la obra “**El Tambor de Hojalata”**, siempre se ha caracterizado por ser un escritor polémico por sus críticas pero también por su participación en la Segunda Guerra Mundial en la fuerza alemana de aviación. Posteriormente en su autobiografía declaró que había sido miembro en **su juventud de las “SS**”, unidad paramilitar nazi.

Su obra maestra **“El Tambor de Hojalata”,** obra inspirada en la **Segunda Guerra Mundial**,  y en la Alemania del nacionalsocialismo donde la figura del hombre-niño **Oscar Matzerath** representa la tenebrosidad de esta etapa histórica de Alemania.

La obra presentada en la postguerra **provocó fuertes críticas y elogios** por parte de las distintas facciones políticas de entonces.

En la obra el protagonista crece hasta los tres años, luego su cuerpo deja de desarrollarse pero su mente continúa. Es entonces cuando le regalan un tambor de hojalata que seguiría presente hasta la muerte de su padre cuando se libera de este dejándolo en su tumba.

El **tambor representa los albores del nazismo como metáfora** de lo que se estaba gestando en la Alemania de principio de siglo, el mero hecho de no crecer representa el estancamiento político de aquella época.

Escenas como cuando el niño se une a una banda de delincuentes **y rompe los escaparates gritando, alegoría de la “noche de los cristales rotos”**, cuando miles de judíos fueron perseguidos, o cuando comienza a trabajar con una banda de enanos circenses para los nazis, que luego más tarde ayudarían a escapar de un ataque.

El autor siempre ha sido un **firme defensor de la socialdemocracia en Alemania**, ha **criticado** duramente las políticas del **padre y el hijo Bush**, calificándolos como terroristas.

Grass escribió un libro sobre **el papel de Israel y la amenaza que constituía que crease una bomba atómica, siendo este considerado como persona non-grata por el estado sionista**.
