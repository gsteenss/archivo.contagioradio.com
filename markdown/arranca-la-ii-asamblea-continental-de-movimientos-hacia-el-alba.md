Title: Inicia la II Asamblea Continental de Movimientos hacia el ALBA
Date: 2016-11-30 23:11
Category: El mundo, Movilización
Tags: alba, construcción de paz, II Asamblea Continental del ALBA, Movimientos Sociales Latinoamericanos
Slug: arranca-la-ii-asamblea-continental-de-movimientos-hacia-el-alba
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/alba.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: ALBA] 

###### [30 Nov 2016] 

Del 1 al 4 de Diciembre Colombia será escenario de la II Asamblea Continental de Movimientos hacia el ALBA, en dicho encuentro **más de 200 organizaciones sociales y populares construirán alrededor del fortalecimiento de la unidad nacional y regional,** las formas de resistencia, la movilización popular, la defensa de las reivindicaciones de las comunidades y un proyecto de integración popular que plantee una forma diferente de hacer democracia.

Previo al inicio de la II Asamblea, algunos líderes como Marylen Serna de Congreso de los Pueblos, Nalu Farias de la Marcha Mundial de Mujeres de Brasil, Gloria Ramírez de la Marcha Patriótica y Manuel Bertoldi del Movimiento Popular Patria Grande de Argentina, dieron a conocer los objetivos, fundamentos y el por qué de este nuevo encuentro continental.

Manuel Bertoldi destacó que la II Asamblea está dedicada a la memoria de líder de la revolución cubana Fidel Castro, “quien dedicó toda su vida a la lucha de los menos favorecidos y se empeñó **en construir alternativas que fueran puente entre los pueblos latinoamericanos”**

De igual forma, los líderes anteriormene mencionados, resaltaron que se escogió a Colombia como escenario del encuentro debido a la **coyuntura que atraviesa el país respecto a los acuerdos de Paz y la arremetida de grupos paramilitares que ha cobrado la vida de más de 125 líderes** sociales en los últimos meses.

Marylen Serna, Gloria Ramírez y Nalu Farias fueron enfáticas en destacar la importancia del papel de las mujeres en el **direccionamiento de propuestas encaminadas a la defensa de los derechos humanos y los territorios, la transformación de las políticas públicas y en la construcción de Paz.** Por su parte Bertoldi resalto que “la paz de Colombia es la paz de todo el continente”.

Nalu Farias aseguró que otro punto fundamental para que haya una paz real en América Latina es que se de reconocimiento y se protejan los derechos de las mujeres, bien sean rurales o urbanas, frente a ello, mientras se desarrollaba la apertura, **mas de 60 mujeres de 19 países, realizaron el primer Encuentro de Feminismos y Mujeres de Alba Movimientos** en el que construyeron una agenda que va de la mano con los objetivos de la Asamblea, pero con un enfoque desde las mujeres y la diversidad de género.

###### Reciba toda la información de Contagio Radio en [[su correo]
