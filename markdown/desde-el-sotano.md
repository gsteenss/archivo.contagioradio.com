Title: Desde el sótano
Date: 2018-11-09 15:33
Author: JOHAN MENDOZA TORRES
Category: Johan Mendoza, Opinion
Tags: izquierda, Marchas, protesta, Revolucion
Slug: desde-el-sotano
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/11/FUEGO-770x400.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Contagio Radio 

#### **Por: [Johan Mendoza Torres](https://archivo.contagioradio.com/johan-mendoza-torres/)** 

###### **9 Nov 2018**  


[Según Schopenhauer toda verdad pasa por tres etapas. En la primera es ridiculizada. En la segunda es criticada y en la tercera es aceptada como autoevidente. No sé si exista una etapa intermedia entre la segunda y la tercera, una que permita definir cómo se ven de extraños los más salvajes defensores del statu quo, cuando el propio sistema que defienden les da palizas, los humilla, los denigra, pero les proporciona unos grados de bienestar lo suficientemente baratos como para ocultar la frustración y reforzar ese sadomasoquismo enfermizo.]

[No sé si exista una etapa entre la segunda y la tercera, en la que se pueda inscribir a ese cuerpo de personajes que se ofuscan hasta el infinito cuando se les atenta contra falsos epítetos de “revolucionarios” o cuando se les propone que se enfrenten a la penosa verdad (ridícula aún para ellos) de que la moral de un grupo dominante los tiene retenidos sin la fuerza, que reproducen esa moral en cada palabra o acto que realizan, y no alcanzan a reconocer que no sirve de nada pensar la solución a través del código de conducta del que domina.]

[El descaro impune, la desfachatez jurídica, el aumento de impuestos, las medidas absolutamente mezquinas que son contrarias a los esfuerzos sociales y que favorecen a quienes se encuentran hoy en el poder, parece que no tienen límite previsible. Incumplieron promesas de campaña (como es habitual) y ahora miles se rasgan las vestiduras, o prefieren desembocar su rabia en el que votó por Duque o contra el que votó en blanco… y si somos francos ¿ya para qué? Rabia contra rabia solo fortalece el sistema de creencias de las partes en discusión, lo que constituye un camino sin salida hacia un conflicto destructivo, por lo menos muy evidente en las redes sociales.]

[Claro, es cierto, todavía no deja de sorprender que un tipo como Duque esté en el poder, no por una razón personal (yo insisto en que el tipo habla con más fluidez y cuerpo que Juan Manuel Santos), sino que es sorprendente cómo su figura (la de un desconocido impulsado por un poderoso) desfiguró incluso hasta al mismísimo marketing político.]

[Rasgarse las vestiduras no sirve de nada porque el problema coyuntural no radica en definir quién es el más bravero, quien insulta más por redes sociales, quién es el más decente ante los medios de comunicación masiva, quién ofende menos a los que dominan o quién es el más racional en sus conclusiones. No, no, no, aquí el problema es grave: estamos ante una parálisis estructural de la crítica.  ]

[Nos encontramos ante la imposibilidad de pensar una solución porque la médula encargada de la producción de ideas revolucionarias está congelada por ese viento helado del conformismo placentero y efímero. La imposibilidad de pensar la solución se une a la práctica mal sana de pensar soluciones ofrecidas por el poder, con el agravante de que son aceptadas no desde la racionalidad, sino desde una moral oculta y no develada que ha transformado la posibilidad de un pensamiento revolucionario en simples manifestaciones filantrópicas, manifestaciones cívicas, ordenadas, muy bellas, muy aceptadas, muy voluntarias, pero nunca conscientemente necesarias.]

[Ni hablar del inconformismo utópico de no pocos personajes que critican todas las luchas sociales, desplazando emociones reprimidas de adentro hacia afuera, tratando en vano de ocultar lo ridícula y desnuda que se ve su frustración no resuelta, o tratando de ocultar su propia humillación vestida con la ropa prestada de los valores que son propios los que dominan, y que al saber que no podrá poseerlos nunca, los compra en el mercado creyendo que semejante vulgaridad puede aumentar su estatus.]

[Las ideas revolucionarias hoy se hallan en el sótano de la historia. Es la verdad. Una verdad que bien pudo ser ridiculizada, criticada, pero que con la victoria de Bolsonaro, Duque, y lo que viene con unos Macías, Paloma y Hoyos en el poder, permitirá aceptar esa verdad como autoevidente. No obstante, al sótano no se debe bajar voluntariamente, se debe bajar necesariamente… el sótano es un escenario oscuro, tranquilo y silencioso que permite recomponer los pensamientos, construir una estrategia.  ]

[De seguro, las ideas revolucionarias pronto resurgirán auténticas, eso es una verdad. Puede que hoy esa verdad se encuentre en la etapa de ser ridiculizada, no obstante, más pronto que tarde y ante las condiciones que oprimen a millones de personas, esa verdad será criticada con fuerza, y allí cuando tengamos la fuerza real (y no solo las ganas) para repeler esa fuerza, entonces será una verdad autoevidente.]

[Desde el sótano pensaremos en cómo superar esa crisis ideológica que ha convertido hasta al más existencialista en un mamarracho neoliberal, que ha transformado a casi todas las fuerzas políticas en manifestaciones liberales vestidas de todos los colores. Desde el sótano pensaremos con nuestro propio criterio y no bajo los principios morales que hoy dominan.]

[Desde el sótano nos organizaremos de nuevo y saldremos. No reventaremos el sistema desde adentro porque eso, para poner el dedo en la llaga que aún nos duele: eso es pura mierda. Lo reventaremos desde afuera, alejándonos lo más posible, de la forma en la que aquellos mezquinos empresarios, terratenientes y educados corruptos que tienen el poder en Colombia, piensan la vida y la obra humana.]

[Arriba lo que sobran son las ganas, y con las ganas no alcanza para cambiar la situación que vive Colombia. Arriba tal parece que el deseo supera lo que se desea y por eso no está funcionando nada. En el sótano, las ganas son efímeras por eso no son de fiar, pero la estrategia que resulte producto de la recomposición de ideas realmente revolucionarias nos dará fuerza para salir de allí y permanecer. “Verdad ridícula” dirán algunos; “verdad criticable” dirán otros cuando sigan intentado en vano cambiar las cosas sin librarse de la moral dominante; “verdad evidente” diremos cuando llegue el momento. ¡Ánimo!]

#### [**Leer más columnas de Johan Mendoza Torres**](https://archivo.contagioradio.com/johan-mendoza-torres/)

------------------------------------------------------------------------

###### Las opiniones expresadas en este artículo son de exclusiva responsabilidad del autor y no necesariamente representan la opinión de Contagio Radio.
