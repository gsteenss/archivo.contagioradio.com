Title: La corrupción, un obstáculo para la Paz en Colombia
Date: 2017-01-25 18:14
Category: Economía, Nacional
Tags: Corrupción en Colombia, Implementación de los Acuerdos de paz, Índice de Percepción de Corrupción
Slug: la-corrupcion-un-obstaculo-para-la-paz-en-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/01/corrupcion.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: El Espectador] 

###### [25 Ene 2017] 

Con una calificación de 37 puntos sobre 100, y ocupando el puesto número 90 de 176 países valuados, por tercer año consecutivo Colombia recibe una baja calificación en el Índice de Percepción de Corrupción –IPC– que elabora cada año Transparencia Internacional. El informe advierte que desde 1995 el fenómeno de la corrupción en Colombia, va en aumento.

### Campañas políticas y corrupción 

Andrés Hernández Codirector de la Corporación Transparencia por Colombia, menciona que los controles por parte de la autoridad electoral son muy débiles, recuerda que en las últimas elecciones de mandatarios locales más del 60% de los recursos reportados por los candidatos fueron registrados como “recursos propios” y 25% como “donaciones y créditos de personas naturales y jurídicas”. Le puede interesar: [“Elecciones constatan la degradación y](https://archivo.contagioradio.com/elecciones-constatan-el-grado-de-degradacion-y-corrupcion-del-sistema-electoral-colombiano/)corrupción[del sistema electoral colombiano”.](https://archivo.contagioradio.com/elecciones-constatan-el-grado-de-degradacion-y-corrupcion-del-sistema-electoral-colombiano/)

En esa misma línea, estudios recientes de la Corporación revelan que más del 90% de los empresarios del país “reconoce que se ofrecen sobornos en el entorno de los negocios” y el promedio de los sobornos “puede llegar hasta el 17.3% de los valores de los contratos (…) más del 50% de los encuestados consideran que el financiamiento de campañas políticas es una forma de sobornar”.

Por su parte Marcela Restrepo, también integrante de la Corporación Transparencia por Colombia, manifestó que los recientes escándalos que involucran al sector privado “obligan a atacar de manera definitiva los vínculos entre contratación pública, financiamiento de la política y corrupción (…) países como Brasil mejoraron en temas de corrupción cuando empezaron a caer las grandes cabezas del gobierno", puntualizó. Le puede interesar: [Odebrecht la punta del iceberg de la](https://archivo.contagioradio.com/odebrecht-la-punta-del-iceberg-de-la-corrupcion-el-colombia/)corrupción[en Colombia.](https://archivo.contagioradio.com/odebrecht-la-punta-del-iceberg-de-la-corrupcion-el-colombia/)

### Corrupción y Paz 

Hernández, indicó que el sector público en Colombia “es propenso a las prácticas corruptas”, señala que la corrupción es uno de los principales “obstáculos para la implementación del acuerdo general y el logro de una paz estable y duradera” y resalta que urge un aumento en la eficacia de investigaciones y sanciones por parte del Estado.

Así mismo, el informe destaca que durante varios años la Fiscalía y la Procuraduría “han reportado preocupantes riesgos de corrupción sin tomar medidas de fondo para superarlos”, Hernández recalca que en un monitoreo inicial realizado por la Corporación entre 2010 y 2016, en 9 de los 125 departamentos priorizados como zonas de posconflicto por la ONU, hubo “más de 70 casos que involucran a más de 100 servidores públicos investigados y sancionados”.

En una de las mediciones del Índice de Transparencia de las Entidades Públicas, entre 2014 y 2015, la Corporación revela que la Fiscalía obtuvo un 62,5/100 y la Procuraduría 68,4/100, quedando ubicadas en “Riesgo Medio de Corrupción” y la Contraloría, logró una calificación de 74,7/100, quedando en un nivel de “Riesgo Moderado”.

El informe resalta que figuras como la Fiscalía, Contraloría y Procuraduría “son las instituciones que deben liderar y ejercer sus funciones con capacidad, idoneidad e independencia de intereses políticos y partidistas”.

### ¿Cómo luchar contra la corrupción? 

Hernández, dice que hay dos puntos clave para tratar de mitigar dicha práctica, 1. Poder conocer cómo está operando la corrupción de aquellas empresas públicas o privadas, que optan por esta práctica y las redes, “incluso criminales”, que se relacionan con estas y 2. Lograr romper el círculo vicioso entre financiamiento de campañas políticas y la devolución de dichos favores a través de contratos y beneficios políticos.

Tanto el informe como la Corporación, hacen un llamado para que la voluntad política que enuncia el Estado “se traduzca en reformas internas institucionales que ayuden a que las entidades de control no se vean permeadas por prácticas corruptas” y no permita que los casos de corrupción revelados en los últimos meses queden en la impunidad.

Por último, Andrés Hernández hace hincapié en que son 3 los puntos clave para la lucha contra la corrupción**,** depurar los órganos de control, combatir efectivamente la corrupción en el sector privado, definir políticas e implementar medidas explícitas anticorrupción en el marco del posconflicto.

### Listado de los 176 países evaluados 

<iframe style="border: none;" title="CPI 2016 table, with '15-'12 scores" src="//e.infogr.am/86e6af1c-8d69-4adb-b50f-e2c2ebf6e2a0?src=embed" width="750" height="581" frameborder="0" scrolling="no"></iframe>

### Mapa Interactivo de países evaluados 

<iframe style="border: none;" title="CPI 2016 map (FINAL)" src="//e.infogr.am/ed08f0e3-9bc7-47b2-b980-d8936b8dad54?src=embed" width="750" height="797" frameborder="0" scrolling="no"></iframe>  
<iframe id="audio_16654134" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_16654134_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
