Title: Hay un balance positivo pero cauteloso de avances en Jornadas de Cumbre Agraria
Date: 2015-09-04 16:23
Author: AdminContagio
Category: Movilización, Nacional, Resistencias
Tags: Agroindignados, César Jeréz, Cumbre Agraria, etnica y popular, Hidroituango, incumplimientos del Gobierno, Movilización, Movimiento de zonas de reserva campesina, Movimiento Ríos Vivos, ONIC
Slug: hay-un-balance
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/11934491_10153021054300812_5511336828768972902_o.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Contagioradio 

<iframe src="http://www.ivoox.com/player_ek_7919732_2_1.html?data=mJ6em5yXdo6ZmKiakpuJd6KllJKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRjMLtjNrbjcfFsMLixMqah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [César Jeréz, ANZORC] 

###### [4 Sep 2015] 

César Jeréz, integrante de ANZORC y vocero de la Cumbre Agraria, Étnica y Popular entregó un balance **positivo pero cauteloso**, por la experiencia de incumplimiento frente a lo que se ha adelantado con el gobierno en las **mesas de discusión y en la generación de un diálogo entre el campo y la ciudad** y las posibilidades de que el gobierno firme acuerdos y proyectos concretos.

De las  discusiones que se han adelantado con el gobierno Jerez destaca la reunión que se sostuvo el día de ayer con el Ministro de agricultura Aurelio Iragorri, quien **ha aceptado los incumplimientos por parte del gobierno y ha dicho que esta vez los acuerdos si se cumplirán** “hay que reconocerlo, el ministro está asumiendo más compromisos”, indica el vocero, destacando la gente se ha dado cuenta del incumplimiento del gobierno.

Jeréz enfatiza que las actividades y movilizaciones que se han realizado en las localidades de la ciudad, han funcionado para **entablar un diálogo entre la ciudad y el campo,** visibilizando así la profunda crisis que viven los distintos sectores y comunidades colombianas.

**Sin embargo pese a estos avances continúan en distintas zonas del país denuncias por la persistencia de los planes de erradicación de los cultivos de uso ilícito** sin cumplir con los acuerdos para la sustitución voluntaria y otras denuncias por la continuidad de la **política minero energética que en Antioquia** deja miles de personas afectadas por la construcción de HidroItuango.

Ver también:

[Movimiento rios vivos bloquea entradas a hidroituango para frenar daños ambientale](https://archivo.contagioradio.com/movimiento-rios-vivos-bloquea-entradas-a-hidroituango-para-frenar-danos-ambientales/)s
