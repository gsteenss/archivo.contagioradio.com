Title: Asesinan a Tailor Gil, líder comunal de cáceres
Date: 2020-05-14 17:32
Author: AdminContagio
Category: Actualidad, Líderes sociales
Tags: asesinato, bajo cauca antioqueño, Cáceres, Líder
Slug: asesinan-a-tailor-gil-lider-comunal-de-caceres
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/05/Taylor-Gil.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:heading {"align":"right","level":6,"textColor":"cyan-bluish-gray"} -->

###### Foto: Contagio Radio {#foto-contagio-radio .has-cyan-bluish-gray-color .has-text-color .has-text-align-right}

<!-- /wp:heading -->

<!-- wp:paragraph -->

Organizaciones sociales denunciaron que el pasado martes fue asesinado en su casa el líder comunal Tailor Gil, delegado la Junta de Acción Comunal (JAC) del barrio La Gloria del corregimiento de Puerto Bélgica en Cáceres, Antioquia. (Le puede interesar: ["Control de AGC se consolida en cuarentena en Bajo Cauca Antioqueño"](https://archivo.contagioradio.com/control-de-agc-se-consolida-en-cuarentena-en-bajo-cauca-antioqueno/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Según las primeras informaciones, el hombre de 47 años fue abordado en su casa por dos hombres armados que le dispararon en múltiples ocasiones. Tailor Gil era propietario de una tienda, y hasta el momento se desconoce si había recibido amenazas por parte de actores armados. (Le puede interesar: ["Asi es hacer periodismo en el Bajo Cauca Antioqueño"](https://archivo.contagioradio.com/asi-es-hacer-periodismo-en-el-bajo-cauca-antioqueno/))

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Bajo Cauca, una zona de peligro para líderes sociales

<!-- /wp:heading -->

<!-- wp:paragraph -->

Recientemente, organizaciones defensoras de derechos humanos denunciaron el aumento del control territorial por parte de las autodenominadas Autodefensas Gaitanistas de Colombia (AGC) en Tarazá, también en el Bajo Cauca Antioqueño. Pero la situación no es nueva, hace parte de una confrontación por dominar un territorio que es clave por su localización.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Según organizaciones del territorio, la disputa por la zona entre grupos como las AGC, los Caparrapos o el Nuevo Frente 18 se explica en que el territorio es un paso importante hacia el mar, además de estar próximo al Nudo del Paramillo. Por ello, los actores ilegales establecen distintas formas de control y amedrantamiento a las comunidades y sus líderes, que son vistos como una amenaza al ejercicio violento que realizan.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

También lo invitamos a consultar: [Se libra una guerra por tomar el control del Nudo de Paramillo](https://archivo.contagioradio.com/guerra-control-nudo-paramillo/).

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
