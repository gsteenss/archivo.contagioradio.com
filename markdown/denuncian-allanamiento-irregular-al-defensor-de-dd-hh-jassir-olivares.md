Title: Denuncian allanamiento “irregular” al defensor de DD.HH. Jassir Olivares
Date: 2020-11-03 17:26
Author: AdminContagio
Category: Actualidad, Nacional
Tags: Defender la Libertad, Jassir Olivares, Persecución Judicial
Slug: denuncian-allanamiento-irregular-al-defensor-de-dd-hh-jassir-olivares
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/11/Denuncian-allanamiento-contra-Jassir-Olivares.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

La Campaña Defender la Libertad denunció sobre un **allanamiento realizado por miembros de la DIJIN en la residencia del defensor de Derechos Humanos, Jassir Olivares,** quien además es miembro de esa organización, señalándolo como un posible **“*acto de persecución contra su liderazgo y activismo*”.** (Lea también: [Si protestar es un delito nos entregamos a la Fiscalía: líderes y activistas políticos](https://archivo.contagioradio.com/lideres-sociales-entrega-voluntaria-fiscalia-derecho-protesta/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Según la denuncia el allanamiento se realizó en la ciudad de Barranquilla, alrededor de las 6:30 de la mañana de este martes 3 de noviembre. En la inspección los agentes de la DIJIN argumentaron “*que su accionar obedece a la búsqueda de elementos digitales contra un familiar suyo*”.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Sin embargo, Defender la Libertad señaló que hubo una especial inspección y fijación por parte de los agentes, en sus elementos de estudio y trabajo “*situación que es irregular para la familia Olivares*”.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Adicionalmente, **la Campaña Defender la Libertad, alertó que este allanamiento podría constituirse como “*un acto de persecución contra su liderazgo y activismo como defensor de derechos humanos*”;** si se tiene en cuenta que anteriormente, Jassir Olivares ya había sido detenido en medio de una manifestación que se presentó en la Universidad del Atlántico en el año 2019; sin respetar que este portaba el chaleco de la Campaña que lo acreditaba como defensor de DD.HH.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

[Defender la Libertad](https://defenderlalibertad.com/atencion-denuncia-publica/) exigió a la Fiscalía dar respuesta sobre el allanamiento que calificaron como una “*situación anómala*” y adicionalmente, instó a las autoridades judiciales a actuar conforme a las garantías procesales dispuestas legalmente; **alertando a la comunidad nacional e internacional sobre el “*riesgo al que han sido expuestas las personas que han padecido de allanamientos injustificados y/o arbitrarios*”.** (Lea también: [El aparato del Estado se revistió de legalidad para afectar la Movilización: Análisis](https://archivo.contagioradio.com/aparato-estado-revistio-legalidad-afectar-movilizacion/))

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/DefenderLiberta/status/1323619969297453058","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/DefenderLiberta/status/1323619969297453058

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

Frente a este hecho, la Federación de Estudiantes Universitarios -FEU-; activó una alerta temprana por las posibles violaciones y vulneraciones a los derechos humanos de las que pudieran ser víctimas el defensor Jassir Olivares, su familia y sus allegados, responsabilizando a las autoridades locales y nacionales sobre la eventual ocurrencia de lo alertado.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/FEUColombia/status/1323653490879668224","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/FEUColombia/status/1323653490879668224

</div>

</figure>
<!-- /wp:core-embed/twitter -->
