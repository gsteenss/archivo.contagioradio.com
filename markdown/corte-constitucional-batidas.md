Title: Corte Constitucional se pronuncia sobre objeción de conciencia y "batidas"
Date: 2015-01-28 21:17
Author: CtgAdm
Category: DDHH, Otra Mirada
Tags: Antimilitarismo, batidas ilegales, objeción de conciencia
Slug: corte-constitucional-batidas
Status: published

###### Foto: Diario ADN 

##### **[Entrevista a Mario Cardoso:]** 

##### <iframe src="http://www.ivoox.com/player_ek_4010214_2_1.html?data=lZWekpeVeI6ZmKiakpqJd6KompKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRh9Dm1cqYxdTSt9Xd1drQy9TSpc2f0dfcys7GqYzgwtiYh5eWpsLoysnO1YqWdoztjNfSxdTSs8TZjNGah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

A raíz de la revisión de la sentencia contra dos jóvenes objetores de conciencia en Medellín, uno de ellos reclutado ilegalmente en un autobús mediante una batida.

La Corte concreta la interpretación de la sentencia T-455 (sobre batidas en referencia al reclutamiento indiscriminado) de 2014, dejando claro que es imposible realizar esta práctica.

Impidiendo que instancias militares la acomoden a sus propios intereses y , aclarando así, aspectos importantes de la propia ley en referencia a la forma en la que se debe de realizar la propia práctica del reclutamiento.

De esta manera soluciona la situación de cientos de jóvenes en todo el país que han sido víctimas del reclutamiento ilegal mediante la práctica de las batidas y desenmascara una realidad que ha marcado el servicio militar obligatorio y la propia dinámica del reclutamiento en el conflicto armado.

También la Corte se pronuncia a favor de la objeción de conciencia, aclarando que es el propio ejército quién debe respetar la acreditación que la persona que lo solicite adjunte ante las correspondientes instancias.

Anteriormente las solicitudes se archivaban o denegaban, ahora se deberán resolver en 15 días y será imposible denegarlas aduciendo que no existe regulación.

[Fragmento de la sentencia T-455: "]...*adelantar, autorizar, ordenar o permitir redadas o batidas indiscriminadas, dirigidas a identificar a los ciudadanos que no han resuelto su situación militar, con el objeto de conducirlos a unidades militares u otros sitios de concentración, para que presten servicio militar*..."

Para poder comprender más a fondo las consecuencias a futuro de la sentencia y la valoración sobre la lucha de colectivos antimilitaristas, congresistas y defensores de derechos humanos, entrevistamos a Mario Cardoso, miembro de la asociación ACOC, (Acción colectiva de objetores y objetoras de conciencia).
