Title: Organizaciones de Víctimas piden audiencia ante CIDH y rechazan propuestas del NO
Date: 2016-10-26 13:32
Category: DDHH, Nacional
Tags: #AcuerdosYA, defensores de derechos humanos, Movimiento de víctimas
Slug: organizaciones-de-victimas-piden-audiencia-ante-cidh
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/10/victimas.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [26 de Oct 2016] 

Organizaciones de víctimas y defensoras de Derechos Humanos, en compañía de los representantes Alirio Uribe y Ángela María Robledo, informaron que **pedirán que se realice una audiencia en la Corte Interamericana de Derechos Humanos para exponer su preocupación frente a las propuestas de los sectores que apoyaron en el No** y que de ser aceptadas perpetuarían la impunidad para las víctimas en Colombia, debido a que son laxas con los crímenes de agentes del Estado y particulares.

En Colombia más del **98%** de los crímenes cometidos por agentes del Estado están en la impunidad. Delitos cómo la desaparición forzada, con **45.944** víctimas directas, han sido atribuidos en un **84%** a grupos paramilitares y miembros de la Fuerza Pública, a su vez hay **4.475** víctimas de ejecuciones extrajudiciales todas atribuidas a agentes estatales. Le puede interesar: ["Más de 500 organizaciones de víctimas respaldan jurisdicción especial para la paz"](https://archivo.contagioradio.com/mas-de-500-organizaciones-de-victimas-respaldan-jurisdiccion-especial-para-la-paz/)

De acuerdo con la abogada Soraya Gutiérrez, directora del Colectivo de Abogados José Alvear Restrepo, propuestas como las del congresista Uribe “lo que pretenden es precisamente sustraer de la justicia a los agentes del Estado y **están dirigidas a desconocer los derechos de las víctimas y particularmente los de las víctimas de crímenes de Estado**”.

Para las organizaciones defensoras de Derechos Humanos, e**l Sistema Integral de Justicia y Reparación, producto de la comisión técnica,  permitía avanzar en la superación de la impunidad** en crímenes cometidos no solo por la guerrilla de las FARC-EP, sino también por el Estado, sus fuerzas militares, los grupos paramilitares y los particulares que participaron en el conflicto armado.

Diana Gómez representante del Movice, expresó “consideramos que **medidas que buscan proteger a los agentes estatales, a empresarios y políticos no favorece el derecho a la verdad de las víctimas y reitera un pacto de silencio**” y agregó “muchas de las víctimas no estamos buscando la cárcel como mecanismo de justicia sino poder conocer la verdad”. Le puede interesar:["Propuestas de Uribe no tienen en cuanta a las víctimas".](https://archivo.contagioradio.com/propuestas-de-uribe-no-nos-tienen-en-cuenta-a-las-victimas/)

**Ley de Justicia y Paz (975)**

Frente a la propuesta que hizo el Fiscal General Néstor Humberto Martínez de aplicar la ley de Justicia y Paz al proceso en la Habana, la abogada Soraya Gutiérrez afirmó que **después de 10 años de aplicación solo tiene 40 sentencias**, y frente a las 15 mil copias que se han compulsado y que vinculan la responsabilidad de políticos, militares y empresarios en todo lo que fue el accionar de estructuras paramilitares siguen sin arrojar resultados de investigación y sanción contra responsables.

###### 

### **Texto comunicado de prensa Organizaciones Defensoras de Derechos Humanos**

Organizaciones de víctimas y defensores de derechos humanos solicitaron a la CIDH una audiencia temática para exponer sus preocupaciones respecto al desconocimiento de los estándares internacionales de justicia que se evidencian en las propuestas de los promotores del No.

El pasado 2 de octubre, con un alto índice de abstención y por una estrecha minoría, los colombianos votantes del plebiscito refrendatorio rechazaron el “Acuerdo Final para la terminación del conflicto y la construcción de una paz estable y duradera” suscrito el 26 de septiembre entre el Presidente de la República, Juan Manuel Santos, y el Comandante de la guerrilla FARC-EP, Rodrigo Londoño.

Uno de los aspectos que mayor sensibilidad generó en la opinión pública fue el acuerdo sobre el Punto 5, Víctimas, que desarrolla un Sistema Integral de Verdad, Justicia, Reparación y Garantías de No Repetición, SIVJRGNR. Los resultados negativos del plebiscito dan cuenta de que en la sociedad y en la opinión pública no se reconoció adecuadamente la importancia de lo pactado para que las víctimas tuvieran la posibilidad de ver realizados sus derechos, con lo cual al final, el resultado de los comicios del pasado 2 de octubre, afectó justamente a las víctimas, a quienes se negó la posibilidad de activar los mecanismos que establece el Acuerdo.

Este resultado se debió en parte a que los mensajes hacia el electorado estuvieron centrados en la imagen desfavorable de la guerrilla, invisibilizando la existencia de crímenes cometidos por agentes estatales.

Al respecto, el informe ¡Basta Ya! del Centro de Memoria Histórica demuestra que la violencia ha estado alimentada mayormente por la responsabilidad colectiva de la fuerza pública e individual de varios de sus agentes. Delitos como la desaparición forzada, con 45.944 víctimas directas, son atribuidos a grupos paramilitares y agentes de la fuerza pública en el 84% de casos. Las ejecuciones extrajudiciales, que entre 2002 y 2010 dejaron 4.475 víctimas, son todas ellas responsabilidad de agentes estatales1. Así mismo ocurre con los asesinatos selectivos en los que paramilitares y fuerza pública son responsables del 69% de los casos, o las 1.982 masacres cuya responsabilidad en el 56% de casos es atribuida a grupos paramilitares en asocio con la fuerza pública, autoridades civiles, y civiles financiadores del conflicto, mientras el 27% de los casos son atribuidos a autores desconocidos y el 17% a las guerrillas.

Teniendo en cuenta las cifras oficiales, no fueron los miembros de las FARC-EP los únicos responsables de crímenes graves en el contexto del conflicto armado. A pesar de ello, los promotores del No han reactivado la propuesta de un “alivio judicial” a integrantes de la fuerza pública, “alivio” que puede significar mayor impunidad y afectación a los derechos de las víctimas de crímenes cometidos por agentes estatales. Esta propuesta no sería aceptable tampoco para la comunidad internacional. Organismos internacionales revisan periódicamente la situación colombiana. Por ejemplo, en la actualidad la Comisión Interamericana de Derechos Humanos, CIDH, examina por lo menos 50 casos de ejecuciones extrajudiciales cometidas entre 2003 y 2008, mientras que la Corte Penal Internacional, CPI, continúa con su examen preliminar del que también hacen parte, entre otros, estos crímenes.

Por considerar que un “alivio judicial” no garantiza la rendición de cuentas de los responsables de la criminalidad estatal y de los financiadores del paramilitarismo, al no adecuarse a los estándares internacionales ni ser satisfactorio para las víctimas, quienes suscribimos este comunicado hemos solicitado formalmente a la CIDH la realización de una audiencia temática sobre los estándares de justicia que los Estados Parte en la Convención Americana de Derechos Humanos deben garantizar, aun en escenarios de justicia transicional.

Es una obligación de la mesa de La Habana reconocer y avanzar sobre lo acordado en materia de verdad, justicia, reparación y garantías de no repetición. El punto 5 de los acuerdos pondera de la mejor manera los derechos a la verdad, la justicia y la reparación con el objetivo de alcanzar una paz estable y duradera. Esperamos que las modificaciones del Acuerdo -que no pueden ser estructurales- respeten la centralidad y los derechos de las víctimas. Estaremos atentos a que las voces de quienes han padecido la guerra y la violencia sean escuchadas.

Finalmente, saludamos el inicio de la mesa de conversaciones entre el gobierno y la guerrilla del ELN. Esperamos que este proceso acoja los principios rectores definidos en el punto 5 de los Acuerdos de La Habana, tomando en cuenta que la arquitectura de la Justicia Especial para la Paz, JEP, y del SIVJRGNR son base fundamental para que los derechos de las víctimas sean reconocidos.

Organizaciones, defensoras y defensores firmantes:  
Alirio Uribe Muñoz – Representante a la Cámara por Bogotá  
Ángela María Robledo – Representante a la Cámara por Bogotá  
Asociación Minga  
Centro de Estudios Juan Gelman  
Centro de Investigación y Educación Popular, CINEP – Programa por la Paz  
Colectivo Socio Jurídico Orlando Fals Borda  
Comisión Intereclesial de Justicia y Paz  
Comité Permanente por la Defensa de los Derechos Humanos – CPDH  
Coordinación Colombia-Europa-Estados Unidos Corporación CAHUCOPANA Corporación Claretiana Norman Pérez Bello  
Colectivo de Abogados “José Alvear Restrepo”  
Corporación Jurídica Libertad  
Corporación Jurídica Yira Castro  
Corporación PODION  
Corporación Reiniciar  
EQUITAS  
Fundación Nydia Erika Bautista  
Fundación Comité de Solidaridad con los Presos Políticos – FCSPP  
Hijos e Hijas por la Memoria y contra la Impunidad  
Humanidad Vigente Corporación Jurídica  
Movimiento de Víctimas de Crímenes de Estado – MOVICE  
Vamos por los Derechos

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)
