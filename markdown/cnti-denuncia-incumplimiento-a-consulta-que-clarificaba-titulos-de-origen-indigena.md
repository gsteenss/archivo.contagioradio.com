Title: CNTI denuncia incumplimiento a consulta que clarificaba títulos de origen indígena
Date: 2020-11-05 18:22
Author: AdminContagio
Category: Actualidad, Comunidad
Tags: Comisión Nacional de Territorios Indígenas, pueblos indígenas, Títulos de origen colonial
Slug: cnti-denuncia-incumplimiento-a-consulta-que-clarificaba-titulos-de-origen-indigena
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/09/ambiente-y-comunidades-e1473705635492.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

Mediante un comunicado presentado este miércoles 4 de noviembre, [la Comisión Nacional de Territorios Indígenas – CNTI-](https://bit.ly/38c65nD) denuncia el **incumplimiento a la consulta previa que fue acordada para la creación de un procedimiento especial para la clarificación de títulos de origen colonial o republicano de resguardos indígenas.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El CNTI expresa inconformidad pues **se convocó a una reunión de socialización, improvisadamente y sin anticipación, este 5 de noviembre** por el director de Ordenamiento Social de la Propiedad Rural, y Uso Productivo del Suelo del Ministerio de Agricultura, Wilber Jairo Vallejo Bocanegra.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En la reunión se pretende discutir el proyecto de decreto «por el cual se adiciona el Capítulo 6 al Título 7 de la Parte 14 del Libro 2 del Decreto 1071 de 2015, para la clarificación de la vigencia legal de los títulos de origen colonial o republicano de los resguardos indígenas». (Le puede interesar: [Son asesinadas 5 personas en masacre de Nechí, Antioquia](https://archivo.contagioradio.com/son-asesinadas-5-personas-en-masacre-de-nechi-antioquia/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El Comité señala que es una falta de respeto que se hayan cancelado agendas preacordadas en CNTI, pero que **«hoy se  
conozca una sesión creada a última hora para dilatar el debido proceso y el acceso de los pueblos indígenas a un trámite especial para sus derechos territoriales».**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Además, agrega que el Gobierno pretende usar este reglamento jurídico como una excusa para negarles a las comunidades sus derechos, así como dejar en inseguridad jurídica los territorios para no cumplir con las deudas históricas que tiene el Estado con las comunidades. (También lea: [528 años después continúa el exterminio para razas y sectores sociales](https://archivo.contagioradio.com/528-anos-despues-continua-el-exterminio-para-razas-y-sectores-sociales/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

La expedición de este decreto fue un compromiso que el Gobierno adquirió a través de un proceso de consulta previa el 28 de diciembre de 2017 y actualmente, se espera la firma del mismo; una vez se realice este procedimiento, se socializará con las comunidades campesinas, negras y organizaciones sociales.

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
