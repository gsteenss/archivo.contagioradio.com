Title: En Fotos - Así avanza el Paro desde el Sur de Bogotá
Date: 2017-09-27 09:44
Category: Movilización, Nacional
Tags: Paro desde el Sur, Relleno sanitario Doña Juana
Slug: asi-avanza-el-paro-sur-en-bogota-galeria-de-imagenes
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/09/WhatsApp-Image-2017-09-27-at-8.12.18-AM-e1506522977405.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Brian gonzáles] 

###### [27 Sept 2017]

Desde diferentes puntos de la Capital, el día de hoy inicia el gran Paro-Sur, un llamado de más de 130 barrios ubicados en 4 localidades del sur de la ciudad: Bosa, Usme, Tunjuelito y Ciudad Bolívar, hacia la Alcaldía de Bogotá para exigirle soluciones inmediatas a las afectaciones que ha  **provocado el Relleno Sanitario Doña Juana y la falta de políticas públicas en salud, ambiente y juventud, que superen la desigualdad de esto territorios.**

Los habitantes que salieron de los Portales Usme, Tunal y Américas, denuncian la alta presencia de Fuerza Pública en estos lugares, sin embargo la movilización que llegará hasta la Autopista Sur, avanza en completa tranquilidad. (Le puede interesar:["Quieren debilitar paro del Sur con anuncio de orden de captura contra líderes"](https://archivo.contagioradio.com/quieren-debilitar-en-bogota-con-anuncio-de-orden-de-captura-contra-lideres/))

En Mochuelos Alto, campesinos y campesinas bajan de las veredas para llegar al mismo punto de encuentro, de igual forma estudiantes de universidades públicas comienzan a juntarse en sus instituciones para acompañar la movilización. (Le puede interesar: ["Distrito ampliaría hasta 2070 la existencia de Relleno Doña Juana"](https://archivo.contagioradio.com/distrito-ampliaria-hasta-2070-la-existencia-del-relleno-sanitario-dona-juana/))

De igual forma los líderes del paro han venido denunciado acciones en su contra para intimidarlos y atemorizar a la ciudadanía que se una a la movilización. De acuerdo con el comité de derechos humanos de Paro desde el Sur, cuenca del Tunjuelo prepárese, en diferentes medios de información se hablo de 15 órdenes de captura contra algunos de los voceros del paro, sin tener elementos probatorios y acusándolos de actos que no han cometido.

\

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
