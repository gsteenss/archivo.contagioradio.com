Title: Tres líderes sociales han sido asesinados en Cauca esta semana
Date: 2019-09-27 17:26
Author: CtgAdm
Category: DDHH, Nacional
Tags: asesinato, Cauca, Lider social, Organización Social
Slug: tres-lideres-sociales-han-sido-asesinados-en-cauca-esta-semana
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/09/Fotos-editadas2.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio  
] 

Desde el pasado miércoles 25 de septiembre, organizaciones sociales han estado denunciado el asesinato de líderes sociales, indígenas y excombatientes de FARC en el departamento de Cauca. De hecho tan solo en esta semana, la violencia ha cobrado la vida del líder Jairo Javier Ruiz Fernández en Balboa, el líder indígena Marlon Ferney Pacho en Tierradentro y el excombatiente Carlos Celimo Iter Conde en Caloto. (Le puede interesar:["«No nos dejen solos», la petición de líderes sociales del Chocó ante amenazas"](https://archivo.contagioradio.com/no-dejen-solos-lideres-sociales-choco/))

### **Carlos Celimo Iter, un excombatiente que había denunciado amenazas en su contra** 

Organizaciones defensoras de derechos humanos denunciaron el pasado miércoles 25 de septiembre el asesinato de **Carlos Celimo Iter Conde** , excombatiente de las FARC que llevaba adelante su proceso de reincorporación en zona rural del municipio de Caloto, Cauca. Carlos era el presidente de la junta directiva de la Cooperativa Multiactiva de Mujeres Víctimas del Conflicto Armado del Común (COOMEC), y estaba incluído en la solicitud de esquema colectivo para la organización.

Según las primeras informaciones, el excombatiente fue asesinado en la vereda Juntas, resguardo de Huellas por sujetos desconocidos; él pertenecía a la Nueva Área de Reincorporación del municipio de Caloto. Las organizaciones recordaron que recientemente Carlos había tenido una reunión con funcionarios de la Unidad Nacional de Protección (UNP) para hablar sobre las amenazas de las que había sido víctima. (Le puede interesar: ["Fue asesinado Gustavo Pérez, líder social y defensor de DDHH en Bolívar"](https://archivo.contagioradio.com/fue-asesinado-gustavo-perez-lider-social-y-defensor-de-ddhh-en-bolivar/))

### **Marlon Ferney Pacho, autoridad indígena de Tierradentro** 

Según denunció el Consejo Regional Indígena del Cauca (CRIC), aproximadamente a las 5 de la tarde del jueves 26 de septiembre, llegaron 4 hombres armados hasta la casa de **Marlon Ferney Pacho**, lo sacaron de su hogar y sin mediar palabra le propinaron varios disparos. Hermes Pete, consejero mayor del CRIC, afirmó que Marlon era un muchacho muy joven (24 años), era autoridad en su territorio, fungiendo como secretario del cabildo del resguardo de Talaga y tenía un gran liderazgo.

El Consejero declaró que desconocen a qué grupo pertenecían los hombres que atentaron contra la vida de Marlon porque en el territorio hay múltiples amenazas hacía los pueblos indígenas, sin embargo, hay elementos que apuntan a que se trató de "disidencia de las FARC", pues el grupo que realiza pintas en las paredes es "Dagoberto Ramos". Sobre los móviles del homicidio, Pete recordó que muchas de esas amenazas están relacionadas con "el control territorial" que realizan las autoridades indígenas.

Para concluir, el Consejero Mayor aseguró que las autoridades aún no han llegado al territorio para determinar cómo se realizarán las investigaciones; no obstante, resoponsabilizó al Gobierno Nacional por no cumplir con lo pactado en el Acuerdo de Paz, en referente a la protección territorial, así como no garantizar la vida a las comunidades en Cauca. (Le puede interesar: ["Líder y defensor de desplazados Yunier Moreno fue asesinado en Caquetá"](https://archivo.contagioradio.com/yunier-moreno-nuevo-atentado-acaba-con-la-vida-de-lider/))

### **Jairo Javier Ruiz Fernández, una sonrisa que la violencia le quita al Cauca** 

La Red de Derechos Humanos Francisco Isaías Cifuentes denunció que sobre las 8 de la noche del jueves 26 de septiembre, luego de haber acompañado una familia campesina a hacer unos trámites de salud, hombres armados atentaron contra el líder **Jairo Javier Ruiz Fernández** mientras se dirigía a su lugar de residencia en Balboa. Javier había acompañado la candidatura al Concejo Municipal de Balboa de Solmey Botina Cordoba, con quién había estado, antes de salir a acompañar la familia campesina.

Botina recordó que Javier Ruíz había sido uno de los fundadores de la Asociación Campesina de Trabajadores de Balboa (ASCATBA), "era un compañero muy activo, muy reconocido en el municipio, en las comunidades y en su forma carismática de apoyo cuando lo necesitaban". El candidato al Concejo de Balboa resaltó la sonrisa de Javier, que siempre estaba ahí para alegrar a las personas cercanas. (Le puede interesar: ["¿Qué está pasando en Cauca? análisis del líder campesino Óscar Salazar"](https://archivo.contagioradio.com/que-esta-pasando-en-el-cauca-un-analisis-del-lider-campesino-oscar-salazar/))

Adicionalmente, dijo que no conocían una amenaza concreta contra el líder, "pero si uno mira, aparecen panfletos por el departamento y por las redes, diciendo que van a atentar contra las asociaciones campesinas". Botina auguró más hechos de violencia contra las organizaciones sociales, por lo que pidió a las autoridades que brinden mayores medidas de seguridad para proteger la vida de los y las líderes sociales.

<iframe id="audio_42417298" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_42417298_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>  
**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
