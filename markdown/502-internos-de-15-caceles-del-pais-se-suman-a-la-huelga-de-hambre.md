Title: 502 internos de 15 cárceles del país se suman a la huelga de hambre
Date: 2016-02-16 16:28
Category: DDHH, Nacional
Tags: crisis carcelaria, Huelga de hambre, Profesor Miguel ángel Beltrán
Slug: 502-internos-de-15-caceles-del-pais-se-suman-a-la-huelga-de-hambre
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/02/Crisis-carcelaria1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: La Vanguardia 

<iframe src="http://www.ivoox.com/player_ek_10459289_2_1.html?data=kpWhl56WfJqhhpywj5aVaZS1kpqah5yncZOhhpywj5WRaZi3jpWah5yncZakk5DW0NnJts%2Fj1JDRx5CVeYzXhqigh6aVp8bgxtiYxsrQb9HVhqigh6aot4znxpDg19LFsozVjNHOjc3ZqY6ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Miguel Angel Beltrán] 

###### [16 Feb 2016] 

En el marco de la huelga de hambre que se realiza en diversas cáceles del país, esta semana se anunció que la huelga de hambre **alcanza a 506 internos en 15 centros penitenciarios y las medidas de desobediencia civil alcanzan a 4.470** privados de la libertad. Además la situación de cerca de 106 internos es crítica debido a la gravedad de su estado de salud y la falta de atención médica como el caso de José Angel Parra que lleva 17 días sin recibir los medicamentos para la leucemia que padece.

Desde hace más de 3 semanas, internos de diversas cárceles del país se mantienen en jornada de protesta frente al incumplimiento por parte del gobierno de Colombia, en los gestos de paz a los que se comprometió en Diciembre de 2015. Según los internos **de los 30 indultos solo se han ejecutado 26 y no se han realizado brigadas de salud ni concentración en patios especiales.**

A este respecto, el profesor **Miguel Angel Beltrán, se unió a partir de este martes a la huelga de hambre en solidaridad** con las personas que exigen cumplimiento por parte del gobierno nacional de los compromisos adquiridos. Beltrán, afirma que sin ser parte integrante de las FARC se une por la profunda crisis y las múltiples situaciones indignas que se viven a diario en las cárceles colombianas.

En el comunicado del profesor, difundido el lunes, recuerda que la situación es tan urgente como lo era en el caso de “John Jairo Moreno" quien "hace diez días murió en un hospital de Pereira esperando una acto de humanidad del gobierno, que ni siquiera le permitió terminar sus últimos días en compañía de su esposa y sus pequeños hijos”.

Así las cosas, los internos siguen solicitando la solidaridad nacional e internacional, puesto que **el INPEC, en palabras de ellos, ha intentado desprestigiar y negar la huelga masiva, la desobediencia civil y la huelga de hambre** que se adelanta, para no atender las exigencias de los internos.
