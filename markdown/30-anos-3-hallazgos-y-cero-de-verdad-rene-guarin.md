Title: 30 años, 3 hallazgos y cero de verdad: René Guarín
Date: 2015-11-03 17:16
Category: Nacional, Sin Olvido
Tags: Alfonso Plazas Vega, Armando Arias Cabrales, Desaparecidos del Palacio de Justicia, Juan Manuel Santos, misa por desaparecidos del palacio de justicia, Palacio de Justicia, René Gaurin, Victimas palacio de justicia
Slug: 30-anos-3-hallazgos-y-cero-de-verdad-rene-guarin
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/11/misa_palacio_justicia_contagioradio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: contagioradio.com 

###### [3 nov 2015] 

Ayer en la iglesia de San Gerardo Mayela **se realizó la misa convocada por los familiares de Luz Mary Portela de León, Cristina del Pilar Guarín y Lucy Amparo Ovideo**, desaparecidas durante la retoma del Palacio de Justicia, ocurrida en 1985 y de las cuales, 30 años después, se encuentran sus restos. Un primer momento de duelo de estas familias a pesar de que en 30 años, solo hay "3 hallazgos y cero de verdad".

La celebración religiosa, que se dio a partir de las 3 de la tarde contó con la presencia de varios de los familiares de las víctimas, sacerdotes y comunidad en general que decidió solidarizarse con el primer acto de duelo de estas tres familias, pero también con las familias de las víctimas de las que se desconoce el paradero.

René Gaurín, hermano de Cristina del Pilar, afirmó que en estos 30 años, se han logrado solamente 3 hallazgos, pero aún hay ausencia total de verdad, **"van 30 años, hay 3 hallazgos y cero verdad"** manifestó el familiar en medio de centenares de personas que lo escuchaban. Los familiares reiteraron la pregunta ¿Dónde están?

La celebración estuvo cargada de símbolos como el pan que se compartió con los asistentes, para simbolizar el cuerpo vivo de Cristo, y con él, el testimonio de las tres décadas de lucha de los familiares de las víctimas por encontrar la verdad. Los arreglos florales que los familiares recogieron después de un ofrecimiento colectivo y el vino consagrado fueron otros de los símbolos presentes que se hicieron presente los 30 años de lucha por la verdad.

**Al acto se trajeron a la memoria los familiares que iniciaron este proceso de denuncia y que murieron sin poder realizar ningún acto** como ese, esperando respuestas por parte de las FFMM y del Estado, y también las personas que como Eduardo Umaña han acompañado el proceso desde el inicio y que fueron asesinados por develar la impunidad.

Este viernes 6 de noviembre se conmemoran 30 años de búsqueda, de exigir verdad y justicia por los desaparecidos del Palacio de Justicia y se tiene planeado un **acto de perdón por parte del presidente Juan Manuel Santos. **Desde las 2 de la tarde una galería de la memoria y a las 4 de la tarde una nueva eucaristía en la iglesia del colegio San Bartolomé.

\
