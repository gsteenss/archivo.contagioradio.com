Title: Terna de Duque le daría el control de la Corte Constitucional al gobierno
Date: 2020-11-24 23:18
Author: AdminContagio
Category: Actualidad, Nacional
Tags: Corte Constitucional, Fernando Grillo, Iván Duque, José del Castillo, Paola Meneses, Terna
Slug: esta-es-la-terna-con-la-que-el-gobierno-duque-tendria-una-mayoria-en-la-corte-constitucional
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/11/Terna-para-la-Corte-Constitucional.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/11/Terna-para-la-Corte-Constitucional-1.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"textColor":"black","fontSize":"small"} -->

Terna para la Corte Constitucional / José del Castillo - Paola Meneses - Fernando Grillo

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Este lunes el presidente Iván Duque presentó la terna que postulará para la elección de un nuevo magistrado de la Corte Constitucional que entraría a remplazar al exmagistrado Carlos Bernal quien dejó vacante el puesto desde el pasado mes de julio tras su renuncia. Los tres opcionados son **Paola Meneses, José del Castillo y Fernando Grillo.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El Gobierno ha sido duramente criticado por la forma en la que ha llevado este proceso, ya que, por una **parte tardó más de 4 meses para integrar la terna, lo cual dejó sin uno de sus magistrados titulares a la Corte**, incumpliendo un deber constitucional pues, sin dicha terna, el Senado no puede realizar la elección; y por otra **parte eliminó el trámite para la conformación de las ternas a magistrado de la Corte Constitucional a cargo de la Presidencia** que había sido previsto en el Gobierno Santos a través del Decreto 1081 del 2015 **dejando así, al arbitrio exclusivo del Presidente la elección de los candidatos.** (Lea también: [En dos años del Gobierno Duque se ha derrumbado institucionalidad](https://archivo.contagioradio.com/en-dos-anos-del-gobierno-duque-se-ha-derrumbado-institucionalidad/))

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/IvanDuque/status/1331021443895799808","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/IvanDuque/status/1331021443895799808

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:heading {"level":3} -->

### Los ternados para magistrados de la Corte Constitucional

<!-- /wp:heading -->

<!-- wp:paragraph -->

Frente a los tres postulados también se han expresado reparos pues **varios sectores han señalado que destacan más por su cercanía con el presidente Duque que por sus méritos y experiencia en el ámbito constitucional.** Y es que, contrario a lo que afirmó el Presidente en el *tweet* en el que dio a conocer su terna, ninguno de los candidatos es un constitucionalista destacado o se ha enfocado en esta rama del derecho a lo largo de su trayectoria.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Una de las ternadas es **Paola Meneses** quien no cuenta con experiencia en derecho constitucional. Se ha desempeñado en otras áreas como abogada de la vicepresidencia jurídica de la Empresa de Telecomunicaciones de Bogotá -ETB-; como secretaria general de Federación Nacional de Departamentos -FDN- adonde llegó de la mano del exministro Amylkar Acosta quien es amigo de su padre.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

**Fue nombrada en 2018 por el propio presidente Duque** **como Superintendente de Subsidio Familiar,** puesto al que renunció después de año y medio para pasar a ser Fiscal delegada contra la criminalidad organizada, **nombrada por el Fiscal General Francisco Barbosa, quien también es conocido por su estrecha amistad con Duque.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En la lista también se encuentra el abogado **José del Castillo** quien tampoco tiene un enfoque en materia constitucional, pues se ha especializado en las ramas del derecho comercial y administrativo. **Es vicerrector de la Universidad Sergio Arboleda, institución de la que egresó Iván Duque y con la que tienen vinculo varios altos funcionarios del Gobierno** como el ministro del Deporte, Ernesto Lucena; el Alto Comisionado para la Paz, Miguel Ceballos; y también el fiscal Barbosa quien en su momento también fuera ternado para ocupar dicho cargo por el presidente Duque luego de ser funcionario de su Gobierno, ocupando la plaza de Consejero Presidencial para los Derechos Humanos.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

**José del Castillo** no ha tenido mayor figuración en el ámbito **público pero es conocido por haber firmado una [carta](https://www.facebook.com/Lahoradelaverdadcol/photos/pcb.2137284346308689/2137283619642095) pidiéndole al presidente Duque objetar la Ley Estatutaria de la JEP cuando esta pasó a sanción presidencial luego de ser aprobada en el Congreso.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Finalmente, el tercer ternado es el abogado Fernando Grillo quien a lo largo de su carrera tampoco ha tenido énfasis en asuntos constitucionales. **Fue director del Departamento Administrativo de la Función Pública -DAFP- nombrado por el expresidente Álvaro Uribe, jefe natural del Centro** Democrático y actualmente repite en el ejercicio de ese cargo, **esta vez nombrado por el presidente Iván Duque, en el año 2018.**

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/CeciliaOrozcoT/status/1331086715168182272","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/CeciliaOrozcoT/status/1331086715168182272

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

**Varios sectores han cuestionado la independencia que pueda llegar a tener cualquiera de los ternados, al asumir su cargo como magistrado(a) de la Corte Constitucional por los estrechos vínculos que tienen con el Presidente y el Partido de Gobierno,** por lo que han caído sobre ellos reproches similares a los que en su momento se dirigieron en contra del Fiscal Francisco Barbosa, de la Procuradora electa Margarita Cabello y  del Defensor del Pueblo Carlos Camargo, cuya elección fue catalogada como **la cooptación del Gobierno sobre los poderes del poder público y los entes de control.** (Le puede interesar: [Las críticas tras la elección de Margarita Cabello en Procuraduría](https://archivo.contagioradio.com/las-criticas-tras-la-eleccion-de-margarita-cabello-en-procuraduria/))

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/MarthaECamargo/status/1331191769040609284","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/MarthaECamargo/status/1331191769040609284

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:block {"ref":78955} /-->
