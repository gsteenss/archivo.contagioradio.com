Title: Otra Mirada: Ecopetrol en venta
Date: 2020-07-18 11:15
Author: PracticasCR
Category: Otra Mirada, Otra Mirada, Programas
Tags: Cenit, Venta de Ecopetrol
Slug: otra-mirada-ecopetrol-en-venta
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/07/Ecopetrol-scaled.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: @oleoductos

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Miembros de la Unión Sindical Obrera (USO) llevan más de 20 días atados al monumento de Ecopetrol en Bogotá y esto debido al decreto 811 expedido por el Gobierno que abriría la posibilidad a la venta de Ecopetrol y otros activos estratégicos del Estado como forma de enfrentar la crisis económica generada por el coronavirus. (Si desea saber más: [Trabajadores de 54 plantas de ECOPETROL protestan contra la posible venta de la empresa estatal](https://archivo.contagioradio.com/trabajadores-de-54-plantas-de-ecopetrol-protestan-contra-la-posible-venta-de-la-empresa-estatal/)) 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por esto, y ya que los sindicalistas mencionan que los colombianos no están al tanto de la situación, se abrió un debate frente a lo que estos miembros hablan, las luchas y razones por las que decidieron protestar, además de explicar qué implicaciones tiene esta venta.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El análisis se realizó desde un punto económico, social y ambiental, y para desarrollarlo estuvieron Hernando Silva integrante de la Secretaría de Derechos Humanos y Unión Sindical Obrera, Luis Álvaro Pardo economista y ex director técnico de minas del Ministerio de Minas y Energía, y Andrés Gómez ex trabajador petrolero especialista en energía geotérmica. 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Los invitados dieron a conocer cuáles son las preocupaciones actuales, qué es lo que se ha logrado con el gobierno y Ecopetrol en las reuniones que han mantenido, y desde una perspectiva económica, se analiza si realmente Ecopetrol está en una crisis y si el precio por el que se pretende vender, justifican la perdida de esta empresa.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Con este panorama presentado, concluyen que no es una acción que beneficie a los trabajadores, no es rentable para el país y no genera unas ganancias razonables para que se venda la empresa, y por sobre todo, recuerdan que esta es una lucha del pueblo colombiano y no solo de los trabajadores.

<!-- /wp:paragraph -->

<!-- wp:quote -->

> “Ecopetrol no se vende, Ecopetrol se defiende”.
>
> <cite>Hernando Silva</cite>

<!-- /wp:quote -->

<!-- wp:paragraph -->

Si desea escuchar el programa del 14 de julio: [Desplazamiento en Ituango: Excombatientes buscan un futuro en paz](https://cutt.ly/GaysVym)

<!-- /wp:paragraph -->

<!-- wp:core-embed/facebook {"url":"https://www.facebook.com/contagioradio/videos/222598968850182","type":"video","providerNameSlug":"facebook","className":""} -->

<figure class="wp-block-embed-facebook wp-block-embed is-type-video is-provider-facebook">
<div class="wp-block-embed__wrapper">

https://www.facebook.com/contagioradio/videos/222598968850182

</div>

</figure>
<!-- /wp:core-embed/facebook -->

<!-- wp:block {"ref":78955} /-->
