Title: Denuncian el asesinato de Ever Edwuardo Velásquez en Puerto Guzmán, Putumayo
Date: 2020-11-11 10:28
Author: AdminContagio
Category: Actualidad, Nacional
Tags: Putumayo
Slug: denuncian-el-asesinato-de-ever-edwuardo-velasquez-en-puerto-guzman-putumayo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/11/Violencia-en-Putumayo.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/11/Violencia-en-Putumayo.png" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

La Red de Derechos Humanos Putumayo, **denunció el asesinato de Ever Edwuardo Velásquez Cuellar, un joven integrante de la Junta de Acción Comunal del corregimiento “José María” ubicado en el municipio de Puerto Guzmán, Putumayo.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Según la denuncia tres hombres con armas de fuego de largo alcance, incursionaron en las viviendas de las familias campesinas, asesinaron al joven y abandonaron el lugar “*como si nada, por la vía principal que conduce a la cabecera municipal*”.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/Indepaz/status/1325526061006909445","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/Indepaz/status/1325526061006909445

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

Para dar cuenta del recrudecimiento de la violencia en el departamento, **la Red de Derechos Humanos Putumayo denunció también el homicidio de dos personas perpetrado el pasado 24 de octubre,** por parte de un grupo armado ilegal identificado como Comando de la Frontera, que asesinó al campesino **Jaider Chávez Erazo y la otra persona**, al parecer integrante de otro grupo armado ilegal, autodenominado Frente Carolina Ramírez.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En la denuncia, la Red también hizo referencia al reciente **homicidio del líder Nelson Ramos**, integrante de la Asociación Municipal de Trabajadores Campesinos de Piamonte, Cauca (ASIMTRACAMPIC) a quien asesinaron luego de haber reunido a la población de la vereda Yapura, del municipio de Piamonte, Cauca. (Le puede interesar: [Asesinan a Nelson Ramos integrante de ASIMTRACAMPIC en Piamonte, Cauca](https://archivo.contagioradio.com/asesina-campesino-nelson-ramos-piamonte/)).

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Adicionalmente, señaló que a la fecha existen **cinco Alertas Tempranas que reflejan la situación de riesgo de las comunidades rurales campesinas, indígenas y afrodescendientes en esa región,** en las cuales se imparten recomendaciones y orientaciones para que las autoridades adopten acciones para evitar la materialización de esos riesgos, pero que no han sido acatadas generando que la violencia, expresada en asesinatos, amenazas y desplazamientos en contra de los habitantes, se siga perpetuando. (Le puede interesar: [Acuerdo de paz tiene herramientas para frenar la violencia en Colombia:](https://archivo.contagioradio.com/onu-gobierno-acudir-a-mecanismos-del-acuerdo-para-combatir-violencia/) [ONU](https://www.un.org/es/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por lo anterior, la comunidad sigue exigiendo acciones al Gobierno y en general a la institucionalidad para adoptar políticas que permitan desescalar la violencia; y adicionalmente, pide a las entidades nacionales y a la Comunidad Internacional, realizar **Misión de Verificación Humanitaria en terreno**, que permita dar cuenta de la situación de la zona, especialmente en lo referente a la gran presencia de grupos armados ilegales ante la omisión y en algunos casos colaboración de la Fuerza Pública.

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->
