Title: ¿Trabajo Sexual o Prostitución? ¿regular o abolir?
Date: 2017-08-17 14:59
Category: Libertades Sonoras, Mujer
Slug: trabajo-sexual-o-prostitucion-regular-o-abolir
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/08/trabajo-sexual1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Getty Imágenes] 

###### [17 Ago. 2017] 

En \#LibertadesSonoras abordamos un tema que está siendo parte de la agenda pública a raíz del reciente proyecto de ley que propuso la senadora Clara Rojas, en el que se propone multar a quienes paguen por servicios sexuales. ¿A qué hacemos referencia? a la prostitución o el trabajo sexual.

Dos debates se suscitan a propósito de este tema, uno es **el abolicionista, planteado por mujeres y algunas organizaciones, que aseguran que la prostitución vulnera** y violenta los derechos de las mujeres desde distintos ámbitos y que atenta contra la dignidad humana de quien la ejerce.

Desde esa línea han promovido desde diversos escenarios, posibilidades distintas y dignas para las personas que se ven “obligadas” por su contexto socio-económico, familiar, territorial a ejercer la prostitución. Le puede interesar: [Organizaciones de Trabajadoras Sexuales rechazan proyecto de Clara Rojas](https://archivo.contagioradio.com/organizaciones-de-trabajadoras-sexuales-rechazan-proyecto-de-clara-rojas/)

De otro lado **están quienes proponen reglamentar la prostitución y proponen que la sociedad lo entienda como Trabajo Sexual**, entendido como igual a otras actividades laborales, pero que debe realizarse y ejercerse de forma voluntaria, con todos los derechos garantizados y en condiciones dignas.

Escucha en nuestro programa a Esperanza una mujer sobreviviente de la prostitución y a Fidelia Suarez, representante de Sintrasexco, primer Sindicato de Trabajadoras Sexuales hablando sobre este tema que han vivido o viven a diario. Le puede interesar: [Crónica del primer sindicato de trabajadoras sexuales en Colombia](https://archivo.contagioradio.com/cronica-del-primer-sindicato-de-trabajadoras-sexuales-en-colombia/)

<iframe id="audio_20390206" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_20390206_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

######  Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU).

 
