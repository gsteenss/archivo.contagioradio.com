Title: "Río Cauca debe ser representado por quienes lo hemos cuidado"
Date: 2019-06-20 21:18
Author: CtgAdm
Category: Ambiente, Comunidad
Tags: Empresas Públicas de Medellín, Hidroituango, Río Cauca
Slug: rio-cauca-debe-ser-representado-por-quienes-lo-hemos-cuidado
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/05/DcpT75PXcAEDk30-e1525800304474.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Foto: Twitter Claudia Ortiz] 

El Tribunal Superior de Medellín **reconoció al Río Cauca como sujeto de derechos**, ordenando su protección, conservación, mantenimiento y restauración, a cargo de Empresas Públicas de Medellín (EPM) y el Estado colombiano. Al respecto, las **comunidades aledañas insistieron que ellos, y no el Estado, el cual ha sido responsable de los daños a la cuenca, deberían ser reconocidos como los representantes legales y guardianes del río**.

Esta decisión se dio después de un suceso de emergencias ambientales que afectaran a los ecosistemas del río, así como el bien estar de las comunidades aledañas. Por tal razón, el Senador Juan Luis Castro, del Partido Liberal, y el activista Diego David Ochoa, solicitaron a través de una tutela que el río fuese reconocido como sujeto de derechos y que el tribunal ordernara al Ministerio de Ambiente, EPM y Hidroituango entregrar un reporte de las estrategias empleadas para la restauración del río.

En respuesta, el tribunal reconció que las generaciones futuras son sujetos de derechos de especial protección y por tal razón, se concedió en su favor **los ámparos de sus derechos fundamentales a la dignidad, al agua, a la seguridad alimentaria y al medio ambiente sano**. También es importante destacar que los magistrados reconocieron los derechos del río y las personas y comunidades que habitan la cuenca del Río Cauca, sus afluentes y territorios aledaños. (Le puede interesar: "[Medidas cautelares priorizan a víctimas y ambiente sobre Hidroituango](https://archivo.contagioradio.com/medidas-cautelares-priorizan-a-victimas-y-ambiente-sobre-hidroituango/)")

### **El Estado como representante legal del Río Cauca**

Sin embargo, un punto del fallo ha generado desacuerdo con las comunidades que han protegido el río hasta ahora y es la orden de los magistrados al Gobierno nacional de ejercer la representación legal de los derechos del río. Como lo señala Isabel Cristina Zuleta, vocera del Movimiento Rios Vivos - Antioquia, **esta decisión pone la protección del río en las manos de las mismas entidades que más le han hecho daño**.

"Esto hace que nosotros veamos este fallo como un riesgo y no como un logro de las luchas sociales y del avance de la jurisprudencia para defender los bienes de la naturaleza", afirmó la lideresa. Agregó que otorgarle al Estado colombiano la representación legal del río "es decirle al país que los quien asesinan tienen la potestad de cuidar a las víctimas".

En ese sentido, el Movimiento Ríos Vivos - Antioquia interpondrán un recurso, por medio del cual solicitarán que las comunidades sean reconocidos como los guardianes y los representantes legales del río.

<iframe id="audio_37377125" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_37377125_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
