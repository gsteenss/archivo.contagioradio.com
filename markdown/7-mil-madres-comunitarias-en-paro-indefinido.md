Title: Paro de madres comunitarias completa 10 días
Date: 2016-04-12 16:00
Category: Movilización, Nacional
Tags: ICBF, Madres Comunitarias, paro madres comunitarias
Slug: 7-mil-madres-comunitarias-en-paro-indefinido
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/04/Madres-Comunitarias-Villavicencio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: CUT Colombia ] 

###### [12 Abril 2016 ]

Por lo menos 7 mil madres comunitarias protagonizan plantones y manifestaciones frente a las instalaciones del Instituto Colombiano de Bienestar Familiar ICBF, en Neiva, Cali, Bogotá, Villavicencio, Bucaramanga y Cartagena, exigiendo al Gobierno que cumpla con la **contratación directa y el reconocimiento del derecho a una pensión digna**, acuerdos con los que se había comprometido.

Según denuncia Sintracihobi, las madres comunitarias se ven obligadas a entrar en paro indefinido debido a la **"improvisación y el desgreño administrativo de los programas de primera infancia"**, como las principales consecuencias de la crisis estructural que enfrenta el ICBF, y que las ha llevado a "hacer una sopa para 12 niños con solo 6 onzas de frijoles", como afirma Nelly de la Hoz, madre comunitaria de La Guajira.

De acuerdo con Olinda García, presidenta de Sintracihobi, esta "lucha es para **dignificar las condiciones de los niños atendidos por los programas de primera infancia** y por supuesto, para dignificar nuestras condiciones laborales. Porque lo que quiere Bienestar Familiar es acabar con el programa de hogares. Hogar que se cierra es hogar que no se vuelve abrir".

Sintracihobi asevera que el ICBF y sus operadores privados, han acosado y hostigado sistemáticamente a las madres comunitarias, para que deserten de los programas de primera infancia que llevan atendiendo más de 30 años, y de los que participan algunos **funcionarios "elegidos a dedo" por el Instituto**, un alto de grado de corrupción que ha obligado a las madres a solicitar trabajo directamente con los operadores.

Estas empresas privadas brindan una "miserable minuta de alimentación" para los niños, niñas y adolescentes de los programas del ICBF, que los ha llevado a **alarmantes indices de desnutrición que en muchas ocasiones han provocado muertes**, pues cómo denuncia una de las madres "cuando se advierte que hay un niño en situación de desnutrición, ICBF envía un formulario, no hace nada más para sacar el niño de la situación".

Las madres comunitarias exigen al Gobierno nacional vincularlas con contratos a término indefinido directamente con el ICBF; **pensionarlas dignamente abarcando todos los años de prestación de servicios**; revocar el 'Banco de oferentes' y permitir que las asociaciones que históricamente han venido contratando con el ICBF lo continúen haciendo; igualar la partida presupuestal para todos los programas de primera infancia; que el ICBF cubra en su totalidad los gatos de salud y pensión de las madres sustitutas y **revisar integralmente los estándares aplicados en los últimos años por el Instituto**.

De acuerdo con García, desde la semana pasada se instaló una mesa de concertación entre el ICBF y las madres comunitarias, pero no han logrado llegar acuerdos, debido a que la funcionaria encargada del **ICBF tuvo una "actitud grosera"** con ellas, y hasta la noche de este lunes la actual directora de la institución, Cristina Plazas, acudió a la mesa; sin embargo, para las madres ella "no es garante para la negociación", y por tanto **continuarán en paro hasta que no se acuerden mejoras** en las raciones para los niños y las niñas, así como en los otros elementos demandados.

En Bogotá, las madres continúan acampando frente a las instalaciones del ICBF, **pagando en las casas vecinas para que las dejen bañar**, y contando con el apoyo de algunos padres de familia que consideran legítimas sus reclamaciones.

##### [Olinda García,  Sintracihobi]<iframe src="http://co.ivoox.com/es/player_ej_11141730_2_1.html?data=kpaelpabd5Ghhpywj5WZaZS1k5yah5yncZOhhpywj5WRaZi3jpWah5yncbDgytPRw5CrpdPXhqigh6aopYyhjNXfx9jNqMbi1caYxsqPl8ri1dfOxc7Ms8PdjoqkpZKns8%2FowszW0ZC2pcXd0JCah5yncZU%3D&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe> 

##### Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)]

 
