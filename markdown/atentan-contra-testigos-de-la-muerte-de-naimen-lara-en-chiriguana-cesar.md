Title: Atentan contra testigos de la muerte de Naimen Lara, en Chiriguaná Cesar
Date: 2016-07-21 13:19
Category: DDHH, Nacional
Tags: cesar, Chiriguaná, ESMAD, Fuerza Pública, policia
Slug: atentan-contra-testigos-de-la-muerte-de-naimen-lara-en-chiriguana-cesar
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/06/Huila-ESMAD-e1465412848210.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Nick Jaussi 

###### [21 Jul 2016] 

La familia de Naimen Agustín Lara, quien habría sido asesinado por efectivos del ESMAD y de la Policía Nacional en Chiriguaná, Cesar; ahora es **objeto de amenazas y atentados, que buscarían que no declaren sobre los hechos que rodearon la muerte de Naimen**, denunció el Equipo Jurídico Pueblos.

De acuerdo con la denuncia, la primera víctima de la familia es la sobrina de Lara de 19 años de edad, de nombre Yeraldin Lara Bitta. **La joven habría sido perseguida hasta su residencia por un hombre desconocido, que la golpeó, la intentó asfixiar y la robó**. Según la declaración de la víctima esta persona le habría dicho que **no fuera a declarar por la muerte de su tío de lo contrario ella o su familia se morirían.**

La joven fue encontrada cerca al lugar donde había muerto su familiar. Allí estaba inconsciente, en ropa interior, la blusa y el pantalón rasgados violentamente. De inmediato fue traslada al hospital del municipio donde recibió atención médica, donde se confirmó que **fuertemente golpeada en la frete al parecer con un ladrillo y en el abdomen con un objeto contundente**, además se  observó fuertes señales de violencia en su cuello, y  se dice que posiblemente  fue víctima de violencia sexual.

Ante la situación, la familia y la comunidad **señalan la responsabilidad del  Estado colombiano por este grave hecho y por cualquier atentado la familia de Lara **o testigo del accionar del Esmad y de la Policía de Chiriguaná.  Así mismo, exigen medidas preventivas y de protección, y las investigaciones pertinentes sobre lo ocurrido con Naimen y con su sobrina Yeraldin.

Cabe recordar, que Naimen murió en medio de las protestas que adelantaban los pobladores exigiendo que no se cerrara el segundo nivel del Hospital del municipio. **Allí agentes de la fuerza pública arremetieron contra los pobladores, dispararon contra tres de ellos, hirieron con cuchillos a otros tantos** y sobre una persona pasaron una de las motos en las que se movilizaban.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en [Otra Mirada](http://bit.ly/1ICYhVU) por Contagio Radio 
