Title: Llega la Cumbre Nacional de Paz
Date: 2016-12-05 17:46
Category: Otra Mirada, Paz
Tags: campesinos, Cumbre Agraria, paz
Slug: llega-la-cumbre-nacional-de-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/02/cumbres_paz_medellin_contagioradio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Alianza de Medios Alternativos] 

###### [5 de Dic. 2016] 

**Entre el 7 y el 10 de Diciembre en la ciudad de Bogotá se llevará a cabo la Cumbre Nacional de Paz, evento en el que se pretende recoger y empezar a ejecutar  diversas propuestas** que han venido siendo recogidas en diversas comunidades de todo el país, que van encaminadas a continuar la construcción de la paz en los territorios.

Cabe recordar que durante el último año la Cumbre Agraria, Étnica y Popular, ha realizado diversos encuentros en todo el territorio nacional a través de diversas cumbres de paz, en las que participaron cerca de 4000 líderes y lideresas de todos los departamentos del país. Le puede interesar: [Cumbre Agraria solicitó medidas cautelares de la CIDH](https://archivo.contagioradio.com/cumbre-agraria-solicito-medidas-cautelares-de-la-cidh/)

Jimmy Moreno, integrante de la Cumbre Agraria, Étnica y Popular aseguró que **“en esta ocasión serán cerca de 1000 personas las que participarán entre indígenas, afros, mestizos y campesinos de todo el país”.**

En dicha cumbre trabajarán **temas como la unidad del movimiento social, temas de la agenda política como la paz y adicionalmente proyectarán las actividades futuras.** Le puede interesar: [Cumbre Agraria suspende negociaciones con Gobierno](https://archivo.contagioradio.com/cumbre-agraria-suspende-negociaciones-con-gobierno/)

Así mismo en este espacio, reforzarán las propuestas que han sido consolidas a través del tiempo y en las cuales han plasmado la visión de paz desde lo territorial y su visión como comunidades a proósito de este tema.

**“Esta cumbre entonces es para complementar ese pliego y para colocarnos en vista de lo que viene en la agenda política de país y mirar cómo con otros sectores podemos construir una confluencia de paz. La idea es trabajar fuertemente para apoyar el acuerdo de paz” agregó Jimmy.**

<iframe id="audio_14639647" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_14639647_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u)  o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](https://archivo.contagioradio.com/).

<div class="ssba ssba-wrap">

</div>

 
