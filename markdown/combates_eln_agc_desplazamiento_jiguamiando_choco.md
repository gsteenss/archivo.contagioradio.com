Title: Combates entre ELN y AGC provocan desplazamiento de 12 familias en Jiguamiandó, Chocó
Date: 2018-01-03 23:23
Category: DDHH, Nacional
Tags: Autodefensas Gaitanistas de Colombia, Chocó, ELN
Slug: combates_eln_agc_desplazamiento_jiguamiando_choco
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/01/desplazamiento.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Comisión de Justicia y Paz] 

###### [3 Ene 2018] 

De acuerdo con la denuncia de la Comisión de Justicia y Paz, debido a combates entre las Autodefensas Gaitanistas de Colombia y la guerrilla del ELN **más de 12 familias de territorios colectivos de Curvaradó y Jiguamiandó** debieron desplazarse hacia la Zona Humanitaria de Nueva Esperanza.

Los combates se desarrollaron en el caserío Las Menas y empezaron desde las 8.00 de la mañana de este miércoles. En la tarde los pobladores decidieron regresar al caserío para evitar la pérdida de sus siembras, pero sobre las 3:30 de la tarde continuó la movilidad de combatientes de ambos bandos entre los territorios colectivos. Además, según algunos habitantes de esa zona, **en el marco de dichos enfrentamientos murieron combatientes.**

Se trata de una situación de la cual venían advirtiendo desde hace meses las comunidades de Jiguamiandó. A inicios del mes de noviembre, los pobladores alertaron que debido a posibles enfrentamientos entre AGC y ELN, la población se encontraba en riesgo por la instalación de** minas antipersona y amenazas a líderes y lideresas comunitarios dela región. **(Le puede interesar: [Alertan sobre presencia de AGC y ELN en Jiguamiandó)](https://archivo.contagioradio.com/eln-y-neoparamilitares_jiguamiando/)

Ante la nueva situación que viven las comunidades, los integrantes de los Consejos Comunitarios llaman al gobierno de **Juan Manuel Santos y al ELN** **a prolongar y profundizar el cese unilateral de fuegos,** y exigen que se aplique una política coherente y eficaz frente a los grupos herederos de paramilitarismo, como las denominadas Autodefensas Gaitanistas de Colombia, **"que operan por ineficacia o complicidad militar y policial en la región"**, como aseguran lo habitantes de la zona.

La organización defensora de derechos humanos, señala también que AGC sirven a empresarios beneficiarios del paramilitarismo de las AUC asegurando agronegocios de banano, palma, coca, ganadería extensiva.  (Le puede interesar: [Los empresarios estarían detrás del asesinato de líderes sociales del Bajo Atrato y Urabá)](https://archivo.contagioradio.com/hasta-cuando-y-cuantos-mas-continuan-la-violencia-contra-lideres-sociales-del-bajo-atrato-y-uraba/)

###### Reciba toda la información de Contagio Radio en [[su correo]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio]{.s1}](http://bit.ly/1ICYhVU)[.]{.s1}
