Title: Lo que está en juego para Latinoamérica con las elecciones del Congreso de EE.UU.
Date: 2018-11-06 18:22
Author: AdminContagio
Category: El mundo, Entrevistas
Tags: elecciones, Estados Unidos, Odio, Trump, xenofobia
Slug: elecciones-congreso-ee-uu
Status: published

###### [Foto: @VonKoutli] 

###### [6 Nov 2018] 

Este miércoles se sabrá como quedará repartido el Congreso de los Estados Unidos para los próximos dos años, y se definirán los Gobernantes de 36 de los 50 estados de la nación norteamericana. Los comicios son considerados como un plebiscito por parte de analistas, puesto que **está en juego el futuro del país por los próximos 10 años**, del Gobierno de Trump y su relación con Latinoamérica.

Según **Gimena Sánchez, integrante de la Oficina en Washington para Asuntos Latinoamericanos (WOLA)**, estas elecciones son vitales para el futuro de esa nación en los próximos 10 años. Dicha importancia, parte del contexto actual que vive el país, puesto que se podrían asumir como un respaldo o rechazo, de la función ejercida hasta el momento por el presidente Donald Trump.

En esta ocasión, **Trump llega a las elecciones con una mayoría en el congreso (55% cámara de representantes y 51% del Senado), y 33 gobernadores que pertenecen a su partido (Republicano)**; por lo tanto, hasta el momento ha tenido un gran apoyo tanto en lo local como en el legislativo para el desarrollo de su mandato. Sin embargo, la correlación de fuerzas podría cambiar en la Cámara de Representantes, en la que según explica Sánchez, el partido Demócrata, tendría 221 de los 218 escaños necesarios para ser mayoría.

**En caso de que el partido de oposición a Trump obtuviese esa mayoría en la Cámara baja, enfrentar al presidente norteamericano sería más sencillo**, puesto que las fuerzas en el Senado están más disputadas; esto, en razón de que 1/3 del Senado se renueva cada 2 años, y en la actualidad Demócratas tienen solo 23 curules aseguradas, mientras republicanos tienen 42, dejando en disputa únicamente 35.

### **Migrantes: otra vez serán objeto de campaña** 

Sánchez aseguró que la retórica de esta campaña ha sido "escalofriante", y **nuevamente se está usando el tema de la migración como objeto de campaña**. La integrante de WOLA sostuvo que Trump ha estado recientemente en varios escenarios expresando una visión según la cual, no votar por el partido Republicano sería apoyar la 'invasión' de migrantes latinoamericanos que afectarían gravemente a Estados Unidos.

Para la agente de WOLA, **este discurso de odio está dando resultado,** y los medios nuevamente están compartiendo este tipo de mensajes que incitan a la xenofobia mediante información falsa. (Le puede interesar: ["Discurso de Donald Trump es una amenaza para toda la humanidad"](https://archivo.contagioradio.com/discurso-de-donald-trump-es-una-amenaza-para-toda-la-humanidad/))

### **La retórica sobre Latinoamérica** 

Sánchez señaló que la retórica que está impulsando Trump invita a pensar que cualquier pronunciamiento del partido Demócrata en términos de derechos humanos a favor de los migrantes es negativo; de tal forma que una victoria en el Legislativo, apoyaría la visión que tienen hombres al interior de la Casa Blanca, como el **asesor de seguridad nacional, John Bolton. **(Le puede interesar: ["Fuertes protestas en Palestina contra decisión de Trump en Jerusalén"](https://archivo.contagioradio.com/palestina-trump-jerusalen/))

Esta visión afectaría la relación de Estados Unidos con países que Bolton considera de 'Troika', es decir, **una amenaza comunista en la región, como Venezuela, Cuba o Nicaragua.** Lo que se traduciría en una retórica más fuerte, como ya se ha visto recientemente, en la que se busca el cambio de régimen en Venezuela a cualquier costo; o eliminar las ayudas económicas a Centroamérica en represalia por permitir el paso de la caravana migrante.

Por último, Sánchez dijo que el debate poselecciones girará en torno al presupuesto de la nación norteamericana para el próximo año, de forma tal que **podría verse una lucha por los recursos para la construcción del muro en la frontera sur de los Estado Unidos (que fue una de las promesas de campaña de Trump),** de tal forma que un congreso más demócrata le haría más difícil conseguir el dinero para construir dicha barrera, mientras uno más republicano le haría las cosas más fáciles, "además hemos visto militares llenando a la frontera, así como barreras puestas de forma artesanal, esto genera odio y miedo total" indicó.

<iframe id="audio_29877359" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_29877359_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
