Title: Distrito puede salvar el humedal La Conejera
Date: 2015-01-29 18:04
Author: CtgAdm
Category: Ambiente, Movilización
Tags: Gustavo Petro, Humedal La Conejera, Secretaría de Ambiente
Slug: distrito-puede-salvar-el-humedal-la-conejera
Status: published

##### Foto: [diarioadn.co]

##### **[Roberto Sáenz]**: <iframe src="http://www.ivoox.com/player_ek_4015280_2_1.html?data=lZWel5ecdI6ZmKiakpyJd6KnkpKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRltDWxtfh0ZC3aaSnhqaex9PecYarpJKw0dPYpcjd0JC%2Fw8nNs4yhhpywj5k%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe> 

La Secretaría de Ambiente del distrito, propuso comprar predios cercanos al Humedal La Conejera, en Suba, para "convertirlos en espacio público y evitar riesgos en la reserva”. Sin embargo, varios concejales rechazaron la propuesta, debido a que creen innecesario privatizar un espacio que por su gran importancia ecológica, ya debe ser protegido como reserva natural.

Desde el año pasado, el distrito otorgó la licencia para que se lleve a cabo la construcción del proyecto Urbanización Fontanar del Río Etapa VIII, que adelanta la firma Praga Servicios Inmobiliarios S. A. (en la que participan familiares políticos del alcalde Gustavo Petro).

El proyecto está ubicado a menos de un metro del humedal y consta de **5 torres con 94 apartamentos.**

La comunidad y los concejales han insistido en que **es una construcción contraria a los intereses ambientales de la ciudad,** y que además tiene vicios de forma en todos los procedimientos que se llevaron a cabo para otorgar la licencia de construcción.

El concejal de Bogotá, Roberto Sáenz, señaló, “he estado muy desconcertado y ahora indignado con esto, hemos apoyado el proyecto de Bogotá Humana, pero en este caso se ha convertido en una discusión que va mas allá del simple debate formal, allí se podría actuar rápido y sin inconvenientes (…) pero no se tomo ninguna medida con respecto a lo que debía proponerse para evitar la construcción, **todo relacionado a que están involucrados familiares políticos del alcalde Petro**”.

Así mismo, el concejal denunció que la administración distrital tomó la decisión de justificar de cualquier modo la realización de la obra, cuando, según Sáenz, evidentemente **hay una violación de varias de las normas básicas del urbanismo y muchas del ámbito ambiental.**

La construcción está detenida por la acción de la comunidad, sin embargo, “hoy en día nadie toma una decisión de modificar la planeación de la urbanización (…) y la Secretaria de Ambiente se limitó a justificar que allí se podía construir y que no se podía mover nada”.

Frente a esos señalamientos de la secretaría, Sáenz, afirma que “**la administración sabe que puede cerrar la obra si algo no está en regla”.**
