Title: Agresiones de la fuerza pública contra la protesta social han sido sistemáticas: Corte Suprema
Date: 2020-09-22 19:55
Author: AdminContagio
Category: Actualidad, Movilización
Tags: Corte Suprema de Justicia, ESMAD, Min. Defensa, Paro Nacional
Slug: corte-suprema-ordena-a-min-defensa-ofrecer-disculpas-por-exceso-en-el-paro-nacional
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/09/esmad-.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

Este 22 de septiembre la **Corte Suprema de Justicia falló a favor de una tutela que indicaba que las agresiones ejercidas por parte de la fuerza pública han sido sistemáticas**, y que además ponen en riesgo el derecho a la protesta; al tiempo que cuestionan el[papel del Gobierno](https://archivo.contagioradio.com/duque-expreso-el-desprecio-que-siente-por-la-ciudadania-angela-robledo/)en su accionar y ordenan una disculpa pública por parte de Min Defensa.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/CorteSupremaJ/status/1308517707483054082","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/CorteSupremaJ/status/1308517707483054082

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

La decisión **fue tomada por la Sala de Casación Civil de la Corte Suprema de Justicia, luego de que cerca de 48 cuidados presentarán una[tutela](https://www.justiciaypazcolombia.com/corte-suprema-de-justicia-esmad-debe-suspender-temporalmente-el-uso-de-escopetas-calibre-12/)en la que por medio de pruebas** evidencian que desde el año 2005 en el marco de movilizaciones pacíficas, el Estado ha desplegado conductas constantes, reiterativas y persistentes para *"socavar, desestimular y debilitar su derecho a expresarse sin temor".*

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/sandraborda/status/1308532068197040128","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/sandraborda/status/1308532068197040128

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

La decisión de la Corte Suprema ordena inicialmente al Gobierno de Iván Duque desplegar varias medidas restructurativas del uso de la f[uerza pública](https://archivo.contagioradio.com/piden-a-corte-constitucional-garantizar-el-derecho-a-preguntar-quien-dio-la-orden/), adoptando acciones que garanticen el ejercicio del derecho a la protesta pacífica y no destructiva.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

A esto se suma la creación conjunta de un protocolo en donde debe participar el Ministerio de Defensa y la Policía, en donde se garantice el forjamiento del ***"estatuto de reacción uso y verificación de la fuerza legítima del estado y protección del derecho a la protesta pacífica ciudadana".***

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Al mismo tiempo la **Corte indicó que el Gobierno debe actuar con total neutralidad ante las acciones que se registren en las movilizaciones**, y no realizar pronunciamientos que estigmaticen a los manifestantes. ([Anuncian demanda contra Iván Duque y Carlos Holmes por crímenes de lesa humanidad](https://archivo.contagioradio.com/anuncian-demanda-a-ivan-duque-y-carlos-holmes-por-crimenes-de-lesa-humanidad/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Finalmente el **alto tribunal dio 48 horas al Ministerio de Defensa para ofrecer disculpas**, las cuales deben ser difundidas por radio, televisión y redes sociales, luego de los excesos de la fuerza pública cometidos durante las protestas desarrolladas el [21 de noviembre del 2019.](https://archivo.contagioradio.com/que-sigue-soluciones-al-21n/)

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Y pese a que la Corte Suprema no pudo tener en cuenta los hechos registrados el [9 y 10 de septiembre](https://archivo.contagioradio.com/otra-mirada-donde-queda-la-responsabilidad-policial/) en el que resultaron heridas más de 200 personas y se registraron 13 muertes, **se ordenó la suspensión inmediata del uso de la escopeta calibre 12** usada por los integrantes del ESMAD.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Misma arma con la que en el marco del Paro Nacional del 21 de noviembre, fue asesinado **Dylan Cruz luego de recibir en su cabeza un proyectil disparado** por un agente de esta fuerza pública. ([En veinte años el ESMAD ha asesinado a 34 personas](https://archivo.contagioradio.com/en-veinte-anos-el-esmad-ha-asesinado-a-34-personas/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Finalmente la presidencia, el Ministerio Defensa, el Ministerio del Interior y las instituciones de orden público según el alto tribunal **tendrán 48 horas para publicar el fallo entregado a través de sus páginas web y redes sociales.**

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
