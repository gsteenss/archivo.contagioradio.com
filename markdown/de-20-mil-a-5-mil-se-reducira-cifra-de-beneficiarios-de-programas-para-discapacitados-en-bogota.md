Title: De 20 mil a 5 mil se reducen los programas para atención de personas con necesidades especiales
Date: 2016-07-22 15:02
Category: DDHH, Entrevistas
Tags: administración Bogotá Humana, administración Enrique Peñalosa, población discapacitada Bogotá, proyecto 721
Slug: de-20-mil-a-5-mil-se-reducira-cifra-de-beneficiarios-de-programas-para-discapacitados-en-bogota
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/07/Proyecto-721.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Directo Bogotá ] 

###### [22 Julio 2016] 

Según denuncia el Consejero de discapacidad física Miguel Ángel González, el proyecto 721 que durante los últimos cuatro años brindó mejor calidad de vida a través de la generación de empleo y procesos de inclusión para 20 mil discapacitados, se transformará en el proyecto 1113 de la Secretaria de Integración Social que **beneficiaría solamente a 5 mil personas de las 250 mil discapacitadas que viven en Bogotá. **

De acuerdo con el Consejero el nuevo proyecto entraría en vigencia el próximo 1° de agosto con unas **metas y un modelo de atención muy distinto al que se venía trabajando con la población discapacitada**, sin especificar las condiciones de los beneficiarios y de los programas, y sin plantear avances con relación con los planes ejecutados en años anteriores.

El proyecto 721 se implementó con 8 metas específicas, la primera de ellas contemplaba la atención psicosocial a cuidadores y cuidadoras de personas en condición de discapacidad y venía acompañada de la **entrega de bonos alimentarios de \$125 mil y \$250 mil**, hoy las metas del proyecto 1113 plantean que se va a hacer un seguimiento a quienes se benefician del bono sin clarificar en qué consiste este seguimiento, asegura González.

"Todo el proceso de liderazgo logrado a través del 721, hoy sentimos que se está perdiendo, el proyecto 1113 no cumple con las expectativas que podría tener la población con discapacidad en estos momentos en la ciudad de Bogotá (...) **esperamos que Integración Social se pronuncie y presente claramente el proyecto**, las metas y lo que se pretende, porque no están claras ni las rutas ni las estrategias", concluye el Consejero.

<iframe src="http://co.ivoox.com/es/player_ej_12308881_2_1.html?data=kpegkp2cfJKhhpywj5WcaZS1lJeah5yncZOhhpywj5WRaZi3jpWah5ynca69yNrSzpCJh5SZmZbbycrQb6jjz9-SpZiJhZLgxt-SlKiPh9Di1MrXx9fTb8XZjMnW1cjFtMLXysnOxpDKaaSnhqax1ZKJe6ShpNTb1sbLrdCfs8bRy9SPcYarpJKh&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)]] 
