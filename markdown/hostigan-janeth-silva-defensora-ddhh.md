Title: Hostigan a Janeth Silva, defensora de DDHH en Putumayo
Date: 2018-08-13 09:21
Category: DDHH, Nacional
Tags: Amenazas a líder, Comisión de Justicia y Paz, Derechos Humanos, Hostigamientos
Slug: hostigan-janeth-silva-defensora-ddhh
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/08/Jany-SIlva.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo Contagio Radio] 

###### [10 Ago 2018] 

La lideresa social y ambiental Janeth Silva, del municipio de Puerto Asís, Putumayo, denunció que viene siendo víctima de hostigamientos por parte de sujetos, que han estado cerca de su casa desde hace dos semanas. **A pesar de las denuncias, no se ha avanzado en la investigación de las amenazas en su contra.**

Desde hace dos semanas Silva viene advirtiendo que sujetos extraños hacen observaciones a su vivienda. Incluso, según ella, una vecina le informo de un hombre con una carreta de perfumes sospechoso que estaba cerca a su casa, con una actitud y productos  que no correspondían con los de un vendedor. (Le puede interesar: ["Continúa la crisis humanitaria en la Zona de Reserva Campesina en Puerto Asís, Putumayo"](https://archivo.contagioradio.com/continua-la-crisis-humanitaria-en-la-zona-de-reserva-campesina-en-puerto-asis-putumayo/))

El más reciente hecho de hostigamiento tuvo lugar el miércoles 8 de agosto, cuando ot**ro hombre que se desplazaba en una moto de alto cilindraje se detuvo repentinamente cerca a su hija**, quien se encontraba en el patio de la vivienda, puso sus manos en el canguro que llevaba en la cintura de forma amenazante y se retiró.

Sin embargo, a pesar de la denuncia de esta serie de amenazas y hostigamientos que ha vivido, **las investigaciones en su caso no avanzan.** Aunque la líder tiene un esquema de seguridad, la Comisión denuncia que **tampoco se han tomado medidas concretas que permitan la libre movilidad de la líder por el territorio y aseguren su vida e integridad.** (Le puede interesar:["Comunidades denuncian judicialización injusta contra lideresa Jani Silva"](https://archivo.contagioradio.com/comunidades-denuncian-judicializacion-injusta-contra-lideresa-jani-silva/))

Janeth Silva es representante legal desde hace 8 meses de ADISPA, organización que representa la **Zona de Reserva Campesina Perla Amazónica**, y **ha impulsado el proceso de implementación del Acuerdo de Paz** en el territorio promoviendo iniciativas de reforestación y actuando contra las afecciones socioambientales de las operaciones petroleras, entre ellas de la empresa **Amerisur Exploración Colombia LTDA.**

<div>

<iframe id="audio_27777596" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_27777596_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen">
</iframe>
</p>
###### [Reciba toda la información de Contagio Radio en] [su correo](http://bit.ly/1nvAO4u) [o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por] [Contagio Radio](http://bit.ly/1ICYhVU) 

</div>
