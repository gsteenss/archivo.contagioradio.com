Title: Dos caras de la misma moneda
Date: 2018-08-21 18:08
Category: Camilo, Opinion
Tags: Iván Duque, Macías
Slug: dos-caras-de-la-misma-moneda
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/08/180807_06_PosesionPresidenteDuque.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Foto: Nelson Cárdenas - Presidencia 

#### Por **[Camilo De Las Casas](https://archivo.contagioradio.com/camilo-de-las-casas/)** 

###### 21 Ago  2018 

No es igual que en el 2002. La táctica es diferente, los propósitos estratégicos impunidad para él y su círculo familiar,  encubrimiento de las verdades de responsabilidad estatal, desquite con el proceso de paz de Santos, (pax neoliberal en la que están de acuerdo), pero sin concesión política son algunas de sus especificidades. Pero también es realidad que el país no es el mismo, qué hay un poder conciente que está creciendo y qué requiere ser protegido ante la cruzada de dos fases.

Existe en evidencia la combinación de unas nuevas formas de Uribe que se expresan por sus dos elegidos, el prohombre es conciente de los riesgos de proyectar solo una imagen autoritaria. Ambos Macías y Duque son fichas de la misma estrategia, muy aceitada en los asesores publicitarios y los más íntimos de Uribe. Macías y Duque, las dos avanzadas del “presidente eterno”, como lo dicen sus aduladores (y las iglesias callan). El presidente del Senado, también designado por Uribe, es el de los sin modales, el de las venganzas, el macho, el negacionista, el de sentimientos de fe.  Este es el de la alta intensidad. Es el del lenguaje privado y público de Uribe. El de “le pego en la cara marica” o de “aprueben la ley antes de que los metan a la cárcel”.  Este el que llega a mucho del pueblo acrítico que ve reflejada en la fuerza su vitalidad. Ingenuos, inocentes, manipulados

Y, ¿qué es lo nuevo? El elegido por Uribe al solio de Bolívar. Él es el de un discurso espumoso un discurso conciliador que encubre sus pretensiones de fondo. Es el discursivo que llega a los sectores medios con lugares comunes. Es el del populismo punitivo que entrecruza los sentimientos venganza con el arribismo de los sectores medios que aman ciegamente la idea del progreso excluyente de los de arriba. Slogan sin fuerza pero seductor de los que huyen de lo polémico y de la discusión sobre las contradicciones. Es el mensaje del Pacto Nacional y de la Unidad. Es evidente que cooptan hoy sectores que parecen independientemente técnicos, que están lejos de formas criminales y creen en un modelo de desarrollo y un negacionismo de las causas de la conflictividad ambiental, social, política, económica, cultural. Este es el de la guerra de baja intensidad.

Ambas caras de la misma moneda para asegurar la impunidad jurídica, política y social de Uribe, y su círculo más cercano en medio de la consolidación de la Pax Neoliberal y del silenciamiento de las verdades de un poder fáctico, real, que ha usado de las formas democráticas para reprimir, violentar y dominar, bajo el pretexto de la persecución a las guerrillas, al comunismo, y que hoy pretende imposibilitar la rendición de sus cuentas y de sus responsabilidades en la barbarie que hemos padecido en los últimos 70 años.

Ahí en los planteamiento de Uribe contra la consulta anticorrupción, en su distanciamiento de la decisión de la Corte Constitucional que valida la ley estatutaria de la Jurisdicción Especial de Paz, y la Comisión de Esclarecimiento de la Verdad, y en el enfrentamiento como una cruzada ante la única investigación que ha avanzado contra él en la Corte Suprema de Justicia, se revelan los propósitos inconfesables del odio, la venganza, la animadversión, y la enfermedad pasional de éste país. Ahí está él, Uribe, encarnando la desesperación y al mismo tiempo, sin que sea su propósito el renacer del nuevo proyecto de país.

#### **[Leer más columnas Camilo De Las Casas](https://archivo.contagioradio.com/camilo-de-las-casas/)**

------------------------------------------------------------------------

###### Las opiniones expresadas en este artículo son de exclusiva responsabilidad del autor y no necesariamente representan la opinión de Contagio Radio.
