Title: Amenazan de muerte a líder social de CONPAZ en Buenaventura
Date: 2017-03-27 14:01
Category: DDHH, Nacional
Tags: buenaventura, Conpaz, paz
Slug: amenazan-de-muerte-a-lider-social-de-conpaz-en-buenaventura
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/06/CONPAZ.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [27 Mar. 2017]

A través de un comunicado, las Comunidades Construyendo Paz en los Territorios – CONPAZ, alertaron de la **amenaza de muerte recibida por el líder Yasmani Grueso Sinisterra por “la estructura de tipo paramilitar** llamados "Los Urabeños". Yasmani además es uno de los gestores del Espacio Humanitario Puente Nayero en Buenaventura e integrante de CONPAZ.

La amenaza sucedió este sábado 25 de marzo, cuando **Yasmani fue abordado en su casa por dos hombres vestidos de civil y con armas cortas** y le expresaron que “la orden es que en este sector de Buenaventura no se iba a permitir otro Espacio Humanitario” dice la comunicación. Le puede interesr: [Asesinan a lideresa de CONPAZ y a su compañero en Buenaventura](https://archivo.contagioradio.com/asesinan-a-lideresa-de-conpaz-y-a-su-companero-en-buenaventura/)

Dice la misiva que los hombres armados **le manifestaron a Yasmani que “sabían de sus movimientos de trabajo”** haciendo referencia a su labor como defensor de derechos humanos, y “que para eso sabían qué hacer con la familia”.

El hecho sucedió 10 minutos después de que el escolta, que hace parte de la protección asignada por la Unidad Nacional de Protección – UNP – lo acompañara hasta su lugar de habitación. Le puede interesr: [CONPAZ exige voluntad política del gobierno para implementación de Acuerdos](https://archivo.contagioradio.com/conpaz-exige-voluntad-politica-para-implementacion-de-acuerdos/)

En el comunicado resaltan que **“la Comuna 3 de Buenaventura es controlada por los Urabeños** y en el Barrio Lleras al mando del llamado ‘Cheo’. Le puede interesar: [Líderes de CONPAZ son víctimas de amenazas y seguimientos](https://archivo.contagioradio.com/lideres-de-conpaz-son-victimas-de-amenazas-y-seguimientos/)

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)
