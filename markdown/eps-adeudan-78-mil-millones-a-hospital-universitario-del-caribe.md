Title: EPS adeudan $78 mil millones a Hospital Universitario del Caribe
Date: 2016-03-03 13:57
Category: Movilización, Nacional
Slug: eps-adeudan-78-mil-millones-a-hospital-universitario-del-caribe
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/03/Hospital-Universitario-del-Caribe.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: S.O.S.H.U.C] 

<iframe src="http://co.ivoox.com/es/player_ek_10661781_2_1.html?data=kpWjmJabfJKhhpywj5WbaZS1kZuah5yncZOhhpywj5WRaZi3jpWah5yncabEtJDOxsrZqMLijIqflpycb87dzZDay9HQs8%2FZ1JDOja3Tt9Hd1cbZjbrSrdfZ09jW1sbWrdCfxZKSmaiRh9Di1cbUy9SPlsLYytSYj4qbh46o&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Natalia Socarras] 

###### [3 Mar 2016] 

Dos semanas en cese de actividades completan los estudiantes de la facultad de Medicina de la Universidad de Cartagena por cuenta de la **crisis estructural que enfrenta el Hospital Universitario del Caribe, con cerca de \$78 mil millones que las EPS le adeudan** y que pone en riesgo las condiciones laborales de los trabajadores y las de atención médica de los pacientes que a diario recibe la institución.

De acuerdo con Natalia Socarras, estudiante de Medicina, **en el Hospital Universitario los estudiantes encuentran “la base del conocimiento clínico”**, teniendo en cuenta que desde cuarto semestre acuden allí para sus prácticas y de no poder hacerlo, se vería **en alto riesgo su formación como médicos para la adecuada atención de pacientes**.

Socarras insiste en que preocupa que las **autoridades nacionales y departamentales no han trazado la ruta para la solución de la crisis**, el Hospital sigue funcionando “a medias” y con exiguos recursos. Como movimiento estudiantil han promovido marchas de antorchas y plantones en distintos puntos de la ciudad. Para este jueves tienen programada una brigada de salud y para el viernes una **marcha zombie, en honor a quienes han muerto esperando atención médica oportuna**.

La estudiante concluye extendiendo la invitación a los cartageneros para que "se unan a las movilizaciones y demuestren que **con el cierre del Hospital nos afectamos todos, no sólo los estudiantes, sino toda la comunidad del Caribe**", por lo que se propone que sea revisada la gestión de la administración actual para que los organismos de control determinen medidas acertadas que permitan solucionar la crisis.

##### Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](https://archivo.contagioradio.com/)].
