Title: ESMAD amenaza huelga de hambre de trabajadores de CocaCola
Date: 2016-11-23 15:00
Category: Movilización, Nacional
Tags: Coca Cola, Huelga de hambre, Sinaltrainal
Slug: esmad-amenaza-huelga-hambre-trabajadores-cocacola
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/huelga-de-hambre.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo] 

###### [23 Nov 2016] 

Debido a los incumplimientos por parte de ejecutivos de la multinacional Coca Cola y el ministerio de trabajo, integrantes de Sinaltrainal decidieron **retomar la huelga de hambre que habían culminado el pasado 4 de Noviembre** y que se extendió por 5 días.

En horas de la mañana del **22 de Noviembre** agentes del ESMAD irrumpieron en las carpas donde se encuentran en huelga de hambre los trabajadores de la embotelladora en Bucaramanga. Después de realizar varias preguntas a los manifestantes, el **teniente** **Harry Serrano**, advirtió que ordenaría **incautar las carpas, el sonido y todos los implementos que estuviesen en el sitio de la huelga. **

### **¿Cuáles son las denuncias?**

Los manifestantes denuncian que se ha visto **violentado su derecho a la libertad de asociación sindical, su derecho a la vida pues** **han recibido amenazas de muerte, no cuentan con baterías sanitarias en sus lugares de trabajo, deben soportar olores fétidos** de las tuberías de aguas residuales que se encuentran en la zona de restaurante y l**a salud de varios trabajadores que sufren de diabetes se ha empeorado** pues no cuentan con la posibilidad de una alimentación acorde a sus necesidades.

Javier Correa uno de los integrantes de Sinaltrainal señaló que a pesar de las reuniones y los derechos de petición interpuestos por el sindicato, la empresa “continúa con la retórica pero en lo concreto no nos da soluciones (…) **los problemas cada vez son mas graves, a algunos trabajadores les están reteniendo el salario, hay acoso y represión”.**

Resaltan que en la embotelladora de Barranquilla Coca Cola **está usando Azúcar importada de Ecuador** y producida por la Compañía Azucarera Valdés S.A, **desconociendo el trabajo y producción nacional.**

Por otra parte el pasado 22 de Noviembre ocurrió otro accidente debido a las pesimas condiciones laborales, por una **fuga de amoniaco resultó afectado el trabajador afiliado a Sinaltrainal Florentino Osorio**, quien fue sacado en ambulancia y se encuentra internado  en un centro hospitalario.

Aseguró que esperan lograr con la nueva huelga de hambre indefinida detener estas “actuaciones en contra de la dignidad de los trabajadores, denunciar los saltos al debido proceso que tienen derecho todos los ciudadanos y en general **poner fin a los abusos sistemáticos de esta empresa”.**

### **¿Qué responde el Gobierno y la empresa?**

En una reunión con la Viceministra de trabajo y empresarios de Coca Cola, los trabajadores habían pactado la culminación de la huelga y una reunión para el 11 de Noviembre en la que expondrían ante estos entes, **la agenda de exigencias y acuerdos, con 60 puntos que evidencian las graves condiciones en las que deben laborar.**

En lugar de atender al llamado de la reunión, ese mismo día las directivas de la empresa procedieron con el llamado a descargos de 4 trabajadores **Alexander Cely, Juan Manuel Concha, Álvaro Navarro y Javier Correa, e iniciaron procesos disciplinarios contra estos empleados** quienes habían participado en los mítines y en la huelga de hambre.

###  

###### Reciba toda la información de Contagio Radio en [[su correo]
