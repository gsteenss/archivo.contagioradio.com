Title: Niños y niñas de las FARC comenzarán su proceso de salida el 10 de Septiembre
Date: 2016-09-02 18:23
Category: Nacional, Paz
Tags: FARC, Gobierno, proceso de paz, Unicef
Slug: ninos-y-ninas-de-las-farc-comenzaran-su-proceso-de-salida-el-10-de-septiembre
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/09/292746_colombia-e1472858527827.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Diario Extra 

###### [2 Sep 2016] 

En el comunicado conjunto 96, el **gobierno de Colombia y las FARC afirmaron que están listos los mecanismos para la salida de menores en las filas de esa guerrill**a y que dicho proceso comenzará el próximo 10 de Septiembre. Recientemente las FARC afirmaron en otro comunicado que esa salida no se había iniciado porque correspondía a un acuerdo de las dos partes, esto ante las críticas de que supuestamente esa guerrilla estaba impidiendo la salida de menores de sus filas.

En el comunicado firmado este 2 de Septiembre se recuerda que el mecanismo de salida de menores de las FARC hace parte del acuerdo firmado el pasado mes de Mayo, en el que se especificaba que debía darse un proceso de acompañamiento con un mecanismo que permitiera que los menores fueran victimizados o que la atención que se diera no fuera en las condiciones necesarias para una integración eficaz.

Según el comunicado la recepción de los y las menores la realizará el Fondo de las Naciones Unidas para la Infancia UNIFEC, que trasladará a **“centros de acogida temporal”,** en dichos espacios habitarán hasta que las instituciones competentes decidan los lugares en los que se realizará el proceso de reincorporación.

Además afirman que todas las fases de este proceso se van a realizar con estricto respeto del interés superior de los niños y las niñas. Una  de las prioridades definidas en el acuerdo del 15 de Mayo era  que hagan parte de **programas especiales que serán verificados por la Unicef,** la Organización Internacional para las Migraciones y tres organizaciones más que la Mesa definirá.

Según lo pactado, una vez los menores dejen los campamentos **no serán declarados penalmente responsables y se beneficiarán del indulto por rebelión y delitos conexos**. Así mismo, tras la firma del acuerdo final, el Gobierno tramitará todas las medidas necesarias para que los menores procesados o condenados por delitos indultables se acojan a la [Jurisdicción Especial para la Paz ](https://archivo.contagioradio.com/sistema-integral-de-verdad-justicia-reparacion-y-no-repeticion/) para examinar sus responsabilidades.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
