Title: Organizaciones y activistas conmemoraron el día por la despenalización del aborto
Date: 2016-09-29 10:03
Category: Mujer, Nacional
Tags: aborto, Aborto legal, mujeres
Slug: organizaciones-y-activistas-conmemoraron-el-dia-por-la-despenalizacion-del-aborto
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/09/Yo_decido.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Montecruz 

###### 29 Sep 2016 

Este 28 de septiembre cientos de activistas y organizaciones de mujeres se reunieron a lo largo del continente para conmemorar el día por la despenalización del aborto, exigiendo el derecho a decidir libremente sobre sus cuerpos. Esta conmemoración fue designada en el V Encuentro Feminista de América Latina y El Caribe en Argentina en 1990, debido al problema de salud pública que suponen los abortos clandestinos, como se reconoció en 1994 por el índice de muertes y las consecuencias que su práctica no segura ocasiona en las mujeres.

Según la Organización Mundial de la Salud, **anualmente se practican 22 millones de abortos inseguros en el mundo que ocasionan la muerte de 47 mil mujeres**,** **siendo América Latina y el Caribe la región con la tasa más alta de abortos inducidos. Teniendo en cuenta que los únicos lugares que están amparados por la ley para realizar abortos legales son Ciudad de México, Cuba, Guyana, Puerto Rico y Uruguay, en Bogotá se realizaron una serie de actividades encaminadas a **difundir el derecho que tiene toda mujer colombiana, al aborto desde la sentencia C-355 de 2006.**

Organizaciones, redes y activistas sostienen que el derecho a decidir implica el reconocimiento de las mujeres como personas moralmente autónomas, responsables, libres y sujetos de derecho, al igual que el resto de la población. Asimismo exigen despenalizar el aborto, con el fin evitar más casos en los que se vulnere el derecho a la igualdad; como ocurrió el caso de 'Belén', en Tucumán, Argentina quien fue condenada a 8 años de pena privativa de su libertad tras un aborto espontáneo, o como evidencian datos de la OMS que evidencian que **1 de cada 3 mujeres que tiene un aborto en la clandestinidad muere** y casi 800 mil son hospitalizadas al año.

La campaña de este 28 de Septiembre es una iniciativa del movimiento de mujeres latinoamericanas y caribeñas, que luchan por la despenalización del aborto en el marco de la democracia y la justicia social. Si desea participar de la campaña a favor de la despenalización del aborto en América Latina y el Caribe en redes sociales, hágalo a través de los hashtag.

**\#NiMuertasNiPresas  
\#AbortoLegal  
\#28S**

###### [Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)]o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)] ] 
