Title: La paz en Colombia es fundamental para la tranquilidad de la región
Date: 2015-06-09 16:30
Category: Otra Mirada, Paz
Tags: cese al fuego unilateral FARC-EP, ELN, FARC, Foro por la Paz de Colombia, II Foro por la paz de Colombia, marcha patriotica, Montevideo, Uruguay
Slug: estamos-convencidos-del-triunfo-de-los-procesos-sociales-y-politicos-foro-por-la-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/06/del-5-al-7-de-junio-ser-el-ii-foro-por-la-paz-en-colombia-.jpg_1718483346.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: telesur 

<iframe src="http://www.ivoox.com/player_ek_4617683_2_1.html?data=lZuemZucd46ZmKiakpmJd6Kml5KSmaiRdo6ZmKiakpKJe6ShkZKSmaiRkMKfxc7gxdrXrYa3lIqvldOPt9DW08qYxdTRs4zn0NHixc7TssLmjMrZjcjTssfgysjh0ZDUs82ZpJiSo6nYcYarpJKw0dPYpcjd0JC%2Fw8nNs4yhhpywj5k%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Laura Pinzón, asistente a las mesas de debate en Uruguay] 

###### [09 Jun 2015] 

Del 5 al 7 de junio, cerca de mil personas pertenecientes a 120 organizaciones sociales se dieron cita en Montevideo, Uruguay, en el **Segundo Foro por la Paz de Colombia**. Un espacio para la discusión y el debate entorno a un asunto que trasciende las fronteras territoriales y compete a toda América Latina.

**Laura Pinzón**, quien hizó parte de las mesas de debate califica como **"**muy positivo**"** el balance que puede hacerse del encuentro que congregó a diferentes pensadores latinoamericanos, maestros, comunicólogos, periodístas y politólogos entre otros asistentes, que **aportaron sus conceptos e ideas sobre las alternativas que existen para la construcción de la paz en Colombia**.

Durante los actos principales que tuvieron lugar en el paraninfo de la Facultad de Derecho de la Universidad de la República de Uruguay, **se abordaron una serie de propuestas que, desde distintos ejes, resaltaron la importancia de la paz colombiana para la región** como **"**un bastión importante para la actualidad política latinoamericana**"**.

Entre los aportes resultantes de las diferentes mesas de debate y paneles de conversación, Pinzón resalta una preocupación común, considerando que **"**lo que sucede en colombia es el futuro a nivel de soberanía en america Latina**"** **en relación con la constante intervención extranjera económica-militar en el país**, lo que resultaría ser **"**una amenaza para los gobiernos progresistas que de alguna manera se estan configurando en la región y que tratan de pensarse la región desde un lugar distinto**"**.

Otra de las preocupaciones  manifestadas durante el **Panel de Solidaridad Internacional** del mismo evento, es la manifestada desde Paraguay, país donde **"**el modelo paramilitar se esta exportando con la llegada de varios cabecillas paramilitares quienes han migrado buscando replicar el modelo en esa nación **"**. Planteamientos que ratifican que **la discusión sobre como solucionar el conflicto político-social-armado en Colombia es fundamental para la paz, la tranquilidad y la lucha antimilitarista de la región**.

En el cierre del Foro, que contó con la presencia del expresidente **José Mujica**, quién se dirigió al auditorio con un discurso magistral sobre la solidaridad internacional, se presentó un documento como **declaración final del evento,** donde **se ratifica que se busca un continente libre de militarización y que para ello es imprescindible la paz en Colombia**.

Ratifican también **su respaldo al proceso de paz actual y reafirman la necesidad de iniciar el proceso de paz con el ELN**. También afirman que es evidente el triunfo de los procesos sociales en América Latina.

A continuación el texto completo de la declaración final...

"*DECLARACIÓN FINAL*  
*II FORO POR LA PAZ DE COLOMBIA*  
*Montevideo, Junio 7 de 2015*

*“Con tu puedo y con mi quiero*  
*Vamos juntos compañero”.*  
*Mario Benedetti.*

*Reunidos en el II Foro por la Paz de Colombia: 120 organizaciones y movimientos sociales y políticos, más de 800 ciudanados/as latinoamericanos/as, exiliados/as de la hermana Colombia, académicos/as e intelectuales, artistas, parlamentarios/as, luchadores/as sociales y populares, trabajadores/as urbanos, campesinos/as, indígenas, afrodescendientes, mujeres, comunidad LGBTI, jóvenes y estudiantes, de Argentina, Brasil, Colombia, Ecuador, Chile, México,Paraguay, País Vasco, Uruguay y Venezuela, declaramos unánimemente nuestro compromiso con la vida y la democracia, valores que esperamos superen los desastres de la guerra y la muerte.*

*En cumplimiento de este ideario expresado también en las bases constitutivas de la Unasur y la Celac, que defienden la paz como imperativo ético, con el cual se pretende avanzar en la integración regional, constituyendo a Latinoamérica como una región de paz, democrática y con justicia social, declaramos:*

*Que nos encontramos en Montevideo, una diversidad de pensamientos y de proyectos colectivos en un ambiente solidario, reflexivo, esperanzador y deliberativo en la búsqueda de la paz con justicia social para Colombia y de todo nuestro continente; abrazados por el pueblo uruguayo y sus más representativas expresiones organizativas, quienes nos han permitido pisar las calles que tienen grabadas las huellas imborrables del General Artigas, Benedetti, Galeano, Liber Arce y de todo el heroico pueblo uruguayo; haciéndonos sentir con todas las fuerzas, los vientos de cambio de época y las voces por independencia que recorren todos los territorios de Nuestra América.*

*Que estamos convencidos del triunfo de los procesos sociales y políticos anti-neoliberales que recorren todo nuestro continente, que vienen marchando con obstáculos, pero sin pausa, permitiendo el comienzo del fin del bloqueo impuesto por Estados Unidos contra la mayor de las Antillas, nuestra querida República de Cuba. Al tiempo que manifestamos nuestro rotundo rechazo a toda la estrategia de desestabilización del legítimo gobierno de la hermana República Bolivariana de Venezuela y particularmente del decreto Obama.*

*Reivindicamos sin titubeos que las Malvinas son Argentinas y de nuestra américa. En general, nos oponemos a toda forma de intervención militarista en cualquier país de Latinoamérica, por lo cual exigimos la abolición de las bases militares estadounidenses y europeas en la región, puntas de lanza contra la soberanía de Colombia, Paraguay, Honduras, Argentina y toda Nuestra América.*

*Que para transitar hacía un continente libre del militarismo, debe ser superado el largo conflicto social y armado colombiano, que ha dejado millones de víctimas, hombres y mujeres en situación de desplazamiento forzado, desaparecidos/as, asesinados/as, exiliados/as, prisioneros/as políticos/as.*

*Para lo cual es necesario el desmonte real de los grupos paramilitares y los obstáculos militaristas que constituyen uno de los principales obstáculos para la implementación de los posibles acuerdos de paz.       *

*La sociedad colombiana tiene en el proceso de paz una oportunidad histórica para concretar su voluntad de justicia social, democracia y soberanía, con las cuales, las generaciones venideras podrán crecer, expresarse libremente y vivir dignamente.*

*Por ello, respaldamos totalmente los diálogos de paz que se realizan en La Habana entre el gobierno dirigido por el presidente Juan Manuel Santos y las Fuerzas Armadas Revolucionarias de Colombia-FARC-EP, entendiendo que existe la voluntad popular de Colombia y Nuestra América para lograrlo.*

*En este sentido entendemos que para lograr una paz estable y duradera, también es necesaria la apertura del diálogo efectivo con las insurgencias armadas del Ejército de Liberación Nacional-ELN y el Ejército Popular de Liberación-EPL.*

*Respaldamos la declaración de los países garantes del proceso de paz: Noruega y Cuba, en el cual afirman la necesidad de un inmediato cese bilateral al fuego,  permitiendo que el diálogo y la palabra primen sobre el estruendoso ruido de la guerra. Esto generará un ambiente de confianza para que la sociedad colombiana y las partes se dirijan con más fuerza hacía un acuerdo de paz estable y duradero.*

*En consecuencia, entendemos que el único camino posible es la unidad del pueblo colombiano. Con alegría reconocemos y saludamos los avances y acumulados en iniciativas como el Frente Amplio por la Paz con justicia social, la Cumbre Agraria: campesina, étnica y popular, y otros procesos de convergencias; para éstas y para todo el pueblo colombiano, pedimos el respeto por sus derechos humanos y la acción plena del ejercicio político, sin que sean intimidados/as, judicializados/as o amenazados/as.*

*Solicitamos garantías políticas y humanitarias para las elecciones locales y departamentales en octubre de 2015, frente a lo cual esperamos contribuir en la construcción de una misión de observación electoral que acompañe diferentes regiones del país.*

*En este sentido también solicitamos la suspensión de las detenciones arbitrarias a dirigentes sociales, los montajes judiciales, la criminalización de la protesta social. Proponemos una vez más una visita de observación sobre la situación de los/as prisioneros/as políticos/as, para contribuir a la garantía de condiciones dignas en las cárceles de Colombia. Pero sobre todo somos solidarios son la exigencia de libertad para los casi 10.000 prisioneros/as políticos/as en ese país.           *

*Que recibimos con esperanza la realización del I Foro Parlamentario por la Paz de Colombia, el que permitió la constitución de la red de parlamentarios/as por la paz de Colombia.*

*En coherencia con todo lo anterior, proponemos crear diversas redes de trabajadores/as, de comunidades agrarias (campesinas y étnicas), de defensores/as de derechos humanos, de jóvenes y estudiantes, de artistas e intelectuales y de mujeres. Desde las cuales asumamos la difusión y acompañamiento a la lucha por la paz con justicia social en Colombia; organicemos visitas de solidaridad a Colombia; coordinemos encuentros y foros por sectores; asumamos la Campaña “Yo te nombro Libertad” y otras campañas que fortalezcan y dinamicen esas luchas.*

*Así mismo, asumimos la revisión cuidadosa de todas las propuestas realizadas a lo largo de este II Foro por la paz de Colombia para que podamos materializarlas. Nos comprometemos en la conformación de una delegación que lleve las conclusiones de este Foro a la mesa de diálogos en la Habana.*

*Y, finalmente, nos comprometemos, desde ahora, en la organización y participación de una tercera versión de este Foro.*

*Somos todos y todas Colombia, estamos en el camino de la unidad Latinoamericana continuando la tradición, artiguista, san martiniana, martiana y bolivariana.*

*Somos más, ahora sí la paz de Colombia con justicia social, democracia y soberanía, por una Latinoamérica libre de militarismo y donde se respeten efectivamente los derechos humanos.*

*Vamos juntos compañeros y compañeras."*
