Title: Peñalosa y el fracaso de la política para atender a los habitantes de calle
Date: 2019-07-20 13:21
Author: CtgAdm
Category: DDHH, Entrevistas
Tags: Calle sexta, Habitantes de calle, Intervención, Peñalosa
Slug: penalosa-fracaso-habitantes-de-calle
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/Habitantes-de-Calle.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Fray Gabriel Gutiérrez  
] 

Desde la tarde del pasado miércoles 18 de julio, funcionarios de **la Alcaldía iniciaron una nueva intervención contra habitantes de calle que se habían establecido en el canal de la calle 6 con carrera 30**. El hecho fue criticado por defensores de derechos humanos, porque la Alcaldía estaría cometiendo los mismos errores en los que incurrió cuando optó por desalojar el Bronx.

Fray Gabriel Gutiérrez, defensor de derechos humanos que hace años desarrolla su trabajo con poblaciones vulnerables dijo estar preocupado por la manera con que se tratan a estas ciudadanías, pues recuerdan la **"intervención fallida del 28 de mayo de 2016 en el Bronx"**. Fallida porque tiene un carácter policivo y no humanitario. (Le puede interesar:["Fin del Bronx no acaba con la violencia contra habitantes de calle"](https://archivo.contagioradio.com/fin-del-bronx-no-acaba-con-la-violencia-contra-habitantes-de-calle/))

En ese sentido, Fray recordó que el fenómeno de la habitabilidad en calle está unido a la violencia estructural que vive el país, el desplazamiento y la pobreza. **"Si encontramos seres humanos sumergidos en la droga, la tristeza y la miseria, el ofrecimiento tiene que ser humanitario, no policivo"**, señaló el Religioso; en cambio, cuestionó que parece que los trataran como a prisioneros, llevándolos a hogares de paso que no brindan soluciones de fondo.

### **Peñalosa está usando el método Donald Trump con los habitantes de calle  
**

Donald Trump llegó a ocupar la presidencia de Estados Unidos, en buena medida, gracias a su propuesta xenófoba de buscar construir un muro en la frontera sur del país, para evitar la movilidad humana que llega a norteamérica sin documentos. El argumento usado por Trump para construir el muro es que los migrantes y refugiados suramericanos son la causa de la inseguridad que se vive en EE.UU. (Le puede interesar:["El muro no va a ser en la frontera sino en México entero"](https://archivo.contagioradio.com/muro-no-va-a-ser-en-la-frontera-34924/))

De igual forma, Fray declaró que ahora se señala a esta ciudadanía (los habitantes de calle) como el "origen de la violencia e inseguridad", pero en realidad ellos mismos son víctimas de ambos fenómenos. Por su parte, el Alcalde también acudió a lo que Gutiérrez llama **"el método Donald Trump", para mantener a los habitantes de calle lejos de puentes peatonales o, en este caso, del canal de la sexta**. (Le puede interesar:["Desalojo del Bronx sólo favoreció a empresas privadas"](https://archivo.contagioradio.com/a-un-ano-de-la-retoma-del-bronx-se-evidencia-la-improvisacion-del-desalojo/))

> Llama poderosamente la atención la relación de la violencia física contra [\#CHdCalle](https://twitter.com/hashtag/CHdCalle?src=hash&ref_src=twsrc%5Etfw) en la zona centro de Bogota la 32 intervención contra esta ciudadanía en el caño los Comuneros [@DefensoriaCol](https://twitter.com/DefensoriaCol?ref_src=twsrc%5Etfw) [@PoliciaColombia](https://twitter.com/PoliciaColombia?ref_src=twsrc%5Etfw) [@integracionbta](https://twitter.com/integracionbta?ref_src=twsrc%5Etfw) [@idipron](https://twitter.com/idipron?ref_src=twsrc%5Etfw) [@FiscaliaCol](https://twitter.com/FiscaliaCol?ref_src=twsrc%5Etfw) [@TembloresOng](https://twitter.com/TembloresOng?ref_src=twsrc%5Etfw)[@AlirioUribeMuoz](https://twitter.com/AlirioUribeMuoz?ref_src=twsrc%5Etfw) [pic.twitter.com/eH9svQDyrd](https://t.co/eH9svQDyrd)
>
> — Fray Gabriel G (@Fraygabrielofm) [February 1, 2019](https://twitter.com/Fraygabrielofm/status/1091437117077704705?ref_src=twsrc%5Etfw)

<p>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
</p>
### **Tenemos una política fallida para atender el fenómeno de habitabilidad de calle**

Fray Gabriel recordó que tras la intervención del Bronx, **"se han presentado 40 intervenciones policivas violentas"**, en algunas ocasiones sin presencia de defensores de derechos humanos. Sumado a ello, en el informe "Los Nunca Nadie", que hace seguimiento se cuentan más de 4 mil asesinatos contra habitantes de calle en los últimos 10 años; ambas situaciones deberían indicar que estamos ante "una política fallida, un abordaje fallido" del fenómeno.

Para finalizar, el Religioso llamó la atención del Defensor del Pueblo, porque no ha actuado en el sentido de proteger los derechos de esta población; asimismo, sentenció que era necesario buscar soluciones más humanas y en derecho para atender con "solidaridad y compasión a estas personas". (Le puede interesar: ["Más de 4 mil habitantes de calle han sido asesinados en 10 años"](https://archivo.contagioradio.com/habitantes-de-calle-asesinados/))

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>  
<iframe id="audio_38663127" frameborder="0" allowfullscreen scrolling="no" height="200" style="border:1px solid #EEE; box-sizing:border-box; width:100%;" src="https://co.ivoox.com/es/player_ej_38663127_4_1.html?c1=ff6600"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
