Title: Estas son las recomendaciones de la Misión Electoral para la reforma política
Date: 2017-04-18 17:07
Category: Nacional, Paz
Tags: Misión electoral, Reforma política electoral
Slug: estas-son-las-recomendaciones-de-la-mision-electoral-para-la-reforma-politica
Status: published

###### [Foto: El País] 

###### [18 Abr 2017] 

La Misión Electoral Especial de Colombia presentó la síntesis de sus propuestas para la reforma política y electoral, que tendrá como finalidad asegurar una mayor autonomía e independencia de la organización electoral; **modernizar y hacer más transparente el sistema electoral; dar mayores garantías para la participación política** en igualdad de condiciones y mejorar la calidad de la democracia.

Durante un periodo de 4 meses, está misión concentró sus esfuerzos en tres ejes: la reforma de la **arquitectura institucional, la reforma del sistema electoral y la reforma del financiamiento de la política del país**. Le puede interesar: ["Colombia tiene un sistema electoral que le permite elegir criminales: MOE"](https://archivo.contagioradio.com/sistema-electoral-elegir-criminales-35907/)

### **La Arquitectura Institucional** 

Para esta reforma la MEE tenía como objetivo formular recomendaciones que permitan el fortalecimiento de un modelo que sea eficiente, sin funciones duplicadas, eficaz, con transparencia en el ámbito electoral y financiamiento de la política, **que supervise a las organizaciones políticas.**

Para ello la MEE propone cambios en la estructura de 3 instituciones electorales: **la Registraduría Nacional del Estado Civil, el Consejo Electoral Colombiano y La Corte Electoral**, que deberán trabajar de forma coordinada, pero que tendrán funciones diferentes. Le puede interesar: ["Víctimas proponen criterios para escoger integrantes de la SIVJRNR"](https://archivo.contagioradio.com/criterios-integrantes-del-sivjrnr/)

La Registraduría Nacional conservará las funciones que tiene actualmente, sin embargo, se le recomienda que debe **avanzar tecnológicamente para depender cada vez menos de las empresas privadas**, el Registrador Nacional tendrá un periodo de dirección de 4 años, no podrá ser reelegido y será designado por el concurso que realizan las Altas Cortes.

El **Consejo Electoral Colombiano tendrá cinco funciones:** reglamentar el proceso electoral, participar activamente durante los procesos electorales, hacer control de la vida de las organizaciones políticas; es decir, les otorgará el registro; llevará el padrón de afiliados, dirimirá los conflictos internos, entre otros; será responsable de otorgar el financiamiento público directo e indirecto en tiempos ordinarios y de campaña a los partidos, recibe la rendición de cuentas, audita, sanciona y acusa ante la Corte Electoral y estará a cargo de la educación democrática y ciudadana.

El CEC estará conformado por un cuerpo colegiado de 7 consejeros, elegidos por la misma institución, a partir de ternas enviadas por el Consejo de Estado, la Corte Constitucional, la Corte Suprema y la Presidencia de la República, tendrán un mandato por ocho años, sin derecho a reelección y **no puede haber tenido funciones partidarias o de elección popular por lo menos diez años antes de la posesión**.

La **Corte Electoral será el órgano especializado en la Jurisdicción electoral, se integrará en la rama Judicial** y sus competencias serán: la resolución de las demandas de nulidad de elecciones y de los actos de contenido electoral; definirá la pérdida de investidura y la pérdida del cargo; decidirá las sanciones disciplinarias de funcionarios elegidos popularmente cuando impliquen separación del cargo, declarará la pérdida y suspensión de la personería jurídica de las organizaciones políticas, y la privación del derecho de presentar candidatos en una circunscripción.

Estará conformado por un cuerpo colegiado de **7 abogados, con experiencia mínima de 15 años** y tendrá el mismo mecanismo que el Consejo Electoral Colombiano, tendrán un periodo de 8 años, sin reelección. Le puede interesar:["Estatuto de Oposición primer paso en largo camino para la participación"](https://archivo.contagioradio.com/estatuto-de-oposicion-un-primer-paso-en-largo-camino-para-la-participacion/)

Finalmente, la MEE señala que los **mecanismos de elección a cargos buscan garantizar la independencia de las autoridades electorales** con respecto a los partidos y las ramas del poder público.

### **Reforma al Sistema Electoral** 

Estas recomendaciones tienen como fin abrir el espacio para que ingresen nuevas organizaciones políticas**, fortalecer los partidos políticos, limitar la personalización de la política y el clientelismo**, mejorar la calidad de la representación política con grupos que tengan poca participación en espacios decisorios y proponer un sistema electoral más sencillo.

En esta medida proponen que el **Senado conserve la circunscripción nacional con 100 senadores, el cambio se encuentra en el paso del voto preferente a la lista cerrada y bloqueada**, esto para que las campañas de los partidos sean más unificadas y desaparezcan las campañas personalistas. Además de esta manera los candidatos deben reunirse en torno a las características del partido y no a las de una persona.

Para la Cámara de representantes se propone un modelo diferente que consiste en dos votos, uno por la lista departamental del partido, cerrada y bloqueada, el otro será por el candidato del partido en la circunscripción nacional. Cada departamento tendrá dos representantes y un tercero que corresponda a la densidad poblacional del 1%. Sin embargo, se mantendrá **Cámara con 173 representantes: 104 elegidos en circunscripción uninominal, 69 en lista plurinominal**.

Frente a la **participación de género se recomienda una aplicación del 40% en las listas** con un paso en la oportunidad siguiente a la paridad, alternancia y universalidad. Sobre la participación en las Asambleas departamentales y Consejos municipales, se plantea de igual forma, cerrar las listas y permitir una segunda vuelta para los 6 municipios más grandes. Le puede interesar: ["Se creara grupo de expertos en tierras para implementación de Acuerdos de Paz"](https://archivo.contagioradio.com/se-creara-grupo-de-expertos-en-tierras-para-implementacion-de-acuerdos-de-paz/)

### **Financiamiento de la Política** 

En este punto la MEE propone un sistema mixto, en el que el aporte del Estado tenga un peso importante con aportes directos e indirectos, **mientras se reducen las fuentes privadas de financiación, sobre todo durante las campañas**. Le puede interesar ["Jurisdicción de Paz será una herramienta para las víctimas"](https://archivo.contagioradio.com/jurisdiccion-de-paz-sera-una-herramienta-para-las-victimas/)

De esta forma es importante que se incremente los aportes a las organizaciones políticas en épocas no electorales, en esta medida proponen aumento del **25% de financiamiento, para las organizaciones políticas con personería jurídica**, un 55% en función de los cargos obtenidos y un 20% por la participación de actores relegados como jóvenes y mujeres.

Esta financiación se entregará en dos etapas, el primer **50% antes del proceso electoral y el otro 50% después de las elecciones**, de manera proporcional a los resultados obtenidos. El financiamiento indirecto se hará con la disposición del transporte público el día de la elección y con un mínimo de propaganda de cada campaña que se difunda en medios de comunicación.

A su vez, se recomienda **limitar los aportes propios que realicen candidatos y familiares de los mismos a la campaña, se restringen los montos que aporten las empresas e individuos** y se recomienda convertir en obligación legal la entrega de información contable a través del mecanismo de cuentas claras.

Para el control de los recursos que ingresan a la política, el Consejo Electoral Colombiano **tendrá una unidad que hará seguimiento de la circulación de fondos en las campañas y monitoreará los principales rubros de gasto.**

[infografia propuestas para reforma del sistema electoral](https://www.scribd.com/document/345552109/infografia-propuestas-para-reforma-del-sistema-electoral#from_embed "View infografia propuestas para reforma del sistema electoral on Scribd") by [Contagioradio](https://es.scribd.com/user/276652802/Contagioradio#from_embed "View Contagioradio's profile on Scribd") on Scribd

<iframe id="doc_79583" class="scribd_iframe_embed" src="https://www.scribd.com/embeds/345552109/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-hE6Mpn4sdu8MLFMAOrcc&amp;show_recommendations=true" width="75%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="0.7147385103011094"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)
