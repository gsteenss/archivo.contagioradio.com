Title: Surge alianza para combatir las mafias de tráfico de inmgrantes
Date: 2015-05-26 13:52
Category: DDHH, El mundo
Tags: Alianza en contra de mafias ide inmigrantes, Bulgaria, Bulgaria y Turquía contra mafias de inmigrantes, Centro de intercambio de información contra mafias inmigrantes Grecia, Grecia, Turquía, Unión europea
Slug: surge-alianza-para-combator-las-mafias-de-inmgrantes
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/05/ab8298f791677596e948eff0062ef6de.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto:ElConfidencial.com 

El lunes **25 de Mayo Grecia, Bulgaria y Turquía firmaron un acuerdo para controlar** la segunda entrada más importante de **inmigrantes en Europa y de esta forma poder controlar las mafias** que trafican con los refugiados de Oriente Próximo. A pesar de que **Turquía no es miembro de la UE**, Bruselas está a favor del acuerdo y ha pedido que se acelere su aplicación.

En un principio el acuerdo comportaría la creación de un **Centro de Información** presumiblemente en territorio Búlgaro, que será quien lidere la misión, en la frontera entre este y Turquía. El Centro tendrá una duración de tres años con posibilidad de renovación del acuerdo.

El objetivo de la colaboración es **recabar datos para poder frenar el ascenso de las mafias que controlan el flujo migratorio de las guerras actuales de Oriente Próximo hacia Europa**. Según los informes de las organizaciones sociales de DDHH, la gran mayoría de estos inmigrantes son refugiados ya que huyen de conflictos armados donde son perseguidos y violados su DDHH.

En **2014** en las fronteras entre los tres países entraron ilegalmente más de **11.000 personas, siendo Turquía, el país por donde entran los refugiados de las guerras de Irak,Siria o Afganistán,** convirtiéndose así la frontera entre Bulgaría y Turquía el paso obligatorio hacia la UE, para más tarde acabar en Grecia como puerta última.

Los **tres países** firmantes han sido **muy críticos con las política de la UE con respecto al acuerdo para fenar la inmigración ilegal en el Mediterráneo**, primera puerta de entrada, y además han advertido que debido principalmente a la guerra en Siria corren riesgo de sufrir una crisis humanitaria con la masiva llegada de estos.

Este conforma el segundo paso de la UE con respecto a la **emergencia humanitaria de inmigrantes** ilegales potencialmente refugiados, que pretende taponar la entrada de Europa y combatir las mafias de trata de personas.
