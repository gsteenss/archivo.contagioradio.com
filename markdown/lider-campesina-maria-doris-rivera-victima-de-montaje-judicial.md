Title: Líder campesina María Doris Rivera, víctima de montaje judicial
Date: 2015-02-11 20:11
Author: CtgAdm
Category: DDHH, Judicial, Política
Tags: Agraria y Popular del Oriente Colombiano, Amanda Vasquez, ASPROMACARENA, María Doris Rivera Rios, Mesa de Interlocución y Acuerdo, Mesa de Unidad Cívico
Slug: lider-campesina-maria-doris-rivera-victima-de-montaje-judicial
Status: published

###### Foto: NotiMundo 

##### <iframe src="http://www.ivoox.com/player_ek_4069578_2_1.html?data=lZWjm5qbfI6ZmKiakp6Jd6KklJKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRh8bmxMaYxsqPfJGfxMba0srXrc%2Fj1JDRx9GPs9PdxtPhx5DHs83jzsfWw9PTb9Tjz5Dd1NTHqY6ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe> 

##### [Esmer Montilla, Fundación para los Derechos Humanos y el DIH del Oriente Colombian]o 

La tarde del martes 10 de febrero, la líder campesina **María Doris Rivera Rios** fue detenida por miembros de la DIJIN cuando se trasladaba del municipio de La Macarena, en el departamento del Meta, hacia Villavicencio, acusada de Rebelión por la Fiscalía. Su caso es el mismo que vivió hace cinco meses **Amanda Vasquez**, otra lideresa de la región.

Para al organización campesina **ASPROMACARENA**, de la cual Maria Doris es presidenta, esta captura se trata de un **montaje judicial que se suma a los más de 80 procesos que se llevan a cabo en contra de lideresas y líderes agrarios**, y que responden a "una política dirigida en contra de las organizaciones que reclaman sus derechos, y que denuncian las fechorías de la fuerza pública y los diferentes organismos del Estado".

Esmer Montilla, presidente de La **Fundación de Derechos Humanos del Oriente Colombiano**, señala que las y los líderes sociales del departamento han denunciado activamente al Ejercito por la **ejecución extrajudicial de campesinos y campesinas en ese territorio,** a quienes la fuerza pública hace pasar por miembros de la guerrilla. Como represalia, el Estado estarían adelantando este tipo de montajes judiciales en los municipios de Granada, San Martín y La Macarena, perjudicando no sólo el buen nombre de las y los líderes procesados, sino también el proceso organizativo al que pertenecen. "Para nosotros es muy critica la situación que están viviendo en el departamento las Juntas de Acción Comunal y las organizaciones campesinas", concluye Montilla, "le hacen montajes a una persona porque esta hablando la realidad de las cosas. Y es por eso que han asesinado gente, y hacen cosas atroces".

María Doris Rivera Rios hace parte también de la **Mesa de Unidad Cívico, Agraria y Popular del Oriente Colombiano, de la Mesa de Interlocución y Acuerdo MIA,** y de la Cumbre Agraria, que actualmente desarrolla negociaciones con el gobierno nacional sobre la situación de la ruralidad colombiana.
