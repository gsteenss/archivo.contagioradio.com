Title: Más de 3 millones de colombianos "atrapados en un círculo vicioso de hambre y violencia"
Date: 2016-07-29 13:17
Category: DDHH, El mundo
Tags: Hambre en el mundo 2016, Informe sobre el hambre FAO 2016, Parlamentarios contra el hambre
Slug: mas-de-3-millones-de-colombianos-atrapados-en-un-circulo-vicioso-de-hambre-y-violencia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/07/Colombia-y-el-hambre.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Primicidiario] 

###### [28 Julio 2016] 

Más de 56 millones de habitantes de 17 países que enfrentan conflictos prolongados, viven **"atrapados en un círculo vicioso de hambre y violencia"**, una grave situación de inseguridad alimentaria que obstaculiza los esfuerzos mundiales para erradicar la malnutrición, según lo reportan los más recientes estudios elaborados por la Organización de las Naciones Unidas para la Alimentación y la Agricultura FAO, y el Programa Mundial de Alimentos PMA, que fueron presentados ante el Consejo de Seguridad de la ONU.

En Yemen más de la mitad de la población, **14 millones de personas, viven en estado de crisis alimentaria**; en Siria 8,7 millones de habitantes, el 37% de la población, necesitan urgentemente ayuda nutricional, mientras que en Sudán del Sur 4,8 millones de pobladores requieren este tipo de asistencia y en Líbano, el 89% de los refugiados sirios la necesitan. En niveles más bajos, pero igualmente preocupantes se sitúan la República Centroafricana, Burundí y Haití, con el 50, 23 y 19% de su población con inseguridad alimentaria.

De acuerdo con estos estudios, **en Colombia por lo menos 3 millones de personas se enfrentan todavía a niveles elevados de inseguridad alimentaria**, relacionados justamente con el largo periodo de conflicto armado que hemos enfrentado, pues tanto la FAO como el PMA han indicado que "cada hambruna en la era moderna se ha caracterizado por un conflicto" en el que se han destruido cultivos e infraestructuras agrícolas, perturbado mercados y generado desplazamientos que han socavado la seguridad alimentaria.

Según las organizaciones "hacer frente al hambre puede ser una contribución importante a la consolidación de la paz", y en este sentido fue lanzada este jueves en el Congreso de la República la **iniciativa 'Parlamentarios Contra el Hambre' que ya cuenta con nodos en distintas naciones latinoamericanas** y que para el caso colombiano busca erradicar el fenómeno del hambre a través del [[fortalecimiento de la agricultura familiar](https://www.facebook.com/VictorCorreaVelez/videos/847726345357376/)] y la protección de la producción nacional.

###### [Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)]] 

###### 
