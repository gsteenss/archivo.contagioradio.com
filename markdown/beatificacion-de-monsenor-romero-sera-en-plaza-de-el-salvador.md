Title: Beatificación de Monseñor Romero será en Plaza de El Salvador
Date: 2015-02-03 23:15
Author: CtgAdm
Category: El mundo, Movilización
Tags: Beatificación, Monseñor Oscar Arnulfo Romero, Papa Francisco, Vaticano
Slug: beatificacion-de-monsenor-romero-sera-en-plaza-de-el-salvador
Status: published

###### Foto: laprensa.hn 

<iframe src="http://www.ivoox.com/player_ek_4034827_2_1.html?data=lZWglp2We46ZmKiak5aJd6KmlZKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRhdPhwtPR0ZCxaaSnhqae1NbZqduhhpywj6jTstXVyM7cjbfFqMrjjJKSmaiReA%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Armando Márquez Sec Ejecutivo del SICSAL] 

Con la declaratoria de mártir de Monseñor Romero, el papa Francisco da vía libre a la beatificación del obispo salvadoreño, **asesinado en 1980 en una operación militar al interior de su propia iglesia, tras denunciar las masacres cometidas por las fuerzas militares de ese país en medio del gobierno de derecha**.

Para Armando Márquez, secretario ejecutivo del Servicio Internacional de Solidaridad con América Latina, Monseñor Romero ya era el **“Santo del Pueblo”** pero el reconocimiento de su santidad por parte del Vaticano, era un paso muy importante, puesto que con la memoria y el testimonio de Romero se han identificado miles de cristianos y cristianas en el mundo que **reivindican las causas de los empobrecidos del mundo**.

Márquez recuerda que **hay muchos mártires de la Iglesia católica como Monseñor Romero**, y que honrarlo con la santificación abre las puertas para la dignificación de todos ellos y ellas. Aunque no se conoce la fecha del evento de beatificación si se habría establecido que será en la capital del Salvador y posiblemente en ausencia del sumo pontífice.

Este es el documento con el que se anuncia la decisión del Sumo Pontífice y del Vaticano.

[![documento-beatificación-romero](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/02/documento-beatificación-romero-150x150.jpg){.wp-image-4334 .aligncenter width="269" height="269"}](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/02/documento-beatificación-romero.jpg)

A diferencia de otros candidatos a la beatificación, los mártires pueden alcanzar la beatificación, el primer paso a la posible elevación a los altares, sin que se atribuya un milagro a su intercesión. La canonización sí requiere un milagro.
