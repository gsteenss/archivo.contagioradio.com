Title: Recomendados de Agosto en Cine Tonalá
Date: 2015-08-12 14:40
Author: AdminContagio
Category: 24 Cuadros, Cultura
Tags: La princesa de Francia de Matías Piñeiro, La tierra y la sombra, Programación Cine Tonalá Agosto, Sady González: una luz en la memoria, The Gambler de Ignas Jonynas
Slug: recomendados-de-agosto-en-cine-tonala
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/08/10659155_295237754002043_6808587449370263673_n.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

[En agosto, les compartimos algunos recomendados para ver este mes en Cine Tonalá:]

[![Losejas-The-Gambler-e1379515198552](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/08/Losejas-The-Gambler-e1379515198552.jpg){.wp-image-12107 .size-full width="619" height="310"}](https://archivo.contagioradio.com/recomendados-de-agosto-en-cine-tonala/losejas-the-gambler-e1379515198552/)

**The Gambler de Ignas Jonynas |Lituania|2013|109’**[,  Vicentas es un enigmático hombre que lleva una doble vida, una extremadamente peligrosa. Es un paramédico muy respetado en su compañía y es al tiempo un adicto a las apuestas. Su adicción y sus descontroladas deudas lo empujarán a crear su propio juego ilegal relacionado con su profesión.]

[![La-tierra-y-la-sombra-05](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/08/La-tierra-y-la-sombra-05-e1439407541193.jpg){.alignnone .wp-image-12109 width="619" height="347"}](https://archivo.contagioradio.com/recomendados-de-agosto-en-cine-tonala/la-tierra-y-la-sombra-05/)

**La tierra y la sombra de  César Augusto Acevedo | Colombia | 2015 | 97’**[, (Ganadora de La Cámara de oro en Cannes 2015) narra la historia de Alfonso, quien  es un viejo campesino que regresa a la casa que abandonó porque su hijo está enfermo. Al llegar, descubre su vieja casa, donde la mujer que alguna vez fue su esposa aún vive, con su nuera y su nieto.]

![images-cms-image-000037863](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/08/images-cms-image-000037863.jpg){.alignnone .wp-image-12111 width="619" height="277"}

**Sady González: una luz en la memoria  de Margarita Carrillo & Guillermo González | Colombia | 2015 | 54’,** [cuenta la historia inédita del fotógrafo colombiano Sady González, pionero en reportería gráfica y creador de uno de los más importantes archivos fotográficos de su país.]

[![princesa1](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/08/princesa1-e1439407770458.jpg){.alignnone .wp-image-12113 width="619" height="326"}](https://archivo.contagioradio.com/recomendados-de-agosto-en-cine-tonala/princesa1/)

**La princesa de Francia de Matías Piñeiro | Argentina | 2014 | 70’**[, Un año después de la muerte de su padre en México, Víctor regresa a Buenos Aires con la intención de rehacer su vida. Uno de sus proyectos es realizar una serie de radioteatros latinoamericanos con su antigua compañía teatral, en la que participan cinco mujeres que lo conocen muy bien. Mientras trabajan en el proyecto, los personajes recapacitan sobre amores perdidos.]

[Los precios de entrada oscilan entre 5.000 y 7.000 para estudiantes, y 7.000 a 9.000 público general. ]

[Otros eventos y películas son de entrada libre (Ver [programación completa](http://www.lbv.co/velvet_voice/cinetonalabog/2015/ctb_prog_agosto_2015.html?utm_campaign=982%3A+Programacion+AGOSTO+2015+%2F+The+Gambler+%2B+La+tierra+y+la+sombra+%2B+Sonidos+vecinos+%2B+El+abrazo+de+la+serpiente&utm_source=MasterBase+LBV&utm_medium=email&utm_content=2&utm_term=none))]

[Cine Tonalá: (CRA. 6 \# 35-37(Barrio La Merced)]

[[\#‎EsteMesQuieroVerBuenCineEnCineTonalá‬]](https://www.facebook.com/hashtag/estemesquieroverbuencineencinetonal%C3%A1?source=feed_text&story_id=969419986449051)

Vista: [QueQuieresHacerHoy](https://www.facebook.com/yoquierohacer?fref=ts) y encuentra más Eventos
