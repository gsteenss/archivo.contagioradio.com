Title: Tutela busca frenar actividades petroleras de Ecopetrol en Guamal, Meta
Date: 2017-09-17 08:21
Category: Ambiente, Nacional
Tags: Ecopetrol, Guamal
Slug: tutela-busca-frenar-actividades-petroleras-de-ecopetrol-en-guamal-meta
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/02/guamal.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:] 

###### [17 Sept 2017]

El pasado 13 de septiembre, el juzgado municipal de Guamal, Meta, admitió la tutela de un habitante de este municipio, en el que solicita el amparo constitucional al derecho al debido proceso, vulnerado por las presuntas irregularidades en el momento en el que el consejo municipal derogo las restricciones a la actividad petrolera en el territorio.

Esa restricción prohibía este tipo de actividades en los sectores en donde hoy se desarrollan los proyectos Trogón I y Lorito I, ambos a cargo de la empresa Ecopetrol. En diferentes ocasiones, los habitantes de este municipio han denunciado las graves afectaciones que ha sufrido y la fuerte represión que ha existido por parte del ESMAD, **hacia las movilizaciones pacíficas que han emprendido para oponerse a estos proyectos.**

Además, la comunidad ha manifestado que el proyecto tiene múltiples irregularidades “**“Habían modificado el esquema de ordenamiento territorial del municipio en dos oportunidades, quitando y poniendo** alguna restricción para favorecer a la industria y hacer oídos sordos a las denuncias de las comunidades” afirmó Diego Salcedo, abogado que ha acompañado a la comunidad. (Le puede interesar: ["Ecopetrol hace caso omiso a Procuraduría y comunidad en Guamal"](https://archivo.contagioradio.com/ecopetrol-hace-caso-omiso-a-contraloria-procuraduria-y-comunidad-para-entrar-en-guamal/))

Sumado a estos hechos, el procurador Helmer Fino aseguro que hay otras 8 irregularidades más, que lo llevaron a tomar la decisión de frenar este proyecto y de solicitarle a la Agencia Nacional de Licencias Ambientales que investigue las situaciones que se están presentando. (Le puede interesar: ["Ecopetrol debería suspender proyecto Trogón I en Guamal, Meta"](https://archivo.contagioradio.com/46238/))

1.  Las fuentes hídricas podrían estar en amenaza ante la realización de este proyecto.
2.  La vocación productiva que tiene la Vereda podría estar en peligro, afectando la soberanía alimentaria de la comunidad.
3.  Falta de socialización del Proyecto y sus impactos ambientales.
4.  Por falta de definición del programa de compensación forestal.
5.  Falta de claridad en los planes de contingencia en caso de derrames.
6.  No se conoce de la entrega de un documento que dé cuenta de los daños que podrían sufrir las fincas de la zona.
7.  No se conoce de la entrega del documento donde delimitan la vereda para conocer las zonas donde la empresa no podrá hacer intervención.
8.  Falta de estudios sobre el posible hallazgo de acuíferos subterráneos en 1032 hectáreas donde se llevará a cabo el proyecto.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
