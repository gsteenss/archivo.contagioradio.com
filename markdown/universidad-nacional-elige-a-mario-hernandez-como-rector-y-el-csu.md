Title: Universidad Nacional eligió a Mario Hernández como rector ¿Y el CSU?
Date: 2015-03-19 18:44
Author: CtgAdm
Category: Educación, Nacional
Tags: educacion, Federación Médica Colombiana, Ignacio Mantilla, mario hernandez, Universidad Nacional, Universidad pública
Slug: universidad-nacional-elige-a-mario-hernandez-como-rector-y-el-csu
Status: published

##### Foto: Mario Hernández 

<iframe src="http://www.ivoox.com/player_ek_4237054_2_1.html?data=lZegmZWZeI6ZmKiakp2Jd6Kll5KSmaiRdo6ZmKiakpKJe6ShkZKSmaiRkcLmytSYqsrWsoa3lIquk9PIqdufyMbbw5DHqc%2Fn0JDRx5DTtMriyoqwlYqmd8%2Bf1NTP1MqPtMLmwpDfx8jYcYarpJKw0dPYpcjd0JC%2Fw8nNs4yhhpywj5k%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Mario Esteban Hernández, candidato a la rectoría de la U. Nacional] 

<iframe src="http://www.ivoox.com/player_ek_4237067_2_1.html?data=lZegmZWae46ZmKiakp2Jd6KplZKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRkcLmytSYqsrWsoa3lIquk9PIqdufyMbbw5DHqc%2Fn0JDRx5DTtMriyoqwlYqmd8%2Bf1NTP1MqPtMLmwpDfx8jYcYarpJKw0dPYpcjd0JC%2Fw8nNs4yhhpywj5k%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [María Alejandra Rojas, Representante estudiantil U. Nacional] 

El candidato a la rectoría de la Universidad Nacional y vocero de la Federación Medica Colombiana, Mario Hernández, recibió la mayor votación en el censo de opinión de la comunidad universitaria sobre quién debería ser el próximo rector de la institución. **Con un total de 7150 votos ponderados, el Médico Hernández ganó sobre el actual rector Ignacio Mantilla  que obtuvo 6219 votos.**

“Es un mensaje muy claro, la comunidad ha recibo muy bien la propuesta de recuperar la identidad publica de la universidad, **el consejo superior debe entender el mensaje que aporta a respetar la legitimidad y acompañamiento de la comunidad universitaria**” afirmó el Doctor.

Cabe resaltar que la cercanía entre en el resultado ponderado entre Hernández y Mantilla, se debe a que **los votos de los docentes valían el 60% de la votación sobre un 30% de los estudiantes y un 10%** de los egresados, ya que “la reglamentación ha decidido que los docentes son los que están llamados a elegir, por ser la comunidad más estable de la Universidad”, explica María Alejandra Rojas, representante estudiantil. La estudiante destaca que esta medida es criticada por la comunidad, debido a que no se tiene en cuenta a la opinión de trabajadores y administrativos y valora de manera diferenciada la importancia de los estamentos.

[![Votaciones Un Contagio Radio](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/03/Votaciones-Un-Contagio-Radio.png){.aligncenter .wp-image-6188 width="482" height="441"}](https://archivo.contagioradio.com/actualidad/universidad-nacional-elige-a-mario-hernandez-como-rector-y-el-csu/attachment/votaciones-un-contagio-radio/)

Pese a que usualmente el Consejo Superior se separa de la consulta con distintos argumentos, es claro que se debe dar una “**cohesión para generar el desarrollo de la comunidad universitaria”,** dice el candidato elegido, quien critica una tendencia de los últimos 20 años, en que la universidad entra cada vez más en “una política de  auto-sostenibilidad y una lógica de mercado que nos hacer ver como una empresa común y corriente que deberá parecer a la Universidad de los Andes o cualquier otra para sobrevivir”, asegura el vocero de la Federación Medica Colombiana.

La propuesta del docente, se enfoca en rescatar **el modelo nacional público y estatal de la universidad,** además, Hernández se compromete a buscar reformar la composición del Consejo Superior, pese a que la única forma es por medio del Congreso a través de una Ley de régimen especial de Universidad Nacional, con el objetivo de que no solo sea ejecutora de la política del gobierno,  sino que debe tener autonomía y un financiamiento estable.

Cabe recodar que la desfinanciación a la que el Estado tiene sometida a la Universidad nacional ha generado que **la planta de profesores de Nacional haya descendido en un 11%**, es por ello que el médico, expresa que “se va a hacer todo lo posible para que se logre un nuevo trato para la universidad en términos financieros".

Por su parte, la representante estudiantil,  afirma que **el próximo 25 de marzo, los estudiantes van a  exigirle al Consejo que acoja los resultados de la consulta,** como ya se ha hecho en otras universidades. Además, seguirán movilizándose como "forma de reivindicar su derecho a decidir sobre el rumbo de la universidad", expresa María Alejandra Rojas.

Finalmente, cabe destacar que el apoyo que ha recibido el profesor Hernández no ha sido únicamente de la comunidad universitaria, sino también de actores políticos del país, como se refleja en las reacciones de diferentes personas a través de twitter tan pronto se conocieron los resultados de la votación.

[![tweets](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/03/tweets.jpg){.aligncenter .wp-image-6213 .size-full width="610" height="525"}](https://archivo.contagioradio.com/actualidad/universidad-nacional-elige-a-mario-hernandez-como-rector-y-el-csu/attachment/tweets/)
