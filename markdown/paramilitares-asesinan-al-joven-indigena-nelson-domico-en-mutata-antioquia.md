Title: Paramilitares asesinan al joven indígena Nelson Domicó en Mutatá, Antioquia
Date: 2019-07-12 08:38
Author: CtgAdm
Category: Comunidad, Nacional
Tags: AGC, Asesinato de indígenas, Jiguamiandó
Slug: paramilitares-asesinan-al-joven-indigena-nelson-domico-en-mutata-antioquia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/lider-social.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

El pasado 6 de julio en horas de la tarde, en la vereda Villarteaga, vía principal que comunica a **Medellín con Urabá y Turbo**,  paramilitares de las autodenominadas Autodefensas Gaitanistas de Colombia (AGC) asesinaron el joven indígena **Nelson Domicó, integrante de la comunidad** Embera Eyábida del resguardo de Uradá,

Según uno de los líderes indígenas del Carmen del Darién, es común que los paramilitares acusen arbirtrariamente a los jóvenes integrantes de las comunidades indígenas de ser parte del ELN o de ser sus informantes, uno de los posibles móvulez por los que Nelson habría sido asesinado a manos de alias **“El Diablo” y “Milton”**, miembros de las AGC señalados de ser los autores materiales. [(Lea también: 35 indígenas han sido asesinados durante Gobierno Duque: ONIC(](https://archivo.contagioradio.com/asesinados-gobierno-duque/)

El joven Embera, quien habría sido amarrado, golpeado y posteriormente degollado, ejercía labores de agricultura y pesca al interior de su resguardo, era a su vez sobrino de, **Ilario Domicó, líder indígena** y médico tradicional de la comunidad de Alto Guayabal, quien fue víctima de una incursión paramilitar el pasado miércoles 29 de mayo y de la que salió herido, este lazo familiar podría ser otro de los móviles para acabar con la vida del joven de 20 años.

Frente al estado del Ilario, el dirigente señaló que este se encuentra por fuera de Jiguamiandó y no ha podido regresar al resguardo pues continúa siendo foco de amenazas. [(Le puede interesar: CRIC no permitirá que asesinato del comunero Deiner Yunda quede en la impunidad)](https://archivo.contagioradio.com/cric-exige-esclarecimiento-del-asesinato-del-comunero-deiner-yuda/)

Agregó además, que el control paramilitar en el Urabá chocoano y antioqueño se ha extendido a lo largo del río Jiguamiandó, tanto en Mutatá como en los corregimientos de **Llano Rico, Pavarandó, Belén de Bajirá y el resguardo Indígena de Urada**, por lo que líderes, integrantes de la comunidad indígena y autoridades locales permanecen bajo amenaza sin poder hacer las respectivas denuncias.

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

<iframe id="audio_38331272" frameborder="0" allowfullscreen scrolling="no" height="200" style="border:1px solid #EEE; box-sizing:border-box; width:100%;" src="https://co.ivoox.com/es/player_ej_38331272_4_1.html?c1=ff6600"></iframe>  

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
