Title: Conozca la selección de la 17 Muestra Internacional Documental de Bogotá
Date: 2015-09-07 10:24
Author: AdminContagio
Category: 24 Cuadros
Tags: 17 MIDBO, Alados Colombia, Cinemateca Distrital de Bogotá, IDARTES, Ministerio de Cultura, Muestra Internacional Documental Bogotá, Proimágenes Colombia
Slug: conozca-la-seleccion-de-la-17-muestra-internacional-documental-de-bogota
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/06/11377391_561418677331147_8985045285743070219_n.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### 7 Ago 2015

La organización encargada de la 17 versión de La muestra Internacional de Bogotá MIDBO, reveló la lista de producciones que integrarán la programación oficial del evento que tendrá lugar entre el 27 de Octubre y el 7 de Noviembre en la capital colombiana. (Ver [MIDBO 17 años de documental en Bogotá](https://archivo.contagioradio.com/midbo-2015-17-anos-de-documental-en-bogota/)).

La elección de los trabajos participantes, recibidos desde el mes de junio, estuvo a cargo de un comité de curadores conformado por expertos academicos de amplia trayectoria en el "Cine de lo real", para cada una de las categorías: Convocatoria Nacional, Convocatoria Internacional, Convocatoria Nacional Estudiantil y para ponenetes en el seminario "Pensar en lo Real".

En la categoría **Documental Nacional**, fue primordial según los curadores "...pensar en los equilibrios que hay que construir entre el cine de hoy y el de ayer: qué películas dialogan y cómo lo hacen con la propia tradición del cine colombiano..." Para esta sección los documentales elegidos harán parte de las categorías: Competencia Nacional y Panorama.

La triada de selección para la categoría **Documental Nacional** estuvo integrada por:

Josetxo Cerdán: (España). Pedagogo, realizador e investigador con amplia trayectoria como curador de diversos festivales internacionales de cine.  
Diego Rojas:(Colombia). Crítico, pedagogo, investigador y coordinador de proyectos de apreciación cinematográfica.  
Ángela Rubiano: (Colombia). Documentalista, fotógrafa y productora. Co-fundadora del colectivo Achiote Cocina Audiovisual.

**Selección Documental Nacional:**

Competencia Nacional  
En Relieve - Marta Hincapié Uribe  
En Largos Días - Adrián Villa Dávila  
Los Habitantes de la Casa del Diablo - Iván Reina Ortiz  
Matachindé - Víctor Palacios  
Mónica - Liliana Sayuri Matsuyama Hoyos  
Garras de Oro, herida abierta a un continente - Ramiro Arbeláez y Óscar Campo  
Clarita - Natalia Rodríguez Forero  
Aislados - Marcela Lizcano

**Categoría Panorama**  
La Siberia - Gerrit Stollbrock e Iván Sierra  
Itaguí - Roberto Niño Betancourt  
Qué Significa Ser Hafu - Nicolás Guarín  
Alumbrando Caminos - Paola Figueroa-Cancino  
Bogotá, Bacatá, Yo Que Sé... - Juan Andrés Rodríguez  
Con los Pies en la Tierra - Talía Osorio Cardona  
Trípido - Mónica Moya  
Distancia - Frank Rodríguez  
Ati - Claudia Fischer  
Sin Título - Julio Lamaña y Ricardo Perea  
Cantadoras: Memorias de Vida y Muerte en Colombia - María Fernanda Carrillo  
No Me Interesa Morir - Clara Rubiano  
Pregón de Manglar - Eugenio Gómez Borrero  
Retornan - Sebastián Mejía  
Duganyina Mamonun (Los Niños y el Mamo) - Wilfred Nieves  
Clandestinos - Diego García Moreno

En la categoría de **Documental Internaciona**l, fueron seleccionados para este año 18 títulos provenientes de 12 países: Alemania, Australia, Bélgica, Bolivia, Brasil, España, Francia, Italia, México, Portugal, Sudáfrica y Reino Unido, dichos títulos, según sus seleccionadores serán los encargados de "...resaltar la multiplicidad de lenguajes, además de plasmar la diversidad temática y geográfica desde diversos puntos de vista narrativos alrededor del mundo..."

El comité de selección para la categoría de **Documental Internacional** estuvo a cargo de:

Diego García Moreno: (Colombia). Director y productor de cine y video documental.  
Ignacio Prieto: (Colombia). Antropólogo, documentalista, experto en antropología visual, investigación de campo y trabajo con comunidades.  
Pablo Mora: (Colombia). Documentalista y antropólogo, director de MIDBO.

**Selección Documental Internacional:**  
A Place For Everyone - Angelos Rallis y Hans Ulrich Gößl  
Komian - Jordi Esteva  
11 Minutos y 49 Segundos de Tiempo en Espera - Maurits Boettger  
Hotel Nueva Isla - Irene Gutiérrez  
Nini - Gigi Giustiniani  
Hermanos - Daniel Touati  
612 Palabras en Alambre - Cecilia Gonzalez Rufo  
Movimientos Espectaculares - Mateo Hinojosa  
Anochecer en Gaia - Juan Francisco Salazar  
Tres Mujeres - Alexis Delgado Burdalo  
Daphné o el Bello Espécimen - Sébastien Laudenbach, y Sylvain Derosne  
Los Príncipes del Yen - Michael Oswald  
Gigantesco Ima - Petronio y Tiago Scorza  
Puntos Suspensivos - Lilian Paola Ovalle y Alonso Díaz Tovar  
The Man Who Mends Women - The Wrath of Hippocrates - Thierry Michel  
El Último Abrazo - Sergi Pitarch Garrido  
Revolución Industrial - Tiago Hespanha y Frederico Lobo  
A la Medianoche - Ángel González Katarain

Por primera vez en sus 17 años de historia MIDBO celebra la creación de su categoría **Documental Estudiantil**, que se desarrolla bajo el título de "Miradas Emergentes". Para esta convocatoria se presentaron obras de diferentes rincones de Colombia: Santa Marta, Bucaramanga, Cali, Medellín, Popayán y Bogotá, además se destaca la participación de obras provenientes de Ecuador y México. Las realizaciones emergentes elegidas, plasmarán desde la mirada de diversos autores sus voces con respecto a preocupaciones sociales y ambientales, búsquedas familiares e identitarias que llaman la atención por su diversidad temática y formal.

La selección para la categoría **Documental Estudianti**l estuvo a cargo de:

Jacobo Del Castillo: (Colombia). Historiador e investigador en el campo de cine documental y las ciencias sociales.  
Santiago Romero: (Colombia). Director, montajista y cineclubista.

**Selección Documental Estudiantil:**  
El Púrpura no es Para mi Lienzo - Cindy Castro  
Shibari - María Clara Serna  
Natural- Carolina Sánchez  
Amor - Una Pardo  
Nat Alie - María Camila Lozano  
Nuestras Semillas - Nicolás Muñoz  
Dulces Memorias - Rolando Angarita  
Pueblo Macizo - Pablo Mejía  
El Hereje - Nicolás Muñoz  
Haiku - Diego Alexander Pinilla  
Paralela - Heider Vargas  
Noviembre - Lyndi Márquez  
Clandestino - Santiago Loaiza  
Marina - Nicolás Mejía  
Volví Para Quedarme - Angélica Meza  
El Cristo de los Nadies - Miguel Romero  
Están Sonando las Campanas - John Rojas  
El Valle de las Tristezas - Camilo Villamizar Plazas  
Devenir - Yefersson Albeiro Henao  
La Ruta de Julita - Omar Eduardo Ospina

**Sobre la muestra:**

La muestra Internacional de Bogotá MIDBO es organizada por la Corporación Colombiana de Documentalistas: Alados Colombia, en asocio con el Instituto de las Artes, IDARTES - Cinemateca Distrital, la Alcaldía Mayor de Bogotá, el Ministerio de Cultura (Dirección de Comunicaciones), y Proimágenes Colombia.

Durante los 9 días de la MIDBO 2015 el público podrá disfrutar de más de 60 películas documentales nacionales e internacionales, que se exhibirán en diversos espacios de la ciudad para confirmar una vez más porque Bogotá también es documental.

MIDBO 2015, es posible gracias al apoyo del Instituto Distrital de las Artes, IDARTES, la Alcaldía Mayor de Bogotá, con el Ministerio de Cultura (Dirección de Comunicaciones), Proimágenes Colombia y La Universidad Central.

(Con información de Comunicaciones MIDBO 2015)
