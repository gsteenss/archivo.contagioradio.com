Title: "Hay un disfraz del desmonte del paramilitarismo": Mesa Nacional de Víctimas
Date: 2017-07-11 12:25
Category: DDHH, Entrevistas
Tags: Amenazas, Mesa Nacional de Víctimas, paramilitares, víctimas
Slug: victimas-de-la-mesa-nacional-reclaman-proteccion-ante-amenazas-contra-su-vida
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/07/Captura-de-pantalla-2017-07-11-a-las-12.16.56-p.m.-e1499793834908.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Google] 

###### [11 Jul 2017] 

La declaración la hizo la señora Virgelina Chará, integrante de la Mesa Nacional de Víctimas en una rueda de prensa en la que estaba presente el Ministro del Interior Gullermo Rivera. Allí las **víctimas denunciaron amenazas por parte de grupos paramilitares** contra la integridad y la vida de las personas y familiares miembros de dicho espacio.

Ellas y ellos manifestaron que **necesitan que se creen mecanismo de seguimiento y diseño de políticas públicas** que garanticen sus derechos a la vez que se establezcan garantías de seguridad en los territorios. De igual forma, reconocieron los esfuerzos que está haciendo el Gobierno Nacional por acompañar y reconocer a las víctimas como personas que se encuentran en riesgo y requieren mecanismos óptimos de protección. (Le puede interesar: ["Nuevas amenazas contra líderes de víctimas y defensores de la mujer")](https://archivo.contagioradio.com/amenazas-lideres-sociales/)

Según Virgelina Chará, víctima y miembro de la Asociación Asomujer y Trabajo, **“las víctimas estamos siendo amenazadas porque estamos contando la verdad,** es vergonzoso que digan que el conflicto se terminó y los más perjudicados sigamos siendo amenazados”. Adicionalmente, ella manifestó que “el gobierno no ha tenido la capacidad de responder y atender a las necesidades de las víctimas por lo que exigimos que se creen mecanismos fuertes de protección”.

Virgelina Chará fue enfática en manifestar que **“los que nos están amenazando son personas del grupo de las Águilas Negras y las bandas criminales que son paramilitares”.** Por eso afirmó que para la sociedad colombiana es necesario que se termine el conflicto armado “que no se da únicamente con los acuerdos con las FARC”. Ella aseguró que en Colombia hay “un disfraz del desmonte del paramilitarismo y ya no debe haber más excusas para decir que estas bandas criminales no son paramilitares”. (Le puede interesar: ["Paramilitares amenazan a víctimas exiliadas en España"](https://archivo.contagioradio.com/amenazas-de-paramilitares-llegan-a-las-victimas-colombianas-exiliadas-en-espana/))

De igual forma, las víctimas de la Mesa Nacional establecieron que lo importante es que el Gobierno Nacional les dé **garantías políticas para poder recorrer el territorio libremente** y que reconozcan el trabajo que realizan las víctimas y los defensores de derechos humanos hacia la consecución de la paz. Así, Chará dijo que “por primera vez el Estado nos ha dado respaldo en temas de protección y esperamos que se materialicen en la práctica”.

Finalmente, el Ministro del Interior manifestó que el Gobierno Nacional ha puesto en funcionamiento un **cuerpo elite de la Policía para perseguir las estructuras criminales** principalmente el Buenaventura y Tumaco. Igualmente el Gobierno estableció un decreto para que la Fiscalía tenga como prioridad investigar y sancionar a las personas que están amenazando a los líderes de derechos humanos.

<iframe id="audio_19743909" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_19743909_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU).

###### 
