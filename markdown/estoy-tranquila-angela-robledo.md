Title: Estoy tranquila ante la denuncia por doble militancia en el CNE: Angela Robledo
Date: 2018-08-22 17:18
Category: Nacional, Política
Tags: Angela Maria Robledo, CNE, Colombia Humana, Gustavo Petro, oposición
Slug: estoy-tranquila-angela-robledo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/11/angela-robledo1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Nelson Cárdenas] 

###### [22 Ago 2018] 

El **Concejo Nacional Electoral (CNE)** recibió la denuncia del veedor Juan Carlos Calderón España sobre una presunta doble militancia en la que habría incurrido la hoy representante a la cámara **Angela María Robledo;** Ante esta situación, la congresista asegura sentirse tranquila, pues su decisión de integrar la campaña de la Colombia Humana fue tomada con base en sólidos argumentos jurídicos y políticos.

Para la Representante a la Cámara, está demanda no va a progresar porque cuando optó por ser la formula vice presidencial de Gustavo Petro, se asesoró con un grupo de abogados conocedores del tema, quienes le aseguraron que no había lugar a la doble militancia, pues según los términos de la ley 1475 de 2011, **Robledo no estaba aspirando a ningún cargo de elección popular.**

Adicionalmente, porque su curul en el Congreso la obtuvo en condición de haber sido formula vice presidencial de la Colombia Humana, y **en razón del Estatuto de Oposición,** que otorga una curul a Senado y Cámara a quienes obtienen la segunda mayor votación en las elecciones presidenciales. (Le puede interesar: ["En una democracia consolidada no se necesitaría estatuto de oposición: Jairo Estrada"](https://archivo.contagioradio.com/39264/))

Sobre su demandante, Robledo aseguró que es una persona que presenta constantemente este tipo de casos ante la justicia, sin embargo, afirmó que él esta en todo su derecho de hacerlo al igual que ella de defenderse. La Representante a la Cámara afirmó que ya está preparando sus contra argumentos para presentarse ante el **CNE,** y sostuvo que estas demandas son "gajes del ejercicio de la política".

### **Hoy la bancada por la vida, la esperanza y la paz se declaró en oposición**

De la bancada por la paz hacen parte congresistas de la Lista por la Decencia, el Partido Verde, el Polo e integrantes de la Fuerza Alternativa Revolucionaria del Común, quienes según Robledo, han venido trabajando en el **ejercicio de una oposición colectiva,** reflexiva y que desde el legislativo promuevan iniciativas incluidas en el programa de la Colombia Humana "por el que votaron más de 8 millones de colombianos y colombianas". (Le puede interesar: ["Los retos a los que se enfrenta la bancada de oposición"](https://archivo.contagioradio.com/los-retos-a-los-que-se-enfrenta-la-bancada-de-oposicion/))

Con esta declaración, los partidos y colectividades que se sumen a la oposición gozarán de garantías para el ejercicio de la misma como una **financiación adicional a la que reciben los partidos del 5%,** acceso a los medios de comunicación social del Estado que hacen uso del espectro electromagnético, acceso a información y documentación oficial, **derecho a réplica frente a funcionarios del Estado** y participación en las mesas directivas de plenarias de las corporaciones públicas que se eligen mediante votación popular.

<iframe id="audio_28031756" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_28031756_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en] [su correo](http://bit.ly/1nvAO4u) [o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por ][Contagio Radio](http://bit.ly/1ICYhVU) 

<div class="osd-sms-wrapper">

</div>
