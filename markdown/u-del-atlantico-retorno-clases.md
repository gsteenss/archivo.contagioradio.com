Title: Estudiantes de U. del Atlántico exigen garantías para el retorno a clases
Date: 2019-01-14 18:39
Author: AdminContagio
Category: Educación, Nacional
Tags: asamblea, estudiantes, paro, Universidad del Atlántico
Slug: u-del-atlantico-retorno-clases
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/01/Captura-de-pantalla-2019-01-14-a-las-6.12.43-p.m.-770x400.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Unees- Atlántico] 

###### [14 Ene 2019] 

El pasado viernes, estudiantes de la Universidad del Atlántico se reunieron en una asamblea multiestamentaria para definir la ruta de movilización durante este año; adicionalmente se definió que el retorno a clases se realizaría el lunes 21 de enero, únicamente si existen garantías académicas y de seguridad para este fin. Adicionalmente, durante la reunión cuatro facultades propusieron continuar la jornada de protesta mediante la asamblea permanente, dos facultades votaron por continuar en paro mientras otras cuatro prefirieron restablecer las actividades académicas.

Ante la necesidad percibida por estudiantes, la Asamblea propuso que el retorno a actividades se produjera este lunes 21 de enero, con garantías académicas como de seguridad para los estudiantes. De esta forma, los jóvenes solicitan que se revise el cronograma académico que contemplaba 25 días de clase para lograr el progreso académico y posterior evaluación de 2 cortes; de otra parte, incluiría la seguridad física de los estudiantes que participan en las jornadas de protesta.

### **Estudiantes denunciaron agresiones por parte de externos a la Universidad** 

El mismo viernes que se realizó la asamblea citada por estudiantes, los directivos de la Universidad citaron una reunión con el mismo carácter de la estudiantil pero en horas de la mañana, según miembros de la comunidad universitaria, en dicho encuentro solo participaron administrativos de la Institución, cuya definición incluía la vuelta a clases este lunes 14 de enero.  (Le puede interesar: ["Unees denuncia persecución y amenaza desde Fuerza Pública a estudiantes"](https://archivo.contagioradio.com/persecucion-fuerza-publica-estudiantes/))

> [@UneesCol](https://twitter.com/UneesCol?ref_src=twsrc%5Etfw) denuncia ingreso de personas externas al campus universitario de la Universidad del Atlántico; los estudiantes sufrieron empadronamientos, golpes e insultos por parte de estas personas, que ingresaron a la institución con complicidad de vigilantes de la Universidad. [pic.twitter.com/Sbj4yGFmrR](https://t.co/Sbj4yGFmrR)
>
> — Contagio Radio (@Contagioradio1) [12 de enero de 2019](https://twitter.com/Contagioradio1/status/1083950185607299072?ref_src=twsrc%5Etfw)

Este choque entre estudiantes y administrativos se agravó el mismo día en razón de acusaciones hechas por parte de los administrativos, quienes acusaron a los líderes estudiantiles citantes de la asamblea, de estar relacionados con los disturbios que se presentaron en horas de la tarde del viernes entre encapuchados y agentes del ESMAD. (Le puede interesar: ["Gobierno y estudiantes logran acuerdo en mesa de negociación"](https://archivo.contagioradio.com/estudiantes-gobierno-acuerdo/))

Adicionalmente, los estudiantes denunciaron que después de la salida de los administrativos del campus universitario, un grupo de pandilleros de una barrio cercano ingresaron al recinto y amedrentaron a los jóvenes que aún se encuentran en campamento universitario. La situación culminó con 2 estudiantes heridos levemente, amenazas directas contra los campistas y algunos computadores perdidos; situación que se sumó al daño de las garitas de los guardas de seguridad de la Institución que, según denuncias de los mismos estudiantes, se retiraron de sus puestos antes que ocurriera el ataque.

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/post.php?href=https%3A%2F%2Fwww.facebook.com%2FUneesatlantico%2Fposts%2F799625057041111&amp;width=500" width="500" height="843" frameborder="0" scrolling="no"></iframe>

La decisión final sobre lo que ocurrirá en la Universidad del Atlántico será tomada por un Consejo Académico ampliado que sesionará el próximo miércoles, en el que se podría decidir el regreso a clases, así como el levantamiento del campamento universitario. Otras Instituciones de Educación Superior (IES) públicas del país también se encuentran realizando asambleas, y preparando la primer movilización de 2019 que será el próximo 17 de enero.

> El 2019 arranca y los/as estudiantes nos seguimos encontrando en asambleas que se han programado en nuestras IES.
>
> Fotografías de la Asamblea en la Universidad Tecnológica de Pereira que se realiza en estos momentos. [\#VenYTeUNEES](https://twitter.com/hashtag/VenYTeUNEES?src=hash&ref_src=twsrc%5Etfw) [pic.twitter.com/LEw0XMLqOl](https://t.co/LEw0XMLqOl)
>
> — UNEES Colombia (@UneesCol) [14 de enero de 2019](https://twitter.com/UneesCol/status/1084910695429361666?ref_src=twsrc%5Etfw)

<p>
<script src="https://platform.twitter.com/widgets.js" async charset="utf-8"></script>
</p>
###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
