Title: A debate de control político la atención a víctimas en Colombia
Date: 2016-05-18 12:45
Category: DDHH, Nacional
Tags: atención a víctimas, crisis salud colombia, pavsivi, Unidad de Víctimas
Slug: a-debate-de-control-politico-la-atencion-a-victimas-en-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/05/Unidad-de-víctimas.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Timana ] 

###### [18 Mayo 2016 ]

La paz estable y duradera no será posible sin la transformación del dolor de las víctimas, afirma la psicóloga Dora Lucia Lancheros, integrante de la Corporación AVRE, a propósito del **debate de control político sobre la atención psicosocial y en salud integral a víctimas**, que se lleva a cabo este miércoles en el Congreso, y en el que se discutirá sí los programas del Ministerio de Salud y la Unidad de Víctimas destinados para atender a las víctimas, permiten la reconstrucción de su tejido social.

De acuerdo con la psicóloga, la atención estatal a las víctimas resulta ineficaz, porque **no existe como política pública, se brinda a través de programas** **desarticulados**, que se limitan por los tiempos presupuestales del Estado y las prácticas clientelistas de las entidades que prestan los servicios de salud; en suma, es una atención que termina revictimizando, teniendo en cuenta las dificultades a las que se enfrentan las víctimas para la garantía de sus derechos.

La respuesta del Estado a la atención integral a las víctimas, ha consistido en una serie de estrategias y guías desarticuladas de la Unidad de Víctimas y del Programa PAPSIVI, **con deficiencias en cobertura y calidad, que las han hecho incapaces de reparar individual y colectivamente**, entre otras, por la [[crisis que afronta el sistema de salud](https://archivo.contagioradio.com/balance-de-la-salud-en-colombia-continuo-enferma-durante-el-2015/)] actualmente en Colombia, asegura Lancheros.

Para garantizar que la atención psicosocial y en salud repare a las víctimas y reconstruya sus tejidos sociales, ésta debe tener un enfoque de derechos, asevera la psicóloga e insiste en que **los programas deben responder a las necesidades emocionales** y territoriales, sí lo que se busca es reconstruir la memoria histórica en sentido individual y colectivo.

"Desde la Mesa Psicosocial, creemos que el debate planteará **posibilidades para que la reparación logre avances importantes para las víctimas**", expresa Lancheros y agrega que es necesario contar con un sistema de información que dé cuenta de los impactos de los programas, así como hacer una evaluación pública del PAVSIVI y de las estrategias de la Unidad de Víctimas, incorporar el enfoque diferencial en la atención y el acompañamiento a víctimas y redefinir la estructura orgánica del Ministerio de Salud.

<iframe src="http://co.ivoox.com/es/player_ej_11576907_2_1.html?data=kpaimZuddJihhpywj5aYaZS1k5qah5yncZOhhpywj5WRaZi3jpWah5yncaXj08aYrsbSp8nZ09Tgj5Cns9Pk0NfOxc6Jh5SZo5jbjaa6lqahhpywj6jTstXVyM7cjbfFqMrjjJKSmaiReA%3D%3D&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU) ]] 

 
