Title: De dobles raseros, capturas y rebeldías
Date: 2016-03-03 10:55
Category: Javier Ruiz, Opinion
Tags: Andrés Pastrana, Centro Democrático, Santiago uribe
Slug: de-dobles-raseros-capturas-y-rebeldias
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/03/IMAGEN-COLUMNA.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Edición imágenes: Contagio Radio 

#### **[Javier Ruiz](https://archivo.contagioradio.com/javier-ruiz/)- [@contragodarria](https://twitter.com/ContraGodarria)** 

###### [3 Mar 2016] 

La extrema derecha está pasando un momento crítico en donde el desespero, los errores, la falta de razón y de sentido común son notorios. A esa extrema derecha no le ha ido bien en su propósito de torpedear la paz y por eso, al momento de criticarla o de sabotearla, les va mal y cometen muchos errores. Además el apoyo a sus posturas ya es mínimo así ésta misma extrema derecha diga sin pena alguna, que siguen teniendo “millones de simpatizantes”.

El doble rasero ha sido una característica inherente del uribismo al momento de criticar las negociaciones de paz. Se ha mencionado que cuando Uribe era presidente intentó darle beneficios a las FARC para que dejaran las armas y lograran negociar la paz que ahora tanto odia. Pero lo más interesante es que ahora otro expresidente, Andrés Pastrana, diga que el gobierno actual entregó el país a las FARC para que hicieran de las suyas insinuando que se perdió la soberanía y la institucionalidad por 72 horas.

Al expresidente Pastrana se le olvida que fue en su mandato que entregó prácticamente medio país a las FARC y que, en su fracasado intento de lograr la paz, la violencia se incrementó mucho, degradando el conflicto armado interno y por su intransigencia, con el Plan Colombia, reforzó la intransigencia de las FARC que aprovecharon eso para fortalecerse militarmente y cometer delitos en la zona de despeje. Por ende, el expresidente Pastrana no es el indicado para criticar el proceso de paz en La Habana porque son notorios sus sesgos que son afines a las posturas uribistas y ha pasado a ser uno de los saboteadores de la paz. El expresidente Pastrana, cuyo gobierno fue nefasto y su ineptitud muy grande, comete un error en ponerle trabas a la paz y ese papel de mercenario contra la paz y contra Venezuela, bajo el pretexto de defender la democracia, no le luce.

Ahora con la captura de Santiago Uribe, hermano de Alvaro Uribe, por paramilitarismo y por ser un miembro fundador del grupo paramilitar conocido como los “12 apóstoles” el uribismo busca excusas para desvirtuar las graves acusaciones contra el hermano menor de los Uribe y en su desespero errático, que raya en lo absurdo y en lo conspirativo, dicen que su captura fue por culpa de las FARC y de una “persecución política”. Debo decir que al uribismo no le luce estar utilizando el léxico o el vocabulario de la izquierda porque no es una persecución política sino una persecución a unos delincuentes comunes. En ningún momento el uribismo ha sido victima de “violencia política”, como ellos justificaron la violencia política hacia la UP, por ejemplo, quienes fueron perseguidos, exiliados y exterminados por su postura política de volver a Colombia un país más justo y democrático.

Pero lo cómico del asunto es que el uribismo ahora se declaró un grupo rebelde y con protestas, a las que no asisten más de 6 personas, buscan el apoyo de un país a unos criminales que están detenidos y otros prófugos de la justicia. El uribismo no sabe nada de rebeldía y cuando eran gobierno perseguían a los “rebeldes” y sin pruebas los acusaban de ser “terroristas vestidos de civil”.

La rebeldía del uribismo se quedó en sus cuentas de twitter creyendo que con un teclado amenazan e intimidan personas y ni que decir de su doble rasero porque exigen paz sin impunidad pero cuando en su grupo están los criminales exigen que los dejen en paz y que los dejen impunes como lo van a intentar con el “apóstol Santiago”.
