Title: Gobierno aproveche la oportunidad salvavidas para pacientes de hepatitis C
Date: 2019-11-12 13:49
Author: Mision Salud
Category: Mision Salud, Opinion
Tags: hepatitis C
Slug: gobierno-aproveche-la-oportunidad-salvavidas-para-pacientes-de-hepatitis-c
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/HepatitisC.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

Prácticamente todas las personas que sufren de hepatitis C podrán curarse y salvar sus vidas si el Gobierno declara de interés público el tratamiento. Le explicamos por qué:

Recientemente se hizo pública la información sobre el precio de las versiones genéricas precalificadas por la Organización Mundial de la Salud (OMS) de los Antivirales de Acción Directa (AAD) para el tratamiento de la hepatitis C, [a través del Fondo Estratégico de la Organización Panamericana de la](http://www.mision-salud.org/wp-content/uploads/2019/10/sf-lta-hcv-aug12019-31-dic-2020.pdf) Salud (OPS).

Según dicha publicación, actualmente el Fondo Estratégico permite la adquisición de estos medicamentos (sofosbuvir y daclatasvir) por un precio de **\$438.150 (US\$127) el tratamiento de 12 semanas. **

Este precio contrasta con el que viene pagando Colombia por el tratamiento a través del mismo Fondo Estratégico (por el mecanismo de “compra centralizada”), que es de \$13'972.500 (US\$4.050), lo que implica **una diferencia de cerca del 97% y un ahorro de más de 13 millones de pesos por cada tratamiento**. Esta es una noticia esperanzadora para las más de 300.000 personas con hepatitis C en Colombia. Sin embargo, nuestro país todavía no puede aprovechar esta oportunidad.  ¿Por qué?

Porque de acuerdo con la misma publicación de la OPS, los precios publicados de *"no están disponibles para algunos países debido a restricciones de patentes o licencias" y* uno de esos países es Colombia.

###### Tomado de documento "Directing action antiviral medicines. Long term agreement valid from august 1, 2019 until december 31, 2020." OPS 2019. 

**¿Qué debe hacer Colombia para acceder a estos precios salvavidas?**

Teniendo en cuenta que los AAD a los que hace referencia la publicación citada se encuentran patentados en Colombia, en nuestra opinión lo que debe hacer el Gobierno colombiano es **declararlos de interés público para poder comprar los genéricos** a través del Fondo Estratégico.

De hacerlo, el Gobierno podría comprar los tratamientos no por \$13.9 millones cada uno, como ocurre hoy, sino por \$438.000, lo que significa un ahorro de cerca del 97% en cada tratamiento. Como actualmente Colombia necesita cerca de 323.000 nuevos tratamientos, si se comparan los genéricos certificados por la OPS se obtendrá un ahorro de más de \$4,3 billones y se salvarían miles de vidas.

**Mientras tanto la hepatitis C en Colombia...**

La hepatitis es una inflamación del hígado que puede llegar a dañarlo y comprometer la vida de la persona. Existen diferentes tipos de hepatitis y una de ellas es la hepatitis C, causada por el virus de la hepatitis C (VHC). Según datos del Ministerio de Salud en Colombia existen cerca de 325.000 personas que pueden estar infectadas con este virus y de ellas solamente 1.692 conocen su diagnóstico y han recibido tratamiento.

No existe vacuna contra el VHC pero en la última década se ha descubierto y aprobado un grupo de medicamentos denominados “Antivirales de Acción Directa” (AAD)[\[1\]](https://docs.google.com/document/d/1qf7bxt9nYQbtwax2z5_9PQ-BKoGMmhUPXC7Yd_oKTx0/edit#heading=h.1fob9te) que logran curar la infección causada por el VHC con tasas hasta del 97% de curación (dependiendo de la cepa del virus que cause la infección)(2). El problema está en su altísimo precio. En Colombia este tratamiento de 12 semanas ha llegado a costar más de \$120 millones de pesos por paciente (US\$30.000).

Frente a esta situación, [en 2015 la Fundación IFARMA solicitó al Ministerio de Salud declarar de interés público los AAD necesarios para tratar y curar la hepatitis C](http://www.mision-salud.org/wp-content/uploads/2019/10/fundacion-ifarma-hepatitis.pdf). Misión Salud y otros actores coadyuvamos esta solicitud.

  -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  [Conoce el memorial de coadyuvancia de Misión Salud en este enlace.](http://www.mision-salud.org/actualidad/hepatitis-c-coadyuvamos-el-proceso-de-solicitud-de-declaracion-de-interes-publico/)
  -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

Si bien el procedimiento respectivo debería tomar solo unos meses, presiones de los laboratorios multinacionales afectados y sus gobiernos, han hecho que se extienda atípicamente. Ya han pasado 4 años desde la solicitud y apenas en octubre de 2019 se reunió el Comité Técnico que debe analizar la solicitud y emitir una recomendación sobre la declaración al Ministro de Salud, a quien corresponde tomar la decisión final. El Comité está integrado por representantes del Ministerio de Comercio, Planeación Nacional y el propio Ministerio de Salud.

A raíz de esta solicitud el Ministerio de Salud tomó la decisión de recurrir al mecanismo de la “compra centralizada” a través del Fondo Estratégico de la OPS, lo que permitió reducir el precio de cada tratamiento/paciente de más de COP\$120 millones a cerca de COP\$24 millones y comprar 2246 tratamientos.

No obstante, la inmensa mayoría de las víctimas de esta enfermedad siguen sin tratamiento.

Conocedores del patriotismo, el sentido de responsabilidad y la sensibilidad social del Ministro de Salud, y su grupo de colaboradores, abrigamos la esperanza de que pronto encontrarán el camino para llegar a los ADD genéricos precalificados por la OMS y ofrecidos por la OPS a los países que no tengan restricciones de patentes o las levanten, **como corresponde a la obligación estatal de proteger, respetar y cumplir el derecho fundamental a la salud, el primero y más importante derecho de los colombianos.**

Les invitamos a conocer la [declaración del Comité de Veeduría y Cooperación en Salud sobre este anuncio](http://www.mision-salud.org/wp-content/uploads/2019/10/CVCS-Dec_Precios-AAD-OPS.pdf%22%20title=%22Declaraci%C3%B3n%20CVCS%20sobre%20DIP%20Hepatitos%20C%20-%20Colombia) de la Organización Panamericana de la Salud.

**Cubrimiento en medios**

<https://www.elespectador.com/noticias/salud/vuelve-la-discusion-por-el-alto-precio-de-los-medicamentos-contra-la-hepatitis-c-articulo-886156>

<https://lasillavacia.com/los-medicamentos-hepatitis-c-ponen-contra-las-cuerdas-al-ministerio-salud-73865>

<https://unperiodico.unal.edu.co/pages/detail/hepatitis-c-una-oportunidad-para-resolver-un-problema-de-salud-publica-en-colombia/>

**Notas**

[\[1\]](https://docs.google.com/document/d/1qf7bxt9nYQbtwax2z5_9PQ-BKoGMmhUPXC7Yd_oKTx0/edit#heading=h.30j0zll) Los AAD son tratamientos medicamentosos para la hepatitis C relativamente nuevos pero muy costosos, los resultados han indicado que parecen erradicar el VHC de la sangre con mayor frecuencia que los tratamientos antiguos basados en interferón y con menores efectos adversos graves (1).

**Referencias**

1.  Jacobsen J, Nielsen E, Feinberg J, Katakam KK, Fobian K, Hauser G, et al. [Antivirales de acción directa para la hepatitis C crónica. Cochrane](https://www.cochrane.org/es/CD012143/antivirales-de-accion-directa-para-la-hepatitis-c-cronica) \[Internet\]. el 18 de septiembre de 2017 \[citado el 1 de noviembre de 2019\]; Disponible en: <https://www.cochrane.org/es/CD012143/antivirales-de-accion-directa-para-la-hepatitis-c-cronica>
2.  Datapharm. Sovaldi 400 mg film coated tablets - Summary of Product Characteristics (SmPC) - (emc). En: Datapharm | EMC \[Internet\]. 2019 \[citado el 1 de noviembre de 2019\]. Disponible en: <https://www.medicines.org.uk/emc/medicine/28539>

