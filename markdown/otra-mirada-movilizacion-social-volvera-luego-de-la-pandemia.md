Title: Otra Mirada: Movilización social volverá con el fin de la cuarentena
Date: 2020-08-26 15:52
Author: AdminContagio
Category: Otra Mirada, Programas
Tags: Defendamos la Paz, Movilización estudiantil, Movilización social, UNEES
Slug: otra-mirada-movilizacion-social-volvera-luego-de-la-pandemia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/74802282_1249874088532329_945839746624520192_o.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: Andrés Zea/Contagio Radio

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El año 2019 estuvo marcado por diversas protestas y manifestaciones para hacer un llamado al gobierno en contra de reformas laborales y educativas y exigiendo la implementación del acuerdo de paz. Sin embargo en este año 2020, este ejercicio se vio imposibilitado desde que inició la pandemia.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Aún así, y ha raíz del desmedido asesinato de líderes sociales, las masacres y y el accionar de la fuerza pública en contra de las comunidades, diferentes sectores empiezan a llamar nuevamente a las calles. (Le puede interesar: [Marcha por la Dignidad aportó para reactivar la movilización social](https://archivo.contagioradio.com/marcha-por-la-dignidad-aporto-para-reactivar-la-movilizacion-social/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En este programa de Otra Mirada, sobre el regreso de las movilizaciones sociales, participaron Armando Caracas, coordinador nacional de la Guardia Cimarrona, Sarah Isabel Klinger integrante de la UNEES y estudiante de la Universidad del Cauca, Edgar Mojica, secretario general de la Central Unitaria de Trabajadores de Colombia y Patricia Salive, periodista e integrante de Defendamos la Paz.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Durante el conversatorio, los invitados reafirmaron que es necesario volver a salir a las calles y exigir repuestas y acciones por parte del Gobierno. Sin embargo, también indican que se pueden articular otras formas de movilización. (También le puede interesar: [«Si no salimos nos van a masacrar a todos… volveremos a las calles»](https://archivo.contagioradio.com/si-no-salimos-nos-van-a-masacrar-a-todos-volveremos-a-las-calles/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Asimismo, explican qué podemos aprender de las movilizaciones que se han realizado en otros países y cómo garantizar que futuras generaciones deseen salir a las calles sin miedo y asuman la voz del liderazgo. 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Finalmente, expresan que ante la indignación que han generado las respuesta del gobierno, desde los diferentes sectores se están organizando para poder reactivar las movilizaciones.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Si desea escuchar el programa del 21 de agosto:[Otra Mirada: Terror paramilitar de nuevo en Colombia](https://www.facebook.com/contagioradio/videos/3838335699517144)

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Si desea escuchar el análisis completo [Haga click aquí](https://www.facebook.com/contagioradio/videos/400188804279393)

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
