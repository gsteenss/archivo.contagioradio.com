Title: Ya son 2095 días de la huelga de SINTRADIT-Buga en contra de Cristar SAS
Date: 2017-08-18 09:25
Category: Columnistas invitados, Opinion
Tags: Buga, Huelga, SIndicatos, Trabajadores
Slug: 2095-dias-de-la-huelga
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/08/sintradit-buga.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Comité Solidaridad Cut Valle 

###### 17 Ago 2017 

#### [**Por José Antonio Gutiérrez D**] 

Aunque parezca increíble, por seis años, un grupo de obreros vallunos, específicamente de la ciudad de Buga, la ciudad señora del Milagroso, mantienen una huelga en las afueras de la empresa de vidrio **Cristar SAS** (filial de la multinacional norteamericana Owens Illinois Inc., que controla el 85% de la empresa). Ahí están en unas carpas, rodeados de carteles que atestiguan la solidaridad que han recibido de varios sindicatos del departamento. Se han mantenido firmes en esta huelga 35 obreros de los 60 que la iniciaron: las deserciones ocurrieron sólo en los primeros meses, una muestra de la voluntad de estos trabajadores (1). Desde entonces este grupo de 35 obreros se ha mantenido inquebrantable reclamando sus derechos. La conciencia de clase y el sentido solidario de estos obreros es admirable.

El origen de este conflicto parte de las condiciones en que los obreros de esta fábrica laboraron -muchos por varias décadas- a través de la figura de cooperativas de trabajo asociado y empresas subcontratistas de fachada. De 750 trabajadores que había en la fábrica, tan sólo 120 eran contratados directamente por la empresa, casi todos trabajadores administrativos, jefes de área e ingenieros. Cansados de las condiciones de trabajo en que se les vulneraban todos los derechos, con un sueldo bajísimo, sin prestaciones y con horas de trabajo muy por encima de lo legal, a la vez que conscientes de las medidas contra el trabajo asociado que se adelantaban en el legislativo colombiano gracias al debate suscitado por la huelga de los corteros de caña en el 2008, decidieron sindicalizarse, formando el “Sindicato de Trabajadores Disponibles y Temporales” (SINTRADIT). Es así como el el 24 de octubre del 2011, presentan un pliego de peticiones a la empresa. Ésta no solamente se negó a negociar y desconoció el derecho a sindicalización de los trabajadores, sino que desde el 22 de noviembre del mismo año negó la entrada a casi todos los miembros del sindicato a la empresa. Luego la empresa contrató directamente a la mayoría de los obreros, para evitar sanciones por malas prácticas: pero según los obreros en huelga, casi todos estos trabajadores han sido despedidos con el tiempo, habiendo un recambio muy alto. Los obreros huelguistas denuncian un auténtico apartheid en su contra, señalando que si alguno de los obreros de planta se acerca a hablar con ellos, inmediatamente se le despide.

Los obreros huelguistas no están pidiendo nada extravagante. Están pidiendo que se les cumplan tres exigencias totalmente básicas y constitucionales: contratación directa, libertad de asociación y libertad para negociar. Nada más, pero nada menos. Han golpeado, pidiendo estas tres cosas, todas las puertas de las instituciones colombianas, incluidas las cortes laborales y la inspección del trabajo, de las cuales no han recibido ninguna respuesta. Por eso han optado por buscar instancias internacionales como la OIT. Han sido visitados no sólo por múltiples delegaciones sindicales nacionales e internacionales, sino también por misiones de derechos humanos como la Caravana de Juristas. Todo esto los ha ayudado a mantener la moral, pero es necesario reforzar la solidaridad y las acciones de presión en contra de la empresa para que se solucione esta ignominia.

Dentro del marco de acciones solidarias con esta huelga, se organizó una charla el 8 de Agosto (día 2.084 de la huelga), en la cual discutimos sobre qué significa el proceso de paz para los obreros de la alicaída industria colombiana, así como para los trabajadores precarios que hoy pululan, en el contexto de destrucción sistemática de los sindicatos y el aniquilamiento de sus dirigentes que se ha vivido en los últimos 30 años. Las garantías para que el pueblo pueda hacer política sin temor a su integridad física también incluyen el respeto a los sindicatos, según varios asistentes expresaron. Como bien se sabe, los ataques sistemáticos al sindicalismo en Colombia sigue poniendo a este país en un verdadero récord a nivel mundial.

Hoy, que Colombia asiste a una explosividad social inmensa, es importante que todas las luchas se unan y busquen puntos básicos de convergencia. Nos decía un obrero, "aquí seguiremos, compañero, así nos toque estar 2.000 días más, porque nuestra lucha es justa. Si la abandonamos, le fallamos a todos los trabajadores de este país". Rodear a los obreros de SINTRADIT y acompañarlos en su titánica lucha, es un deber para todos aquellos a quienes se les arruga el alma ante tanta injusticia. No han pasado un día de esta huelga solos, pero necesitamos más, muchos más, para acompañarlos. Si ellos no nos abandonan, nosotros no los abandonemos a ellos.

###### (1) Sobre esta huelga, ver también un artículo previo [http://www.rebelion.org<wbr></wbr>/noticia.php?id=182980](http://www.rebelion.org/noticia.php?id=182980) 

------------------------------------------------------------------------

###### [Las opiniones expresadas en este artículo son de exclusiva responsabilidad del autor y no necesariamente representan la opinión de Contagio Radio.](http://www.rebelion.org/noticia.php?id=182980)
