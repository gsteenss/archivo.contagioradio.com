Title: Por derecho al agua se interpone tutela para defender páramo de Santurbán
Date: 2015-07-14 14:50
Category: Ambiente, Nacional
Tags: acción de tutela, Colectivo de Abogados Luis Carlos Pérez, consulta con comunidades, derecho al agua, Julia Figueroa, Mega minería, Páramo de Santurbán
Slug: por-derecho-al-agua-se-interpone-tutela-para-defender-paramo-de-santurban
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/07/Protestas-Santurbán.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: [www.olapolitica.com]

<iframe src="http://www.ivoox.com/player_ek_4802001_2_1.html?data=lZ2dlJWUdY6ZmKiakpyJd6KpkZKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRlNDmjMnS1MrHrNCfwtGYw8zZpYznxpDW0NnJttHjz8qY1trYqc3VjNXO1MaPqMbaxtPRx9ePtI6ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Julia Figueroa, Colectivo de Abogados Luis Carlos Pérez] 

###### [14, Julio, 2015] 

El **Colectivo de Abogados Luis Carlos Pérez y representantes del Comité en defensa del Páramo de Santurbán**, presentaron una acción de tutela para defender tres derechos fundamentales que le fueron vulnerados a la población y uno más que está en riesgo, por cuenta de los mega proyectos mineros.

“Se presenta una acción de tutela como el mecanismo más rápido para proteger derechos fundamentales, la participación, **el derecho a la información y el debido proceso, ya fueron vulnerados, además se está amenazando el derecho al agua**”, explica la abogada Julia Figueroa, del Colectivo de Abogados Luis Carlos Pérez.

En diciembre del año pasado se expidió el acto administrativo 2090 de 2014, donde se delimita el páramo. Según la abogada, tal delimitación no fue planteada desde una discusión abierta con la comunidad, negando a los ambientalistas, organizaciones de derechos humanos y universidades, entre otros, ** la posibilidad de plantear cómo debió hacerse la demarcación** de ese ecosistema, teniendo en cuenta aspectos sociales, económicos y ambientales.

La acción de tutela se presentó el  pasado 2 de julio de este año ante el Tribunal Administrativo de Santander, y  **se espera que haya una repuesta a finales de esta semana, **que también dependerá de “la magistrada ya que tiene que analizar las casi 40 pruebas que se presentaron”, señala Figueroa.

Mediante la tutela se solicita **la suspensión de los efectos de la resolución 2090 de 2014,** hasta tanto El Ministerio de ambiente, la Corporación Autónoma Regional Para La Defensa De La Meseta De Bucaramanga, y la Corporación Autónoma Regional de la Frontera Nororiental, CORPONOG , adopten y garanticen medidas necesarias para hacer efectivos los derechos fundamentales a la participación, la igualdad y al debido proceso.

El mecanismo legal se presenta como una de las primeras acciones jurídicas que se interpondrán en defensa de los ecosistemas de páramos, abriendo una puerta a su legalización y protección de manera real efectiva.

Cabe recordar, que la exploración y explotación de oro, **afecta directamente en su totalidad a la población de Bucaramanga, es decir más de 500 mil personas**, ya que las sustancias que se usan para la minería contaminan el agua, pues este ecosistema representa la fuente de agua dulce más importante de la región.
