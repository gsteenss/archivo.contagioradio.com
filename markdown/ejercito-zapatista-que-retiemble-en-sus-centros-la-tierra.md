Title: !Que retiemble en sus centros la tierra!
Date: 2016-12-09 09:09
Category: Eleuterio, Opinion
Tags: mexico, Zapatistas
Slug: ejercito-zapatista-que-retiemble-en-sus-centros-la-tierra
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/12/presidenta-zapatista.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: ecopolitico 

###### 9 Dic 2016 

#### **[Eleuterio Gabón ](https://archivo.contagioradio.com/eleuterio-gabon/)** 

[La propuesta causó revuelo, tanto por inesperada como por  inusual. Sucedió el pasado 12 de octubre en el CIDECI, en la Universidad de la Tierra cerca de San Cristóbal, Chiapas, México durante el V Congreso Nacional Indígena. Allí el **Ejército Zapatista de Liberación Nacional** propuso al  CNI presentar a una candidata indígena a las elecciones presidenciales mexicanas de 2018.]

[Los zapatistas siempre han rechazado la idea de la toma del poder en las instituciones y han apostado desde sus inicios por crear autonomía en sus territorios con independencia del Estado. Por ello su propuesta causó revuelo. Muchos dijeron que los zapatistas estaban manipulando a los indígenas, utilizándolos y que por fin se habían lanzado a la carrera presidencial, que formarían un partido...Nada de esto.]

[La propuesta no tiene nada de electoral, ni habló nadie de forar un partido político, ni de colocar a un indígena en las instituciones. Los pueblos indígenas mexicanos con una población que supera ampliamente los 10 millones de personas sufren la violencia, el saqueo, la explotación y la muerte en sus territorios. Ante esta situación los zapatistas propusieron dejar sólo de resistir y pasar a la ofensiva. En lugar de apartarse y silenciarse en la campaña electoral, cuando el foco de atención está puesto en la cuestión política, los zapatistas proponen irrumpir en la campaña presentando una candidatura independiente, a sabiendas de que ésta jamás saldrá elegida en un país con un sistema corrupto como el mexicano.]

[Presentar como candidata a una mujer indígena a las elecciones es presentar a la anticandidata. Es presentar a la parte más oprimida, maltratada y vulnerable del país por ser indígena y por ser mujer. La intención es de la recorrer México mostrando a la opinión pública las voces de los pueblos indígenas, sus denuncias, sus propuestas, su dignidad.]

[La propuesta está pendiente de ratificación por el Consejo Nacional Indígena que deberá consultar con sus bases en todo el país sobre la conveniencia o no de seguir esta estrategia. Se abre pues un camino que no se sabe bien a dónde conducirá pero como ya se dijo en otras ocasiones, lo importante es seguir caminando. Lo que sí es seguro es que este proceso de consulta servirá para movilizar a los pueblos originarios, para volver a ponerse en contacto, para repensarse, para fortalecerse para hecer que a su paso los centros del sistema retiemblen de nuevo.]

------------------------------------------------------------------------

###### [Las opiniones expresadas en este artículo son de exclusiva responsabilidad del autor y no necesariamente representan la opinión de la Contagio Radio.] 
