Title: Abuso de la Fuerza Pública ha dejado 30 homicidios en medio de la movilización social en cuarentena
Date: 2020-07-13 22:27
Author: CtgAdm
Category: Actualidad, Movilización
Tags: Abuso de fuerza ESMAD, Agresiones fuerza pública, Catatumbo, Erradicación Forzada, Movilización social
Slug: abuso-de-la-fuerza-publica-ha-dejado-30-homicidios-en-medio-de-la-movilizacion-social-en-cuarentena
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/07/6c2cde58-7402-4eba-b1ba-22b23b0a5386.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","fontSize":"small"} -->

Foto: Movilización durante la llegada de la Marcha de la Dignidad/ Contagio Radio

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Según la campaña [Defender la Libertad,](https://twitter.com/DefenderLiberta) desde que empezó el simulacro de aislamiento en Bogotá y la posterior cuarentena a nivel nacional, han ocurrido 30 asesinatos ocasionados por el exceso de la Fuerza Pública en el marco de las protestas sociales, además de un saldo de 135 personas heridas incluida una lesión ocular. Ante escenarios como la ausencia de garantías para una vida digna e la ciudad y en los territorios por parte del Gobierno, analistas señalan que existirá un incremento de la protesta social para el segundo semestre del 2020.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

**Catherine Moore Torres, secretaria técnica de la Campaña,** señala que si bien es cierto que las medidas de aislamiento han implicado un descenso de la protesta social, las personas continúen movilizándose como respuesta a la gestión del gobierno nacional y los gobiernos locales frente a las situaciones más allá de la cuarentena, muestra de ello ha sido el recorrido realizado por la Marcha de la Dignidad y su paso por más de 22 municipios. [(Le puede interesar: Todo lo que necesita sobre el avance de la Marcha por la Dignidad)](https://archivo.contagioradio.com/a-pasos-de-gigante-avanza-la-marcha-por-la-dignidad/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

En medio de estas movilizaciones y desde que se inicia el simulacro de aislamiento se han registrado al menos 38 agresiones con un promedio de 12 agresiones por mes, de las cuales 10 han sido ocasionadas por uso indebido de armamento destinado a la contención de la multitud y su uso indiscriminado, mientras otros 10 de los casos tiene que ver con el uso de armas de fuego por agentes de la Policía, yendo en contravía de los estándares internacionales que regulan la protesta, además de un caso más que implicó armamento no convencional por parte de la Fuerza Publica.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Moore es enfática en que no puede suspenderse el derecho a la movilización y a la libertad constitucional en el marco de la cuarentena, un mandato protegido por la [Comisión Interamerica de DD.HH](https://www.oas.org/es/cidh/decisiones/pdf/Resolucion-1-20-es.pdf), institución que ha alertado sobre el peligro de la suspensión de este tipo de derechos en medio de la pandemia.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

De igual forma **se registraron 19 casos en los que la protesta se vio interrumpida por el uso arbitrario de la fuerza, dispersando a los manifestantes, mientras otras 5 implicaron desalojo, algo que quebranta lo establecido en el decreto 57**, prohibiendo los desalojos durante la pandemia, convirtiéndose se en uno de los espacios más visibles y reiterados en medio de la cuarentena. [(Lea también: Desplazadas, desalojadas, golpeadas y sin techo permanecen 111 f)amilias embera en Bogotá)](https://archivo.contagioradio.com/desplazadas-desalojadas-golpeadas-y-sin-techo-permanecen-111-familias-embera-en-bogota/)

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Existe un patrón en las detenciones arbitrarias

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Defender la Libertad agrega que múltiples organizaciones de DD.HH. han alertado sobre el aumento de las detenciones arbitrarias con el uso de figuras que estipulan escenarios específicos en el Código de Policía y que pese a ello han dejado al menos 189 detenidos como se pudo evidenciar en el marco de la jornada de protestas del pasado 15 de junio, en particular en casos como Medellín en donde, de aproximadamente 53 personas que fueron detenidas arbitrariamente, al menos 40 de ellas fueron presentadas o tenían la intención de presentarse con fines de judicialización. También advierten sobre las continúas torturas y tratos inhumanos a personas que fueron detenidas y que denunciaron agresiones en función de violencia sexual.  [(Lea también: Persecución y desplazamiento campesino: el resultado de incumplimiento del Gobierno en Guaviare)](https://archivo.contagioradio.com/persecucion-y-desplazamiento-campesino-el-resultado-de-incumplimiento-del-gobierno-en-guaviare/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Aunque gran parte de las agresiones se concentran en la ciudad de Bogotá, otros departamentos como **Cauca, Antioquia, Atlántico, Valle del Cauca y Santander** son las que más han sufrido este tipo de vulneraciones a los DD.HH. A propósito Catherine señala que en las regiones también se han dado estas agresiones como consecuencia del adelanto de erradicaciones forzadas de cultivos de uso ilícito, señalando que de los homicidios registrados en el marco de la movilización 2 se dan en este contexto en Norte de Santánder y el uso de armas largas dejando 13 heridos. [(Ascamcat denuncia posible ejecución extrajudicial de Salvador Durán en Teorama)](https://archivo.contagioradio.com/ascamcat-denuncia-posible-ejecucion-extrajudicial-de-campesino-en-teorama/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Resaltan que de los 30 homicidios que registra Defender la Libertad, 23 corresponden a hechos a manos de la guardia penitenciaria contra reclusos el pasado 21 de marzo, cuando el movimiento carcelario convocó a un gran cacerolazo nacional por la crisis humanitaria en los penales. [(Le puede interesar: En veinte años el ESMAD ha asesinado a 34 personas)](https://archivo.contagioradio.com/en-veinte-anos-el-esmad-ha-asesinado-a-34-personas)

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Para el segundo semestre del 2020 viene un incremento en la movilización social

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

"Queda claro que este virus vino para quedarse y la gestión del Gobierno no ha permitido que las personas vivan esta cuarentena con dignidad lo que ha generado una exacerbación de las necesidades", explica Catherine, afirmando que hay situaciones como la ausencia de recursos para la población ni auxilio o la violencia en los territorios que desembocarán en una exigencia a través de la protesta y una continuación de las veedurías por parte de las organizaciones.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

"Lo que debe quedar claro es que es un derecho constitucional y no se puede suspender en épocas de emergencia", concluye con relación a cómo se continúa estigmatizando a la movilización ahora agregando un concepto irresponsabilidad por parte de la ciudadanía marchante cuando en realidad se desconoce "la irresponsabilidad política de quienes debe estar respondiendo a las exigencias de la población" .  [(Le puede interesar: Más de 100 detenidos y 20 heridos deja la violencia policial contra movilización del 15 de Junio)](https://archivo.contagioradio.com/violencia-policial-15-junio/)

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->
