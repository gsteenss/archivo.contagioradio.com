Title: Víctimas de Mapiripán aseguran que la memoria es un pilar fundamental para resistir
Date: 2017-07-13 17:47
Category: DDHH, Otra Mirada
Tags: mapiripan, Masacre de Mapiripan, Poligrow
Slug: victimas-de-mapiripan-aseguran-que-la-memoria-es-un-pilar-fundamental-para-resistir
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/07/ColombiaDesplazados.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Primicia Diario] 

###### [13 Jul. 2017]

Sin importar que hayan pasado 20 años de la masacre de Mapiripán **las víctimas han resistido no solo a través de la exigencia de la restitución de sus tierras, sino también a través de la memoria**. Han sido muchos días y noches en los que han recordado aquellos difíciles 5 días, pero también a sus familias y seres queridos que perdieron la vida.

Muchas de las víctimas, **hoy ven con tristeza cómo sus tierras ya no son las mismas de antes y cómo el pueblo en vez de estar mejor, ha empeorado**, tal y como lo relata Marina SanMiguel una de las víctimas de esta masacre y quien 20 años después asegura que no ha sido fácil reconstruirse.

“Hoy 20 años Mapiripán en nuestras vidas, en mi familia no ha sido recuperada la vida, porque digamos que es diferente que le den a uno un dinero por el asesinato de un ser querido y eso no resarce todo el daño que se hace. **Son cosas que uno no puede borrar que se quedan en el caminar”** manifiesta SanMiguel.

### **Los hijos también tienen en su memoria la Masacre de Mapiripán** 

Asegura SanMiguel que no solo los adultos son quienes llevan grabada en su memoria la Masacre, sino que los “hijos también viven con esa historia, porque recuerdan que salimos de Mapiripán, lo perdimos todo, dicen perdí a mi padre”.

En medio de ese caminar, dice SanMiguel que las otras personas que no comprenden la dimensión de lo que significa ser víctima los miran “como algo raro, pero **seguimos buscando una explicación ante lo que pasó** y por más que uno se pregunta y busca no encuentra explicaciones”. Le puede interesar: [Coronel (r) Plazas Acevedo será procesado por presunta responsabilidad en la Masacre de Mapiripán](https://archivo.contagioradio.com/coronel-r-plazas-acevedo-sera-procesado-por-presunta-responsabilidad-en-la-masacre-de-mapiripan/)

Asegura que desde los y las Mapiripenses hay una lucha por la memoria, de resistencia y que pretende **buscar la “verdadera justicia, porque queremos que el Estado nos diga porqué permitió eso**, porqué nos sacaron de nuestros territorios y porqué hoy en día están otras ocupantes allí”.

### **¿Qué proponen las víctimas para que Mapiripán sea un territorio de paz?** 

Que el Estado reconozca que la masacre sucedió en Mapiripán, que hay unas víctimas que están esperando **una verdad y el regreso a su territorio con garantías, con inclusión social, inversión con proyectos para las comunidades,** mejoras al sistema de salud y a las vías son algunas de las propuestas que las víctimas de esta masacre tienen para que ese municipio sea un territorio de paz.

“Deben ser propuestas con inclusión social, que sean proyectos de vida, no como los que están dando de cualquier cosa, sino que sea un proyecto muy grande con salud, carreteras, devolución de tierras, para que haya una paz de verdad en Mapiripán” añade SanMiguel.

Además, dice SanMiguel que **es necesario mejorar el acceso a la educación superior para sus jóvenes** pues en la actualidad “pasan las solicitudes a las universidades y no los aceptan, entonces la juventud se está quedando sin educación superior”. Le puede interesar: [Mujeres de Mapiripán resisten al olvido tras 20 años de la masacre](https://archivo.contagioradio.com/masacre-de-mapiripan-obligo-a-las-mujeres-a-reconstruirse/)

Por último, insiste en que debe haber **inversión social en sus territorios “porque al no tener tierras muchos de esos campesinos están en las ciudades** y no saben ni que hacer, otros están enfermos, otros han fallecido por todo lo que han tenido que vivir esperando una reparación que no ha llegado luego de 20 años”.

<iframe id="audio_19790986" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_19790986_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU).

<div class="osd-sms-wrapper">

</div>
