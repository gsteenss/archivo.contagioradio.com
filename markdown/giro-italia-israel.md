Title: Polémica por inicio del Giro de Italia 2018 en Israel
Date: 2017-09-28 15:04
Category: Onda Palestina
Tags: Apartheid, BDS, Israel, Palestina
Slug: giro-italia-israel
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/09/giro.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto:bdsmovement.net 

##### 28 Sep 2017 

Por primera vez en más de un siglo de historia el Giro de Italia arrancará fuera de Europa y esta excepción se realiza como una forma de promocionar a Israel a nivel internacional. **La primera etapa, una contrarreloj de 10,1 km, pasará el 4 de mayo de 2018 a las puertas de la Ciudad Vieja de Jerusalén**, considerada por la comunidad internacional una zona ocupada por Israel desde 1967. En los dos días posteriores la carrera se desplazará hasta Tel Aviv y después circulará por el desierto del Neguev, hasta la ciudad israelí de Eilat, en la frontera con Egipto y Jordania.

Para Israel, la carrera será el momento de promocionar la imagen del país y de atraer turistas: “**Queremos atraer turistas durante el Giro y promover la imagen de Israel ante los espectadores del mundo entero**”, explicó el ministro israelí de Turismo, Yariv Levin. Sinembargo, La identificación de Jerusalén con Israel que realizan los organizadores del Giro ha molestado a los palestinos que recuerdan que Jerusalén es una ciudad ocupada y reivindican Jerusalén-Este como propia según los acuerdos de Oslo.

Sharaf Qutaifan, de la Campaña Palestina por el Boicot Cultural y Académico a Israel (PACBI, por sus siglas en inglés), dijo que “**Empezar el Giro de Italia en Israel en la práctica es como dar un premio a ese país por sus décadas de interminables abusos de los derechos humanos del pueblo palestino**, incluyendo a sus atletas. Israel ha bombardeado los estadios deportivos palestinos, detenido, encarcelado y asesinado atletas palestinos, asaltado asociaciones deportivas palestinas y destruido campos de juego con total impunidad. Llevar a cabo la carrera ciclística en Israel va en contra de los ideales y principios de los deportes”.

La PACBI está haciendo un llamado a las amantes del ciclismo, y a la sociedad civil internacional en general, a que pidan a los organizadores del Giro que trasladen el inicio de esta carrera a otro lugar. Están llamando es a enviar una comunicación a RCS Sport, quienes organizan el Giro, que se puede enviar desde la página <https://bdsmovement.net/giro> ; en ella se solicita específicamente se respete la legislación internacional y reubique la carrera en otro país,

En esta ocasión, **en Onda Palestina** hablaremos sobre el estado actual de la Unidad Palestina, Hamas y su influencia en territorio palestino, principalmente en la franja de Gaza y la intervención de la Autoridad Nacional Palestina en la Asamblea General de la ONU. Continuaremos con la tercera parte de la entrevista a la activista de origen judío Liliana Cordova Kaczerginski, y finalmente nuestro tercer especial dedicado a la poesía Palestina.

<iframe id="audio_21134516" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_21134516_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Escuche esta y más información sobre la resistencia del pueblo palestino en [Onda Palestina](https://archivo.contagioradio.com/programas/onda-palestina/), todos los Martes de 7 a 8 de la noche en Contagio Radio. 
