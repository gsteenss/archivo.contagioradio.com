Title: Asesinan a tres líderes sociales en la noche de este 10 de Enero
Date: 2020-01-11 12:38
Author: CtgAdm
Category: DDHH, Nacional
Tags: Antioquia, asesinato de líderes sociales, Cauca, cesar
Slug: asesinan-tres-lideres-10-enero
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/10jpg.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio ] 

Desde diversas fuentes se denunció que la noche de este 10 de enero fueron asesinados tres líderes sociales en diferentes regiones del país. Con esta cadena de asesinatos se completan casi unos 15 en lo que va corrido del 2020. Se trata de Sergio Narváez en Turbo, Antioquia, Amparo Guegia  quien fue asesinada con su hijo en Caloto, Cauca, y Henry Cuello en Rincón Hondo, Cesar.

### Sergio Narvaez ex concejal de Turbo 

La noticia del asesinato de Sergio Narváez se conoció sobre las 9:30 de la noche, cuando desconocidos habrían disparado contra el ex concejal en el sector de Nueva Colonia en el Municipio de Turbo, Antioquia. **Este puerto es controlado por las llamadas Autodefensas Gaitanistas de Colombia por ser una de las principales rutas del narcotráfico desde el departamento del Chocó.**

### Amparo Guegia y su hijo 

Fue asesinada cerca de su casa en el corregimiento del Palo en Caloto Cauca, hasta el momento se desconocen los pormenores del crimen de Guegia, sin embargo era reconocida como una de las fundadoras del Movimiento Nietos del Quintin Lame. [(Le puede interesar: Colombia inicia el año con aumento de violencia contra líderes sociales)](https://archivo.contagioradio.com/colombia-inicia-el-ano-con-aumento-de-violencia-contra-lideres-sociales/)

Pascue Ulkue Julio Cesar, que se identifica en redes sociales como comisario suplente del Cabildo San Lorenzo afirmó, “rabia, dolor e impotencia sentimos quienes hacemos parte del MST Nietos de Manuel Quintin Lame frente al Asesinato de nuestra compañera Amparo Guegia y su hijo, hecho ocurrido hoy a las 10 pm del dia viernes 10 de enero en la localidad del Palo, Caloto, esta mujer fundadora de nuestro movimiento nos deja un gran vacío. Repudiamos este crimen y **exigimos al gobierno nacional se garantice y se respete el derecho a vivir.”**

### Henry Cuello, uno de los líderes comunales de Rincón Hondo, Cesar 

Sobre las 10 de la noche también se conoció que Henry Cuello fue abordado por sujetos que **le dispararon en frente a su lugar de residencia en Rinconhondo, jurisdicción del municipio de Chiriguaná, Cesar,** hasta el momento se ha podido establecer que era un líder comunal cercano a las políticas del gobierno de Iván Duque y el Centro Democrático. [(Lea también: Sabedora indígena Nasa Virginia Silva es asesinada en Cauca)](https://archivo.contagioradio.com/sabedora-indigena-nasa-virginia-silva-es-asesinada-en-cauca/)

Noticia en desarollo...

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
