Title: EEUU, Rusia, Europa y Kiev ultiman un acuerdo de paz para Ucrania
Date: 2015-02-10 06:13
Author: CtgAdm
Category: El mundo, Otra Mirada
Tags: Conversaciones entre EEUU, Europa, Rusia, Ucrania
Slug: eeuu-rusia-europa-y-kiev-ultiman-un-acuerdo-de-paz-para-ucrania
Status: published

###### **Foto:Proyecto40.com** 

##### **Entrevista a[ Kateryna Palanska:]** 

##### <iframe src="http://www.ivoox.com/player_ek_4064022_2_1.html?data=lZWjlpWWdo6ZmKiakp2Jd6Kkl5KSmaiRdo6ZmKiakpKJe6ShkZKSmaiRksbb0MjWw8jNs8%2FZ1JDS0NnWqYzJpoqfpZCpibbJjN6YtNrXrcKf0cbfw5DZsozVxNrS1MnTb8XZjoqkpZKns8%2FowszW0ZC2pcXd0JCah5yncZU%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

Después de la conversación entre **Hollande, Merkel y Putin** esta semana, continuó con una reunión con **Obama**, para ultimar posibles acuerdos con el objetivo de fijar una **agenda de paz y frenar de inmediato el conflicto armado en la zona fronteriza con Rusia**.

Todas las conversaciones han sido a puerta cerrada y en la agenda queda la reunión del miércoles entre Putin, Obama y el presidente Ucraniano. En todo caso **aún no existe representación de las fuerzas políticas de las regiones independentistas en las conversaciones. **

En la última semana en total han muerto más de 12 personas en bombardeos sobre Donestk y otras regiones con fuerte presencia separatista.

En el horizonte queda una negociación complicada en la que los factores económicos pesan más sobre los políticos como es actualmente el gas o la exportación de alimentos hacia Rusia.

Por último es importante remarcar que el **ejército ucraniano no está en condiciones de terminar con los grupos armados insurgentes** y por ello no se descarta un rearme por parte de potencias como EEUU o Alemania de dicho ejército con el objetivo de poder reconquistar la zona.

Entrevistamos a Kateryna Palanska, politóloga ucraniana afincada en España, quién nos informa de lo que esta sucediendo ahora mismo en la frontera y de las consecuencias y valoraciones a futuro de dichas conversaciones.
