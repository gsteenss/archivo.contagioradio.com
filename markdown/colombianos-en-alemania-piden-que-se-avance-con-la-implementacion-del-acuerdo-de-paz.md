Title: Colombianos en Alemania piden que se avance con la implementación del Acuerdo de Paz
Date: 2018-05-16 13:42
Category: Paz, Política
Tags: acuerdo de paz, colombianos en alemania, Colombianos en el exterior, Juan Manuel Santos
Slug: colombianos-en-alemania-piden-que-se-avance-con-la-implementacion-del-acuerdo-de-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/10/colombainos-alemania-e1508872749732.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: El Espectador] 

###### [16 May 2018] 

Diferentes organizaciones de colombianos que viven en Alemania le enviaron una carta al presidente Juan Manuel Santos, en la que rechazaron las afirmaciones del primer mandatario que afirmó que la implementación de los Acuerdos de Paz “va viento en popa”. En ese sentido piden que se desarrollen acciones concretas para **avanzar con la implementación de los mismos.**

Las organizaciones subrayaron siete puntos que consideran han ido en detrimento con la implementación de lo acordado en La Habana y reconocieron que debe haber esfuerzos para “evitar que lo alcanzado en los últimos años **termine en nuevos ciclos de violencia armada”.**

### **Peticiones de los colombianos en Alemania** 

En esa medida, hicieron alusión a la continuidad en los asesinatos de **líderes sociales y defensores de derechos humanos** y la contradicción del Gobierno de Colombia que se vende como país que respeta los derechos del ambiente.** ** De acuerdo con Adriana Yee, integrante del Colectivo Ciudadanos Unidos por la Paz, este es uno de los temas más álgidos en Europa donde ven con preocupación los 282 asesinatos que se han presentado este año.

Adicional a esto, cuestionaron los cambios que se han realizado al Acuerdo de Paz “que incluyen **eliminación de la obligatoriedad** del sometimiento de los terceros civiles intervinientes en el conflicto armado a la Jurisdicción Especial para la Paz y la eliminación de las 16 circunscripciones especiales para la paz”.

Consideran negativo que sólo se haya implementado el **18,3% de lo acordado** y que se estén implementando leyes que van en contravía del espíritu del Acuerdo como lo es la Ley Zidres. A esto se suma “el avance casi nulo de los procesos de reincorporación colectiva, que se evidencia en el escaso número de proyectos productivos que actualmente están operando”. (Le puede interesar:["Movimientos sociales de Alemania exigen parar asesinatos de líderes sociales"](https://archivo.contagioradio.com/grupos-sociales-de-colombianos-en-alemania-exigen-respeto-por-la-vida-en-el-pais/))

Además, le recordaron a Santos que hay “incumplimiento de los acuerdos alcanzados en el marco de la **erradicación voluntaria y sustitución de cultivos de uso ilícito**”. Situación que por el contrario, ha promovido “la erradicación forzada que aumenta las tensiones sociales y ha tenido nefastos desenlaces como la masacre de Tumaco”.

Igualmente, rechazaron “el incumplimiento de la Ley de Amnistía y la permanencia en las cárceles de centenares de excombatientes de las antiguas FARC-EP” a la vez que manifestaron que es importante **ejecutar y fiscalizar** de manera adecuada los recursos que aporta la comunidad internacional para la implementación de lo Acordado.

### **Piden unir esfuerzos para no volver a la violencia** 

Los colombianos en Alemania se unieron al llamado sobre detener la **extradición de Jesús Santrich**, pidieron que se respete el debido proceso y consideraron “que el sometimiento a la JEP, las garantías de verdad y no repetición, deberán prevalecer sobre los requerimientos de cualquier país extranjero”.

Finalmente, recordaron que los colombianos viviendo en el exterior seguirán “haciendo una constante veeduría a la implementación del Acuerdo del Teatro Colón y a los avances del actual proceso de paz con el Ejército de Liberación Nacional. **Nosotros no seremos quienes nos tiremos la paz de Colombia”.**

[180503\_Carta Besuch Santos Berlín - firmada](https://www.scribd.com/document/379423006/180503-Carta-Besuch-Santos-Berli-n-firmada#from_embed "View 180503_Carta Besuch Santos Berlín - firmada on Scribd") by [Contagioradio](https://www.scribd.com/user/276652802/Contagioradio#from_embed "View Contagioradio's profile on Scribd") on Scribd

<iframe id="doc_41473" class="scribd_iframe_embed" title="180503_Carta Besuch Santos Berlín - firmada" src="https://www.scribd.com/embeds/379423006/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-Jr7udy0sIdgwQoRaAcgG&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="0.7729220222793488"></iframe><iframe id="audio_26015405" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_26015405_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### **Rec**iba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 

 
