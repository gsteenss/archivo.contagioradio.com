Title: Nuevos respaldos a Ley de Sometimiento de bandas criminales
Date: 2018-07-12 10:46
Category: Nacional, Paz
Tags: Arzobispo de Cali, Heike Hänsel, Ley de Sometimiento a Bandas Críminales, Obispo de Apartado
Slug: apoyos-ley-de-sometimiento-bandas-criminales
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/03/Autodefensas_Gaitanistas.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: La Razón] 

###### [12 Jul 2018] 

Con las manifestaciones por parte de la iglesia Católica, representada por el Arzobispo de Cali, Darío de Jesús Monsalve y el Obispo de Apartadó, Hugo Alberto Torres así como de la parlamentaria alemana Heike Hänsel, la Ley 908 de 2018, por la cual “se adoptan medidas para la sujeción a la justicia y se dictan otras disposiciones de grupos armados organizados (GAO) y grupos delictivos (GAD)”, recibió un importante espaldarazo en su objetivo de construir una paz completa para Colombia.

Los jerarcas de la Iglesia Católica afirmaron que con esta norma, se abre el camino para construir una paz sostenible y duradera en la que se vean comprometidos todos los actores armados del conflicto. Lo que significa en hechos concretos, la posibilidad del fin de la violencia directa en zonas como Cauca, Chocó, Antioquia y Valle del Cauca.

Adicionalmente, instaron a las Autodefensas Gaitanistas de Colombia y demás Grupos Armados Organizados a que se sometan ante la nueva Ley y manifestaron su "disposición para acompañar todo gesto de paz". (Le puede interesar: ["Ley de Sometimiento a bandas crimínales no implica darles reconocimiento político: Santos"](https://archivo.contagioradio.com/ley-de-sometimiento-a-la-justicia-de-bandas-criminales-un-paso-para-la-construccion-de-paz/))

Por su parte, la parlamentaria Heike Hänsel, celebró que se siga buscando el diálogo y la salida negociada al conflicto colombiano, y destacó que se deben tener de presente tanto la reparación integral a las víctimas, como el debido proceso para que dicho sometimiento sea efectivo.

Estos apoyos resultan valiosos para la construcción de la paz en los próximos años, por una parte, porque la iglesia colombiana ha jugado un papel importante en las negociaciones con el ELN y las FARC, como con otras bandas criminales como el Clan del Golfo; y por otra, porque el pronunciamiento de la parlamentaria alemana, refleja la atención que tiene el proceso colombiano ante el mundo, y el apoyo que se brinda desde diferentes instancias al mismo.

[180711\_Carta al Presidente Juan Manuel Santos\_Sanción Ley de sometimiento colectivo a la Justicia bandas c...](https://www.scribd.com/document/383726992/180711-Carta-al-Presidente-Juan-Manuel-Santos-Sancio-n-Ley-de-sometimiento-colectivo-a-la-Justicia-bandas-criminales-y-otras-Parlamentaria-Heike-Ha-ns#from_embed "View 180711_Carta al Presidente Juan Manuel Santos_Sanción Ley de sometimiento colectivo a la Justicia bandas criminales y otras_Parlamentaria Heike Hänsel on Scribd") by [Contagioradio](https://www.scribd.com/user/276652802/Contagioradio#from_embed "View Contagioradio's profile on Scribd") on Scribd

<iframe id="doc_19609" class="scribd_iframe_embed" title="180711_Carta al Presidente Juan Manuel Santos_Sanción Ley de sometimiento colectivo a la Justicia bandas criminales y otras_Parlamentaria Heike Hänsel" src="https://www.scribd.com/embeds/383726992/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-jBK6M6tj6poBgaAfNzt5&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="0.7080062794348508"></iframe>

[Pronunciamiento de la Arquidiócesis de Cali y Diócesis de Apartadó](https://www.scribd.com/document/383727173/Pronunciamiento-de-la-Arquidiocesis-de-Cali-y-Diocesis-de-Apartado#from_embed "View Pronunciamiento de la Arquidiócesis de Cali y Diócesis de Apartadó on Scribd") by [Contagioradio](https://www.scribd.com/user/276652802/Contagioradio#from_embed "View Contagioradio's profile on Scribd") on Scribd

<iframe id="doc_90392" class="scribd_iframe_embed" title="Pronunciamiento de la Arquidiócesis de Cali y Diócesis de Apartadó" src="https://www.scribd.com/embeds/383727173/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-nT5H5P2XlGcp2cOP4anv&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="0.7080062794348508"></iframe>  
 

###### [Reciba toda la información de Contagio Radio en] [[su correo] [[Contagio Radio]
