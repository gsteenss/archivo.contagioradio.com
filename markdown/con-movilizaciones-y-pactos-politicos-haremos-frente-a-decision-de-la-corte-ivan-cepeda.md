Title: “Con movilizaciones y pactos políticos haremos frente a decisión de la Corte” Iván Cepeda
Date: 2017-05-18 13:25
Category: Entrevistas, Paz
Tags: acuerdo de paz, Congreso, Congreso de Colombia, Fast Track, paz
Slug: con-movilizaciones-y-pactos-politicos-haremos-frente-a-decision-de-la-corte-ivan-cepeda
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/10/marcha-por-la-paz64.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [18 May. 2017] 

Luego de la decisión tomada por la Corte Constitucional en la que el Congreso de la República ahora podrá modificar el acuerdo de paz firmado entre el gobierno y las FARC, las reacciones de diversos sectores políticos no se han hecho esperar, para **Iván Cepeda senador del Polo Democrático, no hay que restarle la importancia al tema**, pero tampoco habría que dejar de recordar que “este no es el primer momento crítico en el proceso”.

Aseguró que sin falsos optimismos hay que proceder a encontrar una solución **"habrá que pensar cuál va a ser la manera de tejer pactos políticos que garanticen una estabilidad para aprobar normas.** Tenemos que hacer un gran esfuerzo para que de manera imaginativa y con la movilización social, con las fuerzas políticas, encontrar la forma de resolver este problema que crea una nueva composición de la Corte Constitucional”. Le puede interesar: [Corte Constitucional congela acuerdo de paz con las FARC: Enrique Santiago](https://archivo.contagioradio.com/corte-constitucional-congela-acuerdo-de-paz-con-las-farc-enrique-santiago/)

Para Cepeda, es importante recordar que hoy existe una mayoría de sectores en el país que están muy afines a todos “estos adversarios del proceso de paz”, lo que según el cabildante crea **problemas complejos como poder garantizar la seguridad jurídica y física a los integrantes de las FARC** que han expresado su voluntad de hacer una dejación de las armas.

“La corte acaba de crear una situación en la cual un proceso que ya era difícil con el “Fast Track” se vuelve más complejo, y habrá que definir una nueva estrategia. Esto pone a la guerrilla en una situación de considerar los tiempos y cómo son también sus propias estrategias en medio del proceso de paz”.

### **El “Slow Track” en el que entra en Congreso** 

Por su parte, el representante a la Cámara por el Polo Democrático, Alirio Uribe se mostró respetuoso ante la decisión de la Corte, pero aseguró que ésta **complica la situación porque el Congreso podrá demorarse más tramitando los proyectos** y que permitirá que “los enemigos de la paz” les introduzcan más micos a los proyectos.

Mientras que el senador Cepeda manifiesta que hay 2 problemas centrales y es que aún **falta por evacuar las normas más difíciles como la ley estatutaria de la JEP**, el desarrollo del punto agrario, la sustitución de cultivos de uso ilícito y la reforma política.

Y por otro lado, el segundo problema es que **la Corte pueda comenzar a hundir o echar abajo lo que haga el Congreso** “todos estos son problemas que se le plantean a las fuerzas políticas y al gobierno quien es el principal responsable de cumplir con los acuerdos” manifiesta Cepeda. Le puede interesar: [Del Fast Track al Slow Track en la implemetación de Acuerdos de Paz](https://archivo.contagioradio.com/del-fast-track-al-slow-track-en-la-implemetacion-de-acuerdos-de-paz/)

### **El Congreso ya había hecho modificaciones al Acuerdo de Paz** 

Hasta el momento se conoce de más de **90 proposiciones que el Congreso realizó al acuerdo de paz en el marco del Acto Legislativo de le JEP**, incluyendo algunas modificaciones que iban en contravía del espíritu de los Acuerdos de Paz como el tema de la responsabilidad de mando, de los actores privados y empresarios y su responsabilidad frente a los actos cometidos en el conflicto ante la JEP.

“Creo que la implementación de los Acuerdos es una lucha que va a tener unos 10 años o no sé cuántos y **el Congreso pues se va a volver ahora un escenario mucho más fuerte para la presión ciudadana** que hay que hacer para que se implementen los Acuerdos. Ahora hay que rodear el Congreso, en el buen sentido de la palabra, para que el Congreso no le haga ‘conejo a la paz’ relató Uribe.

### **El sueño de paz debe continuar** 

Para el Senador y el representante a la Cámara hay que hacer **un llamado a la ciudadanía, a las organizaciones sociales, a los jóvenes para que se movilicen**, tal como se hizo después  del 2 de octubre con el No en el Plebiscito para proteger el proceso de paz y garantizar la implementación de los acuerdos. Le puede interesar: [FARC espera que Santos haga valer sus facultades para sacar adelante acuerdos de paz](https://archivo.contagioradio.com/corte_constitucional_fast_track/)

“Creo que **de ninguna manera esta decisión significa que el sueño de la extrema derecha colombiana se haga realidad**, el proceso de paz ha avanzado, tiene reservas y yo creo que es el momento en que debemos buscar solución a los problemas de la implementación de los acuerdos” concluyó Cepeda.

<iframe id="audio_18772870" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_18772870_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio]{.s1}](http://bit.ly/1ICYhVU)[.]{.s1}

<div class="ssba ssba-wrap">

</div>
