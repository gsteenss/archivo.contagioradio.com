Title: Violence against activists increased in Colombia in 2018: report
Date: 2019-05-10 18:06
Author: CtgAdm
Category: English
Tags: CINEP
Slug: violence-against-activists-increased-in-2018-according-to-cinep
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/Líderes.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:] 

According to a new report from the Center for Research and Popular Education (CINEP), 648 members and leaders of social movements were assassinated in Colombia last year, representing an increase in politically-motivated violence compared to 2017.

From January 1 to December 31 2018, CINEP documented 1,418 cases of human rights violations that included 648 assassinations, 48 assassination attempts, 1151 people threatened, 304 wounded, 66 tortured, 3 victims of sexual violence, 22 disappeared and 243 people arrested arbitrarily. In total, 2,252 people were affected.

The report also warns that Valle del Cauca, Santander, Antioquia, Choco and Bolivar are the departments with the highest number of human rights violations reported. Compared to 2016 and 2017, the number of people injured has decreased but deaths and threats against these activists in the country have increased by 25% and 32%, respectively.

### **The perpetrators remain anonymous **

The CINEP also found that in the most cases, investigators have not been able to identify a culprit. Still, the research center concluded that these cases of violence were politically motivated in accordance with the conceptual framework set by the National Network of Data Banks. Father Javier Giraldo, a CINEP researcher, expressed concern over the failure of investigators to identify the material and intellectual authors of these attacks against social and community leaders.

"We are seeing a metamorphosis. The paramilitary structures are transforming, becoming anonymous, retaining their methodology of oppression. They are no longer claiming a name. The crime remains completely anonymous and absolutely impossible of being investigated," Giraldo said.

### **Attacks against community activists are increasing **

At the same time, the targets of these attacks has changed too. In the past, the victims of political violence were usually supports of left-wing political parties and movements. According to Guillermo Cardona, member of the National Confederation of Community Action, "political activism doesn't matter anymore". (Related: "[Colombia's Transitional Justice System Goes To The Inter-American Comission on Human Rights](https://archivo.contagioradio.com/the-reason-why-the-special-jurisdiction-for-peace-will-be-in-the-inter-american-commission-on-human-rights-focus/)")

"They attack leaders who exercise their constitutional rights in their communities, those who fight against corruption, and those who defend the land and water," Giraldo continued in reference to the more than 64,000 community action boards that exist in the country.

### **The struggle for land **

Giraldo explained that these groups of people are targeted, in part, because they work to substitute illicit crops in their community. "They are not interested in the crop subsitution but in displacing peasants from their lands in order to give it to big multinationals," he said.

Colombia is the second most inequitable country in Latin America, which has been driven by a history of internal forced displacement and the concentration of lands. According to Luis Guillermo Guerrero, the director of CINEP, this is an unresolved issue for the Peace Deal and that it has been overshadowed by other issues of interest to the government, such as drug trafficking.

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
