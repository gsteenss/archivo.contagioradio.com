Title: Familias confinadas en sus fincas por ataques paramilitares en Guacamayas
Date: 2016-09-12 14:21
Category: DDHH, Nacional
Tags: Desplazamiento vereda Guacamayas, desplazamientos en Turbo, Grupos Paramilitares Sucesores, Guacamayas Turbo
Slug: familias-confinadas-en-sus-fincas-por-ataques-paramilitares-en-guacamayas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/09/Reclamantes-Guacamayas-1-1068x601.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: IPC] 

###### [12 Sept 2016] 

Desde principios del mes de junio, las familias campesinas de la vereda Las Guacamayas, en la zona rural del municipio de Turbo, Antioquia, se enfrentan a **continuos ataques por parte de invasores que, aliados con grupos paramilitares**, buscan desplazarlos nuevamente de sus territorios. Según afirma Julio Correal, en Villa Rosa se tomaron la escuela y han matado animales; en Villa Eugenia, un poblador no se atreve a salir de su casa por temor a ser asesinado, y en el resto de la vereda los campesinos son amenazados por cultivar.

Correal asegura que Franklin Cardona, conocido bajo el alias de 'Mono Cardona', tiene responsabilidad en las [[amenazas y los ataques](https://archivo.contagioradio.com/autodefensas-gaitanistas-hostigan-a-familias-de-la-vereda-guacamayas-turbo/)]. El más reciente hecho se dio contra un poblador de la vereda de Guacamayas quien se dirigía a su finca y **fue abordado por hombres en moto cuando iba por el municipio de Chigorodó**. El campesino tuvo que estacionarse cerca al comando de la Policía, mientras el escolta asignado por la Unidad Nacional de Protección llegaba.

La respuesta de las autoridades es lo que más alarma a estas comunidades, pues el **Ejército les ha asegurado que mantendrá sus rutinas y monitoreos pero que la protección de las familias no es de su competencia** sino de la Policía, a quien también se han dirigido los pobladores sin obtener respuestas contundentes. De la Fiscalía tampoco han obtenido acciones efectivas [[pese a las denuncias que con nombres y apellidos han interpuesto](https://archivo.contagioradio.com/presuntos-paramilitares-desplazan-a-por-lo-menos-5-familias-en-turbo-antioquia/)] los líderes campesinos.

"Estamos en favor del sí en el plebiscito pero no vemos respuesta para los que estamos peleando la restitución de nuestras tierras, los que estamos en los Consejos Comunitarios con tierras tituladas desde el 2000, nos atacan (...) **queremos que el presidente Santos nos escuche, nos preste atención** y les diga a las autoridades competentes que aceleren la entrega de nuestras tierras", agrega Correal.

La situación es verdaderamente alarmante concluye Julio Correal, mientras las **personas adultas permanecen confinadas en sus fincas, los niños y las niñas han tenido que dejar sus casas para poder estudiar**, pues en Villa Rosa la escuela está tomada por los invasores y en Guacamayas no se ha reconstruido la escuela que fue destruida años atrás por miembros de las Autodefensas. La comunidad continúa llamando la atención de las autoridades quienes más allá de mercados, [[no han atendido efectivamente la situación de peligro](https://archivo.contagioradio.com/familias-en-turbo-en-riesgo-inminente-de-un-desplazamiento-masivo/)] en la que se encuentran.

<iframe id="audio_12875872" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_12875872_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)]] 
