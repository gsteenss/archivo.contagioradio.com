Title: Humedales Tibanica, El Burro y Jaboque en Bogotá, están en riesgo de desaparición
Date: 2017-10-18 13:17
Category: Ambiente, Voces de la Tierra
Tags: Bogotá, conservación de humedales, Humedales, Humedales de Bogotá, humedales en riesgo
Slug: humedales-de-bogota-tibanica-el-burro-y-jacobe-en-riesgo-de-desaparicion
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/10/humedal2-.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Humedales Bogotá] 

###### [18 Oct 2017] 

El concejal por Bogotá, Antonio Sanguino, denunció que, en Bogotá, **hay 15 humedales que están riesgo** por que se han convertido en destino de las aguas residuales y en botaderos de basura. Además, no tienen zonas de ronda que los protejan y se ha incrementado el número de especies invasivas, además de los problemas con los edificios aledaños.

Luego de haber realizado una visita a los humedales, con su grupo de trabajo, el concejal denunció que hay tres humedales en condiciones preocupantes y de crisis. Estos son: **el humedal Jaboque, El burro y Tibanica** que se encuentran al occidente de la ciudad. Según el concejal hay un manejo inadecuado de las basuras en zonas aledañas y las administraciones distritales no han establecido un plan de protección de estos lugares.

### **¿Qué pone en riesgo a los humedales de la ciudad?** 

Sanguino indicó que el mayor riesgo para los humedales, **es la cantidad de basura que desecha de manera inadecuada la comunidad**. Además, la mayoría de los espejos de agua, no cuenta con la delimitación adecuada y necesaria para evitar que existan problemas de seguridad como expendios de drogas y atracos por la falta de iluminación. (Le puede interesar:["Constructoras estarían rellenando humedales en Chía, Cundinamarca"](https://archivo.contagioradio.com/humedales_constrcutoras_chia/))

Afirmó que **el vertimiento de aguas residuales está afectando la calidad del agua**, la flora y la fauna que se encuentra en estos lugares. Esto se debe, en gran medida, a que no hay zonas de ronda alrededor de los humedales como lo dispone la normatividad que debe ser de “30 metros de ancho a cada lado de los cauces”.

A esto se suma que en las zonas aledañas hay urbanizaciones, bodegas y depósitos de maquinaria pesada que **generan desechos mal tratados** que dificultan la conservación del agua. Igualmente, las administraciones de Bogotá le han dado un mal manejo a los planes ambientales que carecen de articulación con la ciudadanía. (Le puede interesar: ["Las constructoras son un peligro para los humedales en Bogotá"](https://archivo.contagioradio.com/las-constructoras-son-un-peligro-para-los-humedales-de-bogota/))

### **Administración de Peñalosa no ha consolidado estrategias de conservación** 

Sanguino indicó que, en lo que lleva la administración de Enrique Peñalosa, **no se han consolidado los planes** de los parques ecológicos de los humedales Juan Amarillo, Jaboque, Córdoba, Vaca y Burro, que contribuyen a la preservación del agua, la flora y la fauna. Tampoco ha desarrollado la gestión de los planes de manejo ambiental en los humedales que están en crisis en la ciudad.

El concejal propuso y reiteró la importancia de que la administración distrital aplique el Plan Estratégico de RAMSAR 2016-2024 para el cuidado de los humedales. Allí, se establece **la prioridad que existe en la conservación de los humedales** que son importantes “para la conservación de la biodiversidad, la reducción del riesgo de desastres y la mitigación del cambio climático”.

Finalmente, Sanguino recordó que los humedales son importantes para la ciudad en la medida que **son espacios de aprendizaje generadores de conocimiento** e investigación y son espacios de recreación pasiva. Dijo que “cumplen un papel regulador y de preservación del agua” en la capital del país.

\

###### Reciba toda la información de Contagio Radio en [[su correo]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio]{.s1}](http://bit.ly/1ICYhVU)[.]{.s1}
