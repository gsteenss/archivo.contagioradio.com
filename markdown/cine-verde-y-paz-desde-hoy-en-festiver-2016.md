Title: Cine verde y Paz desde hoy en Festiver 2016
Date: 2016-09-21 13:20
Author: AdminContagio
Category: 24 Cuadros
Tags: Ambiente, Cine, colombia, Festival cine Barichara, Festiver 2016
Slug: cine-verde-y-paz-desde-hoy-en-festiver-2016
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/09/13620736_1243083945704101_2474754077787319333_n-e1474481375158.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### Foto: Afiche Festiver 2016 

##### 21 Sep 2016 

Por sexta ocasión Barichara, municipio de Santander, recibe al **Festival de Cine Verde FESTIVER**, uno de los eventos cinematográficos más representativos para el audiovisual con temática ambienta en Latinoamérica, que para la presente edición estará enfocado en [La Paz y el cambio climático](https://archivo.contagioradio.com/el-cambio-climatico-tema-central-de-festiver-2016/).

Desde el 21 hasta el 25 de Septiembre, en FESTIVER, dirigido y producido por Toto Vega y Nórida Rodríguez, se exhibirán cerca de [80 piezas audiovisuales](http://festiver.org/historia/2016/ProgramaMano-2016.pdf), entre cortometrajes, documentales y largometrajes; se realizarán nueve talleres sobre producción de cine y medio ambiente; ocho conferencias y clases magistrales, dos exposiciones y un mercado verde.

Este año el festival también entregará el premio “Fotosíntesis: Apoyo a la postproducción al documental “**De a Caballo**” de la colombiana **Talía Osorio**, y con motivo de los 30 años de la Patrimonio Fílmico Colombiano, la película de apertura, será el cortometraje nacional "**Salvemos Nuestra Tierra**", realizada en 1947 por **Jorge Infante**, considerada la primera película verde colombiana y una joya que ha sido restaurada por la fundación.

Además de las películas en competencia, este año el festival exhibirá una selección de 15 producciones invitadas entre las que se destacan; **Terra y HUMAN** del director francés Yan Arthus-Bertrand; **Mañana** de las directoras francesas Mélanie Laurent y Ciril Dion; **La Ciénaga** del director colombiano Manolo Cruz.

<iframe src="https://www.youtube.com/embed/7bwEzTbTVm8" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

FESTIVER, es un festival que busca concienciar y sensibilizar sobre el medio ambiente y el cuidado de los recursos naturales a través del cine y diversos medios audiovisuales. Toda la programación será de entrada gratuita para los asistentes con inscripción previa en la página del festival www.festiver.org
