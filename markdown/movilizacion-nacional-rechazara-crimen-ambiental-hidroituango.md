Title: Con velatón, rechazarán "crimen ambiental" de Hidroituango
Date: 2019-02-13 11:58
Author: AdminContagio
Category: Ambiente, Nacional
Tags: Antoquia, Empresas Públicas de Medellín, Hidroituango, movilización nacional, Movimiento Ríos Vivos
Slug: movilizacion-nacional-rechazara-crimen-ambiental-hidroituango
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/02/Captura-de-pantalla-19-770x400.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Desde Abajo] 

###### [13 Feb 2019] 

Tras la crisis ambiental que ocasionó Hidroituango la semana pasada que afectó el segundo río más importante del país y 135 mil antioqueños, el Movimiento Ríos Vivos Antioquia convocó una movilización nacional para el 14 de febrero que exigirá el desmonte de este megaproyecto y sanciones para los responsables de este "crimen ambiental".

[En un comunicado](https://riosvivosantioquia.org/llamamiento-nacional-por-la-liberacion-del-rio-cauca-en-colombia/), la organización ambiental llamó la atención a las fallas en los esfuerzos de resiembra de especies que plantea EPM en las próximas semanas tras la muerte de peces, invertebrados, reptiles y otros animales nativos a las zonas aledañas del río. "Si no se hizo la debida protección genética de las especies y de los micro organismos propios del río, **el repoblamiento en las condiciones actuales no tiene sentido e implica una nueva mortandad de especies**," explicó el colectivo.

Frente estos hechos, las comunidades, junto a las organizaciones ambientales, exigen que las autoridades competentes liberen las aguas contenidas en el embalse; convoquen un espacio de dialogo para la resolución y reparación de las problemáticas y daños ocasionado por Hidroituango; y publiquen el informe sobre la viabilidad del proyecto, realizado por organismos independientes.

El encuentro nacional se cita en las siguientes ciudades:

**Bogotá**: 12:00 pm en frente del Ministerio de Ambiente

**Medellín**: 4:00 pm en el Parque de los Pies Descalzos

**Cali**: 9:00 am en el Parque de las Banderas

**Bucaramanga**: 5:00 pm en el Parque García Rovira

**Palmira**: 5:00 pm en el Parque de la Factoría

**Pereira**: 6:00 pm en la Plaza de Bolivar

**Neiva**: 8:00 am en el Parque Leesburg

**San Juan de Pasto**: 5:00 pm en el Parque de Nariño

**Caloto**: 5:00 pm en el Parque Lineal de "Río Grande"

**Ibagué**: 6:00 pm en el Parque Murillo Toro

**Cumbal Nariño**: 6:00 pm en la Plaza Principal

**Villavicencio**: 7:00 pm en el Parque Central Los Libertadores

**Barrancabermeja**: 6:00 pm en el Parque Camilo Torres

**Fusagasugá, Cundinamarca**: 5:30 pm en la Plaza - Parque Principal

**Puente Iglesias, entre Jericó y Fredonia, Antioquia**: 17 de febrero, 11 am

<p style="text-align: justify;">
<script src="https://platform.twitter.com/widgets.js" async charset="utf-8"></script>
</p>
###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
