Title: “Yo sé que los candidatos de Uribe son la peor opción, por eso los apoyo”
Date: 2015-10-16 16:59
Category: Opinion, Schnaida
Tags: Alvaro Uribe, Centro Democrático, elecciones colombia, uribe
Slug: yo-se-que-los-candidatos-de-uribe-son-la-peor-opcion-por-eso-los-apoyo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/10/centro_democratico_20.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto:ElUniversal 

**[Ian Schnaida](https://archivo.contagioradio.com/ian-schnaida/) - [~~@~~IanSchnaida](https://twitter.com/IanSchnaida){.twitter-atreply .pretty-link} **

###### [16 de Oct 2015] 

Recorriendo un hermoso municipio de Antioquia me encontré con un señor bien pintoresco. Mientras hablábamos sobre la actualidad nacional, noté que estaba bien informado y que tenía posiciones claras frente a lo que transita el país.

Después de escucharlo quejarse por la corrupción, el drama de la salud, el clientelismo, el caudillismo y la pobreza extrema, entre otro males que afrontamos, me dijo: “yo sé que los candidatos de Uribe son la peor opción, por eso los apoyo; ya esto está jodido hace rato, entonces mejor ayudar a que se termine de joder de una vez, porque ya no alcanza pa’ más”.

Al principio me pareció totalmente egoísta su posición, pues conociendo los antecedentes del uribismo, es una infamia apoyar sus patrañas; luego me dijo: “no me juzgue, yo soy un apátrida que ama su país pero sabe que no tiene arreglo por ningún lado. El político que sirve lo matan, el periodista que denuncia lo desaparecen y el ciudadano que piensa lo anulan”.

Pese a su razonamiento, no se justifica el derrotismo en el que no sólo él sino muchas personas asumen la situación política del país. Y ni hablar de los que dicen que ya vivieron lo suyo y no les importa que el país se perratee para los que vienen.

Actitudes como estas sólo contribuyen a seguir criando generaciones apáticas a la política y al funcionamiento democrático, porque crecen pensando que todo está perdido y un voto más o un voto menos no sirven de nada.

¿Entonces nos quedamos tranquilos ganando menos de lo que necesitamos para vivir decentemente mientras los políticos corruptos se reparten el patrimonio nacional y enriquecen sus descendentes porque la familia es lo primero?

¿O nos seguimos muriendo en las filas de las EPS o esperando procedimientos mientras la esposa del exmandatario prácticamente es dueña del negocio de la salud en Colombia?

Quizá muchos ya no quieran escuchar denuncias ni alegatos contra Uribe y sus candidatos, porque otra vez, porque tan mamertos, izmierdosos o demás; pero seguimos haciéndolo porque aún hay millones de personas ciegas frente a una realidad que pesa más que los muertos que cobra día a día.

Hay que dejar de lado el poder intimidarorio de las fuerzas al margen de la ley y los cardúmenes uribistas que saltan en manada a atacar cualquier persona que se digne a juzgar a su altísimo; incluso a través de cuentas de Twitter y Facebook falsas que tienen 15 identidades distintas pero un solo administrador, porque en manada se intimida más, así sea mentira, porque ni nombres reales usan.

Los malos son bastantes, sí, pero no tantos, solo que la plata y la publicidad bien ejecutada pueden manipular a un país para que tome las decisiones equivocadas; pero tenemos que entender que ya no estamos bajo el yugo monopolizador de los grandes medios de desinformación nacional, las redes y los nuevos medios han ayudado a democratizar el ejercicio político, porque un ciudadano del común puede denunciar públicamente los delitos que conoce o de los cuales es víctima.

Cuesta creer que la sociedad aún no es capaz de identificar a los candidatos que sólo están a la sombra de un caudillo y no tienen ni ideas propias ni identidad, porque sólo existen a través de la representación de su jefecito.

Sigamos trabajando en construir el país que queremos y erradicar los políticos que buscan enriquecerse a costa del erario público, para envejecer podridos en dinero y ostentosos bienes mientras las personas que votaron por ellos no alcanzan siquiera a envejecer o a vivir dignamente, porque de creer en promesas de mala procedencia se ha muerto y se va a morir mucha más gente.

¿Por qué seguir votando por los mismos candidatos que se hacen reelegir una y otra vez si usted nunca ve los resultados positivos que debería dejar su trabajo?

No más votos por espectros políticos o títeres que no pueden hablar con ideas propias; si queremos mejorar el momento es ahora y la responsabilidad de todos.
