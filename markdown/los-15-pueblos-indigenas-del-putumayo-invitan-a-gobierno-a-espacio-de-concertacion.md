Title: Los 15 pueblos indígenas del Putumayo invitan a gobierno a espacio de concertación
Date: 2018-04-06 17:36
Category: Nacional
Tags: Gobierno, indígenas, Putumayo
Slug: los-15-pueblos-indigenas-del-putumayo-invitan-a-gobierno-a-espacio-de-concertacion
Status: published

###### [Foto: Universal.com] 

###### [06 Abr 2018] 

Los 15 pueblos indígenas del departamento de Putumayo, representadas por sus autoridades, decidieron conformar un espacio de interlocución y relacionamiento con las entidades de gobierno nacional, regional y local, p**ara construir un protocolo y tratar temas sobre consultas y consentimientos previos.**

Con esta acción, los pueblos indígenas buscan que todos los procesos de consulta previa, que se pretenden adelantar en el territorio, sean conocidos a través de la construcción de ese protocolo. En ese sentido construyeron una agenda que consta de **4 puntos prioritarios de discusión y concertación** que deben respetar el derecho de participación de las comunidades.

### **La agenda de los pueblos indígenas del Putumayo** 

El primer punto tiene que ver con **la protección del derecho al territorio ancestral y tradicional de los pueblos,** de acuerdo con lo establecido en el convenio 169 de la OIT en la definición de las políticas sectoriales y medidas administrativas en el Putumayo.

El segundo punto se relaciona con la política minero energética del departamento, para que en primera instancia se respete el **derecho al territorio de los pueblos indígenas como espacio vital de vida y pervivencia de la diversidad cultural**. Esto teniendo en cuenta que 13 de las 15 comunidades que habitan en el Putumayo, están reconocidas dentro de los pueblos en riesgo de exterminio físico y cultural.

El tercer punto consiste en el **Capítulo Étnico e implementación de los Acuerdos de Paz**, puntualmente en los temas de sustitución de cultivos de uso ilícitos y el derecho de los pueblos a decidir sobre los usos y propuestas de desarrollo propio en sus territorios.

Para cerrar la lista se encuentra el **cumplimiento del compromiso del gobierno con temas tratados desde la sentencia T-025 de 2004 y el Auto 004 de 2009**, en donde como tema prioritario proponen tratar el Decreto ley de Víctimas 4633 de 2011.

En esta agenda también serán convocados como garantes del dialogo las Naciones Unidas, la Defensoría del Pueblo Regional y Nacional delegadas de derechos colectivos y del ambiente, para las comunidades indígenas y las minorías étnicas. (Le puede interesar: ["Pueblo Wayúu a punto de desaparecer por incumplimiento estatal"](https://archivo.contagioradio.com/pueblo-wayuu-al-borde-del-exterminio-por-incumplimiento-estatal/))

###### Reciba toda la información de Contagio Radio en [[su correo]
