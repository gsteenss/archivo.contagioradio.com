Title: Misión Humanitaria verificará crisis de derechos humanos en el Naya
Date: 2018-05-09 09:51
Category: DDHH, Movilización
Tags: conflicto armado, lideres sociales, Misión Humanitaria, Naya
Slug: mision-humanitaria-verificara-crisis-de-derechos-humanos-en-el-naya
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/naya_contagioradio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Jezera.org] 

###### [09 May 2018] 

Una misión humanitaria conformada por diferentes organizaciones defensoras de derechos humanos, ingresará durante 4 días al territorio del Naya, para constatar las violaciones a derechos humanos que **se han denunciado en las diferentes comunidades y hacer un llamado a las instituciones gubernamentales para que se tomen medidas urgentes sobre la grave situación**.

El recorrido de la misión iniciará este 11 de mayo y transitará por las diferentes comunidades que se encuentran en el borde del **río Naya y finalizará con una asamblea extraordinaria por la vida en Puerto Merizalde**.

El hecho más reciente de agresión hacia las comunidades, se presentó el pasado fin de semana, cuando el líder social de las comunidades del Naya, Iver Angulo Zamora, fue **retenido por hombres armados** a pesar de que se movilizaba protegido por una misión humanitaria de la que hacían parte la Defensoría del Pueblo y algunos integrantes de la comunidad. (Le puede interesar: ["Crítica situación de derechos humanos en el Naya"](https://archivo.contagioradio.com/critica-situacion-de-derechos-humanos-en-el-naya/))

### **Las comunidades exigen la implementación de los Acuerdos de Paz** 

Con la misión humanitaria, las comunidades también pretenden evidenciar la necesidad y urgencia porque se implementen los Acuerdos de Paz, en esa medida están exigiendo que existan condiciones para la reincorporación de los integrantes de la FARC, el cumplimiento de los planes de sustitución de cultivos de uso ilícito y la ejecución de los mismos, la vinculación del consejo comunitario a los PDET y el desmonte de las estrucutras armadas en el Naya.

De igual forma las comunidades también exhortan al gobierno a que **cumpla con la sentencia de la CIDH que le otorgó medias cautelares a los habitantes del Naya**, que active el mecanismo de búsqueda de personas desaparecidas y convocan a la Comisión de Seguimiento,Impulso y Verificación a la implementación, a encontrarse con ellos en el territorio o en Buenaventura.

<iframe id="audio_25883612" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_25883612_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]

 
