Title: Una cortina de humo llamada psicogenia
Date: 2015-10-30 10:18
Category: Mar Candela, Opinion
Tags: Carmen de Bolívar, Derecho a la salud, Ministerio de Salud, psicogenia masiva, Vacuna contra el Papiloma Humano, Vacunas en Colombia
Slug: una-cortina-de-humo-llamada-psicogenia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/10/vacuna-papiloma.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

#### **[Por [Mar Candela  Castilla ](https://archivo.contagioradio.com/mar-candela/)]** 

###### [30 Oct 2015] 

A Mediados de mayo del 2014, las noticias nacionales informan que cientos de niñas en el Carmen de Bolívar se desmayan sufren convulsiones y que sus padres denuncian a voz en cuello que todos estos síntomas  se develaron  justo después de la Vacuna contra el Papiloma Humano, cada día las noticias son más alarmantes y el número de familias denunciando que sus niñas están afectadas por esta vacuna crece, los padres del Carmen de Bolívar exigen una explicación al estado de salud de sus hijas, hacen manifestaciones públicas, reclaman la atención en salud para sus hijas; El gobierno nacional en cabeza del presidente de la república Juan Manuel Santos manifestó que se trata de manipulación por parte de abogados que tienen como objetivo lucrarse con el dolor ajeno, afirma que crearon grupos de investigación psicológica para determinar qué sucede con las niñas porque  parecen extrañas las reacciones dadas en sus cuerpos.  Aclaremos el panorama: Son reacciones extrañas a la gestión de las instituciones públicas colombianas” el Instituto Nacional de Salud. Emite su concepto, afirmando:

“Se trata de un psicogenia masiva, histeria colectiva” y pegándose al argumento más cliché  señala: “Todas estas niñas debido a su ignorancia y condición social se han sugestionado de tal forma que sufren enfermedades que solo existen en su mente”; con cortinas de humo como la del clasismo  y  el señalamiento de algún tipo de “trastorno mental” convencieron a todo un  país  de que  la vacuna  no tenía nada que ver con los hechos.  Miles de niñas siguen siendo vacunadas sin tener en cuenta que todos los organismos son diferentes y que una vacuna debería ser testeada  en los cuerpos antes de ser aplicada.

Mientras en Colombia el Estado persiste en defender esta vacuna a precio de la dignidad y las vidas de las denunciantes. El panorama es totalmente diferente en Estados Unidos, Canadá, México, España, Bogotá , Medellín, Bucaramanga, Cúcuta, Sincelejo, la misma historia fue sufrida desde muchos meses antes del escándalo del Carmen de Bolívar por madres que veían como la salud de sus hijas se deterioraba paulatinamente desde la aplicación de la vacuna contra el papiloma, sus denuncias eran idénticas; enfermedades autoinmunes, neuropatías, enfermedades de la sangre, infecciones; las desesperadas madres lo decían a las autoridades médicas y no obtenían respuesta alguna. Temerosas pero valientes al informar sobre el estado de salud de sus hijas obteniendo como respuesta:

“La efectividad de la vacuna está ampliamente demostrada” – “deje de estar mirando internet está sugestionando a su hija”, “la niña está somatizando” o el más cruel de los argumentos  que acude al clasismo  “seguramente viene de un hogar disfuncional”.

La historia se repite una y otra vez; en la actualidad más de 600 menores y adultas de Colombia sufren los daños causados por la vacuna del papiloma humano y luchan por que el gobierno nacional reconozca los efectos adversos y proceda a su estudio de manera honesta y  para lograr este objetivo crearon la asociación Reconstruyendo esperanza, en cabeza de la abogada Mónica León Del Río cuya hija es una víctima más de la vacuna tan laureada. La asociación en la actualidad cuenta con un reporte de historias clínicas documentadas. Alrededor de cuatrocientas víctimas y cuatro muertes denunciadas como consecuencia de la aplicación de la vacuna contra el VPH. Estas madres colombianas  no son antivacunas,  de hecho se  vacunaron  voluntariamente y siempre se han vacunado, no son extremistas religiosas y mucho menos enemigas de los derechos sexuales de las mujeres  como lo afirman algunos movimientos  feministas  y el  ministro de salud, que lejos de garantizar el bienestar y la salud de las niñas y mujeres colombianas, las estigmatiza hasta el punto de burlarse de ellas públicamente como lo ha hecho en su blog y twitter.

Cuestiono: ¿Hasta cuándo la injusticia y la negligencia por parte del gobierno nacional va a seguir cobrando las vidas de cientos y, posiblemente a futuro, miles de mujeres a quienes se les siga aplicando la tan recomendada vacuna contra el virus del papiloma humano?

Pueden acusarme de insensata le creo a las denunciantes esta vacuna  está dejando victimas que el estado ignora. Humilla y burla.

Soy madre Feminista .No soy anti vacunas tampoco próvida. Y decidí no vacunar a mi hija contra el Papiloma Humano.

@[femi\_artesanal]{.u-linkComplex-target}
