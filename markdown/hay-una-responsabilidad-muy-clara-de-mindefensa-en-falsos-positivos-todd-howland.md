Title: “Hay una responsabilidad muy clara de MinDefensa en Falsos Positivos” Todd Howland
Date: 2015-03-17 16:32
Author: CtgAdm
Category: DDHH, Entrevistas
Tags: alto comisionado, defensa, falsos positivos, ONU, todd howland
Slug: hay-una-responsabilidad-muy-clara-de-mindefensa-en-falsos-positivos-todd-howland
Status: published

###### Foto: Contagio Radio 

<iframe src="http://www.ivoox.com/player_ek_4226946_2_1.html?data=lZefmJ6Yeo6ZmKiakpyJd6Kll5KSmaiRdo6ZmKiakpKJe6ShkZKSmaiRaaamhp2dh56njMLtjNrbw5DWqdTk0NPgw8fNsMrYwsmYz9rdb8TgwtfOjcnJb67dz6nSyMrSt8KfxtOYqJKJe6ShpNTb1sbLrdCfs8bRy9SPcYarpJKh&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Todd Howland, Alto Comisionado de las Naciones Unidas para los DDHH en Colombia] 

Durante la presentación del Informe anual del Alto Comisionado de las **Naciones Unidad para los Derechos Humanos en Colombia, Todd Howland** señaló que entre las violaciones a los DDHH cometidas por el Estado, siguen preocupando las ejecuciones extrajudiciales, más conocidas como “falsos positivos”.

A la oficina del Alto Comisionado le preocupa que el Ejercito siga excusando estos crímenes como **errores militares**, sin reconocer su responsabilidad, y –por el contrario- promoviendo reformas al fuero penal militar (como las 5 reformas propuestas por el actual Min Defensa Juan Carlos Pinzón) para cobijar con impunidad los más de 800 procesos judiciales que se adelantan contra mandos militares.

Entre el 2001 y el 2011, el Ministerio de Defensa registró más de **16 mil bajas en combate. Alrededor del 20% de esos registros se encuentra condenados o en investigación por responder a “falsos positivos”.** El alto Comisionado considera que en Colombia, puede haber actualmente más de 5 mil víctimas por homicidios extrajudiciales por parte de las fuerzas militares.

El Ejercito no puede, para Howland, continuar argumentando que estos errores se presentaron por omisión de las autoridades y mandos de la institución, ya que, como sentencia el artículo 28 de la CPI, la **omisión en violación a los Derechos Humanos es un crimen internacional.**

“Cuando hay tantos casos que son cuestionables, los altos mandos deben saber que algo está pasando. **Cuando algo pasa por debajo de su mando, usted tiene la responsabilidad,** tiene que saber que está pasando, o tiene que tomas medidas para saber. No puede escudarse en la omisión cuando son tantos casos”, declaró Howland.

En este sentido, para el alto comisionado, la responsabilidad es del **Ministro de Defensa, pero también del poder Ejecutivo y el Legislativo**. “Del 2001 al 2011 los impuestos de los colombianos estaban pagando a personas que estaban matando civiles".

“En razón de un MinDefensa, él tiene la responsabilidad para actuar para evitar o prevenir estas acciones. Hay una responsabilidad muy clara del MinDefensa y del Estado en su conjunto de ejecuciones extrajudiciales.”, concluyó.

#### Gráfica: Comparativa de muertes en combate y presuntos casos de ejecuciones extrajudiciales 

\[caption id="attachment\_6034" align="aligncenter" width="566"\][![Grafica ONU](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/03/Grafica-ONU-764x1024.jpg){.wp-image-6034 width="566" height="759"}](https://archivo.contagioradio.com/ddhh/hay-una-responsabilidad-muy-clara-de-mindefensa-en-falsos-positivos-todd-howland/attachment/grafica-onu/) Grafica ONU\[/caption\]
