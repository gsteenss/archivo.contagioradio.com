Title: Estado debe reconocer su responsabilidad y dar la cara a Bojayá y al mundo
Date: 2019-05-10 18:43
Author: CtgAdm
Category: Comunidad, Judicial
Tags: Chocó, condena a la nacion, Masacre de Bojayá, Tribunal administativo del Chocó
Slug: estado-debe-reconocer-su-responsabilidad-y-dar-la-cara-a-bojaya-y-al-mundo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/10/bojaya-entrega-Cristo-Negro-117.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:] 

**El Tribunal administrativo del Chocó** ratificó el fallo que condena al Estado a pagar 312.564 millones de pesos, destinados a reparar a las víctimas de la masacre y los desplazamientos masivos ocurridos en Bojayá en 2002, por no implementar las medidas de protección para proteger a la población. A pesar que la comunidad valora el fallo, señalan sus habitantes que fue emitido con tardanza pues han tenido que soportar los efectos de la guerra a lo largo de los últimos 17 años.

**Leyner Palacios, sobreviviente de la masacre de Bojayá e integrante de la Comisión Interétnica de la Verdad** indica que como consecuencia de la "omisión del Estado se posibilitó el accionar de las Farc y de los paramilitares" ocasionando un desplazamiento de más de 9.000 personas de la región del Atrato y la pérdida de sus cultivos, su cotidianidad y sus actividades productivas, sucesos que aún hoy tienen un gran impacto en la población civil.

"Hemos sido insistentes en  que el Estado debe reconocer su responsabilidad en esta omisión y dar la cara a Bojayá y al mundo" afirma el líder señalando que si bien el tribunal atribuye esta responsabilidad a la nación, les preocupa que 17 años después no haya sido eficiente en procesos judiciales o condenas contundentes frente a las personas involucradas.

### Bojayá, un pueblo que le ha apostado a la paz 

Leyner destaca que 17 años después de la tragedia, la población continúe resistiendo, reflexionando y haya asumido una posición frente a la consecución de la paz, **"hemos venido recuperando el tejido social de la comunidad, generando y espacios de encuentro, esperamos que la reparación integral sea materializada"**, afirma.

A su vez, el líder señala que a lo largo del tiempo han logrado recuperar los cuerpos de personas quienes están en medicinal legal para en un lapso de dos meses poder sepultarlos y "por fin hacer el velorio que tanto hemos querido realizar, llorar nuestros muertos y entregarlos al dios de la vida". [(Le puede interesar: Los saldos pendientes con las víctimas de la Masacre de Bojayá](https://archivo.contagioradio.com/los-saldos-pendientes-con-las-victimas-de-bojaya/))

Sin embargo, advierte que actualmente, Bojayá está confinada por grupos paramilitares, una situación que "ratifica la desprotección que padecimos en 2002, vemos con angustia que la respuesta del Estado sigue siendo precaria, los líderes que denuncian siguen siendo intimidados y la gente ha perdido la autonomía" concluyendo que en cuestión de seguridad los cambios en el territorio han sido mínimos por lo que esperan, el Gobierno cumpla con la orden del Tribunal administrativo y no dilate más la posibilidad de que las víctimas accedan a sus derechos.

<iframe id="audio_35662394" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_35662394_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
