Title: ¿Existe fracking en Boyacá?
Date: 2018-06-21 17:56
Category: Ambiente, Movilización
Tags: Ambiente, Boyacá, consulta popular, fracking
Slug: existe-fracking-en-boyaca
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/06/fracking-en-boyaca.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo Contagio Radio] 

###### [19 Jun 2018] 

La declaración del gobernador de Boyacá, Carlos Amaya, en la que señala que "nunca permitirá fracking en el departamento", se produce luego de que la Agencia Nacional de Hidrocarburos (ANH) otorgara permiso para hacer estudios de sísmica en el la región, dejando de lado incluso la consulta previa a la comunidad. A través de su cuenta de Twitter, el mandatario solicitó al Gobierno Nacional realizar una reunión con las comunidades además de la suspensión del permiso.

Así mismo, ante la discusión en redes sociales sobre el ‘fracturamiento hidráulico’ que tuvo lugar en días recientes por la aparición de unas fotografías en las que se aprecia maquinaria minera cerca de la Laguna de Tota, la Gobernación de Boyacá salió al paso con un [comunicado](http://www.boyaca.gov.co/prensa-publicaciones/noticias/25830-gobierno-de-boyac%C3%A1-solicita-suspensi%C3%B3n-de-s%C3%ADsmica-y-audiencia-p%C3%BAblica-con-presencia-de-anh) en el que aclara que “hoy en día no se está presentando ningún proceso de explotación petrolera mediante ‘fracking’ en el departamento”.

### [**Boyacá tuvo estudios para exploración de Yacimientos No Convencionales**] 

Aunque la alerta del gobernador Carlos Amaya sobre los estudios de sísmica se enfoca en exploraciones de tipo tradicional, en Boyacá hay zonas que cuentan con potencial viabilidad para Yacimientos No Convencionales (YNC), por parte de la Agencia Nacional de Hidrocarburos.

De hecho, el Ministerio de Ambiente otorgó una licencia que permite la exploración de pozos y hacer uso del método de fracturación hidráulica, mediante la [resolución 1734 del 2011](https://es.scribd.com/document/382298738/Resolucion-1734-del-26-de-Agosto-del-2011). En dicha ocasión, la licencia se otorgó a la empresa Nexen Petroleum Colombia Limited para la realización de estudios de sísmica 2 D, con el fin de “evaluar la potencialidad y la prosperabilidad de los hidrocarburos considerados como yacimientos no convencionales”, estudios que tuvieron lugar en el noroccidente del departamento de Cundinamarca, en el denominado "Bloque Chiquinquirá".

![Captura de pantalla 2018-06-21 a la(s) 11.40.22 a.m.](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/06/Captura-de-pantalla-2018-06-21-a-las-11.40.22-a.m.-645x580.png){.alignnone .size-medium .wp-image-54116 width="645" height="580"}

###### [Foto: Resolución 1734 del 2011.] 

Sin embargo, Tatiana Mosquera Ferro, integrante del colectivo No Exploraciones de Chiquinquirá,  recordó que desde el 2016 la ANH viene realizando estos estudios, y pretende llevarlos a cabo en los municipios de Chiquinquirá, Caldas, San Miguel de Sema, Saboyá, Buena Vista, Pauna, Ráquira, Tinjacá, Cucaita, Samacá, Sáchica, Maripí y Sutamarchán en Boyacá; y en el municipio de Simijacá en Cundinamarca.

### **Hay riesgos inminentes para municipios del altiplano Cundiboyacense** 

Mosquera señaló que los estudios de sísmica 2 D sirven "para determinar exactamente la coordenada y la capacidad de los pozos", y lo que ocurre inmediatamente después es la realización de sísmica 3D, que son estudios mucho más exactos, "pero requieren de perforaciones y el uso de dinamita".

Adicionalmente, afirmó que no están de acuerdo con la realización de los estudios de sísmica, puesto que la zona posee una gran cantidad de acuíferos subterráneos que se verán afectados con cualquier tipo de perforación. De igual modo, por la vocación agraria de la región, los agricultores se verían afectados si se perjudican estos acuíferos, agregando que existe una zona de páramo compartida por algunos de estos municipios y cualquier intervención podría afectar dicho ecosistema.

Sobre las afectaciones del ‘fracking’, Colombia tiene la experiencia cercana del Municipio de San Martín en la Vereda Pita Limón, Cesar, en la que hay presunta contaminación del agua luego de empezar la primera fase de exploración de YNC. Allí los habitantes vienen denunciando que, tras esta primera fase, apareció una mancha de aceite en el pozo del cual toman agua para su consumo. (Le puede interesar: ["En San Martín se contaminó el agua luego del inicio del Frackling"](https://archivo.contagioradio.com/en-san-martin-se-contamino-el-agua-luego-del-inicio-del-fracking/))

###### Reciba toda la información de Contagio Radio en [[su correo]
