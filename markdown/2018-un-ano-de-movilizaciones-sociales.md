Title: 2018: Un año de movilizaciones sociales
Date: 2018-12-23 11:55
Author: Censat
Category: CENSAT, Opinion
Tags: Ambientalismo, colombia, extractivismo, Movilización, Movilización estudiantil, Movilización social
Slug: 2018-un-ano-de-movilizaciones-sociales
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/12/movilización-censat-770x400.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Censat 

##### **[Por: CENSAT Agua Viva - Amigos de la Tierra – Colombia](https://archivo.contagioradio.com/censat-agua-viva/)** 

##### 23 Dic 2018 

El 2018 terminó con una  representativa y creativa movilización del pueblo y la sociedad colombiana en torno a la defensa o incidencia sobre cuestiones esenciales para el país, incluida la cuestión ambiental.

El país vive una situación aciaga, expresada en cambios institucionales y políticos; vulneración de derechos; crímenes ambientales y sus impactos, aún en la impunidad; entre una larga lista. Pero el lugar común que nos habla de los momentos de crisis como una oportunidad, da pie para pensar y aprender de lo que ha sucedido este año en Colombia.

Aunque importantes sectores de la sociedad cifraban expectativas de que se concretaran cambios estructurales mediante la implementación de los acuerdos firmados y los diálogos en ciernes, se revelaron nuevas formas de exacerbación del conflicto. La agudización de la criminalización y violencia contra pueblos indígenas y otras comunidades, luchadoras y luchadores ambientales y por los derechos humanos, es quizás la expresión más patente. Pero desde otra perspectiva, la situación ha sido exasperada por la expansión de la frontera extractivista y la privatización de las aguas, entre otros:  mientras que algunas medidas y proyectos para antiguas zonas en guerra, específicamente las referidas a la economía verde, prometen bienestar para las poblaciones y protección/recuperación del patrimonio ambiental, pero nos depara a asistir a las más altas tasas de deforestación en años recientes, cerca de 220.000 ha en 2017, concentradas en la Amazonia, más del 65%, con valores proyectados superiores para 2018.

[Las políticas y respuestas de los Gobiernos que han transitado en este año, reflejan una distancia abismal entre las necesidades y aspiraciones del conjunto de la sociedad. Basta con rememorar la respuesta del gobierno Santos frente a las desastres ambientales generados por el derrame en Campo Lizama o la construcción de Hidroituango; frente al asesinato sistemático de defensoras y defensores de los territorios y frente a la problemática de los cultivos de uso ilícito; y más recientemente las medidas del nuevo gobierno respecto a las situaciones mencionadas y a las necesidades apremiantes de inversión social, específicamente para el financiamiento de la educación pública.]

De cara a dicho panorama las evaluaciones tienen un común denominador negativo. No obstante, queremos hacer énfasis en la forma en que la mayor parte de la sociedad colombiana ha dado respuesta. Resulta difícil traer a la memoria el nivel de movilización social, , al que hemos asistido este año: la solidaridad, sensibilidad y acción que se ha desencadenado; bien desde distintas orillas, opciones de pensamiento u organización, y propiciando la articulación.

La participación de los individuos y colectivos en el ejercicio de la política está aumentando, más allá de entenderla en términos electorales. Millones de colombianas y colombianos han participado de las consultas populares alrededor del país, para decidir acerca de sus derechos y territorios, participación que no debe ser entendida meramente en la asistencia a las urnas, sino en la movilización, el diálogo puerta a puerta, en la veredas y montañas. Participación que no se agota en los espacios propiciados o negados por el gobierno, como lo demuestran la consulta popular y legítima de San Lorenzo Nariño o los acuerdos municipales participativos que se están gestando en regiones como el Cauca y el suroeste de Antioquia. De otro lado, el resultado de la segunda vuelta en las elecciones presidenciales, dejó ver claramente la aspiración de cambios políticos en el país por parte de más de 8 millones de personas.

[La resistencia y movilización no ha estado restringida al rechazo o la contención, sino que se ha re-creado, siendo plena de contenidos, propuestas y soluciones. La acción frente al modelo político, económico, educativo y cultural ha encontrado nuevos significados y formas de ser y transformar.]

[Frente al extractivismo y su matriz energética los movimientos sociales han  posicionado una propuesta clara de transición energética y pos-extractiva, construida entre diversos sectores, incluidos los y las trabajadores/as. Así, se está abordando la cuestión del manejo, la soberanía, la investigación y el acceso equitativo; recogiendo procesos de décadas de construcción conjunta. Probablemente quien está leyendo esta editorial, también habrá leído o respaldado alguna de las reflexiones o acciones para detener el fracking en el país, que en 2018 se ha consolidado con propuestas proactivas y de transformación.]

[Los y las luchadoras y luchadores han sido blanco de violencias y amenazas desmesuradas; pero inconmensurable también ha sido la respuesta. Cada una de ellas y de ellos está más presente, rodeado, valorado; nutren los ejercicios de memoria, incluida los de memoria ambiental, que vienen creciendo en múltiples territorios, como forma de repensar la vida en los mismos.]

De otro lado, es necesario remembrar el cisma que el movimiento estudiantil ha liderado, propiciando la participación y ejercicio político de muchos otros individuos y sectores que les respaldan y hacen que crezca la resistencia.

[Esta otra evaluación, tiene como objeto tener presentes los motivos, procesos, pueblos y comunidades que nos alientan e inspiran a trabajar por la transformación inapelable en este país; que, si bien no es un cometido sencillo, es posible cuando construimos y jalonamos con otros y otras por encima de las diferencias, que más que nunca deben juntarnos.]

#### [**Leer más columnas de CENSAT Agua Viva - Amigos de la Tierra – Colombia**](https://archivo.contagioradio.com/censat-agua-viva/)

------------------------------------------------------------------------

###### Las opiniones expresadas en este artículo son de exclusiva responsabilidad del autor y no necesariamente representan la opinión de Contagio Radio.
