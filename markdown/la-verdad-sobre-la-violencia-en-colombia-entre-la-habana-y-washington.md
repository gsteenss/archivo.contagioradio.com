Title: La verdad sobre la violencia en Colombia, entre La Habana y Washington
Date: 2015-02-16 19:39
Author: CtgAdm
Category: Camilo, Opinion
Tags: comision de la verdad colombia, informe basta ya, proceso de paz, violencia historica colombia
Slug: la-verdad-sobre-la-violencia-en-colombia-entre-la-habana-y-washington
Status: published

Por **[[Camilo De Las Casas](https://archivo.contagioradio.com/camilo-de-las-casas/)]**

Un paso hacia la ruptura con la verdad oficial, en un escenario aislado, La Habana, ha sido la presentación del Informe de los 12 de la Comisión de Expertos sobre el origen y responsables del conflicto armado. Simultáneamente, un aislamiento con sus propias características vivido por el Centro Democrático en cabeza del dueño del Ubérrimo en los Estados Unidos, sugiere la tensión en el establecimiento que no logra consensuar del todo como pactar la solución del conflicto armado con las FARC EP y el ELN.

Lo innegable es que por lo menos el Uribismo se vio obligado a hablar de la paz, “pax romana”, como la de Santos y presentar su visión en el centro de las decisiones geopolíticas sobre nuestro país. Mientras en Washington el uribismo alzaba la voz, en Bogotá, el santismouribismo \[Unidad Nacional\] se atornilló a sus propios temores sobredimensionando al uberrista y luego llamándolo a la conciliación. Disensos entre uribistas y santouribistas que no son contradicciones, razón, que explica, una y otra vez, porque Uribe no será investigado y sancionado aún.  Y razón de más para comprender porque mientras el exmandatario hace ruido en el Norte, aquí en el patio de atrás, el Procurador General de la Nación, Ordoñez, plantea un Pacto por la Paz con todos los sectores.

El establecimiento ha ido reconociendo implícitamente que la guerra no es la salida, se han ido quedando sin argumentos sin pretextos. El cese unilateral de las FARC EP, que de alguna manera, está planteado por el ELN, a través de más de 10 cartas a los militares, está colocando al establecimiento en una encrucijada..

El asunto por resolver del establecimiento es la actitud de soberbia de uno de sus expositores, el responsable de la creación del Bloque Metro, para aceptar esa realidad, así como, la legitimación subrepticia de esta valoración por parte de algunos de los 12 expertos del Informe de la Comisión Histórica sobre el origen del conflicto. Lograr los consensos fundamentales de militares activos y retirados, paramilitares, paraempresarios, parapolíticos, y la sociedad narca, terrateniente, ganadera, la de los agronegocios y de sectores del empresariado moderno de derecha extrema, es el asunto de Santos.

El reto de los demás, que apostamos por otro país, en la construcción de un movimiento convergente por la paz con justicia socio ambiental, es el conocimiento, la divulgación y la reconstrucción de las apuestas de memoria desde lo negado por la verdad oficial  y que hoy se dice de alguna manera en algunos de los documentos de los expertos de la CH

La historia oficial se ha empezado a romper en un escenario institucional como el de las conversaciones hacia la paz.

Sin pretender ser esquemáticos hay dos metarelatos. Unos basados en la tesis de la negación de la modernidad por la imposibilidad de una reforma agraria,  por una política de  exclusión social causada por la clase política y terrateniente, incidida y legitimada desde los Estados Unidos que diseñó modelos de represión, entre ellos, el paramilitarismo, con actores legitimadores como la iglesia católica, entre otros.

Por otro lado, la verdad oficial con dos vertientes. Un metarelato en consonancia con el ¡Basta Ya! de tipo pastiche** **que aparece en algunos de los documentos de la CH con responsabilidades estatales disimuladas, maquilladas o negadas, y otra visión, que sostiene  el afincamiento en las responsabilidades en la izquierda armada alimentada por ideologías comunistas prosoviéticas, prochinas o procubanas, imposibilitando  la democracia, que era cuasi perfecta, que entró en la crisis por la narcoguerrilla y el narcoparamilitarismo, nacido éste, para defenderse del terror de la izquierda. Es la idea de un Estado víctima, o un Estado ajeno, un Estado ausente que quedó a merced del terror de la izquierda y la derechas, por lo que nunca fue posible una sociedad moderna, que incursionó y se mantiene con un ascendiente de cultura mafiosa. La teoría de los “dos demonios”.

Independientemente del consenso entre los expertos, la sociedad puede acceder, no a la verdad de los iluminados ni de algunos intelectuales, que se conciben algunos de ellos como divas, , sino a comprender dos metarelatos qué pretenden explicar desde dos lugares el origen, la incidencia y los efectos de la violencia.  Ya hay otra narrativa abierta, critica enfrente a otro metarelato de verdad institucional en sus dos vertientes.

Aún estamos lejos de lograr consensos de verdad histórica. No se trata de los expertos, se trata de posiciones éticas y del lugar en donde se ubican los diferentes actores sociales, de la apertura a reconocer y a aceptar. España 70 años después del exterminio a los republicanos no logra un consenso de narración histórica, como aquí, que nunca hubo un relato consensuado sobre nuestra llamada “Violencia”.

El campo de batalla de la legitimación de la verdad exige posición en y ante el poder, exige creatividad. En ese escenario de tensión del poder se ha abierto una ventana con documentos de las CH. Las tensiones uribistas y santouribistas se resolverán pues hay consensos de fondo por su posición de poder de clase. Pero del otro lado, aún se requiere eficacia, convergencia, rompimientos dogmáticos y esquemas para que la verdad negada sea audible y fundante de un nuevo país.

La verdad enunciada por las víctimas, y aquellas guardadas por militares de alto rango, no se han acompasado, quieren ser calladas y aisladas. El Informe de la CH es una ruptura con verdades oficiales, es el reto de movimientos de víctimas, de diverso tipo y dirigentes, lograr que esas verdades sean asumidas por la sociedad, mientras es posible una Comisión de la Verdad, La verdad es un desafío para deslegitimar, el uso de la fuerza para proteger y asegurar riqueza y poder político. Ese es nuestro reto ante las responsabilidades en la violencia del Estado, de la iglesia católica, los partidos políticos, empresarios, entre ellas, los mediáticos y gatekeeper.
