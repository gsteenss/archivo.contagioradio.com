Title: Otra Mirada: Hablemos de la Fiscalía
Date: 2020-07-07 18:22
Author: PracticasCR
Category: Otra Mirada, Otra Mirada, Programas
Tags: Fiscal Francisco Barbosa., Fiscalía General de la Nación
Slug: otra-mirada-hablemos-de-la-fiscalia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/06/Barbosa.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: Presidencia

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Desde hace dos meses, la Fiscalía General de la Nación ha sido foco de la discusión por casos tales como la insinuación de que violación a niña Embera podría haber sido consentida por la menor y el más reciente de los casos en que el fiscal general, rompiendo las medidas de aislamiento, habría estado de paseo en San Andrés junto a su familia. Debido a esto y al altísimo nivel de impunidad el país duda de la eficiencia y ética de este ente y de sus representantes.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Analizando cómo está la Fiscalía actualmente y cómo ha sido el trabajo y el proceso del fiscal desde su nombramiento estuvieron Leonardo Díaz, coordinador de protección del programa Somos Defensores, Luz Marina Hache, vocera nacional del Movice, Helena Hernández, abogada penalista de la Universidad del Rosario y Jomary Ortegón, presidenta Cajar. (Le puede interesar: [Hay políticas que propician la criminalidad en las fuerzas militares cceeu](https://archivo.contagioradio.com/hay-politicas-que-propician-la-criminalidad-en-las-fuerzas-militares-cceeu/)) 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Los analistas proporcionaron una visión general del papel de la Fiscalía este año y de la participación del fiscal general en torno a la pandemia y a aspectos como el proceso de acompañamiento para víctimas, la investigación en los crímenes de Estado y las investigaciones por feminicidios en los distintos territorios.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

A su vez, explican cuál ha sido la relación que ha tenido la Fiscalía con las víctimas sobre todo en los periodos entre marzo y julio, periodo de tiempo en que asesinatos y feminicidios han aumentado. De igual manera, expresan qué espera la ciudadanía que haga la Fiscalía después de su deficiente desempeño.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Además, dan a conocer que las víctimas exigen a la fiscalía la no impunidad y le hacen una invitación para que cumpla el papel que tiene que cumplir en investigar y no en ser un espectador más permitiendo que hayan otras entidades u organizaciones haciendo la labor. (Si desea escuchar el programa del 3 de julio: [¿Cómo va la escena cultural en momento del COVID-19?](https://bit.ly/2Dhnez3))

<!-- /wp:paragraph -->

<!-- wp:core-embed/facebook {"url":"https://www.facebook.com/contagioradio/videos/908498579669234","type":"video","providerNameSlug":"facebook","className":""} -->

<figure class="wp-block-embed-facebook wp-block-embed is-type-video is-provider-facebook">
<div class="wp-block-embed__wrapper">

https://www.facebook.com/contagioradio/videos/908498579669234

</div>

</figure>
<!-- /wp:core-embed/facebook -->

<!-- wp:block {"ref":78955} /-->
