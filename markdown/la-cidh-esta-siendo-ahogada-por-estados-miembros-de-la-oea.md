Title: La CIDH "está siendo ahogada por Estados miembros de la OEA"
Date: 2016-05-25 14:59
Category: DDHH, Nacional
Tags: CIDH, crisis financiera, Derechos Humanos, OEA, Unión europea
Slug: la-cidh-esta-siendo-ahogada-por-estados-miembros-de-la-oea
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/04/CIDH-ORGANIZACIONES-DERECHOS-HUMANOS.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: You Tube] 

###### [25 May 2016] 

Ante la situación actual de crisis financiera que afronta la Comisión Interamericana de Derechos Humanos, CIDH, **organizaciones sociales en Colombia exigen al gobierno que aporte los recursos para el mantenimiento de la Comisión**, de manera que se pueda garantizar el acceso a la justicia mediante aparatos internacionales, pues los colombianos son ineficaces para garantizar los derechos de las víctimas, como lo asegura, Danilo Rueda, defensor de derechos humanos de la Comisión de Justicia y Paz.

Para Rueda, que los Estados miembros no financien la CIDH, consiste en “**un mecanismo para obstruir e imposibilitar que los ciudadanos de las Américas tengan un espacio para la protección de sus derechos**”, a su vez, esto refleja la falta de voluntad política para asumir las medidas cautelares, las soluciones amistosas y demás decisiones que se anuncian desde la CIDH, y que se han vuelto simbólicas, pues no existe disposición de los países americanos para proteger los derechos de sus ciudadanos.

Es decir, que se trata de una situación en la que “un sistema regional de derechos humanos está siendo ahogado por Estados miembros que no tienen voluntad real y tampoco quieren someterse a un escrutinio internacional para que se evalúe si están protegiendo o no a los ciudadanos”, señala el defensor de Derechos Humanos, quien agrega que **los gobiernos americanos “se han sentido incómodos y la respuesta ha sido ir limitando poco a poco los fondos para imposibilitar su accionar”.**

Los recursos que sustentan a la Comisión provienen de los países miembros de la Organización de Estados Americanos, OEA, sin embargo, ese aporte es muy bajo,  y en cambio la Unión Europea es de quien provee mayores recursos a la CIDH por su interés particular en la defensa de los derechos humanos.  Pero ese recurso es insuficiente para este año, lo que generará que **el 40% del personal deba disminuirse, pues los menos de US\$5 millones con los que se cuenta durante este año, son insuficientes para renovar los contratos de los trabajadores.**

Si la crisis persiste, **las víctimas de América se quedarían sin un sistema internacional que si les puede ofrecer mecanismos para acceder a la verdad, justicia y reparación,** frente a la falta de garantías al interior de sus países, pues este sistema, ha sido ejemplo en el mundo de la forma como se han generado estándares y acciones frente a la defensa de los derechos humanos, como lo explica Danilo Rueda.

<iframe src="http://co.ivoox.com/es/player_ej_11675640_2_1.html?data=kpajmZqaeJGhhpywj5WUaZS1kZWah5yncZGhhpywj5WRaZi3jpWah5yncaXVz87Z0ZC2ucbYwpCajajTscrnyoqwlYqmd8%2BfxcqYrNrXuMrXysaY25C0pduhhpywj6jTstXVyM7cjbfFqMrjjJKSmaiReA%3D%3D&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
