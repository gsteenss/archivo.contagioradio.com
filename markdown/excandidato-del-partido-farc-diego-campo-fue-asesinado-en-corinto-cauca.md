Title: Excandidato del Partido FARC, Diego Campo fue asesinado en Corinto, Cauca
Date: 2019-11-10 13:14
Author: CtgAdm
Category: DDHH, Nacional
Tags: nariño, Partido FARC, Violencia Política
Slug: excandidato-del-partido-farc-diego-campo-fue-asesinado-en-corinto-cauca
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/Diego-Campo.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo Particular] 

**Diego Fernando Campo**, quien fuera candidato a la Asamblea Departamental de Nariño por el Partido Fuerza Alternativa Revolucionaria del Común (FARC) en las pasadas elecciones locales, fue asesinado la noche del pasado 9 de noviembre en Corinto Cauca. Ante los hechos, el partido ha exigido garantías de vida reales y celeridad en la investigación.

De acuerdo a lo establecido, Diego quien según varios medios, no combatió dentro de la guerrilla, fue asesinado por hombres armados que dispararon contra su humanidad. [(Lea también:En Colombia han sido asesinados 170 excombatientes tras la firma del Acuerdo de Paz)](https://archivo.contagioradio.com/en-colombia-han-sido-asesinados-170-excombatientes-tras-la-firma-del-acuerdo-de-paz/)

Según María Dolores Guanga, tambien candidata a la misma asamblea y perteneciente al mismo partido, indicó que el candidato **había denunciado ante la Fiscalía las amenazas de muerte** que había recibido con anterioridad por postularse a la Asamblea. "Qué la paz para los líderes sociales no siga siendo la de los sepulcros", manifestó el partido político FARC desde redes sociales. [(Le puede interesar: Violencia, un mecanismo de competencia entre candidatos)](https://archivo.contagioradio.com/violencia-un-mecanismo-de-competencia-entre-candidatos/)

Noticia en desarrollo...

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
