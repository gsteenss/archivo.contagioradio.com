Title: Gracias Fidel, gracias Cuba
Date: 2016-11-26 12:55
Category: Abilio, Opinion
Tags: Cuba
Slug: gracias-fidel-gracias-cuba
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/FIDEL_51.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Roberto Chille] 

#### **[Abilio Peña ](https://archivo.contagioradio.com/abilio-pena-buendia/) - [~~@~~Abiliopena](https://twitter.com/Abiliopena)

###### [26 Nov 2016] 

Hoy en Colombia nos despertamos con la noticia del deceso de Fidel Castro. Nuestra primera sensación fue de tristeza, pero luego, como el sol espanta la niebla, el dolor dejó ver la inmensa luz de su figura para nosotras y nosotros, para el mundo.

Quizás lo que más resplandece es la inmensa dignidad que pudo imprimir Fidel en sus corazones, la capacidad de no ceder a las pretensiones del poder como dominación.

Sin duda la Cuba de Fidel y el Fidel de Cuba se confunden en esta hora. El y ustedes han sido capaces de derrotar una dictadura y sobrevivir bloqueados a diez gobiernos de los Estados Unidos que quisieron acabar con esa experiencia piloto de sociedad diferente que podía contagiar a otros pueblos. Y con ustedes acabar con la esperanza de que la pobreza se pueda reducir en nuestros pueblos, cuando existe la voluntad de quienes lo gobiernan.

Las Colombianas y Colombianos nos hemos beneficiado muy especialmente de Fidel, de su gobierno, de su pueblo. Cuántos exiliados acogidos, cuantos estudiantes graduados en sus universidades, cuantos deportistas formados por entrenadores cubanos que sacan la cara en el mundo por Colombia, como la hoy famosa Katerín Ibarguen.

También gracias por esa asamblea del Servicio Internacional Cristiano Oscar Romero -Sicsal- propiciada en La Habana con el llamado a ser fieles al Jesús de la Justicia. También por ese Alba de los Pueblos que promueven; por ese grupo Oscar Romero cristiano, martirial y solidario que articula la fe y la política. Gracias por el Centro Martin Luter King que media, forma, tiende puentes con el pueblo de los Estados Unidos, facilita la paz y Noviolencia. Gracias por la Coral Leo que nos acoge, nos reconoce, nos enseña interpreta y nos canta.

Y cuanta entrega de Fidel, de Raul, de ustedes a la Paz de nuestro país.

Ese claro llamado de Fidel a las guerrillas a buscar la salida política negociada, que sin duda ha sido escuchada por las FARC-EP y por el ELN. Y esa disposición e incondicionalidad para acoger, auspiciar, garantizar, facilitar diálogos entre el gobierno de Colombia y las guerrillas, en cada momento en que se lo solicitaron.

Fidel no fue perfecto, como ninguno de nosotros lo es, pero si fue un ser humano de gran estatura moral cuya dignidad lo hizo soñar con un mundo más justo y se arriesgó a trabajar por hacerlo realidad. Esa es la Cuba con las muchas virtudes que conocemos hoy en medio de las limitaciones de toda condición social.

Gracias Fidel, Gracias Cuba gracias queridas amigas y amigos por mostrarnos las dimensiones históricas de la dignidad de un pueblo.
