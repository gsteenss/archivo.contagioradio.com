Title: Paramilitarismo y contrabando, problemas comunes en frontera Colombo-venezolana: Cancilleres
Date: 2015-08-27 12:08
Category: El mundo, Movilización, Política
Tags: Cancillería colombiana, Cancillería venezolana, Cierre de frontera, Colombianos deportados, Frontera entre Colombia y Venezuela, Venezuela
Slug: paramilitarismo-y-contrabando-problemas-comunes-en-frontera-colombo-venezolana-cancilleres
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/08/Venezuela.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:www.cancilleria.gov.co] 

<iframe src="http://www.ivoox.com/player_ek_7636318_2_1.html?data=mJugmJiVfI6ZmKiakpyJd6KkmJKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRkcLmhqigh6aopYy1z8zSzsaPjNDgyNqSpZiJhaXihpewjajFssTdzdHS1JCns83jzsfWw9PFcYarpJKw0dPYpcjd0JC%2Fw8nNs4yhhpywj5k%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [María Ángela Holguín, Canciller Colombiana] 

<iframe src="http://www.ivoox.com/player_ek_7636370_2_1.html?data=mJugmJibdI6ZmKiakpyJd6KnlJKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRiMbgxN6YtNTItoa3lIqupszZqduZk6iYxcbSp8rgzcrfjcnJb7fZz8rn18rQpY6ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Delcy Rodríguez, Canciller de Venezuela] 

###### [27 Ago 2015] 

[El día de ayer se llevó a cabo la reunión programada entre las cancillerías de Colombia y Venezuela, encabezadas por María Ángela Holguín y Delcy Rodríguez, respectivamente, los gobernadores de los estados fronterizos y los embajadores de ambos países, con el fin de **transformar y reactivar las relaciones en las fronteras colombo-venezolanas**.]

[Delcy Rodríguez, canciller de Venezuela, afirma que esta reunión es el primer paso hacia la **ruta que se espera posibilite construir una frontera humana y en paz**, ante  el contrabando de alimentos, gasolina y el paramilitarismo que atacan directamente la economía venezolana.]

[Así mismo, la Canciller hizo expresa la solicitud del gobierno venezolano a las **autoridades colombianas para que reporten la lista de paramilitares que se han desmovilizado**, como una muestra de cooperación para el establecimiento de un plan de seguridad conjunto en las zonas fronterizas.]

[Ante las voces que piden la salida de Colombia de la UNASUR, la Canciller afirmó su negativa, **los dos países se encuentran resolviendo sus problemas de manera franca**, **pese a la manipulación mediática que incita el odio entre las dos naciones**.]

[Al respecto indicó que las denuncias de agresiones a colombianos deportados son falsas, el gobierno venezolano ha actuado bajo las leyes que demandan su Constitución para la garantía de los derechos humanos. Por lo que propuso la consolidación de medios de comunicación directos entre los dos países.]

[Por su parte María Ángela Holguín, canciller colombiana, ratificó que existen bandas criminales dedicadas al narcotráfico y el contrabando en las zonas de frontera, que afectan tanto a Venezuela como a Colombia, por lo que en la reunión se estableció un compromiso de trabajo conjunto, materializado en **47 pasos que pretenden fortalecer la operación binacional** para el combate de las problemáticas asociadas al contrabando y al paramilitarismo y para que se transformen las zonas fronterizas.]

[La Canciller afirmó que el gobierno colombiano está presto a brindar acompañamiento a los colombianos deportados, para garantizarles retornar al país y contar con oportunidades de vida digna y que en **conjunto con las autoridades venezolanas implementaran un protocolo de deportación**.]
