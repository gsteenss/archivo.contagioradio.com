Title: "No estamos dispuestas a contar una más" Ángela María Robledo
Date: 2015-06-03 12:11
Category: Mujer, Nacional
Tags: Angela Maria Robledo, bancada de mujeres en congreso, feminicidio en Colombia, Gloria Inés Ramírez, la Secretaria Distrital, Ley Rosa Elvira Cely, Medicina Legal, ONU Mujeres, Rosa Elvira Cely, Violencia contra las mujeres
Slug: no-estamos-dispuestas-a-contar-una-mas-angela-maria-robledo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/06/feminicidios.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: contagioradio.com 

<iframe src="http://www.ivoox.com/player_ek_4590158_2_1.html?data=lZqmkpaZfI6ZmKiak5WJd6KkmZKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRhYzpz5Ddw9jTb8XZjNnW0s7KrcTV05DSzpDKqc7dz87Qy8nNs4zZz5Cw0dHTscPdwpKSmaiRh9Di1cbUy9SPlsLYytSYj4qbh46o&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Ángela María Robledo, representante a la cámara] 

###### [3 de Junio 2015]

**El Proyecto de Ley Rosa Elvira Cely, que penaliza el feminicidio con hasta 50 años de prisión,** fue aprobado este martes en último debate en plenaria de Cámara de Representantes; ahora solo queda esperar la conciliación en el Congreso y la firma del presidente Juan Manuel Santos para que sea declarada nueva ley de la República de Colombia.

Gloria Inés Ramírez, ex senadora; la Secretaria Distrital; diversas organizaciones de mujeres; ONU Mujeres, y en general la bancada de mujeres del congreso, fueron quienes estuvieron detrás de este proyecto de Ley que establece una **herramienta para los operadores de justicia a la hora de penalizar asesinatos contra mujeres,** debido a que se tipifica este tipo de crímenes cuando las víctimas son violentadas por su condición de género.

“Esta Ley busca convertirse en una **pieza persuasiva para quienes convierten la casa en el primer campo de batalla en Colombia**”, señala la representante a la cámara Ángela María Robledo, una de las congresistas que estuvo apoyando todo el proceso de este proyecto.

De ser un hecho, esta Ley obligaría a la Fiscalía a investigar cuando exista alguna sospecha de feminicidio; eleva las penas hasta por 50 años para quienes cometan este tipo de crímenes;s e generan garantías para las familias de las mujeres asesinadas; se tipifica la violencia sexual en territorios de guerra, y se promueve la formación de los operadores de justicia sobre los derechos de las mujeres, entre otras medidas, que sirven no solo para hacer justicia sobre los asesinatos contra las colombianas, sino también para prevenirlos.

De acuerdo a la representante del partido Verde, “la violencia contra las mujeres en el mundo es una pandemia”. En Colombia en el último informe de Medicina Legal, en los primeros dos meses de 2015, se han registrado **125 muertes de mujeres, cada 6 días un hombre mata a una mujer, cada 15 segundos hay un episodio de violencia contra el género femenino.**

Las causas de esta situación en el país, según la congresista, se dan en el marco de la **cultura machista o patriarcal que se vive en Colombia**, exacerbada por la dinámica del conflicto armado, donde se controla, somete y explota la de vida de las mujeres. Así mismo, respecto a ese sentido patriarcal, la ausencia de reconocimiento del rol del género femenino es una de las causas que promueve la violencia hacia las mujeres sin tener en cuenta que, por ejemplo, en materia económica, **las tareas del hogar aportan 130 billones de pesos a la economía del país,** resalta Robledo.

“**Esperamos que estas leyes salgan de los escritorios a los territorios,** y que las mujeres paren cualquier gesto de agresión y lo hagan saber”, concluye la representante a la cámara, quien añade que esta Ley Rosa Elvira Cely deberá complementarse con la ley 1257 del 2008 por la cual se dictan normas de sensibilización, prevención y sanción de formas de violencia y discriminación contra las mujeres.
