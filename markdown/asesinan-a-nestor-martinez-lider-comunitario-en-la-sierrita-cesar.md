Title: Asesinan a Néstor Martínez líder comunitario en La Sierrita, Cesar
Date: 2016-09-12 12:55
Category: DDHH, Nacional
Tags: Asesinatos, cesar, lideres sociales, paramilitares
Slug: asesinan-a-nestor-martinez-lider-comunitario-en-la-sierrita-cesar
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/02/PARAMILITARES-cordoba.gif" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: El Pilón 

###### [12 Sep 2016] 

En la tarde de este domingo fue asesinado **Néstor Iván Martínez, vocero de la Comisión de Interlocución e integrante del Congreso de los Pueblos en Cesar.** El homicidio lo habrían propinado hombres armados que se movilizaban en motocicletas que llegaron al predio del líder comunitario y allí le pegaron  dos tiros en la cabeza.

Según la denuncia, los asesinos ingresaron a la finca de Martínez ubicada en la zona rural de la Sierrita, allí amarraron al administrador de la finca y a su esposa y cuando Néstor llegó lo asesinaron. **“Era un persona humilde y trabajadora, que se había destacado por su entrega a su comunidad”,** expresa Elias Nahuan, integrante del Movimiento de Trabajadores Campesinas y Campesinos de Cesar.

El líder social se encontraba liderando un proceso comunitario en defensa del territorio y el ambiente  en contra de la minería en esta región del país, así mismo guiaba **procesos de reclamación de tierras,** y había en julio había participado de una jornada de protesta ante la decisión del cierre del Hospital público San Andrés del municipio de Chiriguaná.

El pasado 25 de agosto fueron repartidos en varios **municipios del Sur del Cesar panfletos donde se amenazaba a la Comisión de Interlocución**, por parte del grupo paramilitar GALS o Grupo Armado de Limpieza Social. “Néstor fue asesinado por grupos de 'limpieza social', que sabemos que son paramilitares", dice el Elias Nahuan.

En ese panfleto, amenazaba a líderes sociales del Cesar, Bolívar y Santander refiriéndose a una inconformidad por la **ocupación de varias fincas inexplotadas económicamente**, en las cuales centenares de familias víctimas del conflicto armado se han asentado, con el fin de mitigar el hambre y la ausencia de vivienda, ante las difíciles condiciones de pobreza,  desarraigo y abandono al que han sido sometidas por los grupos armados y por el Estado.

Esta no es la primera vez que la comunidad vive hechos de violencia, pues cabe recordar que la jornada de protestas por el cierre del centro hospitalario en Chiriguaná finalizó con el asesinato el joven Neiman Agustin Lara, integrante del Consejo Comunitario de La Sierrita,  además en el marco de esas movilizaciones la comunidad fue víctima de muchas agresiones y atropellos por parte de la Fuerza Pública.

Ante esta situación, **la comunidad exige la inmediata protección de los líderes y lideresas de la región.** También, exigen una investigación eficaz de los hechos que rodearon el asesinato de Néstor Martínez y solicitan el desplazamiento inmediato de una comisión de verificación conformada por autoridades nacionales, en la cual sea invitada la **Oficina del Alto Comisionado de las Naciones Unidas para los Derechos Humanos**, con el fin de acompañar a la comunidad, verificar los hechos de agresión a la misma, iniciar las investigaciones pertinentes y tomar medidas inmediatas de protección.

<iframe id="audio_12875511" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_12875511_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
