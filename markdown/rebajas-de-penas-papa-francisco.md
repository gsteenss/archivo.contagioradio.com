Title: Familiares de detenidos piden rebajas de penas por visita del Papa Francisco a Colombia
Date: 2017-08-12 12:09
Category: Judicial, Nacional
Tags: carceles, Papa Francisco, rebaja de penas
Slug: rebajas-de-penas-papa-francisco
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/08/rebaja-de-penas-papa-francisco.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Equipo Jurídico Pueblos] 

###### [12 Ago 2017]

Cerca de veinte mil firmas fueron recogidas por organizaciones de derechos humanos y de familiares de detenidos de las cárceles de Cúcuta, Valledupar, Cómbita y Bogotá, y entregadas a los sacerdotes Darío Echeverri y Wilson Castaño de la Conferencia Episcopal de Colombia, con la solicitud de una **rebaja de penas a los presos y una oportunidad para la reflexión en torno a la política carcelaria y punitiva en el país de cara a la visita del Papa Francisco al país.**

Durante el encuentro los sacerdotes también recibieron una serie de cartas elaboradas por los internos en los que dan cuenta de las diversas situaciones que se afrontan al interior de los establecimientos penitenciarios. El objetivo de las comunicaciones es aportar en una reflexión en torno a la **necesidad de humanizar la política punitiva y carcelaria que según ellos “criminaliza la pobreza”.**

**De cara a la visita del Papa Francisco a Colombia** los firmantes aseguran que “esperan del más alto jerarca de la Iglesia católica un mensaje humanitario que llegue “ante quienes toman las decisiones sobre política criminal y política penitenciaria, para que no se sigan dejando llevar por el punitivismo de quienes sienten aversión contra los más desfavorecidos” [Lea también: El pico y placa para poder dormir en las cárceles.](https://archivo.contagioradio.com/en-las-carceles-de-colombia-existe-pico-y-placa-para-poder-dormir/)

Durante el encuentro también se logró establecer la continuidad del diálogo en torno la posibilidad de participar directamente en las conversaciones de paz del gobierno de Colombia con el ELN, señaló el Equipo Jurídico Pueblos, organización que acompañó el encuentro.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
