Title: Gobierno incumple acuerdos en erradicación de cultivos de uso ilícito en Putumayo
Date: 2015-09-04 15:29
Author: AdminContagio
Category: Comunidad, Economía, Nacional, Resistencias
Tags: cultivos ilícitos, Cumbre Agraria Étnica y Popular, erradicación, incumplimientos del Gobierno
Slug: gobierno-incumple-acuerdos-en-erradicacion-de-cultivos-de-uso-ilicito-en-putumayo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/cocaina.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: eluniversal.com 

<iframe src="http://www.ivoox.com/player_ek_7916472_2_1.html?data=mJ6emJmbdo6ZmKiakpWJd6KkkZKSmaiRdI6ZmKiakpKJe6ShkZKSmaiRi9DWysrf0NSPrc%2FX1tLdzsqPpcTpxtfR0diPqc%2Bfxtffw8nNp8LXyoqwlYqdd8%2BfxcqYxdrQuMrq0Niah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Orlando Díaz, Comunicador COMPAZ] 

###### [4 Sep 2015] 

Habitantes de la zona de reserva campesina de la Perla Amazónica denunciaron que un grupo de 30 militares de la Fuerza Naval del Sur adelantarían labores de erradicación de cultivos ilícitos en esta zona, incumpliendo los compromisos asumidos por el gobierno en los diálogos de paz.

Frente a estas acciones un grupo de 100 campesinos reclamaron a los militares indicando "*que esta es una de las formas con las que ellos sustentan a sus familias, ya que el gobierno no ha dado soluciones frente a la sustitución de estos cultivos*", indicó Orlando Díaz, habitante de esta zona.

Ante la negativa de los militares de detenerse, los campesinos decidieron acompañarlos a lo que el capitán de la escuadra respondió “*que tenían orden de erradicar y que si la gente se movía con ellos tuvieran en cuenta que no era culpa de ellos si campesinos caían en campos minados*”.

Pese a la continuidad de las labores en la zona la comunidad seguirá organizándose y presionando a los militares para que dejen esta labor y al gobierno para que cumpla los acuerdos.
