Title: “Mujeres en Colombia están viviendo una tragedia humanitaria”: FDIM
Date: 2017-06-16 12:31
Category: Mujer, Nacional
Tags: acuerdos de paz, enfoque de género, FDIM, mujeres
Slug: mujeres-en-colombia-estan-viviendo-una-tragedia-humanitaria-fdim
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/06/20170616_122159-e1497634253524.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [16 Jun 2017] 

En rueda de prensa, La Federación Democrática Internacional de Mujeres FDIM hizo un llamado a que la sociedad civil divulgue el enfoque de género que se encuentra enmarcado en el punto seis de los acuerdos de paz entre el gobierno y las Farc. Según las integrantes de esta federación, la **población femenina en Colombia está viviendo una tragedia humanitaria.**

La FDIM, mostró los avances que ha realizado como organización internacional acompañante de los acuerdos de paz en lo que se refiere al enfoque de género.  Para Gloria Inés Ramírez, vicepresidenta de la Federación Democrática Internacional de Mujeres, “el enfoque de género que está en los acuerdos de paz es importante para la sociedad colombiana porque **garantiza que las leyes asociadas a la defensa de los derechos de las mujeres se vuelvan realidad** en todos los territorios”. (Le puede interesar: ["La violencia contra las mujeres es una pandemia: mujeres en el senado"](https://archivo.contagioradio.com/desde-el-senado-piden-renuncia-del-concejal-ramon-cardona/))

Las representantes del FDIM rescataron que, dentro de las iniciativas de paz, las mujeres han logrado establecer un enfoque que **garantice la consecución de los derechos de ellas y de la población LGBTI**. Para ellas, “es muy importante que en esta etapa de transición hacia la paz exista respeto por la diversidad étnica y sexual en todos los territorios del país.

**Preocupaciones del FDIM**

Si bien existen iniciativas por involucrar en el debate político la importancia del enfoque de género, las preocupaciones que ellas han evidenciado en la implementación de los acuerdos, se fundamentan en la constante agresión hacia la población femenina. La FDIM denunció que **las bandas criminales y los paramilitares han asesinado lideresas y la cifra de feminicidios no disminuye**.

De igual forma manifestaron que las condiciones de salud en las Zonas Veredales para la Transición y los Puntos Transitorios de Normalización, no responden a las necesidades de las mujeres que están es estado de gestación o que tienen hijos pequeños. (Le puede interesar: ["Cuando las mujeres opinamos"](https://archivo.contagioradio.com/cuando-las-mujeres-opinamos/))

Gloria Inés Ramírez fue enfática en establecer que existe una necesidad latente por crear una **política de educación sexual para la población ex combatiente y en especial para las mujeres**. Según Ramírez “tal como el acuerdo ha reconocido los impactos del conflicto en los cuerpos y en la vida de las mujeres hoy necesitamos que las instituciones cumplan los acuerdos que se enfocan en el empoderamiento de las que están haciendo tránsito a la vida civil”.

<iframe src="http://co.ivoox.com/es/player_ek_19306914_2_1.html?data=kp6gkpuddZWhhpywj5WZaZS1k56ah5yncZOhhpywj5WRaZi3jpWah5yncajg0NfWw5Ctsoa3lIqum9iPlsLhhqigh6aotsbuhpewjdjTptPZjNXczoqnd4a1pdnWxcbXb8XZjMrR18jFp8qZpJiSpJjSb9Shhpywj6jTstXVyM7cjbfFqMrjjJKSmaiReA..&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

Hoy una de las grandes preocupaciones de las ex combatientes es que, según lo afirma Ramírez, “**no tienen garantías de seguridad para llegar a las comunidades porque son conscientes de que están en riesgo**”. Ante esto, la FDIM hizo énfasis en que las mujeres de los territorios no tienen a su disponibilidad programas de salud con enfoque de género y tampoco hay un programa de seguridad efectivo que garantice la consecución de la reinserción a la sociedad.

**Lecciones del Salvador para Colombia**

Lorena Peña es la presidenta general de la FDIM y presidenta de la Asamblea Nacional de la República del Salvador. Para ella “es muy importante que las mujeres colombianas hayan logrado **incluir un enfoque de género en el acuerdo de paz** que las respete y garantice el pleno ejercicio de sus derechos en la transición hacia la paz”. (Le puede interesar: ["Las mujeres cultivadoras de coca del sur de Colombia se movilizan por la equidad"](https://archivo.contagioradio.com/las-mujeres-cultivadoras-de-coca-del-sur-de-colombia-se-movilizan-por-la-equidad/))

<iframe src="http://co.ivoox.com/es/player_ek_19306962_2_1.html?data=kp6gkpudepOhhpywj5WYaZS1kZiah5yncZOhhpywj5WRaZi3jpWah5ynca3j08rbw5C0qYa3lIqvk8aJdqSf1NTP1MqPrc7k0Nfhw9PHrcKfxcrZjcrSqtDl1sqYxsqPq4a3lIqum9PJttCfjoqkpZKns8_owszW0ZC2pcXd0JCah5yncZU.&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

Peña afirmó que, “en los acuerdos que se hicieron en el Salvador no hubo un punto que involucrara el enfoque de género (...) fue a través de la lucha femenina del Frente Farabundo Martí para la Liberación Nacional y de las mujeres de la sociedad salvadoreña en general que se **creó una plataforma que reconociera los derechos de las mujeres y las incluyera en los acuerdos de paz**”.

Finalmente, señaló que el enfoque de género “obliga a la sociedad a reconocer que la **población diversa y las mujeres, tenemos la necesidad de programas diferenciados con respecto a los hombres**, en lo económico, en lo social y en lo político”.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU).
