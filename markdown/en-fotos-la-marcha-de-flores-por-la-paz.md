Title: En Fotos la marcha de Flores por la Paz
Date: 2016-10-13 16:14
Category: Galerias
Slug: en-fotos-la-marcha-de-flores-por-la-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/10/marcha-de-las-flores101316_97.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

Delegaciones provenientes de todo el país se reunieron en Bogotá, para exigir que se cumplan de inmediato los acuerdos alcanzados en la Habana entre el gobierno y la guerrilla de las FARC EP.

Más de 150 mil personas  entre organizaciones sociales, sindicales, estudiantiles, políticas, feministas y población LGBTI, se tomaron las calles de la ciudad, haciendo con cada paso un reconocimiento  a las víctimas que han resultado de más de 52 años de conflicto y a los pueblos indígenas, que  conmemoraban el día de su lucha y dignidad, siendo ellos quienes han sufrido en carne propia los embates de la guerra

\
