Title: Falta de apropiación ciudadana es el mayor riesgo para los humedales
Date: 2016-02-02 15:09
Category: Ambiente, Nacional
Tags: ALO, Día Mundial de los Humedales
Slug: falta-de-apropiacion-ciudadana-es-el-mayor-riesgo-para-los-humedales
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/02/Humedales.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Poli Radio 

<iframe src="http://www.ivoox.com/player_ek_10291939_2_1.html?data=kpWfm5add5qhhpywj5aUaZS1lZyah5yncZOhhpywj5WRaZi3jpWah5yncafVzdnOjcnJb8Lk09Tdy8bHrYa3lIqvldOPp8rpxcbRw9PFb8bnjMrZjdLFvdDmjNfWx9jLs4zkwtfOj4qbh4630NPhw8zNs4zGwsnW0ZCRaZi3jpk%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Daniel Bernal, Humedales Bogotá] 

###### [2 Feb 2016] 

“La falta de apropiación ciudadana representa el mayor riesgo para los humedales”, dice Daniel Bernal, fundador de Humedales Bogotá, en el Día Mundial de los Humedales cuando se evidencia que el **64% de estos ecosistemas en el mundo están desapareciendo,** sin embargo, las personas continúan sin entender la importancia de los estos.

“Es importante que haya apropiación y conocimiento, de aquí en adelante los humedales serán los pozos reservorios de agua dulce que tendremos”, dice Bernal, apropósito del lema de este año que adoptó la Convención Ramsar **“Humedales para nuestro futuro”.**

De acuerdo con La Convención, los humedales son “extensiones de marismas, pantanos y turberas, o superficies cubiertas de aguas, sean éstas de régimen natural o artificial, permanentes o temporales, estancadas o corrientes, dulces, salobres o saladas, incluidas las extensiones de agua marina cuya profundidad en marea baja no exceda de seis metros”, allí se alberga una gran diversidad de especies de fauna y flora, y además, estos **son esenciales para enfrentar el cambio climático.**

Según Daniel Bernal, la política en Colombia que protege a los humedales está bien construida, pero “se queda en el papel”, ya que de acuerdo con la Asociación Ambiente y Sociedad, aunque en Colombia existen 31 áreas protegidas, tan solo en Bogotá  existen 15 humedales reconocidos y 16 que no lo son. Así mismo, **el total del territorio colombiano está compuesto en un 26% por humedales, pero solo el 7,2 por ciento de ese total está legalmente protegido**, siendo la ganadería la actividad que más afecta estos ecosistemas.

En el país el 34% de los humedales se encuentran en La Orinoquía, 1.094 municipios poseen áreas con este tipo de ecosistemas, y existen 31.702 humedales inventariados. La variación de estos ecosistemas puede determinar el futuro del país en temas como inundaciones, sequías y fenómenos climáticos.

En Bogotá, proyectos de construcción como urbanizar el borde norte de la capital acabado con la mayor parte de la Reserva Forestal Van Der Hammen, y realizar la Avenida Longitudinal ALO, tienen en mayor riesgo humedales como La Conejera, Juan Amarillo y Capellanía.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
