Title: Productos orgánicos de Cuba son reconocidos en EE.UU
Date: 2017-01-06 11:24
Category: Economía, El mundo
Tags: bloque económico, Cuba, Estados Unidos, productos orgánicos
Slug: productos-organicos-cuba-reconocidos-ee-uu
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/01/Agricultura_de_Cuba-e1483720506111.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Discovery 

###### [6 Ene 2016] 

Scott D. Gilbert, importante empresario de Estados Unidos reconoció que los suelos de Cuba son totalmente orgánicos y por ello instó a que la isla y el país norteamericano **compartan productos, conocimiento y economía.**

El presidente de Reneo Consulting LLc resaltó la producción de carbón vegetal y otros productos agrícolas, gracias a las condiciones de los terrenos con los que cuenta Cuba. Asimismo manifestó que en **los suelos estadounidenses es imposible conseguir campos con esas mismas características debido a que la gran mayoría están contaminados** por el uso de plaguicidas.

Por ello fue que esta semana el empresario firmó un contrato con Cubaexport, con el fin de exportar de carbón vegetal a Estados Unidos. Lo que significa que por primer vez en 50 años, se da la primera venta de un producto cubano a EE.UU.

“La importancia de este acuerdo va más allá de la importación de **carbón vegetal, sino también de miel de abeja y café,** y de que estamos poniendo otro tablón en el puente que empalmará a Cuba y Estados Unidos y que podamos cruzar de un sitio al otro”, dijo Gilbert, al portal de noticias Prensa Latina.

Respecto a Donald Trump, el empresario aseguró que **espera poder trabajar con el nuevo gobierno con el fin de impulsar las relaciones entre las dos naciones**, como una forma de seguir dando pasos hacia el restablecimiento de las relaciones bilaterales, y con ello poder dar fin al bloqueo económico contra la isla.
