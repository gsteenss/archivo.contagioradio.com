Title: "Gobierno y ELN no pueden seguir escalando la guerra"
Date: 2017-02-27 12:11
Category: Entrevistas, Paz
Tags: ELN, Gobierno, Quito
Slug: gobierno-y-eln-no-pueden-seguir-escalando-la-guerra
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/02/PAZ-ELN-CANCILLERIA-Ecuador-2.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Cancillería Ecuador] 

###### [27 Feb 2017] 

**“Están equivocadas las partes si consideran que escalando el conflicto van a transformar las condiciones de la mesa de conversaciones”**  afirmó Carlos Medina, analista político, que señaló como hechos “lamentables” que el ELN haya ratificado su accionar en la activación de un petardo en Bogotá, que dejo un policía muerto,  y la continuidad de la ofensiva de la Fuerzas Militares contra esta guerrilla.

Para Medina, estos actos son consecuencia de, primero,  **tener una mesa de conversaciones sin haber declarado antes un cese bilateral**, y segundo de **operaciones militares que se ejecutan en diferentes lugares, en donde hay presencia del ELN, para “diezmarla”** y llevarla a una mesa de conversaciones como derrotada, afectando no solamente la confianza , sino también la voluntad política para seguir en el proceso de paz. Le puede interesar: ["Los restos de las conversaciones de Paz del ELN-Gobierno" ](https://archivo.contagioradio.com/los-retos-de-las-conversaciones-de-paz-gobierno-eln/)

Uno de las primeras comisiones que se instaló fue la Comisión de dinámicas y acciones humanitarias que busca incentivar acciones, por ambas partes, para establecer confianza e interés por alcanzar la paz, por ende, Medina señaló que es importante que esta **comisión efectué acciones que desescalonen el conflicto, en principio puede que no sea un cese bilateral pero si uno "unilateral declarado por el ELN".**

De igual forma expresó que “**no se puede desarrollar la guerra e invitar a la sociedad a que en  medio de un campo de batalla se siente a dialogar**” sino que por el contrario hay que generar un ambiente que permita seguridades y respaldos.  Por lo cual agregó que es el momento para generar compromisos en avanzar hacia la creación de condiciones para garantizar el diálogo. Le puede interesar: ["Proceso entre ELN-Gobierno no será una paz express"](https://archivo.contagioradio.com/proceso-entre-eln-gobierno-no-sera-una-paz-express/)

<iframe id="audio_17247788" frameborder="0" allowfullscreen scrolling="no" height="200" style="border:1px solid #EEE; box-sizing:border-box; width:100%;" src="https://co.ivoox.com/es/player_ej_17247788_4_1.html?c1=ff6600"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
