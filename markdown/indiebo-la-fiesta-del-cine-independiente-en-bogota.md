Title: Indiebo, La fiesta del cine independiente en Bogotá
Date: 2015-07-18 18:20
Author: AdminContagio
Category: 24 Cuadros, Cultura
Tags: Cinemateca Distrital, Festival de cine Independiente de Bogotá, Indiebo, Indiebox, Paola Turbay, Universidad Javeriana, Universidad Jorge Tadeo Lozano
Slug: indiebo-la-fiesta-del-cine-independiente-en-bogota
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/07/indiebo-2.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [18/julio/2015] 

La primera edición del Festival de Cine Independiente de Bogotá, Indiebo, esta en marcha hasta el próximo 26 de julio. Un espacio en el que confluyen realizadores nacionales e internacionales con una muestra de más de 90 producciones de 31 países, que desfilaron por algunos de los Festivales más prestigiosos del mundo antes de llegar a la capital colombiana.

[![Indiebo 1](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/07/Indiebo-1.png){.aligncenter .wp-image-11426 .size-full width="851" height="351"}](https://archivo.contagioradio.com/indiebo-la-fiesta-del-cine-independiente-en-bogota/indiebo-1/)

13 salas de la capital, sirven de escenario para cada una de las proyecciones,cuya apertura se dió con el estreno de "La tierra y la sombra", del director vallecaucano Carlos Acevedo, largometraje ganador de la "Camara de Oro" en la más reciente edición del Festival de Cine de Cannes, y que llegará a las salas comerciales el próximo 23 de julio.

Paralelo a la programación del Festival, se vienen realizando una serie de jornadas de corte académico, las Charlas Indiebo, en espacios como la Cinemateca Distrital, las Universidades Javeriana y Jorge Tadeo Lozano, con invitados de trascendencia entre los que se encuentran el productor y director Michael Hausman ,el guionista y director de cine Ben Ramsey, el productor y director Warrington Hudlin, entre otros.

Uno de los espacios innovadores del evento es el "Indiebox", instalación que paso por los festivales de Sudance, New Frontier y Tribeca, y que por primera vez se encuentra en Latinoamerica. Los asistentes podrán interactuar hasta el próximo 20 de julio con la propuesta ubicada en el monumento a los héroes, ubicado en el norte de la ciudad.

Adicionalmente, el festival incluye una selección especial de películas para niños, con una destacada participación de las obras del maestro de la animación japonesa Hayao Miyasaki, y el espacio "Cine al parque" con el que se toman diferentes espacios de la ciudad con proyecciones de entrada libre.

Queda bastante tiempo para disfrutar de la [programación de Indiebo](http://indiebo.co/programacion/), estrenos, conversatorios y actividades que todos y todas las amantes del buen cine deben conocer. Para mayor información, consulte [indiebo.co](http://indiebo.co/) .

<iframe src="https://www.youtube.com/embed/x9gSb1dGPOE" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

Un dato:

En septiembre del año anterior, se realizó como antesala al festival, una muestra compuesta por 9 producciones proyectadas en diferentes salas de la ciudad, con éxito de asistencia y recepción por parte de los espectadores capitalinos, lo que sirvió para comprobar la viabilidad de realizar el proyecto que hoy es una realidad.
