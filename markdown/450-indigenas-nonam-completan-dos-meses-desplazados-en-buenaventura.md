Title: 450 indígenas Wounan completan dos meses desplazados en Buenaventura
Date: 2015-01-28 21:47
Author: CtgAdm
Category: DDHH, Resistencias
Tags: buenaventura, Desplazamiento, Nonam
Slug: 450-indigenas-nonam-completan-dos-meses-desplazados-en-buenaventura
Status: published

###### Foto: contagioradio.com 

##### [Mayolo Chamapuro] <iframe src="http://www.ivoox.com/player_ek_4011588_2_1.html?data=lZWek5qcfI6ZmKiakpqJd6Kpk5KSmaiRdo6ZmKiakpKJe6ShkZKSmaiRkcLt0NHcjajMpc7V0drf0YqWh4zgysnS1JCys8%2FVzpDS0JCmucbiwtvS0NnZtsKhhpywj6jTstXVyM7cjbfFqMrjjJKSmaiReA%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe> 

Desde el pasado mes de Noviembre, indígenas Nonam, habitantes de los resguardos Unión Balsaito y Unión Aguas Claras, se encuentran **desplazadas en condiciones inhumanas**  en el coliseo de **Buenaventura** y sin la atención necesaria para sobrevivir de manera digna.

Este martes fueron atendidos por una **comisión mixta de gobierno nacional y municipal** que definirá un cronograma para buscar el regreso digno de las comunidades. Una de las primeras actividades de esta comisión es la visita de **verificación a las familias indígenas, que siguen confinadas al interior de los territorios.**

Las exigencias de las comunidades, para que se garantice un regreso digno, son apenas la garantía de sus derechos a **educación, salud y vivienda digna para permanecer en el territorio**, esto además de las condiciones de seguridad como el desmonte de las estructuras paramilitares y de actores armados que propiciaron el desplazamiento.

Se espera que, una vez terminada la verificación, se definan las **fechas definitivas para el regreso** de las comunidades a sus territorios de origen.
