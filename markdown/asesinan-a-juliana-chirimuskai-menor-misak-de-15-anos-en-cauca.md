Title: Asesinan a Juliana Chirimuskai, menor misak de 15 años en Cauca
Date: 2019-05-14 14:05
Author: CtgAdm
Category: Comunidad, Nacional
Tags: Asesinato de indígenas en Colombia, Misak Misak, Niños y niñas víctimas del conflicto armado
Slug: asesinan-a-juliana-chirimuskai-menor-misak-de-15-anos-en-cauca
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/misac.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo] 

El pasado 13 de mayo, se conoció el asesinato de la joven indígena misak **Juliana Chirimuskay Velasco** de 15 años de edad quien fue hallada sin vida en la vereda Ñimbe, del resguardo indígena de Guambia, en en el municipio de Silvia, Cauca. Yuliana fue encontrada con aparentes signos de abuso sexual y violencia.

La investigación ya está siendo adelantada por la Fiscalía, mientras la Guardia Indígena capturó a dos jóvenes sospechosos de haber cometido el delito, por ahora aguardan a que Medicina Lega determine si Yuliana fue víctima de abuso sexual para esclarecer las causas exactas de su muerte.[(Le puede interesar: Según CINEP en 2018 fueron victimizados 98 líderes cívicos y comunales)](https://archivo.contagioradio.com/en-2018-fueron-victimizados-98-lideres-civicos-y-comunales-cinep/)

[La comunidad educativa de la Institución Educativa Misak Mama Manuela, lugar en el que Juliana adelantaba grado décimo lamentó el suceso y expresó su condolencia hacia la familia de la menor y la comunidad en general, invitando a sus estudiante a recuperar la autoridad de la familia.][(Lea tambiën; Miguel Morales, sexto profesor asesinado en Cauca este año)](https://archivo.contagioradio.com/miguel-morales-profesor-asesinado/)

Por su parte la Organización Nacional Indígena de Colombia (ONIC) y el Consejo Regional Indígena del Cauca (CRIC) manifestaron su preocupación por **"este hecho hecho que ha causado consternación en el pueblo Misak, y demás comunidades indígenas del Cauca**".

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
