Title: 30 días tiene MinDefensa para retirar estación de Policía de El Mango
Date: 2016-08-05 18:33
Category: DDHH, Nacional
Tags: Agresiones fuerza pública, Cauca, el mango, ministerio de defensa
Slug: 30-dias-tiene-mindefensa-para-retirar-estacion-de-policia-de-el-mango
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/08/El-Mango.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: LaFm] 

###### [5 Ago 2016] 

Los pobladores de la vereda Campo Alegre del corregimiento de El Mango, en el departamento del Cauca, celebran el fallo de la Corte Constitucional que ordena al Ministerio de Defensa retirar en máximo 30 días la estación de Policía, teniendo en cuenta que, como denuncian las comunidades, la institución estaba **utilizando a la población civil como escudo ante los ataques** de las FARC-EP.

Hay quienes sienten tranquilidad por la decisión, pero hay otros pobladores como la señora Luz Edilma Montilla que aseguran "que [[hasta no ver no creer](https://archivo.contagioradio.com/gobierno-no-resuelve-infraccion-al-derecho-humanitario-en-el-mango-cauca/)]" y que solo estarán tranquilos cuando vean que en realidad la fuerza pública se va de la vereda, pues actualmente **los uniformados están ocupando siete fincas en las que han cavado bunkers** sin la autorización de los dueños y sin explicar con qué fin.

Rosalba Rivera, campesina de la vereda, asegura que en su finca se han construido ocho bunkers de entre 4 y 3 metros cuadrados; en otras de las fincas como en la del señor Moiso Gaviria **la fuerza pública construyó dos casas de aproximadamente 6 metros cuadrados**, [[la situación preocupa a los pobladores](https://archivo.contagioradio.com/integrantes-de-las-ffmm-habrian-intentado-abusar-sexualmente-de-2-ninas-en-el-mango-cauca/)] porque aseguran que las casas están "demasiado bien hechas para ser temporales".

###### [Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)]] 
