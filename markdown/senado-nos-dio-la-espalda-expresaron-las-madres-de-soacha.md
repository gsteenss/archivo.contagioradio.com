Title: "El Senado nos dio la espalda" expresaron las Madres de Soacha
Date: 2020-11-29 10:36
Author: CtgAdm
Category: Actualidad, DDHH
Tags: Congreso de la República, Ejecuciones Extrajudiciales, madres de soacha, Negacionismo
Slug: senado-nos-dio-la-espalda-expresaron-las-madres-de-soacha
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/06/MAFAPO.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: Madres Falsos Positivos de Colombia

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Fue hundido en la Comisión Segunda del Senado el proyecto que buscaba declarar el 20 de septiembre como el día conmemorativo de las víctimas de ejecuciones extrajudiciales y rendir un homenaje a las Madres de Soacha. La negativa por parte de un sector del Congreso ha sido considerada como una muestra de revictimización y negacionismo.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Antonio Sanguino, senador que presentó el proyecto había planteado instaurar el día conmemorativo de las víctimas de ejecuciones extrajudiciales y rendir un homenaje a la [Fundación Madres Falsos Positivos Soacha y Bogotá (MAFAPO)](https://twitter.com/MAFAPOCOLOMBIA) por sus aportes y construcción de la memoria, la paz, la justicia, reparación y no repetición. [(Le recomedamos leer: Madres de Soacha esperaron más de 10 años para tener el mausoleo de sus hijos)](https://archivo.contagioradio.com/madres-de-soacha-mausoleo-para-hijos/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Pese a ello, en medo de la plenaria los senadores Paola Holguín, Ernesto Macías y Jhon Harold Suárez del Centro Democrático, Juan Diego Gómez del Partido Conservador y Luis Diaz Granados Cambio Radical se opusieron a la propuesta en medio de la ausencia de otros congresistas como Ana Paola Agudelo del Partido MIRA, Jaime Durán Barrera y Lidio García del Partido Liberal y José Luis Pérez de Cambio Radical.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/AntonioSanguino/status/1331645928789463045","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/AntonioSanguino/status/1331645928789463045

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph {"align":"justify"} -->

**"Cuando mataron a nuestros hijos nos mataron a nosotras. No pensábamos que pudiéramos volver a sentir ese dolor, hoy causado por el Senado de Colombia. Nuestros muertos no existen para ellos",** expresaron las madres quienes señalan que sin que exista un reconocimiento por parte del Gobierno, el Senado y las Fuerzas Militares es imposible la construcción de paz.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Para Sanguino, la negativa resulta un acto de negacionismo que revictimiza a las familias de al menos 5.000 personas que se estima según organizaciones sociales fueron víctimas de este crimen de lesa humanidad. el senador expresó que volverá a presentar este proyecto de ley. "Seguiremos en la lucha por la memoria y la verdad", agregó. [(Lea también: ¿Quién los Mató? Homenaje a jóvenes y niños víctimas de masacres y ejecuciones extrajudiciales)](https://archivo.contagioradio.com/quien-los-mato-cancion/)  

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Congreso avala labor del Ejército pero no de las Madres y familiares

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

En contraparte, en la misma Comisión Segunda del Senado también se votó por los ascensos de miembros de las Fuerzas Militares y de la Fuerza Pública**,** que incluye en su lista los nombres de uniformados vinculados a ejecuciones extrajudiciales, tal es el caso del **brigadier Marcos Evangelista Pinto Lizarazo, quien ascendió a mayor general, pese a las alertas hechas por la organización Human Rights Watch** que lo vincula a 23 presuntas ejecuciones entre 2006 y 2007. [(Le puede interesar: Con pintura, Ejército pretende ocultar responsabilidad de altos mandos en 'Falsos Positivos')](https://archivo.contagioradio.com/con-pintura-ejercito-pretende-ocultar-la-verdad-de-los-falsos-positivos/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Un caso similar es el del general **Édgar Alberto Rodríguez**, quien según la organización comandó la novena brigada del batallón Magdalena que según la Fiscalía habría cometido 7 presuntas ejecuciones entre 2006 y 2007. Dichos ascensos fueron aprobados en el primero de cuatro debates.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/JMVivancoHRW/status/1330977524382277632?ref_src=twsrc%5Etfw%7Ctwcamp%5Etweetembed%7Ctwterm%5E1330977551729127424%7Ctwgr%5E%7Ctwcon%5Es2_\u0026ref_url=https%3A%2F%2Fwww.eltiempo.com%2Fpolitica%2Fcongreso%2Ffalsos-positivos-ascenso-de-militares-senalados-de-falsos-positivos-en-la-jep-550812","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/JMVivancoHRW/status/1330977524382277632?ref\_src=twsrc%5Etfw%7Ctwcamp%5Etweetembed%7Ctwterm%5E1330977551729127424%7Ctwgr%5E%7Ctwcon%5Es2\_&ref\_url=https%3A%2F%2Fwww.eltiempo.com%2Fpolitica%2Fcongreso%2Ffalsos-positivos-ascenso-de-militares-senalados-de-falsos-positivos-en-la-jep-550812

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph {"align":"justify"} -->

Cabe resaltar que según un informe entregado por la Fiscalía a la Jurisdicción Especial para la Paz (JEP), **el 97% de las ejecuciones extrajudiciales que se han investigado, ocurrieron entre 2002 y 2008, bajo el mandato de Álvaro Uribe** y la comandancia del general Mario Montoya en las FF.MM. [(Caso Álvaro Uribe es esperanza para las víctimas que necesitan la verdad)](https://archivo.contagioradio.com/caso-alvaro-uribe-es-esperanza-para-las-victimas-que-necesitan-la-verdad/)

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
