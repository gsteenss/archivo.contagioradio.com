Title: Pazororidad, el proyecto de mujeres indígenas y campesinas YouTubers
Date: 2019-05-10 11:34
Author: CtgAdm
Category: Cultura, Mujer
Tags: Meta, mujeres, Pazororidad
Slug: pazororidad-el-proyecto-de-mujeres-indigenas-y-campesinas-youtubers
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/Captura-de-pantalla-97.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Pazororidad] 

Alrededor de **60 mujeres del Meta fueron capacitadas para compartir sus conocimientos a través de un nuevo canal colectivo de YouTube, Pazororidad**, en el que le apuestan a la reconciliación y el empoderamiento con enfoque de género en esta región golpeada por el conflicto armado.

Estas YouTubers son mujeres campesinas e indígenas de los municipios Villavicencio y Uribe, Meta, víctimas de género y del conflicto armado, que **en los últimos ocho meses recibieron formaciones en producción audiovisual** para realizar videos que entremezclan historias de vida, consejos sobre la resolución de problemas cotidianos y la construcción de redes de apoyo.

"La intención es construir trabajo en red, de que las mujeres puedan encontrar su propia voz y sus propios relatos y hacerse compañeras y aliadas en este proceso", manifestó Carolina Moreno, una de las promotoras de este proyecto. (Le puede interesar: "[Amenazan a lideresa que se opone a proyectos petroleros en Guamal, Meta](https://archivo.contagioradio.com/amenazan-a-lideresa-que-se-opone-a-proyectos-petroleros-en-guamal-meta/)")

<iframe src="https://www.youtube.com/embed/PRL1ZHNPAUc" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

### Pazororidad en las redes sociales 

Twitter: @pazororidad - Instagram: @pazororidad - Youtube: @pazororidad

<iframe id="audio_35652190" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_35652190_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
