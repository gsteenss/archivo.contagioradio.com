Title: Inicia ampliación del proyecto minero Buriticá en Antioquia
Date: 2017-01-22 19:56
Category: Economía, Nacional
Tags: Antioquia, Buriticá, Mineria
Slug: inicia-ampliacion-del-proyecto-minero-buritica-antioquia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/01/buritica-e1485132013822.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Notimérica 

###### [22 Ene 2017] 

En medio de las movilizaciones de mineros artesanales antioqueños que buscan que su actividad sea legalizada, la multinacional canadiense Continental Gold obtuvo el permiso de la Agencia Nacional de Licencias Ambientales y logró los recursos del fondo Red Kite Mine Finance, para ampliar el proyecto minero Buriticá, en Antioquia, "**la mina de oro más grande de América Latina".**

**Paramilitarismo y oro**

Este **municipio se ha visto golpeado fuertemente por el paramilitarismo y la minería criminal**, que está acabando con los mineros artesanales (barequeros), quienes hoy le critican al gobierno, que la entrega de los territorios a las compañías extranjeras. Favorecen a las empresas y no le significan progreso a la región, afirman algunos de ellos. [(Le puede interesar: Cerca de 15 mil mineros de los municipios de Segovia y Remedios en Antioquia)](https://archivo.contagioradio.com/15-mil-mineros-en-paro-indefinido-por-incumplimientos-del-gobierno/)

La combinación entre el paramilitarismo y la criminalización de la minería artesanal habría arrancado con el nombramiento de Eduardo Otoya Rojas, como vicepresidente de Continental Gold. Según las autoridades, Otoya se vinculó con Luis Adolfo Cortés Pérez, señalado de tener nexos con el grupo paramilitar, Clan Úsuga. En algunos momentos se obligó a los barequeros a pagar un impuesto.

**Minería ilegal y minería criminal, una diferencia que el Estado no ve**

La respuesta del gobierno a esa problemática no tiene contentos a los pequeños mineros. Ellosw y ellas aseguran que se le da permisos “a los gringos” y deja sin trabajo a miles de familias de la región. Esa problemática se da porque “**el Estado homologa la minería ilegal con la minería criminal”, asegura el investigador Oscar Mesa.**

De acuerdo con Mesa, “La implementación de la política minera ha generado una conflictividad social expresada en paros y movilizaciones de mineros, y “ha sido la expresión más álgida de descontento de un sector vinculado a la explotación del oro que ha visto en la presencia de la compañía canadiense y en la política minera del Gobierno Nacional una causal de menoscabo de sus intereses y deterioro de sus derechos”.

**La llamada minería ilegal le genera en Colombia, 7.2 billones de pesos al año**. Por eso, para Ariel Ávila, subdirector de Fundación Paz y reconciliación, es importante que se de paso a la formalización “Tenemos que avanzar sustancialmente en una formalización de estos mineros, necesitamos es que los dineros de las regalías mayoritariamente lleguen a estos municipios”. [(Le puede interesar: empresas mineras no pagan impuestos)](https://archivo.contagioradio.com/empresas-mineras-no-pagan-impuestos-renta-colombia/)

Mientras los pequeños mineros siguen movilizándose frente a las medias del gobierno, la canadiense Continental Gold, pondrá en marcha la etapa de construcción a gran escala del proyecto aurífero con el que **se estima extraer 250.000 onzas de oro al año.**

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
