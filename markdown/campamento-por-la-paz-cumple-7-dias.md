Title: Campamento por la Paz cumple 7 días
Date: 2016-10-11 15:03
Category: Nacional, Paz
Tags: acuerdos de paz ya, Campamento por la Paz, Paz a la Calle
Slug: campamento-por-la-paz-cumple-7-dias
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/10/IMG-20161007-WA0009-01-e1476216153492.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:Contagio Radio] 

###### [11 Oct de 2016] 

Se cumplen 7 días del Campamento por la Paz, iniciativa ciudadana que ha reunido a jovenes, estudiantes y artistas en más de 50 carpas  instaladas en la Plaza de Bolívar de Bogotá. Juana Ruíz, es una de las participantes, ella afirma que la solidaridad ha estado presente en quienes allí se acercan.

Este martes, el campamento tuvo que trasladarse a un costado de la Plaza de Bolívar debido a que la artista Doris Salcedo llevará a cabo una acción en conmemoración a las víctimas llamada “Sumando Ausencias”. Inicialmente se presentaron tensiones puesto que la acción debía ocupar toda la Plaza, pero **a través del diálogo entre los participantes del campamento y la artista, se logró conciliar y compartir un mismo espacio** para dos iniciativas que tienen el objetivo en común de reivindicar a las víctimas, exigir la implementación de los Acuerdos y defender la Paz como un derecho de toda la ciudadanía.

A través de las redes sociales de Paz a la Calle y del Campamento, se ha informado que regresarán a su lugar inicial en horas de la noche y por lo tanto “**se hace necesario el acompañamiento de los medios de comunicación y de la ciudadanía para asegurar el retorno al campamento y prevenir posibles desalojos**”. La invitación a seguir el desarrollo de estas iniciativas a través de las plataformas y aportar más acciones colectivas para poner freno a la dilatación de los acuerdos de Paz.

Además, Ruíz habló sobre el encuentro con las comunidades indígenas que el pasado lunes visitaron la capital con el mismo objetivo de exigencia de Paz, “el día de ayer fue un valioso intercambio con las comunidades indígenas que visitaron la Plaza de Bolívar (…) **poder saber que nos une a todos el deseo de paz es de gran motivación**”.

Se espera que durante esta semana lleguen más delegaciones de comunidades indígenas, campesinas y afro para las movilizaciones del miércoles y el día viernes, según un comunicado de la ONIC del pasado Domingo, **hasta la fecha hay alrededor de 4mil indígenas en la capital y para los próximos días se contará con la presencia de más de 7mil personas que hacen parte de**  las poblaciones más azotadas por el conflicto armado, político y social colombiano.

Lea También: ["Sumando Ausencias", Recordando a las víctimas](https://archivo.contagioradio.com/sumando-ausencias-recordando-a-las-victimas/), [Comunidad Estudiantil se Suma a los Esfuerzos por Acuerdos Ya ](https://archivo.contagioradio.com/comunidad-estudiantil-se-suma-a-los-esfuerzos-por-acuerdos-ya/)

<iframe id="audio_13276234" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_13276234_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en su correo [[bit.ly/1nvAO4u]
