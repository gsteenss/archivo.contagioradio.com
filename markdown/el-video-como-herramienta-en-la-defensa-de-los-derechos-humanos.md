Title: El video como herramienta en la defensa de los Derechos Humanos
Date: 2016-07-13 17:26
Category: Otra Mirada, yoreporto
Tags: Defensa derechos humanos, Reporteria popular, Video ciudadano
Slug: el-video-como-herramienta-en-la-defensa-de-los-derechos-humanos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/07/mural.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Witness Es] 

#### [Por:  **Witness Lac - Yo Reporto **] 

###### 13 Jul 2016 

¿Tienes una cámara en tu bolsillo? ¿Documentas manifestaciones o abusos a  derechos humanos? Hazlo de manera segura y efectiva. El video que grabas en las calles o a través de entrevistas como parte de tu labor de defensa y promoción de derechos humanos puede convertirse en evidencia.

En muchas situaciones, los testigos, periodistas ciudadanos y activistas somos quienes estamos mejor posicionados para reunir evidencia de abusos de derechos humanos.Sin embargo, aunque nuestros videos ciudadanos a menudo ofrecen pistas útiles sobre qué fue lo que ocurrió y quién podría ser responsable, la calidad rara vez cuenta con los estándares necesarios para ser usado como evidencia en un tribunal.

La buena noticia es que con mínimas modificaciones, el material que documenta los hechos en el lugar donde ocurrieron puede aumentar su calidad y su valor dentro de un artículo noticioso, una campaña de comunicación o incluso como parte de procesos de justicia.

El video puede ser un gran apoyo ya sea en formato de videografías para sensibilizar a la población sobre los derechos de las personas privadas de libertad o para documentar la lucha de una comunidad indígena por la defensa de sus tierras ante las mineras.

Las “Prácticas básicas para grabar, guardar y difundir video” te acompañan a lo largo del proceso con una lista de verificación, casos y recomendaciones. Puedes descargar la guía completa [aquí](https://es.witness.org/portfolio_page/practicas-basicas-grabar-guardar-y-difundir-evidencia-en-video-2/) o la miniguía [aquí](https://es.witness.org/portfolio_page/mini-guia-practicas-basicas-para-grabar-guardar-y-difundir-evidencia-en-video/).
