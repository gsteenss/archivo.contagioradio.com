Title: Paramilitares asesinan a una mujer y amenazan a habitantes de Belén de Bajirá
Date: 2016-12-09 12:36
Category: DDHH, Nacional
Tags: Amenazas, Chocó, paramilitares
Slug: paramilitares-asesinan-a-una-mujer-y-amenazan-a-municipio-en-el-choco
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/08/paramilitares-e1472682962275.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Zona Cero ] 

###### [9 Dic. 2016] 

Contagio Radio conoció que **el pasado miércoles 7 de diciembre, sobre las 6:30 de la mañana** **paramilitares asesinaron a una mujer llamada Nohora Echavarría** cerca del hospital La Anunciación y del parque principal del corregimiento de Belén de Bajirá.

De acuerdo a la información que conoció Contagio Radio,  **la víctima se dedicaba a la preparación de alimentos para la policía de este corregimiento.**

Según la denuncia entregada por la Comisión de Justicia y Paz a nuestro medio, **además de este hecho se conoció que también la noche del 6 de diciembre fueron dejados en las viviendas unos panfletos en el que se observa un logotipo de las Autodefensas Unidas de Colombia AUC y es firmado por "comandante Pablo".**

En dichos panfletos aparecen los nombres de algunos habitantes de la región los cuales son declarados objetivo militar, y advierten que "tienen esta semana para que desocupen el pueblo", anuncian además que, las personas deben estar antes de las 10:00 p.m. en sus casas. Le puede interesar: [Desaparecido líder afrocolombiano de Marcha Patriótica](https://archivo.contagioradio.com/desaparecido-lider-afrocolombiano-del-rio-calima/)

![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/12/anamariarizo-e1481304966288.jpeg){.size-full .wp-image-33460 .aligncenter width="580" height="732"}

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)

<div class="ssba ssba-wrap">

</div>
