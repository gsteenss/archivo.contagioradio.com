Title: Concesiones petroleras amenazan a 81 resguardos en Amazonía
Date: 2019-06-27 18:26
Author: CtgAdm
Category: Ambiente, Comunidad
Tags: Amazonía colombiana, Awá
Slug: concesiones-petroleras-amenazan-a-81-resguardos-en-amazonia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/Captura-de-pantalla-138.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Foto: CRIC] 

Una nueva investigación de la organización Ambiente y Sociedad evidencia la creciente presencia de la industria petrolera en la Amazonía colombiana, atraída por reservas de hasta 6.000 millones de barriles que estarían en la cuenca Caguán - Putumayo, según el departamento de Geociencias de la Universidad Nacional.

En el reporte[ ]**‘Petróleo en la Amazonia ¿pueblos indígenas en peligro?’**[, se advierte que en la región Amazónica existen a la fecha]**51 contratos, [a cargo de]16 empresas nacionales e internacionales, para estudiar técnicamente el área, y para explorar y explotar petróleo.**[ ]** **De los 51 bloques petroleros que se encuentran en la región, 37 atraviesan 81 resguardos indígenas, principalmente en los departamentos de Putumayo y Caquetá.

Según **Álvaro Cruz, vicepresidente de la Organización Zonal Indígena del Putumayo,** estos títulos se concedieron sin una consulta previa, libre e informada, con las comunidades indígenas de  los territorios en cuestión, "consultan las comunidades, ya cuando la empresa llega a ejecutar el proyecto", afirmo el líder Awa.

Cruz indicó que la situación ha conducido a conflictos entre el Gobierno y las comunidades indígenas, en donde las últimas han señalado a las entidades nacionales de conceder territorios para empresas petroleras, pero se niegan a conceder tierras a estos grupos para la constitución de sus resguardos. (Le puede interesar: "[Hay concesiones para petroleras pero no para comunidades: Minga Putumayo](https://archivo.contagioradio.com/hay-concesiones-para-petroleras-pero-no-para-comunidades-minga-putumayo/)")

Cruz manifestó que esta situación tiene grandes implicaciones para los 15 pueblos indígenas que viven en el Putumayo, algunos de ellos en vía de extinción. Además, el vicepresidente aseguró que el boom de proyectos petroleros en este departamento, conlleva afectaciones para los ecosistemas de la Amazonía, en particular por la contaminación con mercurio, la construcción de vías y la deforestación.

<iframe id="audio_37718248" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_37718248_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
