Title: ONU llama la atención sobre posible impunidad frente a crímenes por parte de agentes estatales
Date: 2017-03-16 20:00
Category: Nacional, Paz
Tags: crímenes de estado, impunidad, informe onu, ONU, paz
Slug: onu-llama-la-atencion-posible-impunidad-frente-crimenes-parte-agentes-estatales
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/03/falsos-positivos-e1459276020392.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto:  Europapress 

###### [16 Mar 2016]

Aunque en el informe 2016 de la Oficina del Alto Comisionado de Derechos Humanos en Colombia, reconoce el carácter innovador del acuerdo de paz en Colombia, por otra parte ve con preocupación, los vacíos que quedaron en la reciente aprobada Jurisdicción Especial para la Paz, frente al blindaje de actores estatales, y específicamente frente a la responsabilidad penal de los superiores civiles o militares en las violaciones de derechos humanos.

La ONU señala que para contribuir a la no-repetición, el reconocimiento de las violaciones cometidas por agentes estatales “**debe tomar en cuenta las dimensiones estatales, políticas, institucionales e individuales, en su totalidad”.** En ese sentido añade que en un país que le apuesta a la paz, no es posible que todavía existan sectores oficiales y políticos que nieguen que la participación  de servidores públicos graves crímenes de lesa humanidad.

En ese sentido, el Alto Comisionado hace un fuerte llamado al Estado colombiano, pues manifiesta que con el acto legislativo sobre la JEP, se restringe y distorsiona el marco legal que deben aplicar los magistrados en los casos de violaciones de derechos humanos cometidos por miembros de las fuerzas militares o de la policía y no cumple con los estándares internacionales sobre responsabilidad del superior y del mando.

**“La definición de “control efectivo” en el Acuerdo de Paz, en relación con la responsabilidad penal de los superiores civiles o militares, es parcial y no cumple con los estándares internacionales.** El control efectivo, así como el alcance de las amnistías, debe interpretarse en consonancia con las normas y jurisprudencia internacionales, con el fin de garantizar los derechos de las víctimas a la justicia y la no-repetición. La Fiscal de la Corte Penal Internacional podría manifestarse públicamente para dar claridad sobre las normas aplicables relacionadas con la responsabilidad del superior”, dice el informe.

A su vez, lamenta que, en un estado de derecho, **no se haya asignado a una entidad independiente e imparcial que aplicaría los criterios para determinar qué actores pueden beneficiarse de la amnistía,** ya que no se tuvo en cuenta que no exista ninguna relación con los posibles actores estatales que puedan gozar de esta medida. Asimismo, advierte que tampoco se establece un sistema efectivo para el seguimiento y control de los miles de posibles beneficiarios, con el fin de evitar la obstrucción de la justicia y la intimidación de las víctimas y los testigos.

De esta manera, la ONU hace una fuerte crítica al Estado colombiano, pues asegura que el Estado continúa sin tomar reales medias, para reconocer plenamente sus responsabilidades por las violaciones de derechos humanos y del derecho internacional humanitario en el marco del conflicto armado. Lo que genera la revictimización de las personas afectadas por la guerra.

Otras consideraciones del informe

-   Las solicitudes de perdón de las FARC-EP cumplirán su objetivo una vez las víctimas las acepten y comiencen a recibir los beneficios de acciones más amplias de reparación.
-   No se debe postergar la desvinculación de niñas y niños de las FARC-EP, la cual fue acordada por las partes.
-   La no-repetición implica reformas estructurales institucionales, con la participación de las víctimas y de la sociedad, a fin de garantizar el respecto, la protección y la garantía de los derechos humanos y el goce de una vida digna. Cualquier integrante de las FARC-EP implicado en delitos después de su desmovilización debe ser procesado por la jurisdicción ordinaria y no el sistema de justicia transicional.
-   Es necesario introducir cambios legales, institucionales, presupuestales y culturales relacionados con la identificación, protección, acceso y uso de archivos estatales para facilitar el trabajo del sistema de justicia transicional.
-   Es urgente lograr la inclusión política, social y económica y el empoderamiento de los líderes comunitarios.

<iframe id="doc_66264" class="scribd_iframe_embed" src="https://www.scribd.com/embeds/342129741/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-QQmE1fBvFP516HbENsdc&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="0.7729220222793488"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio]{.s1}](http://bit.ly/1ICYhVU)[.]{.s1}

 
