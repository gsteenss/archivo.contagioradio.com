Title: Por primera vez la protección animal hace parte del Plan Nacional de Desarrollo
Date: 2015-05-06 11:20
Author: CtgAdm
Category: Voces de la Tierra
Tags: Animales, Congreso, Juan Carlos Losada, Natalia Parra Osorio, Onda Animalista, Plan Nacional de Desarrollo 2014 - 2018, Plataforma Alto, PND, Política pública, protección animal, Senador Guillermo García
Slug: por-primera-vez-los-animales-hacen-parte-del-plan_nacional_de_desarrollo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/10/Marcha-No-a-las-corridas-de-toros-11-.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### Foto: Contagio Radio 

###### [5 May 2015] 

<iframe src="http://www.ivoox.com/player_ek_4455941_2_1.html?data=lZmil56YdY6ZmKiak5aJd6Kll5KSmaiRdo6ZmKiakpKJe6ShkZKSmaiRhc%2FdzsbZx9iPrMLXxtOY0sbWuMafxcrZjbWyiIzk0NeY0tfNscbmwpDjx9%2BRaZi3jqjc0NnFq8rjjLfOxs7Tb46ZmKialg%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Guillermo García, Senador Partido Liberal] 

Este martes fue aprobado en la plenaria del senado el **artículo 252 del Plan Nacional de Dasarrollo, que promueve la protección animal** y que fue propuesto por el senador del Partido Liberal, **Guillermo García Realpe**, quien asegura, que se trata de "un avance histórico que se haya incluido este artículo en pro de la protección animal en un PND".

De acuerdo al senador, **"luego de la continua lucha logramos que a través de una proposición se estableciera en el PND** la creación por parte del Gobierno Nacional de una política pública que fomente, promulgue y difunda los derechos y la protección de los animales”.

**Se trata de una propuesta de la bancada animalista del Congreso** de la cual hace parte el senador García Realpe, en compañía de la directora de la Plataforma ALTO, Natalia Parra, quienes aportaron a este decreto donde se establece que "las entidades territoriales y descentralizadas del Estado se encargarán de vigilar, controlar y fomentar el respeto por los animales y su integridad física y anímica", según comunicado de prensa de García Realpe.

“De forma coordinada con las entidades nacionales y territoriales tendrán la tarea de difundir y crear conceptos en donde se establezcan especificaciones sobre el cuidado animal en cuanto a la reproducción, tenencia, adopción, producción, distribución y comercialización de animales domésticos no aptos para reproducirse”.

**Las organizaciones animalistas y la sociedad civil, representan una parte crucial en la implementación de esta política pública,** ya que ellos harán parte del seguimiento para que esta iniciativa sea una realidad.

Este es el artículo 252 incluido en el PND

*El Gobierno Nacional promoverá políticas públicas y acciones gubernamentales en las cuales se fomenten, promulguen y difundan los derechos de los animales y/o la protección animal. Para tal efecto, en coordinación con las organizaciones sociales de defensa de los animales, diseñará una política en la cual se establecerán los conceptos, competencias institucionales, condiciones, aspectos, limitaciones y especificaciones sobre el cuidado animal en cuanto a la reproducción, tenencia, adopción, producción, distribución y comercialización de animales domésticos no aptos para reproducirse. Las entidades territoriales y descentralizadas del Estado se encargarán de vigilar, controlar y fomentar el respeto por los animales y su integridad física y anímica. Adicionalmente, las organizaciones sociales de defensa de los animales participarán de manera coordinada con las entidades nacionales y territoriales para la difusión de las políticas a que se refiere el presente artículo.*

\[embed\]https://www.youtube.com/watch?v=\_SSz-zN\_o2o&feature=youtu.be\[/embed\]

#### [Asesora de Senador Guillermo García explica el artículo 252 por los animales en el Plan Nacional de Desarrollo]
