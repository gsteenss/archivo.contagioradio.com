Title: Concejo de Bogotá decide sobre Consulta Antitaurina
Date: 2015-07-10 12:43
Category: Animales, Voces de la Tierra
Tags: Alianza Verde, Animalistas, Concejo de Bogotá, Consulta Antitaurina, consulta popular, María Fernanda Rojas, Planton carnaval
Slug: hoy-concejo-de-bogota-decide-sobre-consulta-antitaurina
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/07/DSC1064.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Contagio Radio 

##### <iframe src="http://www.ivoox.com/player_ek_4761856_2_1.html?data=lZyjk52Zeo6ZmKiak5eJd6KmkZKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRkcLmhqigh6aopYy6xtfbw9PIpYzG0M%2FO1YqWh4y30NPQx8%2FFsIzYxpCv0czTuIa3lIquk5KJe6ShpNTb1sbLrdCfs8bRy9SPcYarpJKh&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>  
[María Fernanda Rojas, Concejal de Bogotá] 

Este viernes ha sido convocada la instalación de las sesiones extraordinarias, para la votación del concepto por parte del Concejo de Bogotá para la realización de una Consulta Popular, con el objetivo de que la ciudadanía de su opinión frente a la pregunta **¿Está usted de acuerdo, si o no, con que se realicen corridas de toros y novilladas en Bogotá D.C.?**

De acuerdo a María Fernanda Rojas, concejal de Bogotá, por la Alianza Verde, **en términos generales el Concejo tiene una postura favorable a la Consulta Antitaurina,** y añade que su bancada apoya la consulta popular. “Puede que el toreo tenga elementos artísticos pero el eje central es la tortura de un animal”, expresa la concejal.

Durante el debate participarán organizaciones en contra y a favor de este instrumento de participación ciudadana, y además, se realizará un **plantón carnaval desde la 1:00 de la tarde frente al Consejo,** con el fin de pedir un pronunciamiento favorable frente a la Consulta Antitaurina.

“Este debate va a estar muy interesante, hay que demostrar que una práctica donde se tortura a los animales no tiene el arraigo cultural que se les ha reconocido en la jurisprudencia y en la Ley… **algunos pensamos que este tipo de espectáculos ya no tienen un arraigo**”, afirmó María Fernanda Rojas.

Desde Onda Animalista los invitamos a todos y todas a que asistan al plantón por la vida de los toros que se realizará el próximo viernes a la 1 de la tarde.
