Title: Dos civiles muertos deja operación militar contra disidentes de las FARC en Caquetá
Date: 2018-05-28 12:08
Category: DDHH, Política
Tags: Agresiones fuerza pública, Caquetá, Disidentes de las Farc, Fuerza Pública
Slug: dos-civiles-muertos-deja-operacion-militar-contra-disidentes-de-las-farc-en-caqueta
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/10/Ejercito-Popular-de-Liberación-e1527526839809.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: EFE] 

###### [28 May 2018] 

Luego de que el Ministro de Defensa, Luis Carlos Villegas, confirmara que el Ejército de Colombia realizó una operación militar en Caquetá, en el que fueron abatidos once disidentes de las FARC, la comunidad del caserío Miramar en el municipio de Montañita, indicaron que **dos personas eran civiles** y hacían parte de la Junta de Acción Comunal.

### **Dos de los hombres asesinados eran civiles integrantes de la comunidad de Miramar** 

Según lo expresado por una integrante de las comunidades, en horas de la noche del domingo 27 de mayo, un grupo de hombres armados **ingresó al caserío** y obligó a Juvenal Silva y a Jefferson Monroy, integrantes de la comunidad, a conducir un vehículo con la gente armada hacia Montañita.

Ante la preocupación de que los dos hombres no regresaban, “a la 1:00 am la gente salió a buscarlos” y encontraron que “se había presentado **un asalto por parte del Ejército** y el Gaula de la Policía donde mataron a quienes iban en el carro”. Afirmó la persona que Silva y Monroy también fueron asesinados. (Le puede interesar:["Atentan contra la vida del dirigente comunal Pedro Guzmán en la vereda Nueva Etapa en Caquetá"](https://archivo.contagioradio.com/atentan-contra-la-vida-del-dirigente-comunal-pedro-guzman-en-la-vereda-nueva-etapa-caqueta/))

Ante estos hechos, la comunidad enfatizó en que **tiene como comprobar** que los hombres hacen parte de la sociedad civil y que “fueron obligados a llevar al grupo de los hombres armados”. Adicionalmente, rechazaron las afirmaciones del Ejército Nacional, quien aseguró que todos los hombres asesinados hacían parte de las disidencias de las FARC.

### **Comunidad se ha movilizado para reclamar por el asesinato de los dos pobladores** 

Ante las acciones de la Fuerza Pública, la comunidad de Miramar expresó su preocupación y pidieron que se aclare la situación de Silva y Monroy. Afirmaron que las unidades militares **tienen el sitio acordonado** y “no han permitido la verificación de la Junta de Acción Comunal”.

Agregaron que es necesario que se tenga en cuenta que dos civiles fueron asesinados por lo que esperan que se traslade una comisión de verificación desde Florencia donde haya presencia de organizaciones que defienden los derechos humanos. Recordaron que esta zona de Caquetá **“está desprotegida por el Estado”** y temen que se vuelvan a presentar casos de “falsos positivos” donde los habitantes son presentados como muertos en combate.

Por su parte, el **Ejército Nacional** indicó que sus tropas realizaron una operación anti extorsión y secuestro en el municipio de la Montañita donde "fueron neutralizados en combate 13 sujetos". Afirmaron que se trataría de integrantes de los grupos residuales de las FARC que hacen parte de los frentes 3 y 7 "cuyo cabecilla principal es alias Rodrigo Cadete".

<iframe id="audio_26219925" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_26219925_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
