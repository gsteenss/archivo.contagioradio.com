Title: El pedido de perdón de la iglesia después de la visita del papa Francisco
Date: 2017-09-30 09:52
Category: Abilio, Opinion
Tags: Gaitanismo, Iglesia, Teologia
Slug: el-pedido-de-perdon-de-la-iglesia-despues-de-la-vista-del-papa-francisco
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/09/iglesia-pide-perdon.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Contagio Radio 

#### Por **[Abilio Peña ](https://archivo.contagioradio.com/abilio-pena-buendia/) - [~~@~~Abiliopena](https://twitter.com/Abiliopena){.twitter-atreply .pretty-link} **

###### 30 Sep 2017

[El tres y cuatro de septiembre se celebraron en  Bogotá los actos litúrgico  y académico de pedido de perdón a nombre de más de mil sacerdotes, religiosas, religiosos, seglares, teólogas y teólogos de diversos países del mundo,  por la responsabilidad de la iglesia en la violencia en Colombia.]

En el acto registrado por decenas de medios de información  nacionales e internacionales, intervinieron  representantes de las víctimas del Gaitanismo y el  partido liberal; del Partido Comunista de Colombia; campesinos víctimas del grupo paramilitar “los 12 apóstoles”. También concurrieron  familiares de  víctimas que fueron  torturadas en  las guarniciones militares,  en presencia de sacerdotes de la hoy diócesis castrense;  y también representantes de los pueblos indígenas víctimas  de la “evangelización” con la cruz  y la espada en donde murieron miles de ellos y ellas, siendo  objeto, además,  del despojo de sus espiritualidades y bienes (1).

Este acto inspirado en buena parte por la humilde actitud del papa Francisco de pedir perdón por los pecados de la iglesia en otros países,  en continuidad con la actitud penitencial del papa Juan Pablo II, encontró en sus palabras en  Colombia, nuevamente invitaciones a reconocer su responsabilidad y a pedir perdón, aunque  de manera expresa no manifestó de qué y por qué. En sus discursos y homilías en treinta oportunidades habló del perdón y en tres ocasiones se refirió a la necesidad de hacerlo cómo iglesia, enseñando primero con el ejemplo: “yo  también tengo que pedir perdón”. Luego su llamado a perder el  miedo: “Colombia: no le tenga miedo a pedir y ofrecer el perdón”.

En Medellín en presencia de sacerdotes, religiosas, obispos y sus familiares, fue claro y contundente: “Seamos hombres y mujeres reconciliados para reconciliar. Haber sido llamados no nos da un certificado de buena conducta e impecabilidad; no estamos revestidos de una aureola de santidad. “Guai” del religioso, el consagrado, el cura o la monja que vive con cara de estampita, por favor, “guai”. Todos somos pecadores, todos necesitamos del perdón y la misericordia de Dios para levantarnos cada día”.

Semanas antes de la liturgia en la que católicos pidieron perdón,  el Arzobispo de Bogotá fue informado, invitado y se le solicitó autorización para celebrar el acto en la Basílica del Voto Nacional, por su significado en la reconciliación nacional. De él llegaron tres respuestas.

La primera, esperanzadora: “Valoramos profundamente  que una parte de los fieles católicos de nuestros días quieran pedir perdón por actos violentos cometidos por otros cristianos en el pasado. Estos gestos hacen parte de la construcción de la reconciliación nacional”. La segunda entrecerró puertas más dejó  posibilidades de que pidieran perdón en otro momento y circunstancias: “(…) me permito aclarar que no están autorizados ustedes en esta eucaristía  para pedir perdón en nombre de la iglesia por los eventuales errores que ella haya podido cometer en el curso de la historia. Este es un tema que debe ser tratado con mayor profundidad y delicadeza y por tanto, no corresponde realizar ese gesto en esta celebración”. Además  agradece la preocupación por las víctimas de la violencia. La tercera,  desconcertante, luego de haber contado con la autorización del párroco para celebrar en la Basílica del Voto que estaba en reconstrucción: “me permito comunicarle que no es posible realizar esta eucaristía debido a los trabajos de restauración que actualmente se adelantan al interior de la Basílica. (…) Agradezco su comprensión sobre este tema y los animo a seguir trabajando con empeño en pro de la reconciliación nacional.”.

Creemos que luego del gesto de los más de mil católicos y de la visita del papa Francisco, queda el camino abierto para que el paso lo den ahora los jerarcas de la iglesia. El nuevo presidente de la Conferencia Episcopal, Monseñor Oscar Urbina,  al  escuchar al papa manifestó éstas palabras esperanzadoras: “aceptamos su invitación a ser iglesia en salida, comprometida con la paz, la justicia, los pobres”. Y más recientemente el arzobispo de Bogotá, cardenal Rubén Salazar  expresó en un tweets: “El Papa en nombre del Señor sembró las semillas de perdón, reconciliación, fraternidad, solidaridad. Nos toca ser tierra buena y dar fruto”.

Ese es un importante  llamado del papa Francisco para los obispos de la iglesia católica y una respuesta que abre las puertas al reconocimiento de las responsabilidades, a la purificación de la memoria,  a  la confesión pública y a la reconciliación con las víctimas. Esperamos se dé más temprano que tarde.

1.  ###### (ver,: <http://canal1.com.co/noticias/religiosos-piden-perdon-por-el-papel-de-la-iglesia-en-los-50-anos-de-guerra>/ - <http://beta.elespectador.com/noticias/bogota/miembros-de-la-iglesia-catolica-piden-perdon-las-victimas-del-conflicto-articulo-711403>).

#### **[Leer más columnas de Abilio Peña](https://archivo.contagioradio.com/abilio-pena-buendia/)**

------------------------------------------------------------------------

###### Las opiniones expresadas en este artículo son de exclusiva responsabilidad del autor y no necesariamente representan la opinión de Contagio Radio
