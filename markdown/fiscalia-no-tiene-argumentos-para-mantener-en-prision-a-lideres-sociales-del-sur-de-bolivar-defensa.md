Title: Fiscalía no tiene argumentos para mantener en prisión a líderes sociales del Sur de Bolívar
Date: 2017-03-29 13:42
Category: DDHH, Judicial, Nacional
Tags: congreso de los pueblos, Lideres, Sur de Bolivar
Slug: fiscalia-no-tiene-argumentos-para-mantener-en-prision-a-lideres-sociales-del-sur-de-bolivar-defensa
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/06/bogota.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo] 

###### [29 Mar. 2017]

**“Por organizar marchas, exigir igualdad y tener voz política”** la Fiscal encargada \#3 de los Juzgados de Cartagena, María Bernarda Puentes, **consideraría deben ser asegurados varios líderes sociales integrantes del Congreso de los Pueblos** que fueron capturados la semana pasada, dentro de los cuales se encuentra la reconocida lideresa Milena Quiroz. Le puede interesar: ([Así fueron las captras de integrantes del Congreso de los Pueblos](https://archivo.contagioradio.com/capturas-congreso-de-los-pueblos/))

Este miércoles, es el día para que la Juez segunda de control de garantías en Cartagena tome una determinación al respecto. Sin embargo, **Deivis Flórez** **abogado** del Comité de Solidaridad con Presos Políticos **(FCSPP)**, aseveró que los **argumentos de la Fiscalía, son totalmente absurdos** “violatorio de cualquier tipo de derechos procesal e incluso del derecho internacional”.

Para Flórez, **la Fiscal parece haber olvidado que “protestar y organizarse es un derecho fundamental** incluso reconocidos por los tratados internacionales suscritos por Colombia. En ninguna parte del código penal se prohíbe organizar marchas o asociarse como lo plantea la Fiscalía” agregó. Le puede interesar: [Capturas de líderes del Congreso de los Pueblos podrían declararse ilegales](http://Le%20puede%20interesar:%20(Así%20fueron%20las%20captras%20de%20integrantes%20del%20Congreso%20de%20los%20Pueblos))

Según el jurista **estos argumentos demuestran de manera clara la pretensión de judicializar de manera burda al aparato judicial y una criminalización a la protesta social** “y evidentemente esto es una acción política para reprimir el movimiento social y político de la zona del sur de Bolívar” añade.

Otro de los argumentos presentados por la Fiscalía es que **Milena Quiroz** es “una persona que está dedicada a la política (…) es una persona que tiene muy buen manejo de sus relaciones como política, desde el punto de vista no sólo locales sino nacionales (…) la Fiscalía siempre ha dicho que **está dentro del brazo político del ELN” **Le puede interesar:[ Congreso de los pueblos denuncia dos atentados ocurridos en 48 horas](https://archivo.contagioradio.com/atentados-congreso-de-los-pueblos/)

Declaración que para Flórez **carece de sustento dado que esa información está basada en una serie de declaraciones de unos testigos que no fueron acreditados** en la audiencia “no hay certeza de quienes son estas personas, ni que calidad tienen para hacer dichos señalamientos. Se habla de unos reconocimientos. Se logró demostrar que una sola persona hace hasta 40 reconocimientos fotográficos de presuntos “criminales” en 2 horas, lo cual es imposible”.

**Según el integrante de FCSPP se están enfrentando a una Fiscalía que no ha tenido un proceso de investigación** “no se reunieron los elementos que requiere la ley para adelantar este tipo de investigación. Pero además lo señalamientos que utilizó para sustentarlo dejó muestra de que lo que se intenta es **judicializar, criminalizar la protesta social y a los líderes por ejercer liderazgos dentro de las comunidades”**

Lo que se espera ahora es que la Juez tomé una decisión luego de conocer los argumentos de las partes “esperamos que se corrija que se le está causando no solo a estas personas sino a los principios del Estado social de derecho” concluyó Flórez.

**En los próximos días el proceso continuará a la espera de la presentación del escrito de acusación** que dirá si se precluye o continua el juicio en contra de estos líderes sociales del Congreso de los Pueblos

<iframe id="audio_17847847" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_17847847_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]

######  
