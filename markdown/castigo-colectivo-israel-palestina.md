Title: Castigo Colectivo: la guerra psicológica contra los palestinos
Date: 2017-09-04 17:01
Category: Onda Palestina
Tags: Apartheid, BDS, Israel, Palestina
Slug: castigo-colectivo-israel-palestina
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/09/Palestina-presion.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Habitantes de Deir Abu Mash'al 

###### 04 Sept 2017 

En junio de 2017 tres residentes de la aldea Deir Abu Mash'al realizaron un ataque cerca de la Puerta de Damasco en Jerusalén Este, matando a la oficial de policía fronteriza Hadas Malka, de 23 años, e hiriendo a otras cuatro personas. Los tres atacantes, Baraa Saleh, 18, Adel Ankush, 18 y Ousama Ata, de 19 años, fueron muertos a tiros por las fuerzas de seguridad israelíes durante el ataque.

Desde entonces, Israel ha estado interrumpiendo las vidas de todos los residentes del pueblo, aunque no estuvieron involucrados en el ataque y no han sido acusados ​​de ningún delito a manera de castigo colectivo.

La aldea palestina de Deir Abu Mash'al se encuentra a unos 25 kilómetros al  oroeste de Ramallah. Tiene una población de unos 5.000, muchos de los cuales trabajan en Ramallah. Alrededor de 150 de los residentes trabajan en Israel y tienen que cruzar el puesto de control Nilin en su viaje todos los días.

Después del ataque, agentes de la Policía de Fronteras y soldados entraron en la aldea e impusieron varias medidas restrictivas: En la noche del ataque, las fuerzas de seguridad israelíes instalaron una puerta de hierro en la entrada principal del pueblo y se negaron a permitir que nadie la cruzara en ninguna dirección, ni a pie ni en coche.

Los militares también bloquearon los tres caminos de tierra que conducían al pueblo con piedras y pilas de tierra. Al día siguiente, los residentes del pueblo eliminaron parte del bloqueo de dos carreteras secundarias, lo suficiente para permitir que sólo un auto 4X4 pudiera atravesar por ella.

El lunes 19 de junio de 2017, las fuerzas de seguridad permitieron que la mayoría de los habitantes de la aldea abandonaran el pueblo a pie por la entrada principal, con excepción de los hombres de 15 a 25 años. Al día siguiente, las fuerzas abrieron la puerta del camino,  
permitiendo que los coches pasarán, pero sólo después de largos controles, incluyendo búsquedas exhaustivas de los coches. Además de la prohibición de salir de la aldea para hombres de 15 a 25 años.

No fue hasta la noche del sábado 24 de junio de 2017, cuando comenzó la fiesta musulmana de Eid al-Fitr, que las fuerzas de seguridad hicieron más fácil viajar en coche. Sin embargo, la puerta de hierro permanece en su lugar y las fuerzas se colocan junto a él alguna parte del tiempo, realizando controles aleatorios.

Además de las restricciones de circulación, que han afectado a toda la población del pueblo, las fuerzas de seguridad confiscaron unos 50 vehículos de los residentes. Además, 50 residentes del pueblo que trabajan en Israel - todos de las familias extensas de los asaltantes -tuvieron sus permisos de trabajo revocados cuando llegaron al puesto de control en su camino al trabajo, sin previo aviso y sin oportunidad de impugnar la decisión.

El domingo 18 de junio, los soldados allanaron cinco hogares en el pueblo, tres de ellos pertenecientes a las familias de los asaltantes. Los soldados informaron a los padres de los asaltantes que sus casas serian demolidas por las acciones de sus hijos. Si esto ocurre, 15 personas, incluyendo 5 menores inocentes, se quedarán sin un techo sobre sus cabezas.

Los palestinos han afirmado desde hace mucho tiempo que esta política es una forma de castigo colectivo, dirigida a las familias de agresores, al tiempo que impide que las familias de palestinos muertos soliciten autopsias apropiadas a sus seres queridos.

Desde el ataque, los militares han estado interrumpiendo la vida de los casi 5.000 habitantes de la aldea, aunque no se les ha acusado de ningún delito personal. Esta forma automática de represalia se ha convertido en una cuestión de política para los militares, en un abuso cínico de su poder para maltratar a los civiles.

<iframe id="audio_20683697" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_20683697_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Escuche esta y más información sobre la resistencia del pueblo palestino en [Onda Palestina](https://archivo.contagioradio.com/programas/onda-palestina/), todos los Martes de 7 a 8 de la noche en Contagio Radio. 
