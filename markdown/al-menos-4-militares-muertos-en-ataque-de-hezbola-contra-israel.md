Title: Al menos 2 militares muertos en ataque de Hezbolá contra Israel
Date: 2015-01-28 16:19
Author: CtgAdm
Category: El mundo, Otra Mirada
Tags: Altos del Golan, Bombardeo, Hezbolá, Israel, Líbano
Slug: al-menos-4-militares-muertos-en-ataque-de-hezbola-contra-israel
Status: published

###### **Foto:RT** 

**Entrevista con Víctor de Currea Lugo:**  
<iframe src="http://www.ivoox.com/player_ek_4010146_2_1.html?data=lZWekpaYeo6ZmKiak5WJd6Kpl5KSmaiRdo6ZmKiakpKJe6ShkZKSmaiReIzhytHW1sbWqdSfztrS1NnTt4zZz5DO1sbVucafxcqYqsreptDgws2YxdTSuNPVjMrXh6iXaaKt08iah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

La **respuesta al ataque israelí** del pasado domingo contra posiciones de Hezbolá en los altos del Golán, dentro de la parte Siria, en la ciudad de Qunetria no se ha hecho esperar, el movimiento político y armado libanés **Hezbolá ha lanzado un misil antitanques alcanzando posiciones israelíes y matando al menos a 4 soldados.**

El pasado domingo **Israel abatió a 4 miembros del movimiento político libanés**, incluido un conocido líder buscado por EEUU, a lo que el líder de la formación islamista dijo mediante una entrevista televisiva que sería respondido con misiles de procedencia iraní.

Israel ha respondido bombardeando posiciones y localidades del sur del Líbano, en la que **ha muerto un casco azul español**, perteneciente al contingente de **Naciones Unidas** en la misión para la estabilidad del Líbano.

**Hezbolá nace en 1982 como movimiento de resistencia  a la ocupación israelí** y para defender los intereses de la comunidad chií. De influencia iraní, tanto a nivel de financiación como a nivel ideológico, es además de un grupo armado, el partido político más importante en el sur del estado y uno de los principales a nivel parlamentario.

Victor de Currea Lugo explica que el objetivo de Israel con dichos ataques tiene que ver con las **próximas elecciones**, en que el primer ministro Bejamin Netanyahu, podría perder debido a la crisis económica social y política que atraviesa el país hebreo. Es por ello que **está implementando una estrategia del miedo** para poder conseguir la victoria en las elecciones próximas.

Aprovechamos la presencia del profesor para preguntarle sobre las negociaciones de paz entre el gobierno sirio y la oposición, extrayendo la conclusión de que actualmente es difícil saber quiénes la integran y si realmente representan al conjunto de toda la oposición.
