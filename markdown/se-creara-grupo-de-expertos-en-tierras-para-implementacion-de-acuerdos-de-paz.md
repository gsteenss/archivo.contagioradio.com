Title: Se creará Grupo de Expertos en Tierras para implementación de Acuerdos de Paz
Date: 2017-03-14 16:31
Category: Nacional, Paz
Tags: implementación acuerdos de paz, restitución de tierra
Slug: se-creara-grupo-de-expertos-en-tierras-para-implementacion-de-acuerdos-de-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/03/tierras-curvarado.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [14 Mar 2017] 

**Derecho a la propiedad de la tierra, ocupantes y poseedores de buena fe, función ecológica y social de la propiedad, facilidades para el acceso a la tierra y promoción del uso productivo** son los temas que trabajaría el grupo de expertos que según la La Comisión de Seguimiento, Impulso y Verificación a la implementación del Acuerdo Final tendría que comenzar a funcionar de manera urgente.

El grupo que estará conformado por tres personas, tendrá tres meses para hacer recomendaciones en torno a** reformas normativas y políticas públicas que permitan que el punto sobre acceso a la propiedad de tierras, pactado en el acuerdo, sea una realidad.**

Además, la Comisión de Seguimiento, Impulso y Verificación a la implementación del Acuerdo Final solicitó a los rectores de la Universidad de los Andes, la Universidad EAFIT y la Universidad Externado de Colombia, que designe a una persona que haga parte de su planta docente, experta en el tema de tierras, **para que lidere un equipo en su institución y trabaje en conjunto con el grupo de expertos.**

En el documento, la Comisión de Seguimiento señaló que **“actualmente existen distintas situaciones que afectan la seguridad jurídica sobre la tenencia o la propiedad de la tierra en Colombia**” por lo cual es necesario que se encuentre una solución que atienda a las realidades del país, sin “perjuicio” de lo establecido en materia de acceso a la tierra. Le puede interesar: ["Indígenas Wounnan exigen garantías para retornar a sus tierras"](https://archivo.contagioradio.com/entrevista-indigenas-nonam/)

Las recomendaciones y propuestas normativas **serán entregadas al Gobierno Nacional** para atender los procedimientos dispuestos en el Acuerdo Final. Le puede interesar: ["Exequibilidad de Ley ZIDRES atenta contra Banco de Tierras por la Paz"](https://archivo.contagioradio.com/exequibilidad-de-ley-zidres-atenta-contra-banco-de-tierras-por-la-paz/)

###### Reciba toda la información de Contagio Radio en [[su correo]
