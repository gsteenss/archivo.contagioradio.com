Title: MOE pide compromiso de transparencia en elección de contralor
Date: 2018-08-15 15:49
Category: Nacional, Política
Tags: Alejandra Barrios, Contraloría General de la Nación, MOE
Slug: moe-pide-compromiso-de-transparencia-en-eleccion-de-contralor
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/08/contral.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:  W Radio] 

###### [15 Ago 2018] 

Desde la Misión de Observación Electoral, MOE, elevaron un llamado a los presidentes de Senado y Cámara, para que se garantice la **información pública pertinente a la elección del nuevo Contralor General de la Nación**.

La solicitud de la MOE al Congreso, consiste en que se hagan públicos los posibles conflictos de interés,  relaciones laborales, familiares y económicas con otros funcionarios del Estado, entre otros temas, de quienes se presentaron como candidatos a Contralor General de la Nación, con la intensión de que exista un compromiso con la transparencia.

Para Alejandra Barrios, directora de la MOE, “quien sea candidato a Contralor General debe comprometerse con altos estándares de transparencia como el publicar de manera activa información relevante que pueda afectar o condicionar su labor en el cargo. Por eso es necesario que esta información sea publicada en los sistemas de información del Congreso de la República para el escrutinio de toda la ciudadanía.”

### **Hay que garantizar la transparencia** 

Adicionalmente a lo establecido en la ley 1904 de 2018, la Misión de Observación Electoral hizo un llamado para que las diez personas preseleccionadas por el Congreso para ser el próximo Contralor General de la República hagan pública la siguiente información:

**La declaración de renta y la declaración de bienes y patrimonio de los tres años anteriores a la fecha de la elección, hagan público y de manera específica los conflictos de intereses que tendrían** al momento de ejercer el cargo, si han militado, apoyado o declarado pertenencia frente a algún partido con representación en el Congreso de la República.

Otra de las informaciones es si han **financiado a través suyo o de alguna empresa u organización alguna campaña electoral** u organización política, si tienen alguna relación familiar (parentesco hasta el cuarto grado de consanguinidad, segundo de afinidad, primero civil, o ligado por matrimonio o unión permanente) con personas de la rama ejecutiva, legislativa o judicial.

Si tienen alguna relación comercial con personas con parentesco hasta el cuarto grado de consanguinidad, segundo de afinidad, primero civil, o ligado por matrimonio o unión permanente de la **rama ejecutiva, legislativa o judicial** y si han tenido investigaciones en los cargos públicos o privados y en qué estado se encuentran.

“Estas medidas servirán para que el proceso de elección del Contralor sea mucho más transparente y, sobre todo, para que los Congresistas, medios de comunicación y ciudadanía puedan contar con elementos de control social previo a la elección de uno de los servidores públicos con mayor relevancia en el Estado Colombiano” Concluyó la directora de la MOE.

### **¿De qué se encarga el Contralor General de la Nación?** 

La necesidad de garantizar que el Contralor sea una persona idónea para realizar su actividad radica en que este será el encargado de hacer la vigilancia fiscal de la administración  estatal y de los particulares o entidades que manejen fondos o bienes de la Nación. (Le puede interesar: ["Es necesaria una reforma política integral para que haya apertura democrática"](https://archivo.contagioradio.com/reforma_politica_congreso_paz/))

Motivo por el cual, según Fabian Hernández, integrante de la MOE, "**se debe garantizar independencia, providad en la hoja de vida** y que estos cargos estén en manos de la mejor persona que pueda defender a los colombianos, sus recursos públicos y a la gestión que hacen los funcionarios públicos".

<iframe id="audio_27868589" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_27868589_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]

######  
