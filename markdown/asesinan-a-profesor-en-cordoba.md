Title: Asesinan a profesor Washington Cedeño en Córdoba
Date: 2017-06-07 13:49
Category: DDHH, Nicole
Tags: asesinato de docente, Docentes, fecode, paro, profesores
Slug: asesinan-a-profesor-en-cordoba
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/06/Washington-Cedeño.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: IE Sabalito Arriba] 

###### [07 Jun. 2017]

Mientras miles de profesores se movilizaban desde todos los rincones del país para arribar a la capital y exigir mejoras salariales e inversión para la educación pública, en el corregimiento de Sabalito, zona rural de Puerto Escondido en Córdoba fue asesinado de **varios tiros el docente Washington Cedeño Otero,** quien se desempeñaba desde hace más de 20 años como docente de básica primaria en la Institución Educativa Sabalito Arriba.

El profesor Cedeño era **licenciado en lengua castellana e integrante de la Asociación de Maestros y trabajadores de la Educación de Córdoba (ADEMACOR)**, organización que entregó la denuncia. Era padre de cuatro hijos, todos menores de edad. Le puede interesar: [Profesores persisten en vigilia de Iglesia de San Francisco sin agua, luz ni comida](https://archivo.contagioradio.com/profesores-persisten-en-toma-de-iglesia-de-san-francisco-sin-agua-luz-ni-comida/)

La comunidad del sector, aseguró a los integrantes de ADEMACOR que el docente salió de las instalaciones de la institución educativa hacia el mediodía del 6 de junio y cerca de 300 metros después “**hombres armados, sin mediar palabra le dispararon indiscriminadamente,** dejándolo tendido ante la mirada impávida de padres de familia y estudiantes” quienes habían sido citados por Cedeño para realizar una jornada de aseo en el colegio.

Según cifras entregadas por ADEMACOR **desde el año 2010 hasta la fecha han sido asesinados 17 docentes en Córdoba,** ubicándolo como el departamento que tiene el índice más alto de homicidios contra profesores en el país. Le puede interesar: [Paramilitares amenazan a estudiantes, profesores y trabajadores de la UIS](https://archivo.contagioradio.com/paramilitares-amenazan-a-estudiantes-profesores-y-trabajadores-de-la-uis/)

A través de un comunicado ADEMACOR manifestó que el profesor Washington no había dado a conocer que fuera víctima de amenazas. Así mismo, se solidarizó con la familia del docente y repudiaron estos hechos por los que exigen investigaciones eficaces que den con los responsables. Le puede interesar: [Explosión en Bogotá pudo estar dirigida contra FECODE](http://Explosión%20en%20Bogotá%20pudo%20estar%20dirigida%20contra%20FECODE)

![Comunicado](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/06/WhatsApp-Image-2017-06-06-at-7.33.06-PM-1.jpeg){.alignnone .size-full .wp-image-41867 .aligncenter width="960" height="1280"}

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU).

<div class="osd-sms-wrapper">

</div>
