Title: Cortometrajes  contra la violencia hacia la mujer
Date: 2016-10-10 09:00
Category: eventos
Tags: #CortemosLaViolencia, cortometrajes, Latinoamérica, mujer, Violencia de género
Slug: cortometrajes-contra-la-violencia-hacia-la-mujer
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/10/CuFocqOW8AEFwUD.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Cortemos la Violencia 

###### 7 Oct 2016 

Con el objetivo de promover la producción y difusión de mensajes audiovisuales que contribuyan a detener, erradicar y transformar las distintas manifestaciones de violencias basadas en género, se realiza el primer concurso de vídeo corto **por una vida libre de violencias para las mujeres jóvenes,** bajo el hashtag **\#CortemosLaViolencia. **El plazo para enviar los vídeos va hasta el 15 de octubre de 2016.

La iniciativa premiará a los tres mejores videos con un reconocimiento simbólico por su trabajo, junto con todos los gastos pagos para viajar a Bogotá (Colombia) para la premiación que se realizará el 25 de noviembre en el **"Día Internacional de la Eliminación de las Violencias Contra las Mujeres"** además habrá un evento académico y un concierto sorpresa.

Las producciones deben tener una duración mínima de 40 segundos y máxima de 4 minutos y ser dirigidos por mujeres jóvenes de 18 a 30 años, en caso de ser realizados por organizaciones o grupos mixtos, se tendrá  que acreditar la participación mayoritaria de mujeres jóvenes y la dirección de estas en la realización.

**Premios:**  
Primer puesto: USD 3.000  
Segundo puesto: USD 2.000  
Tercer puesto: USD 1.000

El concurso está abierto para Argentina, Bolivia, Brasil, Chile, Colombia, Costa Rica, Ecuador, El Salvador, Guatemala, Honduras, México, Nicaragua, Panamá, Paraguay, Perú, República Dominicana, Uruguay y Venezuela. La iniciativa es apoyada por el Fondo Lunaria (Colombia) • Fondo Alquimia (Chile) • Fondo Centroamericano de Mujeres - FCAM (El Salvador, Honduras, Nicaragua, Costa Rica y Panamá) • Fondo de Mujeres del Sur (Argentina, Uruguay y Paraguay) • Fondo Apthapi Jopueti (Bolivia) • Fondo ELAS (Brasil) • Fondo Semillas (México) • Fondo Frida • Fondo de Acción Urgente (FAU-AL).

Para más información, requisitos y detalles técnicos consulte [www.cortemoslaviolencia.org](http://www.cortemoslaviolencia.org/), o el correo cortemoslaviolencia@gmail.com
