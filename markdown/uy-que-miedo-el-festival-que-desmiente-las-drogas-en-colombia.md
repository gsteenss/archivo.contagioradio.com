Title: ¡Uy que miedo! el festival que desmiente las drogas en Colombia
Date: 2019-10-30 18:06
Author: CtgAdm
Category: Cultura, eventos
Tags: ¡Uy Festival!, Cocaina, Drogas, Marihuana, Miedo, Regulación
Slug: uy-que-miedo-el-festival-que-desmiente-las-drogas-en-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/cannabis-sativa-1418328_960_720.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:Cheifyc] 

Este 30 de octubre desde las 5pm inicia ¡Uy Festival! en el Gimnasio Moderno, una iniciativa que busca poner en la mesa el diálogo sobre todos los estigmas sociales que hay en torno al consumo de sustancias psicoactivas, desmintiendo, aclarando y hablando sin tapujos de las drogas en Colombia. (Le puede interesar:[Desmitificando la Marihuana en Colombia](https://archivo.contagioradio.com/desmitificando-la-marihuana-en-colombia/))

Según Guillermo Solarte, director del ¡Uy Festival! los mitos que se han ido creado en el país en  torno a las drogas surgen desde el momento en el que no se habla claro del tema y no se tiene información comprensible de qué es, qué contiene, qué efectos causa, de donde proviene, "e[l fracaso en el control de las drogas no está en la lucha contra esas 200.000 hectáreas de cultivos de uso ilícito,  sino en la no-política clara construida sobre miedos y estigmas a los consumidores".]

> *["Condenamos  al drogadicto que es  pobre y justificamos al rico, solo porque uno está en situación de calle y el otro lo hace en una fiesta en la 93"]*

Este festival plantea un dialogo entre un país donde circula libremente el capital generado por la venta de sustancias ilegales, y otro que tiene miedo, angustia e indignación sobre temas como la legalización de la marihuana, "la desinformación lo lleva a uno a [pensar que es mucho más fácil asumir que los campesinos que cultivan coca en el Putumayo son los culpables del narcotráfico y no las figuras poderosas en la  políticas que están relacionadas directamente con este", añadió Solarte. ]

### Los mayores mitos al rededor de las drogas

*[Asimismo el Festival busca desmitificar estigmas al rededor del consumo de drogas, por ejemplo la adicción que s]*[egún Solarte, no es del todo cierta, "una cosa es usar una droga de forma recreativa, otra distinta es engancharse a la heroína por circunstancias ajenas", afirmó el director, y añadió que el deterioro que generen sustancias  natural como la marihuana siempre van a depender de la persona y el estilo de vida que lleve. (Le puede interesar:[Gobierno Duque está en absoluta ignorancia sobre la política de drogas](https://archivo.contagioradio.com/gobierno-duque-ignorancia-drogas/))]

Otro de los mitos es *"todas las drogas son malas",* según Solarte esto es falso, y su origen está en el desconocimiento sobre el problema, resaltando que hay millones de sustancias legales, ilegales, naturales y de laboratorio que generan dependencia, "en la mayoría de los casos las dependencias son solo psicológicas, claro hay drogas que son en la mayoría de los casos fatales para el cuerpo, pero un gran número de ella se compone de derivados naturales, como la marihuana, la coca, la amapola y los hongos, que no causan daño real en el organismo", afirmó.

Por último Guillermo Solarte agregó que no hay políticas reales en Colombia que traten sin miedo el tema de las drogas, esto en alusión a las campañas políticas que según él son falsas con este tema,"m[e parece que todos mienten, tanto los que la quieren prohibir hasta los que tienen permisividad, una permisividad que es hipócrita, nunca escuché que alguien hablara sobre la legalización de la marihuana recreativa y en verdad ir al senado y apelar a la regulación". ]

### ¡Uy festival!, el miedo a las drogas

El Festival en su  octava versión, pretende abordar el problema de  las drogas desde todos los ángulos, recogiendo voces de consumidores, madres y familiares de usuarios de sustancias,  logrando diálogos abiertos sobre narcotráfico, legalización, drogas sintéticas, adicciones, y tocando temas como la coca como un producto ancestral en el país y todos los paradigmas  que se dan por su erradicación. (Le puede interesar:[Prohibir el consumo de sustancias en espacios publicos no solucionará nada](https://archivo.contagioradio.com/ley-contra-el-consumo-de-sustancias-en-espacios-publicos-no-solucionara-nada/))

¡Uy Festival!, contará con la participación de economistas, defensores de derechos humanos, periodistas, abogados, científicos e indigenas quienes desde el 31 octubre al 1 de noviembre estarán generando diferentes debates al rededor de la droga; este espacio se hará en el Gimnasio moderno de Bogotá desde las 5:00 pm hasta las 10:00pm durante 3 días, conozca más información de la programación en <http://uyfestivalbogota.blogspot.com/.>

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
