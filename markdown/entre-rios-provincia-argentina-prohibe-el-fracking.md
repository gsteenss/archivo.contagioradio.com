Title: En Argentina comunidad de Entre Ríos logra que se prohíba el fracking
Date: 2017-04-26 15:36
Category: Ambiente, El mundo
Tags: Argentina, extracción petrolera, fracking
Slug: entre-rios-provincia-argentina-prohibe-el-fracking
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/impactos-del-fracking-colombia1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: blog.gesternova.com 

###### [26 Abr 2017] 

**Con una votación unánime, la Cámara de Diputados de la provincia Entre Ríos**, en Argentina, aprobó la prohibición del fracking, como lo venía exigiendo la comunidad hace varios meses. “Poder decir que Entre Ríos es libre de fracking para nosotros es importantísimo”, asegura la Asamblea Ciudadana de Concordia, al medio argentino, AIM.

Los habitantes de la provincia, habían logrado que su iniciativa llegara a la legislatura provincial tras la realización del Foro Regional contra el Fracking, en la que **más de 30 concejos deliberantes acordaron la prohibición del fracturamiento hidráulico** en sus localidades. Finalmente se concretó este martes, tras la discusión en la Cámara, fue aceptada la solicitud de la comunidad, que buscaba proteger las fuentes hídricas y la vocación agrícola de su provincia.

De acuerdo con el portal de noticias AIM, desde esa asamblea de ciudadanos, se logra que no sea permitida la prospección, exploración y explotación de hidrocarburos líquidos y gaseosos por métodos no convencionales como la fractura hidráulica, técnica conocida como fracking. Sin embargo, lamentan que no pasara completa la propuesta, pues además **se pedía la prohibición de cualquier método de extracción  y exploración de crudo.**

 “Es una ley que veníamos pidiendo hace muchos años. Las asambleas, al principio, cuando nos sentimos motivadas por la problemática del fracking era lo primero que pedíamos”, aseguró Facundo Scattone Moulins, integrante de la Asamblea ciudadana.

Tras este primer paso que loga la comunidad de Entre Ríos, afirman que **seguirán luchando para que toda la propuesta sea aprobada, es decir “los cuatro puntos”,** que son la prohibición de la exploración y explotación de hidrocarburos fósiles convencionales y no convencionales, el ingreso a la provincia de todo transporte que tenga por fin la exploración y/o explotación de hidrocarburos; afirmar el carácter de bien público del agua pluvial, superficial y subterránea y la derogación de la Ley de Hidrocarburos 9.991.

“Como dijimos a los legisladores y a la secretaria de Ambiente (Inés Estéves) estamos muy atentos también por lo convencional porque para nosotros **Entre Ríos no es una provincia productora de hidrocarburos y no lo vamos a permitir porque sabemos que tenemos debajo el acuífero Guaraní**”, concluyó Scattone a AIM.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
