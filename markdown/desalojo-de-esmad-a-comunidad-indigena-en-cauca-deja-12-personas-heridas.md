Title: Dos dias de ataques del ESMAD a indígenas en Cauca dejan 12 personas heridas
Date: 2017-07-13 13:54
Category: DDHH, Nacional
Tags: Cauca, ESMAD, Indígenas de Kokonuko, ONIC
Slug: desalojo-de-esmad-a-comunidad-indigena-en-cauca-deja-12-personas-heridas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/07/Kokonuko-1.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: ] 

###### [13 Jul. 2017]

Luego de varias horas de enfrentamientos entre la comunidad indígena Kokonuko en Cauca y más de 300 efectivos del ESMAD que arremetieron contra la población que realizaba un proceso de liberación de la madre tierra en el predio Aguas Tibias \#2, se ha conocido que el resultado son **12 heridos, varios de gravedad, con armas no convencionales y otros más afectados por los gases lacrimógenos.**

Para el **Gobernardor Ismeldo Avirama este hecho constituye una violación grave a los derechos humanos y al territorio** de las comunidades de Kokonuko, sin embargo, manifiesta que seguirán defendiendo sus tierras y no se retirán del sitio hasta que el gobierno cumpla el compromiso de saneamiento del territorio. Le puede interesar: [Pueblo indígena Kokonuko denuncia el asesinato de dos mujeres de su comunidad](https://archivo.contagioradio.com/pueblo-indigena-kokonuko-denuncia-el-asesinato-de-dos-mujeres-de-su-comunidad/)

“Tenemos heridos de la comunidad, son 12 compañeros, algunos de gravedad, otros golpeados porque **ellos arremetieron con toda su fuerza pública.** Unos están en el Hospital de Popayán, otros ya se les ha dado de alta en el hospital del corregimiento de Kokonuko. Hubo violación de los derechos humanos” relata Avirama.

Según el Gobernador, **el ESMAD los sacó de sus casas en donde se encontraban niños, niñas, ancianos** “sin importarles lo que suceda. Porque no solamente están disparando balas de goma y gases sino también balas de guerra”. Le puede interesar: [Continúa proceso de liberación de la madre tierra en Aguas Tibias, Cauca](https://archivo.contagioradio.com/continua-proceso-de-liberacion-de-la-madre-tierra-en-aguas-tibias-cauca/)

Aclaró que ellos en la actualidad no se encuentran en el predio Aguas Tibias II, que es de propiedad privada, sino que están en una parte de la carretera **“nosotros no hemos entrado allá, estamos en una parte que hace parte de nuestro territorio,** porque como esa parte no tienen una escritura pública pues hacemos valer nuestro territorio y nuestra jurisdicción especial”.

### **Niños heridos no fue por accionar de la comunidad** 

Dice Avirama, que **tienen pruebas para demostrar que los 2 niños heridos durante la arremetida del ESMAD no fue el resultado del accionar de la comunidad indígena**, sino que contrario a lo que se ha asegurado, todos los heridos que hay “han sido por causa de la Fuerza Pública. Todo lo tenemos en evidencia y se va a denuncia ante Defensoría del Pueblo”.

### **Comunidad requiere mediación de una comisión especial** 

Por último, el Gobernador manifiesta que **esperan que una comisión especial integrada por el Ministerio del Interior** y otras instituciones competentes que puedan ayudar a que esta situación pueda cesar. Le puede interesar: [Trabajadores de INCAUCA serían responsables del asesinato de comunero indígena](https://archivo.contagioradio.com/incauca-responsables-del-asesinato-de-comunero-indigena-del-cric/)

<iframe id="audio_19788010" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_19788010_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU).
