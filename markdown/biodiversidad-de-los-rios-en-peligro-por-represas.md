Title: Ríos más biodiversos del mundo se encuentran en peligro por hidroeléctricas
Date: 2016-02-08 16:59
Category: Ambiente, Nacional
Tags: amazonas, biodiversidad, Hidroeléctricas
Slug: biodiversidad-de-los-rios-en-peligro-por-represas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/02/Rio-amazonas-e1482197399239.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Mejores 5 

###### [8 Feb 2016.] 

**La tercera parte de las especies de peces de agua dulce del planeta se encuentra en peligro, debido a la construcción de mega proyectos hidroeléctricos** en los ríos más biodiversos del mundo, según concluye la Revista Science.

Se trata del río Mekong, el Congo y el Amazonas, que de acuerdo a 40 científicos  que participaron en el estudio, han sido intervenidos por la implementación de pequeñas represas, además de otra **450 hidroeléctricas que se planea construir en los tres afluentes**.

El artículo señala que “**Las grandes represas invariablemente reducen la diversidad de los peces,** pero también bloquean los movimientos que conectan a las poblaciones y que son vitales para que los peces migratorios completen sus ciclos de vida. Muchos de los peces recorren cientos de kilómetros para superar los pulsos del agua que se dan en las temporadas de sequía y lluvias”.

Los patrones temporales de inundación y sequías de los ríos, afectan los ritmos de crecimiento, maduración y reproducción de peces de los que se alimentan decenas de comunidades. De acuerdo con la publicación de la revista, las represas se ubican en las grandes caídas de agua, donde se concentran una gran cantidad especies únicas en estos ríos.

El río **Amazonas cuenta 2.300 especies de peces, es decir,  al 16% de las especies de todo mundo. Por otro lado, el Congo tiene 1.000 especies, y el Mekong 850.**

Además la publicación indica que se ha comprobado que las medidas que se han tomado por los constructores de las represas, generando pasos para peces, no ha significado una solución y estos animales siguen muriendo tras la construcción de la hidroeléctrica como se ha evidenciado en Colombia, con la represa de El Quimbo una vez entró nuevamente en funcionamiento.

Los científicos de ‘Science’, aseguran que **los estudios de impactos ambientales que realizan las empresas y entidades del Estado no son suficientes ya que no se diseñan de forma integral**, para no evitar este tipo de afectaciones a la fauna y la flora.
