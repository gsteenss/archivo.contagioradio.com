Title: 85 heridos y 12 detenidos deja represión de Fuerza Pública a Minga Nacional
Date: 2016-06-01 14:08
Category: Paro Nacional
Tags: Equipo Jurídico Pueblos, Minga Nacional, Paro Nacional Colombia, Represión Paro Colombia
Slug: se-agrava-la-situacion-de-derechos-humanos-en-la-minga-nacional
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/06/Buenaventura.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Periferia Prensa ] 

###### [1 Junio 2016 ]

En tres días de Minga Nacional el balance en materia de derechos humanos, es negativo. Según el abogado Leonardo Jaimes, integrante del Equipo Jurídico Pueblos, **en todos los puntos de concentración hay presencia del Ejército, ESMAD y Policía**, con alta capacidad de confrontación, legitimada por el presidente colombiano Juan Manuel Santos, que ha ordenado accionar la fuerza en tanto las comunidades asuman vías de hecho.

En la mayoría de las regiones la movilización se ha hecho sentir ante los incumplimientos del Gobierno, asegura Jaimes e insiste en que la respuesta ha sido represiva, pues **hay militares de civil y uniformados, que han fotografiado, grabado y amenazado a los líderes** de la movilización, han bloqueado redes de comunicación, han intentado instalar estaciones aledañas a los puntos de concentración, como ocurrió en La Lizama, Norte de Santander y han aseverado que la Minga es motivada por la guerrilla, poniendo en peligro la integridad de las comunidades.

Preocupa la provocación de los miembros del ESMAD y las declaraciones del presidente Santos, asevera el abogado, pues pese a que está próxima la etapa de posconflicto, **los problemas estructurales no se han superado** y es justamente esta reclamación la que se expresa en la actual Minga Nacional, por lo que las organizaciones continuarán en paro, denunciando las violaciones a los derechos humanos y solicitando presencia internacional, para que la situación no se agrave.

<iframe src="http://co.ivoox.com/es/player_ej_11746034_2_1.html?data=kpaklpuUd5Whhpywj5eWaZS1kZ2ah5yncZOhhpywj5WRaZi3jpWah5ynca3Z0NPO1MnTb6vVytLS1ZCRb6bl1s7d0ZCuudOZpJiSo6nIrcTjjLXix8fQs9Shhpywj6jTstXVyM7cjbfFqMrjjJKSmaiReA%3D%3D&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)] ] 

 
