Title: En Mayo los martes son de cine con BOGOSHORTS sessions
Date: 2019-05-11 22:04
Author: CtgAdm
Category: 24 Cuadros
Tags: Bogotá, Cine Colombiano, Cine Tonalá
Slug: mayo-martes-cine-bogoshorts-sessions
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/BOGOSHORTSDEF.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Cine Tonalá 

Con la presentación de ["Eva menos candela" de la directora Ruth Caudeli](https://archivo.contagioradio.com/eva-menos-candela-bogoshorts/), el martes 7 de mayo inició un nuevo ciclo de **BOGOSHORTS sessions**, un espacio abierto al público capitalino en el que cada martes se estrenará un cortometraje nacional.

La segunda sesión tendrá lugar el martes **14 de Mayo con "La noche resplandece"** una producción estrenada en 2018 bajo la dirección del paisa **Mauricio Maldonado**, que le hizo merecedor del premio a mejor corto en el 16º VLAFF en Canadá.

##### Sinopsis: Un sábado caluroso en Medellín, el eco de las fiestas retumba. Es la última noche de Mafe antes de dejar el barrio donde ha crecido en la periferia. A medianoche se encuentra en secreto con Damián, quien acaba de ser transferido a un lugar discreto donde purga arresto domiciliario. El amor adolescente y la violencia resplandecen durante el encuentro clandestino de estos amantes imposibles. 

El martes 21 de mayo durante la tercera sesión se presentará **"Amalgama"** dirigido por **Alejandra Wills,** y que hace parte del Concurso Iberoamericano de Cortometrajes en el Festival Internacional de Cine de Huesca, 2019 y fue ganador del FDC -Realización de cortometraje, 2016.

##### Sinopsis: Alcira y Henry son dos campesinos a quienes los une un gran dolor. Pasan tiempo juntos, aunque les signifique vivir momentos agobiantes: es la única forma que encuentran de calmar su tristeza. Alcira necesita perdonarlo. Henry necesita su perdón. 

En la última sesión del mes el martes 28, se proyectará **"Zapatillas"** de la boyacénse **Mónica Juanita Hernández,** con el que se estrena en el campo del cortometraje de ficción, y prepara susegundo y tercer cortometraje, Gladiolos Blancos y A la carga Lunga.

##### Sinopsis: El día de su cumpleaños, Harold recibe unas zapatillas para montar bicicleta a pesar de tenerla dañada. El dinero de su trabajo en el campo, lo ahorra para ir a la excursión del colegio en donde estará la niña que le gusta. Presionado por el regalo, Harold le pregunta a su padre sobre su decisión de dejar el ciclismo al comprometerse con su mamá. Harold entonces se convence de arreglar su bicicleta, sin saber que la niña que le gusta, prefiere también montar bicicleta junto a él. 

Las presentaciones de BOGOSHORTS sessions inician a las 8 de la noche en Cinema Tonalá, Cra. 6 \#35-37, La Merced Bogotá

###### Encuentre más información sobre Cine en [24 Cuadros ](https://archivo.contagioradio.com/programas/24-cuadros/)y los sábados a partir de las 11 a.m. en Contagio Radio 
