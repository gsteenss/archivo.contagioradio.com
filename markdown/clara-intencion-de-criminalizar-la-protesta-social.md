Title: Capturas y judicializaciones pretenden criminalizar la protesta social
Date: 2020-01-24 17:15
Author: CtgAdm
Category: Movilización, Nacional
Tags: jovenes, Movimiento social, Paro Nacional, protestas pacíficas
Slug: clara-intencion-de-criminalizar-la-protesta-social
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/paro-nacional-1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: ContagioRadio

<!-- /wp:paragraph -->

<!-- wp:html -->

<figure class="wp-block-audio">
<audio controls src="https://co.ivoox.com/es/isabel-fajardo-informe-allanamientos-21e_md_46797875_wp_1.mp3">
</audio>
  

<figcaption>
Entrevista &gt; Isabel Fajardo | Red Distrital de derechos humanos

</figcaption>
</figure>
<!-- /wp:html -->

<!-- wp:paragraph -->

Integrantes de la Campaña Defender la Libertad: Asunto de Todas, y de las Comisiones de Verificación e Intervención de la sociedad civil, realizaron un llamado al Gobierno Nacional a garantizar el diálogo social y con ello el derecho a la protesta social. ( Le puede interesar: <https://www.justiciaypazcolombia.com/urge-mesa-distrital-de-seguimiento-al-ejercicio-del-derecho-a-la-protesta-comisiones-de-verificacion/> )

<!-- /wp:paragraph -->

<!-- wp:quote -->

> *"Denunciamos que en la jornada \#21E se presentaron abusos de autoridad que contraviene, no sólo el Decreto 563, sino las mismas disposiciones de la nueva administración distrital".*
>
> <cite> Campaña Defender la Libertad </cite>

<!-- /wp:quote -->

<!-- wp:paragraph -->

La acción que solicitan los defensores para garantizar la protección a la protesta ciudadana está en l**a instalación de la primera Mesa Distrital de Seguimiento al Ejercicio de los Derechos a la Libertad de Reunión**. (Le puede interesar: <https://archivo.contagioradio.com/dos-allanamientos-y-tres-capturas-en-el-marco-del-paro-nacional/>)

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Un espacio que se da en conformidad con el decreto 563 de 2015, que corresponde al p**rotocolo de actuación para las movilizaciones sociales en Bogotá,** y ofrece espacios como la Mesa Distrital que no solo garantice derecho a la protesta, también a la vida, la integridad física y la seguridad.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Según Isabel Fajardo Vocera de Red Distrital de Derechos Humanos, "Nosotros instamos a que esa mesa pueda hacer **seguimiento no solo a la movilización social sino a ejercicio pleno de los derechos humanos**", y agregó que en este espacio participaría el Distrito, la sociedad civil, las organizaciones sociales, defensores de derechos humanos y el Ministerio Público.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Los casos que implican al ESMAD quedan impunes

<!-- /wp:heading -->

<!-- wp:paragraph -->

Reinaldo Villalba abogado y vicepresidente de la Federación Internacional de Derechos Humanosa resaltó la falta de seguimiento que existe desde el Distrito ante los casos de agresiones hacia los manifestantes por parte del Escuadrón Móvil Antidisturbios *" La impunidad es un tema que se debe resaltar, porque **los casos que implican al ESMAD no son condenados , pareciera que lo que hacen simplemente la justicia nacional lo pasará por alto** "* .

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Conductas denigrantes y agresivas hacia la protesta social

<!-- /wp:heading -->

<!-- wp:paragraph -->

La primera jornada de movilización de este 2020, según los defensores de Derechos Humanos que hicieron acompañamiento y seguimiento tuvo hechos de represión que estuvieron "por fuera de los protocolos acordados por el Distrito e incluso por la ley donde el uso de la fuerza era el último recurso.

<!-- /wp:paragraph -->

<!-- wp:quote -->

> *"El uso de la fuerza debe ser considerado siempre el último recurso y que la intervención del ESMAD corresponde únicamente a circunstancias excepcionales"*.
>
> <cite> Reynaldo Villalba </cite>

<!-- /wp:quote -->

<!-- wp:paragraph -->

De igual forma revelaron que en la jornada de este 21 de enero se presentaron **más de 212 detenciones, de estas 114 personas fueron conducidas al Centro de Traslado por Protección (CTP)** y 3 con fines de judicialización *" varios conducidos recibieron tratos denigrantes, heridas, y golpes en sus cabezas y rostros durante la detención.* "

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Uno de los casos es el de **Miguel Vargas,** a quien según el abogado defensor **Oscar Ramírez integrante del Comité de Solidaridad con los Presos Políticos**, se le acusa de violencia contra servidor publico, daño al bien ajeno y obstrucción a vías por medios ilícitos. (Le puede interesar: [https://archivo.contagioradio.com/estudiantes-de-la-u-del-atlantico-continuan-exigiendo-autonomia-universitaria)](https://archivo.contagioradio.com/estudiantes-de-la-u-del-atlantico-continuan-exigiendo-autonomia-universitaria/)

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por lo cual se le es decretada medida de aseguramiento domiciliaria, *"inicialmente el fiscal le dio como medida de detención preventiva en establecimiento carcelario sin embargo el juez hizo una interpretación menos gravosa y le designo detención en su casa"*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Otro de los judicializados según Ramírez fue procesado por los cargos de fabricación, tráfico, porte y tenencia de armas de fuego, *"este caso contó con presencia del Ministerio Público y se determinó que no habían motivos para declarar esta captura de manera legal ni mucho menos su traslado a una cárcel"*.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El abogado reconoció que en el caso de Vargas y de los jóvenes privados de la libertad en el marco del Paro, a pesar de no ser trasladados a una prisión, representan un vulneración de derechos fundamentales como la educación, la expresión y la libertar de locomoción, *"Miguel no tiene antecedentes penales, no representa un peligro para la sociedad, lo que jugó aquí y en la mayoría de los casos fue la presión mediática para dar un castigo ejemplar a quienes se suman a la protesta social".*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por último el abogado agregó que en estas detenciones prima el *populismo punitivo*, que promueve que las medidas de represión sean ejemplarizantes sin importar si las personas son responsables o no de los cargos que la fiscalía quiera imputarles, para así provocar la aprobación electoral y responder a la presión mediática.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Ataques a defensores de Derechos Humanos

<!-- /wp:heading -->

<!-- wp:paragraph -->

Según los resultados de la Red Popular de Derechos Humanos Bogotá (REDHUS), en la reciente jornada de Paro, una defensora de derechos humanos fue detenida, y dos más defensores fueron agredidos físicamente por la fuerza pública. (Le puede interesar:<https://archivo.contagioradio.com/retorica-de-duque-no-soluciona-la-crisis-humanitaria-habitantes-de-taraza/>)

<!-- /wp:paragraph -->

<!-- wp:quote -->

> *"Estas agresiones a defensores y defensoras de derechos humanos e integrantes del Ministerio Público, han sido una constante desde el inicio del Paro Nacional"*
>
> <cite>Isabel Fajardo -Redhus</cite>

<!-- /wp:quote -->

<!-- wp:paragraph -->

Ante las acciones que pudieron ser evidenciadas en las jornadas de paro por parte del Estado, el abogado Villalba mencionó que *"hay una clara intención de criminalizar la protesta social"*, hecho que según el es visible en acciones como la detención por obstruir vías públicas y señaló *"yo no me puedo imaginar un paro campesino junto a los palos de café, obvio tienen que salir a las calles y no a los andenes, sino a las vías principales"*.

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->
