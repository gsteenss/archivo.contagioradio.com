Title: Las razones para oponerse al sendero Las Mariposas en los cerros orientales
Date: 2019-08-28 17:50
Author: CtgAdm
Category: Ambiente, Entrevistas
Tags: Bogotá, cerros orientales, Peñalosa, Sendero Las Mariposas
Slug: oponerse-sendero-las-mariposas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/08/9560011750_b1fe9f275f_k-e1567032292425.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Yesid Reyes  
] 

El sendero Las Mariposas vuelve a ser noticia por 52 requerimientos que hizo la Agencia Nacional de Licencias Ambientales (ANLA) a la Alcaldía de Bogotá. Esta ha sido uno de los proyectos que ha impulsado el alcalde Enrique Peñalosa, para promover el turismo en los cerros orientales y generar un corredor que permita evitar los incendios forestales; sin embargo, las comunidades que están en el área de influencia del proyecto han manifestado las razones para estar en contra, y han planteado preguntas que el Distrito debería también resolver.

### **¿Por qué algunos habitantes se oponen a la construcción del sendero Las Mariposas?** 

**Hugo Mendoza, integrante de la Mesa de los Cerros Orientales y habitante de una de las localidades afectadas por el Proyecto,** manifestó que la Administración Distrital desconoce el fallo del Consejo de Estado de 2013 sobre la protección de los cerros. Mendoza señaló que este fallo obliga a que se tenga en cuenta a las comunidades que están en la zona para hacer cualquier intervención en los cerros, castiga cualquier intervención negativa en los mismos y prohíbe que se afecten las fuentes de agua que allí se ubican.

Para el integrante de la Mesa, el sendero de Las Mariposas infringe estas disposiciones pues aumenta la cantidad de personas que circulan por los cerros, **pasando de cerca de 300 personas al año a cerca de 2 mil.** Adicionalmente, dijo que el sendero tendría que intervenir fuentes de agua, y generaría una intervención negativa en el ecosistema, razones que llevaron a ambientalistas y habitantes de las localidades en las que se construirá el sendero a idear una estrategia jurídica para oponerse a la obra.

### **Los terceros intervinientes, personas afectadas por el sendero** 

Con el fin de garantizar la participación de las comunidades en la decisión sobre la obra, diferentes organizaciones buscaron convertirse en terceros intervinientes, es decir, en sujetos de derechos que deben ser incluídos en el proceso de construcción. Para desarrollar este proceso, se recogieron más de 2 mil firmas de personas que tendrían que ser reconocidos como terceros intervinientes; algunos de ellos ya fueron aceptados en este proceso, y recibieron información más precisa sobre este Proyecto.

Mendoza afirmó que el Consorcio encargado de desarrollar el proyecto les entregó un documento con 3 elementos importantes: habrá maquinaria pesada en la obra, se intervendrán 86 caminos ancestrales, y se construirán restaurantes, hoteles miradores a lo largo de la carretera. Adicionalmente, el Proyecto contempla la ampliación de caminos ancestrales que aún existen en el territorio, cuyo ancho no supera los 2 metros, y **se los usará para el 'sendero' que medirá entre 10 y 12 metros.**

### **Las dudas que aún no resuelve la Administración Distrital** 

Uno de los requerimientos hechos por la ANLA, y ante la que el Distrito manifestó su inconformidad por no estar incluida entre los estudios que se deben entregar sobre el proyecto tiene que ver con los estudios de cargas que tendrán los cerros una vez se construya el sendero. Para ambientalistas y habitantes de las zonas afectadas por el Proyecto este es un elemento importante, pues ya se han tenido experiencias de sobrecarga en espacios como el sendero de la quebrada La Vieja, que ha generado deterioro en el ecosistema y fallas en los terrenos cercanos por el exceso de peso.

En ese sentido, piden que aunque este tema no aparezca mencionado entre los requisitos para abrir la licitación del Proyecto, se incluya de tal forma que se estimen los riesgos de forma adecuada. Mendoza también cuestionó el funcionamiento del corredor cortafuegos, porque el Alcalde ha dicho que el sendero servirá para combatir los incendios forestales, pero revisando la experiencia de otros países con corredores cortafuegos, el habitante de Santa Fé indicó que los mismo funcionan con bomberos que están dispuestos en los cerros para evitar que las conflagraciones se expandan, no mediante la construcción de caminos.

Por último, el integrante de la Mesa de los Cerros Orientales **criticó el beneficio que obtendrán las comunidades de esta intervención,** puesto que para ellos, no está clara la forma en que los beneficiara, o si será un proyecto ecoturístico diseñado para satisfacer los intereses de empresas externas al territorio. (Le puede interesar: [" Buscar ¿Cuáles serían las afectaciones del S. Las Mariposas?"](https://archivo.contagioradio.com/cuales-serian-las-afectaciones-del-sendero-las-mariposas/))

### **¿Cuál es la propuesta de las comunidades?** 

El integrante de los Cerros afirmó que una intervención de gran escala como la que se plantea siempre será negativa, en cambio, recordó que **los 86 senderos antiguos que persisten se podrían adecuar para que las comunidades tengan proyectos de ecoturismo sostenibles económicamente, y sustentables** en relación a su impacto en el ambiente. (Le puede interesar:["POT de Peñalosa satisface intereses económicos y no las necesidades de Bogotá"](https://archivo.contagioradio.com/pot-satisface-intereses-economicos/))

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
