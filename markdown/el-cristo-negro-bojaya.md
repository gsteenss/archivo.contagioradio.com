Title: El Cristo Negro una muestra de perdón, esperanza y paz en Bojayá
Date: 2016-10-25 07:43
Category: Nacional, Paz
Tags: Bojaya, Cristo Negro, Masacre en Bojayá
Slug: el-cristo-negro-bojaya
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/10/Bojaya1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Fotos: Contagio Radio] 

###### [11 de Oct 2016] 

El pasado 29 de septiembre, en La Loma, Bojayá, se llevó a cabo la entrega del Cristo Negro por parte de las FARC-EP a la comunidad que vivió la tragedia de la **masacre de Bojayá**, un hecho en donde 117 personas perdieron la vida, después que un cilindro cayera en medio de la iglesia en donde se refugiaban los pobladores que escapaban del combate entre paramilitares y la guerrilla.

El dos de octubre, día del plebiscito por la paz, la población le apostó al sí por refrendar los acuerdos de la Habana y expresaron que su territorio podrá ser el escenario para la construcción de paz, **la que anhelan desde que la guerra se tomo sus vidas. **Le puede interesar: ["En los lugares más afectados por la guerra ganó el Sí"](https://archivo.contagioradio.com/en-los-lugares-mas-afectados-por-la-guerra-gano-el-si/)

Luego de 14 años, las FARC-EP pidió perdón a las víctimas de este hecho y en la iglesia de la Loma de Bojayá, entregó al Cristo Negro, una escultura de 2.85 m esculpida por el maestro Enrique Angulo, **que significó no solo el perdón sino la esperanza de construir un nuevo futuro.**

El Cristo fue un encargo especial, hecho por las FARC-EP al maestro Angulo con requerimientos específicos, **debe tener rasgos afros como reconocimiento a las luchas emprendidas por esta comunidad a lo largo del tiempo y como parte de la memoria que debe persistir.** Esta es una recopilación fotográfica de la entrega del Cristo Negro a la comunidad de Bojayá.

\
