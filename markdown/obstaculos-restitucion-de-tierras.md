Title: Los seis obstáculos a la restitución de tierras en Urabá
Date: 2019-07-30 21:28
Author: CtgAdm
Category: Comunidad, Nacional
Tags: Ley 1448, Restitución de tierras, uraba
Slug: obstaculos-restitucion-de-tierras
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/Restitución-de-Tierras.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio  
] 

Recientemente se lanzó "El proceso de restitución de tierras en el Urabá Antioqueño", libro en el que se analiza el desarrollo que se ha dado a la Ley 1448 de 2011, o ley de víctimas, para quienes han sido despojados de la tierra. En el texto se hace reflexión sobre los retos jurídicos de la restitución, como avanza la jurisprudencia para resolver este tema, y sobre las tensiones en el Urabá que ha generado. (Le puede interesar: ["Campesinos de Urabá demandan acompañamiento policial en proceso de restitución"](https://archivo.contagioradio.com/campesinos-de-uraba-demandan-acompanamiento-policial-en-proceso-de-restitucion/))

La publicación del libro fue posible gracias **al Instituto Popular de Capacitación y la Universidad Autónoma Latinoamericana**. **Carlos Montoya, uno de los investigadores** señaló que el texto fue posible gracias al trabajo de seguimiento a la Ley 1448, un análisis de terreno y seguimiento a las audiencias de restitución de tierras, lo les permitió obtener seis grandes conclusiones. (Le puede interesar: ["Incumplimientos en la restitución irán a la CIDH"](https://archivo.contagioradio.com/incumplimientos-restitucion-de-tierras-cidh/))

### En las agendas del Comité de Justicia Transicional ni siquiera estaba la restitución de tierras

El primero de los hechos que resaltó fue la revisión de las actas del Comité de Justicia Transicional, "que es la entidad que da seguimiento al desarrollo de la Ley", vieron que las entidades responsables de estar presentes en las audiencias no asistían; "en el caso de Turbo, en las agendas del Comité ni siquiera estaba hablar el tema de reclamantes de tierras", indicó. En ese sentido, encontraron un fallo en la operación de esta instancia, que no cumplía la labor para la que había sido creada.

Sumado a ello, gracias a revisiones documentales, se encontró un texto en el que la Defensoría del Pueblo en Urabá narraba cómo coordinar junto a la Alcaldía de Turbo el desalojo de campesinos; lo que allí se relataba era la destinación de recursos por más de 60 millones para la operación, en la que se usaría un helicóptero que trasladaría al Escuadrón Móvil Antidisturbios (ESMAD), mientras la Defensoría garantizaba que esto se pudiera realizar. (Le puede interesar:["Despues de 20 años de lucha, campesinos regresan a sus tierras en Guacamayas, Urabá"](https://archivo.contagioradio.com/despues-de-20-anos-de-lucha-campesinos-regresan-a-sus-tierras-en-guacamayas/))

En tercer lugar, Montoya afirmó que había diferentes estrategias para mantener y legitimar el despojo de tierras: las amenazas, gracias a las "**alianzas entre ganaderos y grandes empresarios de las zonas con grupos armados**"; y también la persecución judicial, que llevaba ante los juzgados a reclamantes de tierras, pero "curiosamente la Fiscalía tomaba las denuncias contra todos los reclamantes" y no contra los despojadores.

Otra de las estrategias denunciada por el Investigador fue la creación de asociaciones organizativas para proteger a presuntos despojadores, "por ejemplo, la Fundación Tierra, Paz y Futuro que se puede confundir con otras que trabajan por la restitución; pero es liderada por empresarios y ganaderos que están contra la restitución y tiene el respaldo de personas como María Fernanda Cabal". (Le puede interesar: ["Cinco propuestas para evitar el fracaso total de Ley de restitución"](https://archivo.contagioradio.com/42667-2/))

Una quinta conclusión que extrajo fue el **incumplimiento de las sentencias de restitución de tierras;** Montoya recordó el caso de una audiencia de restitución en la que la persona beneficiada narraba que el ganadero que le había devuelto la tierra tumbaba su cerco y metía búfalos para dañar sus cultivos. A ello se sumó la agresión contra una de sus familiares, a la que le inyectaron una sustancia desconocida en el cuello para intimidarlos.

Gracias a un análisis cartográfico, quienes realizaron el libro pudieron dar cuenta de una relación entre las veredas donde se ha dado la restitución y la creación de vías terrestres. Como explicó Montoya, **"despojaron donde iba a haber vías de comunicación"**. (Le puede interesar: ["Lucha entre cifras y hechos, los resultados de la restitución"](https://archivo.contagioradio.com/asi-trabaja-unidad-restitucion-tierras/))

### **Modelos de despojo que siguen funcionando en el territorio**

Montoya recordó el caso del Bajo Atrato, que consideró clave para mostrar lo que pasó en Urabá, en que se tejían alianzas entre "ilegales, funcionarios públicos y sectores bananeros y palmeros que se mantiene" para despojar tierras y usarlas en economías extensivas. Según concluyó, **este modelo aún sigue funcionando en el territorio,** pero ahora, con los empresarios y ganaderos intentando mostrarse como ambientales.

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe><iframe id="audio_39186578" frameborder="0" allowfullscreen scrolling="no" height="200" style="border:1px solid #EEE; box-sizing:border-box; width:100%;" src="https://co.ivoox.com/es/player_ej_39186578_4_1.html?c1=ff6600"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
