Title: La defensa de la vida, un punto central del Paro Nacional
Date: 2019-12-04 20:30
Author: CtgAdm
Category: Paro Nacional, Paz
Tags: Comité Nacional, Marchas, paquetazo, Paro Nacional
Slug: la-defensa-de-la-vida-un-punto-central-del-paro-nacional
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/WhatsApp-Image-2019-12-04-at-18.21.01.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

Sectores que integran el Comité Nacional del Paro tienen como uno de los puntos centrales en la agenda de negociación con el Gobierno la garantía para los derechos humanos y el cumplimiento del Acuerdo de Paz. Aunque el Gobierno se ha esforzado en cuestionar las razones para la convocatoria del paro, y cada sector tiene temas en sus agendas propias que reivindican, los une el clamor por el respeto de la vida, como pilar para el funcionamiento de la sociedad.

### **Campesinos: Revisar los TLC y la violación a los derechos humanos**

Óscar Gutiérrez, director ejecutivo nacional de Dignidad Agropecuaria, señaló que los campesinos necesitan "que se revisen seriamente los tratados de libre comercio, por el daño que están haciendo a la producción agropecuaria del país", y afirmó que necesitan tener precios justos y estables, para poder seguir produciendo. En ese sentido, Gutiérrez aseguró que la política de importaciones está afectando esa posibilidad, haciendo que los campesinos compitan con mercados que tienen más tecnología y apoyo económico.

El Directivo señaló que además de estos temas, los campesinos reclaman que cesen las violaciones a los derechos humanos, y se impulse el Acuerdo de Paz, "porque no queremos más muerte, no queremos que siga la violencia que ha tocado al país". (Le puede interesar: ["Asesinato del líder Walter Rodríguez refleja la persecución que viven campesinos de Antioquia"](https://archivo.contagioradio.com/asesinato-del-lider-walter-rodriguez-refleja-la-persecucion-que-viven-campesinos-de-antioquia/))

### **Estudiantes: Del cumplimiento de los Acuerdos a la lucha por la vida, y el desmonte del ESMAD**

Algunas de las primeras grandes movilizaciones que se vivieron durante la segunda mitad de este año estuvieron relacionadas con estudiantes universitarios que reclaman sacar la corrupción de las instituciones públicas. Las marchas iniciaron desde septiembre, y en cuanto fueron reprimidas por el Escuadrón Móvil Antidisturbios (ESMAD), los jóvenes incluyeron en su agenda la lucha por el desmonte de esta fuerza.

En el marco de la convocatoria del Paro Nacional, los estudiantes afirmaron su apoyo al mismo, considerando el Artículo 44 del Presupuesto General de la Nación (PND), que ponía en riesgo el presupuesto de las Universidades estatales, y denunciando las reformas laboral, pensional y tributaria, que los afectaba específicamente en términos de acceso a condiciones de vida digna. (Le puede interesar: ["¡Que nadie se quede en casa! 21 de noviembre, paro nacional"](https://archivo.contagioradio.com/que-nadie-se-quede-en-casa-21-de-noviembre-paro-nacional/))

Posteriormente, mientras se desarrollaba la movilización ocurrió el asesinato de Dilan Cruz, lo que ha llevado a que los jóvenes centran los objetivos de paro en hablar sobre el uso que hacen las Fuerzas Militares y de Policía en el país, en el desarrollo de sus operaciones, y respecto de la protesta social. Los estudiantes han exigido el desmonte del ESMAD, como una forma de garantizar que los ciudadanos tengan la efectiva garantía para ejercer su derecho a la protesta.

### **Indígenas: Para que se pueda vivir en paz en los territorios**

Giovanni Yule, dinamizador político del Consejo Regional Indígena del Cauca (CRIC), sostuvo que el paro puede leerse como el eco que hacen otros sectores de la población a los empeños históricos del movimiento indígena; en consecuencia, dijo que estaban organizandose para poder sostener esta movilización en el tiempo, y poder mantener la presión sobre el Gobierno para que abra la puerta a la negociación. (Le puede interesar:["Autoridades indígenas responsabilizan a Duque de masacre en Tacueyó"](https://archivo.contagioradio.com/autoridades-indigenas-responsabilizan-a-duque-de-masacre-en-tacueyo/))

En la agenda de los pueblos indígenas, Yule manifestó que se encuentra todo lo que "tiene que ver con derechos culturales, económicos y sociales que nos permitan vivir en paz", por eso, explicó que haya llegado una delegación indígena a Bogotá el pasado jueves 28 de noviembre, y otra más esté en camino, "para que la defensa de la vida sea un tema fundamental, como parte de la reconciliación y para salvaguardar, cuidar y proteger la madre tierra".

### **Ambientalistas: El cuidado de las otras formas de vida  
**

El sector ambientalista llegó a reforzar el paro nacional poco antes que el Presidente abriera la puerta a un diálogo nacional con los sectores populares, en la reunión, el movimiento puso en discusión tres temas centrales: La garantía de participación de las comunidades sobre lo que pasa en sus territorios, el daño a los territorios causado por el Fracking, el Glifosato y la minería, y la emergencia climática. (Le puede interesar: ["Los tres puntos de organizaciones y procesos ambientales en el paro nacional"](https://archivo.contagioradio.com/los-tres-puntos-de-organizaciones-y-procesos-ambientales-en-el-paro-nacional/))

En cuanto al primer tema, los ambientalistas pidieron que se garantice la consulta popular libre e informada para que las comunidades tengan incidencia en las decisiones sobre lo que pasa en su territorio; respecto al segundo tema, pidieron compromiso al Gobierno para no realizar aspersión con Glifosato ni practicar fracking. De igual forma, exigieron acciones contundentes para frenar la emergencia climática, al pedir acciones contra la deforestación y que protejan los ecosistemas.

### **Congresistas: ¿De espaldas a la ciudadanía?  
**

Mientras la ciudadanía completó 12 días de movilización contra lo que sectores han denominado el 'paquetazo' de Duque, el Congreso aprobó en primer debate dos de esas reformas que lo componían: La laboral y la tributaria. Según el senador Gustavo Bolívar, en el Proyecto de Ley de 'Crecimiento Económico' quedó una proposición que busca incentivar la contratación de jóvenes, entregando exenciones en impuestos a la renta, pero que puede generar despidos masivos de trabajadores de más de 30 años.

Adicionalmente, también se aprobó en primer debate el Proyecto de Ley Andrés Felipe Arias, que buscaría crear la doble instancia retroactiva para aforados, razones por  las que Bolívar aseguró que "ya no representamos los intereses de los ciudadanos, porque los congresistas estamos del lado de los poderosos", y concluyó que "es una aberración, y el Congreso de espaldas a la gente". (Le puede interesar: ["Nueva reforma tributaria significará ‘crecimiento económico’ para los de siempre"](https://archivo.contagioradio.com/nueva-reforma-tributaria-significara-crecimiento-economico-para-los-mismos-de-siempre/))

Sin embargo, el Senador declaró que la resistencia civil dará los resultados que no se ven en el Congreso: "La resistencia civil pacífica sí da resultados, si mantenemos la protesta el comercio presionará... si no hacemos esto con consistencia, de verdad que no vamos a poder". (Le puede interesar: ["Las voces del Congreso que reivindican las exigencias del Paro Nacional"](https://archivo.contagioradio.com/las-voces-del-congreso-que-reivindican-las-exigencias-del-paro-nacional/))

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe><iframe id="audio_45282331" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_45282331_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
