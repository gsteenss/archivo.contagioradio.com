Title: En 2015 fueron asesinados 39 periodistas según la CIDH
Date: 2016-03-24 11:00
Category: DDHH, Nacional
Tags: CIDH, colombia, Libertad de expresión, Paramilitarismo, periodistas asesinados
Slug: en-2015-fueron-asesinados-39-periodistas-segun-la-cidh
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/03/cidh-libertadexpresion-contagioradio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: caracteres.mx] 

###### [24 Marz 2016] 

El  Informe Anual de libertad de expresión evidencia el aumento de violencia y agresiones contra periodistas y trabajadores de medios de comunicación durante 2015. Según la CIDH sobre libertad de expresión, **en 2015, 27 periodistas fueron asesinados por razones que tienen que ver con el ejercicio de su profesión, y en otros 12 casos  no hay evidencia concreta de ser víctimas por su labor.**

En cuanto a los países en los que ocurrieron dichos asesinatos está **Brasil, Honduras, México, [Colombia](https://archivo.contagioradio.com/libertad-de-prensa-en-colombia-2015/)**, Guatemala, República Dominicana, Estados Unidos y Paraguay. De acuerdo a las cifras, en 2015 [aumentó la violencia contra los periodistas](https://archivo.contagioradio.com/libertad-de-prensa-en-colombia-2015/) con respecto al 2014, año en que se registraron 24 asesinatos.

El informe también expresa su preocupación por la respuesta desmedida de varios Estados al derecho a la manifestación y a la protesta, lo que se hizo presente a través de la **disolución violenta de protestas por parte de cuerpos de seguridad del Estado**, detenciones de activistas y periodistas, agresiones y roturas de equipos de los comunicadores.

Sin embargo, los asesinatos o las agresiones físicas directas no son las únicas amenazas a las que se enfrenta el ejercicio del periodismo, en el informe se señala el control monopólico de los medios de información y la asignación de **pauta publicitaria oficial de manera discriminatoria** de acuerdo a la línea editorial del medio.

Además, se resalta que el uso de la fuerza para dispersar las manifestaciones y protestas públicas también afecta a los y las periodistas puesto que muchos de ellos son víctimas de las agresiones o sus equipos son afectados por la acción directa o indirecta de la fuerza policial o militar, además de las acciones contra la libertad de expresión

De acuerdo a la **[CIDH es necesario y urgente que los Estados adopten mecanismos de prevención adecuados](https://archivo.contagioradio.com/?s=cidh)** para evitar la violencia contra las y los comunicadores, incluyendo la condena pública a todo acto de agresión; la adopción de medidas eficaces de protección para garantizar la seguridad de quienes se encuentran sometidos a un riesgo especial por el ejercicio de su derecho a la libertad de expresión.

Para leer el resumen ejecutivo, haga clic [aquí](http://cidh.us6.list-manage2.com/track/click?u=af0b024f4f6c25b6530ff4c66&id=59c4c720bd&e=8ddefe0e33).
