Title: Comunidades del Norte de Antioquia piden reanudar diálogos Gobierno-ELN
Date: 2019-02-19 15:32
Author: AdminContagio
Category: Comunidad, Nacional
Tags: ELN, Mineria, Norte de Antioquia
Slug: comunidades-del-norte-de-antioquia-piden-reanudar-dialogos-gobierno-eln
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/02/11700799_114353415575398_2162276445867247243_n-770x400.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: 

###### 19 Feb 2019 

[A raíz del asesinato del **líder minero, Alberto Jiménez** y las constantes hostilidades en el territorio, las comunidades y organizaciones campesinas del norte de Antioquia han pedido al Gobierno que reanude la mesa de diálogos con el ELN, grupo armado que habría sido el responsable de la muerte del líder.   
  
El vocero de **CAHUCOPANA, Víctor Vega** indica que según información suministrada por la comunidad, Alberto Jiménez quien participaba en los procesos de constitución de comités mineros de la vereda **Panamá 9**, ya había recibido algunas amenazas por parte del ELN, razón por la que la población tiene la certeza de que tal grupo estaría tras la muerte del minero quien venía liderando dicha actividad en el sector desde hace cinco años   
  
]**La compleja situación de los mineros del nordeste antioqueño**

[Según relata el vocero, a lo largo de la última década, las comunidades han intentado formalizar la minería como actividad sin embargo sus intentos han sido infructuosos pues **“la legislación no está hecha para los pequeños ni los medianos mineros, únicamente para las grandes multinacionales”** quienes sí pueden acceder a la explotación de recursos.  
  
También señala que los requisitos que demanda el Gobierno para la formalización de la minería en la región son muy costosos y que el Estado no ofrece una asesoría para los pequeños mineros que buscan legitimar su actividad económica, una situación que el líder Jaime Jiménez conocía bien y que supervisaba a la par con la junta comunal.  
  
]**Presencia de actores armados**[  
  
Víctor Vega también resalta la fuerte presencia de diferentes actores armados que pretenden el territorio por tales riquezas mineras, y que debido a la ausencia de las Farc en la zona, grupos paramilitares buscan hacerse con las tierras, algo a lo que también debe sumarse la histórica presencia del ELN en la región.]

[El vocero indicó que durante el 2018 fueron asesinados doce habitantes del sector en medio de dicha disputa de territorio y que sin embargo no se ve un accionar efectivo por parte del Estado, ocasión que aprovechó para aclarar que “una solución para velar por la seguridad de la población no es la militarización”.]

[**“Así como las comunidades le apostaron a la mesa de diálogos entre el Gobierno y las Farc también le hemos apostado al diálogo entre el Gobierno y el ELN”**  afirma Víctor quien reiteró el llamado que han hecho las comunidades para que se retome la vía del diálogo entre ambas partes.   
  
Vega indica que desde que se dio la ruptura de la mesa de diálogos con el ELN se han intensificado los operativos tanto de la guerrilla como de la fuerza armada lo que hace más evidente las hostilidades en la zona“, situación que ha llevado a las comunidades ha plantearse el vivir en campamentos de refugios humanitarios pues no quieren desplazarse del territorio a raíz de esta ola de violencia.]

<iframe id="audio_32687290" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_32687290_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
