Title: Hospital de Kennedy pasa a cuidados intensivos
Date: 2016-04-19 12:53
Category: Movilización, Nacional
Tags: hospital kennedy, protestas hospital kennedy
Slug: hospital-de-kennedy-pasa-a-cuidados-intensivos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/04/Hospital-Kenndy.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Fotos: Twitter  ] 

###### [19 Abril 2016 ]

Desde las 5 de la mañana de este martes, usuarios y trabajadores del Hospital de Kennedy de la ciudad de Bogotá, protestaron frente a las instalaciones del centro hospitalario y se tomaron la Avenida 1° de Mayo, para denunciar que desde hace ocho días **no hay insumos, medicamentos, ni alimentación para los pacientes hospitalizados y que no han sido cancelados los salarios de marzo. **

De acuerdo con María Doris González, integrante de la Central Unitaria de Trabajadores, el **Subgerente de la Red Suroccidente, Ricardo Durán, manifestó que no sabe cuando pueden pagarse los salarios**, por lo que trabajadores y pacientes piden a la actual gerente de la institución dar una respuesta concreta al respecto.

El Subgerente también ha argumentado que esta crisis es consecuencia de las anomalías de la administración anterior; sin embargo, según González, en ningún otro hospital se ha atrasado el pago de salarios, o han hecho falta insumos o medicamentos. "Es grave" que **después de cuatro meses de posesión, el nuevo modelo de salud no haya solucionado la problemática del Hospital**, se está pensando en lo que dejó la anterior administración y no en las soluciones que debe presentar, agrega.

[![HKennedy\_\_\_\_](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/04/HKennedy____.jpg){.aligncenter .size-full .wp-image-22904 width="600" height="450"}](https://archivo.contagioradio.com/hospital-de-kennedy-pasa-a-cuidados-intensivos/hkennedy____/)

El Hospital de Kennedy durante los fines de semana recibe 300% más de los pacientes que puede atender, y junto al de Meissen y El Tintal atiende el 60 % de quienes viven al sur de Bogotá, por lo que los trabajadores aseguran que se manifestarán "hasta que paguen, **hasta que venga la gerente y diga ya compramos insumos, ya hay medicamentos y ya les vamos a pagar**".

Tras la movilización, el Subgerente de la entidad aseveró que los pagos no se habían hecho porque se estaba "haciendo una revisión exhaustiva y juiciosa de cada uno de los contratos", y **se comprometió con el pago de salarios a 900 contratistas**, mientras revisa el caso de 200 más.

<iframe src="http://co.ivoox.com/es/player_ej_11223629_2_1.html?data=kpaflJiadpqhhpywj5WXaZS1lpaah5yncZOhhpywj5WRaZi3jpWah5ynca7V04qwlYqliMKfpdTfy9iPi9Di24qwlYqldc3Z1IqfpZCnmbWhhpywj6jTstXVyM7cjbfFqMrjjJKSmaiReA%3D%3D&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u) ]o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)] 

   
   
 
