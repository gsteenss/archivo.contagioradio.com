Title: Sobreviviente de la masacre de Tibú, fue asesinado
Date: 2020-07-28 11:56
Author: CtgAdm
Category: Actualidad, DDHH
Tags: Asesinatos, Catatumbo, Desplazamiento
Slug: sobreviviente-de-la-masacre-de-tibu-fue-asesinado
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/07/Ed-t3FuWAAIrTfl.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/07/WhatsApp-Image-2020-07-28-at-11.34.23-AM.jpeg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

*Foto: Cortesia Ascamcat*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Este lunes 27 de Julio en horas de la noche la voceros de la organización social Ascamcat, denunciaron el asesinato de **Jhonny Alexander Ortiz de 23 años de edad,** en el municipio de Tibú , Norte de Santander.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/OlgaluQS/status/1287939212084420609","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/OlgaluQS/status/1287939212084420609

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

Según voceros de Ascamcat, el joven Jhonny Ortiz, fue atacado por **hombres armados que le dispararon en varias oportunidades**, causándole la muerte. Ortiz había sido desplazado 18 de julio junto con su hermano en condición de discapacidad y su madre, producto de la[](https://archivo.contagioradio.com/masacre-catatumbo-rastrojos/)masacre en la finca El limonar.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

**El hecho se da a un kilómetro del albergue humanitario dónde se encuentran más de 400 personas** que se habían desplazado luego de la masacre registrada el sábado 25 de julio en horas de la noche en la vereda Totumito Carboneras, en el límite entre Tibú y Cúcuta por parte del grupo paramilitar conocido como «Los Rastrojos».

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

(También le puede interesar leer: <https://www.justiciaypazcolombia.com/una-masacre-mas-en-el-sur-de-cordoba/>)

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
