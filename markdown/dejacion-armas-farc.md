Title: Con la dejación de armas se está cerrando la página de la guerra en Colombia
Date: 2017-06-14 16:30
Category: Nacional, Paz
Tags: acuerdo de paz, Implementación
Slug: dejacion-armas-farc
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/06/dejacion-de-armas.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: NC Noticias] 

###### [14 Jun 2017] 

En un acto histórico en donde participaron 300 integrantes de las FARC-EP y 500 habitantes de las comunidades cercanas a La Elvira, en Cauca, se completó el 60% de la dejación de armas de las FARC-EP. Pablo Catatumbo expresó que con ese acto “**las FARC-EP le están demostrando a Colombia y el mundo**, que están cerrando una página de la historia del país y empezando a escribir otra, la de la paz”.

Catatumbo, manifestó que los integrantes de esta guerrilla están comprometidos con la paz y agregó que hasta la fecha y con la puesta en marcha de los Acuerdos de Paz, **se ha evitado la muerte de más de 2.500 personas, que ahora aportarán al desarrollo de una nueva Colombia**. Le puede interesar: ["Dejación de armas de las FARC-EP marca un cambio en la historia del país"](https://archivo.contagioradio.com/42169/)

De igual forma hizo un llamado al gobierno para que cumpla con los compromisos que le corresponden y se avance en la garantía jurídica, económica y de vida, para los integrantes de las FARC-EP, en su reincorporación a la sociedad civil y reiteró la preocupación por la **permanencia y accionar de grupos paramilitares en diferentes territorios del país**.

### **La marcha de las FARC hacia la vida civil "es el mejor símbolo de este proceso" ** 

Por su parte, Jean Arnault, jefe de la Misión en Colombia de las Naciones Unidas manifestó que este acto, que cumple con lo establecido en el cronograma para la reincorporación de los integrantes de las FARC-EP, **también sirve para hacer un balance de lo que ha significado el proceso de paz**.

“El resultado de la determinación está a la vista, el acuerdo generó muchas expectativas y las están cumpliendo, la marcha de las FARC **hacia la vida civil y la política sin armas es posiblemente el mejor símbolo de este proceso**” afirmo Arnault. Le puede interesar:["Asi vivieron los habitantes de La Elvira en Cauca la dejacion de armas de las FARC-EP"](https://archivo.contagioradio.com/asi-vivieron-los-habitantes-de-la-elvira-en-cauca-la-dejacion-de-armas-de-las-farc/)

### **Falta que el gobierno y el congreso cumplan en la implementación** 

Para el senador Iván Cepeda, este acto de la dejación de armas es una acción de desprendimiento y valentía por parte de los integrantes de las FARC “**esta es una nueva muestra de confianza, en que se va a cumplir con los acuerdos a pensar de las dificultades**”.

Una de esas dificultades es la implementación de la Jurisdicción Especial de Paz que podría ser modificada en el Congreso de la República, referente a este tema Cepeda manifestó que “**hay fuerzas políticas que van desde la posición de acabar con la jurisdicción** hasta introducir modificaciones que desvirtúan la esencia y naturaleza de este nuevo sistema de justicia”

Sin embargo, Cepeda, aseguró que será la veeduría de la sociedad y la presión de las organizaciones defensoras de derechos humanos, víctimas y el movimiento popular, son las que podrían evitar que estas modificaciones se realicen. Le puede interesar: ["Altas Cortes siguen avanzando en implementacion de los Acuerdos de Paz"](https://archivo.contagioradio.com/altas-cortes-siguen-avanzando-en-implementacion-del-acuerdo-de-paz/)

<iframe id="audio_19272123" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_19272123_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
