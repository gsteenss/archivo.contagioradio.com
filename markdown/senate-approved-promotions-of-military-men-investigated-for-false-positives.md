Title: Senate approved promotions of military men investigated for 'false positives'
Date: 2019-12-23 17:13
Author: CtgAdm
Category: English
Tags: Armed Forces, extrajudicial killings, false positives
Slug: senate-approved-promotions-of-military-men-investigated-for-false-positives
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/Ascensos-Militares.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Photo: @honohenriquez] 

 

**Follow us on Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

[While the House of Representatives quietly passed President Iván Duque’s tax reform bill early Friday morning, the Senate also approved the promotions of 40 Armed Forces members, three of which are implicated in human rights violations such as extrajudicial killings known commonly in Colombia as "false positives."]

[According to the lawyer Diana Salamanca, of the National Movement of Victims of State Crime (MOVICE), the Senate’s decision ignored warnings and information compiled by various human rights organizations that show evidence of these military men's supposed criminal actions. Their promotions, Salamanca said, violate the victims' rights to non-repetition.]

The general Juan Carlos Ramírez is investigated for nine criminal offenses, including homicide of a protected person, judicial resolution fraud, influence peddling and the falsification of a public document, according to the Attorney General's Office. MOVICE claimed at least 16 extrajudicial killings were committed by the general Sergio Alberto Tafur's subordinates.

The then general Hernando Garzón Rey is also mentioned in an extrajudicial killing case. It's also known that the captain Maritza Soto filed a report in the Inspector General's Office, accusing Garzón of workplace and sexual harassment. The captain also said she denounced the alleged crimes to the head of the Rapid Deployment Force's Chiefs of Staff, the commander of the Volcano Task Force, the commander of the Second Division and the commander of the Army, but received no support.

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
