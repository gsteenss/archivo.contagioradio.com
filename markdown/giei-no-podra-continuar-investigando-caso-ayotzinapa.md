Title: GIEI no podrá continuar investigando caso Ayotzinapa
Date: 2016-04-25 20:19
Category: El mundo
Tags: Ayotzinapa, GIEI, mexico
Slug: giei-no-podra-continuar-investigando-caso-ayotzinapa
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/04/Ayotzinapa-e1461627358355.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Vanguardia México] 

###### [25 Abr 2016]

Los padres de los 43 normalistas de [Ayotzinapa](https://archivo.contagioradio.com/?s=ayotzinapa) se movilizarán este martes para conmemorar la desaparición de sus hijos pero además en rechazo de la expulsión del Grupo Interdisciplinario de Expertos Independientes, GIEI, que buscaba la verdad frente a lo sucedido el 26 de septiembre de 2014.

La expulsión del GIEI, se da luego de la presentación del II Informe sobre la investigación que adelantaba el grupo de expertos convocados por la Comisión Interameriacana de Derechos Humanos. El informe evidencia todas las fallas de las instituciones mexicanas para una adecuada investigación, por lo que **GIEI emitió 22 recomendaciones y además solicitó mantener la investigación sobre un quinto autobús.**

Los investigadores **Ángela Buitrago, Claudia Paz, Francisco Cox, Carlos Beristáin y Alejandro Valencia,** aseguraron que en la indagatoria faltó comunicación pública para evitar filtración, atención adecuada y manejo público de las víctimas, informes médicos adecuados a estándares internacionales y actitud cooperativa frente a asistencia internacional,  por lo que recomendaron la disminución de la burocracia en las indagatorias, evitar que las violaciones de derechos humanos se juzguen bajo el rubro de delincuencia organizada y evitar la fragmentación de los procesos.

Así mismo, señalaron la presunta existencia **de 17 casos de tortura y la participación de policías de Huitzuco en la desaparición de los jóvenes.** El informe señala que unidades policiales de municipios como Iguala, Cocula y Huitzuco, Guerrero, actuaron de forma coordinada para evitar que los normalistas salieran de Iguala, incluso Ángela Buitrago lo calificó como “coordinación perfecta” pues lograron generar un círculo de control para evitar la salida de los autobuses de Iguala.

Por su parte, James Cavallaro, presidente de la Comisión Interamericana de Derechos Humanos, CIDH, lamentó la posición del Estado mexicano de no dar la prórroga al GIEI, y aunque el presidente Enrique Peña Nieto anunció que la PGR analizará el informe y atenderá las recomendaciones, los investigadores denunciaron que su trabajo fue obstaculizado por las autoridades y coincidieron en desestimar **el documento generado del tercer peritaje de fuego realizado en el basurero de Cocula y en el que resolvió la existencia de un incendio controlado en el lugar.**

La jornada del martes tiene prevista la realización de una marcha de las antorchas desde el Ángel de la Independencia hasta el Hemiciclo, y en el Estado de Guerrero se esperan marchas simbólicas por la desaparición de los estudiantes.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
