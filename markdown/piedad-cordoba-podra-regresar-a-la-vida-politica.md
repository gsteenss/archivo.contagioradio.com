Title: Piedad Córdoba recupera sus derechos políticos tras fallo del Consejo de Estado
Date: 2016-10-11 16:22
Category: Judicial, Nacional
Tags: cargos públicos, destitución, piedad cordoba, vida política
Slug: piedad-cordoba-podra-regresar-a-la-vida-politica
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/10/Piedad-e1476220511415.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: fotojaramillo] 

###### [11 Oct 2016] 

El Consejo de Estado tomó la decisión de tumbar la segunda sanción que le había sido impuesta por la Procuraduría a **Piedad Córdoba**, en la cual era destituida e inhabilitada por 14 años para ejercer cargos públicos.

Esta inhabilidad y destitución, fue dispuesta a Córdoba por entregar supuestamente en el año 2010,  la suma de \$25 millones a Ricardo Montenegro, quien para ese momento se desempeñaba como su asesor y que aspiraba a ser Representante a la Cámara por el departamento de Atlántico para las elecciones legislativas de aquel año.

Esta determinación, sumada a la que tomó la Sala Plena del Consejo de Estado que se conoció el pasado 9 de Agosto, en donde también anuló la destitución e inhabilidad por 18 años, le permitirá a Córdoba, si así lo desea **postularse a cargos de elección popular.**

La decisión fue tomada teniendo en cuenta las pruebas que fueron recolectadas por el Ministerio Publico, dentro de las cuales se encontraba el computador de Luis Edgar Devia alias “Raúl Reyes”, que fue confiscado luego de la Operación Fenix en la que el jefe guerrillero cayó abatido.

La Sala Plena aseguró que **la Procuraduría vulneró los derechos de la ex senadora** pues decidió sancionarla sin tener previo cuidado con la legalidad que podrían haber conservado las pruebas recolectadas.

Las reacciones en redes sociales no se han hecho esperar, incluyendo las de la propia Piedad Córdoba quien ha manifestado “Felicito a @consejodeestado, con sus recientes fallos comienza a reconstruir daños que otros le hicieron a la democracia y a la justicia”.

Lea también: [Piedad Córdoba podría recuperar sus derechos políticos](https://archivo.contagioradio.com/piedad-cordoba-podria-recuperar-sus-derechos-politicos/)

###### Reciba toda la información de Contagio Radio en su correo [[bit.ly/1nvAO4u]

######  
