Title: Es refrendar la paz, no la gestión de Santos
Date: 2016-06-28 08:17
Category: Carolina, Opinion
Tags: colombia, paz, proceso de paz
Slug: es-refrendar-la-paz-no-la-gestion-de-santos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/06/carmela-maria.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: [Carmela María  ](https://www.flickr.com/photos/aycarmelamaria/27782517612/in/photostream/)] 

#### [Carolina Garzón Díaz](https://archivo.contagioradio.com/carolina-garzon-diaz/) - [@E\_Vinna](https://twitter.com/E_Vinna) 

###### 28 Jun 2016 

[El 23 de junio de 2016 es un día que los colombianos siempre guardaremos en la memoria y en el corazón. El anuncio del acuerdo sobre el fin del conflicto armado nos conmovió hasta las lágrimas, nos llenó de esperanza y nos indica el inicio de un largo camino en el que todos estamos involucrados.]

[Otro de los anuncios más importantes de ese día fue el relativo a la refrendación. Luego de tres años y medio de conversaciones, el Gobierno y las Farc han llegado a un acuerdo sobre la refrendación que pondrá a consideración del pueblo colombiano el Acuerdo final: se acogerán a la decisión que profiera la Corte Constitucional sobre los alcances del Plebiscito, aceptarán “el mecanismo de participación popular que la Corte indique y en los términos que ese alto tribunal señale”, sostiene el texto.]

[No es un anuncio menor, además de ser la salida a un tema álgido de la negociación, significa que la guerrilla de las Farc reconoce a esta alta corte y se acoge a sus decisiones. La aceptación de la institucionalidad por parte de este grupo insurgente es uno de los logros más importantes de este proceso. Es decir, habrá refrendación y pronto conoceremos cuál será el mecanismo.]

[Pero este anuncio, que coincidió con la noticia mundial de Brexit y la consecuente dimisión del Primer Ministro Británico David Cameron, le permitió a nuestros periodistas y columnistas hacer conjeturas sobre la hipotética renuncia del presidente Juan Manuel Santos ante una posible pérdida de la refrendación del Acuerdo final. Y ese es un terreno muy peligroso para la paz.]

[Hay que ser claros: la votación será única y exclusivamente sobre el Acuerdo de paz entre Gobierno y Farc; no es una revocatoria del presidente Santos, tampoco es un mecanismo para evaluar su gestión, ni siquiera será un medio para su renuncia. Preocupa profundamente que esa relación entre la refrendación y la revocatoria termine por costarnos el fin del conflicto que tanto celebramos el pasado jueves.]

[La pedagogía sobre los acuerdos debe contemplar esta distancia y evitar que sean confundidos con las políticas públicas y los planes de desarrollo. Si bien la paz es integral, este proceso con las Farc no pretende solucionar todos los graves problemas de este país, sino evitar que nos asesinemos los unos con los otros y que lo ocurrido en el marco del conflicto jamás vuelva a suceder.]

[Miles de personas apoyamos la paz siendo críticas de la gestión del presidente y ejerciendo una clara oposición a su gobierno. Respaldar el proceso de paz no es darle el aval a Santos ni hacerle venia; rodear el proceso es nuestro legítimo derecho y no significa, de ninguna forma, que seamos ciegos a la realidad, crédulos o inocentes, significa que defendemos una apuesta válida y necesaria.]

[Lo anuncio desde ya, mi voto será por la refrendación del proceso de paz. Con este voto asumiré mi responsabilidad histórica con las generaciones que vienen y así mismo el trabajo de seguimiento, verificación y cumplimiento de cada uno de los acuerdos.]

------------------------------------------------------------------------

###### [Las opiniones expresadas en este artículo son de exclusiva responsabilidad del autor y no necesariamente representan la opinión de la Contagio Radio.] 
