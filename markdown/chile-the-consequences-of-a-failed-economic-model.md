Title: Chile: the consequences of a failed economic model
Date: 2019-10-22 20:54
Author: CtgAdm
Category: English
Tags: Chile, curfew, Sebastián Piñera
Slug: chile-the-consequences-of-a-failed-economic-model
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/74428421_2495675590701794_8095273841196007424_n-e1571700528781.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Photo: Guillermo Jaramillo Castillo] 

 

**Follow us on Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

[Latin America has turned its gaze to Chile following five days of mass mobilizations in protest of the country’s economic policies, which critics say have failed despite being praised throughout the region as an exemplary model. In response to the demonstrations, president Sebastián Piñera decreed a curfew, enforced by more than 10,500 police officers. Since then, more than 10 people have been assassinated, more than 1,900 arbitrary detentions have been reported and fears of repression reminiscent of the Agosto Pinochet dictatorship have surfaced.]

### **The reasons for Chile to rebel**

[Juan Carlos Pérez, a university professor in political communication in Chile, explained that the pretext for the protests was the rise in transportation fares, but behind this exists a series of growing problems that followed the end of the dictatorship. Chileans hoped the transition to democracy would lead to social change, but according to Pérez, conditions left behind by the dictatorship have hindered any advances. ]

[One example is the binomial voting system that benefited the right in Congress and prevented any amendments to the Constitution. This meant that Congress was limited in its ability to modify key, economic issues in favor of the people, such as the pension system. According to Pérez, the system has created “miserable” conditions for retirees due to the lack of measures to sustain decent wages. The professor also said that the supposed center-left parties that took power after the dictatorship found a political and economic system tied to Pinochet’s conditions and “forgot their role as agents of change and adapted to the system.”]

[Still, the Chilean people have challenged these policies in recent years. In 2011, education protests followed the lead of other social movements. But at the time, despite the strength of these movements, “the Piñera and \[Michelle\] Bachelet governments failed to read or respond to the demands.”]

### **Friday’s march: more than a ride on the subway**

[Regarding the recent social upheaval, the professor said that the initial protests responded to the conflicts of a public school with authorities that lasted a year and a half. “A way for them to express their rejection was to call for a massive fare-dodging protest,” he said. The police responded by targeting youth between the ages of 12 and 14 years old, which provoked more people to join the demonstrations against the transport system and police brutality.]

[Still, Pérez insisted that the protests address many issues. “Chile is an unequal country. It is very indebted. The middle class is very indebted because it pays about 50 o 60% of its salary in debt,” he said. Meanwhile, the image that is sold of the country abroad is one of an economic powerhouse. ]

[Friday’s protests grew out of control, with demonstrators burning the subway and rioting in the streets. In the impoverished outskirts of Chile’s capital, some looted stores and started fires. Given the situation, Piñera issued a decree that froze the subway fare, but for Pérez, the measure won’t be enough to quell such a complex situation.]

### **“The economic model failed”**

[Pérez said that during the Chilean dictatorship, those that led economic policy were the “Chicago Boys,” men trained in the United States by the economist Milton Friedman. They established a free market system and an economy dependent on the export of prime materials. According to the professor, “this model was implemented and it has been very difficult to dismantle” until finally “the model failed.”]

[The professor laid out two, principal causes for the model’s failure. First, the market was allowed to control health, education and even water, which is under concession, and managed by the private sector. Second, the economic war between China and the United States took the country by surprise, affecting its exports of copper and other materials. Still, Pérez said that these factors helped to galvanize sectors of society that had previously never taken part in these demonstrations. ]

[“We are now hearing voices that cry out for a constitutional reform, for a constituent assembly and to stop this intervention of the private world,” Pérez said. ]

### **Chile’s current situation**

[After the social mobilizations last week, citizens denounced on social media and news outlets that more than 10 people had been killed, almost 1,900 detained and more than 10,000 members of the Armed Forces mobilized amidst a state-imposed curfew. According to President Piñera, the government faces “a war” with a “powerful enemy...whose only purpose is to produce as much damage as possible.”]

[The president of the Association of Retired Navy Officers declared that the retired officials are ready to enlist again in the Armed Forces because they have “the preparation and the vast experience of what it means to organize and lead a country during a troubled moment.”.]

[A resident of the Los Andes, Chile, who preferred to remain anonymous, told Contagio Radio that “we all know that when the military appeared in 1973, they disappeared people, killed people and that is repeating itself.” As a result, people have rejected the imposition of a curfew and the declaration of war made by Piñera. Still, the resident said that the demonstrators weren’t scared and that they would continue to go out into the streets — even in tranquil towns such as Los Andes. ]

[In regard to the role the media played in the protests, the Chilean resident said that traditional news outlets are not adequately informing the public on human rights violations committed by the police. He added that they are focusing on the looting, which prompts people to panic. For this reason, he said that social media has been an essential tool to share the realities of what is happening in Chile.]

[Pérez said that this past Sunday, Oct. 20, various social organizations, student movements and pension fund groups convened to agree on specific demands and to outline a political direction for the protests. The professor said that it remained to be seen how leftist parties would organize themselves to achieve “some space within the Senate or the House to generate some type of pressure and create changes.”]

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
