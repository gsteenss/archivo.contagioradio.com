Title: Actuamos administrando justicia imparcial, objetiva y transparente: Altas Cortes y JEP
Date: 2019-05-14 13:07
Author: CtgAdm
Category: Judicial, Política
Tags: Cesar Gaviria, corte, JEP, Visa
Slug: justicia-imparcial-objetiva-transparente-altas-cortes-jep
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/Presidentas-altas-Cortes.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: @JEP\_Colombia] 

Este martes la **Corte Constitucional, el Consejo de Estado y la Jurisdicción Especial para la Paz (JEP)** realizaron una rueda de prensa en la que se refirieron al "contexto de presiones" en el que están estos tribunales; allí se refirieron al retiro de las visas por parte de Estados Unidos a dos magistrados de la Corte Constitucional, y reiteraron su vocación a ejercer sus "funciones jurisdiccionales con autonomía e independencia de cualquier injerencia proveniente de cualquier fuente".

La encargada de hacer la declaración ante los medios fue la doctora Gloria Stella Ortiz, presidenta de la Corte Constitucional, quien se mantuvo al margen de la discusión sobre las visas que habrían sido retiradas a los magistrados Antonio José Lizarazo y Diana Fajardo que integran este Tribunal. La Presidenta en cambio, afirmó que como integrantes de la rama judicial, su deber es **"la correcta y efectiva administración de justicia (...) sin beneficios, predisposiciones ni prejuicios"**.

En ese sentido, Ortiz aseguró que "el país puede tener la tranquilidad de que quienes ejercemos la magistratura actuaremos de conformidad con nuestras convicciones, con la firmeza de preservar la institucionalidad, y con el compromiso que hemos adquirido ante la sociedad de **administrar justicia imparcial, objetiva y transparente".** A esta afirmación, la Togada sostuvo que el presidente fue informado de lo ocurrido con las visas de los magistrados. (Le puede interesar: ["Comunidades reconocieron el trabajo de congresistas que defendieron la JEP"](https://archivo.contagioradio.com/comunidades-congresistas-jep/))

En la conclusión de la rueda de prensa Ortiz manifestó que en la misma no se habían hecho presentes los dirigentes del Consejo Superior de la Judicatura y la Corte Suprema de Justicia porque en el primer caso, el organismo tiene una función más "logística" que de administración de justicia, mientras en el segundo, la Corte ya se había pronunciado sobre el "contexto de presiones". (Le puede interesar:["Defendamos la Paz denuncia presiones contra magistrados en Colombia"](https://archivo.contagioradio.com/defendamos-la-paz-denuncia-ante-la-onu-presiones-contra-magistrados-en-colombia/))

<iframe src="https://www.youtube.com/embed/pMgLH6QnoQs" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

### **Cesar Gaviria: Duque y EE.UU. estarían creando las presiones a las Cortes**

En una carta publicada este martes, el ex presidente y director del Partido Liberal Cesar Gaviria, manifestó su preocupación ante la **"indebida presión que se viene ejerciendo sobre las supremas Cortes y la JEP, ya no solo por el Gobierno Nacional también por los Estados Unidos"**; situación que en consideración de Gaviria, termina afectando la implementación del Acuerdo de Paz, pues entrega la misma "al criterio de funcionarios en Washington", en medio de una violación a nuestra soberanía.

Sobre este tema, y las presiones de Estados Unidos para que Colombia retome las fumigaciones con glifosato, el Ex Presidente manifestó que están dispuestos a respaldar al Gobierno en sus logros alcanzados; pero esto no será posible si Duque "no se defiende o no está interesado en defenderse". Esta comunicación se sumó a la del colectivo Defendamos la Paz, que pidió protección y especial vigilancia de la ONU a los magistrados de las Altas Cortes por los "ataques y presiones de las que han sido víctimas recientemente".

[Comunicado Corte Constitucional, Consejo de Estado y JEP](https://www.scribd.com/document/409989780/Comunicado-Corte-Constitucional-Consejo-de-Estado-y-JEP#from_embed "View Comunicado Corte Constitucional, Consejo de Estado y JEP on Scribd") by [Contagioradio](https://www.scribd.com/user/276652802/Contagioradio#from_embed "View Contagioradio's profile on Scribd") on Scribd

<iframe id="doc_92099" class="scribd_iframe_embed" title="Comunicado Corte Constitucional, Consejo de Estado y JEP" src="https://es.scribd.com/embeds/409989780/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-XIOsOyFI3FsN8vQRGPZr&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="0.7729220222793488"></iframe>

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
