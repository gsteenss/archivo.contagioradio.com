Title: Abierta convocatoria del premio Franco-Alemán de DDHH Antonio Nariño
Date: 2017-10-11 13:10
Category: DDHH, Entrevistas
Tags: defensores de derechos humanos, Derechos Humanos, Organizaciones sociales, premio Antonio Nariño
Slug: abierta-la-convocatoria-del-premio-franco-aleman-de-ddhh-antonio-narino
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/10/premio-antonio-nariño-e1507745357935.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Foro Interétnico FISCETNICO] 

###### [11 Oct 2017] 

La Embajada de Francia en Colombia en conjunto con la Embajada de Alemania, realizan la octava convocatoria para el **premio Franco-Alemán de Derechos Humanos, Antonio Nariño**. El premio busca visibilizar e incentivar el trabajo de los defensores de derechos humanos en el país.

Desde el 2009, el premio Franco-Alemán Antonio Nariño, ha hecho un **reconocimiento a diferentes procesos y acciones** realizadas por personas y organizaciones que han trabajado por defender los derechos humanos en el país. Entre los ganadores han estado, por ejemplo, La Fundación Nydia Erika Bautista, El Museo de la Casa de la Memoria de Medellín o el Foro Interétnico Solidaridad Chocó.

De acuerdo con Jean Marie Druette, consejero político de la embajada de Francia en Colombia, **el premio es un reconocimiento símbolo de reconciliación, una apuesta** que viene desde el fin de las Guerras Mundiales, cuando “los europeos decidimos crear procesos de unidad basados en la reconciliación”. (Le puede interesar: ["Colombia sin garantías para la defensa de los derechos humanos"](https://archivo.contagioradio.com/colombia-sin-garantias-en-la-defensa-de-los-derechos-humanos/))

Colombia “es un país que ha vivido muchos años de violencia que sigue existiendo” y por eso es tan **valioso el trabajo de los defensores y defensoras** señala el diplomático. Teniendo en cuenta el marco del proceso de paz, ambas embajadas, ven como fundamental reconocer los esfuerzos de atención a los derechos humanos que llevan a la construcción de la paz y la reconciliación.

Druette manifestó que es necesario **reconocer “el coraje” de las iniciativas de las organizaciones** de la sociedad civil teniendo en cuenta la situación actual en la que viven los defensores de derechos humanos en Colombia.

Finalmente, el consejero indicó que las organizaciones, grupos o personas que estén interesadas en participar en la convocatoria **deben inscribir un proyecto de defensa de derechos humanos** que ya se haya realizado. (Le puede interesar: ["Delegación Asturina denuncia la reparamilitarización del Estado colombiano"](https://archivo.contagioradio.com/delegacion-asturiana-denuncia-la-reparamilitarizacion-del-territorio-colombiano/))

[El plazo final es el **domingo 22 de octubre** hasta la media noche y las propuestas deberán ser enviadas por correo a las sedes de la embajada de Francia o Alemania en Bogotá o en los correos electrónicos maud.abaa@diplomatie.gouv.fr o pol-hosp2@bogo.auswaertiges-amt.de previo diligenciamiento del formulario. El premio será entregado el 12 de diciembre. ]

Puede encontrar más información aquí: http://bit.ly/2uXRskH

<iframe id="audio_21404418" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_21404418_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio]{.s1}](http://bit.ly/1ICYhVU)[.]{.s1}
