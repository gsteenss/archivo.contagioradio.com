Title: Asesinan al líder sindical Alexis Vergara en Cauca
Date: 2020-03-10 16:49
Author: AdminContagio
Category: Actualidad, Líderes sociales
Tags: CGT, Lider social
Slug: asesinan-al-lider-sindical-alexis-vergara-en-cauca
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/03/Alexis-Vergara-2.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:html -->

<figure class="wp-block-audio">
<audio controls src="https://co.ivoox.com/es/alberto-guzman-sobre-asesinato-alexis-vergara_md_48807549_wp_1.mp3&quot;">
</audio>
  

<figcaption>
Entrevista &gt; Según Alberto Guzmán, presidente de la Federación de Trabajadores del Valle del Cauca (Fegtravalle)

</figcaption>
</figure>
<!-- /wp:html -->

<!-- wp:paragraph {"align":"justify"} -->

La Confederación General del Trabajo denunció este 10 de marzo que en en el corregimiento de Llano de Tabla en Caloto, Cauca, fue asesinado  Alexis Vergara, que trabajaba como delegado ante la Asamblea Sindical de SINTRAINCABAÑA.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

  
Según Alberto Guzmán, presidente de la Federación de Trabajadores del Valle del Cauca (Fegtravalle), los hechos ocurrieron cuando Alexis Vergara (que trabajaba como quemador de caña) finalizaba su labor cuando fue abordado por desconocidos que dispararon en tres ocasiones contra su humanidad. 

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### "En este país no puede haber ni un muerto más, ni un líder social ni sindical más"

<!-- /wp:heading -->

<!-- wp:paragraph -->

Como delegado sindical, Alexis Vergara participaba de las asambleas y escuchaba las inquietudes de sus compañeros. Aunque no se conoce que  hubiera sido amenazado con anterioridad, el presidente de Fegtravalle aclara que Alexis es hijo de Raúl Vergara, exconsejal y actual presidente de Sintraincabaña de Ingenios La Cabaña.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

  
El padre de Alexis, ha sido amenazado por su trabajo político en el Cauca. Según Guzmán, dicho sindicato ha priorizado la defensa del empleo y la concertación entre sindicatos, labor que "a muchos no les ha gustado" y que podría ser la razón por la que se se habría atacado a Alexis. (Le puede interesar: ["Segundo atentado en menos de un mes contra directivos de la USO en Meta"](https://archivo.contagioradio.com/segundo-atentado-en-menos-de-un-mes-contra-directivos-de-la-uso-en-meta/))

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### "Es casi imposible hacer sindicato en esta zona del país" 

<!-- /wp:heading -->

<!-- wp:paragraph -->

  
Según Alberto Guzmán, la acción sindical resulta muy difícil de ejercer en el Cáuca, pese a ello, relata que cerca del 95% de los trabajadores de caña están sindicalizados, sin embargo resulta una labor que se ha visto reducida, "el norte del Cauca es una zona donde hay un alto desempleo, no hay fuentes de empleo, hay ausencia del Estado y pocas oportunidades" explica el presidente sindical sobre la mano de obra del corte de Caña en el Cauca.  

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Tras el asesinato de Alexis Vergara,  señala que existe silencio en la comunidad y es una situación que además ha generado "miedo en el personal y trabajadores", pese a ello Guzmán es enfático en que  el trabajo sindical "hay que seguirlo haciendo, al igual que las reclamaciones del día a día de las empresas, continuaremos con el trabajo, pero necesitamos garantías y presencia del Estado". 

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78980} /-->
