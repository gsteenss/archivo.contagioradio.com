Title: 50.000 personas marchan en Rusia en homenaje al líder opositor asesinado en Moscú
Date: 2015-03-02 21:25
Author: CtgAdm
Category: El mundo, Otra Mirada
Tags: Boris Nemtsov asesinado en Moscú, Líder de la oposición rusa asesinado en Moscú, Marcha contra Putin, Putin acusado de vilor artículos de la Constitución, Violaciones de DDHH rusia
Slug: 50-000-personas-marchan-en-rusia-en-homenaje-al-lider-opositor-asesinado-en-moscu
Status: published

###### Foto:Losandes.com 

##### Entrevista con[ Anna Shkolnik]: 

<iframe src="http://www.ivoox.com/player_ek_4155275_2_1.html?data=lZail5ebeY6ZmKiakpyJd6Kll5KSmaiRdo6ZmKiakpKJe6ShkZKSmaiRkcrgxtiYxsqPscLiysvS1dnFstXZ1JDaw9fHrMLijMrbjbLTt8SZpJiSpKaPqc%2BfydTax9PFrsafwtGah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

**Miles de rusos marcharon en homenaje a Boris Nemtsov, líder de la oposición asesinado** a tiros entre la noche del viernes y el sábado en el centro de Moscú.

**Boris Nemtsov** era el líder opositor más fuerte y **quién mas duramente había criticado las políticas de Putin y la guerra en Ucrania**. Era uno de los principales organizadores de la "Marcha de la primavera contra la guerra en Ucrania y contra el gobierno de Putin" convocada para este domingo.

Después de su asesinato la marcha se convirtió en un homenaje al mismo y cambió de recorrido para pasar por el puente donde fue asesinado.

Entre las **reivindicaciones de la oposición** se encuentra el rechazo a las reiteradas violaciones de los DDHH del gobierno contra la población rusa, la grave crisis económica, la corrupción y la devaluación del rublo.

Las distintas formaciones políticas acusan al régimen de haber acabado con la democracia y de haberle concedido el poder a las mafias.

Por último destacan la censura y los ataques contra la libertad de expresión y el asesinato de líderes de la oposición como ya sucedió con los diputados Galina Staravóitova o Serguéi Yushenkov o la periodista Anna Politkóvskaya, quién escribió un libro sobre la Rusia de Putin denunciando todas las violaciones de DDHH de su gobierno.

En la marcha se produjeron **50 detenciones de activistas y un herido**, de distintas ideologías y formaciones políticas, todos ellos acusados de desobediencia a la autoridad.

**39 de ellos fueron liberados en el transcurso del 1 de marzo**, entre ellos estaba un líder ucraniano, al que más tarde retiraron la acusación. Algunos de ellos fueron entregados por la propia organización de la marcha por portar simbología nazi.

Entre las detenciones destaca la del **activista Nelayev, miembro de la organización social "Solidarnost"** de la cual era fundador el líder opositor asesinado el viernes, cuando en el transcurso de la marcha la policía quería detener a las personas que portaban banderas de Ucrania y él se hizo responsable.

Anna Shkolnik activista defensora de DDHH, afirma que Nemtsov era el líder opositor más valorado por la opinión publica, y por ello era odiado por muchos sectores contrarios.

Según ella todo apunta hacia grupos violentos de nacionalistas alimentados en los últimos tiempos por el gobierno actual. **Entre las consecuencias del asesinato está la gran marcha del domingo, calificada como una de las mayores movilizaciones de la oposición.**
