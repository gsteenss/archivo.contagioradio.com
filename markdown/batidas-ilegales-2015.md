Title: En 2015 se registraron 71 casos de batidas ilegales en Colombia
Date: 2016-04-29 15:28
Category: DDHH, Nacional
Tags: batidas ilegales, Derechos Humanos, Ejército Nacional, proceso de paz
Slug: batidas-ilegales-2015
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/04/Batidas.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Las 2 Orillas 

###### [29 Abr 2016] 

Durante el **2014 se presentaron 58 batidas militares en Colombia y para el 2015, se evidenció que esta práctica de reclutamiento aumentó a 71,** pese a que la Corte Constitucional ha alertado sobre la ilegalidad y la vulneración de derechos humanos en estos procedimientos. Así lo denunciaron, el  Comité Permanente por los Derechos Humanos CPDH y la Acción Colectiva de Objetores y Objetoras de Conciencia ACOOC en la presentación de sus informes que documentan esta problemática durante los años 2014 y 2015.

Julián Camilo Bello, investigador del CPDH, asegura que ha habido un aumento en detenciones arbitrarias sobre todo en lugares donde se vive permanentemente un conflicto, así mismo, ha habido un aumento de reclutamiento asociado al modelo de desarrollo, debido a que el gobierno mantiene su intención de fortalecer los batallones minero-energéticos encargados de proteger principalmente la infraestructura vial y petrolera.

Bogotá y Medellín son los lugares donde más se frecuenta esta práctica militar. De acuerdo con datos de la oficina del represente del Polo Democrático, Alirio Uribe, solamente en Bogotá **entre Enero y Octubre de 2015 se presentaron 88 batidas ilegales por parte de las Fuerza Militares, de las cuales fueron víctimas 299 jóvenes**. En la ciudad, Kennedy y Soacha son las zonas donde más se recluta jóvenes.

De acuerdo con datos de la Subdirección de Reclutamiento y Control de Reservas, **en 2014 se reclutaron más de 86 mil soldados regulares y más de 35 mil soldados bachilleres.** A su vez, según datos del Ministerio de Defensa, para 2013 la cantidad de jóvenes incorporados a **batallones especiales minero-energéticos** y viales representa el 6,2% de la cantidad de soldados regulares reclutados en Colombia y para el 2014 la cifra representa 6,9 %, es decir que **se pasó de 4.994 en 2013 a 5.995 en 2014.**

El informe evidencia que en su mayoría, **los jóvenes que se ven obligados a presentar el servicio militar, son de recursos bajos y en condición de vulnerabilidad.** Algunos casos reportados demuestran que incluso han sido reclutados jóvenes que eran la cabeza de su hogar y el único sustento monetario de su familia. Además, el documento evidencia que se han presentado casos de **varios jóvenes reclutados que han sido enviados a lugares identificados como zonas rojas** o de alto conflicto, poniendo en riesgo su vida e integridad física.

Aunque se conoce de la ilegalidad de las batidas, en el informe se evidencia que algunos mandos del Ejército aseguran que se ven obligados a acudir a mecanismos como las batidas porque deben cumplir con unas cuotas establecidas cada año por la autoridad militar. Por ejemplo **para el año 2014 los ciudadanos incorporados a prestar el servicio militar fueron 86.854 y la cuota para ese año fue de 132.300. Para el 2015 fue de 101.161.**

Frente a esta realidad, las organizaciones subrayan que el actual contexto de negociaciones del gobierno colombiano con las guerrillas de las FARC y el ELN, plantea el reto de construir una paz con justicia social desde las instituciones y la sociedad civil, pero es alarmante que el Ejército Nacional continúe realizando detenciones arbitrarias con fines de reclutamiento para el servicio militar obligatorio.

Pero en contraste con ese ideal, actualmente en el Congreso de la República cursa un **proyecto de Ley que modifica el reclutamiento y abre las posibilidades legales para que se desarrollen detenciones arbitrarias con fines de reclutamiento**, hace que la libreta militar vuelva a ser requisito para el pasaporte y la licencia de conducción, y amplía las posibilidades para imponer mayores castigos económicos a jóvenes, entre otras disposiciones.

“Estamos hablando de la posibilidad que pare la guerra en Colombia y que pensemos un Ejército y una Policía para el posconflicto, pero el gobierno se resiste a esos cambios, y a pesar de que el presidente Santos se había comprometido a revisar el servicio militar obligatorio, un proyecto busca fortalecer el servicio militar, y son tan cínicos que se inventaron un artículo para decir que cuando los soldados mueren o quedan mutilados por fallas de los comandantes solo tienen el derecho a cobrar los seguros, pero no pueden demandar al Estado, para decir prácticamente que un soldado no vale nada”, expresó en la presentación del informe Alirio Uribe, quien añadió que **las batidas pueden ser catalogadas como "concierto para delinquir entre la Policía, las UPJ y el Ejército Nacional".**

<iframe src="http://co.ivoox.com/es/player_ej_11357090_2_1.html?data=kpagl5yUfZGhhpywj5aWaZS1kZ2ah5yncZOhhpywj5WRaZi3jpWah5yncarix9Tfz8qPhsLoysnO1ZCxrc3d1cbfx9iPcYzBhqigh6eXssrXwpC50d%2FFstChhpywj6jTstXVyM7cjbfFqMrjjJKSmaiReA%3D%3D&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
