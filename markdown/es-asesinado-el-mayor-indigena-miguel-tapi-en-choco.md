Title: Es asesinado el Mayor indígena Miguel Tapi en Chocó
Date: 2020-12-04 11:41
Author: AdminContagio
Category: Actualidad, Líderes sociales
Tags: asesinato, Chocó, lider indigena, lideres sociales
Slug: es-asesinado-el-mayor-indigena-miguel-tapi-en-choco
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/12/Captura-de-Pantalla-2020-12-04-a-las-2.57.25-p.-m..png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

Este viernes **4 de de diciembre se registró el asesinado del mayor indígena, Miguel Tapi en el municipio de Bahía Solano, Chocó**. Con él son 278 los líderes, lideresas y defensores de Derechos Humanos, territoriales y ambientales asesinados en el 2020.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/ONIC_Colombia/status/1334831883616710657","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/ONIC\_Colombia/status/1334831883616710657

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

Las Autoridades indígenas de la Mesa de Diálogo y Concertación de los Pueblos Indígenas del Chocó, denunciaron este viernes **el asesinato del reconocido líder de la comunidad Embera Dovida Miguel Tapi Rito** de 60 años.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El mayor indígena **había sido gobernador de las Comunidades Indígenas El Brazo y Bacuru Purru del municipio de Bahía Solano,** además según la Autoridad este no tenia denuncias de amenazas en su contra. ([Líder juvenil y ambiental asesinado en Quibdó, Choco](https://archivo.contagioradio.com/lider-juvenil-y-ambiental-asesinado-en-quibdo-choco/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El secuestro y posterior asesinato del líder social se perpetró el pasado 3 de diciembre sobre las 8:30 de la mañana, según las Autoridades Indigenas Miguel Tapi Rito, se encontraba en su vivienda de la Comunidad de El Brazo, realizando sus actividades cotidianas, allí llegaron ***"hombres armados encapuchados que se identificaron como miembros de la AGC, mediaron algunas palabras y le dijeron que les acompañara al río"* posterior a ello desapareció.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Integrantes de la familia del líder alertaron sobre este hecho, y empezaron un proceso de búsqueda algunas horas después al ver que este no retornara a su hogar, ***"la búsqueda culminó hacía las 11:30 de la mañana, cuando encontraron su cuerpo decapitado***".

<!-- /wp:paragraph -->

<!-- wp:quote -->

> "***RECHAZAMOS** este vil acto que acabó con la vida de un hermano quien dedicó su caminar a liderar su resguardo en este municipio ubicado en la costa del pacifico chocoano*"
>
> <cite>Mesa de diálogo y concertación de los pueblos indígenas del Chocó </cite>

<!-- /wp:quote -->

<!-- wp:paragraph -->

Desde la [Mesa de diálogo y concertación de los pueblos indígenas del Chocó](https://www.cric-colombia.org/portal/mesa-de-dialogo-y-concertacion-de-los-pueblos-indigenas-del-choco-denuncia-y-rechaza-el-asesinato-del-mayor-embera-dobida-miguel-tapi-rito/), indicaron, *"**una vez más los actores armados desangran el territorio y desarmonizan la vida de las Comunidades**"*, y lamentaron el silencio y el dolor que se apodera de sus territorios, ***"hoy nos toca sepultar a los nuestros sin tener alguna respuesta contundente del estado colombiano".***

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Además como movimiento indígena del Chocó hicieron un llamado al Gobierno nacional para que esta serie de eventos en contra de los Derechos de los Pueblos Indígenas *"y sin lugar a duda la inminente violación a los Derechos Humanos"* se detenga.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Al mismo tiempo hicieron un llamado para que sean claras las acciones en defensa de las comunidades ancestrales, **exigiendo la implementación de *"estrategias contundentes y reales para la protección de la vida en los resguardos y comunidades indígenas del chocó".***

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

De igual forma, **exigieron a los grupos armados *"respetar el derecho a la vida en nuestras comunidades"*, así como el retiro de los grupos armados de sus territorios**, en una acción de *"respeto a nuestra Autonomía y Gobernabilidad".*

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
