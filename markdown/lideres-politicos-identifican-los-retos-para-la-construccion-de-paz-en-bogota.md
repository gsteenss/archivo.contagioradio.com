Title: Líderes políticos identifican los retos para la construcción de paz en Bogotá
Date: 2016-08-04 12:55
Category: Otra Mirada
Tags: Enrique Peñalosa, FARC, paz, proceso de paz, Viva la ciudadanía
Slug: lideres-politicos-identifican-los-retos-para-la-construccion-de-paz-en-bogota
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/06/carmela-maria.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Carmela María 

###### [4 Ago 2106] 

Este jueves desde las 2 de la tarde en el Salón Luis Carlos Galán del Capitolio Nacional, Ediles, concejales y congresistas se reunirán para intercambiar perspectivas sobre cuáles deben ser los **desafíos y acciones para avanzar en la construcción de paz en la capital de Colombia,** en el marco del proceso de paz entre la guerrilla de las FARC y el gobierno nacional que se encuentra en su etapa final.

En un primer momento del evento intervendrá el Director de la Corporación Viva la Ciudadanía Antonio Madariaga; Diego Bautista Asesor de Paz Territorial de la Oficina Alto Comisionado de Paz y el representante Alirio Uribe del Partido Polo Democrático Alternativo.

Además, senadores y representantes del Congreso de la República  entre ellos Clara Rojas del Partido Liberal, Ángela María Robledo del Partido Alianza Verde, Rodrigo Lara de Cambio Radical, Fernando Tamayo del Partido Conservador, y Gloria Stella Díaz concejal de Movimiento Político MIRA, evidenciarán desde sus perspectivas cómo se puede construir la paz desde Bogotá, para que la ciudad sea ejemplo para todo el país.

Próximamente, **desde la Corporación Viva La Ciudadanía se espera que exista un espacio con la administración Enrique Peñalosa,** teniendo en cuenta el bajo presupuesto que se evidencia en el Plan de Desarrollo Distrital lo que demuestra que se le da poca importancia y no es una prioridad la construcción de paz y a la atención a las víctimas, pues **aunque el PDD tiene un pilar que incluye elementos sobre víctimas y reconciliación, apenas se trata de 2.8% del presupuesto total.**

<iframe src="http://co.ivoox.com/es/player_ej_12441107_2_1.html?data=kpehlpaVdJihhpywj5WbaZS1kpuah5yncZOhhpywj5WRaZi3jpWah5yncaLp08aYtNTItoa3lIqupszZqduZk6iYuM7apYzgwpCwy9rIpcXVz4qwlYqliMKhhpywj6jTstXVyM7cjbfFqMrjjJKSmaiReA..&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
