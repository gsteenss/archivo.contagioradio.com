Title: "Queremos saber quién ordenó los Falsos positivos" Madres de Soacha
Date: 2018-07-24 12:30
Category: DDHH, Paz
Tags: ejercito, falsos positivos, Jurisdicción Espacial de Paz, madres de soacha
Slug: queremos-saber-quien-ordeno-los-mal-llamados-falsos-positivos-madres-de-suacha
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/07/falsos-positivos.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [24 Jul 2018] 

La primera audiencia del caso 003 de la Jurisdicción Especial para la Paz sobre “Muertes ilegítimamente presentadas como bajas en combate por agentes del Estado” se llevará a **cabo el próximo 8 de agosto e iniciará con 5 procesos,** por lo cual el Colectivo de las Madres de Soacha está preparando un informe que presentará ante la JEP y participarán en la Comisión de la Verdad.

Estos 5 procesos son el caso de Jaider Andrés Palacio, Diego Alberto Tamayo, Julio César Vargas, Jhonatan Orlando Soto Bermúdez y Víctor Fernando Gomez, hermano de Jhon Nilson Gómez, asesinado 6 meses después de iniciar la búsqueda de su familiar.

Jackeline Castillo, hermana de Jaime Castillo, víctima de ejecución extrajudicial en el año 2008, asegura que con este capítulo que se abre esperan encontrar la verdad frente a la cadena de mando responsable de la política de los mal llamados falsos positivos, **“necesitamos saber quién realmente fue el que ordenó cometer estos crímenes”** y agrega que las Madres de Soacha ya no aguardan la esperanza de  una condena ejemplarizante, pero si de saber la verdad.

En ese sentido, Castillo afirmó que espera que el acogimiento a la JEP del General Montoya, investigado por ejecuciones extrajudiciales, logre esclarecer “si realmente eso fue ordenado por el ministro de defensa, **en su momento Juan Manuel Santos o por el presidente, en su momento Álvaro Uribe**”.

### **La justicia ordinaria y las deudas con las ejecuciones extrajudiciales** 

Castillo aseguró que durante estos 10 años los familiares de las víctimas de ejecuciones extrajudiciales han tenido que padecer maltratos, amenazas, intimidaciones y la lentitud de la Justicia ordinaria, “han dicho que esto es una patraña de nosotros para lograr reclamar jugosas indemnizaciones del Estado, … **ha existido falta de respeto a las víctimas, no hemos tenido respaldo del Estado, estamos en desamparo total**”.

Se estima que en Colombia habrían más de **3.000 casos de ejecuciones extrajudiciales**, sin embargo, organizaciones defensoras de derechos humanos afirman que la cifra podría ser mucho más grande debido a que personas que fueron reportadas como desaparecidas también podrían haber sido víctimas de este crimen.

La última condena proferida por un Juzgado ante estos hechos se dio el pasado 3 de abril de 2017, contra 21 integrantes del Ejército por la desaparición, concierto para delinquir y homicidio agravado de los 5 jóvenes. (Le puede interesar:["Falsos positivos son crímenes de Lesa humanidad"](https://archivo.contagioradio.com/falsos-positivos-son-crimenes-de-lesa-humanidad/))

<iframe id="audio_27216893" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_27216893_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
