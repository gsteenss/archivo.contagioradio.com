Title: Fumigaciones no deberían ser el tema de la fiscalía sino mecanismos de combate al narcotráfico
Date: 2016-09-05 12:48
Category: Movilización, Nacional
Tags: Cultivos de uso ilícito, Fiscalía general, Glifosato
Slug: fumigaciones-no-deberian-ser-el-tema-de-la-fiscalia-sino-mecanismos-de-combate-al-narcotrafico
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/09/fiscal-e1473096991642.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: [360radio.com.co]

###### [05 Sep 2016] 

A pesar de la carta del Fiscal Martínez al Consejo Nacional de Estupefacientes en que solicita que se reanuden las fumigaciones con **glifosato, las cifras han demostrado que no son la herramienta eficaz para combatir el problema de las drogas en Colombia**, por el contrario son una estrategia fallida para enfrentar el problema, señala Ricardo Vargas, experto en el análisis y el planteamiento de propuestas para el narcotráfico.

Según Vargas, no solamente es el problema de la sustitución de la coca por otros cultivos, sino que tiene que darse una **solución integral que enfrente el problema** desde la raíz ligado con un negocio que ha impregnado varias de las capas de la sociedad colombiana, pasando por los campesinos y campesinas hasta el mercado bursátil.

Para Ricardo Vargas, la Fiscalía está proponiendo una premisa que está demostrado, no es la acertada. En cambio, **el ente acusador debería estar enfocado en las estrategias de combate al narcotráfico y a la corrupción,** más no encaminar sus acciones hacia la reanudación de las fumigaciones.

Adicionalmente el experto indica que es lastimosa la intervención del fiscal Néstor Humberto Martínez porque está desviando la labor de la fiscalía frente a ese problema, pues el ente "se están demorando hasta 6 meses para encontrar alguna responsabilidad de las estructuras del crimen organizado, es decir, que hay una deficiencia total en la labor de la Fiscalía".

Por su parte, **los ministros de Ambiente y salud se oponen a la reactivación de este método** pues aseguran que las fumigaciones no son la solución para la erradicación de estos cultivos, y en cambio generan graves afectaciones a la salud humana y a los ecosistemas.

Cabe resaltar que el acuerdo de paz firmado entre el gobierno de Colombia y las FARC señala que el narcotráfico es un problema estructural que no debe ser atacado con las mismas políticas demostradas como fallidas, sino que debe enfocarse y articularse con un desarrollo agrario integral que busque soluciones reales a los problemas económicos en el agro colombiano y que no ataque al campesino como un delincuente.

En Colombia y en el mundo ya se ha realizado **una evaluación acerca que las políticas de combate a las drogas que ha sido calificada como fracasada.** Además se ha podido confirmar la intervención de altos funcionarios del gobierno en otros periodos como algunos generales señalados por la propia DEA lo que demuestra la vinculación de amplios sectores de la sociedad con el negocio del narcotráfico.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
