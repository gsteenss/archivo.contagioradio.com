Title: Colombia se pinta de esperanza en la Semana por la Paz
Date: 2018-08-17 12:59
Category: DDHH, Paz
Tags: Conferencia Episcopal de Colmbia, Esperanza, paz, semana por la paz, vida
Slug: colombia-se-pinta-semana-por-la-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/08/Coloreando-esperanza-y-la-paz-Semana-por-la-Paz-2018.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Conferencia Episcopal de Colombia] 

###### [16 Ago 2018] 

Con el lema **"Hay vida, hay esperanza. Sigamos colorenado la paz" del 2 al 9 de septiembre** se realizará la 31 Semana por la paz en Colombia. Luis Emil Sanábria, presidente de **REDEPAZ**, una de las organizadoras, aseguró que hasta que no "logremos un país reconciliado, donde gocemos todos de los derechos humanos la Semana por la paz seguirá teniendo lugar".

La presentación del evento tuvo lugar el 16 de agosto en la Conferencia Episcopal de Colombia, y allí, Sanábria pidió recordar el Derecho Internacional Humanitario y abogar por una salida negociada al conflicto. Mientras que los **Monseñores Elkin Álvarez y Héctor Henao** pidieron tanto al Gobierno Nacional como al ELN avanzar en las negociaciones de paz.

Henao afirmó que la paz "es un espacio y tiempo que nos pertenecen a todos y todas", y por esa razón la ciudadanía debe involucrarse en actividades donde se promueva. Por su parte, **Alejandra Llanos, de la Organización Nacional Indígena de Colombia (ONIC)**, afirmó que el evento, es una buena oportunidad para mantener viva la esperanza.

### **¿Cuáles son los retos de la Paz?** 

Llanos, también señaló que desde la Semana por la Paz se habla de los retos que se avecinan para Colombia, entre los cuales están la implementación del enfoque étnico de los acuerdos, la protección a líderes sociales, la sustitución de cultivos de uso ilícito, y el aumento en la capacidad del Estado para llegar a los territorios y así proteger la vida.

De otra perspectiva, el Magistrado de la Jurisdicción Especial de Paz (JEP), **Carlos Vidal,** afirmó que las instituciones creadas en el marco del Sistema de Verdad, Justicia, Reparación y No Repetición (SVJRNR), son aliados claves para que se logre la implementación de los acuerdos y afirmó que la **"JEP es uno de esos lugares abiertos a que la sociedad los coloree".**

Durante el lanzamiento, se manifestó un reclamo general al Gobierno Iván Duque para que se reconozca a las organizaciones y movimientos sociales que participan en la Semana por la Paz, como interlocutores válidos, y escenarios apropiados para la defensa de la vida y de la paz. (Le puede interesar: ["Reconciliación será el centro en la Semana por la Paz"](https://archivo.contagioradio.com/semana-por-la-paz/))

<iframe id="audio_27904204" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_27904204_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en] [su correo](http://bit.ly/1nvAO4u) [o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por] [Contagio Radio](http://bit.ly/1ICYhVU). 
