Title: Enfoque de género, palabras de la discordia en Acuerdos de Paz
Date: 2016-10-31 14:15
Category: Nacional, Paz
Tags: acuerdos de paz, enfoque de género, FARC, ideologia de genero, Iglesias Cristianas, La Habana, Pastores, paz
Slug: enfoque-de-genero-discordia-en-acuerdos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/10/pastores-e1477940709897.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: El País] 

###### [31 Oct de 2016] 

En las últimas horas se conocieron los **resultados de la reunión que sostuvieron algunos sectores de la iglesia cristiana y la delegación de paz de las Farc**. Según lo aseguraron los asistentes, en dicho encuentro se vio a esta guerrilla muy dispuesta a escuchar las propuestas de este sector de la sociedad.

De acuerdo a lo divulgado por las partes, dentro de **lo propuesto por las iglesias deberá ser negociado o cambiado el tema de género, por el término “igualdad de derechos para hombres y para mujeres”**. Así mismo, insistieron en el tema de revisar el concepto de familia, el respeto a la libertad religiosa y el reconocimiento de las víctimas cristianas.

Luego del anuncio, Sandra Mazo Directora de Católicas por el Derecho a Decidir aseguró que ven con mucha preocupación este tipo de diálogos que están sosteniendo algunas iglesias “porque **no son las religiones, las iglesias las que tienen que hacer acuerdos frente a una agenda de derechos de las mujeres, de las personas LGBTI, porque son derechos que ya hemos logrado en la constitución”**.

Así mismo desde este sector de católicas, aseguraron que otro de los temas preocupantes es que se esté poniendo la moral sexual como un tema de negociación entre las Farc y el estado “eso es una injerencia indebida de la religión, ese no es el lugar ni el papel que deben asumir las iglesias” afirmó Mazo. Le puede interesar: [Enfoque de género no se negocia](https://archivo.contagioradio.com/enfoque-de-genero-no-se-negocia/)

La Directora de Católicas por el Derecho a Decidir, hizo especial énfasis en decir que no se oponen al diálogo. Para la funcionaria, lo que se debe hacer **es velar porque se discuta lo que está en el acuerdo de paz y no temas de la agenda pendiente del estado** y agregó que “detrás de estas conversaciones lo que hay es prejuicios, una homofobia exacerbada, que se quiere poner en los acuerdos para polarizar al país. Esto le está haciendo mucho daño a la nación”.

Por su parte, otro de los temas que ha suscitado esta reunión es que detrás puede haber algún tipo de juego político, para Mazo “**es claro que no hay que desconocer que están en juego las próximas elecciones, una agenda y una campaña política”** y mencionó a personalidades como la senadora Viviane Morales, quien según Mazo ha sonado como posible candidata a la vicepresidencia.

**Invitación a dialogar sobre el enfoque de género.**

La Directora de Católicas por el Derecho a Decidir, Sandra Mazo, aseveró que la invitación a hacer un diálogo nacional está hecha desde diversos sectores religiosos **“a la señora Viviane (Morales) y las demás iglesias cristianas les decimos, dialoguemos, porque somos muchas iglesias las que hay en Colombia,** pero dialoguemos en otro ámbito y es en el de la democracia y el estado social de derecho, no en el espacio de un acuerdo entre las Farc y el estado”.

**Comunidades de fe exigen respeto al enfoque de género**

A través de un comunicado, la **Mesa Ecuménica por la Paz – Por una  Paz con Ética- manifestaron su descontento frente a lo acontecido con algunas iglesias cristianas y el enfoque de género pactado en La Habana.**

En la misiva, que tiene 5 puntos, ponen de manifiesto que rechazan los pronunciamientos lgtbifóbicos de varias iglesias “cristianas”, partidos políticos y militares y reconocen en las personas diversas por orientación sexual e identidades de género (LGTBI) la sensibilidad social, la solidaridad y el compromiso en todo el territorio colombiano como actores claves en la construcción de paz y la transformación no violenta de los conflictos.

Así mismo, recalcaron que continuarán respaldando los Acuerdos a los que llegaron el Gobierno Colombiano y la Guerrilla de las FARC-EP, asegurando que encuentran en estos la posibilidad que las personas diversas por orientación sexual e identidades de género (LGTBI) puedan vivir plenamente su ser de ciudadanos y ciudadanas como personas amadas por el Señor de la Vida. Le puede interesar: [Acuerdos deben implementarse con enfoque de género y territorial](https://archivo.contagioradio.com/acuerdos-deben-implementarse-con-enfoque-de-genero-y-territorial/)
