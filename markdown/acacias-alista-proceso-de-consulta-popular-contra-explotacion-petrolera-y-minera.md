Title: En 15 días arrancaría recolección de firmas para la consulta popular en Acacías
Date: 2017-07-14 16:32
Category: Ambiente, Nacional
Tags: Acacías, consulta popular, Explotación petrolera, Meta, Mineria
Slug: acacias-alista-proceso-de-consulta-popular-contra-explotacion-petrolera-y-minera
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/07/acaciasd-e1500055292492.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Región 365.com] 

###### [14 Jul 2017] 

Los habitantes de Acacías en el Meta, manifestaron que debido a explotación petrolera y minera en su territorio, han habido **afectaciones a los recursos naturales, los bienes han aumentado de precio y las regalías no tienen una retribución en la inversión social.**

Por estas razones y la serie de solicitudes de minería, construcción de hidroeléctricas y otras actividades extractivas, los habitantes de Acacías han conformado un comité el cual recibirá respuesta en 15 días de la Registraduría Nacional para avalar el proceso de recolección de firmas y así dar inicio al proceso de realización de la consulta popular.

Según Gloria Zambrano, integrante de la Mesa Hídrica y de Comité por la Consulta Popular en Acacías, “las comunidades ya estamos cansadas y **nos hemos dado cuenta de las afectaciones que ha traído la explotación petrolera** **y minera”**. por ejemplo, manifestó que “la vereda La esmeralda tiene 66 aljibes de agua completamente contaminados y Ecopetrol tiene que llevar el agua en carro tanques cada 4 días”. (Le puede interesar: ["Seis termoeléctricas de carbón afectarían 5 ecosistemas en Boyacá"](https://archivo.contagioradio.com/habitantes-de-boavita-en-boyaca-preocupados-por-proyecto-minero-extractivo/))

Frente al alza del costo de la vida en el municipio, Zambrano afirmó que “debido a estas exploraciones petroleras, el municipio ha pasado a ser considerado como una zona industrial y petrolera. Esto ha hecho que **un almuerzo que costaba 2 mil hoy cueste más de 10 mil** o que los arriendos hayan subido de 200 mil a 500 mil”.

**Empresa construirá hidroeléctrica que afectaría caudales de ríos**

Según lo manifestado por Zambrano, “la empresa de Energía del Pacífico EPSA, **está planeando hacer una hidroeléctrica que va a desviar el caudal del río Guayuriba**”. Ella explicó que la empresa tendría que realizar túneles utilizando dinamitas para desviar el agua del río y llevarla hasta unas turbinas que alimentarían la hidroeléctrica. “Este proceso termina por matar el río y la vegetación dejando a las veredas sin agua”. (Le puede interesar:["Sibaté alista consulta popular contra la minería")](https://archivo.contagioradio.com/sibate_consulta_popular_mineria/)

Adicional a la hidroeléctrica, Zambrano aseguró que “van a poner unas torres de energía para la planta petrolera de Ecopetrol y les han dicho a las comunidades que les van a poner luz con estas torres. Lo cierto es que les van a dar unos paneles solares que solo sirven para que funcione la licuadora”. Ella afirmó que “**Ecopetrol es una empresa que solo dice mentiras** y han contaminado los pastizales de las veredas que se encargaban de darnos leche”.

Debido a todas las afectaciones que han tenido las comunidades de Acacías, **ellos y ellas están adelantando todos los procesos para realizar la consulta popular**. Han manifestado que la pregunta para los acacireños será: “¿Está usted de acuerdo en que en Acacias haya más explotación minera y petrolera? Si o no”.

<iframe id="audio_19807783" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_19807783_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU).
