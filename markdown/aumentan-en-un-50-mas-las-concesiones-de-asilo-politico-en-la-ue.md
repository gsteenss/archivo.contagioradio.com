Title: Aumentan en un 50% las concesiones de asilo político en la UE
Date: 2015-05-12 16:56
Author: CtgAdm
Category: DDHH, El mundo
Tags: 50% más que en 2014 de solicitudes de refugio en UE, Aumenta en un 50% numero de refugiados europa, Emergencia humanitaria inmigrantes en Mediterráneo, Se dispara solicitudes de refugio político en UE
Slug: aumentan-en-un-50-mas-las-concesiones-de-asilo-politico-en-la-ue
Status: published

###### Foto:Telemundo.com 

En **2014 aumentaron en un 50% las concesiones de asilo político en la UE** respecto a 2013 debido a los conflictos armados en Oriente Próximo y África. Según Eurostat, Agencia Estadística para la Comunidad Europea **desde 2008, 780.000 personas han recibido asilo político** en países de la UE.

La **mitad de ellos son Afganos, Sirios y Eritreos**, en total el 37% son Sirios y los Eritreos y Afganos cifran un 8% respectivamente.

Según cifras de Frontex, la Agencia Comunitaria para el control de las Fronteras, de todos los que solicitan el **asilo político solo el 45% lo logran en la primera instancia y el 18% en la segunda instancia.**

Mapa Concesiones de Asilo Político en 2014 en la UE:

\[caption id="attachment\_8534" align="aligncenter" width="980"\][![Elpais.com](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/05/1431428672_851768_1431450549_sumario_grande.png){.size-full .wp-image-8534 width="980" height="754"}](https://archivo.contagioradio.com/aumentan-en-un-50-mas-las-concesiones-de-asilo-politico-en-la-ue/1431428672_851768_1431450549_sumario_grande/) Elpais.com\[/caption\]

Pero no todos consiguen el **estatus de refugiado político permanente, solo el 56% de ellos lo logra.**

La principal problemática es que **no existen criterios unificados para valorar la situación de un posible solicitante de asilo político** entre países miembros, esto provoca que hayan países que acojan más que otros.

Por ejemplo entre **Alemania, Francia, Italia y Suecia en 2014 acogieron a dos tercios** de todos los solicitantes de asilo en Europa.

Diferencias como las de Bulgaria con el 94% de reconocimiento o de Suecia con el 77%, en comparación con Croacia, el 9% o Hungría con el 14%.

La **UE ha recomendado fijar unos criterios homogéneos** para la aceptación y reconocimiento del asilo político y en últimas del estatus de refugiado político permanente.

Además esta semana se va a presentar un documento para **poder equilibrar las cifras de refugiados** según el PIB y la demografía de cada país. De tal forma se distribuirían según las posibilidades de acogimiento de los distintos países miembros.

Para las organizaciones sociales de defensa de los derechos de las personas solicitantes de asilo político estas cifras **demuestran que la gran mayoría de inmigrantes ilegales que llegan están en condiciones de persecución política** y por tanto son futuros solicitantes del Refugio político.
