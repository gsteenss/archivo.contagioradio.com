Title: Pichima Quebrada, dos veces desterrada por la guerra
Date: 2019-07-03 12:13
Author: CtgAdm
Category: DDHH, Especiales Contagio Radio
Tags: Desplazamiento, Pichimá, pichima quebrada, Río San Juan
Slug: pichima-dos-veces-desterrados-por-la-guerra
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/rio.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/union-balsalito.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/union-balsalito-1.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/docordo.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/estamos-en-peligro.mp3" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/desplazamiento.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/defensa-civil.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/desplazamiento-2.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/desplazamiento-2-1.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/desplazamiento-4.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/la-cabaña.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/COCINA.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/pisos.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/desplazados.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/en-casa.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/viendo-tv.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/cartel.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/reunion.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/incumplimientos.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/entablado.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/Alcalde-Wilinton-Ibargüen.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/litoral.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/litoral-casa.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/agua.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/foto.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/luis-galindo.png" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/pichima-dos-veces-desterrados-por-la-guerra.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/dormir.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/GABR8886_2.gif" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/GABR8886_3.gif" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/GABR8886_5.gif" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/mujer.mp4" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/cocina-fin.gif" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/cocinnas.gif" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/GABR8886_6.gif" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/GABR8886_7.gif" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/Pichima-Quebrada.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/Pichima-Quebrada-1.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/especial-pichima-quebrada.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/GABR8886_8.gif" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/ninos-jugando.gif" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/PICHIMA-QUEBRADA.png" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

**PICHIMA QUEBRADA, DOS VECES DESTERRADA POR LA GUERRA **

 La comunidad indígena de Pichima Quebrada completa 30 días en desplazamiento.

03 de julio de 2019

[Tweet](https://twitter.com/share?ref_src=twsrc%5Etfw)  
<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/share_button.php?href=https%3A%2F%2Fwww.contagioradio.com%2Fpichima-dos-veces-desterrados-por-la-guerra%2F&amp;layout=button_count&amp;size=small&amp;appId=894195857389402&amp;width=91&amp;height=20" width="91" height="20" frameborder="0" scrolling="no"></iframe>

<figure>
![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/luis-galindo.png){width="70" height="70"}

</figure>
###### Luis Gabriel Galindo

Fotógrafo/Contagio Radio

**Por segunda vez la comunidad indígena de Pichima Quebrada ha tenido que salir de su territorio huyendo de la guerra, más de 90 familias completan 30 días desplazadas.**

#### "A las 12 de la tarde empezaron los primeros botes a salir, en ellos solo salieron las familias, todo lo dejamos "

![union-balsalito](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/union-balsalito-1.jpg "union-balsalito")

**Pichima Quebrada,** es uno de los resguardos indígenas del pueblo Wounaan que han habitado el Litoral de San Juan por más de 50 años, su territorio abarca una extensión de 9.024 hectáreas, tierras  que durante las últimas décadas han sido apetecidas por los grupos armados.

Es un sitio estratégico ubicado a las orillas del río San Juan, afluente que durante décadas ha estado en disputa por ser un corredor estratégico de los grupos armados que por allí transitan; primero fueron las FARC, luego los grupos paramilitares, ELN y ahora pasados tres años del Acuerdo de Paz han atestiguado la aparición de las disidencias de las FARC

Desplazamiento
--------------

![desplazamiento 2](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/desplazamiento-2-1.jpg "desplazamiento 2")

**"¡Estamos en peligro, por Dios, publiquen esta información!" **

<audio src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/estamos-en-peligro.mp3" preload="none" controls="controls"></audio>

Este fue el primer mensaje que llegó a Contagio Radio el 2 de junio hacia las 2:40 p.m, en él, un hombre de la comunidad pide que se comunique urgente la situación para evitar una tragedia, de fondo suenan los disparos de los grupos armados.

Al día siguiente de este enfrentamiento entre disidencias de las FARC y ELN, 95 familias y un total de 417 personas tuvieron que abandonar el territorio e irse al municipio de **Santa Genoveva de Docordó.** Salieron únicamente con su ropa hacia la cabecera municipal del Litoral del San Juan, ubicada en el extremo sur del departamento del Chocó y habitada en gran medida por población afro.

No es la primera vez que la **comunidad Wounaan** ha sido víctima de desplazamiento forzoso, el 10 de abril de 2016 tuvieron que abandonar  sus casas por enfrentamientos entre ELN  y Fuerza Pública, ese primer destierro duró nueve meses durante los que Santa Genoveva de  Docordó se convirtió en su refugio.

![Comunidad de Pichima, desplazada en la cabecera municipal de Docordo, Chocó](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/la-cabaña.jpg "Desplazamiento Pichima")  
[Tweet](https://twitter.com/share?ref_src=twsrc%5Etfw)  
<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/share_button.php?href=https%3A%2F%2Fwww.contagioradio.com%2Fpichima-dos-veces-desterrados-por-la-guerra%2F&amp;layout=button_count&amp;size=small&amp;appId=894195857389402&amp;width=91&amp;height=20" width="91" height="20" frameborder="0" scrolling="no"></iframe>

**"Uno aquí no es nada, como si uno no fuera colombiano"**

Son tres lugares que la Alcaldía del municipio les ha dado para refugiarse a las familias afectadas. El primero de estos lugares es La Cabaña; una casa maltrecha en la que se entra el agua cuando llueve y donde hay que tener cuidado para caminar debido a la mala situación en la que se encuentran los pisos.

Las cocinas donde las mujeres se turnan para preparar los alimentos están afuera de La Cabaña y  están rodeadas de plásticos negros que hacen las veces de techo y paredes, algunas mujeres llevaron sus cilindros y estufas y otras consiguen leña para improvisar un fogón.

El suelo esta lleno de barro debido a las constantes lluvias, sin embargo las mujeres descalzas o en chanclas,  le restan importancia y preparan sus alimentos: arroz y algunas arepas de harina de trigo cocinan este día.

![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/GABR8886_7.gif){width="865" height="486"}  
![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/COCINA-1024x683.jpg){width="1024" height="683" sizes="(max-width: 1024px) 100vw, 1024px" srcset="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/COCINA.jpg?resize=1024%2C683&ssl=1 1024w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/COCINA.jpg?resize=300%2C200&ssl=1 300w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/COCINA.jpg?resize=768%2C512&ssl=1 768w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/COCINA.jpg?resize=370%2C247&ssl=1 370w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/COCINA.jpg?w=1200&ssl=1 1200w"}  
![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/COCINAS--1024x683.jpg){width="1024" height="683" sizes="(max-width: 1024px) 100vw, 1024px" srcset="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/COCINAS-.jpg?resize=1024%2C683&ssl=1 1024w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/COCINAS-.jpg?resize=300%2C200&ssl=1 300w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/COCINAS-.jpg?resize=768%2C512&ssl=1 768w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/COCINAS-.jpg?resize=370%2C247&ssl=1 370w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/COCINAS-.jpg?w=1200&ssl=1 1200w"}

La Alcaldía en cabeza de Willington Ibargüen, se comprometió a instalar cocinas y baños en este lugar, sin embargo, lo único que hay al cumplirse 30 días de desplazamiento es un par de inodoros de 4 que se habían acordado,  a las que no llega el  agua, y un entablado, sin paredes y con techos donde algunas mujeres colocan troncos para hacer una estufa.

La comunidad destina como lugar de descanso aquel donde no les llegue el agua cuando duermen, algunas personas se recuestan en hamacas y otras tienden colchonetas.

![Comunidad de Pichima, desplazada en la cabecera municipal de Docordo, Chocó](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/dormir.jpg "Desplazamiento Pichima")

El segundo lugar, es una casa esquinera que tiene un letrero de madera que dice 'Defensa Civil Colombiana', abajo de él un pequeño eslogan "Listos en paz o en emergencia", sin embargo, el espacio donde ahora conviven 12 familias dista de ser un lugar para ser habitado dignamente en esta emergencia. Las personas están repartidas en dos pisos, seis familias en cada uno.

Tienen dos cocinas, una al interior donde las mujeres han puesto tres estufas; mientras la otra se encuentra afuera bajo un plástico negro. Una de las mujeres mayores señala las columnas de la cocina, advirtiendo sobre el peligro que los rodea debido a que las columnas se encuentran en malas condiciones y esto podría ocasionar un colapso por el peso de las familias que habitan el segundo piso.

La casa consta de luz, pero carece de agua potable o baños, un inconveniente para las familias, en mayor medida para las mujeres porque se sienten observadas cuando van a hacer sus necesidades al río.

Es precisamente en el río donde algunos niños y niñas  se divierten, van a la orilla para bañarse, mientras otros se quedan con sus familias viendo algo en la televisión, otros solamente salen al pasillo o se sientan en la escalera de la casa observando la extraña cotidianidad que ahora viven.

![Comunidad de Pichima, desplazada en la cabecera municipal de Docordo, Chocó](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/defensa-civil.jpg "Desplazamiento Pichima")

<figure>
[![en casa](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/en-casa.jpg)](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/en-casa.jpg)

</figure>
<figure>
[![ninos jugando](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/ninos-jugando.gif)](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/ninos-jugando.gif)

</figure>
<figure>
[![Comunidad de Pichima, desplazada en la cabecera municipal de Docordo, Chocó](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/viendo-tv.jpg)](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/viendo-tv.jpg)

</figure>
<figure>
[![Comunidad de Pichima, desplazada en la cabecera municipal de Docordo, Chocó](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/agua.jpg)](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/agua.jpg)

</figure>
<figure>
[![Comunidad de Pichima, desplazada en la cabecera municipal de Docordo, Chocó](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/desplazados.jpg)](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/desplazados.jpg)

</figure>
**"La convivencia con la comunidad afro es difícil en algunas ocasiones y nosotros lo entendemos porque estamos ocupando su espacio, pero a nosotros no nos gusta estar acá. Nosotros ya nos quisiéramos ir" **

El tercer lugar donde están ubicados es la casa de paso de la comunidad, allí hay siete familias en un solo piso, no hay ningún cuarto, solo un espacio mediano y una cocina improvisada que han hecho con troncos y unas piedras.

Una mujer anciana permanece la mayor parte del tiempo sentada sobre el suelo de madera debido a su avanzada edad  mientras otra de las mujeres dice que vivir en este lugar es muy duro, que cuando cocinan todo el humo se mete a la casa y no pueden respirar bien.

"Los vecinos están bravos porque pisamos su terreno" dice una de las mujeres quien señala que las personas de al lado de la casa se disgustan si la comunidad pone un pie en su terreno. También menciona las dificultades que presentan al alimentarse; "solo tenemos arroz y a algunas familias ya se les acabó", mientras en contadas oportunidades obtienen pescado que salen a buscar al río.

En algunas ocasiones consiguen trabajos en fincas de habitantes de Docordó, "nos pagan 12.000 o 15.000 pesos y a veces cuando no hay plata nos pagan con productos" explica una mujer quien afirma que le pagaron con una caneca de papa china, tubérculo que consumen en esta región de Colombia.

Las familias restantes  se encuentran en la comunidad indígena de Balsalito, ubicada al otro lado del río y otras en casas del municipio donde viven con sus familiares.

![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/Alcalde-Wilinton-Ibargüen-1024x683.jpg){width="1024" height="683" sizes="(max-width: 1024px) 100vw, 1024px" srcset="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/Alcalde-Wilinton-Ibargüen.jpg?resize=1024%2C683&ssl=1 1024w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/Alcalde-Wilinton-Ibargüen.jpg?resize=300%2C200&ssl=1 300w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/Alcalde-Wilinton-Ibargüen.jpg?resize=768%2C512&ssl=1 768w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/Alcalde-Wilinton-Ibargüen.jpg?resize=370%2C247&ssl=1 370w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/Alcalde-Wilinton-Ibargüen.jpg?w=1200&ssl=1 1200w"}  
![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/litoral-1024x683.jpg){width="1024" height="683" sizes="(max-width: 1024px) 100vw, 1024px" srcset="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/litoral.jpg?resize=1024%2C683&ssl=1 1024w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/litoral.jpg?resize=300%2C200&ssl=1 300w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/litoral.jpg?resize=768%2C512&ssl=1 768w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/litoral.jpg?resize=370%2C247&ssl=1 370w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/litoral.jpg?w=1200&ssl=1 1200w"}

La Alcaldía de Docordó queda a pocas cuadras de donde están las familias desplazadas, allí, **Willington Ibargüen**, mandatario local que estuvo suspendido durante algunos meses por una inhabilidad del Tribunal Administrativo del Chocó, argumenta que la Alcaldía ha estado pendiente de todo lo que necesitan las familias, explica que las casas no se habían reparado porque no se tenían los contratos firmados con la persona que iba abastecer la madera, pero que eso ya se había solucionado.

Igualmente indicó que se firmó un contrato para alimentación con el que se abastecería a las familias con pescado y comida según sus costumbres, sin embargo, al hablar con personas que se encuentran en los albergues dicen que en ningún momento han recibido pescado o otro tipo de alimento fuera de los mercados básicos que les ha brindado la Unidad de Víctimas.

Algunos integrantes de la comunidad argumentan que la administración de Ibargüen, no se ha comprometido con el bienestar de las familias desplazadas debido a que ellos no votaron en elecciones por el hoy alcalde.

El miedo
--------

![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/cartel-1024x683.jpg){width="1024" height="683" sizes="(max-width: 1024px) 100vw, 1024px" srcset="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/cartel.jpg?resize=1024%2C683&ssl=1 1024w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/cartel.jpg?resize=300%2C200&ssl=1 300w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/cartel.jpg?resize=768%2C512&ssl=1 768w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/cartel.jpg?resize=370%2C247&ssl=1 370w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/cartel.jpg?w=1200&ssl=1 1200w"}

Hablar de quienes fueron los grupos que los desplazaron es delicado para cualquier hombre o mujer de Pichima, ellos prefieren evadir estos temas y centrar las conversaciones en otros puntos;  con esto buscan guardar la seguridad de la comunidad y de ellos mismos ya que los grupos armados controlan el territorio; sin embargo, la sensación de miedo al hablar de estos temas se percibe en el ambiente.

Retornar
--------

![Comunidad de Pichima, desplazada en la cabecera municipal de Docordo, Chocó](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/incumplimientos.jpg "Desplazamiento Pichima")  
![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/litoral-casa-1024x683.jpg){width="1024" height="683" sizes="(max-width: 1024px) 100vw, 1024px" srcset="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/litoral-casa.jpg?resize=1024%2C683&ssl=1 1024w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/litoral-casa.jpg?resize=300%2C200&ssl=1 300w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/litoral-casa.jpg?resize=768%2C512&ssl=1 768w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/litoral-casa.jpg?resize=370%2C247&ssl=1 370w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/litoral-casa.jpg?w=1200&ssl=1 1200w"}

Las familias de **Pichima Quebrada**, quieren regresar lo más pronto a su territorio, lastimosamente, al hablar con los líderes, ese retorno no es posible en este momento debido a que los actores armados siguen en el territorio. En reuniones han planteado la posibilidad de hacer una misión de verificación al territorio en compañía de organizaciones e instituciones de Gobierno, no obstante, esto no tiene una fecha.

Este 3 de julio las familias cumplen un mes de vivir desplazadas, sus anhelos de regresar se reafirman cada día, pero el miedo a vivir nuevamente un enfrentamiento entre grupos armados los hace esperar, ellos y ellas quieren ir a sus casas pero con la garantía que los hechos que han vivido en dos ocasiones no se repitan.

![Comunidad de Pichima, desplazada en la cabecera municipal de Docordo, Chocó](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/reunion.jpg "Desplazamiento Pichima")

**"Antiguamente nosotros vivíamos muy felices en nuestra comunidad Pichima Quebrada con tranquilidad y con la naturaleza que nos rodea y queremos seguir viviendo en paz"**

[Tweet](https://twitter.com/share?ref_src=twsrc%5Etfw)  
<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/share_button.php?href=https%3A%2F%2Fwww.contagioradio.com%2Fpichima-dos-veces-desterrados-por-la-guerra%2F&amp;layout=button_count&amp;size=small&amp;appId=894195857389402&amp;width=91&amp;height=20" width="91" height="20" frameborder="0" scrolling="no"></iframe>  
[![Pichima Quebrada, dos veces desterrada por la guerra](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/PICHIMA-QUEBRADA-370x260.png){width="370" height="260"}](https://archivo.contagioradio.com/pichima-dos-veces-desterrados-por-la-guerra/)

#### [Pichima Quebrada, dos veces desterrada por la guerra](https://archivo.contagioradio.com/pichima-dos-veces-desterrados-por-la-guerra/)

PICHIMA QUEBRADA, DOS VECES DESTERRADA POR LA GUERRA   La comunidad indígena de Pichima Quebrada completa 30 días en…[Leer más](https://archivo.contagioradio.com/el-regreso-del-glifosato-una-amenaza-para-campesinos-en-zonas-cocaleras/)  
[![Con homicidio de Servio Cuasaluzán, son 134 excombatientes asesinados](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/02/Asesinato-lider-indigena-370x260.jpg){width="370" height="260"}](https://archivo.contagioradio.com/con-homicidio-de-servio-cuasaluzan-son-134-excombatientes-asesinados/)

#### [Con homicidio de Servio Cuasaluzán, son 134 excombatientes asesinados](https://archivo.contagioradio.com/con-homicidio-de-servio-cuasaluzan-son-134-excombatientes-asesinados/)

Este fin de semana, se conoció del asesinato del reincorporado Servio Delio Cuasaluzán Guanga, quien se encontraba en…[Leer más](https://archivo.contagioradio.com/con-homicidio-de-servio-cuasaluzan-son-134-excombatientes-asesinados/)  
[![Con asesinato de María del Pilar Hurtado, paramilitares siguen cumpliendo amenazas en Córdoba](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/María-del-Pilar-370x260.jpg){width="370" height="260"}](https://archivo.contagioradio.com/con-asesinato-de-maria-del-pilar-hurtado-paramilitares-siguen-cumpliendo-amenazas-en-cordoba/)

#### [Con asesinato de María del Pilar Hurtado, paramilitares siguen cumpliendo amenazas en Córdoba](https://archivo.contagioradio.com/con-asesinato-de-maria-del-pilar-hurtado-paramilitares-siguen-cumpliendo-amenazas-en-cordoba/)

María del Pilar fue una de las personas  declaradas como objetivo militar en un panfleto amenazante difundido por…[Leer más](https://archivo.contagioradio.com/con-asesinato-de-maria-del-pilar-hurtado-paramilitares-siguen-cumpliendo-amenazas-en-cordoba/)  
[![El regreso del glifosato, una amenaza para campesinos en zonas cocaleras](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/04/Glifosato-Putumayo-370x260.jpg){width="370" height="260"}](https://archivo.contagioradio.com/el-regreso-del-glifosato-una-amenaza-para-campesinos-en-zonas-cocaleras/)

#### [El regreso del glifosato, una amenaza para campesinos en zonas cocaleras](https://archivo.contagioradio.com/el-regreso-del-glifosato-una-amenaza-para-campesinos-en-zonas-cocaleras/)

Comunidades en zonas cocaleras rechazaron la reciente decisión del Gobierno de reanudar en julio el uso de glifosato…[Leer más](https://archivo.contagioradio.com/el-regreso-del-glifosato-una-amenaza-para-campesinos-en-zonas-cocaleras/)
