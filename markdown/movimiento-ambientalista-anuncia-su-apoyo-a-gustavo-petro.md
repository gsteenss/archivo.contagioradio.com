Title: Movimiento Ambientalista anuncia su apoyo a Gustavo Petro y Ángela Robledo
Date: 2018-05-25 12:56
Category: Ambiente, Política
Tags: Angela Maria Robledo, Gustavo Petro, Movimiento Ambientalista
Slug: movimiento-ambientalista-anuncia-su-apoyo-a-gustavo-petro
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/03/gustavo_petro.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Zona Cero] 

###### [25 May 2018]

A través de un comunicado de prensa,  defensores de derechos ambientales, que hacen parte del movimiento ambiental en Colombia, manifestaron su apoyo a la candidatura de Gustavo Petro y Ángela María Robledo. Para los ambientalistas el programa que ofrece la Colombia Humana es una **propuesta real y coherente con las necesidades del país para ser sostenible y proteger el ambiente**.

De acuerdo con Idebrandon Vélez, integrante de Agenda en Movimiento, los puntos en común que encuentran los ambientalistas para unirse a la campaña de Petro es que "**en lo fundamental busca construir la paz con la naturaleza**" y entre los seres humanos.

En ese mismo sentido, Vélez manifestó que en el programa de la Colombia Humana hay un tema central sobre la política de ambiente y la transformación de la matriz energética hacia una que permita dar cuenta de los problemas del cambio climático, y que progresivamente deje los combustibles fósiles y se provea de energías de fuentes renovables. De esta manera se democratizaría el acceso a las energías renovables.

Sumado a ello el uso de combustibles de energías renovables, según Veléz podría ayudar a que **"las ciudades se desintoxiquen"** Otra de las propuestas que para los ambientalistas es crucial, tiene que ver con la planificación y el funcionamiento del país a partir de la protección del patrimonio hídrico. (Le puede interesar: ["Defensores de DDHH del Tolima amenazados ante intención de voto por Gustavo petro"](https://archivo.contagioradio.com/defensores-de-ddhh-del-tolima-amenazados-ante-intencion-de-voto-por-gustavo-petro/))

### **Otros escenarios políticos para la paz ambiental**

Frente a las posibilidades de que la fórmula de Gustavo Petro y Ángela María Robledo no pase en primera vuelta Velés afirmó que el movimiento ambientalista ya ha definido que respaldarían una agenda que considerará y que tuviera **en el centro de sus apuestas los temas ambientales.**

"El ambientalismo no sería egoísta ni mezquino. Probablemente tendríamos que considerar apoyar esa agenda. **Pero en este momento la agenda que mejor nos representa y que mejor identifica las transofrmaciones necesarias en este país es la de Ángela María y Gustavo Petro" afirmó Veléz**.

[1527227749216\_Carta de Apoyo Ambientalista a Colombia Humana 2018 MAYO](https://www.scribd.com/document/380170543/1527227749216-Carta-de-Apoyo-Ambientalista-a-Colombia-Humana-2018-MAYO#from_embed "View 1527227749216_Carta de Apoyo Ambientalista a Colombia Humana 2018 MAYO on Scribd") by [Contagioradio](https://www.scribd.com/user/276652802/Contagioradio#from_embed "View Contagioradio's profile on Scribd") on Scribd

<iframe id="doc_88836" class="scribd_iframe_embed" title="1527227749216_Carta de Apoyo Ambientalista a Colombia Humana 2018 MAYO" src="https://www.scribd.com/embeds/380170543/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-6sA36U6eA3xJ5e1Gnzfd&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="0.7729220222793488"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)
