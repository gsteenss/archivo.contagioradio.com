Title: Sindicato de la UNP pide renuncia de Director tras asesinato de  escoltas en Chocó
Date: 2019-11-14 16:01
Author: CtgAdm
Category: DDHH, Nacional
Tags: Escoltas, Pablo González, Unidad Nacional de protección
Slug: sindicato-de-la-unp-pide-renuncia-de-director-tras-asesinato-de-escoltas-en-choco
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/WhatsApp-Image-2019-11-14-at-15.38.09.jpeg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/WhatsApp-Image-2019-11-14-at-15.56.12.jpeg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

Tras el asesinato de Robinson Romaña y León Jairo Rendón, escoltas de un esquema de seguridad de la Unidad Nacional de Protección, en Chocó, el Sindicato de esta institución exigió la renuncia del director de la organización, Pablo González, por **"su irresponsabilidad" a la hora de garantizar la seguridad de quienes prestan sus vidas para garantizar la protección** de personas en Colombia.

De acuerdo con Wilson Devia, presidente del Sindicato, el asesinato de los dos escoltas se generó el pasado fin de semana, cuando Romaña y Rendón acompañaban a sus protegidos a Domingodó. Según la denuncia hombres paramilitares interceptaron la lancha en la que se movilizaban y retuvieron a los dos integrantes de la UNP, **desmintiendo la hipótesis de un secuestro hacia los protegidos. Posteriormente, fueron asesinados y lanzados al río.**

Devia señaló que el ingreso al territorio se realizó con las alertas por parte de la Fuerza Pública de inseguridad, debido a que no había presencia ni del Ejercito ni de la Marina en el lugar. Sin embargo, también recordó que esta es uno de los riesgos más grandes a los que se ven expuestos los escoltas en Colombia, porque usualmente no hay presencia del Estado en los lugares en donde ellos deben prestar la protección.

De igual forma, el Sindicato también denunció que hubo una ausencia de coordinación por parte de la UNP desde su director, que obligó a que los cuerpos tuviesen que ser recogidos por los escolta, con recursos de una empresa privada que les permitiera **recoger a sus compañeros**, hecho que pudo haber alterado la cadena de custodia para la posterior necropsia.

### **El negocio detrás de la protección de la UNP**

Tanto Romaña como Rendón eran escoltas que hacían parte de la tercerización laboral**, puesto que fueron empleados por empresas con las que la Unidad Nacional de Protección realiza las contrataciones,** hecho que ha sido cuestionado por el sindicato y que para el mismo aumenta los riesgos a los que son expuestos los trabajadores, debido a que esos contratos son temporales, ocasionando un detrimento emocional, no otorgan en buenas condiciones los equipos de seguridad  y brindan seguros de vida que no cubren las contingencias de su labor. (Le puede interesar: [Por malas condiciones laborales escoltas de la UNP cesarían actividades](https://archivo.contagioradio.com/escoltas-unp/))

De acuerdo con el sindicato este modelo de contratación haría parte de un negocio que beneficia a "empresarios que no tienen el interés de proteger vidas, sino de lucrarse con esta tercerización".

### **Un plan pistola contra los escoltas en Colombia**

El Sindicato de escoltas de la Unidad Nacional de Protección recientemente alertó a las instituciones gubernamentales sobre un plan pistola que se estaría impartiendo en contra de quienes hacen parte de esta organización, pero según Devia no han recibido ninguna respuesta por parte ni del director de la UNP ni de las instituciones.

"**Vemos la negligencia del Estado y de los entes de control** porque llamamos la atención, en principio con el hecho que sucedió en Arauca en donde el Ejército Nacional asesina a un escolta que iba con una protegida, posteriormente se dio la masacre en contra de una candidata en el departamento del Cauca, en el que un escolta salió evacuado, pedimos crear un protocolo de seguridad, sin embargo no fuimos escuchados".

A estos hechos se suman la retención a otro escolta en el departamento del Cauca, por disidencias de las FARC y finalmente el asesinato de Romaña y Rendón. Asimismo el sindicato manifestó que su labor es proteger a quienes se les destine por parte de la UNP, hecho por el cual son objetivo de cualquier actor armado. **Es importante resaltar que los escoltas no hacen parte de un grupo de fuerza o de choque**, por tal motivo no pueden asumir las garantías de seguridad que deben ser otorgadas por la Fuerza Pública. (Le puede interesar también: [Retiran parte del armamento a algunos escoltas del Partido FARC](https://archivo.contagioradio.com/esquema-seguridad-sin-armas-partido-farc/))

Este 14 de noviembre, a las 7 de la noche, integrantes del sindicato realizaran un plantón a las afueras de la Unidad Nacional de Protección en homenaje a la vida y labor de los escoltas asesinados y en rechazo al aumento de homicidios en contra de líderes sociales en Colombia.

![Escoltas caídos en actos de servicio](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/WhatsApp-Image-2019-11-14-at-15.38.09-767x1024.jpeg){.aligncenter .wp-image-76514 width="329" height="440"}

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe><iframe id="audio_44353132" frameborder="0" allowfullscreen scrolling="no" height="200" style="border:1px solid #EEE; box-sizing:border-box; width:100%;" src="https://co.ivoox.com/es/player_ej_44353132_4_1.html?c1=ff6600"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
