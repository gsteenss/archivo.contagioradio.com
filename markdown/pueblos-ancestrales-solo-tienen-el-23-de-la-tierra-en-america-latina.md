Title: Pueblos ancestrales solo tienen el 23% de la tierra en América Latina
Date: 2015-10-19 13:37
Category: DDHH, Economía
Tags: Ambiente y Sociedad, Bolivia, colombia, derechos de los pueblos indígenas, Encuentro Regional: “Retos en la implementación de los derechos colectivos a la tierra y los recursos en América Latina”, Hotel TRYP, Indígenas de América Latina, mexico, Perú, Rights and Resourses Iniciative, RRI
Slug: pueblos-ancestrales-solo-tienen-el-23-de-la-tierra-en-america-latina
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/10/image54203ee2c80d35.92871250.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: [www.losandes.com.ar]

###### [21 Oct 2015] 

Este 20 de octubre finalizó en Bogotá el **Encuentro Regional: “Retos en la implementación de los derechos colectivos a la tierra y los recursos en América Latina”**, en el que líderes indígenas, afrodescendientes, y representantes de ONGs de 10 países debatirán sobre la situación de los derechos colectivos de las comunidades para desarrollar nuevas propuestas con el fin de **fortalecer las  políticas sobre los derechos colectivos de las comunidades.**

**En América Latina solo el 23% de la tierra está formalmente reconocida o bajo control de las comunidades y pueblos,** el espacio se centrará en  la “discusión regional frente las reforma y cambios normativos en las políticas nacionales que limitan el ejercicio y el goce de los derechos colectivos de la tierra, del territorio y de los recursos”, según la Asociación Ambiente y Sociedad, uno de los organizadores del evento, junto a Rights and Resourses Iniciative, RRI.

La discusión se basa en un estudio regional enfocado en Bolivia, Colombia, Perú, México, Brasil y Guatemala, en el que se encontró que "**el fracaso catastrófico de los gobiernos en torno al respeto de los derechos básicos sobre la tierra de más de mil millones de personas…** mientras que los líderes de los gobiernos están negociando acuerdos internacionales para poner fin a la pobreza y frenar el cambio climático, están fallando en sus propias casas. Los gobiernos siguen repartiendo las tierras de las poblaciones locales para un desarrollo económico que explota los recursos naturales, acelera el cambio climático y destruye los medios de vida de las comunidades”, según  Andy White, Coordinador del RRI.

Omaira Bolaños Directora de Rights and Resourses Iniciative, para América Latina  afirma que "**de estos países México y Brasil**, son los que tienen un mayor porcentaje de tierras reconocidas por las comunidades" y que México, Brasil, Colombia, Perú y Bolivia "**han instituido leyes para garantizar los derechos de  las comunidades**", aunque hay casos en los que se generan "concesiones de diferentes tipos que generan conflictos" entre los pueblos ancestrales y el gobierno. Por ejemplo en "**Colombia 33.000 tierras de las comunidades están otorgadas a multinacionales".**

Es decir, que **aun existen muchos vacíos y restricciones como la falta de recursos, de capacidad y de voluntad política,** para que los derechos de los pueblos sean una realidad, ya que usualmente cuando **"se avanza en una Ley para garantizar derechos de comunidades, enseguida hay un retroceso", indica Bolaños.**

"Los mecanismos de ayuda existen, pero deben cumplir su objetivo que es que las comunidades puedan tener voz y defender sus derechos", de manera que la seguridad jurídica debe ser para las comunidades ancestrales y no para las multinacionales, de manera que "la consulta previa no puede ser un apoyo a proyectos que ya están definidos", concluye.
