Title: Ruíz Massieu, el mexicano que releva a Jean Arnault en Misión de la ONU
Date: 2018-12-11 11:15
Author: AdminContagio
Category: Nacional, Paz
Tags: Jean Arnault, misión de verificación ONU
Slug: ruiz-massieu-el-mexicano-que-releva-a-jean-arnault-en-mision-de-la-onu
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/12/Diseño-sin-título-770x400.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: ONU 

###### 11 Dic 2018 

[Después de trabajar por tres años en el seguimiento y cumplimiento del acuerdo de paz entre Gobierno y Farc, el diplomático francés, **Jean Arnault** dejará su cargo para darle paso al mexicano Carlos Ruíz Massieu, quien será el responsable de continuar con el proceso de reincorporación de los excombatientes, así lo anunció el secretario general de las **Naciones Unidas, António Guterres**.]

El pasado 30 de noviembre, Arnault anunció el fin de su labor en Colombia argumentando que era necesaria una renovación en el liderazgo del proceso, a su vez destacó los múltiples cambios de los que fue testigo desde su llegada al cargo en 2015,  los avances en cuanto a la dejación de armas, eventos de reconciliación y emprendimientos productivos, además del trabajo desarrollado con entidades gubernamentales y comunidades del país.

Durante su paso por Colombia, Arnault colaboró en el proceso de reintegración a la vida civil de **más de 13.000 excombatientes de las Farc**, la **dejación de más de 9.000 armas**, el establecimiento de la Comisión de la Verdad y a la inclusión y participación política del ahora partido Fuerza Alternativa Revolucionaria del Común (FARC). [(Le puede interesar: Jean Arnault expresó su preocupación por el proceso de reincorporación en Colombia)](https://archivo.contagioradio.com/49353/)

### **¿Quién es el nuevo Jefe de la Misión de la Onu en Colombia Carlos Ruíz Massieu?** 

Carlos Ruiz Massieu cuenta con una licenciatura en derecho de la Universidad Iberoamericana de Ciudad México y una maestría en Ciencias Políticas, con énfasis en políticas latinoamericanas de la Universidad de Essex de Reino Unido. Tiene 25 años de experiencia en el establecimiento y apoyo de operaciones de paz en contextos bilaterales y multilaterales promoviendo reformas en áreas como paz y seguridad, desarrollo y gestión.

Dentro de los cargos en los que se ha desempeñado, Massieu representó a México ante el **Consejo Económico y Social de las Naciones Unidas y ante las Comisiones Principales de la Asamblea Genera**l en cuanto a temas como gestión y desarrollo de 2004 a 2008, se desempeñó  como representante alterno de México ante el Consejo de Seguridad de la la ONU de 2009 a 2010 y fungió como director general adjunto de la Secretaría de Relaciones Exteriores de México además de trabajar en la Embajada de México en Costa Rica.

######  Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
