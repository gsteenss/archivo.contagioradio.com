Title: Convocatoria abierta para el 4to Festival de cine por los DDHH
Date: 2016-11-29 15:29
Author: AdminContagio
Category: 24 Cuadros
Tags: Cine, Convocatoria, DDHH, Festival
Slug: convocatoria-festival-de-cine-ddhh
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/15272131_1090183611079710_2099907284793226525_o.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### Foto: Festicine DDHH 

##### 29 Nov 2016 

Hasta el 20 de diciembre estará abierta la convocatoria para ser parte de la selección oficial de la cuarta edición del **Festival Internacional de Cine por los Derechos Humanos Bogotá** que se realizará en abril de 2017 en las ciudades de Bogotá, Barranquilla, Cartagena y Medellín.

El Festival reúne las mejores películas con enfoque en la **promoción y defensa de los derechos humanos+** en Colombia y el mundo, convirtiéndolo en un escenario óptimo para la exhibición del cine nacional e internacional que aborda esta temática, y en **una iniciativa que resalta el papel del arte y la cultura en la construcción y la comunicación de ideas para la paz y el fomento de los DD. HH**. Le puede interesar: [Estos son los ganadores del 3er Festival de Cine por los DDHH](https://archivo.contagioradio.com/estos-son-los-ganadores-del-3er-festival-de-cine-por-los-ddhh/) .

La invitación es para directores, realizadores y productores nacionales e internacionales a participar con sus creaciones audiovisuales por un puesto en la competencia y **ser parte de la exhibición y circulación de la selección oficial** frente a una amplia audiencia en diferentes regiones de Colombia y del mundo. Para esta cuarta edición, las categorías en competencia son **largometraje, cortometraje, documental, corto documental y largo documental, infantil y animación**, para producciones tanto nacionales como internacionales.

“La convocatoria está abierta para que los realizadores de todo el mundo postulen sus proyectos cinematográficos en ocho categorías y formen parte de un movimiento mundial de c**ine con enfoque en temáticas que aporten a la construcción de nuevos imaginarios sociales de paz y respeto a las diferencias**”, afirma Diana Arias, directora del Festival.

"De esta manera les permitimos a los creadores amateurs **ser parte de la discusión global del aporte del arte, del cine, a la construcción de una cultura de paz** y hacer parte de la principal ventana de exhibición de películas que abordan el tema de los derechos humanos” añade la directora.

### **¿Cómo participar?** 

Los interesados en inscribir sus películas y obtener un puesto en la selección oficial de 2017, pueden **descargar las bases de participación** [aquí](http://cineporlosderechoshumanos.co/wp-content/uploads/2016/10/CONVOCATORIA-2017.pdf) e inscribirse en el formulario online dispuesto para ello. También pueden hacerlo a través de las plataformas FestHome, MoviBeta, UptoFest y Click for festivals.

### **¿Quienes pueden participar del Festival? ** 

Pueden participar las y los productores, directores y distribuidores de cualquier país que sean **titulares de los derechos patrimoniales de autor**, **los derechos intelectuales** y todos los demás derechos sobre la obra audiovisual con la que concursan. Solo podrán participar en el Festival las **obras audiovisuales realizadas después del 1 de julio de 2015**.

El Festival Internacional de Cine por los Derechos Humanos Bogotá es organizado por Impulsos Films. Para mayor información, los interesados pueden comunicarse al correo electrónico: cineporlosderechoshumanos@gmail.com o consultar la página web: www.cineporlosderechoshumanos.co
