Title: Mujeres indígenas del Resguardo Provincial completan un mes protestando contra Cerrejón
Date: 2017-08-16 16:19
Category: DDHH, Entrevistas
Tags: Cerrejón, Guajira, indígenas, La Guajira, Paro de indígenas
Slug: 45359-2
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/08/Mujeres-indigenas-Cerrejon.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Albaciudad.org] 

###### [16 Ago. 2017] 

Hace un mes la comunidad de Provincial en la Guajira se encuentra en paro por las **graves afectaciones pulmonares y de brotes en la piel de los niños a raíz de la actividad del Cerrejón  a 350 metros de las casas del resguardo indígena.** Según denuncia la comunidad, los pulmones de sus niños se ven negros en los exámenes que les practican y algunos están en la Unidad de Cuidados Intensivos, además sus casas se están cayendo por las explosiones que hace la empresa.

**Luz Ángela Uriana, Líder Indígena del Resguardo  de Provincial, **manifiesta que el mayor impacto es la contaminación ambiental, el polvillo del carbón “que es lo que está acabando con nuestros niños y el Resguardo, hay mucho polvillo que cae las 24 horas".

La decisión de irse a paro la tomaron las mujeres ante la falta de atención por parte del Gobernador de ese Resguardo y de la Empresa Cerrejón "quienes dicen que cuentan con un plan de Responsabilidad Social, pero que a la fecha no ha realizado ninguna acción que nos ayude como comunidad".

Manifesta Uriana que e **una cosa es decir todo lo que está pasando aquí, pero una cosa es decirlo y otra es vivirlo.** Lo que vivimos es algo que es muy triste, porque Cerrejón sabe las contaminaciones que hemos denunciado, sabe que nuestros niños se mueren y sabiendo eso no han hecho nada y además estamos sin agua potable”.

Además, relata la líder que hace 4 meses tienen el acueducto comunitario dañado y la respuesta del cabildo ha sido no repararlo para presionar a las mujeres que convocaron el paro a levantarlo.

### **¿Cuáles son las afectaciones a la salud de los niños y niñas de Provincial?** 

Manifiesta Uriana que todos los niños del Resguardo, que representan **cerca del 30% de las 600 personas que lo habitan, sufren de enfermedades respiratorias** que los obligan a ir a la Unidad de Cuidados Intensivos durante varios días y varias veces a la semana “se les ven negros los pulmones en los exámenes que les practican”. Le puede interesar: [El Cerrejón incumple tutela fallada a favor de un menor de 2 años](https://archivo.contagioradio.com/el-cerrejon-incumple-tutela-fallada-a-favor-de-un-menor-de-2-anos/)

Además, dice que muchos de ellos también sufren por las alergias que les aparecen en el cuerpo “hay una niña que falleció hace un año por esas afectaciones, ¿quién va a responder? ¿quién nos va a devolver a nuestros niños? Seguiremos aquí luchando hasta que hagan algo, hasta que no veamos un resultado para nuestros niños”.

### **El Resguardo está viendo morir a sus niños porque no tienen recursos para atención médica** 

La mayoría de los niños y niñas que ingresan a los servicios de salud requieren del tratamiento de especialistas para tratar sus dolencias y si bien algunos ya han podido ser tratados, otros padres no cuentan con los recursos para poder llevar a sus hijos para que sean atendidos en centros asistenciales con suficientes insumos y personal calificado.

**“Por falta de plata, de recurso, algunos no pueden ir al especialista, otros no pueden volver a los controles.** No tenemos trabajo. Cerrejón tampoco se le da la gana de garantizarnos el derecho a la salud después de tanto daño que han hecho”.

### **Cerrejón no ha querido hablar con las comunidades** 

Relata Uriana que la empresa Cerrejón no se ha querido reunir con ellos “porque saben que lo que estamos diciendo es verdad. Que nos pidan los documentos que quieran que nosotros los tenemos”. Le puede interesar: [Comunidades Wayuu vuelven a bloquear la vía férrea del Cerrejón](https://archivo.contagioradio.com/comunidades-wayuu-exigen-derecho-a-la-autonomia-regional/)

### **Exigencias de la comunidad indígena** 

Lo que piden las comunidades a la empresa Cerrejón es que apliquen las medidas necesarias para salvaguardar la vida de los niños, niñas, las mujeres en embarazo y en general de toda la comunidad del resguardo. Le puede interesar: [Suspendidas las operaciones de Cerrejón para desviar el Arroyo Bruno en La Guajira](https://archivo.contagioradio.com/suspendidas_actividades_arroyo_bruno_la_guajira_corte_constitucional/)

“Que no sean tan inhumanos, unos monstruos que no quieren ver. Ellos nos han visitado, han visto a los niños. Lo que pedimos es que se nos escuche, sino haremos tutelas para que no se sigan haciendo más daños ambientales, **que se cierren los trabajos que hacen tan cerca de nuestro resguardo”.**

Además, las comunidades estarán radicando un derecho de petición al Alcalde de Barrancas para que garantice la vida de las personas que están en el paro y el acceso al agua.

<iframe id="audio_20373716" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_20373716_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)

<div class="osd-sms-wrapper">

</div>
