Title: Conozca el acuerdo de diálogo firmado entre el Gobierno y el ELN
Date: 2016-03-30 12:33
Category: Nacional, Paz
Tags: Diálogos de paz en Colombia, Dialogos gobierno eln, ELN
Slug: conozca-el-acuerdo-de-dialogo-firmado-entre-el-gobierno-y-el-eln
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/03/paz-manos.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Generación Paz ] 

###### [30 Mar 2016 ] 

Luego de [[tres años de contactos](https://archivo.contagioradio.com/los-momentos-cruciales-de-la-fase-exploratoria-eln-gobierno/)] y dieciocho meses de diálogos exploratorios entre el gobierno y la guerrilla del ELN, **desde Caracas se anunció el inicio de la fase pública de la mesa de conversaciones de paz**, con Cuba, Noruega, Chile, Venezuela, Brasil y Ecuador, como países garantes y seis puntos de discusión en la agenda, así como [[algunas precisiones sobre el proceso](https://archivo.contagioradio.com/se-anuncia-inicio-fase-publica-de-conversaciones-de-paz-con-guerrilla-del-eln/)].

###### 

[![hoja 1 paz](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/03/hoja-1-paz.jpg){.aligncenter .size-full .wp-image-22041 width="613" height="795"}](https://archivo.contagioradio.com/conozca-el-acuerdo-de-dialogo-firmado-entre-el-gobierno-y-el-eln/hoja-1-paz/) [![hoja 2 paz](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/03/hoja-2-paz.jpg){.aligncenter .size-full .wp-image-22042 width="614" height="796"}](https://archivo.contagioradio.com/conozca-el-acuerdo-de-dialogo-firmado-entre-el-gobierno-y-el-eln/hoja-2-paz/) [![Hoja 3 paz](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/03/Hoja-3-paz.jpg){.aligncenter .size-full .wp-image-22043 width="599" height="791"}](https://archivo.contagioradio.com/conozca-el-acuerdo-de-dialogo-firmado-entre-el-gobierno-y-el-eln/hoja-3-paz/) [![Hoja 4 paz](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/03/Hoja-4-paz.jpg){.aligncenter .size-full .wp-image-22044 width="599" height="795"}](https://archivo.contagioradio.com/conozca-el-acuerdo-de-dialogo-firmado-entre-el-gobierno-y-el-eln/hoja-4-paz/)<iframe src="http://co.ivoox.com/es/player_ej_10999034_2_1.html?data=kpWmm56Ud5Whhpywj5WUaZS1kZWah5yncZGhhpywj5WRaZi3jpWah5yncaLnypDgx5DKrdPhwtfc0JDQs9Sfwsjix9fIs9SfxcqYxs6Jh5SZopbZ0czTb6jjw87S1NPTcYy5rbOah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ%3D%3D&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>  
 
