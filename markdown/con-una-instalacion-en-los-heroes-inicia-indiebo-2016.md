Title: Con una instalación en 'Los Héroes' inicia IndieBo 2016
Date: 2016-07-14 16:48
Author: AdminContagio
Category: 24 Cuadros
Tags: Festival cine independiente, Indiebo
Slug: con-una-instalacion-en-los-heroes-inicia-indiebo-2016
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/07/indiebo.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

#####  Foto: IndieBo 

##### [14 Jul 2016] 

Con una instalación en el interior del Monumento a los Héroes, ubicado en la ciudad de Bogotá, inició la segunda edición de IndieBo, Festival de Cine Independiente que desde hoy hasta el próximo 24 de julio, trae a la ciudad una selección de más de 100 películas de 35 países del mundo.

En esta oportunidad, el encuentro cinematográfico rendirá un tributo al trabajo y legado de Brian de Palma, desde la mirada de Noah Baumbach y Jake Paltrow en el documental que lleva como título el mismo apellido del director de grandes producciones como 'Los intocables' 'Carlito's Way' y ' Scarface'.

Varias de las cintas que hacen parte de la [lista de títulos](http://indiebo.co/programacion/) que podrán disfrutar los bogotanos, en las 6 salas y 4 escenarios al aire libre de Indiebo, han pasado por algunos de los festivales más prestigiosos del mundo como lo son Cannes, Sundance y Tribeca, entre los que se encuentran 'MaggiesPlan'‬ de la directora Rebecca Miller, 'Microbe and Gasoline' de Michel Gondry, el documental 'By Sydney Lumet'  de Nancy Buirski entre otros.

Al igual que en la primera edición, el Festival incluye en su programación actividades académicas y de innovación como el INDIEBOX, en el que se dictarán conferencias y se realizará la presentación de nuevas tecnologías al servicio del audiovisual como la fotografía en 360 grados, experimentación con realidad virtual y proyecciones en gran formato.

El Cinebus, es otra de las propuestas del festival en colaboración con la Universidad Sergio Arboleda , que busca recorrer la ciudad con funciones especiales, así como los IndieTalks que son conversatorios con directores, productores y realizadores sobre diferentes temáticas relacionadas con el movimiento audiovisual independiente.

<iframe src="https://www.youtube.com/embed/JwPzV8Q4y2k" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>
