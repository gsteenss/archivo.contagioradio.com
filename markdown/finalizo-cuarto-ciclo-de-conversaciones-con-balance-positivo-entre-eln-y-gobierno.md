Title: Finalizó cuarto ciclo de conversaciones entre ELN y Gobierno
Date: 2017-12-04 17:14
Category: Uncategorized
Tags: ELN, proceso de paz, Quito
Slug: finalizo-cuarto-ciclo-de-conversaciones-con-balance-positivo-entre-eln-y-gobierno
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/02/proceso-de-paz-eln.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: El Telegrafo] 

###### [04 Dic 2017]

Finalizó el cuarto ciclo de conversaciones en Quito que deja como resultado la recolección de información para construir el mecanismo de participación de la sociedad, **un cese bilateral al fuego que se ha visto empañado por acciones tanto del ELN como de la Fuerza Pública y la renuncia del jefe negociador del equipo del gobierno**, Juan Camilo Restrepo.

### **El cese bilateral tambalea** 

Durante estos dos meses que se han adelantado del mecanismo de cese bilateral al fuego, acciones de la Fuerza Público como el **asesinato de los 7 campesinos en Tumaco, de la indígena Efigenia Vasquez, en el resguardo de Kokonuko Cauca**, el ataque a dos campamentos del ELN, y de esta guerrilla que todavía mantiene el control en algunos territorios del país, ha provocado fuertes violaciones al mecanismo.

Por este motivo ambas partes acordaron establecer una mesa Transitoria de Conversaciones, que comenzó a trabajar desde el 1 de diciembre e ira hasta el 9 de enero, y que buscará resolver las diferentes dificultades que se le han atravesado al cese bilateral y sesionará en Bogotá entre los días 5 al 12 de diciembre. (Le puede interesar:["ELN debe generar credibilidad y reforzar su unidad interna: Carlos Velandia"](https://archivo.contagioradio.com/eln-asesinato-indigena/))

### **El balance positivo del cuarto ciclo** 

Dentro de los avances que se registraron en este cuarto ciclo de conversaciones están las audiencias que se generaron con organizaciones, colectivos y plataformas de todos los sectores de la sociedad y que recogieron las propuestas para **conformar el mecanismo de participación que tendrá la sociedad en la mesa de Quito**.

De igual forma se logró establecer el Fondo de Cooperación conformado por Alemania, Holanda, Italia Suecia y Suiza y que financiará a la delegación del ELN en algunas actividades de la Mesa de conversaciones   durante el proceso de paz. **También se debatieron los términos para concretar el acuerdo sobre desminado humanitario en tres zonas entre los municipios de Samaniego y Cruz en Nariño**. (Le puede interesar: ["Avanzan las audiencias preparatorias para la participación social en diálogos del ELN- Quito"](https://archivo.contagioradio.com/48655/))

### **La probable renuncia de Juan Camilo Restrepo al equipo de paz del gobierno** 

Juan Camilo Restrepo, quien se desempeñaba como jefe negociador del equipo de paz del gobierno, anunció que le solicitó al presidente Juan Manuel Santos que lo retiré de su cargo, acción que será efectuada en los primeros días de enero, **previo a que culmine el primer ciclo del cese bilateral que finaliza el 9 de enero del 2018** y en donde iniciaría el quinto periodo de conversaciones.

En una carta el ELN manifestó que esperan que la “renovación del equipo de gobierno llegue con nuevos impulsos y dinámicas que permitan el avance de la agenda pactada el pasado 30 de marzo del 2016” y agregó que su voluntad continúa intacta.

###### Reciba toda la información de Contagio Radio en [[su correo]
