Title: Reflexiones diversas para las colombianas
Date: 2015-12-30 10:21
Category: Mar Candela, Opinion
Tags: feminismo, genero
Slug: reflexiones-diversas-para-las-colombianas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/12/011.jpg6nmybo.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### foto: Lau HMyO 

#### **[Mar Candela Castilla ](https://archivo.contagioradio.com/mar-candela/)** 

###### 30 Dic 2015 

Yo me siento parte de un libreto, de una historia de horror vieja. De esas que es necesario contarlas una y otra vez para que no sean olvidadas, en busca de que no se repita y logremos cambiar la realidad.

Me resulta difícil hablarles a todas del tema de los feminicidios sin excluir alguna mirada. Hablarle a la santa, a la puta, a la creyente, a la atea, a la feliz y a la infeliz, hablarle a la revolucionaria, a la pacifista; a la que  quiere ver y a la ciega  voluntaria -sin  tocar sensibilidades personales de alguna-.

Soy una mujer de ritos espirituales. Creo en el “más allá” o  como quieran llamarlo, por  ejemplo, “la fuerza”, está muy de moda y aplica perfecto. Creo que todos y todas somos dioses y diosas que provenimos de una misma fuente, llámenla como la llamen.

Creo en muchas cosas que racionalmente no puedo, ni podré nunca comprobar, pero mis vicios “espirituales” no le hacen daño a nadie y desde que no formo parte de alguna institución religiosa NUNCA he catequizado.

En  honor a la defensa de la libertad de credo, conciencia y libre desarrollo de la personalidad,  Feminismo Artesanal camina con mujeres de todos los colores espirituales, mentales, físicos e intelectuales. Y es por eso que me resulta tan difícil pronunciarme ante TODAS  sin la preocupación de incomodar  a alguna-

Debo hacer una confesión  antes de continuar con mis reflexiones de fin de año.

Yo no sustento nuestra lucha política y social desde una mirada “metafísica”, “positivista” o espiritual de ninguna corriente, ni siquiera de la mía.

Aunque  me reconozca  como   una “bruja hechicera”  que conjura a diario con  la fuerza de su “magia interior”  la justicia y la verdad.

Mis argumentos  SIEMPRE han sido racionales, palpables y reales sin importar  las creencias  religiosas: de  todas las mujeres  y hombres con quienes compartimos luchas y debatimos.  Incluyendo las mías.

Para mi es claro que no podemos dejar este tema únicamente en el plano espiritual, a mi manera de ver, como  lo hacen algunos movimientos de mujeres que aprecio profundamente porque sanan y liberan cuerpos y corazones de mujeres víctimas y las llevan a vivir una nueva vida después del infierno, sin demeritar este trabajo, el cual me ha sido útil para muchos casos que he atendido, e incluso para mi propia vida en diferentes momentos. Por  principio ético procuro que mis prácticas espirituales que suenan fantasiosas a muchos pensadores no vicien la causa  real humanitaria que defiendo.

Sin embargo nuestra mirada espiritual sobre la violencia machista no debe aniquilar la mirada material, política, social y de  justicia.

Además, debo agregar que en lo personal, conozco tantas ateas fortalecidas únicamente en su lógica, venciendo así el patriarcado, debo aplaudirlas, yo las admiro. Es de valientes vivir sin ningún tipo de fe mayor que la fe en sí misma, y sus logros me hacen sentir una mujer cobarde porque uno de mis defectos, por llamarlos de alguna manera, es que yo soy absolutamente espiritual aunque no lo parezca, lo soy sin ser seguidora de religiones.

 Más allá de nuestras opiniones sobre lo espiritual,  algo que es indiscutible  es el hecho de que  necesitamos del amor para vencer cada día de lucha, porque al final el único dios incuestionable es el amor.  No ese amor patriarcal, sexista, romántico, heteronormativo que impone  la monogamia como única practica  y lenguaje amatorio valido

Si no  el amor del poder. Ese amor lleno de ternura, afecto revolucionario y libertario, que nos  llena de fuerza a ateas y a creyentes para vencer cada día  que viene lleno de  odio hacia nosotras.

A todas nos une la misma causa: La lucha contra el patriarcado, la defensa de la vida de todas las niñas y mujeres en un sistema de odio hacia  nosotras.  A todas nos une la búsqueda de un planeta seguro para las mujeres, donde todas y todos podamos vivir sin sexismo ni violencias.

Entonces, independiente de quienes seamos, quiero mandarles mis mejores deseos de fin de año y mis regalos son algunas de mis verdades.

Quisiera poder hacer una nota romántica, de esas que te iluminan con palabras bonitas y consejitos de superación personal. Quisiera poder escribir una nota amable como las que tanto nos gustan, llenas de palabras dulces y con entusiasmo. De corazón quisiera decirles a todas que: “la cosa no esta tan grave”,  “que hay esperanza”, “que el Estado y la sociedad han asumido la realidad de la mujer y que no estamos solas en nuestra resistencia en contra de la violencia machista”.

Daría lo que soy y lo que tengo por poder darles en este fin de año palabras alentadoras, llenas de toda esa magia comercial que hemos heredado para estas fechas; donde todo el mundo es bueno, todos nos amamos sin conocernos, todos somos  sensibles y amorosas personas.

Pero la realidad mis estimadas compañeras es que el ratico de las fiestas de fin de año es flor de un día. Que pese a toda la disposición, a la bondad en estas fechas decembrinas, solo Feminismo Artesanal ha tenido que recibir denuncias de una mujer agredida cada semana. No quiero saber las estadísticas sumadas a todas las organizaciones de derechos de las mujeres, me aterra la cifra que podamos  conocer.  Feminismo Artesanal ha tenido que acompañar con amor e impotencia cuatro casos de violaciones y tres feminicidios en lo que va de este mes de “paz y armonía”.

Pero compañeras pese a mi poco ánimo y deseo de romántico positivismo hay algo que debemos tener claro:

Desde donde yo lo veo hemos ganado mucho al lograr que cada vez más mujeres levanten su voz. No es que el feminicidio haya aumentado en los últimos tiempos o que antes a las mujeres nos asesinaban mucho menos como algunas personas afirman.

Lo que está pasando es que gracias a la guerra sin cuartel que el feminismo ha dado contra la violencia de género, cada vez somos muchas más las mujeres que vencemos el miedo a denunciar. Algo que sé es que no existe estadística de lo invisible y que las cosas que nos enteramos no son ni la sombra de las cosas que no conocemos.

No me siento derrotada con el supuesto aumento de feminicidios porque para mí es claro que lo que ha aumentado son las denuncias. Los feminicidios han sido los mismos de siempre que estaban en las sombras y nadie se atrevía a denunciar.

 ¿Cómo puedo ser alentadora? ¿Cómo puedo decirles que vivamos sin miedo?

Puedo porque entendí que ser alentadora y vivir sin miedo no es un sentimiento, es una apuesta racional política. Tenemos que levantarnos todos los días y ser valientes e ir de cara a nuestros miedos  y decirles que cada vez somos más mujeres enfrentando la  realidad y desarticulando el patriarcado. Cada vez que una mujer defiende su  vida  está cambiando la realidad.

Mi deseo es ver  la llegada del glorioso día donde ninguna mujer deba sentirse valiente sino donde todas nos sintamos seguras.

Termino con esto: Mujer, sin importar quien seas, sin conocernos, e incluso sin tener cosas en común, te recuerdo que cuando callas permites que el patriarcado gane y que sigamos siendo asesinadas y antes violentadas. Confía en tu poder de lucha, yo confío en tu lucha individual, porque sé que es con ella que apaleamos el patriarcado y hacemos una transformación autentica social. Cientos de niñas confían en ti y en mí para que les demos una nueva realidad. Por favor,  decide ser tu propia heroína. Abrazos de luz a todas.
