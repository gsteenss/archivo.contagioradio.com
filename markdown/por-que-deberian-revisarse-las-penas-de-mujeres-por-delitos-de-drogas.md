Title: Organizaciones proponen revisar penas de mujeres por delitos de drogas
Date: 2016-05-13 14:17
Category: eventos, Mujer
Tags: Corporación Humanas, crisis carcelaria, justicia transicional, narcotrafico
Slug: por-que-deberian-revisarse-las-penas-de-mujeres-por-delitos-de-drogas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/05/Mujeres-en-la-cárcel-e1463166183400.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Fundación Bacatá 

###### [13 May 2016]

Con el objetivo de impulsar una propuesta para solicitar al gobierno colombiano una revisión de las condenas de las mujeres privadas de la libertad por hechos relacionados con el narcotráfico, **el próximo 16 de mayo, se realizará el foro  “Justicia transicional y delitos de drogas: Una oportunidad para reflexionar sobre las penas desde la experiencia de las mujeres en prisión por estos delitos”.**

Este espacio tiene como fin analizar la pertinencia de criminalizar delitos de drogas con penas altas e incluso, penas privativas de la libertad, cuando quienes incurren en estas conductas lo hacen por razones de pobreza, ya sea porque muchas son mujeres cabeza de hogar o porque fueron engañadas, como lo explica Adriana Benjumea, directora de la Corporación Humanas.

Esta propuesta surge a partir de las investigaciones realizadas por la Corporación Humanas, donde se evidencian las consecuencias personales y sociales que tiene la cárcel en mujeres que **incurrieron en delitos de drogas motivadas por la exclusión, marginalidad social y el abandono estatal,** demostrando que esos aspectos se agudizan en la prisión.

Para Benjumea, el país tiene la oportunidad de analizar lo que han sido las políticas penales por delitos relacionados con drogas, pues en los últimos tiempos se viene endurecido las condenas contra el eslabón más débil esta cadena, **siendo tratados como los grandes narcotraficantes sin serlo, y en cambio se siguen llenando las cárceles y aumentando el hacinamiento.**

Teniendo en cuenta el contexto de pobreza y vulnerabilidad en el que están las mujeres, hay mayor población femenina en las cárceles por estos delitos, que además **no son hechos "violentos”**, como lo asegura Benjumea, quien añade que “estas mujeres están involucradas en un conflicto social que ha llevado a un endurecimiento de las penas, pero que el Estado deberá buscar salidas de extinción de la persecución penal para estas personas”.

El foro se realizará en el Centro de Memoria, Paz y Reconciliación, con entrada abierta y contará con la participación de Hilary Anderson y Luz Patricia Mejía de la Comisión Interamericana de Mujeres de la OEA; Carolina Villadiego y Sergio Chaparro de Dejusticia; Camila Moreno del ICTJ; y Luz Piedad Caicedo de la Corporación Humanas. El evento, convocado por Dejusticia, la CIM y la Corporación Humanas, es apoyado por Open Society Foundations.

<iframe src="http://co.ivoox.com/es/player_ej_11523463_2_1.html?data=kpailJiYepShhpywj5WdaZS1lJeah5yncZOhhpywj5WRaZi3jpWah5yncaLY087O0MaPhsbiy9rax8aPcYy30Nfd0dfFp8qZpJiSpJjSb6npzsbbw9iRaZi3jqjc0NnFq8rjjLfOxs7Tb46ZmKialg%3D%3D&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
