Title: Un vistazo a la poesía palestina
Date: 2017-08-10 13:02
Category: Onda Palestina
Tags: BDS Colombia, Palestina, poesia
Slug: poesia-palestina-bds
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/08/poesia-palestina.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: BDS Colombia 

###### 10 Ago 2017

Hasta 1948 la literatura Palestina fue parte del movimiento literario árabe que floreció durante la primera mitad del Siglo XX, el cual había sido influenciado por escritores egipcios, sirios y libaneses, quienes lideraron el movimiento literario de esa época. Sin embargo, el nacimiento de un nuevo movimiento literario, dentro del cual la poesía era su principal elemento, hizo evidente un inusitado progreso en calidad y técnica.

Según el historiador Ilan Pappe, “la poesía fue el único ámbito que salió ileso de la Nakbah. Lo que los activistas políticos no se atrevían a decir, lo cantaban con fuerza los poetas. El servicio secreto israelí se veía impotente para decidir si debía clasificar este fenómeno como un acto subversivo o como un acontecimiento cultural”.

También decía el profesor Pappe que “la poesía era el único medio en que los acontecimientos diarios relacionados con el amor y el odio, el nacimiento y la muerte, el matrimonio y la familia, se podían vincular con cuestiones políticas, como la confiscación de tierras y la opresión del Estado, y airearlas en púbico en festivales especiales de poesía, como el que se celebraba periódicamente en Kafr Yassif, en Galilea”.

Tal como también anota Joseph B. Abboud, “el corto periodo de silencio literario posterior a la Guerra Árabe-Israelí de 1948 fue inmediatamente seguido de un gran despertar y la poesía nacionalista surgió a borbotones reflejando el fervor de sus gentes”. Ésta poesía “interactuó con movimientos árabes y modas extranjeras y gradualmente rompió los esquemas y las técnicas tradicionales; rechazó los antiguos destellos sentimentales y emergió con el sentimiento de una profunda tristeza, más a tono con la realidad dentro de la cual se encontraron viviendo los palestinos después de 1948”.

Sin embargo, durante las primeras décadas de la ocupación, no se desarrolló una poesía palestina de la Resistencia, debido en gran parte, al aislamiento de las y los palestinos en general, en relación con los países árabes y sus estándares culturales, además de las imposiciones a las que fueron sometidos por el régimen sionista.

Una de esas primeras voces que surgió enarbolando el espíritu de la resistencia fue Tawfiq Zayad, nacido en Nazaret en 1922 y fallecido en el Valle del Jordán en 1994. Este poeta estudió literatura en la Unión Soviética y al regresar a Palestina fue elegido alcalde de Nazaret en 1973 con el partido político Rakah.

Tawfīq Zayyād pertenece al grupo de poetas palestinos de combate desde el interior, es decir, al conjunto de poetas palestinos de resistencia que nunca se exiliaron. Esto hizo que su visión de la problemática social palestina fuera extremadamente lúcida. Su obra poética, como es habitual en la poesía de compromiso de todos los rincones del mundo, alberga un tono épico y se convierte en testimonio y denuncia de la realidad social circundante. A continuación les compartimos dos de sus poemas.

Lo imposible

Aquí estaremos  
con una pared sobre el pecho,  
enfrentados al hambre,  
peleando con hilachas,  
desafiando  
cantando nuestras canciones  
invadiendo las calles  
con nuestra ira,  
llenando nuestras covachas con orgullo,  
enseñando la venganza a nuevas generaciones  
Como miles de prodigios  
vagamos errantes  
en Jaffa, Lidda, Ramallah, en Galilea.

Aquí estaremos,  
lavando platos en vuestros bares,  
llenando las copas de los amos  
limpiando sus cocinas sucias de hollín  
para escamotear un bocado de sus colmillos azules  
para alimentar los niños con hambre.

Aquí estaremos con corazón de hielo  
candente infierno en nervio y alma  
sacamos agua de la roca para calmar la sed  
y despistamos la hambruna con el polvo.  
Pero no nos iremos.

Aquí derramaremos la queridísima sangre,  
aquí tenemos un pasado, un futuro,  
aquí somos los inconquistables,  
así que golpea profundo, golpea profundo  
sobre mis raíces.

Os estrecho las manos

Os convoco,  
os estrecho las manos.  
Beso la tierra bajo vuestros zapatos  
y digo: “os rescato,  
os regalo la luz de mis ojos  
y el calor de mi corazón os lo doy,  
pues la tragedia que vivo  
es parte de vuestra tragedia”.  
Os convoco,  
os estrecho las manos.  
No fui insignificante para mi nación  
y no incliné mi cabeza.  
Me detuve ante el rostro de mis opresores  
- huérfano, descalzo, desnudo -  
porté mi sangre sobre la palma de la mano,  
no puse a media asta mis banderas  
y protegí la hierba que crece  
sobre la sepultura de mis antepasados  
¡Os convoco…! ¡Os estrecho las manos!

Un viento de Oriente

Unas lágrimas de este viento que  
viene de oriente,  
cargadas con gritos de mis amados ausentes,  
gritos degollados por la nostalgia,  
sinceros, sin alaridos,  
llenaron la tierra y los horizontes.  
Vienen cargadas con la pena del valle  
y el olor del rocío, la sangre y la esclavitud.  
Caen sobre mi rostro y mis ojos,  
sobre mi alma y mi garganta  
las lágrimas de este viento que…  
Viene de oriente.

Si te interesa saber más sobre la poesía palestina escucha el especial que emitimos todos los últimos martes del mes por Onda Palestina.

<iframe id="audio_20271080" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_20271080_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Escuche esta y más información sobre la resistencia del pueblo palestino en [Onda Palestina](https://archivo.contagioradio.com/programas/onda-palestina/), todos los Martes de 7 a 8 de la noche en Contagio Radio. 
