Title: Sigue firme la defensa del Río Humadea en Guamal
Date: 2017-02-08 17:38
Category: Ambiente, Nacional
Tags: Bloque CPO9, Ecopetrol y Repsol, Guamal, Río Humadea
Slug: sigue-firme-la-defensa-del-rio-humadea-en-guamal
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/02/guamal.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Mapio] 

###### [8 Feb 2017 ] 

Los habitantes de la vereda Pio XII de Guamal Meta, se manifestaron de forma pacifica en contra de las actividades de Ecopetrol en la zona y por la llegada en horas de la mañana de una cuadrilla de hombres que pretende dar inicio a las obras de construcción del pozo Trogon.

Olga Suárez lideresa comunitaria de Pio XII manifestó, que a pesar del concepto de la Gerencia Ambiental del Meta el cual ordena a la Gobernación, la suspensión de las actividades de Ecopetrol relacionadas con el Bloque CPO9 y Lorito 1, “la empresa insiste en seguir violando nuestros derechos, nuestra tranquilidad y nuestro territorio”.

Suárez comentó que más de 300 familias han firmado para fortalecer el acto legislativo adelantado por la comunidad, exigiendo que Ecopetrol retire sus bloques y maquinarias de las zonas de la cuenca del río Humadea.

Edgar Humberto Cruz, otro de los líderes comunitarios manifestó que está zona de recarga hídrica, de acuerdo con lo establecido en la sentencia T-652/13 de la corte constitucional, “debería ser protegida, por el hecho de que de allí se surten de agua algunos acueductos ubicados aguas abajo de la plataforma”, como es el caso de los acueductos de Castilla la Nueva y la vereda Humadea”.

Por último la lideresa advirtió que la comunidad de Pio XII y las demás afectadas, estarán movilizándose de manera constante “el tiempo que sea necesario, porque aquí fue donde nacimos, crecimos y queremos pasar nuestros últimos días”.

<iframe id="audio_16908543" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_16908543_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
