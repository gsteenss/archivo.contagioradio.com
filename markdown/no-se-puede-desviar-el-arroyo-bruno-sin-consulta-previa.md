Title: No se puede desviar el arroyo Bruno sin consulta previa
Date: 2017-01-13 17:20
Category: Ambiente, Nacional
Tags: Arroyo El Bruno, Cerrejón, Consulta Previa, El Cerrejón en La Guajira, La Guajira, Minería en La Guajira
Slug: no-se-puede-desviar-el-arroyo-bruno-sin-consulta-previa
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/12/Arroyo-Bruno.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Notiwayuu] 

###### [13 Ene 2016]

Luego de la revisión del fallo de segunda instancia el Consejo de Estado ratificó que debe seguir suspendida la desviación del arroyo "Bruno" en la Guajira. Según las primeras informaciones el tribunal ordenó que para este proyecto la multinacional **Cerrejón debe realizar una consulta previa a las comunidades Wayuu que habitan esos territorios.**

Ya desde el mes de Diciembre el tribunal se había pronunciado, sin embargo la empresa impugnó y condujo a que se revisara el fallo en profundidad. Sin embargo, la sección cuarta del Consejo consideró que si se verían afectados los derechos de las comunidades indígenas y por ello ordenó al la dirección de Consulta Previa del Ministerio del Interior que se adelante la diligencia.

Concretamente la decisión y la demanda tienen que ver con que el Ministerio del Interior afirmó, que no se afectaban los derechos de los indígenas asentados en la comunidad "La Horqueta 2" y consideró que la desviación no afectaba a la comunidad, y por ello objetó la decisión del tribunal administrativo y la de segunda instancia del Consejo de Estado.

Se espera que en los próximos días se publique una decisión por parte de la dirección de Consulta Previa de Min Interior acerca del mecanismo y el calendario para aplicarla. Le puede interesar: [ Consejo de Estado frenó desviación de arroyo "Bruno"](https://archivo.contagioradio.com/consejo-de-estado-frena-desvio-arroyo-bruno/).

Lo cierto es que en medio de la crisis por el agua en el departamento y la decisión manifiesta de las comunidades es muy probable que en una consulta previa, apegada a la ley y realizada con alto grado de ética por parte de los funcionarios, la empresa Cerrejón no continuaría, legalmente, con las labores de desviación de la segunda fuente de agua más importante del departamento y de las comunidades Wayuu.

###### Reciba toda la información de Contagio Radio en [[su correo]
