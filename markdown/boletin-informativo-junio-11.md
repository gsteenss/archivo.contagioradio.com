Title: Boletín informativo Junio 11
Date: 2015-06-11 17:30
Category: datos
Tags: alirio uribe, corredor Puerto Vega Teteyé, Derrame de petróleo en Putumayu, Desalojos en Cali, Inundaciones en la cuenca local del Cacarica, Paola Artunduaga, Reforma al Fuero penal militar, representante a la Cámara por el POLO Democrático Alternativo, UTL del Senador Alberto Castilla
Slug: boletin-informativo-junio-11
Status: published

[*Noticias del Día:*]

<iframe src="http://www.ivoox.com/player_ek_4628955_2_1.html?data=lZufmp6ZeY6ZmKiakpqJd6KnmZKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRhtDgxtmSpZiJhaXijK7byNTWscLoytvcjcnJb6vpz87cjZaVcYarpJKw0dPYpcjd0JC%2Fw8nNs4yhhpywj5k%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

-**40 familias** del Jarillón, el Sector Venecia y Las Vegas en Cali, **están siendo desalojadas de sus viviendas** desde el pasado lunes 8 de junio. Aunque **la Alcaldía asegura que las reubicará**, hasta el momento no se les ha hecho entrega de predios, viviendas, o recursos para garantizar su traslado de manera digna y segura. Habla **Ana Eraso**, de la **UTL del Senador Alberto Castilla**.

-El miércoles 10 de junio **la Cámara de Representantes aprobó la reforma del artículo 221 de la Constitución Política de Colombia**, y una ley con más de 100 artículos **que modifica el Código Penal Militar**. Ambas **amplían el fuero penal militar**, según denunció el representante a la Cámara Alirio Uribe, y aún habría en curso **6 iniciativas más promovidas por la bancada del Centro Democrático que buscarían dar impunidad a los militares**. Habla **Alirio Uribe, representante a la Cámara por el POLO Democrático Alternativo**.

-La mayor preocupación de **los habitantes de la vereda** **"La cabaña"** del corredor Puerto Vega Teteyé, tiene que ver con **la contaminación de seis fuentes de agua potable y siete espejos de agua dedicados a la piscicultura**, afectados por la catástrofe ambiental que **perjudica a las 75 familias residentes en el luga**r, sin que el Estado tome medidas aún para mitigar el impacto. **Paola Artunduaga** de la **Comisión Minero-energética y ambiental de la mesa Regional del Putumayo** afirma que el plan de contigencia, "**nunca respondió a las necesidades reales de descontaminación de la zona**".

-**Habitantes de la cuenca local del Cacarica denuncian la ineficacia del estado al no hacer presencia en la alerta ambiental por la ola invernal** que empieza a vivirse en Colombia. Cuatro cuencas en las que residen aproximadamente **3.000** personas han sido afectadas y causan un desplazamiento forzado para los pobladores. Habla **Eulises Ramírez**, **Presidente de la cuenca local del Cacaric**a.
