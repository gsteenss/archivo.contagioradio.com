Title: Música, poesía y teatro en solidaridad con Mocoa
Date: 2017-04-19 14:08
Category: eventos
Tags: artistas, Mocoa, solidaridad
Slug: mocoa-solidaridad-tragedia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/04/maxresdefault-1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### Foto: 

##### 19 Abr 2017 

Este miércoles **19 de abril en la Sala Seki Sano de Bogotá,** un grupo de artistas se solidarizan con los sobrevivientes de la tragedia ocurrida en Mocoa hace 19 días, en una noche donde la música, la poesía y el teatro se encuentran por una buena causa.

Las canciones de **Victoria Sur, los cuentos de Misael Torres,  Aerodanza Visible e Invisible, MC Meya comisión de genero Hip Hop y Maira Lopez Severiche** entre otros artistas, presentaran lo mejor de su repertorio desde las 7 p.m. Le puede interesar: [Un tango por Mocoa](https://archivo.contagioradio.com/un-tango-por-mocoa/).

<iframe src="https://www.youtube.com/embed/TUX9MUuMcrU" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

Las personas que deseen asistir al evento, podrán **ingresar llevando Cobijas, alimentos no perecederos y/o artículos de aseo para adultos y menores de edad a la taquilla de la Sala**, ubicada en la Cll12 No 2-65.
