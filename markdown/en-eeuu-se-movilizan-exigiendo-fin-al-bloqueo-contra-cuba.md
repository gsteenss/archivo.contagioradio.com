Title: En EEUU se movilizan exigiendo fin al bloqueo contra Cuba
Date: 2015-09-16 15:48
Category: Economía, El mundo, Política
Tags: Barakc Obama, Bloqueo económico, Cuba, Cuba resiste, Cuba y EEUU, Encuentro Obama y Raúl Castro Panamá, Estados Unidos, Fidel Castro, Raul Castro, reestablecer relaciones económicas
Slug: en-eeuu-se-movilizan-exigiendo-fin-al-bloqueo-contra-cuba
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/Cubanos-nacidos-bajo-el-bloqueo1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: radio.uchile.cl 

###### [16 sep 2015] 

Más de 60 organizaciones de distintas naciones comenzarán en Washington los “**Días de Acción contra el Bloqueo"**, con el fin de exigir a Estados Unidos que termine con esta acción que ha llevado hambre y necesidades de distintas categorías sociales al pueblo cubano, según lo evalúan las organizaciones.

Los organizadores han informado que hasta la fecha han **confirmado 44 visitas al Congreso, 37 a oficinas de Congresistas y 7 a oficinas de Senadores**. Los delegados de esta jornada proceden de Canadá, EE. UU. y Puerto Rico.

Otras organizaciones participes de estas jornadas, junto a las estadounidenses, serán la Alba-France, de Francia; Australia-Cuba Friendship Society, Bangladesh Sramik Federation; Canadian Network on Cuba (CNC),Toronto; y el Club Argentino de Periodistas Amigos de Cuba. Otras son de países como Brasil, Reino Unido, El Salvador, Dinamarca, Gambia, Guyana, Bélgica, Líbano, Rusia, Kenya y Suecia.

**Resistiendo desde 1959:**

Cuando llega al poder Fidel Castro el **1 de enero de 1959**, Cuba adopta medidas destinadas a **recuperar las riquezas del país y a ponerlas al servicio del pueblo** afectando los intereses de los monopolios que durante más de medio siglo habían saqueado los recursos de la isla e influido en su política interna.

Así la revolución cubana, la decisión de producir cambios económicos y sociales a favor del pueblo,  fue el detonante para que **Estados Unidos de forma brutal e inhumana sancionara mediante la economía** a esta isla que a día de hoy a resistido y avanzado en muchos aspectos, pese al bloqueo.
