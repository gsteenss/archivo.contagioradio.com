Title: Ministerio de Ambiente se compromete con la paz para la naturaleza
Date: 2016-09-21 15:54
Category: Ambiente, Nacional, Paz
Tags: acuerdo de paz, Ambiente, FARC, fracking, Mineria, paz
Slug: ministerio-de-ambiente-se-compromete-con-la-paz-para-la-naturaleza
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/09/CszHRYhXEAA_v-S-e1474490131839.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Ministerio de ambiente 

###### [21 Sep 2016] 

Cerca de **20 organizaciones ambientalistas y animalistas** se reunieron este martes con el Ministro de Ambiente, Luis Gilberto Murillo y el viceministro de la misma cartera, Carlos Alberto Botero López, con el objetivo de ratificarle al gobierno, que ese sector de la sociedad está con la paz para Colombia, en la que también deben estar incluidos los animales y la naturaleza.

**“Es la primera vez que el Ministro de Ambiente le está abriendo una puerta a las organizaciones de base”,** dice Marcela Salguero, integrante de la Plataforma ALTO (Animales Libres de Tortura), una de las organizaciones que participó en la reunión. “Nos la queremos jugar por la paz en todas partes, para que haya una paz con la naturaleza y se reconozca a los animales y el ambiente como una víctima de la guerra”.

Tras la reunión, se definió la creación de **una mesa de trabajo ad hoc con las organizaciones para hacer seguimiento a los compromisos en el pos-acuerdo** en materia ambiental y animal, teniendo en cuenta que en el acuerdo final de paz entre el gobierno y las FARC, se señala que debe haber participación de las organizaciones como veedores en la implementación y cumplimiento de los acuerdos. “Muchas veces el gobierno se queda, y nosotros podríamos ayudar para trabajar de la mano de los técnicos del ministro de ambiente, con la experiencia que tenemos en las regiones”.

Uno de los temas urgentes que le preocupa a estas organizaciones tiene que ver con el hecho de que **las personas que se desmovilizan traen de la selva animales silvestres**. “Ellos han tenido una relación interna con la naturaleza muchas veces se traen los animales de allá, por eso, una de las tareas urgentes es ir a esas zonas de concentración y hacer procesos de educación ambiental y participación, ellos son los que tienen el conocimiento del territorio, los científicos no hemos podido entrar porque son  lugares minados”.

Pese a que este Sí a la paz de las organizaciones ambientalistas no está condicionado, si asegura que **una vez se aprueben los acuerdos de paz en el plebiscito, se empezará un trabajo de presión al gobierno frente a temas álgidos como el fracking o la minería** en páramos, como lo explica Marcela Salguero.

<iframe id="audio_13001748" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_13001748_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
