Title: Nilson Caicedo, líder asesinado en Mocoa habría denunciado más de 10 amenazas de muerte
Date: 2019-12-27 10:20
Author: CtgAdm
Category: Líderes sociales, Nacional
Tags: asesinato, Lider social, Nilson Caicedor, Putumayo
Slug: nilson-caicedo-lider-asesinado-en-mocoa-habria-denunciado-mas-de-10-amenazas-de-muerte
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/Captura-de-Pantalla-2019-12-26-a-las-4.35.02-p.-m..png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:Archivo] 

Este 22 de diciembre fue asesinato Nilson Richard Caicedo, integrante del Concejo Comunitario para el Desarrollo de Comunidades Negras de La Cordillera, en Nariño; se desempeñaba como docente, vocero y líder social de la región Bajo Patía en el mismo departamento; Caicedo se encontraba en Mocoa, Puyumayo, cuando fue atacado por hombres desconocidos quienes le propiciaron 3 tiros con arma de fuego, causando su muerte inmediata.

Caicedo quien según voceros de la comunidad era un hombre que desde temprana edad había luchado por el bienestar y la justicia de su región (Bajo Patía), contaba con esquema de seguridad asignado por la Unidad Nacional de Protección desde el 2017, luego de sobrevivir a un atentando, que lo llevo a desplazarse de Nariño luego de  recibir 10 amenazas en su contra.

Con el asesinato del Nilson Caicedo quedan dos menores de edad, y una joven de 20 años, sin comprender quien le arrebato la vida a su padre. (Le puede interesar:[Líderesa cultural Lucy Villarreal fue asesinada en Llorente-Tumaco](https://archivo.contagioradio.com/lideresa-cultural-lucy-villarreal-fue-asesinada-en-llorente-tumaco/))

Meses antes de su fallecimiento el líder  habría perdido tres hermanos que junto a él integraban el Consejo Comunitario de la región; aún no se conocen los responsable del  homicidio, pero la investigación ya se encuentra en manos de la Fiscalía. (Le puede interesar:[10 mil millones de pesos le adeudan a los maestros de Chocó](https://archivo.contagioradio.com/10-mil-millones-de-pesos-le-adeudan-a-los-maestros-de-choco/))

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
