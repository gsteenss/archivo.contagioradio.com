Title: Acción Ecológica: 30 años rompiendo la dominación con imaginación
Date: 2016-10-19 12:16
Category: CENSAT, Opinion
Tags: Ambientalismo, Ambiente, ecuador
Slug: accion-ecologica-30-anos-rompiendo-la-dominacion-con-imaginacion
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/10/AccionEcologica2.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

#### **[Por: CENSAT Agua Viva /Amigos de la Tierra Colombia](https://archivo.contagioradio.com/censat-agua-viva/)** 

###### 18 Oct 2016 

El 18 de octubre, se celebran en la ciudad de Quito los 30 años de Acción Ecológica, organización ecologista ecuatoriana que durante tres décadas ha dedicado su trabajo -con coraje, creatividad e inteligencia- a la protección de la riqueza social y natural de ese país suramericano. En una ceremonia en la Universidad Central de Ecuador, Acción Ecológica recordará el trabajo que ha compartido con movimientos sociales, de dentro y fuera de su país, en  defensa de territorios amenazados por el avance del extractivismo, por la mercantilización de la naturaleza, por los agronegocios, por la construcción de represas e infraestructura, y destacará los aportes de los y las defensoras del territorio, otorgando un reconocimiento especial por sus luchas ambientales a 30 personas y organizaciones tanto ecuatorianas como de otras latitudes.

Quienes hemos transitado en el ambientalismo popular (ecologismo en Ecuador), reconocemos el legado de Acción Ecológica en sus tres décadas de existencia. Sus debates han contribuido a nutrir el debate internacional del ecologismo popular, ahondando sobre la necesidad de romper las relaciones coloniales y de dominación de la sociedad sobre la naturaleza, e invitando a propiciar, con imaginación e inventiva, transformaciones estructurales para garantizar un futuro para nuestros hijos e hijas.

Junto con el movimiento indígena, Acción Ecológica influyó para que la Constitución ecuatoriana reconociera el buen vivir o Sumak Kawsay como eje central, y también asuntos cruciales como los derechos de la naturaleza, la soberanía alimentaria, el derecho al agua y a la resistencia, entre otros.

Acción Ecológica, organización fundada por varias mujeres y en la que han participado fundamentalmente mujeres, es reconocida por su trabajo en la defensa de la Amazonía y de sus pueblos contra empresas petroleras que desde los setenta destruyeron las selvas amazónicas del norte de Ecuador. Su Campaña Amazonía por la Vida propició una de las luchas más emblemáticas de pueblos locales contra empresas petroleras poderosas, en este caso la Texaco (hoy Chevron Texaco), dando lugar a uno de los juicios más importantes en el mundo contra empresas transnacionales. También la organización ecologista ecuatoriana ha jugado un papel crucial en la construcción y dinamización de articulaciones internacionales en defensa del territorio, como OILWATCH.

En tiempos más cercanos, con la Campaña por el Yasuní, lanzaron una osada propuesta al mundo: dejar el crudo del Parque Natural Yasuní en el subsuelo, e invitaron al pueblo ecuatoriano a priorizar la transformación energética del país, lo que significaba romper la dependencia económica que el Ecuador tiene con el petróleo.

En sus primeros años, el gobierno de Rafael Correa acogió y adaptó la propuesta y la llamó Iniciativa Yasuní, proponiendo a los gobiernos de los países responsables históricos de la crisis climática que garantizaran los recursos económicos para que Ecuador pudiese dejar el crudo en el subsuelo, y de esta manera se protegiera el Parque Nacional Yasuní y los pueblos indígenas Huaoraní, que allí viven en aislamiento voluntario.

Sin embargo, el gobierno no fue apoyado lo suficiente por dichos países y poco a poco se distanció de las propuestas ecologistas, abandonando la oportunidad histórica para que Ecuador liderará en el mundo la transición hacia una sociedad sustentable. Más aún, en 2009 un decreto ministerial ordenó el cierre de Acción Ecológica. La respuesta fue una abrumadora solidaridad internacional; personalidades como Eduardo Galeano y organizaciones de todo el mundo se pronunciaron y obligaron al gobierno de Correa a recular. Más adelante, la propuesta del Yasuní fue sepultada y este territorio rico en biodiversidad fue entregado para la explotación petrolera. Recientemente inició su explotación.

Los planteamientos de Acción Ecológica son sin duda un insumo importante para el ambientalismo popular colombiano, que busca nutrir no sólo las luchas antiextractivas, sino también el movimiento nacional que hoy reclama la posibilidad de una salida negociada al conflicto social y armado. Nuestra obligación, como lo ha hecho Acción Ecológica en su país, es poner en juego las propuestas que el ambientalismo y el movimiento en defensa de los territorios colombianos hemos construido en décadas. Es urgente hacer visibles y viables las propuestas que tienen miles de acueductos comunitarios que gestionan sus aguas, las múltiples experiencias agroecológicas que custodian las semillas frente al poder multinacional que busca controlarlas y homogenizarlas, las bellísimas experiencias de gestión de los territorios que cuidan y conservas selvas y páramos, las cientos de dinámicas urbanas que promueven la movilidad y el hábitat sustentable, los aportes del movimiento animalista que reclama, con justeza, derechos para los animales.

Juntar pensamiento y acción ecologista es algo que nos ha enseñado Acción Ecológica, hoy recordamos más que nunca sus enseñanzas y nos unimos a la celebración que el mundo ambientalista les hace.

------------------------------------------------------------------------

###### Las opiniones expresadas en este artículo son de exclusiva responsabilidad del autor y no necesariamente representan la opinión de la Contagio Radio. 
