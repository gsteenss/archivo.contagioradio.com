Title: Gobierno debe fortalecer su equipo en la mesa de diálogos en Quito: De Currea
Date: 2017-12-06 16:26
Category: Nacional, Paz
Tags: ELN, Jaun Camilo Restrepo, Quito
Slug: gobierno-debe-fortalecer-su-equipo-en-la-mesa-de-dialogos-en-quito
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/02/quito.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: La Prensa] 

###### [06 Dic 2017] 

El equipo de conversaciones de Paz del Gobierno Nacional, en el proceso que se adelanta en Quito, tendrá que seguir con modificaciones luego de la renuncia de Juan Camilo Restrepo, jefe del equipo, y la del General Herrera Berbel. Para Víctor de Currea, esta situación **hace parte de la normalidad del diálogo y no es cierto que esas salidas pongan en riesgo la mesa, sino que son una oportunidad para fortalecer el equipo del gobierno.**

De Currea señaló que lo importante en este momento es pensar como avanzará la mesa y el siguiente pasó **es pensar en la restructuración del equipo de acuerdo a los resultados que esperan obtener hasta Agosto.** Debería haber un equipo con más asesores técnicos que permita una cohesión que proponga y redacte documentos en temas como la participación o la garantía de democracia.

**“Cuántos documentos conjuntos ha producido la mesa**” pregunta el analista y agrega que justo para que se concreten puntos en el proceso deben convocarse personas que respondan a las necesidades de la mesa, así se evitaría que la mesa se rompa por el desgaste natural cuando no son claros los avances sustanciales. (Le puede interesar: ["ELN plantea la posibilidad de exter el cese bilateral en 2018"](https://archivo.contagioradio.com/no-se-puede-postergar-la-idea-de-la-paz-alejandra-gaviria-a-30-anos-del-asesinato-de-su-padre/))

### **¿Cómo blindar el proceso de paz de cara a un nuevo gobierno?** 

Sobre el cese bilateral y las probabilidades de que este continúe, De Currea manifestó que tanto al gobierno como al ELN les interesa continuar con el cese, pero que para ello debe existir voluntad de continuar por parte de ambas partes, sin embargo, también se encuentra la traba de las elecciones 2018 en las que algunos candidatos **como Alejandro Ordoñez ya han manifestado su interés por finalizar este proceso.**

<iframe id="audio_22511019" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_22511019_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

Según De Currea, el blindaje del proceso no solamente corresponde a quienes están en la mesa de conversaciones, sino que hay gran parte de responsabilidad en el ejercicio del voto por parte de los electores y hace la invitación a votar de manera consiente, tanto en la elección de parlamento como en las presidenciales.
