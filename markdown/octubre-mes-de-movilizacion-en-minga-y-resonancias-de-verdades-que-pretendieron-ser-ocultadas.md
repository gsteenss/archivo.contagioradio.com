Title: Octubre: Mes de movilización en Minga y resonancias de verdades que pretendieron ser ocultadas
Date: 2020-12-31 12:25
Author: CtgAdm
Category: Actualidad, Especiales Contagio Radio, Resumen del año
Tags: Minga Indígena, Minga Nacional, Minga Nacional por la Vida, Movilización social, Movilizaciones Bogotá, Octubre, Represión, Resumen de noticias
Slug: octubre-mes-de-movilizacion-en-minga-y-resonancias-de-verdades-que-pretendieron-ser-ocultadas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/12/disenos-para-notas-2-2.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

En Octubre vivimos una de las mas grandes movilizaciones del año con la llegada de la Minga Indígena a Bogotá, después del desplante del presidente Duque quien se negó a dialogar en el departamento del Cauca. A esta minga se sumaron comunidades negras, comunidades campesinas, organizaciones sociales y movimientos ciudadanos que se cansaron de esperar por las promesas del gobierno y decidieron tomarse de nuevo las calles. La minga fue una expresión de la dignidad.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

  
Sin embargo, a pesar de la fuerte movilización ciudadana el gobierno siguió con sus oídos sordos. Informes como el del Centro de Investigación y Educación Popular CINEP dieron cuenta de la desidia del gobierno frente a situaciones que son innegables. FFMM, Policía y Paramilitares se mantienen como los primeros violadores de DDHH en Colombia, mientras el gobierno se sigue negando a hacer las reformas que son necesarias.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

  
Otra de las discusiones que se han dado es en torno a las verdades que han pretendido ser ocultadas. Fieles a su compromiso con la verdad plena, el partido político FARC reconoció, entre otros, su autoría en el asesinato de Álvaro Gómez Hurtado, un aporte a la verdad que no fue bien recibido por la institucionalidad que se niega a reconocer que la impunidad está y es rampante, que la fiscalía no ha operado para el esclarecimiento del 98% los crímenes cometidos en el país y que se han construido “verdades” con mentiras de acuerdo a los intereses políticos de los sectores de poder.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### ["La Minga es una expresión de unidad en medio de la violencia"](https://archivo.contagioradio.com/la-minga-es-una-expresion-de-unidad-en-medio-de-la-violencia/)

<!-- /wp:heading -->

<!-- wp:paragraph -->

Luego de que este 12 de octubre La Minga, ingresara a la Ciudad de Cali con el fin de tener una reunión con el presidente Ivan Duque, **cita que fue incumplida y qué llevo a que miles de personas tomarán la decisión de movilizarse hacia Bogotá en búsqueda de una respuesta **directa por parte del Gobierno Nacional.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El arribo de más de 10.000 personas, luego de una recorrido durante 5 días desde el occidente del país hasta la capital colombiana, se espera que sea este lunes 19 de octubre. ([Avanza la Minga en defensa de la vida](https://archivo.contagioradio.com/la-minga-sera-en-defensa-de-la-vida-el-territorio-la-democracia-y-la-paz/))

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### [Policía, paramilitares y ejército son los mayores violadores de DDHH según el CINEP](https://archivo.contagioradio.com/primer-semestre-2020-mayores-violaciones-dd-hh-cometidas-policia/)

<!-- /wp:heading -->

<!-- wp:paragraph -->

El Centro de Investigación y Educación Popular –[CINEP](https://www.cinep.org.co/Home2/)– publicó su más reciente informe sobre Derechos Humanos, Derecho Internacional Humanitario y violencia política en Colombia; el cual arrojó como hallazgo **un recrudecimiento en la situación del país en estas materias respecto a estudios anteriores realizados por el mismo instituto**. Preocupa la aparición de la Fuerza Pública encabezando la lista de los actores responsables de dichas violaciones.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### [Tras la verdad, hay un sector que no quiere perder el poder del control ideológico](https://archivo.contagioradio.com/sector-teme-verdad-no-quiere-perder-poder-control-ideologico/)

<!-- /wp:heading -->

<!-- wp:paragraph -->

En medio de una conversación entre **el comisionado de la Verdad, Saúl Franco, el fotoperiodista Jesús Abad Colorado y el politólogo Álvaro Villarraga** a la luz de las revelaciones hechas por el Partido FARC sobre el asesinato de Álvaro Gómez Hurtado, la creciente pérdida de la esperanza de las víctimas que exigen a la JEP resultados y el difícil acceso que se ha tenido a la información por parte de la [Comisión de la Verdad,](https://comisiondelaverdad.co/) los tres panelistas coinciden en que el debate no se centra en una pugna por la verdad, sino por el control ideológico de la historia del país y el temor de la élites e instituciones a que les sea arrebatado dicho poder.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

[Entre a nuestra página de facebook aqu](https://www.facebook.com/contagioradio)í

<!-- /wp:paragraph -->
