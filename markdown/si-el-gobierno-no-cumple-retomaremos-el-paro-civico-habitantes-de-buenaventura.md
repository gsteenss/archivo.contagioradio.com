Title: Si el Gobierno no cumple retomaremos el paro cívico: habitantes de Buenaventura
Date: 2019-02-14 10:36
Author: AdminContagio
Category: Comunidad, DDHH
Tags: Paro Cívico de Buenaventura, Plan Nacional de Desarrollo
Slug: si-el-gobierno-no-cumple-retomaremos-el-paro-civico-habitantes-de-buenaventura
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/02/4103147Paro-Buenaventura-EFE-770x400.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Archivo 

###### 13 Feb 2019 

[El pasado 11 de febrero, el comité del **Paro Cívico de Buenaventura** realizó un balance sobre logros y avances del acuerdo alcanzado con el Gobierno en el marco del paro cívico del 2017, a pesar que se han visto mejoras, el comité advierte que no se ha visto la disposición del gobierno Duque de incorporar tales compromisos con Buenaventura en el Plan  Nacional de Desarrollo.]

**Los avances no son los esperados **

**María Miyela Riascos, lideresa del Barrio Independencia Buenaventura** señala que aunque sí se han visto mejoras en materia de salud, educación, saneamiento básico y agua potable,  los avances[ no han sido como la comunidad de Buenaventura lo esperaba y lo planteaba  y muchos otros solo verán el resultado final hasta que los proyectos sean finalizados por los contratistas. ]

[Por otro lado en materia de salud, la Alcaldía del distrito se comprometió a la apertura del hospital de segundo nivel no obstante ha pasado un año y medio y los servicios de salud prestados por el hospital, no corresponden a la atención que debería ser brindada, **"hay unos cumplimientos parciales y otros muy demorados"** explica María Miyela quien denuncia que algunos funcionarios no tienen voluntad política.]

En cuanto al sistema educativo, se lograron ganar más de 100 plazas de nombramientos de maestros, sin embargo la lideresa denuncia que dichas plazas fueran feriadas mientras que en temáticas como ambiente no se han visto avances de ningún tipo en el municipio. [(Le puede interesar: Amenazan a dos líderes sociales del Paró Cívico de Buenaventura) ](https://archivo.contagioradio.com/amenazan-a-dos-lideres-sociales-del-paro-civico-de-buenaventura/)

### **Ausentes en el Plan de Desarrollo Nacional ** 

[La lideresa indica que existe un billón de pesos que se encuentran en ejecución y que con cuya inversión se busca cerrar las brechas de igualdad en Buenaventura durante diez años, sin embargo para que estos recursos sean implementados y financiados, la cifra debe quedar contemplada en el **Plan Nacional de Desarrollo**, lo cual no se ha visto reflejado.]

[Frente a esta situación, MarÍa Miyela expresa las dificultades que han existido para asegurar los recursos de Buenaventura, realizando ejercicios de incidencia, contactando a los congresistas del Valle del Cauca y solicitando a través de una carta al presidente Iván Duque una reunión, petición que hasta entonces no ha sido respondida.]

[La lideresa asegura que no de ser incluidos en el Plan de Desarrollo Nacional retomarán el paro cívico pues **“han aprendido que  la única forma que escuchan a este pueblo es a través de la vía de hecho y si no nos dejan otra opción, nos veremos en las calles con el presidente, el ESMAD y todos los que nos manden”**.]

De igual forma, [desde el comité fue inscrito en la Registraduría Víctor Hugo Vidal a la candidatura del municipio para las elecciones regionales de octubre de este año representando al movimiento Buenaventura con Dignidad.]

<iframe id="audio_32557266" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_32557266_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
