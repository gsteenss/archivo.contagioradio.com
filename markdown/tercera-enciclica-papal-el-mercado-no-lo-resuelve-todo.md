Title: Tercera encíclica papal "El mercado no lo resuelve todo"
Date: 2020-10-06 18:31
Author: AdminContagio
Category: Actualidad
Tags: Encíclica papal, Papa Francisco
Slug: tercera-enciclica-papal-el-mercado-no-lo-resuelve-todo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/09/papafrancisco.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

Este sábado 3 de octubre, el papa Francisco llegó a la ciudad de Asís, en Italia; [allí firmó la encíclica](https://drive.google.com/file/d/1AIRwHKKVtH7nVdFSHwmlMUstpiBV1tSH/view), documento que recoge el legado de Francisco, llamado **«Fratelli** T**utti» (Hermanos Todos) cuyo contenido, dividido en ocho capítulos, habla de la fraternidad y la amistad social** y resalta que «la fraternidad no es una moda que se desarrolla en el tiempo, si no la manifestación de actos concretos».

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Asimismo, la encíclica destaca que la crisis del mundo actual ha servido para demostrar que «nadie se salva solo» y que hay que soñar «como una única humanidad» en la que son «todos hermanos». (Le puede interesar: [Acuerdo de paz tiene herramientas para frenar la violencia en Colombia: ONU](https://archivo.contagioradio.com/onu-gobierno-acudir-a-mecanismos-del-acuerdo-para-combatir-violencia/))

<!-- /wp:paragraph -->

<!-- wp:heading -->

«El mercado no resuelve todo"
-----------------------------

<!-- /wp:heading -->

<!-- wp:paragraph -->

El papa, también se expresa con respecto al mercado y al neoliberalismo y señala que **l**a fragilidad de los sistemas mundiales frente a la pandemia ha evidenciado que **«el mercado solo no resuelve todo, aunque otra vez nos quieran hacer creer este dogma de fe neoliberal. Se trata de un pensamiento pobre, repetitivo, que propone siempre las mismas recetas frente a cualquier desafío que se presente».**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

La solución, cree Francisco radica en «volver a llevar la dignidad humana al centro y que sobre ese pilar se construyan las estructuras sociales alternativas que necesitamos».

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Adicionalmente, afirma que los conflictos y el desinterés por el bien común son «instrumentalizados por la economía global para imponer un modelo cultural único. Esta cultura unifica al mundo pero divide a las personas y a las naciones».  

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Igualmente, rechaza conceptos como el populismo, el consumismo, la globalización despiadada y el individualismo, indicando que debemos aplicar el modelo del «buen samaritano» para que la sociedad «se encamine a la prosecución del bien común y, a partir de esta finalidad, reconstruya una y otra vez su orden político y social, su tejido de relaciones, su proyecto humano».

<!-- /wp:paragraph -->

<!-- wp:heading -->

**Encíclica habla de males como el racismo y la desigualdad**
-------------------------------------------------------------

<!-- /wp:heading -->

<!-- wp:paragraph -->

El Pontífice también habla de los grandes males que se han intensificado en estos tiempos de crisis son el desempleo, el racismo, la pobreza, la desigualdad de derechos, como la esclavitud, la trata y el tráfico de órganos.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En el segundo y cuarto capítulo de la encíclica el **papa se expresa en relación a los migrantes, insistiendo que los derechos no tienen fronteras y «nadie puede quedar excluido, no importa dónde haya nacido, y menos a causa de los privilegios que otros poseen porque nacieron en lugares con mayores posibilidades».**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Y lamenta que además de suscitar alarma, miedo y xenofobia, «desde algunos regímenes políticos populistas como desde planteamientos económicos liberales, se sostiene que hay que evitar a toda costa la llegada de personas migrantes».

<!-- /wp:paragraph -->

<!-- wp:quote -->

> **«No se advierte que, detrás de estas afirmaciones abstractas difíciles de sostener, hay muchas vidas que se desgarran».**
>
> <cite>Papa Francisco</cite>

<!-- /wp:quote -->

<!-- wp:paragraph -->

Ante esto, expone que la fraternidad debe promoverse con hechos. Hechos que se concreten en la «mejor política», pues al servicio del bien de todos. 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El séptimo capítulo, «caminos de reencuentro» se centra en el perdón sin olvido, la reconciliación y la paz, que «es un compromiso constante en el tiempo. Es un trabajo paciente que busca la verdad y la justicia, que honra la memoria de las víctimas y que se abre, paso a paso, a una esperanza común, más fuerte que la venganza». (Le puede interesar: [Rodrigo Londoño se comprometió con el «Movimiento Nacional por la Verdad»](https://archivo.contagioradio.com/rodrigo-londono-se-comprometio-con-el-movimiento-nacional-por-la-verdad/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Pero también señala que la guerra no es «un fantasma del pasado» sino «una amenaza constante” por lo que hay que afirmar «¡Nunca más la guerra!»

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
