Title: La artesanía indígena del resguardo Wounaan de Pichima Quebrada
Date: 2016-08-15 13:17
Category: Comunidad, Paz
Tags: Artesanía ancestral, comunidad Wounaan, Conpaz, Educación Propia
Slug: la-artesania-indigena-del-resguardo-wounaan-de-pichima-quebrada
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/08/artesania-pichima.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Bernardino Dura-CONPAZ] 

###### 3 Ago 2016

El tejido de artesanía es como actividad humana, como experiencia integral de vida, como pensamiento que interaccionan el medio ambiente con las necesidades física y espirituales que el hombre experimenta, comparte y transforma en objetivos útiles y estéticos, al aplicar los conocimientos técnicas que han sido adquiridos a través del tiempo y de la comunicación históricos de la convivencia de la comunidad de Pichimá Quebrada

Una anciana Licenia Opua, una de las fundadora y líder de Pichimá Quebrada, tenía en su casa una especie de cántaro hecho en werregues; era pequeñito poseía tapa y había sido con hilos grueso en la técnica espiral. Nuestros ancestros utilizaban cantaros para guardar anzuelo, agujas, monedas y todas clases de objetos pequeños y también los hombres utilizaban para amarrar los guayucos. A partir de allí las mujeres Wounaan animaron a mejorar el tejidos y a pulir los cantaros de werregues para venderlos como artesanía, haciendo de mayor tamaño con tejido más tupidos, diseños autóctonos y procesando tintas naturales para pintarlas de colores.

La técnica de elaboración de los canastas de werregues se fue extendiendo por todos el bajo san juan a medida que los jóvenes de Pichimá se casaban en otros comunidades y se quedan viviendo en el resguardo. Así ellas empezaron a enseñar a su suegra, cuñada, han mejorado de diseñar platos, tazas, bandejas, bolsa y fruteros.

Los cantaros de werregues se perfeccionaron con el apoyo de un proyecto de la Asociación colombiana de promoción Museo de Artes y Tradiciones populares y el Banco interamericano de Desarrollo BID, en el año 1992, a partir de ese año el cantaros de werregues   empieza a comercializar en el territorio de Colombia y a otro país.

Uno de los más importantes es la palma de werregues. En el choco por lo general crecen solas, pero al igual que nosotros, los Wounaan, tienen a sus familias, papas  y abuelos, que los cuidan y permanecen cerca de ellas. Además de utilizarla para elaborar las artesanías como plantas medicinales, sobre todo por los jaibanas en los rituales de curación de enfermos, el tallo utiliza para la elaboración de trapiche y utiliza mucho para hacer horcones, son bases para sostener el techo de las  viviendas.

La recolección de cogollos  realizan generalmente los hombres, también se compran o se cambian por productos. Para cosechar el cogollo sin tumbar la palma, nació la idea de utilizar la herramienta llamada “media Luna”. Después de cosechar el cogollo viene la limpieza y selección. La fibra delgada se lava con jabón, se agrupan en más o menos de 20 manojitos y se ponen a secar al sol para descolarlos.

Anteriormente la cestería era elaborada en fibra natural, pero cuando se empezó a distribuir y los artículos de werregues se hicieron más populares, el ingenio y a creatividad de nuestra mujeres las levo a dar color a las fibras para hacerlos más llamativas y producir nuevos diseño  a la comercialización de la artesanía nacionales e internacionales.

###### *[![bernardino](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/08/bernardino-150x150.jpg)

###### Como parte del trabajo de formación de Contagio Radio con comunidades afros, indígenas, mestizas y campesinas en toda Colombia, un grupo de comunicadores realizó una serie de trabajos periodísticos recogiendo las apuestas de construcción de paz desde las comunidades y los territorios. Nos alegra compartirlas con nuestros y nuestras lectoras. 
