Title: "La temporada de mala calidad de aire en Bogotá hasta ahora comienza"
Date: 2020-02-19 14:12
Author: CtgAdm
Category: Ambiente
Tags: Día sin carro, oxigeno, paro camioneros
Slug: la-temporada-de-la-mala-calidad-de-aire-en-bogota-hasta-ahora-comienza
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/02/Paro-camioneros.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

*Foto: Agencia noticias UN*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Ante la alerta amarilla sobre el nivel del aire en Bogotá el Distrito expidió del **[Decreto 840 de 2019](http://www.andi.com.co/Uploads/Decreto%20840%202019.pdf),** el cual restringe la circulación de camiones y volquetas de carga en la ciudad, especialmente a aquellas que superen los 20 años de servicio y los 8.500 kilogramos.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Ante esta restricción propietarios y conductores de estos vehículos han bloqueado distintas calles de la capital, en horas de la tarde del 17 de febrero, exigiendo una unas mesas de trabajo con la Alcaldía Mayor que permitan llegar a un acuerdo que beneficiar ambas partes.

<!-- /wp:paragraph -->

<!-- wp:image {"id":80918,"sizeSlug":"large"} -->

<figure class="wp-block-image size-large">
![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/02/Paro-camioneros.jpg){.wp-image-80918}  

<figcaption>
*Foto: Cablenoticias*  

</figcaption>
</figure>
<!-- /wp:image -->

<!-- wp:heading {"level":3} -->

### “El aire que entra a Bogotá ya no es limpio"

<!-- /wp:heading -->

<!-- wp:paragraph -->

Según Luis Belalcázar, profesor de la Universidad Nacional, e investigador de la calidad del aire las causas principales de la mala calidad de este en Bogotá son varias. *"las emisiones de los vehículos y efectivamente los camiones son los que generan la mayor parte de la contaminación en la capital"*. (Le puede interesar: <https://archivo.contagioradio.com/intereses-en-el-pico-y-placa-de-bogota/>)

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Sin embargo afirmó que las condición meteorológica en esta temporada son mucho más complejas*, "tenemos inversiones térmicas y meteorológicas que no está ayudando a dispersar los contaminantes; además la contribución desde las regiones, es decir el aire que entra a Bogotá, ya no es limpio viene contaminado debido a los incendios forestales"*.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Los camiones y los buses, sí son responsables de la calidad del aire en Bogotá

<!-- /wp:heading -->

<!-- wp:paragraph -->

Según Edder Velandia, profesor de la Universidad de La Salle y experto en alternativas de movilidad uno de los principales factores que afectan la calidad del aire son los vehículos de más de 15 años de servicio y además que no tienen una debida atención mecánica.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Ante esto Belalcázar afirmó que se debe hacer una restricción más fuerte cuando los indicadores señalen naranja o amarillo, "*en estos casos no sólo se debe cohibir a ese polígono, también a diferentes tipos de vehículos a nivel local y regional en lugares como Soacha, Mosquera y Madrid, cuya emisión afecta los indicadores".*

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### No hay que pensar solo en las afectaciones y soluciones a corto plazo

<!-- /wp:heading -->

<!-- wp:paragraph -->

Según el experto en aire, hay acciones a largo y corto pazo, una de las mas importantes y que implicará más tiempo y dinero es la renovación del parque automotor *"hay volquetas que tienen muchísimos años funcionando y las emisiones son claramente visibles, la solución mas simple es renovarlas, sin embargo el primer paso es trabajar en políticas transparentes"*.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Estas normas se refieren principalmente al programa de revisión técnico mecánica, *"hay que ajustarlo porque no está funcionando, y es vulnerable a la corrupción en la manipulación de las pruebas, aquí el objetivo no es tener un certificado, sino un vehículo realmente en condiciones aptas de seguridad vial y protección a la salud".*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por su parte Velandia agregó, *"hay que reconocer que si bien hay estrategias para mitigar la contaminación del aire, también deben existir unas que permitan ofrecer oportunidad de seguir trabajando a los transportadores"*. (Le puede interesar:<https://archivo.contagioradio.com/aire-de-bogota-en-alerta-roja/>)

<!-- /wp:paragraph -->
