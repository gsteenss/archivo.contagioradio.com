Title: Ocho activistas podrían ir a prisión por defender DDHH Palestinos
Date: 2019-01-14 15:03
Author: AdminContagio
Category: El mundo, Otra Mirada
Tags: BDS, Palestinos, Valencia
Slug: derechos-palestinos-activistas-prision
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/01/enfrentamos.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Sonia Ferré 

###### 14 Ene 2018 

Ocho integrantes del movimiento **Boicot Desinversiones y Sanciones a Israel (BDS)** en Valencia han sido encausados bajo la acusación de realizar un **boicot en contra del artista norteamericano de ascendencia judía Matisyahu**, quien en agosto de 2015 se presentó en el Festival Rototom Sunsplash de Benicàssim, uno de los eventos de música reggae de mayor trascendencia en Europa.

Carlos Marcos, integrante del BDS  y uno de los encausados, asegura que **no se trató de un boicot al artista, sino de una petición al Festival para que no cantara**, argumentando que ha defendido las masacres a Gaza; subvencionó uno de los conciertos mas grandes que hace el ejército de Israel anualmente para recaudar fondos y por **justificar la masacre de 9 personas en la llamada Flotilla de la Libertad**, ocurrida el 31 de mayo de 2010.

El rechazo por parte del movimiento a la presentación de Matisyahu en el evento, radicaba en que **el Rototom se ha caracterizado por ser un festival en el que siempre habla de los derechos humanos, de la paz y del amor**, que tiene un contenido político y "una estructura de ideas" como asegura Marcos, que además tiene un foro social que cataloga como "uno de los más potentes de Europa y el mundo".

Como un detalle particular, el activista señala que **a este escenario el año anterior fue invitado David Segarra, periodista español que hizo parte de la Flotilla de la libertad**, por lo que resulta paradójico que se invitara a un artista que había defendido el mismo ataque a las embarcaciones que componían la coalición internacional.

[**La presión del lobby israelí**]

**La campaña de presión al artista había iniciado en redes sociales**, donde se le pedía que tomara posición por los derechos humanos, particularmente frente a la situación de los palestinos que viven en ocupación. Al no encontrar respuesta por parte del artista, y recordando la posición política del evento, **desde el mismo Festival procedieron a solicitar a Matisyahu asumir una postura frente al tema**.

Según Marcos, la solicitud hecha por los organizadores del evento **desató una respuesta por parte del lobby israelí**, desde el Ministerio de relaciones exteriores de Israel, algunos medios de comunicación así como desde Israelíes en Estados Unidos, presiones que **coaccionaron al Festival para que volviera a la idea inicial de permitir la presentación del músico**.

**Lo que sigue en el proceso **

Como explica el integrante del BDS Valencia, e**l proceso se encuentra en fase de instrucción**, a la espera de que un juez determine si la causa sigue adelante para ir a juicio, donde **podrían enfrentar hasta cuatro años de cárcel**. A los activistas se les acusa por coacción, amenazas y por **delitos de odio**, **estos últimos considera Marcos podrían orientar la decisión del togado**.

Al haber tenido lugar la presentación del artista en el Festival sin algún tipo de inconveniente, Marcos asegura que **las acusaciones por coacción y amenazas quedarían desmontadas**, sin embargo se buscaría comprobar que existe una incitación al odio por el amplio espectro que cubre este tipo de delito, en el que a su juicio no se defiende a las minorías.

Las evidencias que se han presentado hasta ahora consignadas en 12 mil folios, constan básicamente de las **publicaciones y perfiles en redes sociales de los activistas**, sin que desde las mismas se compruebe la pertenencia de las cuentas a los encausados; y **sin evidencias de algún tipo de amenaza o acto violento** antes, durante o después del evento.

**Solidaridad y apoyo **

Frente a su situación Carlos Marcos destaca la solidaridad que han recibido de diferentes lugares del mundo afirmando que **"las muestras de apoyo están siendo brutal"**, añadiendo que "si nosotros tenemos la oportunidad de poner en boca de todos el tema de Palestina estaremos orgullosos de hacerlo, y benditas sean las redes sociales si consiguen esto pero hay que tener la fijación de que el pueblo palestino es lo importante".

El pasado 12 de enero se realizó en el Palau del Exposició, un **acto por la libertad de expresión y contra la criminalización de la solidaridad con Palestina**, que contó con la participación de artistas y activistas por los Derechos Humanos. (Le puede interesar: [Revocan premio que entregarían a Angela Davis por su apoyo a Palestina](https://archivo.contagioradio.com/revocan-premio-angela-davis-apoyo-palestina/))

A través de redes sociales, **la campaña de solidaridad con los encausados por la libertad de expresión contra la criminalización de la solidaridad con Palestina Defensem DDHH**, vienen invitando a apoyar la causa difundiendo el comunicado proferido por los activistas, ajunto a continuación.

[Comunicado Ddhh Cas](https://www.scribd.com/document/397453665/Comunicado-Ddhh-Cas#from_embed "View Comunicado Ddhh Cas on Scribd") by [Contagioradio](https://www.scribd.com/user/276652802/Contagioradio#from_embed "View Contagioradio's profile on Scribd") on Scribd

<iframe id="audio_31472226" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_31472226_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)
