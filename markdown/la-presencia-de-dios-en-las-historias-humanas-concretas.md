Title: La presencia de Dios en historias concretas
Date: 2019-11-18 15:21
Author: A quien corresponde
Category: A quien corresponda, Opinion
Tags: cristianismo., Cristianos, dios, historia, Religión
Slug: la-presencia-de-dios-en-las-historias-humanas-concretas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/carretera.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

Examínenlo todo y quédense con lo bueno  
1(1Tesalinisenses 5,21)

Bogotá, 19 de octubre del 2019

*Diferenciar entre información y opinión*  
*es necesario para una buena decisión*

Estimado

Hermano en la fe  
Cristianos, cristianas, personas interesadas

Cordial saludo,

 

Tenemos comprensiones diferentes de la religión cristiana, que nos llevan, a reconocer o rechazar, la revelación o manifestación de Dios en la realidad y que pide a los creyentes actuar para cambiarla y superar el sufrimiento, la miseria y la destrucción de la vida humana y del planeta.

Creo que, por esta razón, cuando te hablaba de la realidad humana y social y sus causas, de la responsabilidad de los creyentes por acción u omisión en lo que pasa en el mundo, me interrumpiste alegando que era cuestiones políticas en las que la religión no tiene nada que ver. Olvidas que santos y santas, renovadores y renovadoras eclesiales, fundadores de expresiones eclesiales y congregaciones religiosas, líderes espirituales de todas las iglesias y religiones han descubierto la *“voluntad”* de Dios en la realidad concreta, dentro y fuera del mundo religioso, para responderle cambiaron de vida, tomaron decisiones profundas, generaron rupturas, crearon nuevas formas de vivir la experiencia de fe y crearon grupos religiosos y comunidades que se ocuparon de realidades sociales.

Los evangelios muestran a Jesús actuando en la historia concreta de su época y de su región, interactuando con todo tipo de personas, preocupándose por sus necesidades físicas y espirituales, materiales y *“psicológicas”*, llamándolos a actuar con justicia, amor y paz, invitándolos a descubrir en su manera de actuar y de relacionarse la presencia del Reino de Dios, cuestionándolos porque sabían distinguir las señales del cielo pero no sabían distinguir (discernir) los signos de los tiempos *(Mateo 16,1-4).* En otras palabras, porque no querían entender que la manera concreta de vivir, de relacionarse, hablar y de actuar de Jesús, era la presencia de Dios en nuestra historia, era la Palabra de Dios hecha carne, habitando en nuestra realidad *(Juan 1,14)*.

Para hacerme entender, comparto dos experiencias recientes, en las que hay mezclas de dolor, de sufrimiento, de esperanza y de fe, que actualizan la vida, pasión, muerte y resurrección de Jesús de Nazaret en nuestra historia.

##### La primera experiencia: 

[Fue la participación en el encuentro de la *Red Iglesias* y Minería, realizada ]en Buenos Aires, donde estuvieron presentes delegados y delegadas de toda América Latina, víctimas de la minería contaminante y del extractivismo y quienes las acompañan. En el encuentro escuchamos las experiencias dolorosas de quienes defienden sus derechos ante las amenazas de las grandes empresas mineras, reconocimos la fe que las fortalece en la defensa de la vida y el territorio y que les alimenta la esperanza en medio del conflicto; conocimos información especializada sobre la magnitud de la contaminación del agua, del aire, la agricultura y las consecuencias destructivas para los bosques, los ríos y la vida humana, especialmente de los pobres y marginados; oramos con las fotos de **274** de las víctimas del desastre humano y ambiental de la ruptura de una represa de desechos contaminantes de multinacional Vale, en Brumadinho, Mina Gerais, Brasil; realizamos celebraciones litúrgicas profundas y significativas en las que nos colocamos en actitud de escucha para descubrir lo que Dios quiere que hagamos en esa realidad.

(le puede interesar:[La actitud contradictoria de muchos creyentes ante la reconciliación](https://archivo.contagioradio.com/la-actitud-contradictoria-de-muchos-creyentes-ante-la-reconciliacion/))

**Los ríos contaminados, las selvas destruidas, el aire envenenado, las comunidades dividas y rotas **son la expresión de una lógica humana que está destruyendo la obra de Dios y colocando en riesgo la sobrevivencia de la vida humana y del planeta. Por otro lado, las miles de personas y los cientos de organizaciones defienden la naturaleza y los pueblos y que animadas por su espiritualidad arriesgan la vida, renuncian a intereses personales y “comodidades” y se suman a la corriente que defiende vida, muestran su profunda experiencia de Dios en esta historia concreta y contradictoria.

##### La segunda experiencia: 

Fue el encuentro con *Mirta Acuña* de Baravalle, Madre de la Plaza de Mayo línea fundadora, quien junto a otras 14 madres hace 42 años, un jueves empezaron a caminar alrededor de la Plaza de Mayo, compartiendo el dolor y la incertidumbre por la desaparición forzada de sus hijos (*La dictadura militar había prohibido reuniones de más de tres* *personas en el espacio público)*, aún hoy, continúan caminando, cada jueves alrededor de la plaza, buscando sus hijos, exigiendo, irrenunciablemente, verdad y justicia.

La fuerza y la ternura, la contundencia ética y el impacto en la sociedad de este “pequeño” grupo de mujeres me conmovió lo profundo del alma y me hizo pensar en “los muchos caminos que Dios utiliza” para hablarnos desde el dolor y el sufrimiento y cuestionar *“nuestra humanidad”*, desde lo “*pequeño e* *insignificante”.*

El 27 de agosto en la mañana, en la conmemoración de los 43 años de la desaparición forzada de su hija Ana María, de su yerno Julio Cesar y de su nieto o nieta Camila o Ernesto y a los 94 años de edad, Mirta presentó un Habeas Corpus pidiendo al estado argentino *“ponerse en movimiento* *para saber qué pasó”* con sus seres queridos y para que se *“restituya la identidad de su nieta o* *nieto que nació en cautiverio a principio de enero 1977”.* Al contemplar a Mirta, con su fragilidad, fortaleza y decisión, me vino a la mente la imagen de la viuda del evangelio yendo insistentemente “a pedir justicia ante un juez inicuo que no le importaban ni Dios ni los hombres” *(Lucas 18, 1-8).*  
El mismo 27 de agosto, en la tarde, se realizó en Buenos Aires una marcha contra el “*gatillo fácil”, *el asesinato de jóvenes, en su mayoría pobres, ejecutados por miembros de la policía.

**Desde diciembre de 1983 a enero de 2019 han asesinado 6.564 jóvenes.**

Madres, padres, abuelas, abuelos, hermanos, hermanas y demás familiares, acompañados por organizaciones solidarias y de derechos humanos, marcharon hasta la Plaza de Mayo con sus fotos, dolores, indignación, impunidad, fuerza y decisión. Allí estuvo Mirta expresando su solidaridad. Se podía palpar su conexión, la conexión de quienes han perdido violentamente sus seres queridos a manos del Estado y **no renuncian a la exigencia de la verdad, la justicia y a la memoria dignificadora.**

En Mirta y en todas las madres que caminaban por las calles de Buenos Aires, denunciado el asesinato y la desaparición de sus hijos e hijas, vi a María de Nazaret, la madre de Jesús, acompañando a su hijo por calles de Jerusalén mientras lo torturaba, humillaban, ultrajaban y lo conducían hacia el lugar de la crucifixión, donde los poderes de su tiempo lo asesinaron para acabar con su propuesta de vida que transformaba su sociedad. En la fuerza, el valor y la dignidad de María para estar de pie junto a la cruz *(Cf. Juan 19,25-27)*, estaban todas las madres de todos los hijos e hijas asesinados por todos los poderes del mundo a lo largo de toda la historia.

Fraternalmente,  
*P. Alberto Franco, CSsR, J&P*  
francoalberto9@gmail.com
