Title: “Javier Valdez le temía más a las autoridades que a los criminales”
Date: 2017-05-16 16:49
Category: El mundo, Entrevistas
Tags: Asesinatos, mexico, Periodistas, Sinaloa
Slug: javier-valdez-es-el-sexto-periodista-asesinado-en-mexico-durante-2017
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/05/Javier-Valdez.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Revista Desocupado] 

###### [16 May. 2017] 

“Para Javier la clase política era hija del narcotráfico, pero a la vez **él le tenía más miedo, decía, a las autoridades, que a los grupos criminales”, así lo manifestó Carlos Fazio,** profesor universitario, periodista de La Jornada y amigo de Javier Valdez, el sexto periodista mexicano asesinado en lo que va corrido del 2017.

**México, es el segundo país más peligroso para ejercer el periodismo** después de Siria, justamente la semana pasada “un organismo internacional calificó a Mexico como el país en guerra no declarada donde después de Siria ocurren más muertes de civiles” aseguró Fazio. Le puede interesar: [342 periodistas han sido asesinados entre 2006 y 2015 en América Latina](https://archivo.contagioradio.com/342-periodistas-han-sido-asesinados-entre-2006-y-2015-en-america-latina/).

### **En México investigar el narcotráfico es investigar todo el Estado** 

Javier Valdéz es recordado por sus colegas como una persona comprometida con el **ejercicio del periodismo de denuncia, un hombre valiente, crítico, digno de representar ese oficio** “Javier es otro compañero que colaboraba con el diario La Jornada desde hace 18 años y había colaborado con el semanario Ríodoce, que ayudó a fundar”.

Para Fazio la muerte de Valdéz no tuvo que ver solamente con el tema del narcotráfico, sino también con una economía capitalista “que es **una economía asesina, una economía militarizada y donde justamente los grandes medios forman parte de la artillería** para imponer este modelo” manifiesta Fazio. Le puede interesar: [Durante los últimos 8 años han desaparecido 27 mil personas en México](https://archivo.contagioradio.com/mexico-reporta-mas-de-27-mil-personas-desaparecidas/)

Dice además que el caso de Javier es muestra de lo que le pasa en México a los periodistas críticos, que se dedican no a cubrir el narcotráfico, sino a cubrir las realidades de violencia “en donde **hay una cohabitación de los grupos de la economía criminal con las autoridades”. **Le puede interesar: [Los 6 escándalos más sonados de Enrique Peña Nieto](https://archivo.contagioradio.com/los-6-escandalos-de-enrique-pena-nieto/)

Javier había publicado sus crónicas **en revistas como Proceso, Gatopardo y Emeequis** y entre los galardones que **obtuvo están el Premio Sinaloa de Periodismo y el International Press Freedom Award** del Comité para la Protección de Periodistas de Nueva York.

Para los años 90 **fue reportero de los noticieros televisivos del Canal 3, en Culiacán y luego ingresó al periódico Noroeste.** Valdez además fue reconocido por publicar varios libros del narcotráfico, como “Miss narco” en 2009, “Los morros del narco” en 2011, “Con una granada en la boca: heridas de guerra del narcotráfico en México” en 2014 y “Huérfanos del narco”. Le puede interesar: [Asesinato de Juan Santos es un nuevo ataque contra la libertad de expresión en México](https://archivo.contagioradio.com/asesinato-de-juan-santos-es-un-nuevo-ataque-contra-la-libertad-de-expresion-en-mexico/)

### **Las autoridades hacen declaraciones pero garantizan la impunidad** 

Añade Carlos Fazio que demandarán justicia en este y en los **120 casos más de periodistas asesinados en México, de los cuales el 10% han sido contra mujeres** “porque lo que pasa es que las autoridades siempre hacen declaraciones cuando ocurren estos hechos, pero ellos son quienes garantizan la impunidad (…) no hay prácticamente ninguna sentencia contra los ejecutores de estos crímenes”. Le puede interesar: [90% de crímenes contra periodistas permanecen en la impunidad](https://archivo.contagioradio.com/impunidad-crimenes-contra-periodistas/)

![Info Mexico](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/05/CzHct05VQAAcdNY.jpg){.alignnone .size-full .wp-image-40607 width="1523" height="1198"}

###### Reciba toda la información de Contagio Radio en [[su correo]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio]{.s1}](http://bit.ly/1ICYhVU)[.]{.s1}
