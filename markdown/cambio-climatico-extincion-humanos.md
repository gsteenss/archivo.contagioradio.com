Title: 6ta era de extinción masiva de especies es causada por los humanos
Date: 2018-12-17 16:02
Author: AdminContagio
Category: Voces de la Tierra
Tags: Ambiente, Animales, cambio climatico
Slug: cambio-climatico-extincion-humanos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/12/descarga-770x400.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Heavy Square 

###### 6 Dic 2018 

¿Que pasará si no se cumplen las metas para reducir el calentamiento global, la deforestación, la sobre explotación de los recursos naturales?. **Los acuerdos entre las naciones para detener la marcha que hoy tiene en peligro la existencia del planeta y las especies que en el habitan no han sido suficientes hasta ahora**.

Para hablar sobre el tema en Voces de la Tierra nos acompañaron **Susana Muhamad**, ambientalista, exsecretaria ambiente de Bogotá, **Martin Ramirez** líder sostenibilidad ambiental del proyecto Transforma, y en representación de las comunidades afectadas por proyectos hidroeléctricos **Guillermo Builes**, desde Sabana Larga, Antioquia y **Diana Giraldo**, ambos integrantes del Movimiento Ríos Vivos Colombia.

<iframe id="audio_30853974" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_30853974_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Encuentre más información ambiental en[ Voces de la tierra ](https://archivo.contagioradio.com/programas/onda-animalista/)y escúchelo los jueves a partir de las 11 a.m. en [Contagio Radio](https://archivo.contagioradio.com/alaire.html) 
