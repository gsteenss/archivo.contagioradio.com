Title: Segunda Asamblea Nacional por la Paz en Bogotá
Date: 2015-11-19 18:06
Category: Ambiente, Nacional
Tags: Asamblea minero energetica, asamblea nacional por la paz, Derechos Humanos, Mesa minero energetica, USO
Slug: segunda-asamblea-nacional-por-la-paz-en-bogota
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/11/Segunda-Asamblea-Nacional-por-la-Paz.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto:USO 

###### [19 Nov 2015]

Con el objetivo de sentar una mesa minero energética que brinde al país iniciativas frente a la construcción de sociedad en el posconflicto, se adelanta en Bogotá la segunda Asamblea Nacional por la Paz, organizada por la Unión Síndical Obrera USO, con participación de integrantes de Organizaciones sociales, el Ministerio de Trabajo y la Universidad Nacional de Colombia.

La jornada se desarrolla bajo los tres ejes planteados: Política Minero-Energética, Desarrollo Regional y Cultura para la Paz y Pos-acuerdo, a partir de los insumos recogidos durante las 43 asambleas subregionales, 12 regionales foros, conversatorios y espacios de reflexión en que participaron de manera activa cerca de 8.500 personas de 1.300 ogranizaciones y comunidades del país.

La Asamble busca realizar un ejercicio de construcción desde lo local territorial, hacia lo regional sin extraviar su perspectiva nacional, desde la coordinacion de propuestas y movilizaciones en pro de los recursos minero energéticos, para evitar que con su extracción sigan impactando negativamente el medio ambiente, dignificando a los seres humanos que laboran en las empresas o que habitan en el entorno.

Sin embargo, la vinculación de sectores sociales y populares, gremios empresariales, iglesias, gobiernos municipales y departamentales, agencias internacionales, representantes de las comunidades más afectadas por la violencia y pobreza, algunos mandatarios y entidades locales, contrasta con la ausencia de organizaciones que agrupan los empresarios, necesarios para establecer un diálogo abierto participativo.

En 1996 Ecopetrol, el Gobierno Nacional y la USO realizaron la primera Asamblea Nacional por la Paz, en la cual se buscó explorar caminos con las empresas del sector petrolero y con las organizaciones guerrilleras para que este no fuera involucrado en el conflicto armado.
