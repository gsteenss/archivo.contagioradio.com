Title: El show mediático de la Comunidad del Anillo
Date: 2016-02-18 18:23
Category: Opinion, Otra Mirada
Tags: la comunidad del Anillo, palomino, policia
Slug: el-show-mediatico-en-la-comunidad-del-anillo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/02/B7A97BF8-DDA3-4752-9629-785BCC768C5C_cx0_cy5_cw0_mw1024_s_n_r1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: EFE 

#### **Contagio Radio** 

Tras la publicación de “el video” en el que aparece Carlos Ferro, ex vice ministro del Interior, junto al capitán de la policía Anyelo Palacios, se han suscitado, en varios círculos, discusiones sobre el papel del periodismo y la delgada línea entre la intimidad y las implicaciones de ser un personaje de la vida pública, pero ¿ese es el fondo?

La discusión se centró, con muchas aristas, en dos asuntos: La ética periodística de un medio y el tratamiento a la noticia y la vida íntima de un personaje como Carlos Ferro, pero se obviaron las discusiones que parecían obvias. Por ejemplo, ¿qué tiene que decir la “clase política” y la institución policial, frente a prácticas que parecen sistemáticas?

Como ocurrió en el caso del General Palomino, también ocurrió con el caso de Armando Otálora, ex Defensor del Pueblo, ambos (uno se tardó más que el otro) salieron de su cargo y afirmaron estar prestos a que las investigaciones avancen dando “pasos al costado” para salvar la buena imagen de las instituciones a las que representaban. La cuestión está en las investigaciones, ¿realmente avanzarán y llegarán al fondo promotor de este tipo de prácticas?

Lo que ha sucedido en ocasiones anteriores y con escándalos parecidos, es que las investigaciones exhaustivas culminan en una responsabilidad difuminada de dos o tres “manzanas podridas” entre el costal de las diáfanas instituciones a las que pertenecían, y esa ha sido la regla general.

El escándalo de la Comunidad del Anillo viene desde el 2011 cuando, aparentemente, la Cadete Lina Maritza Zapata, se suicidó horas antes de su graduación. Los primeros indicios apuntaban al suicidio, pero la fuerza de las denuncias de la familia de Lina Maritza ampliaron el espectro hasta llegar a una posible red de prostitución al interior de la Escuela de Sub Oficiales de la Policía. La investigación no avanza.

Pero los otros cargos que la procuraduría le formula a Palomino que tienen que ver con el incremento injustificado de su patrimonio, las interceptaciones ilegales a periodistas y la red de prostitución en la policía, ¿van a ser investigados? y ¿las investigaciones van a tener resultados de fondo? O ¿todo terminará como con el escándalo de la casa de interceptaciones de las FFMM conocida como “Andrómeda”?

Lo cierto es que las preguntas que se están planteando en el Show Mediático desvían la discusión. De la novela personalista y de figuras únicas, habría que pasar a una lógica institucional que nos lleve a discutir lo que está pasando con nuestras instituciones. Y no solamente la Policía y la Defensoría, también la Corte Constitucional y hasta el alto gobierno, que en cabeza del presidente Santos, posesionó, en un hecho atípico, al vice ministro Carlos Ferro como “un caso especial”, “un compañero de lucha política” y una persona con la que se identifican, señalándolo así en el discurso oficial de ese 14 de Octubre de 2014.
