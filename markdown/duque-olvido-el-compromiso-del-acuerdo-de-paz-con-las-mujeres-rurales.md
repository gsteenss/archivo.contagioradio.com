Title: Duque olvidó el compromiso del Acuerdo de Paz con las mujeres rurales
Date: 2019-03-08 16:52
Category: Comunidad, Mujer
Tags: acuerdo de paz, Colombia Humanas, genero, jurisdicción especial para la paz, Sustitución de cultivos de uso ilícito
Slug: duque-olvido-el-compromiso-del-acuerdo-de-paz-con-las-mujeres-rurales
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/03/mujeres-rurales.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio ] 

###### [08 Mar 2019] 

En el Día Internacional de la Mujer, mujeres de varios sectores de la sociedad coinciden en que la implementación de los Acuerdos de Paz, en particular la reforma rural integral y la sustitución de cultivos de uso ilícitos, tiene un papel importante **para la protección de los derechos de las** **campesinas colombianas**, una de las poblaciones más vulneradas en el país.

**Adriana Benjumea, directora de la Corporación Humanas**, sostiene que "hoy, 8 de marzo, es fundamental pensar en el Acuerdo y pensar en ese Acuerdo en lógica de las mujeres trabajadoras". Ante las declaraciones del Presidente Duque en contra de la Jurisdicción Especial para la Paz (JEP) y en favor del uso de glifosato en fumigaciones, Benjumea manifesta que el mandatario debería considerar seriamente su compromiso con las mujeres dado que esta decisiones vulnerarían sus derechos.

"El Acuerdo ha sido muy manipulado por los que no quieren la paz y en esa manipulación se ha **olvidado también de las medidas fundamentales para las mujeres que el acuerdo contenía** ," afirmó la directora. Entre ellas, existe el fondo de tierras para mujeres, un futuro económico para las cocaleras y un mecanismo de justicia transicional para las miles de mujeres víctimas del conflicto armado.

Al respecto, la Representante **Ángela Maria Robledo** sostiene que la preservación de justicia transicional es necesaria para garantizar los derechos de las mujeres víctimas, quienes según ella, exigen la verdad, la reparación y la no repetición por encima de sentencias de cárcel para los responsables.

### **Las lideresas, menospreciadas y amenazadas** 

Por otra parte, Benjumea señala que el trabajo de las mujeres en defensa de la paz, el ambiente y los derechos humanos es gravemente desvalorizado por la sociedad. La directora resalta que a través de las iniciativas de las mujeres se logró incluir 100 medidas con enfoque de género en el Acuerdo de Paz.

Al contrario de gozar de reconocimiento, "estos liderazgos de las mujeres ha significado que hoy tengan mayor amenazas sobre sus vidas y que el número de lideresas asesinadas haya aumentado", afirmó la Representante Robledo. De enero a septiembre de 2017, una defensora de derechos humanos fue asesinada cada 26 días y medio, según información de Somos Defensores. Para 2018, en el mismo periodo, la frecuencia es de 23 días. (Le puede interesar: "[Informe revela aumento en violencias contra las lideresas en Colombia](https://archivo.contagioradio.com/62024/)")

La Representante sostiene que el Gobierno tiene que cumplir sus compromisos, acordados en los Acuerdos de Paz, para garantizar a las lideresas y los líderes sus derechos a un vida digna, en que pueden ejercer sus trabajos de forma segura.

<iframe id="audio_33204173" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_33204173_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>  
<iframe id="audio_33204580" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_33204580_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
