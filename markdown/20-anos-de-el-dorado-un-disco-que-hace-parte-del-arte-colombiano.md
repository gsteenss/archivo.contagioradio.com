Title: 20 años de "El Dorado" un disco que hace parte de la historia nacional
Date: 2015-12-01 11:00
Category: Cultura
Tags: Andrea Echeveri, Aterciopelados, Aterciopelados 20 años, Aterciopelados Rock, Disco el Dorado, Florecita Rockera, Héctor Buitrago, Lanzamiento Disco-Libro el dorado
Slug: 20-anos-de-el-dorado-un-disco-que-hace-parte-del-arte-colombiano
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/10/Aterciopelados.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto:las2orillas.com 

###### [19 oct 2015]

Los años 80 en Colombia se vivian en medio de** la guerra en toda su brutalidad al interior del país,** desde paramilitarismo, narcotráfico, delincuencia común, mezclada con la violencia estatal y la violencia de sus fuerzas militares. El país sufrió en estos años  la Masacre de Trujillo (Valle) los Narcotraficantes en cabeza de Diego Montoya, alias “Don Berna” y Henry Loaiza, alias “El Alacrán”, en alianza con el Estado y la policía, **masacró y desapareció aproximadamente 352 campesinos** que eran tildados de guerrilleros y colaboradores  del Ejército de Liberación Nacional (ELN).

1995 inició con una noticia de titular inmenso, en enero el gobierno representado por **Ernesto Samper, presidente de entonces, acepta la responsabilidad del Estado**, después de que se revelara un informe de la Comisión de la Verdad, integrada por miembros del gobierno colombiano, entidades de control, la iglesia católica y numerosas ONG, que demostraba la participación Estatal en estas muertes.

La violencia continuó, el **10 de agosto** la guerrilla de las FARC disparó contra un avión de la Cruz Roja, el **2 de noviembre** unos sicarios en Bogotá, asesinan al dirigente Álvaro Gómez Hurtado del partido conservador. Más tarde son masacrados grupos de campesinos en varios sectores del Caqueta, no se dice nada a nivel nacional por temor a las represalias de los llamados "héroes de los andaquies” un Frente paramilitar que dominaba esta área.

La juventud sufría toda esta violencia, toda esta sangre, todavía una especie de "No Futuro", de alguna forma había que gritar al mundo que no todo era malo, que la violencia no era nuestra verdadera cara y que **había un pueblo caminando, movilzandose y trabajando por este país**, entonces el rock con aires de punk fue el que decidió gritar y romper muros. **Los Aterciopelados, agrupación que nació en 1990** bajo el nombre de Delia y los Aminoácidos, pero que **en el 92** cambió su nombre por Aterciopelados.

**El Dorado tiene sonido:**

Andrea Echeverri y Héctor Buitrago venían cargados con toda la violencia del país, ninguno de los dos era ajeno y venían puliendo su música, mejorando y experimentado sin miedo, **en el 95 lanzaron “EL Dorado”** un disco cargado de letras y sonidos, cargado de erotismo, amor y sólidas bases colombianas, un Héctor Buitrago y una Andrea que gritaban al universo que eran colombianos y que **habían encontrado El Dorado** y con él serían reconocidos internacionalmente.

**El disco consta de 16 canciones** entre las que se destacan Florecita Rockera, candela, No futuro, El Dorado, La estaca y Mujer Gala. En este 2015 se cumplen 20 años del lanzamiento, por este motivo la Fundación Barrio Colombia decidió lanzar un disco-libro.

Después del lanzamiento de este lanzamiento Andrea Echeverri fue invitada por SodaStereo para un concierto de la serie Unplugged de MTV y luego Héroes del Silencio en cabeza de Enrique Bunbury los invita a de gira con ellos a España.

### **El Disco- Libro:** 

**En este 2015 se cumplen 20 años  **y se celebrará el lanzamiento con un Disco-Libro que se llamará **“El Dorado 20 años”** estará a la venta en discotiendas d**esde el 5 de diciembre de este año** en dos formatos: Libro CD o Libro Lp Doble, en cantidades ilimitadas, sin embargo podría adquirirse de forma anticipada en esta página: <http://tributoaterciopelados.co/>.

Los CD están **editados así:** **un vinilo doble** (2LP) color dorado con las 1 canciones y 36 páginas de textos, fotografías e ilustraciones conmemorativas; y el otro **un C.D de presentación de lujo** con 16 canciones y 2 páginas de textos, fotografías e ilustraciones conmemorativas.

Este disco contará la participación de **Carlos Vives, Enrique Bunbury, Kevin Johansen, Paulinho Moska, Christina Rosenvinge, Marta Gómez, Edson Velandia, Camila Moreno, Esteman, entre otros.**

Además que **contará con 16 escritos de distintos periodistas musicales** del continente para que contarán sus experiencias con esta banda y el disco “El Dorado” para unificar el contenido. Algunos de estos serán: **Joselo Rangek, Luis Daniel Vega, Álvaro Gonzáles-Villamarín, Félix Sant-Jordi, Humphrey Inzillo, Ricardo Durán, Andrés Gualdrón, Pablo Ferrer, Nicolas Vallejo, entre otros.**

Hoy aterciopelados es unas de las bandas más importantes y coherentes del rock nacional con la sociedad, han demostrado un compromiso desde el arte, con su música y su voz, así como la violencia en Colombia no cesa, Héctor y Andrea tampoco. Ellos **saben que Aterciopelados hace 20 años, encontró el Dorado.**
