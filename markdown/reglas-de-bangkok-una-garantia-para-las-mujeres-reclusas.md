Title: Reglas de Bangkok, una garantía para las mujeres reclusas
Date: 2019-11-20 11:00
Author: CtgAdm
Category: Expreso Libertad
Tags: carcel, cárceles mujeres, Expreso Libertad, ONU
Slug: reglas-de-bangkok-una-garantia-para-las-mujeres-reclusas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/PRESO1.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/manos.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/womens-prison6.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

En este programa del **Expreso Libertad** hablamos de las **Reglas de Bangkok**, un conjunto de 70 normas expedidas por la **ONU** que velan por mejorar el tratamiento penitenciario hacia las mujeres, sus roles de género y las garantías para transitar hacia condenas no privativas en centro de reclusión.

Sin embargo, Claudia Cardona, integrante de la Corporación Humanas y Jazmín Reyes, integrante del colectivo Mujeres Libres, denunciaron que a pesar de que Colombia esté suscrito como país miembro de la ONU, no acata ni respeta ninguna de las 70 reglas de Bangkok y por el contrario profundiza un modelo penitenticario patriarcal en contra de las mujeres.

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/video.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2Fvideos%2F1759226217541416%2F&amp;width=600&amp;show_text=false&amp;appId=1237871699583688&amp;height=338" width="600" height="338" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

Le puede interesar:  [Las cárceles de Estados Unidos y sus violencias contra las mujeres](https://archivo.contagioradio.com/las-carceles-de-estados-unidos-y-sus-violencias-contra-las-mujeres/)
