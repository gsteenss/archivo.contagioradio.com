Title: 4 canales comunitarios ganan pelea por estar al aire
Date: 2016-12-16 15:27
Category: El mundo, Otra Mirada
Tags: Argentina, canales, comunitarios, Ley de Medios, televisión
Slug: canales-comunitarios-argentina
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/12/Czxm46YW8AAZH79.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### Foto:Barricada TV 

##### 16 Dic 2016 

Tras una dura pelea por acceder a su derecho de transmitir por Televisión Digital Abierta (TDA),** cuatro canales comunitarios de Argentina empezarán a utilizar el canal 32 de este sistema**, a los cuales ya les habian sido adjudicadas las respectivas licencias en concurso público hace un año.

Las televisoras **Barricada TV y Urbana TeVé**, de la Ciudad Autónoma de Buenos Aires y **Pares TV de Luján y Comarca Si** **de Florida**, ambos bonaerenses, no habían podido iniciar sus transmisiones en medio digital **por la interferencia del Canal 13/Altear**, razón que los llevó a exigir al Ente Nacional de Comunicaciones (ENACOM) se diera cumplimiento a los artículos de la Ley de Medios que reserva un 33 por ciento del espectro a los medios sin fines de lucro.

Contrario a cambiar o restringir el uso de la frecuencia al canal perteneciente al grupo Clarín que a modo de prueba interferia la señal, la decisión de la entidad, presionada por movilizaciones sociales, reclamos y querellas judiciales, fue **reasignar los comunitarios al canal 32**, aun cuando los espacios que estaban en licitación el año anterior eran las frecuencias 33 y 19, asignadas como operadores a Barricada y Pares respectivamente.

La resolución por la cual se ubica a las televisoras de baja potencia en el canal 32, será publicada oficialmente por ENACOM en los próximos días. Por ahora la decisión fue confirmada el jueves a los representantes de los canales adjudicatarios. Le puede interesar: [Mauricio Macri pretende "amordazar" la ley de medios](https://archivo.contagioradio.com/ley-de-medios-amordazada-en-argentina/).

Sin embargo, el reclamo de los canales continua porque la medida suspende la interferencia, que afectaba directamente en su derecho a a la libertad de expresión, pero también l**a reorganización de la grilla de la TDA**, que obliga a este tipo de emisoras a trasladarse de las frecuencias una vez estas fueran asignadas por concurso, con lo que se termina favoreciendo a la consolidación de los canales comerciales y sus señales "espejo", situación que atenta una vez más contra la ley de medios por parte del gobierno Macri.

A pesar de lo positivo de la resolución, los canales alternativos y organizaciones del sector anunciaron que **seguirán movilizandonse** para evitar que que se sigan presentando demoras en la habilitación de sus plantas, en el pago de la deuda que el organismo tiene con el serctor por la lentitud en el pago de los fondes de fomento consursables FOMECA y la suspensión de concursos convocados desde el año anterior para otros canales que buscan acceder al mismo derecho como aparece estipulado en la ley.
