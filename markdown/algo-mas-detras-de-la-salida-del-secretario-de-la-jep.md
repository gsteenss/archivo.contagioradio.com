Title: Algo más detrás de la salida del Secretario de la JEP
Date: 2018-04-04 15:45
Category: Nacional, Paz
Tags: jurisdicción especial para la paz, Nestor Correa
Slug: algo-mas-detras-de-la-salida-del-secretario-de-la-jep
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/04/JEP.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Colombia Informa] 

###### [04 Abril 2018] 

La renuncia del secretario de la JEP, Néstor Raúl Correa, que aparentemente tomo por sorpresa a muchos es una decisión que venía esperándose desde días atrás. De acuerdo con algunas fuentes consultadas por Contagio Radio, había inconformidad en muchos de los magistrados de la Jurisdicción Especial para la Paz, e**n relación con el manejo ineficaz de los recursos que estaba a cargo del secretario.**

Algunos integrantes de esta institución, que se encargará de juzgar crímenes de lesa humanidad, manifestaron que no contaban con computadores para el uso de sus funciones y otros elementos indispensables para el desarrollo de su trabajo. **También cuestionaron el que se gastaran recursos de manera suntuaria en libretas**, bolígrafos y otros materiales innecesarios.

Hace unos días, la Contraloría General de la Nación ordenó investigar el gasto de recursos del nuevo sistema Alternativo Penal, que estuvo a cargo de Correa hasta el nombramiento de la actual presidenta, Patricia Linares.  Correa inició sus funciones como secretario desde el pasado 2 de noviembre de 2017. (Le puede interesar: ["Ante renuncia de secretario de JEP las víctimas de Estado insisten en proteger archivos del DAS"](https://archivo.contagioradio.com/ante-renuncia-de-secretario-de-jep-las-victimas-de-estado-insisten-en-proteger-archivos-del-das/))

### **La salida de Correa y el incumplimiento de sus funciones** 

Otra de las situaciones que llevó a la renuncia de Correa tiene que ver con que desde el comienzo, se percibían actuaciones de Correa que se extralimitaban en sus funciones, dando opiniones o interviniendo en asuntos, **que no eran propiamente administrativos, ni competentes al relacionamiento con organizaciones sociales y de víctimas**.

Frente a la preocupación que expresan organizaciones de víctimas sobre la medida cautelar solicitada por Correa, para los archivos del DAS, analistas manifestaron que esta no tiene nada que ver con su renuncia, ni con la posibilidad de preservar los derechos a la verdad y pruebas fundamentales para las víctimas, que en primera medida no estaba dentro de sus competencia y que en dado caso, si Correa hubiese querido preservar estos derechos, hubiera ordenado una intervención sobre archivos que existen en la Casa de Nariño, **sobre órdenes emanadas en materia de seguridad y le hubiese pedido a alguien facultado de la JEP una solicitud para pedir la protección de archivos** que están en guarniciones militares.

### **¿Qué ha pasado con los dineros para la paz?** 

El cuerpo de países que conforman el Fondo Colombia Sostenible, en el que se encuentran Suiza, Suecia y Noruega, emitieron un reciente comunicado en el que también pidieron mayor claridad sobre el manejo de los dineros de este fondo, destinado a la implementación de los Acuerdos de Paz, **en donde algunos recursos serán destinados a parte de la implementación de los Acuerdos de Paz,** y frente al ejercicio y la cualificación de quienes actualmente ocupan los cargos de la presidencia del consejo directivo y de la directora de la Unidad Técnica de Consultoría del fondo.

El estado de las instalaciones de la JEP tampoco se encuentra en las mejores condiciones para que se inicien las actividades y muchos magistrados están trabajando sin las garantías mínimas para el desarrollo de sus labores. (Le puede interesar: ["JEP otorga medidas cautelares a archivos de Inteligencia del DAS"](https://archivo.contagioradio.com/jep-otorga-medidas-cauteles-a-archivos-de-inteligencia-del-das/))

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
