Title: Madres de Soacha: Mientras Acevedo siga en el Centro de Memoria Histórica nosotras no estaremos
Date: 2020-06-04 21:22
Author: CtgAdm
Category: Actualidad, DDHH
Tags: Dario Acevedo
Slug: madres-de-soacha-mientras-acevedo-siga-en-el-centro-de-memoria-historica-nosotras-no-estaremos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/06/MAFAPO.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/06/madres-de-.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: Madres Falsos Positivos de Colombia

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

El pasado 2 de junio a través de un comunicado, la Fundación Madres Falsos Positivos de Colombia [(MAFAPO)](https://twitter.com/MAFAPOCOLOMBIA), la cual integra a algunas de las madres de las víctimas de ejecuciones extrajudiciales por parte de las FFMM, publicó **su renuncia a participar de los proyectos adelantados por el Centro Nacional de Memoria Histórica (CNMH)**, en su misiva enfatizaron que se retiran pues la dirección de **Darío Acevedo “no da garantías al derecho a la verdad de las víctimas”**

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Las Madres de Soacha exigen garantías

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

MAFAPO trabajaba con el CNMH en un libro que recogiera los testimonios de las madres víctimas de los crímenes presentados, ilegítimamente, como bajas en combate por agentes del Estado; no obstante, con la decisión, **renuncian a participar en esta y cualquier otra actividad que se adelante desde el CNMH mientras esté encabezado por Darío Acevedo.**

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Este hecho se suma a la decisión de la Asociación Minga de retirar los archivos físicos que su organización había confiado al Centro de Memoria al considerar como un riesgo el manejo de la información por parte de la actual dirección. [(Lea también: Organizaciones retiran archivos del Centro de Memoria ante políticas negacionistas del director)](https://archivo.contagioradio.com/organizaciones-retiran-archivos-del-centro-de-memoria-ante-politicas-negacionistas-del-director/)

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### **Darío Acevedo y su negación del conflicto armado en Colombia**

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Desde su nombramiento **Darío Acevedo ha motivado el rechazo de diversos grupos de víctimas. Poco antes de posesionarse en el cargo, negó la existencia del conflicto armado en nuestro país.** —Aunque la ley de víctimas dice que lo vivido fue un conflicto armado, eso no puede convertirse en una verdad oficial— señaló en el año 2019 en entrevista ante un medio de comunicación.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

El retiro de MAFAPO se produce meses después de hacerse oficial un convenio establecido con el Centro Nacional de Memoria Histórica y sectores ganaderos como Fedegan que han sido señalados de oponerse a la restitución de tierras y el proyecto de reforma agraria, acudiendo al despojo de tierras y la financiación del paramilitarismo. [(Le puede interesar: Centro de Memoria y Fedegan ¿una alianza para exonerar a responsables del conflicto?)](https://archivo.contagioradio.com/centro-de-memoria-y-fedegan-una-alianza-para-exonerar-a-responsables-del-conflicto/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

A esta situación se suma la reciente **decisión de la Red de Sitios de Memoria Latinoamericanos y Caribeños de suspender la membresía del Centro de Memoria**. [(Le puede interesar: Sin lineamientos, centros de memoria pueden convertirse en aparatos ideológicos de Gobierno)](https://archivo.contagioradio.com/sin-lineamientos-centros-de-memoria-pueden-convertirse-en-aparatos-ideologicos-de-gobierno/)

<!-- /wp:paragraph -->
