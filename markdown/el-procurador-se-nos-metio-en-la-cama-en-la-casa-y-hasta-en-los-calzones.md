Title: “El procurador se nos metió en la cama, en la casa y hasta en los calzones”
Date: 2015-10-29 16:48
Category: LGBTI, Nacional
Tags: Alejandro Ordoñez, Cambio de sexo en la cédula, Comunidad Trans, Decreto 1227, Derechos Humanos, LGBTI, Plan Arrunchis con el procurador, Radio derechos Humanos
Slug: el-procurador-se-nos-metio-en-la-cama-en-la-casa-y-hasta-en-los-calzones
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/10/Trans.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: contagioradio 

<iframe src="http://www.ivoox.com/player_ek_9210162_2_1.html?data=mpeekpaado6ZmKiakpqJd6KlmpKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRaaamhp2dh56nic2f0dfcxdrWpcXj05Dgx5DSs9Sfzsrhy4qnd4a2lJDS0JDQpYzXwtLOh5enb8bijNHOjcjFt8Kf2pKSmaiRh9Di1cbUy9SPlsLYytSYj4qbh46o&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Alejandro Lanz, PARCES ONG] 

###### [29 oct 2015]

Desde las 11:30 am **300 personas realizan el Plantón "Arrunchis" con el procurador Alejandro Ordoñez,** frente a la Procuraduría** **con fin de explicarle al procurador porqué es importante el decreto 1227 que **permite el cambio de sexo en la cédula a través de un proceso notarial, teniendo en cuenta que ** el jefe del Ministerio Público busca anularlo.

"El procurador se nos metió en la cama, en la casa y hasta en los calzones... desde que está en ejercicio de la Procuraduría con más de 1500 funcionarios ha puesto a esta institución a luchar en contra de los derechos de las mujeres y las personas LGBTI en Colombia” señala Alejandro Lanz, Director Ejecutivo de Pares en Acción-Reacción Contra la Exclusion Social, PARCES ONG.

"La Procuraduría debe proteger los derechos fundamentales y en el caso de la **comunidad LGBTI ocurre todo lo contrario", dice Lanz,** quien explica que a la fecha hay 70 personas de la comunidad Trans que se han logrado acogerse a este decreto para cambiar el sexo en su cédula, de acuerdo con cifras de la Registraduría Nacional.

Los asistentes se presentan en pijama, con pancartas y con figuras del procurador para explicar por qué el cambio de sexo en la cédula es importante. **Además simultáneamente en la Notaría 1 varias personas, validarán su derecho al cambio de sexo en la cédula,**  para que se “se eliminen barreras frente a sus derechos a la educación y a lasalud”, afirma Lanz.
