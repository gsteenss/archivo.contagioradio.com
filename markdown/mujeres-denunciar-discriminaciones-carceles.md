Title: Mujeres se reúnen para denunciar discriminaciones tras salir de las cárceles
Date: 2019-06-12 18:06
Author: CtgAdm
Category: eventos, Mujer
Tags: carcel, Encuentro de Mujeres, Humanas, Libertad
Slug: mujeres-denunciar-discriminaciones-carceles
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/mujeres-en-las-carceles.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:Contagio Radio] 

Este martes y miércoles se llevará a cabo el primer encuentro de mujeres que han estado privadas de la libertad; con el título de **"Mujeres sin rejas, libertad sin cadenas"**, las participantes denunciarán las discriminaciones que sufren durante su permanencia en la cárcel y tras recuperar su libertad. El público podrá asistir el miércoles a la presentación pública de las conclusiones, en la que también participarán entidades encargadas del sistema carcelario en el país.

**Claudia Cardona, integrante de la Corporación Humanas**, explicó que en el evento participarán mujeres de varias regiones del país, y allí relatarán las vulneraciones a las que son sometidas cuando entran a la cárcel y aún, cuando recuperan su libertad. La iniciativa surgió luego de que la Corporación capacitara a 15 mujeres que salieron a la libertad en derechos humanos, y ellas se dieron cuenta que había unos estándares internacionales que no se estaban cumpliendo.

Como lo explicó Cardona, con el evento, las participantes "quieren mostrarle a otras mujeres que han salido a la libertad y a familiares de mujeres recluidas que estos derechos existen". (Le puede interesar: ["No hay garantías para la vida digna en las cárceles de Colombia"](https://archivo.contagioradio.com/garantias-vida-digna-carceles/))

### **¿Que significa mujeres sin rejas, libertad sin cadenas?**

Cardona afirmó que cuando las mujeres regresan a la libertad se chocan con una realidad que las desborda: no encuentran trabajo, la sociedad las rechaza y son estigmatizadas. Incluso, algunas de **las ex reclusas llevan años sin conseguir empleo, razón por la que no han podido acceder al sistema de salud o educación**; esta situación las llevó a reunirse para luchar por sus derechos, y los de las mujeres que siguen en prisión.

En la cárcel, la situación es igualmente difícil: no se cumplen las reglas de Bangkok (que son las que dictan el tratamiento de mujeres en prisión), lo que acaba en una vulneración de derechos máxima. Entre las denuncias sobre estas violaciones están la mala alimentación, un sistema de atención en salud, pésimo, hacinamiento y un inadecuado proceso de resocialización: "no hay profesores calificados para educar y en lo laboral hay talleres que no las capacitan para poder sobrevivir una vez salgan de la cárcel", añadió Cardona.

### **Instituciones y mujeres se encontrarán para discutir estos temas**

El encuentro se desarrollará en el **Hotel Macao (Avenida la Esperanza No. 44A – 21, cerca a Corferias)**, y este martes se desarrollará un cóctel a puerta cerrada en el que participarán 40 mujeres, entes de control y judiciales que tienen relación con el sistema carcelario. El próximo miércoles se realizará un trabajo con mujeres, donde aprenderán las reglas de Bangkok y verán las vulneraciones de las que fueron víctimas.

De esta forma, en la mañana se trabajará en las vulneraciones de mujeres en las cárceles, y en la tarde, sobre lo que están pasando al salir de la cárcel. Posteriormente, sobre las 3 de la tarde se realizará un intercambio con las instituciones que asistieron al cóctel, a las que se les pedirán acciones para que las violaciones a los derechos no sigan pasando; quienes deseen asistir a este intercambio lo podrán hacer confirmando su participación en los correos ccardona@humanas.org.co y gabyeraso@humanas.org.co.

<iframe id="audio_37036537" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_37036537_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
