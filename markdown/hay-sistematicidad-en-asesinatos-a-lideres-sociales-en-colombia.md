Title: ¿Hay sistematicidad en asesinatos a líderes sociales en Colombia?
Date: 2018-07-06 17:14
Category: DDHH, Paz
Tags: Asesinato a líderes sociales, Fiscalía, Sistematicidad
Slug: hay-sistematicidad-en-asesinatos-a-lideres-sociales-en-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/01/lideres-asesinados-20116.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [06 Jul 2018]

El aumento en los asesinatos de líderes sociales en el país ha encendido las alarmas frente a la falta de acciones por parte del gobierno para garantizar la protección de los mismos. En ese sentido, Alberto Yepes, integrante de la Coordinación Colombia, Europa, Estados Unidos que agrupa a más de 300 organizaciones defensoras de derechos humanos, afirmó que la importancia de que se acepte una sistematicidad en los homicidios radica en un cambio en las investigaciones sobre estos hechos que podría dejar a la luz quienes son los responsables.

### **¿Por qué los asesinatos a líderes si son sistemáticos?** 

El primer patrón que puede determinar la sistematicidad en el asesinato a líderes sociales, tiene que ver **con las características de ese liderazgo que compartían las personas,** ya que realizaban actividades de denuncia frente a violación de derechos humanos, eran víctimas de restitución de tierras, apoyaban la sustitución de cultivos de uso ilícito, entre otras actividades.

En segunda medida está el modus operandi sobre cómo se comete el asesinato, que según Yepes, generalmente es efectuado por hombres que hacen parte de grupos sicarales, que han realizado un **plan previo de seguimiento al líder para determinar la mejor manera de llevar a cabo su cometido**.

Para Yepes, sí el gobierno reconoce la sistematicidad detrás de los asesinatos a líderes en primera medida se podría investigar los casos conjuntamente y no por separado, como actualmente se hace. **Además, señaló que podría existir un avance mucho más contundente sobre los responsables intelectuales**.

“La sistematicidad lo que significa es que detrás de los asesinatos hay un plan, es decir hay unas estructuras que decidieron llevar a cabo un plan de exterminio a líderes sociales en el país” afirmó Yepes. (Le puede interesar: ["Estado debe reconocer sistematicidad en asesinatos a líderes sociales:  Somos Defensores"](https://archivo.contagioradio.com/estado-debe-reconocer-sistematicidad-en-asesinato-a-lideres-sociales-somos-defensores/))

En ese sentido aseguró que una vez se conozca quiénes son esos autores que dieron la orden de que **“se apretar el gatillo”** se podrá establecer las relaciones entre estructuras paramilitares, principales responsables de los asesinatos a líderes, y bandas criminales o de sicariatos.

En ese sentido, hizo un llamado a la Fiscalía para que analice el vínculo que pueda existir entre el aumento de asesinatos de líderes sociales luego de la elección de Iván Duque como presidente y para que la unidad de desmonte del paramilitarismo cumpla su deber dentro de los crímenes que han sido efectuados por este grupo armado.

<iframe id="audio_26931784" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_26931784_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
