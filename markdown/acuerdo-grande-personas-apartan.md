Title: "El Acuerdo es mucho más grande que unas personas que se apartan"
Date: 2019-08-29 14:32
Author: CtgAdm
Category: Entrevistas, Paz
Tags: acuerdo, Comunicado, FARC, marquez, paz
Slug: acuerdo-grande-personas-apartan
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/08/FARC-ratifica-compromiso-con-el-Acuerdo-de-Paz.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: FARC  
] 

Tras el anunció hecho por Iván Márquez y varios integrantes de las FARC-EP de rearmar un nuevo movimiento guerrillero, líderes políticos del partido FARC emitieron un comunicado en el que rechazan la decisión y señalan que la posición de Márquez es equivocada. Adicionalmente, resaltaron que la mayoría de los combatientes que dejaron las armas están cumpliendo con su proceso de reincorporación, y la implementación del Acuerdo de paz implica mucho más que la salida de unas pocas personas del proceso.

### **"FARC no comparte ninguno de los términos de dicha alocución"** 

En la declaración política leída por Rodrigo Londoño (conocido como Timoleón Jiménez), **el partido Fuerza Alternativa Revolucionaria del Común (FARC) manifestó que "no comparte ninguno de los términos" mencionados por Márquez en su alocución**. Adicionalmente, recuerdan que los y las exguerrilleras que dieron el paso para dejar las armas, lo hicieron "con el profundo convencimiento de que la guerra había dejado de ser el camino".

En ese sentido, aseguraron que **"proclamar la lucha armada en la Colombia de hoy constituye una equivocación delirante"**. En consecuencia, Timoleón afirmó en la rueda de prensa que aunque se sabían los obstáculos que tendría el Acuerdo de Paz,  también estaba claro que Colombia no se iba a transformar luego de la firma de un papel. (Le puede interesar: ["¿Se agota la esperanza de paz de Iván Marquez y ‘El Paisa’?"](https://archivo.contagioradio.com/esperanza-paz-de-ivan-marquez/))

### **"El Acuerdo es mucho más que personas que se apartan"** 

La senadora por el partido FARC Sandra Ramírez opinó en el mismo sentido de Timoleón, asegurando que cuando se firmó el Acuerdo, sabían que su implementación no "sería un camino de rosas", y que requeriría un esfuerzo por parte de ellos y de la sociedad colombiana. Por lo tanto, **desestimó el anuncio hecho por Márquez como un elemento que afecte la paz**, al declarar que "el Acuerdo de Paz es mucho más grande que unas personas que se apartan". (Le puede interesar: ["Cifras demuestran que gobierno Duque ha hecho muy poco por la paz"](https://archivo.contagioradio.com/gobierno-duque-poco-paz/))

Por su parte, Jiménez cerró su declaración asegurando que probablemente el anuncio de Márquez generará ruidos, pero ello no debe ensordecer el clamor de quienes siguen construyendo paz, y en virtud de ello, hizo un llamado al Gobierno Nacional para que avance en la implementación, y encuentre en este momento una excusa para unir al pueblo colombiano en torno a otras importantes tareas como apoyar con contundencia el proceso de reincorporación,  o avanzar en el desarrollo del punto uno del Acuerdo.

**Síguenos en Facebook:**  
<iframe id="audio_40608898" frameborder="0" allowfullscreen scrolling="no" height="200" style="border:1px solid #EEE; box-sizing:border-box; width:100%;" src="https://co.ivoox.com/es/player_ej_40608898_4_1.html?c1=ff6600"></iframe>  
<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
