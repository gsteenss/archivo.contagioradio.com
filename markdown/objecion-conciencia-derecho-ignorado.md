Title: La Objeción de Conciencia, un derecho ignorado en Colombia
Date: 2019-05-15 19:01
Author: CtgAdm
Category: DDHH, Nacional
Tags: Fuerzas militares, objeción de conciencia, objetor
Slug: objecion-conciencia-derecho-ignorado
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/Objeción-de-Conciencia-por-la-paz.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: @marujillaleon-Justapaz  
] 

Este miércoles se conmemora el día de la Objeción de Conciencia, un derecho consagrado en la Constitución política de Colombia; pero que presenta diferentes situaciones que dificultan su goce por parte de jóvenes. De hecho, según cifras de la asociación Justapaz, de **422 solicitudes** de objeción de conciencia que se presentaron entre agosto de 2017 y el mismo mes de 2018, solo **262 fueron reconocidos como objetores**. (Le puede interesar:["O. de conciencia quedó incluida en Proyecto de Ley que modifica el servicio militar"](https://archivo.contagioradio.com/ley-que-modifica-el-servicio-militar-es-regresiva-para-los-tiempos-de-paz/))

Según el abogado integrante de Justapaz Álvaro Peña, en las mismas fechas, 65 mil jóvenes fueron reclutados, de los cuales, **menos del 0,7% fueron reconocidos como objetores;** "eso nos lleva a evidenciar una serie de problemáticas en el acceso al derecho de objeción de conciencia". Para el Abogado, ello también indica que hay un desconocimiento del derecho a ser objetor, y una falla en las entidades que deben informar y garantizar este derecho como son las fuerzas militares, el ministerio público y el Ministerio de Educación Nacional.

### **¿Qué es la objeción de conciencia y cómo se declara este derecho?**

Como lo explicó el Abogado, "la objeción no es solo una exoneración para la prestación del **Servicio Militar Obligatorio (SMO)** sino una apuesta de construcción de paz con identidad juvenil"; ello porque representa la forma en los jóvenes pueden expresar su repudio a la violencia y la guerra. En resúmen, un objetor de conciencia es **una persona que por cualquier razón política o religiosa no desea prestar el SMO, y para ello debe formalizar su decisión**.

En primer lugar, el objetor debe presentar una solicitud ante el distrito militar más cercano en el cual indique sus razones para no prestar el SMO; Peña recomendó que buscara acompañamiento de la Personería, Defensoría y otra organización social para que se asesore y vea protegido su derecho a presentar esta solicitud. Posteriormente, las fuerzas militares tienen 15 días para revisar la situación y citar a los solicitantes a algo llamado la comisión interisciplinaria territorial, en la que participan 5 uniformados y una delegada del ministerio público; allí se determina si se acepta o no la solicitud de objeción.

Si la persona es reconocida como objetor de conciencia le deben dar un acto administrativo que reconoce su estatus; si no le reconocen su derecho puede hacer uso del subsidio de reposición y subsidio de apelación, para tener una segunda instancia. Entonces, será una comisión nacional la que resuelva en esta instancia la solicitud que haga el joven; en caso de que esta sea negada, la persona puede acudir a la tutela para ver su derecho protegido, amparado en el artículo 18 de la Constitución política de Colombia.

### **Las barreras de acceso al derecho**

A la falta de información para los jóvenes que se presentan a los distritos militares para resolver su situación militar se suma el incumplimiento en los términos para dar respuestas a las solicitudes de los objetores (de 15 días); y el no reconocimiento en la condición de objetor, si no argumentado en las demás causales de exoneración del SMO expuestas en el **artículo 12 de la[Ley 1861 de 2017](http://www.secretariasenado.gov.co/senado/basedoc/ley_1861_2017.html).**

El Abogado resalta que han observado que no se están cumpliendo las dos instancias, pues cuando se niega en primer momento la solicitud para la objeción, los jóvenes parecen desconocer las herramientas a las que se puede acudir para defender su derecho. En **el 80% de las ocasiones, son los militares que integran la Comisión los que determinan si una persona es objetora o no**; de allí que cerca de la mitad de las solicitudes presentadas entre 2017 y 2018 hayan sido rechazadas.

Por último, Peña resalta que a pesar de que la Corte Constitucional catalogó las 'batidas' como detenciones arbitrarias con fines de reclutamiento, y que la nueva Ley sobre este tema negó evidentemente esta práctica, Justapaz ha evidenciado que la misma se sigue presentando. Por lo que invitó a los jóvenes víctimas de batidas a denunciar, para poder hacer evaluación y control a estas situaciones. (Le puede interesar: ["379 solicitudes de Objetores de Conciencia en el primer año de Ley de Reclutamiento"](https://archivo.contagioradio.com/foro-objecion-de-conciencia/))

<iframe id="audio_35900566" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_35900566_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
