Title: Tres hermanos reclamantes de tierras fueron asesinados en Sucre
Date: 2018-01-30 12:43
Category: DDHH, Nacional
Tags: asesinatos de lideres, Restitución de tierras, Sucre
Slug: tres-hermanos-reclamantes-de-tierras-son-asesinados-en-sucre
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/01/jovenes-asesiandos.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [30 Ene 2018] 

Hombres encapuchados acabaron con la vida de los hermanos** Humberto Manuel Escobar Mercado, de 68 años; Prisciliano Manuel Mercado García, de 63 años, y  Manuel Eusebio Osorio Escobar de 53,** a quienes, la vigilancia privada del exdirector de la Caja de Compensación Familiar de Sucre (Comfasucre), William Martínez, les venía impidiendo trazar los linderos de su finca.

De acuerdo con el relato de los hechos denunciados por el Movimiento Nacional de Víctimas de Crímenes de Estado capítulo Sucre, el hecho aconteció sobre las 8 de la mañana del pasado jueves **25 de enero cuando los hermanos intentaban trazar los límites de su finca 'La Concepción’, en el corregimiento de Guaripa.** En ese momento, fueron sorprendidos por tres personas con los rostros tapados que, "sin mediar palabra y a pesar del llamado al diálogo de uno de los hermanos, dos fueron asesinados inmediatamente con arma de fuego y el tercero quedó gravemente herido y falleció horas después", dice el comunicado del MOVICE.

### **Conflictos por la tierra** 

Según la denuncia a los tres hermanos se les había impedido trazar los linderos de su propiedad, que les fue heredada por su padre. Desde el 2016 y con el respaldo de una orden policial, se les había negado el ingreso. Se trata de una finca conformada por dos parcelas de tierra, una de 692 hectáreas, de la cual eran propietarios los hermanos Escobar, y otro terreno de 608 hectáreas de cuya propiedad es William Martínez.

Pese a que del total de las 1300 hectáreas, Martínez no era dueño ni de la mitad, la denuncia del MOVICE señala que hace 6 años**, el exdirector de Comfasucre ha desterrado por la fuerza a los demás hermanos, dueños de dicha propiedad por herencia de su padre** Humberto Manuel Escobar Mercado.

De acuerdo con los antecedentes, Martínez ordenó tumbar las casas en las que habitaban los hermanos, también los trabajadores encerraban el ganado de la familia Escobar para que muriera de hambre y, en una ocasión, el trabajador al que le dicen ‘el guajiro’ con  su arma le tumbó el celular de una hermana de los fallecidos. Aunque estas denuncias se quisieron llevar el proceso judicial, los abogados renunciaban argumentando que no era posible continuar con el proceso.

Ante los hechos, el MOVICE y los familiares de las víctimas exigen una rápida respuesta e investigación de los hechos para hallar a los autores del crimen, que hoy deja a tres familias sin un padre. Asimismo, **piden a la Unidad Nacional de Protección que acompañe a los familiares de los hermanos Escobar fallecidos,** quienes temen por sus vidas.

<iframe id="audio_23448391" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_23448391_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)
