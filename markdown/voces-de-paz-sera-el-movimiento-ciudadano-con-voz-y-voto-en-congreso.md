Title: “Voces de paz” será el movimiento ciudadano con voz en el Congreso
Date: 2016-12-14 18:26
Category: Nacional, Política
Tags: FARC, voces de paz
Slug: voces-de-paz-sera-el-movimiento-ciudadano-con-voz-y-voto-en-congreso
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/12/voces-de-paz.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

#####  

##### 14 Dic 2016 

Hasta el momento se conoce que los y las integrantes de este movimiento ciudadano que está incluido en el acuerdo de paz y que se encargará de acompañar y vigilar la implementación del acuerdo en el congreso, ya está conformado por 6 personas, integrantes de varias organizaciones sociales.

-**Jairo Rivera**, representante de los estudiantes en las movilizaciones estudiantiles de 2012 y vocero de la mesa amplia nacional estudiantil. Integrante de Marcha patriótica.

-**Jairo Estrada**: Investigador de ILSA y experto en políticas sobre desarrollo rural, fue uno de los cercanos al proceso de conversaciones de paz sobre todo en el tema de desarrollo rural.

-**Imelda Daza**: Sobreviviente de la Unión Patriótica y ex candidata a la gobernación del Cesar , reconocida líder social y defensora de Derechos Humanos.

-**Francisco Toloza**: Defensor de Derechos Humanos e integrante del Movimiento Marcha patriótica. Politólogo de la Universidad Nacional de Colombia.

-**Pablo Cruz**: Investigador e integrante de varios grupos en los que se ha evaluado la reforma política. Cercano al proceso de conversaciones sobre todo en el punto de participación política.

-**Judith Maldonado**: Defensora de Derechos Humanos de Norte de Santander, fue candidata a la gobernación de ese departamento y ha sido protagonista de diversos procesos en el Colectivo de abogados Luis Carlos Pérez.

###### Reciba toda la información de Contagio Radio en [[su correo]
