Title: Candidatos del Tolima firman "pacto por la vida"
Date: 2015-10-06 12:05
Category: Ambiente, Nacional, Política
Tags: Anglogold Ashanti, Comité por la vida, Extracción minera, Extracción minero energética, La Colosa, no a la mina, Si a la vida
Slug: candidatos-del-tolima-firman-pacto-por-la-vida
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/10/Megamineria.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Elsalmonurbano.blogspot.com 

<iframe src="http://www.ivoox.com/player_ek_8824086_2_1.html?data=mZ2flpWceo6ZmKiak5eJd6KklZKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRh8Lixc7Rw9nTt4zYxtGYttTQrc7VjMvW1NLFsoyZk5fdw8jYs4zk0NeYzsaPusrYwoqflJKJe6ShpNTb1sbLrdCfs8bRy9SPcYarpJKh&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Renzo García, Comité Ambiental Tolima] 

###### [6 oct 2015] 

En medio de la carrera por las elecciones a la alcaldía de Ibagué y Gobernación en el departamento del Tolima, el Comité Ambiental, que se opone al proyecto "la Colosa" convocó a los candidatos a que se pronunciaran frente al daño medio ambiental producto de la actividad minera en esta región, con la firma de un **“pacto por la vida”, que pretende ser un compromiso público en defensa del la vida.**

Para la explotación de oro del proyecto minero de “La Colosa”, “se necesitaría una tonelada de roca, lo cual dejaría la montaña como una cantera a cielo abierto, con las fuentes hídricas también se verían comprometidas, debido a la exhiliación, proceso en el que se mezcla agua con cianuro para extraer el oro del agua”, indica Renzo García, miembro del Comité Ambiental del Tolima.

El pacto que firmaron los candidatos de la alcaldía y Gobernación se hizo sobre tres ejes:

-   El compromiso de los candidatos a apoyar actividades medio ambientales en el Tolima
-   Profundizar la democracia a través de consultas populares
-   El rechazo manifiesto a la Colosa por sus impactos negativos

Estos ejes apuntan a la construcción de “proyectos productivos en donde se respeten los derechos de la madre tierra y de la gente”, indica García.

Los candidatos que firmaron este pacto fueron Guillermo Jaramillo, Eduardo Reyes y Carlos García, el candidato por el partido Conservador y aliado del Centro Democrático, Óscar Barreto se abstuvo de firmar, lo que es “un mensaje para que estemos muy atentos por quién vamos a votar”, indica Renzo García. (Ver también: [Aguilas negras amenazan a ambientalistas en el Tolima](https://archivo.contagioradio.com/aguilas-negras-amenazan-a-ambientalistas-que-rechazan-actividades-de-anglogold-ashanti/))

<iframe src="https://www.youtube.com/embed/tQLuwbQuMQk" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>
