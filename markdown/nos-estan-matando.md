Title: Nos están matando, el grito de los líderes sociales en Colombia
Date: 2018-07-09 16:16
Author: AdminContagio
Category: 24 Cuadros
Tags: Amenaza a líderes sociales, Documental, lideres sociales
Slug: nos-estan-matando
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/07/nos-estan-matando.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Fotograma del Documental 

###### 9 Jul 2018 

Con la imagen de la guardia indígena campesina de la vereda Siberia, en el Norte del Cauca, cargando en hombros el féretro de uno de sus compañeros asesinados empieza "Nos están matando, el grito de los líderes sociales en Colombia" un documental dirigido por los estadounidenses Emily Wright y Tom Laffay, con la producción del colombiano Daniel Bustos Echeverry.

La pieza audiovisual de 20 minutos de duración, que tomo más de un año de realización, coincidencialmente se presenta en uno de los momentos más difíciles para los líderes sociales y defensores de derechos humanos en el país que ya sobrepasa la centena en lo que va corrido de 2018. (Le puede interesar: [Estado debe reconocer sistematicidad en asesinato de líderes sociales](https://archivo.contagioradio.com/estado-debe-reconocer-sistematicidad-en-asesinato-a-lideres-sociales-somos-defensores/))

Tom Laffay, uno de sus directores, destaca que en su trabajo como periodista "uno siempre depende de líderes locales que nos conectan con los temas, ofreciendo la mano al mundo para que entiendan lo que esta pasando con sus comunidades y que están trabajando" de ahí la importancia que tienen a nivel comunicativo.

Lo que llamó la atención al equipo de realización y a Laffay como foto-periodista y documentalista fue la necesidad de mostrar la realidad de lo que viene sucediendo con los líderes luego del proceso de paz "esto no tiene que ver con política, son simplemente personas que están defendiendo sus comunidades y están siendo asesinadas" asegura.

Para la producción del documental, el equipo se trasladó por lo menos en 4 ocasiones al Cauca durante 2017, recorriendo poblaciones como Santander de Quilichao, Miranda, Corinto y Popayán. Allí establecieron contacto con el líder de la Guardia Cimarrona Héctor Marino Carabalí y el líder indígena Nasa Feliciano Valencia, quienes finalmente se convirtieron en el rostro visible de las luchas campesinas, afro e indígenas de Colombia.

"Pasamos tiempo con ellos y conocimos más de sus historias, y de manera crítica vimos que los afros e indígenas en la región eran los menos escuchados y son tal vez quienes han sufrido quizás mas que las otras comunidades" apunta Laffay, reflexión que es evidente durante todo el documental.

<iframe src="https://www.youtube.com/embed/IJOKfMaMh3w" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

A pesar de que la carga dramática la encarnan los dos líderes sociales inicialmente se tenía proyectado que fueran tres los protagonistas pero al final como explica el realizador "por las mismas cuestiones de seguridad tanto por su familia como de ella decidimos no incluirla además que su historia de cultivadora de coca también era muy compleja aunque totalmente válida pero al final nos quedamos con estos dos líderes".

En cuanto al título del audiovisual, Laffay destaca que se ha convertido de alguna forma en "el grito de los movimientos sociales y los defensores de Derechos Humanos", un grito que se materializa en los miedos de quienes viven bajo amenaza pero que también muestra la capacidad que tienen para organizarse en comunidad para proteger su vida y defender el territorio.

"Es como un reflejo del mismo movimiento que esta pasando y nosotros nos sentimos que era necesario mostrar las realidades más cercanas, más íntimas que pudimos dando la voz a esos líderes que están viviendo esa situación tan grave para que la gente entienda que significa ser líder social, porque hay mucho desprecio sobre lo que significa liderar"

La viralización del documental en las redes sociales, ha permitido que se organicen algunas muestras en conjunto tanto a nivel local como internacional, lo que ven como positivo por que se hizo pensando en que sirviera como herramienta para que los activistas puedan subrayar el tema.

"Lo más importante es que estamos planeando un viaje a Washington D.C. para estrenar el documental en Capitol Hill, donde algunos congresistas están muy interesados en llamar la atención y exigir acción por parte del gobierno de los Estados Unidos frente a la violencia contra los líderes sociales en Colombia" concluye el realizador.

<iframe id="audio_26971583" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_26971583_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Encuentre más información sobre Cine en 24 Cuadros y los sábados a partir de las 11 a.m. en Contagio Radio 
