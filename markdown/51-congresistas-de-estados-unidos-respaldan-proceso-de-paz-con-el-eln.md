Title: 51 congresistas de Estados Unidos respaldan proceso de paz con el ELN
Date: 2016-06-08 12:14
Category: Paz, Política
Tags: cese bilateral, Estados Unidos, James McGovern, Proceso de paz con el ELN
Slug: 51-congresistas-de-estados-unidos-respaldan-proceso-de-paz-con-el-eln
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/06/McGovern-contagioradio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: divacomunications] 

###### [8 Jun 2016]

En una carta dirigida a John Kerry, Secretario del Departamento de Estado de Estados Unidos, un grupo de 51 congresistas de Estados Unidos reafirmaron su respaldo al proceso de paz con el ELN. Según los legisladores el proceso de paz con esa guerrilla asegura el fin completo del conflicto armado y lo calificaron como **la garantía de “una paz justa y duradera”.**

En la carta hicieron un llamado tanto a **las guerrillas en Colombia como al gobierno a firmar un [cese bilateral](https://archivo.contagioradio.com/conpaz-pide-acelerar-negociaciones-de-paz-con-eln-y-farc/) de fuego** como una medida de garantía para que las conversaciones se desarrollen en calma y [garanticen que no se sigan produciendo víctimas](https://archivo.contagioradio.com/gobierno-debe-dejar-de-dilatar-proceso-de-paz-con-eln/)de desplazamiento que sumarían más de 6 millones o las muerte de más de 220.000 colombianos.

Para los congresistas el apoyo que han dado al proceso de paz con las FARC ha sido una muestra de su compromiso con la paz de Colombia y por ello el  nuevo escenario de conversaciones debe tener en cuenta los avances alcanzados y **garantizar que haya una dinámica propia que incluya “las voces de las víctimas y la sociedad civil** en los diálogos, particularmente las más afectadas por el conflicto armado”

El grupo de legisladores hizo un llamado para que el [ELN](https://archivo.contagioradio.com/?s=eln) entregue a las personas que tienen retenidas y a retirar las minas antipersona. En torno al **[paramilitarismo](https://archivo.contagioradio.com/?s=paramilitarismo) los congresistas recordaron que es una de las mayores preocupaciones** por la cantidad de amenazas y asesinatos a defensores y defensoras de DDHH y la persistencia de grupos de ese “estilo” en amplias zonas del territorio nacional.

Por último **instaron al gobierno de Estados Unidos a mantenerse en el apoyo al proceso de paz con las FARC y al proceso con el ELN** así como en la implementación de los acuerdos que se logren para garantizar que resulten en una paz estable y duradera.

Esta es la traducción de la Carta de los congresistas..."6 de Junio de 2016

##### El Honorable John Kerry Secretario de Estado Departamento del Estado de EEUU 

##### 2201 C St NW Washington DC 20520 

##### Estimado Secretario Kerry: 

##### Damos la bienvenida al anuncio que el gobierno colombiano y el Ejército de Liberación Nacional (ELN), iniciarán formalmente negociaciones de paz. 

##### Durante mucho tiempo hemos apoyado a las negociaciones actuales entre el Gobierno Colombiano y las Fuerzas Armadas Revolucionarias de Colombia (FARC) y la posibilidad de una paz justa y duradera que estos diálogos significarían para el pueblo colombiano. Estos nuevos diálogos con el ELN, el último grupo guerrillero considerable en el país, jugará un papel importante en asegurar un fin completo al conflicto armado que ha resultado en la muerte de más que 220.000 colombianos y colombianas, y el desplazamiento de más que 6 millones. 

##### Una paz completa tiene que tomar en cuenta los avances logrado y las lecciones aprendidas en los diálogos entre el gobierno y las FARC, pero también responder a los temas específicos del ELN y avanzar con eficacia, según sus propios dinámicas y agenda. Instamos a ambas partes a hacer todo lo posible para incluir las voces de las víctimas y la sociedad civil en los diálogos, particularmente las más afectadas por el conflicto armado, incluyendo a mujeres, comunidades afrocolombianas e indígenas, las personas en condición de desplazamiento forzado, y refugiados. 

##### Hacemos un llamado al ELN a liberar todas personas que haya detenida o secuestrada, y a evitar futuros secuestros o detenciones. También hacemos un llamado al ELN a cesar el reclutamiento de menores y la siembre de minas antipersonales, y al ELN y el gobierno colombiano a iniciar lo más pronto posible una iniciativa conjunta para mapear y determinar las zonas donde minas antipersonales continúan amenazando a la vida cotidiana de civiles. 

##### Reconocemos el liderazgo del Presidente Santos en iniciar estos diálogos tan cruciales, pero seguimos preocupados sobre información que indica que la búsqueda de la paz pueda ser amenazada por las actividades de grupos de estilo paramilitar y por los ataques contra líderes comunitarios, defensores y defensoras de derechos humanos, y activistas para la paz en los últimos meses. Instamos al gobierno colombiano a buscar respuestas efectivas a estas amenazas, las cuales afectan a comunidades rurales y urbanas, a defensores y defensoras de derechos humanos y a líderes de movimientos sociales promoviendo la construcción de la paz. 

##### Hacemos un llamado a todas las partes al conflicto a establecer lo más pronto posible un cese multilateral de fuego para poder poner fin a la gran mayoría de la violencia que hace daño a comunidades a lo largo y ancho de Colombia. Tal cese de fuego ayudaría a generar confianza en los diálogos y en las posibilidades para un proceso integral de paz. 

##### Finalmente, expresamos nuestra valoración firme de que los Estados Unidos deben apoyar a este proceso de negociación de paz, tal como ha hecho con el proceso de negociación con las FARC, y deben mantenerse comprometidos a contribuir asistencia para la implementación del acuerdo de paz si se logre un acuerdo final para una paz justa y duradera." 

<iframe src="https://www.youtube.com/embed/ch1j63TCZyY" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>
