Title: Misión internacional pide acelerar investigación por crimen de Berta Cáceres
Date: 2016-03-22 16:19
Category: DDHH, El mundo
Tags: asesinato Berta Cáceres, honduras, Tom Kuchars
Slug: mision-internacional-pide-acelerar-investigacion-por-crimen-de-berta-caceres
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/03/Honduras-Berta.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Articulación Popular Hondureña ] 

###### [22 Mar 2016 ]

De España, Argentina, México, El Salvador, Estados Unidos, Guatemala y Nicaragua provienen los quince delegados, entre activistas y diputados, que conforman la misión internacional que viajó a Honduras para reunirse con embajadas e instituciones estatales para presionar una **investigación independiente que permita esclarecer la verdad** en el [[crimen de la lideresa Berta Cáceres](https://archivo.contagioradio.com/condenamos-asesinato-de-mi-hermana-del-alma-amiga-de-berta-caceres/)], así como garantizar condiciones de no repetición de este tipo de hechos.

De acuerdo con Tom Kucharz, integrante de la 'Campaña Global para Desmantelar el Poder de las Transnacionales y poner fin a la Impunidad', esta misión se realizó con el objetivo de "hacer un llamado internacional de repudio" por el asesinato de la lideresa hondureña, teniendo en cuenta que esta nación se ha convertido en un país peligroso para la defensa de los derechos humanos, **durante los últimos años han asesinado por lo menos a 120 defensores**.

Kucharz afirma que durante esta misión los delegados pudieron constatar que **el pueblo hondureño desconfía de las instituciones jurídicas por su falta de independencia**, malestar al que se suma la nula voluntad política que ha mostrado el Estado para acabar con la impunidad que rodea las violaciones a los derechos humanos de las que han sido víctimas las comunidades afrodescendientes, indígenas y campesinas en Honduras.

El **sistema de protección estatal hondureño falló en el cumplimiento de las medidas cautelares** otorgadas a Cáceres, afirma Kucharz e insiste en que tampoco se atendieron las denuncias que la lideresa había presentado frente al hostigamiento por parte de las fuerzas del Estado. El único testigo del crimen cometido contra Cáceres es el líder mexicano Gustavo Castro cuya vida "corre peligro", según comenta Kuchars, por lo que esta misión se reunió con la embajada de México para presionar **el regreso del líder que las autoridades hondureñas han impedido**.

Las conclusiones y recomendaciones de esta misión internacional serán presentadas en las próximas semanas para contribuir al esclarecimiento del crimen de Berta Cáceres y hacer un llamado al Gobierno hondureño para que revise los **acuerdos de cooperación firmados con otros Estados así como las concesiones a los proyectos mineros e hidroeléctricos** cuya implementación ha generado el mayor número de casos de violación a los derechos humanos.

##### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)]
