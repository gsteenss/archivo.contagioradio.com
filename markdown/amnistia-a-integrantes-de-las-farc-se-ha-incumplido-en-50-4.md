Title: Amnistía a integrantes de las FARC se ha incumplido en 50.4%
Date: 2017-07-12 12:40
Category: DDHH, Nacional
Tags: FARC, Ley de Amnistia, presos politicos, proceso de paz
Slug: amnistia-a-integrantes-de-las-farc-se-ha-incumplido-en-50-4
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/05/crisis-carcelaria1-e1463780527863.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: ICRC] 

###### [12 Jul 2017] 

A 17 días de haber empezado la huelga de hambre y luego de que el Gobierno Nacional anunciara que 3000 personas fueron amnistiadas en las Zonas Veredales, el **proceso de amnistía a integrantes de las FARC acordado en la Habana no llega al 50% del cumplimiento**. Aún continúan 1350 presos políticos en huelga en medio del hacinamiento de las cárceles y la falta de prestación de servicios de salud.

La Oficina del Alto Comisionado para la Paz recibió un listado de las FARC que **acredita a 3421 integrantes como personas detenidas.** Sin embargo, el Gobierno Nacional ha acreditado a 2775 presos en la medida que, para el ellos, 298 requieren clarificar su situación.  De igual forma, son 413 presos beneficiados con la Ley de Amnistía, 618 que están en libertad condicional, 415 en Zonas Veredales y el presidente ha firmado 191 indultos. (Le puede interesar: ["FARC pide a organizaciones internacionales trabajar por libertad de presos políticos"](https://archivo.contagioradio.com/42965/))

Esto quiere decir que, en total, 1700 presos han sido beneficiados por la Ley de Amnistía de los **3421 que deberían ser beneficiados desde el primero de enero**. Para Jesús Santrich, “las amnistías que ha otorgado el Gobierno Nacional no tiene nada que ver con las liberaciones de las personas que se encuentran en los centros penitenciarios, ni siquiera un tercio ha sido excarcelado”.

Santrich afirmó también que hay 2400 miembros de las FARC a quienes **“no se les ha solucionado su situación jurídica y esto agrava la situación”.** Las FARC-EP han manifestado en repetidas ocasiones que “los trámites burocráticos son los que están complicando la implementación de la ley de amnistía porque a algunos presos les continúan imponiendo las condiciones de la justicia ordinaria y no lo establecido por la ley en el marco de los acuerdos de paz”. (Le puede interesar: ["Más de 1400 presos políticos de las FARC continúan en huelga de hambre"](https://archivo.contagioradio.com/presos-politicos-de-las-farc-mantienen-la-huelga-de-hambre/))

### **Segundo mandato de la ONU puede ayudar a solucionar la situación de los presos de las FARC-EP.** 

El 26 de septiembre de este año comienza la segunda Misión del Mecanismo de Monitoreo de la ONU, que según Santrich, **“se supone que tiene un sentido más humanitario”.** Por esto le han pedido a este organismo que “presenten al mundo la situación real que viven los presos políticos después de la firma de los acuerdos de paz”. (Le puede interesar: ["Presos políticos de las FARC exigen que se cumpla Ley de Amnistía pactada"](https://archivo.contagioradio.com/presos-politicos-de-las-farc-exigen-que-se-cumpla-la-ley-de-amnistia-de-los-acuerdos/))

Santrich manifestó que debe haber una solución pronta porque durante la primera Misión de la ONU **“se le hicieron muchas exigencias a las FARC en cuanto a la dejación de armas y con eso cumplimos”**. Es por esto que le hizo un llamado al presidente Juan Manuel Santos para que “como Nobel de Paz resuelva esta problemática por lo menos con carácter humanitario”.

<iframe id="audio_19767192" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_19767192_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU).
