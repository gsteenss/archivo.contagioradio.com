Title: Holding financiero; la jugadita de Duque privatizando los dineros públicos
Date: 2019-11-27 15:19
Author: CtgAdm
Category: Nacional
Tags: Holding Financiero, paquetazo, Paro Nacional, Reforma tributaria
Slug: holding-financiero-la-jugadita-de-duque-privatizando-los-dineros-publicos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/Holding-financiero-la-jugadita-de-Duque-a-favor-de-los-inversionistas-privados.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:] 

La determinación del gobierno de **Iván Duque por adelantar su agenda financiera**, sin tener en cuenta el inconformismo general de la población, es alarmante. Prueba de ello es el decreto del llamado “Grupo Bicentenario”, con el que presidente busca crear un **Holding Financiero estatal** donde converjan varias instituciones financieras del Estado, dicho anuncio ha puesto en alerta a sindicatos y organizaciones trabajadoras al considerarlo una “masacre laboral”.

Para organizaciones, como la **Confederación de Trabajadores de Colombia** (CTC) y la **Central Unitaria de Trabajadores** (CUT), la entrada en vigencia de este decreto y la conformación del llamado Grupo Bicentenario acarreará terribles consecuencias para los trabajadores y los ciudadanos en general, ya que: "elimina el control directo del Estado sobre los dineros de las empresas financieras estatales".

### **¿Qué significa Holding Financiero?** 

Lo primero es entender el término, que en español se traduce como conglomerado. Hay 13 conglomerados financieros en Colombia —como menciona el **analista político Daniel Libreros**— entre los cuales, los más importantes son el del grupo Sarmiento y el del grupo Bancolombia, perteneciente este último al grupo empresarial antioqueño. Entre esos dos conglomerados financieros tienen cerca del 75 % de los fondos de pensiones en Colombia, siendo ellos los más interesados en una reforma pensional.

El Holding consiste —siguiendo con la explicación de Daniel Libreros— en “coordinar todas las empresas bajo un mismo dueño en función de producción monetaria”, esto para que puedan entrar a un mercado de capitales donde se negocian acciones, títulos y una gran inversión financiera. En este caso específico lo que buscaría  Duque, junto al ministro de Hacienda Alberto Carrasquilla, es tomar las empresas del Estado que manejan dinero, es decir, el sector público financiero, tales como: **Icetex, Banco Agrario, Fondo Nacional del Ahorro**, unificándolas bajo un sólo criterio y reestructurándolas administrativamente para que produzcan un monto específico de dinero al mes y envíen ese dinero al mercado de capitales.

### **¿De dónde surge este decreto Bicentenario?** 

Para el analista Libreros, las intenciones del gobierno Duque están íntimamente ligadas a las intenciones que tiene la Organización para la Cooperación y el Desarrollo Económicos (OCDE), el Banco Mundial y el Fondo Monetario Internacional (FMI)  en el país, buscando garantizar condiciones para realizar inversiones del gran capital, es decir, fondos de pensiones, aseguradoras y grandes bancos.

En medio del Paro Nacional que actualmente atraviesa Colombia, este decreto fue aprobado dentro del Plan de Desarrollo, por lo que se expidió como decreto. La rapidez de su ejecución fue justificada por el presidente al señalar que el plazo de autorización legal del Congreso se vencía este 25 de noviembre.

Lo más preocupante, según los detractores del decreto, es que “se pierde el sentido de servicio público y las instituciones financieras del Estado se convierten en intermediarias del mercado de negocios para fortalecer la compra de acciones y de títulos por parte de privados”. Esto es lo que Daniel Libreros llama “Financiarización de las entidades públicas”, es decir, la administración se convierte en un intermediario de negocios. Toma recursos del Estado y convierte ese dinero en parte de los negocios del capital privado, movido por el derecho privado y monitoreado por la Superintendencia, expandiendo los negocios de los inversionistas financieros, haciendo que ese dinero vaya a parar en los negocios de los que intermedian acciones y títulos.

La relación interna de esas empresas —siguiendo con la explicación de Libreros— es de costo-beneficio, buscando que se gire una cantidad de dinero mensual. Si el Fondo nacional del Ahorro, por ejemplo, tiene en su relación costo-beneficio una nómina que es más costo que beneficio, la acción consecuente para mejorar ese déficit será despedir trabajadores, siendo lo más preocupante la misma redacción textual del decreto, donde se autoriza dicho acto.

La relación de producción mensual pasa por encima de cualquier otra consideración. Si el Icetex tiene una tasa de interés que no compensa el negocio, hablando directamente a los endeudados con créditos educativos, lo siguiente es subir la tasa de interés para que se ajuste a la tasa de interés del mercado; una especie de expropiación del patrimonio público en beneficio de los inversionistas privados. [(Le puede interesar: El tal paquetazo sí existe: Oposición)](https://archivo.contagioradio.com/el-tal-paquetazo-si-existe-oposicion/)

### **¿El Estado se convertiría en un “tercer conglomerado”?** 

De ser llevado a cabo, el Holding se convertiría en el tercer conglomerado financiero. La inversión directa del estado seria cercana a 14.5 billones, pero la totalidad de esos patrimonios ronda los 84.5 billones. Esto convertiría al “Estado en un sujeto de negocios del mercado de capitales” permitiendo que el dinero del Estado ingrese al negocio de la bolsa de valores y privatizando los dineros públicos.

Mucho se ha hablado del famoso “paquetazo de Duque”, una serie de reformas en varios ámbitos estatales que afectan directamente a la ciudadanía en general, atacando en específico a los ciudadanos de estratos 1, 2 y 3. Dicho Holding haría parte de estas reformas, ya que “es uno de los puntos más graves del mismo porque significa privatizar todo el patrimonio público que tenga circulación de dinero. Por ejemplo, el Icetex, la cantidad de gente que está endeudada y la crisis familiar que genera. Esto ocasionará una tasa de interés mucha más alta para la familia. Se comienza a trabajar como banco y no como servicio público” menciona Libreros.

Uno de los rechazos más fuertes que hay en la calle —a varios días del Paro Nacional— es la forma en que el capital financiero está erosionando los dineros de las familias de los colombianos, produciendo crisis en los usuarios. Libreros señala que la reivindicación que piden varios sectores de la sociedad "debe ser articulada desde los temas que tienen que ver netamente con el cumplimiento de los derechos fundamentales hasta las grandes decisiones en materia ejecutiva y financiera que está llevando a cabo el presidente Iván Duque y sus ministros".

### **No solo se trata del Holding financiero** 

El Gobierno sostiene que ninguna de las críticas al “paquetazo” son reales, pero con este decreto el panorama se vuelve mucho más complejo. Para el analista, “la reforma tributaria mantiene una línea de continuidad con una política anti-democrática que lleva en Colombia varias décadas, se podría decir que desde la apertura económica de los años 90s y la entrada del neoliberalismo”.

Al eximir a los grandes millonarios de pagar impuestos, el Estado tiene que recurrir a llenar ese hueco con los impuestos de la clase media y baja del país. “Le quitas a los ricos lo que tienen que tributar, y con los 10 billones de pesos que ellos dejan de pagar te ahorrarías toda la reforma tributaria”. Sin embargo, la reacción del Gobierno es aumentar el IVA y “castigar a la clase media y a los más desfavorecidos, algo profundamente antidemocrático”, explica Libreros.

Además de esto el Estado tiene que endeudarse. El presupuesto del año entrante será de 30 billones de pesos, dinero que la ciudadanía tendrá que pagar sólo en intereses de deuda, lo que bien invertido, según el analista político, "serían 6 años de financiación de la universidad pública". La pelea en la calle tiene que volcarse en crear propuestas alternativas y una estructura democrática de tributación ante el Estado —enfatiza Libreros— ya que el que tiene más tiene que pagar más, un criterio mínimamente democrático de lo que debe ser la construcción de patrimonios públicos en un país.

Jurídicamente existen formas de frenar el Holding, lanzando un decreto y tumbando el anterior, por ejemplo, lo importante es que desde la calle se derroten estas decisiones del ejecutivo. “La gente necesita pensar acerca de esto y hacerlo un tema de vital importancia dentro de la coyuntura política”. Para el analista político, si algo ha demostrado la perseverancia y el nivel de cohesión de los ciudadanos en estos días de Paro Nacional ha sido la insatisfacción generalizada frente a un gobierno por el cual no se sienten representados.

Como reflexión final, Libreros deja en claro la importancia de las causas agrupadas dentro del Paro Nacional, pero reafirma la necesidad de centrar todos los esfuerzos en los puntos concernientes a la reforma tributaria que el Gobierno está introduciendo a través del “paquetazo”, ya que, “no hay lugar para las exigencias que están haciendo las personas en la calle —derechos sociales, calidad de vida, mejores condiciones en la educación— si no cambia esta política tributaria y seguimos profundizando la entrega del patrimonio público a las redes privadas de intermediarios financieros”.  [(Le puede interesar: Paro Nacional: una expresión ciudadana de contundente movilización)](https://archivo.contagioradio.com/paro-nacional-una-expresion-ciudadana-de-contundente-movilizacion/)

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

<iframe id="audio_44903593" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_44903593_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
