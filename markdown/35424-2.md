Title: "Este fin de mes estaremos en las Zonas Veredales" Jesús Santrich
Date: 2017-01-30 15:56
Category: Entrevistas, Paz
Tags: acuerdos de paz, niños, Paramilitarismo, Zonas Veredales
Slug: 35424-2
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/01/Zonas-Veredales.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: [NCprensa]

###### 30 Ene 2017 

La falta de adecuación logística de las **Zonas Veredales Transitorias de Normalización**, los problemas en la aplicación de los mecanismos para la salida de menores de las FARC y el paramilitarismo que continúa haciendo presencia en todo el país, son algunas de las dificultades que afronta, en lo práctico, la implementación de los acuerdos, sin embargo “lo que ha habido es disposición absoluta para sacar adelante el acuerdo” afirma Jesús Santrich.

Aunque el gobierno ha afirmado que los materiales están listos para que se inicie la construcción de los alojamientos y otras obras de infraestructura, Santrich afirmó que en la mayoría de los terrenos dispuestos para el albergue de los excombatientes de esa guerrilla no están listos dichos materiales, **“en la mayor parte de los puntos y zonas veredales no hay materiales para la construcción”** de las obras que hacen falta.

Sin embargo Santrich resaltó que a pesar de que los materiales no están si hay una voluntad y disposición de los integrantes de esa guerrilla y resaltó que hay una “propaganda absurda” para decir que por parte de las FARC no hay disposición. El integrante del secretariado afirma que tampoco es que falte voluntad por parte del gobierno sino que puede haber trámites burocráticos que están impidiendo el avance del cumplimiento de los acuerdos.

Así las cosas, Santrich afirma que tienen medio siglo luchando por la paz en medio de muchas dificultades y resaltó que **“si alguien está incumpliendo no son las FARC”** y agregó que van a dar un acompañamiento a nivel nacional a los ex combatientes para que se den las condiciones mínimas del proceso de dejación de armas.

### **Sobre el paramilitarismo** 

El paramilitarismo está en todo el país y hay [denuncias concretas en varias regiones cercanas a los puntos en los que se encuentran las FARC](https://archivo.contagioradio.com/zonas-veredales-bajo-la-sombra-paramilitar/) y cercanos a los sitios en los que se instalarán las zonas veredales, por ello llamó la atención sobre la necesidad de activar los mecanismos para proteger a las comunidades y resaltó que el negacionismo no es la respuesta a este problema mayúsculo. “si no se da eso la paz va a fracasar” resaltó.

### Sobre los menores en las FARC-EP 

Jesús Santrich recordó que para la salida de menores de las FARC-EP hay un plan trazado que esperan que se cumpla, pero también afirmó que hay “manipulación e hipocresía” con la que se ha pretendido mostrar el tema. Las FARC-EP han tenido que recibir a “menores que han quedado sin familia” en el marco de la guerra, incluso afirmó que son una cifra cercana a los 30 menores.

#### **Le puede interesar: [[Protocolo de reincorporación para menores de las FARC fue improvisado](https://archivo.contagioradio.com/el-protocolo-de-reincorporacion-para-menores-que-salieron-de-farc-ep-fue-improvisado/)]** 

Pero Santrich fue más allá, resaltó que no se ha dado socorro a los menores que están muriendo en la Guajira o los que viven en las alcantarillas, además los menores que están en las FARC están protegidos y protegidas, sin embargo el problema radica en que hay que acabar con esas condiciones para todos los niños y niñas de Colombia. Además agregó "Pedimos a los senadores Claudia López  y Mauricio Lizcano que dejen de obstruir la Paz, que ayuden a construir, no a destruir".

Tweet:

https://twitter.com/Contagioradio1/status/826060117749727233<iframe id="audio_16726742" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_16726742_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio]{.s1}](http://bit.ly/1ICYhVU)[.]{.s1}
