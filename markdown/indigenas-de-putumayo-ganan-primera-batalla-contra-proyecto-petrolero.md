Title: Indígenas de Putumayo ganan primera batalla contra proyecto petrolero
Date: 2017-02-18 18:24
Category: DDHH, Nacional
Tags: extracción petrolera, indígenas, Putumayo
Slug: indigenas-de-putumayo-ganan-primera-batalla-contra-proyecto-petrolero
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/06/putumayo-1-e1434052286920.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo] 

###### [18 Feb. 2017] 

**La Corte Constitucional dio a conocer una decisión en la que falla a favor de las comunidades indígenas** del Resguardo Indígena Alto Lorenzo de Puerto Asís y la Asociación del Consejo Regional del Pueblo Nasa del Putumayo, como respuesta a la tutela interpuesta por las comunidades al conocer de las licencias otorgadas para el inicio de un proyecto petrolero en el territorio donde se encuentra ubicada la comunidad.

**La decisión fue tomada gracias a una tutela interpuesta por estas dos comunidades indígenas** contra el Ministerio de Minas y Energía, Ingeominas, la Autoridad Nacional de Licencias Ambientales -ANLA-, Ecopetrol S.A., entre otras instituciones, quienes según los indígenas son los responsables de la vulneración de varios derechos como la integridad personal, la consulta previa, la participación y libre determinación de los pueblos indígenas.

**La Corte resolvió conceder el amparo de los derechos fundamentales a la consulta previa y al territorio colectivo** de estos dos pueblos indígenas, por lo que ordenó al Ministerio del Interior realizar dentro de 15 días hábiles la consulta previa que deberá ser completada en máximo 4 meses.

Así mismo, le ordena a ese ministerio adelantar reuniones  con las empresas e instituciones implicadas en el proyecto petrolero, para **tomar medidas inmediatas que logren subsanar y compensar lo más pronto posible los daños causados** a las comunidades indígenas.

Además ordena a la **ANLA verifique las afectaciones que estos proyectos han causado en el territorio**, al ambiente y a los recursos naturales, daños que han sido denunciados por los órganos de control y “de estimarlo procedente adopte las medidas preventivas” agrega la sentencia. Le puede interesar: [Comunidad indígena en Putumayo dijo NO a actividad petrolera de Amerisur](https://archivo.contagioradio.com/comunidad-indigena-en-putumayo-dijo-no-a-actividad-petrolera-de-amerisur/)

De igual modo, la Corte insta al **Ministerio de Hacienda adelantar estudios técnicos que permitan establecer los impactos ambientales** de estos proyectos petroleros en Putumayo.

Por último, ordena que la ANLA y Corpoamazonía remitan informes cada 4 meses al Tribunal Superior de Mocoa, donde dé cuenta de las actividades que se están desarrollando en el territorio, las afectaciones y las medidas de control y prevención que se están tomando. Le puede interesar: [Comunidades índigenas y campesinas de Guzman y Caicedo rechazan explotación minera en Putumayo](https://archivo.contagioradio.com/pueblo-nasa-se-suma-al-rechazo-de-explotacion-petrolera-en-corregimiento-pinuna-blanco-en-putumayo/)

De esta manera la Corte Constitucional da el aval para que se protejan los derechos fundamentales a la vida en condiciones dignas, la integridad personal, la consulta previa, la participación y libre determinación de estos 2 pueblos indígenas, que se verían afectados por la industria del petróleo.

Además, manda un duro mensaje a las instituciones gubernamentales y empresas petroleras para que respeten el entorno de las comunidades que se ven afectadas por la explotación de hidrocarburos, que según la sentencia estaría generando **“contaminación ambiental, comprometiéndose el derecho al medio ambiente sano, la existencia del equilibrio ecológico, el desarrollo sostenible, entre otros”. **

###### Reciba toda la información de Contagio Radio en [[su correo]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio]{.s1}](http://bit.ly/1ICYhVU)[.]{.s1}

<div class="ssba ssba-wrap">

</div>
