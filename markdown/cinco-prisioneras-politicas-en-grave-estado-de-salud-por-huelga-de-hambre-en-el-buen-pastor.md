Title: Cinco prisioneras políticas en grave estado de salud por huelga de hambre en el Buen Pastor
Date: 2018-11-19 10:54
Author: AdminContagio
Category: DDHH, Mujer
Tags: Buen Pastor, Huelga de hambre, mujeres, Prisioneras políticas
Slug: cinco-prisioneras-politicas-en-grave-estado-de-salud-por-huelga-de-hambre-en-el-buen-pastor
Status: published

###### [Foto: Todo Salta] 

###### [19 Nov 2018] 

Se cumplen 7 días de huelga de hambre de las prisioneras políticas de la Cárcel El Buen Pastor en Bogotá, luego de que se les informará que serían trasladadas de su pabellón a un pequeño pasillo con celdas por ordenes del Ministerio de Justicia. **Hasta el momento, 5 de ellas han sido hospitalizadas y no han tenido respuesta alguna por parte de la institucionalidad.**

Según un comunicado emitido por las huelguistas la decisión de destinar el patio a otro tipo de prisioneras "además de poner en riesgo nuestras vidas, nuestra integridad física, mental y emocional, es un golpe a nuestra dignidad y el desconocimiento de nuestro estatus como prisioneras políticas" (Le puede interesar: ["Prisioneras políticas del Buen Pastor denuncian el posible cierre de su patio"](https://archivo.contagioradio.com/prisioneras-politicas-del-buen-pastor-denuncian-el-posible-cierre-de-su-patio/))

También señalan que apesar del deterioro de su estado de salud y no haber recibido la atención médica necesaria se mantendrán en la protesta dado que un cambio de patio, según ellas, "puede poner en riesgo sus vidas" además de obstaculizar el trámite de sus solicitudes ante la Jurisdicción Especial de Paz.

En total son 23 mujeres, 5 lactantes, las que se encuentran en este patio, y pese a la postura de la administración, afirmaron que continuarán su huelga de hambre hasta tener un comunicado oficial desde el Ministerio de Justicia en donde se retracte de esa orden, además, reiteraron que el cierre del pabellón sexto de la Cárcel El Buen Pastor, **significa para ellas perder las condiciones de dignidad para vivir su reclusión en el centro penitenciario.**

###### Reciba toda la información de Contagio Radio en [[su correo]
