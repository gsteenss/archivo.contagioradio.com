Title: No hay pruebas que validen cargos contra estudiantes de Universidad de Antioquia
Date: 2016-05-11 16:22
Category: Educación, Nacional
Tags: estudiantes, Falsos Positivos Judiciales, Medellin
Slug: no-hay-pruebas-que-validen-cargos-contra-estudiantes-de-universidad-de-antioquia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/05/estudiantes-medellín.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Notiagem] 

###### [11 May 2016] 

Después de la imputación del cargo por terrorismo a los cuatro estudiantes de la Universidad de Antioquia, pertenecientes a la Asociación Colombiana de Estudiantes, se esta llevando a cabo e**l proceso de apelación por las irregularidades de la legalización de captura y medida de aseguramiento**. Los estudiantes aún permanecen en la SIJIN debido a que la Cárcel de Bella Vista no puede recibir más presos.

Las pruebas que según la policía sustentan esta captura aún son muy vagas y se conforman por unas 5 calcomanías que hacían alusión a un grupo insurgente y fotografías de los graffitis realizados en la movilización, [en donde no se demuestra que hayan sido los estudiantes los que los realizaron.](https://archivo.contagioradio.com/detencion-de-4-estudiantes-de-la-aceu-podria-ser-un-montaje-judicial/)

El argumento del Fiscal para aplicar medida de aseguramiento en establecimiento carcelario radicó en un mensaje pintado en una pared que decía "muerte al uniformado" que habría sembrado terror y zozobra en la comunidad. **Sin embargo ninguna prueba demuestra que este mensaje haya sido pintado por alguno de los estudiantes.**

De acuerdo con Carlos Galeano, miembro del comité ejecutivo de la Asociación Colombiana de Estudiantes,  e**xiste una completa parcialidad del juez al tomar la decisión de acusar a los estudiantes de terrorismo** debido a la estigmatización que se tiene en contra del movimiento estudiantil.

El juzgado penal del circuito quien es el que llevaría el **caso aún no ha avocado conocimiento**, es decir, aún no tiene el expediente de la situación, una vez el juez tenga conocimiento del caso se daría un plazo de 15 o 20 días para que se de fecha[a la lectura de apelación interpuesta por los abogados defensores de los estudiantes.](https://archivo.contagioradio.com/estos-son-los-perfiles-de-estudiantes-de-la-u-de-antioquia-detenidos-el-1-de-mayo/)

Por el momento los estudiantes siguen en la dirección  de la SIJIN de la Policía de Medellín, debido a que la crisis carcelaria que vive la cárcel de Bella Vista impide el traslado de los mismos, este hecho afecta la salud y el bienestar de las cuatro personas, **debido a que este lugar no brinda alimentación o elementos de aseo a las personas detenidas.**

El día de hoy estudiantes en Bogotá harán un **plantón en las afueras de la sede del Ministerio de Justicia** para exigir la libertad de sus compañeros y el desmonte de los falsos positivos judiciales, a su vez en Medellín** se realizará una asamblea general en la Universidad de Antioquia para planear la agenda de movilización.**

<iframe src="http://co.ivoox.com/es/player_ej_11495533_2_1.html?data=kpahm5qZd5Shhpywj5aWaZS1kZmah5yncZOhhpywj5WRaZi3jpWah5yncaTV09Hc1ZCrpc3ZwtPch5enb6Ln0MjWw8jNaaSnhqeg0JCns83jzsfWw9PFb8XZjKrg1trIrcLi1crgj4qbh4630NPhw8zNs4zGwsnW0ZCRaZi3jpk%3D&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
