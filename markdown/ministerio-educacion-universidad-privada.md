Title: Al Ministerio de Educación se le salieron de las manos las universidades privadas
Date: 2019-07-10 16:35
Author: CtgAdm
Category: Educación, Entrevistas
Tags: Educación Privada, Huelga de hambre, Ministerio de Educación, universidad
Slug: ministerio-educacion-universidad-privada
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/66389852_2436296906420534_1898914447263531008_n.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: CUT Bogotá  
] 

La **Universidad Autónoma de Colombia** presenta una situación financiera crítica desde 2018: sus profesores han denunciado la falta de pagos en la seguridad social desde diciembre, y deudas en pagos salariales para todos los trabajadores en la Institución desde marzo de este año. Esta situación llevó a una huelga de trabajadores que completa 46 días; pero **el Ministerio de Educación Nacional (MEN) dice que no tiene las herramientas para sacar a la Universidad de la crisis.**

### **La Universidad gasta más de 7.500 millones de pesos en pagos a fundadores**

La situación de la Universidad había sido denunciada anteriormente en [Contagio Radio](https://archivo.contagioradio.com/profesores-universidad-autonoma-meses-sin-salario/); en su momento, los profesores dijeron que hace más de un año iniciaron los retrasos en el pago de sus salarios. A finales de 2018 la situación se agravó, y los profesores dejaron de percibir el pago de su seguridad social; **luego, en febrero dejaron de recibir salarios, situación que se repitió con el resto de trabajadores de la Universidad desde marzo.**

Ante los incumplimientos, los profesores entraron en asamblea permanente. Pero la administración de la Universidad se negó a buscar soluciones al problema, así que también los estudiantes iniciaron una huelga de hambre, que está a punto de alcanzar 2 semanas. (Le puede interesar: ["Universidad Distrital sin presupuesto para el 2018 y con déficit de 12 mil millones"](https://archivo.contagioradio.com/universidad-distrital-sin-presupuesto-para-el-2018-y-con-deficit-de-13-mil-millones/))

Santiago Zanabria, estudiante de derecho de noveno semestre y uno de los huelguistas, explicó que la crisis es profunda porque la administración de la Universidad solo vela por los intereses de los fundadores de la Institución y el MEN guardó silencio mientras todo ocurría. De acuerdo al Estudiante, "la universidad solo está invirtiendo en contratos que cuestan más de 10 millones de pesos para amigos; el 14% de ingreso anual se va para pagar a fundadores o hijos de fundadores de la Universidad: es decir, unos 7.500 millones de pesos".

### **El MEN no tiene herramientas para encontrar soluciones a la crisis**

Zanabria aseveró que los estudiantes no tienen control fiscal sobre lo que se hace con los dineros de las matrículas, y aunque desde el Ministerio se designó un inspector 'in situ', **el mismo funcionario ha dicho que no tiene herramientas suficientes para salvaguardar los derechos de los más de 4.600 estudiantes de la Universidad Autónoma.** Para el Estudiante, lo más grave de la situación es que el problema de su Alma Mater se podría repetir en otras Instituciones como la San Martín, INCA y la CUN, "que también están incumpliendo con los pagos" a sus funcionarios.

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>  
<iframe id="audio_38327981" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_38327981_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
