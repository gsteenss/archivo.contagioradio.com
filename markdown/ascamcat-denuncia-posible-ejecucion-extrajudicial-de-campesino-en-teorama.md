Title: Ascamcat denuncia posible ejecución extrajudicial de Salvador Durán en Teorama
Date: 2020-06-28 14:12
Author: CtgAdm
Category: Actualidad, DDHH
Slug: ascamcat-denuncia-posible-ejecucion-extrajudicial-de-campesino-en-teorama
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/06/WhatsApp-Video-2020-06-28-at-11.51.57-AM.mp4" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/06/WhatsApp-Image-2020-06-28-at-9.28.31-AM.jpeg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

*Foto: Ascamcat*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

La **Asociación Campesina del Catatumbo** (ASCAMCAT), denunció este 27 de junio el **asesinato de Salvador Jaime Durán de 22 años**, en Caño Totumo, municipio de Teorama, [Norte de Santander.](https://archivo.contagioradio.com/ejercito-y-narcotrafico-verdugos-de-la-sustitucion-de-la-coca/) Según la Asociación el hecho es atribuido a 6 miembros del Ejército Nacional.

<!-- /wp:paragraph -->

<!-- wp:quote -->

> *"Durán tenía 22 años de edad, esperaba ser padre de familia y  
> **era afiliado a la Junta de Acción Comunal de la vereda Filo Guamo parte baja**"*
>
> <cite>Ascamcat</cite>

<!-- /wp:quote -->

<!-- wp:paragraph -->

Versiones de la comunidad señalan que sobre las 10:00 am de este sábado en la vereda Filo Guamo, en un lugar conocido como Caño Totumo, ubicado entre los corregimientos El Aserrío y San Juancito, **Durán se encontraba esperando a su padre cuando fue impactado en 6 ocasiones por proyectiles de arma de fuego.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

*"Su padre escuchó los llamados de socorro y acudió ante estos, percatándose de que se trataba de su hijo, por lo cual, **la comunidad del sector inició de forma inmediata la persecución de los presuntos autores materiales del homicidio**"*, informa en un comunicado Asacamcat.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En el comunicado agregan que luego de varios minutos de persecución, la comunidad **logra identificar que se trata de 6 miembros del[Ejército  
Nacional](https://www.justiciaypazcolombia.com/militares-ingresan-a-zona-humanitaria-nueva-vida-poniendo-en-riesgo-la-salud-de-los-pobladores/) de Colombia, integrantes del Comando Operativo Energético número 1**, unidad militar orgánica de la segunda división del Ejército.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Intentando verificar la identidad de los uniformados, la comunidad confronta al Sargento Yunda, encargo de la unidad militar, quien inicialmente niega conocer a los 6 hombres; sin embargo minutos después *"admitió que si estaban bajo su cargo generando esto una gran contradicción y un evidente encubrimiento a los militares que presuntamente cometieron la ejecución".*

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/AscamcatOficia/status/1277247044583710721?s=19","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/AscamcatOficia/status/1277247044583710721?s=19

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:quote -->

> *"En la comisión de verificación, s**e pudieron encontrar elementos como vestigios de sangre, huellas, una prenda militar, residuos de alimentos y una escena del crimen** en donde se puede determinar circunstancias de modo, tiempo y lugar, de esta posible ejecución extrajudicial"*
>
> <cite>Ascamcat</cite>

<!-- /wp:quote -->

<!-- wp:heading {"level":3} -->

### "¡Se presume que fue el Ejército el responsable de esta Ejecución Extrajudicial!"

<!-- /wp:heading -->

<!-- wp:video -->

<figure class="wp-block-video">
<video controls src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/06/WhatsApp-Video-2020-06-28-at-11.51.57-AM.mp4">
</video>
  

<figcaption>
Vocera de Ascamcat | Olga Lucía Quintero

</figcaption>
</figure>
<!-- /wp:video -->

<!-- wp:paragraph -->

Luego de la identificación de los uniformados implicados en el asesinato de Salvador Durán, miembros de la comunidad bajo la figura de captura ciudadana procedieron en realizar un cerco humanitario, informando a los 6 hombres motivo de su retención.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

De otro lado Ascamcat agrega que la *"aprehensión material, se hace con el acompañamiento del personero de Teorama, el alcalde, y miembros de la Asociación"*, además de realizar las comunicaciones correspondientes a los  
superiores militares, Fiscalía General de la Nación, Oficina de Derechos Humanos de la ONU para que se haga efectiva la judicialización de los militares.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Actualmente se espera que a la zona lleguen representantes del Ministerio  
Público, Fiscalía, y demás autoridades que permitan esclarecer los hechos. Finamente la organización exige la ejecución de acciones ***"encaminadas al respeto por la vida, la integridad, la paz y tranquilidad de las comunidades campesinas de la subregión del Catatumbo".***

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Y agregaron que, *"teniendo en cuenta los efectos negativos de las denominadas Zonas Futuro como política de guerra, hoy tenemos un saldo de 4 ejecuciones extrajudiciales en lo que va de 2020".*

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
