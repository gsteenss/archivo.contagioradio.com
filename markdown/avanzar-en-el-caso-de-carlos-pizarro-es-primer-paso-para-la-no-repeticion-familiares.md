Title: Avanzar en el caso de Carlos Pizarro, un paso para la no repetición: Familiares
Date: 2017-01-17 12:06
Category: DDHH, Entrevistas
Tags: Carlos Pizarro, justicia, Maria Jose PIzarro, paz
Slug: avanzar-en-el-caso-de-carlos-pizarro-es-primer-paso-para-la-no-repeticion-familiares
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/01/Maria-Pizarro.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Javier Sulé] 

###### [17 Ene. 2017]

María José Pizarro, hija del asesinado dirigente del M-19 Carlos Pizarro, aseguró para Contagio Radio que **la noticia de la captura de Jaime Ernesto Gómez, ex agente del DAS y escolta de Pizarro alegra a la familia “porque aunque es algo que ya se sabía es la primera vez que el estado reconoce y empieza a avanzar en el caso”**.

**Para la Fiscalía, Jaime Ernesto Gómez podría haber estado cumpliendo algún tipo de orden dentro de todo el plan criminal como la de silenciar al sicario**, razón por la cual el escolta mata al sicario luego de asesinar al líder político para “silenciar al que podría ser un testigo del hecho”.

María José, manifiesta que “no sorprende la posición de los medios que lo muestran – a Jaime Ernesto Gómez- como si fuera un héroe y **muestran esto como un grandísimo avance, aunque reconocemos trabajo de la Fiscalía no es así”.**

Para dar este paso, lo que hizo la Fiscalía fue investigar y darse cuenta de una serie de inconsistencias en la forma que procedió el escolta y en sus versiones entregadas al ente investigador.

Según María José Pizarro, **avanzar en el caso de Carlos Pizarro es una garantía de no repetición en un contexto de paz** y agrega “yo veo con muy buenos ojos esto que está pasando en el caso de Pizarro, en este preciso momento cuando se está hablando de la participación política de miembros de las FARC. Creo que contribuye al camino que inicia las FARC hacia la política”. Le puede interesar: [El fin del conflicto pasa por las Garantías de no repetición](https://archivo.contagioradio.com/el-fin-del-conflicto-pasa-por-las-garantias-de-no-repeticion/)

Para los familiares de Pizarro es importante no olvidar su exigibilidad e interés por conocer quiénes fueron los autores intelectuales “en el sentido de poder saber quiénes ordenaron el asesinato de Pizarro, en todos los niveles del poder”.

Y añade que el tema de la verdad es primordial **“es una verdad que nos deben a nosotros como familia pero también al país y no solo en este caso sino de todos los líderes políticos, sociales, comunitarios que fueron asesinados en los 90 y años posteriores”.**

Luego de 27 años de investigaciones María José Pizarro manifiesta que será difícil avanzar en una paz que pueda transformar la sociedad sino “conocemos toda la verdad de lo que ha acontecido en el marco del conflicto armado en Colombia”. Le puede interesar: [Fiscalía estratifica las víctimas en Colombia: El caso de Carlos Pizarro](https://archivo.contagioradio.com/fiscalia-impide-que-se-interrogue-a-testigo-clave-en-asesinato-de-carlos-pizarro/)

Carlos Pizarro Leongómez fue asesinado en un avión pocos minutos después de despegar desde Bogotá hacia Barranquilla. Para esa época, 1990, Pizarro estaba en campaña presidencial, tan solo unos meses después de lograr firmar un acuerdo de paz y desarme con el Gobierno de Virgilio Barco. Le puede interesar: [“Pizarro”, memoria audiovisual de un padre en ausencia](https://archivo.contagioradio.com/pizarro-memoria-audiovisual-de-un-padre-en-ausencia/)

A propósito de ese contexto y del actual, María José dijo **“Pizarro se jugaba su vida como lo saben los miembros de las FARC. Por ello es importante saber que la paz puede ser dolorosa, pero también queremos lanzar un mensaje de que esta vez no nos juguemos la vida sino que la vida sea parte de la paz”** y concluyó haciendo un llamado a propósito de los asesinatos que han venido aconteciendo en el país.

<iframe id="audio_16426543" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_16426543_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u)  o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](https://archivo.contagioradio.com/).
