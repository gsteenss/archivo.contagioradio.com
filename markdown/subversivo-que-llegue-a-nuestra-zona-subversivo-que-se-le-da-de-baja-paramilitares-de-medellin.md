Title: “Subversivo que llegue a nuestra zona, subversivo que se le da de baja” Paramilitares de Medellín
Date: 2015-10-01 13:10
Category: Nacional, Política
Tags: Autodefensas Gaitanistas, Comunas de Medellín, Conversaciones de paz de la habana, CORPADES, FARC, Fernando Quijano, Paramilitarismo, Urabeños
Slug: subversivo-que-llegue-a-nuestra-zona-subversivo-que-se-le-da-de-baja-paramilitares-de-medellin
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/10/paramilitares_medellin_contagioradio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: las2orillas 

###### <iframe src="http://www.ivoox.com/player_ek_8730961_2_1.html?data=mZygkp6adY6ZmKiak5WJd6KllZKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRaaamhp2dh56nl9bW18rf1c7as4zl1sqYztHJq9bZjMaY0NrJt9XmwpDn0dPFaZO3jNjixNvJttTd19SY09rJcYarpJKw0dPYpcjd0JC%2Fw8nNs4yhhpywj5k%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe> 

###### [Fernando Quijano,  CORPADES] 

######  [1 Oct 2015] 

Según Fernando Quijano, presidente de CORPADES, en Medellín desde que se divulgó la posición del comandante de los Urabeños alias “Pantera” en que afirma que **no aceptarán la presencia de integrantes de las FARC que se reintegren a la vida civil** y que lleguen a los sectores controlados por los paramilitares “se les daría de baja”. Según Quijano a este tema no se le ha prestado la atención suficiente.

En la entrevista a “pantera” publicada el pasado 21 de Junio el comandante de los Urabeños afirma que viene “*Una guerra más, porque nosotros no vamos aceptar; subversivo que llegue a nuestras zona, subversivo que se le da de baja… Si ellos están en diálogo, nosotros también como grupo armado que también defiende a Colombia… no lo vamos a aceptar*” cita el informe de **CORPADES.**

La organización, que se ha dedicado a identificar y denunciar las acciones de los paramilitares también ha logrado esclarecer que los paramilitares en Medellín son más de 2000 y que bajo el nombre de **Autodefensas Gaitanistas de Colombia**, controlan gran parte del territorio antioqueño y que tienen la base de su comandancia en la región de San Pedro de los Milagros en el occidente del departamento.

Según Fernando Quijano, el Estado colombiano debe **implementar de manera urgente mecanismo de desmonte real del paramilitarismo** que sigue intacto y que autoridades civiles y militares se han empeñado en negar y ocultar. Para el presidente de Corpades, el acuerdo que se firmó la semana pasada y que incluye la Jurisdicción Especial para la Paz es un acuerdo “en clave de reconciliación” que debería ser aprovechado.
