Title: 'ChuzaDAS' confirmaría "cacería criminal" durante gobierno Uribe
Date: 2016-03-03 10:50
Category: DDHH, Entrevistas
Slug: chuzadas-confirmaria-caceria-criminal-durante-gobierno-uribe
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/03/ChuzaDas1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: CAJAR ] 

<iframe src="http://co.ivoox.com/es/player_ek_10659142_2_1.html?data=kpWjl56VeJOhhpywj5adaZS1lpqah5yncZOhhpywj5WRaZi3jpWah5yncYammKjV19%2FFiKLHhpekjcjTssfd09LO1Iqnd4a1pcaYh5eWp8LXxteSpZiJhaXVjMjfy9LNssLghpefjcnZtsLi1cqYydTGcYarpJKw0dPYpcjd0JC%2Fw8nNs4yhhpywj5k%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Julián Martínez, Periodísta] 

###### [2 Mar 2016 ] 

Este miércoles en Bogotá el periodista Julián Martínez lanzó su libro ‘Chuzadas, ocho años de espionaje y barbarie’, una ardua investigación que inició en 2009 y da cuenta de cómo “el Gobierno de Uribe convirtió al DAS en una policía política para satisfacer su interés personal” de **espiar, chuzar, desprestigiar, amenazar, seguir y torturar a los sectores de oposición durante su mandato**.

De acuerdo con el periodista "este libro no pretende polarizar", tampoco es su opinión personal, está sustentado en **hechos y documentos que confirman la relación entre los discursos de Álvaro Uribe Vélez y las operaciones ilegales del DAS**, por lo que pretende ser “una contribución a la memoria para que ojala nunca se repitan estos hechos. Creemos que sí hacemos Colombia podrá evitar un escándalo de iguales proporciones más adelante”.

“Si nosotros podemos hacer memoria y reconocer esta verdad no solamente podemos garantizar que no se repitan estos hechos sino tener **capacidad de decisión en el momento de votar por las personas que cometieron este delito**”, afirma Martínez.

Quien acceda al libro podrá saber en qué consistió la **“cacería criminal por parte de agentes del Estado” durante los ocho años de gobierno de Uribe**, la infiltración a la Corte Suprema de Justicia, la campaña de desprestigio contra el ‘Colectivo de Abogados José Alvear Restrepo’, los periodistas Holman Morris y Daniel Coronel, los políticos Gustavo Petro y Piedad Córdoba y los magistrados que investigaban la parapolítica, específicamente Iván Velázquez quien reunió pruebas contra familia de Álvaro Uribe en la conformación y acción de grupos paramilitares.

Según afirma el autor, "**Colombia no ha aprendido del escándalo de las chuzadas, se ha acostumbrado"**, comenzando por las del DAS durante el Gobierno de Álvaro Uribe, continuando con las chuzadas al proceso de paz por parte del Centro Democrático, y ahora las de la Policía, y llama la atención sobre el problema que esconde tras estas prácticas, **“la política de Estado de ver la defensa de los derechos humanos y la investigación periodística honesta, como algo subversivo, como algo sospechoso y que se debe acabar”**.

Martínez asegura que las operaciones del DAS deben ser catalogadas como crímenes de lesa humanidad pues **“llegó a asesinar a defensores de derechos  humanos y sindicalistas por el simple hecho de pensar y mostrar su diferencia y su discrepancia con la seguridad democrática”** e insiste en la gravedad de que millones de documentos que dan cuenta de estos delitos fueron destruidos durante la dirección de Felipe Muñoz Gómez y están siendo destruidos ahora que reposan en la Procuraduría.

Otros de los detalles que revela esta investigación es que las empresas Movistar y Comcel hicieron contratos con el DAS, que Andrés Peñate ordenó pegar frases de Álvaro Uribe en la sala del DAS donde se planeaban las operaciones ilegales, y que **Santiago Uribe participó de las reuniones en las que se coordinó el montaje de alias ‘Tasmania’ contra el magistrado Iván Velázquez**.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)] 
