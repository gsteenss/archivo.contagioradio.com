Title: Este es el texto del nuevo acuerdo entre gobierno y FARC
Date: 2016-11-14 17:08
Category: Otra Mirada, Paz
Tags: acuerdo de paz, FARC, Gobierno, La Habana, paz
Slug: este-texto-del-nuevo-acuerdo-gobierno-farc
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/La-Habana-e1479149127341.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: AFP 

###### [14 Nov 2016] 

Desde hoy todas las personas podrán leer cada una de las páginas del nuevo acuerdo de paz alcanzado entre el gobierno colombiano y la guerrilla de las FARC anunciado el sábado desde La Habana, en voz de los jefes de los equipos nogociadores, Humberto de la Calle e Iván Márquez.

El nuevo documento de 310 páginas, titulado Acuerdo General para la Terminación del Conflicto y la Construcción de una Paz Estable y Duradera, incluye los cambios, precisiones y ajustes en torno a lo referente con el proceso de dejación de armas, la Jurisdicción Especial para la Paz, la restricción efectiva de la libertad, el narcotráfico, el carácter constitucional del acuerdo, el enfoque de género, la comunidad LGBTI y la religión.

"Estamos convencidos que la lectura de todo el documento permite una comprensión integral y genuina de lo acordado, y que los cambios, precisiones y ajustes del nuevo Acuerdo lo fortalecen y responden a las inquietudes y sugerencias hechas por diferentes sectores de la sociedad, preservando a la vez  las reformas y transformaciones contenidas en el Acuerdo del 26 de septiembre, que son la base de una paz estable y duradera", dice el comunicado conjunto No. 5.

<iframe id="doc_93582" class="scribd_iframe_embed" src="https://www.scribd.com/embeds/331065723/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-HhNmxK51z8ICiR4Nbwjd&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="0.7729220222793488"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)
