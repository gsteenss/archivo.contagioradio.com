Title: Sí hay voluntad de los terceros para decir la verdad sobre la guerra
Date: 2019-06-22 12:24
Author: CtgAdm
Category: Paz, Política
Tags: comision de la verdad, Congreso, JEP, verdad
Slug: voluntad-terceros-verdad-guerra
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/Fotos-editadas-2.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Foto: Contagio Radio] 

El pasado jueves en el Congreso de la república la representante a la Cámara María José Pizarro citó a una audiencia sobre la **Jurisdicción Especial para la Paz (JEP), militares y terceros;** evento en el que hablaron personas como **Salvatore Mancuso y David Char**, quienes buscan ser acogidos ante esta justicia. Allí se aclaró la forma en que la Jurisdicción funciona, y se abrió la puerta para que otros actores que participaron en el conflicto se acogieran a esta para aportar verdad y reparación a las víctimas.

Según lo explicó la Representante, lo que buscaban era mostrar otras aristas de la JEP, **"es decir que no es una justicia para las FARC",** sino un tribunal al que ya se han acogido más de 2 mil agentes del Estado y cerca de 300 terceros. Una de las personas que dijo querer acogerse como tercero fue Salvatore Mancuso, su declaración ocurrió en medio de la audiencia, en la que pudo participar a través de internet desde su centro de reclusión en Estados Unidos.

### **"Acogerse a la JEP significa recibir todos sus beneficios"**

Mancuso manifestó que quería acogerse a la JEP para aportar verdad a las víctimas, al tiempo que criticó la Ley de Justicia y Paz; por su parte, David Char, integrante de la casa política más grande actualmente en el caribe, señaló que también acudió a esta Jurisdicción para hablar sobre relaciones políticas con actores violentos que participaron del conflicto.

Estos testimonios fueron vistos por muchos reclusos de las cárceles, quienes se conectaron vía internet. Por esto, Pizarro indicó que está creciendo la voluntad de paz y en la medida que se pueda abrir la ruta, explicar el camino que se debe tomar para acogerse al Sistema Integral de Verdad, Justicia, Reparación y No Repetición, más terceros pedirán ser incluídos.

Adicionalmente, la Representante recordó que los terceros que participaron en el conflicto igual podrían ser vinculados a procesos judiciales ordinarios por declaraciones que se den en la JEP, por lo tanto, acogerse a la justicia transicional de forma voluntaria significa recibir beneficios y acogerse a un sistema de justicia que es único en el mundo. (Le puede interesar: ["La verdad de Mancuso que podría llegar a la JEP"](https://archivo.contagioradio.com/la-verdad-de-mancuso-que-podria-llegar-a-la-jep/))

### **Es una justicia restauradora, que se construye entre víctima y victimario**

Pizarro reiteró que la JEP no es un modelo de beneficios para las FARC; es un paradigma que otorga una serie de miradas distintas a la justicia punitiva y de penas intramurales, cuya construcción se logra del trabajo entre la víctima y victimario. Gracias a ello, **ambos encuentran una sanción restauradora, por lo que es un modelo 'sui generis',** que está siendo estudiado por el mundo.

Por lo tanto, el objetivo de la audiencia también buscaba señalar que **"reducir la JEP a Santrich o a las FARC es engañar al país",** porque lo que ella significa es otorgar una mirada distinta a la lógica con la que se mira la reparación o la justicia, y evitar la impunidad. (Le puede interesar: ["Presidente Duque sanciona la Ley Estatutaria de la JEP"](https://archivo.contagioradio.com/presidente-duque-sanciona-ley-estatutaria-de-la-jep/))

### **¿Qué pasa con los terceros que se oponen a la JEP (y a que se sepa la verdad)?**

La Congresista afirmó que el país está viviendo una pugna, porque hay sectores que le tienen miedo a la verdad, lo que significaría perder capital político cuando se sepa la forma en que construyeron su poder; caso en el que** la JEP podría significar un mecanismo "para salir bien parado ante el país"**, evitando que estos hechos sean tramitados por la justicia ordinaria.

En caso contrario, Pizarro sentenció que no serán bien vistos los poderes locales que no hayan contribuido a la JEP, así como los grupos políticos y económicos que hayan hecho connivencia con grupos armados y se opongan a que se sepa la verdad. En ese sentido, la Representante puso una alerta sobre las empresas que estén financiando el referendo promovido por Herbin Hoyos para derogar la JEP, "porque esas son las que le tienen miedo a la verdad".

La Parlamentaria concluyó que en el Congreso se abrió una puerta para que muchos de los reclusos que están en las cárceles y vieron la Audiencia quieran entrar a la JEP; entonces, se abrió "un espacio que no podemos dejar de lado para equilibrar la carga de temas en la opinión pública" en lo que refiere a la Jurisdicción y el proceso de paz. (Le puede interesar:["Son 1.615 miembros de la Fuerza Pública que aún no se han reportado ante la JEP"](https://archivo.contagioradio.com/son-1-615-miembros-de-la-fuerza-publica-los-que-aun-no-se-presentan-ante-la-jep/))

> [\#ComunicadoDePrensa](https://twitter.com/hashtag/ComunicadoDePrensa?src=hash&ref_src=twsrc%5Etfw) Salvatore Mancuso y David Char ofrecen verdad, reparación y hacen un llamado a la reconciliación. [pic.twitter.com/nngYuA6q0T](https://t.co/nngYuA6q0T)
>
> — María José Pizarro Rodríguez (@PizarroMariaJo) [20 de junio de 2019](https://twitter.com/PizarroMariaJo/status/1141799414060724224?ref_src=twsrc%5Etfw)

<p>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
<iframe id="audio_37417957" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_37417957_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen">
</iframe>
</p>
###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
