Title: La burocracia le cuesta más de $10 mil millones anuales a la Universidad del Tolima
Date: 2016-01-20 16:21
Category: Educación, Nacional
Tags: aspu tolima, profesores universidad del tolima, u del tolima, universidad del tolima
Slug: la-burocracia-le-cuesta-mas-de-10-mil-millones-anuales-a-la-universidad-del-tolima
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/01/ASPU-Tolima.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: ASPU Tolima. ] 

<iframe src="http://www.ivoox.com/player_ek_10153478_2_1.html?data=kpWel5iYe5mhhpywj5aWaZS1kZ6ah5yncZOhhpywj5WRaZi3jpWah5ynca3VjMfi1NTHtsLXysaYzsqPp9bZ1NnOjdKJh5SZopbgjcnJb4amlZadjdLNsIzhytHZ0dPJt4zVz9rOzsqRaZi3jqjc0NnFq8rjjLfOxs7Tb46ZmKialg%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Jorge Gantiva, ASPU- Tolima] 

###### [20 Ene 2016. ] 

[En asamblea permanente se declararon los docentes adscritos a la ‘Asociación Sindical de Profesores Universitarios’ de la Universidad del Tolima ASPU-Tolima, con el fin de “generar **espacios para la reflexión, el debate y la construcción de propuestas concretas** que permitan diseñar salidas a la crisis institucional, administrativa y financiera” por la que atraviesa el alma máter.]

[Jorge Gantiva, presidente de ASPU-Tolima asegura que la crisis financiera es tan aguda que **a la fecha la universidad se encuentra cerrada pues no le han cancelado a ningún trabajador los salarios de diciembre, así como tampoco sus primas y prestaciones**. El presupuesto aprobado para este año es 45% menor que el de 2015, "castigando drásticamente los recursos para la docencia, la investigación y demás tareas misionales”, enfatiza.]

[Gantiva afirma que la universidad del Tolima venía de 3 años de una situación de crisis aguda y "**la actual administración, cómo se ha podido comprobar, despilfarro, engañó, humilló**" **las exigencias que se venían demandando**. "Hoy estamos en el punto más crítico, hasta de crisis humanitaria, porque **son centenares de familias que no tienen para el sustento diario"** y la administración del rector se niega reconocer las enormes falencias del alma mater", agrega. ]

[El cuerpo magisterial denuncia además que la burocracia en la alta administración, no menos de un centenar de funcionarios, "consumen una **suma cercana a los 12.000 millones de pesos al año**, lo que sumado a la nómina paralela representa una enorme carga presupuestal que desangra la institución", por lo que exige al actual rector de la institución, José Herman Muñoz que incluya en su plan de reforma una reestructuración de la alta burocracia. ]

[De acuerdo con varios comunicados, **la agudización de la crisis estructural de la universidad ha dependido de las malas prácticas administrativas y del déficit presupuestal acumulado,** debido al insuficiente financiamiento estatal, el desfinanciamiento y no pago de la deuda histórica por parte de la Gobernación del Tolima y la alta irresponsabilidad de sus directivas que “han permitido un **voraz clientelismo, una alta corrupción y una incapacidad de gobernar eficiente y democráticamente** la institución”.]

[Frente a esta aguda situación el cuerpo docente propone que sean creadas **4 comisiones que cuenten con la participación de los estudiantes** para que se evalúen las acciones en materia financiera, académica y sindical que se requieren para solucionar la crisis estructural de la universidad.    ]

###### [Reciba toda la información de Contagio Radio en su [[correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en [[Otra Mirada](http://bit.ly/1ICYhVU)] por Contagio Radio ] 
