Title: Cambio Radical y los miedos a la Jurisdicción Especial de Paz
Date: 2017-08-30 06:00
Category: Opinion
Tags: Cambio Radical, partidos politicos, Rodrigo Lara
Slug: cambio-radical-y-los-miedos-a-la-jurisdiccion-especial-de-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/08/cambio-radical.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: tnnpoliticas 

#### **Por @KinEscribe**

###### 30 Ago 2017 

En una entrevista radial, Rodrigo Lara, presidente de la **Cámara de Representantes y también jefe la bancada de Cambio Radical**, afirmó que no están en contra de la JEP sino que buscan disipar los miedos que esa jurisdicción ha despertado en sectores de militares, que dicen que esos tribunales serán contra ellos, o de políticos enredados en los negocios facilitados por el paramilitarismo, o de “terceros en la guerra” que temen una cacería de brujas.

Se olvida el señor Lara que uno de los objetivos de la JEP es develar las mentiras de la guerra en Colombia y para ello es necesario que todos, militares, guerrilla, paras, políticos y empresarios garanticen los derechos de las víctimas, especialmente el derecho a la verdad y desde allí avanzar en el camino de la construcción de un orden de cosas diferente al existente que ya ha dejado muchas víctimas.

La discusión de esa ley debería centrarse en permitir que en el sistema político se den los cambios que son necesarios y garantizar la inclusión y la verdad. Así ganarían más que frenando la discusión, hasta el punto de impedir el avance para la aprobación de las 16 circunscripciones especiales de paz que al paso que van, estarían aprobadas en Noviembre o Diciembre cuando ya no se pueden inscribir candidaturas.

Por ejemplo, con la venia de Lara y seguramente Vargas Lleras, se eliminaron artículos de la reforma política que tenían que ver con la edad de los congresistas, causales de pérdida de investidura y cambios en los criterios de manejo de presupuestos de entidades territoriales, todos aspectos en que los partidos tradicionales tendrían mucho que perder y que era necesario impedir para garantizar que las cosas sigan igual en el congreso.

La otra gran oposición de Cambio Radical está en el debate sobre la prohibición del paramilitarismo porque como lo dijo Maria Isabel Rueda “Significaría la aceptación de una responsabilidad con los más amplios efectos jurídicos y patrimoniales.”, argumento, que aunque absurdo, expresa en buena medida lo que piensan los sectores tradicionales del poder político en Colombia.

Seguramente la solidez de la candidatura de Germán Vargas Lleras pasa por esas concesiones que se tejen en las primeras discusiones de la ley estatutaria de la JEP. Es necesario, para lograr esa gobernabilidad, que empresarios, militares y los políticos de siempre, incluyendo a Uribe y los uribistas, estén tranquilos de que allí no les va a pasar nada, es necesario que se sigan ocultando verdades.

Así las cosas el camino está muy difícil para la reforma y la ley estatutaria de la JEP, posiblemente se tendrían que enfilar baterías a lo que se pueda impulsar, teniendo en cuenta que ya arrancó la campaña electoral y al fast track le quedan los días contados.

O mejor no ser derrotistas y empujar con fuerza esas iniciativas o asegurar la unidad para que en un próximo periodo legislativo y de gobierno se puedan hacer las reformas que se necesitan sin tanto recorte y politiquería. Ese es el reto para Voces de Paz y para la ciudadanía que no puede dejar la implementación del acuerdo de fin del conflicto en manos de los mismos de siempre que quieren seguir legislando y gobernando.
