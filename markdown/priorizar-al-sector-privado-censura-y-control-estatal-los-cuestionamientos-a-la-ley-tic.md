Title: Priorizar al sector privado, censura y control estatal: los cuestionamientos a la ley TIC
Date: 2019-06-06 15:32
Author: CtgAdm
Category: Nacional, Tecnología
Tags: ANTV, Ley TIC, Libertad de expresión, televisión pública
Slug: priorizar-al-sector-privado-censura-y-control-estatal-los-cuestionamientos-a-la-ley-tic
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/Ley-TIC.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: @PizarroMariaJo] 

Tras ser aprobada en el Congreso la **Ley TIC**, no solo desaparecerán figuras autónomas como la Autoridad Nacional de Televisión (ANTV), entregando más poderes al Ministerio de Tecnologías de la Información y Comunicaciones, sino que se comercializará el espectro electromagnético dando vía a las grandes empresas y minimizando a las redes comunitarias, una decisión que incide directamente sobre el control y regulación de contenidos a la que tendrán acceso los colombianos.

**La representante a la Cámara, María José Pizarro,** quien se opuso a la aprobación de esta ley desde septiembre del 2018, afirmó que se trata de un proyecto "reencauchado al que le adicionan elementos negativos" que no incorpora una regulación ni una contraprestación económica o de producción de contenidos frente al uso del espectro electromagnético por parte Plataformas como las OTT, Facebook o Netflix, mientras en otros países esta tipo de regulaciones fomentar las producción locales.

### La nueva Ley Tic no escuchó a las comunidades

Por otro lado, resaltó la congresista, al entregar las licencias para el uso del espectro por periodos de 20 años prorrogables a otros 20 años, "se contribuirá al enriquecimiento de las grandes plataformas y no a una sana y libre competencia donde operadoras comunitarias puedan desarrollar sus propias redes" y por tanto ofrecer servicios a las comunidades a nivel territorial.

María José Pizarro indicó que se debió consultar a las comunidades étnicas para que se les permitiera participar en la construcción de esta ley, sin embargo advierte que no solo es un tema que concierne a las comunidades sino a la ciudadanía en general, "¿a qué información o a qué contenidos van a acceder? ¿serán contenidos que fomenten la construcción de paz o serán contenidos que no construyan ciudadanía ni sociedad?", inquirió.

### ¿Será una regulación autónoma?

La representante expresó su preocupación frente a la forma en que serán conformadas las instancias de regulación que reemplazarán a la ANTV pues incluirá cinco funcionarios designados por el Gobierno, que no solo tendrán a su cargo la regulación de contenidos sino los recursos para los medios públicos del país, lo que podría ver comprometida su autonomía. [(Lea también: Colombia debe defender la libertad de expresión y apostarle a la alfabetización digital](https://archivo.contagioradio.com/colombia-debe-defender-la-libertad-de-expresion-y-apostarle-a-la-alfabetizacion-digital/)

Pese a que organizaciones como la Unesco y la Organización para la Cooperación y el Desarrollo Económicos (OCDE) advirtieron sobre los riesgos que podría implicar esta ley, **"sus conceptos no fueron tenidos en cuenta y se aprobó un proyecto que va en detrimento de la autonomía de la producción de contenidos"**, sentenció Pizarro quien señaló que ahora, aprobado este proyecto, será necesaria la veeduría ciudadanía para que se ejerza una correcta distribución de los recursos.

Basándose en los conceptos de las organizaciones y en un análisis al proyecto, los opositores de esta ley presentarán las demandas pertinentes "para velar por los derechos de la ciudadanía y velar por el desarrollo de productos independientes".

<iframe id="audio_36781985" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_36781985_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
