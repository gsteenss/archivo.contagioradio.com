Title: Familias de Ituango retornan  a sus veredas sin garantías
Date: 2020-02-28 17:03
Author: CtgAdm
Category: Comunidad, Nacional
Tags: Desplazamiento, Ituango
Slug: familias-de-ituango-regresan-a-sus-veredas-sin-garantias-del-gobierno
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/02/familias-desplazas-de-ituango.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

*Foto:* [@GabbyG1l](https://twitter.com/GabbyG1l)  

<!-- /wp:paragraph -->

<!-- wp:html -->  
<iframe id="audio_48356372" frameborder="0" allowfullscreen scrolling="no" height="200" style="border:1px solid #EEE; box-sizing:border-box; width:100%;" src="https://co.ivoox.com/es/player_ej_48356372_4_1.html?c1=ff6600"></iframe>  
<!-- /wp:html -->

<!-- wp:paragraph -->

La situación de la comunidades de Ituango (Antioquia) no mejora, y pese a la falta de garantías y seguridad que desplazó a 450 familias de diferentes veredas el pasado 23 de enero hasta la cabecera municipal, **este 28 de febrero se ven obligados a retornar a sus hogares.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

La causal del desplazamiento es producto de la confrontación armada entre las autodenominadas **Autodefensas Gaitanistas de Colombia** (AGC) y las disidencias llamadas **Nuevo Frente 18**, por este territorio, situación que hasta el momento no ha sido atendida.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Según el Comité Territorial de Justicia Transicional (CTJT), **847 personas y 306 familias fueron desplazadas de las veredas de Ituango, entre ellas 14 líderes comunales y 13 excombatientes**. (Le puede interesar: <https://archivo.contagioradio.com/mas-de-450-familias-desplazadas-en-ituango-por-enfrentamientos-armados/> )

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/ISAZULETA/status/1232369879132647424","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/ISAZULETA/status/1232369879132647424

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:heading {"level":3} -->

### Un pliego de peticiones ignorado por completo

<!-- /wp:heading -->

<!-- wp:paragraph -->

Según la líder ambiental y defensora de Derechos Humanos Isabel Cristina Zuleta, el pasado 25 de febrero se realizó el **Comité de Justicia Transicional** en el municipio, un espacio donde participaron diferentes entidades como Procuraduría, Alcaldía, Dirección de Derechos Humanos de Antioquia, Ejército y varios integrantes de la comunidad. (Le puede interesar: <https://archivo.contagioradio.com/garantias-de-vida-y-seguridad-para-excombatientes-se-quedaron-en-el-papel/>)

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En este Comité voceros de la comunidad campesina presentaron un pliego de peticiones donde según Zuleta **exigían puntos sencillos pero elementales** como*, "que la presencia del Estado no sea solo militar, **que no se engañe a las personas diciendo que hay garantías cuando no las hay** y que se de un efectivo cumplimiento de lo pactado en el Acuerdo de paz".*

<!-- /wp:paragraph -->

<!-- wp:quote -->

> Lo que nosotros exigimos es que haya presencia real del Estado, que no se dejen esos vacíos que se está viviendo por parte de los actores armados ilegales que llegan a requisar, y hacer reuniones.
>
> <cite>Isabel Cristina Zuleta - Movimiento Ríos Vivos Antioquia </cite>

<!-- /wp:quote -->

<!-- wp:paragraph -->

Adicional Zuleta señaló que la conclusión por parte de las autoridades fue que las comunidades tenían que regresarían ese mismo día a sus territorios, *"**nosotros sabemos que ésta no es la solución, exigir este retorno sin garantías o atención a las peticiones de los campesinos y excombatientes pone en riesgo sus vidas**"*.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Sin garantías de retorno a Ituango

<!-- /wp:heading -->

<!-- wp:paragraph -->

La líder afirmó también que p**ese a que en el territorio hay presencia activa de Fuerza Pública, esto no garantiza su seguridad, sino que al contrario los ha puesto en situaciones de fuego cruzado**, *"la presión que viven los ciudadanos tiene consecuencias en materia de seguridad, hay enfrentamientos, y la población civil no debería estar en la mitad".*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Y señaló que el principal riesgo que enfrenta la comunidad, es a tener que buscar algún apoyo en la ilegalidad, *"el Estado no responde, pero si presiona a las comunidades a que cumpla con su parte de los acuerdos"*, y finalizó diciendo que es probable que se dé **un mayor desplazamiento, pero está vez no hacia la cabecera municipal, sino directamente a la capital antioqueña.** (Le puede interesar también: [Situación de DDHH en Colombia según la ONU](https://www.justiciaypazcolombia.com/situacion-de-los-derechos-humanos-en-colombia-informe-anual-del-alto-comisionado-de-las-naciones-unidas-para-los-derechos-humanos/))

<!-- /wp:paragraph -->
