Title: Denuncian que empresa bananera desarrolla campaña en contra de la Restitución de tierras
Date: 2020-07-03 17:01
Author: CtgAdm
Category: Actualidad, Nacional
Slug: denuncian-que-empresa-bananera-desarrolla-campana-en-contra-de-la-restitucion-de-tierras
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/05/WhatsApp-Image-2020-05-13-at-11.37.55-AM.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

En los últimos días una empresa bananera del Urabá inició una **campaña de desprestigio contra víctimas de despojo y organizaciones de derechos humanos** que acompañan los procesos de restitución de tierras, en el Bajo Atrato chocoano y el Urabá por medio de varias entrevistas en medios locales.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Esto se da luego de que el Tribunal Superior de Antioquia, entregara el pasado 12 de mayo una sentencia en la que **ordenaba realizar investigaciones a dos empresarios y tres empresas bananeras del Urabá**, entre los empresarios estaban los nombres de **Rosalba Zapata y su hijo Felipe Arcesio **Echeverri****; ** **a quienes se les acusa de [despojo y financiación voluntaria a grupos paramilitares](https://archivo.contagioradio.com/empresas-bananeras-de-uraba-son-investigas-por-concierto-para-delinquir-y-financiacion-de-grupos-armados/).

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En los últimos días la empresaria **Zapata inició, como lo denominaron algunos, *"una campaña de* *desprestigio en contra del la restitución de tierras*", afirmando en medios locales que la comunidad era afectada por la Ley de Víctimas**, acción que se da luego de que le fuera retirada la certificación de comercio justo.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Certificación entregada por la firma europea Flocert, quien retiró a la empresa bananera el certificación del comercio justo, **Fairtrade; el cual le destinaba \$1 millón de dólares anuales** a la **Corporación Rosalba Zapata**.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### *"El certificado no se pierde por los reclamantes de tierras, se pierde por el comportamiento ilegal de los empresarios"*

<!-- /wp:heading -->

<!-- wp:paragraph -->

**Gerardo Vega, Director de la Fundación Forjando Futuros** señaló que esto hace parte de una campaña que buscar quitar credibilidad, ***"es un ataque a la justicia, es un ataque a los jueces y a las instituciones que trabajan en la restitución de tierras**, por parte de algunas de estas empresas bananeras que buscan que no haya restitución".*

<!-- /wp:paragraph -->

<!-- wp:quote -->

> *"**No quieren que la gente reclame las tierras que ellos, de manera dolosa, abusiva y fraudulenta le quitaron** a los campesinos, por medio de amenazas, desplazamiento, y en un muchos casos el asesinato permitieron que estas personas se enriquecieran a cosa de la vida de otros"*
>
> <cite>Gerardo Vega | Forjando Futuros </cite>

<!-- /wp:quote -->

<!-- wp:paragraph -->

Y ante el apoyo que ha tenido esta campaña en los medios locales, Vega señala que es difícil que haya una imparcialidad, y más cuando *"se esta hablando de la restitución de 1**4 predios vecinos al Puerto de Urabá, cuyo valor es muy alto, por eso despojaron a los campesinos, y ahora los medios los justifican**"*.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por otro lado ante el manejo del millón de dólares entregado para fines sociales, Vega afirmó que se trata de un acto abusivo y que solo pretendía justificar las acciones generadas por años por las bananeras, *"**lo que realmente hacían era lavar la cara de la empresa bananera, y ahora dicen que se pierde inversión social para Urabá, no, el dinero se pierde por comportamiento ilegal** de Bananeras de Urabá y de sus propietarios"*

<!-- /wp:paragraph -->

<!-- wp:quote -->

> *"Un empresario decente lo que hace es generar empleo para el país, para él, y para la gente, y no aprovecha conductas ilegales y criminales para apropiarse y enriquecerse"*
>
> <cite>Gerardo Vega | Forjando Futuros</cite>

<!-- /wp:quote -->

<!-- wp:paragraph -->

A esto **Isabel Veláquez, abogada de la Comisión Intereclesial de Justicia y Paz,** agregó *"**no es la primera fundación "fachada" por la cuál se han canalizado recursos nacionales e internacionales** , que aparentemente blindan los proyectos económicos de una supuesta legalidad, pero en el fondo hacen parte de una estrategia de despojo".*

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### El costo de trabajar por la restitución en Colombia

<!-- /wp:heading -->

<!-- wp:paragraph -->

En relación a la campaña de desprestigio la abogada señaló que **esta acción no solamente ha afectado al los integrantes de Forjando Futuros, sino a todas las organizaciones** que hacen acompañamiento a las comunidades reclamantes.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Grupo conformado no solo por la organización que ella integra sino también la Corporación Jurídica Libertad , el Instituto Popular de Capacitación, la Comisión Colombiana de Juristas y el Cinep. (Le puede interesar: [Rechazo de montaje mediático contra el defensor de derechos humanos Gerardo Vega](https://www.justiciaypazcolombia.com/rechazo-de-montaje-mediatico-contra-el-defensor-de-derechos-humanos-gerardo-vega/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Y añadió que *"en el marco del conflicto armado que perdura en la región, esto además, **pone en riesgo a las victimas que nosotros acompañamos, porque son ellos quienes se encuentran en los territorios** en el marco de un contexto de violencia generalizado".*

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### "Las victimas reclamantes de tierra son valientes y capaces"

<!-- /wp:heading -->

<!-- wp:paragraph -->

Según los abogados, estas campañas van más allá del desprestigio, t**ambién tienen como objetivo desarticular los procesos de los reclamantes, por medio de miedo, confusión y persecución;** pero que a pensar de ello, como organizaciones acompañantes confían el desarrollo del debido proceso y la fuerza de las víctimas.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

*"**Las víctimas reclamantes de tierra del Urabá son valientes y capaces**, y a pesar de la pandemia el pasado 12 de marzo fue restituida una tierra a una mujer que ya está ahí, produciendo y viviendo con su familia"*, señaló Vega, y destacó que están atentos a la entrega material de 13 predios más.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Información que complementó la abogada Velásquez afirmando que en contra de esa empresa bananera hay dos decisiones judiciales, una del 12 de marzo que los obliga a devolver 11 predios y otra que es la apertura de una investigación a su propietaria por fraude a resolución judicial, junto con una sanción económica "*por poner en riesgo de plagas fitosanitarias a la región, al abanonar el predio y no entregarlo a la Unidad de Restitución de tierras como debería ser"*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Y concluyó diciendo que como organizaciones esperan que las autoridades den garantías a la vida de las personas que retornan, así como el apoyo a las víctimas y no a los victimarios, ***"invitamos a que la gente no desfallezca en este trabajo, que continué, porqué esa fue su tierra y lo sigue siendo , y tienen que volver ahí a trabajarla"***, enfatizó finalmente Vega.

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->
