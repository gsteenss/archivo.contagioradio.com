Title: The Minga joined actively the National Strike
Date: 2019-04-25 21:39
Author: CtgAdm
Category: English
Tags: Minga, National Strike
Slug: the-minga-joined-actively-the-national-strike
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/WhatsApp-Image-2019-04-25-at-10.58.16-AM.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

After almost a month of mobilizations in which the Southwest **Minga is trying to reach an agreement with the Government with respect to the National Development Plan (PND)**, the indigenous communities of the country joined the National Strike. They claimed modifications to the PND consisting in: immediate measures to end with paramilitarism, adjustments to the security policies in their territories and dismantling of the ESMAD (the Colombian National Police on Riot Control a unit specialized on preventing and/or controlling public disturbances and riots) in respect of the right to life.

 

> [\#ATENCIÓN](https://twitter.com/hashtag/ATENCI%C3%93N?src=hash&ref_src=twsrc%5Etfw)| Los Pueblos Indígenas vamos [\#DeLaMingaAlParoNacional](https://twitter.com/hashtag/DeLaMingaAlParoNacional?src=hash&ref_src=twsrc%5Etfw). ¡Este [\#ParoNacional](https://twitter.com/hashtag/ParoNacional?src=hash&ref_src=twsrc%5Etfw) es por la paz, la vida, nuestros territorios! [@luiskankui](https://twitter.com/luiskankui?ref_src=twsrc%5Etfw) [@FelicianoValen](https://twitter.com/FelicianoValen?ref_src=twsrc%5Etfw) [@MPCindigena](https://twitter.com/MPCindigena?ref_src=twsrc%5Etfw) [@CNTI\_Indigena](https://twitter.com/CNTI_Indigena?ref_src=twsrc%5Etfw) [@concipmpc](https://twitter.com/concipmpc?ref_src=twsrc%5Etfw) [@AcivarpV](https://twitter.com/AcivarpV?ref_src=twsrc%5Etfw) [@OIA\_COLOMBIA](https://twitter.com/OIA_COLOMBIA?ref_src=twsrc%5Etfw) [@CumbreAgrariaOf](https://twitter.com/CumbreAgrariaOf?ref_src=twsrc%5Etfw) [@C\_Pueblos](https://twitter.com/C_Pueblos?ref_src=twsrc%5Etfw). [pic.twitter.com/ybmDvG2RfE](https://t.co/ybmDvG2RfE)
>
> — Organización Nacional Indígena de Colombia - ONIC (@ONIC\_Colombia) [25 de abril de 2019](https://twitter.com/ONIC_Colombia/status/1121434872940257281?ref_src=twsrc%5Etfw)

<p>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
</p>
**In the National Territory**

The mobilization joined by various communities throughout the country marched in defense of agricultural production areas and claimed for a land reform that gives priority to the environment preservation as a fundamental human right, besides in defense of a prior consultation that binds mining-energy projects.

\[caption id="attachment\_65624" align="alignnone" width="478"\]![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/Catatumbo-Paro-Nacional-300x146.jpeg){.wp-image-65624 width="478" height="232"} Foto: Archivo\[/caption\]

In **Buenaventur**a, indigenous people from ACIVA RP (Associations of the Indigenous Municipalities in the Cauca valley) were centered in Puerto Cena. In Antioquia department the Embera Chamí gave support  to the protest in the municipality of Valparaiso and finally in Cundinamarca,  Muisca de Suba Municipality was centered in the Fundacional Square.

In Catatumbo, the extracting plant in the Flores river near **Tibú**, various social groups like“Barí People”part of the Catatumbo Commission for Life Reconciliation and Peace, have mobilized.

![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/Paro-Nacional_Buenaventura-300x225.jpg){.wp-image-65559 .alignnone width="471" height="353"}

**According to Rubiel Liss, member of the communication team of the CRIC** (Indigenous’ Regional Council of Cauca), the mobilizations in the Cauca Region together with the Southwest Minga progressed gathering nearly 3,000 indigenous people in Popayán. In addition, 2,000 people joined the strike from the city of Cali. "The Southwest Minga said that will continue and will be open to meet The President in the Permanent Assembly".

 

https://www.facebook.com/cric.colombia/videos/2314430975549656/

In departments such as **Chocó** from April 24^th^ indigenous “Embera” were concentrated in Tadó near the road Quibdó to Pereira demanded for compliance with the Peace Accords.  Finally, in Caldas the Embera People Chamí de Caldas was mobilized in **Riosucio** and **Manizales** whereas in other departments like **La Guajira**, the Wayúu community have come together to demonstrate in Riohacha. Indigenous communities from all over Colombia took to the streets to reject the National Development Plan and the increase in violence in the country.

\[caption id="attachment\_65626" align="alignnone" width="484"\]![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/Pueblo-Pijao-Paro-300x225.jpg){.wp-image-65626 width="484" height="363"} Foto: Onic\[/caption\]
