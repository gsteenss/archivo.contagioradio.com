Title: Asesinato de Mauricio Vélez corrobora riesgo de líderes sindicales
Date: 2017-06-23 13:34
Category: DDHH, Nacional
Tags: lideres sociales, Represión contra sindicatos, represión contra sindicatos Francia, SIndicatos
Slug: asesinato-de-mauricio-velez-corrobora-riesgo-de-lideres-sindicales
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/06/mauricio-velez-lider-sindical.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Tomada de Twitter] 

###### [23 Jun 2017] 

A dos días del asesinato de Mauricio Vélez, vicepresidente nacional de la organización sindical de Universidades Públicas SINTRAUNAL, sus compañeros  **le exigen al Gobierno Nacional garantías para ejercer esta actividad.** Desde comienzos de 2016 en Colombia han sido asesinados 160 líderes sociales incluidos miembros de diversos sindicatos.

El miércoles a las 9:00pm, en una finca de la propiedad de Mauricio Vélez, ubicada en el corregimiento de Buenos Aires en el departamento del Cauca, ingresaron 12 hombres armados, uniformados y encapuchados. **En ese momento intimidaron a la personas que se encontraban en el recinto y** Procedieron a sacar al señor Vélez del lugar para asesinarlo. (Le puede interesar: ["En 2017 han sido asesinados 41 líderes sociales"](https://archivo.contagioradio.com/en-el-transcurso-del-2017-han-sido-asesinados-41-lideres-sociales/))

Para Juan Carlos Arango, presidente del Sindicato Mixto de las Universidades Públicas del país, **“este asesinato es contra un líder sindical por lo que consideramos que el Estado es quien debe responderle a los colombianos por este crimen”.** Arango manifestó que a su compañero no le habían hecho ninguna amenaza por lo que el crimen es aún más preocupante. "El hecho de que no nos amenacen quiere decir que van actuar directamente contra nosotros".

El asesinato del líder sindical corrobora la alarmante situación de riesgo en la que viven ellos y ellas. Arango manifestó que **“hay una preocupación colectiva por el riesgo en el que nos encontramos los miembros de los sindicatos** porque en Colombia hay un plan de exterminio contra los líderes sociales”. Para él, “hay una ola de violencia a todo lo que implique oposición en el país”. (Le puede interesar: ["Gobierno debe garantizar seguridad para líderes y defensores de DDHH"](https://archivo.contagioradio.com/gobierno-debe-garantizar-seguridad-para-lideres-y-defensores-de-ddhh/))

**Aún no hay información de los autores del crimen de Mauricio Vélez**

Según Arango, “hemos hecho indagaciones para ver si la familia de Mauricio Vélez pudo reconocer a los hombres que cometieron el crimen, pero ha sido muy difícil”. **Ninguna autoridad se ha manifestado por lo sucedido,** por lo que los miembros del sindicato le han pedido a la Fiscalía celeridad para esclarecer los móviles y los autores del asesinato.

El sindicato de Funcionarios de las Universidades Públicas convocó a un plantón el día miércoles 28 de Junio, frente al Ministerio de Justicia en Bogotá. Con esta movilización **quieren exigirle al gobierno nacional un pronunciamiento a cerca de lo sucedido con Mauricio Vélez.** Además exigirán garantías para el ejercicio sindical y quieren demostrarle a los colombianos que la violencia contra ellos no es sólo de amenazas sino que también se evidencia cuando “nos llaman vagos o que acabamos con las empresas”.

<iframe id="audio_19439374" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_19439374_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU).
