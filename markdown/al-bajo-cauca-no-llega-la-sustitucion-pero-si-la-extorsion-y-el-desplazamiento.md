Title: Al Bajo Cauca no llega la sustitución pero si la extorsión y el desplazamiento
Date: 2019-04-16 14:48
Author: CtgAdm
Category: DDHH, Nacional
Tags: Implementación de los Acuerdos de paz, La Caucana, PNIS
Slug: al-bajo-cauca-no-llega-la-sustitucion-pero-si-la-extorsion-y-el-desplazamiento
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/DESPLAZAMIENTO-1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [  
Foto:] 

Desde el 14 de marzo se ha presentado un desplazamiento masivo de más de 70 personas, entre ellas cerca de 19 menores de edad provenientes de La Caucana, municipio de Tarazá hacia Montería y Medellín, coercionados por grupos paramilitares que se han apropiado del territorio del Bajo Cauca; la mayoría de estas familias desplazadas hacen parte del programa de sustitución de cultivos el cual debía brindarles garantías en cuestiones de sostenibilidad y seguridad, sin embargo después de dos años de ser firmados estos compromisos, el Gobierno sigue ausente en la región.

**William Muñoz, presidente de Asociación de Campesinos del Bajo Cauca,** afirma que "es más valioso para nosotros la garantía de seguridad de DD.HH. en el territorio que el mismo desarrollo del programa, por lo menos uno con la vida mira qué hace" además señala que **han sido 13 los líderes asesinados quienes promovían la implementación del programa.**

### **Un Estado Ausente** 

Dentro de los incumplimientos con respecto a la falta de un compromiso por parte del Gobierno con el **PNIS, el ASOCBAC** señala que a la fecha no se cuenta con la contratación de gestores comunitarios, ni un delegado permanente para que resuelva las inquietudes de las familias adscritas al programa, además no se han fortalecido los proyectos de auto sostenimiento, ni se les han otorgado garantías de seguridad para los campesinos que lideran el programa en los territorios

Asímismo manifiestan que el Plan Nacional de Desarrollo  no incluye el **Plan Marco de Implementación del Acuerdo de Paz** y que los Programas de Desarrollo con Enfoque Territorial (PDET) y los procesos de formalización de tierras siguen detenidos.[(Le puede interesar: Fuerza de Tarea Aquiles no evitó desplazamiento de más 120 familias en Bajo Cauca)](https://archivo.contagioradio.com/fuerza-de-tarea-aquiles-no-evito-desplazamiento-de-mas-120-familias-en-bajo-cauca/)

Muñoz indica que en la actualidad, la población se encuentra desmotivada pues el retraso en la implementación de los acuerdos ha sido evidente, **"apenas le han dado a 1400 familias en Tarazá y a 900 familias en Cáceres el respectivo pago del Plan de Atención Inmediata",**  además, ninguno de los planes productivos ha sido formulado a pesar de ser acuerdos firmados en 2017.

### **Extorsiones e ineficacia de la Fuerza Pública** 

También se ha denunciado que los grupos paramilitares están extorsionando a las familias que sí han recibido el pago del Plan de Atención Inmediata, una situación que según el líder, "se veía venir al no existir garantías en el territorio", indicando que los grupos armados buscarían alguna forma financiarse y lo encontraron **al cobrar a cada familia beneficiada una cuota de \$200.000.**

A pesar que cerca de 6.000 soldados están presentes en el Bajo Cauca, su pasividad frente a la situación que se presenta, ha llevado a los pobladores ha sentir desconfianza hacia la Fuerza Pública y suponer que no tienen la capacidad para enfrentar a los grupos armados pues de otro modo "la situación ya se hubiera solucionado",  se cuestiona Muñoz.

<iframe id="audio_34567180" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_34567180_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
