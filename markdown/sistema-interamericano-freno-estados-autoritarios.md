Title: El Sistema Interamericano, un freno a los Estados autoritarios
Date: 2019-05-08 17:55
Author: CtgAdm
Category: DDHH, Política
Tags: Cancillería, CIDH, Derechos Humanos, JEP
Slug: sistema-interamericano-freno-estados-autoritarios
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/Diseño-sin-título-2.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

Esta semana la Comisión Interamericana de Derechos Humanos (CIDH), en el marco del periodo de audiencias 172 que se desarrolla en Jamaica, dejó en firme el encuentro solicitado por la Jurisdicción Especial para la Paz (JEP) para tratar temas relacionados con esta justicia transicional; por su parte, la Cancillería de Colombia cuestionó el encuentro en una comunicación, señalando que la JEP no está facultada para acudir a instancias internacionales.

**El profesor Mario Forero, especialista en relaciones internacionales explicó que este choque de posiciones entre el gobierno colombiano y la CIDH** se debe a un control que se quiere imponer sobre la Jurisdicción. Sin embargo, Patricia Linares, presidenta de este organismo justificó su presencia ante la Comisión en jurisprudencia internacional y la misma le dio la razón, ratificando el encuentro privado para el próximo jueves 9 de mayo.

### **¿Gobierno de Colombia vs CIDH?**

El comunicado de la Cancillería pidiendo la cancelación de la reunión se sumó a [otro](https://id.presidencia.gov.co/Paginas/prensa/2019/190424-Comunicado-de-prensa-del-Ministerio-de-Relaciones-Exteriores-sobre-el-Sistema-Interamericano-de-Derechos-Humanos.aspx) emitido por la misma institución en abril, **en el que se cuestionaba el Sistema Interamericano de Derechos Humanos compuesto por la Comisión y la Corte**.  La declaración recogía las criticas de los gobiernos de Argentina, Brasil, Colombia, Chile y Paraguay en 10 puntos, sobre acciones que debería emprender el Sistema.

Para el Profesor, este comunicado se debe a dos factores: el momento político que vive el continente, que debido a una crisis económica y social ha dado un giro hacía los gobiernos de derecha "que son muy reticentes al tema de derechos humanos; y el control que quieren ejercer los Estados (en este caso el colombiano) sobre las actuaciones de la CIDH, para no verse perjudicados de ninguna forma. (Le puede interesar: ["¿Por qué la Jurisdicción Especial para la Paz irá a la CIDH?"](https://archivo.contagioradio.com/jurisdiccion-especial-para-la-paz-cidh/))

Como lo señaló Forero, el Sistema Interamericano se ha consolidado y ha sido muy importante en la región porque ha sido una instancia a la que han acudido muchas personas cuando no se han tramitado sus procesos adecuadamente al interior de los países. En el caso de Colombia, el Estado "ha sido condenado en muchas ocasiones por no hacer los procesos judiciales indicados"; por esta razón, **el Sistema se convirtió en 'una piedra en el zapato' porque "se sale de su órbita de control"**.

### **¿Tiene fallas el sistema interamericano?**

Entre los 10 puntos mencionados en el comunicado la cancillería señaló diferentes argumentos contra el Sistema; el primero de ellos, que tenía fallas y "serios problemas". Sobre esta crítica, Forero sostuvo que el Sistema Interamericano de hecho es muy bueno, y **hablar de fallas es tratar de 'minar' su trabajo.** Por lo contrario, el Profesor manifestó que es necesario fortalecerlo, como una forma de garantizar justicia cuando los Estados fallan. (Le puede interesar:["Incumplimientos en la restitución de tierras irán a la CIDH"](https://archivo.contagioradio.com/incumplimientos-restitucion-de-tierras-cidh/))

### **¿Hay un atraso de peticiones en la CIDH?**

En otro de los puntos, la Cancillería expresó que hay un atraso en las solicitudes ante la CIDH; pero el profesor declaró que aunque es cierto que hay muchos casos en esta instancia, los mismos están avanzando y se han resuelto. A ello se suma que son los mismos Estados los que terminan entorpeciendo los procesos que lleva la Comisión, porque piden aplazar las audiencias cuando se los cita.

Adicionalmente, el Profesor expuso que **la presentación de tantos casos en instancias internacionales se debe a la poca legitimidad que tienen las instancias internas de cada país**, y la tasa de impunidad que hay en los mismos. (Le puede interesar: ["Ante la CIDH comunidades expondrán fallas en acatamiento de medidas cautelares"](https://archivo.contagioradio.com/ante-la-cidh-comunidades-indigenas-expondran-fallas-en-acatamiento-de-medidas-cautelares/))

### **¿Se está buscando debilitar el sistema interamericano?**

Sobre el final del comunicado, el Ministerio de Relaciones Exteriores concluye que no se puede acusar a estos gobiernos de querer debilitar el Sistema; no obstante, Forero enfatizó que se está buscando afectarlo no solo en temas de dilación si no de competencia, incluso recortando el apoyo económico que se brinda para su funcionamiento. (Le puede interesar: ["CIDH presenta recomendaciones para proteger los líderes sociales"](https://archivo.contagioradio.com/cidh-presenta-recomendaciones-proteger-los-lideres-sociales/))

Pese a estos intentos, el Especialista en relaciones internacionales expresó que tratar de minimizar estos sistemas es difícil en momentos en que la globalización y la internacionalización de los derechos exigen instancias como la Comisión y la Corte IDH; por el contrarío, el Profesor concluyó que los Estados ganarían en legitimidad si se fortalecen estos Sistemas, convirtiéndolos en instancias robustas e importantes.

<iframe id="audio_35570306" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://www.ivoox.com/player_ej_35570306_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
