Title: Alcaldía de Barranquilla desaloja 32 familias en el Tamarindo
Date: 2015-12-09 10:35
Category: DDHH, Nacional
Tags: bandas criminales, Barranquilla, Desalojo, desalojo tamarindo, ESMAD, Los Coletos, Norma Balduvino, policia, Policía Nacional, Tamarindo
Slug: desalojo-tamarindo-barranquilla
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/12/desalojo-tamarindo.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Comisión de Justicia y Paz] 

<iframe src="http://www.ivoox.com/player_ek_9647178_2_1.html?data=mpuhmZabfI6ZmKiakpmJd6Kml5KSmaiRdo6ZmKiakpKJe6ShkZKSmaiRhc3XwtHRh6iXaaK4wpDRx5CmpdPmwtbiy9HQpYzYxtjOztTOpYynk5DTw9LNsMrV1JDS0JDJsIzIwtKah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Norma Balduvino, Comunidad Tamarindo] 

###### [Dic 9 de 2015 ] 

A las 8: 45 de la mañana de este miércoles 9 de diciembre, llegaron a los predios del **Tamarindo cuatro camiones del ESMAD**, hombres de civil con armas blancas y con camisetas rojas para iniciar el desalojo de 32 familias que durante más de 10 años han vivido en este territorio. El cuerpo de desalojo destruyó los cultivos de plátano y yuca; además del sistema de acueducto y tres casas de la zona.

Pese a que la comunidad ha interpuesto acciones legales y jurídicas para dar solución al problema de tierras, el desalojo ya se da por hecho. Norma Balduvino, habitante de Tamarindo y secretaria de Asotracampo expone que el saqueo por parte de la Fuerza Pública atenta contra la dignidad de las personas. "Me siento impotente, como un mendigo pidiendo que no pasen por encima de la comunidad".

Aunque la Defensoría del Pueblo mediante un comunicado, se opuso a que se llevara a cabo el desalojo, **la Inspección General de la Policía hizo caso omiso a esta recomendación y a las acciones de tutela hechas por la comunidad**. Una de ellas, interpuso acciones legales para evitar este tipo de procedimiento, argumentando que el proceso policivo tiene vicios de forma y fondo. Aunque el caso continúa en estudio, se llevó a cabo el desalojo, lo que de acuerdo a la defensa de los pobladores, lo convierte en ilegal e ilegítimo.

Ante este hecho, los habitantes esperan por lo menos poder conservar las posesiones materiales: "no sé qué vamos a hacer, pedimos que nos dejen recoger nuestras cosas y no nos las dañen", afirma Balduvino. Presuntamente, bandas delincuenciales de Barranquilla estarían al servicio de quienes impusieron la orden de desalojo en contra de las comunidades. Los habitantes denuncian que miembros de la banda de ‘Los Coletos’, son quienes están arrasando las parcelas de Tamarindo. Por este motivo, la comunidad teme que les roben las pocas pertenencias que les quedan, puesto que la Policía no ha velado por la protección de los bienes de la comunidad ante posibles robos.

Entre sollozos, Balduvino, quien es madre de dos menores de edad, afirma que desconoce cual será su futuro y el de su familia después de la destrucción de las parcelas y el desalojo de Tamarindo: “no sabemos donde vamos a pasar la noche, no tengo un lugar donde quedarme con mi familia”.

**Las 134 familias que habitaron la población del Tamarindo son víctimas del conflicto armado en Colombia y provienen de poblaciones del Cesar, Magdalena Medio y los Montes de María**. Los operativos de desalojo, han hecho que cada vez, más personas abandonen estas tierras, sin ningún tipo de apoyo o acompañamiento de parte del Estado. El llamado de la comunidad siempre ha sido que se les asigne un territorio digno en donde puedan desarrollar proyectos productivos en el sector rural. La segunda tutela, interpuesta por la comunidad, corresponde a la reubicación de las familias de Tamarindo. Esta diligencia se encuentra en revisión de la Corte Constitucional.[  
](https://archivo.contagioradio.com/desalojo-tamarindo-barranquilla/desalojo-tamarindo-2/)

\

> Qué puede decir uno ante estas injustias en Tamarindo. Indignación y rabia ante este desalojo [\#RT](https://twitter.com/hashtag/RT?src=hash) [pic.twitter.com/1SVz6RNads](https://t.co/1SVz6RNads)
>
> — Justicia y Paz (@Justiciaypazcol) [diciembre 9, 2015](https://twitter.com/Justiciaypazcol/status/674604327428820992)

<p>
<script src="//platform.twitter.com/widgets.js" async charset="utf-8"></script>
</p>

