Title: Las niñas en confinamiento
Date: 2020-04-17 18:49
Author: AdminContagio
Category: Columnistas invitados, Opinion
Tags: Covid-19, cuarentena, Derechos niños y niñas, Niñas
Slug: las-ninas-en-confinamiento
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/04/maltrato_infantil.jpg_1902800913.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:heading {"align":"right","level":6,"textColor":"cyan-bluish-gray"} -->

###### Foto por: La verdad Noticias {#foto-por-la-verdad-noticias .has-cyan-bluish-gray-color .has-text-color .has-text-align-right}

<!-- /wp:heading -->

<!-- wp:heading {"level":5} -->

##### Por: **Renata Cabrales**

<!-- /wp:heading -->

<!-- wp:paragraph -->

He estado pensando en la situación de los niños y las niñas en este momento de crisis. Pienso en los niños y las niñas que padecen  hambre  (en las zonas más vulnerables de Bogotá) a cuyos padres, la alcaldía les mandó el ESMAD por exigir ayuda humanitaria. Pienso en los que están viviendo en medio de la violencia intrafamiliar (que por lo general, es violencia machista). Pienso en aquellas(os) cuya única esperanza de ser felices, por unas horas, era compartir con sus amigos en el colegio, donde incluso, tenían al menos, un plato de comida asegurado. Pienso en los hijas e hijas de migrantes venezolanos que sobrevivían  vendiendo o mendigando en los buses y que ahora van en éxodo de regreso a su país, porque sus padres entendieron que Colombia no era el sueño, sino la pesadilla.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

 Y me duele aún más, pensar en esas niñas que  
tienen hambre, que no tienen ni computador ni internet para las clases  
virtuales; que son las que hacen el oficio, incluso, las que  
"atienden" a los hermanitos, porque las  mamás están ocupadas complaciendo, cada una,  
a su rey-monstruo de la casa.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El monstruo de muchas niñas, el que ya no abusa solo por la noche cuando llega borracho(o no), sino a cualquier hora del día, porque en esta cuarentena, muchas mujeres la están pasando mal debido al confinamiento obligado con su maltratador, ¿Pero, qué pasa con esas niñas que son violadas constantemente, por su propio padre, tío,  abuelo o hermano? En este momento, y no solo en Bogotá,  muchas de ellas tienen hambre, pero también, demasiado miedo.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Le puede interesar: [Nuestros columnistas](https://archivo.contagioradio.com/opinion/columnistas/)

<!-- /wp:paragraph -->
