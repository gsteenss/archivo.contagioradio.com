Title: Comunidades de Jiguamiandó, Chocó, en alerta ante presencia del ELN y neoparamilitares
Date: 2017-11-13 12:54
Category: DDHH, Nacional
Tags: ELN, Jiguamiandó, Paramilitarismo
Slug: eln-y-neoparamilitares_jiguamiando
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/02/Paramilitarismo1-e1459970325314.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Defensoría del Pueblo] 

###### [13 Nov 2017] 

Las comunidades de Jiguamiandó se mantienen en alerta debido a la presencia del ELN y el grupo paramilitar de las AGC. Esta vez, la Comisión de Justicia y Paz, denuncia que debido a posibles enfrentamientos entre AGC y ELN, la población se encuentra en riesgo por la instalación de **minas antipersona y amenazas a líderes y lideresas comunitarios de Jiguamiandó.**

Según la denuncia, el sábado sobre las 5:00 pm, ingresaron a la Zona Humanitaria de Pueblo Nuevo, territorio colectivo de Jiguamiandó, cinco hombres portando armas largas pertenecientes al ELN.

La Comisión señala que los pobladores exigieron a los integrantes del ELN respetar la Zona Humanitaria, pero los integrantes de dicha guerrilla manifestaron que pasaban para informar de la **instalación de minas antipersona y que debían abstenerse de transitar por caminos estrechos y lugares montañosos.** En esa línea, advirtieron que está prohibido el tránsito entre la Zona Humanitaria de Pueblo Nuevo y la Zona de Biodiversidad del Ovo.

### **Presencia paramilitar** 

Tras la amenaza del ELN, el domingo siguiente a las 9.00 a.m hicieron presencia en el mismo territorio seis personas pertenecientes a las autodenominadas Autodefensas Gaitanistas de Colombia, AGC, en la comunidad de Urabá.

La denuncia asegura que tres de ellos portaban camuflados, armas largas, granadas y brazaletes, los otros estaban vestidos de civil y dentro de ellos se identificó al llamado JJ. (Le puede interesar: [Paramilitares irrumpen en Zona Humanitaria en Jiguamiandó)](https://archivo.contagioradio.com/paramilitares-jiguamiando-choco/)

### **Amenanzas a líderes sociales ** 

El pasado 28 de octubre el lider comunitario Willington Cuesta fue amenazado por alias "Chiqui" perteneciente a las AGC, luego que éste exigiera respeto a la comunidad.

Según la Comisión de Justicia y Paz,  "Chiqui" expresó "usted es el hp que me quiere sacar, te voy matar", y aseveró que tenía **un listado de lideres y lideresas de las comunidades de Alto Guayabal, Puerto Lleras, Pueblo Nuevo y Urada, y de Curvaradó para ser asesinados.**

La situación es grave, ya que pese a la cercanía con una unidad del Ejército, cerca de 80 hombres de las AGC, permanecen acampando en la Finca Las te, ubicada a tan sólo cinco minutos del caserío de Urada. Desde allí, a unos 40 minutos se encuentra ese batallón de manera ilegal construido en Llano Rico, Curvaradó.

### **El contexto** 

La ONG refiere que las AGC controlan Belén de Bajirá, Brisas, Apartadocito, Llano Rico, Pavarandó, Mutatá sin acción eficaz del Estado. Asimismo, las unidades del ELN están a menos de una hora de dónde se encuentran las AGC. (Le puede interesar: [Medidas cautelares para comunidad indígena en Jiguamiandó)](https://archivo.contagioradio.com/medidas-cautelares-a-resguardo-indigena-urada-jiguamando-en-choco/)

###### Reciba toda la información de Contagio Radio en [[su correo]
