Title: Indígenas Sikuani víctimas de despojo de tierras en El Porvenir, Meta
Date: 2018-05-09 17:34
Category: DDHH, Nacional
Tags: El Porvenir, Fuerza Pública, indígenas sikuani, Puerto Gaitán
Slug: indigenas-sikuani-victimas-de-despojo-de-tierras-en-el-porvenir-meta
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/05/sikuani.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:] 

###### [09 May 2018]

Las comunidades Sikuani que habitan en el municipio de El Porvenir, en el departamento del Meta, denunciaron que el pasado 7 de mayo, **7 hombres llegaron a su resguardo indígena, en compañía de la Fuerza Pública**, entrando de forma violenta al territorio e instalándose en una parte del mismo, en donde talaron árboles y construyeron una habitación.

Estos hechos fueron denunciados ante las diversas autoridades del municipio, como la Policía, que negaron que hubiese existido acompañamiento de la Fuerza Pública en este hecho. Sin embargo, **los indígenas afirmaron que en las diferentes incursiones que se ha dado contra su territorio, la Fuerza Pública ha hecho presencia**.

En diciembre del año pasado, una situación similar se presentó cuando otro grupo de hombres llegó al territorio alegando ser los dueños de la tierra, también respaldado con integrantes de la Fuerza Pública. En esa ocasión, los indígenas denunciaron que las personas dispararon varias veces contra ellos, sin que la Policía actuara.

De acuerdo con el director de la Corporación Claretiana Normán Pérez Bello, Jaime León, en el territoio hay 60 familias indígenas Sikuani que se han visto afectadas por esta situación.

<iframe id="audio_25884821" frameborder="0" allowfullscreen scrolling="no" height="200" style="border:1px solid #EEE; box-sizing:border-box; width:100%;" src="https://co.ivoox.com/es/player_ej_25884821_4_1.html?c1=ff6600"></iframe>

### **La violencia histórica contra los Sikuanis** 

En el 2016 la Corte Constitucional le exigió a la Agencia Nacional de Tierra restituir **27 mil hectáreas de terrenos baldíos, a 72 familias** de la vereda El Porvenir, en Puerto Gaitán, **en las que se beneficiaban las comunidades indígenas Sikuani.**

Sin embargo, a dos años de ese fallo, aún no se ha realizado la restitución de las hectáreas. Actualmente los indígenas ocupan 5 mil hectáreas, que son de las que los han intentado sacar. Para Jaime León, **los intereses detrás de estos actos violentos se encuentran todavía en los familiares de Víctor Carranza, anterior poseedor de esos predios**.

###### Reciba toda la información de Contagio Radio en [[su correo]
