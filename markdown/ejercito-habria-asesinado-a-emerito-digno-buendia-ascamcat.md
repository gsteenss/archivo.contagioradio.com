Title: Ejército habría asesinado a Emerito Digno Buendía: Ascamcat
Date: 2020-05-18 21:33
Author: CtgAdm
Category: Nacional
Slug: ejercito-habria-asesinado-a-emerito-digno-buendia-ascamcat
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/05/Asesinato-de-Emerito-Digno-Buendia.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:heading {"align":"right","level":6,"textColor":"cyan-bluish-gray"} -->

###### Foto: ASCAMCAT {#foto-ascamcat .has-cyan-bluish-gray-color .has-text-color .has-text-align-right}

<!-- /wp:heading -->

<!-- wp:paragraph -->

Este lunes 18 de mayo en horas de la mañana la Asociación Campesina de Catatumbo [(ASCAMCAT)](https://twitter.com/AscamcatOficia/status/1262403876172546048) denunció que integrantes del Ejército abrieron fuego contra un asentamiento campesino en la vereda Vigilancia, zona rural de Cúcuta, provocando la muerte de Emerito Digno Buendía, y dejando heridas de gravedad a otras dos personas.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### El asesinato de Emerito Digno Buendía

<!-- /wp:heading -->

<!-- wp:paragraph -->

Según relatos de los campesinos Emerito Digno Buendía Martínez, de 44 años, fue alcanzado por una bala en su cabeza mientras huía de los disparos del Ejército contra los campesinos en horas de la mañana de este lunes. Junto a él, Juan José Orozco y Jimmy Alerto González también fueron impactados, pero pudieron ser atendidos en centros asistenciales de salud.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

De acuerdo a ASCAMCAT, Emerito Digno formaba parte de la Junta de Acción Comunal (JAC) de la vereda Tutumito, en zona rural de Cúcuta, formaba parte de la Asociación así como de la coordinadora Nacional de Cultivadores de Coca, Amapola y Marihuana (COCCAM); además, era el padre de seis hijos.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

La organización también confirmó que desde tempranas horas se hicieron las debidas gestiones para que tanto Jimmy como Juan José fueran trasladados a centros hospitalarios en Puerto Santander y Cúcuta, donde se espera que se recuperen de sus heridas. (Le puede interesar: ["Alejandro Carvajal, joven de 20 años asesinado por el Ejército en Catatumbo: ASCAMCAT"](https://archivo.contagioradio.com/alejando-carvajal-joven-de-20-anos-asesinado-por-el-ejercito-en-catatumbo-ascamcat/))

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### La erradicación forzada, y el antecedente de Alejandro Carvajal

<!-- /wp:heading -->

<!-- wp:paragraph -->

Junior Maldonado recuerda que desde el pasado 11 de marzo se iniciaron las operaciones de erradicación forzada en las poblaciones de Sardinata y la zona rural de Cúcuta, en las veredas de Totumito y Nueva Victyoria. También recuerda que el 26 de marzo fue asesinado Alejandro Carvajal en medio de protestas campesinas por las operaciones de erradicación.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En dicha ocasión, el Ejército amenazó previamente a los manifestantes con abrir fuego si seguían las protestas, y posteriormente se cumplió la amenaza. Por esta razón, Maldonado sostuvo como necesario poner fín a los operativos, agregando que es necesario cuidar a quienes habitan en regiones apartadas a las que no ha llegado el coronavirus, para evitar que llegue a través de uniformados.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### En Norte de Santander se declaran en asamblea permanente

<!-- /wp:heading -->

<!-- wp:paragraph -->

En horas de la tarde la Fiscalía realizó el levantamiento del cuerpo de Emerito Digno, en medio de la declaratoria de asamblea permanente por parte de los campesinos. Sus líderes señalan que se mantendrán movilizados para evitar las erradicaciones forzadas y pidiendo el cumplimiento de los acuerdos pactados en el marco del Plan Nacional Integral de Sustitución (PNIS), así como la formulación de nuevos pactos.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

También se conoció la respuesta de la Segunda División del Ejército Nacional que por medio de un [comunicado](https://twitter.com/Ejercito_Div2/status/1262449561559339008/photo/1) se excusó diciendo que los hechos se presentaron en medio de acciones de hecho por parte de campesinos, y que aún son materia de investigación. Asimismo, informó que un uniformado resultó herido.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

La Asociación respondió al comunicado y las declaraciones en medios del comandante de la división Marcos Evangelista Pinto señalando que los campesinos no estaban armados, y por lo tanto el Ejército actuó haciendo uso desproporcionado de la fuerza; y que el soldado herido en una mano recibió un disparo de uno de sus compañeros.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/AscamcatOficia/status/1262561298287603723","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/AscamcatOficia/status/1262561298287603723

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:block {"ref":78955} /-->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->
