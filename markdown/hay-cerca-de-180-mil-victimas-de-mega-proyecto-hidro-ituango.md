Title: Constatan grave situación de derechos humanos por mega proyecto Hidro-Ituango
Date: 2015-03-10 18:11
Author: CtgAdm
Category: DDHH, Nacional
Tags: Ambiente, EPM, Hidroituango, megaproyectos, Movimiento Ríos Vivos
Slug: hay-cerca-de-180-mil-victimas-de-mega-proyecto-hidro-ituango
Status: published

###### Foto: defensaterritorios.wordpress.com 

<iframe src="http://www.ivoox.com/player_ek_4194854_2_1.html?data=lZamlp2ZeI6ZmKialJWJd6KmlZKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRh9DhytjWh6iXaaOnz5DQ0dPas8TVxcaY0tTWb67j187ay8rSuNCfs4qwlYqliNDnjLvW2NTXaZO3jNvS1M7KrcShhpywj6jTstXVyM7cjbfFqMrjjJKSmaiReA%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Isabel Cristina Zuleta, Movimiento Ríos Vivos] 

El sábado 7 de marzo, el Movimiento Ríos Vivos, convocó a la Comisión de Verificación a varios colectivos sociales y defensores de derechos humanos, con el objetivo de evaluar y confirmar la situación que viven alrededor de **180 mil personas víctimas de las obras del proyecto hidroeléctrico Hidroituango de Empresas Públicas de Medellín (EPM),** que han generado violaciones a los Derechos Humanos e infracciones al Derecho Internacional Humanitario, específicamente por los desalojos forzosos.

Durante la visita en terreno, se observaron las dinámicas sociales y culturales, los impactos y daños que está generando el megaproyecto con accionar de la Gobernación de Antioquia, según afirma el informe preliminar de la comisión de verificación.

En el recorrido, los movimientos sociales se vieron obligados a parar en San Andrés de Cuerquia, donde se conoció **el caso de tres mujeres  y un hombre que habían sido detenidos ilegalmente,** mientras ejercían su libre derecho a la protesta social en contra de la obra, lo que ha generado temor en la población que quiere movilizarse para exigir sus derechos. Allí se evidenció la presencia de Policía y ESMAD en un Coliseo público.

“*Ha sucedido varias veces (detenciones ilegales), siempre la primera respuesta es la militar, las mujeres se las llevaron con engaños y las sacaron de su territorios, ellas desconocían el motivo por el las habían capturado*”, afirma Isabel Cristina Zuleta, quien hace parte del Movimiento Ríos Vivos.

Mientras realizaban el recorrido, en la rivera del Río Cauca, **la vigilancia privada hostigó a la comisión y tomó fotografías a las personas que asistían a la visita,** así mismo, “*intentaron requisar a la gente y les pidieron las cédulas*”, denuncia Zuleta.

**La comisión compuesta por 150 personas recibió denuncias por infracciones al DIH y los DDHH.** Los pobladores denunciaron el establecimiento de bases militares en la construcción, ya que el ejercito aprovecha el cese unilateral al fuego, por parte de las FARC-EP, para adquirir posiciones militares amedrentando a la población civil. Así mismo, se denunció violaciones al derecho a la intimidad cuando ingresan a la fuerza a las casas de las familias, y el robo de oro perteneciente a las comunidades.

Tras la visita, se estableció que es importante que “*la sociedad se replantee qué significa un desalojo forzoso, de manera que se desarrolle una campaña en contra de estas actuaciones***”,** afirma la integrante del **Movimiento Ríos Vivos**, y resalta que la legislación colombiana solo acepta el desplazamiento por cuenta del conflicto armado y no por megaproyectos, sin evaluar que se está revictimizando a las personas desalojadas por Hidroituango, quienes ya habían sido desplazados por la guerra.

Finalmente, se propone desarrollar reformas en el Congreso que protejan a las familias donde se desarrollan megaproyectos, siendo Hidroituango un referente de aprendizaje en términos organizativos, sociales y jurídicos, para enfrentar este tipo de obras y defender los derechos humanos.

Cabe recordar que este megaproyecto ya ha generado **4 desalojos forzosos de más de 400 familias, y actualmente están siendo amenazadas otras 80 en la Playa la Arenera.**
