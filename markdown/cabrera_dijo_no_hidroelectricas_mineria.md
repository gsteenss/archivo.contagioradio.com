Title: El 'NO' a hidroeléctricas y minería en Cabrera ganó con el 97.28% de los votos
Date: 2017-02-26 18:42
Category: Ambiente, Voces de la Tierra
Tags: Cabrera, consulta popular, Hidroeléctricas, Mineria
Slug: cabrera_dijo_no_hidroelectricas_mineria
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/02/C5mq01zXQAAW9sF.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: [valentinacmpm]

###### [26 Feb 2017] 

Los habitantes del municipio de Cabrera en Cundinamarca, dijeron NO a la construcción de **14 microcentrales hidroeléctricas de EMGESA en el páramo de Sumapaz. **A la pregunta ¿Está usted de acuerdo si o no, que en el municipio de cabrera como ZRC, se ejecuten proyectos mineros y/o hidroeléctricos, que transformen o afecten el uso del suelo, el agua o la vocación agropecuaria del municipio?, Superado el umbral de 1200 votos, los resultados de los votos por el NO fueron 1465, el SI obtuvo 23, los nulos fueron 5, y los no marcados 13.

La decisión, se toma gracias a la capacidad organizativa de la comunidad y específicamente a la gestión de **la Zona de Reserva Campesina (ZRC)** teniendo en cuenta que la comunidad temía por los impactos negativos que podía acarrear la construcción de las hidroeléctricas al páramo de Sumapaz, especialmente, sobre la Quebrada Torcoroma y el acueducto.

En foros y encuentros diferentes sectores sociales y sus asociaciones de productores, de bienes y servicios, mujeres, jóvenes, Juntas de Acción Comunal, sindicatos, colectivos académicos se había expresado el rotundo rechazo al proyecto hidroeléctrico “El Paso” de la empresa multinacional Emgesa-Enel.

Con el NO en la consulta popular  municipios como **Venecia, Pandi e Icononzo en el Tolima,** también se verían gravemente afectados. El proyecto hidroeléctrico abarca toda la cuenca media del río Sumapáz, desde el nacimiento, en la parte alta que es zona de amortiguamiento del páramo, hasta la desembocadura en el Boquerón de Icononzo. De acuerdo con la comunidad, ese proyecto afectaba "la identidad campesina y la vocación agropecuaria de los municipios" ([Le puede interesar: Habitantes de](https://archivo.contagioradio.com/35010/)Cabrera[decidirán sobre micro-hidroeléctricas en Sumapáz](https://archivo.contagioradio.com/35010/))

“Queremos darle las gracias a todos los que hoy vinieron a acompañar esta jornada, tenemos amigos de Venecia, Sumapaz, Icononzo, Ibagué, Cajamarca, Fusagasugá y Antioquia y **queremos decirles que cabrera los acompaña porque la lucha es de todos los colombianos que luchamos por la naturaleza”** dijo Orlando Romero, líder comunal. El proceso de consulta popular de Cabrera, inició el 1 de agosto de 2016 cuando el Alcalde presentó la solicitud ante el Concejo Municipal.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)
