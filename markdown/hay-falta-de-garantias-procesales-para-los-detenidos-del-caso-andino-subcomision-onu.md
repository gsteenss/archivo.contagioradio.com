Title: “Hay falta de garantías procesales para los detenidos del caso Andino”: Subcomisión ONU Parlamento Alemán
Date: 2017-06-29 13:46
Category: DDHH, Nacional
Tags: Andino, Detenidos de Andino, falso positivo judicial
Slug: hay-falta-de-garantias-procesales-para-los-detenidos-del-caso-andino-subcomision-onu
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/06/DDVPUUuWAAAm0LX-e1498760999237.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Corporación Reiniciar] 

###### [29 Jun 2017]

En una carta enviada al Fiscal General de la Nación, Néstor Humberto Martínez, Heike Hänsel, Presidente de la Subcomisión para Naciones Unidas, Organizaciones Internacionales y Globalización del Parlamento Alemán, manifestó su **preocupación por las falta de garantías procesales en el caso de los detenidos por el caso Andino**.

Hänsel aseguró que **“ha habido faltas al debido proceso y no se les ha protegido el derecho a la presunción de inocencia a estas personas”**. En la carta, la parlamentaria alemana dio cuenta de que “Iván Ramírez tuvo que ser dejado en libertad por tratarse de una detención absolutamente arbitraria. Es de conocimiento público que el sr. Ramírez fue detenido en un bus de transporte del día 24 de junio a eso de las 4pm. No obstante, se incurre en gravísima falsedad al querer dar entender que al momento de la captura el sr. Ramírez se encontraba en su residencia y en supuesta posesión de los elementos de prueba incautados”. (Le puede interesar: ["Proceso contra detenidos en caso andino se desmontará cuando baje la presión de los medios"](https://archivo.contagioradio.com/42883/))

Así mismo, escribe que es preocupante que el material probatorio haya sido filtrado a los medios de comunicación creando “un manto de duda sobre la seriedad con la que se está iniciando el proceso acusatorio”. En lo relacionado con el derecho a la presunción de inocencia, que manifiesta ella ha sido violado, le recordó al Fiscal General que “sus afirmaciones y las del General Jorge Nieto Rojas, al decir que las personas pertenecen al MRP, **vulnera el debido proceso y condena anticipadamente a los y las detenidas**”.

Finalmente, la parlamentaria, le solicitó al Fiscal cesar “las acusaciones y los señalamientos anticipados y que se garantice el debido proceso en el contexto de una investigación seria y responsable”. Teniendo como referente el contexto de construcción de paz en el que vive Colombia, Hänsel, aseguró que de no garantizar el debido proceso a estas personas, **“el postconflicto quedaría como un mito”.** (Le puede interesar: ["Somos inocentes": Cartas de las mujeres capturadas por los hechos en Andino"](https://archivo.contagioradio.com/somos-inocentes-cartas-de-las-mujeres-capturadas-por-los-hechos-en-andino/))

[Parlamento alemán\_Carta al Fiscal General de la Nación\_Heike Hänsel](https://www.scribd.com/document/352547400/Parlamento-alema-n-Carta-al-Fiscal-General-de-la-Nacio-n-Heike-Ha-nsel#from_embed "View Parlamento alemán_Carta al Fiscal General de la Nación_Heike Hänsel on Scribd") by [Contagioradio](https://es.scribd.com/user/276652802/Contagioradio#from_embed "View Contagioradio's profile on Scribd") on Scribd

[Carta al Fiscal General de la Nación\_Heike Hänsel](https://www.scribd.com/document/352609139/Carta-al-Fiscal-General-de-la-Nacio-n-Heike-Ha-nsel#from_embed "View Carta al Fiscal General de la Nación_Heike Hänsel on Scribd") by [Contagioradio](https://es.scribd.com/user/276652802/Contagioradio#from_embed "View Contagioradio's profile on Scribd") on Scribd

<iframe id="doc_83284" class="scribd_iframe_embed" src="https://www.scribd.com/embeds/352609139/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-1MiZMYdSsn1ZRQR7SuJV&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="0.7080062794348508"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU).

###### ** ** 
