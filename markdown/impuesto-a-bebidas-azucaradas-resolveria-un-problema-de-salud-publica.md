Title: Impuesto a bebidas azucaradas resolvería un problema de salud pública
Date: 2016-06-01 12:10
Category: Economía, Nacional
Tags: Bebidas azucaradas, cáncer, Diabetes, obesidad
Slug: impuesto-a-bebidas-azucaradas-resolveria-un-problema-de-salud-publica
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/05/Bebidas-azucaradas.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Ecosfera 

###### [26 May 2016] 

El pasado 17 de mayo se realizó un debate de control político en la Comisión Tercera de Cámara de Representantes, donde el grupo de expertos presentó su propuesta frente a lo que podría ser la reforma tributaria que busca el gobierno.

Uno de los temas que quedó a la espera y sin una propuesta clara tiene que ver con la posibilidad de fijar un impuesto a las bebidas azucaradas, como los refrescos o las gaseosas. Desde el año pasado el Ministro de Salud, Alejandro Gaviria ha propuesto esta idea como una iniciativa que podría servir, **no solo para enfrentar un problema de salud pública, sino además para recaudar cerca de  2 billones de pesos para la atención de las enfermedades derivadas del consumo de azúcar y establecer programas de prevención.**

Aunque desde el gobierno aún no existe una postura clara frente a ese tema, pues está en análisis y a la espera de propuestas, como lo aseguró el ministro de Hacienda, Mauricio Cárdenas, lo cierto, es que más allá de llenar un hueco fiscal, lo que se busca es también es “manejar la economía con mucha responsabilidad”, como lo aseguró el ministro, quien añadió que al gobierno le gusta **“un concepto de un Estado efectivo en cerrar brechas, un gasto con gran contenido y función social”, d**e tal manera que la problemática de la obesidad, diabetes, cánceres, entre otras enfermedades causadas por el consumo de bebidas azucaradas, podrían ir vinculadas con la propuesta de implementar un impuesto para este tipo de productos.

Y es que se trata de un problema social y de salud pública. Para que una enfermedad, sobre todo no transmisible, sea considerada un tema de salud pública, significa que las tasas de prevalencia se han incrementado, dicho de otra forma, al igual que una epidemia, se afecta a un porcentaje alto de la población, al ser enfermedades no transmisibles lo hace aún más grave. Así lo ha expresado la OMS frente a la obesidad y la diabetes:  “**La obesidad constituye un problema de salud pública que se ha calificado como la “epidemia del siglo”.**

Colombia no es ajena a esta realidad,  según datos del ministerio de salud y la ENSIN, el exceso de peso en adultos es de 51,2% y en niños es de 17,5%, esto más sus patologías asociadas representan 5 de las 10 primeras causas de mortalidad reportadas en 2010, enfermedad isquémica cardiaca, diabetes, accidente cerebrovascular, enfermedad hipertensiva cardiaca y cáncer (MSPS Ministerio de Salud y Protección Social, 2015).  La tasa anual de muertes asociadas al consumo de bebidas azucaradas es de 112 por millón de habitantes. **Es decir 4.928 personas mueren al año por enfermedades asociadas al consumo de gaseosas.**

A nivel departamental, 18 departamentos se encuentran por encima de la media nacional (52%) en las tasas de prevalencia de obesidad y diabetes. De igual forma, algunos departamentos, presentan las mayores prevalencias de retraso en talla y anemia entre los niños, y a su vez las mayores prevalencias de exceso de peso entre los adultos. Estos departamentos son: Vichada, Guainía, Nariño y Atlántico.

**Colombia tiene 2,2 millones de adultos con diabetes y es el segundo país de América Latina con más personas entre 20 y 79 años padeciendo esta enfermedad. ** De acuerdo con estudios de la Federación Internacional de Diabetes, de no generarse desde ya una estrategia para enfrentar esta situación, para 2035 en Latinoamérica habrá cerca de 38 millones de diabéticos y en el mundo podría haber hasta 205 millones. Sumado a eso, se debe tener en cuenta que la diabetes tiene un costo social muy alto, pues se estima que 1 de cada 12 adultos la padece y sus complicaciones a largo plazo pueden llevar a incapacidad permanente o incluso la muerte.

Es por ello que distintos sectores esperan que haya una decisión del gobierno frente a este tema, de manera que la reforma tributaria tenga un propósito  social, de salud pública y no solo fiscal. **Proponen es que el Estado cumpla con sus compromisos de respeto, protección y garantía de los derechos a la salud, la alimentación adecuada y la vida de las y los colombianos.**

Por todo esto, consecuentes con la OMS, con la comisión de expertos y la Federación de Departamentos; se coincide en la pertinencia de un impuesto a las BBAA , al tabaco y las BB alcohólicas cuyo éxito radica por un lado en la reducción del  consumo de bebidas azucaradas, prevalencias de obesidad y diabetes, general, pero sobre todo en niños y adolescentes, reducción de muertes  asociados a  consumo de bebidas azucaradas, diabetes y obesidad, costos de atención por enfermedades no transmisibles, que hoy se elevan a 755%,  y por el otro lado,  dado que hoy el segmento de las bebidas azucaradas no cuenta con un impuesto salvo, los denominados jugos de frutas que hoy se gravan con la tarifa general del 16%, al gravarlos, se obtendría una ganancias para las finanzas departamentales.

En ese sentido, el próximo 3 de junio a las 8 de la mañana, se realizará una audiencia pública sobre impuestos saludables en la que participarán el ministros Alejandro Gaviria Trujillo, los congresistas  Oscar Ospina Quintero y  Jaime Delgado Zegarra y la directora Educar Consumidores Esperanza Cerón Villaquirán.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
