Title: Asesinan a José Antonio Rivera excombatiente de FARC
Date: 2020-07-14 16:24
Author: PracticasCR
Category: Actualidad, Nacional
Tags: acuerdo de paz, Excombatientes de FARC, FARC, José Antonio Rivera
Slug: asesinan-a-jose-antonio-rivera-excombatiente-de-farc
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/07/José-Antonio-Rivera-excombatiente-de-FARC.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"textColor":"cyan-bluish-gray","fontSize":"small"} -->

José Antonio Rivera / Foto: Partido FARC

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El día de ayer en horas de la mañana **fue asesinado el excombatiente de FARC y firmante de paz José Antonio Rivera quien se encontraba en proceso de reincorporación.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El crimen se produjo en un taller de mecánica ubicado en el municipio de Pitalito, Huila, donde  José Antonio se encontraba cuando hombres armados irrumpieron y le propinaron al menos cuatro disparos.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

**José Antonio estaba haciendo su proceso de reincorporación supervisado por la Agencia para la Reincorporación y la Normalización –[ARN](http://www.reincorporacion.gov.co/es)-** y hacía parte de Cooagropaz, cooperativa que agrupa a excombatientes y víctimas del conflicto en actividades productivas.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Este hecho se suma al asesinato del que fue víctima el también excombatiente y firmante de paz Fredy Fajardo Ávila quien fue asesinado el pasado fin de semana en Uribe, Meta. (Lea también: [Siguen los asesinatos contra líderes y excombatientes en medio de la pandemia](https://archivo.contagioradio.com/siguen-los-asesinatos-contra-lideres-y-excombatientes-en-medio-de-la-pandemia/))

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### José Antonio Rivera se suma la larga lista de excombatientes asesinados

<!-- /wp:heading -->

<!-- wp:paragraph -->

El Partido FARC señala que con el asesinato de José Antonio Rivera, son **218 los excombatientes asesinados** desde la firma del Acuerdo de Paz en 2016. (Le puede interesar: [Partido FARC solicitará formalmente medidas cautelares de la CIDH a favor de excombatientes](https://archivo.contagioradio.com/partido-farc-solicitara-formalmente-medidas-cautelares-de-la-cidh-a-favor-de-excombatientes/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

**140 de estos asesinatos se presentaron bajo el mandato de Iván Duque en la Presidencia y solo en 2020 se habrían presentado 34 homicidios en contra de los firmantes de paz, **lo que ha llevado a FARC a señalar que están siendo víctimas de un **«exterminio sistemático»**.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/PartidoFARC/status/1282764920187162625","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/PartidoFARC/status/1282764920187162625

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

Frente a este escenario de violencia, **al menos 93 personas entre excombatientes en proceso de reincorporación y miembros de la comunidad** del Espacio Territorial de Reincorporación y Capacitación (ETCR) «Santa Lucía» en el municipio de Ituango, **serán trasladadas el día de mañana hasta el área rural de Mutatá en el Urabá antioqueño, por cuenta de las amenazas contra su vida e integridad de las que denuncian haber sido víctimas.**

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
