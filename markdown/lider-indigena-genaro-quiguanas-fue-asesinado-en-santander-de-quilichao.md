Title: Líder indígena, Genaro Quiguanas fue asesinado en Santander de Quilichao
Date: 2019-11-09 12:48
Author: CtgAdm
Category: DDHH, Líderes sociales
Tags: Asesinato de indígenas en Colombia, Cauca, Santander de Quilichao, Toribío
Slug: lider-indigena-genaro-quiguanas-fue-asesinado-en-santander-de-quilichao
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/CRIC.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: CRIC] 

El Consejo Regional Indígena del Cauca (CRIC) informó la mañana de este 9 de noviembre sobre el asesinato del líder indígena **Genaro Quiguanas Ipia** en Santander de Quilichao, Cauca. Según la Oficina de Derechos Humanos de la Organización de las Naciones Unidas en Colombia se han documentado más de 50  asesinatos contra habitantes del territorio indígena Nasa en 2019.

Según la información recibida, el líder indígena quien fue autoridad de  la vereda La playa **Tacueyó, en Toribío**, habría sido asaltado con arma de fuego, cuyos disparos causaron heridas de grave causando su muerte.  [(Le puede interesar: Cinco personas asesinadas y varios heridos deja atentado contra comunidad indígena de Tacueyó)](https://archivo.contagioradio.com/dos-personas-asesinadas-y-varios-heridos-deja-atentado-contra-comunidad-indigena-de-tacueyo/)

El asesinato de esta autoridad indígena ocurre dos semanas después del ataque del que fueron víctimas integrantes de las comunidades indígenas en la vereda La Luz, en Tacueyó y que dejó como resultado seis personas heridas y  muertas entre ellas la lideresa Cristina Taquinas Bautista.  [(Le puede interesar: Autoridades indígenas responsabilizan a Duque de masacre en Tacueyó)](https://archivo.contagioradio.com/autoridades-indigenas-responsabilizan-a-duque-de-masacre-en-tacueyo/)

### Militarización a territorio indígena no soluciona el problema

Como respuesta a las crecientes agresiones contra el pueblo indígena en Colombia, y en particular en el departamento del Cauca, el Gobierno del presidente Iván Duque dio la orden de incrementar el pie de fuerza militar en la zona con el despliegue de 2.500 nuevos  efectivos. [(Lea también:Militarizar más al Cauca es una propuesta "desatinada y arrogante": ACIN)](https://archivo.contagioradio.com/militarizar-mas-al-cauca-es-una-propuesta-desatinada-y-arrogante-acin/)

**Dicha propuesta, han declarado los pueblos indígenas, va en contravía de las soluciones planteadas por sus autoridades** quienes aseguran que la militarización únicamente llevará más violencia al territorio, en lugar de promover la inversión social y el cumplimiento del Acuerdo de Paz en especial el punto de sustitución de cultivos de uso ilícito, usados por los grupos armados que usan los corredores del Cauca como rutas para el narcotráfico.

Noticia en desarrollo...  
**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
