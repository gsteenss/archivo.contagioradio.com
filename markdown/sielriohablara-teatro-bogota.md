Title: Nueva temporada de "Si el río hablara" en La Candelaria
Date: 2017-03-29 11:00
Category: eventos
Tags: Bogotá, memoria, Si el rio hablara, teatro la candelaria
Slug: sielriohablara-teatro-bogota
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/03/Si-el-rio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Teatro La Candelaria 

###### 28 Mar 2017 

Luego de su estreno en 2013 "Si el río hablara" obra dirigida por Cesar "Coco" Badillo regresa al Teatro la Candelaria en una nueva temporada; poniendo sobre la mesa el papel de la memoria de las víctimas, en tiempos en que la búsqueda de una paz duradera, y al derecho a la verdad con justicia social hace parte del diálogo nacional.

"Tratar de sanar, esa es la historia que se teje en 'Si el río hablara', y eso es lo que necesita el país. En la medida en que reconozcamos los dolores, se puede avanzar. Si como sociedad no nos reconocemos en el problema, vamos a seguir aplazando las soluciones, y va a estallar más adelante, que es lo que nos ha pasado con la guerra" expresa Badillo, agregando que "No se trata de hablar de 'postconflicto' como una moda, como un tema superficial, sino de reflexionar. Esa es la tarea de la sociedad, y en ese camino, del Teatro".

La producción teatral,  se presenta del 29 de marzo al 8 de abril en el Teatro La Candelaria de miércoles a sábado a partir de las 7:30 de la noche. Le puede interesar: [Presupuesto para la cultura se redujo en 32.000 millones para 2017](https://archivo.contagioradio.com/presupuesto-para-cultura-se-redujo-en-32-mil-millones-para-2017/)

<iframe src="https://www.youtube.com/embed/_YKT4Vq42SA" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

SINOPSIS: Dos mujeres y un hombre quieren salir de esa zona gris que es la pérdida del sentido de la vida, ocasionado por la guerra. Los cuerpos sin vida han dejado sus huellas inscritas en el agua, y permanecen suspendidas en el tiempo quebrado e inconcluso. "Mujer, Poeta y Devota", son los personajes encargados de crear las circunstancias para que el río hable.

FICHA TÉCNICA: Creación colectiva de Nohra Gonzalez Reyes, Alexandra Escobar Allión, Cesar Badillo. Asesoría dramaturgia de Felipe Vergara y el grupo. Escenografía de Mónica Bastos. Música de Edson Velandia. Construcción de muñecos por "Hombre del Sol". Dirección general por Cesar Badillo.
