Title: Músicos chilenos se unen para salvar el Río Maipo
Date: 2017-01-26 17:26
Category: Cultura, Otra Mirada
Tags: Cajón del Maipo, Chile, Hidroeléctricas, Música
Slug: maipo-chile-hidroelectrica-musica
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/01/Foto-7.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### Foto: Maipo Libre 

##### 26 Ene 2017 

Un grupo de artistas chilenos apoyados por varias organizaciones artisticas, sociales y medio ambientales del pais austral, son los responsables de **'Maipo Libre', un disco que busca generar conciencia sobre la amenaza que representa el desarrollo un mega proyecto hidroeléctrico privado para los ríos y valles que hacen parte del Cajón del Maipo**.

**20 composiciones originales** integran el trabajo producido por Tomás González, miembro de la Coordinadora Ciudadana Ríos del Maipo y Santiago de la Cruz, quienes se encargaron de recopilar todos los temas compuestos por artistas como **Los Vásquez, Camilo Eque, La Banda en Flor, Los del Maipo, Evelyn Cornejo** en un mismo albúm.

El disco, incluye la canción **“No Alto Maipo” grabada por 18 artistas locales**, entre los que destacan Cholomandinga, Santiago Rebelde, Sepamoya, Amahiro de Isla de Pascua, Santo Barrio, Iguana, Ska Prensao, Chocloneta y Carito Plaza entre otros, tema del cual se rodo un video clip, que cuenta con imágenes inéditas de los músicos tocando en **el Valle de las Arenas, lugar que en la actualidad se encuentra destruido por las maquinarias de Aes Gener**.

<iframe src="https://www.youtube.com/embed/VkL8GPOjCNU" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

**“Maipo Libre” fue presentado el 21 de enero** en el Antiguo Sueño, un restaurante ubicado en el Cajón del Maipo, al cual asistieron además de los artistas, ambientalistas, actores y público en general. Durante el evento se interpretaron algunas canciones pertenecientes al disco y se hizo un **avant premier del documental “Secos” de Galut Alarcón**.

De acuerdo con la Coordinadora ciudadana en defensa del Cajón del Maipo, **el Proyecto energético fue aprobado irregularmente** durante el primer gobierno de Michell Bachelet, tiempo desde el cual el colectivo viene denunciado que "es el ícono de la corrupción político empresarial. Los permisos ambientales con los que cuenta el proyecto tienen vicios desde su origen y de esta manera han logrado ponerse, incluso, por sobre la ley que los rige".

El disco Maipo Libre; se encuentra **[disponible en la plataforma musical PortalDisc](http://www.portaldisc.com/disco.php?id=15301)** en los formatos Mp3 y Flac. Los recursos generados por la venta del disco, van en directo apoyo a la campaña **No Alto Maipo**.

 
