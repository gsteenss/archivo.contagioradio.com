Title: Animalistas quieren consulta antitaurina en 2018
Date: 2017-05-16 16:39
Category: Animales, Nacional
Tags: Consulta Antitaurina, Corte Constitucional
Slug: animalistas_consulta_antitaurina_enrique_panalosa
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/07/MG_9805.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Contagio Radio 

###### [16 May 2017] 

Luego de que la Corte Constitucional obligara a la alcaldía de Bogotá a tramitar la Consulta popular, los animalistas promotores de la consulta antitaurina, solicitan mediante una carta dirigida a Enrique Peñalosa que este mecanismo **de participación popular se realice en el mes de marzo del 2018.**

La solicitud se realiza con el objetivo de garantizar la participación ciudadana, pero sobre todo para no generar costos innecesarios, en el marco del del artículo 33 de la Ley 1757 de 2013.

“Con el fin de cumplir con los principios constitucionales de **Economía Presupuestal y Moralidad Administrativa para que se hagan efectivos los derechos de la ciudadanía** a ejercer su participación en las decisiones de la administración sin tener que desgastar desproporcionalmente los recursos públicos”, dice la carta firmada por Natalia Parra y Julián Coy, promotores de la consulta.

En un primer momento la consulta estaba planeada para que se llevara a cabo el 25 de octubre de 2015, es decir junto a las elecciones locales de ese momento, lo que significaba que **la realización de esta consulta tendría un costo hasta 10 veces menor, según cálculos de los promotores. **

### **Alcaldía no ha iniciado trámite de la Consulta Antitaurina** 

Hasta el momento la alcaldía de Bogotá no ha empezado el trámite de la consulta asegurando que no ha sido notificada por parte de la Corte Constitucional. Sin embargo, hace dos años el Ministerio de Hacienda se opuso a la realización de la consulta por los altos costos que demanda la logística del mecanismo, razón por la cual, nuevamente  los promotores de la consulta evidencian la necesidad de que esta sea realizada con las elecciones del 2018.

Cabe recordar que el alcalde **Enrique Peñalosa ha asegurado estar a favor de que se realce una consulta que termine con el maltrato animal en las corridas de toros**.  Además la Corte obliga al distrito al día a gestionar la realización de la consulta, y aunque llegara a ganar la aprobación de los espectáculos taurinos, la Corte advierte a la Administración Distrital que de todas formas, deberá implementar las medidas necesarias para desincentivar esa práctica. [ (Le puede interesar: Corte Constitucional ordena tramitar Consulta Antitaurina)](https://archivo.contagioradio.com/corte_constitucional_distrito_consulta_antitaurina/)

![carta](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/05/carta-e1494972542902.jpg){.alignnone .size-full .wp-image-40613 width="500" height="695"}

###### Reciba toda la información de Contagio Radio en [[su correo]
