Title: Asesinan al líder afrocolombiano Patrocinio Bonilla
Date: 2020-08-12 17:53
Author: AdminContagio
Category: Actualidad, Nacional
Tags: Coordinador Nacional Agrario, Patrocinio Bonilla, Proceso de Comunidades Negras
Slug: asesinan-al-lider-afrocolombiano-patrocinio-bonilla
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/08/Patrocinio-Bonilla.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

El Proceso de Comunidades Negras -[PCN](https://twitter.com/renacientes)- denunció que **Patrocinio Bonilla, líder social afrocolombiano, fue asesinado este martes en la comunidad Santa Rita, en el municipio de Alto Baudó, Chocó.** La organización, que puso en conocimiento de la Defensoría del Pueblo los hechos, indicó que «*paramilitares retuvieron a Bonilla, junto a otras 15 personas, y posteriormente lo asesinaron*».

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/renacientes/status/1293341909180702721","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/renacientes/status/1293341909180702721

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

Según versiones de la comunidad, Patrocinio Bonilla estaba en la quebrada Emparaidá en la comunidad Santa Rita junto a más personas cortando madera, cuando **llegó un grupo paramilitar que raptó a 15 pobladores, dejándolos ir uno a uno, pero quedándose finalmente con Patrocinio a quien momentos después asesinaron a disparos.** (Lea también: [Atentan contra la vida del líder Awá, Javier Cortés Guanga en Nariño](https://archivo.contagioradio.com/atentan-contra-la-vida-del-lider-awa-javier-cortes-guanga-en-narino/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El Coordinador Nacional Agrario -[CNA](https://twitter.com/CNA_Colombia)- se pronunció en rechazo del asesinato del líder social y explicó que Patrocinio Bonilla fue fundador del movimiento étnico e indigena Asokinchas, así como también **fue miembro de la junta nacional y del ejecutivo nacional del CNA.**

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/CNA_Colombia/status/1293359612536721413","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/CNA\_Colombia/status/1293359612536721413

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

José Santos, miembro del PCN, señaló que conoció a Patrocinio cuando estaba trabajando con los jóvenes promoviendo el avance de proyectos productivos con comunidades negras e indígenas; asistiendo incluso a la Asamblea de Organizaciones Sociales que se celebró en la ciudad de Bogotá en una lucha incesante por los derechos colectivos de las comunidades negras. (Le puede interesar: [Otra Mirada: La verdad del pueblo negro](https://archivo.contagioradio.com/otra-mirada-la-verdad-del-pueblo-negro/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por su parte, Germán Bedoya, líder campesino, expresó que conoció a Patrocinio como un joven lleno de vida, navegando los ríos y caminando montañas, «*construyendo sueños, amando siempre su Baudó y su Chocó, orgulloso de ser del Pacífico, condenando la violencia, amando el deporte, la música; feliz de ser Negro, como decía siempre y feliz de tener sus hijos a quienes amaba entrañablemente*».

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### El crimen de Patrocinio Bonilla una muestra más del recrudecimiento de la violencia en los territorios

<!-- /wp:heading -->

<!-- wp:paragraph -->

José Santos,  señaló que con la gestión que se adelantó desde organizaciones como el PCN, se logró la **elaboración del capítulo étnico de los Acuerdos de Paz, pero también ve con preocupación que ni siquiera un 6% de lo allí establecido se ha cumplido en la práctica.** (Lea también: [Compromiso de la Comisión de la Verdad es poner a la luz la resistencia afrocolombiana](https://archivo.contagioradio.com/compromiso-de-la-comision-de-la-verdad-es-poner-a-la-luz-la-resistencia-afrocolombiana/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Contrariamente, asegura, **la violencia se ha recrudecido con la presencia de grupos paramilitares, la guerrilla del ELN y las disidencias de las FARC en los territorios; a lo cual, el Gobierno solo responde con la militarización** de los mismos sin que esto haya contribuido en nada para contener la ola de violencia, sino que por el contrario se ha prestado para levantar dudas en la comunidad sobre **«*confabulación y connivencia de la Fuerza Pública con esos actores irregulares*».**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por ello, recalca la necesidad de establecer acuerdos humanitarios, no solo con la insurgencia sino con grupos armados estatales; cuya iniciativa se debe centrar en el Gobierno que es el llamado a establecer puentes de dialogo.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Adicionalmente, señaló que en el lema que se emplea desde el Gobierno de “paz con legalidad” existe una suerte de estigmatización en contra de las comunidades que se encuentran en los territorios porque se da la idea de que se encuentran fuera de dicha legalidad, cuando lo único que buscan es el establecimiento de pactos humanitarios con los grupos armados para que dejen de atentar contra sus vidas e integridad.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

La violencia en contra de los liderazgos, no solo no acaba, sino que tiende a recrudecerse a juzgar por los registros del Instituto de Estudios para el Desarrollo y la Paz (Indepaz)** que documentan al menos [183](http://www.indepaz.org.co/paz-al-liderazgo-social/) líderes sociales y defensores de Derechos Humanos asesinados en lo que va del año 2020.**

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
