Title: Cine para todos presenta: "El fin de la Guerra"
Date: 2017-11-03 14:06
Author: AdminContagio
Category: 24 Cuadros
Tags: Bogotá, Cine, Documental, el fin de la guerra
Slug: documental-fin-guerra
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/11/el-fin-de-la-guerra.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: El fin de la Guerra 

###### 06 Nov 2017 

En el marco del **Cuarto Festival Internacional de Cine por los Derechos Humanos**, Cine Para Todos realizará una función especial de forma simultánea en las ciudades de Bogotá, Medellín y Barranquilla con **el documental ‘El fin de la Guerra’**.

Esta proyección gratuita, **dirigida a personas con discapacidad visual, auditiva y cognitiva**, que hace parte del proyecto de inclusión cultural del Ministerio de Tecnologías de la Información y las Comunicaciones (MinTIC), **tendrá lugar a partir de las 10:00 a.m.** en salas seleccionadas de Cine Colombia ubicadas en estas tres ciudades del país.

Los asistentes a la función podrán disfrutar del documental, que **se estrenó el pasado 28 de septiembre** y fue producido durante el proceso de paz entre el Gobierno nacional y las Farc, en el que “se explora lo que es necesario estratégica y espiritualmente para que una nación de 50 millones de personas alcance el perdón y vaya de la guerra a la paz”, según lo describió su director, Marc Silver.

La cinta será presentada en Cine Para Todos **con tecnología de audio descripción, subtitulado especial y lengua de señas**, para que las personas con discapacidad de Bogotá, Medellín y Barranquilla no se pierdan un solo detalle de este documental, que promete mostrar detalladamente el acuerdo que marca la historia de nuestro país.

<iframe src="https://www.youtube.com/embed/jDscW1-Qhyg" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

Cine Para Todos nació hace cinco años como una iniciativa del Ministerio TIC, en alianza con la Fundación Saldarriaga Concha y Cine Colombia, con el fin de ofrecer mayores oportunidades de entretenimiento incluyente a las personas con discapacidad. El proyecto ha alcanzado desde sus inicios a más de **73.500 personas** a través de la proyección de **77 películas**.

La función accesible y gratuita del documental ‘El fin de la guerra’ se llevará a cabo el sábado **4 de noviembre a las 10:00 a.m.** en las siguientes salas:

**Bogotá**  
Múltiplex Cine Colombia Gran Estación - Sala 1 (Avenida Calle 26 N° 62-42)  
Capacidad: 299 personas

**Barranquilla**  
Multiplex Cine Colombia Buenavista - Sala 3 (Calle 98 Cra. 52 - 115)  
Capacidad: 337 personas

**Medellín**  
Multiplex Cine Colombia Los Molinos - Sala 4 (Carrera 82 N° 32 A - 26 Local 3003)  
Capacidad: 283 personas
