Title: Policía hondureña no reprimirá movilización ciudadana contra "el fraude" electoral
Date: 2017-12-06 08:57
Category: El mundo, Movilización
Tags: elecciones Honduras, honduras
Slug: elecciones_honduras_fraude
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/12/honduras_policia-e1512497554999.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: REUTERS/Moises Ayala] 

###### [5 Dic 2017] 

[Continúan las movilizaciones sociales en todo el territorio de hondureño denunciando el “fraude” en las elecciones presidenciales de ese país. La ciudadanía exige que no sea reelegido como presidente Juan Orlando Hernández denunciado por múltiples casos de corrupción, y quien aprovecharía los resultados para no ir a juicio.]

[La diferencia entre ambos candidatos es mínima. Hernández le habría ganado a Salvador Nasralla, candidato por la Alianza de Oposición contra la Dictadura, con una diferencia de apenas 1,59%, por eso, Nasralla ha pedido a la OEA que verifique los cuadernillos y las 5.174 actas de votación que habrían sido manipuladas e introducidas al sistema.]

[“Nosotros vencimos un fraude en las mesas, **Juan Orlando se robó un 20% de los votos en las mesas, nosotros vencimos ese 42% y ahora el plan de él es robarse los votos en el tribunal electoral”,** asegura Salvador Nasralla, quien explica que el proyecto reeleccionista va contra la Ley y contra la Constitución.]

[Además el candidato a la presidencia por la Alianza de Oposición contra la Dictadura, manifiesta que tras las múltiples denuncias y movilizaciones **ni la OEA, ni EE.UU, ni la Unión Europea han reconocido dichos resultados** y por tanto no han legitimado a Hernández como presidente de Honduras. Por su parte, Canadá pide hacer un conteo voto por voto de manera pública.]

### **Fraude en las elecciones** 

[Desde la Alianza de Oposición, se venía denunciando a nivel nacional e internacional que Juan Orlando Hernández tiene total control sobre las instituciones electorales, por lo cual la oposición aseguraba que no existían condiciones para que fueran unas elecciones “libres y transparentes”.]

[No obstante decidieron participar teniendo en cuenta que en la mayoría de las encuestas realizadas previamente a las elecciones, **un 70% de la intención de voto evidenciaba el alto apoyo que tenía el candidato Nasralla.**]

[Sin embargo, el lunes el Tribunal Electoral se dispuso a hacer un primer anuncio en el que Salvador Nasralla iba ganando las elecciones con el 60% del escrutinio, “pero luego se cae el sistema y luego se reanuda, misteriosamente empieza a cambiar la tendencia”, indica Rodolfo Pastor vocero de la Alianza de Oposición contra la Dictadura.]

[![76a66a0a-199c-4005-bff0-a6ea419f0e8c](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/12/76a66a0a-199c-4005-bff0-a6ea419f0e8c-800x450.jpg){.aligncenter .wp-image-49735 .size-medium width="800" height="450"}](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/12/76a66a0a-199c-4005-bff0-a6ea419f0e8c.jpg) [![2d56cb1e-1cdb-4c80-aba0-c13cc60d022c](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/12/2d56cb1e-1cdb-4c80-aba0-c13cc60d022c-800x450.jpg){.aligncenter .wp-image-49736 .size-medium width="800" height="450"}](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/12/2d56cb1e-1cdb-4c80-aba0-c13cc60d022c.jpg)

### **Masivas protestas de la ciudadanía** 

[A partir del momento en el que se cae el sistema “se comienza a ver la intención de manipular los resultados y la población comienza a salir a las calles. Ante esto, la estrategia es propiciar actos vandálicos", denuncia el vocero de la Alianza.]

[De acuerdo con Pastor, tras las demoras en los resultados electorales que dan como ganador a Hernández, se fortalece la movilización en todo el país pero con ello se ordena a las fuerzas militares reprimir la movilización, hecho que ya ha dejado 11 personas muertas. Ante ello** la Policía Nacional de Honduras ha decidido deslindarse de los planteamientos políticos de sus superiores.**]

“Nuestro pueblo es soberano y a ellos nos debemos, por lo tanto, no podemos estar confrontando y reprimiendo sus derechos”, resaltó la Policía Nacional mediante un comunicado.

[![DQP71lFXcAA\_\_zy](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/12/DQP71lFXcAA__zy-491x800.jpg){.size-medium .wp-image-49695 width="491" height="800"}](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/12/DQP71lFXcAA__zy.jpg)

[La indignación de la gente se debe a que buscan impedir que nuevamente sea presidente una persona que “ha sido altamente cuestionada y señalada por casos de corrupción y abuso de autoridad", de hecho Juan Orlando y su familia han estado relacionados con narcotráfico, **“para ellos es mejor mantenerse en el poder, porque retirarse significa estar expuestos. Es un asunto de supervivencia”**, afirma.]

[Ante la situación la ciudadanía se plantea mantener manifestándose en las calles en rechazo a los resultados, hasta lograr que Hernández se retire del poder y pueda ser judicializado.]

<iframe id="audio_22482380" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_22482380_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
