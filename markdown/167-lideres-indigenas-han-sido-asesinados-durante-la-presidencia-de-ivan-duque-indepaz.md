Title: 167 líderes indígenas han sido asesinados durante la presidencia de Iván Duque : Indepaz
Date: 2020-06-10 14:07
Author: CtgAdm
Category: Actualidad, DDHH
Tags: conflicto armado, lideres indigenas, pueblos indigenas
Slug: 167-lideres-indigenas-han-sido-asesinados-durante-la-presidencia-de-ivan-duque-indepaz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/06/informe-de-indepaz.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/06/Líderes-indígenas-asesinados.pdf" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

El Instituto de Estudios para el Desarrollo y la Paz (Indepaz) , presentó este 9 de junio un **informe sobre los asesinatos de líderes indígenas en el período de 2016 al 8 de junio de 2020**, en el cual señalan que **han sido asesinados 269 [líderes](https://archivo.contagioradio.com/milton-conda-lider-indigena-nasa-denuncia-amenazas-en-su-contra/) indígenas.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

La organización hace una comparación de los últimos 6 años, en el que registraron , **"31 asesinatos en el 2016, 45 en el 2017, 62 en el 2018, 84 en el 2019 y 47 en el 2020"**; destacando que **262 líderes indígenas fueron asesinados luego de la firma del Acuerdo de Paz** y 167 durante el período de presidencia de Iván Duque.

<!-- /wp:paragraph -->

<!-- wp:quote -->

> *"En el 2020 han sido asesinados 47 líderes indígenas, **14 de estos durante el período de cuarentena**"*
>
> <cite>*Indepaz* </cite>

<!-- /wp:quote -->

<!-- wp:paragraph -->

Según Indepaz, el departamento donde se han presentado mayor número de asesinatos ha liderazgos indígenas desde el 2016, ha sido **el [Cauca](https://archivo.contagioradio.com/eurodiputados-piden-respuestas-a-gobierno-duque-sobre-situacion-de-dd-hh/), con 94 reportes, 28 corresponden al primer semestre de este año.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

A ese departamento le sigue en registros del 2020; **Nariño con 5 asesinatos, Caqueta y Valle con 3**, y de a un caso en lugares como [Putumayo](https://archivo.contagioradio.com/en-putumayo-no-cesa-la-violencia-contra-la-comunidad/), Norte de Santander, Córdoba,Chocó, Antioquia e incluso Bogotá.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

*"**Lo que ocurre en Cauca responde a la lógica de un departamento donde históricamente se han presentado conflictos territoriales**, con sectores privados legales e ilegales tales como la minería"*, señaló la organización.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Destacando otros actores influyentes en el conflicto como, **las azucareras la explotación de madera, la ganadería, la megaminería, la concentración de tierras, los proyectos petroleros, las hidroeléctricas** entre otros.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Asimismo señalaron que los procesos de reclamación, restitución y recuperación de tierras, ademas de los incumplimientos al Pnis , han intensificado la crisis de los liderazgos, **abriendo paso nuevamente a la presencia de [grupos armado](https://www.justiciaypazcolombia.com/se-incrementa-pie-de-fuerza-en-el-guayabero-bombardeos-indiscriminados-campesinos-heridos-capturados-y-desaparecidos-en-la-vereda-tercer-milenio/) que se disputan el control de las rutas del narcotráfico.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El informe destaca las acciones encabezadas por los pueblos indígenas quienes **han firmado su autonomía y decisión de permanecer en sus territorios como condición esencial para garantizar supervivencia**, *"rechazado todas las manifestaciones del conflicto armado y han asumido una posición de resistencia pacífica ante todos los actos armados".*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Resaltando que los pueblos indígenas, *"**reclaman el derecho a la libre determinación para que se reconozca y se respete sus planes de vida,** así como los proyectos en su territorio que intervengan con recursos nacionales e internacionales y que fortalezcan las autoridades tradicionales ilegítimas".*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Finalmente destacan que las comunidades indígenas no dejan de lado que deben también ser reconocidas *"c**omo víctimas del conflicto armado y el reconocimiento de los actores armados a la verdad, la justicia,** la reparación integral y la no repetición hacia los pueblos étnicos".*

<!-- /wp:paragraph -->

<!-- wp:file {"id":85239,"href":"https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/06/Líderes-indígenas-asesinados.pdf"} -->

<div class="wp-block-file">

[*Le puede interesar informe completo Indepaz: Líderes-indígenas-asesinados*](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/06/Líderes-indígenas-asesinados.pdf)[Descarga](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/06/Líderes-indígenas-asesinados.pdf){.wp-block-file__button}

</div>

<!-- /wp:file -->

<!-- wp:block {"ref":78955} /-->
