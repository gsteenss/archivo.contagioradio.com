Title: Continúan las amenazas a líderes de Marcha Patriótica
Date: 2017-08-23 16:51
Category: DDHH, Nacional
Tags: Amenazas líderes sociales, marcha patriotica
Slug: continuan-las-amenazas-a-lideres-de-marcha-patriotica
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/Foto-Lider-asesinado-marcha.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [23 Ago 2017] 

La plataforma política Marcha Patriótica denunció que nuevamente líderes sociales y defensores de derechos humanos fueron amenazados; en esta oportunidad a través de un correo electrónico firmado por un grupo denominado como “Los Urabeños”, **en donde les aseguran que “tienen todos sus pasos vigilados” y los señalan de ser integrantes de las FARC-EP**.

Las personas amenazadas son el vocero nacional de Marcha Patriótica Andrés Gil, el Coordinador internacional de esta organización Mauricio Ramos, Juan Carlos Quintero, integrante de esta plataforma y los integrantes de la Juventud Rebelde Guillermo Quintero y Junior Maldonado. (Le puede interesar: ["Amenazan de muerte a integrantes de la Juventud Rebelde y de Marcha Patriótica"](https://archivo.contagioradio.com/44543/))

La amenaza llego a los correos de cada uno de los miembros de la plataforma desde la dirección de e-mail <laverdeuraba@gmail.com> en la que rezaba: “a los comunistas que en pocos días serán partidos, les quedan sus días contados, tenemos todos sus pasos y lo que han hecho como plan, pero no crean que van a llegar al poder porque a plomo serán eliminados”. (Le puede interesar: ["En 2017 han  sido asesinados 41 líderes sociales"](https://archivo.contagioradio.com/en-el-transcurso-del-2017-han-sido-asesinados-41-lideres-sociales/))

**Estas amenazas se suman a las más de 150 que ha denunciado la organización en su informe sobre agresiones a defensores de derechos humanos** y líderes sociales en el país, en los primeros 6 meses del presente año. Por este motivo, Marcha Patriótica le ha exigido al gobierno que se investigue a fondo quienes son los responsables de estas amenazas y los autores intelectuales tras los asesinatos de los integrantes de esta plataforma.

![Amenaza Marcha Patriótica](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/08/DH4mWVwXgAcC-58.jpg){.alignnone .size-full .wp-image-45693 width="917" height="1200"}

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
