Title: La crisis económica no se resuelve con limosnas: Alirio Uribe Muñóz
Date: 2020-04-02 18:01
Author: AdminContagio
Category: Actualidad, Comunidad
Tags: crisis económica, Donación, Informalidad
Slug: la-crisis-economica-no-se-resuelve-con-limosnas-alirio-uribe-munoz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/04/Crisis-económica-y-social.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:heading {"align":"right","level":6,"textColor":"cyan-bluish-gray"} -->

###### Foto: Contagio Radio {#foto-contagio-radio .has-cyan-bluish-gray-color .has-text-color .has-text-align-right}

<!-- /wp:heading -->

<!-- wp:paragraph -->

El martes 31 de marzo dos manifestaciones (en Suba y en Santa Fe) evidenciaron la vulnerabilidad de vendedores informales, trabajadoras sexuales y otras poblaciones que subsisten con el dinero que logran hacer día a día en la calle. Ante tal situación, líderes políticos y sociales piden medidas estructurales para proteger a la población de la crisis económica que afecta el país.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/jcuestanovoa/status/1245125476290629633","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/jcuestanovoa/status/1245125476290629633

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:heading {"level":3} -->

### Atención del Gobierno a empresarios vs atención a los ciudadanos

<!-- /wp:heading -->

<!-- wp:paragraph -->

El abogado defensor de DD.HH. e integrante del Colectivo de Abogados José Alvear Restrepo, Alirio Uribe Muñoz, cuestionó que mientras el Banco de la República dispuso de más de 20 billones de pesos para dar liquidez a los bancos, y con el Decreto 444 "saca dinero de los fondos de pensiones y regalías" para ellos, dicho altruismo no se ve para la mayoría de ciudadanos.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Uribe Muñoz señaló que también se está pensando en comprar acciones y bonos a la banca para generar un flujo de dinero, pero esas acciones no están orientadas para las pequeñas y medianas empresas que no manejan dichos sistemas. En ese sentido, cuestionó que se sigue subsidiando a los ricos, mientras se descuida a la población vulnerable.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

"Todos los programas de familias en acción, jovenes en acción, adultos mayores y demás son insuficientes", sostuvo el defensor de DD.HH., que además señaló que la devolución de IVA para cerca de un millón de personas por 75 mil pesos cada 2 meses "no resolverá la crisis social que tenemos en el país".

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### La crisis económica es de los más vulnerables

<!-- /wp:heading -->

<!-- wp:paragraph -->

Las protestas que se han visto recientemente en las cárceles así como en sectores donde habita población con problemas de acceso a recursos económicos, muestran que el coronavirus hizo visibles otras crisis: La del hacinamiento en las cárceles y sus consecuencias sanitarias, la de los habitantes de calle y la de los trabajadores informales.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El jurista recordó que según el DANE en Bogotá hay 9.500 ciudadanos en condición de calle, pero solo hay albergue para unos 3 mil, por lo que más de 6 mil tienen que dormir en la calle. Y para aquellos que logran un techo, no hay una política contra la abstinencia, lo que los empuja a hacer lo que sea para conseguir dinero y poder consumir.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

"Han dicho que la bicha de bazuco paso de 2 mil a 7 mil pesos, eso vuelve a poner de presente la necesidad de los Centros de Atención Medica a Drogodependientes (CAMAD)", afirmó. Otro problema evidenciado en la ciudad tiene que ver con las personas que cubren su subsistencia con lo que logran reunir día a día de diferentes formas en la calle.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Zonas del centro histórico de la ciudad y otras de la periferia son escenarios en los que ciudadanos venezolanos o colombianos pagan entre 2 a 5 mil pesos por persona para poder pasar la noche bajo un techo. (Le puede interear: ["La propuesta de FARC para salvar la economía popular"](https://archivo.contagioradio.com/la-propuesta-de-farc-para-salvar-la-economia-popular/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

No es una decisión de alcaldes o gobernadores porque no tienen la posibilidad de discursos. "Son vendedores ambulantes, venezolanos, cachivacheros que si no alcanzan a hacer el dinero suficiente duermen en la calle", aclaró Uribe Muñoz, añadiendo que Bogotá debería tener un censo de estas personas para asegurar que tengan un lugar en el cual pasar la cuarentena.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### La renta básica, un elemento fundamental para enfrentar la crisis

<!-- /wp:heading -->

<!-- wp:paragraph -->

Para enfrentar un problema coyuntural como puede ser el Covid se pueden empreder acciones temporales, pero la desigualdad histórica que se ha hecho evidente en Colombia "requiere medidas estructurales". De acuerdo al abogado, según cifras del DANE la población económicamente activa es de 22 millones de personas, y de ellos, cerca del 50% pertenece a la economía informal.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Es decir que dependen de ellos mismos y no pueden hacer teletrabajo, ni vivir de sus ahorros. En ese sentido, y en consonancia con lo que han pedido organizaciones sociales y defensoras de derechos humanos, asegura que habría que brindar una renta mínima por lo menos durante 6 meses para esas personas vulnerables, y evaluar la continuidad de la medida para redistribuir la riqueza y hacer frente a la desigualdad.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### "La crisis económica que estamos viviendo no se resuelve con donaciones o con limosnas"

<!-- /wp:heading -->

<!-- wp:paragraph -->

Recientemente algunos medios hicieron eco de la donación de 80 mil millones de pesos de Luis Carlos Sarmiento Angulo para combatir los estragos del Covid-19. Sin embargo, Alirio Uribe aseveró que "la crisis social y económica que estamos viviendo no se resuelve con donaciones o con limosnas".

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Para él, esos 80 mil millones de Sarmiento Angulo son 'marketing político y empresarial, y lo más probable es que se vaya a deducir de los impuestos que paga", como el multimillonario que es. (Le puede interesar: ["¿Sarmiento Angulo desconocía los millonarios sobornos pagados por Corficolombiana?"](https://archivo.contagioradio.com/sarmiento-angulo-corficolombiana/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En cambio, propuso que los ricos y super ricos de Colombia paguen este año un impuesto a la riqueza, como fue el impuesto de guerra del gobierno Uribe I para hacer justicia tributaria. Estimó que con ello se podrían generar 11 billones de pesos para dar una renta básica a 3,5 millones de pobres absolutos, así como a los 13 billones de pobres calificados en los índices de Pobreza Multidimensional y brindar subsidios a cerca de 400 mil trabajadores que están en desempleo.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Adicionalmente, propuso que se obtuvieran recursos con la moratoria del pago de la deuda: "para este año tenemos previsto pagar 13 billones de pesos en amortización de deuda, y 8,6 en intereses, es decir 21 billones de pesos que se podrían obtener si se congelan los pagos de la deuda externa", concluyó.

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78980} /-->
