Title: Referendo "discriminatorio" reduce posibilidades de adopción a 3700 niños y niñas
Date: 2017-05-10 14:52
Category: DDHH
Tags: Cámara de Representantes, LGBTI, referendo adopción, Votaciones
Slug: referendo-discriminatorio-dejaria-sin-posibilidad-de-ser-adoptados-a-3700-ninos-y-ninas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/05/lgbti-e1494435755511.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: LaRazon.co] 

###### [10 Mayo 2017] 

Los ponentes del referendo buscan anular la posibilidad de que parejas homosexuales, solteros, viudos y divorciados puedan adoptar niños y niñas. De conseguir 18 votos, se estaría más cerca de modificar la constitución de Colombia.

El 4 de noviembre de 2016 la Corte Constitucional había dado el aval, por medio de una sentencia histórica, de que parejas homosexuales pudieran adoptar niños y niñas. Pero en diciembre del mismo año, **el Senado aprobó el referendo que propuso la senadora Vivian Morales que busca imponer una política pública que va en contravía de un estado de derecho laico.**

El abogado Germán Humberto Rincón afirma que tanto la senadora Morales como su esposo Carlos Alonso Lucio quieren imponer un tipo de familia basado en una religión donde se imponen sus creencias personales. Para Rincón, **"el refrendo le sirve solo al voto religioso, a una estructura política religiosa que lubrica la maquinaria electoral".** Le puede interesar: ["Referendo contra la adopción busca hacer de la religión una política pública"](https://archivo.contagioradio.com/referendo-contra-la-adopcion-busca-hacer-de-la-religion-una-politica-publica/)

Según Colombia Diversa, el referendo no aporta nada a la sociedad y deja entre los afectados a los niños, la comunidad LGBTI, a las personas solteras, viudas y divorciadas. Para el abogado Germán Humberto Rincón, **"los derechos de las minorías no pueden ser objeto de consulta popular en la medida que las mayoría terminaría por oprimir a las minorías".** Le puede interesar: ["Referendo de adopción no es solo contra los gais sino también contra solteros"](https://archivo.contagioradio.com/referendo-de-adopcion-no-es-solo-contra-gais/)

El Ministerio de Hacienda ha dicho que hacer el referendo contra la adopción le costaría al país **280 mil millones,** cifra que ha sido cuestionada por sectores de la sociedad civil y organizaciones que defienden los derechos de las minorías y de los niños.

Según Colombia Diversa, si se trata de proteger a los niños, con ese dinero se podría invertir 210 mil millones de pesos en mejoras en educación de niños y niñas desde la primera infancia hasta la adolescencia. Se podría invertir 10 mil millones para atacar la desnutrición infantil y se podría invertir 10 mil millones de pesos en mejoras para las operaciones del ICBF a nivel nacional y sobraría 21 mil millones de pesos.

**LA VOTACIÓN EN LA CÁMARA**

Para que el referendo se haga efectivo necesita contar con la mayoría simple de los votos de los Representantes, es decir 18 de los 35 posibles. Según sondeos realizados por medios de comunicación, así se tiene pensado que votarán los Representante a la Cámara.

**Liberales:** De los nueve votos que tiene disponibles el partido Liberal, 1 votará que NO, 2 votarán que SI y 6 están Indecisos.

**La U:** Son los que más votos en contra tienen. 6 representantes votarán que NO y 3 le dirán que SI al referendo.

**Conservadores:** La mayoría de los integrantes de este partido en la Cámara están con el referendo. 3 votarán que SI, 1 votará que NO y hay 1 indeciso.

**Centro Democrático:** Es el gran aliado de los ponentes, sus 5 representantes votarán que SI.

**Cambio Radical:** Se espera que los de Cambio Radical apoyen el referendo con 2 votos por el SI y solo 1 por el NO.

**Polo Democrático:** Si integrante Germán Navas Talero se opondrá al referendo.

**Alianza Verde:** Angélica Lozano, quien ha liderado la oposición al proyecto de Morales votará que NO.

Se espera que al final de la tarde de este miercoles se tome una decisión en la Comisión Primera de la Cámara de Representantes. De ser aprobado el referendo se sometería a los colombianos a una votación que podría realizarse bien en las elecciones de 2018 o antes dependiendo de la disponibilidad de recursos de Min Hacienda.

<iframe id="audio_18616704" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_18616704_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
