Title: Policía sí habría agredido a mujeres en protesta contra Ecopetrol en Guamal, Meta
Date: 2017-08-22 13:29
Category: Ambiente, Nacional
Tags: Ecopetrol, ESMAD, Guamal, Huelga de hambre, policia
Slug: 45599-2
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/08/Guamal.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Portal Guamal] 

###### [22 Ago. 2017] 

Luego de un día de huelga de hambre en oposición al proyecto Trogón I de Ecopetrol, **las mujeres de la vereda Pio XII en Guamal, Meta se encontraron con la represión por parte de la Policía** que procedieron a desalojarlas por la fuerza del sitio en el que impedían el ingreso de maquinaria de Ecopetrol al predio en la vereda.

**Sully Téllez integrante de la Comunidad cuenta que eran cerca de 150 Policías** para 5 mujeres, quienes hasta el sábado se habían unido a la huelga de hambre y se habían encadenado. En contexto: [Encadenados y en huelga de hambre protestan contra proyecto de Ecopetrol en el Meta](https://archivo.contagioradio.com/45516/)

“Llegaron a conversar con nosotras, les dijimos que podía pasar el personal, pero maquinaria no y nos dijeron que teníamos que quitarnos. **Esperaron como 4 horas y comenzaron a halar, a romper las cadenas, a tirarnos al piso,** a golpearnos y nos quitaron por la fuerza del lugar”.

### **Ecopetrol niega violación de DD.HH. en desalojo de mujeres** 

A través de su cuenta de Twitter, **Ecopetrol aseguró que lamenta la situación acontecida en la Vereda Pio XII** y que siempre han garantizado o velado por la garantía de los derechos humanos para las comunidades que protestan en esta zona.

> Eso no es cierto. Lamentamos situación en vereda [\#PioXII](https://twitter.com/hashtag/PioXII?src=hash). Respetamos DDHH y contamos con veeduría ciudadana para proteger el agua.
>
> — Ecopetrol (@ECOPETROL\_SA) [19 de agosto de 2017](https://twitter.com/ECOPETROL_SA/status/899023721524023296)

<p style="text-align: justify;">
<script src="//platform.twitter.com/widgets.js" async charset="utf-8"></script>
Sin embargo, en varios vídeos compartidos por redes sociales se puede ver el momento en el que **los Policías retiran por la fuerza a las mujeres y algunos agentes del Escuadrón Móvil Antidisturbios – ESMAD - golpean a las personas** que se encuentran en la zona.

</p>
> Hoy comunidades en resistencia al pozo petrolero Trogón 1 fueron atropellados por el ESMAD [\#PioXIISeRespeta](https://twitter.com/hashtag/PioXIISeRespeta?src=hash) ¿Para quien trabajan? Ecopetrol? [pic.twitter.com/IzrsinxUV6](https://t.co/IzrsinxUV6) — Valen \#VotoPorElAgua (@valentinacmpm) [20 de agosto de 2017](https://twitter.com/valentinacmpm/status/899088455115960324)

<p>
<script src="//platform.twitter.com/widgets.js" async charset="utf-8"></script>
</p>
**“La Policía Nacional tuvo la desfachatez de golpearnos, más los hombres, solo traían 3 mujeres,** pero el subteniente Wilder Herrera fue el que más golpeó a las mujeres. Están los vídeos donde él hala, golpea y nos quitaron”.

### **La empresa continúa sus labores a pesar de las denuncias** 

Luego del desalojo de la comunidad Ecopetrol ha continuado sus labores de trabajo. Dice la comunidad que **durante la mañana de este lunes han ingresado alrededor de 40 volquetas** lo que podría desencadenar en un mayor deterioro de los puentes por donde pasa el transporte y el ruido es intenso. Le puede interesar: [La polémica detrás del proyecto de Ecopetrol en Guamal, Meta](https://archivo.contagioradio.com/la-polemica-detras-del-proyecto-ecopetrol-guamal-meta/)

“Supimos que están haciendo una socialización en Guamal por lo que ya entró un taladro, pero nunca con la comunidad de la Vereda Pio XII que somos los directamente afectados, nunca nos han dicho venga les socializamos, les mostramos nuestro manejo ambiental”.

Sin embargo, **la comunidad aseguró que no van a asistir a esta reunión, porque no fueron invitados** y además porque de hacerlo, Ecopetrol “podría tomarlo como una socialización y van a mostrar las fotos y van a decir que como estuvimos ahí ya pueden hacer todo lo que quieran”.

### **Instituciones no quisieron recibir las denuncias por el accionar de la Policía** 

“No nos quisieron poner atención en las denuncias. La Fiscalía de Guamal nos mandó para Acacias y así nos tuvieron” manifiesta Téllez. Por esta razón **la denuncia será instaurada en la Procuraduría de Bogotá** y esperarán a que Medicina Legal pueda realizar los respectivos exámenes.

### **Seguirán movilizándose en contra del proyecto Trogón I** 

La comunidad ha asegurado que seguirán haciendo diversas actividades para no permitir que esté proyecto continúe en su vereda, por lo que **pasarán cartas, derechos de petición y reclamos por escrito para que se respete la posición de la comunidad.**

“Seguiremos en la lucha en el plantón viendo cómo nos siguen vulnerando los derechos y tocando todas las puertas de las instituciones, que nos han dado la espalda, pero esperamos que tomen conciencia y se den cuenta todos los derechos que nos han vulnerado como campesinos”. Le puede interesar: [Fuerza Pública llegó a Guamal a defender intereses de Ecopetrol](https://archivo.contagioradio.com/44124/)

<iframe id="audio_20466449" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_20466449_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

######  Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU).

<div class="osd-sms-wrapper">

</div>
