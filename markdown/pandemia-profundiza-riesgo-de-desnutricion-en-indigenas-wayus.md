Title: Pandemia profundiza riesgo de desnutrición en  indígenas wayús
Date: 2020-08-14 19:56
Author: AdminContagio
Category: Actualidad, Nacional
Tags: Desnutrición en La Guajira, desnutrición niños Wayúu, pandemia
Slug: pandemia-profundiza-riesgo-de-desnutricion-en-indigenas-wayus
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/02/indigenas_wayuu_0-e1516041332156.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: Zona cero

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Con una población de al menos 380.000 personas según el Censo Nacional de Población y Vivienda 2018 y la mayoría habitando en La Guajira, los wayuu son el grupo indígena más numeroso de Colombia. Sin embargo, según un[informe realizado por Human Rights Watch y el Centro de Salud Humanitaria de Johns Hopkins](https://www.hrw.org/es/news/2020/08/13/colombia-ninos-indigenas-en-riesgo-de-desnutricion-y-muerte) los niños y niñas wayuu están sufriendo de desnutrición y la situación se podría estar agudizado con la pandemia. (Le puede interesar: [Comunidades Wayúu acuden a la ONU para frenar la actividad del Cerrejón en medio de la pandemia](https://archivo.contagioradio.com/comunidades-wayuu-acuden-a-la-onu-para-frenar-la-actividad-del-cerrejon-en-medio-de-la-pandemia/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

La tasa nacional de mortalidad en menores de cinco años por desnutrición en los últimos cinco años en la región de La Guajira ha incrementado, siendo en 2019 de casi seis veces la tasa nacional. Aunque según señalaron médicos y organizaciones la tasa podría ser más alta pues el gobierno no registra todas las muertes, en parte porque muchos niños y niñas mueren en sus hogares. 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

De acuerdo al estudio, esta cifra se debe a la inseguridad alimentaria e hídrica y los obstáculos para el acceso a la atención médica. Según estadísticas oficiales solo 4% de los wayuu que viven en zonas rurales de la región tienen acceso a agua limpia y los que residen en zonas urbanas reciben un servicio irregular. 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

A su vez, la encuesta gubernamental de 2015 sobre nutrición determinó que el [77 %](https://www.minsalud.gov.co/sites/rid/Lists/BibliotecaDigital/RIDE/VS/ED/GCFI/ensin-colombia-2018.pdf) de las familias indígenas de La Guajira están afectadas pues no cuentan con un acceso seguro y permanente a alimentos de calidad en cantidades suficientes.

<!-- /wp:paragraph -->

<!-- wp:heading -->

La pandemia agrava la situación de desnutrición
-----------------------------------------------

<!-- /wp:heading -->

<!-- wp:paragraph -->

Ahora con el coronavirus, la dificultad para acceder a alimento, agua y servicios públicos en el departamento podrían ser devastadores para los wayuu si el virus se sigue expandiendo y es que al 12 de agosto, se han confirmado más de [3.150 casos de Covid-19](https://www.ins.gov.co/Noticias/paginas/coronavirus.aspx) en La Guajira [y más de 80 serían entre indígenas wayuu.](https://infogram.com/1pg21n0qvwxpvps9mkyemj2xp9swlv3pwl5?live) 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Además, el aislamiento preventivo, “limita gravemente el acceso de los wayuu a alimentos”, sin contar el latente riesgo de contagiarse; así como el costo de transportarse hasta los centros de atención que son escasos pues solamente 3 de los 16 hospitales de La Guajira manejan casos complejos de desnutrición aguda y en el caso de La Alta Guajira hay solamente un hospital que ofrece atención básica.

<!-- /wp:paragraph -->

<!-- wp:heading -->

**La corrupción es un determinante**
------------------------------------

<!-- /wp:heading -->

<!-- wp:paragraph -->

A pesar de que el gobierno Nacional ha planteado proyectos para aumentar la seguridad alimentaria y aseguró que invirtió aproximadamente 18 millones de pesos en 2019 para programas de alimentación escolar, los alimentos son demasiado escasos o no están en buenas condiciones, según señalaron autoridades de la comunidad wayuu, fiscales locales y trabajadores de asistencia regionales.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Igualmente, a través de 14 auditorías oficiales de programas de alimentación escolar en La Guajira, se concluyó que 30.000 millones de pesos se habían perdido como consecuencia de la corrupción o una administración deficiente de los recursos. 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Según las organizaciones, un obstáculo para que estos recursos sean dirigidos adecuadamente y se disminuya la tasa de desnutrición, es la cantidad limitada de fiscales, jueces e investigadores con que cuenta el departamento para perseguir la corrupción.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En 2017 la Corte determinó que los wayuu habían experimentado una “vulneración generalizada, desproporcionada e injustificada de los derechos al agua, a la alimentación y a la salud”, ordenando al gobierno a tomar medidas “urgentes y prioritarias” para que hubiera disponibilidad de alimentos, agua y servicios de atención.

<!-- /wp:paragraph -->

<!-- wp:quote -->

> “Human Rights Watch y el Centro de Salud Humanitaria de Johns Hopkins instan al gobierno del presidente Iván Duque a adoptar medidas concretas para garantizar los derechos de los niños y niñas indígenas wayuu de La Guajira a tener acceso a alimentos, agua y atención de la salud”
>
> <cite>Human Rights Watch y el Centro de Salud Humanitaria de Johns Hopkins</cite>

<!-- /wp:quote -->

<!-- wp:heading -->

Empresas también contribuyen a la desnutrición
----------------------------------------------

<!-- /wp:heading -->

<!-- wp:paragraph -->

Desde que inició la pandemia, las comunidades wayuu denunciaron que compañías como Cerrejón continuaban realizando minería en La Guajira, actividades que al desviar accesos de agua y dañar recursos deja a los indígenas prácticamente a la deriva al perder las únicas fuentes de las que disponen. (Le puede interesar: [Guajira sin agua por desvío de arroyo Bruno y sin ayudas para enfrentar al COVID](https://archivo.contagioradio.com/guajira-sin-agua-por-desvio-de-arroyo-bruno-y-sin-ayudas-para-enfrentar-al-covid/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Además, fueron decisiones en las que nunca ni se tuvo cuenta la opinión de las comunidades wayuu ni se estudió las consecuencias que les podría traer; por esto, la comunidad se vio obligada a pedir protección a organismos internacionales.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Ahora, pareciendo que las múltiples denuncias surtieron efecto, la Procuraduría General de la Nación pidió suspender la continuación del proyecto de energía renovable de la línea de transmisión - conexión cuestecitas hasta que la empresa a cargo no cumpla con el procedimiento de consulta previa a las comunidades étnicas de Maicao, Uribia, Manaure y Riohacha. 

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->
