Title: Actualmente no hay ni una condena por feminicidio en Colombia
Date: 2016-11-22 15:25
Category: Mujer, Otra Mirada
Tags: feminicidio, impunidad, justicia, mujeres
Slug: actualmente-no-hay-ni-una-condena-por-feminicidio-en-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/Justicia-en-Caso-de-Mujeres.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Provincia] 

###### [18 Nov. 2016] 

**Los hechos violentos en contra de las mujeres siguen siendo muestra de la sociedad violenta en la que se ha estado inmerso desde tiempos remotos**. Colombia no se salva de esta realidad y  muestra de ello son las cifras dramáticas que van en aumento cada día. Por ejemplo, solo **en el Valle al sur de Colombia, la cifra asciende a más de 2 mil mujeres que han sido maltratadas y abusadas sexualmente.**

Por ello, la Colectiva Reparando Ausencias en Cali realizó una jornada de movilización para denunciar los atroces hechos de los que sigue siendo víctimas las mujeres.

Paola Sánchez integrante de la Colectiva aseguró "los casos en Cali son aberrantes, hemos conocido casos en donde los vecinos sabían lo que sucedía y no informaron".

Este evento que se realiza en la Fiscalía seccional Cali, pretende además **denunciar la negligencia y la impunidad con la que ha actuado esta institución en los casos de feminicidio de muchas mujeres, no solo en el Valle sino en toda Colombia.**

"Nos movilizamos en contra de la **Fiscalía **porque no se pronuncia con prontitud frente a casos de feminicidio" Manifestó Paola.

**Cabe recalcar que luego de un año de haberse promulgado la Ley Rosa Elvira Cely, las condenas en los procesos abiertos por feminicidio no han sido resultados.**

Este colectivo, asegura Paola Sánchez, pretende continuar haciendo diversas actividades para **visibilizar la situación que acontece con las mujeres pero también encaminadas a buscar que la sociedad no se quede callada ante estos hechos** y sean, por el contrario, veedores de las situaciones a las que están expuestas las mujeres. Le puede interesar: [Durante el 2016, 100 mil mujeres han sido violentadas en Colombia](https://archivo.contagioradio.com/2016-100-mil-mujeres-han-sido-violentadas-colombia/)

“La violencia de género en el Valle es alarmante.Sí se hacen cosas pero no es suficiente" contó Paola.

**Según el Instituto  de Medicina Legal en lo que va del 2016 se tiene registro de 27.681 casos de abuso contra las mujeres en Colombia**, donde la capital y el Departamento del Valle presentan los más altos números.

El próximo 25 de Noviembre en el marco de las actividades a realizarse en el Día Internacional de la Eliminación de la Violencia contra la Mujer, **en Cali las mujeres entrarán en un paro simbólico para rechazar los continuos y recientes hechos acontecidos en este departamento de Colombia,** pero también en rechazo a todas las barreras y la falta de equidad de género en el país. Le puede interesar: [Igualdad salarial entre hombres y mujeres se retrasa 170 años](https://archivo.contagioradio.com/igualdad-salarial-hombres-mujeres-se-retrasa-170-anos/)

###### Reciba toda la información de Contagio Radio en [[su correo]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio]{.s1}](http://bit.ly/1ICYhVU)

<div class="ssba ssba-wrap">

</div>
