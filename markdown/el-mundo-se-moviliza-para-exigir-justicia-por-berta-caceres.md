Title: El mundo se moviliza para exigir justicia por Berta Cáceres
Date: 2016-06-15 14:35
Category: El mundo, Entrevistas
Tags: Berta Cáceres, honduras, Tegucigalpa
Slug: el-mundo-se-moviliza-para-exigir-justicia-por-berta-caceres
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/06/dc22b51c-0abb-465a-83ed-c6df5ed90683.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: COPINH 

###### [15 Jun 2016]

Hoy se realiza la jornada de acción global para exigir justicia por el crimen contra la líder indígena y ambientalista, Berta Cáceres, asesinada en Honduras el pasado 3 de marzo, y por cuyo caso hasta el momento **se han capturado dos integrantes de las fuerzas militares de ese país y un funcionario de la empresa DESA**, que lleva a cabo el proyecto  hidroeléctrico Agua zarca.

Miriam Miranda, vocera de la Organización Fraternal Negra Hondureña, OFRANEH señala que lo que se busca con esta movilización mundial **es exigir que se establezca una comisión independiente para investigar a fondo el asesinato de la lideresa** y así se castigue a los verdaderos culpables.

Según se denuncia desde OFRANEH, el gobierno y la empresa lo que han hecho es dilatar los tiempos, además se ha tratado de un “proceso muy sospechoso”, pues a la familia de Berta no se le ha permitido acceder al expediente del caso.

“Hace un mes y medio fueron capturadas cuatro personas. Uno de ellos era de la empresa (Agua Zarca), y con eso ya se da por hecho (de parte del gobierno) que hay justicia”, asegura Miranda, quien expresa que **lo que se exige es que se suspenda definitivamente ese proyecto hidroeléctrico** y no solo se congele temporalmente, como lo hizo actualmente el Banco Finn Fund de Finlandia que financiaba a DESA con 5 millones de dólares, El FMO que aportaba 15 millones de dólares; y el Banco Centroamericano de Integración Económica, BCIE, que provee para el proyecto 24.4 millones de dólares.

En cambio, se denuncia que la criminalización y judicialización contra movimientos sociales en Honduras ha aumentado, y los familiares de Berta y demás integrantes del Consejo Cívico de Organizaciones Populares e Indígenas de Honduras, COPINH, siguen siendo víctimas de amenazas, por parte de estructuras paramilitares.

**“Es una dictadura y régimen fascista que acalla las voces de quienes defienden la vida, el agua, el bosque y los recursos naturales.** El asesinato de Berta Cáceres fue un golpe nefasto, fue una escalada paramilitar”, dice la vocera de OFRANEH.

Este miércoles 15 ciudades de Honduras se movilizaron exigiendo justicia por Berta, con la consigna “Berta vive, la lucha sigue”. En Tegucigalpa, cerca de 500 personas se concentraron frente al edificio del Ministerio Público. Además **en Colombia, Nicaragua, España, Cuba, Panamá, México, Bélgica y en otros lugares de África y Asia.**

\

<iframe src="http://co.ivoox.com/es/player_ej_11914992_2_1.html?data=kpamk5mdfZOhhpywj5aaaZS1kZaah5yncZOhhpywj5WRaZi3jpWah5ynca7d087Oz5CxrdPVz8nOjZKPk6fGorOyqpKJe6ShpNTb1sbLrdCfs8bRy9SPcYarpJKh&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
