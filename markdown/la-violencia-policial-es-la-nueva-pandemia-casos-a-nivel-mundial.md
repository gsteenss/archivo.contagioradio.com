Title: La violencia policial es la nueva pandemia: Casos a nivel mundial
Date: 2020-09-15 10:27
Author: AdminContagio
Category: El mundo
Tags: policia, policía del mundo, violencia policial
Slug: la-violencia-policial-es-la-nueva-pandemia-casos-a-nivel-mundial
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/09/violencia-por-parte-de-la-policia-en-Ecuador.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: Captura video @sybelmartinez

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Las manifestaciones en rechazo al abuso de la Fuerza Pública han dejado más de 200 personas heridas y 13 personas asesinadas en Colombia. Sin embargo no es solo un caso que se da en este país. Este año, en medio de manifestaciones y operativos, manifestantes de todo el mundo han sido víctima de ataques por parte de la Fuerza Pública. (Le puede interesar: [El abogado Javier Ordóñez fue asesinado por la Policía afirman sus familiares](https://archivo.contagioradio.com/el-abogado-javier-ordonez-fue-asesinado-por-la-policia-afirman-sus-familiares/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El domingo 13 de septiembre, **defensores de derechos humanos en la ciudad de Guayaquil, en Ecuador denunciaron un caso de brutalidad policial cuando 3 agentes golpearon a un hombre hasta dejarlo inconsciente.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

La Policía Nacional explicó que el episodio tuvo lugar cuando en un control policial se encontró a un grupo de personas tomando bebidas alcohólicas en la vía pública durante la madrugada.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

[La ministra del Gobierno de Ecuador anunció que la unidad Asuntos Internos](https://twitter.com/mariapaularomo/status/1305344082689101824) inició una investigación para “establecer las circunstancias y pormenores de este procedimiento policial”. **Los uniformados golpearon y patearon al civil mientras una mujer gritaba "ya no más, ya por favor".** (Le puede interesar: [\[En vídeo\] 10 delitos de la Policía en medio de las movilizaciones](https://archivo.contagioradio.com/en-video-10-delitos-de-la-policia-en-medio-de-las-movilizaciones/))

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/sybelmartinez/status/1305318264487260160","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/sybelmartinez/status/1305318264487260160

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:heading -->

**En Nicaragua** la policía detuvo violentamente una manifestación
------------------------------------------------------------------

<!-- /wp:heading -->

<!-- wp:paragraph -->

Este domingo, autoridades informaron que dos niñas de 10 y 12 años fueron asesinadas -una de ellas violada-, en una comunidad rural del noreste de Nicaragua.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En rechazo, hubo una manifestación pacífica de al menos 20 feministas denominada **“acción por las niñas”**  junto a una carretera del sur de Managua. Sin embargo, la policía desmontó la manifestación e interrumpieron con violencia la protesta.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Además, denunciaron que los policias les arrebataron sus celulares, así como pañoletas representativas del movimiento feminista; al menos **una manifestante fue golpeada a patadas, y cinco resultaron detenidas de forma temporal**.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Carmen Rodríguez, madre de las dos niñas dijo al Canal 10 de televisión local que aunque celebraba que la Policía había capturado al agresor, dudaba que se hiciera justicia, ya que en los últimos seis años había denunciado tres violaciones contra su hija de 12 años, y nunca obtuvo respuestas.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/nicadigital/status/1305302272369401856","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/nicadigital/status/1305302272369401856

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:heading -->

**En medio de protestas, Bielorrusia reporta casos de violencia policial**
--------------------------------------------------------------------------

<!-- /wp:heading -->

<!-- wp:paragraph -->

Desde las elecciones del pasado 9 de agosto que terminaron con la reelección del presidente Alexandre Lukashenko en Bielorrusia, se desataron protestas en contra de esta reelección por considerarse fraudulenta. **Protestas que hasta la fecha no han finalizado y que han dejado 2 muertos y miles de detenidos que han sufrido maltrato por parte de la policía bielorrusa. **

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Varios protestantes han considerado las agresiones por parte de los agentes como actos de tortura. **Según el movimiento  Amnistía Internacional, las agresiones podrían ser catalogadas como una "tortura generalizada".**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En el cuarto día de protestas, se habían registrado al menos unas 6.700 detenciones y se registraron casos en que la policía utilizó granadas y balas de caucho contra los manifestantes. 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Varios de los detenidos habían comentado anteriormente a la cadena de noticias BBC que **"golpearon a la gente ferozmente, con impunidad, y arrestaron a cualquiera. Nos obligaron a quedarnos en el patio toda la noche. Podíamos escuchar a las mujeres golpear. No entiendo esa crueldad".**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Además, después de ser liberados explicaban que no solo habían sido maltratados físicamente sino también humillados; muchos fueron obligados a arrodillarse semidesnudos.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Con estos, que no son los únicos casos que se han dado durante este año, se evidencia un patrón en las Fuerzas Públicas que en medio de manifestaciones pacíficas y operativos utilizan la violencia y le quitan las pertenencias a los manifestantes. Y empeorando la situación, los estados no asumen la responsabilidad de los actos y por el contrario, responden con más violencia y desplegando más agentes.

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->
