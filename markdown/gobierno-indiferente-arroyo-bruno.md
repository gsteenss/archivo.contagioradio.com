Title: Gobierno es indiferente ante desviación del arroyo Bruno
Date: 2016-10-21 17:41
Category: Ambiente, Entrevistas
Tags: Agua, ANLA, Cerrejón, Gobierno, Guajira, indigenas wayuu, Wayuu
Slug: gobierno-indiferente-arroyo-bruno
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/10/Arroyo-Bruno-river-Credit-Doris-Elin-Salazar-e1477085291621.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Primero Noticias] 

###### [21 Oct de 2016]

Recientemente se conoció del **ingreso de maquinaria de la multinacional Cerrejón al arroyo Bruno y de la militarización de la zona.** Los indígenas Wayúu se mostraron sorprendidos por el nivel de abandono y el silencio cómplice del gobierno.

En una región como La Guajira, donde las sequías son intensas, **desviar el cauce del arroyo podría generar un mayor desabastecimiento del líquido.** Este mantiene a las comunidades indígenas de municipios como Maicao, Riohacha, Uribia y Manaure. Le puede interesar: [Serían 27 las fuentes de agua que el Cerrejón ha secado en la Guajira](https://archivo.contagioradio.com/con-el-arroyo-bruno-serian-27-las-fuentes-de-agua-que-el-cerrejon-ha-secado-en-la-guajira/)

Misael Socarrás integrante de Fuerza Mujeres Wayúu, aseguró que pese a las tutelas puestas no ha habido un cumplimiento “se les había ordenado detener los trabajos y al contrario reforzaron la mano de obra para continuar con el trabajo de la canalización”. Le puede interesar: [El Cerrejón incumple tutela fallada a favor de un menor de 2 años](https://archivo.contagioradio.com/el-cerrejon-incumple-tutela-fallada-a-favor-de-un-menor-de-2-anos/)

Misael además dijo que **CorpoGuajira y la Autoridad Nacional de Licencias Ambientales  (ANLA) han otorgado todos los permisos a la multinacional así como su respaldo. **

Se asegura que **bajo este arroyo puede haber cerca de 35 millones de toneladas de carbón**, por los cuales la empresa quiere desviar el arroyo. Leer mas : [Mina de carbón del Cerrejón usa diariamente 17 millones de litros de agua](https://archivo.contagioradio.com/mina-de-carbon-del-cerrejon-usa-diariamente-17-millones-de-litros-de-agua/)

La experiencia ya le ha mostrado a los Wayúu, que **una vez se realiza el desvío del cauce a un río o arroyo estos mueren, y la incertidumbre a la que se ven expuestos los habitantes es grande.**

"La única solución que les pedimos es que nos dejen el arroyo como estaba” concluyó Socarrás. Le puede interesar: [Empresa Cerrejón se extiende pasando por encima de las comunidades de la Guajira](https://archivo.contagioradio.com/empresa-cerrejon-se-extiende-pasando-por-encima-de-las-comunidades-de-la-guajira/)

<iframe id="audio_13422438" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_13422438_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>
