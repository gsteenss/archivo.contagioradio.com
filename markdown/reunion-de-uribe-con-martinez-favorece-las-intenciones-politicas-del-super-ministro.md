Title: Reunión de Uribe con Martínez favorece las intenciones políticas del Super-Ministro
Date: 2015-04-30 17:20
Author: CtgAdm
Category: Nacional, Política
Tags: Alvaro Uribe, Centro Democrático, Fiscalía General de la Nación, Nestor Humberto Martínez, ramiro bejarano
Slug: reunion-de-uribe-con-martinez-favorece-las-intenciones-politicas-del-super-ministro
Status: published

###### Foto: eluniversal.com 

<iframe src="http://www.ivoox.com/player_ek_4431858_2_1.html?data=lZmgk52ZfI6ZmKiakpuJd6Kkk5KSmaiRdo6ZmKiakpKJe6ShkZKSmaiRk9Hdz86SpZiJhpTijLfOz87Ws4y2xs%2FO1MbSs46ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Ramiro Bejarano] 

**Ramiro Bejarano** afirma que el encuentro entre Uribe y Martínez no es útil, puesto que cree que no se puede pensar que se lograrán acercamientos entre unos y otros tan fácilmente, además el Centro Democrático tiene una oportunidad política que no va a desaprovechar y mucho menos acercándose al gobierno con una opinión tan desfavorable.

Bejarano afirma que la reunión **le sirve más a Nestor Humberto Martínez** que al gobierno. El super ministro, según Bejarano, **está buscando el respaldo de todos los sectores políticos para afianzar su aspiración al cargo del Fiscal General.**

Sin embargo, “para que haya un proceso de paz como el nuestro es importante que coincidan todas las voces” pero lo que ha habido no es un encuentro con consecuencias políticas y con una agenda desconocida, por ello, lo que se puede concluir es que el acercamiento es entre Nestor Humberto Martínez y el uribismo, más que entre el gobierno Santos con el **Centro Democrático**.
