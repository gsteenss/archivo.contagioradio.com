Title: Paramilitares amenazan a víctimas colombianas exiliadas en España
Date: 2017-06-12 12:10
Category: DDHH, El mundo, Nacional
Tags: españa, Exliado, paramilitares
Slug: amenazas-de-paramilitares-llegan-a-las-victimas-colombianas-exiliadas-en-espana
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/06/colombianos-en-españa.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [12 Jun. 2017]

El pasado 9 de Junio **paramilitares amenazaron a Nelson Javier Restrepo Arango, integrante del Partido Comunista**, y quien, desde el exilio, ha venido trabajando y exigiendo el derecho a la vida de las personas, tanto en el exilio como en Colombia. El panfleto llegó a su lugar de residencia en España y en este, las Autodefensas Unidas de Colombia - AUC - lo declaran objetivo militar.

Según la denuncia, el comunicado firmado por las AUC también señala a otras víctimas de crímenes de Estado y se **afirma que todos los comunistas “deben morir a golpes”,** lo cual sería una clara amenaza a quienes desde fuera de Colombia luchan por su derecho a regresar en condiciones de seguridad y dignidad, pero también a personas que hacen eco de las exigencias de las víctimas y las organizaciones en Colombia. Le puede interesar: Le puede interesar: ["Víctimas de Crímenes de Estado piden que se desclasifiquen archivos de inteligencia"](https://archivo.contagioradio.com/victimas-de-crimenes-de-estado-piden-que-se-desclasifiquen-archivos-militares/)

Adicionalmente, denuncian que tanto el círculo de familiares de Restrepo, como él mismo, desde el mes de Abril han estado recibiendo mensajes intimidatorios que llegan a su perfil personal en redes sociales, o con **llamadas intimidantes a integrantes de la familia, quienes no cuentan con medidas de protección** que puedan garantizar el libre ejercicio de sus derechos.

Para los integrantes del partido Comunista esta amenaza también **tiene el antecedente de la llamada “Operación Europa” como se le conoció a las actividades de seguimientos a defensores de DD.HH. en ese continente** y que habría sido organizada en el gobierno de Álvaro Uribe a través del extinto Departamento de Seguridad, DAS.

Aunque hay varias personas vinculadas a ese proceso, se desconocen los avances en la investigación y también las razones por las cuales se realizaron las actividades ilegales. Le puede interesar:[¡Las víctimas de crímenes de Estado tenemos derecho a ser escuchadas!](https://archivo.contagioradio.com/las-victimas-de-crimenes-de-estado-tenemos-derecho-a-ser-escuchadas/)

Según el comunicado, para prevenir hechos lamentables **es necesario que las autoridades competentes en España y en Colombia investiguen** la procedencia de estas amenazas, se judicialice a los responsables y se tomen las medidas necesarias para garantizar la vida e integridad de las personas en el exilio.

[Denuncia Publica Nelson Restrepo Arango](https://www.scribd.com/document/351058784/Denuncia-Publica-Nelson-Restrepo-Arango#from_embed "View Denuncia Publica Nelson Restrepo Arango on Scribd") by [Contagioradio](https://es.scribd.com/user/276652802/Contagioradio#from_embed "View Contagioradio's profile on Scribd") on Scribd

<iframe id="doc_88761" class="scribd_iframe_embed" src="https://www.scribd.com/embeds/351058784/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-DPeVpqQ7L51qJVgYIQlL&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="0.7080062794348508"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)

 
