Title: Juez 30 mantiene la detención de Álvaro Uribe
Date: 2020-09-22 19:28
Author: AdminContagio
Category: Actualidad, Nacional
Tags: Álvaro Uribe Vélez, Audiencia Uribe, Caso Uribe, corte suprema
Slug: caso-uribe-ley-600-o-ley-906-justicia-o-impunidad
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/09/Caso-Uribe.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/09/Caso-Uribe.jpeg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/09/Caso-Uribe-1.jpeg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

Este martes la Juez 30 de Control de Garantías decretó adelantar el proceso en contra del expresidente Álvaro Uribe a través del régimen procesal de la Ley 906 de 2004 y mantuvo en suspenso la detención preventiva que pesa en contra de Uribe hasta tanto la Corte Suprema de Justicia se pronuncie sobre su decisión.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Luego de la remisión del expediente del caso por parte de la Corte Suprema hacia la Fiscalía, se solicitó por parte de la defensa del expresidente Uribe, reconsiderar la medida de aseguramiento con prisión domiciliaria que pesa en su contra ante la Juez 30 de Control de Garantías que asumió la competencia por reparto.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

No obstante, antes de que la funcionaria judicial pudiese pronunciarse sobre la solicitud concreta; se suscito **un nuevo debate jurídico sobre el estatuto procesal bajo el cual se deben regir las actuaciones jurídicas venideras en el caso.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por un lado, se pidió por parte del abogado de Iván Cepeda acreditado como víctima, y también por Eduardo Montealegre y Jorge Perdomo, quienes buscan alcanzar la misma calidad en el proceso, que el caso siguiera su curso bajo la [Ley 600](http://www.secretariasenado.gov.co/senado/basedoc/ley_0600_2000.html) del año 2000, estatuto procesal bajo el cual se venía surtiendo el proceso mientras estuvo en la competencia de la Corte Suprema de Justicia. (Lea también: [“Responsabilidad de Álvaro Uribe en el caso de Diego Cadena es innegable” - Iván Cepeda](https://archivo.contagioradio.com/alvaro-uribe-tiene-responsabilidad-innegable-en-caso-diego-cadena/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por otro lado, se pidió por parte de Jaime Lombana, apoderado del expresidente Uribe como indiciado, y también del delegado de la Fiscalía y del Ministerio Público, que el proceso pasará a tramitarse bajo la [Ley 906](http://www.secretariasenado.gov.co/senado/basedoc/ley_0906_2004.html) de 2004.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Los argumentos por la Ley 600 de 2000

<!-- /wp:heading -->

<!-- wp:paragraph -->

Para las víctimas y aquellas que buscan ser acreditadas como tal,  el proceso debe continuar en Ley 600, teniendo en cuenta que era la norma aplicable para el momento de comisión de las conductas, ya que, **Álvaro Uribe era plenamente consciente de su condición de senador y aforado cuando desplegó las presuntas conductas punibles que se le atribuyen por fraude procesal y soborno, en coordinación con su abogado Diego Cadena.** (Le puede interesar: [Razones por las que Diego Cadena, abogado de Álvaro Uribe, será imputado por la Fiscalía](https://archivo.contagioradio.com/razones-por-las-que-diego-cadena-abogado-de-alvaro-uribe-sera-imputado-por-la-fiscalia/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El exfiscal Montealgre planteó que el principio de legalidad, esto es, la preexistencia de legislación para el juzgamiento, era una garantía no solo para el procesado sino para la institucionalidad y que existía una prohibición de que se establezcan, con posterioridad a la comisión de las conductas, cambios de normatividad. **También sugirió que lo que estaría haciendo Uribe de manera deliberada es elegir el juez que conocerá de su proceso en contravía de las normas constitucionales del debido proceso.** (Le puede interesar: [Colombia debe respetar a sus Magistrados y sus decisiones](https://archivo.contagioradio.com/colombia-debe-respetar-a-sus-magistrados-y-sus-decisiones/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Adicionalmente, el exvicefiscal Perdomo se sumó a lo expuesto y agregó que el concepto de fuero está supeditado a definir el funcionario competente para conocer de la investigación y el juicio, más no, la ley o estatuto aplicable, razón por la cual el proceso debía mantenerse en Ley 600, aun con la renuncia como senador y consecuentemente como aforado de Álvaro Uribe.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Los argumentos por la Ley 906 de 2004

<!-- /wp:heading -->

<!-- wp:paragraph -->

Jaime Lombana, apoderado de Uribe, sostuvo que la norma aplicable tendría que ser la Ley 906, basado en la renuncia del exsenador Uribe a su fuero, el cual se materializó, según dijo, con su dimisión al Congreso. Adicionalmente, se centró en la fecha de comisión de los hechos que datan del año 2018. (Le puede interesar: [Renuncia de Uribe Vélez a su curul puede ser una maniobra para dilatar el proceso y generar impunidad](https://archivo.contagioradio.com/renuncia-alvaro-uribe-velez-no-responder-justicia-victimas/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

A estos argumentos se sumaron la representante del Ministerio Público, María Lourdes Hernández; y también el delegado de la Fiscalía, Gabriel Jaimes; quienes lo complementaron con precedentes jurisprudenciales.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Las principales diferencias entre Ley 600 y Ley 906  

<!-- /wp:heading -->

<!-- wp:paragraph -->

Una de las diferencias entre seguir con uno u otro estatuto es que **de seguir con la Ley 600, se aplicaría el principio de permanencia de la prueba y lo actuado hasta esa etapa del proceso por la Sala de Instrucción de la Corte Suprema quedaría en firme.** De otra parte, si se decidiese aplicar la Ley 906, se entraría en un debate jurídico sobre la fase desde la cual debería retomarse el proceso, e incluso podría retrotraerse todo lo actuado y empezar desde cero, lo que obligaría a practicar nuevamente las pruebas y demás actos procesales que ya se habían surtido por parte de la Sala de Instrucción de la Corte.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

**Esto tiene su alcance también en la medida de aseguramiento que hoy pesa contra Uribe, pues si se sigue con la Ley 600 se considera válido lo decretado por la Sala de Instrucción de la Corte y se puede considerar precluída esa fase procesal, lo que no permitiría entrar a reconsiderar la medida.** Por el contrario, si se continuara con la Ley 906, esto podría retrotraer lo actuado por la Corte e inmediatamente levantar la medida, lo que obligaría a entrar a discutirla nuevamente. En este último caso, la Fiscalía sería quien estaría llamada a solicitar la medida.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Otra de las diferencias trascendentales es el protagonismo de la Fiscalía. En la Ley 600 la instrucción e investigación se alterna con el Juez competente; mientras que en la Ley 906 el rol de la Fiscalía es protagónico y exclusivo frente al impulso que le da al proceso.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

**Esto es especialmente importante si se tiene en cuenta que por parte de las víctimas existen serias dudas sobre este ente y las actuaciones que podría desarrollar en el proceso**, entre otras cosas, por la estrecha cercanía que tiene el Fiscal General, Francisco Barbosa como cabeza máxima de esa institución, con el presidente Duque y su Partido, que es el mismo al que está adscrito el procesado Álvaro Uribe. (Le puede interesar: [Iván Cepeda recusará a fiscal y llevará caso Uribe a instancias internacionales](https://archivo.contagioradio.com/ivan-cepeda-recusara-a-fiscal-y-llevara-caso-uribe-a-instancias-internacionales/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El exfiscal Eduardo Montealegre planteó que otra de las diferencias entre uno y otro estatuto, es que con la Ley 600 el proceso debería ser instruido por un Fiscal Delegado ante la Corte Suprema de Justicia y la etapa de juicio asumida por este Alto Tribunal. Mientras que con la Ley 906 la competencia en la investigación recaería sobre un delegado elegido por el Fiscal Barbosa y la fase de juicio, presidida por un Juez Penal del Circuito. Cabe anotar que en este último caso, la segunda instancia debería ser asumida por un Tribunal Penal y la eventual casación llegaría al conocimiento de la Corte Suprema.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### ¿Qué sigue en el caso Uribe?

<!-- /wp:heading -->

<!-- wp:paragraph -->

Por ahora, lo que entrará a dirimir la Corte Suprema es si le asiste razón a la Juez 30 de Control de Garantías en trasladar el procedimiento al régimen de la Ley 906 en cuyo caso quedaría plenamente facultada para decidir sobre la libertad, o no, del expresidente Uribe, resolviendo la solicitud planteada por su defensa.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En el entretanto, el expresidente Uribe seguirá cobijado con medida de aseguramientos de prisión domiciliaria preventiva, la cual pesa en su contra desde el pasado 4 de agosto.

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->
