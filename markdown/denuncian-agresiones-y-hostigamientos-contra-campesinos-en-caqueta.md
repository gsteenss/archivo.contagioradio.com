Title: Denuncian agresiones y hostigamientos contra campesinos en Caquetá
Date: 2017-03-01 15:50
Category: Ambiente, Nacional
Tags: Caquetá, Erradicaciones Forzadas
Slug: denuncian-agresiones-y-hostigamientos-contra-campesinos-en-caqueta
Status: published

###### [Foto: Colombia Informa] 

###### [1 Mar 2017] 

Campesinos en el Caquetá denuncian graves incumplimientos por parte del Gobierno Nacional en el plan de sustitución de cultivos ilícitos, al igual que retenciones ilegales y una difícil situación de orden público en las veredas, en donde la **Fuerza Pública estaría amedrentando a los habitantes con la activación de artefactos explosivos.**

De acuerdo con Carlos Alberto Parra, vicepresidente de la Asociación Portal del Fragua, en conversación con REC.Sur, **agentes ingresaron a diferentes fincas y detuvieron a campesinos, en la vereda del Sinaí, en la inspección de Fraguita.** Le puede interesar: ["6 Campesinos heridos en manifestaciones contra erradicación forzada"](https://archivo.contagioradio.com/campesinos-exigen-presencia-del-gobierno-en-tumaco/)

Los campesinos informaron que durante el día, escucharon explosiones en la vereda, señalando que éstas acciones, por parte de la Policía o Fuerzas Militares, pueden entender como una intimidación hacia la población, Parra afirmó que puede ser para que **“las personas se llenen de miedo y no salgan a movilizarce en protesta” frente a los incumplimientos de los planes de sustitución.**

 Además resaltaron que en diferentes veredas como la Fraguita y Zabaleta, se **vienen adelantando erradicaciones forzadas, pese  a que ya exista un plan de sustitución cultivos**, pasando por alto conversaciones y acuerdos a los que ya se habían llegado con el gobierno. Le puede interesar: ["ESMAD atacó a campesinos que protestaban contra erradicación forzada"](https://archivo.contagioradio.com/campesinos-exigen-al-gobierno-frenar-erradicaciones-forzadas/)

En otros departamentos del país como el Putumayo, Nariño y el Norte de Santander, los campesinos también han denunciado las erradicaciones forzadas que se vienen adelantando, y que de acuerdo con COCCAM (Coordinadora de Cultivadores de Coca, Amapola y Marihuana), **van en contravía de lo pactado en el punto cuatro de los acuerdos de paz sobre sustitución de cultivos ilícitos y de las necesidades que afrontan los campesinos.**

<iframe id="audio_17304482" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_17304482_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
