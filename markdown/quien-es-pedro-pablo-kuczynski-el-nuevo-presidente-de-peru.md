Title: ¿Quién es Pedro Pablo Kuczynski el nuevo presidente de Perú?
Date: 2016-07-28 11:33
Category: El mundo, Política
Tags: Mineria, Pedro Pablo Kuczinsky, Perú, PPK
Slug: quien-es-pedro-pablo-kuczynski-el-nuevo-presidente-de-peru
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/07/ppk.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Empodera 

###### [28 Jul 2017] 

Este jueves con el marco del día de la Independencia Nacional, se posesiona **Pedro Pablo Kuczinsky,** como presidente de la República del Perú, luego de haber obtenido en la segunda vuelta electoral una votación del ** 50,12% de los votos frente al 49,88% que logró Keiko Fujimori, candidata por el partido **Fuerza Popular.

Kuczynski ha asegurado que busca establecer conversaciones con los partidos opositores y afirma que generará una **"revolución social"**, pese a que representa la continuidad de un modelo neoliberal y extractivista pues se dice que pretende reactivar proyectos mineros claves para el país y que actualmente están paralizados por conflictos sociales y ambientales.

**El ahora presidente de Perú y** **exbanquero de Wall Street,** **se le involucra con los Papeles de Panamá por la aparición de una nota dirigida a Mossack Fonseca** en 2006 en la que recomendaba que decía le “era grato presentar al señor Francisco Pardo Masones” dando fe de conocerle de tiempo atrás, de sus cualidades y prestancia; un personaje que resultaría haciendo negocios en Cuba con pasaportes venezolanos.

Además se le atribuye la responsabilidad de permitir que la Internacional Petroleum Company (IPC) se llevara 17 millones de dólares del erario nacional, cuando se desempeñaba como asesor económico y gerente del Banco Central de Reserva, teniendo conocimiento que la empresa había sido nacionalizada en 1968.

Otro de los escándalos con los que la prensa relaciona **a Kuczinsky es el generado su ONG ‘Agua limpia’, por medio de la cual habría canalizado los recursos para su campaña con fondos de USAID y la CIA,** con la excusa de proveer agua potable y alcantarillado a poblaciones, dinero que sería recuperado por los inversionistas de resultar electo con la explotación de los recursos hídricos con un alto costo para los pobladores.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
