Title: Compulsan copias contra Álvaro Uribe Vélez por dos masacres en Antioquia
Date: 2018-02-06 12:42
Category: DDHH, Nacional
Tags: alvaro uribe velez, Antioquia, Masacre El Aro, masacre La Granja, Paramilitarismo
Slug: alvaro_uribe_investigacion_masacres_antioquia_paramilitarismo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/02/uribe-princeton-contagioradio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: miaminewtimes.com] 

###### [6 Feb 2018]

Según ha conocido el periódico *El Tiempo, *una sentencia del Tribunal Superior de Medellín, ha compulsado copias contra el expresidente y hoy senador Álvaro Uribe Vélez, por las mascres de El Aro y La Granja y además por el asesinato del defensor de Derechos Humanos, Jesús María Valle, en la época en que Uribe fue gobernador de Antioquia.

“Existen suficientes elementos de juicio conforme a lo expresado en esa decisión, que **probablemente comprometen la responsabilidad penal de varias personas como el gobernador de Antioquia de ese entonces Álvaro Uribe Vélez**”, dice la sentencia a la cual tuvo acceso ese medio.

Cabe recordar que en anteriores ocasiones se ha asegurado que cuando Uribe fue  gobernador recibió denuncias por parte de organizaciones de derechos humanos sobre la proliferación de grupos paramilitares en la zona de El Aro, sin embargo, el gobernador no habría acatado las advertencias y por el contrario **"Los paramilitares estaban a sus anchas en Antioquia, luego de la promoción de las empresas Convivir"**, como lo ha denunciado el congresista Iván Cepeda.

De hecho, la sentencia del Tribunal Superior de Medellín indica que "El solo hecho de que las Fuerzas Militares permitieran su funcionamiento (de los paramilitares), de no combatirlos, como era su deber legal y constitucional, es indicador de su compromiso con esas organizaciones ilegales, y por supuesto de los actos y conductas punibles cometidas, **igual ocurre con la gobernación de Antioquia y sus funcionarios de más alto rango, ellos patrocinaron las Convivir que fue el apoyo de los particulares a las Autodefensas**, es decir ayudaron con sus actuaciones positivas o negativas, permitieron y patrocinaron el desarrollo de tales organizaciones y de los delitos que ellos cometieron. Ello se tiene que investigar y juzgar''.

### **Las masacres** 

La masacre de El Aro, ocurrió en 1997 el 22 de octubre. Según la Comisión Interamericana de DDHH, en ese hecho fueron asesinadas 15 personas. Declaraciones del paramilitar Francisco Barreto, también asesinado, afirman que el gobernador de Antioquia, es decir **Uribe Vélez, habría usado un helicóptero de la gobernación en medio de la masacre y la habría corroborado personalmente.**

La masacre de La Granja, sucedió el 11 de junio de 1996 cuando las Autodefensas Campesinas de Córdoba y Urabá, Accu, asesinaron  a cinco personas en el corregimiento La Granja, en el municipio de Ituango. Ese día, los paramilitares mantuvieron encerrados a los habitantes, los acusaron de ser auxiliadores de la guerrilla y los torturaron y asesinaron delante de sus vecinos y familiares.

Por su parte el defensor de Derechos Humanos, Jesús María Valle en Antioquia fue asesinado el 27 de febrero de 1998 cuando fue baleado en su oficina en el cuarto piso del edificio Colón en el centro de Medellín. El abogado había denunciado la colaboración de militares de la IV Brigada en las masacres de El Aro y La Granja. La misma semana de su asesinato, Valle había rendido versión libre en el proceso por injuria y calumnia que en su contra iniciaron miembros del Ejército por estas mismas denuncias.

El 11 de junio de 1996 en una entrevista del periódico El Colombiano, Valle aseguró '**'Le pedí al gobernador y al comandante de la tercera Brigada que protegiera la población civil de mi pueblo, porque de septiembre a hoy han muerto más de 150 persona**s''. En ese entonces el gobernador que era Uribe Vélez, señaló al abogado como enemigo de las fuerzas armadas, y fue denunciado por calumnia.

Cabe recordar desde Miami "Don Berna" había declarado sobre el crimen del humorista Jaime Garzón, del defensor de derechos humanos Eduardo Umaña, el crimen de Jesús María valle, abogado defensor de Derechos Humanos, y Francisco Villalba testigo de la masacre del Aro. "Don Berna" aseguraba que estos **asesinatos tenían un modus operandi en donde las ordenes venían desde las altas esferas, luego estas contactaban con las Autodefensas quienes planeaban el crimen** y miembros de la banda “La Terraza” ejecutaban estas órdenes.

### **Los militares implicados** 

De acuerdo con la sentencia a la que ha tenido acceso El Tiempo, las compulsas de copias también aparecen el general Carlos Alberto Ospina, el coronel Germán Morantes Hernández, el mayor Emiro Barrios, la oficial Aurora Bonilla, los tenientes Cristian Arias y Everardo Bolaños Galindo y el agente de la Policía, Carlos Emilio Gañán Sánchez.

En Desarrollo...
