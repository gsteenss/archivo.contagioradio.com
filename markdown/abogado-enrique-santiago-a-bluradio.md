Title: Abogado Enrique Santiago desmiente a Blu Radio
Date: 2016-02-12 19:00
Category: Nacional, Paz
Tags: Bluradio, Conversaciones de paz de la habana, enrique santiago, FARC, Jurisdicción Especial de Paz
Slug: abogado-enrique-santiago-a-bluradio
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/02/abogado-enrique-santiago-a-bluradio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

El abogado asesor de la mesa de conversaciones de la Habana, aclara a través de un comunicado público las palabras que pronunció en una reunión de ACORE y calificó de falta de toda ética periodística la "manipulación" que realizó Blu Radio de una reunión privada y que, según Enrique Santiago, el General Ruiz, afirmó, no sería publicada.

A continuación compartimos el contenido completo de la aclaración del abogado.

### [**"SOBRE LA POLEMICA GENERADA POR BLURADIO**] 

Escribo estas líneas no exactamente por voluntad propia  –creo contraproducente alimentar absurdas polémicas-, sino  a petición de distintas personas que me advierten del efecto pernicioso que la actuación de Bluradio en estos días, puede tener para las necesarias actividades de pedagogía social que requiere el proceso de paz.

No entiendo que a estas alturas del proceso de La Habana se busque generar una polémica estéril en lugar de utilizar los espacios en medios de comunicación para profundizar el debate y las explicaciones sobre los distintos acuerdos alcanzados en la Mesa de Diálogos, en un momento en el que la paz es una realidad al alcance de la mano.

La reunión efectuada en ACORE en la que intervine el pasado dia 10 de febrero, era un seminario de trabajo, no una conferencia pública, en la que también participaron representantes del Gobierno. El General Ruiz, Presidente de ACORE,  me confirmó expresamente que el contenido de la reunión era exclusivamente interno, no para divulgar a los medios de comunicación.

Me sorprende por tanto que se hayan difundido las grabaciones, por ser ello contrario a lo que me aseguró el General. Es decir, la intervención difundida no es una declaración a la prensa ni una intervención pública, por lo que no es lícito que se difundan sin mi autorización y la de ACORE, como me consta que ha ocurrido. En todo caso, afirmo contundentemente que nada de lo que allí se hablo es contrario a lo que se ha acordado y consta en los distintos textos oficiales de la Mesa de Conversaciones divulgados a la opinión pública.

De hecho, ninguna de mis afirmaciones en dicho seminario causó estupor o rechazo entre los asistentes, al revés, entiendo que la reunión fue muy correcta y los militares asistentes entendieron que todo lo allí dicho fue muy útil para aclarar malos entendidos sobre los acuerdos alcanzado en materia de justicia. Es decir, cada palabra pronunciada en su contexto tenía un sentido lógico, que se pierde al efectuar recortes, “copia y pega”, de las mismas. Y eso, Bluradio lo sabe mejor que yo.

Las grabaciones descontextualizadas que difunde Bluradio, corresponden a una contestación a una pregunta que me formula un interviniente en el seminario, sobre si es obligatorio para cualquier persona acusada, no solo para guerrilleros, reconocer cualquier acusación formulada ante la Jurisdicción Especial de Paz. Y obviamente no es así ni puede ser así en ningún sistema jurídico, en el que todo el mundo tiene derecho a defenderse.

Mi afirmación sobre “lo obvio y aburrido” hace referencia no a los delitos o conductas de las que se acuse a nadie, o concretamente a las FARC, sino a la reiterada repetición por parte de algunas personas o medios de comunicación de acusaciones infundadas sobre supuestos crímenes y si esto debe o no ser reconocido como cierto por el mero hecho de su repetición “goebeliana” hasta el infinito. Eso es lo “obvio y aburrido”, que existan acusaciones reiteradas y sin fundamento jurídico sobre supuestos crímenes, acusaciones sin fundamento jurídico serio efectuadas  por algunas personas o medios, que dificultan la comprensión del proceso de paz y lo distorsionan. Y un ejemplo claro de esta actitud  es la “chuzada” de Blu Radio de una conversación privada y la emisión de unas grabaciones de forma descontextualizada, efectuando recortes interesados que sacan lo afirmado de su contexto.

No me parece condenable afirmar que hay hechos obvios que han ocurrido en el conflicto, ello, valga la redundancia, es obvio. Y efectivamente es rechazable calificar cualquier conducta criminal como “aburrida”, y es además un insulto a las víctimas. Nunca lo he hecho ni lo haré, y si alguien lo ha entendido así, contundentemente aclaro que nunca ha sido mi intención hacerlo ni creo haberlo hecho.

Lamento profundamente que así se haya entendido, presento mis más sinceras excusas a quienes se hubieran sentido afectados, y espero que ojalá puedan comprender el sentido correcto de mis palabras paliando así su lógico dolor.

Añado que  los que han chuzado y después descontextualizado una grabación también deberían pedir perdón a las victimas ofendidas por esa manipulación periodística que ha provocado una revictimizacion y seguro un inmenso dolor. Ellos son al fin y al cabo quienes han provocado ese dolor y de hecho creo que es lo que buscaban al difundir las grabaciones, causar un daño a las víctimas del conflicto que no podían ignorar iba a producirse.

Pero pidan o no perdón –me temo que no lo harán- , yo si lo pido a quienes se hayan sentido dolidos por la interpretación que puede darse a mis palabras, y también pido perdón por haber creido ilusamente que lo que me afirmó el General Ruiz –que la grabación efectuada seria para exclusiva utilización y análisis interno de ACORE-, por no haber podido evitar que se descontextualicen mis afirmaciones, y porque al fin y al cabo son mis palabras descontextualizadas,  las que sin duda se han utilizado para causar dolor a las víctimas.

Estoy seguro que ACORE opina lo mismo que yo sobre la finalidad de esta impropia actuación de Bluradio.

Dicho lo anterior, a lo que me he querido referir al utilizar los términos “obvio y aburrido” no es a que las FARC deban o no aceptar responsabilidad por una determinada conducta criminal, sino a la insistencia de algunos sectores en acusar sin fundamento a los rebeldes de la comisión de ciertos crímenes, de realización de ciertas conductas criminales, que a mi entender no han existido, y a repetir incesantemente falsedades con la intención de interferir negativamente en el proceso de paz.

Por ejemplo, afirmo en la grabación que algunas acusaciones son “rocambolescas”. Al efectuar esta afirmación pensaba en la acusación que se ha efectuado a las FARC de cometer el muy grave delito de “esterilización forzosa”, por el hecho de que las mujeres vinculadas a la guerrilla puedan utilizar Dispositivos Intrauterinos (DIU) para planificación familiar.

Nadie en su sano juicio va a aceptar esta acusación, puesto que la utilización de métodos anticonceptivos es una victoria del movimiento democrático de mujeres en sociedades claramente patriarcales. Y si se llegara a presentar ante un tribunal esa descabellada acusación, como abogado le aconsejaré al acusado o acusada que se defienda, que no acepte nunca que utilizar un DIU es equiparable a una “esterilización forzosa”, y ello aun a riesgo de saber que podría ser condena por el tribunal.

No merece la pena profundizar una polémica sobre cuales acusaciones van a aceptar en su momento las partes en el conflicto. Eso se comprobará cuando comience a realizar sus tareas el Tribunal para la paz, y en ese momento la sociedad colombiana apreciará la valentía de cada parte a la hora de reconocer lo que le incumbe.

Durante este proceso de Paz, han sido ya varias las manifestaciones de las FARC EP reconociendo responsabilidades, en un contexto en el cual la otra parte en las conversaciones todavía no ha dado ese paso de forma expresa en la Mesa de Diálogos.

Por ello, no tiene fundamento alguno especular con que la guerrilla va a intentar sustraerse a sus responsabilidades, al igual que es absurdo pensar que el acuerdo alcanzado sobre justicia impida defenderse a quien entienda que se le presentan acusaciones infundadas.

De ser así, estaríamos ante un sistema inquisitorial, incompatible con un estado de derecho. Entre los innumerables insultos que en esta ocasión he recibido por twiter – algo habitual siempre cuando me entrevista BluRadio- los que más me han sorprendido han sido los referidos a mi “tono de voz”. Sinceramente lamento que mi entonación sea causa de polémica en Colombia, pero muy humildemente les pido a quienes les moleste ese tono de voz, que piensen que no es ni más ni menos la entonación habitual en la meseta castellana, de la que soy originario y donde resido.

Ojalá pudiera entonar el idioma español de la forma melodiosa, casi musical, como lo hace el pueblo colombiano. En todo caso, felicito a Bluradio porque su objetivo desde hace tiempo esta vez sí lo ha conseguido, la tergiversación de mis palabras, lo que nunca han conseguido en buena lid, entrevistándome.

Aunque BluRadio no puede desconocer que han tenido que recurrir a métodos obtusos, excluidos expresamente de cualquier manual de estilo y ética periodística, para lograrlo: chuzar una conversación privada –las difundidas no son declaraciones a medios ni corresponden a una intervención publica- y recortar y reorganizar a su antojo las grabaciones, descontextualizadamente, actuaciones propias de “propaganda de guerra” más que del actual contexto de búsqueda de paz y reconciliación nacional.

En varias ocasiones he sido entrevistado por BluRadio, entrevistas que en demasiados casos más bien me parecían interrogatorios judiciales en los que yo era el acusado, efectuadas por algunos periodistas que por momentos parecen desconocer u olvidar el mandato deontológico de mínima imparcialidad en el ejercicio de su profesión.

Aun así, me someto gustosamente a dichos “interrogatorios” y me seguiré sometiendo, porque considero una obligación ética explicar a la sociedad colombiana lo que ocurre en  
la Habana. Allá cada cual con sus responsabilidades ante la historia de Colombia, en la que va a quedar claramente escrito quien empujó a favor de la paz y quien empuja,  
contra toda lógica y sentido común, en contra.

Estoy convencido de que esta artificial polémica es perjudicial para el proceso de paz y no soy yo quien vaya a alimentarla. Pero reflexionen quienes la han organizado, o  
quienes siempre buscan zancadillear el proceso de paz asiéndose a cualquier excusa.

También los ánimos belicistas deberían “desmovilizarse”.

Enrique SANTIAGO ROMERO. Asesor jurídico de la delegación de paz de las FARC."
