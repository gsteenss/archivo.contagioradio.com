Title: Teatro por la reivindicación de la mujer y su papel de cara a la paz
Date: 2015-07-25 14:00
Author: AdminContagio
Category: 24 Cuadros, Cultura
Tags: Centro de Memoria Histórica, Corporación Colombiana de Teatro, Festival Mujeres en escena por la paz, Patricia Ariza, Teatro Jorge Eliécer Gaitán, teatro la candelaria
Slug: teatro-por-la-reivindicacion-de-la-mujer-y-su-papel-de-cara-a-la-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/07/imagen-festival.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [25, Julio, 2015] 

Del 1 al 23 de agosto, llega la XXIV Edición del **Festival Mujeres en escena por la paz,** un espacio que busca resaltar la importancia que tiene la mujer en la construcción de caminos hacia la paz y la reconciliación de Colombia, por medio de expresiones artísticas de equidad como el teatro.

"**Soldados**" obra del grupo nacional "Tramaluna" dirigida por la reconocida directora **Patricia Ariza**, será la primera de más de **100 funciones** que serán presentadas en **21 escenarios** de la capital del país entre los que se cuentan el teatro municipal Jorge Eliécer Gaitán, el teatro La Candelaria, la sala Seiki sano y 3 universidades bogotanas. 30 de las presentaciones programadas serán de ingreso gratuito.

<iframe src="https://www.youtube.com/embed/fTDr09TPpz4" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

**60 agrupaciones**, pondrán en escena piezas como “De Monstruos y Martirio” de la directora Victoria Valencia o “Rastros sin Rostro” de Liliana Hurtado;  e **internacionales** como “María (Tina Modotti)”de los directores Haydeé Boetto y Gabriel Figueroa Pacheco; “Pequeños territorios en reconstrucción” del director Jorge Vargas, o “El Discurso de los Pájaros”de Delia Coto. (Ver programación del [1 al 12 de Agosto](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/07/Programación-1.jpg)  y del [13 al 13 de Agosto](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/07/Programación-2.jpg))

La programación de salas se complementa con actividades como el **Encuentro Polifónico Mujeres y Paz: Voces diversas desde los territorios**, que tendrá lugar en el Centro de memoria histórica, un espacio que aviva el debate en cuestiones de género y talleres de formación, para llevar a los participantes a cuestionarse  sobre la forma de concebir el papel que han asumido las mujeres,  **el arte y la paz en las transformaciones de la realidad**.

El Festival Mujeres en escena por la paz, es una iniciativa de la Corporación colombiana de Teatro, creada y liderada por Patricia Ariza, quien afirma que “hacer este festival en estos momentos **es una forma también de participar en la paz**, y las mujeres tenemos que desarrollar un rol muy importante movilizándonos desde la cultura, desde el arte, desde la política y desde todos los ámbitos que nos correspondan ”.

Las boletas están en pre-venta en la taquilla de la Corporación hasta el próximos 31 de Julio, con un costo de \$7000 pesos en ubicación general y \$10.000 pesos a partir del 1 de agosto, fecha de inicio del Festival.
