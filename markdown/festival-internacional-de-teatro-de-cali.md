Title: Desde hoy en Cali se respira Teatro
Date: 2017-10-20 16:18
Category: Cultura
Tags: Cali, Festival, teatro
Slug: festival-internacional-de-teatro-de-cali
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/10/Afiche-festival-teatro.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: FESTCALI 

###### 18 Oct 2017 

La fiesta teatral más importante del sur occidente colombiano vuelve en una nueva edición del Festival Internacional de Teatro de Cali FITCALI, un espacio que del 20 al 28 de Octubre llevara a propios y extraños una muestra de artes escénicas nacionales e internacionales de alta calidad.

La presente edición, es heredera del evento organizado hasta el año 2011 por la Fundación Festival de Teatro de Cali. Luego de más de dos años de trabajo y concertación, y gracias al apoyo de la Secretaria de Cultura de la ciudad, la comunidad teatral logró organizar la muestra que retorna en este 2017, tal como lo cuenta Beatriz Monsalve, Directora ejecutiva del evento.

Más de 350 artistas se tomarán 31 espacios de la ciudad a los cuales pueden asistir toda la comunidad, con una programación de 95 funciones que se incluye teatro callejero, infantil, títeres, circo, teatro contemporáneo y alternativo.

El Festival abre sus fronteras con la participación de 7 países entre los que se encuentran Cuba, de México el maestro Antonio Zuñiga, Israel, Argentina, China con la Academia de teatro de Shangai y la obra La Metamorfosis y África. La representación nacional va por cuenta de 11 compañías colombianas y 24 locales, con lo mejor de su repertorio escénico.

Con un espectáculo de circo se realizará el lanzamiento el viernes 20 de octubre, en el Teatro al aire libre Los Cristales, con Dalicioso, un espectáculo de circo contemporáneo; obra ganadora del estímulo de la Secretaría de Cultura en el 2016, de la compañía Circo Herencias.

Así mismo algunos espectáculos alternativos como la obra Historias comunes de anónimos viajantes de la compañía Mexicana Carretera 45, entre otras compañías, obras y actividades académicas, harán parte de la diversa programación. (Le puede interesar: [Teatro y acrobacias en "La mujer que perdió la cabeza"](https://archivo.contagioradio.com/teatro-mujer-cabeza/).

Con la intención de llevar la mayor cantidad de personas a las salas la tarifa establecida para las funciones tendrá un precio accesible, gracias al aporte del 70% por parte de la secretaría de cultura de Cali, gracias al cual la entrada pasa de \$10.000 pesos a \$3.000 pesos. Las boletas están disponibles en las taquillas de Cine Colombia (Primera Fila).

**Homenajes**

El evento rendirá un sentido homenaje a instituciones y personas que han aportado al desarrollo del teatro en Cali, tal como el Teatro Esquina Latina, que recibirá el homenaje a Obra y Legado por su trayectoria formando generaciones de artistas por más de 30 años.

Otro de los homenajeados será el fallecido actor Iván Montoya, quien re interpretó el personaje de Jovita Feijoo por más de 16 años, cuya figura es la imagen oficial del evento y el homenaje a Toda una vida en el teatro nacional para Hilda Ruiz y Phanor Terán

**Programación académica**

Dentro de la programación académica los asistentes podrán encontrar talleres en entrenamiento actoral, manejo de títeres, escritura y dirección teatral, así como cuatro mesas temáticas, dos exposiciones y dos desmontajes de obras conformarán la programación académica que se propone para la formación de público profesional generando encuentros entre los invitados, los asistentes y el sector del teatro en Cali.
