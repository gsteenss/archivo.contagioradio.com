Title: ¿Cuál es la causa de la actual crisis migratoria europea?
Date: 2015-09-03 17:07
Category: El mundo, Entrevistas, Política
Tags: Afganistán, Amnistía Internacional, Boko Haram, CEAR, Eritrea, migraciones, migraciones hacia Europa, Nigeria, Siria, Somalia, Unicef
Slug: cual-es-la-causa-de-la-actual-crisis-migratoria-europea
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/lampedusa-inmigrantes-1201x900.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: [migracionesmalaga.wordpress.com]

###### [3 Sept 2015]

La crisis migratoria hacia europea ha alcanzado cifras de más de 60 millones de personas migrantes, duplicando el número de desplazados durante la segunda guerra mundial, debido a que muchas personas se ven obligadas a salir de sus países por la situación de guerra y pobreza en la que viven países como Siria, Afganistán,  Eritrea,  Nigeria, Somalia entre otros.

Se trata de la guerra, detenciones arbitrarias, torturas, abusos sexuales, represión informativa y miseria en la que viven las personas que hacen parte de estos países. De acuerdo a la ACNUR, al menos seis de cada diez personas que atraviesan el Mediterráneo  provienen de países donde se violan los derechos humanos.

[![migracion europa](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/migracion-europa.png){.aligncenter .size-full .wp-image-13360 width="581" height="341"}](https://archivo.contagioradio.com/cual-es-la-causa-de-la-actual-crisis-migratoria-europea/migracion-europa/)

**¿Pero, cuál es la situación en los países para que las personas salgan de ellos?**

**Siria**

De acuerdo a Amnistía Internacional, tras cuatro años de guerra, en Siria han perdido la vida 190.000 personas, 11,6 millones han tenido que dejar sus hogares, de ellas 4 millones han salido de su país.

Por otra parte, por cuenta del Estado Islámico, miles de personas han sido detenidas, secuestradas o desaparecidas y además torturadas.

La situación humanitaria no mejora, según Unicef, 5,6 millones de niños de Siria viven en condiciones de extrema pobreza y han sido víctimas del desplazamiento. Y en datos generales, 250.000 civiles viven en estado de sitio y no tienen como acceder a alimentos, medicinas y combustibles.

**Afganistán**

En los primeros seis meses de 2014, hubo 4.853 víctimas civiles del conflicto de acuerdo con Amnistía Internacional. Los talibanes siguen controlando gran parte del país y se producen detenciones arbitrarias además de negarse el debido proceso. En el informe de Amnistía también se denuncia violaciones de la libertad de expresión y aplicaciones de la pena de muerte en juicios sin garantías.

La situación de las mujeres es crítica, de acuerdo con la ONU, ocho de cada diez mujeres afganas han sido acosadas psicológica, física o sexualmente.

**Eritrea**

La situación de los derechos humanos en este país es calificada como “deplorable” según el último informe mundial de Human Rights Watch (HRW). Las mayores violaciones a derechos humanos se dan por detenciones arbitrarias, se vulnera la libertad de expresión, asociación y religión. "Desde 2001, el Gobierno controla con firmeza el acceso a la información y no permite el trabajo de medios independientes, sindicatos y ONG", dice el informe de HRW, allí también se evidencia el acoso a personas que profesan religiones distintas.

El servicio militar obligatorio es otra de las razones por las cuales muchos jóvenes se ven obligados a salir de su país ya que generalmente se obliga a las personas a prestar servicio más tiempo del pactado y muchas veces los niños son explotados laboralmente.

**Nigeria**

Boko Haram es el terror de los nigerianos, quienes salen de su país por miedo a ser violentados, asesinados o secuestrados.

Las detenciones arbitrarias son pan de cada día en Nigeria, Amnistía Internacional asegura que 7 de cada 10 reclusos no han sido declarados culpables de ningún delito.

Por otra parte, se vive una grave crisis humanitaria, según Amnistía "uno de cada tres nigerianos vive en barriadas marginales o asentamientos informales en condiciones de pobreza y hacinamiento, con acceso limitado a agua apta para el consumo y bajo la amenaza constante de desalojo forzoso".

**Somalia**

Somalia vive un conflicto armado de 24 años. Este es responsables de ataques indiscriminados, violencia sexual y detenciones arbitrarias a cientos de mujeres, hombres, niños y niñas.

En este país, hay altos índices de impunidad debido a que generalmente la justicia es administrada por el ente militar, con procedimientos que no concuerdan con estándares internacionales. E

Los niños son una de las principales víctimas del conflicto armado en Somalia, el grupo armado Al Shabab, recluta niños y ataca escuelas, así mismo la ONU ha documentado casos de reclutamiento infantil por parte de las fuerzas militares del Gobierno.
