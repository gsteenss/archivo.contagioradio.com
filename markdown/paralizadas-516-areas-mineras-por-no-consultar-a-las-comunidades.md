Title: Paralizadas 516 áreas mineras por no consultar a las comunidades
Date: 2015-05-20 13:33
Author: CtgAdm
Category: Nacional, Otra Mirada
Tags: Consejo de Estado paraliza 20 millones de hectáreas mineras, Más del 20% del territorio minero en Colombia paralizado, Paralizadas 516 áreas mineras por no consultar a las comunidades, suspendidas 516 áreas mineras en colombia, Tierra digna
Slug: paralizadas-516-areas-mineras-por-no-consultar-a-las-comunidades
Status: published

###### Foto:JusticiaTributaria.co 

###### **Entrevista a [Ximena Gonzalez], miembro de Tierra Digna:** 

<iframe src="http://www.ivoox.com/player_ek_4523233_2_1.html?data=lZqflZeXd46ZmKiakpyJd6KlkpKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRlMLmwtHW3MbIpdSflpajjYqnd4a1ktfSw9iPqdTo08bhh6iXaaKtyM7Qw9iPscrixtfO1ZDJsoy30NHcz8fNcYarpJKw0dPYpcjd0JC%2Fw8nNs4yhhpywj5k%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

En total **516 áreas mineras de Colombia, que ocupan el 20% del territorio del país, sumando 20 millones de hectáreas han sido paralizadas.** La decisión del Consejo de Estado paraliza al menos durante un año **por no haber consultado** previamente a las **comunidades afectadas**.

Según **Ximena Gonzalez, miembro de la organización social Tierra Digna** la decisión del Consejo de Estado sobre la paralización de 516 áreas mineras es importante de cara al **Plan Nacional de Desarrollo**, ya que la estructura que el Estado Colombiano había planificado va a tener que ser consultada y replanificada.

Ximena nos explica que para poder defender las áreas se organizó un proceso comunitario con **19 consejos comunitarios del Chocó,** donde se presentó una **acción de tutela y otra de nulidad** en contra de todos los decretos emitidos por el estado al respecto de las zonas mineras.

Gonzalez recalca que la **política pública** para la concesión de áreas mineras no se puede elaborar a puerta cerrada y **mucho menos sin estudios que la avalen**, al respecto de beneficios o posibles daños sociales o ambientales.

Para Tierra Digna la decisión es de suma importancia porque **avala las consultas previas a los moradores del lugar afectado,** es decir a las comunidades, aunque añade que también existen posibles daños futuros al ambiente que el estado aún no ha fallado al respecto.

Todas las zonas se encuentran repartidas entre el **Chocó, la Amazonía y el Macizo Colombiano** donde habitan comunidades de **campesinos, afrodescendientes e indígenas,** además de ser las zonas con mayor **biodiversidad** de Colombia.

Por otro lado estas zonas coinciden con focos de conflicto armado, donde el estado mantiene **fuerte presencia militar y las violaciones de DDHH** son la orden del día.

Por último Ximena añade que es un paso adelante para que la **minería se realice de acuerdo a los DDHH**, y no al margen de estos dando paso al paramilitarismo y a las propias dinámicas del conflicto armado.

 
