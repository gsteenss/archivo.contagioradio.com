Title: 5 ciudades en 5 años del Festival de cine por los Derechos Humanos
Date: 2018-08-09 15:51
Author: AdminContagio
Category: 24 Cuadros
Tags: Festival de cine ddhh
Slug: 5-festival-cine-derechos-humanos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/08/festivalddhh.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Impulsos Films 

###### 09 Ago 2018 

Cargada de historias inspiradoras convertidas en película, llega la 5 edición del **Festival Internacional de cine por los Derechos Humanos**, que del 10 al 16 de agosto, se tomará 53 escenarios en Soacha, Medellín, Barranquilla, Cartagena, Pereira y Bogotá.

Con su lema \#ElCineNosUne, el Festival ha preparado una [selección](http://cineporlosderechoshumanos.co/seleccion-oficial/)de** 28 largometrajes** y **34 cortometrajes** entre ficción, documental y animación, provenientes de 18 países que competirán por ser ganadoras en las **8 categorías premiadas**.

<iframe src="https://www.youtube.com/embed/LflLv3vKLrA" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

En el evento los espectadores compartirán con  **40 realizadores y 70 invitados nacionales e internacionales**, de las diferentes actividades que componen la [programación académica y de exhibición](http://cineporlosderechoshumanos.co/programacion/), en espacios como salas de cine, universidades, bibliotecas, y 2 proyecciones al aire libre en Medellín, en el Humedal de Soacha y en la Isla de Bocachica que hace parte del programa “Itinerancia Bolívar”.

La programación académica y cultural compuesta por 20 encuentros entre conversaciones y paneles, busca entablar diálogos alrededor de los Derechos Humanos, y el Taller de formación ImpulsoLAB, un laboratorio de formación para largometraje y un masterclass. (Le  puede interesar:[Ojo, convocatorias abiertas para el 11 Ojo al Sancocho](https://archivo.contagioradio.com/ojo-al-sancocho-2018/))

El lanzamiento del Festival tendrá lugar en el Auditorio Jorge Enrique Molina, de la Universidad Central (Teatro México) a partir de las 6:30 de la tarde, con la proyección del documental “La Mujer de los 7 nombres” de la directora Daniela Castro en co dirección de Nicolás Ordóñez.

<iframe src="https://www.youtube.com/embed/SyFBC6YS-cs" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

SINOPSIS

Su identidad se edifica sobre nombres olvidados. Yineth es el séptimo. Yineth es una niña campesina, una joven guerrillera, una madre de clase media, una ejecutiva del gobierno. Este documental es un retrato íntimo sobre una mujer que a través de la reinvención encuentra la manera para sobrevivir a un país que constantemente le dio la espalda. La película forma parte del proyecto transmedia "Alias" junto con la película "Alias María", seleccionada en el Festival de Cannes, Un Certain Regard.
