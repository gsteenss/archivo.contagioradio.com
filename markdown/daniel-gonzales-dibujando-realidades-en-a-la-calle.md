Title: Daniel Gonzáles dibujando realidades en "A la Calle"
Date: 2015-09-05 23:03
Category: Sonidos Urbanos
Slug: daniel-gonzales-dibujando-realidades-en-a-la-calle
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/IMG-20150701-WA0004-e1441512177832.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<iframe src="http://www.ivoox.com/player_ek_7763470_2_1.html?data=mJyjlZmbdI6ZmKial56Jd6KnmJKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRiMLiysrZjazTstuZpJiSo5bQqdSZk6iYw9fYrdTowpDOz8bYqdbmjMrbjYqWdqKfzcaYpcbQsMaZk5eah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Daniel Gonzáles, artista amateur en "A la Calle"] 

\[caption id="attachment\_13442" align="aligncenter" width="2048"\][![Daniel González](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/IMG_20150630_143201.jpg){.wp-image-13442 .size-full width="2048" height="1536"}](https://archivo.contagioradio.com/daniel-gonzales-dibujando-realidades-en-a-la-calle/img_20150630_143201/) Daniel Gonzáles en "A la Calle"\[/caption\]

[Daniel Gonzáles es un artista amateur bogotano, que busca rescatar el valor de la vida, la estética y plasmar lo bello en su trabajo gráfico. Toma objetos, sujetos y sucesos que hacen parte de la construcción social como referente a la hora de dibujar. Su sencillez y gratitud rescatan los valores que también juegan un valor importante en su rol como artista.]

facebook.com//TrrS.Dani
