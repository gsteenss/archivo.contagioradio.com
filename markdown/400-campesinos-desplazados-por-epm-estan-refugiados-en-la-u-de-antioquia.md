Title: 400 campesinos desplazados por EPM están refugiados en la U de Antioquia
Date: 2016-03-15 12:50
Category: Movilización, Nacional
Tags: EPM, Hidroituango, Universidad de Antioquia, Victor Correa Vélez
Slug: 400-campesinos-desplazados-por-epm-estan-refugiados-en-la-u-de-antioquia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/03/Campesinos-U-de-A.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Victor Correa ] 

<iframe src="http://co.ivoox.com/es/player_ek_10813640_2_1.html?data=kpWlk5iaeJGhhpywj5aUaZS1kpiah5yncZOhhpywj5WRaZi3jpWah5yncZWkkZDQw9LUqdTdz9TgjcnJt9Hgwt%2FOxtTXb9Hj05CysrKPqdTohqigh6aVsozmxsviyc7FqNDnjMrbj4qbh4630NPhw8zNs4zGwsnW0ZCRaZi3jpk%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Víctor Correa, Representante a la Cámara] 

##### [15 Mar 2016] 

Desde el pasado lunes, 400 campesinos afectados por la construcción de Hidroituango se desplazaron hacía la Universidad de Antioquia, para exigirle a la Gobernación de este departamento **cumplir con las garantías de retorno a los territorios, con las que se había comprometido** en el marco de las negociaciones con la Cumbre Agraria y frente a las que las comunidades no han visto ningún avance.

Hace dos años ya habían hecho presencia en esta misma universidad 600 campesinos que buscaban presionar la reparación de los derechos que las Empresas Públicas de Medellín vulneraron con la construcción de Hidroituango, que incluyó la **arremetida violenta contra las comunidades y sus procesos organizativos**. Sin embargo, pese a los compromisos, no se evidenciaron soluciones de fondo, por lo que se vieron obligados a movilizarse de nuevo.

De acuerdo con el Representante Víctor Correa, durante esta movilización los campesinos han sido agredidos por el ICBF que se ha hecho presente en la Universidad de Antioquia para decirles que pueden quitare la custodia de sus hijos por no tenerlos en condiciones adecuadas, **condiciones que se han agudizado por cuenta de la demanda que interpusieron contra EPM** por el vertimento de residuos al río y que logró que autoridades ambientales sancionaran a la compañía.

La reciente presencia de los campesinos en la U de Antioquia permitió que se estableciera una mesa de concertación entre el Gobierno y las comunidades, de la que se espera resulten propuestas de retorno digno a los territorios, en los que **hay presencia de actores armados y persiste la estigmatización contra las organizaciones** que apoyan la resistencia de estas familias campesinas.

##### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)]
