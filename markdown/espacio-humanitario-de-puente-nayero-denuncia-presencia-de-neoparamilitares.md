Title: Espacio Humanitario Puente Nayero denuncia presencia de neoparamilitares
Date: 2019-01-30 12:27
Author: AdminContagio
Category: DDHH, Nacional
Tags: buenaventura, Espacio Humanitario Puente Nayero, neoparamilitares
Slug: espacio-humanitario-de-puente-nayero-denuncia-presencia-de-neoparamilitares
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/01/ESPACIO-HUMANITARIO-770x400.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [30 Ene 2019] 

La Comisión de Justicia y Paz denunció la presencia de neoparamilitares, en el espacio Humanitario Puente Nayero, en Buenaventura, el 26 de enero. Los hombres ingresaron a la casa de Norbey Gutiérrez, integrante de la comunidad, **lo golpearon en la cabeza y le robaron un computador portátil y 5 millones de pesos**, horas más tarde los hombres ingresaron a la casa de otra familia y la intimidaron.

De acuerdo con la denuncia, dos días antes de que se presentara la agresión al señor Gutiérrez, otras tres personas encapuchadas, pertenecientes a las estructuras neoparamilitares, ingresaron al Espacio Humanitario y se ubicaron en el lote donde se construye la casa cultural comunitaria, permaneciendo allí por más de quince minutos, observando el movimiento en la tienda del señor Norbey.

Cabe resaltar que el pasado 22 de enero la estructura neoparamilitar de los “Urabeños” amenazó a los trabajadores del proyecto de alcantarillado y pavimentación que trabajan en obras de **infraestructura para el Espacio Humanitario**, interrumpiendo la obra, hecho que afectó a más de 500 habitantes con la retención de la maquinaria y la exigencia de \$20.000.000 para autorizar la continuidad de la obra.

En la denuncia también se establece que se pudo identificar, entre los integrantes de la estructura paramilitar llamada "Los Urabeños", a alias "Cabeza" y alias "Jorlein", haciendo presencia en el espacio Humanitario. (Le puede interesar:["Panfletos de Águilas Negras aparecen en diferentes regiones del país"](https://archivo.contagioradio.com/por-que-estan-aumentando-los-panfletos-de-amenazas-de-las-aguilas-negras/))

La organización defensora de derechos humanos expresó que los habitantes del Espacio Humanitario se encuentran intimidados debido a estas operaciones extorsivas y de amenaza de los llamados “Urabeños” dentro de su lugar humanitario y afirmó que **"dichas operaciones criminales se realizan en medio del control perimetral que hacen efectivos del batallón fluvial sobre el mar y unidades policiales en el acceso por la calle"**.

###### Reciba toda la información de Contagio Radio en [[su correo]
