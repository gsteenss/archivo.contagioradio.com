Title: Los Amorúa, el pueblo indígena que sobrevive entre las basuras
Date: 2019-07-24 18:05
Author: CtgAdm
Category: Comunidad, Nacional
Tags: Amorúa, Derechos Humanos, indígenas
Slug: pueblo-amorua-el-pueblo-indigena-que-sobrevive-entre-las-basuras
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/EAFsFCTW4AAImps.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Defensoría del Pueblo] 

El Pueblo Amorúa, una comunidad semi nómada conformado por 178 personas aproximadamente, que históricamente se ha movido entre las fronteras de Venezuela y Colombia en Casanare y Vichada, hoy enfrenta una grave situación humanitaria, pues han tenido que cambiar sus actividades agrícolas por las tareas de reciclaje en el relleno sanitario de Puerto Carreño.

Este martes 23 de julio la Defensoría del Pueblo de Vichada lanzó una denuncia pública por las condiciones de esta comunidad, pues no pueden desarrollar sus actividades de siembra de pan coger. Además, la atención humanitaria se ha dificultado por el alto flujo de población venezolana que llega a la región.

Aproximadamente 30 indígenas son quienes, a diario, rondan el relleno, entre ellos niños, niñas y mujeres, que buscan algún material útil o valioso para reciclar. Por tanto, están expuestos a las enfermedades típicas dado el ambiente en el que se encuentran, como problemas respiratorios, infecciones y problemas de piel.

Aura Upegui, funcionaria de la Defensoría del Pueblo de Vichada expresó, la grave situación que enfrentan las personas de esta comunidad, en especial los niños y niñas, que además de habitar transitoriamente en el relleno sanitario, se enfrenta a vulnerabilidad de mendicidad, consumo de sustancias psicoactivas y explotación sexual infantil entre otras.

**Degradación de los pueblos indígenas por falta de garantías**

Otra de las preocupaciones de esta situación es la degradación y posible desaparición que enfrentan esta comunidad, para Upegui, con toda esta crisis hay una desintegración del pueblo Amorúa, pues, no hay cohesión social y cultural entre las generaciones de la comunidad dadas las dinámicas de sobrevivencia que enfrentan en el municipio.

Según la Organización Nacional Indígena de Colombia (ONIC), los pueblos en riesgo de extinción son 64, es decir, el 62% de los pueblos indígenas en Colombia. Por tanto, las condiciones de estas comunidades a nivel nacional sufren un exterminio, ya que, no tienen garantías estatales para su conservación, como la han denunciado en distintas ocasiones las diferentes organizaciones indígenas. (Le puede interesar: [Cerca de 600 indígenas y campesinos fueron desplazados en Córdoba: ONU)](https://archivo.contagioradio.com/desplazan-600-indigenas-y-campesinos-en-tierraalta-onu/)

**Puerto Carreño y el flujo masivo de venezolanos**

Según Upegui esta problemática no es nueva, desde el 2016 los Amorúa enfrentan graves condiciones como comunidad indígena, sin embargo, en esta ocasión se ha visto agudizada dada la migración venezolana que atiende el municipio, “Puerto Carreño ha sido uno de los territorios más afectados por la crisis en Venezuela, en especial porque el municipio no cuenta con las capacidades para suplir las necesidades de todos, y hay un gran flujo de habitantes del país vecino” aseguró la funcionaria.

Upegui mencionó que las personas del país vecino, que pueden ser o no de comunidades indígenas, llegan a Puerto Carreño, pero solo tienen dos ‘salidas’, tratar de sobrevivir en el municipio o retornar a su país, pues las condiciones de las vías no permiten una salida fácil de la zona.

**Las Garantías del Gobierno Nacional con la comunidad indígena Amorúa**

En este sentido, la agudización de la problemática con esta comunidad indígena obedece, además, a las faltas de garantías por parte del Gobierno Nacional para enfrentar la crisis, pues, a nivel local la administración no da abasto, y ha tomado las medidas que su capacidad le ha permitido.

Por tanto, las exigencias que realiza la Defensoría del Pueblo, es que el Gobierno Nacional atienda esta situación de manera integral, es decir, que realice un plan que abarque la cobertura de territorio, salud, alimentación, educación entre otros dentro de los diferentes resguardos indígenas, para que esta población pueda retornar a su comunidad y actividades tradicionales.

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>  
<iframe id="audio_38940026" frameborder="0" allowfullscreen scrolling="no" height="200" style="border:1px solid #EEE; box-sizing:border-box; width:100%;" src="https://co.ivoox.com/es/player_ej_38940026_4_1.html?c1=ff6600"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
