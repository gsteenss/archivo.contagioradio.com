Title: Proceso de Paz salió fortalecido de la crisis
Date: 2014-12-24 18:02
Author: CtgAdm
Category: DDHH, Paz
Tags: Cese al fuego, FARC, FFMM, paz
Slug: proceso-de-paz-salio-fortalecido-de-la-crisis-2
Status: published

###### Foto: FarcPaz 

Según Camilo González, director de INDEPAZ y del Centro de Memoria “hay que hablar ya del cese al fuego”. Hay una comisión que está encargada de **pactar un cese**, pero es un trabajo que no se da de la noche a la mañana.

Según González eso requiere un diseño y una arquitectura que no se define en un mes o dos meses, pero ese trabajo debe comenzar ya, agrega que **puede haber un acuerdo tácito en que las fuerzas armadas** de suspendan operaciones ofensivas, a lo que llama “**ceses unilaterales simultáneos**”.

González también señala que “**La doctrina militar tiene que cambiar**” para que el tema de los objetivos de alto valor se minimice con ello los riesgos para el proceso. Agrega que hay un plazo que es simbólico, que son las elecciones de octubre, indicando la irreversibilidad del proceso. “Octubre es el momento del voto por la paz”.

Por su parte Jorge Restrepo, director del **CERAC** afirma que las FARC deben comprometerse de manera oficial a acabar con las retenciones, reclutamiento de menores, desde la perspectiva del gobierno no son un gesto, pero que también **hay compromisos que pueden hacer las FFMM** y que **no necesariamente debe conocer toda la opinión pública**. “Estamos perfectamente listos para lograr esto. Esto es cuestión de meses ahora si” señala Restrepo.
