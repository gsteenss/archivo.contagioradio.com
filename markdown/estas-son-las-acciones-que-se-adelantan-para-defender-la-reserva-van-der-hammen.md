Title: Estas son las acciones que se adelantan para defender la Reserva Van der Hammen
Date: 2016-02-11 14:34
Category: Ambiente, Nacional
Tags: Alcaldía de Bogotá, Enrique Peñalosa, Reserva Thomas Van der Hammen
Slug: estas-son-las-acciones-que-se-adelantan-para-defender-la-reserva-van-der-hammen
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/02/Reserva-Thomas-Van-der-Hammen-e1455219288852.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Ambiente Bogotá 

###### <iframe src="http://www.ivoox.com/player_ek_10401556_2_1.html?data=kpWhkpaZeZehhpywj5WaaZS1lZeah5yncZOhhpywj5WRaZi3jpWah5yncabn1cbgjdjTsozgwtiYw8jHrdDixtiY09rJb9TZjMbRx9HFstXVz5Ddw9fFb8XZx8rbxsrWb82hhpywj6jTstXVyM7cjbfFqMrjjJKSmaiReA%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>[Fernando Gómez] 

###### [11 Feb 2016] 

En el marco de la defensa de la Reserva Thomas Van der Hammen, la Red Ambiental de la reserva, adelanta tres iniciativas para impedir que los planes de urbanización de la alcaldía de Enrique Peñalosa se desarrollen sobre los terrenos biodiversos del borde norte de la capital, que podría ser el segundo pulmón de la capital.

La primera de esas actividades se realizará **el próximo 17 de febrero, en la Universidad de los Andes de la mano del Foro Nacional Ambiental, con el profesor Manuel Rodríguez Becerra.** Allí se realizará un conversatorio al que se invitó al distrito, a la Corporación Autónoma Regional, CAR, y al Ministerio de Ambiente. El evento de entrada gratuita, es un espacio pedagógico e informativo en torno a la importancia de la Reserva Thomas Van der Hammen  y sus posibles amenazas.

Por otra parte, se realizó una petición al consejo directivo de la CAR, para que no se haga **“ni la más mínima sustracción de la reserva, porque Enrique Peñalosa, anunció que sustraerá más del 90% de la reserva asegurando que se trata de un terreno que no tiene ningún valor ambiental,** tratándose de potreros y lotes”, señala Fernando Gómez, integrante de la Red Ambiental de la Reserva, que contrasta las palabras del alcalde con los estudios de los científicos que han evidenciado la riqueza biodiversa que existe en la reserva y que demuestra la importancia ambiental que tienen esas 1.395 hectáreas.

Así mismo, se tiene planeado **realizar salidas de reconocimiento del territorio, con varias organizaciones ambientales.** De esta manera, se realizará un encuentro de experiencias con la Universidad UDCA, que también ha realizado estudios sobre la reserva.

Fernando Gómez, asegura que la única potestad sobre la reserva la tiene la CAR, que a su vez ha anunciado que deben cumplirse las normas 475 y 621 del 2000 ratificadas por el Consejo de Estado, además, existe un plan de manejo establecido por la CAR en el año 2014. Sin embargo, el **distrito insiste en urbanizar la zona, sin haber presentado algún tipo de estudio que demuestre que no habrá ningún tipo de daño ambiental si se llegan a construir las 80 mil viviendas que tiene planeadas.**

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
