Title: 14 revelaciones en la resolución de acusación contra Santiago Uribe Vélez
Date: 2017-10-19 15:25
Category: Judicial, Otra Mirada
Tags: 12 apostoles, Alvaro Uribe, Convivir, Paramilitarismo, Santiago uribe
Slug: las-revelaciones-de-la-resolucion-de-acusacion-contra-santiago-uribe-velez
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/10/cms-image-000060756-e1508382269311.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Confidencial Colombia ] 

###### [18 Oct 2017] 

En el marco del proceso por el cual se acusa al hermano del senador Álvaro Uribe Vélez por delitos como concierto para delinquir y homicidio agravado. La resolución de acusación evidencia, según los testimonios recogidos, los hechos por los cuales se aseguraría que Santiago Uribe fue la cabeza y el principal financiador del grupo paramilitar los '12 Apóstoles' que cometió cerca de 570 asesinatos en Antioquia.  [(Le puede interesar: Las víctimas de Santiago Uribe y los 12 Apóstoles)](https://archivo.contagioradio.com/santiago_uribe_homicidios_12_apostoles/)

**1-** En la década de los noventa Santiago Uribe Vélez, conformó y dirigió desde la hacienda 'La Carolina' en Yarumal, Antioquia, el grupo 'Los 12 Apóstoles', para ejecutar una política de exterminio, contra "indeseables sociales y auxiliadores de grupos subversivos", con la ayuda por acción y omisión de la Policía Nacional e integrantes de la inteligencia militar. Asimismo, del grupo hacía parte el Padre Gonzalo Palacio.

**2-** El grupo de los Los '12 Apóstoles' estaba integrado, entre otros, por Leo Pemberthy, hermano del inspector de Policía; además de que dicha banda contaba con la participación de algunos miembros de la policía en Yarumal y Campamento. Entre ellos, William Ocampo Zapata y alias 'El Ruso'. Los integrantes de la fuerza pública que trabajaron en conjunto con los 'Los 12  Apóstoles'. Dichos agentes habrían cometido directamente delitos con sus propias armas de dotación y se ocultarían con pasamontañas.

**3-** El Teniente Juan Carlos Meneses señala en su declaración que quien le entregó el cargo fue el Capitán Pedro Manuel Benavides, quien el informó que en la zona había presencia de las FARC y el ELN, y por tal razón **la policía recibía apoyo de un grupo de personas financiados por Santiago Uribe**, que operaba en los Valles del Cuiva, en la finca 'La Carolina'. De hecho, señalaba que los altos mandos policiales tenían conocimiento de esa situación teniendo en cuenta que se trata del hermano del senador Álvaro Uribe, quien en este momento era candidato a la gobernación de Antioquia.

**4-** Santiago Uribe le dijo a Meneses que aunque la policía no apoyara las acciones de su grupo, este seguiría con las acciones de los '12 Apóstoles'. Sin embargo, si el Teniente colaboraba, recibiría una determinada cantidad de dinero mensual.

**5-** En una **habitación contigua a la Estación de policía se guardaban prendas militares para realizar las acciones contra civiles.** Una afirmación que se confirmó en el marco de un allanamiento realizado por parte del Departamento Administrativo de Seguridad.

**6-** Los 12 Apóstoles tenían una 'lista negra' de con al menos 25 nombres de personas a asesinar, en cuyo documento se destacaba el nombre de Camilo Barrientos, quien luego fue asesinado entre los municipios de Campamento y Yarumal. Santiago Uribe fue el que le dijo al Teniente Meneses que Camilo Barrientos sería asesinado, y que por eso se necesitaba un repliegue de la policía.

**7-** De acuerdo con la declaración de Hernán de Jesús Betancourt, agente de la Estación de Policía de Yarumal, **en la Hacienda 'La Carolina' había permanente control paramilitar**, quienes realizaban retenes como si se tratara de la Fuerza Pública.

**8-** Pablo Hernán Sierra, desmovilizado de las AUC, asegura que escuchó que **'La Carolina' se usaba como centro de entrenamiento paramilitar**. Además afirmó que Santigo Uribe es uno de los fundadores del Bloque Metro de las AUC y que durante el periodo del gobernador Álvaro Uribe, se le reconoció personería jurídica a las Convivir, las cuales mutaron hacia las autodefensas.

**9-** Según las declaraciones de Eunicio Alfonso, trabajador de la finca 'El Buen Suceso', que colindaba con 'La Carolina'. Allí pudo constatar que e**ra Santiago Uribe quien daba las órdenes sobre las ejecuciones de homicidios**. Por dichos crímenes se pagaba \$200.000 por cada uno.

**10-** El trabajador de la finca de Álvaro Vásquez, Eunicio, asegura que escuchó cómo su patrón y Santiago Uribe, planeaban asesinarlo ya que este no participaba de las actividades ilícitas y en cambio tenía conocimiento de ellas.

**11-** Las Autodefensas se encontraban ubicadas en la finca 'La Carolina'. **Para ese momento el Gobernador de Antioquia, era Álvaro Uribe Vélez, quien aparecía como propietario de la finca**, mientras que quien la administraba era su hermano  Santiago Uribe.

**12-** Según testimonios, Santiago quedaba pendiente de las operaciones con un radio. Este mismo siempre estaba con una ametralladora Ingran dentro del carro. Los testigos aseguran que era el jefe porque era el que coordinaba y todos lo llamaban 'patrón'.

**13-** Según Meneses, Santiago Uribe le pagaba por sus servicios ilegales, entre un millón doscientos mil pesos y un millón quinientos mil pesos. En dos oportunidades, dicho dinero fue entregado en la Hacienda 'La Carolina'.

**14-** El **mayordomo de la finca 'La Carolina', Carlos Serna ha declarado que además de las prácticas militares que se realizaban en la hacienda**, fue testigo del asesinato de un hombre de apellido Várelas dentro del mismo predio. El cuerpo del señor además fue sacado de la finca en un vehículo de la Policía Nacional, y luego, según el periodista Sergio Alejandro fue paseado por todo el pueblo.

Por el momento, la Sala Penal de la Corte Suprema de Justicia ha negado la tutela con la que la defensa de Santiago Uribe Vélez, el abogado Jaime Granados buscaba que el hermano del expresidente quedara libre. La Sala desvirtuó que el Juzgado Primero Penal del Circuito Especializado de **Antioquia** haya vulnerado los **derechos fundamentales a Santiago Uribe,** al prorrogar de oficio la medida de aseguramiento en su contra.

###### Reciba toda la información de Contagio Radio en [[su correo]
