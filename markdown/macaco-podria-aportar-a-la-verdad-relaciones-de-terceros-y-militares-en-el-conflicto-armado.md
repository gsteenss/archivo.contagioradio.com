Title: "Macaco" podría aportar a la verdad sobre relaciones de terceros y militares en el conflicto armado
Date: 2019-07-25 16:00
Author: CtgAdm
Category: DDHH, Judicial
Tags: Carlos Mario Jiménez, JEP, Macaco, paramilitares, víctimas
Slug: macaco-podria-aportar-a-la-verdad-relaciones-de-terceros-y-militares-en-el-conflicto-armado
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/02/bananeras-e1486074005794.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Empresas Bananeras - Paramilitarismo - RT] 

 

Carlos Mario Jiménez, conocido bajo el alias de "Macaco", líder  del Bloque Central Bolívar, fue extraditado el pasado 21 de julio a Colombia en donde sería juzgado por la Justicia Ordinaria . Sin embargo, con la puesta en marcha de la Jurisdicción Especial para la Paz, existe la posibilidad de que Jiménez solicite ingresar a este Tribunal , hecho que implicaría que "Macaco" hable sobre el accionar del Paramilitarismo y sus relaciones con terceros o la Fuerza Pública en el marco del conflicto armado. (Le puede interesar: [La verdad de Mancuso que podría llegar a la JEP)](https://archivo.contagioradio.com/la-verdad-de-mancuso-que-podria-llegar-a-la-jep/)

### El Bloque Central Bolívar y sus relaciones con la Fuerza Pública 

El Bloque Central Bolívar (BCB) fue una de las estructuras más importantes del paramilitarismo, comandada durante 20 años por Carlos Mario Jiménez, que operó en ocho departamentos y dejó un saldo de más de 14 mil víctimas. Entre los crímenes que cometieron se encuentran la perpetración de masacres, reclutamiento armado, desapariciones y desplazamientos forzados.

Se estima que este Bloque llegó a tener 9 frentes y en total estaría conformado por 7.600 personas, que se desmovilizaron bajo la Ley de Justicia y Paz. Además de "Macaco," alias  "Ernesto Baez" era el encargado de la dirección política, mientras que alias "Julián Bolívar", orientaba militarmente a las personas.

Carlos Mario Jiménez habría llegado a Colombia para responder por más de 100 hechos cometidos  bajo su mando en diferentes zonas del país. No obstante, en diferentes audiencias ha manifestado que algunas de las acciones violentas cometidas en contra de la población civil, el BCB recibió ayuda de altos militares, como sería el caso del general Santoyo, quien actualmente se encuentra compareciendo en la JEP.

De igual forma, alias "Julián Bolívar" también ha señalado que este Bloque desaparecía personas por presiones de policías y algunos militares, razón por la cuál habrían optado por abrir los cuerpos de sus víctimas y llenarlas de piedras para lanzarlas a los ríos y que se hundieran.

### **Empresas y El Bloque Central Bolívar liderado por "Macaco"** 

Una de las mayores exigencias de las víctimas del Bloque Central Bolívar, ha sido conocer las causas detrás de los grandes desplazamientos forzados, los asesinatos y las desapariciones, que para ellos, en algunos de los casos, tuvo como principal motor los intereses empresariales.

### **Santander y Nt. Santander **

Sobre esas relaciones, integrantes de ese Bloque también han hecho afirmaciones de las que posiblemente alias "Macaco" tendría conocimiento, al ser el líder de esa estructura paramilitar. Una de esas aseveraciones, fue la que realizó "Julián Bolívar", al manifestar que una red de funcionarios de Ecopetrol se habría unido al BCB para realizar chuzadas y hurtar combustible.

De igual forma, investigadores de la Fiscalía estarían rastreando información en la que terceros estarían implicados en el homicidio de sindicalistas que estuvieron en una lista del BCB, en la región del Catatumbo. La mayoría de las personas hacía parte de la Unión Sindical Obrera (USO), el Sindicato de Trabajadores del Transporte en Santander (Sincotrainder) y de Sintrapalma en la región de Puerto Wilches.

### Nariño

Otra de las zonas en donde el Bloque Central Bolívar estuvo fue el departamento de Nariño. Allí dos grupos pertenecientes a esta estructura se encargaron de controlar el territorio. Por su parte, las víctimas han señalado que ese grupo armado no solo recibió el apoyo de políticos y militares, que hasta ahora no han sido investigados, sino que también muchos empresarios de la zona apoyaron esa organización paramilitar.

Una de esos casos habría sido la Sociedad de Palmicultores y Ganaderos del Nariño (SAGAN), en donde según investigaciones de la Fiscalía miembros de esta asociación financiaron con dineros al BCB, por este hecho hasta el momento se encuentra en etapa de investigación el señor Salvador Escobar.

### **"Macaco" y la verdad** 

Será Carlos Mario Jiménez el que deba hacer la postulación de su caso a la Jurisdicción Especial para la Paz, y luego este Tribunal decidirá si admite o no el proceso, dependiendo de que tanto alias "Macaco" aporte a la verdad de los hechos que cometieron las estructuras paramilitares en Colombia. (Le puede interesar:[Víctimas piden desmantelar el paramilitarismo como garantía de la JEP)](https://archivo.contagioradio.com/dudas-y-cuestionamientos-sobre-jurisdiccion-especial-para-la-paz/)

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
