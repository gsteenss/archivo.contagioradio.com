Title: Poligrow, Expo Milán y elecciones en Mapiripán
Date: 2015-10-28 10:32
Category: Abilio, Opinion
Tags: Carlo Vigna Tiaglanti, Conrado Salazar, elecciones en Mapiripan, feria italiana Expomilan, mapiripan, Meta, Poligrow empresa de palma, postconflicto en Colombia
Slug: poligrow-expo-milan-y-elecciones-en-mapiripan
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/10/poligrow1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: IPSE] 

**Por [Abilio Peña](https://archivo.contagioradio.com/abilio-pena-buendia/) - [~~@~~Abiliopena](https://twitter.com/Abiliopena){.twitter-atreply .pretty-link}**

###### [28 Oct 2015]

En la ubicación de los personajes de esta fotografía se puede interpretar la configuración del poder real municipios como Mapiripán, Meta. En el centro se encuentra el italiano representante de Poligrow en Colombia Carlo Vigna Taglianti, a su derecha el ministro de Minas y Energías del momento, a su lado el gobernador del departamento y relegado en una esquina de la mesa, el joven alcalde Jorge Iván Luque Lenis.

En las elecciones para gobernadores, alcaldes, concejales y ediles celebradas el pasado 25 de octubre de 2015, ese poder volvió a evidenciarse. Según [graves denuncias de uno de los candidatos perdedores, Conrado Salazar](https://archivo.contagioradio.com/incertidumbre-y-violencia-despues-del-fraude-electoral-en-mapiripan/), Poligrow se confabuló con el alcalde para tomarse el control del manejo administrativo del municipio hasta el extremo de que un subgerente de la transnacional llegó a dar la orden de que ningún vehículo al servicio de la empresa podía usarse para la campaña de Salazar, bajo la amenaza de ser expulsado si lo hiciera. Efectivamente, constató, que ninguno pudo ser contratado. El día de las elecciones, afirma Salazar, el alcalde en ejercicio se tomó la escuela donde se adelantaban las elecciones, participó en el conteo de votos y proclamó la victoria de su candidato desde antes del cierre de los comicios generando la reacción airada de sus contrincantes quienes protestaron, recibiendo como reacción gases lacrimógenos por parte de la policía lo que provocó, como airada reacción, el incendio de la alcaldía, la registraduría y un juzgado. Sostiene, además, que en las urnas donde sufragaron miembros de las comunidades indígenas no aparecieron sus votos.

El poder de la compañía transnacional Poligrow, trasciende el ámbito local y se eleva a los nacionales e internacional a juzgar por los eventos en los que interactúan con los mas altos dignatarios del poder político y económico de Colombia: su presidente, su vicepresidente, el viceministro de agricultura y los miembros de la dirección del gremio palmicultor en el país.

Cuando el presidente Santos viajó a Italia, visitó el pabellón de Colombia en la feria Expomilán. Dentro de la delegación del país se encontraba el gerente de Poligrow Carlo Vigna Tiaglanti quien habló fuerte sobre las ventajas comparativas que para el mercado tiene Colombia y por su parte el presidente Santos tal como lo mostrábamos en una [columna anterior](https://archivo.contagioradio.com/las-cooperativas-agricolas-italianas-del-postconflicto-colombiano/.), pidió que las granjas agrícolas italianas se convirtieran en modelos para el “postconflicto”.

En este mes de octubre, en la misma[feria italiana Expomilan](http://www.colombiaexpomilan.co/press-room/pictures/seminar-on-investment-opportunities-in-agroindustry), participó el vicepresidente de Colombia Germán Vargas Lleras y estuvo de nuevo Carlos Vigna Taglianti con el poderoso magnate de los negocios verdes Agostino Re Rebaudengo accionista de la compañía Poligrow, presidente Saja Ambiente Italia, Reba Capital y presidente de Aso Renovable en Italia, entre otras.. El vicepresidente[expuso los avances del actual gobierno](http://www.vicepresidencia.gov.co/prensa/2015/Paginas/En-ExpoMilan-Vicepresidente-Vargas-Lleras-invita-a-empresarios-europeos-a-invertir-en-el-sector-de-infraestructura-151019.aspx)y de su gestión en la adecuación de la infraestructura vial y les abrió las puertas para que inviertan en este sector y en otros de la economía nacional.

Así mismo el viceministro de agricultura de Colombia Hernán Román Calderón ha sostenido encuentros en Torino con Re Rebaudengo, Vigna y otros empresarios. Re Rebaudengo en la presentación dentro del encuentro habló a nombre de Poligrow y Asja sobre la “[oportunidad de inversión](http://www.asja.biz/noticia.php?id=381)” en Colombia

También la propia dirigencia de [Fedepalma llegó hasta las plantaciones de Poligrow](http://web.fedepalma.org/visita-mapiripan) en Mapiripán en el mes de mayo pasado a mostrar, contra toda evidencia, a la compañía como ejemplo de “desarrollo sostenible, inclusión social y protección ambiental”. Carlo Vigna aprovechó ese respaldo para sacar pecho y afirmar que “no desconoce los obstáculos para su proyecto”, dentro de los que ubica la “inseguridad jurídica con respecto a la adjudicación y titulación de tierras” lo que según él “ ha generado la estigmatización del sector agroindustrial, de nuestro grupo empresarial y finalmente del proyecto”.

Las denuncias que en su momento hiciera el representante a la Cámara Wilson Arias, del propio Incoder bajo la gerencia de Miriam Villegas, los reportes de la Contraloría, los procesos que se siguen en la fiscalía contra Poligrow, las investigaciones periodísticas de la La Silla Vacia, Las Dos Orillas, Verdad Abierta, Indepaz - Somo, los informes de la Comisión Intereclesial de Justicia y Paz entre otros, parecen no inquietar a los altos dirigentes, con reacciones que al menos conozcamos ante denuncias que han llegado hasta lo penal en contra de la compañía.

Los obstáculos en el camino de Poligrow tienen que ver justo con irregularidades en la adquisición de las tierras además de otras concomitantes de graves implicaciones legales. Esperamos que el poder económico de la transnacional, al menos en este caso, no aplaste la posibilidad de que se haga justicia a las víctimas de los despojos en Mapiripán, quienes según el presidente Santos son el centro de su gobierno y del proceso de paz que se adelanta en la Habana.
