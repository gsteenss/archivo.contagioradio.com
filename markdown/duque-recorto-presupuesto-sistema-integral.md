Title: Duque recortó 30% de presupuesto al Sistema Integral de Verdad Justicia Reparación y No Repetición
Date: 2019-07-08 16:46
Author: CtgAdm
Category: Paz, Política
Tags: comision de la verdad, JEP, Presupuesto, Unidad de Busqueda
Slug: duque-recorto-presupuesto-sistema-integral
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/Fotos-editadas1.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio  
] 

Después de un encuentro que desarrollaron los y las directoras de la Jurisdicción Especial de Paz (JEP), de la Comisión para el Esclarecimiento de la Verdad (CEV) y la Unidad de Búsqueda de Personas Dadas por Desaparecidas (UBPD) con la comunidad internacional, entre ellas, 14 embajadas y 6 organismos internacionales, se dio a conocer que **de los 90 mil millones de pesos que se necesitan para que funcione la JEP en 2020, el gobierno solo aprobó 67 mil millones**.

En el caso de la Comisión la situación es mucho más grave puesto que eso **podría significar el cierre de las Casas de Paz que se han instalado en diversos territorios**, a pesar del recorte que les impidió contratar el personal necesario. Hasta el momento, las casas funcionan con cerca de cuatro personas, y con el recorte podrían desaparecer dado que no habría suficiencia para sostenerlas. (Le puede interesar: ["Con diálogos para la no repetición, Comisión de la Verdad promoverá defensa de líderes sociales"](https://archivo.contagioradio.com/con-dialogos-para-la-no-repeticion-comision-de-la-verdad-promovera-defensa-de-lideres-sociales/))

Para la Unidad de Búsqueda la situación no es menos peligrosa, dado que **la búsqueda de personas desaparecidas exige un despliegue territorial importante que se vería seriamente afectado** por un recorte tan drástico como el anunciado. (Le puede interesar:["En 2019 Unidad de B´suqueda de Desapareciso llegará a 17 territorios"](https://archivo.contagioradio.com/unidad-de-busqueda-de-desaparecidos/))

Según expresó **Patricia Linares, directora de la JEP,** aunque los organismos son conscientes de la situación del país, se necesitan los recursos necesarios para funcionar de manera eficaz, dado que el Sistema Integral de Verdad, Justicia, Reparación y no Repetición (SIVJRNR) es temporal, de carácter transicional, es decir con un tiempo muy reducido de duración y **un recorte pone en riesgo las metas establecidas.**

Según **Luz Marina Monzón, directora de la UBPD,** este ajuste presupuestal demuestra que para el gobierno Duque la paz, por la vía del cumplimiento de los requerimientos del Sistema Integral, no es la prioridad. Sin embargo, estas preocupaciones expresadas a la comunidad internacional dejaron ver que ellos y ellas están dispuestos a apoyar los mecanismos, y a insistirle al gobierno en la importancia de su sostenibilidad, como lo afirmo el Padre Francisco de Roux, director de la CEV.

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
