Title: Poligamia celebra 20 años de “Mi generación”
Date: 2015-12-03 09:00
Category: Cultura
Tags: Andrés Cepeda, César López, Mi generación, Poligamia, Rock Colombiano, Rock noventas
Slug: poligamia-celebra-20-anos-de-mi-generacion
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/11/Cepeda.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto:Lanuevavozlatina.com 

###### 17 Nov 2015

**Poligamia una de las bandas representativas del Rock nacional de la generación de los 90\`s** se reencontrará **este 4 de diciembre a las 8:00 pm en el Teatro Julio Mario Santo Domingo de Bogotá**, con el fin de celebrar los 20 años de “Mi generación”, su canción más conocida.

**Andrés Cepeda**, Juan Gabriel Turbay, Freddy Camelo, Gustavo Gordillo, Virgilio Guevara y **César López**, **cantarán temas como** \`Bailando sobre tu piel\`, \`Búscame\`, \`Desvanecer\`, \`Confusión\`, \`Luna llena\`, \`Mi generación\`, entre otras, de **sus álbumes \`Una Canción\`(1993); \`Vueltas y Vueltas\` (1995); \`Promotal 500mg\` (1996) y \`Buenas noches-Muchas gracias\`(1998)**.

Poligamia **fue fundada en 1990, pero solo hasta 1993 después de ganar un concurso denominado "Batalla local de las bandas"** con el tema \`Bailando sobre tu piel\`, tomaron fama y **grabaron su primer disco \`Una Canción\`** bajo la producción de Sony Music. Con su álbum \`Promotal 500mg\` de 1996 el grupo no tuvo mayor acogida y en 1998 después de un último disco sus integrantes deciden tomar rumbos independientes en la música y la producción.

### **Información sobre el evento:** 

**Lugar: **Teatro Julio Mario Santo Domingo

**Fecha:** viernes 4 de diciembre

**Horario:** 8:00 p.m.

**Precio:** \$35.000 hasta \$350.000

**Dónde comprar: **en el sitio oficial de Primera Fila
