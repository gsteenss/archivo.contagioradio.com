Title: Consulta minera busca frenar proyecto La Colosa en Cajamarca, Tolima
Date: 2016-08-31 15:26
Category: Ambiente, Nacional
Tags: Ambiente, Cajamarca, extractivismo, Mineria, Tolima
Slug: consulta-minera-busca-frenar-proyecto-la-colosa-en-cajamarca-tolima
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/10/Megamineria.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Comité Ambiental 

###### [31 Ago 2016] 

En el departamento del Tolima, **la comunidad está dispuesta a impedir el inicio de actividades mineras por parte de la multinacional Anglo Gold Ashanti.** Esta vez, a la ciudad de Ibagué se une el municipio de Cajamarca, donde con ocho votos a favor y tres en contra, el Concejo dio vía libre para que los ciudadanos decidan por medio de una consulta popular si quieren o no que se realice actividades mineras en su territorio.

“¿Está usted de acuerdo, sí o no, con que en el municipio de Cajamarca se ejecuten actividades que impliquen contaminación del suelo, pérdida o contaminación de las aguas y afectación de la vocación agropecuaria con motivo de proyectos de naturaleza minera?”, es la pregunta que ahora deberá pasar a revisión constitucional **por parte del Tribunal Administrativo del Tolima,** para que luego (de ser aprobada) el alcalde municipal decida la fecha en la que se realizaría la consulta.

Esta no es la primera vez que se intenta llevar a cabo una consulta minera en Cajamarca. De acuerdo con Camila Méndez, integrante del Comité Ambiental y Campesino de Cajamarca y Anime, el año pasado en febrero la iniciativa que nació de la comunidad fue negada por el concejo del municipio. Sin embargo, en julio de 2015 con la Ley 1757, sobre mecanismos de participación ciudadana se pudo convocar nuevamente a una consulta popular gracias a la recolección de **casi 5 mil firmas aunque solo se necesitaba recoger 1600.**

“Lo que estamos haciendo es **defender el ordenamiento territorial municipal, no queremos que nos cambien la vocación agropecuaria** por** **la minería a cielo abierto”, expresa Méndez, quien añade que la economía local se mueve gracias a las actividades agrícolas y no a las mineras.

Durante el debate en el Concejo, el economista Álvaro Pardo demostró mediante la declaración de renta de algunas empresas mineras, que estas  no pagan  impuestos, **“Paga más impuestos una tienda de barrio que las grandes multinacionales**”, dice la integrante del Comité Ambiental y Campesino de Cajamarca.

Para que la población logre su cometido, se requerirá que **5.500 personas vayan a las urnas y de esta manera puedan detener el proyecto minero La Colosa,** con el que se planea extraer cerca de 29 millones de onzas de oro para el año 2020.

<iframe src="https://co.ivoox.com/es/player_ej_12732342_2_1.html?data=kpeklZeXeJOhhpywj5aVaZS1kp6ah5yncZOhhpywj5WRaZi3jpWah5yncaTVzs7Zw5Cxqc_Yxt-SlKiPh9DhytmSpZiJhZqfotLPy8rSuMLgjN6YpcbRtMbnytPcjcnJb6TVy8baw9eRaZi3jqjc0NnFq8rjjLfOxs7Tb46ZmKialg..&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
