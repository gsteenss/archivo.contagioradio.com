Title: Mujeres Campesinas "rebeldes pero con causa"
Date: 2019-03-08 17:18
Author: ContagioRadio
Category: Mujer
Tags: derechos de las mujeres, Lideresas Sociales, zonas de reserva campesina
Slug: lmujeres-campesinas-rebeldes-pero-con-causa
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/03/grandmother-455977_960_720.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/03/Campesinas.png" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Pixabay] 

###### [08 Mar 2019]

En el marco de la conmemoración del **Día Internacional de la Mujer**, para este 8 de marzo, la **Coordinadora Nacional de Mujeres de las Zonas de Reserva Campesina,** organización que vincula a  las mujeres campesinas del país promueve la campaña **\#NosotrasExigimos** con la cual buscan que sean reconocidos los derechos de las mujeres colombianas que día a día trabajan por la construcción de paz en el campo, luchan por sus territorios y defienden sus derechos participativos.

**Alix Mendoza, delegada de la Coordinadora y de la Asociación Campesina de Inza**, Tierra Adentro (ACIT) indica que dentro de las exigencias que exponen conmemorando el 8 de marzo al Estado colombiano y a la sociedad en general, es que sean garantizados los derechos que las respaldan como mujeres campesinas y que no se nos han cumplido a lo largo de la historia.

De igual forma resaltó que exigen el respeto y el derecho a la vida de los líderes y las lideresas sociales  quienes llevan un trabajo de formación dentro de las comunidades que muchas veces el Estado no cubre.

\[caption id="attachment\_62882" align="alignnone" width="800"\]![Foto: Anzorc](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/03/Campesinas-800x446.png){.size-medium .wp-image-62882 width="800" height="446"} Foto: Anzorc\[/caption\]

### **El aporte de la mujer campesina al país**

La delegada explica que dentro de las diferentes organizaciones, a través de los procesos de base y creación de comités, las mujeres del campo colombiano han logrado llegar al diálogo sobre su realidad afectiva, social y económica, evidenciando cuáles son las problemáticas que existen en su contexto, **"pero hay una cosa clara, las mujeres campesinas somos unas luchadores, unas rebeldes pero con causa y aunque el Estado no nos escuche a través de la historia, siempre buscamos soluciones"**.

Maxi Mendoza afirma que el gran problema en Colombia es que no se reconoce ni se  ve a gran escala el aporte que "hacemos las mujeres campesinas, urbanas y académicas" y que solo a través de las sinergias entre mujeres y la democracia se pueden comenzar a impulsar estos reconocimientos para que se vean reflejados en la sociedad.

<iframe id="audio_33204135" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_33204135_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
