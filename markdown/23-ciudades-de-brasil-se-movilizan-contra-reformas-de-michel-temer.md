Title: 23 ciudades de Brasil se movilizan contra reformas de Michel Temer
Date: 2017-03-15 12:35
Category: El mundo, Movilización
Tags: Michel Temer, Reforma de Previdência
Slug: 23-ciudades-de-brasil-se-movilizan-contra-reformas-de-michel-temer
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/03/brasil-defato2.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Brasil de Fato] 

###### [15 Mar 2017] 

Por lo menos 23 movilizaciones se esperan hoy en las principales ciudades de Brasil en protesta por la reforma a la Ley de los Trabajadores y la reforma a la Ley de Seguridad Social, que en conjunto **aumenta los aportes de contribución fiscal a 49 años y la jubilación de los trabajadores a 65 años.**

La reforma de "Previdência" enviada por Temer al Congreso, establece que los aportes de la contribución fiscal deben tener un tiempo de 49 años, **lo que obliga a las personas a trabajar desde los 15 años para poder retirarse a los 65 cumpliendo con los aportes.** Además se elimina las jubilaciones especiales para sectores como el educativo y el agrícola. Le puede interesar: ["Reforma de Temer perjudica jubilación de trabajadores más pobres"](https://archivo.contagioradio.com/jubilacion-trabajadores-brasil/)

De acuerdo con Fania Rodríguez, directora de Brasil de Fato, estos recortes en derechos laborales no plantean soluciones de fondo a los problemas más graves del país como el desempleo “no hay un plan para disminuir la tasa de desempleo que **hoy suma 13 millones de personas, uno de los números más altos de desempleo en los últimos 10 años**”. Le puede interesar: ["Brasil alcanza cifra récord de desempleo con Temer"](https://archivo.contagioradio.com/desempleo-brasil-temer/)

Y agregó que estas reformas “**ayudan a una pequeña clase de empresarios, que comandan la economía del país** y que presionan al gobierno para sacar los derechos de la clase trabajadora”, de igual forma Rodríguez señaló que diferentes sindicatos están haciendo programas de concientización a los trabajadores en diferentes ciudades y organizando producción de información para explicar el retroceso que implican estas medidas.

La movilización ha sido convocada por los 6 sindicatos más representativos de Brasil y tanto gremios de docentes como organizaciones sociales expresaron que se unirán a la protesta. La hora cero **para el paro de 24 horas será sobre las 4:00pm y se espera que se registren marchas en 23 ciudades**.

<iframe id="audio_17567346" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_17567346_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
