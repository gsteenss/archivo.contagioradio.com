Title: Más de 700 personas se reunirán por la libertad de las y los prisioneros políticos en Colombia
Date: 2015-03-18 17:23
Author: CtgAdm
Category: DDHH, Nacional
Tags: carcel, encuentro, mariposas, paz, presos, prisioneros
Slug: mas-de-700-personas-se-reuniran-por-la-libertad-de-las-y-los-prisioneros-politicos-en-colombia
Status: published

###### Foto:  Crónica Quindío 

<iframe src="http://www.ivoox.com/player_ek_4232413_2_1.html?data=lZeglJmVd46ZmKiakpWJd6KkkZKSmaiRdI6ZmKiakpKJe6ShkZKSmaiRdsXjjKrbxdrJstXm0JC5w9fLpYzKysnOjcaPsMLnjLLO1M7Us9TV1JKSmaiRh9Di1cbUy9SPlsLYytSYj4qbh46o&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Jhon León, Equipo Organizador 2do Encuentro Larga Vida a las Mariposas] 

Al rededor de 700 personas participarán en el 2do **Encuentro Nacional e Internacional Larga Vida a las Mariposas**, del viernes 20 al domingo 22 de marzo en Bogotá. Según lo expuesto por sus organizadores, los principales objetivos del Encuentro son consolidar el **Movimiento Nacional Carcelario**, y construir una hoja de ruta colectiva que permita, en el marco de las conversaciones de paz que adelantan el gobierno de Juan Manuel Santos y las guerrillas de las FARC y el ELN, la libertad los 10 mil prisioneros y prisioneras políticas que existen en Colombia.

"**No es posible hablar de paz mientras en las cárceles colombianas hay hombres y mujeres que están siendo condenados por falta de garantías**", señaló Jhon León, del equipo coordinador del 2do Encuentro. "La libertad de las y los prisioneros es un paso hacia la paz".

Cabe recordar que el Alto Comisionado de las Naciones Unidas para los Derechos Humanos en Colombia se refirió en su informe anual, a la **falta de garantías que existe contra líderes sociales y comunales, que son juzgados por el Estado** sin que existan pruebas contundentes contra ellos y ellas.

<iframe src="http://www.ivoox.com/player_ek_4232782_2_1.html?data=lZeglJycdo6ZmKiakpWJd6KlmJKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRmNDYxZC10dzQpc%2FYhpewjbXWrdTd0NPS1NTXcYarpJKw0dPYpcjd0JC%2Fw8nNs4yhhpywj5k%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Todd Howland, Alto Comisionado de las Naciones Unidas] 

La metodología del Encuentro permitirá, según sus organizadores, que se traten temas de vital importancia para la población carcelaria y sus familiares, como las condiciones humanitarias al interior de los centros penitenciarios, la garantía del derecho a la salud, etc.

La participación en este escenario de los capítulos regionales de la Coalición Larga Vida a las Mariposas garantiza, además, que se construya un informe general de la situación carcelaria con base en detallados informes locales, así como propuestas por parte de los mismos reclusos y reclusas. **"Las cárceles han sido una constante violación de los derechos fundamentales, un estado de cosas inconstitucionales".** enfatiza León. "Desde ya se está preparando una jornada nacional de protesta carcelaria el próximo 20 de abril".

La participación internacional correrá por cuenta de organizaciones solidarias de Kurdistán, el País Vasco, Venezuela, Bélgica, Palestina y Brasil, entre otros.

El evento que además contará con las intervenciones de Piedad Córdoba, Iván Cepeda, Alirio Uribe, Ángela María Robledo, Gloria Inés Ramirez, el INPEC, y la Fiscalía General de la Nación, se llevará a cabo en el Centro de Memoria, Paz y Reconciliación. Las personas interesadas en participar deberán pre-inscribirse en la página web oficial del Encuentro, www.traspasalosmuros.net .
