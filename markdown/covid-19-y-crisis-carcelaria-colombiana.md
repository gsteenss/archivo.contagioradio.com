Title: Covid-19 y crisis carcelaria colombiana
Date: 2020-04-30 21:09
Author: AdminContagio
Category: Expreso Libertad, Nacional
Tags: carcel picota, Covid-19, crisis carcelaria
Slug: covid-19-y-crisis-carcelaria-colombiana
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/04/samyk.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/04/jhon.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/04/cárcel.jpeg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/04/fence-2163951_1920-1.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/04/carcel-2.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/04/93793276_149086079947364_5891560408304950746_n.mp4" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/04/carcel-celda-5.mp4" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/04/WhatsApp-Video-2020-04-27-at-9.55.05-AM.mp4" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/04/carcel-patio.mp4" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/04/94889260_2578537602474350_6768654272771915776_n.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/04/Captura-de-pantalla-2020-04-30-a-las-20.48.29.png" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/04/Captura-de-pantalla-2020-04-30-a-las-20.51.06.png" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

#### COVID-19 Y CRISIS CARCELARIA EN COLOMBIA

[  
![logo contagio radio web](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/elementor/thumbs/logo-contagio-radio-web-1-o62lxz6d07813cvllbcuyf815vxkqff0n31ghd2dc0.png "logo contagio radio web")](https://archivo.contagioradio.com/)

26 de abril de 2020

[Tweet](https://twitter.com/share?ref_src=twsrc%5Etfw)  
<iframe src="https://www.facebook.com/plugins/share_button.php?href=https%3A%2F%2Fwww.contagioradio.com%2Fcovid-19-y-crisis-carcelaria-colombiana%2F&amp;layout=button_count&amp;size=small&amp;appId=894195857389402&amp;width=99&amp;height=20" width="99" height="20" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>

<figure>
![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/04/samyk.jpg){width="1080" height="1080" sizes="(max-width: 1080px) 100vw, 1080px" srcset="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/04/samyk.jpg 1080w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/04/samyk-300x300.jpg 300w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/04/samyk-1024x1024.jpg 1024w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/04/samyk-150x150.jpg 150w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/04/samyk-768x768.jpg 768w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/04/samyk-370x370.jpg 370w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/04/samyk-512x512.jpg 512w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/04/samyk-310x310.jpg 310w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/04/samyk-230x230.jpg 230w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/04/samyk-360x360.jpg 360w"}

</figure>
###### Sandra Gutiérrez

Periodista/Contagio Radio

####   
La crisis del sistema carcelario ha sido denunciado durante años, sentencias han ordenado medidas urgentes para atender el problema, sin embargo, poco y nada se ha hecho. A esto se suma la pandemia del covid-19 que profundiza la vulneración de los derechos de los prisioneros en las cárceles colombianas.

0  
POSITIVOS  
0  
FALLECIDOS

Desde el 15 de marzo el movimiento carcelario y organizaciones de derechos humanos han exigido protocolos al gobierno del presidente Iván Duque para la prevención del Covid-19 en los centros de reclusión. Y pese a que se expidió un Decreto de Excarcelación y se anunciaron medidas, a la fecha las cárceles siguen sin tener medidas eficaces para atender la  pandemia.

Morir en una cárcel por Covid-19

Desde el 16 de marzo, los internos de la Cárcel La Picota anunciaron sus primeras protestas. En ese entonces los reclusos denunciaban que las medidas tomadas por el presidente Iván Duque no frenaba el contagio de Covid-19 en los centros de reclusión.

En ellas el presidente había ordenado la suspensión de visitas a la población reclusa. Sin embargo, la guardia del INPEC, junto a los sectores administrativos y contratistas, **seguían entrando y saliendo de las cárceles**.

La situación se recrudeció el pasado 21 de marzo, cuando el movimiento carcelario convocó a un gran cacerolazo nacional por la crisis humanitaria en los penales. Mientras el resto de la sociedad colombiana ya estaba en la cuarentena obligatoria.

De acuerdo con José Ángel Parra, prisionero político de FARC, en la cárcel la Picota, **el sentimiento de zozobra y angustia se había apoderado de los reclusos.** Un ambiente generalizado en los demás centros penitenciarios, porque hasta ese momento seguían sin llegar los tapabocas, guantes, jabones e incluso el agua a las cárceles.

<figure>
![Cárcel La Picota](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/04/jhon.jpg "Cárcel La Picota")  

<figcaption>
</figcaption>
</figure>
La Masacre en la Cárcel La Modelo
---------------------------------

La noche del 21 de marzo se registraron amotinamientos en más de 10 cárceles del país, entre ellas: Cómbita en Boyacá, La cárcel de Cúcuta, Picaleña en Ibagué, la cárcel de mujeres del Buen Pastor, La Picota y La Modelo en Bogotá, entre otras.

Hacia las nueve de la noche, un conjunto de vídeos de los reclusos de la cárcel la Modelo encendieron las alarmas. En ellos denunciaban el **accionar desproporcionado por parte del INPEC** que habría disparado con armas de fuego en su contra.

El resultado de esta acción fue el **asesinato de 23 personas y más de 83 heridos.** La primera información por parte del gobierno, emitida por la Ministra de Justicia Margarita Cabello, señaló que estos hechos fueron producto de una fuga masiva. (Le puede interesar: ["Negligencia de Estado contra reclusos que protestaba por riesgo de COVID-19"](https://archivo.contagioradio.com/hay-negligencia-de-estado-contra-reclusos-que-protestaba-por-riesgo-de-covid/))

https://twitter.com/infopresidencia/status/1241773687495372800?ref\_src=twsrc%5Etfw%7Ctwcamp%5Etweetembed%7Ctwterm%5E1241773687495372800&ref\_url=https%3A%2F%2Fwww.contagioradio.com%2Famotinamiento-dejo-23-muertos-en-la-carcel-la-modelo%2F, 

Sin embargo, organizaciones defensoras de derechos humanos desmintieron esta afirmación y aseguraron que lo ocurrido fue una **"masacre" producto del abuso de la Fuerza Pública.**

Semanas más tarde, el Fiscal General Roberto Barbosa afirmó que los amotinamientos de La Modelo fueron suscitados por una disidencia de las FARC. Hecho que también fue rechazado por las organizaciones, debido a que en ese penal no habían prisioneros políticos, y no se demostraba ninguna relación entre los reclusos y el grupo armado.

El abogado Uldarico Florez aseguró que esa respuesta por parte de la Fiscalía solo busca **"ocultar a los verdaderos responsables"** y el deber de estas autoridades con los planes y protocolos para prevenir un posible contagio. Acciones que habrían calmado el miedo que sintieron los reclusos y hubiesen evitado la masacre.

A la fecha, Florez aseguró que no se tiene el resultado de los análisis de Medicina Legal ni conocimiento de las investigaciones que tendría que adelantar la Fiscalía. (Le puede interesar: "["Fiscal Barbosa está encubriendo responsables de masacre en cárcel La Modelo" Uldarico Florez"](https://archivo.contagioradio.com/fiscal-barbosa-esta-encubriendo-responsables-de-masacre-en-carcel-la-modelo-uldarico-florez/))

HACINAMIENTO EN LAS CÁRCELES
----------------------------

0  
CAPACIDAD EN LAS CÁRCELES DEL PAÍS  
0  
NÚMERO ACTUAL DE RECLUSOS  
0  
%  
SOBREPOBLACIÓN  
[Tweet](https://twitter.com/share?ref_src=twsrc%5Etfw)

Las cárceles en hacinamiento no soportarán el Covid-19
------------------------------------------------------

Pero ¿cómo entender el miedo que siente la población reclusa? El sistema carcelario en Colombia tiene capacidad para unas 60.000 personas. No obstante actualmente **posee cerca de 120.000 reclusos, es decir, existe un 48% de hacinamiento.**

Este porcentaje no es igualitario en todas las cárceles del país. Algunas como el centro penitenciario Distrital, en Bogotá, no tiene hacinamiento, sin embargo, otras** albergan hasta un 100% más de su capacidad en torno a población reclusa. **

Por ejemplo, la cárcel de Villavicencio tiene un nivel de hacinamiento del 101,9% y a la fecha reporta 270 presos, 40 guardias del INPEC y dos trabajadores contagiados con Covid 19, cifra que ha aumentado de forma exponencial.

De hecho, los reclusos denuncian que la celda en la que inició el brote del virus tenía **60 personas, en un patio con 483 prisioneros**. Condiciones que evitan que prácticas como el distanciamiento social o el aislamiento puedan ser efectivas a la hora de mitigar la pandemia. (Le puede interesar: ["Prisionero político relata el drama de vivir con el COVID en la cárcel de Villavicencio"](https://archivo.contagioradio.com/prisionero-politico-relata-el-drama-de-vivir-con-el-covid-en-la-carcel-de-villavicencio/))

Otras cárceles que presentan contagios son La Picota, con **dos reclusos y un nivel de hacinamiento del 53%;** la cárcel de Leticia, con un interno y la cárcel Distrital que reportó un guardia con el virus. 

Esta problemática ha estado presente en las cárceles desde hace más de dos décadas y no ha sido atendida por ningún gobierno. Tanto así que en 1998 la Corte Constitucional emitió la sentencia T 153 con la que se declaraba el Estado de cosas Inconstitucionales. Es decir que hay una violación masiva, generalizada y estructural de los derechos fundamentales.

"Somos los internos que estamos contagiados del COVID-19"

Cárcel de Villavicencio

https://www.youtube.com/watch?v=MQQzhJ1\_2Ao&t=1s  
[Tweet](https://twitter.com/share?ref_src=twsrc%5Etfw)  
<iframe src="https://www.facebook.com/plugins/share_button.php?href=https%3A%2F%2Fwww.contagioradio.com%2Fcovid-19-y-crisis-carcelaria-colombiana%2F&amp;layout=button_count&amp;size=small&amp;appId=894195857389402&amp;width=99&amp;height=20" width="99" height="20" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>

«No hay médicos, no hay tapabocas, no hay protocolos»
-----------------------------------------------------

José, prisionero político que se encuentra en la Cárcel de Villavicencio, denuncia que además del hacinamiento, los reclusos deben vivir con la ausencia de atención médica. Ello debido a que la cárcel solo cuenta con una enfermera que esta presente por las mañanas.

Por eso, **el mayor miedo de las personas en esta prisión es morirse en la noche**, sin que alguien con los conocimientos adecuados pueda auxiliarles. Además tampoco habrían médicamentos suficientes para calmar las afecciones de quienes ya padecen el covid 19.

«Tengo entendido que las personas fueron trasladadas a otro pabellón. Allá en una celda pequeña, agruparon a los que están enfermos. Decían que en primer lugar, no les estaban suministrando medicamentos y que los tenían hacinados», afirma José.

Igualmente la guardia del INPEC, denuncia que hasta el momento no hay dotación especial que les permita cumplir su labor sin ponerse en riesgo de contagio. Situación que también se estaría dando en otras cárceles del país. (Le puede interesar: [«Emergencia por COVID llegó al INPEC y tampoco hay protocolo»](https://archivo.contagioradio.com/emergencia-por-covid-llego-al-inpec-y-tampoco-hay-protocolo/))

Asimismo, aseguran que tampoco se han practicado a tiempo las pruebas que detectan el Covid 19. Hecho que ha provocado que de dos internos que dieron positivo al virus, después de haber muerto; actualmente **hayan 314 presos, 29 miembros del INPEC y dos trabajadores de la cárcel contagiados.**

Cifra que será mucho más grande en las próximas semanas y que según la Brigada jurídica Eduardo Umañana podría configurarse como un genocidio Anunciado. Ello debido a la **inexistente reacción oportuna** por parte del Director del INPEC, el General Norberto Mogica y la Ministra de Justicia, Margarita Cabello, desde que se registró el primer caso dentro de este centro penitenciario.

https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/04/carcel-patio.mp4"El Covid 19 se vence lavándose las manos pero no hay agua"  
![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/04/cárcel-1024x682.jpeg){width="1024" height="682" sizes="(max-width: 1024px) 100vw, 1024px" srcset="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/04/cárcel-1024x682.jpeg 1024w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/04/cárcel-300x200.jpeg 300w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/04/cárcel-768x512.jpeg 768w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/04/cárcel-370x247.jpeg 370w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/04/cárcel.jpeg 1280w"}  
[Tweet](https://twitter.com/share?ref_src=twsrc%5Etfw)  
<iframe src="https://www.facebook.com/plugins/share_button.php?href=https%3A%2F%2Fwww.contagioradio.com%2Fcovid-19-y-crisis-carcelaria-colombiana%2F&amp;layout=button_count&amp;size=small&amp;appId=894195857389402&amp;width=99&amp;height=20" width="99" height="20" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>

El peligro de la propagación masiva del covid 19 en los centros de reclusión también fue denunciada por la Comisión de Justicia y Paz, que con una serie de tutelas, evidencian las pésimas condiciones de salubridad de los centros de reclusión en Colombia.

En ellas exponen situaciones tan graves como **la restricción al agua que hay en la gran mayoría de cárceles del país.** Por ejemplo en centros penitenciarios cómo Cómbita, el agua llega solo dos veces al día durante 15 minutos.

Es decir que los reclusos tampoco podrán cumplir con las indicaciones de la OMS, en donde se advierte la importancia de **lavarse las manos cada tres horas para prevenir los contagios**. Mucho menos hacer el aseo de las celdas, patios y espacios comunes, en donde una vez más el hacinamiento es máximo.

Igualmente revelan que «los elementos sanitarios y de aseo que permiten prevenir el contagio del virus como guantes, tapabocas, jabones, antibacteriales entre otros, no se han suministrados en los diferentes establecimientos penitenciarios y carcelarios».

Ausencia de disposiciones que ha puesto en máximo riesgo a la población reclusa y que deja dudas sobre el funcionamiento de las cárceles; por ejemplo **¿por qué en estos lugares no hay garantías al derecho fundamental al agua potable?**

Frente a esta situación el Juzgado Primero de Familia de Tunja; el Juzgado Tercero de Familia de Ibagué y el Juzgado Primero de Familia de Manizales, fallaron a favor de la población privada de la libertad de estas cárceles exigiendo que se garantice el derecho al agua potable y a la salud.

A estas acciones en defensa de la población reclusa también se sumaron las de la Fundación Comité de Solidaridad con los Presos Políticos (FCSPP) y el senador Iván Cepeda, que solicitaron medidas cautelares a la Comisión Interamericana de Derechos Humanos (CIDH). **La solicitud espera cobijar 10 centros penitenciarios del país con 14 medidas específicas.**

</p>
![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/04/carcel-2.jpg){width="640" height="480" sizes="(max-width: 640px) 100vw, 640px" srcset="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/04/carcel-2.jpg 640w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/04/carcel-2-300x225.jpg 300w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/04/carcel-2-370x278.jpg 370w"}

La pena de muerte en Colombia
-----------------------------

Frente a la crisis humanitaria que afrontan las cárceles, el pasado 14 de abril el presidente Iván Duque expidió el Derecreto 546 de excarcelación. Con este las y los prisioneros esperaban medidas alternativas para descongestionar las cárceles y evitar la propagación de la pandemia. Sin embargo este decreto solo beneficiaría al 5% de la población.

Según la medida podrán salir mujeres en estado de gestación, quienes hayan cumplido más del 40% de su condena, personas con movilidad reducida y enfermedades crónicas, personas condenada a 5 años o menos y personas mayores de 60 años. El beneficio será por 6 meses y será un juez de garantías quien de la orden de salida.  (Le puede interesar: [«Decreto presidencial no resuelve hacinamiento ni emergencia por COVID»](https://archivo.contagioradio.com/decreto-presidencial-no-resuelve-hacinamiento-ni-emergencia-por-covid/))

Sin embargo, el documento tiene un artículo que especifica que personas condenadas por delitos sexuales, de lesa humanidad, cobijados por justicia transicional o imputados por pertenecer a Grupos Armados delincuenciales no serán incluidos en esta medida.

Artículo que para las organizaciones defensoras de derechos humanos no solo es discriminatorio sino que imposibilita una excarcelarión real. Jazmin Reyes del colectivo Mujeres Libre, **explica que si una mujer embarazada está condenada por esas excepciones no podrá salir de la cárcel.**

Asimismo sucede con quienes ya lleven más del 40% de su condena y estén procesados por los delitos del artículo. Razón por la cual con este decreto no saldrán más de **4.000 personas**. Lo que significa que tampoco es una acción que alivie el hacinamiento o que de garantías para la población reclusa frente al Covid 19.

</p>
Las dudas de la Corte Constitucional frente al decreto de excarcelación
-----------------------------------------------------------------------

El pasado 24 de abril, la Corte Constitucional también emitió un documento con 27 preguntas frente al decreto 546 de excarcelación en donde, en un plazo de tres días, espera que el gobierno del presidente de Iván Duque argumente si ese mandato cumple con los principios de finalidad, necesidad, **proporcionalidad, motivación de incompatibilidad, no discriminación, entre otros.**

Las preguntas también deberán ser respondidas por el Ministerio de Justicia y la dirección del INPEC, en cabeza del general Roberto Mojica. Entre los cuestionamientos más importantes del documento, se encuentra **la relación del mismo con el porcentaje de personas que recibirían otro tipo de medidas.** (Le puede interesar:["Preguntas Corte Constitucional"](https://drive.google.com/file/d/10QUGV6jTLI2gGIWRQUE33WCW0P7sKQOY/view))

También pregunta por la información específica que tuvo en cuenta el gobierno sobre las condiciones actuales de las personas privadas de la libertad y pide que se explique sobre si este decreto «es suficiente para garantizar las medidas de prevención y cuidado en el marco de la pandemia». Ello debido precisamente al número tan bajo de población que saldría de las cárceles.

Igualmente, la **Procuraduría recomendó al Ministerio de Justicia que apliqué el Código Penal**, para contribuir en el deshacinamiento que hay actualmente en los centros de reclusión y que aumentan el riesgo de contagio.

La comisión de seguimiento a la sentencia T-388 de 2013, el pasado 23 de marzo de este año, también dio a conocer una serie de recomendaciones que servirían para descongestionar el sistema carcelario.

Entre las más importantes se encuentra la sustitución masiva de medidas de aseguramiento de detención preventiva. De tal manera que personas que cumplan condenas por delitos leves y no violentos, reciban medidas de aseguramiento no privativas de la libertad.

De igual forma recomienda el uso de **medidas alternativas** como vigilancia electrónica, vigilancia jurídica o de instituciones de policía, detención domiciliaria y otras dependiendo del riesgo del caso, el tipo delictivo y si hubo reincidencia.

El documento expone que de tomarse en cuenta estas posibilidades, con delitos  de tráfico, fabricación o porte de estupefacientes podría **reducirse la población carcelaria de hombres en 7.402 y en mujeres 1.348.**

### Cárcel de combita Boyacá

-   Protesta de prisioneros de Combita

https://www.youtube.com/watch?v=YYUHqAh9nGk&t=19s  
¿Quién responderá por la muerte anunciada?

La respuesta del movimiento carcelario a este decreto ha sido manifestar que el presidente Duque los estaría sometiendo a la pena de muerte, pese a que esta no está consagrada en la Constitución colombiana. Y que lo ha hecho desde la llegada del virus a Colombia.

Ya que debido a la falta de acciones jurídicas y humanitarias, en tres de las cárceles del país se encuentra el virus del Covid 19. Asimismo, las organizaciones defensoras de derechos humanos aseguran que **los responsables de estas muertes son el Estado en cabeza del presidente Duque,** la ministra de Justicia Margarita Cabello y el director del INPEC, el general Mojica.

De igual forma la [Comisión de Justicia y Paz](https://www.justiciaypazcolombia.com/) asegura que en estos hechos se puede hablar de una negligencia del Estado, debido a que «este escenario fue previsible y alertado por el movimiento carcelario desde el 16 marzo, sin que se tomaran las correspondientes medidas y planes que de manera efectiva que controlaran la pandemia».

También aseguran que hay que profundizar en el concepto de ciudadano y derechos. Ello debido a que **«pareciere que las personas que están condenadas, pierden su calidad de ciudadano».**

Finalmente abren el debate con el siguiente interrogante: 

##### «¿Será que es el momento de cuestionarnos sobre el valor real de la humanidad y la dignidad humana como principio legítimante del Estado o por el contrario, estamos dispuestos a soportar las muertes de las y los prisioneros?».

[Tweet](https://twitter.com/share?ref_src=twsrc%5Etfw)  
<iframe src="https://www.facebook.com/plugins/share_button.php?href=https%3A%2F%2Fwww.contagioradio.com%2Fcovid-19-y-crisis-carcelaria-colombiana%2F&amp;layout=button_count&amp;size=small&amp;appId=894195857389402&amp;width=99&amp;height=20" width="99" height="20" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>  
[![Prisionera víctima de caso Andino está en aislamiento por posible contagio de Covid 19](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/05/lina-370x260.png){width="370" height="260"}](https://archivo.contagioradio.com/prisionera-victima-de-caso-andino-esta-en-aislamiento-por-posible-contagio-de-covid-19/)  

#### [Prisionera víctima de caso Andino está en aislamiento por posible contagio de Covid 19](https://archivo.contagioradio.com/prisionera-victima-de-caso-andino-esta-en-aislamiento-por-posible-contagio-de-covid-19/)

El pasado 11 de mayo, las prisioneras políticas, Alejandra Méndez y Lizeth Rodríguez, víctimas del montaje judicial conocido…[Leer más](https://archivo.contagioradio.com/prisionera-victima-de-caso-andino-esta-en-aislamiento-por-posible-contagio-de-covid-19/)  
[![Juez ordena al Ejército pedir perdón a las víctimas de la finca La Galleta](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/Tierras-1-370x260.jpeg){width="370" height="260"}](https://archivo.contagioradio.com/juez-ordena-al-ejercito-acto-de-perdon-hacia-las-victimas-de-la-finca-la-galleta/)  

#### [Juez ordena al Ejército pedir perdón a las víctimas de la finca La Galleta](https://archivo.contagioradio.com/juez-ordena-al-ejercito-acto-de-perdon-hacia-las-victimas-de-la-finca-la-galleta/)

Este 12 de mayo la Fundación Forjando Futuros comunicó la reciente orden del Juzgado Primero Civil del Circuito…[Leer más](https://archivo.contagioradio.com/juez-ordena-al-ejercito-acto-de-perdon-hacia-las-victimas-de-la-finca-la-galleta/)  
[![Ejército pone en riesgo a población de Piamonte, Cauca](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/05/Erradicación-de-Cultivos-de-Coca-en-Catatumbo-370x260.jpg){width="370" height="260"}](https://archivo.contagioradio.com/ejercito-pone-en-riesgo-a-poblacion-de-piamonte-cauca/)  

#### [Ejército pone en riesgo a población de Piamonte, Cauca](https://archivo.contagioradio.com/ejercito-pone-en-riesgo-a-poblacion-de-piamonte-cauca/)

La Comisión de Derechos Humanos de la Asociación interveredal de trabajadores campesinos y campesinas de Piamonte Cauca denuncian…[Leer más](https://archivo.contagioradio.com/ejercito-pone-en-riesgo-a-poblacion-de-piamonte-cauca/)  
[![Asesinan a firmante del Acuerdo de Paz en Bello, Antioquia](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/05/Wilder-Marin-370x260.jpg){width="370" height="260"}](https://archivo.contagioradio.com/asesinan-a-firmante-del-acuerdo-de-paz-en-bello-antioquia/)  

#### [Asesinan a firmante del Acuerdo de Paz en Bello, Antioquia](https://archivo.contagioradio.com/asesinan-a-firmante-del-acuerdo-de-paz-en-bello-antioquia/)

El pasado 8 de mayo fue encontrado el cuerpo sin vida de Wilder Daniel Marín, ex prisionero político,…[Leer más](https://archivo.contagioradio.com/asesinan-a-firmante-del-acuerdo-de-paz-en-bello-antioquia/)
