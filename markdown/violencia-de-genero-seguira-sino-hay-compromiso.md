Title: Violencia de género seguirá sino hay compromiso institucional
Date: 2016-11-30 12:21
Category: Mujer, Nacional
Tags: Dora Lilia, mujeres, Violencia de género
Slug: violencia-de-genero-seguira-sino-hay-compromiso
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/violencia-de-genero.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Periódico Correo] 

###### [30 Nov. 2016] 

Una mujer trabajadora, tranquila, a la que no le gustaban los problemas, así recuerda su familia a **Dora Lilia, aquella mujer de 44 años, de mirada sincera, que fue brutalmente atacada el 6 de noviembre y a quien está mañana, luego de 24 días de luchar por su vida, fue sorprendida por la muerte. Dora es una víctima más de la violencia de género.**

Según habían reportado algunas organizaciones sociales que estuvieron siguiendo el caso en el Valle, **Dora Lilia presentaba algunas mejorías en su estado de salud pero aún mostraba un pronóstico reservado a nivel neurológico.**

Para Olga Amparo Sánchez Directora de la Casa de la Mujer, este hecho entristece y enluta a toda la sociedad **“esta situación lo que muestra es la desprotección de las mujeres, la falta de voluntad política de los entes investigadores para llevar a cabo una investigación rápida y sancionar”** aseveró.

Aún no se conoce a ciencia cierta cuáles fueron los móviles para llevar a cabo el maltrato y daño a la integridad de Dora Lilia, ni tampoco hay mayores avances en lo que respecta a la investigación para dar con los culpables y poder sancionar.

Según cifras de varias organizaciones, además del caso de Dora Lilian, **cerca de 80 mujeres, solamente en Buga han denunciado ser víctimas de abuso** “estamos ante dos hechos graves, uno la dilación de las autoridades frente a los delitos que se comenten contra las mujeres en términos de investigación y sanción. El segundo es una falla grandísima ante la prevención de las violencias contra las mujeres” agregó Olga Amparo.

Para la directora de la Casa de la Mujer, lo que más sorprende de estos casos es que **las mujeres que son asesinadas o violentadas de diversas formas son quienes han pedido protección a las autoridades o en una comisaria de familia** “ellas llevan sus casos y las medidas de protección no son efectivas, lo que muestra la falta de voluntad política desde lo nacional hasta lo territorial”.

La sociedad, que aún se encuentra consternada por el atroz hecho del que fue víctima Dora Lilia, realizó una marcha el viernes pasado en Buga para rechazar este hecho a propósito del Día de la Lucha contra la Violencia a la Mujer.

**“Tenemos que continuar exigiendo, denunciando hasta que nos hagan caso lastimosamente la denuncia no está sirviendo para que las autoridades realmente se muevan”** afirmó Olga.

**¿Son los piropos violencia de género?**

En Timbío, Cauca están trabajando por ser el primer lugar de Colombia que pueda considerarse como libre de acoso contra las mujeres. **Esta iniciativa fue conocida por medio de un decreto el 25 de noviembre, en el que hacen que funcionarios, servidores públicos, contratistas de la administración y la Fuerza Pública eviten lanzar piropos.**

Por supuesto, este decreto ha suscitado todo tipo de reacciones. Según Olga Amparo existe una delgada línea entre el piropo que no es morboso y por el cual una mujer no se siente molesta con ello sin embargo dijo **“yo no veo porque las mujeres nos tengamos que aguantar piropos ni morbosos ni los no morbosos porque nos molestan. Los hombres salen a la calle y no deben aguantarse los piropos de las mujeres”.**

Con esta medida en Timbío, se pretende poner en evidencia que el cuerpo de la mujer no tiene por qué ser motivo de ser molestado por los hombres.

Por último, Olga Amparo concluye que este tipo de iniciativas y otras más que hay en el país no pueden quedarse en el papel “**mientras no existan mecanismos que realmente funcionen para sancionar las violencias contra las mujeres, ni mecanismos de protección y prevención reales, en suma sino hay una voluntad política para este tema no va a pasar nada”. **Le puede interesar: [La violencia contra las mujeres en Colombia no cesa](https://archivo.contagioradio.com/la-violencia-contra-las-mujeresen-colombia-no-cesa/)

<iframe id="audio_14323889" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_14323889_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)

<div class="ssba ssba-wrap">

</div>
