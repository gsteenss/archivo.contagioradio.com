Title: Las memorias de Dabeiba: En busca de la verdad
Date: 2019-12-25 11:46
Author: CtgAdm
Category: DDHH, Especiales Contagio Radio
Tags: Dabeiba, Desplazamiento, Festival de las Memorias, La Balsita, La Llorona
Slug: las-memorias-de-dabeiba-en-busca-de-la-verdad
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/IMG_8470.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

las memorias de Dabeiba:  
en busca de la verdad
=========================

![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/04/logo-contagio-radio-web-1.png){width="201" height="76"}

<figure>
![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/Untitled-1-310x310.png){width="310" height="310" sizes="(max-width: 310px) 100vw, 310px" srcset="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/Untitled-1-310x310.png 310w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/Untitled-1-300x300.png 300w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/Untitled-1-150x150.png 150w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/Untitled-1-370x369.png 370w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/Untitled-1-512x512.png 512w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/Untitled-1-230x230.png 230w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/Untitled-1-360x360.png 360w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/Untitled-1.png 746w"}

</figure>
###### Sandra Gutiérrez

Periodista/Contagio Radio

**20 de Diciembre de 2019**

**Del 28 de noviembre al primero de diciembre en la Zona Humanitaria de la Balsita, en la vereda Caracolón, municipio de Dabeiba, se realizó el cuarto Festival por las Memorias, un escenario en el que otra Colombia ha empezado a construirse a partir de encuentros que transgreden la realidad violenta y le apuestan a la reconciliación.​​**

CAPITULO I  **LA VERDAD**

En la tienda de la zona humanitaria de La Balsita pasan cosas mágicas, y hay que ser muy hábil para percibirlas. Una tarde de noviembre, ese lugar, fue el punto de encuentro para que victimas del conflicto armado y actores armados se vieran los ombligos.

##### **¿Ustedes con qué están ombligados?**

preguntó César Murillo, integrante del Partido político FARC y asistente al 4to Festival de las Memorias, a quiénes estaban presentes en la tienda, entre ellos un capitán del Ejército, defensores de derechos humanos y líderes y lideresas provenientes de diferentes regiones del país.

Algunos desconocedores del tema, se vieron los ombligos con incertidumbre y afirmaron que, sobre esa parte de sus cuerpos, poca información tenían.

Areiza Salazar afrodescendiente de la Comunidad de Nueva Esperanza, en Cacarica, les explicó que ombligarse era una tradición afro ancestral, en donde la tierra y los animales dan poderes mágicos a las personas.

> ##### *“Eso se hace con los bebés.* *Cuando nacen les cortan el cordón del ombligo y lo untan de un emplaste que se hace con alguna parte del cuerpo de un animal, minerales o plantas, dependiendo de los dotes que se quieran obtener, y luego lo entierran debajo de un árbol para que la tierra bendiga a los niños”.* 

Ese fue el principio de una conversación que desembocó en preguntas sobre el conflicto, en respuestas sobre la crudeza de la guerra, pero también sobre la humanidad de quienes fueron partícipes y afectados de estos hechos. En la tienda de un momento a otro una verdad, tranquila, original, apareció.

Paradójicamente, en Bogotá, en esos mismos días, en la **JEP**, la verdad también estaba buscando salir con el testimonio de un militar que confesó como entre 2005 al 2007 fueron ejecutados por militares adscritos a la Brigada XI, más de 45 civiles, con edades entre 19 y 56 años de edad, enterrados en el cementerio de Las Mercedes, en Dabeiba. Los cuerpos de NN eran llevados simulando una sepultura digna o bendita, dotando de una bendición la impunidad de los militares.

###### Le puede interesar: [Somos esa luz, esa esperanza para lograr una patria justa: comunidad de Dabeiba](https://archivo.contagioradio.com/somos-esa-luz-esa-esperanza-para-lograr-una-patria-justa-comunidad-de-dabeiba/)

<figure>
![festival por las memorias](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/Dabeiba-14-scaled.jpg){width="2560" height="1707" sizes="(max-width: 2560px) 100vw, 2560px" srcset="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/Dabeiba-14-scaled.jpg 2560w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/Dabeiba-14-300x200.jpg 300w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/Dabeiba-14-1024x683.jpg 1024w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/Dabeiba-14-768x512.jpg 768w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/Dabeiba-14-1536x1024.jpg 1536w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/Dabeiba-14-2048x1365.jpg 2048w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/Dabeiba-14-370x247.jpg 370w"}  

<figcaption>
niños con cartel

</figcaption>
</figure>
CAPITULO II **EL OMBLIGO DE ADRIANA**

La Zona Humanitaria de la Comunidad de Vida y de Trabajo La Balsita, ubicada en Dabeiba, fue creada por 22 familias desplazadas producto de las operaciones de las Autodefensas Unidas de Córdoba y el Urabá, **AUC,** que pusieron en marcha un plan sistemático que contó con bloqueos económicos, amenazas, asesinatos selectivos y desapariciones forzadas para sacar a las personas de sus territorios y ejercer control sobre el Nudo de Paramillo, a finales de noviembre de 1997, en las veredas de Antazales y La Balsita.

Estas acciones irregulares se desarrollaron con apoyo de hombres de los llamados "Los 12 apostoles" grupo paramilitar que estaba al mando de Santiago Uribe, según un informe judicial.

Ademas, este desplazamiento de la zona rural hasta la cabecera municipal, se desató luego de las masacres de Ituango, con el apoyo de la IV Brigada del Ejército Nacional, en el período en el que Álvaro Uribe se desempeñó como gobernardor del departamento de Antioquia, hoy en poder de las autodenominadas Autodefensas Gaitanistas de Colombia, que coexisten con asentamientos militares de la I División.

![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/322A3255-CFD3-4657-A014-0AAA649E5F6F-770x540.jpg){width="770" height="540" sizes="(max-width: 770px) 100vw, 770px" srcset="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/322A3255-CFD3-4657-A014-0AAA649E5F6F-770x540.jpg 770w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/322A3255-CFD3-4657-A014-0AAA649E5F6F-370x260.jpg 370w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/322A3255-CFD3-4657-A014-0AAA649E5F6F-550x385.jpg 550w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/322A3255-CFD3-4657-A014-0AAA649E5F6F-110x78.jpg 110w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/322A3255-CFD3-4657-A014-0AAA649E5F6F-216x152.jpg 216w"}

**Adriana y Walberto**, fueron dos de los más de mil campesinos víctimas del desplazamiento. Ambos se conocieron en los albergues provisionales, uno ubicado en la sede del Partido Liberal y otros en salones del Colegio White, que resguardaron a más de 400 familias. Tiempo después juntos harían parte del proceso de creación de la zona humanitaria.

Allí Adriana decidió que crecieran sus 5 hijos, en una pequeña casa, a la que no define como hogar porque para ella su casa está en Antazales, vereda de la que salió hace ya 22 años y a la que no ha podido regresar.

Su sala es un templo a la historia, están las fotografías que registran el crecimiento de sus niños y están las fotos de su tío, “*Él es Simón, el esposo de mi tía, a él lo desaparecieron los paras durante la invasión, ese fue el principio de una historia de terror y angustia”* afirmó.

El **24 de noviembre de 1997** Adriana estaba junto a Simón en su finca, ubicada en Antazales. Hacia las diez de la mañana escuchó un ruido tremendo. Cuando salió de la casa, observó un helicóptero asomarse por entre las montañas, despavorida corrió hacia Simón y le suplicó que salieran del lugar. Eran los paramilitares. Simón decidió quedarse, nada de huir, manifestó el hombre porque *"el que nada debe, nada teme”.*

Hacía el medio día los paramilitares entraron a la casa, golpearon a Simón y le dieron a Adriana cinco minutos para salir de la vivienda. *“¿Uno qué hace?”* aseveró.

Una semana más tarde, ella junto a sus vecinos bajaban como lluvia de las montañas, llegaron a Dabeiba en busca de ayuda. Sin embargo, estas imágenes se habían vuelto recurrentes en el pueblo, porque a partir del **27 de noviembre,** las familias llegaban desde caseríos más cercanos al municipio.

Se arrumó, con lo poco que pudo sacar, en el colegio junto a más de mil personas, "mal contados", entre niños, mujeres embarazadas, adultos mayores y por supuesto… su familia.

![la balsita](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/La-Balsita-98-1024x685.jpg){width="1024" height="685" sizes="(max-width: 1024px) 100vw, 1024px" srcset="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/La-Balsita-98-1024x685.jpg 1024w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/La-Balsita-98-300x201.jpg 300w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/La-Balsita-98-768x514.jpg 768w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/La-Balsita-98-1536x1028.jpg 1536w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/La-Balsita-98-2048x1371.jpg 2048w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/La-Balsita-98-370x248.jpg 370w"}

Pero el terror no paró ahí. Una vez “acomodados” los familiares empezaron a contar las historias sobre quienes fueron desaparecidos como Simón, de quienes eran lanzados del punto conocido como La Llorona al afluente Ríosucio y de los centenares de cuerpos que navegaron por los ríos para luego ser enterrados como NN en fosas comunes.

Eran tantos los muertos, que fue necesario abrir más cementerios, para un total de 6 camposantos. Recientemente en el de Las Mercedes se descubrió una fosa común que podría contener más de 50 víctimas de ejecuciones extrajudiciales cometidas por el Ejército. Lamentablemente esta sería una práctica de vieja data, porque según los habitantes de este municipio, desde el 96 los cementerios han sido utilizado para enterrar los muertos que aparecían, los que nadie conocía, o a los que ejecutaban las estructuras paramilitares.

No obstante, Adriana, que por ese momento ya tenía dos hijos, no iba a permitir que la vida se apagará, así que junto a más de 90 personas fundaron el proceso de la zona humanitaria, en compañía de organizaciones defensoras de derechos humanos.

Y aunque se han fortalecido tanto Adriana como los demás, anhelan regresar a sus montañas, *“veintidós años más tarde seguimos a la espera de volver a nuestra casa, yo no digo que acá este mal, pero esta no es mi tierra y mi sueño es que mis hijos regresen a nuestra finca para hacer sus vidas”.*

El dolor y la violencia no fueron impedimento para que, en medio de ausencia estatal, creciera su familia, "en la abundancia del afecto y la solidaridad".

Podría ser que Adriana este ombligada con una mula, que haya adquirido las bondades de la sabia terquedad y persista tanto en la vida misma, que ahora esta se haya tomado cada rincón de la comunidad de vida y trabajo de La Balsita. Son 36 familias y una promesa de juventud que le apuesta a un país distinto.

![taller fotografia](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/elementor/thumbs/IMG_8284-oigxnvm8tbul5wu4e7gbq458us51dcquvvz5qa49kq.jpg "taller fotografia")  
![niños durante el festival](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/Captura-de-pantalla-2019-12-16-a-las-12.04.45.png){width="2864" height="1598" sizes="(max-width: 2864px) 100vw, 2864px" srcset="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/Captura-de-pantalla-2019-12-16-a-las-12.04.45.png 2864w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/Captura-de-pantalla-2019-12-16-a-las-12.04.45-300x167.png 300w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/Captura-de-pantalla-2019-12-16-a-las-12.04.45-1024x571.png 1024w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/Captura-de-pantalla-2019-12-16-a-las-12.04.45-768x429.png 768w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/Captura-de-pantalla-2019-12-16-a-las-12.04.45-1536x857.png 1536w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/Captura-de-pantalla-2019-12-16-a-las-12.04.45-2048x1143.png 2048w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/Captura-de-pantalla-2019-12-16-a-las-12.04.45-370x206.png 370w"}

CAPITULO III **EL OMBLIGO DE DAVID**

El **28 de noviembre** en un taller de fotografía con las y los niños de la zona humanitaria llegó David.  Un pequeño despierto y profundamente identificado con la comunidad. Por su cabeza pasaban los nombres de los papitos y mamitas (abuelos y abuelas) que fundaron la zona humanitaria de La Balsita y conocía de “pe a pa” los lugares de la memoria, creados simbólicamente para no olvidar.

Esa fue una de las razones por las cuales, durante una de las actividades del Festival, David guió a los más de 150 participantes del evento, a uno de los puntos en que vieron con vida por última vez a Francisco Montoya, líder comunitario que denunció el desplazamiento y promotor de salud, **desaparecido por paramilitares el 4 de julio de 1998**, en la recta conocida como La Papayera, a 10 minutos del casco Urbano de Dabeiba.

Tres horas más tarde David había llevado a la caravana de delegados hasta la vereda La Balsita, destino final del recorrido de la memoria y lugar del que por lo menos 400 personas salieron desplazadas entre los días **27 al 29 de noviembre de 1997**, huyendo de la violencia paramilitar.

Él al igual que otros niños visitaron por primera vez la tierra de sus ancestros. Algunos de los adultos más grandes pisaban esas tierras tras 22 años de no poder ingresar por miedo a ser asesinados, como sucedió con las personas que intentaron volver a sus casas, y porque el territorio aún está bajo el control paramilitar, un dominio que va desde el casco urbano de Dabeiba hasta el Nudo de Paramillo.

![niños durante el festival](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/Captura-de-pantalla-2019-12-16-a-las-12.04.09-1-1024x573.png){width="1024" height="573" sizes="(max-width: 1024px) 100vw, 1024px" srcset="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/Captura-de-pantalla-2019-12-16-a-las-12.04.09-1-1024x573.png 1024w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/Captura-de-pantalla-2019-12-16-a-las-12.04.09-1-300x168.png 300w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/Captura-de-pantalla-2019-12-16-a-las-12.04.09-1-768x430.png 768w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/Captura-de-pantalla-2019-12-16-a-las-12.04.09-1-1536x860.png 1536w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/Captura-de-pantalla-2019-12-16-a-las-12.04.09-1-2048x1147.png 2048w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/Captura-de-pantalla-2019-12-16-a-las-12.04.09-1-370x207.png 370w"}

Al día siguiente, a la caravana le esperaba el último viaje para conocer La Llorona, un paraje boscoso y montañoso, con un misterioso silencio húmedo, ubicado en la carretera que conduce hacia Mutatá, a menos de 20 minutos de Dabeiba.

Del lugar David mencionó que solo conocía historias de terror, *“allí botaban los cuerpos de las personas y dicen que asustan”*, y así fue. Durante las últimas dos décadas, La Llorona fue utilizada como punto para amedrentar a la población civil.

Decenas, que pueden ser más de un centenar de NN, fueron arrojados allí al río RioSucio. Algunos eran oriundos de Medellín, otros de Dabeiba, Urama, Cañas Gordas, Mutatá o de cualquier otro lugar, la muerte causadas por los paramilitares no tuvo frontera, su visión de odio inoculado contra insurgente era justificación de cualquier tortura, desaparición o asesinato.

En otras ocasiones, en el túnel de la carretera que conduce al Golfo de Urabá, los paramilitares, con complicidad de la IV Brigada y la Policía de Urabá, instalaron un retén, con lista en mano bajaban de los vehículos a sus víctimas o a veces los escogían al azar, como emperadores sobre la vida.

Sin embargo, durante 45 minutos y casi como una oda a la alegría, mientras los adultos hablaban de las atrocidades que se cometieron en La Llorona, los niños sacaron su balón de Futbol y fueron por la reconquista del gol. David alegó a sus contrincantes las medidas de la cancha, organizo a los equipos y allí al lado de esa carretera, se disipó el miedo.

Esa jornada culminó con una marcha que alumbró el camino hasta el Árbol de la vida, un lugar en homenaje a las víctimas de aquellos hechos. David, junto a los demás niños, integrantes de la comunidad y participes del Festival se encontraron bajo el follaje de los viejos árboles, que derramaron la savia de memoria hacia sus frutos.

Ver mas: [Festival de las Memorias La Balsita-Dabeiba](https://www.justiciaypazcolombia.com/festival-de-las-memorias-la-balsita-dabeiba/)

![arbol de la vida](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/arbol.jpg){width="1119" height="861" sizes="(max-width: 1119px) 100vw, 1119px" srcset="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/arbol.jpg 1119w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/arbol-300x231.jpg 300w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/arbol-1024x788.jpg 1024w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/arbol-768x591.jpg 768w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/arbol-370x285.jpg 370w"}

#### EL ÁRBOL DE LA VIDA

***El Árbol de la vida* se convirtió en faro y trazó el camino de las generaciones venideras, puede ser que sea el ombligo de la Comunidad de vida y trabajo de La Balsita y que bajo sus raíces se encuentren las bendiciones para todos aquellos que heredan una lucha por la justicia y la verdad y que germina en bosques con nombres de personas que no que quedan en el silencio porque la tierra les mantiene latiendo en el corazón de sus ombligados. **

**Es usual que en los Festivales de las memorias se den encuentros particulares, nunca antes imaginados y tal vez solo hasta ahora deseados. Muchos y muchas han venido desde muy lejos para conocer las vicisitudes al otro lado del país, otros se han reencontrado tras largos años de últimos abrazos y eso es lo que podría suceder en otras esquinas mágicas, si la humanidad de los ombligos retorna con la memoria a develar la verdad.**

Entradas recientes:
-------------------

[![Las pistas tras el asesinato de la ecóloga Nathalia Jiménez y su esposo Rodrigo Monsalve](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/646210_1-370x260.jpg){width="370" height="260"}](https://archivo.contagioradio.com/las-pistas-tras-el-asesinato-de-la-ecologista-nathalia-jimenez-y-su-esposo-rodrigo-monsalve/)

#### [Las pistas tras el asesinato de la ecóloga Nathalia Jiménez y su esposo Rodrigo Monsalve](https://archivo.contagioradio.com/las-pistas-tras-el-asesinato-de-la-ecologista-nathalia-jimenez-y-su-esposo-rodrigo-monsalve/)

Nathalia Jiménez y a su esposo, el estudiante de antropología Rodrigo Monsalve habían desaparecido desde el pasado 20…[Leer más](https://archivo.contagioradio.com/las-pistas-tras-el-asesinato-de-la-ecologista-nathalia-jimenez-y-su-esposo-rodrigo-monsalve/)  
[![Asesinato de Ender Ravelo evidencia la ausencia de garantías para excombatientes en Norte de Santander](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/09/bandera-farc-370x260.jpg){width="370" height="260"}](https://archivo.contagioradio.com/asesinato-ender-ravelo-evidencia-incremento-amenazas-excombatiente-norte-de-santander/)

#### [Asesinato de Ender Ravelo evidencia la ausencia de garantías para excombatientes en Norte de Santander](https://archivo.contagioradio.com/asesinato-ender-ravelo-evidencia-incremento-amenazas-excombatiente-norte-de-santander/)

El asesinato del excombatiente Ender Elias Ravelo se suma a los cerca de 180 homicidios cometidos contra reincorporados…[Leer más](https://archivo.contagioradio.com/asesinato-ender-ravelo-evidencia-incremento-amenazas-excombatiente-norte-de-santander/)  
[![Las memorias de Dabeiba: En busca de la verdad](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/IMG_8470-370x260.jpg){width="370" height="260"}](https://archivo.contagioradio.com/las-memorias/)

#### [Las memorias de Dabeiba: En busca de la verdad](https://archivo.contagioradio.com/las-memorias/)

las memorias de Dabeiba:en busca de la verdad Sandra GutiérrezPeriodista/Contagio Radio 20 de Diciembre de 2019 Del 28…[Leer más](https://archivo.contagioradio.com/informe-relata-el-rol-de-la-inteligencia-militar-en-los-crimenes-de-estado/)  
[![Ascender militares investigados golpea el derecho a la no repetición](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/Ascensos-Militares-370x260.jpg){width="370" height="260"}](https://archivo.contagioradio.com/ascender-militares-investigados-golpea-el-derecho-a-la-no-repeticion/)

#### [Ascender militares investigados golpea el derecho a la no repetición](https://archivo.contagioradio.com/ascender-militares-investigados-golpea-el-derecho-a-la-no-repeticion/)

Foto: @honohenriquez Mientras en la madrugada del viernes la Cámara de Representantes aprobaba la Reforma Tributaria, el Senado…[Leer más](https://archivo.contagioradio.com/ascender-militares-investigados-golpea-el-derecho-a-la-no-repeticion/)  
[![Informe relata el rol de la inteligencia militar en los crímenes de Estado](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/inteligencia-militar-370x260.jpg){width="370" height="260"}](https://archivo.contagioradio.com/informe-relata-el-rol-de-la-inteligencia-militar-en-los-crimenes-de-estado/)

#### [Informe relata el rol de la inteligencia militar en los crímenes de Estado](https://archivo.contagioradio.com/informe-relata-el-rol-de-la-inteligencia-militar-en-los-crimenes-de-estado/)

Fue entregado a la Comisión de la Verdad, documento que recoge información sobre cómo incidió la inteligencia militar…[Leer más](https://archivo.contagioradio.com/informe-relata-el-rol-de-la-inteligencia-militar-en-los-crimenes-de-estado/)
