Title: Colombia tendrá un aumento de temperatura del 2,14 °C  
Date: 2015-04-14 14:02
Author: CtgAdm
Category: Ambiente, Nacional
Tags: Ambiente, Ambiente y Sociedad, cambio climatico, colombia, gases efecto invernadero, IDEAM, Naciones Unidas, Tercera Comunicación Nacional de Cambio Climático
Slug: colombia-tendra-un-aumento-de-temperatura-del-2-14-c
Status: published

##### Foto: [infomaderas.com]

De acuerdo a un estudio de las **Naciones Unidas** e **IDEAM**, Colombia vivirá un aumento de temperatura de 2.14 °C, debido a los efectos del **Cambio Climático** que se intensificarán si se sigue emitiendo altas cantidades de gases de efecto invernadero.

[***‘Nuevos Escenarios de Cambio Climático para Colombia 2011-2100, Herramientas Científicas para la Toma de Decisiones’,***] es el nombre del estudio, que evidencia que el aumento de temperatura se dará gradualmente hasta el año 2100. Además señala los posibles escenarios en materia de temperatura y precipitación a nivel nacional y, también a nivel departamental, ya que habrá los mismos efectos para todas las regiones*.*

El estudio indica que “**el 31% del territorio nacional podrá verse afectado por disminución en las precipitaciones durante los siguientes 25 años,** en más de 10 millones de hectáreas de otras zonas aumentarán las lluvias hasta en un 20%”, cita Ambiente y Sociedad, que cita al director general del IDEAM, Omar Franco Torres, quien asegura que “La información obtenida permitirá a los sectores productivos y a los gobiernos locales, regionales y nacionales generar una planeación y acciones que sean diferentes para cada una de las regiones y departamentos del país”.

Estas conclusiones hacen parte de la entrega de la **Tercera Comunicación Nacional de Cambio Climático,** también trata temas sobre adaptación, vulnerabilidad, gases efecto invernadero, aspectos sociales e implementación de estrategias de sensibilización.

**De acuerdo a Ambiente y Sociedad estas serían las 7 alertas que generan los Escenarios de Cambio Climático para Colombia.**

1.  Estos Escenarios indican científicamente que el país en su conjunto estará afectado por el Cambio Climático
2.  A 2100 en Colombia, se espera un aumento gradual en la temperatura  
   media de 2.14 °C.
3.  31% del territorio nacional podrá verse afectado por disminución en las precipitaciones durante los siguientes 25 años.
4.  Los efectos esperados del cambio climático no serán los mismos para todas las regiones del país.
5.  La planificación, acciones territoriales y sectoriales para la adaptación al cambio climático deben ser diferentes para cada una de las regiones y departamentos del país.
6.  Los Escenarios permitirán que todas las entidades, organizaciones y actores interesados, tomen decisiones de Cambio Climático con la misma base de información oficial.
7.  Con los Escenarios, el IDEAM ratifica su papel de aliado estratégico con los sectores, las alcaldías y las gobernaciones para apoyar procesos de planificación y su toma de decisiones.

