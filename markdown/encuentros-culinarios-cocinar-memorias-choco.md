Title: Encuentros culinarios para cocinar las memorias en Chocó
Date: 2019-04-18 14:10
Author: CtgAdm
Category: Comunidad, Entrevistas
Tags: Chocó, Comida, Festival, semillas
Slug: encuentros-culinarios-cocinar-memorias-choco
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/WhatsApp-Image-2019-04-17-at-6.07.10-PM.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

 

###### [Foto: Contagio Radio] 

El pasado miércoles, en predios del Consejo Comunitario de Camelias, territorio colectivo de Curvaradó (Chocó), se desarrolló uno de los Encuentros Culinarios: Memoria y Sabor, que se han desarrollado en otras zonas del país. Los ingredientes de esta reunión fueron la memoria, dignidad e identidad de una comunidad que, a través de sus preparaciones, hace frente a la invasión de su territorio, la violencia y el ritmo de vida actual que se quiere imponer sobre sus tradiciones.

De acuerdo a Andrés Maiz, integrante de Colectivo de la Vida y la Red Semilla Nativa, los encuentros culinarios responde a la necesidad de saciar el hambre de pan de las personas, pero también de memoria y dignidad; y representa una forma de enfrentar el confinamiento que viven algunas comunidades por cuenta de la presencia de actores violentos, construyendo tejido social alrededor del fogón.

### **Talleres para reencontrarse y compartir la memoria en torno a la mesa**

El encuentro inició con un abrazo que le permitió a las personas entrar en contacto con los otros, luego se hicieron grupos definidos por la propia comunidad para preparar los alimentos, al tiempo que se hacía memoria sobre la comida. Maiz explica que hablar sobre la comida es hacer memoria sobre lo que se cosechaba antes, y cómo se preparaban las comunidades en las diferentes etapas de la vida: Cuando se siembre la semilla humana, cuando se da a luz a esta semilla, cuando crecen y están listos para repetir el ciclo.

A lo largo de esa narración, la comida sobrepasa los límites de lo que consume el cuerpo y explora aquello que consume el alma: la relación que se establece con los otros con quienes se siembra, se cosecha, se prepara y se come. Posteriormente los grupos degustan los alimentos preparados, y se hacen conscientes sobre la importancia de luchar por la soberanía alimentaria, y mantener los lazos que se tejen cerca al fogón. (Le puede interesar:["Alerta por fuertes operaciones neoparamilitares en territorios de Curvaradó y Jiguamiandó"](https://archivo.contagioradio.com/operaciones-neoparamilitares-curvarado-jiguamiando/))

### **"Enfrentar la cultura que pone la comida empaquetada en la mano"**

Maiz expone que en este tiempo es importante revisar lo que se come y lo que no, porque  en estos momentos es relevante hablar de soberanía alimentaria como una forma de "enfrentar la cultura que pone la comida empaquetada en la mano". Para los territorios del Chocó, un departamento cuya titulación en buena parte es comunitaria, el retomar la experiencia de vida y cocina comunitaria "es un enfrentamiento al modelo de vida actual" individual y egoísta. (Le puede interesar: ["Más de 170 regiones europeas han prohibido los cultivos transgénicos"](https://archivo.contagioradio.com/mas-de-170-regiones-de-europa-han-prohibido-los-cultivos-transgenicos/))

Adicionalmente, durante los encuentros culinarios se realizan trueques de semillas nativas, otra lucha relevante contra las industrias que mediante los transgénicos alteran el sabor de los alimentos y obligan a quienes siembran a comprar constantemente sus productos, porque las semillas producen frutos en ocasiones predeterminadas. Estos trueques, así como los talleres se realizarán el próximo primero de mayo, en Cacarica, Chocó. (Le puede interesar: ["JEP y comunidades hacen historia con primera audiencia en zona rural de Cacarica"](https://archivo.contagioradio.com/jep-comunidades-audiencia-cacarica/))

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
