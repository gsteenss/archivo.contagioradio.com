Title: En Turbo, campesinos están siendo obligados a entregar el 50% de las tierras que habitan y trabajan
Date: 2020-07-24 16:54
Author: AdminContagio
Category: Actualidad, Comunidad
Tags: despojo de tierras, Turbo
Slug: en-turbo-campesinos-estan-siendo-obligados-a-entregar-el-50-de-las-tierras-que-habitan-y-trabajan
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/4639061927_0363a38865_c.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: Restitución de tierras imagen de referencia Flickr/ Tiago Brandão

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Desde inicios del mes de julio, **campesinos reclamantes de tierras de los corregimientos de Macondo y Blanquiceth en Turbo, Antioquia**, protegidos por medidas cautelares, son víctimas de amenazas y presiones por parte de los administradores de la Hacienda Flor del Monte, actualmente en disputa judicial en el Juzgado Primero Civil del Circuito Especializado en Restitución de Tierras de Quibdó.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

La denuncia que fue dada a conocer por La [Comisión de Justicia y Paz](https://www.justiciaypazcolombia.com/), la Fundación Forjando Futuros y el Instituto Popular de Capacitación este 24 de Julio da cuenta de una acción que es reiterada y que, de múltiples formas siguen intentando intimidar a los campesinos y reclamantes de tierras.l

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

**Gerardo Vega, director de la [Fundación Forjando Futuros](https://twitter.com/forjandofuturos),** relata que los campesinos que han retornado desde el año 2014 a sus tierras, en están siendo obligados a asistir a reuniones con los administradores de las fincas, quienes, acompañados de hombres armados están exigiendo la entrega del 50% de los predios que están siendo habitados y trabajados por familias, víctimas en el pasado de múltiples violaciones de derechos humanos, de desplazamientos y despojo.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

La situación se presenta en las veredas El Cedro y Tumaradocito, en la Hacienda Flor del Monte, que figura en cabeza de la familia Hernández de la Cuesta, quienes son reconocidas por ser opositores a los procesos de restitución de tierras. [(Le recomendamos leer: 66 empresas, entre ellas 4 bancos, tendrán responderle a reclamantes de tierras)](https://archivo.contagioradio.com/66-empresas-entre-ellas-4-bancos-tendran-responderle-a-reclamantes-de-tierras/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Según relatan las víctimas, los administradores, estarían acudiendo a vías ilegales para presionar nuevamente sobre la salida de los propietarios legítimos y legales de las tierras, algo que para organizaciones como la Comisión de Justicia y Paz y la Fundación Forjando Futuros **evidencia la responsabilidad de empresarios en dichas actuaciones en contra de las familias reclamantes.**

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Continúa el accionar contra reclamantes de tierras

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Defensores de DD.HH. han alertado que **este tipo de acciones, más allá de causar temor en las familias buscan desarticular los procesos de los reclamantes, por medio de la confusión y persecución**, por lo que se ha pedido a las instituciones correspondientes se garantice la vida de los reclamantes de tierras que han regresado a los corregimientos de Blanquiceth y Macondo con la esperanza de trabajar y vivir en aquellos territorios como sus legítimos propietarios.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Esta situación se da en medio de una campaña de desprestigio por parte de empresas bananeras en contra de víctimas de despojos y organizaciones defensoras de DD.HH. que acompañan procesos de restitución en el Bajo Atrato chocoano y el Urabá. [(Lea también: Denuncian que empresa bananera desarrolla campaña en contra de la Restitución de tierras)](https://archivo.contagioradio.com/denuncian-que-empresa-bananera-desarrolla-campana-en-contra-de-la-restitucion-de-tierras/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Cabe señalar que las familias reclamantes están protegidas por medidas cautelares emitidas por el **Juzgado Primero Civil del Circuito Especializado en Restitución de Tierras de Quibdó desde el 12 de diciembre del año 2014 mediante Auto 181;** además del Auto 035 del 21 de marzo de 2018, que ordena la suspensión de cualquier proceso legal contra estas comunidades, hasta que exista una sentencia que decida el destino de los predios.

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->
