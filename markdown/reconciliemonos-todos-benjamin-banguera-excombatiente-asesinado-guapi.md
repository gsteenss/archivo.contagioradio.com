Title: "Reconciliémonos todos" Benjamín Banguera, excombatiente asesinado en Guapi
Date: 2020-01-02 11:20
Author: CtgAdm
Category: DDHH, Nacional
Tags: ex combatientes FARC, FARC, Misión de Verificación de la ONU, proceso de paz
Slug: reconciliemonos-todos-benjamin-banguera-excombatiente-asesinado-guapi
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/01/Benjamín-Banguera-Rosales.mp4" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/01/WiJJnpdk7qYCCNuL.mp4" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/01/Benjamín-Banguera.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

[El **Partido FARC** denunció a través de su cuenta de twitter el asesinato de **Benjamín**]{.css-901oao .css-16my406 .r-1qd0xha .r-vw2c0b .r-ad9z0x .r-bcqeeo .r-qvutc0} **[Banguera]{.css-901oao .css-16my406 .r-1qd0xha .r-vw2c0b .r-ad9z0x .r-bcqeeo .r-qvutc0} [Rosales]{.css-901oao .css-16my406 .r-1qd0xha .r-vw2c0b .r-ad9z0x .r-bcqeeo .r-qvutc0}**[**,** excombatiente de esta guerrilla, el hecho sucedió en el municipio de Guapi, Cauca. ]{.css-901oao .css-16my406 .r-1qd0xha .r-ad9z0x .r-bcqeeo .r-qvutc0}

**Banguera** de 41 años de edad fue atacado por hombres armados cuando se movilizaba en motocicleta por el barrio la Esperanza en zona urbana de Guapi, con este asesinato aumenta a 178 excombatientes asesinados luego de la firma de paz y se convierte en el primer crimen de 2020. A través de la red social el Partido Farc reiteró al gobierno garantías para la vida de los hombres y mujeres que se comprometieron con la paz. Ver: [Con Wilson Parra, son 169 excombatientes de FARC asesinados desde la firma del Acuerdo de Paz](https://archivo.contagioradio.com/con-wilson-parra-son-169-excombatientes-de-farc-asesinados-desde-la-firma-del-acuerdo-de-paz/)

> ### Reconciliémonos todos y sembremos para que nuestra generación produzca una vida mejor..." [ Benjamín]{.css-901oao .css-16my406 .r-1qd0xha .r-vw2c0b .r-ad9z0x .r-bcqeeo .r-qvutc0} [Banguera]{.css-901oao .css-16my406 .r-1qd0xha .r-vw2c0b .r-ad9z0x .r-bcqeeo .r-qvutc0}

Con estas palabras se recuerda a través de redes sociales a Benjamin Banguera, como un hombre que luego de dejar las armas le apostó a construir la paz.

\[video width="848" height="480" mp4="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/01/Benjamín-Banguera-Rosales.mp4"\]\[/video\]

 

> Denunciamos el asesinato del reincorporado Benjamín Banguera Rosales en Guapi [\#Cauca](https://twitter.com/hashtag/Cauca?src=hash&ref_src=twsrc%5Etfw), fue asesinado con arma de fuego, de dos balazos. ¡Exigimos garantías![@IvanDuque](https://twitter.com/IvanDuque?ref_src=twsrc%5Etfw) [@infopresidencia](https://twitter.com/infopresidencia?ref_src=twsrc%5Etfw) [@EmilioJArchila](https://twitter.com/EmilioJArchila?ref_src=twsrc%5Etfw) [@PosconflictoCO](https://twitter.com/PosconflictoCO?ref_src=twsrc%5Etfw) [@MisionONUCol](https://twitter.com/MisionONUCol?ref_src=twsrc%5Etfw)
>
> — FARC (@PartidoFARC) [January 2, 2020](https://twitter.com/PartidoFARC/status/1212606356546629632?ref_src=twsrc%5Etfw)

<p>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
</p>
"En 2019 fueron asesinados 77 antiguos combatientes de las Fuerzas Armadas Revolucionarias de Colombia, frente a los 65 de 2018 y los 31 de 2017 " esto convierte al año que pasó en el más violento contra los excombatientes según el más reciente informe de la Misión de Verificación de la ONU.

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
