Title: Entrevista con Enrique Santiago: Justicia en el Proceso de Paz
Date: 2015-07-27 18:18
Category: Entrevistas, Judicial, Paz
Tags: enrique santiago, entrevista, FARC
Slug: entrevista-con-enrique-santiago-justicia-en-el-proceso-de-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/07/enrique-santiago-entrevista.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Contagio Radio 

##### 27 jul 2015 

Enrique Santiago, abogado internacional,  asesor de la mesa de conversaciones  en La Habana entre la guerrilla de las FARC- E-P y el Gobierno de Colombia hablo en el programa Otra Mirada sobre diversos temas que tienen cómo eje central la justicia.

\[audio mp3="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/07/Enrique-Santiago.mp3"\]\[/audio\]

###### [Enrique Santiago, abogado asesor de la Delegación de Paz de las FARC] 

Estas son algunas de sus consideraciones:

"El gobierno elude aplicar mecanismos incorporados en la Constitución, y exige convertir el proceso de paz en un proceso de juzgamiento contra solo una de las partes, y en esas condiciones es imposible un acuerdo político de paz".

"De lo que se trata, con perfecto encaje constitucional en el articulo 66 transitorio de la constitución de 1991, es establecer una comisión de esclarecimiento y de la verdad que pueda provocar un efecto de cosas juzgadas, respecto a aquellos colectivos que han perdurado en el conflicto, que comparecieran, explicaran la verdad de los hechos y sus acciones y ofrecieran por aquellas cuestiones que han tenido un grave perjuicio, ofrecieran la debida reparación concreta".

"La amnistía en el Derecho Internacional no solamente no esta prohibida, no hay ninguna norma internacional que prohiba las amnistías, máxime en procesos de paz, sino más bien al revés; lo que hay es una norma expresa en el artículo 6.5 del  protocolo adicional segundo de ginebra, que lo que recomienda es la aplicación de amplias amnistías una vez concluidos los conflictos armados".

"Lo que no puede pretender el Estado es ganar en la mesa de conversaciones, batallas que no ha sido capaz de ganar en el campo de batalla, ni en terreno estrictamente militar; pero ademas esto hay que orientarlo desde el punto de vista de una justicia prospectiva; donde lo que se trata es de garantizarle a las futuras generaciones de colombianos que el conflicto se cierra de una vez por todas y que se establezcan los suficientemente sólidos mecanismos de no repetición como para que las futuras generaciones colombianas no tengan que volver a padecer los horrores de un conflicto interno".

“Convertir los cilindros en un crimen de guerra y no consolidar la utilización sistemática de bombardeos indiscriminados no tiene mucho simetría ni explicación”

“El estado sigue sin reconocer el estatuto de prisioneros de guerra lo que pone en manifiesto la asimetría que maneja el gobierno”

“Sería más sencillo establecer como ha pasado en todos los procesos de paz, una amplia ley de amnistía indulto que eximiera de responsabilidades a las FARC.”

“No tiene sentido instruir un mecanismo extrajudicial de investigación y sanción, en la constitución colombiana y luego negarse a usarlo en el proceso de paz.”

“Se debe pensar que se trataba de una guerrilla campesina con pocos medios que se enfrentó durante muchos años al mayor aparato militar de América del sur con la ayuda de la mayor maquinaria de guerra del planeta que es Estados Unidos.”

“No es posible que en la comisión de la verdad señale a una persona con nombre y apellidos”

“Lo que no cabe para los militares es una amnistía previa por delitos políticos y conexos”

“En todos los 25 procesos de paz ha habido una amplia amnistía”

“El argumento de que es imposible avanzar a un modelo propio de justicia para la paz es falso”

“Representantes de empresas deben comparecer ante ese mecanismo extrajudicial y explicar como se organizó el paramilitarismo, quien financio el paramilitarismo, que acuerdos había con políticos… para que la sociedad sepa cómo sucedieron masacres durante días enteros, sin que se registrara un solo combate entre FFMM y paramilitares, alguien tendrá que explicar eso en algún momento.”

“Las condenas deben ser indultadas si alcanzan el ejercicio del derecho a la rebelión, condenas de militares no.”

“La cárcel ni rehabilita ni resocializa, se debe tratar de tomar medidas de restauración del daño con resultados positivos inmediatos que mejoren las condiciones de vida de las víctimas.”
