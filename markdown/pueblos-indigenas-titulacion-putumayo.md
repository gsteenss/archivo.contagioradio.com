Title: Pueblos Kamëntšá e Inga celebran titulación de sus territorios
Date: 2016-12-26 20:28
Category: DDHH, Otra Mirada
Tags: indígenas, Putumayo, titulación
Slug: pueblos-indigenas-titulacion-putumayo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/12/articulo-4.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### Foto: Equipo pueblos Kamëntšá e Inga 

##### 26 Dic 2016 

Luego de una lucha de más de seis años frente al Estado colombiano y más de cien de desarraigo, indígenas de los pueblos Kamëntšá e Inga del Alto Putumayo, puerta de entrada a la Amazonía colombiana, recibieron los títulos de sus territorios que abarcan una extensión de cerca 75 mil hectáreas, una ampliación y la constitución de tres resguardos indígenas.

El reconocimiento territorial incluye cuatro resguardos del Valle de Sibundoy: la ampliación del Resguardo Indígena Kamëntšá Biya y la constitución de los resguardos Kamëntšá Inga de San Francisco, Inga de San Andrés e Inga de Colón, haciendo efectivo el reconocimiento y reafirmación de sus derechos territoriales.

Para las comunidades indígenas, las áreas donde se constituyen los actuales resguardos, se encuentran en zonas de gran importancia ecológica y espiritual, en estos se ubican sitios sagrados donde se genera la vida y habitan los espíritus de la madre tierra "Tsbatsan mamá” para el pueblo Kamëntšá y “Nukanchipa Alpa”para el pueblo Inga.

El trabajo para lograr la legalización de las posesiones tradicionales en calidad de resguardos indígenas, se realizó de manera mancomunada entre los cabildos indígenas y sus cooperantes, quienes lograron la inclusión de inclusión del conocimiento tradicional del territorio en los estudios técnicos, a Agencia Nacional de Tierras y la Dirección de Asuntos Indígenas del Ministerio del Interior.

De acuerdo con los estudios ambientales de la región, los territorio indígenas hacen parte de una importante zona de conservación de la biodiversidad mundial y sus bosques se constituyen en reservorios importantes para la vida. Abarca importantes montañas y páramos como el de Bordoncillo, donde nacen y se regulan naturalmente las aguas, para el abastecimiento de los acueductos de las comunidades y para su contribución a la gran cuenca del Amazonas.

Para las comunidades, en riesgo de desaparecer por problemáticas relacionadas con el conflicto armado e intereses económicos extractivistas por los cuales les fueron vulnerados sus derechos fundamentales, resulta trascendental para su supervivencia física y cultural el recuperar la propiedad sobre lo que ancestralmente les pertenece. Le puede interesar: [Se agudiza la ola de amenazas y hostigamientos en el Putumayo](https://archivo.contagioradio.com/se-agudiza-ola-de-amenazas-y-hostigamientos-en-el-putumayo/).

Las comunidades hacen un llamado para que los gobiernos del mundo reconozcan sus territorios y a los movimientos sociales para que apoyen estas luchas milenarias, que son por el cuidado y la protección de la madre tierra. Se espera que el proceso finalice en 2017 con la constitución de los resguardos de Santiago y San Pedro ante la Agencia Nacional de Tierras.

------------------------------------------------------------------------

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
