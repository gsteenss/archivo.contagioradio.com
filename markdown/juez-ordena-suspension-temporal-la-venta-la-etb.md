Title: Juez ordena suspensión temporal de la venta de la ETB
Date: 2017-05-12 11:44
Category: Movilización, Nacional
Tags: Alcaldía de Bogotá, Enrique Peñalosa, Venta de la ETB
Slug: juez-ordena-suspension-temporal-la-venta-la-etb
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/05/cacerolazo-etb-16.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [12 May 2017] 

A través de una sentencia el Juzgado Administrativo de la Seccional Primera de Bogotá, dictaminó la suspensión temporal del procedimiento o actuación administrativa, del **proceso de venta de las acciones de la Empresa Pública de Telecomunicaciones de Bogotá**.

La tutela fue  interpuesta por la bancada del Polo ante el Concejo de Bogotá, en cabeza del concejal Manuel Sarmiento,  Sintrateléfonos y ATELCA, el pasado 4 de mayo, **solicitando la suspensión de la venta de acciones de la ETB**, y en concreto la suspensión inmediata del inicio de la etapa de enajencación de la empresa. Le puede interesar: ["Comenzó la venta de la ETB: La gallina de los huevo de oro"](https://archivo.contagioradio.com/hoy-comienza-la-venta-de-las-acciones-de-la-etb/)

### **Los 4 argumentos por los que se detiene venta de la ETB ** 

De acuerdo con el concejal Sarmiento, la demanda se había interpuesto bajo 3 argumentos de forma: el primero es que Peñalosa, no podía incluir la privatización de la ETB en el plan de desarrollo porque se violaba un proceso de la unidad de materia, **es decir que se presentó como "un mico**" señaló Sarmiento.

El segundo argumento indicó que** Peñalosa no presentó argumentos serios que justificaran la justificación de la ETB**, que es entendida como una falsa motivación y que estaba basado en suposiciones falsas como las cifras sobre el número de afiliados de la ETB en el programa de fibra óptica. Le puede interesar: ["No claudicará la defensa de la ETB: Sindicatos"](https://archivo.contagioradio.com/alcaldia-de-penalosa-no-escucha-a-los-ciudadanos-atelca/)

En tercer lugar se encuentra una violación al reglamento interno al Concejo, debido a que a los concejales del Polo Democrático **no se les permitió participar en la discusión de los artículos que autorizaban la venta de la empresa** y por último se encuentra la aprobación del plan de desarrollo que fue hecha por la Comisión del plan y no por la Comisión de Gobierno, autoridad que tendría que haber cumplido esta función.

El paso siguiente es que la administración de Peñalosa inicie el proceso de apelación, sin embargo se estima que dentro de un mes, **el Tribunal Administrativo de Bogotá ya pueda tomar una decisión definitiva sobre la venta o no de la empresa ETB**.

El próximo martes, ciudadanos se reunirán a las afueras del Consejo Nacional Electoral, para protestar frente a la posible intervención de esta institución en el proceso de revocatoria que se adelanta contra el alcalde de Bogotá **y su intención de continuar con el proceso de venta de la ETB. **Le puede interesar: ["Administración de Peñalosa hace oídos sordos para detener la venta de la ETB" ](https://archivo.contagioradio.com/administracion-penalosa-hace-oidos-sordos-para-detener-la-venta-de-la-etb/)

<iframe id="audio_18655230" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_18655230_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

######  Reciba toda la información de Contagio Radio en [[su correo]
