Title: Colombia "perdería por w" de no comparecer ante La Haya
Date: 2016-03-18 13:57
Category: Nacional
Tags: César Torres Del Río, corte internacional de la haya, litigio nicaragua colombia
Slug: no-hay-posibilidades-de-salir-con-decencia-de-este-conflicto
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/03/Santos_.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Presidencia ] 

<iframe src="http://co.ivoox.com/es/player_ek_10856638_2_1.html?data=kpWll5uad5mhhpywj5WUaZS1kZWah5yncZGhhpywj5WRaZi3jpWah5yncYamk7Pcjc3FvYzk0NjWxM7QrcXVxcrgjcnJb9TVzc7fjcjTsozYxsjS0MjNpYzYxpDS1dnJb8Tjz5KSmaiRh9Di1cbUy9SPlsLYytSYj4qbh46o&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [César Torres del Río, analista] 

###### [18 Mar 2016 ] 

Frente al fallo en el que la Corte Internacional de Justicia CIJ, con sede en La Haya, se declaró competente para resolver el diferendo limítrofe con Nicaragua, el presidente Juan Manuel Santos aseguró que el Estado colombiano no comparecerá ante esta instancia, decisión que según el analista político Cesar Torres es "la tapa de lo ridículo" que "no solamente es inmaduro" sino que representa el **desconocimiento de las relaciones entre los Estados y de las decisiones de las altas cortes internacionales** "que entre otras cosas, están gobernadas y dominadas por las grandes potencias imperialistas".

De acuerdo con Torres, pese a que la CIJ "actúo en derecho", desacierta con su pretensión de exigirle al Estado colombiano que cumpla con la Convención del Mar pues el país no la firmó. No obstante las recientes decisiones de la Corte, el estado del litigio con Nicarugua y la postura del gobierno "muestran el **fracaso total de las relaciones internacionales y de la política exterior de Colombia**", que a causa de "la politiquería estúpida de la cancillería y del gobierno" ha cambiado constantemente de representantes mientras que Nicaragua ha contado con el mismo en los últimos 35 años.

"Quienes estaban en La Haya no supieron argumentar bien para que Colombia evitara la declaratoria de competencia de la CIJ", asegura el analista e insiste en que **no existen posibilidades de negociar directamente con Nicaragua**, a menos que el Estado colombiano acepte las determinaciones de la Corte, postura que el Presidente Santos anunció no asumir, lo que limita las posibilidades "de salir con decencia de este conflicto" y da mayores argumentos para que el fallo sea declarado a favor de Nicaragua, **"una pérdida por w"**, como asevera Torres.

El analista concluye asegurando que el **"despliegue mediático" que Juan Manuel Santos ha puesto en marcha para "defender la soberanía territorial y los derechos de los raizales"**, es el "colmo del cinismo" frente al que la ciudadanía debe asumir una postura crítica que "no le siga el juego" sino que exija atención real a esta población.

##### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)]
