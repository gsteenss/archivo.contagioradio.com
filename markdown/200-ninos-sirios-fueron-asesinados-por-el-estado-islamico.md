Title: 200 niños sirios fueron ejecutados por el Estado Islámico
Date: 2015-11-10 16:03
Category: El mundo, Otra Mirada
Tags: Estado Islámico, Estado Islámico ejecutó a 200 niños sirios, la provincia de Alepo, localidad de al-Sheikh y Tal Ahmar, Siria
Slug: 200-ninos-sirios-fueron-asesinados-por-el-estado-islamico
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/11/isis.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### foto:posta.com 

###### [10 nov 2015]

Mediante un video, fuentes británicas confirmaron que el grupo yihadista Estado Islámico (ISIS) ejecutó a 200 niños sirios que se habrían negado a integrar sus filas.

Según el  diario británico Mirror, los hechos se evidenciaron por medio de un video que circula en redes en sociales difundido por un grupo antiyihadista de Yemen. ** **En el registro audiovisual se ven varios menores de edad obligados a acostarse boca abajo sobre el suelo, luego de esto un integrante del Estado Islámico empieza a disparar. Las autoridades sirias no hay confirmado la autenticidad del video.

Adicionalmente fuentes militares confirmaron que las tropas sirias recuperaron por completo una importante carretera en la provincia de Alepo, al parecer esta carretera será abierta para el uso de civiles. A su vez se han realizado operaciones aéreas por el Gobierno dejando daños materiales y bajas en las filas del autodenominado Estado Islámico en la localidad de al-Sheikh y Tal Ahmar al noreste de Alepo.
