Title: "Hay que erradicar el lenguaje discriminatorio contra comunidad LGTBI"
Date: 2017-04-17 16:17
Category: LGBTI, Nacional
Tags: Arrázola, Caribe Afirmativo, lgtbi
Slug: hay-que-erradicar-el-lenguaje-discriminatorio-contra-comunidad-lgtbi
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/04/Rainbow_flag_and_blue_skies-e1492470098524.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Ludovic Bertron] 

###### [17 Abr 2017] 

La organización defensora de derechos humanos Caribe Afirmativo instauró desde el pasado 23 de marzo una demanda contra el Pastor Migue Arrázola, de la Iglesia Ríos de Vida, en Cartagena, **por hostigar a la comunidad LGTBI durante los sermones, con el lenguaje que usa en sus actos religiosos, incitando a expresiones de odio contra esta comunidad.**

La demanda, de acuerdo con el director de la organización, Wilson Castañeda, está amparada bajo la ley 1482 o Antidiscriminatoria y se fundamenta en vídeos en que el **Pastor se refiere a la comunidad LGTBI como “maricas” o “mariquitas empolvadas”** durante un culto: “Maricas, mariquitas empolvadas, eso es lo que son, unos maricas, el roscograma, roscón en cacacho es marica, también como sabes que no te puedo pegar, que bien te mereces un par de garnatadas míos y yo tengo uno manes tabluos aquí que yo te puedo hacer la vuelta".

Para Castañeda ese discurso está cargado de violencia “incitar a la discriminación configura el delito de hostigamiento, pero es agravado debido a la alta popularidad y reconocimiento que tiene el Pastor y que no solamente la participación en sus cultos es masiva, **sino que sus mensajes son masivos en redes sociales y pueden incitar a expresiones de odio hacia personas LGTBI**”.

Castañeda enfatizó en que “son respetuosos de la libertad de culto y de que se permita que cualquier iglesia pueda realizar sus asambleas y reuniones con garantías” y que el llamado de atención, en general a las iglesias, **es al uso del lenguaje violento que debe ser erradicado.** Le puede interesar:["405 contra personas LGTBI en los últimos 4 años"](https://archivo.contagioradio.com/405-homicidios-contra-personas-lgbt/)

La Corte Constitucional, en un reciente fallo, indicó que el uso de palabras como “mariquita” **podrá señalarse como discriminativo, cuando el contexto que lo acompañe sea violento y tenga la intensión de ser ofensivo en razón de condiciones personales**. Le podría interesar: ["Asesinatos de personas LGTBI han quedado en la impunidad"](https://archivo.contagioradio.com/asesinatos-de-personas-lgbti-han-quedado-en-la-impunidad/)

Para desescalar el lenguaje violento contra la comunidad LGTBI, Caribe Afirmativo propone que se generen procesos pedagógicos no solo para sancionar la discriminación, sino para prevenirla y **comenzar a darle más sentido a la construcción de una sociedad diversa, diferente y que respete al otro**.

<iframe id="audio_18185042" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_18185042_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio]{.s1}](http://bit.ly/1ICYhVU)[.]{.s1}
