Title: Nueva directiva de MinDefensa no garantiza desmonte paramilitar
Date: 2016-05-06 13:01
Category: DDHH, Entrevistas
Tags: dialogos de paz, INDEPAZ, ministerio de defensa, paramilitares
Slug: nueva-directiva-de-mindefensa-no-garantiza-desmonte-paramilitar
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/05/Ejército.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Zona Cero ] 

###### [6 Mayo 2016]

De acuerdo con Camilo González Posso, director de INDEPAZ, la directiva de MinDefensa en la que se establecen los lineamientos para enfrentar a los ahora denominados Grupos Armados Organizados, anteriormente Bandas Criminales BACRIM, no ataca los elementos que han permitido la supervivencia del paramilitarismo: la **complicidad del Estado y sus estrechos vínculos con los poderes económicos y políticos regionales**.

La directiva determina que las Fuerzas Armadas, la Policía y el Ejército tienen la potestad de bombardear las zonas en las que estos grupos hacen presencia, como la fórmula efectiva para neutralizarlos; sin embargo, Posso afirma que **desmontar estos grupos paramilitares implica atacar las relaciones de poder político y económico** a través de las que se articulan.

Lo que sucedió con las AUC fue que se desmantelaron parcialmente, se entregaron armas, pero quedaron vivos los tentáculos políticos y económicos que han permitido que hoy **7 alcaldías del Urabá estén controladas por el paramilitarismo** y que en 266 municipios el poder político se articule abiertamente con el fenómeno, según alertó la Registraduría en las elecciones regionales del año pasado.

Posso asegura que esta directiva demuestra que el aparato estatal se acostumbró a acoger el combate a la insurgencia "como modelo y como estructura", **proponer bombardeos para atacar el problema sólo generará afectaciones para las comunidades**, lo que debe hacerse es intervenir los gobiernos corruptos heredados de la parapolítica, la minería ilegal, el contrabando de gasolina, el narcotráfico de cocaína, y combinar esta intervención con mecanismos de sometimiento a la justicia.

El Ministerio de Defensa asegura que "en Colombia no hay paramilitares, hay mafia y hay crimen organizado (...) estas estructuras no tienen relación con la Fuerza Pública, no son financiados por la sociedad civil y lo único que buscan es el lucro económico"; sin embargo, los [[informes elaborados por distintas organizaciones](https://archivo.contagioradio.com/paramilitares-hacen-presencia-en-149-municipios-de-colombia-indepaz/)] evidencian que los vínculos son estrechos y que es justamente [[ese elemento el que ha garantizado su pervivencia](https://archivo.contagioradio.com/es-fantasioso-asegurar-que-el-paramilitarismo-no-sigue-vigente/)].

Luis Carlos Villegas, Ministro de Defensa, aseguró que **con financiamiento de empresas bananeras**, señaladas por violar derechos humanos y [[articularse con grupos paramilitares](https://archivo.contagioradio.com/control-paramilitar-y-empresarial-impide-restitucion-de-tierras-en-curvarado-y-jiguamiando/)], **se implementarán 300 puestos de patrullaje en Urabá**, y se creará un nuevo Gaula para la región. Según Posso esta es una muy mala señal para la implementación de [los acuerdos de paz](https://archivo.contagioradio.com/recta-final-proceso-de-paz/) y que además pone en duda la intención de MinDefensa de proteger a la población.

<iframe src="http://co.ivoox.com/es/player_ej_11436599_2_1.html?data=kpahlZuZfZqhhpywj5aUaZS1kpaah5yncZOhhpywj5WRaZi3jpWah5yncaTVzs7Z0ZCrs8%2Fuhqigh6aVsMbnjJKYq9PIqdHV25KSmaiRh9Di1cbUy9SPlsLYytSYj4qbh46o&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)]]
