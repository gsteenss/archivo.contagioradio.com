Title: "Se teme nueva ola de mal llamada "limpieza social" con habitantes del Bronx" Alirio Uribe
Date: 2016-08-22 13:14
Category: Nacional
Tags: Desalojo de habitantes de calle, Habitantes de calle
Slug: se-teme-nueva-ola-de-mal-llamada-limpieza-social-con-habitantes-del-bronx-alirio-uribe
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/08/habitantes-de-calle-5-e1471886708387.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: hsbnoticias 

###### 22 Ago 2016 

Continúa la problemática de los habitantes de calle en Bogotá luego de los bloqueos registrados durante el fin de semana a la altura de la carrera 30 con calle 6, convocados por ciudadanos, quienes aseguran que se está agudizando la delincuencia y  han tenido que cerrar sus negocios por la presencia de los habitantes de calle.

De acuerdo con el representante a la Cámara Alirio Uribe, el temor es que se genere una nueva ola de la mal llamada "limpieza social" por el conocimiento que tiene sobre reuniones de policías y comerciantes, que han conformado equipos de seguridad diciendo que hay que volver a esas prácticas de asesinar a estas personas que rondan sus barrios, [producto de la política de desalojo del Bronx, puesta en marcha por el alcalde Peñalosa](https://archivo.contagioradio.com/el-bronx-una-crisis-humanitaria-que-le-esta-quedando-grande-al-alcalde/) que está propiciando la intolerancia en la ciudadanía y que persiste con los desalojos por parte de la Fuerza Pública sin que se establezca un plan de contingencia.

Frente a estos hechos, Uribe afirma que es necesario se instale el campamento humanitario para los habitantes de calle con el fin de que no se vulneren sus derechos, esto debido a que según el representante la capacidad de los albergues es de 3.000 personas y en la actualidad existen más de 14.000 habitantes de calle en la capital, [desbordando el espacio de estos lugares](https://archivo.contagioradio.com/60-millones-por-metro-cuadrado-el-negocio-tras-la-crisis-humanitaria-del-bronx/).

Con este contexto, Alirio Uribe afirma que se están llevando a cabo las investigaciones para constatar si hubo o no abusos de autoridad, y si podría tipificarse desplazamiento forzado. De igual forma, expone que es necesario se lleven a cabo procesos de tolerancia de manera permanentemente con la ciudadanía, para que se comprenda que situaciones como esta solo tienen solución en el marco de los derechos humanos.

<iframe src="http://co.ivoox.com/es/player_ej_12626793_2_1.html?data=kpejlJubfZShhpywj5aUaZS1k5eah5yncZOhhpywj5WRaZi3jpWah5yncaLgytfW0ZC5tsrWxoqfpZC2qdHmxtjS0NnFstXZjMaYzsaPh4a3lIquk9LFtsKhhpywj6jTstXVyM7cjbfFqMrjjJKSmaiReA..&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
