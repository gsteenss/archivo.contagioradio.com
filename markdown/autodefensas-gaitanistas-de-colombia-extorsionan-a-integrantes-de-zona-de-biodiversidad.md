Title: Autodefensas Gaitanistas de Colombia extorsionan a integrantes de Zona de Biodiversidad
Date: 2018-01-05 20:32
Category: DDHH, Nacional
Tags: Autodefensas Gaitanistas de Colombia, Lider social, Pedeguita Mancilla
Slug: autodefensas-gaitanistas-de-colombia-extorsionan-a-integrantes-de-zona-de-biodiversidad
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/01/Paramilitares.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: La Razón] 

###### [05 Ene 2017]

Habitantes de la comunidad del Consejo Comunitarios de Pedeguita y Manicilla denunciaron que en horas de la mañana integrantes de las autodenominadas Autodefensas Gaitanistas de Colombia, vestidos de civil, ingresaron a la Zona de Biodiversidad de Jorge Mercado, **amenazándolo con que debía pagar una vacuna si no quería que se llevaran el ganado**.

El líder de restitución de tierras, se negó al pago de la extorsión, razón por la cual los integrantes de las AGC **lo amenazan advirtiéndole que en los próximos días regresarán al territorio para llevarse las cabezas de ganado del líder social**. (Le puede interesar: ["Amenazan de muerte a líder social y escoltas en Río Sucio, Chocó"](https://archivo.contagioradio.com/amenazan-de-muerte-a-lider-social-y-escoltas-en-riosucio-choco/))

Una situación similar se presentó el pasado 4 de enero, cuando dos integrantes del grupo neoparamilitar de las AGC ingresaron a predios comunitarios de Caño Manso, Territorio Colectivo de Curvaradó y extorsionaros a los integrantes del Consejo Comunitario Alfonso Falla y Francisco Pérez, **también increpándolos, con llevarse los semovientes de no pagar la vacuna**.

En ese sentido la comunidad señaló que, a pesar del compromiso de garantizar los derechos a la vida, a la libertad y el disfrute de la propiedad por parte del Estado a líderes y sus familias, **la Fuerza Pública ha sido incapaz de desestructurar las operaciones de tipo neoparamilitar**.

Además señalaron que los empresarios ocupantes de mala fe y sus estructuras armadas continúan asegurando el despojo e imposibilitando la supervivencia de los integrantes de los consejos comunitarios. En la actualidad más de siete líderes comunitarios se encuentran desplazados por las amenazas y las operaciones de tipo paramilitar.

###### Reciba toda la información de Contagio Radio en [[su correo]
