Title: Las demandas de organizaciones defensoras de DD.HH. al Gobierno, en el Paro Nacional
Date: 2019-12-12 18:18
Author: CtgAdm
Category: DDHH, Paro Nacional
Tags: Defensa de DD.HH., Duque, Paro Nacional, Política de Defensa y Seguridad
Slug: las-demandas-de-organizaciones-defensoras-de-dd-hh-al-gobierno-en-el-paro-nacional
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/67669305_1155684827951256_9199625085365780480_o-e1576192300112.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Andrés Zea  
] 

Recientemente organizaciones defensoras de derechos humanos se sumaron al Comité Nacional de Paro, para nutrir la reflexión sobre los dos ejes que están en la agenda de negociación y tienen que ver con la implementación del Acuerdo de Paz y de derechos. Según la abogada Olga Silva, vocera de las plataformas de derechos humanos, su apoyo busca modificar tres elementos fundamentales: La política de defensa y seguridad, las garantías para la defensa de derechos y el cumplimiento de las recomendaciones internacionales.

### **Es una oportunidad para la articulación que puede derivar en transformación de las condiciones de vida** 

Según explicó la abogada, las demandas del Comité Nacional de Paro se resumen en 13 ejes que tienen que ver con la política de derechos humanos, la paz, el incumplimiento de acuerdos establecidos con diferentes sectores y lo que diferentes gremios han denominado 'el paquetazo' económico de Duque. Es así, como plataformas de derechos humanos se sumaron a los ejes 4 y 10, relacionados con los temas de garantía de derechos y paz.

"Vemos esto como un escenario de oportunidad para la articulación, y ojala permita la transformación que derive en la transformación de las condiciones sociales, políticas, económicas y culturales", señaló la abogada, y añadió que esperan por parte del Gobierno una actitud abierta y de negociación que "materialice la voluntad de generar cambios estructurales que demanda la gente en las calles". (Le puede interesar:["Al pliego del Paro Nacional se suma la negociación del salario mínimo"](https://archivo.contagioradio.com/al-pliego-del-paro-nacional-se-suma-la-negociacion-del-salario-minimo/))

### **Las reflexiones que proponen las organizaciones defensoras de DD.HH.** 

En primer lugar, las organizaciones quieren nutrir la reflexión en torno a la política de defensa y seguridad, a raíz de las violaciones de derechos humanos que se han visto en aumento con el gobierno Duque. En ese sentido, Silva aseguró que es necesario cambiar la doctrina con la que operan las Fuerzas Armadas, porque la actual doctrina que apela al enemigo interno es permisiva, fomenta infracciones al Derecho Internacional Humanitario (DIH) y "permite que la protesta social sea reprimida violentamente".

Para la abogada, replantear esta doctrina implica también pensar en el desmonte del Escuadrón Móvil Antidisturbios (ESMAD); avanzar en el desmantelamiento de estructuras paramilitares, "conforme al mandato de la Comisión Nacional de Garantías de Seguridad establecida en el Acuerdo de Paz"; y depurar la fuerza pública así como instituciones estatales, porque las mismas no deberían estar dirigidas por personas acusadas de violar los DD.HH., como es el caso del Ejército, comandado por el general [Nicasio Martínez](https://archivo.contagioradio.com/deberia-investigado-mayor-general-nicacio-martinez/).

La segunda reflexión, orientada a proteger la vida de quienes defienden los derechos humanos, busca que cese la impunidad en los casos de amenazas, ataques y asesinatos de líderes y lideresas sociales, garantizando así que haya garantías de no repetición, y condiciones para seguir en esta labor. De igual forma, las plataformas buscan que se sanciones a quienes son responsables de grandes violaciones a los derechos humanos. (Le puede interesar:["La defensa de la vida, un punto central del Paro Nacional"](https://archivo.contagioradio.com/la-defensa-de-la-vida-un-punto-central-del-paro-nacional/))

En cuanto al último elemento, la Abogada destacó que se deben acatar las recomendaciones de la Comunidad Internacional, destacando especialmente las que se han hecho para proteger la vida de defensores de derechos humanos. (Le puede interesar: ["Los tres puntos de organizaciones y procesos ambientales en el paro nacional"](https://archivo.contagioradio.com/los-tres-puntos-de-organizaciones-y-procesos-ambientales-en-el-paro-nacional/))

### **La gente no va a dejar de estar en las calles porque hay inconformidad, y ese descontento social no va a parar** 

Respecto a la capacidad de movilización que seguirán teniendo las organizaciones para sostener el paro nacional en temporada navideña, la abogada fue enfática en afirmar que "la gente no va a dejar de estar en las calles porque hay inconformidad, y ese descontento social no va a parar". En su visión, a las ya mencionadas razones del paro se han sumado otras, como la reforma tributaria que hace que la navidad llegue con más angustias para los colombianos.

Por esta razón, sostuvo que la situación en las calles no cambiará si el Gobierno no asume una actitud responsable respecto a los sufrimientos del pueblo colombiano; "el ejercicio de paro no es exclusivamente de quienes están en el Comando del Paro, es una expresión que está en las calles", concluyó. (Le puede interesar:["Delegaciones indígenas llegan a Bogotá para fortalecer el Paro Nacional"](https://archivo.contagioradio.com/delegaciones-indigenas-llegan-a-bogota-para-fortalecer-el-paro-nacional/))

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe><iframe id="audio_45516610" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_45516610_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
