Title: Los siete argumentos más irracionales en contra de la Adopción Homoparental
Date: 2015-05-02 09:19
Author: CtgAdm
Category: Escritora, Opinion
Tags: adopción homoparetal, Adopción igualitaria, colombia, Defensoría del Pueblo, homosexuales, LGBTI, Universidad de Medellin
Slug: los-siete-argumentos-mas-irracionales-en-contra-de-la-adopcion-homoparental
Status: published

###### Foto: Colombia Diversa 

**Por [Escritora Encuebierta ](https://archivo.contagioradio.com/escritora-encubierta/)**

A la hora de hablar sobre la Adopción Igualitaria, quienes están en contra utilizan una variedad de argumentos absurdos basados en prejuicios y estereotipos sin sentido que, a la larga, lo único que hacen es dejar a más de **10 mil niños** sin hogar. A continuación, hablaré sobre ellos.

En el top 7 de los argumentos más estúpidos alguna vez usados, encabezando la lista se encuentra ''tener padres homosexuales *hará* que el menor sea homosexual''. A pesar de que muchas instituciones se han encargado en desmentir esto, la gente todavía sigue recurriendo a este mito. Utilicemos un poco de lógica: si la orientación sexual de los padres afectara en la orientación sexual de los hijos, entonces no habrían homosexuales en el mundo. Poniéndome de ejemplo, yo fui criada en una familia heterosexual, conservadora y 100% católica, sin embargo soy lesbiana. Señores**, la orientación sexual no es algo que se aprende ni que se elige, es algo con lo que se nace.**

El segundo argumento más usado es ''los niños deben crecer con una *figura paterna* y una *figura* *materna*''. **En Colombia la adopción monoparental está permitida**, entonces rigiéndonos por este argumento, ¿qué pasa con las madres y padres solteros que han adoptado?, ¿debemos llamar al ICBF y exigir que les revoquen la custodia de sus hijos? Además, ¿es que acaso los niños en espera de ser adoptados reciben alguna figura materna o paterna?

Anexo a lo anterior, se encuentra ''los niños necesitan de una *madre flexible y amorosa,* y un *padre disciplinado y un poco más severo''*. **Los roles del género son sólo una construcción social**, la misma que origina la absurda creencia de que las mujeres deben encargarse de los labores del hogar y los hombres deben trabajar y traer el dinero a la casa. Estamos en el siglo XXI, estas concepciones carecen de validez.

Entre más estereotipos está ''los homosexuales son promiscuos e inestables''. ¿Es que acaso en la comunidad heterosexual no se encuentran casos de infidelidad? ¿No hay ni un solo divorcio? **Características como esas no van ligadas a la sexualidad o al género, van ligadas la personalidad.**

Otro argumento muy desgastado es ''a los niños criados por homosexuales les hacen más bullying'' y por consecuente, '' los niños tienden a ser *más depresivos*''. Cada vez que escucho esto, no puedo evitar pensar en la frase ''vivimos en una sociedad que enseña a las mujeres a cuidarse de no ser violadas en vez de enseñar a los hombre a no violar''. Lo mismo sucede en la adopción homoparental: se le negará a un niño el derecho de tener una familia sólo para evitar que le hagan bullying en vez de enseñar a las otras personas a no hacer bullying. **Ningún tipo de bullying debe ser aceptado.** Esto no es solucionar el problema, esto es huir temporalmente de él.

Y para finalizar con el top, no podía faltar el muy temido ''según la *Biblia la homosexualidad es una aberración*, no podemos darle niños a unos *enfermos, depravados y pecadores*''. La Biblia es un libro lleno de pasajes incitadores al odio, la violencia, el machismo, el racismo y la homofobia, ningún religioso puede negarlo. Sin embargo, como no estoy aquí para criticar a la religión, sólo quiero recordar que **Colombia es un estado laico**, por lo tanto no se puede basar en ideologías religiosas para tomar una decisión que afectara a todos los colombianos.

Quiero aclarar que mi intención con todo esto no es santificar a los homosexuales sobre los heterosexuales. Mi intención es hacerles entender a todos los lectores que los homosexuales pueden ser tan buenos o tan malos padres como lo pueden ser los heterosexuales. La orientación sexual no debe ser un impedimento para llevar a cabo una adopción y no lo digo sólo yo, lo dicen miles de colombianos y una gran variedad de instituciones con fundamentos como **el Ministerio de Salud, la Defensoría del Pueblo, el mismísimo ICBF, la Fiscalía General de la Nación, la Comisión Colombiana de Juristas, Colombia Diversa, DeJusticia, la Asociación de Abogados de NY, Profamilia, la Universidad del Norte, la Universidad de California, la Universidad Libre, la Universidad Javeriana, la Universidad Externado, la Universidad de Medellin y American University.**

###### Conceptos de Instituciones a favor de la Adopción Igualitaria, un valioso aporte por Colombia Diversa: <https://www.dropbox.com/sh/ntq10blkimql4yd/AAAn6mM2W-hcgYvHoGgsS2IAa?dl=0> 

###### Países a favor de la adopción igualitaria: <http://www.eltiempo.com/mundo/latinoamerica/adopcion-homoparental-paises-en-el-mundo-que-permiten-la-adopcion/15268775> 
