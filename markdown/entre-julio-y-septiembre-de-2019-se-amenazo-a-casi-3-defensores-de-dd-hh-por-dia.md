Title: Entre julio y septiembre de 2019 aumentaron un 43% las agresiones a defensores de DD.HH. respecto al año pasado
Date: 2019-12-04 11:41
Author: CtgAdm
Category: DDHH, Nacional
Tags: Agresiones contra líderes sociales, autodefensas, DDHH, Líderes indígenas, paramilitares, SIADDHH
Slug: entre-julio-y-septiembre-de-2019-se-amenazo-a-casi-3-defensores-de-dd-hh-por-dia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/10jpg.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

[Las agresiones contra defensores y defensoras de derechos, recogidas por el  **Sistema de Información sobre Agresiones contra Personas Defensoras de Derechos Humanos en Colombia (SIADDHH)** en el informe trimestral presentado por el programa Somos Defensores, revela que entre julio y septiembre de 2019,  pese a que existió una reducción en los homicidios, se registró un incremento en las amenazas contra  liderazgos sociales en el territorio nacional. ]

[El informe registra 183 hechos de violencia contra defensoras y defensores de derechos humanos en estos tres meses, siendo el mes de agosto — con 66 casos— el que presentó más agresiones, seguido de julio con 59 y septiembre con 58.][(Le puede interesar: Agresiones contra líderes sociales incrementaron un 49% en el primer semestre del 2019)](https://archivo.contagioradio.com/agresiones-contra-lideres-sociales-incrementaron-un-49-en-el-primer-semestre-del-2019/)

[Respecto a las agresiones por sexo, los hombres fueron los más afectados, reportándose 134 agresiones (73%) contra ellos y 49 (27%) contra mujeres. Las agresiones están conformadas por asesinatos (14%), atentados (5%), robo de información (2%) y amenazas individuales (79%), esta última la de mayor cuantía. ]

### **Existe un incremento en materia de amenazas a líderes en el territorio nacional** 

[Comparando la información obtenida en 2018, se pudo notar un incremento notable en materia de amenazas individuales. Se "pasó de 128 agresiones en 2018 a 183 casos en 2019”. Es decir, “**en el tercer trimestre del 2019 en promedio cada día fueron amenazadas 2,7 personas defensoras de derechos humanos”.**]

[Este tipo de amenazas se presentan de diversas formas, ya sea mensajes de texto en WhatsApp, llamadas telefónicas, hostigamientos, correos electrónicos o por panfletos, siendo estos últimos los más comunes —abarcando un 48% del total—. ]

\[caption id="attachment\_77476" align="aligncenter" width="823"\]![Gráfico cifras de agresión SIADDHH a líderes sociales](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/Tipo-de-agresión-SIADDHH.jpg){.wp-image-77476 .size-full width="823" height="517"} Imagen: Informe SIADDHH.\[/caption\]

### **Los responsables de las agresiones ** 

[Del total de amenazas realizadas en este lapso, 145 fueron individuales, siendo 41 de ellas cometidas por las autodenominadas Autodefensas Gaitanistas de Colombia - AGC, 37 por las Águilas Negras, 25 por las disidencias de las FARC-EP, 24 por desconocidos, 10 por el grupo de los Caparros, 5 por el ELN y 3 por el grupo paramilitar los Pachencas. ]

[En materia de asesinatos, hubo una reducción en comparación con el trimestre de 2018, ya que pasaron de 26 a 23. Es importante resaltar que “los líderes indígenas, campesinos, comunales, afrocolombianos, comunitarios y ambientales son los que registraron mayor número de asesinatos en el trimestre”.]

Los indígenas del Cauca y los defensores de DD.HH. afrocolombianos de varios departamentos del país han vivido una situación de violencia y abandono por parte del Estado. La militarización de su territorio y las presencia de las disidencias de las FARC-EP y grupos armados por fuera de la ley han sido el escenario de las agresiones, amenazas y asesinatos registrados por el informe de la SIADDHH. [(Lea también: Comunidades del Cauca no van a dejar que el terror se apodere de sus territorios)](https://archivo.contagioradio.com/comunidades-cauca-no-terror/)

[Al investigar los autores materiales de las agresiones, el informe señala que, en el tercer trimestre del año, los grupos paramilitares fueron responsables de 91 de los casos (50%). Le siguieron las disidencias de las FARC-EP con 29 casos (16%), el ELN con 5 casos (3%) y 58 casos a manos de actores desconocidos (31%).  [(Le puede interesar: "No nos dejen solos", la petición de líderes sociales del Chocó ante amenazas)](https://archivo.contagioradio.com/no-dejen-solos-lideres-sociales-choco/)]

### **Tipos de liderazgo y agresiones contra **defensores 

[Líderes y lideresas por los derechos humanos, sindicales, LGBTI. Además, de integrantes de comunidades indígenas, ambientales, de víctimas, comunales, campesinos, afrocolombianos y comunitarios continúan siendo las personas más afectadas por las agresiones. En lo que respecta a la población en general **los mayores agredidos fueron los indígenas**, presentándose 51 casos de violencia, seguidos por los líderes comunales, que registraron 36 casos. ]

[Cauca sigue siendo el departamento con el mayor riesgo para “ejercer o desarrollar actividades de liderazgo y defensa de los derechos humanos”. Durante el trimestre de 2019 se presentaron 13 asesinatos, 2 atentados y 3 amenazas.][(Lea también: Militarizar más al Cauca es una propuesta "desatinada y arrogante": ACIN) ](https://archivo.contagioradio.com/militarizar-mas-al-cauca-es-una-propuesta-desatinada-y-arrogante-acin/)

La situación del departamento tampoco parece ser alentadora para los últimos tres meses del año, así lo demuestra la masacre en Toribío ocurrida el pasado martes 29 de octubre donde fueron acribillados la gobernadora del resguardo de Tacueyó junto con cuatro guardias indígenas y dos días después, el jueves 31 de octubre, se dio el asesinato de otras cuatro personas en Corinto y un homicidio en Caloto.

[A su vez, las agresiones registradas en el informe ocurrieron en 23 de los 32 departamentos de Colombia. **Cauca registró la cuota más alta con (26), seguido por Córdoba (21), Antioquia (15), Bolívar (12), Caquetá (11) y Valle del Cauca (10)**, cabe mencionar que estos son algunos de los departamentos con más pie de fuerza militar, Prueba de ellos es la orden del Gobierno de incorporar 2.500 integrantes de la Fuerza Pública al departamento de Cauca.  ]

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
