Title: El Gobierno tiene la oportunidad de asumir las mesas indígenas
Date: 2019-05-02 11:06
Author: Foro Opina
Category: Opinion
Tags: Cauca, indígenas, Minga por la vida
Slug: el-gobierno-tiene-la-oportunidad-de-asumir-las-mesas-indigenas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/minga-indigena.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### Por: Foro Nacional por Colombia 

#### El Gobierno tiene la oportunidad de asumir las mesas indígenas, campesinas y afros como escenario de diálogo social 

Las mesas de interlocución, negociación y seguimiento son escenarios estratégicos para el ejercicio de participación ciudadana en el Cauca

La Minga por la defensa de la vida, el territorio, la justicia y la paz fue el escenario en el que se posicionaron las mesas de interlocución, negociación y seguimiento como espacios de participación de las organizaciones indígenas, campesinas y afrodescendientes en la exigencia de derechos sociales ambientales y culturales.

Desde las mesas de interlocución los pueblos étnicos y comunidades campesinas invitaron al gobierno de Duque a concertar el pliego reivindicativo y político de la Minga. El gobierno en unos casos no hizo presencia y, en otros, participó con funcionarios sin capacidad para tomar decisiones. Esta situación, sumada al incumplimiento sistemático de los acuerdos, llevó a la movilización de la Minga que tuvo bloqueada la vía panamericana por veintisiete días.

> [Las mesas han sido el fruto de la movilización llevada a cabo desde 1999, las cuales, posteriormente, tuvieron un reconocimiento político como escenario de diálogo y seguimiento de acuerdos]

Las mesas han sido el fruto de la movilización llevada a cabo desde 1999, las cuales, posteriormente, tuvieron un reconocimiento político como escenario de diálogo y seguimiento de acuerdos. A partir del 2016, dichas mesas fueron reconocidas jurídicamente en el gobierno de Juan Manuel Santos, de tal manera que se institucionalizaron y legitimaron desde el movimiento social. Entre ellas están: i) La Comisión Mixta para el Desarrollo Integral de la Política Pública Indígena para el Consejo Regional Indígena del Cauca, CRIC, creada mediante el decreto 982 de 1999 y reactivada por el decreto 1811 del 2017 con el fin de establecer mecanismos eficaces que garanticen la participación de los pueblos indígenas. ii) La Mesa Proceso de Unidad Popular del Sur Occidente Colombiano (PUPSOC)/ Comité de Integración del Macizo Colombiano (CIMA), reconocida por la gobernación del Cauca mediante acto administrativo del 2016, luego formalizada por el gobierno nacional mediante la resolución No 1071 del 2018 del Ministerio del Interior. iii) La Mesa de la Asociación Nacional de Usuarios Campesinos – ANUC Nacional reconocida mediante resolución 070 del 2016. iv) La mesa de Desarrollo Territorial Afrocaucana, reconocida mediante acto administrativo de la gobernación del Cauca en 2016 y con reconocimiento político a nivel regional y nacional.

Posicionar las mesas de interlocución mediante el acta No 11, firmada el 06 de abril del 2019 por el gobierno nacional, fue una ganancia política para el movimiento social Caucano, dado que lo consideran un espacio legítimo, institucional en el que no solamente se definen aspectos políticos sino también que abordan temas sectoriales, (salud, educación, minería, deporte, etc.) presupuestales y procedimentales. Y aquellos aspectos que no se negociaron en la Minga continuarán planteándose en el marco de las mesas de interlocución.

También fue ganancia para el gobierno nacional, ya que las mesas de interlocución son un dispositivo de diálogo social entre el gobierno y líderes sociales cualificados en la compresión de la política pública, lógicas estatales y con alto grado de legitimidad en sus comunidades. Es importante que el gobierno nacional reconozca y fortalezca las mesas de interlocución, negociación y seguimiento como escenarios de participación, en la medida que los canales institucionales previstos para tal fin no operan de manera adecuada o no producen los resultados esperados en la solución de sus problemas.

> [El reto para el gobierno nacional: dar continuidad y garantías a las mesas de interlocución como dispositivo de participación de los pueblos étnicos y comunidades campesinas en el Cauca]

Ese es el reto para el gobierno nacional: dar continuidad y garantías a las mesas de interlocución como dispositivo de participación de los pueblos étnicos y comunidades campesinas en el Cauca y generar las condiciones necesarias para la participación con el fin de reducir las tensiones sociales y descentralizar la toma de decisiones mediante un diálogo que permita a las organizaciones, plataformas sociales e instituciones construir escenarios de gobernanza territorial.

###### Foto: Cortesía de integrante de la Minga  para la Fundación Foro Nacional por Colombia Capítulo SurOccidente. 
