Title: Amenazas de Aguilas Negras y Rastrojos en el Cauca están relacionadas con la minería
Date: 2015-02-16 17:37
Author: CtgAdm
Category: DDHH, Nacional
Tags: Cauca, indígenas, Marcha de mujeres negras del Cauca, Mineria, Minería ilegal, Minga Indígena, Paramilitarismo
Slug: mineria-amenaza-en-cauca
Status: published

###### Foto: David Schwarz 

Las comunidades negras e indígenas del **norte del Cauca no solo deben luchar contra la acción de empresas multinacionales extractoras de minerales**, las retro-excavadoras de los mineros nacionales, sino también contra la inacción del Estado.

En diciembre del 2014, el **Ministerio del Interior** se comprometió con la **marcha de mujeres negras del norte del Cauca**, a implementar políticas y acciones contra la minería legal e ilegal en el territorio, expulsando las **dos mil retro-excavadoras de mineros nacionales**, así como garantizando la revisión de permisos y procesos de consulta previa para los territorios mineros ya concesionados.

Sin embargo, nada de ello se ha cumplido, y al flagelo que viven las familias indígenas y negras, se suman ahora las constantes amenazas por parte de grupos paramilitares, que realizan llamadas y envían panfletos que firman como "Rastrojos" y "Aguilas Negras". Las amenazas **"siempre están relacionadas con la minería", afirma Francia Marquez, lideresa del territorio**. "La última amenaza llegó el 12 de febrero, nos mencionan a cuatro personas y familias".

La crisis de Derechos Humanos que se vive en el Cauca se ha profundizado en tal medida, que ha obligado a muchas familias a desplazarse hacia departamentos vecinos, o incluso fuera del país, porque **"no hay medidas de protección completas para quienes estamos amenazados"**, insiste Marquez.

Para las comunidades del norte del Cauca, es contradictorio que el gobierno hable de paz, y siga victimizando a las comunidades. **"Debe haber real decisión política por parte del Estado"**, concluye, Francia Marquez.

Este fin de semana, **indígenas y comunidades negras del norte del Cauca realizaron una minga**, para declararse en estado de movilización permanente contra este ataque en contra de sus comunidades.
