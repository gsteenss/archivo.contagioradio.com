Title: Comisión de la Verdad se convierte en una esperanza para organizaciones del Caribe
Date: 2019-02-02 12:53
Author: AdminContagio
Category: DDHH, Paz
Tags: Caribe, comision de la verdad
Slug: comision-de-la-verdad-se-convierte-en-una-esperanza-para-organizaciones-del-caribe
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/02/DyMy64cWwAAXwK3-770x400.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: @ComidionVerdadC 

###### 31 Ene 2019 

[El pasado 31 de  enero, durante el evento 'Encuentro por la vida, el esclarecimiento de la verdad y la no repetición’, más de 50 dirigentes de comunidades de Magdalena, Bolívar, Sucre, Cesar, La Guajira y la costa Caribe expusieron las situaciones de riesgos que se vive en sus regiones y plantearon diversas propuestas de solución ante la Comisión de la Verdad, para garantizar el bienestar y el pleno desarrollo de las actividades e iniciativas que promueven los líderes sociales.]

Durante el encuentro, que contó con la participación del presidente de la  Verdad [el presidente de la Comisión de la Verdad,]**Francisco de Roux**[; el]**procurador General de la Nación, Fernando Carrillo**[; y el apoyo de la gobernación]**del  Bolívar,** [ se alertó sobre los brotes de violencia y amedrantamiento que han reaparecido con más fuerza en los territorios.]

Ante la posibilidad que existe que dichos grupos armados no quieren que se conozca la verdad de lo sucedido en medio del conflicto armado, [**Francisco De Roux alentó a los asistentes para que sienten las bases de un hecho histórico que contribuya a la no repetición del conflicto en el país**, y agregó que desde “la Comisión señalamos los caminos, pero no garantizamos la no repetición. Me aterra pensar que aquí quieren matar a la verdad”.]

[De la reunión surgieron varias propuestas de  planes de seguridad y de protección que serán expuestos y analizados en un próximo  encuentro que se realizará el 7 de febrero en El Carmen de Bolívar, los líderes clamaron por una  unión que los proteja contra los violentos y fortalezca su trabajo en defensa y beneficio de sus comunidades y de los derechos humanos.[(Lea también Inician los encuentros entre la Comisión de la Verdad y los periodistas en Colombia)](https://archivo.contagioradio.com/inician-los-encuentros-entre-la-comision-de-la-verdad-y-los-periodistas-en-colombia/)]

Durante su estancia en Bolívar, el padre Francisco de Roux también se reunió con diversos empresarios y líderes de opinión en Cartagena y con el Espacio Regional de Construcción de Paz de los Montes de María para explicar el mandato y la complejidad del proceso para esclarecer la verdad en el  contexto actual.

###### Reciba toda la información de Contagio Radio en [[su correo]
