Title: Candidatos y partidos políticos deben pronunciarse contra violencia en campañas: MOE
Date: 2018-03-05 14:37
Category: Nacional, Política
Tags: Alejandra Barrios, campañas electorales, Misión Observación Electoral, MOE
Slug: candidatos-y-partidos-politicos-deben-pronunciarse-contra-violencia-en-campanas-moe
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/03/nube.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Noticias Nube] 

###### [05 Mar 2018] 

La Misión de Observación Electoral, en cabeza de su directora Alejandra Barrios, señaló que la violencia que se está presentando en las campañas políticas del país, evidencian la falta de argumentos para defender las ideas políticas de cada partido o candidatos y evidenciado una necesidad por “negar al opositor” a través de la intolerancia.

“La intolerancia, la falta de un debate civilizado se está poniendo de presente, hemos hecho dos llamados uno a los alcaldes para que tomen las medidas necesarias para permitir **hacer eventos políticos que no debe convertirse en una forma de exclusión de algunas candidaturas del debate público**” afirmó Barrios.

El segundo llamado se lo hacen a los partidos políticos para que convoquen a sus simpatizantes y militantes a respetar las otras campañas políticas, “para hacer un ejercicio electoral en paz, no podemos pasar de los riesgos por violencia de los grupos armados ilegales a los riesgos de una ciudadanía intolerante, **que se deja manipular y llevar, por ideas absolutamente antidemocráticas que no permiten el debate**”.

En esa vía aseguro que son los líderes de esas tendencias políticas las que deben enviar mensajes correctos para que los ciudadanos no se polaricen, sino para que la ciudadanía comprenda que la política es la forma de solucionar las diferencias a través del debate argumentado e inteligente y **no a través de las armas, la negación del contrario o la eliminación del contrario político**.

### **“El ejercicio de la política no ha sido el ejercicio muy inteligente de la misma” MOE** 

De acuerdo con Barrios reconociendo que Colombia está regido por una democracia no tendría sentido generar un artículo en donde se prohíban los malos tratos y las agresiones en campañas electorales, **“Colombia se ha caracterizado por su dificultad para establecer diálogos inteligentes y sobre todo en materia política”** afirmó.

Sin embargo, frente a hechos violentos como agresiones físicas, lanzar objeto o fomentar el odio entre manifestantes, el Código de Policía sería el que podría sancionar a aquellas personas que tengan este tipo de actuaciones. (Le puede interesar: ["Iván Cepeda denuncia montajes a su campaña desde el Uribismo"](https://archivo.contagioradio.com/ivan-cepeda-denuncia-montajes-a-su-campana-desde-el-uribismo/))

“La convivencia pacífica es un trabajo de todos, eso no lo puede imponer ninguna norma si la ciudadanía no está convencida de que el camino es el diálogo, el debate inteligente y no las piedras, las rocas y los huevos” aseguró Barrios.

[Decreto 430 Del 05 Marzo de 2018](https://www.scribd.com/document/373039458/Decreto-430-Del-05-Marzo-de-2018#from_embed "View Decreto 430 Del 05 Marzo de 2018 on Scribd") by [Contagioradio](https://www.scribd.com/user/276652802/Contagioradio#from_embed "View Contagioradio's profile on Scribd") on Scribd

<iframe id="doc_23386" class="scribd_iframe_embed" title="Decreto 430 Del 05 Marzo de 2018" src="https://www.scribd.com/embeds/373039458/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-Bpv2OKHTRGh90RH03PP2&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="0.6522053506869125"></iframe><iframe id="audio_24246969" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_24246969_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
