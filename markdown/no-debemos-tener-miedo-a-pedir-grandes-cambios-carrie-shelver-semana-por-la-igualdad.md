Title: "No debemos tener miedo a pedir grandes cambios” Carrie Shelver Semana por la Igualdad
Date: 2015-09-30 17:30
Category: LGBTI, Nacional
Tags: Bogotá Humana, LGBT, Secretaria Distrital de Planeación, Semana por la igualdad
Slug: no-debemos-tener-miedo-a-pedir-grandes-cambios-carrie-shelver-semana-por-la-igualdad
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/Semana_de_la_igualdad_contagioradio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: contagioradio.com 

###### [30 sep 2015]

Con el objetivo de brindar herramientas conceptuales en relación con los derechos de las personas de los sectores LGBTI, los días 28 y 29 de Septiembre se llevó a cabo el **"1er Congreso Internacional sobre Derechos Humanos de las personas con orientaciones sexuales e identidades de género diversas"**, evento que hace parte de la Quinta Semana por la Igualdad en Bogotá.

Investigaciones recientes y experiencias de reconciliación, negociación y superación de las violencias desde la perspectiva de diversidad sexual y de género, fueron expuestas por parte de organizaciones nacionales e internacionales provenientes de países como Chile, Honduras, Nicaragua y Sudáfrica, **en coherencia con el ambiente de paz que vive el país por los acuerdos alcanzados en la Habana**.

Algunas intervenciones destacadas fueron las de **Juan Carlos Prieto, director de Diversidad sexual de la Secretaria de Planeación Distrital**, quien presentó estudios que demuestran una disminución sustancial en la percepción de inseguridad de integrantes de la comunidad LGBTI residentes en la ciudad de Bogotá, **pasando de un 83,27%  registrado en el año 2010 a un 49,6% de la población en 2014.**

En contraste con lo expuesto, Bertha Neris Sánches, consejera consultiva LGTBI por el derecho a la vida y a la seguridad, señala que “*personas de la comunidad LGBTI están siendo amenazadas, casos como los de Michel Candelaria, Juan Sebastian Cifuentes y una mujer bisexual de quien me reservo el nombre por motivos de seguridad, puesto que tiene hijos*”, sin que se obtenga ayuda del Estado para su protección.

En relación con la participación social y lucha por la equidad de derechos humanos, el antropólogo de la Universidad Nacional de Colombia Jose Fernando Serrano, recuerda que antes de empezar a implementar políticas públicas y procesos legales, los movimientos sociales estaban dirigidos a la trasformación de la base social, pero “*lastimosamente se ve, que las agendas de grupos activistas sobre derechos humanos están pensadas desde las especificidades y no en un apoyo colectivo*” .

Por su parte Carrie Shelver activista "Queer" y feminista,  intervino durante el tercer panel de ponencias sobre diversidad sexual y de género en el tránsito del Apartheid a la democracia en Sudáfrica, con un claro mensaje en relación con los cambios de imaginarios en la sociedad. **“Las cosas no son de a pocos, no debemos tener miedo a pedir grandes cambios”.**
