Title: Estudiantes se toman Biblioteca Nacional en defensa de la educación pública
Date: 2018-11-01 12:44
Author: AdminContagio
Category: Educación, Movilización
Tags: educación pública, Paro Estudiantil
Slug: estudiantes-se-toman-biblioteca-nacional-en-defensa-de-la-educacion-publica
Status: published

###### Fotografía: Foto: @CumbrePopularUrbana 

###### 1 Nov 2018 

Como parte de la jornada de paro estudiantil adelantada el 31 de octubre, 30 estudiantes de diferentes universidades públicas se tomaron de forma pacífica la Biblioteca Nacional. Los estudiantes continúan realizando una tertulia de lectura de 24 horas,  ante las presiones de la **Policía Nacional y el Escuadrón Móvil Anti Disturbios (ESMAD)**.

**Camilo Pardo,** integrante de la Unión Nacional de Estudiantes por la Educación Superior (UNEES)**,** relató cómo al llegar al punto de encuentro, diferentes estudiantes de las universidades Pedagógica, **Nacional, Distrital y Colegio Mayor  de Cundinamarca**, ingresaron al recinto escoltados por integrantes del ESMAD, y con el respectivo acompañamiento de veedores por los derechos humanos y funcionarios del edificio que intercedieron para que no fueran desalojados.

"Desde las 6:30 p.m. entramos en una jornada de 24 horas de resistencia y tertulia nocturna por la educación superior, **haciendo presencia pacifica y simbólica en uno de los centros de conocimiento como lo es la Biblioteca Nacional"** aseguró Pardo. Desde la noche del miércoles, policías del cuadrante pidieron a los estudiantes abandonar el edificio, acordonando la puerta principal con diez uniformados que fueron relevados la mañana del jueves.

Los 30 estudiantes permanecen alerta y aguardan a que el Gobierno Nacional representado por la ministra de Educación, **María Victoria Angulo** y las vocerías estudiantiles de la Universidad Pública lleguen a un acuerdo,  "esperemos que  en esta ocasión se pase del simple diálogo a la negociación" expresó el y estudiante de la Universidad Distrital.

> <div style="padding: 16px;">
>
> [  
> ](https://www.instagram.com/p/BppQcdxnr9Y/?utm_source=ig_embed&utm_medium=loading)
> </p>
> <div style="display: flex; flex-direction: row; align-items: center;">
>
> </div>
>
> <div style="padding: 19% 0;">
>
> </div>
>
> <div style="display: block; height: 50px; margin: 0 auto 12px; width: 50px;">
>
> </div>
>
> <div style="padding-top: 8px;">
>
> <div style="color: #3897f0; font-family: Arial,sans-serif; font-size: 14px; font-style: normal; font-weight: 550; line-height: 18px;">
>
> Ver esta publicación en Instagram
>
> </div>
>
> </div>
>
> [Comunicado número 2 \#24Horas \#NocheDeResistenciaYTertuliaEstudiantil](https://www.instagram.com/p/BppQcdxnr9Y/?utm_source=ig_embed&utm_medium=loading)
>
> Una publicación compartida de [UNEES](https://www.instagram.com/unees.col/?utm_source=ig_embed&utm_medium=loading) (@unees.col) el <time style="font-family: Arial,sans-serif; font-size: 14px; line-height: 17px;" datetime="2018-11-01T16:35:58+00:00">1 Nov, 2018 a las 9:35 PDT</time>
>
> </div>

<p>
<script src="//www.instagram.com/embed.js" async></script>
</p>
Además de las actividades de lectura planeadas para estar jornada, los próximos 5 y 6 de noviembre se realizará una movilización en las vías nacionales y se espera que a nivel latinoamericano se unan más estudiantes a la causa para concluir el 15 de noviembre con un paro cívico nacional.

<iframe id="audio_29777915" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_29777915_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
