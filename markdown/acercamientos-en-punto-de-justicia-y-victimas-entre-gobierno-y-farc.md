Title: Acercamientos en punto de Justicia y Víctimas entre Gobierno y FARC
Date: 2015-07-30 15:27
Category: Nacional, Paz
Tags: acuerdo, Álvaro Leiva, carlos, Doug Cassel, enrique santiago, FARC, gallego, gobienro, Juan Carlos Henao, justicia, Manuel José Cepeda, medina, proceso de paz, Santos, víctimas
Slug: acercamientos-en-punto-de-justicia-y-victimas-entre-gobierno-y-farc
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/07/9649115806_053b451cf8_b.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Carmela María 

<iframe src="http://www.ivoox.com/player_ek_5600902_2_1.html?data=lpudkp6Udo6ZmKialJmJd6KpmpKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRlNPjx9rbxtSPpcTpxtfR0ZDJsozk1tPh0ZDIqYy%2B1tjhy8jNpYztjLuSpZiJhaXX1c7aw9iPqc%2Fo08qah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Carlos Medina Gallego, Investigador grupo de "Seguridad y Defensa"] 

######  

###### [30 jul 2015] 

La discusión sobre el punto de Víctimas y Justicia que inició en febrero del 2015 en la mesa de diálogos entre el gobierno y la insurgencia de las FARC, y que fue considerada por diversos analistas como el punto nodal del proceso de paz, **estaría alcanzando su recta final.**

Según fuentes cercanas a la mesa de diálogos, consultadas por Contagio Radio, **en un par de semanas las partes emitirían un comunicado conjunto declarando acuerdos profundos** en torno a la justicia y la reparación a las víctimas, dando paso a la discusión del siguiente punto, relacionado con el fin del conflicto.

Para el investigador Carlos Medina Gallego, la definición de una nueva metodología para el desarrollo de las conversaciones, que incluyó la conformación de **sub-comisiones técnicas del "cese al fuego" y "de garantías jurídicas"** ha sido fundamental para estos avances.

Cabe recordar que en las últimas semanas las partes en conflicto han contado con la asesoría jurídica de Manuel José Cepeda, quien fue arquitecto del texto de la Constitución de 1991; el demócrata liberal Juan Carlos Henao; Doug Cassel, profesor estadounidente reconocido por su lucha por los Derechos Humanos de los prisioneros de Guantanamo; el gestor de procesos de paz, Álvaro Leiva; y el jurista internacional Enrique Santiago.

Para Gallego, es importante el debate nacional que se ha abierto en los últimos días relacionado con la justicia en el proceso de paz, en particular la intervención que del Senado de la República, en la que la senadora Viviane Morales recuerda que hay modelos de Justicia Transicional tan diversos como proceso de paz ha habido en el mundo: **485 que han respondido a las necesidades particulares de cada país y su conflicto.**

<div>

Señala además la necesidad de que se reconozca la existencia de diversos actores en el marco del conflicto, y sus responsabilidades en el mismo. En este sentido, el reconocimiento tácito que hiciera el presidente de la existencia de **grupos paramilitares**, al afirmar que estos se habrían quedado con cerca de **530 mil millones de pesos del sistema de salud,** no sería suficiente. "Hay un reconocimiento explícito de que el paramilitarismo no fue erradicado por el presidente Uribe, pero no un reconocimiento de su transformación en neo-paramilitarismo y bandas criminales", declara el investigador, para quien haría falta la conformación de una sub-comisión que trate este tema para avanzar en la destrucción de este fenómeno con profundos vínculos societales.

Para avanzar también en acuerdos que desescalen el conflicto, como anunció el presidente Santos durante la visita del premio Nobel de Paz, Oscar Arias, el investigador insiste en **dar muestras de paz con los prisioneros políticos que sufren torturas por inasistencia en su Derecho a la salud;** pero también en acciones beneficiosas para la sociedad, como "desescalar las hostilidades contra los movimientos sociales, contra sus movilizaciones y agendas; que no se judicialice los liderazgos sociales y políticos que existen en el país; que se evite que se mate a los líderes de las organizaciones, que haya seguridad para la protesta legítima y legal, y la existencia de la movilización social y sus respectivas formas de organización. El desescalamiento -agregó finalmente Carlos Medina- **no tiene que ver solo con las armas, sino con la actitud agresiva y la intensión agresiva del Estado frente a la población, a los movimientos sociales y políticos"**.

En el punto a seguir sobre terminación del conflicto, trabajan actualmente tanto comandantes de la guerrilla como generales activos del Ejercito.

</div>
