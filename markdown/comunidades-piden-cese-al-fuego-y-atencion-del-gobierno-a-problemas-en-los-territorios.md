Title: Comunidades piden cese al fuego y atención del Gobierno a problemas en los territorios
Date: 2020-03-30 11:21
Author: CtgAdm
Category: Actualidad, Nacional
Tags: Chocó, ONU, Putumayo, territorios indígenas
Slug: comunidades-piden-cese-al-fuego-y-atencion-del-gobierno-a-problemas-en-los-territorios
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/04/Comunidades-paz.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

Comunidades de diferentes territorios del país emitieron un comunicado tras el anuncio del **[Ejercito de Liberación Nacional](https://archivo.contagioradio.com/guerras-injusticias-grito-pobres-papa-francisco/)** (ELN) de un Cese Unilateral Activo desde el 1 hasta el 30 de abril, como una medida de alivio a las quienes viven en medio de la guerra y están luchando por detener el avance de la pandemia del COVID 19 en territorios rurales.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

*"**Esta decisión es fundamental en momentos en que en [nuestros territorios](https://archivo.contagioradio.com/comunidades-indigenas-generan-espacios-de-proteccion-contra-el-covid-19/)existe una exclusión histórica y sistemática**, con negación de nuestros derechos a la paz, a la salud, a la alimentación"*, señalaron, y pidieron al presidente Duque, que **detenga las operaciones ofensivas en todo el país** .

<!-- /wp:paragraph -->

<!-- wp:quote -->

> *"Este gesto del ELN que nos abre esperanzas humanitarias hacia la continuidad de la paz con justicia socio.ambiental"*

<!-- /wp:quote -->

<!-- wp:paragraph -->

Y agregaron que las acciones que deben ejecutarse por la la [Fuerza Pública](https://archivo.contagioradio.com/alejando-carvajal-joven-de-20-anos-asesinado-por-el-ejercito-en-catatumbo-ascamcat/) deben enfocarse en evitar la expansión del virus y generar unas garantías básicas para su movilización humanitaria, esto a la luz de los recientes hecho donde la comunidad se vio en medio del fuego, generado por las operaciones del Ejército, como son la erradicación forzada terrestre y aérea.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Según ellos y ellas este tipo de acciones deben trasladarse a combatir otros virus que aquejan a los terrorios cómo es el caso dengue con más de 119.840 casos y 78 muertes en los últimos tres mes , según la Organización Panamericana de la Salud (OPS); la crisis humanitaria que ha cobrado la vida de 3 voceros sociales en la última semana en territorios como el Meta y Chocó.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Instaron también a los demás grupos armados activos en el país a sumarse a el llamado humanitario, *"la presión, el confinamiento, las amenazas de muerte y el asesinato como ocurren en el [Putumayo](https://www.justiciaypazcolombia.com/asesinatos-y-control-armado-en-putumayo/https://www.justiciaypazcolombia.com/asesinatos-y-control-armado-en-putumayo/) , San Juan, Bajo Atrato en medio de esta crisis pandémica, lejos de dar ventajas militares son un crimen de lesa humanidad, una afrenta a la vida*".

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/ONUHumanRights/status/1244716665553723396","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/ONUHumanRights/status/1244716665553723396

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

Llamado al que se unió la Organización de las Naciones Unidas en Colombia, quienes acogieron la acción del ELN, "*este gesto puede brindar alivio a las comunidades y grupos vulnerables a las regiones afectadas por el conflicto, así como ayudar que las autoridades se centren en la lucha por el Covid-19"*, señaló el Secretario General de las Naciones Unidas Alberto Brunori.

<!-- /wp:paragraph -->
