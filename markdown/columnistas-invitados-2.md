Title: Columnistas invitados
Date: 2017-03-07 15:13
Author: AdminContagio
Slug: columnistas-invitados-2
Status: published

COLUMNISTAS INVITADOS
---------------------

[](https://archivo.contagioradio.com/porque-no-comer-animales-vegetarianismo/)  

###### [¿Por qué no como animales?](https://archivo.contagioradio.com/porque-no-comer-animales-vegetarianismo/)

[<time datetime="2016-01-12T08:25:27+00:00" title="2016-01-12T08:25:27+00:00">enero 12, 2016</time>](https://archivo.contagioradio.com/2016/01/12/)Una pregunta tan frecuente cuando quien la hace quizá no haya salido de la zona de confort que causa un hábito de consumo incuestionado e inconsciente.[Leer más](https://archivo.contagioradio.com/porque-no-comer-animales-vegetarianismo/)  
[](https://archivo.contagioradio.com/maltrato-animal-al-codigo-penal/)  

###### [Maltrato animal al código penal](https://archivo.contagioradio.com/maltrato-animal-al-codigo-penal/)

[<time datetime="2015-12-04T10:59:18+00:00" title="2015-12-04T10:59:18+00:00">diciembre 4, 2015</time>](https://archivo.contagioradio.com/2015/12/04/)Por  Dragonfly  - @MeVle5 4 Dic 2015 Desde siempre el ser humano ha tenido como consigna una declaración explícita de guerra sin tregua contra los animales aun cuando ellos no lo estén contra nadie, las leyes así[Leer más](https://archivo.contagioradio.com/maltrato-animal-al-codigo-penal/)  
[](https://archivo.contagioradio.com/acusamos-y-senalamos-con-el-dedo/)  

###### [Acusamos y señalamos con el dedo](https://archivo.contagioradio.com/acusamos-y-senalamos-con-el-dedo/)

[<time datetime="2015-11-13T06:00:32+00:00" title="2015-11-13T06:00:32+00:00">noviembre 13, 2015</time>](https://archivo.contagioradio.com/2015/11/13/)Foto: Taringa Dragonfly - @MeVle5 13 Nov 2015 En muchas ocasiones nos ha aturdido la indignación por alguna razón al leer un artículo o ver una noticia en un video, nos puede conmover, vemos con malos[Leer más](https://archivo.contagioradio.com/acusamos-y-senalamos-con-el-dedo/)  
[](https://archivo.contagioradio.com/democracia-colombiana-la-maquina-de-hacer-votos/)  

###### [Democracia colombiana: La máquina de hacer votos](https://archivo.contagioradio.com/democracia-colombiana-la-maquina-de-hacer-votos/)

[<time datetime="2015-11-03T10:05:04+00:00" title="2015-11-03T10:05:04+00:00">noviembre 3, 2015</time>](https://archivo.contagioradio.com/2015/11/03/)Cuando hablamos de democracia en Colombia obligatoriamente nos remitimos al concepto de maquinarias políticas[Leer más](https://archivo.contagioradio.com/democracia-colombiana-la-maquina-de-hacer-votos/)
