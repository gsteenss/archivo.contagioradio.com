Title: Su candidatura pudo ser contradictoria con su fe
Date: 2019-12-05 14:51
Author: CtgAdm
Category: A quien corresponda, Opinion, Uncategorized
Tags: alvaro uribe velez, candidatura, Centro Democrático, Corrupción en Colombia, cristianas, Cristianos, narcotrafico, Religión
Slug: sucandidaturapudosercontradictoriaconsufe
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/carretera.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

Por culpa de ustedes, el nombre de Dios es denigrado entre las naciones  
(Romanos 2,24)

 

Bogotá, 9 de noviembre del 2019

*Me gusta tu Cristo... No me gustan tus cristianos.*  
*Tus cristianos son muy diferentes a tu Cristo.*  
*Mahatma Gandhi.*

 

Estimado  
**Ministro ordenado dos**  
Cristianos, cristianos, personas interesadas

Cordial saludo,

De paso por el municipio de Dosquebradas, Risaralda, me llamó la atención una valla publicitaria que decía: *“Dosquebradas tiene cura”;* al leerla me enteré de su candidatura a la alcaldía por el Centro Democrático –*CD.*

Seguí el viaje pensando en el significado de un sacerdote candidato a un cargo de elección popular por el *CD* y en hechos del máximo líder del *CD* y de varios de sus integrantes con grandes repercusiones para el país y por eso decidí escribirle, **sin cuestionar su sacerdocio y su persona porque no lo conozco, ni tengo referencias suyas**. Además, decidí hacerlo después de las elecciones, para reflexionar sin el calor y la pasión de la campaña electoral.

##### Reconozco que es una mirada parcial, como todas las miradas humanas. 

La aspiración de un *“cura”* o un pastor a un cargo de elección popular tiene sus “más y sus menos” por el papel o el rol que juega en una comunidad. Se puede justificar si el bien es mayor que los problemas que genere, lo difícil es saber si hará bien o no. En ocasiones, sacerdotes y pastores elegidos por elección popular han hecho valiosos aportes a la sociedad, en otros casos no. Por ejemplo, la Iglesia Católica cuestiona la participación política partidista y electoral de los sacerdotes, abriendo la posibilidad para cuando lo exija *“la promoción del bien común”* (Cf. Código de derecho canónico 287).

Reconozco que tengo *“resistencias”* a la ***“política confesional religiosa”,*** de cualquier iglesia o religión, por las experiencias dolorosas del pasado. La religión tiene una dimensión profunda, aborda lo trascendente, llega al inconsciente personal y colectivo y con facilidad induce a las personas y las lleva a tomar decisiones por el *“deber religioso”* y no por el análisis serio de lo más conveniente para toda la sociedad. Es decir, que, en nombre de Dios, pueden llevar a las personas a decidir sobre intereses económicos y politiqueros contrarios al bien común y poco acordes con los valores del Evangelio.

La participación política de personas con valores éticos es necesaria para la democracia, pero hay un tipo de política partidista colombiana con procedimientos, intereses y propuestas contrarias al *“deber ser de la política”*, al bien común, a la ética ciudadana y a los *“valores”* o principios del Evangelio, aunque se haga en nombre de Dios.

**Con frecuencia, la política en nombre de Dios es contraria a lo que dijo Jesús de Nazaret.**

Le puede interesar: [La dificultad de los cristianos para el cambio o conversión.](https://archivo.contagioradio.com/la-dificultad-de-los-cristianos-para-el-cambio-o-conversion/)

Un político cristiano debería reconocerse por asumir y vivir en todos los espacios de su vida los valores de la verdad, la justicia, el derecho, la honradez, el bien de los pobres y abandonados, enseñanzas fundamentales de Jesús de Nazaret y no por repetir constantemente el nombre de Dios, por citar la Biblia en todos los espacios civiles, por usar símbolos religiosos o por la *"autoridad"* de ser pastor, sacerdote o una persona religiosa, creyente.

Le repito, que no lo conozco personalmente y por eso no hago juicios sobre su persona o su ministerio, pero en el partido político del que hace parte, hay valores del evangelio que no podrá poner en práctica, como trataré de explicarle en esta carta, aunque me alargue.

Le “confieso” que tengo amigas y amigos de su partido político, que nos respetamos y dialogamos sobre diversos temas, sin ataques y descalificaciones personales; con algunas trabajamos en temas de interés general, aunque no compartamos las visiones políticas del partido.  
El *CD* es el partido del senador **Álvaro Uribe Vélez** – *AUV*, así lo reconocen sus militantes y contradictores, él decide lo que se hace, le imprime los *“valores”* y toma decisiones fundamentales, lo cual no es el problema que planteo, aunque no sea el ideal democrático, es una decisión del partido y la respeto.

El senador Uribe es una persona controvertida, genera amores y odios, tanto en seguidores como en detractores. Cuando están presentes los amores y los odios hay poca razón, poca reflexión y poca argumentación. Conozco personas *“antiuribistas”* fanáticas, cerradas, irreflexivas que justifican sus posturas políticas con afirmaciones falsas y lo acusan de cosas que no ha hecho. Conozco personas *“uribistas”* fanáticas, cerradas, irreflexivas que justifican sus posturas políticas con afirmaciones falsas y que para defenderlo niegan hechos evidentes y contundentes. Muchas de esas personas son creyentes *(cristianos, católicos y evangélicos)* que en la práctica olvidan las palabras de Jesús:

**“con la vara que midan serán medidos” (Mateo 7,2)**

Usan medidas distintas para los mismos hechos, para valorar los mismos argumentos y para juzgar “*el mismo tipo de personas”*, dependiendo de si favorecen o no a su líder.

Para que comprenda mi inquietud, le comparto tres temas, de público conocimiento, en los que está implicado *AUV*; puede revisar medios de información de la época de los hechos o recientes y con fuentes serias o si prefiere hay libros que recopilaron y organizaron la información, uno de ellos es la “*Biografía no autorizada de Álvaro Uribe Vélez”*  (libro en el que contrasto algunas informaciones) de *Joseph Contreras* corresponsal de la revista **Newsweek** y graduado en las Universidades de *Harvard* y de *London School of Economics*. Estas informaciones verificadas dimensionan las repercusiones humanas, éticas, políticas y sociales y el costo en vidas humanas, en corrupción y derrumbe ético y moral de nuestra sociedad de sus actuaciones. Me pregunto: *¿qué haría y diría el CD si en esos mismos hechos estuvieran implicados otros partidos, movimientos y organizaciones sociales distintas a los suyos?*

#### Relaciones con el narcotráfico. 

Son muchos los hechos verificados por distintas fuentes que muestran las relaciones de *AUV* con el narcotráfico, señalo algunos:

-   Entre octubre y diciembre de **1982** fue Acalde de Medellín, “salió” días después de conocerse su participación en una reunión con *Pablo Escobar, Gonzalo Rodríguez Gacha y Fabio Ochoa*.
-   Cuando su padre *Alberto Uribe Sierra *fue asesinado, AUV viajó hasta la finca en un helicóptero de propiedad de *Pablo Escobar*, que salió del aeropuerto Olaya Herrera de Medellín a las 6:45pm.
-   Tranquilandia fue el mayor complejo cocalero descubierto en Colombia, allí se decomisó el helicóptero Hughes 500 con matrícula HK 2704X perteneciente a la familia *Uribe Vélez*.
-   El 24 de marzo de **1980**, *AUV* fue nombrado director de la Aeronáutica Civil, cargo en el que estuvo hasta el 7 de agosto del 1982, sin grandes dificultades, a pesar de lo *“complicado del cargo”*, pues el anterior director, Fernando Uribe Sénior, nombrado en enero de 1980 fue asesinado el 26 de febrero del mismo año, 20 días después de ordenar la clausura algunos aeropuertos clandestinos por donde salían cargamentos de estupefacientes.
-   Él autorizó la construcción de pistas aéreas y otorgó licencias para aeronaves a personas vinculadas a los carteles de la droga.
-   Pablo Escobar le compartía a Virginia Vallejo la importancia del apoyo de *AUV* para su negocio: *“si no hubiese sido por él, tendríamos que llevar la droga a Estados Unidos nadando”*.
-   Un año después de la salida de *AUV*, la Aeronáutica Civil suspendió el permiso de operación a 57 aeronaves, de propiedad de *Carlos Lehder, Pablo Escobar, Fabio Ochoa y Samuel Alarcón*, por orden del ministro de Justicia *Rodrigo Lara Bonilla* en su lucha contra narcotráfico. Rodrigo Lara fue asesinado el **30** de **abril** de **1984** por el narcotráfico.

#### Vemos dos hechos más: 

El primero:

-   En **1981**, la Aeronáutica Civil, le otorgó la licencia para  la ruta aérea *Medellín-Turbo* a *Jaime Cardona*, empresario conocido por sus vínculos con el narcotráfico, cuando el gobernador de Antioquia lo supo, llamó al director y le preguntó que si no sabía quién era este empresario, éste le respondió que era un hombre de bien.                                                                                                                     El gobernador, le pidió una cita privada al presidente *Turbay* y le entregó la información. No paso nada. *AUV* le “gano la partida” al gobernador de Antioquia, *Iván Duque Escobar* (padre del actual presidente) que intentaba detener este cáncer que empezaba a tomarse el país. **Perdió el país.** Conocemos el desarrollo posterior de los carteles de la droga. Hoy, el hijo del “derrotado gobernador” llegó a la presidencia de Colombia, de la mano de quien le “ganó la partida” a su papá. **¡Qué distinto sería el país si esa partida la hubiese ganado el gobernador Iván Duque Escobar!** *¿Qué pensará desde “la* *otra vida”, viendo la mano que llevó su hijo a la presidencia?*

El segundo:

-   En un informe clasificado de 14 páginas, de septiembre **1991** (Ver**: [Intelligence Information Repfort.pdf](https://drive.google.com/file/d/0B4lHh510lrExVmZxZ3QwRFk5MUE/view)**)**,** el departamento de Estado de los Estados Unidos, reseñaba una lista de 104 personas vinculadas con el narcotráfico. El número 1 es alias *“Popeye”*, número 16 *“La Quica”*, numero 79 *Pablo Escobar*, número 80 *Yair Klain*, el número 82 a *Álvaro Uribe Vélez*, de quien dice, entre otras cosas, que es un político colombiano que colabora con el cartel de Medellín y con el alto gobierno, relacionado con negocios de narcotráfico en Estados Unidos, que su padre fue asesinado por sus conexiones con el narcotráfico.

*AUV* no ha dado explicaciones claras y contundentes sobre estas relaciones, creo que no hay demandas penales por injuria y calumnia al Departamento de Estado de los Estados Unidos, ni a los diferentes medios de información que publicaron estas noticias, pienso que cualquier persona, con recursos económicos para pagar un abogado, lo hubiera hecho ante la gravedad de las denuncias.

#### Relación con el paramilitarismo. 

Son muchas las informaciones bien fundamentadas y de público conocimiento de sus relaciones personales y familiares con este fenómeno, menciono algunas:

-   Sobre las fincas familiares de **la Carolina, la Mundial y Guacharacas** hay múltiples documentos y testimonios de militares, paramilitares, políticos, víctimas y testigos de la presencia paramilitar en  
   ellas. En el caso de los *“doce apóstoles”*, su hermano *Santiago Uribe Vélez* está siendo juzgado (etapa de alegatos finales) por concierto para delinquir (conformación de grupos paramilitares en  
   la finca la Carolina) y por el homicidio de *Camilo Barrientos*; la fiscalía hizo una lista de **533 **asesinatos cometidos por este grupo entre los años **1990** y **1998**.
-   Durante su periodo como gobernador de Antioquia **1995-1997** el accionar paramilitar se profundizó y dio un *“salto cualitativo”* con la promoción de las CONVIVIR, en el departamento y el país, bajo la dirección de paramilitares responsables del baño de sangre.
-   Para su elección presidencial en el año 2002, hay múltiples testimonios y evidencias del apoyado de sectores del poder relacionados con el paramilitarismo; la mayoría de los congresistas condenados por para-política fueron sus aliados.

**Hay varias investigaciones en curso:**

[Tribunal de Medellín pide investigar a Uribe Vélez por masacres de hace 20 años](https://www.elheraldo.co/colombia/tribunal-de-medellin-pide-investigar-uribe-velez-por-masacres-de-hace-20-anos-456446)

[Corte Suprema adelanta 28 procesos contra Álvaro Uribe.](https://www.rcnradio.com/judicial/corte-suprema-adelanta-28-procesos-contra-alvaro-uribe)

#### La relación con la corrupción. 

Hay múltiples hechos de corrupción en las diferentes etapas de su vida pública a los que está vinculado:

-   En octubre de **1980** una subcomisión de la cámara de representes visitó las obras del aeropuerto de Rionegro y luego se trasladó a la Presidencia de la República, al Contralor General, al Procurador General y al Consejo de Estado las denuncias presentadas por el presidente de Asociación Colombiana de Ingenieros por irregularidades en contratos cuando era director de la Aeronáutica Civil.
-   El **30 de octubre de 1994** fue elegido gobernador y el **31 de octubre** *Fabio Valencia Cossio*, denunció ante el Consejo Electoral lo ocurrido en el Centro de Computo Electoral de las Empresas Publicas de Medellín donde se dio un súbito cambio de resultados y la presencia de su emisario *Mario Uribe Escobar* en la Registraduría Departamental del Estado Civil donde solo debían estar sus funcionarios, fue “elegido” gobernador de Antioquia de forma cuestionada.
-   La empresa **Confirmeza S.A,** está relacionada con el asesinato de Guillermo Cano, su principal socio es Luis Carlos Molina Yepes, considerado autor intelectual, *AUV* fue socio de dicha empresa.
-   El **4 de julio del 2008**, el presidente Uribe recibió en la Casa de Nariño a Marcelo **Odebrecht**, luego esta empresa financió la primera campaña presidencial de Juan Manuel Santos dirigida por el uribismo, financió la segunda campaña de Santos y la de su opositor Oscar Iván Zuluaga, quien viajó a San Pablo en febrero del 2014, acompañado de Iván Duque a una reunión con funcionarios de la empresa.

La reelección presidencial merece una mención especial en este tema de la corrupción por las repercusiones para el futuro del país, señalo tres aspectos, entre muchos otros:

#### **Primero:** 

**Cambiar las reglas de juego en la mitad de un partido** en su favor, es inaceptable en una sociedad decente, fue un atentado a la ética de los mínimos, a la moral social y publica, fue dinamitar la credibilidad democrática del país; lo hizo comprando senadores para asegurar el quórum y los votos para la aprobación del “articulito” que le permitía reelegirse inmediatamente, todas y todos recordamos los roles *Teodolindo* y *Yidis Medina* y sus condenas por el delito de cohecho (delito hecho entre dos) mientas sus compañeros del “hecho” quedaban libres.

#### Segundo: 

**La reelección acabó con el sistema de *“pesos y contrapesos”***, creados por la Constitución para controlar la corrupción estatal, es decir, el uso del poder en provecho propio de los funcionarios oficiales, de grupos políticos y económicos en el gobierno y en detrimento de la sociedad. Para controlar la tentación de la corrupción del Estado, se crearon los siguientes mecanismos de control:

-   La **Fiscalía General de la Nación** para investigar todos los delitos, la **Procuraduría General de la Nación** para ejercer el control disciplinario de todos los funcionarios públicos y exigir que actúen de acuerdo a la ley; la **Defensoría del Pueblo** para defender al pueblo frente a las acciones por acción y omisión de los funcionarios públicos; la **Contraloría General de la** **Nación** para controlar el manejo del dinero público.

El sistema de “pesos y contrapesos” consistía en que la elección de estos mecanismos de control, en los que participa el presidente, se realizaba en el tercer año de su mandato, de esta manera que el siguiente gobierno era controlado por funcionarios elegidos por otro gobierno, lo que garantizaba independencia en el ejercicio de sus funciones. Con la reelección, el sistema de pesos y contrapesos se acabó, **porque él mismo eligió a quienes debían controlarlo y como es lógico, nombró a sus amigos.**

#### Tercero: 

Hoy nos escandalizamos por el **Cartel de la Toga**, por el que la politiquería corrupta se tomó la justicia para garantizar la impunidad en el robo de los dineros públicos. Considero que dos decisiones del Presidente, además del punto anterior, favorecieron su actuación:

1.  La fusión del ministerio de la política **(Ministerio de Interior**) con el **Ministerio de Justicia**, al colocar en cabeza de un mismo ministro la política y la justicia; algo parecido pasó con la fusión del **Ministerio del Medio Ambiente**, **de Vivienda y de Desarrollo** que debilitó la protección del “medio ambiente” y la lucha contra el calentamiento global, los daños ambientales, la contaminación del agua…
2.  El uso de *“terna de uno”* para la elección de varios magistrados de las altas cortes y de quienes estaban al frente de los mecanismos de control. Las ternas de uno, consistían en el Presidente presentaba en la terna dos candidatos de poca trayectoria y reconocimiento y uno fuerte, su cercano, lo que junto con la “mermelada” garantizaba que el Congreso lo eligiera.

Respetado padre, podría extenderme recordándole hechos sobre estos temas o sobre otros *(como* *la reforma a la salud en beneficio de las empresas y en detrimento de los usuarios; la reforma* *laboral que con grave daño a los trabajadores y a la economía del país en beneficio de los* *monopolios empresariales o la política que promovió los llamados “falsos positivos”)* con grandes y profundas repercusiones para vida política, social y ética del país, pero creo que con estos comprende por qué me quedé pensando en el significado de su candidatura, como sacerdote, a la alcaldía por el Centro Democrático y puede entender por qué le decía que *“hay valores del* *evangelio que no podría poner en práctica”.*

Espero que me disculpe el atrevimiento.

Fraternalmente,  
P. Alberto Franco, CSsR, J&P  
francoalberto9@gmail.com

###### 
