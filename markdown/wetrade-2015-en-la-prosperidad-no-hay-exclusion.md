Title: Wetrade 2015 "En la prosperidad no hay exclusión"
Date: 2015-10-30 16:40
Category: LGBTI, Nacional
Tags: Camára de comercio LGBT, Felipe Cárdenas Presidente de la Camara de Comercio LGBT, La segunda Edición de la cumbre de negocios LGBT, LGBT
Slug: wetrade-2015-en-la-prosperidad-no-hay-exclusion
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/10/camara_de_comercio_LGBTI_contagioradio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### contagioradio 

###### [29 oct 2015]

El presidente  de la  Cámara de Comercio LGBT, Felipe Cárdenas se expresó acerca del poder  de compra que  tienen  los  5  millones  de  colombianos , superando los US\$16.000 y  como va creciendo  Wetrade 2015, todo en la perspectiva del posconflicto.

La  segunda  edición de la  cumbre  de  negocios LGBT  ha  reunido a todas las cámaras  de  comercio del  continente bajo el  nombre de Wetrade  2015, donde  buscan  como lo indica su eslogan exportar  Diversidad,  con una  audiencia  de más de 300  personas de  15 países  diferentes, analizando estadísticas, generando estrategias   y  significando la importancia económica que tiene las personas  LGBT.

Del 28 al 30 de Octubre  Bogotá  tendrá la oportunidad de participar  en  este evento con la posibilidad  de escuchar  experiencias de cámaras de comercio  de Perú, Argentina y Uruguay y expositores como Ophelia Pastrana  Física y Economista Mediaexpert de Mexico o Tony Tenicela Ejecutivo de desarrollo global en negocios de USA.

El día  de  hoy  de  2  a 6 de la tarde  el ingreso a las conferencias son totalmente  gratis,  basta con registrarte en [www.wetrade.lgbt](http://www.wetrade.lgbt)  y se estará  desarrollando en el Hotel W Avenida carrera 9\# 115-30 en Bogotá, para quienes  no puedan asistir la misma  página da la posibilidad  de visualizar las  conferencias vía Stream.

Cárdenas comentó que aproximadamente son cinco millones de personas  auto reconocidas  dentro de la comunidad LGBT, lo que  justifica que la  última medición de poder de compra en el  2014  arrojó resultados de  US\$ 16.000 en sectores  como la  salud y el cuidado personal, turismo, banca y seguros, servicios  profesionales  y entretenimiento. En  ciudades  como  Bogotá  quien se lleva  el primer lugar  en poder de compra, Barranquilla, Medellín, Cali y Bucaramanga.

La Cámara de Comercio  LGBT  fue  creada en  septiembre del 2012  respondiendo a  una  necesidad  de  cubrir el mercado LGBT del país. Viéndolo como un nuevo nicho con poder de compra, consumo y marcador de tendencias, al mismo tiempo generar  espacios  en donde  nuevos  emprendedores  colombianos  LGBTI puedan  incentivar  sus proyectos con información estadística y asesoría comercial.

Wetrade  2015  es una plataforma  en  que la  visibilizacion  LGBT  es importante no solo como agregado social, si no también demuestra la importancia económica que tiene  satisfacer las necesidades de 5 millones de colombianos frente a un mercado heterosexista.

Abriendo las posibilidades a las personas LGBT nacionales e internacionales de  comercializar productos en áreas  de mercancía  textil como calzado, ropa interior y vestidos  de baño exportados desde Canadá y Estados Unidos o  en el turismo en donde se pueden evidenciar la primera ruta gastronómica en Bucaramanga o el carnaval LGBT en Barranquilla.

Pese a que Colombia sea un “País  godo” Felipe  afirma  que  la igualdad en los negocios  es imparable y  “Una  sociedad  en  paz piensa en prosperidad, y en la prosperidad  no hay  exclusión” por lo que argumenta que no tendría  ningún reparo en  contratar a un reinsertado, justificándolo que tampoco lo tendría al contratar a un afrodescendiente  o una persona  con situación de discapacidad.
