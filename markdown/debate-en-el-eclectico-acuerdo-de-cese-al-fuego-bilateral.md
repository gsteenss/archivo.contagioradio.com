Title: Debate en El Ecléctico "Acuerdo de cese al fuego bilateral"
Date: 2016-06-30 09:04
Category: El Eclectico
Tags: debate, proceso de paz
Slug: debate-en-el-eclectico-acuerdo-de-cese-al-fuego-bilateral
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/06/160623_07_FirmaAcuerdosPazFarc_1800-e1467294057494.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: SIG 

###### 27 Jun 2016 

En este debate sobre el acuerdo de cese al fuego bilateral, firmado por en La Habana entre el Gobierno y las Farc, participaron María Fernanda Carrascal, activista y fundadora de Ideas por Bogotá e integrante del movimiento Por Colombia Sí, que apoya los diálogos; Claudia Quintero, víctima del conflicto armado y directora de la corporación Anne Frank, que trabaja con otras víctimas; Andrés Celis, politólogo y periodista del portal especializado en conflicto armado Verdad Abierta; y Hernán Cadavid, abogado y director nacional de juventudes del Centro Democrático.

Los asistentes discutieron las zonas veredales de normalización que se incluyen en el acuerdo de cese al fuego y Hernán Cadavid, representando al Centro Democrático, manifestó su preocupación por el hecho de que estas zonas se ubiquen en zonas en donde la guerrilla ha sido particularmente fuerte. Claudia Quintero manifestó que cree en la justicia restitutiva y que la justicia punitiva no le ha traído verdad ni reparación en su proceso judicial.

Las comparaciones entre este proceso y el de Justicia y Paz fueron abundantes, y los asistentes señalaron que ese proceso no consiguió la pacificación del territorio, ni la justicia ni la reparación para las víctimas. Hernán Cadavid defendió Justicia y Paz aunque reconoció que fue un proceso imperfecto. También criticó el grado de impunidad que se está planteando en estos acuerdos con las Farc.

La participación política de los guerrilleros fue otro punto que causó controversia en el debate. Algunos, como María Fernanda Carrascal, manifestaron estar dispuestos a “tragar sapos” para lograr que este proceso llegue a buen término. Otros, como Andrés Celis y Hernán Cadavid señalaron no estar de acuerdo con el “falso dilema” entre verdad y justicia. Cadavid argumentó enfáticamente que un estado de derecho “no puede renunciar a la verdad y tampoco a la justicia”.

Todos ellos estuvieron de acuerdo en decir que este proceso de paz es una oportunidad para Colombia y los ciudadanos tendremos que estar muy pendientes de cómo se implementa lo acordado. La veeduría ciudadana es una parte fundamental del proceso y El Ecléctico, a través de sus colaboradores, tendrá el ojo puesto a cómo se lleva a cabo.

<iframe src="http://co.ivoox.com/es/player_ej_12077117_2_1.html?data=kpedmZyVdZihhpywj5WVaZS1kZWSlaaVfI6ZmKialJKJe6ShkZKSmaiRdI6ZmKiah5eWhcTpxtfR0ZDIqYzXxtjSjcbQb8fpxszcjcfNsMLoxtfOzoqWdoyhpcrPw9nJb8bijKrZjarHsIa3lJKSmaiRh9Di1cbUy9SPlsLYytSYj4qbh46o&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>
