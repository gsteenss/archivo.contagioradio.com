Title: Continúa mortandad de peces del Río Magdalena
Date: 2017-02-27 16:49
Category: Ambiente, Voces de la Tierra
Tags: Magdalena, mortandad de peces
Slug: mortandad_peces_rio_magdalena
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/02/peces-e1488231600591.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: región caribe 

###### [27 Feb 2017] 

[Esta vez más de 800 kilos de peces muertos han sido recogidos desde el sábado a orillas del Río Magdalena en el departamento del Huila. Un escenario que se repite con mayor frecuencia debido a las fuertes lluvias en **Campo Alegre y Lebrija,** según explicó Fernando Borja, secretario de Ambiente y Gestión de Riesgo.]

[De acuerdo con las autoridades se trata de una situación causada por el taponamiento de sus agallas con el lodo que llegó al río de esos municipios. **Bagres, bagresapos, bocachicos, cuchas, sardinas, nicuros y patalos** son las especies de peces que desde tempranas horas empezaron a aparecer.]

[“Este lodo comenzó a bajar por nuestro departamento el jueves, todo esto provocó baja de oxígeno y turbiedad en las aguas, de los ríos afluentes directos del Magdalena, provocando la muerte de miles de especies. Los peces debieron salir a las orillas y por su puesto murieron”, explican pobladores.  ]

[Un informe, presentado por Cortolima y organismos de Gestión del Riesgo departamentales y municipales, a situación se presentó  debido a que el pasado jueves 23 de febrero, en Campoalegre (Huila), “**se generó una avalancha originada por fuertes lluvias en la partes altas de la cordillera central,** la cual ocasiona que toneladas de material sólido, lodo, rocas y sedimentos cayeran a los afluentes del Magdalena.]

[La situación es preocupante, debido a que esos municipios ribereños dependen de la pesca. Además, así como puede verse afectada la economía de esos territorios, puede haber  problemas de salubridad pública por la descomposición de los peces contaminados. Por ello, **las autoridades recomiendan**]**prohibir el uso del agua para el consumo humano y no comercializar los peces muertos.**

###### Reciba toda la información de Contagio Radio en [[su correo]
