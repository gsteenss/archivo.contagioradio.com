Title: Se reabre debate sobre la 'rebelión' como delito político
Date: 2015-03-23 23:49
Author: CtgAdm
Category: DDHH, Nacional, Paz
Tags: Angela Maria Robledo, coalicion larga vida a las mariposas, mark burton, piedad cordoba, presos, prisioneros, rueda de prensa
Slug: se-reabre-debate-sobre-la-rebelion-como-delito-politico
Status: published

###### Foto: Contagio Radio 

Los días 20, 21 y 22 de marzo del 2015 se llevó a cabo en Bogotá el 2do Encuentro Nacional e Internacional por la libertad de las y los prisioneros políticos, "Larga vida a las mariposas", para discutir la situación carcelaria en Colombia.

En el marco de los diálogos de paz, la Coalición Larga Vida a las Mariposas exigió que se reconceptualice el delito político de "rebelión", y se reconozca la persecución que el Estado ejerce contra líderes y lideresas sociales en Colombia.

En el Encuentro estuvo presente el abogado  Simón Trinidad, quien reiteró la necesidad de que se repatrie al guerrillero de las FARC como un símbolo y gesto de paz por parte del gobierno colombiano y el de los EEUU.

Finalmente, la defensora de DDHH Piedad Córdoba, incitó a colombianos y colombianas a ser participes de este debate, para construir paz en Colombia.

\[embed\]https://www.youtube.com/watch?v=4dgQuAmtXOA&feature=youtu.be\[/embed\]
