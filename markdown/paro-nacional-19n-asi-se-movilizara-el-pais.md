Title: Paro Nacional 19N: así se movilizará el país
Date: 2020-11-18 21:44
Author: AdminContagio
Category: Actualidad, Movilización, Paro Nacional
Tags: 19N, Movilizaciones, Paro Nacional
Slug: paro-nacional-19n-asi-se-movilizara-el-pais
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/11/ruta-de-movilizacion-19N-2020.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/11/WhatsApp-Image-2020-11-18-at-08.04.07.jpeg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: Carlos Zea/Contagio Radio

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Este 19 de noviembre se llevará a cabo una nueva jornada de movilización nacional, ***«por la vida, la democracia y la negociación del pliego de emergencia**»*, encabezada por sindicatos, centrales obreras, estudiantes, campesinos y otros sectores sociales, un año después del inicio del Paro Nacional.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/Contagioradio1/status/1329245599238152192","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/Contagioradio1/status/1329245599238152192

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

La nueva jornada de Paro Nacional se da a un año de las movilizaciones del 21 de noviembre del 2019, en donde cientos de colombianos y colombianas salieron a las calles, exigiendo derechos a la vida, en relación a la defensa de los lideres y lideresas sociales, al territorio, esto ante las acciones extractivistas, a la educación con respecto a la matricula cero, entre otros puntos. ([¿Qué sigue? soluciones al 21N](https://archivo.contagioradio.com/que-sigue-soluciones-al-21n/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Esta vez, se exige nuevamente protección para líderes sociales, así como la negociación del pliego de peticiones que buscan incrementar el salario mínimo, implementar la matrícula cero y el retiro del proyecto de ley 010.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En Bogotá, uno de los puntos confirmados es el Parque Nacional desde donde se unirán integrantes de la Federación Colombiana de Trabajadores de la Educación ([Fecode](https://twitter.com/CutSantander/status/1329149293207445511)) desde las 9 a.m. Mientras tanto, el sindicato [obrero de trabajadores de Ecopetrol](https://twitter.com/usofrenteobrero) anunció que el punto de encuentro será el Machín de la Resistencia de Ecopetrol (Cra 13. Calle 36) a las 9:30 a.m).

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/fecode/status/1329058613164466176","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/fecode/status/1329058613164466176

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/usofrenteobrero/status/1329169193787338752","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/usofrenteobrero/status/1329169193787338752

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

Por su parte, estudiantes universitario se estarán encontrando desde las 9 a.m. en la Universidad EAN (Calle 79 \#11-45), partirán por la carrera 11 a la Universidad Pedagógica y se unirán a ellos más personas desde la Universidad Distrital (Cra. 7 \#40b-53), tiendo como punto final el Banco Agrario Av. Jiménez.

<!-- /wp:paragraph -->

<!-- wp:image {"id":92954,"sizeSlug":"large"} -->

<figure class="wp-block-image size-large">
![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/11/ruta-de-movilizacion-19N-2020-1024x1024.jpg){.wp-image-92954}

</figure>
<!-- /wp:image -->

<!-- wp:paragraph -->

El Partido Farc también confirmó que se estará uniendo al Paro Nacional con punto de concentración en el Portal del Sur, hasta Protabaco, localidad de Ciudad Bolívar y Bosa a las 6 a.m., para llegar a las 9 a.m. al monumento del Machín.

<!-- /wp:paragraph -->

<!-- wp:image {"id":92955,"sizeSlug":"large"} -->

<figure class="wp-block-image size-large">
![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/11/WhatsApp-Image-2020-11-18-at-08.04.07-1022x1024.jpeg){.wp-image-92955}

</figure>
<!-- /wp:image -->

<!-- wp:paragraph -->

El punto final de las diferentes movilizaciones en la capital será la Plaza de Bolívar desde donde intervendrán voceros y voceras del Comité Nacional de Paro, exponiendo el pliego de peticiones.

<!-- /wp:paragraph -->

<!-- wp:heading -->

Paro Nacional en otras ciudades
-------------------------------

<!-- /wp:heading -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/Asonalca_/status/1328751386910134272","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/Asonalca\_/status/1328751386910134272

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

En Barranquilla, las movilizaciones también iniciarán a las 9 a.m. en el Parque Estercita Forero (Calle 74 Cra. 43). (Le puede interesar: [Venta de papa en peajes no resuelve la crisis estructural del sector que ha quebrado a 116 mil productores](https://archivo.contagioradio.com/venta-de-papa-en-peajes-no-resuelve-la-crisis-estructural-del-sector-que-ha-quebrado-a-116-productores/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En Santa Marta se realizará una caravana de movilización desde la Universidad Cooperativa de Colombia que empezará a las 8 a.m.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En Medellín el punto de salida será el Parque de los Deseos a las 9 a.m., se realizará un plantón en el Parque de las Luces y finalizará en Ciudad del Río aproximadamente a las 4 p.m.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/AdidaSindicato/status/1329155394531438596","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/AdidaSindicato/status/1329155394531438596

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

Otras ciudades que se iniciaran movilizaciones desde las 9 a.m. son el Norte de Santander donde se realizará una marcha desde el Parque Simón Bolívar y el centro comercial Unicentro hasta el parque Santander, Arauca en Sedes de Asodar y Armenia en el Coliseo del Café.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En Pamplona partirán desde el Coliseo Chepe Acero, en Tibú desde la Cuatro hasta el Parque Central y en Ocaña desde el parque San Francisco hasta el parque Santander. Mientras que en Cali iniciará alas 5 a.m. en la estación de gasolina Texaco, pasando el puente de Sameco K1.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Además de esta jornada, también se espera que el 20 y 21 de noviembre continúen las movilizaciones, igualmente, el 23 se realizará un plantón y una velatón en conmemoración a Dilan Cruz y finalmente, el próximo 25 de noviembre, se volverá a salir a las calles en el Día Internacional de la No Violencia contra las mujeres.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/juventudpazcol/status/1328018153520193537","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/juventudpazcol/status/1328018153520193537

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:block {"ref":78955} /-->
