Title: Asesinato de Faiber Erazo causa indignación en Argelia
Date: 2015-01-19 23:03
Author: CtgAdm
Category: DDHH, Movilización
Tags: Argelia, asesianato, Elio Gentil Adrada, Faiber Erazo
Slug: asesinato-de-faiber-erazo-causa-indignacion-en-argelia
Status: published

#### Foto: Pulzo 

**[Escuchar entrevista]**: [Faiber Erazo](http://ctgaudio.megadatesystem.com/upload/0/fsKiV2tT.mp3)

El joven de 25 años de edad era muy conocido en la región y siempre se le resaltó por ser una persona honesta y trabajadora, por ello la indignación de la población del corregimiento El Plateado, tras conocerse la noticia de su muerte a manos de **militares del Batallón de infantería 56** que opera en la región.

Algunas versiones sin confirmar establecen que **Faiber habría sido golpeado por los militares** y ese hecho habría provocado la reacción de más de 3000 personas que retuvieron a 88 uniformados y los condujeron a la cabecera municipal para exigir justicia en el caso.

Por su parte, **Elio Gentil Adrada, alcalde de Argelia, afirma que se realizó un consejo de seguridad y que el caso ya está en manos del propio ejército y de la fiscalía.**

Sin embargo, ante las versiones de los militares en que se afirma que Faiber transportaba pasta de coca, Adrada afirma que el campesino era reconocido por ser un hombre trabajador y desempeñarse como arriero, lo cual desmiente parte de la versión entregada por el propio ejército en el sentido de que Faiber desobedeció una orden de alto en un retén militar.

Cabe recordar que durante el 2014 se presentaron diversas denuncias por señalamientos contra la población civil realizados por mandos militares de la región y por la amenaza y operación constante de paramilitares en varios de los corregimientos del municipio.
