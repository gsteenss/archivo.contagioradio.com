Title: Ejército respalda brigadier general investigado por falso positivo
Date: 2019-02-19 18:41
Author: AdminContagio
Category: DDHH, Nacional
Tags: ejercito, falsos positivos
Slug: ejercito-respalda-brigadier-general-investigado-falso-positivo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/02/TC-Diego-Luis-VIllegas-Munoz-770x400.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Escuela de Relaciones Civiles y Militares] 

###### [19 Feb 2019] 

[Ante las reiteradas denuncias de organizaciones sociales sobre el presunto rol del brigadier general **Diego Luis Villegas Muñoz, actualmente comandante de la Fuerza de Tarea Vulcano**, en un caso de ejecución extrajudicial, el Ejército emitió un comunicado en que defendió su ascenso y nombramiento el año pasado.]

[Según la información presentada por Human Rights Watch en el momento de su ascenso, el Brigadier general estaba señalado por el **homicidio del campesino Omer Alcides Villada el 25 de marzo de 2008 en Montebello, Antioquia**, mientras se desempeñaba como comandante del Batallón de Ingenieros “General Pedro Nel Ospina”, suscrito a la Cuarta Brigada. Las evidencias demuestran que el entonces Coronel Villegas Muñoz **firmó las órdenes de recompensas por la información que llevó al asesinato** de este campesino.]

[A finales del 2016, la Fiscalía General **expidió una orden de captura del Coronel Villegas Muñoz**, que luego fue suspendido después de que el alto mando militar firmara una acta de sometimiento a la Jurisdicción Especial para la Paz (JEP) en febrero de 2018.]

[En diciembre del año pasado, el Senado aprobó el ascenso del entonces Coronel Villegas Muñoz a rango de Brigadier general, además fue designado comandante de la Fuerza de Tarea Vulcano, a cargo de operaciones militares en la región del Catatumbo y el Norte de Santander, pese a las denuncias de las víctimas.]

[Por su parte, el Comando General del Ejército expresó en el comunicado que la fuerza militar no puede “olvidar el respeto a la presunción de inocencia y el pleno ejercicio de un debido proceso en el que se observen todas las garantías. Continuamos prestos a colaborar armónicamente con las demás instituciones del Estado y atendemos los requerimientos de cualquier jurisdicción que opere en Colombia a fin de esclarecer los hechos que la sociedad ha pedido investigar.”]

### **7 militares acusados de ejecuciones extrajudiciales fueron nombrados en el 2018** 

[El año pasado, el Comando del Ejército Nacional anunció el nombramiento de **por lo menos siete militares acusados de ejecuciones extrajudiciales**, incluyendo al Brigadier general Diego Luis Villegas Muñoz, al Estado Mayor del Ejército, según las denuncias presentadas por Human Rights Watch.]

[Los militares señalados son: **Mayor general Jorge Navarette Jadeth**, jefe de la Jefatura de Estado Mayor Generador de Fuerza; **Brigadier general Adolfo León Hernández,** comandante de Comando de Transformación del Ejército del Futuro; **Brigadier general Diego Luis Villegas Muñoz**, comandante Fuerza de Tarea Vulcano; **Brigadier general Marcos Evangelista Pinto Lizarazo**, comandante Décima Tercera Brigada; **Brigadier general Edgar Alberto Rodríguez Sánchez**, comandante de la Fuerza de Tarea Aquiles; **Brigadier general Raúl Hernández Floréz Cuervo**, comandante del Centro Nacional de Entrenamiento; y **Brigadier general Miguel Eduardo David Bastidas**, comandante Décima Brigada Blindada.]

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
