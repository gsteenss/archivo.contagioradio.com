Title: En Colombia se debe cambiar el mecanismo de ascenso en las FFMM y de Policía
Date: 2015-12-02 22:33
Category: Nacional, Política
Tags: Congreso de Colombia aprueba ascensos a militares, Ejecuciones Extrajudiciales, Fuerzas Militares de Colombia, Iván Cepeda, Policía Nacional, violación a derechos humanos
Slug: en-colombia-se-debe-cambiar-el-mecanismo-de-ascenso-en-las-ffmm-y-de-policia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/12/ASCENSO-EJERCITO.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: presidencia] 

###### [2 Dic 2015 ] 

[Frente a la posibilidad de ascenso de un total de **39 integrantes de las Fuerzas Militares, entre ellos algunos miembros activos de la Policía**, el senador Iván Cepeda solicita que cinco de estos casos sean estudiados cuidadosamente, dado que estos militares actualmente son investigados por su presunta responsabilidad en la **comisión de crímenes de lesa humanidad como ejecuciones extrajudiciales**.]

[Se trata de los ascensos del General de la Policía Luis Martínez Guzmán señalado de tener **vínculos con paramilitares y narcotraficantes**, el del Mayor General del Ejército Marco Lino Tamayo contra quien cursa una investigación con compulsa de copias frente a **obstáculos que ha interpuesto para el esclarecimiento de casos de ejecuciones extrajudiciales**. Otro de los casos sería el del Brigadier General Mauricio Zúñiga, involucrado en la Operación Andrómeda, el del Coronel Luis García a quien se le acusa de ejecuciones extrajudiciales y el del Coronel Sergio Andrés Garzón por su **participación en la masacre de Santodomingo en 1998**.  ]

[De acuerdo con Cepeda, la petición radica en la crítica al actual mecanismo de acenso que cobija a las Fuerzas Militares y la Policía Nacional y el papel que juega el Congreso en la toma de estas decisiones, pues no se está respetando la demanda de organismos internacionales frente a la **imposibilidad de que un militar involucrado en crímenes o hechos de corrupción sea ascendido** hasta que no se hayan esclarecido las investigaciones que cursen en su contra.  ]

[Pese a que se hacen “los llamados y las alertas con relación a ciertos oficiales, el **Congreso da luz verde a los ascensos y después vienen los escándalos**, ese fue el caso del General Santoyo, el caso del General Flavio Buitrago, fue el caso del General Francisco Patiño… y podría ser una lista muy larga en la que hemos hecho alertas tempranas con relación a lo que va a ocurrir” agrega Cepeda.]

[Por lo que se estima la posibilidad de proponer un proyecto de ley que busque reformar el mecanismo actual de ascenso de cara a un eventual escenario de posconflicto, los **controles rigurosos** **son los que deben orientar las decisiones en materia de ascensos**, de ellos deben gozar quienes se encuentren exentos de investigaciones frente a hechos violatorios de los derechos humanos o la ley, asegura Cepeda.   ]
