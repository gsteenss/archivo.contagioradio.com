Title: Protesta pacífica de la USO se ve amenazada por el ESMAD
Date: 2016-11-23 13:51
Category: Movilización, Nacional
Tags: Ecopetrol, Represión del ESMAD, union sindical obrera
Slug: protesta-pacifica-la-uso-se-ve-amenazada-esmad
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/06/esmad-presidente-santos.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Kienyke] 

###### [23 Nov 2016] 

Las movilizaciones y protestas pacíficas de la USO - Unión Sindical Obrera en **Puerto Salgar y Coveñas, completan 24 horas, 94 integrantes de la organización se encuentran asentados en estaciones de gasolina y petróleo crudo** exigiendo respuestas frente al futuro de Ecopetrol, la seguridad energética y el autoabastecimiento del país.

A través de un comunicado emitido en horas de la mañana, dirigentes denuncian que el Gobierno no ha atendido formalmente las solicitudes y por el contrario denuncian que **hay un gran numero de efectivos del ESMAD en ambos puntos del país y han impedido el ingreso de agua y alimentos a los manifestantes. **Le puede interesar: [Trabajadores de la uso se toman instalaciones de Ecopetrol.](https://archivo.contagioradio.com/trabajadores-de-la-uso-se-toman-instalaciones-de-ecopetrol/)

### **¿Qué acciones se han adelantado?** 

Dirigentes de la organización sindical han acudido a los senadores Alberto Castilla, Alexander López, Sofía Gaviria, Inti Asprilla, Alirio Uribe y Jorge Enrique Robledo, para que en la sesión del 22 de Noviembre de la comisión V del Senado se manifestara al Ministro de Minas Germán Arce las **“serias y fundamentadas preocupaciones sobre Ecopetrol”.**

### **¿Cuáles son las denuncias?** 

En la misiva señalan que la manifestación busca propiciar escenarios de discusión y concertación para abordar temas como la **venta de campos petroleros** fijada para el próximo el 25 de Noviembre, la aprobación de la **venta de la empresa de polipropilenos del Caribe Propilco,** la pretensión del **cambio en el modelo de prestación de salud** de Ecopetrol a sus empleados y el **empeoramiento del modelo pensional.**

También protestan por la **negligencia en la renovación tecnológica de la refinería de Barrancabermeja**, los anuncios de la privatización de la **transportadora Cenit**, que también se encarga del manejo de los oleoductos, la privatización y decaimiento de las investigaciones del **Instituto Colombiano de Petróleo** y** **la **sustitución del terminal Néstor Pineda de Cartagena** por uno privado en Puerto Bahía.

\[caption id="attachment\_32712" align="alignnone" width="502"\]![ESMAD - USO](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/Captura-de-pantalla-2016-11-23-a-las-13.13.43.png){.size-full .wp-image-32712 width="502" height="502"} Agentes del ESMAD llegan a Coveñas y Puerto Salgar\[/caption\]  
\[caption id="attachment\_32713" align="alignnone" width="503"\]![Dirigente de la USO](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/Captura-de-pantalla-2016-11-23-a-las-13.44.57.png){.size-full .wp-image-32713 width="503" height="503"} Dirigente de la USO en Cantagallo\[/caption\]  
\[caption id="attachment\_32715" align="alignnone" width="502"\]![Manifestantes de la USO](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/Captura-de-pantalla-2016-11-23-a-las-13.45.06.png){.size-full .wp-image-32715 width="502" height="281"} Trabajadores de la USO\[/caption\]

###### Reciba toda la información de Contagio Radio en [[su correo]
