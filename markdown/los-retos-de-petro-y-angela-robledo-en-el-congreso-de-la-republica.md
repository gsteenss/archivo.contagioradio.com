Title: Los retos de Gustavo Petro en el Congreso de la República
Date: 2018-06-18 14:58
Category: Nacional, Política
Tags: acuerdos de paz, Congreso de la República, Elecciones presidenciales 2018, Gustavo Petro
Slug: los-retos-de-petro-y-angela-robledo-en-el-congreso-de-la-republica
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/06/petro.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: La Vanguaardia] 

###### [18 Jun 2018] 

Luego de que Iván Duque ganara las elecciones presidenciales, Gustavo Petro y Ángela María Robledo irán al Congreso de la República, a ocupar curules en Senado y Cámara de Representantes, hecho que, para Iván Cepeda, senador del Polo Democrático, **significa continuar la lucha por la defensa de los derechos humanos, la implementación de los Acuerdos de paz y evitar que se hagan modificaciones a la JEP** que cambien la esencia de la misma.

Otros de los restos que se deberán afrontar desde este lugar en el que Petro y Robledo ya estuvieron, cada uno 7 años respectivamente, será la lucha contra la corrupción, que, según Cepeda, fue un mensaje **“muy claro”** que dieron las personas que votaron por ellos. **En ese sentido, se esperan propuesta que realicen reformas al sistema político electoral**. (Le puede interesar: [" Hay incertidumbre frente a lo que sucederá con la JEP"](https://archivo.contagioradio.com/%C2%A8hay-incertidumbre-sobre-lo-que-ocurrira-con-la-jep%C2%A8-camila-moreno/))

Referente a la conquista de los derechos sociales, Cepeda manifestó que uno de los más importantes es la garantía de la educación pública y gratuita, en todos sus niveles. Además, se suma la lucha por **la desprivatización del sistema de salud y la defensa del ambiente**.

“En ninguna otra oportunidad la izquierda y los sectores habíamos tenido una presencia tan fuerte en el Senado y la Cámara. **Somos bancadas con las cuales habrá que contar para decidir, somos la tercera parte del Congreso**” afirmó Cepeda.

De igual forma, otro de los retos de los candidatos de la Colombia Humana, será generar alianzas mucho más fuertes con otros sectores que no hacen parte de la izquierda, al interior del Congreso. En esa medida, **Cepeda señaló que iniciar por apoyar la campaña de la Consulta Anticorrupción, es fundamental.**

### **El balance de las elecciones 2018** 

De acuerdo con el senador, el balance de la votación del pasado 17 de junio es que, si bien hay un triunfo de la derecha con 10 millones de votos, **“paralelamente, en un hecho inédito, hay un resultado significativo, que expresa un cambio en la política colombiana**” reflejados en los 8 millones de votos para una opción no tradicional, de centro izquierda y que representaba Gustavo Petro y Ángela Robledo.

En ese sentido, para Cepeda, el paso a seguir con este resultado es que se “plasme en una realidad política” con la conformación de una gran coalición por la paz, que tenga la suficiente cohesión y persistencia en el tiempo para obtener un resultado importan en el año 2019 en donde se realizaran elecciones locales a gobernaciones, alcaldías y consejos. (Le puede interesar:["Duque no desconozca el camino ya andado": FARC](https://archivo.contagioradio.com/duque-no-desconozca-el-camino-ya-andado-farc/))

###### Reciba toda la información de Contagio Radio en [[su correo]
