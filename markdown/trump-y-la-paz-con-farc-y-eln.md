Title: Donald Trump y el proceso de paz con las FARC y el ELN
Date: 2016-11-10 18:12
Category: Paz
Slug: trump-y-la-paz-con-farc-y-eln
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/proceso-de-paz-y-donald-trump.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: contagioradio] 

###### [10 Nov 2016]

Luego de la elección de Donald Trump como presidente de Estados Unidos una de las preguntas para Colombia tiene que ver con el respaldo que la administración Obama dio al proceso de Paz de La Habana, y el respaldo, incipiente, para las conversaciones con el ELN ¿**Qué definirá Trump de cara al proceso de paz en Colombia?**

Con lo afirmado por Trump en su campaña no sería de extrañar que **algunas posturas de ese gobierno cambiaran respecto a lo que se está negociando en este momento**. Por ello la premura en el tiempo para resolver un nuevo acuerdo con las FARC y la necesidad de dejar puntos amarrados en las conversaciones con el ELN.

### **El proceso de paz con las FARC y Donlad Trump** 

Para nadie es un secreto que la llegada de Bernard Aronson, enviado especial de ese gobierno para las conversaciones de paz de La Habana, sirvió mucho para avanzar en materia de solución al problema de las drogas ilícitas, así como para resolver algunos puntos cruciales en torno a la Jurisdicción Especial de Paz.

Tampoco es secreto que el “guiño” de Obama al proceso de paz y el llamado “Plan Paz Colombia” eran casi condiciones para avanzar en La Habana.

Para Carlos Lozano, analista y director del Semanario Voz, el papel de Estados Unidos hasta el momento ha pasado por el respaldo a la política tradicional, es decir **un espaldarazo político pero que no se ha materializado** en asuntos como la participación directa de Simón Trinidad en a mesa de conversaciones.

### **La lucha antinarcóticos y el terrorismo** 

Por su parte a partir del 20 de Enero de 2017 el gobierno de Trump sería sustancialmente diferente, si se apega a las promesas de campaña, en las que augura mano dura en la política internacional, lo que **podría representar pronunciamientos en cuanto a los avances de la lucha contra el narcotráfico**.

Analistas afirman que EEUU aceptó, implícitamente, el fracaso de la política actual. Para Lozano, el asunto debe trabajarse como un problema de salud, filosofía que está plasmada en el acuerdo de Cartagena.

También podría cambiar la postura que, en este momento se está trabajando para que las FARC dejen de hacer parte de la lista de organizaciones terroristas, iniciativa a la que se ha opuesto el partido republicano. También se debe tener en cuento que el discurso del terrorismo y las **posturas del Centro Democrático podrían tener más dientes en el gobierno de Trump**.

Aunque, en sus fundamentos, un nuevo gobierno en Estados Unidos no debería afectar el acuerdo o su re negociación, si se espera que esté listo antes de Diciembre para evitar cualquier presión adicional y debería estar listo, si o si, antes de que culmine el 2016.

### **El proceso de paz con el ELN y Donald Trump** 

Según Carlos Lozano, el proceso de paz con el ELN, tendría que arrancar de una vez por todas y el gobierno debería dejar de dilatar el incio y apegarse a los acuerdos previos a los que llegó con esa guerrilla. Según Nicolás Rodríguez “Gabino” hubo un acuerdo que el gobierno cambió sin mutuo acuerdo e impuso la condición de la liberación de Odín Sánchez que no estaba pactada sino para el transcurso del primer mes de conversaciones.

Aunque este proceso cuenta con el respaldo de Chile, Ecuador, Venezuela y Brasil, habría que considerar que **tanto Venezuela como Brasil se están enfrentando a momentos diferentes de cara a sus relaciones con Estados Unidos**. Chile y Ecuador se mantendrían estables, Venezuela y Brasil tienen contextos diferentes.

### **Los países garantes, EEUU y el ELN** 

En Venezuela, la presión de EEUU contra el gobierno de **Nicolás Maduro** podría incrementarse, y la inestabilidad política afectaría la posibilidad de acompañamiento por parte de ese gobierno al proceso. Cabe decir que Venezuela se eligió como sede pensando en acercar las conversaciones a sectores de la sociedad civil.

**En Brasil, el gobierno que se comprometió con las conversaciones de paz fue el de Dilma Roussef,** y aunque no favorecería para nada a Temer cambiar de postura frente a estas conversaciones dado el mínimo respaldo popular que tiene, es evidente que no es lo mismo sentarse a hablar en la casa del PT que en la casa del derechista Movimiento Democrático.

La influencia de EEUU, **más que respaldar o ser indiferente, puede convertirse en negativa**, tanto por la presión a los países garantes en sus asuntos internos, como porque estará en camino el proceso de implementación de los acuerdos con las FARC y esa coyuntura puede influir, si no se aseguran algunos puntos, por ejemplo, la política minero energética, uno de los centrales para el ELN.

**Todo está por verse y este tiempo, hasta el 20 de Enero, día de la posesión de Trump, puede servir para ajustar sus posturas a lo que llaman la “política real”**, esa que está definida también por pactos económicos e influenciada por líneas políticas de los partidos tanto en Estados Unidos como en la Unión Europea, incluso en Rusia.
