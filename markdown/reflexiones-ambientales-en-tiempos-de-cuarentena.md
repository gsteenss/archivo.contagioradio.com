Title: Reflexiones ambientales en tiempos de cuarentena
Date: 2020-04-07 16:00
Author: Censat
Category: CENSAT, Opinion
Tags: Ambiente, cuarentena, reflexión, Sociedad
Slug: reflexiones-ambientales-en-tiempos-de-cuarentena
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/04/na_5abd06d4de74d.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/04/PruebaIII.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/04/b16fe4e0.png" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/05/Foto_Andrés-Camilo-Gómez-Giraldo.jpeg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/05/Foto_Andrés-Camilo-Gómez-Giraldo-1.jpeg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:heading {"level":5} -->

##### \*Primera parte

<!-- /wp:heading -->

<!-- wp:paragraph -->

La actual pandemia del Coronavirus COVID – 19 es un síntoma de los desequilibrios ambientales producto de una visión que considera la naturaleza como una mercancía.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Podríamos afirmar que el impacto de la enfermedad epidémica no habría alcanzado tal magnitud, si no fuera por décadas de neoliberalismo que ha privilegiado los negocios sobre la vida, fomentando el agronegocio y el extractivismo, deforestando los bosques y destruyendo fundamentales ecosistemas hidrícos.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En primer lugar, partamos de la numerosa evidencia que ubica la causa estructural de la pandemia en relación con la destrucción ambiental de los ecosistemas y la biodiversidad. Dicha documentación hace explícita la relación de las enfermedades zoonóticas -las que se transmiten entre animales y seres humanos por ejemplo a través de virus, insectos, bacterias, hongos, etc.- y la forma en que irrumpimos, y alteramos los flujos y funciones biológicas de la naturaleza.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Coincidiendo con Ribeiro, importante ecologista latinoamericana, son tres las causas concomitantes y complementarias que han producido todos los virus infecciosos, estrechamente conectados con la deforestación, y la destrucción y alteración de los hábitats:

<!-- /wp:paragraph -->

<!-- wp:list {"ordered":true} -->

1.  La cría intensiva y extensiva de animales, y el abuso de antibióticos para su crianza, en especial pollos, pavos, cerdos y vacas
2.  Las plantaciones de monocultivos, producto de una agricultura industrial y química
3.  El crecimiento descontrolado de los centros urbanos y las industrias que mantienen su subsistencia.

<!-- /wp:list -->

<!-- wp:heading {"level":5} -->

##### Según el Informe del **PNUMA** “*Fronteras. Nuevos temas de interés ambiental*”:

<!-- /wp:heading -->

<!-- wp:paragraph -->

Los hábitats degradados pueden desencadenar procesos evolutivos acelerados y diversificar enfermedades, ya que los patógenos encuentran condiciones favorables para una fácil propagación tales como el hacinamiento e interacción entre animales, tráfico ilegal de especies, incremento poblacional de insectos y la invasión de territorios degradados por asentamientos urbanos. En este sentido, a menor biodiversidad, mayor diversidad de enfermedades zoonóticas.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por ejemplo, es bien conocido que las represas incrementan los brotes de leishmaniasis, producto de la deforestación extensiva y los espejos de agua estancada que incrementa la proliferación de vectores (mosquitos), o el caso del Ébola en África causado por la fragmentación del bosque tropical que produjo el hacinamiento de animales salvajes, convirtiéndose en el caldo de cultivos para el virus.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":5} -->

##### Por consiguiente, no es fortuito que en las últimas décadas, los titulares de las noticias mundiales sean testigos de varias enfermedades zoonóticas emergentes como el Ébola, la gripe aviar y porcina, la fiebre del Nilo Occidental y el virus del Zika.

<!-- /wp:heading -->

<!-- wp:paragraph -->

Por otro lado, y sin desconocer la gravedad de la crisis ocasionada por la pandemia, la actual situación planetaria se ha convertido en una oportunidad sin igual para reflexionar sobre las necesarias transformaciones de la humanidad en cuanto a sus formas de estar en el mundo y por ende en sus relaciones con la naturaleza.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Ante las medidas planetarias tomadas para hacer frente a la crisis como la reducción de la movilidad de personas y vehículos, son conmovedoras las imágenes en diferentes regiones del planeta de animales paseando tranquilamente por calles vacías, o regresando a los puertos y playas recuperadas.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Somos testigos de la significativa disminución de la contaminación, tanto en los niveles de emisiones contaminantes en el aire como el vertimientos en cuencas. No hay duda que un freno al crecimiento económico se ha convertido en una oportunidad para que la vida animal retome espacios urbanizados. Estas imágenes de recuperación de los ecosistemas demuestran que con voluntad política sería posible hacer frente a la crisis ambiental planetaria. 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Partamos de la aclaración que estas imágenes no nos conducen a concluir que el ser humano es una plaga y que la solución a la crisis ecológica es la reducción de la población. En cambio, estas situaciones descritas confirman que el modo de producción-distribución-consumo que impone unos ritmos acelerados desemboca de forma ineludible en la debacle ambiental.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El sistema capitalista, en donde se privilegia el crecimiento económico, ha demostrado su histórico fracaso para enfrentar la degradación ambiental, y en este momento,  de enfrentar la amenaza de una enfermedad contagiosa. Los tiempos de cuarentena permiten reflexionar sobre la necesidad de reestructurar los sistemas de producción, replantear los sistemas financieros y cambiar los patrones de consumo, simplificando los estilos de vida.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Ante la duda de qué pasará apenas termine esta emergencia global la sociedad debería tomar la iniciativa, como lo está haciendo para enfrentar la crisis, y exigir acciones que propendan por mantener condiciones similares: menos emisiones, menos consumo, menos contaminación y mejores condiciones para las demás formas de vida.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":5} -->

##### Por último, más allá del aislamiento, la emergencia provocada por la pandemia constata que estamos ligados a la naturaleza, en una fundamental interconexión bajo diferentes niveles.

<!-- /wp:heading -->

<!-- wp:paragraph -->

**En primer lugar**, porque el virus ha sido producto de la sistemática degradación ambiental del planeta. **En segundo lugar**, porque un virus que se empieza a propagar en China se ha esparcido en todo el planeta en pocos meses. **En tercer lugar**, y sobre todo, porque en consonancia con un principio de humildad, se determina que un agente infeccioso con una dimensión de nanoparticulas puede diezmar a uno de los seres más complejo que ha producido la evolución y es parte de la naturaleza: el ser humano. Las transformaciones del planeta inician con librarse de una vez por todas de una visión soberbia del mundo, y permitirnos transitar por nuevos estilos de vida, reconocer otras sensibilidades, cambiar cotidianidades y franquear nuevos caminos como sociedad.  

<!-- /wp:paragraph -->

<!-- wp:image {"id":82956,"width":137,"height":71,"sizeSlug":"large","linkDestination":"custom"} -->

<figure class="wp-block-image size-large is-resized">
[![Censat agua viva realiza una serie de editoriales llama "Reflexiones ambientales en tiempos de cuarentena](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/04/b16fe4e0.png){.wp-image-82956 width="137" height="71"}](https://censat.org/)  

<figcaption>
</figcaption>
</figure>
<!-- /wp:image -->

<!-- wp:heading {"level":6,"customTextColor":"#054966"} -->

###### “Reflexiones ambientales en tiempos de cuarentena” son una serie de editoriales que se esfuerzan por pensar los cambios que se están produciendo en el planeta ante la emergencia global. Desde la esperanza y una visión crítica intentan superar las primeras respuestas de miedo y pánico con el objetivo de construir horizontes colectivos y comunitarios para sociedades sustentables.  

<!-- /wp:heading -->

<!-- wp:paragraph -->

Mas columnas de[CENSAT agua viva](https://archivo.contagioradio.com/censat-agua-viva/)

<!-- /wp:paragraph -->
