Title: Con movilizaciones y acciones legales defenderán revocatoria contra Peñalosa
Date: 2017-05-04 12:49
Category: Movilización, Nacional
Tags: Consejo Nacional Electoral, Revocatoria Enrique Peñalosa
Slug: mas-de-600-mil-firmas-expresan-insatisfaccion-hacia-gobierno-de-penalosa
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/05/revoquemos-penalosa.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Unidos Revocaremos a Peñalosa] 

###### [04 May 2017] 

Una decisión del Consejo Nacional Electoral podría redefinir los requisitos para usar el mecanismo de revocatoria por parte de la ciudadanía, y frenaría el proceso que ya se adelanta contra el alcalde Enrique Peñalosa. Sin embargo, los comités que hacen la recolección de firmas señalaron que de ser aprobada esta ponencia se atentaría contra la voz de las **más de 600 mil personas que se han expresado a favor de la revocatoria.**

El punto que mayor preocupación genera, de la ponencia presentada por el presídete del Consejo Nacional Electoral, es que los comités que soliciten un proceso de revocatoria deberán presentar “**material probatorio de los incumplimientos de los mandatarios locales y departamentales para justificar la solicitud**”. Le puede interesar:["Enrique Peñalosa desconoce reservas de los Cerros Orientales"](https://archivo.contagioradio.com/enrique-penalosa-sigue-desconociendo-reservas-forestales-en-los-cerros-orientales/)

Para Carlos Carrillo, vocero del comité Unidos Revocaremos Peñalosa, la insatisfacción se evidencia en **las más de 600 mil firmas que ya se han recogido, en tiempo record**, para la revocatoria del Alcalde, además, expresó que esta estrategia se veía venir, “Enrique Peñalosa es irrespetuoso de la democracia, no tiene como defenderse con argumentos y tienen que recurrir a la arbitrariedad”, afirmó el vocero.

De igual forma, Carrillo cuestiono el accionar del CNE, al afirmar que “**no se dedican a aplicar la ley sino a tratar de lavarle la cara a políticos que atentan contra la democracia**, entonces el Consejo Nacional sirve para salvarlos y está vez pasan la raya, para salvar a que Enrique Peñalosa”. Le puede interesar:["Comités de revocatoria a Peñalosa prometen llegar a un millón de firmas"](https://archivo.contagioradio.com/comites-de-revocatoria-buscan-recolectar-un-millon-de-firmas-contra-penalos/)

El siguiente pasó que establecerán los comités de revocatoria **será el acompañamiento de equipos jurídico que están a la espera de la decisión del CNE** para tomar medidas legales. El próximo 9 de mayo se realizará una movilización que tendrá como punto de encuentro el Planetario desde las 5:00pm para defender la valides del proceso de revocatoria contra Peñalosa y los mecanismos de la democracia.

<iframe id="audio_18504420" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_18504420_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
