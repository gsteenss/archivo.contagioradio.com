Title: En Guatemala ganaron los votos nulos y en blanco
Date: 2015-10-27 17:48
Category: El mundo, Política
Tags: Corrupción en Guatemala, Cúpula militar Jimmy morales, Elecciones Guatemala, Jimmy morales nuevo presidente, Jornada electoral en Gautemala
Slug: en-guatemala-los-votos-nulos-y-en-blanco-superaron-los-votos-validos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/10/Jimmy_Morales_contagioradio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto:rubenluengas.com 

<iframe src="http://www.ivoox.com/player_ek_9184443_2_1.html?data=mpallpmYd46ZmKiak5qJd6KpkpKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRic%2BfqNrO1srRpc3VjNHc1ZDas9Xj1JDb19HTt4ztjMrbjcfQpc%2FX0JDg19XJtsLm0NOYztTXb46ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Andrea Ixtziu, periodísta] 

###### [27 oct 2015] 

Las elecciones de este 25 de octubre en Guatemala que dieron el **triunfo de Jimmy Morales con un 67.44% de votos** en una jornada en donde pocas personas se acercaron a las urnas. Los resultados fueron marcados por el escepticismo y la falta de confianza en cualquiera de los dos candidatos.

Guatemala tiene un total de **7 millones 500 mil personas inscritas en el padrón electoral** y de estas acudieron a votar 4 millones, de los cuales 2 millones 700 mil personas votaron por Jimmy Morales, el **abstencionismo y voto en blanco de la jornada fue  de 3 millones quinientos mil**, cifras que permiten ver “la legalidad del gobierno, más no la legitimidad”, indica Andrea Ixtziu “sentando un precedente en la historia de Guatemala”.

Del total del padrón electoral votó entre el 49% y 50% y de ese número los votos nulos y en blanco fueron mayores con un abstencionismo del 45% lo cual “es grave, si sumamos los votos en blanco, los cuales superan la cantidad de votos”, indica Andrea Ixtziu.

En distintos ámbitos preocupa que “**alcaldes, diputados y empresarios que han sido procesados estos últimos meses, volcaron su apoyo total a Jimmy Morales, para conservar su manto de impunidad**”, y así no ser parte de las investigaciones del ministerio público lo cual, podría dar **continuidad a la corrupción** del poder tradicional.

Además preocupan las violaciones de derechos humanos hacia comunidades indígenas, campesinos y población civil. Sin embargo, según la periodista y analista política con este nuevo movimiento ha surgido “una nueva masa crítica de organización juvenil” que va a tener oposición real ante la falta de garantías.

Los nexos del conservador Jimmy morales con militares quienes son los fundadores de su partido, el Frente de Convergencia Nacional, **el cual está en contra de la judicialización de militares en el conflicto armado** demuestran que la “misma línea militar que tuvo Otto Pérez Molina" queda ligada al poder, además los militares están ligados con la "corrupción" similar a la del gobierno anterior.

**Además el nuevo gobierno se encontrará con un “agujero negro fiscal”** que dejó el gobierno de Otto Pérez Molina, Jimmmy Morales presentó un plan de gobierno de cinco páginas en el que “**nunca se vio que se hubiese preparado un plan de manejo de Estado**”, y preocupa la posibilidad de la “reducción del gasto social prioritario”, es decir que habrá **una inversión “menor de un dólar diario”** por persona. Un ejemplo del “costo social de la corrupción” y que ameritaría la demanda desde la sociedad para que  se exijan más y mejores condiciones de vida.
