Title: Estatuto de oposición está listo para su trámite en el Congreso
Date: 2017-02-15 17:12
Category: Nacional, Política
Tags: acuerdos de paz, colombia, ELN, Estatuto de oposición, FARC, MOE
Slug: estatuto-de-oposicion-saldaria-una-deuda-de-26-anos-con-la-constitucion-de-1991
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/10/marcha-por-la-paz1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: contagioradio] 

###### [15 Feb 2017]

La Misión de Observación Electoral, MOE, calificó el estatuto de la oposición como un proyecto que **cumple con los estándares democráticos y salda una deuda con la constitución de 1991**. Sin embargo señala que hacen falta algunas claridades como la definición de oposición y los mecanismos que garantizarán los derechos de los partidos políticos que se declaren opuestos a los gobiernos.

Para la MOE “Este estatuto avanza realmente en **darle un mayor valor de sentido al ejercicio de la oposición en el país, así como garantías reales para mejorar la calidad del control político** que estas organizaciones hacen tanto en el Congreso, como en las Asambleas y Concejos” según lo declaró Alejandra Barrios, quién también acompañó la presentación del proyecto en el congreso.

### **La definición de oposición, una de las primeras claves** 

Entre los temas señalados por la MOE, para ser completados, están la definición en la que se deberían tener en cuenta 3 criterios. Un primero que considere **la oposición como derecho político fundamental** y por consiguiente tutelable, un segundo criterio es que se presenten alternativas y posturas críticas respecto de las del gobierno y se entienda el ejercicio de oponerse como mecanismo de control político y fiscalización más allá de lo electoral.

Además sería necesario que se establezcan mecanismos para la declaratoria de oposición al interior de los partidos y que se garantice la aplicación de los mecanismos de oposición con instituciones claras al interior del gobierno nacional como de los gobiernos locales, además de “**abrir espacios a los partidos políticos de oposición en la mesa directiva del Congreso**”.

Por último señalan que es necesario que se establezcan una serie de mecanismos para **sancionar a personas en los partidos políticos que se han declarado en oposición y que acepten cargos en los organismos controlados**, lo cual garantizaría que el ejercicio sea efectivo y se desincentive la afiliación de personas acostumbradas a prácticas poco sanas. (Le puede interesar [Sistema electoral colombiano permite elegir criminales](https://archivo.contagioradio.com/sistema-electoral-elegir-criminales-35907/))

Así las cosas, de ser aprobado y terminado el estatuto de la oposición propuesto, se daría cumplimiento al punto del acuerdo de paz sobre participación política, una de las tareas encomendadas a varias organizaciones desde antes de firmado el acuerdo final y que venía siendo estudiado y consultado desde hace cerca de 6 meses. ([Lea tambien Listo el equipo que cambiará la reglas del juego electoral en Colombia](https://archivo.contagioradio.com/listo-el-equipo-que-cambiara-las-reglas-del-juego-electoral/))

<iframe id="audio_17056951" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_17056951_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
