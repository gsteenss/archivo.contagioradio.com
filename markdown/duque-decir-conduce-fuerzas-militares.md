Title: Duque debe decirle al país cómo conduce a las fuerzas militares
Date: 2019-05-20 18:48
Author: CtgAdm
Category: Entrevistas, Paz
Tags: ejercito, falsos positivos, Fuerzas militares, general, New York Times
Slug: duque-decir-conduce-fuerzas-militares
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/150217-Foto-Reunion-Suiza-01.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Descontamina Colombia] 

Recientemente una columna del New York Times cuestionó órdenes que se estarían dando al interior de las Fuerzas Militares para aumentar el número de bajas en combate; según las fuentes consultadas por el medio, con este tipo de órdenes (y otras tendientes en la misma dirección) **se estaría allanando el camino para que se presenten nuevos casos de ejecuciones extrajudiciales.** Esta denuncia se sumó a preocupaciones que se han presentado sobre las actuaciones de la Fuerza Pública en casos como el del excombatiente Dimar Torres.

Para el g**eneral en retiro Rafael Colón,** el país está entrando en una transición en la que intenta interpretar lo que  significa el Acuerdo de Paz pactado en 2016; en ese sentido, recalcó que son vitales las decisiones emprendidas desde las altas esferas del poder para que el Ejército apoye la implementación del proceso de paz. (Le puede interesar: ["Duplicar resultados a toda costa, la orden a militares en el gobierno Duque"](https://archivo.contagioradio.com/duplicar-resultados-a-toda-costa-la-orden-a-militares-en-el-gobierno-duque/))

### <iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/video.php?href=https%3A%2F%2Fwww.facebook.com%2Frafaelpachecopastrana%2Fvideos%2F2187396894676073%2F&amp;show_text=0&amp;width=261" width="261" height="476" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>  
**Dimar Torres, la peor cara de la Fuerza Pública** 

El pasado 22 de abril habitantes de Convención, Norte de Santander, denunciaron que tras escuchar disparos y notar la desaparición de un habitante de la zona, acudieron al Ejército para aclarar lo sucedido. En el campamento encontraron un hueco abierto a modo de fosa común, y tras una búsqueda en el lugar dieron con el cuerpo de Dimar Torres; un excombatiente de las FARC en cuyo cuerpo se veían rastros de tortura y sevicia. (Le puede interesar: ["Señalan a Ejército Nacional de asesinar al excombatiente Dimar Torres"](https://archivo.contagioradio.com/excombatiente-dimar-torres-habria-sido-asesinado-por-ejercito-en-el-catatumbo/))

Tras el hecho, **el General Diego Luis Villegas, comandante en su momento de la Fuerza de Tarea Vulcano pidió perdón por el hecho**; asumiendo que miembros de su brigada cometieron el terrible hecho. Pero tras su declaración, se denunció que al interior de las Fuerzas Militares la misma no fue bien recibida, incluso llegándose a pensar en su retiro obligado del servicio; a lo que se sumaron los [audios](https://www.youtube.com/watch?time_continue=83&v=eh0ipAK7QFA) de un superior jerárquico que condenaba el pedido de perdón. (Le puede interesar:["Denuncian intención de destituir al general Villegas, quien pidió perdón por asesinato de Dimar Torres"](https://archivo.contagioradio.com/denuncian-intencion-de-destituir-al-general-villegas-quien-pidio-perdon-por-asesinato-de-dimar-torres/))

Colón aseveró que **el accionar del general Villegas al pedir perdón por los hechos fue un acto noble y "muy loable"**, y agregó que los audios en los que se recrimina este pedido de perdón son reprochables, "porque de ninguna manera está en consecuencia con las enseñanzas que recibimos quienes portamos el uniforme". Por el contrario, el General (r) criticó el papel que han asumido los altos mandos militares en temas como la protección a líderes sociales, y el apoyo al Acuerdo de Paz.

### **Duque debe decirle al país cómo conduce su fuerza y cuál es su posición frente al Acuerdo de Paz**

Uno de los puntos en los que fue critico el general (r) Colón fue la protección a los líderes sociales; puesto que la función de las Fuerzas Militares es garantizar la vida de todas las personas y proteger los territorios de la criminalidad. Aunque expuso que esta era una tarea difícil, tomando en cuenta la geografía colombiana; indicó que aún hacía falta trabajar en este punto. (Le puede interesar: ["Coronel Jorge Armando Pérez será investigado por asesinato de excombatiente Dimar Torres"](https://archivo.contagioradio.com/coronel-jorge-armando-perez-sera-investigado-por-asesinato-de-excombatiente-dimar-torres/))

La otra critica del General fue hacia el presidente Iván Duque y su "dicotomía" sobre el Acuerdo de Paz; puesto que Colón cree que **el gobierno actual no ha sido claro sobre su apoyo a la implementación del Acuerdo, y ello ha repercutido en la posición de la tropa sobre el mismo**. Tomando en cuenta que, como lo recordó Colón, el jefe máximo de las fuerzas militares es el Presidente y ellas obedecen su mandato, Duque "debe darle cara al país y decirle cómo conduce su fuerza y cuál es su disposición frente" a la paz.

<iframe id="audio_36108800" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_36108800_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
