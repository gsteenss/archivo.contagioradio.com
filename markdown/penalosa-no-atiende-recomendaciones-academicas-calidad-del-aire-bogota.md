Title: Peñalosa no atiende recomendaciones académicas sobre calidad del aire en Bogotá
Date: 2019-02-19 14:37
Author: AdminContagio
Category: Ambiente, DDHH
Tags: Alcaldía de Bogotá, Contaminación del Aire, Enrique Peñalosa
Slug: penalosa-no-atiende-recomendaciones-academicas-calidad-del-aire-bogota
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/02/bogota-2754208_1920-770x400.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: alejobaron] 

###### 18 Feb 2019 

Ante la contingencia ambiental ocasionada por la mala calidad de aire en Bogotá, diez profesores de universidades colombianas presentaron una serie de recomendaciones a la administración del Alcalde Enrique Peñalosa para afrontar la actual crisis y prevenir escenarios similares en el futuro. Sin embargo la administración distrital no ha implementado ninguna de esas recomendaciones.

Según la carta, la emergencia ambiental actual se debe en parte, por las condiciones meteorológicas considerada típicos entre los meses de enero y marzo, sin embargo, sostuvo que **la causa principal de la contaminación de aire es producido por los vehículos de motor diésel, tal como los **[**camiones, buses y incluso, las motocicletas**.]

Estos vehículos son los principales fuentes de emisión de material particulado, un aerosol de sustancias orgánicas e inorgánicas que tiene efectos nocivos para el ambiente y para la salud, incluyendo **enfermedades respiratorios, latidos irregulares, infartos de miocardio no mortales y la muerta prematura en personas con enfermedades cardíacas o pulmonares**.

Luis Carlos Belalcázar, profesor de la Universidad Nacional de Colombia y uno de los firmantes de la carta, resaltó la urgencia de implementar un control sobre las emisiones contaminantes de vehículos diésel. Según las recomendaciones de los docentes, es necesario renovar la flota de buses de Transmilenio y SITP además de adoptar sistemas de control y tecnologías más limpias en vehículos pesados.

Mientras que esta medida se pueda implementar, surgieron implementar un operativo de control en vía más frecuente y efectivo además de garantizar una adecuada inspección técnico mecánica, en el cual se verifican las condiciones mecánicas, ambientales y de seguridad de los vehículos particulares y públicos. "[**Muchos vehículos tienen revisión técnico mecánica, pero uno no se explica porque en la calle eso vehículos tan altas cantidades de contaminantes**," dijo Belalcázar.]

### **La responsabilidad de la Alcaldía** 

El experto de ingeniería química y ambiental le dió la razón a la representante a la Cámara Ángela María Robledo, quien publicó en su página de Twitter que la Alcaldía de Peñalosa compartía la responsabilidad por el deterioro de la calidad del aire capitalino. Belalcázar afirmó que **la política de la Administración Distrital afecta directamente la calidad de aire debido a que no tomo en cuenta las estrategias propuestas por la academia para mejorar la calidad de aire.**

Entre estas decisiones señaladas por el profesor y la representante son la política de derogar el plan de filtros de diésel para vehículos de Transmilenio en el 2016, anular el decreto para introducir taxis eléctricos a las vías en el 2017, de comprar 700 buses diésel en el 2018 y por último, de plantear la tala de más de 30.000 árboles este año.

Finalmente, el profesor concluyó que puede ser muy tarde para implementar e[l filtro de diésel dado que los vehículos circulan desde hace 10 años y pueden ser muy viejos para soportar un filtro de material particulada. Sostuvo que la medida que se debe tomar a largo plazo es la renovación del parque automotor.]

<iframe id="audio_32688846" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_32688846_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
