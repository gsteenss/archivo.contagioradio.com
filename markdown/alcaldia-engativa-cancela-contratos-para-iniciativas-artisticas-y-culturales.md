Title: Alcaldía Engativá cancela contratos para iniciativas artísticas y culturales
Date: 2016-06-28 13:30
Category: Cultura, Nacional
Tags: alcaldía engativá, cultura en engativá, programas culturales Bogotá, universidad de cundinamarca
Slug: alcaldia-engativa-cancela-contratos-para-iniciativas-artisticas-y-culturales
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/06/Engativá.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Engativá Cultura ] 

###### [28 Junio 2016 ]

[Por lo menos mil ciudadanos, entre niños, jóvenes y adultos mayores de organizaciones culturales de la localidad de Engativá, marchan este martes hasta la alcaldía local para rechazar la **cancelación de los contratos que han permitido desde 2014 la ejecución de programas artísticos y culturales** que actualmente benefician a más de 3 mil pobladores. ]

Según Andrés Borrero, uno de los jóvenes beneficiarios, la nulidad de estos contratos fue anunciada por la Alcaldesa Local, Ángela Vianney Ortiz, el pasado martes, argumentando presuntas inconsistencias por parte de la Universidad de Cundinamarca, entidad con la que el Fondo de Desarrollo Local, suscribió los contratos; sin embargo, **la institución ha presentado todos los soportes financieros durante los años de ejecución**.

La iniciativa que empezó en enero de 2014, ha beneficiado a más de 3 mil personas en los Núcleos de Formación Artística y en más de 75 iniciativas de arte y cultura que han sido premiadas y reconocidas a nivel distrital. **Sí los programas no continúan, por lo menos 1.538 beneficiarios perderán la posibilidad de terminar su formación** en las áreas de danza, artes plásticas, artes visuales, música, radio, teatro y arte circense.

Los participantes de la marcha vestidos de negro, recrean un ataúd humano para realizar la **entrega formal de derechos de petición dirigidos a la Alcaldía Local, la Personería y la Contraloría** para que se retracte la nulidad de los contratos, que afectan el desarrollo artístico y cultural de la localidad.

<iframe src="http://co.ivoox.com/es/player_ej_12055771_2_1.html?data=kpedl5qbe5Khhpywj5WaaZS1lpeah5yncZOhhpywj5WRaZi3jpWah5yncaLixdeSpZiJhZrnjKfc1NfJttCfjpC9w9fYrcTd0cbb1sqPsoa3lIqvo8jQqdDnjMnSjcvTts7VxM6SpZiJhpTijoqkpZKns8_owszW0ZC2pcXd0JCah5yncZU.&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)]] 

 
