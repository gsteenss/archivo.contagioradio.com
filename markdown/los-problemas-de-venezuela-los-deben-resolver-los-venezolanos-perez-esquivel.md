Title: "Los problemas de Venezuela los deben resolver los venezolanos" Pérez Esquivel
Date: 2015-03-11 20:11
Author: CtgAdm
Category: El mundo, Otra Mirada
Tags: Adolfo Pérez Esquivel, Democracia en Venezuela, EEUU, Solidaridad internacional con Venezuela por declaración de amenaza para la seguridad nacional de EEUU
Slug: los-problemas-de-venezuela-los-deben-resolver-los-venezolanos-perez-esquivel
Status: published

##### Foto:Wikipedia.org 

##### **Entrevista con[ Adolfo Pérez Esquivel]:** 

<iframe src="http://www.ivoox.com/player_ek_4200194_2_1.html?data=lZedkpadeI6ZmKiakpyJd6KkmpKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRaZOmhqigh52Vq9bdzcbgjbPJq9PV1JDhy8rSqc%2BfwsjQx9jTb8KfytPhx9HNq8bixM7OjdLNsMrowteSlJePcYarpJKw0dPYpcjd0JC%2Fw8nNs4yhhpywj5k%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

Para Adolfo Pérez Esquivel, premio nobel de paz, las recientes sanciones de Estados Unidos contra funcionarios venezolanos son una clara muestra de los intereses de injerencia de ese país sobre América Latina. Según Esquivel, este tipo de acciones ya las ha desarrollado esta estrategia en Honduras y en Paraguay.

Esquivel deja claro que como ya ha dicho la UNASUR, **"(...) es Venezuela quién debe resolver sus propios problemas sin intervencionismo estadounidense(.**..)**"**. Y en todo caso quien debe responder es la **UNASUR y la CELAC** como organismos regionales **Latinoamericanos válidos para resolver la crisis en Venezuela**.

Esquivel rechaza las acusaciones de medios de información contra UNASUR y recuerda las campañas contra Hugo Chávez en medios de comunicación masivos europeos y estadounidenses. No era posible leer un periódico de ninguna parte del mundo que no hiciera referencia a la situación de Venezuela en ese momento, sin  embargo, la fuerza de los hechos lograba desmontar esas campañas, señala el nobel argentino.

Las actuaciones del gobierno de Venezuela siempre han sido en el marco de la democracia, incluso cuando Chávez perdió las únicas elecciones que perdió, salió a reconocer la derrota de manera pública, caso contrario a Estados Unidos. "Estados Unidos no es un país democrático, de lo contrario no estaría bombardeando en otras partes para defender sus intereses"

Este Lunes Barack **Obama calificó a Venezuela** como **amenaza para la seguridad** nacional y emitió **sanciones contra siete funcionarios** del gobierno venezolano, muchas han sido las muestras de solidaridad desde distintas regiones y gobiernos del mundo.

Un ejemplo de ello son las **sanciones contra la fiscal** que está llevando los casos judiciales relacionados con la **crisis de las Guarimbas**.

Hoy la presidenta del Tribunal Supremo de Justicia Venezolana, **Gladys Gutiérrez** leyó un **comunicado** donde el Poder Judicial **rechaza las sanciones de EEUU contra miembros de la fiscalía y funcionarios del estado venezolano**.

En el comunicado se califica las sanciones como injerencia por parte del gobierno estadounidense y **rechaza** también todo tipo de **acciones desestabilizadoras internas**. Además, reconoce la labor del pueblo venezolano por mantener la paz y la democracia.

**Comunicado del TSJ de Venezuela sobre sanciones de EEUU:**

\[embed\]https://www.youtube.com/watch?v=A3l\_UFgDP94\[/embed\]
