Title: Se acerca el II Foro por la paz de Colombia en Uruguay #YoReporto
Date: 2015-05-13 15:59
Author: CtgAdm
Category: Nacional, Paz, yoreporto
Slug: se-acerca-el-ii-foro-por-la-paz-de-colombia-en-uruguay-yoreporto
Status: published

###### [13 May 2015] 

Con la experiencia y el saldo positivo que dejó el **I Foro por la paz de Colombia** realizado en Porto Alegre – Brasil en mayo de 2013, y con la firme y vigente convicción de que la construcción de la paz es una tarea que compete a toda la región y los pueblos del mundo, durante los días 5, 6 y 7 de junio del año en curso, se fortalecerán las demandas de soberanía, democracia y paz con justicia social sumadas a la desmilitarización del país para confluir de nuevo en la segunda versión del **Foro por la paz de Colombia, esta vez a realizarse en Montevideo – Uruguay.**

Con 4 paneles principales y 9 mesas para discutir temas fundamentales para la sociedad colombiana y latinoamericana, como el desplazamiento forzado, la construcción que hacen las comunidades campesinas, indígenas y afrodescendientes de la tan anhelada paz con justicia social, el trabajo de las mujeres como constructoras de paz y de verdad, la lucha por la libertad de los prisioneros y prisioneras políticas, y la necesidad de la democratización de los medios de comunicación entre muchos otros, se llevará a cabo este Foro que va a congregar delegaciones de todos los países del continente.

La realización de este foro se suma a la voluntad real que existe en la región de construir una paz con justicia social en Colombia, y a la defensa del proceso de paz que adelantan el gobierno nacional y la insurgencia de las FARC – EP, para lograr **darle solución política al conflicto social** que cuenta ya con más de cinco décadas de duración, abrazando también la exigencia del pueblo colombiano de declarar un Cese Bilateral al Fuego y la necesaria construcción de una Asamblea Nacional Constituyente que hagan del acuerdo de La Habana un proyecto realizable, que sea garantía de una real transformación de las estructuras sociales y políticas que han motivado el conflicto y su continuación.

Por información actualizada día a día pueden buscarnos en Facebook como **“II Foro por la paz de Colombia”**, en Twitter [@foropazcolombia](https://twitter.com/foropazcolombia) y visitar nuestro blog <http://forumpelapaznacolombia.blogspot.com/> o al correo electrónico forumpelapazcolombia@gmail.com
