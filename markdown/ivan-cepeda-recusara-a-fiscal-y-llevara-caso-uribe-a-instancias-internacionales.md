Title: Iván Cepeda recusará a fiscal y llevará caso Uribe a instancias internacionales
Date: 2020-09-05 11:15
Author: AdminContagio
Category: Actualidad, Política
Tags: Álvaro Uribe Vélez, Fiscal Francisco Barbosa., Fiscalía General de la Nación, Iván Cepeda
Slug: ivan-cepeda-recusara-a-fiscal-y-llevara-caso-uribe-a-instancias-internacionales
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/08/Cepeda-Uribe.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: Iván Cepeda Twtiter

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

A través de una rueda de prensa, el senador Iván Cepeda, acreditado como víctima en el proceso que se adelanta contra el exsenador Álvaro Uribe Vélez por presunta manipulación de testigos, anunció que interpondrá acciones legales contra diferentes personas vinculadas al proceso por sus posibles actuaciones irregulares, esto tras conocerse **el nombramiento del fiscal Gabriel Jaimes Durán, como el encargado de adelantar la investigación contra el expresidente.**

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/IvanCepedaCast/status/1301904029673455616","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/IvanCepedaCast/status/1301904029673455616

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph {"align":"justify"} -->

“Cuando un funcionario es recusado, no lo es posible realizar ninguna actuación. Hacerlo es obrar abiertamente en contra de la ley”, señaló el abogado del congresista, Reinaldo Villalba.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Tras conocerse la asignación del caso a Gabriel Jaimes Durán, quien actualmente funge como fiscal coordinador de la Fiscalía Delegada ante la Corte Suprema de Justicia, el senador Cepeda anunció que denunciará al fiscal Barbosa ante la Comisión de Acusación de la Cámara por **"haber burlado la solicitud de recusación de la parte civil, de la cual él estaba debidamente notificado”** y que fue radicada ante la Corte Suprema.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Cabe señalar que Jaimes Durán ha trabajado durante más de 25 años tanto en la Fiscalía como en la Procuraduría y ha sido relacionado por ser muy cercano al exprocurador Alejandro Ordóñez, no solo por trabajar como procurador delegado ante el Ministerio Público en Asuntos Penales, y procurador delegado ante la Corte Suprema de Justicia sino por sus posturas ideológicas y religiosas. [(Lea también: Transparencia Internacional alerta sobre concentración del poder en gobierno Duque)](https://archivo.contagioradio.com/transparencia-internacional-alerta-sobre-concentracion-del-poder-en-gobierno-duque/)

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Acciones de Cepeda en el ámbito internacional

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Además de esta acción, el [senador](https://twitter.com/IvanCepedaCast)Cepeda informó que denunciará ante la Procuraduría a 16 congresistas del Centro Democrático que a través de derechos de petición habrían exigido información del proceso judicial en contra de Uribe, extralimitando sus funciones al realizar estos cuestionamientos.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

De igual forma señaló, llevará el caso ante la **Comisión Interamericana de Derechos Humanos (CIDH) y presentará un informe a Michelle Bachelet**, **alta Comisionada de la ONU para los Derechos Humanos p**ara así evidenciar la presión que se ejerció sobre la Corte Suprema de Justicia. [(Le recomendamos leer: Renuncia de Uribe Vélez a su curul puede ser una maniobra para dilatar el proceso y generar impunidad)](https://archivo.contagioradio.com/renuncia-alvaro-uribe-velez-no-responder-justicia-victimas/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Por su parte, el fiscal general Francisco Barbosa, anunció que no se declarará impedido, ni aceptará ninguna recusación, frente al manejo del caso del exsenador Uribe, argumentando que no existe una amistad entre él y el expresidente, ni hace parte del Centro Democrático, esto pese a la conocida relación que existe entre él y el presidente Iván Duque, pupilo del expresidente Uribe. [(Lea también: Iván Cepeda anuncia que recusará al Fiscal Barbosa en proceso contra Álvaro Uribe)](https://archivo.contagioradio.com/ivan-cepeda-recusara-a-fiscal-barbosa-en-proceso-contra-alvaro-uribe/)

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
