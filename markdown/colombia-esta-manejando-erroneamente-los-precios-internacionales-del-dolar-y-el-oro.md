Title: Colombia está manejando erroneamente los precios internacionales del Dolar y el Oro
Date: 2015-08-25 15:00
Category: Economía, Nacional
Tags: Banco de la República, Bolsa de valores, colombia, economia, extractivismo, Libardo Sarmiento, Oro, petroleo, Precios internacionales del Dolar, Precios Internacionales del Oro
Slug: colombia-esta-manejando-erroneamente-los-precios-internacionales-del-dolar-y-el-oro
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/08/55d99025c4618847788b45e6.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Taringa.net 

<iframe src="http://www.ivoox.com/player_ek_7598824_2_1.html?data=mJqmmp2WeI6ZmKiak52Jd6KplpKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRh9Dg0NLPy8aPqdTohqigh6aVb87Vz8rXw9PIs4zZ09fc0MrFscbi1cqYztTXb9HmxsjW0diPrc%2Foxteah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [ Libardo Sarmiento, Economista] 

###### [25 Ago 2015]

En un informe de "The economic Times" se ha dado a conocer que el **Banco de la República está vendiendo las reservas de oro colombianas reduciéndolas a 3.77 Toneladas** en menos de seis años, el Economista Libardo Sarmiento afirma que esta acción termina afectando a todos en Colombia, un país que enfocó su economía en base al extractivismo.

El día de ayer se denominó en el mercado bursátil como “Lunes negro”, debido a que algunas de las bolsas de valores más importantes del mundo (China, UE y Brasil) tuvieron caídas muy fuertes. China que venía siendo el motor de la economía mundial desde hace algunos años empezó a devaluar su moneda, causando una “política del sálvese quien pueda”, teniendo efectos negativos para nuestra economía.

“Si se cae la economía mundial, caen las exportaciones, para este año el déficit entre exportaciones e importaciones puede llegar en este 2015 a U\$16.188 millones”, según indicó Sarmiento. ** **Al vender las reservas de oro, el **banco de la república está teniendo una mirada a corto plazo **obviando que las monedas tienden a devaluarse y el oro a revaluarse llegando a máximos históricos de la mano de los grandes especuladores mundiales.

El banco de la república quien maneja oficialmente desde la constitución del 91 las reservas internacionales, busca con esta estrategia no perder reservas con la caída del oro (1000 dólares onza Troys), sin embargo Frente a una recesión económica mundial si el país tiene reservas de oro **sería más fácil salvaguardarse, caso distinto a tener un 95% en monedas internacionales**, en donde frente a una crisis el desplome sería acelerado.

“Es una mala decisión que vuelve vulnerable no solo al banco de la República, sino además a la economía colombiana” Indicó Libardo Sarmiento.
