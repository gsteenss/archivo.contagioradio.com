Title: Entregan a la JEP nuevo informe sobre lo ocurrido en la Operación Orión
Date: 2018-10-16 17:20
Author: AdminContagio
Category: Judicial, Nacional
Tags: #MontoyaDigaLaVerdad, conmemoración, JEP, operación orion, planton
Slug: informe-sobre-operacion-orion
Status: published

###### [Foto: Contagio Radio] 

###### [16 Oct 2018] 

Este miércoles 17 de octubre continuará la audiencia del **General (r) Mario Montoya** **ante la Jurisdicción Especial para la Paz (JEP)**; el proceso ocurrirá en el marco de la conmemoración de los 16 años de la **Operación Orión**, razón por la cual las víctimas presentaron un informe sobre los hechos en los que está relacionado el uniformado.

Al cumplirse 16 años de la operación que se desarrolló en Medellín los días 16 y 17 de octubre de 2002, la **organización Mujeres Caminando por la Verdad**, integrada por familiares de víctimas de ejecuciones extra judiciales y desaparición forzada, realizaron la **entrega de un informe a la JEP, la Comisión de la Verdad y la Comisión de Búsqueda de personas Desaparecidas en el que se exponen algunos de sus casos.**

Durante el evento que inició a las 9 de la mañana en el convento de la Madre Laura, en Medellín, también realizaron un acto simbólico en que abrazaron a otros familiares y sobrevivientes de estos hechos violentos. (Le puede interesar: ["Los casos por los que debería responder Montoya ante la JEP"](https://archivo.contagioradio.com/casos-responder-montoya-ante-jep/))

> El 16 y 17 de oct de 2002 la Fuerza Pública en connivencia con paramilitares cometieron 75 homicidios fuera de combate, cerca de 100 desapariciones y 450 detenciones ilegales en la Operación Orión. <https://t.co/N3ZuvOhp7o> [\#OrionNuncaMas](https://twitter.com/hashtag/OrionNuncaMas?src=hash&ref_src=twsrc%5Etfw) [pic.twitter.com/X0TlOgMYXD](https://t.co/X0TlOgMYXD)
>
> — Sin Olvido (@SINOLVIDO) [16 de octubre de 2018](https://twitter.com/SINOLVIDO/status/1052254403237830657?ref_src=twsrc%5Etfw)

<p>
<script src="https://platform.twitter.com/widgets.js" async charset="utf-8"></script>
</p>
### **\#MontoyaDigaLaVerdad** 

Este miércoles, cuando se reanude la audiencia contra Montoya, **la organización Mujeres Caminando por la Verdad se reunirá nuevamente en el Centro Administrativo "La Alpujarra" desde las 9 de la mañana,** donde realizarán un plantón para exigirle al General que diga la verdad. (Le puede interesar: ["Montoya diga la verdad:Exigen las víctimas"](https://archivo.contagioradio.com/montoya-diga-la-verdad-exigen-victimas/))

**Con la misma exigencia en Bogotá, organizaciones sociales realizarán un plantón frente a la JEP (Carrera 7 \# 63-44), a las 7 de la mañana**, hora en que está previsto el inició de la audiencia.(Le puede interesar: ["El 17 de octubre se reanudará audiencia contra General Montoya"](https://archivo.contagioradio.com/el-17-de-octubre-se-reanudara-audiencia-contra-general-montoya/))

\[caption id="attachment\_57505" align="aligncenter" width="428"\][![Mario Montoya](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/10/Captura-de-pantalla-2018-10-16-a-las-4.58.29-p.m.-612x601.png){.wp-image-57505 width="428" height="420"}](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/10/Captura-de-pantalla-2018-10-16-a-las-4.58.29-p.m..png) Foto: @SINOLVIDO\[/caption\]

###### [Reciba toda la información de Contagio Radio en] [[su correo] [[Contagio Radio]
