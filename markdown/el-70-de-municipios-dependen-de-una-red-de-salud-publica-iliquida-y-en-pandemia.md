Title: El 70% de municipios dependen de una red de salud pública ilíquida y en pandemia
Date: 2020-07-01 23:02
Author: CtgAdm
Category: Actualidad, DDHH
Tags: Amenazas contra médicos, Covid-19, Reforma a la Salud, Salud
Slug: el-70-de-municipios-dependen-de-una-red-de-salud-publica-iliquida-y-en-pandemia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/07/EbcnQsqXsAA75bV.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","fontSize":"small"} -->

Foto: Ministerio de Salud

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Aunque el Gobierno anunció desde enero de 2020 que existiría un incremento de 8,12% frente al del 2019 para el sector salud y que contaría con un presupuesto de 31.8 billones de pesos y con la reciente crisis de la Covid-19 el Ministerio de Hacienda señaló que el Gobierno aportaría cerca de 7 billones de pesos adicionales, la crisis al interior de los centros hospitalarios públicos y privados y la ausencia de garantías para sus profesionales continúa.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Mientras el Observatorio Fiscal de la Universidad Javeriana señala que la mayoría del dinero destinado para atender la pandemia no se sabe a qué rubros está siendo dirigida, senadores como Wilson Árias denuncian que mientras el Gobierno prometió destinar 29 billones de pesos presupuestados para atender la emergencia del Covid-19, solo se han ejecutado 3.6 millones de los cuales 2.3 han ido a parar a los bancos mientras el resto ha sido destinado a la atención social, lo que permite cuestionarse si con los 24.9 billones que no han sido ejecutados podrían destinarse a favorecer la lucha contra la pandemia, entre ellos al sector salud, primera línea de defensa contra la Covid-19. [(Le recomendamos leer: FFMM y bancos, son las prioridades de Iván Duque durante la pandemia: Coeuropa)](https://archivo.contagioradio.com/ffmm-y-bancos-son-las-prioridades-de-ivan-duque-durante-la-pandemia-coeuropa/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

**Olga Lucia Zuluaga, directora ejecutiva de la Asociación Colombiana de Hospitales Públicos** señala que los centros hospitalarios enfrentan una situación de iliquidez a nivel municipal y son los hospitales de alta y mediana complejidad los que se llevan la peor parte pues son los que además de realizar su trabajo cotidiano tuvieron que asumir la preparación para el pico máximo de la pandemia, es decir generar procesos paralelos de atención a la Covid como su ampliación de instalaciones y equipos de bioseguridad.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

La directora manifiesta que esta es una " verdad a medias, porque si bien es cierto se hizo un avance en los recursos para pagos y el Gobierno giro recursos a las EPS de forma anticipada para cubrir los meses de marzo y abril, directamente **las EPS no hicieron ningún giro adicional a las instituciones médicas**, como parte del pago de una deuda que asciende a cerca de 2.1 billones de pesos. [(Lea también Policía gastaría más de 9.500 millones para comprar armas en plena pandemia)](https://archivo.contagioradio.com/policia-gastaria-mas-de-9-500-millones-para-comprar-armas-en-plena-pandemia/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Cuando el Gobierno habla de recursos y se habla del giro realizado para cubrir todo aquello que no es Covid-19, Zuluaga señala que "la cifra está entre 1.2 billones y el giro mayor ha sido de 1.4 billones de pesos es decir que realmente ese dinero corresponde a lo que generalmente se gira, lo que se ha adicionado ha sido para pago de personal con un total de 200.000 millones de pesos para aquellos hospitales que tenían retraso en pagos de nómina. [(Le recomendamos leer: Medidas de gobierno en Colombia agravarán los efectos del COVID 19)](https://archivo.contagioradio.com/en-colombia-ya-tenemos-los-ingredientes-perfectos-para-una-pandemia-aun-peor/)

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Las zonas rurales, continúan siendo las más vulnerables

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Con respecto a la atención a las poblaciones más vulnerables frente a la pandemia, Zuluaga señala que **es necesaria una reforma estructural al sistema de salud pues "en el 70% de los municipios del país, los hospitales públicos somos los únicos prestadores de salud, en zonas dispares y alejadas** donde la deuda e inequidad es el común denominador". Lugares como Tumaco, Amazonas o Urabá, donde se requiere que los hospitales no dependan de la venta de sus servicios, - que disminuyó su recaudo en un 40% desde el inicio de la pandemia - sino que el Ministerio de Salud asuma parte de la atención de esta población.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Frente a la reducción de medidas de confinamiento y sus consecuencias en el aumento de casos, Zuluaga señala que **el territorio colombiano es heterogéneo** por tanto aunque existen zonas que están preparadas para hacer frente al pico de la pandemia como la región andina, hay otros lugares donde se espera no llegue la pandemia, aunque aclara que **no solo se debe depender de las UCIs, sino del talento humano que está al frente de las unidades**, pues incluso si existieran el doble de recursos y material, el sistema puede colapsar si no se adoptan las medidas de distanciamiento y conciencia social. [(Le puede interesar: Entre la pandemia las fantasías mortales de Duque)](https://archivo.contagioradio.com/entre-la-pandemia-las-fantasias-mortales-de-duque/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

A propósito, Carolina Corcho,  médica psiquiatra, vicepresidenta de la Federación Médica Colombiana señala que **se requieren médicos intensivistas preparados, cuyo aprendizaje tarda cerca de dos años** y mientras el Gobierno habla de implementar 9.000 UCIs, en el país se estima que existen 1.200 intensivistas lo que lleva a preguntarse sobre la capacidad real para atender la emergencia, **pues en caso de que se cumpla la promesa de los ventiladores no hay personal suficiente para atender a pacientes que ingresen.**

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

A su vez advierte que el déficit de la salud en Colombia puede superar los 15 billones de pesos y mientras hay trabajadores a los que se les debe casi un año de salario **solo hasta este año se giraron recursos a 500 hospitales públicos de un total de 900 que cubrieron los pagos de trabajadores hasta el 31 de marzo, sin contar pagos previos al 31 de diciembre de 2019 o posteriores al mes de abril de 2020.**

<!-- /wp:paragraph -->

<!-- wp:quote -->

> "El problema no es la plata y que no la haya, el problema es que no está llegando a la destinación de la pandemia"
>
> <cite>Doctora Carolina Corcho</cite>

<!-- /wp:quote -->

<!-- wp:paragraph {"align":"justify"} -->

"El problema no es la plata y que no la haya, el problema es que no está llegando a la destinación de la pandemia", y agrega que este alivio solo se aplicó al sector publico, mientras al sector privado al que pertenecen otras 19.000 institutos prestadores de servicios de salud (IPS) por tanto no se está resolviendo la problema a la que ahora se suman [agresiones e intimidaciones por realizar su trabajo](https://www.facebook.com/watch/?v=1150818098596930). [(Lea también: Mejorar imagen de Duque con recursos para la paz: una muestra de insensibilidad y desconexión hacia el país)](https://archivo.contagioradio.com/mejorar-imagen-de-duque-con-recursos-para-la-paz-una-muestra-de-insensibilidad-y-desconexion-hacia-el-pais/)

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Continúan las agresiones contra profesionales de la salud

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Según la vicepresidenta de la Federación Médica Colombiana, tras realizar un estudio sobre el bienestar del sector salud, **1 de cada 5 profesionales expresaba discriminación y el 40% manifestó deseos de renunciar,** no solo porque sienten temor por el contagio sino por las precarias condiciones laborales y agresiones por parte de la ciudadanía que "atribuye las debilidades del sistema a los doctores porque nadie entiende la estructura".

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

De igual forma señala que se han conocido agresiones vinculadas a grupos armados y de tipo paramilitar que han declarado al personal de salud como objetivo militar y pese a que se ha pedido abrir las investigaciones, no existen resultados, "no solamente no nos pagan, nos obligan a firmar contratos de menos valor, también tenemos miedo porque nos podemos contagiar sino que además nos amenazan, esas son las condiciones a las que estamos sometidos", denuncia Carolina Corcho.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Como advertencia final, señala que otra de las problemática es que **no solo son necesarias las pruebas aleatorias para detectar pacientes asintomáticos sino que so se está realizando una mirada del virus en tiempo real, es decir mientras se debería tomar un tiempo de 24 a 72 horas para tomar decisiones de aislamiento, este tipo de acciones puede tomar semanas,** "no depende de los individuos requiere de medidas como el testeo masivo y aleatorio, aquí no decimos que no hay que tener auto cuidado sino hay que tomar decisiones y no exonerar al gobierno de su responsabilidad" frente a una pandemia que al cierre de esta nota alcanzó los 102.009 casos de Covid-19. [(Le recomendamos leer: "El coronavirus va en cohete, y este Gobierno en mula")](https://archivo.contagioradio.com/coronavirus-va-en-cohete-y-este-gobierno-va-en-mula/)

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->
