Title: Miles de personas en Colombia se manifiestan por el cierre de la cárcel Tramacua
Date: 2016-02-22 20:08
Category: DDHH, Judicial
Tags: crisis carcelaria, La Tramacua, valledupar
Slug: miles-de-personas-en-colombia-se-manifiestan-por-el-cierre-de-la-carcel-tramacua
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/01/TRAMACUA.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: El Pilón 

<iframe src="http://co.ivoox.com/es/player_ek_10541730_2_1.html?data=kpWilpabd5Ghhpywj5WZaZS1kZqah5yncZOhhpywj5WRaZi3jpWah5ynca7dzcrgjcnJb9HZ09jc0MbXb8bijKjcztTRpsrVjNjSjdLFssraysrg1sbSb9Hj05DSzpDHrcahhpywj6jTstXVyM7cjbfFqMrjjJKSmaiReA%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Gloria Silva, Comité Solidaridad Presos Políticos] 

###### [22 Feb 2016] 

A inicios del mes de febrero, cuando se esperaba que la cárcel “Tramacua” fuera cerrada por las evidentes fallas que presenta, se demuestra que se trata de un centro de atentados contra los derechos Humanos de las personas recluidas, el Tribunal Administrativo del Cesar decidió creer en la intensión manifiesta del INPEC de solucionar los problemas que se presentan, sin embargo, nada se ha hecho hasta el momento y la situación no tiende a cambiar, por el contrario a empeorar.

Es por eso que este 23 de febrero desde las 10 de la mañana en **Bogotá, Bucaramanga, Valledupar, Medellín, Cúcuta, Popayán, Ibagué y en países como Canadá y Estados Unidos se realizarán acciones en solidaridad con los presos de La Tramacua, por medio de plantones y otras jornadas culturales.**

Así mismo, los prisioneros de la cárcel y la Fundación Comité de Solidaridad con Presos Políticos, preparan un escrito dirigido a la Corte Constitucional con copia al Tribunal Contencioso Administrativo de Cundinamarca para que se cierre este centro carcelario.

**“El Tribunal ni siquiera mencionó cuáles eran las quejas, informes y toda la situación documentada”**, dice Gloria Silva, abogada del Comité de Solidaridad con Presos Políticos, quien agrega que el plantón, demostrará que el pueblo colombiano no está de acuerdo con prologar la tortura a la que está sometida la población reclusa… la gente se está dando cuenta de la situación y la repudia esperando que las decisiones de Corte se cumplan  y no sea un saludo a la bandera”.

Cabe recordar que tras 1 año del fallo de tutela T282 en el que la Corte Suprema de Justicia ordena al Estado colombiano y particularmente al INPEC implementar acciones eficaces para que cesen las violaciones de derechos humanos y mejoren las condiciones de reclusión al interior de la cárcel La Tramacua, los prisioneros políticos denuncian que persisten las prácticas de tortura, el desabastecimiento de agua y la precaria atención en salud, por lo que el centro carcelario ya debía haberse cerrado, pues durante la visita de verificación se obtuvo **“pruebas fehacientes de que esa cárcel es un centro de castigos de tortura”**, señala la abogada.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u)  o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](https://archivo.contagioradio.com/). 
