Title: "Hojas de papel volando", poesía en escena
Date: 2018-07-10 15:49
Category: eventos
Tags: poesia, recitales, teatro
Slug: hojas-de-papel-volando
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/07/hojas-de-papel.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Hojas de papel Volando 

###### 10 Jul 2018 

Los textos transformados en versos integran la propuesta de "Hojas de papel volando", un recital de poesía inspirado en el libro homónimo de Patricia Ariza, escritora y dramaturga colombiana, dirigido y realizado por la artista cubana Roxana Pineda, que se presenta del 12 al 14 de julio en Bogotá.

Este montaje escénico unipersonal, se realizó gracias al trabajo previo de investigación realizado por la actriz, quien en compañía de la maestra Ariza, estableció 3 líneas de trabajo: teatro, feminismo y política, encaminadas a conservar el equilibrio estético y funcional sin alterar el universo poético contenido en el libro.

Durante hora y media los asistentes podrán disfrutar de un recital sin sosiego, en un viaje donde sentimientos como el amor, la alegría, el dolor y la memoria, permitiran establecer un encuentro personal con la actriz y los versos originales en los que se inspira, siendo a su vez un homenaje a su creadora.

Las funciones tendrán lugar en la Sala Seki Sano, calle 12 N° 2-65, a partir de las 7:30 de la noche. La boletería para las tres funciones está habilitada, con un costo para público general de \$20.000 y estudiantes \$15.000, en la taquilla de la Corporación Colombiana de teatro o en la [página web](https://goo.gl/HWvVyt) de la corporación.

<iframe src="https://www.youtube.com/embed/0HnHtz3ZDFc" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>
