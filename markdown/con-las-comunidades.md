Title: Con las comunidades
Date: 2020-05-05 20:36
Author: CtgAdm
Slug: con-las-comunidades
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/05/Captura-de-pantalla-2020-05-05-a-las-15.53.24.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

Comunidades

Con las comunidades
-------------------

Información actualizada de las noticias y apuestas por la paz y el territorio de las comunidades de las diferentes regiones de Colombia

[  
![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/04/logo-contagio-radio-web-1.png){width="201" height="76"}](https://archivo.contagioradio.com/)

NOTAS
-----

</i>","nextArrow":"","autoplay":false,"autoplaySpeed":5000,"rtl":false}' dir="ltr"&gt;  
Guajira sin agua por desvío de arroyo Bruno y sin ayudas para enfrentar al COVID  
[](https://archivo.contagioradio.com/guajira-sin-agua-por-desvio-de-arroyo-bruno-y-sin-ayudas-para-enfrentar-al-covid/)  
Catatumbo necesita un acuerdo humanitario, no erradicación forzada  
[](https://archivo.contagioradio.com/catatumbo-sigue-clamando-por-un-acuerdo-humanitario-que-ponga-fin-a-la-guerra/)  
La apuesta por la vida de comunidades campesinas en Putumayo  
[](https://archivo.contagioradio.com/la-apuesta-por-la-vida-de-comunidades-campesinas-en-putumayo/)  
Por tercera vez comunidades insisten a Gobierno y ELN por un acuerdo humanitario  
[](https://archivo.contagioradio.com/por-tercera-vez-comunidades-insisten-a-gobierno-y-eln-por-un-acuerdo-humanitario/)  
Ausencia estatal tiene al borde de la hambruna a líderes comunales de Tumaco  
[](https://archivo.contagioradio.com/ausencia-estatal-tiene-al-borde-de-la-hambruna-a-lideres-comunales-de-tumaco/)  
El relleno Doña Juana: la piedra de Sísifo que sigue rodando sobre el sur de Bogotá  
[](https://archivo.contagioradio.com/el-relleno-dona-juana-la-piedra-de-sisifo-que-sigue-rodando-sobre-el-sur-de-bogota/)  
Más de 2.500 indígenas de Antioquia en riesgo de desplazamiento  
[](https://archivo.contagioradio.com/mas-de-2-500-indigenas-de-antioquia-en-riesgo-de-desplazamiento/)  
El abandono estructural a las comunidades indígenas en Putumayo  
[](https://archivo.contagioradio.com/el-abandono-estructural-a-las-comunidades-indigenas-en-putumayo/)  
Comunidades del Pacífico exigen a Gobierno y grupos armados una inmediata salida negociada al conflicto  
[](https://archivo.contagioradio.com/comunidades-del-pacifico-exigen-a-gobierno-y-grupos-armados-una-inmediata-salida-negociada-al-conflicto/)

#### COVID-19 y COMUNIDADES

##### Narran como están viviendo la situación de la pandemia mundial

https://youtu.be/X\_L5Vdgh0Akhttps://youtu.be/JwnwIxuy-1Ihttps://youtu.be/Vd6tu-dPw\_ohttps://youtu.be/AdBdS0zC8U0https://youtu.be/kB\_TF6g0Ugghttps://youtu.be/GLWZe9oWH1U  
Nosotros apostamos por la vida y la seguridad de nuestro territorio porque queremos vivir, queremos que nuestros hijos e hijas vivan acá  
![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/08/putumayo.jpg){width="800" height="543" sizes="(max-width: 800px) 100vw, 800px" srcset="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/08/putumayo.jpg 800w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/08/putumayo-300x204.jpg 300w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/08/putumayo-768x521.jpg 768w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/08/putumayo-370x251.jpg 370w"}  
Wilson Medina  
Zona de Reserva Campesina Perla Amázonica - Putumayo  
Ofrecieron mercados y dinero, sin embargo, la comunidad mantuvo su rotunda decisión de no aceptar este tipo de sobornos y en negarse a la minería, ahora su vida corre riesgo  
![Indígenas en Putumayo](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/04/Pueblos-indígenas-en-Putumayo.jpg){width="696" height="462" sizes="(max-width: 696px) 100vw, 696px" srcset="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/04/Pueblos-indígenas-en-Putumayo.jpg 696w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/04/Pueblos-indígenas-en-Putumayo-300x199.jpg 300w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/04/Pueblos-indígenas-en-Putumayo-370x246.jpg 370w"}  
José Martín Barros Gil  
Secretario de la comunidad de Arimaka  
</i>","nextArrow":"","autoplay":true,"autoplaySpeed":5000}' dir="ltr" data-typing="1"&gt;  
![Ejército pone en riesgo a población de Piamonte, Cauca](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/05/Erradicación-de-Cultivos-de-Coca-en-Catatumbo-150x150.jpg "Ejército pone en riesgo a población de Piamonte, Cauca"){width="50" height="50" sizes="(max-width: 50px) 100vw, 50px" srcset="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/05/Erradicación-de-Cultivos-de-Coca-en-Catatumbo-150x150.jpg 150w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/05/Erradicación-de-Cultivos-de-Coca-en-Catatumbo-512x512.jpg 512w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/05/Erradicación-de-Cultivos-de-Coca-en-Catatumbo-310x310.jpg 310w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/05/Erradicación-de-Cultivos-de-Coca-en-Catatumbo-230x230.jpg 230w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/05/Erradicación-de-Cultivos-de-Coca-en-Catatumbo-360x360.jpg 360w"} 19:23  
[Ejército pone en riesgo a población de Piamonte, Cauca](https://archivo.contagioradio.com/ejercito-pone-en-riesgo-a-poblacion-de-piamonte-cauca/)  
![Asesinan a firmante del Acuerdo de Paz en Bello, Antioquia](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/05/Wilder-Marin-150x150.jpg "Asesinan a firmante del Acuerdo de Paz en Bello, Antioquia"){width="50" height="50" sizes="(max-width: 50px) 100vw, 50px" srcset="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/05/Wilder-Marin-150x150.jpg 150w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/05/Wilder-Marin-512x512.jpg 512w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/05/Wilder-Marin-310x310.jpg 310w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/05/Wilder-Marin-230x230.jpg 230w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/05/Wilder-Marin-360x360.jpg 360w"} 13:43  
[Asesinan a firmante del Acuerdo de Paz en Bello, Antioquia](https://archivo.contagioradio.com/asesinan-a-firmante-del-acuerdo-de-paz-en-bello-antioquia/)  
![Pescadores exigen medidas ambientales que restauren la Ciénaga de Bañó, en Córdoba](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/05/Loric-150x150.jpg "Pescadores exigen medidas ambientales que restauren la Ciénaga de Bañó, en Córdoba"){width="50" height="50" sizes="(max-width: 50px) 100vw, 50px" srcset="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/05/Loric-150x150.jpg 150w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/05/Loric-310x310.jpg 310w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/05/Loric-340x343.jpg 340w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/05/Loric-230x230.jpg 230w"} 21:47  
[Pescadores exigen medidas ambientales que restauren la Ciénaga de Bañó, en Córdoba](https://archivo.contagioradio.com/pescadores-exigen-medidas-ambientales-que-restauren-la-cienaga-de-bano-en-cordoba/)  
![La ingenuidad cristiana ¿Tiene culpa por las consecuencias de este modelo de sociedad?](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/carretera-150x150.jpg "La ingenuidad cristiana ¿Tiene culpa por las consecuencias de este modelo de sociedad?"){width="50" height="50" sizes="(max-width: 50px) 100vw, 50px" srcset="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/carretera-150x150.jpg 150w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/carretera-512x512.jpg 512w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/carretera-310x310.jpg 310w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/carretera-230x230.jpg 230w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/carretera-360x360.jpg 360w"} 15:48  
[La ingenuidad cristiana ¿Tiene culpa por las consecuencias de este modelo de sociedad?](https://archivo.contagioradio.com/la-ingenuidad-cristiana-tiene-culpa-por-las-consecuencias-de-este-modelo-de-sociedad/)  
![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/04/logo-contagio-radio-web-1.png){width="201" height="76"}

[Actualidad](https://archivo.contagioradio.com/actualidad/)

[Programas](https://archivo.contagioradio.com/categoria/programas/)

Paz

[  
Suscríbete  
](https://docs.google.com/forms/d/e/1FAIpQLSc6grPIjO3g1FdHlNNGTd-nrSRD4VImiVceTYRmbHYY3pTafw/viewform)
