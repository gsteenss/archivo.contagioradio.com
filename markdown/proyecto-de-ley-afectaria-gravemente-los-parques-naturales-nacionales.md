Title: Proyecto de ley afectaría gravemente los Parques Naturales Nacionales
Date: 2017-03-02 16:38
Category: Ambiente, Entrevistas
Tags: ecoturismo, Min Comercio, Parques Naturales de Colombia
Slug: proyecto-de-ley-afectaria-gravemente-los-parques-naturales-nacionales
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/03/ParqueNacional.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Ecomochilas] 

###### [2 Mar 2017] 

Los ecosistemas protegidos en los Parques Naturales Nacionales podrían estar en peligro. El Ministerio de Comercio presentó un proyecto de ley que pretende pedir facultades especiales al Gobierno Nacional para **poder encargarse de la administración de estos recintos de conservación y promover el ecoturismo en estas zonas de conservación** ambiental.

Frente a ello, distintas organizaciones ambientales manifestaron que se oponen a la idea y dicen que la conservación debe ser la prioridad. El objetivo de la iniciativa de MinComercio es “establecer la hoja de ruta para el desarrollo del ecoturismo dentro de las áreas protegidas”, pero **“estas zonas no son para promover la industria turística, sino para conservar ecosistemas estratégicos y de vital importancia”**, han señalado los ambientalistas.

Además, el proyecto que contempla la unión entre el Ministerio de Ambiente y Desarrollo Sostenible y el Ministerio de Comercio, Industria y Turismo, formula una **“política nacional para la promoción del ecoturismo, etnoturismo, agroturismo, acuaturismo y turismo metropolitano**”, y redefine cuáles son los PNN “donde se podría hacer turismo de naturaleza”. ([Le puede interesar: ANLA y Ministerio del Interior adelantaban consulta previa ilegal en parque Tayrona](https://archivo.contagioradio.com/anla-y-ministerio-del-interior-adelantaban-consulta-previa-ilegal-en-parque-tayrona/))

Aunque aún la iniciativa debe pasar por 8 debates en el Congreso, ambientalistas advierten que personalidades dentro del Congreso y el equipo del Gobierno Santos, estarían a favor del proyecto, por su parte, la ministra de Comercio, María Claudia Lacouture, aseguró que el turismo es el futuro de Colombia y que “tenemos que tomar un buen pedazo de esa torta”.

###  

Por otra parte el exministro de ambiente Manuel Rodríguez Becerra, recordó que el presidente Santos prometió no tocar los Parques Naturales y ahora MinComercio pretende **entrar a regular una actividad comercial que podría afectar gravemente la biodiversidad de ecosistemas** que se han destinado para la conservación y no para desarrollar actividades económicas.

Lo cierto es que el turismo que se desarrolla en Colombia, no ha sido responsable social y ambientalmente. Dos ejemplos de ello son la intención de **construir un hotel el el PNN Tayrona pasando por encima del territorio ancestral de comunidades indígenas** y la Quebrada Las Gachas, que se encuentra en muy mal estado debido al turismo irresponsable. ([Le puede interesar:Turismo irresponsable amenaza el caño cristales de Santander](https://archivo.contagioradio.com/36163/))

###### Reciba toda la información de Contagio Radio en [[su correo]
