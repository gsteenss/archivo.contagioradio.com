Title: La música en defensa del ambiente vuelve en “Viva el planeta”
Date: 2016-07-07 16:38
Category: Cultura, eventos
Tags: Doctor Krápula, Programación viva el planeta, Viva el planeta 2016
Slug: la-musica-en-defensa-del-ambiente-vuelve-en-viva-el-planeta
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/07/fvep-700x441.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Viva el planeta 

##### [8 Jul 2016] 

[Si no tiene planes en su agenda para este fin de semana y es un abanderado de los temas ambientales, el sábado 9 y 10 de julio  llega en su quinta versión el Festival ‘Viva el Planeta’, que se realizará en el escenario al aire libre La Media Torta.]

[![](http://www.garraproducciones.com/images/articulos/vivaelplaneta2016/bannerVivapLaneta.jpg) ]

[El evento, organizado por el Instituto de las Artes IDARTES y la banda Doctor Krapula, contará en su line up con varias agrupaciones latinoamericanas como:  Motor y Festway de México, Akasha de Costa Rica, la agrupación Papa Chango de Ecuador y por Colombia Mística, Lion Paw, Steff and Will y Revolver Plateado.]

[Nacido en 2012 como una iniciativa surgida del álbum “Viva el planeta” de Doctor Krapula, este espacio de música y cultura  se ha centrado en hacer un llamado a la concientización y la defensa del ambiente, de las personas quienes lo habitan y sobre todo a la preservación la vida.]

<iframe src="https://www.youtube.com/embed/gLti1fDourU" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

[Los asistentes podrán ingresar desde 12:00m hasta las 21:00 p..m., con entrada gratuita.]
