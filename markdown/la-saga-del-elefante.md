Title: La saga del elefante
Date: 2020-07-03 13:31
Author: AdminContagio
Category: Columnistas invitados, Opinion
Tags: Ejercito Nacional, Embera Chami, ernesto samper
Slug: la-saga-del-elefante
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/07/samper.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:heading {"level":5} -->

##### Por: Mayor César Maldonado, **Presidente Fundación Comité de Reconciliación**

<!-- /wp:heading -->

<!-- wp:paragraph -->

En Colombia se volvió costumbre que cualquier desocupado se le ocurra lanzar improperios para patear la honra y buen nombre de quien le venga en gana. Ya ni las instituciones se respetan. Bajo el amparo de la libre expresión, quienes padecen constantes diarreas verbales, suelen hacer sus deposiciones en público sin ningún tipo de pudor. Solo la expulsan y ya, sin importar a quien dañen. 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Paradójicamente, un *excomandante en jefe de las Fuerzas Armadas*, muy repanchingado en su casa, irresponsablemente, decidió herir la moral del ejército nacional por la execrable actuación de siete de sus soldados que violaron a una niña indígena.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":5} -->

##### Vale recordar que los militares fueron los primeros en condenar el infausto hecho, denunciando y poniendo en manos de las autoridades a los responsables, con la inmediatez requerida.

<!-- /wp:heading -->

<!-- wp:paragraph -->

Seguramente el confinamiento y la incertidumbre que causa la pandemia, atolondró al expresidente Ernesto Samper, - *sí, lo dije bien -* Ernesto Samper, el mismo que a empujones metió el enorme elefante a la Casa de Nariño, al punto, de aseverar que los soldados reciben entrenamiento institucional de “cómo violar niñas”. Muy hábil Ernesto, tuvo el cuidado de diluir la responsabilidad del agravio con su hermano Daniel, con quien dice, comparte semejante idiotez.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Es cierto que vivimos en tiempos en los que todo es susceptible de ser cuestionado, y más tratándose de un hecho fatídico y doloroso, condenado por toda la sociedad; pero si se decide añadir ruido, se debe hacer con responsabilidad y la sensatez debida. El debate de fondo sobre un tema crucial, lo desnaturalizó, quitándole altura y protagonismo a la discusión central.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Samper no es propiamente de las personas que se pueda sentir con la razón y el derecho de oficiar como juez, en absoluto. No creo que deambular por el mundo, negando el hecho vulgar del elefante sirva para conjurarlo, sino al revés, lo evidencia. Meses atrás lo vimos muy mediático aceptando una *mea culpa* bastante conveniente y editada, sin embargo, no dejan de molestarme esas personas que tienen la vanidad para hablar de sus horribles defectos.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Esperamos que con esto no se deforme la victoria diaria de los miles de soldados que entregan grandes sacrificios a la patria, con el único anhelo de recibir la gloria militar como recompensa a sus virtudes. 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Expresidente, no olvide la ley de la selva*: “cuando un gorila tiene más canas que buenas ideas, debe partir antes de que lo obliguen. No es algo digno, es vergonzoso”.*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Hasta luego,

<!-- /wp:paragraph -->

<!-- wp:heading {"level":6,"textColor":"vivid-cyan-blue"} -->

###### [Le puede interesar: Nuestros columnistas](https://archivo.contagioradio.com/opinion/columnistas/) {#le-puede-interesar-nuestros-columnistas .has-vivid-cyan-blue-color .has-text-color}

<!-- /wp:heading -->
