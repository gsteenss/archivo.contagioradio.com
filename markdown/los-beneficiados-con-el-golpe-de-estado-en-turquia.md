Title: Los beneficiados con el "golpe de Estado" en Turquía
Date: 2016-07-26 18:47
Category: El mundo, Otra Mirada
Tags: Golpe de Estado Turquía
Slug: los-beneficiados-con-el-golpe-de-estado-en-turquia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/07/turquia.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### Foto:periodicoelnacional 

##### 26 jul 2016 

Tras el fallido golpe de estado perpetrado en Turquia el pasado 15 de julio, crecen las versiones que apuntan a que **fue provocado desde el seno del gobierno del presidente Recep Tayyip Erdogan**, gracias en parte a las inconsistencias y ambigüedades que han salido a la luz desde que la sublevación habría sido controlada.

Para el analista internacional César Torres del Rio, **las consecuencias externas del denominado golpe, son favorables al gobierno de turno**, fortaleciéndose como agente de las potencias europeas y de Estados Unidos en oriente próximo, y en su aspiración **de ingresar a la EU**, después del primer paso dado en marzo de este año con el acuerdo firmado para convertirse en un **filtro en relación con los refugiados provenientes de Siria.**

A nivel interno y como consecuencia principal, el profesor Torres asegura que con Erdogan a la cabeza, fortalecido como presidente elegido por sectores del pueblo turco "**estamos asistiendo al verdadero golpe de estado**" considerando que es el mismo mandatario quien fue quien lo adelantó "**mediante el estado de excepción y el estado de sitio contra el pueblo turco y el pueblo kurdo**", lo que conlleva a un fuerte represión de ambos pueblos e incluso el debilitamiento de algunos sectores islámicos como de su férreo opositor Fethulá Gülen.

**Las consecuencias para el futuro de la democracia y las posibilidades de ejercer oposición son graves**, la represión ha conducido al cierre de **15 Universidades**, la detención de estudiantes y miembros de la prensa contradictora y algunos sectores de la población fuera del alcance del partido de gobierno, "**medidas con las que intenta socavar cualquier tipo de oposición**", conduciendo en lo interno a una serie de modificaciones de tipo institucional "para llevar a la república a un sistema de tipo presidencialista" y con el apoyo de las fuerzas armadas "intenta cimentar un modelo islámico conservador estatalista y neo liberal" afirma Torres.

En cuanto a la petición de Erdogan de reincorporar la pena de muerte en la legislación turca, el profesor asegura que la **EU** aunque públicamente deberá criticar la propuesta, "la vera con buenos ojos, por que ella obedece a lo que ha sido su táctica histórica" asegurando que el organismo "**la viene practicando** así en sus constituciones y legislaciones no aparece como tal, lo hace **al permitir la existencia de los refugiados, al traficar con ellos mediante el acuerdo con Turquía**, por el hecho que pasa por encima de los derechos humanos".

Como fin último instaurar un islam que deje atrás poco a poco esa República secularizada fundada en los años 20, imponer un Islam conservador, distinto al que impulsa Gülen desde los Estados Unidos, que interpreta mejor el sentir del islam, son las intenciones que podrían haber llevado a Erdogan a culminar el autogolpe de Estado y **la puerta para reafirmar desde la llamada "purga" su fuerza frente a la oposición**.

<iframe src="http://co.ivoox.com/es/player_ej_12353257_2_1.html?data=kpegl5iWeZihhpywj5eXaZS1lJqah5yncZOhhpywj5WRaZi3jpWah5yncaTZ1MbfjbnTttPZ1JDRx9GPloa3lIquptSJdqSfotPOzs7XuMKf0dTZh6iXaaK41c7Q0ZKJe6ShpNTb1sbLrdCfs8bRy9SPcYarpJKh&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>
