Title: Congreso de EEUU condiciona ayuda militar a Colombia
Date: 2020-07-22 09:58
Author: PracticasCR
Category: Actualidad, El mundo
Tags: Cámara de Representantes, Espionaje ilegal, Inteligencia Militar, Ley de defensa
Slug: congreso-de-eeuu-condiciona-ayuda-militar-a-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/07/Congreso.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: Pxfuel

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Este 21 de julio, la Cámara de Representantes en Washington aprobó, con 295 votos a favor, dos condiciones a la ayuda militar de ese país hacia Colombia. Las medidas fueron tomadas después de los escándalos reiterados por operaciones ilegales de las FFMM contra la oposición y los problemas que ha significado la persistencia de las aspersiones aéreas y las erradicaciones de los cultivos de uso ilícito.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Dichas iniciativas, de ser aprobadas por el senado, serán incluidas en la ley de Defensa para 2021 y las cuales requieren que el gobierno colombiano produzca un informe detallado sobre la ayuda militar que desde 2002 hasta 2018 ha sido entregada por el gobierno de EEUU.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

La ley contempla también el monto de las ayudas que EE.UU. entregará a países como a Colombia y este monto sería de unos 50 millones de dólares del presupuesto del Pentágono.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Además determina que “ningún equipo o suministro militar o de inteligencia transferidos o vendidos al gobierno de Colombia bajo los programas de asistencia de seguridad de los Estados Unidos, deben utilizarse con fines de espionaje ilegal o recopilación de inteligencia dirigida a civiles, incluyendo defensores de derechos humanos, personal judicial, periodistas u oposición política”. (Le puede interesar: [Espionaje de Ejército colombiano no puede seguir impune: Organizaciones de EE.UU.](https://archivo.contagioradio.com/espionaje-de-ejercito-colombiano-no-puede-seguir-impune-organizaciones-de-ee-uu/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Igualmente, la congresista Alexandria Ocasio-Cortez agregó una enmienda que prohíbe gastar fondos de Defensa para fumigaciones aéreas a menos que el gobierno demuestre que está realizando esta actividad, acogiéndose a las leyes y regulaciones colombianas.

<!-- /wp:paragraph -->

<!-- wp:heading {"customTextColor":"#0471a0"} -->

¿Qué debe incluir el informe? 
-----------------------------

<!-- /wp:heading -->

<!-- wp:paragraph -->

Se determina que 120 días después de la promulgación de esta ley, el Secretario de Estado junto con el Secretario de Defensa y el Director de Inteligencia Nacional deben presentar a los comités del Congreso un informe que evalúe las denuncias relacionadas con que la asistencia del sector seguridad proveído por el gobierno estadounidense, fueron usadas en Colombia para realizar espionaje y recopilar información utilizando inteligencia ilegal.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Este informe debe incluir un resumen con respecto a cualquier actividad de espionaje ilegal o recopilación de información de inteligencia hacia civiles o defensores de DD.HH., en la que hayan estado envueltos las unidades militares o la policía de Colombia desde el 2002 hasta el 2018. 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

También debe incluir una evaluación del alcance total de tales actividades, la identificación de las unidades involucradas, las cadenas de mando relevantes y la naturaleza y objetivos de dicho espionaje o recopilación de inteligencia.    

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Y por último, información sobre las medidas que tomará Colombia con el fin de que estas actividades no se repitan en un futuro, a menos que sean realizadas acogiéndose a las obligaciones internacionales.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Esta ley debe pasar por el Senado de Estados Unidos para ser finalmente aprobada e implementada.

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->
