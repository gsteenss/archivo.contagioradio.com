Title: Comunidad afro de la Guajira bloquea entrada a sede del Cerrejón
Date: 2019-01-16 21:07
Author: AdminContagio
Category: Ambiente, Comunidad
Tags: comunidades afro, El Cerrejón en La Guajira, La Guajira
Slug: comunidad-afro-de-la-guajira-bloquea-entrada-a-sede-del-cerrejon-2
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/11/el-cerrejon.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: laurismu 

###### 16 Ene 2019 

El pasado 15 de enero, la comunidad de Barrancas, La Guajira bloqueó uno de los complejos carboníferos del Cerrejón como mecanismo de protesta contra la compañía minera la cual ha violado el derecho a la consulta previa aprobada por el Consejo de Estado, una medida que buscaba acoger a 514 familias, de las cuales la empresa únicamente accedió incluir 33 vulnerando los derechos fundamentales de los habitantes de la zona.

Según Yoe Arregocés, líder del Consejo comunitario, el tejido social se ha deteriorado como consecuencia del  reasentamiento hecho por  Cerrejón en el 2003,** causando en la población de Roche desempleo, escasez de agua y problemas en los terrenos de las viviendas,** aunque se propuso una mesa de negociación para la mejorar la situación, el protocolo únicamente "se ha realizado entre la empresa y el Ministerio de Interior" deslegitimando a la comunidad.

Ante el incumplimiento en lo pactado, la comunidad bloqueó un cruce conocido como ‘El Túnel’ , colocando candados a los portones de acceso y salida del punto para bloquear la entrada y salida de vehículos del complejo carbonífero, **un lugar que por tradición ha sido considerado por los habitantes un territorio sagrado.**

Cerrejón intentó entablar un diálogo a través de un funcionario que se reunió con la comunidad para levantar el bloqueo, después de llegar a un acuerdo se comunicó lo acordado con la compañía la cual rechazó los puntos acordados, algo que la comunidad considero como una dilatación más y responsabilizó a Cerrejón de cualquier acción represiva que pudiera acontecer. [(Lea también Cerrejón ha contribuido a la vulneración del derecho al ambiente sano en La Guajira: Indepaz)](https://archivo.contagioradio.com/cerrejon-ha-contribuido-a-la-vulneracion-del-derecho-al-ambiente-sano-en-la-guajira-indepaz/)

Dentro de los puntos que se intentaron concertar con el representante de Cerrejón se busca crear una matriz de impacto para evaluar las repercusiones de la minería y el reasentamiento en la comunidad, conocer qué sucedió con los recursos de la consulta previa, además de buscar la participación de diferentes organizaciones garantes para llegar a un acuerdo entre la comunidad y la empresa.

El líder señala que** la comunidad ha perdido la confianza en los representantes que envía Cerrejón para reunirse con los habitantes, sin embargo están abiertos al diálogo** y permanecerán en el lugar hasta conocer un pronunciamiento de Cerrejón, la cual advirtió que hay fuerza policial muy cerca del sitio y está lista “para arremeter contra la comunidad” indicó Arregocés.

###### Reciba toda la información de Contagio Radio en [[su correo]
