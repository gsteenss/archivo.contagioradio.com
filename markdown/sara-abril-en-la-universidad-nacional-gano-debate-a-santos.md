Title: "El debate lo perdió Juan Manuel Santos" Sara  Abril
Date: 2016-01-29 18:11
Category: Educación, Nacional
Tags: educación pública, Juan Manuel Santos, Sara Abril, Universidad Nacional
Slug: sara-abril-en-la-universidad-nacional-gano-debate-a-santos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/01/universidad-nacional.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: El Universal 

<iframe src="http://www.ivoox.com/player_ek_10278343_2_1.html?data=kpWfmZ2XeJShhpywj5aXaZS1lZ6ah5yncZOhhpywj5WRaZi3jpWah5yncYamk6rZjcnJpsLoxpDZ0ZDUqdPYyoqwlYqmd4y%2B1sbbjbLFstbZzZDAw9PYs9SZk5eYtcbWpYy1w9fWzpKJe6ShpNTb1sbLrdCfs8bRy9SPcYarpJKh&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Sara Abril, Representante estudiantil] 

[29 Ene 2016]

“**Yo tengo el interés de que todo el país se entere de lo que está sucediendo con la universidad nacional, la universidad pública y en general con la educación en Colombia”,** afirma Sara Abril, representante estudiantil al Consejo Superior de la Universidad Nacional que negó un cupo en la Universidad de los Andes, porque prefirió entrar a “la mejor universidad de Colombia”, como ella denomina a la Universidad Nacional, y quien el jueves en el auditorio León de Greiff, encaró al presidente Juan Manuel Santos generando todo tipo de comentarios a favor y en contra.

Que la universidad Nacional tiene un déficit de \$2 billones, que a Colciencias le redujeron su presupuesto y que se vendió la mejor empresa del país (ISAGEN), fueron algunos de los argumentos con los que Sara Abril le debatió al presidente Santos acompañado de la Ministra de educación Gina Parody, y quienes según el criterio de Sara perdieron el debate, porque no supieron responder con cifras que demuestren lo contrario a lo presentado por la líder estudiantil, que asegura que los números referidos evidencian la situación de la educación pública en el país y que son extraídos de documentos estatales que cualquier colombiano tiene la posibilidad de conocer.

Tanto a Sara, como a otros representantes estudiantiles, les querían impedir su ingreso al auditorio, en el que los estudiantes nuevos esperaban ver la película “Magia Salvaje”, y no a encontrarse con el presidente de la República, Humberto de la Calle y Gina Parody, a quienes la representante estudiantil les evidenció, según ella, “otra parte de la realidad... **la inconformidad que tenemos con el marchitamiento de la universidad pública,** se tiene que saber que los recursos públicos están llegando a las universidades privadas”.

“¡Qué patética la dizque "lidereza estudiantil" de la [@UNColombia](https://twitter.com/UNColombia) Que solo sabe gritar cual verdulera ante el Presidente de la República!”, fue uno de los comentarios a través de twitter que recibió Sara de parte de Fabián Sanabria, exdecano de la Facultad de Ciencias Humanas, una de las personas que criticó y señaló a la estudiante, quien asegura que términos como “verdulera, campesina o  señora de la plaza de mercado”, no le significan una ofensa y en cambio, le sorprende que ese tipo de términos salgan de un antropólogo y doctor.

Por su parte, el rector de la Universidad Nacional, Ignacio Mantilla, aseguró en su twitter, que Sara estaba esperando “tomarse la foto para su jefe político”, a lo cual, Sara responde que es “**una representante estudiantil elegida, incluso, con más votos que el rector actual, si alguien tiene un jefe político es él,** porque quedó como rector por el Ministerio de Educación, el problema es que yo no permití que su jefe político estuviera cómodo”.

Para Sara, Santos perdió el debate, pues “quedó claro que nuestros sueños y nuestros proyectos de vida se contradicen con el modelo de país del presiente”, que es “contrario a la ciencia, el desarrollo”, asevera Abril, quien concluye que “perdieron el debate porque lo evadieron, no dan cifras, no controvierten”.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
