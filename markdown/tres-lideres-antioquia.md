Title: En menos de tres días asesinan a tres líderes sociales en Antioquia
Date: 2019-09-06 17:57
Author: CtgAdm
Category: Líderes sociales, Nacional
Tags: Antioquia, asesinato de líderes sociales, Magdalena Medio
Slug: tres-lideres-antioquia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/02/lideres-sociales.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

Entre el 4 y el 6 de septiembre fueron asesinados tres líderes sociales en Antioquia, se trata del defensor ambiental Fernando Jaramillo y de los integrantes de la Junta de Acción Comunal de La Milagrosa [**Wilder Elias Godoy Restrepo y León Humberto Alcaraz.**]{.css-901oao .css-16my406 .r-1qd0xha .r-ad9z0x .r-bcqeeo .r-qvutc0} Según la Fundación Sumapaz, han sido asesinados 13 líderes sociales en lo corrido del 2019 e este departamento.

Según información aportada por Óscar Yesid Zapata, coordinador del nodo Antioquia de la Coordinación Colombia-Europa - Estados Unidos (Coeuropa),  **Fernando Jaramillo de 63 años** contemplaba la idea de crear un corredor ecológico en Maceo, se encontraba en su finca en la vereda San Antonio cuando un sujeto armado disparo contra él en varias ocasiones.

### [Líderes que impulsaban la economía local y la protección ambiental]

[Por su parte, **Wilder Elias Godoy Restrepo y León Humberto Alcaraz** fueron asesinados con arma de fuego en una carretera de La Milagrosa en Santa Fe de Antioquia. juntos había sido expresidente y ex vicepresidente respectivamente de la Junta de Acción Comunal de su vereda, adicionalmente fomentaban la siembra del café en su región.]{.css-901oao .css-16my406 .r-1qd0xha .r-ad9z0x .r-bcqeeo .r-qvutc0}[(Lea también: Diofanor Montoya, líder que trabajaba por la tercera edad fue asesinado en Antioquia)](https://archivo.contagioradio.com/diofanor-montoya-lider-que-trabajaba-por-la-tercera-edad-fue-asesinado-en-antioquia/)

Zapata señala que ninguno de los campesinos había mencionado ser víctimas de amenazas, sin embargo expresó preocupación pues en el caso particular de Fernando, la comunidad de Maceo manifiesta que en numerosas ocasiones se había opuesto a proyectos mineros y licencias de explotación de recursos otorgados en su región.

### El Magdalena Medio Antioqueño bajo control paramilitar

Aunque se desconocen los responsables del asesinato, Óscar señala que en el Magdalena Medio Antioqueño existe presencia de grupos armados como las autodenominadas Autodefensas Gaitanistas de Colombia. el Clan Isaza y el ELN. [(Le puede interesar: Grupos paramilitares amenazan a reclamantes de tierras en Magdalena Medio)](https://archivo.contagioradio.com/grupos-paramilitares-amenazan-a-reclamantes-de-tierras-en-magdalena-medio/)

"Esta es una zona de control paramilitar, la gente no denuncia, incluso los mismos delincuentes están limitados" afirma el coordinador agregando que esta zona no solo hay una disputa por el microtráfico, también se busca incidir en los poderes locales y en las economías legales e ilegales.

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
