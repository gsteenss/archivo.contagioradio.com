Title: La paz y el respeto al derecho a disentir
Date: 2015-04-07 14:38
Author: CtgAdm
Category: Carolina, Opinion
Tags: Carolina Garzón Díaz, cumbre de arte y cultura, disentir, escritor, fernando Vallejo, izquierda colombiana, paz, Teatro Jorge Eliécer Gaitán
Slug: la-paz-y-el-respeto-al-derecho-a-disentir
Status: published

###### Foto: Mauricio Dueñas 

#### [**Por [Carolina Garzón Díaz](https://archivo.contagioradio.com/carolina-garzon-diaz/) (@E\_vinna)**] 

“*Disentir es un derecho que falta en Declaración de Derechos Humanos*” José Saramago  
La intervención del escritor colombiano **Fernando Vallejo en la Cumbre Mundial de arte y cultura por la Paz** ha suscitado toda clase de comentarios y reacciones en diversos sectores de eso que se llama “opinión pública”. Con una carpeta roja y más de siete páginas, Vallejo expresó con su estilo característico sus desacuerdos con el proceso de paz, el Presidente Santos, los ex Presidentes Gaviria, Pastrana y Uribe, la justicia transicional y criticó fuertemente el abandono del Estado que derivó en el surgimiento de la violencia. “La ruina de Colombia es inmensurable”, sostuvo Vallejo.

Sin estar de acuerdo con él en varias de sus aseveraciones, creo que sus palabras revelan verdades que nos incomodan y que, a quienes creemos en la construcción de la paz, nos cuestionan sobre temas que hemos querido ignorar o nos cuesta mirar. No es secreto que los reparos que tiene Vallejo en contra del proceso de paz son los mismos que sienten miles o quizás millones de colombianos, por eso vale la pena escucharlo, porque el disenso y la libertad de expresión también son derechos. Así mismo, Vallejo se expresó y escuchó atento a sus compañeros de panel, con quienes podría discrepar, pero jamás irrespetó.

Pero lo que más llamó la atención de este episodio es la actitud del público del Teatro Jorge Eliécer Gaitán que escuchaba a Vallejo y parecía tensarse cada vez más con sus palabras. Una mujer que defensora de la paz me comentó “¿Para qué invitar a Vallejo? ¿De qué nos sirve escuchar en este país más odio e ira?”, creo que es para eso precisamente, para escucharnos y no llenarnos de una censura que finalmente se desborda en la violencia. En el Teatro algunas personas desde el público interrumpieron a Vallejo para increparle sus palabras e incluso exigirle soluciones. Una contradicción: ese mismo público que se le llenaba la boca hablando de la construcción de paz y el respeto de los derechos humanos, irrespetaba el derecho elemental del escritor a pensar diferente y a expresar sus opiniones.

Acaso ¿cuánta sangre de personas que piensan diferente se ha derramado en nuestro país? Millones de litros. La izquierda, más que cualquier otro sector, sabe qué es morir por pensar de forma diferente y por expresar abiertamente sus sentires. Lastimosamente tantos años de guerra nos han hecho radicales y temerosos de aquellos que se expresan, piensan o sienten diferente. Ahí está el meollo de la paz: en el diálogo y la compresión del otro como una persona diferente a mí sin que ello lo convierta en mi enemigo.

Además, aquí entre nos, es sano que quienes creemos en la construcción de la paz en Colombia ampliemos nuestra visión para que el debate no sea únicamente una tertulia entre amigos que coincidimos, sino un verdadero diálogo con otras voces. Escucharnos entre todos sirve para reconstruir y ciertamente las disidencias también son aportes a la paz y a la democracia. El respeto a la alteridad es la base de una sociedad con justicia y equidad.

Insisto en mi desacuerdo con el escritor Fernando Vallejo en varios temas (no todos) pero veo con esperanza que lo ocurrido en este panel de la Cumbre es una ventana al posconflicto y lo más cercano a la paz: Cinco personas, con experiencias y lecturas diferente sobre la vida y el conflicto armado en Colombia, que se pueden expresar con libertad, dialogar, controvertir, contra preguntar y disentir desde la palabra y sin armas. Eso es paz y es democracia, una paz y una democracia en la que caben todos, incluyendo en derecho a disentir.
