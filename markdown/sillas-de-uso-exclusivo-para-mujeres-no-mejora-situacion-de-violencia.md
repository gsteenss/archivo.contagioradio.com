Title: "No se puede golpear a una mujer y seguir siendo parlamentario, concejal o empresario"
Date: 2017-11-14 16:28
Category: Mujer, Nacional
Tags: mujeres, Transmilenio
Slug: sillas-de-uso-exclusivo-para-mujeres-no-mejora-situacion-de-violencia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/11/transmi.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: El Tiempo] 

###### [14 Nov 2017] 

Varias organizaciones que defienden los derechos de las mujeres se han pronunciado y han calificado la propuesta de las sillas exclusivas para mujeres en Transmilenio, como excluyente e ineficaz. Para la Casa de la Mujer, esta medida además de ser insuficiente, **no resuelve el problema del acoso de fondo, dado que no ataca la raíz  que esta en la cultura en donde golpear o acosar a una mujer se ha naturalizado**.

El proyecto propuesto por el concejal Marco Fidel Ramírez, y que ya fue aprobado en primer debate, propone que todas las sillas rojas de los buses de Transmilenio, sean de uso exclusivo para las mujeres, y se contribuya a disminuir el acoso sexual en el servicio de transporte público. Sin embargo para Sanchez el **acoso sexual y las agresiones hacia las mujeres necesitan de políticas públicas integrales, que no se reducen solo al transporte público.**

“No se está cogiendo el toro por los cuernos, sino casi por la cola, mientras que no exista una política consistente integral de prevención de las violencias contra las mujeres y de trasformación de imaginarios en relación con que el cuerpo de la mujer no es un objeto que se puede tocar, coger, poseer, **en el momento en que los hombres lo consideren, estas medidas solo servirán por una o dos semanas**” afirmó Olga Amparo Sanchéz. (Le puede interesar:["Informe revela que hay 15076 víctimas de violencia sexual en Colombia"](https://archivo.contagioradio.com/victimas-de-violencia-sexual-colombia/))

La directora de la Casa de la Mujer manifiesta que incluso es una medida que no podría controlar el acoso en las filas que se realizan en las estaciones o en el ingreso a los buses, y que por el contrario es una política residual, “hay que empezar a desnaturalizar la violencia contra las mujeres, hay que empezar a sancionarla social y políticamente, en este país no se puede golpear a una mujer y seguir siendo parlamentario, concejal o un gran empresario”.

De acuerdo con el más reciente informe de la Secretaría de la mujer el **64% de las mujeres han afirmado ser víctimas de algún tipo de acoso sexual en el transporte público**, mientras que el 80% dicen que la mayoría de estas agresiones son cometidas en articulados de Transmilenio y buses del SITP. (Le puede interesar: ["Denuncian nuevo caso de acoso sexual en plataforma Uber"](https://archivo.contagioradio.com/denuncia_acoso_sexual_uber/))

<iframe id="audio_22070859" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_22070859_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
