Title: #TeSeguimosBuscando Así se conmemora el día del detenido desaparecido
Date: 2018-08-28 14:46
Category: Nacional
Tags: desaparecidos, día del desaparecido
Slug: asi-se-conmemora-el-dia-del-detenido-desaparecido
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/08/desaparecidos-colombia.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Contagio Radio 

###### 28 Ago 2018 

Cada 30 de agosto se conmemora el Día internacional del Detenido Desaparecido en Colombia, en el marco de esta fecha **se realizarán del 27 de agosto al 1 de septiembre diversas actividades como homenaje a las víctimas y sus familias**, en Barranquilla, Cali, Bogotá, Medellín y Manizales entre otras ciudades.

**5 organismos del Estado, 9 organizaciones, y 5 medios de comunicación se unirán por las personas desaparecidas**, entre las que se cuentan la Fundación Nydia Erika Bautista, el Movimiento de Víctimas de crímenes de Estado (MOVICE), la Unidad para la Atención y la reparación integral de las víctimas, la Cruz Roja Colombiana subdelegación de Cali, entre otras, para exigir justicia, verdad, visibilizar los hechos y las víctimas, y hacer memoria, **utilizando la etiqueta \#TeSeguimosBuscando**.

**Barranquilla**

**Entre el 27 y 28 de agosto** de 8 am a 5 pm, se realizará la exhibición de historias de casos de desaparecidos en la Plaza de la paz y la Plaza Alfredo Correa de Andréis. **El 29 de agosto** se lleva a cabo la charla titulada **“La búsqueda de desaparecidos un asunto de política de Estado”**, desde las 7 am, en Vokaribe radio y por último se realizará el **Encuentro solidario por la verdad y la justicia de los desaparecidos en Colombia**, el 30 de agosto a partir de las 9 am, en las Instalaciones de la casa de la Memoria.

**Bogotá**

El 30 de agosto a las 3pm se relizará la  **“Galería, homenaje, acto memoria y acto cultural”** en la Plazoleta Che Guevara  de la Universidad Nacional, para esta actividad el MOVICE invita a la ciudadanía  **a** **participar con una fotografía de la víctima e información en la parte inferior**, utilizando el formato disponible en el siguiente [link ](https://drive.google.com/file/d/1hDNNjmpvgqXV7ZG2jCzJn6tvA2wwAxff/view). Además se realizará  una mesa de radio en vivo acompañada por familiares de Ayotzinapa, Saltillo, Juárez, Buenos Aires y Fedefam.

El viernes 31 de agosto de 1:30 pm a 4:30 pm se realizará el conversatorio **“La desaparición forzada, un crimen que debe doler a toda la sociedad”**, en el auditorio Archivo Histórico Distrital en el tercer piso (Calle 6B \# 5 – 75), actividad acompañada de las presentaciones artísticas de Manuel Garzón, Amarilla Londoño, Beatriz Jaimes y grupo de Flamencos y El Fury Ensamble Acústico, celebrando además el aniversario de la Fundación Hasta Encontrarnos.

**Manizales**

En Manizales, se realizarán dos actividades el 30 de agosto, la primera será el **Conversatorio Día internacional** a las 9 am, en el Auditorio Defensoría del Pueblo; y el segundo evento, es la Galería de la memoria **“Rompamos el cerco del olvido”** a las 3 pm, en el Palacio de Justicia Fanny Gonzalez Franco.

**Cali**

La capital de Valle del Cauca tendrá seis días de actividades para conmemorar esta fecha; el evento denominado **“Semana de las mariposas invisibles”,** hace parte de la campaña “Aquí Falta Alguien” del Comité internacional de la Cruz Roja (CICR), la programación completa se encuentra [aquí](https://www.icrc.org/es/download/file/52172/programacion_cali_desaparecidos.pdf.).

Adicionalmente, El Movimiento Nacional de Víctimas de Crímenes de Estado, Capítulo Valle del Cauca presentará la **obra de teatro por “Por algo sería”**; de entrada, libre, en el “Teatro Jorge Isaacs” (Cra. 3 \# 12 – 28), el 30 de agosto a las 6:30 pm (Le puede interesar: [La unidad de búsqueda, una oportunidad para los familiares de desaparecidos](https://archivo.contagioradio.com/la-unidad-de-busqueda-una-oportunidad-para-los-familiares-de-desaparecidos/))

**Medellín**

Medellín realiza actividades desde el 29 de agosto, iniciará un foro titulado **“Participación de las víctimas en el SIVJRNR”**, desde las 9 am hasta las 5 pm en el Museo Casa de la Memoria; y el 30 de agosto se realizará una rueda de prensa a las 9 am en la entrada a la Arenera **'El Cóndor'** **en el Sector la Escombrera de la Comuna 13** y a las 3 pm se hará un Plantón por las víctimas de Desaparición Forzada, en el Parque de las Luces.

**Villavicencio**

El 30 de agosto se realizará la **"Jornada de Memoria para la paz , la reconciliación y la afirmación de la vida"** que inicia en el Cementerio Central de Villavicencio con la presentación del informe "**Los cuerpos no identificados en los cementerios de los Llanos Orientales"** de 8 a 10 a.m.; acto seguido se llevará acabo el conversatorio "Expectativas y exigibilidad de las víctimas frente a la impementación del acuerdo de paz" en el laboratorio de memoria y paz.

En la tarde se realizará el acto de vida y memoria, **Galería de relatos, memoria y dignidad por las víctimas de desapariciones** desde las 12m hasta las 4:00 p.m. en el Parque de las flores. El cierre será en el Laboratorio de memoria y paz, con la instalación de la escuela de DDHH y paz del Meta de 4 a 6 de la tarde.
