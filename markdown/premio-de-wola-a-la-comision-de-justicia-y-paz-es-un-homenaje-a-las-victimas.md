Title: Premio de WOLA a la Comisión de Justicia y Paz "es un homenaje a las víctimas"
Date: 2015-10-28 17:46
Category: DDHH, Entrevistas
Tags: Comisión Intereclesial de Justicia y Paz, COnflcito aamdo en Colombia, defensores de derechos humanos, Derechos Humanos, Justicia y Paz, víctimas
Slug: premio-de-wola-a-la-comision-de-justicia-y-paz-es-un-homenaje-a-las-victimas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/10/petro_comision_justicia_y_paz_contagioradio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: contagioradio.com 

<iframe src="http://www.ivoox.com/player_ek_9197731_2_1.html?data=mpammZyXdY6ZmKiak5qJd6KpmJKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRlNPZzs7cjcnJb7jDraaYw5DQpYy30NLW1c6Jh5SZo5jbjcnJb6vp1NnWxc7Fb9qfscbnjYqWdsbnjNrbjZKJe6ShpNTb1sbLrdCfs8bRy9SPcYarpJKh&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Ximena Sánchez, WOLA] 

###### [28 oct 2015] 

La Comisión Intereclesial de **Justicia y Paz fue galardonada con el premio WOLA** (Washington para Asuntos Latinoamericanos), que se ha entregado durante los últimos ocho años a organizaciones de derechos humanos. Justicia y paz lo gana como reconocimiento a **su labor con las víctimas del conflicto armado en Colombia** que a trabajado de la mano con las personas y de formas que aportan a la construcción de país.

Ximena Sánchez, integrante de la organización WOLA, afirma que la Comisión Intereclesial merece el premio ya que **ha ayudado** a que “las voces de las víctimas, sus propuestas y también sus recomendaciones y acciones en diferentes partes del país”, pero también en el mundo, con lo que se ha logrado la protección de muchos de ellos y que puedan **continuar con su labor.**

Sánchez resalta que la labor de la Comisión de Justicia y Paz ha sido representativa porque ha instalado una nueva forma de hacer acompañamiento. A diferencia de otras experiencias de América Latina la Comisión hace lo que las comunidades deciden y ese es otro modelo distinto, fruto de ello es, entre otros, la red CONPAZ, que fue creada gracias al acompañamiento de Justicia y Paz.

"S**e logra un trabajo íntimo** y que ayuda a una “reflexión de lo que esas comunidades quieren”, además con una “integración de los derechos básicos”, esto siempre con un trabajo fundamental de las comunidades quienes guían estos procesos.

Este premio también lo recibirá el estadounidense **Ken Richard** quien ha tenido un **liderazgo importante a nivel de derechos humanos** y quien ha logrado “asegurar condicionamientos” para la violación de derechos humanos por parte de las fuerzas militares en Colombia, además que ha tenido incidencia en el proceso con Cuba.
