Title: Defensor de DDHH David Ravelo en libertad tras siete años de prisión
Date: 2017-06-16 16:41
Category: DDHH, Nacional
Tags: cárceles colombia, David Rabelo, defensores de derechos humanos
Slug: david-rabelo-es-dejado-en-libertad-luego-de-siete-anos-de-prision
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/06/fk5a0084.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<div class="group-about-the-case field-group-div">

<div class="field field-name-field-about-the-situation-text field-type-text-long field-label-hidden">

<div class="field-items">

<div class="field-item even">

###### [Foto: PBI] 

###### [16 Jun 2017]

Luego de siete años en prisión es dejado en libertad condicional el **defensor de derechos humanos David Ravelo,** secretario del Consejo de Directores de la Corporación Regional para la Defensa de los Derechos Humanos (CREDHOS). La libertad condicional se podría hacer efctiva la noche de este viernes o el próximo martes.

Según la información recibida, la decisión fue tomada por el Juzgado tercero de ejecución de penas en la tarde de este viernes, el defensor había sido condenado en 2013 a 220 meses de prisión  por el crimen del excandidato a la Alcaldía de Barrancabermeja, David Núñez Cala, en abril de 1991.

El abogado Reynaldo Villalba, del Colectivo de Abogados José Alvear Restrepo interpuso un recurso en el que bajo la ley 1820 contemplada en el acuerdo de paz con las FARC-EP concede la posibilidad de libertad condicional para personas ligadas con delitos relacionados en con el conflicto armado.

Aunque Ravelo y su defensa no están reconociendo que el defensor de Derechos Humanos tenga o haya tenido alguna relación con las FARC, la ley contempla que las personas sentenciadas o acusadas de crímenes relacionados puedan acudir a la Jurisdicción Especial de Paz, puesto que el juzgado acusa a Rabelo de ser responsable político y determinador del crímen de Nuñez.

### **Los falsos testimonios del "Panadero" y el contexto del proceso** 

Luego de haber sido detenido en 2010, el defensor de Derechos Humanos junto a sus abogados habían logrado demostrar que el testimonio de alias "el panadero" carecía de verdad judicial, dado que este paramilitar estaría buscando favorecimientos por parte de la fiscalía y además ya habría material probatorio suficiente para demostrar que "el panadero" mintió.

El 26 de mayo de 2015, el Juzgado Noveno Penal del Circuito de Bucaramanga realizó una audiencia preparatoria para el juicio contra alias “El Panadero” por los cargos de **falso testimonio en el caso Ravelo y “mentirle al país” en el caso de la periodista Jineth Bedoya.**

El **Colectivo de Abogados José Alvear Restrepo** denunció que  el paramilitar luego del ser expulsado de la ley 975 o de Justicia y Paz  seguía gozando de privilegios en el patio 6 de la cárcel Modelo.

###### Reciba toda la información de Contagio Radio en [[su correo]

</div>

</div>

</div>

</div>
