Title: Colombia tiene un sistema electoral que permite elegir criminales: MOE
Date: 2017-02-07 12:41
Category: Entrevistas, Política
Tags: elecciones, MOE, partidos politicos, politica
Slug: sistema-electoral-elegir-criminales-35907
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/moe-elecciones-guajira.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: MOE] 

###### [7 Ene. 2017] 

El tema de los partidos políticos y los hechos de corrupción continúan, por un parte Cambio Radical ha sido cuestionado por haber dado avales a **Kiko Gómez, Oneida Pinto y el alcalde de Riohacha Fabio Velásquez, todos ellos vinculados a investigaciones o juicios criminales**, incluso advertidos antes de  ser elegidos, y por otro lado está el Centro Democrático del que se están descubriendo vínculos con Odebrecht, una de las empresas más corruptas de las que se tiene noticia. Le puede interesar: [La corrupción, un obstáculo para la Paz en Colombia](https://archivo.contagioradio.com/la-corrupcion-un-obstaculo-para-la-paz-en-colombia/)

Para **Camilo Vargas, integrante de la Misión de Observación Electoral – MOE** – estos hechos son la manifestación de dos problemas, el primero es la gran **debilidad de los partidos,** la falta de estructuras organizativas y de criterio para otorgar avales. “Al momento de dar avales no hay un criterio objetivo que filtre tanto la llegada de candidatos inconvenientes como de dinero inconvenientes” aseguró.

Como segundo problema, Vargas manifestó que cada vez que hay elecciones se mueven unas grandes sumas de dinero y al ser **partidos políticos débiles, con estructuras débiles “son fáciles corromper”. **Le puede interesar: [Democracia colombiana: La máquina de hacer votos](https://archivo.contagioradio.com/democracia-colombiana-la-maquina-de-hacer-votos/)

Adicionalmente el sistema electoral con el que cuenta Colombia también es débil, por lo que la MOE ha sugerido que este debe ser “robustecido” para revisar la influencia del sector empresarial y privado, “**El sector privado tiene un rol en las elecciones (…) financia la enorme mayoría de las campañas y luego no responde** o pasa de agache frente a su responsabilidad” dijo Vargas. Le puede interesar: [Odebrecht la punta del Iceberg de la corrupción en Colombia](https://archivo.contagioradio.com/odebrecht-la-punta-del-iceberg-de-la-corrupcion-el-colombia/)

Por ejemplo, en el 2014 Óscar Iván Zuluaga fue el único candidato que no pidió financiación estatal, solamente accedió a financiación privada para su campaña, lo cual es atípico y por  ello llama la atención “esa situación hace que sea muy difícil trazar el origen, el destino y el monto que pasaron por su campaña” lo que podría traducirse en que **el sistema electoral colombiano tiene una “incapacidad absoluta para controlar quién pone a los políticos y la responsabilidad de los dineros”.**

La MOE, en voz de Vargas continúa insistiendo además que existe una gran responsabilidad en los ciudadanos “**hay que decirle a la gente, participe. Que se apropien y le exijan a sus partidos. Pero también del Estado y del voto”. **Le puede interesar: [Los escándalos de corrupción que salpican al Centro Democrático](https://archivo.contagioradio.com/odebrecht-pago-por-servicios-de-publicista-para-la-campana-de-oscar-ivan-zuluaga/)

Si bien hay avances en materia legislativa como la ley 1475 de 2011 para adoptar reglas de organización y funcionamiento de los partidos y movimientos políticos y de los procesos electorales “queda mucho por hacer por los problemas que implica hacer reformas electorales, y es que pasan por el Congreso y cuando a éste no le conviene algún tipo de reforma es muy difícil hacerla pasar” manifestó Vargas.

Finalmente, Vargas manifestó que **hay casos en los que la responsabilidad no recae sobre algún partido político sino que lo que queda demostrado es que quien quiera hacer política lo que necesita es dinero** “Jhon Jairo Torres el exalcalde de Yopal, 3 veces capturado, no llegó con el aval de ningún partido político, lo que muestra que una persona con plata es capaz de recoger las firmas entre la ciudadanía y lanzarse  hacer política” independientemente de su accionar criminal.

<iframe id="audio_16874996" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_16874996_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)

<div class="ssba ssba-wrap">

</div>
