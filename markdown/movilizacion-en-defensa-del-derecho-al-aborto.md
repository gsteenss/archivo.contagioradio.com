Title: Colectivos feministas marcharán en defensa del derecho al aborto
Date: 2019-05-03 16:06
Author: CtgAdm
Category: Mujer, Nacional
Tags: aborto seguro, despenalización
Slug: movilizacion-en-defensa-del-derecho-al-aborto
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/Captura-de-pantalla-90.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Anadolu] 

En defensa del derecho al aborto organizaciones feministas hicieron un llamado a la ciudadanía para que se movilice en las calles en paralelo a la octava **Marcha Nacional por la Vida**, que se realizará el 4 de mayo en varias ciudades del país, donde sectores radicales de derecha y religiosos buscan **reversar los fallos de la Corte Constitucional sobre la despenalización del aborto**.

Valeria Bernal Castillo, cofundadora del colectivo Hoguera Feminista, de la Universidad del Rosario, afirma que estos sectores "pro-vida" quieren que las mujeres no ejerzan autonomía sobre sus cuerpos, y les sean negados sus derechos al aborto bajo las tres causales establecidas por la Corte Constitucional en 2006 (cuando la salud de la mujer esté en riesgo, cuando exista malformaciones del feto y cuando el embarazo se dé por violación o inseminación artificial no consentida).

Actualmente, las mujeres enfrentan barreras para acceder al derecho al aborto a pesar de la protección que  brindó el alto tribunal. Entre ellas, se encuentran la negación de algunas EPS a realizar los procedimientos, la falta de conocimiento sobre la despenalización  y la ausencia de políticas de  educación sexual, hechos que obligan a **las mujeres embarazadas a acudir a abortos clandestinos sin ningún tipo de cuidado médico, arriesgando sus vidas**. (Le puede interesar: "[ABC de la despenalización del aborto en Colombia](https://archivo.contagioradio.com/despenalizacion-aborto-en-colombia/)")

El encuentro será en frente del **Centro Memoria, Paz y Reconciliación** a las 9:30 de la mañana, el próximo 4 de mayo en donde iniciará el recorrido de la marcha que seguirá por la Calle 26 hacia la Avenida Caracas y terminará con un plantón en la Calle 39, a unas cuadras de Profamilia. La movilización será acompañada por los colectivos feministas La Morada, Las Parceras, Aborto Legal, Las Viejas Verdes y Siete Polas.

<iframe id="audio_35334767" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_35334767_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
