Title: Sin Olvido - Gilma Yaneth Pineda Metaute
Date: 2015-01-06 15:10
Author: CtgAdm
Category: Sin Olvido
Slug: gilma-yaneth-pineda-metaute
Status: published

###### Foto: Cortesía de la familia 

[Audio](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/01/fsq9ERtV.mp3)

Hoy, Gilma Yaneth Pineda Metaute tendría 37 años y habría podido acompañar a su hija en estos 18 años, a dar sus primeros pasos, llevarla a la escuela, acompañar su adolescencia,  asistir a sus grados, celebrar sus 15 años...

Hoy se cumplen 18 años de su asesinato por miembros del Estado colombiano en complicidad con grupos paramilitares en el Departamento de Antioquia.

Yaneth fue la tercera de cuatro hermanas (o), nació en una vereda del municipio de San Roque en Antioquia, al igual que sus hermanas (o) creció entre cafetales, con el arrullo de los pájaros y el hermoso paisaje adornado de exuberantes montañas. Para ese entonces su família tenía una parcela en la que cultivaban productos de pan coger, café y árboles frutales, eso les alcanzaba para sobrevivir sin tantos apuros.
