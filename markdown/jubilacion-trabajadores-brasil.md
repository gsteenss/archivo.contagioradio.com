Title: Reforma de Temer perjudica jubilación de trabajadores más pobres
Date: 2016-12-14 15:06
Category: El mundo, Otra Mirada
Tags: Brasil, jubilación, Seguridad Social, Temer
Slug: jubilacion-trabajadores-brasil
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/12/brasil_sintierras.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### Foto: Prensa latina 

##### 14 Dic 2016 

Continuan en Brasil las manifestaciones en contra de la r**eforma al Sistema de Seguridad Social**, presentadas el pasado 6 de diciembre por el presidente no electo Michel Temer, **medidas que afectan directamente a servidores públicos, trabajadores de la empresa privada, trabajadores rurales y pensionados**, en lo que se refiere a las condiciones para acceder a su jubilación.

La propuesta de Temer contempla que **la edad mínima de jubilación para los trabajadores brasileños sea de 65 años**, tanto para hombres como para las mujeres, y sólo será concedida **si el trabajador comprueba por lo menos 25 años de contribución** al Instituto Nacional de Seguro Social (INSS), para obtener el derecho de tan sólo el 76% del total del beneficio y de 49 años para recibir el valor integral (100%).

Con las modificaciones, se **terminariá la diferencia de 5 años entre hombres y mujeres**, mientras que la desigualdad salarial entre géneros se mantiene siendo la remuneración por el trabajo de ellas, en muchos casos alternado con labores del hogar, 30% menor a la de los hombres.

Las reformas aplicarían para **mujeres que actualmente tienen menos de 45 años y hombres de menos de 50**, quienes tienen edad igual o superior pasarán por una etapa de transición para alcanzar el beneficio.

### Jubilación de trabajadores rurales 

Uno de los sectores más afectados con la posible aprobación de la medida en el Congreso Nacional Brasileño son los campesinos, por la **desaparición de la figura jurídica de asegurado especial**, consignada en la Constitución de 1988, que garantiza la jubilación de un salario mínimo para hombres a los 60 años y para las mujeres a los 55, cuando se comprueba la realización de trabajos agrícolas por al menos 15 años.

Con la reforma de Temer, las y **los campesinos deben ingresar obligatoriamente al sistema del INSS para poder acceder a su jubilación** que sería a los 65 años y con una contribución al sistema por no menos de 25 años, y en el caso de las mujeres trabajadoras rurales, las pensiones por viudez se reducirán al 50% del salario mínimo. Le puede interesar: [Movimientos populares piden impeachment contra Temer](https://archivo.contagioradio.com/movimientos-populares-impeachment-temer/).

De ser aprobada la reforma, **los trabajadores rurales que no estén al día con la contribución mensual no podrían acceder al auxilio por enfermedad, jubilación por invalidez o licencia de maternidad**; mientras que los beneficiarios de la prestación continuada, aplicada a personas con más de 65 años y personas con limitaciones que reciben un salario mínimo, la edad se elevará a 70 años y el beneficio pasará a ser desvinculado del salario mínimo, disminuyendo el auxilio recibido.

El malestar de las organizaciones sindicales y campesinas ante tal disposición, es mayor si se tiene en cuenta que **los militares, quienes tienen tambien un sistema de seguridad social diferenciado, no tendrían por el momento un cambio en los beneficios que este les otorga**, perjudicando unicamente a los trabajadores más pobres, quienes regularmente tienen jornadas más pesadas y casi siempre en condiciones de mayor vulnerabilidad.

### Lo que sigue 

**La propuesta aún tiene que pasar por la Comisión de Constitución y Justicia, por la Cámara de Diputados y de Senadores** y deberá enfrentar fuertes resistencias en la sociedad, especialmente por parte de centrales sindicales, que ya declararon que van a luchar contra la medida.

##### Con información de Brasil de Fato 
