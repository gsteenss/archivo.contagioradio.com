Title: Arrogancia del Gobierno no le permite ver oportunidades políticas con el ELN: De Currea-Lugo
Date: 2018-11-21 15:36
Author: AdminContagio
Category: Paz, Política
Tags: Alias Gabino, diálogo con el ELN
Slug: arrogancia-del-gobierno-no-le-permite-ver-oportunidades-politicas-con-el-eln-de-currealugo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/11/eln-770x400.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: @equipoPazGob 

###### 21 Nov 2018 

[El jefe negociador del ELN, **Pablo Beltrán**, anunció que su líder máximo, Nicolás Rodríguez Bautista, "Gabino", fue designado como delegado de paz, una decisión que para el analista y docente]** Víctor de Currea-Lugo**[  ratifica "una voluntad de paz y una  dinámica de audacia política" para avanzar en el dialogo.]

Para el profesor, el primero de estos gestos de voluntad  fue la liberación unilateral de los soldados detenidos en Arauca,  seguida de la liberación de seis personas detenidas en el Chocó durante el mes de septiembre, lo que podría traducirse **"no en una tregua pero si un desescalamiento para facilitar las negociaciones". [(Le puede interesar: Gobierno tendrá que decidir si es una prioridad o no negociar con ELN)](https://archivo.contagioradio.com/gobierno-tendra-que-decidir-si-es-una-prioridad-o-no-negociar-con-eln/)**

Estos actos, sumados al nombramiento de su máximo comandante como jefe negociador en La Habana, confirman una decisión autónoma del grupo armado para continuar en la mesa de negociaciones, interés que no se ve reflejado en la comitiva del gobierno nacional.

> El presidente de la república de Colombia [@IvanDuque](https://twitter.com/IvanDuque?ref_src=twsrc%5Etfw) no nombra los miembros representantes del [@ELN\_Paz](https://twitter.com/ELN_Paz?ref_src=twsrc%5Etfw). Es una decisión autónoma y soberana del ELN. [pic.twitter.com/cNTdVHvcsO](https://t.co/cNTdVHvcsO)
>
> — ELN Colombia (@ELN\_RANPAL\_COL) [21 de noviembre de 2018](https://twitter.com/ELN_RANPAL_COL/status/1065120384004079616?ref_src=twsrc%5Etfw)

<p>
<script src="https://platform.twitter.com/widgets.js" async charset="utf-8"></script>
</p>
 

De Currea-Lugo menciona que a pesar que el Gobierno se comprometió a establecer unas canales permanentes con el ELN una vez se hicieran las respectivas liberaciones en **Cauca y Chocó**, se cerraron las puertas al diálogo lo que demuestra que "la arrogancia gubernamental es tan grande que no pueden leer las oportunidades políticas".

El analista aclaró, que según medios, "Gabino" informó al gobierno Santos sobre su salida del país y fue autorizado, por lo que no existiría ninguna ilegalidad en su desplazamiento e incluso cuando se hizo el empalme con el gobierno Duque se le informó que **Nicolás Rodríguez Bautista** estaba en Cuba, esto en contraste con las declaraciones del Alto Comisionado para la Paz, Miguel Ceballos exigiendo su captura.

De igual el analista forma condenó la presión mediática que se ha generado para "tensionar aún más  las relaciones con Venezuela y tratar de desgastar al máximo la mesa con el ELN” a pesar **del interés del 61% de la población colombiana de ver al Gobierno y ELN reanudar los diálogos** según la más reciente encuesta Invamer.

<iframe id="audio_30246356" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_30246356_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
