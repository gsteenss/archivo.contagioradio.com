Title: De las masacres aplazadas
Date: 2020-08-22 20:14
Author: CtgAdm
Category: Lina Sua, Opinion
Slug: las-masacres-aplazadas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/08/El-regreso-de-las-masacres.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right"} -->

Por: Lina Sua

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

*¿De qué se podría escribir durante esta semana en Colombia si no es de la muerte de sus jóvenes?*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Desde la semana pasada me ha sido imposible dormir. Era miércoles y habían asesinado a Cristian y Maicol, mientras dejaban su tarea en casa de una profesora en Leiva; a Juan, Jean, Álvaro, Jair y Leyder mientras volaban cometa en un barrio de Cali; a Patrocinio Bonilla, un líder social en Chocó; a Michel, la hija de firmantes de la paz en Antioquia; habían atentado contra la vida de Francisco, un líder indígena del pueblo Awá, en Tumaco; y habían mutilado a Luis en Sincelejo, en Altos del Rosario, por su orientación sexual.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El domingo el sol aparecía y con él, la noticia de otros nueve jóvenes asesinados en Samaniego, Óscar Andrés, Laura Michel, Sebastián, Daniel, Byron, Rubén, Campo Elian, Brayan y, en un hecho aparentemente aislado, Yessica. Pensé en la tragedia. Pensé en lo irreversible e irreparable de la muerte, de la muerte de quienes son amados.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Parecía que se cerraba una de las semanas más crueles en un país que intenta transitar hacia la paz, pero no. El martes de esta semana asesinaron en el Resguardo Pialapi-Pueblo Viejo a tres indígenas Awá, eran Lumar, John y Eyder. El viernes en el corregimiento El Caracol en Arauca, en la frontera con Venezuela, fueron asesinadas cinco personas que aún no han sido identificadas; ese mismo día en El Tambo, en Cauca, asesinaron a Carlos, Arcadio, Yoimar, David, Nicolas y una sexta persona que aún no ha podido ser identificada. Y en la mañana del sábado seis jovenes fueron asesinados en Tumaco y otros dos siguen desaparecidos.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

La muerte nos sacude ¿y los medios de comunicación? Concentrados en tres tareas: la primera, entrevistar durante horas a Álvaro, el expresidente detenido, me corrijo, “secuestrado” en su hacienda por la Corte Suprema de Justicia. La segunda, aplaudir de pie la gestión de Iván Duque de cara a la pandemia, aplaudir su “liderazgo y el respaldo de las encuestas”. Y la tercera, justificar la muerte de 18 personas: “por no cumplir el aislamiento, por viciosos, por empobrecidos, por negros, por caminar por donde no debían, por ser hija de guerrilleros, por gay, por nacer donde están los narcos, por indígenas, por oponerse al desarrollo, por levantar sospechas, por venezolanos”, dicen reporteros, analistas y presentadores.  

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Y, ¿qué hace el Gobierno Nacional? Esta semana el presidente Iván Duque concentró sus esfuerzos en supervisar la llegada de un avión a Rionegro y se desplazó a Samaniego a prometer a las familias de las víctimas arreglar el estadio de fútbol, mientras la ministra del Interior, Alicia Arango, lloraba desconsolada en una sesión del Senado. No lloraba por las masacres de los jóvenes, tampoco por la tragedia de las comunidades negras e indígenas y mucho menos por las víctimas de feminicidios. Lloraba por que Álvaro, el expresidente detenido, renunció a su curul en el Senado. Me sorprendí, no por ingenuidad, me esfuerzo por conservar la indignación, el llanto rabioso y el repudio que me produce el cinismo y la estupidez de quienes hoy tienen el poder.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Pues bien, esto no es nada diferente al uribismo en el poder, estas son las masacres aplazadas, las masacres con criterio social de las que habla Álvaro, los asesinatos colectivos de los que habla Iván y las “masacres de jóvenes” así entre comillas de las que habla Claudia. La complicidad es evidente, los culpables andan por donde les plazca y actúan en total impunidad, lo hacen en las zonas más militarizadas del país.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En medio de la sangre, el llanto, el silencio y la tragedia me pregunté sí en este país ¿tendremos algún día la posibilidad de desbordarnos de esperanza? La esperanza me había hecho llorar el 21 de noviembre, en el Paro Nacional, mi recuerdo se inundó de imágenes de estudiantes, trabajadores, feministas, la guardia indígena, los firmantes de la paz, los profes, todos en medio de los gritos de dignidad, reclamándole a este gobierno asesino, corrupto, criminal, machista y racista que detenga las masacres que no nos niegue el derecho a vivir. Pero ellos les asesinan para llenarnos de miedo, les asesinan para recordarnos que no tenemos derecho a la paz, les asesinan para que nos resignemos a su poder, les asesinan para robarnos la dignidad.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

*Claro que me gustaría escribir sobre otra cosa, pero mientras nos estemos disputando la vida, la paz, la existencia y el derecho a soñar nuestro deber es manifestarnos sistemáticamente, escribir, gritar y acompañar a las familias de las víctimas que en medio de la tragedia piden justicia* *y defienden sus nombres, las memorias de sus hijos e hijas.*

<!-- /wp:paragraph -->
