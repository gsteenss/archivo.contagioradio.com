Title: Votación en bloque en Congreso esperanza para Acuerdo de Paz: Imelda Daza
Date: 2017-05-25 17:31
Category: Paz, Política
Tags: acuerdo de paz, Fast Track
Slug: votacion-en-bloque-en-congreso-esperanza-para-acuerdo-de-paz-imelda-daza
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/05/pleno-del-congreso-e1495751460417.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Senado] 

###### [25 May 2017]

**La votación en bloque que propuso la Comisión de Paz del Congreso para avanzar en la votación de los debates de Fast Track significa una esperanza**, así lo señaló la vocera de Voces de Paz Imelda Daza que manifestó que podría ser la mejor forma de salir de una decisión política tomada por la Corte Constitucional contra la implementación de los Acuerdos de paz en el país.

Para Daza, el motivo que sustenta la decisión de la Corte Constitucional, referente a la participación de los partidos políticos en los debates, no es cierta “que al Congreso se le hayan cercenado sus facultades, que no pudo opinar libremente sobre los proyectos de ley, **ni sobre los actos legislativos, no es cierto**” afirmó Daza. Le puede interesar: ["Llegó la hora de la verdad para el Acuerdo de Paz en el Congreso"](https://archivo.contagioradio.com/llego-la-hora-de-la-verdad-para-el-acuerdo-de-paz-en-el-congreso/)

Además, agregó que todos los partidos que hacen parte de la coalición del gobierno afirmaron que votarían en bloque y que seguirán presentando proposiciones que no afecten el espíritu ni el contenido del acuerdo, sin embargo, expresó que sí en la etapa que está culminando de Fast Track, ante cada proyecto de ley y acto legislativo existieron tantas voces pertenecientes a esa coalición de gobierno con la intención de hacer modificaciones de fondo al acuerdo, **es preocupante como se puedan tornar los debates en adelante.**

“**Todo depende del manejo que se haga con la coalición que respalda el gobierno, ese es el juego político que se da en el Congreso Colombiano**, que es el reflejo de la debilidad de nuestros llamados partidos políticos” manifestó Daza. Le puede interesar: ["Con movilizaciones y páctos políticos haremos frente a la decisón de la Corte: Cepeda"](https://archivo.contagioradio.com/con-movilizaciones-y-pactos-politicos-haremos-frente-a-decision-de-la-corte-ivan-cepeda/)

De igual forma, la vocera de Voces de Paz recordó que el **Congreso avaló el acuerdo de paz, por lo que no tendría sentido que ahora se pretenda renegociar** cada punto de lo pactado, sino que se debería avanzar de manera urgente en los trámites legislativos.

<iframe id="audio_18903963" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_18903963_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
