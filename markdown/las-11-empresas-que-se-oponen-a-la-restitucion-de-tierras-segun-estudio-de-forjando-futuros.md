Title: Las 11 empresas  que se oponen a la Restitución de Tierras según estudio de ONG
Date: 2016-04-13 12:48
Category: DDHH, Nacional
Tags: despojo de tierras, Paramilitarismo, Restitución de tierras, tierras
Slug: las-11-empresas-que-se-oponen-a-la-restitucion-de-tierras-segun-estudio-de-forjando-futuros
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/04/empresas-restitucion-de-tierras.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: contagioradio.com] 

###### [Abr 13 2016] 

Según el último informe hecho por la Fundación Forjando Futuros y la Universidad de Antioquia, [sobre la tenencia y restitución de tierras en Colombia, existen cinco personas y](https://archivo.contagioradio.com/estos-son-los-enemigos-de-la-restitucion-de-tierras-en-magdalena-medio/)once empresas que se oponen a que se restituyan las tierras a las víctimas del despojo y el conflicto armado.

Este grupo de personas y empresas se han hecho acreedoras de grandes extensiones de tierras en diferentes zonas del país durante los años y su mayor argumento para oponerse a la restitución de tierras, es acudir al principio de tenencia de buena fe, sin embargo, ninguno de los tenedores de tierras ha probado la buena fe en los procesos judiciales.  
Las empresas que conforman la lista de opositoras a la restitución de tierras a víctimas del despojo son:

**Argos S.A (Fiducor S.A.):** Esta empresa compro más de 12.500 hectáreas en la zona de Montes de María, región en donde se cometieron actos de barbarie como las masacres de Palo Alto, Libertad, El Salado, Chengue y Macayepo. Además, diferentes ejecutivos de Cementos Argos han tenido relaciones con personas vinculadas con grupos paramilitares, es el caso de Ricardo Andrés Sierra Fernández, ex Vicepresidente de Finanzas Corporativas de Cementos Argos, hermano de Juan Felipe Sierra, quien fue capturado el 3 de agosto del año 2008, acusado de ser el enlace entre la organización del jefe paramilitar Daniel Rendón Herrera, alias ‘Don Mario’, y altos funcionarios de la Policía y la Fiscalía en Antioquia y Córdoba.

Juan Felipe Sierra, además, fundó una empresa de seguridad, Control Total Ltda que contaba entre su personal con desmovilizados de las AUC y prestaba servicios de seguridad a la empresa Argos S.A. La licencia de Control Total Ltda., fue cancelada por la Superintendencia de Vigilancia y Seguridad Privada y su nombre fue agregado en la lista Clinton como parte del organigrama de alias ‘Don Mario’. Victoria Eugenia Restrepo Uribe, actual gerente de Reforestadora del Caribe, se vio involucrada en las grabaciones realizadas por la Fiscalía, que ocasionaron la captura del ex fiscal Guillermo León Valencia Cossio, el general Marco Antonio Pedreros y el empresario Juan Felipe Sierra por sus nexos con el paramilitarismo en Antioquia.

El Senador Iván Cepeda ha realizado diferentes acusaciones en donde revela las relaciones de Cementos Argos S.A. con el paramilitarismo.

Argos S.A enfrenta actualmente a 22 familias campesinas que reclaman las 2.000 hectáreas que posee la empresa en San Onofre Sucre, el proceso consta de dos informes de 150 páginas cada uno donde se relata el desplazamiento forzado de las familias y la venta de sus tierras bajo amenazas. Aún no se ha comprobado la tenencia de tierras bajo el principio de buena fe en los procesos judiciales en contra de Argos S.A

**Continental Gold Limited Sucursal Colombia:** Esta empresa de carácter minero que realizaba actividades de explotación en el municipio de Bagadó Choco, se opuso al fallo de la sentencia 007 del 23 de septiembre de 2014, en donde el Tribunal Superior de Antioquia le ordeno restituir a Continental Gold Limited Sucursal Colombia, Anglogold Ashanti Colombia S.A.S y Exploraciones Chocó Colombia S.A.S, 50.000 hectáreas al resguardo indígena Embera Katío del río Alto Andaguedá.  
Continental Gold Limited alego que no existen nexos entre la victimización de los indígenas y la entrega de títulos mineros.  
El municipio de Bagadó y en general el departamento de Choco han sido una de las zonas más fuertemente golpeadas por el fenómeno del paramilitarismo.

**Exploraciones Chocó Colombia S.A.S:** Esta empresa de carácter minero que realizaba actividades de explotación en el municipio de Bagadó Choco, se opuso al fallo de la sentencia 007 del 23 de septiembre de 2014, en donde el Tribunal Superior de Antioquia le ordeno restituir a Exploraciones Chocó Colombia S.A.S, Anglogold Ashanti Colombia S.A.S y Continental Gold Limited Sucursal Colombia 50.000 hectáreas al resguardo indígena Embera Katío del río Alto Andaguedá.

Exploraciones Chocó Colombia S.A.S alego que los títulos mineros no están vinculados al conflicto armado interno que se vive en la región” y que la compañía no había sido responsable del despojo que sufrieron las 33 diferentes comunidades que habitaban el territorio.

El municipio de Bagadó y en general el departamento de Choco ha sido una de las zonas más fuertemente golpeadas por el fenómeno del paramilitarismo.

**Anglogold Ashanti Colombia S.A.S:** Esta empresa de carácter minero que realizaba actividades de explotación en el municipio de Bagadó Choco, se opuso al fallo de la sentencia 007 del 23 de septiembre de 2014, en donde el Tribunal Superior de Antioquia le ordeno restituir a Exploraciones Chocó Colombia S.A.S, Anglogold Ashanti Colombia S.A.S y Continental Gold Limited Sucursal Colombia, 50.000 hectáreas al resguardo indígena Embera Katío del río Alto Andaguedá.

Por otro lado la multinacional aseguro que adquirió lo previos de forma legal, a partir de una concesión de títulos mineros en la zona. Sin embargo, no se ha confirmado el principio de buena fe de la tenencia de las tierras en el proceso judicial.  
El municipio de Bagadó y en general el departamento de Choco ha sido una de las zonas más fuertemente golpeadas por el fenómeno del paramilitarismo.

**Sociedad Agropecuaría Carmen de Bolivar S.A:** La Agropecuaria Carmen de Bolívar S.A, compró en el año 2008 24 hectáreas en el corregimiento de Hato Nuevo, en el Carmen de Bolívar a una familia víctima del desplazamiento forzado, las amenazas por parte de grupos paramilitares y la desaparición forzada de uno de los miembros de la familia.

Según el Tribunal de Cartagena, la empresa actuó de mala fe al comprar los predios a la familia por un precio irrisorio de catorce millones de pesos en el año 2008, cuando la situación que afrontaba el municipio del Carmen de Bolívar era sumamente fuerte por la presencia de grupos paramilitares.

**A. Palacios S.A.S:** Esta compañía compro predios en la vereda de Cuchillo Negro y Bella Rosa en el corregimiento de Macondo, municipio de Turbo. Sin embargo en el año 2015 la Sala Civil de Restitución de Tierras dictamina que los predios en posesión de A. Palacios S.A.S deben ser devueltos a un grupo de campesinos, legitimos dueños que abandonan las tierras como víctimas del desplazamiento forzado.

La empresa ha expresado que en el momento de realizar la compra actuó de buena fe, además alega que las diferentes pruebas presentadas al juez no comprueban el hecho del despojo o desplazamiento del territorio.

**Todo Tiempo S.A.S:** Esta empresa de carácter agrícola y ganadera era propietaria de la Hacienda Monte Verde, una parcelación que se conformaba por 5 predios: Mi Bohio, La Esperanza, Nutibara I, Nutibara II y Los Popochos, ubicada en la vereda los Cedros de Mutatá, actualmente en proceso de restitución de tierras gracias al fallo proferido por el Alto Tribunal de Antioquía quién no reconoció el principio de buena fe de la empresa al adquirir las tierras y que ordenó que sea restituida a cuatro familia de campesinos y a una etnia indígena Embera.

Todo Tiempo S.A.S era una de las empresas con mayor concentración de tierras en Mutatá, alrededor de un 70% solicitadas para restitución de Tierras.

**Agropecuaria Palmas de Bajirá S.A: **La Fiscalía imputo cargos a esta empresa en el año 2007 por los delitos de usurpación de tierras, falsedad en documento público y privado, atentar contra los recursos naturales, invasión de arenas de importancia ecológica y desplazamiento forzado en los territorios de Curvaradó y Jiguamiandó.

**En la lista, también están incluidad las empresas: Sociedad Las Palmas Límitada, Inversiones Futuro Verde S.A, y Palmagan S.A.S.**

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)
