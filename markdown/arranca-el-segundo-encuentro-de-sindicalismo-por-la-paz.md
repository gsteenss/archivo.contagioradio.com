Title: El Sindicalismo y la tarea de la construcción de la paz
Date: 2015-08-27 12:20
Category: DDHH, Economía, Movilización, Nacional
Tags: central unitaria de trabajadores, colectivo de Abogados José Alvear Restrepo, Conversacioines de paz en Colombia, cpdh, CUT, ELN, FARC, justicia transicional en colombia, Mesa de conversaciones de paz de la habana, paz en colombia, Reparación a las víctimas
Slug: arranca-el-segundo-encuentro-de-sindicalismo-por-la-paz
Status: published

###### [Foto: contagioradio.com ] 

###### <iframe src="http://www.ivoox.com/player_ek_7636612_2_1.html?data=mJugmJuVdo6ZmKiakpWJd6KkkZKSmaiRdI6ZmKiakpKJe6ShkZKSmaiRhdPmwtPQw5DJsIznxszi0MnTb8bixNrS0NnWs4zYxpDAy9PIrcTVzc7gz9SPtNDmjNHOjdXFvo6ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe> 

###### [Domingo Tovar, CUT] 

###### [27 Ago 2015] 

[Entre el jueves 27 y el viernes 28 de agosto se llevará a cabo el S**egundo Encuentro de Sindicalismo por la Paz**, iniciativa liderada por la **Central Unitaria de Trabajadores**, que pretende fortalecer el papel del sindicalismo como actor político en la construcción de paz con justicia social y pronunciarse formalmente en la mesa de conversaciones Gobierno - FARC y la posible con el ELN.]

[El evento contará con la participación de las seccionales de derechos humanos de la CUT, el **Colectivo de Abogados José Alvear Restrepo, el Comité Permanente por los Derechos Humanos** y la Oficina Consejería Presidencial para la Paz.]

[Domingo Tovar, encargado de derechos humanos de la CUT, asegura que la intención de este segundo encuentro es avanzar en la agenda de trabajo del sindicalismo colombiano como motor de la producción y el desarrollo en el **respaldo a las propuestas de negociación que permitan una salida política al conflicto armado** y la consolidación de una paz duradera.]

[Durante el encuentro se evaluaran los acuerdos a los que se han llegado en La Habana y la posible **implementación de la mesa de conversaciones con el ELN, en relación con el lugar de las víctimas y su reparación integral en clave de justicia transicional**, teniendo en cuenta que el movimiento sindical ha sido violentado en Colombia desde 1908.]

[El proceso de paz, afirma Tovar, implica la construcción de un modelo de desarrollo distinto al actual, a través del cual se reorganice el sistema productivo para el impulso de la soberanía alimentaria y la garantía de condiciones laborales dignas.]
