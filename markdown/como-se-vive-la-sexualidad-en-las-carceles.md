Title: ¿Cómo se vive la sexualidad en las cárceles?
Date: 2019-04-02 20:30
Author: CtgAdm
Category: Expreso Libertad
Tags: carceles de colombia, presos politicos
Slug: como-se-vive-la-sexualidad-en-las-carceles
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/Amor-tras-las-rejas-reos-de-El-Pozo-se-casan-2.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Abriendo brecha 

###### 25 Mar 2019 

Desde discriminación hasta falta de garantías para estar con sus parejas, son algunas de las situaciones que deben enfrentar las personas que **intentan solicitar el derecho a la visita conyugal**.

En este programa del Expreso Libertad, **Liliana Gutiérrez** comentó las múltiples violaciones a derechos humanos que deben vivir las y los prisioneros en los centros de reclusión a la hora de sostener relaciones sexuales.

Entre las denuncias más graves se encuentran **la falta de un espacio para que se lleve a cabo el encuentro sin vulnerar a la dignidad de las personas; la falta de métodos de protección y planificación; y la violencia en contra de las parejas que se identifican con otras orientaciones sexuales**, distintas a la heterosexual.

<iframe id="audio_33932235" src="https://co.ivoox.com/es/player_ej_33932235_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Encuentre más información sobre Prisioneros políticos en[ Expreso libertad ](https://archivo.contagioradio.com/programas/expreso-libertad/)y escúchelo los martes a partir de las 11 a.m. en [Contagio Radio](https://archivo.contagioradio.com/alaire.html) 
