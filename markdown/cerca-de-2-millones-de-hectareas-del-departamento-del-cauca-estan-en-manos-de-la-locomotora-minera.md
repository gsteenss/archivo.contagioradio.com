Title: Más del 50 % del Cauca en amenaza por intereses mineros
Date: 2015-11-27 15:36
Category: Ambiente, Nacional
Tags: Alberto Castilla, alirio uribe, audiencia minera en el cauca, daño ambiental, Gran mineria en el Cauca, Iván Cepeda, Títulos mineros Cauca
Slug: cerca-de-2-millones-de-hectareas-del-departamento-del-cauca-estan-en-manos-de-la-locomotora-minera
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/11/maxresdefault-10.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### [27 Nov 2015]

Según los datos entregados por las propias comunidades en la audiencia pública realizada este 27 de Noviembre en la ciudad de Popayán, sobre el territorio del departamento hay presencia de intereses mineros y energéticos que abarcan 1.913.943 de las 3.101.532 hectáreas que tiene el departamento y en las cuales habitan cerca de un millón trescientos mil habitantes, lo que indica que la población estaría siendo fruto de confinamiento.

Según esas misas cifras, en el Cauca hay 259 títulos mineros en 380.654 hectáreas y unas 386 solicitudes sobre 681.701 hectáreas, es decir que más de la tercera parte del territorio se enfrenta a los intereses de las empresas mineras lideradas por la Anglo Gold Ashanti, que a través de diversas filiales con 39 títulos para un total de 61.000has y otras 46 solicitudes para otros 137.000 hectáreas.

En cuanto al impacto ambiental que denuncian las comunidades el panorama es más que alarmante, puesto que en 16 de los municipios con mayor densidad poblacional, por lo menos 29 fuentes de agua entre ríos y quebradas se están viendo afectadas y esto podría provocar una crisis ambiental más grave que la actual.

Sumado a esto se denuncian las amenazas y asesinatos de varios líderes campesinos e indígenas, las judicializaciones de varios de ellos y ellas y la fuerte militarización que enfrenta el departamento. A este respecto el Senador Cepeda recordó que muchas veces los convenios militares con las empresas hacen que se supedite la función de las FFMM a las de empresas de seguridad privada al servicio de las multinacionales.

Durante la audiencia pública a la que asistieron los senadores Iván Cepeda, Alberto Castilla y el representante a la Cámara Alirio Uribe, los campesinos, indígenas y comunidades afrodescendientes plantearon tanto las problemáticas como las propuestas entre las que se destacan el fortalecimiento de la consulta previa y el reconocimiento de la autonomía y la soberanía de las propias comunidades sobre sus territorios.
