Title: Lo que diga Don Berna puede "abrir otro capítulo" en investigación por el asesinato de Jaime Garzón
Date: 2015-08-11 17:14
Category: DDHH, Nacional
Tags: Don Berna, General Harold Bedoya, General Mora Rangel, Jaime Garzon, Neoparamilitarismo, Paramilitares de La Terraza, Paramilitarismo
Slug: lo-que-diga-don-berna-puede-abrir-otro-capitulo-en-investigacion-por-el-asesinato-de-jaime-garzon
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/08/jaime-garzon.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Gabriel Galindo - Contagio Radio] 

<iframe src="http://www.ivoox.com/player_ek_6553847_2_1.html?data=l5qilZ2Ye46ZmKiak5WJd6KpkZKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRiNDijKfS1NPFb8bi1dfSycaPqMbXzcbfw8jNaaSnhqeg0JDXs8PmxpDO1crXrc%2FV1dSYxsqPjsLdzsqah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [11 Ago 2015]

El abogado Sebastián Escobar, representante de la familia de Jaime Garzón afirma que en la versión libre que **“Don Berna**” entrega durante esta semana desde Estados Unidos hay especulación de los medios de información, sin embargo también hay esperanzas, puesto que **la información que entregue el jefe paramilitar puede aportar al avance de la justicia en cuanto a los autores intelectuales del asesinato del periodista.**

Según Escobar, un video del grupo paramilitar “La Terraza” da algunas luces en torno a quienes serían los autores intelectuales de ese asesinato, por ejemplo, recuerda que los paramilitares mencionan a los **generales Mora Rangel y Harold Bedoya como determinantes en el crimen.**

“Consideramos que personas desmovilizadas como **Diego Fernando Murillo**, tienen una responsabilidad y un compromiso con la verdad… y para que puedan ser acreedores a una pena alternativa ellos tienen que contar la verdad de todo lo que conocieron mientras fueron parte de las estructuras de los paramilitares” afirma el abogado.

La gran expectativa radicaría en que amplíe los hechos y se documente con mayor rigor a los responsables del magnicidio de Jaime Garzón, documentación que **podría ser el principio de otro capítulo en la investigación y la búsqueda de justicia por este crimen.**
