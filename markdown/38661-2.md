Title: 600 personas del Río Truandó en Chocó fueron desplazadas por presencia paramilitar
Date: 2017-03-31 14:53
Category: DDHH, Nacional
Tags: Chocó, Desplazamiento, Riosucio
Slug: 38661-2
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/06/El-Bagre-paramilitarismo.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo] 

###### [31 Mar. 2017] 

**En Ríosucio, Chocó, cerca de 600 personas** entre niños, mujeres, hombres y adultos mayores, que son parte **de las comunidades del Río Truandó, se encuentran desplazadas y confinadas en el Coliseo de ese municipio**, según varios pobladores de la zona, el desplazamiento se debe la presencia de grupos paramilitares. Le puede interesar: [Persiste la presencia de paramilitares sobre el Río Tamboral en el Chocó](https://archivo.contagioradio.com/persiste-la-presencia-de-estructuras-neoparamilitares-sobre-el-rio-tamboral-en-el-choco/)

“La razón es por el enfrentamiento que se está dando de los grupos al margen de la ley. Después de que salen las FARC entran los “Los Urabeños” y las autodenominadas “Autodefensas Gaitanistas” dijo Apolinar Murillo, integrante de esta comunidad.

**Además de la presencia paramilitar, Apolinar asegura que estas familias han estado obligadas al confinamiento** puesto que el territorio en gran parte esta minado “las comunidades no pueden realzar sus prácticas tradicionales. A uno le dicen no se meta por ahí, porque por ahí hay una mina y eso se convierte en una amenaza”.

Según Apolinar, **las familias se encuentran en el coliseo de Riosucio** pero con el pasar del los días han continuado llegando personas, por lo que **las están ubicando en la Plaza de Mercado del municipio** “hasta que se nos defina la situación permaneceremos allí. La gente no va a retornar de manera voluntaria” insiste. Le puede interesar: [Comunidades indígenas de Jiguamiando, Chocó asediadas por paramilitares](https://archivo.contagioradio.com/pparamilitares_jiguamiando_choco/)

Ante la respuesta estatal, el poblador relata que **el Gobierno nacional no les ha dado la cara hasta el momento** “necesitamos que el Gobierno venga y responda a ver qué es lo que va a pasar y cómo es que se va a hacer un retorno digno, para solucionar este flagelo de violencia”. Le puede interesar: [Paramilitares se toman caserío de Domingodó en el Chocó](https://archivo.contagioradio.com/paramilitares-se-toman-caserio-de-domingodo-en-el-choco/)

Este viernes las comunidades estarán realizando una reunión para estructurar un pliego de peticiones “**allí estará consignado todo lo que exigimos para un retorno digno.** Tienen que resolver la situación, esto viene desde el 97 con el desplazamiento masivo que hubo en la región y ahora se viene otro. Esos son problemas que hay que resolver. **Necesitamos saber quién se va a sentar con nosotros para resolver los problemas”** concluyó Apolinar.

###### Reciba toda la información de Contagio Radio en [[su correo]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio]{.s1}](http://bit.ly/1ICYhVU)[.]{.s1}

<div class="ssba ssba-wrap">

</div>

<div class="ssba ssba-wrap">

</div>

<div class="ssba ssba-wrap">

</div>

<div class="ssba ssba-wrap">

</div>
