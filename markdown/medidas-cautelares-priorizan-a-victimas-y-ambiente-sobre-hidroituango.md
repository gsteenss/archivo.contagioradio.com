Title: Medidas cautelares priorizan a víctimas y ambiente sobre Hidroituango
Date: 2019-06-13 16:05
Author: CtgAdm
Category: Ambiente, Comunidad
Tags: Empresas Públicas de Medellín, Fiscalía General de la Nación, Hidroituango, medidas cautelares
Slug: medidas-cautelares-priorizan-a-victimas-y-ambiente-sobre-hidroituango
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/Captura-de-pantalla-93.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Colprensa] 

En respuesta a las medidas cautelares solicitadas por la Fiscalía en el caso de Hidroituango, el juez 75 penal de control de garantías de Bogotá ordenó este miércoles **la activación de un plan de contingencia** para la protección de los derechos de los afectados por la hidroeléctrica, así como la creación de **una mesa técnica para estudiar la viabilidad del proyecto**.

Durante tres audiencias, la Fiscalía y organizaciones de víctimas argumentaron que las afectaciones generadas por el mega proyecto para el ambiente y las comunidades en las zona de influencia exigían la implementación de medidas cautelares, en particular porque Empresas Públicas de Medellín (EPM) había fallado en desarrollar medidas efectivas para mitigar los impactos. (Le puede interesar: "[Fiscalía pide medidas cautelares para comunidades aledañas a Hidroituango](https://archivo.contagioradio.com/fiscalia-pide-medidas-cautelares-para-comunidades-aledanas-a-hidroituango/)")

Entre los riesgos identificados por la Fiscalía se encuentran **un incremento en casos de leishmaniasis**, posiblemente relacionados con la inadecuada disposición de árboles talados para la construcción de Hidroituango, así como la **invasión extensiva de la planta acuática buchón por el Río Cauca.** (Le puede interesar: "[Casos de leishmaniasis incrementan en zona de influencia de Hidroituango](https://archivo.contagioradio.com/casos-de-leishmaniasis-incrementan-en-zona-de-influencia-de-hidroituango/)")

Además, las víctimas señalaron que las comunidades que dependen del Río Cauca para su alimentación actualmente sufren de hambre a causa de las alteraciones de los ecosistemas del cauce que la empresa ha provocado.

"El juez interpretó las solicitudes que hizo de manera puntual la Fiscalía y las solicitudes que hicieron las víctimas durante el marco de las audiencias y las recogió en dos medidas cautelares", afirmó Isabel Cristina Zuleta, vocera del Movimiento Ríos Vivos - Antioquia, agregando que estas fueron otorgadas bajo la presunción que EPM cometió un delito ambiental al construir presuntamente un túnel de desviación sin licencia ambiental.

Cabe recordar que dicho túnel colapsó dos veces en abril del año pasado. La segunda falla, ocurrida el 30 de abril del 2018, fue definitiva y, según un estudio de la firma noruega Skava Consulting, resultó en las inundaciones del Río Cauca que afectaron a más de 60 mil pobladores.

### **Las medidas cautelares**

En primer lugar, el juez ordenó la activación de un plan de contingencia para todas las comunidades ribereñas de zona de afectación de Hidroituango, con el propósito de garantizar sus derechos a la salud, a una vida digna, a la alimentación y a un ambiente sano, entre otros.

La segunda medida cautelar ordena la creación de una mesa técnica en máximo cinco días, en la cual participará la Fiscalía, la Procuraduría, la Contraloría, la Gobernación de Antioquia, el Ministerio de Ambiente, Ministerio de Salud, la Autoridad Nacional de Licencias Ambientales, EPM e Hidroituango, así como las víctimas, representadas por el Movimiento Ríos Vivos - Antioquia.

La mesa técnica tendrá cómo objetivo determinar la viabilidad del proyecto y para tal fin contará con informes de EPM, entregados cada 10 días. Finalmente, deberá presentar en menos de 60 días una decisión sobre la continuidad o suspensión definitiva o temporal del proyecto Hidroituango.

Por su parte EPM apeló esta decisión del juez penal, sin embargo no afectará los plazos establecidos para la creación y la implementación de los organismos de control.

<iframe id="audio_37071743" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_37071743_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
