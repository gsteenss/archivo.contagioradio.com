Title: Sierra Nevada de Santa Marta en riesgo por construcción de Hidroeléctrica
Date: 2016-11-28 17:15
Category: Ambiente, Nacional
Tags: comunidades indígenas, Hidroeléctrica en Sierra Nevada
Slug: sierra-nevada-de-santa-marta-en-riesgo-por-construccion-de-hidroelectrica
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/sierra-nevada-santa-marta1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: CafiCosta] 

###### [28 Nov 2016]

Comunidades indígenas de la zona rural de Santa Marta se encuentran protestando por la construcción de una Hidróelectrica en el río Do Diego ubicado en la Sierra Nevada y que **captaría el 80% del agua que produce esta fuente hídrica**. Hecho que no solo afectaría a las comunidades indígenas de esta zona del país sino también el ecosistema de una de las reservas de la biosfera más importantes según la UNESCO.

Los intereses detrás de esta Hidroeléctrica serían de la empresa Elemental SAS que desde finales del 2011 habría iniciado el proceso para construir este proyecto.  Sin embargo, de acuerdo con indígenas y campesinos que habitan esta zona, **el proceso de consulta previa a las comunidades no se realizó** y tan solo se hicieron algunas charlas con líderes de diferentes etnias.

Y es que el Magdalena es uno de los departamentos más afectado ambientalmente, la Ciénaga de Santa Marta es uno de los ecosistemas que se encuentra en vía de extinción debido a los maltratos y la falta de protección a la que ha sido sometida, además de la **crisis por recursos hídricos que vive esta región la Sierra Nevada de Santa Marta es uno de los mayores afluentes de agua** para la gran mayoría de la población que habitan en este departamento. Le puede interesar:["Si no somos capaces de defender la Ciénaga Grande no podría haber paz: Manuel Rodríguez"](https://archivo.contagioradio.com/si-no-somos-capaces-de-defender-la-cienaga-grande-no-podria-haber-paz-manuel-rodriguez/)

Este proyecto estaría conformado por  una hidroeléctrica bajo pilo de agua, que generaría que se desvíe el curso del río para pasarlos por un canal, que **tendría afectaciones similares a las producidas en la desviación del Arroyo Bruno en la Guajira**.  De igual forma el proyecto aún no ha especificado que sucedería con las aguas residuales. Le puede interesar:["Cerrejón  desalojaría 80 familias indígenas para desviar Arroyo Bruno"](https://archivo.contagioradio.com/cerrejon-desalojaria-80-indigenas-desviar-arroyo-bruno/)

Por el momento las comunidades campesinas e indígenas han expresado que no aceptarán la construcción de este proyecto mientras se espera una reunión que se llevará a cabo el próximo **6 de diciembre entre ambas partes, para analizar la viabilidad de la Hidroeléctrica.**

###### Reciba toda la información de Contagio Radio en [[su correo]
