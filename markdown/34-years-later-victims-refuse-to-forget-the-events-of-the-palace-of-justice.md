Title: 34 years later, victims refuse to forget the events of the Palace of Justice
Date: 2019-11-08 13:13
Author: CtgAdm
Category: English
Tags: forced disappearances, Palace of Justice
Slug: 34-years-later-victims-refuse-to-forget-the-events-of-the-palace-of-justice
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/WhatsApp-Image-2019-11-07-at-2.56.46-PM.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Photo: Contagio Radio] 

 

**Follow us on Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

[Colombia’s State Council paid homage on Wednesday to the victims of the Palace of Justice siege, which occurred on November 6 and 7 of 1985. Two survivors and two family members of disappeared victims recounted the events that took place and the truth they have been able to reconstruct 32 years later. Among the speakers were Carlos Betancur Jaramillo, court advisor during 21 years; Nubia González Cerón, court rapporteur; Johana Patricia Angulo, the daughter of Blanca Inés Ramírez Suárez who worked as a State Council assistant and died at the Palace; and René Guarín, brother of Cristina Guarín, who was assassinated and disappeared.]

### **A series of unfortunate coincidences**

[In the reports of what occurred on November 6, 1985, there is a common factor: Each of the speakers found themselves at the Palace of Justice that day by coincidence. Nubía González explained that as a rapporteur, she occasionally visited La Picota prison to review the annual records and decided to do so that day. Although usually she took all day to finish her task, but that day she decided to end before midday and return to the State Council 30 minutes before the siege occurred.]

[René Guarín said that his sister was a woman of 27 years old, who spoke and read French and planned to study in Spain in a few months at the Complutense University of Madrid. At the time of the siege, Cristian Gaurín had only been working 36 days in the Palace’s cafeteria as a cashier because she was replacing another worker who was on maternity leave. ]

[Carlos Betancur Jaramillo said that as president of the State Council, he participated in a meeting with police on Oct. 17 to inform law enforcement that a plan to take over the palace had been detected. In that moment, a security strategy was considered that included the installation of safety glass, security cameras and electric fences around the building. This proposal was discarded in favor of an increase in security personnel around the premises because of a limited budget to buy the supplies.]

[The lawyer recalled that three days before the siege, the additional troops were withdrawn — a decision that the lawyer speculates could have been taken deliberately to facilitate the siege. Another coincidence that Betancur pointed out is that the palace was taken over 30 minutes before the Supreme Court discussed the Law of Extradition, which has prompted some to speculate that the drug kingpin Pablo Escobar financed the M-19 guerrilla group’s siege to prevent the approval of the law.]

### **The truths behind the siege**

[Johana Ángulo, daughter of Blanca Inés Ramírez Suárez, who died in 1985, said that thanks to the ruling of the Inter-American Court on Human Rights, which ordered the exhumation of the victims’ bodies, she and her family realized that the remains they received didn’t belong to her mother. Blanca Inés was disappeared for 31 years until she was found in 2016, thanks to the Court, in a common grave in a Bogotá cemetery. ]

[The family of Jorge Alberto Echeverry Correa also find out that the remains they received belonged to Bernardo Beltrán Hernández, who was a waiter at the cafeteria. Meanwhile, the family of the assistant judge Julio César Andrade also discovered there was a mix-up in the remains they received. They actually belonged to Héctor Jaime Beltrán Fuentes, a waiter at the cafeteria as well. ]

[René Guarín said that military intelligence took over the Medical Forensics Agency and denied some of the events. Guarín also recalled that leaked videos showed the military officials telling other officers that “if the sleeve appears, the vest won’t,” a statement that was interpreted as an order to cut the bodies of the disappeared persons.]

[The brother of Cristina Guarín said that the state is wrong to say that there were not forced disappearances in the Palace of Justice and that this statement contradicts the ruling of the international court system.]

### "The best way to help those responsible is to forget"

Betancur said that the "best way to help those responsible is to forget and that is what Colombia has done." She said one of the unanswered questions that remains is why the police went to the amphitheater on November 7, 1985 to collect unidentified bodies that later reappeared in a cemetery in southern Bogotá.

Guarín said that family members of the disappeared want to reconcile with those responsible, but they don't know who they are or what occurred because the truth has yet to be revealed. Consequently, they said they would continue to search for the truth and for the disappeared.

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio] {#reciba-toda-la-información-de-contagio-radio-en-su-correo-o-escúchela-de-lunes-a-viernes-de-8-a-10-de-la-mañana-en-otra-mirada-por-contagio-radio.}
