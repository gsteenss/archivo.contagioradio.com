Title: Aumento de tropas de EEUU en frontera con Panamá ¿Qué esta pasando?
Date: 2019-01-26 14:07
Author: AdminContagio
Category: Nacional
Tags: EEUU, militares, Venezuela
Slug: eeuu-atacar-venezuela-darien
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/01/05280983_xl-770x400.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Archivo Hispan TV 

###### 25 Ene 2019 

En el trascurso de este primer mes del año, **pobladores de la región fronteriza del Darién, limítrofe entre Colombia y Panamá, vienen denunciando un fuerte movimiento de tropas militares norteamericanas**, incluyendo sobrevuelo de helicópteros artillados, y el **traslado de equipos de maquinaria pesada en zona selvática**.

Según Walyd Zayed, defensor de Derechos Humanos en Panamá, lo que esta ocurriendo obedece a una intención de "**reescriturar democráticamente el establecimiento de una ocupación norteamericana en territorio panameño nuevamente**" obedeciendo a los **tratados Salas - Becker**, resultantes de la invasión estadounidense ocurrida en 1989, que a juicio del activista **fueron hasta ahora manejados con un bajo perfil**.

Zayed plantea que **la policia panameña ha permitido que tropas del Comando Sur Unificado de Miami ingresen bajo el pretexto de realizar ejercicios y prácticas militares en la zona** y el traslado de equipo pesado, supuestamente de propiedad de los uniformados panameños para combatir "redes ilícitas trasnacionales", lo que en su criterio no tiene sentido ya que ese país **no cuenta con un ejército que amerite la presencia ni de la maquinaria ni de la misma policía**.

Lo que advierten es que **se trate de la construcción de una base militar panameña pero manejada por norteamericanos**, levantada en lugares poco habitados para ocultar lo que allí se realiza. Han sido **algunas comunidades indígenas que habitan en la región, quienes han notado la presencia de los helicópteros y las tropas**.

"Hay un acantonamiento militar en la frontera no hay fotografías casi prácticamente, aparte de los indígenas que habitan el Darién porque observan la cantidad de helicopteros que sobrevuelan hacia la dirección fronteriza" asegura Zayed y agrega que **"hay un silencio cómplice" por parte de los medios de comunicación** **"como si el panameño quisiera que los gringos regresaran".**

### **¿Una invasión a Venezuela desde el Darién?** 

Hace aproximadamente un mes en los Cayos de Florida, **habría tenido lugar una sorpresiva reunión entre el presidente Varela**, el Ministro de seguridad y el subdirector de la Policía de Panamá, donde se encuentra un centro de comando y monitoreo militar norteamericano para el caribe, Colombia y Venezuela, **una visita relámpago de 6 horas en la que Zayed asegura se le habría notificado de las próximas operaciones norteamericanas.**

Parte de esas decisiones, tendrían que ver con **la construcción de pistas de aterrizaje en la zona del Darién para aeronaves de guerra estadounidenses destinadas a atacar puntos neurálgicos en Venezuela**, mismas que asegura el activista no le conviene a Colombia construir en su territorio y que le resultarían **más baratas para la logística militar norteamericana** que tener sus porta aviones en el Atlántico esperando a salir para un eventual ataque.

Por último, Zayed reflexiona sobre el momento mediático que vive Panamá con la visita del Papa Francisco a su país y el tema de Venezuela como focos principales de la atención, lo que no debe servir para evitar mantenerse alerta frente a lo que esta ocurriendo. "**confiamos en que no se de enfrentamiento militar y en la sensatez delos opositores para que no sigan el camino de ese discurso** que no se dejen manipular desde las oficinas en Miami, Nueva York, Panamá o Bogotá, que quieran ser noticia de oposición pero no quieran entender que la causa sea el tema democrático sino el bloqueo norteamericano".

Las tensiones internacionales de las últimas semanas han llevado a inferir que exista tal posibilidad, sin embargo desde **el Servicio Nacional de fronteras (Senafront)** informan que se trata de una misión conjunta denominada **"Darien fit"** que se extenderá hasta el mes de febrero con dos propósitos, **primero establecer las instalaciones necesarias para mejorar las posición de la fuerza panameña en el área y segundo lograr mejores resultados en las operaciones conjuntas entre organismos de EEUU y Panamá.**

Segun informa la entidad estatal, **los helicópteros llevaran la carga desde el aeropuerto Nicanor en Memeti a diferentes bases fronterizas de Senafront entre ellas Panusa, La bonga, La olla, alto limón y La unión.**

<iframe id="audio_31796495" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_31796495_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
