Title: Corte Constitucional restringe los "súper poderes" del Código de Policía
Date: 2017-04-06 16:24
Category: DDHH, Entrevistas
Tags: código de policía, Fuerzas militares
Slug: corte-constitucional-restringe-los-super-poderes-del-codigo-de-policia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/06/código-de-policía.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo Particular] 

###### [06 Abril 2017] 

La Corte Constitucional se pronunció sobre dos de las 24 demandas, interpuestas contra el Código de Policía, la institución señaló que referente a las posibilidades que tenía la **Policía de ingresar a las viviendas de las personas sin una orden judicial deben existir condiciones que permitan ese ingreso**, de igual modo, aclaró las normas sobre **la invasión al espacio público y los vendedores ambulantes.**

Respecto al primer punto la Corte expresó que el Congreso deberá crear, en un plazo máximo de dos años, una **norma que establezca un control posterior a los ingresos de los policías a las viviendas, de esta forma un juez tendrá que recibir un informe sobre los motivos de entrada** al inmueble y evaluará si estos se encontraban dentro de los parámetros establecidos por el Código de Policía.

Para la representante ante la Cámara, por la Alianza Verde, Ángela María Robledo esta decisión de la Corte es producto de la presión social, sin embargo, señala que esperaban que la Corte fuera mucho más garante del derecho a la intimidad. **No obstante, considera que la medida es un “reten para evitar desmanes” por parte de la Policía. **Le puede interesar:["Ya son 24 las demandas al Código de Policía"](https://archivo.contagioradio.com/ya-son-24-las-demandas-al-codigo-de-policia/)

Frente a la invasión al espacio público, la Corte indicó que los trabajadores ambulates están denominados como de **“legitima confianza” por lo tanto la Policía deberá respetar su derecho al trabajo. **Le puede interesar: ["Nuevo Código de Policía otorga poderes exorbitantes a los uniformados"](https://archivo.contagioradio.com/nuevo-codigo-de-policia-otorga-poderes-exorbitantes-a-los-uniformados/)

De igual modo, sobre los desalojos a vendedores informales la Policía podrá realizarlos, sin embargo, las multas que se **impongan solo se harán efectivas una vez los alcaldes de todo el país establezcan zonas de reubicación para estar personas.**

La Representante, señalo que de todas formas aún quedan mucho otros puntos del Código de Policía sobre los que tiene que pronunciarse la Corte Constitucional como **la reglamentación de la protesta, los derechos de habitantes de la calle, la intervención del espacio público de los adolescentes, entre otros.**

Finalmente, Robledo también expresó que hay una necesidad por hacer una profunda reforma de las Fuerzas de Policía y del Ejercito de Colombia, “**tiene que haber una profunda transformación de la forma en la que se ha entendido la violencia, que pasa por una reconfiguración del sentir cívico**”.

<iframe id="audio_18068467" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_18068467_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
