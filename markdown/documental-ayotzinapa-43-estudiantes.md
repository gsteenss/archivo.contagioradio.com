Title: Documental acompaña la lucha de familiares en Ayotzinapa
Date: 2017-01-25 17:22
Author: AdminContagio
Category: 24 Cuadros
Tags: Ayotzinapa, desparecidos, Documental, mexico
Slug: documental-ayotzinapa-43-estudiantes
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/01/ayotzinapa.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### Foto: Field of Vision 

###### 25 Ene 2017 

**"Vivos se los llevaron"**, es el título del cortometraje documental dirigido por la fotógrafa estadounidense Emily Pederson, en el que **acompaña a los familiares de los estudiantes desaparecidos de la Normal Rural Raúl Isidro Burgos de Ayotzinapa**, Guerrero, en su exigencia de respuestas al gobierno mexicano.

Luego de culminar sus estudios universitarios, Pederson, de 27 años de edad, se encontraba radicada en Chiapas para septiembre de 2014, fecha en la que ocurrieron las desapariciones de Iguala, un hecho que por su formación en materia de derechos humanos **le motivó a fotografiar durante 5 meses a los familiares** mientras recorrían el pais pidiendo un mejor manejo en las investigaciones.

"Yo quería comunicar el costo humano de que las autoridades no tengan voluntad de que esto sea resuelto” aseguró la directora en un artículo publicado por The New York Times, en referencia a las negligencias y contradicciones del gobierno Peña Nieto frente al caso, agregando que “Ayotzinapa es el símbolo donde más ha salido a relucir la verdadera cara de México al ojo público”. Le puede interesar:[GIEI No podrá continuar investigando el caso Ayotzinapa](https://archivo.contagioradio.com/giei-no-podra-continuar-investigando-caso-ayotzinapa/).

<iframe src="https://www.youtube.com/embed/Uqh2TW9JZao" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

Con la producción de **Field of visión**, compañía fundada entre otras por Laura Poitras, **directora del documental 'Citizefour' sobre Edward Snowden** y la revelaciones sobre la red de vigilancia mundial, el documental será presentado en Estados Unidos durante el mes de abril.

Utilizando el mismo lema que retumbó en el pais durante la guerra sucia de los años setenta, como lo describe el diario norteamericano **'Vivos los queremos', es un retrato íntimo de la lucha y la herida abierta que viven los familiares de los estudiantes**; ellos, al igual que los familiares de los otros 27.000 desaparecidos en México en los últimos diez años.
