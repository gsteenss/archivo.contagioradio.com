Title: Manuel Villegas, firmante del Acuerdo de paz fue asesinado en Cauca
Date: 2020-12-27 19:44
Author: CtgAdm
Category: Actualidad, DDHH
Tags: #AcuerdoDePaz, #Cauca, #Excombatiente, #Farc, #Líder
Slug: manuel-villegas-firmante-del-acuerdo-de-paz-fue-asesinado-en-cauca
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/12/WhatsApp-Image-2020-12-27-at-2.57.57-PM.jpeg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/12/manuel-alfonso-villegas.png" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

El partido FARC denunció el asesinato de **Manuel Alfonso Villegas**, integrante del ETCR Monte Redondo, Cauca, quien según la denuncia. fue víctima de torturas e impacto de bala. Con él son 248 firmantes del Acuerdo de paz asesinados en Colombia.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El asesinato del hombre de 55 años, se habría **presentado sobre el medio día de este domingo en el municipio de Miranda**. Villegas fue integrante de la guerrilla del M-19 y fundador del entonces bloque Jaime Báteman. Posteriormente, se unión a la guerrilla de las FARC y se acogió a los Acuerdo de paz. (Le puede interesar: ["Excombatiente de FARC asesinado junto a su compañera en Buga"](https://archivo.contagioradio.com/excombatiente-de-farc-fue-asesinado-junto-a-su-companera-en-buga/))

<!-- /wp:paragraph -->

<!-- wp:heading -->

Con Manuel Villegas son 248 asesinatos de excombatientes
--------------------------------------------------------

<!-- /wp:heading -->

<!-- wp:paragraph -->

De acuerdo con el partido político FARC, con el asesinato de Villegas, "se completa la lamentable cifra de **248 sueños de paz apagados**". Situación que generó que miles de ellos a movilizarse a la capital el pasado 2 de noviembre, con la exigencias de garantías para su vida.

<!-- /wp:paragraph -->

<!-- wp:embed {"url":"https://twitter.com/PartidoFARC/status/1343290650549215235","type":"rich","providerNameSlug":"twitter","responsive":true,"className":""} -->

<figure class="wp-block-embed is-type-rich is-provider-twitter wp-block-embed-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/PartidoFARC/status/1343290650549215235

</div>

</figure>
<!-- /wp:embed -->

<!-- wp:paragraph -->

Recientemente Giovanni Álvarez, integrante de la Unidad de Investigación y Acusación de la JEP, denunció que de continuar esta tendencia, para el año **2024 habrán asesinado a 1600 ex combatientes.** Además, FARC ha denunciado que sobre estos hechos hay un 84% de impunidad referentes las investigaciones que adelanta la Fiscalía y los resultados en torno a los autores intelectuales de los asesinatos.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->
