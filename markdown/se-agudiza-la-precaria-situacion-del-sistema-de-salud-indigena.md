Title: Se agudiza la precaria situación del sistema de salud indígena
Date: 2016-04-27 12:45
Category: DDHH, Nacional
Tags: La Guajira, salud indígena, Wayuu
Slug: se-agudiza-la-precaria-situacion-del-sistema-de-salud-indigena
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/04/Salud-indígena.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Primicia Diario ] 

###### [27 Abril ]

A pesar de contar con la ley 691 de 2001 los pueblos indígenas en Colombia están siendo víctimas de la crisis en el sistema de salud, por partida doble. Tan grave es la situación que **desde el 2009 hasta este año han muerto 4770 niños y niñas indígenas por desnutrición** y de cada 100 mil niños nacidos hay 70 madres que fallecen por no contar con la atención en salud diferenciada y suficiente.

Según el portal actualidad étnica, las poblaciones indígenas con mayor número de muertes son las Wayuu en la Guajira, y las Nukak en el Guaviare, siendo este último un pueblo nómada que tiene cerca de **450 habitantes**, para quienes solamente funciona **un puesto de salud de 4 metros cuadrados** en los lugares en que se asientan. Otras de las comunidades afectadas están en la Sierra Nevada y el departamento del Chocó.

La ley 691 ordena que el sistema de atención a los pueblos indígenas debe ser diferencial y proteger sus condiciones culturales y sociales, además de brindar un subsidio de alimentación; sin embargo, estas dos condiciones se agregan al Plan Obligatorio de Salud POS, lo cual quiere decir que **son las mismas EPS las que deberían garantizar este derecho**.

Según Luis Évelis Andrade, Senador indígena, las EPS’s no garantizan el derecho a la salud de los pueblos indígenas, por varias razones. Una de ellas es que **la capacidad de dichas empresas es mucho menor a la demanda de las comunidades**, es decir, se acoge una población mínima que ni siquiera cubre los niveles de desnutrición de los niños y las niñas indígenas de La Guajira, departamento que cuenta con el **indice de empobrecimiento más alto a nivel nacional**, un 11%, según la Encuesta Nacional de Nutrición en Colombia de 2010.

En el 2014, el secretario de salud de La Guajira, Jorge Orozco, señaló que la mortalidad en madres es un problema de salud pública. Por ejemplo, **el 89,67%  de la mortalidad materna de indígenas corresponde al pueblo Wayuu**, un 89,7% de casos sucedieron en diferentes EPS como Caprecom, Asociación Indígena del Cauca MC y Barrios Unidos, lo que demuestra que la afiliación a las empresas no es garantía del derecho a la salud.

La prestación de servicios del sistema de salud indígena, estipulado en la Ley 691 de 2001, es inestable desde sus inicios. En 2009 se reportaron 2969 niños muertos por desnutrición en La Guajira. En 2012, el **90% de casos de mortalidad materna correspondieron a 29 madres indígenas** que murieron en nueve departamentos del país, incluida La Guajira.

Según Andrade, éste es el resultado de otros agravantes de la crisis como los **niveles de corrupción en instituciones, la fumigación por erradicación de cultivos ilícitos y el cambio climático**. El modelo de salud indígena debe ser manejado por las mismas comunidades, pues la estructura y administración actual de salubridad, no garantiza sus derechos, agrega.

La situación requiere soluciones estructurales, Andrade señala como ejemplo la apertura de pozos hídricos por corregimiento, como sucedió en Uribia; sin embargo, la población denunció que en Siapana **el pozo sólo funcionó el 23 de octubre de 2015, día en que fue inaugurado**.

###### Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)] 

<div class="yj6qo ajU">

<div id=":lx" class="ajR" style="text-align: justify;" tabindex="0" data-tooltip="Mostrar contenido acortado">

![](https://ssl.gstatic.com/ui/v1/icons/mail/images/cleardot.gif){.ajT}

</div>

</div>
