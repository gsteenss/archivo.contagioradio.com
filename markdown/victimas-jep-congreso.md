Title: "No quieren dejar que la justicia fluya" Víctimas al congreso tras modificaciones a la JEP
Date: 2017-11-16 17:00
Category: Uncategorized
Tags: acuerdo de paz, víctimas
Slug: victimas-jep-congreso
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/11/DOnqVVdWAAAlr5G-e1510770947262.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo Particular] 

###### [16 Nov 2017] 

Las víctimas del conflicto armado agrupadas en una veintena de organizaciones expresaron su rechazó frente a la inhabilidad a magistrados de la JEP que tengan trayectoria como defensores de DDHH impuesta por el Congreso de la República y **manifestaron que estas acciones buscan que continúe la impunidad en Colombia**.

Para Luz Marina Cuchumbé, madre de una de las víctimas de ejecución extrajudicial en el Cauca, esta decisión se suma a la impunidad que quiere ser perpetuada en el país **“para nosotros es duro, porque cada día las leyes de Colombia ponen sus trabas y no quieren dejar que la justicia fluya**” afirmó y agregó que uno de los mayores temores de las víctimas del país es no encontrar esa verdad que han buscado durante tanto tiempo.

Para Rene Guarín, hermano de María del Pilar Guarín, víctima de la retoma del Palacio de Justica, no es sorprendente que hayan inhabilitado a juristas con experiencia en derechos humanos “**yo no creo que de la JEP vayan a salir las grandes verdades, de casos particulares como lo ocurrido con el Palacio de Justicia,** no solo con los desaparecidos sino con la toma anunciada, los incendios, las ejecuciones extrajudiciales y así con muchas masacres que el Estado ha realizado”.

De igual forma, organizaciones defensoras de derechos humanos, en Antioquia, expresaron a través de un comunicado de prensa, su preocupación sobre las incorporaciones hechas a los requisitos para magistrados del Tribunal, debido a que “evidencia **una estrategia por garantizar la impunidad para los agentes estatales y terceros responsables** de graves violaciones a los derechos humanos y crímenes contra la humanidad”.

Agregaron que esa decisión representa un antecedente no solo de veto y persecución a los defensores, sino que también implica una negación del derecho de las víctimas a acceder a la justicia, teniendo en cuenta que en un país como Colombia la defensa de los derechos humanos es una labor que se ha convertido en **“blanco de amenazas, asesinatos y múltiples estigmatizaciones”**. (Le puede interesar:["Víctimas interponen tutela contra Congreso para implementar el Acuerdo de Paz"](https://archivo.contagioradio.com/victimas-y-organizaciones-interpusieron-tutela-para-que-congreso-implemente-acuerdo-de-paz/))

Finalmente, las organizaciones hicieron un llamado al Congreso a cumplir con lo pactado en los Acuerdos de paz, eliminar todas las barreras que impiden la participación activa de los ciudadanos en los escenarios de postverdad y a comprometerse con la búsqueda de la verdad, justicia y reparación.

###### Reciba toda la información de Contagio Radio en [[su correo]
