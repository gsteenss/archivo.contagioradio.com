Title: Represión de la Fuerza Pública a indígenas del Alto Baudó
Date: 2015-12-06 08:38
Author: AdminContagio
Category: Nacional, Resistencias
Tags: Alto Baudó, Chocó, comunidades indígenas, ESMAD, Fuerza Pública, Hostigamientos, Manifestaciones, Orden Público, Pié de Pató
Slug: represion-de-la-fuerza-publica-a-indigenas-del-alto-baudo
Status: published

###### Foto: vozenfugacalle 

###### [4 Dic 2015]

Mediante un comunicado, los habitantes de la cabecera municipal de Pie de Pató en el Alto Baudó, reportaron que fueron víctimas de instigamientos por parte de la Fuerza Pública mientras realizaban una movilización en la que llevaban 13 días exigiendo una explicación a las autoridades regionales sobre la destinación de los recursos al municipio.

La comunidad indígena del Pie de Pató señala que estas acciones, en las que además, las mujeres embarazadas que estaban en el lugar presentaron síntomas de aborto; se presentaron afecciones respiratorias en los niños y 8 indígenas resultaron heridos, van en contra de los Derechos Humanos y el Derecho Internacional Humanitario: "Rechazamos de manera categórica los actos infames cometidos en contra de la población y a su vez, responsabilizamos a la administración departamental, municipal y policial por lo ocurrido".

Dentro del texto se dice que "es de resaltar que las comunidades movilizadas no solo fueron despojadas del sitio de concentración, sino que también les fueron despojando sus pertenencias", entre los que se encontraban víveres, combustible, una planta eléctrica, una cámara fotográfica y de video. Además, en el informe se dio a conocer que la Policía revisó la mayoría de bolsos sin la presencia de sus dueños, por lo que la situación se prestó para que se perdieran algunos de los artículos en los equipajes de los manifestantes.

Los integrantes de los cabildos indígenas, consejos comunitarios, autoridades tradicionales y los habitantes de esta zona del país, hacen un llamado a los organismos de justicia y control para que hagan presencia en la cabecera municipal y solicitan a las organizaciones internacionales a que "se hagan los llamados a las administraciones para que sean atendidas las peticiones de las comunidades, de las organizaciones indígenas y afros a pronunciarse solidariamente, teniendo en cuenta que toda organización se debe a su base social y a la defensa de los derechos fundamentales".

El Alto Baudó se encuentra ubicada a 80 kilómetros del Chocó, posee una extensión total de 1532 kilómetros cuadrados y su población es en su mayoría de origen negro (73.33%) de la población, con los que conviven con una alta densidad de personas indígenas (26,6%), los cuales habitan en resguardos legalmente constituidos, según información de la página oficial del municipio.
