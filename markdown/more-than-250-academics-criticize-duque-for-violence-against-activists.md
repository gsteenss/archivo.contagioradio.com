Title: Academics around the world ask Duque for serious human rights policy
Date: 2019-05-26 13:48
Author: CtgAdm
Category: English
Slug: more-than-250-academics-criticize-duque-for-violence-against-activists
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/acuerdo-nacional-duque-1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archive] 

More than 250 academics from around the world sent an open letter to the Colombian President Iván Duque, in which they critiqued his administration's failure to detain an escalation in violence against human rights defenders and ex-combatants. According to a report by the Center for Research and Popular Education, at least 62 social leaders have been assassinated in 2019 so far.

"Given these facts, we are outraged at the Colombian government’s failure to acknowledge the situation and we ask the government to take steps to avoid this continuous and systematic bloodshed and to prevent a repetition of deplorable events, such as the attempt on the life of Francia Márquez and other leaders from the North of Cauca that occurred on the 4th May this year," reads the letter.

According to the document, this wave of violence is linked to territorial disputes among various sectors that seek to establish grand-scale extractivist projects. These academics claim that economic interests in these regions have caused an escalation in socio-environmental conflicts, in which legal and illegal armed groups work together to dispossess communities of their lands. (Related: "[Violence against activists increased in Colombia in 2018: report](https://archivo.contagioradio.com/violence-against-activists-increased-in-2018-according-to-cinep/)")

The letter invited the President to respond to these concerns with measures the government has taken to protect social leaders. It also concluded that there is a need for international entities such as Human Rights Watch and Amnesty International organize a verification commission to investigate the causes of these violent acts to create reliable information, prevent new cases and ensure justice for the victims of the previous cases.

Sir

Iván Duque Márquez

President of the Republic of Colombia,

Bogotá, Colombia

Open Letter

We are academics from Colombia and from all over the world and we wish to voice our concern over recent events in Colombia that include death threats, legal persecution and the assassination of social leaders, former guerrilla combatants and human and environmental rights defenders. According to the Centro de Investigación y Educación Popular, CINEP/Programa por la paz (Centre of Research and Popular Education, CINEP/Peace Program), of the cases categorized as political violence in 2018, there were 648 assassinations, 1,151 death threats, 304 people were injured, 48 people were victims of attacks, 22 forced disappearances, 3 sexual attacks and 243 arbitrary detentions. In 2019, at least 62 social leaders have been killed so far.

Given these facts, we are outraged at the Colombian government’s failure to acknowledge the situation and we ask the government to take steps to avoid this continuous and systematic bloodshed and to prevent a repetition of deplorable events, such as the attempt on the life of Francia Márquez and other leaders from the North of Cauca that occurred on the 4th May this year.

We have observed that hate and violence are encouraged from within places of power and the media and that this disrupts not only the little peace that has been achieved, but as noted by Daniel Pécaut (2001), is a declaration of war against society.

As academics that conduct research on local, regional and international dynamics, we have seen how territories of geopolitical interest have been put in the spotlight and as a result Página 3 de 53 Carta Abierta al Presidente Iván Duque Márquez de Académicos del mundo y de Colombia have seen an escalation of conflicts linked to the expansion of the extractive industry. At the same time, we have noted that nefarious links have developed between legal and illegal forces in order to expel the local population from their territories. These types of relations have also been evidenced by Sassen (2015), Harvey (2004), Escobar (2014), and the analyst of defense, Herold (2007), among others, who have written on expulsion and dispossession as a means to make way for the large-scale accumulation of extractive projects.

In Colombia, a similar situation has been noted in relation to the country’s economic policy, a policy that promotes extractivism as a core strategy for development. This favorable policy climate is used by different sectors holding power, and which represent diverse interests, to take control of territories. As a result, there has been an escalation of assassinations against leaders who are defending the rights of local communities and peoples. Although this has been a reality for a long time, there has been an increase in cases since the signing of the agreements with the FARC – EP in 2016, in clear opposition to the hoped for ‘territorial peace’.

We can conclude that these threats and assassinations are linked to various sectors who have a specific interest in regions in the country where there is a proposal to develop large-scale extractive projects. This also coincides with accounts given in ‘free version hearings’ at transitional justice processes and in decisions made by the Colombian Constitutional Court.

Recurring phrases in death threats like ‘finish off anyone who interferes with the development of the country’ identifies the local population as a military target, as this population, as voiced by their leaders, opposes extractive projects and wishes to avoid the negative impacts they have on ecosystems and populations.

We have also noted that State bodies and the press have failed to take measures to prevent the threats, legal persecution and assassinations taking place and yet at the same time do not hesitate to signal and stigmatize social protest, the activities of social leaders and opposition to government policies.

It is also worrying that it was only when an attempt was made on the life of Francia Márquez, a leader known internationally as winner of the Goldman Environmental prize, who was with other well known leaders at the time, that you chose to make a public pronouncement. Your government has failed to communicate what efforts are being taken to respond to this crisis. We are concerned that the measures taken to date have been insufficient and appear to be limited to weak security plans for social leaders at risk and a hunt for the material authors of these crimes. However, in order to identify the intellectual authors and the sectors behind this strategy of dispossession and extermination, it is imperative to understand the wider context.

As academics we ask that as head of state you order an investigation in order to uncover what is really behind the despicable acts of violence taking place on a daily basis. Página 4 de 53 Carta Abierta al Presidente Iván Duque Márquez de Académicos del mundo y de Colombia.

At the same time, given the lack of action by your government and the size of the problem, we feel it is urgent and necessary to invite international organizations like Human Rights Watch and Amnesty International to establish a Commission of Verification to investigate the causes of these violent acts so that we can have reliable information, prevent new cases occurring and ensure justice for the victims of the previous cases.

We hope that you recognize that this is an historic moment for Colombia and that it is possible to change the course of the national economy and of social policy to ensure that life and the environment are protected, resulting in a better life, a ‘buen vivir’, for future generations.

We invite you to share with us the measures taken and decisions made to date, to avoid more blood shed in Colombia and ask what new actions will be taken to resolve this painful and intolerable humanitarian situation.

We the undersigned continue to work towards world peace, towards territorial peace, towards a peace that is longed for in every corner of the earth and here, in Colombia, that has suffered so much.

cc: Most Holy Father Papa Francisco, Human Rights Watch and Amnesty International

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
