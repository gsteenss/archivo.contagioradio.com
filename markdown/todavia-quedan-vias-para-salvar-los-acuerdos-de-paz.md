Title: "Todavía quedan vías para salvar los acuerdos de paz"
Date: 2016-10-02 19:12
Category: Nacional, Paz
Tags: acuerdo farc y gobierno, Conversaciones de paz con las FARC, Diálogos de paz Colombia
Slug: todavia-quedan-vias-para-salvar-los-acuerdos-de-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/03/Caminata-por-la-paz-60.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio ] 

###### [2 Oct 2016 ] 

Una abstención superior al 62.58% evidencia la escasa legitimidad del NO que ganó en este plebiscito con un 50.21% de los votos, frente al SÍ que fue apoyado con el 49.78% de los sufragios, y sí bien no se discutía la continuación o finalización de la guerra sino el cómo negociar con una guerrilla de la magnitud de las FARC-EP, es alta la incertidumbre de diversos sectores sociales en relación con lo que sucederá de aquí en adelante, no sólo con esta negociación sino con la que [[se avecina con el ELN](https://archivo.contagioradio.com/ad-portas-de-la-fase-publica-de-conversaciones-eln-gobierno/)].

Por un lado, hay quienes aseguran que la victoria del NO es la cuenta de cobro que la ciudadanía colombiana le está pasando al Gobierno Santos por no lograr coherencia entre su postura en la negociación y las [[políticas sociales y económicas](https://archivo.contagioradio.com/debate-en-el-eclectico-la-ley-zidres/)] que al mismo tiempo fue aprobando en el país, y desde sectores como el de las víctimas hay otros que manifiestan que está ganancia habla de la brecha que persiste entre la urbanidad y la ruralidad, y la percepción de la crueldad del conflicto y la necesidad de negociar.

Los resultados demuestran entonces que persiste la disputa entre el encarcelar o no a los miembros de las FARC-EP y obligarlos a que dejen las armas, más allá de la discusión del modelo económico que subyace lo pactado en La Habana, situación que lleva a diversos sectores a plantear la consolidación de una asamblea nacional constituyente como la mejor solución para este nuevo momento en el que entra el país, a fin de lograr un pacto político entre el presidente Juan Manuel Santos, el senador Álvaro Uribe y Timochenko que logre que los [[resultados exitosos](https://archivo.contagioradio.com/se-reanuda-desminado-humanitario-en-el-orejon/)] de estos cuatro años de negociaciones no sean desechados.

Lo importante ahora es salvar los acuerdos pactados, principalmente el [[cese bilateral](https://archivo.contagioradio.com/arranca-la-verificacion-tripartita-al-cese-bilateral-de-fuego-en-colombia/)], aseveran políticos, defensores de los derechos humanos y activistas, pese a las contradicciones del Gobierno Santos en materia tributaria, política minero-energética, diálogo con la ciudadanía, y particularmente con las víctimas, el titubeo del Vaticano con la [[Jurisdicción Especial para la Paz](https://archivo.contagioradio.com/entrevista-especial-justicia-transicional-puede-estar-lista-pero-necesita-espacio-politico-ictj/)] y el mal manejo de la relación con la iglesias, porque ante todo los resultados demuestran que no se discutieron los valores de la vida biológica y la muerte violenta; de la guerra y la paz... todo quedo entre Uribistas y Santistas, a quienes se debe convocar a una asamblea nacional constituyente.

###### [Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por[ [Contagio Radio](http://bit.ly/1ICYhVU)]]
