Title: Firmas encuestadoras y medios masivos serán investigados por el CNE
Date: 2015-10-08 17:54
Category: Nacional, Política
Tags: Blu Radio, Caracol TV, Consejo Nacional Electora, diario La República de Bogotá, El Colombiano de Medellín, El Espectador, El País de Cali, El Tiempo, Elecciones 2015, firmas encuestadoras, Vanguardia Liberal de Bucaramanga
Slug: firmas-encuestadoras-y-medios-masivos-seran-investigados-por-el-cne
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/10/cms-image-000036926.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Confidencial Colombia 

Debido a las persistentes quejas de la ciudadanía y los actuales candidatos y candidatas para las elecciones regionales de este año, el Consejo Nacional Electoral anunció que iniciará una investigación contra diferentes medios de comunicación y firmas encuestadoras.

El Centro Nacional de Consultoría, Gallup Colombia, Cifras y Conceptos, Analizar y Asociados Mercadeo y Opinión, Ipsos Napoleón Franco, Datexco, Centro Regional de Consultoría Urabá, Centro Regional de Consultoría Proyección e Imagen, Colombia Global Mining y Business, Corporación Para la Investigación y el Fomento Empresarial (Cefin), Fredy Armando Revelo, MCW SAS, Mediciones y Medios, Progestión Internacional y Yamil Cura, son las firmas encuestadoras a las que el CNE les abre investigación por diversas irregularidades.

Por ejemplo, según el informe que presenta el CNE, la firma Datexco realizó una encuesta pagada por El Tiempo y La W en la que según la CNE hubo "error en la definición del marco muestral, indica varias ciudades y solo se aplicó en una", así mismo, sucedió con otra encuesta publicada por la misma firma donde se asegura que hubo "diferente presentación de la del CD enviado y la publicada, marco muestral indica varias ciudades y solo se hizo en Barranquilla".

Según  la CNE, las irregularidades encontradas son: "la publicación incompleta de la ficha técnica, distorsión de la información publicada puesto que se detecta carencia de información relevante para dar un análisis objetivo frente a los resultados que producen dichos estudios y Encuestas presentas por firmas no inscritas ante el Registro Nacional de Encuestadores del Consejo Nacional Electoral, las cuales carecen de inspección en cuanto a la rigurosidad de su práctica".

Así mismo, de acuerdo al comunicado de prensa del CNE, medios como "El Tiempo, El País de Cali, diario La República de Bogotá, Vanguardia Liberal de Bucaramanga, El Colombiano de Medellín, entre otros", serán investigados. Sin embargo, cabe recordar que otro medios masivos, como Semana, RCN televisión, RCN la Radio, Blu Radio, El Espectador, Caracol Televisión han pagado encuestas a firmas como Ipsos Napoleón Franco, Datexco, Cifras y Conceptos, y Gallup.

Las sanciones que el Consejo Nacional Electoral interpondrá van desde los \$16.108.750 hasta los \$25.774.000, o con la suspensión o prohibición del ejercicio de estas actividades, a quienes realicen y divulguen encuestas o sondeos  de carácter político o electoral, sin cumplir con los requisitos necesarios.

[Cuadro Observaciones Firmas Encuestadoras](https://es.scribd.com/doc/284112003/Cuadro-Observaciones-Firmas-Encuestadoras "View Cuadro Observaciones Firmas Encuestadoras on Scribd") by [Contagioradio](https://www.scribd.com/contagioradio "View Contagioradio's profile on Scribd")

<iframe id="doc_55945" class="scribd_iframe_embed" src="https://www.scribd.com/embeds/284112003/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-KXpCUbFijBzoKJYJ2lYK&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="1.2941176470588236"></iframe>
