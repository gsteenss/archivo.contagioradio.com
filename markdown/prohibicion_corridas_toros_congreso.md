Title: En manos del Congreso quedó la prohibición de las corridas de toros
Date: 2018-02-08 17:58
Category: Animales, Nacional
Tags: Consulta Antitaurina, corridas de toros, Corte Constitucional
Slug: prohibicion_corridas_toros_congreso
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/02/2717e9ed-0958-46ab-ab6a-35e8434916e8-e1518130389700.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Plataforma ALTO] 

###### [8 Feb 2018] 

La decisión frente a la permanencia de las corridas de toros tanto en Bogotá como a nivel nacional, ha quedado en manos del Congreso de la República. Así lo ha decidido un reciente fallo de la Corte Constitucional, en el que el alto tribunal ha dicho que es el Congreso al que le recae la responsabilidad de legislar para que se desestimule dicha actividad.

["Recibimos con mucho dolor el fallo de la Corte Constitucional. **A la ciudadanía se le está vulnerando el derecho fundamental a participar y decidir sobre la clase de cultura que queremos en Bogotá,** y se está diciendo a la gente que los mecanismo de participación ciudadana no sirven debido a los poderes que manipulan las instituciones", expresa Catalina Reyes, integrante de la Plataforma ALTO, la organización que impulsó la consulta.]

Reyes asegura que no es un secreto que desde hace mucho tiempo se ha conocido que el presidente de la Corporación Taurina de Bogotá, Felipe Negrete, y otros líderes taurinos poderosos, han estado en la Corte Constitucional y en el Congreso haciendo lobby a favor de la permanencia de la tauromaquia. "Los taurinos han recurrido a diferentes artilugios para dilatar algo que desde hace mucho tiempo la ciudadanía ya viene exigiendo y es la abolición de las corridas de toros", dice la animalista.

### **La cuestión queda en el voto del próximo 11 de marzo** 

El debate sobre la protección animal y puntualmente, las corridas de toros ha tomado fuerza en los últimos años, a tal punto, de que en el congreso actualmente existe una bancada animalista, compuesta por partidos como el Liberal, el Verde, Polo Democrático y Cambio Radical, desde el cual se ha impulsado debates a favor de los animales.

A la fecha son dos los proyectos de Ley en curso con los cuales se busca acabar con dicha práctica. Se trata del proyecto de Ley para** elimi**[**nar las expresiones “rejoneo, corridas de toros, novilladas, becerradas y tientas” **contenidas en el artículo 7º de la Ley 84 de 1989 y deroga la Ley 916 de 2004 “Reglamento Nacional Taurino”. Una iniciativa de]{.text_exposed_show}l exministro del Interior, Juan Fernando Cristo y el senador Guillermo García Realpe, del Partido Liberal.

Cabe recordar, que por ejemplo, en la Comisión Séptima de Cámara de Representantes, cuando dicha iniciativa tuvo una votación unánime en el primer debate, la bancada del** Centro Democrático no asistió a votar** el proyecto.

De hecho, es de resaltar que el año pasado, desde el partido del Centro Democrático la** representante a la Cámara Margarita María Restrepo y la senadora Paola Holguín,** radicaron un proyecto de Ley por medio de la cual se expedía el reglamento nacional taurino, que entre otras cosas, reforzaba las corridas de toros obligando al gobierno a proteger la actividad taurina y a declararla patrimonio del país.

Por otro lado, se encuentra el proyecto 'Infancia sin Violencia' con el cual se busca asegurar la aplicación de la Convención Internacional sobre los Derechos del Niño, por lo cual se busca alejar **a la infancia de "la violencia de la tauromaquia", como lo ha asegurado la ONU. Un proyecto del que también es autor el senador García Realpe.**

"En las manos del Congreso de la República está la prohibición de las corridas de toros, por eso la importancia de las elecciones de marzo", explica Catalina Reyes, quien asegura que si bien casi todas las tendencias políticas se han manifestado en contra del maltrato animal, es importante reconocer quienes son los congresistas y partidos políticos que realmente han apoyado acciones y leyes a favor de los animales.

<iframe id="audio_23658491" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_23658491_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)
