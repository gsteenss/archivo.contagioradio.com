Title: Así va el cumplimiento de los acuerdos del Gobierno con Buenaventura
Date: 2017-07-10 13:19
Category: DDHH, Nacional
Tags: Movimiento social, Paro de Buenaventura
Slug: asi-va-el-cumplimiento-del-acuerdo-del-gobierno-con-buenaventura
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/05/paro-civico-marcha-buenaventura-perdidas-actos-vandalicos-22-05-2017-e1496014083254.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo Particular] 

###### [10 Jul 2017]

A un mes de que se hayan firmado los acuerdos del paro de Buenaventura, continúan los **ciudadanos trabajando en las mesas temáticas y en el proyecto legislativo para la creación del fondo autónomo, que permita dar cumplimiento a los puntos pactados con el gobierno**. El proyecto de ley sería presentado a la comunidad el próximo 14 de julio, fecha de fundación de Buenaventura y radicado en el Congreso el 20 de julio.

De acuerdo con Miyela Riascos, la última reunión entre el gobierno y el equipo dinamizador del paro, fue el 7 de julio. Allí ambas partes expusieron sus planes para la caracterizar el fondo autónomo, sin embargo, los **integrantes del equipo dinamizador del paro de Buenaventura manifestaron que la propuesta del gobierno se aleja demasiado de las necesidades de la comunidad**.

“**No coincidimos en mucho, la propuesta del gobierno está muy lejos sobretodo en temas como las fuentes de financiación y la estructura que debe tener el fondo**” afirmó Miyela y agregó que el próximo 12 de julio tendrán una nueva reunión con los delegados del gobierno para continuar en la construcción del proyecto de ley. (Le puede interesar: ["Este es el acuerdo entre el comité cívico de Buenaventura y el Gobierno"](https://archivo.contagioradio.com/acuerdan-plan-de-desarrollo-social-a-10-anos-en-buenaventura/))

Sobre el cumplimiento por parte del Gobierno, Riascos señaló “es una preocupación que todo el esfuerzo no sea un barril sin fondo, lo que estamos proponiendo para el proyecto de ley son varias fuentes de financiación que deben ser administradas por una fiduciaria, mientras que nosotros **nos convertimos en una veeduría ciudadana para evitar que se roben los fondos**”.

Referente a las posibilidades de que para las próximas elecciones que hay en Buenventura de Alcaldía, se lanzará uno de los integrantes del comité promotor del paro, Miyela señaló que por el momento esa intensión no se ha hecho evidente, sin embargo, aseveró que no descartan la iniciativa. (Le puede interesar:["Persiste arremetida del ESMAD contra población de Buenaventura"](https://archivo.contagioradio.com/persiste-arremetida-del-esmad-contra-poblacion-de-buenaventura/))

### **Acuerdos de Buenaventura** 

**Metas propuestas entre gobierno y población**

-   En materia de salud, las partes acordaron garantizar a los ciudadanos del distrito la accesibilidad, integralidad, suficiencia y calidad en la atención de los servicios de baja, mediana y alta complejidad.
-   En cuanto al tema de agua potable y servicios de saneamiento básico, garantizaron el 100% de la cobertura en todo Buenaventura.
-   El mismo acuerdo del 100% se hizo para garantizar la calidad, la cobertura y la pertinencia de la educación.
-   El acuerdo en actividades económicas ancestrales se hizo para recuperar la pesca, el aprovechamiento forestal sostenible, la manufactura y artesanía, la minería artesanal, el comercio local, la producción agropecuaria, la creación de un clúster portuario y de transporte y otras actividades productivas que fortalezcan la autonomía alimentaria y el empleo.
-   Como meta de vivienda, se acordó superar el déficit de vivienda de las comunidades rurales y urbanas en un plazo de 10 años.

### **EL FONDO AUTÓNOMO**

El fondo autónomo que se presentó tendrá como **primer financiador al gobierno nacional quien utilizará el 50% del impuesto a la renta para el fondo.** Seguidamente el gobierno nacional creará un crédito externo de 76 millones de dólares para construir la ciudadela hospitalaria, la construcción de la primera fase del sistema de alcantarillado, los acueductos rurales, la Unidad de Cuidados Intensivos del Hospital Luis Ablanque de la Plata, el tecnoparque industrial pesquero y agrícola y el muelle de cabotaje.

<iframe id="audio_19719564" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_19719564_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU).
