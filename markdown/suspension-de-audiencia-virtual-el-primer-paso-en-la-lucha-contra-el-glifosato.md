Title: Suspensión de audiencia virtual un paso en la lucha contra el glifosato
Date: 2020-05-19 11:15
Author: CtgAdm
Category: Actualidad, Ambiente
Slug: suspension-de-audiencia-virtual-el-primer-paso-en-la-lucha-contra-el-glifosato
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/05/Jenifer-Mojica-_mezcla.mp3" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/05/200515_Tutela-final-contra-APA-aspersiones-glifosato.pdf" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

Ante una tutela interpuesta por la Asociación de Cacaocultores del municipio de Policarpa, Nariño, una **jueza ordenó la suspensión de audiencia virtual que definía términos para la retoma de aspersión con [glifosato](https://archivo.contagioradio.com/aqui-expulsan-al-campesinado-pero-cobijan-al-sector-minero-y-petrolero/)en Colombia.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

La orden fue interpuesta por el Juzgado Primero de Familia del circuito de Pasto, luego de que el pasado 14 de mayo la asociación campesina demandara a la Autoridad Nacional de Licencias Ambientales (ANLA) por convocar a una audiencia virtual en la que se manejarían temas claves en el retorno del las glifosato sin tener en cuenta a los principales afectado.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

La audiencia, que estaba agendada para el 27 de mayo, **pretendía discutir el plan de manejo ambiental del Programa de Erradicación de Cultivos Ilícitos mediante la aspersión aérea con el glifosato (PECIG)**, una decisión respaldada por la Policía Antinarcóticos.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Entidad que ante las posibles respuestas que podría traer esta decisión en las comunidades, **puso al servicio de las comunidades mecanismos comunicativos como la emisora, pretendiendo que iban a ser suficientes** para promover y garantizar la participación de las poblaciones rurales, principales afectados por las posibles aspersiones.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

**Jennifer Mojica abogada especialista en derecho procesal e integrante de la Comisión Nacional de Territorios Indígenas**, organización que junto a otras 88 presentaron este lunes una acción de tutela con el mismo objetivo, ordenar a la ANLA suspender la realización de esta audiencia; señaló qué, "*este es sólo el primer paso para que el Gobierno entienda la ineficiencia del uso del glifosato*".

<!-- /wp:paragraph -->

<!-- wp:file {"id":84436,"href":"https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/05/200515_Tutela-final-contra-APA-aspersiones-glifosato.pdf"} -->

<div class="wp-block-file">

[*Conozca la tutela completa aquí:*  
200515\_Tutela-final-contra-APA-aspersiones-glifosato](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/05/200515_Tutela-final-contra-APA-aspersiones-glifosato.pdf)[Descarga](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/05/200515_Tutela-final-contra-APA-aspersiones-glifosato.pdf){.wp-block-file__button}

</div>

<!-- /wp:file -->

<!-- wp:heading {"level":3} -->

### ¿Que hay de fondo tras la audiencia virtual?

<!-- /wp:heading -->

<!-- wp:paragraph -->

Según la abogada detrás la medida, está toda la historia que tiene Colombia frente a la erradicación con glifosato, *"una historia que desde los años 70, ha registrado el daño al ambiental y a la vida de las personas que son fumigadas"*.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Asimismo agrega que desde el año 2015, el entonces Ministerio de Salud *"demostró con pruebas científicas los efectos dañinos del glifosato en la salud y la vida de las personas como, cáncer , malformaciones y enfermedades respiratorias crónicas de las generaciones actuales y futuras".*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Fue en ese años que la Corte Constitucional decidió suspender la fumigación aérea con glifosato, aceptando la clara evidencia del Ministerio ante los efectos del uso de este pesticida.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Mojica añade, que posterior a ello la Corte ha venido presentando diferentes argumentos bajo los cuales se puede reactivar esta actividad, ***"condiciones como una licenciamiento ambiental riguroso y respaldado por pruebas científicas, un proceso que debe ser claramente participativo y dado mediante las audiencias públicas"***

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Asimismo otro de estos requisitos para la activación es la ejecución de **un plan para la lucha contra las drogas que vaya de la mano en lo planteado en el proceso de paz de la Habana**, el cual prioriza la sustitución voluntaria y a las comunidades cultivadores de coca marihuana y amapola, ***"todo esto está de fondo pero nada se ha cumplido a la fecha".***

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### "Un paño de agua tibia al proceso de sustitución voluntaria"

<!-- /wp:heading -->

<!-- wp:paragraph -->

Para la abogada lo presentado en los últimos días simplemente señala que se suspende la audiencia de mayo, pero **esto no representa aún una respuesta final sobre lo que se está tutelando**.

<!-- /wp:paragraph -->

<!-- wp:quote -->

> *"La ANLA supone que estos procesos de participación virtual se pueden hacer a partir de mecanismos electrónicos, pero esto no es cierto, en la zona rural colombiana la cobertura de internet es muy baja, sumado a que más de la mitad de la población del país no tiene acceso a un compuador"*

<!-- /wp:quote -->

<!-- wp:paragraph -->

Destacando que por ahora, no hay indicios de que el Gobierno desea evaluar su programa de lucha contra los [cultivos de uso ilícito](https://www.justiciaypazcolombia.com/continuan-labores-de-erradicacion-forzada-en-la-zrcpa-sin-medidas-de-proteccion-para-covid19/), a pesar de ser denominado como fallido, la abogada señala que el paso a seguir es esperar a que haya una respuesta a las diferentes tutelas que han sido presentadas por parte de las comunidades.

<!-- /wp:paragraph -->

<!-- wp:quote -->

> *"Lo que sigue es que los jueces tengan que fallar sobre estas peticiones que hemos hecho, seguramente llegará a la Corte Constitucional, **sabemos que esta es una lucha jurídica muy larga pero la seguiremos dando en protección de las comunidades"***

<!-- /wp:quote -->

<!-- wp:audio {"id":84429} -->

<figure class="wp-block-audio">
<audio controls src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/05/Jenifer-Mojica-_mezcla.mp3">
</audio>
  

<figcaption>
*Escuche aquí al entrevista completa con Jennifer Mojica. Abogada especializada en derecho procesal. Integrante de la Comisión Nacional de Territorios Indígenas STI-CNTI*

</figcaption>
</figure>
<!-- /wp:audio -->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->
