Title: Más del 30% de asesinatos a Defensores en el mundo se registraron en Colombia
Date: 2018-01-05 11:21
Category: DDHH, Nacional
Tags: colombia, defensores de derechos humanos, lideres sociales
Slug: mas-del-30-de-asesinatos-a-defensores-en-el-continente-se-registraron-en-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/11/WhatsApp-Image-2017-11-24-at-12.31.24-PM.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo Partícular] 

###### [01 Ene 2017]

El más reciente informe de la organización Front Line Difenders, reportó que el 30% de los asesinatos contra defensores de derechos humanos en el mundo en el 2017, fueron cometidos en Colombia, además manifestó que en el 84% de los casos las víctimas, que se registraron en los países en donde hacen seguimiento, habían recibido al menos una amenaza previa, que demuestra **“la responsabilidad estatal por omisión en los crímenes de defensores de derechos humanos”** en donde Colombia no es la excepción.

En América se registraron un total de 312 asesinatos a defensores de derechos humanos, de los cuales más de 90 casos corresponden a Colombia, de acuerdo con la información recopilada por Front Line Difenders. (Le puede interesar: ["Combates entre el ELN y AGC provocan desplazamiento de 12 familias en Jiguamiando, Chocó"](https://archivo.contagioradio.com/combates_eln_agc_desplazamiento_jiguamiando_choco/))

De igual forma señala que con la firma de los Acuerdos de Paz entre el gobierno y el nuevo partido La FARC, se esperaba que hubiese una reducción de los asesinatos y agresiones a líderes sociales, sin embargo, la violencia hacia los mismos ha aumentado desde la misma firma de los acuerdos.

### **¿Quiénes son los líderes y defensores asesinados?** 

De acuerdo con el informe, las personas asesinadas en Colombia se caracterizaban por pertenecer a territorios locales y desempeñar allí actividades en defensa de la comunidad, así mismo señalan que la mayoría de ellos murieron **“en manos de paramilitares o estructuras armadas no identificadas”**, que están confrontándose en los diferentes departamentos del país para ocupar y controlar los territorios en donde antes se encontraba La FARC

En ese mismo sentido, el informe expresa que la mayor causa de asesinatos en el continente es producto de la defensa del territorio y de la falta de reformas rurales en los países que regulen y ataquen la problemática del acaparamiento de tierras. (Le puede interesar: [Los empresarios estarían detrás del asesinato de líderes sociales del Bajo Atrato y Urabá)](https://archivo.contagioradio.com/hasta-cuando-y-cuantos-mas-continuan-la-violencia-contra-lideres-sociales-del-bajo-atrato-y-uraba/)

Finalmente, referente a los casos de agresiones a defensores de derechos humanos en América, el informe manifiesta una gran preocupación sobre las modalidades de violencias físicas y sicológicas que se usan para agredir, como lo es atenta contra la vida de las personas más cercanas al líder o defensor, para atemorizarlo, así lo demostró el ataque a una de las empleadas de Marylen Serna, que fue violada por tres hombre en Popayán, Colombia.

###### Reciba toda la información de Contagio Radio en [[su correo]
