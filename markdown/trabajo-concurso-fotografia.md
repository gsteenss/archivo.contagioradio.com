Title: Vuelve el concurso de fotografía que destaca el trabajo en Latinoamérica
Date: 2017-08-08 12:15
Category: Cultura
Tags: Documental, Fotografia, trabajo
Slug: trabajo-concurso-fotografia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/08/trabajo.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Wilfredo Limachi Mamani 

###### 04 Ago 2017 

El valor de los trabajadores y trabajadoras de latinoamérica busca ser reconocido en el **Concurso de Fotografía Documental "Los trabajos y los días"**, un evento organizado por la Escuela Nacional Sindical-ENS de Colombia, que para su edición número 23 **abre convocatorias en 5 categorías**.

Durante más de dos décadas de trabajo, el concurso ha alcanzado gran reconocimiento entre la comunidad de fotógrafos del hemisferio por **su enfoque en el tema del mundo laboral y la importancia que tiene la fotografía documenta**l sobre el trabajo como una forma de expresión artística y de investigación sociológica.

Las categorías del concurso son: **Niñez Trabajadora, Mujer Trabajadora, Hombres Trabajadores, Trabajadores Migrantes y Conflictos Laborales**. Los interesados en participar deben acompañar sus imágenes con un texto que ayude a contextualizar su producción fotográfica y **acerque a los espectadores las viejas y nuevas realidades laborales en el continente**.

La selección de las obras estará a cargo de un jurado exigente que filtrará el material, un duro trabajo si se tiene en cuenta que en promedio se reciben **4 mil imágenes provenientes de 30 países**. Las escogidas harán parte de una muestra que será exhibida virtualmente en el portal **concurso.ens.org.co**  y en físico de manera itinerante por diversas ciudades.

Sobre la muestra seleccionada, un jurado de premiación de nivel internacional, compuesto por tres personalidades del medio fotográfico y documental, se encargará de seleccionar la obra ganadora en cada categoría, **las cuales recibirán un premio de US 1.100**.

"Este concurso es un espacio de los fotógrafos, pues son ellos los que cada año nos sorprenden mostrando nuevas facetas de un mundo aparentemente rutinario, donde unos oficios desaparecen y otros nacen, **donde las relaciones laborales se reconfiguran al vaivén de los cambios económicos y de las luchas sociales**" asegura Jairo Ruiz director del concurso.

Los interesados e interesadas en participar pueden hacerlo **hasta el 17 de agosto** accediendo al [enlace ](http://concurso.ens.org.co/participa/inscribete/), donde podrán enviar máximo **5 fotografías** de su autoría, en formato JPG, de **máximo 800 pixeles** por su lado más ancho y **no debe exceder 1000 kilobytes (KB)**. El nombre del archivo debe tener el título de la obra y una numeración en el caso de ser serie.
