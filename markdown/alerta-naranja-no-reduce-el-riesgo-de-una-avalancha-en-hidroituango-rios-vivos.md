Title: Alerta naranja no reduce riesgo de una avalancha en Hidroituango: Ríos vivos
Date: 2018-05-23 12:59
Category: Ambiente, Nacional
Tags: comunidades afectadas por hidroituango, emergencia hidroituango, EPM, Hidroituango
Slug: alerta-naranja-no-reduce-el-riesgo-de-una-avalancha-en-hidroituango-rios-vivos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/05/DdFZ9MIX4AYEomP-e1526402570611.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Movimiento Ríos Vivos] 

###### [23 May 2018] 

Luego de que las autoridades en Antioquia decretaran la disminución de alerta roja a alerta naranja en algunos municipios afectados por Hidroituango, las comunidades indicaron que **no les han dado certeza** de lo que está pasando con la presa por lo que consideran irresponsable el cambio en las alertas.

De acuerdo con Isabel Cristina Zuleta, integrante del Movimiento Ríos Vivos, el cambio de alerta “es un manejo politiquero, porque se acercan las elecciones entonces las alertas cambian”. Afirmó que **no creen que el cambio en la alerta sea real** teniendo en cuenta que “se hizo para tranquilizar a la población pero que no se deriva de una situación técnica y concreta”.

### **El riesgo de una avalancha continúa** 

Teniendo en cuenta las afirmaciones de Empresas Públicas de Medellín, quien afirmó que están cerca de completar **la cota de 410 metros** para mitigar la emergencia de una posible avalancha, el Movimiento Ríos Vivos recordó que “la cota no es una garantía”. Por el contrario, “es un gran riesgo para las comunidades aguas abajo”.

Hidroituango continúa siendo un riesgo en la medida en que su construcción ha afectado la **estabilidad de las montañas** que fueron intervenidas por la empresa. En repetidas ocasiones, Zuleta ha afirmado que existe una debilidad y las montañas se están desmoronando lo que implica un riesgo permanente. (Le puede interesar:["Comunidades afectadas por Hidroituango denuncian "dictadura" de EPM"](https://archivo.contagioradio.com/comunidades-afectadas-por-hidroituango-denuncian-una-dictadura-de-epm/))

Por esto, han cuestionado las afirmaciones de EPM que indican que el muro se encuentra firme. Desde Ríos Vivos, es **“absurdo”** que no se tenga en cuenta las condiciones de las montañas “que son las que sostienen al muro” afirmó Zuleta. Por esto, “no entendemos qué es lo que están diciendo con alegría de haber terminado el muro para que el agua pueda salir por el vertedero, cuando el agua tendría que correr su curso”.

### **Comunidades se mantendrán en los albergues en Puerto Valdivia** 

En cuanto a la situación que viven las personas en los albergues de municipios como Puerto Valdivia, Zuleta enfatizó en que **se ha mejorado la atención humanitaria** debido a las denuncias que se han hechos desde los movimientos sociales. Sin embrago, denunció que las personas continúan recibiendo alimentos en mal estado.

Finalmente, las comunidades realizaron un **plantón frente al puesto de mando unificado** donde exigieron nuevamente que se libere el río Cauca desmontando Hidroituango teniendo en cuenta que la amenaza de una posible avalancha no disminuye llegando a la cota que indica EPM.

<iframe id="audio_26140000" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_26140000_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]

<div class="osd-sms-wrapper">

</div>
