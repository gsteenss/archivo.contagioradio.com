Title: GALERIA POST
Date: 2019-03-25 16:03
Author: CtgAdm
Slug: galeria-post
Status: published

FOTOGRAFÍA
==========

[![En fotos | Las nuevas vidas en la Zona Veredal de Buenavista en Mesetas, Meta](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/02/IMG_1931-770x260.jpg "En fotos | Las nuevas vidas en la Zona Veredal de Buenavista en Mesetas, Meta"){width="770" height="260"}](https://archivo.contagioradio.com/en-fotos-la-vida-en-la-zona-veredal-en-buena-vista-mesetas/)  

###### [En fotos | Las nuevas vidas en la Zona Veredal de Buenavista en Mesetas, Meta](https://archivo.contagioradio.com/en-fotos-la-vida-en-la-zona-veredal-en-buena-vista-mesetas/)

[<time datetime="2017-02-17T18:02:11+00:00" title="2017-02-17T18:02:11+00:00">febrero 17, 2017</time>](https://archivo.contagioradio.com/2017/02/17/)Los lugares de guerra han quedado atrás para las y los integrantes de las FARC que llegaron a la Zona Veredal Transitoria de Buenavista en Mesetas[LEER MÁS](https://archivo.contagioradio.com/en-fotos-la-vida-en-la-zona-veredal-en-buena-vista-mesetas/)  
[![Las FARC-EP caminan hacia la paz](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/02/putumayo-8-770x260.jpg "Las FARC-EP caminan hacia la paz"){width="770" height="260"}](https://archivo.contagioradio.com/las-farc-ep-caminan-hacia-la-paz/)  

###### [Las FARC-EP caminan hacia la paz](https://archivo.contagioradio.com/las-farc-ep-caminan-hacia-la-paz/)

[<time datetime="2017-02-02T17:05:46+00:00" title="2017-02-02T17:05:46+00:00">febrero 2, 2017</time>](https://archivo.contagioradio.com/2017/02/02/)Por diferentes regiones de Colombia caminan más de 6000 mil guerrilleros de las FARC-EP hacia las Zonas Veredales y de Transición[LEER MÁS](https://archivo.contagioradio.com/las-farc-ep-caminan-hacia-la-paz/)  
[![Un ritual de duelo contra la muerte de niños Wayúu](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/05/muerte-de-ninos-wayuu-4-770x260.jpg "Un ritual de duelo contra la muerte de niños Wayúu"){width="770" height="260"}](https://archivo.contagioradio.com/ritual-de-duelo-contra-la-muerte-de-ninos-wayuu/)  

###### [Un ritual de duelo contra la muerte de niños Wayúu](https://archivo.contagioradio.com/ritual-de-duelo-contra-la-muerte-de-ninos-wayuu/)

[<time datetime="2016-05-11T17:53:42+00:00" title="2016-05-11T17:53:42+00:00">mayo 11, 2016</time>](https://archivo.contagioradio.com/2016/05/11/)Fotos: Contagio Radio 11 May 2016 "Cada muerte nos tiene que doler a todos, indígenas y no indígenas, blancos o afros, de Colombia y el mundo", este fragmento acompañaba uno de los textos que mujeres...[LEER MÁS](https://archivo.contagioradio.com/ritual-de-duelo-contra-la-muerte-de-ninos-wayuu/)  
[![Comunidad indígena Unión Agua clara retorna sin garantías en el territorio](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/12/DSC44851-770x260.jpg "Comunidad indígena Unión Agua clara retorna sin garantías en el territorio"){width="770" height="260"}](https://archivo.contagioradio.com/retorno-comunidad-indigena-union-agua-clara/)  

###### [Comunidad indígena Unión Agua clara retorna sin garantías en el territorio](https://archivo.contagioradio.com/retorno-comunidad-indigena-union-agua-clara/)

[<time datetime="2015-12-19T13:00:35+00:00" title="2015-12-19T13:00:35+00:00">diciembre 19, 2015</time>](https://archivo.contagioradio.com/2015/12/19/)Fotos: Contagio Radio - Gabriel Galindo El recorrido de 6 horas y media que en barco debe hacerse desde Buenaventura para llegar a la comunidad indígena "Unión Agua Clara", del pueblo Wounaan, parecía una distancia corta comparada...[LEER MÁS](https://archivo.contagioradio.com/retorno-comunidad-indigena-union-agua-clara/)  
[![Galería – El arte por la vida, la vida por el arte “Tripido” vive](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/08/tripido6-640x260.jpg "Galería – El arte por la vida, la vida por el arte “Tripido” vive"){width="640" height="260"}](https://archivo.contagioradio.com/galeria-el-arte-por-la-vida-la-vida-por-el-arte-tripido-vive/)  

###### [Galería – El arte por la vida, la vida por el arte “Tripido” vive](https://archivo.contagioradio.com/galeria-el-arte-por-la-vida-la-vida-por-el-arte-tripido-vive/)

[<time datetime="2015-08-25T15:17:02+00:00" title="2015-08-25T15:17:02+00:00">agosto 25, 2015</time>](https://archivo.contagioradio.com/2015/08/25/)"Tripido, 4 años de ausencia" fue uno de los mensajes que quedo grabado en el puente de la calle 116 con Av Boyacá,  donde jóvenes y familiares conmemoraron el asesinato de  Diego Felipe Becerra, grafitero...[LEER MÁS](https://archivo.contagioradio.com/galeria-el-arte-por-la-vida-la-vida-por-el-arte-tripido-vive/)  
[![Historias de los jóvenes de Curvaradó y Cacarica a través de la fotografía](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/07/Camelias-5-640x260.jpg "Historias de los jóvenes de Curvaradó y Cacarica a través de la fotografía"){width="640" height="260"}](https://archivo.contagioradio.com/historias-de-los-jovenes-de-curvarado-y-cacarica-a-traves-la-fotografia/)  

###### [Historias de los jóvenes de Curvaradó y Cacarica a través de la fotografía](https://archivo.contagioradio.com/historias-de-los-jovenes-de-curvarado-y-cacarica-a-traves-la-fotografia/)

[<time datetime="2015-07-02T12:36:30+00:00" title="2015-07-02T12:36:30+00:00">julio 2, 2015</time>](https://archivo.contagioradio.com/2015/07/02/)Foto: Revelados2 Jul 2015Con el ánimo de potenciar el diálogo y la comunicación entre quienes hacen parte de las propuestas educativas de las zonas humanitarias Las Camelias (Colegio de la AFLICOC) y Nueva Esperanza en Dios...[LEER MÁS](https://archivo.contagioradio.com/historias-de-los-jovenes-de-curvarado-y-cacarica-a-traves-la-fotografia/)  
[![Así fue la marcha del orgullo gay en Bogotá](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/06/MG_9364-e1475069278488-640x260.jpg "Así fue la marcha del orgullo gay en Bogotá"){width="640" height="260"}](https://archivo.contagioradio.com/galeria-de-fotos-el-orgullo-gay-marcha-en-colombia/)  

###### [Así fue la marcha del orgullo gay en Bogotá](https://archivo.contagioradio.com/galeria-de-fotos-el-orgullo-gay-marcha-en-colombia/)

[<time datetime="2015-06-28T21:00:07+00:00" title="2015-06-28T21:00:07+00:00">junio 28, 2015</time>](https://archivo.contagioradio.com/2015/06/28/)Foto: Contagio Radio 28 jun 2015 Así se vivió el 28 de junio en Bogotá, durante la XX marcha de Orgullo Gay. Ver El domingo 28 de Junio se realizará la XX marcha de la ciudadanía...[LEER MÁS](https://archivo.contagioradio.com/galeria-de-fotos-el-orgullo-gay-marcha-en-colombia/)  
[![Galería de Fotos: Espanta Pájaros y Aguilas Negras en la UN](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/06/MG_8781-copy-640x260.jpg "Galería de Fotos: Espanta Pájaros y Aguilas Negras en la UN"){width="640" height="260"}](https://archivo.contagioradio.com/galeria-de-fotos-espanta-pajaros-y-aguilas-negras-en-la-un/)  

###### [Galería de Fotos: Espanta Pájaros y Aguilas Negras en la UN](https://archivo.contagioradio.com/galeria-de-fotos-espanta-pajaros-y-aguilas-negras-en-la-un/)

[<time datetime="2015-06-04T22:31:45+00:00" title="2015-06-04T22:31:45+00:00">junio 4, 2015</time>](https://archivo.contagioradio.com/2015/06/04/)4 Jun 2014 En la Universidad Nacional se construyeron espantapájaros, en una jornada simbólica de protección, como respuesta a las reiteradas amenazas de muerte contra profesores y estudiantes. Fotografías de Carmela María  [LEER MÁS](https://archivo.contagioradio.com/galeria-de-fotos-espanta-pajaros-y-aguilas-negras-en-la-un/)  
[](https://archivo.contagioradio.com/asi-se-vivio-el-1-de-mayo-en-bogota/)  

###### [Así se vivió el 1 de mayo en Bogotá](https://archivo.contagioradio.com/asi-se-vivio-el-1-de-mayo-en-bogota/)

[<time datetime="2015-05-01T19:18:12+00:00" title="2015-05-01T19:18:12+00:00">mayo 1, 2015</time>](https://archivo.contagioradio.com/2015/05/01/)En Bogotá miles de personas de partidos políticos, grupos juveniles, sindicatos, movimientos sociales, salieron a las calles a marchar conmemorando el 1 de mayo, día internacional del trabajo. La marcha transcurrió entre música, cantos y...[LEER MÁS](https://archivo.contagioradio.com/asi-se-vivio-el-1-de-mayo-en-bogota/)  
[](https://archivo.contagioradio.com/estos-son-los-ganadores-del-2-festival-de-cine-por-los-derechos-humanos/)  

###### [Estos son los ganadores del 2° Festival de Cine por los Derechos humanos](https://archivo.contagioradio.com/estos-son-los-ganadores-del-2-festival-de-cine-por-los-derechos-humanos/)

[<time datetime="2015-04-14T17:34:42+00:00" title="2015-04-14T17:34:42+00:00">abril 14, 2015</time>](https://archivo.contagioradio.com/2015/04/14/)Con gran participación finalizo el 2° Festival de Cine por los Derechos humanos de Bogotá, un espacio de diálogo y reflexión a través del audiovisual entorno a la construcción de una posición crítica frente al...[LEER MÁS](https://archivo.contagioradio.com/estos-son-los-ganadores-del-2-festival-de-cine-por-los-derechos-humanos/)  
[](https://archivo.contagioradio.com/galeria-encuentro-de-ninas-y-ninos-de-asotracampo-para-dejar-volar-la-imaginacion-2/)  

###### [Galería encuentro de niñas y niños de Asotracampo “Para dejar volar la imaginación”](https://archivo.contagioradio.com/galeria-encuentro-de-ninas-y-ninos-de-asotracampo-para-dejar-volar-la-imaginacion-2/)

[<time datetime="2015-04-13T11:50:15+00:00" title="2015-04-13T11:50:15+00:00">abril 13, 2015</time>](https://archivo.contagioradio.com/2015/04/13/)El pasado 12 de abril, niños y niñas de la comunidad del Tamarindo, en Atlántico, que hacen parte del Espacio Humanitario Mirador Refugio de Paz y Esperanza Asotracampo, se encontraron “Para dejar volar la imaginación”,...[LEER MÁS](https://archivo.contagioradio.com/galeria-encuentro-de-ninas-y-ninos-de-asotracampo-para-dejar-volar-la-imaginacion-2/)  
[](https://archivo.contagioradio.com/galeria-de-fotos-del-partido-por-la-paz/)  

###### [Galería de fotos del Partido por la Paz](https://archivo.contagioradio.com/galeria-de-fotos-del-partido-por-la-paz/)

[<time datetime="2015-04-11T16:46:14+00:00" title="2015-04-11T16:46:14+00:00">abril 11, 2015</time>](https://archivo.contagioradio.com/2015/04/11/)La jornada deportiva se llevó a cabo el 10 de abril del 2015, en el marco de la conmemoración del 9 de abril y la Cumbre de Arte y Cultura para la Paz.[LEER MÁS](https://archivo.contagioradio.com/galeria-de-fotos-del-partido-por-la-paz/)
