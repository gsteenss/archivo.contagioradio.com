Title: Plebiscito por la paz se discute en la Corte Constitucional
Date: 2016-05-26 09:21
Category: Nacional, Paz
Tags: Corte Constitucional, dialogos de paz, plebiscito por la paz, refrendación acuerdos de paz
Slug: plebiscito-por-la-paz-va-a-audiencia-publica-en-la-corte-constitucional
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/05/presidente-santos-plesbiscito-para-la-paz.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Presidencia] 

###### [26 Mayo 2016 ]

Este jueves en la Corte Constitucional se lleva a cabo la audiencia pública sobre el Plebiscito por la paz, con la participación de delegados de Gobierno, organizaciones de la sociedad civil y algunas universidades.

### Juan Manuel Santos, Presidente de la República 

El presidente Juan Manuel Santos durante su intervención refirió su obligación y compromiso con el pueblo colombiano de hacer realidad el derecho a la paz, consagrado en el artículo 22 de la Constitución Nacional. En este sentido aseguró que su gobierno está comprometido con poner fin al conflicto armado y obtener la paz, cesando el sufrimiento de las víctimas y fortaleciendo la garantía de sus derechos.

Santos aseguró que el plebiscito es el mecanismo más idóneo para preguntar a quienes lo eligieron, sí ha cumplido con el mandato para el que fue escogido y para que el pueblo decida sí se sigue avanzando en el camino recorrido en La Habana, determinando sí el acuerdo es el adecuado. "La paz no es de mi gobierno, la paz pertenece a todos los colombianos", aseveró el Presidente e insistió en la necesidad de que los acuerdos sean ampliamente conocidos por la sociedad civil para que sean legitimados popularmente.

### Juan Fernando Cristo, Ministerio del Interior 

Por su parte el Ministro del Interior, Juan Fernando Cristo, afirmó que el plebiscito debe leerse en el marco del acto legislativo para la paz que se debatirá en el Congreso la próxima semana, del que ni los sectores más detractores han encontrado vicios de tramite, un hecho que demuestra  el compromiso del gobierno nacional con el logro de una paz estable y duradera, que debe convertirse en el pilar fundamental del Estado para implementar los acuerdos. De acuerdo con Cristo, la reducción del umbral se decidió con el fin de motivar la participación de los colombianos en un tema tan trascedental como acabar un conflicto de más de 60 años, teniendo en cuenta que los umbrales han servido para incrementar la abstención.

### Sergio Jaramillo, Alto Comisionado 

Sergio Jaramillo insistió en que se está llegando al fin de una guerra que pocos creyeron que iba a concluir, y en esa medida urge que se garantice la estabilidad jurídica de los acuerdos y su consulta popular a través de un plebiscito. El Alto Comisionado aseguró que proponer una asamblea nacional constituyente es un camino errado, porque no se trata de hacer otra Constitución sino de garantizar que el fin del conflicto permita hacer realidad los derechos allí consagrados, a través de la participación activa de la ciudadanía, teniendo en cuenta que la refrendación es la base de la implementación y que no es el momento de llamar a la resistencia civil.

### Humberto de La Calle, Equipo Negociador Gobierno 

Humberto de La Calle, Jefe Negociador del Gobierno, afirmó que el ingreso al bloque de constitucionalidad de los acuerdos es una necesidad para todos los sectores y que no puede tildarse de inconstitucional consultar al pueblo colombiano sobre lo acordado en La Habana, en esta medida el plebiscito será "el punto de encuentro de la nación colombiana en torno a temas fundamentales".

### Martha Isabel Castañeda, Procuraduría 

Martha Isabel Castañeda, Vice Procuradora General, expresó que desde la Procuraduría se promueve la paz pero que el organismo tiene notables diferencias con el mecanismo con el que se está llegando a ella. Catañeda aseguró que el plebiscito como se regulará en la ley estatutaria no es adecuado, porque reduce la participación del Congreso, tiene un umbral reducido para la voluntad ciudadana y es una figura que no cumple con requerimientos estatutarios y constitucionales.

### Alfonso Cajiao, Defensoría del Pueblo 

El Defensor del Pueblo, Alfonso Cajiao, aseveró que el nuevo umbral es constitucional, pues asegura el debate democrático pese a que se traduzca en un sí o en un no, no significa entonces que el 13% de la sociedad esté decidiendo por todos, lo que se busca es blindar los acuerdos del alto nivel de abstención en Colombia, teniendo en cuenta que el acuerdo final requiere legitimidad política por parte de la ciudadanía, así como seguridad jurídica y que será el plebiscito el puente que logrará esta unión. Para este ministerio es constitucional que los funcionarios públicos promuevan abiertamente el si o el no por el plebiscito, teniendo en cuenta las sentencias de la Corte.

### Danilo Rojas, Consejo de Estado 

Danilo Rojas, Presidente del Consejo de Estado, afirmó que es un proyecto bien formulado pues no se trata de legitimar a un gobernante sino de buscar la legitimidad de una política pública, y mantiene el carácter vinculante y la obligatoriedad de la consulta de decisiones de interés nacional, como lo es la paz que implica reformas estructurales e incluyentes.

### Rodrigo Uprimny Yepes, Universidad Nacional de Colombia 

El jurista Rodrigo Uprimny Yepes, de la Universidad Nacional de Colombia, aseguró que es posible alcanzar la paz en el marco de la Constitución, y que el umbral aprobatorio establecido no otorga poder a la minoría pues se mantiene que el si debe ganar al no, para que se apruebe la decisión, entonces de ningún modo se viola la libertad de participación de la ciudadanía. El 13% es razonable porque equivale a lo establecido constitucionalmente para modificar la carta magna a través de un referendo, y fuera de este mecanismo de participación, para ningún otro la Constitución determina umbrales aprobatorios, aseveró Uprimny. Por su parte el Decano de la Facultad de derecho, Camilo Guzmán asegura que aún cuando la Corte declare inconstitucional el plebiscito el ejecutivo puede consultar al pueblo colombiano.

### Luisa Fernanda García, Universidad del Rosario 

De acuerdo con Luisa Fernanda García, de la Universidad del Rosario, ante la carencia de idoneidad y legitimidad del plebiscito, la Corte debe declararlo inexequible, teniendo en cuenta que sí la paz es deber y derecho de obligatorio cumplimiento, no resulta acertado preguntarle al pueblo colombiano sí está o no de acuerdo con la paz; por otra parte éste ha sido el eje de los régimenes totalistaristas y por tanto viola el principio democrático, obedeciendo a lo emocional y no a lo racional, como bien podría hacerse con un referendo.

### Gustavo Gallón, Comisión Colombiana de Juristas 

Gustavo Gallón, integrante de la Comisión Colombiana de Juristas, aseguró que por medio del referendo no se puede consultar sobre la paz, pues la ciudadanía tendría que aprobar o desaprobar punto por punto, y no votarlo en conjunto como puede hacerlo a través del plebiscito. Gallón agregó que en el articulado del proyecto de ley no se evidencian riesgos para la democracia y la constitucionalidad, y que aunque el plebiscito, no es necesario, sí es constitucional y puede ser conveniente como mecanismo para aprobar el acuerdo final que convengan el Gobierno y las Farc. [[Intervención CCJ Gustavo Gallón](http://bit.ly/1OQ0KyY) ]

### César Rodríguez, DeJusticia 

Según afirma César Rodríguez, el plebiscito no rompe el orden constitucional para hacer la paz, pues justamente lo que busca es insertar el acuerdo final en la Constitución, para no forzar un mecanismo extraconstitucional a la hora de implementarlo. De este modo el plebiscito actúa como un entramado que convierte un acuerdo bilateral en norma jurídica y política estatal. Rodríguez insiste en que el plebiscito no desconoce, ni atenta contra el artículo 22 de la Constitución, pues lo que busca es garantizar que la paz sea posible.

### Jomary Ortegón Osorio, Colectivos de Abogados 

La abogada Jomary Ortegón Osorio, del Colectivos de Abogados, expresó que el plebiscito, como refrendación jurídica no es obligatorio en términos legales; sin embargo, sí es necesario que la sociedad civil se pronuncie frente a este tema de trascendencia nacional. No obstante este mecanismo no agota la participación de las víctimas, las comunidades, las organizaciones y la institucionalidad en la implementación de los acuerdos paz; y en esa medida puede considerarse más adelante, el desarrollo de otros mecanismos como una asamblea constituyente.

### Luis Emil Sanabria, Redepaz 

De acuerdo con Luis Emil Sanabria, de Redepaz, estamos ante la oportunidad de construir un pacto por la paz y la reconciliación, y la refrendación es el mecanismo que garantizará el papel protagónico de la sociedad civil en la implementación de los acuerdos, que necesitarán el respaldo de la mayoría de los colombianos para que se consolide la paz, lo que incluirá que la población en general comprenda la necesidad de superar el conflicto y llegar a un acuerdo de paz. Sanabria llamó la atención sobre la necesidad de que el plebiscito cuente con las garantías presupuestales y de seguridad para que se pueda llevar a cabo en todo el territorio nacional.

### Pedro Vaca, Fundación para la Libertad de Prensa 

Pedro Vaca, director de la Fundación para la Libertad de Prensa, aseguró que el plebiscito no es inconstitucional dado que tiene un fin legítimo y es la búsqueda de la paz; sin embargo, dada la complejidad y extensión de los acuerdos, la ciudadanía debe informarse ampliamente antes de votar a favor o en contra.

###  Jorge Fernando Pérdomo, Fiscal encargado 

El fiscal encargado aseveró que este mecanismo es inconstitucional porque desconoce y modifica la esencia con la que fue creado el plebiscito, pues transforma las facultades constitucionales del ejecutivo, el Congreso y las demás autoridades públicas. De otro lado, el alcance de la decisión refuerza la idea de que con el plebiscito se vulnera la carta magna, porque una respuesta desaprobatoria sometería el derecho de la paz a la elección de las mayorías, es decir que su cumplimiento sería sometido por un resultado electoral, descnociendo que "la paz es un derecho que no debe depender de la opinión mayoritaría".

### Linda María Cabrera, Alianza Cinco Claves, Sisma Mujer 

Según Linda María, el plebiscito es un mecanismo adecuado de refrendación, teniendo en cuenta que se trata de un política de gobierno que con la aprobación del Congreso y la Corte Constitucional puede llegar a convertirse en política estatal, con la participación de la institucionalidad y la sociedad civil. Como mecanismo incorpora novedades, y desde las organizaciones de mujeres se espera que éstas garanticen la participación efectiva de la población femenina. Cabrera llamó la atención por la elección que se hizo para motivar la intervención de los sectores organizados de las mujeres en esta audiencia pública, e insiste en que sin las mujeres no es posible que se consolide la paz.

\[embed\]https://www.youtube.com/watch?v=Tkdg5nypDx0\[/embed\]

Siga en vivo la audiencia: [[bit.ly/1oh7ixe](http://bit.ly/1oh7ixe)]

<iframe src="http://www.ivoox.com/player_ej_11673890_2_1.html?data=kpajmZicfZGhhpywj5aZaZS1kpeah5yncZOhhpywj5WRaZi3jpWah5yncaXd1Mji1NjTb8XZzZC91MrXrcXZz9nSjbjFstXj1JCastHJpsrnxM7h0ZDUpdufzcaY0sbecYarpJKw0dPYpcjd0JC%2Fw8nNs4yhhpywj5k%3D&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en][su correo](http://bit.ly/1nvAO4u)[o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por][[Contagio Radio](http://bit.ly/1ICYhVU)][ ] 
