Title: Cuáles son los mecanismos de autoprotección para líderes sociales
Date: 2018-09-14 13:13
Author: AdminContagio
Category: Comunidad, DDHH
Tags: Comunidad, Derechos Humanos
Slug: mecanismos-autoproteccion-lideres-sociales
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/06/lideres.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Archivo Contagio Radio 

###### 14 Sept 2018 

Los defensores de los  derechos humanos (DDHH) en Colombia, constantemente reciben amenazas, son asesinados y se encuentran diariamente en riesgo por la labor que cumplen, pero lo más irónico es que el país vive un proceso de paz y pese a esto se ha aumentado el  asesinato de los líderes sociales.

Es por esto  que Colombia se encuentra en la mira de los entes internacionales que observan de manera conjunta el cumplimiento de la ¨**Sentencia T-025**, la Corte ha emitido varios autos en los que le recuerda al Estado que debe aplicar el enfoque diferencial, reconociendo además los riesgos concretos que corren las personas, organizaciones y comunidades expuestas al conflicto y al desplazamiento forzado y que reconociendo esa diferencia, atienda y respete a la población afectada como sujetos de derecho¨ Corte constitucional.

Entre la institucionalidad representada en la Unidad Nacional de Protección hasta las organizaciones de víctimas y el Programa de Naciones Unidas para el Desarrollo (PNUD) con el sentido de lograr acabar con las amenazas,  por esto la importancia actualmente de trabajar hacia la visibilización de los derechos humanos y el auto mecanismo de protección para quienes adelantan la labor de líderes sociales en los territorios o comunidades.

### ¿Cuáles son los mecanismos de autoprotección de los defensores de los derechos humanos? 

Con el fin de proteger a los defensores de los derechos humanos, se han desarrollado una serie de medidas de autoprotección con el fin de ejercer la ¨seguridad¨ que garantice no solo su bienestar físico sino la permanencia de los mismo en los territorios, y puedan continuar con su cultura o sus acciones sin la presión de ser asesinados o secuestrados.

### Mecanismos de autoprotección:

Ante señalamientos o estigmatización  el defensor puede presentar:

-   Denuncia Pública
-   Denuncia penal ante la Fiscalía y Procuraduría.

Vivienda: Los defensores o líderes sociales deben estar en constante comunicación con sus familiares e informarles hacia donde se dirigen y qué tipo de actividades realizan.

Seguimiento ilegal: En Colombia es constante que se les haga seguimiento a los defensores de los derechos humanos, por parte de agentes del estado, quienes de manera irregular registran las actividades que legalmente hacen las organizaciones sociales, lo cual se constituye un delito. Se recomienda en estas situaciones:

-   Denuncie el hecho ante la Personería y la Fiscalía para que se realice las investigaciones pertinentes.
-   No enfrentarse de manera directa ni sólo a quienes realizan el seguimiento.

**Allanamiento**s: en caso de allanamiento no permita:

-   No permita la entrada a su vivienda a los funcionarios que realizan la diligencia, hasta una vez se haya establecido la veracidad de la orden judicial, la cual puede ser verificada llamando a los números que se encuentran en los documentos.
-   Revise la orden judicial mirando la dirección la cual debe corresponder con el domicilio a la que se realizará la inspección, establecer el motivo para saber que busca con esa acción y conocer quien autoriza la orden.

**Zona Rural:** Cuando un defensor va a viajar en zonas rurales debe tener en cuenta

-   Desplazarse en grupo ya sea entre veredas o hacia los cascos urbanos.
-   Si habita una zona donde hay cobertura telefónica, mantenga el celular con suficiente batería y saldo suficiente para llamar en caso de emergencia.
-   Construya lista de contactos que tengan acceso a líneas de la Defensoría del Pueblo, Procuraduría, Cruz Roja Internacional y entre otras instituciones que puedan llevarlo con usted.
-   En zonas donde exista campos minados, no desviarse de los caminos principales.

**Estas medidas de autoprotección** realizadas por los defensores de los derechos humanos, tienen la intención de trabajar de la mano con las medidas de protección por parte del gobierno asegurándose desde las dos partes la seguridad a su bienestar y integridad.

Aun así, tras el aumento de asesinatos de líderes sociales, el ministro del interior reconoció que era necesario implementar más mecanismo de seguridad para los líderes sociales expidiendo un decreto que crean medidas adicionales basado en un modelo de protección colectiva, esto pensado desde más que la atención de seguridad y protección es un ¨promotor de paz¨ integrado en las comunidades o regiones del país con la posibilidad de intervenir entre los defensores y las autoridades en casos de emergencia.

[Con información de Cartilla Corporación jurídica libertad](https://rndp.org.co/wp-content/uploads/2017/06/Cartilla-de-Defensores-de-DDHH-1.pdf)

[<iframe id="audio_28585431" src="https://co.ivoox.com/es/player_ej_28585431_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>](https://rndp.org.co/wp-content/uploads/2017/06/Cartilla-de-Defensores-de-DDHH-1.pdf)

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
