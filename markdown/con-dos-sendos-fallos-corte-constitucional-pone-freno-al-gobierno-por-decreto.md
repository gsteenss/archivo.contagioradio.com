Title: Con dos sendos fallos, Corte Constitucional pone freno al «Gobierno por Decreto»
Date: 2020-07-24 16:59
Author: PracticasCR
Category: Actualidad, Nacional
Tags: Corte Constitucional, Decretos Legislativos, Estado de Emergencia, Fernando Ruíz, Iván Duque
Slug: con-dos-sendos-fallos-corte-constitucional-pone-freno-al-gobierno-por-decreto
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/07/Corte-Constitucional-e-Iván-Duque.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

A través de dos pronunciamientos, dados a conocer este jueves,  **la Corte Constitucional declaró inconstitucionales dos Decretos Legislativos emitidos por el presidente Iván Duque en ejercicio de sus facultades especiales adquiridas con la declaratoria del estado de emergencia**, económica social y ecológica por la pandemia del Covid-19.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Se trata de los Decretos Legislativos [558](https://dapre.presidencia.gov.co/normativa/normativa/DECRETO%20558%20DEL%2015%20DE%20ABRIL%20DE%202020.pdf) y [580](https://dapre.presidencia.gov.co/normativa/normativa/DECRETO%20580%20DEL%2015%20DE%20ABRIL%20DE%202020.pdf) expedidos el 15 de abril de 2020 por el Presidente. Las decisiones de la Corte, han suscitado diversas opiniones e implican que el texto contenido en los decretos pierda total validez jurídica para hacerlos aplicables. (Le puede interesar: [Las razones para pedir a la Corte Constitucional la revisión del decreto de estado de emergencia](https://archivo.contagioradio.com/las-razones-para-pedir-a-la-corte-constitucional-la-revision-del-decreto-de-estado-de-emergencia/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Dado que ambos decretos tenían fuerza de ley y solo pudieron ser emitidos en ejercicio de las facultades extraordinarias adquiridas por el presidente; **ahora mismo la única forma para ‘revivirlos’, jurídicamente hablando, sería la declaratoria de un nuevo estado de emergencia**, lo cual, según varios sectores sería algo inconveniente en la medida en que se ha afirmado que el gobierno está legislando por decreto lo que ha llevado en palabras del Senador Roy Barreras a la **«*anulación del Congreso*».**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Lea también: [Corte Constitucional suspende sesiones virtuales del Congreso y frena al gobierno por decreto](https://archivo.contagioradio.com/corte-constitucional-suspende-sesiones-virtuales-del-congreso-y-frena-al-gobierno-por-decreto/)

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Corte Constitucional le da un respiro a Colpensiones

<!-- /wp:heading -->

<!-- wp:paragraph -->

El Decreto 558 fue uno de los dos decretos declarados inexequibles por la Corte. Esta norma permitía que por dos meses, de forma voluntaria, las empresas no hicieran la cotización completa de sus empleados, reduciendo el aporte de un 16% a un 3%. **Esto iba en contra de los derechos de los trabajadores** porque al no cotizar completos los aportes, las semanas trabajadas por los empleados de las empresas que se acogían al Decreto, no eran contabilizadas en su historia laboral.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por otro lado, el Decreto también ordenaba el traslado de 25.000 afiliados y pensionados de los fondos privados hacia el fondo público Colpensiones, lo cual fue cuestionado por muchos expertos en economía, ya que **el fondo público corría un alto riesgo de desfinanciamiento, por el ingreso masivo y repentino de usuarios provenientes del régimen de ahorro pensional privado.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El traslado, según expertos, se autorizaba en un momento inoportuno, pues cabe recordar que el sistema pensional se creó mediante la ley 100 de 1993, es decir, **en este momento estaría cumpliendo 27 años, que es casi el tiempo que le toma a un trabajador completar el número de semanas cotizadas requerido para pensionarse.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Esto, llevaría en la práctica a que una persona que cotizó e hizo sus aportes toda la vida para un fondo privado, tuviera que ser pensionada por Colpensiones**, librando a los fondos privados de su obligación de desembolsar la pensión** con los dineros que administraron durante toda la vida del trabajador.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/wilsonariasc/status/1286379052228542464","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/wilsonariasc/status/1286379052228542464

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/angelamrobledo/status/1286417705436876806","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/angelamrobledo/status/1286417705436876806

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:heading {"level":3} -->

### El "descuido" del Gobierno que hizo caer los subsidios para el pago de servicios públicos

<!-- /wp:heading -->

<!-- wp:paragraph -->

Por otro lado, **por falta de las firmas de dos de los ministros del gabinete del Gobierno, la Corte Constitucional tuvo que declarar inexequible el Decreto 580**, el cual había permitido subsidios para el pago de servicios públicos con motivo de la pandemia del Covid-19.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En concreto, **al Decreto le faltaron las firmas del Ministro de Salud, Fernando Ruíz Gómez y de la Ministra de Ciencia y Tecnología, Mabel Gisela Torres** y según la Corte, al proceso no se allegó ningún tipo de justificación que permitiera explicar la omisión en las firmas, razón por la cual tuvo que declarar su inconstitucionalidad, por vicios jurídicos de forma.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

La votación fue unánime, pues según la Corte, la Constitución Política ordena que todos los ministros tienen que suscribir los decretos legislativos emitidos por el Presidente; y **este mandato constitucional es indispensable para la validez de las normas emitidas en el marco de estados de emergencia.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El Decreto 580 señalaba que los mandatarios de municipios y ciudades **podían aplicar subsidios sobre el costo de servicios como acueducto, alcantarillado y aseo, hasta por un 80% en el caso de los habitantes de estrato 1, 50% para el estrato 2, y 40% para el estrato 3.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Razón por la cual, con la declaratoria de inconstitucionalidad, por la omisión de los ministros del gabinete del Gobierno Nacional**, los estratos más vulnerables se verán privados de acceder a estos beneficios y tendrán que pagar la totalidad de sus facturas de servicios públicos.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

La omisión causó bastante suspicacia, ejemplo de ello, fue el Senador por el Partido Polo Democrático Alternativo, Wilson Arias, quien se manifestó respecto al hecho en Twitter.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/wilsonariasc/status/1286666073786462208","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/wilsonariasc/status/1286666073786462208

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:block {"ref":78955} /-->
