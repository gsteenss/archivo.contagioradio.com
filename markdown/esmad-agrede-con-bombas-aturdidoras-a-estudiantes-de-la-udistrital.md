Title: ESMAD agrede con bombas aturdidoras a estudiantes de la UDistrital
Date: 2016-05-23 14:55
Category: DDHH, Nacional
Tags: agresiones esmad, ESMAD, paro universidad distrital, Universidad Distrital
Slug: esmad-agrede-con-bombas-aturdidoras-a-estudiantes-de-la-udistrital
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/05/esmad-noche.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio ] 

###### [23 Mayo 2016]

Tras la asamblea general que se llevó a cabo el pasado viernes en la sede administrativa de la Universidad Distrital, para evaluar la respuesta del Consejo Superior ante el pliego de exigencias, por lo menos 200 estudiantes salieron a las calles a comunicar a la ciudadanía las problemáticas por las que atraviesa la institución, cuando fueron **agredidos por efectivos del ESMAD con pistolas marcadoras y bombas aturdidoras. **

**Seis estudiantes resultaron heridos, a uno de ellos le estalló una bomba aturdidora en el brazo izquierdo; sin embargo, están fuera de peligro, **asegura John Noguera, y agrega que cuando intentaron resguardarse en las instalaciones de la Universidad, el ESMAD acordonó el lugar y durante hora y media grabó con celulares los rostros de quienes acampan en esta sede.

Noguera afirma que este martes se espera el pronunciamiento del Consejo Superior sobre la posible suspensión del semestre; sin embargo, la decisión tomada durante la asamblea es que **el paro continúa así se suspenda el semestre**. Entretanto miembros de la Fiscalía adelantan investigaciones en la sede tecnológica para esclarecer los hechos que rodean el [[ataque contra el estudiante Miguel Ángel Barbosa](https://archivo.contagioradio.com/habrian-borrado-videos-que-pobrarian-agresion-de-esmad-contra-estudiante-de-udistrital/)].

En la mañana de este lunes, funcionarios de la Universidad ingresaron a la fuerza por el parqueadero de la sede administrativa, **luego irrumpieron violentamente por el séptimo piso y agredieron a algunos de los estudiantes**. Algunas organizaciones estudiantiles y defensoras de derechos humanos como el CPDH y la Defensoría se hicieron presentes para garantizar la seguridad de los estudiantes, quienes continúan en la toma pacífica del edificio.

<iframe src="http://co.ivoox.com/es/player_ej_11636001_2_1.html?data=kpajlZuUdJKhhpywj5WcaZS1lpeah5yncZOhhpywj5WRaZi3jpWah5yncavjydOYsNTLucbmwpCajarXuNbYysbb1sqPmaWhhpywj6jTstXVyM7cjbfFqMrjjJKSmaiReA%3D%3D&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)] ] 
