Title: Buenaventura sigue en Paro pese a represión, militarización y toque de queda
Date: 2017-05-20 22:17
Category: Movilización, Nacional
Tags: buenaventura, paramilitares
Slug: buenaventura-sigue-en-paro-a-pesar-de-la-represion-la-militarizacion-y-el-toque-de-queda
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/05/buenaventura_4_0.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: elespectador] 

###### [20 Mayo 2016]

Luego de la jornada del pasado viernes en buenaventura, en que el gobierno nacional se levantó de la mesa de concertación con el comité de paro, los líderes de la manifestación aseguraron que no se van a dejar provocar nuevamente por la policía para que **no se justifique la represión pero tampoco se dejen de lado  las razones por las que están protestando desde hace 6 días.**

Según Manuel Bedoya, integrante del Comité del Paro Cívico, para vivir mejor y con dignidad en el territorio, la movilización de ayer era absolutamente pacífica hasta que retiraron la fuerza pública de los locales comerciales, tan solo una hora después de que se levantara la mesa de concertación. Dejaron la ciudad sin policía y luego provocaron los desórdenes al enviar el ESMAD. [Lea también: Si no cerramos el puerto no nos prestan atención](https://archivo.contagioradio.com/si-no-cerramos-el-puerto-el-gobierno-no-nos-presta-atencion-habitantes-de-buenaventura/)

El balance que se conoce deja 80 personas detenidas, un joven asesinado y 2 menores de edad en delicado estado de salud por la acción de los gases que fueron lanzados indiscriminadamente contra la población. Según Bedoya es probable que los niños deban ser trasladados a Cali dado que su estado de salud no mejora y en Buenaventura no se cuenta con una institución de salud que pueda atender ese y u otro tipo de emergencias.

> A esta hora en [\#Buenaventura](https://twitter.com/hashtag/Buenaventura?src=hash) se mantiene el paro cívico pese a la agresión del ESMAD contra manifestantes [pic.twitter.com/05gRR73pnt](https://t.co/05gRR73pnt)
>
> — Contagio Radio (@Contagioradio1) [20 de mayo de 2017](https://twitter.com/Contagioradio1/status/865731829130776577)

<p>
<script src="//platform.twitter.com/widgets.js" async charset="utf-8"></script>
</p>
### **Toque de queda y militarización no detienen la movilización** 

Desde ayer, las FFMM se encuentran en las calles de la ciudad, se reforzaron con más de 530 hombres, aseguró el comandante de lo policía, y ordenaron un toque de queda desde las 6 de la tarde. Sin embargo los habitantes se convocaron en una gran movilización pacífica y continuarán con las marchas este Domingo.

Ante los rumores de la posible presencia del Presidente Santos este Domingo en Buenaventura, los integrantes del Comité de Paro Cívico afirmaron que no han recibido una comunicación o invitación oficial y que por ello la convocatoria de marcha hasta el sector de Carrizal, a partir de las 10 de la mañana, sigue en pié. [Lea también: Pescadores artesanales amenazados por paramilitares en medio del Paro Cívico](https://archivo.contagioradio.com/pescadores-de-buenaventura-son-amenazados-por-paramilitares/).

### **Militarización y paramilitares persisten en Buenaventura** 

Una de las preguntas que desde hace más de una año se vienen haciendo los habitantes del Puerto más importante sobre el pacífico colombiano es cómo en medio de una militarización tan fuerte se siguen presentando las extorsiones, vacunas y control del microtráfico y de los barrios por parte de los paramilitares.

La explicación la han dado los promotores del Paro Cívico que han afirmado que es claro que los paramilitares ya tienen el control, ya no se está dando una disputa por los territorios y por eso ya no se presentan las escandalosas muertes que pusieron al puerto en la vista de organizaciones de DDHH tanto nacionales como internacionales.

A estas alturas y en medio del control paramilitar se explica también el abandono estatal, puesto que los empresarios siguen interesadosen los barrios de Baja Mar para los proyectos turísticos y de infraestructura que pretenden "embellecer el puerto", **"no les conviene o no les sirve que haya gente acá", explica uno de los habitantes que se reserva su nombre por seguridad.**

La situación de los habitantes de Buenaventura sigue siendo crítica y por ello han afirmado que se mantendrán movilizados, "con dignidad"  hasta que el Estado cumpla sus exigencias.

###### Reciba toda la información de Contagio Radio en [[su correo]
