Title: Reforma al sistema de educación se pasaría vía Fast Track
Date: 2017-02-03 16:02
Category: Educación, Nacional
Tags: Fast Track, Sistema Nacional de Educación Terciaria
Slug: reforma-al-sistema-de-educacion-se-pasaria-via-fast-track
Status: published

###### [Foto: Archivo Particular ] 

###### [3 Feb 2017] 

Entre **febrero y marzo de este año y vía Fast Track, sería aprobado por el presidente Juan Manuel Santos, el Sistema Nacional de Educación Terciaria**, que modificaría  la ley 30 de 1992 equiparando los conocimientos que se obtienen en la educación técnica y tecnológica con los adquiridos en la educación universitaria y que no ha sido debatida por la comunidad educativa.

El proyecto formulado por el Ministerio de Educación, respaldado por el SENA y el Ministerio de Trabajo, **pretende que la educación técnica y universitaria se complementen y se reconozcan las posibilidades de tránsito entre ellas**, con la finalidad de ampliar la cubertura en educación y garantizar la formación en regiones afectadas por el conflicto.

Sin embargo, la comunidad educativa considera que se está formulando un Sistema de Educación sin que haya existido algún tipo de debate, y que se está a socializado. Frente a que este proyecto sea presentando dentro del Fast Track. Daniel Mesa, integrante de la organización Comuna, afirma que “**el que una iniciativa que pretende reformar la educación en el país se presente vía Fast Track excede los alcances del acuerdo y segundo es antidemocrático**”

Por otro lado, tanto estudiantes como docentes han expresado su rechazó a este proyecto debido a que consideran que lo que pretende es **generar mayor oferta educativa sin tener en cuenta la calidad de la educación.** Le puede interesar:["Yaneth  Giha Tovar entra a la dirección del Ministerio de Educación"](https://archivo.contagioradio.com/yaneth-giha-tovar-entra-a-direccion-de-ministerio-de-educacion/)

### **Pérdida de la calidad de la educación con el SNET** 

Para Jennifer Pedraza, representante al Consejo Académico de la Universidad Nacional, el hecho de equiparar la educación universitaria con la educación técnica y tecnológica es **“homogenizar la educación con el fin de aumentar el ánimo de lucro y la cobertura, sin invertir en mejorar la calidad, teniendo en cuenta que el fin de estas es distinto**” debido a que en la educación técnica y tecnológica se profundiza en conocimientos prácticos, mientras que en la universitaria se profundiza en la investigación.

Agrega que en otros países del mundo, como Alemania, los técnicos y tecnólogos no son mal pagados, porque se entiende la importancia de su énfasis, mientras que acá se buscaría que "**salarialmente, tanto técnicos como tecnólogos y universitarios, sean remunerados de la misma forma".**

Andrés Salazar, Representante estudiantil ante el Consejo Superior de la Universidad Nacional, añade que los perdedores son los estudiantes **“lo que se está buscando no es hacer que las carreras técnicas y tecnólogas en el país, aumenten su énfasis en la reflexión**, sino por el contrario que sean las carreras universitarias las que disminuyan esta área de la investigación” para facilitar el tránsito entre las mismas.

Daniel Mesa indicó que otro interrogante que se abre es sobre si aumentará la financiación de la educación, ya que con este modelo la inversión sería igual para las tres formas de educación. Le puede interesar: ["Se lanza la iniciativa la Paz si es con educación"](https://archivo.contagioradio.com/se-lanza-la-iniciativa-la-paz-si-es-con-educacion/)

El representante ante la Cámara Víctor Correa citará  a una audiencia pública en la que se espera participe el Ministerio de Educación, el SENA y el Ministerio de trabajo al igual que las diferentes organizaciones de docentes y estudiantes del país **para exponer la profundización de la crisis de la educación que generaría la implementación del Sistema Nacional de Educación Terciaría y he intentar frenar su aplicación vía Fast Track**

<iframe id="audio_16817391" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_16817391_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>
