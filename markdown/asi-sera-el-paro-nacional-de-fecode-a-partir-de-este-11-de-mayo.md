Title: Esta es la agenda del paro nacional de FECODE a partir de este 11 de Mayo
Date: 2017-05-10 17:26
Category: Educación, Nacional
Tags: fecode, Paro de docentes
Slug: asi-sera-el-paro-nacional-de-fecode-a-partir-de-este-11-de-mayo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/05/fecode.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: FECODE] 

###### [10 May 2017] 

Luego de reiterados incumplimientos por parte del gobierno nacional en materia de **nivelación salarial, derecho a la salud, jornada única y ascensos en el escalafón**, los maestros y maestras del país han decidido volver a paro nacional e indefinido que iniciará sus actividades este 11 de Mayo. Según el gremio el paro es inaplazable porque la educación necesita financiación urgente.

Las actividades comenzarán a desarrollarse este 11 de Mayo, sin ambargo, solo hasta el **próximo 15 se verán en diferentes ciudades las grandes marchas y plantones de maestros y maestras**, así como estudiantes y padres de familia. Le puede interesar: ["Déficit en educación es de 600 mil millones de pesos: FECODE"](https://archivo.contagioradio.com/deficit-en-educacion-es-de-600-mil-millones-de-pesos-fecode/)

**11 de Mayo**: **Asambleas informativas en todos los colegios de educación pública en el país.** Los maestros y maestras socializaran las razones del Paro Nacional y darán a conocer las actividades que se desarrollarán de manera simultánea a partir del lunes 15 de Mayo. En algunas Asociaciones de Docentes se harán concentraciones como es el caso de Medellín.

**12 de Mayo:** Estas mismas asambleas que se realizarán con los estudiantes y padres de familia, para explicarles las razones del paro y las acciones que seguirán.

**15 de Mayo:** Coincide con la celebración del Día del Maestro, habrá actividades culturales, artísticas y plantones. En Bogotá saldrá una movilización desde el centro Administrativo de la Asociación de Docentes y Educadores, hasta el Ministerio de Educación.

Desde Medellín se realizarán actividades culturales en el a sede de ADIDA, y se llevarán movilizaciones municipales con la participación de estudiantes y padres de familia.

**16 de Mayo**: Se realizará la gran  toma de Bogotá, en esta actividad confluirá el Comando Unitario de Paro conformado por la CUT, la CTC y la CGT con delegaciones de todo el país, de igual forma se espera que docentes de todo el país lleguen a la capital para que se movilicen desde el parque Nacional hasta la Plaza de Bolívar y exigir que se instale una mesa de concertación con el Ministerio de Educación.

**Bogotá:** Los docentes tendrán como punto de concentración el Parque Nacional y llegarán a la Plaza de Bolívar.

**Medellín:** Docentes de los diferentes municipios de Antioquia se trasladaran hacia Medellín y allí participaran en una movilización masiva que tendrá como punto de encuentro la sede de ADIDA.

**Barranquilla**: Esta por definirse la ruta de movilización

**Cali**: Esta por definirse la ruta de movilización

**17 de Mayo:** Se realizará una reunión de la Junta Directiva de FECODE para evaluar las actividades previas, evaluar también los acercamientos con el gobierno y la continuidad o no del Paro Nacional Indefinido.

La decisión de convocar a Paro Nacional quedó en firme luego de que el gobierno nacional reconociera una des financiación de la educación desde el segundo semestre de 2017 que asciende a **600 mil millones de pesos y que para 2018 podría duplicar la cifra**, es decir, el panorama no tiende a mejorar y el gobierno nacional no plantea alternativas de solución.

<div class="yj6qo ajU">

<div id=":jw" class="ajR" style="text-align: justify;" tabindex="0" data-tooltip="Mostrar contenido acortado">

![](https://ssl.gstatic.com/ui/v1/icons/mail/images/cleardot.gif){.ajT}Reciba toda la información de Contagio Radio en [[su correo]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio]{.s1}](http://bit.ly/1ICYhVU)[.]{.s1}

</div>

</div>
