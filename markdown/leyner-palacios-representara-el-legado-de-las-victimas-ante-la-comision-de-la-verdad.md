Title: Leyner Palacios representará el legado de las víctimas ante la Comisión de la Verdad
Date: 2020-09-29 16:49
Author: AdminContagio
Category: Actualidad, DDHH
Tags: Chocó, Comisión de la Verdad, Leyner Palacios
Slug: leyner-palacios-representara-el-legado-de-las-victimas-ante-la-comision-de-la-verdad
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/09/LEYNER-900x.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: Leyner Palacios /[CIVP](Foto:%20CIVP)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

La Comisión para el Esclarecimiento de la Verdad, indicó que luego de un proceso de selección de más de 60 candidatos y candidatas, Leyner Palacios, líder del Comité por los Derechos de las Víctimas de Bojayá, asumirá como nuevo comisionado y continuará el trabajo que realizaba Ángela Salazar fallecida el pasado 7 de agosto.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

La decisión se toma después de un proceso de selección en el que participaron 60 candidatos a través de una convocatoria pública que establecía entre ellos criterios como la capacidad de escucha y análisis del contexto actual del país, imparcialidad y compromiso con la paz; idoneidad ética, conocimiento del conflicto armado y de la historia del país. [(Lea también: 'Bojayá entre fuegos cruzados', el documental que reconstruye la verdad de un pueblo)](https://archivo.contagioradio.com/bojaya-entre-fuegos-cruzados-el-documental-que-reconstruye-la-verdad-de-un-pueblo/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Leyner Palacios, quien hasta hoy se desempeña como secretario ejecutivo de la [Comisión Interétnica de la Verdad del Pacífico (CIVP)](https://twitter.com/VerdadPacifico)**, ha sido uno de los líderes sociales que ha denunciado la frecuente incursión de grupos paramilitares en municipios de Chocó como Bojayá** y quien ha expresado en varias ocasiones a la Fuerza Pública que hace presencia en la región como la Fuerza de Tarea Conjunta Titán, que su deber es el de dedicar sus esfuerzos a garantizar la seguridad y la libre circulación de todas las comunidades afro e indígenas del río Bojayá. [(Le puede interesar: Con derecho de petición, Ejército exige a líderes del Chocó demostrar lo que denuncian)](https://archivo.contagioradio.com/derecho-de-peticion-ejercito-exige-que-lideres-demostrar-denuncias/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

El líder social, víctima de la masacre de Bojayá en 2002 , suceso en el que en medio de la confrontación armada entre la entonces guerrilla de las FARC y las autodefensas, fueron asesinadas al menos 119 personas, ha sido blanco de constantes amenazas fruto del trabajo y las denuncias que ha realizado de cara a la crisis humanitaria que afronta Bojayá y en donde se ha denunciado la connivencia de las FF.MM. con los grupos de tipo paramilitar que operan en la zona. [(Le puede interesar: Asesinan a Arley Chalá, jefe de escoltas del lider Leyner Palacios)](https://archivo.contagioradio.com/leyner-palacios-escolta-asesinado/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

«Hemos sido insistentes en que el Estado debe reconocer su responsabilidad en esta omisión y dar la cara a Bojayá y al mundo» ha expresado el líder, en ocasiones anteriores señalando la necesidad de que se atienda a las comunidades y se trabaje por una salida dialogada al conflicto que vive el pacífico y en general Colombia, esto a propósito del más reciente Pacto por la Vida y la Paz. [(Lea también: Comunidades del Pacífico exigen a Gobierno y grupos armados una inmediata salida negociada al conflicto)](https://archivo.contagioradio.com/comunidades-del-pacifico-exigen-a-gobierno-y-grupos-armados-una-inmediata-salida-negociada-al-conflicto/)

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

A Palacios le corresponderá continuar con el legado de la comisionada Ángela Salazar, quien trabajaba en el capítulo étnico de la Comisión de la Verdad y su subcapítulo afro que hará parte del informe final que tras tres años de trabajo será entregado el 2021 con el fin de plasmar las verdades y las causas del conflicto armado en Colombia.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

La comisionada Salazar venía trabajando un ejercicio que daba prioridad al territorio, "no como un bien económico de riqueza sino de vida" y en el que , como afirmaba, el informe debía "buscar un reconocimiento de las afectaciones y de la lucha y la resistencia histórica para mantenerse en el territorio y alzar la voz". [(Le puede interesar: Compromiso de la Comisión de la Verdad es poner a la luz la resistencia afrocolombiana)](https://archivo.contagioradio.com/compromiso-de-la-comision-de-la-verdad-es-poner-a-la-luz-la-resistencia-afrocolombiana/)

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
