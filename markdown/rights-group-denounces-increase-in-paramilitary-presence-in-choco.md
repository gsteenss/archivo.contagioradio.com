Title: Rights group denounces increase in paramilitary presence in Chocó
Date: 2019-10-04 15:31
Author: CtgAdm
Category: English
Tags: Autodefensas Gaitanistas de Colombia, Chocó, paramilitaries
Slug: rights-group-denounces-increase-in-paramilitary-presence-in-choco
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/Neoparamilitares-en-Chocó.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Photo: Comisión Intereclesial de Justicia y Paz] 

 

**Follow us on Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

[The presence and territorial control of paramilitary groups in the Chocó province, specifically in the cities of Riosucio and Curbaradó, is increasing due to the lack of action from the Colombian government, the Inter-Church Justice and Peace Commission recently denounced. ]

[According to Diana Marcela Muriel, a lawyer of the Commission, the self-proclaimed Gaitanistas Self-Defense Forces of Colombia (AGC, for its Spanish acronym) has been present in the region for the past two decades, but in different forms. Previously, the group was known as the Banana or Elmer Cárdenas Block of the United Self-Defense Forces of Colombia (AUC), which survived despite the group’s disarmament.]

[While its name changed, the group continues to devastate local communities with death threats, massive displacements similar to the ones that occurred in the late ‘90s and by employing the same tactics of territorial dispossession. The Commission has also reported that the group has increasingly resorted to the forced recruitment of young men and exercises its control over the communities by ordering them how to live. Additionally, the AGC has directly made it known to the Commission that they are not allowed to accompany the communities as they had previously done.]

[Muriel questioned the government’s implementation of the peace deal, given that the agreement called for the creation of a special investigative unit to dismantle paramilitary structures or heirs of these paramilitary structures, which has yet to operate.]

### Paramilitary presence in Chocó's humanitarian zones

[According to the Commission, the AGC is building a house at the point known as El Cable, located in the collective territory of Pedeguita and Mancilla in Riosucio, Chocó. The construction of the building is being carried out on lands that were stripped of their rightful owners in 2017. According to the organization, members of this group, dressed in civilian clothes and carrying weapons and radios, keep a permanent presence there, working on farms and harassing passersby.]

[In the humanitarian zone of Las Camelias, in the collective territory of Curbaradó in the Chocó province, the Commission said that two men of the AGC entered the area this past Sunday, unlocking the fence that protects the space from intruders. There, members of the Commission and international accompaniers, along with the inhabitants of the zone, asked the paramilitary members to respect their territory. Additionally, 31 men of the AGC passed closely by the humanitarian zone on Sept. 19.]

### How has the government responded?

[The lawyer said that one of their concerns is that the region is highly militarized, but the armed forces have proven to be ineffective when combating paramilitary structures, which reportedly operates in groups of 100 or 150 people. Some members wear camouflage and heavy weaponry while transiting near the military’s sites of operations.]

[Muriel also questioned the absence of civil institutions in the region,]which allows for communities to be at the mercy of violent groups because there “isn’t a civil institution that[ receives complaints from the people.” For this reason, she said that the Ministry of Defense must create a mechanism to evaluate the military operations in the area and that the courthouses, the Attorney General’s Office, the Prosecutor’s Office and the Ombudsman’s Office must have a permanent presence in the region.]

[The communities, especially those that live in Pedeguita and Mancilla, should be benefited from precautionary measures by the Special Jurisdiction for Peace, said the lawyer, given that the violence corresponds to efforts to appropriate the lands there. If not addressed, these dynamics could continue to affect the communities.]

### Could agroindustry be a factor in the rise in violence?

[According to the lawyer, there is a territorial struggle between the agroindustry that seeks to dispossess communities of their land to grow plantain and palm oil and the native indigenous and Afro-Colombian inhabitants that want to manage the land as they have done traditionally. The lawyer assured that one of the causes of the violence is the “business project, back and consolidated by a criminal structure that operates in the area,” among other reasons. She]

[said that there are also other businesses such as those related to drug trafficking, which try to consolidate transit routes. In this case, she called on authorities to identify and clarify this phenomenon and its possible relationship with company enterprises.]

### **How the community protects itself**

[Following the great displacements that took place in the 1990s, the communities created humanitarian and biodiverse zones as a form of protection, covered by international human rights. The communities have also taken to the Inter-American Commission on Human Rights to see their rights protected with the granting of precautionary measures. ]

[Most recently, the communities have held memory festivals as a mechanism to protect their lands, to prevent violence from various actors and to restore the areas to their original state. The next of these festivals will be the Third Festival of Memories, which will be held in Caño Manso, Curbaradó.]

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
