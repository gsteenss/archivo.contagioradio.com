Title: Universidad Distrital se moviliza para exigir mayor financiación
Date: 2016-05-06 16:30
Category: Educación, Nacional
Tags: educacion, Universidad Distrital
Slug: universidad-distrital-se-moviliza-para-exigir-mayor-financiacion
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/05/Universidad-Distrital-e1462569966410.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Las 2 Orillas] 

###### [6 May 2016] 

En rueda de prensa delegados de las diferentes facultades de la Universidad Distrital aseveraron que continuarán en paro hasta que sus exigencias sean cumplidas por el Consejo Superior y la administración distrital.

Abraham Rivera, representante estudiantil, aseguró que la crisis en la Universidad es estructural y se relaciona con déficit presupuestal, mal manejo de recursos por parte de las directivas y la elección antidemocrática del rector, que pretende imponer el Consejo Superior.                     <wbr></wbr>

A las demandas se suma la exigencia de que la malla curricular de las licenciaturas no sea modificada de acuerdo con lo estipulado por el Ministerio de Educación, pues la medida afecta la autonomía universitaria.                <wbr></wbr>

El déficit de infraestructura es tan apremiante que en una de las sedes de la Universidad, 100 estudiantes deben ver clases en salones con capacidad para 50 personas, y en días recientes parte del techo del auditorio principal de otra de las sedes, se cayó.                         <wbr></wbr>

Los estudiantes insisten en la necesidad de una reforma al Estatuto General de la Universidad que permita solucionar estas problemáticas y de la que pueda ser participe toda la comunidad universitaria. El próximo 10 de mayo la vocería estudiantil se hará presente en la Comisión Sexta del Senado y el 13 toda la institución se movilizará hacia el Concejo de Bogotá.

Vea también: [[No aparecen los vídeos que prueban agresión de ESMAD contra estudiante U Distrital](https://archivo.contagioradio.com/habrian-borrado-videos-que-pobrarian-agresion-de-esmad-contra-estudiante-de-udistrital/) ]

<iframe src="http://co.ivoox.com/es/player_ej_11481149_2_1.html?data=kpahmpaVeJqhhpywj5WbaZS1lZmah5yncZOhhpywj5WRaZi3jpWah5yncbPpxsnOjcnJb9HmxtPgw5C5b6Xd1Nnfy9nFsI6ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)]]
