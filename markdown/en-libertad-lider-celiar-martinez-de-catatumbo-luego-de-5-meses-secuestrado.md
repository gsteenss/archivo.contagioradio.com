Title: En libertad líder Celiar Martínez de Catatumbo, luego de 5 meses secuestrado
Date: 2020-02-07 17:07
Author: CtgAdm
Category: Líderes sociales, Nacional
Tags: Lider social, secuestrado
Slug: en-libertad-lider-celiar-martinez-de-catatumbo-luego-de-5-meses-secuestrado
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/02/Lider-Celiar-Martinez.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:heading {"align":"right","level":6,"textColor":"cyan-bluish-gray"} -->

###### Foto: @FuerzasMilCo {#foto-fuerzasmilco .has-cyan-bluish-gray-color .has-text-color .has-text-align-right}

<!-- /wp:heading -->

<!-- wp:paragraph -->

Este jueves 6 de febrero fue liberado en el corregimiento de Honduras, municipio de Convención (Norte de Santander) el líder comunal e integrante de la Asociación Campesina del Catatumbo (ASCAMCAT) Celiar Martínez, que el próximo 8 de febrero completaría 5 meses secuestrado por un grupo armado ilegal en el territorio.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### **"El líder está libre, con vida y en buen estado"**

<!-- /wp:heading -->

<!-- wp:paragraph -->

Según relató la Oficina del Alto Comisionado de las Naciones Unidas para los DD.HH. en Colombia, **Celiar Martínez** está libre, con vida y en buen estado. El líder fue rescatado tras una operación adelantada por el Ejército, que terminó con la captura de dos de sus secuestradores. (Le puede interesar: ["Líder social del Catatumbo, Celiar Martínez completa cinco días desaparecido"](https://archivo.contagioradio.com/lider-social-del-catatumbo-celiar-martinez-completa-cinco-dias-desaparecido/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

La Oficina agregó en su cuenta de twitter que **la práctica del secuestro "está prohibida por el derecho internacional humanitario y por el derecho internacional de derechos humanos";** al tiempo, sostuvo que continuarán acompañando a Celiar y su familia en un territorio como Catatumbo, en el que constantemente se denuncia la ausencia de garantías de derechos.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Celiar **Martínez estuvo secuestrado desde el domingo 8 de septiembre**

<!-- /wp:heading -->

<!-- wp:paragraph -->

Celiar Martínez ejercía como presidente de la Junta de Acción Comunal de la vereda Puente Azul, también desarrollaba su liderazgo como vicepresidente de la Asociación de Juntas de Acción Comunal (ASOJUNTAS) y era miembro de ASCAMCAT hace 10 años. Su secuestro se produjo el domingo 8 de septiembre en horas de la tarde, cuando hombres armados, presuntamente del ELN, lo condujeron con rumbo desconocido.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

También le invitamos a consultar: [Fernando Quintero Mena y la paz que no llegó a Catatumbo](https://archivo.contagioradio.com/fernando-quintero-mena-y-la-paz-que-no-llego-a-catatumbo/).

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78980} /-->
