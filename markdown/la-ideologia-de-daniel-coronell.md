Title: La ideología de Daniel Coronell
Date: 2019-04-22 09:59
Author: JOHAN MENDOZA TORRES
Category: Johan Mendoza, Opinion
Tags: Daniel Coronell, periodismo, Petro
Slug: la-ideologia-de-daniel-coronell
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/La-ideología-de-Daniel-Coronell.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: premioggm 

[Hoy en día hay que admitir que, a diferencia de las grandes cadenas corporativas de medios informativos, el periodismo independiente no tiene una ventanilla directa de cuotas políticas partidistas o pacto con grandes oligopolios, eso es claro, totalmente sujeto de mérito y reconocimiento.]

[No obstante, el periodismo en sí tiene en su naturaleza el problema de contener la sincronía en el lenguaje. Es decir, narra hechos que suceden en el marco de lo que se podría denominar una coyuntura, un presente ancho, grueso, cargado con el zumo más puro que lo caracteriza precisamente como presente: la espontaneidad.]

[La espontaneidad contiene un problema bastante grave, detesta la diacronía en el lenguaje, es decir, se manifiesta inconexa, sin perspectiva de pasado, carece de esa perspectiva histórica que no pretende solamente validez sino verdad; es como si rechazar el pasado, bien sea por ignorarlo o por conocerlo, pero creyendo que ha perdido validez, fuera el deporte favorito de la naturaleza misma del periodismo en Colombia.  ]

[**Daniel Coronell** es un excelente profesional, pero obedece con claridad a esa naturaleza del periodismo, que cuando se manifiesta de forma tan explícita como con el rifirrafe que tuvo con Petro, lo arroja inevitablemente, así se esfuerce en negarlo, al plano de la ideología.  ]

[La ideología desde Mannheim es pensamiento consciente y socialmente determinado, la ideología desde Marx es una falsa conciencia que opera como “verdad” negando la posibilidad de descubrir la conciencia real. Desde Gramsci la ideología es un todo orgánico encarnado en las instituciones que genera prácticas productoras de subjetividades. A propósito de subjetividades, yo no sé si Fajardo con su “post-ideología” o Coronell quien entre líneas forma parte de una capa esnobista de críticos que se muestran ante las personas como “a-políticos” o “a-ideológicos”, le hayan entregado a sus masas de seguidores y fieles escuderos alguna referencia verdadera y no solo “válida” de lo que están asumiendo como ideología, porque eso de “no a la ideología” está muy]*[cool]*[ para un trino tranquilizante, pero vista la fuerza política que ha tomado la afirmación, ya no se trata de un juego, ahora se lee como un discurso algo cliché de “ni izquierda ni derecha”, pero que peligrosamente le encarga al ego, dilatar la posibilidad de la verdad que debería ser compartida por todo el mundo no solo en forma de noticia o columna.]

> [Hablar de la ideología de Coronell no es un esfuerzo personalista, no busco generar agravios, ni sumar votos a Petro.]

[Hablar de la ideología de Coronell no es un esfuerzo personalista, no busco generar agravios, ni sumar votos a Petro. Creo que, así como el periodismo independiente por su carácter sincrónico hace llamados a la objetividad, es necesario que desde la crítica universitaria se llame la atención a ese periodismo y su pretensión de objetividad. En este punto de la historia (casi un cuarto de siglo XXI transcurrido) es un cuento muy trasnochado seguir repitiendo el slogan que los vencedores de la guerra fría llamaron “el fin de las ideologías”. No creo que sea muy ético impulsar el engaño que promueve Fajardo con ese bricolaje que llama “post-ideología” es que ¿apoyar a Uribe mientras se cultiva subjetivamente la salvación mediática-personal en el manto de la docilidad? No lo sé… esas cosas son propias de un político que con 4,8 millones de votos y un país desangrándose, se inclina a buscar la paz interior (la de él y solo él) de la que hablaba la tortuguita de Kun Fu Panda, y por tanto prefiere irse a ver ballenas.]

[Pero bueno, de Fajardo ni hablar, él es uribista; preocupa la conciencia de aquellos que lo siguen por “neutro”, pues él sí la tiene clara. Por su parte, esta entrada se trata es de Coronell, pero no para agraviarlo, sino para denotar que él también tiene una ideología, es solo que la diacronía no visita al discurso periodístico y eso le trae problemas.  ]

[La espontaneidad sincrónica del lenguaje periodístico, lo constituye muchas veces en discurso político en la medida que afecta las agendas electorales. La objetividad se escurre como agua entre las manos, cuando con un discurso o una columna se aumenta o disminuye la popularidad de los candidatos, y esto, no es algo negativo ni mucho menos. Es político.]

[La espontaneidad es volátil como la perspectiva de las supuestas ciudadanías libres, que aún consideran que votar es ir a elegir un producto en el centro comercial (a la medida “mía”).]

[La espontaneidad es injusta, porque ya algunas personas sindican a Coronell de “uribista”, eso es muy grave y sigo considerando a la fecha, que no tiene sentido práctico, porque a Coronell literalmente lo han sacado del país bajo amenaza por levantar argumentativamente la voz contra Uribe.]

[La espontaneidad es estúpida, porque incluso Vladdo ya equipara a Petro con Uribe. Luego dirá que Hitler es igual que Fidel Castro, así como así, tan campante y egocéntrico (con permiso de la ideología dominante), destilando esa viscosa subjetividad que pervierte el acercamiento a la verdad, y sobre todo, delegando arbitrariamente emociones de odio o epítetos de “fanáticos” a cualquiera que ose criticarle dicha subjetividad tan ideológica.]

[Equiparar los antagonismos, es el modus operandi de la ideología dominante que hoy existe, ese “no polaricemos” no es cualquier frase, es la frase que manifiesta desde el lenguaje la vigencia de la ideología dominante. Negar la oposición, negar la brutal diferencia que existe entre algunos políticos y otros, metiéndolos a todos en una misma bolsa, es el]*[cómodo abismo]*[ del que hablaba Baudelaire; nada más ideológico que eso.]

[¿Entonces cómo dar lectura a esta tendencia que no es para nada nueva de “ni izquierda, ni derecha” “a-ideología” “a-política”?]

[Tenemos que regresar a nuestro pasado, porque solo allí se halla el momento de la verdad que nos es arrebatado en el presente por esa ciega espontaneidad. Plinio Apuleyo Mendoza lo explica sin tanta parafernalia. En los años 30, los años 40 del siglo XX, luego de la matanza de campesinos producto de la idea fermentada y madurada de dirigentes políticos muy bien hablados, estudiados, socios de clubes y que se sabía jamás tomarían un arma, el Frente Nacional convierte la violencia en una dimensión metafísica, “no hay responsables” dicen los responsables, “no nos dividamos más” la culpa fue de “la violencia” venimos a salvar la patria.]

[Algo así como funciona hoy en día la palabra “corrupción”; es el mal absoluto, no tiene cabeza ni pies, sirve de slogan para atraer, para atacar, lo usa el que se informa y el que no se informa, el que estudia y el que no para definir “el problema de Colombia”, y fomenta la fiesta eterna de la espontaneidad, que nunca busca sino solo juzga.  ]

> [La ideología de Coronell es la que comparten todos aquellos que desde los años 90 y luego de la guerra fría, creyeron que la victoria del capitalismo-liberalismo significó por retrueque o “tastas” el fin de las ideologías.]

[La ideología de Coronell es la que comparten todos aquellos que desde los años 90 y luego de la guerra fría, creyeron que la victoria del capitalismo-liberalismo significó por retrueque o “tastas” el fin de las ideologías. No ocurrió así. Es un funesto determinismo que resulta fácil comprenderlo mediante un eufemismo algo degradado:  si dos partes dicen tener razón, pelean y gana una de las partes ¿Quién queda con la razón? claramente la parte vencedora; sería necio decir “nadie ha quedado con la razón”.]

[Una parte venció, y dicen que la historia es escrita por los vencedores; han sido los vencedores quienes acuñaron la frase “El fin de la ideología”, que es hoy prácticamente un Documento de Cultura que ha servido a muchos como Coronell quienes con la subjetividad aprobada como manifestación de la ideología que portan y aún no reconocen, avanza como falsa conciencia que ocultó y parece seguir ocultando el mensaje real: “ser neutral significa que no se tolerará la emergencia de una ideología que no sea la de los vencedores”.]
