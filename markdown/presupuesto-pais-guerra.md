Title: El presupuesto para el 2019 de Colombia es el de un país en guerra
Date: 2018-09-25 18:05
Author: AdminContagio
Category: Economía, Entrevistas
Tags: Carrasquilla, defensa, educacion, Presupuesto General de la Nación
Slug: presupuesto-pais-guerra
Status: published

###### [Foto: @Notafamousleo] 

###### [25 Sep 2018] 

Por estos días se está tramitando el **Presupuesto General para la Nación (PGN)** para el año 2019, que ya fue aprobado en primer debate conjunto de comisiones cuartas y quintas del Congreso; sin embargo, el programa es objeto de críticas porque sitúa nuevamente el presupuesto para Defensa como uno de los sectores con mayor inversión del Estado.

Para el **economista Libardo Sarmiento,** el problema inicia con que los presupuestos generales no se hacen basados en el bienestar general, sino en los intereses al interior del Gobierno, y para el caso de Colombia, se trata de grupos empresariales y de derecha que también tienen en el 85% de los apoyos en el Congreso.

Este propuesto consta de **259.000.000.000.000, que equivale al 25% de la riqueza que genera la sociedad colombiana**, no obstante, Sarmiento señaló que este dinero se invierte principalmente en el pago de la deuda que creció durante el gobierno Uribe y se mantuvo con el gobierno Santos, hecho que produjo un endeudamiento para el país equivalente al 57% del Producto Interno Bruto (PIB).

Adicionalmente, **hay un déficit fiscal del 3,6% del PIB,** y según lo dicho por el ministro de hacienda, Alberto Carrasquilla, una desfinanciación de 25 billones (es decir, el 11% del presupuesto nacional) en el **PGN** que debe ser solucionado en el Congreso. (Le puede interesar: ["La reforma tributaria de Carrasquilla afectaría al 90% de los colombianos"](https://archivo.contagioradio.com/carrasquilla-afectara-colombianos/))

### **14 billones menos a deuda para solventar la desfinanciación** 

En las comisiones económicas se decidió que en lugar de destinar 66,4 billones en deuda se usarán 52,4, lo que libero **14 billones de pesos para cubrir el hueco de 25 billones que Carrasquilla afirmó que tenía el PGN.** De los 14 billones, 10 serán destinados para inversión y los 4 restantes para el funcionamiento del Estado.

Con esta organización de la deuda, el analista económico sostuvo que Colombia emitirá nuevos bonos de deuda para resolver la desfinanciación del país. Y con los destinos del Presupuesto comprometidos, se sabría que el 14% se destinaría para inversión pública, "monto que es mínimo"; 22% cubriría la deuda; y el 64% (165,5 billones) se gastaría en el funcionamiento, "lo que lo hace un Estado ineficiente".

La parte que se destinaría para gastos de funcionamiento (4 billones) cubriría los 2,9 billones que le hacen falta al sector salud, **900.000 millones para la implementación del acuerdo de paz**, y el restante iría a financiar la rama judicial. Por el lado de la inversión (10 billones) **el sector educación se llevaría la mayor parte con 2 billones.**

El resto del rubro de inversión se repartiría entre minas y energía con 1,9 millones, lo que para Sarmiento **"evidencia la apuesta por el sector extractivo del Gobierno"**, transporte con 1,1 billones, vivienda con 565 mil millones y 500 mil millones para fortalecer las fuerzas militares. Sin embargo, el economista sostuvo que del sector defensa se estaría buscando una inversión de 1,4 billones, **hecho que convierte el PGN en un presupuesto de una nación en guerra.**

### **Presupuesto para Defensa vuelve a crecer** 

Mientras la Contraloría afirma que faltan 76 billones para garantizar la implementación del acuerdo de paz, se están limitando los recursos para que se desarrolle una reforma agraria y de tierras; hecho que se evidencia en la reducción del 13% del presupuesto para la Unidad de Tierras. Adicionalmente, los temas ambientales, de ciencia y tecnología cuentan con un presupuesto de solo 270.ooo millones.

Sin embargo, el sector defensa si ha crecido: Según Sarmiento, en el 2002 representaba el 15,4% del PGN, en 2015 era el 17,5%  mientras **para 2019 se espera que sea del 17,5%, que es el equivalente al 5% del PIB de Colombia,** y para el analista prueba que es Colombia tiene una economía de guerra. (Le puede interesar:["El que la hace la paga no aplica para el ministro Carrasquilla"](https://archivo.contagioradio.com/paga-ministro-carrasquilla/))

<iframe id="audio_28866654" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_28866654_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

 

###### [Reciba toda la información de Contagio Radio en] [[su correo][[Contagio Radio](http://bit.ly/1ICYhVU)]
