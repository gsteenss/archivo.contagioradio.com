Title: En el Ariari buscarán ser patrimonio agrícola de la nación
Date: 2017-09-01 14:28
Category: Ambiente, Nacional
Tags: ariari, explotación minera, fracking, hidrocarburos, Meta, patrimonio agrícola
Slug: en-el-ariari-exigen-ser-patrimonio-agricola-de-la-nacion
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/09/ariari-e1504294532830.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Turismo Meta] 

###### [01 Sept 2017] 

Las comunidades que habitan la región del Ariari en el Meta **continúan manifestando la necesidad de que esta zona sea protegida de las actividades de fracking**. Esta región, compuesta por 17 municipios, es considerada como la despensa alimentaria del país y alberga el 51% de la reserva subterránea de agua dulce de la Orinoquia.

Según Edgar Humberto Cruz, líder social y defensor del ambiente, esta región del país debe ser protegida porque ha sido tradicionalmente una **zona que produce una gran cantidad de alimentos** y debe ser considerada como “una región agroalimentaria para Colombia y el mundo”.

Algunos habitantes de municipios como El Castillo, Guamal, El Dorado, Granada, San Martin entre otros, han manifestado en varias ocasiones que **es necesario que el Gobierno Nacional les brinde garantías para crear proyectos sustentables** independientes a las de la extracción de petróleo. Sin embargo, con proyectos como Trogón de Ecopetrol en Guamal, ha habido una afectación significativa en los territorios. (Le puede interesar: ["Ambientalistas colombianos se unen contra el fracking"](https://archivo.contagioradio.com/colombia_contra_fracking/))

**En el Área de Manejo Especial de la Macarena hay 14  licencias ambientales para la exploración y explotación de hidrocarburos**

Cruz indicó que el proyecto minero energético impulsado por grandes empresas petroleras como Ecopetrol en esta región del Meta, **está afectando a las familias que por tradición han basado su economía en los cultivos de yuca**, plátano y diversos frutos.

De igual manera, algunas de las licencias ambientales sobre **14 bloques petroleros se basan en la perforación por medio del fracking**, esto “va a afectar las reservas subterráneas de agua dulce y de manera irreversible la sostenibilidad de los proyectos agropecuarios”. Hay además intenciones de crear hidroeléctricas y proyectos que tienen relación con la explotación de hidrocarburos. (Le puede interesar: ["Ministerio de Ambiente da vía libre al fracking"](https://archivo.contagioradio.com/colombia_contra_fracking/))

De manera enfática el ambientalista afirmó que “**hay empresas que se cobijan con el TLC y demandan al gobierno cuando se reversan los bloques petroleros”**. Las comunidades han visto como en sus territorios “las industrias cuentan con el respaldo político y no dan prioridad al desarrollo integral agrícola como alternativa a la política minero energética”.

Finalmente, Cruz recordó que **“en la campaña presidencial Juan Manuel Santo había prometido a protección de la zona por ser tan rica en recursos ambientales** sin embargo esto no ha sido así”. Los diferentes municipios alistan procesos de consulta popular exigiendo el derecho que tienen para manifestarse y participar en las decisiones que se tomen sobre su territorio.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
