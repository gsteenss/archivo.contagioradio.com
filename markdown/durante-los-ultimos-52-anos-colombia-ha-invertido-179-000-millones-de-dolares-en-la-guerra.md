Title: Durante los últimos 52 años Colombia ha invertido 179.000 millones de dólares en la guerra
Date: 2016-09-06 21:36
Category: Entrevistas, Paz
Tags: Acuerdos de paz en Colombia, costo de la guerra en Colombia, Presupuesto para defensa en Colombia
Slug: durante-los-ultimos-52-anos-colombia-ha-invertido-179-000-millones-de-dolares-en-la-guerra
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/09/Fuerzas-Militares.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: flickr] 

###### [06 Sep 2016] 

El costo total para financiar la guerra en Colombia, **durante los últimos 52 años, ha sido de aproximadamente 179.000 millones de dólares**, según la investigación realizada por el ex viceministro de minas, Diego Otero. Un monto que posiciona a la nación en la lista de los 10 países que más invierten en la guerra.

**Este cálculo solo incluye el dinero utilizado por el Estado colombiano**, sin contar lo que otros actores armados como las FARC–EP, el ELN o los grupos paramilitares han destinado para la guerra, una inversión que durante estas décadas [podría haber solventado necesidades de sectores como la salud](https://archivo.contagioradio.com/comision-de-la-verdad-debe-esclarecer-la-finalidad-historica-de-los-agentes-de-la-guerra/), la educación o la economía, los cuales actualmente afrontan un déficit presupuestal.

Frente a la Fuerza Armada que mayor inversión tiene en Colombia, Otero plantea que es el **Ejército Nacional, que actualmente cuenta con 250 mil hombres en sus filas**, posicionándose como el segundo ejército más grande del continente, después de Brasil. Si se compara el monto total del costo de la guerra en Colombia, no se equipara con el gasto del [Plan Marshall otorgado a Europa en el año 2014, por una suma de 42.000 millones de dólares](https://archivo.contagioradio.com/los-costos-de-la-guerra-la-carcel-y-la-paz/).

Fue así como a partir de la década de los 90 hubo un aumento de recursos para la guerra, en un momento en el que se recrudeció el conflicto armado al interior del país. Cifra que volvió a aumentar durante la presidencia de Andrés Pastrana, **que se acrecentó en los dos periodos de Gobierno de Álvaro Uribe Vélez** y que se ha mantenido durante el mandato de Juan Manuel Santos.

"Yo espero con mi argumento que como finaliza el conflicto con las FARC, se deba disminuir el gasto en compra de equipos, de combustible, es decir poco a poco **ir dirimiendo el gasto militar**, para tener recursos y poderlos invertir en otras cosas", afirma el investigador.

La investigación completa está en el libro titulado "Gasto de guerra en Colombia: 1964 -2016, 179.000 millones perdidos" que se lanzará el próximo jueves, en la Calle 93b \#21-42, a las 6:00 de la tarde.

<iframe src="https://co.ivoox.com/es/player_ej_12815580_2_1.html?data=kpelk5qZfJGhhpywj5aZaZS1lZWah5yncZOhhpywj5WRaZi3jpWah5yncaXdxszcjbTYqdPjhpewjarcb7fdxMray9PNt9Xm0JDRx5DRrc_V1JKSmaiRh9Di1cbUy9SPlsLYytSYj4qbh46o&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en[ [su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)]] 
