Title: 1500 palestinos presos por Israel se declaran en huelga de hambre
Date: 2017-04-17 15:27
Category: El mundo, Otra Mirada
Tags: Huelga de hambre, Israel, presos
Slug: presos-palestinos-israel
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/04/rkt-i_fCg_930x525.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### Foto:EFE 

##### 17 Abr 2017 

Más de **1500 presos palestinos detenidos en cárceles israelíes iniciaron una huelga de hambre masiva indefinida**, como forma de exigir mejoras en las condiciones en que se encuentran y en contra de las acciones de las autoridades carcelarias que vulneran sus derechos humanos.

De la protesta, que inició a media noche del lunes coincidiendo con la conmemoración del Día Internacional de Solidaridad con los presos palestinos, **participan todas las facciones políticas**, teniendo como principal impulsor al prisionero político **Marwán Barghouti**, líder de Al Fatah que desde hace 15 años cumple 5 cadenas perpetuas por su participación en la Segunda Intifada.

Algunas de las exigencias de los prisioneros, se relacionan con el **régimen de visitas familiares**, para que se permita a los niños visitar a sus padres sin acosos, el fin de las **negligencias médicas**, la apertura de los hospitales a los presos políticos, la **liberación de los presos enfermos**, el fin de la llamada **detención administrativa** sin acusación formal por la cual más 500 personas se encentran recluidas, o las políticas de aislamiento como método de presión o castigo.

Organizaciones humanitarias cómo el **Club de prisioneros palestinos** y **Estrella Palestina**, se han solidarizado con la huelga haciendo un llamado a todos los palestinos a **unirse en apoyo al "Movimiento cautivo",** extendiéndolo a entidades como la ONU, Amnistía Internacional, el Cómite Internacional de la Cruz Roja y a los gobiernos del mundo a quienes invita a luchar en favor de las demandas de los prisioneros.

Desde la dirigencia carcelaria Israelí, la respuesta ha sido acudir a sanciones disciplinarias como el traslado de huelguistas a galerías y medidas como la alimentación forzada a prisioneros en peligro de enfermarse por inanición. Por su parte el Ministro de Seguridad Interior, afirma que la protesta "está motivada por la política interna palestina”, mostrándola como un movimiento táctico de Barguti para ganar protagonismo de cara a una futura aspiración presidencial.

En la actualidad en las cárceles israelíes en territorios ocupados, se encuentran **6.500 personas detenidas en 22 centros carcelarios**, incluyendo a **300 menores de edad**, **62 mujeres y 13 diputados del Consejo Legislativo**. De acuerdo con el registro de organizaciones de apoyo a los internos entre los detenidos hay **23 enfermos en estado terminal**. Le puede interesar: [Lucha contra el aparheid de Israel a Palestina se toma Bogotá](https://archivo.contagioradio.com/lucha-apharteid-palestina/).

**Liberación de Lena Jerboni**

Un día antes de la conmemoración que se realiza desde 1974, **Lena Jerboni,** prisionera desde el año 2002 bajo la acusación de ser colaboradora de los movimientos de 15resistencia palestina, **fue liberada luego de completar 15 años de condena en una cárcel sionista**, convirtiéndose se en la sentencia más larga para una mujer en este tipo de centro.

Durante el tiempo de su confinamiento, **Jerboni se encargó de ayudar a las mujeres retenidas en temas como la educación y la salud**, supliendo las obligaciones incumplidas por las autoridades israelíes, quienes no garantizan el acceso a los derechos básicos para la población carcelaria.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
