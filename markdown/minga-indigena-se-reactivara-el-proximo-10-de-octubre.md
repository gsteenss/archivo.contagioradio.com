Title: Minga indígena se reactivará el próximo 10 de Octubre
Date: 2020-10-02 18:47
Author: AdminContagio
Category: Actualidad
Tags: CRIC, Minga Indígena, Movilización Indígena, Vía Panamericana
Slug: minga-indigena-se-reactivara-el-proximo-10-de-octubre
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/10/Indigenas-via-panamericana.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

Este jueves, **indígenas de la comunidad Misak salieron a manifestarse sobre la Vía Panamericana**, a la altura de La María, en el municipio de Piendamó, Cauca; **por el incumplimiento del Gobierno, que según ellos,  no envió a sus delegados para adelantar los diálogos en la Mesa de Negociación** que se citó para tratar las diversas problemáticas sociales que enfrentan los indígenas. (Lea también: [Otra Mirada: Guerra y abandono estatal, el yugo contra indígenas en Nariño](https://archivo.contagioradio.com/otra-mirada-guerra-y-abandono-estatal-el-yugo-contra-indigenas-en-narino/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

La comunidad esperaba que llegaran representantes de los Ministerios de Hacienda, Cultura, del Interior y la Agencia Nacional de Tierras; pero **al no concretarse el arribo de los delegados,  los indígenas decidieron salir a protestar sobre la vía.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Líderes indígenas manifestaron que el Estado está desconociendo sus reclamos frente a la fumigación aérea y las acciones de tutela que han presentado al respecto. Además, pidieron garantías para la defensa de los derechos humanos en su territorio.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Una de las mayores demandas de los indígenas tiene que ver con garantías de seguridad para sus comunidades y el cese de la violencia en sus territorios; frente a esto, **los representantes de más de 124 cabildos del Cauca, hablaron con el Defensor del Pueblo,  Carlos Camargo** **y le presentaron sus preocupaciones en relación con este tema.** (Lea también: [El Pueblo Awá lanza un S.O.S contra la guerra en Nariño](https://archivo.contagioradio.com/el-pueblo-awa-lanza-un-s-o-s-contra-la-guerra-en-narino/))

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/RedVidaDHCauca/status/1311733109910536192","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/RedVidaDHCauca/status/1311733109910536192

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

Asimismo, líderes del Consejo Regional Indígena del Cauca  -[CRIC](https://www.cric-colombia.org/portal/)-, anunciaron que empezarán movilizaciones de manera masiva el 5 de octubre y aseguraron que estas se realizarán sobre la Vía Panamericana;  señalando que prefieren «*morir en las vías, a seguir esperando que el Gobierno les cumpla con los temas de tierras, educación y salud*». También aseguraron que de no tener respuestas por parte del Gobierno iniciarán  marchas desde el Cauca hasta Cali y luego hacia la ciudad de  Bogotá.  

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Indígenas anuncian la reactivación de la Minga del suroccidente colombiano

<!-- /wp:heading -->

<!-- wp:paragraph -->

Estas manifestaciones se plantean como la antesala de una gran Minga Indígena que se movilizará con el objetivo de reivindicar los derechos, no solo de esas comunidades étnicas, sino de todos los habitantes de Colombia.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El actual senador y exconsejero del CRIC, Feliciano Valencia, se sumó al llamado y **convocó la Minga para el próximo 10 de octubre; donde organizaciones del suroccidente se movilizaran en defensa del territorio, la vida y la paz.**

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/FelicianoValen/status/1310901591017435136","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/FelicianoValen/status/1310901591017435136

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:block {"ref":78955} /-->
