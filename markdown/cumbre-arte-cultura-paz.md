Title: 38 experiencias se dan cita en la Cumbre Nacional Arte y Cultura para la paz
Date: 2018-07-31 11:33
Category: Cultura, Otra Mirada
Tags: Cultura, memoria, teatro
Slug: cumbre-arte-cultura-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/07/Antigone_-085-e1500918041113.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Archivo 

###### 31 Jul 2018 

La Corporación colombiana de Teatro, junto a 25 organizaciones sociales, académicas y culturales del país, con el apoyo del Ministerio de cultura, realizarán **del 2 al 5 de agosto** la **Cumbre Nacional de Arte y Cultura por la Paz, la reconciliación y la convivencia**. Una vitrina para **38 experiencias transformadoras de paz**, locales, nacionales e internacionales en voz de sus protagonistas.

Canciones y videos de paz, poemas, narraciones orales, se tomarán durante cuatro días escenarios como el Teatro Colón, Fundación Gilberto Alzate Avendaño, Universidad Jorge Tadeo Lozano, Teatro la Candelaria y Sala Seki Sano. En cada intervención, el expositor o expositora podrá relatar **cómo desde su apuesta artística, logró transformar imaginarios marcadas por el conflicto, en experiencias de Paz**.

Serán **más de 100 los invitados, entre líderes sociales y culturales, protagonistas de proyectos culturales y artísticos** que con sus propuestas contribuyen a reconstruir el tejido social causado por el desafecto y la violencia, conformadas en **20 experiencias nacionales, 15 distritales y 3 internacionales**, incluyendo la participación especial de Bertha Zuñiga de Honduras y Perla de la Rosa desde Ciudad Juárez en México.

Algunas de las experiencias nacionales participantes, son el **Festival de teatro Selva Adentro del Chocó**, la historia del **colegio del Cuerpo** con Álvaro Restrepo, la **Fundación Arte y Parte** que trabaja la música con población infantil vulnerable, las experiencias en artes plásticas y artesanías que realizan ex combatientes de las FARC, la **Fundación Nacional Batuta** con el poder trasformador de la música, **La Fundación PLAN** que presenta el trabajo de investigación y recopilación de memoria sonora, realizado por César López y otros músicos.

Se suman a estas, la experiencia de 20 años trabajando con víctimas desde el performance de **Patricia Ariza**, la **Corporación Personas con Capacidades Diversas**, el grupo de creación en performance que mediante prácticas artísticas, generan formas de re-existencia para las mujeres, la historia de las **Antígonas**, madres de Soacha y **sobrevivientes del genocidio de la Unión Patriótica**.

También participarán a través de charlas magistrales, reconocidos personajes de la escena académica y social, como el maestro **Sergio de Zubiria** y la investigadora **Yolanda Sierra León,** en** **un encuentro social, académico, popular y cultural para que se visibilice el trabajo de hombres y mujeres que se han empeñado en sacar este país de la guerra desde la cultura.

**Polifonías, Pluralidad de voces**

Durante los días de Cumbre, se presentarán **21 polifonías de corta duración** como el fragmento de la obra **“El evangelio según María”**, dirigida por Juan Carlos Moyano, danza a cargo de Álvaro Restrepo y sus bailarines, intervenciones musicales como la de **Diana Avella**, cantante de Hip Hop, narración oral por **Misael Torres**,** **cuadros artísticos de mujeres ex combatientes, performances y cantos.

**De inicio a fin el arte vivo en escena**

La inauguración será el **jueves 2 de agosto en el Teatro Colón**, con la intervención de la Vicepresidencia de la República, el Ministerio de cultura, Monseñor Héctor Fabio Henao presidente del Comité Nacional del Consejo Nacional de Paz, Reconciliación y Convivencia y la maestra Patricia Ariza.

Además, las presentaciones de la **Orquesta Juvenil de la Paz**, maestro José Arroyo, **la Tremenda Revoltosa** batucada feminista de **Ochy Curiel**, los músicos **José Edier Solis**, de Buenos Aires, Cauca; **Cesar López, Adriana Lizcano, Roxana Pineda, Dayra Quiñones, Edson Velandia** y el poeta **Juan Manuel Roca**.

Para el cierre de la Cumbre, **el domingo 5 de agosto en la Fundación Gilberto Alzate Avendaño** con entrada abierta al público general hasta completar aforo, se realizará un **Concierto por la Paz y por la Vida de los líderes y lideresas sociales**, que sellará  este gran encuentro de saberes y experiencias por la paz.

<iframe id="doc_33697" class="scribd_iframe_embed" title="Cumbre Nacional Arte y Cultura 2018" src="https://www.scribd.com/embeds/385117579/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-bSzRwkVzByzfSoCBEd1W&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="0.7179282868525897"></iframe>  
<iframe id="audio_27395363" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_27395363_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
