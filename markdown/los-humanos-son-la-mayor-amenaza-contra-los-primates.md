Title: Los humanos son la mayor amenaza contra los primates
Date: 2015-11-24 14:21
Category: Voces de la Tierra
Tags: 24 de noviembre, animales en vías de extinción, Charles Darwin, Christoph Schweitzer, el humano y los primates, Madagascar, Orgullo primate, Sociedad Zoológica de Bristol, Tití Cabeciblanco
Slug: los-humanos-son-la-mayor-amenaza-contra-los-primates
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/11/cotton-top-tamarin-saguinus-oedipus5.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: [monotiti.amerisol.com]

###### [24 Nov 2015]

Este 24 de noviembre se celebra el Día mundial del Orgullo Primate, una fecha en donde se recuerda la publicación de la obra maestra de Charles Darwin  "El origen de las especies",  además se conmemora  el descubrimiento de Lucy Australopithecus, el esqueleto de una primate que comprobaría la teoría respecto a la evolución humana que señala que los hombres y mujeres provienen de los primates

Adicionalmente, en el marco de este día se hace un reconocimiento a todos los conservacionistas que trabajan en preservar estas especies animales. Es así, como este día se evidencian cifras alarmantes frente a las especies de primates que se encuentran en vías de extinción.

De acuerdo a expertos de la Sociedad Zoológica de Bristol, en Gran Bretaña, de las 703 especies  y subespecies de primates  censados en el mudo la mitad de ellos están a punto de extinguirse. Según el primatólogo **Christoph Schweitzer  **el orangután de indonesia, el ¨vareci rubra¨ de Madagascar, el mono araña y el mono Tití Cabeciblanco de Colombia, son los más amenazados.

Algunas de las razones por la que estos, están a punto de extinguirse es la destrucción de su hábitat, principalmente por incendios, la deforestación en bosques tropicales y la explotación de la madera, así como por la caza para el consumo o el comercio ilegal de estos animales.

Organizaciones protectoras de los animales trabajan para proteger estas especies a través de la sensibilización social, con colaboraciones de las comunidades locales y la gestión ambiental.
