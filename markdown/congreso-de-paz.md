Title: Se lanza el Congreso Nacional de Paz en Colombia
Date: 2017-04-04 12:42
Category: Nacional, Paz
Tags: Acuerdos de La Habana, Congreso de Paz, Mesa en Quito
Slug: congreso-de-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/04/WhatsApp-Image-2017-04-04-at-12.35.39-PM.jpeg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/FIRMA-DEL-ACUERDO-DE-PAZ-6.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: German Moreno] 

###### [04 Abril 2017] 

Con la participación de diversas organizaciones, colectivos y sectores de la sociedad se lanzó el Congreso Nacional de Paz, una iniciativa que tiene como propósito generar una apropiación social de los Acuerdos de Paz, hacer **veeduría de su implementación y apoyar las conversaciones de paz entre el ELN y el gobierno Nacional**.

Este Congreso se desarrollará a través de dos espacios: **el primero será uno regional que se realizará el próximo 27 de abril, en 20 regiones del país y 6 zonas veredales** transitorias de normalización, en las que se recogerán las propuestas, experiencias e iniciativas que se enfoquen en encontrar las garantías presentes y futuras, para la construcción de paz. Le pude interesar: ["Congreso de Paz una iniciativa para proteger los Acuerdos de Paz"](https://archivo.contagioradio.com/congreso-de-paz-una-iniciativa-para-proteger-los-acuerdos-de-paz/)

**El segundo espacio se hará el 29 de abril, en el Congreso de la República**, allí llegarán todas las propuestas regionales. Se espera que a partir de estos encuentros se logren crear las **Veedurías Regionales Permanentes de Seguimiento a la Implementación y Cumplimiento** de los acuerdos hechos con las FARC-EP y la agenda del proceso de paz con el ELN.

De acuerdo con la representante por la Alianza Verde, Ángela María Robledo, este también es un escenario de convergencia que le apuesta a un cambio generacional “los jóvenes han hecho una tarea muy importante y deben sostenerla, Paz a la Calle, Ojo a la Paz, **fueron dos movilizaciones de voces de jóvenes y finalmente son ellos la generación de la paz**”. Le puede interesar:["Abraza la Paz, una iniciativa de los estudiantes de Colombia para la reconciliación"](https://archivo.contagioradio.com/abraza-la-paz-una-iniciativa-de-los-estudiantes-de-colombia-para-la-reconciliacion/)

De igual modo, el Senador Alberto Castilla, señaló que en este espacio **“las propuestas traídas desde abajo desde las bases son las que tienen que hablar”** y añadió que ejemplo de ello serán las propuestas que tiene el campesinado de Colombia en torno a los planes de sustitución de cultivos ilícitos, como parte del punto 4 de los acuerdos de paz.

### **Los retos del Congreso de Paz** 

Uno de los primeros retos que tendrá que enfrentar este nuevo escenario es **lograr articular a la gran cantidad de personas comprometidas con la defensa de los acuerdos de paz y la implementación** en el congreso de la república, dado que este espacio es fruto de un proceso en que han aparecido el Frente Amplio por la paz y otros, que no han logrado pesar lo suficiente para presionar al gobierno.

Por otra parte, tendrán que enfrentar los intereses electorales de la actual campaña presidencial 2018 y establecer las bases de un posible posicionamiento en un nuevo gobierno que podría ser de transición, si se adopta la estrategia que logre que los **jovenes que han participado en espacios como Paz a la Calle y Ojo a la Paz, se identifiquen** y encuentren que esos espacios realmente son alternativas a las llamadas "dinastías" que han gobernado el país.

###### Reciba toda la información de Contagio Radio en [[su correo]
