Title: Continúa protesta de guardias carcelarios en Guatemala
Date: 2015-09-11 17:59
Category: El mundo, Movilización, Política
Tags: CICIG, Escándalo de corrupción Guatemala, Guatemala, Iván Velásquez, La Línea, Otto Pérez Molina, Protesta de guardias carcelarios, Rosanna Baldetti
Slug: continua-protesta-de-guardias-carcelarios-en-guatemala
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/guardias_guatemala_contagioradio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: prensalibre 

###### [11 Sep 2015] 

Continúan las protestas por parte de **guardias de 22 prisiones guatemaltecas, con el fin de que se mejoren las condiciones salariales; cerca de cuatro mil guardias se reúnen a la salida de las cárceles** impidiendo el traslado de los prisioneros a los tribunales para los juicios e impidiendo la entrada de visitas a los prisioneros.

**Los líderes de la protesta aseguran que su salario debe mejorar,** pues mensualmente reciben 400 quetzales (179 dólares). Es decir, exigen un aumento de 70 dólares mensuales  para devengar un salario de  259 dólares. En una protesta similar hace dos meses aproximadamente el **gobierno les había prometido un bono económico con el fin de compensar el salario establecido**. Sin embargo el incumplimiento de lo establecido,  provoco nuevamente la huelga de los guardias.

Por su parte el viceministro de gobernación comunicó a los medios que se está  en  proceso de negociación, pero los guardias aseguran que **no van a dar el brazo a torcer con sus exigencias hasta que no se  cumplan los acuerdos, es decir que la protesta continuará**.

Otras denuncias  realizadas por los guardias tienen que ver con que en la prisión Santa Teresa **algunas de las reclusas reciben tratos exclusivos, como el ingreso  de televisores y electrodomésticos a la prisión**. Cabe recordar que allí está recluida la ex vicepresidenta  **Roxana Baldetti** quien sería beneficiaria  de estos privilegios.

Los guardias denuncian  al director del Sistema penitenciario por maltrato y amenazas, anunciando “*si denunciamos nos dan de baja*” indicó una de las guardias que cubría su rostro con un pasa montañas para no ser reconocida

Baldetti fue acusada de corrupción junto con 12 funcionarios y el presidente Otto Perez implicados en el **escándalo aduanero “la línea”**, hechos por los que están detenidos en prisión preventiva.

Este hecho se  suma a las jornadas de movilización de la ciudadanía y al proceso electoral que ha sido señalado como ilegitimo, pues cerca de los más del **40 % de los candidatos  son investigados  por delitos de  corrupción y asesinato.**
