Title: JEP negó, en segunda instancia, tutela presentada por el Coronel (r) Plazas Acevedo
Date: 2020-07-17 08:50
Author: PracticasCR
Category: Actualidad, Nacional
Tags: Coronel Plazas Acevedo, Jaime Garzon, JEP, Jorge Eliecer Plazas Acevedo
Slug: jep-nego-en-segunda-instancia-tutela-presentada-por-el-coronel-r-plazas-acevedo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/07/Plazas-Acevedo-JEP.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

La Jurisdicción Especial para la Paz -[JEP](https://twitter.com/JEP_Colombia)- negó en segunda instancia la acción de tutela presentada por el Teniente Coronel (r) Jorge Eliécer Plazas Acevedo en la que exmilitar alegaba una presunta vulneración de sus derechos fundamentales a la libertad personal y al debido proceso. (Le puede interesar: [La JEP le cierra la puerta a Salvatore Mancuso](https://archivo.contagioradio.com/la-jep-le-cierra-la-puerta-a-salvatore-mancuso/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Plazas Acevedo pretendía dejar sin efectos la decisión tomada por la Sección de Apelación de la JEP, mediante la cual se había negado su sometimiento a esa jurisdicción por cuatro secuestros extorsivos contra Benjamín Khoudari, Wilson Martínez Quiroga, los esposos Luis Antonio Castro y Enid Ortiz y Martha Cecilia Velásquez.  

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Además la JEP decidió descartar la posibilidad de tener un beneficio de la libertad transitoria condicionada y anticipada, lo cual, también había sido solicitado por Plazas Acevedo en su tutela.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Coronel (r) Plazas Acevedo es procesado en la JEP por otros actos delictivos

<!-- /wp:heading -->

<!-- wp:paragraph -->

Si bien, la JEP negó el sometimiento del coronel (r) Plazas Acevedo por los cuatro secuestros mencionados, el Tribunal de Paz aceptó conocer bajo su jurisdicción los crímenes de asesinato, contra el humorista Jaime Garzón, Mario Calderón y Elsa Alvarado, investigadores del CINEP y la masacre de Mapiripán, Meta en los que también estuvo involucrado el exmilitar. (Lea también: [Las investigaciones que siguen por el magnicidio de Jaime Garzón](https://archivo.contagioradio.com/las-investigaciones-magnicidio-jaime-garzon/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

La razón de la JEP para no vincularlo por los cuatro secuestros fue que estos delitos, según este Tribunal,  no estarían directamente relacionados con el conflicto armado y tenían fines extorsivos relacionados con delitos comunes, los cuales exceden la competencia de la jurisdicción de paz.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

No obstante, la JEP dejó la puerta abierta al sometimiento pleno de Plazas Acevedo a sus jurisdicción manifestando que: «si en un futuro, el solicitante aporta verdad plena y detallada en los casos por los cuales fue admitido a la JEP \[…\] e igualmente demuestra la existencia de un vínculo entre los cuatro secuestros extorsivos y el conflicto armado interno, su admisión plena a la JEP podrá ser reexaminada».

<!-- /wp:paragraph -->
