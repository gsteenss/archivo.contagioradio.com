Title: Informe revela la violencia estatal-paramilitar en Viotá, Cundinamarca
Date: 2019-12-10 16:14
Author: CtgAdm
Category: DDHH, Entrevistas
Tags: comision de la verdad, Cundinamarca, Desplazamiento forzado
Slug: informe-revela-la-violencia-estatal-paramilitar-en-viota-cundinamarca
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/Informe-Viotá.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Corporación Jurídica Yira Castro] 

Este 10 de diciembre se hizo entrega a la Comisión para el Esclarecimiento de la Verdad del informe **“La tierra para el campesino, es como el agua para los peces: memorias frente a la violencia estatal-paramilitar en Viotá, Cundinamarca (1989-2004)**”, documento que tiene como eje temático las violaciones a los derechos humanos cometidas por la entonces guerrilla FARC-EP, y la escalada de agresiones entre 2003 y 2004 derivada de la alianza entre Fuerzas Militares y grupos paramilitares en la región.

Según Sara Dávila Mesa, abogada de la Corporación Jurídica Yira Castro, Viotá fue históricamente un municipio  con antecedentes de violencia y presencia de las FARC, sin embargo, desde el 2003 se da una escalada de la violencia debido a la instauración de la política de seguridad democrática y el ingreso de las **Autodefensas Campesinas del Casanaré y la presencia del Batallón Aerotransportado N° 28 Batallón Colombia adscrito a la Décimo Tercera Brigada,** que fue también uno de los principales actores de violaciones de DD.HH.

La abogada señala que aunque los sucesos se dieron en cercanías a Bogotá, estos no han alcanzado la visibilidad suficiente debido al miedo que representa para los pobladores hablar al respecto. Muestra de su impacto y trascendencia, el informe revela cómo el desplazamiento forzado fue usado como una manera de control social y territorial afectando a 7221 personas que dejaron atrás sus veredas hasta llegar al casco urbano del municipio.

A su vez, Viotá, tierra reconocida por ser uno de los territorios donde más se defendieron las iniciativas agrarias campesinas, vivió una persecución contra líderes campesinos, según antiguos habitantes del municipio que constituyeron el Partido Comunista y la Guardia Roja de los campesinos, cerca de 80 de sus compañeros fueron desaparecidos, asesinados o encarcelados.

El informe que recoge información de 1989 a 2004, abarca patrones de macrocriminalidad que dan cuenta de la frecuencia con que ocurrieron los hechos, la época en la que se dieron, el actuar de los responsables y las personas que fueron víctimas de estas violaciones de DD.HH, en su mayoría, lideres y lideresas comunales del campesinado. Cabe resaltar que estos hechos no solo ocurrieron en Viotá, sino que también se presentaron en Tequendama o Sumapaz. [(Le puede interesar: Por primera vez, informes de población LGBTI llegan a una Comisión de la Verdad](https://archivo.contagioradio.com/por-primera-vez-informes-de-poblacion-lgbti-llegan-a-una-comision-de-la-verdad/))

### Comunidad de Viotá teme que se repitan los hechos

A su vez, la abogada afirma que en la población existe un temor recurrente que impide que los habitantes de Viotá se sientan a salvo con la presencia de la Fuerza Pública, pues incluso para 2019, casi quince años después de los hechos ocurridos, es un municipio que continúa militarizado, "no están exentos de que la situación vuelva a ocurrir, existe mucha desconfianza de que estos hechos se vuelvan a repetir", explica. [(Lea también: Informes sobre despojo en el Urabá y Bajo Atrato llegarán a la Comisión de la Verdad)](https://archivo.contagioradio.com/informes-sobre-despojo-en-el-uraba-y-bajo-atrato-llegaran-a-la-comision-de-la-verdad/)

Sumado a este incertidumbre es necesario exaltar que en septiembre de este año, líderes y lideresas del municipio fueron nuevamente objeto de hostigamientos, señalamientos y amenazas  a través de un  panfleto atribuido a las Águilas Negras que "advierte el inicio de un nuevo período de limpieza social" en Viotá.

Pese al panorama actual, Sara Dávila concluye que para la población, **"la Comisión de la Verdad es una voz de esperanza y una oportunidad para esclarecer los hecho y las responsabilidades colectivas e individuales tanto del Estado como de grupos armados ilegales"**.  [(Le recomendamos leer: Comisión de la Verdad se convierte en una esperanza para organizaciones del Caribe)](https://archivo.contagioradio.com/comision-de-la-verdad-se-convierte-en-una-esperanza-para-organizaciones-del-caribe/)

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
