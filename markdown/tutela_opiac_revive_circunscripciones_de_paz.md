Title: Revivieron las circunscripciones de paz por cuenta de tutela de OPIAC
Date: 2017-12-12 15:00
Category: Nacional, Paz
Tags: acuerdo de paz, Circunscripciones de paz, Efraín Cepeda, Senado, víctimas
Slug: tutela_opiac_revive_circunscripciones_de_paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/12/opiac-e1513098297180.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: EFE] 

###### [12 Dic 2017] 

[Una tutela de la Organización de Pueblos Indígenas de la Amazonia Colombiana (OPIAC), revivió las circunscripciones especiales de paz. El juzgado 16 ordenó al presidente del Senado, **Efraín Cepeda, enviar la reforma constitucional para que sea firmada y promulgada por la presidencia** de la República.]

Robinson López, uno de los dirigentes de la OPIAC, asegura que se trata de una decisión que **"le da la razón las víctimas del conflicto armado, quienes necesitan ser reparadas y escuchadas,** pero estaban quedando por fuera, sin poder participar y tener una vocería legítima en el parlamento colombiano". (Le puede interesar: [Tres opciones para las circunscripciones de paz)](https://archivo.contagioradio.com/aprobacion-circunscripciones-de-paz/)

Según la OPIAC al impedir la aprobación de las curules para las víctimas se estaban  violando los derechos fundamentales a la paz, a la participación, a la vida económica, política, administrativa y cultural de la Nación, a la representación política y al debido proceso administrativo.

### **Medida cautelar para activar las 16 circunscripciones de paz** 

Catalina Díaz Vargas, es la jueza 16 administrativa que decidió adoptar medidas cautelares y ordenar al presidente del Senado que envíe el texto del acto legislativo a la Presidencia para su sanción y análisis ante la Corte Constitucional. **Ante dicho mandato, **[**Efraín Cepeda anunció en la tarde de hoy martes que no acatará la sentencia del tribunal,**  con ello se abre nuevamente el debate entre el Ministro del Interior, Guillermo Rivera y el gobierno contra el presidente del Senado. (Le puede interesar: [Víctimas interponen tutela a favor de las circunscripciones de paz)](https://archivo.contagioradio.com/tutela_circunscripciones_paz_victimas_crimenes_estado/)]

La jueza argumentó que "La determinación del quórum y las mayorías debe establecerse con base en el número total de integrantes de la respectiva corporación** **fijado en la Constitución, cifra a la que deben restarse las curules que no pueden ser remplazadas...la mayoría absoluta es el número entero inmediatamente superior a la mitad de los votos de los integrantes de la respectiva corporación. Así las cosas, **para el caso concreto de la consulta**, **la mayoría absoluta de 99 es 50**".

Con ello, ahora la tutela necesita un fallo de fondo, pero la medida reactiva las circunscripciones. Por su parte, las comunidades esperan que los jueces sean garantistas de los derechos de las víctimas del conflicto armado, "esa es la apuesta**, la OPIAC representa 6 departamentos 56 pueblos indígenas que no deben ser olvidados**. Es importante y trascendental para los colombianos y que las víctimas puedan ser el centro del acuerdo de paz", concluye López.

###### Reciba toda la información de Contagio Radio en [[su correo]
