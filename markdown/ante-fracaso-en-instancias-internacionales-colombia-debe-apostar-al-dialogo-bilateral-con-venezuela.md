Title: Ante fracaso en instancias internacionales Colombia debe apostar al diálogo bilateral con Venezuela
Date: 2015-09-02 16:01
Category: Nacional, Política
Tags: colombia, Corte Penal Internacional, crisis fronteriza, frontera colombo - venezolana, gobierno de Nicolás Maduro, OEA, Presidente Juan Manuel Santos, unasur, Venezuela
Slug: ante-fracaso-en-instancias-internacionales-colombia-debe-apostar-al-dialogo-bilateral-con-venezuela
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/maxresdefault.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: [www.youtube.com]

<iframe src="http://www.ivoox.com/player_ek_7764149_2_1.html?data=mJyjlpaYfY6ZmKiak5iJd6KompKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRhc%2FoxpDT1MbHpdTjjMrbjc7St9XVz8jWw9iPrc%2Foxtfbw8jNs8%2FVzcrgjajTsNDhw87OjcnJpo6ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [César Torres, analista internacional] 

Ante el fracaso diplomático de Colombia en la Organización de Estados Americano, OEA, donde se buscaba convocar una reunión de cancilleres para abordar la crisis de la frontera colombo-venezolana, el analista internacional César Torres del Río, asegura que esta situación debe manejarse **“inmediatamente en términos bilaterales con Venezuela”.**

**“Colombia ha perdido política y diplomáticamente, el discurso del embajador colombiano fue desalentador y mostró falta de olfato político**", afirmó el analista, quien agregó que se perdió la oportunidad de una gestión política y diplomática internacional, tanto en la OEA como en la UNASUR, donde también el país saldría perdiendo.

De acuerdo con el último informe de la **OCHA desde el 22 de agosto, 1.097 colombianos han sido deportados y 9.826  personas adicionales han regresado a Colombia.** Esto implica, que existe una crisis humanitaria, ya que sea la cifra que sea, los colombianos han sido maltratados por la guardia venezolana, debido al abandono estatal de la frontera colombo-venezolana por parte del Estado Colombiano a quien no se le puede quitar la responsabilidad de la situación actual, señala Torres.

“Los pañitos de agua tibia, no sirven” expresa el analista, refiriéndose a que el gobierno nacional debe atender la zona fronteriza en infraestructura, comunicaciones, educación, salud y vivienda como tuvo que hacerlo desde hace décadas, lo que implica que “**la causa primera de la actual crisis fronteriza debe adjudicársele al gobierno colombiano”**, dice.

Por otro lado, frente a la posibilidad de que **Colombia se salga de la UNASUR, implicaría un doble  aislamiento y sería un grave error** porque en esa institución hay posibilidades de actuar para potenciar los intereses del Estado colombiano, indica el analista, quien señala que sería desastroso si se toma esa decisión.
