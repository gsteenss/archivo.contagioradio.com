Title: ESMAD arremetió contra manifestantes en Pitalito, Huila
Date: 2016-06-08 14:12
Category: Movilización, Paro Nacional
Tags: ESMAD, Huila, Minga Nacional, Paro Nacional
Slug: esmad-arremetio-contra-manifestantes-en-pitalito-huila
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/06/Huila-ESMAD-e1465412848210.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Nick Jaussi 

###### [8 Jun 2016] 

Sobre las 9 de la noche de este martes, la Cumbre Agraria informó que las comunidades del corregimiento de Bruselas, del municipio de Pitalito, Huila donde se concentran varios manifestantes de la Minga Nacional**, fueron atacadas por un escuadrón del ESMAD donde también hacen presencia la Policía Nacional y el Ejército intimidando la movilización.**

De acuerdo con la denuncia, alrededor de las 9:00 de la noche, en la vía que conduce del Municipio de Pitalito a  Mocoa, mientras los manifestantes desarrollaban una actividad pedagógica llegó el **ESMAD a atacar a la población, usando gases lacrimógenos, empleando un uso excesivo de la fuerza contra los manifestantes, dejando varias personas heridas**. Incluso se aseguró que estaban disparando, poniendo  en riesgo la vida y la integridad física de quienes se encuentran en la jornada de protesta.

Isaac Marín, integrante del Congreso de los Pueblos, asegura que la situación es bastante delicada pues **los civiles no tienen ningún tipo de armas y en cambio se le está dando un tratamiento de guerra a la población,** que también se encuentra atemorizada por la presencia de militares y policías con armas de fuego.

Según el integrante del Congreso de los Pueblos, los manifestantes **realizan cierres de la vía por 30 minutos para que las personas conozcan las razones del paro nacional,** por esa acción la fuerza pública presiona para que desbloqueen la vía y permitan pasar los camiones que llevan el crudo.

Frente a esta situación, la comunidad hace un llamado urgente a la Procuraduría, Defensoría del Pueblo, a los organismos nacionales e internacionales veedores de Derechos humanos, a las organizaciones sociales, políticas y a la comunidad en general para que se vigile estas actuaciones institucionales y se garantice el derecho a la protesta y la vida de quienes la ejercen.

Cabe recordar que son **13 puntos el pliego de exigencias departamental y que se espera, sean atendidos en la mesa de negociación y concertación con el gobierno** y los delegados para la mesa departamental del Congreso de los Pueblos Huila.

<iframe src="http://co.ivoox.com/es/player_ej_11829405_2_1.html?data=kpallJ6YdJahhpywj5WZaZS1lZ2ah5yncZOhhpywj5WRaZi3jpWah5yncarnwsbQjbLFtsrijJKYpdTSq9PZ1NSYxsqPsNDnjNXix8fQs9SfjoqkpZKns8_owszW0ZC2pcXd0JCah5yncZU.&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
