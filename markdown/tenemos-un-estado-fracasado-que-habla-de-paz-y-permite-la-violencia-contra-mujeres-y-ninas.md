Title: "Tenemos un Estado fracasado, habla de paz y permite violencia contra mujeres y niñas"
Date: 2017-04-24 15:02
Category: Mujer, Nacional
Tags: asesinatos de mujeres, buso sexual, feminicidio, Violencia contra la mujer, violencia sexual
Slug: tenemos-un-estado-fracasado-que-habla-de-paz-y-permite-la-violencia-contra-mujeres-y-ninas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/03/cdn4.uvnimg.com_.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo] 

###### [24 Abr. 2017] 

**Colombia vivió un fin de semana trágico en materia de violencia contra las mujeres y los niños y las niñas.** El primer caso fue en el departamento del Meta, donde una bebé de 4 meses fue abusada sexualmente por un soldado y en Tolima, donde una niña de 3 años fue abusada torturada. Según cifras oficiales en lo que va corrido del 2017, **se han recibido 2500 denuncias de violaciones contra menores.**

Para **Sandra Mazo, directora de Católicas por el Derecho a Decidir**, no se pueden continuar normalizando las violencias hacia las niñas, niños y mujeres y añadió que lo que sucede en Colombia es muestra de “un Estado fracasado, mientras habla de paz, la violencia estructural contra las mujeres y las niñas continua”. Le puede interesar: [“El Estado es responsable del feminicidio de Claudia”](https://archivo.contagioradio.com/el-estado-es-responsable-de-lo-que-le-sucedio-a-claudia/)

Según Mazo las instituciones se están convirtiendo en una ONG, que solo les interesa documentar casos y mostrar las cifras, pero hay que ir más allá **“nos angustia que sean más de 2.500 denuncias de violaciones en 2017, más la violencia de género**. Sabemos que hay un subregistro en esas cifras. Es indignante que este Estado no haga nada por juzgar y prevenir este tipo de violencias”.

Dice Mazo que lo que se requiere es **un Estado que prevenga, que tome las medidas necesarias para acabar con este tipo de violencias** “es absurda la manera como los responsables de las instituciones estatales deciden y asumen estos casos. ¿Dónde está el Estado, donde están las instituciones que con los recursos de nuestros impuestos no se están viendo en medidas de prevención y protección de las personas más indefensas de la sociedad, como los niños”?

En el caso de la menor de 4 meses, el hecho fue cometido por un soldado del Ejército que estaba de permiso y sería el hermanastro de la mamá de la bebé, razón por la que Mazo cuestiona a dicha institución y el tipo de accionar. Le puede interesar: [Integrantes de las FFMM habrían intentado abusar sexualmente de 2 niñas en el Mango Cauca](https://archivo.contagioradio.com/integrantes-de-las-ffmm-habrian-intentado-abusar-sexualmente-de-2-ninas-en-el-mango-cauca/)

**“¿Qué tipo de soldados se están formando? Es deprimente, ¿no se supone que el Ejército debe mantener el orden, tienen una misión y una visión de protección a los derechos?** Esto no puede quedar impune. La responsabilidad del Ejército tiene que verse. Esto es denigrante, que un soldado abuse de una bebé de 4 meses” manifestó Mazo.

### **En Colombia algo anda muy mal.** 

En cuanto la garantía de los derechos de las mujeres y de la niñez, Mazo dijo que hay algo que anda muy mal, y que este tipo de violencias no pueden seguir siendo secundarias, en medio de un conflicto armado de tantos años **“no se ha querido ver estas violencias que son estructurales y que demandan medidas reales del Estado”** aseveró Mazo. Le puede interesar: [Nos siguen matando aunque ya no sea noticia](https://archivo.contagioradio.com/nos-siguen-matando-aunque-ya-no-sea-noticia/)

### **Metamorfosis de las violencias.** 

“La historia nos ha mostrado que la violencia se recrudece cada día más y hay una metamorfosis indudable en la manera de expresarse las violencias” relata Mazo, quien además dice que estos asuntos no son nuevos en el diario acontecer del país **“violencias estructurales hemos vivido de mil maneras de manera histórica**, solo que en la actualidad se están visibilizando más”. Le puede interesar: [Comisarías, jueces y fiscales desestiman denuncias de mujeres](https://archivo.contagioradio.com/comisarias-jueces-y-fiscales-desestiman-denuncias-de-mujeres/)

### **Embarazos en adolescentes en la capital.** 

Según cifras oficiales de la Secretaría de Salud, **cada 36 horas tan solo en Bogotá, una niña fue mamá, alcanzando en 2016 más de 233 partos**, además entre 2010 y 2017 cerca de 2892 niñas quedaron en embarazo, para Mazo estas cifras son preocupantes.

**“La maternidad tiene que ser una opción, no puede ser una obligación** y aquí indudablemente muchas de estas maternidades son producto de violencia, entonces se necesita una atención sexual integral” agregó Mazo.

Finalmente, dijo que es importante saber que el acceso a salud sexual y reproductiva que **“no basta con decirle a las chicas y a los chicos usen condón o usen métodos anticonceptivos**, porque el asunto también es de acceso que tienen que decidir si compran agua de panela con pan o el anticonceptivo”.

Razones por las que Mazo sugirió que las medidas que se tomen estén encaminadas al acceso público y gratuito a la información y los métodos anticonceptivos. Le puede interesar: [Los retos del 2017 para enfrentar la violencia contra las mujeres en Colombia](https://archivo.contagioradio.com/los-retos-del-2017-enfrentar-la-violencia-las-mujeres-colombia/)

<iframe id="audio_18313724" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_18313724_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)

<div class="ssba ssba-wrap">

</div>
