Title: USO denuncia atentado contra líder que se opone al Fracking en Cesar
Date: 2018-07-11 10:36
Category: DDHH, Nacional
Tags: Atentado, Lider Sindical, Lider social, USO
Slug: uso-denuncia-atentado-contra-lider-que-se-opone-al-fracking-en-cesar
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/07/Captura-de-pantalla-2018-07-11-a-las-10.21.59-a.m..png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: USO] 

###### [11 Jul 2018]

Por medio de un comunicado, la Unión Sindical Obrera de la Industria del Petroleo (USO) denunció un atentado contra el dirigente sindical Luis Alberto Gálvis, que habría tenido lugar el 10 de julio sobre las 8:00 de la noche. Gálvis, trabaja actualmente con Ecopetrol en el Municipio de San Martin, Cesar.

De acuerdo con el comunicado, dos hombres que se desplazaban en una motocicleta se acercaron al vehículo en el que se movilizaba el funcionario, y arrojaron un elemento explosivo que por fortuna no le causo heridas. Está es la segunda vez que se atenta contra el líder sindical, puesto que en en octubre del 2017 extraños atacaron el carro en el que se desplazaba y rompieron los vidrios de su casa.

Además de hacer parte de la Federación Sindical FUNTRAMIEXCO (institución afiliada a la USO), Gálvis está vinculado a La Corporación Defensora del Agua, Territorio y Ecosistemas (CORDATEC) y a otras organizaciones que se oponen al uso del Fracking para extracción del petroleo en Yacimientos No Convencionales en el sur del Cesar.

Por estos atentados, tanto la USO como CORDATEC pidieron garantías para que se respete la vida de los Defensores de Derechos Humanos y exigieron celeridad en el proceso de investigación que ayude a determinar responsabilidades en los hechos con los que se busca amedrentar al líder y su familia.

### **Entre el enero de 2016 y el 27 de febrero de este año han sido asesinados 282 líderes sociales** 

Según el informe de alertas tempranas emitido por la Defensoría del Pueblo, "durante el período comprendido entre el 1 de enero de 2016 y el 27 de febrero de 2018, 282 líderes sociales y defensores de derechos humanos han sido asesinados en Colombia". Según ese documento, entre enero de 2017 y el 27 de febrero de este año, han sido asesinados 7 líderes sindicales y 2 ambientales.

[Acción Urgente nuevo atentando contra Dirigente Luis Alberto Galvis.](https://www.scribd.com/document/383657338/Accio-n-Urgente-nuevo-atentando-contra-Dirigente-Luis-Alberto-Galvis#from_embed "View Acción Urgente nuevo atentando contra Dirigente Luis Alberto Galvis. on Scribd") by [Contagioradio](https://www.scribd.com/user/276652802/Contagioradio#from_embed "View Contagioradio's profile on Scribd") on Scribd

<iframe id="doc_50770" class="scribd_iframe_embed" title="Acción Urgente nuevo atentando contra Dirigente Luis Alberto Galvis." src="https://www.scribd.com/embeds/383657338/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-xSQ2GQNLGeH2KvcmGjOM&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="0.7729220222793488"></iframe>

######  

###### [Reciba toda la información de Contagio Radio en ][su correo](http://bit.ly/1nvAO4u)[ o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por] [Contagio Radio](http://bit.ly/1ICYhVU) 
