Title: Desplazadas al menos 5 familias de Turbo por presuntos paramilitares
Date: 2016-07-06 13:59
Category: DDHH, Nacional
Tags: Autodefensas Gaitanistas de Colombia, desplazamientos en Turbo, paramilitarismo en Colombia, Restitución de tierras
Slug: presuntos-paramilitares-desplazan-a-por-lo-menos-5-familias-en-turbo-antioquia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/07/gucamayas.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Casas quemadas en Guacamayas] 

###### [6 Julio 2016 ] 

Persisten las **acciones violentas contra las familias reclamantes de tierras en la vereda Guacamayas** del municipio de Turbo, Antioquia, el más reciente hecho se registró el pasado viernes cuando se dieron intercambios de disparos entre presuntos paramilitares y miembros del Ejército, luego de que los atacantes incendiaran algunas de las viviendas que los campesinos han dejado abandonadas ante las amenazas y agresiones proferidas en su contra el pasado 11 de junio para obligarlos a desplazarse.

Por lo menos 5 familias se han visto obligadas a desplazarse a municipios aledaños como Carepa, otras tantas permanecen hacinadas en las casas más cercanas a la carretera y al menos cinco han tenido que dormir en el monte, ante la presión de **presuntos paramilitares de las Autodefensas Gaitanistas que estarían vinculados con una empresa ganadera de la zona** y quienes pese a las denuncias de las comunidades, no han sido capturados porque la Policía insiste en que las familias deben conciliar con los agresores.

Para líderes como el señor Julio Correa es muy preocupante esta situación y mucho más que las autoridades locales, la Fiscalía y la fuerza pública no actúen eficazmente para detener las agresiones en su contra, que han incluido robos de comida, animales y ropa, y la destrucción de bienes y cultivos. "**Nosotros los líderes no hemos podido salir a denunciar porque nos tienen fuertemente amenazados**, es como sí estuviéramos secuestrados en nuestro propio sector, [[porque no tenemos seguridad](https://archivo.contagioradio.com/familias-en-turbo-en-riesgo-inminente-de-un-desplazamiento-masivo/)]", asegura el poblador.

<iframe src="http://co.ivoox.com/es/player_ej_12140050_2_1.html?data=kpeelpWUeZGhhpywj5aXaZS1k5iah5yncZOhhpywj5WRaZi3jpWah5yncavpzc7cjajTttPZwoqfpZDMpcPd1cbb1sqPmNbmw9Sah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ..&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en[ [su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)]] 

 
