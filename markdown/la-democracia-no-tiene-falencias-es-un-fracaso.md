Title: La democracia no tiene falencias: es un fracaso
Date: 2016-11-10 10:51
Author: JOHAN MENDOZA TORRES
Category: Johan Mendoza, Opinion
Tags: democracia, Elecciones Estados Unidos
Slug: la-democracia-no-tiene-falencias-es-un-fracaso
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/democracia.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: lacosechadealmas.blogspot.com.co] 

#### **Por: [Johan Mendoza Torres](https://archivo.contagioradio.com/johan-mendoza-torres/)** 

###### 10 Nov 2016 

[¿Democracia para qué? ¿Para que gane Trump, un tipo obsesionado consigo mismo, que sabe muy poco de política y que cree que gobernar es hacer pataletas y pagar más? ¿democracia para qué? ¿para que aceptemos cómo “por ley” gane el gane, así sea un cretino, se debe respetar? ¿democracia para aceptar cómo el premio Nobel de Paz Mr. Barack Obama envía tropas y más tropas a medio oriente y la gente so pretexto de sentirse miserable dice que “era necesario”? ¿democracia para aceptar cómo derrocan a los dictadores en medio oriente y los cambian por guerras civiles sangrientas? ¿defender la democracia para seguir haciéndole un altar al cinismo y evitar la vergüenza de aceptar que Siria, Libia e Irak estaban mejor sin democracia? ¿democracia para aceptar cómo la libertad se bañó de petróleo, de mentira, y el mundo esperó un atentado en París para por fin sentirse conmovido con la violencia?]

[¿Democracia para aceptar cómo Juan Carlos Vélez le dice a un país que la idea era llevar a la gente “verraca” a las urnas?  ¿democracia para aceptar que las Nacionales Unidas salgan a pedirle plata a la clase media en las calles porque no se atreverían a bajar los exuberantes sueldos de altos directivos que están al servicio de la pobreza como un negocio? ¿democracia para seguir pagando de nuestro bolsillo a la OEA sólo para que vivan de cóctel en cóctel, de seguro celebrando la inutilidad de sus recomendaciones?]

[¿Para eso la democracia? ¿para que la gente que no sabe de política, termine votando por el sí o por el no, en un país, que lo que menos necesitaba era polarizarse? ¿para qué la democracia? ¿para seguir diciendo que Colombia tiene los gobiernos más estables de Latinoamérica así la gente se muera de hambre, no tenga carreteras, no tenga trenes, no le den la cita con el especialista y los políticos se roben la plata de las empresas públicas mientras señalan que lo público es menos eficiente que su capacidad privada para robar?  ]

[¿Democracia para qué? ¿para seguir negando cínicamente que Colombia, en verdad solo se desarrolló cuando no hubo democracia por allá en el año 1953? ¿democracia para alimentar el estilo de vida de los apolíticos que viven de esperar qué traerá el centro comercial de nuevo? ¿democracia para que no nos pregunten si queremos que esto o aquello suba de precio? ¿democracia para qué? ¿para decir que la izquierda es un fracaso, que es populista, mientras personajes como Bush, como Trump, como Peña, como Fujimori, como Uribe, siguen sumando y sumando incautos? ¿democracia para tildar a la izquierda de “ideologizada” mientras desde la más primaria educación no se enseña a pensar políticamente nada más allá fuera de la democracia?]

[¿Para eso democracia? ¿para salir a votar por el menos peor? ¿para que un acuerdo de paz quede en manos de la mentira electoral? ¿democracia para salvaguardar a los congresistas colombianos, que traicionando el ideal de su patrono (el general Santander) han quedado con puestos vitalicios, sueldos exuberantes, patrañas de contradicciones ante los medios, pero abrazos y risas el club?]

[¿Para eso la democracia? ¿para seguir dividendo, para seguir mintiendo con que todo está bien, cuando en realidad, todos los colombianos sabemos que todo anda como un vehículo con ruedas cuadradas?   ¿democracia para reconocer que somos incapaces y que estamos llenos de miedo no para lanzar un hijueputazo al político corrupto, sino para reconocer que es el sistema el que está totalmente errado? ¿para eso la democracia? ¿para seguir dando cátedras en las universidades de cómo perpetuar un sistema más fracasado que los intentos de socialismo en el mundo? ¿democracia para huir del sentimiento de culpa que se nos viene encima cuando creemos que pensar por fuera de la democracia es ser una especie de hitlercito?]

[¿Para eso la democracia? ¿para seguir salvaguardando los privilegios de los que seguirán en el poder convenciendo con bonitas palabras la fealdad de la realidad de las mayorías? ¿democracia para qué? ¿para decir que “cómo allá funciona” aquí también debe ser así, aunque veamos como “allá” gana un tipo como Trump o se posiciona un hipócrita como Hollande, que hace poco dejó caer bombas sobre hospitales sirios y que lamentablemente no nos enteramos porque Facebook no nos puso el filtro de la bandera siria?]

[¿Democracia para que tarde o temprano aquí termine de cónsul un presentador de televisión? ¿democracia para que el mismo de las gaseosas financie campañas mentirosas? ¿para que el mismo de los bancos, sea el mismo que nos venda la casa? ¿democracia para qué? ¿para convencer a los que no votan de que tienen la culpa de todo, mientras aquí el golf es el mecanismo de ascenso político por excelencia? ¿democracia para sentir pesar por los Nule, por Arias? ¿para que un loco como Juan Carlos Echeverri se vuelva catedrático sobre el cuidado ambiental? ¿democracia para que tarde o temprano un embaucador como Raisbeck diga “]*[si Trump pudo yo también lo haré”]*[y siga pregonando escuela austriaca como si fuera su propio invento?]

[¿Democracia para eso? ¿para que los altos mandos militares sigan con las barrigas llenas, mientras superan bebiendo alcohol la pena que les causa quedar subordinados ante cualquier capitanzucho gringo que llegue con órdenes de mandar en Tolemaida? ¿democracia para eso? ¿para que algunos compren la desobediencia para sentirse menos tontos que otros? ¿Para engañar al pueblo diciéndole que todo en esta democracia está mal pero que la solución es la democracia?]

### [¿para eso la democracia? …..  ] 

[Si es para eso, hoy más que nunca, me declaro enemigo de la democracia. No es un sistema político viable para nuestra sociedad, ha demostrado ser incapaz de sustentar el bienestar, la ética, las razones más profundas para que las mayorías puedan vivir una vida digna. La democracia es un cheque sin fondos, con muchos ceros, ceros que animan a más de uno, pero sin fondos… sin fondos.]

[Algo tan bello como la política, no debe quedar en manos de la democracia, por lo menos no ahora, no en Colombia, y al parecer no en ningún otro país del mundo.     ]

------------------------------------------------------------------------

###### [Las opiniones expresadas en este artículo son de exclusiva responsabilidad del autor y no necesariamente representan la opinión de la Contagio Radio.] 
