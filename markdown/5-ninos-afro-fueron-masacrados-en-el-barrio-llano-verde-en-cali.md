Title: 5 niños afro fueron masacrados en el barrio Llano Verde en Cali
Date: 2020-08-12 18:55
Author: AdminContagio
Category: Actualidad, Nacional
Tags: comunidades negras, Jorge Iván Ospina, Llano Verde, Masacre en Cali
Slug: 5-ninos-afro-fueron-masacrados-en-el-barrio-llano-verde-en-cali
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/08/Masacre-en-barrio-Llano-Verde-en-Cali.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

La noche de este martes **se registró una masacre en la que cinco niños entre los 13 y 16 años fueron asesinados en un cañaduzal del barrio Llano Verde de la ciudad de Cali, Valle del Cauca.** Sobre las 11 de la noche el alcalde de la ciudad, [Jorge Iván Ospina](https://twitter.com/JorgeIvanOspina), reportó el lamentable hecho.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/JorgeIvanOspina/status/1293431040917278720","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/JorgeIvanOspina/status/1293431040917278720

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

Según medios locales **los menores asesinados fueron** **Jair Andrés Cortés (14 años), Jean Paul Cruz (16 años), Luis Hernando Preciado (14 años), Arturo Montenegro (13 años) y Leider Hurtado (14 años); todos pertenecientes a la comunidad afro.** Informes preliminares de Policía señalan que cuatro de los menores  fueron asesinados con tiros de gracia y el restante fue degollado. (Lea también: [En Caucasia fue asesinada hija de reincorporados](https://archivo.contagioradio.com/en-caucasia-fue-asesinada-hija-de-reincorporados/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Llano Verde, es una zona perteneciente al Distrito de Aguablanca; la mayoría de sus pobladores hasta hace dos años vivían en el Jarillón y fueron reubicados en este lugar por el riesgo de inundaciones cuando el rio Cauca crecía. La población afrodescendiente se estima en un 80% y gran parte de ella ha sido víctima de desplazamiento desde el Pacífico nariñense. **En el barrio hay microtráfico, guerra de pandillas, fronteras invisibles y mucha violencia.** (Le puede interesar: [Otra Mirada: La verdad del pueblo negro](https://archivo.contagioradio.com/otra-mirada-la-verdad-del-pueblo-negro/))

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/llombartpatUE/status/1293552395742408715","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/llombartpatUE/status/1293552395742408715

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/MAPPOEA/status/1293626925374738432","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/MAPPOEA/status/1293626925374738432

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:heading {"level":3} -->

### La masacre de Llano Verde fue solo uno de los hechos que enlutaron a la ciudad de Cali

<!-- /wp:heading -->

<!-- wp:paragraph -->

**A la masacre de los cinco niños en Llano Verde se suman las muertes violentas de seis personas más, que completan un fatídico saldo de 11 personas asesinadas en la ciudad durante el martes 11 de agosto.**

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/MarioALince/status/1293483431100387335","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/MarioALince/status/1293483431100387335

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

En lo corrido del 2020 **la cifra de asesinatos asciende a 613 casos en la capital del Valle** lo que refleja que al igual que en otras zonas del país la violencia se ha recrudecido en medio de la pandemia.

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
