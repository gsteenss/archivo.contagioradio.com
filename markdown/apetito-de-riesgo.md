Title: Apetito de riesgo
Date: 2020-04-13 09:36
Author: AdminContagio
Category: Johan Mendoza, Opinion
Tags: Coronavirus, Covid-19, politica
Slug: apetito-de-riesgo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/04/apetito7a7s7a.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:heading {"level":6,"customTextColor":"#5e686d"} -->

###### Por: Johan Mendoza Torres 

<!-- /wp:heading -->

<!-- wp:paragraph -->

El lenguaje político durante una crisis, el discurso, la interpretación de ellos y ante todo el impacto psicológico que tiene sobre las masas cuando son los gobiernos quienes hacen uso de neologismos o maquillan una realidad adversa con otra menos adversa o “digerible”, es el marco de la guerra psicológica que en estos instantes está librando el uribismo y sus amigos banqueros contra la población colombiana.  

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Desde el instante en que se inventaron un camino más largo como fue “aislamiento preventivo obligatorio” para no llamarle “cuarentena”, con el fin de evitar afrontar la realidad patológica que vivimos como sociedad y dar cuenta de la grave situación, algunos ya sospechaban que el ocultamiento, el engaño en los mensajes y la sensación de bienestar que un Estado manejado por la mafia no puede transmitir, se veía venir y sobre todo, aumentar toda vez que la crisis aumentara. 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por supuesto, hablar del lenguaje no está más allá ni más acá del robo de dinero público y demás manifestaciones del síntoma de una democracia fallida por causa de una clase política con rostro, nombre, familia y partido propios; no obstante, es de suma importancia abordar el tema porque en una situación de cuarentena y toque de queda como en la que estamos, el vehículo con el que nos asociamos a la realidad es el lenguaje. 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Según Walter Benjamin, el lenguaje es una pieza clave de la vida, ***la sociedad se comunica en el lenguaje y no por el lenguaje****,* afirmaba*.* Si nos comunicamos en el lenguaje, debemos asistir a una verdad compleja y es que las palabras crean mundos posibles, y en ese sentido el lenguaje dota a la realidad de un elemento muy importante: realidad.  

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por eso, quizá se recibió con sorna lo de “*aislamiento preventivo obligatorio*” que bien pudo ser “*ley marcial y toque de queda por la cuarentena*” o un simple “*se quedan en la casa carajo*” pero insisto, hasta allí, ese nombre que le dieron nos dejaría solo un debate estúpido por redes sociales. Empero, el concepto que el ministro de hacienda, señor Carrasquilla, emitió hace un par de días espanta la sorna y nos recuerda que estamos en plena guerra psicológica. 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Debates aquí y allá se han presentado respecto a la pregunta básica ¿por qué el gobierno tiene que entregar el dinero público, es decir la riqueza producida por el trabajo de todos y todas, al sector bancario privado para que dicho sector nos lo devuelva en forma de créditos? 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Eso es un cóctel muy peligroso, pues estructuralmente se está privatizando una riqueza que es nacional, pública y colectiva. Pero la cereza del cóctel la arroja Carrasquilla cuando afirma que en este momento en Colombia no hace falta liquidez, (es decir dinero, platica, billete, money, cash, machín, lukas, gambas, pasta, lana) sino que lo que le hace falta a Colombia es:

<!-- /wp:paragraph -->

<!-- wp:heading {"level":5} -->

##### APETITO DE RIESGO

<!-- /wp:heading -->

<!-- wp:paragraph -->

Sui generis el concepto, pero bastante inteligente, y efectivo para que cada hombre y mujer sin un grado mínimo de actitud crítica, se responsabilice a sí mismo/a por no tener dinero…bastante inteligente la jugada del gobierno que sabe que está en guerra psicológica. 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

¿Lo sabía usted? ¿sabía que el objetivo primario de esa guerra es nuestra poca conciencia de que, fue el gobierno entregándole el dinero público a la banca privada, el responsable de que muchos no tengan dinero y no la falta de “apetito de riesgo” de los individuos?  

<!-- /wp:paragraph -->

<!-- wp:heading {"level":5} -->

##### ¿Y bueno, riesgo para qué señor Carrasquilla?... riesgo para endeudarse con la banca privada. 

<!-- /wp:heading -->

<!-- wp:paragraph -->

Es decir, nuevamente, bajo el manto ideológico del neoliberalismo, el ministro de hacienda (uribista desde que acompañó al mismísimo Uribe en el ministerio y que ahora está con el señor Duque) pretende responsabilizar, individualizar, echarle la culpa ¡a usted! ¡a mí! ¡al vecino! ¡a nosotros! de que si nos quedamos sin trabajo o los que ya perdieron el trabajo y no tienen dinero, será por su falta de apetito de riesgo. 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Dinero sí teníamos, era suyo, del vecino, mío, nuestro y se lo dieron a la banca privada, y eso de “banca privada” también es maquillar el lenguaje: se lo dieron a Sarmiento, a su familia y a sus socios que claramente tienen nombre, apellido y partido. No falta el iluso que diga “es culpa de la corrupción”

<!-- /wp:paragraph -->

<!-- wp:heading {"level":5} -->

##### **¡la corrupción no existe sino como síntoma de los indignos que la producen, y esos indignos, tienen rostro, nombre, familia y partido!**

<!-- /wp:heading -->

<!-- wp:paragraph -->

Entonces, la persuasión mediante el lenguaje acarrea el beneficio de incidir sobre aquellos receptores que por ejemplo en este momento no ven a unos “gobernantes”, sino que, en medio de su angustia, de sus terrores de infancia exacerbados respecto a la posibilidad de morir o de que mueran sus seres queridos, de sus instintos primitivos avanzando sobre su raciocinio ante la inminente crisis económica, lo que están viendo son “tutores”. 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

“Tutores” de los que aceptarán el castigo so pretexto de nunca sentirse solos y abandonados como un niño o una niña con miedo. Eso, apreciado lector, sino es Carrasquilla, la gente de su equipo lo sabe, lo han estudiado, y lo están aplicando. 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

La sugestión produce el engaño, ¿Qué busca Carrasquilla con su “apetito de riesgo” sino mantener a los bancos como agentes libres de toda culpa y responsabilidad? 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

¿No tiene dinero? La culpa no es de lo que ha hecho el gobierno, la culpa no es de los bancos que ahora le prestarán plata que era suya en forma de créditos a los que incluso muchos no podrán aplicar…no señoras y señores, la culpa es su falta de “apetito de riesgo”.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Apetito de riesgo probablemente tendrán millones de colombianos y colombianas de emprender, apoyar o simpatizar una revolución política contra el mal gobierno. Por ahora no me apetece el sentido de su concepto señor Carrasquilla, y tenga presente que las revoluciones que se gesten en el mundo luego del Covid-19, no serán esas revoluciones perfectas y tranquilizantes que soñaron incluso los que confiábamos en el abrazo para cambiar la sociedad, no… serán las revoluciones de la historia, imperfectas, pero reales, y eso sí, desde otro sentido del concepto, con mucho apetito de riesgo.  

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Le puede interesar: [Mas columnas de Johan Mendoza](https://archivo.contagioradio.com/johan-mendoza-torres/)

<!-- /wp:paragraph -->
