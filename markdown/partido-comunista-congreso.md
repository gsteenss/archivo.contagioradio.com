Title: Para construir la unidad hay "un abanico de coincidencias" Partido Comunista
Date: 2017-07-15 14:54
Category: Nacional, Política
Tags: elecciones 2018, Partido Comunista
Slug: partido-comunista-congreso
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/07/partido-comunista.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Partido Comunista] 

###### [14 Jul 2017] 

Desde el 13 y hasta el 15 de julio, se está desarrollando el congreso número 22 del Partido Comunista Colombiano, un escenario que de acuerdo con Jaime Caycedo, integrante de este parido, que tiene como objetivo **hacer un llamado a la unidad de diferentes corrientes para lograr avanzar en la implementación de los acuerdos de La Habana** y en construir un país más equitativo.

El congreso ha contado con la participación de diferentes tendencias políticas como las representadas por **Ernesto Samper, Clara López, delegados de las FARC-EP y del ELN, diferentes organizaciones sociales y líderes del movimiento popular en el país** y para Caycedo se da bajo un contexto de cambio tras la firma de los acuerdos de paz y de las diferentes movilizaciones sociales.

### **Hay un abanico de coincidencias para construir la unidad** 

Estas participaciones, para Caycedo son “un abanico de coincidencias que se están mostrando muy claramente en la vida nacional y que indican que **la unidad de esas distintas vertientes puede ser un signo de que se puede consolidar para el 2018** el embrión de los acuerdos de paz”. (Le puede interesar:["La apuesta de la Unión Patriótica para la unidad, de cara a las elecciones del 2018"](https://archivo.contagioradio.com/sectores-del-movimiento-social-se-uniran-para-elecciones-2018-aida-abella/))

Frente a las posibles coaliciones que se puedan gestar para las elecciones del 2018, Caycedo manifestó que el Partido Comunista está planteando **una apertura al diálogo y la unidad que no solo provenga desde sectores políticos sino con la sociedad en general** “tendremos que construir entre todos y todas las condiciones de equidad e igualdad en un país como este” asevero el vocero.

Además, agregó que será una agenda con puntos en común, **como el respaldo a los acuerdos de paz, acabar con la corrupción, y la posibilidad de adelantar un proceso constituyente**, las que permitan que se pueda unir fuerzas de cara al próximo año. (Le puede interesar:["Organizaciones sociales realizan balance de la implementación de los Acuerdos de Paz"](https://archivo.contagioradio.com/organizaciones-sociales-realizan-balance-de-la-implementacion-de-los-acuerdos-de-paz/))

### **Hay que acabar con el lenguaje de odio en la política** 

Sin embargo, Caycedo también señaló que en Colombia se ha construido un lenguaje del odio, la confrontación y el miedo hacia la paz, “se trata de crear esa imagen de que acá hay un negocio oscuro, que algo se está tramando, que con los acuerdos vamos a quedar en condiciones críticas y **se hace con la intensión de tergiversar la realidad**”.

Razón por la cual una de las propuestas que ha expresado el Partido Comunista es que para el próximo periodo electoral **será fundamental tener un lenguaje que propicie el diálogo entre todos, que no genere prevenciones**.

<iframe id="audio_19805867" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_19805867_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
