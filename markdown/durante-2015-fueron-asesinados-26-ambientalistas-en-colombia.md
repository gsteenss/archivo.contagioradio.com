Title: Durante 2015 fueron asesinados 26 ambientalistas en Colombia
Date: 2016-06-22 15:06
Category: DDHH, Nacional
Tags: ambientalistas, Derechos Humanos, paz
Slug: durante-2015-fueron-asesinados-26-ambientalistas-en-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/06/Justicia-Berta-Cáceres_AFP-e1466625702189.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: AFP 

###### [22 Jun 2016] 

Así como continúa aumentando la temperatura del planeta, los asesinatos contra defensores del ambiente en el mundo continúan incrementando, y durante el 2015, la cifra llegó a su punto más alto. De acuerdo con el más reciente informe de la **ONG, Global Witness al menos 185 activistas fueron asesinados durante el 2015, es decir tres por semana, de los cuales el 66% de las víctimas son latinoamericanas.**

Según el informe titulado **‘En terreno peligroso’,** Brasil ocupa el primer lugar con 50 ambientalistas asesinados, enseguida sigue Filipinas con 33, y en tercer lugar se encuentra Colombia con 26 crímenes contra  defensores del ambiente. Luego continúa Perú (12), Nicaragua (12) y la República Democrática del Congo (11). Es decir, una cifra anual un 59% más alta que en 2014.

Los defensores de la tierra, los bosques, los ríos y los animales fueron asesinados en su mayoría por la minería (en 42 casos), la agroindustria (20), la tala (15) y los proyectos hidroeléctricos (15). Frente a eso, Margarita Flores, directora de la Asociación Ambiente y Sociedad, resalta que “**El ambientalismo ejercido como una causa en defensa del ambiente constituye ahora un motivo de muerte,** en vez de resaltarlo como un ejercicio de ciudadanía y defensa de derechos humanos y ambientales”.

Por su parte, Billy Kyte, encargado de campañas de Global Witness. Señala que “Cada vez es más común que las comunidades que toman cartas en el asunto se encuentren en el punto de mira de la seguridad privada de las empresas, las fuerzas estatales y un mercado floreciente de asesinos a sueldo. Por cada asesinato que documentamos, hay muchos otros que no se denuncian. **Los gobiernos deben intervenir urgentemente para detener esta espiral de violencia”.**

Teniendo en cuenta esa situación, que se profundiza en América Latina, la ONG en su informe insta a los gobiernos a aumentar la protección de los activistas amenazados, investigar y llevar a las justicia tanto a los actores empresariales como políticos, defender el derecho de los activistas a negarse a la realización de proyectos y garantizar el derecho a la consulta previa de las comunidades.

Cabe recordar que **entre 2010 y 2015, Global Witness registra 753 asesinatos, cuyo 77% son latinoamericanos. Brasil suma 207, Honduras 109, y Colombia 105.**

### El Caso Colombiano 

Colombia ocupó durante el 2015, el tercer lugar en asesinatos de ambientalistas. Según el informe de Global Witness**, el país es una de las naciones donde menos se respetan los derechos de los pueblos indígenas y por ende sus territorios,** debido a que su tierra y recursos naturales están siendo saqueados por intereses políticos y empresariales.

“El ambientalismo vuelve a sus raíces en Colombia donde se reivindica la tierra como un motivo de defensa, lo que se vuelve causa de señalamientos, descalificaciones y estigmatizaciones, que son una cuota inicial de la posterior muerte”, indica la directora de Ambiente y Sociedad, quien añade que teniendo en cuenta los proceso de paz que se adelantan con las FARC y el ELN, también los actores armados ilegales y legales deberían desarmarse con los defensores del ambiente.

En ese sentido, según Margarita Flores, **el gobierno colombiano debe reconocer esa problemática y pensar estrategias para que la muerte no esté inmersa en la defensa del ambiente,** en el segundo país más biodiverso del mundo.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
