Title: El acoso sexual es violencia y Otálora un infame
Date: 2016-01-25 09:27
Category: Carolina, Opinion
Tags: acoso sexual, Defensoría del Pueblo
Slug: armando-otalora-el-acoso-sexual-es-violencia-y-otalora-un-infame
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/01/jorge-otalora.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Julio Granados.] 

#### [[Carolina Garzón Díaz](https://archivo.contagioradio.com/carolina-garzon-diaz/) - ([@E\_Vinna](https://twitter.com/E_Vinna)).] 

###### [[(]25 Ene 2016[).]] 

[Valientemente el periodista Daniel Coronell ha dedicado una vez más su columna a denunciar con pruebas las agresiones del Defensor del Pueblo, Jorge Armando Otálora, contra sus subalternos. Y ha sido más valiente Astrid Helena Cristancho, exsecretaria privada de Otálora, quien tras vencer el miedo reveló ser víctima de acoso sexual por parte del alto funcionario público. En la Revista Semana se publicaron este domingo los chats y las fotografías que probarían este reprochable e ilegal acto de violencia cometido por el hombre encargado de “velar por la promoción, el ejercicio y la divulgación de los derechos humanos” en Colombia.]

[Las denuncias contra Otálora no son menores y aterra pensar que este mismo hombre ha pasado por varios cargos de poder en las instituciones del Estado. Ha estado en la Procuraduría, fue Vicefiscal y en el 2009 fue ternado por el entonces Presidente Álvaro Uribe para el cargo de Magistrado de la Sala Disciplinaria del Consejo Superior de la Judicatura, puesto con el que se quedó gracias al apoyo del Partido de la U y el Partido Liberal. En agosto de 2012, nuevamente con el apoyo de los partidos Liberal, Conservador y el Partido de La U, fue elegido por la Cámara de Representantes como Defensor del Pueblo. ¿Ahora por qué guardan silencio los congresistas y partidos que lo respaldaron? El Congreso colombiano tiene su cuota de responsabilidad en esta infamia y es su deber exigir al unísono la renuncia de Otálora.]

[En cuanto a los hechos denunciados, es importante resaltar que el acoso sexual no es un hecho menor, sino una conducta rechazada y condenada internacionalmente. El acoso sexual es una forma de violencia, como se expresa en la  ]*[Resolución 48/104 de la Asamblea General de las Naciones Unidas,]*[cuando incluye el acoso sexual en su definición de violencia contra la mujer y exhorta a los Estados a establecer sanciones y acciones preventivas para eliminarla.]

[Asimismo, en los párrafos 59, 180 y 290 de la]*[Declaración y Plataforma de acción de Beijing]*[, se reconoce el acoso sexual como una forma de violencia contra la mujer. De hecho, en el literal C del párrafo 180, se sostiene que se deben “Promulgar y aplicar leyes para luchar contra el acoso sexual y otras formas de hostigamiento en todos los lugares de trabajo”. Un argumento más.]

[Finalmente, la]*[Convención de Belém do Pará]*[ habla sobre el derecho de las mujeres a tener una vida libre de violencias, incluyendo el acoso sexual laboral o en cualquier otro contexto. Adicionalmente, esta convención le exige a los Estados sancionar y promulgar normas para proteger a las mujeres frente al acoso y todas las formas de violencia.]

[Con estos tres marcos internacionales, vemos que el acoso sexual no es un asunto menor, y que en el caso del “Defensor del Pueblo” Jorge Armando Otálora, es una violación a los derechos humanos de las mujeres y su reprochable conducta debe ser tratada como tal.]

[Así, como mujer, como periodista y como ser humano, me uno a la exigencia de la inmediata renuncia de Otálora. Pero no conforme con ello, es necesario que el Estado a través de la Procuraduría e incluso la Fiscalía, abran inmediatamente una investigación disciplinaria y penal contra Jorge Armando Otálora por los hechos de acoso sexual contra sus subalternas y acoso laboral contra los funcionarios de la Defensoría del Pueblo. No hacerlo, sería una prueba más de la negligencia del Gobierno en la protección a los derechos humanos, los derechos de las mujeres y los derechos de los trabajadores.]

###### Más columnas de opinión de [Carolina Garzón](https://archivo.contagioradio.com/carolina-garzon-diaz/) 
