Title: ¡A la fuerza!, así será la erradicación de cultivos de Coca en Colombia
Date: 2018-08-10 17:00
Category: DDHH, Nacional
Tags: COCCAM, Erradicación Forzada, Glifosato, Guillermo Botero
Slug: fuerza-sera-erradicacion-coca
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/11/Militares-en-Argelia.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [10 Ago 2018] 

El ministro de Defensa Guillermo Botero, anunció que se eliminará la erradicación voluntaria de cultivos de uso ilícito, declaración que tuvo lugar el 9 de agosto, horas antes de la realización de un Consejo de Seguridad en Norte de Santander. Sin embargo, Cesar Jerez, vocero de la Coordinadora Nacional de Cultivadores de Coca, Amapola y Marihuana (COCCAM), cree que con este aviso se está volviendo al pasado.

Jerez señala que no existe el concepto de erradicación voluntaria, **puesto que la erradicación de cultivos de uso ilícito siempre es violenta**; sin embargo, si hubo un acuerdo de sustitución,  que estaba anclado a los Acuerdos de Paz de La Habana y ofrecía un enfoque innovador en el tratamiento a los cultivadores de Coca.

Según el vocero de la COCCAM, el enfoque de sustitución con el que se venía trabajando reconocía en los cultivos de uso ilícito la única forma de sustento que tenían muchos campesinos, al tiempo que ofrecía una intervención estructural para transformar la realidad económica y social que les permitiera tener otras fuentes de ingresos.

### **Con la erradicación forzada volveremos al pasado** 

Jerez asegura que la erradicación forzada que propone Botero es una vuelta al pasado, con un enfoque fracasado, porque hay muestras de que con la fumigación, los cultivos se trasladan a otras zonas, y represivo porque la erradicación forzosa es una operación militar que destruye fincas y **deja como saldo la judicialización ilegal de muchos campesinos.**

De acuerdo con Jerez, hasta el momento **"ya tenemos unos 3 mil campesinos en la cárcel",** así que el impacto de la erradicación forzada afecta los derechos humanos, la libertad, y las fincas campesinas, que encuentran en los cultivos de uso ilícito su única fuente de supervivencia.

### **La erradicación forzada también es un tema de agenda internacional** 

De una parte, Jerez señala que hay presión de parte del Gobierno Norteamericano para que se regrese a la fumigación, además, por los intereses económicos que tienen empresas involucradas en la aspersión del Glifosato. Por otra parte, también podría haber conflictos fronterizos con Ecuador, Brasil y Venezuela por el uso de este herbicida, cuyo uso puede ser nocivo para la salud.

Precisamente, la salud de las personas es el principal argumento para oponerse a la erradicación con glifosato, acción sobre la cual se pronunció la Corte Constitucional en la sentencia **T-080 de 2017, que prohíbe cualquier aspersión aérea con este químico** porque "es una sustancia que tiene la potencialidad de afectar la salud humana como probable agente cancerígeno y, también, de forma muy peligrosa, el medio ambiente".

Cesar Jerez finaliza añadiendo que "Colombia es el único país del mundo que en el último periodo permitía las fumigaciones, que permite que se le fumiguen sus selvas, sus ecosistemas porque es imposible dirigir la fumigación a un cultivo de 1 o 2 hectáreas". (Le puede interesar:["La erradicación de la coca en Arauca fue hecha por los campesinos"](https://archivo.contagioradio.com/la-erradicacion-de-la-coca-en-arauca-fue-hecha-por-los-campesinos/))

<iframe id="audio_27744554" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_27744554_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en] [su correo](http://bit.ly/1nvAO4u) [o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por ][Contagio Radio](http://bit.ly/1ICYhVU). 
