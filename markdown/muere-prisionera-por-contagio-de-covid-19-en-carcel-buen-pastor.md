Title: Muere prisionera por contagio de Covid 19 en Cárcel Buen Pastor
Date: 2020-07-23 13:00
Author: CtgAdm
Category: Actualidad, DDHH
Tags: #BuenPastor, #Cárcel, #covid19, #INPEC, #mujeres
Slug: muere-prisionera-por-contagio-de-covid-19-en-carcel-buen-pastor
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/04/cárcel.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

Esta mañana familiares de una **prisionera social del Buen Pastor, confirmaron el fallecimiento de la mujer** luego de 10 días estar hospitalizada y haber dado positivo a Covid 19. Según las reclusas a la fecha hay **12 mujeres** más en aislamiento preventivo. Sin embargo denuncian que el INPEC no estaría tomando las medidas correspondientes.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Las reclusas afirman que desde hace más de un mes vienen denunciando que en el centro penitenciario **no se están cumpliendo los protocolos de seguridad.** Disposiciones que incluyen lavado continúo de manos, tapabocas, gel antibacterial, entre otras.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Las prisioneras dicen que incluso el lugar de aislamiento en donde se encuentran mujeres con síntomas del virus, no estaría adecuado para las necesidades de ellas. Como tampoco cumpliría con los requerimientos para garantizar un aislamiento seguro. (Le puede interesar: "[Mujeres en Cárcel Buen Pastor denuncian negligencia del INPEC por Covid-19](https://archivo.contagioradio.com/mujeres-en-carcel-buen-pastor-denuncian-negligencia-del-inpec-por-covid-19/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Este centro de reclusión **tiene un 70% de hacinamiento**. De igual forma, las mujeres manifiestan que no reciben buena alimentación; que no se está prestando ningún tipo de atención médica y que se niega la visita virtual, por falta de una red estructural que lo permita. (Le puede interesar: "[Pdte Duque garantice la vida de las y los prisioneros en Colombia. ¡Medidas efectivas ya!](https://www.justiciaypazcolombia.com/pdte-duque-garantice-la-vida-de-las-y-los-prisioneros-en-colombia-medidas-efectivas-ya/))

<!-- /wp:paragraph -->
