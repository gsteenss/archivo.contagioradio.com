Title: Río Sumapaz en peligro por construcción de 8 micro-hidroeléctricas
Date: 2016-03-01 08:00
Category: Ambiente, Nacional
Tags: Cabrera, EMGESA, Río Sumapaz
Slug: rio-sumapaz-en-peligro-por-construccion-de-8-micro-hidroelectricas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/02/Zona-de-Reserva-Campesina-Cabrera.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Notimundo 

<iframe src="http://co.ivoox.com/es/player_ek_10628330_2_1.html?data=kpWjlJ2Xd5Ghhpywj5aUaZS1k5qah5yncZOhhpywj5WRaZi3jpWah5yncbOZpJiSo6nTb7Tpzsbdw9%2BPqc%2Bf0crZy8zWs4zk0NeYxdTSt9Xm1sjQy4qnd4a2lNOYxsqPfIzhysjf0ZLMrY6ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Sofía Blanco, ZRC Cabrera] 

###### [29 Feb 2016] 

Este sábado en el municipio de Cabrera, Cundinamarca, se realizó un Cabildo Abierto, con el objetivo de generar acciones frente al **proyecto hidroeléctrico de EMGESA, denominado El Paso,** que afectaría a los habitantes de cuatro municipios de  la región del Sumapaz: Venecia y Pandi, del departamento de Cundinamarca, y el municipio de Icononzo, en Tolima.

Se trata de una cadena de **8 minicentrales, que ocupan aproximadamente 50 kilómetros del río Sumapaz, interviniendo  32 veredas**. Aunque Emgesa ha asegurado que se trata de un proyecto amigable con el ambiente, el campesinado se encuentra preocupado porque los municipios serán atravesados por líneas de alta tensión, cuya radiación podrían afectar los cultivos. Además se estaría interviniendo una zona de amortiguamiento del parque natural del Sumapaz, según explica Sofía Blanco, integrante del Comité de impulso de la Zona de Reserva Campesina de Cabrera.

Para que el proyecto sea posible, la empresa deberá desviar el caudal del río usando **“unos tubos de 2.4 metros de diámetro desviando el afluente 7 kilómetros”,** indica Blanco, Así mismo se construirá dos bocatomas y  un canal de conducción  que rompería la montaña.

Con el cabildo, desde la Zona de Reserva Campesina de Cabrera, se impulsa la realización consultas populares en los municipios para impedir el desarrollo de las micro-centrales, que de acuerdo con Blanco, Emgesa ha dicho que necesitaría cerca de 11.5 m^3^ por segundo, cuando el caudal del río está en 1.5 m^3^.

**La empresa ha realizado un estudio ambiental, que de acuerdo con Sofía, sería un “estudio amañado”, en el que “hay muchas ausencias y mentiras”.** Pues aunque no se trata de un megaproyecto como El Quimbo, de todas formas se ocasionará alteraciones en los hábitats terrestres y acuáticos en diferentes tramos del río Sumapaz. Por el momento la comunidad continurá realizando acciones de movilización en contra de este proyecto de Emgesa.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u)  o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](https://archivo.contagioradio.com/). 
