Title: Víctimas de Bojayá y la Chinita exigen participación en pacto por la paz
Date: 2016-10-04 17:14
Category: Entrevistas, Paz
Tags: acuerdos de paz, FARC, Juan Manuel Santos, Pacto nacional
Slug: victimas-de-bojaya-y-la-chinita-exigen-participacion-en-pacto-por-la-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/10/la-chinita-y-bojaya.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: contagioradio.com] 

###### [04 Oct 2016]

Las víctimas de la Masacre de Bojayá, de la masacre de “La chinita” y víctimas de las Fuerzas Militares y de paramilitares coincidieron en que deben ser incluidos e incluidas en el pacto nacional que se ha convocado para buscar una salida luego del resultado negativo del plebiscito por la paz.

Casi todas ellas y ellos sienten que **no pueden seguir en la incertidumbre y manifiestan que ayudaron en la construcción de los acuerdo y deben participar también en esta etapa de reorganización.**

Para Leyder Castillo, víctima de la **masacre de Bojayá,** es necesario que se defiendan los acuerdos sobre Desarrollo Agrario, también deben defender la **Jurisdicción Especial de Paz porque es la única posibilidad de acabar con la impunidad** y de que hay justicia no solamente para ellos que son víctimas de las FARC sino para las víctimas de los paramilitares y de las Fuerzas Militares.

“*Nosotros construimos el acuerdo*” afirma Leyder y resalta que también deben estar en esta etapa para defender lo que para ellos y ellas es fundamental, como por ejemplo el punto de participación política que también ha sido blanco de ataques por parte de los promotores del NO “***nosotros queremos un sistema políticos diferente***” resalta Leyder.

Por su parte Silvia Berrocal, integrante del Comité de Víctimas de la masacre de la Chinita afirma que lastimosamente lo que percibe de los resultados del plebiscito, afirma que Apartadó le cumplió a la paz y por eso allí ganó el SI con un amplio margen, pero también cree que la gente votó por dos sectores políticos y no por la paz, “es como si hubiese dos presidentes y eso es una falta de respeto porque jugaron con el sentimiento de las víctimas”

Por otro lado agrega que en Apartadó no ganó el Uribe, “que les quede claro eso, aquí ganó el odio y el rencor”. Además Silvia afirma que le parece que no se han realizado propuestas claras porque Uribe no quería que en este gobierno se firmara la paz sino que sea en su gobierno pero resalta que no están dispuestas a seguir esperando 2 años para construir la paz.

La señora  Silvia resalta que ellos y ellas también deben ser incluidos si se trata de re negociar los acuerdos de paz que se alcanzaron con las FARC, puesto que ellos y ellas también construyeron esos acuerdos. “**Queremos participar en todo lo que viene y estamos firmes por la paz**” y aunque relata que lloró afirma que no va a dar ni un centímetro hacia atrás.

<iframe id="audio_13190932" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_13190932_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe><iframe id="audio_13190937" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_13190937_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>
