Title: Mujeres serán veedoras de la implementación de los Acuerdos
Date: 2017-02-07 16:49
Category: Mujer, Nacional
Tags: enfoque de género en los acuerdos de paz, Liga de las Mujeres, Mujeres serán veedoras
Slug: mujeres-seran-veedoras-de-la-implementacion-de-los-acuerdos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/02/mujeres-paz.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [7 Feb 2017] 

En el marco del Primer Encuentro Nacional de Género, organizado por el Equipo de Género de las FARC, se realizó en Bogotá el  taller ‘Implementación de los Acuerdos con Perspectiva de Género”, en dicho encuentro las participantes manifestaron que las mujeres serán veedoras "frente al cumplimiento de lo concerniente al tema de género" en los Acuerdos de Paz.

El taller, organizado por la Liga de las Mujeres, una organización de mujeres feministas que lleva trabajando 3 años en la capital, tuvo como objetivo discutir el papel de la sociedad civil y las mujeres frente al tratamiento, difusión y seguimiento de la implementación de los acuerdos firmados en La Habana.

### Mujeres y Participación de la Sociedad 

El fortalecimiento de organizaciones de mujeres, feministas y comunidades con orientación e identidad de género diversa, en espacios rurales y urbanos, será fundamental para que la participación de la sociedad sea efectiva y se garanticen los compromisos en el ámbito de educación, salud, vivienda, y trabajo digno para las mujeres colombianas.

Con este taller, se inauguró un conjunto de cuatro talleres, que contarán con distintas organizaciones y expertas invitadas, en el abordaje de las implicaciones y alcances del enfoque de género y los mecanismos de participación con que cuenta la ciudadanía para ser garantes de lo pactado, serán cada martes de las próximas semanas.

Lorena Hernández, una de las integrantes de la Liga de las Mujeres, señaló que el próximo encuentro que será el martes 14 de febrero, contará con la participación de Victoria Sandino integrante del equipo de género de las FARC y Juanita Barreto teniente de Navío e integrante de la delegación de paz del Gobierno, quienes abordaran el tema de la participación de las mujeres en la ciudad para la veeduría de los acuerdos, desde la perspectiva legislativa, de políticas públicas y participación.

Para mayor información de hora y lugar de los talleres, puede consultar en el sitio web de Facebook de La Liga de las Mujeres.

###### Reciba toda la información de Contagio Radio en [[su correo]
