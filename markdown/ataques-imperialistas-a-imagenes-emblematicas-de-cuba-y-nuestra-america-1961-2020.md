Title: Ataques imperialistas a imágenes emblemáticas de cuba y nuestra américa (1961- 2020)
Date: 2020-05-19 13:40
Author: AdminContagio
Category: Columnistas invitados, Opinion
Tags: América Latina, Ataque, dignidad, Latinoamérica
Slug: ataques-imperialistas-a-imagenes-emblematicas-de-cuba-y-nuestra-america-1961-2020
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/05/BOOOM.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/05/MACEOOO.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/05/BUSTO.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/05/JIJIJIJ.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/05/yyhyhyhy.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/05/hdjfhfr.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/05/Venezuela.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:heading {"align":"right","level":6,"textColor":"cyan-bluish-gray"} -->

###### Foto de: Globalización.ca {#foto-de-globalización.ca .has-cyan-bluish-gray-color .has-text-color .has-text-align-right}

<!-- /wp:heading -->

<!-- wp:heading {"level":4} -->

#### **¡SÍMBOLOS DE DIGNIDAD!**

<!-- /wp:heading -->

<!-- wp:heading {"level":5} -->

##### por: Renán Vega Cantor

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"right"} -->

“Estos impactos tienen efecto *boomerang*: reviven a nuestros héroes, y nos recuerdan las alertas que ambos nos legaran sobre la naturaleza del naciente imperialismo del Norte. Los balazos a las estatuas y bustos de nuestros mayores, no los hieren, los revitalizan y enaltecen. Son heridas que todos compartimos y sentimos como propias, irradian coraje y vergüenza y nos estimulan a defender sus ideas, principios y nuestra Cuba soberana”. [René González Barrios, Los actos terroristas no aniquilarán el valor legado por nuestros héroes](http://www.granma.cu/mundo/2020-05-02/los-actos-terroristas-no-aniquilaran-el-valor-legado-por-nuestros-heroes-02-05-2020-01-05-26)

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Las agresiones de todo tipo que llevan a cabo los Estados Unidos en diversos lugares del mundo, entre los que sobresalen los países de Nuestra América, han sido una constante histórica desde hace dos siglos.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Aparte de la destrucción, sufrimiento, muertos y heridos que dejan esas agresiones imperialistas, también se llevan a cabo ataques premeditados contra los símbolos que representan el sentido de independencia e identidad de los países agredidos.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Existe una política deliberada, para nada accidental, por parte de Estados Unidos de destruir los símbolos de un país, al mismo tiempo que lo ocupa, invade, se apropia de sus riquezas materiales y esclaviza a sus habitantes. Lo acontecido en Irak es un terrible ejemplo, si recordamos que a la par que se ocupaba el territorio se destruía el patrimonio cultural e histórico de pueblos milenarios, como se evidenció con el arrasamiento de museos y bibliotecas en los que se hallaban riquezas bibliográficas y documentales, así como legendarios artefactos materiales, que desaparecieron en una noche de niebla al más puro estilo nazi.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":5} -->

##### Esa destrucción cultural busca derrumbar las bases simbólicas sobre las cuales se sustentan la identidad de una nación.

<!-- /wp:heading -->

<!-- wp:paragraph -->

Desde este punto de vista, la destrucción material se complementa con la destrucción espiritual con el objetivo es derruir los soportes simbólicos que estructuran la vida de un pueblo, máxime cuando esos símbolos expresen el deseo irredento de independencia y soberanía.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Con esa guerra a los símbolos, Estados Unidos demuestra que está dispuesto a arrasar con lo que encuentre a su paso y, al mismo tiempo, sabe que con esos procedimientos aumenta la desmoralización de los agredidos. Esto no quiere decir que el ataque a los símbolos impida la rebelión y resistencia contra las fuerzas imperialistas y, dependiendo de la capacidad de lucha de los agredidos, puede convertirse en un factor adverso para los atacantes, si estos no pueden obtener un provecho inmediato, como lo veremos en los dos acontecimientos que relatamos en este artículo.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Estas consideraciones vienen al caso para evocar dos hechos ligados a las agresiones que Estados Unidos viene efectuando contra Cuba de manera ininterrumpida desde 1959, y han dejado heridas simbólicas, literalmente hablando, de una gran actualidad, por lo que expresan en sí mismas y porque los dos hechos mencionados, con una diferencia de cincuenta y nueve años, tienen coincidencias que permiten entender en la larga duración el carácter criminal del imperialismo estadounidense, el cual no se ha modificado ni un ápice en las últimas seis décadas.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Pero los hechos aludidos también demuestran que los símbolos atacados se convierten en una importante fuente de resistencia a la dominación imperialista, en la medida en que su permanencia misma los convierte en una imagen visible y reveladora de la agresión, que se mantiene a lo largo del tiempo, como si de la pieza de un museo vivo se tratara, para mostrar en forma directa y sin intermediarios, la alevosía destructiva de los Estados Unidos. 

<!-- /wp:paragraph -->

<!-- wp:heading {"level":5} -->

##### ***15 de abril de 1961: Invasión a Bahía Cochinos y tiroteo al busto de Antonio Maceo ***

<!-- /wp:heading -->

<!-- wp:paragraph -->

*“Con la soberanía nacional obtendremos nuestros naturales derechos, la dignidad sosegada y la representación de pueblo libre e independiente”. * Antonio Maceo 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El 15 de abril de 1961 se efectuó la invasión de los mercenarios a Cuba, financiados, armados y organizados por los Estados Unidos, que pretendían detener el avance de la revolución.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Las operaciones, apoyadas directamente por la CIA, se iniciaron con bombardeos realizados por aviones de mercenarios, portando las insignias de la Fuerza Aérea Revolucionaria, que despegaron desde Nicaragua, contra la base aérea de San Antonio de los Baños, la pista de Ciudad Libertad y el aeropuerto internacional Antonio Maceo de la ciudad de Santiago de Cuba. Con este ataque artero se pretendía destruir la reducida flotilla de aviones que tenía Cuba, como forma de facilitar el desembarco y avance de los contrarrevolucionarios de la Brigada 2506. Como resultado del ataque al aeropuerto de Santiago de Cuba fueron alcanzados siete aviones y fue averiada la Torre de Control. 

<!-- /wp:paragraph -->

<!-- wp:image {"id":84422,"sizeSlug":"large"} -->

<figure class="wp-block-image size-large">
![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/05/BOOOM.jpg){.wp-image-84422}  

<figcaption>
Bombardeo a las instalaciones del Aeropuerto Internacional Antonio Maceo, el 15 de abril de 1961, por fuerzas mercenarios que patrocinaban los Estados Unidos. 

</figcaption>
</figure>
<!-- /wp:image -->

<!-- wp:paragraph -->

Durante el bombardeo fue atacada la estatua de Antonio Maceo y Grajales, cuyo nombre honra a este aeropuerto. Una bala calibre 30 disparada desde el aire atravesó el busto del general de la independencia de Cuba. Este ataque se constituyó en algo así como el segundo asesinato del líder independentista. Recordemos que Antonio Maceo (1845-1896), conocido como el Titan de Bronce, fue un luchador indoblegable contra el colonialismo español, al que enfrentó con las armas en la mano durante dos guerras: la de los Diez Años (1868-1878), y la guerra final de independencia (iniciada en 1895) y durante la cual perdió la vida en 1896. De forma indirecta también participó en la Guerra Chiquita (1879), en la que no se le concedió el mando por el racismo de otros jefes insurreccionales.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Maceo era hijo del venezolano Marcos Maceo y de Mariana Grajales, de origen dominicano, y había nacido en 1845. Durante la guerra de independencia fue, junto con el dominicano Máximo Gómez, el principal conductor militar de la invasión a oriente. El 7 de diciembre de 1896, en Punta Brava Maceo fue herido y rematado a machetazos por la tropa española. 

<!-- /wp:paragraph -->

<!-- wp:image {"id":84423,"sizeSlug":"large"} -->

<figure class="wp-block-image size-large">
![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/05/MACEOOO.jpg){.wp-image-84423}  

<figcaption>
José Antonio de la Caridad Maceo y Grajales, General de la independencia de Cuba 

</figcaption>
</figure>
<!-- /wp:image -->

<!-- wp:paragraph -->

Maceo se distinguió por ser un militar de temple que defendía además de la independencia la libertad de los esclavos, en concordancia con sus orígenes raciales, y por haber soportado en carne propia la discriminación y el racismo.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Maceo no fue solamente un extraordinario estratega militar, sino también un pensador que señaló con convicción que “La libertad se conquista con el filo del machete, no se pide; mendigar derechos es propio de cobardes incapaces de ejercitarlos”. También había hecho una premonitoria advertencia, que se proyecta hasta el día de hoy: “Quien intente apropiarse de Cuba recogerá el polvo de su suelo anegado en sangre, si no perece en la lucha”. 

<!-- /wp:paragraph -->

<!-- wp:image {"id":84424,"sizeSlug":"large"} -->

<figure class="wp-block-image size-large">
![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/05/BUSTO.jpg){.wp-image-84424}  

<figcaption>
El busto de Antonio Macedo en el Aeropuerto de Santiago, con la huella de la bala disparada por los mercenarios en 1961. 

</figcaption>
</figure>
<!-- /wp:image -->

<!-- wp:paragraph -->

Con estos antecedentes, resulta evidente que el busto de Antonio Maceo fue acribillado en forma premeditada, en la medida en que simbólicamente encarna el ideal de libertad, independencia de Cuba y socialmente los sentimientos emancipadores de la población negra.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Para los mercenarios financiados por los Estados Unidos al dispararle a Maceo se estaba atacando a una figura que representa lo más genuino de la nación cubana, entre cuyos méritos sobresale su independencia y autodeterminación.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Pero como la invasión resultó siendo un fracaso absoluto, la estatua de Antonio Maceo se ha mantenido en pie en el mismo lugar que estaba cuando la atacaron los agresores ese 15 de abril de 1961.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Se mantiene invencible como fue la trayectoria del luchador popular contra el dominio colonial. Mejor aún, el orificio que atravesó la estatua se ha convertido en un nuevo símbolo de la resistencia y deseo de independencia indoblegable de Cuba.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por eso, allí en ese Aeropuerto se yergue como imagen de dignidad, mostrando su herida No. 27 como símbolo de resistencia, herida que se le causo 65 años después de muerto, puesto que en vida durante su lucha contra los colonizadores españoles fue herido en 26 ocasiones.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":5} -->

##### Esa herida abierta es un símbolo de la resistencia a la dominación de los Estados Unidos en nuestra América que Cuba encarna con altivez y gallardía. 

<!-- /wp:heading -->

<!-- wp:image {"id":84425,"sizeSlug":"large"} -->

<figure class="wp-block-image size-large">
![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/05/JIJIJIJ.jpg){.wp-image-84425}  

<figcaption>
El busto de Antonio Maceo visto de frente, muestra la huella de la agresión de 1961 

</figcaption>
</figure>
<!-- /wp:image -->

<!-- wp:heading {"level":5} -->

##### ***30 de abril de 2020: Ataque a Embajada de Cuba en Estados Unidos y tiroteo al busto de José Martí ***

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"right"} -->

“(...) *Patria* inaugura, en el número de hoy, una sección permanente de «Apuntes sobre los Estados Unidos», donde (...) se publiquen (...) aquellas calidades de constitución que, por su constancia y autoridad, demuestran las dos verdades útiles a nuestra América: –*el carácter crudo, desigual y decadente de los Estados Unidos– y la existencia, en ellos continua, de todas las violencias, discordias, inmoralidades y desórdenes de que se culpa a los pueblos hispanoamericanos*”. José Martí, *Patria*, marzo 23 de 1894. 

<!-- /wp:paragraph -->

<!-- wp:heading {"level":5} -->

##### 59 años después, el 30 de abril de este año fue atacada con armas de fuego la sede de la Embajada de Cuba en Estados Unidos.

<!-- /wp:heading -->

<!-- wp:paragraph -->

Este ataque tampoco es accidental, y responde a las continuas agresiones de la primera potencia mundial, incrementadas en los últimos meses, como parte de la política de doblegar al país caribeño.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En la madrugada de ese jueves fue alcanzado por 32 disparos la sede de la Embajada, sin que ninguna autoridad policial o militar de Estados Unidos lo impidiera. Después del ataque con un fusil de asalto (algo típico en la política de muerte cotidiana que caracteriza la vida interna de los Estados Unidos) fue detenido el agresor, un individuo de origen cubano y residente en Texas, identificado como Alexander Alazo. Este individuo quien afirmó que si hubiera salido el Embajador de Cuba o algún funcionario de la sede atacada le hubiera disparado a matar por considerarlo su enemigo, portaba una bandera de Cuba con esta confusa e incoherente leyenda (en inglés), pero cuyo sentido profundo si se puede evidenciar: “Deja de mentirle a la gente. El respeto. Trump 2020. Estados Unidos, Tierra y Familia”. Lo que queda claro en el confuso mensaje es el apoyo a Trump y allí se encuentran los nexos con los ataques que desde las altas esferas del poder en Estados Unidos se han orquestado contra Cuba en los últimos meses y han arreciado la permanente política hostil del imperialismo, contra la isla caribeña iniciada hace 61 años y que prolonga los apetitos colonialistas de Estados Unidos que comenzaron en las primeras décadas del siglo XIX.

<!-- /wp:paragraph -->

<!-- wp:image {"id":84426,"sizeSlug":"large"} -->

<figure class="wp-block-image size-large">
![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/05/yyhyhyhy.jpg){.wp-image-84426}  

<figcaption>
Bandera cubana que llevaba el atacante a la Embajada de Cuba en Estados Unidos 

</figcaption>
</figure>
<!-- /wp:image -->

<!-- wp:paragraph -->

En el ataque al edificio de la Embajada, en teoría protegido por Convenios Internacionales, se dispararon 32 proyectiles de un fusil AK-47 con poder mortífero y uno de ellos atravesó la estatua del prócer de la independencia de Cuba, José Martí. Esto tampoco es accidental, ya que recientemente han sido atacados en La Habana algunas estatuas de José Martí por mercenarios financiados desde Miami. 

<!-- /wp:paragraph -->

<!-- wp:image {"id":84427,"sizeSlug":"large"} -->

<figure class="wp-block-image size-large">
![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/05/hdjfhfr.jpg){.wp-image-84427}  

<figcaption>
La figura de José Martí a la entrada de la Embajada de Cuba en Washington. Pueden verse los orificios de los disparos, entre ellos uno en el costado izquierdo de la estatua. 

</figcaption>
</figure>
<!-- /wp:image -->

<!-- wp:paragraph -->

Además, con los sucesos recientes en la Embajada de Venezuela en Washington, que en forma ilegal le fue entregada a los títeres de Guaidó y de donde fueron sacados a las patadas algunos ciudadanos estadounidenses que la defendían, no es dudoso sospechar de la participación directa de funcionarios del gobierno de Trump, que han azuzado a un aparente “desquiciado mental” (pero acaso Donald Trump, Marco Rubio, Mike Pompeo no lo son) para que como “lobo solitario” atacara la embajada en pleno corazón de Estados Unidos.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Ahora bien, el ataque a Martí tiene las mismas dimensiones simbólicas que el realizado a Antonio Maceo, puesto que el “apóstol de Cuba” es la figura emblemática de la independencia de nuestra América, rubricada con su muerte en combate (como Maceo). Y, además, porque Martí vislumbró al naciente imperialismo y señaló la imperiosa necesidad de combatirlo, como cuando dijo en una carta el 18 de mayo de 1895, un día antes de morir, que su lucha se daba para “impedir a tiempo con la independencia de Cuba que se extiendan por las Antillas los Estados Unidos y caigan, con esa fuerza más, sobre nuestras tierras de América. (...) Viví en el monstruo, y le conozco las entrañas: —y mi honda es la de David”.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

La herida que queda en el costado izquierdo del busto de José Martí se convierte desde ahora mismo en una huella simbólica de la intolerancia, el odio y el sentido de destrucción que caracteriza a los Estados Unidos y también de la capacidad de resistencia de un pueblo valeroso, que personificó como pocos el gran poeta, pensador y luchador de nuestra América.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Estos ataques a los monumentos de dos luchadores de nuestra América no solamente se dirigen a Cuba sino a toda nuestra patria grande, la misma que en estos momentos sigue siendo agredida, como lo ilustra el reciente desembarco en las costas venezolanas, de mercenarios y asesinos, respaldados por Donald Trump y compañía y entrenados en territorio de Colombia, como buen ejemplo de lo que significa ser el Caín del continente. Pero las heridas de las estatuas de Antonio Maceo y José Martí, las que siguen incólumes en los mismos lugares donde fueron atacadas, simbolizan el sentido de dignidad de todos los que se niegan a que seamos un suburbio miserable de Miami.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

[Ver mas: Nuestros columnistas](https://archivo.contagioradio.com/opinion/columnistas/)

<!-- /wp:paragraph -->
