Title: Su compromiso con la educación hoy tiene bajo amenaza a profesores del Cauca
Date: 2019-09-12 13:08
Author: CtgAdm
Category: DDHH, Educación
Tags: ASOINCA, Cauca
Slug: compromiso-con-la-educacion-hoy-tiene-bajo-amenaza-a-profesores-del-cauca
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/09/Asoinca-Cauca.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/09/Asoinca-Cauca-1.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Asoinca] 

[A lo largo de la primera semana de septiembre se han conocido reiteradas amenazas por parte de las Águilas Negras a través de panfletos y llamadas telefónicas contra el magisterio y maestros del Cauca. Víctor Hugo Jiménez,  coordinador de la Comisión de derechos humanos de la Asociación de Institutores y Trabajadores de la Educación del Cauca (Asoinca) se refirió a las constantes agresiones que sufre el gremio en medio de la difícil situación que vive el departamento.]

### ¿Cuál es el rol que desempeña un maestro del Cauca?

Nosotros como organización sindical, filial de FECODE  venimos siendo hostigados y amenazados, el año pasado tuvimos que enterrar a 8 de nuestros compañeros afiliados a nuestro organización y esto tiene que ver con el protagonismo político que ejercemos en el Cauca, cuando decimos político no nos referimos al ejercicio sindical toda vez que Asoinca es la única organización sindical que no participa de la vía electoral, nuestra postura política es anti electoral pero eso en el departamento tiene sus implicaciones, porque hay grupos y organizaciones que quieren que nos sometamos a ese ejercicio.

> ASOINCA por su liderazgo, y la defensa de los derechos humanos del magisterio, hoy nuevamente es víctima de amenazas del paramilitarismo rampante.[@IvanDuque](https://twitter.com/IvanDuque?ref_src=twsrc%5Etfw) [@NancyPatricia\_G](https://twitter.com/NancyPatricia_G?ref_src=twsrc%5Etfw) [@1oscarcampo](https://twitter.com/1oscarcampo?ref_src=twsrc%5Etfw) [@ONUHumanRights](https://twitter.com/ONUHumanRights?ref_src=twsrc%5Etfw) [@DefensoriaCol](https://twitter.com/DefensoriaCol?ref_src=twsrc%5Etfw) [@nytimes](https://twitter.com/nytimes?ref_src=twsrc%5Etfw) [@SuperPopayan](https://twitter.com/SuperPopayan?ref_src=twsrc%5Etfw) [@CMILANOTICIA](https://twitter.com/CMILANOTICIA?ref_src=twsrc%5Etfw) [pic.twitter.com/LIkoerpnCL](https://t.co/LIkoerpnCL)
>
> — ASOINCA CAUCA (@ASOINCACAUCA) [September 3, 2019](https://twitter.com/ASOINCACAUCA/status/1168973064098537473?ref_src=twsrc%5Etfw)

<p>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
</p>
[Los docentes en particular de las zonas rurales además de su ejercicio pedagógico, se convierten en los consejeros, en los presidentes de las junta de acción comunal, en los defensores del territorio. A su vez el departamento del Cauca cuenta con varias  particularidades:  las multinacionales, el narcotráfico y las personas que están reclamando sus tierras y que fueron desplazadas por los terratenientes y a su servicio sus ejércitos paramilitares, eso hoy tiene no solo al magisterio sino a todos los movimientos sociales y populares del Cauca en alto riesgo.]

[La semana pasada hubo tres masacres, este fin de semana se registraron otras tres masacres, son hechos que la Fiscalía, la Defensoría alcanzan a registrar, pero las comunidades señalan que son mucho más muertos que los que aparecen en la prensa. [(Le puede interesar:  ¿Qué está pasando en Cauca? análisis del líder campesino Óscar Salazar)](https://archivo.contagioradio.com/que-esta-pasando-en-el-cauca-un-analisis-del-lider-campesino-oscar-salazar/)]

### **¿Qué factores son los que hoy recrudecen la violencia del departamento?** 

Desde los análisis que hemos hecho, hay una política de despojo y desplazamiento y esta favorece a los grandes narcotraficantes, el despojo es para aquellos que necesitan grandes hectáreas para poder cultivar la coca o la marihuana, el segundo factor son aquellos que vienen con el beneplácito del Gobierno, que son las grandes multinacionales que además de llegar a violentar el territorio, violentan a las comunidades con el Ejército.

[Un tercer factor después de la gran minería es el asesinato de reclamantes de tierras, y otro cuarto elemento son las violencias propiciadas por la farsa electoral, nosotros decimos que quien escruta es quien elige a la misma oligarquía de este país, esos son los elementos que están en las violencias del Cauca: narcotráfico, despojo de tierras, minería legal, reclamantes de tierra y politiquería.]

### ¿De cara a las amenazas qué medidas están tomando desde Asoinca?

[Lo único que nosotros tenemos al organizar nuestras comunidades es la movilización, no tenemos otra medida de protección para decirles a los violentos que somos miles que seguiremos desde el Cauca y el territorio defendiéndolo.]

[Aquí cada que sucede una masacre viene el presidente de República o el ministerio de Defensa pare decir que realizará consejos de seguridad y la única respuesta es más violencia, dicen que traen más Ejército, más Policía, pero uno nunca escucha un discurso de inversión social, no necesitamos más agentes de violencia ni bota militar, la gente está asustada de ver tantos grupos armados.]

### ¿Cuántos profesores del Cauca se encuentran en la actualidad bajo amenaza?

[Nosotros hoy estamos conformados como junta por 25 directivos, los que están en su totalidad amenazados por su participación además de los paros, por el tema reivindicativo, por denunciar el pésimo programa de alimentación escolar, por el tema salarial, por el pésimo servicio de salud, además ASOINCA viene siendo solidario con todos los movimientos sociales del Cauca.]

[Además, el papel que ejercemos en cada municipio ha llevado a que toda la junta directiva esté bajo amenaza, nos preocupan los últimos panfletos que no solo están dirigidos a toda la junta sino a toda la base sindical, profesores, docentes, directivos. Todos son objetivo militar.   [(Lea también: Asesinato del profesor Orlando Gómez en Cauca es un atentado contra la educación)](https://archivo.contagioradio.com/orlando-gomez-atentado-educacion/)]

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
