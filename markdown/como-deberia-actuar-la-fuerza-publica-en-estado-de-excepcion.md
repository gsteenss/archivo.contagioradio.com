Title: ¿Cómo debería actuar la Fuerza Pública en estado de excepción?
Date: 2020-04-08 18:32
Author: CtgAdm
Category: Actualidad, DDHH
Tags: #Abusodepoder, #Comparendo, cuarentena, policia
Slug: como-deberia-actuar-la-fuerza-publica-en-estado-de-excepcion
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/07/codigo-de-policia-2.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

De acuerdo con Camila Forero, defensora de derechos humanos de la Comisión de Justicia y Paz, si bien Colombia se encuentra en un estado de excepción, producto de la pandemia, eso no significa que sean permitidos los abusos de autoridad de la Fuerza Pública. Acá le contamos **¿cómo debería actuar la Fuerza Pública en estado de excepción?**

<!-- /wp:paragraph -->

<!-- wp:heading -->

No todo vale en la Cuarentena
-----------------------------

<!-- /wp:heading -->

<!-- wp:paragraph -->

Según la defensora es importante que la población comprenda que en un estado de excepción no se suspende ningún tipo de derecho. Razón por la cual tanto la Policía como los entes estatales siguen actuando en razón de velar por la garantía de los mismos.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

"Esto quiere decir que **no pueden extralimitarse en el rol que tienen como policía** y como entidad de la Fuerza Pública, sino actuar conforme a los derechos humanos" señala. (Le puede interesar: ["Denuncian abuso de poder de la Policía en medio de cuarentena"](https://archivo.contagioradio.com/abuso-de-poder-por-parte-de-la-policia-en-medio-de-cuarentena/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por este motivo Forero resalta que **ningún tipo de abuso, desde el acoso sexual hasta la extorsión, son permitidos a esta autoridad** y que en su caso, es mucho más reprochable porque son los garantes de que estos hechos no sucedan.

<!-- /wp:paragraph -->

<!-- wp:heading -->

¿Los comparendos desmesurados?
------------------------------

<!-- /wp:heading -->

<!-- wp:paragraph -->

En un reciente vídeo, una doctora del Hospital Simón Bolívar rechazaba la multa que le impartía un oficial por estarse movilizando en su vehículo tras hacer mercado. La mujer argumentaba que lo había hecho porque salía de turno y necesitaba llevar alimentos a su familia.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Como ella, muchas otras personas están denunciado hechos similares. Según Forero, el decreto 457, expedido para la cuarentena, **tiene 34 causales por las cuales las personas pueden salir de su hogar** y es importante que la ciudadanía las conozca. Sin embargo afirma que tampoco hay una pedagogía sobre esta norma. (aquí le adjuntamos el [link para que las conozca](https://id.presidencia.gov.co/Paginas/prensa/2020/Gobierno-Nacional-expide-Decreto-457-mediante-el-cual-imparten-instrucciones-para-cumplimiento-Aislamiento-Preventiv-200323.aspx)).

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

"Se ha presentado que las personas salen a comprar víveres, pero les piden excesivas pruebas, dinero, lugares de habitación, direcciones... No obstante **las causales son explicitas frente a las excepciones para violar el aislamiento**" expresa.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En ese sentido Forero afirma que la labor del policía sería analizar las razones que llevan a la persona a salir de su hogar, **si son fortuitas o por fuerza mayor,** y posteriormente, si no esta amparado por ninguna de las 34 causales, proceder con el comparendo.

<!-- /wp:paragraph -->

<!-- wp:heading -->

¿Qué puede hacer la persona si está siendo víctima de un abuso de poder?
------------------------------------------------------------------------

<!-- /wp:heading -->

<!-- wp:paragraph -->

Si usted recibió un comparendo y considera que es víctima de abuso de autoridad, tiene un plazo de tres días para realizar la apelación del mismo. De igual forma asevera que las personas pueden grabar estas situaciones y denunciarlas.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

"Es muy importante que la población haga públicas esas situaciones, porque este tiempo de cuarentena permite que se presenten muchas situaciones así, no solo por parte de la Policía y Fuerza Pública, sino por los demás estamentos como el gubernamental, que aprovechan de este momento para abusar del poder" asegura la defensora.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->
