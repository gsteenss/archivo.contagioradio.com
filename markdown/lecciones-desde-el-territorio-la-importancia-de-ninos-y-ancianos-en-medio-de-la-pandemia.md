Title: Lecciones desde el territorio: la importancia de niños y ancianos en medio de la pandemia
Date: 2020-06-20 14:46
Author: CtgAdm
Category: Actualidad, Comunidad
Tags: comunidades, niñez
Slug: lecciones-desde-el-territorio-la-importancia-de-ninos-y-ancianos-en-medio-de-la-pandemia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/06/WhatsApp-Image-2020-06-18-at-6.05.07-PM.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: Niños y tercera edad/ Contagio Radio

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Organizaciones como UNICEF han alertado que alrededor de todo el mundo el brote del Covid-19 está afectando directamente el estilo de vida, salud y el sustento de muchas personas a medida que se sobrecargan los sistemas de salud, se cierran las escuelas y las familias luchan por continuar a flote. A su vez advierte que las repercusiones de la pandemia serán notables y que afectarán directamente los derechos de la población infantil.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

En Colombia las medidas adoptadas por el Gobierno han dejado en desventaja escolar a cerca de seis millones niños y niñas quienes no han podido poder recibir clases virtuales, lo que dejan en evidencia que en el país hay un atraso en términos de avances de conectividad especialmente en zonas rurales y que existe una deuda con la educación y la niñez colombiana que queda relegada a su hogar en medio de la pandemia.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Para **José Francisco Álvarez y John Jader Córdoba, maestros y parte del proceso educativo en la zona humanitaria de Camelias ubicada en territorio colectivo de Curbaradó,** el valor que han tenido los niños y niñas al interior de la comunidad, incluso en épocas de pandemia es muy relevante pues siempre están en la sintonía de aprender y de esos aprendizajes poder aportar significativamente.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

"Desde el juego, desde la integración con sus otros amigos y el escuchar a los adultos genera una conclusión comunitaria, es un relevo generacional, el niño es el presente pero también es el futuro por eso desde las zonas humanitarias le damos su lugar a niños y niñas dentro del proceso", explica José Francisco quien sostiene que la prueba de ello, está en él y en John Jader quienes pese a juventud, se les dio importancia en el proceso de la comunidad desde muy pequeños y hoy aportan a la construcción de la paz

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Así mismo John Jader señala que cambian las formas de relacionamiento del campo a la ciudad, sin embargo en la zona rural se le da importancia al **"valorar a los niños y a los ancianos, pues los niños son los renacientes que van a ocupar el puesto de nuestros ancestros"** y quienes aportarán el conocimiento a las generaciones venideras.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### El adulto mayor garantiza la pervivencia cultural en el territorio

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Desde Putumayo, el exgobernador indígena del Pueblo Nasa, Luis Ulcué también resaltó la trascendencia del adulto mayor, señalando como en los resguardos, su figura es una "cuna de sabiduría" que a través de sus formas de organización, escenarios inter-generacionales y la cosmovisión nasa, permiten recrear las réplicas de los saberes ancestrales pues en ellos está el potencial que requieren las nuevas generaciones para la pervivencia cultural.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

"Cosa contraria pasa con la política de los gobiernos nacionales, locales y departamentales que ven a los mayores como un estorbo" resalta Ulcué que considera que desde las políticas se les margina y se les aisla para "expropiarlos y lucrar a un sector financiero". [(Le puede interesar: 'Cuarentena con dignidad', movilización social en Arauca en medio del Covid-19)](https://archivo.contagioradio.com/cuarentena-con-dignidad-movilizacion-social-en-arauca-en-medio-del-covid-19/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Al respecto, los maestros de la Zona Humanitaria de Camelias también resaltan el rol del adulto mayor como una pieza clave para que no se pierda la ancestralidad, "el territorio es lo que nos une y lo que permite que adultos y jóvenes estemos en una misma línea de lucha, todos somos una familia, todos estamos entrelazados con todos, como una telaraña", explica Francisco.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### La fragilidad de niños y niñas en medio de la pandemia

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Con el inicio del Covid-19 y el cierre de las escuelas, se ha interrumpido la educación de más de 1.570 millones de estudiantes lo que equivale a un 91% de todo el mundo, según la UNICEF, advirtiendo que, en casos de aislamientos anteriores, ha sido demostrado que los niños que dejan de ir a la escuela durante periodos de tiempo prolongados tienen menos probabilidades de regresar al reanudar clases.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Al respecto, **María Paula Martínez, directora de** [**Save the Children en Colombia**](https://twitter.com/savechcolombia)alerta que hay situaciones de riesgo que han aumentado para la población infantil con la llegada del Covid-19, entre ellas "la falta de servicios y de derechos es algo que nos ha acompañado hace mucho tiempo pero con la pandemia es más notorio que esos derechos no están llegando". [(Le puede interesar: Sin luz ni internet no puede haber educación virtual de calidad)](https://archivo.contagioradio.com/a-colombia-llego-la-educacion-virtual-mientras-el-20-de-los-estudiantes-no-tenia-ni-luz/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Alerta también sobre el maltrato, el trabajo infantil, un incremento en el reclutamiento y las redes de trata de personas, al respecto organizaciones como la Coalición contra la vinculación de niños, niñas y jóvenes al conflicto armado en Colombia (Coalico) han revelado que en los primeros cinco meses del años las cifras de reclutamiento forzado aumentaron en un 113 % con respecto a 2019 siendo 128 los niños, niñas y adolescentes reclutados por grupos armados. [(Lea también: Comunidades rurales trabajan por el goce efectivo de los derechos de las niñas y niños)](https://archivo.contagioradio.com/comunidades-rurales-trabajan-por-el-goce-efectivo-de-los-derechos-de-las-ninas-y-ninos/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Mientras esta situación ocurre en departamentos como **Ñariño Arauca, el Catatumbo, Cauca y Valle,** desde el Gobierno también se han planteado como respuesta las catedras en los colegios abordando un modelo militar. "Vernos afectados por grupos al margen de la ley es algo común y aprovecharse del momento crítico de este virus es una estrategia de importancia para estos grupos, el hecho de que el Gobierno quera inducir a que los jóvenes presten el servicio militar tampoco es conveniente, si hablamos de educación, no los vamos a educar para que vayan por las armas, es mejor educar desde la construcción de paz" argumenta John Jader.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Francisco concluye que en lugares como el Bajo Atrato, las instituciones no han aportado al desarrollo y que los adelantos "han sido por nuestros medios organizados y unidos, la auto protección en el ámbito del Covid-19, así mismo se han adelantado otras medidas históricamente para protegernos de los grupos armados", por lo que afirma que es necesario que el Estado comprenda que las problemáticas de salud, ecónomicas, de educación y culturales requieren un enfoque incluyente en el que se tengan en cuenta las prioridades y necesidades de las poblaciones más vulnerables.

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->
