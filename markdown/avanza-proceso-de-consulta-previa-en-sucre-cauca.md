Title: Avanza proceso de consulta previa en Sucre, Cauca
Date: 2015-02-02 19:23
Author: CtgAdm
Category: Ambiente, Resistencias
Tags: Cauca, Consulta Previa, Explotación petrolera, Mineria, Sucre
Slug: avanza-proceso-de-consulta-previa-en-sucre-cauca
Status: published

###### Foto: Comisión de Justicia y Paz 

En el Municipio de Sucre, en el sur occidente colombiano, la comunidad se organiza en asambleas informativas para adelantar un proceso de consulta previa, ante la posible **explotación petrolera en la región**.

La comunidad, mayoritariamente campesina, realizará en el mes de febrero una **caracterización predial del territorio**, que les permita reconocer los terrenos, sus riquezas naturales y avanzar en la titulación de los mismos.

La **consulta previa se realizaría** una vez terminado el proceso de caracterización, hacia finales del mes del marzo del 2015.
