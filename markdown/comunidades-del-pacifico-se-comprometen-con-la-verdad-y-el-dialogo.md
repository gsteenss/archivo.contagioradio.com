Title: Comunidades del Pacífico se comprometen con la verdad y el diálogo
Date: 2019-10-10 13:42
Author: CtgAdm
Category: Comunidad, Entrevistas
Tags: Chocó, Comisión Interétnica de la Verdad por el Pacífico, Comisión para el Esclarecimiento de la Verdad, Esclarecimiento de la Verdad, Tumaco
Slug: comunidades-del-pacifico-se-comprometen-con-la-verdad-y-el-dialogo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/Pacífico.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:] 

Desde la **Comisión Interétnica de la Verdad por el Pacífico (CIVP)**, la sociedad civil le ha apostado a construir estrategias que permitan el esclarecimiento y el derecho a la verdad, sobre todo para las víctimas del conflicto armado, quienes pese a la crisis humanitaria que se vive en la región, buscan aportar a la armonía en el territorio.

Leyner Palacios, defensor de DDHH y líder social, quien funge como integrante de la Secretaría Técnica de la CIVP, señala que en principio son 34 organizaciones étnico-territoriales y de víctimas que investigarán sobre el verdadero impacto que ha tenido la guerra en los municipios de departamentos como **Nariño, Chocó, Cauca y Valle del Cauca.** De este modo se documentarán las afectaciones ambientales, territoriales, espirituales y sociales cometidas, "toda esa cultura y cosmogonía del pueblo afro e indígena fue atacada brutalmente, logrando que las comunidades no pudieran acudir a las maneras tradicionales de resolver situaciones de toda la cotidianidad", menciona Leyner.

### Una verdad que le compete a todos los sectores del Pacífico

El líder social señala que esta apuesta busca aportar a la convivencia y la forma de fomentar diálogos en la región, **"queremos generar escenarios de construcción de paz y crear pactos de convivencia que permitan que los actores cambien comportamientos de agresión hacia las comunidades"** y así garantizar medidas de no repetición que incluya a los grupos armados, sociedad ciivil y empresarios que hacen parte de un territorio donde existen numerosos intereses de agrindustrias, minero enegéticas y proyectos portuarios que guardan un nexo muy importante con el conflicto armado. [(Le puede interesar: Comisión de la Verdad llega a Buenaventura y fortalece su presencia en el Pacífico)](https://archivo.contagioradio.com/comision-de-la-verdad-llega-a-buenaventura-y-fortalece-su-presencia-en-el-pacifico/)

La iniciativa, señala Leyner, también ha recibido un acompañamiento sólido por parte de la iglesia católica, otorgando confianza a las comunidades y dando garantías para que se pueda llegar a muchos más territorios, de igual forma se ha logrado establecer una alianza con la Comisión de la Verdad para realizar un trabajo conjunto, que esperan también pueda ser llevado a la Jurisidición Especial para la Paz  [(Lea también: 'Bojayá entre fuegos cruzados', el documental que reconstruye la verdad de un pueblo)](https://archivo.contagioradio.com/bojaya-entre-fuegos-cruzados-el-documental-que-reconstruye-la-verdad-de-un-pueblo/)

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
