Title: Comunidades de Quinchía en Risaralda le dicen no a la minería
Date: 2017-03-14 09:48
Category: Ambiente, Voces de la Tierra
Tags: Acueductos Comunitarios Quinchía, Anglo Gold Ashanti, Quinchía Risaralda
Slug: protesta-mineria-quinchia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/03/WhatsApp-Image-2017-03-13-at-2.05.45-PM.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Tejido Territorial] 

###### [13 Mar 2017] 

En Quinchía Risaralda, más de 500 personas se movilizaron el pasado 12 de marzo para rechazar las actividades extractivas de la multinacional Anglo Gold Ashanti, la cual pretende ampliar la mina de La Colosa con el Proyecto ‘La Colosa Regional’, **son 11.000 hectáreas que han sido solicitadas para el proyecto que afectaría los municipios de Quinchía, Filadelfia, y Neira, principalmente.**

Dentro de las consignas que se escucharon ese día lo sparticipantes denunciaron amenazas de desplazamiento a la comunidad Emberá Karambá y amenazas a integrantes de la asociación de Acueductos Comunitarios de Quinchía.

Antonio de Jesús Guevara, presidente de la Asociación de Acueductos Comunitarios, manifestó que las instituciones estatales como la Corporación Autónoma de Risaralda, ha dado facilidades a la Anglo Gold “**dándoles concesiones sin mayor problema, mientras que a los acueductos rurales nos ponen bastantes obstáculos para conseguir las concesiones”.**

También denunció que directivas de la multinacional y operadoras filiales “**están comprando tierras donde hay fuentes hídricas, están privatizándolas** entonces con eso y sin concesiones, no tenemos herramientas para defender el agua potable que es para el pueblo”.

### **Quinchía, tierra para la agricultura** 

Guevara resaltó que dicha problemática ha afectado el tejido social de la comunidad, **“nosotros somos campesinos, nuestro territorio es de vocación agricultora,** es lo que los abuelos han trabajado, por eso decimos que no queremos minería”. ([Le puede interesar: 77 Familias Emberá Karambá serían desplazadas por la Anglo Gold Ashanti](https://archivo.contagioradio.com/77-familias-embera-karamba-serian-desplazadas-por-la-anglo-gold-ashanti/))

Explicó que las comunidades tanto indígenas como campesinas de los municipios afectados, se encuentran en inminente riesgo de desplazamiento, indicó que la multinacional ha ofrecido por sus viviendas de 50 a 100 millones de pesos, **“un dinero que no nos alcanza para nada, el dinero se acaba, la agricultura no”.**

Guevara también confirmó las declaraciones de la Gobernadora de la Parcialidad Indígena Karambá, Edit Taborda, quien reveló las amenazas de la Anglo Gold de destruir la única vía de acceso de estas comunidades, el centro de salud y los lugares de trabajo de orfebrería.

Por último, Guevara manifestó que la comunidad adelantará acciones legales apelando a los impactos ambientales de estas actividades, **exigirá la realización de una Consulta Popular para que el pueblo decida si quiere o no minería y continuarán movilizándose para exigir respeto al territorio, el agua y la vida.**

<iframe id="audio_17535837" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_17535837_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en su correo o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por Contagio Radio. 
