Title: 78% de los casos de asesinatos a periodistas se encuentran en la impunidad: FLIP
Date: 2020-02-18 16:37
Author: CtgAdm
Category: DDHH, Judicial
Tags: FLIP, medios de comunicación, Periodistas, violencia
Slug: 78-de-los-casos-de-asesinatos-a-periodistas-se-encuentran-en-la-impunidad-flip
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/02/8390aa5ce8e50f32cee2f668a00694aa_XL.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/02/image.png" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/02/WhatsApp-Image-2020-02-18-at-3.50.27-PM.jpeg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/02/WhatsApp-Image-2020-02-18-at-3.58.54-PM.jpeg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:html -->

<figure class="wp-block-audio">
<audio controls src="https://co.ivoox.com/es/jonathan-block-sobre-informe-flip-callar_md_47928260_wp_1.mp3">
</audio>
  

<figcaption>
Entrevista &gt; Jonathan Bock | director ejecutivo de la FLIP

</figcaption>
</figure>
<!-- /wp:html -->

<!-- wp:paragraph -->

*"Callar y fingir, la censura de siempre"* es el informe presentado por la Fundación para la Libertad de Prensa (FLIP) y que habla sobre los ataques físicos y psicológicos que han sufrido periodistas en el 2019, así como el control de información e ideas en los medios *"que buscan que la prensa calle y al mismo tiempo finja y pretenda que todo está en orden"*.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### "La intimidaciones a periodistas se han multiplicado desde la firma de los acuerdos de paz": FLIP

<!-- /wp:heading -->

<!-- wp:paragraph -->

La Fundación registró en 2019, **515 ataques contra periodistas**, de los cuales **137 correspondieron a agresiones físicas, 4 a secuestros y 2 a homicidios**, ante ello **Jonathan Bock, director ejecutivo de la FLIP** agregó *"hay zonas vedadas para la prensa en el país como, Caloto, Corinto, Miranda (Cauca) ; Llorente, Arauquita y Puerto Asís que se convirtieron en puntos ciegos, **donde no se puede hacer prensa ni se puede ir a cubrir lo que está pasando**".*

<!-- /wp:paragraph -->

<!-- wp:quote -->

> *"Regresamos a la época donde los periodistas tienen que exiliarse en el extranjero; donde los columnistas y las voces críticas son tachados de las parrillas; y donde, las palabras y expresiones gráficas son borradas de las paredes".*
>
> <cite>FLIP</cite>

<!-- /wp:quote -->

<!-- wp:heading {"level":3} -->

### "40 días de manifestaciones y 66 periodistas agredidos en su mayoría por agentes de la Fuerza Pública"

<!-- /wp:heading -->

<!-- wp:paragraph -->

Durante **los 40 días de manifestaciones y cacerolazos, en el marco del paro nacional de 2019 la FLIP registró 66 periodistas agredidos**, afirmando, *"este fue el escenario más violento, de la historia reciente, contra la prensa en un contexto de protesta social"*, y señaló que la mayoría de los casos, eran de reporteros que estaban grabando las irregularidades en los procedimientos policiales.

<!-- /wp:paragraph -->

<!-- wp:image {"id":80894,"sizeSlug":"large"} -->

<figure class="wp-block-image size-large">
![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/02/WhatsApp-Image-2020-02-18-at-3.50.27-PM.jpeg){.wp-image-80894}  

<figcaption>
*Registro de la FLIP - Informe "Callar y fingir"*

</figcaption>
</figure>
<!-- /wp:image -->

<!-- wp:heading {"level":3} -->

### 78% de los casos de asesinatos a periodistas se encuentran en la impunidad

<!-- /wp:heading -->

<!-- wp:paragraph -->

Según Bock en los casos registrados de asesinatos a periodistas desde 1977 se han logrado algunos avances, *"sin embargo es evidente que estos no son suficientes ni mucho menos han causado trascendentales en el cumplimiento de la justicia hacia los responsables de esto"*. (Le puede interesar: <https://archivo.contagioradio.com/una-guerra-ideologica-y-cultural/>)

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por su parte el informe señala que, *"detrás de los anuncios grandilocuentes de justicia que ocasionalmente aparecen, **persisten las dilaciones injustificadas, la falta de investigaciones serias, eficientes, imparciales así como la judicialización y sanción de los crímenes contra la prensa**".*

<!-- /wp:paragraph -->

<!-- wp:image {"align":"center","id":80895,"sizeSlug":"large"} -->

<div class="wp-block-image">

<figure class="aligncenter size-large">
![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/02/WhatsApp-Image-2020-02-18-at-3.58.54-PM.jpeg){.wp-image-80895}
</figure>

</div>

<!-- /wp:image -->

<!-- wp:heading {"level":3} -->

### Plataformas digitales como el nuevo campo de batalla

<!-- /wp:heading -->

<!-- wp:paragraph -->

Según la FLIP en los periodos de 2012 y 2019, **se han presentado por lo menos quince proyectos de ley que limitan la expresión en internet,** de estos trece, no satisfacen los requisitos que los estándares constitucionales exigen. (Le puede interesar: ["Callar y fingir, informe 2019 FLIP"](https://www.flip.org.co/images/Documentos/Informe_Anual_FLIP_2019_Callar_y_fingir.pdf)

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

**Leyes que según Bock son bastante descabellados y van en contravía de lo que pretendes el internet y la libertad que significa estar ahí**, *"cada vez hay más restricción por parte de funcionarios públicos que quieren controlar que se está diciendo, en el paro varios reporteros afirmaron que la policía les reclamaba por los testimonios y comentarios que hacían en sus redes sociales, justificando que esto generaban una mala imagen a la institución"*.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Retos del nuevo periodismo colombiano

<!-- /wp:heading -->

<!-- wp:paragraph -->

Bock señaló también que la idea de este informe es **evidenciar la realidad del periodista en Colombia** *"que se tiene que enfrentar algún tipo de censura y sobre todo a la violencia, qué ha generado hechos muy tristes como el asesinato a 159 periodistas en los últimos 35 años, lo cual representa una huella imborrable*".

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Y agregó que **uno de los principales retos son las audiencias** *"al lograr que entiendan y valoren la importancia de este oficio, así como las **distintas realidades dentro de este mundo tan grande que es el periodismo y que es precisamente el que se debe apoyar para poder entender mejor lo que ocurre en los territorios**"*.

<!-- /wp:paragraph -->
