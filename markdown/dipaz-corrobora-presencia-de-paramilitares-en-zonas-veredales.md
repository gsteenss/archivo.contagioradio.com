Title: Dipaz corrobora control paramilitar en 7 territorios cercanos a Zonas Veredales
Date: 2017-06-01 13:37
Category: Movilización, Paz
Tags: acuerdo de paz, DIPAZ, Zonas Veredales
Slug: dipaz-corrobora-presencia-de-paramilitares-en-zonas-veredales
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/06/IMG-20170601-WA0005-01.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [01 jun 2017] 

El informe "180 días hacia la vida civil" presentado por Diálogo Intereclesial por la Paz (DIPAZ) da cuenta del **control de grupos paramilitares en por lo menos 7 puntos aledaños a Zonas Veredales** o Puntos Transitorios, de las irregularidades en los contratos para adecuación de las Zonas Veredales, las irregularidades en la contratación para los alimentos, así como serios problemas para la atención en salud de los integrantes de las FARC.

Tras haber realizado veeduría humanitaria a los Protocolos del Acuerdo de paz entre el gobierno nacional y las FARC, DIPAZ afirma que hay **presencia y control de grupos armados ilegales en territorios cercanos a las Zonas Veredales,** situación que se agrava con el asesinato de 3 de los integrantes de esa guerrilla que están en tránsito a la vida civil. Le puede interesar:["Iglesias y organizaciones de fe de Dipaz acompañarán las Zonas Veredales Transitorias"](https://archivo.contagioradio.com/iglesias-y-organizaciones-de-fe-de-dipaz-acompanaran-las-zonas-veredales-transitorias/)

### **No hay respuesta en torno a garantías de seguridad** 

Esta situación es preocupante dado que a pesar de las denuncias y la gravedad de los hechos **"no se ha adoptado medidas en materia de Derechos Humanos que permitan su protección frente a potenciales amenazas"** afirmó Alberto Franco, uno de los voceros de DiPaz.

Adicionalmente el informe establece que "las tareas de monitoreo del Mecanismo de Monitoreo y Verificación, **tienen presencia esporádica en el terreno** por lo que las actividades sobre abastecimiento de agua, salud, suministros, articulación con la población civil y seguridad estuvieron limitadas". Le puede interesar: ["Farc cumplen cese al fuego pero avanza el paramilitarismo: Dipaz"](https://archivo.contagioradio.com/farc-cumplen-cese-al-fuego-pero-avanza-el-paramilitarismo-dipaz/)

En ese aspecto señalaron que, por ejemplo, en la Zona Veredal Transitoria "La Fila" en Tolima los mismos integrantes de las FARC, con medios propios han debido **atender 120 casos de enfermedades de los cuales 87 han requerido atención médica especializada** sin que hasta el momento haya respuestas efectivas por parte del MMV o las organizaciones estatales responsables.

### **Iiregularidades en contratos y sobre costos en insumos y alimentos** 

Según Dipaz "hay irregularidades de los contratistas responsables del suministro de alimentos y sobre costos en los productos". Esto lo constatan con el hecho de que la Distribuidora Remen CMT SAS, encargada de la construcción de las Zonas Veredales y Puntos de Normalización, **pasó de ser una comercializadora mayorista de alimentos a construir obras.**

Además se ha constatado el sobre costo excesivo en algunos de los alimentos, así como la entrega en mal estado o inapropiados para satisfacer las necesidades de salubridad o nutricionales requeridas en estos casos. Le puede interesar:["Crisis de salud, infraestructura y seguridad en Zona Veredal El Gallo"](https://archivo.contagioradio.com/crisis-de-salud-infraestructura-y-seguridad-en-zona-veredal-gallo/)

### **Acompañamiento internacional** 

La Federación Luterana Mundial se encuentra realizando el acompañamiento internacional a los procesos de veeduría. Según Saara Vourensola-Barnes, miembro de esta Federación, **"Dipaz hizo este informe con base en 10 zonas veredales y puntos de transición,** principalmente en Cauca y Chocó donde la organización tiene dos casas de protección para que los miembros de Dipaz puedan participar de manera activa en el diálogo con las comunidades".

Como acompañante internacional, La Federación Luterana Mundial ha podido mostrar lo que está sucediendo en el país en el exterior. Según Vourensola-Barnes, "hemos podido ver, acompañando a Dipaz, **la esperanza que tienen las comunidades por la reinserción a la vida civil** y las esperanzas de que el Estado llegue a sus territorios y haga mejoras sociales y económicas".

### **Recomendaciones de DIPAZ** 

Al cumplirse 180 días de la refrendación del acuerdo, Dipaz realizó unas recomendaciones específicas. Las principales según Vourensola-Barnes, son **" lo que tiene que ver con la seguridad de las ZVTN** y la presencia de paramilitares y Fuerza Pública. Recomendarle al Estado que cumpla lo pactado para garantizar la no violencia".

Otra está relacionada con las mejoras de la infraestructura física y social que requieren las comunidades y las ZVTN. Igualmente, el Padre Alberto Franco hizo un llamado al Estado y sus instituciones, para que **cumpla los acuerdos pactados y garantice la implementación de los acuerdos de manera satisfactoria.**

A la Comisión de Seguimiento, Impulso y Verificación a la Implementación (CSIVI), le hicieron un llamado para que desarrolle estrategias de pedagogía por la paz. A las FARC le recomendaron generar mecanismos que permitan mantener el **compromiso de los integrantes de esta guerrilla para avanzar en su camino a la reincorporación a la sociedad civil.**

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU).
