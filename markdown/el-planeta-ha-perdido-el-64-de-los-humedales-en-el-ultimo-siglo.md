Title: El planeta ha perdido el 64% de los humedales en el último siglo
Date: 2015-06-02 16:34
Category: Ambiente, Otra Mirada
Tags: Conferencia de las Partes de la Convención de Ramsar, Día Mundial del Ambiente, humadales, Humedales en Colombia, Ministerio de Ambiente y Desarrollo Sostenible, titulos mineros, UICN, Unión Internacional para la Conservación de la Naturaleza
Slug: el-planeta-ha-perdido-el-64-de-los-humedales-en-el-ultimo-siglo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/06/humedales.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: [hoyvenezuela.info]

###### 2 Junio 2015 

Esta semana se lleva a cabo la XII Reunión de la Conferencia de las Partes de la Convención de Ramsar, donde se llama la atención a los 168 países que hacen parte de este evento internacional, debido a que en los últimos **100 años **el planeta ha perdido un** 64 % de sus humedales**, y además la población de especies de estos ecosistemas **se ha reducido un 66%**.

De acuerdo al secretario general de la XII Reunión de la Conferencia de las Partes de la Convención de Ramsar, **Christopher Briggs**, es urgente que los países que hacen parte de esta conferencia actúen para enfrentar esa situación, y advirtió que los humedales son una fuente esencial de agua. Además, aproximadamente **660 millones de personas de todo el mundo viven de la pesca**.

Respecto al caso colombiano, el país cuenta **con** **31.702 humedales**, equivalentes a 20 millones de hectáreas, que abarcan terrenos de 1.094 municipios. Sin embargo, varios de estos ecosistemas se encuentran en grave peligro, debido a la explotación minera, pues actualmente existen **347 títulos mineros en 26 zonas de páramo** del país, lo que no contribuye a que se conserve los humedales, y en cambio cada día aumentan los riesgos ambientales.

Así mismo, cabe recordar que de acuerdo al Ministerio de Ambiente y Desarrollo Sostenible, en **Colombia existen** **275 especies de aves migratorias, de las cuales 10 están en peligro de extinción,** clasificadas por la Unión Internacional para la Conservación de la Naturaleza, UICN.

Con el eslogan **"Humedales para nuestro futuro" la** XXII edición de la conferencia llama la atención de los países para que generen políticas de protección de los humedales, teniendo en cuenta que la Convención hace parte de un** convenio intergubernamental respecto al tema** **ambiental, con el fin de preservar y conservar** los humedales del mundo.

Cabe recordar que el encuentro inició el lunes en Uruguay, y finalizará el  próximo 9 de junio. Es un evento que se desarrolla en el marco de la conmemoración del **Día Mundial del Ambiente.**

 
