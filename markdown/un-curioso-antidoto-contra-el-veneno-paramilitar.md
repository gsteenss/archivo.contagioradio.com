Title: Un curioso antídoto contra el veneno paramilitar
Date: 2016-04-13 06:00
Category: Opinion, Ricardo
Tags: Estados Unidos, Fuerzas Militares de Colombia, Paramilitarismo
Slug: un-curioso-antidoto-contra-el-veneno-paramilitar
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/04/BASE-MILITAR.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: SOAW 

#### [Ricardo Ferrer Espinosa](https://archivo.contagioradio.com/ricardo-ferrer-espinosa/) - [@ferr\_es](http://@ferr_es) 

###### Abr 13 2016 

Dejémonos de ilusiones, el “Ejército de Colombia” no responde al interés nacional y mucho menos con su vicio paramilitar.

Las Fuerzas Armadas de Colombia responden, sumisas, al interés nacional de los Estados Unidos. Triste y grave si recordamos que su blanco han sido ciudadanos colombianos, en aplicación de la doctrina de la Seguridad Nacional de la potencia dominante.

Incapaz de controlar el territorio (como lo demostró el paro armado del clan Úsuga), prosigue su relación evidente con fuerzas paramilitares, a las cuales solo se les ha cambiado el nombre.

Si asumimos los hechos, las FF.AA de Colombia siguen ajenas al marco constitucional colombiano, porque omitieron proteger la vida, honra y bienes de los ciudadanos.  Ante los militares carentes de honor, el campesino les teme y el habitante urbano los elude, sobre todo en regiones donde las acciones paramilitares siguen intactas.

El curioso antídoto contra la peste paramilitar seria que las FF.AA “de Colombia” acataran la voz del amo, es decir, el Comando Sur de los Estados Unidos[.]

[Si hacemos memoria, fue en 1996 cuando la elite colombiana tomó la decisión de escalar la guerra, por todos los medios y es en ese momento en que subordinan las FF.AA de Colombia a las órdenes del Comando Sur de los Estados Unidos. Se duplica la tropa, arrecian las masacres paramilitares, se paga al soldado por el número de bajas, llegan mercenarios y]Boinas Verdes, mientras se niega justicia, usurpan tierras y el ciudadano muere.

El gobierno de Andrés Pastrana inicia diálogos con las FARC, únicamente para ganar tiempo, no para superar el conflicto o lograr una salida negociada. La espantosa confesión de Pastrana está en su libro “Memorias olvidadas”, donde además relata  la subordinación militar de Colombia ante el poder extranjero.

Ahora los amos necesitan paz en Latinoamérica, en Cuba y en Colombia, simplemente porque van a la siguiente guerra en Asia, donde les piden que se retiren.[ Desde que se profundizó la alianza militar China – Rusia – Irán, el mensaje para Estados Unidos es que Asia es para los asiáticos. El problema es que sin las materias primas y los combustibles de Asia, los Estados Unidos simplemente se desangran, se mueren. Por esa guerra inminente en Asia, Estados Unidos necesita paz en su patio trasero.]

Si las FF AA de Colombia están subordinadas al comando Sur de los Estados Unidos, no deberían subordinarse ahora al propósito de la paz (americana)? Supuestamente los Estados Unidos ahora promueven la paz en su patio trasero. En ese marco, Barak Obama visita Cuba, bendice la negociación con las FARC y en Argentina anuncia que desclasificará los archivos de la guerra sucia, su dictadura militar.

Si este escenario es real, ¿No es hora de que las FF.AA de Colombia corten sus vínculos paramilitares? ¿Será mucho pedir? Dicho e otro modo, que el comando Sur de los Estados Unidos, verdadero jefe de las FF.AA las llame al orden y las motive a dejar sus viejas amistades.

La misma fuerza extranjera que promovió operaciones clandestinas en Colombia, trajo mercenarios para entrenar a los escuadrones de la muerte, llenó barcos con armas para las AUC, es la única voz que tiene acatamiento en el generalato macondiano.

Entonces, con el vecindario tranquilo, y Colombia en paz, Estados Unidos podrá lanzarse a la guerra en Asia, no por la democracia ni por la libertad, sino por el petróleo y las materias primas que ahora les quieren negar.

------------------------------------------------------------------------

###### Las opiniones expresadas en este artículo son de exclusiva responsabilidad del autor y no necesariamente representan la opinión de la Contagio Radio. 
