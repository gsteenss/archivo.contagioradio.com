Title: Convocan gran manifestación contra sordera de alcalde Peñalosa en Bogotá
Date: 2018-02-27 12:38
Category: Política
Tags: Bogotá, Enrique Peñalosa, Movilización, revocatoria, revocatoria Peñalosa
Slug: bogotanos-exigen-que-alcalde-enrique-penalosa-los-escuche-o-que-renuncie
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/11/DJuJQBhW4AA-mIh.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Comité Unidos Revocamos a Peñalosa] 

###### [27 Feb 2018] 

El día de hoy los bogotanos, que deseen manifestar su rechazo a la gestión del alcalde Enrique Peñalosa, tienen una cita en la **Plaza de Bolívar** para decirle al alcalde que escuche las problemáticas que se están viviendo en la ciudad y que no han sido atendidas de manera satisfactoria por el mandatario. La ciudadanía se reunirá a las 4:30 pm frente al a la Alcaldía.

Esta manifestación fue convocada por los ciudadanos quienes por redes sociales manifestaron que la revocatoria del alcalde **se debe hacer en las calles** teniendo en cuenta que el Consejo Nacional Electoral ha dilatado este proceso. En repetidas ocasiones, los bogotanos han manifestado que la ciudad no aguanta más de la administración actual por lo que Peñalosa debe escuchar a la ciudadanía.

### **Estas son las razones del plantón** 

Dentro de las razones por las cuales se realizará el plantón, está la necesidad de proteger la Reserva Van Der Hammen en dónde el alcalde ha manifestado su intención urbanizarla, la **crisis de las basuras que aún no se ha superado**, la negativa del alcalde de realizar un metro subterráneo para mejorar la movilidad y la insistencia del la admnistración para  construir las vías de Transmilenio por la carrera séptima.

De acuerdo con Carlos Carrillo, ciudadano que ha convocado a la manifestación, y promotor de la revocatoria, se espera que asistan “personas que se sientan afectadas por la crisis de las basuras, los vecinos de la carrera séptima, l**as 700 mil personas que firmaron por la revocatoria** y que les robaron su derecho a ir a las urnas, los desempleados de Aguas de Bogotá, los vecinos de Doña Juana y todas las personas que han sido afectadas por las malas decisiones de esta administración”.

### **“La revocatoria se la robaron”** 

Además, manifestó que esta movilización “es el único camino para que el Gobierno nos escuche”. Dijo que “la revocatoria se la robaron pues en menos de un mes estará enterrada” y “el Consejo Nacional Electoral **está dispuesto a violar la ley** con tal de salvar a Enrique Peñalosa”.

Afirmó que la única forma para realizar la revocatoria es que el CNE emita el certificado de inmediato “pero si logran posponerlo durante unas semanas más no habría caso hacer una revocatoria”. (Le puede interesar:["Situación de trabajadores de Aguas de Bogotá ya se había presentado en primera alcaldía de Peñalosa"](https://archivo.contagioradio.com/situacion-de-trabajadores-de-aseo-de-bogota-ya-se-habia-presentado-en-primera-alcaldia-de-penalosa/))

Esto teniendo en cuenta que, por el tiempo, **no habría elecciones atípicas** y si el alcalde perdiera la revocatoria, “el presidente pondría un alcalde de Cambio Radical”. Afirmó que en Colombia “la ley en muchos casos está supeditada a la política y el hecho de que el Fiscal General de la Nación sea un miembro fundador del partido de Germán Vargas Lleras, es un claro indicio de lo que es la justicia en el país”.

Además, indicó que las decisiones judiciales del caso de la revocatoria siempre favorecieron a sus promotores “sin embargo, **el CNE ha secuestrado la democracia**”. Para Carrillo “hay una política de Estado que se enfoca en defender a Peñalosa y la única manera de que haya un cambio es que la plaza se llene para que les quede claro que el 11 de marzo tendrán un descalabro sin precedentes por culpa de las pésimas decisiones del alcalde”. (Le puede interesar:["Con Peñalosa todo lo malo puede empeorar: Manuel Sarmiento"](https://archivo.contagioradio.com/con-penalosa-todo-lo-malo-puede-empeorar-manuel-sarmiento/))

Finalmente, Carrillo enfatizó que la movilización **“no tiene ningún tinte político”** y que busca demostrar que “ese contubernio político con Enrique Peñalosa les va a costar muy caro”. El plantón se realizará en la Plaza de Bolívar a partir de las 4:30 pm “para que levanten la voz y le digan al alcalde porqué quieren ser escuchados, en caso de que no nos escuche, le debemos exigir la renuncia”.

###### Reciba toda la información de Contagio Radio en [[su correo]
