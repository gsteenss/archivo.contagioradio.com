Title: Memoria, protección y derecho por la vida en el FESDA
Date: 2015-10-16 16:16
Author: AdminContagio
Category: 24 Cuadros, Sin Olvido
Tags: Asonadas policiales, Cali, colectivo mejoda, Daniela Anaconas FESDA, Distrito Aguablanca, Festival de cine y video comunitario de Aguablanca
Slug: memoria-proteccion-y-derecho-por-la-vida-en-el-fesda
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/10/DSCN9952.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### [Foto: Contagioradio] 

<iframe src="http://www.ivoox.com/player_ek_9026828_2_1.html?data=mpWfmJ2WfI6ZmKiakpyJd6KomZKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRhdfVz9%2FOjcrQb5jh0JCzx9jYrdfVzZDRx5Cnrc%2FZjN6YuM7IqdCfpNTa19PNuMLmytSYx9OPqY6ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Daniela Anaconas, Directora FESDA] 

###### [16, Oct 2015] 

Como parte de la programación del [**VII** **Festival de Cine y Video Comunitario del Distrito de Aguablanca (FESDA)**](https://archivo.contagioradio.com/con-pizarro-inicia-el-festival-de-video-y-cine-comunitario-de-aguablanca-en-cali/), ubicado al oriente de la ciudad de Cali, en la tarde noche de este viernes, se adelantará un evento especial de memoria, solidaridad y justicia con los jóvenes caídos en las "asonadas" ocurridas en sectores urbanos.

Se trata de la **Actividad Protección y el respeto a la vida**, acto que tendrá lugar en el barrio "Las Orquideas", zona donde ocurrió el asesinato de los jóvenes Jonathan Goapacha y Andres Rodriguez a manos de la policía nacional, que hasta la fecha se ha mantenido en la impunidad.

El equipo del Festival, invitados especiales, organizaciones sociales y medios alternativos, acompañarán durante el acto a las familias de las víctimas, quienes aún viven con el miedo y la zozobra por la ausencia de sus seres queridos y por la persecución e intimidación por algunas amenazas que han recibido.

De acuerdo con **Daniela Anaconas**, directora del Festival, el propósito de la peregrinación es "generar ese sentido de protección, de visibilización y sobre todo de lo referente a la justicia", utilizando la memoria como una herramienta para la resistencia social, en un sector en el que advierte "hay un fuerte abuso policial".

Con el acto de memoria, empieza a enfilarse el cierre del FESDA, que tendrá su clausura el sábado a partir de las 5 p.m en la sala de Cine Colombia del "**Centro Comercial Río Cauca**", donde se realizará la premiación de las categorías en competencia.

**Momentos.**

La actividad se divide en 6 momentos desde la partida en la unidad recreativa las orquídeas, donde partirá la caminata acompañada por la lectura de un texto de Jesús y palabras del personero, la defensoria del pueblo y de un líder de la comunidad.

Luego se realizaran paradas en la casa de los dos jóvenes asesinados, donde sus familias participarán con algunas palabras, de igual manera en las cruces conmemorativas levantadas en memoria de cada uno.

El cierre del evento contará con una parada en el parque del sector, donde se realizarán algunas presentaciones artísticas, el compartir de aguapanela, estampado de camisetas, y una cartelera de expresión abierta, finalizando con las palabras de uno de los líderes de la comunidad.

 
