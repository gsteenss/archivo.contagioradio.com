Title: "El éxito dependió de las enseñanzas aprendidas del pasado": Alfredo Molano
Date: 2016-08-25 12:08
Category: Entrevistas, Paz
Tags: Conversaciones de paz con las FARC, Negociación Gobierno Colombia y FARC, proceso de paz
Slug: el-exito-dependio-de-las-ensenanzas-aprendidas-del-pasado-alfredo-molano
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/08/Alfredo-Molano.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo Contagio Radio ] 

###### [25 Ago 2016] 

De acuerdo con el sociólogo Alfredo Molano, los aspectos que permitieron concluir con éxito el proceso de negociación iniciado hace cuatro años por la guerrilla de las FARC-EP y el Gobierno colombiano, tienen que ver con las **enseñanzas aprendidas de los procesos de paz que se intentaron en el pasado** y las características del contexto internacional que lo hicieron mucho más propicio para las conversaciones.

Molano asegura que las partes llegaron con los aprendizajes que dejaron los proceso de paz de los Gobiernos de Betancourt, Barco, Gaviria y Pastrana, que les permitieron no cometer los errores del pasado, como el no contar con las Fuerzas Militares y **manejar de forma más estratégica la información**, de un modo más prudente, "secreto pero no escondido".

Para el sociólogo **el contexto internacional actual fue mucho más propicio**, se contó con una actitud diferente por parte del gobierno de Estados Unidos, así como de los gobiernos latinoamericanos, razones por las que estas [[negociaciones concluyeron de forma exitosa](https://archivo.contagioradio.com/gobierno-y-farc-anuncian-cierre-de-conversaciones-de-paz/)]. "Hay mucha historia todavía que contar, hay muchos episodios que aclarar (...) seguiré dedicado a la historia que la verdad oficial no ha querido contar", agrega.

En países que han salido de conflictos armados los índices de violencia han aumentado y ese es justamente uno de los[ [temores a los que se enfrenta la sociedad](https://archivo.contagioradio.com/4-retos-a-superar-para-lograr-una-paz-estable-y-duradera/)], al respecto Molano asevera que es muy probable que no se viva la diáspora de ex guerrilleros que se generó en Centroamérica porque **las FARC no se van a desestructurar como organización política**, teniendo en cuenta que con sus bases han llevado un proceso pedagógico muy adelantando.

Además, en torno a los rumores sobre posibles disidencias al interior de algunos frentes de las FARC, Molano explica que seguramente esa guerrilla ya ha avanzad bastante en esos temas y que la **Conferencia Nacional** que se realizaría en los próximos días será **el Plebiscito de las FARC** y allí podrán resolverse varios asuntos de orden interno.

Vea también: [[Acuerdo Final de Paz entre Gobierno y FARC](https://archivo.contagioradio.com/acuerdo-final-de-paz-entre-gobierno-y-farc/)]

###### [Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU) ]] 

 
