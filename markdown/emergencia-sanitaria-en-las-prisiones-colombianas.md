Title: Emergencia sanitaria en las prisiones colombianas
Date: 2014-12-17 16:51
Author: ContagioRadio
Category: Hablemos alguito
Slug: emergencia-sanitaria-en-las-prisiones-colombianas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/04/descarga-7.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

[Audio](http://ctgaudio.megadatesystem.com/upload/0/fsYIZqQS.mp3)  
Continuando con la colaboración con el Comité de Solidaridad de Presos Políticos, este mes vamos a profundizar en la emergencia sanitaria que se está viviendo en el interior de las prisiones colombianas.  
Incorporamos dos testimonios de gran relevancia como pruebas de lo que esta sucediendo realmente, primero conversamos con Saul Belandia Leon, hermano del interno Jesús Miguel Belandia con cáncer de páncreas, o sea una enfermedad terminal. Él nos relata la desatención sanitaria que ha recibido su propio hermano y las dificultades que se le han impuesto para poder salir del centro penitenciario, ya en estado terminal.El segundo testimonio es el de  Diana Martinez, presidenta de la Asociación de familiares de personas privadas de la libertad, ella, nos describe como sufren la problemática no solo los internos sino también los familiares. Diana también denuncia quién es responsable de dicha situación, el estado colombiano y CAPRECOM, que es la gestora en esta temática. Contamos también en el programa con Ingrid Saavedra, abogada y defensora de DDHH, y con Jorge Bernal, médico especialista en la temática sanitaria dentro del contexto de las cárceles colombianas. Ellos realizan un diagnóstico sobre la situación, finalizando con la presentación de distintas propuestas con el objetivo de paliar la situación.
