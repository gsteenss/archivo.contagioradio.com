Title: Rendición de cuentas 2018
Date: 2019-05-06 16:13
Author: CtgAdm
Category: Rendicion de cuentas
Tags: rendición de cuentas
Slug: rendicion-de-cuentas-2018
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/4.-CERTIFICADO-REQUISITOS-RTE.pdf" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/6.-ESTADOS-FINANCIEROS.pdf" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/4.-CERTIFICADO-REQUISITOS-RTE-1.pdf" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/PRESENTADO-52451000772300.pdf" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/9.-INFORME-DE-GESTION-2018.pdf" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/informe-de-gestion.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

[informe de gestión]{}

-   [ CERTIFICADO REQUISITOS RTE](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/4.-CERTIFICADO-REQUISITOS-RTE.pdf)
-   [ESTADOS FINANCIEROS](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/6.-ESTADOS-FINANCIEROS.pdf)
-   [PRESENTACIÓN DIAN](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/PRESENTADO-52451000772300.pdf)
-   [ INFORME DE GESTION 2018](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/9.-INFORME-DE-GESTION-2018.pdf)

