Title: Quinta jornada de movilización en contra de la brutalidad policial y por el desmonte del Esmad
Date: 2015-02-23 17:16
Author: CtgAdm
Category: Movilización, Nacional
Tags: brutalidad policial, Carlos Alberto Pedraza, ESMAD, Fundación Nicolás Neira, MOVICE
Slug: quinta-jornada-de-movilizacion-en-contra-de-la-brutalidad-policial-y-por-el-desmonte-del-esmad
Status: published

##### Foto: contagioradio.com 

<iframe src="http://www.ivoox.com/player_ek_4122505_2_1.html?data=lZaflJqUeY6ZmKiakpuJd6KlmJKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRkdDqytHW3MbHrYa3lIqvldOPqc%2BfxNTb1tfFb8XZjNHOjcfWudXVzc7Rw8mPtNDgysjWw9GRaZi3jqjc0NnFq8rjjLfOxs7Tb46ZmKialg%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Ángel Molano, Fundación Nicolás Neira] 

Este 24 de febrero, varias organizaciones sociales y antimilitaristas convocan a la quinta jornada contra la brutalidad policial. Desde la 3 de la tarde en el Planetario Distrital, los colectivos se darán cita para que a partir **del arte, la indignación y la memoria, se expresen en contra de los actos violentos de la policía**.

Se trata de una marcha artística desde el Planetario Distrital, donde habrá presencia de batucadas  feministas, entrega de claveles negros, malabares, música y otras actividades culturales, a través de los cuales, los manifestantes se expresarán **“por una Colombia libre llena de esperanza y con memoria construida con los casos consolidados, de quienes han sido víctimas del Estado**”, afirma Ángel Molano, integrante de la Fundación Nicolás Neira y el MOVICE, (Movimiento de Víctimas de Crímenes de Estado).

La Fundación Nicolás Neira de la mano de la campaña permanente contra la brutalidad policial y el uso de la fuerza por parte del Estado, realizarán este movilización en memoria de **Nicolás Neira, Oscar Salas, Jhonny Silva, Giovanni Blanco víctimas de la policía en medio de movilizaciones y reivindicando la memoria de Carlos Alberto Pedraza, quien fue asesinado el pasado 19 de enero.**

Desde el 2010, se empezó a realizar las movilizaciones contra las fuerzas del ESMAD y otros agentes estatales, que de acuerdo a Molano, **“siguen delinquiendo bajo los escudos y gases de la muerte**", Molano, resalta que "la mayoría de los casos siguen en impunidad”.

Este año varias organizaciones sociales a nivel nacional, se han unido a esta movilización denunciando el crimen de Carlos Pedraza, líder y defensor de derechos humanos.
