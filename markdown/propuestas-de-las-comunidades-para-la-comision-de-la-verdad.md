Title: Comunidades se pronuncian frente a Comisión de la Verdad
Date: 2015-06-19 12:31
Category: Comunidad, Nacional
Tags: comision de la verdad, Conpaz, dialogos, ELN, FARC, habana, paz, Santos
Slug: propuestas-de-las-comunidades-para-la-comision-de-la-verdad
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/06/CONPAZ.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Buenaventura 

###### [19 jun 2015] 

Las Comunidades Construyendo Paz en los Territorios, CONPAZ, enviaron una carta abierta al presidente Juan Manuel Santos y a los comandantes guerrilleros de las FARC y el ELN, Timoleón Jimenes y Nicolás Rodríguez Bautista, respectivamente, con aportes para la definición de los objetivos y metodologías de la Comisión de la Verdad, anunciada al finalizar el ciclo 37 de diálogos de paz entre la insurgencia de las FARC y el gobierno nacional.

<div>

En la misiva, las comunidades valoran que se hayan tenido en cuenta sus propuestas en los acuerdos parciales para definir el que hacer de la Comisión de la Verdad, e instan a las partes para que se discuta con víctimas y organizaciones sociales los aspectos de la misma que aún se encuentran en disenso o sin concreción.

Entre las principales propuestas resaltan la necesidad de que la verdad y las garantías de no repetición sean un imperativo ético para el modelo de justicia transicional; la configuración de un sistema penal alternativo al punitivo carcelario; y que la Comisión de la Verdad sesione en 12 regiones del país, para construir efectivamente paz territorial.

A continuación reproducimos la comunicación completa de CONPAZ.

</div>

 

Varias localidades de Colombia, junio 15 de 2015

Presidente  
**Juan Manuel Santos**

Señor  
**Timoleón Jiménez  
**Comandante de las FARC EP

Señor  
**Nicolás Rodríguez Bautista  
**Comandante del ELN

**Ref.** Reacción a la Comisión de Esclarecimiento de la Verdad, Convivencia y No repetición (CV)

Reciban un cordial saludo.

Renueva nuestra esperanza en la construcción de una nueva democracia a la paz con justicia socio ambiental, los avances a los que han llegado parcialmente en la constitución de una Comisión de la Verdad en La Habana y deseamos que los disensos sean resueltos.

Valoramos  el que hayan tenido en cuenta aspectos de nuestra red de organizaciones y de otras víctimas y sobrevivientes.

Conociendo que el punto de la CV no está cerrado, que se requiere la discusión con el ELN y una legitimación con el conjunto de las víctimas, sobrevivientes y sus organizaciones es nuestra responsabilidad brindar unos primeros aportes sobre esta.

1.  Tal como ya lo hemos expresado en nuestra propuesta de Comisión de la Verdad (diciembre de 2013) y nuestra Asamblea Nacional (marzo de 2015) sostenemos que la verdad y reparación son un imperativo ético y las garantías de no repetición son fundamentales para poder hablar de una eventual justicia penal transicional.
2.  Garantías de no repetición base para la convivencia.

El derecho a la verdad no es un asunto de las víctimas, los sobrevivientes y los perpetradores sino de toda la sociedad.

Enunciar la verdad desde las víctimas y escuchar a los responsables permitirá encontrarnos como seres humanos e identificar factores que han llevado a nuestra destrucción, factores que son necesarios  desestructurar para que sea posible la convivencia sin exclusión social y sin violencia, sin daños ambientales y con empresas  que respeten los derechos humanos y los derechos de los pueblos.

La misión de la CV hacia la convivencia definirá políticas públicas que enfrenten factores estructurales para asegurar su no repetición con reformas constitucionales por un congresito o una Asamblea Constituyente, en cualquiera de las fórmulas pedimos nuestra participación como CONPAZ.

3.  Dignificación de las víctimas.

La caracterización histórica y sociológica en el marco de interpretación del derecho internacional de los derechos humanos y el derecho humanitario de los dramas y los relatos de las víctimas, posibilitara dimensionar claramente las responsabilidades estatales y las de los alzados en armas, y la de otros actores, sin simetría, previniendo la revictimización y la falseación de la realidad, la mentira, la tergiversación y el ocultamiento de los responsables y beneficiarios.

La dignificación será cierta y verdadera más allá de las palabras en unas políticas públicas de protección de los derechos a la libre expresión, a la libre asociación en movimientos y apuestas políticas, en una educación y salud integral; en políticas que posibiliten el uso y disfrute de los territorios y la protección de todas las fuentes de vida, ríos y mares, páramos y bosques, animales y bosques; democratización de la posesión y medio de información entre otros, un país justo dignificará a las víctimas; brillará la verdad si hay garantías de retorno, de reubicación y de regreso para los exiliados.

4.  La verdad de las víctimas y la verdad en el derecho penal público.

El marco interpretativo en contexto acompasara la verdad de las víctimas con la verdad que de deriva de su interpretación en el derecho penal público internacional,  permitiendo a nosotros  y a la sociedad en su conjunto, reconocer las razones y las sinrazones de nuestra violencia, identificando la existencia de crímenes de lesa humanidad como lo que ha ocurrido con nuestros desaparecidos forzados,  las brechas existentes frente al espíritu de lo que es un Estado Socio Ambiental de Derecho y las eventuales reformas necesarias en lo legal y otros campos para la construcción de una cultura de paz socio ambiental.

5.  La verdad y la justicia transicional.

Ni leyes, ni doctrinas, ni cárceles abrirán el ambiente y el paso  la justicia transicional. Si hay verdad y proyectos reparativos es aplicable la justicia penal especial hacia la paz. La enunciación sincera de la verdad por parte de  los responsables  directos y sus planificadores y beneficiarios podrá fundar un sistema penal alternativo  al punitivo carcelario para dar paso al restaurativo del responsable victimario y reparador para las víctimas.

La mentira cimentara nuevas fases de violencia pues la verdad deberá corregir prácticas y doctrinas criminales que sostienen la injusticia y la antidemocracia.

6.  La Verdad  base ética para un proyecto de democracia con un buen existir

El respeto de los responsables de la violencia y sus diversos actores a las víctimas con el reconocimiento de sus responsabilidades podrá contribuir a una cultura política para un proyecto de país que  enfrente causas y raíces descritos entre otros en los documentos de las Comisión Histórica, el Nunca Más, entre otros.

La verdad no puede ser solo verbalizada requiere gestos y expresiones concretas, si los responsables de las desapariciones forzadas expresan los lugares dónde se encuentran los restos de ellos e indiquen qué hicieron con ellos o quiénes ordenaron y se beneficiaron de este crimen, harán factible la discusión de la justicia alternativa

En este mismo sentido,  el reconocimiento de los daños causados en bosques, flora, fauna, aguas allanará elementos para el nuevo Pacto Socio Ambiental que debe fundamentar la paz.

7.  La verdad y la Paz Territorial.

Como hemos planteado la CV debería sesionar en 12 regiones incluyendo a los exiliados,  teniendo en cuenta las afecciones diferenciales  étnico territorial, etitario, género y medio ambiente, y los privados de la libertad de todas las partes en las cárceles.

La sesión de CV con audiencias debe realizarse en las 12 regiones en sedes físicas en 12 comunidades rurales, en las que hemos ofrecido la tierra y algunas infraestructuras, asociada al inicio de las Universidades de la Paz de la que ya tenemos currículos y presupuestos básicos. Y como parte de un plan escolar desde las guarderías, la primaria, la secundaria y las materias transversales en la educación superior en la que participarían comunidades, víctimas y sobrevivientes y responsables directos e indirectos.

En este mismo sentido, propusimos que  las frecuencias de FM que son radio pública de la policía y las Fuerzas Militares se fundan en una sola, y las frecuencias sobrantes y equipos  sean asignadas a nuestras comunidades y organizaciones, y con  financiación para el servicio de la CV pero luego  como mecanismo de reparación social para las víctimas. Asimismo debe ocurrir con la asignación de canales de televisión comunitaria y regional para la CV y que serán administradas por las organizaciones de víctimas.

Todo esto será realizable si la propiedad del suelo y el subsuelo es reconocida como bien de la humanidad, reconocido como bien habitado y protegido por comunidades negras, indígenas, mestizos, afromestizos, productoras de bien ambiental, de bien alimentario, de bien reproductor de las fuentes de vidas.

Reiteramos nuestra solicitud para que se nos autorice un diálogo con el Alto Comisionado para la Paz, militares activos y el general Rito Alejo del Río Rojas, la delegación de Paz de las FARC EP, y  la comisión de exploración del ELN para dar a conocer aspectos concretos sobre el deber de la memoria y el derecho a la verdad y su acompasamiento con la justicia reparativa.

Mientras es posible que exista un cese bilateral del fuego y de hostilidades para evitar daños irreparables a nosotros, a los combatientes de las partes, y al ambiente y se desmonte el paramilitarismo de antaño, a sus nuevas formas y la continuidad de la criminalización del movimiento de víctimas, social, ambiental y ecologista, esperamos que está tercera carta  sea asumida como un aporte en la construcción hacia la paz.

Agradeciendo su respuesta,

Comunidades Construyendo Paz en los Territorios
