Title: Mujeres Wayuu también son víctimas de las multinacionales
Date: 2015-11-25 19:30
Category: Mujer, Nacional
Tags: Cerrejón, dia de la no violencia contra la mujer, El Cerrejón en La Guajira, Mujeres Guajira, Mujeres Wayuu, Violencia contra las mujeres en Colombia
Slug: mujeres-wayuu-tambien-son-victimas-de-las-multinacionales
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/11/Violencia-contra-la-mujer-en-La-Guajira.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Ajayu La Revista 

<iframe src="http://www.ivoox.com/player_ek_9509682_2_1.html?data=mpqdm5ucdo6ZmKialJiJd6KllZKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRh9Dh1tPWxsbIb8jVz8aY1trYqc3VjMjc0NnWpYzgwpDSz9XWqdTVjMrZjajJttPZy4qwlYqmd8%2Bhhpywj6jTstXVyM7cjbfFqMrjjJKSmaiReA%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Dora Lucy Arias, CAJAR] 

###### [25 Nov 2015] {#nov-2015 .ecxMsoNormal}

[En este Día Internacional de la Eliminación de la Violencia contra la Mujer, la abogada Dora Lucy Arias del Colectivo de Abogados José Albear Restrepo, con amplia trayectoria en el trabajo con pueblos indígenas de La Guajira, asegura que hablar de **la no violencia hacia las mujeres en Colombia implica eliminar toda forma de violencia contra la naturaleza**, pues en su relación con ella es que las mujeres establecen formas de habitabilidad femeninas tanto en escenarios rurales como urbanos y que muchas veces son violentadas y desconocidas.  ]

[De acuerdo con la experiencia de las mujeres indígenas Wayuu existe una relación estrecha entre las prácticas de violencia contra la mujer y las formas en las que se atenta contra la madre tierra en esta zona del país y que se vinculan directamente con la **extracción carbonífera** por parte de la empresa Carbones del Cerrejón, cuyos efectos **afectan su integridad como mujeres, sus formas de organización colectiva y muy negativamente sus entornos cotidianos.**]

[La abogada concluye afirmando que la lucha por la no violencia hacia las mujeres debe ser integral y en ese sentido **no puede desligarse de las críticas y reclamaciones a los grandes poderes nacionales e internacionales**, que con su accionar atentan contra los territorios en los que transcurren las cotidianidades de cientos de mujeres colombianas, provocando el **incremento de enfermedades relacionadas con el cáncer de seno, anomalías en sus aparatos reproductores o alergias** que no son registradas y por tanto carecen de la atención médica y estatal requerida. ]
