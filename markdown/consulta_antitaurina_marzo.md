Title: Consulta antitaturina costaría un 90% menos si se realiza en elecciones de 2018
Date: 2017-05-22 20:15
Category: Animales, Nacional, Voces de la Tierra
Tags: Consulta Antitaurina, corridas de toros
Slug: consulta_antitaurina_marzo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/05/DSC_0478-1280x640.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Plataforma ALTO 

###### [22 May 2017] 

El domingo 13 de agosto es la fecha escogida por la administración de Enrique Peñalosa para llevar a cabo la **consulta antitaurina**. Sin embargo, este domingo, organizaciones **animalistas y estudiantes, empezaron a alertar sobre uno de los primeros inconvenientes de ese día: Las pruebas saber 11°**.

El registrador Nacional, Juan Carlos Galindo, ya habló de la logística necesaria para realizar la consulta, "Se debe revisar la logística y los costos, pues en la actualidad Bogotá tiene un potencial electoral de más de cinco millones personas, **y se tendrán que instalar cerca de 6.027 mesas en 634 puestos de votación".**

No obstante, además de las complicaciones que representa ese hecho frente a la organización de una consulta popular que usualmente se realizaría en los sitios donde se suele presentar el exámen saber 11°, existen otros problemas para la fecha escogida por el distrito.

### Altos costos 

El segundo problema que implica son los altos costos. En esa línea la Registraduría ha alertado sobre los más de 35 mil millones de pesos que le significarían al erario público. No obstante los mismos animalistas así lo reconocen y por ende, desde que nació la iniciativa se ha buscado la forma de que esta consulta se realice en medio de una elecciones ordinarias, **como las de marzo de 2018, de manera que los costos se reducirían casi en un 90%, como lo han explicado desde Plataforma ALTO, la principal promotora de la consulta.**

"La democracia no debería tener precio. Pensar en que la democracia no cueste alguna inversión es absurdo, pero **hay que buscar una eficiencia fiscal, por ello la necesidad de que la consulta se realice con las elecciones de de los aspirantes al Congreso** de la República", explica Natalia Parra, directora de la Plataforma ALTO.

### El umbral 

Luego de que en **las votaciones de revocatoria de mandato de los alcaldes de los municipios de Ocaña, Norte de Santander y El Copey, Cesar,** ninguna alcanzara el umbral, pese a que en ambas ganó el Sí a la revocatoria, el registrador nacional reveló que, desde los años 90, nunca un proceso de revocatoria ha alcanzado el umbral necesario para que sea válido. Lo que representa un argumento más para los animalistas, para que la consulta sea el próximo año, de manera que se garantice la participación ciudadana.

Y es que cabe recordar que de acuerdo con la Registraduría, el umbral de participación de esta consulta es de 1.882.520 personas y la decisión será obligatoria, cuando la pregunta hecha al pueblo haya sido contestada afirmativamente por la mitad más uno de los votos válidos, siempre y **cuando en la elección haya participado la tercera parte del censo electoral correspondiente.**

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
