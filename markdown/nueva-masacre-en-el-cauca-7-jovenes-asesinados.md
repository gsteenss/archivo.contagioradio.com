Title: Nueva masacre en el Cauca, 7 jóvenes asesinados
Date: 2020-09-20 21:22
Author: AdminContagio
Category: Actualidad
Tags: Cauca
Slug: nueva-masacre-en-el-cauca-7-jovenes-asesinados
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/04/Masacre-los-Uvos-e1460076855890.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

  
Este 20 de septiembre se generó una nueva masacre en el norte del cauca, la cifra que hasta ahora se conoce son de 7 jóvenes asesinados con armas de largo alcance los hechos ocurrieron hacia el mediodía cuando los jóvenes se encontraban departiendo en una gallera del corregimiento de Munchique municipio de Buenos Aires, Cauca.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Clemencia Carabalí, líder social de la zona confirmo el hecho y además expresó su preocupación por medio de sus redes sociales, por el “jovenicidio” que se viene presentando en varias zonas del país.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

“Con profunda indignación y tristeza nos toca registrar una nueva masacre, 7 jóvenes en la vereda Munchique, Buenos Aires Cauca, en nuestro territorio. Quién responde? quién protege a las comunidades?, a nuestros jóvenes? a la población civil? Quién responde!!!”.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por otro lado el Alcalde de Buenos Aires, Cauca, Óscar Edwin López expreso que: “Rechazamos de manera rotunda estos hechos de barbarie que hoy golpean a nuestra comunidad e invitamos al Gobierno Nacional, al Gobierno Departamental, a la Fuerza Pública, a los organismos de Derechos Humanos y entidades de Cooperación Internacional, a que articulemos acciones para construir escenarios de paz en nuestros territorios”

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

La organización Indepaz también resalto que el departamento del Cauca ha sido uno de los más azotados por la violencia. Este hecho sería la masacre número 60 en lo que va corrido de este año.La comunidad del Cauca y los defensores de DDHH hacen un fuerte llamado al gobierno colombiano para que se defienda la vida de las y los jóvenes en todo el país.

<!-- /wp:paragraph -->
