Title: "La literatura es un camino a la libertad" Patricio Pron
Date: 2016-01-30 09:07
Category: Cultura, Viaje Literario
Tags: hay festival, literatura
Slug: la-literatura-es-un-camino-a-la-libertad-patricio-pron
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/01/Patricio-Pron-by-Vasco.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Visperas 

##### [**Carolina Garzón - Contagio Radio **] 

###### 30 Ene 2016 

Patricio Pron, escritor argentino, seleccionado en 2010 como uno de los 22 mejores escritores jóvenes,  diálogo en el Hay Festival con Contagio Radio sobre el camino que abre la literatura a la libertad y sobre como el temor se puede transformar en fuerza a la hora de escribir.

"Liberarse de la imposiciones" reconocer otros mundos a través de la imaginación y plasmar  esas historias a través de libros que luego serán leídos por personas que darán múltiples interpretaciones, para el autor argentino la literatura es personalidad, no solo del que escribe sino de la persona que lee un libro.

#### [**Entrevista Patricio Pron**] 

<iframe src="http://www.ivoox.com/player_ek_10256715_2_1.html?data=kpWfl5ubdZahhpywj5WYaZS1k5uah5yncZOhhpywj5WRaZi3jpWah5yncbHV1dfWxc7Tb7Hm0NOYx9OPqc2fqcbmjavJt9Xd18bZjcjTsoy30NPhw8zNs4zGwsnW0ZCRaZi3jqjc0NnFq8rjjLfOxs7Tb46ZmKialg%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

### **4 Frases de Patricio Pron en entrevista con Contagio Radio** 

"Un escritor valiente, exige lectores valientes"

"Un escritor es alguien que está en un permanente estado de transformación"

"Cuando uno escribe acepta el hecho que la realidad no le basta y busca otros sitios donde ir"

"La literatura es esencialmente libertad"

 
