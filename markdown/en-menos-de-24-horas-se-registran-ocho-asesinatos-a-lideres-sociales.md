Title: En menos de 48 horas se registran 8 asesinatos a líderes sociales
Date: 2020-06-28 13:29
Author: CtgAdm
Category: Actualidad, DDHH
Slug: en-menos-de-24-horas-se-registran-ocho-asesinatos-a-lideres-sociales
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/06/Diseño-sin-título.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

En tan solo dos días del tercer puente continuo del mes de junio se registran diferentes [agresiones](https://www.justiciaypazcolombia.com/militares-agreden-a-mujeres-e-infringen-el-dih-en-desarrollo-de-operaciones-judiciales/) fatídicas contra campesinos, lideres sociales y dirigentes ancestrales en departamentos como Chocó, Norte de Santander , Cauca, Bolívar y Meta.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Asesinan a Gobernador indígena en Chocó

<!-- /wp:heading -->

<!-- wp:paragraph -->

La organización nacional indígena ONIC denunció el asesinato del gobernador indígena de agua clara en el bajo choco, esto luego de que el dirigente hubiese desaparecido el pasado 25 de junio.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/ONIC_Colombia/status/1277052396590563328","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/ONIC\_Colombia/status/1277052396590563328

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

  
A pesar de conocerse el nombre y la edad del líder indígena por por la fotografía revelada por la ONIC se podía identificar que se trataba de un hombre joven, el cual fue hallado sin vida en el corregimiento de Pizarro en el bajo baudó también en el departamento del chocó.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Señalando que el joven había sido encontrado con claros signos de tortura, la organización indígena solicita a las autoridades que se investigué este nuevo homicidio y se garantice la protección de Los defensores de Derechos Humanos en esta región extendiendo el llamado a las organizaciones internacionales para que se haga justicia en este caso han aumentado los índices de violencia en este municipio.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Líder de la USO fue asesinado en Macayepo Bolívar

<!-- /wp:heading -->

<!-- wp:paragraph -->

En la madrugada de este 28 de junio en el corregimiento de Macayepo, municipio de Toluviejo , en Sucre **fue encontrado sin vida Ovidio Baena de 68 años; pensionado de Ecopetrol, y ex dirigente de la Unión Sindical Obrera** de la industria del petróleo (USO).

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/KevinSizaI/status/1277051768690675712","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/KevinSizaI/status/1277051768690675712

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

Según sus familiares Baena habría sido atacado por hombres desconocidos, quienes ingresaron en su finca en el corregimiento de Macayepo, a altas horas de la noche del 27 de junio, **atacándolo con palos y causándole graves heridas** que luego produjeron su muerte.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

**El líder social fue encontrarlo con vida por sus familiares y traslada al centro de atención del municipio de Toluviejo**; en dónde falleció debido a la gravedad de las heridas.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Adicionalmente los familiares, denunciaron ser revictimizados, esto debido a que **el cuerpo del líder, no fue recibido en el Instituto de Medicina Legal, minutos después de su defunción**, esto según el Instituto porqué no había sido previamente inspeccionado y levantado por funcionarios.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Con la respuesta de Medicina Legal, los familiares decidieron llevar el cuerpo del líder a la funeraria del municipio; en donde **5 horas después la entidad ingresa para realizar el procedimiento y trasladarlo a sus instalaciones.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Actualmente la investigación se encuentra en manos de las autoridades quienes no descartan que los motivos estén relacionados con las actividades sindicales que cumplía Ovidio Baena.

<!-- /wp:paragraph -->

<!-- wp:heading -->

¡Paren el genocidio a líderes sociales!
---------------------------------------

<!-- /wp:heading -->

<!-- wp:paragraph -->

La tragedia por el asesinato de líderes sociales en medio de la pandemia sigue en aumento en las diferentes regiones. A estos dos casos se suma el de **Yohanni Yeffer Vanegas, líder de la movilización campesina de Guayabero, asesinado en medio de procedimientos de erradicación forzada** de cultivos de uso ilícito. (También puede leer: [Yohanni Yeffer Vanegas líder de movilización campesina de Guayabero fue asesinado](https://archivo.contagioradio.com/yohann-yeffer-vanegas-asesinado-guayabero/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Adicionalmente hemos conocido de un nuevo asesinato en la región del Catatumbo y dos más en Morales departamento del Cauca, se trataría de una pareja de guardias indígenas que realizaban prácticas de cuidado de sus comunidad para prevenir los contagios por COVID en medio del estado de emergencia que no ha sido atendido eficazmente por el gobierno de Iván Duque.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Vale la pena resaltar casos como el del viernes 19 de junio, en donde fue asesinado Efraín Rodríguez Arias en el municio del Carmen; a este le siguió el 23 de junio Luis Alberto Álvarez Pacheco, asesinado en el corregimiento de Lázaro; posteriormente este 26 de junio fueron asesinados también Carlos Farith Ortiz Acosta y Cristian David Anaya Herazo en la vereda Caño Negro.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Y finalmente el homicidio de Ovidio Baena en la vereda el Pavo, cierra un fin de semana para los liderazgos en los diferentes territorios rurales en Colombia, poniendo en evidencia que pensé a la pandemia , las medidas de aislamiento , y el aumento de la militarización la violencia por parte de grupos armados legales e ilegales en el país no se ha detenido.

<!-- /wp:paragraph -->
