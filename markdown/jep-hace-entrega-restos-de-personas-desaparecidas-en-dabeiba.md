Title: JEP hace entrega de restos de personas desaparecidas en Dabeiba
Date: 2020-11-10 09:28
Author: AdminContagio
Category: Actualidad, DDHH
Tags: Cementerio de Dabeiba, JEP, Jornada de exhumaciones en Dabeiba, personas desaparecidas
Slug: jep-hace-entrega-restos-de-personas-desaparecidas-en-dabeiba
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/11/Jep-en-jornads-de-exhumaciones-en-Dabeiba.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/11/Jep-hace-entrega-de-restos-de-cuerpos-de-personas-desaparecidas..jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/11/Jep-entrega-de-restos-de-personas-desaparecidas.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/11/jep-entrega-de-restos.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/11/jep-entrea-de-restos-2.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/11/jep-entrea-de-restos-2-1.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/11/jep-entrea-de-restos-2-2.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/11/jep-entrea-de-restos-2-3.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/11/jep-entrega-de-restos-1.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: Cortesía JEP

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En el marco de la tercera diligencia de exhumaciones en Dabeiba realizada por la Jurisdicción Especial de Paz (JEP) del 8 al 14 de noviembre, **la JEP junto a la Fiscalía y Medicina Legal, hacen entrega a familiares, de cuatro cuerpos de personas desaparecidas  identificadas en el cementerio Las Mercedes, del municipio antioqueño.**

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/JEP_Colombia/status/1326159159151243268","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/JEP\_Colombia/status/1326159159151243268

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

Hasta la fecha, en el cementerio se han recuperado los cuerpos de 54 personas dadas como desaparecidas en el marco del conflicto armado colombiano. (Le puede interesar: [Presupuesto del 2021 se puede convertir en un nuevo ataque a la JEP](https://archivo.contagioradio.com/presupuesto-del-2021-se-puede-convertir-en-un-nuevo-ataque-a-la-jep/))

<!-- /wp:paragraph -->

<!-- wp:image {"id":92555,"sizeSlug":"large"} -->

<figure class="wp-block-image size-large">
![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/11/Jep-entrega-de-restos-de-personas-desaparecidas-1024x382.jpg){.wp-image-92555}  

<figcaption>
Foto: Cortesía JEP

</figcaption>
</figure>
<!-- /wp:image -->

<!-- wp:paragraph -->

Y este martes, tras décadas de incansable búsqueda, se hizo la entrega digna de los restos de Yulieth Tuberquia, Nelson Gómez, Alveiro Úsuga Uribe y Eliécer de Jesús Úsuga, identificados tras las exhumaciones que se hicieron en el cementerio del municipio en diciembre del 2019. En horas de la mañana, los familiares de las víctimas se trasladaron al cementerio Las Mercedes para darles sepultura.

<!-- /wp:paragraph -->

<!-- wp:jetpack/tiled-gallery {"columnWidths":[[50.02917047346328,49.97082952653671]],"ids":[92560,92561]} -->

<div class="wp-block-jetpack-tiled-gallery aligncenter is-style-rectangular">

<div class="tiled-gallery__gallery">

<div class="tiled-gallery__row">

<div class="tiled-gallery__col" style="flex-basis:50.02917047346328%">

<figure class="tiled-gallery__item">
![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/11/jep-entrea-de-restos-2-3.jpg?ssl=1)
</figure>

</div>

<div class="tiled-gallery__col" style="flex-basis:49.97082952653671%">

<figure class="tiled-gallery__item">
![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/11/jep-entrega-de-restos-1.jpg?ssl=1)
</figure>

</div>

</div>

</div>

</div>

<!-- /wp:jetpack/tiled-gallery -->

<!-- wp:paragraph -->

Durante la ceremonia, realizada **en el Parque Central de Dabeiba**, el magistrado Alejandro Ramelli expreso que **«Yulieth y Eliécer tenían 16 y 14 años cuando les despojaron la vida y sus sueños. Encontrarlos nos recuerda el horror indiscriminado de la guerra. Hallarlos e identificarlos es un renacer de su presencia para orientar a las generaciones venideras».**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Cielo Úsuga Uribe, familiar de Alveiro Úsuga Uribe manifestó que **«**esta no ha sido una situación fácil. Pero gracias a Dios y el aporte de cada una de las entidades pudimos recuperar a los familiares. **Para muchos puede que no sea importante pero es lo que soñábamos desde hace muchos años» .**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por su parte, el alcalde de Dabeiba, Leyton Urrego, señaló que «las exhumaciones van a seguir y están invitando a la comunidad para las tomas de pruebas ADN».

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Estas[jornadas](https://twitter.com/JEP_Colombia/status/1321442628815200257) se están llevando a cabo después de las versiones coincidentes rendidas por 17 miembros de la fuerza pública y en el marco de las medidas cautelares que buscan proteger a las víctimas de desaparición forzada. (Le puede interesar: [Uniformados en la JEP coinciden en que habrían más víctimas de desaparición en Dabeiba](https://archivo.contagioradio.com/uniformados-en-la-jep-coinciden-en-que-habrian-mas-victimas-de-desaparicion-en-dabeiba/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Hasta el momento, el trabajo conjunto de la JEP y de la Fiscalía ha permitido avanzar **«en la identificación de los cuerpos que serán entregados y que conforman un universo de más de 600 personas desaparecidas en este municipio de Antioquia».**

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
