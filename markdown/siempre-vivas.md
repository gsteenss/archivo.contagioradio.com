Title: Siempre vivas
Date: 2015-11-03 09:41
Category: Camilo, Opinion
Tags: 30 años palacio de justicia-la memoria y el arte, Danilo Rueda, siempre viva - Camilo de las casas, Sin Olvido
Slug: siempre-vivas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/11/Siempreviva-CONTAGIO-RADIO.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

#### **[Por [Camilo De Las Casas](https://archivo.contagioradio.com/camilo-de-las-casas/) - [~~@~~CamilodCasas](https://twitter.com/CamilodCasas)** 

###### [2 Nov  2015] 

Cuando el dolor se vuelve concepto, se hace estéril, escribía Adorno. La memoria, siempre la y los hace viva y vivos, y esa memoria se dice de muchas maneras. Las he visto en el Sin Olvido, desde hace 27 años; en bicicletas y en rituales públicos en la Plaza de Bolívar cada año. Las he visto de la mano de Danilo Rueda en las bicicletas, y en las sillas, en los velones, en Miguel Torres en el Teatro, y hace unas pocas horas en el cine a través de la dirección de Klich López. La estética las hace presente, nos las hace visibles y concretas.

Ellos han aparecido en los sueños de sus sobrevivientes, en los retratos, en los vestidos, en los olores, en la música que escuchaban, en las palabras que poco a poco devora el tiempo. Siempre han estado. Ellas han pasado de generación en generación. Las quisieron hacer anónimas o nunca existentes, pero a los militares la tarea no les quedó bien hecha. Su acto absolutamente repudiable de desaparecer inicialmente a once personas, hoy sabemos que son más, unas pocas horas después de que el M 19 se tomara el Palacio de Justicia, quedó con rastros. La persistencia en la memoria de sus sobrevivientes ha sido una terca esperanza y en ella la estética, en este caso, la del cine, nos ha hecho volver a ese vacío del alma o a ese encuentro con aquellos que para nosotros siguen estando vivos, justo porque les traemos, les evocamos, les sentimos.

Esa casa en el centro de Bogotá es la casa común llamada Colombia. El único lugar de la pieza teatral llevada al cine, son unos pocos metros, es una casa de la Candelaria, todo ocurre allí como en La Cita, el país Nacional. En ese espacio afloran en los actores Laura García, Enrique Carriazo, Andrés Parra, Laura Ramos, Andrea Gómez, Alejandro,  eso que somos los colombianos, lo propio de nuestra condición humana pero en una nación. Es la nación encerrada, es la nación reprimida, la nación acartonada.

Esa casa padece la toma y la contra toma, en medio de la identidad catolicista que nos enseñó la falsa lealtad, a la autoridad y a la fuerza, al machismo retrechero y a la mentira. En esa casa en que transcurre la Siempre Viva esa colombianidad tiene intentos de distanciarse de ese autoritarismo reverencial con ideas libertarias, con brotes de solidaridad, con la verdad apuño, sin embargo, con excepción de la memoria, poco a poco se somete al poder cotidiano, que ejerce el agiotista, el acaparador, el acumulador de la riqueza, el mentiroso, el vivo.

Esa Colombia que se expresa en la Siempre Viva, se reconstruye en la memoria, en lo que aparece como una persistente locura, la de una mujer, que no cesa en buscar a su hija; que asume que el dinero de una indemnización no es nada, que la vida es el todo; que la verdad oficial es una farsa, que nos hace ser parte de un circo de la mentira. Quizás por eso la Siempre Viva nos duele, porque es vernos en el espejo de hace 30 años, el mismo rostro que continúa existiendo, con víctimas negadas

La Siempre Viva es ruptura. Cristina del Pilar, Luz Mary o Luz Amparo. Sus restos han sido días atrás hallados, quedan muchos más por hallar, y otros más por contar y por identificar.

A través de Siempre Viva, los desaparecidos forzados irrumpen la cotidianidad, a través de cada cuadro desafían el olvido, el destino de la Niebla, el patriarcalismo. Y esa acumulación de la riqueza que nada ha tenido de justa, con la que se pretende ocultar la desgracia propia, entre ella, la verdad oficiosa y oficial.

Los desaparecidos existen porque hay unos alguienes que los evocan, que los llaman, que les buscan, que les hablan, que les aman, terca y hasta obsesivamente, y que les logran expresar.

La memoria es tenue, es débil, padece de su correlato o contra relato el olvido, y por eso el arte, de alguna manera, es la pretensión de la perpetuación de que lo somos, no sea tan simplemente efímero, para que la epopéyica de sus familiares pase a la historia colectiva cuando alguna vez en Colombia sea posible.

Los desaparecidos existen porque nunca son parte del olvido; porque alguno de los obedientes borregos militares, una pista compartió. Los desaparecidos forzados están vivos, nunca fueron totalmente silenciados, porque el aparato represor que los pretendió negar como alguna vez existentes, se desmoronó en esa obstinada memoria y el arte es una forma de la memoria, aquella que permite en la belleza desmoronar la impunidad social. Gracias Danilo, Miguel, Klich y los que a su lado construyeron estéticas sobre esa verdad negada hace 30 años.
