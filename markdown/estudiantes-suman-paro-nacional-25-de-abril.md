Title: Estudiantes se suman a paro nacional del 25 de abril
Date: 2019-04-23 14:33
Author: CtgAdm
Category: Educación, Paro Nacional
Tags: 25 de abril, estudiantes, Paro Nacional, UNEES
Slug: estudiantes-suman-paro-nacional-25-de-abril
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/Marcha-de-estudiantes-2.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

El movimiento estudiantil anunció que se unirá al gran paro nacional del próximo jueves 25 de abril como una forma de **estrechar los lazos de unidad con el grueso del movimiento social**, y para **exigir el cumplimiento de los acuerdos alcanzados** con el Gobierno en diciembre del año pasado. En ese sentido, la Unión Nacional de Estudiantes de Educación Superior (UNEES) hizo un llamado para que todas las Instituciones de Educación Superior (IES) públicas se unan al paro, y cesen sus actividades.

En un comunicado la UNEES indicó los dos puntos claves para articularse en el llamado al paro nacional: **El Plan Nacional de Desarrollo (PND)**, que ha sido ampliamente criticado por sectores sociales; y el avance de **un modelo de educación superior contrario a los intereses de la mayoría de colombianos**. Como lo explica el vocero nacional de la Plataforma estudiantil, James Pareja, esta organización participó en el encuentro de organizaciones sociales que se realizó en febrero, en el que se acordó la fecha para el paro.

En el encuentro se decidió que las organizaciones sociales se articularían **contra el PND, y en favor de la implementacion del acuerdo de paz,** así como del cumplimiento de los acuerdos alcanzados entre Gobierno Nacional y el movimiento social. Sobre estos puntos, los estudiantes reclaman que el plan de desarrollo no contempla la **reforma integral del ICETEX** (pactada también con el Gobierno), ni la discusión sobre los **artículos 86 y 87 de la Ley 30** sobre educación superior.

### **¿Cómo lograr la articulación de sectores diversos en una agenda de estos tres puntos?**

Para implementar una hoja de ruta que permita hacer una construcción programática sobre temas de agenda a nivel territorial, se formarán **Comités Revolucionarios Populares (CORPUS)** que deberán integrarse; y con los que Pareja espera que se construya desde la diversidad escenarios de lucha conjunta. (Le puede interesar:["25 de abril, paro nacional por la paz y contra el Plan Nacional de Desarrollo"](https://archivo.contagioradio.com/25-abril-paro-nacional-paz-pnd/))

El anuncio de los estudiantes suma fuerzas a **campesinos, trabajadores, pueblos indígenas, profesores de educación básica, comunidades afro y transportadores** quienes han afirmado su decisión de formar parte del paro. (Le puede interesar:["Tumaco: sin gobierno, sin agua y con mucha violencia"](https://archivo.contagioradio.com/tumaco-gobierno-agua-violencia/))

<iframe id="audio_34841306" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_34841306_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [English Version](https://archivo.contagioradio.com/students-join-the-national-strike-on-april-25th/)

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
