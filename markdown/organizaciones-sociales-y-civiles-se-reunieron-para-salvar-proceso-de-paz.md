Title: Sociedad Civil reunida para salvar proceso de paz
Date: 2015-11-11 16:03
Category: Otra Mirada, Paz
Tags: Comisión Intereclesial de Justicia y Paz, ComPaz, diálogos en la Habana, DIPAZ, emisora radio derechos humanos, FARC, Frente Amplio por la PAz, Iván Cepeda, MOVICE, Paramilitarismo, proceso de paz, Radio derechos Humanos
Slug: organizaciones-sociales-y-civiles-se-reunieron-para-salvar-proceso-de-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/11/CTiHuKyWUAAPiYU.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: contagioradio.com 

<iframe src="http://www.ivoox.com/player_ek_9355656_2_1.html?data=mpiil5uZeo6ZmKiakpWJd6KkkZKSmaiRdI6ZmKiakpKJe6ShkZKSmaiRk9PbwtPW3MbHrdDixtiY1dTHrcLgxtiY25DHrdfdzcrgjdjJb9PZ1tPWx9fTsozkwtfOjdjFsI6ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Abilio Peña, Comisión Intereclesial de Justicia y Paz] 

##### [11 nov 2015] 

Miembros de distintas organizaciones sociales y civiles se reunieron la mañana de este miércoles, para **discutir y proponer entorno al difícil momento que atraviesa el proceso de paz,** por las numerosas operaciones militares y el incremento alarmante del paramilitarismo en el país.

Los informes presentados por parte del senador Iván Cepeda, organizaciones como CONPAZ, DIPAZ, representantes de Constituyentes por la paz y del Frente Amplio Común por la Paz; demuestran que **en los últimos quince días se han realizado más de 50 operaciones militares contra la guerrilla de las FARC y en una de ellas murieron 4** **miembros** del grupo, cercanos a uno de los comandantes que está negociando.

Durante el encuentro se puso de manifiesto la preocupación porque deacuerdo con la información suministrada por parte de la delegación de las FARC en la Habana “**no han habido avances en el desarrollo de distintas comisiones**”, como asegura Abilio Peña, integrante de la Comisión de Justicia y Paz, situación que no ha cambiado desde que se dio a conocer el comunicado con los 10 puntos en relación al acuerdo de jurisdicción especial para la paz.

La no develación del acuerdo de  manera integral,  afecta necesariamente al proceso, por lo que  **se pedirá al gobierno y  a las FARC **que “se publique cuales fueron los puntos que se acordaron de manera cierta concisa precisa y definitiva de ese acuerdo que se lanzó públicamente en el mes de septiembre”, precisa Peña.

En la reunión se **destacó la preocupante situación de paramilitarismo** “que se ha extendido a lo largo del país”, por lo que se presentará un informe consolidado al Ministro de defensa; se propuso “**avanzar en la construcción de campamentos humanitarios de paz en las zonas donde más se ve amenazado el cese unilateral al fuego**”, además de impulsar una resolución que mueva el Parlamento Europeo en relación con la paz de Colombia.

**Sociedad Civil sostén del proceso de paz:**

En la comisión se reunieron aproximadamente 50 delegados de organizaciones, iglesias, movimientos por la paz, **víctimas, campesinos,** juntando un espectro “bastante grande de la sociedad” que es un logro de “construcción de unidad para salvar el proceso de paz”, señala Abilio Peña.

Se **resaltó la veeduría social que varias organizaciones vienen desarrollando al proceso** y la importancia de las asambleas nacionales por la paz; otro de los puntos fueron los **avances en tema de reconocimiento y responsabilidad por parte de las FARC, **como por ejemplo frente a la masacre de Bojayá, del cual se invitó a un futuro acto de reconocimiento en este municipio que sería realizado por las el movimiento guerrillero.

La ciudadanía, encabezada por las víctimas aportó sus propuestas en relación con la verdad, justicia y reparación integral; el apoyo de la comunidad internacional; y acciones puntuales como **el 7 de diciembre encender una vela por la paz en toda Colombia**; el 10 de diciembre se desarrollarán acciones por el día de los derechos humanos y en el Cauca del 10 al 14 de diciembre se desarrollarán las primeras visitas pastorales.

   
 
