Title: 35 mil firmas para impedir el desvío de arroyo El Bruno
Date: 2016-12-02 16:31
Category: Ambiente, Entrevistas
Tags: Arroyo El Bruno, colectivo de Abogados José Alvear Restrepo, Crisis humanitaria en la Guajira, El Cerrejón en La Guajira, Gran Minería en Colombia
Slug: 35-mil-firmas-impedir-desvio-arroyo-bruno
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/12/Arroyo-Bruno.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Waronwant] 

###### [2 Dic 2016] 

El pasado 1 de Diciembre fue radicado ante el Congreso un documento que recoge 35 mil firmas para exigir la **suspensión de actividades de la mina El Cerrejón y no lleve a cabo el proyecto que pretende desviar 9.2 kilómetros el arroyo El Bruno.**

De ser favorable el fallo para esta iniciativa de las comunidades de la Guajira, acompañada por el Colectivo de Abogados José Alvear Restrepo, El Cerrejón estaría obligada a realizar consultas previas en el territorio y estudios técnicos que sean verídicos sobre los residuos de **metales pesados, contaminación del aire y fallas geológicas que representan riesgo de sismos.**

Samuel Arregocés líder comunitario de la Guajira, comenta que el proceso de recolección de firmas y sensibilización que se extendió por cinco meses “valió la pena, porque no podemos permitir que la destrucción que ha hecho el Cerrejón **por más de 40 años continúe (…) ya desaparecieron a las comunidades de Tabaco, Manantial y Sarahita, no pueden desaparecer más pueblos”.**

Por otra parte, Jose Jansen abogado del CAJAR, manifestó que uno de los grandes obstáculos para el proceso ha sido “la estrategia del Cerrejón de **no pedir licencias por grandes zonas sino fraccionarlas,** y así mismo fraccionan a las comunidades y fraccionan nuestra capacidad de respuesta -haciendo referencia al Colectivo de Abogados-”. Le puede interesar: [El Cerrejón desalojaría 80 indígenas para desviar arroyo bruno.](https://archivo.contagioradio.com/cerrejon-desalojaria-80-indigenas-desviar-arroyo-bruno/)

Sin embargo, muchas personas y organizaciones como CENSAT han respaldado la iniciativa y “lo que se busca es cuestionar a la Corte sobre cual es el nivel de protección que le va a dar a los cuerpos de agua y a los ríos, porque eso es lo que esta en juego acá, saber si una empresa **para sacar carbón puede desviar ríos y arroyos en una zona declarada en crisis humanitaria por falta de agua y alimentos”** puntualizó el abogado.

Por último, el líder Samuel Arregocés hace un llamado de atención a la sociedad civil y a la Corte para que tome una decisión responsable y pronta, ya que “si se permite que el proyecto continúe para ese entonces **ya no van a tener que desviar el río porque ya va a estar seco y solo procederían a sacar los 500 millones de toneladas de carbón** que están bajo tierra”.

<iframe id="audio_14456494" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_14456494_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>  
<iframe id="audio_14456695" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_14456695_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
