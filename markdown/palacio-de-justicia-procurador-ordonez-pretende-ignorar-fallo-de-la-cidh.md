Title: Procurador Ordoñez pretende ignorar fallo de la CIDH en caso Palacio de Justicia
Date: 2015-11-10 15:57
Category: DDHH, Nacional
Tags: 30 años palacio de justica, Alejandro Ordoñez, Convención Americana de Derechos Humanos, Corte Interamericana de Derechos Humanos, Cristina Guarín, Lucy Oviedo y Luz Mary Pórtela
Slug: palacio-de-justicia-procurador-ordonez-pretende-ignorar-fallo-de-la-cidh
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/08/procurador_contagioradio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### foto:elpais 

###### [9 nov 2015] 

El Procurador General de la Nación Alejandro Ordoñez **pide inferir las indemnizaciones a los familiares  de Cristina Guarín, Lucy Oviedo y Luz Mary Portela victimas del Palacio de Justicia**, pese a que el fallo de la Corte Interamericana de Derechos Humanos, ordena que se realicen las indemnizaciones tras haber sido hallados algunos restos óseos de las víctimas el pasado 20 de octubre.

Ante esta acción del procurador que pretendería desconocer el fallo de la CIDH, la canciller María Ángela Holguín, envió un mensaje  al procurador Ordoñez , señalándole que "**los acuerdos estipulados en los artículos 67 y 68 de la Convención Americana de Derechos Humanos los fallos del Tribunal Interamericano son definitivos e inapelables y, en tal sentido, los estados partes se comprometieron a cumplir sus decisiones”.**

Sin embargo, el procurado sostiene que se debe aclarar primero cómo murieron Cristina Guarín, Luz Mary Portela y Lucy Amparo Oviedo. Pese a que los familiares de las víctimas y algunas pruebas demuestran que las personas salieron vivas del Palacio de Justicia y luego fueron desaparecidas, de acuerdo con Ordoñez, “**si finalmente se establece, como lo refiere el informe del señor fiscal general de la Nación antes enunciado, que no hubo desapariciones, no puede pagarse la condena por ese delito, ya que, todo parece indicar, la conducta no existió”.**

La Corte Interamericana ordenó indemnizaciones por daño material, US\$45.000 (es decir, \$130 millones) y US\$35.000 (hoy unos \$100 millones) para las familias Guarín y Portela; ninguna para la familia Oviedo. Y por daño inmaterial, US\$100.000 (unos \$290 millones hoy) para las tres familias.

La respuesta de Holguín al procurador Ordóñez fue enviada el pasado 5 de noviembre un día antes de que el presidente Juan Manuel Santos realizara el acto de perdón frente a la opinión pública, pero sobre todo frente a los familiares de las víctimas de desapariciones y torturas y ejecuciones extrajudiciales del Palacio de Justicia.
