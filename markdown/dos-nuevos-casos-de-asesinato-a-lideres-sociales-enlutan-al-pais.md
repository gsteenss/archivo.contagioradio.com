Title: Dos nuevos casos de asesinato a líderes sociales enlutan al país
Date: 2020-01-14 17:06
Author: CtgAdm
Category: Líderes sociales, Nacional
Tags: Cauca, colombia, cordoba, indigena, lideres sociales
Slug: dos-nuevos-casos-de-asesinato-a-lideres-sociales-enlutan-al-pais
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/muerte-de-lideres-sociales.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:heading {"align":"right","level":6,"textColor":"cyan-bluish-gray"} -->

###### Foto: Elnuevosiglo {#foto-elnuevosiglo .has-cyan-bluish-gray-color .has-text-color .has-text-align-right}

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

El 13 de enero autoridades regionales denunciaron nuevos casos de violencia que acabaron con la vida de dos líderes sociales, el primero de ellos se registró en Monteliebano, Córdoba, y el segundo en Toribio, Cauca.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

**Jorge Luis Betancurt** vocero social y líder deportivo, fue identificado por la Fundación Social Cordobexia, como el primer caso en el año de asesinato a líderes sociales en esta región. (Le puede interesar: <https://archivo.contagioradio.com/colombia-inicia-el-ano-con-aumento-de-violencia-contra-lideres-sociales/>)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

El hecho se presentó al rededor de las 4:45 p.m; cuando Betancourt se encontraba con sus 3 hijos y esposa en su vivienda ubicada en el corregimiento de San Francisco del Rayo,** zona perteneciente al municipio de Montelibano**.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Según testigos, los agresores fueron dos hombre armados que ingresaron a la casa del líder y le propinaros varios impacto con arma de fuego, heridas que posteriormente le causaron la muerte.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### "Era, como todos los líderes sociales, un héroe invisible... "

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Betancourt a sus 42 años, se desempeñaba como coordinador de deportes de la Junta de Acción Comunal, *"Era, como todos los líderes, un héroe invisible, no solo hacia activismo desde el deporte, también era miembro del resguardo indígena de San Francisco del Rayo"*, señaló Andrés Chica, Director de la Fundación Social Cordoberxia.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Según Chica, el ataque se da luego de que el ejercito asesinara 15 días atrás a un integrante del Clan de Golfo, "*parece que le pasaron cuenta de cobro a Jorge por este asesinato"*. (Le puede interesar: <https://archivo.contagioradio.com/quien-fue-policarpo-guzman-el-historico-lider-social-de-argelia-cauca-que-fue-asesinado/>

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Y agregó que el asesinato del líder responde a un patrón de violencia entre el estado y los grupos al margen de la ley, *"los ciudadanos no hacen parte del conflicto y por lo tanto no deben ser vinculados a ninguna parte"*. (Le puede interesar: <https://archivo.contagioradio.com/no-dejen-solos-lideres-sociales-choco/>)

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### "Al sur de Córdoba está prohibido interlocutar con la fuerza pública"

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Chica afirma que las leyes sociales que han puesto los grupos armados irregulares en el territorio muchas veces se convierten en *"una lapida sobre la cabeza*", a tal punto que *"en este territorio al sur de Córdoba está prohibido interlocutar con la fuerza pública, ya sea Policía o Ejército, ha*cerlo *es un delito que pareciera costar la vida siempre, como en el caso de Jorge"*

<!-- /wp:paragraph -->

<!-- wp:quote -->

> *"Cuando asesinan a un líder social quien termina perdiendo es el pueblo colombiano y las libertades constitucionales"*
>
> <cite> Dir. Fundación Social Cordoberxia </cite>

<!-- /wp:quote -->

<!-- wp:paragraph {"align":"justify"} -->

Ante los reiterados hechos de violencia, Chica afirmó, que para el pueblo cordobés muchas veces resulta complejo concebir los procesos que se realizan en el municipio, *"es difícil entender y aterrizar una realidad positiva cuando siguen asesinando a personas todos los días en el país"*.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Sin embargo agregó que para él y los defensores de derechos humanos en la región, *"la bandera de promoción y defensa de DD.HH. sigue arriba, sigue diciendo que hay verdades que no quieren ser contadas y que nos toca contar en algún momento"*, y añadió, *a pesar de que esto representa un riesgo notable, es necesarios asumirlo,* *porque hay un pueblo, una sociedad en Córdoba que necesita acompañamiento y esperanza.*

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Finalmente, solicitó al Gobierno y a las autoridades garantizar la promoción y defensa de los derechos con seguridad; y ante el asesinato del líder agregó, *"pedimos que esta sea una investigación seria y real, con personas condenadas, pero sobre todo con garantías para que no se repitan los hechos"*. (Le puede interesar: <https://archivo.contagioradio.com/obispos-piden-reabrir-dialogos-con-eln-y-garantias-para-lideres-y-comunidades/>)

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Jaiber Alexander Quitumbo Ascue

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

El otro caso que enlutó al país en la últimas horas fue el del comunero indígena Jaiber Alexander Quitumbo Ascue, de 30 años quien según Eduin Capaz vocero del pueblo Nasa, fue asesinado el 14 de enero en horas de la mañana mientras trabajaba su parcela en la Vereda Vichiqui, resguardo de Toribio.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/FelicianoValen/status/1217131087324946434?ref_src=twsrc%5Etfw%7Ctwcamp%5Etweetembed%7Ctwterm%5E1217131087324946434\u0026ref_url=https%3A%2F%2Fpublish.twitter.com%2F%3Fquery%3Dhttps%253A%252F%252Ftwitter.com%252FFelicianoValen%252Fstatus%252F1217131087324946434%26widget%3DTweet","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/FelicianoValen/status/1217131087324946434?ref\_src=twsrc%5Etfw%7Ctwcamp%5Etweetembed%7Ctwterm%5E1217131087324946434&ref\_url=https%3A%2F%2Fpublish.twitter.com%2F%3Fquery%3Dhttps%253A%252F%252Ftwitter.com%252FFelicianoValen%252Fstatus%252F1217131087324946434%26widget%3DTweet

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph {"align":"justify"} -->

La comunidad indígena rechaza y lamenta el asesinato del comunero, así mismo exige atención en el departamento luego de los cinco casos de indígenas asesinados durante los primeros 14 días del 2020. (Le puede interesar: <https://archivo.contagioradio.com/sabedora-indigena-nasa-virginia-silva-es-asesinada-en-cauca/>).

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
