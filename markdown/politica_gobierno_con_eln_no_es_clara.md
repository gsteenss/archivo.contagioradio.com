Title: "Hay un desgaste del cese al fuego unilateral": Víctor de Currea
Date: 2017-12-14 18:37
Category: Entrevistas
Tags: Cese al fuego bilateral, ELN, gobierno y ELN
Slug: politica_gobierno_con_eln_no_es_clara
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/12/cms-image-000053662-e1513287591710.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Enlace Poli] 

###### [14 Dic 2017] 

[En medio del cese bilateral con entre el gobierno y la guerrilla del ELN, han surgido distintas situaciones como desplazamientos forzados e incluso asesinatos de líderes sociales, que tienen en cuestionamiento la efectividad de ese mecanismo. Para el profesor Víctor de Currea Lugo, aunque el cese empezó “bastante bien”, sin tener algún mecanismo de supervisión, ** se ha producido un desgaste que tiene que ver con la realidad de la guerra**.]

[De acuerdo con la explicación del profesor, aunque **está seguro de que el cese bilateral se prorrogará después del 9 de enero,** no es suficiente seguir con “cualquier cese” y tampoco sirve mantener la mesa en Quito sin un objetivo claro. Señala que debe haber una estrategia concreta que empuje las negociaciones, de tal manera que se garantice la vida de las comunidades y su participación.]

### **La situación con las comunidades** 

El informe sobre los primeros 50 días del cese al fuego, elaborado por más de 75 organizaciones reporta que se ha incrementado la violencia contra la población civil y los líderes y lideresas en los territorios.

Entre el 1 de octubre y el 20 de noviembre de 2017 se presentaron **299 víctimas individuales. De ellos, hay 45 asesinatos, 96 son personas heridas, 35 son víctimas de amenazas individuales y 25 colectivos amenazados**. En Nariño, Cauca, Caldas, Valle del Cauca, Antioquia y Chocó, mientras que las poblaciones indígenas, campesinas y afrodescendientes son las más afectadas.

[Para De Currea, el problema de la relación entre la población civil y el ELN está manifestándose en Nariño, con el asesinato del líder indígena Aulio Isamará en el Alto Baudó. Aunque el ELN ha insistido en respetar el cese al fuego, se debe “poner lupa a las denuncias que están haciendo las comunidades”, referentes a acciones de esa guerrilla que están violentando la tranquilidad de las poblaciones.]

### **Un problema de “dobles lecturas”** 

[El analista manifiesta que existe “un problema de dobles lecturas”, por ejemplo, dicho informe sostiene que las violaciones al cese han sido de un 73% responsabilidad de las fuerzas militares, mientras que al ELN apenas se le ha acuñado el 1%, los autores desconocidos comprenden el 17% de los casos y los paramilitares el 8%. Por ello, el profesor indica que desde los medios de comunicación “]**se ha creado el mito de que el ELN continúa con acciones de guerra”.**

[Ante ello, agrega que se debe tener en cuenta que el c**ese bilateral entre el gobierno y ELN se ha cumplido, pero este no incluye a los paramilitares, ni al EPL, ni a las disidencias de las FARC**, por ello en el Catatumbo han habido enfrentamientos entre esa guerrilla y el EPL, en Nariño ha habido conflictos con disidencias de las FARC y en Chocó con grupos paramilitares como las AGC. Esto, “no se puede ver como una violación al cese, pues ninguna de las partes han renunciado al derecho a la defensa, y en ese sentido el ELN tiene derecho a defenderse, igual que las FFMM en caso de un ataque”, señala Víctor de Currea.]

[Finalmente para el analista, lo que debe haber es el diseño de nuevos mecanismos de supervisión y nuevos compromisos tanto de las partes como de las comunidades, que pueden acompañar y supervisar el cese al fuego, de manera que no solo haya buena voluntad, sino que haya un objetivo claro, que debe ir en vía de proteger a los civiles y garantizar la participación de la sociedad.]

[“**La pregunta no es quién continúa en la mesa de Quito, la pregunta es cuál es la política del presidente Santos con el ELN, porque a mi no me queda claro hoy por hoy qué es lo que el gobierno persigue con esa mesa”**, concluye.]

<iframe id="audio_22671498" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_22671498_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
