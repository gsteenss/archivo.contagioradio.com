Title: Aquí, sí pasan cosas
Date: 2017-12-25 12:08
Category: Camilo, Opinion
Tags: Bajo Atrato, Hernan Bedoya, lideres sociales, Mario Castaño
Slug: aqui-si-pasan-cosas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/12/lideres-sociales.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Contagio Radio 

#### Por [Camilo De Las Casas](https://archivo.contagioradio.com/camilo-de-las-casas/) 

#### 25 Dic 2017 

Seguimos presos de nuestras propias creencias, esas llenas de la realidad de la injusticia social y ambiental, de la muerte violenta. Creencias que nos llevan a ver en blanco y negro, sin matices. Estamos en otro tiempo y con muchas posibilidad de un giro histórico en que el nuevo lenguaje es fundamental. Los falsos profetas del castrochavismo con perversas mentiras ayudan muy eficazmente a instalar esas creencias desde el lado de la dominación que quiere ser totalizante.

La retórica repetitiva y su simbólica del miedo son eficaces ante un presidente carente de identidad de Jefe de Estado, y a merced de cada incendio, un pésimo apagafuegos. Es verdad que más de 166 líderes asesinados entre 2016 y diciembre de 2017, y ya cerca de otra treintena de integrantes de base de los que dejaron las armas de las FARC, duelen y obnubilan el alma. Los privados de la libertad por razones políticas y por montajes judiciales sin que se aplique el derecho humanitario o las libertades acordadas.

Ese es el rostro de hierro de la "democracia". Ante ella, el símbolo de unas sencillas máscaras con el ojo izquierdo derramando una lágrima de sangre expresa el contra rostro, al hierro. En las personas de a pie, en los analistas, y columnistas la imagen de las victimas con el rostro cubierto ha sido contundente.

Hablan de la ausencia de garantías ante el poder criminal del empresariado palmero, ganadero, bananero, maderero combinando negocios con traficantes de drogas, que disfrutan el desplazamiento y el despojo violento. Se habla a través de ellas de más de 250 lideres afros, mestizos e indígenas amenazados de muerte, que pueden correr la suerte de Mario Castaño y Hernán Bedoya, ambientalistas y defensores de la tierra asesinados en estas semana, por delincuentes vestidos de empresarios, beneficiarios del paramilitarismo de Estado de los años 90, El Procurador Carillo, escuchando la contundencia testimonial, expresó que los asesinatos de lideres en bajo Atrato son sistemáticos y están unos empresarios que disfrutan de apartamentos en edificios de lujo de Bogotá.

El negacionista y Fiscal Martínez, aseveró que la hipótesis de los asesinatos de Mario Castaño y Hernán Bedoya, apuntan a empresarios despojadores. Aunque son verdades conocidas en Urabá y el bajo Atrato, las mismas fraccionan el lenguaje encubridor y de desviación de la expresiones del Ministro de Defensa Villegas que muy coloquialmente dice que los asesinatos de los líderes sociales, tienen diversos móviles, entre ellos "líos de faldas, de linderos y de la economía ilegal". Es evidente que la brigada 17 históricamente ha tenido serias responsabilidades, y que las nuevas fuerzas armadas en esas regiones, tienen responsabilidad en lo que sigue pasando. con la protección de ese empresariado mafioso, pero hay avances.

Es difícil decir ahora que se trata de fuerzas oscuras.Se va abriendo paso, a aquellos que se quieren ocultar de la JEP, los planificadores y beneficiarios de la violencia en esta región. Se necesitan más máscaras. Ya es secreto a voces el nombre del reconocido Javier Restrepo Girona. Este empresario, ocupante de mala fe en tierras colectivas de Curvaradó, Pedeguita y Mancilla con siembras de palma, banano, y en el lugar conocido como La Sumbona con un pequeño laboratorio, entre otras.

El empresario antioqueño con aeronave para campaña del gobernador Luis Pérez, está tras de un entramado criminal insospechado, cómo lo expresan diversos informes. Es más existe prueba de ser el propietario del predio llamado La Florida, en el que se encontraron más de 10 toneladas de cocaína.

Si se ven los antecedentes, dicen analistas, ¿A quién sirvieron los asesinatos de los dos recientes líderes asesinados, denunciantes de Restrepo Girona? El avance hacia una sociedad en paz, no la Pax Neoliberal, con los incumplimientos del Acuerdo del Teatro Colón, las trampas en la Mesa de Quito, y también, hay que decirlo con los errores en el ejercicio de la política del nuevo partido de las FARC y del ELN, que le ayudan a ese falso imaginario del castrochavismo, se está dando. El rostro de la mayoría de la clase dirigente, política, religiosa, empresarial y mediática, y no solo los horrores y los responsables de esa guerra, está saliendo a la luz. 39% de la gente piensa hoy votar en Blanco, casi tres veces más que lo que lo harían por uno de los actuales candidatos ¡¿Qué significa? Mucho, mucho.

Las máscaras de las victimas desmoronan el rostro de hierro y los que están detrás de él. La sociedad es de personas empoderadas, más que de gentes que en el blanco y negro se experimentan desempoderadas, en manos de gente poderosa y máquina mundiales inmodificables.

Cada ciudadana tiene la capacidad de parar los ciclos de la historia de sangre emanada y de la anunciada. ¡El porvenir es hoy¡.Superar el ciclo de la historia para pasar el poder de los tiranos a algo distinto a la tiranía de las oprimidas del espejismo significa cambiar la manera de ver la realidad, más allá de ese dolor por el daño causado.

Las máscaras están deshaciendo el hierro. Gracias lideres de Urabá antioqueño y bajo Atrato por esta lección de vida. La crisis despierta una hermosa sabiduría.

PD: Hay más máscaras de estos días que deshacen. Las reacciones a la columna de Antonio Caballero "Acoso", de Carolina Sanín y de Yolanda Ruiz, entre otras.Y las declaraciones de Daniel Rendón Herrera, "Don Mario", antes de su extradición a los EE.UU, hablando de Álvaro y Santiago Uribe (empresario) y de el candidato de Cambio Radical, que es algo más que corrupto y vinculado con el paramilitarismo de los Miguel Arroyabe y don "Martín Llanos".

#### [Leer más columnas de opinión de  Camilo de las Casas](https://archivo.contagioradio.com/camilo-de-las-casas/)

------------------------------------------------------------------------

######  Las opiniones expresadas en este artículo son de exclusiva responsabilidad del autor y no necesariamente representan la opinión de Contagio Radio.
