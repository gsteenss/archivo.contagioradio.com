Title: El día en que La Fania All-Stars puso a bailar a Nueva York
Date: 2015-08-28 15:45
Category: Cultura, En clave de son
Tags: En clave de son, Fania All Stars, Jerry Masucci, Live at the Cheetah, Our latin thing, Salsa
Slug: el-dia-en-que-la-fania-all-stars-puso-a-bailar-a-nueva-york
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/08/chetaa-e1440793158487.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: The New York Times] 

<iframe src="http://www.ivoox.com/player_ek_8400071_2_1.html?data=mZmdkpWbdY6ZmKiakpaJd6KpmIqgo5aYcYarpJKfj4qbh46kjoqkpZKUcYarpJKyzpDIaaSnhp2xw5DJsozl1sqYzsaPqsLiysaYw9HQcdTowtfgjdXZt9CfwpDPw87QpdOfwpDb18rapY6ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### ["En clave de Son" Especial Live at the Cheetah"] 

###### [28 Ago 2015] 

El pasado 26 de Agosto se cumplieron 43 años del "Live Cheetah", día en que La Fania All Stars, contra todo pronóstico, realizó uno de los conciertos mas importantes en la historia de la música latina en los Estados Unidos. Si bien la salsa atravesaba en aquel entonces por una crisis de identidad, los barrios marginales de latinos en Nueva York, indicarían el camino a seguir.

[![Fania All Stars - Live at the Cheetah COVER](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/08/Fania-All-Stars-Live-at-the-Cheetah-COVER.jpg){.aligncenter .size-full .wp-image-13034 width="400" height="360"}](https://archivo.contagioradio.com/el-dia-en-que-la-fania-all-stars-puso-a-bailar-a-nueva-york/fania-all-stars-live-at-the-cheetah-cover/)

Sería Jerry Masucci un ambicioso y arriesgado productor musical, de la mano de Johnny Pacheco y varias estrellas de la música caribeña de la década, quienes trazarían los senderos de ese camino, regalándole al mundo, esa noche de jueves de 1971, una nueva y completa estructura a la salsa, la salsa de barrio, la verdadera salsa.

La excusa: el relanzamiento de un club llamado “Cheetah”, Masucci fue entonces contactado por el administrador, para dar un concierto que devolviera la vida al establecimiento, popular a finales de los años 50; a pesar del mal inicio que la Fania All Stars, había tenido en 1968, con la presentación en vivo en el Red Garter, Masucci aceptaría dar una presentación a su estilo, una Big Band con innovadores arreglos musicales.

Aquella noche, sería de vital importancia para el grupo, ya que no solo darían el concierto un jueves (día con poca afluencia de gente ) sino que se arriesgarían además a registrarlo en un audiovisual "Our latin thing" (Nuestra cosa latina) dirigido por Leon Gast, algo nunca visto hasta esa fecha.

<iframe src="https://www.youtube.com/embed/XWpl0seZf3w" width="420" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

Las Estrellas de la Fania, saltaron a la tarima del "Cheetah" club ubicado en pleno corazón de Manhattan, con una formación única, entre  los que se cuentan a Cheo Feliciano, Hector Lavoe, Willie Colon, Adalberto Salcedo, Ray Barreto, Roberto Roena, con toda la energía necesaria para dar a los asistentes una descarga de fresca y renovada salsa.

No solo el lugar se llenó al doble de su capacidad, con el material recopilado de la presentación se lanzarían dos volúmenes de un disco llamado “live at the Cheeta” y como si fuera poco, esta fecha pasaría a la historia, como el nacimiento de la salsa para algunos y el fortalecimiento de la misma para otros.

Si quiere saber todos los detalles, de esta magnifica noche, lo invitamos a escuchar el nuevo espacio de Contagio Radio, dedicado a la salsa “En Clave de Son” que abrirá hoy sus puertas desde las 6pm.
