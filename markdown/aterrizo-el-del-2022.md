Title: Aterrizó el del 2022
Date: 2019-07-18 12:42
Author: Camilo de las Casas
Category: Camilo, Opinion
Tags: corrupción, Odebrecht, parapolítica, politiquería
Slug: aterrizo-el-del-2022
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/andres_felipe_arias-1_0.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/Uribe-hoy-Magadalena.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/andres-felipe-arias.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/andres-felipe-arias-1.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto montaje Contagio Radio 

Se trata de un perseguido por una Corte Suprema de Justicia corroída por la corrupción y la politiquería de izquierda, que lo condenó por 17 años. ÉL ni él, nunca podrán ser calificados de delincuentes. Ellos no son David Murcia  Guzmán, el estafador. No son Pinochet, ni Videla ni Fujimoris ni Garcías. Simplemente son honorables. Impolutos.

Es un gran estratega. El escenario mediático lo abrió con la llegada en un vuelo chárter, una excepción para un extraditado, ahora repatriado. Nadie sabe quién pago el chárter, ya saldrá una razón sin pudor. Tampoco porque el Ministerio de Relaciones Exteriores guardó silencio y dejó de anunciar su llegada con bombos y platillos. Les estaba sirviendo de telón el desconocido paradero de Santrich

El viernes aterrizó él de ÉL. Era el elegido, nunca Santos. Pero un turbio proceso judicial confeccionado por la Corte Suprema de Justicia, nunca visto antes, lo condenó injustamente. El exministro de Agricultura entre 2005-2009 fue castigado arbitrariamente por celebración de contratos sin el cumplimiento de los requisitos legales y peculado por apropiación a favor de terceros, autorizando la entrega de recursos públicos a pobres familias campesinas cómo los Magdalena Dávila,  Lacouture y los Vives. El Agro Ingreso Seguro, AIS, era para este tipo de grupos familiares productivos,  no, para esos campesinos de a pie. Estos no saben qué hacer con la tierra y con un apoyo financiero del Estado. Eso habría sido AIP, Agro Ingreso Perdido. Los de AIS, generan riqueza, progreso, empleo.

La propuesta de ÉL es un proyecto de ley para que exista una segunda instancia en su decisión judicial. Esto a la luz del derecho internacional de los derechos humanos es un principio básico. Pero ÉL propugna por los derechos humanos *per se*, en particular, en este caso, y en varios de los condenados por parapolítica, sin que lo haga explicito, porque su convicción por los derechos humanos es proporcional al proyecto de poder que él, representa. Un uso funcional para lograr su apuesta estratégica de Estado Comunitario, una nueva forma de dominación en aparentes libertades que brindda la seguridad.

La astucia parlamentaria que logra varios propósitos. Arrinconar a una justicia temerosa, incluyendo a la JEP, que evita sacar fuerzas de sus debilidades y de la necesidad de reconocerse como lo qué pretende ser justa consigo misma, y actuar de tal manera que el conjunto del aparato de justicia logre un consentimiento social de objeción a la cultura criminal en la política y la economía. Fraccionar la débil oposición al gobierno de Él a través de Duque, con una nueva forma de mermelada, si se quiere penal. El cuestionado y sancionado sector corrupto del senado, del pasado, y el del  presente tienen un nuevo mecanismo que bajo la bandera de los derechos humanos, igualdad ante la ley, tendrá en sus manos un nuevo mecanismo para intentar labrar su impunidad. Fomentar el ánimo a una reforma constitucional que unifique en una sola Corte la distribución de funciones y decisiones judiciales, desmoronando la JEP y el bloque judicial que ha enfrentado la cruzada de Duque contra la justicia. Todo en una perspectiva de gestar el Estado de Opinión que se ha debilitado.

Para todos empieza a hacerse claro, él es sin duda alguna, librado de la “injusta” sanción penal en esa doble instancia retroactiva, su alter ego. él será,  el próximo candidato de Él, de su juguete, el Centro Democrático.

Regresó a Colombia el modelo 2022 de Él. Tienen la misma estatura, el mismo perfil, el tonito, y la misma capacidad de posar como los seres humanos bondadosos, inocentes, de familia tradicional (católica). Ellos los perseguidos al librar las campañas más loables contra el terrorismo, contra la corrupción, contra la pobreza, contra los abusos liberales. Él y él, las victimas de una oposición comunista, rancia,  corrupta, narca, castrochavista, que usó del aparato judicial contra un benefactor de los recursos públicos para proveer servicios a quiénes lo necesitan para generar progreso.

Ya nos podemos imaginar todo lo que se argumentara de la relación de él en sus labores profesionales  con Odebrecht cuando gozaba de sus libertades por Él. “Solamente he estado y  estaba pensando en lo mejor para el país”. Así se justifican él y Él con los grandes publirreportajes que deberán salir en los medios empresariales en los próximos días o semanas.

Ya lo veremos. Un hombre sufrido, de familia, librado a pulso, honesto a toda prueba. Ahí está el futuro. ¡Qué vergüenza¡

**Leer más columnas de [opinión de Camilo de las Casas](https://archivo.contagioradio.com/camilo-de-las-casas/)**
