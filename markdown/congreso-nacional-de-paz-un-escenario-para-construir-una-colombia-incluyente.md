Title: Congreso Nacional de Paz escenario para construir una Colombia incluyente
Date: 2017-05-02 15:57
Category: Nacional, Paz
Tags: Acuerdos de paz de la Habana, Congreso Nacional de Paz, diálogos de paz con el ELN
Slug: congreso-nacional-de-paz-un-escenario-para-construir-una-colombia-incluyente
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/05/congreso.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Congreso Nacional de Paz] 

###### [02 May 2017]

Con una plenaria en la Plaza de Bolívar finalizó el Congreso Nacional por la Paz, pese a que el presidente del Congreso hubiese vetado la participación de Iván Marquez y del ELN, diferentes organizaciones sociales, ambientalistas y ciudadanas, se juntaron a este nuevo escenario **para velar por la implementación de los Acuerdo de la Habana y apoyar los diálogos con el ELN**.

De acuerdo con Natalia Parra, vocera de la plataforma ALTO, el balance de esos tres días de Congreso que se desarrollaron en 19 de departamentos y en algunas zonas veredales, es positivo, porque “**salda la deuda histórica frente al centralismo en el país, con la aplicación de los Acuerdos de Paz**” que se enfocan en las problemáticas de las regiones.

“**Una de las grandes lecciones de este encuentro es lo territorial y el valor de las regiones dentro de toda la construcción de los acuerdos**” afirmó Parra y agregó que otro de los aportes a este escenario es la participación de sectores ciudadanos como los ambientalistas, los colectivos de derechos de las mujeres, de la comunidad LGTBI, que se han empezado a gestar desde los diálogos con el ELN.

Frente a la decisión de Mauricio Lizcano, presidente del Senado y de Miguel Ángel Pinto, presidente de la Cámara de Representantes, sobre vetar la participación de Iván Márquez y el ELN, en el congreso de la República, Parra expresó que “**fue una afrenta” que no entorpeció el trabajo que se ha venido adelantando en el país sobre construcción de paz**. Le puede interesar: ["Las FARC-EP y sus posibles coaliciones políticas para el 2018"](https://archivo.contagioradio.com/las-farc-ep-y-sus-posibles-coaliciones-para-las-elecciones-2018/)

“Es un saldo maravillo que, pese a que no prestaron el Congreso, **nos reunimos todos y todas a pensar en la paz, a proyectar aspectos en la Plaza de Bolívar,** tan significativa en la paz de Colombia y saliendo adelante”. Le puede interesar: ["Hay fallas pero hay voluntad para implementar Acuerdo de Paz: Unión por la paz"](https://archivo.contagioradio.com/hay-fallas-pero-hay-voluntad-para-implementar-el-acuerdo-de-paz-union-por-la-paz/)

Durante el Congreso Nacional de Paz, la invitación a los candidatos y su participación estuvo abierta, sin embargo, la única precandidata que hizo presencia fue la senadora por la Alianza Verde, Claudia López, quien alertó sobre una posible segunda vuelta en las elecciones entre dos candidatos de ultra derecha, por lo que señaló la importancia de tener un candidato **fuerte desde los movimientos sociales y diversos sectores de la ciudadanía, desde la primera vuelta.**

<iframe id="audio_18465081" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_18465081_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio]{.s1}](http://bit.ly/1ICYhVU)[.]{.s1}
