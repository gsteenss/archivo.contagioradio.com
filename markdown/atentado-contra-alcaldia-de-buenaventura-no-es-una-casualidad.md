Title: Atentado contra Alcaldía de Buenaventura no es una casualidad
Date: 2020-08-03 23:01
Author: AdminContagio
Category: Actualidad, Comunidad
Tags: buenaventura, Paro Cívico de Buenaventura
Slug: atentado-contra-alcaldia-de-buenaventura-no-es-una-casualidad
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/Buenaventura.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","fontSize":"small"} -->

Foto: Víctor Vidal, alcalde de Buenaventura

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

En horas de la noche del pasado 1 de agosto fue lanzado un artefacto explosivo a la Alcaldía de Buenaventura, Valle del Cauca. Durante el suceso ninguna persona resultó herida, sin embargo el hecho confirma las denuncias hechas por organizaciones sociales que vienen alertando sobre las reiteradas amenazas en contra de la administración municipal que bajo el mandato de Víctor Vidal ha buscado mejorar las condiciones de una de las ciudades más pobres del país.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Al respecto, el padre **John Reina, parte de la Diocesis de Buenaventura**, expresa que se ha desatado una ola de violencia pronunciada en distintos barrios de la ciudad por parte de los grupos que operan en el municipio, "hay sectores que se los ha tomado la delincuencia porque llevan muchos años operando y eso ha hecho camino", una situación que explica se recrudece en medio del establecimiento de fronteras invisibles.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Desde inicios del aislamiento preventivo, líderes sociales habían advertido cómo la presencia y control de actores armados en el territorio no se había detenido ni siquiera en tiempos de pandemia, condición que podía comprometer aún más a los habitantes del municipio que son amenazados por grupos como **La Empresa, los Urabeños, La Local o disidencias de las FARC, "van cambiando el nombre pero a la final son los mismos que tienen el poder",** relata el religioso. [(Le recomedamos leer: Preservemos la vida ante la pandemia: Espacio humanitario a actores armados en Buenaventura)](https://archivo.contagioradio.com/preservemos-la-vida-ante-la-pandemia-espacio-humanitario-a-actores-armados-en-buenaventura/)

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Una respuesta violenta a la ruptura con la delincuencia

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Según Ulpiano Riascos, secretario de Gobierno y Seguridad del municipio, aa lo largo de la adminsitración del alcalde, [Víctor Vidal](https://twitter.com/granvidaaal) es la primera vez que se produce un hecho de violencia al lanzar un artefacto explosivo contra la Alcaldia, sin embargo, el hecho "constituye un acto de suma gravedad, no es una casualidad, son factores de poder los que están detrás de estos actos".

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

El secretario explica que además, se han presentado actos de hostigamiento a través de redes sociales, al igual que "envíos de mensajes de WhatsApp con el fin de atacar a la administración". Denuncia además que se está "remunerando a periodistas con el fin de desprestigiar a la Alcaldía". [(Le puede interesar: Asciende a 10 la cifra de líderes del Paro Cívico de Buenaventura amenazados)](https://archivo.contagioradio.com/asciende-a-10-la-cifra-de-lideres-del-paro-civico-de-buenaventura-amenazados/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

"Aquí había una dinámica perversa que articulaba la delincuencia con los políticos que llegaban a la Alcaldía y recibían grandes utilidades de la administración, eran favorecidos con contratos y nombramientos" relata por lo que al romper con esa lógica se desata una acción de violencia contra la Alcaldía pues implica una ruptura radical con el pasado de administrar lo público".

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### La oportunidad de hacer un cambio real en Buenaventura

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Por su parte , para el sacerdote Reina, esta Alcaldía "implica la esperanza de un pueblo que ha estado en manos de bandidos y personas inescrupulosas que se ha aprovechado de los recursos públicos, naturales y humanos" del municipio, por lo que su llegada ha significado quitarle la autoridad a los que "siempre han estado en la hegemonía del poder". [(Lea también: Del paro cívico a gobernar Buenaventura, los retos que asume Víctor Hugo Vidal)](https://archivo.contagioradio.com/del-paro-civico-a-gobernar-buenaventura-los-retos-que-asume-hugo-vidal/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Para ella resalta que hay dos acciones muy importantes, la primera: una intervención de la justicia, por lo que cuando esta se fortalezca, - señala - se empezarán a ver resultados, por otro lado resaltala importancia de que existe una verdadera inversión social por parte del Estado, **"le pedimos al Gobierno que cumpla con los Acuerdos del Paro Cívico para que podamos sacar adelante a Buenaventura, no tenemos otra opción y está en manos del presidente"**, afirma el padre Reina.[(Lea también: Si el Gobierno no cumple retomaremos el paro cívico: habitantes de Buenaventura)](https://archivo.contagioradio.com/si-el-gobierno-no-cumple-retomaremos-el-paro-civico-habitantes-de-buenaventura/)

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->
