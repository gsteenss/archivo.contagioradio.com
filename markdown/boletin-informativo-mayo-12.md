Title: Boletín Informativo Mayo 12
Date: 2015-05-12 17:26
Author: CtgAdm
Category: datos
Tags: Boletín Informativo, Mayo 12 de 2015, Noticias del día en Contagio Radio
Slug: boletin-informativo-mayo-12
Status: published

[*Noticias del día:*]  
<iframe src="http://www.ivoox.com/player_ek_4485425_2_1.html?data=lZmll5mWeY6ZmKiakpWJd6KkkZKSmaiRdI6ZmKiakpKJe6ShkZKSmaiRhtDgxtmSpZiJhaXijK7byNTWscLoytvcjbLFvdCfkpeah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

-La Procuraduría Segunda Delegada para la Vigilancia Administrativa, inhabilitó por 11 años al ex-rector de la Universidad Industrial de Santander Jaime Camacho Pico, para el ejercicio de cargos públicos, por no realizar denuncias ante las autoridades competentes para alertar sobre la presencia de paramilitares en la institución, ni la violación a los DDHH que pretendían realizar las Águilas Negras a través de ejecución de un Plan Pistola. Habla Daniel Castillo, estudiante de la UIS.

-Durante la guerra de Vietnam, entre 1961 y 1971, los Estados Unidos utilizaron 16 herbicidas para fumigar las selvas y campos de de esa nación con el objetivo de acabar con la vegetación en la cual podían esconderse las guerrillas del Viet Cong. La aspersión de más de 12 millones de galones del Agente Naranja sobre el país asiático afecto las 3.181 villas que fueron rociadas quedando inutilizables para la siembra de alimentos, perjudicando tanto a guerrilleros como a soldados estadounidenses y a las cerca de 5 millones de personas sobre los cuales se realizó la fumigación aérea. La Cruz Roja estima que actualmente cerca 1 millón de personas han sido mutiladas, tienen discapacidades o problemas de salud como consecuencia de su contacto con el territorio infectado por el tóxico, incluyendo al rededor de 150 mil niños.

-Este martes 12 de mayo se celebra el día del Río Bogotá, que de acuerdo a Héctor Buitrago, integrante de la agrupación musical "Aterciopelados", se trata de un río olvidado por los bogotanos.

-En 2014 aumentaron en un 50% las concesiones de asilo político en la UE respecto a 2013 debido a los conflictos armados en Oriente Próximo y África. Según Eurostat, Agencia Estadística para la Comunidad Europea desde 2008, 780.000 personas han recibido asilo político en países de la UE.

-En España se adelanta una campaña de objeción fiscal al gasto militar que consiste en la desobediencia civil, social, pacífica y activa en colaborar con los gastos que conllevan la preparación de la guerra y su ejecución. Se trata de decir: "con mi dinero no se financian las guerras".

-El próximo 14 de mayo, en el Consejo Nacional de Estupefacientes se tomará una decisión sobre la suspensión, o no de fumigaciones con glifosato, que de acuerdo con Astrid Puentes, co-directora de la Asociación Interamericana para la Defensa del Ambiente AIDA, no solo trae graves consecuencias en la salud, sino también para el ambiente.

-La UE quiere lanzar una operación militar para frenar y detener a las mafias radicadas en las costas de Libia y así frenar la emergencia humanitaria de inmigrantes que intentan acceder al sur de Europa .

La compañía petrolera Shell logró aval por parte del gobierno de Estados Unidos para realizar perforaciones en aguas del Océano Glacial Ártico, pese al riesgo que estas actividades le generan al ambiente, específicamente en la Antártida considerada como una zona de alto valor ecológico.
