Title: Gobierno de Nigeria guarda silencio ante masacre de dos mil personas
Date: 2015-01-21 18:31
Author: CtgAdm
Category: El mundo
Tags: 2 mil personas masacre Nigeria Charlie Hebdo, Boko Haram
Slug: nigeria-guarda-silencio-sobre-masacre-de-2-mil-personas
Status: published

###### Foto:RT 

[Cerca de 2 mil personas murieron quemadas vivas a manos del grupo armado islamista Boko Haram en Baga], entre el 3 y el 7 de Enero, en la ciudad fronteriza de Nigeria con Chad. El responsable de la milicia se ha adjudicado la masacre mediante un vídeo.

Boko Haram es una milicia islamista radical, cuyo principal objetivo es la imposición de la Sharia o ley islámica, que es considerada en el norte como justicia informal, pero rechazada frontalmente por los estados del sur de mayoría cristiana.

Aunque la Sharia es considerada como la ley islámica, es más un modo de vida interpretable, que es tomada como patrón de grupos extremistas para así, crear del código de conducta, un dogma sobre lo que se puede o no hacer en la vida.

La milicia se crea en 2002, tomando posiciones en el norte del país. Su acción mas sonada fue en abril del 2014 cuando tomaron una escuela secuestrando a mas de 200 niñas con el objetivo de denunciar la política educativa occidental que se aplica en dichas escuelas.

La polémica generada en las redes sociales no proviene de la masacre en sí, sino de la ausencia de declaraciones, o condenas del propio presidente del país Goodluck Jonathan, quién además de no pronunciarse sobre lo sucedido en su país fue uno de los presidentes que más  condeno los  atentados de Charlie Hebdo, mientras se sucedía la masacre en su propio país.

Distintas organizaciones sociales han creado una tendencia parecida a la de" Je suis Charlie", llamada "Je suis Baga", criticando la escasa solidaridad por parte de la comunidad internacional con la masacre en comparación con lo sucedido en Francia.
