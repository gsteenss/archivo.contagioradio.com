Title: Grecia vuelve a soñar
Date: 2015-01-26 03:42
Author: CtgAdm
Category: El mundo, Política
Tags: elecciones, Grecia, podemos, Syriza
Slug: grecia-vuelve-a-sonar
Status: published

###### Foto: [[rtve.es]

Con la frase, “hoy Grecia vuelve a soñar, hoy está permitido” miles de personas celebran la victoria electoral de Syriza, con un **36% de los votos, ganando 149 escaños en el congreso de 300 posibles**, y quedando a sólo dos escaños de la mayoría absoluta.

Aunque el margen de escaños no le da a Syriza para ser el partido con la mayoría absoluta, si es muy probable que muchas de las políticas del partido estén encaminadas a **renegociar, e incluso, no pagar la deuda**, que durante 5 años ha mantenido a la población griega sumida en la pobreza, luego de que se recortaron casi por completo los rubros de inversión social del Estado.

Tanto la población de Grecia, como la gran mayoría de la población europea, están viendo en el **surgimiento de nuevas fuerzas políticas como Podemos en el Estado Español, y ahora Syriza en Grecia,** una esperanza de derrotar las medidas económicas de la llamada Troika.

En esta jornada electoral el **63,5% de la población, optó por darle su voto al discurso de la esperanza** frente al discurso del miedo que uso el partido de gobierno.

Alexis Tsipras, afirmó en su discurso del triunfo “**Han vencido el miedo y recuperado la esperanza**. Nuestra victoria es una victoria de todos los pueblos de Europa que luchan contra la austeridad. Nuestra prioridad por encima de todo es **devolver la dignidad perdida a Grecia**, con un Gobierno para todos los griegos, nos hayan votado o no”.
