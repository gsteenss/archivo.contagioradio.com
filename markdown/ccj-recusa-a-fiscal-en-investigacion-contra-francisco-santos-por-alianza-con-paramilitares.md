Title: CCJ recusa a Fiscal en investigación contra Francisco Santos por alianza con paramilitares
Date: 2015-02-16 20:21
Author: CtgAdm
Category: Judicial, yoreporto
Tags: Francisco Santos, Paramilitarismo
Slug: ccj-recusa-a-fiscal-en-investigacion-contra-francisco-santos-por-alianza-con-paramilitares
Status: published

###### Foto: elpueblo.com 

Hoy 16 de febrero de 2015 la **Comisión Colombiana de Juristas** presentó una solicitud de recusación a la Fiscal María Claudia Merchán Gutiérrez, con fundamento en i) la mora de más de un año para impulsar la investigación que se sigue en contra del **expresidente Francisco Santos** por su supuesta relación con grupos paramilitares, hecho que demuestra el desinterés y la apatía con la cual ha asumido la misma y constituye una muestra de la falta de independencia e imparcialidad de la administración de justicia; y ii) la compulsa de copias por parte de la Corte Suprema de Justicia, en la cual se ordena la investigación de esta funcionaria por la absolución de Jairo Enrique Merlano Fernández -quien era investigado por sus presuntos nexos con los grupos paramilitares- cuando ejercía como Juez Tercera Penal del Circuito de Bogotá.

La **investigación preliminar** contra Francisco Santos fue abierta para indagar sobre la supuesta alianza con grupos paramilitares y favorecer la conformación de un grupo paramilitar en Bogotá. Aunque hechos que sirven de fundamento a la presente investigación preliminar contra Francisco Santos son de suma importancia para la sociedad colombiana y especialmente para las víctimas, el impulso de la misma por parte del Despacho de la Fiscalía 1 Delegada ante la Corte Suprema de Justicia en los últimos meses ha sido nulo, sin importar que las partes que hacemos parte del proceso hemos realizado varias solicitudes de trascendencia que permitirían el avance de la investigación.

La demora de la Fiscal María Claudia Merchán ha permitido que la defensa de Francisco Santos por medio de escrito allegado al despacho el 30 de octubre del 2012, solicitó al despacho la definición inmediata del proceso por considerar que ya habían transcurrido más de 5 años desde abierta la investigación, y posteriormente, el 13 de abril de 2013 y a pesar de que a través de su abogado inicial había renunciado a la prescripción de la acción penal, su nuevo defensor solicitó por escrito la prescripción de la acción penal por haber transcurrido el término legal para tales efectos sin que se hubiera proferido resolución de acusación.

Por otra parte, el 12 mayo de 2014, la Comisión Colombiana de Juristas presentó un escrito por medio del cual se opuso a la prescripción de la acción penal y con el objetivo de impulsar la investigación solicitó la práctica de pruebas que son conducentes para esclarecer los hechos materia del proceso. El 16 de junio del mismo año se presentó nuevamente un escrito solicitando la práctica de nuevas pruebas y reiterando la solicitud inicial, sobre las cuales la Fiscal titular del despacho nunca se pronunció.

De acuerdo con lo anterior, se puede apreciar de modo objetivo que ha transcurrido más de un año desde que fue emitida por parte del despacho su última decisión al frente de la presente investigación. Lo anterior ocurre a pesar que las personas que participan en el proceso, y en particular las víctimas, han insistido en que se tome una decisión de fondo orientada a impulsar la investigación con el propósito de establecer la verdad e impartir justicia como le corresponde a las autoridades judiciales. Esta  inactividad de la Fiscalía se torna más grave aún si se tiene en cuenta el incumplimiento de los términos procesales ha suscitado la solicitud de prescripción por parte de la defensa de Francisco Santos.

#### **[Comisión Colombiana de Juristas en \#YoReporto ]** 
