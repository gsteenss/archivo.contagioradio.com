Title: Los pueblos originarios olvidados de Formosa
Date: 2015-04-23 17:03
Author: CtgAdm
Category: Capote, Opinion
Tags: Argentina, bandera wiphala, Buenos Aires, Eva Perón, pueblos originarios de Formosa, Qopiwini
Slug: los-pueblos-originarios-olvidados-de-formosa
Status: published

###### Foto: eldiariodebuenosaires.com 

[**Por [Laura Capote ](https://archivo.contagioradio.com/laura-capote/) **]

Pareciera que en épocas electorales, las y los candidatos recordaran que son sujetos a la votación popular y volcaran sus intereses y acciones en competir por quién hace más obras, o queda mejor parado con la sociedad civil en su conjunto, para así garantizar su victoria en las urnas y su legitimidad en la opinión pública.

La competencia por quién ofrece una mejor ciudad o un mejor país a las y los argentinos esta en su punto más álgido: A días de las elecciones primarias abiertas y simultáneas (PASO), la ciudad, el metro, las facultades están empapeladas por afiches y propuestas de los distintos candidatos que no hacen otra cosa que demostrar por qué su victoria sería garantía de ya sea una “Evolución”, una continuación del proyecto o un cambio radical del mismo que favorecería a la población argentina en su conjunto.

Pero en medio de la Capital federal, esa que parece el ombligo del mundo, (o por lo menos del país) donde se reduce muchas veces lo que sucede en toda la territorialidad argentina a los problemas que atraviesan los habitantes de la ciudad, más exactamente en el punto de intersección entre la Av 9 de Julio y Av de Mayo, bajo la imagen emblemática de Eva Perón, se encuentra un campamento de argentinos y argentinas también, pero no esos de apellidos europeos, sino de pueblos originarios de lo que era esta, nuestramérica, antes de la colonización.

Desde hace más de tres meses, personas pertenecientes a los pueblos Qom, Pilagá, Wichí y Nivaclé, originarios de la provincia de Formosa, Argentina, instalaron un campamento para exigir a los tres poderes del Estado, además ser escuchados y valorados como pueblos originarios, la resolución inmediata del despojo de tierras al que están siendo sometidos por parte del gobierno de la provincia, y de situaciones donde la contaminación con agroquímicos de sus principales fuentes de agua, ha causado graves problemas de salud a las personas de las comunidades.

“La lucha por la tierra, la defensa de la madre tierra” es uno de los emblemas que aparece en la gran carpa con la bandera wiphala, acompañado de muchos otros que giran alrededor de la territorialidad de dichos pueblos y sus construcciones espirituales enraizadas en sus territorios. Buscan a como de lugar (muestra de ello son las condiciones en las que se encuentran en este acampe) que el gobierno federal les escuche y garantice su derecho sobre el territorio que llevan ocupando ancestralmente.

La contaminación con agroquímicos de sus principales fuentes de agua, ha causado además graves problemas de salud a las personas de las comunidades.  Me pregunto entonces, por qué en esta época electoral, donde el pueblo vuelve a ser protagonista por su poder de votación, nadie mira el campamento Qopiwini, nadie se acerca a garantizar una solución real a sus problemas y a levantar ese acampe en pleno corazón Bonarense con la promesa del respeto y la vida digna para estas comunidades originarias.   Habrá que esperar entonces, que en estas elecciones que se avecinan, algún candidato o candidata se piense una Argentina que incluya estos pueblos originarios, y que deje de victimizarlos con los proyectos agroindustriales que se negocian sobre sus territorios ancestrales.

**[[@[lauracapout]**
