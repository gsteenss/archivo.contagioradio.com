Title: En Colombia mueren 320 personas al año por cáncer asociado al asbesto
Date: 2017-11-29 12:53
Category: DDHH, Nacional
Tags: asbesto, colombia sin asbesto, Congreso de la República, eternit, green peace, ley ana cecilia niño
Slug: en-colombia-mueren-320-personas-al-ano-de-cancer-asociado-a-la-inhalacion-de-asbesto
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/11/asbesto.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Green Peace] 

###### [29 Nov 2017] 

En Colombia, cerca de 320 personas mueren al año de cáncer relacionado con la exposición a las partículas de asbesto según un estudio realizado por la organización internacional Green Peace. El asbesto es un mineral que se utiliza en diferentes sectores industriales, pero **principalmente en la construcción**. En el Congreso se podría hundir la ley "Ana Cecilia Niño", que busca prohibir este material en el país, si no se aprueba en su último debate.

Gonzalo Strano, vocero de Green Peace en Argentina, explicó que, luego de siete proyectos  que se han presentado en el Congreso de la República, sólo el octavo fue aprobado por la Comisión VII del Senado. Ahora, **a la ley le hace falta entrar en el último debate** para que sea aprobada en su totalidad. (Le puede interesar: ["El drama de la salud de las comunidades de la Guajira por la minería en el Cerrejón"](https://archivo.contagioradio.com/comunidades-de-la-guajira-exige/))

El vocero indicó que existen 6 tipos de asbesto y **en el país se utiliza el crisotilo** “que tiene ventajas para la industria porque es barato y tiene resistencia al fuego”. Sin embargo, el problema que presenta es que “es sumamente peligroso para la salud” en la medida que es un mineral cancerígeno. Afirma que con solo respirar una fibra de este material “es muy probable que la persona quede expuesta a un cáncer, que se desarrolla en 20 años”.

La peligrosidad del asbesto radica en que las pequeñas fibras, cuando son aspiradas, **se pegan a los pulmones** y se va creando un tumor cancerígeno que genera una enfermedad que se llama abestosis que “es reconocida por la Organización Mundial de la Salud y por esto ha sido prohibido en más de 57 países”.

### **A la Ley “Ana Cecilia Niño” le hace falta un debate en el Congreso** 

Ana Cecilia Niño fue una de las víctimas mortales del asbesto y durante su vida, se dedicó a luchar para que el país prohibiera el asbesto. Cuando murió, Daniel Pineda, su esposo, conformó el movimiento **"Colombia sin Asbesto"** del cual Green Peace es aliado. Allí, han desarrollado diferentes campañas para visibilizar el tema y “se logró presentar por octava vez ante el Congreso de Colombia, un proyecto de ley que prohíba esta sustancia en Colombia”. (Le puede interesar: ["Víctimas no estarían recibiendo óptima atención en salud mental: MSF"](https://archivo.contagioradio.com/victimas_salud_medico_sin_fronteras/))

Desde Green Peace han destacado el **apoyo y la movilización de la ciudadanía** en el país para lograr que el proyecto de ley fuera tenido en cuenta y se votara a favor. Ahora, la ciudadanía espera que se establezca la fecha para el debate que le resta a la ley "Ana Cecilia Niño" que busca preservar la vida y la salud de los colombianos.

### **Eternit exporta productos sin asbesto y en Colombia lo sigue utilizando** 

Strano manifestó que la lucha para que las empresas como Eternit dejen de utilizar el asbesto como material para la creación de implementos industriales, **ha sido muy difícil** en la medida que “es un sector que tiene muchísima fuerza”.

Diversas organizaciones han evidenciado que esta empresa **“ya tiene los sustitutos del asbesto** y los está utilizando en otros productos que decide exportarlos porque la mayor parte de los países del mundo no permiten el ingreso de productos con asbesto”. Sin embargo, para Colombia, se desarrollan productos con asbesto.

Esta dualidad **fue denunciada por Green Peace** en donde manifestaron que “no se va a acabar el trabajo de Eternit pero existen sustitutos que son menos perjudiciales y que pueden ser utilizados en la construcción”. Sin embargo, recalcaron que la industria Eternit ha mantenido un silencio absoluto y  hacen lobby para presionar a los congresistas para que no se aprueben las leyes.

Finalmente, Strano indicó que en Colombia **mueren 320 personas al año por estar expuestas al asbesto** y “no son solo trabajadores, mueren vecinos de las fábricas, hijos y familiares de gente que en algún momento trabajó para Eternit, por ejemplo”. Por esto, se ha hecho énfasis en que la abestosis no es una enfermedad laboral, sino que representa un peligro para toda la sociedad.

<iframe id="audio_22358367" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_22358367_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]

<div class="osd-sms-wrapper">

</div>
