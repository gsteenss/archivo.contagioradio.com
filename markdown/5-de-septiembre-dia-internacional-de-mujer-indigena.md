Title: 5 de septiembre: Día Internacional de Mujer Indígena
Date: 2015-09-05 13:05
Category: El mundo, infografia, Mujer
Tags: 5 de septiembre, América Latina y del caribe, Día internacional de la mujer indígena
Slug: 5-de-septiembre-dia-internacional-de-mujer-indigena
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/wayuu-4.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: [noticias.ipesderechoshumanos.org]

Desde 1983 el 5 de septiembre se conmemora el Día Internacional de la Mujer Indígena, en honor a la indígena peruana Bartolina Sisa, guerrera Aymara,  quien luchó contra la dominación de conquistadores españoles, debido a que durante la época  se originó el maltrato y despojo a la mujer indígena.

Bartolina nació el 24 de agosto de 1753, en la comunidad de Sullkawi del Ayllu, durante su infancia recorrió distintos pueblos junto a sus padres por el comercio de la hoja de coca. Ya desde sus primeros años, pudo vivir en carne propia los atropellos que se cometían con las poblaciones indígenas.

Cuando estalla la insurgencia aymara-quechua, Bartolina asume un papel de liderazgo: fue jefa de batallones indígenas., demostró gran capacidad de organización y logró armar un batallón de guerrilleros indígenas. A su vez, armó grupos de mujeres que colaboran con la resistencia a los españoles en diferentes pueblos del alto Perú.

Finalmente fue asesinada y descuartizada el 5 de septiembre de 1782.

[![1439236524](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/1439236524.jpg){.aligncenter .size-full .wp-image-13437 width="1896" height="2451"}](https://archivo.contagioradio.com/5-de-septiembre-dia-internacional-de-mujer-indigena/attachment/1439236524/)

<iframe id="audio_20865363" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_20865363_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>
