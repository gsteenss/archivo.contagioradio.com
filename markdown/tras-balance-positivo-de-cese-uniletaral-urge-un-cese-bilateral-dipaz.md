Title: Tras balance positivo de cese uniletaral urge un cese bilateral: DiPaz
Date: 2015-10-22 13:22
Category: Nacional, Paz
Tags: cerac, cese unilateral de las FARC, David Correal, diálogos en la Habana, DIPAZ, ELN, FARC-EP, Informe desescalamiento del conflicto, Jenny Neme, proceso de paz
Slug: tras-balance-positivo-de-cese-uniletaral-urge-un-cese-bilateral-dipaz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/10/Pimiciadiario.com_.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: primiciadiario 

<iframe src="http://www.ivoox.com/player_ek_9131232_2_1.html?data=mpagk5eXdo6ZmKiak5aJd6Kml5KSmaiRdo6ZmKiakpKJe6ShkZKSmaiRmNPV1JDPw9HFssTZjNXc1c7YrdfjjMnSjcjJt8af1tPWzsrYpdPVzZDi1MzJb9bijMjS1cqPpo6ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Jenny Neme, DiPaz] 

###### [22 oct 2015] 

Una **reducción drástica en las acciones armadas de los últimos 40 años**, así como una **reducción del 82% de las muertes por conflicto armado** son algunas cifras que determina el informe del Centro de Recursos para el Análisis del Conflicto, CERAC, que junto con el de el Diálogo Intereclesial por la Paz, DiPaz, hacen balance de los cuatro meses después de medidas de desescalamiento del conflicto.

David Correal, investigador del CERAC afirma que hay una drástica reducción de las acciones violentas, cerca del 50% y afirma que “esta reducción también se observa en el promedio mensual de víctimas que resultaron heridas, en el periodo actual el promedio de víctimas es de 25, mientras que el promedio mensual desde 1975 es de 74, lo que representa una reducción del 66%”.

Jenny Neme, Vocera de DiPaz indica que en este informe se **evidencia “un completo cumplimiento de cese unilateral por parte de las FARC”**, y la disminución de las acciones de la fuerza pública pese a que se han presentado dos casos de ametrallamiento y otros dos hechos de bombardeo por parte de la fuerza pública, que ponen en riesgo este cese unilateral al fuego, sin embargo, David Correal del CERAC, indica que “el 24 de septiembre hubo una violación al cese al fuego por parte de la guerrilla” en Corinto, Cauca, contra una comisión militar que adelantaba labores antinarcóticos.

[![Acciones ofensivas](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/10/Acciones-ofensivas.jpg){.aligncenter .wp-image-16116 width="728" height="332"}](https://archivo.contagioradio.com/tras-balance-positivo-de-cese-uniletaral-urge-un-cese-bilateral-dipaz/acciones-ofensivas/)

Aunque el CERAC mencionó la denuncia de las FARC frente a los casos de bombardeos y ametrallamientos contra campamentos por parte de las FFMM, DiPaz si logró establecer la ocurrencia de esos hechos dado que tienen contacto permanente con comunidades rurales y están trabajando de manera coordinada con el Frente Amplio por la Paz y el movimiento de Constituyentes por la Paz que también tienen fuentes directas en las regiones.

Los estudios también **visibilizan las acciones del Ejército de Liberación Nacional** (ELN), presentando que este grupo “desescaló el nivel de acciones violentas en los últimos tres meses” y las acciones armadas han descendido a 9, comparadas con 1984, han tenido un **descenso del 64%,** aunque con un nivel de acciones “similar a los promedios mensuales de acciones violentas desde el inicio del proceso de paz entre las FARC y el Gobierno Nacional”.

En ambos informes **alarma el accionar violento que han tenido los grupos paramilitares en estos meses** que se han dirigido, según David Correal, “a la violencia de tipo político, mediante amenazas a candidatos, integrantes de partidos políticos o funcionarios públicos”.

Neme, de DiPaz indica que hay **casos de presencia paramilitar en** “el departamento del Choco, Meta, Norte de Santander, Bajo Cauca Antioqueño, Tumaco, y existe una presencia menos visible en el resto del territorio”, además con “acciones dramáticas en el Bajo Atrato Chocoano con **el desplazamiento de 150 familias en la cuenca del rio Truando**” por presión de paramilitares de las Autodefensas Gaitanistas de Colombia.

Frente a un posible cese bilateral David Correal afirma que el cese unilateral “**ha traído puntos muy positivos y eso ha dinamizado la negociación en la Habana**” y que estamos ad portas de un cese bilateral “para seguir reduciendo las acciones armadas”.

Por su parte Jenny Neme, con una “**preocupación por la afectación a la población civil**” por los hechos de violencia que se presentan en el país a raíz de que no existe un cese bilateral establecido, indica que este “es el momento propicio” y “urge que se tomen medidas frente al cese bilateral”, ya que “l**a gente se encuentra cansada**, no queremos más confrontación”.
