Title: Las propuestas de CAVIDA a 21 años de la operación Génesis
Date: 2018-03-17 12:31
Category: Comunidad, Nacional
Tags: cacarica, Chocó, comunidades, Génesis
Slug: choco-genesis-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/03/DSC06937-e1521301398818.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### Foto: Yannis Filppou 

###### 17 Mar 2018 

Tras la conmemoración de los 21 años de las operaciones Génesis y Cacarica que tuvieron lugar en el bajo Atrato chocoano en 1997, familias y organizaciones comunitarias de la zona, asociadas en CAVIDA escribieron una carta en la que hacen una invitación a diferentes instancias nacionales a trabajar por  la verdad, la memoria, la reconciliación y la paz.

En primer instancia hacen un llamado a la Comisión para el esclarecimiento de la verdad para que sesione en su territorio, donde manifiestan no existe el odio hacia los responsables de su desplazamiento y hacen un llamado para que estos últimos reconozcan y procesen lo ocurrido desde su humanidad, asegurando que se encuentran en "una actitud de reconciliación real y efectiva".

También invitan a los procesos comunitarios, al Estado colombiano, a la Comisión de la Verdad, a la Jurisdiscción Especial de Paz y a la comunidad internacional a respaldar su propuesta de memoria expresada en la Universidad para la Paz sede “Marino López Mena” que permita "posibilitar actos de reconocimiento de responsabilidad y para las sanciones restauradoras".

Las organizaciones firmantes exhortan a los victimarios del pasado y los actuales a entablar un diálogo directo con la comunidades "sin temores y prevenciones" en la procura de construir verdad, y los invitan a ir a la JEP, donde aseguran presentarán informes "en busca de formas efectivas reparación y restauración de los daños ocasionados basados en la verdad".

De manera reiterada hacen varios llamados al Gobierno nacional, primero a reiniciar junto con el ELN el V ciclo de diálogos de paz, en busca de solucionar el conflicto armado y social y sean incluídas sus propuestas. Adicionalmente instan a que cumpla con lo pactado en el acuerdo del Teatro Colón en relación con el desmonte del paramilitarismo, o "o cree un marco político y legal para su desmantelamiento dada su disposición a una entrega colectiva".

Exigen además que hacer efectiva la implementación del acuerdo de paz con las FARC, representa "primer paso para la construcción de una nueva historia", para empezar a hacer los PDETs, una realidad y a respaldar el reconocimiento a los monumentos y casas de memoria de Bahía Cupica, Coliseo de Turbo y Zona Humanitaria Nueva Vida y todas las del bajo Atrato y respaldar su plan agroambiental, la propuesta de autoridades ambientales, y el turismo ambiental.

A continuación, les compartimos el texto completo.

**HUMANOS DEL MUNDO:**

A  21 años  de la operación Génesis y Cacarica

Vamos con alegría por el pacto por la verdad y un bello existir para todos,

(EcoAldea de Paz Nueva Esperanza, 7 de marzo de 2018)

A 21 años de las dos operaciones armadas que marcaron nuestra vida en el territorio de Cacarica; mujeres y hombres, niñas y niños, y jóvenes de diferentes procesos organizativos de Bajo Atrato en presencia de delegaciones internacionales de España, Alemania y Francia, celebramos la memoria en nuestro Festival.

Pasamos en la comida, en la pintura, en la música, en el teatro y en el deporte, fortaleciendo nuestras apuestas por un bello existir entre ellas por ser parte activa en el impulso para una Comisión de la Verdad (CEV) y la apuesta por el Derecho Restaurador en Jurisdicción Especial para la Paz (JEP), propuestas de las que nos sentimos co creadores, desde el año 2000

Navegamos en la memoria dicha en un Monumento que se esperamos sea de la Nación, hecho con nuestras manos en el coliseo de Turbo, donde estuvimos desplazados hasta el 2001.

Al pasar por Bocas del Atrato recordamos la vivencia de 161 personas durante 3 años, y saboreamos el pescado de la vida. Sobre el imponente Río Atrato encontramos el caserío de Tumaradó donde se estableció el control paramilitar que aún persiste sobre nuestras comunidades con los neoparamilitares.

Luego al caserío de Travesía nos recordó también el desplazamiento, asesinato, quema de casas, y abusos sexuales. Minutos después pasamos por  La Loma fue el sitio de ingreso de las estructuras paramilitares y del ejército colombiano donde asesinaron y desparecieron a hermanos de nuestras comunidades.

Finalmente llegamos a nuestra apuesta de la Eco aldea de Paz Nueva Esperanza en Dios luego de una caminada que en la noche volvimos a retomar con la luna roja, absolutamente llena de vida.

Durante dos días hemos pasado por el bello Atrato, por sus límites y horizontes sin límites, en los que hemos asumiendo el dolor en vida, la angustia en vida, la impunidad en vida, desde la que estamos buscando en libertad el restablecimiento de nuevas relaciones entre nosotros y los responsables de los hechos que nos han causado daño, sean ellos, militares, ex militares y exparamilitares, empresarios, exguerrilleros.

Nuestra apuesta, asume las diferencias de la violencia, las motivaciones de los que defienden un orden y los que disienten de él y las que hoy pueden debatirse para crear otra democracia.

Luego de las operaciones  militares y paramilitares simultáneas desde el 24 de febrero de 1997, hemos experimentado dos nuevos desplazamientos forzados internamente en junio de 2001, y en septiembre de 2003 con la mal llamada “seguridad democrática” por parte del Estado colombiano. Y, entre la impunidad, y lo doloroso, no odiamos.

Ni nuestros más de 80 asesinados y desaparecidos, y las huellas imborrables de los torturados nos llevan a odiar. Nuestra memoria es de una lealtad a la vida, no en el sufrimiento, sí de la restauración de nuestras vidas. Esperamos de los perpetradores cuando se exprese la verdad se sumen al torrente de la vida.

Hemos permanecido en el arraigo creador a nuestra madre tierra, en la patria Cacarica, en la Patria Colombia afirmando esperanza, paz, en medio de la guerra con fuego y en medio de la guerra económica, afirmando nuestros derechos a la educación, a la defensa del ambiente, a la verdad, justicia, reparación integral y garantías de no repetición.

Desde 1999 realizamos varios encuentros internacionales creándose espacios como la Red de Alternativas contra la Globalización del mercado y la impunidad, allí surgió la propuesta de la Comisión Ética para la Verdad en Colombia.  Posteriormente nació la red de Comunidades Construyendo Paz en los Territorios (CONPAZ), de la que hacemos parte.

Desde esta menoria estamos dispuestas y dispuestos a aportar desde nuestras vivencias los elementos necesarios para la comprensión y aplicación de la justicia restaurativa, de tal manera que esta permita construir el camino de una verdadera reconciliación y de la creación de un nuevo país.

Expresamos alegría por la disposición de las y los jóvenes para transitar por este nuevo momento a partir sus sentimientos, pensamientos y almas. Entendemos que los sentimientos de rabia, odio y dolor son posibilidad al transformarse para aportar a la construcción de una verdadera paz con justicia socio ambiental, estable y duradera.

Con las comunidades de bajo Atrato concluimos:

\*Invitar a la Comisión de Esclarecimiento de la Verdad para que sesione en nuestro territorio, donde no tenemos sentimientos de odio hacia los responsables. Deseamos que también como seres humanos que son, ellos puedan reconocer y procesar lo ocurrido, que también tengan el derecho al descanso de su conciencia por lo hecho. Reiteramos de todo corazón y con nuestra más profunda tranquilidad y sinceridad  que estamos en actitud de reconciliación real y efectiva haciendo realidad la verdad,

\*Invitamos a todas los procesos comunitarios, al Estado colombiano, a la CEV y a la Jurisdiscción Especial de Paz, a la comunidad internacional  a respaldar nuestra propuesta de memoria restauradora expresada en la Universidad para la Paz sede “Marino López Mena”, en la que hemos instalado una primera base,  para posibilitar actos de reconocimiento de responsabilidad y para las sanciones restauradoras.

\*Invitamos a quiénes nos desplazaron, a los que nos asesinaron, nos desaparecieron, nos bombardearon, nos explotaron, y a los que siguen usando el territorio con ilegalidad, a través de la violencia imponiendo sus negocios, a reconocernos como seres humanos, y les llamamos a un diálogo directo con nuestras comunidades, sin temores, ni prevenciones en la construcción de la verdad en la interconexión de todas las verdades.  Les invitamos a ir a la JEP, allí presentaremos informes buscando formas efectiva para reparación y restauración de los daños ocasionados basados en la verdad porque queremos encontrarnos con ustedes en la verdad no en los barrotes que nos aislan.

\*Invitamos al gobierno  colombiano y al ELN para reiniciar lo más pronto posible el V Ciclo de Conversaciones para que realice una apertura y real compromiso para dar solución integral al conflicto político armado y social, y seamos incluidos con nuestros elementos en el Acuerdo Humanitario Ya.

\*Apelamos al gobierno y al Estado colombiano  para que conforme al Acuerdo del Teatro Colón, desmonte los grupos herederos del paramilitarismo, o cree un marco político y legal para su desmantelamiento dada su disposición a una entrega colectiva, como escuchamos en la visita del Papa

\*Exigimos al gobierno nacional que se haga efectiva la implementación del acuerdo de paz firmado con las FARC, como primer paso para la construcción de una nueva historia, y dentro de los Acuerdos el que empecemos a hacer de los PDETs, una realidad.

 \*Invitamos a respaldar el reconocimiento de Monumento de la Nación los monumentos y casas de memoria de Bahía Cupica, Coliseo de Turbo y Zona Humanitaria Nueva Vida y todas las del bajo Atrato

\*Invitamos a respaldar nuestro plan agroambiental, la propuesta de autoridades ambientales, y el turismo ambiental

Firmamos

Familias desplazadas del río Truandó, Consejo comunitario Limón Quiparadó

Familias desplazadas río Salaquí

Familias del Consejo Comunitario de Curvaradó que habitamos en Zona Humanitarias y Zonas de Biodiversidad

Consejo Comunitario del Jiguamiandó.

Zona Humanitaria y Ambiental So Bia Drua, Jiguamiandó (CAMERUJ)

Familias de comunidad de Bijao Onofre, territorio de Pedeguita y Mancilla

Zona de Biodiversidad La Madre Unión, Territorio Colectivo de Pedeguita y Mancilla

Asociación de Familias Autodeterminación, Vida, Dignidad del Consejo Comunitario de Cacarica, CAVIDA
