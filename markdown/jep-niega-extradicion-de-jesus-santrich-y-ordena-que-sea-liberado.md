Title: JEP niega extradición de Jesús Santrich y ordena que sea liberado
Date: 2019-05-15 10:53
Author: CtgAdm
Category: Judicial, Paz
Tags: JEP, Jesús Santrich, Seuxis Paucias Hernández
Slug: jep-niega-extradicion-de-jesus-santrich-y-ordena-que-sea-liberado
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/01/Jesus-Santrich-6-770x400.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto:Contagio Radio 

La Jurisdicción Especial de Paz aplica la garantía de no extradición a Seuxis Paucias Hernández "Jesús Santrich, asegurando que las pruebas no permiten evaluar la conducta ni establecer la fecha precisa de su realización. En tal consideración ordena a la Fiscalía disponer libertad inmediata para el integrante del partido FARC.

Comunicado de la JEP:

COMUNICADO 073

**LA SECCIÓN DE REVISIÓN DE LA JEP APLICA LA GARANTÍA DE NO EXTRADICIÓN A SEUXIS PAUCIAS HERNÁNDEZ SOLARTE**

-Ordena a la Fiscalía General de la Nación disponer la libertad inmediata de Hernández Solarte

-La JEP considera que con las pruebas aportadas no se puede evaluar la conducta atribuida a Hernández Solarte ni tampoco determinar la fecha precisa de su realización.  
-Considera que el juzgamiento de Hernández en Colombia es la manera más efectiva de respetar el derecho internacional público y posibilitar el goce de los derechos de las víctimas a la verdad, la justicia, la reparación y la no repetición.

Bogotá, 15 de mayo de 2019. La Sección de Revisión del Tribunal para la Paz, mediante decisión mayoritaria aprobada hoy, resolvió aplicar la garantía de no extradición a favor de Seuxis Paucias Hernández Solarte.

La aplicación de la garantía de no extradición se fundamentó en que la Sección no pudo evaluar la conducta para determinar la fecha precisa de su realización, dado que el Departamento de Justicia de los Estados Unidos de América no remitió las evidencias solicitadas y a que en las interceptaciones telefónicas de otro caso enviadas a la JEP por la Fiscalía no se reveló la conducta atribuida a Hernández Solarte en la solicitud de extradición.

Para la Sección de Revisión, el juzgamiento del compareciente en Colombia es la manera más efectiva de garantizar la consolidación de una paz estable y duradera, y el goce de los derechos del solicitante de la garantía de no extradición, en cuanto a su seguridad jurídica, y también los derechos de las víctimas a la Verdad, la Justicia, la Reparación y la No Repetición.  
Asimismo, la Sección de Revisión de la JEP le ordenó al Fiscal General de la Nación disponer la libertad inmediata de Hernández Solarte.

También, la Sección de Revisión determinó que Hernández Solarte siga a disposición de la JEP conforme al régimen de condicionalidad que les exige a todos los comparecientes, entre otras cosas, aportar a la verdad plena, la no repetición, la no reincidencia y la reparación a las víctimas.

La Sección señaló que una vez esté ejecutoriada la decisión adoptada hoy se remitirá el expediente de Hernández Solarte a la Sala especial de instrucción de la Corte Suprema de Justicia y a la Sala de Reconocimiento de Verdad, Responsabilidad y Determinación de Hechos y Conductas de la JEP, para lo de sus respectivas competencias.  
Además, la Sección decidió la compulsa de copias disciplinarias ante la Sala Disciplinaria del Consejo Superior o Seccional de la Judicatura y la Oficina de Control Interno de la Fiscalía General de la Nación, según sea el caso, a raíz de las irregularidades que se presentaron en el trámite de solicitud de asistencia judicial para recaudar pruebas en Colombia.  
En la decisión de hoy se consideró que la garantía de no extradición consignada en el artículo transitorio 19 de la Constitución Política generó un cambio sustancial, que varió la tradición jurídica en materia de extradición, al asignar a la Sección de Revisión competencias y funciones distintas a las ya radicadas en cabeza de la Sala de Casación Penal de la Corte Suprema de Justicia, en el siguiente sentido:

i\. Para el ejercicio de la función encomendada a la Sección de Revisión, el artículo 19 transitorio de la Constitución Política le exige evaluar la conducta objeto de requerimiento de extradición en los casos en los que se alegue que la misma ocurrió después de la firma del Acuerdo Final de Paz, para determinar la fecha precisa de su realización. Para esto es indispensable conocer las pruebas que soportan el indictment y la solicitud de extradición, así como aquellas que la Fiscalía General de la Nación adujo se encontraban relacionadas con la citada conducta y que había ofrecido a las autoridades norteamericanas.

ii\. El ejercicio de valoración de la conducta realizado por la Sección de Revisión, en tanto define la aplicación de una garantía constitucional, implica pronunciarse a través de una providencia judicial.  
En el caso de Hernández, respecto a la declaración jurada del agente de la DEA Brian Witek se constataron serias irregularidades, por cuanto la Fiscalía General no aportó la solicitud de asistencia judicial que debía tramitar la autoridad extranjera y al responderle a la JEP sobre ese requerimiento, justificó la ausencia de la asistencia judicial y del control judicial de las actuaciones de los testigos cooperantes, argumentando que estos intervinieron como particulares.

Por ese motivo, la Sección de Revisión advirtió que las autoridades norteamericanas pudieron violar las normas de cooperación internacional y asistencia judicial en el recaudo de pruebas en Colombia. Lo anterior dio lugar a que se compulsaran copias en contra de los servidores de la Fiscalía General de la Nación que eventualmente omitieron sus deberes de velar por el respeto de los derechos y garantías fundamentales de los ciudadanos y afectación de la soberanía nacional.

Adicionalmente, dado que el Departamento de Justicia de los Estados Unidos de América no envió las evidencias solicitadas y que en las interceptaciones telefónicas referidas a otra investigación remitidas por la Fiscalía no se reveló la conducta atribuida a Hernández Solarte en la solicitud de extradición, la Sección concluyó que no podía evaluarla y, por lo mismo, tampoco le fue posible determinar la fecha precisa de su ejecución.

Se destaca que los videos inaudibles que circularon por redes sociales y que la Sección le pidió a la Fiscalía General de la Nación y a las autoridades de Estados Unidos no fueron entregados a la Jurisdicción y no obran en el expediente.Ante ese escenario de indeterminación y, apoyándose en los principios pro homine, pro paz y pro víctima, y entendiendo que los ex integrantes de las Farc-EP ostentan la garantía de no extradición hasta tanto se acredite que no se cumple el factor temporal, como lo ha entendido la Sala Penal de la Corte Suprema de Justicia (radicado 53719 de 31 de octubre de 2018), la Sección ordenó aplicar la aludida garantía. Como consecuencia de ello, ordenó al Fiscal General de la Nación que disponga la libertad inmediata de Seuxis Paucias Hernández Solarte.

La Sección de Revisión aclaró que la solicitud de pruebas tendiente a evaluar la conducta tiene como propósito determinar la fecha precisa de su realización, motivo por el cual no le corresponde efectuar juicios de responsabilidad penal, ni sobre la existencia o no de aquella. En consecuencia, en el caso en concreto nunca se concluyó que la conducta no existió, sino que, por falta de pruebas, no pudo evaluarla.

La Sección resaltó que la aplicación de la garantía no implica que la conducta se quede sin investigar y, en consecuencia, de conformidad con el principio aut dedere aut judicare (extraditar o juzgar), señaló que compete a las autoridades judiciales nacionales (transicionales u ordinarias, según corresponda) definir si tienen competencia para investigar los hechos sobre la base de elementos de prueba obtenidos legalmente. Por tanto, resolvió, a la ejecutoria de esta providencia, remitir copia del presente proceso a la Sala Especial de Instrucción de la Corte Suprema de Justicia y a la Sala de Reconocimiento de Verdad, de Responsabilidad y de Determinación de los Hechos y Conductas.

La Sección no estimó necesario enviar copia del expediente a la Fiscalía General de la Nación, pues esa entidad ya cuenta con los elementos de convicción obrantes en esta actuación.  
Salvamentos de voto.

Frente a la decisión tomada hoy hubo dos salvamentos de voto, de las magistradas Claudia López Díaz y Gloria Amparo Rodríguez, quienes expusieron razones diferentes para apartarse del Auto SRT- AE-0XX.

La magistrada López Díaz considera que “honrando la interpretación fidedigna del Acuerdo Final y sus propósitos, la Sección debió negar el beneficio de la garantía de no extradición (...) y remitir el expediente a la Sala de Casación Penal de la Corte Suprema de Justicia, para continuar el trámite de extradición, como lo pidió el Ministerio Público”.

Según la magistrada López, la solicitud de extradición de los Estados Unidos de América y sus anexos contenían la prueba suficiente para establecer la fecha precisa de las conductas atribuidas. Además, consideró que “se concedió la garantía de no extradición por un delito común, de narcotráfico, de carácter permanente, sin establecer su conexidad con el delito político”.

Por su parte, la magistrada Rodríguez consideró que la Sección debió haber solicitado otras pruebas “que hubieren permitido establecer la fecha precisa de la comisión de la conducta”. También consideró la magistrada Rodríguez que la Sección no tiene competencia para decidir sobre la libertad de Hernández Solarte y que se limitó a remitir el caso a la Corte Suprema de Justicia, cuando lo que procedía era ordenarle que determinara si procede una investigación en contra de Hernández Solarte por las conductas señaladas en el idictment, con el objetivo de asegurar el cumplimiento de obligaciones internacionales y salvaguardar el Acuerdo de Paz.

 

> ATENCIÓN. [@JEP\_Colombia](https://twitter.com/JEP_Colombia?ref_src=twsrc%5Etfw) aplica la garantía de no extradición a Seuxis Paucias Hernández, porque las pruebas no permiten evaluar la conducta ni establecer la fecha precisa de su realización. Se ordena a [@FiscaliaCol](https://twitter.com/FiscaliaCol?ref_src=twsrc%5Etfw) disponer libertad inmediata.
>
> — Jurisdicción Especial para la Paz (@JEP\_Colombia) [15 de mayo de 2019](https://twitter.com/JEP_Colombia/status/1128687126344884224?ref_src=twsrc%5Etfw)

<p>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
</p>
En desarrollo...
