Title: Paramilitares de las Águilas Negras amenazan a 123 defensores de DDHH en Colombia
Date: 2015-10-02 16:23
Category: DDHH, Nacional
Tags: Aguilas Negras, Amenazas en Colombia, Conversaciones de paz de la habana, Paramilitarismo
Slug: paramilitares-de-las-aguilas-negras-amenazan-a-123-defensores-de-ddhh-en-colombia
Status: published

###### Foto: internet 

###### [2 Oct 2015]

A través de un nuevo panfleto el grupo paramilitar Águilas Negras ha emitido una nueva amenaza en contra de defensores y defensoras de Derechos Humanos en Colombia. Según el grupo se realizaría un “plan limpieza”. En el panfleto se nombran algunas organizaciones de reconocida trayectoria como el Colectivo de Abogados José Alvear Restrepo, la Corporación Yira Castro y Codhes. También mencionan medios de información empresariales y a Canal Capital, canal público de la ciudad de Bogotá.

Esta sería la[segunda amenaza masiva contra defensores de DDHH](https://archivo.contagioradio.com/?s=aguilas+negras) pero una de las más de 6 amenazas de las Águilas Negras en contra de integrantes del movimiento social. Durante el pasado mes de Mayo este mismo grupo paramilitar amenazó a colectivos de animalistas, estudiantes de la Universidad Nacional, de la Universidad Colegio Mayor de Cundinamarca y varios defensores de DDHH así como a diferentes organizaciones.

En reiteradas ocasiones varios líderes han denunciado que este grupo paramilitar tiene relación directa con integrantes de las FFMM con acceso a información de inteligencia militar y se ha reiterado la exigencia de investigar y desmontar este tipo de estructuras que generan un ambiente de zozobra en la sociedad en general y enrarecen el ambiente de cara a un acuerdo de paz entre el gobierno y las guerrillas.

[![aguilas\_negras\_contagioradio](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/10/IMG-20151002-WA0004.jpg){.aligncenter .wp-image-15190 width="575" height="1022"}](https://archivo.contagioradio.com/paramilitares-de-las-aguilas-negras-amenazan-a-123-defensores-de-ddhh-en-colombia/img-20151002-wa0004/)
