Title: Universidad del Valle entra en paro indefinido contra cierre del HUV
Date: 2015-09-23 12:56
Category: Educación, Nacional
Tags: Crisis Hospital Universitario del Valle, Enfermería, estudiantes de salud, Fonoaudiología, Hospital Universitario del Valle, HUV, Medicina
Slug: universidad-del-valle-entra-en-paro-indefinido-contra-cierre-del-huv
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/SAlud-HUV.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: pazificonoticias.com 

###### [23 sep 2015]

Como medida de protesta por la [ crisis presupuestal del Hospital Universitario del Valle](https://archivo.contagioradio.com/epss-deben-al-hospital-universitario-del-valle-192-mil-millones-de-pesos/) (HUV) los estudiantes y directivos de la **facultad de salud de la Universidad del Valle** continúan en cese de actividades indefinidas y realizan jornadas informativas de la crisis a la comunidad.

Frente a la grave situación del hospital aún no se han dado soluciones por parte de las EPS's o el gobierno, por lo cual los estudiantes ven amenazado su futuro, debido a que allí es donde realizan sus **prácticas profesionales**.

Los estudiantes han adelantado **acciones para visibilizar** la problemática de salud que afronta el hospital y el Valle, para esto han decidido subirse al sistema masivo de Occidente (MIO) **informando** a las personas sobre el tema.

De esta forma los estudiantes argumentan a la gente que si se cierra el HUV estás no se abrirían más, al igual que sucedió con el **hospital San Juan de Dios en Bogotá**, que estuvo cerrado durante 14 años.

Los estudiantes se encuentran en cese de actividades desde el pasado 16 de septiembre, exigiéndole al gobierno que obligue a las Empresas promotoras de salud (EPS) para que se **cancelen los 192 mil millones de pesos** que estas adeudan.

### **Ver también:** 

[Funcionarios del HUV continúan en movilizaciones](https://archivo.contagioradio.com/funcionarios-del-hospital-universitario-del-valle-continuan-en-movilizaciones/)

[Los estudiantes se suman a la defensa del HUV](https://archivo.contagioradio.com/estudiantes-se-suman-a-la-defensa-del-hospital-universitario-del-valle/)
