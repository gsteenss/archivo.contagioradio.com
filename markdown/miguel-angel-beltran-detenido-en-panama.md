Title: Profesor Miguel Angel Beltran ya está en Bogotá luego de detención en Panamá
Date: 2016-11-13 20:41
Category: DDHH, Nacional
Tags: miguel angel beltran
Slug: miguel-angel-beltran-detenido-en-panama
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/09/miguel-angel-beltran.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: contagioradio] 

###### [13 Nov 2016] 

Luego de varias horas de detención en Panamá el profesor Miguel Angel Beltrán recobró su libertad tras ser deportado del vecino país. Según el relato a través de un video, esta nueva detención sigue siendo parte de la persecución en su contra luego de un proceso penal del que resultó absuelto.

Desde hace más de 20 horas, el profesor Miguel Angel Beltrán se encuentra detenido en Panamá sin que hasta el momento se conocierna las razones de su detención por parte de las autoridades de ese país**.**

El sociólogo se disponía a viajar el viernes 11 de noviembre a la República de Ghana desde la ciudad de Bogotá, con el fin de presentar una ponencia, siendo interrumpido su viaje por autoridades de Migración Colombia en el Aeropuerto el Dorado, quienes lo detuvieron generandole pérdida de su vuelo. **Después que se dio a conocer su absolución pudo recobrar la libertad.**

**El 12 de noviembre el Profesor Miguel Ángel Beltrán** viajó a Panamá donde impidieron su ingreso,después que las autoridades migratorias de dicho país fundamentaran la restricción en normatividades que prohíben el paso a personas que **hayan estado privadas de la libertad por condenas penales.**

La noche del sábado el Profesor logró comunicarse con su familia confirmando el impedimento de ingreso y la detención en dicho país.  Aunque las [acuasaciones en su contra cesaron recientemente luego de la absolución por parte del Consejo de Estado](https://archivo.contagioradio.com/?s=miguel+angel+beltran), al parecer las autoridades internacionales, en este caso la Interpol no han recibido información por parte de las autoridades colombianas para que se permita la libre circulación del académico.

Este es el video de Miguel Angel Beltrán tras su llegada al aeropuerto El Dorado de Bogotá

\[KGVID\]https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/video-Miguel-Angel-Beltran.mp4\[/KGVID\]
