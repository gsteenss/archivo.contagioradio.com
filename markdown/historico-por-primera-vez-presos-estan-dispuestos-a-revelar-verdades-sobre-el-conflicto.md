Title: Histórico: Por primera vez presos están dispuestos a revelar verdades sobre el conflicto
Date: 2019-03-27 10:26
Category: DDHH, Paz
Tags: comision de la verdad, Comité de Presos, reparación y no repetición, Sistema Integral de Justicia, verdad
Slug: historico-por-primera-vez-presos-estan-dispuestos-a-revelar-verdades-sobre-el-conflicto
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/03/la-carta-por-la-verdad-desde-las-carceles.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Contagio Radio 

###### 27 Mar 2019 

A través de una misiva, el Comité de Presos por la Verdad de Colombia y  200 prisioneros de  La Picota dirigida al Sistema Integral de Justicia, Verdad, Reparación y No Repetición y con copia a la Corte Penal Internacional pusieron a su disposición información sobre 24 tipos de delitos, cometidos en 45 territorios por integrantes de 24 organizaciones.

**La verdad que manifiestan, están dispuestos a revelar incluyen desplazamientos de poblaciones afro por cultivos de palma, operativos militares como Orión e incluso el magnicidio de Álvaro Gómez Hurtado**, reiterando su compromiso con el esclarecimiento del conflicto interno en Colombia. [(Lea también: Actores del Conflicto listos para decir la verdad desde las cárceles)](https://archivo.contagioradio.com/la-verdad-esta-lista-para-salir-de-la-carcel/)

Incluirían** nombres propios, fechas y actividades relativas a homicidios, desaparición forzosa, magnicidios, desplazamiento, torturas, masacres, fosas comunes, casos de  falsos positivos y relaciones entre diversos grupos criminales con la Fuerza Pública  y políticos**.

Solicitando una reunión con el presidente de la Comisión de la Verdad, el padre Francisco de Roux, el Comité de presos expresa que dentro del tipo de información que están dispuesto a revelar, afirman poder dar a conocer información relacionada con diversos actores del conflicto que incluye **paramilitares, autodefensas, guerrilla (FARC-EP, EPL), Bacrim y grupos de narcotráfico desde el año 1986 hasta el año 2016.**

De igual forma indican que podrán esclarecer información sobre el conflicto en los departamentos de: Atlántico, Magdalena, Cesar, Sucre, Guajira, Córdoba, Bolívar, Antioquia, Chocó, Santander, Norte de Santander, Boyacá, Cundinamarca, Meta, Casanare, Caquetá, Vichada, Valle del Cauca, Nariño, Tolima, Huila y las regiones de Santa Marta Golfo de Morrosquillo, Urabá, Norte de Urabá, Sur de Bolívar, Barrancabermeja, Tame, Arauca, Sierra Nevada, Cartagena, Medellín, Bogotá.

Para el abogado Álvaro Leyva, el paso que ha dado el Comité va abriendo las puertas a todos quienes participaron en el conflicto y su deseo de participación, **"la verdad es el éxito de un proceso que muchos rechazan, esto es algo que no lo va a atajar nadie"** afirma destacándolo como un hecho sin precedentes.

Frente a toda la información que esta a punto de ser revelada Leyva concluye que "aquí la guerra fue silenciosa" al referirse a toda la personas que en las grandes ciudades desconocieron todos los hechos que sucedieron durante el conflicto y que incluso hoy al escuchar sobre lo acontencido permanecen incrédulas al respecto.

Asímismo, los firmantes afirman que también han establecido contactos con personas en la clandestinidad, presos en el extranjeros, detenidos y personas en libertad que han expresado su deseo de aportar a la verdad y que "aspiran a no llevarse a la tumba secretos importantes para la verdad del conflicto, de las víctimas y del país" agregan.

<iframe id="audio_33775042" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_33775042_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
