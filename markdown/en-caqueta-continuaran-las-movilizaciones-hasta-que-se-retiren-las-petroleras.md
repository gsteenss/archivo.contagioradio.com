Title: En Caquetá continuarán las movilizaciones hasta que se retiren las petroleras
Date: 2016-09-05 19:28
Category: DDHH, Nacional
Tags: Caquetá represión, ESMAD, Petroleras
Slug: en-caqueta-continuaran-las-movilizaciones-hasta-que-se-retiren-las-petroleras
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/09/esmad.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Vanguardia 

###### [5 Sept 2016] 

Con la presencia de delegados de las comunidades y autoridades regionales, se anunció que las movilizaciones continuarán hasta que las exploraciones petroleras cesen en la región. **Así mismo se definió la instalación de una mesa regional que solamente comenzaría a sesionar una vez se suspendan las actividades** empresariales que se desarrollan en Caquetá.

En una audiencia que se llevó a cabo en el Paujil, Caquetá  las comunidades campesinas le manifestaron a alcaldes, gobernadores y al presidente de Ecopetrol su inconformidad con los atropellos causados por las fuerzas militares y de policia, los daños tanto en las fuentes de agua como en las fincas de la personas en las que se desarrolla la sísmica. Ya la semana pasada se denunció que por lo menos **3 fincas fueron afectadas por las petroleras y el ESMAD.**

Aunque el presidente de Ecopetrol manifestó que el diálogo es la fuente principal de las decisiones los habitantes de la región afirmaron que no ha sido por la vía del diálogo como han llegado las empresas, sino con la protección de la fuerza pública, incluso por encima de las pretensiones de los campesinos que en algunos momentos impidieron el paso de la maquinaria hasta que hubo intervención directa del ESMAD.

Adicionalmente desde Abril de este año las comunidades estuvieron denunciando la entrada inconsulta de maquinaria perteneciente a **la empresa Monterrico a los municipios del Doncello y Paujil,** y desde ese momento se insistió en el diálogo. Por su parte según datos de los campesinos y de la agencia nacional minera en el departamento de Caquetá hay por lo menos [43 bloques petroleros con licencias ambientales,](https://archivo.contagioradio.com/43-bloques-petroleros-en-caqueta-amenazan-la-amazonia-colombiana/)algunos de ellos en predios ocupados tradicionalmente por familias agricultoras y pequeños ganaderos.

Las comunidades campesinas ya han venido realizando diversas acciones en las que no solamente se visibilizan los daños ambientales y sociales, sino las alternativas propuestas por las comunidades, una de ellas las marchas y carnavales por el agua que también han unificado iniciativas como las consultas populares en Ibagué y Cajamarca, así como la insistencia de Movimientos campesinos por la defensa de la tierra como el Movimiento Rios Vivos en Antioquia o en el Huila.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
