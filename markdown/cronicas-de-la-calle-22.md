Title: 5 crónicas teatrales que retratan la vida en la calle 22
Date: 2016-11-11 12:27
Category: Cultura, eventos
Tags: Bogotá, Crónicas de la calle 22, Fritas en acción, teatro
Slug: cronicas-de-la-calle-22
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/14567434_1820885228168901_1211637530170379025_o.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### Foto: Colectivo Fritas en Acción 

##### 11 Nov 2016 

Tras el éxito alcanzado el pasado 9 de octubre con el estreno de su obra anual: **Crónicas en la calle 22**,  el colectivo teatral Fritas en acción, presentará dos nuevas funciones este **11 y 12 de noviembre en el Teatro La Macarena de Bogotá**.

La producción teatral parte del relato de **cinco historias que tienen como escenario la calle 22**, avenida centrica de Bogotá, en la que confluyen todo tipo de personas entre ellas una pareja de ancianos la señora **Tránsito** y **don Jacinto**, en cuyo hogar se encuentran los demás personajes de la puesta en escena: **Gabrielita**, nieta de la pareja, quien debido a la desinformación terminará siendo mamá adolescente pero con el transcurrir de los años y con gran esfuerzo llega a ser profesional.

Es también la historia de **Brayan James**, hijo adoptivo de "los cuchos", como él los llama, quien debido a las circunstancias, años después decide convertirse en el jefe de una banda criminal; **Bhritny**, hija de una trabajadora sexual quien a pesar de los intentos por querer tener una vida diferente, siguió los pasos de su madre; **Rosmery**, mujerque carga con el maltrato psicológico y físico propiciado por su exesposo, con el cuidado materno de su pequeña hija **Marianita** y con la necesidad de trabajar en el rebusque.

Durante una hora, los espectadores podrán ver en la complejidad de cada personaje, el reflejo de las realidades que hacen parte de la cotidianidad de muchos colombianos, **quienes viven en dentro de los cinturones de miseria y no siempre son víctimas de sus decisiones sino de lo que les toca vivir**.

Conformado en 2014, el colectivo **Fritas en acción, esta integrado por mujeres estudiantes y profesionales de Derecho**, quienes encontraron en las artes escénicas un punto de fuga a la rigidez de su campo de acción y como una forma de resignificar ciertas acciones de la vida cotidiana. Le puede interesar: [Un taller teatral para la memoria y contra el olvido](https://archivo.contagioradio.com/taller-teatro-bogota-memoria/) .

Desde su creación, el colectivo ha presentado sus obras diferentes escenarios: **Mujer y t.ierra** en La Aldea Nicho cultural, **Los pecados capitales** en la Universidad Colegio Mayor de  Cundinamarca, **FritArte por los animales** en Ágora club, C**uerpo memoria y resistencia** en el Teatro La Quinta porra y en la Universidad Colegio Mayor de Cundinamarca, y ahora al **Teatro la Macarena Calle 26a N° 4a-17** a las **7:30 p.m**., boletería disponible **20 mil individual, 15 mil para dos o más personas**.

<iframe id="audio_13714682" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_13714682_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>
