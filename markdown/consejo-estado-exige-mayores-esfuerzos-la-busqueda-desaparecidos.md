Title: Consejo de Estado exige mayores esfuerzos en la búsqueda de desaparecidos
Date: 2017-01-03 12:21
Category: DDHH, Nacional
Tags: Consejo de Estado, Desaparición forzada, Fiscalía
Slug: consejo-estado-exige-mayores-esfuerzos-la-busqueda-desaparecidos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/06/c-e1483462801485.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Contagio radio 

###### [3 Ene 2016] 

El **Consejo de Estado hizo un llamado de atención a la Unidad de Víctimas** debido a las constantes demoras que hay en la búsqueda de las personas desaparecidas, como lo vienen denunciando desde hace varios años los familiares de estas víctimas.

Precisamente hace un año, organizaciones  defensoras de derechos humanos hicieron  un llamado a las partes de la mesa de negociaciones de paz de La Habana para agilizar la implementación de las medidas inmediatas de búsqueda de personas desaparecidas, y a hacerlos participes de todo este procedimiento.

Hoy, luego de resolver una acción de tutela el alto tribunal señala que **en varios casos han pasado hasta 12 años y no se han obtenido resultados** eficaces en las búsquedas. Demoras que implican la no garantía de los derechos fundamentales de las víctimas, que han vivido años en medio de la incertidumbre.

Frente a esa necesidad el Consejo de Estado solicitó al Departamento Administrativo de la Presidencia de la República y la Fiscalía General de la Nación realizar las tareas necesarias para generar soluciones rápidas y efectivas para quienes buscan  a sus seres queridos.

Así mismo, ordenó a la Fiscalía activar **un mecanismo de búsqueda para resolver las denuncias que existen actualmente en los diferentes organismos**, ya que los familiares de los desaparecidos piden que los esfuerzos en la búsqueda sean conjuntos e integrales y no aislados. Así  ha venido sucediendo con las excavaciones en La Escombrera o con la intervención del cementerio Universal en el que se han sepultado gran cantidad de personas sin identificar o con identidades cambiadas, según ha afirmado Marta Soto, Secretaria Técnica del MOVICE.

Por su parte para organizaciones como el MOVICE es urgente que se ponga en marcha un plan integral de búsqueda de personas desaparecidas, en el que puedan participar los familiares de las víctimas, pues Colombia es el país con mayor número de desapariciones en el continente, con una **impunidad del 99%.**

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio.](http://bit.ly/1ICYhVU) 
