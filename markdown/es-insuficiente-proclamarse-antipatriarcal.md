Title: Es insuficiente proclamarse antipatriarcal
Date: 2019-08-08 16:44
Author: Camilo de las Casas
Category: Camilo, Opinion
Tags: Claudia López, Holman Morris, Petro
Slug: es-insuficiente-proclamarse-antipatriarcal
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/08/Petro-y-holman.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### *[Foto: Holman Morris]* 

[Volvemos a lo mismo de siempre. Fracturas de fracturas que desencantan, que alejan, sin aprender. Vivir de desencuentros en desencuentros en guetos, en egos que en nada alivianan el peso de una realidad trágica, barbara, atroz, que reclama esperanzas.]

[ La decisión estratégica de **Petro** para resolver el incumplimiento de un acuerdo programático de **Claudia López**, apoyando a **Holman Morris**, es altísimamente costosa en un proceso transformador de lo social.]

[Desconocer procesos judiciales contra Morris por acosos sexuales, y el drama público en que se ha convertido un cierre de su relación de modo insano, entre el hoy candidato y la madre de sus hijos, es inconcebible en una discusión de la política de la Vida que proclama quien lidera y define en Colombia Humana.]

[El candidato no ha sido condenado judicialmente, quizás por las hordas de la derecha, pero es innegable que su legitimidad está en cuestión por asuntos de la ética pública. Nadie es perfecto. Nadie es intachable en algo de su vida. Hoy el ejercicio de lo público requiere en lo distinto: aceptar, reconocer, cambiar.]

[La violencia física, simbólica, verbal, la cosificación, la acumulación, la sobre exposición es patriarcal, es machista.y es capitalista.]

[Más allá de las denuncias por diversas conductas unas querellables, otras litigiosas, se trata de un asunto más que jurídico, de la ética de lo nuevo.]

[Y estoy refiriéndome, ante la inveterada tradición maniquea de la moral judeocristiana, muy bien usada por la llamada derecha,  al aliento que impulsa lo nuevo la necesidad del equilibrio, lo que debe reconstruirse desde lo afectuoso, desde lo dionísiaco como lo evoca el recien fallecido Claudio Naranjo.]

[Lo políticamente revolucionario es sanador. Naranjo en relación con la educación, y lo asumo para lo que en la política es lo distinto, afirma: "*Para que una educación sea sanadora, debía ser una educación que se proponga trascender una mentalidad patriarcal, la neurosis universal que es una forma de ser, en que se ha eclipsado nuestra parte solidaria, nuestra parte naturalmente amorosa, por la fuerza de la competitividad rapaz; y más gravemente, el ser humano se ha vuelto contra su propia naturaleza, contra sus impulsos espontáneos, contra su ser animal*”. Y la política llamada alterna sigue padeciendo de esa experiencia  rapaz, de la ambición por el poder mismo.]

[Lograr el equilibrio es reconocer lo hecho, es romper la lógica dominante de la negación y el ocultamiento, insisto más que jurídico, el asunto patriarcal es de existencia, de la Vida. ]

[Una transformación política, social, ambiental es imposible sin reconocer en el cotidiano las expresiones de un alma guerrera, ambiciosa, acaparadora que vive, se reproduce de un sistema de valores excluyente.]

[Ni basta proclamarse **antipatriarcal ni feminista**. La cuestión es distante de lo gnoseológico es un asunto del movimiento interno que se hace colectivo, eso es lo distinto. Y el punto de partida para cualquiera, sea hombre o mujer, que pretenda enfrentar la rampante injusticia en el ejercicio público es reconocer el propio **patriarcalismo**, y su interacción con una animalidad destructiva, nada festiva,nada amorosa que pervive desde hace unos 400 mil millones de años.]

[Hoy en el proceso de conciencia transformante en el mundo, la política distinta nace en una espiritualidad distante de lo patriarcal, gestada en la memoria que desencadena las transformaciones desde el interior y se objetivan en las transformaciones cotidianas en el mundo del trabajo, del hogar, del solaz, de la vida misma. Dramática esta nueva situación en lo que se expresa como ciudadanías libres.Nos queda seguir aprendiendo. Lo nuevo es cuestión de equilibrio.]

[Leer más columnas de][[opinión de Camilo de las Casas]](https://archivo.contagioradio.com/camilo-de-las-casas/)
