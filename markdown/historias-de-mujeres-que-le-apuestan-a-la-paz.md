Title: Historias de mujeres que le apuestan a la paz
Date: 2016-03-08 17:22
Category: Mujer, Reportajes
Tags: AFRODES, mujeres
Slug: historias-de-mujeres-que-le-apuestan-a-la-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/03/historias-de-mujeres-en-paz.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<iframe src="http://co.ivoox.com/es/player_ek_10720815_2_1.html?data=kpWklJWcdZahhpywj5eUaZS1kpmah5yncZOhhpywj5WRaZi3jpWah5ynca7py8rfx9iPuNPVw8bXw9PIs4zk0NeYzsaPtMLujoqkpZKns8%2FowszW0ZC2pcXd0JCah5yncZU%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [8 Mar 2016] 

#### **Por. Silvia Arjona Martín** 

“Sin las mujeres la democracia no va, sin las mujeres colombianas el proceso de paz no avanza”. La senadora Ángela Robledo lo tiene claro y trabaja desde el Congreso para que el 47% de la población, los hombres, lo comprendan igual. “El papel que han tenido las mujeres en el conflicto armado ha sido el de resistencia. Ellas han encarnado el 80% de las víctimas, pero han hecho de ese dolor un hecho político”, insiste.

Pero como Robledo son muchas las mujeres que luchan en Colombia por un país donde se sientan integradas y donde el sistema patriarcal decline por otro más igualitario. Una pelea contra las injusticias vividas en una guerra cuya peor parte se la han llevado siempre ellas.

Luz Marina Becerra, presidenta de la Asociación Nacional de Afrocolombianos Desplazados, AFRODES, ha sufrido las consecuencias de nacer en un territorio rico y fértil en recursos naturales: el Chocó. La codicia humana ha hecho que ella tuviera que salir huyendo de su departamento y refugiarse en Bogotá en busca de un suspiro alentador que la ayudara, aunque lo que encontró no fuera lo esperado.

Luz Elena Galeano aún busca a sus seres queridos en una escombrera, en Medellín. El ruido de fusiles le persigue en la memoria desde el trágico 2001, año en el que dos famosas operaciones perpetradas por militares se llevaran la vida de muchos civiles de la Comuna 13. Ella y otras mujeres no han desistido de pelear por saber la verdad de la desaparición de sus familiares, que aún buscan con ahínco por derechos y respeto.

Yahaira Mejía sabe cómo construir una casa con sus propias manos. El desplazamiento forzado la convirtió en albañil casi de casualidad. Junto a otras mujeres del barrio periférico de Cartagena de Indias, El Pozón, decidieron emprender un proyecto que las dignificara como personas, pero sobre todo como mujeres. La Ciudad de las Mujeres reina en Turbaco, una localidad cerca de Medellín, con orgullo y satisfacción después de un trabajo de empoderamiento y de reparación colectiva. En ella casi no hay varones, y los que conviven han de seguir las reglas del matriarcado turbaquero.

Como estos ejemplos, hay muchos otros repartidos por toda la geografía de mujeres fuertes y resistentes a todas las vulnerabilidades que las acechan. Todas ellas son constructoras de paz con justicia social y por ese papel tan importante que tienen en la sociedad, viajamos por distintos puntos del país para visibilizar y dignificar a las mujeres colombianas y a sus causas.

Porque todas ellas, aunque invisibilizadas, trabajan desde sus pequeños rincones por construir otro modelo de país más respetuoso, más igualitario y en paz.
