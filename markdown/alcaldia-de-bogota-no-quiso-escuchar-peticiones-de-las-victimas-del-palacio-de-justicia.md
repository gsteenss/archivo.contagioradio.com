Title: Alcaldía de Bogotá no quiso escuchar peticiones de víctimas del Palacio de Justicia
Date: 2016-03-31 15:49
Category: DDHH, Nacional
Tags: Bogotá, Enrique Peñalosa, Palacio de Justicia
Slug: alcaldia-de-bogota-no-quiso-escuchar-peticiones-de-las-victimas-del-palacio-de-justicia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/03/Palacio-de-Justicia-e1459457565788.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: RCN Radio 

###### [31 Mar 2016] 

Pese a que los familiares de las víctimas del Palacio de Justicia, esperaban que la alcaldía de Bogotá no atendiera la solicitud de retirar la placa conmemorativa de los hechos de la toma y retoma del Palacio de Justicia instalada desde el año 2012 por los familiares, la secretaría de General de la Alcaldía, respondió que la placa **“será modificada  cesando así la vulneración al derecho al buen nombre del oficial retirado”,** dice la respuesta.

“Seguimos siendo vulnerados en nuestros derechos  a la reparación y a la dignidad como víctimas, de todas maneras el Coronel Plazas Vega tiene responsabilidad en los hechos de la retoma del [Palacio de Justicia](https://archivo.contagioradio.com/?s=palacio+de+justicia), donde nuestros familiares fueron desaparecidos, torturados y luego asesinados”, expresa Rosa Milena Cárdenas, familiar de uno de los desaparecidos del Palacio de Justicia, quien agrega que **“Nos duele que se quiera seguir desapareciendo de la verdad”.**

La subdirección administrativa de la Secretaria General de la Nación de la Alcaldía le dio respuesta positiva al derecho de petición del concejal Daniel Palacios, quien solicitaba a la actual administración de Enrique Peñalosa el retiro de la placa, lo que para las víctimas significa un acto que atenta contra la dignidad de sus familiares, “No se debe quitar el nombre del coronel Plazas Vega ni de ninguno de los coroneles, porque ellos tenía la responsabilidad de procurar que la vida de las personas fuera respetada”, dice Cárdenas.

Sandra Milena hace un llamado a que la administración distrital actual “**apoye las víctima y la lucha que llevamos durante 30 años… que el señor Enrique Peñalosa se ponga en los zapatos de una víctima,** que se pregunte por un solo día qué haría si tuviera un familiar desaparecido, secuestrado o que mataron”.

Se trata de una placa que representa un homenaje a los familiares desaparecidos por el Estado colombiano y que ya fueron reconocidos internacionalmente por CIDH y por el presidente de la República, Juan Manuel Santos el pasado 6 de noviembre de 2015. 27 años tuvieron que pasar para que por fin se diera el permiso a las víctimas para instalar la placa conmemorativa en uno de las columnas de la alcaldía de Bogotá con vista al Palacio de Justicia, y al lado de otra placa que **tergiversa la historia de los hechos, puesta por Jaime Castro, ex alcalde de Bogotá.**

Para la hermana de Luz Mary Portela, en estos momentos actuales del país, las víctimas necesitan que el Estado este de parte de ellos para empezar una resocialización hacia la paz. Finalmente anuncia que los familiares del holocausto del Palacio de Justicia, se reunirán para dar una respuesta a la actual administración de Bogotá, que **resolvió no escuchar la solicitud de los familiares de respetar la memoria,** y en cambio si envió respuesta escrita al concejal del Centro Democrático.

<iframe src="http://co.ivoox.com/es/player_ej_10999466_2_1.html?data=kpWmm56Yepehhpywj5WbaZS1k52ah5yncZOhhpywj5WRaZi3jpWah5yncaLgxMbZxoqnd4a1pcaYxsqPhtDb0NmSpZiJhZKfz9SY09rNt9CfxtjQ18jMpdOf0crhy8jNs8%2FZ1JDRx5DQpY6ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)
