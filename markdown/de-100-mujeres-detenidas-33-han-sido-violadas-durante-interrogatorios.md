Title: De 100 mujeres detenidas, 33 han sido violadas durante interrogatorios
Date: 2016-06-28 12:21
Category: DDHH, El mundo
Tags: Amnistía Internacional, guerra contra el narcotráfico, mexico, Torturas, violencia sexual
Slug: de-100-mujeres-detenidas-33-han-sido-violadas-durante-interrogatorios
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/06/tortura-mujeres-mexico-contagioradio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:]

###### [28 jun 2016]

La organización de Derechos Humanos Amnistía Internacional, realizó una investigación en la que entrevistó a 100 mujeres detenidas en las prisiones federales de México, y encontró cifras alarmantes en cuanto a torturas sexuales contra ellas. De las 100 mujeres entrevistadas, **72 fueron víctimas de abusos sexuales y 33 denunciaron que fueron violadas** durante los primeros interrogatorios a los que fueron sometidas.

Las denuncias van desde violaciones con los órganos sexuales, las manos e incluso con objetos, además las torturas también incluyen golpes y descargas eléctricas en los genitales de las mujeres. En el informe resaltan el caso de una mujer de 26 años que fue acusada de pertenecer a bandas criminales**, sometida a tortura y obligada a ver la tortura de su esposo que murió en sus brazos mientras eran trasladados a prisión**.

Según esa organización los abusos son cometidos durante las primeras horas de arresto o en los primeros días de interrogatorios y los responsables se encuentran vinculados a las [fuerzas de policía y a las fuerzas militares](https://archivo.contagioradio.com/mexico-reporta-mas-de-27-mil-personas-desaparecidas/) que están desplegadas en todo el país desde hace cerca de 10 años al promulgarse la llamada **“[guerra contra el narcotráfico](https://archivo.contagioradio.com/papa-francisco-ha-olvidado-referirse-a-la-dificil-situacion-de-ddhh-en-mexico/)**” en el gobierno de Felipe Calderón.

### **La impunidad prevalece** 

El alcance de la impunidad es otro factor preocupante en cuanto a las denuncias presentadas por las víctimas de tortura. Amnistía Internacional encontró que solamente en 2013 se presentaron más de 12000 denuncias por torturas, las cuales se duplicaron en 2014. **De esas denuncias solamente 7048 llegaron a la Comisión Nacional de Derechos Humanos entre 2010 y 2015. Desde 1991 solamente 15 investigaciones han llegado a [sanciones contra los organismos señalados](https://archivo.contagioradio.com/mexico-se-moviliza-exigiendo-alto-a-los-crimenes-de-estado/)por las víctimas**.

Ante los tribunales la situación es mucho peor. Aunque la carga de la prueba presentada contra las víctimas recae sobre el ministerio público, en muchos de los casos, las víctimas, que han sido obligadas a firmar confesiones bajo situaciones de tortura, resultan siendo condenadas, puesto que los **juzgados no tienen en cuenta las denuncias y dan toda la credibilidad a los testimonios firmados**.

Sin embargo, a pesar de las alarmantes cifras, los casos podrían ser muchos más y puede haber un sub registro de las denuncias. [Amnistía Internacional](https://archivo.contagioradio.com/?s=amnistia+internacional) aseguró que los **organismos de seguridad no accedieron a abrir sus expedientes** a los delegados de esa organización.

“**Cuesta creer el afán con que [México](https://archivo.contagioradio.com/?s=mexico) encubre su crisis nacional**. En lugar de intentar encubrir miles de casos de tortura y otros malos tratos, las autoridades deberían centrar sus energías en garantizar la erradicación definitiva de la tortura garantizando el enjuiciamiento de los responsables y reparaciones adecuadas a las víctimas”, afirmó Erika Guevara-Rosas representante de AI para las Américas.
