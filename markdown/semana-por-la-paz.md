Title: Reconciliación será el centro en la Semana por la Paz
Date: 2017-08-24 13:51
Category: Nacional, Paz
Tags: acuerdos de paz, negociaciones eln, Papa Francisco, Redepaz, semana por la paz
Slug: semana-por-la-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/08/Semana-por-la-Paz-UIS.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Colombia Apoya la Paz] 

###### [24 Ago 2017] 

La trigésima Semana por la Paz, que se llevará a cabo del 3 al 10 de septiembre **busca reafirmar el mensaje de reconciliación** y la voluntad de paz de los colombianos y de las organizaciones sociales del país. Como esta actividad coincide con la visita del Papa Francisco, las organizaciones esperan que se fortalezca su mensaje de construcción de paz.

Según Luis Emil Sanabria, director de la Red Nacional de Iniciativas Ciudadanas por la paz y contra la Guerra (REDEPAZ), **“la versión 30 de la Semana por la Paz será un momento que refleja el compromiso de la sociedad colombiana con la paz”.** Indicó además que, durante estos días, las diferentes regiones y comunidades del país realizarán exposiciones artísticas y sembrarán árboles por la reconciliación. (Le puede interesar:["Comunicación para la paz, un desafío aún pendiente"](https://archivo.contagioradio.com/comunicacion-paz-colombia/))

Adicionalmente, se realizarán foros que contarán con la participación de delegados del Gobierno Nacional y las FARC. Desde Villavicencio, una de las ciudades que visitará el papa, se realizará la campaña **“Muchos Pasos por la Paz para acompañar al sumo pontífice** con mensajes de amor y donde le vamos a exigir al Gobierno Nacional y al ELN un cese bilateral al fuego”.

Redepaz indicó que la **Semana por la Paz será un espacio para avanzar con las demandas de la ciudadanía referidas al proceso de paz** que se está llevando en el país. Sanabria invitó a las personas para que organicen actos de paz a través de “pequeñas o grandes movilizaciones que evidencien el compromiso constante con la paz y la reconciliación”.

<iframe id="audio_20510129" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_20510129_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)

<div class="osd-sms-wrapper">

</div>
