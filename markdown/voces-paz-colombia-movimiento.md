Title: "Nuestras Voces serán las voces de los acuerdos de paz"
Date: 2016-12-15 12:10
Category: Entrevistas
Tags: FARC-EP, movimiento, proceso de paz, voces de paz
Slug: voces-paz-colombia-movimiento
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/12/VOCES-DE-PAZ-Carmela-Maria.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### Foto: Carmela María 

##### 15 Dic 2016 

Este jueves 15 de Diciembre, se realizó la **inscripción de “Voces de Paz”, movimiento que estará a cargo de la veeduría en la implementación de los acuerdos alcanzados en la Habana, tanto en Senado como en Cámara**. Los representantes de este movimiento tendrán voz pero no voto en el Congreso y su ejercicio servirá como preámbulo para la creación de un partido político de las FARC-EP.

En rueda de prensa, **Imelda Daza, sobreviviente de la Unión Patriótica, reconocida líder social y defensora de Derechos Humanos**, leyó un comunicado en nombre del nuevo movimiento en el que expresan "hemos querido asumir le difícil pero reconfortante tarea de contribuir a que se consoliden las comisiones para la transformación de las FARC-EP en un partido político legal tal y como esta convenido por las partes". Le puede interesar: [Desde voces de paz haremos las cosas diferentes](https://archivo.contagioradio.com/desde-voces-de-paz-haremos-las-cosas-diferentes/).

Durante la solicitud de inscripción ante el Consejo Nacional Electoral CNE colombiano, el movimiento Voces de paz expresó estar "firmemente convencidos de que **los acuerdos son la semilla de las transformaciones  para el buen vivir de nuestras futuras generaciones**"; "Con su exitosa implementación se inaugurará un ciclo de reformas en los más diversos campos de la vida social que contribuirá a cambiar la vida de hombres y mujeres en campos y ciudades".

La vocera en compañía de los otros cinco[ representantes del movimiento](https://archivo.contagioradio.com/voces-de-paz-sera-el-movimiento-ciudadano-con-voz-y-voto-en-congreso/)manifestó en su lectura que "**habrá más democracia y mayores condiciones para la participación social y ciudadana**, sera posible emprender un aplazado proceso de esclarecimiento de la verdad histórica condición ineludible para la superación cierta del pasado de horror y violencia y para el resarcimiento de los derechos de los millones de víctimas que deja el conflicto".

"Tendremos la posibilidad de imaginar una contienda política abierta, desprovista del uso de las armas y en la que el asesinato de compatriotas por razón de sus convicciones políticas no haga parte de la vida cotidiana" y añadió "**Nuestras voces serán, las voces de los acuerdos de paz, nuestras voces en el congreso serán voces de veeduría y control social** y de ciudadanos en ejercicio para que la implementación normativa atienda el espíritu y la letra de lo acordado"

"Nuestras voces serán las voces para contribuir a sentar las bases de una amplia convergencia política y social por la paz, la democracia profunda y la justicia social, y hacer de esta un proyecto común ético y político para un gobierno de transición, **que deje atrás y por siempre el estado de guerra y violencia e inicie la construcción del nuevo país** que anhelan nuestros jóvenes, ese sera nuestro mejor legado para nuestras futuras generaciones" concluyó el comunicado que antecedió a una corta ronda de preguntas.

<iframe id="audio_15035872" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_15035872_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>
