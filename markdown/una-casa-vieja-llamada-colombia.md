Title: Una casa vieja llamada Colombia
Date: 2016-02-03 09:36
Category: invitado, Opinion, superandianda
Tags: paz, pobreza
Slug: una-casa-vieja-llamada-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/02/CASA-VIEJA.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: La Patria 

#### **[Superandianda](https://archivo.contagioradio.com/superandianda/)- [@superandianda](https://twitter.com/Superandianda)** 

###### 3 Feb 2016 

Colombia es la casa vieja que nadie se atreve a derrumbar, llena de ratas y palos podridos que sus vivientes soportan para que no se les caiga encima, esos pobres vivientes que son al fin de cuentas sobre-vivientes, quienes con latas de pintura de colores pintan las viejas paredes de cal esperando algún día poder quitar lo que no sirve. Lo peor de la casa es que ya ni siquiera sus vivientes son sus dueños, han pasado a ser de propietarios a inquilinos.

[Siempre será más difícil construir que destruir, es más fácil actuar bajo lo ya dicho y hecho, eso es lo que sucede en la  casa vieja llamada Colombia, hemos sido formados para acatar no para crear, pensamos que no sabemos construir la casa donde vivimos, cedemos nuestros derechos a importantes firmas para que vengan a levantar muros por nosotros. Sin soberanía, sin identidad y sin pertenencia no somos dueños, no tenemos democracia, y lo sabemos, pero es más fácil saberlo y callarlo que saberlo y cambiarlo.]

[Si vamos a hablar de la paz hablemos de la auténtica paz, de la que construye; si vamos a hablar de política que seamos todos y no los mismos que siempre han callado a los que llaman minoría. No basta con los guerrilleros entregando armas si el ciudadano de a pie anda armado, armado de  lo que nos violenta sin piedad: la pobreza, la desigualdad, la indiferencia, la burocracía; esos palos podridos de la casa vieja que siguen de pie.]

[Resurgir, resucitar, renacer, volver. Religión y ciencia tienen un punto en común: es necesario acabar para volver a empezar, destruir para construir. La paz en los campos es el inicio, pero no es suficiente si queremos una estructura social fuerte para Colombia. No es cosa de dos días ni de un año volver a levantar las paredes que durante décadas han sostenido a la casa, pero lo que se hace urgente es derribar las viejas. Se preguntarán ¿cuáles paredes viejas? Las de los partidos políticos de dinastías familiares, las que han pintado de colores que no nos representan, unas rojas, otras azules y verdes, pero hechas de la misma cosa.]

[Reinventar es el temor, no es fácil  derribar la estructura que de una u otra manera siempre ha sostenido la casa vieja, donde unos han sabido vivir pero muchos otros no, en este caso lo correcto sería salir y verla desde afuera: desde el campesino, desde el guerrillero, desde el soldado, desde el obrero. El problema es que siempre la hemos visto desde el gobierno, desde los noticieros, quizá de ahí la  falta de dignidad y la indiferencia.]
