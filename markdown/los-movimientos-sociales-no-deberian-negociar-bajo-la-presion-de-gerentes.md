Title: Los movimientos sociales no deberían negociar bajo la presión de gerentes
Date: 2016-07-12 11:06
Author: JOHAN MENDOZA TORRES
Category: Johan Mendoza, Opinion
Tags: ESMAD, Movilización, protesta
Slug: los-movimientos-sociales-no-deberian-negociar-bajo-la-presion-de-gerentes
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/07/esmad-marcha.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Contagio Radio 

#### [[Johan Mendoza Torres ](https://archivo.contagioradio.com/johan-mendoza-torres/)] 

###### 12 Jul 2016 

[Unos de los problemas teóricos más graves que existe en Colombia y que es promovido en el discurso mediático, en el discurso político institucional y además en gran parte de la academia, es la absurda creencia de que gobierno y gerencia son lo mismo. Para no ir lejos en la explicación básica,]*[el gobierno]*[ tiene que ver con la conducción de comunidades políticas, cuyo objetivo final debe ser el bienestar social; y siembra sus raíces filosófico-políticas en todo el tema del contrato social, la soberanía, etc.]*[La gerencia]*[ por su parte, está relacionada con la optimización de recursos financieros, con la dirección empresarial, y su objetivo se rige bajo el “mítico”, yo diría embaucador, concepto de éxito.]

[Claramente la cosmovisión, en esta limitadísima explicación, evidencia una diferencia abismal. Pero la cosa es más grave, porque en la práctica, los grandes grupos económicos y financieros de poder, esos mismos que se tomaron el poder político, los medios masivos de información, esos mismos que tienen relación directa o indirecta con la inequitativa tenencia de la tierra en el país,] [y que siguen comprobando el fracaso rotundo de la teoría de Friedman, obviamente no están en función de las mayorías.  ]

[Por eso, cuando hay un paro reprimen con fuerza desmedida, por eso, cuando hay una protesta, desde sus medios masivos salen a deslegitimarla, desde sus universidades no le explican al estudiante el problema teórico y lo convencen de entender los problemas del país exclusivamente desde los conceptos anglosajones del mercadeo, por eso, cuando el Esmad no puede parar la protesta disparan “desde algún lugar” con fusiles, por eso, cuando un gremio o sector entran en paro, y como expresión máxima del fracaso del neoliberalismo, las multinacionales y el Estado se unen con tal de que el flujo no se detenga y la protesta pierda fuerza.]

[Por todo la anterior, la gente del movimiento social, debe reconocer una cosa elemental: el Estado colombiano, ese supuesto Estado social de derecho, está secuestrado por gerentes. Y los gerentes no fueron formados para gobernar, fueron formados para creer que un paro desprestigia, que un paro no sirve, que inversión extranjera es el único foco de desarrollo, fueron formados para optimizar recursos financieros y aquí ya todos sabemos qué significa ahorrar u optimizar recursos… si aún no lo sabe, bastaría con leer de qué se trata la reforma tributaria que se nos viene a los ciudadanos comunes y corrientes, basta con preguntar a quién le dieron el contrato de las carreteras 4G, basta con preguntar quién otorgó los contratos de Reficar, basta preguntarse si en verdad lo poco que le dan a la gente es ayuda social o una estrategia para ganar votos y hacer negocios más jugosos, basta con preguntarnos por el sistema de salud,  basta con tratar de responder por qué los congresistas ganan 28 millones y se suben el sueldo por decreto inderogable en agosto de 2015 y en junio de 2016 y por qué a la hora se subirle a los trabajadores, todas las opciones que beneficien a la gente, supuestamente siempre quebrarán al país… ¡la conclusión es simple!: los gerentes nunca han estado bajo presión.]

[Bajo presión estuvo la gente asesinada en las carreteras y en las ciudades por el Esmad. Bajo presión están los empleados que no reciben sueldo. Bajo presión los estudiantes y docentes que no pueden ejercer su rol social por culpa de la corrupción, bajo presión el campesino, el indígena, el afro; ¡bajo presión la gente! Eso no se debe perder de vista, porque luego terminarán afirmando desde los medios masivos la falacia de siempre: que los problemas del país, son culpa de la gente que no quiere una economía competitiva, que no se ajusta, que no quiere progreso y bla bla bla.      ]

[El movimiento social en Colombia no puede negociar bajo la presión de empresarios que saltan de dirigir una empresa a ser ministros y luego terminan de embajadores. El movimiento social no puede negociar bajo la presión de los gerentes porque ellos han secuestrado la institucionalidad y han negado sistemáticamente la posibilidad de una democracia. El movimiento social no puede negociar bajo la presión de los gerentes, porque los gerentes no gobiernan, los gerentes protegen intereses totalmente distintos a los de la gente común y al final… ¿de qué sirve una democracia que arruina a los actores sociales y los trata de delincuentes, de tercos, de mafias, de necios, de brutos, de trasnochados, de injustos y una cantidad absurda de improperios que emiten los grandes medios masivos de información?]

[Así no se puede negociar. No se puede negociar bajo presión. Pero se debería tener en cuenta que esta frase no les pertenece a los gerentes, le pertenece a la gente que es la que está pasando las duras y las maduras para tratar de convencer desde los campos con paros agrarios, de camioneros y los próximos paros que se vengan en las ciudades, de que en Colombia no hay gobierno, sino una gerencia muy provechosa para pequeños círculos de poder.]

[Por supuesto, estas palabras, a muchos les sonarán “trasnochadas”, no obstante, como los críticos de la crítica creen que los alimentos nacen en el supermercado, o que llegan en helicóptero a los centros comerciales ¿para qué detenernos en ellos? Mejor, hombres y mujeres ajustemos nuestro cinturón, pues no se trata de convencer a los que no saben de qué se trata vivir bajo presión, sino que se trata de hacer de la lucha de los movimientos sociales el acto más fecundo que puede producir nuestra patria, un acto que produzca simpatía, que produzca esperanza, que cultive generaciones y generaciones, pues sin duda alguna, la lucha será larga.]

------------------------------------------------------------------------

###### Las opiniones expresadas en este artículo son de exclusiva responsabilidad del autor y no necesariamente representan la opinión de la Contagio Radio. 
