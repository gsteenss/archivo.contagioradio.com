Title: "Deben cuidarse, atentarán contra sus vidas": Gaula Santander a líderes de ASOGRAS
Date: 2016-07-14 15:59
Category: DDHH, Nacional
Tags: Asociación Agrícola de Santander, Magdalena Medio presencia paramilitar, paramilitarismo en Colombia
Slug: deben-cuidarse-atentaran-contra-sus-vidas-gaula-santander-a-lideres-de-asogras
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/07/ASOGRAS.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Notimundo] 

###### [14 Julio 2016] 

En los últimos días líderes comunales de Sabana de Torres, que luchan por la titulación de sus tierras han sido **perseguidos por hombres desconocidos en motocicletas**, durante sus recorridos en Puerto Wilches, Barrancabermeja y Bucaramanga. Así mismo denuncian que Cesar Tamayo, integrante de la Asociación Agrícola de Santander, recibió una llamada de un funcionario del Gaula de la Brigada Número 5, en la que le advierten que ellos tienen información sobre el riesgo que corre su vida.

Tamayo expresa su preocupación por este tipo de amenazas contra líderes y miembros de la Asociación y mucho más cuando "**hemos venido participando directamente con propuestas a favor del proceso que está llevando en La Habana**, igualmente arrancamos una campaña por la paz en el Magdalena Medio y en Sabana de Torres (...) creemos que es a raíz de esta situación que se vienen presentando estos hechos".

"Estamos muy preocupados precisamente porque **quienes deberían brindarnos seguridad hoy nos manifiestan que debemos cuidarnos**, estamos desprotegidos en este momento, a pesar de eso seguimos trabajando en el desarrollo de nuestras propuestas como pequeños y medianos campesinos", agrega Tamayo.

Pese a las 39 denuncias que el líder ha interpuesto por las amenazas que ha recibido en su contra, las autoridades no han brindado las garantías de seguridad para su vida, presiones que se agudizan por la el **reacomodamiento de la presencia paramilitar en municipios del Magdalena Medio y Santander**, en dónde la Asociación propone la implementación de una zona de desarrollo para la reparación de las familias campesinas, indígenas y afrodescendientes víctimas del conflicto armado.

<iframe src="http://co.ivoox.com/es/player_ej_12226053_2_1.html?data=kpeflJuUeZShhpywj5aUaZS1lp6ah5yncZOhhpywj5WRaZi3jpWah5yncaTZ1MbfjbnFscLt0IqfpZCll7C7s6bAj4qbh4630NPhw8zNs4zGwsnW0ZCRaZi3jpk.&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)]]
