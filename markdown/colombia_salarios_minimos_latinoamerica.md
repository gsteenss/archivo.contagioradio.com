Title: Colombia tiene uno de los salarios mínimos más bajos de toda Latinoamérica
Date: 2017-05-12 17:17
Category: Economía, Nacional
Tags: salario minimo
Slug: colombia_salarios_minimos_latinoamerica
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/12/salario-minimo-contagioradio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: el Universal 

###### [12 May 2017] 

[Mientras en Colombia se trabaja en una recolección de firmas para disminuir el sueldo de los congresistas que es uno de los más altos en toda América Latina. Un informe de la Universidad de La Sabana, demuestra los altos índices de desigualdad que existen en el país, que **ocupa el cuarto lugar en los salarios mínimos más bajos de la región.**]

[El sueldo mínimo en Colombia son 215 dólares, un monto que solo es superado por salarios como el de Brasil que ocupa el tercer lugar entre los más bajos con  **2**]**12 dólares, en segundo lugar está México con 120 USD, y Nicaragua es el país con el sueldo más bajo de toda región con 115 USD.**

Así lo refiere [un estudio realizado por el programa de Economía y Finanzas de la Universidad de La Sabana, donde se concluye que en medio de los bajos pagos, los precios de los alimentos, del transporte, la vivienda y la educación continúan aumentando por encima del salario mínimo, lo que muestra un **incremento en el costo de vida de los colombiano.** [(Le puede interesar: El salario mínimo cada vez más mínimo en Colombia)](https://archivo.contagioradio.com/salario-minimo-cada-vez-mas-minimo-en-colombia/)]

[“Aún estamos muy lejos de ]**Panamá (744 USD), Costa Rica (512), Argentina (448) y Guatemala (369) **[que tienen los mejores salarios mínimos de la región”, señala Ana María Olaya, investigadora de la Universidad de la Sabana y autora del estudio.]

[Y es que de acuerdo con cifras de la Organización Internacional del Trabajo, entre 2006 y 2013 en América Latina el ]crecimiento promedio del salario real fue del 1,01%, y aunque ese porcentaje parece bajo, en **Colombia apenas fue de un** [ ]**0.01% en el mismo periodo.**

[Cabe recordar que en contraposición al bajo salario mínimo que reciben el 54%  de los colombianos, según cifras oficiales, los congresistas del país reciben los sueldos más altos de la región. El Diario La Nación de Argentina revela que Colombia ocupa el quinto lugar con los sueldos más altos para **los senadores y representantes, pues**]**mensualmente obtienen**[ ]**27.929.064 de pesos, **[sin incluir beneficios como pago por seguridad y comunicaciones; mientras que ]el salario mínimo para 2017 subió un 7%, es decir 48.262 pesos.[(Le puede interesar: Hasta el 7 de julio se recogerán firmas para bajar el sueldo a congresistas)](https://archivo.contagioradio.com/hasta-el-7-de-julio-se-recogeran-firmas-para-disminuir-salario-de-congresistas/)

###### Reciba toda la información de Contagio Radio en [[su correo]
