Title: No cesan los asesinatos contra integrantes de las FARC
Date: 2018-01-17 15:07
Category: DDHH, Nacional
Tags: FARC, ONU DDHH
Slug: asesinados-integrantes-de-las-farc-peque
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/01/integrantes-de-las-farc-asesinados.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: FARC] 

###### [17 Ene 2017]

Los integrantes de las FARC, Wilmar Asprilla y Angel de Jesús Montoya fueron asesinados este 16 de Enero en jurisdicción del municipio de Peque, departamento de Antioquia. Según la denuncia del partido político los militantes habían salido de una reunión con la comunidad del sector hacia las 11 de la noche cuando fueron asesinados.

Según el comunicado difundido en la mañana de este 17 de Enero, l**os dos integrantes del partido político se reunieron con algunos de los líderes comunitarios para explicar sus propuestas de campaña y las apuestas programáticas**, esto en cumplimiento del acuerdo alcanzado con el gobierno y en desarrollo de la actividad política de esa organización.

Una vez terminaron la reunión la comunidad escuchó varios disparos acudiendo de manera inmediata al lugar y encontrando los cuerpos de los dos hombres en las proximidades a un parqueadero del sector. Hasta el momento se desconocen más detalles del hecho pero se estableció que l**as exequias se realizarán en el municipio de Dabeiba.**

Según las cifras de las FARC son 49 integrantes de la “comunidad fariana” los que han sido asesinados, once de ellos han sido asesinados en Antioquia. También resaltan que 13 familiares han sido víctimas en diversas regiones del país siendo el departamento del Cauca el más peligroso para ellos y ellas. [Lea tambien: Paramilitares amenazan a las FARC y a líderes sociales en el sur del país](https://archivo.contagioradio.com/nuevas-amenazas-de-paramilitares-contra-farc-y-lideres-sociales/)

Además esto representaría un fuerte golpe al proceso de campaña electoral en el cual están participando y que convoca a las urnas a los colombianos para el próximo 11 de Marzo. Por ello las FARC reiteraron la exigencia de garantizar las condiciones de seguridad para la participación política efectiva tal y como fue acordado con el gobierno. [Lea también: Mas del 30% de asesinatos de defensores en el continente se registran en Colombia](https://archivo.contagioradio.com/mas-del-30-de-asesinatos-a-defensores-en-el-continente-se-registraron-en-colombia/)

### **Municipios de Antioquia han sido tomados por el paramilitarismo** 

Según algunas fuentes que tienen presencia en la región, muchos de los líderes de FARC aseguran que no hay garantías para el ejercicio de la política, dada una situación en la que, según ellos, se está evidenciando una persecución política por parte de paramilitares que afianzan el control de las actividades del narcotráfico y que se habrían manifestado en contra de quienes impulsan la implementación del acuerdo y la sustitución de los cultivos de uso ilícito.

###### Reciba toda la información de Contagio Radio [en su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio ](http://bit.ly/1ICYhVU) 
