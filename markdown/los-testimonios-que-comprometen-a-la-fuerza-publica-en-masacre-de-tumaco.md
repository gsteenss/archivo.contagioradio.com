Title: Los testimonios que comprometen a la Fuerza Pública en masacre de Tumaco
Date: 2017-12-28 11:19
Category: DDHH, Nacional
Slug: los-testimonios-que-comprometen-a-la-fuerza-publica-en-masacre-de-tumaco
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/12/Captura-de-pantalla-2017-12-28-a-las-11.17.47-a.m..png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Asociación Minga] 

###### [28 Dic 2017] 

El 22 de diciembre, la Fiscalía General de la Nación anunció que **imputará cargos contra dos oficiales**, un Policía y un militar por la masacre ocurrida en la vereda el Tandil en Tumaco el 5 de octubre de este año. Los pobladores, que al momento de la masacre se encontraban realizando una protesta pacífica por las acciones de erradicación forzada de los cultivos de hoja de coca, concuerdan con que las personas que dispararon eran miembros de la policía antinarcóticos. Allí fueron asesinadas 7 personas.

Luego de dos meses parece que las **investigaciones han avanzado** teniendo en cuenta evidencias físicas recolectadas por la Fiscalía al igual que informes de necropsias, dictámenes médicos, declaraciones juradas y entrevistas. Sin embargo, en ese momento se encontraron dos versiones, la de los campesinos y la de la Fuerza Pública.

Por un lado, los campesinos manifestaron que trataron dialogar con los integrantes de la Fuerza Pública pero **ellos abrieron fuego contra los manifestantes** quienes trataron de huir a la vez que auxiliaron a los más de 20 heridos que dejaron los disparos. (Le puede interesar: ["Especial: Tumaco llora una masacre "en tiempos de paz"](https://archivo.contagioradio.com/tumaco-llora-una-masacre-en-tiempos-de-paz/))

Por su parte la Policía manifestó que **fueron disidentes de las FARC** quienes dispararon contra los campesinos. Los uniformados indicaron que ellos, al tener entrenamiento, pudieron protegerse de las balas y fueron los campesinos quienes quedaron expuestos.

Sin embargo, **esta versión fue desmentida** por las pruebas y testimonios que aportaron los campesinos como, por ejemplo, que la Fuerza Pública manipuló el lugar de los hechos para encubrir su culpabilidad.

### **Los testimonios de los campesinos a Contagio Radio** 

Todos los relatos concuerdan en que **fueron los policías quienes dispararon contra la población** que mantenía un cordón humanitario para impedir la erradicación forzada. De acuerdo con la información recogida por Contagio Radio en el lugar, sería un comandante de policía, “con acento costeño y con chivera el que dio la orden para disparar”. (Le puede interesar: ["Conclusiones de los medios sobre masacre de Tumaco revicatimizan: ASOMINUMA"](https://archivo.contagioradio.com/47979/))

De acuerdo con uno de los pobladores y sobrevivientes de la masacre, “intentaban hablar con los policías, cuando se oyó unos disparos y se sacó al primer herido, en cuestión de segundos **empezaron a sonar las ráfagas de fusil M60**, y ahí fue donde hubo muchos heridos, ahí me pegaron ese tiro en el ojo y herido me sacaron”.

Otro campesino recuerda que los Policías **los amenazaron** “decían ábranse de aquí. Uno de los policías nos dijo, ¿ustedes lo que quieren es de esto? Y les puso el fusil bajito y lanzó el primer tiro, y a los 30 segundos el señor de la M60 la cogió en ráfaga riéndose”.

Continúa diciendo **“les decíamos que dejaran de disparar**. Cuando pasó todo sacamos cuatro muertos, dos muchachos con disparos en la cabeza y otros en el cuerpo. A unos heridos le desbarataron la mano derecha y a otro una pierna”.

Otro testimonio indica que los Policías **remataron a tiros a un indígena** que se encontraba herido en el piso y que pedía ayuda. Él se escondió detrás de un árbol para que no lo vieran y así salvó su vida. “Si yo pudiera haber grabado hubiera sido mucha evidencia, pero no podía grabar. Me salvé de milagro, si no también me hubiera matado ese policía. Yo me escondí en un palo, y ahí pude observar muchas muertes”.

### **A sindicados se les imputaron cargos por homicidio agravado** 

A quienes se le imputarán cargos por esta masacre son **Javier Enrique Soto García**, comandante Núcleo Delta de la Policía Nacional quién se encuentra en funciones, y **Jorge Niño León**, comandante del pelotón Dinamarca I del Ejército Nacional, adscrito al Batallón Pichinca de la tercera Brigada que está suspendido de su cargo. (Le puede interesar: ["La guerra de Colombia se retrata en la tragedia de Tumaco"](https://archivo.contagioradio.com/la-guerra-de-colombia-se-retrata-en-la-tragedia-de-tumaco/))

De acuerdo con la vice fiscal María Paulina Riveros, los uniformados deberán responder por los delitos de **homicidio agravado** y homicidio agravado tentado en calidad de autores por posición de garantes.

Esto quiere decir que los Policías y militares cometieron delitos cuando tenían que salvaguardar y proteger la **integridad y la vida de los ciudadanos**. Para la Fiscalía, este hecho era completamente evitable. Las audiencias están a la espera de fecha para realizarse en Pasto, Nariño.

###### Reciba toda la información de Contagio Radio [en su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio ](http://bit.ly/1ICYhVU) 
