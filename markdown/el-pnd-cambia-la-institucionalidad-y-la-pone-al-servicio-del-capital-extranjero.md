Title: “El PND cambia la institucionalidad y la pone al servicio del capital extranjero”
Date: 2015-04-28 18:31
Author: CtgAdm
Category: Economía, Otra Mirada
Tags: alirio uribe, Angela Maria Robledo, Congreso de la República, Hector Moncayo, ILSA, Minería en Colombia, Plan Nacional de Desarrollo, Victor Correa Vélez
Slug: el-pnd-cambia-la-institucionalidad-y-la-pone-al-servicio-del-capital-extranjero
Status: published

###### Foto: youtube 

Mientras que el **Plan Nacional de Desarrollo** que se aplicó en el primer gobierno de Juan Manuel Santos se hablaba de las locomotoras como mecanismos de inclusión social y de desarrollo, en el nuevo plan se dedica a c**ambiar la institucionalidad, como paso necesario para que la inversión extranjera** llegue de la mano de multinacionales que tienen en la extracción minera una de sus principales fuentes de ingresos, explica **Hector Moncayo** integrante de ILSA.

Según Moncayo, una de las razones del **cambio en la filosofía del PND** es la caída de los precios del petróleo, minerales e incluso los metales preciosos, así las cosas la inversión foránea es absolutamente necesaria para seguir sosteniendo los gastos del Estado que se está quedando sin fuentes de ingreso.

La otra razón es que lo consignado en el PND está acorde con la filosofía encausada por la **OCDE, el FMI y el BM**. Los temas centrales son el sistema tributario, pensional, laboral, en resumidas cuentas todas las garantías que exigen las empresas para asegurar la inversión pero también el máximo de ganancias.

Así las cosas, para enfrentar la arremetida del PND resta la movilización social para rechazarlo en su integridad o la discusión y la movilización por sectores dado que el Plan parece estar construido a pedazos y luego integrado en un documento nacional. Sin embargo, Moncayo afirma que el movimiento social en Colombia es poco político, puesto que lo global no es del interés de todos los sectores y se presta especial atención a lo que tiene que ver con los sectores específicos.

Moncayo también valora el ejercicio de audiencias públicas como ejercicio político, sin embargo es necesario que los congresistas diferentes a los ponentes sean quienes escuchen el debate público y social pero también se haga un ejercicio de control y sobre las posturas que asumen los congresistas en las diversas votaciones.

Video de la intervención de Hector Moncayo

<iframe style="border: 0; outline: 0;" src="http://cdn.livestream.com/embed/contagioradioytv?layout=4&amp;clip=pla_7ad56298-5b85-411e-9b28-9773c5199882&amp;height=340&amp;width=560&amp;autoplay=false" width="560" height="340" frameborder="0" scrolling="no"></iframe>

<div style="font-size: 11px; padding-top: 10px; text-align: center; width: 560px;">

Watch [live streaming video](http://original.livestream.com/?utm_source=lsplayer&utm_medium=embed&utm_campaign=footerlinks "live streaming video") from [contagioradioytv](http://original.livestream.com/contagioradioytv?utm_source=lsplayer&utm_medium=embed&utm_campaign=footerlinks "Watch contagioradioytv at livestream.com") at livestream.com

</div>
