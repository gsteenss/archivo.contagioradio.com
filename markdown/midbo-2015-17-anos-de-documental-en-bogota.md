Title: MIDBO 2015, 17 años de Documental en Bogotá
Date: 2015-06-16 17:51
Author: AdminContagio
Category: 24 Cuadros, eventos
Tags: Cinemateca Distrital, Corporación Colombiana de Documentalistas: Alados Colombia, Director MIDBO, IDARTES, Instituto de las Artes, Muestra Internacional Documental Bogotá, Pablo Mora, Proimágenes Colombia
Slug: midbo-2015-17-anos-de-documental-en-bogota
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/06/11377391_561418677331147_8985045285743070219_n.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<iframe src="http://www.ivoox.com/player_ek_4617161_2_1.html?data=lZuemZaadY6ZmKiakp2Jd6KokpKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRlMLWzdSYr9TWpYampJCxy9fJp9Xj05DRx5DQpYzB1srg1tfFb6ri1crf0MbHrdDiwtGYptTHuc7ZjoqkpZKns8%2FowszW0ZC2pcXd0JCah5yncZU%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

#### [Pablo Mora, Director de MIDBO 2015] 

Hasta el próximo **31 de Julio**, realizadores y productores audiovisuales locales y foráneos, podrán participar en las diferentes categorías de la **XVII Muestra Internacional Documental de Bogotá**, un espacio para la exhibición y divulgación del Cine de lo real en Colombia en diversos escenarios de la ciudad, para confirmar una vez más porque **Bogotá también es documental**.

Más de **60 producciones documentales** serán las llamadas a hacer parte de la selección oficial del evento, curadas dentro de las categorías: **Convocatoria Nacional, Convocatoria Internacional** y como novedad la inclusión de la **Convocatoria Nacional Estudiantil**, categoría con la que deacuerdo con el director de la muestra **Pablo Mora**, **"**queremos darle fuerza, y empuje a las obras de los jóvenes que están produciendo en Colombia**"**.

[![11059337\_565703080236040\_8842280480838319359\_o](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/06/11059337_565703080236040_8842280480838319359_o.png){.size-full .wp-image-10163 width="1756" height="840"}](https://archivo.contagioradio.com/midbo-2015-17-anos-de-documental-en-bogota/11059337_565703080236040_8842280480838319359_o/)

Similar a la situación que vive la producción audiovisual de ficción en nuestro país **"**El documental en Colombia a estado un poco descuidado en el punto de vista de su difusión, pero es un escenario de creación muy fuerte, desde muchos ambitos se producen muchísimas películas día a día en Colombia**"** asegura Pablo Mora.

El potencial del documental como elemento de visibilización las diferentes problemáticas que enfrentan las comunidades en el territorio nacional y en el exterior, es bien dimensionado por la organización de la muestra que busca dar cabida a las **propuestas que abordan temáticas como son la memoria, el conflicto, y el campo de acción de los movimientos sociales**, a través de las expresiones de autores que puedan mostrar esas realidades **"**de una manera no periodística**"**.

Paralelo al evento de exhibición, durante la muestra se llevara a cabo el **Seminario Internacional "Pensar en lo Real"**, un escenario de **pensamiento y de reflexión** sobre la producción cinematográfica documental, un espacio de corte académico y reflexivo que **"** aspira a generar debates y controversias sobre el campo de la creación documental en Colombia**"**, las inscripciones para presentar ponencias estan abiertas hasta el **30 de junio**.

[![11425209\_565693076903707\_2496453207222233683\_o](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/06/11425209_565693076903707_2496453207222233683_o.png){.size-full .wp-image-10166 width="1756" height="840"}](https://archivo.contagioradio.com/midbo-2015-17-anos-de-documental-en-bogota/11425209_565693076903707_2496453207222233683_o/)

La muestra, que tendrá lugar entre el **27 de octubre y el 4 de noviembre**, es organizada por la **Corporación Colombiana de Documentalistas: Alados Colombia**, en asocio con el I**nstituto de las Artes, IDARTES - Cinemateca Distrital**, la **Alcaldía Mayor de Bogotá**, el **Ministerio de Cultura** (Dirección de Comunicaciones), y **Proimágenes Colombia**,

Los interesados en hacer parte de la **Muestra Internacional Documental,** pueden encontrar los términos y bases de la convocatoria en [www.midbo.co/](http://www.midbo.co/) y si cumplen con los requisitos podrán realizar su proceso de inscripción.
