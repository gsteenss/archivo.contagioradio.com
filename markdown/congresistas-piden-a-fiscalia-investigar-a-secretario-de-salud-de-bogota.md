Title: Congresistas piden a Fiscalía investigar a Secretario de salud de Bogotá
Date: 2016-05-16 13:21
Category: Entrevistas, Judicial
Tags: crisis salud, red hospitalaria bogotá, secretario de salud
Slug: congresistas-piden-a-fiscalia-investigar-a-secretario-de-salud-de-bogota
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/05/Secretario-Salud-Bogotá.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Consultor Salud ] 

###### [16 Mayo 2016 ]

Ángela Robledo e Inti Asprilla, Representantes a la Cámara por el Partido Verde, radican este lunes una petición a la Fiscalía para que el actual Secretario de Salud del distrito, Luis Gonzalo Morales, sea **investigado por presunta negligencia en el manejo de la atención hospitalaria en Bogotá**, y puntualmente por la muerte de Ramón Montaño, quien a sus 57 años falleció esperando que la línea de emergencias 123 le enviara una ambulancia a su hogar.

Según Robledo, la decisión del Secretario de [[recortar 40% del presupuesto de salud](https://archivo.contagioradio.com/40-recorte-presupuestal-bogota-hospitales/)] para la capital y disminuir en 50% el servicio de ambulancias, alegando detrimento patrimonial, no sólo **lleva a que la red de atención hospitalaria bogotana funcione en un 10%**, sino que sentencia a los ciudadanos más pobres a seguir muriendo porque las ambulancias no son suficientes.

"Existe una disposición para los funcionarios públicos que es la diligencia, la oportunidad y la pertinencia para actuar, sabemos que es complejo el mundo de la salud hoy, pero **tenemos que hacer todo lo posible para evitar que mueran los ciudadanos**", afirma Robledo, e insiste en que las [[decisiones y declaraciones de Morales](https://archivo.contagioradio.com/reforma-hospitalaria-del-distrito-seria-el-paso-final-para-privatizacion-del-servicio-medico/)] prueban su negligencia.

Será la Fiscalía entonces la que determinará la responsabilidad del Secretario en la muerte de Ramón Montaño, teniendo en cuenta un posible incumplimiento de funciones para la garantía del derecho a la salud. En tal caso **el funcionario podría estar inmerso en el delito de homicidio por omisión**, por su irresponsable decisión de no establecer un plan de contingencia para garantizar el servicio de urgencias, según afirma Asprilla.

<iframe src="http://co.ivoox.com/es/player_ej_11548968_2_1.html?data=kpailp2depmhhpywj5aXaZS1kZ6ah5yncZOhhpywj5WRaZi3jpWah5yncaLiyMrZw5CxpYzG0MfZx8nTb46fotHWw9PepYzKxtfRx5KJe6ShpNTb1sbLrdCfs8bRy9SPcYarpJKh&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)]] 
