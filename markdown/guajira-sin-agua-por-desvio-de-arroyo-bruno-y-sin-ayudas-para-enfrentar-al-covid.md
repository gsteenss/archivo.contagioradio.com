Title: Guajira sin agua por desvío de arroyo Bruno y sin ayudas para enfrentar al COVID
Date: 2020-05-06 21:01
Author: AdminContagio
Category: Actualidad, Comunidad
Tags: arroyo Bruno, Wayúu
Slug: guajira-sin-agua-por-desvio-de-arroyo-bruno-y-sin-ayudas-para-enfrentar-al-covid
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/Arroyo-Bruno.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:heading {"align":"right","level":6,"textColor":"cyan-bluish-gray"} -->

###### Foto: Extractivismo en Colombia {#foto-extractivismo-en-colombia .has-cyan-bluish-gray-color .has-text-color .has-text-align-right}

<!-- /wp:heading -->

<!-- wp:paragraph -->

El pasado 5 de mayo el Centro de Investigación y Educación Popular (CINEP) denunció que las comunidades cercanas al Cerrejón siguen padeciendo problemas de acceso al agua debido al desvío realizado por la empresa del arroyo Bruno. Situación que se agrava en razón de la pandemia que vive el mundo, y pone de presenten la situación de desconocimiento de derechos en la que viven.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/CINEP_PPP/status/1257677775823126530","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/CINEP\_PPP/status/1257677775823126530

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:heading {"level":3} -->

### En contexto: el desvío del arroyo Bruno para el benficio empresarial

<!-- /wp:heading -->

<!-- wp:paragraph -->

Jackeline Romero Epiayú, integrante de Fuerza Mujeres Wayúu, recuerda que el desvío del arroyo Bruno hace parte del proyecto de expansión de Cerrejón que intentó el desvío total del río Ranchería, pero ante la negativa a esta acción, tomo parte del mencionado afluente. En su sentencia SU 698 de 2017 de la Corte Constitucional ordenó suspender el desvío del arroyo, pero las comunidades [denuncian](https://www.cinep.org.co/Home2/component/k2/703-comunicado-alerta-sobre-situacion-del-arroyo-bruno.html) que la empresa ha incumplido la sentencia.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Según la organización Censat Agua Viva, el arroyo Bruno es una corriente natural de agua que nace a 420 metros de altura, en la reserva natural de los montes Oca entre los municipios de Albania y Maicao en La Guajira. Se estima que su afluente provee de agua a comunidades afrodescendientes y a 34 comunidades Wayúu.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### La situación en La Guajira respecto al coronavirus

<!-- /wp:heading -->

<!-- wp:paragraph -->

Romero explica que una de las primeras preocupaciones que tuvieron las peronas cuando se decretó el inicio del aislamiento obligatorio fue que las comunidades denunciaron que siguió la llegada de buques con bandera internacional al puerto de carga del Cerrejón, lo que significó que sus operaciones no pararon. (Le puede interesar: ["Carbones del Cerrejón incumple sentencia que protege a comunidades y al Arroyo Bruno"](https://archivo.contagioradio.com/carbones-del-cerrejon-incumple-sentencia-que-protege-a-comunidades-y-al-arroyo-bruno/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

La empresa detuvo sus operaciones pero desde el 4 de mayo las está reactivando, pero ahora la preocupación manifestada por la integrante de Fuerza Mujeres Wayúu es otra: "en La Guajira no ha habido un plan de contención o de seguimiento al tema del Coronavirus", lo que para un departamento en el que solo se cuentan con 34 Unidades de Cuidados Intensivos (UCI) significa un riesgo para las comunidades.

<!-- /wp:paragraph -->

<!-- wp:quote -->

> "en La Guajira no ha habido un plan de contención o de seguimiento al tema del Coronavirus"
>
> <cite>Fuerza de Mujeres Wayuu</cite>

<!-- /wp:quote -->

<!-- wp:paragraph -->

Por esta razón, Romero reitera la preocupación de que no se levante el tamponamiento sobre el arroyo Bruno, permitiendo el acceso de comunidades Wayúu y afrodescendientes al agua, un elemento vital para combatir el Covid-19. (Le puede interesar: ["Es necesario actuar ya para salvar al Arroyo Bruno en la Guajira"](https://archivo.contagioradio.com/necesario-actuar-salvar-arroyo-bruno-guajira/))

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->
