Title: Atentado contra candidato a la alcaldía por la UP en Palmitos, Sucre
Date: 2015-09-09 12:40
Category: DDHH, Nacional
Tags: montes de maría, Palmitos, paramilitares, paramilitares y FFMM, Sucre, Unión Patriótica
Slug: atentado-contra-candidato-a-la-alcaldia-por-la-up-en-palmitos-sucre
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/UP.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: UP Sucre] 

<iframe src="http://www.ivoox.com/player_ek_8241672_2_1.html?data=mZehk5ubdo6ZmKiakpWJd6KkkZKSmaiRdI6ZmKiakpKJe6ShkZKSmaiRhdXZz9nOxtSPp9Di1dfOjcjFssXdxcbh0ZDFb83VjMbZxcbQqIa3lIqupsaPtNDmjNHOjbq0b8bijLWah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Aida Abella, UP] 

###### [9 Sept 2015] 

[Hugo Sánchez, candidato por la U.P a la alcaldía del municipio de Palmitos, Sucre, fue víctima de un atentado cuando se movilizaba hacia el corregimiento el Naranjal, el carro en el que se movilizaba fue  ]**[interceptado por 8 disparos, 2 de los cuales impactaron en el chaleco antibalas del candidato,]**[el hecho ]**sucedió[ ]**[en la mañana del pasado martes 8 de septiembre.   ]

[Aida Abella, directora de la UP, afirma que este hecho es alarmante, **las estructuras paramilitares, sin importar como se les nombre, siguen cooptando los negocios y la política en Sucre**, permeando la vida cotidiana y los espacios públicos en toda la región, **con la anuencia tanto de las autoridades departamentales como de los organismos policiales y militares**.]

[Es evidente la **falta de voluntad política de las esferas públicas de poder para hacer frente al accionar paramilitar en todo el territorio nacional**, continúan las amenazas, hostigamientos y montajes judiciales contra reclamantes de tierras y candidatos por la UP en departamentos como Cauca, Caquetá, Chocó y Arauca, asegura  Abella]

[La directora de la UP, reitera la **exigencia al gobierno nacional de condiciones que garanticen el efectivo ejercicio político de la UP**, así como su libre movilización en zonas de álgido conflicto e influencia paramilitar como los Montes de María.]

[En el  actual contexto de desescalamiento del conflicto, es urgente la desarticulación de las estructuras paramilitares, pese a que se firmen los diálogos en La Habana el accionar paramilitar puede recrudecer la violencia, agrega Abella e insiste en que **las autoridades tienen que decidirse en defender la vida de los ciudadanos o la de los criminales**.]
