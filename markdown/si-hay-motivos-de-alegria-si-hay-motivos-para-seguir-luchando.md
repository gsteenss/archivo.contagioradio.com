Title: Sí hay motivos de alegría, sí hay motivos para seguir luchando
Date: 2016-06-29 09:26
Author: JOHAN MENDOZA TORRES
Category: Johan Mendoza, Opinion
Tags: Movilización, Paro Agrario
Slug: si-hay-motivos-de-alegria-si-hay-motivos-para-seguir-luchando
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/06/la-minga-es-alegria-7-.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Congreso de los Pueblos 

#### **Por:[Johan Mendoza Torres](https://archivo.contagioradio.com/johan-mendoza-torres/)** 

###### 29 Jun 2016

[Se fueron para el carajo, aquellos y aquellas que jugaron y siguen jugando con las emociones de la sociedad colombiana ocultando intereses politiqueros. Al centro democrático lo dejó el bus de la historia, quedaron desmantelados su odio, la emisión de discursos cínicos y esa costumbre de hablar de impunidad olvidando toda la corrupción e injusticia vividas durante la presidencia de ese tal Uribe.]

[Se fueron para el carajo todos esos mamertos de izquierda que criticaron a las Farc por querer acabar la guerra, cuando de seguro no caminarían ni medio kilómetro por las selvas pendientes de no ser bombardeados por la Fuerza Aérea. Se fueron para el carajo todos esos mamertos de derecha que apoyan al uribismo y su cinismo, sin saber nada más allá que los meta-relatos fetichistas de esa tal Gurisatti.]

[Hay motivos de alegría porque acabar una guerra, es acabar una guerra.  Los que han luchado alguna guerra, saben que absolutamente todos los escenarios son mejores que aquel infierno que llaman guerra. Los que saben un poco de historia, pueden corroborar la sabiduría de un guerrero en paz. Acabar una guerra es un motivo de alegría, definitivamente, quien no la sufre no se alegra.]

[Hay motivos de alegría porque las ruedas de la historia están girando, y han demostrado que la lucha armada debe dar paso al engrosamiento de otro tipo de lucha social en el país. Por supuesto no existe ni rastro de ingenuidad al creer que el fin de la guerra entre las Farc y el Estado Colombiano significa que el país se arregló o que todo será paz y felicidad, ¡no! eso no es el motivo de la alegría, porque entonces ¿qué le diríamos a las víctimas de los movimientos sociales, a los civiles? ¿qué diríamos de tanta injusticia que se seguirá viviendo en Colombia con guerra o sin guerra?... no obstante, el motivo de la alegría es que al desenfocar la atención del conflicto armado que se presentaba entre Farc y Gobierno significa que aquellas personas que siempre lucharon desde los movimientos sociales, aquellas que nunca tomaron las armas, pero continúan persistiendo en la fiel y noble idea de construir una nueva Colombia, ahora serán más que protagonistas.]

[Sí hay motivos para seguir luchando, porque el fin del conflicto armado, dejará claro que el problema de Colombia nunca fueron los guerrilleros; descubriremos poco a poco, con paciencia, que el problema es y siempre fue el modelo impuesto a la sociedad por ese grupo de privilegiados que se suben el sueldo por decreto inderogable.  ]

[A lo mejor sabremos que nunca fueron malos aquellos descritos por la prensa, sino que fueron buenos los que aún sin armas, siguen arrancando la máscara del sistema liberal que continúa empujando a Colombia hacia el fracaso social.]

[Sí hay motivos para seguir luchando, porque queda toda la transformación, esa lucha que nunca han dejado atrás los movimientos sociales, aquellos movimientos repletos de gente que quiere decir algo, actuar y caminar de frente, sabiendo que aún así no haya guerra, algunos privilegiados muy poderosos seguirán inventando los motivos para frenar lo que no se puede frenar.  ]

[Sí hay motivos para seguir luchando, porque la conciencia que existe en el campo, tiene que migrar a las ciudades, tiene que despertar a los más cautivos, a los más esclavos, a los más indiferentes… hay motivos para seguir luchando, porque la bella noticia del fin de una guerra, es el triunfo de la necesidad de cambio, y el cambio, no vendrá empaquetado en formas teóricas tan visibles para los eruditos o defendidas a capa y espada por la ortodoxia, pero tan esquivas con la gente que no pide explicaciones sino acción y justicia, gente que no pide más enemigos, sino unión contra esta falsa democracia.]

[Los movimientos sociales, emergerán por encima de los partidos, emergerán por encima de las multinacionales, y demostrarán que no hay marcha atrás, que ahora que la lucha armada apaga sus hogueras, del otro lado, la gente en paz, encenderá sus corazones y alistará sus cuerpos para las calles, para las carreteras… entonces, ya no habrá más tristeza… los asesinados en las mingas, en los paros, sabrán que sus muertes no fueron en vano y nos acompañaran siempre, porque lo que es justo no puede morir.   ]

[Qué felicidad que se acabe una de tantas guerras que hay en el país…]

[¡sólo una! (Dirán algunos) …]

[¡una es una! (Diremos nosotros).]

------------------------------------------------------------------------

###### [Las opiniones expresadas en este artículo son de exclusiva responsabilidad del autor y no necesariamente representan la opinión de la Contagio Radio.] 
