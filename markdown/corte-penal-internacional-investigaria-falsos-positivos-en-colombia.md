Title: Corte Penal Internacional investigaría "falsos positivos" en Colombia
Date: 2016-11-16 12:11
Category: DDHH, Nacional
Tags: Falsos positivos extrajudiciales, impunidad en Colombia, Victimas falsos positivos
Slug: corte-penal-internacional-investigaria-falsos-positivos-en-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/11/Falsos-positivos.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [16 Nov 2016] 

La Corte Penal Internacional emitió su informe sobre posibles investigaciones de crímenes de guerra, en el texto hay un capítulo dedicado a Colombia en donde evidencia la falta de avances en investigación de crímenes como las ejecuciones extrajudiciales o los mal llamados “falsos positivos”. De acuerdo con la información recolectada actualmente hay  **817 sentencias condenatorias contra 961 miembros de las Fuerzas Armadas por este delito.**

En el documento también se señala que hasta **julio de este año se investigaban 2.241 casos de asesinatos extrajudiciales, que deja un total de 4.190 víctimas en Colombia**. Sin embargo más del **98%** de los crímenes cometidos por agentes del Estado quedan en la impunidad, de los que un **84%** han sido atribuidos a paramilitares o miembros de las Fuerzas Armadas del país. Le puede interesar:["Organizaciones de víctimas piden audiencia ante CIDH y rechazan propuestas del No"](https://archivo.contagioradio.com/organizaciones-de-victimas-piden-audiencia-ante-cidh/)

La Corte Penal Internacional tendría por lo menos **5 posibles casos de asesinatos extrajudiciales cometidos entre los años 2002 al 2010**, por miembros de las Fuerzas Armadas, pertenecientes a once brigadas diferentes, que darían inicio a las investigaciones.

A su vez, la Corte también informa que habría identificado a un grupo de comandantes a cargo de estás brigadas y divisiones, bajo **“cuyo mando se habrían presuntamente cometido la mayor cantidad de falsos positivos”**. Por otro lado la Corte también señala la importancia y la colaboración de autoridades colombianas sobre la recolección de información, no obstante indicó que “los pasos investigativos exactos son limitados” y que no han recibido información detallada sobre casos adelantados contra comandantes por estos crímenes. Le puede interesar:["Víctimas se darán cita en la Fiscalía para tejer memoria"](https://archivo.contagioradio.com/victimas-se-daran-cita-en-la-fiscalia-para-tejer-la-memoria/)

De igual forma en el documento también se exponen los resultados frente delitos como desplazamiento forzado en el país, frente a este tema se evidencia que los Tribunales de Justicia y Paz solo han  dictado **10 condenas contra 43 miembros de grupos paramilitares**, entre los que figura Ramón Isaza quién comando las Autodefensas Campesinas del Magdalena Medio.

El paso siguiente es que la Corte Penal Internacional entrará a realizar la recolección de pruebas y el estado de las investigaciones, para acogerlas como investigaciones propias y finalmente **dictaminar sentencias frente a crímenes de guerra, crímenes de lesa humanidad, genocidios o violaciones a los derechos humanos.** Le puede interesar:["Informe sobre las actividades de examen preliminar de la CPI"](https://www.icc-cpi.int/iccdocs/otp/161114-otp-rep-PE-Colombia.pdf)

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)

<div class="ssba ssba-wrap">

</div>
