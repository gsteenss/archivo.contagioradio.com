Title: 30 años con la esperanza de encontrarla viva
Date: 2016-01-27 11:15
Category: Sin Olvido
Tags: Desaparición forzada, María Lyda Mondol, Palacio de Justicia
Slug: 30-anos-con-la-esperanza-de-encontrarla-viva
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/01/maria-lyda-mondol_31.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Fotos: Contagio Radio. 

###### [27 Ene 2016.]

[“Mami, siempre vivirás en nuestros corazones, tus ideales serán los nuestros”, con estas palabras Liliana Palacios, una de las cuatro hijas de **María Lyda Mondol, auxiliar del magistrado Mauricio Gaona Cruz que murió incinerada durante la retoma del Palacio de Justicia en noviembre de 1985**, inició el sentido discurso con el que los familiares y amigos más cercanos rindieron homenaje y sepultaron dignamente los restos óseos de una de las mujeres víctimas de esta tragedia.]

[![maria lyda mondol\_51](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/01/maria-lyda-mondol_51.jpg){.aligncenter .size-full .wp-image-19455 width="1000" height="667"}](https://archivo.contagioradio.com/30-anos-con-la-esperanza-de-encontrarla-viva/maria-lyda-mondol_51/)

[“Siempre recordaremos los maravillosos momentos que nos regalaste y las enseñanzas que nos brindaste. Seguiremos siempre tus consejos y te honraremos”, concluyó Liliana, con la voz entrecortada y algunas de las lágrimas que había contenido **durante los 30 años en que mantuvo la esperanza de que su madre estuviera viva en cualquier lugar** y en los que nunca descartó la posibilidad de que María Lyda apareciera viva.]

[La espera terminó en octubre del año pasado cuando un funcionario de La Fiscalía comunicó a Carolina Palacios, la hija menor de la familia, que **los restos depositados en la tumba de la magistrada Blanca Inés Ramírez correspondían a los de su madre**. Pese a lo desgarradora que fue la noticia, Carolina no dudó en comunicarla a sus hermanas, con quienes se reencontraría luego de 10 años en los que acortaron las largas distancias entre sus hogares con frecuentes llamadas telefónicas.]

[![maria lyda mondol\_8](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/01/maria-lyda-mondol_8.jpg){.aligncenter .size-full .wp-image-19451 width="1000" height="667"}](https://archivo.contagioradio.com/30-anos-con-la-esperanza-de-encontrarla-viva/maria-lyda-mondol_8/)

[“**El año pasado compartimos la triste noticia de recordar la presente ausencia de mamá a través de Carolina**, nuestra hermana menor, por quien sentimos un gran cariño y admiración, porque fue ella quien asumió la difícil tarea de afrontar el doloroso pasado, y de paso, hacer realidad este esperado encuentro entre nosotras”, narró una de las hijas María Lyda.]

[![maria lyda mondol\_28](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/01/maria-lyda-mondol_28.jpg){.aligncenter .size-full .wp-image-19452 width="1000" height="667"}](https://archivo.contagioradio.com/30-anos-con-la-esperanza-de-encontrarla-viva/maria-lyda-mondol_28/)

[**La familia Mondol Palacios se enfrentó al duelo que nunca imaginaron vivir, al ciclo de búsqueda que no habían podido cerrar** y al despertar de emociones que les permite estar más tranquilas por haber hallado, aunque sin vida, a María Lyda, la madre de la que recuerdan  los fines de semana en los que iban a los barrios más empobrecidos de Ciudad Bolívar, a compartir un vaso de leche o un roscón con niños que se alegraban al recibir parte de la calidad humana de esta mujer y sus hijas.]

[![maria lyda mondol\_46](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/01/maria-lyda-mondol_46.jpg){.aligncenter .size-full .wp-image-19454 width="1000" height="667"}](https://archivo.contagioradio.com/30-anos-con-la-esperanza-de-encontrarla-viva/maria-lyda-mondol_46/)

[30 años después, las hermanas Mondol Palacios estaban frente a los restos de su madre asegurando que pese a que “la muerte es uno de esos sucesos que generan tristeza, nostalgia, amargura, desesperanza, desolación y soledad” han aprendido que “una persona muere solamente cuando se olvida”, así como **la historia nacional ha olvidado hacer memoria de todas las víctimas de aquellos hechos trágicos que rodearon la toma y retoma del Palacio de Justicia entre el 6 y 7 de noviembre de 1985**.]

[![maria lyda mondol\_117](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/01/maria-lyda-mondol_117.jpg){.aligncenter .size-full .wp-image-19468 width="1000" height="623"}](https://archivo.contagioradio.com/30-anos-con-la-esperanza-de-encontrarla-viva/maria-lyda-mondol_117/)

[Y es que **mientras las hermanas Mondol Palacios experimentan una suerte de alivio** por haber sepultado dignamente los restos de María Lyda, **hay familias que desde el pasado octubre quedaron desconcertadas**.** **Los cuerpos que habían inhumado creyendo que eran los de sus víctimas eran la muestra de los graves errores de la institucionalidad colombiana, “un pecado social que no fue cometido por una sola persona”, como afirmó el sacerdote Alberto Franco, durante la ceremonia religiosa.     ]

[Fue un acto litúrgico al que asistieron familiares de algunas de las **víctimas del Palacio de Justicia que hoy 30 años después continúan desaparecidas y por las que sus familias se han unido**, exigiendo la entrega de los restos fúnebres de aquellos hermanos, hijos, esposos, madres o padres que salieron de sus casas a trabajar, pero que nunca regresaron. Muertes frente a las que reclaman el **esclarecimiento de la verdad, para sanar heridas y construir una sociedad nueva**, en la que el dolor sea convertido en fuerza y las víctimas junto a sus familias sean dignificadas.]

[![maria lyda mondol\_80](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/01/maria-lyda-mondol_80.jpg){.aligncenter .size-full .wp-image-19458 width="1000" height="667"}](https://archivo.contagioradio.com/30-anos-con-la-esperanza-de-encontrarla-viva/maria-lyda-mondol_80/)

[Otro de los signos de paz manifiestos en la ceremonia fue la presencia de Rene Guarín, ex-integrante del M-19 y hermano de Cristina del Pilar Guarín, desaparecida durante la retoma al Palacio, quien con un saludo fraterno a las hermanas Mondol Palacios, evidenció que **es posible que los familiares de las víctimas, sin importar las procedencias, se encuentren en un escenario de solidaridad**, una muestra de la reconcialiación desde las propias víctimas, una lección al país, a los ciudadanos y a los mismos actores de la guerra.]

[![maria lyda mondol\_102](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/01/maria-lyda-mondol_102.jpg){.aligncenter .size-full .wp-image-19460 width="1000" height="667"}](https://archivo.contagioradio.com/30-anos-con-la-esperanza-de-encontrarla-viva/maria-lyda-mondol_102/)

[Demandas que las familias de las víctimas dirigen al Estado colombiano para que se haga justicia y **todas aquellas personas que fueron responsables de las muertes de quienes salieron con vida del Palacio y luego fueron reportadas como muertas, sean sancionadas**. “Que se desenrede todo este lío que se ha mantenido durante estos 30 años, que se esclarezca la verdad y que de alguna manera todo lo que hemos sufrido sea recompensado, porque todo lo que nosotras padecimos económica y psicológicamente no tiene precio”, aseveró Liliana Palacios.]

[![maria lyda mondol\_101](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/01/maria-lyda-mondol_101.jpg){.aligncenter .size-full .wp-image-19467 width="1000" height="667"}](https://archivo.contagioradio.com/30-anos-con-la-esperanza-de-encontrarla-viva/maria-lyda-mondol_101/)

[Este homenaje de esperanza y gratitud, que no fue un adiós sino un hasta pronto, finalizó en el Cementerio Jardines de Paz con la inhumación de los restos óseos de María Lyda Mondol, como semillas que se depositaron con cuidado en la tierra, con la esperanza de que fructifiquen vida, la vida que las cuatro hermanas Mondol Palacios pretenden honrar y dignificar, **persistiendo en la reclamación de verdad, justicia y reparación integral, no sólo en el caso de su madre sino en los de los cientos de víctimas en Colombia**.]

###### [Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio ](http://bit.ly/1ICYhVU)]] 
