Title: "El Silencio ya no es opción", un grito a la defensa de la vida de los líderes sociales
Date: 2020-07-26 11:33
Author: CtgAdm
Category: Actualidad, DDHH
Slug: el-silencio-ya-no-es-opcion-un-grito-a-la-defensa-de-la-vida-de-los-lideres-sociales
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/Líderes-Somos-Defensoers.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: Somos Defensores

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El 26 de julio del 2019 se realizó una movilización en protesta para la protección de la vida de las lideresas, líderes sociales y firmantes de paz, un año después organizaciones plataformas y territorios nacionales e internacionales se unen en la campaña **\#NoMasSilencio.**

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/ONIC_Colombia/status/1287196025669726208","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/ONIC\_Colombia/status/1287196025669726208

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

Una acción de protesta y defensa de la vida en medio de una situación que dificulta salir nuevamente a las calles, **pero que exige la protección a la vida de los líderes sociales en Colombia**, así como también, a la implementación de los planes de desarrollo con enfoque territorial (PDET), y pide garantías de vida para los y las firmantes del Acuerdo de Paz, junto con un acto de memoria a los defensores y defensoras de la vida que han sido asesinados.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Y que según el [informe más reciente de Indepaz](https://archivo.contagioradio.com/971-defensores-de-dd-hh-y-lideres-han-sido-asesinados-desde-la-firma-del-acuerdo-de-paz/)datos desde el 24 de noviembre de 2016 hasta el 15 de julio del 2020 ,señala que desde la firma del Acuerdo de Paz **han sido asesinados 971 líderes y personas defensoras de los Derechos Humanos**, entre ellas 131 mujeres; 250 indígenas; 13 integrantes de la comunidad LGBT+ , 21 afrocolombiano y 251 excombatientes de FARC.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

**Acciones que no han cesado y que fragmentan los tejidos sociales en los territorios** , por ello [Defendamos la Paz](http://defendamoslapazcolombia.com), reiteró su compromiso con la defensa de la vida con una iniciativa virtual, que refuerce el grito de hace un año en la jornada por la vida, *" **Aquí nadie se puede cansar. Bienvenidas todas las voces todas las manos huellas** **y caminos que conduzcan a preservar la vida de nuestro liderazgo sociales** y firmantes de la paz que confiaron en la palabra del Estado colombiano, y hoy siguen apostando a la paz".*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Acción en la que además llaman a la comunidad a unirse por medios de tres tipos de acciones, primera, visibilizar la historia y las luchas de las personas líderes y firmantes del Acuerdo de Paz; segundo, acompañar esto por medio de expresiones artísticas que resalten el valor de la vida de los liderazgos sociales; y tercero, con el **\#NoMásSilencio** acompañar estas expresiones simbólicas a través de una gran movilización virtual.

<!-- /wp:paragraph -->

<!-- wp:image -->

<figure class="wp-block-image">
![Imagen](https://pbs.twimg.com/media/Ed2z91FXoAItTz7?format=jpg&name=medium)  

<figcaption>
Conozca la agenda y sumase a esta iniciativa, en un llamado continuo a la paz y la defensa de la vida.  
  

</figcaption>
</figure>
<!-- /wp:image -->

<!-- wp:core-embed/facebook {"url":"https://www.facebook.com/contagioradio/videos/1400891780099615","type":"video","providerNameSlug":"facebook","className":""} -->

<figure class="wp-block-embed-facebook wp-block-embed is-type-video is-provider-facebook">
<div class="wp-block-embed__wrapper">

https://www.facebook.com/contagioradio/videos/1400891780099615

</div>

<figcaption>
Le puede interesar: Otra Mirada: Construir paz en los territorios

</figcaption>
</figure>
<!-- /wp:core-embed/facebook -->

<!-- wp:block {"ref":78955} /-->
