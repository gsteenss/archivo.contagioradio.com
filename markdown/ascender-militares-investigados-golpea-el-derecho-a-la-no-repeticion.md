Title: Ascender militares investigados golpea el derecho a la no repetición
Date: 2019-12-20 18:45
Author: CtgAdm
Category: DDHH, Política
Tags: Ascenso de militares, Congreso de la República, Violación a DDHH en colombia
Slug: ascender-militares-investigados-golpea-el-derecho-a-la-no-repeticion
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/Ascensos.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/Ascensos-Militares.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: @honohenriquez] 

Mientras en la madrugada del viernes la Cámara de Representantes aprobaba la Reforma Tributaria, el Senado votó por unanimidad a favor de 40 ascensos de miembros de la Fuerza Pública, tres de ellos implicados en investigaciones por violaciones a los derechos humanos relacionadas con ejecuciones extrajudiciales. El congreso aprobó los ascensos pese a las advertencias e información recolectada por diversas organizaciones defensoras de DD.HH.

Según la abogada Diana Salamanca, del Movimiento Nacional de Víctimas de Crímenes de Estado (MOVICE), las hojas de vida que fueron estudiadas por la organización, señalando que este acto es una afrenta contra las víctimas porque niegan el derecho a las garantía de no repetición. [(Lea también:Nueve comandantes del Ejército estarían implicados en falsos positivos: Human Rights Watch)](https://archivo.contagioradio.com/nueve-comandantes-del-ejercito-estarian-implicados-en-falsos-positivos-human-rights-watch/)

Sobre los militares cuestionados, según documentos de la Fiscalía, el general **Juan Carlos Ramírez** tiene 9 procesos en su contra. Tres de las investigaciones son por homicidio en persona protegida, uno por fraude a resolución judicial,  tráfico de influencias a favor de servidor público y falsedad ideológica en documento público. Por su parte, desde el MOVICE advierten que bajo el mando del general **Sergio Alberto Tafur** se cometieron por lo menos 16 ejecuciones extrajudiciales.

Para el caso del entonces coronel **Hernando Garzón Rey**, es mencionado en una ejecución extrajudicial, además es conocido que la capitana Maritza Soto interpuso una denuncia en la Procuraduría, por acoso laboral y sexual sucedidos en Convención, Norte de Santander, la denunciante, además explicó que reportó esta situación ante el jefe del Estado Mayor de la Fudra Nº 3, el comandante de la Fuerza de Tarea Vulcano y el comandante de la Segunda División, así como el comandante del Ejército, el general Nicacio Martínez, pero no recibió apoyo.

### Estos ascensos son una ofensa contra las víctimas

La abogada agrega que los ascensos además son una muestra de cómo desde la Comisión Segunda del Senado no tiene en cuenta la información que por años se ha recopilado desde las organizaciones defensoras de DD.HH. con relación al accionar militar. [(Lea también: Con pintura, Ejército pretende ocultar responsabilidad de altos mandos en 'Falsos Positivos'](https://archivo.contagioradio.com/con-pintura-ejercito-pretende-ocultar-la-verdad-de-los-falsos-positivos/))

Explica que antes de la aprobación de estos ascensos se solicitó a entidades como Fiscalía o Ministerio de Defensa, realizar un estudio sobre estos casos, con respecto al caso 003 vinculado a “Muertes ilegítimamente presentadas como bajas en combate por agentes del Estado” para saber si algunos de los nombres investigados estaban incluidos en este informe que se presentaron a la Jurisdicción Especial para la Paz y si ya en dado caso fueron llamados a rendir versiones, sin embargo no existió respuesta.

La abogada Salamanca hace énfasis en que debe existir una modificación a la manera en que se estudian estos ascensos y "que se reforme un procedimiento público que garantice el derecho de víctimas y organizaciones en armonía con el Acuerdo de Paz" y así se brinden garantías reales frente a estas decisiones y exista una mayor veeduría por parte de la ciudadanía.    [(Le puede interesar: Duplicar resultados a toda costa, la orden a militares en el gobierno Duque)](https://archivo.contagioradio.com/duplicar-resultados-a-toda-costa-la-orden-a-militares-en-el-gobierno-duque/)

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe><iframe id="audio_45775726" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_45775726_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
