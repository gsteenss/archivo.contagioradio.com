Title: Historia, verdad y "pifias"
Date: 2016-04-18 12:53
Category: Cesar, Opinion
Tags: Comisión de Historia del conflicto y sus víctimas Colombia, Historia del conflicto, renan vega, Verdad historica
Slug: historia-verdad-y-pifias
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/04/historia-conflicto.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: FARC-EP 

#### Por [César Torres Del Río ](https://archivo.contagioradio.com/cesar-torres-del-rio/) 

###### 18 Abr 2016 

Hacer historia - en el sentido de investigarla, interpretarla y darla a conocer - es un asunto complejo; cada objeto de estudio que se crea por el científico social tiene múltiples aristas, vetas, para trabajar dentro de lo multiforme de lo Real, como algo independiente del Ser. El “oficio” del historiador, como se le conoce, requiere del trabajo con sus fuentes - la materia prima -, con los “documentos” verbales o escritos, primarios y secundarios; con la memoria individual y colectiva, “trasmitida” y vivida. Y las fuentes son siempre fruto de la selección: entre varias el historiador escoge; esto es el ABC.

[La finalidad principal del hacer historia radica en obtener la Verdad; una verdad que sea, al mismo tiempo, objetiva, parcial y relativa. En este sentido no hay fisura por la que se pueda colar la invención, la ficción; esta es asunto de los literatos (que, lógico, se apoyan y aprehenden la realidad en forma de novela, de cuento y otras variantes). No obstante, historiadores y literatos interactúan constantemente para sus producciones intelectuales. Las mejores de éstas últimas, sin desconocer los respectivos aportes de otras corrientes, son las que parten desde lo glocal, de lo universal-particular, para ampliar el universo de la construcción del conocimiento histórico. La Verdad se coloca, así, en el terreno de lo científico.]

[Pero en la construcción de la Verdad histórica el Leviatán siempre levanta obstáculos; por eso el oficio del historiador entraña peligros, incluso mortales. Lo que más perjudica al Estado es la verdad (sobre la corrupción, sobre el despojo de tierras, sobre los falsos positivos, sobre el modelo extractivista, sobre la intervención norteamericana …); por eso niega el acceso a los archivos, lo limita, expide leyes para que las fuentes sólo se puedan consultar después de 30-40-50 años, quema los documentos, aceita la maquinaria burocrática y utiliza a los medios de comunicación para sus fines. En pocas palabras, es enemigo de la verdad y aplica a fondo el entero mecanismo de su racionalidad instrumental: comercial, bancaria, diplomática, judicial, mediática ...]

[En la historia no hay]*[una]*[  verdad; hay]*[verdades]*[ que amplían el conocimiento de un objeto de estudio determinado; el de las Violencias en Colombia, por ejemplo. Una]*[ única]*[ verdad corresponde al Mito; a los proyectos totalitarios (estalinista y nazista), al de las democracias (autoritarias y excluyentes) modernas, al proyecto de la]*[Ilustración]*[, bajo el cual se cubren partidos políticos de izquierda - Polo Democrático - y  las guerrillas FARC  y  ELN. Las]*[verdades]*[ expresan enfoques, una visión del mundo (]**Weltanschauung**[), interpretaciones, valores, creencias y demás. Ello no significa que haya tantas verdades cuantos investigadores sociales practiquen el oficio (“cada persona es un mundo” se dice), lo cual nos colocaría en el]*[relativismo]*[ anti-científico que niega cualquier posibilidad de verdad (la escuela posmoderna, por ejemplo).]*[La Verdad, así, es un asunto del presente]*[; siempre, siempre, quien aborda la historia (del presente y del pasado) lo hace desde su experiencia en el presente; de allí que toda historia sea siempre política, contemporánea (Antonio Gramsci). La historia es, como ya se sabe, un campo de batalla.]

[Al respecto mencionemos el caso de la Comisión Histórica del Conflicto y sus Víctimas. Creada en 2015  por las partes en negociación en La Habana, se integró con 14 investigadores y dos relatores (que oficiaron como investigadores) produciendo 16 ensayos de tipo histórico. El estudio del historiador Renán Vega fue titulado “Injerencia de los Estados Unidos, contrainsurgencia y terrorismo de Estado”; en nuestro criterio, y considerando el escaso tiempo para elaborarlo, un buen trabajo de investigación e interpretación y un notable acercamiento a la Verdad histórica. Pero inmediatamente fue tildado de “pifia” por algunos medios de comunicación y por uno o dos columnistas; adujeron que Vega no tenía cómo sustentar su afirmación de que 53 niñas fueron abusadas sexualmente por mercenarios norteamericanos en Melgar y Girardot. En otros términos, que había habido invención, que no tenía fuentes en las cuales basarse.]

[La pifia es tanto de los medios de comunicación como del bloguero y el columnista, que sólo denostan pero no argumentan. Vega cita su fuente, que es lo que hace el historiador responsable. La investigación]*[judicial]*[, si se hace, demostrará si la fuente citada está en lo cierto; la verdad histórica es bien diferente de la verdad judicial. En el medio académico y científico-social de Colombia el texto del profesor de la Universidad Pedagógica sigue siendo considerado serio, honesto, profesional y científico.]

**ADENDA**

[Lamentable la derrota de la presidenta de Brasil Dilma Rousseff,  en la Cámara de Diputados. Ahora su caso pasará al Senado, instancia en la que podrían aprobar iniciarle un juicio político que la apartaría del primer cargo político del país. La derecha brasilera está a la a ofensiva y quiere gobernar directamente. Los trabajadores y sectores populares, los pobladores de barrios, los marginados han perdido; la dirigencia del Partido de los Trabajadores había dejado de representarlos. En Brasil, y también en Venezuela, los gobiernos del “progresismo social-liberal” han terminado.]

------------------------------------------------------------------------

###### Las opiniones expresadas en este artículo son de exclusiva responsabilidad del autor y no necesariamente representan la opinión de la Contagio Radio. 
