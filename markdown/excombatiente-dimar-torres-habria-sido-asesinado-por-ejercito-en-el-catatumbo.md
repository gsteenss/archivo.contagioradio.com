Title: Señalan a Ejército Nacional de asesinar al excombatiente Dimar Torres
Date: 2019-04-23 14:36
Author: CtgAdm
Category: DDHH, Nacional
Tags: asesinato de excombatientes, Cauc, El Catatumbo
Slug: excombatiente-dimar-torres-habria-sido-asesinado-por-ejercito-en-el-catatumbo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/Dimar-Torres.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: @BenedictoFARC] 

Habitantes de la vereda Carrizal, en el municipio de Convención, Norte de Santander denuncian el asesinato del excombatiente de Farc en proceso de reincorporación, **Dimar Torres quien habría sido ejecutado extrajudicialmente a manos de integrantes del Ejército Nacional el pasado 22 de abril**, una situación que continúa en investigación pero que reflejaría que la alta militarización en el Catatumbo no garantiza la seguridad de la región ni de sus habitantes.

Según relata **Juan Carlos Quintero, directivo de la Asociación Campesina del Catatumbo (ASCAMCAT)**, sobre las 5:00 de la tarde los pobladores de Carrizal escucharon varios disparos, tras percatarse que todos estaban bien, notaron la ausencia del campesino,  la comunidad se dirige a la base militar, preguntan a integrantes del Ejército sobre el paradero de Torres e ingresan a las instalaciones y encuentran "la apertura de una fosa común donde integrantes de las Fuerzas Militares se aprestaban a ocultar el cuerpo de Dimar", ante la escena, la comunidad encara a los militares y custodian el cuerpo toda la noche impidiendo que fuera movilizado.

Los habitantes procedieron a contactar diversas organizaciones de DD.HH. quienes se comunicaron con las fuerzas militares y acordaron que una delegación de la Fiscalía y la oficina de misión de la ONU arribara al lugar para realizar la investigación de lo sucedido,  la que aún sigue en desarrollo,  sin embargo preocupa el accionar del Ejército frente a los hechos, "se trata de una desproporcionada militarización que ha demostrado no garantizar la vida de las comunidades, ni de los líderes sociales, ni de los excombatientes en proceso de reincorporación", afirma el dirigente de ASCAMCAT. [(Lea también: Samuel David, hijo de excombatientes fue víctima de la guerra con tan solo siete meses) ](https://archivo.contagioradio.com/samuel-david-hijo-de-excombatientes-fue-victima-de-la-guerra-con-tan-solo-siete-meses/)

> "Son practicas que pensábamos estaban eliminadas pero algunos miembros de la Fuerza Pública no están garantizando con su deber de proteger la vida"

"Son practicas que pensábamos estaban eliminadas pero algunos miembros de la Fuerza Pública no están garantizando con su deber de proteger la vida" indica el directivo quien se cuestiona sobre la alta presencia de miembros del Ejército en la zona y a pesar de ello, continúan presentándose asesinatos contra líderes sociales o "masacres en los cascos urbanos a plena luz del día" formula haciendo referencia a la masacre de El Tarra en agosto del 2018.

Dimar, excombatiente de las Farc se encontraba en proceso de reincorporación y según información aportada a ASCAMCAT estaba próximo a recibir su proyecto de renovación de la Agencia Colombiana para la Reintegración información, además hacía parte de la Junta de Acción Comunal de la vereda Campo Alegre.

##### Representante de la Vereda Campo Alegre narra lo sucedido:

<iframe src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/610366512&amp;color=%23ff5500&amp;auto_play=false&amp;hide_related=false&amp;show_comments=true&amp;show_user=true&amp;show_reposts=false&amp;show_teaser=true" width="100%" height="166" frameborder="no" scrolling="no"></iframe>

### Continúan las amenazas contra campesinos en Catatumbo

ASCAMCAT denuncia a su vez que el pasado 20 de abril,  el directivo de **ASCAMCAT Olger Antonio Pérez Quintero,  Nicolás López parte del Comité de Integración Social del Catamtumbo y Horacio Ramírez**, vicepresidente de la Junta de Acción Comunal de La Torcoroma fueron hostigados por miembros del Ejército cuando regresaban a sus hogares mientras cruzaban el puesto militar de la vereda San Luis, los militares procedieron a disparar contra los campesinos quienes tuvieron que resguardarse del fuego, mientras gritaban a los efectivos que eran personas civiles.

Quintero señala que la militarización incide de forma negativa en el Catatumbo pues recrudece el conflicto "estos últimos acontecimientos nos hacen preguntar si la **FUDRA 3 (Fuerza de Despliegue Rápido) ** piensa reencarnar en la temida Brigada Móvil 15 que fue desintegrada cuando se descubrió la práctica de ejecuciones extrajudiciales"  agregando que la situación que se vive en el Cauca es una alarma para las víctimas y para la comunidad internacional.

> [\#DENUNCIA](https://twitter.com/hashtag/DENUNCIA?src=hash&ref_src=twsrc%5Etfw) ataque el 20 de abril Comisión de líderes ascamcat [\#Cisca](https://twitter.com/hashtag/Cisca?src=hash&ref_src=twsrc%5Etfw) y lider comunal en [\#SanCalixto](https://twitter.com/hashtag/SanCalixto?src=hash&ref_src=twsrc%5Etfw) x [@COL\_EJERCITO](https://twitter.com/COL_EJERCITO?ref_src=twsrc%5Etfw) a eso se suma grave denuncia d comunidad de Campo Alegre del asesinato e intento de desaparición de Dimar Torres excombatiente de [\#FARC](https://twitter.com/hashtag/FARC?src=hash&ref_src=twsrc%5Etfw) al parecer por [@COL\_EJERCITO](https://twitter.com/COL_EJERCITO?ref_src=twsrc%5Etfw) [pic.twitter.com/Lz0UaarcAP](https://t.co/Lz0UaarcAP)
>
> — AscamcatOficial (@AscamcatOficia) [23 de abril de 2019](https://twitter.com/AscamcatOficia/status/1120718465978052608?ref_src=twsrc%5Etfw)

<p>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
<iframe id="audio_34841537" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_34841537_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen">
</iframe>
</p>
###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
