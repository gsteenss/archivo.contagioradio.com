Title: Ya hay fecha para que el general (r) Mario Montoya rinda versión ante la JEP por casos de ejecuciones extrajudiciales
Date: 2019-12-24 10:43
Author: CtgAdm
Category: Judicial, Paz
Tags: Caso 003, Ejecuciones Extrajudiciales, JEP, Mario Montoya
Slug: mario-montoya-jep-version
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/03/Mario-Montoya.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo Particular] 

La Sala de Reconocimiento de la Jurisdicción Especial para la Paz (JEP), por medio del auto 261 de 2019, ordenó que para el próximo 12 de febrero de 2020 a las 8:00 am, el general (r) Mario Montoya Uribe, excomandante del Ejército Nacional, deberá rendir versión dentro del caso 003, conocido también como el de “muertes ilegítimamente presentadas como bajas en combate por agentes del Estado”, proceso **en el que el general ha sido comprometido y mencionado en varios informes allegados a la entidad, y en al menos 11 versiones rendidas por integrantes de la Fuerza Pública.**

Al respecto, la Sala de Reconocimiento de la JEP informó que los abogados de las víctimas acreditadas en el caso 03 podrán hacer presencia en la sala principal donde el general (r) Montoya Uribe dará su versión, mientras se dispondrá de una sala alterna para que las víctimas puedan observar en tiempo real la diligencia. [(Le puede interesar: "Montoya diga la verdad" exigen las víctimas](https://archivo.contagioradio.com/montoya-diga-la-verdad-exigen-victimas/))

Según la Fiscalía, el General Mario Montoya, comandante del Ejército Nacional entre 2006 y 2008, tiene responsabilidad en el asesinato de **Daniel Pesca Olaya, Eduardo Garzón Páez y Fair Leonardo Porras**, víctimas de ejecuciones extra judiciales en Soacha, Cundinamarca.  Además, la Human Rights Watch, durante su tiempo al mando, «al menos 2.500 civiles habrían sido víctimas de ejecuciones extrajudiciales»,  en dicho lapso de tres años , el general habría emitido directrices que presionaban resultados.

Adicionalmente, Montoya, quien firmó acta de sometimiento ante la JEP el 17 de octubre de 2018, tendría que responder por violaciones a los derechos humanos cometidas durante la operación Orión en 2002 en la Comuna 13 de Medellín que dejó un saldo de por lo menos 100 personas desaparecidas durante el gobierno de Álvaro Uribe Vélez. [(Le puede interesar: Operación Orión ¡Nunca Más!, 17 años buscando justicia y verdad)](https://archivo.contagioradio.com/victimas-operacion-orion/)

### ¿Qué otros integrantes del Ejército se han presentado ante la JEP?

Hasta el momento, y como parte del caso 003, **la JEP ha realizado 202 versiones, 162 orales y 40 escritas**, han rendido versión 51 soldados, 38 suboficiales, 32 oficiales subalternos  que incluyen a subtenientes, tenientes y capitanes, 10 oficiales con rango de Mayor y 7 de rango de Coronel. Dichas versiones han sido puestas a disposición de las 360 víctimas acreditadas dentro del caso. [(Lea también Queda en firme sometimiento del general (r) Mario Montoya a la JEP](https://archivo.contagioradio.com/queda-en-firme-sometimiento-del-general-r-mario-montoya-a-la-jep/))

La JEP a su vez informa que además de las versiones citadas también han dado su testimonio el general en retiro Paulino Coronado, excomandante de la Brigada 30; el general Miguel David Bastidas, exsegundo comandante del Batallón de Artillería No. 4  y el general (r) Henry Torres Escalante, excomandante de la Brigada 16.

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
