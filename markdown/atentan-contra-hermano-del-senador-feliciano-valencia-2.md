Title: Atentan contra hermano del senador Feliciano Valencia
Date: 2018-12-21 12:15
Author: AdminContagio
Category: DDHH, Nacional
Slug: atentan-contra-hermano-del-senador-feliciano-valencia-2
Status: published

###### Foto: @FelicianoValen 

###### 21 Dic 2018 

[Germán Valencia Medina, defensor de derechos humanos y líder indígena, fue atacado por varios encapuchados quienes intentarlo asesinarlo mientras se encontraba en su vivienda en zona rural de Santander de Quilichao, Cauca, Medina, hermano del senador Nasa, Feliciano Valencia sobrevivió y se encuentra fuera de peligro. ]

Según **Luis Acosta,** coordinador nacional de la Guardia Indígena en el Cauca, los hechos se presentaron el pasado jueves 20 en horas de la noche cuando entre cuatro y seis hombres encapuchados y armados llegaron hasta la vivienda de **Germán Valencia Medina** e intentaron disparar contra su humanidad, sin embargo el arma no funcionó, Valencia sostuvo un breve enfrentamiento con los hombres quienes lo redujeron golpeándolo con la cacha del revólver para luego darse a la fuga.

[Minutos después, Valencia fue escoltado hasta el hospital de **Santander de Quilichao** donde se encuentra estable y en recuperación. El hermano del senador  se desempeña en la actualidad como parte del equipo de  derechos humanos de la **ACIN** y además participa en el control territorial que se viene realizando en las regiones a raíz de la muerte del gobernador indígena **Edwin Dagua en Huellas, Caloto** el pasado 7 de diciembre.]

[El coordinador manifiesta que como parte del control territorial ejercido por la comunidad, ya se están buscando a los responsables del ataque pero expresa su preocupación pues no ha escuchado ninguna reacción por parte de las autoridades sobre el atentado, mientras incrementan las agresiones  y asesinatos contra los líderes indígenas. ]

<iframe id="audio_30964426" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_30964426_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
