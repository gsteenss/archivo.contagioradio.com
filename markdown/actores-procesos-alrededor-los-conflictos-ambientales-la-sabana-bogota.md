Title: Conflictos por intereses ponen en peligro los Páramos en la Sabana de Bogotá
Date: 2017-08-09 16:30
Category: Ambiente, Voces de la Tierra
Tags: páramos, sabana de Bogotá
Slug: actores-procesos-alrededor-los-conflictos-ambientales-la-sabana-bogota
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/08/paramos_sabana_bogota-e1502300852125.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Fundación senderos y memoria ] 

###### [9 Ago 2017] 

La Sabana de Bogotá y particularmente las zonas de páramos se encuentran en riesgo debido a la confluencia de intereses sobre ellos por parte de determinados actores tales como las empresas extractivas, los latifundistas, las comunidades y el Estado, generando una serie de conflictos alrededor los **“procesos de apropiación, uso y manejo”,** como lo explica el nuevo informe ‘Conflictos socioambientales en los páramos de la Sabana de Bogotá’ de la Asociación Ambiente y Sociedad.

Específicamente las principales actividades que tienen en riesgo estos ecosistemas estratégicos son: **la extracción minera, gasífera y petrolera**; la agroindustria papera y los cultivos de cebolla; la restricción al uso del suelo por medidas de conservación; la introducción de especies exóticas; la expansión ganadera y el pastoreo extensivo; la desecación de humedales; la urbanización descontrolada en el altiplano; la canalización de cuerpos de agua; la creciente demanda de servicios ecosistémicos; el turismo sin control y la implementación de megaproyectos **(hidroeléctricas, represas, líneas de alta tensión, vías.**

Dichas actividades están generando la destrucción de la cobertura vegetal, fragmentación de hábitats, contaminación de nacimientos de agua, destrucción del suelo, inestabilidad de terreno, contaminación del aire, modificación del paisaje, pérdida de servicios ambientales, conflictos con autoridades ambientales y entre comunidades, migración campo-ciudad, pérdida de identidad campesina y aumento de la vulnerabilidad de las comunidades campesinas; por citar algunas consecuencias.

### Cuatro procesos generadores de conflictos 

La situación amenaza ecosistemas necesarios para supervivencia de los seres humanos, teniendo en cuenta que **los páramos, la alta montaña y demás espacios son catalogados como “estraté­gicos”** en términos de servicios ambientales, pero además en materia de biodiversidad son el hábitat de diversas y numerosas especies de fauna y flora, que incluso, solo habitan en estos ecosistemas.

Es así como el informe identifica cuatro procesos que se están llevando a cabo en estos territorios, y que sustentan diferentes conflictos socio-ambientales: la delimitación de las áreas de páramo, la consolidación de los acueductos comunitarios, la implementación del De­creto 0953 de 2013 y el Corredor Perimetral de Oriente de Cundinamarca.

El entramado de procesos ha ocasionado la “**rup­tura de la estabilidad histórica entre una comunidad y su hábitat”.** Y aunque existen herramientas estales como el Decreto 0953 del Ministerio de Ambiente, que promueve la con­servación y recuperación de las áreas de importancia estratégica para la conservación de recursos hídricos, esa Ley se enfrenta al desarrollo extractivista y los intereses de las grandes empresas sobre el quehacer gubernamental y la vida local, que a su vez, se encuentra con la invisibilización y menospre­cio de los requerimientos de las comunidades locales.

[Conflictos Socioambientales Paramos Sabana Bogota Julio 2017](https://www.scribd.com/document/355936519/Conflictos-Socioambientales-Paramos-Sabana-Bogota-Julio-2017#from_embed "View Conflictos Socioambientales Paramos Sabana Bogota Julio 2017 on Scribd") by [Contagioradio](https://www.scribd.com/user/276652802/Contagioradio#from_embed "View Contagioradio's profile on Scribd") on Scribd

<iframe id="doc_93084" class="scribd_iframe_embed" src="https://www.scribd.com/embeds/355936519/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-5tIwUfHeB9WtDRhH5hPE&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="0.6615271659324523"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
