Title: Asesinato del profesor Luis Fernando Wolf en Medellín tendría fines políticos
Date: 2015-04-27 17:44
Author: CtgAdm
Category: DDHH, Nacional
Tags: luis fernando wolf, Medellin, profesor, sicarios, sindcalismo, sindicalista, Universidad Nacional
Slug: asesinan-a-profesor-jubilado-activista-social-y-miembro-del-frente-amplio-luis-fernando-wolf
Status: published

###### Foto: Unimedios 

<iframe src="http://www.ivoox.com/player_ek_4419521_2_1.html?data=lZmem5qWdY6ZmKiak5WJd6Kpk5KSmaiRdo6ZmKiakpKJe6ShkZKSmaiRhdTZ1M7bw9OPpc2f0dfcyMrXs9OfrdrW1ZCqqdPiwtPR0ZC7s83ajMnSjdHFb7biytvS1NjNqI6ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Oscar Zapata, Presidente de APUN Medellín] 

El profesor **Luis Fernando Wolf** de 64 años de edad, jubilado tras pertenecer 35 años al Departamento de Física de la **Universidad Nacional de Colombia** en Medellín, era conocido por su compromiso activo en la lucha por los Derechos Sociales.

El profesor Wolf fue prisioneros político durante el gobierno de Turbay Ayala, y por ese motivo integrante del Comité Permanente por la Defensa de los Derechos Humanos desde los años 80's. En la Universidad Nacional de Colombia fue un destacado representante profesoral.

Durante los últimos años de su vida, Luis Fernando Wolf realizó trabajo en las comunas de Medellín, integró el Polo Democrático Alternativo y el Frente Amplio por la Paz.

La mañana del miércoles 27 de abril, sicarios interceptaron su carro en el barrio Iguaná, aledaño a la Universidad Nacional, y acabaron con su vida. Las autoridades descartan que el homicidio pueda estar relacionado con delincuencia común, en tanto el profesor fue ultimado dentro del automóvil por sicarios que se movilizaban en moto por el costado derecho del vehículo. Versiones de amigos cercanos aseguran que el homicidio estaría relacionado con las denuncias que estaba realizando el profesor Wolf sobre el mal manejo de recursos en un barrio de la zona centro-occidental.

El senado Antonio Navarro Wolf, primo del profesor Luis Fernando, exigió a las autoridades que se establezcan las responsabilidades por el crimen cometido.

El velorio del profesor Luis Fernando se llevó a cabo la noche del lunes 27 de abril en Medellín.
