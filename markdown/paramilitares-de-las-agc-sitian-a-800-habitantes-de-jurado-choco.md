Title: Paramilitares de las AGC sitian a 800 habitantes de Juradó, Chocó
Date: 2019-04-26 16:38
Author: CtgAdm
Category: Comunidad, Nacional
Tags: AGC, Chocó, ELN, Juradó
Slug: paramilitares-de-las-agc-sitian-a-800-habitantes-de-jurado-choco
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/03/conflicto-1-1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo] 

Desde el pasado 23 de abril, comunidades indígenas de Cedral y Bongo en Juradó, Chocó, compuesta por cerca de 800 habitantes fue sitiada por las autodenominadas Autodefensas Gaitanistas de Colombia (AGC) quienes después de asentarse en las comunidades de **Buena vista, Dos Bocas y Pichindé**, se han tomado este corredor que resulta estratégico para las rutas del narcotráfico.

**Carlos Mario Cardona, personero de la comunidad indígena de Cedral** señala que el confinamiento al que están sometidas las comunidades ha impedido que se movilicen y desarrollen sus actividades de pancoger y limitar la comunicación con poblados aledaños, según el defensor, de continuar esta presión la población ha expresado que "se desplazarán hasta el casco urbano de Jurado".

### **Enfrentamientos entre el ELN y las AGC** 

El pasado miércoles 24 de abril cerca de las 7:00 de la noche se presentó un enfrentamiento en Cedral entre las AGC y el ELN dejando en medio del fuego cruzado a la población, que ahora teme se presenten nuevas disputas, además han manifestado su miedo a desplazarse pues según advertencias hechas por los paramilitares, **"hay zonas en las que fueron instaladas minas anti persona"**, sin embargo como señala el personero, es necesario realizar un estudio de verificación en los caminos que existen entre las zonas de los territorios indígenas de Cedral y Bongo, población que se desplazó el pasado 16 de abril debido a la presencia del ELN.

Las comunidades indígenas aledañas de **Eyásake, Pichindé y Dos Bocas,** también fueron confinadas y se encuentran sin alimentación ni garantías de seguridad. Ante las violaciones de DD.HH que se presentan en la región,  la administración municipal de Juradó nunca prestó atención a las alertas tempranas ni a la situación que están viviendo las comunidades actualmente,  además, como manifiesta Carlos Mario Cardona, ningún ente del Estado acudió, a excepción de la Defensoría del Pueblo que sí ha hecho presencia en el lugar. [(Lea también:Asesinan al rector indígena Aquileo Mecheche en Río Sucio, Chocó) ](https://archivo.contagioradio.com/asesinan-aquileo-mecheche-rio-sucio/)

<iframe id="audio_34998039" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_34998039_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
