Title: Revocatoria a Enrique Peñalosa podría realizarse en Noviembre
Date: 2017-08-23 12:59
Category: Movilización, Nacional
Slug: revocatoria-a-enrique-penalosa-podria-realizarse-en-noviembre
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/05/marcha_de_las_antorchas_para_revocar_a_penalosa_foto_blu_radio_2.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Blu Radio] 

###### [23 Ago 2017] 

Los comités de revocatoria al alcalde Enrique Peñalosa anunciaron que, pese a las trabas que continúa poniendo el Consejo Electoral, se tomarán medidas como entablar acciones de tutela a esta institución para que el proceso pueda seguir su rumbo, y **calculan que hacía mediados de noviembre podrían desarrollarse las votaciones.**

De acuerdo con Carlos Carrillo, integrante del comité Unidos Revocaremos a Peñalosa, el nuevo freno que ha puesto el Consejo Electoral es la revisión de cada uno de los documentos que certifiquen las inversiones y facturas de lo que costó realizar el proceso de recolección de firmas y argumenta que pueden tomarse el tiempo que sea necesario para realizar esta revisión, **retrasando los tiempos que se establecen constitucionalmente para llevar a cabo una revocatoria**.

“Nosotros vamos a seguir adelante con una estrategia jurídica, porque esto se está configurando como un peculado, el Consejo Nacional Electoral, más temprano que tarde, **tendrá que dejar de entorpecer el proceso y permitir que los bogotanos se pronuncien en las urnas**” afirmó Carrillo. (Le puede interesar: ["Peñalosa "no da la cara": comunidades en el sur de Bogotá"](https://archivo.contagioradio.com/penalosa-no-da-la-cara-comunidades-en-el-sur-de-bogota/))

Además, Carrillo denunció que la Fundación Azul, encargados de hacer la revisión grafológica de las 473 mil firmas recolectadas para revocatoria, no han iniciado este trabajo provocando que se dilate más el proceso de votación, **“ellos tienen mucho miedo de ir a las urnas porque saben que la ciudad en su mayoría está a favor de la revocatoria**”. (Le puede interesar: ["Proceso de revocatoria no se ha suspedindo: Comité Revoquemos  a Peñalosa"](https://archivo.contagioradio.com/proceso-de-revocatoria-no-se-ha-suspendido-comite-revoquemos-a-penalosa/))

Carrillo afirmó que en los próximos días se espera el pronunciamiento por parte de la Registraduría Nacional sobre cuándo sería la fecha de las votaciones, y de esta manera iniciar con la campaña correspondiente para invitar a los ciudadanos a votar.

<iframe id="audio_20489784" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_20489784_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
