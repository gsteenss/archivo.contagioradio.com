Title: Fiscal ad hoc es otra maniobra de Martínez, para manipular el proceso: Robledo
Date: 2018-11-30 16:24
Author: AdminContagio
Category: Nacional, Política
Tags: Fiscal General, Iván Duque, Nestor Humberto Martínez, Odebretch
Slug: fiscal-ad-hoc-es-otra-maniobra-de-martinez-para-manipular-el-proceso-robledo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/09/fiscal-e1473096991642.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Youtube] 

###### [30 Nov 2018] 

El senador Jorge Robledo, del partido Polo Democrático, señaló que la selección de un Fiscal ad hoc, que realice la investigación a Néstor Humberto Martínez, es una maniobra para continuar "manipulando el proceso" que se está llevando por los casos de corrupción que involucran al Fiscal General con la constructora Odebretch.

Robledo expresó que Martínez **"es una ficha de la Ruta del Sol, Luís Carlos Sarmiento y Odebrethc en un caso grande de corrupción"** y lamentó que la Corte Constitucional se haya prestado para continuar con la selección de un Fiscal ad hoc, luego de que tanto el Fiscal General como la Vice Fiscal, María Paulina Rubio, se declararan impedidos.

**Fiscal sigue en pié porque lo protegen los grandes poderes**

Además, el senador denunció que la razón por la cual Néstor Humberto Martínez sigue en su puesto, pese a las graves acusaciones, es porque "lo están sosteniendo los grandes poderes de Colombia, empezando por el presidente Duque", quien será el encargado de nombrar a la terna de donde saldrá el Fiscal ad hoc que llevará el caso. (Le puede interesar:[ "Fiscal general fue amenzante porque sabe que lo desenmascaramos: Iván Cepeda"](https://archivo.contagioradio.com/fiscal-general-fue-amenazante-porque-sabe-que-lo-desenmascaramos-ivan-cepeda/))

### **Duque está impedido para nombrar terna** 

No obstante, Robledo aseveró que el presidente también debería declararse impedido debido  a que existe una acusación en la que se señala que Duque habría viajado a Brasil, acompañado del ex candidato presidencial Óscar Iván Zuluaga, para recibir dineros de integrantes de la multinacional Odebretch, **razón por la cual podría estar envuelto en la trama de corrupción. **

Asimismo, aseguró que la actitud que ha tomado el Fiscal General frente a las acusaciones que se hacen en su contra es de **"matón al salir a amenazar en medios de información a congresistas y periodistas",** razón por la cual manifestó que continuará exigiendo la renuncia de Martínez. (Le puede interesar: ["Se quieres desviar la atención sobre las acusaciones a Néstor Humberto Martínez: Cepeda"](https://archivo.contagioradio.com/fiscal-general-fue-amenazante-porque-sabe-que-lo-desenmascaramos-ivan-cepeda/))

<iframe id="audio_30509209" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_30509209_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
