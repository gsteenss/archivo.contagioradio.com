Title: Amnistía Internacional alerta por "políticas de demonización" que violan los DDHH
Date: 2018-02-22 13:21
Category: DDHH, Nacional
Tags: Amnistía Internacional, defensores de derechos humanos, Derechos Humanos, violencia contra los ddhh
Slug: amnistia-internacional-alerta-por-politicas-de-demonizacion-que-violan-los-ddhh
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/05/policia-reprime.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [22 de Feb 2018] 

La organización Amnistía Internacional presentó su informe anual que documenta la situación de los derechos humanos en **159 países del mundo en el 2017**. En el caso colombiano se alertó la grave situación que viven los campesinos, afros e indígenas por el aumento de asesinatos y amenazas en su contra.

Para empezar, la organización hace una caracterización de los principales eventos en el mundo por los cuales, producto de las “políticas de demonización” de algunos gobiernos, **millones de personas** sufrieron violencia contra sus derechos más básicos. Allí, se hace alusión a el desplazamiento masivo del pueblo Rohingya en Myanmar donde “en cuestión de semanas la campaña militar de limpieza étnica causó el éxodo de más de 655 mil personas”.

Además, “los **líderes de los países ricos** han seguido abordando la crisis global de refugiados con una mezcla de evasión de responsabilidades y absoluta insensibilidad, considerando a las personas refugiadas no como seres humanos con derechos sino como problemas que hay que apartar”.

### **Situación de Derechos Humanos en Colombia** 

En lo que tiene que ver con esta situación en el país, Amnistía Internacional manifestó que, a pesar de haber firmado un Acuerdo de Paz entre el Gobierno Nacional y la guerrilla de las FARC, **el conflicto armado continúa**. Esto en gran medida se debe a que no ha habido una implementación efectiva de lo acordado y se han desligado hechos como “falta de garantías para la participación efectiva de los pueblos indígenas y las comunidades afrodescendientes en la implementación del acuerdo”.

Preocupa a la organización que entre enero y octubre de 2017 se registraron delitos contra **31.047 víctimas del conflicto** armado y que, en departamentos como Antioquia, Cauca, Chocó, y Norte de Santander “persistieron las violaciones de los derechos humanos con homicidios selectivos de miembros de comunidades afrodescendientes y pueblos indígenas”. (Le puede interesar:["Continúan las amenazas contra defensores de derechos humanos en el país"](https://archivo.contagioradio.com/continuan-las-amenazas-a-defensores-de-derechos-humanos-en-el-pais/))

### **A pesar de la existencia del Acuerdo de Paz el conflicto se ha intensificado** 

El informe reitera que las dinámicas del conflicto armado aún permanecen en el país “como consecuencia de los enfrentamientos armados entre guerrilleros del ELN, grupos **paramilitares** y fuerzas del Estado que trataban de llenar el vacío de poder provocado por la desmovilización de las FARC”. Ejemplifican con la masacre de 13 personas el 27 de noviembre en Magüí Payán, Nariño.

Adicionalmente, documentaron que “hubo quejas sobre la **débil presencia del Estado** en zonas históricamente controladas por las FARC, una debilidad que facilita las incursiones de otros grupos armados ilegales”. Frente a estos grupos, alertaron sobre la existencia de grupos paramilitares “pese a su supuesta desmovilización”.

### **Policía fue acusada de cometer homicidios deliberados** 

Además de lo relacionado a los grupos ileglaes, las violaciones a los derechos humanos provinieron también des las **fuerzas del Estado**. “Hubo varias acusaciones de homicidios deliberados cometidos por fuerzas del Estado y denuncias de uso excesivo de la fuerza por parte del Escuadrón Móvil Antidisturbios (ESMAD) en protestas en el Chocó, Valle del Cauca, Cauca y Catatumbo”.

Hace referencia a las agresiones que sufrieron los manifestantes en el marco del Paro Cívico de Buenaventura donde el uso de **gas lacrimógeno** para reprimir la protesta, afectó a “205 niños y niñas, 10 mujeres embarazadas y 19 personas de edad avanzada (…) En total, 313 personas reportaron problemas de salud como consecuencia de su exposición al gas lacrimógeno y 16 sufrieron heridas de bala o traumatismos por golpes con objetos contundentes”. (Le puede interesar: ["Colombia sin garantías para la defensa de los derechos humanos"](https://archivo.contagioradio.com/colombia-sin-garantias-en-la-defensa-de-los-derechos-humanos/))

### **Homicidios de defensores de DDHH aumentó un 30% en la primera mitad de 2017** 

Lo referente a la violencia cometida contra los defensores de derechos humanos preocupa a la organización en la medida en que “la Oficina en Colombia del Alto Comisionado de las Naciones Unidas para los Derechos Humanos reportó que al menos **105 defensores y defensoras** de los derechos humanos habían sido víctimas de homicidio en Colombia en el transcurso del año”.

Recuerdan que las agresiones se cometieron contra “líderes comunitarios, defensores y defensoras del derecho a la tierra, **el territorio y el medio ambiente**, y quienes participaban en campañas a favor de la firma del acuerdo final con las FARC”. Además, hacen referencia a los datos de la organización Somos Defensores quien alertó sobre el aumento en un 30% de los homicidios cometidos contra líderes sociales sólo en el primer semestre de 2017.

El informe cierra el apartado de Colombia haciendo alusión a la violencia contra las mujeres y las niñas donde establece que la **debilidad de los mecanismos de protección**, aumentan el riesgo de la violencia de género. Desde 2005 y tras la desmovilización de las Autodefensas Unidas de Colombia, “hubo un del 28% en los casos de violencia sexual en las comunidades en las que se habían reinsertado excombatientes de las AUC”.

### **Panorama en las Américas** 

La situación de violencia contra los derechos humanos es atribuida por la organización a fenómenos como la **desigualdad y la discriminación**. Afirman que en la región aún se presentan casos masivos de homicidios, desapariciones forzadas y detenciones arbitrarias. Además, quienes más han sufrido alto niveles de violencia son los defensores de derechos humanos y “la impunidad sigue siendo generalizada”.

De igual forma, los pueblos indígenas de los diferentes países han sufrido una constante discriminación en la medida en que se les ha **“negado sus derechos económicos, sociales y culturales**, incluido su derecho a la tierra y al consentimiento libre, previo e informado sobre los proyectos que los afectan”. En cuanto a los que tiene que ver con la defensa de los derechos de las mujeres, las niñas y la población LGBTI “fueron pocos los avances”. (Le puede interesar:["Defensores de derechos humanos "contra las cuerdas"](https://archivo.contagioradio.com/disminuyeron-las-amenazas-pero-aumentaron-los-asesinatos-somos-defensores/))

La región continúa siendo **una de las más violentas del mundo** y no hay garantías para el ejercicio de derechos como la protesta social debido “al uso indebido e innecesario de la fuerza” de las autoridades de varios países. También, el poco acceso a la justicia y la impunidad “fue un factor clave en los abusos y violaciones de los derechos humanos”.

Finalmente y frente a las acciones de las políticas del presidente Donald Trump en Estados Unidos, la organización alertó que ese país ha tomado **medidas extremas** que afectan a personas en condición de refugiados y que han sido perseguidas. Indica que “en sus primeras semanas en el cargo, el presidente Trump dictó órdenes ejecutivas para suspender durante 120 días el programa nacional de reasentamiento de personas refugiadas”.

[Amnistia Internacional 2017](https://www.scribd.com/document/372152343/Amnistia-Internacional-2017#from_embed "View Amnistia Internacional 2017 on Scribd") by [Contagioradio](https://www.scribd.com/user/276652802/Contagioradio#from_embed "View Contagioradio's profile on Scribd") on Scribd

<iframe id="doc_39416" class="scribd_iframe_embed" title="Amnistia Internacional 2017" src="https://www.scribd.com/embeds/372152343/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-dkVhbHoxfpimxG2bpiV0&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="0.6502890173410405"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
