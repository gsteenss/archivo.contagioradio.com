Title: Continúa la represión en contra de normalistas de Ayotzinapa
Date: 2015-03-30 21:57
Author: CtgAdm
Category: El mundo, Otra Mirada
Tags: 43 desaparecidos Ayotzinapa, Ayotzinapa, Policía reprime estudiantes normalistas Ayotzinapa, Represión contra estudiantes de la normal de Ayotzinapa
Slug: continua-la-represion-en-contra-de-normalistas-de-ayotzinapa
Status: published

###### Foto:Regeneración Radio 

El sábado **28 de febrero** se conmemoraba el **89 aniversario** de la fundación de la **Escuela Normal**, y los **6 meses** de la **desaparición forzada** de los **43 de Ayotzinapa**.

La represión no se hizo esperar, al mediodía, estudiantes de la normal viajaban procedentes del municipio **Zumpango de Neri** con un vehículo que transportaba combustible. En el viaje fueron **retenidos por la policía y el ejército**, sustrayéndoles una de las camionetas y el combustible**.**

Más tarde, de regreso a la normal, fueron alcanzados **en Tixtla por más de 600 policías** antidisturbios, quienes destruyeron uno de los **autobuses** con estudiantes dentro. **Rompieron los vidrios con piedras y a lanzaron gas lacrimógeno**, así como a pegaron a los estudiantes que obligaron a bajar del autobús **deteniendo a 2** de ellos e **hiriendo** a un total de **10 personas**.

 Según un **estudiante** “…encontrarnos en la desviación hacia Tixtla fuimos alcanzados por policías federales con quienes tuvimos un breve roce. Al finalizar éste… nos percatamos que uno de los  **autobuses… fue encapsulado por los policías**, por lo que nos vimos orillados a regresar por los compañeros que se encontraban al interior del autobús…”.

\[embed\]https://www.youtube.com/watch?v=xN3\_0C9kjbk\[/embed\]

Al parecer el combustible había sido expropiado para cubrir las necesidades de la propia movilización.

En respuesta a la agresión policial estudiantes de la normal **quemaron un coche de la policía federal y otro de la protección civil para ejercer presión** y que liberaran a sus compañeros, que en menos de una hora quedaron en libertad.

Por la tarde fue realizada una **marcha **por las calles de la ciudad **de Tixtla hasta el zócalo** de la misma en **Guerrero para conmemorar el 89 aniversario** de la creación de la Escuela Normal y de los 6 meses de la desaparición forzada de los 43 estudiantes de Ayotzinapa. En la marcha se pudo ver muestras de **solidaridad** de los propios **ciudadanos y de los comercios por donde pasaba**.
