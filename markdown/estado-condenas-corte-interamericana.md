Title: Estado responderá por 14 condenas en su contra ante la Corte Interamericana de DD.HH.
Date: 2019-09-04 16:03
Author: CtgAdm
Category: DDHH, Judicial
Tags: Condena, Corte Interamericana de Derechos Humanos, estado, Palacio de Justicia
Slug: estado-condenas-corte-interamericana
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/09/33604591058_2b32d1473b_k-e1567630386414.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Corte IDH  
] 

Los próximos 6 y 7 de septiembre se realizará una serie de audiencias privadas de Colombia ante la Corte Interamericana de Derechos Humanos, para rendir cuentas sobre 14 de las 22 sentencias que ha emitido este Tribunal contra el Estado colombiano. Según referenció el Colectivo de Abogados José Alvear Restrepo ([CAJAR](https://www.colectivodeabogados.org/?Estado-colombiano-debera-rendir-cuentas-sobre-14-condenas-ante-la-Corte-IDH)), la Corte revisará casos ocurridos entre 1985 y 1998, que incluyen a 96 víctimas directas de desaparición forzada, 77 de ejecuciones extrajudiciales, 2 de atentados y 1 tortura.

### **En medio de una decisión inédita, la Corte sesionará en Colombia** 

**Jomary Ortegón, abogada del CAJAR,** explicó que la visita se enmarca en el seguimiento que hace la Corte a sus propias sentencias; para hacer dicho seguimiento cuenta con diferentes mecanismos entre los que se encuentran la presentación de informes escritos, y, "excepcionalmente se convocan audiencias". Las últimas, según la abogada, "son la oportunidad para que las partes presenten de viva voz sus apreciaciones y observaciones sobre la falta de cumplimeitno, o cumplimiento de las decisioens emanadas de la Corte".

En esta ocasión, la Corte Interamericana decidió sesionar en el país del que son las sentencias, Ortegón calificó como extraña esta situación, porque normalmente se sesiona en Costa Rica o en países invitados, cuando las audiencias son de carácter público. Ella resaltó que esta decisión, **se traduce en una oportunidad para que las víctimas de los casos en cuestión puedan participar de la audiencia**, pues en ocasiones, las sesiones en países extranjeros no permiten por razones económicas o de orden político, que las víctimas tengan esta posibilidad.

Otro hecho novedoso de estas audiencias es que el seguimiento se realizará mediante la agrupación de medidas en algunos casos, por ejemplo, la primera audiencia que se realizará el jueves 5 de septiembre incluirá los casos de desaparición forzada de Las Palmeras, Pueblo Bello, vereda La Esperanza, Isaza Uribe, 19 comerciantes y Caballero Delgado y Santana. Según señaló la Abogada, esto, porque la Corte revisará el avance en sus decisiones de búsqueda de paradero, localización, identificación y entrega de restos mortales, que son generales para los hechos.

De igual forma, también la Corte agrupó los casos en que la Corte ordenó que a las víctimas se les provea un **tratamiento médico y de atención psicosocial gratuito, digno y cercano a sus lugares de residencia.** Otro casos como el del Palacio de Justicia, la Masacre de Mapiripán o la de las Palmeras tendrán una audiencia individual, para hacer seguimientos de manera específica. (Le puede interesar: ["Fallo Histórico: Corte Internacional condena al Estado por Falsos Positivos"](https://archivo.contagioradio.com/fallo-historico-corte-internacional-condena-al-estado-por-falsos-positivos/))

### **¿Qué llevó a la Corte Interamericana a sesionar en Colombia?** 

Ortegón expuso dos hipótesis que podrían explicar el hecho inédito de hacer audiencias de seguimiento en el país en que se realizan los casos: "Puede ser porque la Corte no tiene suficientes elementos para evaluar sus sentencias", y ello sería importante porque en su visión, "hay sentencias de la Corte históricamente incumplidas"; y la otra opción, es que la Corte lo haya decidido así para responder a las constantes solicitudes del Estado colombiano de declarar como cumplidas algunas de las obligaciones establecidas por el tribunal internacional.

En ese sentido, la abogada defensora de derechos humanos, afirmó que tras las audiencias esperarán la resolución cumplimiento de sentencias, con la que la Corte decidirá sobre el cumplimiento, o no, de las medidas que ha tomado para los casos estudiados. Entre los casos que son sujetos de investigación, y que Ortegón resaltó fue el del Palacio de Justicia, pues aunque la Corte ordenó la creación de un plan de búsqueda, el mismo no se ha realizado. (Le puede interesar: ["Fiscalía demuestra su falta de compromiso con los desaparecidos del Palacio de Justicia"](https://archivo.contagioradio.com/fiscalia-desaparecidos-palacio-de-justicia/))

### **El Estado ha avanzado en lo básico, pero no en lo estructural** 

En la evaluación del cumplimiento de sentencias, la Abogada sostuvo que en cuanto a las medidas indemnizatorias y de reconocimiento público, en la mayoría de casos, el Estado ha cumplido. Pero se presentan incumplimientos en las medidas de administración de justicia y en materia de búsqueda de personas desaparecidas, puesto que las investigaciones para establecer responsabilidades avanzan de forma lenta, mientras hay un vacío en la búsqueda de los desaparecidos por parte de los entes encargados de desarrollar esta labor.

Una muestra de la tensión sobre los cumplimientos ocurrirá en la segunda audiencia grupal que se desarrollará el jueves, sobre medidas de atención en salud y psicosocial; puesto que el Estado ha señalado que las mismas se brindan a través del sistema general de atención en salud, mientras las víctimas reclaman que, si ellas hacen aportes de sus trabajos a este sistema, **son ellas mismas quienes terminan por pagar su propia reparación.**

En consecuencia, y teniendo en cuenta que esta atención debe ser gratuita, digna y cercana a los lugares de residencia de las víctimas, Ortegón espera que la Corte entienda que **el cumplimiento de dichas medidas no ha sido suficiente, "no ha permitido el restablecimiento de derechos y no ha sido reparador"**. (Le puede inetesar: ["El Sistema Interamericano, un freno a los Estados autoritarios"](https://archivo.contagioradio.com/sistema-interamericano-freno-estados-autoritarios/))

### **El Sistema Interamericano: Un mecanismo para proteger DD.HH. en Colombia** 

La Corte IDH es el máximo órgano de justicia de la Organización de Estados Americanos (OEA); sobre Colombia ha emitido 22 sentencias y según Ortegón, la intención de las mismas es impactar sobre problemas estructurales para asegurar garantías de no repetición. Haciendo una evaluación general sobre el funcionamiento de la Corte Interamericana, la abogada aseguró que, "en el país, diríamos que ha sido fundamental para el entendimiento de la relación del paramilitarismo con actores estatales, igualmente, ha sido importante en decisiones como atención para parejas del mismo sexo (...) entonces, en general, sus decisiones que han aportado en materia de jurisprudencia y estándares para la protección de derechos humanos".

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
