Title: Lideresa sindical fue agredida y golpeada en Aguachica, Cesar
Date: 2019-02-15 14:07
Author: AdminContagio
Category: DDHH, Entrevistas, Líderes sociales
Tags: Meta, union sindical obrera, Violencia contra la mujer
Slug: lideresa-sindical-fue-agredida-y-golpeada-en-aguachica-cesar
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/02/NI-UNA-MAS-770x400.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Marcha patriótica 

###### 15 Feb 2019 

La **Unión Sindical Obrera de la Industria del Petróleo (USO)** denunció el ataque del que fue víctima el pasado 13 de febrero la lideresa sindical, **Dibeth Quintana,** quien fue abordada por desconocidos que la raptaron, ataron, golpearon y  luego abandonaron a las afuera del municipio de **Aguachica, César**, Quintana quien se recupera de los traumas y heridas ya había sido víctima de hostilidades y agresiones en años anteriores.

**Jairo Ramírez asesor de la comisión de derechos humanos de la USO**, indica  que Dibeth Quintana ha sido parte de las junta de directiva de la refinería, la subdirectiva de la USO relacionada a los oleoductos  y la junta directiva del sindicato en Bucaramanga,  dedicándose esencialmente a hacer seguimiento y atender los reclamos de los trabajadores en relación con la operación laboral en las plantas.

De igual forma, Ramírez destaca que desde hace un tiempo **la lideresa ha ejercido un destacado rol en la defensa de los derechos de la mujer trabajadora,**  algo que según el asesor podía ser uno de los motivos por los que podría haber sido atacada al generar malestar en algunos sectores de la empresa o de los contratistas.

**Precedentes contra la lideresa**

Según la USO, Dibeth ha sido objeto de constantes seguimientos y acciones de hostilidad al ser interceptada por la fuerza pública en diversas ocasiones con diferentes pretextos; además señalan seguimientos y llamadas telefónicas amenazantes contra ella y su familia.

**"Se nota cierta sevicia, en relación con ella y con la labor que ha desplegado, porque esencialmente lo que ha hecho de forma dedicada y juiciosa es asistir en la defensa y reconocimiento de los derechos de los trabajadores"** argumenta Ramírez quien también relaciona lo sucedido con la mentalidad patriarcal que existe en la sociedad y en el empresariado.

"Quizá se ve extraño que una mujer con su coraje, solvencia y formación sea quien encare las reclamaciones en los lugares a los que se les ha llamado para defender los derechos de los trabajadores  " agrega.

Aunque el ataque continúa siendo materia de investigación, Ramírez  indica que  Dibeth venía siendo víctima de maltrato y persecución por parte del encargado de la seguridad física de Ecopetrol en Cesar, hecho que ella había denunciado en la Fiscalía local, ente que la citó para que acudiera el mismo día que fue atacada.

**Los hechos**

Según el asesor tal citación de la cual solo tenían conocimiento Dibeth, Ecopetrol y la Fiscalía, fue cambiada de fecha sin embargo ella no fue notificada, "lo que observamos es que los agresores la  siguieron todo el tiempo,  ella sale de la Fiscalía y hasta ahí es consciente de lo sucedido, parece que le echaron un sustancia que le hizo perder el sentido, la introdujeron en un vehículo, le pusieron un bolsa en la cabeza, la llevaron a las afueras de Aguachica,  la golpearon y la abandonaron" relata Ramírez.

Para Ramírez, la Fiscalía local no brinda garantías, razón por la que han pedido sea trasladado el caso pues existen una serie de interrogantes sobre la entidad, además indica que el mismo hombre contra el que Dibeth interpuso una demanda por acoso, hizo presencia en el lugar e intento impedir que nadie la viera en el hospital.

[Respecto a lo sucedido, manifiesta que ni la Fiscalía ni Ecopetrol se han pronunciado frente al hecho y que la compañía  ha tenido "una actitud vergonzosa que toma distancia de cualquier actitud humanitaria frente a una mujer que fue agredida y de manera tan violenta.]" en lugar de brindar toda la atención necesaria. **Mientras se espera que las autoridades se pronuncien, Dibeth será trasladada a un centro hospitalario de Bucaramanga donde se espera su estado de salud evolucione.**

<iframe id="audio_32586450" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_32586450_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
