Title: Pueblo Nasa del Putumayo realiza Minga de control territorial
Date: 2017-11-14 14:52
Category: DDHH, Nacional
Tags: derechos de los indígenas, Indigenas Nasa, Minga Indígena, Putumayo
Slug: pueblo-nasa-en-el-putumayo-realiza-minga-de-control-territorial
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/03/NASA1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:  [Victoria Argoty](https://www.flickr.com/photos/plaxy/5095552524/)] 

###### [14 Nov 2017] 

Las comunidades del pueblo indígena Nasa en el Putumayo **realizarán una Minga de control territorial** debido a las afectaciones que ha producido la extracción minera en sus territorios. Con esto buscan realizar una protesta para defender la vida, el agua y los ecosistemas. La Minga inició el 12 de noviembre y 300 guardias indígenas de 38 comunidades estarán a cargo del control territorial.

De acuerdo con el comunicado enviado por la Asociación Consejo Regional del Pueblo Nasa del Putumayo, **las afectaciones por la minería se han registrado en 13 comunidades** de los municipios de Villa Garzón, Puerto Caicedo y Puerto Asís. Además, ha habido afectaciones indirectas en la rivera de afluentes y ríos como Juanambu, Piñuña y Coqueto.

La Minga, que irá hasta el 16 de noviembre, **comenzará en la comunidad de la Floresta Alto Coqueto** del municipio de Puerto Caicedo y tendrá como destino la comunidad de Jerusalén en el Municipio de Villa Garzón. Los indígenas fueron enfáticos en manifestar que las autoridades locales y regionales rechazaron la invitación para acompañar la movilización. (Le puede interesar: ["Denuncian asesinato de Jorge Chantré Achipiz, secretario del cabildo Nasa Pueblo Nuevo"](https://archivo.contagioradio.com/asesinado-jorge-chantre-achipiz-secretario-del-cabildo-nasa-pueblo-nuevo/))

### **Estas son las exigencias de los indígenas NASA** 

Desde hace más de 4 años, **el pueblo Nasa ha denunciado la contaminación de las fuentes hídricas** que hacen parte de su abastecimiento básico y fundamental debido a las acciones mineras que se llevan a cabo. Además, han informado a las autoridades el peligro que corre el desarrollo de prácticas culturales que realizan como “pueblo milenario”.

Por esto y ante la falta de acciones y presencia institucionalidad, lo indígenas le han exigido a Corpoamazonía, que es la autoridad ambiental competente, que **“suspenda las actividades mineras** y actué de manera diligente ya que conoce de esta situación desde hace más de 4 años”. (Le puede interesar: ["Pueblo Nasa denuncia que no está recibiendo ayudas del Gobierno en Mocoa"](https://archivo.contagioradio.com/pueblo-nasa-denuncia-que-no-esta-recibiendo-ayudas-del-gobierno/))

Además, le piden a la Fuerza Pública que **se abstenga de realizar operativos terrestres o aéreos** “mientras la Guardia Indígena realiza el recorrido del control territorial”.  Igualmente, solicitaron a la Procuraduría General de la Nación que inicie los procesos disciplinarios correspondientes contra los funcionarios públicos de las entidades locales y regionales “que han tenido conocimiento de la problemática y han omitido de manera reiterada su actuar para acabar las afectaciones ambientales generadas por la minería”.

Finalmente, le piden a la Fiscalía General de la Nación **actuar de manera contundente** y a la Unidad Nacional de Protección “para que adopte medidas de protección de manera inmediata y urgente contra líderes de la comunidad Nasa amenazados”.

###### Reciba toda la información de Contagio Radio en [[su correo]

<div class="osd-sms-wrapper">

</div>
