Title: Existen mecanismos para presionar al Gobierno y objetar la JEP que pueden ser aplicados
Date: 2019-03-06 15:02
Author: CtgAdm
Category: Paz, Política
Tags: implementación acuerdos de paz, JEP, países garantes del proceso de paz
Slug: existen-mecanismos-presionar-al-gobierno-objetar-la-jep-pueden-aplicados-3
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/03/JEP.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto:  JEP\_Colombia 

###### 6 Mar 2019

Ante las dificultades que ha atravesado la Jurisdicción Especial para la Paz (JEP), relacionadas a la tardía respuesta del presidente Iván Duque para objetar el proyecto de ley, el** abogado y analista Ramiro Bejarano** afirma que los mecanismos judiciales son necesarios para confrontar al Gobierno y asegurar que se cumplan los compromisos pactados tras el Acuerdo de Paz.

**"Estos no son compromisos de Gobierno, son compromisos de Estado avalados por la Corte Suprema de Justicia, la Corte Constitucional y el Consejo de Estado"** indica Bejarano, quien asegura que Duque sí objetará la Jurisdicción Especial para la Paz, esto producto de la presiones por parte de sectores de derecha que le han pedido examine detalladamente ciertos artículos de la Jurisdicción. [ (Lea también JEP y comunidades hacen historia con primera audiencia en zona rural del Cacarica) ](https://archivo.contagioradio.com/jep-comunidades-audiencia-cacarica-articulo-6268)

Sobre la importancia de los países garantes y la presión que podrían ejercer sobre esta decisión, señala que la comunidad internacional no debe permitir que se quebranten los protocolos del Acuerdo de Paz, y que las consecuencias de no hacerlo serían inmediatas, pues países como Noruega perderían la credibilidad "ante cualquier otro proceso de paz en el mundo".

**"La ultraderecha está decidida a aniquilar la JEP"**

Respecto a la captura de Carlos Bermeo, fiscal de apoyo a la Unidad de Investigación y Acusación, Ramiro Bejarano se pregunta por qué en este caso diferentes sectores están pidiendo que se cierre la JEP, mientras que en otras ocasiones cuando el fiscal Gustavo Moreno o el magistrado Jorge Pretelt fueron descubiertos y vinculados a sobornos, no se pidió que clausuraran entidades como la Corte Constitucional o la Fiscalía.

"Se trata de un libreto para desprestigiar a la JEP" indica Bejarano quien argumenta que  **este mecanismo está concebido para que las personas comparezcan y revelen la verdad, lo que "comprometería a gente muy importante de la derecha y a eso es lo que le temen".**

**¿Cómo debe actuar la JEP?**

[Aunque el abogado opina que "el Gobierno lenta y perceptiblemente ha arrinconado a la JEP", afirma que la entidad debe **"hacer valer su justicia mostrando al país sus resultados y silenciar las voces que pretenden convertirla en una cueva de bandidos".**]

Finalmente agrega que la JEP está integrada por diferentes personas que ya han trabajado con la Corte Constitucional, el Consejo de Estado, la academia y con la defensa de los derechos humanos contando con un recorrido respetable. [(Lea también Unidad de Investigación y Acusación de la JEP esperará investigación de la Fiscalía sobre caso Bermeo) ](https://archivo.contagioradio.com/unidad-de-investigacion-y-acusacion-de-la-jep-es)

<iframe id="audio_33141350" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_33141350_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
