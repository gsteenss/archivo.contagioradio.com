Title: Victoria de la izquierda en España dependerá de pactos postelectorales
Date: 2015-05-24 20:13
Category: El mundo, Otra Mirada
Tags: 15-M, Ahora Madrid, Compromís, Marea Atlántica, Plataforma antidesahucios, podemos
Slug: triunfo-historico-de-la-izquierda-en-espana
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/05/colau.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/05/celebracion-compromis5.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto:Lasprovincias.es 

###### **Entrevista con [Jordi Sebastiá], eurodiputado por Compromís:** 

###### <iframe src="http://www.ivoox.com/player_ek_4546509_2_1.html?data=lZqhmJqUfY6ZmKiak5aJd6KpmJKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRic3ZxMjW0dPJt4zh1tPWxc7Upc3Z1JCy1dXFaaSnhqeew5CWdJKpjoqkpZKns8%2FowszW0ZC2pcXd0JCah5yncZU%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>  
**Entrevista con [Jorge García Carreño], diputado por Izquierda Unida:** 

<iframe src="http://www.ivoox.com/player_ek_4546519_2_1.html?data=lZqhmJqVfY6ZmKiakpWJd6KkkZKSmaiRdI6ZmKiakpKJe6ShkZKSmaiRmNPd1tPT0ZDMrdTohqigh6eXtsrX0JDRx5DQpYzd29biy8rWqMKfxtOYx9HJp8Td0NPS1ZDJsoy51NWah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [25 may 2015] 

Los dos grandes partidos en España **PP y PSOE, han perdido más de 2,55 millones de votos y en total a la derecha le han sido arrebatadas 8 mayorías absolutas** como Madrid, Barcelona, y Valencia. El PSOE ha perdido 2 puntos frente a los 11 de la derecha. El **PP continuaría siendo la fuerza más votada** con el 28% de los votos.

Las alcaldías de las **principales capitales del país, Madrid, Valencia y Barcelona, quedan en manos de coaliciones entre PSOE, IU y Podemos**.

Barcelona en *Comú* en Cataluña, *Ahora Madrid* en Madrid, *Compromís* en el País Valenciano y *Marea Atlántica* en Galicia, han provocado **la debacle en los feudos históricos de la derecha.**

La **corrupción, los escándalos inmobiliarios, los recortes sociales y las políticas de austeridad, han mellado** en los dos grandes partidos llevándoles a los peores resultados electorales en la historia de sus formaciones políticas.

***Ciudadanos*** entra en la política Española como **cuarta fuerza política y voz de la ultraderecha** y las políticas anti inmigración.

Después de **7 años de crisis económica** que han conllevado recortes en el campo social, en la sanidad pública y en materia de derechos laborales, España se había situado a la **cabeza de Europa en estadísticas de extrema pobreza, desempleo, endeudamiento y personas desahuciadas.**

Todas estas políticas habían sido impuestas por el FMI y el BM, aprobadas por los dos grandes partidos que ostentaban el poder a modo de bipartidismo, entre la derecha (PP) y el socialismo (PSOE). Después de los resultados electorales, se puede observar la primera victoria de la izquierda y de los movimientos sociales: **Acabar con el bipartidismo y  hacer mas plural la democracia Española.**

Movimientos sociales como el **15-M,** de corte parlamentaria y extraparlamentaria, han conseguido denunciar las injusticia sociales y movilizar a la ciudadanía hacia un cambio social mediante los movimientos sociales, las **candidaturas ciudadanas** y la estrategia comunicativa en las redes sociales.

La **victoria de Ada Colau en Barcelona,** de la coalición de izquierdas ¨Barcelona en Comú¨, ha sido quizás la que más se ha movido en las redes sociales. Y es que la futura alcaldesa es la **presidenta de la PAH (Plataforma antidesahucios)**, el movimiento social Español más fuerte que ha conseguido denunciar los miles de desahucios de personas que no podían pagar su hipoteca por la crisis inmobiliaria.

La victoria de la izquierda en España se suma a la de Syriza en Grecia, **cambiando el escenario político en el viejo continente** y finalizando con las políticas de austeridad auspiciadas por Europa para los países del Sur.

Para los candidatos de **Compromís y de Izquierda Unida, Jordi Sebastiá y Jorge García Carreño,** para que la izquierda pueda gobernar, va a ser necesario que todos acerquen posturas y sobre todo que **el PSOE, ceda ante posturas de carácter más izquierdista.**

Según los diputados, el **trabajo de las izquierdas en contra de la corrupción y a favor de los más desfavorecidos por los recortes sociales,** ha permitido que gran parte de la ciudadanía pudiese **participar** a través de las **Candidaturas de Unidad Popular en las elecciones.**

Además, añaden que ahora mismo lo más importante es **acabar con la corrupción en los ayuntamientos y trabajar en frenar los recortes sociales** de cara a poder gobernar en unas futuras elecciones generales.
