Title: Consejo de Estado condenó a la nación por el crimen de Jaime Garzón
Date: 2016-09-14 18:33
Category: DDHH, Nacional
Tags: Consejo de Estado, crimen de lesa humanidad, estado, Jaime Garzon
Slug: consejo-de-estado-condeno-a-la-nacion-por-el-crimen-de-jaime-garzon
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/03/Jaime-Garzón-e1457652708929.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Con la Otra Oreja] 

###### [14 Sep 2016] 

El Consejo de Estado determinó que los organismos de seguridad del Estado realizaron actividades de inteligencia y compartieron la información recolectada con los paramilitares de las AUC comandadas por Carlos Castaño Gil para, además de sugerir al jefe paramilitar que se perpetrara el asesinato de Jaime Garzón, el 13 de Agosto de 1999.

El tribunal estableció que hay elementos suficientes que determinan la [responsabilidad del jefe de inteligencia del DAS y la B 13 de las FFMM](https://archivo.contagioradio.com/los-generales-y-militares-involucrados-en-asesinato-de-jaime-garzon/). Por el asesinato de Garzón actualmente son procesados **el excomandante del B-2 de la Brigada XIII del Ejército, coronel (r) Jorge Eliécer Plazas Acevedo** por el delito de homicidio agravado en calidad de determinador y el exsubdirector del DAS, José Miguel Narváez por el delito de homicidio agravado en calidad de coautor.

Según la decisión que fue tomada por la sección tercera del Consejo de Estado, el Ministerio de Defensa, el DAS y la Policía Nacional, como instituciones, [no tomaron las acciones necesarias para impedir el crímen](https://archivo.contagioradio.com/generales-mauricio-santoyo-y-rito-alejo-del-rio-son-vinculados-a-crimen-de-jaime-garzon/). Cabe resaltar que por parte de la fiscalía y la representación de la víctima de denunció el **desvío de la investigación para que no se lograra establecer la responsabilidad de agentes estatales.**

En el fallo determina que el director de la Policía Nacional, general Jorge Nieto y el comandante del Ejército, general Juan Pablo Rodríguez deben presidir un acto de [perdón público a los familiares del periodista](https://archivo.contagioradio.com/el-asesinato-de-jaime-garzon-es-un-crimen-de-lesa-humanidad/) en el que se haga **un reconocimiento expreso de responsabilidad agravada por estos hechos que enlutaron a todo el país.**
