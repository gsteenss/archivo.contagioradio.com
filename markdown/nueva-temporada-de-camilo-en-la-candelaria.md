Title: Nueva temporada de Camilo en La Candelaria
Date: 2018-10-25 12:36
Author: AdminContagio
Category: Cultura
Tags: Camilo Torre, la Candelaria, teatro
Slug: nueva-temporada-de-camilo-en-la-candelaria
Status: published

###### Foto: Teatro La Candelaria 

###### 25 Oct 2018 

Regresa al Teatro La Candelaria CAMILO, en temporada **del 25 de octubre al 2 de noviembre**, producción colectiva que desde su estreno en 2015 ha tenido una excelente acogida por parte del público, bajo la dirección de la maestra Patricia Ariza, artista de larga trayectoria como directora, actriz y dramaturga.

La obra se basa en la vida de **Camilo Torres Restrepo**, sacerdote, pionero de la Teología de la Liberación, cofundador de la primera facultad de Sociología de América Latina en la Universidad Nacional de Colombia, predicador de la fe cristiana y a la vez del amor al prójimo, razón por la cual fue perseguido, acosado y amenazado.

Ingresó al ELN en 1966 y sin haber disparado una bala, fue abatido en su primer combate. Para rememorar a este emblemático personaje de la historia de Colombia, y luego de más de 50 años de su desaparición física, la búsqueda de su paradero continua y los candelarios vuelven a prestar su cuerpo para encontrarlo.

El teatro la Candelaria, fundado en 1966 por iniciativa de un grupo de artistas e intelectuales independientes provenientes de la Universidad Nacional y del naciente teatro experimental, presentara esta obra con **funciones de miércoles a sábados a las 7:30 p.m**.

Para reservar sus boletas puede comunicarse al 2814814 o 2863715.
