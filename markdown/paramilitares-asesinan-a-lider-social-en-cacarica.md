Title: Paramilitares asesinan a habitante en Cacarica, Chocó
Date: 2017-10-17 15:40
Category: DDHH, Nacional
Tags: AGC, Autodensas Gaitanistas de Colombia, cacarica, Chocó, paramilitares
Slug: paramilitares-asesinan-a-lider-social-en-cacarica
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/10/cacarica.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: PBI Colombia] 

###### [17 oct 2017] 

La comunidad de Cacarica, en el Chocó, denunció el **asesinato de José Merlín Murillo**, habitante de la Zona Humanitaria. De acuerdo con la información, Merlin fue sacado a la fuerza de su vivienda en la Zona Humanitaria Nueva Vida el 11 de octubre a las 8: 00 am. Los atacantes, fueron integrantes de las Autodefensas Gaitanistas de Colombia.

José Merlín Murillo, de 33 años,  fue subido a un bote **“donde se encontraban tres paramilitares** quienes, al parecer, se dirigieron hacia los predios de la comunidad de Montañita, uno de los puntos en donde las AGC tienen control del territorio colectivo”. (Le puede interesar: ["Paramilitares asesinan a líder indígena en Medio Baudó"](https://archivo.contagioradio.com/paramilitares-asesinan-delante-de-la-comunidad-a-lider-indigena-ezquivel-manyoma/))

Tras la desaparición forzada, **su cuerpo fue encontrado el domingo pasado** flotando a las orillas del río Atrato en un lugar muy cercano al puesto permanente de control de la armada. Según la comunidad, el cuerpo presentaba signos de tortura.

Tras el reconocimiento del cuerpo, **la comunidad de Cacarica repudió el hecho** y reiteraron su “respeto a su propuesta humanitaria y de protección ambiental y territorial”. Además, las organizaciones sociales indicaron que urge una acción eficaz y oportuna de Medicina Legal para el cumplimiento de los protocolos de identificación de las personas dadas por desaparecidas. (Le puede interesar: ["Dos personas fueron torturadas por paramilitares en Blanquicet, Turbo"](https://archivo.contagioradio.com/paramilitares-torturan-a-dos-personas-en-blanquicet/))

### **Presencia de paramilitares evidencia el abandono estatal en Cacarica** 

La Comisión de Justicia y Paz manifestó que, en el territorio de Cacarica, los controles de las AGC **“obedecen al desamparo del Estado** que ha sido tolerante, cómplice e ineficaz para proteger la vida, la integridad y las libertades de los afrocolombianos en Cacarica”. Allí, en el territorio colectivo, las autodefensas tienen el control de 17 zonas.

###### Reciba toda la información de Contagio Radio en [[su correo]

 
