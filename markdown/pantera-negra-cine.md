Title: Reseña Crítica: Pantera Negra, futurista obsidiana de folklore
Date: 2018-02-21 11:51
Author: AdminContagio
Category: 24 Cuadros
Tags: áfrica, Cine, héroes
Slug: pantera-negra-cine
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/02/film-preview-black-panther-0-e1519231077926.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto:Entertainment 

##### Por: Cristian Mora J. 

##### 21 Feb 2018 

En medio de los que opinan que Black Panther recibe la ovación de la crítica únicamente por contar con un elenco afroamericano, visto como algo políticamente correcto, el color de la piel es quizá el menor de los detalles a destacar en la nueva película de Marvel Studios. No es la primera vez que se ven héroes afro en el cine o en plataformas como Netflix: Blade, Hancock y Luke Cage son algunos de estos ejemplos, sin embargo lo que diferencia a Pantera Negra del resto son sus orígenes y tradiciones netamente africanas, el vínculo con sus creencias y la importancia de estos como eje vertical del filme.

Sí, la investigación y trasfondo de la cultura africana son notables, pero los más puristas tampoco han de esperar un documental digno de la National Geographic, la nueva cinta del director Ryan Coogler augura el uso del CGI, explosiones y por cierto brillantes coreografías de combate cuerpo a cuerpo, a fin de cuentas es una película de Marvel, sin embargo el argumento de Pantera Negra puede extrapolarse para visibilizar diferentes problemáticas del mundo real.

Vale la pena examinar el dilema que enfrenta T’Challa, personaje principal de la cinta, nacido de la mente de Stan Lee y del dibujante Jack Kirby en 1966. T’ Challa quien viste el traje de la Pantera Negra, es el defensor y soberano del reino de Wakanda, una nación utópica que ha preferido vivir aislada del mundo exterior lo que ha permitido su desarrollo como civilización a costa de privar al resto del mundo de tecnología y avances más allá de los imaginado.

Algo que bien puede compararse a la propuesta de la administración de Donald Trump y sus políticas migratorias enfocadas en devolver el crecimiento económico de su país a cambio de cerrar las puertas a los miles de inmigrantes que cruzan la frontera creyendo en el ya desgastado  sueño americano.

https://www.youtube.com/watch?v=JQbjS0\_ZfJ0

Pantera Negra es además la primera película que muestra una familia de manera funcional (Los Cuatro Fantásticos son una rara excepción). Marvel nos tenía acostumbrados a la orfandad de Tony Stark, Peter Quill  o Peter Parker incluso al extraño problema de linaje que existe en Thor, pero por primera vez se plantea a la familia como el núcleo de una sociedad. Desde justificar las decisiones que debe tomar un monarca incluso si estas afectan a su familia por el bien común de su pueblo hasta el empoderamiento de la mujer a través de personajes como Shuri, Okoye y Nakia, interpretada por Lupita Nyong’o.

La memoria y la herencia, arraigadas a un territorio o sociedad, cobran igual fuerza a lo largo del filme a través de la visión de Killmonger,  antagonista de la cinta quien logra superar uno de los grandes problemas de las películas de Marvel como lo es la construcción de villanos. Estos son desperdiciados ya sea porque la historia se centra más en el héroe y se explota brevemente la personalidad del antagonista o bien, por la forma en que se desdibuja el carisma del mismo a pesar de contar con un exitoso elenco de actores con anterioridad: Hugo Weaving (Cráneo Rojo en Capitán América), Mickey Rourke ( Whiplash en Iron Man 2) o Ben Kingsley (el Mandarín en Iron Man 3), sin embargo, Killmonger interpretado por Michael B Jordan  - quien ya había trabajado anteriormente con Ryan Coogler en Creed - se convierte en un personaje con verdaderas motivaciones.

No es el típico villano que quiere la destrucción del mundo, sus fines son mucho más complejos, sin llegar a desarrollarse mantiene a lo largo del filme una postura claramente en contra de la esclavitud y la colonización, hasta cierto punto los más radicales podrían simpatizar con su causa a pesar que sus métodos no son los adecuados hasta ser considerado un anti héroe, sin embargo su propia visión de justicia terminará por sumirlo en la villanía.

Incluso si el traje de Pantera Negra es tan oscuro como la obsidiana, la película es un colorido esbozo de las raíces del continente cuna de la humanidad, Ruth Carter,  al mando de la dirección de vestuario de la película,  [en entrevista para The Atlantic](https://www.theatlantic.com/entertainment/archive/2018/02/why-fashion-is-key-to-understanding-the-world-of-black-panther/553157/)explicó que “no tenían realmente un modelo visual para la gente que vivía en Wakanda, así que fue una especie de fantasía o un lugar de la imaginación, fue muy intimidante, crear un mundo no es un chiste”.

Un equipo de más de 30 diseñadores viajó a diferentes ciudades africanas como Nairobi o Mumbai en búsqueda de vestidos, joyería y accesorios para lograr construir la visión de vestuario que Carter tenía en mente: una concepción afro del futuro. Es por eso que a pesar de ser una nación ficticia, la indumentaria de los wakandianos tiene mucho de realista, desde los diseños del pueblo Akan al sur de Ghana, la indumentaria de los Masai en Kenia y Tanzania  e incluso los sombreros del pueblo Zulú  de Sudáfrica.

[En el siguiente hilo de Twitter se destaca un poco mejor la relación entre Wakanda y la verdadera cultura africana.](https://twitter.com/diasporicblues/status/964772084906909696)

Además del vestuario, la fotografía que ofrece ciertos pasajes de la película  hará que los más nostálgicos puedan encontrar ciertas similitudes con clásicos como El Rey León y a un Mufasa dibujado en las nubes mientras habla de su legado a su hijo Simba.

Siempre me ha gustado destacar la forma en que la música permite viajar a una persona, en este caso a un espectador incluso si este cierra sus ojos. Desde que fue lanzado el primer trailer del filme, la música traía consigo mucha fuerza, en tan solo los tres minutos del trailer explora un poco del hip hop actual con un fuerte social con la canción de Gil Scott-Heron: "The Revolution Will Not Be Televised".

https://www.youtube.com/watch?v=qGaoXAwl9kw

Son varios los responsables de lograr este sonido único que permite a quien lo escucha transportarse a las sabanas africanas o al interior de una espesa selva. El OST original  está a cargo de Ludwig Goransson (Creed, Tropic Thunder) quien viajó a  Sudáfrica y Senegal para grabar con músicos locales la música del filme mientras Kendrick Lamar produjo un soundtrack titulado de forma homónima a la película.

Después de conocer acaudalados empresarios como Bruce Wayne y Tony Stark o personajes solitarios y caóticos como Wolverine o Bruce Banner, aparece un monarca de naturaleza prudente como  T’Challa que reúne el honor y liderazgo del Capitán América y la opulencia de Iron Man con la espiritualidad propia de la etnia africana.

https://www.youtube.com/watch?v=dO-lmUXiSs0

Pantera Negra más allá del traje y del hombre, es un símbolo, no es un simple héroe que viste su traje de vibranium para salir a patear traseros; tras él hay un sentido del deber y de proteger a su nación. Una película que se aleja del humor sin perder su carisma y que prefiere resaltar las problemáticas y coyunturas de la geopolítica, imponer la diplomacia sobre las muestras de xenofobia de ciertos dirigentes en pleno siglo XXI y sobre todo, sin importar el lugar en el que se esté, sentirse ligado para siempre a un territorio. Una celebración a nuestra identidad.

###### Encuentre más información sobre Cine en [24 Cuadros](https://archivo.contagioradio.com/programas/24-cuadros/) y los sábados a partir de las 11 a.m. en Contagio Radio 
