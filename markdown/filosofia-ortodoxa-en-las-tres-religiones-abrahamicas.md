Title: Filosofía ortodoxa en las tres religiones abrahámicas
Date: 2014-12-17 16:49
Author: CtgAdm
Category: Hablemos alguito
Slug: filosofia-ortodoxa-en-las-tres-religiones-abrahamicas
Status: published

[Audio](http://ctgaudio.megadatesystem.com/upload/0/fsyjV9fy.mp3)  
En un contexto mundial en el que observamos conflictos armados mediados por la religión, es obligatorio preguntarse, qué papel juega esta en el devenir de los mismos. Evidente es, que, son un factor importante dentro de muchos otros como causa y consecuencia. En el punto de mira investigamos sobre la filosofía ortodoxa dentro de las tres religiones abrahámicas (Judaísmo, Cristianismo e Islamismo) que lleva a miles de personas a la radicalización y al totalitarismo dando paso a la lucha armada de carácter violenta. Para ello entrevistamos a un experto en la temática, él es Misael Kuán Bahamon, jesuita e historiador de la Universidad Javeriana. Misael profundiza en las diferencias entre las tres religiones, su origen común, su implementación hasta el día de hoy, y su papel en los distintos conflictos armados.
