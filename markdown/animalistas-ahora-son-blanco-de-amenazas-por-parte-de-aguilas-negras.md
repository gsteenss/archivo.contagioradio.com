Title: Animalistas ahora son blanco de amenazas por parte de "Águilas Negras"
Date: 2015-03-11 19:23
Author: CtgAdm
Category: Animales, Nacional
Tags: Andrea Padilla, Animales, Animalistas, AnimaNatualis, Natalia Parra, Plataforma Alto
Slug: animalistas-ahora-son-blanco-de-amenazas-por-parte-de-aguilas-negras
Status: published

##### Foto: utadeo 

Líderes de organizaciones animalistas, ahora también son blanco de las amenazas por parte de “Águilas Negras”, esta vez, se trata de un panfleto enviado a los defensores de los animales, donde se les da 24 horas para abandonar el país.

**Natalia Parra Osorio, Carlos Crespo, Rodrigo Arian, Batman Camargo, Juan Parraga, Jesús Merchán, Andrea Padilla, Andrés Guzmán y por último el alcalde de Bogotá Gustavo Petro,** son los nombres de las personas amenazadas por el grupo.

El panfleto, acusa a los animalistas de ser “guerrilleros y defensores del terrorismo”, al hacer parte de diversas protestas sociales y así mismo, afirman que son promotores del comunismo.

[![IMG-20150311-WA0000](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/03/IMG-20150311-WA0000-768x1024.jpg){.aligncenter .wp-image-5827 width="421" height="561"}](https://archivo.contagioradio.com/animales/animalistas-ahora-son-blanco-de-amenazas-por-parte-de-aguilas-negras/attachment/img-20150311-wa0000/)
