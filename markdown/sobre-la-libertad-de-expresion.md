Title: Sobre la libertad de expresión
Date: 2015-04-30 13:33
Author: CtgAdm
Category: Cesar, Opinion
Tags: Boko Haram, Charlie Hebdo, El Malpensante, semanario Voz
Slug: sobre-la-libertad-de-expresion
Status: published

###### Foto: infobae.com 

#### Por [Cesar Torres del Río ](https://archivo.contagioradio.com/cesar-torres-del-rio/) 

¿ Más sobre Charlie Hebdo?[ ]{.Apple-converted-space}Por supuesto. La libertad de expresión está en el centro de lo social.

Soy partidario de la completa libertad de expresión, lo que incluye el derecho a la blasfemia. Como ciudadano mestizo libre, en tanto se pueda serlo dentro del capitalismo, apoyo todo aquello que exprese creatividad artística,[ ]{.Apple-converted-space}sentido del[ ]{.Apple-converted-space}arte, política profana, protesta cultural y desarraigo intelectual. Desde que tengo uso de razón nunca he sido racista, anti-semita (aunque sí anti-sionista) o islamofóbico; mucho menos anti-cristiano o anti-budista. La diversidad sexual y sus variadas manifestaciones[ ]{.Apple-converted-space}me indican el ser, simplemente.

A propósito del[ ]{.Apple-converted-space}crimen contra el intelecto humano y la sátira caricaturesca, personificado en Charlie Hebdo, me indignan aquellas expresiones del tipo de “las religiones deben respetarse y ellos se pasaron del límite …”, “no justifico el crimen, pero …”.[ ]{.Apple-converted-space}El acto terrorista mismo, así, se desdibuja. Por la misma vía me ensoberbecen[ ]{.Apple-converted-space}las visiones campistas que ven en el surgimiento de grupos *terroristas* como Boko Haram, Al-Qaeda o Estado Islámico una[ ]{.Apple-converted-space}forma de lucha *antiimperialista* contra la opresión colonial de Occidente; hasta llegan a apoyar (por ejemplo, el gobierno de Maduro en Venezuela) a un régimen criminal como el de Bashar Al-assad, en Siria, que ha gaseado químicamente a sirios y a kurdos y es el causante de crímenes de lesa humanidad, con el argumento de que antiimperialista.

La sinrazón de las posturas anteriores la encontramos también aquí en Colombia; y se puede apreciar en el semanario *Voz*, del Partido Comunista Colombiano. El 7 de enero de 2015 publicó el artículo “Yo no soy Charlie” en el que se afirma que la revista Charlie Hebdo se caracteriza por ser intolerante, racista y arrogantemente colonial. Mario Jursich, director de El Malpensante (www.elmalpensante.com), ha demolido la postura del autor de la, esa sí, intolerante y arrogante, columna que, además, deja a un lado[ ]{.Apple-converted-space}las expresiones de solidaridad con la revista francesa provenientes de migrantes africanos en Francia, así como de países árabes y del medio Oriente. Infortunadamente, la postura de los comunistas y sus colaboradores es excluyente y megalómana. El artículo en Voz no orienta; difama.

En medio de la libertad de expresión está, entre otros cientos, el asunto de las religiones. En el pasado fueron blanco de todo tipo de críticas, de exclusiones, por ejemplo el judaísmo y el islamismo, y de la sátira. Siguen siéndolo hoy. El “Todos somos musulmanes” (“Yo no soy Charlie”) oculta, coloca en un segundo plano, al fundamentalismo terrorista, opresor de los musulmanes y de la mujer.[ ]{.Apple-converted-space}La caricatura es solo caricatura; la utilización que de ella se haga es otro asunto. Los terroristas querían atacar al imperialismo colonialista; pero Charlie y su equipo editorial eran gente simpatizante del Partido Comunista y del anarquismo, antifascistas y anticolonialistas;[ ]{.Apple-converted-space}el funeral de Stéphane Charbonnier, director de la revista, se realizó cantando La Internacional. Con Charlie o sin Charlie el ataque terrorista estaba cantado. Lo detestable no es la caricatura; es la manifestación en apoyo a la “libertad de expresión” y de los valores de la “democracia” que hicieron Hollande, Merkel, Netanyahu, Zarkosy[ ]{.Apple-converted-space}y todos sus iguales.

Pero si por la caricatura[ ]{.Apple-converted-space}se llevan a cabo el ataque mortal y la condena mediática y se coloca la mordaza, el resultado es un nuevo tipo de fundamentalismo, que favorece los intereses políticos del imperialismo de Hollande, Merkel y sus congéneres.
