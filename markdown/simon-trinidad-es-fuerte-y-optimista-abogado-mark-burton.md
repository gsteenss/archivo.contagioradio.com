Title: Simón Trinidad podría estar en delegación de paz de las FARC: Mark Burton
Date: 2015-03-21 23:32
Author: CtgAdm
Category: Nacional, Paz
Tags: dialogos, EEUU, FARC, habana, mark burton, paz, presos, prisioneros, simon trinidad
Slug: simon-trinidad-es-fuerte-y-optimista-abogado-mark-burton
Status: published

###### Foto: Contagio Radio 

En el marco del Encuentro por la libertad de las y los prisioneros políticos en Colombia "Larga Vida a las Mariposas", el abogado del guerrillero de las FARC, Simón Trinidad quien se encuentra recluido actualmente en los EEUU, concedió una entrevista a Contagio Radio.

En ella habló sobre la manera en que asumió la defensa del líder guerrillero, su lectura de los juicios a los que fue sometido en los EEUU, su contacto con el colombiano, y las perspectivas de su repatriación y liberación.

\[embed\]https://www.youtube.com/watch?v=aXvgFaPNehE&feature=youtu.be\[/embed\]
