Title: Con plantón y linternas, ciudadanía exigirá la salida del fiscal
Date: 2019-01-10 15:47
Author: AdminContagio
Category: Movilización, Nacional
Tags: Movilización social, Nestor Humberto Martínez, Odebrecht
Slug: ciudadania-se-moviliza-para-exigir-la-renuncia-del-fiscal
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/01/Nestor-Humberto-Martinez-e1468345311281-770x400.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Primicidiario 

###### 10 Ene 2019 

[Este viernes 11 de enero diversos sectores sociales han convocado a la denominada ‘Noche de las Linternas’ un plantón frente a la sede de la Fiscalía que tiene como propósito exigir la renuncia del fiscal general, **Néstor Humberto Martínez Neira**, señalado de conocer sobre las irregularidades en las contrataciones de la Ruta del Sol y la firma brasileña Odebrecht.]

[Una de las promotoras del plantón, **Catherine Juvinao** explicó que a  partir del fallecimiento del ex "controller" de la Ruta del Sol, Jorge Enrique Pizano comenzó a gestarse un movimiento en redes sociales demandando la renuncia del fiscal general, una exigencia que trascendió a una movilización planteada por la constitucionalista Cielo Rusinque para el 11 de enero y  a la que se han unido diferentes sectores de la sociedad civil y diversos sindicatos.]

[El plantón tendrá lugar desde las 5:00 pm frente al bunker de la Fiscalía en Bogotá y en las diferentes sedes del ente acusador en varias ciudades del país, además el colectivo también organizó diversos puntos de encuentro frente a la sede del tribunal de La Haya en Holanda, el consulado colombiano en Miami y la plaza del Trocadero en Francia. [(Le puede interesar Fiscal Martínez debe declararse impedido para investigar caso Odebrecht).](https://archivo.contagioradio.com/fiscal-martinez-declararse-impedido-investigar-caso-odebrecht/)  
]

> Para todos los que nos han preguntado sobre los puntos de concentración en otras ciudades y fuera del país, aquí van algunos confirmados hasta ahora. Les iremos actualizando, ayúdenos por favor a rotar esta info.
>
> ¡Vamos con toda! [\#HagaseAUnLadoFiscal](https://twitter.com/hashtag/HagaseAUnLadoFiscal?src=hash&ref_src=twsrc%5Etfw) [\#E11](https://twitter.com/hashtag/E11?src=hash&ref_src=twsrc%5Etfw) [pic.twitter.com/qKoXHuL5gQ](https://t.co/qKoXHuL5gQ)
>
> — Catherine Juvinao C. (@CathyJuvinao) [10 de enero de 2019](https://twitter.com/CathyJuvinao/status/1083151604465979393?ref_src=twsrc%5Etfw)

<p>
<script src="https://platform.twitter.com/widgets.js" async charset="utf-8"></script>
</p>
[La iniciativa surge como reflejo de las multitudinarias protestas en las calles de Perú que exigían la renuncia del fiscal Pedro Chávarry, quien salió de su cargo al ser acusado de pertenecer a una red de corrupción judicial que también involucra a la firma brasileña,  “Perú es un ejemplo de **dignidad e inteligencia colectiva e**n donde la sociedad civil se pudo organizar” afirma la activista.]

[En contraste, Juvinao resalta la importancia de las movilizaciones sociales y el impacto que estás deberían tener sobre los servidores públicos, más en un país como Colombia donde “la clase política tradicional siente un menosprecio hacia el ciudadano común” y donde  es necesario aprender el concepto de ética pública para que los funcionarios   
  
La activista concluye que este plantón podría ser histórico al ser la primera vez que la sociedad colombiana se movilice contra un un servidor público en particular, “buscamos enviar un mensaje de luz, de esperanza, estamos cansados de la oscuridad y zozobra en el caso **Odebrecht**” a lo que agrega que el único actor civil que finalmente logrará que el fiscal Martínez abandone su cargo será la misma ciudadanía.]

###### [  
<iframe id="audio_31377238" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_31377238_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>  
]Reciba toda la información de Contagio Radio en [[su correo]
