Title: Cinemateca al parque la Fiesta de la imágen
Date: 2017-07-30 08:32
Author: AdminContagio
Category: 24 Cuadros
Tags: Bogotá, Cine, Cinemateca, eventos
Slug: cinemateca-al-parque
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/11/cinemateca-al-parque-e1447799390121.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Cinemateca Distrital 

###### 30 Jul 2017 

Espectadores, realizadores, productores tienen una cita con el cine este fin de semana. Llega a Bogotá la sexta versión de Cinemateca al Parque, un evento gratuito al aire libre que para este año trae una programación muy colombiana con música, arte y películas para toda la familia.

El domingo a partir de las 10 de la mañana, el recién inaugurado Parque Bicentenario de la capital recibirá varias producciones que han debutado en Importantes Festivales de Cine de todo el mundo. La primera proyección sera a las 11 am con la cinta animada "Anina", co producción colombo-uruguaya del año 2013.

La programación continua a la 1 de la tarde con el documental [La noche Herida](https://archivo.contagioradio.com/noche-herida-documental/), de Nicolás Rincón Gillé, que narra la historia de Blanca y sus nietos, quienes se enfrentan a las condiciones de la marginalidad urbana y del desarraigo en que viven en el barrio Verbenal de Bogotá, capital de Colombia.

A las 3 de la tarde se presentará la película X Quinientos, cinta que ha tenido un exitoso recorrido por más de 40 festivales del mundo y a las 5 p.m. el público podrá disfrutar del estreno de cortometrajes documentales y animaciones de la VI Temporada de Cinemateca Rodante.

Para el cierre de este evento, pronosticado para las 9 de la noche, se hará el preestreno del documental Amazona dirigido por Claire Weiskopf y producido por Nicolás Van Hemelryck, ganadora de un premio en el Festival Internacional de Documental y Cine de Animación de Leipzig, y la presentación de la agrupación musical 1280 almas.

**Otros eventos**

Ruta Audiovisual: carpas interactivas que le permitirán al público acercarse a la magia de la realización y la producción audiovisual con contenidos de realidad virtual y efectos especiales, exposición de Motion Capture. Los asistentes podrán recrear y grabar los sonidos de la escena de una película. Habrá también exposiciones y talleres sobre cine y realidad virtual, improvisación teatral y lectura de cuentos infantiles.

Domo portátil del Planetario de Bogotá:  los asistentes de todas las edades podrán observar las constelaciones y las principales estrellas que las forman, contemplar los planetas, el sol, los eclipses, las fases de la luna y la Vía Láctea y visualizar el primer cortometraje para full dome hecho en Colombia La forma del caracol, así como Cielos oscuros y Desde la Tierra hacia el Universo. El Planetario de Bogotá estará presente, además, para ofrecerle al público la posibilidad de observar el sol con un telescopio especial.

Concierto Experiencia Awaná del proyecto Nidos: una puesta en escena basada en el juego, un viaje musical por los sonidos de Latinoamérica y el Caribe y a través de la experiencia artística – El lápiz del proyecto Nidos los niños podrán jugar, explorar y crear a partir del punto y la línea.

Picnic Literario: Una lectura de película, habrá un espacio en el que las letras y la naturaleza se unen para ofrecer una experiencia única de lectura al aire libre.
