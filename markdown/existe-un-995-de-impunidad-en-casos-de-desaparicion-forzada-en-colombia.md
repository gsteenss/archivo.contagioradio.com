Title: Existe un 99,5% de impunidad en casos de desaparición forzada en Colombia
Date: 2019-08-27 15:27
Author: CtgAdm
Category: DDHH, Judicial
Tags: conflicto armado, Desaparición forzada, UBPD, Unidad de Búsqueda de personas dadas por desaparecidas
Slug: existe-un-995-de-impunidad-en-casos-de-desaparicion-forzada-en-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/08/Minga.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Asociación Minga] 

**Cartografía de la Desaparición Forzada en Colombia,** es una recopilación que sera lanzada este 28 de agosto y que reúne el trabajo desarrollado por investigadores durante tres años, a**cumulando 200 mapas departamentales con el objetivo de encontrar nuevos lenguajes** que permitan una mejor comprensión de un crimen que según el Centro Nacional de Memoria Histórica continúa sin identificar el paradero de 82.000 personas.

Erik Arellana, periodista y coautor de esta cartografía, afirma que la desaparición es abordada desde diferentes perspectivas, comenzando por las razones que se ha dado este fenómeno, un tema que desarrolla el periodista Paco Gómez Nadal, seguido de temas como  la impunidad, a cargo de la directora de la Corporación Jurídica Libertad, Adriana Arboleda, el rol de las víctimas tratado por la ambientalista Isabel Cristina Zuleta y Jesús Flórez, quienes también abordan los trabajos de memoria en los territorios.

La cartografía también realiza un análisis de las fosas comunes a cargo de la antropóloga Lee Douglas y un prólogo y epílogo del poeta Juan Manuel Roca, además, a este trabajo se suma la experiencia del propio Erik, quien viene trabajando desde hace 33 años en la denuncia y trabajo de incidencia entorno a este tema, como consecuencia de la desaparición y posterior asesinato de su mamá, Nydia Erika Bautista, en 1987. [(Le pude interesar: Nydia Erika Bautista: rostro de la desaparición forzada en Colombia)](https://archivo.contagioradio.com/nydia-erika-bautista-desaparicion-forzada-colombia/)

> [\#Cartografía](https://twitter.com/hashtag/Cartograf%C3%ADa?src=hash&ref_src=twsrc%5Etfw) | ¿Cómo conocer la magnitud de la desaparición forzada en el país?
>
> Por primera vez se recoge información geolocalizada desde el cruce de 4 fuentes de información oficial:
>
> (Hilo) ? [pic.twitter.com/qon39f5v9A](https://t.co/qon39f5v9A)
>
> — Unidad de Búsqueda UBPD (@ubpdbusqueda) [August 16, 2019](https://twitter.com/ubpdbusqueda/status/1162406413936877569?ref_src=twsrc%5Etfw)

<p>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
</p>
Uno de las problemáticas encontradas al abordar el tema es que **el Estado no ha unificado una cifra entorno a las desapariciones forzadas,** por lo que las cuatro organizaciones consultadas para la cartografía: el Registro Único de Víctimas, el Registro Nacional de Desaparecidos, el Sistema Penal Oral Acusatorio y el Centro Nacional de Memoria Histórica manejan diferentes datos y periodos de tiempo.

Al no saber el paradero de las personas, resulta todo un reto el comenzar a realizar trabajos en los territorios con información que no existe, lo que los llevó a abordar el tema a través de las denuncias que han recibido y que pueden ser de utilidad para la búsqueda.

### 2002, el año de mayor incremento en la desaparición forzada en Colombia

Arellana menciona algunos elementos a resaltar como el asunto de la impunidad, de los 80.000 casos, solo 337 han dejado una sentencia condenatoria, lo que equivale a un 99,5% de impunidad entorno a las investigaciones. Asímismo Guaviare y Antioquia fueron identificados como los departamentos más afectados por esta problemática en Colombia.

Este trabajo, que identifica la relación entre el conflicto armado y la desaparición, los cultivos de uso ilícito o los megaproyectos, esperan se convierta en un aporte para la Unidad de Búsqueda de Personas dadas por Desaparecidas (UBPD).  [(Lea también: En 2019 Unidad de Búsqueda de Desaparecidos llegará a 17 territorios)](https://archivo.contagioradio.com/unidad-de-busqueda-de-desaparecidos/)

La cartografía no solo se limita a exponer esta información, a su vez hace un análisis, y propone la búsqueda de nuevos lenguajes y herramientas para su interpretación y sobretodo "para apoyar el trabajo que han realizado las víctimas de esta problemática en el país", explica Arellana.

**Síguenos en Facebook:**  
<iframe id="audio_40608008" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_40608008_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>  
<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
