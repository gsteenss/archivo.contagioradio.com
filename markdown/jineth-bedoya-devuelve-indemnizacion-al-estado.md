Title: Jineth Bedoya devuelve indemnización al Estado colombiano
Date: 2016-05-11 14:23
Category: DDHH, Nacional
Tags: CIDH, conflicto armado, FLIP, Jineth Bedoya
Slug: jineth-bedoya-devuelve-indemnizacion-al-estado
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/05/Jinet-Bedoya.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: La Otra Cara 

###### [11 May 2016]

La periodista Jineth Bedoya Lima, devolvió al Estado **la indemnización administrativa recibida por ser víctima del conflicto armado** en los hechos que rodearon su secuestro, tortura y abuso sexual hace 16 años.

La periodista había denunciado el pasado 5 de abril ante la Comisión Interamericana de Derechos Humanos, que el Estado había intentado cerrar su caso a cambio de un cheque.

 “Sé a qué huele la guerra y la recuerdo todos los días, porque una mañana quedó grabada en mi cuerpo, lo más revictimizante, es que el Estado haya tenido la muy mala idea de llamarme el pasado 7 de enero a negociar para cerrar este caso, quiero creer que fue la buena intensión de alguien, pero señores del Estado: **la impunidad en ningún lugar del mundo puede ser negociable, hay algo que vale más que todos los cheques que pueda girar el Estado y eso es la dignidad y les aseguro que si algo me sobra es dignidad”**, aseguró  la periodista, [ante la Comisión Interamericana de Derechos Humanos en Washington](https://archivo.contagioradio.com/agentes-estatales-deben-ser-sancionados-por-crimen-contra-jineth-bedoya/).

De acuerdo con un comunicado emitido por la Fundación para la Libertad de Prensa, FLIP,  este miércoles  Bedoya decidió devolver esa suma de dinero a la Unidad Nacional de Atención y Reparación Integral a las Víctimas, a partir de una decisión autónoma motivada por “la enorme contradicción que existe desde las autoridades en el tratamiento de su caso. Por un lado, hay reconocimiento público de su lucha por la justicia, pero por otro, el Estado no considera tenga responsabilidad alguna”.

Aunque desde la Fiscalía se sostiene que las investigaciones han avanzado, lo cierto es que el caso todavía se encuentra impune, y los agentes estatales que participaron del hecho no han pagado su responsabilidad, teniendo en cuenta que, según la periodista, ese secuestro se realizó **a manos de alias 'El Panadero' y dos paramilitares más, en articulación con agentes de las Fuerzas Militares, La Policía y el INPEC**.

De acuerdo con Bedoya, el Estado colombiano debe hacerse responsable de las omisiones que se han presentado en su caso y que han llevado a la **pérdida de pruebas, información y expedientes determinantes, que ha impedido el esclarecimiento de la verdad**, la sanción de quienes planearon y ejecutaron este crimen en su contra, y las garantías de no repetición, teniendo en cuenta las amenazas que ha recibido recientemente.

La FLIP, señala que la entrega de una indemnización en la situación en la que se encuentra el caso, implica desconocer la verdad de los hechos y la responsabilidad del Estado en ellos, además, no solo no aporta a la justicia, sino que desvaloriza las reparaciones del valor re-dignificante para las víctimas.

Es así como se espera que el caso llegue a la ** Corte Interamericana de Derechos Humanos y se declare la responsabilidad del Estado colombiano por violación de los derechos humanos de la periodista,** quien más allá de buscar indemnizaciones a indicado en repetidas ocasiones que los que quiere es verdad frente a lo sucedido.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)
