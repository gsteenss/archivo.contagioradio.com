Title: España: miles de personas se manifiestan contra la¨ley mordaza¨
Date: 2015-02-14 17:07
Author: CtgAdm
Category: El mundo, Otra Mirada
Tags: españa, ley de seguridad ciudadana, ley mordaza
Slug: espana-miles-de-personas-se-manifiestan-contra-la%c2%a8ley-mordaza%c2%a8
Status: published

###### **Foto:radiomalva.org** 

Más de **20 ciudades de todo el estado español han salido a la calle para protestar contra la ley de seguridad ciudadana, conocida como ¨ley mordaza¨**, aprobada entre los dos grandes partidos, PP y PSOE.

Convocadas por la plataforma **No somos delito, cientos de organizaciones sociales de derechos humanos han criticado fuertemente la ley y han exhortado al gobierno a derogarla, porque en su opinión es anticosntitucional** y además ya ha sido advertido por tribunales como el de derechos humanos de Estrasburgo o la ONU que es incompatible con las propias libertades.

**Greenpeace ha logrado sacar a más de 6.000 militantes a las calles**, ya que esta ley afecta principalmente a la desobediencia civil activa y pacífica, que es la forma de lucha principal de la histórica organización.

Por otro lado la mayoría de organizaciones sociales asistentes han argumentado que esta ley responde a que los propios **jueces no están haciendo cumplir todas las demandas y montajes judiciales con los que esta actuando el gobierno en contra de la protesta social, ya que los consideran un atropello contra la libertad de expresión.**

Todos los partidos políticos excepto el PP y el PSOE están en la coalición y forman parte de un frente común para frenar la ley de carácter represivo.

Por último es importante remarcar que esta ley reprime a los sectores más débiles de la sociedad convirtiendo delitos en faltas graves, permitiendo así la entrada en prisión de los denunciados.
