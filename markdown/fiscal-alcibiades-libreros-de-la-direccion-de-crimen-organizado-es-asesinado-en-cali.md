Title: Fiscal Alcibíades Libreros de la Dirección de Crimen Organizado es asesinado en Cali
Date: 2019-12-29 17:44
Author: CtgAdm
Category: DDHH, Nacional
Tags: Cali, crimen organizado, Fiscal
Slug: fiscal-alcibiades-libreros-de-la-direccion-de-crimen-organizado-es-asesinado-en-cali
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/Alcibíades-Libreros.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

El hecho sucedió en la tarde de hoy en el sur de Cali cuando hombres en moto dispararon contra **Alcibíades Libreros, Fiscal Especializado** e integrante de la Dirección de Crimen Organizado en esta ciudad.

El funcionario de La Fiscalía trabajaba en casos contra bandas delincuenciales como ‘la Cordillera’ del Eje Cafetero y en la investigación contra integrantes de Los Rastrojos, entre ellos **Óscar Darío Restrepo Rosero, alias ‘Porrón’, cabecilla de la banda; así mismo contra oficinas de cobro.**

Según el comunicado de **Asonal Judicial Cali** el hecho sucedió en la calle 5 con carrera 66 al sur de la ciudad, "cuando individuos que se movilizaban en motocicleta accionaron armas de fuego contra la humanidad del compañero  y emprendieron la huida con rumbo desconocido, **Alcibíades** murió en el sitio de los hechos, al interior del vehículo en el que se movilizaba" **Ver: [Asesinan a Fabian Espinosa, tesorero de Asonal Judicial en Cucuta](https://archivo.contagioradio.com/asesinan-a-fabian-espinosa-tesorero-de-asonal-judicial-en-cucuta/)**

El crimen contra el jurista fue confirmado por Andrés Villamizar,  Secretario de Seguridad y Justicia de Cali desde su cuenta de twitter.

> Lamentamos y condenamos el asesinato del Fiscal contra el crimen organizado **Alcibiades Libreros Varela** ocurrido hoy en Cali. Fue un hombre implacable en la lucha contra las estructuras criminales. Mis condolencias a su familia y a sus compañeros de la [@FiscaliaCol](https://twitter.com/FiscaliaCol?ref_src=twsrc%5Etfw).
>
> — Andrés Villamizar (@villamizar) [29 de diciembre de 2019](https://twitter.com/villamizar/status/1211384501189910534?ref_src=twsrc%5Etfw)

<p style="text-align: justify;">
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
</p>
A través del comunicado Asonal Judicial lamentó los hechos y clamó para que se investigue con prontitud el crimen del Fiscal  "clamamos por la pronta identificación de los responsables tanto materiales como intelectuales de este vil crimen" e igualmente solicitaron al Estado dar mayor protección a los funcionarios y empleados judiciales.
