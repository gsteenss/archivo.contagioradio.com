Title: Cerca de 700 desplazados deja incursión paramilitar de AGC en Alto Baudó
Date: 2017-03-05 11:11
Category: Entrevistas
Slug: cerca-de-700-desplazados-deja-incursion-paramilitar-en-alto-baudo-choco
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/07/Autodefensas-Gaitanistas.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: contagioradio] 

###### [05 Mar 2017] 

Por lo menos 700 personas, habitantes de la cuenca de Peña Azul en el municipio del Alto Baudó, Chocó, se habrían visto obligadas a desplazarse de sus hogares, luego la incursión paramilitar en sus territorios. Cerca de 200 paramilitares que utilizaban brazaletes que los identifican como integrantes de las **Atodefensas Gaitanistas de Colombia, AGC, incursionaron en las comunidades.**

Según la información proporcionada por la Comisión de Derechos Humanos del Congreso de los Pueblos, en horas de la mañana los **paramilitares de las AGC habrían ingresado a las comunidades disparando indiscriminadamente contra la población**, lo que provocó un enfrentamiento con integrantes el ELN que se encontraban cerca de las comunidades. [(Lea también: 97 familias desplazadas por paramilitares de las Autodefensas Gaitanistas](https://archivo.contagioradio.com/97-familias-desplazadas-en-bahia-solano-por-presencia-de-autodefensas-gaitanistas-de-colombia/))

La denuncia enfatiza que aún se desconoce el número de personas heridas o de víctimas fatales y que los enfrentamientos entre AGC y ELN perduraron hasta altas horas de la noche. **También se desconoce el paradero de las familias afrodescendientes** que huyeron hacia sitios despoblados y no han llegado al casco urbano de Pié de Pató.

Actualmente, de acuerdo con el censo realizado por la Cruz Roja Colombiana y la Personería de Píe de Pato, hay 340 personas en la casa de Acción Social del municipio del Alto Baudó, entre estas personas se encuentran **84 niño, 93 niñas, 3 mujeres en embarazo, 17 mujeres lactantes y 14 adultos mayores. **

Las comunidades afectadas serían habitantes de las veredas de Batatal, Las Declicias, Puerto Misael, Boca de Leon, Punta de Peña y Puerto Cardozo, entre otros que en este momento están confinadas y a la espera de la **presencia de organismos del Estados para que se presten las ayudas de emergencia pertienentes.**

<iframe id="audio_17381548" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_17381548_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
