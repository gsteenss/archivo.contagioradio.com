Title: 83 familias a punto de ser desalojadas en Puerto Lleras, Meta
Date: 2016-12-10 14:50
Category: Nacional
Tags: colombia, Desalojo, Familias, Meta, Puerto Lleras
Slug: campesinos-desalojo-puerto-lleras-meta
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/Desplazados-e1481538333709.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### Foto: Archivo Contagio 

##### 10 Dic 2016 

**La inspección de policía del municipio de Puerto Lleras, Meta, anunció que el próximo 12 de diciembre realizará un desalojo** en los predios denominados Guichiral, Mata de agua, Las Abras, Panjuil, entre otros, acción con la que se  revictimiza a los campesinos, muchos de los cuales llegaron a esa zona por cuenta del desplazamiento y despojo forzado.

La decisión que dejaría sin hogar a cerca de 83 familias, integradas en su mayoría por madres cabeza de familia, **obedecería a una querella interpuesta por el grupo Palma Ariari S.A.**, que viene reclamando desde el mes de abril de 2015 la propiedad sobre los predios en los que se establecieron las comunidades pensando que se trataba de baldíos de la nación.

La comunidad resalta que en ese entonces, el personero municipal registró irregularidades en el procedimiento policivo realizado en cabeza de la inspectora Elsa del Carmen Salcedo en compañía del abogado de la empresa Álvaro Ballesteros, donde, al igual que ahora, **no se realizó una identificación plena del predio ni de las personas que serían objeto del desalojo**, advirtiendo en su informe que varias familias campesinas habían sido agredidas y sus cultivos, viviendas y algunas herramientas estropeadas.

En su momento **los afectados presentaron denuncia ante la Fiscalía sin que se tomaran medidas al respecto**, por el contrario la comunidad observó un incremento en la presencia de agentes policiales que, según denuncias de los pobladores, amedrentaban, amenazaban, y ejercían abuso de su autoridad sobre la población civil. Le puede interesar: [Tribunal confirma sentencia contra 16 empresarios y paramilitares por despojo de tierras](https://archivo.contagioradio.com/tribunal-confirma-sentencia-contra-16-empresarios-y-paramilitares-por-despojo-de-tierras/).

De acuerdo con Francisco Henao, abogado de la Corporación Yira Castro, por medio de diferentes oficios enviados desde la defensa de la comunidad, se aclaraba que "**el procedimiento que ellos pensaban hacer era completamente irregular toda vez que no se tuvo en cuenta que hay familias víctimas, hay población vulnerable, adultos mayores, niños mujeres**", a lo que tanto el alcalde, el enlace de vítimas como la personería ha sido que el desalojo "va por que va"

Ante la actual situación, **la comunidad se ha reunido en dos oportunidades con el alcalde del municipio John Triana para encontrar soluciones** que eviten dejar sin hogar a los habitantes, en la segunda con presencia de la Defensoría del Pueblo, entidad que reiteró las falencias en los procedimientos policiales y la falta de falta de un plan de contingencia para la comunidad, **advertencias que los pobladores aseguran no han sido escuchadas por el mandatario local**.

Desde organizaciones como la Corporación Jurìdica Yira Castro, se vienen haciendo llamados para que **no se permita la revictimización de los pobladores** y que no se realicen acciones de desalojo que pasen por encima de las garantías mínimas que deben darse por ley, mientras que los afectados exigen se les de un acompañamiento de las entidades de control y las autoridades competentes para evitar que los intereses particulares primen sobre los derechos colectivos de los campesinos.

 
