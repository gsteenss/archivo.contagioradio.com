Title: Boletín informativo Julio 3
Date: 2015-07-03 17:28
Category: datos
Tags: Camila Moreno, Constituyente Nacional de cultivadores de Coca, Espionaje a Amnistía internacionl por parte del Gobierno Británico, FARC ataca policias en el corregimiento de "El Mango", Fernando Giraldo, Marcela Muñoz, Mecanismo de justicia transicional proceso de paz en Colombia, Resultados encuesta Gallup segundo semestre 2015
Slug: boletin-informativo-julio-3
Status: published

[*Noticias del Día:*]

<iframe src="http://www.ivoox.com/player_ek_4720301_2_1.html?data=lZyfkpiUdY6ZmKiakpqJd6KokZKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRhtDgxtmSpZiJhaXijK7byNTWscLoytvcjcnJb6vpzc7cjZiRaZi3jqjc0NnFq8rjjLfOxs7Tb46ZmKialg%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

-Esta semana fueron publicados los resultados de la **encuesta Gallup** para el Segundo trimestre de 2015. La encuesta contempla **estados de ánimo**, gestión del gobierno nacional y gobiernos locales, favorabilidad de personajes, aprobación de algunas propuestas, favorabilidad de instituciones y proceso de paz. Afortunadamente **las encuestas evalúan estados de ánimo** y ellos son cambiantes de acuerdo a la coyuntura, señala el politólogo **Fernando Giraldo**.

-El punto crucial de la construcción de un **mecanismo de Justicia Transicional** está en la **voluntad política** para reconocer las **responsabilidades que todas las partes tienen en la guerra en Colombia**. También es necesario tener en cuenta que el aparato de Justicia del Estado ha investigado, juzgado y penalizado la mayoría de los crímenes imputados a las guerrillas, afirma **Camila Moreno**, directora del Centro Internacional de justicia transicional.

-El 1 de julio, el Tribunal británico reveló que **Amnistía Internacional** habría sido **víctima de espionaje** mediante el acceso y almacenamiento de sus comunicaciones por parte del **Gobierno Británico**. Tras **18** meses de litigio se confirmó la vigilancia masiva a este movimiento global de derechos humanos. Según una comunicación del Tribunal sobre competencias indagatorias, **Amnistía Internacional** y el **Centro de Recursos Jurídicos de Sudáfrica**, habrían sido víctimas del mismo.

-Diversas **organizaciones sociales** del país se darán cita del 4 al 6 de julio en Mocoa, Putumayo, en la **Constituyente Nacional de cultivadores de Coca, Amapola y Marihuana**. Un espacio para el debate entorno a la construcción de una **propuesta de política antidrogas**, iniciativa que sería enviada a la Habana donde se desarrollan las conversaciones de paz entre el Gobierno y las FARC E-P. **Marcela Muñoz**, integrante del equipo técnico de la comisión de sustitución de cultivos de uso ilícito de la Mesa regional de organizaciones sociales del Putumayo.

-Este viernes en horas de la madrugada la guerrilla de **las FARC - EP atacó a las unidades policiales** que se ubicaron en el punto conocido como El Cedro, corregimiento de "**El Mango"**, ubicado al lado de la población civil en el departamento del Cauca, lo que nuevamente ocasionó **temor en la comunidad**, como o cuenta uno de los habitantes del corregimiento "El Mango".
