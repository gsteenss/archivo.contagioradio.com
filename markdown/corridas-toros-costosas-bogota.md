Title: Las corridas de toros: Muy costosas y no dejan beneficios a Bogotá
Date: 2018-09-12 12:41
Author: AdminContagio
Category: Ambiente, Movilización
Tags: Corporación Taurina, corridas de toros, Plataforma Alto, Plaza de Toros
Slug: corridas-toros-costosas-bogota
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/01/toros-en-bogota.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Yecid Lancheros] 

###### [11 Sept 2018] 

El proceso de licitación mediante el  cual se alquilará la plaza de la **Santamaría de Bogotá** para que se realicen corridas de toros y novilladas durante la temporada taurina del próximo año, perjudica económicamente al distrito según lo viene denunciando desde el movimiento animalista, debido a que estos eventos no se traducen en un ingreso económico para la Ciudad, y sí, un gran gasto, según la denuncia de **Natalia Parra, vocera de la Plataforma ALTO.**

Estas actividades volverán tras la sentencia de la **Corte Constitucional,** que anuló el fallo emitido por el mismo tribunal, con el que se ordenaba al Congreso legislar sobre el tema, empujando al Distrito a garantizar la realización de estos espectáculos. (Le puede interesar: ["Hace falta la decisión del Senado para que se prohiban las corridas de toros en Colombia"](https://archivo.contagioradio.com/hace-falta-la-decision-del-senado-para-que-prohiban-las-corridas-de-toros-en-colombia/))

### [**Corridas de Toros: Sin IVA y con grandes privilegios**] 

Parra, señaló que el reparo por parte de las organizaciones animalistas tiene que ver con el favorecimiento de quienes organizan las corridas de toros; por ejemplo, **"ellos están exentos de IVA, porque las corridas no deben pagar este gravamen",** además, pese a que el Código de Policía prohíbe el consumo de alcohol en aglomeraciones públicas, durante las corridas es una práctica común.

La activista recordó que desde que salieron los pliegos del IDRD para alquilar la plaza, tuvieron reparos porque **"favorecieron mucho a la Corporación Taurina de Bogotá,** que es la única que se puede presentar porque la Plaza es de primera categoría, y dichos pliegos se ajustan a los equilibrios económicos de la Corporación". Además, porque el alquiler del escenario se está pagando sobre un porcentaje de la boletería vendida y no con un canon fijo.

Para la vocera de la Plataforma ALTO, **resulta ilógico que no se cobre un monto fijo por el alquiler de este lugar, pese a que las corridas sí tienen costos fijos asociados como el de la seguridad que presta la Policía a los taurinos. **Del mismo modo, denunció que mientras la Plaza estuvo asignada a la Corporación Taurina, "los impuestos que debían pagar, nunca los pagaron, hasta que precluyó el cobro de ese rubro".

### **Las corridas de toros se acabarán en un momento, pero ¿cuántos animales deben morir antes que eso ocurra?** 

Parra aseguró que la Corte pidió que se desincentivaran las corridas de toros, novilladas y peleas de gallos, y recordó que este tribunal reconoció que allí había maltrato animal, por lo tanto, debía ser desestimulado. La animalista sostuvo que aunque está segura de que las corridas de toros se acabarán en un algún momento se pregunta "**cuántos animales deben morir antes que eso ocurra**".

La activista recordó que en el pasado, el Distrito uso a la Plaza para eventos culturales como Opera al Parque, el Festival de Coros,  el Festival Rafael Pombo o la Pista de Patinaje en Hielo, por lo tanto, hay alternativas culturales para las que puede ser alquilada la Santamaría. (Le puede interesar:["Radicado Proyecto de Ley que que prohibiría el fracking en Colombia"](https://archivo.contagioradio.com/listo-proyecto-de-ley-que-prohibiria-el-fracking-en-colombia/))

<iframe id="audio_28500082" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_28500082_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en] [[su correo][[Contagio Radio]
