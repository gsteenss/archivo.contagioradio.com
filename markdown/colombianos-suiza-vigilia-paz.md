Title: Suiza realizó vigilia por la paz con justicia social en Colombia
Date: 2016-11-08 15:59
Category: yoreporto
Tags: Colombianos, ELN, FARC, paz, suiza, Vigilia
Slug: colombianos-suiza-vigilia-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/marcha-en-ginebra.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Contagio Radio 

##### 8 Nov 2016 

Desafiando la lluvia y el frio otoñal un centenar de colombianos reunidos en la “**Plataforma Colombianos en Suiza por la Paz**” realizó la tarde noche de este sábado 5 de noviembre en **Ginebra, Suiza**, una vigilia pidiendo la **pronta implementación de los acuerdos de paz con justicia social**, los cuales fueron firmados entre el gobierno de este país y la insurgencia de las Farc-EP, así como el **pronto inicio de la mesa de dialogó con ELN en Ecuador**.

**Dirigentes colombianos de diversas organizaciones políticas, sociales, profesionales, exiliados políticos perseguidos por el gobierno de ese país y migrantes** que confluyen en la “Plataforma Colombianos en Suiza por la Paz” y que residen en la región helvética de Ginebra, Lausanne y la frontera franco-suiza, **encendieron luces, entonaron canciones y elevaron plegarias por la paz con justicia social** y exigieron la pronta implementación de los acuerdos citados.

La convocatoria que reunió a los colombianos **exige que se preserve el corazón de los acuerdos que están en etapa de ajustes y de pasó sirvió para comenzar a preparar la próxima gran vigilia por la paz a realizarse a fines de este mes** tanto en Colombia como a nivel internacional y donde los colombianos residentes en este país europeo esperan estar presentes con gran entusiasmo. Le puede interesar: [Demócratas urgen por acuerdo de paz en Colombia](https://archivo.contagioradio.com/democratas-urgen-por-acuerdo-de-paz/).

**Por Eliécer Jiménez Julio desde Ginebra, Suiza. \#YoReporto**
