Title: Debemos trabajar de manera unida para protegernos de la crisis económica mundial: Evo Morales
Date: 2015-12-21 16:11
Category: Economía, El mundo
Tags: Economía en Bolivia mejores indicadores de América Latina, Evo Morales intervención Mercosur, Mercosur
Slug: debemos-trabajar-de-manera-unida-para-protegernos-de-la-crisis-economica-mundial-evo-morales
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/12/155715_561c26d8cf0d4.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### <iframe src="http://www.ivoox.com/player_ek_9806443_2_1.html?data=mp2dmJmYd46ZmKiak56Jd6KolpKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRiMbWxtLc1ZDYtsLWws%2FO1JDIqYzhwtPS1MaPuc%2FdxcaY0sbWpYzk09Thx8zJts%2Fj1JDRx5DQpY6ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe> 

##### [Evo Morales, Presidente Bolivia] 

##### 21, Dic 2015 

En su intervención durante la 49 cumbre de países miembros de Mercosur, el presidente de Bolivia Evo Morales, hizo un llamado a la integración para enfrentar la crisis económica mundial, transferida por los países desarrollados a las naciones latinoamericanas, que viene afectando la estabilidad de la región.

El mandatario manifestó que la intención de los países del norte es "abaratar los precios de las materias primas que producen las economías del sur con el objetivo de abaratar costos de producción" para que sean los trabajadores, campesinos y profesionales "los que soporten los costos de la reactivación del aparato productivo de los países más desarrollados".

Para Morales, la situación esta poniendo en riesgo "las conquistas sociales, el crecimiento económico y la exitosa lucha contra la pobreza que nuestro continente tuvo durante la última década" lo que esta provocando problemas sociales en cada país y de paso inestabilidad política en el continente.

De ahí el enfásis que hace el mandatario en convocar a sus similares a dar una respuesta colectiva continental "que presente los avances sociales, que garantice la estabilidad democrática, sin estabilidad y crecimiento económico no hay estabilidad social y menos política" que permita proteger las naciones de la crisis para garantizar su crecimiento y la justa distribución de la riqueza.

La gestión de Morales en materia económica, reconocida recientemente por el Banco Mundial como la de mejores indicadores económicos de la región, se deben en gran parte a la apuesta política de "combinar el mercado mundial y el mercado interno" lo que le permitiría según ese organismo afrontar sin dificultad la crisis económica mundial en 2016.

En su discurso el presidente boliviano se refirió además a las intervenciones militares en medio oriente como parte de  ajustar sus propias economías en crisis por la vía de la guerra y controlar las fuentes de energía, e instó a sus colegas a abordar el tema medio ambiental. Bolivia tramitá actualmente su ingreso como miembro pleno del bloque.

   
 
