Title: Revisión de indultos al M-19 no se puede decidir de forma unilateral: Navarro Wolff
Date: 2015-11-18 11:53
Category: Entrevistas, Judicial
Tags: dialogos de paz, Diálogos de paz en la Habana, Eduardo Montealegre, Fiscal Eduardo Montealegre, Fiscal General, Fiscal General de la Nación, M19
Slug: revision-de-indultos-al-m19-no-se-puede-decidir-de-forma-unilateral-antonio-navarro-wolff
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/11/NAVARRO.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto:cmi.com.co 

<iframe src="http://www.ivoox.com/player_ek_9430192_2_1.html?data=mpmgkpaddo6ZmKiakpeJd6KpmZKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRlsbqytjWh6iXaaOnz5DRx5DNssXpzdnc1ZDFsIzBjpamjdPTb9TZjNXix8nJb8XZxM7Ry9ePqMafx9Sah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Antonio Navarro Wolf, Congresista] 

###### [18 Nov 2015, ]

El anuncio del Fiscal Eduardo Montealegre sobre revisar los indultos que se le otorgaron al M-19 en 1990 para investigar si se cometieron crímenes de guerra o delitos de lesa humanidad en la toma al Palacio de Justicia, han despertado posiciones en contra incluyendo la de los cuatro exmiembros de esta guerrilla que serían llamados a declarar, razón por la que decidieron enviar una carta al presidente Santos, haciendo evidente su inconformismo.

Antonio Navarro Wolff , senador de la Alianza Verde, señala que e**ste acuerdo que se realizó en 1990 ha sido considerado “un ejemplo” en materia de procesos de paz en el mundo, así mismo agrega que **hace 25 años cuando se pactó esta serie de indultos “nadie objetó” y a **pesar de que se le hizo una revisión “quedó en firme” el acuerdo**.

Navarro indica **que esta decisión no puede tomarse de “manera unilateral”**, causando una “inestabilidad jurídica” en el proceso lo cual es **contrario a lo que las dos partes firmaron, de manera que no debe incumplirse**, afirma Navarro. Para él, este planteamiento unilateral se presenta de forma “muy inconveniente” cuando los exintegrantes del M-19 han cumplido con sus compromisos.

Por su parte, desde la Cumbre del Foro Cooperación Económica Asia-Pacífico (APEC) en Manila, Filipinas, el presidente Santos manifestó que lo que el Fiscal está haciendo contra el M-19 es poco oportuno, ya que de acuerdo al mandatario, una jurisdicción especial de paz da la seguridad jurídica para que en el futuro no se reabran procesos penales si su gobierno logra sellar la paz con la guerrilla de las FARC. "**Yo creo que hay que respetarle el acuerdo al M-19. Es un acuerdo que hizo el Estado colombiano con el M-19, y ese acuerdo hay que respetarlo", dijo el jefe de Estado.**
