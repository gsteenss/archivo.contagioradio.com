Title: La mosca que soñaba que era un águila - Augusto Monterroso
Date: 2015-01-24 00:35
Author: CtgAdm
Category: Viaje Literario
Tags: Augusto Monterroso, literatura, poesia
Slug: la-mosca-que-sonaba-que-era-un-aguila-augusto-monterroso
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/01/monterroso.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<iframe src="http://www.ivoox.com/player_ej_3992260_2_1.html?data=lJ6mlJeadI6ZmKiakpmJd6KkmJKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRkMKfztTgxcaPtdbZjNjch6iXaaOlwsfOjdbZqYzZ08aY19OPaaSnhqaeydrNsMKfjpCu18zZt9XjjLLc0NnJcYarpJKw0dPYpcjd0JC%2Fw8nNs4yhhpywj5k%3D&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

Augusto Monterroso nació en Tegucigalpa, 21 de diciembre de 1921  y falleció en Ciudad de México, 7 de febrero de 2003, fue un escritor hispanoamericano conocido por sus relatos breves e hiperbreves. Es considerado como uno de los maestros de la mini-ficción y, de forma breve, aborda temáticas complejas y fascinantes, con una provocadora visión del mundo en el universo y una narrativa que deleita a los lectores más exigentes, haciendo habitual la sustitución del nombre por el apócope. Entre sus libros destacan además: La oveja negra y demás fábulas (1969), Movimiento perpetuo (1972), la novela Lo demás es silencio (1978); Viaje al centro de la fábula (conversaciones, 1981); La palabra mágica (1983) y La letra e: fragmentos de un diario (1987). En 1998 publicó su colección de ensayos La vaca. En 2000 le fue concedido el Premio Príncipe de Asturias de las Letras en reconocimiento a toda su carrera.
