Title: El antimilitarismo como apuesta hacia la cultura de paz: caminando hacia la justicia y la igualdad
Date: 2019-08-24 13:28
Author: JUSTAPAZ
Category: Opinion
Tags: antiilitarismo, paz, RAMALC, Red Antimilitarista de América Latina
Slug: 72501-el-antimilitarismo-como-apuesta-hacia-la-cultura-de-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/08/arton6053.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: antimilitaristas.org 

[Colombia atraviesa por un proceso de transición política y social sin precedentes en su historia reciente, aún en medio de los diversos discursos de paz, nos enfrentamos a una serie de políticas sociales y económicas que favorecen a los poderes económicos y militares en detrimento de los sectores más vulnerables, impuesto] [con un creciente militarismo de los territorios y la militarización de nuestras vidas.]

[En Colombia, el reclutamiento legal e ilegal hace parte de la estrategia de militarización de los individuos, que se beneficia] [en la desigualdad social,  llevando a que muchas personas se enlisten en los ejércitos, a reproducir valores militares y a construir masculinidades hegemónicas. ]

[El momento histórico por el que atraviesa la sociedad colombiana es una oportunidad para problematizar las diversas formas de militarización y su relación con los diferentes ejercicios de violencia en los ámbitos de la vida de las personas. Además, para visibilizar] [las experiencias de resistencia que han sido acalladas en medio de un conflicto armado interno con diferentes actores armados legales e ilegales.]

[Para construir una paz integral y sustentable es necesario resistir la militarización social, política y económica y al mismo tiempo construir alternativas desde la noviolencia. Por esta razón, una coalición de organizaciones y procesos de la sociedad civil en Colombia, junto a la Red Antimilitarista de América Latina y el Caribe (RAMALC) y la Internacional de Resistentes a la Guerra (IRG) realizaron un espacio de encuentro para visibilizar experiencias colectivas que resisten y desarrollan alternativas a la creciente militarización de los territorios, más allá de los discursos de “paz” que ocultan las condiciones sociales, políticas y económicas de vulnerabilidad e injusticia, que son el soporte fundamental de la guerra. ]

[La Conferencia Internacional:]*[Antimilitarismos en Movimiento: Narrativas de resistencia a la guerra]*[, reunió a más de 150 personas de: Inglaterra, Estados Unidos, Nicaragua, México, Chile, Sudán del Sur, España, Corea del Sur, Alemania, Palestina y otros 20 países del mundo en Bogotá, durante el 30, 31 de agosto y 1 de julio,] [quienes desde experiencias antimilitaristas y de noviolencia realizaron un análisis de los impactos de la militarización y el militarismo en la vida y los territorios en los contextos locales, regionales y globales. ]

[Allí, se abordaron mesas temáticas en torno a las paces justas, sustentables y diversas, desde las cuales se generaron reflexiones en torno a la relación entre la militarización, el ambiente, el territorio, el desarrollo, el militarismo en la cultura, la interseccionalidad, los modelos represivos y las transiciones políticas en situación de conflicto. En el desarrollo de estas mesas se identificaron similitudes en las prácticas de militarización y las diferentes expresiones de resistencia frente a ella. También, se formularon estrategias conjuntas desde la incidencia, formación, investigación y comunicación a nivel local, regional y global para promover diferentes alternativas de resistencia tanto a las causas como a las consecuencias del militarismo y la militarización de nuestras vidas. ]

[Una apuesta clara para avanzar en la consolidación de una cultura de paz en nuestro país es trabajar por la eliminación de la obligatoriedad del servicio militar. Obligar a una persona a pertenecer a un grupo armado (legal o ilegal) y utilizar armas, es un acto inhumano que contradice la libertad de expresión y pensamiento, atenta contra el libre desarrollo de la personalidad y somete a la sociedad a una lógica guerrerista, que poco aporta a la democracia y la construcción de paz. Colombia, tiene mucho que ofrecer a estos procesos, a lo largo del conflicto, diferentes comunidades de paz han resistido a la militarización de sus tierras mientras exploran nuevas formas de trabajar y vivir juntas. ]

[El intercambio de experiencias y construcciones no se supedita a Colombia, toda vez que hay comunidades, colectivos y organizaciones de todo el mundo que buscan formas de transformar su realidad, alejándose de las culturas inmersas en la militarización, en formas de vida que promuevan justicia e igualdad.]

[En estos procesos es clave promover el empoderamiento social y colectivo desde abajo, el fortalecimiento o la construcción de comunidad, y tanto una visión al largo plazo como mejoras concretas de la vida de las personas al corto plazo. Es importante mantener una resistencia antimilitarista como mecanismo que aporte a la consolidación de la paz, desde la justicia, la igualdad y la sostenibilidad.]

[Por: Wendy Ramos - JUSTAPAZ. ]
