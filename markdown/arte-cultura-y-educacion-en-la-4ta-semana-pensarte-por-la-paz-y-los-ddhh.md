Title: Arte, cultura y educación en la 4ta Semana "Pensarte" por la paz y los DDHH
Date: 2015-09-02 17:00
Category: Cultura, Hablemos alguito
Tags: Agencia Diakonia, Cinemateca Distrital de Bogotá, colectivo de Abogados José Alvear Restrepo, Corporación Yurupari, Premio Nacional a la Defensa de los Derechos HUmanos, Semana cultural Pensarte
Slug: arte-cultura-y-educacion-en-la-4ta-semana-pensarte-por-la-paz-y-los-ddhh
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/PRESENTACIONPENSARTE2015.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<iframe src="http://www.ivoox.com/player_ek_7759009_2_1.html?data=mJyim5WUfY6ZmKial52Jd6KpmpKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRh9bg1drfw4qWh4zV09nSjd6PqcXpxMbQy4qnd4a2lNOYx9OPsMKfldnOjbjJscLiwpCSlJe0qc%2Fnwtfhx4qWdoyhhpywj6jTstXVyM7cjbfFqMrjjJKSmaiReA%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [John Jairo Gutierrez, Corp. Yurupari] 

###### [2 Sept 2015] 

Del 3 al 9 de Septiembre se realiza en Bogotá la 4ta Semana por la Paz y los Derechos Humanos, una iniciativa que propende por la sensibilización de las y los ciudadanos de la capital y del país, hacia el respeto por los derechos humanos, multiétnicos y pluriculturales; utilizando el arte y la cultura como vehículo pedagógico y propositivo hacia la paz.

John Jairo Gutierrez, representante legal de la Corporación Yurupari, colectivo que trabaja por la promoción y la defensa de los Derechos Humanos, relata los orígenes de la iniciativa, su proceso de conformación y los retos que ha implicado a nivel personal y grupal asumir año tras año la tarea de gestionar y organizar el evento.

De la misma manera, el profesor, filósofo e historiador de la Corporación, explicó los objetivos planteados para cada una de las actividades propuestas, así como las instituciones, profesionales y artístas que con su aporte enriquecerán el desarrollo en las mesas, foros, conversatorios y talleres incluídos en la [programación](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/PROGRAMCIONPENSARTE-2015.jpg) para este 2015.

Los colegios Mercedes Nariño, José Celestino Mutis, la Universidad Javeriana, el Parque Santanter, el Teatro Municipal Jorge Eliécer Gaitán y  la Cinemateca Distrital de Bogotá, son algunos de los espacios que durante 7 días acogen la semana cultural, instituciones que se suman  a la Coporación Colectivo de Abogados José Alvear Restrepo, ANDESCOL, la Fundación Contravía  y la Plataforma Social Usme, como colaboradores del evento.

<iframe src="https://www.youtube.com/embed/RlehEKlEZNk" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

El cierre del evento, coicide con la entrega por cuarta ocasión el Premio Nacional a la Defensa de los derechos humanos en Colombia,( Ver, [Estos son los nominados al Premio Nacional de Derechos Humanos)](https://archivo.contagioradio.com/estos-son-los-nominados-al-premio-nacional-de-derechos-humanos/)  organizado por la agencia sueca Diakonia, y que tendrá lugar en el Centro de Memoria Paz y Reconciliación de Bogotá.
