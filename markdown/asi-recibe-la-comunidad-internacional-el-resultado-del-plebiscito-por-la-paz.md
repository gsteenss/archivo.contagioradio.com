Title: Así recibe la comunidad internacional el resultado del plebiscito por la paz
Date: 2016-10-03 13:21
Category: Nacional, Paz
Tags: Acuerdos de paz en Colombia, Ban Ki Moon, Plebiscito
Slug: asi-recibe-la-comunidad-internacional-el-resultado-del-plebiscito-por-la-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/10/bankimoon.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: VPItv] 

###### [3 Oct de 2016] 

Tras conocer el rechazo de los acuerdos de Paz de Colombia en el plebiscito del domingo, el secretario general de la ONU Ban Ki Moon decidió **enviar a su representante especial Jean Arnault para el proceso de paz en Colombia a La Habana** en apoyo a las partes negociadoras**, **tras el rechazo en plebiscito al acuerdo de paz.

[Ban Ki Moon extendió su invitación a continuar en la construcción de Paz](https://archivo.contagioradio.com/piden-intervencion-de-papa-y-onu-para-iniciar-fase-publica-con-eln/) y manifestó que fue testigo en Cartagena “del profundo deseo de la gente de Colombia para acabar con la violencia, cuento con ellos, para que presionen hasta que se logre una paz definitiva".

Horas antes, Filippo Grandi alto comisionado de Naciones Unidas para los Refugiados ACNUR, dijo que **confía en que las partes negociadoras y la sociedad civil no decaiga un instante tras el triunfo del no.** “A pesar del retroceso de ayer, las negociaciones de paz en Colombia han llevado al país mucho más cerca del fin de uno de los conflictos mundiales más antiguos" concluyó Grandi.

Añade Grandi que el acuerdo de Paz en Colombia ha sido "uno de los más amplios e integrales que se han creado en el mundo" recalcó que **en su elaboración participaron activamente las víctimas del conflicto**. A ello se suman otros organismos internacionales, quienes han manifestado recientemente su apoyo a los términos del acuerdo de paz.

 La Unión Europea expresó su respeto al resultado del referendo celebrado en Colombia, que rechazó el acuerdo de paz negociado con las Farc, y también las decisiones que adopte el país en el futuro.

Por otra parte el secretario general de la Organización de Estados Americanos Luis Almagro, pidió **"diálogo incluyente" para que el proceso de paz ["llegue a todos los colombianos"](https://archivo.contagioradio.com/debe-existir-una-mejor-correlacion-de-fuerzas-para-constituyente-carlos-medina/)** después de que el no se impusiera en el plebiscito del domingo sobre el acuerdo de paz firmado con las FARC.

"Es clave que el proceso de paz llegue a todos los colombianos, incluyendo el 60 % que no voto. Apoyamos diálogo incluyente. La Paz es obra de todos", escribió Almagro en su cuenta de Twitter.

La secretaria general de la UNASUR emitió un comunicado en el que expresó su reconocimiento y felicitación al pueblo colombiano por el espíritu democrático y el comportamiento cívico y pacífico durante la jornada del Plebiscito. Renovó su compromiso para continuar apoyando a Colombia **"para que alcance la paz por medio del diálogo, la preservación de la vigencia de los Acuerdos de la Habana, la concertación y el entendimiento".**

###### Reciba toda la información de Contagio Radio en su correo [bit.ly/1nvAO4u](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por Contagio Radio [bit.ly/1ICYhVU](http://bit.ly/1ICYhVU) 
