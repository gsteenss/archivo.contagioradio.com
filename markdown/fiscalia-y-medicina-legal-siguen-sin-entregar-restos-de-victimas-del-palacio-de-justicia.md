Title: Fiscalía y Medicina Legal siguen sin entregar restos de víctimas del Palacio de Justicia
Date: 2016-08-04 12:26
Category: DDHH, Entrevistas
Tags: Entrega restos Palacio de Justicia, Palacio de Justicia, Victimas palacio de justicia
Slug: fiscalia-y-medicina-legal-siguen-sin-entregar-restos-de-victimas-del-palacio-de-justicia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/08/Víctimas-de-Palacio-de-Justicia.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: El País ] 

###### [4 Ago 2016] 

La Fiscalía General y el Instituto de Medicina Legal habían asegurado en octubre de 2015 que entregarían los restos mortales de Luz Mary Portela, Cristina del Pilar Guarín y Lucy Amparo Oviedo, víctimas de la toma y la retoma del Palacio de Justicia, a sus familiares; sin embargo, **nueve meses después ni la Fiscalía ni el Instituto han cumplido**, sólo la fiscal cuarta delegada ha enviado una comunicación a la familia Guarín, mientras que la familia Portela espera que se comuniquen con ellos.

De acuerdo con René Guarín, hermano de Cristina del Pilar, los familiares de las víctimas exigen que la **Oficina del Alto Comisionado de las Naciones Unidas actúe como veedor internacional en la entrega de restos**, pues consideran que el Estado colombiano no puede continuar siendo juez y parte en todo el proceso, teniendo en cuenta su responsabilidad en los crímenes de lesa humanidad que rodearon los hechos ocurridos en el Palacio de Justicia y la constante revictimización que ha ocasionado.

Guarín asegura que la comunicación enviada por la fiscal delegada determina que en los próximos días se reunirán para conocer el dictamen técnico de Medicina Legal con el fin de que **el 9 de septiembre pueda realizarse la entrega de los restos con la veeduría del Alto Comisionado**, buscando que el Estado colombiano no continúe [[revictimizando a las familias](https://archivo.contagioradio.com/9-meses-despues-no-entregan-restos-de-victimas-del-palacio-de-justicia-hallados-en-2015/)], como ha ocurrido con la de Luz Mary Portela, quien espera poder asistir al encuentro.

Según Guarín [[la placa que estaba en el Palacio del Lievano](https://archivo.contagioradio.com/alcaldia-de-bogota-no-quiso-escuchar-peticiones-de-las-victimas-del-palacio-de-justicia/)] fue retirada y cambiada por otra en la que no se reconoce la responsabilidad estatal en la retoma, así mismo denuncia que ni el IDU ni el Instituto de Patrimonio Cultural han permitido la **construcción de un monumento frente a la Casa del Florero**, decisión frente a la que no se ha pronunciado la Alta Consejera de Víctimas.

<iframe src="http://co.ivoox.com/es/player_ej_12441078_2_1.html?data=kpehlpaUe5mhhpywj5aWaZS1lZyah5yncZOhhpywj5WRaZi3jpWah5yncbPZz8qYqdrFtoa3lIquptOJdqSfp8bay9HNpdPZ1JCxx9jFtMLmxsjWxtTXb7HVzcbQy9SPqMafq9rg1s6RaZi3jqjc0NnFq8rjjLfOxs7Tb46ZmKialg..&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)]] 
