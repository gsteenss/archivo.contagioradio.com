Title: Sin Olvido - Nydia Erika Bautista
Date: 2015-01-06 15:15
Author: CtgAdm
Category: Sin Olvido
Slug: nydia-erika-bautista
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/08/nydia-erika-bautista.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Chico Bauti 

[ Audio](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/01/fstNHept.mp3)

Nydia Erika Bautista vive en los pensamientos y los corazones de quienes tuvieron la dicha de conocerla y quienes han visto sus fotografías en calles, postes, manifestaciones y algunos periódicos durante los últimos 27 años. Quienes la desaparecieron no han podido asesinar su recuerdo porque sus luchas han germinado en las nuevas generaciones.

La historia de Nydia Erika Bautista es la de una mujer sensible a las realidades de sus semejantes, de sus hermanos del pueblo colombiano víctimas de tanta violencia e injusticia. Ella fue la hija mayor de un hogar sencillo de la ciudad de Bogotá. Creció de la mano de la literatura universal, latinoaméricana y nacional. Estudió Sociología en la Universidad Nacional y economía en la Universidad Central. Con todo su ingenio fue Directora del Periódico El Aquelarre y participó activamente del sindicato de INRAVISIÓN hasta el año 1984.

En ese mismo año, 1984, Nydia Erika tomó una decisión importante: vincularse al Movimiento 19 de abril (M19) desde las acciones que realizaban en la ciudad de Bogotá. Nydia Erika ayudó a construir escuelas y jardines en Bosa. A ella no le gustaban las armas, siempre prefirió el trabajo social con la comunidad y auxiliaba en la sanidad de los integrantes del M19 que eran heridos y debían regresar a la ciudad.

Su familia recuerda la primera detención de Nydia Erika en 1986 cuando militares de la tercera Brigada del Ejército se la llevaron durante dos semanas y allí, bajo torturas, la obligaron a firmar una declaración en donde cuenta sobre su vinculación al M-19, además de un documento dónde, falsamente, aseguraba que no había víctima de malos tratos durante su detención.

La siguiente vez que se llevaron viva a Nydia Erika, el 30 de agosto de 1987, fue para siempre.
