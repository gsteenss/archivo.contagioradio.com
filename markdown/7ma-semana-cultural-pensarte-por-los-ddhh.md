Title: PensArte una semana dedicada a los Derechos Humanos
Date: 2018-09-03 17:06
Author: AdminContagio
Category: eventos
Tags: Derechos Humanos, semana pensarte
Slug: 7ma-semana-cultural-pensarte-por-los-ddhh
Status: published

###### Foto:PensArte 

###### 03 Sep 2018 

Del 5 al 11 de septiembre tendrá lugar la **7ma Semana Cultural PensArte,** un esfuerzo de construcción colectiva por los Derechos Humanos, la paz y las garantías de no repetición, organizada por la **Corporación Yurupari y el Colectivo de Abogados José Alvear Restrepo CAJAR,** en el marco de sus 40 años de creación.

Durante las jornadas programadas, se realizarán una serie **conversatorios,  intervenciones urbanas, proyección de películas de ficción y documentales, obras de teatro y recorridos por diferentes espacios de la capital**, relacionados con la defensa de los Derechos Humanos en el país.

La jornada inicia con la entrega del **Premio Nacional a la Defensa de los Derechos humanos** por parte de la organización Diakonia con apoyo de la iglesia Sueca, en el auditorio Luis Carlos Galán de la Pontificia Universidad Javeriana, a partir de las 8:30 de la mañana (Le puede interesar:[Estos son los finalistas del premio nacional a la defensa de los Derechos humanos](https://archivo.contagioradio.com/finalistas-premio-nacional-a-la-defensa-de-los-ddhh/))

Desde las 10 a.m. hasta las 2 p.m., se realizará en el Parque Santander una intervención urbana titulada: **"Banderas por la vida y el respeto a la labor de las y los defensores de los derechos humanos en Colombia"**, evento que se replicará en diferentes espacios de la ciudad durante la semana que dura el evento.

Luego, de 2 a 6 p.m. en las oficinas del CAJAR, calle 16 \#6-66 piso 25, se realizará la presentación de la **Caja de herramientas: principios rectores sobre empresas y derechos humanos**, actividad que requiere de inscripción previa. De manera paralela, se realizará el lanzamiento de la Semana Cultural  poesía por la vida y la defensa de los DDHH en la Casa de Poesía Silva, ubicada en calle 12c \#3-41 de Bogotá.

El 9 de septiembre, día Nacional de los Derechos Humanos, se realizará **un recorrido ambiental por los cerros orientales** desde las 7 de la mañana y luego un paseo artístico por la carrera séptima partiendo desde el edificio Colpatria desde las 2 de la tarde.

El 11 de septiembre se realizará el conversatorio abierto: **problemáticas sociales: por el derecho a contar los derechos**, desde la 1 p.m en la Sede de SINTRATELÉFONOS; y se presentará un homenaje a la memoria del inmolado líder social y político Salvador Allende, con la obra CONSUMIN del grupo CONTRABAJO, en la Sala DANTEXCO.

Para conocer todas las actividades, horarios y costos consulte aquí la [Programación](https://www.colectivodeabogados.org/IMG/pdf/programacion_final_curvas_web_1_.pdf) completa.

###### Reciba toda la información de Contagio Radio en [[su correo]
