Title: La propuesta de Pijao como alternativa a la minería tras consulta popular
Date: 2017-07-10 13:36
Category: Ambiente, Nacional
Tags: alternativas, consulta popular, Mineria, Pijao
Slug: la-propuesta-de-pijao-como-alternativa-a-la-mineria-tras-consulta-popular
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/07/pijao-quindio-e1499711750161.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Vive Quindío] 

###### [10 Jul 2017 ] 

En Pijao los habitantes votaron por mantener el uso tradicional del suelo mediante el desarrollo de actividades agropecuarias. **Ellos y ellas proponen generar un turismo ambiental responsable y fomentar las actividades creativas** para producir bienes y servicios que puedan generar ingresos económicos para el municipio.

Para Néstor Ocampo, ambientalista de la Fundación Ecológica Cosmos de Calarcá, “con el resultado de la consulta popular **enviamos el mensaje de que preferimos mantener la cultura “Cittaslow**” en vez del modelo económico extractivista que quiere imponer el gobierno”. (Le puede interesar: ["Comunidades votaron NO a la minería en pijao y Arbeláez"](https://archivo.contagioradio.com/arbelaez-piajo-consulta-minera/))

Ocampo también recordó que **Pijao es la única ciudad de Latinoamérica que hace parte del movimiento de ciudad lenta** en donde se vive en comunión con la naturaleza, las tradiciones culturales y el patrimonio arquitectónico y eso es lo que se pretende conservar.

Él manifestó que los resultados de la consulta popular ponen por encima “la dignidad del pueblo, la democracia, la cultura y los derechos humanos”. Es por esto que en ciudades como Pijao se debe trabajar para que la gente pueda generar bienes y servicios que potencialicen la diversidad cultural. Así mismo, propuso  el turismo ambiental responsable, como una alternativa para **fomentar las actividades económicas que reemplazarían la extracción de minerales.** (Le puede interesar: ["Consulta popular en Pijao busca impedir actividad minera"](https://archivo.contagioradio.com/consulta-popular-pijao/))

**Mineria no trae beneficios reales para la población**

Ocampo fue enfático en manifestar que la extracción de minerales no le representa a la población una ganancia económica real. Por el contrario, afirmó que “en las regiones donde se ha hecho minería **aumenta la pobreza, la violencia y la degradación ambiental y social”**. Así mismo indicó que las regalías por minería no representan un beneficio para la nación en la medida que “de cada 100 pesos de regalías para la salud o la educación, solo llegan 22 pesos”.

Adicionalmente, puso de manifiesto la minería del Cerrejón en la **Guajira como un ejemplo de la degradación del territorio y de los recursos naturales**. “La desarticulación de los tejidos sociales y lo que somos como territorio queda destruido, esto sucedió en la Guajira donde hace 35 años era una región de vida y hoy son la devastación total”.

<iframe id="audio_19719710" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_19719710_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU).
