Title: Las razones frente a la crisis que se vive al interior de Nicaragua
Date: 2018-05-09 08:53
Category: El mundo
Tags: Bluefields, Daniel Ortega, Nicaragua, Rosario Murillo
Slug: razones_crisis_nicaragua
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/05/ortega_nicaragua.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:  CNN en Español] 

###### [29 Abr 2018] 

Se  cumplen 10 días de protestas en Nicaragua y la situación no parece mejorar. Aunque el presidente Daniel Ortega convocó un diálogo nacional para dar paso a una posible solución a la situación actual, miles de nicaragüenses, organizaciones sociales, medios de comunicación y la iglesia católica han asegurado que es difícil que tal diálogo exista cuando en las calles se sigue reprimiendo violentamente a los manifestantes. Ya se cuentan **más de 40 personas muertas y 70 desaparecidos en el marco de estas movilizaciones.**

Así lo asegura Sayuri Nishime, periodista del medio digital nicaragüense Onda Local, quien señala que la población ya no aguantó otra reforma más y por ello decidió salir a las calles a protestar. Esta vez, e**l gobierno de Ortega impulsó una reforma a las pensiones a través de la cual se establecía que el gobierno le iba a quitar a los jubilados un 5% de la pensión que reciben.** Dicha medida impulsó una gran movilización, especialmente de estudiantes universitarios y personas de la tercera edad exigiendo la derogación de esa reforma.

### **No es una situación nueva** 

El panorama de indignación de la población viene alimentándose desde hace varios años. De acuerdo con Nishime, hace mucho tiempo Daniel Ortega y su vicepresidenta y esposa, Rosario Murillo, vienen utilizando el discurso de la Revolución Sandinista para mantenerse en el poder, pero lo que realmente estaría sucediendo en Nicaragua es una violación a los derechos humanos.

La periodista señala que eso se refleja en la forma como ha sido violentada la protesta social. **Desde el primer día de las manifestaciones, la policía fue enviada para reprimir y golpear a quienes estaban en las calles.** A raíz de esa primera situación, la movilización se fortaleció y miles de ciudadanos decidieron exigir que se detenga la violación a los derechos humanos de la población.

Aunque de acuerdo con Sayuri Nishime, desde los medios oficialistas de ese país se ha dicho que los actos vandálicos, tales como el saqueo de supermercados, se debe a las acciones de jóvenes de las universidades; en medios alternativos, estarían circulando videos en donde se evidenciaría que son integrantes del movimiento sandinista junto a la policía, quienes estarían detrás de dichos actos.

En cambio, muchos de los jóvenes que han salido movilizarse contra el gobierno, han denunciado que una vez capturados por la policía han sido víctimas de torturas y tratos inhumanos. Cuentan que "e**n la cárcel lo rasuraron a todos, los dejaron completamente calvos y denuncian que los policías hacían fila para patear a los estudiantes uno por uno".**

Pero además cuatro canales televisivos que han mostrado lo que estaría pasando al interior del país fueron censurados, una emisora fue quemada y dos medios digitales que han seguido las protestas han sido saboteados. También ha sido noticia el asesinato de  Ángel Gahona, periodista y director de Bluefields, quien recibió un disparo en la cabeza de parte de un francotirador del cual aún se desconoce su identidad, mientras cubría el conflicto entre la policía y los manifestantes.

### **¿Qué ha pasado en los 11 años de gobierno de Ortega?** 

En 1979 se celebra el triunfo de la revolución sandinista, y aunque en un principio empezó a verse que los valores y las comunidades irían primero, poco a poco ese discurso fue desvaneciéndose, como lo narra la comunicadora. "**Es cierto que hemos tenido un gobierno que se hace llamar de izquierda y que dice que representa los valores de Sandino, pero una cosa son sus discursos, y otras sus acciones".**

Una de las primeras situaciones que evidencia esa ruptura con el movimiento sandinista, fue la forma como muchas comunidades vieron cómo les fueron arrebatadas sus tierras, cuando en 1990 ellas mismas dieron sus propiedades al Estado para que estas fueran repartidas de manera más equitativa. Sin embargo, esos territorios nunca fueron entregados a las familias desfavorecidas, sino que por el contrario, quedaron a título personal de algunos miembros del partido de Ortega, el Frente Sandinista de Liberación Nacional (FSLN).

"Se ha dicho que quienes están en las cales eran pandilleros y delincuentes. Que son protestas financiadas por Estados Unidos", dice Sayuri Nishime, quien revela que quien está dando entrada al país a las multinacionales, es el propio gobierno de Daniel Ortega, que ha dado concesiones mineras a varias empresas extranjeras.

### **Las medidas neoliberales del gobierno Ortega** 

Antes del inicio las movilizaciones actuales, se incendiaron más de 5.600 mil hectáreas de la reserva forestal más importante de país conocida como "Indio Maíz" Aún así, Daniel Ortega no declaró emergencia. Las organizaciones que protegen la reserva, han denunciado que lo que se pretendía con el incendio era que se pudiera empezar un proyecto de construcción de carreteras. Cabe resaltar, que  tras dar a  conocer esa situación, la Fundación que hizo la denuncia públicamente, pero la entidad fue amenazada con quitarle la personería.

Otro de los puntos claves tiene que ver con la concesión de Canal Interoceánico que según explica la comunicadora, le va a quitar muchas tierras a los campesinos, además del impacto ambiental que significa ese proyecto.

Ante tal situación, **el movimiento estudiantil ha declarado un paro nacional, impulsado incluso por muchos de los integrantes del movimiento sandinista,** que también se muestran indignados ante las violaciones a los derechos humanos por parte del gobierno. Los estudiantes piden que sean juzgadas las autoridades locales y también sean destituidos todos los agentes de la Fuerza pública que ha incurrido en acciones violentas para detener la protesta de la ciudadanía.

El paro y las actividades de movilización actuales buscan no solo que se judicialice a quienes están inmersos en la muerte de las más de 40 personas, sino que también tanto el presidente Ortega como su esposa, Rosario Murillo dimitan de sus cargos como presidente y vicepresidenta, respectivamente, así como la Asamblea Nacional. "Queremos que cambien los líderes, no los ideales sandinistas", enfatiza la periodista.

<iframe id="audio_25902648" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_25902648_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU).
