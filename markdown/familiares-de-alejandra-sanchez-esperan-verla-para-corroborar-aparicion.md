Title: Familiares de Alejandra Sánchez esperan verla para corroborar aparición
Date: 2016-04-28 12:46
Category: Mujer
Tags: Alejandra Sánchez, desaparición
Slug: familiares-de-alejandra-sanchez-esperan-verla-para-corroborar-aparicion
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/04/alejandra-sanchez.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Fundación Para La Libertad de Prensa 

###### [28 Abr 2016] 

Contagio Radio se comunicó con Yamile Sánchez, tía de Alejandra, quien comentó que las relaciones en su núcleo familiar son muy buenas y estrechas y que no cree que sea su sobrina hasta que no corroborare la información suministrada por la Policía.

Según declaraciones de la Policía Metropolitana de Calí, Alejandra se encontraría en buenas condiciones, en un hostal en Guatapé y se habría ido por sus propios medios, al parecer por una discusión con su familia.

Alejandra Sánchez, estudiante de periodismo de la Universidad del Rosario, desapareció el pasado 26 de abril y se comunicó por última vez   con su mamá a través de un mensaje, informando que se vería en la Plaza Caicedo con un funcionario de la secretaría Cultural de Calí.

Alejandra se encontraba en Calí desarrollando su proyecto de grado sobre la Historia de la salsa y la cultura, hecho que la llevo a radicarse en esta ciudad y motivo por el cual se vería con el funcionario de la secretaría de Cultural.

<iframe src="http://co.ivoox.com/es/player_ej_11341417_2_1.html?data=kpaglpaYdZihhpywj5aVaZS1k5uah5yncZOhhpywj5WRaZi3jpWah5yncbrVzs7Zx5C3aaSnhqae0MjMqdufjpCzw9LNsMrV05CuzsrOpc%2FY08aah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ%3D%3D&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
