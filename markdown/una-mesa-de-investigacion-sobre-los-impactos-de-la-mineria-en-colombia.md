Title: Una mesa de investigación sobre los impactos de la minería en Colombia
Date: 2017-06-02 06:00
Category: Ambiente y Sociedad, Opinion
Tags: Gran Minería en Colombia, Mineria
Slug: una-mesa-de-investigacion-sobre-los-impactos-de-la-mineria-en-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/06/impactos-de-la-mineria-.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: mineriambiental 

#### [Por: Alejandra Lozano, Investigadora Ambiente y Sociedad](https://archivo.contagioradio.com/margarita-florez/) 

###### 2 Jun 2017 

[En octubre de 2016, la Corte Constitucional mediante sentencia T-445 de 2016, llamó la atención sobre un defecto estructural en el desarrollo de la locomotora minera en Colombia, y es que no había certeza sobre los impactos en la ejecución de proyectos mineros, y así su ejecución parecía obedecer a caprichos políticos. Esto denotaba el absurdo que el Gobierno Nacional hubiere construido y ejecutado una política minera sin contar con estudios técnicos, sociológicos y científicos que permitan con anterioridad evaluar los impactos que genera dicha actividad sobre los territorios, pero así se hizo.]

[Teniendo en cuenta lo anterior, la Corte ordenó al Ministerio de Ambiente y Desarrollo Sostenible, al Ministerio del Interior, a la Unidad de Parques Nacionales Naturales, al Instituto de Investigación de Recursos Biológicos Alexander Von Humboldt y a la Contraloría General de la República que conformaran una mesa de trabajo interinstitucional a la cual se pudieran vincular entidades estatales y privadas, centros de investigación y miembros de la sociedad civil, con el objeto de construir una investigación oficial científica y sociológica que sirviera de soporte a las decisiones sobre los proyectos extractivos. El pasado martes 16 de mayo, la Oficina Jurídica del Ministerio de Ambiente, cumplió la orden, mediante resolución 0931 estableció la creación de dicha mesa interinstitucional.]

[La falta de estos estudios previos ha llevado a que los conflictos socioambientales se solucionen a través de pronunciamientos judiciales, circunstancia que se ha hecho visible en el último tiempo con cada una de las respuestas que la Corte ha tenido que dar en los casos de oposición por violación de derechos fundamentales a la ejecución de proyectos mineros.]

[La Corte en octubre del año pasado señaló la necesidad de que el país contara en su marco político con una línea base clara, que definiera los lineamientos de la locomotora minera, pues sin estos, no es posible desestimar las continuas solicitudes desde distintos sectores que señalaban los impactos no deseables. Tales como, el deterioro que produce las actividades mineras en los recursos hídricos, el impacto, daño o afectación que se pueda causar a la vida, las costumbres de los pueblos, municipios o territorios que obligan a contar con este informe para el que la Corte Constitucional señaló un plazo de dos años que ya está corriendo desde hace más de seis meses.]

[¿Y qué se espera? Que la mesa realice una investigación oficial científica y sociológica sobre la actividad minera en Colombia, que no solo responda al seguimiento trimestral que por su parte deben hacer la Procuraduría General de la Nación y la Contraloría General de la República, sino que desarrolle una política estructural para la protección del medio ambiente, en el que los impactos socioambientales de la actividad minera sean respondidos con la salvaguarda de los derechos fundamentales y los derechos colectivos de los habitantes de los territorios en los que se pretenda desarrollar un proyecto a futuro en el país.]

**[Conozca las columnas de opinión de Ambiente y Sociedad](https://archivo.contagioradio.com/margarita-florez/)**

------------------------------------------------------------------------

###### [Las opiniones expresadas en este artículo son de exclusiva responsabilidad del autor y no necesariamente representan la opinión de Contagio Radio.] 
