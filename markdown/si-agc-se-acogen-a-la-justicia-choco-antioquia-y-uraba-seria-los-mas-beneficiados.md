Title: Si AGC se acogen a la justicia Chocó, Antioquía y Urabá sería los más beneficiados
Date: 2017-09-06 15:33
Category: DDHH, Nacional
Tags: Autodefensas Gaitanistas de Colombia, Paramilitarismo
Slug: si-agc-se-acogen-a-la-justicia-choco-antioquia-y-uraba-seria-los-mas-beneficiados
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/04/paramilitares-cordoba.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:] 

###### [06 Sept 2017] 

De acuerdo con el analista y director de Indepaz, Camilo Gonzáles Posso, el anuncio realizado por la Autodefensas Gaitanistas de Colombia, AGC, de tener un interés por someterse a un proceso de paz, **“tienen consecuencias positivas en el territorio”** debido a la fuerte presencia que tienen en distintas regiones del país.

El analista manifestó, que de ser este hecho una realidad, habría aproximadamente 5 mil integrantes de esta estructura, que dejarían la violencia en **departamentos como Antioquia, Córdoba o Chocó, en donde está el 60% de sus efectivos**. (Le puede interesar:["Sometimiento de las AGC debe garantizar la verdad para las víctimas"](https://archivo.contagioradio.com/sometimiento-de-agc-debe-garantizar-la-verdad-a-las-victimas/))

“Es una gran noticia, tiene un impacto directo en departamentos como el Chocó, en Nariño, en el bajo Cauca, el núcleo fuerte del Urabá, **allí está la realidad de la presencia de estos grupos, su incidencia en el poder político, sus redes de corrupción sobre la Fuerza Pública**” afirmó Gonzáles.

Frente a las condiciones para que se de este proceso, Gonzáles dijo que debe estudiarse bajo qué marco legal se podría acobijar el interés de las AGC, “en este caso tienen que clarificarse aspectos tipo jurídico y lo segundo es lo relacionado con la extradición”, en ese sentido, señaló que **no se puede permitir que las extradiciones se manejen de acuerdo a las disposiciones de Estados Unidos**, para no repetir errores como los de las personas extraditadas de las Autodefensas Campesinas de Colombia, que singuen incumpliendo a la verdad.

Además, manifestó que tienen que abrirse otros capítulos en relación al inventario de bienes, procedimientos de re incorporación y los procedimientos de operación de la justicia, que podría ser que ya estén en marcha, sin embargo, expresó que la pregunta que debe hacerse es **si los integrantes de las AGC también serán juzgados por narcotráfico o habrá alguna ponderación de su papel como insurgentes**. (Le puede interesar:["Paramilitares imponen controles de transporte público en Chocó"](https://archivo.contagioradio.com/44498/))

De igual forma, manifestó que no se puede olvidar “que las Autodefensas Gaitanistas vienen directamente de la desmovilización de las AUC” y agregó que el poco tiempo que le resta al gobierno de Juan Manuel Santos, es el que ofrecería las condiciones para que este proceso se lleve acabo.

<iframe id="audio_20730937" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_20730937_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
