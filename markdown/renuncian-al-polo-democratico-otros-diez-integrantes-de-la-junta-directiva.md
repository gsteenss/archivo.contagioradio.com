Title: Renuncian al Polo Democrático otros diez integrantes de la junta directiva
Date: 2017-06-22 16:43
Category: Nacional, Política
Tags: Clara López, Jorge Robledo, Polo Democrático
Slug: renuncian-al-polo-democratico-otros-diez-integrantes-de-la-junta-directiva
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/06/DCElgBtWAAMqani.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Diego Casas] 

###### [22 Jun 2017]

En un comunicado de 4 puntos conocido este 21 de Junio, diez dirigentes del Polo Democrático Alternativo renunciaron al partido denunciando que el Polo “no es ni la sombra de lo que fundamos con diversos movimientos democráticos” y resaltaron que la **candidatura de Jorge Robledo fue impuesta por el Comité Ejecutivo y no se eligió en el Congreso** como lo establecería el relgamento

Los y las firmantes también denuncian que desde la elección del presidente Santos para sus segundo mandato, se ha excluido por lo menos a la mitad de las personas del partido que decidieron apoyar la candidatura en la segunda vuelta expresando el respaldo a la continuidad de la mesa de conversaciones de paz. Según ellos desde que tomaron esa decisión **los han acusado de “santistas y de derecha”**

Además denunciaron que hay discriminación de las personas LGBTI puesto que se les ha negado el derecho al voto, aunque no especificaron cuando y para que votaciones. Entre los firmantes de la misiva se encuentran dirigentes sindicales como T**arcisio Mora u otros miembros representativos del partido en sectores de base, entre ellos Carlos Romero y Alba Helena Ulloa.** [Lea tambien: El sueño de unidad a 10 años de nacimiento del PDA](https://archivo.contagioradio.com/el-polo-cumple-10-anos-y-retoma-politica-unitaria-de-izquierda/)

Esta renuncia masiva se da luego de la **renuncia de Clara López**, ex presidenta de la agrupación quien en su momento denunció la “moirización” del Polo, en referencia al control por parte de la corriente del MOIR representada en la figura de Jorge Robledo.

Aunque para algunos integrantes del partido la renuncia de López representa una pugna normal al interior de un partido político, otros sectores evalúan la situación como el debilitamiento de una **fuerza democrática que podría ser clave para la construcción de la paz y con una importancia relevante en las próximas contiendas electorales.**

Esta es la carta completa

[Carta Polo Democrático Alternativo](https://www.scribd.com/document/352026382/Carta-Polo-Democratico-Alternativo#from_embed "View Carta Polo Democrático Alternativo on Scribd") by [Contagioradio](https://es.scribd.com/user/276652802/Contagioradio#from_embed "View Contagioradio's profile on Scribd") on Scribd

<iframe id="doc_28559" class="scribd_iframe_embed" src="https://www.scribd.com/embeds/352026382/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-XJ4SyPQqEWVUepW8aZaJ&amp;show_recommendations=true" width="75%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="0.7068965517241379"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
