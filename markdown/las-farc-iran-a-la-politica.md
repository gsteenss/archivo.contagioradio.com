Title: La apuesta de las FARC es la "apertura democrática"
Date: 2017-11-01 13:45
Category: Nacional, Paz
Tags: FARC, Iván Márquez, Rodrigo Londoño
Slug: las-farc-iran-a-la-politica
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/11/timochenko-farc-.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [01 Oct 2017]

Las FARC confirmó a través de una rueda de prensa sus candidatos para las próximas elecciones 2018. **Rodrigo Londoño encabezará la lista para presidencia junto a Imdelda Daza que será su fórmula política. Por su parte, Iván Márquez encabeza la lista al senado, y a en Cámara estará Jesús Santrich**.

Junto a Márquez estarán Pablo Catatumbo, Carlos Antonio Lozada, Victoria Sandino, Sandra Ramírez y Benkos Biojó, de igual forma, a la Cámara de representantes también llegarán Byron Yepes, por Bogotá; Olmedo Ruíz, por Antioquia y Marco León Calarcá, por el Valle del Cauca; y Jairo Quintero por Santander.

En el resto de departamentos las FARC manifestó que **apoyarán a los candidatos de convergencia social, que hayan demostrado su compromiso con la implementación** de los acuerdos de paz y la transformación social de cada uno de sus territorios.

### **La Apertura democrática el reto de las FARC** 

Las FARC señalaron que la participación política es un punto fundamental de los Acuerdos de paz, y por tanto debe respetarse y afirmarse para que se pueda dar en Colombia una apertura democrática, sobre todo cuando en lo corrido del año **van 33 excombatientes asesinados, que ya tenían la amnistía o estaban en este proceso**. Una situación que de acuerdo con las FARC continúa demostrando las pocas garantías que hay para que exista una mayor diversidad política en el país. (Le puede interesar:["Dura carta de las FARC al gobierno por asesinato de integrantes de este partido político"](https://archivo.contagioradio.com/farc_carta_presidente_juan_manuel_santos/))

Además, afirmaron que dan el paso hacia la política electoral, en un contexto en donde “las grandes mayorías esperan pasar definitivamente la página de la guerra” con la implementación de los acuerdos de paz y la concreción de los acuerdos con la guerrilla del ELN. De igual forma, **expresaron que este partido político tendrá la tareas de incluir las exigencias y necesidades de quienes han quedado excluidos históricamente** por las políticas Estatales y el conflicto armado.

Finalmente manifestaron su interés por construir una unidad de convergencia nacional que vaya en pro de un gobierno de transición, y exigieron que frente a la grave situación que atraviesan los Acuerdos de La Habana en el Congreso, este se ciña “estrictamente a lo determinado por la Corte Constitucional” y no varíe más el contenido del articulado.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
