Title: Ecosocialismo del siglo XXI
Date: 2017-02-01 08:53
Category: Cesar, Opinion
Tags: Ambientalismo, Socialismo
Slug: ecosocialismo-del-siglo-xxi
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/02/Ecosocialismo-1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Prensa Rural 

#### Por  **[Cesar Torres del Río ](https://archivo.contagioradio.com/cesar-torres-del-rio/)** 

###### 1 Feb 2017 

Henos aquí, la especie, en plena catástrofe socio-ambiental. Han sido traspasados los límites que, aceptados y respetados, hubieran podido evitar el calentamiento del planeta, el consiguiente desbordamiento de los mares por la progresiva desaparición de los glaciales y el aniquilamiento de la vida en todas sus formas. Esta es una objetiva apreciación de lo que en verdad importa para nuestro presente.

**Los vencidos, con todo, continuamos la labor de profetas en cuanto a qué pasará si … ¿Qué pasará si se le permite a las transnacionales deforestar y extraer combustible fósil con el método del fracking?** ¿Qué futuro les espera a las especies si el efecto invernadero modifica a mayor velocidad lo que era el equilibrio climático? ¿Qué esperar del “desarrollo”, que expolia a millones de seres por la vía de la desposesión, la guerra y el hambre? ¿Qué sucederá si la política ecológica de los Verdes, los socialdemócratas y los comunistas continúa sirviéndole al gran capital (Rusia, China, Estados Unidos, Francia …), apoyando las medidas de la Cumbre de Paris de 2015 y despolitizando al sujeto histórico?

Si de algo estamos seguros es que no hay salida posible a la catástrofe por la vía de la socialdemocracia o de los Frentes Amplios impulsados por los comunistas; mucho menos con la  coartada liberal de los partidos Verdes. Las transnacionales, la Organización Mundial del Comercio (OMC), las Naciones Unidas, la Organización de Estados Americanos (OEA), los gobiernos imperialistas, las burguesías de América Latina - e infortunadamente los gobiernos del “socialismo del siglo XXI” - han promovido e impuesto el fundamentalista modelo extractivista  neoliberal  que incrementa el efecto invernadero y coloca a los pueblos en el matadero de la guerra imperialista.

Ya lo decía el Manifiesto de Zimmerwald contra la guerra en septiembre de 1915: “Toda la civilización, creada por el trabajo de muchas generaciones está condenada a la destrucción. La barbarie más salvaje celebra hoy su triunfo sobre todo aquello que hasta la fecha constituía el orgullo de la humanidad (…) Cualesquiera que sean los principales responsables directos del desencadenamiento de esta guerra, una cosa es cierta: la guerra que ha provocado todo este caos es producto del imperialismo (…) Colapso de la civilización, depresión económica, reacción política; estos son los beneficiarios de este terrible conflicto de pueblos. La guerra revela así el verdadero carácter del capitalismo moderno que se ha revelado incompatible no sólo con los intereses de las clases trabajadoras sino también con las condiciones elementales de existencia de la comunidad humana”.

Así, la catástrofe del ecosistema planetario no se produjo por la “acción del hombre”; el capitalismo como sistema es el telón de fondo que sustenta la debacle. Los actores mencionados arriba han desencadenado la guerra y la destrucción del planeta; han levantado muros para aislarse en su “civilización” y condenar a “los otros”; han inculcado la avidez por la propiedad, la santificación del individuo y el fetichismo mercantil; han renovado el racismo y la xenofobia, y descargado armas químicas y nucleares sobre los pueblos; ahora rechazan a los refugiados que ellos mismos han producido y repudian a los musulmanes por ser “el enemigo”.  Los vencidos apenas logran resistir. Una vez más Rosa Luxemburgo ocupa la primera plana: “Socialismo o barbarie”.

“Hay que crear otra utopía”, dijo hace pocos días el escritor cubano Leonardo Padura (“El hombre que amaba a los perros”) en su conversación con Héctor Abad Faciolince en el Hay Festival 2017 en Cartagena.

¿Otra? ¿Se verificó alguna en el siglo XX? ¿Fueron las revoluciones rusa, cubana y China - y su desarrollo - la cristalización de la utopía?

En próxima oportunidad responderemos a tales interrogantes. Pero hay un hecho particular y relevante: la (en sentido genérico) utopía no alcanzará realización a no ser que el sujeto histórico se desempeñe consciente y decisivamente en el diseño - y concreción - de un programa de transición que formule una ruta ecosocialista de transformación social.

**El ecosocialismo es apenas una posibilidad. **Pero en formulación y aspiraciones ofrece salvar a la especie, y con ella a todas las especies, y al planeta de la desaparición física. El capitalismo no ofrece sino guerra y degradación ambiental. Lo que llamado “socialismo real” demostró a las claras es que sin democracia directa de los trabajadores la “utopía” degenera en burocratismo autoritario, en estalinismo. Es lo que - palabras más, palabras menos - dijo Leonardo Padura en Cartagena.

El Manifiesto Ecosocialista de 2001 ([www.rebelion.org](http://www.rebelion.org)) recoge, hoy, la necesidad básica del cambio:

“El ecosocialismo mantiene los objetivos emancipatorios del socialismo de primera época y rechaza tanto las metas reformistas, atenuadas, de la socialdemocracia, como las estructuras productivistas de las variantes burocráticas de socialismo. En cambio, insiste en redefinir tanto la vía como el objetivo de la producción socialista en un marco ecológico. Lo hace específicamente con respecto a los "límites del crecimiento" esenciales para la sustentabilidad de la sociedad. Estos se adoptan, sin embargo, no en el sentido de imponer escasez, privación y represión. El objetivo, por el contrario, consiste en una transformación de las necesidades y un cambio profundo hacia la dimensión cualitativa, alejándose de la cuantitativa. Desde el punto de vista de la producción de mercancías, esto se traduce en una valorización de los valores de uso por sobre los valores de cambio -un proyecto de vasto significado, que se funda en la actividad económica directa.

La generalización de la producción ecológica bajo condiciones socialistas puede proporcionar la base para superar las crisis actuales. Una sociedad de productores libremente asociados no se detiene en su propia democratización. Debe, por el contrario, insistir en la liberación de todos los seres como fundamento y propósito. Supera así el impulso imperialista, subjetiva y objetivamente. Al realizar tal objetivo, lucha por superar todas las formas de dominación, incluyendo en especial las de género y raza. Y supera las condiciones que dan origen a las distorsiones fundamentalistas y sus manifestaciones terroristas. En suma, supone una sociedad mundial en un grado de armonía ecológica con la naturaleza impensable en las condiciones actuales. Una consecuencia práctica de estas tendencias se expresaría, por ejemplo, en la extinción de la dependencia en los combustibles fósiles consustancial al capitalismo industrial. Y esto a su vez puede proporcionar la base material para la liberación de los países oprimidos por el imperialismo del petróleo, mientras que permite la contención del calentamiento global, junto a otros problemas de la crisis ecológica”.

#### [[**Leer más columnas de opinión de  Cesar Torres del Río **](https://archivo.contagioradio.com/cesar-torres-del-rio/)] 

------------------------------------------------------------------------

#### 

###### Las opiniones expresadas en este artículo son de exclusiva responsabilidad del autor y no necesariamente representan la opinión de Contagio Radio. 

#### 
