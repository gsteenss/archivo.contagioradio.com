Title: "Las amenazas paramilitares son reales y comprobables": Unión Patriótica
Date: 2017-10-13 12:26
Category: DDHH, Nacional
Tags: amenazas contra la UP, Autodefensas Gaitanistas de Colombia, grupos paramilitares, Unión Patriótica
Slug: las-amenazas-paramilitares-son-reales-y-comprobables-union-patriotica
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/07/union-patriotica.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Prensa Rural] 

###### [13 Oct 2017] 

El comité ejecutivo de la Unión Patriótica, **fue víctima de una amenaza** que llegó a sus oficinas en la tarde de este jueves, y aunque las AGC hayan desmentido la autoría de la amenaza, esto no quiere decir que no sea real y que las fuerzas paramilitares no estén atentando contra líderes sociales y políticos. Por lo tanto deben realizarse acciones urgentes de desmonte de esas estructuras, afirma uno de sus integrantes.

De acuerdo con Pavel Santodomingo, integrante de la Unión Patriótica, la amenaza contra el partido político se da en el marco de la situación que está viviendo el país, “no es una casualidad que la amenaza se dé **después de que la Corte Constitucional blindó** jurídicamente los acuerdos de paz”. Dijo que cuando esto sucede, las amenazas contra los grupos sociales aumentan.

### **Es más peligroso que se evidencie que hay grupos ocultos que no quieren a la UP** 

Frente a la negación de las Autodefensas Gaitanistas, de reconocer la autoría de la amenaza, Santodomingo indicó que es una acción normal. Sin embargo, de ser así, “la situación es mucho más preocupante porque esto **demuestra que hay otras fuerzas** que no quieren que la UP siga existiendo y alzando su voz”. (Le puede interesar: ["Se teje de nuevo la unidad de la izquierda en congreso de la Unión Patriótica"](https://archivo.contagioradio.com/union-patriotica-unidad-izquierda/))

Si bien aún no está claro quién fue el autor de la amenaza, y desde la UP le exigirán al Gobierno Nacional que investigue y aclare el tema, “es necesario que el Gobierno cumpla los acuerdos en lo que tiene que ver con el **desmonte del paramilitarismo** para proteger la vida de todos los colombianos”. La amenaza, para los dirigentes políticos, es real, verídica y comprobable como una acción criminal.

Para Santodomingo, **“en Colombia hay una estrategia de silenciamiento** sistemático contra las personas y las organizaciones sociales que han trabajado por la defensa de los territorios y los derechos humanos de las diferentes comunidades”.

### **Se requiere una estrategia entre las fuerzas políticas alternativas para acabar con la violencia** 

Para éste partido político, es necesario que exista un diálogo en el cual se desarrolle una **estrategia común entre las diferentes fuerzas alternativas**, para poder tener una representación política en donde “se cumpla el propósito de cesar la violencia y se le respete la vida a cada uno de los colombianos”. (Le puede interesar: ["La Unión Patriótica quiere demostrar que hay otra forma de gobernar"](https://archivo.contagioradio.com/la-union-patriotica-quiere-demostrar-que-hay-otra-forma-de-gobernar/))

Con esta intención, Santodomingo afirmó que ha habido diálogos con las diferentes representaciones políticas para **“apostarle a la unidad y construir una coalición** que sea capaz de derrotar la mafia y el paramilitarismo que representan varios partidos políticos que quieren re elegirse".

Es por esto que, para esta colectividad, **es importante que la reforma política**, que se discute en el Congreso de la República, se establezca de manera satisfactoria. Sin embargo, han sido enfáticos en manifestar que “con o sin reforma la Unión Patriótica seguirá buscando la unidad de la izquierda y de los sectores alternativos en Colombia”.

Finalmente, Santodomingo manifestó que **la amenaza que recibieron no puede pasar desapercibida** en la medida que restringe las posibilidades de continuar con el ejercicio político. Por esto, le exigirán a las autoridades que se garantice el derecho a la vida de los colombianos “para poder participar del escenario político”.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
