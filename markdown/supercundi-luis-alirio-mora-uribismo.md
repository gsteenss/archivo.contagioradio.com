Title: Socio de Supercundi y Merkandrea habría participado en acto del Uribismo
Date: 2018-02-23 17:35
Category: Judicial, Nacional
Tags: Alvaro Uribe, Centro Democrático, FARC, Luis Alirio Mora, Merkafusa, Merkandrea, Supercundi
Slug: supercundi-luis-alirio-mora-uribismo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/02/supercundi.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: facebook Jorge Gómez] 

###### [23 Feb 2018]

La denuncia se hace a través de las redes sociales de Jorge Gómez Pinilla, columnista de El Espectador, quién señala que un reportero anónimo le hizo llegar unas fotografías en las que se identifica a Luis Alirio Mora, uno de los hermanos señalados de ser **testaferros de las FARC a través de los supermercados Merkandrea, Merkafusa y Supercundi, participando en acto de campaña del Uribismo en el Sur de Bogotá.**

Según la denuncia de Gómez, el acto de campaña del Centro Democrático se habría dado hace pocas semanas en el barrio Santa Librada en el sur de la capital colombiana y allí habría participado, no solamente Mora sino algunos de los trabajadores de Merkandrea. Esta afirmación la corrobora una fotografía de **una mujer con el uniforme del establecmiento posando al lado del senador Alvaro Uribe.**

En otra de las fotografías se señala a **Luis Alirio Mora, quien hoy es prófugo de la justicia, al lado del senador y en otra fotografía está conversando con el candidato presidencial del Centro Democrático Iván Duque**. Según el columnista esta situación genera serias sospechas dado que no se entiende por qué, un supuesto testaferro de las FARC trabajaría en una campaña del Uribismo. [Le puede interesar: Investiga a Alvaro Uribe por dos masacres en Antioquia](https://archivo.contagioradio.com/alvaro_uribe_investigacion_masacres_antioquia_paramilitarismo/).

### **Saqueos y violencia habría sido instigada** 

Sin embargo, la denuncia no se queda solamente en las fotografías y el hecho de campaña. El columnista también señala que los saqueos y los fuertes hechos de violencia que se han presentado durante esta semana, habrían sido instigados y también a cambio de dineros.

Otra fuente le confirmó a Contagio Radio que muchos de los manifestantes eran menores de edad que venían de otras zonas de la ciudad, aunque también participaron habitantes de los sectores afectados. [Lea también: La telaraña de los falsos testigos de Alvaro Uribe](https://archivo.contagioradio.com/la-telarana-de-falsos-testigos-y-colaboradores-de-uribe-velez/)

Según el columnista, quien también hizo una publicación en Facebook, esta situación resulta por lo menos sospechosa y tendría que establecerse la autenticidad de las fotografías como de las afirmaciones sobre presuntos pagos a manifestantes. **Sin embargo señala que "algo huele mal en Supercundi".**

###### Publicación de Twitter de Jorge Gómez 

![supercundi jorge gomez](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/02/supercundi-jorge-gomez-337x600.jpeg){.alignnone .size-large .wp-image-51741 .aligncenter width="337" height="600"}

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
