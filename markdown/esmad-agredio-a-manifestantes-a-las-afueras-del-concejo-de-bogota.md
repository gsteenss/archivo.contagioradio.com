Title: Manifestantes que defienden la ETB fueron agredidos por el ESMAD
Date: 2016-05-18 12:05
Category: Economía, Nacional
Tags: Alcaldía de Bogotá, Concejo de Bogotá, Enrique Peñalosa, ETB
Slug: esmad-agredio-a-manifestantes-a-las-afueras-del-concejo-de-bogota
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/05/Civ3aS8WsAAFJSr.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: ATELCA 

###### [18 May 2016] 

Desde las 8 de la mañana cientos de trabajadores, estudiantes y defensores de la ETB realizan un plantón pacífico para solicitarle a los concejales que no permitan la venta de la empresa pública. Sin embargo lo que recibieron fue represión por parte del ESMAD, lo que dejó **una persona con una herida en la cara por un golpe de bolillo de parte de uno de los agentes.**

Alejandra Wilches, presidenta de Asociación Nacional de Técnicos en Telefonía y Comunicaciones Afines de la ETB, ATELCA, cuenta que desde las tempranas horas de la mañana “estábamos apoyando los debates internos en el Concejo, se cerraron unas cuadras y **al que se acercara lo estaban empujando y se estaban lanzando gases lacrimógenos**”.

Por su parte, Camilo Nieto secretario general de ATELCA y quien vivió el ataque por parte del ESMAD, asegura que lo único que estaban buscando era ubicarse cerca al edificio del Concejo pero **los agentes del Escuadrón Antidisturbios empezaron a “dar bolillo a los trabajadores”.** Pese a esa situación que ya se superó, Nieto, asegura que continuarán cercanos a las instalaciones del Concejo de Bogotá mientras se realizan los debates de enajenación de las acciones de la compañía de teléfonos.

Allí los representantes de los sindicatos de la ETB, argumentan que sería [u[n detrimento evidente vender la ETB]](https://archivo.contagioradio.com/?s=etb), pues según sus cifras **el Estado ha recibido \$1.3 billones por concepto de impuestos por parte de la empresa, así mismo, del 2002 a 2014 la Universidad Distrital recibió 42 mil millones de pesos**,  y además en los últimos 4 años se han generado utilidades por \$228 mil millones.

Finalmente, la presidenta de ATELCA aseguró que **los concejales que voten a favor van a ser demandados,** como lo serían los integrantes del partido Cambio Radical que ya que dieron su visto bueno a la propuesta del alcalde. Sin embargo, dice Wilches, “Esperamos que se tome una decisión consiente y no por intereses políticos y económicos de la administración”.

<iframe src="http://co.ivoox.com/es/player_ej_11576691_2_1.html?data=kpaimZuafZKhhpywj5WZaZS1k52ah5yncZOhhpywj5WRaZi3jpWah5yncaLgxs%2FO0MnWpYzLytHQysrXb46formyrqilcYarpJKw0dPYpcjd0JC%2Fw8nNs4yhhpywj5k%3D&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>  
<iframe src="http://co.ivoox.com/es/player_ej_11576804_2_1.html?data=kpaimZucdJWhhpywj5WUaZS1kZWah5yncZGhhpywj5WRaZi3jpWah5yncaTVzs7Z0ZCyrcbo0JCajabYqc3XwpKSmaiRh9Di1cbUy9SPlsLYytSYj4qbh46o&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
