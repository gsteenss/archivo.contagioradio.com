Title: Los retos a los que se enfrenta la Bancada de Oposición
Date: 2018-07-23 12:05
Category: Nacional, Política
Tags: Bancada de oposición, Carlos Lozano, Gustavo Petro, Jorge Robledo
Slug: los-retos-a-los-que-se-enfrenta-la-bancada-de-oposicion
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/07/petro.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Twitter Gustavo Petro ] 

###### [23 Jul 2018] 

Ya está conformada la bancada de oposición al gobierno de Iván Duque en el Congreso de la República, sin embargo, el principal reto será establecer cómo funcionará internamente esta bancada que reúne al partido del Polo Democrático, la Alianza Verde, la lista de Decentes y FARC.

El analista y politólogo Fernando Giraldo, asegura que **ese 30% que representa la oposición al interior del Congreso** podría ser la fuerza mayoritaria si logra un común acuerdo. No obstante, sus mayores dificultades radican en la cantidad de vertientes políticas que reúne, que implicará más discusiones para lograr consensos en frente a los temas, y la falta de prebendas a la hora de interlocutar con otros partidos que los convertirá en una bancada por convicción.

“La oposición va a jugar mucho mejor de lo que hizo en el pasado, porque es mucho más fuerte ahora, pero va a tener que jugar muy inteligentemente con las mayorías gubernamentales” afirmó Giraldo y agregó que la pregunta es sí la bancada logrará hacer acuerdos con senadores como Roy Barreras, Armando Benedetti o partidos como el Mira y **comprender que deberá priorizar temas y tener una agenda muy puntual.**

### **La figura del partido independiente** 

Hasta el 7 de agosto cada partido deberá reconocer públicamente si pertenecerá a la oposición del Congreso, si estará a favor del gobierno o si se cataloga como independiente, paso que posiblemente podrían dar el partido Liberal, Cambio Radical y el partido de la U. **En ese sentido, la figura de independiente, particular en la democracia colombiana, significa que es un partido ajeno al gobierno de turno, que podría apoyarlo o no.**

Este hecho según Giraldo lo que ocasiona es que se aumenten las negociaciones cada vez que se vaya a dar un respaldo por parte de estos partidos. Además, eso se podría traducir a que Duque tendrá que estar dispuesto a esos tratos, si quiere obtener la mayoría parlamentaria en las votaciones.

“El gobierno no tiene aseguradas unas mayorías legislativas siempre y para todos los temas. Es decir, tiene mayorías ocasionales, temáticas y que dependerán de la coyuntura política” afirmó Giraldo. (Le pude interesar:["De la Plaza Pública al Congreso: La agenda legislativa de la Oposición"](https://archivo.contagioradio.com/de-la-plaza-publica-al-congreso/))

### **¿En qué esta Iván Duque?** 

Si bien es cierto que Iván Duque está a la cabeza del gobierno y que el Centro Democrático se hizo con la presidencia del Senado, para Giraldo eso no significa que Duque vaya a tener el control de la política del país y mucho menos que vaya a poder lograr todo lo que proponga, según el analista lo que sucederá en este gobierno responderá más a las posturas de **Álvaro Uribe Vélez y los sectores más fuertes dentro de ese partido político.**

<iframe id="audio_27196751" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_27196751_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
