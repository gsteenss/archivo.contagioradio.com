Title: Entre la cuarentena y la solidaridad.
Date: 2020-03-26 08:00
Author: AdminContagio
Category: Columnistas invitados, Opinion
Tags: Coronavirus, Covid-19, cuarentena, solidaridad
Slug: entre-la-cuarentena-y-la-solidaridad
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/03/000_1q05f2_0.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:heading {"level":6,"customTextColor":"#575f66"} -->

###### **Por: Karen Carrillo, Valencia - España** 

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"right"} -->

*Creo que el coronavirus está impactando fuertemente nuestras vidas ya  
sea social, política, económica, cultural o ambientalmente.*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Si bien, en enero los medios nos  
hablaban de las personas contagiadas en Wuhan, a mí me parecía que China estaba  
tan lejos de España y que tenía la capacidad de controlar el contagio, que nada  
nos pasaría, que esto no impactaría nuestras vidas. Ya después en febrero,  
cuando empezaron a hablar del aumento de contagios, seguía siendo algo lejano,  
oigan pero a la par en Italia nos estaban hablando de los primeros contagiados  
y a pesar de que el virus ya estaba en el continente, este seguía siendo un  
problema lejano.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Pero todo empezó el 10 de marzo,  
cuando oficialmente en los medios de comunicación nacionales informaron sobre  
la cancelación de las fallas, unas fiestas de Valencia, que únicamente se  
habían cancelado durante la guerra civil ¿pueden creerlo? Esto de inmediato  
tocó las emotividades de quienes viven acá, a mí me entristeció porque pensé  
“soy la sal, mis primeras fallas y la primera vez que las cancelan después de  
83 años”.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":5} -->

##### Ahí aumentó el caos...

<!-- /wp:heading -->

<!-- wp:paragraph -->

Ya se veían los supermercados casi que vaciados, todas las tiendas atendidas por personas de China estaban cerradas, me impactaba mucho salir y ver las calles casi que desocupadas, porque eso en Valencia no es usual, acá siempre hay fiesta hasta tarde y gente a cualquier hora del día.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Luego de que el estado de emergencia se hiciera oficial en España empezó todo, sé que muchas personas están en mi misma situación: lejos de su país de origen y totalmente solas. Empecé a tener pánico, no paraba de actualizar las noticias a ver qué decían, a ver qué pasaba, empecé a ver a cada momento si el número de contagiados aumentaba en España y me impactaba que la cifra diariamente aumentaba en miles. Ese fue un primer momento, el miedo, pero me preguntaba ¿cuál es mi miedo?

<!-- /wp:paragraph -->

<!-- wp:heading {"level":5} -->

##### Y la respuesta no era contagiarme del virus, la respuesta era la incertidumbre con respecto a lo que vendría durante y después del estado de alarma por varias razones:

<!-- /wp:heading -->

<!-- wp:list {"ordered":true} -->

1.  **Ser migrante** y no saber qué tipo de medidas económicas y sanitarias tomará el gobierno y si éstas serán sólo para las personas de acá o tendrán en cuenta a quienes actualmente viven acá sin discriminar origen.
2.  **Las personas de la calle**, aunque acá no se ven tantas personas viviendo en la calle como en Colombia, igual las hay, hay mucha gente que vive del día a día que el comer o no dependía del dinero que les daban otras persona
3.  **Los y las jubiladas:** en Valencia viven muchas personas de la tercera edad, mayoritariamente solas y dependen de la pensión para comer, claro deben estar pensando ¿pero si tiene pensión cuál es la preocupación? Y la respuesta es el egoísmo e individualismo de la gente. Realmente esto me impactó mucho, el lunes siguiente fui a mercar y habían muchas personas haciendo fila para entrar al supermercado y la mayoría eran de la tercera edad. No entendía por qué y luego mientras hacía la fila, escuché que decían que el dinero de la pensión siempre llegaba a fin de mes y que por lo mismo no tenían la capacidad de abastecerse como otros ya lo estaban haciendo. Había quienes lloraban porque no encontraban lo que necesitaban y porque tampoco les alcanzaba el dinero.
4.  **La xenofobia**, no sólo ante personas de China, también hacia los latinos.

<!-- /wp:list -->

<!-- wp:paragraph -->

A eso se le sumó que a los pocos  
días anunciaron estado de alarma en Colombia y creo que desde ese momento fue  
cuando mis mayores miedos se activaron porque pensaba ¿y las y los trabajadores  
informales? ¿la gente que vive en la calle? ¿los niños y niñas rurales que no  
tienen acceso a internet para las clases virtuales? O ¿los que van al colegio  
motivados por el refrigerio que allí les dan? Pero oigan es que el riesgo  
económico y social no era sólo para ellos, también estaban muchas otras  
personas a la espera de que les congelaran o no sus contratos y esto me hacía  
pensar en las pésimas políticas laborales que tenemos en Colombia, que son  
incapaces de soportarnos en épocas de crisis.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":5} -->

##### El coronavirus ha sido contundente en mostrarnos las verdaderas problemáticas de Colombia y es que creo que la mayoría de colombianos y colombianas, al igual que yo no tienen miedo de contagiarse sino de no tener que comer o un techo para vivir o de las consecuencias de éste.

<!-- /wp:heading -->

<!-- wp:paragraph -->

Esta pandemia nos puso en frente de esos problemas que no vemos por el afán de sobrevivir en el día a día, porque aunque muchos utilicen dicho “un día a la vez” para referirse a sus grandiosos estilos de vida, para otros “un día a la vez” es literal un día de rebusque a la vez. Este virus nos dejó ver con más claridad los problemas sociales, económicos, sociales, culturales y políticos del país, ¡sí, políticos!, porque aunque hay quienes vivan diciendo que no hay que meterle política a esto, pues se equivocan, si tuviéramos buenos gobernantes este tipo de problemas se llevarían de una manera diferente porque si, efectivamente la cuarentena es un privilegio de clase o ¿se han preguntado cuántas personas están aisladas sufriendo de pánico o ansiedad por el temor a lo que pueda pasar social y económicamente? ¿ha hablado el gobierno de eso?

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Hoy empieza a regir el  
aislamiento preventivo obligatorio, al que varios han denominado el decreto del  
“sálvese quien pueda”, porque aún no sabemos que vaya a pasar con las personas  
que si o si deben salir a trabajar para pagar deudas y comer, lo que nos  
muestra que el problema en Colombia es el principal obstáculo para contener el  
virus son la pobreza y la desigualdad.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Ahora, si me preguntan como llevo  
esta situación en España, les cuento que si, aun miro diariamente las cifras,  
pero ya no me asusto tanto, puesto que mis miedos se siguen trasladando a  
Colombia, no sólo por el virus, también por el asesinato de líderes sociales y  
el problema en los centros de reclusión, que no hubiera salido a la luz si no  
es por lo que está pasando.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":5} -->

##### Pero miren que no todo ha sido tan malo.

<!-- /wp:heading -->

<!-- wp:paragraph -->

Aunque efectivamente, hay muchas personas que se siguen consumiendo en su individualismo, es realmente conmovedor ver otras acciones que como sociedad hacemos para que el virus no nos venza. Nunca pensé decirlo pero amo el Internet, creo que sin Internet en plena pandemia estaría más abrumada. Literal muchas personas me han tocado las fibras cuando publican estrategias para llevar recursos a quienes esta situación les está afectando de manera directa y es increíble como entre extraños buscamos la manera de ayudar a quienes lo necesitan, porque aunque el miedo latente cuando empecé a vivir esta pandemia de cerca era que ésta sacara lo peor de nosotros como seres humanos, me di cuenta de la importancia de crear lazos de solidaridad con nuestro entorno, sin distinción alguna, porque aunque el mundo es tan grande, todo esto ha permitido conectarnos desde nuestra humanidad, un gran ejemplo es lo que está pasando Italia ¿Quién se hubiera imaginado que Cuba, Rusia y China hoy unieran esfuerzos para que allí se superara este problema?

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Finalmente, decirles que en la mañana por recomendación de una amiga, leía un artículo de **Byung – Chul**, un filósofo coreano que vive en Berlin, quien señalaba que  pesar de todos estos riesgos que no son menores, es importante hacer énfasis en el pánico que ha desatado esta pandemia que ha sido desproporcional; y es que nos enfrentamos ante un enemigo invisible que tenemos que derrotar o eso es lo que nos han querido mostrar, por eso el cierre de fronteras, por eso el aislamiento este virus nos ha individualizado, ha logrado profundizar en la búsqueda de nuestra propia supervivencia, pero ante esto creo que si lo que estamos viviendo no nos cambia como seres humanos, apague y vámonos, es un momento para seguir avanzando como sociedad, no podemos retroceder, esperemos el virus traiga consigo una verdadera revolución humana.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Ver mas: [Nuestros Columnistas](https://archivo.contagioradio.com/opinion/columnistas/)

<!-- /wp:paragraph -->
