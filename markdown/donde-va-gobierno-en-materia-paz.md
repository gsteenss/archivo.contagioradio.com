Title: "Aún no sabemos para dónde va el Gobierno en materia de paz": Víctor de Currea-Lugo
Date: 2018-10-16 15:30
Author: AdminContagio
Category: Nacional, Paz
Tags: ELN, Iván Duque, Pablo Beltrán, Víctor de Currea Lugo
Slug: donde-va-gobierno-en-materia-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/10/Captura-de-pantalla-2018-10-16-a-las-3.05.18-p.m.-770x400-1.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: @ELN\_Paz] 

###### [16 Oct 2018] 

**El analista y profesor Víctor de Currea-Lugo,** señaló que e**l Gobierno Nacional está desgastando peligrosamente la mesa de conversaciones con el Ejército de Liberación Nacional (ELN)**, porque parece no tener clara su política en materia de paz, mientras la esa guerrilla permanece a la espera del nombramiento de un nuevo equipo negociador que llegue a La Habana para continuar con la búsqueda de una salida dialogada al conflicto.

Tras la salida del equipo negociador por parte del Gobierno el pasado 20 de septiembre, la delegación del ELN ha insistido en la continuación de los diálogos de paz; sin embargo, dadas las operaciones militares recientes en todo el país, **el jefe del equipo negociador Pablo Beltrán, aseveró que el presidente Duque entregó la mesa de conversaciones a los militares.** (Le puede interesar: ["La sociedad tiene que presionar para que sigan las negociaciones con el ELN"](https://archivo.contagioradio.com/sociedad-negociaciones-eln/))

Para de Currea-Lugo, lo lógico ante esta situación es que el Gobierno tuviera "seriedad en su política de paz, pero **lo cierto es que no tiene una política de paz"**; situación que se observa en las inconsistencias en la implementación del acuerdo con las FARC, como del desarrollo de los diálogos con el ELN. Adicionalmente, porque pasados más de dos meses desde la llegada de Duque a la presidencia, aún no concluye su evaluación de la Mesa de conversaciones, por lo tanto, "no sabemos para donde van esas negociaciones".

El analista afirmó que eso se debe, en parte, a que **al interior del uribismo hay dos corrientes encontradas sobre como afrontar el conflicto**: de una parte, un sector que entiende el llamado de la comunidad internacional sobre lo que representaría en materia de inversión extranjera para el país que fracase el  proceso de paz; y por otra parte, sectores que siguen pujando por una salida militar al conflicto.

### **El ELN sí tiene disposición para seguir negociando** 

Desde otro ángulo, **la guerrilla ha mostrado su disposición para la negociación con hechos como las liberaciones de Arauca y Chocó,** porque según el profesor, "fueron liberaciones audaces a las que el ELN no nos tiene acostumbrados", y pueden ser leídas como un gesto de paz que el Gobierno no supo entender. (Le puede interesar: ["ELN afirma que Duque está haciendo trizas el proceso de paz"](https://archivo.contagioradio.com/eln-trizas-proceso-paz/))

De Currea sostuvo que se podría decir incluso que **el ELN "está casi en un cese unilateral",** teniendo en cuenta la mínima porción de acciones militares realizadas por esta guerrilla, en relación con su capacidad militar. Adicionalmente, el equipo negociador de la guerrilla se mantiene completo en La Habana, hecho que sin embargo, no se mantendrá eternamente.

A esta indecisión por parte del Gobierno sobre las negociaciones de paz, se suman las determinaciones que ha tomado sobre la erradicación aérea de cultivos de uso ilícito con glifosato, el tema ambiental, y el incremento en el asesinato de líderes sociales, situaciones que ya ha generado llamados a paro por parte de organizaciones sociales para 2019; y que para De Currea evidencian que **Duque está "jugando con candela"**.

<iframe id="audio_29361760" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_29361760_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en] [[su correo] [[Contagio Radio](http://bit.ly/1ICYhVU)]
