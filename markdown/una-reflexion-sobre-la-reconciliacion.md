Title: Una reflexión sobre la reconciliación 
Date: 2019-09-16 11:31
Author: A quien corresponde
Category: Opinion
Tags: cristianismo., Cristianos, jesus, Reconcilaición, reflexión
Slug: una-reflexion-sobre-la-reconciliacion
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/carretera.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

***Examínenlo todo y quédense con lo bueno***

(1Tesalinisenses 5,21)

Bogotá, 9 septiembre del 2019

 

 

*Diferenciar entre información y opinión *

*es necesario para una buena decisión *

Estimado

**Hermano en la fe**

Cristianos, cristianas, personas interesadas

Cordial saludo,

Por ser una realidad central para vida en sociedad, una exigencia cristiana y una condición para construir las comunidades que Dios quiere, le comparto esta reflexión sobre la reconciliación.

La reconciliación ha sido, frecuentemente, tergiversada y manipulada, desgastada y devaluada a pesar de ser una realidad fundamental para la vida personal, familiar y social. Es una realidad central en varias religiones y, en nuestro caso, en el cristianismo.

Lo anterior, le ha quitado la fuerza cristiana, social y política a la reconciliación, generando daños éticos y humanos a la sociedad. Afortunadamente, hay muchas personas y sectores de la sociedad que están asumiendo esta realidad con seriedad, responsabilidad y profundidad, para bien de quienes han padecido graves afectaciones en su vida, en sus relaciones y a su integridad por diversas causas, especialmente por el conflicto armado.

El reencuentro del número monográfico de la revista Concilio (del año 2003): *“Reconciliación en un mundo en conflictos”* y del artículo de Jon Sobrino *“El cristianismo y la reconciliación: camino a una utopía”*, escrito desde la realidad del Salvador, después de los acuerdos de de paz, hace valiosos aportes, tanto a creyentes como a no creyentes, en la Colombia de hoy. A continuación comparto algunos elementos claves de artículo.

La reconciliación presupone algún tipo de conflicto entre grupos humanos y pueden ser guerras, depredación, sometimientos…; su definición la suelen hacer quienes tienen el poder político y mediático; el término reconciliación es más usado cuando el conflicto es violento y armando que para los conflictos sociales más hondos y profundos, como la opresión sobre pueblos  indígenas y afroamericanos, la existente entre minorías opresoras y mayorías oprimidas. En el Salvador el conflicto se originó por la opresión de unas minorías sobre las mayorías.

El cristianismo conoce el corazón de piedra de personas y pueblos, el misterio de iniquidad que atraviesa la realidad; los conflictos se agudizan con la renuencia a aceptar la verdad, con la tendencia al encubrimiento y con la arrogancia de no querer perdonar y, peor aún, de no dejarse perdonar. El cristianismo reconoce que el pecado tiene poder, el poder que dio muerte al Hijo de Dios y sigue dando muerte a los hijos e hijas de Dios, de forma violenta (represión, guerras)  o lenta (injusticia) y genera división, antagonismo, iniquidad; el poder del pecado es la muerte que se despliega a lo largo de la historia y tiene hondas raíces en la naturaleza humana, como la codicia del dinero, considerada por los primeros cristianos como la raíz de todos los males (Cf. 1Tim 6,10; Col 3,5), como el miedo a los inmigrantes en un mundo de abundancia, como el deseo de acaparar de la economía cruel que destruye la vida de seres humanos, pueblos y la naturaleza, como el sentimiento de superioridad de quienes definen las guerras preventivas.

1.  Sobrino recuerda, que una vez terminado el conflicto armado en el Salvador, se insistió que el camino de la reconciliación requería tres pasos: verdad, justicia y perdón.

**Verdad, ** la divinidad de Dios se muestra en la defensa de la verdad contra la mentira: “La cólera de Dios se ha rebelado contra toda impiedad e injusticia humana, la de aquellos que reprimen con la injusticia la verdad” (Rom 1,18-32). Cuando el ser humano, desconoce la verdad y se funda en la mentira, las cosas no revelan lo que son, el corazón se entenebrece y toda su realidad queda entregada a la deshumanización. En los procesos de reconciliación, la importancia de la verdad es evidente y su dificultad es máxima, sobre todo después de la guerra sucia o la represión; en estos escenarios, sectores de poder político, religiosos y económicos, con muchos medios de comunicación, no exigen verdad sino perdón y olvido.

**Justicia, ** en medio de los conflictos, Dios siempre se revela a favor del débil y oprimido. En el salmo 82, los dioses paganos son rechazado porque no exigen ni hacen justicia en la tierra y  la justicia es la protección de los, sistemáticamente, débiles frente a los, sistemáticamente, poderos; en este salmo la lucha no es entre Dios y los seres humanos, sino entre el Dios de la vida y los ídolos, los dioses de la muerte; el Dios que quiere la reconciliación, se introduce en la historia para defender la vida del pobre.

**Perdón**,  el Dios cristiano es un Dios sin venganza, sin “derechos”, para El lo más sagrado son los seres humanos. El creador que entra en conflicto con la creatura es un Dios falso. La realidad de Dios es estar siempre a favor de los seres humanos aunque los seres humanos estén en su contra. Dios no destruye al ser humano ni siquiera cuando estos dan muerte a su Hijo; en el proceso de reconciliación siempre aparece de alguna manera el perdón, pedir perdón, ofrecer el perdón, aceptar el perdón ofrecido.

Dios realiza los valores de la verdad, la justicia y el perdón, los hace historia en Jesús de Nazaret. Jesús mostró al Dios de la verdad, fue honrado con la realidad y desenmascaró la mentira, en la que veía el principio de la deshumanización fundamental; tambien se ubicó en la línea del Dios parcial a favor del débil, defendiéndolos de los opresores y buscando el fin de la opresión y el comienzo de la mesa compartida; Jesús se colocó en la línea de un Dios que no quiere la derrota de  sus enemigos, que en la cruz los perdona y pidió hacer lo mismo a sus seguidores, devolver bien por mal y a perdonar a los enemigos. A una vida anti-reconciliación Jesús opone la vida según la lógica del amor y del servicio, con el énfasis explícito del amor el pobre, al débil y a la víctima.

La reconciliación es sumamente difícil y es una utopía necesaria que solo puede acontecer plenamente al final y a través de un proceso difícil. La reconciliación plena es muy difícil, pero aunque solo se logre algo de reconciliación, muchas veces, es absolutamente necesario apostar por ella, aunque no se pueda poden fin a los conflictos porque la vida se hace imposible en medio de ellos.

“El ideal cristiano de la reconciliación es la mesa compartida, el Reino de Dios. El aporte fundamental del cristianismo el propiciarlo en la historia, en la tensión entre pasos realistas necesarios, y, lo suyo más específico, pasos guiados por la esperanza utópica de plenitud, expresadas en el mismo dios y su Cristo”.

Gracias a Jon Sobrino por las claridades que nos hace sobre un tema tan importante para nosotros hoy.

Fraternalmente,

 Alberto Franco, CSsR, J&P

francoalberto9@gmail.com

 
