Title: Academia Colombiana de cine elige cintas que irán por el Oscar y el Goya
Date: 2015-09-14 12:47
Author: AdminContagio
Category: 24 Cuadros, Cultura
Tags: Academia colombiana de artes cinematográficas, Burning blue, Carlos Acevedo, Ciro Guerra, el abrazo de la serpiente, Ella película colombiana, Franco Lolli, Gente de bien, GMO producciones, La tierra y la sombra, Libia Stella Goméz, Siempre viva
Slug: academia-colombiana-de-cine-escogera-cintas-que-van-por-el-oscar-y-el-goya
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/portada.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [14 Sep 2015]

A las 12:00 a.m. del próximo 15 de septiembre termina el plazo para que los 28 miembros activos de la Academia colombiana de Artes y Ciencias Cinematográficas elijan las dos cintas que representarán al cine nacional en las próximas ediciones de los premios Oscar de la Academia estadounidense y los Goya españoles.

"**El abrazo de la serpiente**" de Ciro Guerra, "**La tierra y la sombra**" de Carlos Acevedo,  "**Ella**" de Libia Stella Gómez y "**Gente de bien**" de Franco Lolli, estrenadas en 2015, y "**Siempreviva**" de Klych López por estrenar; son las 5 producciones pre-seleccionadas por la Academia en cabeza de la actriz Lucero Luzardo.

Las cintas fueron postuladas por convocatoria abierta relaizada entre el 11 y el 23 de Agosto; donde podían participar  aquellas películas que salieron a salas comerciales desde el 1 de Noviembre de 2014 hasta el 31 de Octubre de 2015, para el caso de los Goya, y desde el 1 de Octubre de 2014 al 30 de Septiembre de 2015 para los Premios de la Academia de Hollywood.

El panorama para las producciones postuladas, es optimista teniendo en cuenta los buenos resultados obtenidos por el cine nacional en Festivales del mundo, incluyendo la más reciente edición del Festival de Cine de Cannes, recibiendo elogios por parte del público y la crítica especializada.

Pasadas las 12:00 a.m., al cerrar las votaciones  se realizará el conteo de votos por parte de la Academia y se dará a conocer los filmes ganadores.

**El abrazo de la serpiente (2015) Ciro Guerra.**

<iframe src="https://www.youtube.com/embed/FdOYd-21qaA" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

**La tierra y la sombra (2015) Cesar Acevedo.**

<iframe src="https://www.youtube.com/embed/eXPagOt9nYI" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

**Gente de Bien (2014) Franco Lolli.**

<iframe src="https://www.youtube.com/embed/5XD4waS2-bs" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

**Ella (2015) Libia Stella Gómez.**

<iframe src="https://www.youtube.com/embed/yrgcLQgP1IA" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

**Siempre viva (2015) Klych López (estreno octubre 1)**

<iframe src="https://www.youtube.com/embed/1EFokEx31u4" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>
