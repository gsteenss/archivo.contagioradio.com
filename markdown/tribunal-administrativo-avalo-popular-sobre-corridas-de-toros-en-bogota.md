Title: Tribunal de Cundinamarca avaló consulta popular sobre corridas de toros en Bogotá
Date: 2015-08-25 17:40
Category: Animales, Nacional, Voces de la Tierra
Tags: Bogotá Sin Toreo, Concejo de Bogotá, Consulta Antitaurina, Tribunal Administrativo
Slug: tribunal-administrativo-avalo-popular-sobre-corridas-de-toros-en-bogota
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/08/11846142_10153390761890020_519767596_n.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Contagio Radio 

El Tribunal Administrativo de Cundinamarca dio su aval a  la consulta popular  para que el próximo 25 octubre la ciudadanía decida el futuro de las corridas en la ciudad.

El Tribunal  dio su aprobación con base en la constitucionalidad de estos mecanismos de participación popular, lo que quiere decir que la consulta es legal y se rige bajo los requerimientos de la Ley y la Constitución política.

La consulta ya cuenta con el aval político y constitucional para que el próximo 25 de octubre los bogotanos digan si o no a las corridas de toros y novilladas en Bogotá.

 
