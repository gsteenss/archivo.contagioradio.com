Title: Organizaciones sociales plantearon sus expectativas a la Comisión de la Verdad
Date: 2018-02-16 14:21
Category: Nacional, Paz
Tags: comision de la verdad, conflicto armado, Organizaciones sociales, víctimas
Slug: organizaciones-sociales-plantearon-sus-expectativas-a-la-comision-de-la-verdad
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/11/sin-olvido-700.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Conpaz] 

###### [16 Feb 2018] 

Organizaciones de la sociedad civil, que hacen parte de la Mesa por la Verdad, se reunieron con los integrantes de la Comisión de la Verdad que será la encargada de reconstruir la historia del conflicto armado en el país. Allí, se plantearon las **expectativas que hay desde la sociedad** para lograr establecer la verdad de los hechos y así evitar que vuelvan a ocurrir.

De acuerdo con Diego Herrera, integrante del Instituto Popular de Capacitación de Antioquia, “la Comisión que apenas está naciendo tiene que tener un amplio respaldo **social y político** dado que trata un tema que es muy sensible y polarizante”.

### **La sociedad civil ha construido experiencias para organizar los testimonios de la guerra** 

Dijo que ya en el país se ha habido experiencias desde la sociedad civil para **sistematizar los testimonios** que se han aportado a la reconstrucción de los hechos del conflicto armado “en clave de esclarecimiento”. Este nuevo mecanismo de Justicia Transicional “debe recoger estas experiencias para pronunciarse sobre los temas que han estado más alejados de la sociedad”. (Le puede interesar:["Fuerzas armadas abrirán sus archivos a la Comisión de la Verdad"](https://archivo.contagioradio.com/fuerzas-armadas-abriran-sus-archivos-a-la-comision-de-la-verdad/))

Además, realizaron una interlocución con la Comisión en lo que tiene que ver con el mandato de la misma “donde la columna vertebral es el esclarecimiento, el reconocimiento y la **centralidad de las víctimas** en el proceso”. En la reunión, lograron dialogar sobre la interpretación de ese mandato teniendo en cuenta las expectativas que se tiene sobre el funcionamiento de la Comisión.

### **La verdad debe ser entendida como social y movilizadora** 

Abordaron también la forma en cómo se va a desarrollare la participación de las víctimas “frente a la dinámica que se genera alrededor de la Comisión”. Herrera manifestó que “se planteó que la verdad sea vista como un **proceso social** movilizador”. Esto teniendo en cuenta que “la verdad no es sólo la que sale de la Comisión pues hay muchas verdades y experiencias regionales”.

Además, aclararon que la Comisión deberá acoger procesos de investigación para **reconstruir la verdad**. Esto, teniendo de presente los testimonios de las víctimas y las hipótesis que existen en cuanto a las responsabilidades colectivas. Esto es una tarea que tiene la Comisión pues deberá identificar patrones de criminalidad sin ver los hechos como casos aislados. (Le puede interesar:["Organizaciones sociales respaldan la Comisión de la Verdad"](https://archivo.contagioradio.com/comunidades-respaldan-y-apoyan-a-la-comision-de-la-verdad/))

### **Organizaciones sociales han venido construyendo un relato** 

Para el funcionamiento de la Comisión, es importante que se tengan en cuenta los relatos que se han venido construyendo por medio de las investigaciones de diferentes organizaciones de la sociedad civil. Por esto y según Herrera, “lo que ya hay es una fuente, junto con los **testimonios de las víctimas**, para que la Comisión de la Verdad no arranque de ceros”.

Es por esto que varios comisionados se han desplazado a diferentes regiones para analizar lo que se ha producido. El director de la Comisión de la Verdad, el padre Francisco de Roux, ha dicho en repetidas ocasiones que la verdad del conflicto armado **no será la que brinde la institución** en sus informes sino la que se ha venido construyendo en la sociedad.

<iframe id="audio_23831147" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_23831147_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

######  Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 

<div class="osd-sms-wrapper">

</div>
