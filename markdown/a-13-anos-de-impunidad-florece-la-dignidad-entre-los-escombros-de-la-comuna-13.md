Title: Entre los escombros de la Comuna 13 florece la dignidad
Date: 2015-10-15 18:00
Category: DDHH, Nacional
Tags: Comuna 13, Corporación Jurídica Libertad, La escombrera, Medellin, Mujeres caminando por la memoria, Noticias de Derechos Humanos, operación orion, Paramilitarismo, Sin Olvido
Slug: a-13-anos-de-impunidad-florece-la-dignidad-entre-los-escombros-de-la-comuna-13
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/07/escombrera-contagioradio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: documentalamarillo] 

###### [15 Oct 2015]

Este 16 de Octubre, en el marco de las actividades de conmemoración de las víctimas de detenciones arbitrarias, asesinatos, desapariciones forzadas durante la operación Orión en la Comuna 13, se realizarán actividades de memoria y dignificación, tanto en el corazón de la ciudad como en “La Escombrera” y otros sectores de la propia comuna.

**En “La Escombrera, a partir de las 10 de la mañana se realizará el acto de Memoria** “13 años resistiendo contra la impunidad”. Conmemoración de Operación Orión. Organizado por Mujeres Caminando por la Verdad y se presentará un primer informe acerca de los avances de las excavaciones.

Casi simultáneamente se realizará en el Centro Administrativo “La Alpujarra” en el parque de las luces el Performance Cuerpos Gramaticales en el que [100 personas se sembrarán en la tierra durante 6 horas simbolizando la presencia de los más de 92 desaparecidos](https://archivo.contagioradio.com/mujeres-siembran-por-la-memoria-en-la-escombrera/) y desaparecidas durante las operaciones militares comandadas por el General Mario Montoya.

Por último y para cerrar la semana por la memoria se realizará un Homenaje a Mujeres Caminando por la Verdad. Misa y acto de memoria en el Convento Madre Laura Cra. 92 Nº 34D 21 a partir de las 2 de la tarde.

### **La operación Orión y sus consecuencias** 

El 16 de octubre, efectivos militares de la IV Brigada, las Fuerzas de Despliegue Rápido (FUDRA), efectivos de la policía metropolitana y de la policía de Antioquia, con el apoyo del DAS, sumando más de 3000 hombre ingresaron en los barrios de la Comuna 13 de Medellín para realizar la operación de mayor envergadura a nivel urbano en Colombia, muchas de las acciones de las FFMM dirigidas contra la población civil.

Más de 20 acciones militares (según la Corporación Jurídica Libertad), dejaron 650 víctimas directas y al menos 92 casos de desaparición forzada documentados a la fecha. 13 años después la comunidad reclama que el Estado no ha garantizado las condiciones de protección necesaria a la población.

Sin embargo, el control por parte de los grupos paramilitares es más claro y asentado desde la realización de dicha operación. Las milicias urbanas de las FARC y el ELN que controlaban una pequeña parte de la comuna fueron desterradas para instaurar el terror paramilitar.

Así, luego de 13 años de impunidad, durante el 2015 se ha logrado el inicio de las excavaciones en el sector de la “Escombrera” lo que para muchos de los familiares de las víctimas hace [resplandecer de nuevo la esperanza de encontrar a los desaparecidos pero también desenterrar la verdad](https://archivo.contagioradio.com/en-la-escombrera-no-solo-deberan-removerse-escombros-sino-tambien-la-impunidad/) de los crímenes cometidos durante tantos años.
