Title: Nuevos desplazamientos forzados en Buenaventura
Date: 2017-01-05 21:24
Category: DDHH, Nacional
Tags: buenaventura, Desplazamiento forzado, paramilitares
Slug: nuevos-desplazamientos-forzados-en-buenaventura
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/01/La-playita.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio  
] 

###### [5 Enero 2017] 

Cinco familias del sector de Piedras Cantan, **Barrio La Playita** en la Comuna 4 de Buenaventura, fueron desplazadas de manera forzada el pasado 2 de Enero a las 9 pm. Las familias abandonaron sus hogares **luego de recibir amenazas de muerte por parte de los paramilitares alias "Paipa" y "Tito Rojas"** jefes de la estructura criminal en el sector.

Algunos habitantes del sector aseguraron que la amenaza ocurrió dos horas después de que a bordo de un taxi el paramilitar alias "Wali" con hombres armados llegaran al sector y se presentara el enfrentamiento en el que resultó herido el señor Víctor Valenzuela Rivas,  alias "Calentura", hermano de Manuel Valenzuela Rivas, alias "Tito Rojas" y de alias "Paipa".

Por otra parte, familias vecinas denunciaron que las amenazas, el desplazamiento y el enfrentamiento armado entre paramilitares, **“se dio a pesar de que en el barrio  hay militarización permanente de infantería de marina y policía nacional** y a pesar que desde finales de 2016, las autoridades ubicaron cámaras de video en calles del sector”.

Por ultimo, las autoridades revelan que **durante 2016 y los primeros días de 2017, 14 familias en total han tenido que desplazarse forzadamente** del sector de Piedras Cantan por amenazas de grupos paramilitares.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio.](http://bit.ly/1ICYhVU) 
