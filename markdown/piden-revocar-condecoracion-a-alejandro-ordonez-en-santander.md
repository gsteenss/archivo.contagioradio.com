Title: Piden revocar condecoración a Alejandro Ordoñez en Santander
Date: 2017-05-19 16:11
Category: DDHH, Nacional
Tags: Alejandro Ordoñez, Condecoración, ex procurador, ordoñez, Polo Democrático
Slug: piden-revocar-condecoracion-a-alejandro-ordonez-en-santander
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/09/procurador2-contagioradio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Semana] 

###### [19 May. 2017]

Diversas organizaciones sociales, culturales y comunitarias de Santander, mostraron su rechazo y **radicaron un derecho de petición a raíz de la condecoración *Luis Carlos Galán Sarmiento* entregada al exprocurador Alejandro Ordoñez.** Dicha distinción se entregó luego de que 9 de los 11 diputados de ese departamento votaran y aseguraran que Ordoñez ha realizado “aportes para el avance y el desarrollo de Colombia”.

**"Seguimos diciendo que el Santander es digno y no vamos a aceptar esta condecoración,** por eso presentaremos una acción de revocatoria porque esa condecoración tiene vicios de procedimiento y de fondo” relató Jorge Flórez, Concejal del Polo Democrático. Le puede interesar: [Procurador Ordoñez pretende ignorar fallo de la CIDH en caso Palacio de Justicia](https://archivo.contagioradio.com/palacio-de-justicia-procurador-ordonez-pretende-ignorar-fallo-de-la-cidh/)

Según Flórez  la intención del derecho de petición es evaluar dicha condecoración porque el reglamento de la Asamblea Departamental plantea que debe haber un estudio de méritos para poder valer la postulación. Lo que la Asamblea les respondió es que el estudio es la hoja de vida.

Sin embargo, dice Flórez que ese estudio no debe ser solamente la hoja de vida de los procuradores, dado que **deben ser demostrables los aportes para el avance y el desarrollo del país**, en este caso que haya realizado Alejandro Ordoñez. Le puede interesar: [El Procurador viola abiertamente la Constitución: Iván Cepeda](https://archivo.contagioradio.com/el-procurador-viola-abiertamente-la-constitucion-ivan-cepeda/)

**“Es conocido que Alejandro Ordoñez no es un ejemplo para darle esa condecoración,** la pregunta es ¿Qué hizo Alejandro Ordoñez cuando el escándalo de Odebrecht, o el escándalo de Reficar, ¿Dónde estaba Alejandro Ordoñez con el problema de Navelena? Y así un sin número de investigaciones que dejó pasar favoreciendo a sus amigos de la derecha y ultraderecha colombiana” relató Flórez. Le puede interesar: [Procurador Ordoñez "no estaba a la altura del cargo"](https://archivo.contagioradio.com/procurador-ordonez-no-estaba-a-la-altura-del-cargo/)

Además de las **movilizaciones hechas al frente de las instalaciones de la Asamblea Departamental,** que fueron dispersadas por el ESMAD, se convocó a una tuiteratón convocada en redes con el \#CondecoranAlCorrupto. Le puede interesar: [“El procurador se nos metió en la cama, en la casa y hasta en los calzones”](https://archivo.contagioradio.com/el-procurador-se-nos-metio-en-la-cama-en-la-casa-y-hasta-en-los-calzones/)

### **Diputada Ángela Hernández defiende postulación del exprocurador** 

Según Flórez, la diputada Ángela Hernández, del partido de la U y recordada por promover las marchas en contra de la propuesta de educación sexual hecha por el Ministerio de Educación de la exministra Gina Parody, ha defendido de manera acérrima dicha postulación y lo ha considerado como persona no grata en la Asamblea “sabiendo que es un recinto público” concluyó.

<iframe id="audio_18792969" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_18792969_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

[Carta a Asamblea Condecoracion](https://www.scribd.com/document/348878307/Carta-a-Asamblea-Condecoracion#from_embed "View Carta a Asamblea Condecoracion on Scribd") by [Contagioradio](https://es.scribd.com/user/276652802/Contagioradio#from_embed "View Contagioradio's profile on Scribd") on Scribd

<iframe id="doc_21243" class="scribd_iframe_embed" src="https://www.scribd.com/embeds/348878307/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-MgHE3wVrnTwPKJMsigQi&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="0.7729220222793488"><iframe id="audio_18792969" frameborder="0" allowfullscreen="allowfullscreen" scrolling="no" height="200" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_18792969_4_1.html?c1=ff6600"></iframe></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU).
