Title: Democracia colombiana: La máquina de hacer votos
Date: 2015-11-03 10:05
Category: invitado, Opinion, superandianda
Tags: Aida Avella, compra de votos colombia, democracia colombia, elecciones colombia, falsa democracia, partidos politicos
Slug: democracia-colombiana-la-maquina-de-hacer-votos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/11/compra-de-votos-contagio-radio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

#### [**Por [Superandianda](https://archivo.contagioradio.com/superandianda/) - [~~@~~Superandianda](https://twitter.com/Superandianda) 

###### 3 Nov 2015 

[Cuando hablamos de democracia en Colombia obligatoriamente nos remitimos al concepto de maquinarias políticas: los partidos que por décadas han consolidado al poder burocrático; haciendo referencia a este sistema, personalmente, me resulta difícil y casi imposible entender cómo un estado puede llamarse de libre opinión si la opinión de la ciudadanía es fabricada por las patrocinadas maquinarias.]

[No somos libres, las maquinarias políticas son las que ponen  los votos, ponen las opiniones en los medios de comunicación,  y es que hablar de “poner” resultados en Colombia es tan natural que descaradamente estamos llenos de empresas que nos hacen funcionar a su servicio diciéndonos que eso es democracia.]

[En Colombia no existen partidos políticos, existen empresas que funcionan bajo el nombre de la política -política totalmente inexistente, dada  la complejidad que abarca el sentido real de la  palabra y que los ciudadanos desconocen-  No somos un país político, somos un país mafioso, un país empresa que genera su productividad a partir de la ignorancia y  pobreza de su gente.]

[Con el progreso orientado hacia el cemento – perfecto amigo de la corrupción- con sus megaobras, también han pavimentado cerebros, lamentablemente el progreso no ha llegado a nuestra democracia, ante  un país atrasado en su manera de pensar y obtuso ejecutando decisiones colectivas,  se suma  la ausencia de un sistema electoral digno y creíble que pueda garantizar protección al voto de la ciudadanía, es sin dudarlo,  un sistema diseñado para el fraude, desde los recursos que se emplean derrochando papelería hasta el voto que se bota a la urna, que bien podría ser a la basura,  y que se pierde en un conteo]*[picapiedra]*[, casi "contando" con  ábaco.]

[Nada de lo que escriba aquí puede llegar a caer en la exageración tratándose del país que entiende por política al poder de las empresas privadas, por representantes  a los sirvientes de turno de las llamadas maquinarias  y por democracia a un sistema vergonzoso de conteo de votos de papel, donde lo que menos importa es la opinión.]

[Sin educación para entender el sistema político, social y económico del país donde se vive, es muy peligroso e irresponsable llevar a cabo una jornada con mecanismos obsoletos, y con la que supuestamente se decide nuestro futuro político. Combatir la falsa democracia, la máquina de hacer votos, es pedir lo imposible: destruir a las empresas que se llaman partidos políticos y al sistema]*[picapiedra]*[ que llaman elecciones.]

[Finalmente resulta extraño que la dictadura y atraso castrochavista de Venezuela tenga un sistema electoral más moderno y confiable que la democracia y el progreso de Colombia. Sin volver a nombrar nuestras falencias como estado democrático, tenemos un serio problema identificando el significado entre democracia y dictadura, pero tenemos otro problema más grave y abismal: el analfabestismo político.]

[Un fuerte abrazo a la UP Colombia – Aida Avella- que todavía están buscando los votos del 25 de octubre  que se extraviaron.]
