Title: Juez absuelve y ordena libertad al líder social Julian Gil
Date: 2020-11-25 15:15
Author: CtgAdm
Category: Actualidad, DDHH
Tags: #Congresodelospueblos, #JulianGil, #LiderSocial, #MontajeJudicial
Slug: juez-absuelve-y-ordena-libertad-al-lider-social-julian-gil
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/11/Portada_Juli-770x540-1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

Este 24 de noviembre, durante la lectura del fallo de sentencia, la juez especializada de Cundinamarca**, absolvió y ordenó la libertad del líder social Julian Gil**, quien llevaba más de 900 días siendo víctima de un montaje judicial.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Gil fue capturado el pasado 6 de junio y acusado de rebelión, porte de armas y municiones y el delito de receptación.-Sin embargo, durante el juicio que se extendió por más de un año, la **Fiscalía no evidencio ninguna prueba en contra del líder social.** (Le puede interesar:["Julián Gil: A dos años del montaje judicial del líder social"](https://archivo.contagioradio.com/julian-gil-a-dos-anos-del-montaje-judicial-del-lider-social/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Además, de acuerdo con las denuncias que Gil hizo en su proceso judicial, el único móvil que se presenta en su contra, es el testimonio de un hombre asesinado mientras se realizaban las distintas audiencias. Hecho que para él evidencio el montaje judicial por parte del ente acusador, con la intensión de reprimir al movimiento social.

<!-- /wp:paragraph -->

<!-- wp:heading -->

Julian Gil y el trabajo popular
-------------------------------

<!-- /wp:heading -->

<!-- wp:paragraph -->

Julian Gil hace parte de la plataforma política Congreso de los Pueblos, que agrupa a distintas organizaciones a nivel nacional. Cuando fue capturado ejercía como secretario general al interior de la misma. Esta captura se sumo a otras, **que se realizaron en contra de más líderes sociales como Milena Quiroz**. (Le puede interesar: ["Atentan contra la vida de la lideresa social Milena Quiroz"](https://archivo.contagioradio.com/atentan-contra-vida-lideresa-social-milena-quiroz/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Gil, además hace parte de organizaciones barriales como el proceso Quinua, en la localidad de Bosa y se encontraba apoyando un preuniversitario popular en el momento de su captura. (Le puede interesar:["Movimiento Quinua"](https://www.facebook.com/movimiento.quinua))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Con la salida de Julian también se hicieron las lecturas de fallo y ordenaron las libertades de Harry Gil y Joseff Betancourt, ambos también habían denunciado ser víctimas de montajes judiciales.

<!-- /wp:paragraph -->
