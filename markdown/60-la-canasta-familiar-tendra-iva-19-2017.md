Title: 60% de la canasta familiar tendrá IVA de 19% para 2017
Date: 2016-12-20 15:05
Category: Economía, Nacional
Tags: Canasta familiar, Gobierno Santos, IVA 19%, Reforma tributaria
Slug: 60-la-canasta-familiar-tendra-iva-19-2017
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/12/canasta-familiar.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Corrientes] 

###### [20 Dic 2016] 

Distintos analistas aseguran que hay una intencionalidad del Gobierno, en dejar para épocas decembrinas el trámite de la reforma tributaria que afectará el 60% de los productos de la canasta familiar, grabados con un IVA de 16% el cual aumentaría a 19% de aprobarse la ley. **Colombia tendrá entonces un IVA similar al de países ricos** de la Organización para la Cooperación y el Desarrollo Económico **pero con una desigualdad más parecida a la de países empobrecidos. **

El analista Mario Valencia quien hace parte de la Red por la Justicia Tributaria, señala que las dos reformas anteriores “se aprobaron para estas mismas fechas, que es cuando hay menos capacidad de movilización” y resaltó que aunque acogieron algunas sugerencias de anteriores debates, **“es una reforma negativa y lesiva para los intereses de los colombianos y los cambios hechos han sido para empeorar”.**

Añade que proyectos como la actual reforma están en contra del bolsillo de los colombianos y “desconocen totalmente las necesidades de las mayorías empobrecidas del país”. Se logró eliminar el impuesto al gas natural, pero el IVA de 19% para productos de primera necesidad como **alimentos, medicamentos, implementos de aseo personal, productos de uso femenino, bicicletas y motocicletas, no se logró eliminar ni disminuir.**

Valencia explica que esta tercera reforma del Gobierno Santos busca que Colombia tenga un IVA similar a países miembros de la ODCE como Alemania que tiene un IVA de 19%, o Austria, Francia y Reino Unido que tienen un IVA del 20%, **pero con una brecha de desigualdad cercana a la de países como Haití, que tiene una tasa de pobreza de 90%, teniendo Colombia una brecha cercana al 60%. **

“La aprobación de la reforma, en las comisiones económicas **no tuvo ni un sólo debate y se aprobó en menos de 8 horas, eso sólo sucede acá en Colombia”** comentó Valencia. Le puede interesar: [Reforma tributaria hace pagar mayores impuestos al ciudadano de a pie.](https://archivo.contagioradio.com/reforma-tributaria-pagar-mayores-impuestos-al-ciudadano-pie/)

Por último, Mario Valencia resaltó que “debe fortalecerse el movimiento ciudadano para que a futuro **no vuelvan a suceder este tipo de proyectos tan nocivos** (…) **es necesario hacer movilización social para hacer presión publica al Congreso**, hemos trabajado en campañas junto a otras organizaciones pero no ha sido suficiente”.

<iframe id="audio_15220621" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_15220621_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
