Title: Exposición: "Pachamama, lo mágico y lo sagrado Andino-Amazónico
Date: 2015-11-18 12:35
Category: Cultura, eventos
Tags: Arte boliviano, Embajada de Venezuela en Colombia, Exposiciones de arte indígena en Bogotá, Mamani Mamani
Slug: exposicion-pachamama-lo-magico-y-lo-sagrado-andino-amazonico
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/11/bolivia.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### 18 Nov 2015

El trabajo el artista indígena boliviano **Mamani Mamani** estará en Bogotá, en la exposición **"Pachamama, lo mágico y lo sagrado Andino-Amazónico"** evento organizado por la embajada del Estado Plurinacional de Bolivia y la Secretaria Distrital de Gobierno de la capital colombiana.

Se trata de un recuento de las obras pictoricas del maestro de nacimiento quechua y de sangre aymara, nacido en Bolivia en 1962, quién ha desarrollado de manera autodidacta su plástica a partir de la visión y sentimientos originarios de su pueblo, una tierra vibrante, vital, llena de colores, carácter, texturas y emociones.

[![mamani.mamani.retrato](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/11/mamani.mamani.retrato.jpg){.aligncenter .wp-image-17431 width="285" height="268"}](https://archivo.contagioradio.com/exposicion-pachamama-lo-magico-y-lo-sagrado-andino-amazonico/mamani-mamani-retrato/)

"La armonia de la pachamama es la armonía de nuestros pueblos, somos tan distantes y tan cercanos a la vez la cultura nos acerca mas", expresa el maestro sobre su obra que desde 1983 ha sido expuesta en más de 58 escenarios, individualmente en 52 de ellas, y ha sido adquirida en colecciones privadas en países de América Latina, Europa y Asia.

La obra de Mamani Mamani ha merecido numerosos Premios y Distinciones Nacionales e Internacionales, entre los que destacan el 1er. Premio en Fotografía. Día Mundial de la Población, Naciones Unidas, la Distinción como Maestro de Maestros en el Arte Pictrórico, de la Universidad Tomás Frías de Potosí y la Medalla al mérito cultural. Prefectura del departamento de La Paz.

La exposición se inaugura este **18 de noviembre** a las **6:30 p.m.** en el **Centro cultural Simón Bolívar** de la Embajada de Venezuela en Colombia, ubicada en la **Cr 11 \#87-51**, y estará abierta al público hasta el proximo 27 de noviembre de lunes a viernes entre las 9:30 y las 4:00 p.m.
