Title: Todos Somos Río Blanco, convergencia en defensa del territorio, el agua y la vida
Date: 2017-06-01 17:05
Category: Nacional, yoreporto
Tags: manizales, Marcha carnaval
Slug: todos-somos-rio-blanco-una-convergencia-en-defensa-del-territorio-el-agua-y-la-vida
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/06/mz.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Marcha Carnaval] 

###### [01 Jun 2017] 

Por Periferia Prensa Alternativa \#YoReporto

En la capital de Caldas, manizaleñas y manizaleños saldrán a las calles al son del Carnaval, HOY 01 de junio a las 6:00 pm que saldrá de la Torre del Cable, una marcha llena de cultura, vida y naturaleza. Esta iniciativa surge ante la amenaza de un proyecto urbanistico llamado “Tierra Viva Biociudadela” que pretende construir una ciudadela aproximadamente para ser habitada por 10 mil personas junto a la Resarva Forestal Protectora de Río Blanco, esta construcción afectaría directamente la quebrada “siete cueros”.

### **El proyecto que amenaza la reserva Río Blanco** 

Construcciones CFC&A y Vélez Uribe ingeniería S.A, tienen un proyecto de urbanización sobre el suelo de La Aurora, que se encuentra ubicado en el área adyacente de la Reserva Forestal Río Blanco, más exactamente a 800 metros de la misma.

Según la convergencia \#TodosSomosRíoBlanco este proyecto no cumple con la función amortiguadora, donde la ley así lo define “área que permita mitigar los impactos negativos que las acciones humanas puedan causar sobre las áreas protegidas”, además contradice el Convenio sobre la Diversidad Biológica ratificado con la ley 165 de 1994, donde se promueve un “desarrollo ambientalmente adecuado y sostenible en zonas adyacentes a áreas protegidas”, además de ignorar el artículo 31 del Decreto 2372 de 2010, en el que mandata que “El ordenamiento territorial de la superficie de territorio circunvecina y colindante a las áreas protegidas deberá cumplir una función amortiguadora que permita mitigar los impactos negativos que las acciones humanas puedan causar sobre dichas áreas.

El ordenamiento territorial que se adopte por los municipios para estas zonas deberá orientarse a atenuar y prevenir las perturbaciones sobre las áreas protegidas, contribuir a subsanar alteraciones que se presenten por efecto de las presiones en dichas áreas, armonizar la ocupación y transformación del territorio con los objetivos de conservación de las áreas protegidas y aportar a la conservación de los elementos biofísicos, los elementos y valores culturales, los servicios ambientales y los procesos ecológicos relacionados con las áreas protegidas.”

Esta reserva es importante porque su localización se da en una de las zonas con mayor biodiversidad del mundo, convirtiendose en soporte vital para la ciudad y el aporte a la regulación climática, la descontaminación del aire y el surtimiento de agua potable en un 35%  a la ciudad de Manizales.

Según Julian Serna esta área protegida cobra mayor importancia ya que se encuentran cerca de 375 especies de aves, esta virtud hace que sea un lugar para el avistamiento de aves y un lugar nombrado como destino turístico por su gran riqueza cultural y biológica.

En el año 2012 con la presentación de una acción popular se reclamaba el derecho a un ambiente sano y la exigencia de no dejar permitir construcciones de alto impacto en la zona, al día de hoy dicha acción jurídica no ha fallado, por el contrario este silencio a permitido a la constructora continuar con la venta de viviendas de dicho proyecto y sus campañas publicitarias para promocionarlo, así lo manifiesta la convergencia.

Por su parte, Corpocaldas reconoció la importancia de la reserva para el municipio y el departamento, promoviendo medidas de protección del área adyacente, pero estas definiciones se han visto vulneradas por la administración municipal actual quien se opuso a las medidas y las desmontó dando vía a la continuación del proyecto inmoviliario.

Esta realidad que se presenta en la ciudad de Manizales a despertado las voces de organizaciones sociales que en convergencia se han propuesto defender la Madre Tierra, el agua y la vida, decidiendo asumir la \#MarchaCarnaval HOY jueves 01 de junio a las 6:00 de la tarde , se saldrá desde la Torre del Cable, haciendo un recorrido por toda la Avenida Santander hasta el Parque de la Mujer, este sera un Carnaval cultural en defensa de la Reserva y un NO rotundo al proyecto urbanístico.
