Title: Colombia: El país con más asesinatos de defensores en el mundo
Date: 2019-01-16 15:18
Author: AdminContagio
Category: DDHH, Nacional
Tags: asesinato de líderes sociales, Front Line Defenders
Slug: colombia-pais-mas-asesinatos-defensores-mundo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/12/asesinato-de-líderes.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Contagio Radio 

###### 15 ene 2019 

Según la organización irlandesa Front Line Defenders (FLD), Colombia lidera la lista mundial en asesinatos de defensores de derechos humanos, con 126 casos registrados en el 2018, seguido por México con 48 asesinatos, Filipinas con 39, Guatemala con 26 y Brazil con 23.

Cabe resaltar, que **en Colombia fueron asesinados casi tres veces el número de defensores que en México**. Sin embargo, el número total de asesinatos de líderes sociales en el 2018 podría ser más alto debido a que el informe de la FLD no incluye casos registrados en diciembre. Según el Programa Somos Defensores, cuyos datos fueron usados para el reporte de FLD, un registro preliminar de asesinato de líderes sociales para el mes de diciembre documenta seis casos.

Estas cifras alarmantes de violencia se debe a que, después de dos años del acuerdo de paz entre el Gobierno y las FARC - EP, el "**Estado ha fallado en implementar la mayoría de sus obligaciones**, como establecer una presencia integral en las regiones rurales y implementar la restitución de tierras y programas de sustituciones de cultivos de uso ilícito."

A consecuencia, los peligros a los cuales se enfrentan activistas van incrementando. El año pasado fueron asesinados 35 defensores de derechos humanos más que en el 2017. Además, se ha registrado más casos de amenazas de muerte y de encarcelamiento, lo cual ha forzado el desplazamiento de estos líderes, indicó la organización.

### **Fiscal General: sí hay sistematicidad en los asesinatos de los líderes sociales** 

Frente al tema de la continuidad de agresiones contra los líderes sociales, el Fiscal General Néstor Humberto Martínez reconoció que los asesinatos **sí son sistemáticos** por dos razones: el perfil de los victimarios y de las victimas en estos casos que recorren el país muestran similitudes. Según la Fiscalía, los autores de la mayoría de estos crímenes se han reportado como los autodenominados Autodefensas Gaitanistas de Colombia, los Caparrapos y el ELN; mientras tanto, el 50 % de las víctimas consiste de miembros de las Juntas de Acción Comunal.

Este pronunciamiento de la Fiscalía representa un retroceso en sus previas declaraciones que negaba la sistematicidad detrás de los asesinatos. Según Leonardo Díaz, coordinador de protección del Programa Somos Defensores, este reconocimiento es "un paso adelante" aunque demorado debido a que organizaciones sociales como Somos Defensores ha documentado evidencia de sistematicidad detrás de este tipo de casos desde hace más de 10 años.

Ante la declaración del fiscal que la responsabilidad de los crímenes no cae en la Fuerza Pública, Díaz manifestó que **"se ve la responsabilidad del Estado por acción o por inacción frente al accionar de los paramilitares**." Además, al reconocer que estos crímenes siguen un patrón, "el Estado esta reconociendo que ha dejado de hacer lo que le corresponde, que es proteger a la sociedad civil y las organizaciones sociales."

Le puede interesar: "[Informe de Fisalía sobre líderes sociales asesinados por el Estado es irrisorio](https://archivo.contagioradio.com/informe-de-fiscalia-sobre-lideres-sociales-asesinados-por-el-estado-es-irrisorio/)"

######  Reciba toda la información de Contagio Radio en [[su correo]
