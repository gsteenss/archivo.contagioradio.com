Title: Así está Colombia en materia de derechos sexuales y reproductivos
Date: 2016-11-24 21:53
Category: Mujer, Nacional
Tags: Aborto en Colombia, Defensa de derechos de las mujeres, Derechos sexuales y reproductivos
Slug: colombia-derechos-sexuales-y-reproductivos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/mujeresgrito.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Sin Mujeres No Hay Revolución] 

###### [24 Nov 2016] 

De acuerdo al comunicado de la **Mesa por la Vida y la Salud de las Mujeres** emitido en junio de 2016 sobre el panorama en materia de servicios amigables en salud sexual y reproductiva para las mujeres, particularmente en el caso de Bogotá, ha presentado **serios retrocesos que impiden el cumplimiento efectivo de los derechos sexuales y reproductivos para las mujeres** desde un enfoque diferencial e integral.

### **¿En qué consisten esos derechos sexuales y reproductivos?** 

Los derechos sexuales y reproductivos de las mujeres que abarcan puntos claves como el pleno **disfrute de una vida sexual placentera,** sin miedos, temores y prejuicios; el **derecho a expresar libre y autónomamente su orientación sexual;** el vivir una **sexualidad sin sometimiento,** miedo, acoso, coacción u abuso; el **respeto por la opción de reproducción;**  el derecho a disponer **servicios de salud adecuados y a recibir protección ante la amenaza o la violación de los derechos fundamentales sexuales** y reproductivos, siguen siendo vulnerados en Colombia.

El derecho a la interrupción voluntaria del embarazo sigue siendo uno de los más vulnerados a las mujeres en Colombia. Según el artículo emitido en 2015 por Casa de La Mujer, aún después de la **sentencia C355 de 2006** **de la Corte Constitucional** en la que se despenaliza el aborto bajo tres causales, **el 99,9% de los abortos para 2015 se daban por fuera de la ley afectando la salud e integridad de las mujeres** al ser practicados en condiciones inseguras. Le puede interesar: [Organizaciones y activistas conmemoraron el día por la despenalización del aborto.](https://archivo.contagioradio.com/organizaciones-y-activistas-conmemoraron-el-dia-por-la-despenalizacion-del-aborto/)

### **¿Por qué no se hacen efectivos esos derechos?** 

La **falta de información de las mujeres** frente a este proceso, **la vergüenza** que estas pueden sentir al presentarse a los centros de atención para practicarse un aborto y la **objeción de conciencia por parte del médico** son unas de las razones más frecuentes señaladas en el informe.

En 2015 el Fondo de Población de Naciones Unidas en asocio con ONU Mujeres y la Oficina de Coordinación de Asuntos Humanitarios (OCHA), realizó un foro que tuvo por nombre “*Haciendo visible lo invisible”*  *garantía de derechos de adolescentes, jóvenes y mujeres en situaciones de emergencia*. Allí **se evidenció la crítica situación en materia de los derechos sexuales y reproductivos de las mujeres en contextos de violencia.**

Las mujeres y las adolescentes sufren mayor riesgo de abuso y explotación sexual, violencias, embarazos no deseados, infecciones de transmisión sexual, ** dada la falta de acceso a servicios de salud y protección que garanticen sus derechos** fue uno de los principales argumentos expuestos durante el foro.

###### Reciba toda la información de Contagio Radio en [[su correo]
