Title: “Estamos frente a un nuevo genocidio" Carlos Lozano
Date: 2016-11-21 11:50
Category: DDHH, Entrevistas
Tags: marcha patriotica, Persecución a defensores de derechos humanos
Slug: estamos-frente-a-un-nuevo-genocidio-carlos-lozano
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/Foto-Lider-asesinado-marcha.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Marcha Patriótica ] 

###### [21 Nov 2016] 

Con el asesinato de tres personas y los atentados en contra de dos defensores de derechos humanos, en diferentes regiones del país, se prenden las alarmas frente a la persecución al pensamiento político y sobre todo al movimiento social Marcha Patriótica, de acuerdo Carlos Lozano, vocero de esta plataforma **“estamos frente a un nuevo genocidio, que nos recuerda los peores días del genocidio de la Unión Patriótica”.**

Y es que a lo largo de este año Marcha Patriótica a denunciado públicamente que los asesinatos y amenazas en contra de sus líderes han aumentado de forma escalofriante, según su último reporte van **123 miembros asesinados**. Para Lozano “lo que ha pasado esta semana es sumamente trágico y es un llamado al gobierno y **Estado Colombianos para que genere medidas drásticas de garantías, protección  y seguridad y sobre todo para desmontar estos grupos  criminales que atentan contra la paz”.**

El Comité Permanente por la Defensa de los Derechos Humanos expresó en un comunicado de prensa que los hechos que se han registrado durante esta semana “**no corresponden a hechos aislados o casos fortuitos**, corresponden a un claro plan sistemático de exterminio y a un atentado al actual proceso de paz”. Le puede interesar: ["Con vigilia mujeres repudian 112 asesinatos de miembros de Marcha Patriótica"](https://archivo.contagioradio.com/con-vigilia-mujeres-repudian-112-asesinatos-a-integrantes-de-marcha-patriotica/)

Según la información recopilada por el Comité en lo corrido de este año se han presentado **314 casos de amenazas individuales** a miembros del movimiento social o defensores de derechos humanos, **323 amenazas**, **21 atentados y 70 homicidios selectivos** y sistemáticos. Números que corroboran lo que diferentes organizaciones han venido denunciado y es “la presencia de y persistencia en diferentes regiones del país del fenómeno y las estructuras paramilitares”.

Otro factor de que es grave para Lozano, tiene que ver con la Unidad de Protección a Víctimas que ha señalado que no tiene fondos para proteger a todas las personas a amenazadas  y afirmá que c**on la implementación de los acuerdos se aspira a que se fortalezca la democracia y se generen nuevas condiciones de apertura política** en el país, para que se modifique la actual condición de Colombia.

<iframe id="audio_13865124" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_13865124_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>
