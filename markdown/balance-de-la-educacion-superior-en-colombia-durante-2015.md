Title: Balance de la educación superior en Colombia durante 2015
Date: 2015-12-31 12:15
Category: Educación, Otra Mirada
Tags: educacion
Slug: balance-de-la-educacion-superior-en-colombia-durante-2015
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/12/EDUCACION.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Contagio Radio 

<iframe src="http://www.ivoox.com/player_ek_9927096_2_1.html?data=mp6fmZWdeo6ZmKialJqJd6Knk5KSmaiRdo6ZmKiakpKJe6ShkZKSmaiRhsLgwtPQx5DIqYzgwpDSxtrHpcTdhqigh6eXsozn1tXS1M7TtozZz5Cw0dHTscPdwpDR19fFstXZjJeah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [30 Dic 2015 ]

[En materia de educación durante el 2015 se evidenciaron **más retrocesos que avances en términos de financiación, cobertura, calidad, normatividad y movilizaciones**. El año inició con el anuncio del ministro de Hacienda Mauricio Cárdenas de la aprobación de \$29,4 billones para el presupuesto en educación superior, una cifra que **no permitió a las universidades públicas superar la crisis presupuestal a la que se enfrentan**, con un déficit de por lo menos \$16 billones, y que en cambio posibilitó a las universidades privadas incrementar su patrimonio.]

[El **aumento presupuestal del 7.5% en relación con 2014, fue destinado a la financiación de la demanda** a través de créditos educativos ofrecidos por el ICETEX en diferentes modalidades como el programa ‘Ser Pilo Paga’ SPP y los subsidios de sostenimiento para la permanencia de los estudiantes en el sistema. De acuerdo con el ‘Observatorio de la Universidad Colombiana’ con los recursos estatales destinados para los 40 mil bachilleres beneficiarios de SPP se hubiera podido financiar la **permanencia de por lo menos 500 mil jóvenes en universidades públicas**.]

[En cobertura, las **cifras oficiales del MEN variaron permanentemente durante este año, oscilando entre el 46.1% y el 47.8%**. Para marzo se indicó que el avance en cobertura llegó al 47.8%; para abril se habló de un 47% "sujeto a verificación"; en septiembre se aseguró que la tasa era cercana al 47 % y en noviembre se indicó que era de 46.1%. En todo caso éste es **un promedio muy bajo** con relación al de los 34 países pertenecientes a la OCDE que reportan un índice de 75%.]

[Estos **mínimos incrementos en cobertura fueron asumidos por las universidades públicas a cambio de recursos insuficientes** que las obligaron a vender sus servicios académicos, disminuir sus costos de operación y tratar de mantener su productividad, descuidando la infraestructura, dotación y calidad de los programas de pregrado y posgrado. Situación que ha determinado la acreditación **de las 81 universidades públicas del país**, de las que **sólo 9 cuentan con esta calificación** frente a las 19 instituciones privadas que lograron escalar este peldaño.]

[Ante la desfinanciación estatal y las reformas que el MEN y el Gobierno aprobaron, entre las que se destaca el decreto 2450 que modifica los criterios para el registro calificado de las Licenciaturas en el país, **estudiantes, profesores y trabajadores del sector se movilizaron en distintas ciudades exigiendo autonomía universitaria, adecuada financiación para 2016 y dignificación de la profesión docente**, proyectando para el año que viene continuar articulados en la defensa de la educación pública, gratuita y de calidad para todos y todas.  ]

[En materia de avances, durante este año se registró el **lanzamiento de la propuesta de ‘Cátedra abierta de Universidades por la Paz’** liderada por la ‘Red CONPAZ’ y la Comisión Intereclesial de Justicia y Paz’, un proyecto que pretende la creación de 12 universidades para que desde los territorios y las comunidades se generen **conocimientos teórico-prácticos alrededor de la paz mediante el diálogo de saberes** y a través de 3 ejes ‘Ambiente, Territorio y Paz’; ‘Identidades y Género’ y ‘Derechos Humanos’. Otro de los logros es el anuncio hecho por el saliente alcalde Gustavo Petro sobre la **disponibilidad de cerca de 4.500 cupos gratuitos** que vendrán con la entrega parcial de la sede de la Universidad Distrital, en la localidad de Bosa.]
