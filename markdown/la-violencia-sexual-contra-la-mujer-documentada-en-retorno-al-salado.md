Title: La violencia sexual contra la mujer documentada en "Retorno al Salado"
Date: 2015-12-16 10:56
Author: AdminContagio
Category: 24 Cuadros
Tags: Documental regreso a El Salado, Masascre El Salado, víctimas de violencia sexual, Violencia contra la mujer
Slug: la-violencia-sexual-contra-la-mujer-documentada-en-retorno-al-salado
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/12/no-mas.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### Foto: eluniversal.com.co 

##### [16 Dic 2015] 

En medio del largo conflicto armado colombiano, las mujeres y niñas del país han resultado vulneradas en sus derechos, en particular por la violencia sexual a la que historicamente han estado sometidas por parte de diferentes actores armados que hacen presencia en comunidades vulnerables gracias a la ausencia estatal.

"El retorno a El Salado: miradas de la violencia sexual en zonas de conflicto", es un cortometraje documental que hace parte de "Las leyes del silencio", proyecto basado en el libro homónimo de Lenny Schouten, promovido por la Organización Internacional para las Migraciones, ONU mujeres y la Delegación de la Unión Europea en Colombia.

El audiovisual de 20 minutos de duración, hace foco particular en las víctimas de violencia sexual de "El Salado" corregimiento ubicado en Carmen de Bolívar y su marcha de regreso; un trabajo de producción realizado por el mismo Schouten, a quien se suma Jan van den Berg y Martijn Schroevers.

La presentación del corto documental, que tendrá lugar este miércoles 16 de diciembre, contará con la presencia de dos mujeres que con sus historias sirven de hilo conductor a la producción, en conversación con la periodísta Jineth Bedoya, activista por los derechos de la mujer y víctima de violación por parte de paramilitares.

En los últimos 30 años, la fiscalía tiene en sus registros cerca de 4000 víctimas de violencia sexual, cifra que corresponde a los casos denunciados, cifras que según explica Ana Paula Zacarias, embajadora de la UE en Colombia "son alarmantes y tenemos que continuar trabajando para evitar que esto siga sucediendo".

La funcionaría participará en el lanzamiento, junto con Belén Sanz, representante de ONU mujeres en Colombia, Embajadores de algunos Estados Miembros de la UE en el país, integrantes del cuerpo diplomático acreditado en Colombia, entidades del Estado relacionados con el tema, representantes de organizaciones de la sociedad civil y los realizadores del documental.
