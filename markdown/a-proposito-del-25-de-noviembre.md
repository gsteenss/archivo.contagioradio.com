Title: A propósito del 25 de noviembre
Date: 2015-11-29 15:35
Category: Mar Candela, Opinion
Tags: 25 de noviembre día de la no violencia contra la mujer, feminismo, patriarcado, Violencia de género
Slug: a-proposito-del-25-de-noviembre
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/11/fotlog.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### Foto:Fotolog.com 

##### Por: [Mar Candela](https://archivo.contagioradio.com/mar-candela/) 

##### [29 Nov 2015] 

“Pobrecitas nosotras mujeres”, es como sonamos para varias personas cada vez que levantamos nuestra voz en contra del patriarcado, machismo, sexismo, heteronormatividad y la monogamia impuesta. Así les sonamos tanto a hombres y mujeres que nos acusan a quienes resistimos y exigimos nuestro derecho a vivir sin miedo. Y nos rotulan de exageradas, lastimeras, ridículas y hasta de locas.

Puedo decirles que sus reproches son risibles que su prófuga ignorancia sobre la realidad histórica, política, social y de derecho debe ser retenida por unos cuantos libros. Estoy hasta los ovarios de escuchar afirmaciones de semejante factura sobre de la violencia hacia las mujeres:

- Es victimismo femenino, es sexismo porque es otro modo de discriminación.

- Los hombres también son maltratado, violados todo lo demás.

- Que sea la ocasión para darles a conocer la indignante noticia que en Colombia cada día son asesinadas dos mujeres, la mayoría por sus parejas o ex.

-Que las principales víctimas del conflicto armado son mujeres ¡3.657.438! 438.906 asesinadas, 72.910 desaparecidas y 9.892 víctimas de algún tipo de violencia sexual. (INMLCF2014/2015).

Lo curioso es que hasta no hace mucho se hablaba de que en Colombia nos asesinaban dos veces por semana y que cada cuatro minutos una mujer denunciaba violencias.

Pese a esta escandalosa cifra hay quienes reprochan el hecho de que exista la línea 155 en Bogotá para que las mujeres pidan ayuda en caso de ser víctimas – y sin ninguna contemplación de fondo a la realidad de nosotras afirman que las mujeres exageramos en nuestras demandas.

También informo que si bien es cierto que algunos hombres son abusados y maltratados, es fundamental comprender y dimensionar la violencia que ellos viven con una mirada clara sobre la historia que nos precede como humanidad y sociedades :

- ¿Cuántos hombres son golpeados, acosados, violados y asesinados solo por el hecho de ser hombres libres y decidir sobre todos los aspectos de su vida por las mujeres?

–Los hombres son abusado y violados por otros hombres con pocas excepciones aisladas alguno ha sido obligado a tomar viagra por alguna mujer y sometido a tener sexo.

No me alejo de la realidad de los hombres que viven violencia intra familiar de mano de las mujeres con quienes comparten la vida y del hecho de que no se conocer con certeza las cifras sobre estos casos – preguntémonos la razón porque no se conocen estas cifras. La principal razón es que los hombres temen ser burlados y cuestionados por “dejarse de su mujer”.

No ayuda el entorno social donde efectivamente a los hombres que denuncian abuso intrafamiliar son cuestionados en su hombría ¿quién tiene la responsabilidad de que eso suceda? El sistema social machista y no fuimos las mujeres las que impusimos el sistema. Los hombres tienen que emprender su propia lucha contra el patriarcado - Tienen que empezar a denunciar sin miedo para que cuando todos denuncien se pueda reconocer este problema y combatirlo del modo que se combate el abuso a las mujeres.

No soy tan facilista de culpar a los hombres de su suerte – creo firmemente que son tan victimas del patriarcado como nosotras – que el machismo los deshumaniza y que es hora que lo comprendan pero nosotras no vamos a hacerles la tarea. Y sin justificar a las mujeres abusadoras claramente las abusadoras de los hombres por lo general lo hacen con el argumento débil e invalido de que los hombres deben probar un poco de su medicina y casi siempre son mujeres que antes de ser victimarias fueron víctimas alguna vez en su vida

- Es hora de parar, es hora de que los hombres empiecen a buscar modelos de masculinidades alternativas que les despojen de sus miedos que los lleven a explorar una vida libre de la presión de un “deber ser” que nunca debió ser - .- unas nuevas masculinidades que cuestionen los privilegios que le brinda el patriarcado desde siempre y de modo gratuito.

Es hora que las mujeres aceptemos que vivir en el régimen patriarcal de alguna manera resulta cómodo porque se trata de ser “princesitas que necesitan el respaldo de su príncipe protector” y empezar a renunciar a la ilusión de seguridad que el patriarcado nos brinda.

Es evidente que las mujeres vendemos nuestra libertad, y derechos al primer “caballero amoroso” que nos brinde su protección a cambio de nuestra sumisión porque nos da miedo asumir la responsabilidad de nuestras vidas solas porque nos educaron para odiar la soledad y asumir que somos fracasadas si un hombre no nos acompaña.

Si queremos cambiar la realidad , si queremos sociedades donde mujeres y hombres vivan libres del sexismo que nos condena a una vida miserable. si queremos que ya no existan más muertes a razón del patriarcado el primer paso es comprender que debemos comprometernos a replantear el lenguaje amatorio y las dinámicas de construcción de relaciones sexo afectivas, tenemos que despojaron de la afecto dependencia. el segundo paso es romper el molde que nos impuso el patriarcado como único modelo idóneo de mujer y de hombre.

Luego ya vendrá la acción concreta y política de entendernos como humanidad antes que como géneros. Solo así empezaran cambios estructurales en las sociedades. Dijo Angela Davis: El feminismo es la idea radical que sostiene que las mujeres somos personas” y mientras las mujeres no seamos tratadas como humanas nuestra resistencia seguirá vigente. El único camino es que revolucionemos desde nuestra realidad la mente y el corazón y nos desconectemos todos y todas de la tradición que tanto nos daña.

Seguiremos levantando días conmemorativos sobre la realidad de la mujer como herramienta pedagógica y le daremos la importancia política necesaria mientras cambiamos las políticas patriarcales.
