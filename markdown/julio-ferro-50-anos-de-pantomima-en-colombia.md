Title: Julio Ferro, 50 años de pantomima en Colombia
Date: 2016-08-03 10:00
Category: Cultura, eventos
Tags: Julio Ferro, Pantomima en Colombia, Tetro
Slug: julio-ferro-50-anos-de-pantomima-en-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/08/b7322680-51bc-4c89-babc-177652c97c52.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Alterego 

###### 3 Agos 2016 

El dramaturgo, actor y maestro de la pantomima clásica en Colombia **Julio Ferro**, cumple 50 años de carrera artística, razón que motiva al Teatro la Mama de Bogotá a rendirle un homenaje con la presentación de dos de sus obras más importantes: **Cuentos Para Soñar** y **Pá Mis Adentros**.

Durante el mes de agosto, en temporada de miércoles a sábado, los espectadores tendrán la oportunidad de apreciar [el trabajo artístico y pedagógico](https://archivo.contagioradio.com/el-arte-es-fundamental-para-la-educacion-basica/) que durante medio siglo Julio Ferro ha llevado a los escenarios, espacios comunitarios y a las aulas de clase, con proyectos de formación cultural en diferentes programas e instituciones educativas a nivel local e internacional.

<iframe src="https://www.youtube.com/embed/jrewqzD-A04" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

Del miércoles 3 al Sábado 13 se presentará "**Cuentos Para Soñar**" compuesta por cuatro historias de pantomima clásica, llevadas a la escena por un personaje que está privado de la libertad por causa de los equívocos de las autoridades y del miércoles 17 al Sábado 27 "**Pá Mis Adentros**" una mirada crítica y sincera que enfrenta a la audiencia con la desnudes de la vida de un cualquiera que generaliza y reúne en si la felicidad, la pena y los sin sabores de la sociedad en que vivimos.

Para finalizar, el miércoles 25 de Agosto tendrá lugar la "Noche de homenaje" donde los asistentes podrán degustar una copa de vino y acompañar al maestro en un brindis por la el amor, la dedicación y pasión que ha demostrado por el teatro y la pantomima durante tantos años de una incansable labor.

**Lugar:** Teatro La Mama, Calle 63 No. 9-60

**Boletería: **Estudiantes: \$15.000 / General: \$25.000

**Reservas:**  (57-1) 2112709
