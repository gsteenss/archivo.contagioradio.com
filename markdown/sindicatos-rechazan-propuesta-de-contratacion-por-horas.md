Title: Sindicatos rechazan propuesta de contratación por horas
Date: 2019-10-08 13:15
Author: CtgAdm
Category: Economía, Nacional
Tags: CUT, FENALCO
Slug: sindicatos-rechazan-propuesta-de-contratacion-por-horas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/CUT-1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: @cutcolombia  
] 

Gremios empresariales y de comerciantes han estado presionando a lo largo de las últimas semanas para que el Gobierno implemente la contratación por horas, y acabe las horas extras nocturnas para que en este tipo se contrate a nuevos empleados. Según advirtieron gremios de sindicatos, **estas medidas serían nua nueva forma de precarización del trabajo**, y serían una de las razones para la realización de la jornada de paro nacional el próximo 21 de noviembre.

### **"La única forma de combatir la informalidad es crear fuentes de empleo"**

En declaraciones a medios de comunicación empresariales, sectores como el representado por la Federación Nacional de Comerciantes (FENALCO) ha sostenido que para solucionar el problema generado por el desempleo (que según el DANE, en agosto estuvo en 11,4%) se debe flexibilizar la contratación, permitiendo que se realice por horas. Según este gremio, ello permitiría absorver parte de trabajadores que están en la informalidad, y crear nuevas fuentes de empleo.

Sin embargo, **Diógenes Orjuela, presidente de la Central Unitaria de Trabajadores (CUT),** asegura que "la única forma de combatir la informalidad es creando fuentes de empleo y el problema es que varios gobiernos, han renunciado a estas fuentes de empleo que son agricultura e industria". Según explica Orjuela, "en un país que importa 14 millones de toneladas de alimentos y que importa la mayoría de productos que se consumen, incluso materias primas" es común que se presenten este tipo de problemas.

El presidente de la CUT advierte que mientras no se solucione de fondo el problema de la matriz productiva, "va a terminar siempre proponiendo medidas paliativas como pisos de protección a la miseria". Para Orjuela, una de las soluciones que también se debe emprender es aumentar los ingresos de los colombianos, para que haya mayor consumo y la economía se reactive. (Le puede interesar: ["Se necesita aumento del 12% en salario mínimo para recuperar la economía: CUT"](https://archivo.contagioradio.com/se-necesita-aumento-del-12-en-salario-minimo-para-recuperar-la-economia-cut/))

Tomando en cuenta la cercanía con la puja por el aumento del salario mínimo que se dará hacía finales de año, el líder sindical afirma que **Gobierno y empresarios "se pegan un tiro en el pie se niegan a entender la necesidad de la que población tenga mayores ingresos"**; de lo contrario, asegura que "en esas condiciones, vamos a tener una mayor precarización del trabajo en Colombia, sumado con una disminución de los ingresos".

### **Las jornadas de movilización que plantean los sindicatos**

El pasado 4 de octubre se reunieron más de 100 organizaciones sociales y sindicatos en un encuentro nacional en el que analizaron los anuncios de reforma laboral y pensional, los recortes a la protesta social y el incumplimiento de los acuerdos alcanzados con el Gobierno. En ese encuentro, acordaron la realización de un plan de acción que contempla acompañar la manifestación estudiantil del próximo 10 de octubre, una toma de capitales para el próximo 17 de octubre  y el paro nacional del 21 de noviembre.

Tras el paro, **se realizará un nuevo encuentro el lunes 25 de noviembre** en que analizarán el resultado de las manifestaciones, y pensaran en nuevas definiciones para 2020 que Orjuela anticipa, "incluyen, indiscutiblemente, una huelga de mayor alcance". (Le puede interesar: ["Vendiendo 8 empresas, gobierno Duque pretende financiar el PGN"](https://archivo.contagioradio.com/vendiendo-empresas-gobierno-duque-pretende-financiar-pgn/))

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>  
<iframe id="audio_42879357" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_42879357_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
