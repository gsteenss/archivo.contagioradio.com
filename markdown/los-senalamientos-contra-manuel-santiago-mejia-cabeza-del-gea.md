Title: Los señalamientos contra Manuel Santiago Mejía cabeza del GEA
Date: 2020-09-22 19:15
Author: AdminContagio
Category: Actualidad
Tags: EPM, GEA, Hidroituango, Manuel Santiago Mejía, Santiago uribe
Slug: los-senalamientos-contra-manuel-santiago-mejia-cabeza-del-gea
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/09/Manuel-Santiago-Mejia.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

La renuncia de los antiguos miembros de la Junta Directiva de Empresas Públicas de Medellín -[EPM](https://www.epm.com.co/site/)-, ha dejado al descubierto no solo los malos manejos que han generado la debacle y crisis financiera de la empresa, sino el actuar particular de algunos de los exmiembros. **Tal es el caso de Manuel Santiago Mejía Correa quien ha sido cuestionado por sus vínculos con Santiago Uribe Vélez, procesado por los delitos de concierto para delinquir y homicidio agravado.** (Lea también: [Lo que hay detrás de los exmiembros de la Junta Directiva de EPM](https://archivo.contagioradio.com/lo-que-hay-detras-de-los-exmiembros-de-la-junta-directiva-de-epm/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Manuel Santiago Mejía, hizo parte de la Junta de EPM desde 2012 hasta su reciente renuncia. Es la cabeza del **Grupo Empresarial Antioqueño –GEA-, conglomerado económico poseedor de varias empresas que se han visto beneficiadas en la contratación pública con la propia EPM**, lo que ha evidenciado los serios conflictos de intereses, que tenían él y otros miembros de Junta, en dichas contrataciones. (Lea también: [Los conflictos de intereses entre la exdirectiva de EPM y el GEA](https://archivo.contagioradio.com/los-conflictos-de-intereses-entre-la-exdirectiva-de-epm-y-el-gea/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

**Por otra parte, Manuel Santiago Mejía fue gerente de la hacienda “La Carolina” en la que presuntamente fue conformado y entrenado el grupo paramilitar denominado los “Doce Apóstoles”, al que se le atribuye el asesinato de por lo menos 50 personas en Yarumal, Antioquia entre 1993 y 1994.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El exmiembro de la Junta de EPM, tenía una sociedad con Santiago Uribe —hermano del expresidente Álvaro Uribe— quien ha sido acusado por el mayor (r) de la Policía, Juan Carlos Meneses, de ser el jefe de los “Doce Apóstoles”, razón por la que actualmente se adelanta un juicio en su contra.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En 2018, Manuel Santiago Mejía declaró en favor de su socio Santiago Uribe, buscando desmentir la versión del mayor (r) Meneses y otros testigos, quienes señalaron que Santiago Uribe se encontraba en una **reunión celebrada en Yarumal, al norte de Antioquia, en la que se habría conformado el grupo paramilitar.** **En medio del juicio, Manuel Santiago Mejía testificó que para esos días se encontraba con Uribe en la Feria de Manizales.**

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### **Manuel Santiago Mejía como miembro de la Junta de EPM**

<!-- /wp:heading -->

<!-- wp:paragraph -->

Manuel Santiago Mejía no ha sido únicamente señalado por los conflictos de intereses por ser cabeza del GEA, grupo empresarial favorecido con contratos con EPM. También recae sobre él y los demás miembros de la Junta, la responsabilidad en decisiones que fueron en claro detrimento patrimonial de la empresa.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

**Decisiones como la compra del Parque Eólico "Los Cururus" que fue adquirido e**n 227 millones de dólares y terminó siendo vendido por 138 millones de dólares como parte de un plan de enajenaciones por la crisis que produjo Hidroituango. ** **

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

**Los sobrecostos en los proyectos Porce III, la** Hidroeléctrica del Teribe - Bonyic en Panamá e Hidroituango; los de este último, que motivaron el anuncio de una demanda multimillonaria contra los consorcios comprometidos, por parte del alcalde Daniel Quintero, la cual produjo la renuncia de los miembros de la antigua Junta.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

**También los sobrecostos en la adquisición de la empresa chilena Aguas de Antofagasta**, **que según algunas estimaciones superaron los 562 mil millones de pesos,** lo que motivó la intervención de la Contraloría de Medellín, la cual documentó hallazgos fiscales en la investigación.

<!-- /wp:paragraph -->
