Title: Ibagué se convierte en la primera ciudad capital de Colombia en prohibir la minería
Date: 2017-07-31 19:28
Category: Ambiente
Tags: Anglo Gold Ashanti, Ibagué, Mineria
Slug: ibague-se-convierte-en-la-primera-ciudad-capital-en-prohibir-la-mineria
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/07/DGGYpg8WAAE6jHc-e1501547249565.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Comité Ambiental 

###### [31 Jul 2017] 

**En plenaria del Concejo municipal de Ibagué, se aprobó el acuerdo número 012, mediante el cual se prohíbe las actividades mineras,** y específicamente dicta medidas para la preservación y la defensa del patrimonio ecológico y cultural del municipio.

Así lo determinó el voto positivo de los concejales Víctor Julio Ariza Loaiza, Juan Evangelista Ávila Sánchez, Carlos Andrés Castro León, Camilo Ernesto Delgado Herrera, Víctor Hugo Gracia Contreras, Harold Oswaldo Lopera Rodríguez, Ernesto Ortiz Aguilar, Linda Esperanza Perdomo Ramírez, Carlos Andrés Portela Calderón, Marco Tulio Quiroga Mendieta, Pedro Mora, Martha Ruiz, Jorge Bolívar y William Santiago Molina.

Mientras que a la plenaria no asistieron Luis Alberto Lozano, Hasbleidy Morales y William Rosas. De igual forma, los concejales Oswaldo Rubio y Humberto Quintero tampoco asistieron, pero se excusaron.

Con este acuerdo Ibagué se convierte en **la primera ciudad capital en prohibir la minería, como también estaría buscándolo Yopal, mediante una consulta popular**. Cabe recordar que en octubre del año pasado fue suspendida la consulta popular minera con la que los ibaguereños buscaban impedir las actividades de empresas como Anglogold Ashanti. (Le puede interesar: ["Consulta popular minera busca frenar 35 títulos mineros en Ibagué"](https://archivo.contagioradio.com/consulta-popular-en-ibague-busca-frenar-35-titulos-mineros/))

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
