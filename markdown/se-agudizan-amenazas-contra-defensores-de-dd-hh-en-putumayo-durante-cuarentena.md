Title: Se agudizan amenazas contra defensores de DD.HH. en Putumayo durante cuarentena
Date: 2020-04-13 20:40
Author: CtgAdm
Category: Actualidad, DDHH
Tags: Sinaloa
Slug: se-agudizan-amenazas-contra-defensores-de-dd-hh-en-putumayo-durante-cuarentena
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/01/EKWb2rWWoAEu9hJ.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: Putumayo/ @CancimanceL

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Con el homicidio de Miguel Ángel Álvarez Lazo, quien fue abordado por hombres armados que le dispararon en varias ocasiones, ya son 12 las personas asesinadas **en Putumayo** en medio del **aislamiento preventivo obligatorio,** ocho de ellos ocurridos en Puerto Asís, las comunidades señalan que dicho accionar sucede ante la mirada de la Policía y el Ejército.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Pese a que la información preliminar indica que Miguel Ángel Álvarez trabajaba como **comisionista de pasta base de coca, y que su muerte habría sido un ajuste de cuentas,** es evidente que la violencia se ha incrementado en medio de la cuarentena en este departamento. [(Lea también: Más de 10.000 familias en Putumayo afectadas por incumplimiento del Gobierno en acuerdo de sustitución)](https://archivo.contagioradio.com/mas-de-10-000-familias-en-putumayo-afectadas-por-incumplimiento-del-gobierno-en-acuerdo-de-sustitucion/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

**Según explica defensores de DD.HH. de la región,** en Putumayo existen dos actores armados que se disputan el territorio, el autodenominado Frente Carolina Ramírez, parte de las disidencias que no se acogieron al Acuerdo de Paz y una estructura al servicio del narcotráfico denominada como 'La Mafia', la que ha tenido el control en los centros urbanos desde la desmovilización del Bloque Sur de las Autodefensas Unidas de Colombia (AUC).

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Respecto a la presencia o no, de lo que se denomina el Cartel de Sinaloa, algunos ligan la organización a un fuerte control de cultivos de uso ilícito y laboratorios en la zona fronteriza con la provincia de Sucumbíos, Ecuador. Por otro lado, diferentes organizaciones de DD.HH. plantean que este fue un grupo creado **para justificar muchas de las muertes que han ocurrido en las zonas rurales,** "es un nombre que oculta muchas cosas, no es un problema nuevo, es una situación que se veía desde antes del Acuerdo de Paz y que con la salida de las FARC-EP ha extendido su presencia".

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Defender los DD.HH. en Putumayo, es más peligroso que una pandemia

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Acerca de la situación que viven los líderes sociales del departamento, el defensor de DD.HH. señala que la situación es cada vez más critica, mientras líderes que promueven la sustitución como **Marco Rivadeneira son asesinados, otros como Jani Silva son víctimas de hostigamientos y planes que buscan atentar contra su vida**. [(Le puede interesar: Lideresa Jani Silva en riesgo tras descubrirse plan para atentar contra su vida)](https://archivo.contagioradio.com/lideresa-jani-silva-en-riesgo-tras-descubrirse-plan-para-atentar-contra-su-vida/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

A esta lista se suman los integrantes de la **Red de Derechos Humanos del Putumayo: Yuri Quintero, Yule Anzueta, y Wilmar Madroñero,** quienes vienen denunciando los daños provocados por la actividad de empresas petroleras en la zona y el incumplimiento del Gobierno en la sustitución de cultivos voluntaria, y que desde inicios de abril han sido advertidos de que en municipios como San Miguel existen ordenes de acabar con sus vidas.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

En **la actualidad, Yuri Quintero no cuenta con ninguna medida de protección alguna** y aunque Wilmar Madroñero y Yule Anzueta si bien cuentan con medidas de protección, como esquemas de protección y escoltas, estas resultan insuficientes para garantizar su protección en todo el territorio. [(Lea también: 8 asesinatos en menos de 40 horas en Putumayo)](https://archivo.contagioradio.com/8-asesinatos-en-menos-de-40-horas-en-putumayo/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

"No hay una respuesta que garantice su seguridad ni la de las poblaciones rurales, este accionar está dirigido a líderes sociales, comunitarios y populares que vienen desarrollando un trabajo comunitario y la posibilidad de un cambio social que se ha ido consolidando", relata el defensor de DD.HH. quien hace énfasis en que estos sucesos se dan medio de la presencia y control de la Brigada XXVII de Selva y la Policía Nacional.

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
