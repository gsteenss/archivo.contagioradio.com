Title: El ingreso a la OCDE y la salud de los colombianos
Date: 2018-06-30 07:00
Category: Mision Salud, Opinion
Tags: OCDE
Slug: el-ingreso-a-la-ocde-y-la-salud-de-los-colombianos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/04/ocde.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: OCDE 

#### [**[Por Germán Holguín Zamorano – Director de Misión Salud](https://archivo.contagioradio.com/opinion/mision-salud/)**] 

###### 29 Jun 2018

[La noticia del ingreso de Colombia a la OCDE ha sido recibida con alborozo por el Gobierno y los medios de comunicación, ya que se trata de un club de 36 países que representan el 45% de la economía mundial y cuyo objeto es mejorar las prácticas de la administración pública en los estados miembros para acrecentar su desarrollo económico y social.]

[Desde la sociedad civil saludamos con esperanza este paso, considerado por muchos, con razón, “uno de los principales triunfos en relaciones internacionales del Presidente Santos”, y deseamos que el próximo gobierno y los subsiguientes sepan aprovecharlo para lograr mejor calidad de vida de los colombianos, generación de empleo formal, estable y bien remunerado, poner freno a la corrupción y construir una sociedad más igualitaria e incluyente, que brinde oportunidades a toda la población, sin distintos de ninguna especie, y garantice los derechos humanos, la ciudadanía activa y la dignidad de cada uno. ]

[Tan importante como los ánimos de fiesta por causa de la OCDE es que la opinión pública conozca, en aras de la transparencia propia de las prácticas públicas de buena calidad que promueve esta organización, qué reformas tuvo que introducir el Gobierno en las políticas, la legislación y las regulaciones internas, durante los cinco años que tomó el proceso de admisión, así como aquellas que tendrá que incorporar en el futuro para satisfacer las sugerencias de los 23 Comités de Expertos de la OCDE. De igual manera, las reformas que efectuó y las que se comprometió a realizar en los meses venideros para asegurar el apoyo de los Estados Unidos, que en la última etapa trató de aprovechar la coyuntura, indebidamente, para conseguir para su industria ventajas que no pudo alcanzar a través del TLC.]

[En lo que respecta a la salud pública, las organizaciones de la sociedad civil que hemos venido vigilando el proceso sabemos, y lo hemos denunciado públicamente, que en abril pasado el Gobierno, cediendo a presiones inaceptables del Representante Comercial y diversos grupos de presión de Estados Unidos, modificó varios artículos del Decreto 433 de 2018, cuyo propósito era controlar los injustificadamente elevados precios de los medicamentos desde el momento en que son aprobados y entran al mercado. El decreto modificatorio (D. 710 de 2018) dilata indefinidamente este control y fue redactado atendiendo los lineamientos de Afidro, el gremio de las multinacionales farmacéuticas, como éste lo ha reconocido abiertamente.]

[Preocupa que esta priorización de los intereses comerciales sobre el derecho a la salud sea fruto de una negociación entre un miembro activo del “Club de las buenas prácticas públicas” y un país que pretende serlo. Lo que denota incoherencia suma y autoriza pensar, y ojalá estemos equivocados, que estamos frente a una nueva instancia al servicio de los intereses comerciales de las grandes corporaciones multinacionales.]

[Otro hecho es que, como respuesta a presiones de multinacionales farmacéuticas, el año pasado el Gobierno, actuando en el marco de la solicitud de admisión a la OCDE, expidió un Decreto que redujo drásticamente la posibilidad de declarar el interés público con fines de reducción de precios, en el caso de medicamentos en condiciones de monopolio.   ]

[Sabemos también que en algún momento del proceso de ingreso Estados Unidos exigió derogar o modificar actos administrativos en los campos de medicamentos biotecnológicos, declaración de interés público del imatinib (medicamente para el tratamiento del cáncer) y mecanismos para estimular la competencia y bajar los precios, los tres con inmenso impacto negativo para el acceso a medicamentos esenciales, especialmente por parte de la población que vive en condiciones de vulnerabilidad. En la medida en que ignoramos si el Gobierno se comprometió o no a tomar estas medidas en el futuro, invitamos a nuestros conciudadanos a acompañarnos en el ejercicio del derecho a saberlo.  ]

**Nada justifica que un esfuerzo realizado durante tantos años en procura de beneficios nobles para el país se convierta en una amenaza contra el derecho fundamental a la salud y al acceso a medicamentos que salvan vidas.**

##### *\*Columna publicada originalmente en* [*Opinión de El Espectador el 19 de junio de 2018*](https://www.elespectador.com/opinion/el-ingreso-la-ocde-y-la-salud-de-los-colombianos-columna-795408)*.*

##### 
