Title: "Plan de Desarrollo de Peñalosa no tiene nada que ver con la paz" Hollman Morris
Date: 2016-05-31 17:00
Category: Economía, Nacional
Tags: Bogotá, Enrique Peñalosa, ETB, Plan de Desarrollo
Slug: plan-de-desarrollo-de-penalosa-no-tiene-nada-que-ver-con-la-paz-hollman-morris
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/05/concejo-de-bogota-e1464731224630.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: [scabogota.org]

###### [31 May 2016] 

Este lunes, el Concejo de Bogotá aprobó la enajenación de la empresa de teléfonos de Bogotá y la construcción del Distrito norte que ocuparía parte de la reserva Thomas Van der Hammen.

Mientras se llevaba a cabo la votación de los artículos del Plan de Desarrollo, sindicatos, estudiantes y otros ciudadanos, protestaban a las afueras del Concejo para exigir que se votara en contra del Plan Distrital de Enrique Peñalosa, pues varios de sus puntos permiten la **enajenación de empresas públicas, desalojos injustificados, urbanizaciones en reservas forestales, peajes urbanos, pagos para circular durante pico y placa**, entre otros.

**“Un Plan de Desarrollo que no tiene nada que ver con una paz con justicia social, un plan que no entiende el momento histórico que vive el país”**, asegura Hollman Morris, concejal progresista, quien incluso asegura que en los discursos de Enrique Peñalosa no se menciona la paz, de hecho denuncia que el alcalde es la punta de lanza del senador Álvaro Uribe, pues concejales del Centro Democrático están intentado incluir artículos que irían en contra de los acuerdos entre el gobierno y las FARC.

Uno de los temas más polémicos e indignantes para la ciudadanía es la venta de la ETB, una empresa que en los próximos años podría estar entregando a la ciudad de uno a tres billones de pesos anuales, según las proyecciones que se tienen para el 2022.

Morris señala que **el trámite para la enajenación de esta empresa pública ha sido “viciado”, denuncia que no ha habido estudios suficientes, como lo dijo la Contraloría,**  y además “se han evidenciado una serie de irregularidades que demuestran el apetito voraz de algunos concejales. Uno entiende como este patrimonio tan importante como la ETB sea vendido… Detrás de esto debe haber un gran negocio que todavía no hemos descubierto”.

A su vez, el concejal también afirma que los argumentos que presentaron los concejales que votaron a favor de la venta “son muy pobres” pues están basados en las declaraciones del gerente de la ETB, **Jorge Castellanos, denunciado por dar información “errónea y parcializada” de la empresa**, y quien ha actuado más como un “conductor de carro de balineras y no como uno de Fórmula 1 como lo sería la ETB. Castellanos ha obrado más como un liquidador que un gerente”, expresa Morris, quien añade que esta empresa tiene un potencial como ninguna  otra empresa en América Latina frente a la fibra óptica.

Concejales en contra de la venta de la empresa indican que **esta situación era predecible, luego de la venta de ISAGEN,** que era la puerta para que otros bienes públicos pudieran venderse.

<iframe src="http://co.ivoox.com/es/player_ej_11745721_2_1.html?data=kpaklpqbdpKhhpywj5WYaZS1lpmah5yncZOhhpywj5WRaZi3jpWah5yncanjzdLO0JCxs9PmytiYj5Cns8%2FXxs%2FOzpC0ttDb08rgy9jYpdShhpywj6jTstXVyM7cjbfFqMrjjJKSmaiReA%3D%3D&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
