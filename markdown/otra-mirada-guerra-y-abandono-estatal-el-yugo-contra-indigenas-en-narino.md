Title: Otra Mirada: Guerra y abandono estatal, el yugo contra indígenas en Nariño
Date: 2020-09-28 19:36
Author: AdminContagio
Category: Otra Mirada, Programas
Tags: Comunidad indígena Awá, nariño, Resguardo Inda Sabaleta
Slug: otra-mirada-guerra-y-abandono-estatal-el-yugo-contra-indigenas-en-narino
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/08/Grupos-armados-en-Nariño.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: Colprensa

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Este fin de semana, además de haberse perpetrado 2 masacres en Chocó y en Soacha, también se registró la pérdida de vidas indígenas, mujeres hombres y niños, en el resguardo Inda Sabaleta en Tumaco, Nariño. Indígenas de la comunidad Awá siguen desaparecidos y otros perdieron la vida durante los enfrentamientos del grupo armado **“Los Contadores” y el frente “Oliver Sinisterra”**. (Le puede interesar: [5 personas muertas y 40 secuestradas en enfrentamientos en Inda Sabaleta, Tumaco](https://archivo.contagioradio.com/inda-sabaleta-tumaco/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En el programa de Otra Mirada sobre la violencia que se viene viviendo la comunidad Awá y el olvido estatal en el territorio nariñense, hicieron parte del panel Juan Orlando Moreano, consejero de Guardia Indígena Pueblo Awá -Unipa, Rosa María Mateus, abogada del CAJAR y Jorge García, coordinador de la Casa de la Verdad - Costa Pacífica Nariñense. 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Durante el programa, los invitados e invitadas señalan que esta crisis se da por el olvido estatal y por la falta de cumplimiento de los acuerdos de paz. Además, comentan que la presencia del Gobierno solo se percibe a través de las fuerzas militares. (Le puede interesar: [Tras cuatro años de la firma del Acuerdo de Paz, la deuda más grande es con el campo colombiano](https://archivo.contagioradio.com/tras-cuatro-anos-firma-del-acuerdo-de-paz-la-deuda-mas-grande-es-con-el-campo-colombiano/))  

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Asimismo, explican que uno de los elementos clave para entender la guerra en los territorios es que estos son puntos estratégicos para los grupos armados en que el punto central es el manejo de rutas del narcotráfico.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

También añaden que el sentimiento que hoy vive la comunidad es de temor de denunciar y por un posible desplazamiento.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Finalmente, comparten qué tan difícil es el acceso para llegar a la verdad que están exponiendo las comunidades en los territorios. (Si desea escuchar el programa del 25 de septiembre: [Otra Mirada: Defender la paz: Cuatro años de la firma del Acuerdo de la Habana](https://www.facebook.com/contagioradio/videos/773438533437813))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Si desea escuchar el análisis completo: [Haga click aquí](https://www.facebook.com/contagioradio/videos/358019121988438)

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
