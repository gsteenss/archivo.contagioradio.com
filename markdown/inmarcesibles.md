Title: Inmarcesibles
Date: 2020-08-25 13:59
Author: JOHAN MENDOZA TORRES
Category: Johan Mendoza, Opinion
Tags: colombia, dignidad, dolor, Inmarcesibles, Lagrimas
Slug: inmarcesibles
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/08/colombia.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: LasdosOrillas 

\[video mp4="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/08/WhatsApp-Audio-2020-08-25-at-9.04.37-AM.mp4"\]\[/video\]

Escuche aquí la columna completa

#### **Por: [Johan Mendoza Torres](https://archivo.contagioradio.com/johan-mendoza-torres/)** 

**[Son tantas las almas amenazadas en Colombia, y otras tantas que ya viajan por las riveras de La Magdalena rumbo a Bocas de Ceniza.]{lang="es-CO"}**

[Los gritos y los últimos alientos se esfuman entre el Cauca veranero, el calor del Catatumbo, el macizo Nariño, la selva sabia del Caquetá, la lejanía Guainía de nuestro oriente, y ese cielo que es mi cielo y es tu cielo.]{lang="es-CO"}

[Son tan pocos los que consideran esto una barbarie, y esos pocos son tan muchos que conducen a los asesinos hacia su tribuna de laureles electorales.]{lang="es-CO"}

[**Lágrimas ya no quedan. Risas siempre sobran**, es la patria solitaria, bonhomía de los olvidados, el paseo por los cementerios de Colombia o las silenciosas fosas bajo la luna que lo sabe todo.]{lang="es-CO"}

[Si viniste a matarme acaba conmigo de una buena vez, pero recuerda: **no dejes ni una idea viva, porque te aseguro que volverán a florecer.**]{lang="es-CO"}

[Si tu único camino es acabar conmigo, entonces mírame antes de mi suplicio y vive conmigo el delirio de tu injusticia combinada con mi martirio.]{lang="es-CO"}

[Porque sé que así será la única forma en que me recuerdes durante las noches en que te sientas culpable.]{lang="es-CO"}

[Si nada te hace sentir culpable y la muerte de colombianos inocentes no te afecta, entonces juramos regresar como fantasmas a atormentarte en tus horas solitarias, seremos vivo aliento de revancha pura y maciza contra tu perdón líquido hijo de una vejez poco sincera y solapada.]{lang="es-CO"}

[Por qué esconder el secreto que todos llevamos dentro… recordarte sin perdón no es algo que marchite nuestra alma. No es un odio, es un deseo; la mojigatería es una cosa que dejamos luego de salir de casa y caminamos viendo los paisajes de nuestra patria.]{lang="es-CO"}

[La revancha no es otra cosa que una madrugadita ordeñada con vivo trino de aves colombianas.]{lang="es-CO"}

**[Queremos morir decentemente en nuestra cama, no bajo la incapacidad que tuviste para aceptar las rondas de tu injusticia.]{lang="es-CO"}**

[Porque no hay aplausos para tus espectáculos, porque la indiferencia a veces sonríe a miles de almas que ahora tienen paz, pero los indiferentes, por menos que parezca, tienen más miedo que nosotros.]{lang="es-CO"}

[Mira cómo sopla este viento por laderas y trochas, mira como mira septiembre con mirada sempiterna.]{lang="es-CO"}

[Las calles ya no hablan tanto, pero el silencio de la ciudad es más profundo que una tumba.]{lang="es-CO"}

[No dejes de mirar porque en un segundo me pierdes y no me encontrarás.]{lang="es-CO"}

[La sorna sobre los cuentos de la vida que ofreces a tus adeptos nos permite ver con claridad las alas de las mariposas durante los arreboles de aquel agosto temperamental.]{lang="es-CO"}

[Del todo inefables las ganas de contarlo todo, de que por un instante no fuera solo tu versión contra la nuestra.]{lang="es-CO"}

**[Sí, sí, sí, sí, ¡ya sé! vivir en Colombia es vivir con una completa contradicción. Es la mayor de las dialécticas, inimaginable concordia en los brazos de la discordia.]{lang="es-CO"}**

[Nos arrojamos a vivir pensado en la patria que quisiéramos, pero la realidad lo niega todo, una y otra vez. La realidad golpea y nosotros sangramos. La realidad grita y nosotros cantamos. La realidad llora y nosotros, aun así, sabemos que no sabemos cómo lo sabemos, pero todo saldrá bien.]{lang="es-CO"}

[No nos escuchamos los unos a los otros, sólo se empeora todo con el ánimo de que este mejor. Por eso la patria que soñamos es la patria que destruimos. Por eso lo que queremos construir es tan solo un cimiento de la discordia.]{lang="es-CO"}

[Bendita sea la anarquía que me arrojó a estos pensamientos y maldito sea el gobierno que comete la barbarie en el filo de la sonrisa de la gente que siempre se olvida; porque no nos digamos mentiras, a muchos no les duele tanto, y siempre olvidan.]{lang="es-CO"}

[Resultaría en una epifanía para el más idiota de los idiotas, porque la vocación es eterna, tiene petricor, es perenne y eso es lo que te resuelve a matarme…no me mientas cobarde, no me mientas.]{lang="es-CO"}

[Cerrera gente colombiana que por ofrecer un tinto quedó condenada a muerte. Tinto suave y melifluo el que tenemos la fortuna de beber en Colombia, que no le bastó tocar garganta, que se queda con cuerpo y fondo de verdad.]{lang="es-CO"}

[Iridiscencia cuando serena sobre la cordillera, iridiscencia que habitaba en el reflejo de tu sonrisa, iridiscencia del sol que no se ha dejado tentar por la luna y esta lluvia que nos habita y que moja todo y que sufre poco.]{lang="es-CO"}

[No tenemos deberes como héroes, pero toda la liturgia de la muerte injusta es una provocación ante lo inmarcesible…]{lang="es-CO"}

**[Inmarcesibles son aquellos a quienes no se olvida.]{lang="es-CO"}**

#### [**Leer más columnas de Johan Mendoza Torres**](https://archivo.contagioradio.com/johan-mendoza-torres/)

------------------------------------------------------------------------

###### Las opiniones expresadas en este artículo son de exclusiva responsabilidad del autor y no necesariamente representan la opinión de Contagio Radio.
