Title: Campesinos se movilizan para rechazar operaciones de Emerald Energy en Caquetá
Date: 2016-07-06 12:39
Category: Ambiente, Entrevistas
Tags: Amazonía colombiana, emeral energy en colombia, explotación petrolera en colombia, vereda La Curvinata
Slug: 500-campesinos-se-movilizan-para-rechazar-operaciones-de-emerald-energy-en-caqueta
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/07/Valparaiso-Caquetá_.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Kavilando Org ] 

###### [6 Julio 2016] 

Desde el pasado 28 de junio entre 500 y 200 campesinos se movilizan pacíficamente en la vereda La Curvinata del municipio de Valparaiso, Caquetá, en rechazo a las actividades de perforación de pozos que viene adelantando la petrolera Emerald Energy y su contratista Petroseismic Service, en la zona denominada como bloque El Nogal, **sin la autorización de las comunidades y con presuntas irregularidades,** como no contar con estudios de impacto ambiental.

De acuerdo con la profesora Mercedes Mejía quien hace parte de la 'Mesa Departamental por la Defensa del Agua y del Territorio del Caquetá', desde hace una semana las familias adelantan un plantón en la zona y este martes **uno de los campesinos decidió entrar en huelga de hambre, ante la continuidad de las operaciones, el desacato de la empresa** y la falta de garantías por parte de las autoridades locales y departamentales.

El pasado sábado  un representante de la gobernación, junto con el alcalde del municipio, el personero y un alto mando de las fuerzas militares se reunieron con los campesinos para acordar una comisión de interlocución con la petrolera, para que **cesen las actividades hasta que se cuenten con los estudios que indiquen que se está cumpliendo con el cuidado de las aguas** superficiales y profundas; sin embargo, hasta el momento no se ha cumplido con el acuerdo pactado.

Las comunidades insisten en que el bien general debe primar sobre el interés particular de la empresa, teniendo en cuenta que la Amazonia ha sido declarada como patrimonio ambiental de la humanidad y actualmente es amenazada por la [[posible implementación de 43 bloques petroleros](https://archivo.contagioradio.com/43-bloques-petroleros-en-caqueta-amenazan-la-amazonia-colombiana/).]**Así mismo los líderes campesinos han sufrido todo tipo de ataques y estigmatizaciones por parte de la fuerza pública** por oponerse a los proyectos de la petrolera.

Vea también: [[En Tolima, Quindío y Caquetá el agua se defiende en carnaval](https://archivo.contagioradio.com/tolima-quindio-y-caqueta-realizan-un-carnaval-en-defensa-del-agua/)]

###### [Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)]] 

 
