Title: Corte Constitucional habría aprobado ampliación del Fuero Penal Militar
Date: 2016-02-24 16:23
Category: Judicial, Nacional
Tags: Corte Constitucional, fuero penal militar
Slug: corte-constitucional-falla-a-favor-de-ampliacion-del-fuero-penal-militar
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/07/planton-fuero-penal-2.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Contagio Radio 

###### [24 Feb 2016] 

La sala plena de la Corte Constitucional, habría aprobado la ampliación del Fuero Penal Militar con algunos reparos. El comunicado de la Corte aún se desconoce.

Una de las preocupaciones planteadas en la demanda era el conflicto que se establece entre el Derecho Internacional Humanitario y los Derechos Humanos puesto que los crímenes cometidos por militares serían juzgados a la luz del DIH, sin embargo, Contagio Radio ha logrado establecer que, la decisión acepta que se juzgue a los militares bajo el DIH (como está en el Fuero Penal Militar) pero que también la fuerza pública deberá regirse bajo el marco de los DDHH.

Cabe recodar que de acuerdo con el representante Alirio Uribe, uno de los demandantes, la modificación del Fuero Penal Militar, **“es inconveniente, inconstitucional y un riesgo para los derechos humanos”.**

La demanda además había sido interpuesta por el Colectivo de Abogados José Alvear Restrepo, más de diez organizaciones defensoras de derechos humanos, víctimas, y congresistas que esperaban tumbar el Acto Legislativo 01 de 2015.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u)  o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](https://archivo.contagioradio.com/). 
