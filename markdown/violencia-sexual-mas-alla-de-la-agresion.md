Title: Violencia sexual más allá de la agresión
Date: 2016-03-25 07:00
Category: Carolina, Opinion
Tags: genero, mujeres, Violencia de género
Slug: violencia-sexual-mas-alla-de-la-agresion
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/03/mujeres-colombianas-desnudas-protestando.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Rui Dong 

#### Por [Carolina Garzón Díaz](https://archivo.contagioradio.com/carolina-garzon-diaz/) - [@E\_Vinna](https://twitter.com/E_Vinna) 

###### 25 Mar 2015 

##### *[Dirigida especialmente a la Fiscalía y al Ministerio de Salud]* 

[Hace algunos días se presentó el informe “Acceso a la justicia para mujeres víctimas de violencia sexual en Colombia” un documento muy detallado sobre el estado de los 634 casos que hacen parte de los anexos reservados que la Corte Constitucional le entregó a la Fiscalía mediante los autos 092 de 2008 y 009 de 2015. Con esta remisión la Corte esperaba que la Fiscalía diera prioridad en la investigación, juzgamiento y sanción de estos hechos de violencia sexual ocurridos en el conflicto armado. Pero eso no ha ocurrido.]

**Además del aterrador índice de 97% de impunidad en estos casos, se destacan los obstáculos que enfrentan las mujeres para acceder a la justicia**[, como la ausencia de enfoques diferenciales, la falta de garantías para la denuncia, de acompañamiento jurídico y de claridad en las rutas. Los casos de estas 768 víctimas son reflejo de una verdad abrumadora: la permanente y prolongada re-victimización de las mujeres que han sufrido violencia sexual.]

[En el auditorio donde se presentó el informe se escucharon las voces de Yirley Velazco y Jineth Bedoya, dos mujeres que hacen parte de estos Anexos Reservados y que ahora han decidido darle su rostro a la lucha contra este crimen. Ellas, con mirada valerosa y sin titubear reconocieron un hecho devastador:]**la violencia sexual va más allá de la agresión misma, es vivir cada día con las consecuencias de ese hecho**[. La violencia sexual, al vulnerar la esfera más privada e intima de una persona, hace que su relación con ella misma, con sus seres más cercanos, con la sociedad e incluso con las autoridades sea particular y condicione la atención que debe recibir.]

[¿De qué justicia, medidas de protección y acceso a la salud puede hablar un Estado que no dimensiona con seriedad la atrocidad de este flagelo?]**Las autoridades locales, regionales y nacionales tienen una gran deuda con las mujeres víctimas de violencia sexual**[ ya que no previnieron estos hechos ni les han brindado una atención adecuada e integral luego de ocurridos. Y si la atención no ha sido efectiva, mucho menos la reparación que en estos casos es aún más compleja de construir.]

[Según los relatos de varias mujeres y lo que expone el informe presentado por la Mesa de Seguimiento a los Autos 092 y 009 de la Corte Constitucional,]**tampoco se conocen pasos contundentes hacia la prevención de este crimen.** [Ahora que hablamos con tanta insistencia de las Garantías de No Repetición de los hechos ocurridos en el marco de conflicto armado, así como en aquellos que sirvieron como caldo de cultivo para la violencia en nuestro país, es inevitable tratar este tema.]

[La violencia sexual en el marco del conflicto armado, cometida mayoritariamente por autores indeterminados, seguidos por paramilitares, guerrilla, fuerza pública, BACRIM, grupos armados sin identificar, paramilitares en conjunto con la fuerza pública, y civiles (en ese orden), ha herido profundamente a nuestras mujeres, a nuestras niñas y a nuestra sociedad en su conjunto. Pensarnos y respaldar las medidas para evitar que estos hechos se repitan deben ser prioridad para quienes dialogan en La Habana, así como para quienes desde nuestros respectivos ámbitos participamos de una pedagogía para la paz.]

[Una paz duradera necesita de una reconstrucción de nuestra vida desde lo íntimo y desde lo más privado para que pueda manifestarse en lo público.]**Dignificar, reparar y prevenir**[ con un enfoque integral y de derechos humanos deben ser temas claves cuando hablamos de violencia sexual dentro y también fuera del conflicto armado.]
