Title: Sin Olvido, Gilma Yaneth Pineda Metaute
Date: 2016-10-19 17:10
Category: Sin Olvido
Tags: Antioquia, colombia, crímenes de estado, falsos positivos
Slug: gilma-yaneth-pineda
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/10/Gilma-Yaneth-Pineda.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### Foto: Archivo Familiar 

###### 8 Oct 2016 

Han pasado 20 años del asesinato de [Gilma Yaneth Pineda Metaut](http://sinolvido.justiciaypazcolombia.com/2014/10/gilma-yaneth-pineda-metaute.html)e, por parte de miembros del Estado colombiano en complicidad con grupos paramilitares de Antioquia; una mujer nacida en el seno de una familia campesina, de la que aprendió el valor del trabajo, la dignidad y la tierra, misma de la que aprendió a luchar y exigir sus derechos.

Las amenazas e intimidaciones llevaron a Yaneth junto a su familia a desplazarse, condición en la que tuvo que pasar por muchas tristezas pero también en la que encontraría la mayor bendición de su vida, el nacimiento de su hija Catalina.

Cuando apenas tenía 19 años, poco menos de la edad que tiene su pequeña hoy, fue asesinada junto a 3 personas más, a quienes vistieron e hicieron pasar como guerrilleros del ELN, en un inminente caso de ejecución extrajudicial que tuvo lugar durante la gobernación de Alvaro Uribe Velez.

Aunque no se han logrado comprobar los móviles que condujeron a estos hechos, Catalina sabe que hoy, dos décadas después de su partida, su mamá siempre ha estado con ella, extrañándola cada día sin entender por qué la violencia le arrebató la oportunidad de conocerla, abrazarla y acompañarla en el camino de su vida.

Desde la distancia de este plano terrenal, le envía una carta en la que expresa la tristeza por su ausencia y reafirma que es la motivación para seguir su ejemplo y continuar adelante.

<iframe id="audio_13390821" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_13390821_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>
