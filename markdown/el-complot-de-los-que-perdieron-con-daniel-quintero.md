Title: El complot de los que perdieron con Daniel Quintero
Date: 2020-08-27 13:40
Author: CtgAdm
Category: Columnistas invitados, Opinion
Tags: Alcalde Daniel Quintero, Antioquia, colombia, Medellin, opinion
Slug: el-complot-de-los-que-perdieron-con-daniel-quintero
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/08/adad.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/08/image_content_35006615_20200122213526.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Autor desconocido 

*En la política como en la vida, es natural que haya ganadores y perdedores, esto es precisamente lo que algunos excandidatos a la alcaldía de Medellín no han entendido, y se aferran como náufragos a la remota posibilidad de ocupar el puesto del alcalde Quintero.*

**Por: Giovanni Toro**

La coyuntura política desatada por la decisión de EPM de demandar a los contratistas del proyecto Hidroituango, ya completa 17 días; desde que se conoció la noticia, comenzó la lluvia de opiniones, críticas y señalamientos en contra del alcalde Quintero, las cuales inicialmente parecían provenir de ciudadanas del común, y pretendían hacernos creer a los medellinenses que era la sociedad la que estaba en contra de la decisión del alcalde y que no habían intereses políticos, ni económicos involucrados en esta supuesta iniciativa ciudadana.

Como decían las abuelas: “la verdad escondida aparece” y fue precisamente esto lo que sucedió en un evento convocado por Alejandro Posada en el grupo MedellínPideRevocatoria a las afueras del edificio inteligente de EPM, dicho evento tenía como finalidad notificarle al alcalde Quintero que darían inicio a una campaña de revocatoria en su contra, pero fue allí donde se le “chispotió” un poco más de información.

\[embed\]https://youtu.be/gc3QOgxsBr0\[/embed\]

###### Video en el que Alejandro POSADA, líder del grupo de MedellínPideRevocatoria, manifiesta que estaba reunido en torno al tema con políticos y empresarios {#video-en-el-que-alejandro-posada-líder-del-grupo-de-medellínpiderevocatoria-manifiesta-que-estaba-reunido-en-torno-al-tema-con-políticos-y-empresarios align="JUSTIFY"}

Según lo expuesto por Posada en el video anterior “acababa de sostener una reunión con Alfredo Ramos, Federico Gutiérrez, juan David escobar, juan David Valderrama, y una cantidad de empresarios sobre el apoyo a la iniciativa de revocatoria”.

El apoyo de estos políticos a la iniciativa, puede asociarse a un complot en el que se evidencia las oscuras intenciones y los intereses reales que tienen en la situación, ya que han mostrado que solo buscan arrebatarle a la alcaldía a Quintero para devolverle el poder y la conveniencia a los partidos políticos de siempre y a sus allegados, los grandes empresarios.

<https://www.facebook.com/TotusTVDigital/videos/616943522350276/?vh=e&extid=mvBrdsbhHcalWrzF&d=n>

###### Video Gran Ciudad-Totustv. Con el excandidato a la alcaldía de Medellín, Juan David Valderrama. {#video-gran-ciudad-totustv.-con-el-excandidato-a-la-alcaldía-de-medellín-juan-david-valderrama. align="JUSTIFY"}

En entrevista a Gran ciudad, El excandidato a la alcaldía Juan David Valderrama, desmintió lo dicho por Alejandro Posada y manifestó que, según él, no tiene intenciones de revocatoria y aprovecho y para restarle apoyo público al movimiento, Manifestando que ve mucho más viable una veeduría ciudadana que una revocatoria de mandato, sin embargo, no podemos olvidar que estos políticos han hecho gala de las mejores estrategias y artimañas para manipular la opinión de los ciudadanos, mentir, dividir y sobre todo lo que más experiencia tienen: !confundir!, mostrándonos a los medellinenses un alcalde culpable por defender los intereses de la empresa más importante para los antioqueños, mientras ellos en secreto solo buscan la obtención de beneficios propios.

------------------------------------------------------------------------

###### Las opiniones expresadas en este artículo son de exclusiva responsabilidad del autor y no necesariamente representan la opinión de Contagio Radio.
