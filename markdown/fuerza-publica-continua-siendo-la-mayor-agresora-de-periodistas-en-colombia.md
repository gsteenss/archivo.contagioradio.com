Title: Fuerza Pública es la mayor agresora de periodistas en Colombia
Date: 2016-08-05 12:57
Category: DDHH, Nacional
Tags: Agresiones contra periodistas, Fundación para la Libertad de Prensa, libertad de prensa en colombia, Policía Nacional
Slug: fuerza-publica-continua-siendo-la-mayor-agresora-de-periodistas-en-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/08/Fuerza-pública-periodistas.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Altereddie] 

###### [5 Ago de 2016 ] 

En la noche de este jueves el periodista Víctor Ballestas, junto con el camarógrafo David Romero y el asistente de cámara Jorge Mercado, integrantes del equipo periodístico del Noctámbulo de CityTv, quienes se habían dirigido al centro de la capital para cubrir la ciclovía nocturna, fueron **violentamente agredidos por miembros de la Policía Nacional**.

De acuerdo con la información suministrada por la 'Fundación para la Libertad de Prensa' "Ballestas y Romero notaron un arresto que estaba realizando la Policía y se acercaron para registrarlo. Cuando el uniformado se dio cuenta de la presencia de los reporteros, **golpeó al camarógrafo para impedir que continuara grabando**".

"En ese momento Ballestas le reclamó al oficial por la agresión e inmediatamente fue detenido por los demás uniformados y trasladado a la estación Museo Nacional. Al llegar al lugar los policías detuvieron a Romero, quien seguía grabando la agresión y lo golpearon violentamente entre varios uniformados. Incluso, **uno de ellos utilizó el revolver de dotación para golpear en la cabeza al camarógrafo**".

"Luego de que los oficiales detuvieran a los reporteros de CityTv, los trasladaron a la estación de Santa Fe donde **pretendían judicializar al equipo periodístico**. Finalmente, Ballestas mostró los vídeos que varios ciudadanos le habían enviado a su celular, los oficiales ofrecieron disculpas y los dejaron en libertad".

Este viernes en un comunicado la Policía Nacional anunció que abrirá una investigación disciplinaria contra los uniformados responsables de las agresiones y se comprometió a ofrecer resultados en los próximos días. Desde la Fundación se considera que existe sufiente material probatorio para determinar la responsabilidad de los miembros de la institución y por tanto **exige la aplicación de sanciones drásticas inmediatas que impidan la repetición de estos hechos**.

De acuerdo con Pedro Vaca, director de la Fundación, la fuerza pública continúa siendo la mayor agresora de periodistas en Colombia, prefiere trabajar sin presencia de cámaras y eso sólo ocurre en regímenes autoritarios, no en la democracia. En 2014 la fuerza pública ocupo el primer lugar como principal agresor, [[en 2015 el tercer lugar](https://archivo.contagioradio.com/en-2015-fueron-asesinados-39-periodistas-segun-la-cidh/)] y **en 2016 podría ocupar alguno de los dos primeros lugares de continuar con la tendencia**.

<iframe src="http://co.ivoox.com/es/player_ej_12453311_2_1.html?data=kpehl5iXdZKhhpywj5WaaZS1kpWah5yncZOhhpywj5WRaZi3jpWah5yncbHZxdfcjbvFp8KZk6iYqLGtlI6ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)]]
