Title: Obligaciones del Estado colombiano para ser coherente con la paz
Date: 2015-10-13 16:21
Category: Nacional, Paz
Tags: Comisión de Justicia y Paz, Conversaciones de paz de la habana, Fundación Comite de Solidaridad con Presos Políticos, MOVICE, Paramilitarismo, víctimas
Slug: estas-son-las-obligaciones-del-estado-colombiano-para-ser-coherente-con-la-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/05/fo1-mundo.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: laportadacanada 

###### [13 Oct 2015]

El 14 de Octubre, en la Casa España se presentaron 3 informes y un libro en el que organizaciones de Derechos Humanos hacen un **inventario en torno a las obligaciones del Estado colombiano si realmente está comprometido con la paz.** Tanto la Fundación Comité de Solidaridad con Presos Políticos como el Movimiento de Víctimas de Crímenes de Estado y la Comisión de Justicia y Paz recogen experiencias y expectativas de las víctimas.

El informe “***Protección de las Víctimas de Crímenes de Estado Como un Imperativo Para la Paz***” recoge la experiencia de más de 200 organizaciones de DDHH en todo el territorio nacional y da cuenta de las 4.075 violaciones a los DDHH, la ineficacia de los mecanismos de protección de la UNP y la necesidad de desmontar las estructuras criminales que funcionan y que propician estas agresiones.

El segundo informe titulado “***Estructuras Criminales al Interior del Estado Colombiano***” en el que las organizaciones resaltan como indispensable la necesidad de desmantelar las estructura paramilitares enquistadas en las ramas del poder público “*resulta indispensable hacia la construcción de la nueva nación en paz la depuración de las tres ramas del poder público y todas las instancias gubernamentales*”

Así mismo se presentará el libro: “***El desmantelamiento del Paramilitarismo: Aprendizajes y Recomendaciones desde las Víctimas***” que recoge recomendaciones desde las propias víctimas y un detallado informe en cuanto a la “La Extradición: Aprendizajes y recomendaciones desde las víctimas.” Que según las organizaciones provoca serias violaciones a los DDHH y a la soberanía nacional.

Video del evento

<iframe style="border: 0; outline: 0;" src="http://cdn.livestream.com/embed/contagioradioytv?layout=4&amp;clip=pla_329954c0-68b2-4e84-9da8-42991b40c8b6&amp;height=340&amp;width=560&amp;autoplay=false" width="560" height="340" frameborder="0" scrolling="no"></iframe>

<div style="font-size: 11px; padding-top: 10px; text-align: center; width: 560px;">

Watch [live streaming video](http://original.livestream.com/?utm_source=lsplayer&utm_medium=embed&utm_campaign=footerlinks "live streaming video") from [contagioradioytv](http://original.livestream.com/contagioradioytv?utm_source=lsplayer&utm_medium=embed&utm_campaign=footerlinks "Watch contagioradioytv at livestream.com") at livestream.com

</div>
