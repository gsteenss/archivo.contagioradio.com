Title: Amenazan con "aislamiento" a presos de las FARC en huelga de hambre
Date: 2017-07-04 12:27
Category: DDHH, Entrevistas
Tags: Acuerdos de La Habana, FARC, Ley de Amnistia, presos politicos
Slug: amenazan-con-aislamiento-a-presos-de-las-farc-en-huelga-de-hambre
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/07/DDgD2JpXsAAX9BC-e1499189639249.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Marcha Patriótica] 

###### [04 Jul 2017] 

Se completan ya 9 días de huelga de hambre por parte de integrantes de las FARC en las prisiones colombianas y **no hay asomo de cumplimiento de la ley 1820 que ordena amnistías** y traslados a las Zonas Veredales a integrantes de esa guerrilla que ya completó el proceso de dejación de armas el pasado 27 de Junio

Además el anuncio de la salida de las cárceles de 320 presos políticos de las FARC por parte del Gobierno Nacional fue **desmentido por integrantes del Estado mayor de las FARC**. La huelga de hambre de más de 1400 presos políticos completa el noveno día y según ellos, continuará hasta que se cumpla lo pactado en la Habana con relación a la ley de amnistía e indulto.

Según Jesús Santrich, **“la información de que han sido liberados 320 presos políticos es falso. Esto sólo es una promesa del Gobierno como muchas otras”**. Afirmó además que “la otra semana sólo van a liberar a 15 personas y 40 más van a ser trasladados a la Zona Veredal de Mesetas en el Meta porque ya estaban en proceso de traslado”. (Le puede interesar:["Farc pide a organizaciones internacionales trabajar por libertad de presos políticos"](https://archivo.contagioradio.com/42965/))

### **La ley 1820 es la más reglamentada de los procesos paz** 

El gobierno presentó la ley que fue aprobada en el congreso en Diciembre y según Santirch **esta era una de las más completas discusiones que se llevó a cabo en la Habana** en el punto de "reincorporación". Sin embargo, los jueces y fiscales no la aplicaron aduciendo algunas dudas sobre el procedimiento que se disiparon con el decreto 227 de 2017. Sin embargo, aún así, todavía faltan por ser amnistiados 2577 presos políticos.

Sumado a esto, Santrich manifestó que el acuerdo sobre amnistía e indultó tiene como **fecha límite de salida de los detenidos el primero de enero de este año**. Con esto en mente, Santrich desmintió la afirmación que hacen algunos medios de información y manifestó que “la fecha de salida propuesta en el acuerdo era enero y no el primero de agosto como dicen algunos”. (Le puede interesar: ["Más de 1400 presos políticos continúan en huelga de hambre"](https://archivo.contagioradio.com/presos-politicos-de-las-farc-mantienen-la-huelga-de-hambre/))

De los 2.577 presos políticos que se encuentran detenidos en diferentes cáceles del país, **1713 continúan en desobediencia civil y  99 hombres y mujeres que se han unido a los 1400 que se encuentran en huelga de hambre**. Santrich afirmó que “los presos han sido amenazados en las cárceles con que serán aislados y 120 de ellos son adultos mayores”.

**Protocolos de atención a la huelga de hambre de la Cruz Roja no se han implementado**

Además de la poca atención en salud que reciben los detenidos que se encuentran en huelga de hambre, Santrich manifestó que **“no se han iniciado los protocolos de huelga de hambre de la Cruz Roja”**. Estos protocolos tienen como finalidad prestar atención médica en los centros de reclusión para afrontar estas situaciones otorgando sales rehidratantes, camillas, balanzas y otro tipo de medicinas. (Le puede interesar: ["Presos políticos de las Farc exigen que se cumpla la ley de amnistía pactada"](https://archivo.contagioradio.com/presos-politicos-de-las-farc-exigen-que-se-cumpla-la-ley-de-amnistia-de-los-acuerdos/))

Los miembros de las FARC están a la expectativa de las decisiones que tome el Gobierno. Ellos han manifestado que “que la ley 1820 de 2016 referente a la amnistía de los presos políticos es muy clara y ha habido negligencia por parte de los jueces de ejecución de penas que no han actuado en derecho”. Santrich recordó que **“la huelga de hambre no es una súplica ni un capricho** sino que se hace para exigir que se cumpla un acuerdo de Estado que se hizo en la Habana”.

<iframe id="audio_19621565" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_19621565_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU).
