Title: Hay que garantizar la vida de quienes ayudan en la construcción de la paz: Eurodiputados
Date: 2016-11-18 17:15
Category: Nacional, Paz
Slug: hay-que-garantizar-la-vida-de-quienes-ayudan-a-la-construccion-de-la-paz-eurodiputados
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/eurodiputados-paz.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: elconfidencial] 

###### [18 Nov 2016]

En una comunicación dirigida al presidente Santos, 40 eurodiputados y diputadas de diversas corrientes políticas respaldaron el nuevo acuerdo de paz con las FARC y pidieron que se refrende con celeridad, **manteniendo a las víctimas en el centro del acuerdo.** Además pidieron que se inicie el proceso con el ELN y expresaron su preocupación por la situación de derechos humanos en el país.

Los y las integrantes del parlamento europeo hicieron especial énfasis **en la situación de  riesgo que afrontan las personas que han apoyado la construcción de la paz,** con las FARC o con el ELN, “Reiteramos la necesidad de garantizar, con atención especial, la integridad física y psicológica de todas las personas defensoras de derechos humanos, incluyendo las que están involucradas en la construcción de paz, cuyo nivel de riesgo y agresiones en su contra ha aumentado debido a su labor fundamental”

Los **eurodiputados reiteraron que las víctimas deben seguir siendo el centro del acuerdo y de su refrendación, pero también se debe garantizar su protección** frente a los nuevos escenarios que se comienzan a transitar “En particular, llamamos la atención frente a la situación de riesgo de las comunidades rurales más afectadas por el conflicto” para quienes pidieron protección especial.

Los firmantes de la carta también insistieron en que para garantizar la seguridad de los defensores y defensoras de DDHH deben **ponerse en marcha los mecanismos para el combate o el desmonte del paramilitarismo**, como una de las principales y urgentes tareas para la implementación de los acuerdos.

A continuación el texto completo...

###### "Estimado Sr. Presidente Juan Manuel Santos,  
Las y los Eurodiputados abajo firmantes saludamos el nuevo acuerdo entre el gobierno de Colombia y las FARC – EP anunciado el 12 de noviembre. Reiteramos nuestro apoyo a dicho proceso y alentamos para que se logre una puesta en marcha rápida del mismo hacia la construcción de una Paz duradera y sostenible, siempre con un enfoque a las víctimas como centro de este proceso.  
El 10 de octubre de 2016, el gobierno de Colombia y el Ejército de Liberación Nacional (ELN) anunciaron la instalación de una mesa pública de conversaciones prevista para el 27 de octubre de 2016 en Quito, Ecuador, y, posteriormente, suspendida. Invitamos a las partes a encontrar rápidamente un acuerdo para poder avanzar en este proceso, el cual representa un paso fundamental hacía la construcción de la paz y de un país donde primen los derechos humanos y la justicia social.  
Asimismo, le felicitamos por el reconocimiento del Premio Nobel de la Paz y apreciamos su declaración de compartir este premio con todas las víctimas del conflicto armado colombiano.  
Como miembros del Parlamento Europeo expresamos preocupación por los efectos de seguridad que pueda tener el actual contexto para la sociedad civil colombiana, considerando los retos que hay en estos primeros momentos de transición hacia la paz. En particular, llamamos la atención frente a la situación de riesgo de las comunidades rurales más afectadas por el conflicto y los grupos más vulnerables, como niñas y mujeres, comunidades afro-colombianas, campesinas y pueblos indígenas, para los cuales pedimos garantías de protección.  
Reiteramos la necesidad de garantizar, con atención especial, la integridad física y psicológica de todas las personas defensoras de derechos humanos, incluyendo las que están involucradas en la construcción de paz (tanto con las FARC como con el ELN), cuyo nivel de riesgo y agresiones en su contra ha aumentado debido a su labor fundamental. Instamos a que se implementen cuanto antes las medidas previstas en el acuerdo con las FARC en materia de desmantelamiento de todas las estructuras paramilitares que siguen constituyendo una amenaza para diferentes sectores de la sociedad civil y, en especial, para las personas defensoras de los derechos humanos.  
Finalmente, subrayamos el rol fundamental de monitoreo de la situación de derechos humanos que realiza la Oficina del Alto Comisionado de las Naciones Unidas para los Derechos Humanos en Colombia (OACNUDH), en particular en la implementación de los acuerdos de paz, y saludamos la renovación de su mandato.  
Un cordial saludo,  
1. Martina Anderson - Irlanda – GUE/NGL  
2. Ulrike Lunacek (Vicepresidenta del PE, Austria, parte de delegación del PE de apoyo al referéndum del 2 de Octubre, Greens/EFA)  
3. Jude Kirton Darling - Reino Unido – S&D  
4. Javier Nart - España - ALDE  
5. Javier Couso - España – GUE/NGL  
6. Josep María Terricabras – España - Greens / EFA  
7. Karoline Graswander-Hainz - Austria – S&D  
8. Ignazio Corrao – Italia - EFDD  
9. Matt Carthy - Irlanda – GUE/NGL  
10. Bodil Valero – Suecia - Greens / EFA  
11. Nessa Childers – Irlanda - S&D  
12. Liadh Ni Riada - Irlanda – GUE/NGL  
13. Bart Staes – Bélgica - Greens / EFA  
14. Javi López – España - S&D  
15. Lynn Boylan - Irlanda – GUE/NGL  
16. Ernest Urtásun – España - Greens / EFA  
17. Josu Juaristi – España - GUE/NGL  
18. Monika Vana – Austria - Greens / EFA  
19. Marina Albiol – España – GUE/NGL  
20. Molly Scott-Cato – Reino Unido - Greens / EFA  
21. Marisa Matias – Portugal – GUE/NGL  
22. Martin Haeusling – Alemania - Greens / EFA  
23. Kostadinka Kuneva – Grecia – GUE/NGL  
24. Tania González – España – GUE/NGL  
25.Helmut Scholz – Alemania – GUE/NGL  
26. Merja Kyllönen – Finlandia – GUE/NGL  
27.Miguel Viegas – Portugal – GUE/NGL  
28. Malin Björk – Suecia – GUE/NGL  
29. Eleonora Forenza – Italia – GUE/NGL  
30.Lola Sánchez – España – GUE/NGL  
31.Martina Michels – Alemania – GUE/NGL  
32. Stelios Kouloglou – Grecia – GUE/NGL  
33. Xabier Benito – España – GUE/NGL  
34. Miguel Urbán – España – GUE/NGL  
35. Dimitrios Papadimoulis –Grecia – GUE/NGL  
36. João Ferreira – Portugal – GUE/NGL  
37. Estefanía Torres – España – GUE/NGL  
38. João Pimenta Lopes – Portugal – GUE/NGL  
39. Jean-Luc Mélenchon – Francia – GUE/NGL" 

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)
