Title: En disturbios terminó movilización por asesinato de Berta Cáceres
Date: 2016-10-21 13:18
Category: El mundo, Otra Mirada
Tags: Agua Zarca, Berta Cáceres, Copihn, honduras, Movilizaciones
Slug: disturbios-termino-movilizacion-asesinato-berta-caceres
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/10/Marcha-Berta-Caceres.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Prensa Libre 

###### [21 Oct 2016] 

Integrantes del **Consejo Cívico de Organizaciones Populares e Indígenas de Honduras (Copinh)**, salieron a las calles de Tegucigalpa para exigir **se agilicen los procesos jurídicos por el [asesinato de la líder ambientalista Berta Cáceres](https://archivo.contagioradio.com/el-viaje-de-berta/)**, ocurrido el pasado 3 de marzo.

Entre las consignas presentadas durante la manifestación popular pacífica, esta la de **hacer público el nombre de los autores intelectuales del crimen** y l**a cancelación del proyecto hidroeléctrico Agua zarca**, contra el que lucho en vida la activista por los daños que causan al ambiente y a las comunidades ancestrales en el occidente del país.

Tomás Gómez, coordinador del Copinh, aseguró que **en las últimas semanas se ha desatado “una campaña de desprestigio” contra la organización indígena**, de la que Cáceres fue su coordinadora general, por parte de la empresa que desarrolla el proyecto y el Estado hondureño. Lea también [Continuan agresiones contra la vida de miembros del Copihn en Honduras](https://archivo.contagioradio.com/continuan-agresiones-contra-la-vida-de-miembros-del-copinh-en-honduras/).

La manifestación que recorrió diferentes calles de la ciudad, se plantó frente a la Fiscalía, esperando que las autoridades se reunieran con los familiares de Berta, allí **se les sumaron estudiantes de la Universidad Nacional Autónoma de Honduras (UNAH)**, que según reportes de medios locales **se habrían afrentado "a piedra" con las autoridades policiales**.

Otros reportes de prensa señalan que se trataría de **"estudiantes infiltrados"** quienes degeneraron la marcha, pintando los muros de edificios públicos y agrediendo a los periodístas que cubrían la movilización. En 15 minutos los uniformados dispersaron a los manifestantes utilizando gas lacrimógeno. Lea también: [Gobierno de Honduras quiere desviar investigación del asesinato de Berta Cáceres](https://archivo.contagioradio.com/gobierno-de-honduras-quiere-desviar-investigacion-del-asesinato-de-berta-caceres/).
