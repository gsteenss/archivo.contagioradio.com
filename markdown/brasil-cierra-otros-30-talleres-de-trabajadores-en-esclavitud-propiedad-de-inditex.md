Title: Brasil cierra otros 30 talleres de trabajadores en esclavitud propiedad de INDITEX
Date: 2015-03-05 20:16
Author: CtgAdm
Category: El mundo, Otra Mirada
Tags: INDITEX esclaviza a trabajadores en Brasil, ZARA esclaviza en Brasil, ZARA esclaviza en Marruecos
Slug: brasil-cierra-otros-30-talleres-de-trabajadores-en-esclavitud-propiedad-de-inditex
Status: published

###### Foto:Franklinstudents.com 

El **gobierno brasileño** desarticula **30 talleres** de fabricación **de ropa**, donde los trabajadores estaban en una situación de **esclavitud** y precariedad total. La fiscalía relaciona los talleres con la multinacional **INDITEX, a la que pertenece la cadena de ropa española ZARA**.

Juliana Cassiano, fiscal del ministerio de trabajo aseguró que la mayoría de los trabajadores liberados eran peruanos y bolivianos que habían entrado en el país ilegalmente a través de una red de trata de personas; con lo que además de la situación de esclavitud habían contraído grandes deudas con las mafias que los habían introducido.

Las condiciones de los trabajadores eran infrahumanas con **jornadas laborales de 16 horas, sin baño, viviendo en las propias fabricas, con un salario de 90 euros al mes**.

**INDITEX responsabilizó** a una de **sus filiales, en concreto AHA** que trabaja en Brasil y otros países de Suramérica. La multinacional declaró que no ha podido realizar seguimiento a las empresas subcontratadas que surten de ropa a ZARA.

A lo que ha respondido el gobierno, que si **ellos han logrado hacerlo, más fácil tendría que haber sido para la multinacional que es quien subcontrata**. En palabras de Cassiano *"...Y si Inditex es capaz de controlar la calidad de sus productos durante todo el proceso de producción, ¿por qué no hace lo mismo con la mano de obra que emplea?...".*

La multinacional dueña del magnate español Amancio Ortega, que posee la tercera fortuna a nivel mundial, y que ha sido **denunciada en tres ocasiones** por la organización de DDHH y antitrata en Latinoamerica, **Fundación Alameda**, por casos similares en Argentina, Perú y Bolivia.

Después de las denuncias de los activistas, varios de los **miembros de la organización han sido amenazados de muerte.**

ZARA ha sido denunciada en todos los países donde opera, como Marruecos, que es el caso donde ha habido mas denuncias por maquilas y trabajo forzado en régimen de esclavitud.

El modus operandi de la transnacional consiste en **terciarizar su producción, subcontratando empresas en otros países y creando un entramado empresarial que complica a las autoridades relacionar a INDITEX**, con las empresas que están detrás de la flagrante **violación de DDHH contra los trabajadores**.
