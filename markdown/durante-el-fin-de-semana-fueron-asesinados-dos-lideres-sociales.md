Title: Durante el fin de semana fueron asesinados dos líderes sociales
Date: 2018-04-02 10:59
Category: DDHH, Nacional
Tags: asesinato de líderes sociales, belisario benavides, defensores de ddhh, lideres sociales, mapiripan, maria magdalena cruz rojas, Meta, Popayán
Slug: durante-el-fin-de-semana-fueron-asesinados-dos-lideres-sociales
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/04/lideres-asesinados-e1522683733671.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:  Twitter Athemay Sterling CPDH] 

###### [02 Abr 2018] 

No se detienen los asesinatos contra líderes sociales en el país. Durante la semana santa fueron asesinados **Belisario Benavides Ordóñez**, delegado de las víctimas del municipio de Rosas en Popayán y **María Magdalena Cruz Rojas**, lideresa comunitaria de Mapiripán en Meta. En el 2017, cada tres días fue asesinado un líder social en el país.

De acuerdo con la Comisión Intereclesial de Justicia y Paz, el asesinato de la lideresa ocurrió el viernes 30 de marzo a las 8:00 pm en el caserío El Rincón del Indio en el municipio de **Mapiripán**. De acuerdo con algunos pobladores, habrían sido integrantes de las disidencias de las FARC quienes la asesinaron.

Dichos hombres, “llegaron a la vivienda de la lideresa y **le dispararon en su cabeza** frente a toda su familia”. Luego, el cuerpo sin vida fue trasladado a la ciudad de Villavicencio donde se espera que se adelanten las investigaciones pertinentes. (Le puede interesar:["Asesinan a dos líderes sociales en Antioquia"](https://archivo.contagioradio.com/asesinan-a-dos-lideres-sociales-en-antioquia/))

### **La lideresa hacía parte del programa de sustitución de cultivos de uso ilícito** 

De acuerdo con la organización, Cruz Rojas, hacía parte del programa de sustitución de cultivos de uso ilícito resultado del Acuerdo de Paz entre la guerrilla de las FARC y el Gobierno Nacional. Ella venía trabajando **por el esclarecimiento de la propiedad de tierras** en un territorio disputado por hechos violentos durante la guerra.

A este hecho, se suma el asesinato del líder social Belisario Benavides Ordóñez de 35 años. El defensor de derechos humanos **representaba a las víctimas del conflicto armado** ante el Comité Territorial de Justicia Transicional del municipio de Rosas al sur de Popayán en el Cauca. (Le puede interesar:["Enero un mes trágico para líderes y reclamantes de tierras"](https://archivo.contagioradio.com/enero_asesinatos_lideres_sociales/))

De acuerdo con el informe de la Oficina de Derechos Humanos de las Naciones Unidas, en 2017 se registraron **441 ataques contra los defensores de derechos humanos** y hubo 121 asesinatos de los cuales 81 eran líderes sociales. Esto responde a que hay “ausencia integral del Estado, corrupción, presencia de grupos armados, ausencia de educación, salud y trabajo, presencia de economías ilícitas, violencia y pobreza”.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 

<div class="osd-sms-wrapper">

</div>
