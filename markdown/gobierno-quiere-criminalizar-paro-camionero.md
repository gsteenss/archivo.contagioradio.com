Title: Gobierno criminaliza el paro camionero: ACC
Date: 2015-03-18 19:36
Author: CtgAdm
Category: Movilización, Nacional
Tags: ACC, Armando Benedetti, Asociación de Colombiana de Camioneros, Camioneros, gina parody, IMPALA, Magdalena, Paro camionero, Protesta social, Santos
Slug: gobierno-quiere-criminalizar-paro-camionero
Status: published

##### Foto: Diario ADN 

<iframe src="http://www.ivoox.com/player_ek_4232737_2_1.html?data=lZeglJyXe46ZmKialJiJd6KomZKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRi9DWysrf0NSPtdbdxtfSjcjWrc7dz8bZy9%2FFtozZzZDdw9fTb8TVzs7c0MrWs46ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Heyner Galviz, Asociación de Colombiana de Camioneros (ACC) de Magadalena] 

Esta semana la Fiscalía anunció **la captura de 36 camioneros, que ya están en proceso de judicialización**, debido a los “disturbios ocasionados por el paro camionero” según se informa. Sin embargo, el presidente de la Asociación de Colombiana de Camioneros (ACC) de Magadalena, Heyner Galviz, asegura se trata de un paro pacífico, en cambio, **"están reprimiendo a la gente que pide una solución a su problemática".**

**“Nunca ha sido secreto que las autoridades infiltran la protesta,** hay pruebas de que ellos son quienes ocasionan los actos vandálicos y criminalizan las manifestaciones sociales”, denuncia Galviz, quien añade que la ministra de transporte y el presidente  le incumplieron a los camioneros  la semana pasada, pues ya se había llegado a un acuerdo respecto a los costos de operación, pero al día siguiente, el gobierno cambió las condiciones del arreglo, y por lo tanto, no se logró ningún convenio. “**Nos creen ignorantes”,** expresó el Presidente de la ACC de Magdalena.

“**Nuestros compañeros quisieron caminar por las vías, pero los golpean y los insultan, para eso tienen al ESMAD**”, afirma el camionero, quien agrega que fueron inventados los delitos para detener a los líderes del gremio, y **ahora los amenazan con expropiarles los camiones.**  Es por eso, que los transportadores de carga, han decidido que hasta que no se aclaren las detenciones,  no van a dialogar y seguirán en paro.

Según Galviz, el gobierno no ha sido honesto con la opinión pública, y quiere sacar a los camioneros tradicionales, dándoles 15 años de vida útil a lo camiones. Además, el gremio denuncia que la empresa **IMPALA, ingresará a Colombia sin tener que pagar los 350 mil millones de pesos que le corresponde por cada uno de los cupos de los 5.000 camiones** que entrarán al país, en cambio a los camioneros tradicionales ya se les hizo pagar esa póliza.

De acuerdo al camionero de Magdalena, es la ministra **Gina Parody y el senador Armando Benedetti, quienes han hecho lobby**  para que esta multinacional llegue a la nación y así, se pueda acabar con el gremio tradicional de camioneros.

Finamente, agrega que los colombianos y las colombianas, no deben pensar que el desabastecimiento y el aumento en el costo de los alimentos es culpa de los camineros, ya que **se trata de una responsabilidad del Gobierno Santos,** que no ha querido darle una solución a las exigencias del gremio.
