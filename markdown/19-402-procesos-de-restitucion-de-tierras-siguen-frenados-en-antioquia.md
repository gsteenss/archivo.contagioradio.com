Title: 19.402 procesos de restitución de tierras siguen frenados en Antioquia
Date: 2020-09-03 19:03
Author: AdminContagio
Category: Actualidad, DDHH
Tags: Antioquia, Desplazamiento forzado, despojo de tierras, Restitución de tierras
Slug: 19-402-procesos-de-restitucion-de-tierras-siguen-frenados-en-antioquia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/09/Antioquia.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: subregión Urabá entre Antioquia, Córdoba, Chocó/ Forjando Futuros

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

A propósito de la entrega a la Comisión de la Verdad del sistema de información recopilado por Forjando Futuros, organización que ha logrado recopilar 470.000 datos a partir del análisis del despojo, el desplazamiento y la restitución de tierras, se ha podido concluir que los mayores responsables del fenómeno del despojo en Colombia han sido los empresarios antioqueños **vinculados a compañías extractivistas, mineras y petroleras.**

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

El cruce de información, entregado a la Comisión de la Verdad, según Gerardo Vega, director de [Forjando Futuros](https://www.forjandofuturos.org/), ha permitido identificar no solo el nivel de participación de empresas sino quiénes fueron las personas despojadas. De igual forma se logró determinar que Antioquia es el departamento con el mayor número de personas que esperan les sean restituidas sus tierras con 21.272 casos, seguida de los departamentos del César con 7.779 casos y Nariño con 7.109, cabe señalar que de los casos registrados en Antioquia, solo 1674 han sido resueltos.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

“Hay diferencias en cada región, pero también hay constantes. Una constante es que siempre pierden los campesinos y otra constante es que siempre hay intereses económicos detrás del despojo”, agregó el investigador, Ilhan Can. [(Lea también: En Turbo, campesinos están siendo obligados a entregar el 50% de las tierras que habitan y trabajan)](https://archivo.contagioradio.com/en-turbo-campesinos-estan-siendo-obligados-a-entregar-el-50-de-las-tierras-que-habitan-y-trabajan/)

<!-- /wp:paragraph -->

<!-- wp:core-embed/youtube {"url":"https://www.youtube.com/watch?v=ce4gW_5foRo","type":"video","providerNameSlug":"youtube","className":"wp-embed-aspect-16-9 wp-has-aspect-ratio"} -->

<figure class="wp-block-embed-youtube wp-block-embed is-type-video is-provider-youtube wp-embed-aspect-16-9 wp-has-aspect-ratio">
<div class="wp-block-embed__wrapper">

https://www.youtube.com/watch?v=ce4gW\_5foRo

</div>

</figure>
<!-- /wp:core-embed/youtube -->

<!-- wp:paragraph {"align":"justify"} -->

Con el apoyo de diversas organizaciones como la Comisión Intereclesial de justicia y Paz, la Corporación Jurídica Libertad y el Instituto Popular de Capacitación también se determinó que existen 66 empresas condenadas a la restitución y que se encuentran divididas en tres grupos. Uno del sector agroindustrial, otras que corresponden a empresas extractivistas y otro grupo que son los bancos incluidas entidades como **Davivienda, BBVA, Bancolombia y Banco Agrario.**

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Entre las empresas vinculadas al despojo de tierras se encuentran Argos, con 14 sentencias, además de empresas bananeras asociadas a través de Augura como Uniban, Banacol, Bananeras del Urabá, por cierto asociadas a la construcción del puerto de Urabá y otras del sector extractivista como **Anglo Gold Ashanti, Continental Gold y Ecopetrol**. Vega resalta que hay otros grupos de empresarios que están en trámites de restitución, como los Hernández de la Cuesta, propietarios de el periódico El Colombiano.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

También se pudo establecer patrones ligados al auge del paramilitarismo a mediados de la década de los 90, su expansión y llegada a otras zonas del país como la región Andina y el Bajo Cauca antioqueño.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Empresarios de Antioquia los mayores responsables

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Dichos datos, son el resultado de años de investigación, **soportados en más de 5.775 sentencias proferidas por jueces en todo el territorio nacional,** que además han permitido determinar que son 25 las empresas antioqueñas o con registro en la Cámara de Comercio de Medellín las que han sido condenadas a devolver tierras a víctimas del despojo en una extensión de 115.000 hectáreas.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

**"Esta matriz nos permite identificar dinámicas y patrones de conductas que tuvieron empresarios que aparecen en estas sentencias y que aprovecharon el conflicto armado y utilizarlo para sus propios intereses",** señala la comisionada de la verdad, Alejandra Miller. [(Lea también: 66 empresas, entre ellas 4 bancos, tendrán responderle a reclamantes de tierras)](https://archivo.contagioradio.com/66-empresas-entre-ellas-4-bancos-tendran-responderle-a-reclamantes-de-tierras/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

El director de la fundación expresó que esta base de datos espera sirva para que su resultado quede evidenciado en las recomendaciones que deberá hacer la Comisión de la Verdad **"para que este hecho no vuelve a suceder y que el despojo y desplazamiento no sigan siendo un comportamiento normal y repetitivo en Colombia".**

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
