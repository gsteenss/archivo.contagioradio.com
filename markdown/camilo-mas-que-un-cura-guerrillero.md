Title: Camilo, más que un cura guerrillero
Date: 2016-08-21 10:00
Author: AdminContagio
Category: 24 Cuadros, Paz
Tags: camilo torres, Documental Camilo Torres
Slug: camilo-mas-que-un-cura-guerrillero
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/08/Camilo-Torres.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### Fotograma Documental 

##### [19 Agos 2016] 

En más de veinte lugares de Colombia se  presentará por primera vez "**Camilo, más que un cura Guerrillero**", un proyecto audiovisual que busca desmitificar la imagen del sacerdote **Camilo Torres Restrepo**, distorsionada en el transcurso de los cincuenta años que han pasado de su muerte.

Este 26 de agosto, en universidades, cine foros barriales, bibliotecas populares del país y en en el centro Cultural Olga Vasquez de Argentina, se proyectará el documental que aborda otras facetas y relatos al rededor de Camilo, que ayuden desde el reconocimiento del sujeto a construir la memoria colectiva.

La producción de Agip Pro y Periferia Prensa Alternativa, es dirigida por **David Guevara**, realizador bogotano que busca desde su visión de  “la generación de la guerra” aportar herramientas para construir colectivamente la generación de la paz. Para comprender mejor la propuesta, Andrés Gamboa habló con su director.

**¿Que significa hablar hoy de Camilo Torres en Colombia?**

Para nosotros que nos sumergimos en este ejercicio audiovisual fue bastante sorprendente en cuanto la visión que se tiene de Camilo hoy día. Es decir nos encontramos con la imagen de un ser humano que fue a contra corriente con su clase social, que se vinculó con la academia para tener las bases teóricas y entender un poco los problemas coyunturales del país, y que aun así fue más allá de los libros y vivió realmente lo que era la pobreza en Colombia.

Décadas atrás, nombrar a Camilo era casi un acto subversivo y era precisamente ante el auge de masas que tenía el pueblo colombiano en sus luchas, el estado tomo una estrategia en la cual relacionar el Camilismo con el ELN, y en ese sentido desarrollar toda una estrategia de represión muy significativo que llevó pues a la eliminación completa de movimientos que se reivindicaban como camilistas no solo “a luchar”, también recordemos la goiconda que llevo a que varios curas, se exiliaran, acabaran muertos o tomaran otros caminos.

Entonces hablar hoy de él 50 años después es porque el país esta en otra situación, en un estado en el cual necesita reencontrarse con su pasado, reencontrarse con lo que originó el conflicto, para poder entenderlo y que esa es la gran significación que tiene Camilo Torres Restrepo, con eso que convocó a la guerra pero que también daba posibilidades de evitarla.

**Camilo torres Restrepo es un Icono en la lucha social y armada en Colombia de los años 60, ¿Cómo logra transcender esta figura a una época como la de hoy en la cual están en pleno desarrollo los diálogos de paz (entre las FARC y el Gobierno)**

La figura de Camilo trasciende hoy día, a través de su corriente de pensamiento que se expresa en el movimiento social y político que lucha por los cambios estructurales del país. Además su mensaje de unidad es fundamental bajo las dinámicas en la que nos estamos moviendo, ya que se asoman momentos de grandes cambios y de gran agite social, en el que se es necesario la unidad del pueblo colombiano. Por otro lado, Camilo entendía la paz, si se realizaban verdaderas transformaciones en cuanto al modelo de miseria y desigualdad que se había implantado en el país.

**¿Este momento histórico en el que nos desenvolvemos ha dado la posibilidad de que existan otras voces sobre el conflicto? O por el contrario ¿han sido parcializadas? ¿Es necesario incentivar estas voces y estas producciones de las generaciones que hemos vivido en el conflicto?**

El relato del conflicto siempre ha estado mediado por el poder, siempre ha estado en defensa de los intereses de clase. Otros puntos de vista pueden tener oportunidad de construir el relato nacional sobre el conflicto que hemos vivido, pero esto pasa no solamente porque haya producciones ¿cierto?, sino porque el país en su conjunto, y sobre todo los movimientos sociales, hoy se acerquen y entiendan la importancia de estos relatos.

**Ya que el relato de Camilo Torres se relaciona con la insurgencia, es complejo entablar estas otras perspectivas ¿Cuándo surge esta idea y en qué momento comienza a consolidarse**?

Yo creo que sí, hablar de Camilo Torres es relacionado con la insurgencia, pero él tuvo más de 37 años, de los cuales sólo 4 meses duró con la insurgencia. Ahora no quiero minimizar esta etapa de su vida porque fue muy importante para él y para el país. Lo que quiero decir es que Camilo tenía otras facetas mucho más desarrolladas que su papel como guerrillero, de hecho en su primer combate es que cae, no desarrolla o el no alcanza a desarrollar todo lo que él quiso.

Cuentan que él le planteó al estado mayor de esa organización que quería graduarse como guerrillero en ese combate, pero que su sueño realmente era irse a los llanos orientales a seguir el camino de bolívar y de Guadalupe salcedo de organizar a los llaneros para la revolución. Entonces fue muy truncado esa etapa insurgente.

Por lo tanto otras facetas de su vida estuvieron muy desarrolladas como la académica, el fue precursor de la sociología en Latinoamérica. Además fue un referente político inigualable tratando de organizar un movimiento social de la nada, porque él organiza ese frente unido con unos campesinos recién llegados a la ciudad y el trató de organizarlos en una plataforma política junto a la izquierda en un momento en el cual estaba más dividida que nunca. Es decir muchas veces no se han analizado estas otras miradas y se ha especulado sobre 4 meses de los cuales han sido más los vacíos narrativos, porque poca gente los conoce. Pero estos son (estos) de los que más se recuerdan de Camilo Torres.

Entonces la idea de contar más allá del cliché del cura guerrillero, uno se va encontrando con ese personaje mítico, que termina siendo muy humano y muy sencillo.

**¿Podríamos decir que al hablar de Camilo hoy 50 años de su muerte, es una insistencia política?¿ O nos arriesgaríamos a decir que hablar de es retratar la historia colombiana del conflicto?**

Hablar de Camilo sí es una apuesta política, pero es una apuesta política por la memoria de encontrarnos con los orígenes que dieron a este conflicto, pero que al mismo tiempo ha dejado tantas enseñanzas de resistencia en el pueblo colombiano. , Entonces es una apuesta política por la memoria del conflicto, desde esta orilla ideológica que podamos ampliar mucho más la perspectiva de lo que es el conflicto y no minimizarlo a lo que simplemente fue cosa de unas personas que se alzaron en armas .

**Es difícil reconocer en Colombia este tipo de proyectos ya que lo político generalmente se ve como lo malo y hay pocos espacios donde visibilizarse, ¿dónde queda el trabajo de las personas q están detrás de este proyecto? ¿Quienes hacen parte de él?**

Bueno este proyecto ha contado con la co producción de periferia prensa alternativa, el cual es un proyecto de comunicación popular que lleva 10 años en el país y también agit pro que es una productora nueva, que básicamente es un ejercicio de nuevos productores y nuevos realizadores colombianos, que tenemos una apuesta por este tipo de narrativas, de hecho este documental es básicamente casual, porque el proyecto estaba en función de hacer un cortometraje sobre la vida de Camilo Torres, lo que pasa es que con la investigación que hemos querido para desarrollar este proyecto contó con hablar con alguna gente que lo conocía de primera mano, y algunas facetas de su vida, y pues aprovechamos este espacio de investigación para hacer este documental que hable del camilismo, que también es bastante relevante para el país que después de 50 años ese pensamiento de Camilo a mutado por varias generaciones.

**Hay diversos espacios, organizaciones que hablan de Camilo Torres, ¿cómo cree que recibirán este nuevo proyecto? ¿Estamos próximos a conocer este proyecto terminado?**

. La familia camilista es muy grande y diversa, y nuestra apuesta es poder hacer el estreno el 26 de agosto, el día que se cumplen 51 años de la publicación numero uno del periódico del Frente Unido. Donde precisamente se socializaba al país y a la gente organizada en el movimiento de Camilo, sus mensajes, sus ideas, su pensamiento, básicamente lo que queremos reflejar con este documental es poder ser puente para que las ideas de Camilo le lleguen a la gente.

<iframe src="https://www.youtube.com/embed/9eVS9o-EfR4" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>
