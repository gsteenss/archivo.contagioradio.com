Title: Si CSJ absuelve a Plazas Vega "continuará la impunidad"
Date: 2015-07-06 17:43
Category: DDHH, Nacional
Tags: Coronel Plazas Vega, Corte Interamericana de Derechos Humanos, corte suprema, desaparecidos, Escuela de Caballería, Palacio de Justicia, proceso de paz, René Guarín, Universidad Cooperativa INDESCO
Slug: si-csj-absuelve-a-plazas-vega-continuara-la-impunidad
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/07/Sin-Olvido-Palacio-de-Justicia.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Contagio Radio 

<iframe src="http://www.ivoox.com/player_ek_4728730_2_1.html?data=lZyfmpyXdI6ZmKiakp2Jd6KmkpKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRlsbihqigh6adb6jpwteSpZiJhaXihpewjcvFscrgysbfjcnJb8XZ1Mbdw9fJp8rY0JC9w9HFp8rjjMnSja%2FZt9Whhpywj6jTstXVyM7cjbfFqMrjjJKSmaiReA%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [René Guarín, familiar de las víctimas del Palacio de Justicia] 

###### [6 Jul 2015] 

De acuerdo a una publicación de la revista Semana, la Corte Suprema de Justicia estaría pensado en absolver al Coronel (r) Alfonso Plazas Vega. Según ese medio, seis magistrados estarían a favor de esta postura. Para las víctimas la absolución significaría **continuar en la impunidad como ya se ha hecho durante 30 años**,  afirma René Guarín, familiar de una de las personas desaparecidas del Palacio de Justicia.

“*Este caso de desaparición forzada de nuestros familiares es desde hace tres décadas y se ha mantenido en la impunidad… este caso es **un crimen de Estado donde el Estado es juez y parte, por lo que es más complicado que pueda haber justicia***”, dice Guarín, y agrega que “*lo cierto es que, si se absuelve a Plazas Vega,  no tendrá un cambio dramático en su vida porque siempre ha estado libre, es tal vez el detenido en Colombia que tiene mayor cantidad de hectáreas para moverse y una gran cantidad de gente a su servicio*”.

Seis magistrados creen que no hay pruebas claras de que el coronel haya ordenado de manera contundente la desaparición de dos personas.  Los  otros dos magistrados que estarían en contra de absolver a Plazas Vega, piensan que **él debe responder por las acciones que se dieron en la Escuela de Caballería,** donde se ha establecido que dos desaparecidos fueron llevados.

Para Guarin, la libertad del coronel Plazas respondería a un “**accionar de tipo político para alinearse con los diálogos de paz en la Habana** y buscar el apoyo del estamento militar a través de esta libertad del coronel”.

Guarín también resaltó que el magistrado que propuso la absolución de Plazas Vega es el mismo que planteó la absolución del ex rector de Universidad Cooperativa **INDESCO**, acusado de masacres paramilitares en los años 80´s, lo que implica que no es sorpresa la nueva iniciativa del magistrado.

**“Esperamos que la Corte Suprema valore que llevamos 30 años con nuestros familiares desaparecido sin que el Estado los devuelva ni vivos ni muertos; el Estado tiene esa deuda”,** dice Guarín, y señala que absolver al coronel sería una decisión muy cuestionada  a nivel nacional e internacional, teniendo en cuenta que el Estado fue condenado por la desaparición de los desaparecidos del Palacio de Justicia a través de la Corte Interamericana de Derechos Humanos el año pasado.
