Title: Comunidades le recuerdan al Gobierno que acudir a aspersión aérea sería ilegal
Date: 2020-08-26 15:49
Author: AdminContagio
Category: Actualidad, Comunidad
Tags: Aspersión Aérea, Corte Constitucional, PDET, PNIS
Slug: comunidades-le-recuerdan-al-gobierno-que-acudir-a-aspersion-aerea-seria-ilegal
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/08/Asperison.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: biodiversidadla.org

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Mientras el gobierno de Iván Duque y su gabinete resaltan la necesidad de volver a la aspersión aérea para luchar contra el narcotráfico, variable que han identificado como la causante de las más recientes masacres en departamentos como Nariño, Cauca, Norte de Santander, Antioquia y Arauca, comunidades y abogados le recuerdan que **según la Corte Constitucional en su sentencia T-236 de 2017 y el Auto 387 de julio de 2019, está prohibido reanudar esta actividad** hasta no ofrecer las garantías requeridas para la salud de quienes habitan en los territorios.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

**Franklin Cortés, vocero de la Coordinadora Nacional de Cultivadores de Coca Amapola y Marihuana (COCCAM)** considera lamentable que después de reactivarse la violencia en el departamento de Nariño la única forma de responder del Gobierno sea la aspersión que recuerda, "es otra forma de violencia", recalcando que para los territorios debe haber consulta previa para cualquier acción que el Gobierno quiera realizar.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Según el vocero, hoy son aproximadamente 16.360 familias que están en el programa, sin embargo en medio de la erradicación forzosa no ha existido una sustitución ***"se han cumplido algunos elementos del PNIS, se están terminando de pagar las bonos, pero la asistencia técnica no ha llegado a todas las familias y hasta hoy no hay formulación de proyectos productivos".***

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Cabe resaltar que para 2019, según la Oficina de las Naciones Unidas contra la Droga y el Delito en Colombia, **la erradicación forzosa dejaba como resultado una resiembra del 40% mientras la voluntaria fue de menos del 6%,** lo que lleva a concluir a Cortés que las familias han cumplido con los compromisos, el que no ha cumplido es el gobierno".

<!-- /wp:paragraph -->

<!-- wp:quote -->

> "Es tan perfecto el programa que al Gobierno le ha quedado grande implementarlo, porque no quiere la sustitución voluntaria de cultivos de uso ilícito, porque si hay sustitución, hay desarrollo".
>
> <cite>Franklin Cortés</cite>

<!-- /wp:quote -->

<!-- wp:paragraph {"align":"justify"} -->

Jhenifer Mojica, abogada e integrante de la Comisión Nacional de Territorios Indígenas recalca que existe evidencia científica de que las sustancias químicas usadas en la aspersión han tenido efectos irreversibles en el territorio, en las cuencas de agua y en los territorios, sin mencionar las más de seis sentencias judiciales que han reiterado al Gobierno la necesidad de evaluar su política de lucha contra las drogas. [(Lea también: Suspensión de audiencia virtual un paso en la lucha contra el glifosato)](https://archivo.contagioradio.com/suspension-de-audiencia-virtual-el-primer-paso-en-la-lucha-contra-el-glifosato/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Según [datos compartidos por el senador Iván Marulanda,](https://drive.google.com/file/d/1eIvBmQiaT3ory6ANVArkyPpI35S0D1nE/view) se estima que entre el 2000 y el 2015, Colombia con financiación parcial del gobierno de Estados Unidos, invirtió cerca de \$1.200 millones de dólares por año para combatir la producción y tráfico de drogas, **mientras entre 2005 y 2014 se invirtieron 8,8 billones de pesos al año para la erradicación de cultivos de coca, más de cinco veces el presupuesto anual de todo el sector agricultura.**

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Ante el escenario que plante el Gobierno, la abogada resalta que no solo se intenta desconocer los fallos de la Corte, sino los derechos a la consulta previa y el punto 4 del acuerdo de paz, en un momento en que la pandemia tampoco ofrece condiciones para hacer un proceso de participación vinculante en los territorios.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### El Gobierno no puede asperjar al día de hoy

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Por su parte **el abogado y defensor de DD.HH, Alirio Uribe Muñoz** fue positivo al respecto al afirmar que al día de hoy, legalmente, el Gobierno no puede asperjar con glifosato hoy en Colombia, "el ministro de la Defensa está mintiendo porque las aspersiones está suspendidas por la Corte Constitucional desde el 2017", recuerda el abogado quien resalta que la alta corte establecío una serie de requisitos que deben ser cumplidos antes de que se contemple la opción de la aspersión.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Aunque el Gobierno ha expresado que su estrategia comenzaría en 14 zonas del país incluyendo los departamentos de **Meta, Guaviare, Vichada, Caquetá, Putumayo, Cauca, Nariño, Antioquia, Bolívar, Córdoba y Santander,** el abogado reafirma que "no se puede hacer aspersiones aéreas en ninguna parte del país porque no hay una licencia ambiental" y que de llegar a hacerlo se estaría viciando la decisión de la Corte, además reitera, que antes que la erradicación forzada se estableció darle prioridad a la sustitución voluntaria al acuerdo con las comunidades.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

"Todo lo que ha hecho el Gobierno en materia de drogas es un fracaso, la erradicación forzada ha traído muertos y el decomiso de la dosis personal fue tumbado por la Corte Constitucional y el Consejo de Estado, todo lo que hace Duque lo hace mal", expresó Uribe Muñoz.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### La perversidad de usar las masacres para una política de aspersión

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Para Alirio Uribe el pretender usar la aspersión aérea como una forma de combatir las masacres, que según el Gobierno son generadas por el narcotráfico resulta contradictorio por parte de un Gobierno que no cumplió el acuerdo de paz y que "por haber estimulado la violencia, y las masacres, para prevenirlas dice que va a asperjar".

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Aunque el Gobierno ha anunciado la creación de un escuadrón del Ejército para combatir el narcotráfico, el abogado llama la atención sobre cómo las masacres vienen ocurriendo en las zonas más militarizadas del país, "hemos visto cómo no para la violencia, se siguen moviendo por los ríos las toneladas de cocaína y no atacan a los grupos armados pero hacen presencia en las regiones".

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Ante el aumento de las masacres, Mojica afirma que se está enviando un mensaje de de terror contra las comunidades y justo en el que momento que se piden respuestas efectivas de garantías y protección a la vida al Gobierno, este "extiende una cortina de humo entorno al glifosato, que no tiene que ver" y es aprovechada la crisis "para adelantar una política que ha sido cuestionada por sus efectos negativos.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

La abogada expresa que si bien la presencia de actores legales e ilegales que tienes que ver con el narcotráfico es una de las variables que ha aumentado el riesgo de las comunidades, esta no es la única, sin embargo hora está siendo usada para sacar adelante "una agenda que le impone Estados Unidos a Colombia en la lucha antidrogas".

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Una agenda impuesta por los EE.U.U.

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Sobre la aspersión aérea y la agenda de EE.UU, Ricardo Vargas, investigador asociado del Instituto Trasnacional - TNI señala que aunque el país norteamericano ha usado esa narrativa para acercarse a Colombia. Aunque el consumo de cocaína en Estados Unidos pasó del 2,2% en el 2010 al 2.7% en el 2018, no existe evidencia de que la coca sea el factor central en la actualidad en la lucha contra las drogas.[(Lea también: El regreso del glifosato, una amenaza para campesinos en zonas cocaleras)](https://archivo.contagioradio.com/el-regreso-del-glifosato-una-amenaza-para-campesinos-en-zonas-cocaleras/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

"Estamos asistiendo a un fenómeno de consumo de cocaína donde no hay problemas de epidemia como se veía en los años 80, hoy en día el problema de EE.UU. es el uso de opiacidos y de drogas sintéticas donde los distribuidores son China, India y México", algo que para el experto cambia la ecuación y apunta directamente a que la agenda de Trump está centrada en Venezuela, por lo que **la lucha contra las drogas es la forma de justificar la presencia de trompas en suelo nacional, cercano a la frontera con el gobierno de Nicolás Maduro.**

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Aunque el analista no se mostró positivo frente a los dos próximos años que restan del Gobierno y ausencia de una política clara antinarcótica y un compromiso con las comunidades, destaca que muchas de las decisiones que vengan a futuro con respecto a este tema estarán ligadas a los esfuerzos que se realicen junto a la comunidad internacional, el Congreso de los Estados Unidos y las elecciones presidenciales de este mismo país.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Por otro lado, concluye que ante la eventual ausencia de equilibrio en los organismos de poder que vive el país y que lesionaría directamente las garantías de las comunidades, son necesarios mecanismos que han venido utilizando las poblaciones como realizar registros de lo que pasa en sus territorios, una constante información sobre la opinión pública y fortalecer los procesos de movilización para la contención de la erradicación forzada que continúa siendo abanderada por el Gobierno. [(Lea también: Aspersión aérea con glifosato, una estrategia fallida que se repite)](https://archivo.contagioradio.com/la-aspersion-aerea-con-glifosato-no-es-una-estrategia-efectiva/)

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
