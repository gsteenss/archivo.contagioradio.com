Title: La  frustrante JEP
Date: 2020-06-09 00:40
Author: Camilo de las Casas
Category: Camilo, Opinion
Slug: frustrante-jep-mancuso
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/01/Acogimiento-de-Mancuso-a-la-JEP.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

Para los afectados por la violencia de Estado –entre ellas la violencia paramilitar- que comprenden que una de las modalidades de la estrategia criminal de Estado fue la vinculación de civiles para desarrollar operaciones encubiertas de violaciones de derechos humanos, decisiones como la de rechazo a comparecencia del excomandante paramilitar Mancuso es un portazo simbólico y real.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El infortunio de nuestro país es que las aspiraciones de millones de víctimas y de la sociedad de saber la verdad y de encontrar justicia en el mecanismo adoptado fruto del Acuerdo de Paz entre el Estado y las FARC EP como Justicia Transicional se ha ido desvaneciendo-

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

La JEP ha tenido un camino tortuoso para su creación. Desde la propia Habana este mecanismo resultó ser un punto de los de mayor discusión. Un Tribunal más allá de los armados de Estado y FARC. En el mecanismo de rendición de cuentas se incluía su competencia para investigar a expresidentes, a terceros y militares conforme al Estatuto de Roma.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En el pulso político y como otra concesión de la entonces guerrilla ese cuerpo de poderosos los ex, los primeros llamados terceros y las llamadas cadenas de mando quedaron por fuera de la competencia de la JEP. Luego el honorable Congreso de la República y la propia Corte Constitucional afinaron mucho más el recorte imposibilitando competencias precisas sobre los exparamilitares y llamados terceros.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El titular sepulturero del 4 de junio  la JEP fue resonado por los medios privados con la propia fuerza de original del boletín 077: “JEP RECHAZA SOMETIMIENTO DE MANCUSO” [Aquí la decisión de la JEP](https://www.jep.gov.co/Sala-de-Prensa/Paginas/La-JEP-rechaza-sometimiento-de-Salvatore-Mancuso.aspx#:~:text=La%20JEP%20rechaza%20el%20sometimiento,04%20de%20junio%20de%202020.&text=En%20consecuencia%2C%20al%20no%20tratarse,delitos%20cometidos%20por%20Mancuso%20G%C3%B3mez)

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

La postergada decisión sobre Mancuso que desde el año pasado se rumoraba resultó un fiasco para muchas de las víctimas y la sociedad expectantes de verdades. Es una verdad histórica que las verdades reconocidas o dichas por Mancuso en los tribunales de la ley 975 de Justicia y Paz y las que se encuentran engavetadas en las compulsas de copias en la Fiscalía General desde hace más de 10 años, requieren ser profundizadas en un escenario pos procesamiento en Estados Unidos de cara a sus afectados directos y al país.

<!-- /wp:paragraph -->

<!-- wp:quote -->

> Es una realidad que el proceso judicial en el país del norte por tráfico de drogas contiene unas verdades de financiación que solo conocen las agencias como DEA, FBI, y CIA, verdades que nuestro país desconoce.
>
> <cite>Camilo de las Casas</cite>

<!-- /wp:quote -->

<!-- wp:paragraph -->

La decisión de la Sala de Reconocimiento, que afortunadamente es por una mayoría relativa, está demostrando el desconocimiento del país nacional y la evidencia que esta justicia transicional está lejos de lograr lo que millones de colombianos, entre ellas, prioritariamente las víctimas, esperaban: la verdad.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Aun así, intentando zanjar la distancia entre la verdad histórica y la verdad procesal, entre la verdad real y la santanderista, es decir, entre el país nacional y el país real, víctimas y procesados esperaban que la JEP, tuviera la posibilidad entre los márgenes de realizar un esfuerzo jurisprudencial, de construcción dogmática conforme al país real y respetando su marco legal de competencias.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

La expresión necrológica de su medio de información Mancuso rechazado tiene un efecto simbólico sin precedentes que genera total desconcierto al compararse con otras decisiones de ese tribunal. Sus visiones tan plurales generan dudas en el país de a pie. La decisión sobre Mancuso parece demostrar aún que magistrados de la sala de Reconocimiento, como ocurre con otras Salas, están aferrados a una dogmática que deja de lado la realidad. Mancuso fue financiador, fue promotor, fue planificador, fue ejecutor, fue beneficiario.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Al lado de Mancuso están unos grandes empresarios, esos sí, que nunca se armaron, pero cuyos nombres aparecen en el anonimato ante todos los tribunales, que orquestaron y se beneficiaron en la estrategia paramilitar. Ellos ríen en estos días. Espantan sus miedos, al lado de Él. Esos empresarios del círculo del Ubérrimo están desarrollando grandes negocios en Colombia, Perú, Centro América y otros a escala mundial. Esos que temían a la JEP, ya empezaron a darse cuenta que esa justicia transicional es un tigre de papel.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Los sectores del Centro Democrático que conocen de la decisión de la JEP están de plácemes.

<!-- /wp:heading -->

<!-- wp:paragraph -->

Los sectores del Centro Democrático que conocen de la decisión de la JEP están de plácemes. Les debemos invitar a que cesen de usar operaciones ilegales de inteligencia respecto a este solicitante y otros más. Es suficiente con que los abogados obrando de buena fe redacten motivos y argumentos para que sus clientes  sean recibidos en la JEP, en la Sala de Reconocimiento de Verdad, para que el menor espacio de duda, sea usado por el santanderismo y la asepsia de algunos magistrados, que sumados a sus miedos, prefieran como el avestruz desconocer un fenómeno de crímenes de lesa humanidad consumados con la privatización de la fuerza a través del paramilitarismo y la disposición de decir verdad de uno de sus máximos responsables.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En el sentido común, no en las mujeres y hombres de las leyes, **a pocos comparecientes se les ocurriría ir a un tribunal (JEP) en el que puedan ser condenados a 20 años de cárcel, si mienten, si la pena puede ser de ocho años (Ley 975) en el que tribunales de ese mecanismo judicial han tomado decisiones sobre su caso fundamentales.** }

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Es posible incluso que Mancuso alegue haber cumplido en Estados Unidos, sus años de privación de la libertad,  lo que le daría libertad inmediata en Colombia. Algo importante se juega Mancuso. ¿Qué es ese algo, qué requiere se judicializado? Si tiene algo entre manos contra sus víctimas y el país, sería bastante insensato.

<!-- /wp:paragraph -->

<!-- wp:quote -->

> Una sociedad conciente de la paz deberá prepararse para construir un sistema de justicia, ese sí, transicional integral, qué sin desnaturalizar su visión, asuma un sentido histórico de transformación de todas las violencias...

<!-- /wp:quote -->

<!-- wp:paragraph -->

**Ahora, solo resta que la sala de Apelación que ha sido brillante en definir límites y posibilidades escuché a las víctimas de Mancuso**, al propio Mancuso que se define en períodos aciagos de nuestra historia como tercero, que este identifique su rol hasta el 13 de mayo de 2008 cuando fue extraditado a los Estados Unidos por el gobierno de Uribe para silenciarlo. Tal vez,  así pueda revivir la esperanza, que la JEP es el tribunal idóneo para saldar una deuda histórica ante la impunidad.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Si estas expectativas son lejanas de cumplirse está siendo evidente que una sociedad conciente de la paz deberá prepararse para construir un sistema de justicia, ese sí, transicional integral, qué sin desnaturalizar su visión, asuma un sentido histórico de transformación de todas las violencias con una rendición de cuentas acorde con los derechos de todas las víctimas y procesados, entre ellas, los llamados terceros.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

[Lea aquí más columnas de Camilo de las Casas](https://archivo.contagioradio.com/author/camilo-de-las-casas/)

<!-- /wp:paragraph -->
