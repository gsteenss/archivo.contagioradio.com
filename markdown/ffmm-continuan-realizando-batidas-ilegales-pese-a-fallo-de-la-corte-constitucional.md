Title: FFMM continúan realizando batidas ilegales pese a fallo de la Corte Constitucional
Date: 2015-08-11 13:08
Category: DDHH, Nacional
Tags: batidas ilegales, Corte Constitucional, ejecuciones extrajudiciales en Colombia, Ejército Nacional, falsos positivos, FFMM, Medellin, objeción de conciencia, Polo Democrático Alternativo, Víctor Correa
Slug: ffmm-continuan-realizando-batidas-ilegales-pese-a-fallo-de-la-corte-constitucional
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/08/batida_ilegal.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Diario ADN 

<iframe src="http://www.ivoox.com/player_ek_6536600_2_1.html?data=l5qgmJuUdI6ZmKiakpqJd6KomZKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRlsbk08rgx9PYqc%2FoxpDDh6iXaaK4xNnc1JCns9PmxsaSlKiPqMbi1tPQy8aPtdbZjMjc0NnNsoa3lIqvo8bSb8Ohhpywj6jTstXVyM7cjbfFqMrjjJKSmaiReA%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Víctor Correa, represente a la cámara Polo Democrático Alternativo] 

###### [11 Ago 2015]

Luego de conocerse la situación que enfrentó el [representante a la cámara Víctor Correa en la ciudad de Medellín al evitar la retención de 15 jóvenes](https://archivo.contagioradio.com/represente-victor-correa-es-subido-a-camion-de-ffmm-al-defender-a-jovenes-de-batida-ilegal/) en una batida ilegal del Ejército Nacional, el congresista denunció en el programa Otra Mirada de Contagio Radio, que **pese al fallo de la Corte Constitucional y la Procuraduría, la fuerza pública continúa reclutando jóvenes en diferentes ciudades de Colombia.**

"Yo me subí voluntariamente al camión", aseguró Correa, quien añade que de no ser así, los jóvenes habrían sido llevados a Arauca sin ningún tipo de garantía de sus derechos. Esto lo argumenta tras constatar que el camión realmente pertenecía a la Brigada XVIII en ese departamento.

“Cuando vi el camión, supe que se trataba de una batida ilegal, y **todas las violaciones a las que este tipo de acciones conlleva”,** por ejemplo, el representante recuerda que esa fue una de las modalidades utilizadas en los casos conocidos como “falsos positivos” o ejecuciones extrajudiciales.

Según las palabras del congresista, los militares insistieron en que **“tienen que reclutar y cumplir con un número de soldados”**, además, señala que en el momento en que habló con los altos mandos presentaron documentos que no tienen el peso jurídico para argumentar el reclutamiento forzado.

“La Corte y el Ministerio Público han sido enfáticos en esto” dice Correa, refiriéndose a la Sentencia T-455/14 de la Corte Constitucional, por la cual se decreta “*DERECHO FUNDAMENTAL A LA OBJECIÓN DE CONCIENCIA AL SERVICIO MILITAR OBLIGATORIO-Reiteración de jurisprudencia/**REDADAS O BATIDAS DESTINADAS AL RECLUTAMIENTO Y MOVILIZACIÓN DE CONSCRIPTOS SON INCONSTITUCIONALES/VULNERACIÓN DEL DERECHO FUNDAMENTAL A LA LIBERTAD PERSONAL** EN REDADAS O BATIDAS QUE REALIZA EL EJERCITO/EFICACIA DEL DERECHO FUNDAMENTAL DE PETICIÓN DEPENDE DE LA RESPUESTA OPORTUNA Y DE FONDO A LO SOLICITADO-Reiteración de jurisprudencia/SOLICITUDES DE EXENCIÓN DEL SERVICIO MILITAR-Autoridades militares están obligadas a responder de fondo y dentro de término máximo de 15 días”.*

Para el representante del Polo Democrático Alternativo, se deben generar otras acciones para que los jóvenes defienda sus derechos, es por eso que diversas  organizaciones de derechos humanos y colectivos juveniles han realizado elementos pedagógicos para que los jóvenes conozcan sus derechos.

<p>
<script src="//e.issuu.com/embed.js" async="true" type="text/javascript"></script>
</p>
<div class="issuuembed" style="width: 525px; height: 549px; text-align: center;" data-configid="0/14611024">

</div>

<p>
<script src="//e.issuu.com/embed.js" async="true" type="text/javascript"></script>
</p>

