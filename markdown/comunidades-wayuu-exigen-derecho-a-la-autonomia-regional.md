Title: Comunidades Wayuu vuelven a bloquear la vía férrea del Cerrejón
Date: 2017-02-01 16:21
Category: DDHH, Nacional
Tags: Fuerza Wayuu, Instituto Colombiano de Bienestar Familiar
Slug: comunidades-wayuu-exigen-derecho-a-la-autonomia-regional
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/02/indigenas_wayuu_0-e1516041332156.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: ZonaCero] 

###### [1 Feb 2017]

Desde muy temprano en la mañana indígenas Wayuu bloquean la vía férrea que sirve al Cerrejon, a la altura del kilometro 70, en el territorio ancestral de la comunidad Katsaliamana,  **debido al incumplimiento por parte del Instituto de Bienestar Familiar y que siga sin garantizarse los derechos a la autonomía territorial y consulta previa.**

Desde el pasado 21 de diciembre de 2016, los indígenas ya habían expresado la falta de atención por parte del Instituto Colombiano de Bienestar Familiar sobre los **planes de alimentación, ya que  las características de sus territorios no permiten que se conserven** los alimentos y tampoco están acordes a la alimentación ancestral de las comunidades. Le puede interesar: ["Comunidades Wayuu de la Guajira cierran pasos de vía férrea de empresa Cerrejón"](https://archivo.contagioradio.com/comunidades-wayuu-de-la-guajira-cierran-pasos-de-via-ferrea-de-empresa-cerrejon/)

De igual forma habían expresado la necesidad de que se pusiera en marcha el **decreto 2500**, por el cual se regula de manera transitoria la contratación educativa por parte de entidades certificadas como cabildos, autoridades tradicionales indígenas y organizaciones indígenas y que efectúa el derecho a la autonomía regional, para **poder llevar a cabo la construcción del Sistema de Educación Propio y continuar con una educación enfocada en el traspaso de saberes.**

###### Reciba toda la información de Contagio Radio en [[su correo]
