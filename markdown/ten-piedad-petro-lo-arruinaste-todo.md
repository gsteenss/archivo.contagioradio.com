Title: ¡Ten Piedad Petro! … lo arruinaste todo
Date: 2017-12-08 08:48
Author: JOHAN MENDOZA TORRES
Category: Johan Mendoza, Opinion
Tags: Candidatos a la presidencia, Clara López, Gustavo Petro
Slug: ten-piedad-petro-lo-arruinaste-todo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/12/PETRO.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Coronades03 

#### **Por: [Johan Mendoza Torres](https://archivo.contagioradio.com/johan-mendoza-torres/)** 

###### 8 Dic 2017 

[Las alianzas políticas, las rupturas de coaliciones que prometían un “no ser lo mismo”, el cinismo de Lleras, la intriga y la mentira de Uribe, las aguas tibias de Fajardo, la impotencia política del saliente y relajado Santos que pronto se va para París a gozar de las mieles de una sociedad construida sobre la base del colonialismo y la represión contra Argelia e Indochina, mientras en Soacha lo siguen bendiciendo y perdonando por lo de las ejecuciones a jóvenes inocentes para hacerlos pasar por guerrilleros….]

[¡¡¡¡Todo, todo, todo!!!! lo que ha ocurrido conforma un cóctel de múltiples licores que  simplemente ha dejado a más colombianos embriagados con la podredumbre intelectual, política, de criterio, de ideología, de principios, de honradez, de integridad que tiene la actual clase política.]

[¿Cuál fue la cereza del cóctel? … Petro. La confianza que se había ganado Petro, la intencionalidad favorable que le acompañaba, no estaban alineadas con una maquinaria tradicional o un apoyo de los medios masivos. Bien se puede afirmar que si Petro hubiese cometido las burradas de Peñalosa, lo seguirían ridiculizando desde todos los frentes.]

[Asimismo, Caracol Radio quien no ha hecho más sino culpar a Petro de la lluvia, el sol, el frío y el calor, ni siquiera con toda la tecnología a su servicio ha podido detener el caudal de gente que lo ha escuchado, que se ha dado cuenta de que no es egocéntrico sino que habla lento y chistoso, que finalmente cree en lo público y eso es un gran valor en medio de un mundo neoliberal.]

[Petro la embarró al mezclar la necesidad de fuerza política, con la vinculación de sus planes de gobierno progresista, con una señora como Clara López. ¿Por qué? Sencillamente porque Clara López es la misma que fue opositora de Santos durante casi 4 años, lideró a un partido supuestamente de izquierda, y finalmente “se dejó callar la boca” además de traicionar a su propio partido, aceptando el ministerio del trabajo que el mismísimo Santos ¡¡a dedo!! le otorgó.]

[Si lo anterior no es suficiente para tener claro que Clara en cualquier momento “la tiene clara”, entonces es preciso que nos apretemos más fuerte la venda sobre los ojos, que le demos un aplauso a Petro y que se consume la traición.]

[Ser de izquierda no significa tener un “]*[discurso social]*[”, ser de izquierda no significa “]*[ser un liberal más o menos más liberal que otro liberal que es más o menos, menos liberal]*[”, ser de izquierda no significa “]*[tener responsabilidad social y empresarial]*[”, ser de izquierda no es “]*[ser un intelectual crítico, pero que cuando se le pide coherencia en la vida real, se ofusca y se oculta tras un libro, un buen sexo y una chaqueta con parchecitos en los codos]*[”.  ]

[Ser de izquierda no significa serlo porque lo dice RCN o Caracol TV o Caracol Radio, ser de izquierda simplemente es reconocer algo elemental:]**los intereses de la clase política, oligarca y terrateniente son** **ABSOLUTAMENTE OPUESTOS** **a los intereses de la gente trabajadora, campesina, explotada y endeudada de este país.**

[Por eso, los que critican a la izquierda siempre anuncian “es que son muy polarizados” ¡¡pues claro!!, la absoluta oposición de intereses significa que existe polaridad, y eso no es nada negativo, eso significa que existe un principio político, y que debería ser inviolable para por lo menos generar respeto entre los enemigos y no desilusión o vergüenza tácita… Hay desechar a los neoliberales que insisten con su epistemología trasnochada de]*[“soy a-político”  “la economía debe liberarse de la política”, “no me interesa la política”]*[ así como desechar esas terminologías baratas de]*[“centro-izquierda” “centro-derecha” “centro-centrista” “centro más pa’ allá” “centro más pa’ acá” ¡¡absurdos!!]*

[Tengamos criterio… en política no existen puntos medios. Lo único que existe son miedos a aceptarlo.  ]

[La gente común, no tiene nada en parecido con la clase política y clientelista más allá de que respiramos un mismo aire, ¡es más! de seguro pronto nos lo cobrarán porque a eso se dedican, a usufructuar nuestro esfuerzo, nuestro trabajo, para solventar su codicia, cultivar sus amistades y procurar que sus hijitos continúen su legado. ¿Le parece estúpido escribir que serán capaces de cobrarnos el aire? ¡¡¿acaso no nos cobran ya la tierra, el agua y el fuego?!!!  ]

[Clara López es crema y nata…. No tiene nada en común con la gente común y corriente, mientras ella era una hippie en Harvard, en Colombia torturaban gente. Petro: la embarraste, pues tu independencia no debió mezclarse con la clase politiquera de Colombia, lo mejor que pudiste hacer, fue seguir la línea marginal que te mantenía digno de confianza. Apoyar a los nadie, a los que no tienen voz, a los que han sido excluidos, a los que no tienen nada en común con los que han tenido siempre el poder, esa es la tarea más trascendental que hoy solicita Colombia.]

[Colombia No necesita más violencia, pues ya imagino que muchos asegurarán que lo que se intenta con estas letras es “dividir”. ¡Claro que no!, lo que se intenta en estas letras es no dejar pasar por alto que Petro se alió con Clara López y eso objetivamente es una traición a la confianza que había generado.]

[De hecho, antes de que este artículo sea considerado “guerrerista” es necesario exponer que a los enemigos políticos se les debe ante todo respetar. Asimismo, se debe reconocer que lo que más tiene]*[mamada]*[ a nuestra gente es esa incoherencia ideológica de los candidatos y candidatas que lastimosamente hacen culminar una conversación con el comentario “todos se venden”.]

[No sería muy útil sugerir que por pensar distinto tengamos que matarnos o insultarnos. Pero del mismo modo no es posible que el respeto al enemigo político se convierta en la fraternización con el enemigo político, ¡¡¡allí reside la pérdida de los principios!!! En el respeto al enemigo político, y en la nula fraternización con él, reside la posibilidad de la resistencia biopolítica al poder. Y si se consuma como biopolítica, entonces tendrá posibilidad de ser una política, seria, digna de confianza, digna de esperanza que tenga capacidad de oponerse al poder.]

[Pero, mientras la invitación al restaurante, la cena en el club, las risas y palmaditas en la espalda luego de que en los debates televisados parecían los más acérrimos enemigos, mientras el jueguito de golf el domingo siga consolidando la impregnación de todo el sistema simbólico del poder a quienes supuestamente pretenden derrocar las viejas formas de hacer la política…. simplemente el fracaso, la traición y la desilusión emergerán una y otra vez…  ¿o acaso cree que la conversación de Petro con Clara se dio en una calle comiendo perro de 2 mil, o en una plaza de mercado con olor a campo?... ¿Qué eso que importa? … precisamente…. importa mucho. Importa todo.]

#### [**Leer más columnas de Johan Mendoza Torres**](https://archivo.contagioradio.com/johan-mendoza-torres/)

------------------------------------------------------------------------

###### Las opiniones expresadas en este artículo son de exclusiva responsabilidad del autor y no necesariamente representan la opinión de Contagio Radio.
