Title: Ni consultas previas ni licencias ambientales deben ser virtuales
Date: 2020-04-16 21:25
Author: CtgAdm
Category: Actualidad, Ambiente
Tags: afros, Ambiente, Decreto, Minesa
Slug: el-gozar-de-un-ambiente-sano-no-frena-el-desarrollo-del-pais
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/11/arhuacos-indigenas-e1511801126214.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

Organizaciones ambientales y sociales, rechazaron la presentación de una medida del Ministerio de Ambiente e Interior que permite continuar con los trámites de licencias ambientales de manera virtual, obviando derechos fundamentales de las comunidades étnicas y campesinas.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Derechos como la consulta previa y la audiencia pública, tendrían que hacerse de manera virtual, desconociendo las realidades de acceso a las nuevas tecnológicas de las comunidades que se verían afectadas en los procesos de aprobación de licencias.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Medida que para Mayerly López , vocera del Comité por la defensa del páramo de Santurban *es "una desfachatez por parte del Gobierno en medio de la crisis que vivimos "* .

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

López afirmó también, que en medio del estado de excepción y de emergencia social, económico y ambiental, emitido por el Gobierno se está aprovechando la confusión para emitir políticas públicas vulnerando así los derechos fundamentales de la población.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/ComiteSanturban/status/1250494077881253889","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/ComiteSanturban/status/1250494077881253889

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

En el la carta Ricardo Lozano, Ministro de Ambiente se dirigió a los diferentes directores de las autoridades ambientales regionales urbanas y territoriales, dando licencia de que estos puedan retomar los permisos, concesiones, autorizaciones, licencias ambientales y certificaciones de control ambiental que se adelantaban ante cada una de las entidades.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

No sin antes aclarar que se deben tomar en cuenta las visitas técnicas realizadas previamente, desconociendo así la realidad actual del país, donde cualquier tipo de intervención en los territorios puede aumentar el indice de contagio en los territorios.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En entrevista con Contagio Radio la vocera del páramo de Santurbán, señaló, *"las multinacionales se están aprovechando de esta contingencia para saquear y extraer de manera desmedida los recursos del ambiente, lo cual significa una falta de respeto total con las comunidades y los derechos constitucionales".*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Y enfatizo que el 18 de marzo el Director de la ANLA, Rodrigo Suárez, [firmó un compromiso](https://archivo.contagioradio.com/anla-suspende-tramite-de-licencia-ambiental-sobre-santurban/) de no llevar a cabo la siguiente etapa del proceso de licenciamiento de Minesa, el cual corresponde a la audiencia pública ambiental mientras se encuentre la emergencia sanitaria.

<!-- /wp:paragraph -->

<!-- wp:quote -->

> *" Esperamos que cumpla porque lo que aquí está en juego es el agua de todos los santandereanos recordando que en medio de esta pandemia el agua y la agricultura son más importantes que la minería".*
>
> <cite>Mayerly López| Vocera del Comité por la Defensa del Páramo de Santurbán </cite>

<!-- /wp:quote -->

<!-- wp:heading {"level":3} -->

### Nuevamente desconocen los derechos de los grupos étnicos y sus territorios

<!-- /wp:heading -->

<!-- wp:paragraph -->

Cerca de 100 organizaciones y personas defensoras de los Derechos Humanos y el ambiente presentaron una carta al presidente Iván Duque señalando su preocupación y rechazo ante las medidas adoptadas dentro del paquete de alternativas para la prevención de las parálisis de la actividad económica en el escenario de la pandemia por covid-19.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Medidas dialogadas el 3 de abril con 25 empresarios asociados al Instituto de Ciencia Política Hernán Echavarría Olózaga, y que señalan que es necesario *"reactivar diferentes procedimientos en los territorios con el fin de evitar que la actividad económica real caiga de forma drástica y persistente".*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Medidas que al final de cuentas según las organizaciones vulneran derechos fundamentales de las comunidades étnicas, *"la consulta previa libre e informada no es un simple trámite, es un derecho de carácter fundamental íntimamente vinculado con el ejercicio de la autodeterminación de los pueblos y su posibilidad de sobrevivir física y culturalmente".*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Agregando que este decreto es, *"contrario a la naturaleza y fin del derecho al tiempo que desconoce las reales condiciones de vida en los territorios indígenas donde el acceso a tecnologías de la información es generalmente excepcional"*.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Finalmente y como lo han señalado diferentes agremiaciones, tras la presentación de los diferentes decretos y medidas por parte del Gobierno; es necesaria la rigurosidad en las decisiones que se tomen, y aún mas importante pensar en la comunidad mas vulnerable del país y sus ecosistemas sagrados.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Le puede interesar: [Carta completa de las organizaciones](https://www.justiciaypazcolombia.com/rechazo-de-medidas-regresivas-en-materia-de-derechos-humanos-y-ambiente-ante-las-alternativas-para-la-prevencion-de-la-paralisis-economica-por-covid-191/)

<!-- /wp:paragraph -->
