Title: Más de 1000 solicitudes mineras amenazan a la Sierra Nevada de Santa Marta
Date: 2017-11-20 13:04
Category: Movilización, Nacional
Tags: arhuacos, indígenas, Minga Indígena, Pueblo Arhuaco, Sierra Nevada
Slug: arhuacos-continuan-en-movilizacion-exigiendo-se-respeten-sus-derechos-sobre-la-sierra-nevada
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/11/arhuacos-e1511199795562.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:  Confederación Indígena Tayrona ] 

###### [20 Nov 2017] 

Los indígenas del pueblo Arhuaco **continúan con la movilización pacífica** exigiendo que se respeten sus derechos territoriales sobre la Sierra Nevada de Santa Marta. En una rueda de prensa realizada en Valledupar, la Confederación Indigena Tayrona argumentó que hay una amenaza sobre el pueblo Arhuaco debido a la existencia de más de mil solicitudes de títulos mineros que buscan activar la extracción de minerales en la Sierra Nevada.

Los indígenas Arhuacos, que desde hace una semana se encuentran concentrados frente a la Gobernación del Cesar en Valledupar, indicaron que **hay incumplimiento de las sentencias de la Corte Constitucional** que protegen la identidad cultural de este pueblo ancestral. Además, indicaron que se han reunido con el viceministro de participación del Ministerio del Interior para realizar un acercamiento con los indígenas.

### **El 25 de noviembre se instalará la mesa de negociación de alto nivel** 

En repetidas ocasiones, los Arhuacos han pedido que el Gobierno Nacional haga presencia en el territorio **a través de una delegación de alto nivel** a la cual le puedan expresar sus preocupaciones. Sin embargo, hay que recordar que esta solicitud no fue atendida por las instituciones del Gobierno y los diálogos de la semana pasada tuvieron que ser suspendidos. (Le puede interesar: "[Arhuacos exigen que la Sierra Nevada sea zona libre de minería"](https://archivo.contagioradio.com/arhuacos-exigen-que-la-sierra-nevada-sea-zona-libre-de-mineria/))

Indicaron que el viceministro de participación del Ministerio del Interior "apenas está desarrollando la agenda política que se va a desarrollar en la mesa de negociación", pero que,  **"no tiene capacidad política para resolver nuestras solicitudes"** por lo que continúan esperando que otros funcionarios hagan presencia.

La autoridad indígena Leonor Zalabata indicó que **“la decisión política debe estar encabezada del Presidente de la República”**. Dijo que no van a aceptar que los temas de las actividades mineras en la Sierra Nevada, “sean presentados como un tema de desarrollo porque van a acabar con el agua de nuestra madre y con el pueblo Arhuaco”. (Le puede interesar: ["ESMAD arremete contra indígenas del pueblo Arhuaco en Valledupar"](https://archivo.contagioradio.com/esmad-pueblo-arhuaco/))

### **Sierra Nevada está amenazada por 348 títulos mineros** 

En varias ocasiones, las comunidades indígenas han denunciado que en su territorio hay 160 minas que están afectando 332 fuentes hídricas donde se incluye la Ciénaga Grande de Santa Marta. Además, según el Consejo Territorial de Cabildos de la Sierra Nevada de Santa Martaen, total **son 348 títulos y solicitudes de títulos mineros,** 285 títulos vigentes, 132 proyectos en marcha.

Adicionalmente **hay cerca de 1320 solicitudes de títulos y otros 18 títulos de explotación en convenio con la Agencia Nacional de Hidrocarburos.** Esta situación tiene en alerta al pueblo Arhuaco que ha insistido en que esto supone una amenaza para la subsistencia de los pueblos originarios. (Le puede interesar: ["Sierra Nevada de Santa Marta amenazada por 348 títulos mineros"](https://archivo.contagioradio.com/sierra_nevada_de_sana_marta_peligro_mineria_/))

Finalmente, Zalabata recordó que los indígenas de la Sierra Nevada cuentan con la solidaridad de diferentes organizaciones sociales y han programado una movilización para el día **jueves 23 de noviembre en Valledupar**. Allí, buscarán defender los recursos hídricos que hacen parte de su territorio sagrado y de una gran parte de la población colombiana.

###### Reciba toda la información de Contagio Radio en [[su correo]

<div class="osd-sms-wrapper">

</div>
