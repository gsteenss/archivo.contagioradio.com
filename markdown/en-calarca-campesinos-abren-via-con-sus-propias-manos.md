Title: En Calarcá,Quindío Campesinos abren vía con sus propias manos
Date: 2020-03-20 16:22
Author: AdminContagio
Category: Actualidad, Nacional
Tags: campesinos, Quindío
Slug: en-calarca-campesinos-abren-via-con-sus-propias-manos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/03/49766eb9-ceaa-49ca-a35c-d9d6d1d95fd4.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/03/5f402b87-752a-43f7-acf2-1dc9d809863a.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: Comunidad el Crucero

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Con sus propias manos, campesinos afectados con un derrumbe que afectaba hace 51 días a más a los habitantes de la vereda El Crucero en el municipio de Calarcá, [Quindío](https://archivo.contagioradio.com/onic-rechaza-abuso-contra-menor-embera-en-el-quindio/). Habilitaron esta vía principal en una acción que, reconocen, fue riesgosa, pero que afectaba el desplazamiento de más de 500 personas.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Eliecer Atuesta, Campesino de  
la Vereda el Crucero afirmó que se vieron obligados a poner en riesgo sus vidas  
en medio del desespero por no encontrar paso, *“tuvimos que coger palas y empezar a abrir paso a los carros, esta es  
la única vía que nos permite llegar rápido a la zona donde compramos nuestros víveres,  
así como salir a vender nuestros productos, la otra es mucho más larga y  
costosa”*; y afirmó que el uso del otro camino les puede costar entre  
\$80.000 y \$100.000 por viaje.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Según el campesino el derrumbe que inhabilitó la vía fue  
causado por el desplazamiento de uno de taludes de contención del vaso Las Azucenas  
del basurero Villa Karina, responsabilidad de la empresa Multipropósito de  
Calarcá, de igual forma medios locales señalaron que *“con ese talud se derrumbó también una planta para tratamiento de  
lixiviados que, por fortuna, no estaba en uso. La responsabilidad es de la empresa,  
pero hasta el momento no ha hecho casi nada para ayudar a resolver el problema  
que ella misma generó”.*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

La suspensión de la obra en su totalidad se registró el pasado  
domingo 15 de marzo en horas de la noche, *“el  
Ejército Nacional y la Alcaldía de Calarcá, retiraron maquinaria y suspendieron  
trabajos afirmando, sin fundamento, que las comunidades del sector amenazaron  
con la retención de la maquinaria ante la falta de actividad en las obras. Acusación  
absurda puesto que quienes mayor interés tienen en la apertura de la vía son  
las comunidades afectadas que ya llevan 51 padeciendo el problema”,* señaló  
en su cuenta de Facebook Néstor Ocampo vocero de los calarqueños.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Ante esto Atuesta afirma que la responsabilidad de la obra había  
quedado en manos del Ejército Nacional quien habían prestado la maquinaria para  
atender la emergencia*, “el Ejército dice  
que los amenazamos, pero lo cierto es que ellos se quieren salir de ahí y usan  
eso como excusa para hacerlo”*, aseverando que los uniformados no son los  
que tienen que atender esta emergía, “quedaron básicamente solos solucionando  
la apertura, cuando quien deben estar es la autoridad departamental”.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Adicional una de las acciones que más rechaza Atuesta son las acusaciones del Secretario del Interior del departamento, Eduardo Orozco, quien señala en un vídeo que la protesta de los campesinos es una acción *“de protagonismo y oportunismo por algún sector, que es insolidario ante los verdaderos problemas que hoy tiene el departamento que son superiores, porqué está de por medio la vida misma”* , haciendo referencia en este caso a la situación que afrontan con el coronavirus, que a la fecha tiene 3 contagiados en el departamento del Quindío.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### **“No es solo venir y limpiar la vía se trata de hacer toda una obra para que esto no vuelva a ocurrir”**

<!-- /wp:heading -->

<!-- wp:image {"id":82268,"sizeSlug":"large"} -->

<figure class="wp-block-image size-large">
![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/03/49766eb9-ceaa-49ca-a35c-d9d6d1d95fd4-1024x576.jpg){.wp-image-82268}  

<figcaption>
Foto: Comunidad El Crucero

</figcaption>
</figure>
<!-- /wp:image -->

<!-- wp:paragraph -->

Eliecer Atuesta recalca que la emergencia que viven por el  
derrumbe no se trata simplemente de “venir y retirar el material que obstruye  
la vía, o hacer un puente que beneficie a unos pocos, dejando afectados a  
quienes viven en las fincas más lejanas; aquí es necesaria una planeación  
completa”.

<!-- /wp:paragraph -->

<!-- wp:quote -->

> *“Es admirable la voluntad de algunos vecinos, vecinas y otras personas, para ir con palas a resolver el problema por su propia mano. Pero eso no es tan fácil como parece, son miles de toneladas que aún hay que mover de ahí. Y es peligroso para quienes hagan eso, es peligroso incluso para quienes pasan por ahí a pesar de la dificultad. En cualquier momento se puede dar un nuevo derrumbe que atrape a alguien”*
>
> <cite> Nestor Ocampo – Habitante Calarqueño </cite>

<!-- /wp:quote -->

<!-- wp:heading {"level":3} -->

### **“La presencia de Multipropósito S.A, solo ha creado problemas a los campesinos de Calarcá”**

<!-- /wp:heading -->

<!-- wp:paragraph -->

Por último Atuesta afirmó que la gran mayoría de problemas que aquejan a los habitantes de Calaracá es la falta de atención por parte de la Corporación Autónoma Regional del Quindío (CRQ), “la corporación ha pasado por alto los problemas en las vías y especialmente las afecciones sanitarias y de seguridad causadas por la permisividad que le dan a la empresa [Multipropósito,](https://www.cronicadelquindio.com/noticia-completa-titulo-exfuncionario-de-multiproposito-continuara-en-prision-cronica-del-quindio-nota-137027) quien debió atender la situación que generó”. (Le puede interesar: <https://archivo.contagioradio.com/no-solo-es-el-derrumbe-de-un-barranco-en-calarca/>)

<!-- /wp:paragraph -->
