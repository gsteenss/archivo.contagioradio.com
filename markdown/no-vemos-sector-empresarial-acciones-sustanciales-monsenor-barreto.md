Title: "No vemos al sector empresarial con acciones sustanciales en este momento": Monseñor Barreto
Date: 2020-03-29 18:46
Author: CtgAdm
Category: Actualidad, DDHH
Tags: Diocesis de Chocó, Empresarios, Quibdo
Slug: no-vemos-sector-empresarial-acciones-sustanciales-monsenor-barreto
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/03/maxresdefault.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: Monseñor Barreto/ Caritas Colombiana

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Para el obispo de Quibdó, Juan Carlos Barreto con la llegada del Covid-19 a Colombia, se debe exigir solidaridad en este momento de crisis para el país a las empresas que más ingresos han obtenido históricamente y al sector financiero con el fin de contribuir con el costo de la actual crisis que podría tener un costo de entre \$12 y 15 billones para la economía nacional.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

"No estamos viendo al sector empresarial con acciones sustanciales en este momento, por tanto siempre se recurrirá a los recursos del pueblo, creo que es importante que el país reciba una respuesta de los sectores más privilegiados a lo largo de la historia colombiana. [(Lea también: El Gobierno está del lado contrario de la ciudadanía: Arzobispo de Cali)](https://archivo.contagioradio.com/el-gobierno-esta-del-lado-contrario-de-la-ciudadania-arzobispo-de-cali/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Para Barreto, esta crisis está evidenciando otras crisis que pese a ser evidenciadas en múltiples ocasiones han permanecido ocultas por la agenda del país, "los ricos son cada vez más ricos y cada vez menos, es una realidad que se está constatando en todos los países, se acumulan las riquezas en pocas manos mientras gran parte de la población sufre un proceso de empobrecimiento" afirma el religioso [(Le puede interesar: Fondos de pensiones públicos son la mejor opción para los colombianos)](https://archivo.contagioradio.com/fondos-de-pensiones-publicos-son-la-mejor-opcion-para-los-colombianos/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Explica que en las tres últimas asambleas realizadas por la Diocesis de Quibdo se han abordado temas como la política, el ambiente y las problemáticas sociales del país, "la sociedad colombiana es muy desigual socialmente y eso requiere solidaridad por parte de sectores que tienen que aportar desde sus recursos."

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Aunque en los últimos 15 años Colombia el nivel de desigualdad del índice de Gini. ha descendido en 7.9 puntos, encontrándose en 2019 en 49.7, **Colombia continúa siendo el segundo país más desigual de América Latina, solo después de Brasil.** Resulta más alarmante que, como resalta el obispo, según los datos de la Supersociedades, las mil empresas más grandes del país han tenido ganancias exorbitantes en los últimos tres años que alcanzaron un tope de \$68 billones anuales en 2018 y que sumadas llegan a \$158 billones, una cifra de la que podría tomarse una parte para aportar al país.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

**A estos registros se suman los \$10 billones de impuestos de los que fueron exonerados los grandes empresarios en la más reciente reforma tributaria**, una medida que según el [Ministerio de Hacienda](https://twitter.com/MinHacienda) disminuiría el desempleo, sin embargo en Colombia para enero de 2020 se reportó un aumento de 39.000 nuevos desempleados.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### "Mientras no se resuelvan los problemas de los pobres, no se resolverán los problemas del mundo"

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Mientras desde redes sociales advierten que existen cadenas que tomarán la decisión de despedir a miles de empleados colombianos, algunos sectores como Bavaria han entregado 100.000 botellas de gel antibacterial y otros negocios como Arturo Calle, Cine Colombia y Crepes&Waffles que pese a cerrar de manera indefinida sus tiendas, continuarán pagando las nóminas de sus empleados. [(Lea también: Entre la cuarentena y la solidaridad)](https://archivo.contagioradio.com/entre-la-cuarentena-y-la-solidaridad/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Respecto a las medidas tomadas por el Gobierno, las que destinarían recursos a los bancos y empresas, monseñor Barreto expresó que algunas son paleativas pero en el contexto general, los recursos que se ofrecen "son muy pocos para las necesidades que tienen y van a tener muchas personas, “regalan algunos tapabocas, algunos mercados, pero no están apuntando a ser generosos ni a contribuir con soluciones de fondo, ellos que tienen la capacidad de hacerlo”. [("Los decretos no pueden ser solamente para los ricos": Aída Avella)](https://archivo.contagioradio.com/los-decretos-no-pueden-ser-solamente-para-los-ricos-aida-avella-2/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

El obispo concluyó que la comunidad religiosa de la costa Pacífica, siempre ha expresado que el conflicto armado es una de las causas del abandono estatal, y que falta un compromiso por parte del Estado para tener un enfoque de derechos de la población y crear un plan estructural que resuelva estos problemas, que en medio de la pandemia del Covid-19 son las más vulnerables. [(Lea también: Obispos del Pacífico piden a candidatos atender crisis humanitaria)](https://archivo.contagioradio.com/obispos-pacifico-candidatos-crisis-humanitaria/)

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
