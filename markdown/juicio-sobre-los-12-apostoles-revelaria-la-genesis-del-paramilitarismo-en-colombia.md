Title: Juicio sobre Los 12 Apóstoles revelaría la génesis del paramilitarismo en Colombia
Date: 2019-07-30 18:03
Author: CtgAdm
Category: Judicial, Nacional
Tags: Los 12 apostoles, paramilitarismo en Colombia, Santiago uribe
Slug: juicio-sobre-los-12-apostoles-revelaria-la-genesis-del-paramilitarismo-en-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/12-Apostoles.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: El Nuevo Siglo] 

En medio del proceso que se lleva en contra del ganadero Santiago Uribe por su posible participación en la conformación de Los 12 Apóstoles, que es el inicio del paramilitarismo en el país, previo a Las Convivir,  **el Tribunal de Antioquia ha dado validez al testimonio de Eunicio Pineda**, uno de los testigos claves que respaldaría las denuncias y destrabaría el juicio, al respecto, el abogado representante de las víctimas, Daniel Prado  señala que con esta decisión, se acerca el final de un caso que aportaría justicia y verdad al país y a las personas afectadas.

El fallo del Tribunal de Antioquia es respaldado por la opinión de los médicos psiquiatras de Medicina Legal quienes  concluyeron que Pineda, estaba **"en optimas condiciones para declarar"**, descartando un caso de esquizofrenia, tal como arguyo en su momento el abogado defensor de Uribe, Jaime Granados y enfatizando que "se trata de un caso de  estrés postraumático debido a las torturas a las que fue sometido", explica Prado.

Para Prado, **"la estrategia ha sido minimizar el número de pruebas que señalan a Santiago Uribe como responsable"** y aunque el Tribunal de Antioquia no da valor a las pruebas de Eunicio, pues no es de su competencia sí permite que se continúe con el proceso, lo que conduciría a los alegatos finales y finalmente al fallo o decisión final que tome el juez, "estamos próximos, es satisfactorio saber que un juicio tan importante como este está a punto de terminar", afirma.   [(Le puede interesar: Una vez más suspenden audiencia contra Santiago Uribe)](https://archivo.contagioradio.com/una-vez-mas-suspenden-audiencia-contra-santiago-uribe/)

Si bien el testimonio de Pineda es clave para resolver el origen de los 12 Apóstoles, el abogado aclara que desde 1996 hay personas que vienen declarando sobre la existencia del grupo paramilitar y la vinculación de Santiago Uribe, "otra cosa es que las autoridades no le hayan dado la importancia a estos testigos como la que tiene ahora".

<iframe src="https://www.youtube.com/embed/mhi96lHHOpQ" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

### "Cueste lo que cueste vamos a terminar el proceso"

Prado afirma que pese a que el riesgo es latente, por tratarse de un juicio en contra de una persona influyente como Santiago Uribe, existe "un compromiso con el país y que estos hechos no quedan en la impunidad, hemos asumido la como ciudadanos la posibilidad de que se cuente la historia".

Agrega que pese a las amenazas y presiones que existen en contra de su persona y las víctimas, solo queda seguir con el proceso tal como ha ocurrido con otros casos en los que se ha buscado ocultar la verdad o generar miedo en los testigos. [(Lea también: Defensa de Santiago Uribe es más mediática que jurídica: Daniel Prado)](https://archivo.contagioradio.com/defensa-de-santiago-uribe-es-mas-mediatica-que-juridica-daniel-prado/)

Con el final de este proceso tan cerca y a la espera de los alegatos finales, Prado sabe que la defensa de Uribe podría  acudir a otras acciones dilatorias para retrasar un dictamen. **"El perjuicio que se ha causado con estos delitos de lesa humanidad no solo ha afectado a las víctimas, cada uno de los ciudadanos de este país es víctima de estas conductas"**, concluye el abogado.

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>  
<iframe id="audio_39186593" frameborder="0" allowfullscreen scrolling="no" height="200" style="border:1px solid #EEE; box-sizing:border-box; width:100%;" src="https://co.ivoox.com/es/player_ej_39186593_4_1.html?c1=ff6600"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
