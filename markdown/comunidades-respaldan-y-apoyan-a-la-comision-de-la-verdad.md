Title: Organizaciones sociales respaldan y apoyan a la Comisión de la Verdad
Date: 2017-11-10 15:31
Category: DDHH, Nacional
Tags: comision de la verdad, Comite de Escogencia, Conpaz, paz
Slug: comunidades-respaldan-y-apoyan-a-la-comision-de-la-verdad
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/11/sin-olvido-700.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: CONPAZ] 

###### [10 Nov 2017] 

Las Comunidades Construyendo Paz en los Territorios (CONPAZ) en conjunto con más de 100 organizaciones sociales, **enviaron una carta a la recién creada Comisión de la Verdad** en la que manifiestan su respaldo para que las historias de violencia de los territorios contribuyan con “la reconciliación como equilibrio, como justicia socio ambiental y de género en un nuevo país”.

En la carta, las comunidades de diferentes territorios del país reconocen **el gran reto que tiene la Comisión** en estos tres años, pero son enfáticos en manifestar el apoyo a la contribución de la verdad “para reconocer el país que hemos hecho y él que podemos construir sobre la base de la verdad”.

Adicionalmente, invitaron a las y los colombianos a **convertir en diálogo constructivo todas las críticas** pues “estamos en una oportunidad para vernos en un espejo, lo que ha sido, ha sido así y es fundamental desmarañar los prejuicios, asumirlos cómo parte de una ceguera que nos ha impedido ver nuestra tragedia y asumir en ella la responsabilidad que nos compete, para cesar el odio y amar la vida sana, la vida en un bello existir de todas y todos”. (Le puede interesar: ["Comité de Escogencia abre posibilidad de opiniones ciudadanas a candidatos"](https://archivo.contagioradio.com/comite-de-escogencia-abre-posibilidad-de-opiniones-ciudadanas-a-candidatos/))

### **Comunidades han estado al frente del respaldo de procesos de construcción de paz** 

De acuerdo con Shirley Chimonja, integrante de la organización CONPAZ, las personas de las diferentes comunidades y de los territorios **son las que se han encargado de construir la paz en el país**. En este sentido, han sido las encargadas de respaldar procesos como las Circunscripciones Especiales de Paz y la consolidación de la Comisión de la Verdad.

Igualmente, dijo que para las organizaciones sociales la reconstrucción de la verdad significa un **proceso de memoria para evitar que los hechos atroces se repitan**. Por eso, recordó que de la mano de las víctimas van a aportar con esa labor de memoria histórica. Sin embargo, Chimonja recordó que esa labor ya la han venido haciendo las comunidades desde diferentes espacios.

Finalmente, ella insistió en que **“construir un mañana no significa que nos vamos a olvidar del ayer** más aún cuando se trata de historias tan tristes que han vivido nuestras comunidades”. Por lo tanto, indicó que a pesar de que las regiones se encuentran alejadas, están unidas para trabajar por el esclarecimiento de la verdad del país.

[carta Comité de escogencia](https://www.scribd.com/document/364068499/carta-Comite-de-escogencia#from_embed "View carta Comité de escogencia on Scribd") by [Anonymous 9gchRS](https://www.scribd.com/user/379499957/Anonymous-9gchRS#from_embed "View Anonymous 9gchRS's profile on Scribd") on Scribd

<iframe id="doc_67308" class="scribd_iframe_embed" title="carta Comité de escogencia" src="https://www.scribd.com/embeds/364068499/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-hq4bE6s3117I1b6AmfCV&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="0.7729220222793488"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 

<div class="osd-sms-wrapper" style="text-align: justify;">

</div>

 
