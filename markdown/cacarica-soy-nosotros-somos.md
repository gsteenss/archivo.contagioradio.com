Title: "Yo soy porque nosotros somos" festival deportivo por la hermandad en el territorio
Date: 2019-08-06 10:51
Author: CtgAdm
Category: Comunidad, Cultura
Tags: cacarica, Campeonato Intercultural por la Vida y el Territorio, Deporte, reclutamiento
Slug: cacarica-soy-nosotros-somos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/GAB0268.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/GAB0300.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/GAB0313.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio  
] 

Con el lema Ubuntu, o "Yo soy porque nosotros somos" se adelantó el Campeonato Intercultural por la Vida y el Territorio en la zona Humanitaria Nueva Vida en Cacarica, Chocó, entre el 12 y el 15 de Julio. Jóvenes del Bajo Atrato y el Urabá Antioqueño potenciaron la apropiación cultural y territorial para hacerle frente a las delicadas situaciones que se están viviendo en los territorios del Bajo Atrato.

Líderes de las comunidades que participaron del Campeonato explicaron las razones para ver en el deporte un vehículo para que los y las jóvenes se 'enraicen', y tengan opciones de vida distintas a las que actores violentos quieren imponer en sus territorios.

### **Un líder para su comunidad, una comunidad para su líder**

Según explicó uno de los líderes de Cacarica, el lema escogido "Yo soy, porque nosotros somos" se utilizó por los pueblos tribales africanos Zulú para entender la relación entre las comunidades: "Cuando lo digo, yo estoy incluido en el esquema (...) quiere decir que ustedes me dan esa fuerza para ser".

Distintos pensadores utilizaron este lema, como por ejemplo Nelson Mandela, con el que ideó un nuevo modelo de sociedad en Sudáfrica. Haciendo honor a ese pasado, desde el primer Festival de las Memorias en Cacarica se utilizó el término Ubuntu. En el deporte se puede encontrar un sentido de grupo similar: es la posibilidad de armonía y reconocimiento de las diferencias para alcanzar la complementariedad necesaria y el logro de objetivos.

### **El deporte, una excusa para encontrarse en opciones de vida digna**

\[caption id="attachment\_71472" align="aligncenter" width="1024"\]![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/GAB0268-1024x683.jpg){.size-large .wp-image-71472 width="1024" height="683"} Encuentro intercultural por la vida y el territorio en Cacarica\[/caption\]

De acuerdo al líder, el deporte sirve para generar diferentes cosas: recreación, inmersión en el arte y la cultura; y ello tiene relevancia "porque hoy los territorios están pasando por dificultades grandes", y los y las jóvenes "son la cuota más importante para darle cuerpo a la cobertura que quiere tomar en el territorio" la dinámica de la guerra, que persiste a pesar de la firma del Acuerdo de Paz.

Pero los deportes son más que una opción para mantener la mente ocupada; la estrategia es de largo plazo para que niños, niñas, adolescentes y jóvenes fortalezcan su identidad territorial y entiendan la importancia de asumir en su contexto el sentido del Ubuntu. (Le puede interesar:["Soy porque todos somos"](https://archivo.contagioradio.com/soy-porque-todos-somos/))

### **Deporte, apropiación cultural y territorial de los y las jóvenes**

\[caption id="attachment\_71474" align="aligncenter" width="1024"\]![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/GAB0300-1024x684.jpg){.size-large .wp-image-71474 width="1024" height="684"} Encuentro intercultural por la vida y el territorio en Cacarica\[/caption\]

Para otro de los líderes que asistió al encuentro, el deporte es una estrategia colectiva "que nos sirve para incentivar a los y las jóvenes a que se integren a la comunidad y pongan de presente los talentos que tienen", funciona como un espacio de encuentro y es "una ficha clave de trabajo que estamos desarrollando".

En ese sentido, el líder de Cacarica, señaló que a través del deporte "la niñez puede tener la capacidad de apropiarse y tener sentido de pertenencia por su territorio". El líder declaró que el objetivo del encuentro también era romper el paradigma del deporte sobre la competencia, poniendo de presente que siempre es más importante el trabajo en equipo.

Quienes participaban de los campeonatos de Voleibol, Fútbol y juegos de mesa, apoyados por Terre Des Hommes Alemania (TDH) y el gobierno alemán, tuvieron como trofeo la diversión y la hermandad. Sobre este aspecto, el líder de Cacarica dijo que desde las zonas humanitarias se ha forjado una estrategia para la verdadera paz de Colombia que pasa por impulsar valores como la verdad, la libertad, la justicia, la solidaridad y la fraternidad.

De allí, que aprender estos valores mediante los deportes les permitan entender que "yo soy porque nosotros somos hermandad y diversidad". (Le puede interesar:["JEP y comunidades hacen historia con primera audiencia en zona rural del Cacarica"](https://archivo.contagioradio.com/jep-comunidades-audiencia-cacarica/))

\[caption id="attachment\_71475" align="aligncenter" width="1024"\]![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/GAB0313-1024x683.jpg){.size-large .wp-image-71475 width="1024" height="683"} Encuentro intercultural por la vida y el territorio en Cacarica\[/caption\]

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
