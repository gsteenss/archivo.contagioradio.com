Title: Militares guatemaltecos serán procesados por desapariciones
Date: 2016-01-19 17:44
Category: El mundo, Judicial
Tags: Conflicto armado interno Guatemala, desapariciones forzadas guatemala, Juicio a militares Guatemala
Slug: militares-guatemaltecos-seran-procesados-por-desapariciones
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/01/intento-de-golpe-2-e1453243371479.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### [16 Ene 2016]

11 de los 14 militares en retiro, quienes entre 1978 y 1988 fueron miembros activos en la zona militar número 21, en Alta Verapaz, región norte de Guatemala, donde según la fiscalía fueron asesinadas y desaparecidas cientos de personas y cometidos otros crímenes de lesa humanidad, serán procesados penalmente por la justicia de ese país.

La resolucion impartida por la jueza Claudette Domínguez, titular del Juzgado "A" de mayor riesgo de Guatemala, alude a que la participación de los uniformados  se dió de forma "Sistemática", en acciones consideradas como crímenes de guerra, entre los que figuran, además de las desapariciones violaciones a mujeres y menores de edad.

Por los hechos sera procesado el general (r) Manuel Benedicto Lucas García y los oficiales (r) José Antonio Vásquez García, Carlos Augusto Garavito Morán, Raúl Dehesa Oliva, Gustavo Alonzo Rosales García, César Augusto Cabrera Mejía, Ismael Segura Abularach y Juan Ovalle Salazar. Además de Byron Humberto Barrientos Díaz, César Augusto Ruiz Morales y Luis Alberto Paredes Nájera.

En su argumentación, la jueza considero otorgar "falta de mérito" a las acusaciones sobre los militares Pablo Roberto Saucedo Mérida, Carlos Humberto Rodríguez López y Edgar Rolando Hernández, por incongruencias en las fechas en que estas se realizaron, situación que el Ministerio Público podrá enmendar en un lapso de tres meses, en los cuales deberán permanecer los uniformados bajo detención domiciliaria.

El caso es conocido como "CREOMPAZ", por ser la sede del Comando Regional de Entrenamiento de Operaciones de Mantenimiento de Paz donde se localizaron en 83 fosas comunes al menos 558 osamentas y restos humanos, de los cuales 97 han sido identificados.

Los procesados deberán continuar en prisión preventiva en la brigada militar Mariscal Zavala, de manera que no puedan perjudicar de alguna manera las investigaciones, por su parte  la jueza dió a la fiscalía un plazo de tres meses para que concluya la investigación y de esa manera determinar si se realiza un juicio oral o público.

##### Reciba toda la información de Contagio Radio en su correo <http://bit.ly/1nvAO4u> o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por Contagio Radio. 
