Title: La lucha invisible de las mujeres migrantes
Date: 2018-03-08 21:10
Category: Mujer
Tags: 8 de marzo, 8M, huelga de mujeres, mujeres migrantes, Venezuela
Slug: lucha_mujeres_migrantes_8m
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/03/Mujeres-migrantes-afrontan-discriminación-inseguridad-y-violencia.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: http://diarioevolucion.com.mx] 

###### [8 Mar 2018] 

Debido a la crisis social, política y económica que afronta Venezuela, la migración de venezolanos a Colombia se ha multiplicado dramáticamente entre 2016 y 2017: Para diciembre del año pasado, **Migración Colombia** **registró más de un millón trescientos mil personas que cruzaron la frontera, de las cuales la mitad son mujeres.**

Este panorama de crisis y ante las dificultades que los gobiernos ponen para que la población que busca salir de su país pueda hacerlo con garantías, genera unas problemáticas que vulneran los derechos humanos de las mujeres, cuando estas deben atravesar las fronteras de manera ilegal. Pero además, siempre que una persona es obligada a salir de su país se ve enfrentada a una serie de situaciones traumáticas, que  empeoran si se trata de mujeres.

"Nosotros decimos desde hace muchos años que el derecho de asilo es una carrera de obstáculos: **el primero es salir de tu país, el segundo es llegar a un país seguro, y el tercero es pedir la protección en un sistema jurídico que tiende a denegar la protección,** por unos parámetros de exclusión que hacen que sea casi imposible acceder a ese derecho", explica  Patricia Bárcena, directora del CEAR Euskadi, una situación que no solo se presenta en el caso de España, sino también ahora en Colombia.

En el caso de las venezolanas, muchas han decidido cruzar la frontera para recibir atención, dar a luz y criar a sus hijos en Colombia, o enviar dinero para que sus familias puedan vivir mejor en su país, sin embargo son múltiples las situaciones que deben atravesar las mujeres.

### **¿Qué significa migrar para una mujer?** 

De acuerdo con Cecilia Theme, doctora en estudios e investigaciones feministas y de género, las mujeres tienen mayores problemas para acceder a un estatus legal en los países, también les es más difícil acceder a un trabajo y aunque en algunos casos pueden hacerlo, se ven obligadas a desarrollar labores domésticas o de cuidado **en medio de precarias condiciones laborales. **

A su vez, el solo hecho de atravesar la frontera hacia otro país, de manera irregular, implica en muchos casos que **las mujeres sean víctimas de violencia sexual por parte de los hombres que, a su vez las explotan sexualmente en el trascurso de trayecto.**

### **Los logros de las mujeres migrantes** 

A pesar de todas las condiciones desfavorables que deben afrontar las mujeres, es imposible dejar de resaltar **el papel de ellas mujeres en la resolución de conflictos**, y su trabajo en la **lucha contra la impunidad** de crímenes de violencia sexual.

"Desde luego, las mujeres migrantes se organizan, luchan, pero además son el pilar y el sostenimiento de sus familias a través del envío de remesas", dice Theme, quien agrega que en esa medida las mujeres aportan a la economía del país de acogida.

De hecho, de acuerdo con ONU Mujeres, **las remesas que envían las mujeres migrantes mejoran los medios de vida y la salud de sus familias y fortalecen la economía.** En 2015, las y los migrantes internacionales enviaron a los países en desarrollo remesas por un total de 441 mil millones de dólares estadounidenses; lo que equivale a casi el triple de la asistencia oficial para el desarrollo, que suma un total de 131,6 mil millones de dólares.

### **La migración ha fortalecido el movimiento feminista en el mundo** 

Por otra parte, la experta indica que algo muy novedoso y revolucionario que está pasando, en el caso de España, es que **mujeres diversas han decidido unirse para reivindicar sus derechos y los de las comunidades de donde provienen**, de allí el impulso que ha cogido la iniciativa de la huelga de mujeres del 8M.

Cecilia Theme concluye en cada lugar hay diferentes voces de mujeres y diversas reivindicaciones, lo que ha fortalecido la lucha feminista. "**Las mujeres sostenemos el mundo, y aportamos con nuestro trabajo, remunerado y no remunerado, para que el mundo avance,** y las mujeres migrantes  hacen un trabajo de cuidados que hay que ponerlo en valor", dice.

Cabe resaltar que de acuerdo con la ONU, en todo el mundo las mujeres representan casi el 50% de los 244 millones de migrantes y la mitad de los 19,6 millones de personas refugiadas del mundo.

<iframe id="audio_24311983" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_24311983_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
