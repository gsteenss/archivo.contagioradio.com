Title: Comisión de la Verdad expuso sus avances tras un año de trabajo
Date: 2019-07-25 17:46
Author: CtgAdm
Category: Nacional, Paz
Tags: Casas de la Verdad, comision de la verdad, Diálogos para la No Repetición
Slug: comision-de-la-verdad-expuso-sus-avances-tras-un-ano-de-trabajo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/Presencia-en-territorio.png" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/Casas-de-la-Verdad.png" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/Comisión.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: ComisiónVerdadC] 

Este 25 de julio se realizó la primera rendición de cuentas de la Comisión para el Esclarecimiento de la Verdad,  el primero de un ejercicio que la entidad debe realizar cada seis meses para dar a conocer sus avances. En esta ocasión el trabajo iniciado desde el **28 de mayo del 2018 hasta el 31 de mayo del 2019** reveló el progreso realizado en 29 puntos del territorio colombiano junto a las víctimas y organizaciones que están dispuestas a compartir sus historias en pos de la verdad que se esconde tras el conflicto armado.

### Cifras concretas 

Para el 2019, el Ministerio de Hacienda asignó \$81.480 millones a la Comisión de la Verdad, de los que \$49.360 millones fueron destinados a su funcionamiento y \$32.120 millones para su inversión, lo que como fue advertido, representó una disminución del 40% frente a lo solicitado.

La reducción, según el informe final afectó no solo el número de trabajadores de la Comisión, sino que a su vez perjudicó su despliegue territorial  e "incrementó los desafíos institucionales y esfuerzos por lograr apoyo de la cooperación internacional".

\[caption id="attachment\_71184" align="alignleft" width="232"\]![Casa de la Verdad](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/Casas-de-la-Verdad-232x300.png){.size-medium .wp-image-71184 width="232" height="300"} Foto: Informe Comisión de la Verdad\[/caption\]

### Pese al presupuesto se avanza a grandes pasos

La rendición de cuentas, presidida por el padre Francisco de Roux y el secretario General de la Comisión, Mauricio Katz, quien también funge como representante legal y responsable de la ordenación del gasto de la entidad, reveló una serie de datos de entre los que destacan la apertura de **19 Casas de la Verdad en diferentes puntos del país, en las que cinco de ellas también trabaja la Unidad de Búsqueda de Personas dadas por Desaparecidas. **

Katz explicó además, que en el plazo de 6 meses se han celebrado 300 eventos de diferente naturaleza; talleres, foros y encuentros que han llegado a 150 municipios promoviendo la participación plural de las poblaciones indígena, afro, raizales y palenqueras.

El secretario general además resaltó que se ha hecho especial énfasis en las regiones para proteger los datos de las personas que han relatado su verdad, además se han puesto en marcha rutas de trabajo para fortalecer los equipos territoriales y sobretodo en la creación de confianza en la población

Estos 29 lugares en los que la Comisión hace presencia están repartidos a lo largo del país en la región Caribe e Insular, Costa Pacífica, Antioquia y Eje Cafetero, región Surandina, Bogotá, Magdalena Medio,  región Nororiente, Centroandina, Orinoquía, Amazonía, territorios de los Pueblos étnicos y a su vez con los colombianos y colombianas en el exilio.

\[caption id="attachment\_71181" align="aligncenter" width="677"\]![Comisión de la Verdad Presencia](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/Presencia-en-territorio.png){.wp-image-71181 .size-full width="677" height="587"} Foto: Informe de Gestión I Semestre de 2019\[/caption\]

El comisionado Carlos Berinstain, a cargo del trabajo que debe ser realizado en el exilio, afirma que en el ámbito internacional, se han realizado viajes a 16 países de Europa, Norteamérica, Sudamérica y Oceanía para estar en contacto con las víctimas y comenzar a formar equipos para la toma de más de 400 testimonios.

"Es la primera Comisión del Mundo que está trabajando en tantos países, eso no se ha hecho nunca, además también hemos encontrado mucha voluntariedad y ganas de colaborar de las víctimas".

### El mundo respalda la Comisión de la Verdad 

A propósito del trabajo en el extranjero y con la comunidad internacional y de las más recientes declaraciones del Consejo de Seguridad de la ONU, el cual celebró el inicio del trabajo de la Comisión,  también se destacó el apoyo de 105 aliados internacionales, 24 universidades del exterior, 62 entidades nacionales y 42 organizaciones de la sociedad civil

**Al respecto, el embajador de Suecia, Tommy Strömberg expresó que "hay una dedicación muy fuerte por parte de la Comisión de buscar esas verdades que son muchas, son distintas y ponerlas sobre la mesa para que el país sepa lo que ha pasado e ir hacia adelante"**, agregó.

Durante su ponencia, el padre Francisco de Roux, además de destacar la participación de las víctimas, resaltó la cooperación que han demostrado diversos miembros de la Fuerza Pública, exmiembros de las antiguas FARC  y exintegrantes de las Autodefensas Unidas de Colombia.

Por su parte, Ligia Caicedo, una de las víctimas que decidió acudir a la Casa de la Verdad en Tumaco, expresó, "hay personas que estamos convencidas que para que no se repita la historia tan trágica que ha sufrido Colombia y en especial nuestro Pacífico hay que contar la verdad", argumentando que la Comisión permite que exista un legado que trascienda generaciones.

Al evento también asistió la presidenta de la Jurisdicción Especial para la Paz (JEP), Patricia Linares, quien exaltó el avance de la entidad, "en tampoco tiempo con dificultades muy serias, entre ellas el tema de seguridad, principalmente de las víctimas y el tema presupuestal, no obstante eso el balance es positivo y esperanzador, creo que la Comisión está buscando la verdad donde debe buscarla". [(Lea también: Comisión de la Verdad sigue firme en su respaldo a la JEP)](https://archivo.contagioradio.com/comision-de-la-verdad-deja-en-firme-su-respaldo-a-la-jep/)

La Comisión de la Verdad cuenta con los siguientes dos años y cinco meses para seguir su trabajo que culminará en 2021 cuando se entregue un informe final que finalmente esclarezca las preguntas que se han planteado las víctimas y la sociedad acerca de los sucesos que acaecieron durante el conflicto armado.

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
