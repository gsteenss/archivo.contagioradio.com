Title: Piden suspender toque de queda en Buenaventura
Date: 2017-05-25 13:49
Category: DDHH, Movilización
Tags: buenaventura, Paro en buenaventura, Toque de queda
Slug: piden-suspender-toque-de-queda-en-buenaventura
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/05/marcha_7-e1495738166177.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: El Espectador] 

###### [25 may 2017] 

El toque de queda, impuesto por el alcalde de Buenaventura rige a partir de las 6 de la tarde hasta las 6 de la mañana en lo que va del paro y que buscaría mantener la seguridad y evitar que haya saqueos a locales comerciales. Sin embargo, **está sirviendo para evitar la movilización de los habitantes de la ciudad** y no estaría siendo aplicado a grandes negocios o vehículos de carga, según denunciaron los promotores del paro cívico.

Para Javier Torres, presidente de la Flota de Barcos de Cabotaje de Buenaventura, **“el toque de queda solo busca reprimir a la gente en horas nocturnas para que no se movilicen”**. Ante esta situación, le han pedido al alcalde que levante esta medida para poder realizar las actividades culturales que acompañan la protesta. Le puede interesar: ["Camioneros se unen al paro de Buenaventura"](https://archivo.contagioradio.com/camioneros-se-unen-al-paro-de-buenaventura/)

De igual manera, los bonaverenses han denunciado que los camiones de tráfico pesado, sí están autorizados para movilizarse en horas de la noche, lo que ha generado disgustos en la población. Torres afirmó que “**con estas acciones queda demostrado que a las autoridades lo único que les importa es que se mantenga el comercio y no se defienden los derechos de la población”**.Torres aseguró además que, es un toque de queda diseñado para reprimir a la población pero no a los grandes negocios.

### **Comité del Paro Cívico continúa reclamando la presencia del presidente Santos** 

Pese a que, con la visita del vice Ministro del Interior, Luis Ernesto Gómez Londoño, se avanzaron en temas que conciernen a la declaratoria de emergencia económica y social en Buenaventura, **los pobladores continúan reclamando la presencia del Presidente de la República en la mesa de negociación.** Le puede interesar: ["No levantaremos el paro si no se decreta la emergencia: Buenaventura"](https://archivo.contagioradio.com/no-levantaremos-el-paro-si-no-se-decreta-la-emergencia-buenaventura/)

La negativa del gobierno ante impedir que se declare la emergencia social y económica en Buenaventura **hizo que el diálogo con los pobladores se detuviera.** Según, Javier torres, “los insumos para que se declare la medida ya están y estos no los quiere reconocer el gobierno porque lo pueden sancionar”.

**Para la jornada de hoy, los bonaverenses convocaron a una marcha desde las 9:00 am y a un acto cultural a las 2:00 pm**. Ellos y ellas esperan que esta vez el Esmad no arremeta contra la población al tratarse de marchas multitudinarias. Torres le pidió a las autoridades “que hagan el acompañamiento respectivo para garantizar la seguridad de los marchantes sin cometer actos de represión contra ellos”. Le puede interesar: ["Buenaventura sigue en paro pese a represión, militarización y toque de queda"](https://archivo.contagioradio.com/buenaventura-sigue-en-paro-a-pesar-de-la-represion-la-militarizacion-y-el-toque-de-queda/)

De igual forma, Buenaventura ve con buenos ojos las expresiones de solidaridad que se han llevado a cabo en ciudades como Bogotá, Cali, Medellín y Pereira. Para Torres, **“estos actos de solidaridad nos demuestran que estamos luchando por algo justo y nos da fuerza para seguir con esta lucha”.**

<iframe id="audio_18900138" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_18900138_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU).
