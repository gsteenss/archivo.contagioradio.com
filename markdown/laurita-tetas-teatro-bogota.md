Title: Laurita y las tetas, una obra que habla del cáncer de mama
Date: 2018-01-24 16:07
Category: eventos
Tags: Bogotá, Cultura, teatro
Slug: laurita-tetas-teatro-bogota
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/01/IMG_7360_preview-e1516205393891.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto:Medios y Artes 

###### 20 Ene 2018 

Del  25 de enero al 10 de febrero llega al Teatro La Maldita vanidad de Bogotá "Laurita y Las tetas" una obra que aborda sin miedo, tabús o vergüenza y con una dosis de humor negro sobre el cáncer de mama, los creencias que giran en torno a esta enfermedad y sobre todo como cambia la relaciones de las mujeres que la padecen.

La pieza aborda las relaciones y personas que giran en torno a las mujeres víctimas de esta enfermedad, basada en el texto original del dramaturgo Juan Pablo Castro, con la dirección de Juan Luna y las actuaciones de Alexis Rojas, Iván Carvajal, Andrés Estrada y Camila Valenzuela, quien da vida a Laurita.

**Sinopsis:**

Laurita es una joven que  padece de cáncer de mama a causa de una herencia familia y su mundo se ha comenzado a desmoronar por cuenta de la enfermedad. Es carismática, bella, aguerrida e intrépida: lo ha dejado todo para instalarse en Bogotá junto a su novio, Mauro, un joven antropólogo.

Sin motivo aparente, un día la pareja ha despertado al otro lado del paraíso; la causa que termina por socavar toda ilusión de vida idealista es el cáncer de mama que ha sido diagnosticado a Laura, y cuya amenaza la obligará a someterse a una dolorosa cirugía. Mientras el mundo que la rodea se hunde, Laura se aferra a su antigua vitalidad a través del arte y sus encuentros con Mario, un antiguo amante y Robert, un habitante de la calle.

En escena, el ambiente realista de la historia de Laura se funde con la magia de la narración y el radio teatro, herramienta de la protagonista para entender su situación y expresar sus emociones, la cual lleva al público por una historia paralela llena de fantasía y misterio.

Laurita y Las Tetas gira en torno a la manera en que Laura busca establecer algún tipo de contacto, de comunicación y de entendimiento con los Otros, respecto a su situación. Pero la brecha entra ella y los demás personajes se acrecienta por la imposibilidad del universo masculino de comprender las necesidades y razones del universo femenino. (Le puede interesar: ¿[Que es el Cyberfeminismo](https://archivo.contagioradio.com/que_es_cyberfeminismo/)?)

**Laurita y las tetas**

   
Del 25 de enero al 10 de febrero  
Funciones: jueves a sábado a las 8:00 p.m.  
Bono de apoyo: \$30.000. \$20.000 estudiantes.  
50% Membresía Maldita Vanidad.  
Lugar: Cra 19 \# 45 A 17. Barrio Palermo, Bogotá.
