Title: Profesores del país se tomarán Bogotá de manera permanente
Date: 2015-04-30 15:43
Author: CtgAdm
Category: Movilización, Nacional
Tags: Asociación Distrital de Educadores, fecode, gina parody, Marcha de profesores, Paro Nacional, Profesores de Colombia
Slug: profesores-del-pais-se-tomaran-bogota-de-manera-permanente
Status: published

###### Foto: contagioradio.com 

<iframe src="http://www.ivoox.com/player_ek_4431594_2_1.html?data=lZmgk5qdeI6ZmKiakpmJd6KllZKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRlNPjx8rg0dfJt4zYxtGY0saJh5SZoqngjdjJb9Xjzsbfh6iXaaKlz5Cv0czTuIa3lIquk5DIqYzhwtPS1MaPtMbmzpKSmaiRh9Di1cbUy9SPlsLYytSYj4qbh46o&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Luis Alberto Grubert, presidente de Fecode] 

Para la próxima semana se tiene previsto que cerca de **50000 maestros y maestras del país se tomen Bogotá** con la instalación de campamentos en diversas partes de la ciudad pero que tendrán un sitio base en el Ministerio de Eduación. La tarde de este viernes 30 de Abril, iniciarían actividades con caravanas y marchas de antorchas en más de 400 municipios del país, indica **Luis Alberto Grubert**, presidente de la Federación Colombiana de Educadores, **FECODE.**

Respecto de la posible propuesta que presentaría la **Ministra de Eduación Gina Parody,** Grubert afirma que el gobierno no ha hecho ningún tipo de propuesta formal y lo que se ha conocido hasta el momento ha sido a través de los medios de información, o “rumores de prensa”.

Maestros y maestras de Colombia iniciaron un **Paro Nacional desde el pasado 20 de Abri**l, exigiendo el cumplimiento del pliego de exigencias, construido desde hace más de 3 años pero que ha tenido incumplimiento y dilación por parte del gobierno nacional.

**El pliego de exigencias del magisterio** incluye aumento salarial, mejoras en el servicio de salud para los maestros y maestras, garantías de seguridad para los profesores amenazados y un sistema de evaluación acorde con las necesidades de la educación en Colombia y que permita que los educadores asciendan en el escalafón.
