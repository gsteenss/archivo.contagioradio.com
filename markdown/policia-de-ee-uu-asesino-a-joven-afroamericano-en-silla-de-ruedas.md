Title: Policía de EE.UU asesinó a joven afroamericano en silla de ruedas
Date: 2015-09-28 17:18
Category: DDHH, El mundo
Tags: asesinaron a Jeremy Mcdole afro- estadounidense, Discriminación racial en EEUU, homicidio contra hombre negro, Michael Brown, racismo
Slug: policia-de-ee-uu-asesino-a-joven-afroamericano-en-silla-de-ruedas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/policias_disparan_a_silla_de_ruedas_0.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### zonacero.com 

###### [Sep 28 2015] 

Nuevamente agentes de policía de Estados Unidos asesinaron a un afroamericano. Se trata del joven de 28 años, **Jeremy Mcdole, quien estaba en silla de ruedas cuando fue baleado por tres agentes**  en una calle de Wilmington, Delaware.

Según las autoridades, los policías llegaron al lugar de los hechos porque habían denunciado que se estaban presentando disparos en la zona donde se encontraba Mcdole, quien se sintió intimidado al ver la presencia de la policía y fue ordenado a subir las manos, segundos después le dispararan causándole la muerte.

**La madre del joven asegura que su hijo estaba desarmado,** por lo que la Oficina de Derechos Civiles y de Justicia y Confianza Pública de Delaware investiga el hecho para determinar la responsabilidad de los policías. El hecho fue filmado por un transeúnte que pasaba por el lugar donde sucedieron los hechos.

A partir del asesinato del joven afroamericano en agosto de 2014 en Misouri, **se han evidenciado 10 muertes en varios estados de ese país,** lo que ha generando indignación de la comunidad negra.

El periódico The Times, denunció que el uso excesivo por la policía de este ingenio electrónico y cita la situación de North Charleston, Carolina del Sur, **donde los registros indican que de los mil 238 usos de la fuerza por parte de los oficiales entre 2010-2014,** se empleó el arma Taser 825 veces, especialmente contra afrodescendientes.

\[embed\]https://www.youtube.com/watch?v=L6GX6sXEOHU\[/embed\]
