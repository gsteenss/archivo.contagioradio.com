Title: Cancelación de semestre no detuvo la movilización de UniPamplona
Date: 2017-10-23 16:47
Category: Educación, Nacional
Slug: estudiantes-de-la-unipamplona-continuan-en-asamblea-permanente
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/03/Pamplona.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Mapio.net] 

###### [23 Oct 2017]

Los estudiantes del a Universidad de Pamplona afirmaron que se mantendrán en movilización, pese a la decisión de las directivas de suspender el semestre y afirmaron que durante este tiempo seguirán asistiendo al campus universitario y **realizando actividades de organización y formación de rechazo a la desfinanciación que vive el centro educativo.**

De acuerdo con Juliana Herrera, estudiante de Medicina de la UniPamplona, lo que se ha venido desarrollando en las asambleas estudiantiles son las agendas de movilización, generando un proceso de construcción constante y de propuesta a la crisis que afronta la universidad. Además, los estudiantes están exigiendo **garantías de participación en las jornadas de cese de actividades y que no haya una persecución académica**.

### **Las exigencias** 

En el pliego de exigencias los estudiantes solicitan el informe financiero de los años 2016, 2017 y 2018; **el estudio realizado para la adquisición de un crédito de 20 mil millones de pesos, hecho por la universidad y mejoras en la contratación docente**. (Le puede interesar:["No vamos a dejar en manos del olvido, la lucha estudiantil: Estudiantes UniPamplona"](https://archivo.contagioradio.com/decision-de-directivas-de-unipamplona-es-arbitraria-y-egoista-estudiantes/))

El **70% del presupuesto es autofinanciado, lo cual no es correcto en una universidad pública, solamente se sostiene en planta al 17.3% de docentes,** los demás son contratados por cátedra y no por la totalidad del semestre, además de que ingresar luego dos semanas del inicio de clases, razón por la cual, exigen que los miembros del Consejo Superior Universitario hagan el requerimiento al gobierno nacional de un aumento de la base presupuestal a la institución.

### **Viene un nuevo movimiento estudiantil nacional** 

Dentro de dos semanas se dará en Bogotá el Encuentro Distrital Ampliado de estudiantes, en donde se espera que lleguen delegados de la universidad de Pamplona y de otras instituciones públicas del país, para establecer un balance general de la crisis de la educación en Colombia. (Le puede interesar: ["Estudiantes de públicas y privadas listos para afrontar política de "Ser Pilo Paga"](https://archivo.contagioradio.com/48227/))

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
