Title: En San Martín, ConocoPhillips busca adelantar Fracking junto a policía y FFMM
Date: 2016-09-08 12:29
Author: AdminContagio
Category: Nacional, Resistencias
Tags: Ambiente, fracking, san martin
Slug: en-san-martin-conocophillips-intenta-realizar-fraking-en-compania-de-policia-y-ffmm
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/09/San-Martin.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: El País Vallenato 

###### [8 Sep 2016] 

En el municipio de San Martín, en Cesar la comunidad denuncia que la empresa petrolera **ConocoPhillips intenta ingresar a la fuerza al pozo Picoplata1 para iniciar los trabajos de exploración para Fracking  junto al Ejército y la Policía,** pese a la resistencia de los habitantes y los impactos evidentes que puede causar a la naturaleza el fracturamiento hidráulico.

“No vamos a negociar nada con la empresa, es innegociable la vida”, asegura Romario Torres, integrante de la Corporación en Defensa del Agua, Territorios y Ecosistemas, CORDATEC. La organización que ha liderado la lucha de los pobladores en esta región del país, donde el gobierno niega que se trate de actividades para adelantar fracking, mientras los habitantes aseguran que ** tienen las copias** **de cartas que demuestran que la Agencia Nacional de Hidrocarburos, ANH, ha dado permisos para iniciar esta actividad.**

De la mano de la policía y el Ejército, ConocoPhillips intenta intimidar a la comunidad que desde hace varios meses, viene resistiendo pacíficamente, como lo señala Torres, “Ayer en la zona había mucha fuerza pública con actitud amenazante contra la población, Intentado ingresar personal y maquinaria. **Llegaron pidiéndole papeles a la gente y diciendo que no los podían grabar”.**

Además desde la Corporación, aseguran que están [promoviendo la movilización social pacífica contra la llegada del Fracking](https://archivo.contagioradio.com/comunidad-de-san-martin-cesar-no-permitira-que-el-fracking-ingrese-a-colombia/), pues aunque han instado al gobierno a conversar, afirman que este ha violado los acuerdos de construir escenarios de diálogo en el marco de la Estrategia Territorial de Hidrocarburos, de ahí que no solo están protestando los habitantes de San Martín, sino también los de San Alberto y Aguachica. “**El gobierno da la posibilidad a la empresa de que se mueva por el territorio a sus anchas, sin consultarlo con la comunidad”.**

Cabe recordar que ya existen contratos firmados para más de **7 bloques petroleros para el fracking, y además hay más de 20 bloques ya asignados,** no solo en San Martín, Cesar, si no también Cundinamarca, Tolima, Boyacá, Santander, Meta, es decir que “no es solo la lucha de un municipio sino de todo el país en el que se necesita que se declare la moratoria”, como lo ha asegurado CORDATEC.

**“Si permitimos que haya fracturamiento hidráulico en San Martín esto se va a expandir como una plaga por todo el país”**, dice Romario Torres, quien añade que el próximo [25 de septiembre se realizará la tercera movilización](https://archivo.contagioradio.com/san-martin-se-alista-para-tercera-movilizacion-contra-el-fracking/) contra la actividad petrolera en ese municipio.

[![doc](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/09/doc-465x600.jpg){.aligncenter .wp-image-28956 .size-large width="465" height="600"}](https://archivo.contagioradio.com/en-san-martin-conocophillips-intenta-realizar-fraking-en-compania-de-policia-y-ffmm/doc/)

<iframe src="https://co.ivoox.com/es/player_ej_12829801_2_1.html?data=kpellJ6cdJKhhpywj5acaZS1kpWah5yncZOhhpywj5WRaZi3jpWah5yncbPjzsbfy9SPmNDm08rgh5enb6TDs6mutqqncYarpJKw0dPYpcjd0JC_w8nNs4yhhpywj5k.&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio [en su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio ](http://bit.ly/1ICYhVU) 
