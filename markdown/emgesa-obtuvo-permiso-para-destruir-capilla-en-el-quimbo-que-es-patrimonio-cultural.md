Title: EMGESA obtuvo permiso para destruir patrimonio cultural en el Quimbo
Date: 2015-08-10 12:40
Category: Cultura, DDHH, Nacional
Tags: Agencia Nacional de Licencias Ambientales, ANLA, ASOQUIMBO, Capilla San José de Belén, Diócesis de Garzón, EMGESA, Huila, Ley de cultura, Magistrado Aponte Pino, Miller Dussán, Quimbo, Tribunal Admministrativo del Huila
Slug: emgesa-obtuvo-permiso-para-destruir-capilla-en-el-quimbo-que-es-patrimonio-cultural
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/08/san-jose.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto:  [www.quimbo.com.co]

<iframe src="http://www.ivoox.com/player_ek_6452155_2_1.html?data=l5milJaZeY6ZmKiakpuJd6Kkl5KSmaiRdo6ZmKiakpKJe6ShkZKSmaiRia67priujdTGuNbq0JDdx9fRrdTjjNXO1MaPqMbn1dfiy9ePh8LkytHZw5DJsozZzZC%2B187Rpo6ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Miller Dussán, ASOQUIMBO] 

El Tribunal Administrativo del Huila autorizó la destrucción de la capilla San José de Belén, **según los términos establecidos por EMGESA,** empresa que lleva cabo el proyecto hidroeléctrico El Quimbo.

Frente a esta decisión, se prepara una **gran movilización de los Huilenses** para mostrar su rechazo y proteger la iglesia que hace parte del patrimonio cultural y religioso del país, así mismo la **Diócesis de Garzón aseguró que no va a entregar las llaves de la Capilla.**

Al Tribunal se le había solicitado una acción cautelar para proteger el patrimonio y para que se garantizara el traslado integral, como había quedado estipulado en la licencia ambiental y en el plan de manejo ambiental, sin embargo, aunque se debía haber realizado un estudio para hacer el traslado de la capilla, “**el Tribunal adecuó la medida cautelar de acuerdo con los intereses de la empresa**”, asegura Miller Dussán, investigador de ASOQUIMBO.

Dussán, asegura que la decisión se tomó con base en un estudio técnico y científico, realizado por la misma EMGESA, donde se concluía que no se podía trasladar integralmente la capilla, y por lo tanto **se debía destruir y construir una réplica.** Además, **EMGESA solicitó al tribunal que se permita la utilización de un cerrajero** que fuerce la chapa de la puerta, para sacar los muebles y luego demoler la capilla.

“Lo que hizo el tribunal fue **legalizar una saqueo que ya se había intentado por parte de una compañía contratada por  EMGESA…** Nos sentimos sorprendidos por esta decisión del **Magistrado Aponte Pino**, que acomodó la decisión a favor de la empresa”, expresa el investigador de ASOQUIMBO.

Según Dussán, ese dictamen se habría tomado en contra de la Agencia Nacional de Licencias Ambientales, ANLA y contra la Ley de cultura, donde se establece que ningún patrimonio se puede demoler, modificar o  transformar. Aun así,  se favoreció económicamente a la empresa ya que realizar la réplica solo le costaría 3 mil millones de peso, mientras que hacer el traslado integral le costaría  28 mil millones de pesos.
