Title: Vendedores informales protestan por decisiones de la Alcaldía Peñalosa
Date: 2016-02-01 12:06
Category: Economía, Entrevistas
Tags: Alcaldía de Bogotá, Enrique Peñalosa, vendedores ambulantes
Slug: vendedores-ambulantes-reprimidos-por-alcaldia-de-bogota
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/02/vendedores-ambulantes1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Contagio Radio 

<iframe src="http://www.ivoox.com/player_ek_10276384_2_1.html?data=kpWfmZuXfJWhhpywj5WUaZS1kZWah5yncZGhhpywj5WRaZi3jpWah5yncaLgxMbZxoqnd4a1pcaY1MrUtsrhxpDOjdvJssXZxdTfx9iPpc7W1tHO0NnJt4ztjNPcjdHJt4zYwpDUj4qbh4630NPhw8zNs4zGwsnW0ZCRaZi3jpk%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [1 Feb 2016]

En la mañana de este lunes, decenas de vendedores ambulantes de la capital, decidieron salir a marchar desde la Calle 72 con séptima hasta la Plaza de Bolívar, debido a los últimos operativos realizados por la Alcaldía de Bogotá, en los que **“se está haciendo uso de la fuerza para desalojar contra viento y marea, pegándole a todo el mundo y quitándole las cosas,** desacatando los fallos de la Corte Constitucional”, asegura Pedro Fonseca vendedor ambulante.

Desde hace 47 años, Pedro, es una de las cientos de personas que han decidido salir a las calles a vivir de las ventas ambulantes para mantener a su familia. Él afirma que “Enrique Peñalosa está ejerciendo abuso de autoridad como lo hizo en su primer mandato, ya que tiene en contra suya **un sentencia de la Corte Constitucional, que nunca se han cumplido”** y en la que dice que “se debe conciliar tanto los derechos de los vendedores como los derechos de las autoridades”.

De acuerdo con la sentencia de la Corte Constitucional, los vendedores que trabajan en las calles, no pueden ser desalojados mientras no exista soluciones puntuales para su situación, sin embargo, pese a que desde la Alcaldía se ha afirmado que si se han planteado opciones, muchos vendedores aseveran que **la reubicación prometida en puntos comerciales no se ha dado y lo único que han hecho es “engañarlos”.**

Pedro, asegura que las soluciones que se han ofrecido ya llevan 17 años, y no son garantía para los vendedores, es por ello, que piden se **abra una mesa de trabajo y concertación entre la Alcaldía y los vendedores ambulantes,** donde sean escuchadas sus propuestas, encaminadas a que exista kioscos destinados a este tipo de ventas y se pueda capacitar a los vendedores para fortalecer su negocio.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
