Title: Hay que parar “la ofensiva criminal contra la paz” Pastor Alape
Date: 2016-11-26 23:00
Category: Entrevistas, Paz
Slug: hay-que-parar-la-ofensiva-criminal-contra-la-paz-entrevista-con-pastor-alape
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/pastor-alape.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Delegación Paz Farc] 

###### [27 Nov 2016]

Pastor Alape, integrante del Secretariado de las FARC conversó con Contagio Radio sobre los retos de las FARC en adelante, la oleada de violencia y las alternativas que están planteando para superarla, la mentalidad guerrerista que sigue presente en nuestra sociedad, los planteamientos del uribismo y la configuración de la dirección para pasar de la guerra a la construcción de la paz

**Contagio Radio ¿Cuál es el reto que viene para las FARC, además de la pedagogía de los nuevos acuerdos?**

**Pastor Alape**: El gran reto es poder lograr que se sumen mayores apoyos y voluntades para implementar  este proceso, asegurar una reincorporación de todos los integrantes de la organización de manera expedita que permita garantías para la vida de todas las personas.

Hoy la gran preocupación es la ofensiva criminal contra la paz que se ha establecido, hoy nos llega otra preocupación con el atentado que le hacen a Marta Diaz en Barranquilla y despertamos ayer con el dolor de la muerte de otro líder en el valle.

Esta es una situación que necesariamente el Estado y el gobierno colombiano tiene que poner medidas concretas para poder parar este desangre que necesariamente va a afectar el proceso.

**¿Qué exigen las FARC frente a esa situación?**:

**Pastor Alape**: En reunión con el gobierno vamos a tratar el tema de la implementación de la comisión de garantías de seguridad y montar toda la estructura que de ella depende para poder desarrollar todas las actividades para garantizar a todas las organizaciones sociales a todos los partidos y a todos los movimientos su funcionamiento amplio en el proceso de fortalecimiento de la construcción de la paz.

**¿Qué opinión tiene las FARC frente a la mentalidad guerrerista que persiste en Colombia?**

Efectivamente  que las medidas militares y policiales no son las que van a resolver este problema,  aquí se requiere, decisión, voluntad política, medidas de tipo político, que todo el país y todas las fuerzas, políticas, sociales y económicas se comprometan con realmente con el proceso de paz.

Por supuesto que en este escenario que se abre, en este horizonte de construcción de paz tenemos que controvertir porque es la esencia de la democracia, pero hacerlo en condiciones de civilización y es a lo que estamos dispuestos.

Nosotros efectivamente que tendremos que hablar con todos los sectores políticos, es necesario reunir la comisión de todas las fuerzas políticas para buscar el pacto por la paz, pero no un pacto dirigido a desmontar todo lo que se ha construido en el acuerdo.

**¿Rodrigo Londoño hablo de un “gobierno de transición” puede desglosar la propuesta?**

Hay que clarificar que nuestro planteamiento es fortalecer los acuerdos, garantizar que el proceso de paz mantenga su curso para poder ampliar en todo el territorio esos acuerdos, que cada colombiano sienta que en realidad el país está cambiando, que la paz los está tocando, que los está abrazando, y para ello se requiere establecer una amplia coalición de fuerzas políticas que estén dispuestas a desarrollar, en el próximo gobierno, la implementación completa de lo acordado.

Es decir, **poder transitar del fin de la guerra a la construcción de la paz en Colombia**, y para eso se requiere un gobierno que esté comprometido con la paz, no es que estemos planteando que nosotros vamos a liderar el gobierno de transición, nosotros en este momento estamos empeñados es en que los acuerdos le lleguen a todos los colombianos y que esos acuerdos se manifiesten en hechos materiales.

**¿Quiénes serán los delegados al congreso?**

Necesitamos primero instalar la comisión de seguimiento e impulso a la implementación, la verificación y la resolución de diferencias, es necesario, en estas horas y días instalar el consejo nacional de reincorporación para establecer la organización política.

Establecido eso se nombrarían  las personas que nos representarían en el congreso, eso tiene que ser de manera rápida. Son ciudadanos en ejercicio, dice el acuerdo, es decir, que quedaríamos descartados dirigentes de la organización hasta que no se establezca o se decrete la ley de amnistía e indulto. El acuerdo es claro de que son ciudadanos en ejercicio.

**¿Consideran la amnistía como asunto indispensable para la movilización a las zonas veredales?**

Eso es lo que establece el acuerdo.

<iframe id="audio_14178830" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_14178830_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>
