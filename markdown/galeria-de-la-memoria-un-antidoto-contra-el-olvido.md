Title: Galería de la memoria: un antídoto contra el olvido
Date: 2015-07-31 17:22
Category: eventos, Nacional
Tags: DDHH, Galería de la memoria, MOVICE
Slug: galeria-de-la-memoria-un-antidoto-contra-el-olvido
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/07/mov.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### Foto: MOVICE 

##### [Luz Marina Hache, MOVICE] 

###### [31 jul 2015]

En homenaje a víctimas de desaparición forzada, el Movimiento Nacional de Víctimas de Crímenes de Estado, MOVICE, presentará al público la **"Galería de la memoria"** la tarde de hoy desde las 5:00 p.m en el Parque Santander, cra 7 con calle 16.

Como estrategia para mantener la memoria viva y en homenaje a Antonio Camacho, desaparecido el 16 de junio del 1965 en el Parque Santander, a Alirio de Jesús Pedraza Becerra, y con la intención de **recordar la masacre de Mapiripán, que cumple 18 años de llevarse a cabo el  17 y 20 de julio del 1997,** la organización defensora de derechos humanos pretende dar a conocer a la opinión pública lo que han padecido estas víctimas del Estado.

"Con esta galería la opinión pública conocerá una realidad que ha querido ser ocultada porque las victimas de crímenes del Estado siempre hemos sido invisibilizadas. **La galería sirve como un mensaje a la comunidad para que conozca la verdad que tenemos las víctimas".** Afirma Luz Marina Hache, miembro del MOVICE.

Esta actividad, que se ha presentado por años consecutivos en diferentes departamentos del país. Esta vez contará además con la exposición de pinturas de Antonio Rugeler, encontradas 30 años después, además contará con música latinoamericana en vivo.
