Title: Corte Constitucional llama a rendir cuentas a instituciones encargadas del Sistema Carcelario
Date: 2018-09-27 16:23
Author: AdminContagio
Category: DDHH, Nacional
Tags: cárcel la tramacua, Corte Constitucional, crisis carcelaria, La Tramacua
Slug: corte-constitucional-llama-a-rendir-cuentas-a-instituciones-encargadas-del-sistema-carcelario
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/06/Carcel.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo Partícular] 

###### [27 Sept 2018] 

La Corte Constitucional convocó a una audiencia a las diferentes instituciones encargadas del sistema penitenciario de Colombia, para hacer seguimiento al estado de cosas inconstitucionales que generan la crisis carcelaria en el país, que de acuerdo con el último informe realizado por organizaciones defensoras de derechos humanos**, no ha tenido mayor avance en garantía de los derechos humanos a la población reclusa.**

El documento revela puntualmente la realidad en la que se encuentran las personas recluidas en la Cárcel de Valledupar, o La Tramacúa, en donde según la abogada Gloria Silva, del Equipo Jurídico Pueblos, "a pesar de que desde la Corte ya se habían emitido unas órdenes claras a distintas autoridades, **son lamentables las condiciones de agua, del expendio de alimentos y la salud, que sigue siendo precaria**" en ese lugar.

Además, expresó que también se logró investigar el tratamiento penitenciario en el que se evidenciaron prácticas y tratos degradantes, **e incluso actos de humillación que podrían constituir, en algunas ocasiones, acciones de tortura en contra de los presos**.

### **¿Quién controla a las instituciones que hacen parte del Sistema Carcelario?** 

Silva explicó que el estado de cosas incosntitucionales se declaró en las cárceles del país, por la masiva violación a los derechos humanos de la población carcelaria, razón por la cual las **vulneraciones son de carácter estructural y dependen de un conjunto de instituciones gubernamentales. **(Le puede interesar:["Cárcel La Picota retira dieta vital a preso con enfermedad terminal"](https://archivo.contagioradio.com/carcel-la-picota-retira-dieta-vital-a-persona-con-enfermedad-terminal/))

En esa medida, aseveró que existe una falta de coordinación entre las autoridades que ha desembocado en situaciones tan críticas como la inversión de recursos en obras mal hechas o la asignación de la alimentación a privados, que según las denuncias de los presos, **llevan las comidas en estados de descomposición o con gramajes inferiores por los que han sido contratados.**

### **Urge un cambio en la visión de Justicia en Colombia**

La abogada señaló que este informe también hace un análisis sobre la justicia actual que se imparte en el país y las iniciativas que han salido desde entidades como la Fiscalía frente a la política criminal, debido a que ninguna de ellas busca superar el estado de cosas incostitucionales, sino que por el contrario las profundiza a través del endurecimiento de penas o la restricción de beneficios, **que han llevado al encarcelamiento masivo de la población colombiana y la prolongación de la privación de la libertad.**

La audiencia se llevará a cabo el próximo 25 de octubre y según Silva, se espera que desde este espacio **"se presiones al Estado y a todas sus autoridades para que las acciones que debe realizar sean coordinadas"** además, señaló que deben desarrollarse planes de trabajo que tengan objetivos claros junto con indicadores de goce efectivo de derechos humanos.

Finalmente la abogada hizo un llamado para que durante este espacio se convoque a la población reclusa y sus familiares que son quienes diariamente son víctimas del sistema carcelario.

<iframe id="audio_28939133" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_28939133_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
