Title: No hay freno a los asesinatos de líderes sociales en el país
Date: 2018-04-10 15:08
Category: DDHH, Nacional
Tags: asesinatos de líderes sociales, Chocó, defensores de derechos humanos, Guaviare, lideres sociales
Slug: continuan-los-asesinatos-de-lideres-sociales-en-el-pais
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/02/lideres-sociales.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [10 Abr 2018] 

Desde la Agencia de Renovación del Territorio, se denunció el asesinato del líder social **Yobany Velasco Ariza** en San José del Guaviare. De igual forma, fue asesinado **Wilson Arnulfo Quetama**, integrante de la Asociación de Víctimas en el Palmar Chocó. Solamente en el Chocó han sido asesinados 6 líderes sociales en 2018.

### **Yobany Velasco apoyaba el programa Desarrollo Territorial** 

De acuerdo con la información de la Agencia, Velasco “representaba a su comunidad en el Grupo Motor, como la ruta de construcción de los **Programas de Desarrollo con Enfoque Territorial**”. Esto en el marco del desarrollo de ese programa en 170 municipios que han sido afectados por el conflicto armado.

Por esto, han pedido a las autoridades que se **refuercen las medidas de seguridad** para los líderes sociales que “vienen trabajando a favor de la renovación de los territorios en la búsqueda de la paz”. Recuerda la Agencia la importancia de la labor de los líderes sociales para la construcción de la paz en Colombia. (Le puede interesar:["Valores de los líderes sociales son retratados en una serie documental"](https://archivo.contagioradio.com/valores_lideres_sociales_serie_documental/))

### **En Chocó no cesan los asesinatos de defensores de derechos humanos** 

La muerte del líder Wilson Arnulfo Quetama fue confirmada por las autoridades quienes manifestaron que se **desempeñaba como el coordinador de la mesa de participación** de las comunidades entre 2016 y 2017. Además, era representante de las víctimas para la reparación colectiva de las comunidades de San José del Palmar en Chocó.

Los defensores de derechos humanos de esta región ya habían reportado el asesinato de **Claudio Chávez** durante el primer fin de semana del mes de abril. Con el asesinato de Quetama, se confirma el aumento en la violencia contra los líderes sociales de ese departamento que registra 6 defensores asesinados en 2018.

###### Reciba toda la información de Contagio Radio en [[su correo]

<div class="osd-sms-wrapper">

</div>
