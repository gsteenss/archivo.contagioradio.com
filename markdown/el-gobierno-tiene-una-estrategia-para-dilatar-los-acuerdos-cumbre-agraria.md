Title: El gobierno tiene una estrategia para dilatar los acuerdos: Cumbre Agraria
Date: 2015-04-29 12:00
Author: CtgAdm
Category: Movilización, Nacional
Tags: ANZORC, Cumbre Agraria, Juan Manuel Santos, Ministerio de Agricultura, Ministro del Interior, Paro Agrario en Colombia
Slug: el-gobierno-tiene-una-estrategia-para-dilatar-los-acuerdos-cumbre-agraria
Status: published

###### Foto: contagioradio.com 

En una carta dirigida al Ministro del Interior y un informe enviado al **Ministerio de Agricultura** las organizaciones que integran la **Cumbre Agraria** afirman que el gobierno está en saldo rojo, en cuanto al cumplimiento de los acuerdos a los que se llegó para darle final al Paro Agrario de 2013 y 2014, y tampoco ha cumplido los acuerdos a los que se han llegado en el mediano plazo.

\[embed\]https://www.youtube.com/watch?v=npaZEzzrfNs\[/embed\]

Cesar Jeréz, vocero de **ANZORC**, integrante de la Cumbre Agraria, afirma que el gobierno tiene una estrategia de dilación que se basa en la toma de decisiones en la mesa, que luego deben ser consultadas con las demás comisiones del gobierno lo cual retrasa la firma. De igual manera los requerimientos técnicos como estudios o evaluaciones que el gobierno pide a las organizaciones son otras de las estrategias que se usan para alargar la firma y el cumplimiento de los acuerdos.

En la misiva la Cumbre Agraria solicitó una reunión con el presidente Juan Manuel Santos en la que se presentará el informe de evaluación que evidencia incumplimiento en todos los aspectos de la Mesa Única de Concertación como Derechos Humanos, Comunidades Afrodescendientes, Bloque Económico en el que no se ha podido iniciar ninguno de los 233 proyectos presentados y aprobados en actas.

Esta es la evaluación de la Cumbre Agraria

[Evaluacion Cumbre Agraria Contagio Radio](https://es.scribd.com/doc/263545177/Evaluacion-Cumbre-Agraria-Contagio-Radio "View Evaluacion Cumbre Agraria Contagio Radio on Scribd")

<iframe id="doc_17391" class="scribd_iframe_embed" src="https://www.scribd.com/embeds/263545177/content?start_page=1&amp;view_mode=scroll&amp;show_recommendations=true" width="60%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="undefined"></iframe>
