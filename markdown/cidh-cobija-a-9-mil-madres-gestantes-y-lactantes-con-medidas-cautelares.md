Title: CIDH cobija a 9 mil mujeres wayúu con medidas cautelares
Date: 2017-02-07 14:59
Category: DDHH, Nacional
Tags: CIDH, indígenas, La Guajira, medidas cautelares, Wayuu
Slug: cidh-cobija-a-9-mil-madres-gestantes-y-lactantes-con-medidas-cautelares
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/02/familia-wayuu-e1486498253343.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Viva FM Villanueva] 

###### [7 Ene. 2017] 

**“El Estado colombiano debe proteger y garantizar el derecho a la vida e integridad personal de las madres gestantes y lactantes de la etnia Wayúu** ubicadas en los municipios Manaure, Riohacha y Uribía” así lo definió la Comisión Interamericana de Derechos Humanos -CIDH- que cobijó a este grupo con medidas cautelares manifestando que cerca de 9 mil mujeres “están en una situación de gravedad y urgencia”.

Esta decisión, es una ampliación de las medidas cautelares de las cuales ya eran beneficiarios los niños y niñas de La Guajira, cuyo objetivo es garantizar la vida e integridad personal. Le puede interesar: [Robos, golpes e insultos de ESMAD a indígenas Wayúu en Guajira](https://archivo.contagioradio.com/robos-golpes-e-insultos-de-esmad-a-indigenas-wayuu-en-guajira/)

Las medidas cautelares otorgadas a las madres gestantes y lactantes, se da luego de la petición realizada a la CIDH por la abogada y directora del Consultorio Jurídico de la UJTL, Carolina Sáchica Moreno y el líder indígena Wayúu Javier Rojas por considerar que **la vida de 9 mil mujeres en estado de gestación está en alto riesgo debido a su nivel de desnutrición y falta de acceso a salud.**

“Lograr estas medidas cautelares para la Comunidad indígena Wayúu abandonada a su suerte, fue un triunfo jurídico enorme, así como una vergüenza nacional que fuera necesaria la intervención de una instancia internacional que requiriera al Estado para algo tan obvio como lo es **la protección de la vida de esta población vulnerable que muere de hambre y sed**” dijo Sáchica en un artículo publicado en la Universidad Jorge Tadeo Lozano.

En la medida la CIDH ordena al Estado Colombiano garantizar a las mujeres Wayúu acceso a salud, atención médica con enfoque integral y cultural adecuado, acceso a agua potable, alimentos de calidad y en general todo lo requerido para cubrir las necesidades de estas 9 mil madres. Le puede interesar: [Amenazan a Jackeline Romero lideresa Wayúu](https://archivo.contagioradio.com/amenazan-jackeline-romero-lideresa-wayuu/)

La Asociación Shipia Wayúu celebró a través de su red social en Twitter esta decisión de la CIDH y agradecieron el “apoyo invaluable” que han recibido de diversas organizaciones para cobijar a las mujeres gestantes con las medidas cautelares.

> [\#Atención](https://twitter.com/hashtag/Atenci%C3%B3n?src=hash) [@CIDH](https://twitter.com/CIDH) concedió [\#MedidasCautelares](https://twitter.com/hashtag/MedidasCautelares?src=hash) para madres Gestantes y Lactantes de nuestras [\#ComWayuu](https://twitter.com/hashtag/ComWayuu?src=hash). Gracias a todos por su invaluable apoyo [pic.twitter.com/YtNO1w3plW](https://t.co/YtNO1w3plW)
>
> — Asociac Shipia Wayuu (@shipiawayuu) [6 de febrero de 2017](https://twitter.com/shipiawayuu/status/828605713391968256)

<p>
<script src="//platform.twitter.com/widgets.js" async charset="utf-8"></script>
</p>
Sin embargo, cabe recordar que esta etnia ya contaba con medidas cautelares de la CIDH desde el 11 de diciembre de 2015, y pese a ello, más de un año después, un número superior a los 100 niños han muerto en La Guajira.

Dentro de los diversos problemas que afronta La Guajira se encuentra la corrupción, desvío de recursos, presencia de minería con empresas como el Cerrejón, desvío de arroyos y fuentes hídricas, así como el abandono estatal. Le puede interesar: [13 Razones para no desviar el Arroyo Bruno](https://archivo.contagioradio.com/13-razones-para-no-desviar-el-arroyo-bruno/)

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)

<div class="ssba ssba-wrap">

</div>
