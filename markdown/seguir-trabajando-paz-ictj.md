Title: Es momento para seguir trabajando insistentemente por la paz: ICTJ
Date: 2019-08-30 19:07
Author: CtgAdm
Category: DDHH, Paz
Tags: conflicto, ICTJ, marquez, paz
Slug: seguir-trabajando-paz-ictj
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/08/paz-colombia.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio  
] 

Un día después del anuncio hecho por Iván Márquez y Jesús Santrich de querer iniciar una nueva guerrilla, la doctora **Camila Moreno, directora del Centro Internacional para la Justicia Transicional (ICTJ, por sus siglas en inglés)**, explicó la posición que se debe asumir luego de estas declaraciones, e invitó a hacer un análisis sosegado sobre el contexto que vive el país en este momento. (Le puede interesar: ["La paz no la detiene nadie, afirman excombatientes de FARC"](https://archivo.contagioradio.com/la-paz-no-la-detiene-nadie-afirman-excombatientes-de-farc/))

### **Nuestro deber ético con el país es seguir insistiendo en la paz: Directora ICTJ  
** 

Tras el anunció hecho por los exdirigentes de las FARC, Moreno afirmó que la sensación que muchos han sentido entre ayer y hoy es "como si se nos escapara entre los dedos la esperanza de la paz"; y reconoció que aunque el anuncio era algo que se sabía que podía pasar, era un "golpe duro ver cómo se materializa la idea de este nuevo levantamiento en armas". (Le puede interesar:["Es momento de redoblar los esfuerzos por la paz: Iván Cepeda"](https://archivo.contagioradio.com/redoblar-esfuerzos-paz/))

Sin embargo, rescató que esta situación debe invitar a la sociedad a seguir trabajando insistentemente por una paz completa, como un esfuerzo para las futuras generaciones. En ese sentido, el compromiso consiste en sentar las bases para un país mejor, por lo que concluyó que "acá no hay posibilidades de desfallecer, y **nuestro deber ético con el país es seguir insistiendo en la paz**". (Le puede interesar: ["Gobierno también es responsable del regreso a las armas de Iván Márquez"](https://archivo.contagioradio.com/gobierno-tambien-es-responsable-del-regreso-a-las-armas-de-ivan-marquez/))

### **De la tristeza, al análisis de la situación** 

La directora del ICTJ admitió que también ella siente desesperanza, "porque la imagen es profundamente dolorosa para este país, es como si no visualizaramos un futuro donde el debate político no esté atravesado por las armas", pero declaró que la desesperanza siempre estará "como la espada de Damocles: pendiendo sobre nosotros". No obstante, afirmó que ese sentimiento debería renovar el compromiso de quienes han defendido la paz por años, y también es la oportunidad de hacer la reflexión que el país actual reclama.

En consecuencia, aseguró que es momento de dejar pasar las emociones y evaluar responsabilidades, porque no se puede culpar únicamente a quienes decidieron empuñar las armas, "que por supuesto tienen su parte de responsabilidad", pero también se debe exigir el cumplimiento de lo acordado en La Habana. "Tenemos que enviarle el mensaje a quienes creen que la lucha armada, para decirles que el pais quiere otra cosa, pero también **tenemos que generar las condiciones para que eso sea posible**", puntualizó.

### **¿Qué opinar de las razones mencionadas por Márquez para volver a las armas?** 

Moreno resaltó que hay una realidad, "y es que ha habido serios problemas en la implementación del Acuerdo", y recalcó que es un error reducir este debate únicamente al proceso de reincorporación, pues el Acuerdo es de carácter inescindible y se debe implementar de forma integral. Por consiguiente, **una tarea para quienes deseen la paz es insistir en que la sostenibilidad de la paz depende de una implementación integral del Acuerdo.**

Adicionalmente, la experta sostuvo que el anunció de Márquez pone en evidencia los factores que explican que en este país, alzarse en armas siga siendo un camino para las personas que quieren lograr transformaciones; por lo tanto, **"ahora más que nunca, necesitamos una reflexión profunda sobre esos factores de persistencia del conflicto".** (Le puede interesar: ["El Acuerdo es mucho más grande que unas personas que se apartan"](https://archivo.contagioradio.com/acuerdo-grande-personas-apartan/))

### **Los factores que permiten la persistencia del conflicto** 

La directora del ICTJ comentó que esos factores son los mismo que pretende transformar, o iniciar su transformación, el Acuerdo de La Habana: el problema de la concentración de la tierra, el narcotráfico, la participación política, la inequidad en la distribución de la riqueza. También añadió que parte de la responsabilidad en la violencia la tienen las élites políticas y económicas del país, "que han sido muy mezquinas, y que solamente están para defender sus propios intereses ".

"Cuando uno ve el vídeo de Iván Márquez y compañia, más allá de la lucha armada y de las formas, **él está señalando que existen y siguen existiendo razones que explican el conflicto, y que incluso explican -no justifican- su decisión",** enfatizó Moreno. (Le puede interesar: ["Seguiremos cumpliendo con nuestra función y mandato: JEP"](https://archivo.contagioradio.com/seguiremos-cumpliendo-con-nuestra-funcion-y-mandato-jep/))

### **El papel que juegan las instituciones del Sistema Integral de Verdad, Justicia, Reparación y No Repetición** 

Para hacer esa reflexión, Moreno resaltó la labor que realizará la Comisión para el Esclarecimiento de la Verdad (CEV), como una forma de aportar a entender qué es lo que ha pasado en más de 50 años de conflicto armado. En cuanto al trabajo de la Jurisdicción Especial para la Paz (JEP), la directora del ICTJ calificó el pronunciamiento hecho por su directora sobre Santrich y Márquez como contundente e importante, puesto que aunque el país demanda una decisión sobre los excombatientes que regresaron a las armas, dicha decisión debe tomarse en derecho y la JEP ha mostrado que quiere hacerlo con celeridad.

<iframe id="audio_40652311" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_40652311_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>**Síguenos en Facebook:**  
<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
