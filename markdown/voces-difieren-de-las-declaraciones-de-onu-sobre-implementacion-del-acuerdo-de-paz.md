Title: Voces difieren de las declaraciones de ONU sobre implementación del acuerdo de paz
Date: 2020-01-10 11:37
Author: CtgAdm
Category: Entrevistas, Paz
Tags: acuerdo de paz, Duque, Implementación, ONU
Slug: voces-difieren-de-las-declaraciones-de-onu-sobre-implementacion-del-acuerdo-de-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/01/Análisis-de-implementación-de-la-paz-e1578605192157.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: @MisionONUCol] 

El pasado miércoles 8 de enero el jefe de la **Misión de Verificación de la ONU en Colombia, Carlos Ruiz Massieu**, realizó una rueda de prensa en conjunto con el presidente Iván Duque en la que destacaba los avances en materia de implementación del Acuerdo de Paz en los últimos 15 meses, al tiempo que señalaba algunos de los desafíos. Entre los desafíos destacó la falta de condiciones de seguridad para excombatientes, defensores de derechos humanos y comunidades, mientras señaló que había avances en la reincorporación, el desarrollo de los Planes de Desarrollo con Enfoque Territorial (PDET) y el Sistema Integral de Verdad, Justicia, Reparación y No Repetición (SIVJRNR).

### **La declaración de la Misión de Verificación**

En su declaración, Ruiz Massieu expresó que la Misión analizaba de forma positiva la reincorporación de los excombatientes en términos de los proyectos productivos que han emprendido, y superan los 2.500 tomando en cuenta los proyectos individuales. Destacó la ruta de reincorporación recientemente aprobada, así como el avance en 15 de las 23 medidas para mejorar las condiciones de seguridad para excombatientes, aunque reconoció que 2019 fue "un año complejo en ese tema".

De igual forma, el Jefe de la Misión de Verificación de la ONU sostuvo que encontraron avances en los PDET y en el trabajo del SIVJRNR. En conclusión, Ruiz sostuvo que la implementación del Acuerdo de Paz ha avanzado en estos 15 meses de forma integral, y destacó el trabajo de la Comisión Nacional de Garantías de Seguridad, pese a que reconoció que habían 'grandes desafíos' en términos de la seguridad de excombatientes, defensores de derechos humanos y comunidades.

### **¿Qué piensan los excombatientes sobre la implementación?**

**William Betancur,** integrante de la organización del partido Fuerza Alternativa Revolucionaria del Común (FARC) en Meta, sostuvo que se siente como un "parte de victoria" por parte de la Misión pero ellos creen que hay elementos de jucio para opinar diferente en relación a lo que tiene que ver con el tema de tierras, seguridad para los excombatientes y su participación en política.  Adicionalmente, Betancur cuestionó la forma en que se adelanta la indemnización a las víctimas por parte del Gobierno.

Respecto al avance en medidas de protección señaladas por Ruiz, Betancur criticó que las mismas desconocen el enfoque de seguridad preventiva, poniendo énfasis en el aumento de pie de fueerza que "muestra lo equivocado en esa política de paz con legalidad". En cambio, sostuvo que los territorios donde se vive la violencia requieren "que se desmonte el paramilitarismo y sus grupos sucesores", eliminar las prácticas de corrupción y avanzar en garantías de derechos.

Sobre la Comisión Nacional de Garantías de Seguridad, el excombatiente aseguró que si bien era un espacio de articulación en el cual se podían hacer denuncias, "es un fracaso", y muestra de ello son las cifras de asesinatos de líderes sociales y excombatientes. (Le puede interesar: ["Colombia inicia el año con aumento de violencia contra líderes sociales"](https://archivo.contagioradio.com/colombia-inicia-el-ano-con-aumento-de-violencia-contra-lideres-sociales/))

En cuanto a la ruta de reincorporación recientemente aprobada, Betancur sostuvo que antes se tenían en cuenta las opiniones de FARC y de organismos internacionales, pero las decisiones ahora se toman de forma unilateral, "lo que ha hecho que tengamos muchos vacíos, eso sí, reconociendo que ha habido grandes avances, entre ellos, en algunos proyectos productivos".

Sin embargo, sostuvo que no sienten el apoyo para los proyectos por parte del Estado, y en su lugar, "lo que se ha visto es una capacidad de gestión de los excombatientes con la comunidad internacional que sí le ha apostado a la implementación del Acuerdo".  En ese mismo sentido, aseguró que ningún Espacio Territorial de Capacitación y Reincorporación (ETCR) cuente con la propiedad de la tierra, razón por la que los proyectos productivos que basan su economía en el uso de la tierra se quedan sin 'piso' económico.

Por último, Betancur aseguró que FARC sí apropió los recursos para indeminzar a las víctimas pero aún no se ha aclarado por qué el Gobierno no ha empezado a ejecutar estos recursos, o cómo se realizará esta indeminzación. (Le puede interesar:["Excombatientes de Farc bajo ataque en el Cauca"](https://archivo.contagioradio.com/excombatientes-de-farc-bajo-ataque-en-el-cauca/))

### **Una implementación a medias: La opinión de los campesinos**

José David Ortega, vocero de la Asociación de Campesinos del Sur de Córdoba (ASCSUCOR), sostuvo que en el Departamento la materialización de los "PDET no se ve por ningún lado", puesto que los Planes no empezaron a funcionar y ahora los municipios PDET se están viendo transformados en Zonas Estratégicas de Intervención Integral, que concentran la acción del Estado en acciones de seguridad.

Ortega también sostuvo que hay incumplimientos en el Plan Nacional Integral de Sustitución (PNIS), porque la gente confió y erradicó los cultivos pero "ahora están aguantando hambre". Esto, según explica, porque los proyectos productivos que reemplazarían a los cultivos no se desarrollaron, la asistencia técnica llegó a medias o en algunas zonas no llegó y tampoco tienen vías terciarias para sacar los cultivos que producen.

Por último, respecto a las condiciones de seguridad en la zona y el funcionamiento de la Comisión Nacional de Garantías de Seguridad, el líder campesino puso en duda su operatividad en Córdoba, al recordar que allí se tuvo que hacer una audiencia pública luego del asesinato de Maria del Pilar Hurtado y el de otras personas. Asimismo, dijo que en 2019 grupos paramilitares se dejaron ver con armas largas en San José de Uré, un hecho que no se veía hace años, y que habla sobre el crecimiento que estan teniendo estos grupos.

### **"En este Gobierno estamos asistiendo a lo que se ha definido como la tendencia a la simulación" **

El profesor e integrante de Voces de Paz, Jairo Estrada, explicó que la implementación está en un "estado crítico y de precariedad, que este Gobierno ha acentuado", agregando que estamos frente a una constante tendencia de incumplimiento respecto a lo acordado, lo que explicaría algunos de los problemas en términos de garantía para los derechos humanos y acceso a la tierra. No obstante, la manifestación de la **Misión de Naciones Unidas** para el Profesor se podría explicar en que este organismo hace énfasis en los elementos del proceso que le interesan: Desarme, Desmovilización y Reinserción (DDR) de las FARC, sin embargo, recordó que este proceso de paz es integral, y va más allá de esos puntos.

> La “Paz con legalidad” representa una simplificación grosera del  
> Acuerdo de paz y tiene como propósito acomodarlo a los fines del gobierno. [@IvanDuque](https://twitter.com/IvanDuque?ref_src=twsrc%5Etfw) debe implementar integralmente el acuerdo y la [@MisionONUCol](https://twitter.com/MisionONUCol?ref_src=twsrc%5Etfw) debe tener mayor rigor en su ponderación<https://t.co/DbQNl515XY> [pic.twitter.com/xrCnqCdisQ](https://t.co/xrCnqCdisQ)
>
> — FARC (@PartidoFARC) [January 9, 2020](https://twitter.com/PartidoFARC/status/1215387046086422531?ref_src=twsrc%5Etfw)

<p>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
</p>
En ese sentido, sostuvo que "lo que se trata de poner de presente es que en este Gobierno estamos asistiendo a lo que se ha definido como la tendencia a la simulación", es decir, que se implementa parte de lo que le interesa al Gobierno que esta consignado en el Acuerdo de Paz y cabe dentro de su política de Paz con Legalidad, pero se desconocen otras partes igualmente fundamentales del Acuerdo. Estrada señaló que esta declaración genera preocupación, en tanto tiene consecuencias internacionales las afirmaciones que una institución como ONU haga sobre la implementación del Acuerdo, pero recordó que habría que esperar la presentación del informe de la Misión, que será entregado la próxima semana en Nueva York (Estados Unidos) ante el Consejo de Seguridad.

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
