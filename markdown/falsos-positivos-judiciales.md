Title: ¿Falsos positivos judiciales una forma de persecución al Movimiento Social?
Date: 2018-07-10 12:43
Category: Expreso Libertad
Tags: congreso de los pueblos, Falsos Positivos Judiciales, presos politicos
Slug: falsos-positivos-judiciales
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/07/falsos-positivos-judiciales.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/04/falsos-positivos-judiciales-e1524507146917.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/acsn1.png" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Patria Grande Int 

###### 19 Jun 2018 

El pasado 6 de junio Julian Gil, secretario técnico de la plataforma política y social Congreso de los Pueblos, fue capturado a las afueras de las instalaciones de esta plataforma, acusado de ser participar en un hecho ocurrido el 20 de julio de 2017, en Vianí, Cundinamarca.

Sin embargo, de acuerdo con Congreso de los Pueblos, no solo no hay pruebas suficientes en contra de Julian para culparlo, sino que se suma a los más de 40 integrantes de esta organización que han sido capturados en los últimos dos años.

Para Blandine Jux, integrante de esa plataforma, estas capturas se han convertido en una estrategia por parte de los gobiernos para intentar desmovilizar al Movimiento Social y fracturar procesos de defensa de derechos humanos en el país.

No obstante, según Jux, aún queda mucho por hacer en el país y confían en que prontamente los líderes sociales que son víctimas de falsos positivos judiciales queden en libertad.

<iframe id="audio_26972982" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_26972982_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Encuentre más información sobre Prisioneros políticos en[ Expreso libertad ](https://archivo.contagioradio.com/programas/expreso-libertad/)y escúchelo los martes a partir de las 11 a.m. en [Contagio Radio](https://archivo.contagioradio.com/alaire.html) 
