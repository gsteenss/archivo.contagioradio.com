Title: Con venta de ISAGEN Estado dejaría de recibir 225 mil millones de pesos anuales
Date: 2015-05-15 13:25
Author: CtgAdm
Category: Economía, Entrevistas
Tags: Banco de la República, bonos de deuda, Consejo de Estado, déficit fiscal, Gobierno Nacional Cedetrabajo, Hugo Bastidas, ISAGEN, Mario Valencia, sostenibilidad fiscal, venga de Isagen
Slug: con-venta-de-isagen-estado-dejaria-de-recibir-225-mil-millones-de-pesos-anuales
Status: published

###### Foto: [inteligenciapetrolera.com.co]

###### [15 de May 2015]

<iframe src="http://www.ivoox.com/player_ek_4501739_2_1.html?data=lZqdk5yXfY6ZmKiakp6Jd6KpmZKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRaZOmt8rb1saPqMafqriuqaqyb8jZz8rfw9eJh5SZoqnOjc3ZqcTjjMvW1cjFsIzZz5DZw9iPqsriwtPnw5KJe6ShpNTb1sbLrdCfs8bRy9SPcYarpJKh&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Mario Valencia, Centro de Estudios del Trabajo, Cedetrabajo] 

Este jueves, nuevamente el **Consejo de Estado frenó la venta de la empresa ISAGEN**, que estaba prevista para el próximo martes, y dictaminó medidas cautelares mínimo durante 15 días, mientras se soluciona de fondo la demanda que busca defender el patrimonio del país.

“Lo que hace el Consejo de Estado es proteger el patrimonio y por ello pone las medidas cautelares”, afirma Mario Valencia, Subdirector y analista del Centro de Estudios del Trabajo, Cedetrabajo, una de las organizaciones que demandó la venta de ISAGEN.

De acuerdo a Valencia, el afán del gobierno en vender ISAGEN es por las **concesiones de las vías 4G que tienen un costo cerca  a los  40 billones de pesos** y con las que el gobierno se ha comprometido con una serie de alianzas publico privadas, teniendo en cuenta que el vicepresidente Germán Vargas Lleras busca recursos para inaugurar proyectos de infraestructura.

Las demandas han girado en torno a la **inconveniencia económica  patrimonial** de vender la empresa, y las **violaciones de parte del gobierno** a la hora de planificar indebidamente la subasta de ISAGEN.

Por otro lado, según señala el analista, el **gobierno tenía que cumplir con la Ley de Sostenibilidad Fiscal,** es decir, justificar qué va a pasar con las ganancias que recibía el Estado de parte de ISAGEN, teniendo en cuenta que s**e dejaría de recibir aproximadamente 225 mil millones de pesos al año,** punto que el gobierno no ha logrado aclarar y aun no se sabe cómo se podría recuperar nuevamente esos ingresos, lo que generaría **“un hueco en las finanzas de la nación”**, indica Mario Valencia.

**De 102 senadores 80 están en contra de la venta de ISAGEN,** también, la Procuraduría  y la Contraloría. Esta última demostró mediante un informe que los recursos que se van invertir en las vías 4G serían muy difícil recuperar, y en cambio ese flujo de caja solo servirá para el mantenimiento y operación de las vías, es decir, que “es una plata que se va a enterrar en las carreteras”, expresa Valencia, resaltando que **“un buen negocio no es salir de un activo para comprar otro, es algo absurdo”.**

Cedetrabajo al igual que otras organizaciones que demandaron esta acción del gobierno, ha presentado diferentes propuestas para la financiación de las 4G, ya que la venta de ISAGEN no solo representa un déficit fiscal, sino además la pérdida de una de las mejores empresas  de América Latina que además es la fuente de energía de Colombia.

Entre las alternativas, el analista de Cedetrabajo destaca que en se podría **gestionar créditos para el gobierno nacional aprovechando la buena rentabilidad de la ISAGEN; **también se pueden **gestionar bonos de deuda del Estado** con el apoyo del Banco de la República.

Otra de las propuestas, es que se haga una **emisión directa del Banco de la República**, recordando que esta opción está respaldada constitucionalmente, y se lograría recoger fácilmente los recursos.

**“El costo hacia futuro de vender ISAGEN es muy alto,** es la energía de los colombianos, ese precio es superior a los costos financieros”, asegura el analista.

Valencia asevera que el **gobierno no escucha los argumentos e insiste tercamente** en la venta de ISAGEN, es por eso que organizaciones como Cedetrabajo, celebran la decisión de la Sección Cuarta del Consejo de Estado, con la ponencia del consejero Hugo Bastidas.
