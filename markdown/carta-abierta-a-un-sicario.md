Title: Carta abierta a un sicario
Date: 2018-04-27 10:52
Category: invitado, Opinion
Tags: buenaventura, Lideres, temistocles
Slug: carta-abierta-a-un-sicario
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/04/Temístocles-1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

#### Foto:Elizabeth Otálvaro/Pacifísta 

#### **Por: Cristian Rivera Machado** 

[En nuestro hermoso país aproximadamente cada 5 días asesinan a un líder social. Según la Organización Global Witness, somos el segundo país del mundo en donde más asesinan a defensores y defensoras del medio ambiente. Estamos en el top cuatro de los países más letales para los defensores de Derechos Humanos según Amnistía Internacional y esto es en gran parte gracias a usted, y a quienes pagan su salario.]

Muchas veces he soñado con usted. Usted es la pesadilla de cientos de colombianos, la razón por la cual las comunidades se ven obligadas a desplazarse de sus hogares, el miedo de los niños cuando juegan en los parques y en las calles y la razón por la cual muchos compatriotas dudan en regresar a nuestro país. Por esto las parejas piensan sobre traer hijos a este mundo habitado por personas como usted y sus colegas.

Nunca pensé en escribirle, sin embargo, tengo la necesidad de decirle que ya es suficiente de su accionar en Colombia y en el mundo. Nos hemos cansado de sus asesinatos que acallan a los que hablan por las comunidades, a los que dicen lo que piensan, a los que anhelan un país mejor, a nuestros miles de líderes y lideresas sociales que han muerto defendiendo nuestros territorios, nuestro medio ambiente y nuestras comunidades.

Hace ya tres meses, asesinó a Temístocles Machado “Don Temis”, gran líder de Buenaventura que luchó toda una vida por los derechos de los habitantes de la comuna seis, del mal llamado barrio “Isla de la Paz”. Supongo que había estudiado y seguido sus movimientos y hasta tal vez ya tenía una fecha para asesinarlo. Llegado el día del siniestro, lo esperó en el parqueadero donde Don Temis se ganaba la vida honestamente y cobardemente acabó con él, sin compasión, sin siquiera pensar en porqué tenía que matarlo, simplemente obedeciendo órdenes sin chistar, como un perro entrenado para dar la mano o para sentarse.

He pensado mucho en su trabajo, en cómo es un día a día de un sicario. ¿Tiene familia y con el sudor de su frente compra el sustento? ¿Qué le enseña a sus hijos? Algo así como mijo “para matar a un líder social se necesita dispararle cuando esté indefenso, solo, con dos disparos en el pecho y uno en la cabeza”. ¿Llega en la noche a dormir con su mujer y le dice: “hoy me fue muy bien en el trabajo”? ¿Cómo cobra? ¿Pide un anticipo y luego el resto después del dictamen de medicina legal? ¿Tiene seguridad social y plan de pensiones? ¿Su jefe le pone metas y objetivos? ¿Vacaciones colectivas?

Seguramente usted es una víctima más. Tal vez fue abusado cuando era pequeño, discriminado, marginado, rechazado, creció en un hogar disfuncional o simplemente no tuvo muchas oportunidades. Lo siento si fue así. Sin embargo, por ningún motivo pienso justificar sus crímenes y [atrocidades, pues existen muchas personas en el mundo que han pasado por situaciones similares y no han tomado el camino de la sangre y la muerte.]

Seguramente si lee esta carta, se preguntará ¿y éste porque me escribe a mí? Una amiga me dijo, “no pierdas el tiempo que esa gente tiene podrida el alma”. ¿Será que está en lo cierto? ¿Algo de humanidad queda dentro de usted?

Quizás debería escribirle a sus jefes que pueden ser paramilitares, empresarios, guerrilleros, políticos, miembros oscuros del estado, o una mezcla de ellos. Pero sabe usted que ellos ya tienen mucha atención, todos los días los medios de comunicación les regalan páginas y minutos. Pero en cambio usted es invisible, nadie lo quiere, nadie lo extraña, nadie sabe de usted, sólo es importante a la hora de hacer el trabajo sucio, a la hora de matar... Debe ser triste eso...

Tengo todavía muchas preguntas, ¿Cuánto dinero gana? ¿Cuánto cobra por asesinar a alguien? Apuesto a que no gana mucho, no tiene casa ni quizás familia, seguro vive en un cuarto oscuro y húmedo y ni los perros se le acercan.

En cambio, las personas a quienes mata son seres humanos que probablemente tampoco tienen mucho dinero. Es más, pueden ser sus vecinos, un tío o un amigo. Son en general personas humildes de comunidades marginadas que defienden a sus vecinos y familias en contra de proyectos públicos o privados que vulneran sus derechos, contaminan nuestro medio ambiente o usurpan sus tierras. En otras palabras, fácilmente pueden estar peleando por sus derechos y los de su familia.

Por otro lado, creo que debes informarle a sus dirigentes (si es que no se han dado cuenta), que la estrategia de matar a los líderes no está funcionando. Pese a que en estos últimos años usted ha tenido bastante trabajo, los líderes no paran y no van a detener su lucha. La estrategia del miedo ya no funciona como antes, las comunidades se empoderan más, se reinventan en medio del dolor por [la pérdida de sus líderes y siguen adelante con sus ideas firmes y contundentes. La estrategia no está funcionando, están matando líderes para crear mártires.]

Creo que en el contexto del posconfilcto usted tiene cabida, todavía está a tiempo de salir de ese cuarto oscuro y de las noches con pesadillas en donde todavía siente el olor a pólvora y a sangre del día laboral. Todavía tiene algo de chance de entregarse, hablar sobre sus jefes y pagar sus crímenes. Quizás así encontrará algo de redención, algo de paz, algo de porvenir para sus años venideros o para la otra vida. De pronto así podrá ver la vida de una manera diferente, podrá disfrutar de un atardecer, comer un helado y hasta tener una familia, podría ayudarnos a construir un mundo mejor.

[Sin embargo, si insiste y siente que tiene ganas de matar y de acabar con alguna vida, intente hacerlo con la suya y de golpe así se acaba el sufrimiento.]
