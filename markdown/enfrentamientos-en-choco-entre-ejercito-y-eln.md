Title: Comunidades denuncian afectaciones en Chocó por enfrentamientos Ejército y ELN
Date: 2017-09-25 12:20
Category: Uncategorized
Tags: Chocó, ELN, Fuerza Pública, Gobierno, Jiguamiandó
Slug: enfrentamientos-en-choco-entre-ejercito-y-eln
Status: published

###### [Foto: Cablenoticias] 

###### [25 Sept. 2017]

Este sábado 23 de septiembre durante 1 hora se escucharon **bombardeos y ráfagas de fusil cerca de las Zonas Humanitarias de Nueva Esperanza** y la comunidad de Santa Fe de Churima en el departamento del Chocó, así lo ha denunciado la Comisión Intereclesial de Justicia y Paz gracias a testimonios de las comunidades que aseguraron se trataría de enfrentamientos entre el Ejército y el ELN.

Dichos estallidos por los enfrentamientos continuaron escuchándose durante todo el domingo completando en total, 11 explosiones. Le puede interesar: [Control paramilitar se afianza en Bajo Atrato y Cauca](https://archivo.contagioradio.com/control-paramilitar-se-afianza-atrato-cauca/)

“Los **enfrentamientos en el Jiguamiandó ponen en riesgo la vida, integridad y permanencia** en el territorio de las comunidades afro, indígenas y campesinas que ancestralmente han habitado estos territorios” aseveran en la comunicación.

Hace dos semanas esta organización denunció que se presentaron enfrentamientos entre las autodenominadas Autodefensas Gaitanistas de Colombia y el ELN dentro del territorio colectivo de Jiguamiandó. Le puede interesar: [Enfrentamientos entre paramilitares y ELN afectan comunidades en Chocó](https://archivo.contagioradio.com/46439/)

Hasta el momento y luego de las reiteradas denuncias **no se ha conocido una respuesta por parte del Estado**, por lo que evidencian la ineficacia de la Fuerza Pública y la incapacidad del conjunto del Estado para garantizar el derecho a la vida en los territorios.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)
