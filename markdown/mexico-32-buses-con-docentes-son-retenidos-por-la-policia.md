Title: México: 32 buses con docentes son retenidos por la policía
Date: 2016-06-16 18:42
Category: El mundo, Movilización
Tags: mexico, protestas, reforma educativa
Slug: mexico-32-buses-con-docentes-son-retenidos-por-la-policia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/06/mexico.jpg_1718483346.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Telesur 

###### [16 Jun 2016] 

**Cerca de dos meses llevan los profesores, alumnos y padres de familia en movilizaciones en México,** oponiéndose a la reforma educativa que afectaría la calidad de la educación  de los mexicanos, pero además las condiciones laborales de los docentes, que durante este jueves fueron víctimas de la represión por parte del gobierno de Enrique Peña Nieto.

Este jueves policías de la Ciudad de México detuvieron **32 buses donde se transportaban más de mil maestros de la Coordinadora Nacional de Trabajadores de la Educación** en Chiapas, cuando intentaban llegar a la capital para continuar con las movilizaciones exigiendo respuestas del gobierno a sus reclamaciones.

De acuerdo con Santiago Navarro, periodista mexicano de la Red Avispa en Oaxaca, la represión se ha agudizado y la presencia policial es cada vez mayor.  Sin embargo, pese al hostigamiento de la fuerza pública, cada vez es más fuerte la movilización en estados como Oaxaca donde la ciudad está prácticamente paralizada y miles de alumnos y padres siguen uniéndose a la jornada, aunque **el gobierno y el congreso sigan negando la posibilidad de algún tipo de diálogo.**

Según denuncian, la reforma educativa se trataría más bien de una reforma laboral dictada por las reglamentaciones de la OCDE, “**una reforma laboral que desdibuja conquistas  laborales de los educadores y trabajadores de la educación”**, es decir, una privatización de la educación donde los padres deberían financiar las necesidades básicas de las instituciones, y que además, generaría una tercerización en la contratación de los docentes, quienes perderían varios derechos laborales.

Se conoce que hasta el momento **cerca de 4 mil profesores que se movilizan han sido despedidos**, pues desde las instituciones se amenaza a los docentes, y se les dice que si cumplen más de 4 días en jornada de protestas, los maestros pierden su trabajo.

<iframe src="http://co.ivoox.com/es/player_ej_11930518_2_1.html?data=kpamlZWZdZmhhpywj5aZaZS1kpmah5yncZOhhpywj5WRaZi3jpWah5yncbTVz9nWw8zTb6_V18bf1NSPcYzGxsmYo9vNt9HVjoqkpZKns8_owszW0ZC2pcXd0JCah5yncZU.&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
