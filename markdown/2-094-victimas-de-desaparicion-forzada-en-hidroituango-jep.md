Title: 2.094 personas fueron víctimas de desaparición forzada en el área de influencia de Hidroituango: JEP
Date: 2020-12-10 20:17
Author: AdminContagio
Category: Actualidad, Nacional
Tags: Desaparición forzada, Hidroituango, JEP
Slug: 2-094-victimas-de-desaparicion-forzada-en-hidroituango-jep
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/12/2.094-victimas-de-desaparicion-forzada-en-Hidroituango-JEP.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

El controvertido proyecto Hidroituango continúa en el centro de la polémica, esta vez por cuenta de un informe publicado por la Jurisdicción Especial para la Paz -[JEP](https://www.jep.gov.co/Paginas/Inicio.aspx)-. Apenas una semana después de que la Contraloría General anunciara la imputación de cerca de 20 exfuncionarios por el detrimento patrimonial de 4,1 billones de pesos de dicho proyecto; la JEP dio a conocer **que 2.094 personas fueron víctimas de desaparición forzada en el área de influencia de la represa.** (Lea también: [Imputación de responsables en el caso Hidroituango un avance para las víctimas](https://archivo.contagioradio.com/imputacion-de-responsables-en-el-caso-hidroituango-un-avance-para-las-victimas/))

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/JEP_Colombia/status/1336716550146052100","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/JEP\_Colombia/status/1336716550146052100

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

Según detalló la JEP, un total de** **2.094 personas fueron víctimas de desaparición forzada en los municipios que hacen parte del área de influencia de Hidroituango: **Briceño, Cáceres, Ituango, Nechí, Peque, Sabanalarga, Tarazá, Toledo y Valdivia en el departamento de Antioquia;** así lo estableció la Sección de Ausencia de Reconocimiento de dicha Jurisdicción. (Le puede interesar: [JEP abre incidente contra gerente de EPM](https://archivo.contagioradio.com/jep-abre-incidente-contra-gerente-de-epm/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Las desapariciones forzadas habrían sido ejecutadas por **grupos paramilitares (Bloque Mineros y Bloque Metro), los frentes 18, 36 y 5 de las FARC-EP y por la Fuerza Pública.  
**  
Los datos recogidos permiten constatar que este crimen tuvo **un incremento importante a partir del primer semestre de 1996, con la irrupción de estructuras paramilitares en Antioquia,** en especial en los municipios de Cáceres y Tarazá los cuales concentran cerca del 50 % de las  víctimas. Asimismo, el punto de mayor incidencia se dio durante los años 2001 y 2002, periodo en el que fueron desaparecidos(as) 358 ciudadanos(as).

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

**Adicionalmente, la JEP dio a conocer que a la fecha ha hallado ocho (8) cuerpos con signos de muerte violenta de personas no identificadas, posibles <abbr title="víctimas: Son aquellos que, individual o colectivamente, sufrieron daños como consecuencia de las acciones u omisiones presentadas en el marco del conflicto armado.">víctimas de desaparición forzada, provenientes del Cementerio de Orobajo,</abbr>** municipio de Sabanalarga, zona inundada por la represa, los cuales se encontraban en el Laboratorio de Osteología de la Universidad de Antioquia.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Según la JEP en la última inspección forense fueron hallados cinco (5) nuevos cuerpos que se suman a los tres (3) encontrados durante el primer semestre de 2020;  frente a estos cuerpos la JEP se encuentra investigando posibles irregularidades en su traslado y manejo como **«*presuntas alteraciones perimortem compatibles con mecanismo por proyectil de arma de fuego*».** (Le puede interesar: [Universidad de Antioquia estaría evitando respuesta de fondo a Ríos Vivos Antioquia](https://archivo.contagioradio.com/universidad-de-antioquia-estaria-evitando-respuesta-de-fondo-a-rios-vivos-antioquia/))

<!-- /wp:paragraph -->

<!-- wp:quote -->

> “Las víctimas presentan heridas con arma de fuego como posible causa de muerte, disparos en el cráneo compatibles con ejecución sumaria y en estado de indefensión”
>
> <cite>Jurisdicción Especial para la Paz Auto AT-195-20.</cite>

<!-- /wp:quote -->

<!-- wp:paragraph -->

Estos ocho cuerpos se suman a los 176 exhumados a lo largo de los últimos años por la Fiscalía General de la Nación en los municipios del área de influencia del proyecto de Hidroituango.

<!-- /wp:paragraph -->

<!-- wp:quote -->

> "En esta región este gravísimo crimen** **fue masivo, generalizado y persistente, al mismo tiempo que, sin duda, lesionóen grado sumo a las comunidades"
>
> <cite>Jurisdicción Especial para la Paz Auto AT-195-20.</cite>

<!-- /wp:quote -->

<!-- wp:paragraph -->

De acuerdo con la JEP, ya se avanza en la práctica de pruebas para contrastar la información allegada en relación con la existencia de lugares formales e informales  de inhumación en el área de influencia del proyecto Hidroituango, entre las que se encuentran una inspección aérea y fluvial a las zonas inmediatas afectadas por el proyecto.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/wilsonariasc/status/1336858499054235654","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/wilsonariasc/status/1336858499054235654

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

Estos hallazgos se dieron a partir de una base de datos construida por la JEP en un ejercicio de consolidación de información de seis fuentes oficiales entre las cuales se encuentran el Centro Nacional de Memoria Histórica; la Fiscalía General de la Nación; la Unidad de Atención a Víctimas –UARIV-; y la Comisión de Búsqueda de Personas Desaparecidas -CBPD-.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Las labores se adelantaron en el marco del trámite de medidas cautelares de protección de lugares de inhumación de víctimas de desaparición forzada en los municipios del área de influencia de Hidroituango, con las cuales la JEP busca proteger estos sitios y establecer la verdad plena sobre lo ocurrido en esta zona del país. 

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
