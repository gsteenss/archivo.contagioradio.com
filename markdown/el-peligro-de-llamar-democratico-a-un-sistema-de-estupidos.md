Title: El peligro de llamar democrático a un sistema de estúpidos
Date: 2015-07-10 07:00
Category: Opinion, superandianda
Tags: democracia, elecciones colombia, superandianda, votacion
Slug: el-peligro-de-llamar-democratico-a-un-sistema-de-estupidos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/07/image.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: ETCE] 

#### **[Por [Superandianda ](https://archivo.contagioradio.com/superandianda/)- [~~@~~Superandianda](https://twitter.com/Superandianda)** 

###### [10 Jul 2015] 

Cuando Colombia se llena la boca hablando de democracia cree que la práctica solo consiste en asistir a una jornada electoral cada 4 años. Soy fiel defensora de la libertad de opinión, pero cuando se trata de poder, opinar  mal informados, manipulados  e ignorantes nos convierte no solamente en peligrosos sino en estúpidos.

Escasos de participación y pluralismo, defino que el principal problema que sufre nuestro país no es el hambre, no es la pobreza, no es la falta de identidad, sino que su principal problema – de donde nacen todos los anteriores problemas mencionados-  es la falta de preparación para entender y practicar su democracia. Dudo mucho que un estado democrático consista en la validación de un sistema demasiado generoso con el poderoso y extremadamente vil con el desfavorecido.

La costumbre de sentirse solidario “ayudando” a quienes lo necesitan sin transformar el sistema, un sistema diseñado para tener a pocos en el privilegio y a miles en la miseria, nos pone en el puesto del país más desigual de América Latina y el cuarto en el mundo; pero el arribismo colombiano no permite ver nuestras falencias sociales, quienes viven bien lo hacen bajo el precio de mantener mal a muchos, un círculo vicioso que los mismos desfavorecidos promueven en medio de su ignorancia, haciendolos electores peligrosos para cualquier estado que se pretenda llamar democrático (la ignorancia de un pueblo siempre será más peligrosa que las armas). Ni participación ni formación política existe en Colombia, unos sin  ganas de participar, por  las innumerables  decepciones que nos han dado nuestros gobernantes, y otros porque participan inútilmente sin ningún tipo de criterio ni idea. Falta el inmenso compromiso de asumir la  responsabilidad  de nuestro fracaso o transformación.

Alguna vez imagine el día en que la indignación alcanzara para dejar de asistir a las urnas, para que el domingo de elecciones  las calles fueran soledad, hubiera sido la protesta del silencio, hubiera sido el inicio de la transformación con dignidad,  y a pesar de no abandonar mis ideas a   la  utopía, veo lejana la posibilidad de encontrar la dignidad en Colombia -como ejemplo podríamos nombrar a la señorita Colombia, pues  nos representa muy bien en el universo entero,  dejándose pisotear por poder y dinero, no está lejos de la realidad política- La voz más alta es la del ignorante que repite las palabras que le ponen en la boca los noticieros  y el silencio que aún guardan las víctimas, los olvidados, los “ayudados” por aquellos que bajan el vidrio del carro para lanzar una moneda y sentirse superiores.

Criticamos a Venezuela por hacer filas por comida pero nosotros hacemos largas filas para elegir  corruptos, mientras los niños de la Guajira se mueren de hambre; nos escandalizamos por los atentados de las FARC que golpean nuestra naturaleza pero dejamos pasar por alto los abusos de las multinacionales en nuestro territorio. La doble moral es tanta que llaman héroes a los hijos de los más pobres que sirven como carne de cañón. Con escasa formación ideológica, pretenden que la paz no nos cueste nada, como si 50 años de guerra no fueran suficientes y 8 años de seguridad democrática no hubiera demostrado que el plan de resultados  -contando cabezas-  sacrifica la vida de muchos inocentes.

Le llaman democrático a un sistema de estúpidos útiles que le han servido por décadas a las mismas familias y maquinarias, siendo mayoría y escogiendo por  las minorías, conduciendonos  a nuestro fracaso como sociedad. Si estuviéramos dispuestos a cambiar el sistema politico colombiano tomaría muchos años curarnos de la enfermedad de la indiferencia. Somos la zoociedad fragmentada, como diría Jaime Garzón.
