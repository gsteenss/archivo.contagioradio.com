Title: Víctimas se enterrarán para exigir búsqueda de los desaparecidos
Date: 2017-07-26 15:06
Category: DDHH, Paz
Tags: Cuerpos Gramaticales, Desaparición forzada, Ejecuciones Extrajudiciales, falsos positivos, MOVICE, obra de teatro
Slug: victimas-se-enterraran-en-bogota-para-exigir-la-busqueda-de-los-desaparecidos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/07/Cuerpos-G.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Facebook Cuerpos Gramaticales] 

###### [26 Jul. 2017] 

En el marco de la conmemoración de los 9 años de la ejecución extrajudicial de Fair Leonardo Porras, uno de los jóvenes de Soacha presentado en 2008 como guerrillero muerto en combate, diversas organizaciones de víctimas de detenidos, desaparecidos y de ejecuciones extrajudiciales se dan cita este **27 de julio desde las 6 a.m. en la Plaza de Bolívar de Bogotá** para exigir de manera simbólica la búsqueda de los y las desaparecidas.

A través de una obra de teatro llamada *Cuerpos Gramaticales*, que usa como elemento primordial el cuerpo, las madres que han perdido a sus hijos por la desaparición forzada o que han sido presentados como “falsos positivos” se sembrarán en la tierra, con una planta, durante seis horas, para mostrar así la conexión que hay entre la tierra y la sociedad y como **el cuerpo le cuenta a la sociedad las historias de dolor, perdón, lucha, resistencia y esperanza.**

**Carolina López, integrante del Movimiento de Víctimas de Crímenes de Estado (MOVICE),** manifestó que “esta obra de *Cuerpos Gramaticales* pretende desde la siembra hacer un ejercicio de catarsis colectiva. En esta oportunidad reivindicando la memoria de las víctimas de ejecuciones extrajudiciales en el país”.

### **No hay un dato concreto de ejecuciones extrajudiciales en Colombia** 

En el tema de las ejecuciones extrajudiciales se manejan varias cifras una de ellas desde la Oficina para el Alto Comisionado de las Naciones Unidas que han documentado **cerca de 4.397 víctimas de ejecuciones extrajudiciales en el país.**

“Ese registro se tiene es porque han logrado contacto con organizaciones sociales, de derechos humanos, con víctimas quienes han denunciado este tipo de actuaciones”.

Sin embargo, el subregistro hace que las cifras cambien e incluso asciendan y que **en la actualidad muchos de esos casos se encuentren en un 97% de impunidad “**es muy grave la problemática para miles de víctimas que ni siquiera han sido reconocidas en el Registro Único de Víctimas y que sus derechos siempre han estado a discrecionalidad de los funcionarios”.

### **“No más falsos positivos en la JEP”** 

A través de la **etiqueta promovida en las redes sociales como \#NoMasFalsosPositivosEnLaJEP** las organizaciones sociales y los defensores y defensoras de DD.HH. manifiestan que las ejecuciones extrajudiciales al no haber sucedido en el marco del conflicto armado interno no deberían ser tenidas en cuenta en la Jurisdicción Especial de Paz (JEP).

"Lo que decimos como organizaciones y víctimas es que **no estamos de acuerdo con la posibilidad de que las fuerzas militares ingresen a la JEP cuando no han hecho acuerdos** frente a la nación de que van a reconocer la responsabilidad en los casos de ejecuciones extrajudiciales”.

### **El reto para las víctimas de ejecuciones extrajudiciales** 

“Tenemos que juntarnos, juntar voces para visibilizar que esto no es una problemática solo de una familia sino de miles de familias que están esperando de manera efectiva se tenga una respuesta de parte del Estado frente a la desaparición forzada y las ejecuciones extrajudiciales”.

### **Las víctimas mantienen sus esperanzas en el Acuerdo de Paz** 

Dice López que las víctimas han asegurado que en Colombia **un proyecto de paz estable y duradera no se puede construir desde tesis negacionistas,** porque ellos no van a poder avanzar si las Fuerzas Militares y el Estado siguen negando que cometieron hechos victimizantes de manera sistemática y de manera generalizada como las desapariciones forzadas, las masacres, las ejecuciones extrajudiciales, las torturas, entre otras.

“Si las víctimas vienen caminando en sus procesos de formación, de reivindicación, de declaración, de visibilidad de sus derechos se hace necesaria la exigibilidad de los derechos, de las víctimas y su reconocimiento y garantía”.

![19702515\_1402724366449582\_4565577087584549483\_n](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/07/19702515_1402724366449582_4565577087584549483_n.jpg){.alignnone .size-full .wp-image-44298 width="960" height="534"}

<iframe id="audio_20015923" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_20015923_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU).
