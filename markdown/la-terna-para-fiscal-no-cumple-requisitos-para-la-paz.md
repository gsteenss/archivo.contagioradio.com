Title: El perfil del fiscal general según organizaciones de DDHH
Date: 2016-05-02 15:34
Category: Entrevistas, Judicial
Tags: Derechos Humanos, Fiscalía, Terna
Slug: la-terna-para-fiscal-no-cumple-requisitos-para-la-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/05/terna-fiscal.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: TeleMedellín] 

###### [02 Mayo 2016 ]

La terna presentada por el presidente Juan Manuel Santos para la Fiscalía General de la Nación, ha generado gran preocupación en las organizaciones defensoras de derechos humanos por el **poco compromiso que podrían tener con el proceso de paz y la superación de la impunidad  en el país.**

Las organizaciones de derechos humanos le expresaron a la Corte Suprema de Justicia los criterios y compromisos que debería tener un Fiscal para la paz y  **que no ven reflejados la terna** conformada por[Nestor Humberto Martínez (ex ministro de la Presidencia)](https://archivo.contagioradio.com/con-nestor-humberto-martinez-la-fiscalia-quedaria-en-manos-del-poder-empresarial/), Yesid Reyes  (ministro de Justicia) y Mónica Cifuentes Osorio (asesora jurídica del proceso de paz con las FARC) .** **

De acuerdo con Claudia Erazo, abogada de la Corporación Jurídica Yira Castro y vocera de la plataforma de Organizaciones de Derechos Humanos, algunos de los criterios que debería tener el Fiscal General de la Nación son el **compromiso con la paz.** Para consolidarla y desmontar las estructuras criminales se necesita un Fiscal general desligado de los poderes económicos e independiente de intereses políticos clientelistas,

Que pueda **superar la impunidad de los graves crímenes que se han cometido en el país**, debe apostarle a los procesos de justicia que se requieren para lograr procesos de reconciliación, debe superar los delitos como el desplazamiento forzado, avanzar en la investigación de críemenes de lesa humanidad y **debe estar comprometido con la superación de la impunidad frente a la corrupción.**

Erazo considera que el proceso de elección para la terna se realizó más con **fines políticos** que por los criterios que debería tener un Fiscal para la paz y que no presentan es sus hojas de vida esos compromisos fuera de la institucionalidad y más hacia la justicia y los derechos humanos. Esto teniendo en cuenta que el próximo Fiscal será quién genere la jurisdicción para la paz.

Se espera que la Corte Constitucional tenga en cuenta la serie de recomendaciones que genera la plataforma de Organizaciones de Derechos Humanos para realizar la elección del Fiscal General de la Nación.

<iframe src="http://co.ivoox.com/es/player_ej_11383495_2_1.html?data=kpagmpiYfZahhpywj5WaaZS1lZaah5yncZOhhpywj5WRaZi3jpWah5yncaTgwtrRy8aPjMbmwt_cjZKPh9Dm0dTfw8jNaaSnhqeg0JCns83jzsfWw5CpudPj0caYp6q5mY6ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)]]
