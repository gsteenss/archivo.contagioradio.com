Title: Sin olvido a Jesús de Nazaret en esta pascua
Date: 2016-04-08 09:45
Category: Abilio, Opinion
Tags: Jesus de nazaret, proceso de paz
Slug: sin-olvido-a-jesus-de-nazaret-en-esta-pascua
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/04/15021713319_6555c3dbab_b.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Juan Reguera 

#### **[Abilio Peña](https://archivo.contagioradio.com/abilio-pena-buendia/) -[@[Abiliopena]

###### 8 Abr 2016 

[Según  el  Pew Research Center,  centro de investigación de Washington  la población católica de Colombia  [asciende a 38.100.000](http://www.semana.com/mundo/articulo/colombia-sexto-pais-mas-catolicos/333397-3) , el  82% del total de habitantes,  y  de acuerdo con  la fundación Latinobarómetro,   el  [3 %  de los colombianos pertenecen a  iglesias evangélicas](http://www.eltiempo.com/archivo/documento/CMS-13888195).  Para ese 85 % de cristianas y cristianos,  los acontecimientos que se conmemoran el viernes Santo y el domingo de resurrección, que inauguran el tiempo de pascua, tienen un especial significado, así no todos participen de los ritos.]

[Vale la pena dar una mirada  a los fundamentos del cristianismo que deberían inspirar a las y los creyentes en Colombia, en un momento crucial de la historia del país en que se está buscando una salida al conflicto armado, que tiene como causa principal las injusticias que padece nuestra sociedad.]

[Son dos los momentos fundentes de la Semana Santa: la crucifixión - no cualquier muerte- y la presencia resucitada de Jesús. Los relatos de los 4 Evangelios y las cartas de Pablo, dan cuenta de la pasión de Jesús por el Reino de Dios, la utopía de un mundo que funcione como si Dios fuera el Rey. Ese anuncio se dio en momentos en que en Palestina gobernaban los herodes, reyes dependientes del todo poderoso imperio romano, que proclamaba como dios al emperador, al César, a Tiberio como Señor.]

[El anuncio del Reino del que hablaba Jesús, de acuerdo con el Nuevo Testamento, se centra en la opción por los empobrecidos, clama por la justicia distributiva de la mesa compartida, bellamente ilustrada en la metáfora de Mateo del juicio final  en el que se juzga la fidelidad al Dios de Jesús por lo que hicieron o dejaron de hacer con los pequeños, por la satisfacción de sus necesidades vitales: "Porque tuve hambre y me diste de comer, sed y me diste de beber, estuve desnudo y me vestiste, fui forastero y me hospedaste, estuve en la cárcel y enfermo y me visitaste".]

[Ese anuncio estuvo impulsado por la fuerza del amor,  por aquel  “ama al otro como a ti mismo”, porque ese amor, es lo que somos, o en el caso contrario donde fuera el odio el que domine nuestros sentimientos eso terminamos siendo, odio. La fuerza del amor,  que invita a amar hasta al enemigo,  es la que da sentido a los riesgos que amenazan la propia vida biológica, pues asumir en serio la construcción del Reino  puede acarrear la muerte al enfrentar a los poderes de dominación, que en los tiempos de Jesús se encarnaban en el Imperio Romano,  en la sumisión de los Reyes locales y en la institucionalidad religiosa aliada con dichos poderes.]

[Y  llega el “Viernes Santo". Jesús peregrina a Jerusalén, capital de la colonia, sede de Herodes Arquelao y Centro del Poder Religioso. Ingresa, según los relatos, al templo; se indigna por la injusticia que se condensa en un culto distante al Dios que experimenta, y procede a la destrucción simbólica del lugar que monopolizaba lo sagrado. Sigue, luego, la detención, la tortura, la burla, el juicio y la condena a la pena de muerte decidida por los gobernantes del momento.]

[Ahí no terminó todo. Los relatos de la "aparición", que conmemoramos el “Domingo de Resurrección” inaugurando la pascua,  dan cuenta de que a Jesús lo  experimentaron  vivo; las mujeres primero, y luego los hombres; sus seguidoes y seguidoras más cercanas. Lo expresaron en parábolas que condensan toda la verdad de ese acontecimiento pascual: Jesús  vive, es el Señor.  Dios no lo dejó en la tumba, en el olvido.]

[En otras palabras quiere decir, como interpretaron sus seguidoras y seguidoras, que Dios lo resucitó; que dijo "no" a sus asesinos. El deseo de los victimarios fue frustrado. Esa convicción hizo que sus discípulas y discípulos continuaran anunciando a Jesús como Señor en medio de un imperio que permitía que se le llamara así sólo al Cesar y  proclamando la mesa compartida, la distribución equitativa de los bienes.]

[Aunque no siempre las prácticas  de los cristianos se  corresponden con  el testimonio de Jesús, su presencia anima e interpela en realidades como la colombiana, donde la injusticia es la que impera, donde el gobierno, a propósito de los diálogos con las guerrillas, insiste en afirmar que no se está discutiendo el modelo social que reproduce la exclusión; donde el imperio de las transnacionales que acaparan tierras determina las políticas agrarias en contra de la justicia distributiva; donde las víctimas de crímenes de del poder se multiplican por millares. La pasión de Jesús se repite en Colombia, pero también la fuerza de su presencia que anima a trabajar por construir una sociedad que funcione como si  Dios fuera el gobernante. ]

------------------------------------------------------------------------

###### Las opiniones expresadas en este artículo son de exclusiva responsabilidad del autor y no necesariamente representan la opinión de la Contagio Radio. 
