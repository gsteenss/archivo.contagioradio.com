Title: Cese bilateral abre la puerta a la participación social en mesa con el ELN
Date: 2017-10-03 16:03
Category: Nacional, Paz
Tags: proceso de paz eln, Quito
Slug: cese-bilateral-abre-la-puerta-a-la-participacion-social-en-mesa-con-el-eln
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/09/paz.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo Paarticular] 

###### [03 Oct 2017] 

Inició el cese bilateral armado entre el Ejército de Liberación Nacional y el Gobierno Nacional, que ha sido catalogado como histórico y que pretender generar un clima de confianza que permita que acciones para el desescalamiento de conflicto se gesten más rápidamente y de acuerdo con el gestor de paz, Carlos Velandia, lograr que el cese **“se prolongue en el tiempo”**.

“Imagínense, que podamos todos los colombianos, el día 7 de agosto de 2018 a las 3 de la tarde, mostrarle al mandatario electo una radiografía en el que el país este sin guerra” afirmó Velandia, manifestando que de esta forma **no se podría reversar el país a tiempos de guerra. **(Le puede interesar: ["Inicia el cese bilateral entre el ELN y el Gobierno Nacional"](https://archivo.contagioradio.com/cese-bilateral-eln-2/))

Sobre el protocolo que se estableció para corroborar el cese bilateral, Velandia afirmó que es poco “ortodoxo” y sitúa a otros sectores de la sociedad como la Iglesia en papeles muy importantes, **“es una mixtura bastante inusual, pero este mecanismo es idóneo y altamente calificado”**. (Le puede interesar: ["Así es el protocolo de veeduría para verificación del cese bilateral entre ELN - Gobierno"](https://archivo.contagioradio.com/asi-es-el-protocolo-de-veeduria-del-cese-al-fuego-entre-el-gobierno-y-el-eln/))

De igual forma, el gestor de paz recalcó que el cese bilateral hasta ahora empieza “son 100 días en donde se pondrá a prueba que tanta fortaleza tiene el gobierno al interior de sus tropas y que tanto es acatado el comando central al interior del ELN”.

### **La participación social centro del proceso** 

Con el inicio del cese bilateral Velandia señaló que queda mucho más abierta la puerta para que se dé la participación social, sin tantas restricciones con un clima global de garantías para todos, y enfatizó que es importante que haya templanza y fuerza en el escenario actual, en donde después de firmados los acuerdos de paz de La Habana, hay serios incumplimientos al proceso, **que no pueden traducirse en un “reversazo hacia la guerra”.**

“Tenemos que escapar de la guerra y situarnos en un escenario de gran complejidad para el trámite e implementación de los acuerdos, en donde el aspecto del incumplimiento genera incertidumbre, **pero lo peor que podría aconsejarse es que el proceso con el ELN se detenga**” manifestó Velandia.

<iframe id="audio_21246093" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_21246093_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
