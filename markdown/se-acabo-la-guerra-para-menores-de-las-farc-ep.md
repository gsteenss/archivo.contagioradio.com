Title: Se acabó la guerra para menores de las FARC-EP
Date: 2016-05-15 19:42
Category: Nacional, Paz
Tags: acuerdos de paz, diálogos paz, menores de edad en las farc, reclutamiento infantil farc
Slug: se-acabo-la-guerra-para-menores-de-las-farc-ep
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/02/Reclutamiento-infantil.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Infobae ] 

###### [15 Mayo 2016]

Este domingo, las delegaciones de paz de las FARC-EP y el Gobierno Colombiano pactaron en La Habana el acuerdo que permitirá a todos los niños, niñas y adolescentes dejar los campamentos y reintegrarse a la vida civil, a través de **programas económicos, educativos y de salud que garantizarán sus derechos humanos y evitarán que sean reclutados nuevamente**.

De acuerdo con el comunicado, en los próximos 15 días se acordará una **hoja de ruta que permitirá la desvinculación de los 21 menores de edad que integran las filas de las FARC-EP**, quienes retornarán a sus comunidades de origen para, junto a sus familias, hacer parte de programas especiales que serán verificados por la Unicef, la Organización Internacional para las Migraciones y tres organizaciones más que la Mesa definirá.

Según lo pactado, una vez los menores dejen los campamentos **no serán declarados penalmente responsables y se beneficiarán del indulto por rebelión y delitos conexos**. Así mismo, tras la firma del acuerdo final, el Gobierno tramitará todas las medidas necesarias para que los menores procesados o condenados por delitos indultables se acojan a la [[Jurisdicción Especial para la Paz] ](https://archivo.contagioradio.com/sistema-integral-de-verdad-justicia-reparacion-y-no-repeticion/) para examinar sus responsabilidades.

La delegación de las FARC-EP, reiteró su compromiso de [[poner fin al reclutamiento de menores de edad](https://archivo.contagioradio.com/farc-ep-anuncian-que-no-reclutaran-mas-menores-de-edad/)] y aseguró que **entregará toda la información de los menores de 15 años que dejarán los campamentos**, en el marco de las medidas de construcción de confianza que se han pactado para el fortalecimiento del proceso de paz.

Por su parte, Leila Zerrougui, Secretaria General Adjunta para la cuestión de los niños y los conflictos armados de la ONU, insistió en la **importancia de los programas de reintegración, con enfoque comunitario y familiar**, para evitar que los menores de edad sean nuevamente reclutados por otros grupos armados y garantizar que cuenten con oportunidades reales para ser miembros activos y pacíficos de la sociedad.

"Estamos poniendo fin a la guerra (...) los niños y jóvenes deben liderar la construcción de un nuevo país" aseguró Humberto de La Calle, mientras que Iván Márquez aseveró que el acuerdo es el punto de partida para que todos los colombianos comprendan la **urgencia de garantizar los derechos de todos los niños, niñas y adolescentes**, quienes día a día enfrentan el narcotráfico, la prostitución y el reclutamiento por parte de las fuerzas militares.

Según Márquez, "**el más grave problema de la infancia colombiana no es la existencia de menores de edad en campamentos de las FARC**". En el 'Bronx', cientos de niños, niñas y adolescentes se drogan, son prostituidos y maltratados, y delinquen para sobrevivir a la miseria y al abandono. Otros, ante el deber de trabajar para ganarse el pan, permanecen en las tinieblas del analfabetismo y en La Guajira, desde el 2012, [[han muerto de hambre por lo menos 12.000 menores de 5 años](https://archivo.contagioradio.com/ritual-de-duelo-contra-la-muerte-de-ninos-wayuu/)]. Por lo que el acuerdo constituye un **avance en la garantía de derechos para la población infantil colombiana**.

###### [Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)] ] 
