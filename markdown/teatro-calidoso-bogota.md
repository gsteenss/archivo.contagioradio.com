Title: Una obra recuerda a Calidoso y a los habi-tantes de calle
Date: 2018-10-17 11:21
Author: AdminContagio
Category: eventos
Tags: Bogotá, calidoso, teatro
Slug: teatro-calidoso-bogota
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/04/calidoso.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Hormiguero Teatro 

###### 27 Abr 2018 

En las grandes ciudades hay un sin número de personas que no cuentan con un hogar y las condiciones básicas, considerados por muchos como desechables o indigentes. Personas que no queremos ver, pero que reflejan nuestros miedos y que cuentan con historias, algunas cargadas de mucho dolor, pero también de alegrías.

Es desde ahí, donde Hormiguero Teatro indaga y reflexiona para presentar al público *HABI-TANTES*, una obra de teatro que cuenta esas historias, que bajo la dirección de Andrés Caballero y con la dramaturgia de Erick Leyton, se presentará del 19 de octubre al 1 de noviembre en Bogotá.

La obra está enmarcada en un muro que divide radicalmente nuestro mundo del mundo de ellos; y aunque la distancia parece evidente, logran entrometerse y fisgonear en sus vidas, en sus acciones cotidianas, en su manera de estar, sobrevivir y relacionarse.

El hambre, el frío, el destierro, el despojo, la pérdida de identidad, la infancia, la maternidad en las sombras, la intimidad, el sexo, la droga, el deterioro del cuerpo, son algunos de los temas que aborda este montaje, pero a partir del humor negro y la ironía, ofreciendo al espectador una experiencia única y exquisita.

El muro, que también representa la ciudad y sus calles, se abrirá cada noche para entrar en la intimidad de cada uno de estos HABI-TANTES, que además esperan la llegada de alguien a quien llaman “Calidoso” y aunque, no lo recuerdan con claridad, es de vital importancia para cada uno de ellos.

En su segunda temporada, la obra tendrá funciones los días jueves, viernes y sábado a las 8:00 p.m. en la Sala Ágora de la Academia de Artes Guerrero Cra 18ª \# 43 – 50, del 19.
