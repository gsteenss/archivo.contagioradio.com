Title: ¡Nunca Más Violencia Ginecobstétrica!
Date: 2020-02-25 15:14
Author: AdminContagio
Category: Columnistas invitados, Opinion
Tags: Embarazos, ginecobstetra, Médicos, mujeres, Violencia Obstétrica
Slug: nunca-mas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/02/3f8558_9b3a9ab3df05472dac3f5eddec9c73a9mv2_d_2560_1600_s_2.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:heading {"level":6} -->

###### **Por Ángela Galvis Ardila.**  

<!-- /wp:heading -->

<!-- wp:paragraph -->

El primer recuerdo que tengo de la violencia ginecobstétrica fue escuchar decir a mi mamá que era común que a las mujeres a punto de parir, el personal de salud le hiciera comentarios como: “pero cuando lo estaba haciendo no le dolía, ¿no?” o “deje de quejarse que no es la primera mujer que va a tener un hijo”, y muchas más frases de mal gusto, cargadas de un trato humillante y de una dolorosa ofensividad que se fueron colando hasta volverse no solo normales y aceptadas, sino graciosas a costa de la dignidad de la mujer.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Y a mi madre no fue a la única persona que le oí hablar del tema, a medida que iba oyendo a más mujeres en su experiencia al dar a luz, me fui encontrando con que la mayoría han pasado por lo mismo. 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

La violencia ginecobstétrica es una cruel realidad que está tan arraigada que no nos damos cuenta de haberla padecido. Ni siquiera la llamamos violencia, a lo sumo, la dejamos como parte de nuestro anecdotario, tal y como la dejé yo cuando fui víctima y me remití simplemente a calificar el episodio como uno más de la mala suerte.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Hace muy poco, a alguien que conozco le oí decir: “gracias a que una tía me dijo que así me doliera no fuera a quejarme en el hospital fue que logré que no me trataran mal las enfermeras. Ha sido el mejor consejo que me han dado”.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":5} -->

##### Es decir, el mejor consejo fue que se hiciera invisible para no incomodar.

<!-- /wp:heading -->

<!-- wp:paragraph -->

Y así, casi todas las mujeres que hemos sido madres tenemos una historia maltratadora por contar en el camino del embarazo y del parto y que fuimos naturalizando porque nos enseñaron que todo lo que tiene que ver con la maternidad es casi que un privilegio divino y renegar de lo mal que nos fue “en el día más feliz de nuestras vidas” equivale tanto como a blasfemar, sin contar con que nos llamen flojas o exageradas.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Es tan amplia esta violencia que no la sufrimos solamente las mujeres, también los hombres como padres, pues a muchos no les permiten entrar a acompañar a su pareja, tampoco a ver el nacimiento de su hijo o de su hija, otros pasan horas afuera en una sala de espera sin tener noticias de lo que pasa allá adentro y solo saben de la suerte del bebé y de la madre horas después del nacimiento porque por parte de la institución hospitalaria nadie se ha tomado la delicadeza de brindarle información.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":5} -->

##### Son múltiples los actos demostrativos de que la obstétrica es una forma de violencia y no actos propios de la ciencia médica:

<!-- /wp:heading -->

<!-- wp:paragraph -->

inducir un parto sin necesidad; hacer episiotimías innecesarias; realizar tactos vaginales por varias manos, excediendo el número permitido y con la consecuencia de infecciones graves; ignorar a la mujer en su dolor físico y emocional; regañarla, criticarla, rasurarla sin su consentimiento; atribuirle la culpa de todo lo que sale mal, hasta de cuando el feto muere o nace sin vida; cesáreas mal hechas o negarse a hacerlas cuando hay estrechez pélvica y a cambio llevar a cabo partos instrumentalizados con las consecuencias funestas que pueden desencadenarse; hacer comentarios sexistas y degradantes del recién nacido, y algo realmente increible pero que ocurre mucho más de lo que creemos: suturar a la mujer de tal manera que al volver a tener relaciones sexuales el “hombre goce de su estrechez” sin importar que a ella se le dificulte hasta sentarse.  

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

“¿Ya todos han visto cómo se hace una sutura perineal?”, preguntó un profesor a sus alumnos practicantes de medicina en una universidad bogotana; como la respuesta de algunos fue un no, el reconocido profesor, para hacer más práctica la clase, no tuvo problema en romperle, sin necesidad, desde la vagina al ano a su paciente parturienta. Esto es como si un ortopedista preguntara a sus alumnos si han visto un fémur roto y, frente a un no, procediera a rompérselo a su paciente para que aprendan.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Denunciemos, quejémonos, seamos red de apoyo de nuestras hijas, hermanas, sobrinas, amigas, etcétera, no aceptemos ningún comentario destemplado del personal de salud sobre la sexualidad ni sobre nuestro cuerpo, exijamos una atención respetuosa que cumpla con los protocolos impuestos por la **Organización Mundial de la Salud**, rompamos esa estructura dañina que nos hace ver como “pecadoras” por haber quedado embarazadas y que nos conduce equivocadamente a considerar merecido un trato deshumanizado y abusivo, que impacta negativamente en nuestra dignidad y en la de nuestro entorno.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

No volvamos a permitir que el miedo, la vulnerabilidad, las condiciones extremas o la condena de “parirás con dolor” sea la justificación de esta otra forma de violencia escondida. **¡Nunca más!**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Le puede interesar: [Sí, las mujeres menstruamos](https://archivo.contagioradio.com/si-las-mujeres-menstruamos/)

<!-- /wp:paragraph -->
