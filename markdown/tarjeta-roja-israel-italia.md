Title: Tarjeta roja a Israel en Italia por violación de DDHH
Date: 2017-09-21 12:56
Category: Onda Palestina
Tags: Apartheid, BDS, FIFA, Israel, Palestina
Slug: tarjeta-roja-israel-italia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/09/Imagen1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: BDS Italia 

###### 20 Sep 2017 

El pasado 5 de septiembre se jugó el partido entre las selecciones de fútbol de Italia e Israel en las clasificaciones para el mundial de Rusia del 2018. Aprovechando esta oportunidad, activistas del BDS en Italia realizaron una actividad alrededor de la campaña “ Tarjeta Roja para la suspensión de Israel de la FIFA”.

Antes de que empezara el partido, varios activistas repartieron miles de comunicados y tarjetas rojas a los asistentes al juego, con el fin de sensibilizar a los ciudadanos sobre las violaciones de los derechos humanos por parte del Estado israelí, que también afecta un derecho elemental como son las actividades deportivas.

La tarjeta roja busca simbolizar el pedido de suspensión de la Federación de Fútbol de Israel de la FIFA hasta que cumplan con los estatutos de la Federación, los derechos humanos y el derecho internacional. Entre otras cosas se critica el la Federación tenga afiliados a equipos que están radicados en las colonias ilegales israelíes en Palestina.

Según afirma el sitio oficial de la campaña BDS Italia Muchos aficionados ya conocían la iniciativa, ya que la noticia tiene una amplia cobertura de medios locales y nacionales. También hicieron preguntas los periodistas en la conferencia de prensa oficial antes de la reunión. En la entrada del estadio, las fuerzas de seguridad sistemáticamente intimidaron y registraron a personas que buscaban tarjetas rojas, temiendo que la suspensión de la FIFA por parte de Israel también pudiera expresarse durante el juego.

Las iniciativas de apoyo a los derechos palestinos arrancaron el sábado 2 de septiembre con la realización de un flashmob en la plaza Fontanesi, en la ciudad de Reggio Emilia, donde los ciudadanos respondieron a la invitación tomándose fotografías con una tarjeta roja en solidaridad con la lucha por los derechos de los palestinos. Un día antes a este flashmob el concejal Yuri Torri del Partido de Izquierda Italiana había presentado una  
petición a la Asamblea Legislativa de la región de Emilia Romagna, instando al Ayuntamiento a unirse a la campaña para la suspensión de Israel de la FIFA.

En esta emisión de Onda Palestina también hablamos sobre la conmemoración de la masacre de Sabra y Chatila, hablaremos de un caricaturista que ha centrado su trabajo en denunciar lo que sucede en Palestina, y haremos una reseña del plantón de la semana anterior en rechazo a la visita de Netanyahu.

<iframe id="audio_21018857" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_21018857_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Escuche esta y más información sobre la resistencia del pueblo palestino en [Onda Palestina](https://archivo.contagioradio.com/programas/onda-palestina/), todos los Martes de 7 a 8 de la noche en Contagio Radio. 
