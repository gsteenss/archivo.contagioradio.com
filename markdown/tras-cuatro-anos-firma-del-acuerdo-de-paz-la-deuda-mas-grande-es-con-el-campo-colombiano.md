Title: Tras cuatro años de la firma del Acuerdo de Paz, la deuda más grande es con el campo colombiano
Date: 2020-09-26 21:46
Author: AdminContagio
Category: Actualidad, Paz
Tags: acuerdo de paz, reforma rural integral, reincorporación, Sustitución de cultivos de uso ilícito
Slug: tras-cuatro-anos-firma-del-acuerdo-de-paz-la-deuda-mas-grande-es-con-el-campo-colombiano
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/09/2020-09-26.png" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/09/2601175006d2b48.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: firma del Acuerdo / Banco de la República

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Ante los ojos de países garantes como Cuba y Noruega, la veeduría de Venezuela y Chile; el 26 de septiembre de 2016 el gobierno de Juan Manuel Santos y la antigua guerrilla de las FARC, firmaba el Acuerdo de Paz que significaba el primer paso del cese de un conflicto de más de 60 años de historia. Hoy, cuatro años después de dicha fecha, con un nuevo Gobierno que no ha dado ha prioridad a la implementación y en medio de nuevos ciclos de violencia, esta es el diagnóstico hecho por los expertos y desde los territorios a una paz que tarda en llegar.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Para [**Fernando Cristo, exministro de Interior e integrante de Defendamos la Paz**](https://twitter.com/CristoBustos)hay, después de cuatro años de firmado el acuerdo, también podría hacerse un balance positivo en medio de la desesperanza fruto de la “apatía del gobierno Duque” en la implementación.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Pese a que señala que el regreso a las armas de Jesús Santrích e Iván Márquez fue “una profunda equivocación, agrega que la desmovilización de las FARC y su transformación en partido político **“produjo una explosión democrática en el país que se vio con la elección de alcaldes alternativos y que se va a consolidar a futuro, la sola firma del Acuerdo trajo aspecto positivos”.**

<!-- /wp:paragraph -->

<!-- wp:quote -->

> “Hoy conocíamos información de la directora del Hospital Militar de cómo hay una disminución del 97% por parte de soldados y policías de las habitaciones”
>
> <cite>Juan Fernando Cristo</cite>

<!-- /wp:quote -->

<!-- wp:paragraph {"align":"justify"} -->

El exministro también se refirió al no cumplimiento de las curules para la paz, fundamentales para las víctimas, “ustedes se imaginan la situación de los líderes sociales si tuvieran 16 voceros en la Cámara de Representantes exigiendo a nombre de sus comunidades protección del Estado? Otra sería la circunstancia”, en medio de un contexto en que los ciclos de violencia han aumentado sin que tampoco se apele a la Comisión Nacional de Garantías de Seguridad

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

“No han hecho trizas los acuerdos porque hay un respaldo muy fuerte de la comunidad internacional, porque hay una fuerte movilización de la sociedad civil y porque la Corte Constitucional ha blindado los acuerdos”; pero la mala noticia es que engavetaron el acuerdo y hay muchos pendientes en la implementación.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Tras cuatro años, la transformación del territorio no ha avanzado

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

A su vez Cristo resaltó los nulos avances en el campo colombiano, a lo que **Consuelo Corredor, coordinadora de la Secretaría Técnica de Verificación Internacional del Acuerdo**, menciona que en materia de reforma rural integral, en los primeros años hubo una dinámica de normativa e institucionalidad pero a cuatro años aún están incompletos, limitando el cerrar las brechas entre el campo y la ciudad.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Para hacerse una idea, señala que lo que está más adelantado en ese sentido son los Planes de Desarrollo con Enfoque Territorial (PDET) y la creación del Fondo de Tierras. De igual forma aunque se han aprobado seis Planes Nacional de Reforma Rural Integral, por ahora aún falta aprobar otros diez, asímismo resaltó la necesidad de continuar avanzando en **el Catastro Multipropósito que permitirá formalizar la tenencia y propieda de la tierra. [](https://archivo.contagioradio.com/inventario-tierras-catastro-multiproposito/)**[(Lea también: En 2025 Colombia tendrá un inventario de sus tierras gracias al catastro multipropósito)](https://archivo.contagioradio.com/inventario-tierras-catastro-multiproposito/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Corredor resalta la importancia de los PDET como eje del punto de reforma rural, revisando tres subregiones en particular: **Sur de Córdoba, Catatumbo y Pacífico Medio** donde más del 50% de la población está inscrita como víctima, se han visto avances en mejoramientos de vías, dotación de mobiliario escolar, agua, saneamiento básico, pese a su importancia, resalta, **"no tienen el alcance para hacer una transformación estructural del campo".**

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

La experta, señala que su preocupación más grande está en la ausencia de garantías para el ejercicio político, en especial "para los excombatientes y enormemente los asesinatos de líderes sociales y defensores de DD.HH." por lo que señala el foco de atención debe ser puesto en la integralidad del acuerdo, pues afectadas la seguridad de esta población se vulneran otros puntos del acuerdo como la sustitución de cultivos y reincorporación.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

**Leider Valencia, delegado nacional de la COCCAM ante el Programa Nacional Integral de Sustitucion de Cultivos de Uso Ilicito (PNIS)**, se refirió a la Solución al Problema de las Drogas Ilícitas, acuñado en el punto 4, exaltando que es mínimo el avance en términos de desembolsos, huertas caseras y el Plan de Atención Inmediata (PAI), agregando que con la llegada del gobierno Duque se han cerrado las inscripciones de familias que han manifestado su voluntad de cambiar la economía de la marihuana, coca y amapola.

<!-- /wp:paragraph -->

<!-- wp:image {"id":90496,"sizeSlug":"large"} -->

<figure class="wp-block-image size-large">
![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/09/2020-09-26.png){.wp-image-90496}  

<figcaption>
Informe Ejecutivo PNIS \#19

</figcaption>
</figure>
<!-- /wp:image -->

<!-- wp:paragraph {"align":"justify"} -->

Leider expresó además, los riesgos que corren quienes se han acogido al PNIS, siendo cerca de 60 los líderes sociales de la COCCAM que han sido asesinados y que han apoyado la sustitución voluntaria. [(Lea también: Tras Acuerdo de Paz han sido asesinados 7 beneficiarios del PNIS en Córdoba)](https://archivo.contagioradio.com/tras-acuerdo-de-paz-han-sido-asesinados-7-beneficiarios-del-pnis-en-cordoba/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

"Están erradicando donde las comunidades han manifestado su voluntad de sustituir, donde tenemos acuerdos colectivos, municipios como Miranda, tenemos 600 familias que están cumpliendo con el PNIS, pero hace tres años no llegan los recursos", expresa el líder social, señalando cómo sí existe dinero para pagar a erradicadores pero no para continuar con los desembolsos, una situación que señala, se replica no solo en Cauca sino regiones como Caquetá, Catatumbo o Nariño.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Valencia afirma que **"en las comunidades ya no sabemos quién es quién, con tantos y diferentes grupos, y todo eso se debe a la demora en la implementación del Acuerdo**", cuestionando el cómo se logran transportar la droga a través de las regiones y la diversidad de grupos armados pese a la alta militarización que señala, además tiene la tarea de erradicar 35.000 hectáreas en los territorios.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### El acuerdo no se hizo con el gobierno Santos, fue con el Estado

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

"Falta una empatía no solo del Estado, ni del Gobierno sino de las personas que estamos en la ciudad", expresó la abogada Natalia Herrera de la Corporación Jurídica Yira Castro quien exaltó la necesidad de las garantías de no repetición y reparación, esto de cara a los reciente sucesos de violencia y las más de 60 masacres que han ocurrido en el año y que de cierta forma impide que se avance en la implementación misma del acuerdo.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Por su parte, Horacio Castro, enlace territorial del Consejo Nacional de Reincorporación (CNR) exaltó que desde los reincorporados quien reiteró la necesidad de proteger la vida de los excombatientes, sus familias y las comunidades, llamando la atención sobre los más de 228 signatarios del Acuerdo asesinados, 43 en lo corrido del 2020.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

En cuanto a los proyectos productivos, Castro alerta que los proyectos aprobados benefician a cercan de 3.500 personas firmantes quedando alrededor de 8.000 personas más que se acogieron al Acuerdo, por fuera de cualquier posibilidad de acceder a los recursos pactados, "pero incluso aquellos que han sido beneficiados, tienen problemas, relata que algunos de los proyectos individuales han fracasado al no hacerse un acompañamiento, sino que se asume que la persona ya fue reincorporada", mientras los proyectos colectivos, algunos han tenido que ser abandonados por riesgos de seguridad, casos concretos como La Uribe en Meta o en Santa Lucia, Ituango. [(Lea también: Esperamos que en Mutatá encontremos la paz: excombatientes de Ituango)](https://archivo.contagioradio.com/esperamos-que-en-mutata-encontremos-la-paz-excombatientes-de-ituango/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

"A veces se aprueban los proyectos pero no hay donde ejecutarlos, y en las partes donde mínimamente hay tierras, no hay proyectos", expresa el firmante, agregando que otra de las problemáticas, es que el Gobierno ha volcado las acciones de reincorporación en los antiguos ETCR, sin tener en cuenta que la mayoría de los firmantes del acuerdo, al menos el 70%, viven por fuera de los espacios territoriales y una gran parte en las zonas urbanas.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Acompañamiento internacional y veeduría son claves

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Tras conocerse la decisión del Consejo de Seguridad de la Organización de Naciones Unidas de extender hasta el 25 de septiembre del año 2021 el mandato de la Misión de Verificación del Acuerdo de Paz en Colombia, la abogada Herrera señala que el mandato de la ONU debe seguir acercándose a los territorios y escuchar lo que está pasando para de tal modo "hacer un contrapeso al Gobierno". [(Lea también: ONU extiende Misión de Verificación del Acuerdo de Paz en Colombia)](https://archivo.contagioradio.com/onu-extiende-mision-de-verificacion-del-acuerdo-de-paz-en-colombia/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Horacio Castro, concluye que si el acompañamiento de la ONU fue clave durante el proceso de paz, ahora en la implementación es mucho más importante y considera una lástima que este esté sometido a sí el Gobierno lo aprueba o no "entendemos la soberanía nacional pero no sabemos que pasaría si nos dejan solos a los reincorporados reclamando al Estado, no sabemos qué pasaría si no contamos con la mediación y veeduria de un organismo independiente como la ONU".

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Cabe señala que el más reciente informe de la ONU, llamó la atención al Gobierno sobre la necesidad de avanzar en temas como la reforma rural integral y alertó sobre los asesinatos de defensoras y defensores de DD.HH. y de excombatientes", compartiendo datos como los 108 líderes asesinados o las 36 masacres, ocurridas en 2019, la cifra más alta conocida desde 2014 y que ya fue superada durante este 2020.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Dichas recomendaciones hechas por el Alto Comisionado para los DD.HH. de la ONU fueron tomadas por el Gobierno como una "intromisión en la soberanía". Pese a la reacción del Gobierno, la ONU **reiteró que continuará afianzando el diálogo respetuoso y apoyando a Colombia en la construcción de un futuro próspero y en paz”.**

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->
