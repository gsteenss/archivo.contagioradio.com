Title: La plataforma Defendamos la Paz lanza su capítulo internacional
Date: 2019-06-13 13:36
Author: CtgAdm
Category: Comunidad, Política
Tags: CINEP, Defendamos la Paz, Internacional, ONU
Slug: defendamos-paz-lanza-capitulo-internacional
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/Capitulo-internacioal-de-Defendamos-la-Paz.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: @GustavoBolivar  
] 

Este miércoles será el **lanzamiento del capítulo internacional de Defendamos la Paz,** un movimiento que agrupa a distintas organizaciones y buscará tener incidencia en el exterior sobre el proceso de paz en Colombia, y el proceso de movilidad humana de nacionales. El lanzamiento será en las instalaciones del CINEP, desde las 2 de la tarde, y participarán los senadores **Aída Avella, Victoria Sandino, Gustavo Bolívar, Iván Cepeda y Gustavo Petro,** así como el ex ministro de interior, **Juan Fernando Cristo.**

**María Cepeda Castro, defensora de derechos humanos**, afirmó que la idea de crear este capítulo internacional surgió tras la realización de la marcha a La Haya, momento en que entendieron que "era momento de llegar unidos a un espacio". Fue así que se unieron a Defendamos la Paz, para defender los derechos en Colombia, lograr visibilizar la situación de violación al Acuerdo de Paz y exigir al Gobierno nacional que continúe las negociaciones de paz con el ELN.

Con esos objetivos, Cepeda dijo que se estaba ideando una agenda de trabajo que permita tocar las puertas de organizaciones internacionales. El primer acto de esa agenda incluye la realización simultánea de una marcha en 4 continentes (en los que hace presencia este capítulo internacional), que se adelantará en noviembre de este año. (Le puede interesar: ["¡A la Corte Penal Internacional para para exigir protección a líderes!"](https://archivo.contagioradio.com/la-corte-penal-internacional-exigir-proteccion-lideres/))

### **¿Funciona la presión internacional?**

La activista manifestó que por un lado, **el Gobierno hace lobby "para lavarle la cara a la situación de derechos humanos, y esconder la realidad colombiana"**, pero por otro, la ONU recientemente hizo un reclamo a Duque sobre el tema de falsos positivos; esto significa que la comunidad internacional sí se está dando cuenta de la situación y está actuando.

Sin embargo, advirtió que aún falta trabajo, pues muchos países que daban asilo a colombianos refugiados que salían por la violencia en el país ya no están otorgando este beneficio, argumentando que en Colombia se firmó un acuerdo de paz. En esa medida, Cepeda considera importante **"decirle a esta comunidad internacional que los factores que han generado esta violencia no han desaparecido y siguen vigentes"**.

El lanzamiento del Capítulo Internacional se desarrollará en las instalaciones del CINEP desde las 2 de la tarde, se realizará un conversatorio que podrá ser visto a través de diferentes canales de comunicación, incluído el Facebook de Contagio Radio. (Le puede interesar: ["Defendamos la Paz denuncia ante la ONU presiones contra magistrados en Colombia"](https://archivo.contagioradio.com/defendamos-la-paz-denuncia-ante-la-onu-presiones-contra-magistrados-en-colombia/))

<iframe id="audio_37070480" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_37070480_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
