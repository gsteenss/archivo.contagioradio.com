Title: Bolívar es víctima de 2 nuevas masacres
Date: 2020-09-08 12:00
Author: AdminContagio
Category: Actualidad, Nacional
Tags: masacre en Carmen de Bolivar, Simití
Slug: bolivar-es-victima-de-2-nuevas-masacres
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/04/Masacre-los-Uvos-e1460076855890.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: Prensa Libre Cauca

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Este lunes, en horas de la noche se reportó una nueva masacre que dejó cuatro hombres muertos, entre ellos un joven de 17 años  y 3 personas gravemente heridas en el municipio del Carmen de Bolívar, departamento de Bolívar. (Le puede interesar:  [Autoridades Tradicionales denuncian agresiones de la Policia en Putumayo](https://archivo.contagioradio.com/autoridades-tradicionales-denuncian-agresiones-de-la-policia-en-putumayo/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Según información de la comunidad, dos hombres en moto llegaron hasta el lugar en que se encontraba este grupo de personas y atacaron con armas de fuego. Los hombres asesinados fueron identificados como Jesús Benites Pineda (18 años) y Farid Luna Sierra (17 años), quienes fallecieron en el lugar de los hechos.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Las víctimas que murieron horas después fueron Jorge García Barrios (37 años) quien fue traslado hasta la ciudad de Sincelejo donde murió en un centro asistencial y Deivi Ronaldo Montes Parra, (27 años) quien era atendido en el hospital local del municipio.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Las dos personas que resultaron heridas, fueron identificadas como, Jesús Alberto Vega Villegas (27 años) y Moisés Daniel Carbaja Señas (21 años), quienes son atendidos en el hospital local de El Carmen de Bolívar.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Las primeras hipótesis apuntarían a que es una presunta disputa territorial entre expendedores de drogas al servicio del Clan del Golfo en el municipio. (Le puede interesar: [Las razones que explican el regreso de las masacres a Colombia](https://archivo.contagioradio.com/las-razones-que-explican-el-regreso-de-las-masacres-a-colombia/))

<!-- /wp:paragraph -->

<!-- wp:heading -->

Otra masacre y más dolor en Bolívar
-----------------------------------

<!-- /wp:heading -->

<!-- wp:paragraph -->

A la vez, se reportó otra masacre en la carretera de San Pablo, a Monterrey (Simití) en el sur de Bolívar donde fueron hallados los cuerpos de 3 hombres. Las autoridades de Bolívar confirmaron que dos de los hombres eran de nacionalidad venezolana, sin embargo, aún no se conoce su identidad. Comunidades denuncian la presencia de grupos armados en la zona.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

«Preliminarmente se conoce que las víctimas fueron ultimadas con arma de fuego y los cuerpos se encuentran tendidos sobre la vía», señaló la Policía del Magdalena Medio.

<!-- /wp:paragraph -->

<!-- wp:heading -->

No paran las masacres en Colombia
---------------------------------

<!-- /wp:heading -->

<!-- wp:paragraph -->

Según el [Instituto de estudios para el desarrollo y la paz -Indepaz](http://www.indepaz.org.co/informe-de-masacres-en-colombia-durante-el-2020/) - en lo que va del 2020, hasta el 5 de septiembre, se han presentado 51 masacres en todo el país. Ahora con estos dos lamentables sucesos y uno más en el Bajo Cauca Antioqueño, la cifra llegaría a 54 masacres, por el momento.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Con estas dos masacres y la que se presentó anoche en el Bajo Cauca Antioqueño, ya serían 55 hechos de este tipo lo que preocupa por el recrudecimiento de la violencia en el país y la crisis de institucionalidad que esto representa, pues el número de efectivos de las FFMM no ha podido controlar los territorios para salvar las vidas. Por el contrario, muchas de las denuncias apuntan a complicidad u omisión por parte de las FFMM.

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->
