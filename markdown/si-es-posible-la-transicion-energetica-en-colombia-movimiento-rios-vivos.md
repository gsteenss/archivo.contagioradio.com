Title: Sí es posible la transición energética en Colombia: Movimiento Ríos Vivos
Date: 2018-10-10 23:33
Author: AdminContagio
Category: Ambiente, Nacional
Tags: ambientalistas, extractivismo, represas
Slug: si-es-posible-la-transicion-energetica-en-colombia-movimiento-rios-vivos
Status: published

###### Foto: Movimiento Rios Vivos 

###### 10 Oct 2018 

Con un mercado campesino, actividades culturales y místicas, diversas comunidades del país congregadas en el Movimiento Colombiano Ríos Vivos, se unieron en un solo grito: "Ríos para la vida, no para la muerte".

Así inició la agenda pública del Segundo Encuentro Nacional del Movimiento Ríos Vivos en Barrancabermeja, Santander. El objetivo de dicho evento es construir una propuesta hacia la transición energética del país, y así dejar a un lado los proyectos extractivistas que han afectado a las poblaciones y a la naturaleza.

En el tercer día del Encuentro, organizaciones ambientales y sociales, nacionales e internacionales, saludaron el evento dando a conocer cada una de las luchas que lleva a cabo cada proceso que participa en el encuentro. La lucha histórica de ASOQUIMBO, pasando por la defensa del páramo de Santurbán, el trabajo que desarrollan las comunidades a través de las consultas populares en todo el país, así como lla Alianza Colombia Libre de Fracking, hasta invitados internacionales como Coordinación Nacional MABI - Brasil, entre otras, saludaron y participaron de la jornada.

En su intervención, José Jiménez, integrante del Movimiento Nacional Ambiental, hizo un llamado a la articulación en defensa de la naturaleza, de todas las organizaciones sociales y ambientales del país, para hacerle frente a las políticas del Gobierno de Iván Duque que buscan profundizar el modelo extractivista.

### **La cultura su papel en la transición energética** 

En esta primera parte, se dio cuenta de cómo la cultura y las tradiciones son claves para lograr una transición energética. Así lo explicó Fabio Muñoz, integrante del Movimiento Ríos Vivos, quien en el mercado campesino se dedicó a vender sus productos y semillas nativas.

"Lo principal es el aprovechamiento de la naturaleza, sin dañarla. Hay unas energías que utilizamos casi inconscientemente y no sabemos realmente cuál es el uso de esas energías alternativas", expresó Muñoz.

Las comunidades aseguran que se han dado cuenta que la misma polisombra son los árboles, que se puede recolectar y reutilizar el agua lluvia, y han instalado en sus casas la estufas ecoeficientes. Además, los asistentes del evento fueron testigos de las acciones que adelantan diversas comunidades como la de San Sebastián y el Bajo Sinú, en el departamento de Córdoba, con los paneles solares.

"El uso de energías alternativas es una forma de resistir. El estado no recibe pago por la energía tradicional, si nosotros mismos la podemos producir. Así se propenda por la unión de las comunidades y se evite el desplazamiento y los hechos victimizantes", expresó el movimiento Ríos Vivos.

Finalmente, la Mesa Social Mineroenergética y Ambiental por la Paz, mencionó que los buenos planes de energía comunitaria, necesitan  una mayor participación de las comunidades y una gran  organización social. También indicaron que es clave identificar cuáles son las necesidades para  lograr una producción que sostenga el autoabastecimiento, y señalaron que se debe trabajar en la educación de las niñas y los niños y capacitar a las comunidades.
