Title: Asesinan a Héctor Anteliz, líder social en Norte de Santander
Date: 2018-06-25 10:59
Category: DDHH, Nacional
Tags: ASCAMCAT, Hector Anteliz, Lider social, Norte de Santadner
Slug: asesinan-a-hector-anteliz-lider-social-en-norte-de-santander
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/06/h.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: ASCAMCAT] 

###### [25 Jun 2018] 

Fue asesinado **Héctor Anteliz, integrante de ASCAMCAT y líder social de Teorema, en Norte de Santander**. De acuerdo con la información recopilada por la comunidad, Anteliz fue retenido por hombres armados que llegaron a su vivienda el pasado viernes 22 de junio. Su cuerpo fue encontrado en la carretera al día siguiente, con varios impactos de bala.

Olga Quintero, vocera de ASCAMCAT, afirmó que a la vivienda del líder llegaron 4 hombres que le manifestaron a Anteliz que los acompañara debido a que “el comándate” necesitaba hablar con él. **Pese a que la familia del líder se opuso a este hecho, los hombres armados aseguraron que no pasaría nada malo**.

Posteriormente, los habitantes manifestaron que encontraron el cuerpo la mañana del día siguiente sobre la carretera. Sin embargo, solo hasta que sus hijos llegaron en una moto, el cadáver fue trasladado hacia un centro médico. (Le puede interesar: ["Colombia Humana denuncia amenazas de Águilas Negras")](https://archivo.contagioradio.com/colombia-humana-denuncia-amenazas-de-aguilas-negras/)

ASCAMCAT manifestó en un comunicado de prensa, que Anteliz se desempeñaba como defensor de derechos humanos en Teorema, coordinador del comité veredal y presidente de la Junta de Acción Comunal de San José. **De igual forma señaló que ninguna estructura armada que opera en el territorio se ha responsabilizado de este acto.**

Frente a estos hechos la organización le exigió a los actores armados que están en la región, que respeten la vida de las y los habitantes y les pidieron que de forma urgente, se encuentren alternativas dialogadas y concertadas que permitan el desescalonamiento del conflicto armado.

<iframe id="audio_26718477" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_26718477_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
