Title: Madres de Soacha enfrentan nuevo golpe tras 8 años de impunidad
Date: 2016-02-09 15:43
Category: DDHH, Nacional
Tags: crímenes de estado, Ejecuciones Extrajudiciales, Falsos potivos, madres de soacha
Slug: madres-de-soacha-enfrentan-nuevo-golpe-tras-8-anos-de-impunidad
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/02/Madres-de-Soacha-e1505154647984.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: La Información 

<iframe src="http://www.ivoox.com/player_ek_10374752_2_1.html?data=kpWgmZmbeZOhhpywj5aXaZS1lZWah5yncZOhhpywj5WRaZi3jpWah5ynca7VxdfS1ZDIqYzH0MbQysaPqc%2Fa08rb1sbSb8%2FpxtvcjczTsNHZjNnfw9iPfIzVhqigh6eVs9SfxcqYj4qbh4630NPhw8zNs4zGwsnW0ZCRaZi3jpk%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Maria Sanabria] 

###### [9 Feb 2016.]

Tras 8 años de lucha en la búsqueda de verdad y justicia, las madres de Soacha deben enfrentarse a un nuevo obstáculo. Ahora deberán esperar a que les sea asignado un nuevo fiscal para las investigaciones por las ejecuciones extrajudiciales de **Daniel Alexander Martínez, Diego Armando Marín Giraldo y el menor de edad Jaime Estiven Valencia Sanabria**, debido a que el que llevaba 5 años investigando, **fue nombrado como fiscal delegado ante la Corte.**

Lo anterior significa un retraso más en las diligencias judiciales que las obliga a empezar las investigaciones nuevamente, con el agravante de que en los años que llevan los casos han habido pocos avances en el esclarecimiento de la verdad.

**“Qué voy a hacer por Dios santísimo, son 8 años de impunidad, pero no crean que voy a dejar de luchar**… estoy muy triste, muy dolida, me quitaron toda mi vida, yo ya no vivo”, afirma María Sanabria, madre de Jaime Estiven Valencia, quien en medio de su dolor, asegura que debe continuar luchando, “porque son más de 5.700 ejecuciones extrajudiciales”.

La información llega a los familiares, el mismo 6 de febrero, cuando se conmemoraron los 8 años de la desaparición forzada y el asesinato de estos tres jóvenes, que para María Sanabria, son los casos que dieron a conocer “la olla podrida en el Ejército”.

A la fecha formalmente no se ha imputado cargos a ninguno de los militares responsables pese a contar con todo el material probatorio que demuestra la responsabilidad del Ejército Nacional. María, expresa estar cansada aunque no se rinde, **“Yo no sé porque hay tanta impunidad, ya no más dilaciones necesitamos encontrar justicia**… Cada cosa a mí me duele, es un palazo en mi corazón porque me siento impotente”.

Según afirman los reclutadores de Daniel, Diego, y Jaime, los jóvenes fueron llevados al municipio de Ocaña, Norte de Santander, donde fueron entregados a miembros del Ejército Nacional; quienes posteriormente los hicieron pasar como miembros de grupos al margen de la Ley dados de baja  en  combate; sus cuerpos fueron enterrados como NN en una fosa común en este municipio, y sólo 8 meses después pudieron ser identificados por sus familiares.

**“En los corazones de ninguna de las víctimas hay paz, por ninguna parte veo la paz”,** asevera Sanabria, quien termina diciendo en medio de lágrimas, "ni con todo el oro del mundo me van a reponer a mi hijo, yo lo que quiero es justicia, ustedes no saben lo que significa para mí el día de la madre… Quiero que el mundo sepa a qué es a lo que se dedican los grandes héroes de la patria”.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
