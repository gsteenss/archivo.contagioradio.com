Title: Parlamentaria alemana rechaza detención del profesor Miguel Ángel Beltrán
Date: 2015-08-05 15:45
Category: DDHH, El mundo, Nacional
Tags: detenciones arbitrarias, Falsos Positivos Judiciales, Heike Hänsel, Juan Manuel Santos, miguel angel beltran, Naciones Unidas, parlamento alemán, prisioneros políticos
Slug: exclusivo-parlamentaria-alemana-rechaza-detencion-del-profesor-miguel-angel-beltra
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/08/Sin-título.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Contagio Radio 

**“A través de la presente permítame expresarle mi más profunda preocupación por la nueva detención que afecta al profesor e investigador social Miguel Ángel Beltrán**”, es el inicio de la carta  dirigida al Presidente Juan Manuel Santos, de parte de Heike Hänsel, parlamentaria Alemana y presidenta de la Subcomisión Naciones Unidas, Organizaciones Internacionales y Globalización, quien hizo un llamado al gobierno nacional, rechazando la detención del profesor Beltrán, y solicitando que se le respete el debido proceso.

De acuerdo con la carta enviada, para la parlamentaria el caso del sociólogo se trata de un hecho más de persecución política como ya otros movimientos sociales lo han señalado, “la situación penal del profesor Beltrán evidencia **para mí un caso de persecución política y una clara falta al debido proceso”.**

Así mismo resalta que el fallo de la Corte Suprema de Justicia que se refirió a la ilegalidad de las pruebas que provenían del computador de Raúl Reyes, por lo que en el 2011 el profesor Beltrán había sido absuelto de los delitos de concierto para delinquir agravado y rebelión.

Finalmente, **resalta que es incoherente que este tipo de actos se den, cuando el gobierno y la guerrilla de las FARC** buscan llegar a un acuerdo de paz, por lo que hace un llamado para que “esta situación de persecución política en contra del profesor Miguel Ángel Beltrán se resuelva en concordancia con el clima de paz que se está construyendo en Colombia”, dice en la carta, donde agrega que no se puede seguir persiguiendo a las personas por su perspectiva intelectual de izquierda.

Cabe recodar que tras ser capturado, el sociólogo Miguel Ángel Beltrán Villegas es acusado nuevamente por el delito de rebelión por lo que podría ser condenado hasta a 8 años de cárcel.

[Rechazo a Detención y Respeto Debido Proceso a Sociólogo Miguel Ángel Beltrán\_Presidente de La Republica\_He...](https://es.scribd.com/doc/273646631/Rechazo-a-Detencion-y-Respeto-Debido-Proceso-a-Sociologo-Miguel-Angel-Beltran-Presidente-de-La-Republica-Heike-Hansel "View Rechazo a Detención y Respeto Debido Proceso a Sociólogo Miguel Ángel Beltrán_Presidente de La Republica_Heike Hänsel on Scribd") by [Contagioradio](https://www.scribd.com/contagioradio "View Contagioradio's profile on Scribd")

<iframe id="doc_1675" class="scribd_iframe_embed" src="https://www.scribd.com/embeds/273646631/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-SSkJ3gHvRyS1i6HNxK9L&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="0.7080062794348508"></iframe>
