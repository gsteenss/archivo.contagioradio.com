Title: El último adiós a Samuel Recuero
Date: 2020-04-09 20:14
Author: CtgAdm
Category: Comunidad, yoreporto
Tags: Camelias, Zona humanitaria Camelias
Slug: el-ultimo-adios-a-samuel-recuero
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/04/Captura-de-pantalla-2020-04-09-a-las-19.48.13.png" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/04/Captura-de-pantalla-2020-04-09-a-las-19.47.49.png" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

Comunidades - José Francisco Alvárez

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El día 8 de abril, en la comunidad de Buenavista (Tesoro), se le dio el último adiós a el señor Samuel Recuero, miembro ancestral de esta comunidad, trabajador, luchador, una persona honesta, sincera, responsable, transparente, una persona ejemplar para esta comunidad.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Murió el 6 de abril del 2020, cuando acompañaba a sus compañeros en un día comunitario en la carretera, habían cortado un árbol para sacar unos tablones para arreglar un puente por donde pasarían las volquetas que cargan la arena que mejoraría la carretera, pero el árbol que cortaron cayó y desprendió otro árbol que se encontraba débil de raíz, este golpeó en la cabeza al señor Samuel Recuero, los compañeros lo sacaron a unos 75 metros aproximadamente donde se encontraba la carretera y ahí murió por el gran impacto que tuvo.

<!-- /wp:paragraph -->

<!-- wp:image {"id":83007,"sizeSlug":"large"} -->

<figure class="wp-block-image size-large">
![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/04/Captura-de-pantalla-2020-04-09-a-las-19.47.49.png){.wp-image-83007}  

<figcaption>
Entierro Samuel Recuero

</figcaption>
</figure>
<!-- /wp:image -->

<!-- wp:paragraph -->

**Samuel Recuero**, era Presidente de Trabajo, tenía 58 años, nació en 1962 y murió el 6 de abril del 2020, tuvo 5 hijos y 10 nietos, nació en montería Córdoba, murió en territorio de Camelias, sepultado en el cementerio de Buenavista el 8 de abril del 2020, murió luchando por su sueño, llevar la carretera a su comunidad adecuadamente. Samuel, no estaba en el equipo de trabajo del lunes 6, estaba programado para trabajar el día 7, así lo habían acordado en la comunidad, su voluntad y anhelo de aportar, lo llevo a ir ayudarle a sus compañeros el lunes. Ver: [Comunidades del Bajo Atrato denuncian incrementos de costos y actividad bananera](https://archivo.contagioradio.com/comunidades-del-bajo-atrato-denuncian-alza-de-precios-y-actividad-bananera-en-medio-de-la-cuarentena/)

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

A la comunidad de Buenavista le queda un vacío grande, pero también le quedan muchas enseñanzas del difunto Samuel Recuero, para la comunidad fue su héroe y siempre permanecera en sus mentes y corazones, esperan seguir luchando por los sueños de la comunidad que eran los sueños de Samuel.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Noticias escritas por las comunidades - Texto y fotos: José Francisco Alvárez, comunidad Las Camelias, Curbaradó.

<!-- /wp:paragraph -->
