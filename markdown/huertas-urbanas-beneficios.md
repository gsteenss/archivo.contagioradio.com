Title: Cinco aspectos generales que debes saber sobre las huertas urbanas
Date: 2018-02-08 12:45
Category: Nacional, yoreporto
Tags: agricultura urbana, huertas
Slug: huertas-urbanas-beneficios
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/02/Captura-de-pantalla-2015-09-18-a-las-12.28.40.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto:Ecoperiodismo 

###### 08 Feb 2017

###### María Fernanda Fuentes \#Yo reporto 

Actualmente, enfrentar los desafíos alimentarios del planeta es un reto, ya que, a diario nacen miles de personas en un espacio con recursos naturales limitados, por lo que abastecer de alimentos a toda la población, incrementa consigo la degradación de recursos y la pérdida de biodiversidad. Teniendo en cuenta esta situación, en los últimos el sector agrícola además de tener un auge, ha desarrollado diferentes alternativas con el fin de minimizar los impactos ambientales negativos ocasionados por dicha práctica.

Dentro de las diferentes formas de agricultura, se encuentra la agricultura urbana, una práctica que se originó a partir de la migración de campesinos a las ciudades, pero que hoy en día es ampliamente usada por la población en general, la cual se ha motivado a cultivar sus propias frutas y verduras para autoconsumo. La agricultura urbana, tiene como objetivo aprovechar materiales reutilizados o reciclados presentes en los hogares, establecer mecanismos de producción de abono, recolección de aguas lluvias y conservación de semillas, lo anterior, con el fin de garantizar la sostenibilidad de la huerta en el tiempo, así como la disminución de costos de manutención. A continuación, te enunciaremos cinco aspectos que debes tener en cuenta al momento de construir tu propia huerta.

1\. Ubicación: este aspecto es de gran relevancia, ya que, teniendo en cuenta las condiciones de temperatura, humedad, luminosidad, aire o pluviosidad, podrás determinar la especie de planta a sembrar. Dentro de los lugares más comunes para establecer la huerta se encuentran la terraza, el balcón las ventanas, patio interior o jardín, de igual manera, el lugar seleccionado debe ser un espacio seguro, así evitaremos alteraciones en el desarrollo de las plantas.

2\. Plantas: las plantas seleccionadas dependerán de las condiciones específicas según la ubicación. Las plantas que mejor se adaptan a las huertas urbanas en general son tomate, cilantro, pimentón, albaca, orégano, lechuga, menta y otras plantas medicinales.

3\. Recipientes: como se mencionó anteriormente, las huertas urbanas nos permiten contribuir al ambiente y a nuestra economía, es por esto que en el momento de elegir un recipiente recomendamos ser creativos, un balde o caneca que ya no utilicemos puede ser una buena opción.

4\. Sustrato: el sustrato que emplees debe ser blando y de estructura esponjosa, con eso nos aseguraremos de que el agua no se encharque y por ende, no se afecten las plantas. Así mismo, puedes mejorar tu sustrato y el desarrollo de tus plantas empleando algún tipo de abono, que aunque existan comerciales, lo ideal es que con el tiempo puedas fabricarlo tú mismo.

5\. Posibles plagas: aunque nuestra huerta este en la ciudad, también es posible encontrar ciertas plagas o depredadores en las plantas, no obstante, antes de tomar alguna medida es recomendable indagar acerca de ellas ya que, pueden contribuir al correcto desarrollo de la huerta. Así mismo, es importante incluir en la huerta una planta con flores, con el objetivo de atraer polinizadores que fecunden las plantas.

Recuerda que aunque construir una huerta urbana pueda parecer un reto, teniendo en cuenta las condiciones de la ciudad y sus diferencias respecto al campo, cultivar alimentos propios en casa trae grandes beneficios, no solo económicos o ambientales, pues resulta ser una gran actividad para compartir en familia ¡Anímate!
