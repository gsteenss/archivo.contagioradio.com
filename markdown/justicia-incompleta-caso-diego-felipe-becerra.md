Title: Justicia incompleta en caso de Diego Felipe Becerra
Date: 2017-01-13 17:41
Category: DDHH
Tags: “Trípido”, crímenes de estado, Diego Felipe Becerra, Nelson Tovar, Policía Nacional, Wilmer Acosta
Slug: justicia-incompleta-caso-diego-felipe-becerra
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/01/Diego-Felipe-Becerra.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Wide Walls] 

###### [13 Enero 2017] 

Aunque este viernes se conoció la condena contra el Sub Intendente de la Policía, Nelson Tovar, por su participación en la alteración de la escena del crimen de Diego Felipe Becerra, la familia del joven sigue enfrentando la lentitud de la justicia y las dilaciones del caso. Además se sigue a la espera de la re captura de Wilmer Alarcón, patrullero que asesinó al joven, prófugo de la justicia desde el 18 de agosto pasado.

[El Sub Intendente fue condenado a 8 años de prisión domiciliaria](https://archivo.contagioradio.com/patrullero-admitio-haber-alterado-escena-del-crimen-de-diego-felipe-becerra/) por los delitos de favorecimiento al homicidio, fraude procesal y alteración de elementos materiales probatorios.

### La ley se contradice 

En agosto de 2016, el juez 47 de conocimiento ordenó la libertad del patrullero Wilmer Antonio Alarcón puesto que el acusado alegó vencimiento de términos, justo ese mismo día, la Fiscalía y la Procuraduría habían solicitado al juzgado 43 de conocimiento que se emitiera el fallo condenatorio contra el patrullero por el homicidio agravado del joven Diego Felipe Becerra.

Pese a que reza una condena de 33 y 50 años de prisión contra Alarcón, éste se encuentra prófugo desde el 18 de agosto, [fecha en que el juez 47 de conocimiento ordenara su libertad.](https://archivo.contagioradio.com/ordenan-libertad-para-patrullero-que-disparo-a-diego-felipe-becerra/)

De acuerdo con Gustavo Trejos, padre de Diego Felipe en la alteración de la escena del crimen, hay 13 personas implicadas, de las cuales tres son civiles y el resto policías. 9 de ellos han decidido contar la verdad incluyendo el testimonio del subintendente Giovanny Tovar quien el 18 de julio de 2016  reconoció que llevó el arma con la que pretendieron justificar el asesinato.

El padre de Diego Felipe asevera que "después de tener 13 personas capturadas, ahora todas están en libertad, sólo hay dos patrulleros detenidos**,** mientras los coroneles están trabajando como si no hubiera pasado nada”. Le puede interesar: [Policías reconocen que alteraron la escena del crimen de Diego Felipe.](https://archivo.contagioradio.com/policia-nacional-reconoce-que-altero-la-escena-del-crimen-de-diego-felipe-becerra/)

Los familiares de Diego Felipe Becerra están a la espera de las conclusiones de una nueva audiencia para retomar el caso del patrullero prófugo Wilmer Alarcón, el próximo 18 de Enero y de lo que arrojen las demás audiencias a otros testigos claves en el caso, que lleva más de 5 años y sólo 2 personas condenadas, de un total de 13 implicadas.

###### Reciba toda la información de Contagio Radio en [[su correo]
