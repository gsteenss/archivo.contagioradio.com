Title: Conozca el nuevo Protocolo sobre la Protesta Social
Date: 2018-07-31 17:53
Category: DDHH, Nacional
Tags: ESMAD, Juan Manuel Santos, Ministerio del Interior, Protesta social, Protocolo
Slug: protocolo-sobre-protesta-social
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/12/mujeres.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [21 Jul 2018] 

El Protocolo "para articular acciones de prevención, seguridad, respeto, gestión y atención de las situaciones que se dan en el marco del ejercicio de la protesta pacífica", que fue presentado por el Presidente Juan Manuel Santos, y que se concertó con organizaciones sociales, así como gremios del sector industrial, tiene por objetivo ser una "guía metodológica" que parte de la legislación vigente para garantizar el derecho a la manifestación.

Aunque el documento fue objeto de criticas por parte de los Gremios que participaron en el proceso de concertación, quienes señalaron que se estaba limitando la actuación de la fuerza pública, en realidad, es un acta en la que se compilan las leyes que garantizan las manifestaciones de acuerdo con la Constitución Política de Colombia; las sentencias emitidas al respecto por la Corte Constitucional; la Declaración Universal de los Derechos Humanos; y lo expresado en materia de participación ciudadana en el Acuerdo Final para la Paz.

Según Jomari Ortegón, abogada de del Colectivo de Abogados José Alvear Restrepo (CAJAR), el protocolo surge como una forma de dar a conocer los derechos y deberes tanto de ciudadanos, como de la Policía, en el desarrollo de la protesta social. Adicionalmente, fue concertada en un proceso en el que hubo participación por parte de los Gremios, razón por la cuál, Ortegón no se explica porque se opusieron a ese protocolo.

Adicionalmente, el Protocolo se expide tras las declaraciones de Guillermo Botero, anunciado Ministro de Defensa del Gobierno Duque, en las que pedía que se reglamentara la protesta social; hecho que para Ortegón significa que se quiere limitar las manifestaciones pacíficas. (Le puede interesar: ["ESMAD reprime protestas contra proyectos petroleros en Sáchica, Boyacá"](https://archivo.contagioradio.com/esmad-reprime-protestas-contra-proyectos-petroleros-en-sachica-boyaca/))

En el protocolo se afirma que las autoridades locales deberán garantizar el ejercicio de protesta, pero siempre cuidando que no se vean afectados "los derechos de terceros como el trabajo, la libre circulación, la integridad personal y la propiedad privada". De igual forma recuerda que solo el poder legislativo puede limitar el derecho de reunión y manifestación, pero que la "naturaleza del derecho de reunión, en sí mismo conflictivo, no puede ser la causa justificativa de normas limitativas del mismo".

### **Las autoridades competentes pueden crear una mesa de seguimiento a la protesta** 

El Protocolo propone la creación por parte de las autoridades locales, departamentales o nacionales, de una mesa de seguimiento y garantía de la protesta pacífica, integrada por los máximos entes de cada jurisdicción, así como representantes de organizaciones y movimientos sociales; dicho ente podría ser garante de Derechos Humanos en el desarrollo de protestas.

Igualmente, se define al Punto de Mando Unificado (PMU), como una instancia que puede ser activada por parte de las máximas autoridades locales, regionales o nacionales en materia de Garantía de Participación Ciudadana (alcaldes, gobernadores y viceministro de participación ciudadana), como instrumento de coordinación inter-institucional en el desarrollo de una protesta.

Adicionalmente, se comunica sobre la posibilidad que tiene la sociedad civil de organizarse mediante comisiones de verificación para difundir y velar que se cumplan los elementos del Protocolo. Gracias a esta Comisión, las personas que la integran pueden ser garantes de derechos en medio de protestas, marchas o plantones y ser intermediarios en conjunto con los funcionarios públicos en caso de presentarse hechos violencia.

### **Papel de la Fuerza Pública en las protestas** 

En el oficio se aclara también que siempre deberán haber gestores de convivencia o funcionarios delegados acompañando el desarrollo de las protestas pacificas, y en el transcurso de la misma. Las entidades públicas deben informar al PMU las alteraciones que se presenten, con un debido proceso que de cuenta de dichas novedades.

Cuando ocurran disturbios, "corresponde a la autoridad competente tomar las medidas orientadas a controlar la situación de tal manera que se proceda a proteger y garantizar los derechos fundamentales de todos los ciudadanos", es decir, que en caso de intervención por parte del ESMAD, se debe garantizar la vida tanto de protestantes como de aquellos que se vean implicados en actos de violencia.

Sobre la actuación de la Fuerza Pública, el protocolo afirma que el uso de la fuerza debe ser el último recurso de la intervención; la "actuación de la Policía Nacional deberá ser desarrollada en todo momento mediante personal y equipos identificados de manera clara y visible"; las Fuerzas Militares no pueden intervenir de ninguna forma en las movilizaciones sociales salvo en los casos excepcionales que autoriza la Constitución; los funcionarios de la Policía no pueden portar armas de fuego; y que las personas capturadas o trasladadas por procedimiento policial deben ser tratadas con dignidad y respeto de sus derechos.

Sobre los materiales que pueden utilizar los agentes del ESMAD, el protocolo señala que el Ministerio Público, mediante un oficio o solicitud a la autoridad competente "podrá solicitar la verificación in situ de los elementos de dotación -de los uniformados- según los reglamentos internos de la Policía Nacional. Finalmente, el documento concluye que "ninguna disposición del presente protocolo podrá ser interpretada de manera que menoscabe los derechos humanos".

### **Nancy Patricia Gutiérrez, nueva Ministra del Interior** 

Ante la llegada del nuevo gabinete ministerial que tiene entre sus agendas la participación ciudadana  (Guillermo Botero, ministro de Defensa y Nancy Patricia Gutiérrez, ministra de Interior), Jomari Ortegón espera que de una parte, la política de limitación de la protesta no sea un lineamiento del nuevo Gobierno, y de otra que se cumplan los compromisos establecidos con el Ministerio de Interior, entre los cuales está mantener la Mesa Nacional de Garantías.

Específicamente, sobre Nancy Patricia Gutiérrez, Jomari afirmó que no sabe si será una protectora de los Derechos Humanos, y dijo no sentir confianza en su gestión; sin embargo, aclaró que estarán dispuestos a dialogar con el nuevo Gobierno para lograr concertar un Proyecto de Ley que garantice la protesta social, de acuerdo a lo ordenado por la Corte Constitucional, y lo pactado en el Acuerdo Final de Paz.

### **ONU y PNUD saludan la firma de este protocolo** 

En un comunicado emitido el 3 de agosto, la Oficina en Colombia del Alto Comisionado de las Naciones Unidas para los Derechos Humanos (ONU Derechos Humanos), y el Programa de las Naciones Unidas para el Desarrollo (PNUD) saludaron el Protocolo y dijeron que "contribuirá a la aplicación de estándares internacionales de derechos humanos" en materia de protesta pacífica.

[ONU y PNUD apoyan protocolo de protesta pacífica](https://www.scribd.com/document/385588704/ONU-y-PNUD-apoyan-protocolo-de-protesta-pacifica#from_embed "View ONU y PNUD apoyan protocolo de protesta pacífica on Scribd") by [Contagioradio](https://www.scribd.com/user/276652802/Contagioradio#from_embed "View Contagioradio's profile on Scribd") on Scribd

<iframe id="doc_32270" class="scribd_iframe_embed" title="ONU y PNUD apoyan protocolo de protesta pacífica" src="https://www.scribd.com/embeds/385588704/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-G9y8ZhIegXf1OdbPiROS&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="0.7729220222793488"></iframe><iframe id="audio_27619577" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_27619577_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en] [su correo](http://bit.ly/1nvAO4u) [o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por] [Contagio Radio](http://bit.ly/1ICYhVU) 
