Title: Condenan al ejército por la "Masacre del Rozo" en el Valle
Date: 2015-02-10 20:07
Author: CtgAdm
Category: DDHH, Judicial, Paz
Tags: Ejecucion extrajudicial, FFMM, ministerio de defensa, tribunal administrativo del valle
Slug: condenado-ejercito-por-masacre-del-rozo
Status: published

###### Foto: minuto30 

Mediante la Sentencia No. 3 de 27 de enero de 2015, el Tribunal Administrativo del Valle del Cauca, condenó al Ministerio de Defensa y al Ejército Nacional, por los perjuicios ocasionados a los familiares de **Jonathan Bedoya Sierra**, asesinado en un falso operativo militar junto a cuatro jóvenes más, el **5 de agosto de 2008.**

El Tribunal dispuso que *“En estos eventos es necesario formular las pertinentes advertencias a la Administración con el fin de que procure evitar la reiteración de conductas anormales y para que la decisión asumida por la justicia contenciosa administrativa sirva para trazar políticas públicas en materia de administración.”.* En tal sentido ordenó **publicar la sentencia al ministerio de Defensa y a todas las brigadas del país**, informa la **Corporación Justicia y Dignidad de Cali.**

Por el crimen se encuentran vinculados al proceso penal los suboficiales **Bernardo Antonio Londoño Corrales y Edgar Hernan Velasco Prada, así como los mayores del ejército Yimmy Alberto Guerrero Alvarez Y Ricardo Gonzalez Pinto**, estos últimos privados de la libertad.

Según la denuncia de la organización de Derechos Humanos resulta preocupante que el **Sargento Julio Cesar Mahecha Prieto**, testigo de los hechos, haya sido amenazado de muerte por los implicados y que el Estado no le haya brindado la protección suficiente para proteger su vida y la de su familia, además se conoció que el sargento fue retirado de la institución de manera sospechosa en meses anteriores.

La organización de Derechos Humanos Justicia y Dignidad afirma que "*ojalá, de verdad, estas decisiones sirvieran para que los altos mandos civiles y militares en un acto de contrición desmontaran su perversa doctrina y consideraran a sus hermanos colombianos como personas dignas de vivir y no como seres despreciables que merecen morir bajo su cañón*."
