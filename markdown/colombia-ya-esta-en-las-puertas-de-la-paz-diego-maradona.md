Title: "Colombia ya está en las puertas de la Paz" Diego Maradona
Date: 2015-04-13 11:16
Author: CtgAdm
Category: Nacional, Paz
Tags: 9 de abril, cumbre de arte y cultura, cumbre por la paz, habana, maradona, Partido por la Paz, paz, piedad cordoba
Slug: colombia-ya-esta-en-las-puertas-de-la-paz-diego-maradona
Status: published

###### Foto: Contagio Radio 

Diego Armando Maradona viajó a Colombia para participar en el Partido Por la Paz, del cual fue un importante convocante por medio de su programa de televisión "De Zurda", junto a la ex-senadora y defensora de Derechos Humanos, Piedad Córdoba, e instituciones del distrito como IDARTES y el IDRD.  
"Colombia ya está en las puertas de la Paz", fueron las palabras de Diego Maradona para Colombia.  
   
\[embed\]https://www.youtube.com/watch?v=ijatopV-Oxk\[/embed\]
