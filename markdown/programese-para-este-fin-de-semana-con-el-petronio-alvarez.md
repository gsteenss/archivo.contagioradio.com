Title: Prográmese para este fin de semana con el "Petronio Álvarez"
Date: 2015-08-14 11:52
Author: AdminContagio
Category: Cultura, eventos
Tags: Cali, Costa pacífica, folclor pacífico, Germán Patiño, Pacífico colombiano, XIX Festival de Música del Pacífico Petronio
Slug: programese-para-este-fin-de-semana-con-el-petronio-alvarez
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/08/petroni1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: www.cbonlinecali.com]

Hasta el 16 de agosto se realizará el **XIX Festival de Música del Pacífico Petronio Álvarez,** uno de los eventos más importantes del año, sobre lo que tiene que ver con  el folclor del pacífico colombiano.

En la ciudadela Petronio, en las Canchas Panamericanas, los visitantes podrán encontrar variedad de actividades en el marco de las tradiciones del pacífico colombiano, allí habrá un amplio menú de **comidas, bebidas tradicionales, artesanías, peinados y moda de esta región del país.**

En la ciudadela se han implementado dos pantallas para mostrar las actividades y conciertos que se desarrollarán en el Petronio, que terminará con un **gran homenaje al creador del Petronio Álvarez, Germán Patiño Ossa.**

Aquí les presentamos la programación durante este fin de semana.

Viernes 14 de Agosto:

-   11:30 a 1:30 pm: Prácticas de lutéria Conversatorio sobre la construcción de la marimba y el violín tradicional. También habrá una exposición de instrumento en el Auditorio José Fernando Arroyo Valencia - Escuela Nacional del Deporte.
-   2 a 5 pm: Exposición permanente Archivo Histórico de Cali Exploración del Pacífico como fuente historiográfica, la muerte como ritual musical en el Pacífico. Centro Cultural de Cali.
-   6 a 11 pm: Clasificatorias Modalidades: Marimba, Chirimía, Violín Caucano, versión Libre. En la Unidad Deportiva Panamericana.

Sábado 15 de agosto:

-   9 a 11 am: Presentación Documental Unesco violines caucanos Panel de expertos - La música de chirimía en el Pacífico a partir de la declaratoria de la Unesco en el Auditorio José Fernando Arroyo Valencia - Escuela Nacional del Deporte.
-   11:30 a 1:30 pm: Ritmos del Pacífico norte. Conversatorio a cargo de músicos tradicionales con invitado en modalidad libre. Intervienen algunos grupos participantes en modalidad chirimía. Clausura y certificación en el Auditorio José Fernando Arroyo Valencia - Escuela Nacional del Deporte.
-   2 a 5 pm:  Exposición permanente Archivo Histórico de Cali. Exploración del Pacífico como fuente historiográfica, la muerte como ritual musical en el Pacífico. Centro Cultural de Cali.
-   2:30 pm: Balsada de la Virgen de la Asunción (Playa Renaciente Corregimiento Navarro, costado derecho del puente de Juanchito)
-   6 a 11 pm: Noche Internacional. Este año el Petronio contará con la participación del brasileño Carlinhos Brown.
-   Se presentarán los grupos ganadores del año anterior por modalidad: Marimba: Fundación Folclórica Changó de Tumaco,Chirimía: Mi raza de Cali, Versión Libre:La Chiribanda del Chocó,Violín Caucano: Son Balanta de Santander de Quilichao

Domingo 16 de agosto:

-   6 a 11 pm: Homenaje póstumo al creador del Festival Germán Patiño Ossa.
-   Ensamble Pacífico: Montaje musical con ritmos de Pacífico colombiano, acompañado de cantadoras e instrumentos del Pacífico.

