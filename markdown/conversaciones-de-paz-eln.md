Title: ¿Por qué el Gobierno ni avanza ni rompe conversaciones con el ELN?
Date: 2018-12-17 12:02
Author: AdminContagio
Category: Paz, Política
Tags: cese unilateral, colombia, ELN, Iván Duque
Slug: conversaciones-de-paz-eln
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/12/Captura-de-pantalla-2018-09-03-a-las-12.46.46-p.m.-770x400.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo Particular] 

###### [17 Dic 2018] 

En un comunicado de prensa el Ejército de Liberación Nacional anunció que realizará un cese al fuego unilateral desde el [23 de diciembre de 2018 al 3 de enero de 2019], hecho que para el analista Víctor de Currea, podría ser un **mensaje sin atender por parte del gobierno Duque, debido a la falta de voluntad que este ha demostrado en continuar con las conversaciones.**

<iframe src="https://co.ivoox.com/es/player_ek_30848636_2_1.html?data=lJWllp2ad5ehhpywj5aUaZS1lJWah5yncZOhhpywj5WRaZi3jpWah5yncbfdxNnc1JDIqYy31tffx8aJdqSf1NTP1MqPsMKf1dfSydrFb8_V187Rx4qnd4a2ksaYxsrQb6bAr5KSmaiRh9Di1cbUy9SPlsLYytSYj4qbh46o&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

Para Monseñor Darío de Jesús Monsalve esta acción hace parte de **"un momento en el que se acaricia la paz, como estilo de vida"** y el ELN no estaría ajeno a esas festividades, además expresó que Colombia esta cansada de la guerra, razón por la cual la sociedad en general debe apoyar este proceso.

**"El gobierno no quiere continuar con la mesa pero no tiene la voluntad para seguirla" **

"Realmente si el presidente Duque no quiere negociar con el ELN, por qué no rompe la mesa" preguntó De Currea, y afirmó que la razón de fondo sería económica, porque el Fondo Monetario ya dejó claro que lo peor que puede hacer el primer mandatario para des estimular la inversión extranjera es dejar de lado las conversaciones, mientras que cooperación internacional ha puesto varios millones en la mesa advirtiendo que **"hay que seguir las negociaciones".**

De igual forma, tanto de Currea como Monseñor Monsalve, consideran preocupante la actitud del Alto Comisionado para la Paz, Miguel Ceballos, debido a que "impone el criterio de la legalidad, sin definir una política de paz o diálogo, ni dar la certeza al pueblo colombiano de la continuidad de este proceso de Estado".

Asimismo, De Currea señaló que hay otras presiones que recaen sobre Duque, una de ellas es que algunos de los integrantes de la guerrilla ya han regresado al país. Entretanto las encuestas muestran que** el 68% de colombianos quieren que  prosigan los diálogos con el ELN. **([Le puede interesar: "Se acaba el tiempo para reanudar la mesa de conversaciones con el ELN"](https://archivo.contagioradio.com/mesa-conversaciones-eln/))

<iframe src="https://co.ivoox.com/es/player_ek_30848684_2_1.html?data=lJWllp2afJWhhpywj5WZaZS1lJeah5yncZOhhpywj5WRaZi3jpWah5ynca7jz9jSh6iXaaOl0NeYpsbWaaSnhqax0ZCxs8_nwtHjx4qWh4zn0Mffx5DHqdTZjMnSjdTKqc_nytvO1ZDIqc2fjoqkpZKns8_owszW0ZC2pcXd0JCah5yncZU.&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

Para Monseñor Darío Monsalve "el pueblo colombiano tiene que reflexionar sobre qué se esta pretendiendo hacer con la paz, qué pretenden los actores armados y qué pretenden los gobierno que polarizan y exageran lo político para mantener réditos" y aunque lamentó que no se haya firmado el cese bilateral durante este año, aseguró que aguarda la esperanza porque sea la sociedad colombiana la que "tenga una visión de futuro" y paz.

###### Reciba toda la información de Contagio Radio en [[su correo]
