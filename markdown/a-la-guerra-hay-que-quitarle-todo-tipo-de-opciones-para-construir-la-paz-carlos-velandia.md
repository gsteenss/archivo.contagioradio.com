Title: "A la guerra hay que quitarle todo tipo de opciones para construir la paz": Carlos Velandia
Date: 2016-08-02 12:01
Category: Otra Mirada, Paz
Tags: Carlos Arturo Velandia, Crisis carcelaria colombia, Negociaciones con ELN
Slug: a-la-guerra-hay-que-quitarle-todo-tipo-de-opciones-para-construir-la-paz-carlos-velandia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/08/Carlos-Velandía.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: CSPPP] 

###### [2 Ago 2016 ] 

"Nunca pensé que después de haber estado diez años en prisión y haber saldado todas mis cuentas pendientes con el Estado y con la sociedad tuviera que volver a ir a la cárcel, exactamente por las causas, razones y hechos por las que me llevaron la primera ocasión", asegura Carlos Arturo Velandía, quien **luego de cuarenta días de estar recluido vuelve a la libertad** en el marco de la gestoría de paz designada por el presidente Juan Manuel Santos.

Esta gestoría que ordenó la libertad de Velandía, suspende temporalmente las ordenes de captura en su contra pero el proceso continúa su marcha y en ese sentido él está obligado a **asistir a las diligencias y requerimientos que le exija la justicia colombiana**.

"Estar en la cárcel no es nada grato (...) allí se sufre (...) en el tiempo breve que estuve pude conectarme con la [[situación que están viviendo muchos presos](https://archivo.contagioradio.com/internos-de-la-picota-completan-una-semana-de-desobediencia-civil/)] en el país, del drama humano que padecen, el hacinamiento y las injusticias, aunque también pude recibir la solidaridad de mucha gente que han entendido **lo injusto del proceso que se me ha abierto**", agrega Velandía.

El gestor de paz concluye aseverando que lo inmediato es poderse reunir con la oficina del Alto Comisionado para cuadrar la agenda de trabajo en favor de las negociaciones con el ELN que no han podido iniciar formalmente y que necesitan que tanto el Gobierno como esta guerrilla tramiten políticamente sus diferencias porque **por encima de todo está la sociedad y la consolidación de una paz completa**.

<iframe src="http://co.ivoox.com/es/player_ej_12417146_2_1.html?data=kpehk5yVeJehhpywj5WdaZS1lpyah5yncZOhhpywj5WRaZi3jpWah5yncaTV09Hc1ZC6qc3Vz8nWw4qWh4y7xtjh0dePqMaf0cbnj4qbh4630NPhw8zNs4zGwsnW0ZCRaZi3jpk.&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)] ] 
