Title: En las próximas horas se abriría la frontera entre Colombia y Venezuela para paso de carga
Date: 2015-09-03 16:11
Category: El mundo, Nacional, Política
Tags: colombia, Conversaciones de paz de la habana, Crisis de la frontera, gobierno de Nicolás Maduro, Juan Manuel Santos, Venezuela
Slug: en-las-proximas-horas-se-abriria-la-frontera-entre-colombia-y-venezuela-para-paso-de-carga
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/cucuta_venezuela_frontera_contagioradio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: eluniversal 

###### [3 Sep 2015]

El General Carlos Martínez, Comandante de la Autoridad Única Militar de Táchira afirmó en un encuentro con oficiales que **en las próximas horas se abrirá el paso para transporte de carga en ese estado de Venezuela como parte de las medidas de normalización de las actividades** propias de la frontera. Además se abriría el paso a los **trabajadores municipales de Bolívar y Ureña que tengan carnet de identificación autorizado**.

Por su parte el presidente **Juan Manuel Santos afirmó que está dispuesto a reunirse con Nicolás Maduro** siempre y cuando se garanticen los derechos humanos de los habitantes de la frontera y se abra un corredor humanitario para que más de 2000 niños vuelvan a clases.

El presidente Juan Manuel Santos aseguró que otra de las situaciones más preocupante es la manera como se está realizando la **deportación de nacionales colombianos e insistió en la necesidad de definir y respetar unos protocolos.**

De otro lado el presidente de Venezuela, Nicolás Maduro se reunió en la mañana de este Jueves con el secretario general de las Naciones Unidas, Ban Ki-moon para ponerlo al tanto de la situación que desde hace varios días se vive en la zona de frontera.

Desde el pasado 22 de Agosto el gobierno de Venezuela decidió tomar medidas para combatir el problema de la **presencia paramilitar colombiana en la frontera, así como identificar y evidenciar los actores que generan y controlan el mercado ilegal de combustibles y alimentos.**

Diversos analistas han señalado que tanto la gasolina venezolana como los productos regulados del mercado venezolano han abastecido la zona de frontera en Colombia dada la inoperancia de las autoridades colombianas. Algunas cifras que dan cuenta del abandono Estatal en **Cúcuta son  la informalidad laboral que marcó en enero fue de 71% y el trimestre agosto-octubre 65,9% por ciento, siendo la ciudad más informal del país.**
