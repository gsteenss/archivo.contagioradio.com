Title: "En este momento en Colombia, no es legal hacer fracking"
Date: 2019-05-28 16:00
Author: CtgAdm
Category: Ambiente, Nacional
Tags: Alianza Colombia Libre del Fracking, Consejo de Estado, Ecopetrol, fracking
Slug: en-este-momento-en-colombia-no-es-legal-hacer-fracking
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/10/fracking-san-martin.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Gert Steenssens / EsperanzaProxima.net] 

Organizaciones ambientales rechazaron este martes las declaraciones recientes de Ecopetrol, en que anunciaron estar listos para la segunda fase de proyectos piloto de fracking, en  regiones como el Magdalena Medio.

En una rueda de prensa en el Congreso, grupos como la Alianza Colombia Libre del Fracking, la [Asociación Interamericana para la Defensa del Ambiente] y la [Corporación Geoambiental Terrae]aseguraron **que cualquier actividad de exploración o explotación de yacimientos no convencionales son ilegales debido a una moratoria interpuesta por el Consejo de Estado**.

["Si realizan cualquier actividad de fracking, es ilegal y los funcionarios tendrán que responder penalmente y disciplinariamente. En el terreno, haremos valer esta decisión del Consejo de Estado porque estarían incumpliendo", declaró Carlos Andrés Santiago, vocero de la Alianza Colombia Libre del Fracking. (Le puede interesar: "[Comunidades rechazan el avance de proyectos piloto de fracking](https://archivo.contagioradio.com/comunidades-rechazan-avance-proyectos-piloto-fracking/)")]

En las últimas semanas, Ecopetrol, la Agencia Nacional de Hidrocarburos y el Ministerio de Minas y Energía se han pronunciado en favor del uso de este técnica no convencional, argumentando que se puede realizar de manera responsable y que es necesario para garantizar la seguridad energética del país. En particular, el presidente de Ecopetrol Felipe Bayón anunció este martes que **la empresa esta lista para iniciar la fase de exploración de yacimientos no convencionales**.

Sin embargo, como lo indicaron varios grupos ambientales presentes en la rueda de prensa, **la suspensión de las normas que regulan el fracking siguen vigentes**. Según la sentencia, el Alto Tribunal consideró pertinente las conclusiones de la Contraloría en el 2012, en las que advirtió que "el aumento de la sismicidad, la contaminación hídrica y la consecuente afectación de la salubridad provocadas por el *fracking* eran potencialmente riesgosas".

La moratoria corresponde a una demanda interpuesta por el Grupo de Litigio e Interés Público de la Universidad del Norte, que está en curso desde el 2016. El próximo 7 de junio se llevará a cabo la audiencia inicial para reanudar este proceso que busca la nulidad de estas normas, es decir, el Decreto 3004 del 2013 y la Resolución 90341 del 2014 del Ministerio de Minas. Para acompañar esta audiencia, ciudadanos en **más de 100 municipios saldrá a las calles para manifestarse en contra del fracking y la minería contaminante**.

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/video.php?href=https%3A%2F%2Fwww.facebook.com%2FAlianzaColombiaLibreDeFracking%2Fvideos%2F350665142308920%2F&amp;show_text=0&amp;width=560" width="560" height="315" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
