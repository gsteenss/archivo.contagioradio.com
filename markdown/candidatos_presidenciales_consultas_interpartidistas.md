Title: ¿Cómo quedarían repartidos los votos tras las consultas interpartidistas?
Date: 2018-03-12 21:53
Category: Entrevistas, Política
Tags: Cambio Radical, Centro Democrático, Consultas interpartidistas, elecciones 2018, Germán Vargas Lleras, Gustavo Petro, Iván Duque
Slug: candidatos_presidenciales_consultas_interpartidistas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/03/Diseño-sin-título-11.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Colprensa ] 

###### [12 Mar 2018] 

Como era de esperarse el panorama parece un poco más claro tras las consultas interpartidistas celebradas este 11 de marzo. Por un lado, **Gustavo Petro quedó como el candidato más fuerte de la izquierda con el 84% de los votos, mientras Iván Duque será el candidato de la coalición de la derecha con el 65% de los votos.**

De acuerdo con el analista  periodista Mario López,  lo que ha podido quedar más evidente tras las consultas es que ambos candidatos tienen grandes posibilidades de pasar a una segunda vuelta, "Son consultas que pueden estar entre 8 y 9 millones lo que puede significar que le tomarán ventaja considerable a otros candidatos para la segunda vuelta", dice.

De hecho, Fernando Giraldo, analista político, señala que esta consulta **"dejó menos tranquilo a Germán Vargas Lleras, porque tiene que enfrentarse con dos candidatos fuertes** para pasar a la segunda vuelta de las presidenciales. Petro tiene casi que asegurada la segunda vuelta", dice.

### **Así podrían repartirse los votos** 

<iframe src="https://co.ivoox.com/es/player_ek_24375773_2_1.html?data=k5mgmZqbe5Shhpywj5WZaZS1kpqah5yncZOhhpywj5WRaZi3jpWah5ynca7V087cjbGJh5SZo5jdx9-JdqSf0crfy9TIrdTowpCah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ..&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

Para el analista López, es necesario relacionar el voto del Uribismo con los votos de su lista al senado de manera que se pueda saber cuáles son los votos de esa coalición. "En razón de la polarización muy seguramente no votarán los mismos que votaron en la consulta por Duque en la primera vuelta". Esto, ya que los votos del candidato del Centro Democrático, incluyen votos de partidos como Cambio Radical, del Partido de la U, del Partido Liberal o los cristianos que ya tienen un candidato a la presidencia.

Por su parte, **respecto a Petro, la ganancia en votos es evidente respecto a años anteriores.** Hoy si se suman los votos por Caicedo y Petro, da un total cercano a los 3'400.000. "Petro no tiene mucho voto prestado, y con eso pasaría la primera vuelta. Por él votó su sector de la izquierda, del Polo y del Verde, y a su vez hay voto de opinión, indudablemente", expresa el periodista.

Es decir que, según López, mientras probablemente Iván Duque en la primera vuelta descuente votos, es posible que Gustavo Petro logre tener votos adicionales. Esto, teniendo en cuenta que el candidato del uribismo tendrá que competir con candidatos de derecha en mayo.

Así también lo afirma Fernando Giraldo, que no ve tan probable la segunda vuelta para Duque, salvo que logre un acuerdo con Germán Vargas Lleras, aunque reconoce que el candidato de la derecha necesita menos votos para pasar a la segunda vuelta que lo que necesitaría Petro.

### **La Coalición como condición para gobernar ** 

Para Giraldo, **el presidente que quede, necesitará de una coalición en el Congreso de la República para poder gobernar **de por lo menos tres partidos políticos. Frente  eso resalta que la 'U' tuvo una "estruendosa pérdida", mientras que Cambio Radical ganó, y aunque una mayoría se mantiene, sostiene que el Congreso continuará en una situación similar a la que ya venía, aunque reconoce que "la izquierda avanzó un poco".

<iframe src="https://co.ivoox.com/es/player_ek_24375749_2_1.html?data=k5mgmZqbeJqhhpywj5WZaZS1lpWah5yncZOhhpywj5WRaZi3jpWah5yncafZ09PO0MnTb6jd08bZxtSJdqSfwtPOzoqnd4a1pdjhw5DUs82ZpJiSo6nYrcTjjoqkpZKns8_owszW0ZC2pcXd0JCah5yncZU.&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio]{.s1}](http://bit.ly/1ICYhVU)[.]{.s1}
