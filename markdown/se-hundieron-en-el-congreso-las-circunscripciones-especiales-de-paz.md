Title: Se hundieron en el congreso las 16 Circunscripciones Especiales de Paz
Date: 2017-12-06 19:04
Category: Nacional, Paz
Tags: circunscripciones especiales de paz, Senado
Slug: se-hundieron-en-el-congreso-las-circunscripciones-especiales-de-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/03/senado_farc_operación-tortuga.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: honduradio] 

###### [6 Dic 2017] 

En una decisión anunciada por Efraín Cepeda este miércoles, el senado dio por terminado el trámite del acto legislativo que creaba las Circunscripciones Especiales de Paz. Según el congresista, s**e analizaron las sentencias y la constitución y no se pudo dar por aprobado el acto por ausencia del quorum necesario.** Sin embargo las víctimas y el gobierno han anunciado otras medidas para lograr la creación de las curules para las comunidades más afectadas por el conflicto y el abandono estatal.

Para Cepeda, e**l proyecto no obtuvo las mayorías necesarias,** ya que para él el quórum no era de 99 sino de 102 senadores. Asimismo dijo que los **ponentes de la iniciativa debieron haber objetado esa decisión del senado durante la misma sesión** en plenaria del senado.

Mientras que los sectores de víctimas y de Organizaciones de Derechos Humanos han anunciado una “tutelatón” basada en el derecho a la participación política. Para ellos y ellas no se puede negar este derecho fundamental con argumentos como los expuestos por el Centro Democrático o Cambio Radical en el sentido de que esas curules serían para las FARC.

Por otro lado, el gobierno ha dejado ver la posibilidad de que el acto legislativo sea revisado por el Consejo de Estado y se ordene su aprobación. Sin embargo, según algunos voceros de las víctimas, estas medidas deberán estar acompañadas de una **exigencia de prórroga para la inscripción de candidatos que se cierra el próximo 11 de Diciembre.** Además debería hacerse la solicitud a la registraduría para que no cierre el trámite que ubicará los candidatos en los tarjetones y las mesas respectivas.

Luego de conocer la decisión, el Presidente  Juan Manuel Santos expresó que el siguiente paso será acudir a las cortes "la democracia establece caminos para dirimir los problemas entre el legislativo y el ejecutivo. Y esas son las cortes. Nos comprometimos con las víctimas".

*Aquí el anuncio completo del presidente del Senado*

<iframe id="doc_45134" class="scribd_iframe_embed" title="DECLARACIÓN CIRCUNSCRIPCIONES." src="https://www.scribd.com/embeds/366532079/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-DyDAWNxuoxM6BdlUSxHz&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="0.7729220222793488"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
