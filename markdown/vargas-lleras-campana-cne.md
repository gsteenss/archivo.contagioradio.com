Title: Las trampas de Vargas Lleras al Sistema de control electoral en Colombia
Date: 2017-10-19 12:16
Category: Nacional, Política
Tags: elecciones 2018, Vargas Lleras
Slug: vargas-lleras-campana-cne
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/10/german_vargas_lleras_5.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Zona Cero] 

###### [18 Oct 2017] 

Las afirmaciones del candidato Vargas Lleras, en que reconoce haber hecho campaña electoral desde que participo como ministro y vicepresidente en los dos periodos presidenciales de Santos, abre el debate frente a las irregularidades que se están cometiendo al inciar campañas con recolección **de firmas y además porque ya se estarían invirtiendo dineros en campañas que no serían controlados por el CNE**.

De acuerdo con Camilo Vargas, coordinador del observatorio político de la Misión de Observación Electoral, lo preocupante de que las entrevistas del precandidato fueran **"publireportajes"** asegura que “mientras no se esté oficialmente en campaña electoral, la autoridad electoral no va a estar pidiendo rendición de cuentas del dinero usado en campañas”, por eso el llamado es para que el CNE comience a controlar esas situaciones.

Sobre las afirmaciones hechas por el senador Robledo y Claudia López, en donde señalaron que Vargas Lleras habría reclutado a sus candidatos para Cámara y Senado por Cambio Radical, **en 21 departamentos del país, ofreciéndoles impunidad en sus procesos penales por corrupción**, Camilo Vargas afirmó que estas deben ser comprobadas con denuncias e investigaciones por parte de autoridades estatales:

“Ad portas de un proceso electoral es muy importante que haya transparencia y es claro que **vivimos una crisis de responsabilidad política de los partidos, que no tienen mayor problema en avalar candidatos a pesar de sus investigaciones**” afirmó Vargas. (Le puede interesar: ["Cambio Radical y Centro Democrático se unen para hacer trizas los acuerdos: FARC")](https://archivo.contagioradio.com/cambio-radical-y-centro-democratico-se-unen-para-hacer-trizas-los-acuerdos-farc/)

Frente al papel de la Fiscalía  y los demás entes de control, Camilo Vargas señaló que le queda a la ciudadanía la función de presionar y exigir regulaciones sobre las campañas electorales y denunciar aquellos actos de fraude y corrupción que observen.

### **Los riesgos de las elecciones 2018** 

La corrupción, el paramilitarismo y el control de histórico de grupos o familias en regiones del país, son los más grandes obstáculos que tendrán que afrontar las elecciones del 2018. Para Camilo Vargas, frente al paramilitarismo ya se han hecho recomendaciones como el cumplimiento de los acuerdos de paz**, que permitirían que se diezmaran las presiones de estas estructuras sobre las poblaciones.**

“Hay nuevas instituciones desarrolladas por el Acuerdo de Paz, por ejemplo, **el Sistema Integral de Seguridad para el Ejercicio de la Política** debe trabajar muy de cerca con el sistema de garantías para la seguridad de líderes sociales y combatir el paramilitarismo”.

De igual forma, frente a las circunscripciones de paz y los fuertes debates que ha afrontado en donde se ha intentado sacarlas de los Acuerdo de Paz, Vargas afirmó que el hecho de no crearlas significaría lanzarse a 4 años de ejecución de políticas públicas sin la participación de la ciudadanía y de los territorios del país que han estado excluidas del Estado. (Le puede interesar[: "JEP continúa pese a intentos del Fiscal por modificar su esencia")](https://archivo.contagioradio.com/jep-continua-pese-a-intentos-por-modificar-su-esencia-de-parte-del-fiscal/)

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
