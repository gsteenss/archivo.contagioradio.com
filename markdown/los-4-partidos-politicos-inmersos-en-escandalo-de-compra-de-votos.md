Title: Los 4 partidos políticos inmersos en escándalo de compra de votos
Date: 2018-06-21 17:35
Category: Nacional
Tags: Compra de votos, elecciones 2018
Slug: los-4-partidos-politicos-inmersos-en-escandalo-de-compra-de-votos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/votacion-e1478194341725.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo Particular] 

###### [21 Jun 2018] 

El Fiscal General de la Nación Néstor Humberto Martínez, denunció las múltiples irregularidades que se encontraron en varias regiones del país, durante las elecciones al Congreso de la República. Asimismo, dejó al descubierto lo que serían redes de compra de votos que funcionaron en Medellín, Bogotá, entre otras ciudades y en donde están involucrados **políticos de los partidos Cambio Radical, Conservador, Liberal y Centro Democrático. **

### **Operación Casa Blanca** 

Es una empresa criminal, según el Fiscal, que tenía una red de compra de votos. Inicialmente funcionaba para favorecer a la congresista **Aida Merlano, en Barranquilla y otras ciudades del Atlántico**. Sin embargo, la Fiscalía encontró evidencias que demuestran que esta organización estaba trabajando desde el 2015 para las elecciones regionales de ese año.

La red era controlada por políticos regionales como Jorge Rangel y Margarita Ballén, diputados del partido Conservador en el Atlántico, Juan Carlos Zamora y Aissar Castro Bravo, ambos concejales del departamento y Aissar Castro Reyes, padre de Castro Bravo.  que contactaban a líderes barriales para que convocaran al electorado, posteriormente, daba dinero por cada uno de los votos que obtenía. Sus fuentes de financiación eran ilícitas.

Para las elecciones al Congreso de la República de 2018, esta organización compró votos para la senadora Aida Merlano, del partido Conservador y la candidatura de Lilibet Llinás, del partido Cambio Radical, quien no llegó a la Cámara. (Le puede interesar:["Los retos de Gustavo Petro en el Congreso de la República"](https://twitter.comwww.contagioradio.com/los-retos-de-petro-y-angela-robledo-en-el-congreso-de-la-republica-articulo-53950/))

La red se financiaba de manera pública y privada. Del lado público existieron contratos de prestación de servicios del Consejo de Barranquilla hacia quienes trabajaban con la “Casa Blanca”; del lado privado, se encontraron cientos de cheques de particulares. Una de las personas que trabajaba en esta organización **durante solo el mes de febrero cobró 21 cheques que superaron los mil millones de pesos.** Los dineros se depositaban en lugares de la casa.

### **Cómo funcionaba la red** 

La red organizaba a las personas a través de personal de diferentes rangos. En primer lugar, estaban los administradores que manejaban los dineros y la logística. Luego estaban los punteadores que revisaban las listas de los votantes reclutados, los certificados electorales y las contraseñas con códigos de barras.

Luego estaban los pagadores que entregaban las contraseñas en las casas de los votantes, los coordinadores que entregaban las contraseñas en las casas de campañas y estaban los didactas que enseñaban cómo votar y al mismo tiempo eran testigos electorales. **Cada líder llevaba a un centro de operaciones a un grupo de personas, con un talonario en el que marcaba a los votantes con fotocopia de la cédula y la huella**.

El día de las elecciones se arrendaron 187 terrazas en el departamento del Atlántico, a esos lugares iban los electores con el certificado y los líderes recibían el dinero para pagar el voto. Dentro de los votantes había adultos mayores y personas con bajos recursos económicos. En total, esta red logró vincular a 2323 líderes barriales y locales. (Le puede interesar:["Duque no desconozca el camino ya andado": FARC"](https://archivo.contagioradio.com/duque-no-desconozca-el-camino-ya-andado-farc/))

El próximo 30 de julio, se realizará la imputación de cargos a Llinás, a los diputados y concejales y al señor Aissar Castro Bravo. De igual forma se imputarán cargos tanto a los administradores, líderes como a quienes han podido ser capturados por participar en la red. Contra Merlano se generó una compulsa de copias ante la Corte Suprema de Justicia por violar **los topes máximos de dineros en campaña**. Los empresarios Julio Gerlein y Mauricio Gerlein también serán llamados por su presunta participación en estos hechos.

### **Compra de votos en Ciénaga Magdalena** 

En este hecho el Fiscal Néstor Martínez firmó que el pasado fue capturado Sergio Lora, quien portaba 7 millones de pesos en efectivo y un listado de votantes con propaganda política alusiva a Fabian Castillo, senador de Cambio Radical.

En el celular de Sergio se encontraron conversaciones de este con políticos para generar compra de votos.  El alcalde de Ciénaga, Edgardo de Jesús Pérez, una vez supo sobre la captura de Lora, se dirigió al lugar en donde lo tenían, allí solicitó ser contactado con el Fiscal de la seccional de Santa Marta para pedirle que Lora no fuese judicializado. Castillo también visitó a Lora.

Frente a este hecho Martínez aseguró que aún se investiga cuantos líderes barriales y políticos podrían estar inmersos en esta red de compra de votos. Por ahora serán imputados cargos a Sergio Lora y en contra del alcalde de Ciénaga. **A la Corte Suprema de Justicia también se le solicitó compulsar copias contra Fabian Castillo.**

### **Compra de votos en Medellín** 

Margarita Restrepo, representante por el Centro Democrático también está inmersa en la compra de votos. De acuerdo con el Fiscal, Restrepo habría ofrecido viajes a San Andrés, tablets y televisores a cambio de ingresos monetarios a su campaña. Alejandro Cuartas y Elizabeth Jaramillo, fueron los integrantes de esta campaña encargados de hacer los ofrecimientos a lo que se ha calculado más de 400 personas.

A la fecha el Fiscal ha logrado establecer que **55 sufragantes viajaron a San Andrés, 3 recibieron insólitos y otros no recibieron lo prometido, razón por la cual se acercaron a la Fiscalía para denunciar estafa.**

La imputación de cargos contra Cuartas y Jaramillo se realizó el día de hoy. Frente a Restrepo ya se solicitó la consulta de copias ante la Corte Suprema de Justicia.

### **Corrupción en el SENA de Caucacia** 

El subdirector del SENA, Braulio Suárez, de la seccional Caucacia, también estaría investigado por actos de corrupción. Suárez habría reunido a diferentes empleados y personas en lugares públicos para exigirles que votaran por el representante a la Cámara por el partido Liberal, Julian Bedoya.

De ganar Bedoya, Suárez aseguró que quienes tenían contrato con el SENA, lo mantuvieran y quienes no lo tuvieran fuesen contratistas. **Las personas que denunciaron este tipo de hechos expresaron que fueron víctimas de amenazas.** Suárez fue capturado y se le realizará la audiencia de captura e imputación de cargos.

### **Corrupción en Bogotá** 

La capital de la nación no se salvó de ser víctima de la corrupción. Julian Gutiérrez, contratista de Secretaría de Integración de Bogotá, supuestamente exigió a las profesoras de jardines infantiles de la localidad de Kénedy que votaran de forma direccionada por el candidato a la Cámara **Diego Caro y su fórmula al senado María Fernanda Cabal. De no hacerlo perderían sus puestos**.

Para corroborar el voto, pidió a las sufragistas el número de teléfono de 10 de sus familiares, el barrio de residencia y el número de cédula. Uno de los encuentros se realizó el 8 de marzo, en donde fueron citadas varias maestras en donde se les entregó un sobre con doscientos mil pesos. Por estos hechos el próximo 3 de julio se imputarán cargos contra Julian Gutiérrez y Jhon Rodríguez.

###### Reciba toda la información de Contagio Radio en su correo bit.ly/1nvAO4u o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por Contagio Radio 
