Title: La pandemia: Un reto para los planes de desarrollo territoriales y la participación ciudadana
Date: 2020-06-03 10:38
Author: CtgAdm
Category: Opinion
Tags: Foro Nacional por Colombia, pandemia, participación ciudadana, Plan de desarrollo territorial
Slug: pandemia-plan-desarrollo-participacion-ciudadana
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/06/participacion-plan-de-desarrollo.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

Por: Foro Opina

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El Covid-19 nos llegó, no sólo en un tiempo de considerable convulsión política nacional, sino también en un periodo de transición de los nuevos gobiernos locales y en medio de una importantísima tarea: la elaboración de los planes de desarrollo territorial. Pretender resarcir las afectaciones de la pandemia a partir de acciones cortoplacistas, propias de los estados de emergencia, y no incluir en los planes de desarrollo políticas integrales que respondan al nuevo contexto nacional es -al menos- irresponsable.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

La expansión del virus ha requerido que las administraciones locales dediquen sus mayores esfuerzos a atender la crisis, y los procesos de formulación de los planes de desarrollo han sufrido retrasos, al tiempo que se genera una serie de interrogantes sobre la pertinencia de replantear los contenidos y distribución presupuestal de los planes, así como de garantizar canales y promover acciones innovadoras para la efectiva participación de la ciudadanía y de los Consejos Territoriales de Planeación en estas circunstancias.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Traemos a la discusión tres casos con intención de ejemplificar el punto anterior. En  el caso de Bogotá, la llegada del virus ha representado una modificación no menor en las actividades de planeación. Cabe resaltar que la alcaldía alcanzó a hacer la entrega del proyecto del plan de desarrollo al Consejo Distrital de Planeación, instancia que realizó algunas reuniones con la ciudadanía con el objeto de recoger insumos para la elaboración del concepto sobre el documento. Sin embargo, la pandemia impidió continuar con este ejercicio. De otro lado, la administración distrital modificó la ruta metodológica de los “Encuentros Ciudadanos” para ser realizados a través de canales virtuales, ejercicios en los que se definiría la distribución del 40% de los recursos asignados a las localidades.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En Barranquilla también pueden hacerse varias apreciaciones. Si bien se alcanzaron a realizar la mayoría de las mesas de trabajo territoriales y sectoriales convocadas por la administración, no se han realizado más convocatorias virtuales ni se conocen acciones de incidencia impulsadas por el CTP. Las organizaciones sociales se encuentran a la espera de anuncios información sobre las modificaciones en los temas priorizados inicialmente en el Plan, concretamente, los referidos a la construcción de megaobras. De igual forma, el Concejo Distrital ya venía adoleciendo de verdaderos ejercicios de control político y de una bancada opositora. ¿agravarán las anunciadas sesiones virtuales este comportamiento de la corporación?

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por último, en Cali también se pueden destacar algunas afectaciones por cuenta de la llegada del Covid-19. Como ha expresado el alcalde Jorge Iván Ospina, el virus “ha roto los procesos de consulta y construcción comunitaria del plan”. La ciudad, que atravesaba un complejo proceso de planeación que incluía ambiciosos proyectos urbanísticos, ha sufrido de igual manera modificaciones profundas en la agenda que han complicado la discusión ciudadana del plan, que incluía un importante debate sobre el proceso de transición a distrito especial y la reorganización territorial que esto implica.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Es por esta situación descrita, que un grupo de organizaciones sociales, congresistas miembros de diversos partidos políticos y miembros de la Comisión de Ordenamiento Territorial presentaron sendas cartas dirigidas al Departamento Nacional de Planeación, instando a que se ampliaran los plazos y se dictaran unos nuevos lineamientos para la formulación de los planes de desarrollo, de tal forma que se incluyan modificaciones concernientes al tratamiento de la pandemia y sus consecuencias sanitarias, sociales y económicas. A este grupo, se sumaron recientemente el Contralor General de la República y el Procurador General de la Nación, los cuales solicitaron al DNP, un proyecto de ley que amplíe el plazo para la aprobación de los planes de desarrollo.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

A la fecha, el DNP no ha emitido respuesta ante esta cantidad de solicitudes. Con este panorama, el poco tiempo con el que cuentan los mandatarios, suscita serias preocupaciones sobre las garantías que tendrán las asambleas y concejos para hacer una labor efectiva de discusión, control político, y generación de propuestas. Igualmente, preocupa la poca capacidad real que tendrá la ciudadanía de participar activamente en la discusión y revisión de los borradores de los planes.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Finalmente, esta coyuntura abre nuevamente la discusión sobre las debilidades de la Ley 152 de 1994, en la que establecen los lineamientos para la formulación, ejecución y evaluación de los planes de desarrollo. Desde Foro Nacional por Colombia y otras organizaciones sociales, hemos subrayado la necesidad de reformar esta ley, en aras de darle más preponderancia a al alcance decisorio, no sólo consultivo, que deben tener los Consejos Territoriales de Planeación y en general, la apertura al diálogo democrático y la concertación entre la institucionalidad y la ciudadanía.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

**La Fundación Foro Nacional Por Colombia**

<!-- /wp:paragraph -->

<!-- wp:heading {"level":5} -->

##### ·         Es un Organismo Civil no Gubernamental sin ánimo de lucro, creado en 1982, cuyos objetivos son contribuir al fortalecimiento de la democracia en Colombia. Desarrolla actividades de investigación, intervención social, divulgación y deliberación pública, asesoría e incidencia en campos como el fortalecimiento de organizaciones, redes y movimientos sociales, la participación ciudadana y política, la descentralización y la gestión pública, los derechos humanos, el conflicto, la paz y las relaciones de género en la perspectiva de una democracia incluyente y efectiva. Foro es una entidad descentralizada con sede en Bogotá y con tres capítulos regionales en Bogotá (Foro Región Central), Barranquilla (Foro Costa Atlántica) y Cali (Foro Suroccidente).

<!-- /wp:heading -->

<!-- wp:paragraph -->

**Contáctenos: 316 697 8026\_ 031282 2550 /Comunicaciones@foro.org.co**

<!-- /wp:paragraph -->
