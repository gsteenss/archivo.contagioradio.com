Title: El rol Afro en el cine colombiano: Johnny Hendrix Hinestroza.
Date: 2015-05-21 16:31
Author: CtgAdm
Category: 24 Cuadros
Tags: Afrocolombianidad en el cine, Antorcha Films, Chocó película, Cine Colombiano, Jhonny Hendrix Hinestroza, Saudó
Slug: el-rol-afro-en-el-cine-colombiano-johnny-hendrix-hinestroza
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/05/54hendrix.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<iframe src="http://www.ivoox.com/player_ek_4530113_2_1.html?data=lZqgkpaVd46ZmKiak5iJd6KnlZKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRic%2Fo08rjy9jYpYzX0NOYx9GPqMrmxsjh0dePjsnjz9Pmja3JssXmyt2Yqs7SqdTo09Tnw5KJe6ShpNTb1sbLrdCfs8bRy9SPcYarpJKh&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Entrevista con Jhonny Hendrix Hinestroza para Contagio Radio.] 

Resulta paradójico que uno de los directores con mayor reconocimiento del cine colombiano, en particular por ser uno de los pocos pertenecientes a la comunidad Afrocolombiana, haya vivido únicamente dos años de su vida en la tierra que lo vio nacer, Chocó, Quibdo. El destino de **Jhonny Hendrix Hinestroza **era muy diferente al de muchos de los niños nacidos allí en la mitad de la década del setenta, y aún en nuestros días, en el bello, pero olvidado departamento por el estado, de nuestro país.

Y es que precisamente, su nombre empezó a sonar con más fuerza cuando en el año 2012 la producción homónima a su ciudad de nacimiento, "Chocó", su ópera prima, fué estrenada con todas las galas en la sección Panorama del Festival de Berlín de ese año.

Aún más paradojico resulta saber que el hombre detrás de la producción de cintas como **"Perro come perro"**, **"Anina"** y **"Noche buena"** entre otras, sea alérgico al pescado, no baile con el sabor del pacífico y no sea un galán como muchos hombres con su ascendencia raizal. Un pequeño sacrificio de virtudes, compensadas con otras adquiridas en el camino emprendido en el momento en que sus padres decidieron, en busca de una mejor educación para él y sus hermanos, trasladar la familia a la ciudad de Pereira y luego a Cali.

Es un cúmulo de experiencias adquiridas con cada uno de los pasos recorridos en el camino de su vida, combinadas con los conocimientos adquiridos durante su formación en Comunicación Social, pasando por algunos años de experiencia de trabajo en la producción radial y de comerciales para televisión, hasta su formación en escritura de guiones en Madrid, España. El objetivo principal de combinar unas con otras en apariencia resulta bastante simple: **encontrar el modo de contar historias, el cómo narrar eso que hace parte de la tradición de muchas comunidades colombianas.**

A pesar de que hoy, Johnny Hendrix Hinestroza sea un referente a la hora de hablar del cine hecho en nuestro país y necesariamente del retrato audiovisual de las comunidades afrodescendientes del pacífico colombiano, aún no se considera él mismo un director; siente que aún le faltan hacer por lo menos 5 o 6 películas para adquirir tal denominación. Su visión en relación con la imágen que se tiene de las comunidades Afro en la pantalla en general en nuestro país es bastante crítica: **"Antes de existir "Chocó", no existia un imaginario del Pacífico colombiano, no estabamos en el panorama, ni en la televisión, ni en el cine "**.

En retrospectiva, la inclusión de personajes Afrodescendientes en producciones televisivas colombianas ha estado presente desde la década de los ochentas y noventas, en general con roles secundarios, que establecen un perfil casi caricaturezco de estas comunidades, que difícilmente obedecen a las realidades de muchas de estas, **"No me siento identificado en la mayoria de trabajos televisivos que han planteado casi siempre un afro alegre, un imaginario muy bailarin, rumbero, muy en lo mismo. Nos convertimos en un cliché y de ahí no salimos".**

<iframe src="https://www.youtube.com/embed/dw7fq_sp4jY" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

**"Chocó"**, además de ser el retrato de la ciudad y la gente, es una historia que se utiliza para contar una problemática real que aqueja a las mujeres sin ningún tipo de distinción en Colombia y el mundo: El maltrato. Y es precisamente lo que el director quiere mostrar intrínsecamente:**"Tenemos unas problematicas que son muy del hombre afro pero que independiente también lo tienen otras regiones"**, buscando, además, romper **"un poco el cliché que hay de las comunidades afro, y queriendo reflejarnos y vernos de una manera distinta. Somos una baraja de naipes, algunos muy felices, baile, y tambien hay tristeza, llanto y mucho más"**.

Una de las satisfacciones del director, además de las particulares, es que su trabajo ha servido para motivar a nuevos realizadores a contar este tipo de historias aportando a nuevas construcciones de los imaginarios en torno a su comunidad, **"creo que se están dando movimientos que permiten que lleguemos por lo menos a figurar (...), existe una intención, una necesidad de contar historias distintas, visibilizarnos distinto mas fuerte; quienes somos, quienes fuimos, de dónde vinimos y para dónde vamos. Hay mucha tela por cortar".**

En la actualidad, Johnny Hendrix trabaja en su segundo largometraje títulado **"Saudó",** una cinta de suspenso que narra el drama de un padre que busca huír de sus miedos y su pasado, con personajes e historias de la mitología existente, y poco conocida por muchos en el Pacífico colombiano, un proyecto que se espera llegue a las salas el año que viene.
