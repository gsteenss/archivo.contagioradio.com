Title: Ex prisioneras políticas de FARC están siendo victimas de amenazas
Date: 2020-03-11 16:39
Author: CtgAdm
Category: Actualidad, DDHH
Tags: asesinato, FARC, mujeres
Slug: ex-prisioneras-politicas-estan-siendo-victimas-de-amenazas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/03/WhatsApp-Image-2020-03-11-at-3.57.34-PM.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:html -->

<figure class="wp-block-audio">
<audio controls src="https://co.ivoox.com/es/juan-david-bonilla-sobre-amenazas-a-mujeres_md_48849147_wp_1.mp3">
</audio>
  

<figcaption>
Entrevista &gt; Juan David Bonilla | Integrante de la Corporación Solidaridad Jurídica

</figcaption>
</figure>
<!-- /wp:html -->

<!-- wp:paragraph -->

Tras el asesinato de Astrid Conde, ex prisionera política que hacía su tránsito a la reincorporación social, la Corporación Solidaridad Jurídica denunció que **otras ex prisioneras políticas están siendo víctimas de amenazas y hostigamientos en contra de sus vidas.**

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

De acuerdo con Juan David Bonilla, integrante de esta organización, el grupo de mujeres que promueve la defensa de los derechos humanos y el enfoque de género pactados en el Acuerdo de Paz, que trabaja con la Corporación ha recibido amenazas algunas de actores armados identificados **como las Águilas Negras.**

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

"Una de ellas fue amenazada por las Águilas Negras. Le metieron panfletos en la puerta de su casa, en el trabajo, la siguieron, hasta que le tocó salir corriendo de Bogotá", afirmó el abogado. (Le puede interesar: ["El miedo que sentíamos en la guerra es el miedo que ahora sentimos al salir de la casa: excombatientes"](https://archivo.contagioradio.com/el-miedo-que-sentiamos-en-la-guerra-es-el-miedo-que-ahora-sentimos-al-salir-de-casa-excombatientes/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

A la fecha, dos mujeres son las que han sido mayoritariamente víctimas de los hostigamientos. Sin embargo, el resto de mujeres que conforman este grupo de reincorporación **han manifestado que también han tenido seguimientos**, hecho que acrecienta el temor entre ellas.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Bonilla asegura que estas intimidaciones para la Corporación Solidaridad Jurídica, tienen como principal objetivo amedrentar a personas que están en proceso de reincorporación, hecho que ha sido constatado en algunos panfletos, "otra de las integrantes de este grupo, también ha recibido **reiteradas amenazas por haber pertenecido a la entonces guerrilla de las FARC-EP".**

<!-- /wp:paragraph -->

<!-- wp:heading -->

El asesinato de Astrid Conde no puede quedar en la impunidad
------------------------------------------------------------

<!-- /wp:heading -->

<!-- wp:paragraph -->

Frente al asesinato de la firmante del Acuerdo de Paz, Astrid Conde, Bonilla asevera, que días previos al homicidio ella había alertado sobre seguimientos en su contra, y que durante su tiempo en prisión había sido víctima de intimidaciones por la Fiscalía.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

"Astrid había manifestado un hostigamiento por parte de la Fiscalía, que la mantenía constantemente citando a entrevistas y audiencias, frente a un caso anterior de uno de sus ex compañeros sentimentales. Lo que **ella nos manifestaba es que posiblemente eran seguimientos por parte del Estado,** por haber sido compañera de Gentil Duarte".

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Bonilla recalca que si bien ya se tiene la captura de la persona que habría cometido el asesinato, esperan que este **hecho no quede en la impunidad** frente a los autores intelectuales que dieron la orden del asesinato. (Le puede interesar: ["Astrid Conte, tercera firmante de la paz asesinada en Bogotá"](https://archivo.contagioradio.com/astrid-conde-gutierrez-tercera-firmante-de-la-paz-asesinada-en-bogota/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->
