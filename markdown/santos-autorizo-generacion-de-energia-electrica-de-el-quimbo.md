Title: Pese a decisión del Tribunal del Huila, Santos abrió El Quimbo
Date: 2015-10-13 12:54
Category: Ambiente, Nacional
Tags: 27 de noviembre, Agencia Nacional de Licencias Ambientales, ANLA, ASOQUIMBO, CAM, Corporación Autónoma del Magadalena, El Quimbo, EMGESA, fracking, Huila, Miller Dussán, No al Quimbo, Tribunal Administrativo del Huila
Slug: santos-autorizo-generacion-de-energia-electrica-de-el-quimbo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/10/quimbo-1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: [www.quimbo.com.co]

<iframe src="http://www.ivoox.com/player_ek_8960973_2_1.html?data=mZ6jkp6bd46ZmKiakpWJd6KkkZKSmaiRdI6ZmKiakpKJe6ShkZKSmaiRl8Li1dTgjcbZuNDmyt%2BSpZiJhpSfyMrbx9fFp8qZpJiSpJjSb8XZjMrbx9fLaaSnhqaxw5DJsIa3lIqum8jYtsrXwpDRx5CRaZi3jqjc0NnFq8rjjLfOxs7Tb46ZmKialg%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Miller Dussán, ASOQUIMBO] 

Mediante el decreto 1979 del 6 de octubre 2015, el presidente Juan Manuel Santos, autorizó el inicio de generación de energía eléctrica a través del proyecto hidroeléctrico de El Quimbo, pese a que el **Tribunal Administrativo del Huila lo había suspendido** hasta que la multinacional EMGESA retirara del vaso del embalse el material vegetal.

De acuerdo con Miller Dussán, investigador de El Quimbo, con la firma de todos los ministros, **Santos “pasó por encima de la suspensión del Tribunal Administrativo del Huila”,** para favorecer los intereses de los accionistas de EMGESA.

El gobierno habría utilizado un decreto con el que declaró emergencia económica, social y ambiental a raíz de la crisis fronteriza con Venezuela, por lo cual se veía como necesario que la represa entrara en funcionamiento para poder transportar el crudo, teniendo en cuenta que el embalse de Betania cuenta con bajos niveles de agua por la actual crisis climática, lo que a su vez incrementa el costo de la energía, según asegura el investigador de ASOQUIMBO.

Para Dussán, esa explicación del presidente Santos, no es creíble y en realidad la decisión del **gobierno se debería a la presión de las transnacionales, como EMGESA** que han asegurado que el mandatario les ha cumplido, alegando que en Colombia no hay confianza inversionista, no tienen garantizada la seguridad jurídica y se están afectando los compromisos financieros en el mercado de capitales, por lo que posiblemente el gobierno sería sancionado por los mercados internacionales.

El requisito para que la represa entrara en funcionamiento, era el retiro total de la biomasa vegetal que aun se encuentra en el vaso del embalse. Según la Agencia Nacional de Licencias Ambientales, EMGESA **ha cumplido con esa medida en un 99%, sin embargo, la Corporación Autónoma del Magdalena,  CAM, reportó que ese requisito no se ha cumplido y no se removió “la biomasa en 1.000 hectáreas del vaso del embalse”**; lo que provocará daños ambientales irreparables que afectarán la salud humana.

**“Se van a vivir pandemias por la irresponsabilidad de Santos y EMGESA”**, expresa el investigador de ASOQUIMBO, quien agrega que con la decisión del presidente de Colombia se destruirá por completo la seguridad alimentaria.

Tras esa situación, los afectados por El Quimbo, se movilizarán **el próximo 27 de noviembre en contra de las actividades de empresas como EMGESA**, también se manifestarán en contra del extractivismos y el fracking.
