Title: Sometimiento de AGC debe garantizar la verdad a las víctimas
Date: 2017-09-06 12:57
Category: DDHH, Nacional
Tags: Autodefensas Gaitanistas de Colombia, Conpaz
Slug: sometimiento-de-agc-debe-garantizar-la-verdad-a-las-victimas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/03/CONPAZ.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:] 

###### [06 Sept 2017] 

Las comunidades que hacen parte de la Red de Comunidades construyendo paz en los territorios (CONPAZ), manifestaron, a través, de un comunicado que el interés que han señalado las autodenominadas Autodefensas Gaitanistas de Colombia, de acogerse a un proceso de paz es muy positivo y **debe estar basado en la verdad y el respeto a los derechos humanos.**

De acuerdo con María Eugenia Mosquera, integrante de CONPAZ, si esta estructura armada aporta con decir la verdad, se tendrá información sobre quiénes son los financiadores de este grupo y las motivaciones para sus actuaciones, así como las complicidades de otros organismos, **“queremos saber cuáles son los intereses y quienes están detrás de lo que ha pasado en nuestros territorios”. **(Le puede interesar: ["Las Autodefensas Gaitanistas de Colombia están dispuestas a acogerse a la justicia"](https://archivo.contagioradio.com/las-autodefensas-gaitanistas-de-colombia-estan-dispuestas-a-acogerse-a-la-justicia/))

Sin embargo, aseguró que no esperan que la justicia ordinaria sea la herramienta que opere para encontrar esta verdad, teniendo como antecedente la desmovilización de las Autodefensas Unidas de Colombia “la verdad se construye también desde las comunidades y nosotros como víctimas queremos ir hablar con ellos para sacar un proceso de justicia real, con las comunidades, **no en las cárceles porque allí no se van a re educar, ni a decirnos toda la verdad de lo que ha ocurrido**”.

De igual forma, aseguraron que al ser víctimas del conflicto armado desde 1996 y seguir padeciendo las dinámicas del control social territorial por parte de las autodenominadas Auto Defensas Gaitanistas de Colombia, **valoran positivamente el mensaje de esta estructura paramilitar**. (Le puede interesar: ["Paramilitares asesinan a dos integrantes de la comunidad Wounnan"](https://archivo.contagioradio.com/paramilitares-asesinan-a-dos-intengrantes-de-la-comunidad-wounnan/))

Además, aseguran que este paso puede ser un “buen signo” para lograr desactivar nuevas dinámicas de violencia en el territorio que ponen en riesgo la construcción de paz en la sociedad y que existen las condiciones necesarias para que los grupos armados que colocan en riesgo sus vidas y permanencia en el territorio, **de un paso al costado y se junten y aporten a la paz de Colombia**.

*Comunicado de prensa*

Varios lugares de Colombia, 5 de septiembre de 2017

Señor   
**JUAN MANUEL SANTOS **  
Presidente

Señor   
**ÓSCAR NARANJO**  
Vicepresidente

Señor   
**ENRIQUE GIL BOTERO**  
Ministro de Justicia

**Ref:** *El acogimiento de AGC se debe basar en respeto a derechos humanos*

Reciban un respetuoso saludo.

En nombre de nuestra red Comunidades Construyendo Paz en los Territorios - CONPAZ -, que hemos sido afectados por la violencia política desde 1996 y que hoy seguimos padeciendo con dinámicas de control social territorial por parte de las autodenominadas Autodefensas Gaitanistas de Colombia, AGC, o llamados en los medios de información Clan del Golfo, valoramos muy positivamente, la disposición del máximo mando de esta esta estructura armada de iniciar un proceso de acogimiento a la justicia.

Creemos que tal disposición es un buen signo para lograr desactivar nuevas dinámicas de violencia que ponen en riesgo la consecución de una sociedad en paz, creemos que existen instrumentos legales que harán posible que tal disposición garantice los derechos de los eventualmente procesados y de los miles de afectados por estas operaciones armadas ilegales

Consideramos que se están dando las condiciones para que factores armados que colocan en riesgos nuestras vidas y permanencia en el territorio se desactiven, sin que sigamos con el temor de ser asesinados, desaparecidos, desarraigados y sin condiciones básicas de libertad. Como victimas esperamos se aseguren nuestros derechos en desarrollo de esta disposición de sometimiento a la justicia y se les garanticen a ellos sus derechos.

Deseamos que esta disposición se base en la verdad y sobre ella sea posible la paz.

**Comunidades Construyendo Paz en los territorios – CONPAZ –**

<iframe id="audio_20728637" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_20728637_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
