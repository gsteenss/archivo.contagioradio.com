Title: "Semana Santa", crueldad y Coronavirus
Date: 2020-04-11 14:23
Author: AdminContagio
Category: Columnistas invitados, Opinion
Tags: cordoba, hicoteas, huevos de tortugas, Semana Santa, tortugas
Slug: semana-santa-crueldad-y-coronavirus
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/04/DSC1860-e1500346191844.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:heading {"align":"right","level":6,"textColor":"cyan-bluish-gray"} -->

###### Foto de: La prensa web {#foto-de-la-prensa-web .has-cyan-bluish-gray-color .has-text-color .has-text-align-right}

<!-- /wp:heading -->

<!-- wp:heading {"level":5} -->

##### Por: **Renata Cabrales**

<!-- /wp:heading -->

<!-- wp:paragraph -->

No solo Jesús  
revive un viacrucis cada Semana Santa (les encanta verlo sufrir todos los  
años). En el departamento de Córdoba, mi tierra natal, también lo padecen por  
esos días, las inofensivas hicoteas (una especie de tortuga de la familia de  
los emídidos, que vive en las zonas cenagosas del norte de Colombia); su falta:  
ser apetecidas por el cruel paladar de los lugareños, quienes religiosamente,  
cumplen con el deber de no comer carne en Cuaresma. A cambio, y de manera  
candorosa, devoran la carne que no es carne, según ellos, de esta perseguida  
criatura.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":5,"customTextColor":"#03435e"} -->

#####  En estos momentos, se supone que ya es  
considerado un crimen ambiental criar a esta clase de tortuga con un fin  
comercial, pero, como si fuera una especie de droga, mientras más demanda tenga  
como producto de mercado y mientras más controles policiales se hagan, se  
dispara su valor comercial. Así pues en un restaurante de prestigio, un plato  
de esta apetecida carne no carne, oscila entre 15 mil y 25 mil pesos. 

<!-- /wp:heading -->

<!-- wp:paragraph -->

Reconozco que  
tampoco soy una santa, pues mi vergonzante especismo me obligó a convertirla  
también, en mi oscuro pasado,  en uno de  
mis platos predilectos.  Peor aún, esto  
sucedió siendo consciente de la forma cruel en que son asesinadas antes de su  
preparación, y habiendo sido testigo de   
tal crueldad, no pocas veces en mi propia casa, y siendo apenas una  
niña. Pero yo no era una niña insensible, ¡no! ¡Yo lloraba cuando mi abuela  
despescuezaba a las gallinas! Y por eso mismo, ella, antes de cometer semejante  
acto de barbaridad, me decía con cierta   
consideración de psiquiatra en manicomio: "Vete, Alicia, ¡no mires  
esto, no lo mires!".

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Sin embargo, un largo Mea culpa le debo a las silenciosas hicoteas: ¡perdón, perdón, perdón!, yo merezco ser crucificada. Solo espero que el Coronavirus haya ido a su rescate este año (por lo menos salvó a Jesús de la morbosidad "cristiana"), y hayan podido respirar con libertad, sin temor a la sed de riqueza de los cazadores despiadados, que se consideran mejores cristianos por el hecho de vender y consumir solo carne no carne, durante la Cuaresma.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

P.D: esta semana(de 2020) que si es santa, porque el Coronavirus ha logrado aplanar la curva de la tortura y el morbo religioso, me inclino por comer el apetecido conejo suizo que simboliza la Pascua helvética: ¡un  conejo, pero de chocolate!

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Ver mas:[Nuestros columnistas](https://archivo.contagioradio.com/opinion/columnistas/)

<!-- /wp:paragraph -->
