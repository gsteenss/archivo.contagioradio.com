Title: "Las mujeres son protagonistas en resistencia Kurda de Kobani" Alan Kanjo
Date: 2015-01-29 21:05
Author: CtgAdm
Category: El mundo, Otra Mirada
Tags: Alan Kanjo, Director de cine Kurdo, Estado Islámico, Kobani, Rojava
Slug: las-mujeres-son-protagonistas-en-resistencia-kurda-de-kobani-alan-kanjo
Status: published

###### **Foto: Vozpopuli.com** 

**Entrevista con Alan Kanjo:**  
<iframe src="http://www.ivoox.com/player_ek_4015351_2_1.html?data=lZWel5iZdY6ZmKiakp2Jd6KpmZKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRic%2Fo08rjy9jYpYy1zcbbjaa3l6K4jKnRy9fJp9Xj05DRx5DHrcbijLDi1MnTcYarpJKw0dPYpcjd0JC%2Fw8nNs4yhhpywj5k%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

Después de que el pueblo Kurdo de **Kobani desalojara al Estado Islámico y frenara la invasión  sobre la región de Rojava** en la frontera entre Siria y Turquía, se abre una brecha entre los combatientes de los grupos islámicos extremistas y la población de las regiones afectadas que se opone a ellos.

Todo **comenzó en la revolución Siria de 2011**, cuando estallo la revolución y consiguiente guerra entre el régimen Sirio, el Estado libre Sirio y los grupos armados islamistas fuertemente vinculados a Al-Qaeda. Es entonces cuando las tres regiones Kurdas quedaron aisladas y rodeadas por estos últimos.

Fue entonces cuando decidieron crear una autonomía gobernada por el partido PYD (Partido de la Unión Democrática), hermano del partido de los trabajadores del Kurdistán, actualmente ilegal en Turquía; con el objetivo de poder gestionar las tres autonomías.

Los enfrentamientos  entre fuerzas Kurdas, agrupadas en autodefensas, y los combatientes del Estado Islámico, comenzaron poco después de la revolución siria prolongándose hasta hoy en día.

Lo que no se esperaban los combatientes es encontrarse **con milicias en las que el 40% de sus combatientes son mujeres**. Tampoco que entre la organización políticas se encuentre entre las reivindicaciones la autogestión, el laicismo o el fin del fanatismo religioso, situación que ha llevado a muchos analistas ha compararla con la España del 36.

Entrevistamos a **Alan Kanjo realizador de documentales y director hispanokurdo**, quién además de estar trabajando en un documental sobre lo sucedido, tiene a toda su familia en Kobani, y recientemente ha visitado la zona.

Él nos explica todo el entramado social que existe y el por que de la fuerte presencia de las mujeres en las milicias y en la propias organizaciones Kurdas.También el por que de como sin apenas apoyo internacional han conseguido liberar la ciudad y parte d la región.Por último analiza la influencia.
