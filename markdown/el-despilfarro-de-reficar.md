Title: Infografía: El despilfarro financiero de Reficar
Date: 2016-05-12 08:06
Category: infografia
Tags: reficar
Slug: el-despilfarro-de-reficar
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/05/INFOGRAFIA-REFICAR-CONTAGIORADIO.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

\[caption id="attachment\_23647" align="aligncenter" width="1522"\][![INFOGRAFIA REFICAR CONTAGIO RADIO](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/05/INFOGRAFIA-REFICAR-CONTAGIO-RADIO.png){.wp-image-23647 .size-full width="1522" height="1109"}](https://archivo.contagioradio.com/reficar-la-mayor-feria-del-despilfarro-en-colombia/infografia-reficar-contagio-radio/) Los sobrecostos en la ampliación de la Refinería de Cartagena (Reficar), el proyecto que más dinero le ha costado a Colombia en toda su historia, se calculan en por lo menos 4 mil millones de dólares de acuerdo con la Contraloría General de la República\[/caption\]
