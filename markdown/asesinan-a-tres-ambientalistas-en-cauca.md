Title: Asesinan a tres ambientalistas en Cauca
Date: 2016-08-29 15:43
Category: DDHH, Nacional
Tags: Cauca, CIMA, Derechos Humanos, Paramilitarismo
Slug: asesinan-a-tres-ambientalistas-en-cauca
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/08/w_111112-marcha-panamericana-asamblea-cima_thumb.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: memoriaydignidad] 

###### [29 Ago 2016]

Este lunes se conoció que** Joel Meneses, Ariel Sotelo y Mereo Neneses, reconocidos ambientalistas del departamento del Cauca fueron asesinados sobre las 8 de la mañana,** a manos de hombres armados que vestían uniformes militares con armas largas, en el sitio Guayabillas del corregimiento de Llacuanas, municipio de Almaguer.

Según se conoció mediante un comunicado del  Comité de Integración del Macizo Colombiano, CIMA, los **hombres armados interceptaron los vehículos donde iban  los ambientalistas que se dirigían al mercado de éste corregimiento**. Luego les ordenaron apagar sus celulares y detuvieron el vehículo donde se transportaban Joel, Nereo y Ariel quienes fueron llevados hasta el sitio conocido como Monte Oscuro, en los límites de los municipios de Bolívar y Almaguer, donde fueron encontrados asesinados con tiros de gracia.

**Los líderes participaron activamente en diferentes movilizaciones campesinas en defensa del ambiente,** entre ellas la reciente Minga Nacional Agraria, Campesina, Étnica y Popular que se desarrolló desde el 30 de mayo hasta el 12 de junio de este año.

Joel Meneses era un líder histórico del CIMA y dirigente comunitario del Proceso Campesino e Indígena de Almaguer-PROCAMINA, quien antes ya había recibido varias amenazas; Nero Meneses era integrante de PROCAMINA y líder de la vereda Garbanzal del mismo corregimiento y Ariel Sotelo, hacía parte de la comunidad de La Herradura.

Desde el Comité, se asegura que constantemente habían denunciado a las autoridades el  asesinato de decenas de líderes y lideresas en la región, las amenazas continuas de orden personal y colectivas, la persecución, estigmatización, señalamientos, judicializaciones individuales y masivas, **en medio de la su lucha contra minería legal e ilegal, al narcotráfico y la construcción de la paz.**

Frente a esta situación, el CIMA hace un llamado a los actores armados legales e ilegales para que cesen la violencia contra los líderes sociales, y además **exigen a las autoridades que se investiguen los hechos** que de forma constante ha denunciado el Comité de Integración del Macizo Colombiano CIMA durante varios años.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
