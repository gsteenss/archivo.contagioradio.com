Title: Asesinan a excombatiente de las FARC en el ETCR de Santa Lucía, Ituango
Date: 2019-12-14 12:40
Author: CtgAdm
Category: DDHH, Nacional
Tags: Asesinatos, etcr, excombatientes, FARC, Ituango, Santa Lucía
Slug: asesinan-a-excombatiente-de-las-farc-en-el-etcr-de-santa-lucia-ituango
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/Asesinan-excombatiente-FARC.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Twitter] 

El viernes 13 de diciembre, alrededor de las 03:00 pm, **el excombatiente de las FARC, Manuel Antonio González Buelvas, fue asesinado en el municipio de Ituango, Antioquia.** El asesinato se produjo en la vía que lleva del casco urbano de Ituango hacia la vereda de Santa Lucía, donde se encuentra el Espacio Territorial de Reincorporación (ETCR), Román Ruiz.

El cuerpo permaneció inerme sobre la vía hasta que otros excombatientes del lugar hicieron el levantamiento, ya que no se registró presencia de la fuerza pública. González Vuelvas, quien era conocido con el seudónimo de Yair Cartagena, **era padre de dos hijos nacidos después de la implementación del Acuerdo de Paz en la Habana. **[(Lea también: Con Wilson Parra, son 169 excombatientes de FARC asesinados desde la firma del Acuerdo de Paz)](https://archivo.contagioradio.com/con-wilson-parra-son-169-excombatientes-de-farc-asesinados-desde-la-firma-del-acuerdo-de-paz/)

González Buelvas hacía parte de un proyecto ganadero colectivo, que está conformado por más de 100 excombatientes de las FARC que habitan en la región rural de Santa Lucía, en Ituango. Su muerte se suma a la de otros 19 excombatientes que han sido asesinados a lo largo del año, de los cuales 11 han sido asesinados en el municipio de Ituango.

**Los excombatientes de las FARC están siendo aniquilados**

En lo que va del año **han sido registrados 218 homicidios en contra de excombatientes de las FARC o familiares de los mismos.** 176 eran exintegrantes y 42 familiares. Por otro lado, la tentativa de homicidio ha sido un factor determinante, reportándose 20 casos y 13 desapariciones forzadas, todo ello en distintos puntos del país.

La Fuerza Alternativa Revolucionaria del Común (FARC) se pronunció ante el hecho imponiendo una denuncia pública ante la comunidad internacional y nacional por la gravedad de los hechos. El partido político afirmó que la comunidad de excombatientes en Ituango “está en zozobra y consideran que estos hechos son un claro mensaje de intimidación para que se desplacen del ETCR Román Ruiz”.

En la zona hacen presencia las Autodefensas Gaitanistas (AGC) y disidencias del frente 18 de las FARC, mismo frente del cual González hacía parte. Para los excombatientes que están asentados en Santa Lucía, Ituango esto es sumamente preocupante ya que, **además de los múltiples asesinatos contra excombatientes, no hay seguridad en la única vía de espacio que comunica el casco urbano con la zona rural del municipio antioqueño,** donde transitan a diario. [(Le puede interesar: 500 hectáreas para la construcción del ETCR de Santa Lucía, Ituango)](https://archivo.contagioradio.com/construccion-del-etcr-de-santa-lucia-ituango/)

 

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
