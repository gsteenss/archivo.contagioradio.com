Title: "El asesinato de la periodista Flor Alba no puede quedar en la impunidad"
Date: 2015-09-11 15:01
Category: DDHH, Entrevistas
Tags: Asesinato contra flor alba, atentado contra la libertad de expresión, FLIP, Flor Alba Nuñez, impunidad, Libertad de expresión, Libertad de Prensa
Slug: el-asesinato-de-flor-alba-no-puede-quedar-en-la-impunidad
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/Asesinada-periodista-Flor-Alba-NuNez-en-Pitalito-2.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: facebook 

<iframe src="http://www.ivoox.com/player_ek_8322465_2_1.html?data=mZiflJmaeY6ZmKiak56Jd6KplpKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRhdTZ1M7bw9nTb8Tjz9nfw5CqsNDmjKbZxMaPstCf0drSxsqPtdbZxcbfjcrSb83VjM7a0trSrY6ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Sirly, Periodísta] 

###### [11 sep 2015]

El asesinato de Flor Alba Nuñez Vargas, periodista de Pitalito, Huila **conmociona al país por la brutalidad del ataque a plena luz del día**. Las hipótesis sobre este hechos varían y van desde el cubrimiento sobre el proceso electoral, hasta la publicación de una nota sobre las personas que se vieron implicadas en el asesinato de un perro de raza Bull Terrier el pasado mes agosto.

Sirly, su amiga cercana y también periodista supo, que después de haber publicado una nota periodística sobre el asesinato de la perra Dominic, después de publicar las fotos de una captura a unos ladrones en flagrancia y de iniciar un seguimiento a una campaña política **empezó a recibir amenazas en sus redes sociales**, sin embargo, ninguna de estas se hizo pública, por no considerarlas de gravedad.

Nuñez trabajaba en la emisora La preferida Stereo (en Pitalito),  en los espacios informativos locales de Canal 6, TV5 y del Canal Nación TV. Anteriormente había trabajado para la emisora Hjdoblek. Había estudiado Licenciatura en Lengua Castellana y se preparaba para especializarse en Comunicación.

### **¿Hay garantías para el ejercicio periodístico?**

Este hecho ha generado que algunos de **los periodistas de Pitalito también se sientan amenazados en su labor, en su libertad y en su integridad**, además que las autoridades no han indicado de que forma se velará por su seguridad.

En un concejo de seguridad extraordinario en donde participaron comandantes de la policía, el Alcalde y el Secretario de Gobierno se dijo que la policía brindará una recompensa para quien brinde información sobre el caso, pero esta medida no es suficiente para resolver el atroz crimen, ni mucho menos para garantizar el ejercicio periodístico.

Por su parte el Coronel del quinto distrito de Pitalito y una unidad investigativa, arribaron hasta Pitalito y ya tienen historial de las denuncias completas y se comprometieron a adelantar sus propias investigaciones "**porque el asesinato de Flor no puede quedarse en la impunidad**", indica la amiga cercana a Flor.
