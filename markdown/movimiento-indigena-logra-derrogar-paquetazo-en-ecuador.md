Title: Movimiento indígena logra derogar el decreto 883 en Ecuador
Date: 2019-10-14 01:36
Author: CtgAdm
Category: El mundo, Política
Tags: CONAIE, Decreto 883, ecuador, Lenín Moreno
Slug: movimiento-indigena-logra-derrogar-paquetazo-en-ecuador
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/10694.jpeg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/Ecuador-1.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Mov Indígena Cotopaxi] 

Tras varias horas de diálogo entre el gobierno de Lenin Moreno y movimientos indígenas de Ecuador, el mandatario acordó este domingo derogar el decreto 883 parte de las medidas del denominado **'Paquetazo',** que busca eliminar el subsidio a los combustibles y que derivó en una ola de protestas en el país. Al abolir este decreto fue evidente la importancia del accionar de los pueblos originarios que representan un 8% de la población ecuatoriana.

La decisión fue tomada después de 11 días de manifestaciones en las que en medio de enfrentamientos contra la Fuerza Pública, en las que se cometieron numerosas violaciones a los derechos humanos, registrándose un total de 7 muertes de manera oficial, 937 personas heridas y 1121 detenidas según la Defensoría del Pueblo. [(Lea también: Lenin Moreno anuncia que recuperará el orden en Ecuador, desplegando las FFMM)](https://archivo.contagioradio.com/lenin-moreno-anuncia-que-recuperara-el-orden-en-ecuador-desplegando-las-ffmm/)

El anuncio lo realizó Arnaud Peral, coordinador de las Naciones Unidas en Ecuador quien hizo parte de la mesa de negociación. "Como resultado del diálogo se establece un nuevo decreto que **deja sin efecto el decreto 883**, para lo cual se instala una comisión que elaborará este nuevo decreto", expresó el funcionario.

En medio de las negociaciones Jaime Vargas, líder de la **Confederación de Nacionalidades Indígenas del Ecuador (CONAIE)** pidió al Gobierno ser transparente con "los acuerdos a los que llegó con el Fondo Monetario Internacional" y, en aras de la paz,  pedir la renuncia de los ministros de Defensa, Oswaldo Jarrín, y de Gobierno, María Paula Romo, a quienes responsabilizó de la violencia desmedida contra el pueblo.

### Continúan las detenciones 

Pese al acuerdo alcanzado, información proveniente de Pichincha, señala que la prefecta Paola Pabón fue detenida en horas de la madrugada por agentes de la Fiscalía General del Estado quienes allanaron su domicilio para fines investigativos, tal detención se realizó con la presencia de Policía y Ejército.

### ¿Será derogado en su totalidad el decreto 883 en Ecuador? 

Según el anuncio, las nuevas medidas serán elaboradas mancomunadamente entre el Gobierno y la **CONAIE bajo la supervisión de las Naciones Unidas y la Conferencia Episcopal Ecuatoriana, y** aunque el anuncio de las últimas horas es celebrado como una victoria para el pueblo indígena, para otras voces, como el profesor y periodista, Kintto Lucas, no se derogara en su totalidad el decreto ni se eliminará el subsidio a los combustibles, sino que solo se darán unas compensaciones.

"Se puede entender que era difícil sostener la medida por las condiciones de represión. Sin embargo al participar en el nuevo decreto, los indígenas se transformarán en cómplices de la eliminación del subsidio a los combustibles", manifestó el periodista ante la petición del presidente Moreno de cesar las movilizaciones y "medidas de hecho" en todo el país.

> ¡Victoria de la lucha popular!
>
> Exteriores de la Casa de la Cultura Ecuatoriana luego del anuncio de la derogatoria del decreto 883  
> Hoy celebramos, mañana, con todos y todas, minka para limpiar las calles del país.[\#SomosCONAIE](https://twitter.com/hashtag/SomosCONAIE?src=hash&ref_src=twsrc%5Etfw) [pic.twitter.com/OzoFUDT2SQ](https://t.co/OzoFUDT2SQ)
>
> — CONAIE (@CONAIE\_Ecuador) [October 14, 2019](https://twitter.com/CONAIE_Ecuador/status/1183595885441277953?ref_src=twsrc%5Etfw)

<p style="text-align: justify;">
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
</p>
**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
