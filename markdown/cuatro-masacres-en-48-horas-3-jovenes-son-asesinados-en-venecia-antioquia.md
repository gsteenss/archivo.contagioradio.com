Title: Cuatro masacres en 48 horas, 3 jóvenes son asesinados en Venecia, Antioquia
Date: 2020-08-24 11:00
Author: AdminContagio
Category: Actualidad, Nacional
Tags: Antioquia, Jóvenes capturados, masacre, Venecia, Violencia en Colombia
Slug: cuatro-masacres-en-48-horas-3-jovenes-son-asesinados-en-venecia-antioquia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/08/Diseno-sin-titulo-2.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

Luego de que este fin de semana se registraran **3 masacres en territorios como [Arauca](https://archivo.contagioradio.com/las-masacres-aplazadas/), [Cauca](https://archivo.contagioradio.com/seis-jovenes-fueron-torturados-y-asesinados-en-el-tambo-cauca/) y [Tumaco](https://archivo.contagioradio.com/tres-masacres-en-una-semana-seis-jovenes-fueron-asesinados-en-tumaco-narino/), donde perdieron la vida 20 personas**, este domingo se registró otro hecho violento en el que fueron asesinadas 3 personas y una más resultó herida.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

**El hecho se registró a las 6:30 pm en el barrio Los Álamos, municipio de Venecia, ubicado en el suroeste del departamento de Antioquia**, en donde según testigos, dos hombres que llegaron en una motocicleta vestidos de negro y con los rostros cubiertos, dispararon contra 5 personas que se encontraban en una casa a periferias del municipio.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Y en un patrón ya conocido, en los hechos violentos fueron asesinadas personas jóvenes en un rango de los 15 a los 19 años; **de las victimas dos tenían 19 y 18 años de edad, la tercera es un adolescente de 15 años**, a ellos se suman también la agresión a un hombre de 28 años quién resultó herido con arma de fuego.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

**Mientras diferentes personas señalan que este tipo de hechos se dan ante la sordera del Gobierno en los diferentes territorios del país, en donde la respuesta a la crisis es la militarizació, dando así paso a que reine la política de guerra y no la de paz**, el alcalde de este municipio Oscar Sánchez señala que esta masacre es producto de las redes de microtráfico que se disputan el territorio.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/AidaAvellaE/status/1297886002879569921","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/AidaAvellaE/status/1297886002879569921

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/SandraFARC/status/1297743641285472256","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/SandraFARC/status/1297743641285472256

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

Y argumentando que la situación de violencia en el suroeste Antioqueño viene en aumentando por los intereses de estructuras criminales, **dentro de las hipótesis agregó que también este acto de violencia puedo ser producto de la época de cosecha de café** en la que se encuentran.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/DeiAparicio/status/1297730010095202305","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/DeiAparicio/status/1297730010095202305

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

Este hecho se suma a las 45 masacres cometidas durante el 2020, en el que **han sido asesinadas 182 personas, según el más [reciente informe](http://www.indepaz.org.co/informe-de-masacres-en-colombia-durante-el-2020/) de masacres en Colombia**, registrado por el Instituto de Estudios para el Desarrollo y la Paz (Indepaz), de estas 9 corresponden al departamento de Antioquia, 7 a Cauca y Nariño, y 4 al Norte de Santander.

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
