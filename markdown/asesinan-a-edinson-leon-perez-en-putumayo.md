Title: Asesinan a Edinson León Pérez en Putumayo
Date: 2020-06-09 20:34
Author: CtgAdm
Category: Actualidad, Líderes sociales
Tags: asesinato de líderes sociales, Córdoba
Slug: asesinan-a-edinson-leon-perez-en-putumayo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/06/Edison-León-Pérez.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: Putumayo: Comisión de Justicia y Paz

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

El día 8 de junio **fue asesinado el líder social Edinson León Pérez en el municipio de San Miguel, Putumayo** cuando hombres armados le dispararon causándole la muerte. Edison se desempeñaba como presidente de la Junta de Acción Comunal (JAC) de la comunidad de San Juan Bosco y **había recibido amenazas por su liderazgo** al interior de esta organización.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Debido a estas amenazas, Edison tenía medidas de protección con vehículo blindado y dos escoltas por parte de la Unidad Nacional de Protección (UNP), aunque según versiones, **en el momento en el que fue atacado, su esquema de protección no se encontraba presente.** Pese a que aún no está confirmada la autoría del hecho, en el municipio de San Miguel tiene presencia **el grupo heredero del paramilitarismo conocido como La Mafia.** [(Lea también: En Putumayo no cesa la violencia contra la comunidad)](https://archivo.contagioradio.com/en-putumayo-no-cesa-la-violencia-contra-la-comunidad/)

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Crisis humanitaria en Putumayo

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Según la [Comisión Interclesial de Justicia y Paz](https://www.justiciaypazcolombia.com/asesinado-el-lider-social-edinson-leon-perez/) «**a la fecha se registran más de 20 asesinatos en el departamento de Putumayo**, en zonas de control del grupo La Mafia, situación que se ha denunciado desde 2019 y que pese a las Alertas Tempranas emitidas por la Defensoría del Pueblo, nada se ha hecho».

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Este hecho se suma a las muertes violentas de las que ya habían sido víctimas 7 líderes sociales, defensores de derechos humanos y familiares de excombatientes en lo que va corrido del mes de junio. ([Lea también: Liderazgos sociales en alerta máxima. En lo corrido de Junio van 7 asesinatos](https://archivo.contagioradio.com/liderazgos-sociales-en-alerta-maxima-en-lo-corrido-de-junio-van-7-asesinatos/))

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Entre las víctimas fatales se encontraban al igual que Edison León, los también miembros de Juntas de Acción Comunal (JAC) Hermes Loaiza y Arcángel Pantoja en los departamentos de Valle del Cauca y Córdoba respectivamente; lo que denota la **alerta máxima a la que están sometidos los integrantes de dichas corporaciones y en general todas las personas que ejercen liderazgo en los diferentes territorios.**   .

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->
