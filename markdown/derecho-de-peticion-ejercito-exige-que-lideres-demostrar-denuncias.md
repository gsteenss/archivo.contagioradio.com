Title: Con derecho de petición, Ejército exige a líderes del Chocó demostrar lo que denuncian
Date: 2020-01-13 12:09
Author: CtgAdm
Category: Entrevistas, Líderes sociales
Tags: Abandono estatal Chocó, Bajo Atrato, Bojaya, ejercito, Incursiones Paramilitares
Slug: derecho-de-peticion-ejercito-exige-que-lideres-demostrar-denuncias
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/Pacífico.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:block {"ref":78981} /-->

<!-- wp:paragraph {"align":"justify"} -->

Leyner Palacios, secretario ejecutivo de la Comisión Interétnica de la Verdad del Pacífico (CIVP) y uno de los líderes que ha denunciado la frecuente incursión de grupos paramilitares en municipios de Chocó como Bojayá, fue cuestionado por el comandante de la Fuerza de Tarea Conjunta Titán quien, a través de un **derecho de petición exige al líder social entregar información que respalde su testimonio**, controvirtiendo la verdad de las comunidades y relegando una tarea e investigación que debería realizar el propio Estado.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

El pasado fin de semana, el líder social recibió del Ejército un Derecho de Petición, dirigido a él por el comandante Darío Fernando Cardona Castrillón, responsable de la Fuerza de Tarea Conjunta Titán, Por medio del derecho de petición, el comandante cuestiona un reportaje emitido en Noticias Uno en el que Leyner Palacios cuestiona cómo puede ser que cientos de hombres armados entren al río Bojayá sin que la Fuerza Pública se dé cuenta.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Tras sus declaraciones, Cardona indica al líder social que, de estar afirmando la existencia de una connivencia entre las FF.MM. y grupos armados al margen de la ley, entregue nombres de integrantes de la Fuerza Pública, lugar y fecha que prueben estos nexos. [(Lea también: Persisten incursiones paramilitares en el Bajo Atrato)](https://archivo.contagioradio.com/persisten-incursiones-paramilitares-en-el-bajo-atrato/)

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/VerdadPacifico/status/1216472785473720321","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/VerdadPacifico/status/1216472785473720321

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:heading {"level":3} -->

### Se pone en duda la voz de líderes y comunidades

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

A su vez, como si se desconociera la situación que viven las comunidades del medio y bajo Atrato, pidió a Leyner informar sobre los corregimientos en los que hay confinamiento, listar el nombre de las organizaciones que en 2019 denunciaron la presencia de grupos armados y además que dé explicaciones de por qué tienen información sobre lo acontecido, dando a entender que pone en duda la palabra y testimonio de los y las líderes, comunidades, organizaciones sociales del Chocó y la Diócesis de Quibdó.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

El líder social de Bojayá expresó sobre la petición del comandante Cardona que se siente decepcionado de las Fuerzas Militares pues con su actuar, le **han delegado el trabajo de responder las preguntas que debe resolver el propio Ejército Nacional,** además fue enfático en que existe una gran presión sobre él y su familia.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

La Comisión Interetnica de la Verdad de la Región Pacífico [recordó que el deber de la Fuerza de Tarea Conjunta Titán debe ser precisamente el dedicar sus esfuerzos a garantizar la seguridad](https://verdadpacifico.org/la-fuerza-de-tarea-conjunta-titan-deriva-su-responsabilidad-sobre-la-seguridad-en-el-rio-bojaya-a-leyner-palacios/?fbclid=IwAR0wb2JvvEtre1N_RTzun1jkK7M0VzhY153B_xEFD2GZBnHMMyLWGIgZSnY)y la libre circulación de todas las comunidades afro e indígenas del río Bojayá.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### "No importan los hechos sino los denunciantes"

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Al respecto, monseñor Darío de Jesús Monsalve reprochó la actuación del comandante, al confrontar a un líder indefenso "con los instrumentos legales y constitucionales que son precisamente para el pueblo frente a quienes abusan de sus derechos", afirmando que existe una intención de silenciar a todas las personas quienes denuncian hechos violentos en sus regiones, un modo de pensar que se ve reflejando en cómo al Ejército pareciera importarle más los denunciantes que los hechos.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Concluye Monsalve que es necesario continuar con los acuerdos humanitarios regionales y generar un clima de coexistencia, "estos son pasos que debe dar el país, no podemos detenernos en la lucha con las comunidades", aseguró. [(Le puede interesar: Obispos piden reabrir diálogos con ELN y garantías para líderes y comunidades)](https://archivo.contagioradio.com/obispos-piden-reabrir-dialogos-con-eln-y-garantias-para-lideres-y-comunidades/)

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78980} /-->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->
