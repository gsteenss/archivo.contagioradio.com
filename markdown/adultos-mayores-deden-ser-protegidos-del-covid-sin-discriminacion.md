Title: Adultos mayores deben ser protegidos del COVID sin discriminación
Date: 2020-07-06 20:32
Author: PracticasCR
Category: Actualidad, Nacional
Tags: Adultos mayores, Juzgado 61, pensiones, Presidente Ivan Duque, Rebelión de las canas
Slug: adultos-mayores-deden-ser-protegidos-del-covid-sin-discriminacion
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/07/Adultos-mayores.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

La semana pasada se dio a conocer el fallo de tutela que **ordenó al presidente Duque dejar de aplicar las restricciones impuestas a los adultos mayores de 70 años en medio de la contingencia del Covid-19**, pues según el Juzgado 61 de Bogotá esto configura una conducta discriminatoria frente a dicho grupo poblacional. (Le puede interesar: [Otra Mirada: Niños y mayores también cuentan durante la pandemia](https://archivo.contagioradio.com/otra-mirada-ninos-y-mayores-tambien-cuentan-durante-la-pandemia/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En el fallo de tutela, la Juez 61 consideró que: «*el hecho de que el Estado limite su libertad de locomoción, de forma más estricta que el restante de la ciudadanía, configura una **conducta discriminatoria frente a los adultos mayores**, razón por la cual, \[...\] el objetivo perseguido con la medida no es válido y, por ende, (es) **inconstitucional***».

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Tras conocer el fallo de la tutela, **el presidente Iván Duque anuncio que impugnaría la decisión para reinstaurar las medidas restrictivas aplicadas a las personas mayores de 70 años.**

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Condiciones de vulnerabilidad económica de los adultos mayores

<!-- /wp:heading -->

<!-- wp:paragraph -->

Como otros grupos poblacionales, muchas de las personas mayores que se ven imposibilitadas para cumplir las medidas de confinamiento, lo están en razón de que **no tienen un ingreso para cubrir su mínima subsistencia y se ven obligadas a salir a buscar su sustento diario.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Hace unas semanas se dio a conocer el caso de abuso policial en contra de un vendedor informal de la tercera edad.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https:\/\/twitter.com\/JuanAbelG\/status\/1263229118654681094","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/JuanAbelG/status/1263229118654681094

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

Según el gremio de aseguradoras Fasecolda, en los últimos 6 años, **apenas un 37% de los aportantes al sistema de seguridad social del país ha logrado pensionarse.** Considerando además que otra parte de la población no llega siquiera a cotizar porque se desempeña en trabajos informales o porque es desempleada, solo un 30% de los colombianos aspira a obtener una pensión de vejez.

<!-- /wp:paragraph -->

<!-- wp:quote -->

> ***Solo tres (3) de cada diez (10) colombianos lograrán pensionarse***»
>
> <cite>Mario Cruz, director de la Cámara de Seguridad Social de Fasecolda</cite>

<!-- /wp:quote -->

<!-- wp:paragraph -->

Pese a este panorama, agravado por la actual pandemia, la única solución que se ha dado desde el Gobierno ha sido la entrega del auxilio económico denominado **Ingreso Solidario**, el cual ha tenido críticas por parte de varios sectores que lo consideran **insuficiente** y que además han hecho reparos sobre el **margen de intermediación de las entidades financieras para su entrega a los beneficiarios.** (Le puede interesar: [Proyecto de Renta Básica de Emergencia contaría con mayorías en el congreso](https://archivo.contagioradio.com/proyecto-de-renta-basica-de-emergencia-contaria-con-mayorias-en-el-congreso/))

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https:\/\/twitter.com\/navarrowolff\/status\/1257456227929423874","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/navarrowolff/status/1257456227929423874

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:core-embed/twitter {"url":"https:\/\/twitter.com\/wilsonariasc\/status\/1279067757322088448","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/wilsonariasc/status/1279067757322088448

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:heading {"level":3} -->

### Igualdad sin distinción de edad

<!-- /wp:heading -->

<!-- wp:paragraph -->

Tanto la Constitución como las normas internacionales han sido expresas en prohibir todo tipo de discriminación con base en cualquier condición humana, incluida la edad. (Lea también: [Lecciones desde el territorio: la importancia de niños y ancianos en medio de la pandemia](https://archivo.contagioradio.com/lecciones-desde-el-territorio-la-importancia-de-ninos-y-ancianos-en-medio-de-la-pandemia/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por ello, la Comisión Interamericana de Derechos Humanos expidió la [Resolución 1/2020](https://www.oas.org/es/cidh/decisiones/pdf/Resolucion-1-20-es.pdf) «Pandemia y Derechos Humanos en las Américas» donde recomienda a los Estados miembros de la Convención la adopción de una serie de medidas en relación con los adultos mayores.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Entre dichas directrices se encuentran: La inclusión prioritaria en materia médica para prevenir y tratar los efectos del coronavirus garantizándoles el acceso a pruebas Covid-19, acceso a medicamentos y cuidados paliativos; el monitoreo y vigilancia contra la violencia hacia personas mayores; y la consideración en la adopción de medidas de contingencia de un balance entre la protección contra el virus, frente a la necesidad particular de las personas mayores de conectarse con sus familiares en medio del confinamiento.

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
