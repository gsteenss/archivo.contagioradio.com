Title: ¡Que no se queden en papel promesas para defender la vida de líderes sociales!
Date: 2019-04-16 13:19
Author: CtgAdm
Category: Entrevistas, Líderes sociales
Tags: Fiscalía, lideres sociales, paz, Somos defensores
Slug: defender-vida-lideres-sociales
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/lideres-sociales-colombia.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

La iniciativa ciudadana Defendamos la Paz emitió una nueva comunicación en la que manifiesta su preocupación ante el asesinato sistemático de líderes sociales y pide acciones urgentes para detener este flagelo. Los firmantes solicitan que se implementen las normativas existentes para la protección de las comunidades, y se investiguen los hechos de asesinato así como de amenazas que constantemente reciben quienes defienden los derechos humanos.

> Grupo Defendamos La Paz [@DefendamosPaz](https://twitter.com/DefendamosPaz?ref_src=twsrc%5Etfw) llama al Presidente [@IvanDuque](https://twitter.com/IvanDuque?ref_src=twsrc%5Etfw) a que paren los asesinatos de líderes sociales y haya una política pública de garantías! [@Ccajar](https://twitter.com/Ccajar?ref_src=twsrc%5Etfw) [pic.twitter.com/Kt92NaZNlm](https://t.co/Kt92NaZNlm)
>
> — Alirio Uribe Muñoz (@AlirioUribeMuoz) [15 de abril de 2019](https://twitter.com/AlirioUribeMuoz/status/1117762877463592960?ref_src=twsrc%5Etfw)

<p>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
</p>
### **"Matar a lideresas y líderes sociales es un crimen contra la paz"** 

Alirio Uribe Muñoz, uno de los integrantes de la Iniciativa explicó que están preocupados porque el Gobierno no tiene una política pública clara en materia de protección a liderazgos sociales, pues desde la firma del Acuerdo de Paz han sido asesinados más de 570 líderes. "Estos son crímenes contra la paz, porque se suponía que la paz iba a significar el fin de la violencia", y está ocurriendo lo contrario: aparecen de nuevo reportes sobre masacres, asesinatos y desplazamientos forzados.

En ese sentido, Uribe Muñoz explicó que el Gobierno podría implementar el Decreto 660 de 2018, la Resolución 845 de 2018 y la directiva 02 de la Procuraduría; al tiempo que se debería impulsar la aplicación de lo acordado en los puntos 2,3 y 4 del Acuerdo de Paz, abrir la posibilidad de continuar los diálogos con el ELN y "desmontar las estructuras paramilitares". (Le puede interesar: ["¡A la Corte Penal Internacional para exigir protección a líderes"](https://archivo.contagioradio.com/la-corte-penal-internacional-exigir-proteccion-lideres/))

### **El Fiscal miente en las cifras sobre investigaciones en casos de líderes asesinados** 

El Activista anunció que durante esta semana el Programa Somos Defensores publicará un informe en el que demostrará que "el Fiscal General está mintiendo cuando señala en los informes que ha resuelto el 50% de los casos de asesinato de líderes sociales", porque aunque la Fiscalía ha pedido medida de aseguramiento para más de 180 personas involucradas en estos hechos, en su mayoría son autores materiales y no intelectuales; adicionalmente, no se investigan como debería las amenazas a los líderes.

En ese sentido, es necesario que la unidad especial creada para tratar este tema actúe encontrando los máximos responsables de los hechos, para evitar que sigan determinando la muerte de los y las defensoras de derechos humanos; al tiempo que se investigue el origen de las amenazas en su contra para determinar de mejor forma de quienes se trata. (Le puede interesar: ["Samuel David, hijo de excombatientes fue víctima de la guerra con solo siete meses"](https://archivo.contagioradio.com/samuel-david-hijo-de-excombatientes-fue-victima-de-la-guerra-con-tan-solo-siete-meses/))

### **El refugio humanitario por la vida se tomará Bogotá** 

Del próximo 28 de abril al 3 de mayo, 3 mil líderes de todo el país llegarán a Bogotá para contar lo que está pasando en sus regiones; con ello, Uribe Muñoz señaló que esperan lograr compromisos de la ciudadanía con la protección de estas personas para lograr que los planes de protección colectiva se construyan desde las comunidades, evitando una visión de seguridad que funciona en la ciudad pero no siempre en los territorios. También se buscará el reconocimiento de figuras como los territorios humanitarios y aldeas de paz, así como los mecanismos de autoprotección como las guardias indígenas, campesinas y cimarronas.

<iframe id="audio_34554440" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_34554440_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
