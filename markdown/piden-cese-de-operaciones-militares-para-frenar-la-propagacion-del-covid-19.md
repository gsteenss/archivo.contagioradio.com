Title: Piden cese de operaciones militares para frenar la propagación del COVID 19
Date: 2020-03-18 13:18
Author: CtgAdm
Category: Actualidad, Nacional
Tags: Acuerdo Humanitario, colombia, ejercito, Fuerzas Militares de Colombia, pandemia
Slug: piden-cese-de-operaciones-militares-para-frenar-la-propagacion-del-covid-19
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/Militares-e1571869281157.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

En una serie de cartas dirigidas a las FFMM, las FARC que permanecen en armas, las AGC, el ELN y todos los grupos armados que operan en sus territorios, **más de 120 comunidades y organizaciones pidieron un cese de operaciones militares para frenar la propagación de la pandemia COVID 19**. En las comunicaciones las comunidades hacen un paquete de propuestas como acuerdos humanitarios.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Las comunidades firmantes que viven en medio de la guerra y habitan departamentos como [Putumayo](https://archivo.contagioradio.com/mas-de-10-000-familias-en-putumayo-afectadas-por-incumplimiento-del-gobierno-en-acuerdo-de-sustitucion/), Chocó, Antioquia, [Cauca](https://archivo.contagioradio.com/argelia-hasta-mil-personas-desplazadas-por-combates-y-el-olvido-del-estado/), Valle del Cauca, entre otros aseguran que en medio de la declaración de la OMS es necesario que "***Aprovechemos el COVID - 19 para pensar en la vida de cada uno de ustedes, en la vida de cada uno de nosotros, en la vida del paí**s, asuman la reflexión en sus cuadrillas, frentes, brigadas, batallones, comandancias. De nuestras soberbias nada queda, del vano orgullo tampoco. Es el tiempo de la solidaridad y desde ella la paz en una democracia nueva*."

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Las propuestas concretas son:

<!-- /wp:paragraph -->

<!-- wp:list {"ordered":true} -->

1.  Informar a todo su personal de la pandemia COVID - 19 y las consecuencias para sus vidas y las de quiénes están en contacto con ellos.
2.  Formarles en los mecanismos de prevención.
3.  Permanecer quietos en sus lugares de campamentación sin realizar actuaciones de avanzadas y ofensivas. Solo si hubiera ataques e incumplimiento por los contrarios de esta propuesta implícito de [Acuerdo Humanitario](https://archivo.contagioradio.com/en-colombia-hay-un-narcoestado-paramilitar/) Global por Pandemia.  
   Esta solicitud la hacemos también explicita a FFMM y Policiales, organismos de seguridad, erradicadores; tenemos reporte del virus contagiado en personal de las FFMM de los Estados Unidos.
4.  Retirar su personal de nuestros entornos o comunidades y ubicarlos a distancias que impidan expandir el virus.
5.  Abstenerse de convocar a algún tipo de reunión obligatoria.

<!-- /wp:list -->

<!-- wp:paragraph -->

También recordaron que **sus situaciones de vida no son las mejores para afrontar la [pandemia](https://www.justiciaypazcolombia.com/solicitud-para-que-colombia-adquiera-el-antiviral-cubano-interferon-alfa-2b-para-evitar-propagacion-del-covid-19/)**, por ejemplo aseguran que comunidades en algunas regiones están viviendo sequías, otras regiones siguen siendo afectadas por el invierno. De esta manera reforzaron su mensaje de que "Las estrategias armadas por razón de humanidad, de toda la humanidad deben parar por lo menos por dos semanas, hasta el 1 de abril, desde mañana con prórroga por lo menos hasta el 30 de mayo."  

<!-- /wp:paragraph -->
