Title: Salario mínimo aumenta el 7% en Colombia
Date: 2015-12-30 09:21
Category: Economía, Nacional
Tags: Centra Unitaria de Trabajadores, CUT, economía. Colombia, ISAGEN, salario minimo
Slug: salario-minimo-aumenta-el-7-en-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/12/Salario-mínimo-Colombia1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: inflación.com 

###### [30 Dic 2015]

Por decreto, el Gobierno Nacional incrementó el [salario mínimo](https://archivo.contagioradio.com/aumento-del-salario-minimo-para-el-2016-rondaria-los-45-mil-pesos/) un 7% para el 2016. Queda en \$689.454 pesos, es decir un aumento de \$45103 y el subsidio de transporte queda decretado en \$ 79.180. Para integrantes de la **Central Unitaria de Trabajadores** este salario mínimo no alcanza para la mitad de los productos de la canasta familiar.

En términos reales el aumento solamente sería de 0.2% o 0.3% puntos porcentuales dadas las cifras de inflación que rondan el 6%. Adicionalmente el **gobierno colombiano estudia una reforma tributaria para el 2016 que aumentaría el Impuesto al Valor Agregado IVA hasta el 18% o el 19%** y también se ampliarían los productos gravables de la canasta familiar.

Adicionalmente **las alzas en las tarifas del transporte masivo en ciudades como Bogotá, Cali y Barranquilla rondarían los 200** pesos para el primer semestre de 2016. A esta situación se suma que los precios de la gasolina en Colombia no estarán por debajo de los 7000 pesos en todo el año independientemente de los precios internacionales del petroleo que se mantendrían a la baja.

Para diversos analistas consultados por Contagio Radio, el aumento del salario mínimo afecta gravemente el poder adquisitivo de los trabajadores y trabajadoras colombianas y podría significar un nuevo freno a la economía nacional. Las mismas voces también han criticado otras medidas aplicadas por la **hacienda nacional como la [subasta de la empresa ISAGEN](https://archivo.contagioradio.com/venta-de-isagen-seria-el-peor-negocio-para-el-pais/)** que se realizará los primeros días del mes de enero.

A través de redes sociales se han manifestado opiniones en torno al anuncio sobre isagen como "vender Isagen para tener plata para las vías 4G es como vender la casa para pavimentar el anden".
