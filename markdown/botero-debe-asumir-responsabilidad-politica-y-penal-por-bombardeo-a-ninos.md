Title: Botero debe asumir responsabilidad política y penal por bombardeo a niños
Date: 2019-11-06 11:06
Author: CtgAdm
Category: DDHH, Entrevistas
Tags: Asesinato de menores de edad, Caquetá, Ejecuciones Extrajudiciales, Fuerzas militares, Guillermo Botero, ministerio de defensa
Slug: botero-debe-asumir-responsabilidad-politica-y-penal-por-bombardeo-a-ninos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/Diseño-sin-título-5.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: SenadoGovCo] 

En medio del debate de la segunda moción de censura que se impulsó contra al ministro de Defensa, Guillermo Botero, el senador Roy Barreras denunció cómo el 2 de septiembre, inteligencia militar realizó un bombardeo en Caquetá contra integrantes de las disidencias y en el que habrían sido asesinados siete menores de edad, información que fue ocultada al país y que deja en evidencia el nivel de responsabilidad del funcionario ante este suceso.

La información aportada por Medicina Legal y el senador Barreras la tarde del pasado  5 de noviembre, establece que los menores asesinados eran **Abimiller Morales de 17 años, Willmer Castro de 17, Diana Medina de 16 años, José Rojas Andrade  de 15 años, Jhon Edinson Pinzón de 17,  Ángela María Gaitán quien tenía 12 años y Sandra Patricia Vargas de 16.**

Según el personero de Puerto Rico, Caquetá, Herner Carreño, los reclutamientos se dieron la última semana de julio en las veredas de **Pringamoso, Villa Hermosa Alta y La Flor,** en cercanías al  el corregimiento de Lusitania en el municipio de Puerto Rico. [(Le puede interesar: La estrategia del paramilitarismo para atraer a jóvenes del Chocó a sus filas)](https://archivo.contagioradio.com/la-estrategia-del-paramilitarismo-para-atraer-a-jovenes-del-choco-a-sus-filas/)

Para el analista Víctor de Currea Lugo, este suceso debe trascender de un debate jurídico, de cara al pronunciamiento de el personero quien indicó que meses atrás había denunciado ante el Gobierno y consejos de seguridad el reclutamiento de menores en la zona e incluso había entregado los nombres de los niños y niñas.

"Esconderle esas muertes a Colombia es razón suficiente para que este Senado lo censure, como estoy seguro que lo hará por primera vez" manifestó el senador Roy Barreras quien también resalta que dentro de la “nueva política” del Ministerio de Defensa se disminuyeron los estándares de certeza previos a las operaciones.

"Está claro que había una premeditación, el Derecho Internacional Humanitario (DIH) es claro en que los civiles que toman decisiones militares en el conflicto, son igualmente responsables y en ese sentido el ministro Botero debe responder", afirma Currea Lugo.

### El problema no es solo Botero, es la política de seguridad del Gobierno

De Currea Lugo advierte que el problema va más allá de la censura al ministro Botero y que se trata de un problema enfocado en la doctrina militar del Gobierno, lo que deja ver que **los asesinatos de Dímar Torres en el Catatumbo, Flower Trompeta en Cauca y este reciente hecho que involucra las muertes de menores de edad, no son hechos aislados** sino que obedecen a la lógica con la que trabaja la Fuerza Pública. [(Lea también: Militar implicado señala que Ejército ordenó seguir a Dimar Torres) ](https://archivo.contagioradio.com/militar-implicado-senala-que-ejercito-ordeno-seguir-a-dimar-torres/)

Ante la confirmación de este bombardeo por parte del mismo ministro Botero, este justifico su accionar al  afirmar que fue un procedimiento legítimo, responsabilizando a las disidencias de retener menores en sus campamentos, sin embargo,  para Juan Sebastián Soris de Justapaz  hay que preguntarse el rol del funcionario, de la inteligencia militar y general de la Cúpula Militar, **"son niños a los que les falla el Estado, que son reclutados y luego son presentados como las grandes bajas, el éxito de una operación"**.

La votación del Congreso con respecto a la salida o permanencia de Botero, tendrá lugar el miércoles de la próxima semana y ya conocidas las posiciones de las bancadas de opinión y del partido de Gobierno, la decisión dependerá de Cambio Radical y el Partido Liberal.  [(Le puede interesar: Moción de censura a Botero, una oportunidad para rediseñar política de defensa nacional)](https://archivo.contagioradio.com/mocion-de-censura-botero-oportunidad-para-redisenar-politica-de-defensa/)

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
