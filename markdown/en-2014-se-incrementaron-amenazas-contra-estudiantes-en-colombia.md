Title: En 2014 se incrementaron amenazas contra estudiantes en Colombia
Date: 2015-02-02 21:09
Author: CtgAdm
Category: DDHH, Educación
Tags: Amenazas, DDHH, estudiantes, Movilización, Paramilitarismo
Slug: en-2014-se-incrementaron-amenazas-contra-estudiantes-en-colombia
Status: published

###### Foto: Carmela María 

##### [Carolina Rendón, UN]<iframe src="http://www.ivoox.com/player_ek_4029579_2_1.html?data=lZWfm5qbfY6ZmKiak5eJd6KklZKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRkIa3lIqupsnJtsbnjMrg1trIrcLi1c7Zx9iPt8af08qSpZiJhqLixtOYxdTSb6Xd08rQ1tTWb8XZjLXczs7HcYarpJKw0dPYpcjd0JC%2Fw8nNs4yhhpywj5k%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe> 

[En el último año se ha incrementaron en un 170% las amenazas contra defensores de Derechos Humanos y líderes sociales en Colombia, y el movimiento estudiantil no ha estado exento de la problemática. **En el 2014, se presentaron múltiples amenazas contra estudiantes de las universidades de Córdoba, Cauca, Tecnológica de Pereira, Pedagógica Nacional, y la Universidad Nacional de Colombia.**]

El viernes 30 de enero, representantes estudiantiles de la Universidad Nacional de Colombia se reunieron en la Presidencia de la República con el Director de la Policía Nacional, Grl. Oscar Naranjo, para discutir la **grave situación de Derechos Humanos que vive el movimiento estudiantil en Colombia. **

Los y las estudiantes aseguran que la violación a los Derechos Humanos de las comunidades universitarias es en Colombia una política de Estado, que existe la necesidad de **reconocer al movimiento estudiantil como un grupo de riesgo** y generar todo tipo de medidas que realmente garanticen el papel de los estudiantes como agentes constructores de paz.

El General Oscar Naranjo se comprometió con la delegación estudiantil a generar un ambiente de **protección a la comunidad universitaria**; abrir espacios para debatir sobre la construcción de la paz, el tratamiento de la conflictividad social, garantías para la participación política y el uso de la fuerza por parte del Estado.

En un año en que el gobierno espera aprobar el Plan Nacional de Desarrollo 2015-2018, y con él, implementar  la política pública para educación "Acuerdo por lo Superior 2034", con la que los y las estudiantes de la Mesa Amplia Nacional Estudiantil han anunciado no estar de acuerdo, **se auguran procesos de movilización universitaria en todo el país**. Amanecerá y veremos la respuesta del gobierno, y el cumplimiento de los compromisos anunciados por Naranjo a los delegados del movimiento estudiantil.
