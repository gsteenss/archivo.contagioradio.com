Title: Los tres géneros de la salsa que lo pondrán a bailar
Date: 2015-10-08 11:19
Category: En clave de son
Tags: danzón, Géneros de la salsa, Historia de la Salsa, mambo, Programas de radio sobre salsa, son cubano, variaciones de la salsa
Slug: los-inicios-de-la-salsa
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/10/hurd-salsa-36x24-sold.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [08 Oct 2015]

La salsa es un género musical versátil, desde sus inicios hasta el dia de hoy, ha tenido una gran cantidad de variaciones en cuanto al ritmo, el tempo y la interpretación tanto instrumental como vocal. Es por eso que hoy se puede escuchar, y bailar, desde la cadenciosa salsa rosa hasta las fusiones urbanas que han derivado en la salsa choke.

La salsa en sí misma es también resultado de otras mixturas musicales, surgidas tanto en el barrio como en los grandes salones, con letras y estilos que en combinación permitieron la re estructuración de sus bases sonoras tradicionales, de ahí la importancia que tiene el abordar algunos de los géneros anteriormente inmediatos a la salsa.

**Son Cubano:**

Nace dentro de los “minifundios”, plantaciones de tabaco, otros dedicadas a la ganaderia, entre algunos sectores, gracias culturalidad implícita en estas practicas económicas que permitirián la expansión generosa del género, desde las antillas hasta influir en la musica caribeña.

Una de las características más importantes del Son, musicalmente hablando, es la expresividad del interprete, ya que el género como tal, narra el día a día de los personajes de los que hablabamos anteriormente. La guitarra también hace parte fundamental al tener en su interpretación técnicas tales como el punto libre y la seguidilla entre otras.

**Mambo:**

Este término, tiene un origen congoleño, que algunos traducen como una simple conversación, pero otros la interpretan como conversación con los dioses. Pero más allá del origen de la palabra, el mambo es una conversación entre las partes de la orquesta y de la orquesta misma con los bailadores, por ello inicialmente se úbica en el ambiente de la danza cerca de la decada del 30.

**Danzón:**

El danzón es una herencia Europea, que hace referencia a los grandes bailes de salón, adaptados a ese sector “criollo” como se denominaban a los caribeños, esto daría una música de salon, mucho mas bailable.

Si quiere conocer mas de estos generos, si quiere escuchar uno por uno por medio de la música de algunos de sus máximos exponentes, lo invitamos a darle play a nuestro poadcast.

<iframe src="http://www.ivoox.com/player_ek_8848873_2_1.html?data=mZ2hmp2bd46ZmKiakpeJd6KmmYqgo5mVcYarpJKfj4qbh46kjoqkpZKUcYarpJKYr8bRptCZk6iYtdTSb9qfpcbb3Iqnd4a2lNOSlKiPsdbXydSYz4qnd4a1ktiY09rJb9TVzdjOj4qbh4630NPhw8zNs4zGwsnW0ZCRaZi3jpk%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>
