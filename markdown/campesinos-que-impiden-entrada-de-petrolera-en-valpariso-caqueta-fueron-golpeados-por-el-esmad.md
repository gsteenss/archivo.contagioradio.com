Title: Campesinos que impiden entrada de petrolera en Valparíso, Caquetá fueron golpeados por el ESMAD
Date: 2015-07-01 16:51
Author: AdminContagio
Category: Nacional, Resistencias
Tags: Caquetá, conflicto armado, Defensoría del Pueblo, Desplazamiento forzado, Emerald Energy, ESMAD, exploración de petróleo, Explotación petrolera, extracción petrolera, Florencia, revictimización, San Vicente del Caguán, Valparaíso
Slug: campesinos-que-impiden-entrada-de-petrolera-en-valpariso-caqueta-fueron-golpeados-por-el-esmad
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/07/1518219_1652358444980672_201858220648405920_n.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Contagio Radio 

<iframe src="http://www.ivoox.com/player_ek_4710537_2_1.html?data=lZyekpqXe46ZmKiakpyJd6KpkpKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRh8Lh0crgy9PTt4zYxpDDw9HUpdOZpJiSo6nXs4ampJCww9bZqdWZpJiSo5aPt9DijMbU1MrIrcXj1JDd0dePqc2hhpywj6jTstXVyM7cjbfFqMrjjJKSmaiReA%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Gregorio Pérez, habitante Valparaíso] 

<iframe src="http://www.ivoox.com/player_ek_4710555_2_1.html?data=lZyekpqZeY6ZmKiakp2Jd6KpmZKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRic%2Fo08rjy9jYpYzX0NOYqdfJq9DmytSYsoqnd4a1mtfS3IqWh4zcwsfW1sbSuMaft8bZ0sbWpYa3lIquptjTaZO3jJKSmaiRh9Di1cbUy9SPlsLYytSYj4qbh46o&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [José Saldarriaga, habitante Valparaíso] 

<iframe src="http://www.ivoox.com/player_ek_4711043_2_1.html?data=lZyek5WYd46ZmKiak5WJd6KlkpKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRh8Lh0crgy9PTt4zl1sqYy9LUrcXZz5DS0NnWpcXVjMnSjdXJuNPjzcrfw5DJsozKwtHdw9eJh5SZoqmah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [   Arturo Mayorga, abogado de la comunidad de Valparaíso] 

###### [1 Jul 2015]

Aproximadamente **diez personas resultaron heridas a manos del ESMAD, cuando la comunidad de Valparaiso**, Caquetá intentaba retener la entrada de la empresa china **Emerald Energy**. El objetivo de la población que lleva dos meses resistiendo, es proteger la biodiversidad y las fuentes hídricas de su territorio.

Tras conocer los efectos nocivos que trajo la exploración y extracción de hidrocarburos en San Vicente del Caguán  y San José del Fragua, en ese mismo departamento, la comunidad decidió no permitir la entrada de la maquinaria a su municipio, teniendo en cuenta que el **pozo que se planea construir queda a unos 130 metros del río.**

**“El gobierno se ha lavado las manos y no ha dicho nada”**, asegura Gregorio Pérez, habitante de Valparaiso, quien cuenta que en la junta de acción comunal se desarrolló una consulta popular para saber si los campesinos querían aceptar que se realizara el proyecto. Según Pérez, **el 99% de de la población no aprobó la presencia de Emerald Energy,** y aunque esos resultados se mostraron al gobierno Santos, se hizo caso omiso a ellos y se continúo “a la fuerza” realizando el proyecto, argumentando que el Ministerio de Minas y Energía ya había dado el permiso.

Es por eso, que la comunidad decidió oponerse a la entrada de la maquinaria de forma pacífica. Sin embargo, fue el martes a las ocho de la mañana cuando llegó el ESMAD, y al ver que los campesinos no querían dejar ingresar la maquinaria de la petrolera, los empezaron a **agredir durante 2 horas, lo que dejó diez heridos, entre ellos, uno de gravedad.**

José Saldarriaga, campesino de zona y quien presenció el ataque de la fuerza pública, cuenta que los agentes del **ESMAD amenazaban  a los habitantes con “darles plomo”**.

Por su parte, Arturo Mayorga, abogado de la población, asegura que este tipo de actos de parte del gobierno lo que hacen es **revictimizar a las personas, que ya habían sido víctimas de desplazamiento forzado** por cuenta del conflicto armado en el año 2003.

La comunidad exige la presencia de las entidades gubernamentales como la Defensoría del Pueblo y el Ministerio Público, para que se verifique la violación de derechos humanos de la que fueron víctimas los pobladores de Valparaiso.  Por el momento, se espera la presencia del defensor del pueblo de la región, y se cuenta con el apoyo de diez diputados departamentales que se han manifestado a favor de la comunidad.
