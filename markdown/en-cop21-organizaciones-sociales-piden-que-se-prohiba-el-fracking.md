Title: En COP21 organizaciones sociales piden que se prohíba el fracking
Date: 2015-12-10 15:21
Category: Ambiente, Movilización
Tags: AIDA, Ambiente y Sociedad, Calentamiento global, cambio climatico, COP21, Derechos Humanos, Ecologistas en Acción, fracking
Slug: en-cop21-organizaciones-sociales-piden-que-se-prohiba-el-fracking
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/12/no-al-fracking.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: [www.europapress.es]

###### 10 Dic 2015 

Diversas organizaciones de la sociedad civil en Latinoamérica han pedido a los gobierno están haciendo presencia en COP21, prohibir las operaciones de fracking o la fracturación hidráulica teniendo en cuenta que esta actividad no contribuye a enfrentar el cambio climático y en cambio puede generar que la temperatura de la tierra continúe aumentando.

**El próximo 11 de diciembre, las organizaciones defensoras del ambiente y los derechos humanos, presentarán un documento en el que explican las razones por las que debe suspenderse y su vez prohibirse esta actividad.** Allí argumentarán que  “durante el ciclo de extracción, procesamiento, almacenamiento, traslado y distribución de los hidrocarburos no convencionales extraídos vía fracking, se libera metano que, como gas de efecto invernadero, es 87 veces más activo que el dióxido de carbono en un margen temporal de 20 años, lo que provoca mayor calentamiento global”, dice el pronunciamiento dirigido a los Estados.

Además también se visibilizará cómo la fracturación hidráulica ha afectado negativamente al ambiente y a las comunidades donde se realiza esta práctica que tiene como fin aumentar la extracción de petróleo. Entre estos casos, se evidencia que **México, Colombia, Argentina, Chile y Bolivia ya han iniciado actividades de exploración o explotación de hidrocarburos no convencionales a través del fracking.**

Frente a lo anterior, en el documento expresan que “**el fracking está avanzando a ciegas en América Latina, sin que existan estudios integrales y de largo plazo** sobre los riesgos y daños graves e irreversibles que puede causar en la salud de las personas y en el ambiente”, señala Ariel Pérez Castellón, abogado de AIDA, Asociación Interamericana para la Defensa del Ambiente.

Por su parte, Milena Bernal, abogada de Ambiente y Sociedad asegura que  “Las operaciones de este tipo en la región no han respetado derechos humanos fundamentales como la consulta y el consentimiento previo, libre e informado; el derecho a la participación y control social; y el derecho a la información”.

Así mismo, la organización ambientalista Ecologistas en Acción se suma a esta iniciativa, y en el marco de COP21 presentará un libro coordinado por la organización: **Resistencia global al fracking, con 15 experiencias de resistencia en todo el mundo.**

Finalmente, **la carta solicita que se vinculen los derechos humanos** a los acuerdos a los que se llegue en la COP 21.
