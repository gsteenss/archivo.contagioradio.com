Title: Brasil exige respuestas ante asesinato de la activista Marielle Franco
Date: 2018-03-16 17:15
Category: El mundo
Tags: Brasil, Marielle Franco
Slug: brasil_asesinato_marielle_franco
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/03/5aaaefb977577.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: EFE] 

###### [16 Mar 2018] 

[Tras el asesinato de la brasileña Marielle Franco, concejal, activista y defensora de los derechos humanos, Brasil se ha manifestado masivamente frente a ese crimen ocurrido el pasado 14 de marzo de 2018. Principalmente las personas se han movilizado en las ciudades de Sao Paulo y Río de Janeiro.]

Marielle Franco fue criada en la Favela de Maré, una de las más peligrosas en Rio de Janeiro. Una mujer muy preparada. Era magíster en Administración Pública por la Universidad Federal Fluminense (UFF), fue electa concejala en la Cámara Municipal de Río de Janeiro​ por la coalición Cambiar es posible, con más de 46 mil votos y fue la quinta candidata más votada en la ciudad.

Esta activista feminista, madre y mujer fue símbolo de lucha de las mujeres negras brasileñas contra el racismo, el machismo y la violencia policial. Una de sus principales  luchas fue contra la militarización de las favelas en su país, ante las violaciones a los derechos humanos de los civiles por parte de las fuerzas estatales. Por ello, muchos de los que rodeaban a Franco, aseguran que precisamente esa lucha fue la que le quitó la vida con nueve disparos.

["Mi perspectiva cuando llegué ahí era la de una mujer negra de la favela, perteneciente a la Maré. Y había una disputa... sobre si mi cuerpo estaría en ese lugar de enseñanza de calidad, porque soy favelada", había dicho Marielle Franco, en una movilización de mujeres negras, minutos antes de que fuera asesinada.]

[Marcelo Freixo, integrante y compañero de Marielle del Partido Partido Socialismo y Libertad, ha asegurado a medios locales que “las características son las de una ejecución. Tenemos que resolver esto lo más pronto posible, no por cada uno de nosotros sino por Rio de Janeiro. Esto es totalmente inadmisible, era un persona llena de vida y de deseos, era fundamental para Rio”.]

[El asesinato del concejal no es único caso. En el 2016 hubo 925 personas muertas durante las operaciones policiales, según Think Tank Foro de Seguridad Pública de Brasil. Las organizaciones de derechos humanos aseguran que estos hechos se han incrementado considerablemente con el gobierno  Temer.]

### **ONU pide investigación independiente** 

Ante el asesinato de Franco, la Oficina del Alto Comisionado de la Organización de las Naciones Unidas para los Derechos Humanos anunció que es necesario que para esclarecer los hechos de este crimen, se asegure una investigación transparente  e independiente.

Tras condenar el asesinato, la ONU señaló, "Entendemos que las autoridades se comprometerán a realizar una completa investigación de los asesinatos ocurridos en Río de Janeiro el miércoles en la noche". Además manifestó que el Estado debe dar el mayor esfuerzo para** **dar con los responsables y llevarlos ante los tribunales.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
