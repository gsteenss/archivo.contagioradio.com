Title: Administración Peñalosa hace oídos sordos para detener la venta de la ETB
Date: 2017-03-07 16:47
Category: DDHH, Nacional
Tags: Bogotá, ETB, Peñalosa
Slug: administracion-penalosa-hace-oidos-sordos-para-detener-la-venta-de-la-etb
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/03/Enrique_Peñalosa-e1488922778412.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Wikimedia Commons] 

###### [07 Mar 2017] 

Pese a que el alcalde Peñalosa reconoció en el Cabildo Abierto convocado por la ciudadanía, que la Empresa Pública de Telefonía de Bogotá, ETB, **no se “está marchitando”, confirmó la decisión de la venta de la empresa, entre abucheos y denuncias por parte de la ciudadanía.**

Peñalosa señaló que los motivos para vender la ETB radican en la necesidad de una inversión social, ya que **“seguir invirtiendo en la empresa significa un alto riesgo”** para la capital, que de acuerdo con el Alcalde se evidencia en la perdida de participación en el mercado y la falta de rentabilidad de la fibra óptica y la telefonía de la empresa.

Alejandra Wilches, presidenta de ATELCA, uno de los sindicatos que participó en el Cabildo, anunció que frente a estas afirmaciones y a la presentación de los informes del gerente Jorge Castellanos, sobre los estados financieros de la ETB, se va a instaurar una denuncia por injuria, **“nosotros estamos defendiendo a la ETB porque venderla es un desfalco para la ciudadanía”** y agregó que no es cierto que la empresa este en pérdidas. Le puede interesar: ["Ya se han recolectado 200 mil firmas para la venta de la ETB"](https://archivo.contagioradio.com/ya-se-han-recolectado-200-mil-firmas-para-la-revocatoria-de-penalosa/)

No obstante, frente a los motivos expuestos tanto por sindicatos como por senadores y ciudadanos, para impedir la venta de este patrimonio público, Peñalosa señaló que la venta de la ETB financiará la construcción de 6 nuevos hospitales, 8 jardines infantiles, 16 centros de discapacidad y dos centros culturales afirmando que su **“prioridad es la inversión social sobre la empresarial”.**

Sin embargo, de acuerdo con la concejala Gloria Stella Díaz, ni durante la campaña “ni en el plan de gobierno se habían especificado estas propuestas que **ahora se quieren pasar y que carecen de estudios.**” Le puede interesar: ["Acción de nulidad de Sintrateléfonos podría frenar venta de la ETB"](https://archivo.contagioradio.com/accion-de-nulidad-de-sintratelefonos-podria-frenar-venta-de-la-etb/)

De otro lado, los asistentes al cabildo, denunciaron que de nuevo los funcionarios de la alcaldía llenaron tres cuartos del salón e impidieron que la ciudadanía ingresara, mientras que Wilches informó que **“obliagaron trabajadores y amenazándolos con su estabilidad laboral para ir al cabildo”.**

Los sindicatos expresaron que el próximo paso es optar por las vías jurídicas y demandar la decisión de Peñalosa, a su vez, indicaron que continúan evaluando la posibilidad de **una consulta popular y una movilización.**

### **Así transcurrió el Cabildo Abierto** 

El cabildo abierto, que tanto sindicatos como ciudadanos habían propuesto como escenario de dialogo y conversación para evitar la venta de la ETB, **terminó en un espacio de confrontación entre quienes se oponían y quienes no a la decisión del alcalde.**

En varias ocasiones el moderador del cabildo se vio obligado a amenazar con levantar la sesión debido a los gritos y al **ánimo alterado de los asistentes que impedía la participación de los ponentes.**

Entre los hechos que más generaron controversia durante el cabildo, está [**el escupitajo que **]**uno de los asistentes al cabildo le lanzó al concejal Lucía Bastidas**, después de su intervención y la intervención de uno de los integrantes del comité Revoquemos a Peñalosa, durante las ponencias, que le quito el micrófono a Jorge Castellanos para hablar sobre los motivos de la revocatoria. La persona fue retirada del salón por la Policía.

<iframe id="audio_17413440" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_17413440_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
