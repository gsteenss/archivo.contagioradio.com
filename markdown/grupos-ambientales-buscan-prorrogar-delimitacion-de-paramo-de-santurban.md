Title: Grupos ambientales buscan prorrogar delimitación del Páramo de Santurbán
Date: 2019-05-27 15:47
Author: CtgAdm
Category: Ambiente, Nacional
Tags: Comité para la Defensa del Agua y el Páramo de Santurbán, delimitación de páramos, minería ancestral, Páramo de Santurbán
Slug: grupos-ambientales-buscan-prorrogar-delimitacion-de-paramo-de-santurban
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/Captura-de-pantalla-115.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Twitter Jorge Florez] 

El Ministerio de Ambiente tiene plazo **hasta el 16 de julio para definir la nueva delimitación del Páramo de Santurbán. S**in embargo, grupos ambientales pidieron ayer en una audiencia pública con esta entidad, que la fecha sea prorrogada hasta que se obtenga información veraz sobre cuáles podrían ser los efectos que tenga la delimitación de ese ecosistema sobre el agua de la ciudadanía de Bucaramanga.

"[Nosotros insistimos que cualquier decisión que se tome sobre el Páramo de Santurbán, sin los estudios técnicos necesarios, va ha ser insuficiente. Estamos pidiéndole al Ministerio de Ambiente que haga los estudios isotópicos e hidrológicos para saber el flujo de las aguas subterráneas, y sobre esos estudios, tomar cualquier decisión que no afecte el agua de Bucaramanaga”, afirmó Mayerly López, vocera del Comité para la Defensa del Agua y el Páramo de Santurbán.]

### **Los enredos de la delimitación del Páramo de Santurban** 

La delimitación que propone el Ministerio de Ambiente no incluye el subpáramo y el bosque alto-andino, a pesar de que estos ecosistemas trabajan en conjunto con el páramo para generar agua y hábitats de rica diversidad biológica. En ese sentido, la ambientalista señaló que en cambio de proteger los recursos naturales, esta propuesta favorecería a la empresa Minesa para el desarrollo de una mina de oro, de 780 metros de profundidad y a solo unos metros de la línea de delimitación. (Le puede interesar: "'[No se puede fraccionar el Páramo de Santurbán': Comité para la Defensa](https://archivo.contagioradio.com/no-se-puede-fraccionar-paramo-santurban-comite-la-defensa/)")

Según la vocera, desde que inició la segunda delimitación del páramo, los estudios necesarios se realizaron en la parte de este ecosistema que se encuentra ubicado en el departamento del Norte de Santander. No obstante, ninguno de ellos se ha replicado en la zona que se encuentra en el departamento del Santander. Asimismo aseguró que la realización de los mismos podrían tardar un año y medio en ser completados.

### **Los mineros ancestrales y agricultores del Páramo** 

Un punto de contención es el tema de la inclusión de los mineros ancestrales y agricultores que han vivido dentro de la provincia Soto Norte, en el Páramo de Santurbán, por más de 400 años. Al respecto, el Comité indicó que no está en contra de los proyectos de sustento que realizan estas poblaciones sino a la minería a gran escala, como lo ha planteado Minesa y el Gobierno nacional.

"Nosotros en ningún momento hemos planteado el desplazamiento de estas comunidades", sostuvo López. Al contrario, indicó que proponen al Gobierno realizar una zonificación de predio a predio para establecer áreas de protección y al mismo tiempo, proteger el sustento de las comunidades. Si bien estas actividades generan impactos nocivos para los ecosistemas, López afirmó que se pueden mitigar con una modernización tecnológico.

Finalmente, la ambientalista manifestó que esperan que el Ministro de Ambiente Ricardo Lozano, quien estuvo presente en la audiencia, tome en cuenta las demandas de las comunidades al proceder a la siguiente fase de la delimitación que es la concertación.

<iframe id="audio_36374733" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_36374733_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
