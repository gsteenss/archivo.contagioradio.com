Title: Más de 1.500 estudiantes se dieron cita en Cauca para hablar de educación
Date: 2019-03-18 16:00
Author: CtgAdm
Category: Educación, Movilización
Tags: Cauca, ENEES, estudiantes, UNEES
Slug: estudiantes-cauca-educacion
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/03/Captura-de-pantalla-2019-03-18-a-las-3.25.49-p.m..png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: @UneesCol] 

###### [18 Mar 2019] 

Durante este fin de semana se desarrolló el tercer **Encuentro Nacional de Estudiantes de Educación Superior (ENEES)** con el que los jóvenes reanudarían la dinámica del movimiento, y trazarían la ruta de acción para seguir construyendo una educación superior mejor digna. Aunque el encuentro no estuvo exento de inconvenientes para la llegada de las delegaciones, **contó con la participación de cerca de 1.500 estudiantes**.

Julián Baez, vocero nacional de la Unión Nacional de Estudiantes de Educación Superior (UNEES), explica que para la llegada a la Universidad del Cauca (lugar en el que se realizaría el encuentro) **tuvieron problemas por parte de la Fuerza Pública y no de los bloqueos que se realizan en el marco de la Minga por la Vida**; "fue la retención por parte de la Policía y el Ejército la que no dejó pasar una caravana" que intentaba llegar a Popayán, detalla Baez.

La retención provocó la división de algunas delegaciones, que tuvieron que tomar rutas diferentes para llegar a la Capital de Cauca, mientras otras fueron abandonadas en medio del camino, ante la negativa de conductores y empresas prestadoras del servicio de transporte de cambiar de ruta para garantizar la seguridad de los estudiantes. (Le puede interesar: ["A la Minga indígena llegaron primero los estudiantes que Duque"](https://archivo.contagioradio.com/minga-indigena-estudiantes-duque/))

Estas situaciones provocaron un retraso en el desarrollo del ENEES, al que se sumó un enfrentamiento entre encapuchados y el Escuadrón Móvil Anti Disturbios (ESMAD), que se produjo el sábado en la UniCauca, mientras los estudiantes discutían la organización del movimiento. Esta situación fue denunciaba por los mismos integrante de la UNEES, quienes estaban preocupados por una intervención de la Fuerza Pública en la Universidad.

### **Con los movimientos sociales por la educación** 

Entre las discusiones que se dieron durante el evento, el integrante de la UNEES señala que avanzaron en propuestas para lograr la materialización de los acuerdos alcanzados con el Gobierno Nacional, decidieron que construirían un nuevo modelo de educación que les permita tener autonomía y democracia universitaria; y aseguraron que estarían apoyando todos los encuentros de movimientos sociales, **así como que garantizaron su participación en dichos espacios.**

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
