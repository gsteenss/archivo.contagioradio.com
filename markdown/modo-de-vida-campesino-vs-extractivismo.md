Title: Modo de vida campesino Vs. extractivismo
Date: 2020-06-19 10:19
Author: Censat
Category: Censat Agua Viva - Amigos de la Tierra Colombia, Opinion
Tags: campesinos, CENSAT, Covid-19, crisis, extractivismo
Slug: modo-de-vida-campesino-vs-extractivismo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/06/WhatsApp-Image-2020-06-18-at-9.25.13-AM.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:heading {"align":"right","level":6,"textColor":"cyan-bluish-gray"} -->

###### Foto de: Censat Agua Viva - Amigos de la Tierra Colombia {#foto-de-censat-agua-viva---amigos-de-la-tierra-colombia .has-cyan-bluish-gray-color .has-text-color .has-text-align-right}

<!-- /wp:heading -->

<!-- wp:paragraph -->

Los territorios y comunidades que mantienen activa una vida campesina, son mucho más resistentes a la crisis, que aquellos donde prima el extractivismo. Esto lo hemos visto en los múltiples territorios que se han alimentado y sostenido su vida digna gracias a su relación armónica con la tierra y su modo de vida campesino, aún cuando en Colombia estas comunidades enfrentan y han enfrentado históricamente múltiples dificultades.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":5} -->

##### No obstante, los estados no sólo desatienden las necesidades (exacerbadas por la crisis) de alimentación, salud, educación, economía, servicios públicos, vivienda y empleo de millones de personas, sino que favorecen empresas extractivas, que a su vez desdibujan la vida.

<!-- /wp:heading -->

<!-- wp:paragraph -->

Esto se hace evidente en los departamentos del Caribe Colombiano, donde la extracción de carbón a gran escala por más de tres décadas ha generado dependencia económica y social, graves impactos ambientales y a la salud aumentando su vulnerabilidad, pues reduce su acceso al agua y a la producción alimentaria. Estas condiciones exacerban la crisis que vive la región desde hace varios años, a la que el Estado no ha podido dar respuesta, y la empresa privada no ha querido asumir los daños históricos que ha causado.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En contraste, hay comunidades que han podido alimentarse y recrear su vida y cultura gracias al sostenimiento de sus prácticas campesinas y de producción alimentaria. Los territorios que mantienen sistemas productivos y culturales tienen el alimento cerca, no son víctimas de la especulación, no se arriesgan al contagio por consumir productos con cadenas de mercado demasiado extensas y tienen dietas ricas en sabores y nutrientes. Además, el campesinado comparte la semilla y los saberes de la tierra, manifestando su solidaridad al compartir parte de su producción con quienes no tienen.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Aunque la alimentación es fundamental para garantizar el derecho a la vida, y que nuestra mayor fuente de alimento es el agro colombiano, el gobierno nacional prefirió reactivar la economía favoreciendo actividades mineras y extractivas. Por un lado, se han impulsado decretos de emergencia como el **574** y el **766**, que reducen los costos que deben asumir las empresas mineras para la explotación del subsuelo, y amplían los plazos para el pago de impuestos relacionados con la exploración. Por otro lado, se ofrecen garantías a las empresas mineras, como minimizar los trámites en los procesos de licenciamiento (decreto 766). Además, el decreto 440 promueve audiencias ambientales de forma virtual en zonas donde la población tiene poco acceso a internet, con lo que se vulnera el derecho a la participación.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Adicionalmente, en Colombia desde hace años, la economía campesina enfrenta una crisis a causa de políticas y medidas que fomentan la agroindustria y privilegian la importación de alimentos por encima de la producción nacional, campesina y familiar.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Los decretos de emergencia nacional favorecen la importación y la reducción de aranceles de productos como el trigo, la soya y el sorgo. Además, las subvenciones de **FINAGRO** que buscaban favorecer el campo, fueron acaparadas por grandes empresarios que, según la Contraloría, a abril de 2020 son el 90% de los beneficiarios de estos créditos. Todo esto no fortalece las propuestas y acciones de políticas públicas que viene impulsando la Red Nacional de Agricultura Familiar -RENAF-, las cuales promocionan la comercialización de la producción de la economía campesina, familiar y comunitaria.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":5} -->

##### Aunque se ha querido impulsar la imagen de Colombia como un país minero, el modo de vida y producción campesina sigue siendo la opción para recrear una vida más digna y justa.

<!-- /wp:heading -->

<!-- wp:paragraph -->

La economía familiar y la agroecología, fomentan la autonomía económica, alimentaria, garantizan la reproducción de la vida y de las diversas culturas, lo que implica asegurar la alimentación familiar por largos períodos más allá de la dependencia a un salario, sosteniendo una vida digna y una relación armónica con el territorio.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En Colombia hay comunidades que tienen claro que la producción agropecuaria y sus formas de vida son más importantes que los proyectos extractivos; este es el caso de Cajamarca, Fusagasugá, el Suroeste de Antioquia, el Cauca, entre otros territorios donde las comunidades a través de mecanismos de participación ciudadana, institucionales y autónomos (por ejemplo: consulta popular, consulta autónoma o legítima, mandatos populares, acuerdos municipales, etc.), decidieron decir no a la minería y la explotación petrolera para privilegiar la vida campesina.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El momento actual nos obliga a re-pensar nuestra relación con la vida, y nos hace entender la importancia de la vida campesina por las formas en que produce, sostiene, cuida y concibe el ambiente, el alimento, el agua, el suelo, el trabajo y la solidaridad. Garantizar la existencia digna de la vida campesina implica un reconocimiento y respeto por los territorios que le sustentan, lo cual no es compatible con el desarrollo promovido por el actual gobierno y las corporaciones que impulsan el extractivismo, proyectos que generan graves impactos en el suelo, las aguas y el aire, la vida humana y no humana.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":6} -->

###### Le puede interesar: ["La crisis no viene de la Tierra, sino del modelo de habitarla"](https://archivo.contagioradio.com/la-crisis-no-viene-de-la-tierra-sino-del-modelo-de-habitarla/)

<!-- /wp:heading -->

<!-- wp:paragraph -->

En todo el país las comunidades locales, rurales y urbanas, podemos vivir sin la operación de proyectos extractivos, pero no sin la vida campesina y sus aportes.

<!-- /wp:paragraph -->
