Title: 595 personas se gradúan de diplomado que busca fortalecer la lucha contra la minería
Date: 2017-06-25 18:44
Category: Ambiente, Nacional
Tags: Mineria
Slug: 415_personas_graduan_diplomado_tolima
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/06/DDClfBRWsAA7Iyx.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Comité Ambiental en Defensa de la Vida 

###### [25 Jun 2017]

En el marco de la lucha y resistencia frente a la minería en Tolima, esta semana se graduaron 595 personas del Diplomado en Gestión Ambiental y Territorial. **Un espacio de formación del Comité Ambiental en Defensa de la Vida** y que se suma a una serie de actividades que desarrollan para fortalecer a las personas y comunidades que se oponen al extractivismo.

El objetivo del diplomado es cualificar al movimiento ambiental del departamento de manera que los diferentes **la comunidad fortalezca sus procesos de resistencia y defensa del agua, la vida y el territorio.**

Se trata de un diplomado dictado en **la Universidad del Tolima** que contó con la participación de lideres en materia de defensa del territorio, modelos de desarrollo extractivos entre otros.

El curso contó con el apoyo de instituciones y organizaciones como el Observatorio Ambiental de la Facultad de Ciencias de la Universidad del Tolima, La Biblioteca Darío Echandía, la Corporación SOS Ambiental, la Fundación Rosa Luxemburg y el Grupo Semillas, un apoyo que permitió que el diplomado fuera completamente gratuito.

\

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
