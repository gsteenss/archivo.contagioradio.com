Title: Productores locales invitados a participar en el 3er Festival Internacional de Cine por los Derechos Humanos
Date: 2015-12-09 17:30
Author: AdminContagio
Category: 24 Cuadros, DDHH
Tags: 3er Festival Internacional de Cine por los DDHH, Convocatorias cine Derechos humanos, Diana Arias
Slug: productores-locales-invitados-a-participar-en-el-3er-festival-internacional-de-cine-por-los-derechos-humanos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/12/convocatoria.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

#####  Foto: 

<iframe src="http://www.ivoox.com/player_ek_9647273_2_1.html?data=mpuhmZebd46ZmKialJmJd6Kml5KSmaiRdo6ZmKiakpKJe6ShkZKSmaiRh9Di19TQw9nTtsrVjMbPy8rWuMKf0cbfw5DJsIynxteYqMrXuMrqwtGYq9PYqdPiwsjW0dPFsI6ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Diana Arias, Director Festival Internacional de Cine por los DDHH] 

##### 9 Dic 2015 

Hasta el próximo 20 de diciembre se encuentra abierta la convocatoria para la 3ra edición del Festival Internacional de Cine por los Derechos Humanos de Bogotá, principal vitrina de exhibición y difusión de la producción audiovisual enfocada en la promoción de los Derechos Humanos en Colombia y el mundo.

Tras la buena acogida durante la[Segunda edición del Festival](https://archivo.contagioradio.com/hoy-es-el-lanzamiento-del-festival-de-cine-y-derechos-humanos-de-bogota/)  Diana Arias, Productora de cine y directora del evento, hace un balance muy positivo al cumplir el objetivo de "llegar a través del cine como herramienta de sensibilización a diversos públicos desde niños hasta personas de la tercera edad" tanto en la semana del Festival como en la[circulación](https://archivo.contagioradio.com/comienza-circulacion-del-2-festival-de-cine-por-los-derechos-humanos/) posterior donde se conquistaron nuevos espacios y audiencias.

Además de ser el espacio ideal para que los nuevos realizadores den a conocer su trabajo y evitar que se queden sus propuestas archivadas por la falta de alternativas de circulación, el Festival ha servido para resaltar el papel de la cultura, en particular del cine, en la construcción de un país en paz, recurso vital de cara al tan anhelado postconflicto en Colombia.

Las categorías en competencia para la edición 2016, premiarán al mejor Largometraje sobre Derechos Humanos, Cortometraje nacional, Cortometraje internacional, Documental nacional, Documental internacional y gracias a la significativa recepción de cortometrajes animados, la organización decidió abrir una categoría propia para esta 3ra entrega.

**¿Como participar?**

Los creadores interesados e interesadas en participar con sus trabajos pueden encontrar [Aquí](http://www.festivaldecineddhhbogota.com/convocatoria-abierta-2016/) todas las bases y condiciones a cumplir, para luego realizar su inscripción diligenciando el [formulario](http://www.festivaldecineddhhbogota.com/formulario-de-inscripcion/)creado para tal fin. También pueden hacerlo a través de las plataformas [FestHome](https://festhome.com/), [Mobiveta](http://festival.movibeta.com/web/controllers/siteController.php?action=11&categoria=12), [UptoFest](http://www.uptofest.com/es/listado_festivales) y [Click For Festival](https://www.clickforfestivals.com/?busqueda=derechos+humanos&x=0&y=0)  en las que encontrarán las instrucciones para el registro de su material.

La selección de las producciones que integrarán la selección oficial, estará en manos de un comité experto, designado por la organización, bajo criterios relacionados con el contenido y la factura de los postulantes entre quienes saldrán los ganadores (Lea también E[stos son los ganadores del 2do Festival de Cine por los Derechos Humanos](https://archivo.contagioradio.com/estos-son-los-ganadores-del-2-festival-de-cine-por-los-derechos-humanos/))

La fecha estipulada por Impulsos Films, productora encargada de la organización, sería del 16 al 21 de mayo del año que viene.

 

 
