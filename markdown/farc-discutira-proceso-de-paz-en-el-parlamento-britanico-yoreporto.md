Title: FARC discutirá proceso de paz en el Parlamento Británico. #YoReporto
Date: 2015-03-05 17:46
Author: CtgAdm
Category: Nacional, Paz
Tags: Conversacioines de paz en Colombia, FARC, IRA, Irlanda, Justice for Colombia, Paralmento Británico, paz en colombia
Slug: farc-discutira-proceso-de-paz-en-el-parlamento-britanico-yoreporto
Status: published

###### Foto: entrehistorias.blogspot.com 

El grupo de Amigos de Colombia del Parlamento británico, el Grupo Parlamentario sobre Conflicto, y la ONG Justicia por Colombia han organizado el evento del martes 10 de marzo, en el cual negociadores del gobierno y de las FARC han sido invitados a discutir el desarrollo del proceso de paz con una audiencia de parlamentarios de varios partidos, expertos académicos, representantes diplomáticos y de ONGs internacionales, y público interesado. Se han invitado a las dos partes. Hasta ahora la delegación de paz de las FARC-EP ha confirmado su participación representada por el jefe del equipo negociador Ivan Márquez.

El evento cuenta con la participación del destacado periodista Seumas Milne, quien en 2014 viajó a  Colombia y Cuba para conocer de cerca la realidad del conflicto y para conocer las posiciones del gobierno y la insurgencia. También participarán ex negociadores y ex combatientes de ambos lados de Irlanda de Norte quienes han estado compartiendo su experiencia de la construcción de paz con las partes del conflicto colombiano desde 2012.

El evento marca un gran paso en el apoyo internacional al proceso de paz. Será la primera vez que la delegación de paz de la guerrilla se dirige a un público británico y será una oportunidad única para que parlamentarios británicos conozcan de cerca las conversaciones  y qué se necesita de la comunidad internacional.

La carta de invitación, enviada por los grupos parlamentarios representando varios partidos políticos a la mesa de diálogo dijo “Algunos de nosotros hemos tenido la oportunidad de conocerlos en persona, y sería un honor ofrecerles este espacio constructivo en el Parlamento.”

El parlamentario y ex soldado británico de Irlanda del Norte Jeffrey Donaldson del partido DUP, quien ha conocido al Presidente Santos y el equipo del gobierno y a la delegación de las FARC dijo: “es importante que la comunidad internacional apoye los esfuerzos de ambos lados para poner fin a uno de los conflictos más largos del mundo”, puntualizando que, “nunca hubiéramos logrado la paz en Irlanda del Norte sin el apoyo internacional imparcial que fue recibido por todos los actores”.

Mariela Kohon, Directora de la ONG Justice for Colombia dijo: “esta es una oportunidad para que los parlamentarios británicos y otras figuras políticas puedan intercambiar con los negociadores de paz y explorar como la comunidad internacional puede seguir apoyando a este importante paso hacia la paz con justicia social en Colombia.”

Por Justicia por Colombia para YoReporto
