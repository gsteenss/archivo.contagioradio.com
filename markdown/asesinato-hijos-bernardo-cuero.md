Title: Denuncian asesinato de hijos del líder Bernardo Cuero
Date: 2018-03-20 13:37
Category: DDHH, Nacional
Tags: Bernardo Cuero, lideres sociales
Slug: asesinato-hijos-bernardo-cuero
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/03/Hijos-bernardo-e1521570653436.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Archivo familiar 

###### 20 Mar 2018 

En un comunicado de la Asociación Nacional de Afrocolombianos Desplazados AFRODES, denuncian el asesinato de los hermanos Silvio Silvio Duban Ortiz Ortiz de 27 años y Javier Bernardo Cuero Ortiz de 32 años de edad, hijastro e hijo respectivamente del líder y fiscal de la asociación Bernardo Cuero Calvo asesinado en junio de 2017.

De acuerdo con la información, los jóvenes fueron asesinados el 19 de marzo cerca de las 2 a.m. en el barrio La Independencia, avenida Ferrocarril con Tres Tablas en el Municipio de Tumaco, por dos hombres en motos quienes dispararon contra los hermanos sin mediar palabra quienes murieron al instante. En los hechos, donde también se encontraba parte de la familia, la esposa de Javier resulto herida por un impacto de bala en su hombro izquierdo.

Marino Córdoba, presidente de la organización, asegura que otro de los hijos de Bernardo también ha sido objeto de serias amenazas "hemos solicitado al gobierno nacional las medidas de seguridad incluso para sacarlo de donde se encuentra en este momento para que no vaya a ser asesinado" y ninguno de los otros miembros de la familia del líder.

Informa AFRODES que este nuevo asesinato se presenta doce días después de realizada la primera audiencia pública por el asesinato de Cuero Calvo en Barranquilla, durante la cual algunos integrantes de la organización junto con amigos, integrantes de otras organizaciones y familiares, realizaron un plantón en exigencia de justicia en su caso.(Le puede interesar: [Bernardo Cuero, líder de AFRODES asesinado, llevaba 2 años y medio solicitando medidas de protección](https://archivo.contagioradio.com/41928/))

"Nos deja una gran preocupación, no sabemos lo que esta pasando, que hay detrás de todo esto" asegura Córdoba en cuanto a las razones de la persecución contra la familia de Bernardo, "con la muerte de los dos jóvenes nos deja como que debemos movernos rápidamente para ver que hacemos para salvar su vida" refiriéndose puntualmente al otro hijo amenazado.

Desde la organización, envían una mensaje a las autoridades para que garanticen la seguridad de los familiares del líder ante las múltiples amenazas y estado de vulnerabilidad. Adicionalmente piden a al Fiscalía general y al gobierno celeridad en las investigaciones por la muerte de Bernardo, sus hijos y también de otros líderes y defensores de DDHH en el país.

De igual forma extienden su llamado a la comunidad internacional a brindar mayor solidaridad a los líderes, defensores y sus familias a quienes a pesar de los acuerdos la paz no les llega, y a que se exija al gobierno nacional condiciones y garantías para la defensa de la vida y el liderazgo comunitario.

**Sobre el caso de Bernardo Cuero**

La audiencia inicial programada para el mes de febrero fue cancelada, y finalmente se realizó el 6 de marzo en Barranquilla, un proceso inicial donde no hubo mucho avance según AFRODES, quienes esperan que se programe una segunda audiencia donde los abogados puedan exponer sus argumentos.

"Tenemos serias preocupaciones por la lentitud con la que se esta avanzando en este proceso porque ademas nos han dicho que hay muchos intereses para no conocerse la verdad del asesinato de Bernardo" manifiesta el Presidente de la Asociación.

"Seguimos exigiendo a la justicia en que hay que avanzar con el proceso y hay que esclarecer los hechos no tanto el autor material pero si los autores intelectuales del mismo que llevaron al asesinato  de  Bernardo" insistiendo en que, contrario a las posiciones del gobierno, el crimen si guarda relación con el activismo del líder y que existió negligencia por parte del gobierno nacional.

[Asesinados Hijos de Bernardo Cuero](https://www.scribd.com/document/374440641/Asesinados-Hijos-de-Bernardo-Cuero#from_embed "View Asesinados Hijos de Bernardo Cuero on Scribd") by [Contagioradio](https://www.scribd.com/user/276652802/Contagioradio#from_embed "View Contagioradio's profile on Scribd") on Scribd

<iframe id="doc_52582" class="scribd_iframe_embed" title="Asesinados Hijos de Bernardo Cuero" src="https://www.scribd.com/embeds/374440641/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-XsEUCATJKNPWCbXe749P&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="0.7080062794348508"></iframe><iframe id="audio_24722573" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_24722573_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
