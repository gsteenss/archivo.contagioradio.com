Title: En Guapi somos actores activos de resistencia a la violencia: CocoCauca
Date: 2019-10-25 11:58
Author: CtgAdm
Category: DDHH, Nacional
Tags: BastaYaConTantaGuerra, Cauca, Guapi, violencia
Slug: guapi-dice-no-mas-a-la-violencia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/masgorgona-presente-840x560.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

 

###### [Foto:]@CococaucaOrg 

Pobladores del municipio de Guapi han hecho público el miedo que sienten por los continuos enfrentamientos entre grupos armados que disputan su territorio, y que han provocado la muerte de 14 personas, “cada uno de ellos tenía una familia y sueños, pero los impulsores de la guerra nos cegaron sus vidas”, expresa la organización CocoCauca en un comunicado junto con los nombres de las víctimas. (Le puede interesar: [Bombardeos de FFMM ya deja 500 desplazados en Guapi, Cauca](https://archivo.contagioradio.com/bombardeos-de-ffmm-ya-deja-500-desplazados-en-guapi-cauca/))

También han hecho notoria su preocupación frente a la decisión de la alcaldía de Guapi al decretar el pasado lunes 2 de Octubre un "toque de queda" de 9 pm a 5 am hasta el 30 de octubre, cuando según ellos desde hace un tiempo los actores armados ya lo habían impuesto por medio de terror, así mismo señalaron que el aumentar la fuerza militar en el territorio no es la solución.

> [\#Guapi](https://twitter.com/hashtag/Guapi?src=hash&ref_src=twsrc%5Etfw) [\#Cauca](https://twitter.com/hashtag/Cauca?src=hash&ref_src=twsrc%5Etfw): Compartimos este comunicado frente a la situación actual, favor compartir: "Enfrentamientos armados dejan varios muertos en Guapi - Administración decreta "toque de queda"![\#LaPazTambiénEsNuestra](https://twitter.com/hashtag/LaPazTambi%C3%A9nEsNuestra?src=hash&ref_src=twsrc%5Etfw) [\#BastaYaConTantaGuerra](https://twitter.com/hashtag/BastaYaConTantaGuerra?src=hash&ref_src=twsrc%5Etfw) [\#LaPazEsDeTodos](https://twitter.com/hashtag/LaPazEsDeTodos?src=hash&ref_src=twsrc%5Etfw)<https://t.co/UIcznM2OnP>
>
> — Cococauca (@CococaucaOrg) [October 23, 2019](https://twitter.com/CococaucaOrg/status/1187010403051675648?ref_src=twsrc%5Etfw)

<p>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
</p>
Frente a esto Orlando Pantoja, palenquero y Mayor de la Organización CocoCauca  aseguró que, "nosotros nos preguntamos entonces que control de fuerza pública hay, porque por todos lados vemos presencia del ejército y aún así los índices de violencia son cada vez más altos", y agregó que desde el día que los líderes de la disidencia de FARC Santrich y Márquez, comunicaron la retoma en armas , el pueblo se llenó de grafittis que decian alusivos al ELN.

### **Guapi un municipio sumido en el olvido estatal **

Este es un municipio conformado en un 95% de comunidad negra, está ubicado en el centro del pacífico sur que lo hace un punto estratégico para turistas por el mar pacífico y la espesa selva que lo rodea, pero también lo hace blanco de enfrentamientos entre grupos armados que desean obtener el control territorial. (Le puede interesar:[¿Qué está pasando en Cauca? análisis del líder campesino Óscar Salazar](https://archivo.contagioradio.com/que-esta-pasando-en-el-cauca-un-analisis-del-lider-campesino-oscar-salazar/))

Para Pantoja el olvido que se ve por parte del Estado, en temas de salud, educación y garantías de la paz en su territorio se debe a temas históricos que van más allá del actual gobierno, “las comunidades negras a lo largo de los años hemos sido denigradas, discriminadas y olvidadas por el estado colombiano, y nos hemos visto forzados a vivir en la marginalidad, y teniendo como motor nuestro ánimo de salir adelante”, afirmó el líder.

Los pobladores de Guapi lamentan los hechos de violencia y las vidas perdidas, pero también reconocen que este no es motivo para silenciar sino al contrario es el motor que los alienta a reclamar su territorio, “lo que hicimos y seguimos haciendo es trabajar el tema de la sensibilización para que seamos actores activos de resistencia frente a cualquier tipo de violencia”, agregó Pantoja.

Por último la comunidad hacen un llamado a la Fiscalía, al Gobierno, al Defensor del Pueblo, al Alcalde del Municipio, y especialmente a los actores armados presentes en tu territorio, “frente 30 de las Farc – EP, Frente Jaime Martínez, ELN y Fuerzas Armadas a que respeten la vida  y los derechos humanos de los habitantes de Guapi”, exigieron en un comunicado emitido por Cococauca.

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe><iframe id="audio_44015756" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_44015756_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
