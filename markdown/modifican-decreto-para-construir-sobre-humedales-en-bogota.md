Title: Modifican decreto para construir sobre humedales en Bogotá
Date: 2017-10-30 17:18
Category: Ambiente, Nacional
Tags: Corredores Ambientales, Humedales, Peñalosa
Slug: modifican-decreto-para-construir-sobre-humedales-en-bogota
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/02/Torcahumedal.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: UN medios] 

###### [30 Oct 2017] 

Organizaciones ambientalistas están denunciando la firma de un decreto por parte del alcalde Enrique Peñalosa y la Secretaría de Ambiente **que permitiría que se construyeran ciclorutas, puentes o alamedas en los humedales de la capital**, sin contar con la participación de organizaciones o la ciudadanía para analizar los efectos de esta acción.

De acuerdo con Daniel Bernal, el proceso con el que se aprobó este decreto fue bastante “pobre” debido a que no existió un debate con la Mesa de Humedales, escenario válidos para la consulta a los ciudadanos, **ni las organizaciones defensoras del medio ambiente, además el decreto modificaría la política de humedales de Bogotá**.

“Ni siquiera la Mesa Distrital de Humedales, espacio diseñado y acordado entre el Distrito y la sociedad civil para manejar estos temas estuvo en los debates, entonces no hubo una participación ciudadana y **para ellos conveniente no tenerla porque estaríamos en desacuerdo**” afirmó Bernal.

Los humedales más afectados por este decreto serían los que hacen parte del proyecto corredores ambientales 2016-2021, en donde se encontrarían humedales como **el Juan Amarillo, El Jaboque, el Humedal Córdoba, la Conejera Salitrosa y en Guaimaral.**

En esa medida los ambientalistas han señalado que construir ciclorutas o puentes afecta el entendido de un corredor ambiental y debilita las cadenas de fauna y flora que habitan en estos lugares. (Le pude interesar:["Humedales Tibanica, El Burro y Jaboque, están en riesgo de desaparecer"](https://archivo.contagioradio.com/humedales-de-bogota-tibanica-el-burro-y-jacobe-en-riesgo-de-desaparicion/))

Bernal afirmó que esta idea de intervenir humedales ya había sido puesta en marcha por Peñalosa en lugares como el Juan Amarillo, “este era un humedal completamente natural y lo convirtió en una piscina de cemento, una inversión multimillonaria y colocó ciclorutas por todo el borde, el resultado, **15 años después, un humedal en el mismo estado de deterioro y peligroso para transitar”** Además señaló que las características ecológicas se perdieron.

Otro ejemplo del deterioro que provocaría estas obras, es el Humedal Jaboque, rodeado por alamedas y ciclorutas, en ese sentido ambientalistas han señalado que las intervenciones son importantes, pero con un carácter ambiental y no urbanístico como lo ha impulsado la administración. (Le puede interesar: ["Aprueban consulta popular que define destino de la Macarena, Meta"](https://archivo.contagioradio.com/la_macarena_consulta_popular/))

Mañana diversas organizaciones se darán cita para exigir al Distrito respuestas frente al decreto expedido por la Alcaldía y se estaría mirando la posibilidad de instaurar una demanda contra el decreto en conjunto con acciones más pedagógicas.

<iframe id="audio_21778998" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_21778998_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
