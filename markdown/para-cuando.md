Title: ¿Para cuándo?
Date: 2017-08-29 11:52
Category: Columnistas invitados, Opinion
Tags: Equidad de género, feminismo
Slug: para-cuando
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/08/pregunta.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

#### **Por: Ángela Galvis Ardila.** 

###### 29 Ago 2017 

Desde muy niños empezamos a hacer preguntas a los demás y desde ahí jamás dejamos de hacerlas. ¿Por qué? ¿Dónde? ¿Cómo? Son solo algunas de las fórmulas  que empleamos para dar inicio a ese mar de interrogantes con los que bombardeamos a lo largo de la vida a los otros. Pero al llegar a la adultez existen tres preguntas casi que obligadas de formular y recibir: ¿Para cuándo el matrimonio? **¿Para cuándo el bebé?** y **¿Para cuándo el hermanito?** Aquí el orden de los factores sí altera el resultado y, por tanto, una va seguida de la otra, solo es cuestión de tiempo y espera.

Vivimos en una sociedad controladora, que nos ha sometido a un esquema de vida predeterminado y todo lo que se salga de allí o es contra natura, o es inmoral, o está mal visto. Nos volvimos marionetas de un sistema que nos fue absorbiendo como un remolino y, por eso, esas tres absurdas preguntas suelen colarse en cualquier conversación, como si estuvieran precedidas de una flecha invisible que conducen al deber ser de la vida.

[¿Para cuándo el matrimonio? Es una pregunta que tiene un abanico enorme de respuestas según a quien se le formule. Pero la respuesta esperada siempre será si no el lugar y la hora, al menos una fecha que nos brinde algo de confiabilidad, porque en ese universo de metas que nos hemos dejado imponer, el matrimonio se ha vuelto una más, un sinónimo de éxito del amor, y entre más lujosa la fiesta, más lejos se siente el fracaso, así tan solo esté a la vuelta de la esquina. Y entonces, como grandes señores dueños de la verdad, miramos con desconfianza a quienes no quieren o no quisieron casarse jamás; a quienes no han creído en instituciones transmitidas durante años por religiones en las que no confían; a quienes la soledad fue su opción de vida. En fin, a quienes su respuesta frente a esta obligatoria pregunta será un lacónico pero muy bien dado “para nunca”.]

¿Para cuándo el bebé? Como dije, esta viene casi que obligatoriamente después, porque reproducirnos también se nos convirtió en un mandato social, en una especie de deber con la humanidad, como si viniéramos a este mundo con un destino común: ser padres. Pasamos por alto que decisiones de esta índole son del resorte íntimo de cada quien, y que la persona que decida vivir tan maravillosa experiencia debe responder a su deseo de serlo, a eso y a nada más. Pero nuevamente desde nuestra mirada de superioridad, vemos como una especie de subnormales y egoístas a quienes ejerciendo su derecho a elegir han decidido no traer más gente a este mundo. Yo veo más egoísmo, muchísimo más, en quienes traen hijos con fines deshonestos, que en últimas es cualquiera distinto al deseo genuino de dar vida a otro ser humano.

**¿Para cuándo el hermanito?** Esta puede ser la pregunta del deber ser por excelencia,  porque soterradamente nos han venido convenciendo de que así funciona la vida, y que no solo debemos dejar huella con un hijo sino con mínimo dos, porque es necesario traer a este mundo a otro ser a brindarle compañía a ese primer hijo para que no se quede solo. Nace ya con un trabajo adjudicado, con un INRI: ser compañía de su hermano mayor, lo cual, con las trampas que a veces suele poner la vida, pueda ser que no se cumpla. Y no quiero decir que tener hermanos no sea una bonita experiencia de amor, solo que tenerlos no garantiza la fórmula de la felicidad que nos prometen ni las verdades de las que nos creemos poseedores. Tener uno, dos, tres o los hijos que se quiera, debe ser siempre una opción guiada por el deseo y la responsabilidad, no por los cánones sociales.

Ojalá llegue el día en que nos despercudamos de tanto prejuicio que nos inocularon desde niños, desde esos niños preguntones que más adelante querrán saber, sin siquiera sonrojarse, cuanta estupidez se les ocurra. Ojalá algún día podamos dejar de señalar con ese dedo inquisidor y dejemos de hacer preguntas basadas en imposiciones y desaprender cuanta basura hemos ido aprendiendo en este camino que llamamos vida.

#### **Leer el [Fantasma en el espejo](https://archivo.contagioradio.com/el-fantasma-en-el-espejo/) de  Ángela Galvis Ardila** 

------------------------------------------------------------------------

###### Las opiniones expresadas en este artículo son de exclusiva responsabilidad del autor y no necesariamente representan la opinión de Contagio Radio.

<div class="osd-sms-wrapper">

</div>
