Title: Duro reclamo de las FARC al gobierno por incumplimientos al acuerdo
Date: 2017-02-21 17:58
Category: Nacional, Paz
Tags: FARC-EP, Implementación de Acuerdos
Slug: duro-reclamo-de-las-farc-al-gobierno-por-incumplimientos-al-acuerdo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/09/unnamed-1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio ] 

###### [21 Feb 2017] 

El Estado Mayor de las FARC señaló que **es necesario que se active el componente internacional para la implementación del acuerdo, la activación de la segunda misión de la ONU e insistieron en la necesidad de la “recalendarización”** de la dejación de armas debido los incumplimientos en adecuación de las ZVTN, el paramilitarismo y los ataques a la implementación de los acuerdos en el congreso.

El reclamo del Estado Mayor se dio este 21 de Febrero en una carta dirigida a a Jean Arnault, secretario general de la misión de la ONU en Colombia. En la comunicación el Estado Mayor señala que se concluyó el ciclo de ubicación de las FARC-EP en los puntos y zonas verdales de concentración, sin embargo, ninguna está adecuada para alojar a los integrantes de la guerrilla y señalan que **“no es cierto que el 80% de las áreas comunes estén concluidas como lo afirma la Cancillería y la Oficina del Alto Comisionado** para la Paz”

Adicionalmente señaló el estado mayor que ellos **han hecho sus dormitorios y han puesto su mano de obra en la construcción de las áreas comunes** y denunciaron que “**si hay demora en las construcciones no ha sido por falta de voluntad de las FARC**, sino porque los materiales no llegan a tiempo”. Sin embargo resaltaron que en la última etapa han visto “mayor empeño de parte del Gobierno”. Le puede interesar: ["Crece el riesgo para los Acuerdos de Paz"](https://archivo.contagioradio.com/crece-el-riesgo-para-los-acuerdos-de-paz/)

El comunicado plantea una pregunta como elemento central “**¿Si no hay campamento qué protocolos se pueden invocar que no sean los del sentido común?”**  y señalaron como “temerario”  la afirmación por parte del Gobierno que indica que hay muchos hechos violatorios de los protocolos que rigen la dejación de armas.

Señalan también que esas afirmaciones pueden evaluarse como una “acusación a las FARC de estar violando los protocolos” y agregan que se desconoce el esfuerzo para llegar a las zonas veredales a pesar de la **“falta de gerencia e improvisación del gobierno”**. Además recordaron que los **paramilitares se mueven y se extienden “a sus anchas amenazando y asesinando” a dirigentes populares, en las áreas dejadas por las FARC. **Le puede interesar: ["Defensores de derechos humanos contra las cuerdas"](https://archivo.contagioradio.com/disminuyeron-las-amenazas-pero-aumentaron-los-asesinatos-somos-defensores/)

De igual forma, recordaron que **nunca se ha dado una reunión entre la Cancillería, la Oficina del Alto Comisionado para la Paz y el Mecanismo de Monitoreo y Verificación para hacer un balance de estas situaciones**, por lo que no se debería “culpar de las falencias en terreno a las FARC  y a la Misión de Naciones Unidas.

Por otra parte, evidenciaron problemas en torno a la **ubicación de los contenedores para la dejación de armas**, la activación de los protocolos  en condiciones mínimas para su activación y recuerdan que la dejación de armas tiene que ver con la implementación de todo lo acordado y el cumplimiento recíproco de los compromisos.

El texto también señala, como problemático el nivel de discusión del acuerdo en el Congreso  y las repetidas intervenciones de la Ficalía “**¿Si el acuerdo de paz de la Habana, ya fue refrendado por el Congreso en representación del pueblo por qué pretenden   algunos voceros de la institucionalidad modificar el texto de los compromisos?**”.  En ese punto indican que hay un concierto de enemigos de la reconciliación buscando la destrucción de la Jurisdicción Especial para la Paz.

De igual modo denuncian que después de **45 días de aprobada la ley de Amnistía no haya seguridad jurídica para las FARC**, esto sería que se produjera la excarcelación de los guerrilleros y  de los detenidos en razón de la protesta social, para que “los guerrilleros ni nadie, piensen que pueden estar siendo engañados”. Le puede interesar: ["Ley de Amnistía la manzana de la discordia"](https://archivo.contagioradio.com/ley-de-amnistia-la-manzana-de-la-discordia/)

Por todo ello señalan que **debe comenzar la segunda misión de la ONU, la activación inmediata del componente internacional de la implementación**, la participación de las instancias internacionales previstas en el acuerdo y plantean la necesidad de una “recalendarización” del proceso de dejación de armas.

[Consulte la carta completa](https://es.scribd.com/document/339957958/Senor-Jean-Arnault)

###### Reciba toda la información de Contagio Radio en [[su correo]
