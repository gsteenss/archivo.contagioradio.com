Title: Persecución política al movimiento social y al pensamiento crítico
Date: 2016-01-26 09:13
Category: DDHH, Hablemos alguito
Tags: 13 estudiantes, Carlos Pedraza, Criminalización movimientos sociales, miguel angel beltran, Presos políticos en Colombia
Slug: persecucion-politica-y-campana-somos-dignidad-contagio-radio
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/01/persecucion.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### <iframe src="http://www.ivoox.com/player_ek_10191841_2_1.html?data=kpWem5aceJKhhpywj5WVaZS1kZaSlaaXeY6ZmKialJKJe6ShkZKSmaiRdI6ZmKiaqsbGsMbh0NiYo9HLucro0JKYpcbRtMKZpJiSpJbFb7HZ09LO0MrSuMafhpeetdTRs9Sfpc7U0M7IpcWZk5aah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe> 

###### [Hablemos Alguito, Delito Político] 

##### [25 Ene 2016]

Es larga la lista de campesinos, estudiantes, líderes comunitarios, dirigentes políticos y periodístas entre otros, que han vivido bajo la continua amenaza que para su vida, dignidad y buen nombre representa la persecución política al movimiento social y de Derechos Humanos en Colombia.

La continua persecución y criminalización, presente en los más de 200 años de vida republicana del país, ha venido recrudeciendo durante los últimos años del actual gobierno nacional, convirtiendo a reclamantes de tierras, defensores del ambiente y líderes de comunidades indígenas en las mayores [víctimas de amanezas y homicidios](https://archivo.contagioradio.com/54-defensores-de-derechos-humanos-fueron-asesinados-en-colombia-durante-2015/).

Comenzando el año anterior, el movimiento social registraría su primera pérdida con la desaparición y asesinato del líder comunitario [Carlos Pedraza](https://archivo.contagioradio.com/asesinado-defensor-de-derechos-humanos-carlos-pedraza/), integrante del proyecto Nunca Más y del Congreso de los Pueblos, quien según el dictamen de Medicina Legal, murió por impacto de arma de fuego en la cabeza.

Casos como el del profesor [Miguel Angel Beltrán](https://archivo.contagioradio.com/ataques-contra-las-universidades-tienen-fines-politicos-miguel-angel-beltran/), condenado a 8 años de cárcel en diciembre de 2014 y detenido en julio de 2015 , con las mismas pruebas y por los mismos delitos por los que ya había sido absuelto en 2011, demuestran que no existe lugar para el pensamiento crítico y en el caso particular para la libertad de cátedra en el país, situación que no ha sido ajena para la [comunidad académica internacional](https://archivo.contagioradio.com/exclusivo-parlamentaria-alemana-rechaza-detencion-del-profesor-miguel-angel-beltra/).

Gran parte de las capturas ordenadas contra integrantes de los movimientos sociales y populares, han sido catalogadas como [falsos positivos judiciales](https://archivo.contagioradio.com/capturas-a-lideres-del-movimiento-social-serian-falsos-positivos-judiciales/), procesos con trámites ligeros en los que no se brindan garantías a los imputados para su legítimo derecho a la defensa, donde los grandes medios de comunicación toman el rol de juez y parte, como quedó demostrado en el caso de los [13 estudiantes](https://archivo.contagioradio.com/que-sigue-para-los-13-liberados/) señalados de integrar una célula urbana del ELN.

Resultado del agotamiento por estos y los cientos de casos que a diario se registran en todo el país, nace la campaña permanente contra la Persecución al Movimiento Social ¡SOMOS DIGNIDAD!, una iniciativa desde la que se busca denunciar la fuerte represión y persecución contra poblaciones vulnerables del país, principalmente campesinos, indígenas, estudiantes, jóvenes, profesores y presos políticos.

Sebastián Quiroga, vocero de la campaña, compartió en Hablemos Alguito, algunos de los objetivos de la campaña entre los que se encuentran el promover y participar en la construcción de propuestas e iniciativas que permitan avanzar y fortalecer la defensa de los derechos humanos y la participación democrática de todos los sectores sociales.

Junto a Quiroga, nos acompañó Gustavo Pedraza, hermano del fallecido líder social Carlos Pedraza, y como parte del colectivo libertad a los 13, Marco Rodríguez, padre de Andrés Felipe Rodriguez y Juan Hernández hermano de Daniel Hernández,  estudiantes retenidos y[liberados](https://archivo.contagioradio.com/en-libertad-los-13-jovenes-capturados/) el pasado 11 de septiembre.
