Title: Abortar debe ser una decisión libre y segura
Date: 2015-11-11 17:22
Category: Mujer, Nacional
Tags: Aborto legal, Abortos clandestinos en Colombia, Derechos reproductivos en Colombia, Foro ‘Aborto legal en Colombia: Presente y Futuro’, Women’s Link Worldwide’
Slug: abortar-debe-ser-un-decision-libre-y-segura
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/11/Foro-aborto-legal.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Palabra de Mujer 

<iframe src="http://www.ivoox.com/player_ek_9356417_2_1.html?data=mpiimJmVe46ZmKiak5aJd6KpmJKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRhcPj09nO1JDIqcPZjNjS1JDZssKfxcrQy9jNaaSnhqeg0JDQrcPmxpDmjdjJq9bmwpKSmaiRh9Di1cbUy9SPlsLYytSYj4qbh46o&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Mónica Roa, Women’s Link Worldwide’] 

###### [11 Nov 2015] 

[El pasado martes se llevó a cabo el Foro ‘Aborto legal en Colombia: Presente y Futuro’ realizado con el objetivo fundamental de discutir las circunstancias en las que actualmente las mujeres pueden interrumpir de forma legal su embarazo y que tienen que ver con los riesgos que éste tenga para su salud tanto física como mental, **incluyendo la angustia que pueda generarle por la afectación a su proyecto de vida**.  ]

[La abogada Mónica Roa, actual Vicepresidenta de Women’s Link Worldwide’ y panelista del Foro, asegura que **las mujeres tienen derecho a optar por interrumpir su embarazo**, continuarlo o dar su hijo en adopción, en cualquiera de los casos debe tener la posibilidad de conocer a fondo y con claridad las opciones con las que cuenta, el **Estado debe respetar la decisión y otorgar todas las garantías,** incluyendo para la interrupción del embarazo.]

[Según las cifras presentadas por médicos participantes en el Foro **en los últimos 10 años sólo 15.000 abortos han sido practicados legalmente y cientos de miles han sido clandestinamente**, lo que denota que las  mujeres actualmente tienen muy poco conocimiento sobre el derecho que tienen a abortar de manera legal, segura, digna y acompañada contando con el respaldo del Estado.  ]

[De acuerdo con la abogada, resulta interesante abrir el debate sobre la eliminación de los requisitos para abortar en los primeros 3 meses de embarazo; sin embargo, **es necesario que primero la sociedad tenga claridad sobre lo resuelto por la Corte** hace 10 años y que establece las circunstancias en las que hoy en día las mujeres pueden decidir la interrupción de su embarazo como garantía de sus derechos.  ]

[“El país debe comprometerse con la educación sexual” asegura Roa, pues al ser los **derechos reproductivos un asunto de todos y todas**, el aborto debe importar a la sociedad en su conjunto y los medios de comunicación tienen la responsabilidad de cooperar para que las mujeres conozcan sus posibilidades y derechos, **sin confundir la información con las opiniones personales**.]
