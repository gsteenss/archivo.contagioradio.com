Title: Quienes somos
Date: 2018-12-17 10:31
Author: AdminContagio
Slug: quienes-somos
Status: published

QUIENES SOMOS
-------------

**Contagio,** una historia que nace en el año de 1.995, en sus inicios el nombre fue dado a la revista de derechos humanos que tenía una periodicidad semestral, el objetivo principal de esta revista fue el de informar y visibilizar el trabajo de los derechos humanos en Colombia.

Posterior a esto, se comienza a realizar un trabajo de investigación y redacción para el montaje de una radio en internet con enfoque de derechos humanos, que dio vida a esta apuesta comunicativa en el año 2009.

Hoy somos la expresión radial y audiovisual de una apuesta comunicativa multimedia, con enfoque en los DH, que procura la democratización de la información, a través del medio radial, audiovisual y las nuevas tecnologías, permitiendo una mayor libertad de expresión de las comunidades rurales y urbanas de diversos sectores sociales en Colombia y en el mundo. Nuestro trabajo informativo permite el análisis, la interpretación y la contrastación de la información en aras de la construcción de una opinión crítica en una cultura de paz, de respeto de derechos y del ambiente.

Contagio cree en la comunicación que se organiza desde la comunidad, desde aquellas y aquellos a los que en ocasiones no se les da la palabra para opinar, exigir y denunciar, y es por esta razón que realiza trabajos de comunicación con diferentes comunidades en resistencia de Colombia ubicadas en; Cacarica, Curvaradó, Jiguamiandó, (Chocó), Putumayo, Meta, Cauca y Buenaventura, y jóvenes en Bogotá, un trabajo que ha consistido en la realización de talleres en estas regiones del país, contribuyendo al fortalecimiento de competencias para indagar, analizar e intervenir en los canales y los medios de comunicación, con estrategias que mejoren la asertividad en los contextos comunicativos, especialmente en el periodístico. Desde sus realidades como comunidades y sujetos pensantes de una sociedad.

Contagio se vale de las herramientas tecnológicas del internet para la producción y distribución de información durante 24 horas al día, 7 días a la semana, a través de nuestro portal web y las diferentes redes sociales.

Para su autogestión, además de acudir a la gestión y ejecución de proyectos de financiación, Contagio presta los servicios de publicidad, grabación y edición sonora y audiovisual, alquiler de cabina de grabación, realización de cuñas, spots, promos, talleres de formación en comunicación multimedia, asesorías para montaje de radios virtuales, gestión y streaming de eventos públicos.

![](https://archivo.contagioradio.com/wp-content/uploads/2018/04/al-aire-1024x120.gif){width="1024" height="120" sizes="(max-width: 1024px) 100vw, 1024px" srcset="https://archivo.contagioradio.com/wp-content/uploads/2018/04/al-aire-1024x120.gif 1024w, https://archivo.contagioradio.com/wp-content/uploads/2018/04/al-aire-300x35.gif 300w, https://archivo.contagioradio.com/wp-content/uploads/2018/04/al-aire-768x90.gif 768w, https://archivo.contagioradio.com/wp-content/uploads/2018/04/al-aire-370x43.gif 370w"}
