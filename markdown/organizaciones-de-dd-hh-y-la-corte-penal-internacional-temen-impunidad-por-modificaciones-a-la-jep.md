Title: Organizaciones de DD.HH y la Corte Penal Internacional temen impunidad por modificaciones a la JEP
Date: 2018-11-01 16:31
Author: AdminContagio
Category: DDHH, Paz
Tags: Corte Penal Inteernacional, jurisdicción especial para la paz, militares, Senado
Slug: organizaciones-de-dd-hh-y-la-corte-penal-internacional-temen-impunidad-por-modificaciones-a-la-jep
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/04/JEP.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Colombia Informa] 

###### [01 Nov 2018] 

Aunque quedan 7 debates para que la Jurisdicción Especial de Paz tenga 14 magistrados más, que darían un trato diferencial a lo smilitares que se acojan, organismos como la Corte Penal Internacional y organizaciones defensoras de derechos humanos, ya hicieron un llamado pues este tipo de cambios podría abrir la puerta a la impunidad para los crímenes cometidos por agentes estatales.

En las últimas horas, el fiscal adjunto de la CPI, James Stewart, señaló que temen que las modificaciones hechas a este Tribunal, **perjudiquen las investigaciones que ya hay en marcha contra integrantes de la Fuerza Pública por delitos de lesa humanidad,** así lo hizo saber a través de un comunicado fechado este primero de noviembre.

[Declaraciones Fiscal Adjunto CPI](https://www.scribd.com/document/392161966/Declaraciones-Fiscal-Adjunto-CPI#from_embed "View Declaraciones Fiscal Adjunto CPI on Scribd") by [Contagioradio](https://www.scribd.com/user/276652802/Contagioradio#from_embed "View Contagioradio's profile on Scribd") on Scribd

<iframe id="doc_43294" class="scribd_iframe_embed" title="Declaraciones Fiscal Adjunto CPI" src="https://www.scribd.com/embeds/392161966/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-cFNjQBLPJdL1BXhkvdeF&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="0.7729220222793488"></iframe>

### **En qué consiste la modificación propuesta a la JEP** 

<iframe src="https://co.ivoox.com/es/player_ek_29779677_2_1.html?data=k56kmZ6ae5ihhpywj5aVaZS1k5mah5yncZOhhpywj5WRaZi3jpWah5yncaLgytfW0ZC5tsrWxpDg0cfWqYzgwtiYw8vJp9XVxM7c0MrXb8KfzcaYy9PIqdHZz8nS0MjNpYyhhpywj6jTstXVyM7cjbfFqMrjjJKSmaiReA..&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

De acuerdo con el abogado y defensor de derechos humanos Alirio Uribe,  en la Comisión primera del Senado, se aprobó la inclusión de 14 magistrados, dos por cada sala del sistema de la JEP, con una forma de nombramiento diferente a la que ya se hizo, que estaría a cargo de la Consejo Superior de la Judicatura, las presidencias de las Cortes y la Procuraduría.

Hecho que para Alirio Uribe podría traducirse en afectar la independencia y autonomía del Tribunal de Paz, además de ir en contravía de lo pactado en los Acuerdos de Paz de La Habana. (Le puede interesar: ["Entregan a la JEP nuevo informe sobre lo ocurrido durante la Operación Orión"](https://archivo.contagioradio.com/informe-sobre-operacion-orion/))

### **¿Qué hay detrás de esta modificación?** 

<iframe src="https://co.ivoox.com/es/player_ek_29779657_2_1.html?data=k56kmZ6aeZihhpywj5WYaZS1k5Wah5yncZOhhpywj5WRaZi3jpWah5yncarqhqigh6aVsoy3xtXSxsaJdqSf1NTP1MqPsMKfytPQztrXrYa3lIqvldOPqMafkpmYz8bLrdTo08bR0diPpYzgjoqkpZKns8_owszW0ZC2pcXd0JCah5yncZU.&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

Tanto Alirio Uribe, como el senador Iván Cepeda del Polo Democrático, manifestaron que tras esa modificación a la JEP, se encuentran los intereses del Centro Democrático, para evitar que se conozca la verdad frente a los hechos y las acciones cometidas por la Fuerza Pública en el marco del conflicto armado y la cadena de mando.

Además, Cepeda aseveró que este podría ser otro cambio sustancial que se le hace a la JEP, **como el hecho de sacar a los magistrados internacionales del sistema de Justicia y la exclusión de juzgamiento de terceros en el marco del conflicto armado. **

Para Alirio Uribe, si bien esta noticia se recibió con "molestia y peligro para los derechos de las víctimas", sobretodo de crímenes de Estado, podría existir una esperanza que garantice la verdad, justicia, reparación y derecho a la no repetición a quienes han sido víctimas de la Fuerza Pública, si se ejerce una presión por parte del movimiento social a las instituciones políticas.

###### Reciba toda la información de Contagio Radio en [[su correo]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio]{.s1}](http://bit.ly/1ICYhVU)[.]{.s1}
