Title: A un año de la Libertad de Mateo Gutiérrez ¿Qué ha pasado?
Date: 2020-01-28 15:24
Author: AdminContagio
Category: Expreso Libertad, Nacional
Tags: Expreso Libertad, Falsos Positivos Judiciales
Slug: a-un-ano-de-la-libertad-de-mateo-gutierrez-que-ha-pasado
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/01/516904_1.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/01/589962_1.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/01/hggfgjghj.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"justify"} -->

Se cumplen dos años de la libertad del ex prisionero político Mateo Gutiérrez.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

En entrevista para el Expreso Libertad, afirmó que continuará en la defensa de su inocencia y expresó que tras el fallo en derecho, queda por resolver las dudas frente a ¿por qué se montó este falso positivo judicial en su contra? y quiénes están detrás del mismo, desde las instituciones estatales.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Luego de que un juez lo absolviera de los delitos que lo acusaban por supuestamente haber participado de un atentado en el sector conocido como La Macarena, en Bogotá, Gutiérrez salió de la cárcel en noviembre de 2018.  
Sin embargo, la Fiscalía General de la Nación expresó que interpondría una casación en este proceso para llevarlo a la Corte Suprema de Justicia, luego de que el Tribunal Superior de Bogotá respaldara la decisión del juez. 

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

  
Gutiérrez señaló que continúa en este proceso con tranquilidad y envió un mensaje de resistencia y perseverancia a las y los prisioneros políticos de Colombia.

<!-- /wp:paragraph -->

<!-- wp:html -->  
<iframe id="audio_59928875" frameborder="0" allowfullscreen scrolling="no" height="200" style="border:1px solid #EEE; box-sizing:border-box; width:100%;" src="https://co.ivoox.com/es/player_ej_59928875_4_1.html?c1=ff6600"></iframe>  
<!-- /wp:html -->

<!-- wp:paragraph -->

Ver mas: [Programas de EXPRESO LIBERTAD](https://archivo.contagioradio.com/programas/expreso-libertad/)

<!-- /wp:paragraph -->
