Title: Ambientalistas se ofrecen como garantes entre gobierno y ELN
Date: 2016-09-16 13:46
Category: Ambiente
Tags: Ambiente, ELN, Gobierno, naturaleza, paz, víctimas
Slug: ambientalistas-se-ofrecen-como-garantes-entre-gobierno-y-farc
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/09/eln-y-gobierno-e1474051328819.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: La Patilla 

###### [16 Sep 2016]

Ambientalistas y animalistas, enviaron una carta a la guerrilla del ELN y al gobierno nacional, buscando **impulsar la fase pública de diálogos de paz** entre las partes, y además ofreciéndose como mediadores del proceso.

En el documento, afirman que es necesario que se inicie esta nueva fase con el ELN, para garantizar una paz completa, y de esa manera cesen las acciones de guerra que no solo afectan a las comunidades sino también a la naturaleza. Es por ello que se ofrecen como garantes entre el gobierno y el ELN, para construir junto a la sociedad civil **“una agenda de transición y paz social, con la naturaleza y con la vida;** una paz democrática y sustantiva para la restauración de nuestra estructura ecológica”, y añaden  “No queremos legar a las futuras generaciones una guerra en un desierto”.

En ese sentido, presentaron una serie de propuestas para garantizar esa agenda. Entre ellas se destaca el desarrollo de una “**Cumbre Nacional para la Paz Ambiental”**, donde habrá un espacio propicio para la construcción de una agenda socio-ambiental. Así mismo, anuncian que buscan generar y acompañar planes territoriales alternativos y sustentables, que permitan reparar los daños ambientales y las víctimas ocasionadas por la guerra y el modelo extractivista.

Por otra parte, buscan contribuir a la creación de mecanismos para que la participación del ambientalismo **“sea un aporte vibrante a esa paz que nos comprometemos construir”,** dice la carta.

Finalmente, señalan que como movimiento ambientalista seguirán trabajando en buscar que el gobierno y el ELN se sienten a dialogar con base en una agenda que busque una paz con justicia social y ambiental.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 

<iframe id="doc_79476" class="scribd_iframe_embed" src="https://www.scribd.com/embeds/324244014/content?start_page=1&amp;view_mode=scroll&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="undefined"></iframe>
