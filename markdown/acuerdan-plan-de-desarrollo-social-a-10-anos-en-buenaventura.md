Title: Este es el acuerdo entre Comité Cívico de Buenaventura y gobierno
Date: 2017-06-06 11:14
Category: Entrevistas, Movilización, Nacional
Tags: buenaventura, Movilización, Paro Buenaventura
Slug: acuerdan-plan-de-desarrollo-social-a-10-anos-en-buenaventura
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/06/5936ba5c044d7.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Elpais.com.co] 

###### [06 jun 2017] 

Gobierno y miembros del Comité del paro Cívico de Buenaventura llegaron a un acuerdo para levantar el paro que completa 22 días. **Acordaron el día de hoy la creación de un proyecto de ley que contendrá un plan de desarrollo social a 10 años** cuyos proyectos, programas e iniciativas serán financiados a través de un patrimonio o fondo autónomo.

Concretamente, en el proyecto de ley quedarán enmarcados los proyectos, programas y fuentes de financiación, que incluyen los recursos del Presupuesto General de la Nación, para **garantizar la satisfacción de los derechos económicos, sociales y culturales de los bonaverenses.** De igual forma, en el acuerdo se establecieron los mecanismos de evaluación, seguimientos y control social de los avances de los proyectos y los programas. Le puede interesar: ["Continuará paro en Buenaventura hasta que el fondo autónomo sea una realidad"](https://archivo.contagioradio.com/continuara-paro-en-buenaventura-hasta-que-fondo-autonomo-sea-una-realidad/)

En la mesa de negociación, según Javier Torres miembro del Comité Cívico de Buenaventura, “alcanzamos un buen acuerdo con el gobierno que inicia con la creación de un fondo autónomo **con base de 1.5 billones de pesos para iniciar obras de infraestructura** **inmediatamente,** el monto irá aumentando paulatinamente”.

Torres afirmó que el resto de los recursos, que completan **los 10 billones de pesos propuestos por los pobladores,** serán dineros de regalías, impuestos por aduana, contra prestación portuaria, presupuesto de la gobernación y el presupuesto nacional para la región.

**Metas propuestas entre gobierno y población**

-   En materia de salud, las partes acordaron garantizar a los ciudadanos del distrito la accesibilidad, integralidad, suficiencia y calidad en la atención de los servicios de baja, mediana y alta complejidad.
-   En cuanto al tema de agua potable y servicios de saneamiento básico, garantizaron el 100% de la cobertura en todo Buenaventura.
-   El mismo acuerdo del 100% se hizo para garantizar la calidad, la cobertura y la pertinencia de la educación.
-   El acuerdo en actividades económicas ancestrales se hizo para recuperar la pesca, el aprovechamiento forestal sostenible, la manufactura y artesanía, la minería artesanal, el comercio local, la producción agropecuaria, la creación de un clúster portuario y de transporte y otras actividades productivas que fortalezcan la autonomía alimentaria y el empleo.
-   Como meta de vivienda, se acordó superar el déficit de vivienda de las comunidades rurales y urbanas en un plazo de 10 años.

### **El fondo autónomo**

El fondo autónomo que se presentó tendrá como **primer financiador al gobierno nacional quien utilizará el 50% del impuesto a la renta para el fondo.** Seguidamente el gobierno nacional creará un crédito externo de 76 millones de dólares para construir la ciudadela hospitalaria, la construcción de la primera fase del sistema de alcantarillado, los acueductos rurales, la Unidad de Cuidados Intensivos del Hospital Luis Ablanque de la Plata, el tecnoparque industrial pesquero y agrícola y el muelle de cabotaje. Le puede interesar: ["El nobel de paz en "modo"represión"](https://archivo.contagioradio.com/el-nobel-de-paz-en-modo-represion-en-buenaventura/)

Adicionalmente, Javier Torres aseguró que **“se acordó la creación de una comisión que garantice la no persecución de los miembros del comité del paro** y los bonaveresnse en general”. La comisión estará compuesta por miembros representantes de las Naciones Unidas, organizaciones sociales, la Procuraduría General de la Nación y La Defensoría del Pueblo.

Los pobladores acordaron ejercer mecanismos de veeduría autónomos de alto nivel para vigilar que los acuerdos se materialicen y se cumplan. Ellos y ellas han manifestado que **“el manejo de los recursos del fondo autónomo, no va a dar pie a que el dinero se invierta donde no debe ser”.**

Se espera que la veeduría se haga de manera transparente con mecanismos como audiencias públicas y vigilancias estatales. El proyecto de ley que se acordó, será presentado el **20 de julio en el Congreso de la República con un mensaje de urgencia.**

<iframe id="audio_19109002" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_19109002_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU).
