Title: En las calles por la autonomía universitaria
Date: 2019-10-10 22:29
Author: CtgAdm
Category: Educación, Movilización
Tags: ESMAD, estudiantes, marcha, Universidades
Slug: en-las-calles-por-la-autonomia-universitaria
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/Autonomía-universitaria.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: @JhonBallesteros  
] 

Este jueves en todo el país se adelantan jornadas de movilización por parte de sectores estudiantiles que reclaman, principalmente, el desmonte del Escuadrón Móvil Antidisturbios (ESMAD), por ser una fuerza contraria al interés de proteger el derecho a la protesta. Sin embargo, según han explicado líderes del movimiento, **esta jornada tiene el carácter de unificar más banderas, tal como ocurrió en los meses finales de 2018.** (Le puede interesar: ["Con represión quieren detener al movimiento estudiantil"](https://archivo.contagioradio.com/saldo-catastrofico-de-operacion-del-esmad-contra-protestas-y-universidades/))

### **Una movilización que une instituciones, movimientos sociales y banderas** 

Julián Báez, representante estudiantil de la Universidad Distrital, afirmó que la jornadas de marcha fueron posibles gracias a una convocatoria a nivel nacional que unió a todas las Instituciones de Educación Superior (IES) e Instituciones Técnicas Profesionales, Tecnológicas y Universitarias Públicas (ITTU's) en torno a cuatro banderas concretas: Contra la represión sufrida por los estudiantes, por el desmonte del ESMAD, el respeto a la autonomía universitaria y contra la corrupción en los campus.

Adicionalmente, Báez señaló que otros temas comunes son la lucha para **que se abran más espacios de participación en la que los estudiantes puedan ser veedores y partícipes de la asignación presupuestal en sus universidades,** así como el reclamo al Gobierno para que cumpla con los recursos prometidos tras el acuerdo alcanzado con los estudiantes durante 2018. (Le puede interesar:["Gobierno y estudiantes logran acuerdo en mesa de negociación"](https://archivo.contagioradio.com/estudiantes-gobierno-acuerdo/))

### **Con los sectores sociales por el desmonte del ESMAD** 

Báez señaló que para esta marcha, los estudiantes se unieron a sectores que históricamente han sido reprimidos por el ESMAD, así, "logramos una bandera conjunta que estamos viabilizando". Posteriormente, el líder señaló que los estudiantes harán conciencia sobre lo que significa la represión, al tiempo que recordó que desde la Red Distrital Universitaria de Derechos Humanos se está preparando una demanda contra este cuerpo policial.

El representante de la Universidad Distrital criticó el despliegue de policía que se puso en marcha este jueves contra los estudiantes, al afirmar que **los recursos invertidos por las entidades locales para tal despliegue, podrían ser utilizados en abrir plazas para educación** en centros universitarios. (Le puede interesar: ["Estudiantes de la Universidad Distrital tienen razones de sobra para protestar"](https://archivo.contagioradio.com/estudiantes-de-la-universidad-distrital-tienen-razones-de-sobra-para-protestar/))

### **Autonomía universitaria, democracia participativa  
** 

En cuanto a la propuesta sobre la corrupción, Báez aseguró que la solución no está en sacar a las personas que están implicadas en estos casos, "porque incluso pueden venir nuevas personas a hacer lo mismo después", en cambio, explicó que en la Universidad Distrital la propuesta está en reformar la institución, "al punto que las estructuras de decisión no recaigan en pocas personas o sobre la directiva". De esta manera, se crearía un espacio amplio de decisión que han decidido llamar "asamblea universitaria", donde se reestructure la Universidad y sus estatutos.

Báez aclaró que asumiendo que la formación y participación de los estudiantes en este proceso es necesaria, esa asamblea debería incluirlos en los espacios de decisión, en ese sentido, aseguró que "la propuesta para todas las universidades es esa: tenemos que reformarnos". El líder estudiantil concluyó que el movimiento espera poder reunirse nuevamente para tratar este, y otros temas, en un próximo Encuentro Nacional de Estudiantes de Educación Superior (ENEES) que se espera realizar el próximo año.

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
