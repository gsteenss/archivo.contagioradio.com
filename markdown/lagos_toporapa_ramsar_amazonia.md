Title: Blindaje Ramsar para los Lagos de Tarapoto en la Amazonía
Date: 2018-01-18 18:13
Category: Ambiente, Nacional
Tags: Amazonía, lagos de Toporapa, Ramsar, WWF
Slug: lagos_toporapa_ramsar_amazonia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/01/vista_aerea_de_tarapoto_2_fernando_trujillo_fundacion_omacha-e1516316378568.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Fernando Trujillo - Fundación Omacha] 

###### [18 Ene 2018] 

Los **lagos de Tarapoto ubicado en la Amazonía, y un complejo de 30 lagos y humedales acaba de ser declarado como sitio Ramsar. **En medio de las múltiples amenazas que enfrenta el pulmón del mundo, la Amazonía, se asoma una luz de esperanza para impedir que esa región tan importante para todo el planeta se siga destruyendo, ya que el 12% de su territorio ha sido transformado.

El complejo de humedales comprende cerca de 40 mil hectáreas. Una zona de interés ecológico debido a la gran biodiversidad presente en sus territorios, al ser el hogar de  **883 especies de plantas, 244 de aves, 176 de peces, 30 de reptiles, 201 de mamíferos y 56 de anfibios.**

Se trata de un paso crucial para blindar esa zona. Que este lugar se defina como un sitio Ramsar, representa el compromiso del Gobierno de adoptar las medidas necesarias para garantizar que se mantengan sus características ecológicas. Esta decisión también servirá como herramienta para que las comunidades incidan positivamente en la conservación de sus estilos de vida.

Esto además, permitirá que se fortalezca el plan de manejo de los lagos, la soberanía alimentaria, la medicina ancestral, la construcción sostenible, la ornamentación, ceremonias y rituales socioculturales en Tarapoto, gracias a la estrategia de protección que se ha trazado entre la comunidad y el gobierno colombiano.

**Colombia ya cuenta con seis sitios Ramsar con una superficie total de 708,684 hectáreas,** entre los que están  el Sistema Delta Estuarino del Río Magdalena y la Ciénaga Grande de Santa Marta, la Laguna de La Cocha, el Delta del río Baudó, el Complejo de Humedales Laguna del Otún, el Sistema Lacustre de Chingaza y la Estrella Fluvial del Inírida.

### **¿Por qué se define como sitio Ramsar?** 

La WWF, el Instituto Sinchi, el Ministerio de Ambiente y la Fundación Omacha, de la mano de las comunidades indígenas, en cabeza del resguardo Aticoya, vienen desarrollando un arduo trabajo desde hace más de 8 años, con el objetivo de lograr que dicha región de la Amazonía sea esencial para la conservación de la naturaleza ante los ojos del mundo, que haya tal necesidad de declararla en la lista de humedales de importancia internacional.

Además la importancia ambiental de la Amazonía no se limita a la gran cantidad de especies de flora y fauna. Su conservación -según los ambientalistas y autoridades- juega un papel decisivo para el bienestar del ser humano. **La selva representa el 19% de la superficie forestal del mundo y allí se concentran el 20% de las reservas de agua dulce del planeta.** (Le puede interesar: [12% del territorio de la Amazonía ha sido transformado)](https://archivo.contagioradio.com/12-la-amazonia-colombiana-ha-transformada/)

Cabe recordar que el equivalente a 5 millones de canchas de fútbol, ha sido el total del territorio colombiano de la Amazonía que se ha visto afectado debido, entre otras, a la ganadería extensiva, la minería,  los proyectos hidroeléctricos y la construcción de vías de trasporte, según un informe de WWF (Fondo Mundial para la Naturaleza). Esto, sin  tenerse en cuenta que los daños a esta región del mundo, aceleran el cambio climático.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)
