Title: Líder y defensor de desplazados Yunier Moreno fue asesinado en Caquetá
Date: 2019-09-09 13:42
Author: CtgAdm
Category: DDHH, Nacional
Tags: Caquetá, Cristales, Líder Social Asesinado, Muerte, Policía Nacional
Slug: yunier-moreno-nuevo-atentado-acaba-con-la-vida-de-lider
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/09/lider_social_caqueta_0.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:] 

El pasado 8 de septiembre en el departamento de Caquetá, **Yunier Moreno**, **líder social** perteneciente a la Asociación de Desplazados de la Vereda Peñas Coloradas,  fue abordado por hombres desconocidos en su vivienda en la vereda Cristales, quienes lo atacaron con arma de fuego hasta ocasionar su muerte. (Le puede interesar: [No vamos a salir de nuestro territorio, no vamos a dejar nuestra casa: líderes sociales del Cauca](https://archivo.contagioradio.com/no-vamos-salir-nuestro-territorio-no-vamos-a-dejar-nuestra-casa-lideres-sociales-cauca/))

<div class="group-container field-group-html-element">

<div class="node-teaser field field--name-field-teaser field--type-text-long field--label-hidden">

**Yunier Moreno Jave,** vivió en el corregimiento de Cristales, zona rural del municipio de Cartagena del Chairá por mas de 10 años, trabajó como líder social, y se encargaba del manejo de la planta generadora de energía en esta zona,  ademas, según fuentes de la comunidad tenia interés en postularse como concejal del municipio, pero no alcanzó a cumplir con los requisitos para tal fin. (Le puede interesar: [La MOE ha registrado 228 hechos de violencia contra líderes sociales en últimos 8 meses](https://archivo.contagioradio.com/moe-228-hechos-violencia-lideres/))

Según autoridades de la zona Moreno, no había denunciado amenazas en su contra, lo cual ha generado una mayor incertidumbre sobre  la razón de su muerte y la identidad de sus agresores, para ello, la Fiscalía dispuso un equipo de investigadores del CTI y el cuerpo élite de la Policía Nacional, hasta el momento no se conocen resultados de las investigaciones.

</div>

</div>

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
