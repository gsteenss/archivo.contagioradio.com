Title: Policía aumentó fumigaciones con glifosato en Putumayo y Nariño
Date: 2015-06-01 16:44
Category: Ambiente, Nacional
Tags: CCJ, CINEP, fumigaciones aéreas, Glifosato, Glifosato en Colombia, GRUPO SEMILLAS, herbicidas, INDEPAZ, LAWG, MAMACOCA, PCN, Plan de Manejo Ambiental del Programa de Erradicación de Cultivos Ilícitos, PLANETA PAZ, RED DE JUSTICIA AMBIENTAL, WOLA
Slug: policia-aunmentofumigaciones-con-glifosato-en-putumayo-y-narino
Status: published

###### Foto: [es-us.noticias.yahoo.com]

<iframe src="http://www.ivoox.com/player_ek_4580633_2_1.html?data=lZqlkpuXd46ZmKiakpWJd6KkkZKSmaiRdI6ZmKiakpKJe6ShkZKSmaiRk9PbwtPW3MbHrdDixtiY1dTHrcLgxtiY0s7Iqc%2BfxtjQzsbWqcTdzs7S0NnTb9Tjw9fSjczQrY6ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Yamile Salinas, abogada] 

Diferentes organizaciones sociales solicitan la **revocatoria del Plan de Manejo Ambiental del Programa de Erradicación de Cultivos Ilícitos con el herbicida Glifosato** (PECIG) y piden a los  Ministros, de Salud, Agricultura y Ambiente para que se **convoque al Consejo Nacional de Plaguicidas,** con el fin de que estudien las nuevas sustancias que está utilizando la Policía Antinarcóticos para erradicar cultivos de uso ilícito.

**NDEPAZ, Comisión Colombiana de Juristas, CINEP, Grupo Semillas, Planeta Paz, LAWG, MAMACOCA, Red de Justicia Ambiental, PCN y WOLA,** son las organizaciones que hacen el llamado a que el Consejo Nacional inste a la Autoridad Nacional de Licencias Ambientales, ANLA, para que revoque el Plan de Manejo del programa de erradicación con glifosato (del año 2001) pues consideran que ese plan de manejo no puede aplicarse a nuevas sustancias.

Yamile Salinas, abogada y asesora de INDEPAZ, asegura que el Consejo Nacional de Estupefacientes dijo que se suspendería el glifosato, más no las fumigaciones, y no hay una decisión contundente sobre ese tema. Además, afirmó que lo que está haciendo la Policía Antinarcóticos es **aumentar las fumigaciones en Putumayo y Nariño** hasta el plazo que dé el CNE para suspender el glifosato.

Así mismo, según la asesora de Indepaz, se conoce que se están haciendo pruebas con 5 y 7 moléculas de herbicidas en una base de la policía en San Luis, Tolima, pero **no se conoce sobre sus efectos en el ambiente y la salud humana;** es por ello que se hace urgente un estudio sobre los niveles de toxicidad de las nuevas sustancias.

Si la ANLA revoca el Plan de Manejo Ambiental que existe, la policía no podría continuar con las fumigaciones en el país, y además, si se piensa realizar fumigaciones con nuevos herbicidas, se debería solicitar una licencia ambiental que reglamente ese programa.

Cabe resaltar que el pasado sábado 30 de mayo la **Policía continuó las fumigaciones aéreas con glifosato sobre comunidades indígenas del Putumayo, **a pesar de efectos cancerígenos de este herbicida. Como consecuencia se vio afectada la salud de los comuneros, sus cultivos de pan coger como yuca, chiro, plátano, entre otros, pastos, y la Biodiversidad de esta región Amazónica.
