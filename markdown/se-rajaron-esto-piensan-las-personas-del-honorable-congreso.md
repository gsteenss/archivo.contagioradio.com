Title: ¡Se rajaron! esto opinan las personas del "honorable Congreso"
Date: 2018-01-29 14:41
Category: Otra Mirada, Video
Tags: A la Calle, Congresistas, Congreso, Votaciones
Slug: se-rajaron-esto-piensan-las-personas-del-honorable-congreso
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/01/a-la-calle-.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

Salimos a las calles de Bogotá para preguntar a las personas qué opinan del Congreso de la República y qué tendrán en cuenta a la hora de votar el próximo 11 de marzo, esto nos dijeron.
