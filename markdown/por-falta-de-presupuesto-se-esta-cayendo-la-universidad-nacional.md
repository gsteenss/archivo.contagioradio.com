Title: Por falta de presupuesto se está cayendo la Universidad Nacional
Date: 2016-11-01 16:21
Category: Educación, Nacional
Tags: crisis universidad pública Colombia, Movimiento estudiantil Colombia
Slug: por-falta-de-presupuesto-se-esta-cayendo-la-universidad-nacional
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/08/universidad-nacional-se-cae-10.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [1 de Nov 2016] 

**Los edificios de la Universidad Nacional se están cayendo debido al déficit presupuestal** que afronta esta institución. El viernes de la semana pasada, por segunda vez en dos años, se cayó el techo de la Facultad de Derecho, mientras que ayer edificios como el de Enfermería y Diseño gráfico se inundaron.

En total s**on 21 edificios** de la Universidad Nacional que **se encuentran catalogados como de alto riesgo de vulnerabilidad, 4** están **identificados con amenaza de ruina y 7 han sido reforzados en sus cimientos**. Sin embargo, pese al peligro que esto representa los estudiantes siguen tomando clases en edificios que están a punto de caerse como la Facultad de Cine y Televisión o la Facultad de Artes, edificio que está considerado como patrimonio arquitectónico de la ciudad.

Sumado a esto **71% de los edificios no tienen adecuación de sismo resistencia** y tampoco hay más escenario o salones que puedan adecuarse para que los estudiantes sean trasladados, caso de las y los estudiantes de Artes que por ahora no ven clases en su edificio para prevenir incidentes.

Carlos Ariel Bautista, representante por los estudiantes de la Facultad de Derecho, afirmó que la profundización de la falta de inversión a la universidad pública viene dándose desde la implementación de **la ley 30, en donde se realizan cortes reiterativos al presupuesto y que en la actualidad da como resultado un déficit de 16 billones de pesos.**

Bautista señala que como primera medida y después de una asamblea los estudiantes, iniciarán con la **"toma de acciones judiciales que responsabilicen a la Administración por lo que ocurra dentro de la Universidad y al Gobierno Nacional"**.  En el caso de la Facultad de Derecho, hay una acción interpuesta en el Tribunal Superior de Cundinamarca, que señala que una de las medidas para garantizar la protección de los estudiantes es evacuar el edificio, no obstante por el momento no hay ningún pronunciamiento.

Para los estudiantes las responsabilidades frente al deterioro del campus son evidentes, por un lado se encuentra la **Administración del a Universidad que “ha sido negligente al no hacer estudios de mantenimiento de los edificios de la institución”** y por el otro se encuentra el Ministerio de Educación que de acuerdo con Carlos **“sigue reduciendo los presupuestos a la Universidad y que intenta reparar con paños lo que ocurre”**. Le puede interesar: ["Universidad Nacional se raja en infraestructura"](https://archivo.contagioradio.com/23-edificios-en-riesgo-y-4-con-amenaza-de-ruina-estudiantes-unal-se-movilizan/)

<iframe id="audio_13571774" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_13571774_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

######  Reciba toda la información de Contagio Radio en su correo [[bit.ly/1nvAO4u]
