Title: Avalancha de interpretaciones a la Jurisdicción Especial para la Paz
Date: 2015-12-22 17:55
Category: Otra Mirada, Paz
Tags: Acuerdo sobre víctimas, Conversaciones de paz de la habana, ELN, FARC, Juan Manuel Santos, jurisdicción especial para la paz
Slug: interpretaciones-a-la-jurisdiccion-especial-para-la-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/12/Acuerdo_victimas_contagioradio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: eluniversal 

###### [22 Dic 2015]

Mientras que para algunas víctimas el **Sistema Integral de Verdad, Justicia, Reparación y No Repetición ha significado un avance innegable en materia de derecho a la verdad**, para otras víctimas y algunas organizaciones de DDHH las interpretaciones que se han discutido en torno al acuerdo lo ven como la posibilidad de impunidad para los crímenes cometidos por agentes del Estado.

Algunas de las críticas que se han suscitado tienen que ver con la cadena de mando de crímenes cometidos por agentes estatales, que **se presume la legalidad de todas las actuaciones de los agentes del Estado** y que se revisarían condenas para acceder a penas alternativas e incluso reducción de las condenas impuestas.

Mientras tanto para otro grupo de víctimas este acuerdo es muy avanzado en materia de justicia transicional porque permite que prime el derecho a la verdad, aplica la posibilidad de penas restaurativas ante la premisa de que la cárcel tradicional no repara el daño ni permite una “resocialización”, previo reconocimiento de responsabilidad y develación de la verdad, como lo han afirmado la Red de Comunidades Construyendo Paz, CONPAZ y lo contempla la JEP “**no es cierto que haya impunidad sino que se estimula la verdad”**.

Agregan que en lo planteado si se diferencia el delito político del crimen de Estado y las sanciones son proporcionales a la verdad aceptada y que “es evidente” que la Jurisdicción Especial para la Paz va a juzgar graves violaciones a los **Derechos Humanos e infracciones al Derecho Internacional Humanitario**. Ese tipo de crímenes no van a quedar por fuera de la JEP y [serán juzgados ha afirmado Enrique Santiago](https://archivo.contagioradio.com/?s=jurisdiccion+especial), uno de los juristas que asesoró la elaboración del acuerdo.

Sin embargo, las mismas comunidades CONPAZ señalan que una **ausencia sensible en el SIVJRR y la JEP es en torno a la responsabilidad de los presidentes y ex presidentes** que seguirá en manos de la Comisión de Acusaciones de la Cámara de Representantes que ha sido valorado como un claro escenario de impunidad para los crímenes de los antecesores del presidente Juan Manuel Santos.

[CONPAZ](https://archivo.contagioradio.com/?s=conpaz) señala que la imposibilidad de que los expresidentes hagan parte de la JEP es “triste pero es así, estamos hablando de unas conversaciones para llegar a acuerdos y eso es lo avanzado” y reiteran que “a la sociedad y a las víctimas les toca asumir unos retos, uno de ellos es **lograr avances en el proceso con el ELN y que algo como este punto de los expresidentes pueda cambiarse bajo el principio de igualdad ante la ley**”.

Frente a las [garantías de No Repetición el grupo de comunidades insiste en la necesidad del desmonte del paramilitarismo](https://archivo.contagioradio.com/dudas-y-cuestionamientos-sobre-jurisdiccion-especial-para-la-paz/) e insisten en que el juzgamiento de ex presidentes sería **un gesto de real voluntad de paz por parte del mandatario actual y los ex mandatarios.**

Recientemente **Jose Miguel Vivanco, director para las Américas de la organización Human Rigths Watch**, afirmó que el acuerdo sobre JEP es "una piñata de impunidad" afirmaciones que según el gobierno obedecen a interpretaciones parciales de lo pactado en la Habana.
