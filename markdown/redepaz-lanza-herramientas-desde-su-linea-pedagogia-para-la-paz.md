Title: Redepaz lanza herramientas desde su línea Pedagogía para la Paz
Date: 2016-03-06 11:05
Category: Nacional, Paz
Tags: FARC, proceso de paz, Redepaz
Slug: redepaz-lanza-herramientas-desde-su-linea-pedagogia-para-la-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/03/Terrepaz-e1457126046305.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Notimerica 

###### [5 Mar 2016]

Con dos cartillas y un curso virtual, Redepaz realiza un ejercicio de sensibilización de los acuerdos de Paz en la Habana, a partir de elementos pedagógicos que servirán como guía para la sociedad civil en el entendimiento de la refrendación de la Paz de Colombia.

El pasado 3 de marzo, en las instalaciones de Redepaz, se llevó a cabo el lanzamiento de las cartillas “Las niñas y los niños tenemos derechos” y “Pistas Pedagógicas” dos instrumentos que permiten abordar de  forma clara los acuerdos de paz en la Habana, desde la infancia y la docencia en los colegios.

“Las cartillas son pistas pedagógicas que le permiten a niños y niñas, reconocerse como sujetos de derechos y a su vez, adquirir el compromiso con la construcción de una sociedad de paz, la segunda cartilla es un conjunto de herramientas para las y los docentes en la instalación de las Cátedras de Paz” dice Luis Emil Sanabria, vocero de Redepaz.

A su vez, se realizó el lanzamiento del curso virtual “Es la Hora de la Paz” que a través de 7 diferentes módulos busca explicar cada uno de los puntos de refrendación de los acuerdos de Paz en la Habana, estos módulos tienen como personaje principal a Nicolasa, que es el personaje encargado de guiar al público en general, durante cada uno de los talleres.

También se hizo un llamado al gobierno nacional y a la guerrilla del ELN para que se supere la fase exploratorio de diálogos de paz y se pase a la fase pública.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
