Title: Revelan posible saboteo a visita del Papa Francisco a Cuba
Date: 2015-09-14 15:23
Category: El mundo, Otra Mirada, Política
Tags: CIA, Cuba, NEA, Raúl Capote, Relaciones Cuba - EEUU, USAID
Slug: revelan-posible-saboteo-a-visita-del-papa-francisco-a-cuba
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/Capote.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:www.telepinar.icrt.cu] 

###### [14 Sept 2015] 

[Raúl Capote, ex agente de seguridad cubano infiltrado en la CIA por cerca de 7 años, en entrevista exclusiva con RT habló sobre **el funcionamiento de los grupos opositores en Cuba, así como las posibles razones de sus intentos de sabotaje a la anunciada visita del Papa Francisco a la isla**.]

[Capote aseguró que grupos de derecha extrema situados en Miami, están convocando a ocupar iglesias e interrumpir celebraciones eucarísticas presididas por el Papa Francisco una vez llegue al país, con el fin de que autoridades y medios de comunicación declaren que Cuba es peligrosa e inestable, para  poner en entredicho la viabilidad de las relaciones políticas que el gobierno de EEUU ha afirmado revitalizar con este país.]

[Entre los nombres de los directamente implicados en estas provocaciones figuran Iliana Ros, Marco Rubio, Ibrahím Bosch y Mario Díaz-Balart, agrega Capote y enfatiza que organizaciones como la USAID, la NEA y la CIA continúan financiando grupos de oposición ilegal en Cuba, con el objetivo de subvertir el orden interno en la isla.  ]
