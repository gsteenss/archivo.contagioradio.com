Title: ¿Qué le espera a la reserva forestal Thomas van der Hammen?
Date: 2016-03-10 12:34
Category: Ambiente, Nacional
Tags: Ambiente, Bogotá, Peñalosa, Reserva forestal Tomas Van Der Hammen
Slug: reserva-thomas-van-der-hammen-y-su-futuro
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/01/Reserva-Tomas-Van-der-Hammen-e1457630338748.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

#### **\* Fernando Gómez P.** 

[*[“Así como el siglo XIX perteneció al carbón y el XX al petróleo, ]el XXI pertenecerá a la luz solar, [el viento y la energía generada por la Tierra” ]*[Lester Brown.]]

###### 10 Mar 2016 

[En reciente entrevista el Alcalde Enrique Peñalosa se pregunta]*[“…¿Qué es lo mejor ambientalmente para Bogotá?”]*[. Sin duda, una inquietud que estará en los bogotanos porque la realidad socio-ambiental así lo demanda. Llama la atención la manera en que se refiere a una de las zonas estratégicas por su función ecológica: la Reserva Forestal ubicada en el Borde Norte. Sobre ésta comenta que es un]*[“lote”]*[y que es]*[“… la única reserva forestal del mundo que no tiene árboles: son potreros”.]*

[Sin embargo, estudios realizados por Universidades, Centros de Investigación, entidades del Estado, organizaciones ambientales, expertos nacionales e internacionales contradicen lo expresado por el mandatario. Las investigaciones señalan que]***la Reserva Forestal Productora Regional de Bogotá “Thomas van der Hammen”*** [(en adelante R.F.R.P.N)]***es un área con una extraordinaria riqueza natural, histórica, cultural y paisajística de Bogotá y de la Región Central del país.***[ Precisamente la Reserva cumple funciones de conectividad con la Estructura Ecológica Principal de Bogotá y la Estructura Ecológica Regional. Existen ecosistemas propios de bosque bajo andino. Es lugar de hábitat de especies endémicas por estar ubicada en la Cuenca Alta del río Bogotá. Allí se encuentran los mejores suelos agropecuarios (Tipos II y III). Cumple funciones de regulación hídrica al poseer zonas recarga de agua, humedales y quebradas. Adicionalmente posee un inmenso valor cultural y arqueológico como lo ha señalado el Instituto Colombiano de Antropología pues allí existe vestigios de una obra de Ingeniería Muisca.]

[En efecto, la investigación realizada por la CAR, la UDCA, el Instituto Geográfico Agustín Codazzi, la Fundación Avina, el IEU de la Universidad Nacional, la Academia Colombiana de Ciencias Exactas, Físicas y Naturales y la Dirección Especial de Catastro Distrital, concluyen que la R.F.R.P.N]*[“Thomas van der Hammen”]*[es un lugar natural con cobertura vegetal y ecosistema relictuales. Existe un sistema de humedales como son El Torca, Guymaral y la Conejera. En el subsuelo hay abundantes recursos hídricos alimentados con aguas de escorrentía. Contiene suelos agroecológicos de la mejor calidad para proyectos productivos agroforestales, silvopastoriles y de seguridad alimentaria. Incluye un bosque andino que sirve como reservorio de biodiversidad. En flora existen 514 especies de plantas inventariadas. Con respecto a la fauna existen aves migratorias, acuáticas, ardillas, borugas, curíes, conejos, murciélagos, faras, guaches, comadrejas, 47 ejemplares de mariposas, anfibios y reptiles, especies endémicas como el Chamicero y el Picocono rufu. Por sus características rurales y de sabana se identifica a primera vista su riqueza natural e histórica, que demuestra la valiosa biodiversidad existente en el área forestal.]

[![conectividad-van-der-hammen](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/03/conectividad-van-der-hammen.png){.wp-image-21299 .alignleft width="437" height="244"}](https://archivo.contagioradio.com/reserva-thomas-van-der-hammen-y-su-futuro/conectividad-van-der-hammen/)

### [**¿Y LA RESERVA PARA QUÉ?**] 

El caso de la R.F.R.P. ilustra la urgencia de aportar elementos importantes en el debate entre densificación o expansión urbana, pues es pieza clave para efectos de definir el llamado límite del crecimiento para evitar la conurbación. La Reserva posibilita: minimizar los conflictos socio-ambientales por la transformación drástica del territorio rural, mayor control por parte de las autoridades ambientales, cambio de uso de suelo, aprovechamiento óptimo con vocación agroforestal.

El profesor van der Hammen y un grupo de expertos indicaban que ***el propósito de la Reserva Forestal es garantizar la conectividad ecológica entre la Reserva Forestal de los Cerros Orientales, el río Bogotá y continuar la conexión con los cerros de Chía y Cota, especialmente el Cerro del Manjui***. Esta Reserva permitirá la estructura de una compleja red ambiental, conectando varios ecosistemas y consolidando un amplio sistema de hábitat de flora y fauna. La R.F.R.P.N por su extensión (1.391,68 hectáreas) puede constituirse en el bosque natural urbano más extenso del planeta, cuatro veces más grande que el Central Park de Nueva York.

### [**BOGOTÁ NECESITA LA RESERVA** ***“THOMAS van der HAMMEN”***] 

Factores como el cambio climático, densidad demográfica, centralización del conflicto armando, inseguridad alimentaria y la contaminación de fuentes hídricas, permiten repensar el modelo de desarrollo sostenible para evitar el colapso urbano. La R.F.R.P.N contribuye significativamente a la adaptación y mitigación al cambio climático. Bogotá tiene un déficit promedio de un millón y medio de árboles según el Jardín Botánico. Para la Organización Mundial de la Salud el estándar internacional debe ser mínimo de un árbol por cada tres habitantes. Es decir hay un árbol por cada ocho ciudadanos. De tal manera que la deuda social y ecológica con la ciudad es monumental.

[En septiembre de 2014 la CAR adoptó el Plan de Manejo Ambiental (PMA) para la R.F.R.P.N]*[.]*[Fue construido de manera participativa por diversos actores institucionales, propietarios y organizaciones ambientalistas. El PMA detiene el mal uso del suelo que durante décadas sufrió la zona y acoge los diversos actores e intereses de acuerdo a la extensión y características ecológicas del área. De conformidad la Ley 388/97]***este acto admnistrativo se constituye como una determinante ambiental de superior jerarquía,*** [que por ningun motivo puede ser desconocido, contrariado o modificado. Debe quedar incorporado en el nuevo Plan de Ordenamiento Territorial para Bogotá y demás municipios aledaños a la Reserva.]

[Según la Ley 99/93 la CAR asume: la administración, ejercer las funciones de control y vigilancia e impone las medidas preventivas necesarias. Por estar ubicada la R.F.R.P.N en la zona rural del Distrito, le corresponde a la Alcaldía responder por la formulación e implemetación de instrumentos económicos y tributarios.]**Es competencia del Distrito controlar obras de urbanismo y de construcción que contraríen la norma. Debe adelantar acciones para la recuperación de espacios públicos y ejecutar los proyectos con respecto a la compra de los predios que permita resolver los conflictos prediales**[.]

***La discusión sobre la Reserva es el reflejo de la concepción de un modelo de desarrollo caduco para el Siglo XXI. Es insostenible ambientalmente. La toma de decisiones con respecto a un enfoque de desarrollo sostenible a escala humana es una salida democrática que reorganiza la ciudad respondiendo a los retos y desafíos ambientales y un buen vivir.***

###### \* Sociólogo, Especialista en Estudios del Territorio, Maestrante en Desarrollo Sostenible y Medio Ambiente, Promotor de la Red Ambiental de la Reserva Forestal “Thomas van der Hammen” 
