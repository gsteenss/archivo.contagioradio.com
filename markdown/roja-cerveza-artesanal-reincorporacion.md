Title: La Roja, una cerveza artesanal fruto de la reincorporación
Date: 2019-03-18 11:31
Author: ambiente y sociedad
Category: Expreso Libertad
Tags: cerveza artesansal, FARC, la roja
Slug: roja-cerveza-artesanal-reincorporacion
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/03/la-roja.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/03/6POodqVh.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Contagio radio 

###### 12 Mar 2019 

Integrantes del Espacio Territorial de Capacitación y Reincorporación (ETCR) de Icononzo crearon **La Roja**, una cerveza artesanal hecha a base de panela y lupúlo que **busca rescatar el sabor campesino y la memoria histórica del país**.

Desde finales del año pasado, este proyecto solidario que no contó con el apoyo del gobierno, **nació por la autogestión** y hasta el momento ha logrado una producción de 400 botellas mensuales.

Si quiere probar La Roja, en Bogotá la venden en los cáfe-bar La Lubianka y Café Cinema.

<iframe id="audio_33419527" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_33419527_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Encuentre más información sobre Prisioneros políticos en[ Expreso libertad ](https://archivo.contagioradio.com/programas/expreso-libertad/)y escúchelo los martes a partir de las 11 a.m. en [Contagio Radio](https://archivo.contagioradio.com/alaire.html) 
