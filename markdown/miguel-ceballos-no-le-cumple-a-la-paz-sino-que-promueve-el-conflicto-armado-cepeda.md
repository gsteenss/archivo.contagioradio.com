Title: Miguel Ceballos no le cumple a la paz sino que "promueve el conflicto armado": Cepeda
Date: 2020-05-19 22:12
Author: CtgAdm
Category: Actualidad, Paz
Tags: Cuba, Miguel Ceballos
Slug: miguel-ceballos-no-le-cumple-a-la-paz-sino-que-promueve-el-conflicto-armado-cepeda
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/05/Diseño-sin-título-1.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto:

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

El senador por el Polo Democrático, Iván Cepeda pidió la renuncia del Alto Comisionado para la Paz, Miguel Ceballos, al resaltar que este no desempeña las funciones para las que fue creado el cargo y que, por el contrario, "promueve el conflicto armado”, sin mostrar avances significativos en la Reforma Rural Integral y oponiéndose a medidas cruciales para la implementación del Acuerdo entre ellas las 16 curules de paz.

<!-- /wp:paragraph -->

<!-- wp:core-embed/youtube {"url":"https://www.youtube.com/watch?v=lxRpDvm6kJA\u0026feature=emb_title","type":"video","providerNameSlug":"youtube","className":"wp-embed-aspect-16-9 wp-has-aspect-ratio"} -->

<figure class="wp-block-embed-youtube wp-block-embed is-type-video is-provider-youtube wp-embed-aspect-16-9 wp-has-aspect-ratio">
<div class="wp-block-embed__wrapper">

https://www.youtube.com/watch?v=lxRpDvm6kJA&feature=emb\_title

</div>

</figure>
<!-- /wp:core-embed/youtube -->

<!-- wp:paragraph {"align":"justify"} -->

La petición se dio en medio de una sesión plenaria virtual del Senado en la que el senador se recordó que Ceballos celebró la inclusión de Cuba y Venezuela en la lista de países que no combaten el terrorismo según EEUU. Además de haber “desdeñado el grave hecho de que están ocurriendo de manera sistemática asesinatos contra los excombatientes”. [(Lea también: Es momento de redoblar los esfuerzos por la paz: Iván Cepeda)](https://archivo.contagioradio.com/redoblar-esfuerzos-paz/)

<!-- /wp:paragraph -->

<!-- wp:quote -->

> “Tiene el descaro de decirle al mundo que Colombia, como Estado, se siente orgulloso de que el país (Cuba) que contribuyó a lograr este acuerdo de paz hoy esté considerado como un país que tolera el terrorismo” - Iván Cepeda

<!-- /wp:quote -->

<!-- wp:heading {"level":3} -->

### No hay voluntad política para con el ELN ni diplomacia hacia Cuba

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

En enero de 2019, el gobierno suspendió las conversaciones de paz con el ELN tras el atentado con un carro bomba en la Escuela de Policía General Santander, en donde murieron 22 uniformados. Desde entonces, **Ceballos ha sido firme en su posición de responsabilizar a Cuba de la no entrega y extradición de los miembros negociadores del ELN que están en la isla pese a los protocolos internacionales que blindan la negociación**. [(Lea también: ¿Por qué el Gobierno ni avanza ni rompe conversaciones con el ELN?)](https://archivo.contagioradio.com/conversaciones-de-paz-eln/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Desde 2019 el alto comisionado para la paz ha asegurado que, aunque el país garante es una nación soberana, "hay un tratado de extradición vigente con una jerarquía superior a los protocolos". **Ante estos hechos y la posición del Gobierno, intelectuales y académicos han advertido sobre el error que significaría romper relaciones diplomáticas con Cuba** a través de acciones que "lo único que han logrado con esta política inútil ha sido un escalonamiento de las tensiones bilaterales".[(Lea también: No se puede menosprecia el rol de Cuba en los procesos de paz del mundo)](https://archivo.contagioradio.com/no-se-puede-menosprecia-el-rol-de-cuba-en-los-procesos-de-paz-del-mundo/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

A su vez, calificó la decisión del Departamento de Estado de EE.UU. de incluir a Cuba en la lista de países que cooperan con el terrorismo como un “espaldarazo” a las solicitudes del presidente Iván Duque para que Cuba entregue los jefes del grupo guerrillero, y agregó que esta decisión fue tomada gracias a la insistencia de Colombia para sancionar al país garante ante la administración Trump.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Incluso después que el ELN anunció un cese al fuego unilateral activo durante todo el mes de abril, Ceballos se refirió al gesto como una acción "tardía y no suficiente", nuevamente el pasado 14 de mayo el comisionado señaló que no se ha visto una voluntad de paz por parte del grupo guerrillero, una postura que ha reiterado desde su llegada al cargo. [(Lea también: Arrogancia del Gobierno no le permite ver oportunidades políticas con el ELN: De Currea-Lugo)](https://archivo.contagioradio.com/arrogancia-del-gobierno-no-le-permite-ver-oportunidades-politicas-con-el-eln-de-currealugo/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Aunque Ceballos ha reiterado que el Gobierno tiene las puertas abiertas para dialogar con el ELN, siempre ha insistido en que ese diálogo se debe hacer bajo los términos del Gobierno, que incluyen la entrega de secuestrados y el cese del minado, al respecto, Pablo Beltrán, jefe negociador del ELN ha señalado que siempre han escuchado a la población mientras el Gobierno se ha caracterizado por "sordera y falta de voluntad para una solución política». [(Es necesario negociar un cese bilateral: ELN)](https://archivo.contagioradio.com/en-necesario-negociar-un-cese-bilateral-eln/)

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Ceballos fue designado para responder a la población caucana

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

De igual forma, desde noviembre de 2019, el alto Comisionado de Paz fue designado por el gobierno Duque para coordinar un conjunto de acciones sociales en Cauca que incluirían 41 proyectos de inversión social que beneficiarían a más de 800.000 personas, casi seis meses después, Ceballos indicó que son 15 los proyectos en marcha mientras la situación en el departamento es crítica y se ve reflejada en el asesinato de más de 26 líderes sociales en lo que va corrido del año.  
  
Mientras las masacres ocurridas en los municipios de Micay y Piendamó durante el mes de abril y las situaciones de confinamiento, reclutamiento de menores y agresiones, que se dan fruto de las disputas territoriales entre el ELN y las disidencias de las FARC, y otros actores armados, las acciones del Gobierno continúan dando prioridad a la militarización mientras las comunidades que sostienen, existe una complicidad entre el accionar paramilitar y la Fuerza Pública.[(Lea también: Gobierno tendrá que decidir si es una prioridad o no negociar con ELN)](https://archivo.contagioradio.com/gobierno-tendra-que-decidir-si-es-una-prioridad-o-no-negociar-con-eln/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Dicho impacto negativo se evidencia en un aumento del 25% en la tasa de homicidios en los municipios del Programa de Desarrollo con Enfoque Territorial (PDET) y en los que hacen parte del Programa Nacional Integral de Sustitución de Cultivos de Uso Ilícito (PNIS) entre 2017 y 2019. [(Lea también: Ivan Cepeda: "No se trata solamente de una promesa del Gobierno, sino de que se cumpla lo que está dicho")](https://archivo.contagioradio.com/no-se-trata-solamente-de-una-promesa-del-gobierno-sino-de-que-se-cumpla-lo-que-esta-dicho-ivan-cepeda/)

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### La Reforma Rural Integral tampoco avanza

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Aunque el plan de Acción Social en Cauca contaba con un presupuesto de 1.3 billones de pesos que serían destinados a emprendimientos incluidos los PDET, núcleo de la Reforma Rural Integral que prometía la entrega de tres millones de hectáreas para el campesinado de la que **no se ha entregado ninguna, ejecutándose tan solo el 1.36%, es decir, 90 mil millones de pesos de los 4,67 billones que se tendrían que estar invirtiendo en los municipios PDET.**

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Organizaciones ya habían advertido que para el 2020, el Gobierno redujo los recursos de entidades claves para la Reforma Rural Integral (RRI) otorgando, según el Departamento Nacional de Planeación, \$7,3 billones de un total de \$271,7 billones del Presupuesto General, recortes que representan una ralentización en proyectos destinados a la transformación del campo colombiano.[(Le recomendamos leer: Recortes presupuestales frenan la posibilidad de una Reforma Rural Integral)](https://archivo.contagioradio.com/recortes-presupuestales-frenan-la-posibilidad-de-una-reforma-rural-integral/)

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### La espalda a las 16 curules de paz

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Otro punto al que se refirió el senador fue el rechazo que ha mostrado Ceballos ante las circunscripciones de la paz, cabe resaltar que en enero de 2020, Ceballos, criticó los argumentos jurídicos que presentó el presidente del Congreso, Lidio García, para implementar las 16 curules para la paz, agregando en aquel entonces que desde el Gobierno se presentaría un proyecto de ley para representar a las víctimas en el Congreso. [(Lea también: Propuesta del Gobierno sobre Curules de Paz busca desconocer lo acordado en La Habana)](https://archivo.contagioradio.com/curules-gobierno-desconocer-acuerdo-habana/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

El senador [Iván Cepeda](https://twitter.com/IvanCepedaCast) solicitó al presidente Iván Duque eliminar el cargo en caso que Ceballos no presente su renuncia, afirmando que al evidenciarse una “ausencia de una política de paz“ son necesarios pro proyectos diseñados por el Gobierno para implementar el acuerdo de paz. Por su parte, Ceballos recordó que aunque "debe buscar la voluntad real de paz", su función también es la de desmantelar los grupos armados del país.

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->
