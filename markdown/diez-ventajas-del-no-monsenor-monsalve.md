Title: Diez ventajas del No: Monseñor Monsalve
Date: 2016-10-04 16:01
Category: Nacional, Paz
Tags: plebiscito por la paz, proceso de paz
Slug: diez-ventajas-del-no-monsenor-monsalve
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/10/webnoticias1-e1475614843365.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:WebNoticias] 

###### [4 de Oct 2016] 

El arzobispo de Cali, Monseñor Monsalve ha sido una de las personas, que durante el proceso de diálogos entre las FARC-EP y el gobierno, ha demostrado constantemente su compromiso con la paz, motivo por el cual durante las elecciones al plebiscito aseguro su voto por el sí, y ahora con los resultados expone **10 posibilidades que deja el no para construir otro camino.**

1.  Fue un proceso pacífico, sin violencia ni atropellos contra nadie.
2.  Es una prueba fehaciente de que vivimos en una democracia, donde todos pueden expresar su opinión.
3.  La Registraduría cumplió con transparencia su deber.
4.  Es mejor que él SI haya perdido por poco, a que si hubiera ganado por poco.
5.  Los que votaron por él NO tienen que mostrarle ahora a Colombia cómo es que quieren ayudar a construir la paz que dicen que también quieren.
6.  El gobierno y las FARC tienen que seguir conversando para mejorar los acuerdos.
7.  Tenemos la oportunidad y la obligación de trabajar ahora unidos por la paz.
8.  Los partidos deben reforzar su decisión de trabajar por la paz por encima de intereses mezquinos.
9.  Somos más conscientes de la falta de compromiso y educación política de una inmensa mayoría de colombianos que no votan.
10. La adversidad es maestra de la vida y nos exige ser creativos y resilientes.

Estos puntos reflejan el momento que atraviesa Colombia, por un lado esta el **cese bilateral al fuego que tanto las FARC-EP como el gobierno del presidente Santos** se comprometieron a mantener y que da un parte de tranquilidad a la sociedad, y por el otro la posibilidad de que se habrán nuevos escenarios como la [mesa de negociación con la guerrilla del ELN y el pacto Nacional. ](https://archivo.contagioradio.com/pacto-nacional-podria-ser-la-oportunidad-de-unificar-procesos-de-paz/)

###### [Reciba toda la información de Contagio Radio en su correo [bit.ly/1nvAO4u](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por Contagio Radio [bit.ly/1ICYhVU](http://bit.ly/1ICYhVU)] 

<div class="yj6qo ajU">

</div>
