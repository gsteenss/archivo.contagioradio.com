Title: Ambiente y Sociedad
Date: 2015-01-22 19:44
Author: AdminContagio
Slug: margarita-florez
Status: published

### AMBIENTEYSOCIEDAD

[![El Río Cauca en estado de emergencia tras operación de Hidroituango](https://archivo.contagioradio.com/wp-content/uploads/2019/01/Captura-de-pantalla-6.png "El Río Cauca en estado de emergencia tras operación de Hidroituango"){width="741" height="504" sizes="(max-width: 741px) 100vw, 741px" srcset="https://archivo.contagioradio.com/wp-content/uploads/2019/01/Captura-de-pantalla-6.png 741w, https://archivo.contagioradio.com/wp-content/uploads/2019/01/Captura-de-pantalla-6-300x204.png 300w, https://archivo.contagioradio.com/wp-content/uploads/2019/01/Captura-de-pantalla-6-370x252.png 370w"}](https://archivo.contagioradio.com/el-rio-cauca-en-emergencia-ambiental-por-sequias/)  

###### [El Río Cauca en estado de emergencia tras operación de Hidroituango](https://archivo.contagioradio.com/el-rio-cauca-en-emergencia-ambiental-por-sequias/)

[<time datetime="2019-01-30T16:06:56+00:00" title="2019-01-30T16:06:56+00:00">enero 30, 2019</time>](https://archivo.contagioradio.com/2019/01/30/)Habitantes de Caucasia, Antioquia expresaron alarma sobre los bajos niveles de agua en el Río Cauca.[LEER MÁS](https://archivo.contagioradio.com/el-rio-cauca-en-emergencia-ambiental-por-sequias/)  
[![Sur de Bogotá: Condenado a recibir la basura de la ciudad por 37 años más](https://archivo.contagioradio.com/wp-content/uploads/2019/01/relleno-sanitario-doña-juana-e1548870723322-770x400.jpg "Sur de Bogotá: Condenado a recibir la basura de la ciudad por 37 años más"){width="770" height="400" sizes="(max-width: 770px) 100vw, 770px" srcset="https://archivo.contagioradio.com/wp-content/uploads/2019/01/relleno-sanitario-doña-juana-e1548870723322-770x400.jpg 770w, https://archivo.contagioradio.com/wp-content/uploads/2019/01/relleno-sanitario-doña-juana-e1548870723322-770x400-300x156.jpg 300w, https://archivo.contagioradio.com/wp-content/uploads/2019/01/relleno-sanitario-doña-juana-e1548870723322-770x400-768x399.jpg 768w, https://archivo.contagioradio.com/wp-content/uploads/2019/01/relleno-sanitario-doña-juana-e1548870723322-770x400-370x192.jpg 370w"}](https://archivo.contagioradio.com/sur-bogota-basura/)  

###### [Sur de Bogotá: Condenado a recibir la basura de la ciudad por 37 años más](https://archivo.contagioradio.com/sur-bogota-basura/)

[<time datetime="2019-01-30T13:18:23+00:00" title="2019-01-30T13:18:23+00:00">enero 30, 2019</time>](https://archivo.contagioradio.com/2019/01/30/)En caso de aprobarse la propuesta de la alcaldía Peñalosa, el sur de Bogotá tendría que seguir recibiendo la basura de la Ciudad por 37 años más[LEER MÁS](https://archivo.contagioradio.com/sur-bogota-basura/)  
[![Alcalde de Jericó exige a AngloGold Ashanti suspender actividad minera](https://archivo.contagioradio.com/wp-content/uploads/2019/01/Captura-de-pantalla-2-770x400.png "Alcalde de Jericó exige a AngloGold Ashanti suspender actividad minera"){width="770" height="400" sizes="(max-width: 770px) 100vw, 770px" srcset="https://archivo.contagioradio.com/wp-content/uploads/2019/01/Captura-de-pantalla-2-770x400.png 770w, https://archivo.contagioradio.com/wp-content/uploads/2019/01/Captura-de-pantalla-2-770x400-300x156.png 300w, https://archivo.contagioradio.com/wp-content/uploads/2019/01/Captura-de-pantalla-2-770x400-768x399.png 768w, https://archivo.contagioradio.com/wp-content/uploads/2019/01/Captura-de-pantalla-2-770x400-370x192.png 370w"}](https://archivo.contagioradio.com/alcalde-jerico-exige-anglogold-ashanti-suspender-actividad-minera/)  

###### [Alcalde de Jericó exige a AngloGold Ashanti suspender actividad minera](https://archivo.contagioradio.com/alcalde-jerico-exige-anglogold-ashanti-suspender-actividad-minera/)

[<time datetime="2019-01-28T15:19:20+00:00" title="2019-01-28T15:19:20+00:00">enero 28, 2019</time>](https://archivo.contagioradio.com/2019/01/28/)El alcalde de Jericó, Antioquia firmó un acto para exigirle a la multinacional AngloGold Ashanti de suspender inmediatamente toda actividad de explotación de metales en el municipio, en cumplimiento de la prohibición de explotación minera,[LEER MÁS](https://archivo.contagioradio.com/alcalde-jerico-exige-anglogold-ashanti-suspender-actividad-minera/)  
[](https://archivo.contagioradio.com/las-mafias-tras-los-incendios-la-amazonia/)  

###### [Las mafias tras los incendios en la Amazonia](https://archivo.contagioradio.com/las-mafias-tras-los-incendios-la-amazonia/)

[<time datetime="2019-01-25T18:28:56+00:00" title="2019-01-25T18:28:56+00:00">enero 25, 2019</time>](https://archivo.contagioradio.com/2019/01/25/)Tras un incendio que calcinó 300 hectáreas de bosque en el Guaviare, organizaciones ambientales, junto con comunidades locales, están pidiendo a la Fiscalía General de la Nación de investigar la construcción ilegal de rutas en[LEER MÁS](https://archivo.contagioradio.com/las-mafias-tras-los-incendios-la-amazonia/)
