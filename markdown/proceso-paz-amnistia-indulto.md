Title: Ley de Amnistía y la libertad de las y los prisioneros políticos
Date: 2018-03-26 17:05
Category: Expreso Libertad
Tags: acuerdo de paz, Amnistia, Indulto, presos politicos
Slug: proceso-paz-amnistia-indulto
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/09/proceso_de_paz_2.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: BBC] 

###### [27 Feb 2018] 

El acuerdo de Paz pactado entre el Gobierno Nacional y la FARC, tuvo como uno de sus ejes más importantes la creación de una Ley de Amnistía, calificada como  una de las herramientas más acorde para lograr un tránsito de la ilegalidad a la vida social y colaborar a la justicia transicional, sin embargo, su proceso de trámite fue tortuoso. En el Expreso Libertad se conocerá si esta ley ha sido efectiva para la libertad de las y los prisioneros políticos o si se quedo en papel.

<iframe id="audio_25008892" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_25008892_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Encuentre más información sobre Prisioneros políticos en[ Expreso libertad ](https://archivo.contagioradio.com/programas/expreso-libertad/)y escúchelo los martes a partir de las 11 a.m. en [Contagio Radio](https://archivo.contagioradio.com/alaire.html) 
