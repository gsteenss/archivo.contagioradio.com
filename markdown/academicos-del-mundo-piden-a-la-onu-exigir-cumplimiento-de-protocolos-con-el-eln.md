Title: Académicos del mundo piden a la ONU exigir cumplimiento de protocolos con el ELN
Date: 2019-11-28 15:34
Author: CtgAdm
Category: El mundo, Paz
Tags: Academia, Derecho internacional, ELN, guerrilla, Guerrilleros, noam chomsky, ONU, paz, Perfidia, tratado de paz
Slug: academicos-del-mundo-piden-a-la-onu-exigir-cumplimiento-de-protocolos-con-el-eln
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/02/eln_0-e1517506384192.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: EFE] 

[Varios académicos de distintas nacionalidades le han pedido al **Secretario General de la Organización de las Naciones Unidas (ONU), Antonio Guterres**, que intervenga dentro de la delicada situación diplomática de Colombia hacia Cuba tras la llegada del presidente Iván Duque a la presidencia y el rompimiento de las conversaciones de paz con el Ejército de Liberación Nacional (ELN). ]

[El lingüista norteamericano Noam Chomsky, el premio nobel de paz argentino Adolfo Pérez Esquivel y  el Secretario General filipino del WorLd Student Christian Federation, Nécta Rocas Montes son algunos nombres que figuran en la carta. También han firmado organizaciones como la Comisión General de Justicia y Paz de España y el Comité Carlos Fonseca, de Roma, Italia. ]

[En la carta los académicos englobaron sus preocupaciones bajo el nombre de “Concepto referido al cumplimiento de lo pactado para las conversaciones con el ELN”, esperando que el Secretario General de la ONU se pronuncie sobre el tema.][(Le puede interesar: Organizaciones sociales piden al Consejo de Seguridad de la ONU reanudar diálogos con el ELN).](https://archivo.contagioradio.com/organizaciones-sociales-piden-al-consejo-de-seguridad-de-la-onu-reanudar-dialogos-con-el-eln/)

[La exigencia del gobierno del presidente Duque al gobierno de Cuba —solicitando la entrega de los miembros del equipo negociador del ELN— y su abstención en la votación para levantar el bloqueo económico de Estados Unidos a Cuba, propiciado en los primeros días de noviembre, ha generado una relación tensa entre ambos países, donde la comunidad internacional está **“pasivamente como espectadora”.**]

### **Académicos resaltan ante la ONU las consecuencias de las decisiones del Gobierno **

[Al dejar atrás las decisiones ejecutivas de su predecesor, el presidente Duque canceló las conversaciones de paz con el ELN después del atentado a la Escuela de Cadetes General Santander en Bogotá. Para los académicos, esta es una señal que indica que todo lo que se logró hasta el momento “está siendo atacado por intereses para los que no vale la palabra, el derecho, la coexistencia, ni mucho menos los derechos humanos y otros costosos fundamentos, necesarios para la construcción de una cultura de paz”.]

[El documento señala que: “**la ONU acompaña igualmente los esfuerzos de la sociedad civil** que pide no se menoscabe el derecho internacional para la terminación pacífica o la regulación de los conflictos”. De igual forma, se pide que “se tome nota de esta situación y sean requeridos por posibles infracciones los funcionarios del ejecutivo colombiano que encabezan esta agresión contra el derecho internacional”.][(Le puede interesar: ELN depositó acuerdos parciales de paz ante la comunidad internacional).](https://archivo.contagioradio.com/eln-acuerdos-comunidad-internacional/)

[Los académicos han criticado el poco compromiso del presidente Iván Duque con la paz y renuevan sus esfuerzos por actuar de buena fe con los implicados dentro del tratado, dejando en claro que no quieren que se haga “trizas un compromiso de Estado” ni que Colombia incite a otros Estados a “que hagan lo mismo y cometan un grave delito internacional como es la Perfidia”. ]

 

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
