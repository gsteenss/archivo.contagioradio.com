Title: Mancuso: "Colombia no conoce la verdad, porque no hubo ni existe interés político"
Date: 2020-09-07 11:00
Author: CtgAdm
Category: Actualidad, Nacional
Slug: mancuso-colombia-no-conoce-la-verdad-porque-no-hubo-ni-existe-interes-politico
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/09/carta1.jpeg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/09/carta1-1.jpeg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/09/carta2.jpeg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/09/carta3.jpeg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/09/carta4.jpeg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/09/WhatsApp-Image-2020-09-07-at-7.15.54-PM.jpeg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/09/WhatsApp-Image-2020-09-07-at-7.15.54-PM-1.jpeg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/09/WhatsApp-Image-2020-09-07-at-7.15.54-PM-2.jpeg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/09/WhatsApp-Image-2020-09-07-at-7.15.54-PM-3.jpeg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/09/WhatsApp-Image-2020-09-07-at-7.15.54-PM-4.jpeg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/09/WhatsApp-Image-2020-09-07-at-7.15.54-PM-1-1.jpeg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/09/WhatsApp-Image-2020-09-07-at-7.15.54-PM-2-1.jpeg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/09/WhatsApp-Image-2020-09-07-at-7.15.54-PM-5.jpeg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/09/WhatsApp-Image-2020-09-07-at-7.15.54-PM-1-2.jpeg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/09/WhatsApp-Image-2020-09-07-at-7.15.54-PM-2-2.jpeg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/09/WhatsApp-Image-2020-09-07-at-7.15.54-PM-3-1.jpeg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","fontSize":"small"} -->

*Foto: @LaLibertadCo*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En una carta enviada por **Salvatore Mancuso**, el pasado 3 de septiembre desde la cárcel de Irwin, en Estados Unidos al exministro Álvaro Leyva, el ex jefe paramilitar **reafirmó su compromiso con las víctimas, señalando que pese a su situación migratoria, comparecerá ante la Comisión de la Verdad en Colombia.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Esta respuesta llegó luego de que Leyva, parte del equipo negociador del Acuerdo de Paz en la Habana; **enviara una misiva el pasado 19 de agosto a Mancuso y al actual presidente del partido FARC, Rodrigo Londoño, en dónde les pedía aportar a la verdad por cualquier medio**,

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

**Londoño, respaldado por miembros del [Partido FARC](https://archivo.contagioradio.com/ataques-a-la-cidh-son-un-golpe-al-corazon-de-las-victimas/), reafirmó su compromiso hacia la verdad,** y extendieron la invitación a los demás actores responsables del conflicto armado a unirse en esta intención.

<!-- /wp:paragraph -->

<!-- wp:quote -->

> ***"Es hora de que los actores armados que hicimos y hacen parte del conflicto armado nos sentemos de forma sincera de cara al país sin permitir que la verdad sea secuestrada o manipulada"***
>
> <cite>Salvatore Mancuso| Apartado de la carta a Leyva</cite>

<!-- /wp:quote -->

<!-- wp:paragraph -->

Sin embargo**, el exjefe paramilitar indicó que en la carta de Leyva había un error, señalando que esta deja una sensación de que no ha contado la verdad,** *"eso es incorrecto, desde cuándo me desmovilice no he dejado de cumplir mis compromisos y mi único deseo es seguir haciéndolo hasta culminar los procesos transicionales".*

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### "La verdad es la verdad, sin importar a quién beneficia o perjudica", Mancuso

<!-- /wp:heading -->

<!-- wp:paragraph -->

Mancuso también expresó que pese a que lleva 15 años trabajando por la verdad ocurrida en el conflicto armado, hay quienes no les interesa que se conozca, *"**tratan de hacerme ver como si no lo hubiera hecho, esta es una estrategia de intimidación.** Es por eso que se hace indispensable y necesario que las verdades sean conocidas en tiempo real sin ediciones y sin limitaciones"*.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

También aclaró que lo que viene ocurriendo con su situación jurídica, carcelaria y migratoria, *"no es una estrategia o justificación para no contar la verdad, nada más alejado de la realidad, es absolutamente falso. **Lo único que buscó son las garantías necesarias para proteger mi vida y la de mi familia".***

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Y agregó que su compromiso con la verdad se ha venido reflejando en diferentes actos de comparecencia ante la Comisión de la Verdad. ***"Hace pocos días vivenciamos un primer acto de reconciliación con las comunidades étnicas y campesinas de Antioquia, Chocó y Córdoba**, renovando ante ellos mi compromiso de verdad reparación y no repetición"* manifestó.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Encuentros que *fueron* registrados el primero de septiembre. Uno de ellos juntó a víctimas del Urabá Antioqueño, Norte del Cauca y Bajo Atrato, en la ciudad de Apartadó, en donde sostuvieron una [comunicación vía telefónica con Mancuso](https://archivo.contagioradio.com/asi-fue-encuentro-de-dialogo-entre-victimas-y-salvatore-mancuso/), e**n un acercamiento de valoración de la voluntad de contar la verdad, reconocer responsabilidades y participar desde allí en la justicia y la reparación integral.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Este acercamiento telefónico se dio en medio de un viaje de recolección de testimonios de las víctimas y de sus iniciativas de garantías de no repetición y transición territorial de la paz, adelantada por la Comisión de la Verdad y encabezada por la comisionada Patricia Tobón y con el apoyo organizaciones como la [Comisión Intereclesial de Justicia y Paz.](https://www.justiciaypazcolombia.com/kimy-pernia/)

<!-- /wp:paragraph -->

<!-- wp:core-embed/facebook {"url":"https://www.facebook.com/watch/?v=1393939254336472\u0026extid=NAqlQHwaQilno3rv","className":""} -->

<figure class="wp-block-embed-facebook wp-block-embed">
<div class="wp-block-embed__wrapper">

https://www.facebook.com/watch/?v=1393939254336472&extid=NAqlQHwaQilno3rv

</div>

</figure>
<!-- /wp:core-embed/facebook -->

<!-- wp:paragraph -->

Mancuso también aseguró en esos encuentros que *"estoy preparado y listo para asistir a sesiones públicas conjuntas, a través del medio que sea necesario con las personas que en el pasado nos juntamos en la guerra*".

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### "En Colombia se rasgan las vestiduras exigiendo la verdad, pero hipocritamente se impide la reconstrucción de la misma"

<!-- /wp:heading -->

<!-- wp:paragraph -->

El ex jefe paramilitar señaló que, el haber participado directamente del conflicto armado interno, lo convierte en autoridad para ser parte de este proceso de **justicia transicional que busca dar cierre al conflicto armado, expresando que está más que calificado y cumple todos los requisitos para ser admitido a la JEP.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

También recordó que cuando confesó que *"**35% de miembros del Congreso eran personas apoyadas por las AUC, me trataron de loco; cuando conté acerca de la parapolítica, la negaron**: revelé que yo era la prueba viviente de lo que en ese entonces denominé paramilitarismo de Estado y me trataron de iluso".*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Igualmente, Mancuso indicó que han intentado evitar que su verdad llegué al país, ***"al utilizar medios coercitivos para intimidarme, como torturas, amenazas, persecuciones judiciales a través de los montajes, todo tratando de callarme".***

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En ese sentido contó cómo una venganza criminal para silenciarlo al revelar de forma detallada la relación del Estado y las Autodefensas Unidas de Colombia, fue extraditarlo a Estados Unidos.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Declaraciones en las que Mancuso señaló a colaboradores cercanos al gobierno de Álvaro Uribe como **Francisco Santos,** hoy embajador de Colombia en Estados Unidos; **José Miguel Narváez**, subdirector del DAS e implicado en el asesinato de [Jaime Garzón](https://archivo.contagioradio.com/otra-mirada-21-anos-sin-jaime-garzon/); **Pedro Juan Moreno**, secretario de gobierno de la Gobernación de Antioquia; **eran personas cercanas a la AUC.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Ante esto en las ultimas horas, **el embajador, Francisco Santos,** emitió un comunicado en el que asegura que Mancuso, ya lo había acusado de estar relacionado con las AUC, argumento emitido sin pruebas que permitan desmentir lo dicho por Mancuso o respaldar las palabras del embajador

<!-- /wp:paragraph -->

<!-- wp:image {"id":89483,"sizeSlug":"large"} -->

<figure class="wp-block-image size-large">
![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/09/WhatsApp-Image-2020-09-07-at-7.15.54-PM-5-759x1024.jpeg){.wp-image-89483}

</figure>
<!-- /wp:image -->

<!-- wp:image {"id":89484,"sizeSlug":"large"} -->

<figure class="wp-block-image size-large">
![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/09/WhatsApp-Image-2020-09-07-at-7.15.54-PM-1-2-764x1024.jpeg){.wp-image-89484}

</figure>
<!-- /wp:image -->

<!-- wp:image {"id":89485,"sizeSlug":"large"} -->

<figure class="wp-block-image size-large">
![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/09/WhatsApp-Image-2020-09-07-at-7.15.54-PM-2-2-765x1024.jpeg){.wp-image-89485}

</figure>
<!-- /wp:image -->

<!-- wp:image {"id":89486,"sizeSlug":"large"} -->

<figure class="wp-block-image size-large">
![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/09/WhatsApp-Image-2020-09-07-at-7.15.54-PM-3-1-758x1024.jpeg){.wp-image-89486}  

<figcaption>
Carta completa de Salvatore Mancuso

</figcaption>
</figure>
<!-- /wp:image -->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
