Title: Luis Enrique de la Cruz, integrante de la comunidad Senú fue asesinado en Cáceres
Date: 2019-11-06 17:56
Author: CtgAdm
Category: DDHH, Nacional
Tags: Antioquia, Asesinato de indígenas, bajo cauca antioqueño
Slug: luis-enrique-de-la-cruz-integrante-de-la-comunidad-zenu-fue-asesinado-en-caceres
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/Zenú.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/Zenú-1.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio ] 

Según información proveniente de la Organización Indígena de Antioquia (OIA), **Luis Enrique de la Cruz Suárez** de 43 años, parte de la Guardia Indígena de Cáceres y perteneciente de la comunidad Senú, fue asesinado en la madrugada de este 6 de noviembre en la vía que conduce de este municipio a la vereda El Tigre en el Bajo Cauca Antioqueño, con su homicidio, son 135 los integrantes de pueblos ancestrales asesinados durante el Gobierno Duque.

Según lo establecido por el Consejo de Gobierno Mayor, Luis no tenía amenazas en su contra de ningún tipo y al igual que su familia se dedicaba a la agricultura al interior de su comunidad. [(Lea también: En el Bajo Cauca Antioqueño la violencia se está tomando el poder)](https://archivo.contagioradio.com/bajo-cauca-antioqueno-violencia-gobierno/)

Según Alexis Espitia, consejero de DD.HH. de la OIA, los móviles del homicidio apuntan a que el guardia no habría acatado el toque de queda declarado por los grupos armados ilegales presentes en la zona, como los Caparrapos o las autodenominadas Autodefensas Gaitanistas de Colombia,  quienes estarían exigiendo a la comunidad Senú, no movilizarse por las calles después de las 6 de la tarde, limitando la cotidianidad de las comunidades.

"Hacemos un llamado a que el Gobierno garantice la vida de las comunidades y los dirigentes indígenas" expresó el consejero quien también pidió se realice un acompañamiento a los pueblos ancestrales para evitar posibles desplazamientos en el futuro.

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
