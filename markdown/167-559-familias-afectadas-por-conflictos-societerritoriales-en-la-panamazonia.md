Title: 167.559 familias afectadas por conflictos societerritoriales en la Panamazonía
Date: 2020-09-24 18:46
Author: PracticasCR
Category: Actualidad
Tags: Bolivia, colombia, Panamazonía
Slug: 167-559-familias-afectadas-por-conflictos-societerritoriales-en-la-panamazonia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/09/atlas-de-conflictos.png" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/09/conflictos-en-la-panamazonia.png" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/09/familias-conflictos-en-la-panamazonia.png" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","className":"has-cyan-bluish-gray-color has-text-color","fontSize":"small"} -->

Foto: Archivo

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Varias entidades, entre ellas La Comisión Pastoral de la Tierra (CPT) y el Grupo de Investigación y Extensión sobre Tierra y Territorio en la Amazonia (Gruter), presentaron el **[«atlas de conflictos socioterritoriales](http://www.forosocialpanamazonico.com/paises-de-la-pan-amazonia-presentan-atlas-de-conflictos-socioterritoriales/)»** del área de la Panamazonía, con datos de Bolivia, Brasil, Colombia y Perú durante el 2017-2018 .

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En la investigación, se registran 1.308 conflictos socioterritoriales activos, muchos hasta el día de hoy, durante estos dos años. **Brasil es el país en que más conflictos se registraron (995), seguido por Colombia (227), Perú (69) y finalmente Bolivia (17).**

<!-- /wp:paragraph -->

<!-- wp:image {"id":90395,"sizeSlug":"large"} -->

<figure class="wp-block-image size-large">
![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/09/conflictos-en-la-panamazonia.png){.wp-image-90395}  

<figcaption>
Número de conflictos sociterritoriales en la Panamazonía.

</figcaption>
</figure>
<!-- /wp:image -->

<!-- wp:paragraph -->

Durante este periodo, más de 167.559 familias de la Panamazonía se han visto afectadas por los conflictos. En Brasil 131.309 familias se han visto afectadas, en Colombia 7.040, en Perú 27.279 y finalmente en Bolivia 1.931 familias afectadas.

<!-- /wp:paragraph -->

<!-- wp:image {"id":90396,"sizeSlug":"large"} -->

<figure class="wp-block-image size-large">
![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/09/familias-conflictos-en-la-panamazonia.png){.wp-image-90396}  

<figcaption>
Familias afectadas en cada país.

</figcaption>
</figure>
<!-- /wp:image -->

<!-- wp:paragraph -->

Además, **en estos dos años, se registraron 118 asesinatos a líderes y miembros de las comunidades.** Siendo siete los asesinatos a mujeres lideresas y defensoras de la Amazonía. (Le puede interesar: [Asesinatos contra líderes sociales se incrementaron un 85%: MOE](https://archivo.contagioradio.com/asesinatos-contra-lideres-sociales-se-incrementaron-un-85-moe/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En el atlas, las comunidades que se encuentran en los territorios se agruparon en cuatro grandes grupos: indígenas, campesinos*,* afrodescendientes y pequeños agricultores.

<!-- /wp:paragraph -->

<!-- wp:heading -->

Brasil
------

<!-- /wp:heading -->

<!-- wp:paragraph -->

«En Brasil, más del 42% de los conflictos envolvieron pequeños agricultores de los frentes de emigración y de colonización», posteriormente indígenas (17%), campesinos (29%) y afrodescendientes (11%). Estos grupos juntos representan más de la mitad de los conflictos (528). 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

**Por otro lado, en Brasil se registraron 80 asesinatos (seis a mujeres), 100 intentos de asesinato, 225 amenazas de muerte, 115 agresiones de diversos tipos y 351 personas fueron detenidas, presas o sometidas a procesos judiciales por causa de la defensa de sus territorios.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

De acuerdo con el atlas, en este país 12% de los grupos en conflicto no tienen tierras y están fuera de sus territorios tradicionales, o en situación de desalojado.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

«En números generales, en la Panamazonía, pesa más la situación de Brasil, donde la mayoría de territorios en conflicto (59%) son tierras sin legalización y/o con falta de titulación legal: comunidades tradicionales e indígenas sin su territorio reconocido y demarcado, o áreas de ocupación sin reconocimiento legal». 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Uno de los conflictos que predominan en la Amazonía es el agronegocio, es decir, ganadería y monocultivos, representando en Brasil el 60% de las causas registradas de conflictos

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Otra causa de conflicto, son las construcciones de Hidroeléctricas que representan 5% del total general, la mayor parte en Brasil.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

También, el Atlas registra violencia contra bienes, especialmente la tomada de tierras de la comunidades, sumando 401 desalojos de tierras, y 380 casos de destrucción de casas y otros bienes. En Brasil hubo la mayor parte de las destrucciones y también de los desalojos: «375, se dieron por decisiones judiciales o por expulsiones a la fuerza».

<!-- /wp:paragraph -->

<!-- wp:heading -->

Colombia
--------

<!-- /wp:heading -->

<!-- wp:paragraph -->

En Colombia, 27 conflictos se registraron por la plantación de cultivos ilícitos, aun así, no fue la principal causa de conflictos. La causa principal fue la construcción de infraestructuras de transporte (carreteras, puentes) que alcanzan un número de 90 conflictos. 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Sin embargo, en Colombia (84%) una de las situaciones territoriales de conflicto que predomina son los problemas ambientales.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

**Con respecto a los asesinatos, la cifra aumentó después a la firma del acuerdo de paz, correspondiendo al 10% de la superficie Panamazónica.** En el mismo período, hubo nueve muertes en la región amazónica peruana y ninguna registrada en Bolivia.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Respecto a desalojos forzados y destrucción de bienes, en el atlas se señala que hubo 22 casos.

<!-- /wp:paragraph -->

<!-- wp:heading -->

Perú
----

<!-- /wp:heading -->

<!-- wp:paragraph -->

En Perú, la mayoría de los conflictos registrados (78%) han afectado a pueblos indígenas.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

La Minería representa el 4,9% de las causas de conflictos en el total de la panamazonía. Particularmente, en Perú, 22 casos de conflictos son debido a la minería y 18 a la extracción de petróleo o de gas, siendo las principales causas de los conflictos, 56% del total. 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Así como Colombia y Bolivia, en Perú (50%) otra de las situaciones territoriales de conflicto que predominan son los problemas ambientales.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

«**Del número total de conflictos, a 50 se los considera provocados por lo que se suponía ser la solución de la Amazonía, la llamada "economía verde"».** Estos se generaron por problemas que inicialmente buscaban ser soluciones ambiental «como la creación de parques de conservación ambiental, en choque con las comunidades que los habitan». 

<!-- /wp:paragraph -->

<!-- wp:heading -->

Bolivia
-------

<!-- /wp:heading -->

<!-- wp:paragraph -->

En el caso de Bolivia, los indígenas y campesinos representan la totalidad de los grupos afectados y el 25% de los territorios tienen problemas de legalización; otros 25% sufren invasiones y avasallamiento.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

**Asimismo, en Bolivia (34%), las situaciones territoriales de conflicto que predominan son los problemas ambientales, como derrames de químicos.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En cuanto a desalojos y destrucción de bienes, en Bolivia también hubo 19 desalojos y 14 destrucciones de bienes.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Josep Iborra, de la CPT señaló que el Atlas nos permite tener una visión que unifique nuestra perspectiva sobre lo que está pasando en toda la Amazonía ...Así crear redes para fortalecer el enfrentamiento y la resistencia de los mismos problemas, por aquellos que luchan por sus tierras y territorios".

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
