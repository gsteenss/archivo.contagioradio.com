Title: Denuncian creciente presencia paramilitar en el municipio del Castillo en Meta
Date: 2016-03-02 13:07
Category: DDHH, Nacional
Tags: Alto Ariari, Castillo, Meta, Paramilitarismo
Slug: denuncian-creciente-presencia-paramilitar-en-el-municipio-del-castillo-en-meta
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/02/PARAMILITARES-cordoba.gif" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: archivo] 

###### [02 Mar 2016] 

Durante el mes de febrero**[la presencia paramilitar se ha incrementado en el municipio del Castillo en el departamento del Meta](https://archivo.contagioradio.com/?s=paramilitares)**, una de las regiones más golpeadas por los operativos paramilitares que se desarrollaron desde 2002. Según las denuncias los paramilitares han hecho presencia en establecimientos públicos del casco urbano y han sido vistos en la carretera de conduce de Medellín del Ariari al caserío de Puerto Esperanza.

Uno de los paramilitares vistos es el conocido con el alias de “care garra” en compañía de 2 hombres fuertemente armados. El paramilitar fue visto el pasado 28 de Febrero en un establecimiento público en **el casco urbano que mantiene presencia militar y policial de manera permanente.**

Como en el caso del Putumayo, las autoridades militares y de policía se empeñan en negar la presencia o la existencia de grupos paramilitares. Según la denuncia de los pobladores y de la Comisión de Justicia y Paz que hace presencia en la zona, ante la afirmación de que el 24 de Febrero fueron vistos por lo menos 10 hombres de civil y con armas largas “el Teniente Coronel Enrique Alonso Álvarez Hernández, **comandante del Batallón de Infantería respondió, que  no se trataba de ningún grupo armado, solo eran 10 delincuentes**”.

Adicionalmente el 1 de Marzo se encontró el cuerpo de un hombre que no fue reconocido por los habitantes del municipio en la vereda Malabar, ubicada a 5 minutos de la cabecera municipal. Los pobladores afirman que podría tratarse de una persona que fue trasladada hasta el lugar para ser asesinado por los paramilitares. Hasta el momento se desconoce la identidad de la persona así como los móviles del asesinato.

En la región también se ha incrementado la presencia militar, que según el comandante del Batallón obedece a la construcción de obras de infraestructura en el municipio. Los pobladores del Alto Ariari afirman que este tipo de **acciones de tipo paramilitar podrían provocar asesinatos y un nuevo desplazamiento** en caso de que no se tomen medidas para desmontar esas estructuras.
