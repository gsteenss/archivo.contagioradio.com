Title: Asesinato de Jesús María Valle, masacres del Aro y La Granja tienen características de crimen de lesa humanidad
Date: 2018-02-28 15:00
Category: DDHH, Nacional
Tags: alvaro uribe velez, Jesús María Valle, Masacre de La Granja, Masacre del Aro
Slug: asesinato-de-jesus-maria-valle-masacres-del-aro-y-la-granja-tienen-caracteristicas-de-crimen-de-lesa-humanidad
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/10/vigilia-por-la-paz.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [28 Feb 2018] 

La Corte Suprema de Justicia tendría todos los argumentos para declarar la masacre Del Aro, de la Granja y el asesinato del abogado defensor de derechos humanos Jesús María Valle, como crímenes de lesa humanidad, así lo ratificó Ramiro Bejarano, abogado defensor de derechos humanos, que además expresó, **que los tres actos cuentan con las características de sistematicidad, ser un acto brutal y aniquilar a personas por un interés particular**.

Referente a las investigaciones que vinculan a el expresidente Álvaro Uribe con estos hechos y la posibilidad de que se llegué a un juicio, Bejarano manifestó que es muy prematuro hablar de que pueda existir un juicio. (Le puede interesar:["La telaraña de falsos testigos y colaboradores de Álvaro Uribe Vélez"](https://archivo.contagioradio.com/la-telarana-de-falsos-testigos-y-colaboradores-de-uribe-velez/))

En ese sentido afirmó que hay elementos importantes para concluir que una investigación bien conducida puede despejar la duda o debe confirmar la inquietud, pero no puede quedarse quieto, además aseveró que con la investigación que hizo la **Corte Suprema en donde fueron develados los audios de Uribe, se ratificó que hay una independencia política**.

Sin embargo, considera que afirmaciones hechas en la Revista Semana en el artículo en donde se afirmaría que llamar a indagatoria a Álvaro Uribe puede generar un problema de orden público, es inadmisible en la democracia.

“Con ese criterio nunca se habría podrido juzgar en Chile a Pinochet, ni se hubiera podido juzgar a Fujimori en Perú, tampoco en Estados Unidos, cuando calló Nixon se habría podido adelantar un juicio, ese es un mensaje que francamente no representa a todos los colombianos” afirmó Bejarano. (Le puede interesar: ["Uribe a investigación por presunta manipulación de testigos contra Iván Cepeda"](https://archivo.contagioradio.com/uribe-testigos-cepeda/))

<iframe id="audio_24134763" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_24134763_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
