Title: Más de 300 barrios de Ciudad Bolívar tienen problemas de movilidad
Date: 2017-10-18 13:50
Category: DDHH, Nacional
Tags: Bogotá, ciudad bolivar, crisis de movilidad, movilidad en ciudad bolívar
Slug: mas-del-60-de-ciudad-bolivar-tiene-problemas-de-movilidad
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/10/diego-pinto.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Twitter Diego Pinto] 

###### [18 oct 2017] 

Tras la suspensión por un día del servicio del Sistema Integrado de Transporte Urbano en la localidad de Ciudad Bolívar en Bogotá, el 11 de octubre de 2017, **volvió a ponerse sobre la mesa la precaria situación de movilidad** en Ciudad Bolívar. Sus líderes denuncian que hay por lo menos 300 barrios que no tienen ni buses ni vías para movilizarse.

Si bien las rutas alimentadoras volvieron a la normalidad, **más del 60% de la localidad** no tiene vías y medios de transporte para comunicarse con el resto de la ciudad. Por ello la travesía de un habitante es mayúscula. Por lo menos 2 horas dura un recorrido desde la parte alta hasta el centro de la ciudad y el costo de esa movilización puede llegar a los 5000 pesos. (Le puede interesar: ["Transporte público de Bogotá puede ser suspendido por crisis de 4 empresas"](https://archivo.contagioradio.com/transporte-publico-de-bogota-puede-ser-suspendido-por-crisis-de-4-empresas/))

### **Rutas alimentadoras funcionan pero no aumentan y son insuficientes** 

De acuerdo con Lindon Arévalo, miembro de la Mesa Local de Paz de Ciudad Bolívar, la situación de movilidad en Ciudad Bolívar ha sido desatendida, “sobre todo en las partes altas de la localidad donde todos los días es complicada la movilidad”. Las rutas habituales han seguido funcionando, **pero no han aumentado** y “no hay conciencia sobre la situación por parte de los operadores de Transmilenio”.

Arévalo indicó que las personas, que necesitan hacer uso del sistema de buses para llegar a diferentes puntos de la ciudad, **“se tienen que movilizar como sardinas porque no hay transporte”**. Manifestó que "la administración distrital no ha tenido en cuenta a estas personas como seres humanos que tienen derecho a hacer uso de un sistema de transporte adecuado y digno".

### **Las propuestas de solución que tienen las comunidades** 

Los habitantes de las tres montañas que comprenden la localidad de Ciudad Bolívar, han manifestado en reiteradas ocasiones que **requieren un sistema de metro cable** que contribuya con la movilidad y la conexión de esa localidad con el resto de la ciudad. Si bien van a inaugurar este sistema en la zona de El Paraíso, “aún se requieren 3 más”. (Le puede interesar: ["Usme y Tunjuelito podrían ser afectadas con suspensión de SITP"](https://archivo.contagioradio.com/ciento-sesenta-mil-personas-afectadas-por-suspension-de-servicio-de-sitp-en-ciudad-bolivar/))

Arévalo manifestó que **son más de 300 barrios los que se encuentran afectados** por la falta de vías de ingreso y comunicación con el resto de la ciudad. Además, como cualquier ciudadano, hacen el pago de impuestos requerido que no ven reflejado en la construcción de vías de acceso y mucho menos transporte público.

Ante esto, los habitantes han continuado realizando reuniones y concentraciones **para discutir los problemas más importantes de la localidad.** Recuerdan que aún se encuentran en el marco del Paro del Sur y están abogando por la presencia de la comunidad para crear foros y asambleas para darle a entender a la administración los múltiples problemas que tienen más de 800 mil personas en Ciudad Bolívar.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
