Title: Paramilitares de AGC intimidan comunidad de paz y se asientan en territorio colectivo
Date: 2016-04-18 15:59
Category: DDHH, Nacional
Tags: Autodefensas Gaitanistas de Colombia, comunidades de paz, paramilitares
Slug: paramilitares-de-agc-intimidan-comunidad-de-paz-y-se-asientan-en-territorio-colectivo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/04/paramilitares_agc-contagioradio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Comunidad de paz 

###### [18 Abr 2016]

El domingo 17 de abril, varias paredes de viviendas, sitios de trabajo y vallas de la comunidad de Paz de San José de Apartado amanecieron con **graffitis amenazantes de las [Autodefensas Gaitanistas de Colombia](https://archivo.contagioradio.com/aumenta-temor-por-amenaza-de-autodefensas-gaitanistas-a-defensores-de-ddhh/)**, situación que preocupa a pobladores dado el alto nivel de militarización de la zona y la permanencia de una base militar del ejercito y una estación de policía a escasos metros de la comunidad de paz.

Los campesinos de la comunidad de Paz de San José de Apartado anunciaron  que para este lunes 18 de abril una comisión representativa se dirigirá al despacho del alcalde municipal de Apartadó, Eliécer Arteaga, para manifestarle la situación y para pedirle al mandatario municipal que informe sobre las acciones que  se adelantarán para proteger a los habitantes.

Por otro lado, en el territorio colectivo de Cacarica, en la comunidad de Teguerre se conoció información en donde se estima que unos **200 hombres armados estarían transitando en la comunidad.** Esta sería la segunda vez  en que las Autodefensas Gaitanistas de Colombia hacen presencia en este territorio.

[Los pobladores informaron que el pasado 15 de abril, los paramilitares confinaron a la comunidad ordenandoles no salir](https://archivo.contagioradio.com/comunidades-atemorizadas-por-paro-de-autodefensas-gaitanistas/)a trabajar  y les prohibieron comunicarse por celular, también se conoció que este mismo grupo irrumpió en la vivienda de un habitante de la comunidad y hurtaron ropa de su esposa y un caballo.

También se reporto un asentamiento de grupos paramilitares el 16 de abril en el territorio Colectivo de Salaquí al que se accede a través de río, altamente patrullado y controlado por el batallón fluvial.  A su vez se estima que **700 hombres de las Autodefensas Gaitanistas de Colombia estarían asentados en lugares conocidos como  Regadero, Isletas, Playa Aguirre y Bocas de Tamboral.**

Recientemente las AGC realizaron un paro armado en la región, en el que demostraron que ejercen el control de varios municipios y presencia en varios departamentos. a su vez, organizaciones de DDHH como el CINEP o la organización Somos Defensores han emitido informes en los que dan cuenta de la presencia de paramilitares.

Por su parte, el gobierno nacional, en cabeza del Ministro de Defensa, Luis Carlos Villegas, han confluido en afirmar que el fenómeno al que se refiere gran parte de las organizaciones son ["un fantasma"](https://archivo.contagioradio.com/es-fantasioso-asegurar-que-el-paramilitarismo-no-sigue-vigente/)lo que **imposibilita la toma de acciones urgentes de desmonte de este fenómeno.**

Además la CIDH al finalizar su periodo de sesiones 157, recomendó al Estado Colombiano que se tomen todas las medidas necesarias para el [desmonte y desarticulación de los grupos paramilitares](https://archivo.contagioradio.com/audiencia-persistencia-del-paramilitarismo-en-colombia/), a los cuales considera un **obstáculo para el proceso de paz y una amenaza constante para el trabajo de los defensores de DDHH en Colombia.**

###### Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)] 
