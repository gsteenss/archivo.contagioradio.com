Title: Atentan contra Francia Márquez y otros líderes de comunidades negras del Norte del Cauca
Date: 2019-05-04 18:40
Author: CtgAdm
Category: Líderes sociales, Nacional
Tags: Cauca, Francia Márquez, lideres sociales
Slug: atentan-contra-lideresa-francia-marquez
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/francia.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/WhatsApp-Image-2019-05-04-at-6.40.03-PM-1.jpeg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/WhatsApp-Image-2019-05-04-at-6.40.03-PM.jpeg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Cauca extremo 

En horas de la tarde de este sábado la lideresa **Francia Márquez resultó ilesa tras un atentado ocurrido mientras se encontraba en una reunión de líderes en Santander de Quilichao**, comunidad de Lomitas, Finca La trinidad. Al lugar llegaron personas armadas que empezaron a disparar y lanzaron dos granadas. Los atacantes fueron repelidos por los esquemas de protección de los líderes, resultando dos escoltas heridos.

Según la información que se ha conocido la reunión se encontraban desde las 8 de la mañana **16 personas entre ellos una menor de edad**. Además de los esquemas de protección, afuera del espacio se encontraban dos personas sospechosas que lanzaban improperios contra los participantes, hasta que un tercer individuo montado en una motocicleta empezó a disparar con arma corta y luego fueron arrojados dos artefactos explosivos.

En la reunión preparatoria de la cita fijada con el gobierno para el próximo 8 de mayo, en el marco de la Minga, se encontraban además de Márquez, **Clemencia Carabalí, Sofía Garzón, Carlos Rosero, Víctor Moreno** y otros líderes sociales y defensores de DDHH que hacen parte de ACONC, así como integrantes de ASOM, PCN, cuando fueron atacados cerca de las 5:35 de la tarde.

![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/WhatsApp-Image-2019-05-04-at-6.40.03-PM-1-300x225.jpeg){.size-medium .wp-image-66369 .alignnone width="300" height="225"}![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/WhatsApp-Image-2019-05-04-at-6.40.03-PM-300x225.jpeg){.size-medium .wp-image-66370 .alignnone width="300" height="225"}

Los lideres y sus esquemas de seguridad están saliendo del lugar para proteger sus vidas. Según reportan físicamente se encuentran en buenas condiciones aunque psicologicamente afectados. Por su parte la Defensoría del Pueblo se pronunció en rechazo contra el atentado y exigió garantías para que estos defensores de derechos humanos puedan reunirse libremente y trabajar por sus comunidades.

En desarrollo...
