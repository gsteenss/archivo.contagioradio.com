Title: Piden a la UE frenar las violaciones a DDHH por parte de empresas palmeras
Date: 2016-05-06 17:33
Category: Ambiente, Nacional
Tags: aceite de palma, Londres, Monocultivos, Unión europea
Slug: indonesia-peru-colombia-y-liberia-pidieron-a-la-ue-frenar-la-violencia-por-parte-de-empresas-palmeras
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/08/Captura-de-pantalla-2015-08-11-a-las-16.51.09.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Entre el agua y el aceite de palma 

###### [6 May 2016]

Entre el 25 de abril y el 4 de mayo delegados de Indonesia, Perú, Colombia y Liberia, entre otros países exigieron en Londres que la Unión Europea ponga fin a las violaciones de derechos humanos y al acaparamiento de tierras vinculados a las empresas de palma de aceite, que en su mayoría consume la UE.

“Necesitamos que la comunidad mundial entienda que cuando consumen aceite de palma y biocombustibles están consumiendo la sangre de nuestros pueblos en Indonesia, Liberia, Colombia y Perú. Las violaciones de derechos humanos están siendo realizadas por una industria que se encuentra en expansión debido a la demanda de aceite de palma y de bioenergía por parte de la Unión Europea”, sostuvo durante su intervención Agus Sutomo, director de la ONG basada en Pontianak LinkAR-Borneo de Indonesia.

Esta declaración, como las los demás líderes indígenas y campesinos, demostraron que a pesar de que se ha intentado regular la industria del aceite de palma las empresas están generando prácticas que violan los derechos ambientales, culturales y económicos de las comunidades.

Además de la presentación de testimonios, la delegación estuvo en una refinería de aceite de palma en el puerto de Rotterdam y a Canary Wharf, en Londres. De acuerdo con el reporte de la Comisión de Justicia y Paz, estas acciones fueron tomadas para informar a los responsables de la toma de decisiones y permitir que los delegados sean testigos de primera mano del alcance de la cadena de suministro de aceite de palma en Europa.

Desde Colombia, William Aljure, el defensor de derechos humanos y representante de las Comunidades Construyendo Paz en los Territorios, CONPAZ,  hablo sobre el caso puntual que viven las comunidades étnicas y campesinas en Mapiripán, Meta con la presencia de la empresa palmera Poligrow, refiriéndose no solo a los daños ambientales sino también al paramilitarismo que ha sembrado terror en la región.

“Nuestra Madre Tierra está llorando por la violación de los derechos de nuestros pueblos y la destrucción de nuestro medioambiente. Primero a manos de los grupos paramilitares y ahora por el sufrimiento que está causado la industria del aceite de palma”, dijo Aljure, quien hizo un llamado a la solidaridad internacional para exigir que las inversiones perjudiciales y las operaciones de las plantaciones en el sector del aceite de palma que afectan a las comunidades indígenas y locales sean investigadas y debidamente castigadas, incluso por las injusticias históricas. “No se pueden separar los derechos humanos del daño ambiental”, expresó.

Por su parte, Robert Guimaraes Vásquez, miembro del pueblo indígena Shipibo-Conibo de la Amazonía peruana habló sobre la situación en su territorio y solicitó que las entidades reguladoras financieras remuevan a la United Cacao Ltd SEZC del registro del Mercado de Inversiones Alternativas (AIM) en la Bolsa de Valores de Londres, pues en la Amazonía se deforestaría de forma ilegal al menos 11.100 hectáreas, afectando gravemente a los pueblos indígenas y al ambiente.

Las denuncias se realizaron frente a miembros del Parlamento Europeo y los directores generales de medio ambiente, comercio, energía, clima y la cooperación al desarrollo de la Comisión Europea, así como ante los miembros del gabinete de comisionarios, para ejercer presión por una regulación más estricta de la UE sobre las cadenas de suministro del aceite de palma.
