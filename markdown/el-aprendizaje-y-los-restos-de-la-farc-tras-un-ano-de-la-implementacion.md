Title: Aprendizajes y retos para las FARC tras un año del acuerdo de paz
Date: 2017-11-24 13:21
Category: Nacional, Paz
Tags: acuerdos de paz, Implementación
Slug: el-aprendizaje-y-los-restos-de-la-farc-tras-un-ano-de-la-implementacion
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/01/Farc-en-Yari.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [24 Nov 2017] 

Se cumplieron 365 días de la firma de los Acuerdos de Paz en el Teatro Colón, un año que de acuerdo con la integrante del partido político FARC, Victoria Sandino, ha significado retos, **aprendizajes y conocimientos para trazar el camino de lo que será la propuesta política de este nuevo partido**.

“**Hemos puesto toda nuestra experiencia para construir una paz estable y duradera, una paz con justicia social e incluyente**” afirmó Sandino al evaluar cómo ha sido el trabajo que se desarrolló en este último año de cara a la implementación de los Acuerdos de Paz. (Le puede interesar: ["A un año de la firma de los Acuerdos en el Teatro Colón son muchos los retos para el 2018"](https://archivo.contagioradio.com/49340/))

### **Los aprendizajes del camino hacia la Paz** 

Victoria Sandino afirmó que durante esos primeros 365 días, los integrantes de la FARC han tenido que aprender a **elaborar rutas que cumplan con lo pactado en La Habana**, lo que los ha llevado a comprender las políticas públicas y planificación en los territorios. (Le puede interesar: ["JEP sería aprobada al finalizar esta semana"](https://archivo.contagioradio.com/49367/))

“Hemos aprendido muchísimo, hay una elaboración de documentos de manera permanente, es una discusión constante con el gobierno, debido a que lo **más fácil debería ser implementar la carta de navegación, pero la posición es otra”** y agregó que de las cosas más importantes que se han modificado desde la firma de los acuerdos, es el diálogo con otras organizaciones y aquellos con quienes antes no podían interlocutar.

### **Las dificultades a un año de la implementación de los Acuerdos de Paz** 

Para Sandino las dificultades que han tenido que afrontar los Acuerdos de Paz radican en mayor medida en los incumplimientos por parte del Gobierno de Juan Manuel Santos y la renegociación que se está dando en el Congreso de la República.

“Es triste y lamentable lo que está ocurriendo en el Congreso, después del plebiscito hubo una renegociación del acuerdo, en La Habana estuvieron congresistas, personas de la coalición de gobierno, personas de la oposición que expresaron que garantizaban por parte del Congreso su compromiso absoluto con lo pactado y **que tramitarían de manera expedita las normas, y lo que ha ocurrido es otra cosa completamente distinta**” afirmó Sandino.

Frente a este panorama Victoria Sandino manifestó que es necesaria una mayor actividad por pare de la ciudadanía y el movimiento social **“necesitamos exigir al gobierno que, en el poco tiempo que le queda, debe cumplir lo pactado”**.

Otra de las dificultades ha sido la unión de las fuerzas políticas que hacen parte de la izquierda y que aún no han planteado una estrategia conjunta para las elecciones del 2018, situación que, para Sandino, **debe ser urgente que se defina a partir de la construcción de una agenda y una campaña distinta que involucre a la ciudadanía y los anhelos de paz**.

<iframe id="audio_22334541" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_22334541_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
