Title: Las peores prácticas de inteligencia militar nunca se fueron
Date: 2020-01-15 06:00
Author: AdminContagio
Category: DDHH, Nacional
Tags: Binci, chuzadas, das, ejercito, Inteligencia Militar
Slug: las-peores-practicas-de-inteligencia-militar-nunca-se-fueron
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/01/inteligencia-militar.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:group -->

<div class="wp-block-group">

<div class="wp-block-group__inner-container">

<!-- wp:paragraph {"align":"justify"} -->
</p>
El reciente escándalo sobre las chuzadas que de acuerdo con revista Semana realiza la inteligencia militar, no es el primer hecho que se descubre en Colombia sobre el uso de tecnologías aplicadas para seguimientos, persecuciones e incluso violaciones a derechos humanos en contra de periodistas, políticos o integrantes del Movimiento Social. Por el contrario, es una práctica bastante común y reiterativa de las Fuerzas militares constituida como la doctrina del enemigo interno.

<p>
<!-- /wp:paragraph -->

</div>

</div>

<!-- /wp:group -->

<!-- wp:heading {"level":3} -->

### **Los años 60**, el BINCI y la inteligencia militar

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

En un reciente informe entregado a la Comisión de la Verdad, titulado "**BINCI y Brigada 20**: **El rol de inteligencia militar en los crímenes de Estado y la construcción del enemigo interno** (1977-1998)" organizaciones defensoras de derechos humanos expusieron las conductas criminales llevadas a cabo por los integrantes del Batallón de Inteligencia y Contrainteligencia Brigadier General Ricardo Charry Solano (BINCI-Charry Solano) y la Brigada 20 del Ejército Nacional.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Los 90 casos recopilados en el informe ocurrieron entre 1977 a 1998 y evidencian una persecución por parte del Estado a un grupo de personas, bajo motivaciones políticas, a partir de acciones violentas como** la desaparición forzada, la tortura y ejecuciones extrajudiciales.** Estas actuaciones eran precedidas por interceptaciones teléfonicas, persecuciones, seguimientos e infiltraciones de agentes estatales a diversos grupos u organizaciones pertenecientes a corrientes de oposición.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Además, según el informe, esas prácticas se encontraban sustentadas bajo la doctrina del enemigo interno, "un eje fundamental para la planeación y ejecución de una política de persecución y eliminación» en contra de sectores de **pensamiento alternativo, oposición o movilización social**, que no estaban de acuerdo con el gobierno de turno. (Le puede interesar: ["Informe relata el rol de la inteligencia militar en los crímenes del Estado"](https://archivo.contagioradio.com/informe-relata-el-rol-de-la-inteligencia-militar-en-los-crimenes-de-estado/))

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### La década de los 90S y la Brigada 20

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

El Batallón de Inteligencia y Contrainteligencia Brigadier General Ricardo Charry Solano operó a nivel nacional en Colombia  desde 1962 hasta 1985, cuando se amplía y surge la Brigada 20, que cumplía también con las tareas de inteligencia militar.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Esta institución Castrense tampoco escapó a las violaciones a derechos humanos. De hecho, organizaciones como Amnistía Internacional habían señalado que para 1998, la inmensa mayoría de las violaciones de los derechos humanos -según informes confirmados- fueron cometidos por «agentes estatales o fuerzas paramilitares que actúan con su aquiescencia o complicidad» en referencia a la Brigada 20.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Asimismo, el entonces embajador de Estados Unidos, Myles Frechette denunció que al interior de la unidad militar existirían escuadrones de la muerte, mientras que otras plataformas alertaron la creación de grupos de justicia privada, desapariciones y ejecuciones extrajudiciales.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

De igual forma, en el documento entregado a la CEV, se evidencia como se creó todo un aparato organizado del Estado que contó con **diferentes conexiones con instituciones gubernamentales o sectores civiles,** para desarrollar operaciones sicológicas, de hostigamientos, desaparición o ejecución. La Brigada 20 finalmente fue disuelta en noviembre de 1998, tras las denuncias de Estados Unidos, sobre las violaciones a derechos humanos cometidas por la unidad.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Los escándalos del D.A.S

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Si bien el Departamento Administrativo de Seguridad no obedece a las Fuerzas Armadas sino al gobierno nacional, esta ha sido otra de las instituciones puestas en cuestión, debido a los distintos escándalos en los que ha estado inmersa, principalmente por el uso de sus instalaciones, personal y tecnología para el espionaje a periodistas, defensores de derechos humanos, políticos y organizaciones sociales durante el mandato de Álvaro Uribe.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Entre los hecho más relevantes se encuentra la creación del G-3 un grupo conformado por 15 agentes, que desde el 2003 al 2005, se encargó puntualmente de espiar organizaciones defensoras de derechos humanos como el Colectivo de Abogados José Alvear Restrepo, la Consultoría para los Derechos Humanos y el Desplazamiento (Codhes), el Centro de Investigación y Educación Popular (Cinep) y la Asociación de Familiares de Detenidos-Desaparecidos (Asfades).

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Además el entonces director de esta institución, Jorge Noguera, fue condenado a 28 años de cárcel por su responsabilidad en el plan para asesinar al sociólogo y **profesor universitario Alfredo Correa de Andreis, **cometido el 17 de septiembre de 2004 en Barranquilla y orquestado por el jefe paramilitar por Rodrigo Tovar, alias "Jorge 40".

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Posteriormente, en septiembre de 2017, Noguera recibió otra condena por su participación en las "chuzadas", en donde el entonces director, **ordenó la creación de un grupo secreto que hizo seguimientos e interceptaciones telefónicas** a abogados, periodistas como Daniel Coronel, defensores de derechos humanos y dirigentes políticos, entre los que se encontraban Piedad Córdoba, Gustavo Petro, Iván Cepeda, entre otros.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

**María del Pilar Hurtado**, quien también fue directora del D.A.S, al igual que su antecesor se encuentra en la cárcel por su responsabilidad en los seguimientos e interceptaciones ilegales que se adelantaron en contra de magistrados de la **Corte Suprema de Justicia,** y una campaña de desprestigio hacia al magistrado Iván Velasquez que para ese momento investigaba a legisladores y congresistas relacionados con la parapolítica.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

El más reciente capítulo del D.A.S se vivió el año pasado cuando se produjo la captura de Laude José Fernandéz Arroyo, ex director de inteligencia, acusado de **concierto para delinquir agravado**, interceptación ilícita de comunicaciones y violación de datos personales, en las chuzadas realizadas a pilotos del sindicato de aviadores ACDAC.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### La inteligencia de Andromeda

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

La presidencia de Santos tampoco estuvo exenta de "las chuzadas", que además se dieron en un contexto histórico para el país: los diálogos en La Habana, que culminaron con la firma del Acuerdo de Paz. En esa ocasión, la revista Semana denunció que en el barrio Galería funcionaba una Central de inteligencia militar. Ver: [En 2015 Colombia compró 850 mil Euros en software de Hacking Team](https://archivo.contagioradio.com/en-2015-colombia-compro-850-mil-euros-en-software-de-hacking-team/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Posteriormente, en los allanamientos adelantados por la Fiscalía, se descubriría que en el lugar trabajaban hackers con la función de intervenir las comunicaciones de los integrantes del equipo negociador de la entonces guerrilla FARC y del equipo del gobierno nacional, encabezado por Sergio Jaramillo.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Ha estos hechos se sumaron la captura del hacker Andrés Sepulveda, quien trabajaba en la central y que tendría la labor de sabotear el proceso de paz. A su vez, tiempo después se conoció un vídeo en el que hablaba con Iván Zuluaga, ex candidato por el partido Centro Democrático a la presidencia, sobre la contienda electoral y los posibles golpes que podrían darle a la campaña del entonces candidato y presidente Juan Manuel Santos, con el uso de información de las Fuerzas Armadas.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### **El hombre invisible vi**ste de camuflado

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Finalmente, frente al más reciente hecho sobre uso indebido de la inteligencia militar en Colombia, en donde se volvió a demostrar que la tecnología era usada para interceptar comunicaciones de políticos y periodistas, el senador Antonio Sanquino, víctima de este nuevo capítulo de las "chuzadas" aseguró que con la confirmación de la situación "volvieron las peores prácticas del pasado". (Le puede interesar: ["Ejército realizó "chuzadas a congresistas, jueces, periodistas y defensores de DD.DD")](https://archivo.contagioradio.com/ejercito-realizo-chuzadas-a-congresistas-jueces-periodistas-y-defensores-de-dd-hh/)

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El senador aseguró que desde el año pasado, cuando descubrieron los espionajes, pusieron en conocimiento tanto al presidente de la República como a distintas instituciones estatales

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

En ese sentido, y tras la entrega del informe "BINCI y Brigada XX: El rol de inteligencia militar en los crímenes de Estado y la construcción del enemigo interno(1977-1998) a la Comisión de la Verdad, una de las peticiones que se le hicieron a esa organización, es que efectúe una evaluación de la doctrina militar, acceder a manuales de inteligencia y operación para que su conocimiento permita establecer de qué manera estos desconocieron los estándares de derechos humanos y el Derecho Internacional Humanitario.

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
