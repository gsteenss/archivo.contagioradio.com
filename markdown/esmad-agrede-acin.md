Title: ESMAD agrede a pueblo indígena del Cauca: ACIN
Date: 2018-10-19 18:04
Author: AdminContagio
Category: DDHH, Nacional
Tags: Comuneros, ESMAD, Liberación de la Tierra, NASACIN
Slug: esmad-agrede-acin
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/05/liberacion-madre-tierra-_-policia.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo Contagio Radio] 

###### [19 Oct 2018] 

Según información de la Cxhab Wala Kiwe - **Asociación de Cabildos Indígenas del Norte del Cauca (ACIN)**, mientras un grupo de indígenas realizaba una actividad de liberación de la madre tierra, fue agredido con violencia desmedida por integrantes del **Escuadrón Móvil Anti Disturbios (ESMAD)**. De la acción **resultaron heridos 11 comuneros**, producto del ataque excesivo con gases lacrimógenos y pistolas marcadoras.

**Edwin Capaz, coordinador de derechos humanos de la Asociación,** explicó que un grupo de indígenas se encontraba el miércoles en una acción de reclamación de derechos directa en el sector de Vista Hermosa, **municipio de Caloto, Cauca**, acción que se conoce como liberación de la tierra que **consiste en recuperar la tierra, devolver las siembras y la vegetación de la zona. (Le puede interesar: ["ONIC denuncia la quema de una casa ceremonial del pueblo Kankuamo"](https://archivo.contagioradio.com/quema-casa-kankuamo/))**

Mientas los comuneros realizaban esta actividad, **integrantes del ESMAD hicieron uso de la fuerza de forma "desmedida e irregular", **empleando gases lacrimógenos  y dañando los lugares en que se estaban albergando. Posteriormente, los "escuadrones de la muerte", como llaman los indígenas al grupo de uniformados, establecieron patrullajes a otro lugar cercano donde también se estaba liberando la tierra.

Las acciones violentas por parte del ESMAD se producen pocos días después de que el presidente de FEDEGAN, **José Felix Lafaurie** señalara que las liberaciones de tierra son actos vandálicos ante los que los empresarios están indefensos, frente a los pueblos indígenas. (Le puede interesar: ["Estudiantes de la U. de Antioquia denuncian agresiones por parte del ESMAD"](https://archivo.contagioradio.com/u-de-antioquia-denuncian-esmad/))

### **Seis comuneros han sido asesinados por el ESMAD desde 2005** 

Capaz afirmó que la acción del ESMAD hace parte de una política del Estado en la que se estigmatiza y señala la actuación de los indígenas y posteriormente la reprime de forma violenta. El integrante de la ACIN recordó que **desde el año 2005, fecha en la que iniciaron las liberaciones en Cauca, han sido asesinados 6 comuneros indígenas por acción del escuadrón**; y señaló que dichas muertes se presentaron por el empleo excesivo de la fuerza con armas no letales.

Para concluir, el defensor de derechos humanos de las comunidades indígenas señaló que afortunadamente no hubo heridos graves que atender, en comparación de otros intentos de desalojo por parte del ESMAD que terminaron peor; y sostuvo que no obstante los riesgos que la liberación de la tierra representa, **los pueblos originarios continuarán realizándolas como una experiencia cultural y espiritual, en exigencia de sus derechos**.

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
