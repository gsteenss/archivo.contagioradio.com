Title: Trabajadores de la ETB denuncian despidos masivos e injustificados
Date: 2018-06-27 16:33
Category: DDHH, Nacional
Tags: Bogotá, ETB, Trabajadores
Slug: trabajadores-de-la-etb-denuncia-despidos-masivos-e-injustificados
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/06/524075_1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Semana] 

###### [27 Jun 2018] 

Trabajadores de la Empresa de Telecomunicaciones de Bogotá, ETB, denunciaron el despido masivo e injustificado de **52 empleados, algunos que pertenecen al Sindicato Sintrateléfonos**, como represalia por las movilizaciones que han hecho para impedir la venta de esta empresa pública.

Los trabajadores manifestaron que desde las 8 de la mañana, cuando llegaron a las instalaciones de la ETB, notaron la presencia de policías e integrantes del ESMAD. Posteriormente, llegó personal de talento humano con cartas de despido. Según Alejandra Wilches, presidenta de ATELCA, sindicato de la ETB, **las personas que han sido retiradas de sus cargos llevaban más de 15 años laborando.**

Wilches, también afirmó que a las personas despedidas se les están entregando dos documentos, en el primero de ellos la empresa propone un arreglo económico, si el trabajador no lo firma se le pasa el segundo documento que es la carta de despido de inmediato.

Además, los trabajadores denunciaron que **hay presencia de la Fuerza Pública al interior de los diferentes edificios** que hacen parte de la empresa y que en la sede que queda en la localidad de Kenedy uno de los trabajadores que fue despedido, se encadenó a las instalaciones. (Le puede interesar:["Nuevo intento de Peñalosa por vender la ETB no sería aprobado por el Consejo"](https://archivo.contagioradio.com/nuevo-intento-de-penalosa-por-vender-la-etb-no-seria-aprobado-por-el-concejo/))

\[caption id="attachment\_54310" align="alignnone" width="800"\]![ATELCA](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/06/WhatsApp-Image-2018-06-27-at-3.48.11-PM-800x480.jpeg){.size-medium .wp-image-54310 width="800" height="480"} ATELCA\[/caption\]

En un comunicado de prensa la ETB expresó que los despidos son producto de un recorte en gastos que busca ajustar la planta de operaciones a los estándares del mercado. De igual forma manifestó que solo 5% de los trabajadores fueron retirados de sus cargos por lo que no se considera un despido masivo.

https://youtu.be/XNZANRXtcG4  
<iframe id="audio_26765675" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_26765675_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
