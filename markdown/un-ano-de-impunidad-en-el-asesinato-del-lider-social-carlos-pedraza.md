Title: Un año de impunidad en el asesinato del líder social Carlos Pedraza
Date: 2016-01-19 15:22
Category: DDHH, Otra Mirada
Tags: Carlos Pedraza, Derechos Humanos, proceso de paz
Slug: un-ano-de-impunidad-en-el-asesinato-del-lider-social-carlos-pedraza
Status: published

###### Foto: Contagio Radio 

<iframe src="http://www.ivoox.com/player_ek_10128619_2_1.html?data=kpWelJ2adZqhhpywj5WdaZS1kZeah5yncZOhhpywj5WRaZi3jpWah5yncbbijMaSpZiJfZLjjMnSjc7RtNbiysnOxpDJsozZzZDO1crXrc%2FV1dSYxsrQb82ZpJiSmqnIqdOf1NTQy8bQb46ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Mayerli Garzón, Congreso de los pueblos] 

###### [19 Enero 2016] 

Dos años llevaba Carlos Pedraza trabajando en un proyecto que buscaba la creación de una comercializadora agropecuaria que promovería un modelo de economía solidaria para la venta de productos campesinos en Bogotá. Pero **una bala en su nuca acabó con esta apuesta y sus sueños de aportar a una Colombia en paz y con justicia social.**

Carlos Alberto Pedraza Salcedo, era un  líder comunitario licenciado en Ciencias Sociales de la Universidad Pedagógica de Bogotá, donde se dedicó a la defensa de la universidad pública y al desarrollo del movimiento estudiantil colombiano. Al salir de la universidad trabajó como profesor en varias escuelas de pedagogía popular, buscando enseñar cómo preservar la memoria de las víctimas en las escuelas, además fue coordinador de los paros nacionales de los años 2013 y 2014.

**El 19 de enero de 2015, fue la última vez que se supo de este líder social** integrante del Congreso de los Pueblos, que dos días después fue hallado muerto en la vereda San Bartolomé, municipio de Gachanchipá, Cundinamarca. Hoy, un año después, no se ha esclarecido el caso de Pedraza y mucho menos ha habido señal alguna de justicia.

**“No existe una investigación concreta que pueda conducir a determinar quiénes fueron los autores intelectuales y materiales del asesinato de Carlos”,** señala Mayerli Garzón del Comité de derechos humanos del Congreso de los Pueblos, quien añade que ya se cumple un año de impunidad, pues no hay una respuesta  clara de la unidad de derechos humanos de la Fiscalía, donde se ha dilatado el proceso.

De acuerdo con Garzón, el asesinato de Carlos Pedraza, se dio en el marco de una serie de amenazas contra líderes sociales y luego se genera una serie de atentados, investigaciones y persecuciones contra integrantes del movimiento social y popular, que se relacionan con los enemigos de la paz, teniendo en cuenta que desde diferentes organizaciones sociales y defensoras de derechos humanos se le ha venido apostando al proceso de paz.

**En 2015 se registraron 62 asesinatos de líderes sociales, de los cuales no existe una sola investigación frente a esa oleada de atentados** contra el movimiento popular, lo que hace pensar que “los paramilitares se están reorganizando, o el Estado está emprendiendo una arremetida contra el movimiento popular sin tener en cuenta que se serán los constructores de una paz con justicia social”, dice la integrante del Congreso de los Pueblos, quien concluye que “es necesario que se generen respuestas frente a las violaciones de derechos humanos y se den garantías para el movimiento social”.

Reciba toda la información de Contagio Radio en su correo <http://bit.ly/1nvAO4u> o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por Contagio Radio.
