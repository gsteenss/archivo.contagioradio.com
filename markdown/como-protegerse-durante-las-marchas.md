Title: ¿Cómo protegerse durante las marchas?
Date: 2019-12-23 16:37
Author: CtgAdm
Category: Expreso Libertad
Slug: como-protegerse-durante-las-marchas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/thumbnail_large.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

#### En este Expreso Libertad hablamos con

Sebastían Quiroga, integrante de Ciudad en Movimiento y Camila Forero, abogada de la Comisión de Justicia y Paz,  sobre las múltiples violaciones a derechos humanos cometidas por parte de la Fuerza Pública en medio del paro nacional adelantado desde el pasado mes de noviembre.

Ambos dieron recomendaciones sobre qué puede hacer y qué no las autoridades y cómo cuidar a las personas para evitar que sean víctimas de falsos positivos judiciales y de agresiones físicas.

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/video.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2Fvideos%2F3042129852481976%2F&amp;width=600&amp;show_text=false&amp;appId=1237871699583688&amp;height=338" width="600" height="338" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

Ver mas: [PARO NACIONAL, Siga toda la información de la movilización](https://archivo.contagioradio.com/paro-nacional/)
