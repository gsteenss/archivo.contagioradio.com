Title: Distrito reducirá de 164 a 40 los centros básicos hospitalarios
Date: 2016-03-18 14:23
Category: DDHH, Nacional
Tags: Alcaldía, Consejo de Bogotá, Hospitales de Bogotá
Slug: distrito-reducira-de-164-a-40-los-centros-basicos-hospitalarios
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/03/Crisis-de-la-salud1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Crónica Uno 

<iframe src="http://co.ivoox.com/es/player_ek_10856760_2_1.html?data=kpWll5ubepGhhpywj5aaaZS1kp6ah5yncZOhhpywj5WRaZi3jpWah5yncaXd1Nnfy9nTb9PZxdrQy9eJh5SZopaYxsqPdZeojMaYlpWPsNDnjMjS0NnWs9Sfw4qwlYqlddTdxNTgjc3Tt46ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Fernando Castro, SINDESS] 

###### [18 Mar 2016] 

En la plenaria del Concejo de Bogotá se aprobó la [reforma a la red hospitalaria pública de Bogotá](https://archivo.contagioradio.com/reforma-hospitalaria-del-distrito-seria-el-paso-final-para-privatizacion-del-servicio-medico/)que para el Sindicato Nacional de La Salud y Seguridad Social, SINDESS, sería el **paso final para la privatización del derecho a la salud en la capital y en el país.**

**Con 33 votos a favor y uno en contra, los 22 hospitales de la ciudad se integrarían en 4 gerencias** distribuidas al sur, norte, centro oriente y suroccidente de Bogotá. Cada una de estas redes tendría servicios especializados, lo que significa que las personas deberán ir a los hospitales, no por la cercanía y facilidad de movilidad vial, sino por las características de la urgencia que tengan.

De acuerdo con Fernando Castro, vicepresidente de SINDESS, los concejales dieron vía libre a este proyecto, pese a las advertencias de que la reforma no cuenta con ningún tipo de estudio técnico, ni de geografía sanitaria, metodología y formulación que validen las rutas integrales y tampoco hay estudios metodológicos de la situación de la salud en Bogotá. Así mismo, Castro asegura que no se realizaron consultas a la comunidad.

Con la reforma, se crean dos entidades que costarían 10 mil millones de pesos y que además serían innecesarias, según explica el vicepresidente de SINDESS, quien añade que también se acabaría con los **164 centros hospitalarios básicos primarios para dejar únicamente 40, “Eso no va a solucionar el problema, va a complicarlo porque los pacientes tendrán que peregrinar mucho más para que los atiendan”.**

María Doris González, presidenta de SINDESS, se trata de una reforma que afecta directamente la estructura de la red hospitalaria, la especialización de los servicios y la movilidad de los usuarios. Además, la reforma no  plantea soluciones referentes a las deudas de las EPS con los hospitales.

Castro concluye que la reforma cuenta con serios vicios de forma y fondo, y que los trabajadores del sector de la salud, junto a congresistas del Polo Democrático y el Partido Mira, estudian las acciones jurídicas necesarias para enfrentar esa decisión que **“afectaría directamente la vida y salud de la ciudadanía”**.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)
