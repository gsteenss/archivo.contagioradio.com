Title: ¿Qué viene luego de la aprobación de la Jurisdicción Especial para la Paz?
Date: 2017-03-14 16:09
Category: Nacional, Paz
Tags: acuerdo de paz, FARC, reincorporacion politica, voces de paz
Slug: los-temas-que-vienen-en-el-tramite-del-acuerdo-de-paz-via-fast-track
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/01/ninos-farc.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [14 Mar. 2017] 

Jairo Estrada, integrante de Voces de Paz aseguro que, **lo que viene luego de la aprobación de la Jurisdicción Especial para la Paz (JEP), es el acto legislativo de reincorporación política de las FARC** a la vida política legal, es decir que se deben empezar a trabaja para dar“todas las condiciones particulares para que esa guerrilla se pueda convertir en partido político legal”. Le puede interesar: [Las FARC-EP a un paso de conformar su partido político](https://archivo.contagioradio.com/las-farc-ep-y-su-partido-politico/)

Además, dijo que probablemente la próxima semana se estará presentando el proyecto de acto legislativo, mediante el cual **se crean 16 circunscripciones especiales territoriales en lugares que han sido víctimas del conflicto** y que se caracterizan por condiciones de desigualdad y pobreza extrema.

Según Estrada, estas circunscripciones servirán, no solamente para que se pongan sobre la mesa del congreso temas que han sido históricamente segregados, sino también para que **se obligue a una renovación de las viejas estructuras políticas** que se han perpetuado en el legislativo. Le puede interesar: [Implementación de acuerdos de paz pasa del terreno jurídico al político: Enrique Santiago](https://archivo.contagioradio.com/wp-admin/post.php?post=30172&action=edit)

**“Se va perfilando un marco normativo importante en general para el campo popular de nuestro país**, bajo el entendido que con los marcos normativos no es suficiente (…) si el marco normativo no se acompaña del correspondiente apoyo social y popular pues seguramente quedará como letra muerta” concluyó Estrada.

<iframe id="audio_17542248" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_17542248_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio]{.s1}](http://bit.ly/1ICYhVU)[.]{.s1}
