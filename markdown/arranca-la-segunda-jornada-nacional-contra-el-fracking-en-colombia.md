Title: Arranca la Segunda Jornada Nacional contra el Fracking en Colombia
Date: 2016-09-19 18:54
Category: Ambiente, eventos
Tags: efectos de la explotación de petróleo, Exploración Petrolera, fracking, fractura hidráulica
Slug: arranca-la-segunda-jornada-nacional-contra-el-fracking-en-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/09/No-al-Fracking.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Avaaz] 

###### [19 Sept 2016] 

Desde este martes y hasta el próximo domingo, organizaciones y plataformas ambientales, sociales y sindicales se congregarán para la Segunda Jornada Nacional contra el Fracking en la que **denunciarán las políticas, programas y proyectos que promueven el desarrollo del fracking** en distintas regiones colombianas. Así mismo, mostrarán su apoyo a las diversas iniciativas de defensa del territorio y promoverán su articulación.

De acuerdo con las organizaciones, debido a la política mineroenergética promovida por el gobierno colombiano, que ha impulsado la exploración y explotación de hidrocarburos no convencionales, desde hace ya varios años **se han venido ofreciendo bloques petroleros en los que se utilizará el fracking**, en zonas como Santander, Norte de Santander, Antioquia, Cundinamarca o Tolima.

Estas acciones han tenido como marco legal el Decreto 3004 de 2013, la Resolución 90341 de 2014 y el Plan Nacional de Desarrollo 2014-2018; no obstante se han venido impulsando desde mandatos anteriores como el de Álvaro Uribe o Andrés Pastrana, **en contradicción con los llamados internacionales para frenar el cambio climático** y alcanzar un verdadero desarrollo sostenible.

El fracking requiere del **uso intensivo de aguas superficiales y subterráneas; provoca contaminación atmosférica, escapes de gas metano**, e impactos al cambio climático; así como sismicidad. Pese a que en Colombia no se ha aplicado la técnica, ya han emergido conflictos socioambientales en regiones como San Martín, Cesar, en las que se están impulsando estos proyectos.

Esta segunda jornada se pone en marcha con el objetivo de prevenir los **nocivos impactos ambientales y a la salud pública que podrían originarse tras la aplicación del fracking** en Colombia; riesgos que han llevado a que en países como Francia y Bulgaria, la técnica haya sido prohibida, y a que en naciones como Escocia y Holanda hayan declarado su moratoria.

### PROGRAMACIÓN 

20 de septiembre. Diplomado ambiental: El fracking en América Latina. Ibagué. Horario: 4:00 – 6:00 p.m y 6:00 – 8:00 p.m. Lugar: Auditorio Los Ocobos. Universidad del Tolima, Sede Centro.

20 y 21 de septiembre. Intervención artística, realización de un mural, No al Fracking en el Magdalena Medio y en Colombia, muro de la USO nacional.

21 de septiembre. Foro Nacional: "El fracking en las Américas y Colombia, fracking y resistencias". Lugar: Auditorio Camilo Torres, Facultad de Derecho, Universidad Nacional de Colombia, Bogotá. Horario: 2:00 p.m. 6:00 p.m.

21 de septiembre. Twitteraton Nacional. 7:00 p.m.

22 de septiembre. Foro Internacional “Agua, petróleo y tierra: Dinámicas económicas en el siglo XXI.” – Puerto Boyacá. Horario: 9:00 a.m. 12:30 p.m.

22 de septiembre. Foro Nacional "Impacto social y ambiental del fracking", Bucaramanga. Lugar: Biblioteca Pública Gabriel Turbay. Auditorio Pedro Gómez Valderrama. Horario: 8:00 a.m. 12:30 p.m.

22 de Septiembre: Conversatorio sobre las realidades del Fracking en USA, México y Colombia. Horario: 7:00 pm. Lugar: Auditorio USO Nacional.

22 de septiembre. Plantón Nacional contra al fracking

23 de septiembre. Taller sobre el Fracking con los medios locales, nacionales e internacionales Horario: 9 am a 11:30 am.

23 al 25 de septiembre Curso Internacional Ecología Política fracking: impactos, conflictos y resistencias. Barrancabermeja, Santander Organizan CENSAT Agua viva – Amigos de la Tierra Colombia, Movimiento Ríos Vivos, Escuela de Sustentabilidad, Uso y Funtramiexco. Lugar: Unión Sindical Obrera - USO

23 de septiembre 8 p. m Integración cultural, a cargo del Colectivo Gallo Fino. Lanzamiento de la Feria de las Artes Independiente: [["Por la Defensa del Agua, las Ciénagas, los Ríos y las Fuentes Hídricas del Magdalena Medio"](https://archivo.contagioradio.com/mas-de-100-familias-afectadas-por-contaminacion-de-agua-en-barrancabermeja/)]. Lugar: La Escala de Milán, Hora: 8:30 pm

24 de septiembre de I Simposio Internacional Agua, Fracking y Derecho Ambiental organizado por CorpoYariguíes, GEAM y Dignidad Santandereana. Horario: 9 a.m a 1 p.m; Lugar: sin definir

24 de septiembre. Foro Regional “El fracking en las Américas y Colombia. Luchas y resistencias” – San Martín. Horario: 9:00 a.m. 01:00 p.m. Lugar: sin definir

###### [Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)]] 
