Title: Tres medidas para frenar la violencia política según la MOE
Date: 2019-09-03 18:54
Author: CtgAdm
Category: DDHH, Política
Tags: Elecciones en Colombia, Karina García, MOE
Slug: tres-medidas-para-frenar-la-violencia-politica-segun-la-moe
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/09/MOE.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/09/MOE-Accidente.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: MOE] 

Desde la Misión de Observación Electoral (MOE) advierten que el asesinato de Karina García, candidata a la Alcaldía de Suárez, Cauca y del aspirante al Concejo, Yeison Obando es un síntoma de un incremento en la violencia política que no solo se vive en este departamento pues tras 35 días de proceso electoral se han presentado un atentado, un secuestro y 10 amenazas.

Alejandra Barrios, directora de la MOE advierte que los riesgos, afectaciones y oportunidades de los candidatos varían según la región en la que se encuentre y la lógica con la que funcionan los actores del territorio, "por ejemplo en Arauca, no es lo mismo un candidato que proviene de movimientos sociales, que uno que venga de partidos tradicionales, esto depende de las regiones y su seguridad depende de eso".

Pese a esta segmentación en los territorios, agrega que desde la MOE consideran a las economías y la financiación ilegal de grupos armados como el verdadero hilo conductor de estos actos de violencia política. [(Lea también: Paramilitares amenazan candidatos a la Alcaldía en Ovejas, Sucre)](https://archivo.contagioradio.com/paramilitares-amenazan-candidatos-a-la-alcaldia-en-sucre/)

### Activación del Grupo de Reacción Inmediata Electoral

Este plan incluye la focalización de zonas críticas, la organización  y articulación de organismos del estado, poniendo en marcha lo que el Gobierno llama el Grupo de Reacción Inmediata Electoral (GRIE).

También se crearía el comité especial de medidas de protección electoral y se debe instalar el centro integrado de información de inteligencia electoral en el que están incluidos la Policía y la MOE.

### **Según la MOE es necesario caracterizar el territorio** 

De igual forma desde la MOE se advierte que no basta con actividades de protección aplicadas a nivel nacional y de forma homogénea, señalando que precisamente estos riesgos diferenciados mencionados con anterioridad también deberían obedecer a características locales de seguridad.

> "Los planes para elecciones que solo contemplan elecciones nacionales no contemplan ni interpretan las realidades y las amenazas que pueden presentarse a nivel local".

Barrios resalta que en todo el territorio nacional son 117.000 candidatos y que no todos pueden contar con esquemas de protección, autos blindados o escoltas, medidas con las que contaba la candidata Karina Garcia, una razón más por la que se hace necesario adoptar otros métodos de protección a través de una "**lectura y caracterización del territorio**".

Dichos métodos, resalta Barrios, pueden ser abordados a partir del pacto firmado por todos los partidos políticos del país con el fin de establecer un acuerdo en la sociedad por la reconciliación. [(Le puede interesar: La importancia de firmar un pacto por la no violencia en época electoral)](https://archivo.contagioradio.com/pacto-por-la-no-violencia-electoral/)

### Desmontar la violencia verbal

De igual forma resaltó que tanto las redes sociales como el voz a voz pueden ser espacios que generan descontextualización e información falsa, **"no solo es una violencia simbólica, también genera una violencia real que genera muertos amenazas y candidatos**"; una afirmación que cobra fuerza después de conocerse el video en el que Karina advirtió lo peligroso que podría resultar la difusión de información falsa.

La directora de la MOE concluyó que este momento es crucial para que los partidos cuiden de las comunicaciones de sus candidatos, "el debate sí, con fuerza sí, pero no podemos a través de mensajes de odio incentivar el asesinato de quienes compiten por cargos públicos en este país".

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
