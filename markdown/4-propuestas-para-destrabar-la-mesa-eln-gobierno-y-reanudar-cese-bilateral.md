Title: 4 propuestas para destrabar la mesa ELN-Gobierno y reanudar cese bilateral
Date: 2018-01-11 11:41
Category: Nacional, Paz
Tags: cese bilateral, Mesa de conversciones, proceso de paz, Quito
Slug: 4-propuestas-para-destrabar-la-mesa-eln-gobierno-y-reanudar-cese-bilateral
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/02/proceso-de-paz-eln.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: El Telegrafo] 

###### [11 Ene 2018] 

Diferentes analistas realizaron **4  propuestas que podrían destrabar la mesa de conversaciones y lograr que se restablezca el cese bilateral**, tras el anunció de la no reanudación del cese bilateral entre el ELN y el Gobierno Nacional, hecho por el presidente Santos.

### **No devolver la piedra** 

Monseñor Darío Monsalve, Arzobispo de Cali y una de las personas que ha estado respaldando este proceso de paz, hizo un llamado al gobierno del presidente Santos y al Ministerio de Defensa, para “poner una pausa de reflexión” ante la agresión, antes de que se retome el uso de la fuerza contra el ELN. Además propone que **el proceso se acoja a un tercero, que bien podría ser la iglesia o alguno de los países garantes**, para que se mantenga la voluntad hacia el cese bilateral.

De igual forma, considera que con este acompañamiento, se podría definir los términos del nuevo pacto de cese bilateral y su mecanismo, “**no puede significar, en la semántica de la paz sincera, terminar un cese y volver a la violencia,** que desnaturaliza la voluntad de paz, infla la coyuntura adversa a ella, desalienta a las víctimas y al pueblo colombiano, defrauda a la Iglesia y a la comunidad internacional” afirmó Monseñor.

Así mismo señaló que debe ser un diálogo con eficacia de propuestas y propósito, lo que conlleve a que el cese bilateral “sea la condición justa para llegar a un acuerdo justo”, en ese sentido  Monseñor Monsalve expresó **“No devolver la piedra lanzada, será valorar lo alcanzado** y vencer la tentación de volver al desastroso hábito de la guerra y de la destrucción”. (Le puede interesar: "[En el limbo mesa de conversaciones entre ELN y Gobierno"](https://archivo.contagioradio.com/cuidados_intensivos_mesa_quito_eln_gobierno/))

### **Facilitadores del proceso ficha clave en la mesa** 

Luis Eduardo Celis, analista político e integrante de la Fundación Nuevo Arcoíris, afirmó que en efecto el anunció de ayer del presidente Santos, revela una situación de crisis, sin embargo, considera que es urgente que iniciativas como la de la Conferencia Episcopal, la ONU y los países garantes, giren en torno a ser facilitadores del diálogo.

“**La resolución de esta crisis pasa porque la Iglesia Colombiana, la ONU y los países garantes, hablen con las dos partes y busquen una fórmula para restablecer el cese bilateral** y la continuidad de la mesa” manifestó Celis y agregó que el ELN debe evaluar con mucha tranquilidad su iniciativa militar, porque podría ser una “sobreactuación” que no contribuye a la participación de la sociedad y que por el contrario la aleja.

### **Reanudar el cese bilateral** 

Carlos Velandia, gestor de paz de este proceso, recomendó que, para salir del estado de crisis que afronta la mesa, primero debe haber una activación de la actividad diplomática del grupo de países garantes, **específicamente de Noruega y Cuba, que han demostrado una “gran solvencia”** a la hora de mediar en crisis.

Con respecto al cese bilateral, Velandia considera que **este necesita prorrogarse de manera “inmediata”, por 30 días** y que en este plazo se reinstalen las mesas que ya venían funcionando dentro del proceso, de esa manera la mesa sobre participación, podría ir adelantando el diseño del modelo de participación para la sociedad civil en el proceso de paz, mientras que en la mesa sobre acciones humanitarias, se podría hacer la evaluación de las distintas situaciones que se presentaron en el pasado cese bilateral, con la finalidad de re diseñar un nuevo cese bilateral que vaya hasta el 7 de agosto de este año.

“De esta manera si podremos **blindar, no solamente la mesa de diálogos, sino dar garantías efectivas a los participantes en este proceso social, y blindar, también, las próximas elecciones** y así entregar al próximo presidente un país en un estado de no guerra” afirmó Velandia.

### **Los actores que pueden salvar la mesa** 

El historiador y analista político Víctor de Currea, aseveró que es el momento de que haya una participación más activa de tres actores: los países garantes, la Iglesia y la sociedad civil y de esta manera generar un mecanismo más sólido de intermediación con los equipos de la mesa de Quito.

Frente al cese bilateral al fuego, de Currea afirmó que es **necesario que se precisen los protocolos del cese, crean un glosario sobre la terminología de guerra, incorporar a la comunidad en la recolección de la información** y desplegar personas en el territorio en comisiones que estén al tanto de la observación del cese. (Le puede interesar: ["El balón del cese al fuego esta en la cancha del ELN: Víctor de Currea"](https://archivo.contagioradio.com/cese-al-fuego-bilateral-eln/)

### **Las víctimas también esperan un pronto Cese Bilateral** 

Por su parte, las comunidades CONPAZ, manifestaron el pasado 10 de Enero que siguen insistiendo en que las escuchen y reiteraron que están agotados de la guerra "les invitamos a volver a sentarse e identificar con precisión lo que puede mejorarse del cese bilateral y en segundo lugar, definir un derrotero de los temas que pueden abordar para llegar a un Acuerdo Parcial de asuntos de fondo" reza el comunicado.

### **Gobierno reitera voluntad para pactar un nuevo cese bilateral**

Por otra parte, el jefe negociador del gobierno, Gustavo Bell manifestó que el gobierno sigue con la voluntad de reanudar el Cese Bilateral del fuego y pactar uno nuevo, sin embargo expresó que la delegación continuará en Colombia para atender la visita del Secretario General de las Naciones Unidas el próximo 13 de Enero y a la espera de instrucciones por parte del Presidente Santos.

###### Reciba toda la información de Contagio Radio en [[su correo]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio]{.s1}](http://bit.ly/1ICYhVU)[.]{.s1}
