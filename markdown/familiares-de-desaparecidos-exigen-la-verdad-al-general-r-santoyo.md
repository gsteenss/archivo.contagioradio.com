Title: Familiares de desaparecidos exigen la verdad al general (r) Santoyo
Date: 2019-04-30 11:38
Author: CtgAdm
Category: Comunidad, Judicial
Tags: General Mauricio Santoyo, personas desaparecidas
Slug: familiares-de-desaparecidos-exigen-la-verdad-al-general-r-santoyo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/Santoyo.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Fiscalía] 

La mañana de este lunes tras cumplir 6 años de una condena 13 años por narcotráfico y ayudar a grupos de autodefensas, el general (r) de la Policía, Mauricio Santoyo, fue capturado por participar en la presunta desaparición de los defensores de derechos: **Ángel Quintero Mesa y Claudia Monsalve.** Adriana Quintero, hija de Ángel ha manifestado que después de 19 años transcurridos, es hora que la justicia actué y se conozca el paradero de su padre.

### **"Recibimos con esperanza la noticia de la captura en Colombia del general (r) Santoyo"** 

La hija de Ángel Quintero Mesa, integrante de la **Asociación de Familiares Detenidos Desaparecidos, (ASFADDES)** exigió que se actúe con celeridad y cumplimiento señalando que aquella noche del 6 de octubre de 2000 en Medellín,en que desapareció, su padre rindió declaración frente al Gaula en Medellín con relación en caso de sus familiares desaparecidos y que en lugar de ser protegido por , Santoyo, su padre le fue arrebatado.

> [\#ATENCIÓN](https://twitter.com/hashtag/ATENCI%C3%93N?src=hash&ref_src=twsrc%5Etfw) Tras su llegada a [\#Bogotá](https://twitter.com/hashtag/Bogot%C3%A1?src=hash&ref_src=twsrc%5Etfw), luego de cumplir condena en Estados Unidos, [\#Fiscalía](https://twitter.com/hashtag/Fiscal%C3%ADa?src=hash&ref_src=twsrc%5Etfw) capturó al General (R) Mauricio Santoyo por su presunta participación en la desaparición de dos defensores de derechos humanos en [\#Medellín](https://twitter.com/hashtag/Medell%C3%ADn?src=hash&ref_src=twsrc%5Etfw) [pic.twitter.com/uXsvd0LlcL](https://t.co/uXsvd0LlcL)
>
> — Fiscalía Colombia (@FiscaliaCol) [29 de abril de 2019](https://twitter.com/FiscaliaCol/status/1122929118792884224?ref_src=twsrc%5Etfw)

<p>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
</p>
Adriana Quintero se dirigió en particular al general pidiéndole que **"nos dé la cara y mirándonos de frente nos saque de este dolor e incertidumbre de tantos años, tenemos derecho a  conocer la verdad, este es un acto de humanidad ante el país".** [(Lea también: Generales Mauricio Santoyo y Rito Alejo Del Río son vinculados a crimen de Jaime Garzón) ](https://archivo.contagioradio.com/generales-mauricio-santoyo-y-rito-alejo-del-rio-son-vinculados-a-crimen-de-jaime-garzon/)

 

[English Version](https://archivo.contagioradio.com/families-of-missing-people-demand-the-truth-from-general-santoyo/)

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
