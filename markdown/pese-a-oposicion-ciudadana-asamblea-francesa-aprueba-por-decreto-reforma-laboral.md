Title: Pese a oposición ciudadana, Asamblea Francesa aprueba por decreto reforma laboral
Date: 2016-07-07 16:12
Category: Economía, El mundo
Tags: crisis de los refugiados, protestas contra reforma laboral Francia, reforma laboral Francia
Slug: pese-a-oposicion-ciudadana-asamblea-francesa-aprueba-por-decreto-reforma-laboral
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/07/652474_protesta-francia-contra-reforma-laboral.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: INFO] 

###### [7 jul 2016] 

La Asamblea Nacional Francesa, mediante decreto, **aprobó en segunda lectura la reforma laboral a la que se han opuesto centenares de ciudadanos** porque incrementa la jornada de trabajo, no contempla el pago de horas extras, plantea despidos sin mediación sindical, reduce los permisos y tiempos de vacaciones, y desconoce las garantías conquistadas por los sindicatos. Según información extraoficial, la ley pasará al Senado para una segunda lectura y, como la mayoría es conservadora, se estima que sea aprobada sin mayores obstáculos.

De acuerdo con el analista político Cesar Torres del Río,  este hecho autoritario se da en el marco de la declaratoria de estado de excepción y pone en marcha una **suerte de guerra frontal contra los sectores organizados de la sociedad civil**, en detrimento de las garantías y derechos de grupos sociales como los sindicatos, y agudizando las tensiones en las que actualmente viven los refugiados en todas las naciones que conforman la Unión Europea.

Actualmente la ciudadanía francesa se enfrenta a alzas en los niveles de desempleo y ve en los inmigrantes que llegan al país una competencia fuerte, pues este factor ha llevado entre otras, a descensos en los salarios, lo que sitúa a los ciudadanos en una situación compleja en la que deben **defender sus empleos sin menoscabar la dignidad de los refugiados**, y "ese es el gran dilema", porque no hay políticas claras para la defensa de los derechos ni de los inmigrantes, ni de los trabajadores, asegura el analista.

<iframe src="http://co.ivoox.com/es/player_ej_12153623_2_1.html?data=kpeel5iadpShhpywj5acaZS1kp6ah5yncZOhhpywj5WRaZi3jpWah5yncaTZ1MbfjbnTttPZ1JDRx9GPloa3lIquptSJdqSfotPOzs7XuMKfytPhx9fSpcTd0NPOzpKJe6ShpNTb1sbLrdCfs8bRy9SPcYarpJKh&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)]  ] 
