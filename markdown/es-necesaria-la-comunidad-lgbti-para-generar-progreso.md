Title: ¿Es necesaria la comunidad LGBTI para generar progreso?
Date: 2016-09-28 08:29
Category: Opinion, Sofia
Tags: comunidad gay, LGBTI
Slug: es-necesaria-la-comunidad-lgbti-para-generar-progreso
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/08/LGBTI.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

#### Por [Sofia](https://archivo.contagioradio.com/teticaida/) - [@Teticaida ](https://twitter.com/Teticaida) 

[Durante mi infancia y mi adolescencia escuche casi que a diario, en el colegio, que Dios era bueno, que Dios era amor, que era hombre y que ese señor nos amaba a todos por igual, que nos había hecho a su imagen y semejanza. Durante mi primaria y secundaria el común denominador en las clases de educación sexual siempre fue una pareja heterosexual. Básicamente todo se reduce a que un hombre y una mujer se enamoran, se casan, tienen relaciones sexuales, esperan nueve meses y tienen un bebé. Luego ese bebé crece y probablemente repetirá el proceso.]

[Cuando tenía alrededor de 8 años recuerdo mucho a un vecino súper simpático que vivía en el apartamento del frente. Era amigo de mis papás. Recuerdo que me llamaba mucho la atención porque se vestía con mucho estilo, se atrevía a usar cosas que en Bogotá, uniformada casi que de azul con negro, muchos no se atrevían a usar. Un día le pregunté a mi mamá si el vecino tenía esposa, y que si así era, que por qué yo no la había conocido aún. Mi mamá me respondió con total naturalidad que él no tenía novia, que él era gay, homosexual, que le gustaban los hombres y que tenía un novio. Me lo dijo con tal naturalidad que yo lo entendí como lo que es, algo completamente normal.]

[Cuando tenía 16 años una compañera del colegio tuvo que confesar, bajo presión y señalamientos, que era lesbiana. Por razones que en este momento no son de importancia, en esos momentos nos encontrábamos durmiendo en el colegio. Recuerdo que en la noche, después de su confesión, alrededor de 15 compañeras estábamos reunidas hablando de ello, sin ella. Esa noche fue la primera vez que me cuestioné la clase de educación que habían recibido 11 de mis compañeras. Pocas veces escuché comentarios tan ignorantes, llenos de odio e hirientes. Hoy me cuestiono que sería lo que escuchaban en sus casas o iglesias para llegar a decir de esa manera semejantes cosas.]

[En un país en dónde todavía para muchos es difícil decir abiertamente que son homosexuales o transgénero porque temen sentir el rechazo de sus seres queridos, el matoneo de sus colegas o compañeros de colegio, se la ponemos aún más difícil al señalarlos por enamorarse y por querer conformar una familia. ¿Por qué estos seres humanos, que pagan sus impuestos, que son buenos ciudadanos, que se preparan, que estudian, que trabajan día a día así como usted y yo, no tienen derecho a soñar con conformar una familia, a tener o adoptar bebés? ¿Qué tienen ellos de diferente a usted o a mí? Nada. Ser una buena persona y poder ser un buen padre o madre no tiene nada que ver con si se es heterosexual, homosexual o transgénero.]

[Nos quejamos mucho de nuestro subdesarrollo. Los que pueden salir del país son felices describiendo el progreso de esos países desarrollados que visitaron, hablan de cómo es de limpio el metro, de que en ese país hicieron un determinado descubrimiento científico, de que ese pintor tan famoso se formó allí. Nunca nos detenemos a pensar cuánta gente de la comunidad LGBTI está detrás de ese progreso. Una de las diferencias entre los países desarrollados y subdesarrollados es que en los desarrollados todos los ciudadanos valen lo mismo. Sin discriminación se llega más lejos porque para progresar se necesita la inteligencia de todos. En los países subdesarrollados generalmente el progreso se da con  la inteligencia de unos pocos, que infortunadamente no siempre resultan ser los más brillantes.]

[¿Cuántos grandes ejecutivos, científicos y artistas, pertenecientes a la comunidad LGBTI han hecho y continúan haciendo grandes avances para la humanidad? Muchos. En Colombia necesitamos más gente como Brigitte Baptiste ocupando cargos importantes. Gente que ocupa esos cargos porque es buena, inteligente, preparada y con estudios. Como ciudadana no me quiero perder del conocimiento y las ganas de trabajar de gente buena que no ha podido escalar lo suficiente solo por el hecho de que ahí afuera hay gente que es felíz cerrándoles el camino solo por sus preferencias sexuales. Como ciudadana no quiero que haya niños que crezcan sin el amor de una familia. Las nuevas generaciones tienen que crecer viendo que el amor entre seres humanos, en todas sus expresiones es normal. Que las familias monoparentales, familias de dos mamás, familias de dos papás, familias de papá y mamá transexual son completamente posibles. Que las preferencias sexuales de cada quien no determinan si uno es buen o mal ciudadano ni definen si uno puede ser un buen padre.]

[Para terminar, tengo la esperanza de que algún día nadie tendrá que decir “yo soy transexual”, “soy lesbiana”, “soy esto, soy lo otro”. Estoy segura de que en el futuro eso no importará. Todos podremos ser lo que sintamos y queramos y nadie nos va a discriminar por ello ni nos va a decir que no podemos amar a determinada persona o soñar con tener una familia. Nadie se va a interponer entre nuestros deseos de tener una familia y educar a nuestros hijos dentro del marco del respeto y el amor. Nadie se opondrá a que eduquemos niños para construir un mejor país y para progresar.]

[Creo en las sociedades incluyentes y tolerantes en las que todos tenemos un espacio. Sociedades en las cuales, todos tenemos un espacio para aportar al progreso. Sociedades en las cuales la gente no se escuda en un Dios para atacar sistemáticamente a lo que a simples ojos parece una minoría.]

------------------------------------------------------------------------

------------------------------------------------------------------------

###### Las opiniones expresadas en este artículo son de exclusiva responsabilidad del autor y no necesariamente representan la opinión de la Contagio Radio.
