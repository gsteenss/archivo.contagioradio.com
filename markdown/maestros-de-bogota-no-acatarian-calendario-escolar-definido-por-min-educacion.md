Title: Maestros de Bogotá no acatarían calendario escolar definido por Min. Educación
Date: 2017-07-07 15:40
Category: Educación, Nacional
Tags: ade, estudiantes, fecode, maestro, Paro de maestros
Slug: maestros-de-bogota-no-acatarian-calendario-escolar-definido-por-min-educacion
Status: published

###### [Foto: ADE Bogotá] 

###### [07 Jul. 2017 ] 

Como revanchista, calificó la Asociación Distrital de Educadores ADE, la decisión del Ministerio de Educación Nacional – MEN- de extender las clases en los colegios hasta el 22 de diciembre. Para los maestros **esta decisión desconoce las propuestas presentadas y las jornadas de receso que quedan hasta final de año**. Razón por la cual han dicho desconocerán dicha directriz. Le puede interesar: ["Hay propuestas pero el gobierno no tiene voluntad de negociar: FECODE"](https://archivo.contagioradio.com/maestros-tiene-propuestas-pero-el-gobierno-no-tiene-voluntad-para-negociar/)

En un breve comunicado la ADE manifestó que la Circular 005 emitida por el ministerio **“pretende proceder en contravía de nuestra dignidad y de nuestro compromiso ético para con los estudiantes,** padres y madres de familia, circular que rechazamos y orientamos no acatar”. Le puede interesar: [Déficit en educación es de 600 mil millones de pesos: FECODE](https://archivo.contagioradio.com/deficit-en-educacion-es-de-600-mil-millones-de-pesos-fecode/)

**En la actualidad, FECODE y el MEN se encuentran en reuniones para lograr un acuerdo en el ajuste del Calendario Académico,** mesa para la cual la ADE realizó una propuesta que dicen espera sea tenida en cuenta o de lo contrario anunciaron nuevas movilizaciones. Le puede interesar: [Gobierno no resuelve exigencias sobre déficit presupuestal de Educación: ADE](https://archivo.contagioradio.com/gobierno-no-resuelve-exigencias-sobre-deficit-presupuestal-de-educacion-ade/)

Finalmente, han dicho que **rechazan y desmienten las declaraciones del Viceministro de Educación, Pablo Jaramillo** quien “pretendió dar a entender a la opinión pública que había acuerdo con la ADE en tan sensible tema como producto de una reunión que nunca se realizó”. Le puede interesar:[ "Esperamos que Estado invierta en Educación los recursos que se necesitan"](https://archivo.contagioradio.com/fecode-acepta-a-procurador-general-como-facilitador-en-las-negociaciones/)

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU).

<div class="osd-sms-wrapper">

</div>
