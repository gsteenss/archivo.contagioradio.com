Title: Los principales responsables de la crisis humanitaria en La Guajira
Date: 2016-02-10 16:31
Category: DDHH, Entrevistas
Tags: corrupción, La Guajira, Wayuu
Slug: empresas-y-estado-responsables-de-la-situacion-en-la-guajira
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/02/Guajira-e1455139314228.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Arauca Estéreo 

<iframe src="http://www.ivoox.com/player_ek_10388727_2_1.html?data=kpWgmp2bdpihhpywj5acaZS1kZuah5yncZOhhpywj5WRaZi3jpWah5yncabh0dfS1cbXb9qfptjhw8nTaZO3jNXfy9PHrdHVzcrgjdfJt9Hjz9jOxNHJt4zYxpDZw5DXrdXpwpKSmaiRh9Di1cbUy9SPlsLYytSYj4qbh46o&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Armando Valbuena] 

###### [10 Feb 2016.] 

La corrupción en las administraciones y los proyectos mineros implementados en la Guajira han provocado la muerte de por lo menos 4000 niños en todo el departamento, paraArmando Valbuena, líder indígena, esta crisis se debe a **la acción empresarial junto a la desatención estatal.**

Las recientes muertes de niños sumado a los 16 casos de los menores de edad wayuu que se debaten en la vida y la muerte, fueron el agravante para que el presidente Juan Manuel Santos decidiera liderar una reunión para buscar salidas a la situación que se vive en la región, sin embargo, Valbuena, asegura que no hay ningún tipo de expectativa frente al plan de acción que se tome desde el gobierno, pues de acuerdo con él “**hay genocidio estructural de Estado contra los pueblos indígenas de La Guajira”.**

Según Valbuena es evidente la inacción de los entes estatales, pues aun cuando la** Corte Constitucional profirió un Auto en 2009 que obligaba al Estado a ejecutar un plan de salvaguarda** para la región éste no ha sido aplicado, por el contrario ha aumentado la corrupción en sus funciones administrativas y los avances en la solución de estas problemáticas han sido mínimos.

Desde el Ministerio de Vivienda la entrega de 222 pozos de agua para las comunidades indígenas, sin embargo, esto contrasta con la acción empresarial de empresas como el Cerrejón custodiadas por el Estado, y que en palabras del líder indígena “lleva 30 años despilfarrando millones de litros de agua del río Ranchería para extraer carbón… La Guajira no cuenta con el mínimo vital”.

**La Serranía de Macuira y el río Ranchería**, han sido afectadas tanto por el cambio climático, como por los 30 años de extracción carbonífera por parte de Cerrejón. **La empresa minera para extraer el carbón usa las aguas del rio y luego vierte en su cuenca las aguas contaminadas resultantes.** Lo más grave es que esas son las dos únicas fuentes de las que depende el abastecimiento de agua y alimentos del pueblo Wayuu.

Para Valbuena, la “crisis humanitaria en La Guajira responde a desorden estructural del Estado”. La Fiscalía y Procuraduría no han funcionan para investigar los casos de corrupción que han dejado en la pobreza a una de las regiones que más le genera regalías al país.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
