Title: Propuestas de Uribe no tienen en cuenta a las víctimas
Date: 2016-10-13 15:53
Category: Nacional, Paz
Tags: Alvaro Uribe, apartado, Chocó, desarrollo, guerra, paz, Reconciliación, victimas de la chinita
Slug: propuestas-de-uribe-no-nos-tienen-en-cuenta-a-las-victimas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/10/victimas.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Contagioradio 

###### 13 Oct 2016 

Las **víctimas de La Chinita**, quienes **encabezaron la Marcha de las Flores** realizada en Bogotá, aseguraron que no van a permitir que el acuerdo de La Habana se eche para atrás y que seguirán marchando para que éstos se cumplan.

Además aseguraron que con su participación en la movilización **ratificaron su perdón a las FARC – EP** “y también en el marco de todo esto hemos hecho un proceso de reconciliación” aseguró Silvia Berrocal, una de las víctimas de La Chinita y participante de la marcha.

Por su parte, la señora Silvia aseguró que esperan que el presidente Juan Manuel Santos **ratifique los acuerdos firmados en Cartagena**, “porque nosotras las víctimas queremos la paz” agregó.

A propósito del cordón de flores que se realizó en la Plaza de Bolívar para recibir a los manifestantes, Doña Silvia aseguró que se sintieron muy emocionados y agradecidos y que esa fue la oportunidad de manifestarle a las personas de Bogotá “los esperamos en Urabá y no solo a las personas sino todo el desarrollo y el progreso que pueda llegar a nuestra región con la paz” puntualizó.

**Respecto a las propuestas que en las últimas horas entregó el Centro Democrático** en cabeza del Senador Álvaro Uribe, **las víctimas de La Chinita manifestaron no estar de acuerdo** pues “esas propuestas no tienen en cuenta a las víctimas” y agregó “a ellos no les interesa la paz, no les interesan las víctimas, no les interesa nada, les interesan los lucros personales, por eso estamos en desacuerdo con las propuestas que presento Álvaro Úribe Vélez.

Por último, manifestó que seguirán trabajando desde sus regiones por las víctimas y para que el acuerdo se firme.

Le puede interesar: [Víctimas de Bojayá y la chinita exigen participación en pacto por la paz](https://archivo.contagioradio.com/victimas-de-bojaya-y-la-chinita-exigen-participacion-en-pacto-por-la-paz/)  
<iframe id="audio_13306772" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_13306772_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>
