Title: USO se toma instalaciones de Ecopetrol
Date: 2016-11-22 10:53
Category: Movilización, Nacional
Tags: Ecopetrol, union sindical obrera
Slug: uso-se-toma-instalaciones-de-ecopetrol
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/uso.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:USO] 

###### [22 Nov 2016] 

Trabajadores pertenecientes a la USO y empleados de Ecopetrol decidieron tomarse las estaciones de Coveñas de recibo y despacho del Crudo y la estación de Puerto Salgar, en donde se despacha gasolina al país, esto en rechazo a la **decisión del gobierno Nacional y de Ecopetrol de poner a la venta activos como los campos maduros, propilco y CENIT**, paso que de acuerdo con los trabajadores es una acción más que conlleva a la privatización de Ecopetrol.

De acuerdo con Edwin Castaño presidente de la USO, esta medida que ellos denominan de “ejercicio legítimo”, **"busca llamar  la atención del gobiernos y establecer un escenario de diálogo que permita hacer énfasis en la amenaza latente que significan estas medidas para el país”**

### **Las razones de la protesta de la USO** 

Serían 20 los pozos que se habrían puesto en subasta, que producen aproximadamente **4.000 galones de petróleo** y que se ubican en zonas del Magdalena Medio. Las multinacionales que participarían de esta subasta son 22 y este hecho dejaría sin empleo a aproximadamente **2.200 trabajadores. **

Frente a riesgos de corrupción en estas acciones los trabajadores pidieron que se presente la Contraloria y haga la veeduría pertinente para evitar delitos.

Para Castaño el ingreso de estas multinacionales solo “garantiza la precarización laboral, que estas empresas no paguen regalías y que se siga tercerizando la mano de obra y que se acabe una empresa como Ecopetrol que es patrimonio de los colombianos”. Le puede interesar:["Ecopetrol y OCENSA demandadas por comunidades de Santa Cruz del Islote"](https://archivo.contagioradio.com/ecopetrol-y-ocensa-demandadas-por-comunidades-de-santa-cruz-del-islote/)

Otras de las medidas anunciadas por los administrativos de Ecopetrol es acabar con el Instituto Colombiano de Petroléo y la posible venta de activos estratégicos.

Los trabajadores afirmaron que continuar protestando hasta que no se instale la mesa de diálogos con el gobierno y se generen medidas que protejan tanto las garantías laborales de los trabajadores como las acciones y el patrimonio de Ecopetrol.

<iframe id="audio_13873672" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_13873672_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

######  Reciba toda la información de Contagio Radio en [[su correo]
