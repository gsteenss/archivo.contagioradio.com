Title: Cemex se lucra con la ocupación Israelí en Palestina
Date: 2017-07-05 15:30
Category: Onda Palestina
Tags: BDS, Cemex, Israel, Palestina
Slug: cemex-palestina-bds
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/07/Cemex.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Economía hoy 

###### 05 Jun 2017 

La empresa Mexicana **Cemex**, que es la cementera más grande de Latinoamérica y la segunda más grande de Colombia, **opera actualmente de forma ilegal en el Territorio Palestino Ocupado** y se lucra con la infraestructura de la ocupación israelí.

A través de su subsidiaria Readymix, Cemex tiene plantas de producción en varias colonias israelíes en Cisjordania ocupada, así como en los Altos del Golán sirios también ocupados. **El cemento y hormigón producidos por Cemex son utilizados en la construcción de estas colonias ilegales**, en puestos de control militar en Huwwara y Azzun-Atma, en partes del Muro israelí de Apartheid y en la construcción del tren ligero en Jerusalén, que conecta a las colonias ilegales de la ciudad y de Cisjordania.

Según datos de 2015, **la tierra que las colonias israelíes se han robado constituyen el 8.5% de Cisjordania**, más otro 28% donde el ejército israelí ha construido 210 bases militares y confiscado tierra por razones de “seguridad”; es decir, a los palestinos se les ha despojado el 36.5% de sus tierras en Cisjordania, que junto a Gaza componen la actual Palestina.

Todo lo anterior indica que **las operaciones de Cemex la implican en violaciones el Derecho Internacional y las obligaciones establecidas por la ONU para las empresas**. También contradicen muchas de sus posturas de ética empresarial y desarrollo sostenible.

Para conocer sobre la presencia de Cemex en Colombia les invitamos a escuchar esta emisión de [Onda Palestina](https://archivo.contagioradio.com/programas/onda-palestina/) en la cual se discuten además las noticias de la semana del territorio ocupado y del movimiento de Boicot Desinversiones y Sanciones a Israel, así como entrevistas, cultura y mucha musica.

<iframe id="audio_19643463" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_19643463_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>
