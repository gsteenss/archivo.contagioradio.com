Title: "Hablar de rebrote de Covid es iluso cuando ni se ha resuelto la primera ola de contagios" expertos de la salud
Date: 2020-09-26 11:06
Author: AdminContagio
Category: Actualidad, DDHH
Tags: Covid 19, Crisis en la Salud
Slug: hablar-de-rebrote-de-covid-es-iluso-cuando-ni-se-ha-resuelto-la-primera-ola-de-contagios-expertos-de-la-salud
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/09/Covid.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: Covid19 - Min Salud

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Mientras Colombia cierra el 24 de septiembre con 790.823 casos confirmados de Covid-19, 24.924 fallecidos y 89.282 casos activos; la economía se ha reactivado en medio de una nueva normalidad con un regreso a las calles de una ciudadanía sin garantías, en medio de cifras alarmantes que revelan cómo el país se ubica en el quinto lugar de naciones con mayor número de contagios tan solo después de Estados Unidos, India, Brasil y Rusia, según el reporte diario que realiza la Universidad Johns Hopkins.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

**Jaime Ordónez, médico y PhD en Epidemiología** reitera que el pico de la pandemia es un momento de duración indeterminada y que puede variar en cada país, condición que estará determinada por la velocidad a la que disminuya el número de casos nuevos y el número de muertes. Aunque ha existido una disminución en el número de casos nuevos y de muertes, el epidemiólogo señala que está relacionada en el número de pruebas que se ha reducido en comparación a las que se realizaban en julio y agosto.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Al respecto, la **doctora Marlene Vélez, presidenta del Colegio Nacional de Bacteriología** señala que al cambiar los protocolos, así mismo cambió la forma de hacer los diagnósticos, explicando que **las pruebas PCR ( por sus siglas en inglés de “Reacción en Cadena de la Polimersa”)**, que se han usado desde el estallido de la pandemia se les han incorporado las **pruebas rápidas que como su nombre indica, son más rápidas y sencillas.** Ambas sirven para comprobar si una persona está infectada o no por el Covid-19.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Por su parte, periodistas como Pascual Gaviria, que han hecho un juicioso seguimiento a la pandemia en el país, relatan que **del lunes 14 a domingo 20 de Septiembre se realizaron 23.513 pruebas PCR, mientras del 7 al 13 del mismo mes fueron 24.532, agregando que las pruebas rápidas de antígeno nivelaron la cifra entre ambas semanas.** Pese a los datos revelados por la Universidad John Hopkins y el mayor número de casos, la semana que terminó el promedio diario de muertes fue de 183 en Colombia, cayendo un 15% respecto a la semana anterior.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/rabodeajip/status/1308269692923138050","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/rabodeajip/status/1308269692923138050

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:heading {"level":3} -->

### Muchos muertos ha puesto el sector salud

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

**Según la Organización Panamericana de la Salud, América es el continente con más médicos muertos e infectados por Covid-19**, una lista encabezada por México, Estados Unidos y Brasil, sobrepasando los 2.500 fallecimientos y los 570.000 contagios, en el caso de Colombia para el mes de julio eran 27 los profesionales de la salud fallecidos debido a la enfermedad, y 2.726 personas contagiadas.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

La presidente del Colegio de Bacteriología, concuerdanen que el modelo económico ha incidido a que en la actualidad, no se respalde al sector salud, y mientras los salarios no han mejorado, los elementos de protección son escasos y se siguen realizando contrataciones por OPS, los profesionales también deben enfrentar la ansiedad de contagiar a sus familias o perder a sus colegas, "**hay lugares en los que pagan a los operarios 26.000 pesos diarios por trabajar en tiempos de pandemia, \[...\] el profesional de la salud no está siendo bien remunerado y esto es lo que ha puesto a descubierto esta pandemia"**. [(Lea también: Sí hay recursos para Avianca pero no para sectores más vulnerables)](https://archivo.contagioradio.com/si-hay-recursos-para-avianca-pero-no-para-sectores-mas-vulnerables/)

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### ¿Cuántas muertes diarias estamos dispuestos aceptar?

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Ante la reactivación económica y la reapertura de de restaurantes, cines, iglesias y otros lugares, el doctor Ordóñez señala que únicamente sucedió porque el país afronta la mayor crisis económica de la historia, que coincide con el peor momento de la pandemia, por lo que las precauciones deben reforzarse aún más, pues aunque hay actividades que estén autorizadas implican un alto riesgo de contagio.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Para sustentar el nivel riesgo, el epidemiólogo compara el promedio de muertes diarias por Covid de la semana del 13 al 19 de septiembre, que llegó a los 183 casos con las muertes registradas por el DANE de cáncer del primer trimestre del 2020 (130) y las muertes ocasionadas por infarto (96) cuya suma casi equipara a las ocasionadas por la pandemia diariamente en Colombia.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

"El 65% de los infectados son menores de 60 años y el 15% son niños y adolescentes, los jóvenes han puesto muertos y enfermos", agrega el doctor Ordóñez quien señala que no solo se trata de adquirir o no la enfermedad, sino de sus efectos, que se han extendido a una serie de discapacidades. Según los estudios, mientras el 88% de los pacientes presentan una pérdida del olfato y del gusto y el 80% lo recupera, existe otra porción de la población que lo pierde totalmente, mientras otro pequeño porcentaje adquirir discapacidades neuromotoras como cefalea, mialgias, mareo y fatiga.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

El doctor Ordóñez señala que aún no se puede hablar de un rebrote, pues en la mayor parte del continente aún no se ha llegado a valores endémicos, - es decir cuando la presencia de la enfermedad se da de una forma muy baja - en contraste con otras regiones como Europa, Asia y países como Australia y Canadá, donde ya se ha vivido una segunda ola del virus mucho más fuerte que la primera.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Agrega que de las 34 naciones que han logrado alcanzar esta fase de rebrote, el país con mayor mortalidad reportó 2 muertes por un millón de habitantes, que en Colombia equivaldría a 14 muertes diarias, lo que contrasta con las altas cifras que se viven en la actualidad.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Países como **Israel experimentaron un incremento de 50 veces en los casos de coronavirus, pasando de casi 20 contagios nuevos por día a más de 1.000 tras reactivar** restaurantes, centros comerciales y playas. [(Lea también: Colombia registró la mayor tasa de muertes por Covid-19 del mundo)](https://archivo.contagioradio.com/colombia-registro-la-mayor-tasa-de-muertes-por-covid-19-del-mundo/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

"Con toda seguridad habrá un rebrote, nos estamos acostumbrando al virus y nos olvidamos de las medidas de precaución" reitera la doctora Marlene Vélez, quien recuerda, medidas como no usar aire acondicionado, evitar aglomeraciones, mantener la distancia, aislarse al sentir cualquier síntoma, usar el tapabocas para convertirlo en otro elemento diario.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Las consecuencias de la Covid en la salud mental

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Además de los trastornos descritos a nivel físico, según **Jorge Téllez, fundador de la Asociación Colombiana de Psiquiatría Biológica**, los expertos en salud mental ya han visto episodios de estrés postraumáticos y alteraciones de la memoria, mientras en el sector salud, existirán respuestas emocionales que van a llevar a trastornos depresivos, de conductas suicidas, a los que aún no hay a dónde consultar ni tratar, pues aumentar el número de psiquiatras o psicólogos no parece una opción considerada por el [Ministerio de Salud.](https://twitter.com/MinSaludCol)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Se trata de un problema de comportamiento que parte del miedo al contagio y aunque este temor se haya disminuido "porque ya no tenemos las imágenes de muertes de lugares como Guayaquil o Italia, el problema está en que no va a ser fácil que todos los individuos lo asuman". Agrega que esta mortalidad fue asociada a un grupo especial de personas como personas con problemas cardiovasculares o a la tercera edad, por lo que hay una población que no tiene la capacidad de comprender que también son portadore, por lo que es necesario crear hábitos, sin embargo, esto requiere un tiempo para ser interiorizado.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

A largo plazo, sugiere que las consecuencias del Covid-19 en el comportamiento y en la salud mental de la humanidad, advertencia que ya lanzó la Organización Mundial de la Salud, llevará a una pandemia de problemas emocionales, aumentos de suicidio, **"jugamos a que los colombianos estamos en una situación de resiliencia y que siempre salimos de los grandes problemas y eso no es cierto, tenemos que pensar que no tenemos la estructura, requerimos un sistema de salud que nos cuide".**

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
