Title: 5 grupos armados se disputan control en el sur de Córdoba
Date: 2018-10-23 18:37
Author: AdminContagio
Category: DDHH, Entrevistas
Tags: Comisión de Verificación, cordoba, Derechos Humanos, grupos armados, Ministerio de Interior
Slug: grupos-armados-disputan-cordoba
Status: published

###### [Foto: Contagio Radio] 

###### [19 Oct 2018] 

La F**undación Social Cordobería denunció que este fin de semana fue asesinado en San José de Uré, un joven de 23 años** y otro de 21 años fue herido; no obstante, la violencia no es extraña en el municipio pues según cifras de la organización, **los asesinatos en la zona se han incrementado un 400%** desde hace 8 meses, mientras el número de desplazados asciende a 500 personas.

**Andrés Chica, vocero de Cordobería,** recordó que desde diciembre del año pasado se han emitido una serie de denuncias por parte de la Defensoría del Pueblo y otras instituciones, porque el sur de Córdoba, especialmente, San José de Uré, se encuentra en medio de una disputa entre 5 grupos armados ilegales, por lo que no resulta extraño el asesinato del joven el pasado domingo "a escasos metros de la estación de policía".

### **San José de Uré: El último municipio fundado en Córdoba** 

Según Chica,** el municipio se encuentra en medio de una disputa territorial ** entre Clan del Golfo, Autodefensas Gaitanistas de Colombia, Los Caparrapos, hombres armados al servicio del Cartel de Sinaloa, y 3 estructuras irregulares integradas por ex miembros de las FARC que no se reconocen como disidencias; por ser un corredor estratégico que lleva al sur de Bolívar y conecta con el Golfo de Urabá, el Bajo Cauca y el nudo del Paramillo.

La presencia de estos actores armados ha generado situaciones como las extorsiones, el incremento de minería ilegal, y el aumento de hectáreas sembradas de cultivos de uso ilícito, que como aclara Chica, se han sembrado por presión de grupos armados a las familias cultivadoras. Adicionalmente, se presenta un **aumento del 400% de los asesinatos en menos de 8 meses, y el desplazamiento de más de 500 personas en el Municipio.**

Situaciones que tienen sumida a la población en un crisis humanitaria, en uno de los cinco municipios priorizados por el Programa Integral de Seguridad y Protección para Comunidades y Organizaciones en los Territorios, creado por el Gobierno Nacional. (Le puede interesar:["Asesinado líder de sustitución de sustitución de cultivos en San José de Uré"](https://archivo.contagioradio.com/asesinado-lider-de-sustitucion-de-cultivos-en-san-jose-de-ure-cordoba/))

### **La comunidad se resiste a la guerra** 

El Ministerio de Interior visitará San José de Uré, para realizar una Comisión de Verificación en la zona, acción de presencia estatal que, según los habitantes, no es recurrente en la región: **"Tenemos una ciudadanía que está en medio de una situación delicada mientras el Gobierno se limita a anotar cosas en papeles".**

A pesar de esta condición, los pobladores no pierden su capacidad organizativa para afrontar el conflicto, ejemplo de ellos es la reciente **instalación de la plataforma municipal de derechos humanos, y del concejo de paz, convivencia y reconciliación,** tratando de fortalecer la participación ciudadana, y la organización civil. (Le puede interesar: ["Asesinan a Plinio Pulgarin, líder comunal en San José de Uré"](https://archivo.contagioradio.com/asesinan-a-plinio-pulgarin-lider-comunal/))

[Fundación Cordobería grave situación en materia de derechos humanos en Córdoba](https://www.scribd.com/document/391450645/Fundacion-Cordoberia-grave-situacion-en-materia-de-derechos-humanos-en-Cordoba#from_embed "View Fundación Cordobería grave situación en materia de derechos humanos en Córdoba on Scribd") by [Contagioradio](https://www.scribd.com/user/276652802/Contagioradio#from_embed "View Contagioradio's profile on Scribd") on Scribd

<iframe id="doc_60183" class="scribd_iframe_embed" title="Fundación Cordobería grave situación en materia de derechos humanos en Córdoba" src="https://www.scribd.com/embeds/391450645/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-EAnc36IBH6xRzsZt6JJ4&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="0.7729220222793488"></iframe>

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
