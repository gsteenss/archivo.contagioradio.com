Title: "Extrema derecha teme asumir acuerdo de justicia"
Date: 2015-09-28 15:25
Category: Nacional, Paz
Tags: Acuerdo de justicia farc y gobierno, acuerdo de justicia firmado en la habana, Délitos de lesa humanidad, Justicia restaurativa, justicia transicional, posconflicto, Radio de derechos humanos, Responsabilidades de empresarios en conflicto armado, Responsabilidades de ex presidentes en conflicto armado, Responsabilidades de ganaderos en conflicto armado, Responsabilidades de políticos en conflicto armado
Slug: extrema-derecha-teme-asumir-acuerdo-de-justicia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/Lozano.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: www.colmundoradio.com.co 

<iframe src="http://www.ivoox.com/player_ek_8666400_2_1.html?data=mZujmJmUdI6ZmKiak5iJd6Kol5KSmaiRdo6ZmKiakpKJe6ShkZKSmaiRaZOmtIqwlYqliIzl1srfx9LTt4zgwpDdw9%2BPuNDY0NiY1srSqc7j1JDe18qPpcTo1sbfjcjTsozc1tLWzpKJe6ShpNTb1sbLrdCfs8bRy9SPcYarpJKh&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Carlos Lozano, Semanario Voz] 

###### [28 Sept 2015] 

[En una entrevista para la casa editorial El Tiempo, Enrique Santos afirma que **juzgar a ex presidentes, generales, banqueros y latifundistas por sus responsabilidades en la comisión de delitos de lesa humanidad** es una "salida torpe, arrogante y contraproducente" que "no se compadece con el espíritu de reconciliación que reclama el papa Francisco para este proceso".]

[Al respecto, Carlos Lozano, director del Semanario Voz, asegura que esta es una muestra del **temor que siente la extrema derecha y los sectores guerreristas** de tener que asumir la **obligación y revelar todos los secretos que conocen** para el esclarecimiento de múltiples hechos en los que han participado como autores intelectuales.  ]

[Teniendo en cuenta que el **acuerdo de justicia** firmado en La Habana contempla **verdad, justicia y reparación, más allá de una visión punitiva**, sectores asociados a la extrema derecha y al aparato gubernamental han afirmado su desacuerdo con lo pactado hasta el momento, pues su intención era ver en la cárcel a las FARC, asevera Lozano. ]

[Lozano afirma que las palabras de Santos reflejan las tensiones del aparato gubernamental respecto al acuerdo de justicia. La pretensión inicial del Gobierno era aprobar la aplicación de justicia transicional de forma unilateral, sancionatoria únicamente para la guerrilla. Sin embargo, lo que se ha aprobado es un **acuerdo de justicia restaurativa multilateral, para juzgar a todos los actores del conflicto, teniendo en cuenta que se estableció una comisión para el esclarecimiento de la verdad.**]

[Lozano indica que la firma de este acuerdo de justicia implica un punto de no retorno, con **exigencias** tanto para las partes en negociación como para la sociedad colombiana, **con el fin de asegurar que este proceso dé resultados** y pueda ser ejemplo para otros contextos.]

[Pese a que tanto el Gobierno como las FARC tienen posiciones distintas, en aspectos como los hechos que han afectado el proceso de paz, es necesario **para la consolidación de la paz que se diriman las actitudes arrogantes y prepotentes**, como las de Juan Manuel Santos, para posibilitar "un acto de reencuentro y reconciliación en función de la paz" pues "sí queremos la paz todos debemos actuar con humildad... Con mucho **sentido constructivo para abrir el camino a la solución política del conflicto colombiano**", concluye Carlos Lozano. ]
