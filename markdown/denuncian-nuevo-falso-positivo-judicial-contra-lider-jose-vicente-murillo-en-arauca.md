Title: Denuncian nuevo falso positivo judicial contra líder José Vicente Murillo, en Arauca
Date: 2019-12-09 18:28
Author: CtgAdm
Category: Judicial, Líderes sociales
Tags: Arauca, Dirigentes Sociales, Falsos Positivos Judiciales, lideres sociales, Saravena
Slug: denuncian-nuevo-falso-positivo-judicial-contra-lider-jose-vicente-murillo-en-arauca
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/Falsos-positivos-en-Arauca.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Twitter.] 

[Un nuevo **falso positivo judicial se presentó el día 07 de diciembre contra el líder social José Vicente Murillo**, capturado por la fuerza pública en el municipio de Saravena, Arauca. Murillo hace parte del Movimiento Político Masa Social y Popular del Centro Oriente de Colombia (MPMSPCOC) y es vocero del Congreso de los Pueblos.]

[Para Sonia López, dirigente social y vocera del MPMSPCOC, la razón de la captura tiene que ver con el trabajo que ha venido desempeñando el Líder en la región desde hace varios años, “denunciando la responsabilidad de las empresas petroleras en la violación a los derechos humanos, crisis social y ambiental en que vive el Departamento”.][(Le puede interesar: Diálogo por la verdad de líderes sociales asesinados llega a Arauca)](https://archivo.contagioradio.com/dialogo-por-la-verdad-de-lideres-sociales-asesinados-llega-a-arauca/)

[En Arauca —continúa López—, “se persigue a los movimientos que nos oponemos al saqueo y al despojo de nuestra tierra”. Este tipo de organizaciones buscan mejorar la calidad de vida de las personas en el Departamento, al tiempo que hacen veeduría ciudadana en materia ambiental. ]

### **Ser líder social en Arauca es estigmatizado por las autoridades** 

[Esta no es la primera vez que José Vicente Trujillo sufre de un intento de falso positivo judicial. “El compañero ya había sido detenido en años anteriores por la fuerza pública y siguió recibiendo amenazas por parte del Estado y por miembros de las Autodefensas Gaitanistas” afirmó la vocera.]

[Al líder social se le imputan los delitos de rebelión, concierto para delinquir, secuestro simple y obstrucción de vías públicas. La captura fue ejecutada por la Unidad de Desmantelamiento de Organizaciones Criminales de la Fiscalía General, que ha sido objeto de denuncia por parte de líderes y dirigentes sociales de la región. ]

[La captura de José Vicente Murillo fue ordenada por "un juez de control de garantías de Arauca y solicitada por la Unidad de Desmantelamiento de Organizaciones Criminales de la Fiscalía General de la Nación. Esta unidad ha tenido varias críticas y denuncias desde el movimiento social y de los derechos humanos. Esta fue creada gracias a la cooperación entre el Estado y Ecopetrol, para investigar hechos relacionados con temas energéticos y petroleros en la región” explicó López. ]

### **Una lista de falsos positivos judiciales en la región ** 

[El caso de Murillo no es el único, ya que **se han venido presentando varios arrestos contra líderes y dirigentes sociales en la región.** El mismo día del arresto del líder social, otro dirigente, Jorge Enrique Niño Torres, fue capturado en el municipio de Arauquita. De igual forma, Hermes José Burgos, un dirigente campesino, fue arrestado hace un año y sigue privado de su libertad. ]

[A pesar de la complejidad del caso, López reafirma el compromiso de las organizaciones en defensa de sus dirigente: "Nos disponemos a defenderlo jurídicamente en el estrado judicial y en el proceso penal en su contra. De igual forma, seguiremos adelantando las acciones políticas de movilización que sean necesarias para seguir protegiendo nuestro territorio, para que ese legado y esas banderas de lucha, por las que fueron encarcelados los compañeros, sigan vivas”. ]

### **La situación de los dirigentes sociales y sus batallas** 

[Debido al trabajo de los dirigentes sociales en el territorio, impulsando iniciativas de consultas populares en los municipios de Arauquita, Saravena y Fortul, las empresas petroleras no han visto con buenos ojos su trabajo. Debido a esta, **la Unidad Especial de la Fiscalía se**]**“ha ensañado con el movimiento social y ha adelantado distintos procesos contra líderes en el Departamento”.**

[Todo aquello llevó a los dirigentes, incluído Murillo, a “movilizarse desde finales del mes de abril y todo el mes de mayo contra las empresas petroleras, para que se garantizarán condiciones laborales dignas para los trabajadores de esa región, inversión para mitigar el impacto ambiental y responsabilidad social”, afirmó López.][(Lea también: Tras una semana en cautiverio, fue liberada Andrea Ardila, lideresa de Saravena, Arauca)](https://archivo.contagioradio.com/andrea-ardila-lideresa-de-saravena-arauca-completa-una-semana-desaparecida/)

[Todos los dirigentes han sido arrestados bajos cargos similares por defender su territorio, pertenecer a asociaciones campesinas, estar en contra de muchas medidas llevadas a cabo por las petroleras y la defensa contra las violaciones a los derechos humanos. “Ha sido una persecución constante y el mismo compañero Murillo la denunció”.]

[A pesar de las constantes agresiones, el trabajo que llevan los líderes en la región ha sido “ratificado desde las organizaciones que conforman nuestros movimientos —complementó López—. **Somos conscientes de los riesgos que corremos los que desarrollamos este tipo de labores en defensa de los derechos colectivos.** Pero ni la cárcel, ni la muerte, ni la estigmatización van hacer que esa lucha que se desarrolla diariamente caiga”. ]

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe><iframe id="audio_45287645" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_45287645_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
