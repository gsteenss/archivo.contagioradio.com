Title: "TDK del 90", rap de vida, para oídos sordos
Date: 2015-09-23 13:14
Category: Sonidos Urbanos
Tags: Hip hop bogotano, La Colosa canción, Rap, TDK del 90
Slug: tdk-del-90-rap-de-vida-para-oidos-sordos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/tdk-del-90-e1443127050818.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [<iframe src="http://www.ivoox.com/player_ek_8568418_2_1.html?data=mZqjmpmVfI6ZmKial56Jd6KolpKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRmKW%2FjMnSzpCddIampJDfw9WPqMaf187Rw4qWh4zkwtfOjdSJh5SZoqnR0diPt9DmxdTgj4qbh4630NPhw8zNs4zGwsnW0ZCRaZi3jpk%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>]

###### [TDK del 90 en "A la Calle"] 

###### [23 Sep 2015] 

TDK del 90 es una banda Bogotana que nace de los sonidos del Rap, en particular el producido en los años 90, que consideran la mejor y última década del género,[ por influencia de agrupaciones como “A tribe called cuest”, “Big Pun” y “The roots”, entre otros.]{.text_exposed_show}

[TDK del 90 plantea un modo diferente de entender la música, el grafiti y la cultura urbana Bogotana y de la fría montaña, fusionando el rap desde el 2010 con Funk, Electrónica y Hardcore, entre otros. Hacen letras de la ciudad y sus historias, combate al manejable y al aburrido. Cantándole” Despabilo” o “Generación y Mente”.]{.text_exposed_show}

![tdk contagio](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/tdk-contagio-e1443032638544.jpg){.wp-image-14535 .aligncenter width="604" height="285"}

Su canción "La Colosa", hace parte de un documental que se estrenará en 2016, donde se denuncia la explotación de oro realizada por la empresa Anglo Gold Ashanti en Cajamarca, Departamento del Tolima. En la actualidad, la banda está preseleccionada por concurso para ser parte del Festival "La coneja ciega" en su tercer edición.

<iframe src="https://www.youtube.com/embed/2tSzdePvbyg" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

[Facebook.com/TDKdel90](http://Facebook.com/TDKdel90)
