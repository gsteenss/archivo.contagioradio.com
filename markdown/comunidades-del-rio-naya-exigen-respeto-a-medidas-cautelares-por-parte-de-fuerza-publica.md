Title: Comunidades del Río Naya exigen respeto a medidas cautelares por parte de Fuerza Pública
Date: 2018-06-12 17:11
Category: DDHH, Movilización
Tags: buenaventura, Cuenca del Río Naya, Fuerza Pública, Valle del Cauca
Slug: comunidades-del-rio-naya-exigen-respeto-a-medidas-cautelares-por-parte-de-fuerza-publica
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/03/invierno-naya.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [12 Jun 2018] 

Debido a la fuerte presencia de grupos armados, autoridades **anunciaron la militarización de la Cuenca del Río Naya y un puesto de control en Puerto Merizalde**, Valle del Cauca. Acciones que para las comunidades podrían violar las medidas cautelares que se establecieron sobre los habitantes de la región e incrementar el riesgo sobre las garantías para sus vidas y permanencia del territorio.

El aumento de disponibles del Ejército en el río Naya se dio a conocer en el último consejo de seguridad, que se desarrolló en Buenaventura y que contó con la participación de la gobernadora del Valle, Dilian Francisca Toro; el gobernador del Cauca, Óscar Campo; el comando de Policía, la Defensoría del Pueblo, un comando del Ejército, entre otras autoridades.

De acuerdo con Isabelino Valencia, representante legal del Consejo Mayor del Naya, el principal temor no tiene que ver con la presencia de la Fuerza Pública ni con el puesto de control que se quiere instalar, **sino con la falta de resultados frente a la disminución de la violencia en la región**. (Le puede interesar:["Crítica situación de derechos humanos en el Río Naya")](https://archivo.contagioradio.com/critica-situacion-de-derechos-humanos-en-el-naya/)

"Hace 17 años que la Fuerza Pública hace presencia en el territorio, por qué desde esa época a la fecha hay 48 desaparecidos y **por qué el Naya se esta llamando la ruta oculta de la cocaina"** afirmó Valencia. Además señaló que oficialmente se le ha pedido tanto al Ejército como a la Policía y Armada, que cualquier acción que vayan a realizar debe respetar las medidas cautelares interpuestas por la CIDH.

En ese sentido los habitantes están exigiendo que se respete el derecho a la consulta previa frente a las decisiones que se tomen en su territorio y el mandato de la ley 70 que expresa que el máximo órgano **de toma de decisión de las comunidades afro e indígenas serán los consejos comunitarios.**

<iframe id="audio_26501031" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_26501031_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
