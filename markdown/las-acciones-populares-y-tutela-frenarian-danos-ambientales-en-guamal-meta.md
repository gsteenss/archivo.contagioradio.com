Title: Acciones populares y tutela frenarían daños ambientales en Guamal, Meta
Date: 2017-09-18 16:30
Category: Ambiente, Nacional
Tags: Ecopetrol, Guamal
Slug: las-acciones-populares-y-tutela-frenarian-danos-ambientales-en-guamal-meta
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/07/Guamal.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Guamal ] 

###### [18 Sept 2017] 

Habitantes de Guamal han interpuesto acciones de tutela y una acción popular para que Trogón I y Lorito I, proyectos de exploración petrolera de Ecopetrol, cumplan con la licencia ambiental y deje de provocar, **de acuerdo con la comunidad, afectaciones en el ambiente y la producción agrícola del municipio**.

Diego Salcedo, abogado y asesor de la comunidad afirma que la acción popular va bastante avanzada y sobre ella la Procuraduría solicitó una revisión de 8 puntos por incumplimiento a la licencia ambiental otorgada, y señaló que la falta de acciones por parte de Ecopetrol es un incumplimiento. (Le puede interesar: ["Tutela busca frenar actividades petroleras de Ecopetrol en Guamal, Meta")](https://archivo.contagioradio.com/tutela-busca-frenar-actividades-petroleras-de-ecopetrol-en-guamal-meta/)

“Somos una comunidad de campesinos que estamos pidiendo que se cumpla una licencia ambiental frente a unas obligaciones previas, se ha modificado el esquema territorial dos veces, se han expedido decretos prohibiendo un día al otro se aprueba, **siempre vulnerando a la comunidad** afirmó Salcedo.

### **El accionar de la Fuerza Pública contra comunidad de Guamal** 

Una de las acciones que ha tomado la comunidad de Guamal para defender su territorio, es la movilización pacífica, en donde para Salcedo ha existido un uso desmedido hacia los habitantes, que ha pasado desde las detenciones arbitrarias, hasta el cierre de vías.

“**El sábado pasado las personas estuvieron más de tres horas retenidas, porque como no cumplieron con las adecuaciones viales**, cuando ingresaron la maquinaria tumbaron árboles” señaló Salcedo.

Otro de los hechos que se registró fue la detención de Sandra Meneses y Liliana Rueda, por los delitos de obstrucción a vías públicas, en este caso Salcedo manifestó que ambas fueron presentadas a la Fiscalía **3 horas después de su retención en Acacias, cuando el recorrido usualmente demora solo 15 minutos**, violando protocolos de legalización de capturas.

### **Las acciones de las instituciones no han sido suficientes** 

Tanto la ANLA como Cormacarena, tendrán una reunión con representantes de la comunidad en la que se espera que, se tome en cuenta la solicitud del Procurador y se ordene la suspensión temporal hasta tanto se cumpla la revisión de la licencia ambiental. La reunión se realizaría el próximo 21 de septiembre. (Le puede interesar: ["Ecopetrol debería suspender proyecto Trógon I en Guamal, Meta"](https://archivo.contagioradio.com/46238/))

###### Reciba toda la información de Contagio Radio en [[su correo]
