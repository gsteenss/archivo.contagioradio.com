Title: Defendamos la Paz pide a Gobierno y ELN reanudar  diálogos
Date: 2020-04-16 20:51
Author: CtgAdm
Category: Actualidad, Nacional, Paz
Tags: Defendamos la Paz, ELN
Slug: defendamos-la-paz-pide-a-gobierno-y-eln-reanudar-dialogos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/DefendamosPaz.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: Defendamos la Paz

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Recogiendo el sentir de diversos sectores de la sociedad y en particular de las regiones más golpeadas por la violencia, [la plataforma Defendamos la Paz](https://twitter.com/DefendamosPaz) instará este viernes a través de un encuentro virtual a reanudar los esfuerzos por una salida negociada del conflicto armado con el ELN.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Con el gesto humanitario del **ELN, al establecer un cese al fuego unilateral en el territorio nacional durante un mes,** como respuesta a la crisis que vive Colombia y el mundo ante la pandemia del Covid-19, Defendamos la Paz reconoce que hay una posibilidad de crear las condiciones para retomar un diálogo entre el Gobierno y la guerrilla. [(Le puede interesar: Cese al fuego bilateral una oportunidad para combatir el Covid-19: Defendamos la Paz)](https://archivo.contagioradio.com/cese-fuego-bilateral-defendamos-la-paz/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Así mismo la plataforma reconoce la decisión del Gobierno de nombrar a Francisco Torres y a Carlos Velandia como gestores de paz, pese a que el ELN difiere de esta asignación, Defendamos la Paz, sugiere que estas acciones podrían representar el inicio de un camino a la comunicación.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Cabe resaltar que Pablo Beltrán, jefe de la Delegación de paz del ELN reconoció **la posibilidad de que después del 1 de mayo, fecha en que concluye el cese unilateral activo, se haga una propuesta para extender este alto a la violencia y convertirlo en un cese al fuego bilateral,** algo de lo que también dependerá el avance del Covid-19 en el país y de la actitud del gobierno frente a esas voces que piden cese bilateral. [(Lea también: Es necesario negociar un cese bilateral: ELN)](https://archivo.contagioradio.com/en-necesario-negociar-un-cese-bilateral-eln/)

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Gestos de paz son el camino

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Antes estas muestras de voluntad, Defendamos la Paz solicita a ambas partes **"no dilapidar la oportunidad histórica de finalizar el conflicto colombiano"** y continuar en la búsqueda de alternativas para la reanudación de un diálogo y avanzar en gestos que permitan se construya confianza que permitan el nacimiento de acuerdos humanitarios que disminuyan la intensidad del conflicto en las regiones de Colombia. [(Lea también: Cese unilateral al fuego del ELN, una oportunidad para la paz)](https://archivo.contagioradio.com/cese-unilateral-al-fuego-del-eln-una-oportunidad-para-la-paz/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

De este modo Defendamos la Paz buscará dar luces de los avances que han existido en la búsqueda de una solución pacífica, como lo han sido los acercamientos desde 2014 que han permitido un cese bilateral por cien días y pequeños avances en una metodología para la participación de la sociedad. A esta encuentro se sumarán representantes de la comunidad internacional, la iglesia, la sociedad civil, los exnegociadores del proceso y representantes de las comunidades de **Cauca, Chocó y Catatumbo,**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El encuentro virtual será este viernes 17 de abril a las 10:00 en el facebook de Defendamos la Paz: https://www.facebook.com/DefendamosLaPazColombia

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->
