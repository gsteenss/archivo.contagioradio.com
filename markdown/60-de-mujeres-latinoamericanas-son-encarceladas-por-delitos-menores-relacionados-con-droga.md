Title: 60% de mujeres latinoamericanas son encarceladas por delitos menores relacionados con droga
Date: 2018-01-26 12:53
Category: DDHH, Nacional
Tags: cárceles mujeres, delitos de droga, droga, Latinoamérica, mujeres, WOLA
Slug: 60-de-mujeres-latinoamericanas-son-encarceladas-por-delitos-menores-relacionados-con-droga
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/01/Captura-de-pantalla-2018-01-26-a-las-12.31.30-p.m..png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: WOLA] 

###### [26 Ene 2018] 

La Oficina en Washington para Asuntos Latinoamericano WOLA, alertó sobre la grave situación que viven las mujeres encarceladas en diferentes países de las Américas. Indicó que **el 60% de la población carcelaria femenina** está allí por delitos menores relacionados con droga. Ante esto, propusieron una guía de reforma política donde este tipo de delitos no sean sancionados con la privación de la libertad.

Los países más afectados son **Brasil, Argentina y Costa Rica** y las mujeres allí “tienen poca educación, viven en condiciones de pobreza y son responsables del cuidado de personas dependientes como niños, jóvenes, personas de mayor edad o con discapacidad”. Estas mujeres, según la organización, “son rara vez una verdadera amenaza para la sociedad” y se llevan la peor parte de las políticas punitivas.

Las mujeres están siendo encarceladas por delitos como **distribución de droga a pequeña escala** o por transportar las mismas. Indica WOLA que “Su encarcelamiento poco o nada contribuye a desmantelar los mercados ilegales de drogas y a mejorar la seguridad pública” y por el contrario “la prisión suele empeorar la situación, dado que reduce la posibilidad de que encuentren un empleo decente y legal cuando recuperan la libertad”. (Le puede interesar:["Prisioneros políticos denunciaron accionar desmedido por parte del INPEC en la Picota"](https://archivo.contagioradio.com/48955/))

### **El número de mujeres detenidas en Latinoamérica por delitos de droga es el mayor del mundo** 

Los niveles de encarcelamiento de mujeres asociados a este delito han crecido a un ritmo acelerado. WOLA manifiesta que América Latina, junto con Asia, **tienen los niveles más altos de encarcelamiento** femenino que cualquier otra región del mundo. Hacen referencia a las cifras del el Institute for Criminal Policy Research quien indica que en la región el fenómeno ha aumentado en un 51,6% entre el 2000 y el 2015.

Esto responde a que las leyes de droga son **extremamente punitivas** y hay una desproporción en la imposición de penas. Esto para “los delitos que cometen las mujeres para obtener drogas para el consumo, como su involucramiento en delitos relacionados con la producción, la distribución, el suministro y la venta de drogas, tienen que ver, a menudo, con la exclusión social, la pobreza y la violencia de género.”

En cifras, Wola indica que en Argentina, las mujeres encarceladas por estos delitos aumentó un 271% entre 1989 y 2005 y en Brasil hubo un aumento del 290% entre 2005 y 2013. Para el caso de Colombia, el **76% de las internas no han podido concluir con su educación secundaria** y en su mayoría son madres solteras, fenómeno que se repite también en Costa Rica.

“Las explicaciones de las mujeres que ingresan a la cárcel por delitos de drogas son muy diversas, y es recomendable tomar en serio y explorar más lo que ellas dicen”, indica el informe. Además, **muchas son obligadas por sus parejas** a cometer estos delitos o son engañadas por algunas personas. (Le puede interesar: ["FARC confía en que se cumpla liberación de presos políticos"](https://archivo.contagioradio.com/44063/))

### **Recomendaciones para la reforma de políticas carcelarias** 

Para superar esta problemática, WOLA manifestó que es necesario que **“se revisen las políticas punitivas** de manera que los delitos de bajo nivel o no violentos cometidos por mujeres u hombres, no se penalicen con cárcel”. Además, es necesario que haya una proporcionalidad en las penas y que los sistemas de justicia penal tengan en cuenta atenuantes como que las mujeres cuidan de sus hijos menores de edad o a otros familiares.

Indican que debe implementarse **medidas alternativas a la prisión**, políticas más incluyentes, reformas a la política de drogas y programas de inclusión social entre otros. Recomiendan que las políticas tengan un enfoque de género y que se basen en el establecimiento de un problema de salud pública “con estricto apego a las obligaciones de los Estados en materia de derechos humanos e inclusión social”.

[WOLA cárceles](https://www.scribd.com/document/370061951/WOLA-ca-rceles#from_embed "View WOLA cárceles on Scribd") by [Contagioradio](https://www.scribd.com/user/276652802/Contagioradio#from_embed "View Contagioradio's profile on Scribd") on Scribd

<iframe id="doc_41186" class="scribd_iframe_embed" title="WOLA cárceles" src="https://www.scribd.com/embeds/370061951/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-YO4nRglqU3pIUPiyR1PF&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="0.8003549245785271"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
