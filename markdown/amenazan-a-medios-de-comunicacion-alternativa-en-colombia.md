Title: Amenazan a medios de comunicación alternativa en Colombia
Date: 2014-12-12 14:22
Author: CtgAdm
Category: Uncategorized
Slug: amenazan-a-medios-de-comunicacion-alternativa-en-colombia
Status: published

[![logo peque contagio radio](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/04/logo-peque-contagio-radio-e1474085425460.png){.aligncenter .size-full .wp-image-22140 width="128" height="50"}](https://archivo.contagioradio.com/amenazan-a-medios-de-comunicacion-alternativa-en-colombia/logo-peque-contagio-radio/)

Los elementos gráficos con que se firma la amenaza contra medios de comunicación alternativos, es utilizado por quienes han pretendido robar las tierras de los despojados en el Magdalena Medio y por mineros en Suarez, Cauca, lo que sugiere para Ignacio Gómez, de la FLIP, que el nombre “águilas negras” es un nombre utilizado por diferentes personas o grupos no necesariamente paramilitares, para amenazar a personas en diferentes circunstancias.

Los paramilitares son una organización que cumplió su ciclo histórico. El propósito de estos nuevos grupos es, por decir, mucho mas sincero en el sentido de hacer comercio con el narcotráfico y no política”, afirma Gómez.  Según este análisis, hay personas que quieren silenciar a un grupo de periodistas, pero no se puede entender como la existencia de un grupo paramilitar.

Finalmente, señala que desde la Fundación para la Libertad de Prensa esperamos que no logren silenciar a estos medios.
