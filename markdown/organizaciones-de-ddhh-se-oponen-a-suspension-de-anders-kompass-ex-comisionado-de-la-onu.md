Title: Organizaciones de DDHH se oponen a suspensión de Anders Kompass ex comisionado de la ONU
Date: 2015-05-05 16:26
Author: CtgAdm
Category: DDHH, Entrevistas
Tags: Anders Kompass, Ban Ki Moon, Claudia Samayoa, Democracia y Desarrollo, ejecuciones extrajudiciales en Colombia, Naciones Unidas para los Derechos Humanos, Oficina de Operaciones de la OACDH, Plataforma Interamericana por los Derechos Humanos, República Centro Africana, UDEFEGUA
Slug: organizaciones-de-ddhh-se-oponen-a-suspension-de-anders-kompass-ex-comisionado-de-la-onu
Status: published

###### Foto: [www.yzgeneration.com]

**Anders Kompass,** quien se desempeñó como delegado de la Oficina del Alto Comisionado de las **Naciones Unidas para los Derechos Humanos** en varios países de América Latina y contribuyó, según varias organizaciones, al cambio de filosofía de la oficina, que le apostó al acompañamiento a las organizaciones defensoras, fue suspendido de su cargo como Jefe de la **Oficina de Operaciones de la OACDH**, luego de entregar a la fiscalía francesa un informe sobre agresiones sexuales por parte de las FFMM de ese país.

Según las organizaciones de DDHH que hacen parte de la **Plataforma Interamericana por los Derechos Humanos, Democracia y Desarrollo** entre otras, Kompass hizo un gran aporte en el cambio de visión del mandato de las Naciones Unidas. Según **Claudia Samayoa**, de **UDEFEGUA** en Guatemala, en la ONUDH hay una tendencia que impulsa las labores de la ONU hacia talleres y cosas poco operativas, y Kompass impulsó el compromiso de esa instancia con el cese de la continuidad y sistematicidad de las violaciones a los DDHH.

Samayoa explica que Kompass en el cargo del Jefe de Operaciones, se desempeñó con una labor que resalta la necesidad de que la oficina se acerque más a las víctimas, por ejemplo, en los casos de las **ejecuciones extrajudiciales en Colombia** y otras situaciones en el resto de América Latina.

Para Samayoa la suspensión del funcionario de la ONU tiene que ver, no solamente con el informe sobre las **[violaciones sexuales por parte de militares franceses](https://archivo.contagioradio.com/informe-de-la-onu-acusa-a-ejercito-frances-de-abusos-sexuales-en-africa/)** en la **República Centro Africana** sino con toda la labor realizada en las oficinas en las que se desempeñó. La idea de Kompass, al entregar el informe a la fiscalía, era interpretar el mandato de las Naciones Unidas en el sentido en que es más importante que dejen de violar los niños, por encima de la confidencialidad de un documento.

Las organizaciones de Derechos Humanos, han dirigido una carta al Secretario General **Ban Ki Moon,** en la que exigen celeridad en la investigación de la ONU, transparente y objetiva, puesto que están seguros y seguras que en el transcurso de la investigación se confirmará que Kompass estaba cumpliendo con su labor y no reveló el informe a la prensa sino que se lo entregó al gobierno, cumpliendo con el reglamento y el conducto establecido.

Anexamos el documento de las Organizaciones de DDHH

[Carta a Ban Ki Moon contra suspensión de Anders Kompass](https://es.scribd.com/doc/264292056/Carta-a-Ban-Ki-Moon-contra-suspension-de-Anders-Kompass "View Carta a Ban Ki Moon contra suspensión de Anders Kompass on Scribd")

<iframe id="doc_56627" class="scribd_iframe_embed" src="https://www.scribd.com/embeds/264292056/content?start_page=1&amp;view_mode=scroll&amp;show_recommendations=true" width="75%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="undefined"></iframe>
