Title: En 2014 cada 7 días fue asesinado un defensor o defensora de DDHH
Date: 2015-02-18 19:45
Author: CtgAdm
Category: DDHH, Entrevistas
Tags: defensor de derechos humanos, informe divina comedia, situación de defensores de ddhh
Slug: en-2014-cada-7-dias-fue-asesinado-un-defensor-o-defensora-de-ddhh
Status: published

##### Foto: Somos Defensores 

<iframe src="http://www.ivoox.com/player_ek_4102739_2_1.html?data=lZadlJyXfY6ZmKialJ6Jd6KnmpKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRic%2Bfk5WelpDHpcXVjJyYxoqnd4a1pcbgjcvZqYzV1Mrgy9PFqNCf1tOYxsrKqc%2Fn0NeYxsqPqMbmxsiah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Diana Sánchez, Asociación Minga] 

La organización, Somos Defensores, presentó su último informe sobre la situación de los defensores y defensoras de derechos humanos en el país.  El documento titulado la “**Divina Comedia”,** como en el poema de Dante Alighieri, se metaforiza la defensa de los DDHH como un viaje por el infierno y el purgatorio en el camino para llegar al paraíso. La conclusión no es alentadora**, los ataques contra los defensores y defensoras han incrementado aun cuando se está en medio de los conversaciones de paz.**

El documento, fue elaborado con base al seguimiento de la política en materia de protección y las agresiones contra líderes, defensores y defensoras de derechos humanos registradas por el Sistema de Información del **Programa Somos Defensores – SIADDHH en 2014**.  Los cuestionamientos sobre  la **Unidad Nacional de Protección** (UNP), el incremento de amenazas, y finalmente las propuestas de cambio al modelo de protección estatal, son los ejes a través de los cuales se desarrolla la “Divina Comedia”.

En la primera parte del informe denominada “El Infierno”, se resumen las nueve problemáticas a las que se enfrentan los defensores, cuando requieren de la UNP. Los círculos van desde la solicitud de las medidas de protección, que fueron negadas en un 56% en 2014, hasta el modelo de protección que está al borde del fracaso, por no responder a las necesidades reales de los y las defensoras.

En “El Purgatorio”, se demuestra el panorama de agresiones en cifras del año 2014. Allí se evidencia que hubo **626 defensores y defensoras fueron víctimas de ataques**, lo que quiere decir que hubo (488 casos), asesinatos (55 casos), atentados (41 casos), detenciones Arbitrarias (23 casos), desapariciones (1 caso), uso arbitrario del sistema penal (8 casos) y hurtos de información (10 casos). Además de 212  organizaciones sociales y de derechos humanos, que fueron víctimas de algún tipo de agresión.

Entre las cifras más preocupantes se resalta que **hubo un incremento en las amenazas del 133% en comparación el año 2013.** Estas fueron enviadas a líderes sociales, representantes de las víctimas en la mesa de negociación de paz en La Habana entre el Gobierno de Colombia y la guerrilla de las FARC, periodistas, comunicadores alternativos, líderes políticos de izquierda e incluso, funcionarios públicos pertenecientes a la Unidad de Víctimas y a la Unidad de Restitución de Tierras.

De acuerdo al SIADDHH, los presuntos responsables de las agresiones serían, **los grupos Paramilitares con un 72% de los casos,** siguen desconocidos con un 19%, miembros de Fuerzas de Seguridad del Estado con un 7%, y las guerrillas (FARC y ELN) aparecen con el 1.5%.

Finalmente en el capítulo “El Paraíso”, **la organización hace sus propuestas para mejorar la política de protección a los defensores y defensoras de derechos humanos**, teniendo en cuenta los tratados internacionales de DDHH a los que se acoge Colombia.

[La Divina Comedia](https://es.scribd.com/doc/256169064/La-Divina-Comedia "View La Divina Comedia  on Scribd") by [Contagioradio](https://www.scribd.com/contagioradio "View Contagioradio's profile on Scribd")

<iframe id="doc_93960" class="scribd_iframe_embed" src="https://www.scribd.com/embeds/256169064/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-qH4ewNzbdFi1bORs3CkS&amp;show_recommendations=true" width="600" height="800" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="0.6872616323417239"></iframe>
