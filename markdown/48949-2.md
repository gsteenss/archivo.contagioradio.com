Title: Las 5 jugadas de los congresistas para "hacer trizas el acuerdo de paz"
Date: 2017-11-11 09:50
Category: Otra Mirada, Paz
Tags: acuerdos de paz, Cambio Radical, Centro Democrático
Slug: 48949-2
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/07/CONGRESO.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo Partícular] 

###### [10 Nov 2017]

Durante la implementación de los Acuerdos de Paz y los debates que se han dado en el Congreso para aprobar los diferentes proyectos de ley tanto en Cámara como en Senado, se han evidenciado con mayor fuerza, las diferentes artimañas que tienen los partidos políticos y congresistas para dilatar los debates, **eludir sus responsabilidades ante su labor de legisladores, agotar el tiempo del Fast Track y “hacer trizas los Acuerdos**”.

### **¡No hay quorum!** 

En diferentes momentos desde que iniciaron los debates en Senado y Cámara, uno de los principales inconvenientes ha sido la falta de quorum para poder iniciar una sesión. Completar un quorum para Cámara o Senado, depende directamente del tipo de votación que se esté realizando.

En el quorum deliberatorio se conforma con por lo menos la cuarta parte de la respectiva comisión permanente. Para el **quorum decisorio hay tres escenarios, el primero es que sea ordinario, lo que significa que las decisiones solo pueden tomarse con la asistencia de la mayoría de los integrantes**.

El segundo escenario es quorum calificado en donde las decisiones pueden tomarse con al menos la asistencia de las dos terceras partes de los miembros de la Corporación Legislativa y el tercer escenario es el quorum especial en donde las decisiones pueden ser tomadas por las 3 cuartas partes de los integrantes. (Le puede interesar:["Los cambios del Congreso a las Circunscripciones de paz"](https://archivo.contagioradio.com/circunscripciones-paz-un-paso-firme-en-la-implementacion-de-los-acuerdos/))

### **Las mayorías el azar en las sesiones ** 

Sin embargo, hay unas mayorías requeridas establecidas en el quorum decisorio dependiendo del trámite que se esté llevando a cabo. La Mayoría Simple tiene aplicación en todas las decisiones que adopten las Cámaras legislativas y cuando las disposiciones constitucionales no han dispuesto otra clase de mayoría.

La Mayoría Absoluta se solicita cuando se presenten proyecto de reforma constitucional, leyes que den facultades extraordinarias al presidente y leyes orgánicas, en este punto es **en donde se dan la gran mayoría de las sesiones de los proyectos legislativos de los Acuerdos de Paz**.

De igual forma se solicita mayoría absoluta cuando se tramiten leyes estatutarias en una sola legislatura, leyes que busquen convocar una asamblea constituyente y leyes que sometan a referendo un proyecto de reforma constitucional. (Le puede interesar: ["Los Acuerdos de Paz cojean en el Congreso de la República"](https://archivo.contagioradio.com/los-acuerdos-de-paz-cojean-en-el-congreso-de-la-republica/))

En la mayoría calificada las decisiones se toman por los dos tercios de los votos de los miembros en el caso de que las leyes busquen reformar o derogar decretos durante un Estado Guerra y leyes que concedan amnistías o indultos.

Estos son los diferentes quorum que se han saltado los congresistas en reiteradas ocasiones, debido a que **si no existe el quorum debe aplazarse la sesión y reprogramarse, esto en tiempos de Fast Track ha sido una herramienta para poco a poco ir agotando los tiempos** y así evitar sesionar.

### **Salirse de las sesiones** 

Otra de las artimañas que han usado los congresistas y partidos políticos consiste en llegar a las sesiones citadas, ser contados dentro del quorum, pero a la hora de votar los articulados o las proposiciones ya no están en el recinto, desajustando el quorum y obligando a que este, otra vez, sea aplazado y re programado.

### **Proposiciones a la lata** 

El Centro Democrático, en el debate de la Jurisdicción Especial para la Paz, genero proposiciones a casi todos los artículos que conforman el proyecto de ley, de hecho, **hay más de 400 proposiciones que tardan en ser debatidas entre una a dos horas y si a esto se le suma que no hay quorum para debatirlas**, se provoca un círculo vicioso, en donde se realiza la proposición, pero no se asiste a la sesión para darle trámite a la misma.

Este mismo hecho se presentó en otros debates como el de la Reforma Política, tanto en Cámara como en Senado, actualmente solo se ha aprobado el 18% de los Acuerdos de Paz y tan solo quedan dos semanas del tiempo del Fast Track. (Le puede interesar: ["Próxima semana habría luz verde para JEP en el Senado"](https://archivo.contagioradio.com/48938/))

### **No votar, pero participar** 

Esta estrategia ha sido utilizada en un principio por el partido Cambio Radical, pero con el tiempo ha sido adoptada también por el Centro Democrático y consiste en aseverar que no votaran en la sesión, pero participaran.

###### Reciba toda la información de Contagio Radio en [[su correo]
