Title: General Uscátegui no cumple requisitos para acceder a la JEP: Ccajar
Date: 2017-05-08 16:04
Category: Judicial, Nacional
Tags: JEP, Jurisdicción Especial de Paz, Masacre de Mapiripan, Uscátegui
Slug: no-se-deben-conceder-libertades-a-militares-que-no-se-comprometen-con-la-verdad
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/05/uscategui-masacre-de-mapiripan.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: elpais] 

###### [08 May 2017] 

Uno de los casos que preocupa a víctimas y defensores de DDHH es el del General Uscátegui para quién se ordenó libertad. Según un reciente pronunciamiento del Colectivo de Abogados José Alvear Restrepo, para que se aplique la ley de Amnistía y tratamientos especiales hay varios requisitos que el mencionado general no ha cumplido.

Para el Ccajar la ley es clara en exigir que para acogerse a dicha ley es necesario que los militares deben **firmar su intención de contribuir con la verdad**, aportar en materia de justicia restaurativa y aportar también para que esos hechos no se repitan.

Uscátegui no cumple esos compromisos, además la JEP no se hará cargo de delitos graves como las masacres o las “ejecuciones extrajudiciales” puesto que no tendrían relación con el conflicto armado. "La norma establece este beneficio para quienes no hayan cometido graves crímenes, acepten responsabilidad y deseen contribuir al SIVJRNR, **no para quienes entren a disputar el establecimiento judicial de los hechos**". ([Lea también: Listado de militares beneficiados con la JEP](https://archivo.contagioradio.com/min-defensa-entrego-listado-de-817-militares-que-quedaran-a-la-espera-de-jep/))

Por otra parte los juristas manifiestan que no son los jueces ordinarios los que deberían determinar si los militares cumplen o no los requisitos, sino que son los jueces de conocimiento de la propia JEP a los que se les atribuye dicha responsabilidad, en ese caso, **para conceder o negar libertades debería entrar en funcionamiento ese tribunal**. ([Le puede interesar: Si el Estado no juzga a militares que lo haga la CPI](https://archivo.contagioradio.com/si-el-estado-no-puede-juzgar-empresarios-y-altos-mandos-militares-que-lo-haga-la-cpi/))

### **Víctimas preparan acciones legales contra libertades a militares que no cumplen los requisitos para acceder a la JEP** 

Las madres de las víctimas de los “falsos positivos” de Soacha, que durante los últimos días han enfrentado situaciones calificadas como revictimizantes, han iniciado una serie de acciones jurídicas para que los militares implicados en dichos hechos no recobren la libertad con la ley de amnistía y otros tratamientos especiales, en el marco de la aplicación del acuerdo con las FARC y en concreto la entrada en vigencia de la **Jurisdicción Especial de Paz.**

Las familias de las víctimas han mostrado ya su indignación en varios casos, uno de ellos las decisiones de los jueces ordinarios de suspender los procesos bajo el argumento de que serían asumidos por la Jurisdicción Especial de Paz, o la inasistencia del senador Uribe en el marco de una demanda por injuria y calumnia.

Por estas razones, tanto las madres de Soacha, como diversas víctimas y organizaciones han asegurado que están trabajando en varios mecanismos jurídicos para **evitar que los militares implicados en los asesinatos de sus hijos recobren la libertad**. En este sentido el Ministerio de Justicia se manifestó y aseguró que las víctimas podrán apelar las decisiones de los jueces. ([Lea también: Falsos positivos no deben pasar a la JEP](https://archivo.contagioradio.com/38886/))

Según el abogado Luis Guillermo Perez, del Colectivo de Abogados José Alvear Restrepo, ya hay una decisión manifiesta de las madres de soacha, desde el momento en que se enteraron de los asesinatos a manos de efectivos militares, es que lucharían por la justicia, por la verdad y en resumidas cuentas **lucharían para que los crímenes de sus hijos no quedaran en la impunidad.**

<iframe id="audio_18564140" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_18564140_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
