Title: El disfraz democrático del avance minero-energético
Date: 2017-12-06 06:00
Category: CENSAT, Opinion
Tags: Cajamarca, consultas populares, Mineria, Roy Barreras
Slug: el-disfraz-democratico-del-avance-minero-energetico
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/12/avance-minero-energético.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo COA ] 

#### **[Por: CENSAT Agua Viva - Amigos de la Tierra – Colombia](https://archivo.contagioradio.com/censat-agua-viva/)** 

###### 5 Dic 2017 

[Ante la contundencia de las consultas populares y la gran cantidad que se vienen formulando en distintas zonas del país, el gobierno colombiano ha empleado un sinnúmero de estrategias para limitar la participación ciudadana. Entre estas está la polémica sobre la financiación de las consultas populares, las declaraciones de altos miembros del gabinete negando su carácter vinculante y últimamente, proyectos de ley que modifican los mecanismos de participación ciudadana. El pasado 23 de agosto de 2017, el senador Roy Barreras radicó un proyecto de ley estatutaria que pretende “modernizar el funcionamiento de los mecanismos de participación ciudadana y promover nuevos mecanismos que faciliten el diálogo entre el Gobierno y la ciudadanía.” En realidad el proyecto plantea dos cosas: limitar las fechas de revocatoria de los mandatos de alcaldes y gobernadores; y crear la figura de licencia social para “los proyectos de alto impacto social y ambiental”.]

[Según el senador Barreras “ninguna revocatoria del mandato ha tenido éxito y las consultas populares desordenadas, han significado una legítima expresión popular, pero a la vez una amenaza contra la inversión extrajera y una desordenada proliferación de vetos y rechazos a compromisos del gobierno frente a la economía minero energética, es decir, una fuente de inseguridad jurídica” Estas palabras evidencian la intencionalidad que tiene este nuevo mecanismo, además de seguir construyendo vías para la locomotora minero energética, continúa alimentando el discurso de la inseguridad jurídica en materia ambiental y social para el sector extractivo, y creando límites temporales para la toma de decisiones de las comunidades sobre aquello que va a afectar sus vidas.]

Desde la constitución de 1991 se plantea una democracia participativa; la cual es desarrollada por la sentencia C021 de 1996 de la Corte Constitucional. Entendemos que la participación es un ejercicio constante y popular, la cual no depende de coyunturas, sino que debe permanecer como un ejercicio de la política real, que debe apuntar a ser informada, decisiva, efectiva, vinculante, incluyente, directa, visible y pública.

Proyectos de ley como éste, desconocen el derecho fundamental a la participación; puesto plantean que la licencia social es tan solo un mecanismo de diálogo que se crea para concertar con las comunidades y autoridades territoriales acuerdos sobre el manejo de los impactos económicos, sociales y ambientales de los proyectos extractivos a desarrollar, y tras una serie de procedimientos como audiencia pública y talleres de análisis donde participan todos los actores, entiéndase autoridades territoriales, empresas y comunidades; será formalizada por los alcaldes y los consejos o en el caso más amplio los gobernadores, se haya o no establecido un acuerdo, dando por sentado que sólo se manifestarán las inquietudes de las comunidades.

Dicho en otras palabras, se está dando por sentada la realización de los proyectos “de alto impacto”, pues sólo se “concertarán” las afectaciones e impactos, sin tener en claro el peso de la participación de las comunidades y sus inquietudes en la toma de decisiones respecto a su vida y su territorio. De igual forma, este tipo de procedimientos termina dilatando los procesos, dividiendo y agotando las comunidades, por lo cual estas reformas profundizan la asimetría de poderes entre las comunidades y las empresas; además facilita las estrategia de estas últimas para cooptar a la institucionalidad del país, como en el caso de Cajamarca donde Concejales y Alcaldes negaron la realización de la Consulta Popular e incluso el Ministro de Minas y Energía, y tras los resultados de la consulta popular realizada por iniciativa ciudadana, negó el carácter vinculante de la misma.

Además, es difícil hablar de consensos en el país cuando el gobierno colombiano no ha querido reconocer la expresión popular de decir si a la vida y al agua, y no a los proyectos extractivos. Aun así, y pese a la voz que han dado las consultas populares y los acuerdos municipales, esta situación nos pone por delante un interrogante ¿vamos a seguir discutiendo de manera municipal el modelo o vamos a promover el debate nacional?

Proyectos de ley como este, y como el proyecto de ley 60 que se trató de proponer en 2014 para crear la licencia social de operación para la minería lo que hacen es ahondar el déficit democrático, que sigue negando el reconocimiento al derecho sobre los territorios y a la participación. El Estado colombiano sigue buscando simular la participación a través de instrumentos que lo único que buscan es profundizar una dictadura extractiva que reproduce la injusticia social y ambiental. Además, dificulta el replanteamiento del modelo económico hacia un tránsito en su matriz energética y en el modelo de desarrollo hegemónico para construir una paz estable y duradera, que incluya todas las voces contenidas en los planes de vida y/o planes del buen vivir que han construido las comunidades para permanecer dignamente en los territorios, pero no de forma fragmentaria, sino buscando una conexión real que fortalezca tejidos sociales de una gran trama.

#### [**Leer más columnas de CENSAT Agua Viva - Amigos de la Tierra – Colombia**](https://archivo.contagioradio.com/censat-agua-viva/)

------------------------------------------------------------------------

###### Las opiniones expresadas en este artículo son de exclusiva responsabilidad del autor y no necesariamente representan la opinión de Contagio Radio.
