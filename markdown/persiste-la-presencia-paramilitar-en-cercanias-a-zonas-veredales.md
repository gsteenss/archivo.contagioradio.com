Title: Persiste la presencia paramilitar en cercanías a Zonas Veredales
Date: 2017-03-06 17:14
Category: Entrevistas, Paz
Tags: Fast Track, Zonas Veredales Transitorias de Normalización
Slug: persiste-la-presencia-paramilitar-en-cercanias-a-zonas-veredales
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/12/ZVT.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo Particular] 

###### [06 Mar 2017] 

Tras haber visitado diferentes zonas veredales, el senador Iván Cepeda, afirmó que existe un temor en las comunidades por la **presencia del paramilitarismo en lugares cercanos a los puntos de normalización, al igual que una preocupación por las incipientes condiciones** en la que aún se encuentran estos espacios, que ya deberían estar en plenas capacidades para el proceso de reincorporación de los guerrilleros

De acuerdo con Iván Cepeda, en el dialogo con algunas comunidades que están cercanas a las zonas veredales, se ha obtenido información de varios hechos de **amenazas contra líderes campesinos en territorios como Mesetas, en el Meta y denuncias** frente a que los lugares que dejó las FARC están siendo ocupados por paramilitares o disidentes de la guerrilla. Le puede interesar:["Delegación Asturiana denuncia reparamilitarizacion del territorio Colombiano"](https://archivo.contagioradio.com/delegacion-asturiana-denuncia-la-reparamilitarizacion-del-territorio-colombiano/)

Agregó que, **“los compromisos básicos, como el tema del agua, las construcciones, la energía eléctrica, están en un estado aún incipiente”**, aunque hayan sido acuerdos ya firmados por el gobierno. Le puede interesar: ["Hay un 80% de incumplimientos por parte del Gobierno en Zonas Veredales: FARC"](https://archivo.contagioradio.com/gobierno-ha-incumplido-en-un-80/)

Igualmente, la implementación de los acuerdos a través del Congreso también ha presentado demoras, esta semana la **Jurisdicción Especial para la paz afrontará su último debate,** en donde se espera que se recoja las exigencias que hacían organizaciones sociales y víctimas, de ser incluidas en los diferentes escenarios de debates y no únicamente contar con la participación de la Mesa Nacional de Víctimas.

Cepeda señala que el próximo debate será sobre la participación de las víctimas en el Sistema de Verdad, Justicia y Reparación, “se abrirá la fase en la que se empieza a preparar la conformación de la Comisión de la Verdad, de todo el sistema de justicia y de la Unidad para la Búsqueda de las personas Desaparecidas y ese será el nuevo **estadio en donde las víctimas tendrán que movilizarse por sus derechos de manera muy intensa”.**

###### Reciba toda la información de Contagio Radio en [[su correo]
