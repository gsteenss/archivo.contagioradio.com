Title: Medidas anunciadas por el gobierno no solucionan la crisis carcelaria
Date: 2015-04-15 15:10
Author: CtgAdm
Category: DDHH, Nacional
Tags: Comité de solidaridad Presos políticos, Crisis carcelaria colombia, Gloria silva, Receta de Santos para la crisis carcelaria
Slug: medidas-anunciadas-por-el-gobierno-no-solucionan-la-crisis-carcelaria
Status: published

###### Foto:Sicsemanal.wordpress.com 

###### **Entrevista con [Gloria Silva], abogada del Comité de Solidaridad con los presos políticos:** 

<iframe src="http://www.ivoox.com/player_ek_4357682_2_1.html?data=lZiimZucdo6ZmKiak56Jd6Kol5KSmaiRdo6ZmKiakpKJe6ShkZKSmaiRkMKf0Nnfw5DHpdPVjMnSjdHFb9PZxMrhw5DIqYzHwtPh0diPtMLmwpDZw5CntsrnytiYxcbWp46ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

Las **medidas anunciadas por el gobierno** , contemplan el  aumento en 12.000 cupos en las prisiones ya existentes, la asociación con el sector privado para construir más centros, la adecuación de la salud de los internos, o la creación de una agencia de empleo para los internos.

Según **Gloria Silva,** abogada del **Comité de Solidaridad de Presos Políticos,** el gobierno no está dando una respuesta real a la grave crisis carcelaria.

Las medidas responden a la **amplia movilización desplegada por los familiares** de los presos y los propios internos.

Para la abogada, el gobierno **prima la seguridad sobre los DDHH de los internos**, y la construcción de nuevos complejos **no soluciona el hacinamiento** actual de las prisiones Colombianas, sino que es parte de un plan para privatizarlos y hacer negocio entre el gobierno y el sector privado.

La **política criminal  del gobierno** basada en  la privación de la libertad como solución a la delincuencia y a la protesta social ha sido la **responsable del hacinamiento** y de la gran masa de personas privadas de la libertad.

Entre las medidas anunciadas no hay ninguna que contemple otra solución para la criminalidad que la cárcel. Es más las **penas se endurecen** y se imponen más trabas para solicitar la libertad condicional.

La emergencia humanitaria con respecto a la salud de los presos, tampoco se soluciona en el anuncio del gobierno. Santos tuvo la oportunidad de solucionar la situación a través de la ley 1709,  con la que en el plazo de un año debía regular un modelo de salud para las personas privadas de la libertad. Fue cuando contrató con **CAPRECOM,** entidad privada que presta los servicios de salud para los internos.

Para la organización social CSPP es de vital importancia la supresión de empresas intermediadoras como **CAPRECOM**, en el caso de la salud de los presos. De tal manera el gobierno antepone el negocio a los DDHH de los presos, como ya hemos comprobado en el tiempo de su funcionamiento.

Gloria Silva afirma que es necesario que el gobierno cree **un fondo para la salud de los presos** y sea el garante del cumplimiento del derecho a la salud en las cárceles y no una entidad privada que prima el negocio a los DDHH.

Con respecto a la ampliación de cupos en las cárceles , la abogada advierte que ejemplos como la **cárcel de Palmira** en la que se están construyendo 120 cupos más, nos dejan claro que al construirlo sobrehacinan a los reclusos agravando la situación que ya padecían.

Cuando construyen una ampliación es necesario reubicar a los presos pero esto no sucede, sino que por el contrario se les hacina aún más en una habitación reducida y no acondicionada.

Silva explica que las violaciones de los DDHH en las cárceles Colombianas son la orden del día, y que Colombia no ha ratificado el protocolo facultativo para la prevención de la tortura, que permitiría el acceso a organizaciones externas a la prisión para poder realizar programas de prevención y judicializar los casos reportados hasta ahora.

Dentro de estas violaciones de los DDHH se encuentran los **castigos a presos por protestar**,  que consisten en trasladarlos a otras prisiones, lo que conlleva también a un **castigo a los familiares** al tener que trasladarse a otras regiones para poder visitarlos.

La **resocialización de los internos es otro de los fracasos de la política penitenciaria** actual que tampoco contemplan las medidas anunciadas. La falta de trabajo o cuando lo hay la explotación laboral, los impedimentos a la hora de poder estudiar dentro de las cárceles, o la graduación en la libertad con el objetivo de retornar al arraigo familiar son cuestiones que aún no se han abordado.

Por último una de las peores situaciones es la de los presos que están en la sección de preventivos a la espera de juicio que según las organizaciones sociales las medidas del gobierno no contempla la legalización de los juicios, ya que estas medidas no conllevan una reforma de la ley de criminalidad.
