Title: Odio y violencia se toman los pasillos de Universidad Surcolombiana
Date: 2018-02-22 13:12
Category: DDHH, Nacional
Tags: estudiantes, juventud rebelde, USCO
Slug: odio-y-violencia-se-toman-los-pasillos-de-universidad-surcolombiana
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/02/surcolombiana.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Polinizaciones] 

###### [22 Feb 2018] 

Estudiantes de la Universidad Sur Colombiana, pertenecientes a la organización Juventud Rebelde en Neiva, Huila, denunciaron que uno de sus integrantes y un compañero del mismo, fueron víctimas de **golpizas por parte de otros estudiantes del centro educativo, pertenecientes al grupo de Facebook “Justo en la venada”**, que incita a actos violentos, el odio y la discriminación, no solo contra quienes tengan un pensamiento diferente, sino en general a la comunidad académica.

Los hechos ocurrieron el pasado 20 de febrero cuando los estudiantes, Lenin Vargas y Jefersson Gómez se encontraban en las ágoras del centro educativo. Jefersson se trasladó al baño y mientras tanto, Lenin fue agredido por un grupo de 5 personas, uno de ellos le propinó **una patada en el rostro**, provocándo graves heridas.  Posteriormente los mismos 5 agresores se dirigieron al baño en el que estaba Jefferson, **lo retuvieron  y lo golpearon varias veces**.

Lenin es estudiante de licenciatura en Educación Artística y Cultural, pertenece a la organización Juventud Rebelde y actualmente está militando en el partido político FARC, Jefferson es estudiante de la licenciatura en Literatura y Lengua castellana y pese a no pertenecer a ninguna organización, ha participado activamente de las actividades en defensa de la educación.

\[caption id="" align="aligncenter" width="324"\]![Juventud Rebelde](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/02/WhatsApp-Image-2018-02-21-at-10.30.18-PM-e1519323144264.jpeg){.wp-image-51676 width="324" height="432"} foto: Juventud Rebelde\[/caption\]

### **“Justo en la venada”** 

De acuerdo con Marta Liliana Sánchez, encargada de derechos humanos de la Juventud Rebelde, los integrantes de “Justo en la venada” un grupo de Facebook con evidente contenido violento y discriminatorio que incita al odio, son los que estarían detrás de los ataques a ambos estudiantes.

“Lo que hacen con este grupo es burlarse de cualquier persona y de los procesos organizativos que se llevan en la USCO, hacen memes, chistes, señalamientos, acoso a las mujeres, se roban los datos personales y fotos y ponen en la palestra pública para que miles de estudiantes se burlen y hagan comentarios”. La defensora denuncia que las acciones de este **grupo promueven el odio y todo tipo de violencia en contra de sectores políticos de izquierda**.

### **Las acciones de las Directivas de la USCO** 

Las directivas de la Institución rechazaron enfáticamente este acto de violencia y afirmaron que iniciaran las investigaciones pertinentes para encontrar a los responsables y tomar las sanciones debidas en este caso. (Le puede interesar: ["Pretenden judicializar a estudiantes de Bucaramanga por defender el bosque del colegio"](https://archivo.contagioradio.com/estudiantes-normalistas-fueron-agredidos-por-el-esmad-en-bucaramanga/))

En ese mismo sentido, la organización Juventud Rebelde expresó en un comunicado de prensa que tomará las acciones legales correspondientes contras los estudiantes que cometieron estos actos y aseguraron que repudian todo tipo acto de violencia, matoneo, discriminación o acoso **al interior del campus universitario y le exigen a las directivas que tomen medidas frente a este tipos de conductas que llevan al odio**.

<iframe id="audio_23976403" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_23976403_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
