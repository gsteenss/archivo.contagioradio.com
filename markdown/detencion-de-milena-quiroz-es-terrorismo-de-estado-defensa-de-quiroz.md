Title: "Detención de Milena Quiroz es terrorismo de Estado" Defensa
Date: 2017-08-03 13:20
Category: Judicial, Nacional
Tags: Defensores de derechos humano, detenciones ilegales, Fiscalía, lideres sociales, Milena Quiroz
Slug: detencion-de-milena-quiroz-es-terrorismo-de-estado-defensa-de-quiroz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/08/quiroz.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: El Heraldo] 

###### [03 Ago 2017] 

En audiencia por el caso de Milena Quiroz, la juez encargada **negó la solicitud de anular la medida de aseguramiento contra la líder social** e integrante del Congreso de los Pueblos. Sus abogados manifestaron que a Milena “se le han violado todos los derechos, le negaron el derecho a hablar y de acceder a la justicia”, esto además de haber sido acusada por una fiscal investigada por nexos con paramilitares.

Milena Quiroz, quien es líder social de Arenal en el Sur del Bolívar, **se encuentra en prisión domiciliaria en Bogotá desde el 22 de marzo**. Tras haber sido capturada por el delito de rebelión, la lideresa fue obligada a salir de su territorio bajo el argumento de “organizar marchas y representar un peligro para la sociedad”. (Le puede interesar: ["Milena Quiroz fue condenada al "destierro" abogados de la defensa"](https://archivo.contagioradio.com/milena-quiroz-fue-condenada-al-destierro-abogados-de-la-defensa/))

Según Jorge Molano, abogado defensor que lleva el caso de Quiroz, la medida de aseguramiento impuesta contra la defensora de derechos humanos se hizo **“de manera arbitraria e ilegal”**. De igual forma, aseguró que hay una persecución estatal en contra de ella y 5 defensores más que fueron capturados junto a ella en el Sur de Bolívar.

El defensor manifestó que las pruebas que presentaron para pedir la nulidad del caso, no fueron tenidas en cuenta por la Fiscal, “quien dijo que las declaraciones de defensores, líderes y miembros de la iglesia no eran suficientes para establecer que Milena no es un peligro para la sociedad”, por ello **“ha quedado claro que hay un concierto para delinquir en contra de los derechos** a la libertad de pensamiento, el derecho a la manifestación y el caso de Milena es un terrorismo de Estado”. (Le puede interesar: ["Fiscalía no tiene argumentos para mantener en prisión a líderes sociales del Sur de Bolívar"](https://archivo.contagioradio.com/fiscalia-no-tiene-argumentos-para-mantener-en-prision-a-lideres-sociales-del-sur-de-bolivar-defensa/))

### **Lo que sigue en el caso** 

Ahora, la defensa va a acudir al grupo de la ONU sobre Detenciones Arbitrarias para presentar el caso de Quiroz. Argumentarán que es un caso de detención con **“motivaciones políticas y propósitos de persecución** para que ella no ejerza sus derechos constitucionales”. Igualmente, presentarán a nivel internacional las irregularidades en los casos de detención de los otros 5 líderes y defensores de derechos humanos.

Además, la defensa presentó una apelación para “indicarle al juez de segunda instancia que la **detención de Milena es ilegal y le han violado todos los derechos**”. Le harán saber que las 50 pruebas que presentaron en la audiencia, no fueron tenidas en cuenta. (Le puede interesar: ["Por persecución judicial se presentaron líderes de Congreso de los pueblos ante Fiscalía"](https://archivo.contagioradio.com/persecucion-judicial-congreso-de-los-pueblos/))

### **Fiscal anterior del caso fue capturada por nexos con los paramilitares** 

La fiscal María Bernarda Puente, que llevaba el caso de Milena, y de los otros defensores detenidos, **fue capturada por pertenecer a una red de corrupción al interior de la Fiscalía** y por haber favorecido a narcotraficantes y paramilitares. Ante esto, en la audiencia la defensa denunció los actos criminales de la Fiscal pero Molano manifestó que “la única repuesta que obtuvimos fue un llamado a que respetemos a la funcionaria y a la institución”.

<iframe id="audio_20153909" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_20153909_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU).
