Title: “Visibilizar para proteger”: la estrategia de líderes sociales
Date: 2019-05-08 17:29
Author: CtgAdm
Category: DDHH, Líderes sociales
Tags: lideres sociales, Unión europea
Slug: visibilizar-para-proteger-la-estrategia-de-lideres-sociales
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/8-e1556904407217.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Contagio Radio 

[El pasado 6 de mayo en el marco de la Feria Internacional del Libro de Bogotá, La Unión Europea junto con el medio de información Colombia2020, realizaron el evento “Los líderes sociales y sus propuestas de autoprotección”. Este evento también fue una oportunidad para que la Unión Europea lanzara la campaña “**Defendamos la vida**” a fin de expresar la solidaridad internacional con la defensa de los derechos humanos reconociendo la labor de los protectores de la vida, del territorio y del medio ambiente.]{lang="ES-TRAD"}

### **[Defendamos la Vida]

[La embajadora de la Unión Europea, Patricia Lombart, abrió el encuentro lanzando la etiqueta a Twitter \#DefendamosLaVida, una iniciativa de sensibilización que busca visibilizar la importancia de proteger a los líderes que luchan por sus tierras y sus derechos.]{lang="ES-TRAD"}

[Por este motivo, 17 embajadas empezarán una campaña mediática con el fin de promover la no estigmatización hacia los líderes sociales y defensores de derechos humanos que cumplen un papel fundamental en los territorios. ]{lang="ES-TRAD"}

[“**Más que seguir preguntándose por qué se repiten estos episodios de muerte y violencia tenemos que seguir preguntándonos qué se puede hacer**”; afirmó la Embajadora, refiriéndose a la labor que han desempeñado diversas instituciones gubernamentales, como la Procuraduría o la Fiscalía en torno a la protección de los liderazgos. ]{lang="ES-TRAD"}

**[Los líderes y sus opiniones]{lang="ES-TRAD"}**

[Joe Sauca coordinador del programa de derechos humanos del Consejo Regional Indígena del Cauca (CRIC), Luz Perly Córdoba lideresa de la Defensa Campesina de Arauca y Mayerlis Angarita de la organización de mujeres en la región de los Montes de María, fueron los líderes sociales que participaron de este evento, quienes enfatizaron dos puntos fundamentales: la exigencia de fortalecer la visibilización como método de prevención y la autoprotección de los líderes, a través de la creación de redes con las instituciones nacionales e internacionales.]{lang="ES-TRAD"}

[Luz Perly Córdoba afirmó que “**matar a los líderes sociales es un acto miserable, cobarde, que ensucia la memoria y las historias de estas comunidades**”. De igual forma Mayerlis Angarita insistió en la falta de acciones por parte de todas las instituciones gubernamentales para garantizar la protección a la vida de las y los líderes en el país.]{lang="ES-TRAD"}

[En ese sentido, los líderes le exigieron a las autoridades estatales que pongan en marcha campañas efectivas para protegerlos y que no se repitan hechos como el atentado en contra de la lideresa Francia Márquez en la zona rural de Santander de Quilichao, al norte del Cauca. De igual forma, pidieron al Presidente Iván Duque que se comprometa “**escuchando las personas porque solamente visitando las comunidades y escuchando el sufrimiento de esta gente se puede entender lo que están pasando**”.]{lang="ES-TRAD"}
