Title: Asesinan al líder social Miguel Antonio Gutiérrez en Caquetá
Date: 2019-01-10 15:43
Author: AdminContagio
Category: Líderes sociales, Nacional, Paz
Tags: Lider social
Slug: asesinan-al-lider-social-miguel-antonio-gutierrez-caqueta
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/01/Captura-de-pantalla-2018-09-18-a-las-11.44.48-a.m.-770x400.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Archivo 

###### 10 Ene 2019 

Miguel Antonio Gutiérrez, presidente de la Junta de Acción Comunal (JAC) del barrio de La Victoria, Chairá, Caquetá, es **el séptimo líder social asesinado del 2019**, quien fue ultimado a tiros por varios hombres en su hogar el pasado 7 de enero. Desde octubre del 2018, Gutiérrez se encontraba amenazado tras denunciar el microtráfico de drogas psicoactivas en Cartagena del Chairá, municipio donde operan disidencia de las FARC y paramilitares, según las comunidades.

La Confederación Nacional de Acción Comunal rechazó el crimen y hizo una llamado al Gobierno de hacer mayor presencia en la región para garantizar la seguridad de los líderes sociales.

### **El país reacciona ante la violencia contra líderes sociales** 

El hecho de que se haya presentado 7 asesinatos de líderes sociales en las primeras dos semanas del año ha indignado ciudadanos, organizaciones de defensa de los derechos humanos y la Procuraduría General, entre otras entidades. El 9 de enero, la Procuraduría le pidió al Gobierno de convocar una **Comisión Nacional de Garantías de Seguridad** **para evaluar la eficacia de las medidas de seguridad activas para prevenir el asesinato de líderes sociales**.

El defensor del pueblo Carlos Negret alertó que en el 2018 fueron asesinados **172 líderes en el 2018, aproximadamente uno cada 48 horas**. Asimismo explicó que la violencia se ha enfocado en líderes que se oponen a los cultivos de uso ilícito y la minería de oro, tal como los defensores de la restitución de tierras.

Además, señaló que los departamentos donde más se registró asesinatos el año pasado fueron: Cauca (35); Antioquia (24); Norte de Santander (18); Valle del Cauca (14); Caquetá (11); Putumayo (11); Meta (10); Nariño (7); Chocó (6); Arauca (5) y Córdoba  (5).

[(Le puede interesar: Consejo de Seguridad de Naciones Unidas preocupada por asesinato de líderes sociales en Colombia)](https://archivo.contagioradio.com/onu-preocupada-por-el-asesinato-de-lideres-sociales-en-colombia/)

###### Reciba toda la información de Contagio Radio en [[su correo]
