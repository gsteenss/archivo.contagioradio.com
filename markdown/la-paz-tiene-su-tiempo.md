Title: La Paz tiene su tiempo
Date: 2016-10-19 09:17
Category: Abilio, Opinion
Tags: marchas por la paz, Plebiscito, plebiscito por la paz, proceso de paz
Slug: la-paz-tiene-su-tiempo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/10/marcha-de-las-flores101316_62.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Contagio Radio/ Gabriel Galindo 

#### **Por [Abilio Peña](https://archivo.contagioradio.com/abilio-pena-buendia/) -[@[Abiliopena]

###### 19 oct 216 

El traspiés que está teniendo la implementación del acuerdo de paz alcanzado por el gobierno nacional y la guerrilla de las Farc-Ep a raíz del estrecho triunfo del No en el plebiscito, da cuenta que el tiempo de la paz para la sociedad colombiana no ha llegado aún.

La larga historia de guerras de la que dan cuenta la mayoría de los ensayos de la Comisión Histórica sobre el Gobierno y sus Víctimas, que en su momento acordaron el gobierno y la guerrilla, muestran la magnitud de la violencia, sus causas económica, políticas, espirituales que han hecho parte del alma de la sociedad colombiana. La guerra llamada de los mil días, La Violencia liberal y conservadora y el más reciente conflicto armado interno se han tomado entre todas cerca de 100 años casi ininterrumpidos de nuestra historia con las afectaciones que ello implica.

Ni los bebes en gestación se escapan de la marca cultural y hasta quizás genética de la confrontación armada, de la que las élites políticas y económicas tienen la mayor parte de responsabilidad. Algún tatarabuelo, bisabuelo, abuelo, tío suyo ha tenido que ver con esta guerra.

La guerra de los “Mil Días”, produjo al rededor de 100.000 muertos, “La Violencia”, entre liberales y conservadores, al rededor de 300.000 y El Conflicto que se quiere resolver con los acuerdos actuales, al rededor de 220.000 personas. Sin cotar las desapariciones forzadas y el desplazamiento forzado, estamos hablando un siglo con cerca de 620.000 muertes, con el impacto en las familias, comunidades y la sociedad que cada una de ellas significa.

Creíamos que el dos de octubre el Si iba ganar, pero los resultados nos hicieron despertar de la ilusión de una paz a la vuelta de la esquina. Los mismo promotores del No se vieron sorprendidos por los resultados inesperados. El presidente por su parte mintió al afirmar que de ganar el No se volvería a la guerra, desafiando a unos poderes a quienes no les importa la suerte del país, mientras las Farc afirmaban que se mantendrían en la paz. Los cerca de 6 años de diálogos entre el gobierno y las Farc-Ep consiguieron el acuerdo que se firmó el 24 de agosto y se protocolizó el 26 de septiembre. Muy poco tiempo desde la firma para que fueran conocidos y pudieran espantar la niebla de mentiras que los opositores regaron por todas partes y para convencer a los millones de escépticos que no tienen motivos para creer en la dirigencia política Santista y Uribista que no les da la menor credibilidad.

El poco tiempo que impuso el presidente Santos para refrendar un acuerdo que finalizaba una guerra de 52 años, antecedida por guerras de otros 50 años atrás, no se compadeció ni de lejos con las marcas dejadas por el conflicto social, político y armado, en la ciudadanía electora y que requieren de tiempo, de reuniones en los barrios, en las calles, en las veredas, en las iglesias, colegios, universidades, oficinas entre otros para intentar sanarlas. El arte, las celebraciones, discusiones, símbolos que permitan la expresión de los sentimientos de la población, estuvieron ausentes por la carrera electoral que nos impuso el gobierno.

Quizás el triunfo del No tuvo como causa profunda esa larga historia de guerras, de engaños, de traiciones, de mediocridad de los sectores dirigentes que las provocaron y que no fueron capaces de asumir un acuerdo más allá de la politiquera práctica de obrar con cálculos electorales de corto plazo.

Quizás si vemos éste fracaso con una mirada de época, antes que de coyuntura, nos encontremos con la posibilidad de sentar las bases de una paz más real, de la que los acuerdos son solo el inicio, en la que los fantasmas del genocidio político de los que hacen tránsito a la vida civil, de los incumplimientos históricos de casi todos los acuerdos, puedan ser ahuyentados.

“Por algo será” y “no hay mal que por bien no venga” dice la lectura esperanzada de los acontecimientos adversos que hacen los mayores sabios populares. Algo similar ha dicho el Eclesiastés de la Biblia: “todo tiene su momento, y cada cosa su tiempo bajo el cielo...: Su tiempo el amar, y su tiempo el odiar; su tiempo la guerra, y su tiempo la paz” ( 3, 8).

Las masivas movilizaciones, los campamentos, las reuniones, los debates que se han generado a partir del dos de octubre en diversas ciudades del país, dan cuenta que la paz no es de las élites del poder sino de la sociedad que la está tomando en sus manos.

------------------------------------------------------------------------

###### Las opiniones expresadas en este artículo son de exclusiva responsabilidad del autor y no necesariamente representan la opinión de la Contagio Radio. 
