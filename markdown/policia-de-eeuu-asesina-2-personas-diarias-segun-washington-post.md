Title: Policía de EEUU asesina 2 personas diarias según Washington Post
Date: 2015-06-01 17:34
Category: El mundo, Otra Mirada
Tags: brutalidad policial, Estados Unidos, Jessie Hernández, Jim Bueermann, LGBTI, Michael Brown, The Washington Post
Slug: policia-de-eeuu-asesina-2-personas-diarias-segun-washington-post
Status: published

##### Foto: cubasi.cu 

##### [1Jun2015] 

El periódico **The Washington Post** recogió y publicó sus propias cifras estadísticas, pues los departamentos de policía no están obligados a elevar informes al gobierno federal, según esto, se han identificado **385 asesinatos** con arma de fuego, lo que significa más del doble de la cifra registrada durante la última década.

De estos **385** asesinatos se registraron **20 mujeres**, 9 de raza blanca, 5 afroamericanas, 3 latinas y 3 de otras razas; **365 hombres**, 171 de raza blanca, 100 de raza negra, 54 latinos, 6 asiáticos y 3 de otras razas. Estas cifras se refieren únicamente a homicidios ocurridos en tiroteos, y no con otro tipo de armas.

Entre la lista de muertos hay **8 menores de edad**, entre ellos, **Jessie Hernández**, miembro de la comunidad **LGBTI**, en rangos de edad, 118 de las víctimas tenían entre 25 y 34 años y otras 94 tenían entre 35 y 44 años.

El informe del **Washington Post** alarma a la población estadounidense al demostrar que las cifras de muertes en el país han aumentado considerablemente. Se asegura que las fuerzas policiales matan a dos personas o más al día y que este tipo de hechos se inclinan más hacia las minorías negras o de otro tipo.

Las cifras han generado reacciones por parte de agentes estatales, entre ellos, **Jim Bueermann**, jefe de policía y ex presidente de la Fundación con sede en **Washington** "Estos ataques están siendo sobre registrados", "Nunca vamos a reducir el número ataques de la policía si no empezamos a rastrear con precisión esta información."

Hasta el momento se desconocen los alcances de esta publicación, sin embargo se espera que se tomen correctivos por parte de las autoridades, dado que la mayoría de los casos de asesinatos de personas desarmadas y de raza negra, han generado fuertes protestas y están en la impunidad por la absolución de los agentes responsables de esas muertes.

[![police\_02](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/06/police_02.jpg){.aligncenter .wp-image-9543 width="566" height="549"}](https://archivo.contagioradio.com/policia-de-eeuu-asesina-2-personas-diarias-segun-washington-post/police_02/)
