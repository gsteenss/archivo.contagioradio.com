Title: Las mejores películas colombianas de 2015 competirán en los "Premios Macondo"
Date: 2015-11-24 16:07
Author: AdminContagio
Category: 24 Cuadros
Tags: Academia colombiana de artes y ciencias cinematográficas, Cine Colombiano, el abrazo de la serpiente, Gente de bien, Infierno o Paraíso, Las últimas vacaciones, Los Hongos, Nominaciones premios Macondo, Premios Macondo
Slug: las-mejores-peliculas-colombianas-de-2015-competiran-en-los-premios-macondo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/11/abrazo-serpiente.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### 24 Nov 2015

La noche de este lunes 23 de noviembre, fue presentada la lista de nominados a la 4ta edición de los "Premios Macondo", un reconocimiento de la industria a lo más reciente del cine nacional organizados por la Academia Colombiana de Artes y Ciencias Cinematográficas.

[![Premios Macondo](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/11/macondo.jpg){.wp-image-17766 .aligncenter width="336" height="255"}](https://archivo.contagioradio.com/las-mejores-peliculas-colombianas-de-2015-competiran-en-los-premios-macondo/macondo/)

Durante la rueda de prensa, la junta directiva encabezada por la actriz Consuelo Luzardo, actual presidenta de la Academia, se dieron a conocer las producciones que participaran por las 15 categorías, en la gala que tendrá lugar el próximo 4 de diciembre en Salón Rojo del hotel Tequendama.

Los 389 miembros de la ACACC, fueron los encargados de seleccionar a los nominados entre las 28 producciones inscritas, quienes de acuerdo con su especialidad escogieron las mejores producciones de los últimos dos años, entre las que sobresale "El abrazo de la Serpiente" de Ciro Guerra, con 8 nominaciones en las principales categorías.

El evento rendirá un homenaje especial a la documentalista Marta Rodríguez, por su trayectoría y aportes al cine colombiano.

Los nominados a los Premios Macondo 2015:

MEJOR PELÍCULA  
El Abrazo de la serpiente – Ciro Guerra  
Gente de bien – Franco Lolli  
Los hongos – Oscar Ruiz Navia  
Manos sucias – Josef Kubota Wladyka

MEJOR DIRECCIÓN  
Gente de bien – Franco Lolli  
El abrazo de la serpiente – Ciro Guerra  
Tierra en la lengua – Rubén Mendoza

MEJOR GUION  
El abrazo de la serpiente – Ciro Guerra & Jacks Toulemonde Vidal  
Manos sucias - Josef Kubota Wladyka & Alan Blanco  
Gente de bien - Franco Lolli, Virginie Legeay & Catherine Paille

MEJOR MÚSICA ORIGINAL  
El Abrazo de la Serpiente – Nascuy Linares  
El Elefante Desaparecido – Selma Mutal  
Todos se van – Iván Wyszogrod

MEJOR ACTOR  
Abel Rodríguez – Todos se van  
Humberto Arango – Ella  
Cristian James Advincula – Manos Sucias

MEJOR ACTRÍZ  
Mabel Pizarro – Ruido Rosa  
Alejandra Borrero – Gente de Bien  
Claudia Ruíz del Castillo – Climas

MEJOR ACTOR DE REPARTO  
Felipe Botero – Mateo  
Jhon Alex Toro – La Rectora  
Feliz Antequera – Todos se van

MEJOR ACTRÍZ DE REPARTO  
Indira Serrano – Todos se van  
Shirley Martínez – Ella  
Alma Rodríguez – Tierra en la lengua

MEJOR PELÍCULA DOCUMENTAL  
Infierno o paraíso - German Piffano  
Matachinde - Víctor Palacios  
Las últimas vacaciones - Manuel F. Contreras  
Buenaventura, no me dejes más - Marcela Gómez Montoya

MEJOR MONTAJE  
El abrazo de la serpiente – Etienne Boussac & Cristina Gallego  
Los Hongos – Felipe Guerrero  
Gente de bien – Nicolas Desmaisson & Julie Duclaux

MEJOR DIRECCIÓN DE ARTE  
El abrazo de la serpiente – Ramsés Benjumea  
Ruido Rosa – Miguel Vargas Mejía  
Manos Sucias – Sofía Guzmán

MEJOR DIRECCIÓN DE FOTOGRAFÍA  
El Abrazo de la Serpiente – David Gallego  
Manos Sucias – Alan Blanco  
Tierra en la lengua – Juan Carlos Gil

MEJOR SONIDO  
Jardín de Amapolas - Miguel Vargas  
El abrazo de la serpiente – Carlos García  
Tierra en la lengua – Isabel Torres

MEJOR MAQUILLAJE  
Encerrada – Olga Turrini  
Secreto de confesión – Stella Jacobs

PREMIO A TODA UNA VIDA  
Marta Rodríguez
