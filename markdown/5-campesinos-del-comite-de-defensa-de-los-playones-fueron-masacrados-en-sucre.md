Title: 4 campesinos y un abogado reclamantes de tierras, fueron masacrados en Sucre
Date: 2020-10-25 12:18
Author: CtgAdm
Category: Actualidad, Líderes sociales
Tags: campesinos, colombia, reclamantes de tierras
Slug: 5-campesinos-del-comite-de-defensa-de-los-playones-fueron-masacrados-en-sucre
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/08/Asesinato-de-líderes-sociales.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"fontSize":"normal"} -->

Este domingo 25 de octubre **se registro una nueva masacre de 5 personas, en zona rural del departamento de Sucre**, pese a que la identidad de las víctimas se desconoce, organizaciones de Derechos Humanos presenten en el territorio, indican que se trata de reclamantes de tierras.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/AidaAvellaE/status/1320384375666638849","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/AidaAvellaE/status/1320384375666638849

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

Según informan autoridades locales, **el hecho se registró en la vereda Calle Nueva, exactamente en la finca Caracoles, ubicada entre los municipios de San Benito Abad y San Marcos**, lugar al que llegaron hombres armados y dispararon en contra de 5 personas que estaban allí reunidas, en la tarde de este sábado 24 de octubre. ([Águilas Negras amenazan a líderes y reclamantes de tierra en el Magdalena Medio](https://archivo.contagioradio.com/aguilas-negras-amenazan-a-lideres-y-reclamantes-de-tierra-en-el-magdalena-medio/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Según indican voceros del **Movimiento de Víctimas de Crímenes de Estado** (Movice) de Sucre, **las 5 víctimas de la masacre eran 4 campesinos integrantes de Comité de Defensa de los Playones y Sabanas comunales de San Marcos, la quinta persona era un abogado** que acompañaba el caso de reclamación de tierras. Al mismo tiempo se presumen que hay dos víctimas más.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En días anteriores voceros y voceras del **Movice también habían denunciado en un Consejo de Seguridad los riesgos que enfrentaban los y las reclamantes de tierras en el departamento de Sucre**, esto a la luz del atentado que había sufrido días anteriores, el [líder Hernando Benítez](https://movimientodevictimas.org/denuncia-publica-lider-campesino-e-integrante-del-movice-en-sucre-fue-victima-de-cinco-disparos/).

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/Movicecol/status/1319334376602259457","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/Movicecol/status/1319334376602259457

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

Dentro de la alerta que había presentado el Movice días anteriores sobre la situación de riesgo de líderes y lideresas sociales de Sucre y Montes de María, señalaban que **desde el 2018 se ha venido denunciando la reactivación de grupos armados irregulares de carácter paramilitar,** organizados en los Montes de María.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

También el vínculo del Gobierno local de San Onofre, con el narcotráfico y paramilitarismo encabezado por las Autodefensas Gaitanistas de Colombia, y junto a ello durante el 2020 el aumento de amenazas de muerte contra las organizaciones defensoras en los territorios, *"fragmentado el tejido social y las dificultades de seguridad para quienes ejercen liderazgos".*

<!-- /wp:paragraph -->
