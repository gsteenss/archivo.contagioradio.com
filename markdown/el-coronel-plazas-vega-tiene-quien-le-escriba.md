Title: El coronel que tiene quién le escriba
Date: 2015-12-15 09:15
Category: Opinion, Tatianna
Tags: Alfonso Plazas Vega, Palacio de Justicia
Slug: el-coronel-plazas-vega-tiene-quien-le-escriba
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/12/coronel-plazas-vega.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: You Tube] 

#### **[Tatianna Riveros](https://archivo.contagioradio.com/opinion/tatianna-riveros/)- [@[tatiannarive]

###### [15 Dic 2015 ] 

“Al momento de la toma del Palacio se hallaban al interior del  mismo, magistrados, abogados, personal de servicios generales, escoltas y empleados de la cafetería, la toma subversiva por parte del M19 se ejecutaba como lo planeado” cita el expediente del juzgado tercero penal del circuito especializado, sobre el proceso adelantado contra el Coronel Plazas Vega.

En Colombia, miles han sido las teorías alrededor del caso del [Palacio de Justicia](https://archivo.contagioradio.com/?s=palacio+de+justicia), incluso muchos nos hemos atrevido a escribir sobre los hechos ocurridos, pero lo que si es cierto, es que una denuncia fue el inicio para este proceso que desde el año 2006 viene siendo objeto de la justicia, los medios colombianos, y de las familias de las víctimas de esa noche que aun reclaman la verdad; pero solo hasta el 12 de julio de 2007, se ordenó la vinculación del Coronel con medida de aseguramiento con detención preventiva.

En 2008, la Fiscalía, que en ese entonces no condecoraba a contratistas, solicitó que Plazas Vega fuese acusado por los delitos de secuestro simple agravado y desaparición forzada, regulados por los artículos 239 y 270 del decreto 100 de 1980 y 165 y 166 de la ley 599 de 2000, por los llamados desaparecidos tras el “teatro de la guerra” y la labor de “limpieza del Palacio” realizado entonces; para este proceso, Plazas Vega fue condenado a 30 años de prisión y 10 años sin carrera militar.

Pero este Coronel, no ha estado solo en su lucha: hay quienes creen que los hechos ocurridos en la retoma no son atribuibles en un grado de culpabilidad a los militares que, en el intento de mitigar las acciones de un grupo al margen de la ley, olvidaron que su deber era el de proteger a las personas que se encontraban dentro del Palacio esa noche, y tiene a su esposa, claro que haciendo uso de su voz como senadora elegida por el centro democrático, y ahora que es bien sonado del tema del Palacio, ha hecho eco de la inocencia del Coronel.

A hoy, Plazas Vega espera que la Corte Suprema de Justicia decida sobre su condena, lo que para muchos medios será una decisión “polémica”; los magistrados que están de acuerdo con la absolución, sostienen que no existen evidencias que demuestren que Plazas Vega dio la orden de desaparecer a quienes salieron vivos del Palacio, para los restantes (y algo que no es ajeno a la realidad), es que existió una omisión por parte del Coronel, pues no hizo nada evitarlo.

Desde mi opinión, si el fallo de la corte es absolutorio, la posibilidad de victimizar al Coronel y solicitar indemnizaciones por daño al buen nombre por los años que lleva el coronel cumpliendo una condena son altos, y si el fallo resulta contrario, para muchos existiría justicia, pero lo que si es cierto, es que hasta que no se sepa toda la verdad, no habrá pena, condena, conmemoración o perdón que pueda resarcir los daños causados ese día.
