Title: Ad portas de la fase pública de conversaciones  ELN - Gobierno
Date: 2016-09-28 17:28
Category: Nacional, Paz
Tags: conversaciones de paz con el ELN, diálogos de paz con el ELN, ELN Conversaciones de paz
Slug: ad-portas-de-la-fase-publica-de-conversaciones-eln-gobierno
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/07/dialogos-Eln-y-gobierno-e1468532020926.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo Contagio Radio ] 

###### [28 Sept 2016] 

Mientras el 69.6% de los colombianos apoya el inicio de la fase pública de negociaciones entre el Gobierno y la guerrilla del ELN, diversas iniciativas como la campaña Por Una Paz Completa saludan que las partes hayan dado serias señales de iniciación de la mesa de negociación. Por un lado el comandante Pablo Beltrán aseguró que el ELN [[suspenderá sus operaciones ofensivas para las votaciones del plebiscito](https://archivo.contagioradio.com/eln-decreta-cese-de-operaciones-ofensivas-para-votacion-del-plebiscito/)] y por el otro el presidente Juan Manuel Santos afirmó que si liberan secuestrados la **mesa podría iniciar la próxima semana**.

Por su parte el analista Víctor De Currea Lugo asegura que estos anuncios son mensajes contundentes para aquellos escépticos que creen que es imposible el restablecimiento del proceso entre el Gobierno y el ELN, pues demuestran que **los canales siguen vivos y que las partes mantienen su voluntad de paz y negociación**. Si bien han primado las tensiones, en este momento hay un tono y un lenguaje que es bienvenido, agrega.

El ELN ha entendido el daño político del secuestro, afirma el analista, quien insiste en que "la paz tiene combustible suficiente para muchos años de debate", y que esta guerrilla debe saber leer los tres mensajes que va a dejar el plebiscito, como cierre importante para consolidar una tendencia nacional a favor de la paz, como mensaje claro a la extrema derecha representada en Uribe y el paramilitarismo y como **mandato popular para que Gobierno y ELN se sienten a negociar**, "[[porque una paz sin ellos es incompleta](https://archivo.contagioradio.com/hay-que-insistir-en-que-gobierno-y-eln-se-sienten-a-negociar-victor-de-currea/)]".

Al respecto la guerrilla del ELN ha asegurado que "está por la paz, que esperan y anhelan los colombianos y el mundo. Una paz con democracia y equidad social. Así no compartamos varios de los puntos de los acuerdos de La Habana, consideramos que **la paz es un camino que apenas empieza y que no se ha completado**. El sueño del ELN es contribuir de manera significativa, propiciando una protagónica participación de la sociedad".

Agregando que "Por encima de las diferencias está el bien de toda la nación. A fin de facilitar el desarrollo de este proceso, no realizaremos acciones militares entre el 30 de Septiembre y el 5 de octubre, para que la población participe como estime conveniente en el Plebiscito. **Estamos listos para continuar en la fase pública de negociaciones con el gobierno**, dándole continuidad a lo acordado el 30 de marzo y buscándole salidas a las dificultades".

###### [Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)] ] 
