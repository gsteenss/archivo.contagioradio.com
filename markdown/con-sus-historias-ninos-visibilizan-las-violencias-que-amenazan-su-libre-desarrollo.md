Title: Con sus historias niños visibilizan las violencias que amenazan su libre desarrollo
Date: 2019-05-24 16:31
Author: CtgAdm
Category: Comunidad, Educación
Tags: educacion, Fundación Creciendo Unidos
Slug: con-sus-historias-ninos-visibilizan-las-violencias-que-amenazan-su-libre-desarrollo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/Niñez.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/Niñez1.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Fundación Creciendo Unido] 

En el marco de la **Asamblea Nacional de Infancia, la Fundación Creciendo Unidos**, organización que desde 1986 ha trabajado por  los niños, niñas y adolescentes en Bogotá y Norte de Santander que han sido víctimas de diversos tipos de violencia, presentarán la Agenda Territorial de los niños y niñas, un informe elaborado por los cerca de 400 menores que participan del proceso.

Según **Olga Díaz, educadora de la fundación**, desde 2016 se generó este proceso de construcción a través de una asamblea infantil en la que visibilizan las principales problemáticas que viven en sus ciudades y fenómenos como el microtráfico, la limpieza social, las pandillas y  las fronteras invisibles que surgen en sus barrios creadas por bandas criminales.

Todas estas problemáticas son recopiladas por los niños de Creciendo Unidos quienes, como parte de la presentación socializarán este informe a través de 22 niños, delegados de la fundación quienes presentar la agenda territorial y cómo la ven ellos como niños y niñas, siempre buscamos que sean ellos los que cuenten, los que motiven a la sociedad, a los adultos a que se merecen un país distinto" indicó Olga.

### Los problemas que enfrentan los niños

Olga Díaz señala que las principales problemáticas encontradas a lo largo de la presentación de sus más de 30 años de experiencia han sido la explotación infantil y la dificultad que han experimentado las familias que debido a dificultades económicas llevan a sus niños a trabajar. [(Lea también: Por medio de dibujos y fotografías, niños Embera reconstruyen la memoria)](https://archivo.contagioradio.com/por-medio-de-dibujos-y-fotografias-ninos-embera-reconstruyen-la-memoria/)

Otra de las principales problemáticas que han encontrado, principalmente en Norte de Santander ha sido el fenómeno del reclutamiento de menores por parte de grupos armados, cifras que se han incrementado por la disputa de los territorios en este año, problema del que se deriva el desplazamiento de las comunidades, que también afecta a los más pequeños.

Asímismo y con el incremento del éxodo de la población en Venezuela, la fundación se han enfocado en la población infantil de este país, pues el número de niños y niñas refugiadas ha ido en aumento, según la Unicef en las escuelas de Cúcuta se han aceptado a casi 10.000 menores venezolanos.

### **Cambios generacionales**

La educadora resalta que lo largo de los 32 años en los que han trabajado por la infancia de Colombia, han visto el proceso de los niños y cómo ellos mismo comenzaron a gestionar sus proyectos de vida y al crecer han logrado ingresar a bachillerato, "se trata de romper ese ciclo generacional y que los niños gocen de sus derechos".

<iframe id="audio_36309572" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_36309572_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
