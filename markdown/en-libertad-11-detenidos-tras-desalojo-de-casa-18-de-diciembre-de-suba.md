Title: En libertad 11 detenidos tras desalojo de Casa "18 de Diciembre" de Suba
Date: 2015-07-17 17:29
Category: Nacional
Tags: Casa 18 de Diciembre, desalojo forzoso, ESMAD, Suba
Slug: en-libertad-11-detenidos-tras-desalojo-de-casa-18-de-diciembre-de-suba
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/07/Casa-18-dic.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: contagioradio.com 

###### [17 Jul 2015] 

Hacia las 5 de la tarde de este viernes se conoció que las 11 personas detenidas en el desalojo de la Casa Cultural 18 de Diciembre en la localidad de Suba fueron dejadas en libertad luego de que se logró demostrar su inocencia ante los delitos que pretendían imputarse por parte de la Fiscalía, según algunas fuentes, se les habría imputado el delito de agresión a servidos público, tras intentar impedir el desalojo.

Hacia las 5:51 de la tarde salían en medio de aplausos, arengas y el retumbar de los tambores de la batucada.

\[caption id="attachment\_11414" align="aligncenter" width="360"\][![Libertad](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/07/CKJqW1jWgAEJnin.jpg){.wp-image-11414 width="360" height="480"}](https://archivo.contagioradio.com/en-libertad-11-detenidos-tras-desalojo-de-casa-18-de-diciembre-de-suba/ckjqw1jwgaejnin/) Tomada de twitter de Frente Amplio Suba\[/caption\]  
\[caption id="attachment\_11415" align="aligncenter" width="363"\][![Tomada de twitter de Frente Amplio Suba](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/07/CKJqYs9WEAEUKLL.jpg){.wp-image-11415 width="363" height="483"}](https://archivo.contagioradio.com/en-libertad-11-detenidos-tras-desalojo-de-casa-18-de-diciembre-de-suba/ckjqys9weaeukll/) Tomada de twitter de Frente Amplio Suba\[/caption\]

Noticia en desarrollo...
