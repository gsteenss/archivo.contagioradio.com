Title: La dictadura persiste contra el pueblo mapuche en Chile
Date: 2020-06-11 21:42
Author: CtgAdm
Category: Actualidad, El mundo
Tags: Asesinato de indígenas, Chile, Comunidades Mapuche
Slug: la-dictadura-persiste-contra-el-pueblo-mapuche-en-chile
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/06/Mapuches.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: comunidad mapuche/ @caldillodetripa

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

A propósito del más reciente informe de[Indepaz,](https://twitter.com/Indepaz) que revela cómo 269 líderes indígenas han sido asesinados tras la firma del Acuerdo de Paz y al menos 167 durante la presidencia de Iván Duque, vale la pena analizar cuál es la situación de DD.HH. en países del continente como Chile, donde se vive una continua persecución contra la comunidad mapuche y **donde según medios locales, han sido 15 los mapuches asesinados durante los gobiernos de Ricardo Lagos, Michelle Bachelet y Sebastián Piñera.**

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

**Manuela Royo, integrante del Movimiento de Defensa por el acceso al Agua, la Tierra y la Protección del Medio Ambiente (Modatima)** y asesora de la alianza territorial mapuche señala que en Chile existe históricamente una situación casi de la misma complejidad entre los pueblos indígenas y el Gobierno y que se ha incrementado con el paso de los años. **A lo largo de los últimos nueve años, el Instituto Nacional de Derechos Humanos ha presentado más de treinta quejas sobre acciones abusivas de la policía en contra de los mapuches.**

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### La persecución contra el pueblo mapuche va en aumento

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Al respecto, la defensora. resalta que Chile fue condenado en el 2014 por la Corte Interamericana de DD.HH. por la vulneración a los pueblos indígenas al aplicar la Ley Antiterrorista (ley reformada en la actualidad y promulgada por Pinochet en 1984) a integrantes del pueblo Mapuche, violando sus derechos a la igualdad y la no discriminación. En aquel entonces organizaciones como el Centro por la Justicia y el Derecho Internacional señalaron que **dicha ley, constituía una respuesta inadecuada del Estado ante la movilización social de un pueblo que busca defender sus derechos ancestrales.**

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Pese a que existen resoluciones internacionales de este talante las violaciones de DD.HH. siguen sucediendo y se ha incrementado la persecución a líderes indígenas mapuches, exponiendo de ejemplo el caso de la Operación Huracán, una investigación de los Carabineros de Chile amparados por la Ley de Inteligencia que detuvieron a ocho comuneros​ mapuches supuestamente involucrados en una asociación ilícita terrorista, descubriendose después que la Policía habría manipulado las pruebas que incriminaban a los detenidos.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Manuela Royo advierte que la persecución puede incluso empeorar, pues desde el Gobierno, Sebastián Piñera ha dado prioridad a un proyecto de ley que busca fortalecer y modernizar el Sistema de Inteligencia del Estado que permitiría convertir en objeto de investigación **"a grupos nacionales, incluidos movimientos ambientalistas y grupos indígenas",** resaltando que de igual forma ha incrementado la persecución penal a raíz de las protestas de 2019. [(Le puede interesar: Mujeres e indígenas, tienen la clave para afrontar crisis del Capitalismo: R Zibechi)](https://archivo.contagioradio.com/es-momento-de-aprender-de-las-mujeres-y-los-pueblos-indigenas-raul-zibechi/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Si bien en Chile, no existe una cifra de indígenas asesinados como en Colombia, la defensora señala que casos como el del mapuche Camilo Catrillanca joven indígena de 24 años, el 14 de noviembre de 2018 han permitido evidenciar la lucha por el territorio ancestral y la represión por parte de la Fuerza Pública contra las comunidades, sin embargo otros hechos de agresiones siguen sucediendo y quedan en el anonimato. [(Lea también: 167 líderes indígenas han sido asesinados durante la presidencia de Iván Duque : Indepaz)](https://archivo.contagioradio.com/167-lideres-indigenas-han-sido-asesinados-durante-la-presidencia-de-ivan-duque-indepaz/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

**El pasado 4 de junio, fue asesinado el werkén (autoridad tradicional del pueblo mapuche), Alejandro Treuquil,** según su viuda, Andrea Neculpan, viuda, previo a los hechos existieron amenazas de muerte que recibió su marido por parte de los Carabineros, policía militar chilena. A su vez, comunidades indígenas del país austral vienen denunciando represión por parte de los carabineros en regiones como Lonquimay donde se han realizado desalojos injustificados desde hace algunas semanas.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### El despojo histórico del territorio Mapuche

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

La usurpación sistemática de territorios indígenas en Chile se sitúa en 1883, cuando nació la Comisión Radicadora de Indígenas que debía reubicar a las comunidad en espacios delimitados para colonizar el resto de la zona, sin embargo de las 10 millones de hectáreas que España había reconocido como territorio mapuche se entregaron sólo 536. 0000 hectáreas a 150.000 nativos, despojando a muchos de sus tierras y por ende de su agricultura como forma de sustento. [(Lea también: La unión del pueblo y la persistencia: claves en la movilización de Chile)](https://archivo.contagioradio.com/la-union-del-pueblo-y-la-persistencia-claves-en-la-movilizacion-de-chile/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Aunque en 1930 se eliminó la figura de la Comisión Radicadora, durante los siguientes treinta años las comunidades indígenas fueron divididas en pequeños grupos y durante la época de la dictadura de Augusto Pinochet , se persiguió a dirigentes indígenas y se retrocedió en la restitución territorial. Según el censo de 2017 cerca de 614.881 mapuches viven en las ciudades como resultado de este despojo.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Según explica Royo, en Chile la constitución no reconoce la existencia de los pueblos indígenas a diferencia de otros países del continente, idea de la qe parte para expicar por qué, si no existen reconocimientos a sus derechos territorial y culturales, en la práctica se les impone a las comunidades "leyes propias de occidente y no reconoce leyes diferenciales para los pueblos originarios, algo que se ve directamente ligado a la criminalización por parte del Estado que "asocia la figura del mapuche con una figura criminal".

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

La defensora de DD.H. concluye que al igual que la persecución contra mapuches, también persisten las interceptaciones y seguimientos, agregando que en Chile la constitución vigente es de la dictadura al igual que la ley antiterrorista que apunta a a una persecución a organizaciones internas, perpetuando el mismo modelo de represión hasta nuestros días. [(Lea también: De Francia a Latinoamérica, la lucha contra la precarización de la vida)](https://archivo.contagioradio.com/de-francia-a-latinoamerica-la-lucha-contra-la-precarizacion-de-la-vida/)

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
