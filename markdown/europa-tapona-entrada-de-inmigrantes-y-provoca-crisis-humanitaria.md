Title: Europa tapona entrada de inmigrantes y provoca crisis humanitaria
Date: 2015-05-06 15:19
Author: CtgAdm
Category: El mundo, Otra Mirada
Tags: 6.000 inmigrantes llegan a Sicilia, Campaña CIES No, Centros de Internamiento de Extranjeros, Emergencia humanitaria en el mediterraneo, francia, Inmigrantes llegan a Europa, Italia, Libia
Slug: europa-tapona-entrada-de-inmigrantes-y-provoca-crisis-humanitaria
Status: published

###### Foto:20minutos.es 

###### Entrevista con[ **Carlos Soledades**], miembro Campaña CIES NO: 

<iframe src="http://www.ivoox.com/player_ek_4454822_2_1.html?data=lZmilp2Wdo6ZmKiak5eJd6KmkpKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRl8ro1sbQy4qnd4a2lNOYy9PRrcjmwtPhx9iPqc%2BfxtGYz8rIrdXZ09fO0MrTcYarpJKw0dPYpcjd0JC%2Fw8nNs4yhhpywj5k%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

Este fin de semana la **Guardia Costera italiana** y el equipo de rescate de **Médicos sin Fronteras** rescataron un total de **6.639 inmigrantes,** sin embargo se presentaron 8 desaparecidos y 2 ahogados mientras se desarrollaba el operativo.

Conforme se acerca el verano el flujo va en aumento debido al buen tiempo, pero después de la **muerte de más de 1.200 inmigrante**s en las últimas semanas, Francia, España y sobretodo Italia ha reforzado la búsqueda de los inmigrantes con 5 buques de guerra más y 6 botes con preparación para el salvamento.

Para **Carlos Soledades**, activista de la campaña española contra los **Centros de Internamiento de Extranjeros**, Europa está taponando la entrada de inmigrantes subsaharianos provocando una emergencia humanitaria y permitiendo así que las mafias puedan desarrollar su trabajo en medio de graves violaciones de los DDHH.

Los **Centros de Internamiento de Extranjeros** se extienden por toda Europa pero se concentran en España, Italia y Grecia, debido a que son los países que constituyen la puerta de entrada desde el sur.

En dichos centro se **recluye a los inmigrantes ilegalmente** como si hubieran cometido algún delito por el mero hecho de inmigrar, además de ello las organizaciones sociales de DDHH han emitido informes donde se testimonios de las **graves violaciones de DDHH que se cometen en su interior**.

La avalancha de extranjeros que intenta entrar a través de las **costas de Libia tiene que pagar altos precios a mafias establecidas** que los transportan en barcazas de extrema peligrosidad y hacinamiento. Además la situación en Libia es extremadamente grave debido a la violencia entre facciones y el creciente conflicto armado.

Por último la procedencia de los inmigrantes, **casi en su totalidad subsahariano**, hace que la inmigración adquiera un carácter económico pero también humanitario, debido a los conflictos armados que se están viviendo en la región. Esto implicaría que muchos de los inmigrantes serían potenciales solicitantes de asilo político y refugio.

De tal manera Carlos relata que la mayoría de inmigrantes ilegales **son refugiados políticos de guerra** debido a que proceden de conflictos armados como Congo, Somalia, Ruanda, o Nigeria, donde las violaciones de DDHH son el orden del día y donde posiblemente hayan sido perseguidos política o socialmente.

También nos recuerda que las "**devoluciones en caliente"** consisten **en retornar inmediatamente a los inmigrantes infringiendo la actual normativa de acuerdo a los derechos humanos** que indica que el inmigrante debe acudir a una comisaría de policía donde debe recibir apoyo jurídico y psicológico antes de comenzar el trámite para su devolución. Esta es una práctica generalizada en países como España o Italia.

Para el activista de la campaña CIES No el actual **gobierno griego que ha decidido cerrar los CIES** o ejemplos como el presidente de la isla de Lampedusa en Italia que esta de acuerdo con la libre movilidad de personas son ejemplos de cambio y de esperanza para desbloquear la emergencia humanitaria. Pero también puntualiza que la **UE bloquea los intentos de cambio y boicotea las propuestas alternativas**.
