Title: Bienvenido papa Francisco
Date: 2017-03-15 09:38
Category: Abilio, Opinion
Tags: Iglesia, iglesia católica, Papa Francisco, Teologia, Visita a Colombia
Slug: bienvenido-papa-francisco
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/03/papa-en-colombia.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Presidencia México 

#### Por **[Abilio Peña ](https://archivo.contagioradio.com/abilio-pena-buendia/) - [~~@~~Abiliopena](https://twitter.com/Abiliopena)

###### 15 Mar 2017 

A buena hora llega el papa Francisco a Colombia. Sin lugar a dudas su palabra profética ha animado laicos, religiosas, sacerdotes y algunos obispos a hablar más claro en consonancia con  la búsqueda de la justicia imperativa de los evangelios. Y  no es para menos. En medio de la tibieza de los pronunciamientos  o del claro compromiso con los sectores de poder, retumba, como signo de contradicción,   la palabra del propio papa, mucho, muchísimo más comprometida con los empobrecidos  que la gran mayoría de los católicos de  nuestro país.

El 6 de septiembre  de este año pisará tierra colombiana no solo Francisco, sino la propuesta de renovación eclesial que representa. Ante la ausencia de  actitud penitencial frente a responsabilidades históricas de la iglesia en la violencia, llega un papa que ha pedido perdón a los pueblos indígenas por el genocidio en el que estuvo comprometida la iglesia en la conquista;   ante la incapacidad  de compromiso  con el acuerdo firmado entre el gobierno y la guerrilla de las Farc-Ep, llega un papa que condicionó la visita al país a la refrendación del acuerdo y al blindaje por la comunidad internacional; ante  el silencio, roto sólo por un puñado de obispos por la destrucción del agua, de los bosques, del subsuelo, de las comunidades indígenas;  llega un papa que denunció en su encíclica Laudato Sí el modelo económico de mercado y su racionalidad instrumental; ante una institucionalidad que ha excluido a las mujeres de la toma de decisiones y del ejercicio del sacerdocio, llega un papa que propuso el diaconado femenino; ante una estructura eclesial que  niega la participación de parejas no casadas, que segrega a homosexuales; llega  un papa que llama a ser como Jesús, misericordioso, cercano, comprensivo. Ante la mercantilización de los sacramentos; llega  un papa que  invitó a los miembros de la iglesia a negarse a pagar por los bautismos, las primeras comuniones, los matrimonios, la exequias.  Nos visitará nadie más ni nadie menos que un signo de contradicción que ya ha encontrado en el vaticano poderosos detractores entre el grupo de cardenales.

El papa Francisc,o que nos visitará  en septiembre, es el mismo que ordenó a la iglesia Argentina y al propio Vaticano, [abrir los archivos sobre la dictadura militar](http://internacional.elpais.com/internacional/2016/10/25/actualidad/1477397403_091562.html),  con la que no pocos miembros de la iglesia  de ese país estuvieron comprometidos y por lo que en viarias oportunidades han pedido  perdón.

Sacerdotes castrenses (diócesis exclusiva para  la atención de militares)  y miembros de la jerarquía apoyaron decididamente a los oficiales comprometidos con la desaparición forzada y la tortura en el país sudamericano.

También en Colombia se sabe de casos de tortura que fueron de conocimiento de sacerdotes de la, entonces, vicaría castrense en la oscura época del presidente Julio Cesar Ayala. Más recientemente el silencio de ésta jurisdicción eclesiástica ante los crímenes  de lesa humanidad conocidos como “falsos positivos”,  ha sido   elocuente. Muchas brigadas y divisiones del Ejercito implicadas en horrendos crímenes son atendidas por ésta diócesis. El testimonio de Francisco de abrir los archivos de la iglesia en Argentina, puede inspirar también la exigencia de las víctimas de una verdad, que en tiempos en que se implementan los acuerdos con la guerrilla de las Farc-Ep y se dialoga con el Eln, será un aporte significativo a la reconciliación.

Puede ser la oportunidad para que el papa Francisco tome la decisión de clausurar la diócesis castrense en Colombia, como un signo de reconciliación y de independencia de la iglesia frente al poder militar claramente implicado en violaciones a los derechos humanos en nuestro país, tal como se lo han pedido más de 600 católicos y católicas de Colombia y del mundo.

Justo quien está encargado de la organizar la visita del papa Francisco en Colombia es Monseñor Fabio  Suescún  Mutis, el obispo de la diócesis castrense, quien lanzó la imagen oficial  invitando a los Colombianos a que “demos el primer paso” y agrega  que “la visita del papa Francisco es un momento de gracia y alegría para soñar con la posibilidad de dar el primer paso. [El Santo Padre es un misionero de la reconciliación](https://www.cec.org.co/sistema-informativo/destacados/el-papa-invita-dar-el-primer-paso-hacia-la-reconciliaci%C3%B3n)” .  Estas palabras  nos hacen soñar con signos concretos que nos permitan dar ese “primer paso”:  pedir perdón por la documentada responsabilidad en la violencia y  clausurar la diócesis castrense  serían signos  de que la iglesia colombiana se enruta hacia la renovación que anima el papa Francisco.

#### **[Leer más columnas de Abilio Peña](https://archivo.contagioradio.com/abilio-pena-buendia/)** 

------------------------------------------------------------------------

####  

###### Las opiniones expresadas en este artículo son de exclusiva responsabilidad del autor y no necesariamente representan la opinión de Contagio Radio.
