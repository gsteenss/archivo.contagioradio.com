Title: Niñas y niños indígenas de Colombia están siendo víctimas de "genocidio"
Date: 2016-05-10 15:55
Category: DDHH, Nacional
Tags: CIDH, ICBF, La Guajira, niños indígenas
Slug: ninos-indigenas-de-colombia-estan-siendo-victimas-de-genocidio
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/05/niños-Guajira.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: primicidiario 

###### [10 May 2016] 

Desde el ministerio de Salud se asegura que **la tasa de mortalidad infantil asociada a la desnutrición aguda en el departamento de La Guajira aumentó un 50 % entre 2011 y 2015.** Y aunque la media nacional de mortalidad infantil es de 18 sobre 1000 nacidos vivos, en regiones donde habitan las comunidades indígenas la cifra es alarmante, por ejemplo en Nariño donde están los awá, son 166 los niños muertos por cada 1000 y en el Cesar, donde viven los Yucpas es de 200.

Frente a esa situación, desde el Movimiento Mantas Negras, se realiza este 10 y 11 de mayo, una serie de actividades, para llamar la atención del gobierno y sus instituciones, las empresas y la sociedad civil, frente a la mortalidad de niños y niñas indígenas asociadas a **“la exclusión, la inequidad, el olvido, la desatención en salud, la corrupción administrativa y política, la falta de agua”,** como lo asegura Ruth Consuelo Chaparro, vocera del Movimiento Mantas Negras.

“Lo que está pasado es un genocidio, es un holocausto, los niños no están muriendo por desnutrición, están siendo asesinados porque quienes tienen que tomar las decisiones no lo hacen y por que quienes tienen los recursos no los destinan a los niños, porque los papales priman por encima de los seres humanos”, expresa Chaparro, quien agrega que se trata de una responsabilidad conjunta y estructural que le compete a las entidades territoriales, los alcaldes, gobernadores, que deben incluir en el plan de desarrollo los planes de vida y salvaguarda de los niños, pero estos no están apareciendo en los planes de desarrollo.

Según la vocera de FUCAI, los **jueces están negando las tutelas del derecho al agua, a las empresas no les interesa el ambiente ni las comunidades y la sociedad civil** tiene parte de responsabilidad por la indiferencia y por haberse callado frente a lo que viene sucediendo hace años con la niñez indígena.

Para evidenciar esa situación, se creó el **Movimiento de las Mantas Negras**; conformado por autoridades indígenas, líderes de diferentes pueblos indígenas del país, autoridades de base, defensores de Derechos Humanos,  artistas y sociedad civil, que indignados por lo que ocurre a la niñez  de los diferentes pueblos indígenas de Colombia se unen para realizar actividades como la de este miércoles donde se realizará un ritual de duelo **con más de mil ataúdes, “porque los niños deben ser reconocidos y su muerte debe ser honrada, para decir no más”**, expresa Chaparro.

El Movimiento de las Mantas Negras se dará cita este miércoles en la Plaza de Bolívar de Bogotá para realizar el ritual de duelo “Entre Mantas y Llantos” para mostrar la situación de los niños y niñas indígenas, pero además, hacer llegar al gobierno su pliego de peticiones con el objetivo de que las instituciones y específicamente el Instituto de Bienestar familiar actúen frente a la mortandad de infantes, a partir de las siguientes acciones.

<u>Acciones de corto plazo:</u>

1.  Que la Corte Interamericana de Derechos Humanos (CIDH) no levante en La Guajira las medidas cautelares para la preservación de la vida y la integridad personal de los niños, niñas y adolescentes de los municipios de Uribia, Maicao, Manaure y Riohacha hasta tanto no sean garantizados los derechos al agua, a la alimentación y a la atención y cuidado pertinentes por parte de las entidades responsables y que esto se haga de manera sostenible y con verificación en campo.

<!-- -->

1.  Que se implementen los planes de vida y salvaguarda como respuesta efectiva para proteger la vida de los pueblos indígenas exigida por la Corte Constitucional mediante Auto 004 de 2009.

<!-- -->

3.  Que los órganos de control cumplan con la función de vigilar y sancionar de manera oportuna y eficaz a entidades y servidores públicos de los distintos niveles que omitan el cumplimiento de sus funciones en la garantía de los derechos de la niñez indígena.
4.  Que las entidades de control y las organizaciones de base exijan e inspeccionen que alcaldes y gobernadores incorporen en sus planes de desarrollo: los planes de vida y salvaguarda indígenas así como los programas y proyectos,  que beneficien de forma directa a la niñez indígena, siempre en concertación con las autoridades indígenas de las distintas zonas.

<u>Acciones de mediano plazo:</u>

<ol start="5">
5.  Que las entidades públicas encargadas de los programas dirigidos a la niñez indígena incrementen la cobertura y hagan efectivo el enfoque diferencial, fortaleciendo los gobiernos propios y administraciones locales en el marco del decreto autonómico 1953 de Octubre 7 de 2014.
6.  Que el ICBF asuma con mayor diligencia el rol de rector y articulador del Sistema Nacional de Bienestar Familiar para dar respuestas efectivas e inmediatas.
7.  Que la rama judicial forme y monitoree el ejercicio de los jueces en materia de garantía de derechos de los niños.
8.  Que el Estado Colombiano tome en serio su papel de protector del medio ambiente y que con base es estudios técnicos serios y consultas previas pertinentes controle de manera efectiva el accionar de empresas privadas dedicadas a la minería, a los hidrocarburos, y a la explotación de recursos del suelo y el subsuelo.
9.  Que la protección de las fuentes de agua y el acceso a la misma por parte de las comunidades prime sobre usos comerciales con intereses particulares.
10. Que las acciones estén orientadas a la superación de la problemática en forma integral, estructural y articulada en los planes de cida y salva guarda, en los planes de desarrollo de las entidades territoriales y que se cuente con indicadores de evaluación que permitan monitorear la superación efectiva de la inequidad y la exclusión de la niñez indígena y sus comunidades.

</ol>
<iframe src="http://co.ivoox.com/es/player_ej_11481067_2_1.html?data=kpahmpaUepihhpywj5aVaZS1lpuah5yncZOhhpywj5WRaZi3jpWah5yncbPp1c2Ypc3FtMLm09SYj5Cxs9fdzs7S0NnTb67Vz9nO1ZCyqcjmwtiah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ%3D%3D&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)
