Title: Policía mexicana reprime brutalmente a profesores en Acapulco
Date: 2015-02-26 06:19
Author: CtgAdm
Category: Movilización, Otra Mirada
Tags: Acapulco, magisterio de Guerrero, Profesores Acapulco protestan, Represión magisterio Acapulco
Slug: policia-mexicana-reprime-brutalmente-a-profesores-en-acapulco
Status: published

###### Foto:Noticaribe.com 

##### **Entrevista a [Vidulfo Rosales]:** 

<iframe src="http://www.ivoox.com/player_ek_4138249_2_1.html?data=lZagmpeYfY6ZmKiakpWJd6KkkZKSmaiRdI6ZmKiakpKJe6ShkZKSmaiRhtPp1cbZjdfJtNPZ1M6SpZiJhpTijMjc0NnWpYzhwtfQysaPqMaf0dfcyMrXs9PZ1JDS0JClp8Lk1tGah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

Ayer continuaron las **protestas de los maestros del Magisterio en Guerrero, la movilización pretendía tomar el aeropuerto de Acapulco cuando fueron brutalmente reprimidos por la policía federal**. Los maestros **reivindicaban el pago atrasado de sus salarios**, así como la aparición de los 43 estudiantes desaparecidos en la Escuela Normal rural de Ayotzinapa.

Los maestros habían programado una **mesa de negociación con el gobierno, pero esta fue suspendida,** y así, decidieron continuar con las protestas con la idea de en un futuro poder sentarse y negociar.

En el transcurso de la marcha la **policía atacó fuertemente contra la primera fila conformada por mujeres y niños**, en total las cifras oficiales hablan de **72 detenidos y un profesor jubilado muerto en la carga policial.**

**Claudio Castillo Peña** era un **profesor que llevaba 8 años jubilado y que estaba participando en la protesta, fue golpeado brutalmente por la policía cuando inició la represión, horas más tarde fallecía en el hospital**.

Hasta ahora los **maestros están resguardados en una tienda de Punta Diamante**, a la que ha intentado acceder la policía en reiteradas ocasiones. El **colectivo de profesores acusa a Rogelio Ortega Martínez y a Enrique Peña Nieto de la represión sufrida el día de ayer.**

En el momento **todos los profesores han sido liberados** y han continuado las fuertes protestas, en solidaridad con los detenidos, el profesor asesinado en la jornada de ayer y por las reivindicaciones del sector de Magisterio.

Entrevistamos a **Vidulfo Rosales, abogado defensor de DDHH de los familiares de los 43 desparecidos en Ayotzinapa**, quién nos relata que la represión contra los movimientos sociales y el sector educativo en México se ha convertido en una práctica habitual y sistemática por parte del estado.

Así como nos recuerda que **hoy se cumplen 5 meses de la desaparición forzada de los 43 estudiantes normalistas en Ayotzinapa**, y se han convocado marchas de solidaridad en todo el país con el objetivo de que aparezcan ya y se pueda ajusticiar a los responsables.
