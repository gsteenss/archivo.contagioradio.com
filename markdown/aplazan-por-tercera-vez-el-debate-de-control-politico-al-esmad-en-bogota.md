Title: Aplazan por tercera vez el debate de control político al ESMAD en Bogotá
Date: 2020-02-06 16:50
Author: CtgAdm
Category: Movilización, Política
Tags: Bogotá, Debate de Control Político, ESMAD
Slug: aplazan-por-tercera-vez-el-debate-de-control-politico-al-esmad-en-bogota
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/WhatsApp-Image-2019-10-18-at-12.46.29-PM-e1571421549155.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:heading {"align":"right","level":6,"textColor":"cyan-bluish-gray"} -->

###### Foto: Contagio Radio {#foto-contagio-radio .has-cyan-bluish-gray-color .has-text-color .has-text-align-right}

<!-- /wp:heading -->

<!-- wp:html -->

<figure class="wp-block-audio">
<audio controls src="https://co.ivoox.com/es/heidy-sanchez-sobre-aplazamiento-debate-control_md_47430964_wp_1.mp3">
</audio>
  

<figcaption>
Entrevista &gt; Heidy Sanchéz| Consejala de Bogotá

</figcaption>
</figure>
<!-- /wp:html -->

<!-- wp:paragraph -->

Este jueves 6 de enero **fue aplazado por tercera vez el debate de control político al Escuadrón Móvil Antidisturbios (ESMAD)** en el Concejo de Bogotá. Unas recusaciones anónimas a concejales sobre la elección de Personero han provocado la reprogramación del evento, en el que se discutirá la relevancia de esta fuerza en el marco de la protesta social.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### **Recusaciones a concejales anónimas, lo que ha impedido el debate**

<!-- /wp:heading -->

<!-- wp:paragraph -->

La concejala Heidy Sánchez, citante al debate, explicó que el debate fue aplazado porque llegaron recusaciones anónimas a los cabildantes, con números de teléfono y cédulas falsas, para la apertura de la convocatoria para la elección del Personero. Aunque los datos son falsos, los concejales tuvieron que resolverlas en plenaria, lo que obligó a aplazar el debate.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Si este jueves se resuelven dichas recusaciones, **el debate podría realizarse el próximo lunes 10 de febrero a las 9 de la mañana**, de lo contrario, tendría que ser pospuesto una vez más. (Le puede interesar: ["En 20 años el ESMAD ha asesinado a 34 personas"](https://archivo.contagioradio.com/en-veinte-anos-el-esmad-ha-asesinado-a-34-personas/))

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### **Defender al ESMAD, crear un protocolo para su acción o eliminarlo**

<!-- /wp:heading -->

<!-- wp:paragraph -->

Sánchez señaló que en el concejo hay, por lo menos, tres posiciones respecto al ESMAD: La derecha, que lo defiende; las bancadas de Gobierno que defienden el protocolo y la acción del Escuadrón en este; y la visión de la Colombia Humana que dista de ambas posiciones. La Concejala por esa colectividad afirmó en ese sentido que en lugar del uso de la fuerza, la acción de los gobernantes debería centrarse en resolver las razones de la protesta, en lugar de disolverla.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/ClaudiaLopez/status/1225213990583853057","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/ClaudiaLopez/status/1225213990583853057

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

Sánchez también se refirió a las declaraciones de la Alcaldesa, en las que señalaba que las disidencias de las FARC y el ELN estaban pagando a personas durante las protestas para generar hechos de violencia. La Concejala dijo que esa afirmación era una forma más de estigmatización al movimiento social, agravado por el hecho de que no haya investigación judicial al respecto.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### **¿Por qué hablar sobre el fín del ESMAD?**

<!-- /wp:heading -->

<!-- wp:paragraph -->

La Concejala explicó que el debate tiene tres objetivos: Evidenciar lo obsoleta e innecesaria que es la fuerza del ESMAD; denunciar el costo del Escuadrón y establecer un diálogo sobre lo que significa el ejercicio del derecho a la protesta. (Le puede interesar: ["El uribismo sigue culpando a 'las FARC' de lo que pasa en Colombia"](https://archivo.contagioradio.com/el-uribismo-sigue-culpando-a-las-farc-de-lo-que-pasa-en-colombia/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En cuanto al primer objetivo, Sánchez sostuvo que el Escuadrón fue creado en el marco del conflicto armado y tiene una doctrina obsoleta. Adicionalmente, el uso excesivo de la fuerza a lo largo de sus 20 años ha significado la pérdida de, por lo menos, [34 personas](https://www.academia.edu/41239342/Silencio_Oficial_un_aturdido_grito_de_justicia_por_los_20_a%C3%B1os_del_Esmad). (Le puede interesar: ["Las razones para pedir el desmonte del ESMAD a 20 años de su creación"](https://archivo.contagioradio.com/las-razones-para-pedir-el-desmonte-del-esmad-a-20-anos-de-su-creacion/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por otra parte, también se cuestionará el costo del ESMAD **(490 mil millones de pesos anuales)** y lo que significa tal gasto, tomando en cuenta otras necesidades de inversión que solventarían algunos de los problemas que evocan las manifestaciones, como la gratuidad en la educación o garantía en el acceso a la salud.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por último, Sánchez dijo que se hablaría del ejercicio de la protesta porque la Alcaldía en su protocolo estableció una forma de protestar que, por ejemplo, restringe los espacios por los que se puede marchar. Situación que desconoce que la protesta social puede bloquear vías y no requiere permisos para su realización.

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78980} /-->
