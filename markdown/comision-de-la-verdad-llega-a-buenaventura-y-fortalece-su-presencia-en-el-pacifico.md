Title: Comisión de la Verdad llega a Buenaventura y fortalece su presencia en el Pacífico
Date: 2019-07-12 16:18
Author: CtgAdm
Category: Nacional, Paz
Tags: buenaventura, Casas de la Verdad, comision de la verdad, pacífico
Slug: comision-de-la-verdad-llega-a-buenaventura-y-fortalece-su-presencia-en-el-pacifico
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/Comisión-Bueaventura.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: @ComisionVerdadC] 

El pasado 11 de julio fue el lanzamiento de la **Casa de la Verdad en Buenaventura, Valle del Cauca**, la tercera de las sedes de la Comisión de la Verdad ubicada en la región del pacífico junto a las de Quibdó  y Tumaco y la que, según **su coordinadora, Eliana Ángulo tendrán un gran enfoque étnico que será determinante en el análisis del conflicto que ha marcado a la región.**

La coordinadora territorial de Buenaventura explicó que se están realizando procesos de articulación y pedagogía con la comunidad y organizaciones sociales que harán parte de la construcción de memoria en un territorio constituido por comunidades afrodescendientes,  indígenas y un número importante de población campesina, lo que será un eje diferencial en la forma de narrar qué sucedió durante el conflicto. [(Le puede interesar: Antioquia y Eje Cafetero, listos para trabajar junto a la Comisión de la Verdad)](https://archivo.contagioradio.com/antioquia-y-eje-cafetero-listos-para-aportar-sus-relatos-a-la-comision-de-la-verdad/)

### Eje diferencial en Buenaventura

"La forma en que el conflicto se exacerbó en nuestro territorio está relacionada al racismo estructural y con la forma de afectación que ha intentado romper las dinámicas culturales y ancestrales", aseguró la funcionaria, quien señaló que el fortalecimiento de **prácticas culturales desde la oralidad, la música o el teatro pueden generar cierto diferenciador en el territorio y permitir que revivan ciertas tradiciones que se perdieron con la violencia.**

Debido al corto tiempo que se tiene para ejecutar el trabajo de la Comisión, Eliana clarificó que no se podrá realizar un minucioso estudio de cada hecho victimizante, pero fue enfática en que se priorizarán los casos que puedan dar cuenta de una construcción de patrones que permitan esclarecer los hechos que rodean la historia del conflicto en Colombia. [(Lea también: Comisión de la Verdad expresó su respaldo a líderes sociales del Cauca)](https://archivo.contagioradio.com/comision-de-la-verdad-expreso-su-respaldo-a-lideres-sociales-del-cauca/)

Frente a la inquietud que ha surgido alrededor del recorte de recursos que baraja el Departamento Nacional de Planeación para el Sistema de Verdad, Justicia, Reparación y No repetición, Eliana expresó que la tarea de la Comisión fue asumida como un esfuerzo conjunto, **"no es que tengamos la máxima tranquilidad, es un trabajo arduo pero sabemos que vamos a contar con apoyo"**, afirmó destacando el apoyo que ha brindado la comunidad internacional y las instituciones académicas, , afirmó.

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

<iframe id="audio_38457899" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_38457899_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
