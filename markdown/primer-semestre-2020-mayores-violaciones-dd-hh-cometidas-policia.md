Title: Policía, paramilitares y ejército son los mayores violadores de DDHH según el CINEP
Date: 2020-10-07 21:55
Author: AdminContagio
Category: Actualidad, DDHH
Tags: CINEP, DDHH, Derechos Humanos, Ejército, Informe CINEP 2020, Paramilitarismo, policia nacional
Slug: primer-semestre-2020-mayores-violaciones-dd-hh-cometidas-policia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/10/Informe-CINEP-Aumento-Violencias.jpeg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/10/Informe-CINEP-violaciones-a-los-DDHH.jpeg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: Policía/ Contagio Radio

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

El Centro de Investigación y Educación Popular -[CINEP](https://www.cinep.org.co/Home2/)- publicó su más reciente informe sobre Derechos Humanos, Derecho Internacional Humanitario y violencia política en Colombia; el cual arrojó como hallazgo **un recrudecimiento en la situación del país en estas materias respecto a estudios anteriores realizados por el mismo instituto**. Preocupa la aparición de la Fuerza Pública encabezando la lista de los actores responsables de dichas violaciones.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Uno de los hallazgos del informe es el **sostenido aumento** que se viene presentando en los registros de victimizaciones en el último año y medio, pasando de **446 en el primer semestre de 2019, 532 en el segundo semestre de ese mismo año, a un total de 609 victimizaciones en el primer semestre del 2020.** Esto es un **aumento de cerca del 37%** entre el primer semestre de este año y el mismo periodo del 2019.

<!-- /wp:paragraph -->

<!-- wp:image {"id":91023,"sizeSlug":"large"} -->

<figure class="wp-block-image size-large">
![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/10/Informe-CINEP-Aumento-Violencias.jpeg){.wp-image-91023}  

<figcaption>
Fuente: CINEP

</figcaption>
</figure>
<!-- /wp:image -->

<!-- wp:heading {"level":3} -->

### **El punto de crear la Policía es la defensa de la población**

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Según el Informe, **la Policía Nacional fue el actor que más violó los Derechos Humanos, ocupando este año, el primer lugar que en 2019 ocuparon los paramilitares.** La Policía participó en 296 casos, seguida por los grupos paramilitares con 223; y el Ejército Nacional con 106 casos. [(Lea también: "Crímenes de lesa humanidad" de la Policía irán a la Corte Penal Internacional)](https://archivo.contagioradio.com/crimenes-de-lesa-humanidad-de-la-policia-iran-a-la-corte-penal-internacional/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Para el padre **Alejandro Angulo, coordinador de del Banco de datos y violencia política del CINEP** se trata de un problema que deriva de un juego político de las administraciones locales y el uso que hacen de su facultad para manejar a la Policía y al Ejército *"una vez que entran las armas, la negociación termina y empieza la violencia".*

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Para el experto, además hay que prestar atención sobre cómo funciona la comunicación entre los miembros de la Fuerza Pública y evitar que se cometan o se estimulen las violaciones a los DD.HH y se acumulen los hechos de impunidad ante la ausencia de sanciones. [(Lea también: "Crímenes de lesa humanidad" de la Policía irán a la Corte Penal Internacional)](https://archivo.contagioradio.com/crimenes-de-lesa-humanidad-de-la-policia-iran-a-la-corte-penal-internacional/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

***"Justamente, el punto de crear la Policía en los países ha sido para la defensa de la población, queda uno preocupado porque entonces la Policía es un instrumento inútil, porque no es una protección es un riesgo"*****,** agregando además la responsabilidad que tienen de por medio las alcaldías locales y evidentemente la dirección nacional de la Policía.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

El padre asegura que en las regiones rurales se ha encontrado el mismo modelo de *"mala gestión del orden público"* que ha llevado a que se responsabilice a los trabajadores de base de la Policía y, no los altos mandos, *"no es claro nunca quién dio la orden ni cuál fue la orden que se dio \[...\] esa concepción del orden público en mano de las fuerzas armadas es errónea, el orden público está en mano de los ministerios".*

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Las zonas rurales siguen siendo las más afectadas

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Los territorios mayormente afectados por violaciones a los Derechos Humanos fueron **Bogotá con 182 victimizaciones, Antioquia con 108, Cauca con 46, Norte de Santander con 45, Santander con 43 y Putumayo con 38.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Adicionalmente, el CINEP documentó que producto de estas violaciones a los Derechos Humanos, **al menos 324 personas perdieron la vida a través de ejecuciones extrajudiciales o asesinatos en el primer semestre del año 2020**.

<!-- /wp:paragraph -->

<!-- wp:image {"id":91024,"sizeSlug":"large"} -->

<figure class="wp-block-image size-large">
![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/10/Informe-CINEP-violaciones-a-los-DDHH.jpeg){.wp-image-91024}

</figure>
<!-- /wp:image -->

<!-- wp:paragraph {"align":"justify"} -->

Para el padre, la escalada de la violencia es la evolución de un problema que evidencia una ausencia de negociación posible entre cada una de las partes de la sociedad y que requiere de un diálogo entre comunidades, actores armados y terceros como empresarios; una labor que recae en el Gobierno y que debe priorizar a las poblaciones, *"Como comunidades debemos presionar a nuestros gobernantes \[...\], no sentido hablar de derechos en un país en el que la institución política no responde".*

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### El paramilitarismo es el gran pecado colombiano

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

El informe también hace referencia a un fortalecimiento del paramilitarismo **con 163 amenazas de muerte, 47 ejecuciones extrajudiciales y 8 casos de tortura cometidas por grupos de esta índole**, en medio de una expansión paramilitar que se dio como fruto de la salida de las FARC de los territorios y el tardío accionar del Estado, lo que lleva al padre a verlo como la representación de un problema de un sector la sociedad colombiana que busca constituir ejércitos privados para obtener objetivos en su mayoría ilícitos ante una ausencia de las instituciones.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

***"Se entra en un ambiente corrompido que destruye las instituciones, las mina por dentro y las hace ineficientes, no solo es un problema moral, es un problema físico de día por día"***, expresa el experto resaltando la necesidad de instituciones fuertes y seguras en las que se puedan confiar, una tarea que señala también es responsabilidad de los medios de comunicación, *"si acabamos con nuestra comunicación sobre la base de falsas verdades, evidentemente estamos abocados a crear nuestro propio sistema de autodefensa y esa es la explicación original del paramilitarismo, "el gran pecado colombiano".* [(Lea también: MOVICE se afirma en favor de la tipificación del paramilitarismo como delito)](https://archivo.contagioradio.com/movice-se-afirma-en-favor-de-la-tipificacion-del-paramilitarismo-como-delito/)

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
