Title: En fotos | Las nuevas vidas en la Zona Veredal de Buenavista en Mesetas, Meta
Date: 2017-02-17 18:02
Category: Fotografia, Otra Mirada
Tags: FARC, proceso de paz, Zonas Veredales
Slug: en-fotos-la-vida-en-la-zona-veredal-en-buena-vista-mesetas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/02/IMG_1898-copy-1.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/02/IMG_2086-1.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/02/IMG_2091-1.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/02/IMG_2104-copy-1.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Fotos:  Carolina Covelli - Contagio Radio 

###### 17 Feb 2017 

Los lugares de guerra han quedado atrás para las y los integrantes de las FARC que llegaron a la **Zona Veredal Transitoria de Buenavista en Mesetas**, allí y bajo plásticos negros viven algunas familias que se hicieron en las montañas, algunos son acompañados por sus hijos o hijas, pequeños y pequeñas que gracias al acuerdo de paz inician su vida lejos de los sonidos de la guerra. Ver: [Las FARC-EP caminan hacia la paz](https://archivo.contagioradio.com/las-farc-ep-caminan-hacia-la-paz/)

Las armas han cambiado para ellos y ellas, sus manos ahora se ven ocupadas por sus hijos, o por la maquina de coser para arreglar un pantalón, o por las tijeras para cortar el cabello, todos y todas tienen un oficio, se colaboran como una gran familia día a día.  Aunque saben que viven momentos de dificultades, ellos tienen la esperanza de seguir construyendo, de seguir adelante en ese camino hacia la paz, en la que creen y en la que saben tienen que continuar aportando. (Ver [Inicia campaña de donaciones para guerrilleros de las Zonas Veredales](https://archivo.contagioradio.com/inicia-campana-de-donaciones-para-guerrilleros-de-las-zonas-veredales/))

Estas son algunas imágenes que relatan sus nuevas vidas en la **Zona Veredal Transitoria de Buena Vista en Mesetas.**

\

<figure>
[![IMG\_2104-copy](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/02/IMG_2104-copy-1.jpg)](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/02/IMG_2104-copy-1.jpg)

</figure>
<figure>
[![IMG\_2091](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/02/IMG_2091-1.jpg)](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/02/IMG_2091-1.jpg)

</figure>
<figure>
[![IMG\_2086](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/02/IMG_2086-1.jpg)](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/02/IMG_2086-1.jpg)

</figure>
<figure>
[![IMG\_1898-copy](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/02/IMG_1898-copy-1.jpg)](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/02/IMG_1898-copy-1.jpg)

</figure>
Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)
