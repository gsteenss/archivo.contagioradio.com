Title: Ante anuncio de Paro cívico distrito militariza el sur de Bogotá
Date: 2017-09-20 17:21
Author: AdminContagio
Category: Nacional, Resistencias
Tags: relleno doña juana, Río Tunjuelo
Slug: militarizan_sur_bogota_relleno_dona_juana
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/08/relleno-doña-juana-6.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Habitantes 

###### [20 Sep 2017] 

[Este miércoles amanecieron militarizadas dos veredas en Ciudad Bolívar y más de 100 barrios de ese sector. Además la empresa operadora** **del Relleno sanitario Doña Juana en compañía de militares y policía habrían instalado cámaras al interior de la zona con el fin de impedir el registro en video y fotografías por parte de trabajadores que han denunciado el manejo irregular y erróneo del basurero.]

[La denuncia realizada por Óscar Barón, uno de los habitantes y lideres de la protesta, también incluye la posibilidad de represión por parte de las FFMM cuando se preparan las jornadas de movilización a las que también acudirán sectores de trabajadores y habitantes de todos los barrios afectados por la contaminación de río Tunjuelo.]

[De acuerdo con  Barón, la militarización aumentó en la noche del martes y se hizo evidente esta madrugada. Para él y la comunidad esta situación “indica la intensión del alcalde que en lugar de dialogar para llegar a acuerdos prefiere la represión”. Incluso denuncia que se han instalado cámaras para impedir que los trabajadores puedan tomar fotos de la forma “desastrosa” como opera ese basurero.]

[Para la comunidad, dicha situación de militarización pretendería “dañar un paro justo y propositivo”. Barón explica que además de los impactos ambientales y sociales hay otra serie de conflictos que se extiende desde Sumapaz, hasta Bosa y Soacha.]

[El paro está coordinado por más de 100 barrios, pero también otros sectores sociales como estudiantes y bogotanos de otras áreas de la capital que han decidido apoyar el paro indefinido. Allí exigen un diálogo con el distrito para que se restaure ambientalmente la zona y se ponga en marcha un plan para acabar con los roedores y moscas que ponen en peligro la salud de la población aledaña al relleno. (Le puede interesar:[Distrito ampliaría hasta 2070 relleno doña Juana](https://archivo.contagioradio.com/distrito-ampliaria-hasta-2070-la-existencia-del-relleno-sanitario-dona-juana/))]

### **Consecuencias ambientales y sanitanitarias** 

[Y es que las consecuencias son preocupantes ya que se incumplió el contrato y la licencia ambiental. Según explica el vocero, en los últimos días en el Relleno sanitario Doña Juana se han podido ver varias grietas que crecen con el pasar del tiempo; una situación que aumenta la proliferación de moscas y ratas. Además, el mal manejo que ha tenido ese relleno ha ocasionado que el río Tunjuelo esté expuesto a gases tóxicos que contaminan las aguas del afluente.]

[Ante dicha represión los pobladores se encuentran atemorizados por la presencia de los militares. Óscar Barón asegura que lo que busca es distrito es generar una crisis el basurero para expandirlo Sin embargo señala que “este es un paro pacífico en el sur, no vamos a permitir que nos provoquen, porque nuestra discusión es política y no militar”.]

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
