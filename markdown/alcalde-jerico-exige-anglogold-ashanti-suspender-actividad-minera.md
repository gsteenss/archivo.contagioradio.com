Title: Alcalde de Jericó exige a AngloGold Ashanti suspender actividad minera
Date: 2019-01-28 15:19
Author: AdminContagio
Category: Ambiente, Comunidad
Tags: acuerdo municipal, Anglogold Ashanti, Jericó, Mineria, Quebradona
Slug: alcalde-jerico-exige-anglogold-ashanti-suspender-actividad-minera
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/01/Captura-de-pantalla-2-770x400.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Suministrada] 

###### [28 ene 2019] 

[Jorge Pérez Hernández, alcalde de Jericó, Antioquia, firmó un acto para exigirle a la multinacional AngloGold Ashanti (AGA)  **suspender inmediatamente toda actividad de explotación de metales en el municipio**, en cumplimiento de la prohibición de explotación minera, acordado por el Concejo municipal a finales del año pasado.]

El alcalde tomó esta medida tras dos semanas de protestas por 40 campesinos de Jericó y Támesis, que tuvieron lugar en el corregimiento Palocabildo, de la vereda Cauca, donde reclamaban a la multinacional por continuar con actividades de perforación del proyecto la Quebradona, pese a que el Concejo prohibió la minería a través de un acuerdo firmado el 20 de noviembre de 2018, el cual [empezó a el 10 de diciembre.]

La semana pasada, los campesinos denunciaron ante las autoridades ambientales que las actividades de perforación habían ocasionado la sequía del caudal de una quebrada. El 24 de enero, iniciaron un plantón de 48 horas en las plataformas "[exigiendo el apagado de los taladros de perforación y la presencia de la autoridad municipal para que se cumpliera el Acuerdo Municipal." ](Le puede interesar: "[Jericó, Antioquia le dice ¡No! a la minería por segunda ocasión](https://archivo.contagioradio.com/jerico-antioquia-le-dice-no-a-la-mineria-por-segunda-ocasion/)")

Ante estas denuncias, la Alcaldía se reunió con[ representantes de los campesinos, concejales, miembros de la Mesa Ambiental de Jericó, la Personera Municipal, el abogado Rodrigo Negrete, el geólogo Julio Fierro, el exministro de minas Jorge Eduardo Cock y tres ejecutivos de Minera Quebradona, filial de AngloGold Ashanti en Jericó.]

Según José Fernando Jaramillo, coordinador de la Mesa Ambiental de Jericó, **la multinacional manifestó que no iban a suspender actividades de exploración tras no reconocer la validez del acuerdo municipal y que, por el contrario, iban pedir la judicialización penal de los campesinos y miembros de la Mesa Ambiental.**

### **Un acta sin precedentes** 

Tras la reunión, el alcalde accedió a las protestas de los campesinos y presentó un "Acta de imposición de medida preventiva de suspensión de actividades mineras en jurisdicción del municipio de Jericó." **Este documento se sustenta en los artículos 80 y 315 de la Constitución Política que declara que los municipios pueden decidir sobre las vocaciones productivas de sus territorios, y que el alcalde tiene la autoridad para hacer cumplir dichos acuerdos.**

Según Jaramillo, **este acta no tiene precedentes** debido a que en el país ningún alcalde municipal ha tomado esta medida para suspender la actividad minera de una empresa. Jaramillo afirmó que las organizaciones ambientales estarán tomando medidas legales para garantizar el cumplimiento de este acto; sin embargo, si AGA decide pasar por alto de esta ordenanza, estarán dispuestas de acudir a la Fuerza Pública para despojar la empresa del territorio.

<iframe id="audio_31886230" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_31886230_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
