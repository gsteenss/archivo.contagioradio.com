Title: Chile o Cuba podrían ser las próximas sedes del proceso de paz ELN-Gobierno
Date: 2018-04-19 10:53
Category: Paz, Política
Tags: ELN, Lennin Moreno, Mesa de conversaciones en Quito
Slug: chile-o-cuba-podrian-ser-las-proximas-sedes-del-proceso-de-paz-eln-gobierno
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/02/eln_0-e1517506384192.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: EFE] 

###### [19 Abr 2018]

El anuncio del presidente de Ecuador, Lennin Moreno, sobre no ser más garante del proceso de paz entre la guerrilla del ELN y el gobierno de Colombia, provocó que la incertidumbre volviera a rondar estos diálogos de paz. Para Marylen Serna, vocera de Congreso de los Pueblos, este hecho no puede significar ni levantar la mes**a, ni ejercer más presión sobre las partes**, pero si dar respuesta pronta sobre qué país garante recibiría la mesa.

Serna afirmó que de los países garantes, **en este momento los más opcionado para ser la sede del proceso son Chile o Cuba**, debido a su actual contexto político, mientras que Venezuela y las diferentes crisis que tiene el país, podrían generar más obstáculos para el desarrollo de la misma.

Frente a los tiempos y el poco tiempo que queda para las elecciones presidenciales 2018, Serna manifestó que hay una preocupación por la inmediata continuidad de los diálogos,  **"la mesa iba en un ritmo bueno y esta situación tendrá consecuencias importantes".** Asimismo, señaló que ya se estaban retomando los espacios de la Mesa Social en las regiones, para continuar con la construcción del primer punto sobre participación socia.

Las Comunidades Constriyendo Paz en los Territorios, CONPAZ, propusieron que se realice en Colombia y que sean estas comunidades garantes del proceso y alguna de ellas, la sede para desarrollar los diálogos. (Le puede interesar:["En abril podría iniciar el cese bilateral entre el ELN y el gobierno"](https://archivo.contagioradio.com/en-abril-podria-iniciar-cese-bilateral-entre-eln-y-gobierno-nacional/))

**El ELN y el conflicto en el Catatumbo**

El día de ayer las organizaciones sociales y defensoras de derechos humanos denunciaron la violación a los derechos humanos de las comunidades del Catatumbo, por el conflicto armado entre el EPL y el ELN, así mismo, habitantes de los resguardos humanitarios afirmaron que integrantes del ELN, **estarían instalado minas antipersonas al rededor de estos lugares**. (Le puede interesar:["Gobierno se niega a reconocer conflicto armado en el Catatumbo: Organizaciones sociales"](https://archivo.contagioradio.com/gobierno-se-niega-a-reconocer-conflicto-armado-en-el-catatumbo-organizaciones-sociales/))

Para Marylen Serna, este tipo de acciones no contribuyen a la construcción de paz en Colombia, razón por la cual manifestó que hay organizaciones que ya se han propuesto como garantes en un dialogo entre estas dos insurgencias, que planteé una salida a esta situación.

<iframe id="audio_25575769" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_25575769_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
