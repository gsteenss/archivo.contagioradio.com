Title: El último bosque semi-seco de Colombia está bajo amenaza minera
Date: 2015-09-22 17:20
Category: Ambiente, Nacional
Tags: Amenazas a líderes de Ciudad Bolívar, ANLA, CAR, Contaminación ambiental por cuenta de la minería, Destrucción de fuentes de agua en Ciudad Bolívar, Impactos de la actividade minera en Ciudad Bolívar, Mesa Ambiental de Ciudad Bolívar, Minería en Ciudad Bolívar, No le saque la piedra a la montaña, Paramilitarismo en Ciudad Bolívar
Slug: el-ultimo-bosque-semi-seco-de-colombia-bajo-amenaza-minera
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/Mesa-No-le-saque-la-piedra.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Mesa Ambiental] 

<iframe src="http://www.ivoox.com/player_ek_8552080_2_1.html?data=mZqilJWcdI6ZmKiak52Jd6KnkZKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRic2fhqigh6elsNXdztSYxNTXtdbZjNjSz86Rt8bX0JDRx5Cns83jzsfWw5DGpcvjjMbax9PFvsKfzs6ah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Julián Arana, No le saque la piedra a la montañ]a 

###### [22 Sept 2015] 

[Tras los** impactos nocivos en la salud, el aire y las fuentes hídricas, producidos por la extracción de arcilla, arena y gravilla en las partes altas de Ciudad Bolívar**, siete colectivos de jóvenes se articularon desde hace dos años en la Mesa Ambiental **“No le saque la piedra a la montaña”,** para frenar la actividad minera en esta localidad.]

[El **90% de las extracciones de material para construcción** se da en la cuenca media del río Tunjuelo, en el último reducto del bosque semiseco en Colombia, por cuenta de las **empresas: Canteras Unidas, La Esmeralda y Cantera Cerro Colorado**, que con la aprobación de licencias ambientales desde hace 20 años obtienen indiscriminadamente arena, arcilla y piedra sin cumplir con los planes de manejo ambiental y por fuera de los parques mineros aprobados.]

[Julián Arana, integrante de la Mesa, asegura que tras el cierre preventivo de la Cantera La Esmeralda ordenado por la CAR, producto de las denuncias y movilizaciones adelatandas en mayo de este año, han recibido amenazas a través de cartas enviadas por empleados de esta empresa, que entre otras, **pertenece a la familia Forero Fetecua, vinculada con la actividad esmeraldera de la familia Carranza en Boyacá.**]

[Pese a las denuncias y exigencias de garantías de seguridad, **quienes integran la Mesa han sido exhortados por la Personería para que suspendan su movilización,** pues según ellos no están en condiciones de brindarles tales garantías, agrega Arana.  ]

[Sin embargo estos jóvenes persisten en su exigencia a las autoridades de orden local, distrital y nacional para la suspensión de licencias mineras en toda Ciudad Bolívar, **para poder implementar proyectos de redefinición de usos del suelo urbano-rural de la localidad**, en pro de las condiciones dignas de vivienda y salud que han sido laceradas por cuenta de la extracción y transporte de material para construcción por parte de estas canteras.]

[Desde "No le saque la piedra a la montaña" se extiende la invitación a participar del foro, "Desafíos del movimiento social frente a la paz territorial y la cuestión minera en Colombia", que se realizará el próximo jueves 24 de septiembre en el Auditorio Camilo Torres de la Universidad Nacional desde las 2:00 pm. ]

[![Foro](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/Foro.jpg){.aligncenter .size-full .wp-image-14444 width="742" height="960"}](https://archivo.contagioradio.com/el-ultimo-bosque-semi-seco-de-colombia-bajo-amenaza-minera/foro/)

[ ]
