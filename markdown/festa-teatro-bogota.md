Title: FESTA 2018, una fiesta escénica en homenaje al teatro colombiano
Date: 2018-03-20 11:42
Category: Cultura
Tags: Bogotá, FESTA, teatro
Slug: festa-teatro-bogota
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/03/IMAGEN-2018-FESTA-RGB-e1521563883518.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: FESTA 

###### 20 Mar 2018 

Con un homenaje al teatro colombiano este miércoles 21 de marzo inicia una versión del Festival de Teatro Alternativo FESTA 2018, que durante 12 días se tomará con música, performance, danza, clown, teatro de improvisación, 30 escenarios de la ciudad en más de 130 funciones.

Como reza en el slogan del evento, este año se  rendirá un especial reconocimiento a la trayectoria de compañías como la Esquina Latina de Cali, Ditirambo de Bogotá, Vendimia Teatro de Bogotá, Oficina Central de los Sueños de Medellín y a personajes como Carolina Rueda, narradora oral, fundamentales para el teatro nacional.

Serán 46 las compañías distritales, 24 compañías nacionales, y 17 Compañías internacionales y países invitados especiales como México, Estados Unidos, Ecuador, Francia, Venezuela, Argentina y Bolivia, que hasta el 1 de abril se presentarán en el evento que tiene lugar cada dos años desde 1994. (Le puede interesar: [Del clown al drama](https://archivo.contagioradio.com/del-clown-al-drama/))

La Fiesta escénica tendrá su apertura con las obras “Si el río hablara”, “Soldados”, “La voz humana”, La metamorfosis o el extraño caso del señor G” y “Cruzada para un loco”, las salas de La Candelaria, SekiSano, Sala Vargas Teajada, Teatro Acto Latino y Teatro de Garaje.

La programación incluye además una agenda académica compuesta por exposiciones, talleres y encuentros, para invitar a la reflexión, la investigación y la capacitación en torno a las artes escénicas y su papel fundamental en el fortalecimiento y re construcción de valores para vivir en un nuevo país.

Con ocasión del Festival, la Sala SekiSano de la Corporación Colombiana de Teatro ubicada en la calle 12 No 2 – 65 , se re inaugura durante esta nueva versión, con un nuevo aspecto más acogedor y condiciones técnicas más apropiadas para quienes se presentan. Allí tendrá lugar la apertura del evento desde las 4:30 de la tarde.

**Boletería:**  
General: 15 Mil pesos,  
Estudiantes y Adulto Mayor: 10 Mil pesos  
Abonos: 6 obras por 40 Mil pesos

   [FESTA: Festival de teatro alternativo](https://www.scribd.com/document/374744436/FESTA-Festival-de-teatro-alternativo#from_embed "View FESTA: Festival de teatro alternativo on Scribd") by [Contagioradio](https://www.scribd.com/user/276652802/Contagioradio#from_embed "View Contagioradio's profile on Scribd") on Scribd

<iframe id="doc_35436" class="scribd_iframe_embed" title="FESTA: Festival de teatro alternativo" src="https://www.scribd.com/embeds/374744436/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-7Xqtm0qFz34flpu76QXf&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="1.4363057324840764"></iframe>
