Title: El llamado de los pueblos indígenas en su día
Date: 2019-08-09 18:49
Author: CtgAdm
Category: Comunidad, Nacional
Tags: Ambiente, pueblos indígenas, territorio, vida
Slug: llamado-pueblos-indigenas-dia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/08/dia-internacional-de-los-pueblos-indigenas.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio  
] 

Este 9 de agosto se conmemora el Día Internacional de los Pueblos Indígenas, en Colombia hay 102 pueblos indígenas, de los cuales 70 están en grave riesgo de exterminio físico y cultural. Para hablar sobre los derechos ganados, y los que faltan por garantizar, Contagio Radio entrevistó a **Aida Quilcue**, consejera de derechos humanos de la Organización Nacional Indígena de Colombia (ONIC). (Le puede interesar: ["66% de los pueblos ancestrales está a punto de desaparecer: ONIC"](https://archivo.contagioradio.com/pueblos-indigenas-a-punto-de-desaparecer-onic/))

Quilcue aseguró que los pueblos indígenas recuerdan esta fecha en el marco reivindicativo de derechos, pero también con tristeza porque "el genocidio de los pueblos se acelera con mucha fuerza" mientras continúan en los territorios resistiendo a la guerra. Ella reconoció que se ha avanzado en la garantía de derechos, pero reclamó que muchos de esos avances han sido gracias a las movilizaciones y reclamos que el mismo movimiento indígena ha generado.

La integrante de la ONIC señaló que lo que más preocupa a los pueblos indígenas es que la paz no llegó a los territorios, y lo que más desestabiliza el desarrollo de los planes de vida es la violencia. En ese sentido, dijo que era vital volver a buscar un equilibrio con "la madre tierra", que permitiera poner condiciones al llamado desarrollo económico. (Le puede interesar: ["Son defensores ambientales no enemigos del Estado"](https://archivo.contagioradio.com/defensores-ambientales-o-enemigos-del-estado/))

### **Gobierno, desarrollo económico y protección del ambiente**

Quilcue sostuvo que los pueblos indígenas han sido defensores del territorio, porque mientras **"el mundo occidental habla de calentamiento global, nosotros hablamos del desequilibrio de la madre tierra"**. Por lo tanto, cuestionan el desarrollo económico, que tras el proceso de paz ha buscado hacerse presente en las regiones con el ingreso de empresas interesadas en la extracción de hidrocarburos o la puesta en marcha de grandes proyectos agroindustriales.

Con relación al estado de los pueblos ancestrales con la llegada de Iván Duque a la presidencia, Quilcue declaró que para ellos ha significado una regresividad en derechos; ello se observa en la minga del suroccidente que tuvo que paralizar esa zona del país para exigir el cumplimiento de acuerdos pactados, y tras la que "se triplicaron las muertes en Cauca y Chocó". Ello se suma a las numerosas situaciones de amenazas, confinamientos y desplazamientos de las que son víctimas distintos pueblos indígenas que afectan el desarrollo de una paz territorial.

Para finalizar, la líder indígena manifestó que el llamado en este día es a que **"desde las distintas iniciativas de resistencia que tienen los pueblos que nos encontramos en la defensa de la vida"**. (Le puede interesar: ["Los Amorúa, el pueblo indígena que sobrevive entre las basuras"](https://archivo.contagioradio.com/pueblo-amorua-el-pueblo-indigena-que-sobrevive-entre-las-basuras/))

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe><iframe id="audio_39776425" frameborder="0" allowfullscreen scrolling="no" height="200" style="border:1px solid #EEE; box-sizing:border-box; width:100%;" src="https://co.ivoox.com/es/player_ej_39776425_4_1.html?c1=ff6600"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
