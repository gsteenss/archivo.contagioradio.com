Title: Asesinatos contra líderes sociales se incrementaron un 85%: MOE
Date: 2020-09-22 19:47
Author: AdminContagio
Category: Actualidad, Nacional
Tags: Asesinato de líderes comunales, líderes políticos, Líderes Sociales Asesinados, Misión de Observación Electoral
Slug: asesinatos-contra-lideres-sociales-se-incrementaron-un-85-moe
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/08/WhatsApp-Image-2018-08-06-at-3.26.09-PM.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: Contagio Radio

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Según la [Misión de Observación Electoral (MOE)](https://moe.org.co/), 248 actos de violencia se han registrado contra líderes políticos, sociales y comunales en el primer semestre de 2020, con una reducción del 2% con respecto al mismo periodo en 2019. La Misión señala que, a pesar de haber un reducción, las cifras siguen siendo preocupantes, pues muestran que la violencia no se ha reducido significativamente. (Le puede interesar: [Asesinan a joven de 16 años y a comunera indígena en Cauca y Nariño](https://archivo.contagioradio.com/asesinan-a-joven-de-16-anos-y-a-comunera-indigena-en-cauca-y-narino/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

57 de los 248 hechos registrados corresponden a agresiones contra lideresas y se asocia con una violencia estructural de género. Según Alejandra Barrios, Directora de la MOE: **«las agresiones a las lideresas son particularmente preocupantes, pues se convierten en una medida de disuasión a participar en los espacios de toma de decisiones y gobierno».**

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/moecolombia/status/1308390598173286400","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/moecolombia/status/1308390598173286400

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

En relación a los tipos de liderazgos, los líderes sociales registraron el 50% de estos hechos siendo los que más vulneraciones han sufrido. **En estos casos, la letalidad de las agresiones es lo más preocupante pues el número de asesinatos se incrementó en un 85%.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

**Por otra parte, los líderes políticos fueron el segundo tipo de liderazgo más afectado con 89 casos registrados.** Hasta junio, fueron agredidos 8 alcaldes, 42 concejales, 3 ediles y 2 diputados. La Misión advierte que con esto se busca «controlar no solo el actuar social, sino de los gobiernos locales a partir del sometimiento de sus líderes».

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En adición, 35 hechos de violencia fueron contra líderes comunales, un 13% más que en 2019. De esta cifra, 19 fueron asesinatos, 2 atentados, 3 secuestros, 1 desaparición y 10 amenazas, es decir 60% de las agresiones fueron letales.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Finalmente, la violencia contra líderes afro e indígenas, representan el 13% de los hechos; de este porcentaje, **el 58% de los casos contra líderes afro fueron letales, y en el caso de los líderes indígenas, el 70%.**

<!-- /wp:paragraph -->

<!-- wp:heading -->

En 109 municipios los liderazgos están siendo víctimas de violencia
-------------------------------------------------------------------

<!-- /wp:heading -->

<!-- wp:paragraph -->

De acuerdo con el informe presentado por la MOE, 52 asesinatos se perpetraron en territorios PDET (Planes de Desarrollo con Enfoque Territorial), «lo que significa que 64,2% de los asesinatos se dieron en zonas priorizadas por el Estado para evitar los focos generadores de la violencia».

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

**A esto se suma que en 109 municipios los liderazgos están siendo víctimas de violencia y en 67% de ellos hay presencia de al menos un grupo armado ilegal.** Sin embargo, el informe agrega que esto no constituye la única causa de la violencia, por lo que es importante el reconocimiento de **«**otros actores e intereses detrás de la violencia**»**.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Los departamentos más afectados son Cauca, Arauca, Norte de Santander, Antioquia y Córdoba.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Para Barrios **«**lo que este informe nos muestra es que lo que está pasando es una tragedia en términos locales; cada día son mayores los ataques a los liderazgos que trabajan por transformar la realidad de sus comunidades. Si no tomamos medidas urgentes, nos vamos a quedar sin líderes**»**.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Ante esto, la MOE hace un llamado para que se repiensen los mecanismos utilizados para la protección de los líderes, encontrando alternativas más efectivas.

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
