Title: Family members ask IACHR to review murder case of Elsa Alvarado and Mario Calderón
Date: 2019-05-29 17:35
Author: CtgAdm
Category: English
Tags: Elsa Alvarado and Mario Calderón, Inter-American Commission on Human Rights
Slug: family-members-ask-iachr-to-review-murder-case-of-elsa-alvarado-and-mario-calderon
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/Elsa-Alvarado.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

Friends, family and colleagues of Mario Calderón, Elsa Alvarado and her father Carlos gathered on May 19 to honor the memory of the human rights defenders, assassinated by paramilitaries in 1997, and to call on the Interamerican Comision on Human Rights (IACHR) to declare the Colombian state responsable for their murder.

Sergio Ocazionez, lawyer of the Colombian Commission of Jurists, claimed that the criminal investigation has led to only sentence in this case against one of the hitmen who took part in the homicide. The others were assassinated, a practice that the lawyer said is common among paramilitary groups.

Despite these setbacks, there have been advances. A few years, alias Don Berna testified that state agents, in particular the retired coronel Plazas Acevedo, whose case has been accepted by the Special Peace Jurisdiction, were involved in the logistics of the crime. Ocazionez added that a verdict in this case will depend on the truth that the retired coronel contributes in the transitional justice system.

### **The role of the IACHR** 

According to the lawyer, the legal representatives of this case expect the IACHR to uphold the victims' rights and to repair some of the damage suffered by the relatives of the victims. Although he warned that this process within the IACHR may be delayed, they expect that the accumulation of cases, such as that of the humorist Jaime Garzón, may allow for progress to be made.
