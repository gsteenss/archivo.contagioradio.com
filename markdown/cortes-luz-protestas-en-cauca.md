Title: Cortes de luz para frenar cultivo de Marihuana provocan protestas en Cauca
Date: 2019-07-31 16:01
Author: CtgAdm
Category: Nacional
Tags: Cauca, coca, Cultivos de uso ilícito, Marihuana
Slug: cortes-luz-protestas-en-cauca
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/Protesta-en-Miranda.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Sucesos Cauca  
] 

Hasta ayer comunidades de Miranda, Cauca, ubicadas en la vía que conduce a Corinto, mantuvieron una protesta por los cortes de luz que hace cerca de 15 días iniciaron en el Municipio con el objetivo de controlar los cultivos de marihuana. Sin embargo, tras una reunión con las autoridades, representantes de Compañía Energética de Occidente (CEO) y las comunidades se decidió levantar la protesta, así como suspender los cortes de luz. (Le puede interesar: ["La 'brillante' idea del Gobierno: Cortar la luz para reducir los cultivos de Marihuana"](https://archivo.contagioradio.com/cortar-la-luz-cultivos-marihuana/))

### **En contexto: Los cortes de energía no sirven, terminan en protesta mientras la empresa solo quiere que se pague la luz** 

En los primeros días de julio, Contagio Radio se comunicó con Leider Valencia, **vocero nacional de la Coordinadora Nacional de Cultivadores de Coca, Amapola y marihuana (COCCAM)** para hablar sobre la problemática generada por los cortes de luz. En su momento, Valencia explicó que el Gobierno buscaba quitar el servicio a los cultivos, porque las plantas que allí se siembran son modificadas genéticamente y necesitan de la luz en sus primeros 20 días para crecer.

Sin embargo, dijo que la medida no serviría porque quienes tienen cultivos pueden trabajar con plantas de energía a gasolina; en su lugar, sentenció que **afectaría a otros habitantes que no tienen nada que ver con los cultivos en los cuatro municipios donde se harían los cortes ( Miranda, Toribío, Caloto y Corinto).** Adicionalmente, habitantes que habían estado en las reuniones de socialización de la medida dijeron que CEO únicamente estaba interesada en que se pague por el servicio pues consideran que las conexiones son ilegales.

### **En Miranda se cumplió el pronóstico** 

Según relató Luis Elmer Fernández, integrante de la Asociación Campesina de Miranda, luego de 15 días de cortes de energía e intermitencias en el servicio, comunidades campesinas e indígenas de las 22 veredas del Municipio iniciaron un bloqueo sobre la vía que conduce a Toribío, en la vereda Guatemala, para pedir que el restablecimiento del mismo. El pasado domingo, los manifestantes intervinieron las líneas de alta tensión, lo que provocó que Miranda se quedara sin luz durante casi 24 horas.

Luego de enfrentamientos con el Escuadrón Móvil Antidisturbios (ESMAD), el martes las comunidades se reunieron con la Empresa, la Defensoría del Pueblo y la Alcaldía de Miranda para levantar para reanudar el servicio de energía. En el encuentro, las comunidades expresaron que **en Miranda no hay cultivos de Marihuana**, por lo que los cortes estaban generando problemas para vendedores de lácteos y carnes, así como para los niños que deben hacer sus tareas para la escuela en la tarde y noche.

Adicionalmente, señalaron que **las intermitencias están dañando electrodomésticos y vulnerando los derechos de las comunidades**. Ante los reclamos, la empresa de energía dijo que se comprometía a hacer revisión de los tendidos eléctricos, pues como explicó Fernández, "en algunas partes los postes son de madera y guadua"; para cumplir con ello, CEO pidió que se se pagara por el servicio, porque haY personas usando el servicio de forma ilegal, y solicitó el acompañamiento de la Fuerza Pública para garantizar la seguridad de sus trabajadores.

### **¿Por qué no funcionan los cortes de luz, y la gente seguirá plantando Marihuana?** 

Pese a que en Miranda no hay cultivos, en otros municipios del norte de Cauca sí; según Fernández, esto se debe a que las personas en el campo no tienen otra forma de vivir, y los cultivos de Marihuana o Coca se convierten en cultivos de subsistencia. "No hay otra manera (...) **si uno lleva una cebolla o un plátano al mercado eso no tiene gran valor, no da para subsistir,** para la educación de los hijos, la salud...", explicó el Integrante de la Asociación de Campesinos.

En la búsqueda de alternativas, Fernández aseguró que se debía invertir en el campo e impulsar una verdadera reforma agraria que estimule programas integrales para el campo. Además, **que se apoye el Programa Nacional Integral de Sustitución (PNIS),** cumpliendose lo pactado en el Acuerdo de Paz. (Le puede interesar: ["Mitigación del crimen y la violencia en las áreas de cultivo de Coca"](https://archivo.contagioradio.com/mitigacion-del-crimen-y-la-violencia-en-las-areas-de-cultivo-de-coca/))

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe><iframe id="audio_39329241" frameborder="0" allowfullscreen scrolling="no" height="200" style="border:1px solid #EEE; box-sizing:border-box; width:100%;" src="https://co.ivoox.com/es/player_ej_39329241_4_1.html?c1=ff6600"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
