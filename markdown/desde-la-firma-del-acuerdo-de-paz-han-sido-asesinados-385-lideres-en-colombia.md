Title: Desde la firma del Acuerdo de paz han sido asesinados 385 líderes en Colombia
Date: 2018-05-18 16:40
Category: DDHH, Nacional
Tags: acuerdos de paz, Cumbre Agriria, defensores de derechos humanos, INDEPAZ, lideres sociales, marcha patriotica
Slug: desde-la-firma-del-acuerdo-de-paz-han-sido-asesinados-385-lideres-en-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/10/vigilia-por-la-paz.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [18 May 2018] 

La organización política Marcha Ptriotica, Indepaz, La Cumbre Agraria Étnica y Popular y la Fundación Henrich Boll Stiftung, presentaron el informe “Todos los nombres, Todos los rostros”, un documento que reveló que desde la firma del Acuerdo de Paz hasta la fecha actual, **han sido asesinados 385 líderes sociales y defensores de derechos humanos, de los cuales 161 pertenecían a Marcha Patriótica y 63 excombatientes**.

### **No cesan las agresiones contra el Movimiento Social** 

El informe afirma que, si bien hay un contexto que está cambiando frente a las agresiones a la población civil, producto de la firma del Acuerdo de Paz y el inicio de los diálogos de paz entre la guerrilla del ELN y el gobierno, hay un aumento en la violencia sociopolítica del país que se ha visto reflejada en el asesinato a líderes sociales, defensores de derechos humanos y ex combatientes del nuevo partido FARC.

De igual forma, el control territorial de nuevos grupos armados, estructuras paramilitares y de las guerrillas del ELN y el EPL ha provocado enfrentamientos y desplazamientos al interior del país. Sumado a ello están las diversas alertas que tanto **la Defensoría del Pueblo, como organizaciones defensoras de derechos humanos**, han enviado al Estado para poner freno a esta situación. (Le puede interesar: ["No hay paz para las personas defensoras de derechos humanos: OMCT"](https://archivo.contagioradio.com/no-hay-paz-para-las-personas-defensoras-de-derechos-humanos-omct/))

De acuerdo con la organización Marcha Patriótica, los factores de riesgo a los que están expuestos sus integrantes en primera medida tienen que ver con la ubicación geográfica, esto porque de acuerdo con la Defensoría del Pueblo, de los **33 departamentos del país, 32 están en situación de alerta, mientras que en 324 municipios de los 1.122 que tiene Colombia, se encuentra aún mucho más focalizado el riesgo, es decir en el 28,7%** del territorio del país.

### **¿Quiénes son los líderes asesinados?** 

Las poblaciones que más se encuentran en peligro son las campesinas, indígenas, afrodecendientes, comunidades que realicen procesos de sustitución de cultivos de uso ilícitos, líderes comunales, **líderes ambientales y líderes de restitución de tierras. En ese sentido, la mayoría de ellos se oponía a proyectos minero energéticos.**

Además, denunciaron que ha existido una retoma de la doctrina militar por parte de la Fuerza Pública que ha **originado nuevos casos de ejecuciones extrajudiciales en departamentos como Arauca**, o de detenciones ilegales conocidas como falsos positivos judiciales, a partir de capturas masivas a líderes sociales.

En el caso de las líderesas, **de los 385 personas asesinadas 56 eran mujeres**. En el año 2016 fueron asesinadas 18, en el año 2017 fueron asesinadas 26 y en lo que va corrido de este año 12 líderesas y defensoras fueron asesinadas.

Según la recolección de datos de Marcha Patriótica 116 personas fueron asesinadas en el año 2016, 191 líderes en el año 2017 y en lo corrido del año 2018 son 78 las personas asesinadas. De igual forma, desde la firma del Acuerdo de Paz hasta el momento fueron asesinados **63 excombatientes de la FARC y 17 familiares, mientras que 6 militantes se encuentran desaparecidos**.

### **¿En dónde ocurren los asesinatos?** 

El informe señaló que en el 61% del territorio se han presentado actos violentos contra líderes sociales y defensores de derechos humanos, mientras que el **87% de los lugares en donde sucedieron estaban altamente militarizados**. Adicionalmente, son territorios en donde hay interés tanto legales como ilegales por la extracción de hidrocarburos y minerales o por la siembra de monocultivos y ganadería extensiva.

Frente al lugar en donde se comete el asesinato o la acción violenta, el informe evidenció que la mayoría de ellos ocurren en la vivienda del líder social o en cercanías a la misma. El departamento con mayor número de asesinatos a líderes y defensores de derechos humanos es el Cauca, seguido por Antioquia, Nariño, Chocó, Córdoba, Arauca y Norte de Santander.

La gran mayoría de los homicidios, en un **57% fueron cometidos por sicarios, con arma de fuego de largo o corto alcance**. Otra de las modalidades de asesinato ha sido secuestrar al defensor o líder, para luego torturarlo y que su cadáver sea encontrado por la comunidad.

### **Los responsables de los asesinatos a líderes sociales y defensores** 

Frente a quiénes están detrás de estos hechos, el informe manifestó que si bien es cierto que en la gran mayoría de los asesinatos se desconocen los autores intelectuales, se ha establecido que los principales responsables de los mismos son estructuras paramilitares, seguidos de la Fuerza Pública y las guerrillas del ELN y el EPL. En ese mismo sentido se estableció que previo a los asesinatos existieron amenazas a los líderes.

El desplazamiento forzado también ha aumentado en los territorios, el informe expresó que **3.700 personas se han movilizado en Colombia hasta el mes de abril producto de la violencia**. De igual forma el confinamiento en las regiones ha empezado a darse con mayor frecuencia, sobretodo en el Catatumbo y el Litoral Bajo San Juan.

### **Las acciones institucionales** 

De acuerdo con el informe, si bien es cierto que con los Acuerdos de paz se crearon nuevas instituciones que propenden por la búsqueda de la verdad, la justicia y la reparación a las víctimas del conflicto armado. **Los esfuerzos por parte de las instituciones para salvaguardar las vidas de líderes y defensores, aún son insuficientes**. (Le puede interesar: ["¿Impunidad para los terceros civiles que participaron en el conflicto armado?"](https://archivo.contagioradio.com/impunidad-para-los-terceros-civiles-que-participaron-en-la-guerra/))

En primera medida, la erradicación forzada que se está llevando a cabo contra los cultivos de uso ilícito de campesinos que se han acogido a los planes de sustitución, están generando conflictos en los territorios. Ejemplo de ello es el asesinato de 8 campesinos el año pasado en Tumaco, a manos del Ejército.

###### Reciba toda la información de Contagio Radio en [[su correo]
