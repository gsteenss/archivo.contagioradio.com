Title: Si no es de otro modo, que se hundan las 16 curules de víctimas
Date: 2017-12-07 13:27
Category: Camilo, Opinion
Tags: asesinatos contra líderes, Curvarado, lideres sociales
Slug: 49799-2
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/12/LIDERES-SOCIALES-JYP.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Comisión Justicia y paz 

#### Por [Camilo De Las Casas](https://archivo.contagioradio.com/camilo-de-las-casas/) 

###### 4 Dic 2017 

Empezamos el año con el asesinato de Emilsen Manyoma (15 Ene 2017) y cerca de terminar éste 2017 fue Mario Castaño (26 Nov 2017), ojalá sea el último asesinado. En este insepulto cadáver de país con balas para hombres y mujeres que buscan el equilibrio  crearnos en libertad es un movimiento de la Vida.

Es imposible la vida sin la muerte. Es así. Lo que se muestra como una aparente contradicción es lo que se intenta domar con la paz integral. Tanta muerte injusta no ha dado a luz a un nuevo país. El campo y los barrios siguen siendo un valle de vidas silenciadas y vidas silenciosas de libertad, muchas de ellas alegremente neoesclavizadas. Son atisbos y fugacidad la firma del Acuerdo del Colón y la Mesa de Quito, aún distantes de las mayorías nacionales.

Estamos ante un permanente  depredador con muchos rostros que acecha a su objeto para ser depradado, él continúa  sin ser puesto al descubierto y ponerlo en un límite. En la cultura terrateniente  premoderna está instalado  el amor infinito a la muerte violenta. Adoraron a los paramilitares mientras les servían. Amantes ciegos de la ambición acaparadora ejercen el poder a nombre de la democracia,  de las buenas costumbres, de dios cristiano o católico, y del progreso con negocios 'legales' y con el tráfico de drogas. La tierra ha sido su becerro de oro. Becerro trastocado hoy por lo que se encuentra en su interior, en su subsuelo. Ya no es la superficie todo lo que vale, es lo de debajo. Esos depredadores son corporativos en la guerra y en la Pax Neoliberal.Así  aunque millones de los llamados de  abajo y de los sectores crean en tan falsa verdad del progreso excluyente.

Los depredadores ordenan el gatillo a la autoridad y se benefician del gatillero. Y estas, las "autoridades"  a su vez, llaman a los mercenarios de muerte que ofician a los sicarios de abajo para la ejecución.Así fue en el Valle y hace unos días en el Chocó con Mario.

Las víctimas todas dejaron de ser el centro del Acuerdo del Teatro Colón, tal vez, nunca lo fueron. El uso retórico del Presidente es una realidad desde antes del proceso de diálogo con las FARC EP. Igual es la actitud de la mayoría de los partidos, imposibilitando o bloqueando las Circunscripciones Especial de Paz para las víctimas.

La ley de 1448 o de víctimas es parte de una guerra económica preventiva de tipo fiscal muy bien publicitada evitando indemnizaciones a las víctimas conforme a los estándares internacionales. Esa ley disfrazada de bondad fue imposible de modificar en lo sustancial por el Acuerdo hacia la Paz, salvo el reconocimiento de los derechos de los exiliados.

¡Y qué decir de la mal llamada restitución de tierras!. Entramado para dar  legalidad de la propiedad a los despojados de la tierra facultando para que privados  puedan hacer "buenos negocios", sin que medie restitución material del predio.

Los centenares de líderes muertos violentos son una realidad. Crimenes de sistema o no, pero una realidad. Los lideres son asesinados por alguna razón similar, un modelo de Estado y de sociedad inmodificable. La operación encubierta está ahí asegurando la impunidad de los que están detrás  asegurando  negocios o su apuestas políticas y el negacionismo del fondo de este drama de asesinatos

Esos rostros ocultos, los de atrás, se sustrajeron de la JEP, acordaron con los políticos y cuentan con imagólogos en los medios masivos empresariales para hacerles ver a los corporativos multinacionales cómo nacionales, los que son claramente criminales, cómo los del progreso.

Estas muertes violentas nos podrían levantar del letargo institucional en que creemos que la realidad se configura con leyes. Las infestadas trampas de la ley para asegurar el perverso ejercicio de la democracia deliberativa es superable.

Estas muertes las de las balas y las de las leyes,  se nos revela como posibilidad, en el breve tiempo de la historia que nos ha sido concedido. A la muerte violenta se vive como cuerpos materiales hechos átomos y en la memoria  emancipadora o paralizante. O sobrevivientes organizados desde la ética de la Vida transformando su mirada saneada sobre lo politico y la economía, la tierra y el territorio  o rehaciendo el victimismo sin duelos concluidos, mendicantes de dignidad sin transformación.

La muerte violenta irrumpe y fragmenta el ciclo del amor por la Vida y desmorona, otra vez,  el inexistente Estado de Derecho. Interrumpido el movimiento de la Vida en los asesinados y al mismo tiempo en los responsables directos y planificadores.

 ¿En dónde están los ocho millones de víctimas? ¿Sobreviviendo de las sobras o Indiferentes?

¿Qué es abrazar la Vida, cuándo la muerte violenta arrasa el cuerpo físico de ya centenares, en este momento de la historia donde un grupo de los alzados en armas dejaron la violencia?

Emilse o Mario, y los centenares de líderes asesinados yacen  en el territorio de la muerte. Ellos pueden ser despedidos en su dignidad en el corazón palpitante de lo nuevo. Ellos asumidos en la extensión de la Vida serían parte de otra historia transformante ante la revictimización autocompasiva, ante el culto al miedo y al aislamiento fragmentador. O la repetición victimizante de un amargo, comprensible, repetorio relato de dolores insanos o de dolores hecho concepto datos. Transformar la catástrofe de esas muertes en fidelidad original a la Vida, la fuerza de la fuerza es lo nuevo.

La Vida es máz que ese derecho  punitivo carcelaria, más que la privatización territorial. Si no es hoy, quizás mañana han de descubrir ellos o sus generaciones el desequilibrio causado. Lo nuestro es la Vida siendo Bello Existir, todo está abierto para ellos se acerquen, para que los victimarios y sus beneficiarios, reconozcan el movimiento interrumpido de la Vida, para rearmar la Vida. La Vida que está más allá de esa instituida injusticia judicial, social y ambiental.

A nosotros nos corresponde el acto creativo de la Vida ante la propia muerte, amar lo segado y reconocer a los segadores liberando lo que no es nuestro, es de ellos, su impunidad, su poder esquizofrénico.

Y para empezar, si las 16 circunscripciones de paz se aprobaran, la Vida sería que esas curules fueran en honor a los millones de víctimas con listas consensuadas sobre la base de la Ética de la Vida mas que sobre el poder politiquero.

Así que si las víctimas van a repetir lo mismo es mejor que se hundan las Circunscripciones Especiales de Paz,  la mejor honra a nuestra historia.

#### [Leer más columnas de opinión de  Camilo de las Casas](https://archivo.contagioradio.com/camilo-de-las-casas/)

------------------------------------------------------------------------

######  Las opiniones expresadas en este artículo son de exclusiva responsabilidad del autor y no necesariamente representan la opinión de Contagio Radio.
