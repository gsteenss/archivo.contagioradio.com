Title: "El Comandante": propaganda sucia y manipulación masiva
Date: 2017-01-27 09:21
Category: Nacional, yoreporto
Tags: Canal RCN, Hugo Chavez, Venezuela
Slug: el-comandante-propaganda-sucia-y-manipulacion-masiva
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/01/el-comandante-hugo-chavez.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: RCN - popticular 

#### [Por: Agencia de Comunicaciones Prensa Alternativa Cauca (ACPA Cauca) - Yo Reporto] 

27 Ene 2017

El próximo lunes 30 de enero, RCN estrena en Colombia la serie “**El Comandante”, supuesta ficción ‘inspirada’ en la vida de Hugo Chávez**; lo hace un día antes que TNT y otros medios masivos de la región. Partiendo de que no existe ni pelo de ingenuidad o inocencia en sus creadores, ni casualidades en lo que respecta al momento histórico en que se realiza, cómo se hace y sus intenciones, nos proponemos desde el análisis a profundidad descubrir hacia dónde apunta y por qué, este nuevo producto de consumo masivo que intentará meternos por las narices y con la guardia baja, la versión de un Chávez corrupto, adicto al poder, machista e individualista.

### [El contexto] 

El modelo neoliberal se encuentra en permanente y creciente crisis desde hace casi 10 años. Sus máximos defensores, que encabezan el eje Miami-Bogotá-Madrid, al no encontrar solución por medios democráticos, se encuentran en clara ofensiva promoviendo los nacionalismos extremistas, que implican posiciones cuasi fascistas en lo que corresponde a integración regional, migración, solidaridad entre países, convenios económicos, etc. Frente a ellos, mantenemos en pie la esperanza de construir modelos de país y de sociedades distintos, que prioricen lo humano por sobre lo metálico, que reivindiquen los derechos sociales históricamente negados y den verdadero poder, independencia y soberanía a los pueblos nuestroamericanos.

En este sentido, esta oligarquía global, imperialista y capitalista a ultranza, se encuentra en plena campaña sucia para deslegitimar y tumbar en lo posible, estas alternativas populares, que encuentran uno de sus faros de luz en el Proceso Bolivariano que se vive en la Venezuela Chavista. Basta mirar, leer o escuchar algo de lo que dicen los medios masivos propiedad de estos señores, para darse cuenta de sus intenciones: destruir todo lo que huela a pueblo.

A su vez, la reintegración a la vida política legal de las FARC-EP mediante la implementación de los acuerdos de La Habana, supone una inyección de capacidad política y visión estratégica necesaria para hacer fuerza a esos poderes oscuros, quienes desde siempre han apuntado sus cañones contra todo lo alternativo, revolucionario y bolivariano por el miedo que les infunde la perspectiva de perder sus privilegios, riquezas y poder, en manos del pueblo al que históricamente pisotearon, aun por los medios masivos como la televisión.

Hugo Chávez significa para el continente y el mundo una creciente referencia de liderazgo popular genuino, sin hipocresía, demagogia ni cinismo; un verdadero hijo del pueblo que encarnó sus necesidades históricas y que, junto al movimiento popular venezolano, de larga trayectoria de lucha, supieron construir una alternativa real de poder que ilumine, al igual que Cuba, Bolivia, Nicaragua, las esperanzas de quienes somos concientes de la necesidad de transformar las realidades de nuestros pueblos.

Por esta batalla ideológica es que “los dueños del mundo” deciden dar un paso más allá en su ofensiva mediática y cultural contra nosotras y nosotros, imponiendo su versión de la historia de Chávez, para neutralizar en las mentes del común su legado y el proyecto humanista que encarna.

### [El discurso] 

Como la serie "El Comandante" relata la historia de Hugo Chávez, obviamente el principal objetivo es ensuciar su memoria; pero a la vez, también intenta posicionar a sus seguidores, sean militares o pueblo civil, como gentes ignorantes, sumisas y manipuladas por su genialidad (que muchas veces se les reconoce a los peores seres de la historia en el marco de su supuesta psicosis social). De esta manera, se invisibiliza el movimiento popular creciente en Venezuela desde hace décadas, que explotó con “el Caracazo”, del que el mismo creador de la serie fue directo responsable, como ya veremos más adelante. Aclarado esto, nos enfocaremos en el tratamiento de la imagen del Comandante.

Al ver los distintos videos de promoción de la serie, las entrevistas con sus realizadores y las distintas notas que han ido publicando los medios alineados, se identifica cómo va a ser la narrativa, cómo se va a ir construyendo el relato en torno al personaje. En este sentido, el eslogan de la serie reza “El poder de la pasión y la pasión por el poder”, lo que establece un rasgo que sería esencial en el personaje, esa supuesta adicción al poder, como también se demuestra en las promociones, que sostienen que “el poder y el dinero son como las drogas, uno siempre quiere más” y que “soñaba con un mundo nuevo y el poder lo consumió”.

Venden la serie como “una historia de violencia, sexo, ambición y dinero”. Podría ser la descripción de una novela sobre algún parapolítico senador expresidente, pero no lo es; lo que demuestra el nivel de hipocresía que se maneja. Buscan mostrar a Chávez como un populista nato, un demagogo sin escrúpulos, adicto al poder y mujeriego. Es que la serie es una mercancía y con tal de venderla a nivel masivo, no escatiman en recursos. Aseguran que se trata de “la vida secreta de Hugo Chávez”. De hecho, una promoción se centra en 3 mujeres que supuestamente estuvieron con él, haciendo énfasis de esta manera en una vida amorosa cargada de infidelidades y machismo. “Hugo Chávez se enamoró de muchas mujeres, pero al final resultó prefiriendo el amor por el poder”, aseguran sus creadores.

Vale mencionar la aparición, intuimos de cierta importancia en el desarrollo de la serie (la campaña mediática contra el castrochavismo nos da la pista), de Fidel Castro, quien en una promoción aparece diciendo “con Chávez vamos a dar una sorpresita”, en tono cínico y malicioso. Sin dudas, se va a golpear también la imagen del recientemente fallecido líder, al que ya le preparan su propia serie, los mismos creadores, como también veremos más adelante.

### [El creador] 

Es Moisés Naim, venezolano, ministro de los gobiernos neoliberales previos a Chávez. Fue quien impuso el “paquete económico” del Fondo Monetario Internacional y el Banco Mundial al pueblo venezolano en febrero de 1989, siendo ministro de Fomento, Industria y Comercio del segundo gobierno de Carlos Andrés Pérez, lo que desencadenó el “Caracazo”, hecho por el cual murieron más de 3,500 personas, teniendo en cuenta la aplicación del “Plan Avila”, diseñado por asesores norteamericanos e israelitas para restaurar el orden con el uso de armas de guerra. Esta situación desembocó en la rebelión militar de 1992, dirigida por el entonces Coronel Hugo Chávez.

Hay una línea casi directa en el mapa de relaciones entre Naím y la CIA; en toda su carrera hizo parte de medios y empresas financiadas por la maquinaria de la Agencia y del Congreso de los Estados Unidos, todos mecanismos de intervención extranjera. Su prédica como “periodista”, que la derecha llama “análisis”, se inserta a la perfección en las líneas centrales de la Agencia en su estrategia de injerencia e influencia a nivel global y en particular, en los países latinoamericanos. Además, fue “halcón” de Bush durante la “guerra contra el terrorismo”, lo es del Banco Mundial, de la Fundación Rockefeller y del grupo Prisa, dueño de más de 1,200 emisoras, periódicos y canales de televisión en América latina que se dedican día a día a destilar odio contra los procesos y dirigentes revolucionarios. \[1\]

Basta con mirar su perfil de Twitter (@MoisesNaim), predominantemente en inglés, donde comparte solo información respectiva a los Estados Unidos y Europa, es decir, a los intereses de los grandes capitales globales. Su cuenta evidencia su desinterés real por Latinoamérica, promoviendo la visión del continente como un patio trasero, subdesarrollado y de sumisión total al neocolonialismo.

Su programa de televisión, “El efecto Naím”, lo transmiten CNN, NTN4 y Globovisión; donde interpreta a total fidelidad los designios y líneas políticas de la elite global. Por esto mismo, y como no podía ser de otra manera, ha sido también premiado con cualquier cantidad de títulos, todos correspondientes a la tiranía global. Todos los medios y organizaciones para los que trabajó y trabaja, siempre se refirieron a Chávez como un dictador, etc., etc. Dedicó los últimos 20 años de su vida a sostener y promover la campaña sucia y manipulativa, basada en mentiras, contra Chávez y el proyecto político que encarnó y que aun continua vigente.

Resultan hasta graciosas sus declaraciones sobre la serie: “Venía investigando la vida del líder de la revolución bolivariana desde hace una década y tuve la idea original. (…) Motivado, me senté y comencé a escribir una historia de 120 páginas. Sony la compró y a partir de ese primer manuscrito se desarrollaron los libretos y los personajes.” ¡Todo fue como por casualidad! Qué bien se le da la vida a esta gente.

Además, se encuentra produciendo una serie “biográfica” sobre Fidel Castro, del mismo tono, profundizando en esta clara ofensiva cultural y mediática contra la memoria de los líderes populares más destacados de nuestra historia reciente. \[2\]

### [La productora] 

Sony Pictures Television es una de las más grandes empresas de cine y televisión en el mundo, haciendo parte importante de la maquinaria de propaganda ideológica del capital internacional. Como directos responsables legales de la serie, se escudan en que “es una serie de ficción, inspirada en Hugo Chávez”, lo que reiteran una y otra vez. Pura formalidad, nadie lo va a considerar así. En este sentido, aseguran que “Es difícil que nosotros logremos, y tampoco pretendemos, cambiar la visión de alguien. Quienes lo admiran lo seguirán haciendo y quienes lo critican, también. Lo que nosotros estamos haciendo es dramatizar una parte de la vida de un líder icónico”, como explica Juan Felipe Cano, uno de los dos directores (el otro es Henry Rivero). Jugar a la ingenuidad fue una orientación clara a todos los implicados.

Se realizaron 60 capítulos, los cuales ya están todos grabados. Además de en Colombia, transmitirán la serie  en Estados Unidos, por Telemundo; en Argentina, por Telefé; en México, por Blim; y a todos los países de la región, por el canal pago TNT, principal difusor de la serie.

### [El actor] 

“En la calle me gritan Pablo”, cuenta el elegido para encarnar al Comandante, Andrés Parra. Es que pocos lo conocen por su nombre o por su larga trayectoria; para la gran mayoría de la gente, es Pablo Escobar, de la serie El Patrón del Mal. Esto no es casualidad, se trata de jugar con el imaginario de la audiencia, que asociará inevitablemente a los dos personajes. Se reconoce que Escobar fue uno de los personajes más nefastos de la historia de Colombia; para los señores detrás de esta serie, Hugo Chávez lo fue en Venezuela. De esta manera, instalan la idea en la mente de la gente, que en muchos casos considera que lo que diga la televisión, tiene que ser verdad. A su vez, al elegir al actor más popular y con más prestigio de Colombia, buscan dar mayor legitimidad a la serie y a su relato del personaje.

Para legitimar esta elección del actor, diversos medios han venido entrevistándolo y elogiando su supuesto parecido con Chávez, que quienes realmente lo hayan visto y oído, se darán cuenta que no es para nada así. Eso pasa cuando el personaje a representar es tan especialmente particular y al actor le exigen hacer una caricatura de él, no una representación fidedigna. Para justificarse, Parra aseguró que es difícil representar a alguien que es mejor actor que uno, sosteniendo la supuesta interpretación de un personaje por parte de Chávez, negando la genuina expresividad y personalidad del líder.

El actor asegura que para conocer a Chávez hubo un acompañamiento por parte de Sony con charlas con periodistas e historiadores venezolanos que le “empapaban sobre el fenómeno de Venezuela, sobre la guerra de la independencia, el culto a Bolívar… como para que entendiéramos dónde nos estábamos parando”; debemos suponer de qué talante serían esos sabios de la realidad venezolana y la visión de Chávez que le metieron en la cabeza a Andrés Parra. \[3\]

### [Los medios] 

Como decíamos más arriba, el principal difusor en la región será el canal pago TNT, quien va a llevar la serie a todos los rincones del continente. Este es uno de los principales canales de películas de la televisión por cable, que hace parte importante de la maquinaria de propaganda capitalista, televisando, por ejemplo, series de criminología, películas de guerra o la gala de Miss Universo, todos productos donde se estereotipan sectores sociales, pueblos y mujeres, respectivamente.

En Colombia, se transmitirá por RCN, medio perteneciente a Ardila Lulle, dueño además gran parte del mercado de las bebidas en el país y del fútbol nacional. Su patrimonio, para el 2016, era de 5 mil millones de dólares‚ posicionándose entre los hombres más ricos del mundo. Este medio, junto con Caracol, son los que más alcance e influencia tienen en la opinión pública nacional. Evidentes enemigos de la paz (hicieron y hacen campaña activa en contra del proceso de paz entre el gobierno colombiano y las FARC-EP) y de las alternativas políticas, estigmatizan la lucha social y sus organizaciones, e invisibilzan sistemáticamente las violaciones a los derechos humanos (asesinatos de dirigentes sociales y defensores de derechos humanos incluidos) que cometen el paramilitarismo y el mismo Estado colombiano.

Para RCN, “El Comandante” será la principal serie del año, irá en horario central y la campaña de expectativa ha ido en aumento desde hace unos meses. Es un medio que no tiene ninguna vocación de paz, que no promueve el diálogo ni la reconciliación; es decir, la democracia.

### [Nuestras conclusiones] 

La ecuación creador+productora+actor+medios, da como resultado un combo ultraconservador, de sesgo fascista, que buscará imponer una mirada obtusa y tendenciosa de una realidad que quieren ensuciar e invisibilizar, los logros de la Revolución Bolivariana, la alternativa política real que implica el chavismo y la memoria histórica de Hugo Chávez. Buscarán manipular las mentes de quienes no tienen una visión propia de quien fue Chávez, del proceso bolivariano en Venezuela y el Socialismo del Siglo para el continente y el mundo.

Al mostrar un personaje autoritario, déspota, machista e individualista, desvirtúan el carácter social y la entrega por amor de este líder a su pueblo. Expondrán al Comandante Chávez al escarnio mundial y continuarán en su campaña de posicionar el proceso bolivariano venezolano como referencia de lo que “no queremos ser”, un imaginario construido por los mismos medios de comunicación y sus voceros políticos, temerosos, como indicábamos más arriba, de que se haga justicia contra sus privilegios y riquezas.

Sin dudas que esta producción puede ser considerada como injerencia directa en la historia y el presente de Venezuela, en el sentir de un pueblo, en una corriente de unidad en el continente, en un sistema alternativo de democracia y poder popular, frente al claudicante modelo neoliberal, represor e injusto. En Venezuela, ya se expresó el rechazo a la serie; habrá que esperar si el gobierno y la familia de Chávez inician acciones legales contra la productora o contra quien consideren. También resta por ver la reacción del movimiento popular nuestroamericano, del chavismo internacional y venezolano, y de todas y todos los que trabajan todos los días por construir un continente de paz, con memoria y justicia social.

Fuentes:

###### \[1\] <http://misionverdad.com/la-guerra-en-venezuela/perfil-moises-naim-un-fascista-globalizado%20> 

###### \[2\] <http://www.naimmedia.com/en/fiction/> 

###### \[3\] <http://www.semana.com/cultura/articulo/entrevista-a-andres-parra-sobre-su-personaje-hugo-chavez/508724> 

###### acpacauca.wordpress.com 

###### caucacomunica@gmail.com 

###### Fb: /CaucAlternativa 

------------------------------------------------------------------------

###### Las opiniones expresadas en este artículo son de exclusiva responsabilidad del autor y no necesariamente representan la opinión de la Contagio Radio. 
