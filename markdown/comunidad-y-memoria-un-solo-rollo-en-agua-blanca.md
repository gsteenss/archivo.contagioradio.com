Title: Comunidad y memoria, un solo rollo en Agua Blanca.
Date: 2015-07-14 17:14
Author: AdminContagio
Category: 24 Cuadros, Cultura
Tags: @FESDAcine, Festival de cine y video comunitario del Distrito de Agua Blanca, Proimágenes Colombia
Slug: comunidad-y-memoria-un-solo-rollo-en-agua-blanca
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/07/fesda5.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

######  

###### 14, Julio, 2015 

Hasta el próximo 30 de Julio, estará abierta la convocatoria para la séptima edición de "FESDA" Festival Nacional de Cine y video Comunitario del Distrito de Agua Blanca en la ciudad de Cali, Valle del Cauca, un espacio para incentivar y fortalecer los procesos de creación audiovisual con y desde las comunidades en Colombia.

La cita audiovisual, que tendrá lugar entre el 6 y el 10 de octubre, tiene como lema "Comunidad y Memoria, un solo rollo", en palabras de Daniela Ancona, directora del evento, "es la ocasión para recordarnos a nosotros como festival de cine comunitario, recordar nuestra historia, la historia audiovisual del país y su memoria política que no debemos olvidar".[![fesda 4](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/07/fesda-4-e1436910994937.jpg){.aligncenter .wp-image-11246 .size-full width="1669" height="599"}](https://archivo.contagioradio.com/comunidad-y-memoria-un-solo-rollo-en-agua-blanca/fesda-4/)

También resulta ser la oportunidad para aportar en la formación de audiencias en torno a la recepción de contenidos y estéticas del audiovisual producido para y por las comunidades, "no es un festival para un publico masivo, hemos tenido una gran acogida en la convocatoria durante todos estos años al ser una oportunidad para las personas que estan emergiendo en el audiovisual" afirma la directora.

Las y los interesados en participar con sus producciones, deberán tener en cuenta algunos requisitos fundamentales: ser trabajos realizados en Colombia, con fecha posterior al 1ro de enero de 2013 y no haber sido publicados completamente en la web en las categorías Documental comunitario, ficción comunitaria, videoclip, animación y video experimental y video profesional o universitario con comunidades.

La selección de las obras estará a cargo de un equipo de curadores con amplia trayectoría y reconocimiento en el medio, y los reconocimientos serán escogidos por el público asistente a las jornadas de exhibición que se adelantarán en casas de la cultura, el centro de la ciudad y algunos espacios públicos del Distrito de Agua Blanca, alternadas con la programación académica del Festival.

<iframe src="https://www.youtube.com/embed/XxxMTGEHquE" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

En cuanto a la proyección del evento "A largo plazo queremos convertirnos en una plataforma donde la gente pueda aprender las formas de produccion, sin dejar de se ellos quienes se cuenten a si mismos", apoyando en el presente a los nuevos realizadores a través del laboratorio de proyectos, donde se premiara a los participantes con alquiler de quipos, y asesoria en sus proyectos, ayudarles a "visibilizar su realidad sin intermediarios" afirma Ancona.

Las condiciones para el envío de las piezas audiovisuales, las plataformas on line para compartirlas se encuentran en el sitio [www.fesdacine.com](http://www.fesdacine.com/) y en el correo festi.videocomunitario@gmail.com. Quienes deseen enviar sus trabajos en físico lo pueden hacer por correo certificado a la Biblioteca de la ciudad de Cali.
