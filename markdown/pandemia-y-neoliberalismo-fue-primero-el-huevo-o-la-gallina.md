Title: Pandemia y neoliberalismo: ¿Fue primero el huevo o la gallina?
Date: 2020-04-22 15:30
Author: AdminContagio
Category: Columnistas invitados, Opinion
Tags: Covid-19, crisis de la salud, crisis económica, Crisis Sanitaria, neoliberalismo, pandemia
Slug: pandemia-y-neoliberalismo-fue-primero-el-huevo-o-la-gallina
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/04/1584630096_576462_1584630520_noticia_normal.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:heading {"align":"right","level":6,"textColor":"cyan-bluish-gray"} -->

###### Foto por: El país {#foto-por-el-país .has-cyan-bluish-gray-color .has-text-color .has-text-align-right}

<!-- /wp:heading -->

<!-- wp:heading {"level":6} -->

###### Por: Laura pinzón Capote

<!-- /wp:heading -->

<!-- wp:paragraph -->

La emergencia de la pandemia del **COVID-19** en el mundo ha despertado una serie de interrogantes sobre la capacidad de los Estados de gestionar la crisis económica y sanitaria que atravesamos en la actualidad.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Numerosas imágenes de hospitales a reventar en lo que los medios de comunicación llaman “el primer mundo” o “los países desarrollados” nos hace preguntarnos inevitablemente, si en esos lugares que en América Latina y el Caribe y las clases dominantes suelen tomar como ejemplo de sociedades, la gestión de la pandemia ha sido tan devastadora en términos de vidas humanas, ¿qué le espera los países de nuestro continente, donde las políticas neoliberales de desfinanciación y reducción del Estado se encuentran enraizadas hace más de treinta años?

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Esta pregunta vale la pena tenerla presente a la hora de dar una lectura de la situación del continente en este contexto, donde la diversidad se manifiesta hasta en las formas radicalmente distintas de gestión de gobierno y de proyectos políticos en los diferentes países de la región. Algunos, la mayoría, de fuerte tradición neoliberal e histórica presencia norteamericana, se han encontrado con el problema del tratamiento de la pandemia con pocas herramientas:

<!-- /wp:paragraph -->

<!-- wp:heading {"level":5} -->

##### ¿Cómo va a ser posible una buena gestión en el marco de una pandemia en países donde ha habido una progresiva reducción del Estado, desvinculando su intervención en la estructura de lo público, donde no hay seguridad social, y se ha entregado la gestión de la salud al sector privado que tiene en condiciones de precarización a los y las profesionales de la salud, que además ha desfinanciado la investigación científica, y tampoco ha invertido en la educación pública para permitir una adecuada continuación virtual de las agendas académicas en un contexto de distanciamiento social?

<!-- /wp:heading -->

<!-- wp:paragraph -->

Muestra de ello son las grandes masas de la población que dependen de la economía informal para sobrevivir, ya que no pueden acceder a ningún trabajo con prestaciones legales producto de la flexibilización laboral, que en el medio de una necesidad de un aislamiento social, se ven obligadas a no cumplirlo y salir a las calles a continuar consiguiendo su sustento diario, no por capricho individual, sino por materiales razones de supervivencia.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

De esto conocemos en países como Chile, Ecuador o especialmente Colombia, (ni hablar de los EE.UU), donde la población ampliamente precarizada se ve criminalizada por los masivos medios de comunicación y reprimida por los Estados por decidir salir a conseguir alimentos, mientras la gestión estatal decide complacer con beneficios económicos a las grandes empresas para que no sufran ‘pérdidas’ a perjuicio de los y las trabajadoras, sin pensar medidas que resuelvan la urgencia de la alimentación y los gastos básicos que y permitan a las clases populares cumplir con las medidas de sanidad públicas.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Contrasta con esta fotografía neoliberal la gestión de algunos gobiernos de la región que avanzando por otros frentes obtuvieron distintos resultados del tratamiento del COVID-19.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Argentina, a pesar de haber salido de cuatro años de administración neoliberal y desarticulación del aparato público del Estado,  es uno de los países de la región que según las cifras de contagiados y relación de muertes por cantidad de población, mejor han realizado el tratamiento de la pandemia. Por un lado, por las medidas que tomó en materia de distanciamiento social obligatorio el gobierno nacional, pero por otro lado, por el trabajo que las diferentes organizaciones sociales han hecho para evitar que los enormes sectores populares del país que se ven afectados por la imposibilidad de salir a trabajar en este contexto, y que se desempeñan en la economía informal, puedan acceder a la alimentación básica.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

La articulación de los sectores más empobrecidos a través de comedores  
populares es la principal garantía para que miles de personas puedan cumplir,  
en la medida de lo posible, el distanciamiento social necesario. A ello se  
suman también las decisiones del Ejecutivo de garantizar un Ingreso Familiar de  
Emergencia a las personas más afectadas económicamente por el aislamiento,  
además de decretar el congelamiento de alquileres, la prohibición de desalojos  
y de despidos en el contexto de la cuarentena obligatoria.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Hacer una mirada regional nos permite pensar en que el COVID-19 llegó a potenciar una crisis preexistente:

<!-- /wp:paragraph -->

<!-- wp:heading {"level":5} -->

##### La crisis de los proyectos neoliberales en la región que se enfrentan hoy con las consecuencias de sus decisiones en perjuicio de las clases populares.

<!-- /wp:heading -->

<!-- wp:paragraph -->

Conocer los mecanismos que han tenido otros países para evitar una mayor propagación de la pandemia nos permite exigir que nuestros gobiernos pongan como prioridad a la población más golpeada por la crisis social, política y económica, en vez de tomar como eje central el rescate de la economía a costas de la vida de miles de personas como estamos viendo a diario en nuestros países donde el capital parece estar antes que la vida.

<!-- /wp:paragraph -->
