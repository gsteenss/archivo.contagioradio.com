Title: RCN pretende “enlodar” el nombre de Jaime Garzón
Date: 2016-09-15 14:32
Category: DDHH, Nacional
Tags: Asesinato Jaime Garzón, Jaime Garzon
Slug: rcn-pretende-enlodar-el-nombre-de-jaime-garzon
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/09/marisol-garzon-y-jaime-garzon.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [15 de Sept de 2016] 

Durante la rueda de prensa realizada por el Colectivo de Abogados José Alvear Restrepo, Marisol Garzón, hermana de Jaime Garzón, **índico que el canal RCN enloda el nombre del periodista y humorista**, al intentar llevar su vida a la ficción a partir de una novela que planean producir.

Mientras se refería a la revictimización que ha sufrido su hermano, la forma de recordarlo y de reparar a los familiares, Marisol Garzón afirmó que **“lo que están haciendo es enlodar el nombre de Jaime, como lo quiere hacer RCN con una novela porque así no se hacen homenajes, a una persona como Jaime que le entrego la vida a este país, no se le hace un homenaje con una novela de ese estilo”.**

De igual forma, Garzón expresó que ella **no firmó la autorización para que el canal de televisión realice esta producción**, a diferencia de sus hermanos y que no quiere tener nada que ver con RCN o dar declaraciones para este medio.

“Por qué no le hacen una novela al señor Ardila Lule, ya que está vivo, háganle un homenaje a ese señor que lleva tantos años haciendo tantas cosas por este país, no estoy diciendo si buenas o malas” aseveró Marisol al aclarar que considera que esta no es la forma de reivindicar la memoria de su hermano.

De otro lado, en la rueda de prensa, la hermana del humorista **agradeció al Colectivo de Abogados José Alvear Restrepo por estar al lado de su familia durante los 17 años** de su asesinato y por la labor que han ejercido desde que asumieron el caso, asimismo agradeció al Consejo de Estado por pronunciarse, ya que esto significaría que la Fiscalía podría [declarar el asesinato del humorista y periodista como crimen de Lesa Humanidad](https://archivo.contagioradio.com/el-asesinato-de-jaime-garzon-es-un-crimen-de-lesa-humanidad/).

En el falló, el Consejo de Estado determinó que los [organismos de seguridad estatales realizaron actividades de inteligencia](https://archivo.contagioradio.com/consejo-de-estado-condeno-a-la-nacion-por-el-crimen-de-jaime-garzon/)y compartieron la información recolectada con paramilitares miembros de las AUC, a su vez estableció que hay pruebas suficientes que precisan la responsabilidad del ex subdirector del DAS, José Miguel Narvaéz y del ex comandante del B-2, brigada XIII del Ejercito, el coronel Jorge Eliécer Plazas Acevedo.

Sin embargo, al caso se han vinculado más coroneles y generales como lo son el General Mauricio Santoyo, ex jefe de seguridad de Álvaro Uribe Vélez, quien actualmente se encuentra condenado a 13 años de cárcel por vínculos con el grupo narcoparamilitar la Oficina de Envigado. [Santoyo habría sido la persona que secuestró a los dos sicarios que organizaron el asesinato de Garzón](https://archivo.contagioradio.com/generales-mauricio-santoyo-y-rito-alejo-del-rio-son-vinculados-a-crimen-de-jaime-garzon/).

De igual forma se encuentra vinculado el General Rito Alejo del Río, quien comandaba la Brigada XIII de Bogotá y está condenado a 25 años de cárcel por el crimen de asesinato a Marino López. Rito Alejo[habría sido una de las personas que planeó el asesinato del humorista](https://archivo.contagioradio.com/los-generales-y-militares-involucrados-en-asesinato-de-jaime-garzon/) por solicitud del jefe paramilitar Carlos Castaño. 17 años después, no se ha condenado a ninguna persona por ser el autor intelectual del crimen.
