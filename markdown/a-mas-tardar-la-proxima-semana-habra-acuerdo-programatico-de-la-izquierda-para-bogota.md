Title: A más tardar, la próxima semana habra acuerdo programático de la izquierda para Bogotá
Date: 2019-07-12 11:32
Author: CtgAdm
Category: Entrevistas, Política
Tags: “clima ambicioso”, Alcaldía de Bogotá, candidatos, Claudia López
Slug: a-mas-tardar-la-proxima-semana-habra-acuerdo-programatico-de-la-izquierda-para-bogota
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/Fotos-editadas-2.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio  
] 

Según el precandidato a la alcaldía por la Colombia Humana, **Jorge Rojas, cinco precandidatos reafirmaron esta semana su deseo de alcanzar un acuerdo programático de Gobierno para Bogotá y una sola candidatura,** que les permita ser opción de poder en la Ciudad. Rojas adelantó que estos días han trabajado por ese programa, y esperan poder presentarlo antes del martes de la próxima semana.

### **Antes de presentar una candidatura, tienen que presentar un programa de gobierno**

Pese a que la coalición se ha anunciado en diferentes ocasiones, y los cinco precandidatos que la componen se han referido a la misma reiteradamente, la duda sobre su viabilidad surgió luego que Claudia López y Gustavo Petro protagonizaron una discusión a través de su cuenta de twitter. Además de lo dicho por la Candidata del Partido Verde, llamó la atención su opinión de apoyo al metro elevado de Bogotá, que ha sido un objeto fuerte discusión para sectores de oposición, incluso desde el Concejo de Bogotá.

> En esto lleva estancada Bogotá 8 años!....PEñalosa le dice a PEtro populista carretudo, PEtro acusa sin fundamento a PEñalosa de paramilitar...cada uno cree que Bogotá empezó y se acabó con su gobierno, ninguno reconoce nada del otro, ni continúa algo.  
> No mas! <https://t.co/ugI759eQSo>
>
> — Claudia López (@ClaudiaLopez) [10 de julio de 2019](https://twitter.com/ClaudiaLopez/status/1148768080451051521?ref_src=twsrc%5Etfw)

<p>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
</p>
No obstante, **Rojas afirmó que ayer se reunieron López, Celio Nieves y Luis Gómez para reafirmar la idea de presentar una candidatura conjunta**; pero antes que eso, acordaron que debían presentar un acuerdo programático de gobierno que le de el mensaje a la ciudadad "que vamos  volver a ganar la Alcaldía de Bogotá". "En eso estamos trabajando con mucha responsabilidad, porque se resume en una visión de ciudad distinta a la que plantea Peñalosa", señaló Rojas.

### **Los pilares del programa de Gobierno**

El primer pilar del acuerdo programático, y la manzana de la discordia, es el metro; para Rojas, **el metro subterráneo y la red férrea** "como nuevo eje estructurador de la movilidad en Bogotá" debe ser una realidad. Sobre este tema, el precandidato por la Colombia Humana dijo que ya había acuerdo en que "el mejor metro para Bogotá es subterráneo", pero hay diferencias sobre qué puede pasar si el alcalde Peñalosa deja licitado el metro elevado.

El segundo pilar es **la defensa de lo público,** otro de los puntos de oposición que ha sido fuerte en el Concejo de Bogotá, pues la actual Administración intentó privatizar la Empresa de Telecomunicaciones de Bogotá (ETB) así como devolvió el servicio de recolección de basuras a manos de privados. En ese sentido, el programa alternativo de Gobierno tendría la visión de lo público como un ejercicio de protección de derechos.

Otros puntos comunes serían la protección de los Cerros Orientales, y de la Reserva Thomas Van der Hammen; la defensa de la democracia, la implementación del Acuerdo de Paz y la lucha contra la corrupción. Así las cosas, los precandidatos presentarían su programa faltando poco menos de dos semanas para el cierre de inscripciones para la coalición, que será el 27 de julio. (Le puede interesar: ["Concejales piden medidas cautelares para evitar corrupción en Metro de Peñalosa"](https://archivo.contagioradio.com/evitar-corrupcion-metro-penalosa/))

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>  
<iframe id="audio_38331251" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_38331251_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
