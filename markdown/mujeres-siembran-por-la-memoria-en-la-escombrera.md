Title: Mujeres siembran por la memoria en 'La Escombrera'
Date: 2015-10-09 12:46
Category: Nacional, Sin Olvido
Tags: Comuna 13, Desaparecidos de Medellín, Desapariciones forzadas, La escombrera, Medellin, Mujeres Caminando por la Verdad, operación orion, Radio de derechos humanos, semana por la memoria
Slug: mujeres-siembran-por-la-memoria-en-la-escombrera
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/10/escombrera.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: www.ipc.org.co 

<iframe src="http://www.ivoox.com/player_ek_8890506_2_1.html?data=mZ2mkpqUeo6ZmKiakpqJd6KmmJKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRkdbextfS1ZDXrcbhw9fO0JDUs9OfzcaYz8rRs9PdwpDS0JCJdpjAwpCy1cjTscPmxtfOh5ebcYarpJKw0dPYpcjd0JC%2Fw8nNs4yhhpywj5k%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Luz Helena Galeano, Mujeres Caminando por la Verdad] 

###### [[9 Oct 2015]] 

[En el marco de la [[VIII Semana por la Memoria en Colombia](https://archivo.contagioradio.com/viii-semana-por-la-memoria-en-colombia/) ]se han desarrollado foros, laboratorios artísticos, lanzamientos de proyectos musicales y proyecciones de películas en las que se muestran las experiencias de lucha y resistencia de víctimas de la guerra, como la de “Mujeres Caminando por la Verdad”, una organización de familiares de quienes desaparecieron durante las **11 operaciones militares ocurridas en Medellín entre febrero y diciembre de 2002 y quienes hoy con **]**una siembra recuerdan a sus familiares desaparecidos en La Escombrera.**

[Luz Helena Galeano, integrante de esta organización comenta que durante toda la semana han participado en distintas actividades en la capital antioqueña, entre las que se destaca la puesta en escena de una obra de teatro que ilustra la **Operación Orión**. Operativo llevado a cabo entre el 16 y el 17 de octubre de 2002, por miembros de las FFMM y la Policía Nacional con apoyo de la FAC en la Comuna 13 y que según el Instituto Popular de Capacitación dejó como resultado **1 civil muerto, 38 heridos y ocho más desaparecidos**.]

[En esta misma Comuna las “Mujeres Caminando por la Verdad” se han dado cita, específicamente en La Escombrera para sembrar plantas que llevan el nombre de sus familiares desaparecidos y que pueden ser llevadas a sus casas o dejadas allí pues **“significan la memoria viva de nuestros seres queridos”, como expresa Galeano.**]

[Desde el pasado mes de agosto, cuando iniciaron las excavaciones estas mujeres han realizado un ejercicio de veeduría continua. Pese a que aún no ha habido hallazgos de cuerpos, se estima que en un plazo de 5 meses puede haber resultados. Entre tanto reconocen que **la articulación de organismos estatales, victimas y organizaciones sociales ha sido fundamental en todo este proceso**.]
