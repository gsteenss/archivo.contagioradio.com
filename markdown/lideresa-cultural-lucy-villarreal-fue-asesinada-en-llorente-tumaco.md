Title: Líderesa cultural Lucy Villarreal fue asesinada en Llorente-Tumaco
Date: 2019-12-24 20:33
Author: CtgAdm
Category: DDHH, Nacional
Tags: Asesinato a líderes sociales, lider, Llorente, Lucy, Tumaco, Villarreal
Slug: lideresa-cultural-lucy-villarreal-fue-asesinada-en-llorente-tumaco
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/lucy.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo Particular] 

 Este 24 de diciembre, el Proceso Social de Garantías para la labor de las y los defensores de derechos humanos, denunció el asesinato en Llorente, Tumaco de **Lucy Villarreal, activista que se dedicaba a la promoción de la cultura en su territorio** y la defensa de derechos humanos. Los hechos se habrían registrado una vez la lideresa terminaba de dar un taller a menores de edad.

Villarreal, madre de dos niñas, se desempeñaba como gestora cultural en el municipio. Además era la coordinadora de la exposición "Mujeres sur y vida", dedicada a exaltar el trabajo que realizan las mujeres líderes en el territorio y el trabajo que hacen en torno a la defensa de derechos. Asimismo hacía parte del Colectivo Indoamericano, en la ciudad de Pasto. [(Le puede interesar: En Tumaco opera la justicia de los grupos residuales en medio de la militarización)](https://archivo.contagioradio.com/en-tumaco-opera-la-justicia-de-los-grupos-residuales-en-medio-de-la-militarizacion/)

La comunidad de Llorente hizo un llamado a las autoridades para que se esclarezcan los motivos tras el homicidio de Lucy y se realicen las capturas pertinentes. Con ella son más de 160 líderes asesinados en Colombia en el transcurso del año 2019. [(Lea también: El colegio de Tumaco que se cansó de la muerte: 21 asesinatos de estudiantes en 4 años)](https://archivo.contagioradio.com/el-colegio-de-tumaco-que-se-canso-de-la-muerte-21-asesinatos-de-estudiantes-en-4-anos/)

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
