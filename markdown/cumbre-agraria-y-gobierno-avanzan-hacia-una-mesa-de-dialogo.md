Title: Cumbre Agraria y Gobierno avanzan hacia una mesa de diálogo
Date: 2016-06-08 14:27
Category: Paro Nacional
Tags: Cumbre Agraria, Minga Nacional, Paro Colombia 2016
Slug: cumbre-agraria-y-gobierno-avanzan-hacia-una-mesa-de-dialogo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/06/Cumbre-Agraria-y-Gobierno.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Cumbre Agraria ] 

###### [8 Junio 2016 ] 

De acuerdo con Omar Fernández vocero de la Cumbre Agraria e integrante de la 'Coalición de Movimientos y Organizaciones Sociales de Colombia', **con el Gobierno nacional se está concertando una mesa de diálogo** **con cuatro sitios**, uno de instalación en la vía Panamericana, en el punto conocido como Quinamayó; dos en los que habrán dos subcomisiones y un último, en el que se acordará la refrendación con la presencia de Juan Manuel Santos.

La primera subcomisión estará en el Cauca en La María, Piendamó y se concentrará en la problemática indígena y la segunda, sesionará en Cali para encargarse de discutir los ocho puntos del pliego de exigencias de la Minga Nacional. Sí bien los sectores campesinos, afro e indígenas tienen sus reclamaciones propias, **la Cumbre insiste en que esta negociación es nacional** y reconoce los logros alcanzados por la fuerte movilización indígena.

Según Fernández la demora en la concertación de la mesa se dio por falta de garantías en el tema humanitario, de un lado el Gobierno exigía que, previo al diálogo, se abriera un corredor en la vía Panamericana, "porque parece que no le interesan los otros bloqueos", y por el otro, la Cumbre planteaba la **necesidad de garantías de derechos humanos para la movilización social**.

Sin embargo, la situación de derechos humanos en el marco de la Minga, no ha mejorado, se ha agudizado, por cuenta de la **constante represión de la fuerza pública en los puntos de concentración**, por lo que las comunidades insisten en que hay garantías para el desbloqueo de las vías. Este miércoles líderes y garantes, como el Padre Francisco de Roux, se reúnen para ver cómo pueden destrabarse las negociaciones, y emitieron el siguiente comunicado:

[![Cumbre Agraria](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/06/Cumbre-Agraria.jpg){.aligncenter .size-full .wp-image-25128 width="720" height="1088"}](https://archivo.contagioradio.com/cumbre-agraria-y-gobierno-avanzan-hacia-una-mesa-de-dialogo/cumbre-agraria-4/)

###### [![Comunicado Cumbre Agraria](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/06/Comunicado-Cumbre-Agraria.jpg)

<iframe src="http://co.ivoox.com/es/player_ej_11829576_2_1.html?data=kpallJ6Ze5ehhpywj5WcaZS1lJeah5yncZOhhpywj5WRaZi3jpWah5yncbDhwteYqMrWsoa3lIquk9PIqdufjpCw19LGtsafoszfw9fNpY6ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)]] 
