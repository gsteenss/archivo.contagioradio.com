Title: Consejo de Seguridad de Naciones Unidas preocupada por asesinato de líderes sociales en Colombia
Date: 2019-01-02 13:54
Author: AdminContagio
Category: DDHH, Nacional
Tags: acuerdos de paz, FARC, Iván Duque, ONU, reincorporación
Slug: onu-preocupada-por-el-asesinato-de-lideres-sociales-en-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/08/WhatsApp-Image-2018-08-06-at-3.26.09-PM.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [02 Ene 2019]

En su más reciente informe el Consejo de Seguridad de las Naciones Unidas sobre la Misión de Verificación en Colombia, resaltó que si bien en los primeros 100 días de mandato del presidente Iván Duque, han existido compromisos por la implementación de los Acuerdos de Paz, **persiste la preocupación del organismo por el asesinato de líderes sociales. **

### **Las preocupaciones de la ONU** 

En el informe trimestral el secretario general, Antonio Guterres, reiteró su preocupación "debido a que se siguen produciendo ataques en contra de líderes sociales y defensores de derechos humanos" y citó las cifras del organismo que señalan que desde septiembre de 2018 se han verificado 7 homicidios, mientras que otros 22 están por verificarse; y desde la **firma del Acuerdo de Paz se han verificado 163 asesinatos a líderes sociales y se han informado un total de 456 casos**.

Situación que para el Secretario revela la importancia de la puesta en marcha del "Plan Acción Oportuna (PAO) de Prevención y Protección individual y colectiva de los derechos a la vida, la libertad, la integridad y la seguridad de defensores de derechos humanos y líderes sociales, comunales y periodistas", lanzado el pasado 19 de noviembre de 2018. (Le puede interesar: ["Desplazamiento forzoso en Colombia incrementó un 106%"](https://archivo.contagioradio.com/desplazamiento-forzoso-en-colombia-incremento-un-106-en-2018/))

Asimismo, **el Secretario invitó a que se convoque prontamente la Comisión Nacional de Garantías de Seguridad,** espacio en el que participan altos funcionarios y representantes de las organizaciones de la sociedad civil, que permite la creación de estrategias para "hacer frente a la amenaza de los grupos ilegales y desmantelar la delincuencia organizada.

### **La presencia del Estado en el territorio, el mayor reto** 

De acuerdo con el informe, se ha podido establecer que el mayor reto que afronta el Estado, frente a la implementación de los Acuerdos de Paz, tiene que ver con su presencia en los territorios más afectados por el conflicto armado, en el que persiste la presencia de grupos armados ilegales, y "atender el clamor de las comunidades" por seguridad, educación, salud, tierra, infraestructura y alternativas viables a las economías ilegales.

Frente a esa situación el Secretario celebró la firma y creación del plan "Paz con legalidad" que a partir de instrumentos como los Planes de Desarrollo con Enfoque Territorial, "promete un esfuerzo amplio por estabilizar las zonas más afectadas por el conflicto", incluyendo los compromisos adquiridos con las víctimas, las comunidades involucradas en los planes de sustitución de cultivos de uso ilícitos y los reincorporados. **Sin embargo, subrayaron la urgencia porque estos planes se transformen en medidas eficaces en los territorios. **

### **Cumplir los Acuerdos de Paz ** 

El Secretario también  manifestó su preocupación por el asesinato de integrantes del partido político FARC que realizaron su tránsito a la reincorporación, debido a que en lo corrido del último trimestre del año 2018 **se presentaron 14 homicidios, mientras que desde la firma del Acuerdo de Paz, han asesinado a 85 personas**.

A su vez, el informe señaló que el estado de los 24 Espacios Territoriales de Capacitación y Reincorporación, varia considerablemente, razón por la cual "se espera que las decisiones que se tomen con respecto a ellos y el futuro del estipendio mensual, después del próximo 15 de agosto del 2019, se basen en una comprensión cabal de las condiciones" de los mismos.

No obstante, el informe destaca que las entidades garantes de la implementación de los Acuerdos de Paz, se han venido reuniendo periódicamente, y el último semestre se aceleró la aprobación de proyectos económicos. En total fueron aprobados **20 proyectos colectivos y 29 proyectos individuales**, que en conjunto, están valorados en 3,7 millones de dólares y beneficia a un total de 1.340 excombatientes. (Le puede interesar: ["Las razones para que el Gobierno mantenga las zonas de reincorporación"](https://archivo.contagioradio.com/gobierno-zonas-reincorporacion/))

De igual manera, el informe destaca el hecho de que el pasado 5 de octubre, se hizo una declaración conjunta de la Misión de Verificación de las Naciones Unidas y la Oficina del Alto Comisionado de las Naciones Unidas para los Derechos Humanos en Colombia, que subrayó el papel central de Jurisdicción Especial para la Paz dentro del proceso de paz en el país y la necesidad de que todas las entidades públicas respeten plenamente su independencia y autonomía.

### **Diálogos entre el gobierno y el ELN** 

Finalmente el informe del Consejo de Seguridad de las Naciones Unidas insta a que los canales de comunicación tanto de la guerrilla del ELN como del Gobierno Nacional, continúen abiertos, pese a que sus posturas están mucho más lejanas de lo que han estado desde el inicio de las conversaciones en el 2016.

El organismo igualmente, reiteró su apoyo al gobierno colombiano en la implementación de los Acuerdos de Paz, e "incluso en los esfuerzos del nuevo gobierno para inyectar mayor vigor y viabilidad en la reincorporación socioeconómica de los excombatientes".

######  Reciba toda la información de Contagio Radio en [[su correo]
