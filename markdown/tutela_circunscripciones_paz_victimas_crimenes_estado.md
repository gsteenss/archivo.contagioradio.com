Title: Víctimas de crímenes de Estado interponen tutela a favor de circunscripciones de paz
Date: 2017-12-06 17:32
Category: Nacional, Paz
Tags: Circunscripciones de paz, MOVICE, víctimas de crímenes de Estado
Slug: tutela_circunscripciones_paz_victimas_crimenes_estado
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/12/movice-sin-olvido-e1512597929707.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [ 6 Dic 2017] 

Ante la incertidumbre por lo que será la última palabra frente a las 16 circunscripciones especiales de paz, las víctimas de crímenes de Estado respaldadas por **25 organizaciones sociales han decidido interponer una tutela para exigir al Presidente del Senado** que envíe el proyecto de las Circunscripciones Transitorias Especiales de Paz a sanción presidencial.

"Estas dilaciones están violando nuestros derechos fundamentales a la Paz, a la participación en las decisiones que los afectan y a la participación política de las víctimas, constituyendo una auténtica verdadera denegación de justicia, que sólo el Juez constitucional puede corregir", señalan.

De acuerdo con Rodrigo Ramírez, integrante del Movimiento de Víctimas de Crímenes de Estado, capítulo Sucre, "de una manera maliciosa el Congreso le hizo conejo a lo que son las circunscripciones de paz, cuando en realidad fueron aprobadas porque hubo mayoría absoluta, según las propia Ley que reglamenta el Congreso", en cambio **se desconoce una de las formas de reparación de las víctimas y por tanto el centro del acuerdo de paz.**

Es por eso piden al juez de tutela ordenar la aprobación de las circunscripciones, con el fin de que entren en vigencia oportunamente antes del cierre de las inscripciones de candidatos para la Cámara de Representantes en el período 2018-2022.

### **Montes de María se prepara las elecciones** 

Puntualmente, en el departamento de Sucre, en Montes de María, un lugar gravemente golpeado por el conflicto armado pero que ha sabido reconstruir el tejido social, ve con preocupación la situación de zozobra con las circunscripciones. Según manifiesta Ramírez, para esa región se trata de "**un golpe en el corazón la negación de las circunscripciones, pues se estaría dejando sin voz y sin voto en el congreso a las personas que históricamente han sido marginadas".**

En esa zona, el integrante del MOVICE, expresa que la comunidad ha venido trabajando de manera fuerte y organizada, mediante asambleas y múltiples reuniones parar fomentar  espacios de convergencia en aras de constituir una candidatura única que represente a los intereses de la población de manera que no exista ningún tipo de disputa o rivalidad, y en cambio se trabaje de forma coordinada.

"Deben perder miedo de que la gente se organice. Son las castas políticas las que hoy se oponen esto. Esas mismas son las que han masacrado y las que están condenadas por nexos con el paramilitarismo", concluye Ramírez.

<iframe id="audio_22511167" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_22511167_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)
