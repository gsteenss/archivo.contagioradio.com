Title: #HoyMarchoPorLaPaz: Movilización nacional en apoyo a la JEP
Date: 2019-03-18 13:03
Category: Movilización, Paz
Tags: JEP, marcha, proceso de paz
Slug: hoymarchoporlapaz-movilizacion-jep
Status: published

###### [Foto: Contagio Radio] 

###### [18 Mar 2019] 

Este lunes ciudadanos se han dado cita en diferentes lugares de Colombia y el mundo para **"iluminar" al Congreso**, en busca de evitar que acepte las objeciones presentadas por el presidente **Iván Duque a la Ley Estatutaria de la Jurisdicción Especial para la Paz (JEP)**, y elevar un llamado a respetar este mecanismo, así como a lo pactado en el Acuerdo de Paz.

La Representante a la Cámara Katherine Miranda, asegura que en Bogotá se vivirá una marcha tranquila, que se encontrará en el **Planetario Distrital sobre las 5 de la tarde**, y se desplazará por la Carrera Séptima hasta la Plaza de Bolivar, donde harán un llamado al Congreso, "y sobre todo al Presidente, para que cumpla lo dicho en campaña: **Que no iba a hacer trizas los Acuerdos de Paz**".

El mensaje también será emitido desde capitales grandes y medias del país, así como algunos municipios donde la ciudadanía **ha aceptado el llamado a defender la JEP y para exigir que se nieguen las objeciones de Duque** evitando que, como lo expone Miranda, "muchos excombatientes se sientan engañados por el Estado, ensanchando las filas de disidencias o ELN".

### **La puja por las objeciones también se dará en el Congreso** 

Mientras en las calles los ciudadanos impulsan la aprobación de la Ley Estaturia, el Congreso está dividido sobre la decisión. Por una parte Alejandro Chacón, presidente de la Cámara de Representantes, envío una solicitud a la Corte Constitucional para que se pronuncie sobre la competencia del Congreso para formar parte de una discusión que ya había sido zanjada; en cualquier caso, **en esa corporación las mayorías apuntan a rechazar las objeciones.**

Por otra parte, en el Senado s**e está impulsando la aceptación de las objeciones por parte del Centro Democrático;** según Miranda, esta divergencia de posiciones responde a que en la Cámara alta del Congreso, los políticos son viejos y los odios están más reforzados.

La Congresista concluye que **la Corte Constitucional debería pronunciarse para evitar un choque de trenes**, así como para desmentir las objeciones por supuestas inconveniencias, que la oposición ya ha tachado como sofismas jurídicos. No obstante, mientras la Corte se pronuncia, igual que hace dos años **"se necesita nuevamente estar en la calle para defender el proceso de paz".**

<iframe id="audio_33479546" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_33479546_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

######  

###### [[![Iluminar la Paz](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/03/54222273_10156402365526379_928031437825769472_o-800x400.jpg)[[su correo][[Contagio Radio]
