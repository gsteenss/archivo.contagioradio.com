Title: Iván Cepeda continuará con la defensa de los derechos humanos
Date: 2018-03-27 18:26
Category: Nacional
Tags: Iván Cepeda
Slug: ivan-cepeda-continuara-con-la-defensa-de-los-derechos-humanos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/10/epeda-e1476470290248.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo Particular] 

###### [27 Mar 2018] 

En un comunicado a la opinión publica el senador Iván Cepeda manifestó que esta afrontando un cáncer de colon, descubierto en fase temprana, sin embargo, **continuará con su labor de defensa de los derechos humanos en el Congreso**, hasta que su salud se lo permita.

“He informado sobre mi actual estado de salud. Lo hice con sentido de responsabilidad para evitar rumores. Pero sobre todo para anunciar que seguiré cumpliendo mis obligaciones políticas y mi trabajo en el Congreso de manera normal” afirmó Cepeda.

El senador afirmó que ya fue sometido a una intervención quirúrgica en el Hospital Universitario San Ignacio de la capital de la República, que debe ser complementada con quimioterapias y cuidados médicos. (Le puede interesar: ["Uribe a investigación por presunta manipulación de testigos contra Iván Cepeda"](https://archivo.contagioradio.com/uribe-testigos-cepeda/))

Así mismo frente a esta situación, Cepeda radicará ante la secretaría general del Senado de la República la solicitud de incapacidad médica total mientras dure su tratamiento. Cepeda se ha destacado **en su labor de congresista como ser uno de los mayores defensores de derechos humanos y ser acérrimo constructor de paz para Colombia**.

Además el senador afirmó que este revés le ha significado adquirir nuevas experiencias para reafirmas su trabajo en la búsqueda de garantizar los derechos de la ciudadanía como la salud, “Esta experiencia me ha permitido conocer la vida, y el trabajo de muchos colombianos que afrontan esta enfermedad y lo hace de una forma valiente y con éxito, también lo hago yo a manera de ejemplo y estímulo para otros que tienen esta enfermedad” expresó el senador para dejar en claro que no está derrotado y seguirá sus labores común y corriente.

<iframe id="audio_24926615" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_24926615_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
