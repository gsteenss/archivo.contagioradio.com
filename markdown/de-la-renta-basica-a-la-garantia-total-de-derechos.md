Title: De la renta básica, a la garantía total de derechos
Date: 2020-04-09 10:40
Author: AdminContagio
Category: Actualidad, Comunidad
Tags: Covid-19, Población, Renta básica
Slug: de-la-renta-basica-a-la-garantia-total-de-derechos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/04/La-renta-básica.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:heading {"align":"right","level":6,"textColor":"cyan-bluish-gray"} -->

###### Foto: @rudaut {#foto-rudaut .has-cyan-bluish-gray-color .has-text-color .has-text-align-right}

<!-- /wp:heading -->

<!-- wp:paragraph -->

Durante esta semana organizaciones sociales reafirmaron la insistencia en que así como se extendió el aislamiento preventivo, en aras de proteger a la población del virus del Covid-19, se implemente la renta básica para asegurar el acceso de personas vulnerables económicamente a alimentación y vivienda. Sin embargo, algunos señalan que la medida debería extenderse y ampliarse con otros horizontes.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/Ccajar/status/1247664890384388104","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/Ccajar/status/1247664890384388104

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:heading {"level":3} -->

### **La renta básica, necesidad a corto plazo**

<!-- /wp:heading -->

<!-- wp:paragraph -->

Zaira Agudelo, profesora de la Universidad de Antioquia e integrante de Viva La Ciudadanía aseguró que la renta básica era una propuesta utópica, que dada la realidad de la crisis generada por el Covid-19 ahora se está planteando como una medida que se debe tomar. Se trataría de "un ingreso monetario al que toda persona tiene derecho" sin importar su condición, para ser autosuficiente y no depender de nadie.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Agudelo señaló que en América Latina hay experiencias de programas parecidos, como la implementación en Brasil del programa Hambre Cero y otro llamado Bolsa de Familia con el que "salieron más o menos 28 millones de personas de la pobreza y 29 millones ingresaron a la clase media". También referenció los programas de asistencia a mayores de 65 años en México, y las canastas básicas familiares que funcionaban en países como Argentina o Uruguay.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### **El Covid-19 expuso las desigualdades, pero también abre la puerta a las oportunidades**

<!-- /wp:heading -->

<!-- wp:paragraph -->

En el largo plazo, sin embargo, Agudelo sostuvo que era necesario pensar en otros derechos como la seguridad alimentaria, la atención en salud, educación y la eliminación de todas las formas de explotación que también se pusieron de presentes ante las medidas de aislamiento preventivo. (Le puede interesar:["La crisis económica no se resuelve con limosnas: Alirio Uribe Muñóz"](https://archivo.contagioradio.com/la-crisis-economica-no-se-resuelve-con-limosnas-alirio-uribe-munoz/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Es así como el Estado debería tomar medidas de largo plazo (10 a 15 años) para cerrar las brechas de desigualdad expuestas, y evitando que la renta básica se vea reducida a un impuesto, mientras se perpetúan otras condiciones que siguen aumentando la brecha que existe entre personas ricas y pobres.

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78980} /-->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->
