Title: "En Cauca las comunidades somos el peón de los grupos armados"
Date: 2020-04-20 19:21
Author: CtgAdm
Category: Actualidad, Nacional
Tags: asesinato, Cauca, Disidencias de las FARC, ELN
Slug: en-cauca-las-comunidades-somos-el-peon-de-los-grupos-armados
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/03/Captura-de-pantalla-2019-03-20-a-las-12.51.53-p.m..png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

A pesar de la crisis nacional que se vive por causa del covid-19, hoy hay otro virus que afecta los liderazgos del departamento del Cauca, y que ha cobrado en los últimos días la vida de 3 personas.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Asesinatos atribuidos a los grupos armados presentes en el territorio, reconocidos por las comunidades como los frentes Carlos Patiño y Jaime Martínez, integrados por disidencia de las FARC, y quienes mantienen un enfrentamiento constante con el frente José Maria Becerra del ELN; y una disputa por manejar las rutas del [narcotráfico](https://www.justiciaypazcolombia.com/llamado-misericordioso-a-parar-el-narcotrafico/)en Argelia, Patía y El Tampo.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Tres vidas silenciadas en menos de 72 horas

<!-- /wp:heading -->

<!-- wp:paragraph -->

Según *Teresa*, vocera de Micay e integrante de Asociación Nacional Campesina Coordinador Nacional Agrario (CNA), los integrantes del frente Carlos Patiño ingresaron al territorio, desde que el ELN anunció el cese unilateral del fuego, *"a generar miedo y desplazamientos".*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

La primera victima, fue **Teodomiro Sotello Anacona**, **líder del Consejo Comunitario  Afro renacer del Micay, en Cauca**; miembro de la Mesa Alternativa de Cultivos y del proceso Intercultural para la Transformación Productiva del Territorio impulsado por el CNA, además integraba la Asociación Nacional Campesina CNA.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Según Teresa el hecho se dió en horas de la mañana del 17 de abril, *"Teodomiro fue asesinado frente a su familia y a sus trabajadores, él era un gran compañero en el territorio, emprendedor y trabajaba por el desarrollo de la comunidad; **hacía parte de la vocería en el tema de la sustitución de cultivos**, y además impulsó el comité pro-carretera, **con un sueño claro y trabajo duro, logró que las comunidades hoy de Betania y Agua Clara tengan carretera**".*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

A pocas horas y kilometros del homicidio de Teodomiro, se registró el asesinato de **Andrés Adrelio Cansimanse Burbano,** campesino de la Vereda Honduras, y compañero sentimental de Lorena una líderesa social del Tambo.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

*"Este hecho duele, porqué **a pesar de que la buscaban a ella y logró sobrevivir, le arrebataron a su compañero de vida**"*, agrega Teresa y señala que la lideresa es presidenta del comité de deportes del Tambo, e integrante también de la Asociación Nacional Campesina CNA.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por último el día domingo 18 de abril sobre las 5:00 am se registró el asesinato del líder campesino de Naya[Mario Chilhueso](https://archivo.contagioradio.com/fue-asesinado-mario-chilhueso-lider-campesino-del-naya/). cuando hombres armados ingresaron a **la vereda Los Robles de Buenos Aires**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

**Chilhueso** era sobreviviente del la masacre del Naya en 2001, y presidente de la **Asociación de Trabajadores Campesinos del Cauca, (ASTCAP),** *"la violencia en el Cauca no cesa, en medio de la crisis de salud, estos actores armados al servicio del narcotráfico con miedo y violencia controlan el territorio*", anexó Teresa.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Me fui porque mi vida corría el mismo riesgo

<!-- /wp:heading -->

<!-- wp:paragraph -->

Teresa, cuyo nombre cambiamos para proteger su identidad integra también la Asociación Nacional Campesina (CNA), y manifestó que tuvo que desplazarse hace casi 15 días del Tambo, a causa de amenazas contra su vida por parte del frente Carlos Patiño.

<!-- /wp:paragraph -->

<!-- wp:quote -->

> "Nuestro lucha lleva 15 años en Tambo y Argelia; para nosotros lo más importante es que nuestros territorios invadidos por el tema de los cultivos de coca, vuelvan a ser tierra de pan coger, de vida y unidad con la madre tierra que tanto hemos agredido"
>
> <cite>Lideresa del Tambo</cite>

<!-- /wp:quote -->

<!-- wp:paragraph -->

Al mismo tiempo que Teresa salia a proteger su vida a kilometros de su hogar, 30 familias más salieron de las diferentes veredas temiendo por sus vidas, a pesar de los riesgos de no tener un techo o alimentos, y la posibilitad de contraer el coronavirus.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

*"Sabemos que gran parte de este conflicto es por el tema del narcotráfico y la siembra de Coca pero también por el abandono estatal"*, la lideresa señaló que los campesinos y campesinas actualmente no tienen más alternativa que ponerse a cultivar coca, *"ellos lo hacen para sobrevivir, ya sea por no morir de hambre o por evitar que los maten al no prestar sus tierras".*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

**Con tristeza Teresa reconoce que estos actores armados llegaron a acabar con los procesos que ellos llevaban por año**s, *"el decir que estabamos en contra de los cultivos de coca, causó que no acusaran de peligro para la sociedad, un señalamiento que acabó con la vida de varios de mis compañeros".*

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Es necesario un cese al fuego definitivo en el Cauca

<!-- /wp:heading -->

<!-- wp:paragraph -->

Finalmente la lideresa agregó que las comunidades del Cauca han sido por años, víctimas del fuego cruzado entre el ELN, las disidencias del Farc y el narcotráfico, *"exigimos un cese real al fuego, de TODOS los actores armados, tanto legales como ilegales".*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

***"Históricamente hemos sido peones de una guerra sin fin entre los grupos armados"**,* señala, y a su vez indica que los recientes hechos son *"responsabilidad del los frente Carlos Patiño y Jairo Martínez, porqué desde que el ELN declaró un cese a sus actividades, estos aprovecharon para expandir su dominio y aumentar sus actividades de narcotráfico y violencia".*

<!-- /wp:paragraph -->

<!-- wp:quote -->

> *"Creímos que con la salida del ELN el territorio iba a tener paz, pero al contrario los otros actores armados aprovecharon para dar rienda suelta a sus fechorías, sin importar la vida de nadie"*
>
> <cite>Teresa | Lideresa del Tambo</cite>

<!-- /wp:quote -->

<!-- wp:paragraph -->

Uno de los mayores miedos que actualmente tienen las comunidades donde hace presencia el ELN, es que luego de este 30 de abril cuando finalice el periodo de [cese al fuego](https://archivo.contagioradio.com/cese-unilateral-del-eln-completa-18-dias-con-balance-positivo/) por parte de este grupo guerrillero, se intensifique la disputa territorial y la violencia que pone en riesgo la vida de miles de personas de las comunidad étnicas y campesinas que habitan este departamento.  

<!-- /wp:paragraph -->
