Title: Desaparecidos regresan con sus familiares en los Llanos Orientales
Date: 2016-12-05 12:38
Category: Otra Mirada, Paz
Tags: Acuerdo busqueda de desaparecidos, Búsqueda de desaparecidos en Colombia, Colectivo Orlando Fals Borda, Desaparición forzada Colombia, Implementación de Acuerdos
Slug: desaparecidos-regresan-familiares-los-llanos-orientales
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/12/desaparecidos-recuperados-por-sus-familias.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Colectivo OFB] 

###### [5 Dic 2016] 

A través de un comunicado, los familiares de desaparecidos en los Llanos Orientales, quienes **después de 20 años recibieron los cuerpos de 15 seres queridos en el marco de de las medidas humanitarias del proceso de paz,** entre agosto y noviembre del presente año, hacen un llamado al Gobierno para que se de celeridad a los demás casos que están impunes.

En la misiva, los familiares denunciaron que “el Estado debe reconocer su responsabilidad en los crímenes y no puede ocultar más la información sobre cómo sucedieron los hechos. **Es responsabilidad del Estado dar respuestas a las familias de los desaparecidos, no es un favor sino un deber de las autoridades** para lograr el resarcimiento de derechos”.

Por otro lado, exigen a los grupos armados, Ejército, grupos paramilitares y guerrillas, que **“den información que permita recuperar los demás cuerpos para darles una digna sepultura”. **

Estas familias extienden un llamado de solidaridad a distintos medios de comunicación y a instituciones del Estado para que **“difundan públicamente en cada rincón del país los rostros y nombres de quienes se han identificado para que otras familias logren recuperar los cuerpos de sus víctimas"**. Le puede interesar: Desaparecidos [podrían estar sepultadas en cementerios como personas no identificadas.](https://archivo.contagioradio.com/personas-desaparecidas-podrian-estar-sepultadas-como-nn/)

Por último, agradecen la labor y acompañamiento de organizaciones de Derechos Humanos como el Colectivo OFB, a Medicina Legal, a los sacerdotes y pastores de DiPAZ, al Padre Henry Ramírez y a los Claretianos y a los equipos negociadores del proceso de paz por parte del gobierno y la guerrilla de las FARC EP.

###### Reciba toda la información de Contagio Radio en [[su correo]
