Title: 180 familias campesinas son desalojadas por ESMAD en el Meta
Date: 2017-05-09 14:04
Category: DDHH, Nacional
Tags: campesino, Desalojo, ESMAD, Meta, Puerto Gaitán
Slug: 180-familias-campesinas-son-desalojadas-por-esmad-en-el-meta
Status: published

###### [Foto: Archivo] 

###### [09 May. 2017]

**Alrededor de 180 familias campesinas que se encontraban en condición de desplazamiento** y que ante la falta de respuesta del Estado ocuparon la finca “El Brasil” en la vereda La Cristalina, Municipio de Puerto Gaitán, Meta fueron desalojadas por 400 miembros del ESMAD. De las 180 familias campesinas, **cerca de 80 son víctimas del conflicto armado. **Le puede interesar: [Ley 133 legalizaría el acaparamiento de tierras en Colombia](https://archivo.contagioradio.com/ley-133-legalizaria-el-acaparamiento-de-tierras-en-colombia/)

La ocupación del territorio se dio hace un año, razón por la cual las personas ya cuentan con cultivos, han construido viviendas **“ya que se encuentran sin ningún uso por parte de los propietarios”** dice un comunicado entregado por las familias. Le puede interesar: [Ley  Zidres para  acaparadores de tierras afecta territorio colectivos](https://archivo.contagioradio.com/ley-zidres-para-acaparadores-de-tierras-afecta-territorio-colectivos/)

Pese a que las comunidades habían solicitado que **no se realizará dicho desalojo debido a la presencia de niños, niñas y adultos mayores,** la comunicación no fue atendida “las instituciones como: Defensoría del Pueblo, Procuraduría y Unidad de Víctimas, tienen conocimiento de la situación de desplazamiento de estas familias campesinas” dice la denuncia.

Así mismo, insisten en detener dicho desalojo por parte del ESMAD para evitar la vulneración de los derechos de niños, niñas y adultos mayores presentes en el territorio y **responsabilizan a la Alcaldía de Puerto Gaitán y el Estado de cualquier agresión** o daño en los bienes con los que cuentan los campesinos.

Por último, aseguran que **dicho desalojo “revictimiza y agudiza la situación de vulnerabilidad de las familias, sin aportarnos alguna solución”. **

### **¿Cuál es la historia de la Finca “El Brasil”?** 

Son cerca de **16 mil hectáreas** las que constituyen dicho predio **que pertenecen al grupo “Aliar- La Fazenda”**, cuestionado por la acumulación ilegal y adquisición de estas tierras en donde se pretendía realizar producción de maíz y soya .

Para el caso de “El Brasil”, la superintendencia investiga si Aliar - Laa Fazenda violó la ley 160 de 1994, que **prohíbe la acumulación de tierras baldías de la nación que hayan sido adjudicadas a campesinos** en el marco de la reforma agraria. Le puede interesar: [Cerca de 19 empresas son las que han acaparado más tierras en Colombia](https://archivo.contagioradio.com/cerca-de-19-empresas-son-las-que-han-acaparado-mas-tierras-en-colombia/)

De igual modo, dicho predio ha sido cuestionado por **servir como escenario de entrenamiento del grupo paramilitar autodenominado “Los Carranceros”** al mando de Guillermo Torres. Acusaciones que fueron desmentidas para el año 2013 en una entrevista para Verdad Abierta por parte de Jaime Liévano, presidente de dicha compañía.

###### Reciba toda la información de Contagio Radio en [[su correo]

<div class="ssba ssba-wrap">

</div>

<div class="ssba ssba-wrap">

</div>
