Title: Octava jornada global por Ayotzinapa
Date: 2015-01-23 21:15
Author: CtgAdm
Category: El mundo, eventos
Tags: Ayotzinapa, DDHH, mexico, Paramilitarismo
Slug: octava-jornada-global-por-ayotzinapa
Status: published

###### Foto: mashable.com 

**26 de enero, a cuatro meses ¡Ni perdón ni olvido!**

A cinco meses de la desaparición de **43 estudiantes de la Normal Rural Isidro Burgos de Ayotzinapa**, en Guerrero, México, se realiza la Octava Jornada de Acción Global, que cuenta con la participación de más de 20 ciudades en el mundo y con la participación de miles de personas que siguen afirmando que no perdonan y no olvidan.

Estas son Algunas de las ciudades de la convocatoria y los sitios de las movilizaciones

**Internacionales**  
**España - Barcelona:** Placa Universitat a las 19 horas - Castellón: Viernes 30 de enero en la Plaza de la Paz a las 18 horas.  
**Estados Unidos -Manhattan, Nueva York:** Consulado Mexicano 27 East 39th Street a las 16 horas - Berkeley, California: Sproul Plaza a las 9 am - Los Ángeles, California: Frente al consulado mexicano 17 horas - San Diego, California: Parque Chicano, 92113 a las 18 horas .

**Raleigh, Carolina del Norte:** Consulado Mexicano, 431 Raleigh View Road, 27610 a las 11 am

**Canadá - Calgary AB:** domingo 25 de enero en Prince´s Island Park a las 14 horas  
**Francia - Paris:** Place Saint – Michel a las 18:30 horas  
**Uruguay - Montevideo**: Playa Ramírez, Rambla Wilson y Av. Sarmiento a las 20 horas.
