Title: ¿Sexo y poder hacia un posconflicto?
Date: 2015-09-13 10:00
Category: eventos, Mujer
Tags: Conversaciones de paz en la Habana, participación de las mujeres en sociedades en conflicto y posconflicto, Plataforma de Acción de Beijing, posconflicto, proceso de paz, Seminario Internacional “Sexo y Poder - Hacia un posconflicto con apuestas feministas”
Slug: sexo-y-poder-hacia-un-posconflicto
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/sexo-y-poder.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: www.humanas.org.co 

###### [14 sep 2015 ]

El próximo 17 de Septiembre se realizará  el Seminario Internacional “Sexo y Poder - Hacia un posconflicto con apuestas feministas” en la Universidad de los Andes, donde se contará con la participación de expertas académicas y activistas.

La Corporación Humanas llevará a cabo este seminario con el fin de conmemorar 10 años de actividades y reflexiones sobre la participación de las mujeres en sociedades en conflicto y posconflicto. Para la Corporación es esperanzador las conversaciones de paz en La Habana ya que este escenario de posconflicto en el país  genera una reflexión particular sobre los desafíos para la construcción y promoción de nuevas ciudadanías, donde las mujeres juegan un papel esencial en la estabilidad política y la construcción de una democracia estructural inclusiva y participativa.

El evento se desarrolla, teniendo en cuenta que este año se celebran el 20 aniversario de la adopción de la Declaración y la Plataforma de Acción de Beijing firmada por 189 gobiernos, los 15 años de la Resolución 1325, y se inicia una nueva agenda global para el desarrollo con el objetivo de pensar acciones para un crecimiento equitativo, sostenido e inclusivo para las mujeres.

Este seminario contará con expertas nacionales e internacionales quienes compartirán sus reflexiones sobre la participación política y social de las mujeres en Estados militarizados y heteronormativos, en países en conflicto y posconflicto.

Cabe recordar que a través de[contagioradio.com ](https://archivo.contagioradio.com/envivo.html)se podrá seguir en vivo la realización del seminario.
