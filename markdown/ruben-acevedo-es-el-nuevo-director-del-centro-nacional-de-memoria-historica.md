Title: Ruben Acevedo nuevo director del Centro Nacional de Memoria Histórica
Date: 2019-02-04 17:29
Author: AdminContagio
Category: DDHH, Entrevistas
Tags: Centro Nacional de Memoria Histórica, falsos positivos, Ruben Dario Acevedo, víctimas
Slug: ruben-acevedo-es-el-nuevo-director-del-centro-nacional-de-memoria-historica
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/02/dario-acevedo-1-770x400.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: AlPoniente] 

###### [19 Feb 2019] 

El presidente Iván Duque confirmó este martes el nombramiento de Rubén Acevedo como nuevo director del Centro Nacional de Memoria Histórica, pese a que  organizaciones sociales se habían opuesto, debido a que Acevedo ha manifestado su rechazo a **la Comisión de la Verdad, la Jurisdicción Especial para la Paz e incluso ha negado la existencia del conflicto armado en el país**.

El profesor emérito de la Universidad Nacional, ha hecho afirmaciones como que la Comisión de la Verdad **“es fruto del interés político de las guerrillas por imponer una explicación justificadora de sus aventuras y crímenes **en ropaje académico”, argumentando que sí lo que Colombia busca es realizar estudios sobre la violencia política y el conflicto armado, “lo correcto, lo adecuado y lo pertinente es que se cree una línea macro de investigación dirigida por Colciencias”, hecho que para la Fundación Paz y reconciliación evidencia su poca credibilidad en el trabajo que ha venido adelantando la Comisión de la Verdad. (Le puede interesar:["Comisión de la Verdad se convierte en una esperanza para organizaciones del Caribe"](https://archivo.contagioradio.com/comision-de-la-verdad-se-convierte-en-una-esperanza-para-organizaciones-del-caribe/))

Además, también ha enfatizado que “lo deseable es que la verdad y la memoria sean abordadas con criterios profesionales y técnicos abiertos. En tal sentido, los productos de la memoria deberían ser clasificados, **divulgados y puestos a disposición del público y de los académicos en su sitio adecuado: el Archivo General de la Nación**”, afirmación que cuestiona su compromiso con la misión y el deber ser del Centro Nacional de Memoria Histórica.

Frente a los Acuerdos de Paz y la JEP, Acevedo ha asegurado, desde su columna en el diario El Expectador, en diversas ocasiones, la inexistencia de un conflicto armado en el país y la tierra como principal factor que suscitó un periodo de violencia. De igual forma ha rechazado las posibles investigaciones a terceros e integrantes de la Fuerza Pública en el Tribunal para la paz por nexos con el paramilitarismo.

### **Ruben Dario Acevedo y sus posturas contra la izquierda** 

Otra alerta expresada por las organizaciones sociales en Colombia, tiene que ver con el “desdén” que Acevedo ha manifestado en contra de quienes hacen parte de partidos políticos con tendencias a la izquierda, situación que para las organizaciones afectaría la reconstrucción de memoria de esos sectores, **en casos tan emblemáticos como el genocidio a la Unión Patriótica, al Partido Comunista y líderes de izquierda**.

Asimismo, Acevedo aseveró que no considera que en Colombia el **Estado haya impulsado una política de violación a derechos humanos, como lo fueron las ejecuciones** extrajudiciales cometidas bajo el gobierno del entonces presidente Álvaro Uribe Vélez, las interceptaciones del DAS, entre otras actuaciones cuestionables y sancionadas en contra del país, además cuestionó los llamados de atención de la Corte Penal Internacional frente al juzgamiento de altos mandos militares y sus implicaciones en delitos de lesa humanidad.

### **Las organizaciones sociales rechazan la hoja de vida de Acevedo** 

En un comunicado de prensa, diversas organizaciones expresaron su inconformidad con el nombramiento de Acevedo en la dirección del Centro Nacional de Memoria Histórica, debido a que en primera instancia no cumple con los requisitos para desempeñar este cargo, tiene una visión sesgada frente a **la construcción de memoria y habría una falta de garantías para las víctimas, sin darles mecanismos de participación**.

Finalmente las organizaciones afirmaron que “este momento de nuestra historia requiere de compromiso y voluntad política para construir juntos y juntas nuestro futuro, es la oportunidad para que el Estado colombiano se abra a las víctimas y a la sociedad, para promover una participación e inclusión real, necesarias para fortalecer la reconciliación nacional, saldar la deuda histórica con las víctimas y garantizar la no repetición, evitando re victimizar a quienes históricamente han visto negada su voz.”

[Decreto 247 Del 19 de Febre...](https://www.scribd.com/document/400017287/Decreto-247-Del-19-de-Febrero-de-2019#from_embed "View Decreto 247 Del 19 de Febrero de 2019 on Scribd") by on Scribd

<iframe class="scribd_iframe_embed" title="Decreto 247 Del 19 de Febrero de 2019" src="https://www.scribd.com/embeds/400017287/content?start_page=1&amp;view_mode=scroll&amp;show_recommendations=true&amp;access_key=key-ghVdDQ0t3PonRKxQyfll" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="true" data-aspect-ratio="null"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
