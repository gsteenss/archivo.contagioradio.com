Title: En el Medio: Los silencios del periodismo colombiano
Date: 2017-03-01 09:13
Author: AdminContagio
Category: 24 Cuadros
Tags: Cine, colombia, Documental, Libertad de Prensa, periodismo
Slug: prensa-libertad-documental-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/02/documental-en-el-medio-silencios-periodismo-colombiano.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto:FLIP 

###### 23 Feb 2017 

En el Medio: Los silencios del periodismo colombiano, es un documental realizado por la Fundación para la Libertad de Prensa (FLIP), junto con la Universidad del Rosario y el Fondo para la Democracia de las Naciones Unidas (UNDEF).

La producción aborda el tema de la censura a la libertad de prensa en las regiones más apartadas del país, en departamentos como Chocó, Putumayo Guaviare y el Magdalena. Denuncia como las emisoras tratan de sobrevivir en condiciones precarias, en lugares donde la delincuencia común aliada con el Estado es pan de cada día.

Las amenazas por difundir la información y la estigmatización de los gobiernos hacia los comunicadores independientes son constantes, al punto que los periodistas se ven obligados a dejar de transmitir sus programas e incluso cerrar sus emisoras. Le puede interesar: [El estado, la prensa y los ciudadanos una relación conflictiva](https://archivo.contagioradio.com/el-estado-la-prensa-y-los-ciudadanos-una-relacion-conflictiva/) .

El documental, retrata la difícil condición de ejercer el periodismo en Colombia, un pais en el que se registraron 262 víctimas en 216 casos de violación a la libertad de prensa en el trascurso de 2016 y que hoy en día, el periodismo, trata de salir a flote en tiempos de transición a una paz que pretende ser estable y duradera.

<iframe src="https://www.youtube.com/embed/MF_YB5sJtw4" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)
