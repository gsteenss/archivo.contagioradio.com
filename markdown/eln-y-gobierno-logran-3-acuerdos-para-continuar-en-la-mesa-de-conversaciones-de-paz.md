Title: ELN y gobierno logran 3 acuerdos para continuar en la mesa de conversaciones de paz
Date: 2017-06-06 12:53
Category: Nacional, Paz
Tags: ELN, Gobierno Nacional, Negociaciones paz
Slug: eln-y-gobierno-logran-3-acuerdos-para-continuar-en-la-mesa-de-conversaciones-de-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/06/GettyImages-518213622-e1496771599527.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Univisión] 

###### [06 Jun 2017] 

Un comunicado difundido la mañana de este martes da cuenta de **tres acuerdos en los que han avanzado las delegaciones de paz del ELN y el Gobierno Nacional** en el marco de este ciclo de conversaciones. Se acordó la creación de un equipo de comunicación para la paz con un plan de acción inmediato, los términos de referencia para el respaldo de los países acompañantes y el fondo de financiación de la mesa.

La creación del equipo conjunto de pedagogía y comunicación para la paz, tendrá como objetivo promover la **construcción de una cultura de paz que genere confianza, credibilidad y solidez al trabajo de la mesa de diálogos** y los acuerdos a los que se lleguen entre el ELN y el Gobierno Nacional. Adicionalmente, en el marco del acuerdo, se acordó el plan de acción, funciones, criterios, alcances, integrantes y metodología de trabajo del equipo de comunicación para la paz. Le puede interesar: ["ELN propone cese bilateral para recibir al papa Francisco"](https://archivo.contagioradio.com/eln-propone-cese-bilateral-para-recibir-al-papa-francisco/)

En lo que tiene que ver con el Grupo de Países de Apoyo, Acompañamiento y Cooperación a la Mesa de Diálogos se informó que la conformarán **Alemania, Holanda, Italia, Suecia y Suiza.** Finalmente las partes acordaron que se estableció el fondo de financiación para la mesa de conversaciones. El objeto del fondo es movilizar contribuciones de la cooperación internacional para financiar los gastos relacionados con el desarrollo de la Mesa. Le puede interesar: ["5 países crearán fondo económico para apoyar proceso de paz con el ELN"](https://archivo.contagioradio.com/5-paises-crearan-fondo-economico-para-apoyar-proceso-de-paz-con-eln/)

Las Delegaciones del Gobierno Nacional y el Ejército de Liberación Nacional, ELN, también agradecieron a Brasil, Chile, Cuba, Noruega y Venezuela quienes son los países garantes de las negociaciones.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU).
