Title: Colombianos en el exterior rompen el cerco y hacen pedagogía por la paz
Date: 2016-03-14 18:20
Category: Nacional, Paz
Tags: Encuentro por la paz, Exiliados, Paramilitarismo
Slug: colombianos-en-el-exterior-rompen-el-cerco-y-hacen-pedagogia-por-la-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/03/Paz_colombia_madrid_contagioradio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: elespectador] 

<iframe src="http://co.ivoox.com/es/player_ek_10809221_2_1.html?data=kpWlkp6WdpKhhpywj5aVaZS1k5aah5yncZOhhpywj5WRaZi3jpWah5yncaTjzdTaxM7FstDnjMrbjcrQb8bs1crfy9TWb9PjztXS0JDJsIzXxtfQ0ZDdb8nVxMrbjdXJqMKhhpywj6jTstXVyM7cjbfFqMrjjJKSmaiReA%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Olguer Santodomingo] 

###### [14 Mar 2016] 

Durante el tercer encuentro por la paz de Colombia en Madrid, cerca de 100 personas, representantes de organizaciones sociales, colectivos, así como víctimas y exiliados en general, resaltaron que una de las **principales preocupaciones frente al proceso de paz es el [desmonte de paramilitarismo](https://archivo.contagioradio.com/?s=paramilitarismo).**

Olguer Santodomingo, integrante de la Plataforma Justicia por Colombia, en España, aseguró que si no hay garantía de desmonte del paramilitarismo la situación en Colombia no mejorará. Es urgente que el gobierno nacional tome **medidas urgentes para proteger la vida y la integridad de las personas que desde las organizaciones de DDHH trabajan por la paz con justicia social.**

Santodomigo agrega que otro de los problemas en el marco del proceso es el de la posibilidad de la **[pedagogía por la paz](https://archivo.contagioradio.com/?s=pedagogia)**, en España y Europa en general, la visión que se tiene del proceso de conversaciones de paz proviene, sobre todo, de la versión de los grandes medios de información que muchas veces y no de las partes en conversaciones que son la verdadera autoridad.

Por ello, el tercer encuentro por la paz se centró en la idea de conocer tanto los avances, como los puntos complicados y nodales de la discusión, además **la necesidad de la inclusión de las reflexiones desde los colombianos y colombianas en el exterior**, por ello, tanto las conclusiones como las propuestas se entregarán en un documento a los equipos de paz del gobierno y las FARC.

Santodomingo también afirma que el próximo 17 de Marzo, en el marco del paro nacional realizarán una concentración en la plaza de la Puerta del Sol en Madrid y así acompañarán las exigencias de los trabajadores y trabajadoras en Colombia, puesto que no se pueden promover políticas públicas contrarias a la construcción de paz **como en el caso de la [venta de ISAGEN](https://archivo.contagioradio.com/?s=isagen).**
