Title: Las FARC-EP a un paso de conformar su partido político
Date: 2017-01-24 10:10
Category: Entrevistas, Paz
Tags: acuerdo farc y gobierno, Paramilitarismo, Trump, voces de paz
Slug: las-farc-ep-y-su-partido-politico
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/01/Farc-en-Yari.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### Foto: Contagio Radio 

##### 24 Ene 2017 

El Estado Mayor de las FARC-EP, en transito hacia la vida civil, se reunió durante esta semana en los llanos del Yarí, con el objetivo de evaluar el cumplimiento del mandato de la X Conferencia , evaluar el avance del proceso de implementación de los acuerdos de paz y elegir el Estado Mayor de Transición conformado por 61 personas de cara a un nuevo partido político.

### [**Partido Político FARC-EP **] 

Jesús Santrich, explicó que el lanzamiento del partido político tendría que darse luego del congreso de esa organización que podría ser en el mes de Mayo de este año, además con un trabajo previo de asambleas y reuniones en las que se recogerán los puntos de vista, los criterios de la organización así como sus representantes. Para ello se plantean realizar asambleas y reuniones en los diferentes frentes y con una metodología, explicó.

Según Santrich durante el trascurso de esta semana se darían a conocer los nombres de los nuevos integrantes del Estado Mayor de Transición, pensando en el paso a la vida política de las FARC pero también en el proceso de implementación de los acuerdos. Un partido político de las FARC  se construiría con democracia participativa, que no solamente incluya la participación parlamentaria sino la construcción de nuevas formas de hacer política.

### [Voces de paz y su desempeño en el Congreso] 

Santrich resaltó que aunque no se ha podido realizar una reunión de evaluación con los y las integrantes de **Voces de Paz** si hay señales muy positivas del trabajo que se está realizando en torno a la implementación de los acuerdos. Que se haya aprobado la ley de amnistía con la mayoría con la que se dio y que se esté discutiendo la Jurisdicción Especial de Paz ya son puntos que dan cuenta de un buen trabajo.

### [Trump, Hollande y el respaldo internacional] 

Frente a los respaldos internacionales Santrich afirma que son muy importantes. En el caso del gobierno de Estados Unidos que revisará los acuerdos para definir si continúa el respaldo o no, Santrich dijo que es muy importante que se continúe pero que si se decide lo contrario será con la fuerza de la ciudadanía y de los gobiernos de América Latina que han reiterado la necesidad de que en Colombia se continúe la construcción de la paz.

Por otro lado, la visita del presidente de Francia, François Hollande, es de resaltar porque este tipo de acciones ponen sobre la mesa del concierto mundial al proceso de paz de Colombia y son un aporte que se valora “inmensamente”. “El presidente de Francia es bienvenido” y además no sería una visita protocolaria, porque visitar una zona veredal significa mucho más que eso, destaca el delegado de las FARC-EP

### [Paramilitarismo] 

Sobre este punto Jesús Santrich  afirmó "entre finales y comienzos de este año van más de 29 asesinados por paramilitarismo" y ratifica que ese fenómeno es una realidad en Colombia que no se puede negar, "sería vivir en otro mundo negar el paramilitarismo cuando los muertos están ahí a la orden del día" **[Ver:  Paramilitares asesinan a dos pobladores del Salaquí en el Bajo Atrato](https://archivo.contagioradio.com/parmilitares-choco-asesinato/)**

En la entrevista recalcó que esas fuerzas deben tener un respaldo en la vieja institucionalidad y que además están mostrando una actuación sistemática. Frente a ello señalo que esta guerrilla entregó una evaluación al gobierno y que se ha visto buena actitud para trabajar de manera conjunta. Recordó que en el acuerdo hay mecanismos establecidos que se pueden aplicar.

Sobre la posibilidad de un acuerdo de desmovilización de los grupos paramilitares, Santrich resaltó que ellos siempre han dicho que el paramilitarismo hay que desarticularlo, no solamente con medidas de fuerza sino con acciones conjuntas que también incluyan la participación de la sociedad. [Le puede interesar: ["Jurisdicción Especial de Paz debe ser fiel a víctimas del conflicto armado" Iván Cepeda](https://archivo.contagioradio.com/jurisdiccion-especial-para-la-paz-debe-ser-fiel-a-las-victimas-del-conflicto-armado-ivan-cepeda/)]

### [La incertidumbre es un mal ingrediente para este momento de transición] 

En las semanas anteriores se habló del alistamiento de las **Zonas Veredales Transitorias de Normalización** y se dijo que solamente estaban listas 8 de 20. Para Jesús Santrich la incertidumbre que produce que los factores logísticos no estén listos si afecta la confianza, sin embargo recordó que los integrantes de las FARC-EP están muy próximos a las zonas y solamente hacen falta algunos detalles “mínimos”.

Las FARC-EP tienen toda la voluntad de ayudar y de construir una realidad diferente y por ello, a pesar de las dificultades, se mantienen firmes en el propósito de construir la paz,, concluyo Jesús Santrich.

<iframe id="audio_16614080" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_16614080_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>
