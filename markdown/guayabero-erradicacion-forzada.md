Title: Operativo de erradicación forzada en Guayabero deja 4 heridos y 20 campesinos retenidos por las FFMM
Date: 2020-08-08 13:39
Author: AdminContagio
Category: Actualidad, DDHH
Tags: Erradicación Forzada, Guayabero, Meta, Vista Hermosa
Slug: guayabero-erradicacion-forzada
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/08/Video-Guayabero.mp4" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/08/Denuncia-comunicador-voces-del-guayabero.mp4" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/08/Denuncia-comunicador-voces-del-guayabero-1.mp4" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

Según la información inicial por lo menos un campesino herido, otro desaparecido, 2 campesinos detenidos y una fuerte confusión son los resultados de un operativo de erradicación forzada en la vereda Nueva Colombia del Muncipio de Vista Hermosa, región del Guayabero en el departamento del Meta.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

La denuncia de los campesinos relata que desde tempranas horas de la mañana de este sábado 8 de Agosto, campesinos habitantes de la región de Losada - Guayabero en el Meta se desarrolla un operativo de erradicación forzada, por parte de la la Fuerza de Tarea Omega.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Diversos audios enviados por reporteros comunitarios dan cuenta de por lo **menos 20 campesinos estarían retenidos**. Adicionalmente cuando la comunidad se dirigía al punto de la retención fueron atacados con ráfagas de fusil por parte de las FFMM, quienes argumentan que desarrollarán la erradicación "por encima de quien sea". Hasta el momento de la denuncia se desconoce la situación de los campesinos confinados por el ejército.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### "El ejército llegó disparando contra la comunidad de Vista Hermosa"

<!-- /wp:heading -->

<!-- wp:paragraph -->

Según uno de los testimonios el ejército habría llegado disparando gases lacrimógenos y bombas aturdidoras en contra de los habitantes de la vereda Nueva Colombia en Vista Hermosa. Además denuncian que los militares presentes allí no se han identificado y no hay manera de mediar en una conversación pues la negativa del ejercito lo impide.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Otro de los hechos que más preocupa a la comunidad es la desaparición de José Medardo Córdoba Rivas, identificado con CC 1076332074, quien se encontraba tratando de impedir la operación y que no ha sido ubicado por parte de la comunidad desde esta madrugada según un testimonio de [ANZORC](http://anzorc.com/).

<!-- /wp:paragraph -->

<!-- wp:video -->

<figure class="wp-block-video">
<video controls src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/08/Video-Guayabero.mp4">
</video>
  

<figcaption>
Video de una de las zonas de ataques del ejército

</figcaption>
</figure>
<!-- /wp:video -->

<!-- wp:heading {"level":3} -->

### Ejército habría amenazado a comunicador popular de Voces del Guayabero

<!-- /wp:heading -->

<!-- wp:paragraph -->

Una de la integrantes de ANZORC y un video difundido por redes sociales da cuenta de fuertes amenazas por parte del ejército en contra de las personas que desarrollan su trabajo como comunicadores.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Se trata de Fernando Montes Osorio quien denunció que el 7 de Agosto los militares que llegaron a la zona le decían "tranquilo que te vamos a coger y te vamos a partir en pedacitos. Tu eres el reportero pero de la guerrilla" relata el comunicador.

<!-- /wp:paragraph -->

<!-- wp:html -->

> Los hombres armados amenazaron a reporteros de prensa comunitaria "Voces del Guayabero, gritándole según información e los mismos campesinos “usted es el que está poniendo los explosivos, no se le haga raro cuando lo cojamos por ahí solo y vera que lo vamos a volver picadillo” [pic.twitter.com/0ZyeinHhZ3](https://t.co/0ZyeinHhZ3)
>
> — ANZORC- ZRC (@ANZORC\_OFICIAL) [August 8, 2020](https://twitter.com/ANZORC_OFICIAL/status/1292133591250620416?ref_src=twsrc%5Etfw)

<p>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
  
<!-- /wp:html -->

</p>
<!-- wp:heading {"level":3} -->

### Zona del Guayabero está en crisis por erradicación forzada desde hace más de dos meses

<!-- /wp:heading -->

<!-- wp:paragraph -->

Desde el pasado 2o de mayo cerca de mil personas han decidido manifestarse en contra de los operativos de erradicación forzada adelantados por el ejército, asi como las reiteradas amenazas y estigamitzaciones por parte de la misma fuerza pública.

<!-- /wp:paragraph -->

<!-- wp:video {"align":"center"} -->

<figure class="wp-block-video aligncenter">
<video controls src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/08/Denuncia-comunicador-voces-del-guayabero-1.mp4">
</video>
  

<figcaption>
Reportero "Voces del Guayabero" amenazado por el ejército

</figcaption>
</figure>
<!-- /wp:video -->

<!-- wp:paragraph -->

Según la comunidad se han convocado varios espacios de diálogo pero los compromisos de respeto por parte de las FFMM no se han cumplido y la defensoría del pueblo tampoco ha sido efectiva en su labor de proteger la vida de los campesinos y campesinas. [Lea también Asesinan a Johani Yeffer Vanegas lider de Guayabero](https://archivo.contagioradio.com/yohann-yeffer-vanegas-asesinado-guayabero/)

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Noticia en desarrollo...

<!-- /wp:paragraph -->
