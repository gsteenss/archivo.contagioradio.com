Title: Wajib, primera coproducción colombo-palestina se estrena hoy
Date: 2018-11-22 16:31
Author: AdminContagio
Category: 24 Cuadros
Tags: Cine, cine palestino
Slug: wajib-coproduccion-colombo-palestina
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/11/Wajib-e1542922027900-770x400.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Wajib 

##### 22 Nov 2018

Con la dirección de **Annemarie Jacir** y la colaboración de la cineasta colombiana **Cristina Gallego** (Pájaros de verano, El abrazo de la serpiente), llega a las salas de cine ***Wajib invitación de boda***, un drama romántico de una familia palestina-cristiana en la ciudad israelí de Nazaret.

El término Wajib, se refiere a “deber social” de los hombres de la familia de entregar las invitaciones para su boda de sus hijas. La cinta además de mostrarle al espectador diversos aspectos de las costumbres de una familia palestina y su relación con su entorno israelí, **habla de aspectos propios de cualquier familia en el mundo, como lo son las diferencias generaciones, políticas e ideológicas entre un padre y un hijo**.

<iframe src="https://www.youtube.com/embed/l8hiIPTPfzw" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

Los personajes principales son interpretados por los actores **Mohammad y Saleh Bakri**, padre e hijo en la vida real. Saleh es Shadi, un joven que regresa a casa después de varios años de vivir en Roma para acompañar a su padre, un maestro de escuela divorciado, a entregar las más de 300 invitaciones de la boda de su hija.

Padre e hijo hacen el recorrido y visita por las estrechas calles en un carro viejo, en el que comienzan una nueva relación mientras tratan de lidiar con las viejas tensiones. A**bu Shadi defiende la realidad política y social de aquellos palestinos que no se convirtieron en refugiados en 1948** y se quedaron en su tierra natal mientras Shadi se desespera por lo que ve como una comunidad apática que sufre una crisis de identidad.

La directora Annemarie Jacir, ha dirigido **16 películas,es considerada una de las 25 nuevas caras del cine independiente palestino **y es parte de la nueva ola árabe según Variety. Dos de sus películas se han estrenado como Official Selections en Cannes, una en Berlín y en Venecia, Locarno y Telluride, su cortometraje Como veinte imposibles (2003) fue el primer cortometraje árabe en la historia en ser una selección oficial del Festival de Cine de Cannes y continuó abriéndose camino cuando fue finalista de los Premios de la Academia.

###### Encuentre más información sobre Cine en [24 Cuadros ](https://archivo.contagioradio.com/programas/24-cuadros/)y los sábados a partir de las 11 a.m. en Contagio Radio 
