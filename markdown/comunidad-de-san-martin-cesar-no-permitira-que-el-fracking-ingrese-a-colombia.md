Title: Comunidad de San Martin, Cesar, no permitirá que el Fracking ingrese a Colombia
Date: 2016-04-13 17:51
Author: AdminContagio
Category: Nacional, Resistencias
Tags: cesar, fracking, san martin
Slug: comunidad-de-san-martin-cesar-no-permitira-que-el-fracking-ingrese-a-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/04/fracking-e1460586646301.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Burgos digital 

###### 13 Abr 2016 

“Queremos decirle al gobierno nacional que vamos a defender nuestra agua, nuestro territorio, vamos a defender la vida y no vamos a permitir que se realice francking en San Martín ni en ninguna parte del país, porque sabemos que sería la entrada del fracking a Colombia”, expresa Carlos Andrés Santiago, líder de la comunidad de San Martín, del departamento del Cesar, donde la Agencia Nacional de Hidrocarburos, ANH, ya firmó un contrato con dos multinacionales que llegan al país para implementar la técnica del fracturamiento hidráulico.

Se trata de las empresas **ConocoPhillips Colombia Ventures Ltd. y CNE Oil & Gas S.A** las que firmaron el primer contrato para la exploración y producción de hidrocarburos en yacimientos no convencionales el pasado 2 de diciembre.

La comunidad manifiesta que **las empresas engañaron a la población** pues en la primera socialización que hicieron sobre el proyecto se habló sobre fracturamiento hidráulico, pero cuando la comunidad empezó oponerse al proyecto “cambiaron de discurso, y ahora dicen que no se va a hacer realizar prácticas no convencionales (fracking)”, afirma Carlos Andrés quien asegura que ya lograron tener acceso al contrato firmado donde dice que se implementarán explotaciones no convencionales.

El Fracking  o fracturamiento hidráulico es una técnica mediante la cual se bombea una mezcla de agua y sustancias químicas en el suelo para poder romper formaciones rocosas y liberar el petróleo y el gas. **Esta técnica ha evidenciado graves impactos ambientales y en la salud de las personas que habitan cerca a los lugares donde se realiza.**

La ANH ha asegurado que en Colombia ya se cuenta con un marco regulatorio claro que supuestamente garantizaría que no haya afectaciones en los acuíferos y que indica que en caso de que haya un movimiento por encima de 3 grados Richter, se suspenderán las operaciones. Pero esto no es suficiente para la población, y sostienen que es una mentira del gobierno “**porque la legislación no es nada fuerte y las corporaciones no tiene la capacidad política, ni jurídica, ni financiera en caso de que haya una afectación ambiental”**, sostiene Santiago.

Esta técnica se realizaría en los municipios de San Martin, Agua Chica, Río Negro, Puerto Wilches  y otros lugares del Magdalena Medio donde se llevaría a cabo el proyecto denominado APE VME-3 con una inversión superior a los US\$85 millones. Según Carlos Santiago, se trataría de un  contrato “lesivo para los intereses del Estado”, pues el porcentaje que pagarán las empresas en regalías no es el 100% sino el 60% y la participación del Estado en el proyecto corresponde solo al 2%.

Además, de acuerdo con el líder de la comunidad de San Martín, existen dos condiciones para que haya regalías para el país. Las empresas **solo estarían obligadas a pagar cuando la producción supere 5 millones de barriles,** y sumado a eso, **el precio del petróleo deberá estar por encima de los 81 dólares**. De no cumplirse esas dos condiciones las empresas no estarán obligadas a pagar regalías al Estado.

Teniendo en cuenta lo anterior y los efectos que podría sufrir el ambiente, el agua y la salud, la población asegura que utilizarán todos los mecanismos, jurídicos, políticos, mediáticos y de movilización para detener la entrada del fracking al país.

Es por eso que **el próximo 17 de abril cerca de 5 mil personas de San Martín marcharán** para darle a conocer el gobierno nacional que a la comunidad no le interesa ningún tipo de acuerdo "porque sobre la defensa del agua y la vida no hay debe haber concertación. Esta no es una discusión de un municipio, es una discusión nacional, no vamos a cambiar el agua por el petróleo”, concluye Santiago.

##### <iframe src="http://co.ivoox.com/es/player_ej_11155562_2_1.html?data=kpael5qZepOhhpywj5aaaZS1lZeah5yncZOhhpywj5WRaZi3jpWah5yncaTV09Hc1ZClssXmhqigh6adt4zHwtPhy8bLs46ftMbbjbLFttXdz5DA18jWqY6ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe> 

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
