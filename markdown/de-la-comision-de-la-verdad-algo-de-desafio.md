Title: De la Comisión de la verdad, algo de desafío
Date: 2015-06-12 12:24
Category: Camilo, Opinion
Tags: ASFADDES, ASFAMIPAZ, camilo de las casas, Conpaz, Familiares Colombia, Fundación Nydia Erika, MOVICE, Proceso de paz en Colombia
Slug: de-la-comision-de-la-verdad-algo-de-desafio
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/06/justicia-no-impunidad-en-el-salvador-e1434130927765.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

#### Por [Camilo De Las Casas](https://archivo.contagioradio.com/camilo-de-las-casas/) 

###### 12 Jun 2015 

Aquí siguen vivas las verdades de los que se ufanan del heroismo, ídolos de barro que ocultan tras se sí, a los maquiavélicos horrendos planificadores y muchos de ellos beneficiarios de la violencia que se ha desarrollado después de nuestra independencia y que han usado desde hace 60 años, una vez más.

Esas verdades están cimentadas sobre la mentira que ideológicamente la  espiritualidad maniquea de la iglesia católica acuñó proscribiendo el humanismo social y el comunismo; esa misma fuerza ideológica alentó el paramilitarismo de los pájaros y de la policía; sobre ese oscurantismo, que hoy encarna Ordóñez, la doctrina de la seguridad nacional de la defensa de la democracia, el capitalismo y la fe se fue legitimando un marco legal para la creación de grupos armados de civiles y de operaciones encubiertas contra esas otras ideas o expresiones sociales y o políticas desde el 62.

Así nuestra historia ha estado signada con una verdad judicial que con aparente liberalidad y formalidad de independencia de poderes se ha cimentado sobre esa espiritualidad retrógrada, maniquea;  sus  avances liberales han ido quedando a merced de la arquitectura de la impunidad de empresarios, de sotanas, de militares y de políticos, que recurrentemente los medios de información replican por supuesto con contadas excepciones de independientes ante ese unanimismo mediático y cultural.  
La Comisión de Esclarecimiento y de Verdad (CEV) acordada, luego de un año ciclos, entre el gobierno y las FARC EP, es un avance con posibilidades.

No todo está definido sobre la CEV. Hay varios elementos para tener en cuenta. No va a haber una Comisión de la Verdad acordada con las FARC EP y otras oara el ELN. Si se instala esa mesa habrán nuevos aportes.  
Se expresa que la CEV es solo un elemento, uno dentro de una propuesta integral. Dicha propuesta integral no está definida, está en ciernes.

Hoy estamos con un diseño de CEV acordada entre enemigos de clase, si se quiere decir así, que no puede ni podía expresar terrorismo de Estado o Crímenes de Lesa Humanidad en el acuerdo. La expresión es más sutil y quedó consignada como crímenes masivos y estas expresiones están definiendo una materia, un cuerpo fundamental de casos.

Se trata de un acuerdo entre dos, no de una imposición y eso debe comprenderlo el movimiento de víctimas no para asentir sino para dar contextos y contenidos en sus valoraciones y su movilización.

La CEV en medio de unas guerrillas con perdida de legitimidad en amplios sectores por efecto de sus propios errores militares y politicos, también a veces, por una precaria manera de comunicarse con el país, y por supuesto por las operaciones mediáticas y psicológicas del conjunto de sus contradictores es un logro para el movimiento social que de sintonizarse con el fondo lo profundizara y logra generar una narrativa y una apuesta de memoria colectiva como ruptura histórica. La CEV abrirá escenarios de legitimación de apuestas transformantes en medio de una derrota ideológica ante la hegemonia del establecimiento.

La actitud de las FARC EP meses después de su discurso en Oslo ha ido creciendo en la legitimidad ética que le corresponde aunque no la sepa compartir. Ha respetado su decisión de no a la retención por razones extorsivas y en la práctica aplicada por otros motivos con los militares en acciones de guerra en medio de las conversaciones. Dio inició a una serie de ceses unilaterales temporales al fuego que cumplió, uno de ellos, incluyó al ELN e infortunadamente creyó en que un cese indefinido seria acogido por sus contradictores, pero no fue así, y se les devolvió en deslegitimación masiva.

Es la posición ética del revolucionario que se concibe como víctima pero también capaz de asumir su responsabilidad de una expresión diáfana con el acto de reconocimiento de responsabilidad de los 85 muertos en Bojayá por un error militar con una pipeta que cayó sobre civiles. Un avance que debería retomar el movimiento de las víctimas, las ONG en la lucha contra la impunidad y desde la memoria. En esa misma postura está el ELN.

Otro elemento clave de esa Comisión de la Verdad es que tendrá un carácter regional fundamental para el reconocimiento de las especificidades culturales y étnico territoriales, así como, la identificación de actores locales aún encubiertos y su interacción con actores nacionales e internacionales.

Si bien, habrá comparecencias y se asumirán responsabilidades al estilo de las decisiones del sistema interamericano (sin individualización y sanciones penales carcelarias) este podrá ser un mecanismo de mucha posibilidad de enunciación de la verdad y de proyección del derecho reparativo. No es todo a lo que las víctimas de Estado aspiran pero es una armazón sin terminar.

Los retos son mayúsculos para el MOVICE, CONPAZ, ASFADDES, Familiares Colombia, Fundación Nydia Erika y ASFAMIPAZ entre otras, mucho más cuando hay serias incertidumbres sobre la posibilidad real que el proceso termine con relativos avances sobre el conjunto de las 28 salvedades y las que ya existen sobre la mismas CEV.  
La pax santista es un mar de señales equívocas y de políticas en contravía de un país incluyente. No pretende limitar abusos militares y policiales ni busca promover la justicia socio ambiental ni penal.Esto puede dar paso no solamente al escepticismo, a la incredulidad en una mal llamada política de prosperidad sino a formas renovadas de autoritarismo, y de neoparamilitarismo, una sociedad que pasa de la desesperanza a la desesperanción que podría caer en el neofacismo por ignorancia y cansancio.

Solo el movimiento de víctimas con comprensión de lo que sucede, con una identificación de sus retos en el plano político,y con respuestas convergentes y creativas con Cumbre Agraria, Marcha Patriótica, Cumbre de los Pueblos, Comosoc, CONPAZ puede dar el sentido a la CEV en la creación de una narrativa y una comprensión critica del derecho a la verdad y de identificación de los factores y actores que han hecho una democracia genocida, puede contribuir a desatar los nudos gordianos del atascamiento en que vivimos por la ganada legitimidad de la verdad oficial.

La movilización y el agenciamiento colectivo en la conducción de una nueva sensibilidad sobre nuestro pasado y el proyecto de país que queremos y que desde ya construimos, nos abrirá a otra democracia social, política y ambiental como parte de otro país.

Un reto está a la vista en esa reivindicación de derechos en las elecciones de octubre derrotando el sectarismo siendo parte de la acción contra el Plan Nacional de Desarrollo, siendo sujetos en la construcción de una ley de víctimas real, siendo parte del movimiento animalista, de la lucha por los ríos y nuestros páramos y humedales. El problema no es de las FARC EP y del ELN lo que puedan lograr en los acuerdos es lo que es y lo suyo, y parte de lo nuestro; el asunto de la verdad y de la justicia es nuestro asunto con la deslegitimación de la sensibilidades y la verdad oficial.

Solo así los héroes, si de esa narrativa se trata en los términos oficiales, son aquellos que fueron asesinados,desaparecidos por construir otro país los que merecen nuestra honra en su humanidad y en sus apuestas por otro país, es con esas memorias exhumadas y actualizadas que podremos redefinir lo que esperamos de la CEV y lo que debemos construir como memoria colectiva en este armazón y en otras iniciativas por construir.
