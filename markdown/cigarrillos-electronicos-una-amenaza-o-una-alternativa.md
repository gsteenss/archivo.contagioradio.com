Title: Cigarrillos electrónicos: ¿una amenaza o una alternativa?
Date: 2016-04-15 14:15
Category: Nacional
Tags: Cigarrillo, colombia, OMS, Salud
Slug: cigarrillos-electronicos-una-amenaza-o-una-alternativa
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/04/Cigarrillo1-e1460746448324.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Contenido 

###### [14 Abr 2016]

Actualmente en Colombia se comercializa sin registro sanitario el cigarrillo electrónico, sobre el cual **existen varios mitos frente a si es o no realmente una buena alternativa para dejar de consumir el cigarrillo convencional,** que genera graves consecuencias en la salud y en el ambiente.

Sin embargo, de acuerdo con la Organización Mundial de la Salud, OMS, el cigarrillo electrónico también representaría un grave riesgo para los adolescentes y bebés en gestación, además hasta la fecha no se ha demostrado que se trate de un tratamiento terapéutico, por lo en agosto de 2014, la OMS recomendó prohibir la venta de cigarrillos electrónicos a menores.

Es por eso que el pasado 7 de abril en la Comisión Séptima del Senado, se llevó a cabo el foro “cigarrillos electrónicos: ¿una amenaza o una alternativa?" teniendo en cuenta que en el congreso cursa el  Proyecto de Ley 130 de 2015, **“Por medio de la cual se regula la comercialización, distribución, publicidad y promoción de sistemas electrónicos de administración de nicotina y cigarrillos electrónicos”.**

Allí participaron diferentes ponentes tanto del Estado, como de organizaciones ciudadanas expertas en el tema que respondieron varios interrogantes frente a los mitos que existen alrededor de este  mecanismo que se ha vendido como una posibilidad para dejar el cigarrillo tradicional.

Desde el Instituto Nacional de Salud, su directora, Martha Lucía Ospina, explicó algunas de las cifras más alarmantes frente a los comportamientos de uso del cigarrillo electrónico. **En América Latina el 7% de los menores 14 años de edad había probado cigarrillos electrónicos**. Así mismo explicó por qué no son un método terapéutico para dejar el cigarrillo convencional.

Carolina Wiesner, directora del Instituto Nacional de Cancerología, evidenció a partir de análisis de los cartuchos de cigarrillo, por qué este tipo de elementos podrían ser son cancerígenos y tóxicos para los seres humanos, según su composición química.

Este tipo de cigarrillos son sistemas electrónicos de inhalación de vapor de nicotina. **Hoy día estos sistemas no tienen ninguna regulación, y por lo tanto se venden libremente en el mercado.** El Instituto Nacional de Vigilancia de Medicamentos y Alimentos, INVIMA, no los ha avalado y durante el foro, de la voz de Javier Ospina se habló sobre las razones por las cuales en Colombia no se ha respaldado este producto y por lo cual en el 2010 se hizo un llamado de alerta sanitaria.

El representante del INVIMA, explicó que esa entidad no ha avalado el cigarrillo electrónico debido a cinco razones: la primera es que no hay evidencia científica de que realmente el cigarrillo electrónico no cause ningún daño; la segunda es que estos elementos no están estandarizados; por otro lado, se ha evidenciado que contienen ingredientes nocivos para la salud, incluidos la nicotina aunque sea en pocas proporciones; Además en otros países se encuentra prohibido; y la última razón para que el cigarrillo no sea avalado tiene que ver con que no existen controles de calidad, por lo cual el se considera que **no es un mecanismo con algún tipo de bondad terapéutica.**

Este sistema electrónico tampoco tiene restricciones en la oficina de estupefacientes ni del Ministerio de salud Protección Social, quien también hizo presencia en el foro con Lorena Calderón, ella, trajo a colación algunas cifras sobre la actual situación de este producto y el consumo en la población colombiana, por ejemplo que **el 15% de los colombianos mueren a causa del tabaco y el país gasta cerca de 4,2 billones de pesos atendiendo enfermedades causadas por fumar. **

A partir de ello, desde el Ministerio se demostró que se apoya la iniciativa contenida en el Proyecto  de Ley 130, pero además se cree necesario que se refuerce el proyecto prohibiéndose este producto en todo el país sin discriminación de edad.

Por su parte, Diana María Sáenz, Subdirectora de Gestión Técnica para la Atención a la Niñez y la Adolescencia. Evidenció graves cifras sobre la situación de los niños y niñas frente al consumo del tabaco, teniendo en cuenta que **actualmente Bienestar Familiar atiende 5700 niños, niñas y adolescentes por consumo de sustancias psicoactivas.**

Teniendo en cuenta los estudios científicos presentados y el grave riesgo que este producto significa para la salud y por lo tanto para la vida. Liliana Avila, quien representaba a la Veeduría Ciudadana Contra el Tabaco, quien explicó por qué debe reconocerse el tabaquismo como asunto de derechos humanos.

A su vez, Avila se refirió al Proyecto de Ley y al Convenio Marco de la OMS para el control del Tabaco, en base al cual debería desarrollarse la regulación de los sistemas electrónicos de inhalación de nicotina, como lo propone la Veeduría.

Otra de las exposiciones que evidenció el peligro de estos cigarrillos apoyando la postura del Ministerio de Salud de prohibirlos, fue la de Jaime Arcila, coordinador para América Latina de Corporate Accountability International quien dijo que lo que busca la industria tabacalera es transformar la cultura como ambiente favorable para ellos y explicó el **por qué ahora es tan importante el control del cigarrillo electrónico para la industria tabacalera.**

Finalmente, Mauricio Lizcano, senador y quien presentó este proyecto agradeció los aportes académicos y científicos para la construcción de esta Ley que atendería este nuevo riesgo para la salud pública de los colombianos y colombianas.

Durante el debate se evidenció que los cigarrillos electrónicos no son una alternativa para dejar de fumar. El 80% de las personas no necesitan otra sustancia para dejar el tabaco. Y al ser un problema de salud pública es también un asunto de derechos humanos, por lo que el **Estado debe tomar todas las medidas posibles para evitar que la población se vea afectada por esta nueva estrategia de la industria tabacalera** que busca nuevos consumidores.

<iframe src="http://co.ivoox.com/es/player_ej_11180938_2_1.html?data=kpaempWdd5mhhpywj5aZaZS1lpqah5yncZOhhpywj5WRaZi3jpWah5yncaTdyMbf1M7QsNDnjMrZx8jYtoa3lIqvldPNp9DnjIqwlIqmirbiwpDOz8rSpdvVjNSY19PFb8Lg1crf0MbYrY6ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
