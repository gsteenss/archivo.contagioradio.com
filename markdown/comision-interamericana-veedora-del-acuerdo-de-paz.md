Title: Comisión Interamericana de Derechos será veedora del Acuerdo de Paz
Date: 2016-12-02 12:53
Category: DDHH, Paz
Tags: Comisión Interamericana de Derechos Humanos, Implementación de Acuerdos, Mecanismos de implementación, Verificación de implementación
Slug: comision-interamericana-veedora-del-acuerdo-de-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/FIRMA-DEL-ACUERDO-DE-PAZ-5.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [1 Dic 2016] 

En las ultimas horas la Comisión Interamericana de Derechos Humanos manifestó mediante un comunicado su respaldo a la refrendación del nuevo Acuerdo de Paz en Colombia, en la misiva también reafirman su compromiso de **continuar monitoreando que la implementación del acuerdo se realice dentro del respeto y garantía a los estándares de derechos humanos.**

En el documento también se hace mención sobre la preocupante **“precariedad del cese al fuego”**, pues en el marco de refrendación y aprobación de los acuerdos, en las zonas rurales se ha agudizado progresivamente la violencia y “por ello **se hace urgente resolver el futuro de la implementación para la paz”.**

James Cavallaro presidente de la CIDH aseguró que “mantienen su convicción respecto a que la consolidación de la paz es un requisito indispensable para el ejercicio y respeto de los derechos humanos, **nuestro compromiso por la búsqueda de la paz en Colombia continúa".**

Por otra parte el Comisionado José de Jesús Orozco, destacó que "la implementación efectiva **debe satisfacer los estándares internacionales de verdad, justicia y reparación en el contexto de transición hacia la paz".**

[CIDH reafirma su apoyo al proceso de paz en Colombia y monitorea cumplimiento de estándares interamericano...](https://www.scribd.com/document/332939970/CIDH-reafirma-su-apoyo-al-proceso-de-paz-en-Colombia-y-monitorea-cumplimiento-de-esta-ndares-interamericanos#from_embed "View CIDH reafirma su apoyo al proceso de paz en Colombia y monitorea cumplimiento de estándares interamericanos on Scribd") by [Contagioradio](https://www.scribd.com/user/276652802/Contagioradio#from_embed "View Contagioradio's profile on Scribd") on Scribd

<iframe id="doc_95648" class="scribd_iframe_embed" src="https://www.scribd.com/embeds/332939970/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-oKFGffD6shMmdAWZwC7L&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="0.7729220222793488"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
