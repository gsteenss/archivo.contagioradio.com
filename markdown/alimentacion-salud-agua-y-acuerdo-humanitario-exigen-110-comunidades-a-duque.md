Title: Alimentación, salud, agua y acuerdo humanitario, exigen 110 comunidades a Duque
Date: 2020-04-09 18:52
Author: CtgAdm
Category: Actualidad, DDHH
Tags: #AcuerdoHumanitario, #IvánDuque, comunidades, covid19
Slug: alimentacion-salud-agua-y-acuerdo-humanitario-exigen-110-comunidades-a-duque
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/09/Iván-Duque-ante-la-ONU.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

Comunidades víctimas del conflicto armado, por segunda vez solicitan al presidente Iván Duque un Acuerdo Humanitario frente al riesgo por la pandemia de COVID19. Que en sus territorios se ha traducido al urgente suministro de agua potable, **alimentos, atención en salud y un diálogo con los distintos actores del conflicto**, pues les aquejan problemas como el paludismo, el dengue, gripas y desnutrición.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

La primera comunicación fue enviada el pasado 18 de marzo sin que hasta el momento se haya emitido una respuesta. (Le puede interesar: "[Comunidades del Bajo Atrato denuncian alza de precios y actividad bananera en medio de la cuarentena](https://archivo.contagioradio.com/comunidades-del-bajo-atrato-denuncian-alza-de-precios-y-actividad-bananera-en-medio-de-la-cuarentena/)")

<!-- /wp:paragraph -->

<!-- wp:heading -->

¡Como usted nosotros también tenemos familia!
---------------------------------------------

<!-- /wp:heading -->

<!-- wp:paragraph -->

En la misiva, las comunidades expresan al presidente que en los territorios hay más de **700 personas con síntomas de paludismo, dengue, gripas** y desnutrición. Situación que a la fecha ya cobró la vida de dos bebés indígenas.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

" Todos tenemos derecho a vivir. No estamos pidiendo limosnas. Estamos como hace **30 o 25 años pidiendo presencia integral del Estado**". Asimismo, exponen que, producto de la fuerte sequía que se vive en algunas regiones del país, o las lluvias, sus condiciones de vida se han visto afectadas.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

"El fuerte cambio climático expresado en intenso verano ha secado nuestras fuentes de agua, ríos y cañadas, nos tiene cerca de la hambruna en algunos territorios. En otras comunidades han arreciado las lluvias destruyendo viviendas, siembras. **En otras, logramos aún autosostenernos, pero no será por más de veinte días".**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

También señalan que en estos territorios "los agroindustriales siguen sus negocios, sin ningún tipo de contemplación, incluso pagando mucho menos el jornal". Hecho que ya había sido puesto en conocimiento del primer mandatario. (Le puede interesar:["Delicada situación en la comunidad Embera por confinamiento debido a operaciones armadas y crisis en salud"](https://www.justiciaypazcolombia.com/delicada-situacion-en-la-comunidad-embera-por-confinamiento-debido-a-operaciones-armadas-y-crisis-en-salud/))

<!-- /wp:paragraph -->

<!-- wp:heading -->

El conflicto armado sigue latente en medio de la cuarentena
-----------------------------------------------------------

<!-- /wp:heading -->

<!-- wp:paragraph -->

De acuerdo con las comunidades en sus territorios persiste el conflicto armado, producto de un olvido histórico que ha dejado miles de víctimas. Razón por la cual señalan que **"nos toca afrontar por nuestra vocación de paz a los que ejercen la violencia"**.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Situación que llevó a que estas comunidades hicieran una propuesta de Acuerdo Humanitario global Covid19 compartida a los distintos actores armados, sectores empresariales y al gobierno de Iván Duque.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Frente a los actores del conflicto, las comunidades manifiestan que "hasta ahora el ELN ha escuchado en su gran mayoría nuestra exigencia. Las FARC en Putumayo y Caquetá ha escuchado nuestro clamor. Algunos grupos de las AGC también" y continúan a la espera de regiones como el Catatumbo.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

No obstante, hasta el momento no ha sido respondida por el primer mandatario. Hecho que para las comunidades también desconoce la realidad que viven las y los integrantes de la Fuerza Pública que continúan en acción.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Según las comunidades, en medio de una crisis humanitaria como la que están viviendo con el covid 19, este acuerdo sería una garantía a la vida. Reiterando que **"se puede concretar si usted como máximo comandante toma una decisión coherente con el derecho humanitario".**

<!-- /wp:paragraph -->

<!-- wp:heading -->

Necesitamos su respuesta para enfrentar esta crisis
---------------------------------------------------

<!-- /wp:heading -->

<!-- wp:paragraph -->

En la carta las comunidades aseguran que este llamado que hacen al presidente es en procura del bien común. De igual forma expresan que se unen a los otros mensajes enviados por la población reclusa y " clamor de los empobrecidos en las grandes ciudades y municipios" que se están viendo afectados.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

"Es tiempo de cambiar la guerra por la paz, **la ambición por la solidaridad. La expansión del COVID19** por un contagio que pase del odio al amor y de la injusticia a la inclusión socio ambiental 2020" afirman.

<!-- /wp:paragraph -->
