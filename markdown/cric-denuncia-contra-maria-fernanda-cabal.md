Title: El CRIC tramitará denuncia formal contra María Fernanda Cabal
Date: 2019-09-11 11:56
Author: CtgAdm
Category: Comunidad, Política
Tags: CRIC, Desplazamiento, María Fernanda Cabal, policías, soldado
Slug: cric-denuncia-contra-maria-fernanda-cabal
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/09/FOTO-CRIC-e1568221622344.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/09/FOTO-CRIC-1.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/09/FOTO-CRIC-2.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/09/Diseño-sin-título.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/09/Diseño-sin-título-1.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:ContagioRadio] 

Los integrantes del **Consejo Regional Indígena del Cauca, CRIC**, denunciaron las acusaciones de María Fernanda Cabal, quién en una columna de opinión los señaló como los responsables de torturas, desplazamientos, atentados contra policías y soldados en sus territorios. Ante estas acusaciones anunciaron que estudian alternativas legales.

**Respuesta del CRIC**

Giovanni Yule, dinamizador político del Consejo Regional Indígena del Cauca (CRIC), en declaraciones a Contagio Radio, afirma que “en nuestro país, desafortunadamente, hay un discurso de violencia y ese discurso ha sido el que ha engendrado la guerra" y hacen un llamamiento a “los sectores sociales y populares para que nos juntemos contra estas aseveraciones de la derecha”.

Además, Yule anuncia que “los abogados están pensando en establecer los procesos jurídicos que correspondan, porque esas aseveraciones trasgreden la dignidad nuestra”. (Le puede interesar: [Pese a ataques contra el CRIC, Minga reitera su voluntad de diálogo](https://archivo.contagioradio.com/pese-a-ataques-contra-sedes-del-cric-la-minga-reitera-su-voluntad-de-dialogo/))

En esa misma línea, desde el CRIC se pide "no caer en estas injurias, en este lenguaje de la guerra, el conflicto y la xenofobia" y que en lugar de responder de la misma forma y con las mismas palabras de agresión “planteemos la forma de la unidad, y fortalecimiento de la democracia para que caminemos hacia la reconciliación”.

**Ataque a los indígenas del Cauca**

En el escrito, la senadora afirma que “el primer paso será derrumbar la violencia anarquista del CRIC y su Guardia Indígena, que los alejó de los parámetros comunes de autoridad”. No es la primera vez que Cabal emite este tipo de opiniones en sus redes sociales.

María Fernanda Cabal no es la primera política que realiza unas declaraciones que atacan a los indígenas del Cauca. El pasado mes de abril, la gobernadora del Magdalena, Rosa Cotes, hizo unas declaraciones racistas: "y mis indígenas, presidente, no son como los de la Minga. Aquí son aterrizados, inteligentes y preparados". (Le puede interesar: [En riesgo indígenas caucanos por persistencia paramilitar](https://archivo.contagioradio.com/la-onic-denuncia-la-aparicion-del-paramilitarismo-en-el-cauca-y-la-falta-de-acciones-estatales-en-la-defensa-de-la-poblacion-indigena-y-afrocolombiana/))

La comunidad indígena se encuentra en una situación de gran vulnerabilidad, con lo que ellos denominan un “genocidio sistemático”. Tan  solo unas horas después de las declaraciones de la senadora en su página web, en el departamento del Cauca hallaron muerto a Mario Alberto Achicué, un guardia indígena, con signos de violencia y se denunció que grupos armados reclutaron a seis menores indígenas más.

**Síguenos en Facebook:**  
<iframe id="audio_41569562" frameborder="0" allowfullscreen scrolling="no" height="200" style="border:1px solid #EEE; box-sizing:border-box; width:100%;" src="https://co.ivoox.com/es/player_ej_41569562_4_1.html?c1=ff6600"></iframe>  
<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
