Title: Una luz musical brilla por los niños del mundo
Date: 2018-03-14 13:15
Category: Cultura, El mundo
Tags: españa, Música, solidaridad
Slug: luz-musical-ninos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/03/Ana-Agraz-Luces-Solidarias_-150-1-1-e1521051098688.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Luces solidarias 

###### 14 Mar 2018

Siete jóvenes españoles, menores todos de 22 años, han emprendido de manera altruista la labor de ayudar a niños del mundo. Son los integrantes de Luces solidarias, un proyecto que por medio de la música han logrado recaudar dinero para la investigación del cáncer infantil.

La iniciativa, que busca además estimular a jóvenes para que emprendan y lideren proyectos altruistas, fue creada por Rocío Soler, una estudiante de piano y composición en el Liceu de Barcelona que en 2016 inició un voluntariado en el Hospital Sant Joan de Déu de Barcelona, de esa experiencia surgió la idea de promover este proyecto solidario.

[![luces](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/03/Foto-equipo-LS-800x533.jpg){.alignnone .size-medium .wp-image-52137 .aligncenter width="800" height="533"}](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/03/Foto-equipo-LS-e1521049062399.jpg)

Anualmente la organización realiza un concierto benéfico, desde el año 2016 han logrado recaudar más de 34.000 euros. Las presentaciones están enmarcadas en la música pop, con influencia en ocasiones de géneros como el rock, el Gospel, el jazz y el soul, con canciones propias y versiones de temas de agrupaciones como The Beatles, Michael Jackson, Prince, Stevie Wonder, Paul Simon, The Police, Adele entre otros, interpretadas por alumnos del Liceu y de la ESMUC y la colaboración de músicos invitados durante el acto.

<iframe src="https://player.vimeo.com/video/238896046" width="640" height="360" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

El próximo concierto, programado para el 14 de mayo en la sala BARTS de Barcelona, tiene como objetivo recaudar 35.000 euros destinados a la  construcción de un orfanato en Chockwé (Mozambique) para albergar a 150 niños huérfanos que viven en condiciones difíciles.

El trabajo de Luces solidarias apoyará a la Fundación NEPP, una entidad que lleva 10 años trabajando en Mozambique, en Guinea Ecuatorial y en el Campamento de Refugiados Saharauis (Argelia), lugares donde han construido hospitales Infantiles, hospitales psiquiátricos y escuelas. En el proyecto de Mozambique, llevan 5 años enviando dinero mensualmente para alimentar, vestir, pagar una sanidad y permitir una educación a los 150 niños huérfanos, muchos de ellos enfermos.

[www.lucesolidarias.com](http://www.lucesolidarias.com/)  
Instagram: @lucesolidarias  
Youtube y Facebook: Luces Solidarias
