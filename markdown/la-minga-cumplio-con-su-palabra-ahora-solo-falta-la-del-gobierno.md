Title: La Minga vive y cumplió con su palabra, ahora solo falta la del Gobierno
Date: 2019-04-08 20:24
Author: CtgAdm
Category: Comunidad, Nacional
Tags: CRIC, Minga Nacional por la Vida, ONIC
Slug: la-minga-cumplio-con-su-palabra-ahora-solo-falta-la-del-gobierno
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/D3hmql2WAAAyiyq.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/D3pGsn9WsAEtumB.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: @ONIC] 

En rueda de prensa, **el consejero mayor de la ONIC, Luis Fernando Árias**, señaló que ahora que la vía Panamericana se ha desbloqueado y se alcanzaran unos acuerdos parciales con el Gobierno, esperan que se establezca un dialogo político con el presidente Duque, además rechazaron las más recientes declaraciones de Álvaro Uribe con los que justifica las masacres como respuesta a la protesta social.

> **"La Minga Nacional no ha terminado, se han avivado los fogones de la resistencia"**

**Héctor Fabio Dicué, consejero del CRIC**, se refirió a tales pre acuerdos a los que se llegó en materia de DD.HH. humanos, territorio, ambiente, salud, educación, niñez y la creación del Fondo para el Buen Vivir de los Pueblos Indígenas; adelantó que ya fueron concertadas algunas cifras para los sectores movilizados, según el CRIC aunque la minga buscaba en el tema reivindicativo cuatro billones, la cifra acordada está cerca a los 832.148 millones.

Por su parte Luis Fernando Árias agregó que esperan poder avanzar en un dialogo político cuando se reúnan con el presidente Duque, sin embargo puntualizó que las acciones que pueda tomar la Minga a futuro dependerán de la reunión con el mandatario, aunque en concreto los acuerdos se darán a conocer después de la reunión con el presidente la agenda contará con cinco puntos:

1\. Defensa de la vida, la paz y los derechos humanos  
2. Defensa de los territorios  
3. Defensa de los derechos económicos, sociales, culturales y ambientales  
4. Acuerdos incumplidos con e Movimiento Social Colombiano  
5. Posicionamiento político y exigibilidad frente al Plan de Desarollo de 2018-2022

**Rechazamos el racismo antes, durante y después de la Minga**

Frente a los señalamientos de Álvaro Uribe, los líderes indígenas reiteraron su rechazo pues **"prácticamente está llamando a masacrar la Minga, nosotros responsabilizamos al senador Uribe por lo que pueda ocurrir a mingueros y mingueras"**, además, aseguró que ya se están revisando las acciones de carácter penal que puedan tomar ante estas declaraciones y que esperan que en medio del encuentro con el presidente Duque, este tome una postura clara frente al tema y se aparte de estos señalamientos. (Le puede interesar: [Continúan embates de Fuerza Pública contra campamentos de la Minga](https://archivo.contagioradio.com/cerro-tijeras/))

Frente a los sectores y ciudadanos que se vieron afectados por los bloqueos, los líderes indígenas agregaron que durante los 27 días que la Vía Panamericana fue tomada, existió un cordón humanitario  "como un gesto para paliar la situación de estas poblaciones", añadiendo que después de la firma de los acuerdos de paz, han sido asesinados 118 indígenas y que a veces es necesario "tomar este tipo de acciones para detener esta masacres" pues pueblos han sido **"bloqueados hace más de 500 años en la exclusión,  el extermino, la opresión y el racismo"**.

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
