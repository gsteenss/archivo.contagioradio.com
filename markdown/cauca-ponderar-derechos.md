Title: Cauca: ¿ponderar derechos?
Date: 2019-04-05 16:09
Author: JOHAN MENDOZA TORRES
Category: Johan Mendoza, Opinion
Tags: Cauca, Minga, Movilizaciones
Slug: cauca-ponderar-derechos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/56232221_1676961222406256_7904919769650823168_o.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto:Andrés Elias Gil 

No se trata de montar la dicotomía, derecho a la protesta versus el derecho a la movilidad ¡y listo! No se trata de exacerbar al pueblo oprimido solo cuando se subleva, pero ignorarlo, incumplirle y casi burlarse cuando ese mismo pueblo pide las cosas por las buenas. ¿Están cansados los señores “neoliberales” de tanta pedidera de la gente? sencillo, ¡no cobren más impuestos, suelten los cargos del Estado, saquen a las multinacionales y gritemos juntos, que viva el real libre mercado! Si no es así, entonces no balbuceen y recuerden que no todos nos alimentamos de ficción.

“En Colombia todo toca a los berracazos” dicen los más viejos de nuestra sociedad. No hay un camino hacia la justicia, si este no es develado a partir de su propósito, y el propósito de la justicia en Colombia no puede quedar radicado en el trámite de levantar o no un bloqueo.

Se trata de que los gobiernos no se burlen del derecho a la protesta mediante sus dos típicos mecanismos. El primero, ese radical institucionalismo que dice “primero el Estado, primero las leyes, primero todo y después la gente” ¡es que ni republicanos son! El segundo, el sentimentalismo como acción de propaganda, según el cual, desde la maquinaria mediática, represivamente nivelan los derechos que se les antoja para que la tele-radio-audiencia “elija”desde su posición individual “qué derecho es más importante”.

Error poner a ponderar derechos sin una evaluación histórica. El régimen quiere esclavos de un presente inconexo, pero las comunidades luchan día y noche, transmiten, transmitimos y seguiremos transmitiendo mediante el arte de la palabra (que no podrán detener), que preferimos ser una incomodidad, antes que esclavos de un presente pensado o sentido sin perspectiva de la injusticia histórica y sin detallar el tamaño de la barbarie cometida por diferentes gobiernos y sus reales manipuladores.

El problema del indio en Colombia tiene más de 100 años, el problema del indio nació antes que las carreteras, vio nacer incluso al capitalismo, pero también ha visto permanecer incrustado en la cultura ese racismo y ese feudalismo colonial que se niega a morir, ese elitismo payanés que si pudiera besar todos los días los pies del rey de España lo haría con “plena libertad”.

Aquí se trata de resolver el problema de evaluar equivalencias entre las cosmovisiones del  
liberalismo republicano, y la de los pueblos ancestrales. En Colombia había liberales de sepa… no sé cuantos queden, pero la política se llenó fue de gerentes que creen que un problema cultural y político se soluciona con la mera administración del problema. ¡Necios son y equivocados están! Parece que entre más títulos logran y entre más suben en el ranking, más se cierra su visión de mundo.

La protesta no es el problema, es solo el síntoma. El problema no es el gobierno, este es solo un alfil y buen siervo de las causas reales (por eso incumple). Las causas reales no se pueden dejar planteadas en estas escuetas líneas de opinión, pero sí se puede saber a posteriori el camino para su encuentro: senadoras de la republiqueta pidiendo la división del Cauca en castas. Financiadores de paramilitares que sin escrituras piden proteger la propiedad privada… ¡allí comienza el problema!

Queda claro que la gente tiene límites. El gobierno también se toma la frase y dice “todo tienen límites” pero eso lo dice porque habla desde su posición de poder. No obstante, la gente cuando reconoce que tiene límites, ha demostrado en Colombia una capacidad organizativa, de perseverancia, de persistencia, de resistencia impresionante y con comprobación histórica. Por ejemplo, ni con ayuda de Estados Unidos pudieron “solucionar” el problema de las familias campesinas organizadas como guerrillas en los años 50 ¿ahora creen que con ese juego vacío de ponderar derechos para restar apoyo a la protesta funcionará?

¿acaso no saben toda la mierda que tiene que comer la gente en Colombia como para pretender que se considere como más valioso que fluyan las mercancías del capital? No. No lo saben, porque creen que el problema del Cauca es un problema de “administración”.

La gente ya no creemos en la sindicación moralista del régimen mediático-político de Colombia, ya no creemos en las frases propaganderas de “tienen secuestrada una ciudad, secuestraron el departamento”. A esos medios en manos de los más poderosos, no les gusta ponderar la afectación, la muerte, el engaño, el incumplimiento, el asesinato, lrepresión y la injusticia que viven campesinos e indígenas en Colombia, frente a la gran tranquilidad y libertad que viven los financiadores o autores intelectuales de tanta muerte en las ciudades. ¿Qué son las vías de hecho sino la ampliación de la comprensión de la libertad para aquellos que tienen el cerebro del tamaño de una nuez?

Si la única manera para que el país hable de los indígenas y de los campesinos así sea “moralmente mal” es mediante el aumento de la presión social ¡pues que sea entonces la única manera! Eso no lo decidieron los pueblos que hoy protestan, eso lo decidieron los gobiernos con tanto incumplimiento.

Por ahora, las críticas de aquellos que viven ahogados en un presente que no quieren comprender mas allá de sus narices, las críticas de los que no evalúan las condiciones históricas del problema de la tierra no solo en el Cauca sino en Colombia, o aquellos que alientan en los foros universitarios, pero luego cantan a la par del régimen condenando hasta al que raya una pared o les da pánico escénico ante el primer gas del Esmad…. sencillamente les podemos decir: no ayuden tanto carajo.

**La construcción de una moral de lucha lleva años, muertos, transferencias orales, memoria, potencia colectiva. La construcción del moralismo por un bloqueo solo tarda 30 segundos de aceptable redacción en redes sociales. ¿Van a venir a ponderar? No señores, pretender la validez sin pretender la verdad es la estupidez propuesta por el régimen político; ante eso, ya los pueblos no caerán una vez más.**
