Title: Siguiendo los "Rastros de Camilo" en documental
Date: 2016-02-14 10:41
Category: Nacional, Sin Olvido
Tags: camilo torres, Conmemoración 50 años Camilo Torres
Slug: siguiendo-los-rastros-de-camilo-en-documental
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/02/Camilo-Torres.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Zona Cero 

###### [13 Feb 2016]

A partir de artículos de prensa, videos, archivos de audio, y entrevistas con diversas personas que de una u otra forma hicieron parte de la vida de Camilo Torres Restrepo, el director **Diego Briceño Orduz,** presenta a manera de documental **apartes de la vida del sacerdote revolucionario**.

**“Rastros de Camilo”**, es el título de la producción, estrenada en el marco de la conmemoración de los cincuenta años de la muerte de Camilo Torres, con el que sus realizadores y productores buscaron rendir homenaje a la vida y obra del sacerdote, teniendo en cuenta su influencia en el devenir político latinoamericano y revivir la incógnita sobre el paradero de sus restos mortales.

“Cuando me di cuenta que en Colombia, no podíamos realizar el amor al prójimo solamente con la caridad, sino que había que cambiar las estructuras políticas económicas y sociales del país, entonces entendí que el amor al prójimo estaba muy ligado a la revolución”, es una de los apartes de los discursos de Camilo que se presentan en el documental, en el que amigos, familiares e incluso militares hablan sobre el destino de los restos de Camilo y las preguntas que hay alrededor de este hecho**, ¿Por qué no aparecen? ¿Dónde están? ¿Por qué no quieren que se encuentren?**

“**Mi objetivo no era que la historia de Camilo llegara a los mismos de siempre, quería contar la historia para el común de los colombianos”**, dice el director del documental, quien asegura que no quisiera dejar inconclusas aquellas preguntas, él espera tener una segunda parte del documental cuando hayan sido encontrados los restos del sacerdote.

La jornada de conmemoración inició el viernes 12 de febrero en la Universidad Industrial de Santander, con la instalación del **Foro ‘Camilo Sacerdote Humanista y Revolucionario’**, y el lanzamiento del **documental ‘Rastros de Camilo’**. Este domingo continúan con un **acto ecuménico, místico y cultural** en Patio Cemento, lugar donde el “cura guerrillero” perdió la vida, aunque sigue vivo en los corazones de muchos y muchas, como lo aseguran los asistentes al evento.
