Title: ONU discute tratado para que empresas respeten derechos humanos
Date: 2015-07-02 16:46
Category: DDHH, Otra Mirada
Tags: Amazonía, Australia, Campaña Global para Desmantelar el Poder de las Trasnacionales, Canadá, CDHNU, Censat agua Viva, Cerrejón, Chevron, colombia, Consejo de Derechos Humanos de las Naciones Unidas, Derechos Humanos, Diana Aguiar, ONU, Shell, Unión europea, violación de DDHH
Slug: onu-discute-tratado-para-que-empresas-respeten-derechos-humanos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/07/marcha-chevron.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: [alejandrobodart.com.ar]

<iframe src="http://www.ivoox.com/player_ek_4715394_2_1.html?data=lZyel5ideI6ZmKiak5yJd6KnmJKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRiMrVz8aYo8zZrcLmhpewjajFsdHVhqigh6eVpYy7zdTPw9GPtMLmwpCxx9jRpc%2FoxtHO1JDJsIzE0MnS1JKJe6ShpNTb1sbLrdCfs8bRy9SPcYarpJKh&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Diana Aguiar, Campaña Global para Desmantelar el Poder de las Trasnacionales] 

El Consejo de Derechos Humanos de las Naciones Unidas (CDHNU) **analiza la adopción de una resolución para establecer un instrumento legal obligatorio**, a través del cual se exija a las corporaciones transnacionales (ETNs) cumplir con las normas en relación a los **derechos humanos.**

Son **190 organizaciones sociales de todo el mundo**, las que han promovido y presionado este tratado histórico vinculante en términos de Derechos Humanos, en el que se estipulan “reglas muy claras y se crean mecanismos para hacerlas cumplir”, teniendo en cuenta que “hoy en el Derecho Internacional Humanitario, solo hay obligaciones voluntarias y no hay mecanismos para que las empresas respeten los derechos de las comunidades”, explica **Diana Aguiar, quien hace parte de la Campaña Global para Desmantelar el Poder de las Trasnacionales.**

Desde hace 30 años, las organizaciones sociales vienen luchando para que la ONU apruebe este tratado, debido a las **continuas violaciones a los derechos humanos causadas por las ETNs** como el desastre de 2013 en Rana Plaza, Bangladesh, que acabó con la  vida de 1132 trabajadores; la masacre de 2012 de Marikana que mató a 34 mineros en Sudáfrica; la creciente destrucción causada por Shell en la región de Ogoni y por Chevron en la Amazonia ecuatoriana; así como las violaciones a los derechos humanos relacionadas con la mina de carbón en El Cerrejón, Colombia , entre otras.

Países como **EEUU, Canadá y Australia se han opuesto en bloque a la discusión y firma de un tratado en ese sentido**, al ser los países con las mayores empresas trasnacionales, en Latinoamerica o en África. Sin embargo, el convenio solo será vinculante para quién lo firma, aunque se espera ejercer presión sobre las naciones que no acepten hacer parte de este.

Aguiar, afirma que el propósito es que la **Unión Europea y otros gobiernos “paren la hipocresía de decir que están a favor de las comunidades**”, ya que no incluyen normas vinculantes sobre las empresas trasnacionales frente a los DDHH, lo que hace cómplices a los estados.  Se espera que con este tratado, salga a la luz la impunidad y violación de derechos desde las empresas.

La Campaña Global para Desmantelar el Poder de las Trasnacionales, invita a la ciudadanía a que haga parte de esta, y ejerzan presión para que el tratado sea un hecho. Los interesados en apoyar esta iniciativa pueden visitar la página [stopcorporateimpunity.org.](http://www.stopcorporateimpunity.org/)
