Title: Denuncian 2 nuevos 'Falsos positivos judiciales' en Córdoba
Date: 2015-10-20 15:30
Category: Judicial, Nacional
Tags: 'Clan Úsuga', ASCSUCOR, Asociación de Campesinos del Sur de Córdoba, Captura de Jesús María Herazo Redondo, Captura de Lidys Isabel Rivera Naranjo, Falsos Positivos Judiciales, Falsos positivos judiciales en Montería
Slug: denuncian-2-nuevos-falsos-positivos-judiciales-en-cordoba
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/10/Puerto-Libertador.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: El Universal 

###### [19 Oct 2015] 

[El pasado 1 de octubre sobre las 4 de la mañana fueron **capturados** el señor **Jesús María Herazo Redondo** **y** su esposa **Lidys Isabel Rivera Naranjo**, en la vereda La Mina, municipio de Puerto Libertador, Córdoba, **sindicados** junto con otros campesinos de la región **de pertenecer al**]**'**[**Clan Úsuga'**. La **Asociación de Campesinos del Sur de Córdoba ASCSUCOR**, denuncia que estas detenciones por parte de la Policía Judicial y el Ejército se tratan de **'Falsos positivos judiciales masivos'**.]

[De acuerdo con ASCSUCOR, **las comunidades campesinas del municipio “Dan fe de la honorabilidad e inocencia de los sindicados”**. La señora Lidys Rivera es madre de 3 hijos, responsable de 3 nietos, se ha dedicado “Desde siempre a oficios del hogar” y ha convivido desde hace muchos años con su esposo Jesús Herazo.  ]

[Este hecho se inscribe en las acciones adelantadas por **unidades de la Policía** quienes 2 meses atrás llegaron a las casas de los campesinos de la zona a exigirles sus **cédulas para tomarles fotografías**, con el pretexto de que se había extraviado una motocicleta en el sector. Lo que para ASCSUCOR “Deja en evidencia que ha sido una persecución selectiva coaccionada con **montajes judiciales** y testigos ajenos a la verdad”.   ]

[ASCSUCOR **rechaza las 50 capturas** que según datos oficiales de la Policía se han realizado, entre las que se destaca la del aspirante al concejo municipal Farli Eliecer Velásquez Patiño quien se encuentra recluido en la cárcel Las Mercedes de Montería, pues faltan a la verdad y a la justicia **“Los verdaderos dueños de los “alias” con que los sindican están en la zona, ejerciendo sus actos de terror y criminalidad”**.]
