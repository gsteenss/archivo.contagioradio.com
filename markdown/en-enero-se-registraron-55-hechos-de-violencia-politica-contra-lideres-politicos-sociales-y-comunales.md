Title: En enero se registraron 55 hechos de violencia política contra líderes políticos, sociales y comunales
Date: 2020-02-05 16:12
Author: CtgAdm
Category: DDHH, Nacional
Tags: Asesinatos, Lideres, MOE
Slug: en-enero-se-registraron-55-hechos-de-violencia-politica-contra-lideres-politicos-sociales-y-comunales
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/08/WhatsApp-Image-2018-08-06-at-3.26.09-PM.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

*Foto: Contagio Radio*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Según la Misión de Observación Electoral (MOE), en su informe más reciente informe **Violencia contra líderes políticos, sociales y comunales,** en los 5 años estudiados, enero de **2020 fue el mes con mayor número de ataques a líderes.** 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El informe da cuenta que el número de asesinatos de 2017 a 2020 se ha multiplicado casi cuatro veces en comparación con años pasados, "*En 2016 se registraron 5 asesinatos en el primer mes del año, en 2017 hubo 11, en 2018 se registraron 18 , en 2019* *16* * y **en 2020 el registro fue de 19"***. (Le puede interesar: <https://www.justiciaypazcolombia.com/por-que-las-victimas-piden-la-renuncia-de-dario-acevedo-como-director-del-cnmh/>)

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Asimismo evidenció que "***55 hechos de violencia política contra líderes políticos, sociales y comunales, siendo los líderes sociales los más afectados (35 hechos)***" MOE (Le puede interesar: <https://archivo.contagioradio.com/para-proteger-a-los-lideres-hay-que-pasar-de-voluntad-a-hechos-diakonia/>)

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Violencia electoral y postelectoral

<!-- /wp:heading -->

<!-- wp:paragraph -->

La MOE destacó también que en los últimos 5 años , especialmente en el periodo electoral del 2019, **549 líderes políticos, sociales y comunales fueron víctimas de violencia, de los cuales 125 fueron asesinados**, **pese a las denuncias y solicitudes de garantías al estado.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

De estos tan solo 142 fueron víctimas de violencia en el periodo entre julio y octubre, 116 eran hombres y 26 mujeres, de estos candidatos *"**22 resultaron electos y llegaron a ejercer su cargo público siendo víctimas de hechos de violencia política**, 20 de ellos a la alcaldía y dos a la Gobernación"* señaló la MOE.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por otro lado la Misión afirmó que las agresiones se presentaron en 205 municipios y en 29 departamentos, en donde l**os únicos que no registraron víctimas fueron Guainía, Vichada y Vaupés**.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

De igual forma el informe reflejó las categoría más comunes hacia los líderes: El primero son las **amenaza**, que se representan como amedrentamientos que buscan desincentivar la participación posturas o decisiones, a este lo siguen el **secuestro**, **desaparición forzada**, **atentados** *(acción violenta que busca acabar la vida o dañar la integridad física o moral de un líder o lideresa*), y por último el **asesinato**, que según el informe es la *"acción que causa gran impacto sobre los procesos de representación y acción colectiva".*

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### ¿Cómo proteger a los líderes lideras del país?

<!-- /wp:heading -->

<!-- wp:paragraph -->

Uno de los puntos importantes y que podría salvar la vida de los representantes y voceros de la sociales en Colombia es, según la MOE, la realización desde ya, **de reuniones previas con los consejos de seguridad departamentales y municipales, en donde se tome en cuenta la participación de representantes de organizaciones sociales y de las Juntas de Acción Local.** Esto con el fin de conocer las problemáticas de seguridad a las que se enfrentan las comunidades y lo liderazgos.

<!-- /wp:paragraph -->
