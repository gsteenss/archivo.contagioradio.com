Title: Desde enero hasta agosto de 2017 han sido asesinados ciento un líderes sociales en Colombia
Date: 2017-08-25 16:46
Category: DDHH, Entrevistas
Tags: Asesinatos defensores DD.HH, marcha patriotica
Slug: entre-enero-y-agosto-han-sido-asesinados-ciento-un-lideres-sociales
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/FIRMA-DEL-ACUERDO-DE-PAZ-4-e1504831395382.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [25 Ago 2017] 

La plataforma política Marcha Patriótica y la organización Indepaz dieron a conocer su último informe sobre el estado de derechos humanos en Colombia, que reveló que en lo corrido de enero hasta agosto de 2017, **101 líderes sociales y defensores han sido asesinados, 194 han recibido amenazas y se reportaron 484 violaciones a los derechos humanos**.

### **¿Quiénes son los líderes y defensores asesinados?** 

De acuerdo con la recolección de información hecha por Marcha Patriótica, el 88% de los casos de agresiones a defensores de derechos humanos y líderes sociales son contra la población masculina, **el 16% contra mujeres, el 1% a la comunidad LGTBI y el 3% se dirige a organizaciones sociales**. (Le puede interesar: ["En Antioquia se han presentado 111 agresiones contra defensores de DD.HH"](https://archivo.contagioradio.com/en-antioquia-se-han-presentando-111agresiones-contra-defensores-de-dd-hh/))

Además, se pudo establecer que el 92% de las agresiones se realizan en contra de comunidades y que los departamentos en donde más se reportan estos actos de violencia son **Cauca con 61 amenazas, 29 asesinatos** y 5 atentados; Chocó con 49 amenazas y 16 atentados; Valle con 27 amenazas y 20 asesinatos; y Antioquia con 11 amenazas y 17 asesinatos.

### **El paramilitarismo y el control en el territorio** 

El informe señaló que hay una fuerte preocupación expresada por las comunidades frente a la **presencia de grupos armados ilegales en los departamentos del Cauca, Chocó y Antioquia** y la falta de acciones por parte del Estado para combatirlas. (Le puede interesar[:"Paramilitares asesinan a dos integrantes de la comunidad Wounnan"](https://archivo.contagioradio.com/paramilitares-asesinan-a-dos-intengrantes-de-la-comunidad-wounnan/))

En este sentido el informé asegura que el 19% de las violaciones a derechos humanos se han podido atribuir a las autodenominadas Autodefensas Gaitanistas de Colombia, el 3% a las Autodefensas Campesinas de Colombia, el 2% a Los Urabeños, mientras que hay un **70% de violaciones a derechos humanos que no se han atribuido a ningún grupo en específico.**

### **La Fuerza Pública tiene responsabilidad en varias violaciones de DD.HH** 

El informe, también expone las violaciones a los derechos humanos cometidas por miembros de la Fuerza Pública, evidenciando que los departamentos en donde más casos se han registrado son el Valle del Cauca, **específicamente durante el paro de Buenaventura y en Bolívar, con las capturas a líderes del movimiento social**.

### **¿Continúa la violencia contra los líderes y defensores de derechos humanos?** 

Una de las conclusiones más importantes que arroja este informe consiste en afirmar que **sí hay sistematicidad y generalidad en la violación a los derechos humanos en Colombia, al tener un número significativo de víctimas,** pertenecientes a grupos con características similares y sucedidas en un mismo periodo y espacio geográfico, por actores determinados

En esa medida Cristian Delgado integrante de la Comisión de Derecos Humanos de Marcha Patriótica, señaló que una de las medidas más importantes que se deben tomar por parte del Estado, es el reconocimiento de las estructuras paramilitares para en ese mismo sentido desmontarlas y manifestó que a falta de medidas de seguridad y protección “las comunidades se han organizado en las guardias indígenas y campesinas” para protegerse.

<iframe id="audio_20526040" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_20526040_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
