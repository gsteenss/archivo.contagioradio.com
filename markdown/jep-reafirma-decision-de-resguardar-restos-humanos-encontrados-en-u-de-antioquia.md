Title: JEP reafirma decisión de resguardar restos humanos encontrados en U de Antioquia
Date: 2020-04-22 13:28
Author: CtgAdm
Category: Actualidad, DDHH
Tags: Búsqueda de personas dadas por desaparecidas, Hidroituango
Slug: jep-reafirma-decision-de-resguardar-restos-humanos-encontrados-en-u-de-antioquia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/04/JEP.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: JEP

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

La [Jurisdicción Especial para la Paz](https://twitter.com/JEP_Colombia)(JEP), rechazó la solicitud de aclaración presentada por la Universidad de Antioquia, sobre las medidas cautelares solicitadas por el Movimiento Nacional de Víctimas de Crímenes de Estado (MOVICE) y decretadas sobre el sellamiento temporal y parcial de los laboratorios de Osteología Antropológica y Forense de esa institución para preservar al menos 185 cuerpos de personas víctimas de desaparición forzada en el marco del conflicto armado y que estarían en zona de influencia del proyecto hidroeléctrico de Hidroituango.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

La Sección de Ausencia de Reconocimiento de Verdad y Responsabilidad reafirmó que esas medidas cautelares, tomadas desde noviembre de 2019 y que fueron extendidas desde marzo del 2020, buscan garantizar el posible hallazgo, identificación y entrega digna de los cuerpos de víctimas del crimen de desaparición forzada en el proceso que, a su vez, adelanta la Unidad de Búsqueda de Personas Desaparecidas (UBPD). [(Le puede interesar: Víctimas de la Comuna 13 no serían 100 sino 450 según datos de JEP)](https://archivo.contagioradio.com/victimas-de-la-comuna-13-no-serian-100-sino-450-segun-datos-de-jep/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Cabe señalar que la JEP vinculó de manera formal a Empresas Públicas de Medellín (EPM) en el proceso que adelanta en relación a las personas dadas por desaparecidas en el marco del conflicto armado en zona de influencia del proyecto Hidroituango y que llegaron a la institución universitaria desde los cementerios **El Universal de Medellín y de cementerios comunitarios de los municipios de Sabanalarga, Barbacoas y Peque.**

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Protegemos los derechos de familiares de desaparecidos: JEP a U de Antioquia

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Sobre la solicitud de aclaración presentada por la Universidad de Antioquia, esta pedía ser informada sobre el tipo de vinculación al proceso que se adelanta para estudiar medidas de protección en 17 lugares donde habría cuerpos de personas dadas por desaparecidas.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Según la Universidad de Antioquia, la Ley Estatutaria de la JEP prohíbe que intervenga en el proceso penal y se considera la institución de educación superior como un sujeto procesal en la Jurisdicción, al respecto, el organismo judicial aclaró que la solicitud era extemporánea y reiteró que cuenta con plena competencia para vincular, requerir e imponer medidas cautelares que afecten a terceros, priorizando los derechos de las víctimas.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

«Esperamos que la JEP resguarde estos lugares para encontrar a los desaparecidos, tenemos esperanza con que esta audiencia va a resignificar el sentido de la muerte en el Cañon del Río Cauca» , había declarado al respecto en ocasiones anteriores la lideresa ambiental Isabel Cristina Zuleta sobre un proceso que viene siendo abordado desde 2010 ante organismos de derechos humanos, denunciando la ineficiencia de organismos como la Fiscalía en la búsqueda de personas dadas por desaparecidas y la protección de los sitios donde podrían estar ubicadas. [(Le puede interesar: Ante la JEP buscan reconocimiento de los desaparecidos en HidroItuango)](https://archivo.contagioradio.com/audiencia-ante-la-jep-por-desaparecidos-hidroituango/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

El auto también permitió conocer que al menos tres de los cuerpos provenientes del cementerio de Orobajo, exhumados como parte de un convenio con Integral S.A, empresa de gestión social de Hidroituango, presentan posible causa de muerte violenta como consecuencia de disparos con arma de fuego. **Además, 26 de los 185 cuerpos que se encuentran en las instalaciones, estarían en un mal estado de conservación, mientras otros 16 estarían en un pésimo estado, según información aportado por la Unidad de Investigación y Acusación** -.[(Le puede interesar: Medidas cautelares priorizan a víctimas y ambiente sobre Hidroituango)](https://archivo.contagioradio.com/medidas-cautelares-priorizan-a-victimas-y-ambiente-sobre-hidroituango/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

La JEP ha advertido que aunque EPM ha remitido documentación, no se ha "entregado información relativa a las actividades de búsqueda, prospección y exhumaciones realizadas, con ocasión del contrato celebrado entre la Universidad de Antioquia, la empresa Integral S.A y EPM.

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
