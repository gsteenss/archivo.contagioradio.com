Title: Los enredos de los funcionarios que están detrás de la urbanización de la Van Der Hammen
Date: 2016-03-28 16:32
Category: Nacional
Tags: Reserva Forestal Thomas Van der Hammen, Secretaría de Ambiente, secretaria de planeación
Slug: los-enredos-de-los-funcionarios-que-estan-detras-de-la-urbanizacion-de-la-van-der-hammen
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/01/Reserva-Tomas-Van-der-Hammen-e1457630338748.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Secretaría de Ambiente 

###### [28 Mar 2016] 

La posible urbanización de la Reserva Tomas Van Der Hammen está en manos de Francisco Cruz Prada y Andrés Ortíz, secretarios de Ambiente y Planeación de la actual administración Peñalosa, quienes buscan construir 80.000 viviendas en 1.200 de las 1.400 hectáreas que conforman la Reserva.

Los secretarios de Ambiente y Planeación que están a la espera de una respuesta por parte de la Corporación Autónoma Regional de Cundinamarca frente a la modificación de los lineamientos de la Reserva que permitirían llevar a cabo el proyecto Ciudad Paz, llevan a cuestas algunos asuntos que harían dudar de la transparencia en sus funciones como servidores públicos.

**El Secretario Ambiente:**

La Contraloría, Procuraduría y Fiscalía adelantan investigaciones contra Francisco José Cruz Prada, por un presunto detrimento patrimonial de aproximadamente \$50 mil millones en la construcción del Aeropuerto de Palestina, cuando el funcionario se desempeñaba como Gerente de Aerocafé, .

De acuerdo con la Silla Vacía, por investigaciones de la Corporación Cívica de Caldas y el periódico La Patria, se logró demostrar que los consorcios que se ganaron los contratos eran los mismos que estaban haciendo las interventorías de la obra, además, se encontró sobrecostos, como lo anunciaron en su momento. “Los trabajos, avaluados en casi 120 mil millones, quedaron en manos de un grupo de constructores que tienen negocios con sus interventores. Hay miembros que, por fuera de las obras de Aerocafé, son socios hace 13 años”, dice la nota.

Por esas irregularidades, la excontralora Sandra Moreli, imputó cargos a 21 personas, entre ellos Cruz. Por tal circunstancia la Corporación Cívica de Caldas, aseguró que le sorprendió que Cruz Prada hubiera sido nombrado como Secretario de Ambiente de la capital. “El Doctor Francisco José Cruz Prada, no está condenado y como no está condenado, está habilitado para ocupar un cargo público, pero uno si lamenta que una persona tan cuestionada en lo local, sea premiada con un cargo público y máxime con una secretaría en la ciudad capital de un país”, explicó Adriana Villegas, quien agregó que uno si esperaría que para esas responsabilidades, lleguen personas sin ninguna tacha de orden legal, ni ética.

**Secretario de Planeación**

De no ser por la investigación del periódico El Tiempo, no se hubiese hecho público que Andrés Ortíz, actual secretario de planeación, tiene una propiedad de 2 mil metros cuadrados en la Reserva Thomas Van Der Hammen; pese a ello anunció haber interpuesto una acción para que se analice si es o no impedido para llevar a cabo el proyecto, aunque ha afirmado que “no cree tener impedimento ético o legal” por ese tema.

“No soy yo quien lidera la solicitud de cambio de uso de la Reserva que se haría ante la CAR. El proceso está a cargo de la secretaría de Medio Ambiente, por su condición de autoridad en la materia”, aseguró Ortiz.

El secretario ha liderado el proyecto de urbanización de la Reserva y es, quien junto a la Secretaría de Ambiente del Distrito, tiene lista una solicitud oficial para que la CAR decida si se puede o no construir sobre ésta.

##### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)]
