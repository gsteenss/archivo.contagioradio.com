Title: Minería en Salento, Quindío, amenaza el árbol nacional de Colombia
Date: 2016-05-04 15:35
Category: Ambiente, Nacional
Tags: Anglo Gold Ashanti, loro orejiamarrillo, Mineria, palma de cera, Qundío, Salento
Slug: mineria-en-salento-quindio-amenaza-el-arbol-nacional-de-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/05/Valle-del-cocora.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: onglobaltrails]

###### [4 May 2016] 

AngloGold Ashanti está detrás de la concesión minera en el municipio de Salento, del departamento de Quindío, poniendo **en riesgo la palma de cera, símbolo nacional de Colombia que hace parte del Valle de Cocora**, y que a su vez este constituye un segmento del  Parque Nacional Natural Los Nevados.

Son tres las concesiones para megaminería  que se encuentran en curso en ese municipio, cuyo **98% del territorio se encuentra bajo algún tipo de protección ambiental,** el 87% es Distrito Regional de Manejo Integrado, es decir, que esa zona hace parte de una estrategia de conservación y utilización sostenible de la biodiversidad, para un total de más de 37 mil hectáreas que se encuentran en las categorías de Áreas Forestales Protectoras y Reservas Naturales de la Sociedad Civil.

“**Menos del 2% no tiene alguna declaratoria de protección ambiental. Salento provee de agua a Circasia en un 70%, también a Armenia, Calarcá, La Tebaida, y se surte así mismo**, es decir, es un productor hídrico del departamento del Quindío”, explica Yessica Herrera, personera del municipio, quien tiene la certeza de que una de esas concesiones contempla 3.182 hectáreas.

Fue entre febrero y abril de este año cuando de parte de la Agencia Nacional de Minería llegó la notificación anunciando que esos territorios se encuentran en estudio y evaluación para concesión minera. Una de ellas sería para AngloGold Ashanti, pues **esta zona del departamento de Quindío haría parte del proyecto La Colosa, que también se piensa adelantar en Tolima, pese a la negativa de la población.**

De acuerdo con la personera, los estudios técnicos efectuados por la ANLA determinaron que las áreas de estas solicitudes no se encuentran superpuestas a ningún área excluida de la minería, "lo que no es más que una evidencia de lo que se pretende realizar, invisibilizar lo obvio y no considerar la importancia del municipio de Salento como patrimonio ambiental, cultural y arqueológico de Colombia, pretendiendo hacerlo parte del Proyecto Regional de la Colosa y entregarlo sin mayor reparo a la multinacional Anglo Gold Ashanti", señala Herrera.

“En el Valle de Cocora tenemos la palma de cera, que es el árbol nacional, hay una ley que la protege, que prohíbe la tala, pero no nos pueden decir que con la minería no van a talar palma”, expresa Yessica Herrera, quien asegura que no se han  puesto consideración los impactos ambientales, la afectación a la identidad salentina, el desplazamiento ambiental forzado, generando además **implicaciones económicas pues se causaría un gran impacto en el turismo paisajístico.**

Los animales sería una de las primeras víctimas de esta explotación minera, pues en la palma de cera habita **el Loro orejiamarillo, que actualmente es una de las especies de loros más amenazadas del mundo** debido principalmente a la destrucción de su hábitat y la tala de la palma de cera donde construye sus nidos y de la cual se alimenta.

Frente a esa situación, desde la personería junto a la Mesa Ciudadana y el alcalde del municipio entre otros funcionarios que han demostrado su rechazo a la actividad minera en  Salento, se adelanta una recolección de firmas a través del portal [Change.org,](https://www.change.org/p/detengan-la-megaminer%C3%ADa-en-salento?recruiter=535386176&utm_source=share_petition&utm_medium=facebook&utm_campaign=share_for_starters_page&utm_term=des-lg-no_src-no_msg&fb_ref=Default) para exigirle al gobierno nacional y a las autoridades ambientales que detengan este proyecto minero. Así mismo, los pobladores alistan diversas acciones jurídicas y movilizaciones, entre ellas la del **próximo 19 y 20 de mayo, en torno a la responsabilidad social por la defensa de la vida y la tierra.**

<iframe src="http://co.ivoox.com/es/player_ej_11411100_2_1.html?data=kpahk5aVdJGhhpywj5aXaZS1lZyah5yncZOhhpywj5WRaZi3jpWah5yncbrZ1NjWxcaPjMbm08rfw4qWh4zExtfg0dPJtsKftMbZx9PYs46ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>\

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
