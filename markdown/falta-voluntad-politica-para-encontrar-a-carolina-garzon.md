Title: Falta voluntad política para encontrar a Carolina Garzón
Date: 2015-02-05 18:41
Author: CtgAdm
Category: Hablemos alguito
Tags: Carolina Garzon desparecida Ecuador, Desaparición forzada Colombia, Desaparición forzada Ecuador, Mane Colombia
Slug: falta-voluntad-politica-para-encontrar-a-carolina-garzon
Status: published

###### Foto:Metroecuador.com 

<iframe src="http://www.ivoox.com/player_ek_4043986_2_1.html?data=lZWhlZ6ceo6ZmKial5mJd6KokZKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRh8Lm0NHW0MaPi8Lm24qwlYqmd8%2Bfxcrg0sbWqcTdxcaah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### **[Entrevista a Walter Garzón y Jennifer Mosquera:]** 

**Stephany Carolina Garzón Ardila**, **desaparecida** el día Sábado 28 de Abril de 2012 a las 14:OO aproximadamente por la cercanía de su vivienda ubicada en la calle principal, cuarta escalinata, barrio Paluco (A un costado de la Av. Rumiñahui entre el trébol y el puente del Barrio Orquídeas) **Ciudad de Quito - Ecuador**. Desde este día no se tienen conocimiento de su paradero.

El día Sábado 28 de Abril, en las horas de la tarde alrededor de las 14:00 Carolina , indicó a sus compañeros que iría de visita al museo de arte contemporáneo saliendo de su casa ubicada en la calle principal 4 escalinata  barrio Paluco (a un costado de la Av. Rumiñahui entre el trébol y el puente del Barrio Orquídeas), sorprendiendo a sus compañeros de vivienda que  Carolina dejara todas sus pertenecías en casa (billetera, documentos, cámara fotográfica, libreta de apuntes y bolso), siendo estos elementos que siempre llevaba consigo.

Carolina Garzón Ardila  es estudiante de licenciatura básica con énfasis en educación artística en la Universidad Distrital Francisco José de Caldas – Bogotá/Colombia, se desempeña como **periodista del periódico EL MACARENAZO** de la misma Universidad y es militante y **dirigente del partido socialista de los trabajadores “PST”** en la ciudad de Bogotá-Colombia desde el 1 de mayo del 2007, siendo la encargada de la dirección de la juventud zonal de partido Regional Bogotá; en coordinación con organizaciones como el **MANE (Mesa Amplia Nacional Estudiantil), MOVICE (Movimiento de víctimas de crímenes de Estado)** ha participado de diversos espacios de defensa y acompañamiento a víctimas de crímenes de Estado, procesos de Falsos Positivos y Congreso de los pueblos entre otros. Producto de estos hechos Stephany Carolina Garzón Ardila  **ha sido señalada y estigmatizada en varias ocasiones**.

Como una medida de protección  Carolina Garzón  decide viajar  a Ecuador, llegando el día 19 de marzo del presente año, desde esta fecha se radica temporalmente en la ciudad de Quito (calle principal, cuarta escalinata, barrio Paluco, a un costado de la Av. Rumiñahui entre el trébol y el puente del Barrio Orquídeas), con el ánimo de retornar a Colombia en el mes de Agosto para dar continuidad a sus estudios Universitarios y compromisos socio político. Durante su estadía en Quito, junto con otros jóvenes realizaron viajes a ciudades como Ambato, Baños, Riobamba.

Como medio de sustento trabajaba en la venta de artesanías y chocolates en diferentes zonas de la ciudad, de manera especial en la Pontificia Universidad Católica del Ecuador y los días jueves, viernes o sábado realizaba actos de teatro callejero en la Calle de La Ronda.

A la fecha, amigos y familiares de Stephany Carolina Garzón Ardila  han realizado las siguientes diligencias con el ánimo de dar con su paradero: Visita de centros médicos, hospitales y estaciones de policía; recorridos por el barrio y zonas que habitualmente frecuentaba Carolina.

El día miércoles 2 de Mayo de 2012 se interpone denuncia por desaparición ante la Fiscalía Provincial de Pichincha, expediente fiscal 714-2012 – 10007-AA-DP-CAS, siendo designado el Cabo de la Policía Freddy Anchaluiza para la realización de las investigaciones pertinentes.

Con fecha 4 de Mayo de 2012 el Cabo de la Policía Freddy Anchaluiza tomo versión a los compañeros de vivienda de  Carolina Garzón y se dirigió a la residencia para la respectiva inspección del lugar.

Para entender mejor la envergadura de la investigación que están llevando a cabo familiares, amigos y solidarios entrevistamos **a Walter Garzón, padre de Carolina y a Jennifer Mosquera, abogada que lleva el caso .**

Ellos nos describen las reiteradas trabas y **la falta de voluntad política de los gobiernos de Ecuador y Colombia en la investigación**, e incluso en la propia coordinación entre las justicias de ambos países.

También escuchamos de la voz del padre la descripción de toda la investigación y del porque se llega a la conclusión de que es una desaparición forzada. Por último descubrimos que en el país andino existe una gran red de trata de personas , donde han sido desaparecidos  ciudadanos de toda Suramérica.
