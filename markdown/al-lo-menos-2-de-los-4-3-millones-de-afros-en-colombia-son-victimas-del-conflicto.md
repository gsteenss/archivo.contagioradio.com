Title: Al menos 2 de los 4.3 millones de afros en Colombia son víctimas del conflicto
Date: 2015-10-09 12:29
Category: Nacional, Paz
Tags: AFRODES, CONPA, Diálogos de paz en Colombia, Diálogos de paz en la Habana, Foro Nacional "Proceso de paz y los afro-colombianos", Marino Córdoba AFRODES, Pueblos afrocolombianos, Radio de derechos humanos, Víctimas del conflicto en Colombia
Slug: al-lo-menos-2-de-los-4-3-millones-de-afros-en-colombia-son-victimas-del-conflicto
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/10/Foro-Nacional.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:www.youtube.com] 

###### <iframe src="http://www.ivoox.com/player_ek_8890676_2_1.html?data=mZ2mkpubeo6ZmKiak52Jd6KokpKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRhc2fzsrb0diPdozYxpDZ0diPeI%2BnjNLWztHTssbnjMnSjcbKttDnjMrbjajTsNDhw87OjdjTso6ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe> [Marino Córdoba, AFRODES] 

###### [9 Oct 2015] 

[El pasado 5 de octubre en la ciudad de Bogotá se realizó el Foro Nacional “Proceso de Paz y los Afro-Colombianos”, para exigir que las propuestas colectivas de estos pueblos sean articuladas al proceso de negociación en La Habana, debido a que **de los 7 millones de víctimas del conflicto armado en Colombia más de 2 millones son afro-descendientes**, es decir, un 35%.]

[Este foro convocado por el Consejo Nacional de Paz Afrocolombiano, CONPA que contó con la participación del sindicato afro de EEUU y uno de sus congresistas, llamó la atención sobre la **necesidad de que en La Habana sea recibida una delegación étnica**, para la consolidación de una mesa de trabajo que aborde las problemáticas y soluciones construidas por los pueblos étnicos.]

[Marino Córdoba, presidente de la Asociación Nacional de Afrocolombianos Desplazados AFRODES, asegura que el foro se enmarcó en la apuesta colectiva del sector afro de tener voz en los acuerdos de paz, a través de la participación efectiva de sus líderes regionales, para la **construcción de un proceso de paz incluyente** que tenga en cuenta sus aportes, pues “como victimas apoyamos y respaldamos salida negociada al conflicto”, pero **no se ven representados en la delegación del Gobierno**.]

[**“Sí queremos avanzar hacia una paz duradera deben ser incluidas las propuestas de acción de los afro”**, lo que implica que se defina un mecanismo de participación, para que los sectores más afectados puedan contribuir efectivamente a la salida política del conflicto, agrega Córdoba. “Seguimos avanzando en la incidencia directa con EEUU” para presionar a las delegaciones pues **las solicitudes enviadas a La Habana no han tenido respuesta**.]

[Las acciones que adelantan las distintas organizaciones articuladas al CONPA pretenden dejar claro su apoyo a la salida negociada al conflicto armado,  con la insistencia en que **los pueblos afro-descendientes puedan participar antes de que se cierre el ciclo final para que sus propuestas queden plasmadas en los acuerdos finales**.]
