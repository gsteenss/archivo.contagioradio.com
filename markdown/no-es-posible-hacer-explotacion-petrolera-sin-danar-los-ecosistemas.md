Title: No es posible hacer explotación petrolera sin dañar los ecosistemas
Date: 2016-04-15 17:25
Category: Ambiente, Nacional
Tags: Caño Cristales, CENSAT, Explotación petrolera, La Macarena
Slug: no-es-posible-hacer-explotacion-petrolera-sin-danar-los-ecosistemas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/04/Mapas-mineria-1.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Mapas: geoactivismo.org] 

###### [15 Abril 2016]

La presión ejercida por ambientalistas y el movimiento social en Colombia y el mundo obligó al gobierno a suspender la licencia de explotación petrolera en la región de la Macarena en el departamento del Meta. Sin embargo Tatiana Roa, directora del CENSAT, afirma que la suspensión no es una revocatoria y que **el anuncio del presidente Santos preocupa en la medida en que puede reanudarse la licencia y es ambiguo**.

Además, para Tatiana Roa, el problema no es de una única licencia, sino que es mucho más grave puesto que la sierra de la Macarena está “sitiada” por proyectos de explotación minera y petrolera, es decir, que el problema es un **[modelo extractivo](https://archivo.contagioradio.com/?s=macarena) que no ha respetado ni las zonas de reserva o parque, ni sus zonas especiales de amortiguamiento**.

La postura del gobierno es muy ambigua y no puede bajarse la guardia frente a la frontera de extracción petrolera que está llegando ya a la zona de la Sierra de la Macarena. **El problema es si se va a seguir permitiendo que esa frontera avance**, afirma Roa, es necesario que se frene ese avance que [ya está demostrado es “devastador”](https://archivo.contagioradio.com/gobierno-busca-expropiar-tierras-de-campesinos-e-indigenas-en-el-meta/).

Para evidenciar esta problemática Roa recuerda que solo basta mirar los mapas de tierras de hace algunos años y mirar los de ahora. **La frontera ha ido avanzando, tiene copada la Orinoquía y avanza hacia la región de la amazonía.**

Según Roa, la licencia está vigente y para reanudarla lo único que tendría que hacer la empresa es hacer un supuesto compromiso con el respeto del ambiente o inventar que usará alta tecnología, sin embargo, está comprobado que muchas veces esos compromisos de las empresas quedan en el papel y que [no existe una “alta tecnología” que realmente garantice la inmunidad de los ecosistemas](https://archivo.contagioradio.com/empresa-petrolera-teniria-de-negro-cano-cristales/)de los tres parques que rodean la zona de explotación de la licencia 286.

“Los debates han estado colocándose a nivel nacional, la relación de la hidrología demuestra que en la medida en que se comience a extraer el petróleo, [el agua de los pozos iría a parar a Caño Cristales”](https://archivo.contagioradio.com/presumo-que-hubo-torciditos-para-lograr-esa-licencia-lamacarena/) afirma Roa, y agrega que “no hay actividad petrolera que no produzca daños ambientales”.
