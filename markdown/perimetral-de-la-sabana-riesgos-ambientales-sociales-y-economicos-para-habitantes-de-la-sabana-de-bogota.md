Title: Perimetral de la sabana: Riesgos ambientales, sociales y económicos para habitantes de la sabana de Bogotá
Date: 2020-12-12 14:00
Author: CtgAdm
Category: Nacional
Tags: defensa del ambiente, defensa del territorio, Movilización social
Slug: perimetral-de-la-sabana-riesgos-ambientales-sociales-y-economicos-para-habitantes-de-la-sabana-de-bogota
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/12/WhatsApp-Image-2020-12-12-at-7.48.55-AM.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

Este domingo 13 de diciembre se llevará a cabo una marcha convocada por el Comité No a la Perimetral de la Sabana y sus Peajes. Uno de los puntos de encuentro será en Porkys, un lugar ubicado entre los municipios de Tenjo y Tabio y otro punto será en el CAI que queda entre Cajicá y Tabio. De ahí los manifestantes marcharán de forma pacífica hacia el foro principal del municipio de Tabio.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/TabioAvizor/status/1336030499463114752","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/TabioAvizor/status/1336030499463114752

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

Esta manifestación ciudadana se convoca con el fin de defender el territorio tabiuno, que hace unos años se ve amenazado por la construcción de una carretera que tendría implicaciones negativas en materia ambiental, social y económica, con las que los habitantes ya han manifestado su desacuerdo.

<!-- /wp:paragraph -->

<!-- wp:quote -->

> ***"Esta expresión pacifica consiste en una visión que defiende la sabana como un territorio ambientalmente sostenible, libre del volteo de tierras, de la especulación inmobiliaria y con una planeación urbana ordenada que favorezca el crecimiento de los habitantes de los territorios"***
>
> <cite>Miembro del Comité No a la Perimetral de la Sabana y sus Peajes </cite>

<!-- /wp:quote -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/manuel_rodb/status/1335564298156990464","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/manuel\_rodb/status/1335564298156990464

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:heading {"level":3} -->

### La consigna es NO a la Perimetral de la Sabana

<!-- /wp:heading -->

<!-- wp:quote -->

> ***“Creemos que el proyecto de la perimetral de la sabana, que ha tenido 4 modificaciones y arguyas políticas para que no podamos hacerle una oposición como debe ser, es un proyecto que exige de la participación a través de la movilización para derrotarlo, es el mecanismo principal que tenemos, en el cual vamos a expresar todos nuestros argumentos”***
>
> <cite>Miembro del Comité No a la Perimetral de la Sabana y sus Peajes</cite>

<!-- /wp:quote -->

<!-- wp:paragraph -->

Según el mismo Odinsa, proponente del proyecto, a precios del 2017, la inversión para la perimetral de la Sabana sería de 1.55 billones de pesos. Es decir, que ahora acomodándose a los precios actuales saldría más costoso. Esta se considera una cifra desproporcionada teniendo en cuenta que esta vía ya cuenta con el 90% de su construcción.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Los manifestantes resaltan ente sus peticiones que "Es absurdo que nos quieran imponer más peajes en el departamento de Colombia que más tiene y además de eso que se haga una vía sin un estudio ambiental y que no cuenta con la aprobación de los habitantes"        

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Se espera que el gobierno de Cundinamarca y el Estado garantice que no abra represión por parte de la policía en el transcurso de las manifestaciones y que se respete el derecho a la movilización pacifica ciudadana.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Le puede interesar [Arte urbano, un espejo crítico del contexto histórico en Latinoamérica](https://archivo.contagioradio.com/arte-urbano-espejo-critico-del-contexto-historico-en-latinoamerica/)

<!-- /wp:paragraph -->
