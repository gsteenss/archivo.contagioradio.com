Title: Acuerdo para no sacar a PPK tendría que ver con indulto a Alberto Fujimori
Date: 2017-12-22 13:29
Category: El mundo
Tags: Fujimori, Pedro Pablo Kuczynski, Perú
Slug: ppk-indulto-fujimori
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/12/ppk.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: El Comercio Perú] 

###### [22 Dic 2017] 

A pesar de que el miercoles pasado, los votos necesarios para la vacancia de PPK en la presidencia estaban listos, al parecer hubo llamadas personales de Alberto Fujimori, desde la prisión para que se abstuvieran de votar. Esta situación, según Rocio Silva, solamente tendría dos explicaciones, una que se dividió el fujimorismo o que hubo un acuerdo para un indulto presidencial a cambio de que PPK continúe en el poder.

La votación dio como resultado 78 votos a favor, 19 en contra y 21 abstenciones. Situación que de acuerdo con Silva, defensora de derechos humanos, perversa y hace parte de un **“trueque” entre el Fujimorismo y quienes se abstuvieron de poner a la familia Fujimori en el poder, con tal de que haya indulto presidencial a favor del ex dictador. **

Para Silva si la vacancia se hubiese hecho efectiva de igual forma, el panorama hubiese sido desalentador debido a que seguirían los Fujimori en el poder y lo que pasó durante la votación es que los integrantes del Fujimorismo se quebraron en dos, los que están a favor de que continúe **Kuczynski en el poder y los que se abstuvieron para intentar que subiera Keiko Fujimori**.

“La vacancia lo que iba a hacer era permitir que el Fujimorismo se instalará no solo en el Congreso sino también en el ejecutivo, pero lo que ha sucedido anoche verdaderamente es perverso, el Fujimorismo se partió” señaló Silva. (Le puede interesar: ["Vacancia de PPK en Perú sería una oportunidad para los movimiento sociales"](https://archivo.contagioradio.com/vacancia-ppk-peru/))

### **Fiscalía seguirá investigando a Kuczisnki y a Keiko Fujimori** 

Además, la defensora de derechos humanos afirmó que los medios de información peruanos, están afirmado que los congresistas que se abstuvieron en votar, tiene una relación muy estrecha con el hijo de Alberto Fujimori y que minutos antes de que se realizará la votación sería el mismo **Alberto Fujimori el que habría llamado desde la cárcel, para convencer a los congresistas de que estuviesen en abstención y posteriormente dar indulto a Fujimori para que salga de la cárcel.**

Frente a esta situación Silva expresó que aún existe la posibilidad de que la Fiscalía continúe con las investigaciones contra Pedro Pablo Kuczynski, de igual forma esta semana se conocería la declaración de Marcelo Odebrech y pese a que no haya sido el Congreso el que sacará a Kuszynski del poder, lo podría hacer la justicia.

<iframe id="audio_22804051" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_22804051_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
