Title: 92 feminicidios en América Latina han sido registrados en menos de 2 meses
Date: 2017-02-22 16:51
Category: El mundo, Mujer, Nacional
Slug: 92-feminicidios-en-america-latina-han-sido-registrados-en-menos-de-2-meses
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/02/feminicidios.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Oaxaca ] 

###### [22 Feb 2017] 

En menos de 2 meses de 2017 han sido registrados 6 feminicidios en Paraguay, en México 10, en Ecuador 19 y en Argentina 57. Organizaciones de mujeres de América Latina advierten que a pesar de la existencia de leyes que sancionan el feminicidio, los Estados **no erradican las causas fundamentales de las violencias basadas en género y** **más del 90% de los casos se mantienen en la impunidad.**

Noelia Figueroa, docente universitaria y activista feminista argentina del Colectivo Ni una Menos, señaló que desde hace años las organizaciones vienen denunciando esta problemática sin obtener respuestas satisfactorias, además, reveló que no hay estadísticas oficiales, **“el registro lo hemos hecho las organizaciones de mujeres y feministas, el Estado no presta atención a este fenómeno”. **([Le puede interesar: El mundo grita, ni una menos vivas nos queremos](https://archivo.contagioradio.com/el-mundo-grita-ni-una-menos-vivas-nos-queremos/))

La activista resaltó que [nunca antes el movimiento de mujeres había alcanzado igual masividad y la posibilidad de instalar una agenda en espacios de participación política, y **“como fenómeno reactivo están estas violencias”**. Explica que para 2016 en Argentina,] “la cifra era una mujer asesinada cada 24 o 26 horas, ahora es cada 18 horas”, lo que **es una evidente muestra del aumento no sólo de los feminicidios y casos de violencias, sino “la crueldad con que los realizan".**

Comenta que este fenómeno de feminicidios por “el sólo hecho de ser mujeres”, es un flagelo que se extiende a nivel continental y mundial y diversas organizaciones han denunciado que “más del 90% de los casos permanecen impunes”.

### ¿Qué pasa entonces con las leyes contra el Feminicidio? 

Respecto a las leyes de feminicidio, vigentes en 16 países latinoamericanos, manifiesta que **“sólo responden de manera punitiva, procesan a algunos agresores, cuando lo hacen, pero no aborda el cómo erradicar las violencias”**, indica que faltan abordajes integrales pues “las mujeres no solo estamos expuestas a las construcciones de género impuestas”.

También están las violencias en cuanto a empleos precarios, poco reconocimiento e imposición de actividades del hogar, brecha salarial entre hombres y mujeres y “nulas oportunidades para que las mujeres tengan autonomía económica” señala Figueroa.

Asegura que se trata de un problema estructural, **“que atraviesa instituciones estatales, edades, clases sociales y demás”, insiste en que “urge un cambio de paradigma”**, a través de la sensibilización a poblaciones de distintas edades y clases, en los barrios, escuelas facultades y espacios laborales, prevenir y sensibilizar sobre las violencias sexistas para que “se pueda hacer frente a esta grave situación”.

### Fortalecer la unión entre mujeres, la alternativa 

La activista feminista, manifiesta que “las alternativas que tenemos las mujeres son seguir encontrándonos, organizarnos, intercambiar saberes y escucharnos”, fortalecer las redes de organizaciones de mujeres y feministas en América Latina y el mundo, para **hacer un trabajo colectivo orientado a la transformación de políticas publicas y esquemas de pensamiento machistas.**

Por último, menciona la gran iniciativa promovida por mujeres de todos los continentes, el **Paro Internacional de Mujeres que será el próximo 8 de Marzo y tiene como objetivo “mostrar que somos fundamentales para que el mundo funcione**, las cosas sucedan y demostraremos esa importancia parando nuestras actividades”.

<iframe id="audio_17172814" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_17172814_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
