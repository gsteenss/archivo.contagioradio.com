Title: CIDH ordena medidas cautelares para proteger al pueblo Siona
Date: 2018-07-18 15:48
Category: DDHH, Nacional
Tags: CIDH, Derechos Humanos, medidas cautelares, Siona
Slug: cidh-ordena-proteger-al-pueblo-siona
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/07/Captura-de-pantalla-2018-07-18-a-las-3.11.31-p.m..png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: @cejil] 

###### [18 Jul 2018] 

La Comisión Interamericana de Derechos Humanos (CIDH) ordenó medidas cautelares en favor del Pueblo Siona de Colombia, que habita en las riveras del Río Putumayo, acción que se da **en respuesta a la situación de exterminio físico y cultural a la que se enfrenta esta comunidad indígena,** por la acción de diferentes grupos armados en su territorio.

Francisco Quintana, director del Centro por la Justicia y el Derecho Internacional (CEJIL) para la Región Andina, Norte America y el Caribe, afirma que a pesar del Acuerdo de Paz, en la zona que habitan los Siona aún no se vive la paz, y por el contrario, como ocurre en otros lugares de Colombia, "se mantiene un alto nivel de riesgo".

La medida cautelar fue ordenada gracias a la constatación hecha por la CIDH, de que los miembros del pueblo Siona, viven una situación de urgencia debido a que sufren limitaciones para desplazarse en su territorio por parte de actores armados, la presencia de minas antipersona y el reclutamiento forzado que padecen los jóvenes de la comunidad.

Estos factores, además de poner en riesgo la vida e integridad de los nativos, también afectarían las formas de autogobierno de la organización indígena, así como la vida comunitaria. Por estas razones, los habitantes de los resguardos Buenavista y Santa Cruz de Puñuñu Blanco, esperan que se tomen acciones que detengan la violencia, particularmente en esos territorios.

De igual forma, **las medidas cautelares  obligan al Estado colombiano** para que se destinen los recursos humanos, financieros y técnicos suficientes e idóneos, para **proveer las medidas integrales de reparación y restitución de derechos territoriales**, que garanticen su compromiso con la vida e integridad del Pueblo Siona. (Le puede interesar: ["66% de los pueblos indígenas está a punto de desaparecer: ONIC"](https://archivo.contagioradio.com/pueblos-indigenas-a-punto-de-desaparecer-onic/))

###### [Reciba toda la información de Contagio Radio en] [su correo](http://bit.ly/1nvAO4u) [o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por] [Contagio Radio](http://bit.ly/1ICYhVU) 
