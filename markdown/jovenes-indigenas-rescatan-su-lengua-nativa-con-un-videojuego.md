Title: Jóvenes indígenas rescatan su lengua nativa con un videojuego
Date: 2015-02-04 20:41
Author: CtgAdm
Category: datos, Resistencias
Slug: jovenes-indigenas-rescatan-su-lengua-nativa-con-un-videojuego
Status: published

###### Foto: Agencia de Noticias UN 

El video juego, que espera ser presentado a mediados del 2015, podrá ser descargado por los jóvenes de la comunidad como una aplicación en los dispositivos móviles (celulares, tablets) y computadores con sistemas operativos Windows, Android e IOS. El paso por los niveles del juego incita a los y las jóvenes a usar palabras en Kichwa relacionadas con costumbres, expresiones y rituales de la comunidad.

Esta iniciativa, que surgió ante la preocupación de la comunidad de Sesquilé, Cundinamarca, por la pérdida de sus costumbres y el desuso de la lengua Kichwa por el contacto de los indígenas con las costumbres de la llamada "sociedad de occidente", ha sido trabajada por las 72 personas que conforman la comunidad de la mano de investigadores, ingenieros, antropólogos y lingüistas de la Universidad Nacional de Colombia.

De tener éxito la fase de presentación del prototipo, el sistema del videojuego podría ser replicado en otras comunidades indígenas y afrodecendientes.
