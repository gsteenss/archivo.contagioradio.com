Title: Corte Constitucional podría poner fin a ampliación del Fuero Penal Militar
Date: 2016-02-24 13:19
Category: Judicial
Tags: crímenes de estado, Derecho Internacional Humanitario, fuero penal militar
Slug: corte-constitucional-podria-poner-fin-a-ampliacion-del-fuero-penal-militar
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/07/planton-fuero-penal-6.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Coljuristas 

<iframe src="http://co.ivoox.com/es/player_ek_10559501_2_1.html?data=kpWil56ZdJKhhpywj5WUaZS1kZWah5yncZGhhpywj5WRaZi3jpWah5yncaTj09nSjajTstToytnixc7TssLgjNXcxteJh5SZoqnOjdXTssbmjMvW0JDFb8Lh0dHWw8jNaaSnhqeg0JDIqY6ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Alirio Uribe] 

###### [24 Feb 2016] 

La Corte Constitucional dará inicio al debate que busca revocar el acto legislativo mediante el cual cual se modificó el fuero penal militar en el Congreso de la República, de acuerdo con una demanda interpuesta el año pasado por diversas organizaciones que argumentan que la ampliación de fuero penal militar afectaría la **obligación estatal de respetar y garantizar los derechos humanos, el principio de autonomía judicial como manifestación del principio de separación de poderes y el principio de igualdad ante la ley.**

El magistrado Luis Ernesto Vargas será quien decida si se acoge las razones de la demanda, entre las que se asegura que la modificación del fuero penal militar, **“es inconveniente, inconstitucional y un riesgo para los derechos humanos”**, como lo explica Alirio Uribe, representante del Polo Democrático alternativo.

De acuerdo con los demandantes, la ampliación de fuero podría afectar los procesos que existen contra miembros de la Policía y el Ejército Nacional por diversos hechos, entre los cuales se destacan la violación al Derecho Internacional Humanitario. Una de las preocupaciones, es que por medio esta reforma se modifica el artículo 221 de la Constitución, con lo que se pretende **desconocer los derechos humanos y que solo se aplique el Derecho Internacional humanitario.**

Una segunda preocupación es que en el Congreso de República también está en curso el **proyecto de Ley 129 que reinterpreta DIH,** y por el cual se legitimaría las violaciones del derecho a la vida y se justifica incluir a civiles en el conflicto armado, como lo dice el representante Uribe Muñoz.

Cabe recordar que  Comisión Colombiana de Juristas, el Colectivo de Abogados José Alvear Restrepo, más de diez organizaciones defensoras de derechos humanos, víctimas, y congresistas como Alirio Uribe, Iván Cepeda y Ángela María Robledo,  fueron quienes presentaron la demanda ante la Corte Constitucional el año pasado.
