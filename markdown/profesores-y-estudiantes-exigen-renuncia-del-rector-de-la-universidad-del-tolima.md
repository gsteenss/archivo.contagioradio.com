Title: Profesores y estudiantes exigen renuncia del rector de la Universidad del Tolima
Date: 2016-03-03 12:43
Category: Movilización, Nacional
Tags: Paro U Tolima, universidad del tolima
Slug: profesores-y-estudiantes-exigen-renuncia-del-rector-de-la-universidad-del-tolima
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/03/U-Tolima.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Prensa U Tolima ] 

<iframe src="http://co.ivoox.com/es/player_ek_10660668_2_1.html?data=kpWjmJWaepmhhpywj5aZaZS1lZ2ah5yncZOhhpywj5WRaZi3jpWah5yncbHm0MvS1dTWqdSf2pDS1dnZqMrVz9nS1ZDJvMrbxtOY1MrSuc%2FXysaYxsrQb9PZxNnc1JDIqYyhhpywj6jTstXVyM7cjbfFqMrjjJKSmaiReA%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Jorge Gantiva, Docente] 

###### [2 Mar 2016 ] 

Suspendido el semestre académico en la Universidad del Tolima por cuenta de la profunda crisis presupuestal que enfrenta la institución desde hace tres años, con un **déficit de más de \$23 mil millones, sumado a un recorte para este semestre de \$20 mil millones**. Una situación que la Asociación Sindical de Profesores Universitarios ASPU, ha venido denunciando como el resultado de las prácticas “clientelistas y de despilfarro” de la actual administración de José Herman Muñoz y de la desfinanciación por parte de los gobiernos nacional y departamental, como asegura el docente Jorge Gantiva.

La decisión fue tomada por el Consejo Académico pues “mientras no esté claro el plan de ajuste presupuestal, no hay posibilidades, condiciones ni garantías para el desarrollo del semestre”, según afirma Gantiva. El argumento se suma al **total rechazo por parte de la Asamblea Permanente de Profesores a la disposición del Consejo Superior de reajustar el presupuesto, reduciendo la inversión de por lo menos \$19 mil millones**, y que para este docente, no es la solución adecuada, “es inadmisible creer que el Gobierno nacional que habla del país más educado, en vez de apoyar la universidad pública, le recorte su ya menguado presupuesto”.

En el Consejo Superior de la institución “el Gobierno nacional, departamental y el rector tienen una relativa mayoría”, asevera Jorge; sin embargo, los representantes estudiantiles y profesorales “se han opuesto a las medidas arbitrarias y agresivas” que pretenden ser impuestas y han denunciado uno de los puntos en los que es evidente el pago de favores políticos en las contrataciones de esta administración, la **nómina paralela que ha gastado por lo menos \$10 mil millones en las órdenes de prestación de servicios, en aumento durante los últimos tres años**.

Los profesores y estudiantes que han organizado plantones, marchas, actividades artísticas en el marco de esta movilización que ya completa tres meses, plantean que la **solución real a la crisis presupuestal debe incluir la supresión de la alta burocracia de la institución y la renuncia del actual rector** “porque es el responsable, es el ordenador del gasto, mintió, ocultó la información, durante tres años dijo que no había crisis, dijo que era flujo de caja. Haciéndose a un lado profesores, trabajadores y estudiantes podremos emprender el proceso de transformación de la Universidad”, concluye el docente.

##### Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](https://archivo.contagioradio.com/).] 
