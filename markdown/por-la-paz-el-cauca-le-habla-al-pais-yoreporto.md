Title: Por la Paz, el Cauca le habla al país. #YoReporto
Date: 2015-07-22 13:09
Category: Nacional, yoreporto
Tags: Cese Bilateral de Fuego, Cese unilateral de fuego, Conversaciones de paz de la habana, ELN, FARC, Juan Manuel Santos
Slug: por-la-paz-el-cauca-le-habla-al-pais-yoreporto
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/07/paz-cauca-contagioradio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: prensalibrecauca.wordpress 

###### [22 Jul 2015] 

El Cauca, que por muchos años ha sido territorio de la guerra y simultáneamente de construcción de  paz, convoca a todas y todos los colombianos para que Caminemos por la Paz, para que se ponga  fin al conflicto armado, para que no haya más víctimas y dolor, para que campos y ciudades sean territorios de bienestar general y de progreso, en suma, para que pongamos nuestro mejor esfuerzo en construir Paz con Justicia Social. Esta es una repetición, pero no sobra. Quizá a fuerza de repetir y repetir alcancemos la conciencia suficiente para trabajar más unidos y fuertemente por la paz.

Saludamos con renovada esperanza el anuncio de las FARC-EP de decretar el cese unilateral al fuego a partir del 20 de julio y por un mes. Igualmente el comunicado conjunto No. 55 suscrito entre el Gobierno Nacional y las FARC-EP, el cual busca agilizar los diálogos y desescalar el conflicto. Estos gestos y los avances de la Mesa de la Habana, entre ellos los relacionados con aspectos humanitarios contribuyen a disminuir la angustia y zozobra de muchas comunidades, y a generar más confianza y credibilidad de la sociedad en el proceso y en los compromisos de las partes.

Desde nuestro diverso territorio Caucano, llamamos a toda Colombia para que clamemos y exijamos al Gobierno  y a las Farc-ep, así como al Eln:

1.  Que no se levanten de la Mesa y se mantengan los diálogos hasta llegar a un Acuerdo Final.
2.  Que avancen hacia el Cese Fuego y de hostilidades, bilateral y definitivo.
3.  Que se respeten los principios y reglas consagradas en el DIH, en particular que se respete el principio de inmunidad de la población civil y se promuevan acciones humanitarias y de desescalamiento del conflicto en nuestro territorio como: Avance en el barrido humanitario ya acordado, de minas antipersonales (MAP), restos explosivos de guerra (REG) y municiones sin explotar (MUSE) que pueden encontrase en las zonas de las confrontaciones armadas; respeto a personas y bienes protegidos y a la integridad física y psicológica de las mujeres, y el retorno inmediato a sus hogares de los menores de edad involucrados en la guerra.
4.  Que se promueva la construcción de una paz con Justicia Social, con participación de las comunidades, sectores y organizaciones sociales.
5.  Que se progrese en el diálogo exploratorio entre el ELN y el Gobierno Nacional.

Este es un País injusto sí, desigual sí, excluyente sí, sin verdadera participación ciudadana. Es todo lo que debemos cambiar, y la guerra no contribuye a transformar nuestro País. Este es el momento de acordar el fin del conflicto con las FARC e iniciar los diálogos con el ELN,  es el tiempo de avanzar hacia la sociedad justa, que nos merecemos, con la participación de todos y todas  y con plenas garantías para ello.

¡La Paz es un Mandato Constitucional y un Derecho de las y los Colombianos!

GOBERNACIÓN DEL CAUCA – ALCALDIAS DEL CAUCA-ORGANIZACIONES SOCIALES: MOVIMIENTO SOCIAL Y POLÍTICO  MARCHA PATRIOTICA – CONGRESO DE LOS PUEBLOS- CONSEJO REGIONAL INDIGENA DEL CAUCA-CRIC- RUTA PACIFICA DE MUJERES CAUCA – ANUC CAUCA-– UAFROC- COMITÉ DE INTEGRACION DEL MACIZO COLOMBIANO-CIMA- ORDEURCA –ACADER- MCCAJIBIO- COCOCAUCA - COORDINADOR NACIONAL AGRARIO CAUCA- ESPACIO REGIONAL DE PAZ-

**Por: Comunicaciones Espacio Regional de paz del Cauca \#YoReporto**
