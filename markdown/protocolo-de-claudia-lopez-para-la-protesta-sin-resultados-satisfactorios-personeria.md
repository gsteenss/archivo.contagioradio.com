Title: Protocolo de Claudia López para la protesta sin resultados satisfactorios: Personería
Date: 2020-02-19 15:21
Author: CtgAdm
Category: Movilización, Política
Tags: ESMAD, Personería, Protocolo
Slug: protocolo-de-claudia-lopez-para-la-protesta-sin-resultados-satisfactorios-personeria
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/02/Personería-se-refiere-a-protocolo-de-Claudia-López.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:heading {"align":"right","level":6,"textColor":"cyan-bluish-gray"} -->

###### Foto: @personeriabta {#foto-personeriabta .has-cyan-bluish-gray-color .has-text-color .has-text-align-right}

<!-- /wp:heading -->

<!-- wp:paragraph -->

La directora de la Personería de Bogotá, Carmen Teresa Castañeda Villamizar, señaló en un comunicado de la entidad que no existe un nuevo protocolo para atender la protesta social, porque el establecido en 2015 aún está vigente, de igual forma, aseguró que las medidas que ha tomado la actual Alcaldía de la ciudad "no han obtenido resultados satisfactorios, en términos de garantía de derechos de los manifestantes y de terceros".

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### **"El distrito no ha adoptado ningún acto administrativo estableciendo un nuevo protocolo"**

<!-- /wp:heading -->

<!-- wp:paragraph -->

En primer lugar, la personera afirmó que pese a que tanto el secretario de Gobierno, Luis Ernesto Gómez, como la alcaldesa Claudia López han hablado de un nuevo protocolo para atender la protesta social, en realidad tal no existe, porque "el distrito no ha adoptado ningún acto administrativo estableciendo un nuevo protocolo".

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por lo tanto, aclara que se mantiene vigente el Decreto 563 de 2015, llamado “Protocolo de Actuación para las Movilizaciones Sociales en Bogotá: por el Derecho a la Movilización y la Protesta Pacífica”. Un acto administrativo que fue consensuado y creado en compañía de organizaciones sociales y defensoras de derechos humanos.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Aunque la Personería reconoce que la alcaldesa puede proponer y utilizar nuevas estrategias para acompañar las movilizaciones sociales, sostiene que "éstas no han obtenido resultados satisfactorios, en términos de garantía de derechos de los manifestantes y de terceros". (Le puede interesara: ["ESMAD no está capacitado para usar Escopeta calibre 12: Procuraduría"](https://archivo.contagioradio.com/esmad-no-esta-capacitado-para-usar-escopeta-calibre-12-procuraduria/))

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### **Medidas irresponsables y poco fiables**

<!-- /wp:heading -->

<!-- wp:paragraph -->

El 'protocolo' presentado por Claudia López a inicio del año incluía la participación de madres, posterior intervención de gestores de convivencia y como última medida el Escuadrón Móvil Antidisturbios (ESMAD). Tras la presentación, diferentes voces rechazaron la participación de las madres en la protesta, por ser una forma de infantilizar las demandas de las personas en las calles.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

A esta crítica se sumó ahora la de la personera, que afirmó que "la utilización de madres gestoras de paz es altamente irresponsable", en tanto ellas no cuentan con la preparación necesaria para cumplir con este papel y se enfrentan a un alto riesgo. A ello se suma que el ESMAD, tan solo en este año ha intervenido en 36 ocasiones, 30 de ellas en escenarios universitarios por lo que es cuestionable si en verdad es la 'última opción'.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### **La crítica a la Fuerza Pública** de la Personería

<!-- /wp:heading -->

<!-- wp:paragraph -->

La personera aseguró que "es inaceptable la cantidad de personas que han resultado lesionadas en sus ojos en el marco de escenarios de protesta", y añadió que era imperativo establecer las responsabilidades sobre estas lesiones. Por otra parte, también puso en duda acudir a la Fuerza Disponible (cuerpo de la Policía para la protesta que usa solo escudo y tonfa), en el marco de la protesta.

<!-- /wp:paragraph -->

<!-- wp:html -->

> [\#Entérate](https://twitter.com/hashtag/Ent%C3%A9rate?src=hash&ref_src=twsrc%5Etfw) ? Desde [@DefenderLiberta](https://twitter.com/DefenderLiberta?ref_src=twsrc%5Etfw) realizamos un ejercicio de monitoreo y verificación a las violaciones de los DDHH en el marco de la protesta social a través del Sistema de Información de Agresiones a la Protesta Social -SIAP.  
>   
> Conoce aquí las cifras⬇️<https://t.co/nH8XG1o2vF> [pic.twitter.com/3jo1afzREO](https://t.co/3jo1afzREO)
>
> — Campaña Defender la libertad (@DefenderLiberta) [February 18, 2020](https://twitter.com/DefenderLiberta/status/1229810595509293057?ref_src=twsrc%5Etfw)

<p>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
  
<!-- /wp:html -->

</p>
<!-- wp:paragraph -->

Según se explicó en el comunicado de la Personería, el que este cuerpo use estos elementos es "una invitación al enfrentamiento cuerpo a cuerpo, lo que implica un aumento del riesgo para los manifestantes y los uniformados". La Fuerza Disponible es un cuerpo que no recibe formación especializada y tiene una alta rotación del personal, por esa razón, Castañeda afirma que sus integrantes desconocen a funcionarios civiles y se extralimitan en sus procedimientos.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por último, la personera sostuvo que han documentado en forma reiterativa el lento trámite de hasta 16 horas en el desplazamiento de personas capturadas en el marco de la movilización a los Centros de Traslado por Protección (CTP), antes llamados UPJ. A ello se suma la detención de ciudadanos en estaciones de Policía, "contrariando los protocolos y jurisprudencia constitucional".

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Puede leer el comunicado de la Personería en este [enlace.](https://www.personeriabogota.gov.co/sala-de-prensa/notas-de-prensa/item/677-no-hay-un-nuevo-protocolo-de-movilizaciones-personera)

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78980} /-->
