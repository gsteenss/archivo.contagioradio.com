Title: Histórico respaldo en Congreso a prohibición del asbesto
Date: 2019-05-22 14:23
Author: CtgAdm
Category: Ambiente, Política
Tags: asbesto, colombia sin asbesto, Fundación Ana Cecilia Niño
Slug: historico-respaldo-en-congreso-a-eliminacion-de-asbesto
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/11/asbesto.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Greenpeace] 

El proyecto de ley para eliminar el asbesto en Colombia fue aprobado ayer por la Comisión Séptima de la Cámara de Representantes, un paso histórico, que se da luego de su octavo intento por ser aprobado en el Congreso. Esta iniciativa busca proteger a las y los colombianos de **este material que cada año produce 540 casos de cáncer de pulmón** en el país.

La norma contempla sanciones que van desde los **100 hasta los 5.000 salarios mínimos por el uso de asbesto**, así como sanciones penales y administrativas. También establece que a partir del **primero de enero de 2021 queda prohibido el uso, comercialización, importación y distribución del mineral en Colombia**. (Le puede interesar: "[Tras fallo histórico, activistas insisten que asbesto debe ser prohibido](https://archivo.contagioradio.com/tras-fallo-historico-activistas-insisten-asbesto-prohibido/)")

### **El camino del proyecto para prohibir el asbesto en Colombia**

Tras este histórico paso, el proyecto ahora será revisado por la plenaria de Congreso, donde enfrentará dos desafíos concretos, sostuvo **Daniel Pineda, integrante del movimiento Colombia sin asbesto y director de la Fundación Ana Cecilia Niño**. Por un lado, el proyecto tienen plazo hasta **el próximo 20 de junio** para ser sancionado, hecho que deja poco tiempo para que la iniciativa sea debatida, si se tiene en cuenta que existe "una acumulación de proyectos" en la Cámara.

Por otro lado, Pineda indicó que los senadores podrían presentar nuevas modificaciones que cambiaría el propósito del proyecto. En la Comisión Séptima, el texto del documento ya sostuvo una modificación al sustituir la palabra "prohibir" por "eliminar" con el fin de aclarar que la norma no sancionaría a dueños de viviendas construidas con asbesto.

A pesar de este cambio, el activista afirmó que "el grueso del proyecto sigue intacto". Sin embargo hay un tema en el aire, la prohibición de minería de asbesto en el país. Actualmente, la **mina Las Brisas contrata a 250 trabajadores en Antioquia** y espera seguir operando como la única mina de explotación de asbesto en Colombia. No obstante, la norma **busca prohibir la explotación doméstica para el 2021 y máximo el 2024**.

Como lo indicó Pineda, el proyecto incluye un plan de adaptación laboral que tendrá que desarrollar el Ministerio de Trabajo para ellos que laboren en este industria. "Se dará un tiempo prudente para que las empresas hagan la transición. Nosotros no atacamos a un empresa en particular sino el uso del asbesto", expresó el director.

<iframe id="audio_36245790" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_36245790_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
