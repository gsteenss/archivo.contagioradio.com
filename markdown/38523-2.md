Title: 80 mil madres comunitarias a la espera de que Corte Constitucional ratifique sus derechos
Date: 2017-03-29 13:20
Category: DDHH, Nacional
Tags: acceso a salarios, ICBF, Madres Comunitarias, Madres Comunitarias ICBF, pensiones
Slug: 38523-2
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/04/Madres-Comunitarias.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: ] 

###### [29 Mar. 2017] 

Este miércoles, las **más de 80 mil madres comunitarias se encuentran movilizándose** en varios lugares del país a la espera de la **decisión definitiva de la Corte Constitucional en la cual dirá si deja o no en firme la sentencia T-480 de 2016**, con la cual se podría abrir la puerta para estas mujeres que esperan les sean reconocidos sus derechos salariales y prestacionales. Le puede interesar: [106 Madres Comunitarias serían indemnizadas](https://archivo.contagioradio.com/106-madres-comunitarias-serian-indemnizadas/)

Para las madres comunitarias, es importante recordar que **la Corte Constitucional ha dicho que la situación a la que se ven expuestas en materia de desconocimiento de derechos laborales y pensionales**, puede considerarse como un acto de discriminación de género de índole público, compuesto, continuado, sistemático y de relevancia constitucional.

En dicha sentencia, se explica como **el Instituto Colombiano de Bienestar Familiar (ICBF) vulneró los derechos fundamentales** a la igualdad, al mínimo vital, a la dignidad humana de cerca de 106 demandantes, luego de que les fuera negado el pago de sus aportes de pensión durante un largo tiempo. Le puede interesar: [Acuerdo con ICBF no satisface demandas de las madres comunitarias](https://archivo.contagioradio.com/acuerdo-de-icbf-no-satisface-demandas-de-las-madres-comunitarias/)

Bajo la consigna Ni Una Más, las madres comunitarias aseguraron en un comunicado que no van a permitir “ni una madre comunitaria, sustituta o tutora más, muerta en la ignominia y la injusticia laboral, sin pensión y sin que el Estado Colombiano reconozca más de tres décadas de trabajo, educando a la primera infancia”.

Hasta la fecha, **cinco Madres Comunitarias de las 106 beneficiadas de la tutela fallecieron en los últimos meses sin poder acceder a su salario** y pensión digna a la que tenían derecho.

###### Reciba toda la información de Contagio Radio en [[su correo]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio]{.s1}](http://bit.ly/1ICYhVU)[.]{.s1} {#reciba-toda-la-información-de-contagio-radio-en-su-correo-o-escúchela-de-lunes-a-viernes-de-8-a-10-de-la-mañana-en-otra-mirada-por-contagio-radio. .p1}
