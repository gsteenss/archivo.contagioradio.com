Title: En Tumaco opera la justicia de los grupos residuales en medio de la militarización
Date: 2019-10-09 15:00
Author: CtgAdm
Category: DDHH, Nacional
Tags: Cajapí, Los Contadores, Oliver Sinisterra, Tumaco
Slug: en-tumaco-opera-la-justicia-de-los-grupos-residuales-en-medio-de-la-militarizacion
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/Tumaco-grupos-residuales.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: El Comercio, imagen de referencia] 

En hechos sucedidos en la vereda Cajapí, de Tumaco, Nariño, un enfrentamiento ent[re miembros del "frente Oliver Sinisterra" y el grupo residual "Los Contadores" dejó un resultado de al menos cuatro personas muertes y un número aún sin determinar de heridos, un suceso que demuestra con notoriedad la disputa armada en la que se encuentran atrapados los habitantes de este municipio del pacífico nariñense, y en la que la presencia militar no juega un papel de protección a los civiles.]

Aunque en principio se creía que se trataba de cuatro víctimas mortales, información reciente permite establecer que serían seis las personas que fueron sacadas de sus casas a la fuerza, Según Leder Rodríguez, integrante de la Federación de Acción Comunal de Nariño, se trataría de personas de la misma comunidad, en su mayoría dedicada a labores agrícolas, sin embargo no hay certeza de si alguno de ellos haría parte de alguno de los grupos armados.

Los hechos, se dan en un contexto en que la lucha por el control del narcotráfico en Tumaco se da entre el 'Frente Oliver Sinisterra', facción disidente de la antigua guerrilla de las FARC  liderada por alias **'El Gringo'**, **'Los Contadores'**, bautizados así por estar bajo las ordenes de alias **'Contador'**, narco  considerado como uno de los más poderosos del Pacífico y las **Guerrillas Unidas del Pacífico**, comandadas por alias **'Borojó'.**

### En Tumaco ni siquiera se sabe cuánta gente han asesinado

Información recopilada por Contagio Radio, permite establecer que la Oliver Sinisterra domina parte del casco urbano de Tumaco, esto debido a una alianza entre alias 'El Gringo' y 'Borojo', mientras la zona rural casi en su totalidad, está bajo el control de Contador y el narco  Mario Lata quienes parecen garantizar el control de la zona, esto pese a que en la región se encuentren cerca de 9.000 soldados de la Fuerza de Tarea Conjunta Hércules.

Aunque Guachal ha cobrado notoriedad por la masacre, **Leder advierte que las muertes son frecuentes y cuando se trata de asesinatos aislados, se ocultan arrojando a los muertos al río** Mira sin que nadie hable al respecto, "hay un miedo total, allá la gente no puede salir a denunciar, cuando pueden recogen a los muertos y los entierran, en otras ocasiones dejan que la corriente se los lleve río abajo".

El líder afirma que no existe con exactitud una forma de conocer cuántos habitantes han sido víctimas de esta disputa territorial,  sin embargo destaca que en lo corrido del año en las zonas aledañas al otro costado del Río Mira, "han sido asesinadas entre 60 o 70 personas entre Cajapí y Guachal" sin que se den soluciones reales a esta problemática.   [(Le puede interesar: Asesinan a Argemiro López, líder social y promotor de sustitución en Tumaco)](https://archivo.contagioradio.com/asesinan-a-argemiro-lopez-lider-social-y-promotor-de-sustitucion-de-cultivos-en-tumaco/)

**Follow us on Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
