Title: Cuando la democracia es mafia
Date: 2015-08-22 16:45
Category: Opinion, superandianda
Tags: Cultura mafiosa, democracia, Oscar Mejía Quintana
Slug: cuando-la-democracia-es-mafia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/08/descarga.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: yinyang] 

#### **[Por [Superandianda ](https://archivo.contagioradio.com/superandianda/) - @Superandianda  ]** 

###### [22 Ago 2015]

[La democracia en un pueblo ignorante deja de ser democracia para convertirse en  la validación de un  sistema corrupto que esclaviza y somete.] [El estado colombiano es un estado mafioso, hemos sido educados, gobernados, informados y alineados por una estructura social hecha para mantener y consolidar la ilegalidad.  ]

[Cuando un estado vive y se nutre de la ilegalidad, la búsqueda por la  supervivencia es la guerra constante, el ciudadano deja de ser colectivo para ser individual, las metas y logros que se proponga le exigen pasar por encima de los demás –metas y logros que le traza la misma estructura social- defiende sus necesidades sin importar el cómo, siendo el móvil que  valida el sistema gubernamental  que lo explota. Entonces la democracia  y el sistema social que lo mueve es mafia y su vida misma contribuye para que lo sea; culpable y víctima de la cultura que lo premia y lo castiga.]

[En Cultura política mafiosa en Colombia  de Oscar Mejía Quintana , señala “la percepción de que el Estado es para ser usufructuado por los “vivos”, de que la política no persigue un ideal de bienestar general, ni siquiera de bien común que es un concepto tradicional, que más bien es la posibilidad de lucrarse en favor propio por debajo del orden legal y que para ello el camino adecuado es una actitud de complicidad, nunca de crítica o fiscalización, con el poder, lo que se pone de manifiesto con una cultura súbdito-parroquial como la colombiana. El caldo de cultivo de prácticas mafiosas más elaboradas está dado desde este nivel primario de la pirámide social”  ][[http://www.revistas.unal.edu.co/index.php/cienciapol/article/view/18867/19757]](http://www.revistas.unal.edu.co/index.php/cienciapol/article/view/18867/19757)

[Es la época electoral donde nos hablan de participación ciudadana, como si bajo el mismo sistema, que se encuentra podrido y enfermo, cupiera la posibilidad de algún cambio. Lo que llaman nuevas generaciones políticas, las juventudes de los partidos políticos, no son otra cosa diferente que las mismas ideas obsoletas de la  burocracia,  que durante la historia nacional  han regido y han robado la posibilidad de buscar nuevas maneras de pensar la política. Entonces, estamos ante la  pirámide política-social que se consolida con guerra, violencia urbana y rural, cultura mafiosa, medios serviles y toda una estructura ciudadana detrás de la  figura que en el momento tenga en promoción cada uno de los partidos políticos.]

[¿Y la izquierda? Vemos a los que se llaman partidos de izquierda con las mismas prácticas de los de derecha, conformes en su minoría, promocionando sus candidatos –los mismos y eternos candidatos- con propuestas no muy lejanas a lo que debería ser su oposición. ¿Y si la fragmentada izquierda colombiana solo es un pequeño espacio para los creyentes de Carlos Marx y Lenin? No encuentro otra manera para explicar el por qué nos dicen que tenemos participación por  unas cuantas curules en el congreso. No queremos unas cuantas curules, también podemos gobernar, también podemos reestructurar la sociedad colombiana, no somos minoría y no tenemos por qué revolcarnos con las mismas obsoletas prácticas de la burocracia colombiana.]

[El gris no es blanco ni tampoco negro: los partidos de centro, más alineados hacia la derecha solo han engañado a quienes se dicen neutrales. Ese es el gran estado democrático colombiano, toda su cultura y política orientada a la derecha y a la mafia que premia a la ciudadanía que contribuya con la corrupción.]

[No llamen irresponsables a quienes no participan de la validación del estado mafioso colombiano, no puede ser combativo para la corrupción pretender hacer parte de la misma corrupción sin reconocer que la gran falencia que tenemos es la estructura social que nos ha llevado al colapso de derechos tan básicos como lo son  la salud y la educación. Es de esta manera como cada candidato que vemos desfilar en campaña es el comodín de cada partido y  jefe de cada contratista. La fiesta de los partidos, empresas y contratistas, ese es el gran día de la democracia colombiana.]
