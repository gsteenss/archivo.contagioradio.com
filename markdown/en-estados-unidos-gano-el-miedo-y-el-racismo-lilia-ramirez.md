Title: "En Estados Unidos ganó el miedo y el racismo" Lilia Ramírez
Date: 2016-11-09 12:45
Category: El mundo, Nacional
Tags: Donald Trump, Elecciones Estados Unidos
Slug: en-estados-unidos-gano-el-miedo-y-el-racismo-lilia-ramirez
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/trump.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Noticiasmvs] 

###### [9 de Nov 2016] 

Con propuestas como la construcción del muro de México en contra de los inmigrantes, la cero tolerancia con los indocumentados duplicando el número de oficiales de deportación o prohibir el ingreso de refugiados de Libia o Siria, Donald Trump llega al periodo electoral número 45 y se posicionará el 20 de enero de 2017 como el presidente de Estados Unidos.

Muchas de sus iniciativas van en contra de la población migrante, latina, afro o árabe que se encuentra en el país, sin embargo los **resultados electorales demostraron que estados como Florida con una alta población extranjera, respaldaron la candidatura de Trump** y le dieron la victoria.

Para Lilia Ramírez, socióloga colombiana que se encuentra viviendo en Nashville Tenneessee, esta situación es sorprendente por la estrategia del miedo “**Donald Trump uso el miedo y lo reflejo en los inmigrantes, los culpo de quitarle trabajo a las personas y afloro el racismo que ahora se expresa más abiertamente que en el pasado**”

No obstante, Ramírez considera que estas elecciones pueden ser un despertar “**podría crear una conciencias sobretodo en la población Hispana y tal vez en sectores más progresista**s o personas que se han dado cuenta que este sistema electoral no es funcional”.

Amy Vélez otra colombiana que se encuentra en Nueva York, expresa que tal vez lo que sucedió en Estados Unidos es tan solo el reflejo de la crisis interna del país, en donde un candidato reta al status Quo representando en la candidata Hilary Clinton y quién solo se quedo con 218 votos “ **se ha dado una internacionalización de la opresión, a las personas les es más fácil condenar a los inmigrantes y culparlos de quitarles el trabajo**”.

La problemática del desempleo fue una de las banderas que acogió Trump durante su campaña, señalando a los indocumentados como los principales culpables de esta situación. “**Vamos a salir a las calles, estamos atravesando un momento en donde no tenemos nada que perder porque todo está perdido, son los indocumentados los que realizan en trabajo que nadie quiere hacer y que están en la profundidad del sistema económico**”, afirmo Veléz.

Lo que está por verse es si serán realidad las propuestas de Trump, que podrían ser un hecho con la aprobación del **senado en donde por mayorías el partido Republicano arraso y se queda como partido dominante**, facilitando que proyectos como el de dotar con más armas y entrenamiento a los oficiales de seguridad de ese país o no promover el uso de las energías verdes sean las próximas leyes de Estados Unidos.

<iframe id="audio_13684838" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_13684838_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en su correo [[bit.ly/1nvAO4u]
