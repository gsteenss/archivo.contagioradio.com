Title: Advierten que proyecto para prohibir plástico en San Andrés corre peligro
Date: 2019-06-07 18:32
Author: CtgAdm
Category: Ambiente, Nacional
Tags: plástico, San Andrés
Slug: advierten-que-proyecto-para-prohibir-plastico-en-san-andres-corre-peligro
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/Captura-de-pantalla-123.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: *Agencia de Noticias UN*] 

Activistas advierten que el proyecto de ley que busca prohibir [ el ingreso y comercialización de varios elementos de plástico al Archipiélago de San Andrés corre el peligro de ser archivado. Así lo señaló Natalia Parra, vocera de la plataforma animalista ALTO, quien indicó que **el proyecto tiene plazo hasta el 20 de junio para ser aprobado en la plenario del Senado**.]

Según la animalista, el proyecto atiende a una emergencia ambiental que se da en el archipiélago por el uso descontrolado de este material y por la falta de medidas para su disposición final de manera adecuada. Esto tiene efectos directos para **los ecosistemas frágiles de la Reserva de Biósfera Seaflower, una zona de gran biodiversidad marina**.

"La situación es realmente dramática en cuanto a que hacer con la basura. Entonces mientras que más se pueda minimizar la cantidad de objetos que van al botadero o al relleno, es importantísmo", afirmó Parra.

El proyecto contempla un plazo de dos años para la transición al uso de productos hechos de materiales biodegradables. Cabe recordar que **el plástico PET puede tardar hasta 1.000 años en desintegrarse**. ** **Además, estos plásticos terminan contaminando el océano y las playas de San Andrés, despidiendo químicos peligrosos y generando microplásticos que luego son consumidos por la fauna silvestre. (Le puede interesar: "[¿Que hacer frente al Tsunami de plástico que amenaza a Colombia?](https://archivo.contagioradio.com/tsunami-plastico-enfrenta-colombia/)")

### **Obstáculos en el Senado** 

A pesar de ser aprobado por la Cámara de Representantes y la Comisión Quinta del Senado, el proyecto de ley ahora enfrenta nuevos obstáculos en la plenaria del Senado. Como lo denunció Parra, el proyecto estaba programado para ser debatido el pasado 30 de mayo, sin embargo, fue aplazado.

Según la animalista, esto se debe a las presiones por parte de los gremios del plástico para frenar el proyecto. "Comenzamos a notar como fue cambiando la posición de los congresistas y vimos como pasó de ser segundo punto del orden del día a octavo punto", afirmó.

Si no es aprobado antes del 20 de junio, el proyecto se hunde y tendría que ser presentado nuevamente al Congreso en dos años. Esto se suma a los dos años que se demoraría implementar la iniciativa si es sancionada. "Eso quiere decir la isla seguiría recibiendo el mismo número de basura y seguiría esta tragedia no tan visible pero tan fuerte para el mar y sus especies".

Se espera que el proyecto se debata **el próximo 10 de junio** y que pueda ir a conciliación para el siguiente día.

<iframe id="audio_36821563" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_36821563_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
