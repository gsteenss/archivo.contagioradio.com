Title: Los campos de concentración para personas LGBTI en Chechenia
Date: 2017-04-17 17:03
Category: El mundo, LGBTI
Tags: Chechenia, LGBTI
Slug: los-campos-concentracion-personas-lgbti-chechenia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/04/chechenia-e1492466423974.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Alexander Nemenov 

###### [17 Abr 2017] 

En Chechenia **más de 100 hombres gay han sido detenidos por las autoridades.** Según la investigación de la periodista Elena Milashina, de la mayoría se desconoce si están escondidos, presos o muertos, tras haber sido víctimas de los actos que se cometen en los “campos de concentración” destinados para torturar e incluso asesinar a integrantes de la comunidad LBTI en Chechenia, lugar conocido por su homofobia.

**"Conocemos de cuatro prisiones secretas. Dos se encuentran en Grozny, la capital chechena, y hay una en Argún** -que fue la primera que identificamos- donde tenían detenidas a personas LGBT, donde las golpeaban, torturaban y asesinaban", contó la periodista Milashina al programa Victoria Derbyshire de la BBC.

Aunque la investigación resultó difícil, debido a la represión gubernamental y por el temor que hay de las personas LGBTI, los hallazgos son escalofriantes.  Testimonios de las víctimas que han logrado sobrevivir hablan de **apaleamientos, confinamientos en condiciones infrahumanas, descargas eléctricas, violaciones con botellas, golpes, y hasta asesinatos.**

The Guardian, obtuvo la versión de Adam,  (un nombre ficticio para proteger su identidad), un hombre que cuenta cómo fue engañado citándolo a un lugar una persona conocida por él, que lo recibió con seis hombres más, algunos con uniforme, le gritaban y agredían repitiéndole que sabían que era gay. Luego lo metieron a la fuerza en una camioneta y se lo llevaron a un centro de detención donde había otros hombres encerrados.

**"Diferentes personas entraban y tomaban turnos golpeándonos. Algunas veces traían a otros reclusos a quienes les decían que éramos gay y les ordenaban que nos dieran una paliza"**, le narró Adam a The Guardian. Finalmente tras varios días él y otros hombres fueron dejados en libertad, pero al entregarlos a sus familias les advirtieron: "Su hijo es un maricón. Hagan lo que tienen que hacer con él". Es decir que las autoridades los devuelve, pero esperando a las mismas familias sean las que los asesinen.

### Respuestas de las autoridades 

El gobierno checheno es conocido por sus bases tradicionalistas, basadas en temas como la familia y la lealtad a clanes y a la religión islámica. En esa línea tener un integrante gay en la familia afecta el honor de la misma, por eso desde el mismo gobierno el mismo promueven la violencia familiar contra los hijos y además reprimen movilizaciones de la comunidad LGBTI.

Sin embargo, pese a esos antecedentes, las autoridades han negado que esa situación se esté  dando, asegurando que la investigación de la periodista Elena Milashina es falsa, porque **argumentan que en esa república no existen homosexuales.**

Alvi Karimov, portavoz de Ramzán Ajmátovich Kadýrov, el primer ministro de la república de Chechenia, afirmó en un comunicado que las personas LGBTI, "simplemente no existen en la república”, y agrega, **“Si tales personas existiesen en Chechenia, la ley no tendría que preocuparse por ellas ya que sus parientes lo habrían enviado a un lugar de donde nunca regresarían".**

Por el momento la comunidad internacional sigue presionando para que se actúe contra este tipo de actos crueles e inhumanos contra las personas homosexuales en Chechenia. Por su parte, la periodista de la investigación debió salir del país, **huyendo de los más de 15.000 religiosos y altos miembros de la sociedad chechena que anunciaron una** guerra santa, contra ella y los periodistas del diario donde publicaron la investigación.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
