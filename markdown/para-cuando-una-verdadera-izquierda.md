Title: ¿Para cuándo una verdadera izquierda?
Date: 2015-02-04 20:26
Author: CtgAdm
Category: Opinion, superandianda
Tags: cátedra para la paz, noticias colombia, proceso de paz
Slug: para-cuando-una-verdadera-izquierda
Status: published

Por [Superandianda ](https://archivo.contagioradio.com/superandianda/)

Colombia pasa por la época más importante de los últimos años, donde nuevas fuerzas políticas podrian lograr un escenario para surgir. Quizás ese es el miedo de quienes, por décadas, han mantenido el poder y nos han hecho creer que democracia es elegir el mismo sistema con diferentes caras y propuestas que sólo quedan en campañas publicitarias, que al final terminan siendo lo mismo.

‏El gran temor es la conversión y unidad de aquellos que nunca antes habían participado

Es lamentable que los colombianos no comprendan la dimensión política que traería la salida del conflicto que por más de 50 años ha azotado a las poblaciones más pobres y mantenido en el poder a la misma clase politica dirigente con el mismo discurso guerrerista que nunca logro un acercamiento real a un acuerdo tan próximo como el que estamos a punto de firmar en la Habana. Es entonces la oportunidad para hacer llegar un nuevo mensaje y de organizarnos en una sola fuerza.

La fragmentación de las ideas izquierdistas, la vanidad y egoísmo de sus líderes  junto con la  mala propaganda por parte de los medios de comunicación comparando las negociaciones actuales de la Habana con antiguos intentos fallidos, no sólo siembran desinformación sino el terror constante sobre puntos claves que la mayoría de los ciudadanos ignora porque no se ha tenido ni pedagogía real para recibir la "cátedra para la paz"  ni el compromiso profesional periodístico para informar sin posiciones políticas.

‏Camuflados neoliberales que se dicen de centro, "puristas", líderes en partidos de izquierda que solo caen en la crítica destructiva favoreciendo indirectamente a aquellos que quieren acabar con el proceso de paz, logran que no exista una verdadera ideología y que sea tal su desorganización que muchos simpatizantes han optado por no apoyar ninguno de dichos partidos o sencillamente emigran a grupos independientes.

¿Dónde está entonces la verdadera izquierda? ¿La que no vive de vanidades, prepotencias y busca unidad? Es importante la libertad de crítica pero es aún más importante la crítica con propuestas que pueda orientar a quienes ya no creen en política y formen nuevos espacios de práctica e intercambio de ideas. Es necesario y urgente estar dispuestos a una organización que explique a los ciudadanos la importancia de apoyar el proceso político que se está desarrollando, el post-conflicto y plantear una verdadera pedagogía de la cátedra para la paz.

Colombia sufre más temor a la izquierda política que a la pobreza, ignorancia, violación de derechos humanos y mercantilización de sus derechos básicos. Los medios masivos de información se encargan de asociar socialismo con miseria y desigualdad, algo que ya lo tiene Colombia dentro del mismo esquema de gobierno neoliberal que la ha regido en su historia. Falta la verdadera fuerza  que logre identificar aquellos que ya no creen y aclarar a muchos que ya se acostumbraron al sistema corrupto que se ha perpetuado. Hay que desmentir la errónea idea de que manteniendo a los mismos partidos políticos estaremos a salvo del “terror” del comunismo del que se han venido “infectando” países vecinos.  
‏Como ciudadana apoyo el proceso político de paz de la Habana con la espera de una nueva unidad nacional, de una autentica izquierda, que erradique intereses individuales y llame a la colectividad. Creo que las ideas están por encima de la vanidad de los líderes e intereses particulares.

El tiempo se acorta y es necesario llevar la voz a quienes nunca han escuchado que la palabra terrorífica “castrochavismo” no existe, que la unidad latinoamericana no es terrorismo, que sentirnos propios en nuestro país y participar en decisiones colectivas es un deber como pueblo y que estamos a puertas de continuar como hace más de 50 años o de iniciar una nueva página escrita por nosotros.
