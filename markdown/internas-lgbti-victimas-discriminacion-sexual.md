Title: Internas LGBTI en Medellín son víctimas de discriminación sexual
Date: 2016-02-10 17:29
Category: LGBTI
Tags: discriminación sexual, LGBTI
Slug: internas-lgbti-victimas-discriminacion-sexual
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/12/Presos-politicos-2-e1519746085716.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Agencia para la Libertad 

###### [10 Feb 2016] 

En la cárcel Pedregal de Medellín, la internas de la comunidad LGBTI denuncian ser víctimas de discriminación por su orientación sexual. De acuerdo con ellas, al momento de recibir sus visitas conyugales e**l ingreso de sus parejas es negado por las autoridades carcelarias.**

Según la denuncia, a las reclusas se les estaría violando a su derecho de la vida íntima. El argumento que utilizan las guardias para impedir el ingreso de sus parejas mujeres es que no hay lugares disponibles y que no están autorizadas o programas en la agenda del día.

**"La situación se agudiza cuando a pesar del buen comportamiento de las reclusas no les cumplen el derecho a la vida íntima estando tras las rejas"**, dijo una de las internas en una entrevista realizada en La Fm.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
