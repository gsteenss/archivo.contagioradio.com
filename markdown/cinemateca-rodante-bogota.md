Title: Con un laboratorio para contar historias vuelve la Cinemateca Rodante
Date: 2017-05-08 15:50
Author: AdminContagio
Category: 24 Cuadros
Tags: Audiovisual, Bogotá, Cinemateca, formación
Slug: cinemateca-rodante-bogota
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/05/postal_fase22.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Idartes 

###### 08 May 2017 

En su sexto año de trabajo, la **Cinemateca Rodante**, apuesta formativa de la Cinemateca Distrital de Bogotá, presenta el **Laboratorio de Creación Audiovisual**. Una propuesta que busca **contar historias de la ciudad** en un espacio de creación colectiva y formación transdisciplinaria compartida entre la comunidad audiovisual de la capital.

Las actividades, que corresponden a la fase II de la estrategia, buscan brindar **herramientas y técnicas de creación audiovisual**, en esta ocasión enfocadas a la creación y producción de historias de ficción, ofreciendo cupos para 5 **talleres especializados en dirección, producción, fotografía, sonido y arte para cortometrajes de ficción**.

Los talleres de la Cinemateca rodante, serán impartidos en **Teusaquillo, Puente Aranda, San Cristóbal, Usme y Antonio Nariño**, localidades estratégicas de la ciudad, que favorecerán a la participación, comunicación, retroalimentación y articulación entre las comunidades y el sector audiovisual de Bogotá. Le puede interesar: [Inicia la Muestra Itinerante de Cine Africano en Colombia](https://archivo.contagioradio.com/africano-colombia-cine/).

Las inscripciones estarán abiertas **hasta el próximo 17 de mayo**. Las jornadas tienen como fecha de inicio del 26 del mismo mes con un taller de sensibilización audiovisual con énfasis en el cortometraje y la narrativa latinoamericana, allí participarán los inscritos en todos los laboratorios, que tendrán lugar desde el 9 de junio hasta el 8 de julio los lunes, miércoles y viernes (no festivos) en el horario de 5 a 8 p.m.

La iniciativa busca que participen escritores, contadores de historias, realizadores o productores audiovisuales, líderes, gestores culturales y agentes activos de la cultura de toda la ciudad, y **cualquier ciudadano interesado en desarrollar o fortalecer habilidades para la creación audiovisual** y que, además, tenga la capacidad de **replicar la información y el aprendizaje para el beneficio de su comunidad**.

Para mayor información o inquietud sobre la Cinemateca Rodante puede comunicarse al teléfono 3795750 ext. 3405 o al correo electrónico cinematecarodante@idartes.gov.co

<iframe id="audio_18563765" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_18563765_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]

 
