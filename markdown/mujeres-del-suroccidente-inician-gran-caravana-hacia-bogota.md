Title: Caravana de mujeres avanza hacia Bogotá
Date: 2016-11-23 16:41
Category: Mujer, Nacional
Tags: caravana de mujeres tejiendo paz, Día de no Violencia contra la mujer, enfoque de género en los acuerdos de paz, organizaciones de mujeres
Slug: mujeres-del-suroccidente-inician-gran-caravana-hacia-bogota
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/03/mujeres-colombianas-desnudas-protestando.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Rui Dong] 

###### [23 Nov 2016] 

Cientos de mujeres del Suroccidente colombiano se dirigen hacia la capital en la **Caravana de Mujeres Tejiendo Paz y Reconciliación**, con el objetivo de visibilizar el respaldo al enfoque de género en los  acuerdos, exigir la **implementación inmediata, el respeto por el cese bilateral y el inicio de la mesa de conversaciones con el ELN.**

Después de una serie de actividades político-culturales en la plaza de  Nariño de  la ciudad de Pasto, las mujeres Nariñenses Por la Paz, una de las 25 colectividades que impulsan la iniciativa,  comienza el recorrido** hacia  el departamento del Cauca y el Valle del Cauca, en donde otras organizaciones de mujeres esperan poder integrarse a esta caravana.**

Jaqueline Ruano, de la organización Mujeres Nariñenses por la Paz, aseguró que la iniciativa también apoya la gran movilización que se realizará el viernes **25 de Noviembre desde el Planetario de Bogotá hasta la Plaza de Bolívar a partir de la 1pm**, para rechazar todo tipo de violencia contra las mujeres y **exigir justicia para los casos de feminicidio e intento de feminicidio ocurridos en las ultimas semanas.** Le puede interesar: [Dora Liliana Gálvez, otra víctima de la violencia feminicida.](https://archivo.contagioradio.com/dora-liliana-galvez-otra-victima-de-la-violencia-feminicida/)

###### Reciba toda la información de Contagio Radio en [[su correo]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio]{.s1}](http://bit.ly/1ICYhVU) {#reciba-toda-la-información-de-contagio-radio-en-su-correo-o-escúchela-de-lunes-a-viernes-de-8-a-10-de-la-mañana-en-otra-mirada-por-contagio-radio .p1}
