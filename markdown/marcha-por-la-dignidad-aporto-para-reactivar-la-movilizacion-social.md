Title: Marcha por la Dignidad aportó para reactivar la movilización social
Date: 2020-07-27 22:47
Author: AdminContagio
Category: Actualidad, Nacional
Tags: Caravana sur de Bolívar, Marcha por la dignidad, Movilización social, Ruta Comunera, Ruta Libertadora
Slug: marcha-por-la-dignidad-aporto-para-reactivar-la-movilizacion-social
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/07/Marcha-por-la-Dignidad-3.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/07/Caravana-Sur-de-Bolivar-Marcha-por-la-Dignidad.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"textColor":"cyan-bluish-gray","fontSize":"small"} -->

Marcha por la Dignidad / Emzac uaque

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Desde el pasado 25 de junio se emprendió la Marcha por la Dignidad, cuyo primer recorrido se inició desde Popayán, Cauca; donde **las comunidades se manifestaron, entre otras cosas, por el asesinato de líderes sociales,  la erradicación forzada, la desmedida militarización de los territorios y las violaciones a mujeres de comunidades indígenas.** (Le puede interesar: [Todo lo que necesita sobre el avance de la Marcha por la Dignidad](https://archivo.contagioradio.com/a-pasos-de-gigante-avanza-la-marcha-por-la-dignidad/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

A este primer recorrido se sumaron posteriormente las Rutas «Libertadora» y «Comunera» que partieron el 13 de juliodesde Gibraltar en Norte de Santander y Barrancabermeja en Santander respectivamente. La marcha original arribó a Bogotá el 10 de julio mientras que las rutas «Libertadora» y «Comunera» lo hicieron el pasado día 20, «Día Nacional de Independencia». (Lea también: [Marcha por la Dignidad arribará el 20 de julio a Bogotá buscando una «verdadera independencia»](https://archivo.contagioradio.com/marcha-por-la-dignidad-arribara-el-20-de-julio-a-bogota-buscando-una-verdadera-independencia/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Estas movilizaciones fueron el inicio de una gran expresión popular que motivó el surgimiento de nuevas manifestaciones sociales algunas de las cuales se originaron en el sur de Bolívar.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Los logros y avances de la Marcha por la Dignidad

<!-- /wp:heading -->

<!-- wp:paragraph -->

José Daniel Gallego,  defensor de Derechos Humanos y estudiante de la Universidad del Cauca, señaló que la **Marcha por la Dignidad llegó a la ciudad de Bogotá, no para reunirse con el gobierno de Duque, ni su gabinete**, porque ya han sido recurrentes los escenarios en los que se ha buscado interlocución con ellos sin encontrar respuestas a sus pedidos; manifestó en cambio, **que la búsqueda era elevar un SOS frente a la Comunidad Internacional**  para que escuchara los llamados y denuncias de las comunidades y pudieran hacer eco de los mismos.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En ese sentido, destacó como logro el hecho de haber despertado una **«*consciencia de movilización*»** que no sólo sumó a más personas que acompañaron esa Marcha, sino que motivó el surgimiento de nuevos recorridos y rutas provenientes de otros territorios.

<!-- /wp:paragraph -->

<!-- wp:quote -->

> **Salimos el 25 de junio desde Popayán y pensamos que íbamos a llegar solos a Bogotá, pero la respuesta del movimiento social fue satisfactoria, abrazando este proceso  y marchando con nosotros en las diferentes ciudades y regiones**»
>
> <cite>José Daniel Gallego, defensor de Derechos Humanos</cite>

<!-- /wp:quote -->

<!-- wp:paragraph -->

Adicionalmente, José Daniel subrayó como logro el **haber obtenido un espacio con las bancadas alternativas en el Congreso de la República**, donde se suscribió un documento conjunto en el cual se hace un llamado a la ONU para la visita de relatores al país, poniendo en su conocimiento las diversas violencias y problemáticas que se viven en los territorios. Asímismo, manifestó que fueron atendidos por embajadas de otros países en Colombia para también facilitar la llegada de sus pedidos al organismo internacional.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En igual sentido, Javier Antonio Peña, del Proceso de Comunidades Negras -[PCN](https://renacientes.net/)- participante en la Marcha por la Dignidad, resaltó que debido a la contingencia de la pandemia **hubo acercamiento y reuniones virtuales con organizaciones de DD.HH., embajadas de la Unión Europea y la ONU** donde se buscaba que «*supieran sobre la problemática que atraviesa en este momento nuestro país*».

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Fortalecimiento de la movilización social

<!-- /wp:heading -->

<!-- wp:paragraph -->

Por otra parte, Erika Prieto, integrante de la Ruta Comunera, enfatizó en que uno de los objetivos de la marcha había sido **llamar a la solidaridad del pueblo colombiano para consolidar un despertar popular** y afirmó que lo que se viene para el movimiento social y popular, luego del auge de estas marchas, «*es un ejercicio de lucha y resistencia*» que según ella es lo único que le queda al pueblo colombiano para pronunciarse en contra del «*mal gobierno*». Por tal motivo, destacó el surgimiento de la Caravana y Misión de Verificación del sur de Bolívar como una nueva movilización que entra a sumarse a la Marcha por la Dignidad.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Teófilo Acuña, vocero de la Comisión de la Caravana del Sur de Bolívar, señaló que esta movilización se originó «para acompañar las luchas de organizaciones sociales en estos territorios» y evitar así, una inminente masacre y un desplazamiento masivo de las  comunidades de esta zona, ya que **son al menos 12 asesinatos los que se han registrado en solo 3 meses.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Adicionalmente, denunciar lo que para él es **una «*clara connivencia*» entre paramilitares, fuerza pública e incluso autoridades civiles en algunos de estos municipios.**

<!-- /wp:paragraph -->

<!-- wp:quote -->

> **En nuestro territorio tenemos que decir que el paramilitarismo nunca se desmovilizó**»
>
> <cite>Teófilo Acuña, vocero de la Comisión de Interlocución del sur de Bolívar</cite>

<!-- /wp:quote -->

<!-- wp:image {"id":87276,"sizeSlug":"large"} -->

<figure class="wp-block-image size-large">
![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/07/Caravana-Sur-de-Bolivar-Marcha-por-la-Dignidad.jpg){.wp-image-87276}  

<figcaption>
Ingografía: Rueda Suelta

</figcaption>
</figure>
<!-- /wp:image -->

<!-- wp:block {"ref":78955} /-->
