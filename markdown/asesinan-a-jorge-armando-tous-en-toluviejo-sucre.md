Title: Asesinan a Jorge Armando Tous en Toluviejo, Sucre
Date: 2020-11-25 10:23
Author: AdminContagio
Category: Actualidad, Comunidad
Slug: asesinan-a-jorge-armando-tous-en-toluviejo-sucre
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/08/WhatsApp-Image-2018-08-06-at-3.26.09-PM.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

Este 24 de noviembre se registró el asesinato de Jorge Armando Tous sobre las 8 de la noche en el corregimiento de Toluviejo, municipio al norte del departamento de Sucre, ubicado a 18 km de Sincelejo. (Lea también: [Edgar Hernández, líder social, es asesinado en Puerto Caicedo](https://archivo.contagioradio.com/edgar-hernandez-lider-social-es-asesinado-en-puerto-caicedo/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

**Jorge Armando Tous** era conocido por sus familiares y amigos como **“Guandú"**. Según versiones de miembros de la comunidad, los hechos ocurrieron cuando Jorge se encontraba departiendo con amigos en este corregimiento a donde llegaron en moto sujetos desconocidos y armados.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Los sujetos amenazaron a los amigos de "Guandú" y les indicaron que corrieran, mientras tanto le disparaban al joven Tous, uno de los disparos fue en la frente y los otros cinco en el resto del cuerpo.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Este asesinato se produce luego de la realización del un consejo de seguridad, este 24 de noviembre en la región, esto ante la cifra de **124 asesinatos perpetrados en lo que va corrido del 2020 en el departamento de Sucre.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Adicionalmente, en este territorio, que colinda con los [Montes de María](http://www.centrodememoriahistorica.gov.co/micrositios/recorridos-por-paisajes-de-la-violencia/montes-maria.html), se conoce el control que ejercen las autodenominadas Autofensas Gaitanistas de Colombia -AGC-, mediante operaciones que se han extendido por el conjunto de municipios de esta gran región del país, que vivió en la década del 90 y el 2000 fuertes arremetidas paramilitares, despojo violento de tierras y el desarrollo de agronegocios. (Le puede interesar: [La violencia se reagrupa y reestructura en los Montes de María](https://archivo.contagioradio.com/la-violencia-se-reagrupa-y-reestructura-en-los-montes-de-maria/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

De la misma manera, luego de la desmovilización paramilitar del año 2006, este lugar fue epicentro de múltiples ejecuciones extrajudiciales y hoy aún las organizaciones de Derechos Humanos y los líderes y lideresas de la región trabajan por recuperar el tejido social fragmentado y junto a la comunidad exigir verdad, reparación y justicia ante la violencia que persiste y la impunidad en muchos de los casos.

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
