Title: "Acuerdo de desminado es un paso trascendental hacia la paz territorial" Gr. (r) Rafael Colón
Date: 2015-03-11 20:23
Author: CtgAdm
Category: Nacional, Paz
Tags: acuerdo, conflicto, desminado, habana, paz, rafael colon
Slug: acuerdo-de-desminado-es-un-paso-trascendental-hacia-la-paz-territorial-gr-r-rafael-colon
Status: published

###### Foto: Agencia de Noticias UN 

<iframe src="http://www.ivoox.com/player_ek_4201221_2_1.html?data=lZedk5eWdY6ZmKiak5aJd6KllZKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRb4amk6bQ18rWqNCfxcqYxsrXscriwsncjcrXb9bijNXO1dSPuNPV1MjS0MnJstXVzZDVw8jNpYzgjoqkpZKns8%2FowszW0ZC2pcXd0JCah5yncZU%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [General (r) Rafael Colón] 

Según lo acordado en la Mesa de Conversaciones entre el gobierno y la guerrilla de las FARC el pasado 7 de marzo, sobre limpieza de artefactos explosivos y desminado en el territorio colombiano, 6 personas integrarían el Equipo Técnico que en Colombia generará la ruta para la realización de la tarea: 2 militares, 2 guerrilleros y 2 personas de organizaciones sociales.

Para el **Brigadier General (r) Rafaél Colón**, "este acuerdo es un paso trascendente hacia la construcción de paz territorial" y permite un optimismo en todos los sectores de la sociedad a nivel nacional e internacional, al dar muestras claras de las "buenas decisiones que se han tomado en la mesa de negociaciones".

"*Los acuerdos nos indican que hay una buena comunicación al interior de la Mesa  y que se están tocando los puntos mas sensibles del conflicto. El conflicto comienza a des-escalarze, a ceder*", enfatizó el Gr. (r) Colón.

Indicó a demás que la conformación del equipo es fundamental, pues en tanto la guerrilla permitirá identificar rutas y tipos de artefactos para facilitar el trabajo por polígonos, la disposición del Ejercito de aumentar el pie de fuerza en trabajo de 500 a 10 mil permitirá realizar la tarea más rápidamente.

Colombia, además, cumpliría con este proceso de desminado, con el Acuerdo de Otawa firmado en 1999.
