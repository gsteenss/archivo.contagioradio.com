Title: ¿Por qué paran 340 mil profesores de toda Colombia?
Date: 2019-03-19 12:49
Author: CtgAdm
Category: Educación, Movilización
Tags: educacion, fecode, paro, profesores
Slug: paran-340-mil-profesores
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/03/Captura-de-pantalla-2019-03-19-a-las-12.14.54-p.m..png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: @SandraFARC] 

###### [19 Mar 2019] 

Durante este **martes 19 y miércoles 20 de marzo cerca de 340 mil profesores de educación primaria, básica secundaria y media entrarán en jornada de paro,** llamando la atención al Gobierno sobre los recursos que ingresan para la prestación de este servicio por parte del Sistema General de Participación. Los maestros señalan además que el Gobierno ha incumplido con los acuerdos pactados en el pasado, y también protestan contra ese fallo.

Como explica Luis Alberto Gruibert, **exdirectivo de la Federación Colombiana de Trabajadores de la Educación (FECODE), **del Sistema General de Participación provienen los recursos para la educación, "pero esa bolsa está al limite". Eso significa que el Gobierno no ha garantizado los recursos para aumentar la contratación de mayor planta docente, aumentar la cobertura de educación ni mejorar su calidad.

Adicionalmente, Gruibert recuerda que dicho Sistema vencía en 2016, momento en que se podían tomar dos rutas de acción: hacer un reajuste y prolongar la vigencia del Sistema existente, o hacer una reforma estructural al mismo. En ese sentido, los profesores convocan al paro para llamar la atención sobre la necesidad de tomar la segunda ruta, y encontrar en el Plan Nacional de Desarrollo el cumplimiento de este punto.

El magisterio ha reclamado desde hace tiempo que es necesario tener acceso a mayores recursos para garantizar el acceso de todos los menores a educación básica; **en octubre del año pasado FECODE advertía que haría falta casi un billón de pesos para operar durante 2019**, al tiempo que pedían al Gobierno Central hacerse cargo nuevamente de servicios básicos como agua y educación en los municipios, porque estos no podían suplir esos gastos.

### **Pactos incumplidos y otros en los que poco se avanza** 

En esta ocasión, y contrario a lo que se ha dicho en algunos medios de información, los profesores no tienen en su agenda de movilización el tema salarial porque ya fue discutido con anterioridad, y el Gobierno ha cumplido en la nivelación en ese sentido. De acuerdo a lo expresado por el exdirectivo de FECODE, de igual forma se han hecho ajustes en "el tema de la evaluación diagnóstica formativa" y el ajuste de procesos de tipo administrativo.

Sin embargo, el Profesor sostiene que **el Gobierno ha incumplido con los compromisos en materia de alimentación escolar**, que fue una de las grandes luchas del magisterio, tomar las medidas administrativas para corregir los desajustes del sistema de salud, y la definición de la comisión tripartita para definir un estatuto único para el magisterio. (Le puede interesar: ["Por deuda histórica magisterio vuelve a las calles en febrero"](https://archivo.contagioradio.com/profesores-vuelven-calles/))

<iframe id="audio_33512676" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_33512676_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
