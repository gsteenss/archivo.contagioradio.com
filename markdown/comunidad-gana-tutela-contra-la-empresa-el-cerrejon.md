Title: Comunidad gana tutela contra la empresa el  Cerrejón
Date: 2015-11-25 17:32
Category: Ambiente, Nacional
Tags: CAJAAR, Cerrejón, El Cerrejón en La Guajira, Impactos ambientales Cerrejón, Pueblos Wayuu en Colombia, Tutela a favor de Moises Guette
Slug: comunidad-gana-tutela-contra-la-empresa-el-cerrejon
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/11/el-cerrejon.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: laurismu] 

<iframe src="http://www.ivoox.com/player_ek_9509682_2_1.html?data=mpqdm5ucdo6ZmKialJiJd6KllZKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRh9Dh1tPWxsbIb8jVz8aY1trYqc3VjMjc0NnWpYzgwpDSz9XWqdTVjMrZjajJttPZy4qwlYqmd8%2Bhhpywj6jTstXVyM7cjbfFqMrjjJKSmaiReA%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Dora Lucy Arias, CAJAR] 

###### [25 Nov 2015 ] 

[Falla tutela a favor de Moisés Guette, niño indígena Wayuu de 2 años que padece graves problemas respiratorios y de hemoglobina por cuenta de las **fuertes explosiones producidas para la extracción carbonífera** de la empresa minera Carbones del Cerrejón, sumada a la **liberación de polvillo de carbón** y a los **incendios** provocados por la reacción entre el material residual estéril de esta actividad y las altas temperaturas de La Guajira.]

[De acuerdo con Dora Lucy Arias, abogada del Colectivo de Abogados José Albear Restrepo que llevaba el caso, pese a que la **tutela fue fallada a favor del menor** y en tal sentido ordena a una EPS que brinde atención en servicios de neumología y hemoglobina pediátrica para atender su estado de salud, apela al principio de precaución y determina que se deben hacer estudios científicos para evaluar los impactos negativos de la extracción de carbón en la región, **sin ordenar la suspensión inmediata de las actividades del Cerrejón como debiera ser**.]

[Según indica Arias es inaudito que pese a que **el Estado colombiano reconoció desde 1993 el principio de precaución al día de hoy no haya adelantado estudios científicos de impacto ambiental** como desde entonces lo ordena la medida; sin embargo, destaca que son conocidos los graves impactos al ambiente y a la salud de la extracción de carbón que el Estado no está atendiendo, desconociendo su responsabilidad nacional e internacional en el fortalecimiento de sus mecanismos para la garantía de derechos.]

[**“Los estándares ambientales en Colombia son bastante laxos y el hecho de que una actividad sea legal no significa que no se generen efectos de impacto negativo a las comunidades”** asegura la abogada e insiste en la necesidad de que sean reconocidos los derechos a la habitabilidad del territorio que desde hace por lo menos 4 años vienen reclamando los pueblos indígenas Wayuu, quienes cada vez se ven más **hacinada por la acción minera del Cerrejón** que además les impide pastorear o cultivar, poniendo en **riesgo su seguridad alimentaria**. ]
