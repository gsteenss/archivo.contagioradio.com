Title: Se sigue en la espera de la llegada del Presidente al Putumayo
Date: 2015-07-06 07:28
Category: Comunidad, yoreporto
Tags: Putumayo, Zona de Reserva Campesina
Slug: se-sigue-en-la-espera-de-la-llegada-del-presidente-al-putumayo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/06/PUTUMAYO.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

 

###### Foto: Contagio Radio 

###### [6 Jun 2015] 

Las organizaciones sociales que son conformadas por campesinos de diferentes municipios y departamentos han delegado un grupo de personas pedagogas en la cual son los encargados de establecer una negociación con el presidente Juan Manuel Santos para demostrar las necesidades y derechos que tiene cada familia campesina.

Estas personas han logrado dialogar con comisiones que envía el estado, pero ellos dicen que el gobierno no da visto bueno para empezar a construir un mejor mañana. Los campesinos aportan ideas entre ellos y llegan al límite de hacer marchas y paros en las vías pero  al parecer el gobierno sigue mintiéndoles.

Hubo un acuerdo de parte del estado con los campesinos  que se reuniría con ellos en el mes de mayo del presente año pero otra vez  les ha incumplido. Nuevamente hubo un comunicado por un medio radial de Puerto Asís donde anunciaban la llegada del Presidente a Puerto Asís en este  mes de junio para escuchar a los campesinos y darles una fecha exacta para que comience la sustitución de los cultivos ilícitos.

Los campesinos muy molestos: dicen que ellos comienzan la sustitución gradualmente mientras el gobierno les de una forma de proyectarse donde ellos puedan tener una seguridad alimentaria y poderles pagar una educación superior  a sus hijos en un futuro. Mientras tanto se sigue en la espera de la llegada de Presidente al Putumayo.

###### Por Miller Osnas  - Comunicador popular CONPAZ 

###### Zona de Reserva Campesina Perla Amazónica. 

###### 
