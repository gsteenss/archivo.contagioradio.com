Title: Los conflictos de intereses entre la exdirectiva de EPM y el GEA
Date: 2020-09-15 10:35
Author: AdminContagio
Category: Actualidad, Nacional
Tags: Empresas Públicas de Medellín, EPM, GEA, Grupo Empresarial Antioqueño, Hidroituango
Slug: los-conflictos-de-intereses-entre-la-exdirectiva-de-epm-y-el-gea
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/09/EPM-y-el-GEA.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

El colapso del proyecto hidroeléctrico Hidroituango y el reciente anuncio del alcalde de Medellín, Daniel Quintero, sobre una demanda en contra de los contratistas del proyecto por una suma de 9,9 billones de pesos, lo que produjo la renuncia de los miembros de la Junta Directiva de Empresa Pública de Medellín -[EPM](https://www.epm.com.co/site/)-; sacó a la luz **la ineficiente gestión que adelantaba la directiva en la empresa, los serios conflictos de intereses de miembros de la Junta con el Grupo Empresarial Antioqueño -GEA-, y un posible entramado de corrupción que condujo a un detrimento patrimonial y la consecuente crisis financiera por la que atraviesa la EPM.     **

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Sin embargo, la crisis institucional de la Empresa trasciende el fracaso del proyecto  Hidroituango. La Fundación Contravía denunció a través de un informe el «*secuestro*» del que ha sido presa EPM, a manos del Grupo Empresarial Antioqueño -GEA- por más de 13 años, mediante la consolidación de una Junta Directiva afín con los intereses de este conglomerado económico. (Lea también: **[Lo que hay detrás de los exmiembros de la Junta Directiva de EPM](https://archivo.contagioradio.com/lo-que-hay-detras-de-los-exmiembros-de-la-junta-directiva-de-epm/)**)

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En el informe también se destaca un aumento en las deudas de EPM igual al 861% que hacen que el pasivo sea superior al patrimonio, poniendo en alto riesgo la sostenibilidad económica de la empresa. Esta crisis financiera según varios concejales de Medellín la han tenido que costear los usuarios mediante el pago de sus tarifas de servicios públicos, con un evidente incremente en los montos de facturación.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Adicionalmente, se detalla la responsabilidad de varios de los mandatarios locales en la crisis de EPM, especialmente la del exalcalde de Medellín y exgobernador de Antioquia, Sergio Fajardo a quién se le atribuye la privatización del capital de la empresa, y también, ser el artífice de la conformación de la saliente  y cuestionada Junta Directiva. (Le puede interesar: **[El complot de los que perdieron con Daniel Quintero](https://archivo.contagioradio.com/el-complot-de-los-que-perdieron-con-daniel-quintero/)**)

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Asimismo, se denuncia una especie de puerta giratoria en la que los gerentes y miembros de Junta, que culminan su gestión en EPM pasan luego a asumir cargos en empresas pertenecientes al GEA, tal es el caso de Juan Esteban Calle, gerente de EPM en 2016 quien actualmente es Presidente de la cementera Argos y otros exdirectivos de la empresa como Federico Restrepo, Juan Felipe Gaviria, Esteban Iriarte entre otros.

<!-- /wp:paragraph -->

<!-- wp:core-embed/facebook {"url":"https://www.facebook.com/contravia.tv/videos/811211116284437","type":"video","providerNameSlug":"facebook","className":""} -->

<figure class="wp-block-embed-facebook wp-block-embed is-type-video is-provider-facebook">
<div class="wp-block-embed__wrapper">

https://www.facebook.com/contravia.tv/videos/811211116284437

</div>

</figure>
<!-- /wp:core-embed/facebook -->
