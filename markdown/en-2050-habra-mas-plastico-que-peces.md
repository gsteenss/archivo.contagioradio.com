Title: En 35 años habrá más plástico que peces en el océano
Date: 2016-01-22 15:34
Category: Ambiente, El mundo
Tags: animales en vías de extinción, Ellen MacArthur, Foro Económico Mundial, Revista Science
Slug: en-2050-habra-mas-plastico-que-peces
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/01/El-Plastico-Mata.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Michell Pitts 

###### [22 Ene 2016]

En el 2050 los mares y océanos tendrán más desechos plásticos que peces, es la conclusión de un estudio realizado el año pasado por la revista «Science» con apoyo de la fundación de la navegadora Ellen MacArthur, presentado durante el **Foro Económico Mundial **que se desarrolla en Davos, Suiza.

De acuerdo con el informe, si el mundo continúa con el ritmo actual de consumo, en 10 años habrá** un residuo de plástico en el mar por cada pez, lo que significa que en el 2050** el peso de los residuos plásticos será mayor al peso de todos los peces en el mundo.

“La nueva economía de los plásticos”, como se titula el informe, indica que la mayoría de los envases de plástico se utiliza una sola vez, asegurando que “el sistema actual de producción, utilización y abandono de los plásticos tiene efectos negativos importantes: entre 80.000 y 120.000 millones de dólares se pierden cada año en forma de envoltorios plásticos que no son reutilizados”.

Así mismo, señala que cada año en el mar terminan **ocho millones de toneladas de desechos plásticos**, un número que podría aumentar 10 veces más en los próximos 10 años, lo que equivaldría a la isla de Manhathan 34 veces cubierta por este tipo de basuras. Una situación que solo podría evitarse si  se toman medidas radicales e inmediatas que transformen las formas de consumo del total de la población mundial que **en el último siglo ha aumentado** **veinte veces el uso de plásticos.**

**De continuar así, en los próximos 20 años se duplicará la cantidad de plástico usado,** lo que conllevará a que en el  2050 la industria del plástico consumirá el 20% de la producción total de petróleo y el 15% del presupuesto anual de carbono.

#### **“Si no se toman medidas los vertidos serán equivalentes a dos camiones por minuto en 2030 y cuatro camiones por minuto para el año 2050”**, dice el estudio, con el que se quiere llamar la atención sobre el sistema de producción, consumo y reciclaje, que podría lograr un cambio a través de la economía circular, es decir, que los residuos regresen como recursos al ciclo productivo. Según los investigadores, sería necesario repensar totalmente el sistema de embalaje y la búsqueda de alternativas al petróleo como material de base para su producción. 

Para cambiar este triste panorama, los residuos que ingresan al mar deberán reducirse por lo menos en un 40%, es por ello, que el informe recomienda la implantación de sistemas de recogida selectiva y reciclaje, en todos aquellos lugares donde no existen, pero sobre todo en los países que han sido identificados como **los mayores contaminadores, como China, Indonesia, Filipinas, Vietnam y Sri Lanka,** que acumulan el 83 por ciento de los residuos de plástico mal procesados.

**Los animales, las primeras víctimas,**

Según la organización *El Plástico Mata,* cerca de **un millón de aves marinas y más de 100.000 mamíferos marinos y tortugas mueren cada año** por ingesta, toxicidad, asfixia y atrapamiento en desechos plásticos.

Por ejemplo, los peces mueren al ingerir fragmentos de plástico que han sido arrojados al mar. "Incluso los seres microscópicos que componen el plancton marino comen microplásticos**.** Los plásticos en el mar, además de su propia toxicidad, atraen y acumulan los tóxicos presentes el agua de mar" dice la organización.

Las ballenas y todas las especies de tortugas marinas comen fragmentos de plástico o bolsas que usualmente les generan una muerte lenta y dolorosa.

Pero no solo los animales acuáticos se ven afectados por la cotidiana actividad de algunos seres humanos de arrojar desechos plásticos, ya que estos residuos acaban con la vida de animales como los dromedarios de Dubai, el cóndor californiano, el ibis , o el elefante africano en Bostwana, entre muchas otras especies.

**Dato:** Cabe recodar, que en 1997 Charles Moore, investigador oceánico descubrió la “isla de basura” ubicada a mil kilómetros de Hawaii, donde permanecen millones de toneladas de plástico que es alentado por la corriente del Pacífico Norte impidiendo que los plásticos se dispersen hacia las costas continentales.
