Title: ESMAD amenaza desalojar proceso de "liberación de la madre tierra" en el Norte del Cauca #YoReporto
Date: 2015-02-26 22:52
Author: AdminContagio
Category: Nacional, Resistencias
Tags: Recuperación de la tierra en el Norte del Cauca, Tierras apropiadas por empresas
Slug: esmad-amenaza-desalojar-proceso-de-liberacion-de-la-madre-tierra-en-el-norte-del-cauca-yoreporto-2
Status: published

###### Foto: pulzo.com 

Este 2 de Febrero se informa desde la Hacienda Quebradaseca, en las goteras de Corinto, han hecho presencia elementos del **ESMAD**, con el objetivo, de **desalojar** a los comuneros de diferentes cabildos aledaños a **Corinto y Miranda**, que en cumplimiento de la orden de liberación de la madre tierra, decidieron realizar teniendo en cuenta los fallidos intentos de las autoridades tradicionales de encontrar respuestas en el Estado Colombiano, a los incumplimientos a los acuerdos entre el gobierno y las comunidades indígenas del Norte del Cauca y en que se comprometía a solucionar el grave problema de falta de tierras para éstas y la atención con recursos para adelantar planes de desarrollo alternativos en la región de la mano de las autoridades tradicionales.

Importante precisar, que la liberación de la madre tierra, que hoy se adelanta en el territorio ancestral del Norte del Cauca, tiene antecedentes de lucha y resistencia, que diferentes gobiernos se han negado a solucionar y hoy, se podría volver a regar con sangre nasa de los cientos de niños, niñas, jovenes, adulltos, mayoras y mayores, que se encuentran haciendo resistencia.

Los responsables de estos hechos están en cabeza del Estado Colombiano, su jefe Juan Manuel Santos, autoridades civiles y militares que incluso se han hecho presentes ante las comunidades sólo para amenazar y anunciar el desalojo con las consecuencias que estoy adelantando tristemente van a darse en Quebradaseca, Alto Miraflores Y Garcia arriba, terrenos en manos de terratenientes y al servicio del monocultivo de la caña (Ingenio del Cauca) del señor Ardila Lulle.

El llamado desde la solidaridad para con un pueblo una vez más agredido y olvidado por el Estado, a todas las conciencias justas, a toas y todos que nos duele estas atrocidades que se siguen cometiendo con las comunidades indígenas de este país y que nos dan ejemplo de lucha es denunciar lo que ésta ocurriendo ahora, en este momento en esas haciendas y que se sepa quienes comandan desde la policía este operativo para que se precise todos los abusos que allí ya se estén cometiendo. Es necesario llegar a la zona, para denunciar iniciar a nivel nacional e internacional el desalojo de las comunidades y el incumplimiento por parte del gobierno.

Es necesario la presencia de organizaciones sociales y de medios de comunicación como también pedirle al gobernador del Cauca intervenga, para parar el operativo y se abrir una mesa de conversaciones entre las autoridades de las comunidades allí presentes y el gobireno nacional, con el fin de atender las exigencias de estas comunidades.

Cut- Valle
