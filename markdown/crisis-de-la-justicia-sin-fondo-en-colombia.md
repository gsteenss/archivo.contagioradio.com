Title: Hay que empezar a resolver ya la Crisis de la Justicia: Alfredo Beltrán
Date: 2017-09-25 15:49
Category: Entrevistas, Política
Tags: Camilo Tarquino, Corte Suprema de Justicia, Leonidas Bustos
Slug: crisis-de-la-justicia-sin-fondo-en-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/08/corte_constitucional-Fuero_Penal_Militar_contagioradio-e1485870742527.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:CM&] 

###### [25 Sept 2017] 

Desde la detención del ex Fiscal anticorrupción Gustavo Moreno, la crisis de la justicia continúa agravándose. Instancias como la **Corte Suprema de Justicia con la supuesta implicación de su ex presidente Leonidas Bustos, y de congresistas como Musa Besaile** en actos de corrupción, ponen en jaque la más alta esfera de la justicia del país.

De acuerdo con el ex magistrado Alfredo Beltrán, la crisis que se está presentando en la administración de justicia, tiene que ver con un ambiente generalizado de corrupción en el país y con **las formas en las que se eligen a los magistrados y la responsabilidad de quienes los postulan**. (Le puede interesar:["Los partidos se convirtieron en empresas electorales")](https://archivo.contagioradio.com/46707/)

Sin embargo, Beltrán afirmó que es importante entender que no es la corporación la culpable sino algunos de los integrantes “la responsabilidad penal o disciplinaria, es de tipo individual, por eso cuando se han revelado hechos, tiene que ser objeto de juzgamiento” afirmó el ex magistrado.

### **Caso de Francisco Ricaurte y Camilo Tarquino** 

Según Beltrán en el caso de Ricaurte, ex magistrado de la Corte Suprema de Justicia, los actos de corrupción por los que se le acusa, hasta el momento, fueron cometidos después de haber estado en esta institución, razón por la cual la Fiscalía imputó delitos y la juez 40 de garantías Constitucionales admitió como posible y peligroso que **Ricaurte continuara en estado de libertad para afrontar el proceso y dispuso como medida preventiva la privación** de la libertad.

Por su parte, Camilo Tarquino está acusado de haber incurrido en conductas presuntamente delictuales después de que dejo de ser magistrado de la Corte Suprema, **es decir que el procedimiento debe ser el mismo que el de Ricaurte**.

### **Caso de Bustos** 

A Bustos se le acusa de haber cometido actos delictuales en el ejercicio de la magistratura, eso explica por qué el juzgamiento inicial tendría que hacerlo la Cámara de Representantes, en la comisión de acusación, si esta acepta acusar ante el Senado, esta instancia resolverá sobre la acusación. De admitirla, Leonidas Bustos quedaría suspendido del empleo, sin embargo como ya no está desempeñando este cargo, Bejarano manifestó que **solo se le haría una anotación a la hoja de vida y pasaría el expediente a la Corte Suprema de Justicia**.

“Se les debe aplicar el rigor de la ley y de la constitución, no puede ser admisible en una sociedad que los máximos jerarcas de la administración de **justicia incurran en delitos que deslegitimas a la administración de justicia, deslegitiman al Estado**” afirmó Beltrán.

### **Recuperar la confianza en la Justicia** 

El ex magistrado Beltrán manifestó que el paso siguiente es re construir la confianza en la justicia, “**la solución de conjunto, es tener mucho cuidado, precaución y responsabilidad en el momento en que se elija un magistrado** y tener los magistrados la convicción de que al menor asomo de duda sobre el ejercicio de sus funciones deben retirarse del cargo, en beneficio a la sociedad”

Sobre el Tribunal de Aforados, Beltrán expresó que podría volver a presentarse esta propuesta, en el Congreso de la República ya corre un proyecto para intentar hacerlo, sin embargo, la duda es si se debe esperar a que se activen estos mecanismos de juzgamiento o si se empieza de inmediato con los procesos con las herramientas que ya hay.

<iframe id="audio_21083555" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_21083555_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
