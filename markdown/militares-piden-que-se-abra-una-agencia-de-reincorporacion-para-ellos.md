Title: ¿Por qué militares piden que se abra una agencia de reincorporación para ellos?
Date: 2019-10-23 17:24
Author: CtgAdm
Category: Entrevistas, Paz
Tags: Armados, GAO, militares, reincorporación
Slug: militares-piden-que-se-abra-una-agencia-de-reincorporacion-para-ellos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/Militares-e1571869281157.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: @COL\_EJERCITO  
] 

Desde la fundación Comité de Reconciliación, que acoge a miembros de las Fuerzas Militares, **denunciaron que algunos miembros de la organización han recibido invitaciones a integrar grupos armados ilegales**, aprovechando las pocas oportunidades de trabajo que hay para ellos, y su experiencia militar. Para evitar que los "cantos de sirenas" lleven a los militares a retomar armas con estos grupos, el Comité pidió que se creara una agencia de reincorporación especial para ex miembros de la Fuerza Pública acogidos a la JEP.

### La apuesta por la reincorporación de los militares 

El mayor César Maldonado, presidente del Comité, explicó que más allá de la firma de un acuerdo de paz, lo más importante para quienes son excombatientes es que puedan construir un proyecto de vida distinto al de las armas. En ese sentido, resaltó que quienes integraron las FARC tengan proyectos productivos como el de la cerveza La Roja, lo que significa que puedan tener opciones distintas a las armas, aunque lamentó que **el Gobierno no haya entendido la importancia de este proceso "a fondo".**

De igual forma, Maldonado señaló que por parte de los miembros de la Fuerza Pública no están siendo incluídos en los planes de Gobierno para asegurar una reincorporación económica.  La preocupación se presenta porque casi 3 mil integrantes de las FF.MM. están obteniendo su libertad gracias a la Jurisdicción Especial para la Paz (JEP), pero no han podido ubicarse laboralmente debido a sus antecedentes penales. (Le puede interesar:["Ante la JEP buscan reconocimiento de los desaparecidos en HidroItuango"](https://archivo.contagioradio.com/audiencia-ante-la-jep-por-desaparecidos-hidroituango/))

### Seguir los cantos de sirena, o vivir de la mendicidad 

Maldonado afirmó que algunos de los militares que están pasando por esta situación no alcanzaron una asignación de retiro por su tiempo de servicio, por lo que **"no tienen otra alternativa distinta a la mendicidad, o escuchar los cantos de sirena que les hacen desde las organizaciones criminales"**. Según explicó el exmilitar, así como lo han denunciado algunos excombatientes de FARC, que desde diferentes grupos los tientan para volver a las armas, a miembros de su organización también les han pedido que se sumen a los Grupos Armados Organizados (GAO) que hacen presencia en Valle del Cauca o la costa atlántica.

Por esa razón, Maldonado sostuvo que era importante crear una agencia para la reincorporación de los militares que los apoyara en la creación de proyectos productivos, mediante asistencia técnica, y prestara financiación para los emprendimientos, así como que se crearan convenios para abrir las puertas a ex militares en las empresas de tal forma que tengan opciones de vida distinta a la guerra. (Le puede interesar: ["La Roja, una cerveza artesanal fruto de la reincorporación](https://archivo.contagioradio.com/roja-cerveza-artesanal-reincorporacion/)")

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe><iframe id="audio_43539566" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_43539566_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
