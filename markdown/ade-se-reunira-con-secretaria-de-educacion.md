Title: ADE denuncia represalias contra docentes que participaron en paro
Date: 2017-08-16 15:14
Category: Educación, Nacional
Tags: ade, fecode
Slug: ade-se-reunira-con-secretaria-de-educacion
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/06/84841b8f-f84e-4697-8226-6c3ad2604df0.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: FECODE] 

###### [16 Ago 2017] 

La Asociación Distrital de Educadores (ADE) logró llegar a un acuerdo con la Secretaría de Educación, luego de tomarse estas instalaciones durante más de 7 horas, para exigir una reunión con María Victoria Angulo, secretaría de Educación, y poderle exponer las graves denuncias **sobre represalias hacia los docentes provisionales, luego del paro, entre otras situaciones.**

De acuerdo con William Agudelo, presidente de la ADE, los docentes han denunciado despidos injustificados, **descuentos por participar en el paro y una constante persecución a los maestros que hicieron parte de la movilización de este año**. (Le puede interesar:["La Asociación Distrital de Educadores se toma la Secretaría de Educación"](https://archivo.contagioradio.com/la-asociacion-distrital-de-educadores-se-toma-la-secretaria-de-educacion/))

De igual forma la ADE, expresó que otras de las denuncias que han intentado manifestarle a la secretaria de educación, son las dificultades con las que **se esta implementando la jornada única sin que estén las condiciones como infraestructura y planta docentes**, además de la falta de refrigerios tanto para estudiantes como para docentes.

Hoy, a partir de las dos de la tarde, se reunirán la Personería Distrital, una delegación de la ADE, de FECODE y de la Secretaria de Educación para establecer soluciones a las diferentes problemáticas que se están presentando en el sector de la educación de la capital. (Le puede interesar:["Docentes podrían irse a Paro si no se resuelve calendario de ingreso a clases"](https://archivo.contagioradio.com/docentes-estan-preocupados-por-el-calendario-de-recuperacion-de-clases/))

<iframe id="audio_20373508" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_20373508_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
