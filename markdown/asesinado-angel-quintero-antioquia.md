Title: Después de ser raptado, hallan sin vida al líder social Ángel Quintero en Antioquia
Date: 2020-03-19 20:41
Author: CtgAdm
Category: Actualidad, Líderes sociales
Tags: Ángel Ovidio Quintero, asesinato de líderes sociales
Slug: asesinado-angel-quintero-antioquia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/03/Angel-Ovidio-Quintero.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:heading {"align":"right","level":6,"textColor":"cyan-bluish-gray"} -->

###### Foto: Antioquia Ángel Ovidio Quintero / @Actualidad\_O  
{#foto-antioquia-ángel-ovidio-quintero-actualidad_o .has-cyan-bluish-gray-color .has-text-color .has-text-align-right}

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Este jueves 19 de marzo organismos encontraron el cuerpo de [Ángel Ovidio Quintero González,](https://twitter.com/Ccajar/status/1240809392590794755) presidente del Concejo de San Francisco, oriente de Antioquia, quien fue víctima de un atentado en horas de la mañana por hombres armados. El concejal además lideraba una asociación de mineros del Municipio, y fue hallado por cuerpos de rescate en el río Santo Domingo.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Según información preliminar, Ángel Ovidio habría sido víctima de atentado esta mañana en el sector El Grillo, entre las veredas La Pailania y La Tolda, cerca de los límites con Cocorná, donde hombres con armas largas dispararon contra él y su sobrino. Tras informarse del atentado, las autoridades procedieron a buscarlo, y lo encontraron en horas de la tarde.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Según el periodista Óscar Castaño Valencia, "**el Oriente Antioqueño es una bomba de tiempo y si no se le pone urgente atención volverá a ser noticia nacional, por la guerra, en poco tiempo"**. A su afirmación se suman las denuncias de los campesinos de la región quienes hablan nuevamente de la presencia del ELN, un resurgimiento del antiguo Frente 9 de las FARC y de continua presencia y accionar paramilitar que unidosintensifican la violencia en la región.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Al accionar de grupos armados en Antioquia se suman las agresiones del Ejército

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

En otros hechos ocurridos al norte del departamento, la Asociación Campesina del Norte de Antioquia denunció que el 18 de enero, mientras tropas del Batallón Girardot adelantaban operativos en los municipios de Campamento, Angostura, Guadalupe y Anorí, se conoció que en la vereda La Milagrosa, de Angostura, la Fuerza Pública irrumpió de forma violenta contra la vivienda de un habitante de la tercera edad, quien estaba acompañado de dos menores de edad.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Al entrar a la vivienda, los dos menores fueron golpeados mientras se les preguntaba por mandos de grupos ilegales que hacen presencia en la zona, más tarde a las 7:00 pm el Ejército regresa a la vereda, requisa y amenaza a otras familias conformadas por "adultos mayores, niños, niñas y personas en condición de discapacidad", evidenciando la violencia estatal que viven a su vez las comunidades campesinas del Norte de Antioquia. [(Lea también: Campesinos exigen que Ejército diga la verdad del asesinato de Didian Agudelo)](https://archivo.contagioradio.com/campesinos-exigen-que-ejercito-diga-la-verdad-del-asesinato-de-didian-agudelo/)

<!-- /wp:paragraph -->
