Title: Prácticas paramilitares del pasado resurgen en Sucre
Date: 2019-07-03 15:25
Author: CtgAdm
Category: DDHH, Nacional
Tags: AGC, montes de maría, Paramilitarismo, Sucre
Slug: practicas-paramilitares-del-pasado-resurgen-en-sucre
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/02/AGC-Y-ELN-1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Comisión Intereclesial de Justicia y Paz] 

Desde el  Movimiento de Víctimas de Crímenes de Estado MOVICE  se ha denunciado el resurgimiento del accionar paramilitar en Sucre, departamento donde pese a que algunos políticos vinculados al crimen fueron detenidos o inhabilitados, las estructuras armadas siguieron en el territorio, en particular en el municipio de San Onofre y en los **Montes de María en Bolívar** donde hay presencia de las **Autodefensas Gaitanistas de Colombia** **(AGC**), se desplazan por el sector para ejercer control.

Para  el investigador del **MOVICE, Rodrigo Ramírez Salazar**, se trata de un resurgimiento de actividad paramilitar,  pues además de depender del narcotráfico, sostienen relacionamiento con las autoridades locales, participan políticamente, mantienen el control territorial, imponen vacunas y estigmatizan a sectores como la población LGBTI, lo que los diferencia de la delincuencia común. [(Le puede interesar: Paramilitares están retomando el control del Bajo Atrato)](https://archivo.contagioradio.com/paramilitares-estan-retomando-el-control-del-bajo-atrato/)

"Ellos están volviendo a prácticas del pasado, el control social a la luz pública. Cuando el paramilitarismo se desmovilizó ellos comenzaron a hacer control de manera clandestina, eran invisibles para la gente pero todo el mundo sabía quién estaba operando; ahora se presentan con fusiles" expresa con preocupación Ramírez quien relata cómo los paramilitares se han presentado ante los presidentes de las Juntas de Acción Comunal,  identificándose como parte de las AGC, y afirmando que han llegado para "acabar con la delincuencia".

### Preocupa la posición de las autoridades 

Rodrigo Ramírez advierte que lo más preocupante han sido las declaraciones de las autoridades quienes ante las denuncias de las comunidades argumentan que en la región "no está pasando nada", esto a propósito de la visita del ministro de Defensa, Guillermo Botero quien declaró que el único actor armado en la zona es el Clan del Golfo.  A su vez, el líder destacó el rol de la Defensoría del Pueblo, la que ha alertado desde 2018 sobre la presencia y control territorial de las AGC en el municipio de San Onofre.

A su vez, ven con extrañeza cómo en los Montes de María donde hay presencia militar de la infantería de marina, se vean merodear a los grupos armados irregulares, **"que no volvamos al pasado donde las autoridades por acción u omisión articularon labores irregulares e ilegales con estas estructuras armadas, es preocupante"**.  [(Lea también: Narcotráfico, política y paramilitares conviven en Tierralta: Comisión de Paz)](https://archivo.contagioradio.com/narcotrafico-politica-y-paramilitares-conviven-en-tierralta-comision-de-paz/)

### Intereses políticos y electorales de los paramilitares 

La comunidad también ha manifestado que existirían vínculos entre estructuras del narcotráfico y **la alcaldesa de San Onofre, Maida del Carmen Balseiro  quien es sobrina de Waldemaro Balseiro, preso  en Panamá por tráfico de cocaína**, "aquí siempre han tenido el respaldo y han sido financiados por la cocaína que sale del golfo de Morrosquillo en lanchas hacia el exterior".

Frente a las teorías que apuntan a que estos grupos paramilitares solo estarían interesados en el narcotráfico, el integrantel del MOVICE afirma que eso sería desconocer "las razones por las que quieren presionar a las comunidades", agregando que  "las AGC ya no solamente están narcotraficando, también están diciéndole a líderes en el territorio, que tienen que apoyar su proyecto político".

Para Ramírez, esto representa que en Sucre y en los Montes de María hay un panorama electoral bastante claro en el que los paramilitares, de cara a las elecciones regionales de octubre están logrando amedrentar a todos aquellos que piensan postularse a ediles, alcaldes o gobernadores y que no comparten la línea ideológica de los grupos armados.

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

<iframe id="audio_37976004" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_37976004_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
