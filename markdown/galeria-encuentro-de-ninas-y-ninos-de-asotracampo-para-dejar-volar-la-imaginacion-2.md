Title: Galería encuentro de niñas y niños de Asotracampo “Para dejar volar la imaginación”
Date: 2015-04-13 11:50
Author: CtgAdm
Category: Comunidad, Fotografia
Tags: Asociación de Desplazados y Campesinos del Tamarindo, Asotracampo, Atlántico, Conpaz, niñez, niños, Red de Comunidades Construyendo Paz en los Territorios, Tamarindo
Slug: galeria-encuentro-de-ninas-y-ninos-de-asotracampo-para-dejar-volar-la-imaginacion-2
Status: published

El pasado 12 de abril, niños y niñas de la comunidad del Tamarindo, en Atlántico, que hacen parte del Espacio Humanitario Mirador Refugio de Paz y Esperanza Asotracampo, se encontraron “**Para dejar volar la imaginación”, a partir del juego, la pintura y la música** generaron un espacio de construcción de memoria colectiva sobre sus vidas en Tamarindo.

Con cometas recordaron el momento cuando les fue arrebatada su tierra, pero con ellas mismas dejaron volar sus anhelos de **poder gozar de una vida que va más allá de los mínimos.**

El encuentro fue organizado por la Asociación de Desplazados y Campesinos del Tamarindo **(Asotracampo)** y la Red de Comunidades Construyendo Paz en los Territorios **(CONPAZ).**

 
