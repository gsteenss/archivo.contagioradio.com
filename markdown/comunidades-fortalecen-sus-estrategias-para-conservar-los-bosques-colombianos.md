Title: Comunidades fortalecen sus estrategias para conservar los bosques colombianos
Date: 2016-09-19 17:10
Category: Ambiente
Tags: Ambiente, Bosques, cambio climatico, conservación del ambiente, cordoba
Slug: comunidades-fortalecen-sus-estrategias-para-conservar-los-bosques-colombianos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/09/bosques-y-comunidad.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: arte y sociedad 

###### [18 Sep 2016] 

Del **16 al 19 de septiembre, se realiza en Lorica, Córdoba** un proceso de evaluación y fortalecimiento de los proyectos de comunidades que buscan  conservar y cuidar los bosques y territorios en los que viven, pero que se encuentran amenazados por el modelo extractivistas, los monocultivos, el calentamiento global y la ganadería, entre otros.

El evento denominado, ‘Encuentro Nacional de Gestión Comunitaria de Bosques y Territorios’, tiene como objetivo “fortalecer las capacidades de las comunidades en relación a sus iniciativas de gestión biocultural territorial”, teniendo en cuenta que los pueblos indígenas, comunidades negras y **comunidades locales son quienes pueden garantizar la implementación de estrategias de cuidado y restauración de ecosistemas.**

La iniciativa que se viene adelantando en 20 países, busca **ayudar diseñar un conjunto de herramienta para fortalecer** esos procesos que llevan a cabo las comunidades. Consiste en “conocer y entender las formas en las cuales las comunidades protegen y recuperan sus territorios, identificar los principales obstáculos y necesidades, desarrollar recomendaciones ‘desde abajo hacia arriba’ y finalmente realizar un proceso de incidencia, nacional e internacional, que genere apoyo, reconocimiento y condiciones adecuadas para que las comunidades puedan continuar y replicar ese tipo de prácticas”, como lo explica Miguel Lovera, integrante de la Coalición Mundial por los Bosques.

De acuerdo con Lovera se busca hacerle frente a los efectos devastadores que pueden generar diferentes actividades propias del modelo neoliberal, que traen **efectos devastadores sobre los modelos de las comunidades que son los más sustentables.** Es así como diversas organizaciones ambientalistas se adaptan a esas poblaciones para acompañar sus procesos y propiciar ese debate antes de la COP de diversidad biológica en Cancún.

Una de las principales conclusiones del evento, es que las amenazas por las que se encuentran en riesgo los bosques del mundo son comunes en todo los países, y por ello, es necesario que los procesos comunitarios del planeta se unan para detener esas afectaciones. Así mismo, debido a que el evento se desarrolla como un juicio, el último día del Encuentro se **leerá el veredicto final sobre la responsabilidad de los actores que impactan los territorios. **

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
