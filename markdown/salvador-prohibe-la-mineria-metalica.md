Title: El Salvador prohíbe la minería metálica
Date: 2017-03-29 18:32
Category: Ambiente, El mundo
Tags: El Salvador, Mineria
Slug: salvador-prohibe-la-mineria-metalica
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/03/17622064_1230986810354044_2174009015313372223_o.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Radio Güija 

###### [29 Mar 2017] 

El Salvador declaró la prohibición de la minería metálica en su territorio. Así lo aprobó hoy mediante una Ley del congreso de ese país, al considerar esa actividad como "una amenaza para el desarrollo y bienestar de las familias".

**La Ley que se venía discutiendo desde el 2005 contó con 69 votos de 84**, vetando tanto la explotación como la exploración minera en el suelo y bio-subsuelo del territorio salvadoreño, y además se especifica esa prohibición incluyendo el procesamiento a cielo abierto o subterráneo.

Con esta decisión se deja "sin efecto" todos los procesos que estén abiertos para la obtención de licencias mineras. Adicionalmente, se da un plazo de dos años a los mineros artesanales, de "pequeña escala" y "subsistencia familiar" para "reconvertir" sus actividades a otras con el apoyo estatal, y **desde ya les prohíbe la utilización de "químicos tóxicos como cianuro, mercurio y otros".**

También se blinda al país para que **"ninguna institución, norma o acto administrativo" pueda "autorizar" o dar licencias para la "exploración, explotación, extracción y procesamiento de minerales metálicos**". Y para asegurar todavía más al país frente a las consecuencias que generan este tipo de actividades, el Ministerio de Economía deberá tramitar el "cierre de minas metálicas" y deberá trabajar de la mano del Ministerio de Ambiente para una solución "a los daños causados por las minas" y de esa manera, "devolver a la población las condiciones de un ambiente sano".

Sin embargo, los diputados **excluyeron de la prohibición "el trabajo artesanal de fabricación, reparación o comercialización de joyas o productos de metales preciosos"**, que se contemplaban en el proyecto.

El diputado Guillermo Mata aprovechó el escenario para agradecer a aquellos que han defendido sus territorios y las comunidades de proyectos mineros. "Nuestros agradecimientos póstumos a las personas que entregaron su vida por la lucha contra la explotación minera, (...) defendieron los recursos naturales y tuvieron el valor de denunciar que no beneficiaba a nuestro país".

###### Reciba toda la información de Contagio Radio en [[su correo]

######  
