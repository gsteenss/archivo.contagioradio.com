Title: Consumo responsable en Navidad
Date: 2018-12-23 11:00
Author: AdminContagio
Category: Voces de la Tierra
Tags: Ambiente, consumo, navidad
Slug: consumo-responsable-navidad
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/12/consumo-responsable-navidad.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Reciclajes A. Marquéz 

###### 13  Dic 2018 

**Desde el mes de agosto de 2017, la humanidad vive a crédito por el uso excesivo de los recursos que anualmente produce la tierra para el consumo humano**. Superar la capacidad regeneradora del planeta, pone en peligro la existencia de las especies que lo habitan, estableciendo un delgado margen que hoy está al limite.

En este Voces de la tierra, nos acompañaron** Adriana Sánchez**, influenciadora en estilo de vida de bienestar holístico a través de su perfil El Blog de Adri; y **Camilo Zarate**, Chef profesional especializado en comida integral basada en plantas sin procesar, aportando desde su conocimiento algunas claves para consumir responsablemente durante las festividades de fin de año.

<iframe id="audio_30939816" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_30939816_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Encuentre más información ambiental en[ Voces de la tierra ](https://archivo.contagioradio.com/programas/onda-animalista/)y escúchelo los jueves a partir de las 11 a.m. en [Contagio Radio](https://archivo.contagioradio.com/alaire.html) 
