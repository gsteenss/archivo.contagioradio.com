Title: Modelo extractivista, un riesgo para los 737.000 cuerpos de agua en Colombia
Date: 2016-03-22 16:09
Category: Ambiente, Nacional
Tags: Día mundial del agua
Slug: modelo-extractivista-un-riesgo-para-los-737-000-cuerpos-de-agua-en-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/03/agua-e1458679659269.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Agencia Noticias UNAL 

<iframe src="http://co.ivoox.com/es/player_ek_10899644_2_1.html?data=kpWlm56aeJWhhpywj5WYaZS1lZ6ah5yncZOhhpywj5WRaZi3jpWah5ynca7jxcrZ0ZDJvNXmwsjhy9vNt9XVhpewjdrSb9PdxtjU0ZDUpdPVjNHc1ZCbd5iikZWdjcjZqdPk0JKSmaiRh9Di1cbUy9SPlsLYytSYj4qbh46o&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Hector Buitrago] 

###### [22 Mar 2016] 

**Colombia posee el mayor volumen de agua después de la Unión Soviética, Canadá y Brasil, con al menos 737.000 cuerpos de agua** entre ríos, quebradas, caños y lagunas, pero muchas de estas riquezas hídricas se encuentran en peligro, no solo por el fenómeno de El Niño, sino porque la economía colombiana se basa en un modelo extractivista que genera el desgaste de estas fuentes hídricas por las actividades de las empresas petroleras, mineras y de monocultivos.

Es por esa situación que hace siete años nace **‘Canto al Agua’ con el objetivo de que la ciudadanía reflexione sobre la forma como se relaciona con el agua,** como lo asegura Héctor Buitrago, músico de Aterciopelados y vocero de la iniciativa, quien en el Día Mundial del Agua invita a “la comunidad a que proteja el tejido hídrico y sean veedores del buen uso del agua por partes de las industrias, **exigiendo al gobierno que haya un manejo adecuado de este recurso vital por parte de las empresas así como se exige a los ciudadanos”.**

En más de 12 países se realizan distintas actividades para conmemorar esta fecha, en la que se recuerda que, por ejemplo en La Guajira, mientras los niños mueren de sed se han desviado cerca de 10 arroyos por la mina de carbón a cielo abierto más grande del mundo propiedad de la empresa El Cerrejón; así mismo, empresas como Poligrow en Mapiripán gastan 1’700.000 litros de agua diarios por cada plantación de palma aceitera; en Putumayo, la petrolera Amerisur realiza explotación de crudo en medio de un humedal que representa el 50% de la zona de reserva campesina Perla Amazónica,  afectando el recurso hídrico de la comunidad, pues esta empresa bota agua industrial a los caños de los que se abastece la población.

En Bogotá, las actividades se realizarán en lugares como la Reserva Thomas Van Der Hammen, Reserva el Delirio, el Jardín Botánico José Celestino Mutis y el Eje Ambiental donde se llevará a cabo estas actividades de Canto al Agua, con el fin de que la comunidad haga un reconocimiento de las fuentes hídricas que hay en la ciudad impulsado propuestas frente al manejo y consumo del agua.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
