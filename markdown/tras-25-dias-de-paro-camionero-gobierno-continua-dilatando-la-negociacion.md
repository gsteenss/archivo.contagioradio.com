Title: Tras 25 días de paro camionero Gobierno continúa dilatando la negociación
Date: 2016-06-29 13:36
Category: Movilización, Nacional
Tags: Asociación Colombiana de Camioneros, Ministerio de Transporte, Paro camionero
Slug: tras-25-dias-de-paro-camionero-gobierno-continua-dilatando-la-negociacion
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/06/Paro-Camionero.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Victor Correa ] 

###### [29 Junio 2016 ]

Desde el pasado 5 de junio el sector camionero se declaró en paro y asegura continuar en él hasta que el Gobierno nacional solucione estructuralmente la crisis que enfrenta, en relación con los **\$2 billones que fueron asignados para la modernización del parque automotor pero que no llegaron a sus manos** y los altos costos de peajes y combustibles, principalmente.

Pedro Aguilar, actual presidente de la 'Asociación Colombiana de Camioneros' asegura que el **gobierno está dilatando la discusión para que el sector "se aburra y se vaya a trabajar"**. Aguilar denuncia que en la mesa de negociación establecida, el Ministro de Transporte, Jorge Rojas, "entrega un documento hoy, mañana entrega otro, luego vuelve y empieza", lo que para él "se llama la tomadera de pelo", que intenta ocultar la protesta del sector.

<iframe src="http://co.ivoox.com/es/player_ej_12068567_2_1.html?data=kpedmJ2Zepihhpywj5WZaZS1lJmah5yncZOhhpywj5WRaZi3jpWah5yncbHZxdfcjabLucrgwteYj5Clt9DXysbQy4qnd4a2lNOYpdTQs87Wysbbw5DIqYy3wtLW0dPJttDnjoqkpZKns8_owszW0ZC2pcXd0JCah5yncZU.&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)] ] 
