Title: Fue asesinado excombatiente Manuel Santo Yatacué en Buenos Aires, Cauca
Date: 2019-12-03 15:53
Author: CtgAdm
Category: DDHH, Nacional
Tags: asesinato de excombatientes, Cauca
Slug: fue-asesinado-excombatiente-manuel-santo-yatacue-en-buenos-aires-cauca
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/Manuel-Santo-Yatacue.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:  @Betocoralg  
] 

A las 6:00 pm del pasado 2 de diciembre cerca de las 6:00 pm fue asesinado el excombatiente Manuel Santo Yatacué Ramos, en la vereda La Elvira municipio de Buenos Aires, Cauca. Manuel se había acogido al Acuerdo de Paz firmado con las FARC en 2016 y actualmente  hacia parte de la cooperativa conformada para producir y transformar productos derivados del café.

Según información de medios locales, Manuel fue atacado por hombres con armas de fuego que dispararon en varias ocasiones en el corregimiento de El Ceral, **en cercanías al  Espacio Territorial de Capacitación y Reincorporación (ETCR) de La Elvira.** por ahora no se conocen quienes podrían ser los responsables ni los móviles del asesinato del excombatiente. [(Lea también: ¿Existen garantías de seguridad para excombatientes que habitan los ETCR?) ](https://archivo.contagioradio.com/existen-garantias-de-seguridad-para-excombatientes-que-habitan-los-etcr/)}

Tulio Murillo, consejero político departamental del ETCR de Mesetas, se refirió recientemente al asesinato de excombatientes, en particular a los hechos en los que falleció el también excombatiente Alexander Parra, señalando que aunque la seguridad desplegada en cada uno de los ETCR, ha permitido generar cierto grado de confianza, en las zonas más militarizadas como Norte de Santander, Cauca y Meta es donde se presenta una mayor violencia contra los exintegrantes de las FARC-EP.  [(Lea también: Alexander Parra, representante ambiental y excombatiente es asesinado en ETCR) ](https://archivo.contagioradio.com/alexander-parra-representante-ambiental-y-excombatiente-es-asesinado-en-etcr/)

Al respecto, líderes del partido Farc como Pablo Catatumbo reiteraron la necesidad de que el Gobierno garantice la vida y la seguridad de los firmantes de La Paz, mientras la senadora Victoria Sandino manifestó que el Gobierno continúa sin prestar atención a **la necesidad de implementar las garantías de seguridad pactadas en el Acuerdo de Paz.**  [(Le puede interesar: En Colombia han sido asesinados 170 excombatientes tras la firma del Acuerdo de Paz)](https://archivo.contagioradio.com/en-colombia-han-sido-asesinados-170-excombatientes-tras-la-firma-del-acuerdo-de-paz/)

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
