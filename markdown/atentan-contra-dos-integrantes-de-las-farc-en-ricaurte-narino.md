Title: Atentan contra dos integrantes de las FARC en Ricaurte, Nariño
Date: 2017-10-31 16:27
Category: DDHH, Nacional
Tags: acuerdos de paz, FARC
Slug: atentan-contra-dos-integrantes-de-las-farc-en-ricaurte-narino
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/05/Asesinado-integrantes-de-las-farc.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [31 Oct 2017] 

De acuerdo con información de la Red de Derechos Humanos Francisco Isaías Cifuentes en las últimas horas, Orlando García fue asesinado, mientras que Juan Pablo Guanga fue herido en la vereda San Isidro, en el municipio de Ricaurte, Nariño ambos hacían parte del proceso de reincorporación de las FARC y del cabildo indígena Awá Ramón Mongón.

Los hechos se presentaron sobre las 11:37 am cuando finalizaba una reunión sobre cooperativismo, paz, organización, reincorporación, entre otros temas del partido político FARC. Las personas que se encontraban en el lugar manifestaron que escucharon varios disparos sobre la parte alta del lugar en donde se encontraban. (Le puede interesar:["ELN y FARC le están apostando a la paz"](https://archivo.contagioradio.com/eln-y-farc-quito/))

A 150 metros de este lugar se encuentra el cuerpo de Orlando García y a Juan Pablo Guanga herido, quienes previamente habían salido de la reunión. **Según el testimonio de las personas, ambos integrantes de las FARC**, fueron atacados por hombres armados.

Este acto se suma a las denuncias que viene realizando el partido político, frente a la falta de garantías por parte del gobierno para que se respete y no se atente contra la vida de los integrantes de las FARC en el proceso de reincorporación. (Le puede interesar:["Dura carta de las FARC al gobierno por asesinato de integrantes de ese partido político"](https://archivo.contagioradio.com/farc_carta_presidente_juan_manuel_santos/))

Noticia en desarrollo…
