Title: Atentan contra Carlos Tobar, integrante del Comité del Paro Cívico por Buenaventura
Date: 2019-07-26 16:45
Author: CtgAdm
Category: DDHH, Nacional
Tags: Atentado, buenaventura, Carlos Tobar, Derechos Humanos, ex combatientes
Slug: atentan-contra-carlos-tobar-integrante-del-comite-del-paro-civico-por-buenaventura
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/c3ca02515eed4fcf6667074342a4fc5d_XL-1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: tomada de CNMH] 

Procesos organizativos que integran el Comité del Paro Cívico de Buenaventura denunciaron el atentado que sufrió Carlos Tobar, también miembro del Comité, en su casa. Según las primeras informaciones, Tobar fue atacado por hombres que entraron a su vivienda y le dispararon, causándole heridas; Tobar fue llevado al centro asistencial Santa Sofía, donde permanece con pronóstico reservado. (Le puede interesar: ["Buenaventura: ¿La ciudad que encarna el Estado fallido?"](https://archivo.contagioradio.com/buenaventura-la-ciudad-que-encarna-el-estado-fallido/))

Carlos Tobar es integrante de la Mesa de Acceso a la Justicia del Comité, y había solicitado medidas de protección a la Unidad Nacional de Protección (UNP), que aún no han sido respondidas. Adicionalmente, es integrante del Consejo Comunitario del Bajo Calima, asimismo, es un líder activo de la construcción del Centro de Memoria Histórica del Litoral Pacífico (Le puede interesar: ["Asciende a 10 la cifra de líderes del Paro Cívico de Buenaventura amenazados"](https://archivo.contagioradio.com/asciende-a-10-la-cifra-de-lideres-del-paro-civico-de-buenaventura-amenazados/))

### **No es la primera vez que Carlos Tobar ve amenazada su vida**

En un pronunciamiento, la Mesa Distrital de Participación Efectiva de Víctimas en Buenaventura recordó que Tobar participó en la Mesa de Fortalecimiento de la población desplazada que se instaló en 2005; en 2013 fue integrante del Comité ejecutivo de la Mesa de víctimas y en 2015, "por razones de amenazas decidió pretirarse, pero volvió a ingresar en 2017 donde ha participado activamente de la construcción de la política pública de atención a las víctimas del conflicto armado".

La Mesa resaltó que Carlos, "como muchos de los líderes y lideresas de la mesa de víctimas han sido objetos de amenazas que fueron denunciadas ante la Fiscalía"; pero mostraron su preocupación ante la falta de "resultados en las investigaciones y por el contrario, la seguridad de los miembros de la mesa cada vez es más precaria por la inserguridad que viven muchos barrios de la ciudad (Buenaventura)". (Le puede interesar: ["Amenazan a dos líderes sociales del paro Cívico de Buenaventura"](https://archivo.contagioradio.com/amenazan-a-dos-lideres-sociales-del-paro-civico-de-buenaventura/))

### **"Hace parte de una estrategia para acallar voces"**

Según Charo Mina, integrante del Proceso de Comunidades Negras (PCN), y que también participó en el Comité del paro cívico, el atentado contra el líder "hace parte de esa estrategia de acallar las voces, disolver los procesos organizativos que buscan con su trabajo la reivindicación de derechos; en el caso de Buenaventura, pasa por el derecho a la tierra, al agua, a educación, acceso a un servicio de salud digno y a la vida". (Le puede interesar: ["Si el Gobierno no cumple, retomaremos el paro cívico: habitantes de Buenaventura"](https://archivo.contagioradio.com/si-el-gobierno-no-cumple-retomaremos-el-paro-civico-habitantes-de-buenaventura/)).

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
