Title: La macabra alianza entre paramilitares y empresas Bananeras
Date: 2017-02-08 15:19
Category: DDHH
Tags: Empresas Bananeras, paramilitares
Slug: la-macabra-alianza-entre-los-paramilitares-y-las-empresas-bananeras
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/02/palma-y-paramilitarismo.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [8 Feb 2017] 

La Sala de Justicia y Paz  del Tribunal Superior de Bogotá, dio a conocer una lista con 57 empresas que habrían financiado estructuras paramilitares, 7 de ellas son bananeras y han sido mencionadas en diferentes testimonios y confesiones de desmovilizados paramilitares que se acogieron a la Ley 975 de Justicia y Paz.

Raúl Hasbún, quien fuera el comandante de  las AUC en la región del Urabá, declaró en versiones libres, sobre reuniones sostenidas **entre delegados de empresas bananeras y representantes de grupos paramilitares, en las que se acordó el mecanismo de financiación** para asegurar la protección de las empresas en las plantaciones del Urabá. Le puede interesar: [Empresas Bananeras responsables de crimes de Lesa Humanidad](https://archivo.contagioradio.com/empresas-bananeras-responsables-de-crimenes-de-lesa-humanidad/)

Según Hasbún, estas reuniones se hacían cada mes y se realizaban los pagos, primero en efectivo y luego a través de las cuentas bancarias de las Convivir. **Dinero que financió crímenes en contra de quienes rechazaban el agronegocio**. Algunas de las empresas mencionadas son:

### **Banadex** 

(Filial de Chiquita Brands en Colombia) En el año 2001, **un cargamento con 3.400 fusiles AK47, acompañados de  5 millones de municiones, desembarcó en el puerto privado de Banadex**, adquirida posteriormente por la empresa Banacol. De acuerdo con la Comisión Interclesial de Justicia y Paz, pese a que la multinacional Chiquita Brands afirmó que desde el 2004 ya no tiene presencia en Colombia, debido a las serias acusaciones que la relacionan con paramilitarismo, esta continúa comprando banano en el país”

De igual forma, Víctor Manuel Henríquez Velásquez, presidente y Juan Diego Trujillo, vicepresidente de Banacol, **están siendo investigados por  concierto para delinquir agravado, financiación, organización y promoción de grupos armados ilegales**. Le puede interesar: [La multinacional Chiquita Brands podría asumir juicio en Estados Unidos.](https://archivo.contagioradio.com/la-multinacional-chiquita-brands-podria-afrontar-juicio-en-estados-unidos/)

Durante las imputaciones de mayo de 2016, se estableció que probablemente “hubo un acuerdo de voluntades” entre empresarios bananeros  y jefes paramilitares para **“promover, organizar, financiar, armar o promocionar grupos al margen de la ley"** por tal motivo, en caso de que sean condenados por concierto para delinquir, se aumentaría la pena.

### **Grupo Unibán** 

Está conformada por diferentes grupos en los que se encuentra Probán y la Agricola Santamaría. **Probán es una de las empresas nombradas por paramilitares desmovilizados como Salvatore Mancuso y Hasbún**, quienes afirmaron que les financiaron en el Urabá. Algunas compañías de este grupo tienen filiales en Europa, en países como Bélgica en donde se encuentra bajo la denominación de Tropical Marketing Association. TMA.

A su vez entre los principales accionistas de Unibán está la **familia Gaviria** Correa, relacionada en una sentencia de tierras proferida por el Tribunal de Cartagena que determinó que la Agropecuaria Cármen de Bolívar **no probó “la buena fe” en las compras de tierra en la Región de Montes de María.** Le puede interesar: ["20 empresas deberán devolver 53.821 hectáreas de tierras despojadas"](https://archivo.contagioradio.com/20-empresas-deberan-devolver-53-821-hectareas-de-tierras-despojadas/)

### **Banafrut** 

Es una comercializadora internacional que cuenta con 35 fincas en el Urabá, con una extensión cercana a las 3.500 hectáreas. Nicolás Echavarría Mesa forma parte de su Junta Directiva, fue una de las **personas nombradas por financiar a los paramilitares en esta región** y de acuerdo con Hasbún, Echavarría habría hecho aportes por 8 fincas explotadas por agrícolas asociadas a la comercializadora Banafrut.

De otro lado, **Echavarría fue gerente de la campaña presidencial del partido político Centro Democrático** en el 2014 y ex embajador de Colombia en Bélgica, principal país al que llega el banano del Urabá.

**Tropical:** Entre los accionistas de esta comercializadora destacan Javier Francisco Girona y Yolanada Retrepo Girona, propietarios del 40% de la empresa. **Javier Francisco se encuentra vinculado a un proceso judicial por desplazamiento forzado**. A su vez, ambos son directivos de la sociedad Recife S.A.S , que según el informe de Justicia y Paz, siembra palma de aceite de manera ilegal en Curbaradó y Pedeguita y Mansilla.

De igual forma, esta **empresa fue declarada como ocupante ilegal depredios pertenecientes al territorio colectivo de las comunidades de Curvaradó y Jiguamiando**, por el Tribunal Contencioso Administrativo del Chocó en 2009.

**Conserva:** Es la filial en Colombia de la multinacional Fresh Del Monte. El ex jefe paramilitar mencionó en una locución a medios de información que **“todas las empresas en la región bananera pagaron. Por ejemplo Dole y Del Monte, que creo son compañías de Estados Unidos”.**

Además, Del Monte también tuvo convenios con la empresa bananera Multifruits Ltda. , de la que fue directivo Enrique Rendón, hermano del paramilitar Freddy Rendón conocido como alias “El Alemán”.

### **Coindex** 

Está conformada por diferentes accionistas que a su vez son sociedades, las principales son: Pacuare S.A., Agropecuaria El Tesoro y Agropecuaria Bananeras, esta última pertenece a la familia Argote relacionada en el informe “Colombia: Banacol, empresa implicada en el paramilitarismo y acaparamiento de tierras en el Curvaradó y Jiguamiando”, con el **acaparamiento irregular de 1.236 hectáreas de territorios colectivos de comunidades negras.**

De igual forma los Argote son nombrados como ocupantes ilegales de tierras en caracterización adelantada por el Incoder.

El informe de la Comisión Interclesial de Justicia y Paz, también señala que existen indicios, pruebas prácticas y judiciales cotejadas y otras que aún deben ser confrontadas, de que l**as relaciones entre empresarios y paramilitares fueron la causa principal de crímenes como desplazamiento forzado, el despojo y la legalización irregular de predios para concentrar tierras**, el uso y la financiación con  recursos públicos en la ilegalidad, el uso de paraísos fiscales para el lavado de activos, el narcotráfico, entre otras.

[Encuentre el informe aquí](http://jyp.megadatesystem.com/Informe-Empresas-bananeras-vulneracion-de-derechos-humanos)

###### Reciba toda la información de Contagio Radio en [[su correo]
