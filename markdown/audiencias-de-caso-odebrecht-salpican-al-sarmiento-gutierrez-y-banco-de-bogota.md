Title: Escándalo Odebrecht salpica a familia Sarmiento y al Banco de Bogotá
Date: 2019-01-23 15:08
Author: AdminContagio
Category: Entrevistas, Política
Tags: Banco de Bogotá, Fiscal Néstor Humberto Martínez, Grupo Aval, Luis Carlos Sarmiento Gutiérrez, Odebrecht
Slug: audiencias-de-caso-odebrecht-salpican-al-sarmiento-gutierrez-y-banco-de-bogota
Status: published

###### [Foto: Contagio Radio] 

###### [22 Ene 2019]

Tras los dos primeros días de audiencias en el caso de Odebrecht, ha salido a la luz el nombre de Luis Carlos Sarmiento Jr y el Banco de Bogotá, en donde se afirma que no solo tenían conocimiento de los sobornos en los contratos de la Ruta del Sol II, sino que además ayudaron a la configuración de los actos de corrupción para el pago de dineros.

### **Los lazos de la corrupción entre Odebrecht y el grupo AVAL** 

La audiencia realizada este 22 de enero en donde se presentó José Elias Melo, quien era el presidente de Corficolombiana, compañía que hace parte del Grupo AVAL, que a su vez integraba el consorcio para la construcción de la Ruta del Sol II, tenía como finalidad hablar del primer soborno **por 6.5 millones de dólares que se habrían dado para entregar el primer contrato de esa obra.**

Previamente, el 21 de enero, tres directivos de Odebrecht, entre los que se encuentra Luis Antonio Bueno Jr, presidente de la multinacional para Colombia, rindieron indagatoria. Bueno relató que cuando llegó a Colombia, después de averiguaciones, empezó a tener acercamientos con el grupo Aval, y a través de Andrés Uriel Gallego, ministro de transporte para el 2009 y su viceministro Gabriel García Morales, empezaron a sostener múltiples reuniones en el norte de Bogota, en la propiedad de Juan Manuel Barraza, funcionario de Álvaro Uribe Vélez, **en donde se habría configurado ese primer soborno. **

Posteriormente, Bueno afirmó que Odebrecht desembolsó el efectivo y le dijo a Melo que se cruzarían cuentas de los dineros a partir de los contratos de las vías, a lo que el presidente de Corficolombiana habría respondido que él tendría que escalar toda la operación para programar los pagos con Luis Carlos Sarmiento Gutiérrez.

### **Luis Carlos Sarmiento Jr y el Banco de Bogotá** 

Catherine Juvinao, integrante del colectivo \#RenuncieFiscal señaló que si bien en la audiencia, Bueno manifestó que no tenía conocimiento sobre si finalmente Melo habría conversado con Sarmiento sobre esos temas, "**la sola declaración da, para que por lo menos, se vincule a Sarmiento Jr al proceso". **

Sumado a ello, Juvinao manifestó que se tiene información sobre la programación de la salida de algunos de los pagos, de esos 6.5 millones de dólares, que sí se realizaron a través del Banco de Bogotá. Razón por la cual cuestionó que Néstor Humberto Martínez, actual fiscal general de la nación, **que previamente había asesorado a Luis Carlos Sarmiento Angulo, este a la cabeza de este caso de corrupción. **

"En la medida en que ya se está empezando a salpicar de frente al grupo AVAL, a Sarmiento Jr y al Banco de Bogotá, es absolutamente más evidente que el Fiscal esta totalmente impedido para estar a la cabeza de la Fiscalía y para estar al frente del caso Odebrecht" aseguró Juvinao.  (Le puede interesar:["Las razones para pedir la nulidad en la elección del Fiscal Néstor Humberto Martínez")](https://archivo.contagioradio.com/piden-nulidad-en-eleccion-del-fiscal-nestor-martinez-por-ocultar-informacion/)

Asimismo, la integrante del colectivo \#RenuncieFiscal, aseveró que el siguiente caso de soborno que tendrá que revisarse es el de las mejoras en las condiciones de los contratos para darle estabilidad jurídica a Odebrecth, en donde entraran las acusaciones a Néstor Humberto Martínez, porque él sería quien abría asesorado esas modificaciones.

### **La corrupción de Odebrecth no puede quedar de lado** 

Juvinao aseguró que el colectivo \#RenuncieFiscal tiene la misión de impedir "que este tema salga de la agenda" y mantener a la ciudadanía informada frente a las declaraciones que se realicen en las audiencias. De igual forma señaló que el próximo 29 de enero se realizarán plantones frente a las sedes de la Fiscalía para exigir la renuncia de Néstor Humberto Martínez y la iluminación de la justicia en Colombia.

"Lo que exigimos los ciudadanos es que renuncie y no utilice la Fiscalía y las herramientas de la Fiscalía para su defensa personal, eso no tiene presentación en ninguna democracia seria, ni en ningún país, y no se puede normalizar en Colombia" manifestó Juvinao.

######  Reciba toda la información de Contagio Radio en [[su correo]
