Title: Una guerra ideológica y cultural
Date: 2020-02-14 12:10
Author: JOHAN MENDOZA TORRES
Category: Johan Mendoza, Opinion
Tags: guerra, ideologias, neutralidad, Opinión, politica, Protesta social
Slug: una-guerra-ideologica-y-cultural
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/02/Entrada-82.mp3" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/02/revolucion12.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/02/ideologia-2.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/02/phpThumb_generated_thumbnail.jpeg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

Así intenten por todos los medios reprimir nuestra aspiración, para millones de personas continúa viva:

<!-- /wp:paragraph -->

<!-- wp:heading {"level":4} -->

#### **Colombia necesita un cambio de régimen político.**

<!-- /wp:heading -->

<!-- wp:paragraph -->

La pregunta es **¿cómo?** la cosa no está fácil pues nos encontramos perdiendo una batalla ideológica que, debido al peso hegemónico del **totalitarismo de la neutralidad**, pasa desapercibida por muchas personas.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El lenguaje y la forma en que vemos lo político nos coloca como individuos ante expresiones creadas con el objetivo de difamar y pervertir aquellas alternativas que podrían subvertir la realidad colombiana. 

<!-- /wp:paragraph -->

<!-- wp:heading {"level":4} -->

#### No nos digamos más mentiras.

<!-- /wp:heading -->

<!-- wp:paragraph -->

La crítica dócil que desarrolla alguna izquierda siempre será utilizada por los poderosos para aniquilar un pensamiento originalmente con capacidad de subvertir una situación. Esa capacidad es peligrosa para un poder establecido, y por eso el poder establecido celebra la protesta; pero cuando el semáforo este en rojo, como si fuéramos payasos y no un pueblo pidiendo dignidad y justicia.  

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Tal parece que el arma más desarrollada por el poder hegemónico trasnacional es el famoso “centro”. El centro nació en la guerra fría, cuando serios intelectuales financiados por la **CIA** compararon al comunismo con el nazismo para favorecer a los bondadosos bombarderos de los Estados Unidos. El centro no es un cuento tan nuevo. Su forma de operar es la **paradoja funcional. **

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Esta consiste en mantener viva la necesidad de desfigurar una oposición con capacidad de subvertir una situación a la vez que permite una oposición débil, que pide cosas pequeñas, que ofrece debates débiles, que genera adeptos masivos pero efímeros que no afectan el funcionamiento del sistema. 

<!-- /wp:paragraph -->

<!-- wp:heading {"level":4} -->

#### La raíz de esto es la turbia idea de la neutralidad política.

<!-- /wp:heading -->

<!-- wp:paragraph -->

La neutralidad se ha vuelto de alguna manera “indiscutible”; su modus operandi es despolitizar, universalizar, equiparar opuestos; avanzando como **paradoja funcional**, impulsa de forma totalitaria la necesidad de que las personas abandonen el análisis de la totalidad y se queden solo con lo que les conviene: un pedacito de realidad. 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

No olvidemos que cuando no se analiza la totalidad, se siembra la raíz de la exclusión política. Y es la exclusión política la madre de toda violencia y toda corrupción.  

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Ojalá las personas que deseamos un cambio político, sepamos ya que no habrá renuncias voluntarias al poder por parte de quienes lo tienen, esto debe tenerse claro.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

No podemos ganar mucho con algunas elecciones porque el sistema actualmente funciona por encima y por debajo de la línea de la institucionalidad, no solo hay corrupción como síntoma que tiene una causa y no surge “porque sí”, sino que hay familias y amigos rotando en cargos públicos de un poder inimaginable, hay algunos que se visten de periodistas, montan un falso altar de imparcialidad y luego nos enteramos de que todos viajan en el avión presidencial.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Vea, aquí los intereses de los grupos de poder son más elevados que toda la institucionalidad, y ese es nuestro fracaso parcial como república.   

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

No nos digamos más mentiras, equiparar aquello que es diametralmente opuesto es un juego lingüístico para mantener el poder estático, si ni siquiera somos capaces de hablar sobre la posibilidad de transformarlo todo, ¿cómo podríamos crear las condiciones para transformarlo todo? por eso los ataques más fuertes del **totalitarismo de la neutralidad** viajan en el lenguaje político y cultural. 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Hoy, es común escuchar que “izquierda y derecha son iguales”. Eso es una necedad petulante porque si existe la derecha y existe la izquierda sencillamente es porque son diametralmente opuestos, si no se observa lo opuestos que son, entonces el análisis es superfluo, o bien solo se ha hecho una mirada moral o también porque, se sabe que, al equipararlos, se protege al poder totalizante y reinante bajo la forma de una lingüística de la neutralidad. 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Y esto no se trata de apología al binarismo, pues en el binarismo 0 y 1 son opuestos lógicos que no varían, no son dinámicos, en cambio izquierda y derecha varían, cambian, son circunstanciales en su presentación histórica, pero eternos elementos dialécticos. Por eso, como dirían los viejos, “son muy brutos o son muy inteligentes quienes niegan la irreconciliable diferencia entre izquierda y derecha”.  

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

De lo anterior, puedo atreverme a decir que la creación de anti-hegemonía es urgente. La izquierda debe definirse, debería dejar de creer el cuento liberal de que “ya no es tiempo de hablar de socialismo”, debería destruir esas ideas que son falsas y son presentadas por tendencias comunicativas o académicas como “universales”.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

La izquierda debería repensar si en verdad la solución exclusiva es distribuir la riqueza, y más bien, podría preparar y componer con todo el carácter, las bases para librar una guerra a muerte:

<!-- /wp:paragraph -->

<!-- wp:heading {"level":4} -->

#### la guerra ideológica y cultural que hoy está perdiendo. 

<!-- /wp:heading -->

<!-- wp:paragraph -->

Una de esas bases es aceptar resignadamente una realidad tan cierta como el aire que respiramos: en todo lo concerniente a la política **no se puede ni se podrá gobernar para todos**. Por eso, si la izquierda quiere construir anti-hegemonía, podría comenzar por un primer paso y es asesinar esa utopía. (léase bien “esa utopía” no “a la utopía”). 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El segundo paso si se pretende un cambio político de raíz, es decir radical, es urgente incluir de nuevo, resignificar, popularizar e intelectualizar el concepto **revolución.** 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El tercer paso es saber que los pasos 1 y 2 no podrán contradecirse. 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Y el cuarto paso es que, si se piensa una revolución en Colombia, ha de suponerse que será para destronar a un poder que está enquistado, incrustado, casi tatuado en la economía, en la política y en la cultura desde hace muchos años, por lo que de ningún modo un cambio en la situación será un evento tranquilizador que genere agrado entre quienes están siendo sacados del poder… ellos tal y como lo han demostrado, lucharán con todo lo que tienen para seguir como amos indiscutibles del orden político. 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

No es casualidad que el asesinato sistemático de líderes sociales denote que quienes tienen el poder en Colombia serán capaces de cualquier cosa con tal de mantenerse. Acusarán de resentidos y promotores del odio a aquellos que lloran a los muertos y piden justicia. 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Es que el peso de la ideología del sistema actual consiste en hacer tendencia y promover la idea de que los problemas no están vinculados entre sí; fomenta la individualización de problemas que son históricos, que tienen causas colectivas, ideológicas, de clase social. Esta tendencia es la fuerza y la fuente oculta de la lógica del actual sistema ante los hechos políticos.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

 Por eso el hecho político es tratado como “bueno o malo”, consiguientemente, cuando un ciudadano cree en el “mal necesario” lo hace porque ese mal le ha favorecido, de lo contrario no creería en esa premisa.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

La guerra ideológica la están ganando los dueños del poder, porque han descubierto que desde las ciudades pueden dominar a millones sin disparar una sola bala. Claro, en el campo las condiciones para esa dominación no se dan, por eso allí sí asesinan a tanta, pero tanta gente. 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

La guerra ideológica y cultural es una confrontación por el control del pensamiento colectivo; por parte de los gobiernos esa guerra no está cargo del ESMAD o del Ejército, sino de todas esas frases, apreciaciones, noticias, imágenes, formas de entretenimiento, que viajan en forma de costumbre, de lenguaje, de forma de ser y pensar así sea en soledad

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

¿o acaso lo que millones consumen solitariamente por medio de sus celulares una noche cualquiera tiene algo de diferente? **¡es el mismo potaje! **

<!-- /wp:paragraph -->

<!-- wp:heading {"level":4} -->

#### Hubiéramos podido acabar con 18 años de uribismo el 21 de noviembre, pero lastimosamente hoy, están ganando la batalla ideológica.

<!-- /wp:heading -->

<!-- wp:paragraph -->

Al estar ganando, su reafirmación no es a través del discurso doctrinario de sus tesis políticas, sino través de un discurso doctrinario de neutralidad, que contiene frases como: “no polarizar”, “no es hora de controvertir”, “superemos las diferencias” etc. etc. etc. 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Yo pregunto, no a ese sujeto que con su alternatividad se convierte en el siervo más libre, en el rebelde más obediente del sistema, en el amante del cuidado animal y planetario que le hace quite a las cabezas de los hombres decapitados en Colombia, sino a aquellos que se piensan los problemas del país como la carne misma:

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

¿Es posible desarrollar una contrainteligencia cultural y política para dotar de dudas a aquellos que dan por hecho que no tenemos cómo subvertir una situación? 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

¿Es posible derrumbar esa influencia falsificada, así como cuando comprendemos qué los fenómenos no son casualidad y que son parte de un proyecto ideológico? ¿acaso pensamos que dan premios a los youtubers por casualidad? 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

A esos intelectuales tan críticos en sus citas, tan profundos con sus trabajos de grado pero tan inútiles para los oprimidos de Colombia

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

¿No les basta rechazar el mundo binario mientras se niegan a vivir lejos de los Carulla sin percatarse que no es una casualidad? 

<!-- /wp:paragraph -->

<!-- wp:audio {"id":80723} -->

<figure class="wp-block-audio">
<audio controls src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/02/Entrada-82.mp3">
</audio>
</figure>
<!-- /wp:audio -->

<!-- wp:paragraph -->

Vea mas Columnas de[Johan Mendoza](https://archivo.contagioradio.com/johan-mendoza-torres/)

<!-- /wp:paragraph -->
