Title: Jericó, Antioquia le dice ¡No! a la minería por segunda ocasión
Date: 2018-11-22 17:38
Author: AdminContagio
Category: Ambiente, Nacional
Tags: Jericó Antioquía, Minería en Colombia
Slug: jerico-antioquia-le-dice-no-a-la-mineria-por-segunda-ocasion
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/05/33081661_10155900278132772_7763543078492700672_n.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Archivo 

###### 22 Nov 2018 

Con cinco votos a favor y cuatro en contra el Concejo el municipio de Jericó, Antioquia aprobó por segunda vez la ley que prohíbe la exploración minera a pequeña, mediana y grande escala en su territorio, la decisión inicial había sido anulado por el ** Tribunal Administrativo de Antioquia**  en diciembre de 2017.

La decisión se tomó el pasado 20 de noviembre luego de que el Consejo de Estado anuló lo establecido por el Tribunal Administrativo de Antioquia, de tal modo que el cabildo municipal interpretó que los entes territoriales sí pueden prohibir la minería en sus localidades y que “por tanto los municipios en protección del patrimonio ecológico actúan constitucionalmente” explicó **Fernando Jaramillo vocero de la Mesa Ambiental de Jericó.**

Cuando el alcalde del municipio, Jorge Andrés Pérez Hernández ponga en vigencia la ley, lo cual sucederá en los próximos días,  la compañía **AngloGold Ashanti, a cargo del proyecto minero Quebradona**, tendrá que interrumpir las actividades mineras que vienen realizando desde hace siete años a las afueras del casco urbano.

Según Jaramillo, Anglogold dice “haber descubierto un depósito de cobre, oro, plata y molibdeno a 500 metros de profundidad en las montañas limítrofes entre Jericó y Tarso” sin embargo para tener acceso a dichos metales, la compañía tendría que remover miles de toneladas de rocas que al ser desplazadas afectarían los nacimientos de agua que llegan directamente a los municipios de Támesis y de Jericó. **(Le puede interesar [Habitantes de Jericó tutelarán derecho a prohibir la minería en su territorio](https://archivo.contagioradio.com/habitantes-de-jerico-tutelaran-derecho-a-prohibir-la-mineria-en-su-territorio/))**

Adicionalmente, el vocero indicó que **“el Gobierno está empeñado en adelantar las actividades de extracción en la región”** y que por “intervención de las multinacionales y los mismos organismos gubernamentales se intentará invalidar el acuerdo” una vez más.

El vocero de la Mesa Ambiental de Jericó reprochó el voto de los cuatro concejales que se opusieron a la aprobación de la medida los cuales “no atendieron a los deseos de la comunidad” dejó claro con esta decisión que busca proteger en su municipio.

###### Reciba toda la información de Contagio Radio en [[su correo]
