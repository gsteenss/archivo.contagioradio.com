Title: A pesar de exigencias de Movimiento carcelario muere primera persona por COVID 19
Date: 2020-04-10 22:39
Author: CtgAdm
Category: DDHH, Nacional
Tags: #Recluso, carcel, Villavicencio
Slug: a-pesar-de-exigencias-de-movimiento-carcelario-muere-primera-persona-por-covid-19
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/04/Tramacua-1.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/04/Tramacua-2.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

Desde el 22 de Marzo, luego de cerca de un mes de aplicarse las medidas para prevenir el contagio del COVID 19 en las cárceles a través de la prohibición de visitas, pero sin controlar la entrada y salida de personal del INPEC y administrativos de las cárceles, el propio organismo **ha confirmado la muerte de una persona de 63 años,** interno de la cárcel de Villavicencio quién quedó en libertad el pasado 1 de Abril, muriendo el 7 de abril.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Según el comunicado del INPEC, el hombre habría estado recluido en la cárcel de Villavicencio hasta el pasado 7 de abril cuando fue liberado. Sin embargo, según las propias estadísticas del gobierno, los resultados de las pruebas del COVID **están tardando más de 8 días,** lo que aumenta la zosobra al interior de los penales. Ello debido a que no se tienen cifras concretas ni se han hecho las pruebas al interior de los establecimientos carcelarios.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### INPEC ni gobierno atendieron las exigencias de la población reclusa.

<!-- /wp:heading -->

<!-- wp:paragraph -->

Desde el pasado 22 de Marzo, más de 18 cárceles del país realizaron una dura jornada de protestas que dejó un saldo de 28 internos muertos de la cárcel La Modelo en Bogotá, tras una desmedida represión por parte del ejército, el ESMAD, el INPEC y la policía. Sin embargo, **a pesar de la gravedad de la situación no se tomaron las medidas necesarias para proteger a los internos.** (Le puede interesar: "[Más de 80 reclusos de cárcel de Palogordo están en huelga de hambre](https://archivo.contagioradio.com/mas-de-250-reclusos-de-carcel-de-palogordo-estan-en-huelga-de-hambre/)")

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Un ejemplo de ello es que se prohibieron las visitas pero tanto el personal del INPEC como administrativos de las cárceles podían ingresar a los penales sin ningún tipo de protección o protocolo de salud tendiente a contener el COVID 19. Esto se dio a pesar de un anuncio del gobierno nacional en torno a la declaración de una crisis carcelaria en la que se decieron aplicar medidas extremas para proteger la vida de los internos e internas.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Siguen sin mejora las condiciones de salud al interior de los penales

<!-- /wp:heading -->

<!-- wp:paragraph -->

Una denuncia de prisioneros de penal La Tramacua, deja al descubierto que no se ha aplicado ninguna acción para mejorar las condiciones de salubridad. Esto con el agravante de que no hay acceso al agua las 24 horas del día, en un lugar con más de 40 grados de temperatura.

<!-- /wp:paragraph -->

<!-- wp:image {"id":83026,"sizeSlug":"large"} -->

<figure class="wp-block-image size-large">
![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/04/Tramacua-1.jpg){.wp-image-83026}

</figure>
<!-- /wp:image -->

<!-- wp:image {"id":83027,"sizeSlug":"large"} -->

<figure class="wp-block-image size-large">
![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/04/Tramacua-2.jpg){.wp-image-83027}

</figure>
<!-- /wp:image -->

<!-- wp:heading {"level":3} -->

### Inpec activa protocolo de emergencia

<!-- /wp:heading -->

<!-- wp:paragraph -->

Aunque el INPEC anunció la activación del protocolo de emergencia, esta situación es grave por el nivel de hacinamiento carcelario que impide el distanciamiento social. Adicionalmente, las condiciones de salud al interior de los penales es crítica debido a que no hay enfermería ni atención médica. (Le puede interesar: ["Todos tenemos derechos, ellos también"](https://www.justiciaypazcolombia.com/todos-tenemos-derechos-ellos-tambien/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->
