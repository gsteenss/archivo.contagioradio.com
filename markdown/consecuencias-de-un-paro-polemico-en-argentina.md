Title: Consecuencias de un Paro polémico en Argentina
Date: 2015-04-04 18:38
Author: CtgAdm
Category: Capote, Opinion
Tags: Impuesto a la ganancia, Laura capote, Nahuel March, Paro en Argentina
Slug: consecuencias-de-un-paro-polemico-en-argentina
Status: published

###### Foto: saladeprensa.net 

#### Por [**[Laura Capote]**](https://archivo.contagioradio.com/laura-capote/) - [@lauracapout](https://twitter.com/lauracapout) 

El pasado 31 de marzo se llevó a cabo en las principales ciudades Argentinas un **Paro General** convocado inicialmente por las dos grandes centrales sindicales de Moyano y Barrionuevo en el gremio de los trabajadores de los medios de transporte, pero al cual se adhirieron posteriormente trabajadores de otros sectores de la sociedad Argentina, no por sus convocantes, sino por las reclamaciones profundas que lo motivaban y que eran trasversales a cualquier lineamiento político de quienes participaban: **Eliminación del impuesto a la ganancia**, **un impuesto que se cobra igualmente a todos y todas las trabajadoras con un ingreso mínimo sobre su salario**, el cual ni siquiera alcanza para el cubrimiento de la canasta básica familiar, de la mano del reclamo por el cese de la precarización laboral que está tan presente en la dinámica laboral argentina, caracterizada por la tercerización del trabajo y la inestabilidad laboral (con cifras de hasta el 40% de trabajadores en negro, es decir, en trabajo informal carentes de ningún tipo de garantías), es decir, la exigencia al gobierno de una urgente reforma tributaria.

Este, como los anteriores paros, han buscado ser capitalizados por parte de los distintos sectores políticos para aumentar o disminuir su contundencia. Trata, desde algunos sectores, de mostrarse como un paro sujeto simplemente a una puja de intereses entre los sectores políticos que se enfrentan al gobierno y que no tiene mayor adhesión en la clase trabajadora. Sin embargo, desde un punto de vista quizás mas profundo de la problemática, es evidente que, además de ser lógicamente expresión de esta riña política, responde a problemas laborales estructurales por los que atraviesan hoy las y los trabajadores argentinos que superan en gran medida esta disputa, y que tratan de minimizarse desde ciertos sectores.

Después de un paro similar que se presentó en 2014, el gobierno aumentó un 31% el salario de \$3.300 en ese año, a \$4.700 en enero del 2015 que, dependiendo del tipo de cambio oscilaría entre U\$376,60 (no oficial) y U\$587,50 (oficial). Para Nahuel March, de la mesa política de Camino de los Libres, estas expresiones del gobierno responden, además de a una presión por parte del sector trabajador por dichas mejoras, a un clima electoral por el que atraviesa el país durante este 2015 en el cual la política de gobierno busca salir favorecida, además de ser este un aumento que es absorbido por el pago del impuesto.

A pesar de las tensiones que dicho paro representa entre los distintos sectores, vale la pena resaltar que el mismo testifica que hay una masa trabajadora activa respecto sus problemáticas y situaciones y empoderada en cierta medida del avance de sus derechos y exigencias históricas. Es interesante desde este punto hacer una lectura de la situación laboral en el resto de nuestro continente, y especialmente en Colombia, tanto en el sentido de qué luchas estamos dando aún, como en el análisis de la unidad de la clase trabajadora en pro de seguirlas librando ante el Estado de manera efectiva.
