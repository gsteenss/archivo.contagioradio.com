Title: Hombres armados asesinan a poblador en Escuela de Dabeiba
Date: 2016-10-26 16:34
Category: DDHH, Nacional
Tags: Asesinatos, Dabeiba, paramilitares, paz, San Jose de Urama
Slug: hombres-armados-asesinan-poblador-de-dabeiba
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/10/dabeiba-e1477516905457.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [26 Oct de 2016] 

El pasado fin de semana mientras varios adultos recibían clase **en la Escuela del Corregimiento de San José de Urama en Dabeiba, varios hombres armados vestidos con prendas militares**, irrumpieron la clase y sin previo aviso **impactaron en dos oportunidades con arma de fuego a uno de los adultos que se encontraba allí**. Una vez realizado este acto, deciden sacarlo de las instalaciones y en presencia de la comunidad continuaron impactándole para ser posteriormente degollado.

Así describe este lamentable hecho Vanessa Vasco, abogada de la Corporación Jurídica Libertad quien asegura además que **la comunidad se encuentra atemorizada y muy conmovida por este acto.**

Vasco manifiesta, que al ser un lugar alejado del casco urbano **no se cumplieron los protocolos del levantamiento del cadáver pues las autoridades no llegaron a tiempo, y así mismo ve con preocupación que la Policía pese a ser informada del hecho por uno de los habitantes de la zona, no haya hecho mayores acciones** en miras a emprender una investigación en el caso “la Policía se llevó el cuerpo e hizo algunas preguntas a los habitantes de esta zona de Dabeiba, pero no es una entrevista que tenga un fin investigativo” puntualizó.

Actualmente, **la comunidad de San José de Urama en Dabeiba ha manifestado que no retornará a sus actividades normales hasta tanto por parte de la Alcaldía y la Gobernación no se garantice el respeto a la vida en sus territorios** “la comunidad está muy preocupada, en este momento está en un estado de temor y zozobra. Tendremos que esperar que pasa con las investigaciones luego de haber puesto la denuncia ante la fiscalía el pasado lunes” manifestó. Le puede interesar:  [Amenazas paramilitares en zona campamentaria de las Farc](https://archivo.contagioradio.com/amenazas-paramilitares-en-zona-campamentaria/)

**¿Accionar paramilitar en la zona?**

**Según la abogada Vasco, la forma de actuar de estos hombres que se encuentran en la población, es muy similar a los grupos paramilitares de otras épocas** “llegan  la población se identifican como autodefensas gaitanistas y se reúnen con la gente para decirles que ellos serán quienes ejerzan el control en la zona y que si no les gusta se pueden ir retirando de la zona. Llegan mostrando su poder a través de actos tan lamentables como el asesinato del poblador ocurrido el sábado”.

La abogada asegura que ya se han realizado los respectivos llamados a organizaciones y entidades estatales, que puedan garantizar la vida de las comunidades de la Comunidad de San José de Urama. Le puede interesar: [Paramilitares amenazan a organizaciones de derechos humanos del Valle del Cauca](https://archivo.contagioradio.com/organizaciones-ddhh-del-valle-del-cauca-amenazadas-por-paramilitares/)

<iframe id="audio_13490709" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_13490709_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)

<div class="ssba ssba-wrap">

</div>
