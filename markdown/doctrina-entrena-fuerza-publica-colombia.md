Title: La doctrina bajo la que se entrena la Fuerza Pública en Colombia
Date: 2019-06-08 10:35
Author: CtgAdm
Category: DDHH, Entrevistas
Tags: doctrina, Entrenamiento, Fuerza Pública, militares
Slug: doctrina-entrena-fuerza-publica-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/Doctrina-de-entrenamiento-militar.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: @petrogustavo] 

En el debate de control político "¿Por qué el Ejército comete homicidios?", el senador Gustavo Petro hizo públicos registros en video de entrenamientos que incluían tratos crueles, inhumanos y degradantes como latigar a soldados, recreación de situaciones de secuestro, o matando animales con la boca para tomar su sangre, lo que ha provocado cuestionamientos sobre la doctrina con la que se adiestra la Fuerza Pública, y la formación que recibe en materia de derechos humanos.

Eduardo Carreño, abogado del Colectivo de Abogados José Alvear Restrepo (CAJAR), afirmó que un entrenamiento que facilite tratos crueles e inhumanos como estos, haciéndolos pasar incluso como enemigos en combate, hace que se legitime la tortura en personas que son consideradas enemigos. Este tipo de formación se traduce en que es "absolutamente válido, legítimo hacer lo que se estime conveniente frente a un enemigo, es decir, perdiendo todas las condiciones humanas".

Para el abogado este tipo de formación rompe con las normas del combatiente, haciendo que no sea capaz de evaluar la proporcionalidad en el uso de las armas, respetando al enemigo que se rinde por cualquier situación o que en debe ser atendido porque está enfermo o herido. Situación que termina en una degradación de la vida, la honra y la dignidad de la otra persona; además, Carreño advirtió que "lo más grave es que el enemigo no es solamente el guerrillero, son también los no combatientes, o civiles".

### La [tesis] del enemigo interno deforma cualquier doctrina militar existente

Durante la época de la guerra fría se planteó la tesis del enemigo universal, cuya característica fundamental era ser comunista; dicha idea se exporto desde Estados Unidos hacía latinoamérica, haciendo que cada país lo incorporara a su realidad. La tesis del enemigo interno en Colombia significó luchar contra todo pensamiento distinto al que ejerce el poder, sin distinción entre las guerrillas y los progresismos legalmente constituidos y no violentos.

Bajo la tesis del enemigo interno, "cualquiera que no es mi amigo, es mi enemigo"; para el abogado, esta tesis es capaz de deformar cualquier doctrina militar existente porque "es un tratamiento para aniquilar o destruir a cualquiera que se considere enemigo". En lugar de ello, Carreño sostuvo que la Fuerza Pública requiere formación en protección de civiles así como de rivales en el combate, para que estén en capacidad de acatar las normas del Derecho Internacional Humanitario (DIH) y de los derechos humanos.

### **Soldados estarían recibiendo el entrenamiento que se da en Estados Unidos**

El Abogado integrante del CAJAR explicó que los criterios de formación, que aparentemente se estarían siguiendo, van alineados con la doctrina enseñada por Estados Unidos en la Escuela de las Américas, "donde han justificado todas las prácticas de tortura sobre los potenciales enemigos, que ellos plantean como blancos legítimos"; brindando asistencia médica/psicológica a personas torturadas para evitar que mueran y poder seguir martirizando, lo que se traduce en una deshumanización del enemigo, así como del torturador.

En esa medida, Carreño señaló que sería relevante que el Estado reconociera esos entrenamientos y los cambiará, pensando también en una modificación de la doctrina militar imperante, para que la Fuerza Pública sea también una fuerza constructora de paz. (Le puede interesar: ["Ejército habría torturado, asesinado e intentado desaparecer a Dimar Torres"](https://archivo.contagioradio.com/ejercito-habria-torturado-asesinado-e-intentado-desaparecer-a-dimar-torres/))

<iframe id="audio_36819496" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_36819496_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
