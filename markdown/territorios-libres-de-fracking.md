Title: #TerritoriosLibresDeFracking
Date: 2018-09-10 08:54
Author: Censat
Category: CENSAT, Opinion
Tags: Ambiente, fracking
Slug: territorios-libres-de-fracking
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/WhatsApp-Image-2019-06-07-at-5.57.15-PM.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Carlos Amaya 

##### **[Por: CENSAT Agua Viva - Amigos de la Tierra – Colombia](https://archivo.contagioradio.com/censat-agua-viva/)** 

##### 7 Sep 2018 

Del 29 de agosto al 1 de septiembre se realizó el Encuentro “Territorios Frente al Fracking en América Latina”, organizado por la Alianza Colombia Libre de Fracking y la Alianza Latinoamericana Frente al Fracking. Esta jornada consistió en una importante movilización de lideres latinoamericanos y nacionales de las luchas contra el fracking por diversos municipios del país: San Miguel de Sema, Tunja, Ráquira, Sáchica y Chiquinquirá en Boyacá; Puerto Wilches, Bucaramanga y San Vicente de Chucurí, en Santander, Yondó en Antioquia, Cantagallo en Bolívar y San Martín en Cesar y Bogotá.

La intensa semana contra el fracking acogió diversas expresiones: talleres, foros, encuentros y acciones simbólicas que lograron una importante incidencia con congresistas y periodistas del país para poner en el escenario público lo que implica el fracking en la vida y los territorios. Su objetivo principal consistió en advertir a las comunidades y autoridades locales sobre los problemas que puede generar la extracción de petróleo y gas a partir de la técnica conocida como fracturamiento hidráulico o fracking.

El fracking es una controvertida técnica que requiere grandes cantidades de agua mezclada con arena y un cóctel de sustancias químicas con hasta 750 componentes, de los cuales el 10% es altamente perjudicial para la salud humana y el ambiente. Otros asuntos como fugas de aguas contaminadas, sismicidad inducida, ocupación territorial intensiva, han sido denunciadas en EEUU y otros países donde ha sido aplicada la técnica.

Este panorama nos hace cuestionarnos: ¿De cuál seguridad en el proceso industrial hablamos cuando existen una importante incertidumbre de los daños que pueda generar? ¿Centramos nuestro debate sobre el tema de seguridad o mejor lo abordamos desde los conflictos ocasionados por el uso indiscriminado de químicos y la dispersión de material radioactivo que se libera con la perforación de las capas profundas del subsuelo?

Ante este escenario, la Alianza Colombia libre de fracking decidió elaborar una propuesta de ley mediante la cual se busca prohibir en el territorio nacional la exploración y/o explotación de los yacimientos no-convencionales de hidrocarburos y se dictan otras disposiciones para la prohibición del fracking en Colombia. Dicha ley busca además, a través de su artículo 6, cumplir con una deuda que tiene el Gobierno colombiano ante la sociedad en general y ante los territorios afectados por la extracción de hidrocarburos en concreto: “Los Ministerios de Minas y Energía, de Ambiente y Desarrollo Sostenible y de Salud, la Agencia Nacional de Hidrocarburos (ANH) y la Autoridad Nacional de Licencias Ambientales (ANLA) o las entidades que hagan sus veces, deberán elaborar y presentar al Congreso de la República en un término improrrogable de dos (2) años, un informe de los impactos socioambientales y de salud pública, y de los pasivos ambientales que han ocasionado las actividades de exploración y explotación de hidrocarburos que se han adelantado en el país”.

Para que esta ley sea tratada en el congreso, debe ser conocida y acogida por quienes estamos haciendo frente al modelo extractivista en su conjunto. De ahí que, la movilización social, ambiental y popular tiene que hacerse escuchar y sentir en las calles, los campos y los sitios de trabajo; puesto que la propuesta emerge de quienes han llevado un camino de afectaciones generadas por el modelo extractivista en sus territorios y han conocido los impactos que ha generado el fracturamiento hidráulico en otros sitios de América.

Nuestra invitación es a movilizarnos y exigir en el Congreso y en la calles que queremos una Colombia Libre de fracking. Tenemos suficientes argumentos para sustentarlo.
