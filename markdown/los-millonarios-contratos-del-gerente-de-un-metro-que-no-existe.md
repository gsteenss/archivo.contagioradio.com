Title: Los millonarios contratos del gerente de un metro que no existe
Date: 2017-02-24 18:10
Category: Nacional
Tags: Alcalde Peñalosa, Bogotá, Metro de Bogotá
Slug: los-millonarios-contratos-del-gerente-de-un-metro-que-no-existe
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/02/Metro-de-Bta.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Revista Semana] 

###### [23 Feb. 2017 ] 

**El metro aún no se construye en Bogotá, tampoco hay estudios pero ya existen dos millonarios contratos pactados con Andrés Escobar**, gerente de dicho proyecto. Uno de los contratos es por valor de \$174 millones de pesos y el segundo por \$126 millones, razón por la cual la Contraloría Distrital adelanta una investigación, **así lo denunció la representante a la Cámara, Ángela María Robledo.**

La preocupación de la cabildante no es solo por el monto del contrato de Andrés Escobar sino también por los **13 asesores que tiene el Alcalde de Bogotá** en distintos campos como comunicaciones, movilidad**, que le restaron el año pasado al presupuesto de la ciudad cerca de 2100 millones de pesos.**

“Eso es mucha plata, recordando que Enrique Peñalosa llegó de manera muy contundente criticando lo que había pasado en el gobierno anterior y anunció su famoso libro blanco, que en realidad es un listado de contratos sin ningún análisis” indicó Robledo. Le puede interesar: [Estas son las razones para la revocatoria de Enrique Peñalosa](https://archivo.contagioradio.com/estas-las-razones-la-revocatoria-enrique-penalosa/)

La denuncia que fue hecha el pasado 2 de Febrero en la audiencia que fue convocada por Alirio Uribe, Germán Navas e Inti Asprilla para hablar sobre las diversas inquietudes que tienen los representantes a la Cámara en cuanto a sus preocupaciones frente a la capital de Colombia. Le puede interesar: [Enrique Peñalosa desconoce Reservas en los Cerros Orientales](https://archivo.contagioradio.com/enrique-penalosa-sigue-desconociendo-reservas-forestales-en-los-cerros-orientales/)

Para Robledo, es preocupante todo lo que están perdiendo las ciudadanas y ciudadanos en cuanto a salud, educación, transporte y en todo lo que tiene que ver con las poblaciones más vulnerables como los habitantes de calle, vendedores ambulantes y personas en prostitución “por eso **queremos saber qué es lo que está pasando en Bogotá, aprovechando las épocas de transparencia”. **Le puede interesar: [\$60 millones por metro cuadrado: el negocio tras la crisis humanitaria del Bronx](https://archivo.contagioradio.com/60-millones-por-metro-cuadrado-el-negocio-tras-la-crisis-humanitaria-del-bronx/)

Además del monto de los contratos, la representante a la Cámara dice que han solicitado a la Contraloría que se investigue el contenido del contrato “**porque en el primer contrato de febrero del año pasado aparece como parte integrante del mismo la realización de los estudios y de eso todavía no sabemos nada".**

Otra de las solicitudes a la Contraloría es que investigue la contratación directa que se está dando “nos llama altamente la atención que con respecto a la contratación de otras entidades como la Secretaría de Gobierno o la Secretaría de Educación esta es desmesuradamente grande. Son cerca de 5200 millones de pesos”.

Dice Robledo que “**a esta Alcaldía además de gerencia le falta transparencia”** por eso ella y otros cabildantes se han dado a la tarea de hacer seguimiento a lo que sucede en la capital porque según ella **“hay una enorme indignación ciudadana, mucha desazón, desilusión con lo que estamos viviendo en la ciudad”. **Le puede interesar: [En riesgo ecosistemas de agua en la Sabana de Bogotá](https://archivo.contagioradio.com/en-riesgo-ecosistemas-de-agua-en-la-sabana-de-bogota/)

Se espera que la Contraloría de una respuesta frente a este caso en las próximas semanas, luego que la institución realice las investigaciones que requiere el caso “nos han dicho que se encuentran en la Secretaría General de la Alcaldía haciendo una auditoria”, sin embargo **de tardarse mucho existe la posibilidad de usar dos recursos más, un derecho de petición y después el derecho de insistencia.**

“Son dos herramientas que tenemos todos los ciudadanos pero que en nuestra condición de congresistas hay una prioridad.  Les daremos un tiempo, porque no es una tarea fácil, una semana quizás. Sino le insistiremos al contralor, al que igual le preguntamos ¿dónde está frente a este tipo de tareas” concluyó Robledo. Le puede interesar: [¿Cómo califica la administración de Enrique Peñalosa?](https://archivo.contagioradio.com/como-califica-la-administracion-de-enrique-penalosa-frente-a-la-alcaldia-de-bogota/)

###### Reciba toda la información de Contagio Radio en [[su correo]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio]{.s1}](http://bit.ly/1ICYhVU)[.]{.s1}

<div class="ssba ssba-wrap">

</div>
