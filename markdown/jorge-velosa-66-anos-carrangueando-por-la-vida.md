Title: Jorge Velosa: 66 años carrangueando por la vida
Date: 2015-10-06 15:30
Category: Ambiente, Nacional
Tags: Boyacá, canciones de Jorge Velosa, cumple años de Jorge Velosa, defensa del ambiente, Día del Campesino, En cantos verdes, Jorge Velosa, Raquira, reconocimientos de Jorge Velosa, Universidad Nacional
Slug: jorge-velosa-66-anos-carrangueando-por-la-vida
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/10/Jorge-Velosa.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: [www.youtube.com]

###### [6 Oct 2015] 

Hace 66 años, llegó al mundo el hombre que **convirtió la carranga en uno de los ritmos más representativos de Colombia para el mundo**. Jorge Velosa Ruíz, nació en Ráquira, Boyacá, el 6 de octubre de 1949.

Velosa ha recibido diferentes reconocimientos entre los que se encuentran la **Orden de la Libertad**, por parte del Departamento de Boyacá en 1997; en el año 2000, en Bolivia, una de sus canciones infantiles fue escogida oficialmente como ayuda en la enseñanza del español para las comunidades indígenas; la Universidad Nacional de Colombia y su Asociación de Ex alumnos le otorgó el **premio a la Excelencia Nacional en Artes y Ciencias**; y también recibió la **Condecoración Gonzalo Suárez Rendón**, en su más alto grado de Gran Collar de Oro, concedida el Día del Campesino por el alcalde de Tunja en 2008.

En 1994, **el biólogo John Lynch bautizó con sus nombres dos especies de ranas** perenecientes a la familia Leptodactylidae, a saber: Eleutherodactylus carranguerorum y Eleutherodactylus jorgevelosai, amabas están en peligro de extinción por la pérdida de su hábitat natural.

Se graduó de medicina veterinaria en la Universidad Nacional, en los años setenta y aunque nunca ejerció esa profesión por dedicarse al canto, si dedicó parte de su carrera en la música a que las personas tomaran conciencia frente al cuidado de los animales y la naturaleza, razón por la cual en **Contagio Radio**, decidimos recodar al máximo exponente de la carranga, con cinco canciones en las que le canta al ambiente, incluídas en el álbum “**En cantos verdes” del año 1998.**

1.  Planeta tierra

\[embed\]https://www.youtube.com/watch?v=p-sRvTS8yuU\[/embed\]

2. Marranito

\[embed\]https://www.youtube.com/watch?v=wh2Bq0I2OEs\[/embed\]

3\. Póngale cariño al monte

\[embed\]https://www.youtube.com/watch?v=waufAL27TCg\[/embed\]

4\. La rumba del bosque

\[embed\]https://www.youtube.com/watch?v=d3jFCznF4zw\[/embed\]

5\. La rumba de los animales

\[embed\]https://www.youtube.com/watch?v=rGSc98hw9IQ\[/embed\]
