Title: Con Cristian Sánchez ascienden a 230 los firmantes de paz asesinados
Date: 2020-09-28 15:26
Author: AdminContagio
Category: Actualidad, DDHH
Tags: asesinato de excombatientes, Cauca, Disidencias de las FARC
Slug: con-cristian-sanchez-ascienden-a-230-los-firmantes-de-paz-asesinados
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/09/EjBTfHWXkAATRw0.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: Cristian Sánchez/ FARC

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

El partido [Fuerza Alternativa Revolucionaria del Común (FARC)](https://twitter.com/PartidoFARC), denunció el asesinato del excombatiente **Cristian Arnulfo Sánchez Cuchillo de 26 años,** su cuerpo fue hallado el pasado 27 de septiembre en la vereda Santa Barbara, en zona rural del Municipio de Suárez, Cauca. Con Cristian, el registro de firmantes de la paz asesinados asciende a 230 desde la firma del Acuerdo de Paz.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Los hechos, previos al hallazgo del cuerpo de Cristian Sánchez, se dan desde el 13 de septiembre cuando fue hallado sin vida el comunero indígena **Luis Arley Chaguendo Liponce** con varios impactos de arma de fuego en su cuerpo. Luis, de 24 años quien vivía en la vereda López del municipio de Corinto, se transportaba en una moto junto a Cristian, quien desde esa fecha fue reportado como desaparecido.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Según relata la madre de Cristian Sánchez, quien se desempeñaba como jornalero, este vivía en el corregimiento de Tacueyó y posteriormente se trasladó a la vereda Olivares del municipio de Suárez. Su última comunicación con su familia, fue el 11 de septiembre vía telefónica.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Desde el 14 de septiembre su familia comenzó las labores de búsqueda hasta hallarlo en una fosa en la Vereda Santa Bárbara del municipio de Suárez.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Aunque las causas de su muerte están por ser establecidas, debido al avanzado estado de descomposición en que fue hallado, según relata la madre de Cristian, alias David, **integrante de las disidencias de las FARC, Dagoberto Ramos señaló como responsables a la columna Jaime Martínez.** Agrega que un integrante de este grupo la llevó hasta el lugar donde se encontraba enterrado su hijo. [(Le puede interesar: Tres diferencias entre disidencias de FARC recorriendo las guerras en Colombia)](https://archivo.contagioradio.com/tres-diferencias-entre-disidencias-de-farc-recorriendo-las-guerras-en-colombia/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Mientras las disidencias que operan en el Cauca se lucran del narcotráfico, la explotación ilícita de yacimiento mineros, el secuestro y la extorsión; Según el Instituto de estudios para el desarrollo y la paz (Indepaz), **en lo corrido del 2020 son más de 43 los excombatientes asesinados; Cristian es el segundo caso ocurrido en este departamento.**

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
