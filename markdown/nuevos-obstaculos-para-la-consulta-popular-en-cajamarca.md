Title: Denuncian nuevos obstáculos para la Consulta Popular en Cajamarca
Date: 2017-03-26 09:38
Category: Ambiente
Tags: Anglo Gold Ashanti, consulta popular minera, La Colosa Cajamarca
Slug: nuevos-obstaculos-para-la-consulta-popular-en-cajamarca
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/03/Cajamarca.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Catapa] 

###### [21 Mar 2017] 

Después de haber advertido del riesgo que suponían las elecciones atípicas a la alcaldía, el Comité promotor del NO en la Consulta Popular de Cajamarca, denunció la reducción de mesas de votación para la Consulta del próximo 26 de marzo, señalan que dicha modificación realizada por la alcaldía de Pedro Pablo Marín Cruz, impediría alcanzar el umbral necesario para hacer efectiva la Consulta.

El Comité promotor y los habitantes de Cajamarca han hecho un llamado público a la Registraduría, para que mantenga el número inicial de 36 mesas, puesto que la reducción a 18 mesas supone una falta de garantías para lograr los objetivos de la Consulta.

Son aproximadamente 5.500 personas las que deberán votar en la consulta, el Comité explicó que, con la mitad de las mesas y esa cantidad de votantes, cada persona “deberá demorarse al menos minuto y medio, situación que en la práctica no sucede”. Resaltaron además que, al estar sólo 18 mesas, se podrían generar largas filas y esperas entre 2 y 4 horas, “hay personas de la tercera edad que votarían y pueden durar más de 5 minutos votando”.

### **No están las garantías para la Consulta** 

Según el cambio, en el casco urbano de Cajamarca quedarían 16 mesas, 1 en Anaime y la otra quedaría en El Cajón. Por otra parte, el Código Nacional Electoral dispone en su Artículo 99 que: "La Registraduría Nacional,  fijará el número de ciudadanos que podrá sufragar en las distintas mesas de votación. Dicho número no podrá ser superior a 800 votantes en las mesas de censo ni a cuatrocientos 400 en las mesas de inscripción, el comité alertó que con la reducción de mesas "se están fijando 980 votantes por mesa".

Integrantes del Comité y defensores ambientales de diversas organizaciones, han manifestado que hay algunos intereses políticos y económicos del nuevo alcalde en relación al proyecto La Colosa de la empresa Anglogold Ashanti, “lo que supone nuevos retos y obstáculos para la defensa ambiental en Cajamarca”. ([Le puede interesar: Nuevo alcalde de Cajamarca promete garantizar la Consulta Popular contra la minería](https://archivo.contagioradio.com/nuevo-alcalde-de-cajamarca-promete-garantizar-la-consulta-popular-contra-la-mineria/))

Llamaron la atención sobre un pronunciamiento previo hecho por la comunidad, habían manifestado a la Alcaldía que no era conveniente “mezclar en un mismo mes las elecciones atípicas de alcaldía con las votaciones de la Consulta Popular”, sin embargo dichas recomendaciones no fueron escuchadas “y ahora nos preocupa que no estén las garantías”.

Por último, exigieron al nuevo alcalde que “brinde las garantías necesarias para el desarrollo de la consulta popular” programada para el 26 de marzo, y aseguraron que continuaran las movilizaciones, las marchas carnaval y demás actividades para que pueda hacerse efectiva la decisión de Cajamarca de decirle no a la minería.

###### Reciba toda la información de Contagio Radio en [[su correo]
