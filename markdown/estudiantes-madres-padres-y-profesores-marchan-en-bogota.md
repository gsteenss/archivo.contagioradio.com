Title: Educadores marchan en Bogotá en contra de privatización de la educación
Date: 2016-05-03 12:32
Category: Movilización, Nacional
Tags: ade, marcha docentes bogota, secretaria de educacion
Slug: estudiantes-madres-padres-y-profesores-marchan-en-bogota
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/05/ADE.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: ADE Bogotá ] 

###### [3 Mayo 2016 ]

En defensa del patrimonio público y en contra de la privatización de la educación, madres, padres, estudiantes y profesores marchan este martes en Bogotá. Denuncian la intensión de la administración Peñalosa de **entregar en concesión 15 colegios distritales**, la falta de garantías laborales y de infraestructura para la jornada única que se implementó en varias instituciones, la subcontratación para la ejecución del programa 40 x 40, así como las **irregularidades que se han venido presentando en la entrega de refrigerios escolares**.

De acuerdo con William Agudelo, presidente de la Asociación Distrital de Educadores, la subasta inversa implementada para la entrega de refrigerios escolares, en la que participan 10 empresas privadas, ha llevado a que en localidades como San Cristobal y Ciudad Bolívar **se disminuya la cantidad y calidad de los alimentos** que están llegando a las instituciones, sin tener en cuenta estándares de balance nutricional.

Según Agudelo, de los 160 mil refrigerios que deben ser entregados en los colegios **están dejando de llegar por lo menos 80 mil**, la mayor parte de los cuales son cambiados por refrigerios de contingencia, una galleta o una cocada más un jugo, con el que niños y niñas deben cumplir toda la jornada escolar, desde la 7 de la mañana hasta las 3 de la tarde.

La Secretaria de Educación ha asegurado que existen problemas en la contratación y en la entrega de refrigerios; sin embargo, no ha presentado soluciones, por lo que los maestros insisten en la **necesidad de que cada colegio cuente con un restaurante escolar**. Frente a estas problemáticas y exigiendo que se negocie el pliego de peticiones radicado el 25 de febrero, los docentes del distrito consideran entrar en paro indefinido a finales de este mes.

###### [Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)]] 
