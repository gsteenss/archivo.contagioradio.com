Title: Estética e identidad en el cine colombiano
Date: 2015-02-18 22:07
Author: CtgAdm
Category: 24 Cuadros
Tags: 24 cuadros, Cine, You Tube
Slug: estetica-e-indentidad-en-el-cine-colombiano
Status: published

###### Foto: caliescribe.com 

**El debate entre la estética y la identidad en el cine colombiano tiene raíces profundas y posiciones variadas** que durante años han encontrado su eco en diferentes canales: publicaciones escritas, espacios radiales y televisivos y en los últimos años  a través de la red.

En el marco de la convocatoria de la **Dirección de Investigación de la Universidad de Bogotá Jorge Tadeo Lozano**, Nadia Gonzales Bautista, docente del programa profesional de Cine y Televisión de la Institución, presentó un proyecto en el que pretende llevar estos debates a la web por medio de la producción audiovisual, **en busca de generar nuevos espacios de discusión que favorezcan a la consolidación del cine nacional.**

Bajo el título: **“Cine colombiano: Estética  vs Identidad”,** pude encontrarse hoy a través de la red de videos Youtube,  el resultado de este primer trabajo mancomunado entre la Universidad, profesores de la facultad y estudiantes del semillero de investigación **“Géneros Argumentales en Colombia”** también dirigido por la creadora del proyecto.

A las voces del crítico e investigador **Pedro Adrián Zuluaga**, el diseñado de producción y director de arte Ricardo Duque, se suman las de los directores y productores **Gabriel Gonzáles y Sphiros Stathoulopoulos, Miguel Urrutia Y Liliana Tavera entre otros**; quienes comparten ideas y conceptos desde su perspectiva, experiencia y conocimiento en el campo audiovisual en general y del diseño de arte en particular y en esta primera entrega.

Como ingrediente añadido dentro del debate expuesto en el audiovisual documental, aparece **la figura de la distribución y la recepción de las audiencias**, que resulta determinante dentro del ciclo de distribución y el destino que los realizadores dan a sus producciones; situación a la que el proyecto pretende aportar de alguna manera dejando el debate abierto y listo para ser presentado en una siguiente producción que se espera circule muy pronto en la red.

http://youtu.be/pHenySJFkAw
