Title: Continúan amenazas a líderes barequeros y opositores de hidroituango
Date: 2015-06-30 12:38
Category: DDHH, Nacional
Tags: amenazas de muerte, Antiquia, barequería, Empresas Públicas de Medellín, EPM, Hidroituango, Ituango, megaproyectos, Playa la Arenera, Rios Vivos
Slug: continuan-amenazas-a-lideres-barequeros-y-opositores-de-hidroituango
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/06/Hidroituango-no-es-paz.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: [polinizaciones.blogspot.com]

<iframe src="http://www.ivoox.com/player_ek_4705799_2_1.html?data=lZydl5ydfY6ZmKiakp2Jd6KplpKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRh9Di1c7bh6iXaaO1wtOYw9LJssLuwtiYw5DQaaSnhqaxxsrWqdSfw8bfx9bZqdPj1JDmjdTUs9Td1dTfx9iPcYarpJKw0dPYpcjd0JC%2Fw8nNs4yhhpywj5k%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Juan Pablo Soler, Movimiento Ríos Vivos] 

**Continúan llegando amenazas a barequeros y opositores de hidroituango,** esta vez de parte del grupo autodenominado** NPLM**, que a través de un mensaje de texto afirma tener identificados los líderes que se oponen al mega proyecto, les da 72 horas para que salgan de la Playa "la Arenera", donde se construye la hidroeléctrica de Empresa Públicas de Medellín, EPM.

\[caption id="attachment\_10621" align="alignleft" width="300"\][![Amenaza a los teléfonos de Ríos Vivos](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/06/CIrn58aWgAAnnVU.jpg "Amenaza a los teléfonos de Ríos Vivos "){.wp-image-10621 width="300" height="400"}](https://archivo.contagioradio.com/continuan-amenazas-a-lideres-barequeros-y-opositores-de-hidroituango/cirn58awgaannvu/) Amenaza a los teléfonos de Ríos Vivos\[/caption\]

Así mismo, en días pasados había llegado un [me](https://archivo.contagioradio.com/movimiento-rios-vivos-nuevamente-es-blanco-de-amenazas/)[nsaje con amenaza de muerte al celular de María Eugenia Gómez, i](https://archivo.contagioradio.com/movimiento-rios-vivos-nuevamente-es-blanco-de-amenazas/)ntegrante del movimiento Ríos Vivos, en el que se amenazaba la vida de todas las personas que se estén oponiendo a desalojar la playa del Río Cauca.

Juan Pablo Soler, integrante del mismo movimiento, denuncia que hace unos meses apareció pintado uno de los cambuches en las playa con un mensaje amenazante firmado con la misma sigla, **NPLM**.

Al intentar interponer la denuncia ante las entidades gubernamentales correspondientes, los funcionarios no la recibieron  porque no se encontraban, circunstancia que motivó a los integrantes del colectivo dar a conocer el hecho a la Fiscalía directamente, sin embargo, **hasta el momento no hay ningún resultado de la investigación que deben llevar a cabo**, como lo asegura Soler.

Los integrantes del Movimiento Ríos Vivos denuncian que con frecuencia, “se ha observado que ha sido **el personal** **de vigilancia privada de EPM los que a diario persiguen y presionan a barequeros** para que salgan de las playas del río Cauca, argumentando que fueron contratados para no permitir que alguien esté allí, estos han insistido a los barequeros en que deben salir y han llegado a impedir el ingreso por los caminos que conducen a las playas".

“**Nos preocupa el seguimiento que hay a los barequeros, ya que el ejercito reconoce a los líderes**, lo que atemoriza a los pobladores”, dice el integrante del Movimiento Ríos Vivos.

**Cabe recordar que en 2013 se registró que existen más de 500 familias que se dedican a la barequería** en esa zona del país, pero los hechos amenazantes han generado que muchos de ellos hayan decidió desplazarse.
