Title: Indígenas del Norte del Cauca se unen a movilización nacional
Date: 2018-11-23 14:57
Author: AdminContagio
Category: Movilización, Nacional
Tags: Cabildos Indígenas, Movilización Indígena
Slug: indigenas-del-norte-del-cauca-se-unen-a-movilizacion-nacional
Status: published

###### Foto: Contagio Radio 

###### 23 Nov 2018 

En rechazo al plan de gobierno de Duque, las amenazas contra líderes y lideresas y la restricción presupuestal a los Acuerdos de Paz, **la Asociación de Cabildos Indígenas del Norte del Cauca (ACIN)** decidió sumarse a la movilización nacional, plantear su propia agenda política y anunciar su participación el 28 de noviembre en la gran marcha nacional.

Otros de los motivos por los que la ACIN se unirá a las movilizaciones, según **Edwin Güetio, vocero del cabildo,** son manifestarse en contra de los proyectos que actualmente cursan en el Congreso y que afectarían de forma directa a las comunidades como el acto legislativo que limita el derecho a la tutela y el proyecto de ley de Financiación, además de rechazar la desfinanciación de la educación pública y la fuerte represión a la protesta social.**[(Le puede interesar: ESMAD vuelve a atacar a comunidades indígenas en Norte del Cauca)](https://archivo.contagioradio.com/esmad-vuelve-a-atacar-comunidades-indigenas-en-norte-del-cauca/)**

### **Las consultas previas una herramienta democrática de las comunidades indígenas** 

Güetio reprobó en particular el proyecto de ley que busca regular la consulta previa en Colombia, una propuesta que reduciría la  autonomía de las comunidades indígenas para decidir sobre los proyectos que se llevan a cabo dentro sus territorios,  pues la consulta “es la única herramienta que nosotros tenemos como comunidad indígena para blindar nuestros recursos naturales”.

Asimismo, advirtió sobre la importancia de movilizarse precisamente porque es a final de año **“cuando el Gobierno aprovecha que la mayoría del país se encuentra en fiestas navideñas y en temas de fútbol  y el congreso aprovecha para aprobar las leyes que van en contra de los derechos  de la población colombiana”**, razón por la cual el cabildo preparó su propia agenda política fundamentada en siete puntos clave:

1\. Cuidado de la protección del territorio específicamente del agua  
2. Territorio libre de violencia contra las mujeres  
3.  Fortalecimiento de las organizaciones sociales negras e indígenas  
4. Defensa del patrimonio público y del estado, no se pueden definir o tomar historias o decisiones desde el estado  
5. Fortalecimiento de los sistemas propios como el de salud, educación, vivienda  
6. Cumplimiento de los acuerdos con el Gobierno Nacional  
7.  Defensa de la vida y los derechos humanos

El cabildo comunicó que se sumará a la movilización de este 25 de noviembre en defensa de los derechos de la mujer y la no violencia y posteriormente se movilizarán hacia Jamundí para el 28 unirse a la gran movilización en la ciudad de Cali como muestra de acompañamiento y respaldo político a los sectores educativos  y sociales que luchan por sus derechos.

###### Reciba toda la información de Contagio Radio en [[su correo]
