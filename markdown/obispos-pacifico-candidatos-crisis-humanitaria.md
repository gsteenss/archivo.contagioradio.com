Title: Obispos del Pacífico piden a candidatos atender crisis humanitaria
Date: 2019-09-12 14:47
Author: CtgAdm
Category: Comunidad, Nacional
Tags: Chocó, corrupción, Elecciones 2019, pacífico
Slug: obispos-pacifico-candidatos-crisis-humanitaria
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/09/Chocó.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Comisión de Conciliacion] 

En medio de la crisis humanitaria que viven cerca de 30 municipios del Pacífico, obispos de las **Diócesis de Itsmina, Quibdó y Apartadó**, realizaron un llamado a los candidatos a alcaldías y gobernación para que actúen e incluyan dentro de sus planes de gobierno, políticas para dar atención real a la crisis humanitaria, social, económica y ambiental que vive una región que escogerá a sus dirigentes el próximo mes de octubre.

Desde el trabajo conjunto realizado a nivel pastoral por las Diócesis de estos municipios, monseñor Juan Carlos Barreto, señala la importancia de expresar la visión política que tiene la iglesia de cara a las elecciones locales, resaltando la necesidad del **"buen uso de recursos y la implementación de políticas públicas eficaces".**[(Lea también: CIDH estudia aumento de la violencia en el Chocó)](https://archivo.contagioradio.com/cidh-estudia-aumento-de-la-violencia-en-el-choco/)

> Desde el [\#Chocó](https://twitter.com/hashtag/Choc%C3%B3?src=hash&ref_src=twsrc%5Etfw) sus obispos hacen un llamado a tomar acciones urgentes ante la crisis humanitaria, social, económica y ambiental de este departamento del Pacífico colombiano. [@dioapartado](https://twitter.com/dioapartado?ref_src=twsrc%5Etfw) @DiócesisdeQuibdó [@diocesismision](https://twitter.com/diocesismision?ref_src=twsrc%5Etfw) [@VerdadPacifico](https://twitter.com/VerdadPacifico?ref_src=twsrc%5Etfw) [@ONUHumanRights](https://twitter.com/ONUHumanRights?ref_src=twsrc%5Etfw) [@HREV\_Derechos](https://twitter.com/HREV_Derechos?ref_src=twsrc%5Etfw) [pic.twitter.com/3uiT22gx36](https://t.co/3uiT22gx36)
>
> — CRPacifico (@CRPacifico) [September 11, 2019](https://twitter.com/CRPacifico/status/1171792308020400128?ref_src=twsrc%5Etfw)

<p>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
</p>
### Un ciudadano que vende su voto, participa de la corrupción

Monseñor Barreto afirma que este es uno de los temas más álgidos del país, pues las personas pueden convertirse fácilmente en víctimas de compra de votos, además resalta que el departamento requiere propuestas concretas por parte de los candidatos a las que se les pueda hacer veeduría ciudadana agregando que la austeridad y transparencia en las campañas son un reflejo de lo que será la administración pública.

> Es importante que se vote por un proyecto al que después la ciudadanía pueda hacerle un seguimiento a los programas de Gobierno.

### Seamos conscientes de la crisis en la que está Pacífico 

Acerca de la crisis humanitaria que vive el departamento consecuencia del "abandono estatal, la corrupción de entes territoriales y la presencia de grupos armados ilegales", además de la situación de conflicto que persiste en medio de elecciones, monseñor afirmó que el regreso de las violencias requiere propuestas de construcción de paz y se dé una articulación entre municipios población y nación.

**"En un territorio como Chocó siempre está la tentación de hacer campañas exorbitantes y que cuando se analiza por qué lograron todo ese despliegue publicitario pues se encuentra un transfondo de ilegalidad que puede estar financiando estas campañas"  a**dvierte monseñor quien destaca la importancia de que los candidatos lleguen a los cargo con el único interés de servir a su comunidad".

### El Pacífico en contexto

Según la Procuraduría, para 2016 existían **300  investigaciones en contra de funcionarios públicos** por diversos casos de corrupción, de las cuales avanzaban 85 con procesos abiertos. Como uno de los ejemplos de los niveles de corrupción en el departamento del Chocó, recientemente fue capturado **Isaias Chalá Ibargüen**, alcalde de Quibdó por celebración de contratos sin el cumplimiento de los requisitos exigidos, interés indebido, peculado por apropiación y falsedad en documento público.

Adicionalmente, un informe de Razón Pública señala que apellidos como Córdoba, Palacios, Maya y Lozano y al menos hace 20 años otras familias como los Sánchez Montes de Oca y los Torres, han ocupado escaños en el Congreso y en la Asamblea, la gobernación y la alcaldía en Quibdó y en otros 28 municipios donde se encuentran sus allegados, haciendo alianzas temporales. [(Le puede interesar: Más de 70 familias afectadas por combates entre grupos ilegales en Juradó, Chocó)](https://archivo.contagioradio.com/mas-de-70-familias-afectadas-por-combates-entre-grupos-ilegales-en-jurado-choco/)

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
