Title: "Hasta que haya mesa de interlocución, desbloqueamos la vía"
Date: 2016-06-08 11:04
Category: Paro Nacional
Tags: congreso de los pueblos, Coordinador Nacional Agrario, Minga Nacional, Paro Colombia 2016
Slug: hasta-que-haya-mesa-de-interlocucion-desbloqueamos-la-via
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/06/Minga-Nacional-Cauca.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo ] 

###### [7 Junio 2016 ]

La Minga Nacional ya va en su noveno día, por un lado, las comunidades aseguran que desbloquearán la vía Panamericana hasta que se instale la mesa de interlocución entre el Gobierno y los voceros de la Cumbre Agraria, y por el otro, el Gobierno afirma que esta interlocución no se dará hasta que no sea desbloqueada la vía. Los campesinos insisten en su exigencia, porque **temen que, como en el ocasiones pasadas, el desbloqueo de la vía sea aprovechado para redoblar la fuerza pública y agredir a los manifestantes**.

Robert Daza, vocero del Coordinador Nacional Agrario e integrante del Congreso de los Pueblos, asevera que el desarrollo de esta Minga ha sido dinámico y complejo; de una parte, algunas comunidades han decido dejar las vías por uno o dos días para luego volver, por otra, **el Gobierno nacional no ha querido tratar con la seriedad que se requiere las propuestas presentadas por la Cumbre Agraria**.

Pese a que se instaló una [[comisión de derechos humanos](https://archivo.contagioradio.com/se-reactiva-comision-conjunta-de-ddhh-para-evitar-mas-agresiones-contra-minga-nacional/)] para atender las agresiones que se han presentado en el marco de la Minga, desde el Ministerio de Defensa se insiste en que esta movilización está infiltrada por el ELN, acusación que es rechazada por las comunidades rechazan, argumentando que **son autónomas y se están manifestando por las afectaciones que les han representado las actuales políticas públicas**.

Mientras se instala la mesa de interlocución en La María, Piendamó, **los campesinos reiteran la exigencia de que el Ejército Nacional se retire de los puntos de concentración** y que el ESMAD no intervenga en la movilización. Así mismo hacen el llamado para que [[los 15 menores que fueron detenidos en Berlín](https://archivo.contagioradio.com/100-campesinos-detenidos-en-santander-durante-el-quinto-dia-de-minga-nacional/)], Norte de Santander, sean puestos en libertad.

\[audio mp3="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/06/Robert-Daza.mp3"\]\[/audio\]

###### [Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)]] 
