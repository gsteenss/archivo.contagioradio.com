Title: La postura de la guerrilla del ELN frente al proceso de paz
Date: 2016-04-12 13:42
Category: Entrevistas, Paz
Tags: Paramilitarismo, proceso de paz eln, sociedad civil
Slug: la-postura-de-la-guerrilla-del-eln-frente-al-proceso-de-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/04/proceso-de-paz-eln.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Caracas. Abr 2016 

El país recibió con júbilo el anuncio de la fase pública de los diálogos entre el Ejército de Liberación Nacional -ELN- y el Gobierno Nacional. En medio de este episodio, *Alba TV, Contagio Radio y Colombia Informa*, conversaron con los voceros de la delegación de paz de esa guerrilla: los comandantes ‘Pablo Beltrán’ y ‘Antonio García’.

Nunca antes Colombia se había acercado tanto a conseguir la solución política del conflicto armado entre insurgencias y Gobierno. El anuncio de esta nueva mesa de conversaciones alimenta la esperanza para construir caminos hacia la paz y a propósito de las particularidades de la agenda, de los tiempos de la negociación, de las coincidencias con el proceso de La Habana y el papel de la sociedad en esta mesa de conversaciones conversamos tres medios alternativos de Colombia y Venezuela con dos comandantes de una de las guerrillas más antiguas del mundo.

**Frente al tema de la participación de la sociedad, ustedes han planteado una serie de mecanismos. En el acuerdo se habla de varias formas en las que podría participar la sociedad civil, ¿ustedes creen que hay tiempo si se va a firmar un acuerdo conjunto en 2016? **

**Antonio García:** Si se requiere que se avance en la solución política del conflicto, lo fundamental es que haya tiempo para que la gente pueda formular interrogantes a los problemas que históricamente no se han resuelto. El tiempo de la paz necesariamente tiene que ver con trabajar soluciones a los problemas que la sociedad necesita. No es un tiempo cronológico o simplemente de números, de años o de meses, sino la búsqueda de la soluciones. En la medida que la sociedad pueda estructurar soluciones, que pueda discutir los problemas, que pueda crear un nuevo consenso en la búsqueda de soluciones que interpreten a la diversidad de Colombia, no solamente a una parte.

**La guerrilla del ELN es histórica y ha tenido varios intentos de hablar de paz y sentarse en un proceso. ¿Ustedes creen que ahora es el momento en que un proceso de conversaciones puede generar esas condiciones de las que habla el comandante Antonio?**

**Pablo Beltrán:** Nosotros estamos participando en estos procesos desde el año 91, 25 años. ¿Cuál es la diferencia de este con procesos anteriores? Es más madura, eso es cierto. Ambas partes tenemos más conciencia de lo que es posible y de lo que no es posible. Somos más realistas: 25 años de intentos de procesos de solución política también dejan muchas enseñanzas.

**¿Pero ahora sí? ¿Será el tiempo de la paz?**

**A.G.:** El tiempo de la paz tiene que estar ligado con el tiempo de las soluciones, de la implementación de los acuerdos. El acuerdo de la solución política no es la paz. Eso es hipotéticamente. Hay que ir a la realidad para ver si esos acuerdos se pueden implementar, que puedan incidir sobre la realidad y cambiar las circunstancias del conflicto.

Podemos decir que el tiempo de la paz llegó cuando haya una sincronía con la ejecución de los acuerdos. No es solamente se trata de la voluntad que se expresa en firmar el acuerdo sino que ese documento tenga la fuerza, la capacidad, la audiencia, el consenso en la sociedad colombiana para cambiar el país.

**Algunas personas dicen que el proceso de conversaciones de La Habana está muy alejado de la realidad de la gente, ¿cómo acercar ese proceso y este proceso a esa realidad, de tal manera que se puedan impulsar esos cambios sociales? **

**P.B.:** Una de las experiencias que se extraen de proceso anteriores es esa. Para que estos procesos tengan éxito, hay que empoderar a la sociedad como sujeto protagónico e independiente de las partes que están en la mesa. Poner eso como una meta, como un requisito, justifica que hayamos determinado como primer punto de la agenda la participación. ¿Cómo lograr eso? El diseño no lo va a hacer el ELN ni el Gobierno, lo tiene que hacer la misma gente. ¿Cuál es punto de partida? El conjunto de la sociedad colombiana que se está movilizando por la paz. De ahí se parte para trascender más allá hacia los sectores que no están ni organizados, ni movilizados ni interesados en estos procesos.

**Sin embargo, a muchos de esos procesos el Gobierno les ha incumplido, ¿cómo hacer que esos procesos de participación sean eficaces cuando, según dice la gente misma, no hay una voluntad política real de cambio por parte del Estado?**

**A.G.:** Una solución política es el reconocimiento de que la sociedad vive un conflicto que lo ha llevado a la confrontación militar, quiere decir que no ha tenido la capacidad de tramitar correctamente los conflictos. Es un conflicto mayor producto de que la democracia no funciona bien. Se requiere de una gran reflexión: es la diferencia entre un pliego de peticiones y el reconocimiento de la incapacidad del Estado para tramitar los conflictos que tiene que irse a buscar el origen de la confrontación en las luchas del movimiento social y político, de los pueblos originarios, de los pueblos negros, de las mujeres.

Se requiere asumir que en Colombia no hay democracia, reconocer que en nuestro país hay pobres, marginados, trabajadores, desplazados, víctimas.  Es de ese reconocimiento y de esa diversidad de donde nace un nuevo consenso. La solución política abre una tremenda posibilidad de generar un proceso democratizador.

\[caption id="attachment\_22544" align="aligncenter" width="800"\][![Proceso de paz eln300316-10](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/04/Proceso-de-paz-eln300316-10.jpg){.wp-image-22544 .size-full width="800" height="533"}](https://archivo.contagioradio.com/la-postura-de-la-guerrilla-del-eln-frente-al-proceso-de-paz/proceso-de-paz-eln300316-10/) Pablo Beltrán, delegado Comisión de paz ELN\[/caption\]

**¿Ustedes creen que con esta fase pública y por la vía política sí se va a lograr lo que en estos años no se ha conseguido con la confrontación armada?**

**P.B.:** Este proceso es un experimento y una exploración. Los intentos anteriores no han sido tan claros. ¿Se acuerda de un proceso en el año 91? Se hizo una nueva Constitución, un pacto que lleva más de 30 ajustes en función de las transnacionales. Eso da una muestra de la voluntad política porque las constituciones son tratados de paz, para mencionar la inmediatamente anterior.

Entonces, si ésta que viene va a tener éxito, lo va a dar el empoderamiento de la sociedad porque si estos procesos se quedan solamente en la mesa, no pasan de ser uno más. Pero si la gente lo retoma, será un proceso democratizador y por supuesto se abre una vía muy importante que de verdad va a aportar a que las cosas cambien.

**Es decir, ¿constitución? ¿Constituyente?**

**A.G.:** No es que en Colombia falte una constitución o una constituyente. Incluso muchas de las cosas que están en la constitución, si se aplicaran, no tendríamos tantos problemas. Hay que ver por qué mucha de la letra no se cumple. Tiene que ver en la manera de ver la política, las instituciones, cómo se orienta el tratamiento a los problemas de la sociedad. Muchas veces, para decirlo de una manera muy concreta, la medicina puede existir pero si no está el médico que identifique la enfermedad o que el paciente no tenga el dinero para comprarla o que la EPS le dé la cita para formularle la medicina… Eso es la constitución, la medicina, pero si no se aplica o depende de a quién se le aplica, ¿entonces?

En Colombia no hay ciudadanía. No todos tenemos los mismos derechos ni las mismas condiciones para acceder a ellos. No es un Estado ni de derecho ni garantista entonces, ¿para qué una constitución si estamos fallando en lo básico?

**El paramilitarismo tiene preocupado al país, ¿cómo entienden ustedes ese fenómeno y que plantean de entrada al respecto?**

**A.G.:** El paramilitarismo para el ELN es una lógica creada por el Estado y enmarcada en una política internacional del Pentágono. Es la participación del Estado en el control sobre quien disienta de las políticas de Gobierno. Para eso, se desarrollan estructuras que encubren, que protegen al Estado y que aparentemente funcionan paralelamente a la institucionalidad  pero responden a las directrices del Estado. Pero en Colombia eso se ligó tanto que llegó un punto en el que no se pudo diferenciar. Nosotros, en el campo de la confrontación militar, cuando atacábamos a los paramilitares resultábamos combatiendo con compañías o batallones del Ejército. Los paramilitares se movían en una lógica de complementariedad en la confrontación militar con el Estado.

Ahora, que eso ya se haya superado, no lo creemos. Lo que ocurrió fue que el paramilitarismo se trasladó hacia adentro de las instituciones, lo que permitió posteriormente los falsos positivos. Ese es un reflejo de que el paramilitarismo está adentro. Es una manera de escaparse a la justicia para obedecer unas disposiciones políticas. En estos días están llamando a declarar al general Montoya porque es quien más exigía resultados. Él no pedía heridos, no pedía capturados: Pedía muertos.

**En los últimos días, el paro de las AGC dejó 17 víctimas. Unos mencionados en panfletos, otros amenazados. Este es un fenómeno vivo, no solamente se ha dedicado al control social, está matando gente. ¿A ustedes les preocupa esa realidad de cara al proceso?**

**P.B.:** Los paramilitares están aliados con los sectores de ultraderecha que no quieren los procesos de paz. Sin embargo, el Gobierno niega la existencia de estos grupos, dice que ya no existe el paramilitarismo, que son bandas. La realidad demuestra que sí existen, que sí tienen relaciones con el Estado, pero insisten en que solo son ‘manzanas podridas’. Aseguran que no es una decisión del Estado la guerra sucia, que se trata de casos aislados de personajes que se extralimitaron en sus funciones. Si los que desataron al guerra sucia no reconoce que son los responsables, ¿el futuro cuál es? ¡Que lo sigan haciendo!

La discusión sobre justicia tiene que ver con eso, con asumir responsabilidades por lo que ha ocurrido, por lo que está ocurriendo, ¿sí o no? Nosotros, como guerrilla, las vamos a asumir pero lo que vemos del Gobierno es que no, que ellos individualizan los casos pero como Estado, como régimen, no lo quieren a asumir.

**Hablando del tema de la justicia, existe un controvertido acuerdo al respecto construido en el proceso de La Habana, ¿qué piensan de la jurisdicción especial para la paz? ¿Puede servir para el ELN?**

**P.B.:** Sobre lo que se ha trabajado en La Habana, nosotros decimos que son acuerdos que se han hecho allá. Por supuesto, que si el Gobierno quiere llevar los conceptos o el modelo que se ha trabajado allá a esta negociación, nosotros no tenemos problema.

No obstante, creemos que es imposible que haya justicia si no hay verdad. ¿Las clases dominantes tienen disposición de reconocer la verdad del conflicto o va a ser solo el cuento de las ‘manzanas podridas’? Ese es el corazón del problema porque sin verdad, no hay reparación ni garantías de no repetición.

Esa es la discusión cuando en el punto de víctimas hablamos del tema de no olvido. Hay que hacer memoria, estos pueblos necesitan memoria colectiva e historias comunes. En ese sentido, ese es un punto que vamos a tocar en el diálogo sobre víctimas, acá debe haber verdad histórica, no es un asunto solo de académicos. ¿Somos capaces los colombianos de tener una historia común del conflicto o cada uno va a hacer su historia por aparte?.

**Y entonces ¿cómo se hace para construir la memoria histórica, para desenterrar la verdad?**

**A.G.:** En el caso del ELN hacemos una precisión. Una cosa es el tema de las víctimas y otra, muy diferente, la situación jurídica de nuestros integrantes. Porque a veces se quiere relacionar la justicia como si la insurgencia fuera una ‘fábrica de victimarios’ y nosotros no estamos acá para negociar eso. El ELN tiene una condición de ser un movimiento alzado en armas y va a negociar con el Gobierno colombiano cómo va a ser la nueva situación jurídica del ELN y sus integrantes para hacer la política legal.

El tema de justicia será abordado por la comunidad de víctimas. Eso no es con el ELN. Vamos a contribuir para que ese tema sea tratado por los afectados, ¿cómo va a ser el mismo Estado, quien se hizo ausente para proteger a la sociedad y para aplicar la justicia cuando se violó, el que la aplique ahora?  Se trata de reconstruir los hechos, que es la verdad histórica, lo que nos debe llevar a la verdad jurídica.

**Por todo lo que mencionan surge la necesidad de hablar de Estado Unidos, ¿cuál  es el papel de Estado Unidos, su responsabilidad en este conflicto?**

**P.B.:** En una publicación del Washington Post del mes de diciembre de 2013, en la que el Pentágono comenta que las FARC están negociando porque ‘hicimos’ la guerra. Y no lo digo yo, lo dice Estados Unidos: “la guerrilla está sentada gracias al diseño de guerra que aplicamos”.  Ellos hacen la guerra para qué. Por ejemplo, ¿cuál es el departamento que tiene más tierra productiva en el país? El Vichada. ¿Cuál fue el modelo de guerra en este territorio? De paramilitarismo, de descomposición del tejido social y comunitario indígena, de disgregación de la organización  social de los indígenas. ¿Después qué vino? Las Zidres que piden la entrega de la altillanura a las multinacionales del agronegocio. ¿Todo eso es una coincidencia? No, son etapas del mismo plan.

El despojo de las comunidades afro en el Bajo Atrato lo motivó la constitución de grandes empresas para la producción de palma aceitera. Primero vino el paramilitarismo, la expulsión de las comunidades negras, el apropiamiento de los paramilitares de esas tierras, vendérselas al agronegocio, asociarse al capital extranjero y hoy existe una mezcla entre ese capital y los testaferros de los paramilitares que expulsaron a las comunidades negras. Ese modelo es la prueba de que primero viene la guerra sucia, toda la destrucción del tejido social y luego los grandes proyectos de palma aceitera, de caucho, de cacao.

\[caption id="attachment\_22548" align="aligncenter" width="800"\][![Antonio García -proceso de paz eln](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/04/Antonio-García-proceso-de-paz-eln.jpg){.wp-image-22548 .size-full width="800" height="533"}](https://archivo.contagioradio.com/la-postura-de-la-guerrilla-del-eln-frente-al-proceso-de-paz/antonio-garcia-proceso-de-paz-eln/) Antonio Garcia y Pablo Beltrán -Delegados de paz ELN\[/caption\]

**Es decir, ¿hay una responsabilidad de Estados Unidos en todo eso?**

**A.G.:** Lo que termina de decir Pablo es muy importante porque en el fenómeno que analizamos confluyen varios elementos. Uno, el narcotráfico que fue introducido para comprometer a la población en la siembra. Después se introdujo el paramilitarismo en las zonas de cultivo y de ahí mismo se financió. Tercero, el diseño militar: hubo una gran reestructuración que terminó de hacerse en 2003 y sigue. Luego viene el modelo neoliberal y dentro de ese contexto la lógica norteamericana.

**Ahora bien, ¿qué implica hacer este anuncio en Venezuela, teniendo en cuenta la situación de guerra económica?**

**P.B.:** Es Venezuela, aun en medio de sus dificultades políticas internas, la que ha promovido el proceso de paz en Colombia. Gracias al presidente Chávez, las FARC iniciaron un proceso de diálogo con Álvaro Uribe que hoy sigue en La Habana. También gracias a él empezamos diálogos exploratorios con Santos, en la sede de la cancillería en Caracas.

**En América Latina está cambiando el panorama. Vuelve la derecha a Argentina, difícil la situación en Bolivia y Brasil, complicada en Ecuador ¿Qué decirle a la izquierda del continente que vive esa regulación en su proyecto?**

**P.B.:** Hoy Estados Unidos da un pie atrás en el norte de África y Medio Oriente, lo que implica que van a cuidar más su ‘patio trasero’. Hace poco, Obama en su visita en Argentina afirmó: “vengo a apoyar esta transición”.

¿A qué se refería Obama? A revertir todas las medidas progresistas y democráticas, en eso están concentrado. El capital financiero  no considera que sus ganancias en Brasil sean suficientes y si el gobierno de Dilma sigue en función de regularlo, van a insistir en el golpe, van a conspirar para derrocarlo. La transición de Obama es revertir los avances de la última década.

Hasta el momento el país espera conocer cómo será el desarrollo concreto de los ciclos de diálogo pero, sobre todo, cuáles serán los caminos para que la sociedad pueda participar de las transformaciones necesarias para llegar a lo que los negociadores entrevistados llamaron “un nuevo consenso nacional”.
