Title: Amnistía acusa al gobierno de Siria de ataques indiscriminados contra civiles
Date: 2015-05-06 15:07
Author: CtgAdm
Category: DDHH, El mundo
Tags: Alepo, Amnistía Internacional, Amnistía Internacional acusa Al-Asad de ataques contra civiles, Bashar El Asad, Estados Unidos, Observatorio Sirio de DDHH, Siria
Slug: amnistia-acusa-al-gobierno-de-siria-de-ataques-indiscriminados-contra-civiles
Status: published

###### Foto:EcuaVisa.com 

###### **Entrevista con [Leila Nachawati], activista hispanosiria:** 

<iframe src="http://www.ivoox.com/player_ek_4491324_2_1.html?data=lZmmk5iWeI6ZmKiak5aJd6Kkl5KSmaiRdo6ZmKiakpKJe6ShkZKSmaiRjc%2Fa0Nfax5DIqYy1ztPW1dmJh5SZoqnOja7SuMbmz8bQy9TSpc2f1NTP1MqPp9OZpJiSo6nRqc%2FZ1JDRx5DLcYarpJKw0dPYpcjd0JC%2Fw8nNs4yhhpywj5k%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

La ONG **Amnistía Internacional** ha realizado un informe donde acusa al **régimen de Bashar El Asad** de haber cometido **crímenes de guerra contra la población de Alepo** y de otras ciudades del país desde que comenzó el conflicto armado en 2011.

El informe apunta hacia los bombardeos indiscriminados del ejército sirio contra población en Alepo, donde se lanzaban **barriles de explosivos contra escuelas**, universidades y hospitales, siendo estos objetivos civiles.

Además el informe detalla **ejecuciones extrajudiciales, torturas y persecución contra rebeldes y contra población civil indiscriminadamente**. La ONG acusa a la Comunidad Internacional de no hacer nada al respecto y al régimen Sirio de no permitir a las ONG'S entrar en zonas donde existen emergencias humanitarias.

Según [[**Philip Luther** director de AI para Oriente Medio "...Estos reprobables y continuos ataques en zonas [[residenciales]{.hps} [apuntan a]{.hps} [una política de]{.hps} [focalización]{.hps} [deliberada y sistemática]{.hps} en [los civiles.]{.hps} Estos [constituyen]{.hps} **[crímenes de guerra y]{.hps} [crímenes contra la humanidad...]{.hps}[".]{.hps .atn}**]{#result_box lang="es" lang="es"}]{.hps}]{#result_box lang="es" lang="es"}

AI en el informe reporta que en el último año han muerto **3.000 personas** a causa de los **bombardeos aéreos y 220.000 desde que comenzó la contienda en 2011**. También acusa a los rebeldes de atacar a la población civil dejando un saldo de 612 muertos.

Para el **Observatorio Sirio de DDHH**, los ataques provocados desde la aviación Siria han sido 11.017 y  en ellos han fallecido [[2.314 personas, desde Enero hasta Abril de 2015.]{.hps}]{lang="es" lang="es"}** 1.606 eran civiles, de los cuales 369 eran niños, y 255 mujeres**. Los bombardeos también han dejado un saldo de 13.000 heridos.

**Al-Asad** se ha **defendido** de las acusaciones mediante una entrevista en una cadena televisiva francesa, alegando que las **bombas de la aviación son de precisión** y por tanto es imposible el bombardeo indiscriminado.
