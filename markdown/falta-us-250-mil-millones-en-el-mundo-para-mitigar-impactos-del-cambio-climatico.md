Title: Hay un déficit mundial de US$ 250 mil millones para mitigar impactos del cambio climático
Date: 2015-08-26 17:39
Category: Ambiente, El mundo, Política
Tags: Ambiente y Sociedad, Calentamiento global, cambio climatico, DNP, Fondo Verde de Cambio climático, Milena Bernal
Slug: falta-us-250-mil-millones-en-el-mundo-para-mitigar-impactos-del-cambio-climatico
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/08/CAUSAS_DEL_CAMBIO_CLIMÁTICO.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: [www.minambiente.gov.co]

<iframe src="http://www.ivoox.com/player_ek_7620013_2_1.html?data=mJufkpWVd46ZmKiak5eJd6Knk5KSmaiRdo6ZmKiakpKJe6ShkZKSmaiRmbSZk5mYlJqUb87dzZDay9HQs8%2FZ1JDS1ZDJsIzYhqigh6adqsrXytmYz9rSqMrVzZDdw9fFb8bix9fS0JKJe6ShpNTb1sbLrdCfs8bRy9SPcYarpJKh&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Milena Bernal, Ambiente y Sociedad] 

###### [26 Ago 2015]

Aunque el Fondo Verde de Climático piensa reunir 100 mil millones de dólares para enfrentar los impactos del calentamiento global, los países más pobres son los que necesitarán mayor inversión, por lo que se ha estimado que e**l mundo realmente necesitaría 350 mil millones de dólares para mitigar, adaptarse y reducir los riesgos** que se generan por el aumento de la temperatura a nivel global, lo que quiere decir que hay un déficit de 250 mil millones de dólares.

Respecto al caso colombiano la situación no mejora, ya que esta semana el Departamento Nacional de Planeación, DNP, aseguró que si no se implementan medidas para lograr una adaptación al cambio climático, **el país perdería aproximadamente 0,5% del PIB desde el año 2011 al 2100, es decir la pérdida de 3,8 billones de pesos por año.**

De acuerdo a la información del DNP, basada en el “Estudio de Impactos Económicos del Cambio Climático”, el sector más afectado económicamente sería **el agropecuario que perdería 504.000 millones de pesos al año.** Así mismo se verían afectados los sectores de la pesca, el transporte y la ganadería.

Según Milena Bernal, abogada de la Asociación de Ambiente y Sociedad, el cambio climático afecta todos los sectores económicos de la sociedad colombiana, es por eso que el Plan Nacional de Desarrollo y las demás medidas deberían incluir “acceso a la información clara, debe haber fortalecimiento institucional, transparencia en la inversión del dinero para la mitigación y adaptación al cambio climático, y el desarrollo de una política nacional de bosques que integre actores de reducción de riesgos frente al aumento de la temperatura”, según explicó Bernal.

Finalmente, la abogada asegura que **“la forma como el país le apunta al desarrollo, no concuerda con los planes para mitigar el cambio climático”**, refiriéndose a que la economía colombiana se basa en el extractivismo.
