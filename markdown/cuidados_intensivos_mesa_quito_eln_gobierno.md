Title: En el limbo mesa de conversaciones entre Gobierno y ELN
Date: 2018-01-10 14:46
Category: Nacional, Paz
Tags: ELN, Juan Manuel Santos, Mesa en Quito, terminación del cese al fuego, Víctor de Currea
Slug: cuidados_intensivos_mesa_quito_eln_gobierno
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/02/quito.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: La Prensa] 

###### [10 Ene 2017] 

Una nueva dificultad atraviesa la mesa de conversaciones en Quito, entre el gobierno y el ELN. Esta vez, tras el término del Cese Bilateral y una serie acciones en diferentes puntos del país, atribuidos por las autoridades a esa guerrilla, el presidente Juan Manuel le ordenó a Gustavo Bell, jefe negociador, junto a todo su equipo, abandonar Quito.

Según el mandatario “**El ELN no solo se negó, sino que reanudó sus ataques terroristas esta madrugad**a, justo el día que debía iniciarse el nuevo ciclo. He conversado con el jefe de la delegación del gobierno (Gustavo Bell) para que se regrese de inmediato a Colombia para evaluar el futuro del proceso”, indicó en una alocución de este miércoles.

De acuerdo con la autoridades, tras la terminación del cese al fuego bilateral, esa guerrilla ha llevado a cabo ataques en cercanías en Aguazul Casanare; Benadía, Arauca, en la vereda Cañaguata municipio de Cubará en Boyacá, y además hizo el lanzamiento de una granada contra infantes de marina en la vía entre Arauquita y Saravena. (Le puede interesar: [Balón del cese al fuego está en manos del ELN)](https://archivo.contagioradio.com/cese-al-fuego-bilateral-eln/)

### **Comunidad nacional e internacional insisten en prórroga del cese al fuego** 

Las organizaciones sociales y las comunidades, piden que nuevamente se pacte una etapa de cese al fuego, como palanca para avanzar en las negociaciones del proceso de paz. Señalan que “la salida negociada del conflicto armado es una prioridad” y reconocen que los esfuerzos de las partes en la mesa de Quito ha beneficiado a las comunidades en las diferentes regiones del país.

Mientras tanto, Santos **ordenó a las fuerzas militares “actuar con contundencia para responder a esta agresión **y proteger la vida y honra de los colombianos como es su deber constitucional”.

Además, ante la reciente decisión del Gobierno Nacional, la guerrilla del ELN emitió un comunicado  afirmando que ellos cumplieron el cese. Y señalaron que "los incidentes sucedidos el día de hoy en el oriente colombiano, ocurren en medio de la compleja situación de conflicto que sufre el país; pero pese a ellos, **no debe alterarse el curso de las conversaciones, para lograr una salida política del conflicto"**.

En ese sentido también señalan que se mantienen en la decisión expresada de pactar una nueva tregua que se supere las dificultades del anterior. Asimismo piden que se mantenga el apoyo tanto de la comunidad internacional como de la sociedad civil.  (Le puede interesar: [Así transcurrieron 4 meses de cese al fuego)](https://archivo.contagioradio.com/asi-transcurrieron-los-4-meses-del-cese-bilateral-al-fuego-entre-el-eln-y-el-gobierno/)

### **"ELN no debió saltar a la guerra"** 

El analista Víctor de Currea, había asegurado que el balón del cese bilaterial estaba en manos del ELN dado que el presidente Santos se manifestó en el sentido de renegociar algunas de las condiciones que el ELN valora como negativas del cese actual.

De hecho en su reciente columna sobre la terminación del cese en la Revista Semana, dice el analista que, "Un cese de tan solo 100 días no mide mucho, casi solo la voluntad de las partes. **El ELN tiene toda la razón al rechazar el actual y plantear modificaciones, pero debería hacerlo en medio del cese y no saltar a la guerra**, creyendo que luego negociará uno mejor. Y así lo logre, el costo de saltar a la guerra es innecesario. Es más, el gobierno de Santos les cogió la caña y propuso alargarlo y renegociarlo ¿para qué entonces saltar a la guerra donde, como es sabido, los platos rotos los pagan las comunidades?"

Finalmente, asegura que "Los elenos están creciendo en las regiones, por las torpezas del gobierno (...) pero lo que ganan de manera puntual en las regiones, lo pierden en el escenario nacional", y agrega que el regreso de la Delegación del gobierno al país, de manera intempestiva, es un mal presagio. **"La respuesta que se prevé de las Fuerzas Militares y el escepticismo en la sociedad, aumentan el pesimismo.** El proceso no ha muerto, pero entra a cuidados intensivos".

###### Reciba toda la información de Contagio Radio en [[su correo]
