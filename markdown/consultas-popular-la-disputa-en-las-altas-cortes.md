Title: La discusión sobre las consultas populares en la Corte Constitucional
Date: 2018-04-13 19:26
Category: Ambiente, Nacional
Tags: consultas populares, Corte Constitucional
Slug: consultas-popular-la-disputa-en-las-altas-cortes
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/Consulta-popular-ya.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Consulta popular Ibague] 

###### [13 Abr 2018]

El Consejo de Estado ratificó la competencia que **tienen las autoridades locales sobre el suelos y el subsuelo, un hecho que podría ayudar a destrabar** el debate sobre las consultas populares y los diferentes frenos que se les han puesto. El fallo es producto de una tutela interpuesta por el Ministerio de Minas, contra la consulta popular realizada en el municipio de Jesús María, Santander.

Según el abogado y ambientalista Rodrigo Negrete, esta decisión desde el Consejo de Estado, que ya había sido reiterada en diversas ocasiones, ratifica que "sí hay la competencia y **el mandato constitucional en cabeza de los municipios, que permite hacer consultas populares**".

Sin embargo, nuevos retos deberá afrontar el mecanismo de consulta popular en Colombia, uno de ellos, de acuerdo con el abogado, es que hay tres magistrados de la Corte Constitucional que deben declararse impedidos en este debate. El primero es Alejandro Linares, presidente de la Corte Constitucional, que ya se había pronunciado en contra de las consultas **populares y afirmando que la nueva corte limitaría este mecanismo al punto que se merece**.

Negrete aseguro, que la magistrada Cristina Pardo, también hace parte de esta triada, porque abría aprobado decretos que limitaban las facultades de los municipios sobre minería, cuando ejerció como secretaria jurídica de la presidencia.

Finalmente se encuentra la magistrada Diana Fajardo, que hizo un pronunciamiento sobre las consultas cuando se desempeño como subdirectora en la Agencia Nacional de Defensa Jurídica del Estado, en contra de la consulta popular de Cumaral.

### **El Primer round por las consultas populares**

Referente al primer encuentro en la audiencia citada por la Corte Constitucional sobre los límites de las Consultas Populares, Negrete señaló que preocupan varias posiciones. Una de ellas es la de el gobierno que **"defendiendo a ultranza la actividad extractiva". **(Le puede interesar: ["Comunidades exigen a la Corte Constitucional no modificar Consultas Populares"](https://archivo.contagioradio.com/se-alista-gran-movilizacion-en-defensa-de-las-consultas-populares/))

Durante la audiencia, habitantes de cada uno de los municipios en donde se han realizado consultas populares o en donde se han frenado estos procesos, hicieron parte de un plantón. Además, más de 74 organzaciones internacionales firmaron un comunicado en donde le piden a la Corte que no modifique el alcance de las consultas populares.

###### Reciba toda la información de Contagio Radio en [[su correo]
