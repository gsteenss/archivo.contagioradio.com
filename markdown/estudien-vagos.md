Title: Estudien, vagos
Date: 2017-04-18 11:39
Category: Javier Ruiz, Opinion
Tags: Centro Democrático, Día de las dignidad de las víctimas, María Fernanda Cabal
Slug: estudien-vagos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/04/Maria-Fernanda-Cabal.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: YouTube 

#### Por: **[Javier Ruiz](https://archivo.contagioradio.com/javier-ruiz/) - [@contragodarria](https://twitter.com/ContraGodarria)** 

###### 18 Abr 2017 

**El pasado 9 de abril en el Congreso de la República** sucedió un incidente en el cual el uribismo estaba disgustado porque supuestamente no tuvieron garantías en la sesión especial que era dedicada para que las víctimas de la violencia hablaran y fueran escuchadas. No era una sesión ordinaria del Congreso, y Uribe disgustado se marcha con su séquito porque no le dejaron hacer una réplica para contestar por las duras réplicas de las víctimas a su gobierno.

Lo gracioso, y a la vez lo grotesco, fue el show que intentó hacer Uribe en la Plaza de Bolívar. A la salida la gente le protestó por su cinismo y por su populismo y al ver que no convencieron en su show se devuelven no sin antes insultar a quienes le reclamaban y de ahí que la Representante a la Cámara **María Fernanda Cabal** lanzara el típico insulto clasista y arribista de la ultraderecha: Estudien, vagos. Después el senador **Alfredo Ramos Maya, del Centro Democrático**, cuestionado por estar involucrado en el escándalo de los Panamá Papers, grita a quienes protestaban que “**Lean…Lean…Lean… Aprendan de historia**”.

Si uno se pone a pensar el congresista de Colombia es vago por antonomasia y si es congresista del Centro Democrático es el doble de vago por antonomasia. Y precisamente es que el uribismo no tiene argumentos y recurre a las mentiras, las desinformaciones, a las posverdades (el concepto de moda) y a la furia para movilizar a sus adeptos, como lo hicieron el primero de abril en una marcha que no era nada más que una medición de fuerza de la ultraderecha para las elecciones del 2018.

**Si el uribista no fuera vago y aprendiera de historia seria más comprensivo** sobre los orígenes del conflicto interno, no se hubiera dejado engañar tan fácil para que votara “NO” ese fatídico 2 de octubre de 2016. Si el uribista no fuera un vago hubiera leído los acuerdos y no se hubiera dejado engañar con las mentiras de “la ideología de género”, ni con el cuento de que nos íbamos a volver Venezuela o que habría impunidad entre otras.

Si el uribista no fuera vago, leyera y aprendiera de historia sabría que Uribe negó el conflicto armado, que compró su reelección como lo confesó su secuaz Diego Palacio y que para dar la sensación de que “íbamos ganado la guerra” ordenó matar jóvenes inocentes para hacerlos pasar como guerrilleros dados de baja en combates.

Si el uribista no fuera vago y leyera no se inventaría atentados terroristas o se aprovecharían de tragedias catastróficas como la sucedida en Mocoa para decir que fue culpa de las FARC como lo dijo el torpe senador Daniel Cabrales quien es un vago cuyo mayor aporte legislativo ha sido la de sostener el maletín del expresidente Uribe.

Si el uribista no fuera vago nunca votaría por el Centro Democrático porque esos representantes y senadores no representan al pueblo, que no aportan nada legislativamente al país, no piensan por si mismos, solo obedecen los dictados del líder Uribe y cuando son cuestionados por ser malos congresistas solo se ponen a gritar y a insultar. A los vagos uribistas toca recordarles que sus congresistas no están porque les votaran sino porque entraron de arrastre al Congreso gracias a Uribe, porque sin él nadie les hubiera votado.

Podemos seguir nombrando acontecimientos donde quedan mal parados los vagos uribistas porque si son conscientes verían la situación desde una óptica diferente y no una impuesta por las mentiras de Uribe que ahora se encuentra mintiendo y arrodillándose ante Trump.

Solo les diré a los uribistas que: **Estudien, vagos… Tienen un año para revertir la situación o seguir en la vagancia que ha puesto el país muy mal. **

#### [**Leer más columnas de Javier Ruiz**](https://archivo.contagioradio.com/javier-ruiz/)

------------------------------------------------------------------------

###### Las opiniones expresadas en este artículo son de exclusiva responsabilidad del autor y no necesariamente representan la opinión de Contagio Radio.
