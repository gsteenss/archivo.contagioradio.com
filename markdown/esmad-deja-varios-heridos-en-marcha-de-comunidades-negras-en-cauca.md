Title: ESMAD deja varios heridos en marcha de comunidades negras en Cauca
Date: 2016-04-25 18:35
Category: DDHH
Tags: Cauca, comunidades negras, ESMAD
Slug: esmad-deja-varios-heridos-en-marcha-de-comunidades-negras-en-cauca
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/04/ANCOC-contagioradio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: ANCOC] 

###### [25 Abril 2016] 

Por ANCOC \#Yo Reporto

La Asociación de Consejos Comunitarios del Norte del Cauca (ANCOC) denuncia agresión policial, un herido por arma de fuego y seis personas más lesionadas por artefactos explosivos que dispara el ESMAD contra la población civil.

A esta hora unas 2000 personas de comunidades Afrocolombianas del norte del Cauca se movilizan pacíficamente en el norte del departamento y denuncian una fuerte arremetidita y agresión policial en respuesta a la exigencia de las comunidades al gobierno por el incumplimiento de algunos acuerdos pactados como el Acuerdo de la Salvajina por parte del ya extinto INCODER que se había comprometido entre otros a entregar territorios a estas comunidades. Diversos líderes de la región denuncian la brutalidad del ESMAD que dispara gases y bala contra las comunidades.

La denuncia fue hecha por líderes afro y un comunicado (adjunto) de la Asociación de Concejos Comunitarios del Norte del Cauca (ACONC), quienes denuncian violación y vulneración a sus derechos y un trato de represión por parte del ESMAD a sus justas exigencias. Al incumplimiento por parte del INCODER (entidad en liquidación) denuncian que hay un desconocimiento por parte del gobierno nacional que niega la existencia de población afro en el departamento del Cauca para no hacer la consulta previa por la implementación de la doble calzada en la vía que del norte del departamento conduce a Popayán, denuncian daños ambientales a sus territorios y una situación de vulneración y precarias condiciones de las comunidades que se dedican a la minería artesanal; señalan que las comunidades se cansaron de los engaños y mentiras del gobierno que no atiende sus peticiones ni garantiza el ejercicio pleno de sus derechos y su autonomía.

Según las fuentes de las comunidades Afro, la Policía habría disparado contra la población civil una vez se movilizaban por el centro histórico de Quinamayo en Santander de Quilichao al norte del departamento, donde permanecerán hasta que haya respuesta del gobierno y anunciaron que para  el día de mañana recibirán el apoyo de las comunidades indígenas y campesinas de la región que se sumaran a la movilización y acompañara a los más de 2000 personas de comunidades afro.
