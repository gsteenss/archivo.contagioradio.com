Title: Gobierno Colombiano sigue diciendo no a la protección de líderes Ambientales
Date: 2019-10-02 14:46
Author: CtgAdm
Category: Ambiente
Tags: Acuerdo de Escazú, defensa de ecosistema, Líderes ambientales, Ministerios de Relaciones
Slug: gobierno-niega-acuerdo-escazu
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/06/Marcha-por-el-agua-e1464906866438.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo] 

La Cancillería de **Colombia afirmó el pasado 30 de septiembre que no firmará el Acuerdo de Escazú**, argumentando que, *“existen diversos instrumentos nacionales e internacionales que ya vinculan al país en esta materia y cuyo alcance ha sido definido por la jurisprudencia constitucional por lo que no se considera necesario suscribir el acuerdo en este momento".* La noticia es un golpe para quienes han exigido mayor compromiso de Colombia hacia la protección de líderes ambientales, ya que de los 164 defensores ambientales asesinados en el último año en el mundo, 24 eran colombianos.

### ¿Por qué  es necesario el Acuerdo de Escazú ?

Según la Asociación Ambiente y Sociedad, en el ultimo año en Colombia han sido asesinados mas de 10 líderes ambientales, esta cifra refleja el panorama al que se enfrentan las y los defensores ambientales en Colombia; adicionalmente, según la ONU (Organización de las Naciones Unidas), Colombia es el segundo país en América Latina con mayor ataque a líderes sociales, una situación que ha venido escalando desde la firma de los Acuerdos de Paz.

A estos hechos se suman las denuncias que han realizado organizaciones defensoras del ambiente debido a la continuación y aumento de  explotaciones mineras, cultivos masivos con agroquímicos, aumento de especies en vía de extinción, exploraciones ilegales en territorios protegidos, entre otros; que aumentan los daños al territorio e impiden frenar el avance de la crisis climática. En ese sentido,  la Asociación se pregunta, si las  normas que contempla el Ministerio para la protección del ecosistema y sus defensores son suficientes y pertinentes.

### ¿Qué es el Acuerdo de Escazú?

El acuerdo nace el  4 de marzo de 2018, con el aval de 24 Estados de América Latina y el Caribe, incluyendo a Colombia, que acogieron el *Acuerdo Regional sobre Acceso a la Información, Participación y Justicia en Asuntos Ambientales para América latina y el Caribe;* que posteriormente fue llamado Acuerdo de Escazú, en relación a la provincia de Costa Rica donde se aprobó.

El mismo fue firmado por 14 países el 27 de septiembre de 2018 en el marco de la reunión anual de la Asamblea General de las Naciones Unidas, y contempla protocolos  para la protección del ambiente, al igual que los  defensores de estos territorio. (Le puede interesar:[Acuerdo de Escazú una herramienta para proteger la vida de líderes ambientales en Colombia](https://archivo.contagioradio.com/acuerdo-de-escazu-una-herramienta-para-proteger-la-vida-de-lideres-ambientales-en-colombia/))

<div class="flex">

> <div class="quote" style="text-align: right;">
>
> *Garantizar la implementación plena y efectiva en América Latina y el Caribe de los derechos de acceso a la información ambiental, participación pública en los procesos de toma de decisiones ambientales y acceso a la justicia en asuntos ambientales, así como la creación y el fortalecimiento de las capacidades y la cooperación, contribuyendo a la protección del derecho de cada persona, de las generaciones presentes y futuras, a vivir en un medio ambiente sano y al desarrollo sostenible.*
>
> </div>

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]

</div>
