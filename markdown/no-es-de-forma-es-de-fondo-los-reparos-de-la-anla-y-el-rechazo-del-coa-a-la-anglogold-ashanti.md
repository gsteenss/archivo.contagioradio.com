Title: ¡No es de forma, es de fondo!: Los reparos de la ANLA y el rechazo del COA a la AngloGold Ashanti
Date: 2019-12-17 12:25
Author: CtgAdm
Category: Opinion
Tags: ANLA, Comunidad, Licencia Ambiental, Mineria
Slug: no-es-de-forma-es-de-fondo-los-reparos-de-la-anla-y-el-rechazo-del-coa-a-la-anglogold-ashanti
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/Mapa-Sueño-EOT-e1576197964936-670x300-1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### 

###### [Foto de: COA] 

Después de las elecciones realizadas el **27 de octubre** del presente año, la **empresa sudafricana AngloGold Ashanti** inició una fuerte campaña mediática en diferentes medios de comunicación nacional, intentando mejorar la cuestionada imagen de un proyecto que ha carecido de legitimidad, caracterizándose por ser un modelo impositivo que ha vulnerado  -desde hace más de 10 años-  el derecho a la participación efectiva de la comunidad, agudizando la división social, interfiriendo en decisiones municipales, militarizando el territorio y estigmatizando al movimiento social.

Mediante su estrategia mediática ha intentado demostrar las *“buenas prácticas mineras”* bajo su política de *“responsabilidad social empresarial”*, intentando ocultar la fuerte oposición que lleva más de diez años y que se ha expresado de diversas formas en el suroeste de Antioquia, como han sido las vigilias por la defensa del territorio, la travesía por el suroeste de Antioquia:

Un abrazo a la montaña, los encuentros regionales de economías indígenas y campesinas, los encuentros regionales de mujeres y de jóvenes, los cabildos abiertos, los Consejos de Concejales, acuerdos municipales, iniciativas de consulta popular, entre otras acciones más.

Esta estrategia mediática intensiva le apuesta a mejorar la imagen corporativa en su proceso de licenciamiento social y ambiental. El pasado **29 de noviembre Minera Quebradona radicó ante la Agencia Nacional de Licenciamiento Ambiental (ANLA)**, el estudio de impacto ambiental **(EIA)**.

#### Para el 10 de diciembre  la ANLA le devuelve la solicitud a la empresa minera manifestando tres reparos: 

1\. No presentó información completa para los permisos de captación de aguas superficiales,vertimientos, ocupación de cauces, aprovechamiento de materiales de construcción, aprovechamiento forestal, entre otros.  
2. La geodatabase no está debidamente estructurada.  
3. Faltó certificado actualizado expedido por el ministerio del interior de la presencia o ausencia de comunidades étnicas o territorios colectivos en el área de influencia. Consecuentemente un vocero de la empresa emite concepto, en el periódico El Colombiano, manifestando que esta devolución es de forma y no de fondo.

###### ver mas: [Anglo Gold Ashanti pretende bloquear investigación sobre violencia en territorios en los que hace presencia](https://archivo.contagioradio.com/anglo-gold-ashanti-bloquear-investigacion/)

Sin embargo, desde el **Cinturón Occidental Ambiental –COA**- queremos reiterar que más allá del concepto de la ANLA, persiste un problema de fondo y no de forma. El proyecto Quebradona de AngloGold Ashanti ha sido un modelo que nunca fue construido con la gente, ni ha sido aceptado por el suroeste de Antioquia, tampoco es producto de las necesidades nuestras, ha dividido comunidades e interfiriere persistentemente en la posibilidad de continuar la construcción de proyectos de vida propios.

Durante estos 10 años de movilización permanente contra el proyecto minero, desde los rincones de las montañas y ríos, continuamos fortaleciendo y articulando proyectos de vida que son producto del sentihabitar de los pueblos indígenas y campesinos, de mujeres, hombres, niñas, niños, jóvenes y abuelos en la edificación del Territorio que nos soñamos.

Como el conflicto es de fondo y no de forma, nos oponemos al proyecto Quebradona porque en estas majestuosas montañas le apostamos a la construcción de Territorios Sagrados para la Vida, propuesta que nace de la articulación regional y que se erige a partir de la tradición campesina y la cultura Embera, desde prácticas productivas y culturales de la economía familiar campesina, los sistemas agroecológicos, los circuitos económicos solidarios, los trapiches comunitarios, la gestión comunitaria del agua, los sitios sagrados, las zonas de protección y el patrimonio cultural y ambiental.

Como nuestras luchas son de fondo y no de forma, continuamos conquistando espacios para consolidar los procesos de participación social efectiva y afectiva, que trasciende hasta más allá del infinito, que no se limita a las listas de asistencia. Este proceso se estructura desde los cabildos comunitarios, consultas autónomas, mandatos populares, círculos de mujeres, audiencias autónomas, planes de vida comunitarios, entre otros procesos más.

Le puede interesar:[Los lunares de AngloGold Ashanti](https://www.las2orillas.co/los-lunares-de-anglogold-ashanti/)

#### El movimiento social se fortalece cada vez más 

Continuaremos defendiendo nuestros mandatos populares, entre ellos, su declaratoria como actores no gratos para el suroeste de Antioquia. Desde las escuelas agroecológicas, las escuelas de sustentabilidad, la polinización del territorio, las escuelas de arte y cultura, desde la pedagogía de la madre tierra, las fincas escuelas, las cátedras del territorio, las casasmadre del territorio, las salas patrimoniales, desde los medios de comunicación comunitarios y alternativos, desde las mesas de concertación social: plan de vida comunitario... continuaremos nuestras resistencias, ratificando permanentemente que estamos construyendo nuestros planes de vida comunitarios y autónomos.

NO PERMITIREMOS QUE NOS ARREBATEN LA POSIBILIDAD DE DECIDIR CÓMO QUEREMOS VIVIR.

**LARGA ES NUESTRA LUCHA Y CADA VEZ SOMOS MÁS**
