Title: Aumentaron moscas y malos olores en zonas aledañas al basurero de Doña Juana
Date: 2017-10-04 16:25
Category: Movilización, Nacional
Tags: Paro Sur Bogotá, Relleno sanitario Doña Juana
Slug: aumentaron-moscas-y-malos-olores-en-zonas-aledanas-al-basurero-de-dona-juana
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/10/DHNq2J2W0AAAkkI.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Inti Asprilla] 

###### [04 Oct 2017] 

A una semana de que se haya iniciado el paro del Sur – Tunjuelo, diferentes actividades se han llevado a cabo para continuar en la exigencia de garantías para la vida digna en los barrios del Sur de Bogotá y denunciaron que la operadora CGR del Relleno no está haciendo un manejo responsable y por ello **han aumentado los vectores como moscas y malos olores en los alrededores.**

“Tenemos la sospecha que esta vez la pésima operación del basurero es una cuenta de cobro que le está pasando el CGR y el gobierno, **en tanto que las moscas, los olores y las ratas se dispararon, peor que hace un mes”** afirmó Oscar Barón, uno de los líderes. (Le puede interesar: ["Distrito ampliaría hasta el 2070 la existencia del Relleno Sanitario Doña Juana"](https://archivo.contagioradio.com/distrito-ampliaria-hasta-2070-la-existencia-del-relleno-sanitario-dona-juana/))

De igual forma, el vocero señaló que, pese a que se han llamado a las diferentes autoridades para que resuelvan prontamente esta problemática, ninguna institución se ha pronunciado frente a esta situación, a su vez las grietas que estarían dentro del relleno **también seguirían creciendo y serían las que provocan el aumento en los malos olores y las plagas**.

### **El paro del Sur continúa** 

El próximo 8 de octubre se realizará una movilización en Usme, convocada por los campesinos que habitan en la ruralidad de Ciudad Bolívar, Sumapaz y esta localidad, para exigir que se frene la contaminación del Relleno Sanitario Doña Juana. (Le puede interesaar: ["Suacha se une al paro del Sur de Bogotá"](https://archivo.contagioradio.com/47337/))

El vocero del paro esta movilización se ha caracterizado por tener expresiones más artísticas y pacíficas de indignación que **han promovido el debate desde las universidades hasta los hogares** de quienes son víctimas de las moscas producto del Basurero Doña Juana o de la falta de vías en las periferias de la ciudad.

Sobre la movilización del próximo domingo Barón señaló que “el punto central es la defensa del Páramos de Sumapaz y las denuncias de los riesgos que ciernen sobre este lugar” además, agregó que las organizaciones sociales siguen en jornadas permanentes de debate y programando más actividades.

<iframe id="audio_21274091" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_21274091_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
