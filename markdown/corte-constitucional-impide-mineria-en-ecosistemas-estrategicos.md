Title: Corte Constitucional impide minería en ecosistemas estratégicos
Date: 2016-06-13 16:05
Category: Ambiente, Nacional
Tags: Áreas Estratégicas Mineras, concesiones mineras, Corte Constitucional Colombiana, plan de desarrollo Colombia
Slug: corte-constitucional-impide-mineria-en-ecosistemas-estrategicos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/06/Páramo-Macizo-Colombiano.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Tierra de Vientos ] 

###### [13 Junio 2016 ]

516 bloques que habían sido declarados por el Ministerio de Minas y Energía como Áreas Estratégicas Mineras en 20 departamentos del país, quedaron sin efectos jurídicos por cuenta de la más reciente decisión de la Corte Constitucional, en la que se ratificó que **esta declaratoria no contó con la consulta de las comunidades, ni autoridades locales, ni con los estudios de impacto ambiental**.

De acuerdo con la abogada Johana Rocha, del 'Centro de Estudios para la Justicia Social Tierra Digna', **más de 20 millones de hectáreas en la amazonía, en el Chocó biogeográfico y en el macizo colombiano**, fueron declaradas como áreas mineras sin haber consultado a las comunidades, y sin que hayan habido estudios que mostraran los impactos ambientales de la actividad minera en las regiones.

Si bien esta decisión tiene implicaciones para las próximas titulaciones que se otorguen, la abogada insiste en que para las que ya existen se deben aplicar estándares constitucionales que determinen **si está en peligro la garantía de derechos fundamentales, y en tal caso estas licencias podrían perder su valor jurídico**.

Esta decisión de la Corte, ratifica que las comunidades deben ser consultadas para las titulaciones mineras y que éstas deben fundamentarse en estudios que argumenten si son o no positivas para la vida de las poblaciones y para las especies vegetales y animales que están en los territorios, según afirma la abogada.

Estamos ante la oportunidad de que las autoridades integren todas las resoluciones que la Corte ha determinado en materia minera y de que se establezca un ordenamiento minero que no sea unilateral y encuentre **mecanismos de participación para la superación de conflictos medioambientales**, para que se encamine el desarrollo dentro del marco jurídico establecido por la Constitución Política.

<iframe src="http://co.ivoox.com/es/player_ej_11886146_2_1.html?data=kpalmpuVeJehhpywj5aWaZS1lpeah5yncZOhhpywj5WRaZi3jpWah5yncavjycbbw5C2s8TcwpCajabGs8jVxcaah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ..&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)]] 
