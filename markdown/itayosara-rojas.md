Title: Itayosara Rojas
Date: 2015-01-22 19:48
Author: AdminContagio
Slug: itayosara-rojas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/02/Itayosara-Rojas-Herrera.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

### ITAYOSARA

[![El Río Cauca en estado de emergencia tras operación de Hidroituango](https://archivo.contagioradio.com/wp-content/uploads/2019/01/Captura-de-pantalla-6.png "El Río Cauca en estado de emergencia tras operación de Hidroituango"){width="741" height="504" sizes="(max-width: 741px) 100vw, 741px" srcset="https://archivo.contagioradio.com/wp-content/uploads/2019/01/Captura-de-pantalla-6.png 741w, https://archivo.contagioradio.com/wp-content/uploads/2019/01/Captura-de-pantalla-6-300x204.png 300w, https://archivo.contagioradio.com/wp-content/uploads/2019/01/Captura-de-pantalla-6-370x252.png 370w"}](https://archivo.contagioradio.com/el-rio-cauca-en-emergencia-ambiental-por-sequias/)  

###### [El Río Cauca en estado de emergencia tras operación de Hidroituango](https://archivo.contagioradio.com/el-rio-cauca-en-emergencia-ambiental-por-sequias/)

[<time datetime="2019-01-30T16:06:56+00:00" title="2019-01-30T16:06:56+00:00">enero 30, 2019</time>](https://archivo.contagioradio.com/2019/01/30/)Habitantes de Caucasia, Antioquia expresaron alarma sobre los bajos niveles de agua en el Río Cauca.[LEER MÁS](https://archivo.contagioradio.com/el-rio-cauca-en-emergencia-ambiental-por-sequias/)  
[![Panfleto del Quintín Lame ¿una estrategia para justificar asesinatos de líderes?](https://archivo.contagioradio.com/wp-content/uploads/2019/01/NASA1.jpg "Panfleto del Quintín Lame ¿una estrategia para justificar asesinatos de líderes?"){width="700" height="500" sizes="(max-width: 700px) 100vw, 700px" srcset="https://archivo.contagioradio.com/wp-content/uploads/2019/01/NASA1.jpg 700w, https://archivo.contagioradio.com/wp-content/uploads/2019/01/NASA1-300x214.jpg 300w, https://archivo.contagioradio.com/wp-content/uploads/2019/01/NASA1-275x195.jpg 275w, https://archivo.contagioradio.com/wp-content/uploads/2019/01/NASA1-370x264.jpg 370w, https://archivo.contagioradio.com/wp-content/uploads/2019/01/NASA1-110x78.jpg 110w, https://archivo.contagioradio.com/wp-content/uploads/2019/01/NASA1-240x170.jpg 240w"}](https://archivo.contagioradio.com/panfleto-del-quintin-lame-una-estrategia-para-justificar-asesinatos-de-lideres/)  

###### [Panfleto del Quintín Lame ¿una estrategia para justificar asesinatos de líderes?](https://archivo.contagioradio.com/panfleto-del-quintin-lame-una-estrategia-para-justificar-asesinatos-de-lideres/)

[<time datetime="2019-01-30T14:21:50+00:00" title="2019-01-30T14:21:50+00:00">enero 30, 2019</time>](https://archivo.contagioradio.com/2019/01/30/)Integrantes de la comunidad Nasa rechazaron los panfletos firmados por un grupo identificado como Quintin lame y abogaron por la paz[LEER MÁS](https://archivo.contagioradio.com/panfleto-del-quintin-lame-una-estrategia-para-justificar-asesinatos-de-lideres/)  
[![Sur de Bogotá: Condenado a recibir la basura de la ciudad por 37 años más](https://archivo.contagioradio.com/wp-content/uploads/2019/01/relleno-sanitario-doña-juana-e1548870723322-770x400.jpg "Sur de Bogotá: Condenado a recibir la basura de la ciudad por 37 años más"){width="770" height="400" sizes="(max-width: 770px) 100vw, 770px" srcset="https://archivo.contagioradio.com/wp-content/uploads/2019/01/relleno-sanitario-doña-juana-e1548870723322-770x400.jpg 770w, https://archivo.contagioradio.com/wp-content/uploads/2019/01/relleno-sanitario-doña-juana-e1548870723322-770x400-300x156.jpg 300w, https://archivo.contagioradio.com/wp-content/uploads/2019/01/relleno-sanitario-doña-juana-e1548870723322-770x400-768x399.jpg 768w, https://archivo.contagioradio.com/wp-content/uploads/2019/01/relleno-sanitario-doña-juana-e1548870723322-770x400-370x192.jpg 370w"}](https://archivo.contagioradio.com/sur-bogota-basura/)  

###### [Sur de Bogotá: Condenado a recibir la basura de la ciudad por 37 años más](https://archivo.contagioradio.com/sur-bogota-basura/)

[<time datetime="2019-01-30T13:18:23+00:00" title="2019-01-30T13:18:23+00:00">enero 30, 2019</time>](https://archivo.contagioradio.com/2019/01/30/)En caso de aprobarse la propuesta de la alcaldía Peñalosa, el sur de Bogotá tendría que seguir recibiendo la basura de la Ciudad por 37 años más[LEER MÁS](https://archivo.contagioradio.com/sur-bogota-basura/)  
[](https://archivo.contagioradio.com/espacio-humanitario-de-puente-nayero-denuncia-presencia-de-neoparamilitares/)  

###### [Espacio Humanitario Puente Nayero denuncia presencia de neoparamilitares](https://archivo.contagioradio.com/espacio-humanitario-de-puente-nayero-denuncia-presencia-de-neoparamilitares/)

[<time datetime="2019-01-30T12:27:21+00:00" title="2019-01-30T12:27:21+00:00">enero 30, 2019</time>](https://archivo.contagioradio.com/2019/01/30/)La Comisión de Justicia y Paz denunció la aparición de noparamilitares en el espacio humanitario Puente Nayero, en Buenaventura[LEER MÁS](https://archivo.contagioradio.com/espacio-humanitario-de-puente-nayero-denuncia-presencia-de-neoparamilitares/)
