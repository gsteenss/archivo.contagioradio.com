Title: Entrevista con Pablo Catatumbo, Delegación de Paz de las FARC
Date: 2014-12-26 16:23
Author: CtgAdm
Category: Entrevistas
Tags: dialogos de paz, pablo catatumbo, proceso de paz
Slug: para-construir-la-paz-no-tenemos-las-formulas-pero-tenemos-voluntad-politica-pablo-catatumbo
Status: published

###### Foto: Atlas.com 

 **Entrevista a Pablo Catatumbo**

[Audio](http://ctgaudio.megadatesystem.com/upload/0/fsJZhUWL.mp3)

Pablo Catatumbo, integrante de la delegación de paz de las FARC en la Habana, afirma que la decisión de liberar a las 5 personas que se encuentran en poder de esa guerrilla se tomó en Colombia y bajo las órdenes de Timoleón Jiménez. Catatumbo afirma que para las FARC como para el país, fue una sorpresa que el General Alzate estuviera sin esquemas de seguridad y sin avanzada en esa zona del país y no se descarta que sus acciones tuvieran que ver con actividades ilegales ligadas al narcotráfico “cuando él salga, tendrá que explicarle al país en qué andaba” afirma Catatumbo. Agrega que a pesar del alto nivel militar de Alzate no pidieron ningún tipo de contraprestación a cambio de su liberación.

Acerca del cese bilateral y el des escalamiento del conflicto, Catatumbo afirma que es necesaria la sensatez y la creatividad, encerrar a los guerrilleros en zonas especiales como las propuestas por el Centro Democrático y el fiscal general fue una propuesta del gobierno de Virgilio Barco para el M-19, en ese sentido hay que ser creativos, y señaló que propuestas como la de CONPAZ y Colombianos y Colombianas por la Paz son una posibilidad que hay que explorar, sin embargo todo ello requiere de voluntad política que es la que debe mostrar el gobierno, porque ellos ya lo han demostrado.

En el tema de la justicia y la impunidad el delegado de paz de las FARC afirma que no se puede esperar que haya justicia para unos y para otros no, refiriéndose a la responsabilidad de empresarios, clase política y las mismas FFMM.
