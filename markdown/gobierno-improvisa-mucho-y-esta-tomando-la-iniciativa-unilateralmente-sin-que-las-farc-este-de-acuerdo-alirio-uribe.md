Title: PDA decide esperar la finalización de proceso de paz antes de empezar a legislar
Date: 2015-10-07 10:56
Category: Nacional, Paz, Política
Tags: Diálogos de paz en Colombia, Diálogos de paz en la Habana, firma de acuerdo de justicia gobierno y farc, Negociación Gobierno Colombia y FARC, Polo Democrático Alternativo, Radio de derechos humanos, Representante Alirio Uribe
Slug: gobierno-improvisa-mucho-y-esta-tomando-la-iniciativa-unilateralmente-sin-que-las-farc-este-de-acuerdo-alirio-uribe
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/10/Alirio-Uribe.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: youtube] 

<iframe src="http://www.ivoox.com/player_ek_8842223_2_1.html?data=mZ2hlJeWd46ZmKiakpuJd6KkkpKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRaaamhp2dh56ni9DWysrf0NSPrc7k09Tjy9jFb87pxM3cjd6PqdTohqigh6aVb9XjzsbbxtSPsMKfytPWxc7FuMqhhpywj6jTstXVyM7cjbfFqMrjjJKSmaiReA%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Alirio Uribe, Representante Polo Democrático] 

###### [07 Oct 2015] 

[El proyecto de reforma constitucional para la implementación de los acuerdos de paz firmados en La Habana, fue aprobado en su primer debate por la Comisión Primera del Senado, sin la participación de la bancada del Polo Democrático que decidió retirarse al **considerar más prudente esperar a que finalice la discusión sobre estos acuerdos entre la delegación del Gobierno y las FARC**.  ]

[El Representante a la Cámara por el Polo Alirio Uribe, asegura que este partido político apoya el proceso de negociación, pero **"Lo que el Polo no quiere es que se pierda el tiempo como sucedió con las discusiones sobre el Marco Jurídico para la Paz"** que no influyeron en las decisiones asumidas por el Gobierno nacional al respecto.]

[Uribe, agregó que el **“Gobierno improvisa mucho y está tomando la iniciativa unilateralmente sin que las FARC esté de acuerdo"** por lo que resulta más provechoso, tanto para el Congreso como para la sociedad civil en su conjunto, esperar a que entre las partes en negociación se pacte el mecanismo de implementación de los acuerdos.   ]

[Respecto a las intervenciones en el debate entre las que se destacaron la del Centro Democrático de que "Juan Manuel Santos es un dictador" y la respuesta de Roy Barreras de que "Ahora los nuevos terroristas son los del Centro Democrático", el Representante indicó que “Es lamentable que no se pueda dar un debate democrático” en el **Congreso** que **se polariza por las distintas posiciones de los sectores políticos del país**.]

[Vea también: [[Un acuerdo de paz debe tener la garantía de que no será modificado](https://archivo.contagioradio.com/un-acuerdo-de-paz-debe-tener-la-garantia-de-que-no-sera-modificado/)]]
