Title: Por hechos del Palacio de Justicia "el Estado violó el Derecho Internacional Humanitario": Santos
Date: 2015-11-06 12:50
Category: DDHH, Nacional
Tags: 30 años palacio de justica, Juan Manuel Santos, perdon de juan manuel santos, perdón víctimas palacio de justicia
Slug: por-hechos-del-palacio-de-justicia-el-estado-violo-el-derecho-internacional-humanitario-santos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/11/presidente-juan-manuel-santos-perdon.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### [6 Nov 2015]

El presidente de la república Juan Manuel Santos cumplió con la orden de la Corte Interamericana de Derechos Humanos de perdón público a las familias de los desaparecidos durante la retoma del Palacio de Justicia, de la que hoy se cumplen 30 años, asegurando que por estos hechos "el Estado colombiano violó el Derecho Internacional Humanitario".

<iframe src="https://www.youtube.com/embed/IAaFm6SD_2A" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>
