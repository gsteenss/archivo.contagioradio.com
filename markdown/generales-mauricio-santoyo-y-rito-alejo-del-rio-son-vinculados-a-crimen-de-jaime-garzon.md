Title: Generales Mauricio Santoyo y Rito Alejo Del Río son vinculados a crimen de Jaime Garzón
Date: 2015-08-12 13:01
Category: DDHH, Nacional
Tags: Comisión Colombiana de Juristas, Don Berna, Eduardo Umaña, Elsa Alvarado, General Mauricio Santoyo, General Rito Alejo del Río, Jaime Garzon, Jesús María Valle, Mario Calderon, Sebastian Escobar
Slug: generales-mauricio-santoyo-y-rito-alejo-del-rio-son-vinculados-a-crimen-de-jaime-garzon
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/08/don_berna_contagioradio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: eluniversal 

Tras conocerse nuevos detalles sobre la declaración de **Diego Fernando Murillo “Don Berna”** desde la cárcel en EEUU en que se encuentra recluido luego de su extradición, se ha podido establecer que vincula a los generales **Mauricio Santoyo, ex jefe de seguridad del presidente Uribe por la desviación de las investigaciones luego del asesinato y al General Rito Alejo del Río** por apoyar logísticamente a los sicarios que perpetraron el crimen.

Según las primeras informaciones, el General Mauricio Santoyo, **comandante del Gaula de Medellín** en esa época, fue mencionado por “Don Berna” como determinador de la información que **desvió la investigación** y que condujo a las personas encargadas a algunos señuelos que facilitaron la impunidad que reina luego de 16 años del asesinato.

Además Santoyo fue actor clave en los asesinatos de los integrantes del grupo paramilitar de “La Terraza” quienes develaron información de la participación de las FFMM en el asesinato de Garzón y de otros defensores de DDHH como **Eduardo Umaña, Jesús María Valle y los investigadores del CINEP Elsa Alvarado y Mario Calderón.**

Por su parte, el **General Rito Alejo del Río**, comandante de la Brigada 13 en el momento del asesinato de Garzón, habría proporcionado la información y **coordinado la logística que permitió a los sicarios operar eficazmente.**

Según el abogado Sebastián Escobar, de la **Comisión Colombiana de Juristas**, estas informaciones pueden contribuir al esclarecimiento del asesinato de Jaime Garzón pero también reconducir investigaciones por otros asesinatos de defensores de DDHH en Colombia durante la época.
