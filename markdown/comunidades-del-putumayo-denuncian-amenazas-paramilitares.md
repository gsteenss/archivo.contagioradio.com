Title: Comunidades del Putumayo denuncian amenazas paramilitares
Date: 2017-02-13 16:53
Category: DDHH, Nacional
Tags: Comisión Intereclesial de Justicia y Paz, Paramilitares en el Putumayo, Presencia Paramilitar
Slug: comunidades-del-putumayo-denuncian-amenazas-paramilitares
Status: published

###### [Foto: Contagio Radio] 

###### [13 Feb 2017] 

Comunidades del Putumayo denunciaron la aparición de un nuevo panfleto que amenaza la vida de los habitantes de Mocoa y zonas aledañas, la misiva realizada por una estructura paramilitar impone horarios de movilidad a la comunidad, **amenazan de muerte a quienes infrinjan los horarios, advierten de “nuevas jornadas de limpieza social”** **y cobros de vacunas.**

Carlos Fernández defensor de derechos humanos, indicó que dichos grupos paramilitares han anunciado en distintas zonas que harán reclutamientos, permanecerán en los territorios y **realizaran cobros a quienes tengan cultivos de coca, hagan minería y otras actividades que “son el trabajo habitual de las comunidades”. **([Le puede interesar: Se agudiza ola de amenazas y hostigamientos en el Putumayo](https://archivo.contagioradio.com/se-agudiza-ola-de-amenazas-y-hostigamientos-en-el-putumayo/))

Por otra parte, agentes de la Policía Nacional en el departamento del Putumayo informaron que tanto el panfleto como mensajes que circularon a través de Whatsapp, “son falsos y fueron creados por personas inescrupulosas que sólo quieren acabar con la tranquilidad de los putumayenses y generar zozobra”, el comandante del departamento, aseguró que **“este tipo de mensajes anónimos no tienen ninguna credibilidad” y desconocen si se trata de un grupo paramilitar.**

Por último, el defensor de Derechos Humanos Carlos Fernández, manifestó que en los últimos meses se ha hecho evidente “el incremento de la violencia en el departamento”, y que ante las constantes denuncias de organizaciones y comunidades, **“el Estado dice que son hechos aislados” pero los habitantes señalan que “se trata de algo sistemático”.**

<iframe id="audio_16985655" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_16985655_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
