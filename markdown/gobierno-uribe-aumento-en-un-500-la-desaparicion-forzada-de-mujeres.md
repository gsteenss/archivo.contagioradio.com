Title: Durante gobierno Uribe aumentó en un 500% la desaparición forzada de mujeres
Date: 2015-05-28 18:57
Category: DDHH, Nacional
Tags: Aida Cecilia Padilla, Alvaro Uribe, Amparo Tordesilla, desaparecida, desaparecido, forzada, forzado, Leidy Johana Robayo, María Cristina Cobo, Maria del Carmen Santana, Monica Patricia García, mujer, mujeres, Nancy Apraez, Nidia Erika Bautista, Orfilia Guisao, paramilitares, Sadith Elena Mendoza, uribe
Slug: gobierno-uribe-aumento-en-un-500-la-desaparicion-forzada-de-mujeres
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/05/Whimpala.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Friedrich Kircher 

######  

<iframe src="http://www.ivoox.com/player_ek_4567870_2_1.html?data=lZqjmZ2bdI6ZmKiakpWJd6KkkZKSmaiRdI6ZmKiakpKJe6ShkZKSmaiRhc%2FY08rOjbnTttPZ1IqfpZCtssfj09LSjdLZrsbmxtiYxsrXt8LkwtfSxc7IpdShhpywj6jTstXVyM7cjbfFqMrjjJKSmaiReA%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Andrea Torres y Janeth Bautista, Fundación Nidia Erika Bautista] 

######  

###### [28 may 2015] 

En el marco de la Semana Internacional del Detenido y Desaparecido, organizaciones de familiares víctimas de este crimen de lesa humanidad, presentaron un informe sobre el estado de la desaparición forzada de mujeres en Colombia, el primero con enfoque de género en el mundo.

En él, se denuncian (entre otros) los casos Nidia Erika Bautista, Amparo Tordesilla, Maria del Carmen Santana, Nancy Apraez, Monica Patricia García, Leidy Johana Robayo, Aida Cecilia Padilla, Sadith Elena Mendoza, Orfilia Guisao, María Cristina Cobo, y miles de colombianas que fueron desaparecidas forzadamente en el marco del conflicto armado interno.

Según se denuncia, a pesar de que Colombia cuenta con una normatividad que reconoce y prohíbe la desaparición forzada y la tipifica como un delito penal a través de la Ley 5-89 del año 2000 (fruto del esfuerzo de los familiares de las víctimas), aún no existen los mecanismos idóneos para investigar eficazmente las denuncias, buscar con vida a las personas desaparecidas, ni judicializar a los victimarios.

\[caption id="attachment\_9382" align="alignleft" width="357"\][![Informe mujeres](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/05/20150528_113429.jpg){.wp-image-9382 width="357" height="214"}](https://archivo.contagioradio.com/gobierno-uribe-aumento-en-un-500-la-desaparicion-forzada-de-mujeres/20150528_113429/) Presentación del Informe, mujeres desaparecidas\[/caption\]

En el caso de las mujeres, señala el informe, el crimen ha estado vinculado a otros tipos de violencias de género: Aunque se encuentra un registro de 19.625 mujeres desaparecidas, la Fiscalía asegura que en la mayoría de los casos no se tiene información, motivo por el cual no los tipifica como "desapariciones forzadas". Para la abogada e investigadora Andrea Torres, "con las mujeres siempre se parte de la base de justificar que "quizá se fueron con el novio, quizás eran de izquierda, que quizás eran amantes de guerrilleros”, dándole poco valor a la desaparición, y caracterizándolo por la condición de su género.

La desaparición forzada contra las mujeres ha incluido además tratos inhumanos, crueles y degradantes, violencia exacerbada y sevicia; como agresión física y verbal, acceso carnal violento, rasurado forzado del cabello, y descuartización vivas; esta última fue la causa de muerte de todas las mujeres desaparecidas por paramilitares, que registra el informe.

#####  

<iframe src="http://www.ivoox.com/player_ek_4568139_2_1.html?data=lZqjmpaXfY6ZmKiak5aJd6KkmZKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRiMbgxszOxsaPqMafzcaYsbO5aZO3jK7byNTWscafztrXx9fJt4zYxtjO0sbWqcTdxcbgj4qbh4630NPhw8zNs4zGwsnW0ZCRaZi3jpk%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Delegada de la ONU] 

De los 33 casos ejemplificados, 15 fueron cometidos por agentes del Estado, y 18 por paramilitares con aquiescencia, colaboración, o en acuerdo con agentes estatales. 20 de ellas aún continúan desaparecidas.

De los 19.625 casos de mujeres desaparecidas, 11.297 se presentaron entre los años 2004 y 2006, durante el gobierno de Álvaro Uribe Vélez, y empezaron a disminuir a partir del año 2010.

Según el informe, en ningún otro país la desaparición forzada se ha presentado de manera tan recurrente, y ha afectado a un número tan grande de personas, como en Colombia. Se refleja que como consecuencia de los altos niveles de clandestinidad con que se ejecuta, pero sobre todo, por la impunidad que ha cobijado a los victimarios, la desaparición forzada ha venido a llamarse el "crimen perfecto".

Las organizaciones de familiares denuncian que el gobierno colombiano no ha tenido la voluntad política para establecer una base de datos oficial, que recoja las denuncias e investigaciones hechas por los familiares; y que esta problemática se ha visto agudizada por el temor de los familiares a denunciar, la inseguridad que se genera cuando lo hacen, la vinculación de autoridades del Estado con grupos victimarios al margen de la ley, y la revictimización que se produce cuando inician los procesos de investigación.

#####  

<iframe src="http://www.ivoox.com/player_ek_4567965_2_1.html?data=lZqjmZ6aeY6ZmKiakpyJd6Klk5KSmaiRdo6ZmKiakpKJe6ShkZKSmaiRjsrixtnVjcfJqNDtwoqfpZCtssfj09LSjdLZrsbmxtiYxsrXpdHV08rQy8nFt46ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Jineth Bedoya, Periodista] 

Para la periodista Jineth Bedoya, víctima de violencia sexual por parte de grupos paramilitares, quien también asistió a la presentación del informe, "la peor tortura es y seguirá siendo la impunidad; no podemos dejar de pedir, un solo día, dónde están”.
