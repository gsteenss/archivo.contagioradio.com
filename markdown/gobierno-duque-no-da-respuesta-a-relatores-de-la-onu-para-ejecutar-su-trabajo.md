Title: Dos relatores de la ONU no tienen autorización del Gobierno Duque para entrar a Colombia
Date: 2020-07-01 17:55
Author: CtgAdm
Category: Actualidad, Nacional
Slug: gobierno-duque-no-da-respuesta-a-relatores-de-la-onu-para-ejecutar-su-trabajo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/03/FORST.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

*Foto: Entrevista Michel Forst/ Contagio Radio*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Luego de una primera entrega de su informe de relatoría en el mes de marzo del 2020 ; **[Michel Forst,](https://twitter.com/ForstMichel) relator especial sobre la situación de los defensores de los derechos humanos de la ONU**, señala que por falta de respuesta por parte del Gobierno no ha podido ingresar a Colombia a terminar su informe.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por otro lado **Fabián Salvioli relator especial de la ONU sobre la promoción de verdad, la justicia, la reparación y las garantías de no repetición**, señaló en medio del conversatorio ´*Lucha contra la impunidad y la participación de las víctimas en contextos de transició*n´, que a pesar de su interés en estudiar más a fondo el proceso de Paz en Colombia a la fecha *"**no ha recibido la aprobación del Estado para su visita**".*

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/SomosDef/status/1278045394677702657","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/SomosDef/status/1278045394677702657

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:heading {"level":3} -->

### En Colombia la violencia sigue en aumento, 100 liderazgos silenciados en 2020

<!-- /wp:heading -->

<!-- wp:paragraph -->

En el informe de Forst entregado a inicios del 2020, presentaba la alarmante situación evidenciada por diversas organizaciones como Defensoría del Pueblo (486), Oficina de ACNUDH (324) y el Programa Somos Defensores (400); sobre el asesinato a personas líderes y defensoras de los territorios en 2019, **cifra que en el primer semestre del 2020 ya superó los 100** casos.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En ese entonces Forst **lamentó que el debate de las cifras desviara la atención y los esfuerzos de** ***«las cuestiones claves para lograr un ambiente seguro y propicio para la defensa de derechos humanos en Colombia»**.*

<!-- /wp:paragraph -->

<!-- wp:quote -->

> *" La impunidad en casos de asesinatos a líderes y defensores de DD.HH supera el 89%, lo que sugiere un alto grado de preocupación pues este alto índice envía un mensaje de ausencia de reconocimiento de la labor en la sociedad de quienes son víctimas y ello implica **una invitación para seguir violentando sus derechos».***
>
> <cite>Michel Forst | 4 de marzo 2020 </cite>

<!-- /wp:quote -->

<!-- wp:heading {"level":3} -->

### ¿Qué le ocurre al Gobierno de Duque con la veeduría internacional?

<!-- /wp:heading -->

<!-- wp:paragraph -->

Hay varios obstáculos que ha revelado el mismo relator de la ONU, para retomar y finalizar el informe, ante un panorama de DDHH desalentador y al que se suma el aumento de la erradicación forzada, las agresiones por parte de las Fuerzas Militares, el aumento en asesinatos a líderes, defensores y firmantes de paz, así como el incremento de la presencia de los grupos armados ilegales, y las recientes denuncias por [abuso sexual a mujeres](https://archivo.contagioradio.com/otra-mirada-del-feminicidio-y-la-violencia-contra-la-mujer/)por parte del Ejército.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En una entrevista Forst señala que su insistencia para continuar con su relatoría ha sido en vano y destaca que las reuniones con el hoy Fiscal Barbosa y el embajador de Ginebra no fueron suficientes para recibir una aprobación. Agregó que incluso le señalaron que *"el Gobierno intentaba poner en práctica las recomendaciones dadas".*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El caso de Forst al no poder finalizar su trabajo, así como la resistencia para el ingreso de **Fabián Salvioli relator especial de la ONU sobre la promoción de verdad, la justicia, la reparación y las garantías de no repetición**, pone en evidencia, nuevamente, el desinterés del Gobierno de Duque en atender a los llamados de la comunidad internacional para mejorar la situación de DDHH en el país.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Le puede interesar leer: [Incumplir acuerdos internacionales trae consecuencias": Eurodiputados sobre Colombia](https://archivo.contagioradio.com/incumplir-acuerdos-internacionales-trae-consecuencias-eurodiputados-sobre-colombia/)

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->
