Title: La estrategia de Peñalosa para retrasar proceso de revocatoria
Date: 2017-07-27 13:20
Category: Movilización, Nacional
Tags: Bogotá, Enrique Peñalosa, revocatoria, Unidos revocaremos a Peñalosa
Slug: hay-un-intento-por-demorar-revocatoria-en-bogota-unidos-revocamos-a-penalosa
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/05/marcha_de_las_antorchas_para_revocar_a_penalosa_foto_blu_radio_2.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Blu Radio] 

###### [27 Jul 2017] 

El Comité Unidos Revocamos a Peñalosa manifestó su preocupación ante la petición del Alcalde de hacer revisar las 473.000 firmas recogidas para la revocatoria. Sin embargo, ante la desinformación que han generado los medios de comunicación, recordaron que la revocatoria **“es un proceso que está andando y está en firme”.**

Luego que la Registraduría Nacional, el 20 de junio avalara las firmas recolectadas por el Comité y diera paso al establecimiento del proceso de votación de la revocatoria, la defensa del Alcalde **pidió una segunda revisión de las firmas por parte de grafólogos** que él mismo puede disponer. Este organismo le dio a Peñalosa un plazo de 15 días para realizar el análisis. (Le puede interesar: ["Nuevo intento de Peñalosa por vender la ETB no sería aprobado por el Concejo"](https://archivo.contagioradio.com/nuevo-intento-de-penalosa-por-vender-la-etb-no-seria-aprobado-por-el-concejo/))

Según Diego Pinto, miembro del Comité Unidos Revoquemos a Peñalosa, **“esta solicitud del Alcalde es un intento más por dilatar el proceso de revocatoria** y nos preocupa que hay un intento por demorar todo el proceso de manera permanente”. Manifestó además que, durante la recolección de firmas, el Comité cumplió con los requisitos solicitados por la ley y “además entregamos 200 mil firmas adicionales”.

Igualmente, ellos están esperando que el CNE se manifieste frente a los papeles que solicitó para revisar los dineros de la financiación del proceso, pues **no les han entregado aún el certificado** que manifiesta que “no hubo excesos en los topes de financiación”. (Le puede interesar: ["Proceso de revocatoria no se ha suspendido"](https://archivo.contagioradio.com/proceso-de-revocatoria-no-se-ha-suspendido-comite-revoquemos-a-penalosa/))

Pinto fue enfático en manifestar que los ciudadanos siguen **“organizados en torno a las políticas nocivas del alcalde”.** Dijo además que la desfavorabilidad de Peñalosa continúa siendo muy alta y “las personas reprochan medidas como la venta de la ETB y el Transmilenio por la 7ma”.

Finalmente hizo un **llamado para que los ciudadanos defiendan la revocatoria en las calles**. El Comité seguirá organizando movilizaciones y plantones para que los bogotanos puedan defender su derecho a decidir. “Hay una actitud de la clase dirigente de no querer permitir que las personas decidan sobre sus territorios y ponen trabas en las consultas populares y las revocatorias”.

<iframe id="audio_20032696" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_20032696_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
