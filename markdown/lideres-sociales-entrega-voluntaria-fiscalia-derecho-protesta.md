Title: Si protestar es un delito nos entregamos a la Fiscalía: líderes y activistas políticos
Date: 2020-09-28 19:32
Author: AdminContagio
Category: Actualidad, DDHH
Tags: Carlos Holmes Trujjllo, Estigmatización movimiento social en Colombia, Fiscalía general
Slug: lideres-sociales-entrega-voluntaria-fiscalia-derecho-protesta
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/09/Fiscalia.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

PFoto: Fiscalía General / Contagio Radio

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Este martes 29 de septiembre, desde las 10:00 am y frente a las diferentes sedes de la Fiscalía del país se entregarán masivamente y de forma voluntaria líderes sociales y activistas políticos en particular de la Colombia Humana que han sido blanco de señalamientos por parte del Gobierno y autoridades por su participación pacífica en las recientes movilizaciones en contra de la brutalidad policial y por denunciar los ataques de la Fuerza Pública contra la ciudadanía a través de redes sociales.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

"**Somos autores del delito de protesta en circunstancias de agravación al incurrir en dicha conducta de manera pacífica, al movilizarnos arengando por el respeto a la libertad a la vida e igualmente al utilizar nuestra cuentas personales en las redes sociales para visibilizar los abusos de la autoridad policial",** acuña un fragmento del derecho de petición que será entregado el día de mañana ante la Fiscalía

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Los señalamientos de la Fiscalía

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

El gesto de los líderes sociales, se da en medio de los señalamientos hechos por la Policía Nacional que sostiene, ha recopilado en un informe de inteligencia la participación de al menos 20 organizaciones vinculadas a protagonizar actos vandálicos en medio de la movilización. A las acusaciones de las autoridades se suman a la posición de la Fiscalía General que se ha referido a un plan de “guerra de cuarta generación” con comandos urbanos de grupos armados en Bogotá. [(Le recomendamos leer: El aparato del Estado se revistió de legalidad para afectar la Movilización: Análisis)](https://archivo.contagioradio.com/aparato-estado-revistio-legalidad-afectar-movilizacion/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Mientras el [ente investigador](https://twitter.com/FiscaliaCol) asegura que a lo largo de 10 meses de investigación, realizó 11 diligencias de allanamiento, interceptaciones telefónicas de más de 1.600 horas de escucha y 112.000 actividades como audios, datos y mensajes; vigilancias y seguimiento de personas así como solicitudes de información en entidades públicas o privadas; **estudiantes de universidades públicas y miembros de colectivos han denunciado la estigmatización por parte de instituciones estatales que se refleja en montajes judiciales o persecuciones de este tipo.**[(Lea también: Estigmatización de Policías contra estudiantes se materializa en judicialización o tortura)](https://archivo.contagioradio.com/estigmatizacion-de-policias-contra-estudiantes-se-materializa-en-judicializacion-o-tortura/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Abogados como **Óscar Ramírez del Comité de Solidaridad con los Presos Políticos** han descrito estos ataques como una forma de castigar la protesta, señalando que el Estado, además de "criminalizar el liderazgo social, reprime la libertad de expresión y el derecho a la movilización"

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Con dicho gesto político, señalan, **esperan se desmonte la estrategia de desprestigio que se ha difundido en particular desde el Ministerio de Defensa encabezado por Carlos Holmes Trujillo**, quien en varias ocasiones ha vinculado a los manifestantes civiles con disidencias de las FARC o con integrantes del ELN, poniendo en riesgo a procesos sociales y comunitarios del país. [(Lea también: Panfleto de Águilas Negras amenaza a integrantes de la Colombia Humana en Suacha)](https://archivo.contagioradio.com/panfleto-de-aguilas-negras-amenaza-a-integrantes-de-la-colombia-humana-en-suacha/)

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### La estigmatización se reproduce como un altavoz del Gobierno

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Tales señalamientos que se han reproducido de mayor forma a través de los medios de comunicación empresariales, son considerados por expertos en comunicación como el profesor Omar Rincón como una forma de actuar como altavoz de los discursos del Gobierno, en particular frente a la movilización, ha planteado que existe una "demonización de la protesta social" a través de la reproducción de los señalamientos y estigmatizaciones que se originan desde el Gobierno. [(Lea también: Medios masivos "han sido incapaces de apartar su línea editorial del oficialismo" O Rincón)](https://archivo.contagioradio.com/medios-masivos-han-sido-incapaces-de-apartar-su-linea-editorial-del-oficialismo-o-rincon/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Cabe señalar que el jefe de la delegación de paz del ELN, Pablo Beltrán, expresó que las protestas son espontáneas y que la ciudadanía viene manifestándose contra la brutalidad policial, aclarando que desde la guerrilla no se ha pensando en «ataques urbanos o ataques contra Bogotá».[(ELN niega vinculación a manifestaciones y reitera voluntad de diálogo)](https://archivo.contagioradio.com/eln-niega-vinculacion-a-manifestaciones-y-reitera-voluntad-de-dialogo/)

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
