Title: Desde noviembre ELN está listo para la fase pública de proceso de paz
Date: 2016-02-01 16:20
Category: Nacional, Paz
Tags: ELN, proceso de paz
Slug: desde-noviembre-eln-esta-listo-para-la-fase-publica-de-proceso-de-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/02/maxresdefault-1-e1454356698981.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### <iframe src="http://www.ivoox.com/player_ek_10277708_2_1.html?data=kpWfmZybdJmhhpywj5aUaZS1kpeah5yncZOhhpywj5WRaZi3jpWah5yncaXZ1MnSjdPTusrZzsffx5CpkK%2Bfxtjhh6iXaaKljNHW1dnTb9HV08aYzsaPqsLnxpDdh6iXaaO1w9HWxcaPqI6ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe> 

###### [Luis Eduardo Celis] 

###### [1 Ene 2016.]

De acuerdo con el más reciente comunicado emitido por el Comando Central del ELN sólo hace falta que la delegación de paz del Gobierno nacional concrete la fecha y el lugar para la siguiente reunión con la que se cerraría la etapa confidencial y se daría paso a la fase pública de las conversaciones de paz con esta guerrilla.

Luis Eduardo Celis, analista político, asegura que más allá de las responsabilidades que competen a cada una de las partes, tanto en los asuntos logísticos como en los temas álgidos de la negociación, es importante que logren llegar a consensos, teniendo en cuenta que “este proceso ha sido largo y difícil”, aún hay temas pendientes y sobre todo una gran expectativa “porque creemos que es la hora de cerrar el conflicto armado”.

La propuesta de que Venezuela sea la sede de la mesa de conversaciones, Celis precisa que más allá de la citada “dependencia moral a manera de deuda” de esta guerrilla con el país vecino, resulta significativa la atención que tanto el Gobierno como el pueblo venezolano han brindado a las víctimas del conflicto armado que han migrado a su territorio durante los últimos 40 años,  y también su disposición en los procesos de paz con las FARC-EP y ELN.

Ante las voces que hablan de la presunta falta de voluntad de paz del Frente Domingo Laín, el analista asevera que “hasta el momento el ELN ha mantenido una voz unificada frente al proceso de paz, y ninguna voz al interior del ELN ha sido disidente”. Celis concluye aseverando que para la armonización entre las mesas de conversación Gobierno-FARC y ELN-Gobierno hace falta conocer la agenda y el diseño pactado entre las partes y competirá a la sociedad civil acompañar y respaldar las negociaciones a fin de lograr “un acuerdo satisfactorio y garantista”.[![comunicado](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/02/CaEfE9TWkAAIA3e.jpg){.aligncenter .size-full .wp-image-19946 width="600" height="915"}](https://archivo.contagioradio.com/desde-noviembre-eln-esta-listo-para-la-fase-publica-de-proceso-de-paz/caefe9twkaaia3e/)
