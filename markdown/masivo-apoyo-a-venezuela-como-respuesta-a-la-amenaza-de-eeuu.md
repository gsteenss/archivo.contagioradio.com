Title: Masivo apoyo a Venezuela como respuesta a la amenaza de EEUU
Date: 2015-03-17 21:30
Author: CtgAdm
Category: El mundo, Otra Mirada
Slug: masivo-apoyo-a-venezuela-como-respuesta-a-la-amenaza-de-eeuu
Status: published

###### Foto:Albaciudad.org[ ] 

<iframe src="http://www.ivoox.com/player_ek_4227480_2_1.html?data=lZefmZmcdI6ZmKiak5eJd6KlmZKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRkMbtjM3OxM7QrdXVz9nSjcnJb7fZz8rn18rQpY6ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Alejandro Silva, Periodista] 

**Alejandro Silva**, **periodista de Radio del Sur** y profesor universitario relata que los **venezolanos** han salido masivamente a las calles para **rechazar las amenazas por parte de EEUU.**

Han sido muchos los **apoyos internacionales**, no solo en Sudamérica, sino también en Europa, África, o Asia. Países como **Grecia, Italia, Siria y Libia** han sido convocadas marchas en solidaridad.

En Sudamérica, según Alejandro Silva, la respuesta ha sido contundente y unánime, como por ejemplo en el caso de **UNASUR, donde todos los Estados miembros han apoyado una resolución de condena a la agresión**. El periodista destaca apoyos como el de Perú, rompiendo su línea respecto a Venezuela.

Marchas y concentraciones se han podido ver en redes sociales o actos organizados institucionalmente como en **Nicaragua, con la presencia del presidente Nicolás Maduro, o el concierto en solidaridad en Cuba**.

Para el periodista de Radio del Sur, la ciudadanía entiende que este tipo de declaraciones por parte de **EEUU son el precedente para una intervención militar, como las agresiones en Siria, Libia o Irak.**

En respuesta a la amenaza, el presipresidente Nicolás Maduro solicitó al congreso una "Ley Habilitante" que se aprobó con el total respaldo de la bancada de gobierno e incluso el voto favorable de uno de los diputados de la oposición.

Sin embargo la **oposición se ha alineado con EEUU**, **excepto** el diputado, **Ricardo Sánchez**, que ha condenado la amenaza y la injerencia de EEUU y ha **votado** a favor de la **ley habilitante.**

Como respuesta rápida para defender los intereses venezolanos, la soberanía, la autodeterminación, la justicia y la paz. El presidente Nicolás Maduro ha explicado a la nación que esta **ley permite emitir decretos en temas referidos a la justicia y a la soberanía y su defensa.**
