Title: 30 estudiantes resultaron heridos durante movilización en Bogotá
Date: 2018-11-09 18:12
Author: AdminContagio
Category: DDHH, Movilización
Tags: educación pública, Movilización
Slug: al-menos-30-estudiantes-resultaron-heridos-durante-movilizacion-en-bogota
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/06/esmad-presidente-santos.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Archivo 

###### 9 Nov 2018 

Alrededor de 30 estudiantes resultaron heridos con aturdidoras y bolillos durante la movilización, por ahora no hay conocimiento de estudiantes desaparecidos durante las manifestaciones, existen listas que deben manejarse con el debido protocolo y las fuentes correspondientes  para corroborar la información.

Como parte de las marchas por la educación que cubrieron varias rutas en Bogotá el pasado 8 de noviembre, un grupo de estudiantes se desplazó en sentido sur-norte a través de las calles 30 y Caracas, trayectos durante los cuales el ESMAD atacó a estudiantes con bombas aturdidoras y lacrimógenos, una actitud que se repitió a lo largo del recorrido, según denunciaron desde la UNEES.

Tal como se vio a través de redes sociales y como lo confirmó **Valentina Ávila,  delegada nacional de la UNEES**, uno de los casos a resaltar sucedió en la calle 97 al interior de un Éxito Express; mientras 44 estudiantes, entre ellos una persona herida, huían de los gases lacrimógenos que comenzaban a asfixiarlos y decidieron refugiarse en el lugar, el ESMAD se percata del ingreso de los estudiantes y los encierra impidiéndoles la salida.

Mientras medios empresariales afirman que se trató de una toma del almacén por parte de los estudiantes, las versiones de los afectados son distintas dado que **la actitud intimidante del ESMAD también generó temor entre los clientes y trabajadores** que se encontraban en la tienda quienes se han negado a dar declaraciones por considerar que pondrían en riesgo su seguridad.

"No estaban armados, no estaban haciendo disturbios, no eran encapuchados y el ESMAD no los deja salir aun cuando les  gritaban que tenían una compañera herida y no iban entrar en disturbios con ellos, sencillamente querían refugiarse" aclaró  Ávila.

A tan solo unas cuadras, mientras protestaban pacíficamente, otro grupo de e**studiantes fue atropellado por un mismo vehículo en dos ocasiones**. En otro punto de la ciudad (Cra Séptima con calle 13)  nueve estudiantes fueron detenidos, golpeados y transportados al CAI del Rosario y luego a la estación de La Candelaria.

**Declaraciones del Ministerio de  Defensa**

Respecto a los ataques hacia los policías que escoltaban las oficinas de RCN Radio en la carrera 37 con calle 13, la vocera de la **UNEES** explicó "que son casos que aún deben ser esclarecidos para saber qué sucedió, no tenemos la certeza de quienes hayan hecho este acto sean estudiantes".

A través de redes sociales, el ministro de Defensa, **Guillermo Botero** se refirió a esa situación y afirmó que los responsables "serán judicializados y les caerá todo el peso de la ley", una respuesta que no sorprende a los estudiantes quienes ya esperaban esta reacción del gobierno entrante.

**"Necesitamos garantías para el ejercicio de la movilización y la protesta,  eso no se ha dado,  reprimir la protesta y no escuchar a los estudiantes, es una posición oficial del gobierno" **señaló Ávila quien concluye que el foco de atención se ha desviado de la movilización pacífica hacia los ataques a la fuerza pública.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
