Title: Universidad Distrital sin presupuesto para el 2018 y con déficit de 13 mil millones
Date: 2017-12-26 10:36
Category: Educación, Nacional
Tags: CSU, Universidad Distrital
Slug: universidad-distrital-sin-presupuesto-para-el-2018-y-con-deficit-de-13-mil-millones
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/12/6.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:] 

###### [26 Dic 2017] 

Los estudiantes de la Universidad Distrital manifestaron una vez más su preocupación frente al déficit presupuestal que asciende a** 13 mil millones de pesos para el 2018.** El recorte que significará cambios, según los estudiantes, en la contratación a la planta docente y en algunos rubros, además señalaron que, en el último consejo superior, se pasaron por alto los acuerdos pactados luego del paro estudiantil.

“Este recorte manda un mensaje claro de austeridad y de recortes dentro de la misma universidad, este déficit se va a tratar de suplir con algunos rubros que llegarían en el transcurso del año, pero que todavía no es claro que se tengan, **hay un nivel de irresponsabilidad**” afirmó José Abraham Rivera, representante ante el CSU.

De igual forma la contratación docente se hará a 10 meses, situación que repercutiría en las áreas de investigación y calidad de la educación, situación que para los estudiantes evidencia la profunda crisis que aún no tiene respuestas por parte de directivas. (Le puede interesar: ["Universidad Distrital dejará de recibir más de 7 mil millones de pesos al año"](https://archivo.contagioradio.com/universidad-distrital-dejara-de-recibir-mas-de-7-mil-millones-de-pesos-al-ano/))

A su vez, los representantes estudiantiles, manifestaron que la información sobre el presupuesto **se dio a conocer dos horas antes de que se citará al último consejo del año**, bajo la rectoría de Ricardo García y así mismo fue aprobado por la mayoría de los integrantes de este espacio.

### **Los incumplimientos a lo acordado** 

De acuerdo con Ana María Nates, representante al CSU, en el cronograma y la metodología para este consejo también se pasaron por alto todos los acuerdos que se negociaron del último paro estudiantil “es importante señalar que en primer lugar no se tienen en cuenta los acuerdos de la mesa de negociación, **sino que se restringe a la aprobación del estatuto, ignorando la vinculación y socialización con los estudiantes**” afirmó.

En el último paro, los estudiantes habían acordado la importancia de modificar los estatutos de la Universidad para que las elecciones a cargos se dieran de formas más democráticas, esas reformas al estatuto se socializarían con los estudiantes y se construiría en conjunto, sin embargo con la propuesta que se realizó por parte la administración, estos estos escenarios no se darían.  (Le puede interesar: ["Elección de rector de Universidad Distrital es ilegítima: Estudiantes"](https://archivo.contagioradio.com/eleccion-de-rector-de-la-universidad-distrital-es-ilegitima-estudiantes/))

En ese sentido los estudiantes lograron acordar que el **18 de enero se hará una presentación de los estatutos por parte de la mesa negociación**, además hicieron un llamado para que las y los estudiantes exijan el próximo año tener conocimiento sobre la forma en la que se ejecuta el presupuesto de la universidad y estar al tanto del cumplimiento de los acuerdos del último paro.

###### Reciba toda la información de Contagio Radio [en su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio ](http://bit.ly/1ICYhVU) 
