Title: $180.000 millones ha sido el ahorro desde el reajuste a las megapensiones
Date: 2017-01-06 00:08
Category: Economía, Nacional
Tags: Megapensiones, Pensiones de MAgistrados, Pensiones del Congreso
Slug: 180-000-millones-ha-ahorro-desde-reajuste-las-megapensiones
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/01/megapensiones.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Zonacero] 

###### [5 Enero 2017] 

Frente a las demandas interpuestas por un grupo de ex congresistas, quienes rechazan el rejauste a las megapensiones y reclaman se les restablezca la pensión al valor que devengaban antes de la sentencia C-258 de 2013, la Corte Constitucional se pronunció y ratificó los alcances del decreto, que **según el Fondo de Pensiones del Congreso –Fonprecón–, ha generado un ahorro de \$180.000 millones.**

El fondo de pensiones, aseguró que hasta finales del 2016 recibieron **más de 500 reclamaciones y acciones de tutela**, interpuestas por cerca de 150 pensionados, que buscan derogar el ajuste del monto pensional.

El magistrado Jorge Iván Palacio aseguró que luego de un extenso y complejo debate, “no se encontraron méritos para modificar los alcances de la sentencia que fue avalada en el alto tribunal” **la cual fija el tope máximo en 25 salarios mínimos para las conocidas megapensiones de congresistas y magistrados de las altas cortes.**

La Corte, señaló que el monto de las pensiones **pueden ser modificadas solo si estas fueron otorgadas de forma irregular o incumpliendo lo establecido en la ley.**

Por último, Palacio señaló que “los lineamientos de la sentencia son claros cuando se fija el tope, (…) **las pensiones fijadas antes de septiembre de 2013 quedan como estaban estipuladas”.**

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio.](http://bit.ly/1ICYhVU) 

 
