Title: Gobierno y estudiantes logran acuerdo en mesa de negociación
Date: 2018-12-14 12:35
Author: AdminContagio
Category: Educación, Movilización
Tags: estudiantes, Gobierno, Paro Nacional de Estudiantes, Presupuesto
Slug: estudiantes-gobierno-acuerdo
Status: published

###### [Foto: @IvánDuque] 

###### [14 Dic 2018] 

Este viernes, y tras 64 días de paro nacional, estudiantes y Gobierno Nacional llegaron a un pre-acuerdo encaminado a superar la crisis que atraviesa la educación superior pública. En un documento firmado por las organizaciones estudiantiles, profesorales y representantes del Gobierno, encabezado por el presidente Duque, se lograron cerca de 5,8 billones para las Instituciones de Educación Superior (IES) públicas del país para los próximos 4 años.

El documento de acuerdo parte reconociendo que la Mesa de Negociación fue posible gracias a la movilización estudiantil. La aceptación de recursos para la educación superior del fondo nacional de regalías no implica aceptar el modelo extractivo con el que se obtienen dichos recursos, y que lo pactado será socializado en los procesos asamblearios que hacen parte del paro, cuyos resultados se presentarán el 25 de enero.

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/post.php?href=https%3A%2F%2Fwww.facebook.com%2FUNEES.COL%2Fposts%2F263467030992560&amp;width=500" width="500" height="702" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
