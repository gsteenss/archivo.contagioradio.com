Title: Paramilitares torturaron y asesinaron una persona en Salaquí, Chocó
Date: 2016-01-21 18:25
Category: DDHH, Nacional
Tags: Bajo Atrato, paramilitares colombia, Paramilitares en Chocó
Slug: paramilitares-persona-asesinada-choco
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/01/Bajo-Atrato.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: IPS Noticias. 

<iframe src="http://www.ivoox.com/player_ek_10166749_2_1.html?data=kpWemJubeJqhhpywj5WZaZS1k5mah5yncZOhhpywj5WRaZi3jpWah5yncbHV08bay9HNuMLmxtiY1tTWuNbmwtfc0JDdb8LnxtjW0MbWs8%2Bf1tPOjdXJttTjz8aYx9OPl8Khhpywj6jTstXVyM7cjbfFqMrjjJKSmaiReA%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [[(]21 Ene 2015[)]] 

[En las últimas horas, la Comisión de Justicia y Paz **confirmó la muerte de uno de los cinco afrodescendientes que se habían reportado como desaparecido**s, en medio de la nueva incursión paramilitar en el [[Bajo Atrato](https://archivo.contagioradio.com/?s=bajo+atrato)] chocoano. Se trata de **Walter Cartagena**, quien vivía en la comunidad de Balsagira, territorio colectivo de la cuenca del río Cacarica, y que **fue asesinado luego de ser torturado por grupos paramilitares**, en la cuenca del río Salaquí. Las otras cuatro personas ya fueron liberadas.]

[De acuerdo con pobladores estas incursiones paramilitares inician su avanzada en Salaquí y de allí se trasladan a los territorios colectivos de Truando y Cacarica, con no menos de 300 paramilitares. La situación alarma a las comunidades pues temen que sucedan hechos trágicos como los que rodearon el desplazamiento masivo entre 1996 y 1997, **“se habla de paz, pero en el campo uno siente que la situación no cambia, es preocupante y no como se habla en los medios de que ya no hay ninguna guerra”**, aseguran.]

[Lo que más alarma a los pobladores es que las autoridades militares y civiles del municipio de Riosucio, no den respuestas efectivas frente a la problemática, pese a las denuncias que han presentado las comunidades, "**por las cabeceras de los ríos transitan pangas llenas de paramilitares frente a las autoridades y éstas no se manifiestan"** aseveran e insisten en que los paramilitares se movilizan con armas largas y vestidos de camuflado. En este momento se presume que están en los territorios del Salaquí y Balsagira.  ]

[Así mismo, en medio de estas acciones que repercuten en la vida de los pobladores del Chocó, los días **14, 18 y 20 de enero en las en las playas de Unguía, Chocó, el mar arrastró nueve cuerpos en descomposición,** que ya fueron trasladados a Turbo, Antioquia, para realizar las investigaciones, la necropsia y demás procedimientos judiciales para determinar las causas de la muerte.]

[Cabe recordar que desde hace dos semanas, habitantes del municipio de Turbo, denuncian que a varias casas de la zona, a los correos y a las redes sociales, está llegando un panfleto amenazando con la realización de una **“limpieza social”, por parte del grupo paramilitar los Urabeños.**]

###### [Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU) ]] 
