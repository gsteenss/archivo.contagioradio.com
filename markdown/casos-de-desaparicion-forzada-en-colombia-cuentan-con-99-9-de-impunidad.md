Title: Hay 99.9% de impunidad en casos de desaparición forzada
Date: 2015-11-04 19:30
Category: DDHH, Nacional
Tags: alirio uribe, Congreso de la República, Desaparición forzada en Colombia, impunidad en Colombia, Polo Democrático Alternativo
Slug: casos-de-desaparicion-forzada-en-colombia-cuentan-con-99-9-de-impunidad
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/11/Desaparición-forzada.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Contagio Radio 

###### [4 Nov 2015]

Este miércoles los congresistas Iván Cepeda, Ángela María Robledo y Alirio Uribe, llevaron a cabo una audiencia sobre la desaparición forzada, donde se concluyó que esta problemática continua siendo un fenómeno del presente, teniendo en cuenta que **en los últimos 10 años se han registrado 30 mil nuevos casos, “es decir 3 mil por año, aproximadamente 8 o 10 por día”,** señala el representante del Polo Democrático Alternativo, Alirio Uribe.

Durante la audiencia, los congresistas, las víctimas y las organizaciones denunciaron que actualmente existen más de 122 mil personas desaparecidas según cifras oficiales, además hay 90 mil investigaciones en cursos, y únicamente hay 105 sentencias, lo que significa que **“el 99.9%  de los casos están en la impunidad”,** indica el representante.

Frente al tema de la comisión de búsqueda de las personas desaparecidas, la preocupación se centra en que este mecanismo es ineficiente, “las autoridades no se han apropiado de la búsqueda urgente. **Cuando se trata de un secuestro en segundos se hace seguimiento pero para buscar a los desaparecidos no hay nada, ni plata ni recursos ni voluntad política**”, afirma Uribe Muñoz.

Así mismo, se solicitó que exista un enfoque de género para agilizar la búsqueda de mujeres desaparecidas, cuyos casos cuentan con mayores índices de impunidad, debido a que en Colombia prima una sociedad patriarcal, y siempre se dan diversas excusas para llevar a cabalidad la búsqueda, lo que implica que se da paso a que ya no “se busca una persona sino un cadáver”, dijo el representante, y agregó, **“Colombia no está honrando sus compromisos internacionales en evitar, prevenir e investigar las desapariciones”.**

Mediante la audiencia, las organizaciones de víctimas entregaron sus propuestas a Medicina Legal,  la Comisión de Búsqueda, y también se harán llegar a la Habana, teniendo en cuenta que en el marco de las conversaciones de paz se acordó la búsqueda de las personas desaparecidas durante los casi 60 años de conflicto armado.

“**Esperamos que las entregas humanitarias de restos de guerrilleros, civiles o militares pueda ayudar a ver la importancia de devolver los cuerpos y darle nombre y rostros a los desaparecidos en Colombia”**, concluyó el congresista.
