Title: Comunidades del Río Chagüi se preparan para regresar a su hogar
Date: 2020-01-24 18:11
Author: CtgAdm
Category: Comunidad, Nacional
Tags: Clan del Golfo, Río Chagui, Tumaco
Slug: comunidades-del-rio-chagui-se-preparan-para-regresar-a-su-hogar
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/01/Chagui.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: Chagüi Unidad de Víctimas

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Desde horas de la mañana de este viernes 24 de enero, algunas embarcaciones con habitantes de **las 29 veredas del Río Chagüi en Nariño, se preparan para emprender la travesía de regreso a sus hogares,** los cuales fueron abandonados como resultados de los enfrentamientos entre la columna Oliver Sinisterra, el ELN y el Clan del Golfo.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Más de 3.000 personas desplazadas del río Chagüi, de esta población, aguardan a que se den los protocolos de seguridad que les permita regresar a su tierra, pese a ello, también hubo algunas embarcaciones que partieron por sus propios medios rumbo a su territorio. [(Lea también: Disputa por Triángulo de Telembí en Nariño, impide el regreso de 1.179 familias a su hogar](https://archivo.contagioradio.com/habitantes-chagui-soluciones-integrales/))

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Una Comisión de la Gestión del Riesgo se ha desplazado a través del río para verificar que el caudal del río sea óptimo para que los pobladores de las veredas que actualmente están en [Tumaco](https://twitter.com/UnidadVictimas/status/1220803613116813312) puedan retornan con normalidad, para ello, Armada y Ejército ya se encuentran a orillas del río  
para garantizar un regreso a salvo. [(Le puede interesar: Obispos piden reabrir diálogos con ELN y garantías para líderes y comunidades)](https://archivo.contagioradio.com/obispos-piden-reabrir-dialogos-con-eln-y-garantias-para-lideres-y-comunidades/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

A su vez, comenzó el transporte de las 69 toneladas de ayuda humanitaria destinadas a las comunidades y que serán repartidas en tres puntos ubicados en Las Mercedes, Palambí y Coarazangá. Se manifestó de igual forma que otras entidades como la Unidad de Víctimas, Personeria, Procuraduría, Defensoría e Instituto Colombiano de Bienestar Familiar acompañarían el retorno. [(Le recomendamos leer: Líderesa cultural Lucy Villarreal fue asesinada en Llorente-Tumaco)](https://archivo.contagioradio.com/lideresa-cultural-lucy-villarreal-fue-asesinada-en-llorente-tumaco/)

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Noticia en desarrollo...

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
