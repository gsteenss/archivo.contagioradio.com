Title: Juntos, somos más
Date: 2015-11-22 21:28
Category: Nacional
Tags: alba, Brasil, encuentro comunicadores, Movimiento Sin Tierra del Brasil
Slug: juntos-somos-mas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/11/juntos-somo-mas-encuentro-brasil.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Contagio Radio 

#### **[Por Contagio Radio]** 

###### [22 de Nov 2015]

La siembra de un árbol a muchas manos,  expresa el arraigo y la confluencia en medio de diversidades, luego de tres días de deliberación constructiva de más de 50 delegados de medios de información y comunicación virtual, impresa, radio y televisión de las Américas.

A las afueras de Sao Paulo, Brasil, la reunión Continental de Medios de Comunicación, convocada por el **Movimiento Sin Tierra del Brasil**, Brasil de Fato y Alba Movimientos, del 20 al 22 de noviembre, se construyeron desafíos y planes de trabajo específicos para continuar  afirmando el derecho a la comunicación y a la información democrática desde la base,  asumiendo retos en los nuevos tiempos y realidades políticas y económicas, para seguir produciendo mensajes y apuestas comunicativas  desde otros sujetos, negados o manipulados por corporaciones mediáticas.

Una agenda mediática desde el movimiento social que comprende la memoria colectiva, la crisis ambiental y el modelo extractivista, la igualdad de género y el movimiento LGTBIQ, la construcción de cultura política transformante y su renovación desde el movimiento de base, la cultura y el deporte, los nuevos lenguajes e identidades, la pluriculturalidad en norte, centro y sur son parte de una acción comunicativa conjunta de la Patria Grande en su diversidad cultural.

Los aportes concretos sobre las narrativas, la búsqueda de llegar a nuevos públicos, la medición de impactos, el aprendizaje sobre el valor de las redes sociales, y la importancia de la comunicación como generación y apoyo a procesos populares fueron parte de las deliberaciones y de los desafíos a los que los participantes y sus medios se comprometieron a asumir.

Los delegados de Argentina, Chile, Brasil, Perú, Ecuador, Colombia, Venezuela, Nicaragua, Guatemala, El Salvador, Honduras, República Dominicana, Canadá, Estados Unidos fortalecerán la agenda del movimiento social con sus agendas comunicativas,

La persecución judicial y la represiva violenta contra periodistas y grupos comunicativos que denuncian violaciones de derechos humanos, la corrupción institucional y la criminalidad de empresas privadas multinacionales fue uno de los temas de preocupación en el Encuentro. Los periodistas expresaron su solidaridad con la situación de Argentina, Honduras, México, Colombia.

En medio de la humedad brasilera, de una fraterna expresión de apoyo mutuo y compromiso, ha quedado fortalecida una apuesta por una comunicación articulada, desde otro lugar, la del movimiento social que construye propuestas de transformación que convierte los derechos en realidades, entre ellos, el derecho de la comunicación. Por eso, como bellamente lo expresó una de los participantes: "[**Juntos, somos más**]". Y otros resonaron: "No estamos solos"
