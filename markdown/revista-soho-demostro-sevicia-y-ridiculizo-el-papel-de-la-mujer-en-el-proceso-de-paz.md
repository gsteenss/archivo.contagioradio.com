Title: Revista Soho demostró sevicia y ridiculizó el papel de la mujer en el proceso de paz
Date: 2015-11-13 18:54
Category: Mujer
Tags: movimiento feminista, Revista Soho
Slug: revista-soho-demostro-sevicia-y-ridiculizo-el-papel-de-la-mujer-en-el-proceso-de-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/11/soho.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto:almomento 

###### [13 nov 2015 ]

El próximo 17 de noviembre se realizará un plantón a las 5:30 de la tarde, frente a las instalaciones de la Revista Soho, en rechazo a la última publicación de ese medio, cuya portada **"ridiculiza" el aporte de las mujeres frente al proceso de paz,** como lo afirma Mar Candela, vocera dle Movimiento Feminismo Artesanal.

En la última publicación de Soho se muestran dos mujeres en una pose erótica, una de ellas es exguerrillera de las FARC, la otra fue detective del DAS y adelantó diversas investigaciones en contra de esa guerrilla. Según la revista se hace un homenaje a la reconciliación, sin embargo, para Mar Candela lo que se hizo fue "**demostrar sevicia con el sentido de vender morbo y vender revistas, en ningún momento lo hicieron para construir paz”.**

"A Soho nadie la busca para informarse sobre el proceso de paz, afectaron toda una lucha de derechos **¿Cuándo Soho le va a dar una voz seria a las mujeres frente al proceso de paz?",** expresa con indignación la feminista, quien concluye que **lo que se logró fue satanizar a las mujeres que han hecho parte del conflicto armado, que tienen bastante por aportar en materia intelectual.**

**[![CTuLAa4WoAAn6eZ](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/11/CTuLAa4WoAAn6eZ.jpg){.aligncenter .size-full .wp-image-17248 width="599" height="381"}](https://archivo.contagioradio.com/revista-soho-demostro-sevicia-y-ridiculizo-el-papel-de-la-mujer-en-el-proceso-de-paz/ctulaa4woaan6ez/)**
