Title: Humedal El Burrito en riesgo por proyectos urbanísticos
Date: 2020-01-14 13:19
Author: CtgAdm
Category: Ambiente, Entrevistas
Tags: Ambiente, El Burrito, El Burro, Humedales, Kennedy
Slug: humedal-el-burrito-en-riesgo-por-proyectos-urbanisticos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/01/EOKvP0TXsAUSZGi-scaled.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/01/EOML1OEX0AAOfVm.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/01/EOKnbIzX0AMPYVQ-scaled.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/01/EOLB94wWoAAnG80.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/01/EOKnbIzX0AMPYVQ-1-scaled.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"justify"} -->

El 13 de enero frente a la Secretaría de Ambiente de Bogotá se realizó un plantón por parte de ambientalistas y vecinos del humedal **El Burrito**, ubicado en la localidad de Kennedy. La manifestación se desarrolló ante la amenaza de construcción urbanística en el terreno.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

La situación ya había sido denunciada por la concejala y ex -secretaria de ambiente **Susana Muhamad**, quien había enviado una solicitud a Carolina Urrutia, actual Secretaria Distrital de Ambiente, para que efectué una visita al Humedal e inicie las investigaciones que permitan sancionar las posibles construcciones que atenten contra El Burrito. (Le puede interesar: <https://archivo.contagioradio.com/pot-satisface-intereses-economicos/> )

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

*"En mi calidad de Concejal de Bogotá acudo a su Despacho con el objeto de solicitar una visita de control y seguimiento a las actividades constructivas que están desarrollando las empresas Construcciones Marval S.A y IC Constructora S.A, presuntamente en la faja paralela (ronda hídrica) y en la zona de protección y manejo del humedal El Burrito, ubicado en la Localidad de Kennedy del D.C",* señaló Muhamad en la solicitud.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

La respuesta a las demandas fue atendida en medio del plantón, cuando Urrutia diálogo con los voceros y escuchó las agresiones hacia el Humedal por parte de la administración pasada, en cabeza de Enrique Peñalosa y las constructoras interesadas en el predio.

<!-- /wp:paragraph -->

<!-- wp:image {"id":79027,"sizeSlug":"large"} -->

<figure class="wp-block-image size-large">
![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/01/EOML1OEX0AAOfVm-1024x768.jpg){.wp-image-79027}  

<figcaption>
  
@Luise17407144

</figcaption>
</figure>
<!-- /wp:image -->

<!-- wp:heading {"level":3} -->

### **El Burrito no entró en la marcación del** humedal El Burro

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

**El humedal El Burrito** lleva este nombre al limitar al norte con el Parque Ecológico Distrital de Humedal “El Burro”, uno de los 12 humedales protegidos bajo la Resolución 1238 del 2012, “*Por medio de la cual se adoptan medidas de protección de un ecosistema y se toman otras determinaciones”*

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Diferentes solicitudes y reclamos por parte de ciudadanos y veedurías de la localidad de Kennedy han logrado identificar que la empresa **Construcciones Marval S.A** inició actividades constructivas luego de que la **alcaldía de Peñalosa, con el respaldo de la autoridad ambiental distrital, afirmara que este humedal *“no existe”***. 

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Decisión que fue respaldada por la Secretaria Distrital de ambiente mediante la Resolución 03643 del 16 de diciembre de 2019, la cual deja invalidado el decreto anterior que protegía El Burrito.

<!-- /wp:paragraph -->

<!-- wp:quote -->

> *“Sospechosamente la administración del exalcalde Peñalosa justifica que la medida de protección hay que levantarla para permitir que los señores de Marval sigan rellenando, desaguando y rompiendo el alcantarillado para dar viabilidad a su proyecto urbanístico”*
>
> <cite>Alejandro Torres</cite>

<!-- /wp:quote -->

<!-- wp:paragraph {"align":"justify"} -->

Según Alejandro Torres, vocero y líder ambiental, cuando se hizo la demarcación de este humedal hace 26 años el distrito dejó por fuera varias zonas con fauna y flora propia de este ecosistema, entre estas, El Burrito.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Asimismo afirmó que este territorio, más no su recurso hídrico, actualmente es propiedad de la constructora Marval que pretende desarrollar un proyecto urbanístico bajo la premisa *[**"no**]{}**existe humedal acá"***.

<!-- /wp:paragraph -->

<!-- wp:quote -->

> “Cómo no va haber humedal si los señores de Marval metieron unas dragas, hicieron unas zanjas de 2,50 metros de profundidad, rompieron un sistema de alcantarillado y drenamos más de 12mil m³ de agua; cómo va haber humedal si ya lo desaguaron, la pregunta es, ¿en qué estado está el proceso sancionatorio por el drenaje de un cuerpo de agua? ”
>
> <cite>Alejandro Torres</cite>

<!-- /wp:quote -->

<!-- wp:image {"id":79031,"sizeSlug":"large"} -->

<figure class="wp-block-image size-large">
![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/01/EOKnbIzX0AMPYVQ-1-1024x300.jpg){.wp-image-79031}  

<figcaption>
Foto por: @Luise17407144

</figcaption>
</figure>
<!-- /wp:image -->

<!-- wp:heading {"level":3} -->

### **Un recurso ambiental deteriorado está avaluado en** \$100.000 millones

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

El humedal El Burrito corresponde a un área de 4.75 hectáreas, cada una de estas según el avaluó catastral más reciente por un valor de  \$22.000 millones, un total de \$100.000 millones. "El valor de este predio no existe, lo que vale allí es el cuerpo de agua cada vez más deteriorado y que significa un tema de gran calado y que amerita toda la atención de la nueva alcaldesa”

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

El paso a seguir según el líder ambiental es no detener el trabajo desarrollado por más de 26 años en la defensa del humedal, *“creemos que con esta nueva administración pronto van a derogar esa orden que atenta contra el derecho de un ambiente sano y la salud pública y por lo contrario van a tomar medida de protección a este ecosistema”* .

<!-- /wp:paragraph -->

<!-- wp:html -->  
<iframe id="audio_46591536" frameborder="0" allowfullscreen scrolling="no" height="200" style="border:1px solid #EEE; box-sizing:border-box; width:100%;" src="https://co.ivoox.com/es/player_ej_46591536_4_1.html?c1=ff6600"></iframe>  
<!-- /wp:html -->

<!-- wp:block {"ref":78955} /-->
