Title: Trabajadores Drummond en el Cesar, denuncian falta de protección en medio de crisis sanitaria
Date: 2020-03-26 07:00
Author: AdminContagio
Category: yoreporto
Tags: cesar, Covid-19, Drummond, SINTRADEM, yo reporto
Slug: trabajadores-drummond-en-el-cesar-denuncian-falta-de-proteccion-en-medio-de-crisis-sanitaria
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/03/drummond-848x480-1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

*Bogotá, Marzo 24 de 2020 -*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Trabajadores del Sindicato Nacional de Trabajadores, Enfermos y Discapacitados del Sector Minero -**SINTRADEM**- denuncian que en medio de crisis internacional por el virus **COVID-19**, la multinacional Drummond está obligando a los trabajadores a acudir a sus puestos de trabajo, exponiéndolos a riesgos innecesarios.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En medio de la pandemia a nivel mundial declarada por la Organización Mundial de la Salud por la propagación del virus Covid-19 donde hay hasta la fecha 360.000 infectados en todo el mundo y más de 16.000 personas fallecidas. En Colombia hasta el momento hay más de 306 casos confirmados según el último reporte y es por ello que el presidente Iván Duque declaró desde el Puesto de Mando Unificado, el pasado 12 de marzo, la emergencia Sanitaria que impulsa restricciones a la movilidad, concentración y por ende la cancelación de importantes eventos en el país.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":5} -->

##### El Gobierno nacional para afrontar la crisis sanitaria ha emitido una serie de decretos que le permita tomar de decisiones para contener el avance del Covid-19.

<!-- /wp:heading -->

<!-- wp:paragraph -->

Uno de éstos fué el decreto **457**, el cual decretó la cuarentena total en el país que durará hasta el 13 de Abril de este año. Adicionalmente, en la noche del 22 de marzo se expidió el decreto **444** con el fin de crear el Fondo de Mitigación de Emergencias **-FOME-** el cual ya desató polémica debido a que dicho decreto en el numeral 3 contempla *“efectuar operaciones de apoyo de liquidez transitoria al sector financiero a través de transferencia temporal de valores, depósitos a plazo, entre otras”.* El Ministerio del Trabajo por su parte emitió la resolución **803** mediante el cual otorga “poder preferente”, lo que implica que las decisiones sobre situaciones laborales serán tomadas desde el nivel central y no desde los inspectores regionales. Resolución que alertó al conjunto de los trabajadores del país por abrirse la puerta para medidas en materia laboral que atenten contra la estabilidad de millones de familias.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En medio de esta difícil coyuntura, el día 22 de marzo recibimos la  
denuncia de trabajadores de la multinacional Drummond en el municipio de  
Becerril, departamento del Cesar, que afirman que la empresa no ha tomado las  
medidas necesarias para asegurar la salud de sus trabajadores, por el contrario  
los han obligado a ir a sus puestos de trabajo.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

“En esta pandemia  
a nivel mundial las y los trabajadores deben estar en casa, ésta situación no  
es culpa de la clase trabajadora y en la empresa no estamos preparados” aseguró  
José Cabrera, trabajador de la multinacional del Carbón.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por su parte, Jose Miguel Linares, presidente de Drummond Colombia  
manifestó vía twitter que “En el caso del carbón, es nuestra responsabilidad  
garantizar que este producto esté disponible para la generación de energía  
eléctrica en más de 24 países”.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Ante la inminencia de una propagación masiva en el país donde el  
sistema de salud es supremamente precario, vemos necesario el llamado a  
respetar la cuarentena por parte de la población y  las empresas en consecuencia, deben ser  
consecuentes con el momento y buscar alternativas para asegurar el bienestar de  
las y los trabajadores, velando por el bienestar de sus familias.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por esta razón, el Senador Alberto Castilla manifestó su preocupación e invitó al Gobierno Nacional a que “priorice el bienestar de la gente por encima de las finanzas privadas y utilidades de grandes empresas” y manifestó que se deben “tomar decisiones que brinden tranquilidad a las millones de familias de trabajadoras y trabajadores formales e informales y presionando a las empresas a que se respeten los derechos laborales y asegure su bienestar para evitar la propagación del virus entre las comunidades”.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Le puede interesar: [Yo reporto](https://archivo.contagioradio.com/?s=yo+reporto)

<!-- /wp:paragraph -->
