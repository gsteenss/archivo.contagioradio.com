Title: El suicidio de 500 familias indígenas durante la conquista inspira novela
Date: 2017-09-28 13:25
Category: Cultura
Tags: Bogotá, indígenas, literatura, Novela
Slug: novela-ayer-terminara-manana
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/09/AYER-TERMINARA.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto:Portada novela 

###### 28 Sep 2017 

Este jueves se presentará en Bogotá **"Ayer terminará mañana"**,  primera novela del escritor **Santiago José Sepúlveda**, fruto de su trabajo de más de dos años por culminar la maestría en escrituras creativas de la Universidad Nacional de Colombia.

El palabras de su autor, la novela es "**un poco experimental tanto en estructura como en lenguaje**" partiendo de tres líneas narrativas: la de **500 familias indígenas que se suicidan de manera colectiva**, la de una **familia descendiente de los nativos** quienes revivirán parte del destino de sus antepasados y **la mirada del escritor** quien se cuestiona por su relación con los dos relatos.

La imagen histórica de las 500 familias indígenas que se suicidan durante la época de la conquista, le llegó al escritor tras asistir a una serie de conferencias sobre la historia de Bogotá, impartidas por Manuel Hernández, **a partir de la información consignada en el Compendio histórico de Indias**. (Le puede interesar: [Mujeres indígenas siguen luchando por sus derechos](https://archivo.contagioradio.com/mujeres-indigenas-luchan-por-sus-derechos/))

El ubicar la segunda línea narrativa en el futuro de los descendientes, partió de la necesidad del autor de definir si su propósito era construir una novela histórica ficcional o si se movería a través del tiempo por la significación que tiene el suicidio indígena; y la aparición del escritor como una estrategia para poner en diálogo dos episodios, uniendo las dos historias a través de la voz del escritor.

"Ayer terminará mañana" pasó de ser un proyecto de grado a una novela publicada, gracias a la recomendación que Alejandra Jaramillo, jurado de la tesis presentada por Sepúlveda, hiciera del texto a Escarabajo editorial, quienes se encontraban en la búsqueda de escritores inéditos, una propuesta que a juicio del escritor no se ve todos los días considerando las dificultades que trae para cualquier autor publicar su primer libro.

Tras un proceso de edición de aproximadamente seis meses, se imprimió una **primera edición de 177 páginas y 500 ejemplares disponibles en librerías de la ciudad**. La publicación incluye algunos **grabados realizados por Theodore de Bry** en el siglo XVI, así como numeración de los 10 capítulos que la componen tanto en **numeración Muisca como Castellana** y un diseño bastante agradable para el lector.

El lanzamiento del libro tendrá lugar este **jueves 28 de septiembre en el Hotel B3 de Bogotá**, Av Carrera 15 \#88-36 a las 7:30 de la noche. A partir de su lanzamiento, tanto autor como editorial iniciarán el proceso de difusión en diferentes espacios culturales para presentarlo a todos los públicos posibles.

<iframe id="audio_21156337" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_21156337_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>
