Title: "Intromisión del Fiscal ha sido funesta para la JEP": Imelda Daza
Date: 2017-10-05 17:11
Category: Entrevistas, Paz
Tags: acuerdos de paz, Jurisdicción Especial de Paz
Slug: 47601-2
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/10/semana.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Revista Semana] 

###### [05 Oct 2017] 

Hasta el próximo lunes 10 de octubre se retomarán los debates de la JEP en las comisiones primeras conjuntas de Cámara y Senado, luego de las múltiples dilataciones que se realizaron el día de ayer durante el debate. De acuerdo con la integrante de Voces de Paz, Imelda Daza, el Fiscal General, quien hizo 15 proposiciones a la JEP está intentando torpedear **de forma "funesta" los acuerdos y modificarlos cambiando la esencia de los mismos. **

Para la integrante de Voces de Paz, la JEP tiene dos grandes enemigos, el primero de ellos es la “mezquindad de un sector de parlamentarios que dilatan de muchas maneras el proceso” y el otro es la “intromisión del Fiscal” que según Daza ha sido **“funesta” para el desarrollo e implementación de los Acuerdos de Paz**.

De acuerdo con Daza las intervenciones del Fiscal General, **demostraron su interés por querer juzgar a los miembros de las FARC**, sin que importe mucho el accionar de los demás actores del conflicto armado en Colombia y pasando por encima de lo que ya se acordó en La Habana. (Le puede interesar:["Los puntos gruesos de la JEP cruciales en la aprobación del articulado"](https://archivo.contagioradio.com/tres-puntos-gruesos-de-la-jep-que-serian-cruciales-en-la-aprobacion-del-articulado/))

“**El Fiscal pretende poner a la JEP como subordinada y ser él quien defina quien va a la cárcel**, quién es condenado, quién absuelto. Quiere implementar la justicia ordinaria por sobre la Jurisdicción Especial de Paz” asevero Daza.

### **Las proposiciones del Fiscal Néstor Humberto Martínez** 

El Fiscal General radicó 15 proposiciones a la JEP que giraban en torno a 3 temas: los testaferros, los disidentes de las FARC y los desertores de esta guerrilla. Sin embargo, Daza señaló que lo que pretende el Fiscal es **“partir del supuesto de que todos los ex combatientes son potenciales delincuentes comunes”**, además aseguró que al Fiscal no le importaría el accionar del resto de victimarios durante el conflicto armado sino “juzgar solamente a las FARC”.

De igual forma aseveró que con las proposiciones Néstor Humberto Martínez, estaría buscando ser el vigilante de la conducta de los ex combatientes **“quiere convertirse en el perseguidor eterno de quienes pasen por la Jurisdicción Especial de Paz**”. Además, Daza afirmó que el Fiscal también estaría intentando que se aplique la extradición para los excombatientes si infringen la ley después de ser aplicada la Jurisdicción Especial de Paz.

### **Las fuerzas de oposición al Fiscal en el Congreso** 

Frente a las posibilidades de que exista una fuerza de oposición en el Congreso a las proposiciones que realiza Néstor Humberto Martínez, Daza señaló que **“todos temen que algún día, el Fiscal los enjuicie”** razón por cual expresó que muy pocos se atreven a exponer sus reparos sobre los ajustes que propone el Fiscal. (Le puede interesar: ["Solo se ha implementado el 18% de los acuerdos de paz de La Habana"](https://archivo.contagioradio.com/solo-se-ha-cumplido-el-18-de-los-acuerdos-de-la-habana/))

No obstante, Daza señaló que hay una coalición conformada por congresistas del Polo, de la Alianza Verde y Voces de Paz, que están planteando estrategias para defender la JEP. De igual forma Daza afirmó que la JEP es la que **“garantiza la verdad que es lo primero que se destruyó cuando empezó la guerra”**, para de esta forma iniciar un proceso de reconciliación.

<iframe id="audio_21306400" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_21306400_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
