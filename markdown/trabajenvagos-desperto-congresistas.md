Title: #TrabajenVagos: La campaña que despertó a los congresistas
Date: 2019-04-03 16:23
Author: CtgAdm
Category: Entrevistas, Política
Tags: Congreso, eleccion, juvinao, vagos
Slug: trabajenvagos-desperto-congresistas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/5c084a21003a1-1.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: elcomercio.pe] 

###### [3 Mar 2019] 

\#TrabajenVagos fue la tendencia con la que se posicionó un grupo de ciudadanos que decidió hacer veeduría al trabajo de los Congresistas. En su primera acción, el grupo investigó la participación de los parlamentarios en las sesiones plenarias y en las de comisión y dio a conocer una lista de 30 integrantes que estarían faltando a su labor de forma sistemática. La investigación reveló nombres conocidos, y despertó la curiosidad de muchos pero, ¿qué viene para este movimiento ciudadano?

Catherine Juvinao, activista que integra esta veeduría, explicó que la iniciativa surgió de la 'tusa' por la Consulta Anticorrupción, que en su punto 5 pedía a los congresistas brindar información sobre su trabajo en el legislativo, por ejemplo, sobre los proyectos que votaban, aquellos que rechazaban y explicar sus ausencias. El sentido de este punto era que los colombianos pudieran evaluar en cada legislatura al político que habían elegido, y en caso de que estuviera inasistiendo, o actuando de forma contraria a sus promesas de campaña, no votar por él en los siguientes comicios.

Cuando la Consulta se quedó sin el umbral suficiente para que el Congreso legislara sobre sus puntos, un grupo de ciudadanos optó por tomar este trabajo por sus manos y revisando 35 mil páginas de documentos sobre asistencias a sesiones, se dieron cuenta que muchos parlamentarios no asisten a las plenarias, o asisten y posteriormente se retiran. (Le puede interesar: ["Hablan por las víctimas pero cuando van al Congreso no las escuchan"](https://archivo.contagioradio.com/victimas-congreso-escuchan/))

> La versión corta de este video se filtró antes de que lo publicáramos!! Aquí les compartimos el original con todos los detalles de nuestra investigación inédita al Congreso de la República. A cumplir la Constitución, señoras y señores. [@TrabajenVagosco](https://twitter.com/TrabajenVagosCo?ref_src=twsrc%5Etfw) <https://t.co/xGhZBLdfhY> [pic.twitter.com/BK6LRVlnyJ](https://t.co/BK6LRVlnyJ)
>
> — Catherine Juvinao C. (@CathyJuvinao) [2 de abril de 2019](https://twitter.com/CathyJuvinao/status/1113074833468874752?ref_src=twsrc%5Etfw)

Como explicó Juvinao, la Jurisprudencia del[Consejo de Estado](http://www.consejodeestado.gov.co/documentos/boletines/PDF/11001-03-15-000-2018-00318-00(PI).pdf)ha sido clara en que asistir a las sesiones es permanecer en las mismas, participar en la deliberación y votar; y cuando un congresista falta a las sesiones en más de seis ocasiones en una legislatura (hay dos legislaturas en el año), "la sanción debe ser la pérdida de investidura y la muerte política", lo que significaría prohibir que se lance nuevamente a un cargo de elección popular.

Conocida la lista de 30 congresistas, Juvinao denunció que algunos de ellos se reunieron para emprender acciones en su contra, y comenzaron a hostigarlos. Sin embargo, la Activista señaló que seguirán haciendo su trabajo de control ciudadano al legislativo, y afirmó que su siguiente paso será denunciar penalmente a quienes sistemáticamente han dejado de asistir a las sesiones en Senado y Cámara.

> Me cuentan que varios congresistas del ranking de ausentistas se reunieron ayer a ver ‘cómo me hacían pagar’ porque ‘ella no sabe con quiénes se está metiendo’. Que me van a demandar y a atacar mi vida personal. Señores: sí sé con quiénes me estoy metiendo y no les tengo miedo.
>
> — Catherine Juvinao C. (@CathyJuvinao) [3 de abril de 2019](https://twitter.com/CathyJuvinao/status/1113434950227853312?ref_src=twsrc%5Etfw)

### **Los 30 Congresistas que faltan a las sesiones:**

David Barguil, (Conservador, 73 fallas); Luis Eduardo Diaz (Cambio Radical, 60 fallas); Alfredo Ape (Conservador, 54 fallas); Cristóbal Rodríguez (Partido de la U, 59 fallas); Jaime Lozada (Conservador, 54 fallas); Keilyn González (Liberal, 48 fallas); Ciro Fernándes (Cambio Radical, 48 fallas); Sara Piedrahita (Partido de la U, 51 fallas); Carlos Osorio (Parido de la U, 46 fallas); Julián Bedoya (Liberal, 51 fallas); Jorge Pedraza (Conservador, 46 fallas); Juan Corzo (Conservador, 33 fallas); Olga Suarez (Conservador, 35 fallas); Albeiro Vanegas (Partido de la U, 40 fallas); Jack Housni (Liberal, 39 fallas); Javier Alvarez (Liberal, 36 fallas); Laureano Acuña (Conservador, 40 fallas); Luis Sierra (Conservador, 32 fallas); José Gnecco (Partido de la U, 38 fallas); Silvio Carrasquilla (Liberal, 37 fallas); Álvaro Ashton (Liberal, 28 fallas); Yahir Acuña (100% por Colombia, 10 fallas); Sandra Ortíz (Alianza Verde, 20 fallas); Arturo Char (Cambio Radical, 33 fallas); Germán Varón (Cambio Radical, 29 fallas); Nerthink Aguilar (Opción Ciudadana, 15 fallas); Juan Gómez (Conservador, 23 fallas); José Hernández (Conservador, 20 fallas); José Name (Partido de la U, 21 fallas); Eduardo Pulgar (Partido de la U, 24 fallas).

<p>
<script src="https://platform.twitter.com/widgets.js" async charset="utf-8"></script>
<iframe id="audio_34096888" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_34096888_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen">
</iframe>
</p>
###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
