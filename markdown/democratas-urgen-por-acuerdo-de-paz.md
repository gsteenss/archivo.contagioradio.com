Title: Demócratas urgen por acuerdo de paz en Colombia
Date: 2016-11-03 16:56
Category: Nacional, Paz
Tags: Acuerdo de paz de la habana, Demócratas en Estados Unidos, WOLA
Slug: democratas-urgen-por-acuerdo-de-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/Demócratas-de-Estados-Unidos-2.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: opi.7] 

###### [03 Nov 2016]

Un grupo de 11 congresistas demócratas de Estados Unidos escribieron una comunicación dirigida al secretario de Estado John Kerry, en la que afirman que es necesario que los ajustes del acuerdo de paz con las FARC se haga **rápidamente, mientras el cese al fuego bilateral se mantenga.**

**También insisten en que las víctimas deben seguir siendo el centro de las discusiones** y se mostraron preocupados por los nuevos actores, “*Los nuevos actores en estos diálogos (...) no han pasado por la misma experiencia transformativa, ni se han relacionado directamente con la cantidad de víctimas que presentaron sus historias y casos a los negociantes*” señalaron.

### **Las víctimas deben seguir en el centro del acuerdo según congresistas Demócratas** 

En la misiva los congresistas instaron tanto al secretario de Estado, como al enviado especial de Estados Unidos, a garantizar los avances presentes en el acuerdo, que contaron con la ayuda de las víctimas, “se logró identificar **las formas específicas en las que estos miembros de la sociedad colombiana sufrieron las décadas de conflicto armado**”. (Le puede interesar [Congresistas de EEUU instan a que las víctimas sigan estando en el centro de los acuerdos](https://archivo.contagioradio.com/congresistas-de-eeuu-instan-a-mantener-los-derechos-de-las-victimas-en-el-centro-del-proceso-de-paz/))

Según los congresistas, los **avances en materia de verdad, justicia restaurativa, reparación entre otros no tienen precedentes, son históricos** y se deben mantener en un nuevo acuerdo, por ello llamaron a delegados de ese país “a presionar a los actores en la mesa de negociaciones para proteger los avances que se han hecho en nombre de las víctimas...”

Respecto a la situación de Colombia los representantes demócratas afirman que “a Colombia le resultará muy difícil cumplir sus metas como país moderno sin un acuerdo de paz ni la inclusión social y **progreso económico que prometería la implementación**”.

A continuación anexamos el texto completo en Inglés. La traducción ofrecida en esta nota no es la traducción oficial de la comunicación de los congresistas, sino una ofrecida por WOLA.

[Colombia 2016 - Secy Kerry - Peace Process Post-Plebiscite](https://www.scribd.com/document/329919706/Colombia-2016-Secy-Kerry-Peace-Process-Post-Plebiscite#from_embed "View Colombia 2016 - Secy Kerry - Peace Process Post-Plebiscite on Scribd") by [Contagioradio](https://www.scribd.com/user/276652802/Contagioradio#from_embed "View Contagioradio's profile on Scribd") on Scribd

<iframe id="doc_50745" class="scribd_iframe_embed" src="https://www.scribd.com/embeds/329919706/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-MX3Z0bel28RFVSMBSoNf&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="0.7729220222793488"></iframe>  
 
