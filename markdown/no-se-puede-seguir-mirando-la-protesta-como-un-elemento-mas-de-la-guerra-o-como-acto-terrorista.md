Title: "No se puede seguir mirando la protesta como un elemento más de la guerra o como acto terrorista"
Date: 2015-10-28 11:52
Category: DDHH, Entrevistas
Tags: Armas Químicas, Armas Sonoras, Armas Taser, Cámara de Representantes, Debate de control político ESMAD, ESMAD, Polo Democrático, Represión de las protesta social, Violacion a derechos humanos en Colombia
Slug: no-se-puede-seguir-mirando-la-protesta-como-un-elemento-mas-de-la-guerra-o-como-acto-terrorista
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/10/ESMAD.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Ernesto Che M Jones 

<iframe src="http://www.ivoox.com/player_ek_9193763_2_1.html?data=mpamlZyad46ZmKiakp6Jd6KllZKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRiMbWwtnSjcnJb8Tjz9nf0dGPtNDghqigh6aouMrX0JDOjcvZssTd0NPS1ZDIqc2fpri6o6mRaZi3jqjc0NnFq8rjjLfOxs7Tb46ZmKialg%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

#### **[Alirio Uribe, Polo Democrático]** 

###### [28 Oct 2015 ] 

[Hoy se lleva a cabo en la Cámara de representantes el "Debate de Control Político Sobre el Uso de la Fuerza del ESMAD" en el que se pretende **cuestionar la existencia de mecanismos de represión** de la protesta social en un próximo escenario de posconflicto, así como insistir en la **prohibición del reclutamiento ilegal**.]

[Alirio Uribe, representante del Polo Democrático, asegura que el debate exigirá una **rendición de cuentas** por parte del Gobierno y de sus instituciones de la fuerza pública por la mala gestión en sus funciones, que ha provocado la **violación de derechos humanos en el marco de protestas y movilizaciones sociales** y que exige una mayor regulación en las normas de funcionamiento del ESMAD, junto con la **prohibición del uso de armas no convencionales**.]

El congresista asegura que el ESMAD está usando armas químicas, eléctricas y sonoras que están matando o les están causando graves daños a la gente en medio de las protestas y asegura que este tipo de situaciones no pueden seguirse presentando.

[De acuerdo con Uribe es necesaria una comisión de seguimiento a los abusos de la fuerza pública en distintas regiones del país, en muchas ocasiones favorables a instituciones bancarias que ordenan desalojos violentos, pues **no hay cifras coincidentes entre los organismos estatales** como la Fiscalía **y las organizaciones de derechos humanos** que trabajan en la documentación de estos casos.]

[Del paro agrario de 2013 se registran un total de **900 graves violaciones a los derechos humanos y 14 asesinatos** que según Uribe, dan cuenta del **“tratamiento de guerra del Estado a la protesta social”**. Situación que a nivel internacional también es problemática, pues con el uso de armas “aparentemente no letales” se están causando **graves lesiones personales** que en muchos casos resultan en **muertes** y que terminan por agudizar los niveles de violencia en los países.]

[Debido a que **“los ciudadanos no pueden delegar su deber constituyente de protestar”** en mecanismos represivos como el ESMAD, en este debate se emitirán un conjunto de recomendaciones para el tratamiento adecuado de la movilización social en Colombia.]
