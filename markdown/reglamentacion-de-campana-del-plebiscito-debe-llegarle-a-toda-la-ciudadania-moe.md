Title: "Reglamentación de campaña del plebiscito debe llegarle a toda la ciudadanía", MOE
Date: 2016-09-01 12:56
Category: Nacional, Paz
Tags: FARC, Gobierno, paz, Plebiscito
Slug: reglamentacion-de-campana-del-plebiscito-debe-llegarle-a-toda-la-ciudadania-moe
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/08/Plebiscito-e1470157319868.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Las 2 Orillas 

###### [1 Sep 2016] 

El Consejo Nacional Electoral dio a conocer las normas para reglamentar las campañas por el SI o por el NO, frente al [plebiscito](https://archivo.contagioradio.com/el-decreto-que-oficializa-el-plebiscito-para-la-paz/) que se realizará el próximo 2 de octubre, con el que se refrendarán los acuerdos pactados en La Habana entre el gobierno y la guerrilla de las FARC.

Se trata de la resolución **1733 de 2016 que consta de un documento de 20 páginas, donde se detallan las reglas del juego** de cara a la decisión que deberán tomar las colombianas y los colombianos, que tendrán desde hoy hasta el 15 de septiembre para inscribirse al ente electoral si quieren hacer parte de uno de los comités promotores del SI o el NO.

Además de las disposiciones sobre los comités promotores, también se detalló cómo se deberán desarrollar temas sobre la **financiación privada, incentivos, donaciones, servidores públicos, encuestas, publicidad, medios de comunicación y testigos electorales.**

Algunas de estas reglamentaciones, responden a las principales preocupaciones que había señalado la Misión de Observación Electoral, MOE, sobre temas como la **publicidad, financiación, divulgación y participación en política que eran las principales [preocupaciones frente a las garantías electorales del Plebiscito](https://archivo.contagioradio.com/las-preocupaciones-en-torno-al-plebiscito-por-la-paz/);** así mismo, habían presentado el mapa donde se evidencia que [243 municipios están en riesgo](https://archivo.contagioradio.com/243-municipios-en-riesgo-de-fraude-y-violencia-para-votacion-del-plebiscito/) por factores de violencia y fraude para la votación del 2 de octubre.

Camilo Mancera, coordinador jurídico de la MOE indica que **“Es un gran reto para todas las autoridades electorales en Colombia”,** pues se “busca ejercer un mayor control sobre los gastos de campaña. Además busca ampliar la participación al máximo, por lo que se disminuyen una cantidad de requisitos".

Con esta reglamentación se introducen figuras que en jornadas electorales anteriores no se habían visto. Desde la MOE se **resalta la responsabilidad que se endilga a los medios de comunicación y empresas publicitarias,** aunque afirman que falta ver cómo se realizará ese control y qué tan efectivo puede ser.

Así mismo, destacan que hay topes de financiación, pues con esta normativa lo que se hace es remitir a otras leyes que regulan los ingresos de los recursos a las distintas campañas, sin embargo la pregunta que le queda a la MOE tiene que ver con los recursos provenientes del Estado, pues de acuerdo con Mancera, el **CNE se queda corto frente a la reglamentación para la utilización de recursos estatales** que no son solo económicos, sino también que tiene que ver con el trabajo de los servidores públicos.

Por otra parte, señalaron que la organización electoral debe hacer un esfuerzo enorme para que esa reglamentación le llegue a los comités promotores pues **"faltan claridades y es importante que llegue esa información a todos los ciudadanos",** teniendo en cuenta que  uno de los principales riesgos es que "no hay seguridad jurídica", dice Camilo Mancera, quien agrega que “reglamentar es mucho más sencillo, verificar es mucho más complejo”.

Mientras tanto, **la MOE ya se prepara logísticamente con más de 2 mil observadores **que harán  presencia en todas las zonas de votación, y además solicitan que organismos internacionales hagan parte de esa vigilancia el próximo 2 de octubre, teniendo en cuenta las características de polarización que existe en el país.

<iframe id="doc_75060" class="scribd_iframe_embed" src="https://www.scribd.com/embeds/322753029/content?start_page=1&amp;view_mode=scroll&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="undefined"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)
