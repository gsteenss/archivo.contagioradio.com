Title: Las prácticas turísticas que más dañan los ecosistemas según Pueblos Indígenas de la Sierra
Date: 2020-01-24 17:47
Author: CtgAdm
Category: Ambiente, Nacional
Tags: Playa Blanca, pueblos indígenas
Slug: las-practicas-turisticas-que-mas-danan-los-ecosistemas-segun-pueblos-indigenas-de-la-sierra
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/01/Sierra-Nevada-de-Santa-Marta.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:heading {"align":"right","level":6,"textColor":"cyan-bluish-gray"} -->

###### Foto: Peter Chovanec {#foto-peter-chovanec .has-cyan-bluish-gray-color .has-text-color .has-text-align-right}

<!-- /wp:heading -->

<!-- wp:paragraph -->

Durante la temporada de final de año se ha evidenciado la alta cantidad de turistas que visitan destinos como Playa Blanca, en Islas del Rosario (Cartagena, Bolívar), o Santa Marta (Magdalena), generando una sobre explotación del territorio. Asimismo, se han presentado accidentes en el mar, sucesos ante los que algunas personas creen que debe replantearse el turismo en términos de sustentabilidad.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### **La naturaleza al servicio de la vida**

<!-- /wp:heading -->

<!-- wp:paragraph -->

Para **Gelbert Zapata**, integrante del equipo de recuperación de la SIerra Nevada de Santa Marta, es necesario hablar sobre la protección de la naturaleza para que esté al servicio de la vida. En ese sentido, lo más importante es que la población y las instituciones entiendan que la sobre explotación de la naturaleza es un problema que causamos los humanos.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En consecuencia, estamos llamados a tomar medidas para recuperar el equilibrio que deben tener los ecosistemas. De acuerdo a Zapata, una de las primeras medidas **tendría que reconocer cuáles espacios pueden ser abiertos al público y cuales no**, evitando la construcción de espolones para abrir playas.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En segundo lugar, señala que es necesario hablar sobre las **opciones económicas para las comunidades que habitan los territorios**, dado que muchas no tienen opciones distintas al turismo masivo, "que no respeta ni coopera con el ambiente". (Le puede interesar: ["El proyecto ecoturístico que redujo a cero la deforestación en Guaviare"](https://archivo.contagioradio.com/proyecto-ecoturistico-redujo-a-cero-deforestacion-en-guaviare/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Y en tercer lugar, Zapata afirma que al tiempo que se recupera la conciencia sobre los espacios y nuestro papel en ellos, se debe **permitir un descanso para que estos reparen los daños que generamos**. "Eso está demostrado en algunas medidas que hemos intentado transmitir desde la Sierra, más particular en el Parque Tayrona", sostiene.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### **La responsabilidad de los turistas**

<!-- /wp:heading -->

<!-- wp:paragraph -->

Zapata asegura que en las playas con mayor presencia de turistas hay prácticas muy nocivas que afectan el territorio como **dejar basura,** hecho que afecta su capacidad de recuperación. Adicionalmente, manifiesta que la llegada masiva de turistas es un problema, pues no **toma en cuenta las capacidades de los ecosistemas.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

De igual forma, cuestiona el uso de sustancias como drogas en los espacios, porque son actividades que generan deterioro al territorio y a la sociedad que lo habita. Sin embargo, Zapata dice que es posible dar un giro para repensar el turismo hacia una economía sostenible y que no sea "esa forma de explotación que acaba la vida".

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### **Las enseñanzas de**sde la Sierra Nevada

<!-- /wp:heading -->

<!-- wp:paragraph -->

Uno de los elementos señalados por Zapata que podrían retomarse en otras playas tiene que ver con permitir la recuperación de los espacios, para el caso del Tayrona, los pueblos de la Sierra han creado un calendario de cierres (en febrero, junio y octubre) para la realización de rituales "encaminados a proteger la vida natural". (Le puede interesar: ["Cinco claves para hacer turismo responsable en temporada de descanso"](https://archivo.contagioradio.com/turismo_responsable_vacaciones/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Según explica, "es un ejercicio que se desarrolla a través de las autoridades tradicionales, que son las que definen cómo hacerlo", y **sus resultados se ven en hechos pequeños, pero específicos:** Hay más vegetación en los caminos peatonales al interior del Parque, se nota el aumento en el caudal de ciertos ríos y es posible apreciar la llegada de especies animales que solían habitar la zona.

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78980} /-->
