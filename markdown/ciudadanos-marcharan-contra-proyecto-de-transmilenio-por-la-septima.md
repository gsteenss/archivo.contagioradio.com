Title: Anuncian acciones legales y movilización contra Transmilenio por la Séptima
Date: 2017-10-06 16:31
Category: Movilización, Nacional
Tags: Séptima, Transmilenio
Slug: ciudadanos-marcharan-contra-proyecto-de-transmilenio-por-la-septima
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/05/TransMilenio-Calle-100-RCN-9-copia-e1493944742569.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo Particular] 

###### [06 Oct 2017] 

De acuerdo con el concejal Manuel Sarmiento, del Polo Democrático ya están preparando medidas para frenar el cupo de endeudamiento de 2.8 billones de pesos, solicitados por Peñalosa para a la construcción de Transmilenio por esta vía. Algunas de ellas son generar una **acción de nulidad, mientras que los habitantes de la carrera séptima anunciaron movilización para el próximo 21 de octubre en rechazo de los planes del alcalde.**

La acción de nulidad sería interpuesta por el Partido Mira, considerando que el cupo de endeudamiento viola algunas disposiciones legales. Mientras que el Comité “Defendamos la Séptima” **convocó una movilización ciudadana para evitar que el proyecto de Transmilenio por esta vía tenga éxito**. (Le puede interesar: ["Sin estudios completos aprobarían cupo de endeudamiento para Transmilenio por la Séptima"](https://archivo.contagioradio.com/sin-estudios-completos-aprobaria-cupo-de-endeudamiento-para-transmilenio-por-la-carrera-septima/))

Sarmiento señaló que algunos de los riesgos de permitir que se construya por allí este sistema de transporte tienen que ver con que esta carrera es muy estrecha para construir una troncal “generando un deterioro del entorno urbano de la séptima”, además señaló que el **distrito tendrá que adquirir más de 300 predios lo que provoca que este proyecto sea mucho más costoso**.

Sumado a ello el concejal mencionó que para que el proyecto sea viable debe construirse deprimidos y aun así, significaría un daño a la salud y el ambiente, “la troncal que planea hacer Peñalosa funcionaría con buses diésel, que es un agente cancerígeno, afectando gravemente la salud de los ciudadanos”. (Le puede interesar: ["Revocatoria de Enrique Peñalosa podría realizarse en Noviembre"](https://archivo.contagioradio.com/revocatoria-a-enrique-penalosa-podria-realizarse-en-noviembre/))

Sin embargo, aunque se frene la aplicación del cupo, el dinero estaría disponible para los proyectos que la capital requiera, en ese sentido Sarmiento manifestó que los **2.8 billones de pesos se podrían invertir en el sector de movilidad, en la malla vial o usar más recursos para construir colegios, jardines infantiles** o suplir las necesidades grandes de la ciudad.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
