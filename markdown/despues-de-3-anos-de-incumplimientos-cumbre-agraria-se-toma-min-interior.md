Title: Después de 3 años de incumplimientos Cumbre Agraria se toma Min Interior
Date: 2017-03-09 13:14
Category: DDHH, Nacional
Tags: Asesinato a líderes, Cumbre Agraria Campesina Étnica y Popular
Slug: despues-de-3-anos-de-incumplimientos-cumbre-agraria-se-toma-min-interior
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/03/cumbre-asamblea-ministerio-1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Cumbre Agraria ] 

###### [09 Mar 2017] 

50 delegados de la Cumbre Agraria Étnica, Campesina y Popular, se declararon en asamblea permanente y desde ayer permanecen en las instalaciones del Ministerio del Interior exigiendo que **cesen los asesinatos hacía líderes y lideresas del movimiento social, que el gobierno tome medidas sobre el accionar paramilitar y que cumpla los acuerdos pactados durante el paro del 2014**.

De acuerdo con la Cumbre Agraria en lo corrido de este año han **“asesinado a 30 líderes y defensores de derechos humanos, entre ellos a 6 mujeres”**, que para Marylen Serna, integrante de Congreso de los Pueblos y vocera de Cumbre Agraria, son acciones que se suma a las amenazas selectivas, desplazamientos y presencia de grupos paramilitares en las regiones, que evidencia la falta de garantías para el ejercicio político.

Por lo cual están exigiéndole al gobierno que reconozca la existencia del paramilitarismo, que configure políticas para su desmonte, “rechazamos las declaraciones que han dado los funcionarios de la Fiscalía y que hacen que el **movimiento social se sienta muy vulnerable y en la necesidad de reclamarle al gobierno protección y garantías**” agregó Serna. Le puede interesar: ["120 defensores de derechos humanos asesinados en 14 meses en Colombia: Defensoría"](https://archivo.contagioradio.com/120-defensores-asesinados-defensoria/)

A estas exigencias se suman las peticiones que hace la Cumbre Agraria al gobierno de Santos, para que cumpla y avance con los acuerdos pactados durante el paro de 2014, como  la concertación en las propuestas sobre economía propia o campesina, en las políticas minero energéticas, en las políticas de sustitución de cultivos ilícitos, entre otras, que de acuerdo con la Cumbre no solo **“no han sido escuchadas” sino que se han aprobado leyes lesivas con el campesinado como la ley ZIDRES y el Código de Policía.**

“**A pesar de que tengamos actas y compromisos del gobierno, en el Congreso aprueban leyes que lo que buscan es despojar al campesino y hunden las que lo protegen** como el proyecto de ley de reconocimiento del campesinado como sujeto político” señaló Marylen Serna. Le puede interesar: ["Cumbre Agraria suspende conversaciones con Gobierno"](https://archivo.contagioradio.com/cumbre-agraria-suspende-negociaciones-con-gobierno/)

La delegación que se encuentra en el Ministerio del Interior ha denunciado la fuerte presencia del ESMAD, sin embargo, afirman que no se retiraran de este lugar hasta **tener una reunión con el presidente Santos y ministros de su gabinete, con la finalidad de hacer un balance exhaustivo de los avances en los compromisos del gobierno** y posteriormente establecer una ruta eficaz de cumplimiento.

[Encuentre el comunicado de prensa aquí](file:///C:/Users/SANDRA/Downloads/ComunicacionFinal-MUN-08-03-2017.pdf)

###### Reciba toda la información de Contagio Radio en [[su correo]
