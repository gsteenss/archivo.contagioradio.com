Title: "PND no es un plan de desarrollo sino de negocio", Angela María Robledo
Date: 2015-05-07 15:47
Author: CtgAdm
Category: Nacional, Política
Tags: Alianza Verde, Angela Maria Robledo, Cámara de Representantes, Cambio Radical, Comisión de Conciliación, Congreso, Efraín Cepeda. Partido Conservador, Germán Hoyos, Germán Navas, Juan Carlos Restrepo, la U, liberal, Luis Fernando Duque, micos del PND, Partido Verde, Plan Nacional de Desarrollo 2014 - 2018, PND, Polo Democrático Alternativo, Simón Gaviria
Slug: pnd-no-es-un-plan-de-desarrollo-sino-de-negocio-angela-maria-robledo
Status: published

##### Foto: [www.tnnpoliticas.com]

##### <iframe src="http://www.ivoox.com/player_ek_4462374_2_1.html?data=lZmjlJibeI6ZmKiak5qJd6Klk5KSmaiRdo6ZmKiakpKJe6ShkZKSmaiRaZOmsbOxjdPTb8bnjNrbjdXQpc%2BfxcqYxsrXpdPm0NHZ0ZDXrc%2FjjMnSjdPJq9DXytSSlJeJdqSfotPUx5KJe6ShpNTb1sbLrdCfs8bRy9SPcYarpJKh&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>  
[Ángela María Robledo, representante a la cámara del Partido Alianza Verde] 

“**El Plan Nacional de Desarrollo tiene problemas en su contenido y en el trámite, vicios de forma y fondo”** asegura  Ángela María Robledo, representante a la cámara del Partido Alianza Verde, cuyo partido analizará la posibilidad de demandar el PND 2014 – 2018, que fue aprobado este miércoles en plenaria del Senado con **57 votos a favor.**

De acuerdo a la congresista, **no hubo garantías reales para la oposición,** por ejemplo, el Partido Verde realizó alrededor de 180 propuestas para el PND, pero **“prácticamente ninguna fue acogida, yo vote en contra del PND  porque prácticamente nada fue acogido”**, asegura la representante, quien agrega que en las comisiones tres y cuatro, el polo y verdes prácticamente no tienen presencia “y la garantía debe estar ahí”, dice.

El representante a la cámara por el Polo Democrático Alternativo, Germán Navas, denuncio la forma como fue convocada la Comisión de Conciliación integrada los senadores Germán Hoyos, de la U; Efraín Cepeda, conservador; Luis Fernando Duque, liberal y Juan Carlos Restrepo, de Cambio Radical, quienes compararon los textos aprobados en Senado y Cámara, para presentar el documento final en aproximadamente 30 minutos, como lo dijo la congresista del Partido Verde, quien agrega que en el documento de la conciliación, tuvo que haber quedado especificado cómo votaron y quienes votaron, **“eso no es un acta de conciliación eso es una grosería,** y ahí estaba los ministros y Simón Gaviria presionando”.

Para la representante Robledo, el lunes y martes en Cámara, hubo garantías, pero al final del miércoles con la presentación de las nuevas proposiciones, que parecían titulares de artículos que se habían negado, se consolidaron vicios de procedimiento del PND. “Por la manera como se tramitó la conciliación si pudo haber grandes vicios constitucionales, es por eso que **la alianza verde analiza si demandan el Plan**”.

Así mismo, Robledo se refierió a la forma como fueron votados los artículos del PND, que estaba dividido en 6 estrategias, y aunque hubo momentos en los que se votó de manera más individual cada artículo, al inicio en Cámara de Representantes se aprobó por bloques de hasta 90 artículos donde uno no tenía nada que ver con el otro, **“si se quería acelerar, debió agruparse por materias, pero no fue así”,** por lo que era difícil votar pues en un mismo bloque podía haber artículos con los que estaban y no de acuerdo los congresistas, como le pasó a la representante.

“Cada gran sector tiene su articulito para preservar sus intereses, **no es un plan que haga realidad los tres ejes que dicen (paz, equidad, educación) es más un plan de negocios**”, concluye la congresista.
