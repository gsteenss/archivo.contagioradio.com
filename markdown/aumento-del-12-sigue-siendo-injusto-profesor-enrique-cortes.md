Title: "Aumento salarial del 12% sigue siendo injusto" profesor Enrique Cortés
Date: 2015-05-06 17:36
Author: CtgAdm
Category: Educación, Nacional
Tags: Bogotá, educacion, fecode, maestros, Ministerio de Educación, Santander
Slug: aumento-del-12-sigue-siendo-injusto-profesor-enrique-cortes
Status: published

##### Foto: Contagio Radio 

<iframe src="http://www.ivoox.com/player_ek_4457716_2_1.html?data=lZmimZyVeo6ZmKiak5mJd6KmmJKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRaZOmotrax9PYs4zYxtGYk5eJdpaf1M7U18qPt8rZz8ncjc7Srtbn1dSSlJePlNPjx8rg0dePic%2Fmytbix5KJe6ShpNTb1sbLrdCfs8bRy9SPcYarpJKh&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Enrique Cortés, profesor del Socorro, Santander] 

Enrique Cortes, profesor **del Socorro, Santander, caminaba hacia Bogotá como símbolo de apoyo a la movilización de maestros y maestras e**n todo el país, que al igual que él, exigían que su pliego de peticiones sea atendido por el gobierno nacional.

Debido a que se anunciaba un acercamiento con el gobierno, el docente llegó este miércoles a Bogotá.

Aunque asegura que **gran parte del magisterio está decepcionado por el preacuerdo** al que llegó Fecode con el Ministerio de Educación**,** Cortés, afirma que tiene fe porque hubo un avance y que se ganó una reivindicación, sin embargo, dice que el aumento salarial del **12% sigue siendo injusto.**

Cortés lleva 5 años ejerciendo como profesor rural, según su testimonio **gana \$1´200.000**, y aunque estuvo a punto de ser ascendido, así lo hubiera hecho ganaría \$300.000 más, teniendo en cuenta que **posee título profesional como ingeniero agrónomo y además una especialización en educación.**

El docente denuncia que la atención en salud es muy mala, el sistema de evaluación para profesores no es académico sino “muy subjetivo” y diseñado para que el gobierno no tenga que ascender a los maestros y por tal motivo, no deban pagarles más de \$1´200.000, en esa misma situación **están alrededor del 90% de los profesores del Socorro, Santander, según palabras del docente.**

“Somos el segundo hogar de los muchachos, ser maestro es un don que debemos defender y seguir apoyando”, concluye el profesor.
