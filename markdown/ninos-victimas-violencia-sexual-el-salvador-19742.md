Title: Niños y niñas las mayores víctimas de abuso sexual en El Salvador
Date: 2016-01-28 11:18
Category: El mundo, Otra Mirada
Tags: violaciones sexuales El Salvador, violencia sexual El Salvador
Slug: ninos-victimas-violencia-sexual-el-salvador-19742
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/01/Violacion-sexual-El-Salvador.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: El Nuevo Diario ] 

<iframe src="http://www.ivoox.com/player_ek_10230831_2_1.html?data=kpWflZWcd5Khhpywj5WcaZS1lpeah5yncZOhhpywj5WRaZi3jpWah5ynca%2Fdhqigh6eVs9Sf2pDby4qnd4a2ksbgjdHFt4zhwt7c1MrXb9eZpJiSo6nHuMrhwtiYxsqPpcPp1NSY1crcucLgjMqah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Silvia Juárez, ORMUSA] 

###### [27 Ene 2016] 

De acuerdo con el Instituto de Medicina Legal de El Salvador durante la última década se registraron **por lo menos 16 mil violaciones sexuales** en el país. En 97% de éstas las víctimas fueron mujeres, particularmente **niñas y adolescentes entre los 8 y los 15 años** de edad, en el restante 3% las víctimas fueron **niños entre los 0 y los 9 años**. Los márgenes de **impunidad de estos delitos fue de 92%** para las violaciones sexuales de mujeres y de 88% para las cometidas contra hombres.

Silvia Juárez, coordinadora del programa ‘Por una vida sin violencias’ de la Organización de Mujeres Salvadoreñas ORMUSA, asegura que **cerca del 68% de los casos fueron cometidos en el ámbito de la confianza**, es decir, por los padres, tíos o hermanos de las víctimas o personas allegadas a la familia y cuyas edades oscilaron entre los 35 y los 60 años.

“Muchos de los casos que llegan a Medicina Legal no necesariamente terminan en condenas”, afirma Juárez al referirse al alto grado de impunidad que continúa “lanzado un **mensaje de tolerancia social sobre el fenómeno**” y que llevó **en 2014 a que sólo el 20% de las denuncias hayan terminado en condenas**, mientras el 54% de las acusaciones interpuestas ante la Fiscalía fueron judicializadas y 32% archivadas.

Los factores determinantes en la impunidad que ha rodeado la comisión de estas violaciones sexuales tienen que ver la **minimización del delito** y con la **culpabilización y la cadena de venganza que ha recaído sobre las víctimas** una vez hacen la denuncia respectiva, asevera Juárez  e insiste en que “**todavía se usa un lenguaje completamente arcaico como el de la víctima provocadora** en una cultura patriarcal que reconoce los cuerpos de las niñas y las adolescentes como apropiables”.

El Salvador enfrenta un nuevo contexto de conflicto armado con grupos como Las Maras que están incidiendo notablemente en la violencia sexual contra las mujeres. Se han identificado mujeres con problemas psiquiátricos producto de violaciones sexuales sistemáticas por parte grupos criminales, “**hay territorios en los que las niñas no tienen la posibilidad de decir que no sí un pandillero decide que ella se convierta en su esclava sexual**, la comunidad misma no puede hacer nada, los padres no pueden hacer nada, no se pueden poner denuncias porque la familia corre peligro de ser asesinada”, refiere Juárez.

“Nos preocupa que en el imaginario de las niñas se construya una lógica de que no podemos decir que no a esas formas de violencia. **El actor ha cambiado y las estrategias del Estado deben cambiar**” asegura Juárez en relación con las medidas que las autoridades deben asumir para la prevención, sanción y reparación de este delito, pues **El Salvador es uno de los países que cuenta con una de las tasas de violación sexual más alta** de América.

###### Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU) ] 
