Title: “Estamos pidiendo acompañamiento humanitario porque no queremos abandonar nuestros territorios”
Date: 2015-11-26 17:55
Category: Comunidad, Nacional
Tags: Alto Sinú colombiano, Cabildo Mayor de Río Verde, Comisión Mixta de acompañamiento al pueblo indígena Embera Katío, Indígenas Emberá Katio
Slug: estamos-pidiendo-acompanamiento-humanitario-porque-no-queremos-abandonar-nuestros-territorios
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/11/PuebloIndigenaJaikerazabidec16.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Sandra Hernández 

<iframe src="http://www.ivoox.com/player_ek_9520844_2_1.html?data=mpqfkp2YeI6ZmKiakpuJd6KkmJKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRaaamhp2dh56nidTowtLc1ZDUrcXdxtPR0ZDFp9Dh0caSpZiJhpLVzs7S0NnTb8npzsbby9nFtsrjjNXc1NbZqYyhhpywj6jTstXVyM7cjbfFqMrjjJKSmaiReA%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Juan de Dios Domico] 

###### [26 Nov 2015 ]

[Tras seis años de inactividad se **reactiva la Comisión Mixta** **de acompañamiento al pueblo indígena Embera Katío**, integrada por Delegados de alto nivel del gobierno nacional, organizaciones de derechos humanos, garantes internacionales y autoridades del Cabildo Mayor de Río Verde y Río Sinú, dada la persistencia de la **violación de derechos humanos de este pueblo indígena del Alto Sinú**.]

[De acuerdo con Juan de Dios Domico, líder indígena, pese a que la dinámica del conflicto armado interno se ha modificado, la situación de violación a sus derechos humanos persiste, por lo que estas comunidades indígenas consideran **necesaria la reactivación de la Comisión**, que fue creada en junio de 2005 con el objetivo de hacer **seguimiento a las acciones de protección y prevención de sus derechos** económicos, sociales y culturales.]

[“Al día de hoy los jóvenes no tienen donde terminar su secundaria, no tenemos colegios, también **tenemos problemas de salud**... **como el territorio está minado** nosotros no podemos ejercer nuestra medicina tradicional... ni podemos ir como antes a la cacería o a la pesca, tenemos **problemas de seguridad alimentaria**... no hay libre circulación entre los resguardos indígenas, **nos sentimos abandonados por parte de las instituciones**”, asegura Dominico. ]

[Esta Comisión surgió tras la decisión de la CIDH de otorgar en el año 2001 medidas cautelares al **pueblo Embera Katío** y funcionó realizando visitas cada 2 meses al territorio de estas comunidades **víctimas de constantes violaciones** a sus derechos humanos, derivadas especialmente de la **construcción de la Represa Urrá** y de la **presencia de grupos armados** que atentaban en su contra.]

["Para el año 2001 las comunidades indígenas del resguardo del Alto Sinú presentaba muchos problemas por diferentes actores, como **amenazas, desplazamientos, asesinatos, restricciones en suministro de alimentos**... nosotros estábamos preocupados por el pueblo porque cuando una persona se desplaza del territorio a las ciudades, ahí se están acabando nuestros pueblos, ahí estamos perdiendo nuestra identidad", agrega el líder indígena. ]

[Pese a ello esta Comisión Mixta “se fue desgastando con el pasar de los años” y en mayo de 2009, el Pueblo Embera Katío del Alto Sinú y sus representantes plantearon la necesidad de evaluar su eficacia, revisar los métodos y resultados alcanzados y **proponer soluciones para brindar una verdadera protección a los derechos de sus comunidades**.]

[El **evento de reactivación se lleva a cabo en Montería** entre este 26 y 27 de noviembre y cuenta con la presencia de la Alta Consejería para los Derechos Humanos de la Presidencia, el Ministerio del Interior, la Procuraduría, la Defensoría, comunidades indígenas y diversas organizaciones sociales.]
