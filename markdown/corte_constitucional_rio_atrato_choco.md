Title: Corte Constitucional declara al Río Atrato como sujeto de derechos
Date: 2017-05-01 17:52
Category: Ambiente, Voces de la Tierra
Tags: Corte Constitucional, río atrato chocó
Slug: corte_constitucional_rio_atrato_choco
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/05/rio-atrato-e1493679094206.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: EFE 

###### [1 May 2017] 

La Corte Constitucional sigue dando pasos hacia la protección de la naturaleza. Uno de los más recientes fallos del alto tribunal dictamina que el Río Atrato debe ser sujeto de derechos, ordenando al Estado colombiano a protegerlo luego de años de olvido hacia el conjunto de la región, incluyendo sus afluentes y la situación de salubridad de la población.

“De acuerdo con esta interpretación, la especie humana es solo un evento más dentro de una larga cadena evolutiva que ha perdurado por miles de millones de años y, por tanto, **de ninguna manera es la dueña de las demás especies, de la biodiversidad ni de los recursos naturales, como tampoco del destino del planeta**. En consecuencia, esta teoría concibe **la naturaleza como un auténtico sujeto de derechos que deben ser reconocidos por los estados y ejercidos bajo la tutela de sus representantes legales,** verbigracia, por las comunidades que la habitan o que tienen una especial relación con ella”, dice la histórica sentencia de la Corte, que deja de ver a la naturaleza al servicio de las personas, para pasar a verla al mismo nivel de los seres humanos, como sujetos de derechos.

Esa declaratoria, se da tras **una demanda interpuesta en enero del 2015,** por la ONG “Tierra Digna”, en representación de diferentes comunidades de esa zona del país, contra la Presidencia de la República, los ministerios del Interior, Ambiente y Desarrollo Sostenible, Minas y Energía, Defensa Nacional, Salud y Protección Social, Agricultura, Vivienda, Ciudad y Territorio y Educación, así como contra las gobernaciones del Chocó y Antioquia, entre otras autoridades nacionales, regionales y locales. [(Gobierno debe atender estructuralmente la crisis humanitaria del Chocó)](https://archivo.contagioradio.com/gobierno-debe-atender-estructuralmente-crisis-humanitaria-del-choco/)

Una demanda que se había interpuesto, por los daños causados tanto por la explotación minera informal en la que se utiliza mercurio, la explotación forestal de manera legal que realizan empresas como Maderas de Darién y el olvido estatal. Ximena González, abogada de Tierra Digna, señala que no se han realizado las investigaciones pertinentes para determinar la causa de las muertes de los niñas y niños en el Chocó, pues siempre se le atribuye a los efectos del agua y alimentos contaminados con mercurio, “pero hay que analizar el problema a profundidad”, pues según ella, **“hay una problemática estructural y el gobierno ha abandonado históricamente la región”.**

### La contaminación del Atrato 

La situación del Río Atrato es caótica. La fiebre por el oro tiene agonizando al afluente, y por ende todo aquello que depende él, como las comunidades y la biodiversidad. Solo en 2014 la Secretaría de Salud de Lloró evidenció que 14 niñas y niños indígenas fallecieron. Además de acuerdo con los demandantes, **alrededor de 44 mil hectáreas de selva chocoana han sido devastadas por cuenta de la minería.**

Los niveles de mercurio en las aguas del río son absurdas, como explica la Corte, “Por lo general, solo el 10 % del mercurio agregado a un barril se combina con el oro para formar la amalgama, el restante 90 % sobra y se desecha en fuentes hídricas”. Pero hay otro dato impactante. El nivel de mercurio en el cuerpo de los integrantes de las comunidades es alarmante. Aunque **el promedio mínimo mundial considerado aceptable es de 0,5 puntos por millón, el nivel de mercurio en ellos y ellas es de 60 puntos por millón,** lo que produce diarreas, edemas pulmonares, dermatitis, fracaso reproductivo y malformaciones genéticas.

No obstante, aunque era evidente el daño que estaba generando la minería en los territorios del Chocó, la demanda también subrayaba, que a junio de 2014 se han celebrado contratos de concesión sobre aproximadamente 320.000 hectáreas, sumado a que existen múltiples solicitudes de contratos de concesión en trámite.

Hoy, gracias a la demanda interpuesta **los más de 40 mil kilómetros cuadrados del Atrato que atraviesan más de la mitad del Chocó deben ser protegidos y restaurados por el simple hecho de que albergan vida**. El agua "hace parte del núcleo esencial del derecho a la vida en condiciones dignas no sólo cuando está destinada al consumo humano, sino en tanto es parte esencial del medio ambiente y resulta necesaria para la vida de los múltiples organismos y especies que habitan el planeta”, manifiesta el alto tribunal.

### ¿Qué medidas se tomaron? 

Para atender con medidas reales la situación humanitaria y ambiental del departamento del Chocó, y puntualmente la del Río Atrato, la primera medida se dirige al **Ministerio de Ambiente que tiene máximo un año para implementar un plan de acción** **destinado a descontaminar** las fuentes hídricas, especialmente, la cuenca del Atrato. La misma entidad deberá comprometerse a reforestar las miles de hectáreas perdidas por la minería.

También ordena que las autoridades tendrán que **“neutralizar y erradicar definitivamente las actividades de minería ilegal** que se realicen no solo en el río Atrato y sus afluentes, sino también en el departamento de Chocó”, y  se deberá prohibir el tránsito de insumos combustible y sustancias químicas como mercurio y cianuro. Asimismo, **pidió un diagnóstico del estado de contaminación del agua y las personas.** Estos estudios tienen un plazo entre 3 y 9 meses para iniciar y terminar dichos análisis.

Finalmente, la Corte ordena que el Gobierno y las comunidades afectadas nombren dos **delegados encargados de hacer veeduría del proceso de restauración y protección.** Estas mismas personas, acompañadas por el Instituto Humboldt y WWF Colombia, deberán crear una comisión de guardianes del río en los próximos tres meses.

###### Reciba toda la información de Contagio Radio en [[su correo]
