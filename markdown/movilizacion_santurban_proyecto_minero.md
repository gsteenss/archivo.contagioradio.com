Title: Masiva marcha en Santander exige que se proteja el páramo de Santurbán
Date: 2017-10-05 16:58
Category: Movilización, Nacional
Tags: Mineria, Santander, santurbán
Slug: movilizacion_santurban_proyecto_minero
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/10/santurban.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Twitter @EduarOsorio1] 

###### [5 Oct 2017] 

Este 6 de octubre se ha declarado día cívico en Bucaramanga y varios municipios de Santander que saldrán a las calles en defensa del agua y **en rechazo a la posibilidad de que se otorgue licencia ambiental al proyecto minero de la empresa Minesa** que afectaría el Páramo de Santurbán.

Se trata de una movilización que la población organiza ya que la empresa tramita ante Autoridad Nacional de Licencias Ambientales, ANLA, la licencia ambiental que le daría vía libre al proyecto de explotación ‘Soto Norte’, muy cerca al Páramo de Santurbán. **Un proyecto que busca explotar durante 23 años nueve millones de onzas en concentrados de pirita y cobre.**

El objetivo es que el gobierno escuche el clamor de la ciudadanía y no se ortorgue dicha licencia ambiental que pondría en riesgo el suministro de agua para la capital del departamento de Santander y otros municipios cercanos.

### **El plan de manejo ambiental de Minesa** 

Actualmente la comunidad se encuentra estudiando el plan de manejo ambiental que ha entregado la empresa. "Son más de 5 mil páginas, pero lo que hasta el momento hemos podido ver es preocupante para la permanencia del páramo", explica Dadán Amaya, integrante del Comité por la Defensa del Páramo de Santurbán. "En Un lugar como Santander no se puede hacer la mina que quiere Minesa", señala.

De hecho, según explica Amaya, en la etapa de exploración que ya realizó la empresa, se han generado graves impactos al ecosistema. Por ejemplo, las perforaciones que han hecho las multinacionales **Eco Oro Minerals y Minesa han afectado la calidad del líquido vital, además se han perdido algunos nacimientos de agua** en fincas cercanas a los lugares donde se desarrolló esa primera fase de exploración.

Por tanto, la etapa de exploración podría conllevar a consecuencias peores, como considera el ambientalista. **"La construcción de los túneles que se necesitan van a afectar las fuentes hídricas **que alimentan el acueducto metropolitano como los son el Río Suratá, el Río de oro y Río Frío”.

Ante tal situación, que las comunidades cercanas a Santurbán ven como una amenaza para el agua y la naturaleza, en Bucaramanga la movilización empezará a las 2 de la tarde en la Puerta del Sol, para marchar hasta la Plaza Luis Carlos Galán. Asimismo habrá otras protestas en Piedecuesta, Girón, Florida Blanca, e incluso en Cúcuta.

<iframe id="audio_21306499" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_21306499_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
