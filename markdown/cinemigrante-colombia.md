Title: Llega la 7ma edición de CineMigrante Colombia
Date: 2017-11-12 15:00
Author: AdminContagio
Category: 24 Cuadros
Tags: Bogotá, Cine, Festivales
Slug: cinemigrante-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/11/CineMigrante.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto:Cinemigrante 

###### 12 Nov 2017 

Tras el éxito alcanzado en Argentina, Barcelona y Venecia en sus ediciones anteriores, regresa a Bogotá entre el 14 y el 19 de noviembre **CineMigrante Colombia**, una apuesta por la diversidad cultural, la migración y los Derechos Humanos desde el audiovisual, que en su 7mo año presentará en 21 funciones gratuitas 19 películas provenientes de diferentes países del mundo .

La muestra inicia el **martes 14 de noviembre en el Centro de Memoria Paz y Reconciliación** con el estreno nacional de la película danesa "La Permanence", de la directora Alice Diop y la presentación de **Burning Caravan**, agrupación conformada por músicos de varias nacionalidades, que regresa de su gira por México con un novedoso formato acústico.

**Actividades especiales**

La Cinemateca Distrital será la sede de las actividades alternas que comienzan el 15 de noviembre a las 5 pm con el conversatorio “**Derechos Humanos, migración, refugio y desplazamiento**” con Marco Romero, Director de la Consultoría para los Derechos Humanos y el desplazamiento – CODHES, como invitado.

El 16 de noviembre a las 11 de la mañana el turno es para el conversatorio “**El empoderamiento de las mujeres y la paz**”, con las invitadas María Eugenia Ramírez, vocera de la Cumbre Nacional de mujeres y paz, Nazly Blandón, Representante del proyecto Muñecas Uramberas, destacado en la Lista negra por El negro está de moda, Dagmar Lucía Hernández y Martha Ibeth Cardona Bonilla, Representantes de la Red de mujeres Chaparralunas por la paz y Andrea Solano, Artista plástica y representante del Proyecto Comunidad-es Arte, ejecutado conjuntamente por el Ministerio y la Secretaría de Cultura.

El mismo día a las 5 de la tarde se proyectan las películas "**Import**" y "**Tales of two who dreamt**", con la presencia de Ivonne Pineda, Asesora en prevención y asistencia a víctimas de trata de personas UNODC.

El 17 de noviembre a las 7pm se desarrollará la sección **Resistencias Creativas**, con películas como **La Belle At The Movies** (Congo, Reino Unido, Bélgica - 2015), **Wrinting On The City** (Irán - 2015), **The Television Wont Be Televised** (Senegal – 2015) y **Fonko** (Suecia – 2016). Estarán invitados Adriana Mena, bailarina y co-creadora del proyecto Afroproud destacado en la Lista negra por El negro está de moda, Leonard Rentería y Emma Nereida Montaño Potes, artistas de la Asociación Rostros urbanos de Buenaventura y Juan Manuel Torres “BOPS”, artista hip-hop de la agrupación Jotaika Bops y el proyecto Comunidad-es Arte, de esta manera a través de las músicas urbanas celebraremos un encuentro entre las culturas afro e indígenas que habitan nuestro territorio.

La edición cerrará con la participación de CineMigrante en el evento al aire libre el sábado 18 de noviembre en la Cancha 1 del Barrio Las Margaritas en la Localidad de Kennedy, del el cierre del proyecto Comunidad-es Arte y finalmente la función de clausura en la Casa Cultural Kilele en el Barrio Armenia en la Localidad de Teusaquillo.

[Programación CineMigrante 2017](https://www.scribd.com/document/363991604/Programacio-n-CineMigrante-2017#from_embed "View Programación CineMigrante 2017 on Scribd") by [Contagioradio](https://www.scribd.com/user/276652802/Contagioradio#from_embed "View Contagioradio's profile on Scribd") on Scribd

<iframe id="doc_86166" class="scribd_iframe_embed" title="Programación CineMigrante 2017" src="https://www.scribd.com/embeds/363991604/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-jULIeXS9vNTl4VUtWbLW&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="0.7068965517241379"></iframe>

###### Encuentre más información sobre Cine en [24 Cuadros](https://archivo.contagioradio.com/programas/24-cuadros/) y los sábados a partir de las 11 a.m. en Contagio Radio 

 
