Title: Las 3 condiciones de los estudiantes para levantar el paro nacional
Date: 2018-11-19 15:36
Author: AdminContagio
Category: Educación, Nacional
Tags: educacion, estudiantes, Mesa de negociación, Ministra de educación, Paro Nacional
Slug: condiciones-levantar-paro-nacional
Status: published

###### [Foto: Contagio Radio] 

###### [19 Nov 2018] 

Este lunes se reanuda la Mesa de Conversaciones entre el Gobierno Nacional y delegados de los estudiantes para buscar una salida a la crisis que atraviesa la educación superior en el país **tras una semana caracterizada por las movilizaciones, y la suspensión de la Mesa por parte de los jóvenes argumentando falta de garantías**.

Para **Valentina Ávila, integrante de la Unión Nacional de Estudiantes por la Educación Superior (UNEES)**, en esta ocasión esperan que a la Mesa de Negociación acudan representantes del Gobierno “con carácter decisorio para disponer del Presupuesto General de la Nación”, y con voluntad para encontrar soluciones a la crisis. Adicionalmente, que los acompañen garantes nacionales e internacionales que puedan verificar el cumplimiento de los acuerdos alcanzados en la Mesa.

Estas expectativas se resumen en lo que Ávila llamó **“voluntad política”** para solucionar la crisis puesto que, hasta el momento, lo que ha mostrado el Gobierno es su afán por que retorne la normalidad académica a las Instituciones de Educación Superior (IES) públicas, sin aumentar recursos que se necesitan para su funcionamiento. (Le puede interesar:["Con movilizaciones estudiantes presionarán al Gobierno para negociar"](https://archivo.contagioradio.com/movilizaciones-estudiantes/))

Aunque la estudiante reconoció que el paro nacional implica un costo grande para los estudiantes, sostuvo que todos los procesos asamblearios que integran la UNEES “están en posición de mantenerse firmes en el paro”, porque entienden que está coyuntura es difícil de conseguir y **saben que las ganancias para todo el pueblo colombiano pueden ser muy grandes**.

No obstante, Ávila reconoció que una de las exigencias en la Mesa de Negociación con el Gobierno serán las garantías para finalizar el periodo académico, así como para la movilización estudiantil y la protesta. (Le puede interesar: ["119 estudiantes fueron capturados tras la movilización estudiantil en Bogotá"](https://archivo.contagioradio.com/119-estudiantes-fueron-capturadas-tras-la-movilizacion-estudiantil-en-bogota/))

### **Las 3 condiciones para levantar el paro estudiantil** 

La integrante de la UNEES señaló que cuando los estudiantes logren “compromisos puntuales en la mesa, estrategias y tiempos determinados para verificar su cumplimiento y garantes nacionales e internacionales” que hagan dicha verificación, **convocarían a un Encuentro Nacional de Estudiantes por la Educación Superior, y allí, decidirían levantar el paro.**

Entre tanto, Ávila afirmó que las dinámicas de movilización seguirán, **la próxima será el 28 de noviembre,** en la que se encontrarán con el gremio transportador y realizarán una marcha para tomarse las principales vías del país. (Le puede interesar:["De Caquetá a Bogotá: 550 kilómetros caminando por la educación superior"](https://archivo.contagioradio.com/550-km-caminando-educacion-superior/))

[Reinicio Mesa de Diálogo Gobierno con estudiantes](https://www.scribd.com/document/393613564/Reinicio-Mesa-de-Dia-logo-Gobierno-con-estudiantes#from_embed "View Reinicio Mesa de Diálogo Gobierno con estudiantes on Scribd") by [Contagioradio](https://www.scribd.com/user/276652802/Contagioradio#from_embed "View Contagioradio's profile on Scribd") on Scribd

<iframe id="doc_19789" class="scribd_iframe_embed" title="Reinicio Mesa de Diálogo Gobierno con estudiantes" src="https://www.scribd.com/embeds/393613564/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-M04TKavxXCUDUyu4SsOV&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="0.7068965517241379"></iframe><iframe id="audio_30175577" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_30175577_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
