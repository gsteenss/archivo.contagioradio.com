Title: Empresa minera Cosargo amenaza el páramo Cruz Verde - Sumapaz
Date: 2015-02-13 17:08
Author: CtgAdm
Category: Ambiente, Nacional
Tags: Ambiente, Mineria, Sumapaz
Slug: empresa-minera-cosargo-amenaza-el-paramo-cruz-verde-sumapaz
Status: published

##### Foto: [fr.dbpedia.org]

<iframe src="http://www.ivoox.com/player_ek_4079768_2_1.html?data=lZWkm5yafI6ZmKiakpuJd6KlkpKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRkMKfxNTa0saJh5SZo5aSpZiJhaXVjKjc1cbWq9CftKbAjcbRqc%2FV28aYzsaPp9Di1Mrf2MbHrYa3lIqvldOPqMbgjJKSmaiRh9Di1cbUy9SPlsLYytSYj4qbh46o&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Lenin Pescador, proyecto [Corredor de Conservación de Paramos.]] 

**La compañía Cosargo S.A.S amenaza la conservación del páramo Cruz Verde- Sumapaz, ** del municipio de Choachi, Cundinamarca, debido a que la empresa realiza estudios en zonas del páramo, que están protegidas por el Estado como reserva natural, con el objetivo de **extraer arenisca**, un material usado para la construcción.

Los habitantes de la zona se encuentran preocupados, debido a que si la minería llega a los territorios del páramo Cruz Verde- Sumapaz, habrá graves **problemas de abastecimiento** de agua para las comunidades del municipio de Choachí y los lugares aledaños.

El proyecto minero se planea desarrollar aun cuando ese lugar **se encuentra protegido por el Estado como una zona ecológica**, debido a que se hace parte del Parque Nacional Natural Sumapaz  que abarca aproximadamente el **43% del complejo de paramos más grande del mundo,** de acuerdo a "Parques Nacionales Naturales de Colombia".

Las comunidades que viven cerca al páramo, denuncian que en la zona se ha empezado a ver maquinas de construcción, y su preocupación es que el proyecto minero sea un hecho. Por ese motivo,  “**quiere adoptar una serie de estrategias para hacer visible la problemática, tomar acciones legales y realizar movilizaciones”**, según afirmó Lenin Pescador, que hace parte del proyecto Corredor de Conservación de Paramos.

Las personas de la región se encuentran alerta y empezarán a realizar actividades culturales este fin de semana en la cabecera municipal, para visibilizar la intensión de la compañía Cosargo S.A.S, que quiere empezar actividades mineras, poniendo en riesgo la conservación del uno de los páramos más importantes del mundo, **generando otros impactos como alteraciones en el suelo y en las coberturas vegetales y contaminación.**
