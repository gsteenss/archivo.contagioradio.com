Title: Vuelve la amenaza de desalojo a 32 Familias en el Tamarindo
Date: 2015-05-11 07:34
Author: CtgAdm
Category: DDHH, Nacional
Tags: Asotracampo, Barranquilla, Derechos Humanos, Juan Martinez, Tamarindo
Slug: vuelve-la-amenaza-de-desalojo-a-32-familias-en-el-tamarindo
Status: published

##### Foto: contagioradio.com 

<iframe src="http://www.ivoox.com/player_ek_4468115_2_1.html?data=lZmjmpaVeY6ZmKiak5uJd6KlmpKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRmMLhwtfW0MnTaZO3jKfO1NfFstLpytHZw5KJe6ShpNTb1sbLrdCfs8bRy9SPcYarpJKh&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Juan Martinez, Asotracampo] 

El pasado 7 de Mayo se llevó a cabo una inspección ocular la predio "El tamarino" en el que habitan familias desplazadas integrantes de ASOTRACAMPO. A la diligencia asistieron funcionarios de la alcaldía y personería de Barranquilla, la abogada de los actores privados y reconocidos personajes como "el paramédico" y el "mono" quienes trabajan para las empresas interesadas en el desalojo de la comunidad.

Juan Martínez denuncia que la abogada de los empresarios ofreció tres millones de pesos a una familia para que le entregaran la parcela de forma inmediata, con los funcionarios de la alcaldía como testigos, y califica esta acción como un procedimiento ilegal, pues, la comunidad exige una propuesta concreta de reubicación y se están ofreciendo casas en lugar de tierras.

El representante de ASOTRACAMPO asegura que la propuesta por parte de la alcaldía es ofrecer casas para 24 familias, excluyendo a las 8 familias restantes que quedarían a la deriva, propuesta que no están dispuestos a aceptar pues, su necesidad es la reubicación en zonas rurales que les permita mantener su identidad campesina y vivir en condiciones dignas.

Hasta el momento no se ha llevado a cabo el desalojo de las 32 familias que permanecen en Tamarindo, Barranquilla, la inspección ocular quedó suspendida y el martes se reanudará a las 9 A.M., las partes implicadas darán su testimonio. Las familias desplazadas afirman no están en desacuerdo con entregar el predio, siempre y cuando se les garantice su derecho a vivir dignamente y como campesinos.
