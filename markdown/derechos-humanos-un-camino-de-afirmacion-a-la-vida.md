Title: Derechos Humanos: Un camino de afirmación de la vida
Date: 2020-12-10 20:06
Author: AdminContagio
Category: Actualidad, DDHH
Slug: derechos-humanos-un-camino-de-afirmacion-a-la-vida
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/12/derechos-humanos.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

Este 10 de noviembre se celebra el día internacional de los Derechos Humanos, una fecha que llega en un año en donde el mundo enfrenta u**no de los retos más grandes, qué va más allá de la pandemia y está basado en garantizar el presente y el futuro mediante un trabajo universal que busque la vida en dignidad para todas y todos.**

<!-- /wp:paragraph -->

<!-- wp:core-embed/instagram {"url":"https://www.instagram.com/p/CIoMuDNB0vO/","type":"rich","providerNameSlug":"instagram","className":""} -->

<figure class="wp-block-embed-instagram wp-block-embed is-type-rich is-provider-instagram">
<div class="wp-block-embed__wrapper">

https://www.instagram.com/p/CIoMuDNB0vO/

</div>

</figure>
<!-- /wp:core-embed/instagram -->

<!-- wp:paragraph -->

En este sentido, [Antonio Guterres](https://news.un.org/es/story/2020/12/1485382), secretario general de la ONU indicó que *"**la pandemia nos ha recordado 2 verdades sobre los derechos humanos, la primera de las cuales es que violarlos nos perjudica a todos**, y la segunda es que los Derechos Humanos son universales y protegen a todos, eso quiere decir que una respuesta eficaz a la pandemia debe basarse en la solidaridad y cooperación"*.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por tanto los esfuerzos divididos el autoritarismo y el nacionalismo *"no tiene sentido frente a una amenaza global y más aún cuando está tiene un mayor impacto en los grupos vulnerables*". ([Se agota el tiempo para dar vida a las curules de la paz advierten al Consejo de Estado](https://archivo.contagioradio.com/se-agota-tiempo-para-dar-vida-a-las-curules-de-paz-advierten-al-consejo-de-estado/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

**Hasan Dodwell, director de Justice for Colombia**, indicó que como organización de Derechos Humanos en la defensa de la vida y la paz de Colombia, han logrado qué **quiénes han sido enemigos en casa, refiriéndose a su país de origen, se unan en la defensa y la protección no sólo de los Derechos en Colombia sino de quienes diariamente ponen su vida en riesgo** por defenderlos y exigirlos.

<!-- /wp:paragraph -->

<!-- wp:quote -->

> ***"las cifras de líderes y lideresas asesinados durante el 2020 dan cuenta de que la situación en Colombia en materia de Derechos Humanos ha empeorado*"**. ([Comunidades indígenas enfrentan masacres, amenazas y asesinatos en menos de 24 de horas](https://archivo.contagioradio.com/comunidades-indigenas-enfrenta-masacres-amenazas-y-asesinatos-en-24-de-horas/))

<!-- /wp:quote -->

<!-- wp:paragraph -->

Según JFC, pese a que es difícil llevar a la comunidad internacional el tema de la situación en Colombia es algo en lo que ya han avanzado, pues ya se realizó, la primera semana de diciembre, en una de las dos cámaras del Parlamento Británico **un debate que a su vez contó con la participación de 10 miembros de la cámara quienes cuestionaron el compromiso de Duque y la Fiscalía con la paz de Colombia.**

<!-- /wp:paragraph -->

<!-- wp:paragraph {"fontSize":"small"} -->

*(Para profundizar mas este este le invitamos a leer la entrevista especial con Hasan Dodwell* [Parlamento británico cuestiona el compromiso de Duque y la Fiscalía con la paz](https://archivo.contagioradio.com/parlamento-britanico-cuestiona-el-compromiso-de-duque-y-la-fiscalia-con-la-paz/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Cabe resaltar que la presión por parte de la comunidad internacional se ha extendido también al Congreso de los Estados Unidos, el Parlamento Europeo, e incluso la Comisión Interamericana de Derechos Humanos quienes han reconocido su preocupación ante el panorama de violencia que se ha aumentado en el país. ([CIDH está al tanto de las barreras que pone el Estado para acceder a información sobre la verdad del conflicto](https://archivo.contagioradio.com/cidh-barreras-impuestas-por-instituciones-del-estado-para-que-conozca-verdad-del-conflicto/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Estas acciones *"**nunca van a ser suficientes las acciones en la defensa de la vida y más aún cuando hay personas que viven con miedo**, cuando hay constantes vulneraciones a los Derechos Humanos, cuando la vida de un líder social está bajo tanto riesgo en Colombia"*, es allí donde se da tanta frustración y tristeza al ver que las denuncias y alertas pasan en blanco.

<!-- /wp:paragraph -->

<!-- wp:quote -->

> *" Nosotros intentamos que desde las esferas políticas se generen acciones por Colombia, pero al final estas son cartas y palabras, que no tienen una incidencia inmediata mientras las personas siguen siendo asesinadas en sus territorios".*
>
> <cite>**Hasan Dodwell, Director de Justice for Colombia**</cite>

<!-- /wp:quote -->

<!-- wp:paragraph -->

Por tanto *"los esfuerzos no dejarán de ser suficientes, hasta que se detenga la tragedia de Derechos Humanos que vive el país en este momento"*. Así pues **una de las acciones que permite que estos esfuerzos se sigan reproduciendo y extendiendo es la movilización social**, *"un motor de esperanza para las comunidades, para que sientan que no están solos".*

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### "El Gobierno de Duque parece ser alérgico a los Derechos Humanos"

<!-- /wp:heading -->

<!-- wp:paragraph -->

**Diana Sánchez, directora de la Asociación Minga** y vocera de la Coordinación Colombia Europa- Estados Unidos, habló sobre las razones para el incremento de la violencia, las violaciones de Derechos Humanos y la crisis humanitaria en el país.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Para ella la respuesta a estás preguntas no tiene que ver con lo que algunos sectores señalan como un Estado fallido, que no puede hacer nada o que carezca de un marco de políticas públicas para actuar, prevenir y controlar toda esta violencia. Por el contrario las leyes, la constitución y el Acuerdo de Paz contienen herramientas suficientes para actuar de manera eficaz frente a estos problemas. ([Proteger a los Defensores de DD.HH, un desafío para las políticas públicas en Colombia](https://archivo.contagioradio.com/proteger-a-los-defensores-de-dd-hh-un-desafio-para-las-politicas-publicas-en-colombia/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

U**no de los puntos más importantes, para Sánchez, es la creación de 13 dispositivos políticos normativos, jurídicos e institucionales.** Uno de ellos es la Comisión Nacional de garantías de seguridad encabezada por el Presidente de la República, cuyo objetivo es crear políticas que permitan el desmonte total del paramilitarismo, *"sin embargo esta **Comisión no está funcionando y aparentemente sí lo hace, porque se convocan reuniones, y públicamente indican que todo funciona con normatividad,** pero en esencia la semilla de esa comisión no funciona porque el Gobierno le quitó este mandato y la desnaturalizó".*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

**Una muestra clara de ello es que no se está desmantelando el crimen organizado y por el contrario se ve un crecimiento exponencial de las disidencias de las FARC, grupos asociados al narcotráfico o el paramilitarismo,** *"el Gobierno indica que son ellos los que atacan a los líderes y lideresas, pero se desconocen los responsables y mucho menos cómo operan, y **es cuando uno se pregunta ¿Por qué no hay un desmantelamiento de estas estructuras?, y al contrario recibimos denuncias en donde pareciese que hay un pacto entre estas estructuras armadas y la fuerza pública"*** , ahí es donde hay un papel nefasto del Estado para prevenir.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Y enfatizó en la preocupación qué tienen diferentes organizaciones y defensores de Derechos Humanos sobre el papel de las entidades del Estado, que tendrían que controlar las acciones del Estado y por el contrario se estarían alineando con los intereses del gobierno. ([Colegio de abogados de New York advierte sobre presiones a la rama judicial en caso Uribe](https://archivo.contagioradio.com/colegio-de-abogados-de-new-york-advierte-sobre-presiones-a-la-rama-judicial-en-caso-uribe/))

<!-- /wp:paragraph -->

<!-- wp:quote -->

> *"Hemos notado con preocupación como el partido de gobierno a cooptado varios de los entes del Estado como la Procaduria, la Contraloría, la Fiscalía y recientemente la Defensoría del Pueblo, esto teniendo claro que **el enfoque del Centro Democrático va en contravía de la realización de los Derechos Humanos, atrapadas en estas políticas contra derechos".***
>
> <cite>**Diana Sánchez |Directora de la Asociación Minga**</cite>

<!-- /wp:quote -->

<!-- wp:heading {"level":3} -->

### La labor de defender Derechos Humanos en Colombia

<!-- /wp:heading -->

<!-- wp:paragraph -->

**Marino Córdoba, vocero de la Asociación Nacional de Afrocolombianos desplazados** (Afrodes ) y recientemente ganador del *reconocimiento "toda una vida"* del Premio Nacional a la Defensa de los Derechos Humanos, indica que alrededor del trabajo que hacen hombres y mujeres para defender la vida, el territorio y el ambiente **debe haber un reconocimiento de quiénes son, dónde están y lo que hacen. Esta actitud de reconocimiento podría salvar muchas vidas.**

<!-- /wp:paragraph -->

<!-- wp:quote -->

> *"para mí un defensor de Derechos Humanos es una persona que trabaja directamente enfocado en esta área y que aborda una situación que tiene una afectación en la comunidad de la que pertenece, en mi caso particular las comunidades víctimas del conflicto armado".*
>
> <cite>M Córdoba.</cite>

<!-- /wp:quote -->

<!-- wp:paragraph -->

Resaltó además que el trabajo de los líderes y lideresas es representar comunidades y transmitir un mensaje de sus necesidades, sus aspiraciones. Pero las respuestas a estos problemas y necesidades están en manos del Gobierno Nacional que tiene la responsabilidad otorgada por los mismos ciudadanos.

<!-- /wp:paragraph -->

<!-- wp:quote -->

> *"Como seres humanos tenemos una misión en esta tierra, la que asumimos los líderes y lideresas es defender la vida y defender los Derechos Humanos"*
>
> *"a los defensores de Derechos Humanos se les ha estigmatizado, se les persigue y se les asesina, limitando sus derechos fundamentales, en donde **al defender los derechos debemos eliminar muchas de nuestras libertades, una de ellas andar libremente por los territorios sin miedo a perder nuestras vidas".***
>
> <cite>**Marino Córdoba vocero de Afrodes** </cite>

<!-- /wp:quote -->

<!-- wp:paragraph -->

También señaló que esta labor no solamente en Colombia representa un riesgo, sino también una tarea para que el mensaje se extienda y esta labor se convierta en escuela generacional, ***"sí se apaga la luz de un defensor de Derechos Humanos vamos haber cientos más encendiendo una llama más fuerte, en exigencia de la vida, porque esta es una labor que se transmite esperanza y se realiza de por vida***".

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por último **Luz Marina Cuchumbe lideresa y víctima del conflicto armado de Inza, Cauca**, indicó este es el momento para que *"nos juntemos víctimas y victimarios, desde las ciudades y y las zonas rurales, para llevar al mundo el mensaje de que la paz es posible y es una realidad qué esperamos*".

<!-- /wp:paragraph -->

<!-- wp:core-embed/youtube {"url":"https://www.youtube.com/watch?v=06swgFi-sLg\u0026t=30s","type":"video","providerNameSlug":"youtube","className":"wp-embed-aspect-16-9 wp-has-aspect-ratio"} -->

<figure class="wp-block-embed-youtube wp-block-embed is-type-video is-provider-youtube wp-embed-aspect-16-9 wp-has-aspect-ratio">
<div class="wp-block-embed__wrapper">

https://www.youtube.com/watch?v=06swgFi-sLg&t=30s

</div>

<figcaption>
Conozca el mensaje completo de la lideresa del Cauca Luz marina Cuchumbe  

</figcaption>
</figure>
<!-- /wp:core-embed/youtube -->

<!-- wp:paragraph -->

*Escuche el programa completo en : [Otra Mirada 10 de diciembre día de los Derechos Humanos](https://www.facebook.com/contagioradio/videos/441052003588078/) [\#DDHHAfirmandoLaVida](https://twitter.com/hashtag/DDHHAfirmandoLaVida?src=hashtag_click)*

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
