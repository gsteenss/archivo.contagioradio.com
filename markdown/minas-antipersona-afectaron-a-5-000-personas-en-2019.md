Title: Minas antipersona afectaron a 5.000 personas en 2019
Date: 2020-11-18 18:33
Author: AdminContagio
Category: Actualidad
Tags: minas antipersona, Monitor de Minas Terrestres
Slug: minas-antipersona-afectaron-a-5-000-personas-en-2019
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/08/minas-colombia.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: imperiocrucial.com

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Según un informe publicado este jueves por [Monitor de Minas Terrestres](http://www.the-monitor.org/en-gb/reports/2020/landmine-monitor-2020.aspx), alrededor de 5.554 personas resultaron afectadas en 2019 por la explosión de minas antipersona en 55 países. Este es el quinto año consecutivo con un elevado número de víctimas registradas.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Las personas que murieron debido a estos explosivos fueron 2.170 y 3.300 resultaron heridas, mientras que de 27 víctimas se desconoce el estado de supervivencia. De la cifra total de afectados, 80% eran civiles y al menos 1.562 eran menores de edad, representando el 43% de todas las víctimas civiles.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Adicionalmente, un 85% de las personas que murieron o resultaron heridas por estos artefactos fueron hombres y niños menores de 18 años. (Le puede interesar: [Un adulto y dos menores indígenas, víctimas fatales de minas en Antioquia y Chocó](https://archivo.contagioradio.com/un-adulto-y-dos-menores-indigenas-victimas-fatales-de-minas-en-antioquia-y-choco/)) 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Los países más afectados fueron Afganistán, Pakistán, Mali, Ucrania, Yemen, Nigeria, Irak y **Colombia entra como el sexto país más afectado.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

**"El aumento significativo de las víctimas desde 2014 se debe sobre todo al elevado número de bajas en unos pocos países inmersos en intensos conflictos armados, en los que se hace un amplio uso e improvisado de las minas**", indica el informe.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Solo en Birmania (Myanmar), el ejército fue quien usó minas antipersona mientras que en Afganistán, Colombia, la India, Libia y Pakistán, grupos armados no estatales fueron quienes hicieron uso de estas.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

A pesar de lo preocupante de la situación, en 2019 al menos 156 km² de tierra fueron desminados y se destruyeron o eliminaron más de 123.000 minas, concentrados en su mayoría en Afganistán, Camboya, Croacia e Irak, quedaron libres de ellas.

<!-- /wp:paragraph -->

<!-- wp:heading -->

La pandemia ha entorpecido los operativos de desminado
------------------------------------------------------

<!-- /wp:heading -->

<!-- wp:paragraph -->

El informe señala que estas cifras resultan inferiores a las 6.897 víctimas del 2018 y las 9.439 del 2016. Sin embargo, superan en un 60% a las del año 2013 con el dato más bajo, cuando fueron 3.457.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Aunque es un cifra inferior, sigue siendo un número bastante elevado a pesar de los esfuerzos que se han venido realizando a nivel internacional con la firma del Tratado de Ottawa en 1997, que busca la prohibición del almacenamiento y producción de minas antipersona. (Le puede interesar: [Por mina antipersona muere indígena en Ituango](https://archivo.contagioradio.com/por-mina-antipersona-muere-indigena-en-ituango/))

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Por otro lado, el estudio señala que incluso con los logros de varios país como Chile, Reino Unido y Brasil por adelantar operativos en zonas minadas, la pandemia generada por la Covid-19 ha entorpecido las labores. Tal es el caso de Las Malvinas y otros 12 países en que se suspendieron temporalmente los operativos de desminado.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Y a pesar de la aparición del coronavirus, también se indica que ha habido una disminución en el financiamiento ya que los 45 países donantes y afectados aportaron aproximadamente 650,7 millones de dólares, 7 % menos que en 2018.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Además, por primera vez desde 2016 el apoyo internacional cayó por debajo de los 600 millones.

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
