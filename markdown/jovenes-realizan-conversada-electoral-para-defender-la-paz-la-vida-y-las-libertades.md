Title: Jóvenes realizan "conversada electoral" para defender la paz, la vida y las libertades
Date: 2018-06-08 12:55
Category: Paz, Política
Tags: conversada electoral, Elecciones presidenciales, Medellin, voto en blanco
Slug: jovenes-realizan-conversada-electoral-para-defender-la-paz-la-vida-y-las-libertades
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/06/DfLTEsPWsAA9drP-e1528480080994.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: La Oreja Roja ] 

###### [08 Jun 2018] 

Bajo el lema “argumentos con más pola y menos odios”, un grupo de jóvenes en Medellín, invitaron a la ciudadanía a participar de una **“conversada electoral”** con el fin dialogar sobre los comicios presidenciales de segunda vuelta, y despejar las dudas de quienes han manifestado su intención de votar en blanco o aún no han escogido su candidato.

Catalina Vásquez, promotora de la iniciativa, indicó que es necesario que la ciudadanía **“se salga de las pantallas y de las redes sociales”** para expresar los argumentos de manera personal y afirmó que la convocatoria se hizo alrededor de las preocupaciones comunes y la intención de expresar las preocupaciones sobre la coyuntura nacional.

### **Es necesario hacer una discusión política respetuosa** 

Vásquez enfatizó en que las preocupaciones como **la paz, la vida y las libertades**, unen al electorado por lo que se necesita saber “¿cómo se pueden proteger estas cosas fundamentales a través del voto?”. De igual forma, asegura que es importante discutir en torno al voto en blanco “de manera tranquila y respetuosa”. (Le puede interesar:["Claudia López y Antanas Mockus se unen a Gustavo Petro"](https://archivo.contagioradio.com/claudia-lopez-y-antanas-mockus-se-unen-a-gustavo-petro/))

Desde la iniciativa, creen que el voto en blanco **se puede reducir** y es “con las personas que tienen esa intención y que votaron en primera vuelta por Sergio Fajardo con los que nos queremos sentar a discutir”. Esto teniendo en cuenta que la coyuntura del país “pide que pensemos más allá de lo que diga nuestro líder político” y “debemos pensar qué es lo que más nos conviene”.

La convocatoria esta abierta para toda la ciudadanía y se va a realizar en el Café Teatro Pablo Tobón Uribe a partir de las **6:30 pm en Medellín.** Los jóvenes invitaron a que se haga un ejercicio de reflexión sobre lo que se quiere proteger. “Queremos hacer un alto en el camino para pensar en la oportunidad valiosa que tenemos en las manos”.

<iframe id="audio_26434737" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_26434737_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]

<div class="osd-sms-wrapper">

</div>
