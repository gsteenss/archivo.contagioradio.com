Title: Fiscalía será investigada por negligencia en el caso de Nicolás Neira
Date: 2017-10-11 15:52
Category: Judicial, Nacional
Tags: ESMAD, Fiscalía, Justicia Penal Militar, nicolas neira
Slug: fiscalia-sera-investigada-por-negligencia-en-el-caso-de-nicolas-neira
Status: published

###### [Foto: Sin Olvido] 

###### [11 Oct 2017] {#oct-2017 dir="auto"}

El Consejo Superior de la Judicatura ordenó que el caso lo lleve la justicia ordinaria, y compulsó copias para ordenar investigación por la tardanza y negligencia en el caso de Nicolás Neira, asesinado por el ESMAD cuando tenía 15 años. Los últimos dos fiscales que conocieron el caso, **Juan Carlos Molina y Luis Ernesto Orduz, serían algunos de los investigados.**

Según algunos analistas e integrantes de organanizaciones de Derechos Humanos, por esta situación, incluso, tendría que responder el fiscal general de la nación, Néstor Humberto Martínez como responsable de la actuación de sus fiscales.

“Llama profundamente la atención, el hecho de que pese a que la decisión de 8 de  octubre de 2007 se instó a la Fiscalía General de la Nación a imprimir celeridad al trámite de la actuación penal, proceda a radicar solicitud de audiencia preliminar para la formulación de imputación y **solicitud de imputación de medida de aseguramiento, el 9 de junio de 2017, es decir casi 10 años después”**, se lee en la decisión del Consejo Superior de la Judicatura.

### **Ya se habían solicitado investigaciones por dilaciones** 

El pasado 18 de agosto se envió el caso de Nicolás Neira para definición de conocimiento, por lo que se negó que fuera juzgado por la justicia penal militar. La audiencia de imputación de cargos a Néstor Julio Rodríguez Rúa, agente del ESMAD, por el homicidio ocurrido durante la marcha del primero de mayo de 2005, se postergó nuevamente para el 20 de octubre.

Ya el abogado de la familia de Nicolás, Pedro Mahecha, había solicitado a la Procuraduría que se entablara una investigación formal contra el juzgado en primera instancia, por haber enviado el caso del menor de edad a la justicia penal militar. Sin embargo el fallo en segunda instancia ordenó que se prosiguiera con el proceso en la justicia ordinaria. Lea tambien: [Caso de Nicolás Neira] [abre la posibilidad para esclarecer los crímenes del ESMAD](https://archivo.contagioradio.com/caso-de-nicolas-neira-abre-la-puerta-para-esclarecer-crimenes-del-esmad/)

### **Así va el caso de Nicolás** 

A la fecha por el asesinato de Nicolás Neira han sido destituidos e inhabilitados para la función publica por 10 años, el Subteniente Edgar Mauricio Fontal Cornejo y el teniente Julio César Torrijo Devia, capturado tiempo después con 103 kilos de cocaína. Sin embargo, Yuri Neira, padre del menor asegura que faltan por judicializar los mandos que encubrieron el asesinato y no han sido vinculados al proceso.

###### MOVICE/Contagio Radio\* 

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). {#reciba-toda-la-información-de-contagio-radio-ensu-correoo-escúchela-de-lunes-a-viernes-de-8-a-10-de-la-mañana-en-otra-mirada-porcontagio-radio. dir="auto"}
