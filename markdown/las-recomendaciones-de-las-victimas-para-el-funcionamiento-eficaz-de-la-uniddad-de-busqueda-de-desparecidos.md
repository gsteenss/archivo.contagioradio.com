Title: Las recomendaciones de las víctimas para la Unidad de Búsqueda de desparecidos
Date: 2017-08-29 13:37
Category: DDHH, Nacional
Tags: desparición forzada, Unidad de Busqueda, víctimas de crímenes de Estado
Slug: las-recomendaciones-de-las-victimas-para-el-funcionamiento-eficaz-de-la-uniddad-de-busqueda-de-desparecidos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/08/desaparecidos-e1504032110792.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: El Heraldo] 

###### [29 Ago 2017] 

En el marco del día Internacional de la Desaparición Forzada, organizaciones y familiares de víctimas realizarán actividades en todo el país para **exigirle al Estado colombiano que ratifique el tratado sobre Desaparición Forzada de las Naciones Unidas.** Igualmente, le harán recomendaciones al Comité de Escogencia para la creación del nuevo Comité de Búsqueda de Personas Desaparecidas.

A partir del 30 de agosto, y a lo largo de una semana, los familiares de las víctimas **le recordarán a la ciudadanía la importancia del esclarecimiento de los hechos de la guerra, de la verdad y las garantías de no repetición** como “piedra angular de una paz estable y duradera”. (Le puede interesar: ["El 99% de los casos de desaparición forzada están en la impunidad"](https://archivo.contagioradio.com/99-de-los-casos-de-desaparicion-forzada-estan-en-la-impunidad/))

### **Agenda de actividades de la Semana del Desaparecido en Colombia** 

**Bogotá:** Como primera actividad, le entregarán al presidente Juan Manuel Santos una carta pidiéndole que ratifique el Comité de Desaparición Forzada de las Naciones Unidas.

Además, En la capital del país el 30 de agosto se realizará el conversatorio “¿Dónde están?” en el auditorio Margarita González de la Universidad Nacional a partir de las 5:00pm. El 31 de agosto en el Centro de Memoria Paz y Reconciliación, la Fundación Nidia Erika Bautista hará el evento simbólico “Banderas y cometas por los detenidos y desaparecidos, especialmente mujeres hasta encontrarlos”.

**Medellín:** El 29 de agosto se realizará en las instalaciones de la Gobernación un taller y una obra llamada “Desde adentro” donde participarán funcionarios de la Personería, la Gobernación, la Alcaldía y la Unidad de Víctimas. Además, a las 9:00 am el 30 de agosto se realizará un plantón y una galería en la Plaza Botero sobre el tema de la desaparición forzada.

**Villavicencio:** El Movimiento de Víctimas de Crímenes de Estado capítulo Meta liderará en la Plaza Central un encuentro de flores en homenaje a las víctimas de desaparición forzada.

**Bucaramanga:** En la Universidad Industrial de Santander se realizará el 30 de agosto una exposición artística llamada “Millones de pasos hasta encontrarlos” de 9:00 am a 2:00 pm.

**Manizales:** El 30 de agosto se realizará la exposición “Prohibido olvidar” en el Palacio de Justicia a partir de las 4:00 pm y hasta las 6:00 pm.

**Cúcuta:** El Movice realizará la actividad “Ponte la camiseta del desaparecido” en la Biblioteca Central de Cúcuta de 8:00 am a 1:00 pm el 30 de agosto. Igualmente, allí el 31 de agosto la organización AFAES realizará un foro de 8:00 am a 1:00 pm.

**Sucre:** En la Plaza de Colosó del municipio de Colosó realizarán un plantón y una galería el 30 de agosto a las 2:00 pm.

**Barranquilla:** el 31 de agosto habrá un conversatorio en la universidad de Barranquilla sobre los desaparecidos.

### **Recomendaciones para la Unidad de Búsqueda de Desaparecidos** 

Luz Marina Hache, integrante del Movimiento de Víctimas de Crímenes de Estado, manifestó que, tanto los familiares como diferentes organizaciones sociales, han hecho recomendaciones para la escogencia del **director de la Unidad de Búsqueda de Personas Desaparecidas** que funcionará por 20 años.

Ella indicó que **“el director debe ser una persona que haya hecho acompañamiento, que conozca el tema de desaparición forzada** y que haya orientado a los familiares de desaparecidos en la búsqueda de las personas desparecidas”. Dijo que es importante que el director que se escoja tenga sensibilidad social frente a la problemática.

Finalmente, han exigido que la Unidad de Búsqueda de personas desaparecidas **no se haga solo en Bogotá** sino que tenga diferentes sedes en las regiones donde “hay un mayor número de personas desaparecidas”.

<iframe id="audio_20611048" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_20611048_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
