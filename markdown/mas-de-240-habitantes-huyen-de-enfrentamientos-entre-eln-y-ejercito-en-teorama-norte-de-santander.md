Title: Más de 240 habitantes huyen de enfrentamientos entre ELN y Ejército en Teorama, Norte de Santander
Date: 2019-03-04 12:43
Category: Comunidad, Entrevistas
Tags: Desplazamiento Forzoso, Norte de Santader, Norte de Santander
Slug: mas-de-240-habitantes-huyen-de-enfrentamientos-entre-eln-y-ejercito-en-teorama-norte-de-santander
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/01/desplazamiento.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Archivo 

###### 4 Mar 2019

**Ya son 245 los habitantes pertenecientes a 74 familias de las veredas Caño Seco y Piedras de Moler, en el municipio de Teorama, Norte de Santander** quienes desde finales de febrero se han desplazado  forzadamente de sus viviendas  debido a los combates entre la Fuerza Pública y la guerilla del ELN. Dentro de las personas hay más de cien menores de edad que tampoco han podido retornar a sus clases.

**Olga Quintero, integrante de la Asociación Campesina del Catatumbo (ASCAMCAT) ** indica que las familias afectadas se han trasladado hacia el corregimiento de San Pablo y fincas aledañas. De igual forma, afirma que esta situación se ha incrementado desde el año pasado, debido los enfrentamientos con el EPL y el ELN. Lo que además de generar inseguridad en la población, ha causado un impacto negativo  sobre la economía de las comunidades, pues al abandonar sus viviendas los cultivos quedan abandonados.

Quintero explica que producto del fuego cruzado, adicionalmente  se generó un daño en las tuberías del **oleoducto Caño Limón, ** ocasionando que una de las fuentes de agua del corregimiento fuera contaminada, afectando el abastecimiento y consumo de aproximadamente **100 familias. **

Asimismo, la integrante de ASCAMCAT indica que en la vereda La Cristalina, hubo fuertes enfrentamientos con uso de bombas y morteros, "hace mucho tiempo no se veía esta situación en la zona, lo que quiere decir y reafirma que es necesaria la paz",   además **agrega que en la región del Catatumbo hay cerca de 15.000 unidades del Ejército** que ocasiona un aumento en los enfrentamientos. **[(Le puede interesar José Arquímedes Moreno, tercer líder social asesinado en el Catatumbo en 2019) ](https://archivo.contagioradio.com/asesinan-lider-social-jose-antonio-navas-catatumbo/)**

Finalmente, Olga Quintero hace un llamado al Gobierno para que reanude las conversaciones con el ELN e  implemente el Acuerdo de Paz firmado con el actual partido Farc, "es necesario que el Gobierno tome cartas sobre el asunto, pero no a través de la bota militar sino en inversión social, en la implementación del acuerdo y en otras salidas, necesitamos un cambio para la región" afirma la lideresa de ASCAMCAT.

<iframe id="audio_33048665" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_33048665_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
