Title: El movimiento juvenil latinoamericano realizará Brigada hacia El Mango, Cauca
Date: 2015-07-29 16:09
Category: eventos, Nacional
Tags: argela, Cauca, cese, fuego, jovenes, juco juventud rebelde, mango, paz
Slug: el-movimiento-juvenil-latinoamericano-realizara-brigada-hacia-el-mango-cauca
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/07/Joevenes-y-paz.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: [magdaleneandoejecutivo]

######  

<iframe src="http://www.ivoox.com/player_ek_5529465_2_1.html?data=lpqfm5maeY6ZmKiakp6Jd6KklZKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRic2fztTjy9LNqc%2Fo0JDX19vJssrgjNHO1s7Ss8LhxtfWxcbSs4zmxsbZy9%2FFtoa3lIquk5Cmtsrbwsmah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Deisy Aparicio, Juventud Comunista] 

###### [29 jul 2015] 

**Jóvenes de Colombia, Nicaragua, El Salvador, Argentina, España y Portugal, se darán cita del 15 al 17 de agosto en el corregimiento de El Mango, Cauca,** para compartir reflexiones en torno a la paz en Colombia.

Para los jóvenes convocantes, **más allá del miedo y la zozobra que pudieran generar 8 años del gobierno de Álvaro Uribe, provocó un despertar consciente en la población juvenil** frente a los impactos de la guerra, lo que se podría evidenciar en las iniciativas nacionales y territoriales en torno a la paz, y el dinamismo juvenil al momento de encarar la pelea por la desmilitarización de la vida civil.

"El despertar del movimiento juvenil no solo se da en Colombia, sino en todo el continente americano, **como el movimiento juvenil en Chile, en Brasil y en centro América, que evidencian la necesidad de transformaciones profundas**", afirma Deisy Aparicio, de la Juventud Comunista Colombiana.

Las y los jóvenes rurales del Cauca han participado activamente en esta convocatoria, esperando que se pueda constatar la violación a los Derechos Humanos de que viene siendo objeto esta población, tanto en el Mango como en Buenos Aires, Guapi, Cajibío y demás corregimientos y municipios; generar apoyo a la exigencia de un cese al fuego de hostilidades por parte de los actores en conflicto; y apoyo al proceso de paz que se adelanta entre la insurgencia de las FARC y el gobierno colombiano.

Para más información, las personas interesadas pueden comunicarse al correo electrónico 2brigadajuvenilpazcolombia@gmail.com , y a los números de teléfono 0057(1) 2880613 , 0057- 310-8025788 , 316-2653762 , 300-3052223.
