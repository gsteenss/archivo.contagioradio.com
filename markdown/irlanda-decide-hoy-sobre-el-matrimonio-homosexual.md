Title: Irlanda decide hoy sobre el matrimonio homosexual
Date: 2015-05-22 14:24
Author: CtgAdm
Category: LGBTI, Nacional
Tags: Colectivos LGBT Irlanda, Referéndum por el matrimonio gay en Irlanda
Slug: irlanda-decide-hoy-sobre-el-matrimonio-homosexual
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/05/d05a1b38a1e33ac6dd0d08681c01cf05.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/05/d05a1b38a1e33ac6dd0d08681c01cf05.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto:Elconfidencial.com 

**Irlanda se ha convertido en el primer país del mundo que organiza un referéndum sobre la legalización del matrimonio homosexual**, o entre personas del mismo sexo.

El Referéndum ha sido convocado después de que la **Convención Constitucional Nacional en 2012 recomendara al parlamento realizar un referéndum** para consultar al pueblo Irlandés sobre el derecho de las parejas del mismo sexo a contraer matrimonio.

En total **3.2 millones de Irlandeses** de los 4.5 millones están llamados a votar a través de unas papeletas en las que se puede leer si están de acuerdo en que la **Constitución se cambie a favor de que : “Pueden contraer matrimonio de acuerdo con la ley dos personas sin distinción de su sexo”.**

En la misma votación también están llamados a decidir sobre si un **candidato a presidente** puede acceder al cargo con una **edad mínima; **en vez de** 35 años, con 21.**

**Irlanda** es un Estado **tradicionalmente católico** donde hasta **1993 era penado ser homosexual**. Fue entonces cuando el Tribunal Europeo de Derechos Humanos ilegalizó dichas leyes, que fueron derogadas por el propio parlamento. De esta manera, se convirtió en el **último país europeo en despenalizar la homosexualidad.**

Todas las **encuestas vaticinan un "Sí"**, pero siempre cabe esperar el voto rural que mayoritariamente es más tradicional y por tanto partidario del "No".

A pesar de todo en el país los homosexuales cuentan con **derecho a la adopción igualitaria, y conjunta, al acceso igualitario, a técnicas de reproducción asistida, a la unión civil**, el acceso libre al servicio militar o las prestaciones de bienes y servicios que entran a formar **parte de las medidas en contra de la discriminación.**

Si el referéndum le da la razón al **"Sí", el país se convertiría en uno de los estados del mundo donde más derechos tienen los colectivos LGBTI**.
