Title: Las contradicciones en nueva condena contra Lula
Date: 2019-02-07 12:32
Author: AdminContagio
Category: El mundo, Otra Mirada
Tags: Lava Jato, Lula da Silva
Slug: contradicciones-nueva-condena-lula
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/02/Luiz-Inacio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: La prensa 

###### 7 Feb 2019 

Tras la nueva **condena a 12 años y 11 meses de prisión**, emitida este miércoles en contra del ex presidente brasileño Luiz Inácio Lula da Silva por los delitos de corrupción y lavado de dinero, han surgido **nuevos interrogantes sobre la consistencia de las pruebas emitidas desde la parte acusatoria.**

En el proceso conocido como "**casa de campo Atibaia**", adelantado en el marco de la Operación Lava Jato, se consideró que **Lula da Silva se benefició con coimas** a través de reformas a una propiedad ubicada en el municipio de Atibaia, localizado en el estado de Sao Paulo, **acusaciones que la defensa del ex presidente desestima y buscará apelar la decisión**.

En las consideraciones de la jueza **Gabriela Hardt**, quien relevó en el caso al hoy ministro de justicia de Bolsonaro Sergio Moro, la información aportada es suficiente para comprobar que existió un crimen de corrupción pasiva, lo que **legalmente debería demostrarse al identificar el beneficio conseguido por un funcionario público y precisar la ejecución de un "acto oficial"** en su consecución, evidencias que no se presentan en la determinación.

El sustento utilizado por la parte acusatoria, se afirma que **Lula habría actuado para beneficiar a las empresas contratistas OAS, Schahin y Odebrecht**, por una serie de reformas que las compañías habrían financiado a casa de campo a cambio de contratos con la estatal Petrobras. La acusación se refiere a una suma de **920 mil reales** que habría recibido Lula por las reformas hechas a la propiedad.

Lo contradictorio es que la sentencia **no identifica a Lula como propietario de la casa de campo, sino al empresario Fernando Bittar**, argumento que fue presentado por la defensa, alegando que **no existe algún tipo de beneficio para el dirigente del Partido de los trabajadores** (PT), que no pasaba de conocer y visitar el lugar en ocasionalmente.

A través de un comunicado **el PT ha calificado la decisión como un "acoso judicial",** asegurando que se han presentado injusticias y atropellos durante el proceso. El movimiento asegura que **Lula es un preso político, y convoca a nuevas movilizaciones en diferentes ciudades del país para este jueves**.

###### \*Con información de Brasil de Fato 

###### Reciba toda la información de Contagio Radio en [[su correo]
