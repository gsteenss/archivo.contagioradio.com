Title: Crisis sanitaria en Baudó deja 5 niños muertos y 187 Wounaan enfermos
Date: 2020-02-04 16:23
Author: CtgAdm
Category: Comunidad, Nacional
Tags: Bajo Baudó, Chocó, Crisis Sanitaria, Wounaan
Slug: crisis-sanitaria-baudo-wounaan
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/02/Wounaan-Chocó.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: Wounaan Min Salud

<!-- /wp:paragraph -->

<!-- wp:html -->

<figure class="wp-block-audio">
<audio controls src="https://co.ivoox.com/es/orlando-moya-sobre-epidemia-comunidad-wounan_md_47316417_wp_1.mp3">
</audio>
  

<figcaption>
Entrevista &gt; Orlando Moya | Representante legal del Consejo de Autoridades del Pueblo Wounaan

</figcaption>
</figure>
<!-- /wp:html -->

<!-- wp:paragraph {"align":"justify"} -->

Desde el pasado 23 de enero, **los 513 habitantes de la étnia Wounaan de Buenavista en Bajo Baudó, Chocó** han alertado sobre la existencia de una enfermedad que ha causado el fallecimiento de 5 niños y síntomas como diarrea, vómito y deshidratación en al menos otros 100 menores de edad. Tras la visita del Ministerio de Salud que solo acudió al lugar una semana después de las advertencias hechas por la comunidades, la cartera descartó la existencia de una epidemia activa, pese a que la enfermedad continúa propagándose.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Según Orlando Moya, representante legal del Consejo de Autoridades del Pueblo Wounaan, **hasta la semana pasada se dio un reporte de 187 niños atendidos y cerca de 300 personas examinadas** entre las comunidades de Buenavista Unión Pitalito y Puerto Piña, de ellos, tres permanecen en grave estado de salud.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### El abandono estatal la enfermedad que aqueja a comunidades Wounaan

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Aunque el ministro de Salud (e), Iván González,[visitó el corregimiento](https://twitter.com/MinSaludCol/status/1224462790360010753?ref_src=twsrc%5Etfw%7Ctwcamp%5Etweetembed%7Ctwterm%5E1224462790360010753&ref_url=https%3A%2F%2Fwww.semana.com%2Fnacion%2Farticulo%2Fno-hay-epidemia-minsalud-confirma-las-causas-de-la-muerte-de-cinco-ninos-en-buenavista%2F650546) y reconoció que han fallecido cuatro menores en enero de este año y uno en diciembre del 2020, afirmó que su muerte no se debió a una epidemia, al respecto Moya resalta que el Gobierno debe tener en cuenta el contexto departamental y el precario sistema de salud del Chocó. [(Lea también: Sin garantías del Estado, comunidad indígena de Pichimá Quebrada regresa a su territorio)](https://archivo.contagioradio.com/pichima-quebrada-retorno/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Resalta que no se paga puntual a los enfermeros y que el centro hospitalario ubicado en la cabecera municipal se encuentra a una distancia de seis horas de Buenavista, además que a veces no existen los medicamentos suficientes ni los programas de salud diferenciales para la población indígena.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

El Ministerio de Salud se comprometió a reforzar el puesto de salud de Buenavista, a mitigar la enfermedad por medio de filtros de agua e implementar una ambulancia para que se pueda transportar a las personas a la cabecera municipal. Pese a estas medidas, Moya expresó que **el Gobierno debe hacer un estudio más serio y profundo, además de garantizar el acceso al agua potable, saneamiento básico y establecer un plan de contingencia para que así "no transcurran más de una semana antes de que lleguen las autoridades y la gente siga muriendo"**.

<!-- /wp:paragraph -->

<!-- wp:quote -->

> "Lo cierto es que no se ha podido controlar, diariamente se presentan niños con esos síntomas" - Orlando Moya
>
> </p>

<!-- /wp:quote -->

<!-- wp:paragraph {"align":"justify"} -->

Sobre la enfermedad, Carlos Tirso Murillo, secretario de Salud de Chocó expresó que los niños han presentado **"una infección respiratoria aguda y enfermedad diarreica aguda"**, razón por la que se han tomado muestras del agua de la zona para analizarla y encontrar algún resultado relevante.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Independientemente del resultado que arrojen estas pruebas, Moya alerta que en otras comunidades incluidas la Embera Wounaan Katio Chamí y la población de Juradó, también ha presentado síntomas similares, muchos de ellos derivados de enfermedades que provienen, según relata Moya de las grandes ciudades, **"afectando a las poblaciones por medio de las fumigaciones o la contaminación del agua"** que las poblaciones consumen directamente de afluentes naturales.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Chocó, un departamento propenso a las enfermedades

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

El problema de salud pública en el departamento, las barreras de acceso para la población indígena, la ausencia de cobertura de entidades promotora de salud y la desnutrición infantil se ven reflejados en casos de enfermedades como la malaria que pasó de 19.520 casos a 28.853 de 2015 a 2016 o la enfermedad diarreica aguda que pasó de 12.801 a 20.513 en la misma época.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Pero no se trata solo de enfermedades físicas, según la encuesta nacional de Salud Mental, el Pacífico es la zona que presenta una mayor prevalencia de trastornos mentales, derivados de factores ligados a la violencia y la pobreza extrema, tan solo en el Chocó el 80% de la población ha sufrido de desplazamiento, Factores que aumentan la posibilidad de desarrollar trastornos como ansiedad, pánico, depresión, trastorno afectivo bipolar y esquizofrenia.

<!-- /wp:paragraph -->
