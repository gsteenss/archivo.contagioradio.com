Title: ¿Qué dicen los resultados de la consulta interna de los partidos?
Date: 2015-04-20 10:23
Author: CtgAdm
Category: Nacional, Política
Tags: alirio uribe, Centro Democrático, Clara López, conservador, eleccion, elecciones, Iván Cepeda, liberal, oscar ivan zuluaga, partidos, paz, polo, voto
Slug: que-dicen-los-resultados-de-la-consulta-interna-de-los-partidos
Status: published

###### Foto: Registraduría 

Con la participación de cerca de 1 millón de personas, se llevaron a cabo este domingo 19 de abril, consultas internas en los principales partidos políticos colombianos.

<div>

No todas las consultas fueron iguales. El Partido Conservados, el Centro Democrático y el Partido Liberal, optaron por realizar consultas para la postulación de candidatos y candidatas a las elecciones de alcaldes, concejales y Juntas de Acción Comunal del próximo mes de octubre, y sólo en algunas regiones del país.

El Polo Democrático Alternativo, por otra parte, realizó una consulta identificando listas a nivel nacional, territorial (o departamental), y local. El principal objetivo del Polo consistió en generar un mecanismo para la elección de las 766 personas que participarán como delegados y delegadas en su IV Congreso, espacio de deliberación y decisión que se llevará a cabo en el mes de mayo, y en el cual se conformará la nueva dirección nacional del Partido, así como las lineas de acción para los próximos años.

Mientras algunas voces desde el gobierno, como la del MinInterior Juan Fernando Cristo, señalaron que el proceso de consulta es demasiado costoso para el erario público (18 mil millones de pesos), y en consecuencia, poco benéfico para el país; líderes y lideresas de los partidos salieron en su defensa, al señalar, como Clara Lopez, que el proceso de consulta "no solo fortalece la democracia interna del partido, sino la del país". Al respecto también se refirió el presidente del Partido que representa la orilla ideológica contraria, Oscar Iván Zuluaga, quien señaló que "es la forma en la que el país debe avanzar para el fortalecimiento de los partidos".

La directora de la Misión de Observación Electoral, Alejandra Barrios, insistió en la necesidad de fortalecer los procesos de consulta interna como escenarios que abren espacio a la democracia, en la medida en que permite la renovación de candidatos y candidatas, y elimina paulatinamente el relevo de "clanes familiares", que "en algunos departamentos pasa el poder de generación en generación en una misma familia, o se definen por alianzas regionales según convenga más".

Ante los resultados de las consultas, nuevas voces críticas señalaron la baja participación: cerca de 60 mil colombianos y colombianas participaron en el proceso, con una abstención que rodea el 90% de los potenciales electores. Si bien, son cifras que prenden las alarmas, es necesario recordar que los niveles de abstención en los procesos electorales en Colombia han sido muy altos en los últimos 60 años, cercanos al 60%, y que el sistema de consulta interna en los partidos es reciente y aún confuso para la sociedad, como demostró el sondeo realizado por Contagio Radio ( Link http://bit.ly/1Iq5jgM ).

Otras cifras contrarrestan positivamente las críticas, como el aumento en el número de candidatos y candidatas inscritas: 6.911. El Polo Democrático, por ejemplo, inscribió a un porcentaje mayor de mujeres que de hombres en sus listas: 2002 mujeres y 1627 hombres, lo que indicaría una paulatina apropiación de este sistema.

También cabe resaltar que el proceso de consulta interna tiene en cuenta la participación activa de ciudadanos y ciudadanas en el sistema de partidos, y no los votos de opinión. En el Polo Democrático, por ejemplo, la consulta de cara a su IV Congreso permite identificar las tendencias al interior del Partido, que dieron fortaleza al ala que lideran Clara Lopez, Iván Cepeda y Alirio Uribe con un 29.6% de los votos, sobre un 26.2% del MOIR, que lidera el Senador Jorge Enrique Robledo.

Otros Partidos y Movimientos no realizaron consultas internas, como el Movimiento Alianza Social Indígena, el Partido Verde, Movimiento Progresistas, el Partido de la U, la Alianza Social Independiente, Partido Opción Ciudadana, y el Partido Cambio Radical.

\[caption id="attachment\_7519" align="alignleft" width="909"\][![consulta interna polo](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/04/Captura-de-pantalla-2015-04-20-a-las-10.26.25.png){.size-full .wp-image-7519 width="909" height="634"}](https://archivo.contagioradio.com/que-dicen-los-resultados-de-la-consulta-interna-de-los-partidos/captura-de-pantalla-2015-04-20-a-las-10-26-25/) consulta interna polo\[/caption\]

<div>

</div>

</div>
