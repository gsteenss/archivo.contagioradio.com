Title: Piden proteger a todos los testigos contra los Uribe Vélez
Date: 2018-04-09 17:03
Category: Entrevistas, Judicial
Tags: alvaro uribe velez, Bloque Metro, Corte Suprema de Justicia, Corte Suprema de Justicia de Canadá, Juan Guillermo Monsalve, Paramilitarismo, Santiago Uribe Vélez
Slug: testigos_uribe_velez
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/04/Diseño-sin-título-4-e1523308291459.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: El Carbonero / El Tiempo] 

###### [9 Abr 2018] 

La ordenó la Corte Suprema de Justicia al INPEC sobre reforzar las medidas de seguridad al ex integrante de Bloque Metro, Juan Guillermo Monsalve Pineda, testigo contra **el expresidente Álvaro Uribe y su hermano Santiago Uribe Vélez, "debería ser una medida que se extienda a todos los testigos contra los Uribe",** afirma el abogado Daniel Prado, abogado de las víctimas del grupo paramilitar delos 12 Apóstoles.

“Mantener a este interno en condiciones de aislamiento, por seguridad de modo que a él sólo tenga acceso su familia, su abogado y las autoridades judiciales que lo requieran, previa información por escrito y con los mayores de control”, dice la Corte.

De acuerdo con el abogado, es un hecho que las personas que han sido testigos de los delitos de los cuales se sindica de haber participado a la familia Uribe Vélez, hay personas que han sido víctimas y que han sido asesinadas, amenazadas o han sido objeto de atentados.

### **Los otros testigos contra los Uribe ** 

Entre la lista de asesinados se encuentra Francisco Villalba, exintegrante de las AUC, condenado por la masacre del Aro en Antioquia, y quien fue asesinado en el 2009 luego de vincular a Álvaro Uribe, con dicha masacre.  Asimismo, de acuerdo con algunas investigaciones, se cree que quien fue secretario de la gobernación de Antioquia de Uribe, Pedro Juan Moreno, podría haber sido víctima de una manipulación de mecánica del helicóptero en que el que viajaba a Apartadó, y que se accidentó causándole la muerte a él a su hijo.

También, cabe recordar que hace poco el testigo Olwan de Jesús Agudelo, ha sido amenazado en las últimas semanas por sus declaraciones frente a la relación de Santiago Uribe en la conformación del grupo paramilitar de los 12 Apóstoles. En el mismo caso, Alexander Amaya, Juan Carlos Meneses y Eunicio Pineda.

Asimismo, sobre ese caso han sido asesinados alias 'El Relojero', un supuesto informante del grupo y que tenía una relojería al lado de la Alcaldía de Yarumal;  los hermanos Múnera, Hernán Darío Zapata, conocido como Pelo de Chonta.

Incluso, señala el abogado Prado que él mismo ha sido víctima de amenazas y atentados en su contra. De hecho hace unos meses aparecieron aflojadas las 4 llantas de su vehículo particular, **"es muy sospechosos por el nivel de poder de los Uribe y la vinculación mía en este proceso. Eso hace pensar que podría ser por eso el intento de que me pasara algún accidente"**, dice.

### **"El país merece la verdad"** 

Ante tal situación que se repite con el pasar de los años, para el abogado dicha medida de la Corte debería extenderse a todas aquellas personas que conocen de los hechos delictivos en los que estaría relacionada la familia Uribe. "El país debe saber cuál fue la vinculación de ellos en la violencia en Colombia, ya es hora de que las autoridades hagan de lado la impunidad y se mire si tantas denuncias contra Uribe son ciertas o no, así como también lo requiere la comunidad nacional e internacional", expresa el abogado.

Es de resaltar que Juan Guillermo Monsalve,  fue uno de los exparamilitares que denunció **supuestos vínculos del expresidente y su hermano con los paramilitares de la región antioqueña. Dicho paramilitar, con** Pablo Hernán Sierra, señaló a Álvaro y Santiago Uribe, así como a Juan Guillermo Villegas Uribe y** a Santiago Gallón de ser fundadores del Bloque Metro de las Autodefensas.**

Finalmente, apropósito  Día Nacional en Solidaridad con las Víctimas, Prado asegura que "es una vergüenza que una persona vinculada a delitos de lesa humanidad, pueda llegar a ser nombrada presidente del Congreso", como podría ser el caso de Álvaro Uribe, senador del Centro Democrático.

<iframe id="audio_25205459" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_25205459_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
