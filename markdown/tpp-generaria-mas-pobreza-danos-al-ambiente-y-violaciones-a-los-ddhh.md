Title: TPP generaría más pobreza, daños al ambiente y violaciones a los DDHH
Date: 2015-10-14 17:51
Category: Otra Mirada
Tags: Acuerdo TPP, Acuerdo Transpacífico de Asociación Económica, Derechos Humanos, Ecologistas en Acción, Radio derechos Humanos, Tratado de Libre Comercio
Slug: tpp-generaria-mas-pobreza-danos-al-ambiente-y-violaciones-a-los-ddhh
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/10/TPP.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto:questiondigital.com 

<iframe src="http://www.ivoox.com/player_ek_8987795_2_1.html?data=mZ6lmZydeY6ZmKiak5uJd6KkmZKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRmLHEjMzS0MrWpdOZpJiSo6nFb86ZpJiSo5bXb9Hjw9fS3MaJdqSfxcaSpZiJhpLj1JDOzpDFscPdxtPhx5Ddb9fd0NHOj4qbh4630NPhw8zNs4zGwsnW0ZCRaZi3jpk%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Tom Kuchartz, Ecologistas en acción] 

###### [14 oct 2015]

El pasado 5 de octubre, doce países de la cuenca del Pacífico discutieron el Acuerdo Estratégico Transpacífico de Asociación Económica (TPP, por sus siglas en inglés), que reduciría las barreras arancelarias en artículos que van desde automóviles hasta arroz, y que a su vez generaría **mayores niveles de pobreza, daños al ambiente y violaciones a los derechos humanos,** como lo asegura Tom Kuchartz, miembro de Ecologistas en acción.

Japón, Brunei, Vietnam, Malasia, Singapur, EE.UU, Canadá, México, Perú, Chile, Australia y Nueva Zelanda, son los países que hacen parte de este pacto comercial, que para Kuchartz, implica un acuerdo de libre comercio que funciona como los otros acuerdos en donde “**se beneficia a los grandes capitales**” y se afectan “ **las mayorías sociales, es decir los más desfavorecidos, los que sufren pobreza, desempleo y atentados al ambiente**”.

El TPP plantea consolidar la relación de U.S.A con Japón y otros países del pacífico, no solo en el ámbito económico sino geopolítico, como lo dijo el secretario de Defensa de U.S.A, Ashton Carter “ este acuerdo es más importante que poner otro portaaviones en el Pacífico”, explica el integrante de Ecologistas en Acción, quien agrega que para Latinoamérica el TPP implica mayor desigualdad social y una disminución en los ingresos del gobierno para invertir en el país.

Por otra parte, la adhesión de Chile, México y Perú a este acuerdo “es una apuesta geopolítica de impedir una integración regional alternativa”, como se ha intentado construir desde los países del ALBA.

Según Ecologistas en Acción,  **la agricultura y específicamente los campesinos se verían gravemente afectados por la eliminación de aranceles** que sirven “para proteger la pequeña agricultura", lo que “perjudicará a pequeños productores, especializando una serie de productos y eliminando la soberanía alimentaria, al usar productos de exportación”.

La salud sería otro de los temas afectados con este acuerdo Estratégico TransPacífico de Asociación Económica, ya que plantea reglas de propiedad intelectual, las cuales favorecen a las grandes empresas farmacéuticas permitiendo **"alargar los monopolios de medicamentos patentados, dejando por fuera los medicamentos genéricos que son más baratos”,** encareciendo los servicios de salud de ciertos países donde se provee medicamentos genéricos, pero sobre todo perjudicando al consumidor.

Otro de los problemas al que se refiere Kuchartz, es que con este acuerdo se desregula el sector financiero, provocando más pobreza y desempleo pues se trata de leyes a favor de las multinacionales, teniendo en cuenta también que "cuando se elimina los aranceles se pierden ingresos del presupuesto público".

Frente a este panorama, **desde el pasado 18 de abril se han desarrollado diversas acciones de movilización en diferentes partes del mundo**, como formas de sensibilizar y poner en alerta a los sectores sociales sobre las implicaciones que tiene el acuerdo. Así mismo, se planea una gran jornada de movilizaciones para el 2016.
