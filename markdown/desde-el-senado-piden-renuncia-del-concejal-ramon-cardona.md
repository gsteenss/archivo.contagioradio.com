Title: La violencia contra las mujeres es una pandemia: mujeres en el Senado
Date: 2017-06-12 16:32
Category: Libertades Sonoras, Mujer
Tags: Claudia López, feminicidio, Fiscal, genero, mujeres, Violencia de género
Slug: desde-el-senado-piden-renuncia-del-concejal-ramon-cardona
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/03/parodemujeres.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo] 

###### [12 Jun. 2017] 

**Cada 28 minutos una mujer es víctima de violencia sexual, cada 12 minutos una mujer es agredida por su pareja o expareja**, por los menos 1/3 de las mujeres son víctimas de violencia económica y cada 4 días 1 mujer es asesinada por su pareja o expareja, estas fueron tan solo algunas de las cifras presentadas en el Foro: Violencia contra la mujer en la Comisión I del Senado convocado por la senadora Claudia López y al que asistieron instituciones del Estado y organizaciones de mujeres.  Le puede interesar: [413 mujeres fueron víctimas de feminicidio en los últimos 3 años](https://archivo.contagioradio.com/413-mujeres-han-sido-victimas-de-feminicidio-en-colombia-en-los-ultimos-3-anos/)

Además de realizar todo un panorama de la situación que viven las mujeres en Colombia en acceso a salud, equidad en salarios, empleo y justicia, las personas asistentes dijeron que el caso del Concejal del partido conservador, Ramón Cardona en el que aseguró que “las mujeres son como las leyes, se hicieron para violarlas”, es parte de un discurso machista que ve a las mujeres como objetos, **razón por la cual aprovecharon para solicitar la renuncia del funcionario.**

### **Obstáculos para las mujeres en materia de salud** 

Según Women’s Link, una de las organizaciones asistentes al foro, en temas de salud las mujeres siguen enfrentando 3 graves problemas, el primero de ellos es la **falta de acceso a equipos técnicos y humanos capacitados para tratar el tema de violencias de género,** el segundo es la falta de acceso al aborto legal y el tercero es la falta de capacitación y el desconocimiento de las leyes por parte de las instituciones y su personal.

“No hay equipos técnicos para dar respuestas a los derechos sexuales y reproductivos de las mujeres, en donde según la región el acceso es cada vez más difícil. En el posconflicto debe haber un fortalecimiento del acceso a salud sexual y reproductiva de las mujeres, sino las promesas de paz van a quedar incumplidas” aseguró Mariana Ardila, abogada de Women´s link. Le puede interesar: ["Tenemos un Estado fracasado, habla de paz y permite violencia contra mujeres y niñas"](https://archivo.contagioradio.com/tenemos-un-estado-fracasado-que-habla-de-paz-y-permite-la-violencia-contra-mujeres-y-ninas/)

### **Mujeres no tienen acceso a justicia** 

Según la senadora Claudia López gracias a un análisis realizado por su Unidad de Trabajo Legislativo – UTL- de **los 50 municipios con mayor tasa de violencia contra las mujeres tanto sexual como intrafamiliar en Colombia, el 96% no hay casa de la justicia**, en el 76% no hay comisarías de familia, ni Fiscalia, ni Medicina Legal.

“Esa precariedad estatal en los territorios, sobre todo rurales, lo que esta es permitiendo que se reproduzca la violencia y que se hagan inaplicables las leyes que ha producido el Congreso” dijo Claudia López en el Foro.

Por su parte, María Paulina Riveros,  Vicefiscal General de la Nación, aseveró que durante **el 2017 aumentaron los casos de denuncia por parte de mujeres en lo que respecta a la violencia contra ellas** “los números que manejan desde la Fiscalía son 38426 casos” precisa

Así mismo, relató que son 20 mil casos de violencia familiar los que se han registrado entre 2014 y 2015, sin que la Fiscalía sepa a ciencia cierta si dicho aumento en las cifras es por mayor conducta delictiva o mayor número de denuncias. Por último, aseveró que la violencia intrafamiliar fue el tercer delito más denunciado durante el 2016.Le puede interesar: [Colombia se disputa segundo lugar en Feminicidios en América Latina](https://archivo.contagioradio.com/colombia-se-disputa-segundo-lugar-en-feminicidios-en-america-latina/)

### “**El Estado está improvisando en la atención a mujeres”** 

Así lo manifestó **Carlos Valdés, director del Instituto de Medicina Legal para quien la violencia que atendían hace 12 años, ya no es la misma de hoy** “hoy las armas de fuego están en primer lugar de violencia hacia la mujer” recalca el funcionario.

Quien además relata que la violencia sexual ha aumentado en niñas, adolescentes y mujeres de la tercera edad, siendo los días sábados y domingo y los meses de junio y septiembre los picos más altos “año tras año hay mayor violencia contra la mujer” dijo Valdés.

Así mismo añadió el funcionario que muchas de las acciones que se están haciendo en la actualidad “son cosas que se hacen de manera improvisada, razón por la cual hay que formar por ejemplo médicos que estén preparados para tratar la violencia contra la mujer”. Le puede interesar: [La música reivindica los derechos de las mujeres](https://archivo.contagioradio.com/el-feminismo-en-la-musica/)

### **Por ser mujer se gana 20% menos que los hombres** 

Ana Isabel Arenas, estudiosa del tema de "economía del cuidado" hizo hincapié en su intervención a seguir trabajando por una igualdad en el tema económico para las mujeres, pues según cifras oficiales “el 30% de las mujeres no tiene ingreso; frente al 10% de hombres en igual situación y **por ser mujer se gana 20% menos que los hombres en diversos cargos”.**

### **Tres propuestas claves para el Gobierno Nacional** 

En el marco de este foro se lanzó la propuesta de **realizar unas Brigadas Móviles de Justicia** para que de tal manera se pueda llegar a los lugares más apartados del país “donde no hay Estado y tampoco justicia” añade la senadora Claudia López

Así mismo, se dijo que sea el **Ministerio de Justicia se haga cargo de las comisarías de familia,** sea quien las coordine, quien asigne el personal capacitado, reclutado meritoriamente y bien pagos.

“Hemos estimado que se necesitan 2 billones de pesos para que tengamos comisarías de familia sólidas y presentes en todos los municipios del país con equipo técnico de derecho, de psicólogos, de trabajadores sociales dispuestos en el territorio para que atiendan a las mujeres” recalcó López.

La tercer propuesta es que a propósito del caso del Concejal Cardona, que calificaron como un caso para nada aislado, debe comenzarse a **imponer sanciones de tipo disciplinario a cualquier funcionario público que incurra en este tipo de actuaciones.**

"Le pediremos a la Procuraduría que sancione disciplinariamente a cualquier funcionario público electo o designado que le diga a una mujer que ella es la culpable de la violencia que le ocurre. No nos podemos quedar en  la sanción moral y mediática" puntualizó López.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)
