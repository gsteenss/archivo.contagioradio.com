Title: ¿Quién protege a los líderes y lideresas sociales?
Date: 2019-05-24 09:55
Author: CtgAdm
Category: Líderes sociales, Nacional
Tags: audiencia pública, Congreso, FARC, Líderes y lideresas sociales
Slug: quien-protege-lideres-y-lideresas-sociales
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/Líderes-y-lideresas-sociales-en-el-Congreso.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: @SandraFARC] 

Este viernes desde las 8 de la mañana se desarrolla una audiencia en el Congreso de la República qué tiene como principal objetivo responder a la pregunta: ¿Quién protege a los líderes y lideresas sociales? **Sandra Ramírez, senadora del partido FARC** y citante de la audiencia, manifestó que también buscan sensibilizar a funcionarios públicos sobre el riesgo de defender los derechos humanos en el país.

En la audiencia, se plantearán desde las voces de líderes y lideresas sociales, los principales riesgos a los que se enfrentan en los territorios, y así recoger testimonios que sirvan de evidencia para las instituciones encargadas de proteger los derechos humanos; y con ello, esperan contribuir con insumos para que se garantice esta importante labor. (Le puede interesar: ["Senadores ceden su curul a líderes sociales por un día"](https://archivo.contagioradio.com/senadores-ceden-su-curul-a-lideres-sociales-por-un-dia/))

Ramírez recordó que en el Acuerdo de Paz habían quedado consignadas garantías para la defensa de derechos humanos, así como para los liderazgos sociales, pero no se ha avanzado en ese sentido; adicionalmente, señaló que **tampoco se ha cumplido con el acuerdo de desmantelar los grupos paramilitares**. La senadora planteó que la pregunta para los delegados del Gobierno será ¿qué se está haciendo para proteger a líderes y lideresas sociales desde las políticas públicas?

> [\#Inicia](https://twitter.com/hashtag/Inicia?src=hash&ref_src=twsrc%5Etfw)| Intervención de líderes sociales denunciando el mal manejo de las políticas públicas y la inseguridad que viven actualmente y como esto ha afectado los acuerdos de paz. [\#NosEstanMatando](https://twitter.com/hashtag/NosEstanMatando?src=hash&ref_src=twsrc%5Etfw) [pic.twitter.com/1n8Zryyt78](https://t.co/1n8Zryyt78)
>
> — Lazos de Dignidad (@FLazosDignidad) [24 de mayo de 2019](https://twitter.com/FLazosDignidad/status/1131933865474449410?ref_src=twsrc%5Etfw)

<p>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
</p>
### **Ministro de Defensa y Ministra de Interior no se presentarán a la audiencia** 

A la audiencia pública fueron citados los directores de entidades encargadas de cuidar los derechos humanos: Procuraduría General de la Nación, Defensoría del Pueblo, Unidad Nacional de Protección (UNP), Ministerio de Interior y Ministerio de Defensa. Sin embargo, Lobo afirmó que **tanto la ministra Nancy Gutiérrez como el ministro Guillermo Botero enviaron a delegados de sus instituciones y no se presentarán al evento**.

Por su parte, la senadora indicó que la Procuraduría y la Defnesoría presentarán acciones que se están tomando para proteger a líderes y lideresas; mientras la UNP expondrá la cantidad de solicitudes de protección que tiene. No obstante, la congresista concluyó que no se trata únicamente de brindar medidas de protección individual, sino de tomar medidas para la seguridad colectiva que eviten "que nos sigan matando". (Le puede interesar: ["La táctica de la derecha para generar caos y miedo"](https://archivo.contagioradio.com/tactica-derecha-generar-caos-miedo/))

<iframe id="audio_36301190" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_36301190_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
