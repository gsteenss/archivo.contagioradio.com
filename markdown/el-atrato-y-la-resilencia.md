Title: El Atrato y la resilencia
Date: 2019-05-21 14:30
Author: Antonio Jose Garcia
Category: Antonio José García, Opinion
Tags: Atrato, cacarica, Comisión Intereclesial de Justicia y Paz, uraba
Slug: el-atrato-y-la-resilencia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/bajo-atrato-cacarica.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/CACARICA-FESTIVAL.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Contagio Radio 

Con excepción de la cuenca de los ríos Cauca y Magdalena, las grandes cuencas de los ríos Colombia son síntomas de la gravísima enfermedad del abandono estatal. La falta de estado es un hecho notorio, y la cuenca del rio Atrato no es la excepción. Es casi imposible ver un funcionario del estado cumpliendo su misión institucional, con excepción de patrullas militares que se ubican en puntos estratégicos de los ríos para censar a quienes pasan. En las riberas, directamente sobre el río, se levantan poblados pequeños de casas de madera y techos de zinc construidos en palafitos sobre el agua, sin agua potable, ningún saneamiento básico, sin energía eléctrica distinta a la que pueden proveerse algunos con plantas que consumen combustibles carísimos. Clima malsano, calor excesivo, enfermedades tropicales, ausencia de servicios básicos.

En medio de todo estas manifestaciones del síntoma, están las comunidades, nuestras gentes colombianas de muchas razas orígenes y lenguas que sobreviven a la dura realidad de su cotidianidad en medio de la pobreza total pero rodeados de una naturaleza exuberante, un paisaje hermoso y aguas caudalosas con un suelo fértil que les provee arroz, yuca, plátano y peces como alimento y remedios ancestrales para las dolencias. Pero la realidad es que el paisaje nunca provee educación, atención médica ni salud suficiente para enfermedades graves ni una dieta suficiente, nutritiva y equilibrada.

Una pobreza que duele. Ahí entiendo que sin lugar a dudas no se puede hablar de abandono estatal por que el estado colombiano nunca ha estado allá. No puede un país abandonar una región en la que nunca ha estado presente. Es la forma mas dura para entender que el mapa no es el territorio.

Esporádicamente llegan oleadas de la única manifestación del estado que se conoce por esos lares: Las fuerzas militares. Y llegan en lo que llaman en forma rimbombante “operaciones” que se adentran cientos de soldados, enviados a las regiones con planes “estratégicos” concebidos sobre mapas y cumpliendo ordenes dadas desde escritorios en Bogotá. En ellos si invierte eficazmente el estado los recursos de que dispone para ir al territorio: helicópteros de transporte y de combate, lanchas de transporte y de combate; naves de transporte y de combate, aviones de transporte y de combate todo para llevar fugazmente un muy visible despliegue de fuerza que cumple mal con su función de intimidar a quienes no necesitan ser intimidados. Esa es la única forma que en las comunidades, dentro de la manigua y aisladas por la naturaleza pueden sentir como “presencia del estado”.

Ninguna otra institución gubernamental hace presencia por allí. Mil veces me llega a la mente la desesperada pregunta del poeta Gonzalo Arango: ¿no habrá manera de que Colombia, en vez de matar a sus hijos los haga dignos de vivir?

Después de varios años volví al Atrato y al Chocó, donde había pasado algún tiempo cuando laboraba en Urabá. Fui de la mano de la Comisión Intereclesial de Justicia y paz y otras entidades no gubernamentales; me adentré por la cuenca del río Cacarica, hasta los territorios hoy nuevamente habitados por quienes fueron parte del desplazamiento forzado en su momento, por una de aquellas extrañas formas que tiene el estado colombiano de “hacer presencia” en las comunidades, que en ese momento se conoció como la operación militar “Génesis”. Décadas después aun pude observar el temor que se refleja en las caras de las personas cuando escuchan ese nombre, lo recuerdan, o sienten el ruido de un avión o un helicóptero sobre ellas.

Allí en medio de la manigua, me encontré en la **Zona humanitaria Nueva Esperanza en Dios**, una de las estigmatizadas “comunidades de paz” a las que tanto se les teme desde las entidades gubernamentales. Esta comunidad está conformada por personas sobrevivientes de las violencias que aun azotan a Colombia, y todas víctimas de la última gran plaga de la humanidad: La indiferencia social.

No vi por ninguna parte el supuesto aquelarre delincuencial de la “ república independiente” que se les atribuye desde la mitología oficial. Lo que vi y viví durante varios días me conmovió verdaderamente. Mujeres y hombres, que se reconocen como colombianos, gentes de distintas etnias y de todas las edades, que se auto-determinan en una forma organizada con un profundo respeto por la vida y el ambiente en medio de una pobreza digna, con amor, solidaridad, amabilidad, resiliencia, cultura popular, todo en medio del evidente abandono estatal.

Ellos, en una forma cordial pero con firmeza no permiten el ingreso a la zona humanitaria de ningún actor armado, hecho que pareciera ofender más a los actores armados legales que a los ilegales. De ahí la condena a llevar el peligroso estigma de “subversivos”.

Allí, queda en plena evidencia que el esfuerzo colectivo está enfocado a la consolidación de la reconciliación, a la construcción de la memoria, a la búsqueda de la verdad, a dejarles a las generaciones venideras el gran legado de paz de su historia individual y colectiva y su armoniosa convivencia como ejemplo de resiliencia a través de su proyecto emblemático: la Universidad de Paz.

Desde el Atrato, desde el Cacarica y otras regiones colombianas golpeadas por la violencia, se teje el futuro de un país en paz, en comunidades que con una valiente dignidad y claridad pide que la presencia del estado en las regiones alejadas no sea a través de lamentables “operaciones” militares, sino que sea civil, respetuosa, efectiva, total y permanente, impactando positivamente calidad de vida de todos.

Una verdadera lección de resiliencia que conmueve y motiva a seguir trabajando por la reconciliación y buscando la tan esquiva paz que todos merecemos y queremos sentir en vida.
