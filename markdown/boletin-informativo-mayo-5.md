Title: BOLETÍN INFORMATIVO MAYO 5
Date: 2015-05-04 15:41
Author: CtgAdm
Category: datos
Tags: Agencia de medios alternativos de Israe, Andrés Leyton, Bogotá, cambio climatico, ELN, ELN. Habla Luis Fernando Vega, Etíope, FARC, Gaza, Guardia Costera italiana, Holón, Humedal, Humedal La Conejera, inmigrantes, Latinoamérica, Médicos sin Fronteras, Palestinos, peligro de extinción, racismo institucional, Rompiendo el Silencio, Sergio Yani, UE, Universidad de Connecticut, Universidad El Rosario, Universidad Pedagógica Nacional
Slug: boletin-informativo-mayo-5
Status: published

[Noticias del Día: ]

<iframe src="http://www.ivoox.com/player_ek_4445347_2_1.html?data=lZmhl5iYe46ZmKiakpWJd6KkkZKSmaiRdI6ZmKiakpKJe6ShkZKSmaiRhtDgxtmSpZiJhaXijK7byNTWscLoytvcjbLFvdCflpKSmaiRh9Di1cbUy9SPlsLYytSYj4qbh46o&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

-Después de la difusión de un video donde policías blancos maltrataban a un detenido **Etíope** en la ciudad de **Holón**, la comunidad protestó en contra del **racismo institucional** que padecen desde décadas dejando un balance de más de 30 heridos y dos detenidos. Habla **Sergio Yani**, miembro de la **Agencia de medios alternativos de Israe**l.

-De acuerdo con un estudio de la **Universidad de Connecticut**, Estados Unidos, una de cada seis especies de fauna y flora desaparecerán del planeta a causa del **cambio climático**; **Latinoamérica** es la región con mayores índices de especies animales y vegetales en **peligro de extinción** con un 23%, seguido por Australia y Nueva Zelanda con el 14 %.

-La **Guardia Costera italiana** y el equipo de rescate de **Médicos sin Fronteras** rescataron este fin de semana un total de 6.639 **inmigrantes**, en operaciones en las que 8 resultaron desaparecidos y 2 ahogados. La emergencia humanitaria continua sin ningún tipo de respuesta política por parte de la **UE**, ni de los países afectados, que en un intento de ocultar la situación están invirtiendo en el rescate, en internamiento forzado y la expulsión de los inmigrantes.

-Desde las seis de la mañana alrededor de cien agentes de la fuerza pública hacen presencia en el campamento que defiende el **humedal "La Conejera "** para desalojar a las personas que llevan meses frente a la constructora, con el objetivo de realizar un ejercicio de resistencia y veeduría en defensa del Humedal. Habla **Andrés Leyton**, defensor del **Humedal**.

-La **ONG** israelí **Rompiendo el Silencio** ha recogido el testimonio de 60 oficiales, suboficiales y soldados que acusan al estado de permitir y ordenar al ejercito acciones indiscriminadas contra la población que tuvieron como consecuencia la masacre de civiles **Palestinos**, así como de la elevada y desproporcionada destrucción de viviendas y propiedades en **Gaza** durante la invasión de agosto de 2014.

-Estudiantes universitarios y de secundaria se reunirán el sábado 9 y domingo 10 de mayo en la **Universidad Pedagógica Nacional**, en **Bogotá**, para debatir cuál es su papel en la construcción de la paz en Colombia y hacerse participes de los procesos que adelanta el gobierno actualmente con las guerrillas de las **FARC** y el **ELN**. Habla **Luis Fernando Vega**, estudiante de la **Universidad El Rosario**.
