Title: UBPD interviene cementerio en Samaná y otros 3 municipios de Caldas
Date: 2020-10-26 16:40
Author: AdminContagio
Category: Actualidad, Nacional
Tags: Caldas, Samaná, San Agustín, UBPD, Unidad de Busqueda de Personas dadas por Desaparecidas
Slug: ubpd-interviene-cementerio-en-samana-y-otros-3-municipios-de-caldas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/10/UBPD-accion-humanitaria-Caldas.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/10/WhatsApp-Image-2020-10-26-at-3.11.19-PM.jpeg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

La Unidad de Búsqueda de Personas Dadas por Desaparecidas -UBPD-, anunció que realizará acciones humanitarias en el municipio de Samaná, Caldas; en el marco del Plan de Búsqueda Regional que se desarrolla en esta región del Magdalena Medio Caldense; **asimismo informó que realizará por primera vez una toma de muestras biológicas a 182 familiares de víctimas de desaparición de la región.**

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/UBPDcolombia/status/1320704771834155008","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/UBPDcolombia/status/1320704771834155008

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

**A partir de este lunes 26 de octubre y hasta el 8 de noviembre, la UBPD intervendrá el cementerio San Agustín en Samaná, en donde tiene proyectado recuperar cuerpos que corresponderían a personas desaparecidas en el marco del conflicto armado.**

<!-- /wp:paragraph -->

<!-- wp:quote -->

> “Por primera vez el Estado en cabeza de la Unidad de Búsqueda de Personas Desaparecidas junto a los familiares de las víctimas y las organizaciones realizan una acción humanitaria en el cementerio de Samaná, en el Magdalena Medio caldense, con el propósito de contribuir al alivio de su sufrimiento, que produce no saber la suerte y paradero de sus seres queridos” 
>
> <cite>Luz Marina Monzón, directora de la Unidad de Búsqueda de Personas dadas por Desaparecidas -UBPD-</cite>

<!-- /wp:quote -->

<!-- wp:paragraph -->

**Esta misión humanitaria es el resultado del trabajo conjunto entre la Unidad de Búsqueda y otras organizaciones sociales** como el Centro de Estudios sobre Conflicto, Violencia y Convivencia Social, de la Universidad de Caldas -CEDAT-; el Equipo Colombiano Interdisciplinario de Trabajo Forense y Asistencia Psicosocial –EQUITAS-, y la Fundación para el Desarrollo Comunitario de Samaná –FUNDECOS-; las cuales entregaron en febrero pasado a la UBPD **una investigación que da cuenta de 187 casos documentados de personas dadas por desaparecidas en los municipios de La Dorada, Norcasia, Victoria y Samaná del departamento de Caldas.**

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/UBPDcolombia/status/1320736172423958528","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/UBPDcolombia/status/1320736172423958528

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

**Luz Marina Monzón, directora de la UBPD, señaló que la entidad tiene proyectado avanzar con estas acciones humanitarias en los demás municipios documentados** y ratificó su compromiso con garantizar, en cada una de las fases del proceso de búsqueda, la participación tanto de los familiares que buscan a sus seres queridos, como de las organizaciones de DD.HH. que los acompañan.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### La UBPD continúa con su labor para hallar personas dadas por desaparecidas

<!-- /wp:heading -->

<!-- wp:paragraph -->

En el marco de su labor la UBPD ha adelantado varias acciones para cumplir su misión de hallar personas dadas por desaparecidas, hace un par de meses **declaró el sitio de inhumación de El Copey (Cesar) como «*[lugar de interés para la búsqueda de personas desaparecidas](https://www.ubpdbusquedadesaparecidos.co/actualidad/ubpd-declara-sitio-de-inhumacion-de-el-copey-cesar-como-lugar-de-interes-para-la-busqueda-de-personas-desaparecidas/)*»; a raíz de una denuncia** ciudadana dada a conocer en la que se alertaba sobre el entierro de cadáveres de personas fallecidas por Covid-19 en dicho terreno. (Lea también: [UBPD pide protección para sitio de inhumación en El Copey (Cesar)](https://archivo.contagioradio.com/ubpd-pide-proteccion-para-sitio-de-inhumacion-en-el-copey-cesar/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Asimismo ha encontrado apoyo en la acción coordinada con la Jurisdicción Especial para la Paz -JEP-, que por ejemplo en Antioquia, **ordenó proteger 16 zonas ubicadas en el área de influencia del proyecto hidroeléctrico Hidroituango, donde habría cuerpos de víctimas del conflicto armado**; y abrió un incidente en contra del gerente de Empresas Públicas de Medellín -EPM-, entidad responsable del proyecto, por el no cumplimiento de dichas medidas de protección. (Lea también: [JEP abre incidente contra gerente de EPM](https://archivo.contagioradio.com/jep-abre-incidente-contra-gerente-de-epm/))

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
