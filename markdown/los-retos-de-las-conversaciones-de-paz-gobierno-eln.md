Title: Los retos de las conversaciones de Paz Gobierno - ELN
Date: 2017-02-06 17:33
Category: Entrevistas, Paz
Tags: Proceso de paz con el ELN, Quito
Slug: los-retos-de-las-conversaciones-de-paz-gobierno-eln
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/10/eln-y-gobierno.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo Particular] 

###### [6 Feb 2017] 

Este martes se llevará a cabo la instalación de la mesa de diálogos entre el Ejército de Liberación Nacional y el Gobierno de Colombia, que tendrán como primeros puntos de conversación la conformación de la **comisión sobre participación social y la comisión de dinámicas y acciones humanitarias**  con la finalidad de ir desescalando poco a poco el conflicto y generar confianza para el proceso.

De acuerdo con el gestor de paz Carlos Velandia, una vez se sienten en la mesa de conversaciones ambos equipos y se empiecen a conformar mecanismos humanitarios, **se podría dar paso a cesar poco a poco las confrontaciones hasta llegar a declarar el cese bilateral. **Le puede interesar: ["ELN y Gobierno cumplen acuerdos humanitarios"](https://archivo.contagioradio.com/cumplidos-los-acuerdos-humanitarios-para-iniciar-conversaciones-gobierno-y-eln/)

“**Ambas partes tienen razón en plantearse expectativas al respecto**, en primer lugar nada mejor que desarrollar los diálogos en medio de un clima positivo y sin violencia como lo quiere el ELN, pero otro punto a favor para el gobierno es que arrancar discutiendo un cese bilateral al fuego de entrada podría irse a un año, sin hablar de otros puntos”

Velandia considera que empezar por desescalonar el conflicto a través de acciones humanitarias permite **ver resultados más rápidos del conflicto** generando por una parte un alivio de las tenciones entre los que conversan y obteniendo aceptación del proceso desde la sociedad

### **Proceso de paz ELN-Gobierno de cara a elecciones 2018** 

Para el analista Carlos Velandia, **el  poco tiempo que le queda al gobierno de Juan Manuel Santos pone en riesgo al proceso**, debido a que no hay garantías para que el gobierno que llegue  continúe con las conversaciones con el ELN, teniendo en cuenta las candidaturas que ya se conocen como Vargas Lleras o Alejandro Ordoñez.

“Nos podemos dar por bien servidos si el proceso arranca, se estabiliza y en los próximos 12 meses evacúa el punto de participación para llegar a un **punto de no retorno y pactar el cese bilateral definitivo**, teniendo en cuenta que el único gobierno que ha brindado garantías para este proceso es el actual”

Otra de las posibilidades que han surgido de cara a darle continuidad a la implementación del proceso de paz de las FARC-EP y continuar con el del ELN, es la que realizo **Rodrigo Londoño al proponer un gobierno de transición**, a su vez, Velandia afirmó que pueden existir otras opciones como un candidato desde la izquierda democrática.

### **Participación Social en el proceso de Paz** 

Frente a  la participación social, Velandia considera que es uno de **“los nudos ciegos”** que ambas partes tendrán que desenredar, ya que aún no se ha establecido una metodología sobre cómo se dará esa participación a la espera de que sea la misma sociedad la que construya esa ruta.  Sin embargo, considera que uno de los sectores que mayor participación tendrá son **las localidades y territorios donde existen agendas de trabajo ya planteadas y con exigencias concretas.**

Este fin de semana se realizaron los 100 encuentros propuestos por la Mesa Social para la Paz, que de acuerdo con una de sus voceras, Diana Sánchez arrojaron como resultado la intensión que existe al interior de la sociedad de buscar participar y hacer modificaciones en el país, además agregó que escenarios como los encuentros sirven para **“dotar de legitimidad el proceso de paz con el ELN”.** [Le puede interesar: "Para necogiar es necesario abrir debates y para eso se requiere a la sociedad: ELN"](https://archivo.contagioradio.com/para-negociar-es-necesario-abrir-debates-y-para-eso-se-requiere-a-la-socieda/)

<iframe id="audio_16854576" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_16854576_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio]{.s1}](http://bit.ly/1ICYhVU)[.]{.s1}
