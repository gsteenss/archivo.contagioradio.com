Title: Consulta Popular en Cajamarca pondría fin a La Colosa
Date: 2016-11-10 15:16
Category: Ambiente, Nacional
Tags: consulta popular minera, La Colosa, La Colosa Cajamarca
Slug: consulta-popular-en-cajamarca-pondria-fin-colosa
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/la_colosa-e1478806078542.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: No a La Colosa] 

###### [10 Nov de 2016] 

El alto Tribunal Administrativo del Tolima declaró **constitucionalidad del mecanismo de Consulta Popular Minera en el municipio de Cajamarca, la cual buscará frenar el proyecto de explotación de oro La Colosa** desarrollado por la multinacional Anglo Gold Ashanti y que cuenta con 25 títulos mineros.

**Para que los resultados tengan validez se necesita que 5.500 ciudadanos de 16.101 habilitados vayan a las urnas y voten** a la pregunta: “¿Está usted de acuerdo Si o No con que en el municipio de Cajamarca se ejecuten actividades que impliquen contaminación del suelo, perdida o contaminación de las aguas o afectación de la vocación tradicional agropecuaria del municipio, con motivos de proyectos de naturaleza minera?”.

Esta iniciativa podría evitar la extracción de **29 millones de onzas de oro que se pretenden extraer en La Colosa para 2020,** **revocar los 25 títulos mineros vigentes y 8 solicitudes pendientes para zonas de explotación,** que representan el 86% del municipio de Cajamarca. Le puede interesar: [Campesinos defienden su derecho a la consulta en Cajamarca, Tolima](https://archivo.contagioradio.com/campesinos-defienden-su-derecho-a-la-consulta-en-cajamarca-tolima/).

### **¿Cuáles son los impactos que ha generado La Colosa?** 

Los estudios de impacto adelantados por organizaciones defensoras del agua, la vida y el territorio demuestran que La Colosa está ubicada en zona de alta actividad sísmica, en inmediaciones del **volcán el Machín** donde también hay importantes **áreas de recarga hídrica**. Demuestran además que la empresa **extrae diariamente entre 50.000 y 80.000 toneladas de roca** para obtener el mineral microscópico, utilizando **3 millones de litros de agua por hora,** la cual queda en su totalidad **contaminada con cianuro**.

### **¿Qué ha dicho la multinacional?** 

A través de un comunicado la Anglo Gold afirmó que "La pregunta aprobada por el Tribunal Administrativo del Tolima hace referencia a la minería ilegal, una minería que debe ser erradicada en el país. **La minería formal y legal como la nuestra cumple con todas sus obligaciones, genera empleo de calidad, es auditada por la misma comunidad, las autoridades locales, regionales y nacionales; y promueve espacios de participación** con las comunidades para la construcción conjunta de planes y políticas de desarrollo y progreso".

Por otra parte, mientras en Cajamarca reciben una noticia esperanzadora, los habitantes de Ibagué siguen a la espera, pues l**a Consulta Popular programada para el 30 de octubre en esta ciudad que ha sido aplazada **por una medida provisional del Consejo de Estado mientras evalúa una tutela interpuesta por Alberto Enrique Cruz. Sin embargo, **esto no ha desalentado a los ibaguereños, por el contrario se ha fortalecido la movilización social. **Le puede interesar: [Consulta popular en Ibagué busca frenar 35 títulos mineros](https://archivo.contagioradio.com/consulta-popular-en-ibague-busca-frenar-35-titulos-mineros/).

Robinson Mejía, quien hace parte del comité impulsor de la Consulta, manifestó que **mantener la movilización es fundamental, pues temen nuevos sabotajes** por parte de personajes como Alberto Cruz o Nestor Gregory Díaz, quien es representante de la Anglo Gold y **ha obstaculizado consultas en Tolima, Quindío y Risaralda.**

###### Reciba toda la información de Contagio Radio en su correo [[bit.ly/1nvAO4u]
