Title: Por ahora, Duque no quiere la negociación sino la rendición del ELN: Victor de Currea
Date: 2018-09-03 13:00
Author: AdminContagio
Category: Nacional, Paz
Tags: ELN, Iván Duque, proceso de paz, Víctor de Currea
Slug: duque-quiere-rendicion-eln-currea
Status: published

###### [Foto: @eln\_voces] 

###### [3 Sept 2018] 

A falta de 4 días para cumplirse el plazo estipulado por el presidente Duque en su posesión para evaluar el proceso de paz con el **Ejercito de Liberación Nacional (ELN),** el analista Víctor de Currea-Lugo, criticó que en dicha valoración no se tome en cuenta a la sociedad civil, así como a la misma guerrilla, y añadió que se están cometiendo errores del pasado en estas negociaciones.

Para de Currea, en la evaluación del proceso de paz se está escuchando la voz de las Fuerzas Armadas, "los enemigos de la paz, antiguos negociadores, la iglesia y la ONU"; pero **no está escuchando la voz de las comunidades que se beneficiaron de la tregua bilateral que hubo finalizando el año pasado,** y tampoco se toma en cuenta la opinión del propio ELN.

Hechos que para el analista, hacen que la evaluación que se está realizando no sea seria y cojee, "y algo que cojea, en cualquier momento se puede caer". Riesgo que se suma al 'secuestro de la mesa', término que usa de Currea para explicar la subordinación de la mesa sobre el objetivo de liberar personas retenidas.

### **Duque está cometiendo los mismos errores en el proceso que otros gobiernos** 

Para el académico, hay dos errores fundamentales que está cometiendo Iván Duque en el enfoque con el que está asumiendo el proceso de paz con el ELN y pueden afectarlo: **el no tener claro el objetivo general de la mesa y no conocer a la guerrilla. **(Le puede interesar:["Duque podría darle una oportunidad al proceso de paz: De Currea"](https://archivo.contagioradio.com/duque-podria-darle-una-oportunidad-al-proceso-de-paz-con-el-eln-de-currea/))

De Currea aseguró que la mesa de conversaciones se establece para **"liberar al país de la violencia política";** en consecuencia, el Gobierno debería tener claro el objetivo principal, y con base en éste, generar los objetivos específicos como acabar con las retenciones, el desarme, la desmovilización y la reinserción. Sin embargo, apuntó que Duque por ahora habla **"más de una rendición que de una negociación".**

El segundo error es **no conocer al oponente,** el experto afirmó que constantemente se dice que el ELN son una banda de narcotraficantes, que está dividido, que su Comando Central no tiene control sobre su base, y la realidad es diferente. Por lo tanto, de Currea reclamó que se conozca a la parte negociadora, para poder conversar con esta.

### **¿Cómo ve el apoyo que España ha expresado al proceso de paz?** 

Desde antes de la llegada del presidente del ejecutivo español, Pedro Sánchez a Latinoamérica para su gira por el continente, el mandatario expresó su voluntad para apoyar el proceso de paz entre el Gobierno colombiano y el ELN, hecho que fue saludado por la guerrilla. De igual forma, de Currea sostuvo que tanto **Sánchez como Beltrán habían expresado su deseo de que el ofrecimiento funcionara como un impulso** para la mesa de conversaciones en La Habana.

De otra parte, el experto ratificó que de concretarse la entrada de España como acompañante de las negociaciones, sería un movimiento muy importante, porque significaría que **la Unión Europea "ponga un pie en el proceso de paz".** Adicionalmente, sostuvo que no se trata de un debate sobre el lugar en que se desarrollan las conversaciones, porque Cuba y Venezuela le han apostado a la paz de Colombia mucho antes que este proceso, y tratar de marginar a estos países sería un acto de ingratitud.

### **"Uno termina creyendo más en la selección Colombia que en la paz en Colombia"** 

El experto afirmó que hasta ahora, "el proceso siempre ha sido un empuje y esperar que no nos falten los 5 centavos para el peso", pero eso no significa que se deba dejar de insistir en la posibilidad de una salida negociada al conflicto armado. De Currea reconoció que hay un sector en el Gobierno que sí desea la paz, pero advirtió que **si se deja el proceso en los sectores más retardatarios, "estaremos en problemas".**

<iframe id="audio_28284269" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_28284269_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en] [su correo](http://bit.ly/1nvAO4u) [o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por] [Contagio Radio](http://bit.ly/1ICYhVU) 
