Title: #Galería Así fueron las movilizaciones por Bogotá
Date: 2016-02-25 11:16
Category: Nacional, yoreporto
Tags: CUT, Movilizaciones Bogotá
Slug: galeria-asi-avanzan-las-movilizaciones-por-bogota
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/02/Rema-1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Rema 

##### [25 Feb 2016] 

Estudiantes, profesores, centrales obreras, movimientos sociales y ciudadanos indignados entre otros, se movilizan esta mañana por Bogotá con el objetivo de llegar hasta la Plaza de Bolívar, [para entregar un pliego de peticiones a la Administración Distrital](https://archivo.contagioradio.com/razones-de-la-movilizacion-en-bogota/). Usuarios nos comparten fotografías en Contagio Radio del avance en el recorrido que partió desde la calle 26 con carrera 30.

\
