Title: Servicio militar obligatorio es rechazado por los jóvenes en Colombia
Date: 2016-06-13 13:19
Category: DDHH, Nacional
Tags: #NoSirvoALaGuerra, Diálogos de paz Colombia, objeción de conciencia, servicio militar obligatorio
Slug: servicio-militar-obligatorio-es-rechazado-por-los-jovenes-en-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/06/Servicio-Militar-oposición-Colombia.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: ANDES ] 

###### [13 Junio 2016 ]

"Militarizar la juventud es sembrar la guerra hacia el futuro. **Sin desmilitarización de la vida social y juvenil será imposible lograr una paz estable y duradera**". Con esta premisa jóvenes de diversos sectores sociales muestran su respaldo a los esfuerzos de las guerrillas colombianas y el Gobierno nacional, por negociar la salida política al conflicto, y en ese sentido enviaron una comunicación a las delegaciones, en la que exponen sus propuestas para la paz y el buen vivir.

"Nos declaramos en desacato ante el servicio militar obligatorio y cualquier forma de reclutamiento forzoso", afirmaron los jóvenes, y es que de acuerdo con diversas ONGs **un campesino o joven de estrato 0, 1 o 2 tiene 42 veces más posibilidades de ser forzado a ir a la guerra**, que alguien de estrato 4, 5 o 6. Quienes no se hayan graduado, tienen ocho veces más probabilidades de morir en el Ejército y actualmente hay 764.161 jóvenes remisos.

Así mismo, el 74% de los jóvenes que sufrieron daños físicos y/o psicológicos durante el servicio militar fueron soldados regulares, el 11% bachilleres y el 15% campesinos. El 88% de los hombres que prestaron el servicio militar no continuaron con la carrera, y **entre 1993 y 2015 se registraron 1.294 jóvenes muertos prestando el servicio militar**.

Teniendo en cuenta lo que ha implicado para la juventud el conflicto armado en Colombia, centenares de jóvenes a través de redes sociales han dicho NO MÁS \#NoSirvoALaGuerra, insisten en que **"ningún joven deben morir por la patria" y "las universidades deben ser territorios de paz"**, así que se bajan del camión para bajarse de la guerra, para ir por la paz, la reconciliación y la vida y para que "el sonido de los cañones se convierta en guitarras, arpas y tambores".

Conozca la carta enviada a La Habana:

[![Propuesta jóvenes paz](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/06/Propuesta-jóvenes-paz.jpg){.aligncenter .size-full .wp-image-25302 width="927" height="1200"}](https://archivo.contagioradio.com/servicio-militar-obligatorio-es-rechazado-por-los-jovenes-en-colombia/propuesta-jovenes-paz/)

[![Propuesta jóvenes paz.](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/06/Propuesta-jóvenes-paz..jpg){.aligncenter .size-full .wp-image-25303 width="927" height="1200"}](https://archivo.contagioradio.com/servicio-militar-obligatorio-es-rechazado-por-los-jovenes-en-colombia/propuesta-jovenes-paz-2/)

[![Propuesta jóvenes paz..](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/06/Propuesta-jóvenes-paz...jpg){.aligncenter .size-full .wp-image-25304 width="927" height="1200"}](https://archivo.contagioradio.com/servicio-militar-obligatorio-es-rechazado-por-los-jovenes-en-colombia/propuesta-jovenes-paz-3/)

[![Propuesta jóvenes paz...](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/06/Propuesta-jóvenes-paz....jpg){.aligncenter .size-full .wp-image-25305 width="927" height="1200"}](https://archivo.contagioradio.com/servicio-militar-obligatorio-es-rechazado-por-los-jovenes-en-colombia/propuesta-jovenes-paz-4/)

 
