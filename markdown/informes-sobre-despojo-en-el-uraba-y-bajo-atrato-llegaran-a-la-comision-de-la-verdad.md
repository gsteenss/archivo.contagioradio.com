Title: Informes sobre despojo en el Urabá y Bajo Atrato llegarán a la Comisión de la Verdad
Date: 2019-11-21 06:45
Author: CtgAdm
Category: DDHH, Nacional
Tags: acaparamiento, Bajo Atrato, conflicto armado, despojo, tierras, uraba
Slug: informes-sobre-despojo-en-el-uraba-y-bajo-atrato-llegaran-a-la-comision-de-la-verdad
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/informe-despojo-y-desplazamiento-uraba-y-bajo-atrato.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

El próximo 2 de diciembre en Apartadó, Antioquia, cuatro organizaciones defensoras de derechos humanos realizarán la entrega de dos informes a la Comisión de la Verdad referidas al **despojo territorial en el Bajo Atrato y el Urabá** . Los documentos pretenden identificar responsabilidades y dinámicas con las que operaron distintos actores para la apropiación y redistribución de la tierra y la identificación de las afectaciones a las comunidades étnicoterritoriales y campesinas de estas regiones del país.** **

De acuerdo con los informes, más de 150 mil hectáreas de tierra fueron despojadas en el marco del desarrollo de o**peraciones de tipo paramilitar en el Urabá y Bajo Atrato**, que a partir de estrategias de violencia como desplazamiento forzados, asesinatos selectivos, desapariciones forzadas y masacres, pretendieron imponer un modelo de desarrollo en esta región, fundamental para el comercio internacional. (Le puede interesar:["Cinco casos por los que la JEP pone la lupa a crímenes en Urabá y el Bajo Atrato Chocoano"](https://archivo.contagioradio.com/cinco-casos-los-la-jep-pone-la-lupa-crimenes-uraba-atrato-chocoano/))

Todo ello, según Marta Peña, integrante del Instituto Popular de Capacitación, con la finalidad de contribuir a un **"modelo de acumulación por desposesión",** que contó con prácticas sistemáticas y planificas en el tiempo, para dar paso a la expansión agrícola de sectores como los palmeros, bananeros, ganaderos y madereros.

A su vez, los estudios también hacen un análisis de las dinámicas de violencia y su relación con el despojo, desatadas desde 1996, con el desarrollo de dos formas del ejercicio paramilitar, la legal a través de la creación de las CONVIVIR y la ilegal, a través de estructuras o grupos armados identificados como las Autodefensas Campesinas de Córdoba y **Urabá**, organizadas en distintos frentes que correspondían a una distribución territorial y a intereses de un diverso portafolio de negocios.

Por esta razón, Gerardo Vega, director de Forjando Futuros, aseguró que comprender las relaciones que se gestaron en torno al despojo de tierras entre quienes gobernaban, los sectores empresariales y los grupos armados, es un paso a la no repetición. "Si no se devela, qué pasó y quiénes lo patrocinaron, fomentaron y financiaron, lo volverán a hacer", concluyó Vega.

Asimismo en los documentos se expone las afectaciones de las comunidades tras los hechos de violencia, entre ellos la imposibilidad de generar un modelo agrario de economía campesina, porque se impuso un modelo agroexportador, **los daños ambientales que provocaron la destrucción de ecosistemas como selvas tropicales o la desviación de fuentes hídricas.**

### **La tarea de la Comisión de la Verdad**

Los informes buscan que la Comisión de la Verdad inicie su trabajo en torno al esclarecimiento de los hechos, apelando a 4 mandatos de los 13 que tiene este organismo. El primero de ellos consiste en develar** los factores y condiciones que facilitaron o contribuyeron a la persistencia del conflicto,** teniendo en cuenta como insumo los informes de la Comisión Histórica del Conflicto y sus Víctimas, entre otros.

El segundo, sobre ** el desarrollo del conflicto,** en particular la actuación del Estado, de las guerrillas, de los grupos paramilitares y el involucramiento de diferentes sectores de la sociedad, teniendo en cuenta las relaciones que se tejieron en la región del Bajo Atrato y el Urabá en torno al despojo de tierras. (Le puede interesar: ["«Van por nuestras tierras»: Informe entregado a la JEP sobre despojos en Urabá"](https://archivo.contagioradio.com/van-por-nuestras-tierras-informe/))

El tercero en torno al **fenómeno del paramilitarismo,** en particular sus causas, orígenes y formas de manifestarse; su organización y las diferentes formas de colaboración con esta, incluyendo su financiación; así como el impacto de sus actuaciones en el conflicto.

Y finalmente se encuentra el** que consiste en las causas del desplazamiento forzado y despojo de tierras con ocasión del conflicto y sus consecuencias.** (Le puede interesar: ["Comisión de la Verdad expuso sus avances tras un año de trabajo"](https://archivo.contagioradio.com/comision-de-la-verdad-expuso-sus-avances-tras-un-ano-de-trabajo/))

En ese sentido, Danilo Rueda, secretario ejecutivo de la Comisión Intereclesial de Justicia y Paz, aseguro que "lo fundamental es señalar con las afecciones generadas a comunidades afro, indígenas y campesinas, qué es necesario que el país conozca, y eso que se podrá saber dependerá de las conclusiones que la Comisión de la Verdad exprese".

Algunas de esas conclusiones, para Rueda, tendrían que girar en torno a responder por qué un sector empresarial se acuñó para la acumulación de las riquezas con el uso de la violencia y como se interactuó con las estrategias paramilitares para hacerse a la tierra, desconociendo derechos humanos y las leyes nacionales.

Las organizaciones que presentarán estos informes son la Comisión Intereclesial de Justicia y Paz, la Corporación Jurídica Libertad, Forjando Futuros y el Instituto Popular de Capacitación. La entrega se realizará en el Parque de los Encuentros, y se espera que más de 200 víctimas de esta región lleguen hasta este lugar para hacer la entrega oficial de los documentos a los comisionados de la verdad.
