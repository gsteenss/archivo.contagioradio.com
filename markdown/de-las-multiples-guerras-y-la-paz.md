Title: De las múltiples guerras y la paz
Date: 2017-12-29 13:25
Category: Camilo, Opinion
Tags: amenazas a líderes sociales, ELN, lideres sociales, paz
Slug: de-las-multiples-guerras-y-la-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/12/paz-y-guerra.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Contagio Radio 

#### Por [Camilo De Las Casas](https://archivo.contagioradio.com/camilo-de-las-casas/) 

#### **29 Dic 2017** 

Entre la continuidad de la guerra por otro medios contra las FARC EP,  hoy partido político, el conflicto socio político y armado con el ELN, las disidencias de las FARC, que aún no se han caracterizado adecuadamente en los análisis, la neoparamilitarización y las nuevas formas de criminalidad, sigue en forcejeo la paz positiva.

166 líderes asesinados en dos años son el sello de los que apuestan por la pax neoliberal que sigue el presupuesto, en muchos casos, no en su totalidad, del pensamiento del enemigo interno o del uso de la violencia para asegurar la riqueza mal habida o para acumular más. Da la mismo uno que 100, decía Guillermo Cano, respecto a los desaparacidos forzados en los 80. Aquí escribimos, da lo mismo que sea un asesinato de los 166 por móviles políticos, eso ya es grave. Podemos sostener que si hay sistematicidad en un alto porcentaje de los asesinatos de líderes sociales por los móviles, el modos operandi y la estructura responsable.

Otro síntoma escándaloso es la Colombia, segundo país más desigual de América Latina, después de Haiti y el séptimo en el mundo, según el Banco Mundial. Así, 13,5 millones viven en condición de pobreza, sin acceso a servicios públicos básicos y sin condiciones de vida digna. Un altísimo porcentaje de ellos víctimas del desarraigo por la violencia, el acaparamiento de tierras despojadas por empresas legales y las articuladas a la ilegalidad. En fin ambas, absolutamente ilegítimas por sus abusos y arbitrariedad

Y qué no decir, de aquella otra exclusión de vidas. Las forestales, aguas, animales, páramos por la desarticulación del Sistema Nacional Ambiental de Colombia, y una erosión de la institucionalidad ambiental que accede a  decisiones políticas y económicas propias de la pax neoliberal y su corporativismo.

Más allá de que la  Corte Constitucional declaró como sujeto de derechos al río Atrato, por ejemplo, o el gran potencial de las consultas populares como mecanismo de participación ciudadana para decidir sobre asuntos ambientales o la gran acogida de iniciativas del movimiento animalista, persiste la desconexión de las crisis climática en sus diversas manifestaciones con cambios de lógicas y políticas sociales de inclusión que le son inherentes.

Así, entre esa fenomenología, a la que podemos agregar el entierro de la Reforma Política, y los irés y venires de la llamadas Circunscripciones de Paz, las trampas oficiales al cese de fuegos con el ELN, el patente cese unilateral de los neoparamilitares de las AGC, se encuentra la construcción de la paz con justicia socioambiental.

El poder conciente de grupos de base en  regiones, de expresiones urbanas vinculadas al ambientalismo, al animalismo y nuevas formas de expresión de espiritualidades sin fórmulas religiosas ortodoxas (incluso de la liberación), entre ellas las del Arzobispo de Cali, y  cientos más, así como, una nueva construcción transversal, fresca y joven en el ejercicio de la discusión, distante de miradas sectarias partidistas o caudillitas, está creciendo. Ese poder conciente respeta a los candidatos que se lanzan hacia lo  nuevo, desde odres nuevos, se acercan al 39 % de gente que votaría en blanco, y a aquellos que siendo caudillos están construyendo un sentido de patria sin exclusiones.

Ese poder conciente sin ser ascéptico en lo electoral ni desconociendo la exclusión socio política y económica, asume la ruptura de la lógica amigo enemigo. Lo nuevo ha sido buscar resolver las fracturas históricas de los llamados movimientos o agrupaciones politicas y sociales alternativas, causados en falsos dilema ideológicos, que han hecho ver a los cercanos como enemigos. Y son aventurados en la apuesta de lo nuevo, asumiendo lo que antes era mirado como enemigo de clase, cómo un sujeto contradictor que es posible incluir en otro proyecto de país, sin su negación ni eliminación.

Inclusión, una apuesta de resolución a la exclusión, a las miradas permanentes de una negación, a observar el cambio del alma existente en la sociedad. La inclusión de los victimarios y de los sujetos victimizantes sin esperar que ellos cambien, transformando sí, el propio sujeto ciudadano . El cambio no depende de ellos, es mío y nuestro.

Es cómo dice Mindell, el cuerpo que sueña. El cuerpo nación que se levanta de su letargo y que despierta del engaño. Estamos en los tiempos del cazador, el de Castaneda. La realidad es más allá de lo bueno y lo malo, de la paz negativa como distinta a la paz positiva. La realidad cómo es, en la percepción de que están por ocurrir cosas inusuales. En medio de la implacable realidad de lo injusto está brotando lo justo. Y allí, en esa coción a fuego lento, el alma vieja en lo nuevo está germinando ese nuevo país.

Nada se soluciona inmediatamente., ¡Nada! Pero muchas, miles de personas están cambiando la forma de pensar sobre la guerra. Algo se está moviendo, más allá de las lecturas derrotistas o bipolares. Eso que se mueve no siempre es racional. Eso irracional está creciendo borrando miles de fronteras y uniendo lo resquebrajado por la mentira, la miseria y la violencia. El amor transformante  que hace posible la verdad de las verdades y la armonía en la justicia está ahí en la paz positiva que está en lo negativo. Un honor a la memoria, a los ancestros, a las visiones enterradas, a los espíritus prohibidos es donde se encuentra la reconciliación.

Titula Andrés Mauricio Muñoz en su último libro de cuentos:"Hay días en que estamos idos". Historias de cotidianos que se pierden de la realidad o que la niegan, hasta que la propia pulsión de la vida los pone en la realidad.

Hoy en nuestra Colombia  estamos regresando a la realidad. Se han ido cayendo los falsos pretextos y nuestras propias imágenes de nosotros mismos. Estamos viéndonos tal cómo somos, abriendo una nueva cultura, la de la paz, que parece distante e incierta, pero que ya está naciendo. Entre las múltiples guerras, la paz.

#### [Leer más columnas de opinión de  Camilo de las Casas](https://archivo.contagioradio.com/camilo-de-las-casas/)

------------------------------------------------------------------------

######  Las opiniones expresadas en este artículo son de exclusiva responsabilidad del autor y no necesariamente representan la opinión de Contagio Radio.
