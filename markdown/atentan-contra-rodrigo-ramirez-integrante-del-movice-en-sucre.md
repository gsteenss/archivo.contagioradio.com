Title: Atentan contra Rodrigo Ramírez integrante del MOVICE en Sucre
Date: 2016-12-14 09:28
Category: DDHH, Nacional
Slug: atentan-contra-rodrigo-ramirez-integrante-del-movice-en-sucre
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/finca_la_europa.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: archivo] 

###### [14 Dic 2016] 

El hecho se produjo en Sincelejo, Sucre cuando Rodrigo Ramírez se dirigía hacia Colosó a una reunión con campesinos y campesinas para trabajar el tema de restitución de tierras, en el marco de labores de **acompañamiento que desarrolla el MOVICE en esa región.**

Según las primeras informaciones los atacantes se acercaron a la residencia de Rodrigo y uno de ellos desenfundó el arma pero encontró la reacción del esquema de protección de Argemiro Lara, también integrante de MOVICE, en medio de los disparos uno de los atacantes resultó herido pero alcanzaron a huir, **abandonando una de las motocicletas que no tiene placas.**

Los integrantes del Movimiento de Víctimas de Crímenes de Estado en el departamento de Sucre han denunciado **por lo menos 40 amenazas y varios atentados**. Hasta el momento no hay respuestas sobre quienes realizan los atentados o amenazas o sobre desarticulación de las estructuras paramilitares que operan en la región. (También le puede interesar [MOVICE sigue siendo víctima de persecución](https://archivo.contagioradio.com/?s=movice))

El MOVICE ha denunciado el agravamiento de las situaciones de seguridad y de riesgo que afrontan defensores de DDHH en todo el territorio nacional, pero sobre todo en el departamento de Sucre, en el que los intereses anti restitución de tierras operan a sus anchas en contra de campesinos campesinas.

###### [Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)]] 
