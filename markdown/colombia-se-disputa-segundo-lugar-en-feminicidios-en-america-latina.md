Title: Colombia se disputa segundo lugar en Feminicidios en América Latina
Date: 2017-05-16 17:00
Category: Libertades Sonoras, Mujer
Tags: Asesinatos, feminicidio, Ley Rosa Elvia Cely, mujeres
Slug: colombia-se-disputa-segundo-lugar-en-feminicidios-en-america-latina
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/05/mujeres-feminicidio.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Red Chilena Contra la violencia Hacia las Mujeres] 

###### [16 May. 2017]

**Colombia es el país que se está disputando de manera lamentable el segundo lugar con mayor número de feminicidios** en América Latina, con Salvador y México, todos los días nos levantamos con una trágica noticia en la que vemos cómo asesinan a las mujeres por su condición.

Por eso en **\#LibertadesSonoras** quisimos hablar sobre éste tema, dar algunos tips para que comunicadores y comunicadoras sepan cómo abordar el tema con una perspectiva de género. Además de clarificar algunos temas a propósito de los términos y la legislación existente en Colombia.

Para ello, estuvimos en nuestro programa acompañadas de **Isabel Agatón, abogada, poeta y feminista** quien además nos expuso las importancia de narrar las realidades de las mujeres y las violencias existencias de otras maneras.

"No es solamente el responsable de un feminicidio un conocido, sino también un desconocido y ocurre en todos los lugares del mundo (...) mientras antes de la firma de los Acuerdos de Paz eran asesinado 20 hombres en combate, hoy día, **después de la firma de los Acuerdos son asesinadas 60 mujeres en el marco de la violencia de pareja, pero no es el único ámbito" relató Agatón.**

Además estuvimos en las calles, escuchando qué es lo que piensa y sabe la gente acerca del Feminicidio.

Por último, hablamos de **cómo hacer para llegar a la sociedad civil con temas tan sensibles pero importantes como los son los derechos de las mujeres**, las violencias de género, los feminicidios y las luchas que se continuarán haciendo para construir una nueva sociedad que respete a las mujeres.

<iframe id="audio_18750089" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_18750089_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio]{.s1}](http://bit.ly/1ICYhVU)[.]{.s1}

<div class="ssba ssba-wrap">

</div>
