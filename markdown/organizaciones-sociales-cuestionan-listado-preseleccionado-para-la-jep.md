Title: Las preocupaciones de las víctimas ante preseleccionados a la JEP
Date: 2017-09-21 17:17
Category: Nacional, Paz
Tags: comision de la verdad, Comite de Escogencia, JEP, Justicia Penal Militar, Tribunal de Paz
Slug: organizaciones-sociales-cuestionan-listado-preseleccionado-para-la-jep
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/05/Justicia.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo] 

###### [21 Sept. 2017] 

Las preocupaciones de sectores de organizaciones de DD.HH. y víctimas giran en torno a los nombres de varios preseleccionados para ocupar los cargos del Tribunal Especial para la Paz y de las salas de la Jurisdicción Especial para la Paz - JEP-.

Manifiesta jose Humberto Torres que en un primer momento muchas de las personas no cumplen con los requisitos. Existen fiscales, jueces y magistrados que durante la semana administran justicia con saco y corbata y el fin de semana se ponen un uniforme militar y hace ejercicios militares. Le puede interesar: [La ardua tarea que tendrán los integrantes de la Comisión de la Verdad](https://archivo.contagioradio.com/comienza-convocatoria-para-integrantes-de-la-comision-de-la-verdad/)  
Todo lo anterior, concluye **el jurista, podría generar la vulneración del principio de imparcialidad "y eso lo hemos cuestionado**. Por ejemplo, llevamos un caso a la Comisión Interamericana de Derechos Humanos. Estamos preocupados, con las alarmas encendidas porque dudamos por supuesto de la imparcialdidad de estas personas cuando tengan que juzgar a individuos que en el pasado consideraron enemigos y para juzgar a sus amigos”.

### **Lo que dicen las Organizaciones de Derecho Humanos** 

“Hemos visto a personas que en nuestro criterio no deberían estar allí en razón de una característica básica y esencial de una judicatura que tiene que ver con la independencia e imparcialidad que debe caracterizarla y **nos preocupa que abogados, defensores de derechos humanos y de víctimas** fueron descabezados y no considerados en la lista de la preselección, como Eduardo Matías, Danilo Guarín de Cali o Miguel Puerto experto en derechos humanos e internacional”.

Lo que podría traducirse en que no hubo una selección para que tuviera de lado y lado candidatos, sino que de alguna manera pesaron más las listas que elaboró y entrego el ex Magistrado Leonidas Bustos hoy cuestionado por hecho de corrupción, afirma el abogado.

### **En el listado hay un amplio número de personas de la Justicia Penal Militar** 

Encontrar varios nombres de personas que han trabajado en la justicia penal militar es otra de las preocupaciones de las organizaciones sociales y de derechos humanos, porque, dicen ellos, de alguna manera **su condición hace que se dude de su imparcialidad y transparencia en las decisiones. **Le puede interesar: [Las recomendaciones de las víctimas para la Unidad de Búsqueda de desparecidos](https://archivo.contagioradio.com/las-recomendaciones-de-las-victimas-para-el-funcionamiento-eficaz-de-la-uniddad-de-busqueda-de-desparecidos/)

“Pero sobre todo lo que está de por medio es el juzgamiento no solo de los integrantes del Estado Mayor de las FARC y contra quienes cometieron delitos de lesa humanidad, sino que también está la presentación de casos de personas que han estado involucrados en Ejecuciones Extrajudiciales y el 90% de los militares que están haciendo cola para la JEP están comprometidos en esos hechos”.

<iframe id="audio_21022209" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_21022209_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)
