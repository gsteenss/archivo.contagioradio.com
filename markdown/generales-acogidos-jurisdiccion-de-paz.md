Title: Tres generales serían los primeros en acogerse a Jurisdicción de Paz
Date: 2017-03-06 16:30
Category: Judicial, Nacional
Tags: Generales, Jurisdicción Espacial de Paz
Slug: generales-acogidos-jurisdiccion-de-paz
Status: published

###### [Foto: usahispanicpress] 

###### [06 Mar 2017]

Cuando se está a la espera del listado de integrantes de las FARC y de la Fuerza Pública que se verían beneficiados por la amnistía Contagio Radio logró establecer que habría por lo menos 3 generales que estarían próximos a acogerse a ese beneficio y quedarían a la espera de la entrada e funcionamiento de la Jurisdicción Especial de Paz.

En principio se ha conocido que el grupo de militares estaría integrado por cerca de 150 militares, entre ellos 3 generales entre los que figuran el **General ® Rito Alejo Del Río, procesado y condenado por el asesinato de Marino López en el marco de la operación Génesis, y el General Jaime Alberto Uscátegui responsable en la Masacre de Mapiripan** y el Mayor Cesar Alonso Maldonado condenado por varios llamados “Falsos Positivos”.

De la lista también harían parte 7 Coroneles y 21 Mayores también procesados por violaciones de DDHH que serán conocidas por la Jurisdicción Especial de Paz. El listado que deberá ser entregado por el Ministerio de Defensa a los respectivos juzgados estaría compuesto, en su mayoría, por militares en retiro, aunque, **cerca de una decena están en servicio activo.** [(Lea también: Jurisdicción especial de paz incluirá a terceros en la guerra)](https://archivo.contagioradio.com/jep-colombia-debate-paz/)

### **Las víctimas y el centro del acuerdo** 

Frente a estas medidas son diversos los pronunciamientos de organizaciones de víctimas y de defensores de Derechos Humanos que han presentado sus preocupaciones puesto que **la impunidad en Colombia que alcanza el 90% y en su mayoría obedece a procesos contra agentes estatales como integrantes de la Fuerza Pública**, ex agentes de organismos de seguridad, entre otros. ([Le puede interesar: Extrema derecha le teme a la JEP](https://archivo.contagioradio.com/sectores-de-ultraderecha-y-empresarios-le-temen-a-la-jep-ivan-cepeda/))

Por este tipo de situaciones es que esas organizaciones de víctimas y de DDHH siguen manifestando que la cadena de mando y la investigación de la responsabilidad de altos mandos militares **no podrían quedar por fuera de las investigaciones ni de las sanciones, que serían la única garantía de verdad a la que tendrían acceso las víctimas.** ([Lea también: Los reparos de las víctimas en debate de la JEP](https://archivo.contagioradio.com/jurisdiccion-especial-debe-beneficiar-a-las-victimas/))

Se espera que los militares se acojan a lo ordenado por la Jurisdicción Especial de Paz y relaten la verdad en cuanto a sus responsabilidades y la de otros militares o civiles en la comisión de las violaciones a los Derechos Humanos por las que fueron procesados en la jurisdicción ordinaria.

###### Reciba toda la información de Contagio Radio en [[su correo]
