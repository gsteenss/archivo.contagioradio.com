Title: Los puntos gruesos de la JEP cruciales en la aprobación del articulado
Date: 2017-10-03 17:39
Category: Nacional, Paz
Tags: Cambio Radical, Centro Democrático, JEP
Slug: tres-puntos-gruesos-de-la-jep-que-serian-cruciales-en-la-aprobacion-del-articulado
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/08/Congreso-Colombia-e1507068163688.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Congreso Visible] 

###### [3 Oct 2017] 

En la sesión de este lunes se aprobaron 102 de los 167 artículos de la ley estatutaria de la Jurisdicción Especial de Paz, hecho que fue calificado como un respiro a la implementación del acuerdo en el congreso y como "una de las pocas cosas positivas" que se han presentado en los últimos días por parte de Voces de Paz. Sin embargo, quedan varios puntos neurálgicos en los que seguramente los congresistas del Centro Democrático y los de Cambio Radical tendrán mucho que decir.

### **La participación de las víctimas como partes procesales ante la JEP** 

Una de las proposiciones que hacen parte de las 102 presentadas para discutir esta Ley estatutaria fue la presentada por los congresistas Ángela María Robledo, Alirio Uribe e Iván Cepeda. La propuesta es que las víctimas puedan presentarse ante la JEP como partes procesales, lo que significa que podrían recopilar información y entregarla directamente a las salas. Una labor que podría ser de la fiscalía de la jurisdicción que sería complementada con la información que tienen las víctimas.

Son 40 proposiciones las que han sido incluidas en un primer bloque por la comisión de estudio y serían parte de un primer bloque para votación. Estas incluyen que el Estado garantice la asistencia técnica a las víctimas que deciden participar en la JEP.

### **Juzgamiento de terceros en la guerra** 

Una de las preocupaciones de Cambio Radical ha sido la posibilidad de que se juzgue a terceros involucrados por financiación, participación directa u omisión en crímenes en el marco del conflicto armado. Uno de los casos conocidos ha sido el de la empresa Chiquita Brands investigada por, presuntamente, participar con aportes a los paramilitares de las AUC. Para los congresistas de Cambio Radical y del Centro Democrático, la JEP abre la posibilidad de que empresarios que han actuado de buena fe sean juzgados.

Este conjunto de normas podría incluir también la responsabilidad de terceros civiles como ministros y dueños de medios de información, entre otros, que hayan participado en actividades propias del conflicto, independientemente si fueron o no determinadores de crímenes, es decir, que si se dieron ordenes que pudieron derivar en la comisión de delitos tendrían que ser juzgados ante la JEP.

### **Altos mandos y cadena de mando en la JEP** 

Tanto el Centro Democrático, Cambio Radical y un sector de militares han manifestado su desacuerdo en permitir que las investigaciones de la JEP lleguen hasta el posible juzgamiento de altos mandos de las FFMM que tengan responsabilidad sobre las actuaciones de las tropas bajo su mando. Sin embargo, varios de los puntos incluidos contemplan la posibilidad de amplia defensa y aplicación de un marco de justicia similar para los integrantes de la Fuerza Pública.

Durante la primera hora de la discusión se ha planteado la posibilidad de aprobar las 102 proposiciones en bloque,avalando casi 40 artículos que tienen que ver con la posibilidad de participación de las víctimas.

### **Los derechos y participación política de quienes se someten a la JEP** 

Otra de las resistencias de varios grupos de congresistas tiene que ver con la posibilidad de la partición política de los integrantes de FARC en particular, y en general de quienes se presentan a la Justicia Especial. El planteamiento de los opositores del acuerdo contempla que no deberían acceder a derechos políticos las personas que estén incursas en dicha jurisdicción.

No obstante, tanto el Ministro del Interior como los integrantes de Voces de Paz han manifestado que es necesario que se mantengan los derechos políticos puesto que este es un punto clave del acuerdo y uno de los que posibilitó la firma del acuerdo. Además recordaron que el partido político de las FARC será el único que por norma constitucional se tendrá que someter a la JEP.

Por su parte Alirio Uribe recordó que ya está en la Constitución que los integrantes de las FARC pueda participar de manera inmediata en la política. Según el congresista esta posibilidad está incluida en el Acto legislativo 01, en el artículo 20. Es decir ya está aprobado y no puede derogarse por una ley estatutaria.

*Nota: Al cierre de este artículo se aprobó el conjunto de proposiciones sobre las víctimas como intervinientes.*

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
