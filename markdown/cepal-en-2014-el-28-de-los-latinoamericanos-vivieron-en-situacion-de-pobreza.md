Title: Cepal: En 2014 el 28% de los latinoamericanos vivieron en situación de pobreza
Date: 2015-01-28 22:00
Author: CtgAdm
Category: Economía, infografia, Otra Mirada
Tags: América Latina, CELAC, Cepal, pobreza
Slug: cepal-en-2014-el-28-de-los-latinoamericanos-vivieron-en-situacion-de-pobreza
Status: published

##### Foto: Cepal.org 

Este miércoles, los países latinoamericanos que hacen parte de la Comunidad de Estados Latinoamericanos y Caribeños (CELAC) se congregaron con el objetivo de dialogar sobre la manera de enfrentar la pobreza.

Durante la reunión, se presentó el informe **“Panorama Social de América Latina 2014”, de la Comisión Económica para América Latina y el Caribe (Cepal)**, donde se evidencia que en 2014, había 167 millones de personas que se encontraban en situación de pobreza, de ellas 71 millones vivían en condición de indigencia.

En el documento también se concluye que **Uruguay es el país de Latinoamérica con el índice más bajo de pobreza, un 5,7%.** Para que esto suceda, de acuerdo al informe, la educación es la variable más importante para que esta nación tenga el menor porcentaje de personas que viven en situación de pobreza.

En la apertura de la cumbre, el presidente de Cuba, Raúl Castro, pidió al presidente Barack Obama, para que Estados Unidos termine con el embargo de la isla. Así mismo, los gobernantes de América Latina y el Caribe **impulsaron el fin al embargo, con el objetivo de que se logre normalizar las relaciones diplomáticas entre ambos países.**

Por su parte, Colombia fue de los cinco países con mayores índices de reducción de pobreza, en 2012 el país contaba con un 32,9% del total de la población en esta situación de pobreza, **en el 2013 bajó al 30,7%.**

En la siguiente infografía se resumen algunos de los resultados más representativos que se recogieron del informe de la Cepal.

[![infografias indices de pobreza](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/01/infografias-indices-de-pobreza-855x1024.jpg){.aligncenter .wp-image-3908 .size-large width="604" height="723"}](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/01/infografias-indices-de-pobreza.jpg)
