Title: En “Panikós” todos somos sospechos
Date: 2018-06-20 15:04
Category: eventos
Tags: Bogotá, teatro
Slug: en-panikos-todos-somos-sospechos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/05/IMG_0312.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Medea 73 

###### 31 May 2018 

La Sala, Fábrica de Hechos Culturales y la compañía Medea 73 celebrará su primer aniversario con el estreno de la obra "Panikós", una pieza escrita y dirigida por Lorena Briscoe junto al reconocido actor colombiano Bernardo García.

La obra Panikós nos lleva a un lugar en donde es habitual hablar y pensar en seres civilizados contemporáneos que cada día el miedo es la noticia que los entretiene y laparanoia colectiva se convierte para ellos un lugar común para encontrarse con ese otro a quien consideramos desconocido. Un performance que se conectara con el público para transmitir los medio tonos que quedan por llenar, en los que se cuestiona al otro, quién es,  
que hace y la ardua tarea de recocerse a uno mismo.

La obra trae consigo una singular organización de la sala. Los asistentes se ubicaran en círculo en torno a los personajes, donde el público pasa de ser un observador aun participante para hablar sobre las maneras de identificar a un terrorista. Ejercicio que permite a los protagonistas y sus actuaciones atravesar a los espectadores, que ellos mismo duden de su forma de pensar, actuar y hasta de sus propios valores.

Basada en el libro Ciudad Pánico del autor Paul Virilo la obra Panikós se presentara del 21 al 23 de junio, de jueves a sábado a las 8:00 p.m. con un bono de apoyo general 30.000 en La Sala Fábrica de Hechos Culturales Carrera 22 \# 41-28 , si desea saber más información de la obra y del lugar puede ingresar a [www.lasala.co](http://www.lasala.co)
