Title: Simón Trinidad solamente tiene contacto con humanos 3 horas al día: Abogado Mark Burton
Date: 2017-09-09 11:54
Category: Entrevistas, Nacional
Tags: Heike Hänsel, Mark Burton Abogado Simón Trinidad, simon trinidad
Slug: entrevista-con-mark-burton-su-abogado
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/09/abogado-simoni-trinidad.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [ Foto: Adriana Yee Meyberg] 

###### [09 Sept 2017] 

[“Se lee Dignidad, se escribe Simón Trinidad” – La Campaña por su liberación sale al ruedo internacional. Entrevista exclusiva con Mark Burton, abogado defensor del prisionero Político Simón Trinidad y participante en la gira internacional de la campaña por su liberación. ]

#### [**Por Adriana Yee Meyberg**] 

*[Tras casi 14 años de reclusión en la cárcel de máxima seguridad en Colorado, Estados Unidos, resurge con fuerza el impulso para ver a Simón Trinidad, el exlíder pedagógico y político de las FARC, en libertad.]*

*[Colombia transita un momento histórico hacia un estado en el que sea posible la reconciliación y la construcción de paz. Los diálogos en la Habana entre las Fuerzas Armadas Revolucionarias de Colombia-Ejército del Pueblo -FARC-EP- y el estado Colombiano dejaron como resultado un acuerdo de paz, el  mejor y único posible, el cual además cuenta con un componente de Justicia Especial para la Paz -JEP- que se ha convertido en referente mundial de vanguardia en cuanto a justicia transicional se refiere.  ]*

*[En tiempos de cambio, en los que también se ha logrado un cese bilateral al fuego entre el Ejército de Liberación Nacional -ELN- y el gobierno, se hace cada vez más imperiosa la necesidad de luchar por la liberación de Simón Trinidad, un preso político que en esta coyuntura debería ingresar a la JEP si es que se quiere lograr una base consistente y coherente para una Colombia en Paz basada en principios de justicia, reconciliación y reparación.]*

*[En Julio de 2017 nació la campaña por su liberación como resultado de la convergencia de múltiples movimientos sociales. Con la presencia activa de su abogado Mark Burton y gracias a redes de apoyo civil, se ha lanzado la gira recientemente a nivel internacional. En su primera parada en Berlín, Alemania y tras reunirse con la parlamentaria alemana Heike Hänsel y con el Colectivo Ciudadano Unidos por la Paz-Alemania,  pudimos indagar más sobre el caso de Simón Trinidad, la campaña y las perspectivas]*

**Adriana Yee Meyberg (AY):** *[“Se lee Dignidad, se escribe Simón Trinidad”]*[ ese es el nombre de la campaña. ¿Por qué decidieron nombrarla así? ¿Cómo surgió?]

**Mark Burton (MB):**[ Se quiere mostrar la idea de que Simón Trinidad es un símbolo de dignidad en Colombia. Es una persona que ha resistido la violencia de estado, en toda su lucha y toda su vida es una persona digna.  ]

[Hay gente que está interesada en el caso de Simón Trinidad, hay grupos de derechos humanos involucrados, el movimiento social Marcha Patriótica. La campaña nace del esfuerzo conjunto de diversos grupos en España y en Colombia que se han coordinado y decidieron echar a rodar esta iniciativa.  Es el momento y la coyuntura apta para ejercer presión y poner una vez más en el ojo público la urgencia de liberar a Simón Trinidad, tras años de injusticia.]

**AY:**[ Simón Trinidad lleva 13 años encarcelado en Estados Unidos. ¿Qué cargos se le imputan exactamente?]

**MB:**[ Se le imputaron cargos de importación de drogas y de toma de Rehenes, pero solamente lo hallaron culpable del cargo de conspiración en toma de rehenes, que es ridículo porque no tuvo nada que ver con esos prisioneros de guerra, ni siquiera rehenes. Eran combatientes a pesar de ser civiles pues trabajaban para el cuerpo militar. Eran tres estadounidenses filmando las posiciones en el campo de las FARC, enviando el video y la información al Southern Command del ejército en los EE.UU. Bajo el derecho internacional humanitario en el marco del convenio de Ginebra, ellos quedarían catalogados como prisioneros de guerra.]

**AY:**[ Y si eso es así, ¿cómo pudieron durante el juicio aportar pruebas de que lo que se le imputaba efectivamente era un acto por el que se le podía juzgar? Si no había evidencia de ello ¿cómo probaron su culpabilidad?]

**MB:**[ Ellos llevaron varios testigos que eran mentirosos, desertores de las FARC que declararon que Simón Trinidad era el jefe de secuestros en la costa caribe de Colombia y otras cosas que son totalmente falsas. Yo creo que el jurado quizá tampoco creía mucho en ellos. Sin embargo, también llevaron a atestiguar a un diplomático Colombiano que dijo que las FARC eran culpables de crímenes de Lesa Humanidad, pero en verdad los reportes de la ONU  muestran que es el gobierno el más culpable de este tipo de crímenes.]

[El jurado sabía que Simón Trinidad fue a Ecuador para buscar a un señor de las Naciones Unidas que se llama James Lemoyne que él conoció durante las conversaciones de paz en San Vicente del Caguán, Colombia. Las FARC querían tener otro diálogo con el gobierno sobre intercambio de prisioneros políticos. Simón Trinidad fue el encargado de hacer eso.]

[También hubo un anuncio de las FARC en 2003 diciendo que en el caso de que el gobierno quisiera negociar con ellos, Simón Trinidad sería uno de los representantes. De hecho, en su juicio él confirmó que si las FARC le hubieran solicitado ejercer dicha representación, él habría aceptado. Eso es lo único. Él no tenía nada que ver con esos prisioneros de guerra, no tenía ningún mando sobre ellos ni nada que ver en su captura; aun así lo declararon culpable. [Lea también: Trinidad debería ser parte de la delegación de paz de las FARC](https://archivo.contagioradio.com/simon-trinidad-es-fuerte-y-optimista-abogado-mark-burton/)]

[Incluso, Simón Trinidad, tras dos juicios, fue hallado inocente de los cargos que le imputaban de tráfico de drogas. El jurado no halló pruebas contundentes más allá de unos testimonios bastante cuestionables y que en últimas condujeron a su declaración de inocencia en ese cargo en particular.]

\[caption id="attachment\_46424" align="alignnone" width="703"\]![MarK Burton](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/09/Captura-de-pantalla-2017-09-09-a-las-13.34.29.png){.wp-image-46424 .size-full width="703" height="529"} MarK Burton con Parlamentarios de la Unión Europea en Bruselas tras reunión sobre el caso de Simón Trinidad. Foto: Organizadores Campaña\[/caption\]

**AY:** [¿Hay alguien que por ejemplo haya abierto algún caso en el que se demande que se aportaron testigos falsos o evidencias falsas?]

**MB:** [No, Simón Trinidad y el equipo defensor solicitaron apelación y fue negada rotundamente por la corte superior. Ante eso no se pudo hacer más.]

**AY:**[ ¿En qué está trabajando entonces la defensa de él actualmente? ¿En demostrar su inocencia? O ¿Cómo se busca la liberación? ¿Por la vía política o por la vía netamente jurídica?]

**MB:**[ Ahora es más obvia la vía política. En el futuro es probable que se vuelva a trabajar la vía jurídica tratando de reabrir el caso. Pero todavía eso es incierto. Estamos educando e informando a la gente sobre su caso, sobre la injusticia de su caso y las condiciones bajo las que está preso y también la necesidad de que sea parte del proceso de paz, de la legislación nueva. Porque él es al mismo tiempo excombatiente y víctima del conflicto armado, él debe tener los beneficios de este acuerdo en Colombia. Porque él es colombiano, Todos los hechos ocurrieron en Colombia en su caso. Él debe tener acceso a estas leyes en Colombia.]

**AY:**[ Y ¿qué tan viable ve usted ese escenario de que efectivamente lo liberen tras ejercer una presión como la que busca la campaña?]

**MB:**[ Sí, es difícil, pero nada es imposible. Podemos ver el caso de años de trabajo por la liberación de los cinco héroes cubanos que finalmente tuvo éxito. Así mismo, la liberación tres 36 años de reclusión del puertorriqueño Oscar López Rivera. Tenemos que crear las condiciones políticas y sociales para que sea posible la liberación de Simón Trinidad.]

**AY:**[ Pero ya en el marco del proceso de paz, el gobierno de Colombiana había realizado unos pronunciamientos públicos sobre la liberación de él sin embargo nunca se logró. ¿Qué más se debería hacer para que una nueva solicitud fuera contundente y efectiva?]

\[caption id="attachment\_46423" align="alignnone" width="689"\]![Mark Burton con Heike Hänsel, Parlamentaria alemana del partido Die Linke (La Izquierda). Foto: Carlos Ceballos](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/09/Captura-de-pantalla-2017-09-09-a-las-13.34.37.png){.size-full .wp-image-46423 width="689" height="489"} Mark Burton con Heike Hänsel, Parlamentaria alemana del partido Die Linke (La Izquierda). Foto: Carlos Ceballos\[/caption\]

**MB:**[ El problema es que algunos miembros del gobierno colombiano hicieron unos pronunciamientos pero solicitud formal no se ha hecho nunca. Iván Márquez ha dicho que Santos no hizo nada. Necesitamos un gobierno en Colombia que entienda que ésta es una cuestión importante para el estado colombiano, para completar el proceso de paz. Sin simón no estará completo]

**AY:**[ Y es en esta coyuntura que ustedes se lanzan al mundo con la campaña por su liberación. ¿Cuál es la agenda? ¿Con qué organizaciones o personajes se están reuniendo y qué acciones concretas buscan con la campaña?]

**MB:**[ Ahora estamos empezando. Estamos buscando apoyo de otras organizaciones de derechos humanos y otras que están interesadas en asuntos latinoamericanos y también grupos políticos. Acá en Europa estamos hablando con parlamentarios. En Alemania me he reunido con la parlamentaria Heike Hänsel y mañana voy a Bruselas a reunirme con parlamentarios de la Unión Europea. Voy al parlamento Vasco y también me reuniré con parlamentarios españoles. Tenemos previsto también visitar parlamentarios ingleses.]

[Estamos aun buscando apoyo en el caso internacional, porque éste es un asunto internacional. No es solamente interno de Estados Unidos o de Colombia. La Unión Europea se ha comprometido a ser una acompañante del acuerdo y del proceso de paz, entonces como dije, sin Simón Trinidad, no está cumplido. Tiene derecho a acceder a la Justicia Especial para la Paz pero no solamente eso; tiene derecho a ser reparado como víctima y en los Estados Unidos no se puede.]

**AY:**[ ¿Por qué usted menciona a Simón Trinidad como víctima? ¿Qué pasó con él?]

**MB:**[ Por sus ideas políticas, él vivía bajo amenaza constante de asesinato. Él era miembro de la Unión Patriótica (UP) en el año 1987 y varios de sus colegas y amigos fueron asesinados. Por esas amenazas su familia tuvo que huir del país. Fueron a México primero y después a  Estados Unidos. Él tenía dos caminos, o huir con ellos o ir al monte con la guerrilla. Él no podía dejar Colombia y entró en la guerra bajo amenaza. Él es una víctima en este sentido.]

[También son víctimas los miembros de su familia que salieron al exilio, forzados a dejar su país natal y su hermana quien fue secuestrada por los paramilitares. Durante la guerra su esposa e hijo fueron asesinados por el gobierno. Se localizaron sus posiciones y tras bombardeo fueron asesinados.]

**AY:**[ En un escenario de liberación y repatriación de Simón Trinidad, ¿él podría automáticamente ir a Colombia y acogerse a la JEP o volverían a encarcelarlo? ¿Tiene otros cargos en Colombia? ¿Cómo sería esa transición?]

**MB:**[ No, allá él iría directamente a estas jurisdicciones del sistema de JEP. En el transcurso del proceso, él estaría bajo fianza mientras se llegara a una decisión. El caso se desarrolla fuera de la cárcel y por delitos políticos puede ser absuelto.]

**AY:**[ Ya en lo que ha transcurrido de la gira que se está adelantando ¿ha habido logros en las reuniones que se han tenido?]

**MB:**[ Sí. Aun cuando ésta es la primera parada, hemos estado en entrevista con algunos medios, lo cual le da visibilidad a la campaña. En segundo lugar, nos reunimos con el partido alemán Die Linke (La Izquierda). Ellos han mostrado interés, y quieren trabajar con nosotros. En mi reunión con la parlamentaria Heike Hänsel se mostró el interés de que se realice una solicitud al gobierno Estadounidense de permiso para una visita humanitaria por parte de un grupo de parlamentarios a Simón Trinidad en la Cárcel.]

[Aun cuando lo más probable es que sea negado el permiso por la medida especial administrativa]*[SAM]*[que tiene impuesta y que implica un aislamiento total en cuanto a comunicación con el mundo exterior. Él en principio no puede recibir visita ni comunicación de nadie, pero esto sería un gran acto simbólico que ejercería aún más presión al gobierno Estadounidense y ante los ojos de todo el mundo. De hecho el obtener negación del permiso podría ser una excusa para hacer una manifestación en las premisas de la cárcel. Ésto tendría gran significado e impacto. Aún son sólo ideas y planes por desarrollar, pero estoy muy contento con los resultados de este primer encuentro.]

**AY:**[ Pero bueno, también tengo entendido que se van a reunir con organizaciones de la sociedad civil. ¿Qué papel podrían jugar los ciudadanos del común en esta campaña?]

**MB:**[ Yo no soy organizador de la campaña, participo y contribuyo también un poco en su organización, pero yo me imagino que en un futuro cercano se van a sumar muchos para escribir cartas, hablar con sus parlamentarios sobre el caso. Tenemos que crear un ambiente favorable a su liberación]

\[caption id="attachment\_46422" align="alignnone" width="720"\]![MarK Burton con Parlamentarios de la Unión Europea en Bruselas tras reunión sobre el caso de Simón Trinidad. Foto: Organizadores Campaña](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/09/Captura-de-pantalla-2017-09-09-a-las-13.34.47.png){.size-full .wp-image-46422 width="720" height="410"} MarK Burton con Parlamentarios de la Unión Europea en Bruselas tras reunión sobre el caso de Simón Trinidad. Foto: Organizadores Campaña\[/caption\]

**AY:**[ Usted como el abogado de Simón Trinidad ¿Qué nos puede contar sobre él? ¿Cómo ha sobrellevado su reclusión? ¿Cómo son las condiciones de encarcelamiento a las que está sometido?]

**MB:**[ Las condiciones a las que está sometido son pésimas. Las peores que podría haber en el sistema carcelario estadounidense. Ha estado durante 13 años en aislamiento con muy poco, casi nulo contacto humano. Sólo hasta el año pasado mejoró un poco la situación por el trabajo que hemos realizado desde la defensa. Ahora ha podido tener contacto con otros tres prisioneros algunas horas al día. Al principio, uno de los prisioneros era un mexicano y cuando se dieron cuenta de que estaban hablando en español lo sacaron y cambiaron.]

[Como mencioné no puede tener contacto con nadie externo a excepción de su abogado y su familia. Tiene permitido 15 minutos al teléfono con su familia al mes. De acuerdo  a la Medida Especial Administrativa SAM, ni su familia o abogado pueden darle ningún mensaje externo ni llevar mensajes de él afuera. Simón se encuentra en una celda de menos de 2x3 m en la que entra además muy poca luz por un foso que hay.]

[Sin embargo, Simón no se ha quebrado. Él es una persona tan inteligente y tan enfocada en una visión de su país, que eso no ha dejado que se doblegue. La primera vez que lo vi, yo esperaba ver un hombre acabado, derrotado. Pero no, es todo lo contrario. Es muy animado por su lucha personal y por la lucha de su país. Ahora está bien de salud. Pero tuvimos que alegar mejoramiento en las condiciones y atención médica. Tiene algunas dolencias pero ahora ha mejorado y está bien. Seguiremos en la lucha por la liberación de Simón Trinidad.]
