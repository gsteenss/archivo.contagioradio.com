Title: “Niños y niñas indígenas no mueren de hambre sino de abandono”
Date: 2015-03-25 20:57
Author: CtgAdm
Category: DDHH, Entrevistas
Tags: desnutrición, Desnutrición crónica, Hambre, ICBF, indígenas, Instituto Colombiano de Bienestar Familiar, Juan Pablo Gutierrez, Naciones Unidas, ONIC, pueblos indígenas, Unicef
Slug: ninos-y-ninas-indigenas-no-mueren-de-hambre-sino-de-abandono
Status: published

##### [Foto: www.velocidadmaxima.com]

<iframe src="http://www.ivoox.com/player_ek_4263679_2_1.html?data=lZejlZubfY6ZmKiak5iJd6KnlpKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRksqZpJiSpJbTt4ztjNPWh6iXaaOlwtiYy9PIaaSnhqaxycrSpdSfztrS1MrSb9Hj05DRx9jSudXmysjWh6iXaaOnz5DQw9qRaZi3jqjc0NnFq8rjjLfOxs7Tb46ZmKialg%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Juan Pablo Gutiérrez, delegado internacional de la ONIC y embajador de la UNICEF en Francia] 

**“Cientos de niños indígenas del Casanare solo comen una vez al día y únicamente comen mango de diferentes maneras, pero siempre verde, porque no tienen tiempo de dejarlo madurar”** expresa Juan Pablo Gutiérrez, delegado internacional de la ONIC y embajador de la UNICEF en Francia, quien añade que es “indignante ver que hay niños que están viviendo en esas condiciones en Colombia”.

De acuerdo a las Naciones Unidas, las comunidades indígenas de Colombia se encuentran en peligro de extinción por hambre. Según cifras, **el 70% de niños y niñas indígenas del país sufren de desnutrición crónica**, específicamente de regiones como Atlántico, Pacífico, Orinoquía y Amazonas.

Uno de los ejemplos que evidencian esta situación se encuentra en la Orinoquia, que es una de las regiones que aporta mayores porcentajes de regalías al país, sin embargo, las comunidades indígenas están padeciendo una difícil situación ocasionada por el hambre, que a su vez se genera por el **abandono permanente del Gobierno.**

El delegado internacional de la ONIC, asegura que es muy triste la situación de miles de niños y niñas que mueren de hambre en diferentes regiones del país. Él ha hecho parte de la realización de un programa de caracterización en el que se hace evaluación de la situación de los pueblos indígenas del país, del cual, el caso que más resalta es el del **resguardo de Caño Mochuelo, en el Casanare,** constituido por nueve pueblos indígenas, de los cuales dos  se encuentran en extrema vulnerabilidad.

Se trata de la tribu **Tsiripu, que actualmente solo cuenta con 67 habitantes,** y los **Waüpijiwi, que tiene menos de 100** indígenas, debido a la falta de alimento y el abandono del Estado.

El delegado de la ONIC, denuncia que pese a que el proyecto de caracterización ya fue entregado al Ministerio del Interior, lo cierto, es que no ha habido voluntad de parte del gobierno para hacerle frente a esta crisis alimentaria que está extinguiendo a las comunidades indígenas.

**“No hay un enfoque diferencial, y no llegan las ayudas,** cuando el ICBF (Instituto Colombiano de Bienestar Familiar) va a las comunidades no se tiene en cuenta los alimentos adecuados a los indígenas, según su situación y a su cultura”, asegura Juan Pablo, quien añade que el gobierno se escuda en el discurso de que esa región del país es zona guerrillera.

Cabe resaltar, que más de la mitad de los **cerca de 1,37 millones de indígenas colombianos viven en la 'pobreza estructural', y 7 de cada 10 niños y niñas** de esta minoría étnica sufre de desnutrición crónica, según datos de las Naciones Unidas.
