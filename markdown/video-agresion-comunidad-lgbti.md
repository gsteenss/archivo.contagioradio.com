Title: En video quedó registrada agresión contra comunidad LGBTI en la Plaza de Bolívar
Date: 2017-05-21 00:32
Category: LGBTI, Nacional
Tags: Bus de la Libertad, LGBTI
Slug: video-agresion-comunidad-lgbti
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/05/3c128cb0-9bbb-488e-89c8-a06e64ecc331.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Laura Ortíz] 

###### [20 May 2017]

En medio de una protesta contra el “Bus de la Libertad” por lo menos 2 integrantes de la comunidad LGBTI fueron detenidos por la policía. Según los asistentes la agresión y el uso de la fuerza no se dirigió a detener la confrontación que se presentó con los impulsores de la protesta, sino que la policía se enfocó a impedir la manifestación de varias personas que se oponían a que el bus ingresara a la plaza de Bolívar.

### **El “bus de la libertad” es una ofensa contra integrantes de la comunidad LGBTI** 

El llamado “Bus de la libertad”  es una propuesta que ha venido apareciendo en algunos países y que buscaría evidenciar que ser homosexual es estar enfermo y que por lo tanto deben ser impedidas a toda costa, además es, supuestamente, una manera de llamar la atención sobre la “exportación” de la ideología de género. U**no de los propulsores de la propuesta en Colombia es el polémico concejal  Marco Fidel Ramírez.** [Lea tambien: Al hundirse referendo discriminatorio ganan los niños y las niñas](https://archivo.contagioradio.com/al-hundirse-el-referendo-discriminatorio-ganan-los-ninos-y-las-ninas/)

Según algunas personas trans que se encontraban en el sitio y que protagonizaron la protesta, no se entiende como se puede llamar “Bus de la Libertad” a una iniciativa que lo único que hace es generar exclusión y estigmatización a una población que viene ganando terreno en cuanto a acceso a sus derechos en medio de una fuerte y permanente agresión por parte de quienes se consideran cristianos y "modelos" de familia.

Por esa razón, decenas de personas de la comunidad LGBTI decidieron impedir que el “bus de la libertad” ingresara a la Plaza de Bolívar, lugar considerado como simbólico para quienes defienden los derechos de todos y de todas. Y fue en medio de esta protesta cuando los promotores de la iniciativa agredieron a personas LGBTI. [Lea también: Queda mucho por hacer en materia de DDHH de todos y todas.](https://archivo.contagioradio.com/40631/)

\[KGVID\]https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/05/WhatsApp-Video-2017-05-20-at-5.18.05-PM.mp4\[/KGVID\]

### **La agresión fue contra la comunidad LGBTI** 

Lo paradójico del asunto, es que cuando se esperaba una intervención de la policía (que acompañaba al “bus”) a favor de los y las agredidas o por lo menos una mediación, lo que se dio fue el arresto de tres personas  de la comunidad LGBTI que fueron conducidas a una estación de policía en medio de golpes y empujones, por impedir el libre tránsito de un vehículo hacia la Plaza de Bolívar, lugar que está cerrado para cualquier tipo de tránsito.

\

###### Reciba toda la información de Contagio Radio en [[su correo]
