Title: Actores del conflicto listos para decir la verdad desde las cárceles
Date: 2019-02-13 10:33
Author: AdminContagio
Category: Judicial, Paz
Tags: comision de la verdad, JEP, Unidad de Búsqueda de Desaparecidos
Slug: la-verdad-esta-lista-para-salir-de-la-carcel
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/02/mujeres-en-las-carceles-770x400.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Pixabay 

###### 13 Feb 2019 

Por lo menos 310 personas privadas de la libertad en razón de su participación en el conflicto enviaron una carta dirigida a **los organismos del SIVJRNR** en la que expresa su voluntad para contribuir en el esclarecimiento de la verdad y la justicia para las víctimas desde las cárceles del país.

<div>

Además los firmantes aseguran que las personas en sus mismas condiciones y recluidas en siete establecimientos penitenciarios también estarían en esta misma lógica y **aportarían sus verdades al Sistema Integral.**

**Para el abogado Alirio Uribe, esta es la oportunidad de conocer quiénes han sido los máximos responsables y "de todos aquellos que han patrocinado a las estructuras militares y han capitalizado a la guerra",** además de hablar sobre aquellos políticos e integrantes de los Gobiernos de turno que dieron ordenes a miembros de la fuerza pública para cometer crímenes.

En la misiva hacen una invitación directa al padre **Francisco de Roux** para que se haga presente en la penitenciaría y definir así un proceso de contribución, de la misma manera hicieron voto porque acuda la Unidad de Búsqueda de Personas Dadas por Desaparecidas y a la JEP.

"No guardar más en secreto cómplice los hechos de violencia y guerra interna,las circunstancias propias de los mismo, los nombres de los actores participantes sin consideracion de su sexo, raza, estirpe, oficio o condición económica, social o profesional" señala la misiva.

<iframe id="audio_32516125" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_32516125_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]

</div>

<div>

</div>

<div>

</div>
