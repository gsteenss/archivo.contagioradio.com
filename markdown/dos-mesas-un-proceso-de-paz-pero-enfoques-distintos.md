Title: Dos mesas, un proceso de paz, pero con enfoques distintos
Date: 2015-05-13 15:18
Author: CtgAdm
Category: Nacional, Paz
Tags: Colombia se moviliza por la paz, Conversaciones de paz en la Habana, ELN Nicolas Rodríguez Bautista, FARC, Gabino, Luis Eduardo Celis, paz, Timochenko
Slug: dos-mesas-un-proceso-de-paz-pero-enfoques-distintos
Status: published

###### Foto: radiomacondo 

<iframe src="http://www.ivoox.com/player_ek_4491641_2_1.html?data=lZmmk5uYdY6ZmKiakp6Jd6KomZKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRic2fxtPT0dbZqYzYxpDZw9iPp9Di18rf1cbHrdDixtiYxsqPtMLujMjc0JDJsIy5rbOYx9iPqI6ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Luis Eduardo Celis ] 

###### [13May2015]

Tras confirmación de la reunión entre el comandante de las **FARC, Timochenko**, y el comandante del **ELN Nicolas Rodríguez Bautista “Gabino”**, en la que se hace énfasis en la necesidad de abordar un proceso con el ELN, **Luis Eduardo Celis** analista político, señala que el principal problema puede ser el enfoque.

Mientras que con las FARC se plantea un proceso que contempla un **acuerdo, luego la dejación de armas y por último la implementación de los acuerdos refrendados** por la sociedad; con el ELN se estaría planteando en primer lugar un **acuerdo, en segundo lugar la implementación y por último la dejación de armas**.

Para el analista, esa metodología es la que tiene que ajustarse puesto que el gobierno ya tiene un modelo planteado. Sin embargo el ELN ha manifestado también que una de las principales exigencias es la **participación amplia de la sociedad** y la discusión sobre el modelo extractivo, que para el gobierno ha sido un punto intocable en las conversaciones de paz con las FARC, aunque deberá estar muy implicado en la implementación.

Sin embargo, el encuentro entre los dos comandantes puede indicar que el proceso con el ELN y con las FARC será uno solo pero en dos mesas diferentes, lo que para Celis no es una propuesta nueva sino que se tiene clara desde un principio. Adicionalmente **el proceso con el ELN debe ser diferente al proceso con las FARC**, señala Celis.
