Title: La hipocresía de los medios empresariales
Date: 2015-08-26 14:04
Author: AdminContagio
Category: Otra Mirada, Resistencias
Tags: ACIN, ASOQUIMBO, Barequeros, desalojo en Suba, Desalojos en Cali, Desalojos Forzados, Hidroituango, marcha patriotica, Movimiento Ríos Vivos, ONIC, Quimbo, Venezuela
Slug: la-hipocresia-de-los-medios-empresariales
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/08/desalojos_contagioradio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: montaje contagioradio.com 

###### [26 Ago 2015] 

Ante la mirada de miles de personas circulan las fotografías de la deportación de 1017 colombianos indocumentados que habitan territorio venezolano desde hace varios años sin haber solucionado su situación jurídica. Sin embargo las fotografías y las realidades de miles y miles de colombianos desalojados y desplazados no circula con la misma intensidad.

Las fotografías de la policía ayudando a los colombianos deportados desde Venezuela contrasta fuertemente con la actuación de la misma policía en sitios como Villa Café, las fincas reclamadas por los indígenas en el departamento del Cauca o las víctimas de desalojos forzosos en el Quimbo y el Hidroituango.

Para compartir con ustedes estas realidades les ofrecemos un pequeño recorrido por algunas de las situaciones de desalojo, despojo y desplazamiento que hemos registrado en nuestro medio de comunicación durante el 2015.

### **Mas de 100 indígenas heridos deja arremetida del ESMAD tras 1 semana de liberación de la Madre Tierra** 

Durante las últimas dos semanas la represión por parte de las FFMM y de policía se han recrudecido contra las comunidades indígenas del norte del Cauca, tanto en el municipio de Corinto como en Santander de Quilichao, dónde se concentran cerca de 12000 indígenas nasa. [Ver nota de prensa](https://archivo.contagioradio.com/mas-de-100-indigenas-heridos-dejan-dos-semanas-de-liberacion-de-la-madre-tierra-en-el-cauca/)

### **ESMAD desaloja forzadamente a 1500 personas en Villa Café** 

En Medellín continúan los desalojos forzosos de las familias más pobres de esta ciudad. Esta vez se trata del barrio Villa Café, donde esta mañana desde las 6:30, 1500 personas de estratos 0 y 1 han sido desalojados a manos de 1000 agentes de la policía, que han usado la violencia para lograr desplazarlos. [Ver nota de prensa](https://archivo.contagioradio.com/esmad-desaloja-forzosamente-a-1500-familias-en-villa-cafe-medellin/)

### **Van más de 400 familias desalojadas forzosamente por Hidroituango** 

Isabel Cristina Zuleta, integrante del Movimiento Ríos Vivos, denunció a través de Contagio Radio, los desalojos forzados que viene realizando Empresas Públicas de Medellín, EPM, debido a la realización del proyecto hidroeléctrico Hidroituango en Antioquia. [Ver nota de prensa ](https://archivo.contagioradio.com/van-mas-de-400-familias-desalojadas-forzosamente-por-hidroituango/)

### **ESMAD y EPM nuevamente desalojan forzadamente a Barequeros en Antioquia** 

En la Playa La Arenera, al Norte de Antioquia, los habitantes de esa zona se despertaron rodeados de agentes del ESMAD, que buscan desalojar a los pobladores, a quienes les dan dos horas para irse, debido a la construcción de la hidroeléctrica Hidroituango, desarrollada por EPM, Empresas Públicas de Medellín. [Ver nota de prensa](https://archivo.contagioradio.com/nuevamente-epm-y-esmad-realizan-desalojo-forzoso-por-hidroituango/)

### **Comunidades son víctimas del ESMAD y las empresas por construcción de Hidroeléctrica del Quimbo** 

La represa del Quimbo es quizás uno de los macroproyectos con más afectación construidos en Colombia. Pasando por los daños ambientales y arqueológicos, las consecuencias de su construcción se derivan en violencia como desplazamiento forzado y represión contra las comunidades afectadas. [Ver nota de prensa](https://archivo.contagioradio.com/resistencia-contra-la-represa-del-quimbo/)
