Title: La difícil tarea de denunciar la criminalidad en Medellín
Date: 2015-03-26 17:21
Author: CtgAdm
Category: Fernando Q, Opinion
Slug: la-dificil-tarea-de-denunciar-la-criminalidad-en-medellin
Status: published

###### Foto: ipc.org.co 

Al buscar el significado de la palabra denuncia hay que ir su origen etimológico en el latín. Consta, entonces, de tres partes: el prefijo *de*, que traduce privación; el verbo *nunciare*, que es sinónimo de “hacer saber”; y el sufijo *ia*, que equivale a cualidad. Luego viene su significado conjunto: acción y efecto de denunciar irregularidades, ilegalidades que ocurren, las cuales se pueden hacer ante autoridad competente o públicamente.

La institucionalidad repite, permanentemente, el mismo estribillo: *¡Denuncie, no sea cómplice del delito y mucho menos del criminal!*, y así demuestra que incentiva el apoyo ciudadano en su lucha contra el delito.

Y es cierto, la denuncia seria, responsable y con pruebas, sería una herramienta eficaz para luchar contra el crimen y contra la corrupción estatal. En Medellín, ciudad de múltiples contrates, donde lo ilegal cruza permanentemente lo legal, debería existir una política pública que enseñara y fomentara la acción de denunciar.

La denuncia es una forma de materializarnos como ciudadanos verdaderamente activos y libres, por eso es importante enseñar cómo se hace: ¿por qué debo denunciar? ¿Qué logro denunciando? ¿A quiénes debo denunciar? ¿Cómo denuncio y dónde lo hago?, son preguntas que muchas personas no saben responder.

Ahora bien, algo extraño viene ocurriendo con la denuncia en Medellín, ésta viene bajando vertiginosamente, coincidiendo con la poco convincente premisa que nos repiten: hay reducción de homicidios en la ciudad. Pese a las campañas que realizan la Administración Municipal y la Policía Metropolitana, en las que ofrecen dinero para comprar voluntad ciudadana, seduciendo por medio de la plata para “incentivar el valor ciudadano”, la denuncia sigue descendiendo.

Este declive podría interpretarse de dos formas: la primera, la gente no denuncia porque la eficacia de la fuerza pública y los organismos de investigación arrinconaron a la criminalidad y ya no es necesario denunciar; la segunda, por la cual el pesimismo me inclina, la personas no denuncian porque **no** **confían** en la institucionalidad y menos en su forma de actuar.

Los ciudadanos de Medellín, cada cual a su manera, han sido testigos de la corrupción de algunos miembros de la fuerza pública o de los organismos de investigación. Los criminales se protegieron y se hicieron de una nómina ilegal y paralela cuya red maneja informes sobre las personas que se atreven a denunciar. No nos llamemos a engaños, la protección oficial al crimen organizado existe y es una realidad en esta ciudad, y todo aquel que ose denunciarla también se estaría colgando una lápida metafórica sobre el cuello.

Denunciar no es tan fácil como parece. He practicado juiciosamente mi derecho a la denuncia, constantemente denuncio a la criminalidad y a sus aliados y peones dentro del estado y. aún con pruebas en mano, los resultados han sido precarios. En lugar de investigar y depurarse, la institucionalidad emprende un contrataque en el que descalifica, persigue y veta a sus denunciantes, por lo menos eso ha hecho en mi caso.

¿Será que a los ciudadanos les va mejor que a mí a la hora de denunciar? No lo creo, no todos cuentan con el respaldo de un conglomerado de Ong’s y menos con un esquema de seguridad que les brinde una mínima protección. En Medellín, quien se atreva a denunciar, en el mejor de los casos, es amenazado o desplazado de su territorio; en el peor de los casos, es asesinado o desaparecido y con su muerte confirma su denuncia.

Desde el espacio de información alternativo que dirijo, Análisisurbano.com, que es la agencia de prensa de Corpades en la que se investiga, se analiza y se denuncia las irregularidades de la ciudad, entre ellas el crimen, se ha tratado de ejercer e incentivar la denuncia como mecanismo de defensa y de lucha ciudadana contra la criminalidad. Se intenta generar un espacio en el que la gente se siente libre para denunciar y actúe con confianza, aunque la labor es difícil por el miedo que imponen las balas.

Esta columna no desmoraliza frente a la acción de denunciar, por el contrario, invita a ello. Debemos seguir creyendo que la denuncia evitará más muertes de ciudadanos, especialmente de aquellos que han creído en su legítimo derecho de no callar ante el crimen.

Creo en la denuncia pública y sin tapujos y para hacerla es imperioso creer en las instituciones, creo en ellas pero no cierro los ojos a lo evidente: las instituciones están cooptadas por los criminales.

Nota: Seguimos esperando el regreso, sano y salvo, de Alejandro Ramírez Acosta, desaparecido el pasado 2 de marzo en el municipio de Girardota, Antioquia. **\#TeEsperamosAlejo**
