Title: ¿Cómo transitar el camino de la guerra a la paz?
Date: 2015-02-26 12:10
Author: CtgAdm
Category: Fernando Q, Opinion
Tags: Fernando Quijano, Guerra en Colombia, justicia transicional en colombia, muerte de gaitan
Slug: como-transitar-el-camino-de-la-guerra-a-la-paz
Status: published

###### Foto:EFE 

#### Por [**[Fernando Quijano]** ](https://archivo.contagioradio.com/fernando-quijano/) 

En el siglo XIX, Colombia se vio inmersa en continúas guerras, muchas de ellas partidistas. La última guerra civil de ese siglo no fue la excepción y prosiguió en el siglo XX. Tres momentos de guerra y violencia extrema vivió ese siglo, la cual persiste en el siglo XXI, convirtiéndolo en el más sangriento y cruel por la degradación que sufrió el conflicto armado y la multiplicidad de intereses que lo nutren.

El primer momento, la guerra civil conocida como la ‘Guerra de los mil días’, de 17 de octubre de 1899 al 21 de noviembre de 1902, dejó cientos de miles de muertos -sin contar los desaparecidos-, un territorio nacional desolado, una economía arrasada, una sociedad polarizada y desarticulada de las decisiones del poder real y formal que solo defendía los privilegios de la clase dominante. También dejó un acuerdo de paz, bastante frágil, firmado entre vencedores (conservadores) y vencidos (liberales) en los tratados de Neerlandia y del Wisconsin.

Al fin de la guerra de los mil días, la clase dirigente distraída por el triunfo, pensó, ingenuamente, que demoraría el inicio de otro conflicto, desentendiéndose de la lucha indígena que se gestaba desde el mismo momento que acabó la guerra civil, y del campesinado pobre que, reñidamente, luchaba por sus derechos políticos: ambos luchaban por la tenencia de la tierra. Estos factores encubaron la violencia de los siguientes 45 años, convirtiéndola en una época violenta más dura que la anterior. Desde 1946 acechaba un nuevo conflicto armado y en 1947 la represión conservadora cobró más de 14 mil muertos, que solo fue el inicio de un nuevo desangre, mostrando, aparentemente, el resurgir de la violencia…esa que nunca se fue.

El 9 de abril de 1948, la muerte de Gaitán desencadenó un segundo período violento. Estudiosos de la historia colombiana plantean que de 1946 a 1966,[ ]{.Apple-converted-space}la guerra dejó más de 200 mil muertos y, aproximadamente, 4 millones de desplazados.

Pese a los esfuerzos de pacificación emprendidos por los vencedores -que en realidad fueron imposiciones para alcanzar una paz a fuerza de sangre y plomo, eliminando o sometiendo al contrario-, desde el año 1958, se cocinaba una nueva etapa de confrontación armada en Colombia y esta vez Estados Unidos tendría injerencia directa. El país instauró la doctrina de la seguridad nacional y con eso la tesis del enemigo interno, como contrapeso efectivo a la llegada del comunismo a Latinoamérica. El tercer momento de la guerra posiblemente inició en esta época, caracterizada por la primacía de los intereses particulares sobre el interés público.

La acumulación de problemáticas (agraria, urbana, redistribución de la riqueza, falta de participación política real, corrupción, clientelismo, narcotráfico, mafia, paraestado) ocasionaron que la confrontación se elevara hasta lo que es hoy, confrontación de la que se pretende salir con la firma de un acuerdo de paz.

**¿La paz con quién? **

La solución política al conflicto armado es urgente y necesaria para cambiar la dinámica que actualmente tiene Colombia. Los diálogos de paz en la Habana podrían cerrar la puerta a un conflicto antiguo con las Farc (se espera que pronto se dé también un cierre con el ELN). Los diálogos discuten temas como participación política, víctimas, reforma agraria, dejación o entrega de armas y el tipo de justicia que cobijará los acuerdos firmados: la justicia transicional, que se aplicará sobre los miembros de la guerrilla e incluso sobre los militares.

Gobierno y subversión ponen reparos, pero terminan aceptando que la necesitan; eso sí, en costales separados por aquello del “Honor militar”. No obstante, un tema debe entrar al debate: ¿Quién más necesita la justicia transicional a fin de no quedar exento de la reconciliación y el posconflicto que nos convoca a gritos?[   ]{.Apple-converted-space}

Hay quienes mencionan que se debe incluir a empresarios y políticos que financiaron y auspiciaron la guerra, e incluso que también deben estar las estructuras paramafiosas conocidas como Bacrim o neoparamilitares. El punto se entiende si se tiene en cuenta que los tres momentos de conflicto armado mostraron que los acuerdos firmados que no llevaron a la paz, precisamente porque no se negoció con todas las partes en guerra. Entonces, ¿qué hacer con los que están por fuera?

Hasta ahora, los acuerdos de paz solo han dejado heridas abiertas que generan nuevos focos de violencia. La paz debe servir para saldar la deuda social, política y económica con la sociedad, para esto es necesario que los actores del conflicto también salden sus deudas con la justicia, obviamente sin impunidad. Bienvenida sea la justicia transicional si esta sirve para el cierre verdadero del conflicto armado colombiano, así se requieran más mesas para negociar.

##### **[~~@~~FdoQuijano](https://twitter.com/FdoQuijano)
