Title: Uno de los grandes desafíos de Enrique Peñalosa en Bogotá: La salud pública y la historia de una ciudad
Date: 2015-12-17 12:44
Category: Opinion
Tags: Alcaldía Enrique Peñalosa, Crisis de la salud en Colombia, Infraestructura hospitalaria en Colombia, Salud en Bogotá
Slug: uno-de-los-grandes-desafios-de-enrique-penalosa-en-bogota-la-salud-publica-y-la-historia-de-una-ciudad
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/12/escallon-1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### Foto: Ma Elvira Escallón 

##### [17 Dic 2015]

Por: Carolina Corcho - Médica Psiquiatra

Como es de amplio conocimiento la salud es uno de las grandes problemáticas que afecta a la ciudad y el país, siendo este un momento en el que atraviesa por una de sus crisis más profundas que uno podría definir en tres líneas: 1. Una crisis Financiera que afecta a la Red Pública de Hospitales de Bogotá (ESEs) 2. Una crisis de prestación de servicios que afecta a la población 3. Una crisis laboral por la tercerización y pauperización de los trabajadores del sector.

De alguna manera el impacto de la crisis ha sido menor en Bogotá que en el resto del país porque el Distrito ha contado con un músculo financiero dado por su generoso presupuesto que le ha permitido solventar las deudas de las EPS bajo distintas figuras para mantener las 22 Empresas Sociales del Estado abiertas, so pena de que aún las EPS adeudan una cifra superior a los 850 mil millones de pesos que reclaman medidas efectivas de cobro y reclamación jurídica por constituirse en un detrimento patrimonial para los Bogotanos.

Por otro lado es importante el tema de infraestructura hospitalaria que en las alocuciones del alcalde electo hemos observado redunda en un espacial interés, sin embargo preocupa la poca claridad que la administración entrante manifiesta sobre el destino del Hospital San Juan de Dios, que es de lejos la principal infraestructura hospitalaria de la que adolece el Distrito, en tanto los servicios de salud en Bogotá están concentrados en un 70% en el corredor nororiental de la ciudad, con una inmensa desproporción en el acceso de 4 millones de habitantes del Sur de Bogotá que conforme a las desreguladas definiciones de las EPS deben desplazarse hasta dos horas al norte de la ciudad para ser atendidos, lo que redunda que en materia de salud se reproduce la segregación social histórica de una Bogotá dividida de manera simbólica y real entre ciudadanos del Norte y del Sur.

El conocimiento de la historia Bogotana, sus símbolos, su historia precolombina, colonial, republicana representada en el patrimonio físico de su arquitectura e inmaterial dado por el reconocimiento del valor científico del Hospital San Juan de Dios para el mundo, en sus aportes en el campo de la cirugía, la nefrología, la inmunología y diversos campos del conocimiento médico con la Universidad Nacional de Colombia, aún reconocidos en el planeta entero, debería compeler a un mandatario capitalino a la defensa de este patrimonio que es testigo de la historia misma de la ciudad y el país.

Las grandes ciudades europeas destinan sendos recursos en recuperar sus hospitales patrimoniales, para ellos eso es la renovación urbana, reconstruyeron sus ciudades después de las guerras con cuidado de no sepultar su historia y su memoria por el cemento inerte que nada dice de lo que esos pueblos fueron, son y serán. Este fue el caso de la Catedral Frauenkirche en Alemania, una iglesia luterana de la época barroca construida en Alemania en 1726 y 1746 , destruida por completo en la segunda guerra mundial , y que termino su reconstrucción recientemente en el 2005.

El Hospital San Juan de Dios es un símbolo de la historia de la Medicina desde 1564 en Colombia, soportando cinco siglos de lucha contra la enfermedades y las epidemias de las épocas , es además el centro geográfico e histórico de una frontera invisible en un régimen de Apartheid que se trazó en Bogotá y que confinó a millones de ciudadanos a la exclusión respecto a otros millones que gozan de posibilidades de acceso a bienes y servicios, por tanto, su apertura sería el primer mensaje de reconciliación con una población que ha sido excluida en función de un pensamiento que ha imperado en Colombia, aquel que considera que la salud debe ser un negocio y dejó en manos de la mano invisible del mercado el acceso a servicios de salud, como consecuencia el resultado es la ausencia de suficientes hospitales en aquellas localidades en donde la mortalidad infantil es más alta, lo que se traduce a una condena de nuestros niños a no poder acceder con oportunidad a la mejoría de su calidad de vida con los avances que la ciencia médica provee en el siglo 21.

La reapertura del Hospital San Juan de Dios significa que Bogotá se pone a la vanguardia de las grandes conquistas del mundo en materia de derechos humanos, y hace su primer desarrollo en el marco de la salud como Derecho que consagró la ley estatutaria 1751 de 2015 que es el punto de llegada de un trasegar de luchas de la sociedad colombiana en materia de salud que compiló la Corte Constitucional que genera una jurisprudencia en salud considerada la más progresiva del mundo y que ahora requiere el compromiso de los gobiernos locales y nacional.
