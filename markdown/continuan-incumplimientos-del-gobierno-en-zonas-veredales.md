Title: Continúan incumplimientos del Gobierno en Zonas Veredales
Date: 2017-02-05 12:31
Category: Nacional, Paz
Tags: Implementación de los Acuerdos de paz, La Elvira Cauca, Zonas Veredales Transitorias de Normalización
Slug: continuan-incumplimientos-del-gobierno-en-zonas-veredales
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/02/IMG-20170203-WA00691.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [4 Feb 2017] 

Los 16 integrantes del frente 30 de las FARC, que se movilizaron el 4 de febrero hacia Buenaventura, punto de agrupamiento previo a llegar a la Zona Veredal de La Elvira en Buenos Aires Cauca, manifestaron que están cumpliendo con la paz, pero siguen los incumplimientos del Gobierno, "no han mostrado la suficiente voluntad política”.

Según un integrante del Consejo Naya, el grupo conformado por 14 hombres y 2 mujeres, que se trasladaron desde las inmediaciones del río Naya en el Cauca, se encuentran en buen estado de salud, e hicieron un llamado al gobierno para que dé celeridad a su parte del compromiso y puedan proceder a completar las obras. ([Le puede interesar: “Este fin de mes estaremos en las Zonas Veredales”](https://archivo.contagioradio.com/35424/))

De la ciudad de Buenaventura, los integrantes se dirigen en horas de la mañana del domingo hacia Timba Cauca, y de allí hacia la Zona Veredal de La Elvira, en la que ya se encuentran 300 guerrilleros, dedicados a labores de adecuación de los campamentos pues cuando llegaron al punto, se encontraron con que ni un 10% de las obras de infraestructura estaban adelantadas.

En el departamento del Cauca, se calcula que serán unos 1.400 integrantes de las FARC, concentrados en las distintas Zonas Veredales Transitorias de Normalización.

\

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por Contagio Radio. 
