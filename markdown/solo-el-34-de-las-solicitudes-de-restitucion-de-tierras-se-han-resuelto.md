Title: Solo el 3,4% de las solicitudes de restitución de tierras se han resuelto
Date: 2016-04-11 21:50
Category: Ambiente, Nacional
Tags: Paramilitarismo, Restitución de tierras
Slug: solo-el-34-de-las-solicitudes-de-restitucion-de-tierras-se-han-resuelto
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/03/Restitución-de-tierras-.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Las 2 Orillas 

###### [11 Abr 2016] 

De **87.118 solicitudes de restitución de tierras solo 2.943 han sido resueltas,** es decir, únicamente el 3,4%, y de los 32 departamentos del país en 15 no se ha restituido ni una sola hectárea. Estas son algunas de las cifras que se evidencian en un reciente que demuestra los pocos avances de la Ley de Víctimas y Restitución de Tierras, publicado por la Fundación Forjando Futuros y la Universidad de Antioquia.

En ese marco, la Fundación Paz y Reconciliación, Oxfam y Fundación Forjando Futuros, que acompañan a víctimas de despojo de tierras denunciaron que el procurador general, **Alejandro Ordoñez, la representante del Centro Democrático, María Fernanda Cabal y grupos paramilitares,** están amenazando a reclamantes de tierras e impidiendo su retorno.

El procurador, hace pocos días aseguró que "el diseño de la Ley no respondió o desconoció unas realidades. Si bien es cierto que lo que la ley pretendió es algo sano -quitarle la tierra a los despojadores-, lo que se ha encontrado en muchos casos no es los despojadores, sino terceros ocupantes de buena fe”. Por su parte, la congresista Cabal  ha tildado a las organizaciones defensoras de derechos humanos que acompañan a las víctimas como  auxiliadores de los grupos guerrilleros de las FARC, el ELN y el EPL.

Sin embargo, de acuerdo con Nora Saldarriaga, integrante de Fundación Forjando Futuros, es posible que estos señalamientos de Ordoñez y Cabal, se deban a que ellos tendrían predios en diferentes zonas del país que podrían ser objeto de restitución, **“se ha detectado que hay un grupo u organización que se oponen a la Ley porque son quienes, han despojado, desplazado y que no quieren entregar los territorios a sus legítimos dueños”,** por lo que agrega que “Es extraño que el procurador en vez de apoyar a los campesinos que fueron despojados y violentados, está a favor de personas que fueron despojadores, lo cual ha sido demostrado por la Ley”.

Según estas organizaciones, cuando los campesinos deciden retornar, los grupos antirestitución llegan a los predios, incluso, en ocasiones acompañados de la fuerza pública, para impedir que puedan recibir sus tierras. Asimismo, generan una estrategia de desprestigio a  las organizaciones sociales que acompañan a las víctimas, y constituyen empresas paralelas u organizaciones espejo para confundir  a que los jueces.

Por ejemplo, hace 20 días en el municipio de Chigorodó ubicado en Urabá Antioqueño, estas **organizaciones antirestitución acompañados de la fuerza pública desalojaron violentamente a un grupo de campesinos que retornaron a sus predios de manera voluntaria,** según denuncia la ONG Forjando Futuros. También la situación se ha repetido en el municipio de Turbo donde varias veces quienes dicen ser propietarios no han permitido la restitución.

Según el informe, Argos S.A (Fiducor S.A), Continental Gold Limited Sucursal Colombia, Exploraciones Chocó Colombia S.A.S, Anglogold Ashanti Colombia S.A, Sociedad Agropecuaria Carmen de Bolívar S.A, Sociedad Las Palmas Limitada, Agropecuaria Palmas de Bajirá S.A, Inversiones Futuro Verde S.A, Palmagan S.A.S, A. Palacios S.A.S y Todo Tiempo S.A, serían las empresas opositoras a la restitución, además de otros 5 nombres de personas investigadas por haberse apropiado de tierras.

**“Lo que venimos reclamando al Estado y ahora al procurador, es que no puede hacerse lado de quienes despojaron y desplazaron**”, dice Saldarriaga, quien agrega que solicitan al gobierno agilizar la entrega de tierras antes de que los poseedores de mala fe vuelvan a impedir que los campesinos retornen a sus predios.

<iframe src="http://co.ivoox.com/es/player_ej_11141156_2_1.html?data=kpaelpaVeZehhpywj5aXaZS1k5aah5yncZOhhpywj5WRaZi3jpWah5yncbTjzdSYx9GPd4ampJmSlJqPqMafzcbgjdjTsMrXytnixsrXb8XZjNfS1dnNuNbXyoqwlYqmd8%2BfxcqY1s7Jto6ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

[Informe Restitución de Tierras](https://es.scribd.com/doc/308169781/Informe-Restitucion-de-Tierras "View Informe Restitución de Tierras on Scribd")

<iframe id="doc_4302" class="scribd_iframe_embed" src="https://www.scribd.com/embeds/308169781/content?start_page=1&amp;view_mode=scroll&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="undefined"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)
