Title: 8 películas compiten por la nominación al Oscar con "El abrazo de la Serpiente"
Date: 2016-01-13 17:03
Author: AdminContagio
Category: 24 Cuadros
Tags: el abrazo de la serpiente, Mejor pelicula lengua extranjera Oscar, Premios Oscar
Slug: 8-peliculas-compiten-por-la-nominacion-al-oscar-con-el-abrazo-de-la-serpiente
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/01/El-abrazo-de-la-serpiente-Jan-Bijvoet-860x450_c.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### Foto: Liliana Merizalde 

##### 13 Ene 2016

Este jueves se conocerán las 5 películas nominadas al Oscar de la Academia 2016, y la expectativa en Colombia crece por saber si “El Abrazo de la Serpiente” del director Ciro Guerra, estará en la bajara de candidatas a llevarse el galardón en la categoría que reconoce la mejor cinta en lengua no inglesa.

La producción colombiana, estrenada con gran éxito en 2015, compite con 8 películas procedentes de países, algunos de ellos con mayor tradición cinematográfica y volumen de producción y distribución, lo que hace de esta categoría una de las más duras año tras año. La pre nominación representa un hecho sin precedentes para una película producida en el país.

La expectativa por la posible nominación ha crecido en parte gracias a los diferentes reconocimientos que la cinta ha recogido en diferentes salas y festivales del mundo, empezando por Cannes, donde recibió todos los aplausos del público y la crítica, en los Festivales de la India, Lima, Mar del Plata, Fenix de México y en los premios Macondo.

Lo invitamos a conocer un poco más sobre las películas que compiten en la misma categoría, así como algunas de las nominaciones que han recibido en otros Festivales del mundo.

**Las cintas en competencia.**

**The Brand New Testament', Dir. Jaco Van Dormael, (Bélgica)**

<iframe src="https://www.youtube.com/embed/dRSSmt6KwzA" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

Dios vive en Bruselas, donde lidia con sus conflictos de personalidad que le ocasionan problemas con su familia, en particular con su hija Ea, quien decide rebelarse ante su conducta, anunciando a todos a través de su ordenador el día en que moriran, haciendo que todos piensen sobre el tiempo que les queda por vivir.

**Palmarés 2015:** Quincena de Realizadores Festival de Cannes; Nominada a Mejor película de habla no inglesa en los Globo de Oro y Satellite Awards; Nominada a mejor diseño de producción Premios de cine Europeo, mejor comedia, y a Mejor actriz (Groyne) en el Festival de Sitges.

**A War', Dir. Tobias Lindholm (Dinamarca)**

<iframe src="https://www.youtube.com/embed/N9nUIFYc5II" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

El comandante Pedersen se debate entre el cumplimiento de los reglamentos militares, su responsabilidad ante sus hombres y los civiles afganos y su deseo de volver a casa con su esposa y sus tres hijos.

**'The Fencer', Dir Klaus Härö (Finlandia)**

<iframe src="https://www.youtube.com/embed/ShMAkhyC6bY" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

Huyendo de la policía secreta rusa, Endel, un joven campeón de esgrima, se ve obligado a regresar a su tierra natal, donde se convierte en profesor de educación física en una escuela local. Pero el pasado le pone frente a una difícil elección

**'Mustang', Dir. Deniz Gamze Ergüven (Francia)**

<iframe src="https://www.youtube.com/embed/uuVDkMT00gE" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

Cinco hermanas huérfanas que viven al norte de Turquía, pasan el verano en un jardín cerca al mar negro con los chicos de su escuela, hasta que las condiciones que se imponen para la mujer en el país provocan rumores de inmoralidad y escándalo, lo que conlleva a que sus familias inicien su preparación hacia un destino de sumisión marital.

**Palmarés 2015:** Nominada a Mejor película de habla no inglesa Globos de Oro, Independent Spirit Awards, Satellite Awards, Critics Choise Awards, Mejor ópera prima Premios de Cine Europeos, Mejor película Europea Premios Goya, Premio a la libertad de expresión en National Board of Review, Premio del Público Festival de Cine de Sevilla y Mejor película Festival de Sarajevo.

**'Labyrinth of Lies', Dir Giulio Ricciarelli (Alemania)**

<iframe src="https://www.youtube.com/embed/1IZtuTYFONU" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

Un joven y ambicioso fiscal descubre cómo importantes instituciones alemanas y algunos miembros del gobierno están involucrados en una conspiración cuyo fin es encubrir los crímenes de los nazis durante la Segunda Guerra Mundial (1939-1945)

**Palmarés 2015:** Satellite Awards: Nominada a Mejor película de habla no inglesa

**'Son of Saul', Dir. László Nemes (Hungría)**

<iframe src="https://www.youtube.com/embed/Kvux1G32C2g" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

En el año 1944, durante el horror del campo de concentración de Auschwitz, un prisionero encargado de quemar los cadáveres de su propia gente encuentra cierta supervivencia moral tratando de salvar de los hornos crematorios a un niño que toma como su hijo.

**Palmarés 2015:** Gran Premio del Jurado y Premio FIPRESCI Festival de Cannes; Mejor película extranjera Globos de Oro, Independent Spirit Awards, National Board Review, Satellite Awards, Critics Choice Awards, Críticos de Los Angeles, Nominado a mejor nuevo director Directors Guild of America, Mejor Ópera prima Círculo de Críticos de Nueva York.

**'Viva', Dir. Paddy Breathnach, (Irlanda)**

<iframe src="https://www.youtube.com/embed/cGQxqTMA3go" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

Jesús es un joven cubano que trabaja en un club de drag queens en La Habana que sueña convertirse en artista mientras gana dinero a través de la prostitución. Al regresar su padre, un boxeador exitoso que habia estado 15 años en prisión por homicidio, la vida del chico cambiara para siempre.

**'Theeb', Dir. Naji Abu Nowar, (Jordania).**

<iframe src="https://www.youtube.com/embed/q9eVnRShnuA" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

Theeb vive con su tribu beduina en Arabia en 1916, su hemano Hussein debe encargarse de su crianza luego de la muerte de su padre. Sus vidas se interrumpen con la llegada de un oficial británico y su guía en una misteriosa misión hacia un pozo de agua ubicado en la  ruta de peregrinación a La Meca. Theeb seguirá a su hermano mayor a través del desierto, en un viaje peligroso luego del estallido la Primera Guerra mundial.

**Palmarés 2015**: Nominada a Mejor película en habla no inglesa Premios BAFTA, y Mejor director en el Festival de Venecia (Sección Orizzonti).
