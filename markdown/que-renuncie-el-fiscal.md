Title: ¡Que renuncie el Fiscal!
Date: 2018-11-13 18:30
Author: AdminContagio
Category: Judicial, Nacional
Tags: Fiscal General, Nestor Humberto Martínez, Odebrecht
Slug: que-renuncie-el-fiscal
Status: published

###### [Foto: Contagio Radio] 

###### [13 Nov 2018] 

Luego de la revelación de audios en los cuales se evidencia que **Nestor Humberto Martínez Neira**, fiscal general de la nación, estaba al tanto de **las irregularidades en la contratación de Odebrecht,** el caso de corrupción con mayor repercusión en el continente; se abrió el debate sobre su idoneidad como cabeza del ente investigador.

Las evidencias en audio, que fueron entregadas por el ingeniero, y ex-auditor de la Ruta del Sol II, **Jorge Enrique Pizano** a Noticias Uno, se escucha cuando advierte a Martínez (quien fungía como asesor jurídico de Corficolombiana), sobre la posibilidad de que se estuvieran presentando irregularidades en la contratación de la obra. Corficolombiana era socia de Odebrecht en Colombia y sólo meses después el país se enteraría que **la firma entregó más de 84 mil millones de pesos en sobornos para lograr contratos como la Ruta del Sol**.

Como lo recordó el senador por el Polo Iván Cepeda, cuando Martínez fue elegido como fiscal general de la nación en 2016, su partido, en cabeza de Jorge Enrique Robledo, citó a 2 debates de control político en los que pedían que Martínez Neira se declarara impedido para investigar el caso Odebrecht por ser asesor de la firma socia de la multinacional brasileña en Colombia.

Ahora **los audios de Pizano prueban que tanto Martínez como Luis Carlos Sarmiento Angulo, jefe del grupo empresarial de Corficolombiana, sabían de los Sobornos de Odebrecht y no dijeron nada a la justicia**.

Estos archivos de audio fueron conocidos después de la muerte de Pizano, a esto se suma deceso en Chile de una Fiscal que llevaba el caso tras un accidente automovilístico y el fallecimiento de Alejandro Pizano Ponce de León, hijo del ingeniero y quien podía tener acceso a información de su padre sobre el caso, en días recientes por envenenamiento con cianuro; hechos que sumados, crean un manto de dudas sobre una posible conexidad en los decesos.

> [\#ATENCIÓN](https://twitter.com/hashtag/ATENCI%C3%93N?src=hash&ref_src=twsrc%5Etfw) Comunicado de prensa \# 189 [pic.twitter.com/MGdXvW5Doi](https://t.co/MGdXvW5Doi)
>
> — Fiscalía Colombia (@FiscaliaCol) [13 de noviembre de 2018](https://twitter.com/FiscaliaCol/status/1062475917874941953?ref_src=twsrc%5Etfw)

<p>
<script src="https://platform.twitter.com/widgets.js" async charset="utf-8"></script>
</p>
 

**¿Qué sigue para el fiscal Nestor Humberto Martínez?**

 

Ante la posibilidad de que Martínez sea investigado por este caso, Cepeda señaló que el mecanismo para juzgar a funcionarios con fuero es la Comisión de Acusaciones, "que la opinión pública conoce más como **Comisión de Absoluciones**", en razón de que los congresistas que la componen también tienen "asuntos pendientes con la justicia", y podrían verse beneficiados, o perjudicados por la acción del Fiscal.

 

Por lo tanto, **sería difícil que tuviera lugar dicho proceso**; en lugar de ello, diferentes personajes de la opinión pública consideran que **lo que correspondería en este caso es la renuncia del Fiscal**, de tal forma que, como lo afirmó Cepeda, sea otra persona quien se encargue de investigaciones sobre las cuales la opinión pública pueda tener la tranquilidad que se están haciendo sin ninguna clase de interés". (Le puede interesar: ["Nestor Humberto Martínez debe declararse impedido para investigar caso Odebrecht"](https://archivo.contagioradio.com/fiscal-martinez-declararse-impedido-investigar-caso-odebrecht/))

 

Para el Senador, Martínez debería estar dándole explicaciones a la justicia y la opinión pública sobre temas como las denuncias sobre propiedades no declaradas en España y Estados Unidos o el nombramiento de Luis Gustavo Moreno como fiscal anticorrupción, en lugar de ser el primer investigador de la justicia penal. (Le puede interesar: ["Objetividad, prudencia y respeto: El llamado a la Fiscalía en episodio JEP"](https://archivo.contagioradio.com/objetividad-prudencia-y-respeto-el-llamado-a-la-fiscalia-en-episodio-jep/))

 

> Tanta confianza le tenía Jorge Enrique Pizano a Néstor Humberto Martínez, que decidió grabar las conversaciones. Sr. [@FiscaliaCol](https://twitter.com/FiscaliaCol?ref_src=twsrc%5Etfw) no venga a decirnos que fue un diálogo entre amigos, él le advirtió la corrupción de Odebrecht y usted calló como Fiscal.
>
> — Ángela María Robledo (@angelamrobledo) [13 de noviembre de 2018](https://twitter.com/angelamrobledo/status/1062361925118558208?ref_src=twsrc%5Etfw)

<p>
<script src="https://platform.twitter.com/widgets.js" async charset="utf-8"></script>
</p>
 

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
