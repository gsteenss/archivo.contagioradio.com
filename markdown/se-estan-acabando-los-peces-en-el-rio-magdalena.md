Title: Se están acabando los peces en el río Magdalena
Date: 2015-08-18 13:17
Category: Ambiente, Otra Mirada
Tags: Cormagdalena, Hidroeléctricas, Mineria, Río Magdalena
Slug: se-estan-acabando-los-peces-en-el-rio-magdalena
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/08/pescadores_contagioradio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: bajandoelmagdalena.com 

###### [14 Ago 2015] 

Pescadores y comunidad en general han manifestado preocupación ante la drástica disminución pesquera que se ha presentado en el río Magdalena en el departamento de Santander. **Hace 40 años se producían alrededor de 70.000 toneladas al año en la cuenca del río, en la actualidad, se ha reducido a 11.000 toneladas.**

Según algunos pescadores del Magdalena y la Unidad de Planificación Rural Agropecuaria, adscrita al Ministerio de ambiente, **la ausencia de peces ha sido causa de las malas técnicas de pesca, la presencia de empresas hidroeléctricas y de extracción minera**, de utilización de redes prohibidas, entre otras, lo cual modifica el ecosistema generando rupturas en la reproducción natural de los peces.

Con el fin de realizar propuestas metodológicas para la identificación de zonas con potencial  de pesca para el consumo y   se ha realizado en Barrancabermeja el taller de pesca continental de la zona del Magdalena Medio en el marco de la creación del Plan Nacional de Ordenamiento Productivo de la acuicultura y la pesca, estudiando criterios socioeconómicos, ambientales y territoriales.
