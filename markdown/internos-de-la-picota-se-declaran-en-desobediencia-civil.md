Title: Internos de 'La Picota' se declaran en desobediencia civil
Date: 2016-05-11 16:02
Category: DDHH, Nacional
Tags: cárceles colombia, Crisis carcelaria colombia, La Picota
Slug: internos-de-la-picota-se-declaran-en-desobediencia-civil
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/12/crisis-carcelaria1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: ECOS] 

###### [11 Mayo 2016 ] 

Desde este miércoles internos de 'La Picota' se declaran en desobediencia civil y pacífica, para exigir al Ministerio de Justicia la declaratoria de una emergencia humanitaria, que permita solucionar la **crisis de atención en salud y hacinamiento que se vive en las cárceles colombianas**.

En 'La Picota' no hay ningún tipo de atención en salud básica ni de urgencias, los **internos enfermos de gravedad no cuentan con suministro de ningún tipo de medicamentos**, las cirugías y remisiones programadas son incumplidas, los recluidos con problemas mentales no son tratados y los que tienen VIH y tuberculosis no reciben ninguna atención, en parte porque al personal médico no le están pagando sus salarios.

Según denuncian, la emergencia carcelaria declarada por MinJusticia es insuficiente para atender la magnitud y [[gravedad de la crisis](https://archivo.contagioradio.com/?s=crisis+carcelaria+)], que **ha cobrado la vida de cientos de reclusos con enfermedades terminales**, como el adulto mayor Francisco Correa quien murió en la cárcel 'La Modelo' de Bogotá.

"Ésta no puede seguir siendo la suerte de colombianos privados de la libertad", aseguran e insisten en la necesidad de promover huelgas y movilizaciones para presionar soluciones reales por parte del gobierno nacional, que incluyan **atención médica integral tanto física como psicológica**, así como una reforma a la política penitenciaria, en clave restaurativa.

###### [Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU) ]] 
