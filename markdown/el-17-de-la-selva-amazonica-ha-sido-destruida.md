Title: El 17% de la selva amazónica ha sido destruida
Date: 2016-06-19 08:00
Category: Ambiente, Nacional
Tags: biodiversidad, reforestación, selva amazónica
Slug: el-17-de-la-selva-amazonica-ha-sido-destruida
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/06/Saving-the-Amazon.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: [elsolweb.tv]

###### 19 Jun 2016 

De acuerdo con ‘Amazonia Viva’, el nuevo informe de la WWF (Fondo Mundial para la Naturaleza) **la ganadería extensiva, la minería,  los proyectos hidroeléctricos y la construcción de vías de trasporte,** son las principales amenazas que actualmente tienen en riesgo una de las selvas más importantes del mundo.

En el Amazonas se produce el 25% del oxígeno mundial, se encuentra  más del 25% del agua dulce y el 31% de los bosques del planeta. Sin embargo, la tasa de mortalidad en niños en la región amazónica es del 38,44% y e**l 17 % de la selva ha sido destruida.**

Según el informe, la producción agrícola, la ganadería extensiva, pero además los cultivos de palma de aceite  son la causa principal de la deforestación, y por tanto, la degradación del ecosistema. Así mismo, el río más grande del mundo se ve amenazado por **250 proyectos hidroeléctricos, todo esto a causa de la política gubernamental de** Brasil, Bolivia, Perú, Ecuador, Colombia, Venezuela, Guyana, Surinam y Guayana Francesa que han permitido que las empresas realizadoras de estos proyectos tengan a la Amazonía en esta situación de alerta.

Por otra parte, la WWF señala que en todo el territorio amazónico hay **800 autorizaciones para el desarrollo de proyectos mineros, sumado a eso, existen 6.800 solicitudes en proceso de revisión. Además se indica que hay otros 20 proyectos de construcción de carreteras.**

** Salvando la Amazonía**

Debido a esta alarmante situación, hace tres años inició un proyecto de reforestación en la Amazonía colombiana, desde la iniciativa “Saving the Amazon”, que tiene como objetivo preservar la selva amazónica y salvaguardar a las comunidades indígenas que viven allí.

El proyecto de restauración del ecosistema inició en la selva del Vaupés y de la mano de las comunidades indígenas de esa zona. Durante 6 meses se realizó el proceso de llegada, conocimiento de la zona, reuniones con los líderes de las comunidades indígenas de la comunidad Trubón, de la etnia Cubeo.

El ‘payé’, es decir, maestro de la comunidad del Timbó aprobó el  proyecto y actualmente ‘Saving the Amazon’ **ya trabaja con las comunidades Timbó y Trubón y con las familias de Tayazú, Santa Cruz, Pueblo Nuevo y Tucandirá de la etnia Wanano.** En diálogo con la población indígena se decidió que los árboles que se planta serían frutales, maderables y medicinales.

Además cada árbol también deja a las comunidades una aportación económica del 10% del costo del mismo, sin que su cotidianidad sea perjudicada, así lo confirma Henry González, líder de la comunidad Trubón, “Las comunidades siguen su vida tradicional, tienen sus casas, hacen sus cacerías y pesca y adicional siembran los árboles y reciben una remuneración monetaria por esto que les complementa la economía familiar y pueden hacer frentes a gastos extra (jabón, sal, tiquetes, gasolina…)”

Es así, como **se ha conseguido salvar más del 90% de la población joven indígena**, ya que, ahora tienen un trabajo remunerado y por tanto no tienen que salir de la comunidad a buscar recursos, según explica Saving The Amazon.

<iframe src="http://co.ivoox.com/es/player_ej_11942014_2_1.html?data=kpamlpeUdZWhhpywj5aUaZS1lpmah5yncZOhhpywj5WRaZi3jpWah5yncYzLytHZy8bRb6_dxtncjZKPl8LqytPUjdnMqYy1zsbn0dORaZi3jqjc0NnFq8rjjLfOxs7Tb46ZmKialg..&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
