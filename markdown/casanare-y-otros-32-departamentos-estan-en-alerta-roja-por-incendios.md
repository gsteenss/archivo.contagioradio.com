Title: Casanare y 32 departamentos en alerta roja por incendios forestales
Date: 2018-02-21 17:02
Category: Ambiente, Nacional
Tags: incendios, incendios forestales, incendios forestales Colombia
Slug: casanare-y-otros-32-departamentos-estan-en-alerta-roja-por-incendios
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/02/incendios.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Colprensa] 

###### [21 Feb 2018] 

Desde el Casanare han denunciado que se está presentando un incendio que está poniendo en riesgo 5 mil hectáreas de bosque mega diverso. Aún se desconoce qué o quién los produjo y hay **32 departamentos del país** que están en alerta roja según el Ideam.

Tanto esta entidad como la Unidad Nacional de Gestión del Riego han manifestado que **más del 90% de los incendios** que se están presentado en el país son provocados por los humanos. Sin embargo, reconocen que Colombia se encuentra en una época de sequía que aumenta la posibilidad de que se generen nuevos incendios.

### **Incendio en Casanare pudo haber sido provocado por alguna persona** 

De acuerdo con Laura Miranda, integrante de la Fundación Cunaguaro, en el Casanare, específicamente en la vereda Alta Gracia en el municipio de Trinidad, hay 13 reservas colindantes con los bosques que **se están incendiando**. Afirmó que es una zona muy seca en donde no llueve hace más de dos meses.

Sin embargo, desde la Fundación creen que el incendio fue ocasionado **“por alguien mal intencionado** que realizó una quema en la parte alta de la vereda”. Este incendio se expandió por las zonas de bosques y sabana “que tienen ecosistemas de morichales y fauna que se vieron afectadas”. (Le puede interesar:["Sin implementación ambientes va a seguir siendo víctima en el posconflicto"](https://archivo.contagioradio.com/sin-implementacion-ambiente-va-a-seguir-siendo-victima-en-el-posconflicto/))

Desde la Fundación le hicieron un llamado a las personas que realizan quemas para que cuando lleguen las lluvias broten los pastos, de **abstenerse de realizar estas acciones**. Indicaron que, en los llanos orientales, cuando se presenta una temporada de sequía, “las personas realizan quemas para sembrar arroz y para mejorar los pastos”.

### **Ha habido poca respuesta de las autoridades** 

Miranda enfatizó en que la repuesta de las autoridades **no ha sido suficiente para apagar las llamas**. “Hubo colaboración de los bomberos de los municipios de Trinidad y de Pore, sin embargo, tienen pocos equipos”, manifestó. Además, dijo que el acceso al lugar del incendio es muy precario debido a las malas condiciones de la carretera.

Desde Yopal enviaron un carro tanque para atender la emergencia, pero “hay 7 horas de camino hasta allá”. Denunciaron también que la Fuerza Aérea argumentó **no tener una fuente de abastecimiento de combustible** cercano por lo que no pudieron llegar al lugar. Por esto, no ha habido una atención con helicóptero para apaciguar el fuego. La afectaciones ambientales no son menores y los animales más afectados son los osos hormigueros, los armadillos, los búhos y las tortugas morrocoy.

<iframe id="audio_23957470" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_23957470_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]

 
