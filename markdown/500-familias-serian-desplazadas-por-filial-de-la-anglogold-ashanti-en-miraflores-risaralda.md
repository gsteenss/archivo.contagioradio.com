Title: 500 familias serían desplazadas por la asociación Miraflores Compañía Minera, en Quinchía Risaralda
Date: 2017-09-01 12:47
Category: DDHH, Nacional
Tags: Anglo Gold Ashanti, Embera-Carambá
Slug: 500-familias-serian-desplazadas-por-filial-de-la-anglogold-ashanti-en-miraflores-risaralda
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/09/cerro-e1504285016875.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Alcaldía de Quinchía] 

###### [01 Sept 2017] 

**500 familias indígenas y campesinas serían desplazadas por la actividad que realiza la empresa Miraflores Compañía Minera asociada de Anglogold Ashanti**, en Quinchía, Risaralda, además, los habitantes denunciaron ser víctimas de presiones y abusos que realiza la empresa con apoyo del gobierno Nacional, vulnerando los derechos de las comunidades que allí habitan.

De acuerdo con Érika Molina, integrante de la Guardia Indígena Embera- Karambá, las familias que serían desplazadas del territorio son indígenas, campesinas y mineros artesanales, y lo harían por las intenciones que tiene la empresa Miraflores Compañía Minera de realizar explotación de oro **en el mismo lugar en donde viven las personas.**

Los habitantes denunciaron que la empresa adquirió a través de manipulación y **cesiones fraudulentas el contrato minero que en un principio había sido entregado a los mineros artesanales**, que realizaban esa actividad en el territorio desde hace más de 110 años. (Le puede interesar: ["Comunidades en Quinchía, Risaralda le dicen no a la minería"](https://archivo.contagioradio.com/protesta-mineria-quinchia/))

Además, la consulta previa que debía hacerse en el territorio para que la empresa hiciera la explotación, nunca llego a ser finiquitada, Molina aseguró, que, si bien el proceso inició, la comunidad solicitó un plazo para llevarla a cabo, sin embargo, **la empresa se negó y la delegada del gobierno Nacional dió por finalizado el proceso de consulta previa**.

Según la comunidad, la empresa también estaría incumpliendo requerimientos ambientales, de igual forma la organización Tejido Territorial ha señalado que esta compañía es asociada de Anglogold Ashanti, multinacional que a su vez, sería copropietaria de algunos de los títulos que componen el proyecto minero.

Y ha manifestado, que desde que la empresa inició actividades, 11 familias se han desplazado, sumado a ello estarían las afectaciones a las fuentes de agua que integran parte del a cuenca del Río Cauca, “haciendo que se pierda la capacidad alimentaria y condiciones de la vida digna en la región”, **producto de ello sería la desaparición de especies de flora y fauna y la perdida de la soberanía alimentaria** que tenían las comunidades. (Le puede interesar: ["430 familias serían desplazadas para explotar oro en Miraflores, Resaralda"](https://archivo.contagioradio.com/430-familias-serian-desalojadas-para-explotar-mercurio-en-miraflores-quindio/))

En un comunicado de prensa la multinacional Anglogold Ashanti informó que "actualmente no adelanta ningún trabajo de minería en el municipio de Quinchía, ni posee títulos mineros en la vereda Miraflores a nombre propio ni a través de alguna empresa filial o asociada".

Frente a esta situación Érika Molina afirmó "ellos estan convencidos de que la gente les cree pero nosotros como comunidad indígena no lo hacemos, porque los estudios que se han hecho demuestran que los títulos mineros terminan en las manos de la multinacional" y agregó que la comunidad ha señalado que se mantendrá en movilización permanente y esta a la espera de que, tanto la Alcaldía local como las autoridades Nacionales, respeten el derecho a **la vida digna y protejan a las 500 familias que actualmente habitan allí**.

<iframe id="audio_20644886" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_20644886_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU).
