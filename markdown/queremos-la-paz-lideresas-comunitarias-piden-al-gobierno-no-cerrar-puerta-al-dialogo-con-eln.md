Title: Mujeres víctimas piden no cerrar la puerta del diálogo con el ELN
Date: 2019-01-21 12:39
Author: AdminContagio
Category: Comunidad, Mujer
Tags: Comunidades campesinas, comunidades indígenas, lideres sociales, Mesa de diálogo con el ELN
Slug: queremos-la-paz-lideresas-comunitarias-piden-al-gobierno-no-cerrar-puerta-al-dialogo-con-eln
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/01/luz-marina-cuchumbe-770x400.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Archivo 

###### 21 Ene 2019 

**Luz Marina Cuchumbe y Jani Silva, lideresas** y víctimas del Cauca y el  Putumayo expresaron su preocupación ante la agudización de la violencia en el territorio colombiano y la decisión del presidente Iván Duque de levantar la mesa de diálogos con el ELN. Las mujeres clamaron por que se siga buscando la salida negociada y se eviten más muertes por la guerra.

**Luz Marina Cuchumbe,** lideresa de Inzá, Cauca manifestó que la muerte de los jóvenes estudiantes de la Escuela de Cadetes General Santander, “tocó su corazón” pues se trató de un acontecimiento que atentó contra “quienes apenas empezaban a vivir, tal vez con el sueño de empezar un camino por la paz”, una razón más para extender su pésame a las madres campesinas que perdieron a sus hijos como consecuencia de la guerra.

 [<iframe src="https://co.ivoox.com/es/player_ek_31652157_2_1.html?data=lJajl5eVeZihhpywj5WZaZS1kpaah5yncZOhhpywj5WRaZi3jpWah5ynca3p25C6w9fNssKfpNrQytrRpsaZk6iY1dTGtsafzcaYxsrHrdTdhqigh6eXsozYxpDZx9vFstXV05Dax9iRaZi3jqjc0NnFq8rjjLfOxs7Tb46ZmKialg..&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>   
La lideresa expresó que la guerra, **“solo trae dolor y división  a los campesinos"** e invitó al presidente Duque, al Senado y la ciudadanía a “que se pongan la mano en el pecho” y abran las puertas al diálogo con el ELN, **“las víctimas estamos dispuestas a perdonar sabemos que la guerra no trae nada bueno para las comunidades”** agregó.]

**“No queremos que se disparen más armas”**

[Desde Inza, Luz Marina insistió en que el mensaje que quieren compartir las comunidades con el país es el de detener el asesinato de líderes sociales, **“no queremos que se disparen más armas”**, señaló Cuchumbe quien además indicó que el camino a la paz debe abrirse y ser labrado para dejar un legado a las nuevas generaciones.]

[<iframe src="https://co.ivoox.com/es/player_ek_31652186_2_1.html?data=lJajl5eVfJehhpywj5WXaZS1lJmah5yncZOhhpywj5WRaZi3jpWah5yncavVz86Ytc7QusKZk6iY1dTGtsafxcrQy9jNaaSnhqeg0JDIqYzoxtfay9PFtozhxtjOjcjTsozZzZCyrrORaZi3jqjc0NnFq8rjjLfOxs7Tb46ZmKialg..&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>]

[Por su parte, Jany Silva, lider campesina de la Perla Amazónica en el Putumayo **manifestó la gran preocupación que siente las comunidades de su región ante la perspectiva de “un país en guerra que trae dolor y miseria”** y advirtió que como consecuencia de la decisión tomada por el Gobierno, la violencia únicamente generará más violencia.]

La lideresa quien también expresó su dolor y solidaridad con las madres y familiares de las víctimas del atentado declaró que este es el momento menos oportuno para que se dé una ruptura en la mesa de negociación y enfatizó en la necesidad de llamar a las partes pues **"el proceso de paz representa una esperanza para el pueblo colombiano"**, en especial para los campesinos cuyos “hijos son los que van a prestar el servicio militar y quienes exponen sus vidas”.

###### Reciba toda la información de Contagio Radio en [[su correo]
