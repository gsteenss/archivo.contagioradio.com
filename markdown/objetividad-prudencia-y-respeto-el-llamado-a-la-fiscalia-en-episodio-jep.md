Title: Objetividad, prudencia y respeto, el llamado a La Fiscalía en episodio JEP
Date: 2018-10-08 13:06
Author: AdminContagio
Category: Entrevistas, Paz
Tags: comision de la verdad, Fiscalía, Luz Marina Monzon, Unidad de Búsqueda de personas dadas por desaparecidas
Slug: objetividad-prudencia-y-respeto-el-llamado-a-la-fiscalia-en-episodio-jep
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/08/Investigaciones-Fiscalía.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo Partícular] 

###### [08 Octu del 2018] 

La Comisión de la Verdad y la Unidad de Búsqueda de personas dadas por desaparecidas, señalaron a través de un comunicado de prensa, que debe haber una "colaboración armónicamente de las instituciones en la construcción de paz en el país", e hicieron un llamado para que **la prudencia, la objetividad y el respeto sean los principios que orienten la actuación de las instituciones estatales, entre esas la Fiscalía.**

Asimismo la directora de la UBPD, Luz Marina Monzón afirmó que "no hay ningún riesgo" frente a las nuevas tareas que esta asumiendo el Sistema de Verdad, Justicia, Reparación y no repetición y señaló que por el contrario el riesgo es que si las instituciones no actúan como un todo, "no se cumpla con los fines esenciales", **se pierdan recursos valiosos y sobretodo "contribuir a una desesperanza" en la transformación del país. **

Para Monzón, la situación que se presentó entre la Fiscalía y la Jurisdicción Especial para la paz, es un hecho que se ha venido dando con otras instituciones creadas con el Acuerdo de Paz,  y que puede ser producto del momento de ajuste que están afrontando esas estructuras, razón por la cual aseguró que se necesita que todas las organizaciones estén a la **"altura" en términos éticos y profesionales para cumplir con la tarea de la construcción de la paz**.

"Yo creo en este país, y estoy convencida de que estos obstáculos hacen parte de un camino de construcción de paz que nos demanda unas formas de actuar distintas, y tengo confianza en que las instituciones, incluida la Fiscalía, van a saber comprender y estar a la altura de lo que nos demanda este momento histórico" aseveró Monzón. (Le puede interesar: ["Accionar de la Fiscalía contra la JEP es innadminisble: Comisión Colombiana de Juristas"](https://archivo.contagioradio.com/accionar-de-la-fiscalia-contra-jep-es-inadmisible/))

[Comunicado de Prensa Conjunto 0810 18 (2)](https://www.scribd.com/document/390406834/Comunicado-de-Prensa-Conjunto-0810-18-2#from_embed "View Comunicado de Prensa Conjunto 0810 18 (2) on Scribd") by [Contagioradio](https://www.scribd.com/user/276652802/Contagioradio#from_embed "View Contagioradio's profile on Scribd") on Scribd

<iframe id="doc_77937" class="scribd_iframe_embed" title="Comunicado de Prensa Conjunto 0810 18 (2)" src="https://www.scribd.com/embeds/390406834/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-TqEzlH4l7QY7vSAWuipt&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="0.7729220222793488"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 

 
