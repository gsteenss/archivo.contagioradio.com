Title: Sequía en Murindó, Antioquia afecta a nueve comunidades afro e indígena
Date: 2019-01-08 16:16
Author: AdminContagio
Category: Ambiente, Nacional
Tags: Sequía, Urabá Antioqueño
Slug: sequia-en-murindo-antioquia-afecta-nueve-comunidades-afro-indigena
Status: published

###### Foto:  @DapardAntioquia 

###### 08 Ene 2019 

La desviación de cerca de 35 kilómetros en el cauce del río Murindó en el Urabá antioqueño, ha dejado incomunicadas y sin acceso a agua potable a nueve comunidades indígenas y afro, las cuales habían advertido con anterioridad sobre el represamiento de agua en la zona.

Aunque permanente los habitantes de Murindó padecen inundaciones por el desbordamiento del río, esta vez la sequía ha afectado a las comunidades **afro de Canal, Pital, Murindó Viejo, No Hay Como Dios y Bartolo, así como las comunidades indígenas Guagua, Isla, Coredó y Bachidubi, del pueblo Embera Eyábida**

De acuerdo a la Organización Indígena de Antioquia (OIA), la sequía ha incomunicado a los habitantes de los territorios, pues el afluente es la única vía de acceso, de igual forma la carencia de agua ha afectado los cultivos, la ganadería y ha ocasionado enfermedades a varios indígenas, entre ellos menores de edad  y adultos mayores.

Tal como advirtió en días pasados  el consejero de la Organización Indígena de Antioquia y representante de las comunidades indígenas **Embera Eyábida**, Alberto Siniguí las comunidades han tenido que desplazarse varios kilómetros hasta la cabecera municipal de Murindó para abastecerse de agua.

La comunidad hizo un llamado a los gobiernos departamental y nacional para que sean destinados los recursos humanos y financieros necesarios y así evitar una mayor tragedia ambiental. Cabe resaltar que el pasado 10 de diciembre, la OIA interpuso un derecho de petición a **Corpourabá,** máxima autoridad ambiental de la región solicitando el dragado del Rio Murindó, petición que no ha sido respondida.

###### Reciba toda la información de Contagio Radio en [[su correo]
