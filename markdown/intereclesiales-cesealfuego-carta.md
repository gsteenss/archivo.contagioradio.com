Title: Líderes de iglesias respaldan cese al fuego bilateral
Date: 2017-10-01 13:41
Category: Nacional, Paz
Tags: Cese al fuego bilateral, DIPAZ, ELN, Gobierno, paz
Slug: intereclesiales-cesealfuego-carta
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/09/paz-col.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Archivo 

###### 1 Oct 2017 

En una carta dirigida al Presidente Juan Manuel Santos y a Nicolás Rodríguez Bautista, Comandante del Ejército de Liberación Nacional ELN, **integrantes del Diálogo Intereclesial por la Paz DiPaz**, compuesto por lideresas y líderes religiosos de Colombia y del mundo, **expresaron su regocijo** por el inicio del [cese bilateral al fuego](https://archivo.contagioradio.com/cese-bilateral-eln-2/) entre el Gobierno y esa guerrilla.

En la misiva, manifiestan a las partes que **cuentan con "sus oraciones, respaldo, colaboración y con experiencia como observadores para la veeduría"** en esta nueva etapa de la negociación, al igual que lo hicieran durante momentos similares en su momento al proceso de paz con las FARC EP.

Desde DiPaz, recordaron que **cuentan con el apoyo de hombres y mujeres que se congregan en iglesias del mundo** articuladas, a nivel global en organizaciones como las distintas familias confesionales, el Consejo Mundial de Iglesias CMI y el trabajo del Papa Francisco. Le puede interesar: [El pedido de perdón de la iglesia después de la visita del Papa Francisco](https://archivo.contagioradio.com/el-pedido-de-perdon-de-la-iglesia-despues-de-la-vista-del-papa-francisco/).

Los **más de** **180 firmantes** aseguran que el éxito del cese bilateral al fuego, "requiere de **acciones efectivas para detener la amenaza paramilitar que asedia a las comunidades** y se proteja de manera especial a las campesinos y campesinas, líderes y lideresas sociales, defensores y defensoras de Derechos Humanos, trabajadores y trabajadoras de la paz que están siendo asesinados".

"La paz requiere generosidad con la nación, dejar de lado las mezquindades de los cálculos  
económicos y políticos, **dar oportunidad a que la vida que nace de las comunidades se pueda expresar de manera libre** con sus iniciativas, propuestas, esperanzas, en este momento en el que las armas se silencian, despejando el camino para que por fin, luego de esta larga confrontación en Colombia" finaliza la comunicación.

[Carta DiPaz Gobierno ELN](https://www.scribd.com/document/360401854/Carta-DiPaz-Gobierno-ELN#from_embed "View Carta DiPaz Gobierno ELN on Scribd") by [Contagioradio](https://www.scribd.com/user/276652802/Contagioradio#from_embed "View Contagioradio's profile on Scribd") on Scribd

<iframe id="doc_27844" class="scribd_iframe_embed" title="Carta DiPaz Gobierno ELN" src="https://www.scribd.com/embeds/360401854/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-r7mi3yZfZU6vynyHM1yw&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="0.7068965517241379"></iframe>
