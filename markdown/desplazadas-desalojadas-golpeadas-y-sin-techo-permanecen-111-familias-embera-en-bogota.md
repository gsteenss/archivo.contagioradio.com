Title: Desplazadas, desalojadas, golpeadas y sin techo permanecen 111 familias embera en Bogotá
Date: 2020-05-13 01:23
Author: AdminContagio
Category: Actualidad, Comunidad
Tags: ESMAD
Slug: desplazadas-desalojadas-golpeadas-y-sin-techo-permanecen-111-familias-embera-en-bogota
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/05/Menores-embera-desalojados.png" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/05/Menores-embera-desalojados-1.png" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/05/indigenas-Chami-desalojados.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:heading {"align":"right","level":6,"textColor":"cyan-bluish-gray"} -->

###### Foto: @Gabriel Galindo - Contagio Radio {#foto-gabriel-galindo---contagio-radio .has-cyan-bluish-gray-color .has-text-color .has-text-align-right}

<!-- /wp:heading -->

<!-- wp:paragraph -->

El pasado jueves 7 de mayo, en medio de un desalojo a un edificio en el barrio de la Candelaria, ciudadanos denunciaron el uso excesivo de la fuerza contra integrantes del pueblo Embera que habitan en Bogotá tras ser desplazados de sus territorios. Ellos denuncian que los niños fueron maltratados en medio del desalojo por integrantes del Escuadrón Móvil Antidisturbios (ESMAD), y que el procedimiento se dió en medio de incumplimientos a acuerdos establecidos previamente.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/heidy_up/status/1259591937834827778","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/heidy\_up/status/1259591937834827778

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:heading {"level":3} -->

### El incuplimiento al pueblo embera en Bogotá

<!-- /wp:heading -->

<!-- wp:paragraph -->

Leonibal Campo integrante de la comunidad señala que ellos están pidiendo una casa en la que puedan habitar colectivamente, para no separarse. Agrega que este fue uno de los compromisos a los que llegaron con entidades del distrito el pasado 18 de marzo, luego que desarrollaran una protesta frente al edificio de Avianca, en el Parque Santander de Bogotá.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/CallejerosMise/status/1251662647927492608","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/CallejerosMise/status/1251662647927492608

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

Pero afirma que el acuerdo fue incumplido y nuevamente se presentaron otras protestas que culminaron en acuerdos el 8 de abril, para finalmente desembocar en la toma de una construcción en el barrio Candelaria que fue desalojado el pasado 7 de mayo por la Policía. (Le puede interesar: ["Paramilitares de AGC amenazan con limpieza social en Ciudad Bolívar"](https://archivo.contagioradio.com/paramilitares-de-agc-amenazan-con-limpieza-social-en-ciudad-bolivar/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Campo explica que la comunidad está compuesta por 111 familias, poco más de 300 personas entre las que se encuentran mujeres en estado de embarazo, menores de edad y adultos mayores que tuvieron que huir de su territorio, y desplazarse a Bogotá por amenazas del Ejército de Liberación Nacional (ELN).

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Pese a tener el reconocimiento por parte de las instituciones de la Ciudad como víctimas del conflicto, el integrante del pueblo Embera sostiene que no han recibido atención por parte de esas instituciones. Por esta razón, los niños han pasado hambre, y solo han recibido el acompañamiento de una Fundación para recibir alimentos.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Adicionalmente, reclama que Bienestar Familiar los amenazó con quitarles los menores, aún cuando la comunidad denuncia que la Policía los maltrató incluso con golpes de bolillos. Tampoco los mayores se salvaron de los golpes, "a un hombre de 71 años lo arrastró la Policía", denuncia Campo.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### ¿Golpes y maltratos para los más vulnerables?

<!-- /wp:heading -->

<!-- wp:paragraph -->

Campo asegura que el próximo miércoles 13 de mayo se adelantará una reunión con instituciones de la ciudad para intentar alcanzar nuevos acuerdos, y métodos de verificación de cumplimiento de los mismos pero también se está evaluando acudir a instancias internacionales para hacer prevalecer sus derechos.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En el marco del desalojo también se denunció en [redes sociales](https://twitter.com/heidy_up/status/1259597525591502848) que una mujer embera en estado de embarazó habría perdido su bebe por golpes de la Policia; situación que negó la misma institución por medio de un [comunicado](https://twitter.com/PoliciaBogota/status/1259971966682959874) en el que informó el seguimiento que hizo al caso. Aunque el caso no haya sido verídico, ciudadanos siguen cuestionando la actuación de la policía con poblaciones vulnerables como personas desplazadas.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por ejemplo, igual que con las familias Embera, se ha denunciado que en la localidad de Ciudad Bolívar se siguen haciendo desalojos por la fuerza, lo que significa maltratos para las comunidades así como que las mismas no tengan si quiera un lugar en el cual pasar la cuarentena. (Le puede interesar: ["El hambre de las familias en Bogotá no se soluciona con Policía"](https://archivo.contagioradio.com/el-hambre-de-las-familias-en-bogota-no-se-soluciona-con-policia/))

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->
