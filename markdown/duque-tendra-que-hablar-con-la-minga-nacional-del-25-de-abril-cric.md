Title: Duque tendrá que hablar con la Minga Nacional del 25 de abril: CRIC
Date: 2019-04-10 21:17
Author: CtgAdm
Category: Comunidad, Movilización
Tags: Minga Nacional, Paro Nacional
Slug: duque-tendra-que-hablar-con-la-minga-nacional-del-25-de-abril-cric
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/minga-nacional.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: @CRIC] 

Después de que el presidente Duque, quien a pesar de viajar a Caldono incumplió con su palabra y no asistió a la reunión que estaba pactada para el pasado 9 de abril con los mingueros del suroccidente del país,  las partes continúan sin llegar a ningún acuerdo; un escenario que permite preguntarse **¿Cómo avanzar cuando se obvia el diálogo?** ¿en qué situación queda la Minga del suroccidente del país?

### **La Minga es un ejercicio colectivo ** 

**Giovanni Yule, dinamizador político del CRIC** señala que desde el principio se había acordado con la ministra del Interior, Nancy Patricia Gutiérrez el encuentro con el presidente y la comunidad, pues la Minga es un ejercicio colectivo en el que "todos deben participar, en la palabra, el pensamiento y la acción, si todos siembran, todos cosechan", en referencia a que el gobierno esperaba el encuentro en espacio cerrado y con algunos delegados.

[(Le puede interesar: Duque viaja a Cauca pero no se reúne con la Minga)](https://archivo.contagioradio.com/duque-viaja-a-cauca-pero-no-se-reune-con-la-minga/)

A pesar de que otros presidentes se han reunido con las comunidades indígenas en la plaza de Caldono, **Duque sí estuvo en el lugar** pero permaneció al interior de la Casa de la Cultura de la cabecera municipal, "lo que argumenta el Gobierno es que habían asuntos de seguridad que le impedían salir", lo que según el vocero es una muestra más de que la Minga estaba **"estigmatizada y culpada por una serie de situaciones de actores armados ilegales los cuales hemos desmentido",** refiriéndose a las declaraciones del fiscal Néstor Humberto Martínez sobre un plan de atentado contra el presidente, "creemos que es una falsedad, y si tiene evidencias tiene que actuar pero no impedir las comunicaciones", afirma.

El dinamizador sostiene que hasta ahora no tienen conocimiento de capturas de sospechosos, y recuerda que el fiscal Martínez "ha sido una persona muy cuestionada y que no tiene mucha credibilidad", lo que se prestaría para "salvaguardar ciertos  intereses del Gobierno e ir en contra de algunos ejercicios de protesta" que han ejercido algunos sectores de la sociedad.

### **¿Hacía dónde camina la Minga?** 

Yule afirma que tras el fallido diálogo con el presidente Duque, los pueblos indígenas regresarán a sus territorios y realizarán un ejercicio evaluativo sobre la Minga y replantearán su estrategia,  "si el presidente no habló con la Minga tendrá la posibilidad de hablar con la Minga Nacional el 25 de abril" ratificando que la Minga continúa pese a los señalamientos del fiscal, las declaraciones del senador Álvaro Uribe y las consecutivas amenazas paramilitares que coincidieron con dichas afirmaciones.

<iframe id="audio_34338273" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_34338273_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
