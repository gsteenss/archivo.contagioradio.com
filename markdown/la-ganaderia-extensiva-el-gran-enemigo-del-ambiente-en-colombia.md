Title: La ganadería extensiva el gran enemigo del Ambiente en Colombia
Date: 2018-06-05 15:12
Category: Ambiente, Nacional
Tags: Amazonía, deforestación, ganadería extensiva, Mineria
Slug: la-ganaderia-extensiva-el-gran-enemigo-del-ambiente-en-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/07/tuparro-bosques-secos.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Instituto Von Humboldt] 

###### [05 Jun 2018] 

El 5 de junio se conmemora el día mundial del Ambiente, y en Colombia quedan muchos retos por cumplir para garantizar la protección de los ecosistemas, la fauna y la flora. De acuerdo con Manuel Rodriguez, docente de la Universidad de los Andes y ex Ministro de Ambiente, uno de las mayores afectaciones al ambiente es el aumento **de la ganadería extensiva, que aún no tiene freno y la deforestación de bosques y selvas.**

Respecto al balance de cómo se ha manejado la protección del ambiente, Rodríguez afirmó que en especial en el último año, las políticas en este ámbito y en torno a la creación de más áreas protegidas han aumentado. En ese sentido, el docente aseguró que "de acuerdo a los datos del IDEAM, en el año 2017, en el mapa de los ecosistemas de Colombia, se **observa que los parques nacionales presentan una deforestación sustantivamente menor a la del resto del país"**.

### **Los riesgos para el ambiente**

Según Rodríguez una de las actividades que más afectaciones genera al Ambiente es la ganadería. "**La deforestación del norte del Amazonas se hace para crear grandes extensiones para una ganadería muy ineficiente que deja muy pocos reditos al país**, y en parte parece que es un proceso de control de territorio y especulación de tierras.

De igual forma, las selvas del Chocó también se encuentran en un alto riesgo debido a los cultivos extensivos de uso ilítico, como la coca, y la minería ilegal que se práctica en todo el departamento. A este hecho se suman la deforestación del norte de la selva Amazónica y la tala de más de **7 mil hectáreas del Parque Nacional Tinigua**, en el departamento del Meta.

Otros ecosistemas que se han visto afectados son los que eran inaccesibles producto del conflicto armado como el Parque Nacional de La Macarena que ahora se ha visto fuertemente golpeado y ha comenzado a perder hectáreas debido a la tala de árboles y la siembra de cultivos de uso ilícitos. (Le puede interesar: ["El precedente que marca el fallo sobre la Amazonía en Colombia"](https://archivo.contagioradio.com/fallo_historico_corte_suprema_amazonas/))

El cambio en el uso de energías no renovables a energías renovables también es un reto que Colombia debe afrontar y que de acuerdo con el analista, si bien no se va a dar de la noche a la mañana, el país ya tiene todas las herramientas para comenzar el tránsito. Asimismo Rodríguez enfatizó en la necesidad de que todos estos problemas se debe resolver con el aumento en la pedagogía sobre la protección al ambiente, que continúa siendo muy débil en el país.

<iframe id="audio_26371072" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_26371072_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 

 
