Title: Reportan un joven desaparecido tras procedimiento irregular de Policía en Cauca
Date: 2019-03-23 17:38
Author: CtgAdm
Category: DDHH, Nacional
Tags: Cauca, Comunidad, ESMAD, Minga
Slug: joven-desaparecido-policia-cauca
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/03/Esmad-Cauca.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: @camilovelasco99] 

###### [23 Mar 2019] 

Comunidades que se encuentran concentradas en la vereda La Pajosa, corregimiento de El Cairo (Cauca), denunciaron que dos integrantes de la población fueron víctimas de un procedimiento ilegal por parte de la Policía que provocó la desaparición de uno de los jóvenes. Adicionalmente, señalan que helicópteros de la Fuerza Pública siguen sobrevolando los campamentos en los que se concentra la Minga.

De acuerdo a información de la comunidad, los dos jóvenes se retiraron de la concentración en la zona de La Pajosa cerca de las 10 de la mañana, y cuando iban llegando al puente de El Cairo fueron detenidos por integrantes del Escuadrón Móvil de Carabineros (EMCAR), quienes tras pedirles la identificación, dijeron que iban a ser esposados y trasladados. Ante esa situación, uno de los jóvenes huyó del lugar rumbo a la Pajosa, siendo perseguido por 3 uniformados.

Sobre las 11 am el otro joven capturado decidió huir también del lugar, tras no tener información de su compañero, y ante la insistencia de los uniformados por capturarlo. Él fue encontrado cerca de media hora después en la concentración de la Pajosa, pero el joven que huyó más temprano aún no fue encontrado. (Le puede interesar: ["Así habría sido la muerte de ocho indígenas según la Minga en Dagua, Valle del Cauca"](https://archivo.contagioradio.com/nuestras-autoridades-llegaran-al-fondo-de-la-verdad-afirman-indigenas-en-dagua/))

Posteriormente, cerca del mediodía, defensores de derechos humanos se dirigieron al comandante del EMCAR quien se negó a hablar con ellos; sin embargo, lograron establecer comunicación con integrantes del Escuadrón Móvil Antidisturbios (ESMAD), quienes manifestaron que realizaron un procedimiento acorde a la Ley, mientras negaron tener las pertenencias de los jóvenes.

Según información recogida por organizaciones de derechos humanos e indígenas de la zona, los jóvenes fueron despojados de sus pertenencias, incluyendo un teléfono celular. Sobre el jóven desaparecido aún no se tiene información, y su compañero no quiere brindar su nombre por temor a represalias por sus denuncias.  (Le puede interesar: ["Indígenas y Gobierno establecen mesa técnica pero la Minga continúa"](https://archivo.contagioradio.com/indigenas-del-cauca-y-gobierno-establecen-mesa-tecnica-sin-embargo-la-minga-continua/))

### **Actuación de la Policía sigue afectando a integrantes de la Minga ** 

Las comunidades de igual forma denunciaron que pese a acuerdo suscrito con el Gobierno Nacional, helicópteros de la fuerza pública han realizado sobrevuelos sobre las zonas de concentración de participantes de la Minga. Adicionalmente, dos personas han resultado gravemente heridas en enfrentamientos con la Policía en Suarez, Cauca. (Le puede interesar:["Mientras polícia afirma que disidencias infiltran la Minga, comunidad indígena las capturan"](https://archivo.contagioradio.com/mientras-policia-afirma-que-disidencias-infiltran-la-minga-comunidades-indigenas-las-capturan/))

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
