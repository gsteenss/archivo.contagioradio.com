Title: Medidas para prevenir el cambio climático vulneran los DDHH #YoReporto
Date: 2015-03-13 19:41
Author: CtgAdm
Category: Ambiente, El mundo
Tags: AIDA, Ambiente, Calentamiento global, cambio climatico, comunidades indígenas, DDHH, Derechos Humanos
Slug: medidas-para-prevenir-el-cambio-climatico-vulneran-los-ddhh-yoreporto
Status: published

##### Foto: AIDA, Asociación Interamericana para la Defensa del Ambiente 

El pasado mes de diciembre, la AIDA (Asociación Interamericana para la defensa del Ambiente) hizo parte de la 20ª Conferencia de Estados Parte de la Convención Marco de las Naciones Unidas sobre Cambio Climático (COP 20) en Lima, Perú. Tras la conferencia, se publicó el documento “[Llamado de Lima a la Acción Climática](http://unfccc.int/files/meetings/lima_dec_2014/application/pdf/auv_cop20_lima_call_for_climate_action.pdf)”, que de acuerdo a la abogada de AIDA, María José Veramendi Villa, “**un punto clave y ausente es el reconocimiento de que el cambio climático interfiere con el goce de los derechos humanos”.**

Según Veramendi, en función de Protocolo de Kioto, varios países industrializados implementan proyectos de reducción de emisiones, sin embargo, el problema es que **al ponerse en marcha estos proyectos, se han violado derechos humanos de diferentes comunidades.**

**“Detrás de los tecnicismos y las negociaciones, está el rostro humano del cambio climático”**, expresa Veramendi, quien indicó que Mary Robinson, Ex Alta Comisionada de las Naciones Unidas para los Derechos Humanos y ahora Enviada Especial del Secretario General de la ONU para el Cambio Climático, advirtió que este tema es "el mayor problema de derechos humanos del siglo XXI".

Además, los expertos de Naciones Unidas pidieron a los países que hacen parte de la Convención “incluir en el acuerdo un lenguaje que establezca que todas las partes deben, en todas las medidas relativas al cambio climático, promover, respetar, proteger y cumplir con los derechos humanos para todos y todas”, dice Veramendi Villa.

María José Veramendi, resalta el caso explicado por Máximo Ba Tiul, indígena Maya Poqomchi de Guatemala y representante del Consejo de Pueblos de Tezulutlán, quien también participó en las discusiones de la COP.

![DDHH y cambio climatico contagioradio 2](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/03/DDHH-y-cambio-climatico-contagioradio-2-286x300.jpg){.aligncenter .wp-image-5952 width="332" height="348"}

##### Foto: AIDA. Máximo Ba Tiul expone el caso de la hidroeléctrica Santa Rita en un evento de la COP20. Crédito: AIDA. 

El líder indígena, se refirió a las comunidades indígenas de Guatemala que se han visto afectadas por la hidroeléctrica Santa Rita, proyecto registrado en 2014 bajo el Mecanismo de Desarrollo Limpio (MDL) de la Convención.

“**El Estado guatemalteco no consultó ni obtuvo el consentimiento previo, libre e informado de los pueblos indígenas afectados antes de autorizar Santa Rita**. Aún más, el proyecto ha generado la oposición de las comunidades, las cuales han obtenido como respuesta el hostigamiento de la Fuerza Pública y la criminalización de sus líderes. La represión y violencia ha llegado a tal punto que en agosto de 2013 un trabajador de la empresa que buscaba atentar contra la vida de los líderes comunitarios asesinó a los niños David y Ageo, de 11 y 13 años respectivamente”, afirmó la delegada de la AIDA, a lo que agrega que Máximo se pregunta: “¿Por qué se tienen que violar los derechos humanos para mitigar los efectos del cambio climático?”.

[![DDHH y cambio climatico contagioradio](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/03/DDHH-y-cambio-climatico-contagioradio-300x223.jpg){.aligncenter .wp-image-5953 width="378" height="280"}](https://archivo.contagioradio.com/elmundo/medidas-para-prevenir-el-cambio-climatico-vulneran-los-ddhh-yoreporto/attachment/ddhh-y-cambio-climatico-contagioradio/)

##### Foto: Maquinaría inicia el dragado del río Dolores para la construcción de la hidroeléctrica Santa Rita a finales de 2011. Crédito: Archivo comunitario. 

Este caso se une a otros como el de la represa Barro Blanco en Panamá y otros tantos proyectos registrados bajo el MDL, que “**nos recuerdan la necesidad imperativa de que los derechos humanos sean incorporados en todas las acciones climáticas y que ello sea vinculante en el nuevo acuerdo climático de París.** De no ser asi, la lucha por la defensa de los derechos de las comunidades de líderes y lideresas como Máximo Ba Tiul en Guatemala y Weni Bagama en Panamá, con quienes tuve el honor de compartir en Lima, será un llamado más sin respuesta en la búsqueda de soluciones y compromisos frente al cambio climático”, argumentó la integrante de la AIDA.
