Title: Violencia contra líderes sociales y defensores el mayor obstáculo para la paz
Date: 2017-10-18 13:55
Category: DDHH, Nacional
Tags: defensores de derechos humanos, lideres sociales
Slug: a-mayor-posibilidad-de-participacion-mayor-es-la-violencia-contra-lideres-informe
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/02/Movice.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [18 Oct 2017] 

De acuerdo con el más reciente informe de las organizaciones Indepaz, CINEP, Programa por la Paz, IEPRI y la Comisión Colombiana de Juristas, durante el año 2016, en Colombia se registraron **101 violaciones al derecho a la vida, 98 homicidios y tres desapariciones forzadas**, todas contra defensores y líderes sociales y mayoritariamente en regiones que están priorizadas para la implementación del acuerdo con las FARC.

### **¿A quiénes asesinan en Colombia?** 

El informe titulado “Panorama de violaciones del derecho a la vida, libertad e integridad de líderes sociales y defensores de derechos humanos en 2016 y el primer semestre del 2017”, expone que, de las 101 violaciones al derecho de la vida, **86 actos fueron contra hombres, mientras 15 hacia mujeres.**

A su vez estableció que las comunidades más afectadas son las indígenas con 23 líderes asesinados, seguida de las comunidades campesinas, **con 20 líderes, los integrantes de las Juntas de Acción, con 17 asesinatos y los consejos comunitarios con 7 homicidios**. (Le puede interesar: ["A Jair lo mataron porque Estado no garantiza protección de quienes impulsan sustitución"](https://archivo.contagioradio.com/?p=48032&preview=true))

De igual forma el documento señala que las organizaciones sociales y populares más afectadas por actos de violencia son Marcha Patriótica, el Consejo Regional Indígena del Cauca (CRIC) y las Asociación de Cabildos Indígenas Embera, Wounaan, Katío, Chamí y Tule, del Chocó.

### **El aumento de la violencia en Colombia** 

El informe permitió establecer los tiempos en los que el país ha registrado aumentos en los actos de violencia, el primero de ellos se registró en marzo con 13 asesinatos, luego se dio en agosto con 12 líderes y defensores de DD.HH, asesinados y hubo un aumento en **noviembre y diciembre en donde por cada mes se presentaron 10 asesinatos.**

A su vez, los aumentos en los asesinatos coinciden con dos momentos trascendentales en el proceso de paz el primero es el 24 de agosto, cuando se firmó el Acuerdo Final en La Habana y se acordó el cese bilateral al fuego entre el gobierno y las FARC-EP, a partir de esa fecha se produjo un aumento de los asesinatos con un total de **41, posteriormente el 24 de noviembre con la firma del Teatro Colón 12 asesinatos**.

Los departamentos en donde persiste la violencia, con mayor intensidad, de acuerdo con el informé son Antioquia, Cauca, Valle del Cauca, Norte de Santander, Nariño y Chocó. (Le puede interesar: ["ESMAD retiene a 15 comuneros del Resguardo de Kokonuko, Cauca: CRIC](https://archivo.contagioradio.com/esmad-retiene-a-5-comuneros-del-resguardo-kokonuko-cauca-cric/)")

### **Los responsables de los asesinatos contra líderes** 

El informe evidenció que en el 57% de los casos de asesinatos se desconoce los autores de los hechos, el 24% de los casos corresponde a los narco-paramilitares, el 5% a grupos armados sin identificar, 4% corresponden al ELN y a las FARC, mientras que el Escuadrón Móvil Antidisturbios es el presunto autor del 4% de los asesinatos.

### **La estigmatización hacia los defensores de Derechos Humanos y líderes sociales** 

Camilo Bonilla, sociólogo de la Comisión Colombiana de Jurista y quien participó en la elaboración del documento, expresó que en el informe se muestran **5 casos en donde hubo un manejo tendencioso de la información y de estigmatización** por parte de funcionarios públicos y con el respaldo de medios de información.

“Estamos frente a un ejercicio asimétrico de poder en donde **el medio de información únicamente tiene en cuenta la versión de la fuente más poderosa”** afirmó Bonilla y agregó que en el ejercicio periodístico debe hacer una consulta de todas las fuentes e “ir más allá de los hechos” para develar lo que hay detrás del asesinato. (Le puede interesar: "[Conclusiones de los medios sobre masacre de Tumaco revictimiza: ASUMINUMA"](https://archivo.contagioradio.com/47979/))

### **Que la paz no cueste la vida** 

El documento afirmó que si bien es cierto que hay informes de instituciones gubernamentales que alertan sobre la crítica situación del país, el Estado sigue sin tomar medidas concretas frente a estas situaciones, mientras que en diferentes departamentos del país las condiciones siguen siendo críticas.

Además se evidencia una **cercanía espacial de las violaciones al derecho a la vida de líderes y defensores con las regiones priorizadas para la implementación de los Acuerdos de Paz**. De igual forma manifestó que las zonas en donde hay más amenazas de estructuras narco-paramilitares coinciden con los territorios en donde más han asesinado personas.

Lo que de acuerdo con el informe traduce que “a mayor apertura de posibilidades de participación política, de reforma agraria y de transformación social, aumentan las violaciones de derechos humanos”.

<iframe id="audio_21540888" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_21540888_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
