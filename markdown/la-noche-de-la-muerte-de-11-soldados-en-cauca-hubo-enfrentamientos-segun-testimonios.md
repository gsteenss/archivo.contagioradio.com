Title: Testimonios de habitantes de la vereda "La Esperanza" en Cauca desmienten versión oficial
Date: 2015-04-17 11:33
Author: CtgAdm
Category: Entrevistas, Paz
Tags: Buenos Aires Cauca, cese al fuego unilateral FARC-EP, Conversaciones de paz en Colombia, Cristian Delgado, FARC, Fuerza de Tarea Apolo, Juan Manuel Santos, Proceso de paz en Colombia, Red de Derechos Humanos Francisco Isaías Cifuentes, vereda “la esperanza”
Slug: la-noche-de-la-muerte-de-11-soldados-en-cauca-hubo-enfrentamientos-segun-testimonios
Status: published

###### Foto: pacifista.com 

<iframe src="http://www.ivoox.com/player_ek_4369410_2_1.html?data=lZijm5mVdI6ZmKialJmJd6Knk5KSmaiRdo6ZmKiakpKJe6ShkZKSmaiRkMKfz9TQysqPqMafzcaYz9rJttXZjMnSjZaVb9TjzcnOxtTXb8bijKjO18jFb8npw9SYx9PKto6ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Cristian Delgado, Red de DDHH Francisco Isaías Cifuentes] 

La versión de los habitantes de la vereda “La Esperanza”, lugar en el que se registró la muerte de 11 soldados adscritos a la **Fuerza de Tarea Apolo** de las **FFMM**, es muy diferente a la que se ha conocido como la versión oficial.

Según la verificación realizada por la Red de DDHH Francisco Isaías Cifuentes, cerca de **300 habitantes del caserío**, denunciaron que los **militares se habían apostado en el polideportivo del caserío desde el pasado 19 de Marzo** y no se retiraron de allí, pese a los reclamos de los habitantes, argumentando que “tenían una orden superior” para permanecer en el centro urbano.

Según los testimonios entregados por los habitantes de la **vereda “La Esperanza”** en el municipio de Buenos Aires, Cauca, **los disparos comenzaron a escucharse hacia las 11:30 de la noche del martes y solo terminaron entre las 5 am y la 6 am del miércoles 15 de abril**, este testimonio deja claridad en el hecho de que si hubo combates entre el Ejercito y la Guerrilla, y no un ataque unilateral, como han afirmado los medios.

En cuanto a las afirmaciones de oficiales militares, en torno a que los militares estaban en el **polideportivo** para resguardarse de la lluvia, y solamente desde la misma noche del enfrentamiento, la comunidad afirma que **cuando comenzaron los combates no estaba lloviendo** y que sólo después de varias horas de comenzados los enfrentamientos fue que el clima comenzó a arreciar.

Según **Cristian Delgado**, integrante de la **Red de Derechos Humanos Francisco Isaías Cifuentes**, los habitantes de la vereda La Esperanza, solicitaron a los militares que se retiraran del Polideportivo puesto que es un espacio para la población civil y se encuentra a tan solo 50 metros del sector poblado, los militares se retiraron durante 4 días pero volvieron y se asentaron allí, instalando una **base militar desde el 11 de Abril, es decir, 5 días antes del hecho que dejó un saldo de 11 militares muertos**. Esta evidencia dejaría claro que los militares, asentados en la base si tuvieron las condiciones para defenderse siguiendo los protocolos de seguridad para la instalación de una base militar.

Adicionalmente los habitantes denunciaron, ante la delegación del Frente Amplio, que **en la cocina de una de las casas del sector poblado, se encontró el cuerpo sin vida de uno de los militares**, lo cual evidenciaría que hubo clara infracción del **Derecho Internacional Humanitario**, al usar el polideportivo como base militar, pero también al buscar las casas de los civiles para resguardarse en medio de un combate y en una zona de alta confrontación.

Así las cosas, la versión entregada por militares y reproducida por diversos medios de información quedaría desmentida por los habitantes de la vereda, quienes son los testigos directos del lamentable hecho.
