Title: Los hijos de los días presentan "Al margen del tiempo"
Date: 2017-07-20 11:00
Category: eventos
Tags: Bogotá, Hip Hop, Música
Slug: al-margen-del-tiempo-hip-hop
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/07/18077050_1529706673707455_5938086016344301283_o.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Los hijos de los días 

###### 20 Jul 2017 

La agrupación bogotana de Hip hop **Los hijos de los días**, lanzará la tercera edición de su disco "**Al margen del tiempo**", en el que recogen lo más importante de su trayectoria musical hasta la fecha. Un trabajo que representa para la agrupación "el cierre de un ciclo" antes de concluir la producción de su nuevo material.

La primer edición del albúm fue lanzada virtualmente hace 3 años y hace dos una segunda en la que se incluyó el proyecto "historias de andén" presentada en el marco de la Feria del libro de Bogotá. La actual y definitiva **combina la línea de reflexión social y crítica política, con canciones que exploran temas de la sensibilidad humana como el amor y la familia**.

<iframe src="https://www.youtube.com/embed/ILQuIfNX8LE" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

"Somos una voz que llama a unir todos los esfuerzos por la construcción de paz en el país aunque también **somos críticos con el tipo de paz que quiere implementar el gobierno**" asegura Camilo Cely, integrante del trío, al referirse a las realidades que han inspirado las líricas de gran parte de los temas incluidos en las primeras ediciones del disco. (Le puede interesar: [5 de los conciertos benéficos más memorables](https://archivo.contagioradio.com/rock-cociertos-beneficos/)).

En cuanto a lo musical, **la exploración en "Al margen del tiempo" les ha permitido realizar acercamientos a géneros como la salsa y el jazz,** particularmente el el primer corte del disco "Allá me espera ella", que cuenta con la colaboración del cantaautor Fernando Cely, tema que hace parte de la banda sonora del cortometraje **Invisibles**, "una polifonía de alguien que habla desde la cárcel" como lo describe Camilo Cely quién es además director de la producción audiovisual.

El lanzamiento tendrá lugar el **viernes 21 de julio** en la **Casa Cultural Nuestra América Calle 45 \# 21 - 07** , en una noche de Jam que inicia a las **8 de la noche** donde la agrupación estará acompañada por Cazomizo & Luis Francisco Abreu a.k.a. CISCO en el microfono y de Nicolás Oyuela Jiménez a.k.a. NCK, en los beats. El costo de la entrada es de **\$15.000** incluyendo el disco y una cerveza.

<iframe id="audio_19895464" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_19895464_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>
