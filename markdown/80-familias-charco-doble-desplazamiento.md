Title: 80 familias de El Charco, Nariño, son víctimas de doble desplazamiento
Date: 2018-09-26 12:33
Author: AdminContagio
Category: DDHH, Nacional
Tags: Desplazamiento forzado, Enfrentamientos, nariño, retorno, Verificación Internacional
Slug: 80-familias-charco-doble-desplazamiento
Status: published

###### [Foto: Contagio Radio] 

###### [26 Sep 2018] 

Cerca de **80 familias fueron obligadas a retornar** a la vereda **Las Mercedes,** en el Municipio de **El Charco, Nariño**, tras el desplazamiento del que fueron víctimas por enfrentamientos armados que se presentaron en la zona. Los habitantes denuncian que no han contado con el acompañamiento institucional adecuado para evitar que nuevas confrontaciones terminen desplazando otra vez a la población.

Según un habitante de Las Mercedes, la situación de desplazamiento se presentó el pasado 30 de agosto, cuando grupos armados se enfrentaron en la vereda, obligando a 80 familias que quedaron bajo fuego cruzado a trasladarse hasta la cabecera municipal. Allí, hablaron con el Alcalde, quien con el aval del rector del Colegio Nuestra Señora del Cármen, les permitió hospedarse en sus instalaciones.

No obstante, **las familias alojadas en el Colegio fueron víctimas de** [**persecución**]** y maltrato verbal por parte de la comunidad académica**, específicamente de los padres de los estudiantes, quienes señalaron que las personas debían salir de este lugar porque estaban afectando las actividades escolares.

### **El Retorno Obligado: Un nuevo desplazamiento** 

Tras varias reuniones con las autoridades municipales, en las que señalaron que El Charco no tiene un sitio adecuado para albergar a las familias, los habitantes de Las Mercedes optaron por retirarse del lugar, y **dejar constancia de que su retorno es obligado, y no hay condiciones humanitarias para volver a la vereda.**

El habitante de las Mercedes sostuvo que aunque en el primer instante gozaron de acompañamiento internacional, para el retorno no se dio verificación que garantice el fin del riesgo en la Zona. (Le puede interesar: ["Se agudiza crisis humanitaria en Chocó"](https://archivo.contagioradio.com/se-agudiza-crisis-humanitaria-en-choco/))

Adicionalmente, recordó que en el lugar opera una Brigada Móvil del Ejército, por lo tanto, no cuentan con ninguna entidad institucional permanente en la zona; por estas razones, **la comunidad esta atemorizada de tener que huir una vez más de la zona, por nuevos enfrentamientos armados.**

[Comunicado Situacion Humani...](https://www.scribd.com/embeds/389245996/content#from_embed "View Comunicado Situacion Humanitaria El Charco on Scribd") by on Scribd

<iframe class="scribd_iframe_embed" title="Comunicado Situacion Humanitaria El Charco" src="https://www.scribd.com/embeds/389245996/content?start_page=1&amp;view_mode=scroll&amp;show_recommendations=true&amp;access_key=key-cHQEAJCzJynxwmbYT2pU" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="true" data-aspect-ratio="null"></iframe><iframe id="audio_28911749" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_28911749_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en] [su correo](http://bit.ly/1nvAO4u) [o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por ][Contagio Radio](http://bit.ly/1ICYhVU). 
