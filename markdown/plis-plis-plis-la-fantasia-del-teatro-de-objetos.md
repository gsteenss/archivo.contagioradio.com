Title: "Plis, plis, plis" la fantasía del teatro de objetos
Date: 2015-08-19 16:00
Category: Cultura, Hablemos alguito
Tags: Corporación Colombiana de Teatro, Festival Mujeres en escena por la paz, Obra teatral Plis plis plis, Ulularia Teatro
Slug: plis-plis-plis-la-fantasia-del-teatro-de-objetos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/08/plis-e1440003808408.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<iframe src="http://www.ivoox.com/player_ek_6448439_2_1.html?data=l5mhmpmXfY6ZmKiakpaJd6Kkl4qgo5qacYarpJKfj4qbh46kjoqkpZKUcYarpJKYstHNt4ampJC9zs7XaZO3jLXZy9iSb7Xjxcbjh6iXaaK4wpC%2B18rIpc%2BftdfS1ZCoqdTZ0JC90dePlMbYyteah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Hablemos alguito,entrevista con Ulularia Teatro] 

###### [19, Ago, 2015] 

En "Hablemos alguito", nos acompañaron la directora María Laura Gallo y la dramaturga Lucía Miani, integrantes de "Ulularia Teatro", quienes desde la provincia de Córdoba, Argentina, presentaron en el marco de la 24 edición del Festival de Mujeres en escena por la paz en Bogotá la obra: "Plis, plis, plis, todavía quedan 3 deseos por pedir hoy".

"Para nosotras, Plis plis plis es una obra en la que se cruzan el lenguaje del teatro de objetos y elementos de la técnica del Clown, no puros, sino mixtura, impura, fuera de los límites", explica María Laura Galla, es una obra "autoreferencial, la versión teatral de una vida, la memoria de los fragmentos contados teatralmente, poeticamente y simbolicamente".

[![plis plis plis](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/08/plis-plis-plis.jpg){.alignleft .wp-image-12365 width="300" height="427"}](https://archivo.contagioradio.com/?attachment_id=12365)

Con 18 años de trayectoría de manera independiente, "Ulularia" encuentra en el teatro un espacio de posicionamiento, lucha y creación de lazo[s, con una firme creencia en el trabajo colectivo, haciendo parte del colectivo "Señores niños al teatro" con el objetivo fundamental de lograr que el teatro llegue a todas partes como un derecho de todas las personas, en particular de niños, niñas y jóvenes.]{.text_exposed_show}

Gallo y Miani, comparten sus vivencias personales, su trayectoría académica y artística, así como algunas anécdotas realcionadas con el entorno social y político en el que se conformó y desarrolla el proyecto teatral, que combina lenguajes como el clown, el audiovisual y la animación de objetos y muñecos.

[  
](https://archivo.contagioradio.com/?attachment_id=12366) [![Ulularia](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/08/IMG-20150819-WA0014-e1440004048351.jpg){.wp-image-12367 .alignright width="268" height="231"}](https://archivo.contagioradio.com/?attachment_id=12367)**Sinopsis de la obra:**

Ella narra su vida,  son  recortes, no importa  el orden, recuerda  lo que  soñaba  ser y  no fue. Se transforma para ser lo que le da la gana. Despliega  fragmentos familiares; mamá y  la extraña proporción de su universo, papá; un teléfono peleado con el mundo. Y  ella, una niña que guarda tres  deseos en su  diario. Se abraza a los  refugios que la salvan. Y un día se va  para buscar su mirada…

 
