Title: Colombia está atrasada 15 años en la contratación de jueces: ASONAL Judicial
Date: 2019-10-02 15:39
Author: CtgAdm
Category: Judicial, Movilización
Tags: ASONAL, judicial, Juzgados, paro
Slug: colombia-esta-atrasada-15-anos-contratacion-jueces-asonal-judicial
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/Asonal.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: [@KarolaEnriquez] 

Este miércoles 2 y jueves 3 de octubre, **la Asociación Nacional de Funcionarios y Empleados de la Rama Judicial (ASONAL) desarrollarán una jornada de protesta** en la que pedirán la inyección de recursos que garanticen el funcionamiento del sistema judicial. Según los manifestantes, los recursos son más que necesarios, porque Colombia tiene un déficit de personal que no permite garantizar el acceso a la justicia de los ciudadanos.

### **"Mientras la demanda de justicia ha crecido un 323%, la rama ha crecido un 22%"** 

**Luis Fernando Otálvaro, presidente de ASONAL**, indicó que la rama necesita mayor presupuesto "para poder crecer a la par que crece la demanda de justicia en el país". Según referenció, Colombia está atrasada en la contratación de jueces, y **en este momento tiene los jueces que debió tener hace 15 años**, ello significa que "mientras la demanda de justicia ha crecido un 323%, la rama ha crecido en un 22%".

Los efectos de esta situación se observan en el retraso de los procesos judiciales, que afectan igualmente a los trabajadores de la rama, víctimas de exceso de trabajo y problemas de acoso laboral. De acuerdo a lo señalado por Otálvaro, aunque se cuestione la eficiencia del trabajo que realizan los funcionarios judiciales, lo cierto es que "el problema es del Estado que no ha dado recursos".

> [\#EsJustaLaProtestaDeLaRamaJudicial](https://twitter.com/hashtag/EsJustaLaProtestaDeLaRamaJudicial?src=hash&ref_src=twsrc%5Etfw) porque la planta de personal es insuficiente para atender la creciente demanda de justicia. [pic.twitter.com/zvRFRwzS5c](https://t.co/zvRFRwzS5c)
>
> — Karola Enriquez (@KarolaEnriquez) [October 2, 2019](https://twitter.com/KarolaEnriquez/status/1179400942371921923?ref_src=twsrc%5Etfw)

<p>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
</p>
### **¿Cómo está la rama en el Presupuesto General de la Nación para 2020?** 

Otalvaro declaró que la rama judicial pidió para el próximo año 5,3 billones para su funcionamiento pero el Gobierno concedió solo 4,3 billones, por su parte, **la Fiscalía solicitó 6 billones pero les sería entregados solo 3,8.** Ello significa que para 2020 la rama **no podría crecer en planta de personal,** que según el Consejo Superior de la Judicatura debería aumentar en 3 mil cargos, mientras la Fiscalía requería cerca de 4 mil cargos nuevos.

A ello se suma la falta de recursos para tener instalaciones y equipos tecnológicos necesarios para adelantar las audiencias, así como para que el ente acusador cuente con las herramientas necesarias para enfrentar la delincuencia y adelantar las investigaciones pertinentes. (Le puede interesar:["Más de cien mil investigaciones de la Fiscalía fortalecerán búsqueda de personas desaparecidas"](https://archivo.contagioradio.com/mas-de-cien-mil-investigaciones-de-la-fiscalia-fortaleceran-busqueda-de-personas-desaparecidas/))

### **Que se preste atención a la rama, y se reforme lo que necesitan los ciudadanos** 

El presidente reclamó que siempre se necesite hacer una jornada de movilización para que se preste atención a la rama, por ejemplo, el pasado 12 de septiembre se realizó una jornada nacional que permitió que el Consejo Superior de la Judicatura aceptara discutir algunos puntos en torno a la carrera judicial, capacitación y traslado de funcionarios; al tiempo que fue la oportunidad para que la Ministra de Justicia se reuniera con los funcionarios.

Gracias a ello, Otalvaro señaló que tienen antecedentes que demuestran que sí se pueden brindar soluciones a los problemas.  Adicionalmente, pidió que en caso de presentarse una reforma a la justicia, no se toque la tutela; en lugar de ello, propuso "mejorar la calidad de servicio de las instituciones que son tuteladas a diario como las EPS y los municipios, que no están brindando los recursos necesarios para la salud o la educación, y  que no responden las peticiones que les hacen".

<iframe id="audio_42590543" frameborder="0" allowfullscreen scrolling="no" height="200" style="border:1px solid #EEE; box-sizing:border-box; width:100%;" src="https://co.ivoox.com/es/player_ej_42590543_4_1.html?c1=ff6600"></iframe>  
**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
