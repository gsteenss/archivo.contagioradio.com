Title: Estado Islámico avanza y logra controlar la mitad de Siria
Date: 2015-05-22 13:28
Author: CtgAdm
Category: El mundo, Otra Mirada
Tags: Control yihadista de los puestos fronterizos entre Irak y Siria, Estado Islámico, Estado Islámico controla la mitad de Siria, Irak controlado por los yihadistas, Siria caída de Palmira
Slug: estado-islamico-avanza-y-logra-controlar-la-mitad-de-siria
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/05/Free_Syrian_Army_soldier_walking_among_rubble_in_Aleppo.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto:es.wikipedia.org 

Según ha informado el **Observatorio Sirio para los DDHH,** el Estado Islámico ha tomado el último puesto fronterizo entre Irak y Siria, consolidando así la toma del norte de Irak y el sur de Siria.

Después de la toma de la ciudad **Irakí de Ramadi y la ciudad Siria de Palmira**, en la frontera con Irak, el toma fuerza y controla ya la mitad de Siria. Los yihadistas han resistido los ataques de la coalición gubernamental entre EEUU, Siria, Irak y otros países del mundo árabe.

**Al Tanaf es el puesto fronterizo** entre ambos países que ahora está en manos islamistas, considerado de vital importancia geo-estratégica. Significa un duro golpe para la coalición internacional que está contraatacando a los yihadistas.

Además, él EI **controla toda la provincia de Homs en Siria**, donde han **ocupado el gaseoducto** de mayor importancia para el régimen de Al-Asad. Con esta última conquista accederían a todas las zonas menos pobladas de Siria.

Los yihadistas están realizando registros casa por casa y ajusticiando a soldados del ejercito Sirio. Organizaciones sociales denuncian **miles de desplazados y constantes violaciones de DDHH** en las zonas bajo control del EI.

La **UNESCO** ha mostrado el temor de que las ruinas arqueológicas de la ciudad de Palmira sean destruidas, ya que el EI no es la primera vez que ataca monumentos declarados Patrimonio de la Humanidad.

Mandatarios **Estadounidenses han expresado su preocupación por el avance del EI** y de la necesidad de cambiar de estrategia para poder hacerles frente mediante coaliciones efectivas de países aliados en la región.
