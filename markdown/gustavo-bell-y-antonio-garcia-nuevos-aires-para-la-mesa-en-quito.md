Title: Los retos para los nuevos equipos en la mesa de diálogos entre gobierno y  ELN
Date: 2017-12-20 14:48
Category: Nacional, Paz
Tags: ELN, Gustavo Bell, Mesa en Quito
Slug: gustavo-bell-y-antonio-garcia-nuevos-aires-para-la-mesa-en-quito
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/02/proceso-de-paz-eln.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: El Telégrafo] 

###### [20 Dic 2018] 

Gustavo Bell asumió la jafatura del equipo de diálogos del proceso de paz con el ELN. De acuerdo con el analista político Luis Celis, se espera que **aporte a este espacio con la trayectoria política y la tranquilidad que ha demostrado en diferentes escenarios**. Otro de los cambios que podría haber dentro de la mesa, es que **Antonio García, integrante del COCE, asuma también el liderazgo por parte del ELN**.

Para Celis, Gustavo Bell aportará a los diálogos en Quito los conocimientos que adquirió como vicepresidente del país durante el periodo de gobierno de Pastrana, y su personalidad pausada y equilibrada, necesaria para las particularidades de este proceso.

Referente a los demás integrantes que conforman el equipo del gobierno en esta mesa, Eduardo Celis señaló que considera que es importante que no se cambie la totalidad del equipo porque ya conocen las metodología y dinámicas, en ese sentido recalcó las participaciones del General Padilla León, de los exministros Juan Meyer y Luz Helena Sarmiento, sin embargo, se espera que** en los próximos días se conozcan los nombres de dos o tres nuevos integrantes de la mesa**.

Sobre el posible nombramiento de Antonio García como jefe del equipo de diálogos por parte del ELN, Celis afirmó que esta guerrilla está en plena autonomía para tomar decisiones sobre la conformación de su equipo, pero que considera que en efecto lo mejor para este equipo negociador **es que este conformado por la mayor cantidad de miembros del COCE**.

### **Las apuestas del proceso de paz en Quito para el 2018** 

De acuerdo con Luís Eduardo Celis, el año 2018 tiene retos muy concretos para que el acuerdo de paz entre la guerrilla del ELN y el Gobierno sea una realidad, **el primero de ellos tiene que ver con la concertación del proceso metodológico de participación**, como resultado de las audiencias de Tocancipa, en la que estuvieron más de 120 delegaciones.

Este será uno de los puntos principales de la agenda de conversaciones de la quinta ronda que iniciará en enero del próximo año. (Le puede interesar: ["ELN plantea la posibilidad de extener el cese al fuego bilateral en el 2018"](https://archivo.contagioradio.com/eln-plantea-la-posibilidad-de-extender-cese-al-fuego-bilateral-en-2018/))

Sobre el cese bilateral, Luís Eduardo Celis, expresó que en general se puede hacer un “muy buen balance de el” debido a que durante el tiempo que estuvo se registraron cero enfrentamientos entre el LEN y la Fuerza Pública, **tampoco se han registrado ataques a los oleoductos y hay una sensación de calma en los pueblos y territorios del país en donde hay una fuerte presencia de esta guerrilla**.

No obstante, el analista político señaló que se han cometido errores que deben evaluarse como las agresiones que la guerrilla ha cometido contra la población, ejemplo de ello es que continúan instalando minas antipersona en los territorios, el asesinato del gobernador Isarama y la masacre Magüi Payán, en Nariño. Además, para Celis **es un error que el cese bilateral finalice y se deba esperar hasta el ciclo de conversaciones de enero para decidir si se retoma o no**.

### **¿Quién es Quién? ** 

Gustavo Bell es un socioeconomista de la Universidad Javeriana, que cuenta con una especialización en Derecho Penal de la Universidad de Los Andes y estudios en doctorado de la Universidad de Oxford. Se ha desempeñado como gobernador del Atlántico desde 1991 hasta 1994 e hizo parte de los académicos que estuvieron en la reforma constitucional de 1991.

En 1998, Andrés Pastrana lo convoco a ser su formula vicepresidencial, cargo en el que estuvo desde 1998 hasta el 2002. Desde el 2005 hasta el 2010 se desempeño como director del Diario El Heraldo y posteriormente como **embajador de Colombia en Cuba desde el 2011 hasta el 2016**.

Antonio García se incorporó a las filas del Ejército Nacional de Liberación en 1987, cuando estudiaba ingeniería electrónica en la Universidad Industríal de Santander. Al inició de la década de los 80, durante el proceso de re unificación de la guerrilla, **García asumió el nombramiento como integrante del Comando Central, instancia de dirección del ELN hasta la actualidad**.

###### Reciba toda la información de Contagio Radio en [[su correo]
