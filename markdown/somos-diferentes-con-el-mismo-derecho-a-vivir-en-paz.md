Title: Somos  Diferentes con el  mismo derecho a vivir  en  paz
Date: 2015-09-25 07:45
Category: closet, LGBTI
Tags: Alcaldía Mayor de Bogotá, Comunidad LGBTI, diversidad Sexual y de Géneros, Feria por la Igualdad- Entre Iguales, LGBTI, semana de la igualdad
Slug: somos-diferentes-con-el-mismo-derecho-a-vivir-en-paz
Status: published

###### Foto: Contagio Radio 

<iframe src="http://www.ivoox.com/player_ek_8610805_2_1.html?data=mZuekp2UeY6ZmKiak5aJd6KllJKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRl9Dh0NiYh6iWaaKkpc7Tx9fJstXZ1JDQ0dOPqc2fhqifh6aUscrnztSYxsrWqcTc0JDOjdvNusrmjIqwlIqldMbijJKSmaiRh9Di1cbUy9SPlsLYytSYj4qbh46o&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Juan Carlos Prieto, Diversidad  sexual  en  Bogotá] 

###### 25 de Sep 2015 

Bajo el eslogan "Somos  Diferentes con el  mismo derecho a vivir  en  paz",  se  abre  paso la **quinta semana  por  la  diversidad Sexual y de Géneros**, dirigida  por  Juan Carlos Prieto, director  de  diversidad  sexual  en  Bogotá. Desde  el  24 de Septiembre hasta  el 3 de Octubre del 2015,  Bogotá podrá  disfrutar  de  diversas actividades encaminadas  al reconocimiento y  la   igualdad,  llevando un latente y colorido  mensaje  de  paz.

Prieto  asegura que  la intención  de  la  semana  por la  diversidad  es  generar  un reconocimiento  internacional,  posicionando  a  Bogotá  como  un  sitio  turístico  para la **comunidad  LGBTI**.  Al  igual  que   esta  semana,  sea un  referente  como Rock al Parque o  el  Festival Iberoamericano de Teatro. “Si  bien  es  cierto que  esta  semana  lleva una corta edad, se pasó de  2.000 espectadores a 20.000” “Buscamos  cambiar los imaginarios de la ciudanía  en  general, no se  busca  homogenizar,  solo acceder a los mismos derechos”  afirma.

El  gran  lanzamiento  de  la **Semana  por la  diversidad  Sexual y  de Género** tendrá lugar en la Plazoleta  de las  mariposas de la Alcaldía Mayor  de Bogotá el 24  de septiembre,  en donde  se  podrá  presenciar  las  representantes  de Mujer T  2015, reconocimientos internacionales  y  el  concierto  de  Jerau (artista local) y la  Prohibida (invitada internacional).

Actívate por  la  Igualdad es una iniciativa  encaminada  a cambiar  ese  imaginario que “las personas  LGBTI son  solo  rumba”, pues el  27  de  septiembre los  gais  se  toman abiertamente la  ciclovía en una   jornada  de  aeróbicos con  mensajes de inclusión   y  no discriminación  a  sus participantes,  a partir  de las  8:00 am todos  los  visitantes del parque  Nacional  podrán participar  de esta  actividad y ser  merecedores de kits deportivos.

Bajo  el  marco académico  la  semana  por  la  diversidad  abre las puertas  a  invitados   y ponentes  al  Primer  Congreso  Internacional sobre Derechos Humanos  de las Personas con Orientaciones Sexuales e Identidades de Género Diversas. En donde se  podrá asistir a conferencias  encaminadas   sobre  el  desarrollo  y  la  igualdad en  países  como  Chile, Honduras, Nicaragua   y  Sudáfrica. Durante  el  28  y 29  de septiembre este  congreso tendrá   lugar  en el auditorio Huitaca de la Alcaldía Mayor de Bogotá. Son  pocos  cupos  y la  inscripción es  gratuita.

La  cultura y el  arte   hacen  parte también   de  esta  semana  y  por  ello nos  invitan a ExpresARTE por la igualdad que  se desarrollará  en la Fundación  Gilberto Alzate Avendaño el próximo 28  de  septiembre,  en  donde  se le  dará  un reconocimiento   y premiación a los  mejores  artistas y  muestras   culturales  de las personas de los sectores LGBTI con la  participación  de  Jamaruk  como artista   invitado.

Juan Carlos  comenta que  uno de los  eventos  que  generó “más  ampolla” en  la  sociedad es Mujer T puesto que “ la  sociedad  creía  que  se  trataba  de  un  reinado de  chicas transexuales” siguiendo  su  estructura  pero  no su   objetivo,  Mujer  T  busca  visibilizar un sector  de las personas LGBTI  no tan visible. En donde las  representantes  de  cada localidad visibilizan  lo  procesos de  empoderamiento  de su localidad  bajo el  discurso  y el conocimiento. Desde las  6 de la  tarde el 30 de septiembre,  el  Teatro  Jorge Eliecer Gaitán  abre  las  puertas  a las representantes  de  cada localidad  en  Mujer T  junto a Maia y las Extrellas orquesta  como artistas invitados.

La  apuesta  diferencial de  esta  Semana por la Igualdad  es la  de sectorización y la visibilización  de las personas LGBTI, por  ello   Juan Carlos  propuso  que  la Feria por la Igualdad- Entre Iguales se  desarrollara  en  la  plazoleta de la calle  85 con  carrera  15 “Lugar  concurrido  por los  jóvenes  para parchar  un  rato” Desde  las  2  de la tarde este  2 de  Octubre todos  los asistentes a  este  evento podrán interactuar  con  organizaciones y grupos  juveniles LGBTI para  la  deconstrucción de  imaginarios negativos, con la participación especial  de Naty Botero.

Para  concluir esta  Semana  por la  Diversidad  Sexual y de Género el   3  de  Octubre El parque Nacional  hasta  la Plazoleta de Lourdes  serán  testigos  de Bogotá Noche de Colores, para  darle ese  toque  turístico  e  iniciar  con esta iniciativa, Desde las   6:00 de la tarde  se  darán  cita comparsas, carrozas  y más  sorpresas  que  estarán difundiendo  el arte y la  cultura.

Dando  finalización  con broche  de oro a la  semana, se  presenciará  en  la Plazoleta  de Lourdes El concierto  por la Igualdad en  donde  The Mills, Marbelle  y Mike Baíha estarán encargados de dar un  mensaje  sentido  en  contra  de la homofobia y la discriminación  sexual y  de género. Se anunciará un homenaje a Sergio Urrego, joven estudiante  que  tras  discriminación por parte de  su colegio se quitó  la  vida. Con una placa  conmemorativa en la Plazoleta de Lourdes, Bogotá dirá no a la  discriminación sexual y de  género.
