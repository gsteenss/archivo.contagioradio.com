Title: FARC teme nuevas capturas y allanamientos en los próximos días
Date: 2018-04-10 18:14
Category: Nacional, Paz
Tags: FARC, Fiscal General, Jesús Santrich, Nestor Humberto Martínez
Slug: farc_teme_nuevos_allanamientos_capturas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/04/farc-e1523400908842.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: 360 Radio] 

###### [10 Abr 2018 ] 

"No se está garantizando el debido proceso, el Tribunal de la JEP era el que tenía que proceder", afirma Gustavo Gallardo, abogado de Jesús Santrich, integrante de la Fuerza Alternativa Revolucionaria del Común (FARC), capturado este lunes, en el marco de un allanamiento por una investigación de la DEA. Se presume que Santrich habría cometido actos de narcotráfico.

Seuxis Hernández Solart, nombre real del integrante de la FARC, fue retenido por una orden de la Interpool, expedida por una Corte de Nueva York, para dar captura internacional con fines de extradición por los delitos de conspiración para el narcotráfico. (Le puede interesar: ["Captura de Santrich es un montaje")](https://archivo.contagioradio.com/captura-de-jesus-santrich-es-un-montaje-ivan-marquez/)

Sin embargo, la Fiscalía de Colombia no sería la autoridad competente para hacer el trámite  de los procedimientos judiciales. El acto Legislativo para la paz del año 2017 y el propio acuerdo de paz establecen que, para estas situaciones, es el **Tribunal en su sección de revisión de la Jurisdicción Especial para la Paz era quien debía valorar el caso** para efectuar las acciones necesarias.

### **¿Montaje judicial?** 

“**Nos enteramos de la captura de Santrich por los medios”**, fue lo que aseguró la presidenta de la JEP, y agregó que “El señor Hernández Solarte es sujeto de la Jurisdicción Especial de Paz en la medida en que él se sometió a esta jurisdicción”.

Tal situación ha sido calificada por los integrantes de la FARC como un montaje judicial que afecta y viola el acuerdo pactado en La Habana. De acuerdo con el abogado Gallardo, la forma como ha actuado el Estado colombiano deja ver que no se ha respetado el debido proceso, de igual forma lo ve Enrique Enrique Santiago, asesor de la FARC en La Habana, que en otros medios de comunicación afirmó "l**a justicia ordinaria está actuando sin dar lugar a que se actúe como está pactado, es decir, primero la JEP y luego se mira quién debe llevar el caso".**

Con esa forma de llevar el caso, para Santiago, "lo que se pretende es poner el acuerdo de paz en un callejón sin salida". De hecho el abogado Gallardo, asegura que "en Colombia no hay un proceso, no hay un pedido formal de extradición y por tanto no hay expediente, Además a la defensa no han entregado las supuestas pruebas", y agrega que formalmente no hay un descubrimiento probatorio.

Ahora lo que tiene que suceder es que el Tribunal deberá proceder a pedir el expediente para continuar con el trámite conforme a las normas de la JEP, para determinar si es o no de su competencia. (Le puede interesar:[Quieren acabar con el partido FARC)](https://archivo.contagioradio.com/?p=52697)

### **Santrich asegura su inocencia** 

El tiempo en el que tuvo la oportunidad el abogado de Santrich de escucharlo, este le expresó **"categóricamente que no es cierto, que tiene cómo demostrar su actividad lícita durante todo el proceso** en el marco de la implementación de los acuerdos, de su trabajo en la CSIVI (Comisión de Seguimiento, Impulso y Verificación a la Implementación del Acuerdo Final), y en el marco de su actividad política".

Respecto a las demás personas que capturaron junto a Santrich, estas no tendrían relación directa con él.  También, preocupa a la defensa que en medio del allanamiento y sin autorización se llevaron computadores y celulares propiedad de la FARC. "**De ahí en adelante puede continuar el montaje por todos los elementos extraídos** (...) esperamos que haya una buena cadena de custodia y se respete el debido proceso, pero como van las cosas tenemos pocas expectativas", manifiesta Gallardo.

Asimismo desde la defensa se está verificando cómo concluyó el allanamiento del lunes, porque denuncian excesos por parte de los funcionarios del CTI, dejando a las mujeres y niños que habitan ese inmueble en le barrio Modelia de Bogotá, heridos por la desproporcionada acción por parte de las autoridades.

### **Piden respeto al acuerdo de paz** 

"Logrado el propósito de la dejación de armas, el gobierno se desinteresó totalmente por el cumplimiento, que ahora aparece como una obligación internacional del Estado (...) L**e pedimos al presidente que no permita que la injerencias externas, unos legisladores y un fiscal destroce el acuerdo de paz"**, dijo Iván Márquez, integrante del secretariado de la FARC, durante la rueda de prensa de la mañana de este martes.

Por su parte, desde Justice for Colombia consideran como preocupante las implicaciones por de la detención de Jesús Santrich, "ayer por la mañana, nuestra delegación se reunió con el Sr. Santrich y miembros de la dirección de la FARC. Sentimos que hay un compromiso absoluto y sin reservas con el proceso de paz", dicen en un comunicado.

Por ello, al igual que la ONU, la FARC y otras organizaciones sociales, instan al gobierno  a garantizar "un proceso legal, transparente y justo" para el integrante del movimiento político, y así se cumpla totalmente con los términos del acuerdo de paz. Por tanto, que sea la JEP la que revise el caso.

### **Podría haber más  más capturas** 

De acuerdo con informaciones de fuentes oficiales, es posible que en los próximos días haya otras capturas de los ideólogos de la FARC. Analistas consultados por Contagio Radio señalan que **lo que busca el Fiscal Néstor Humberto Martínez "es sepultar la JEP para que el fiscal asuma todas competencias".**

La misma fuente afirma que de esa forma se podría proteger al senador del Centro Democrático, Álvaro Uribe, se controla nuevamente el Congreso y se logra poner límites a las investigaciones que existen en la Corte Suprema de Justicia contra el expresidente. Es decir que **el fiscal estaría jugando a favor de Iván Duque y se habría desprendido de Vargas Lleras, para a su vez "ganarse a Estados Unidos".**

De hecho, Márquez indica que lo que busca el fiscal es proteger a terceros para que no se sepa la verdad frente a las responsabilidades sobre el conflicto armado. A su vez, a la FARC,  le preocupa las afirmaciones del presidente Santos frente a que **van a continuar los allanamientos y capturas en todo el territorio nacional, "imagínense eso qué impacto puede causar en un excombatiente o un jefe exguerrillero".**

Cabe recordar que en una reciente columna escrita por Adam Isacson, director de WOLA en Estados Unidos, "los exinsurgentes se enfrentan a la posibilidad de más derrotas si, como las encuestas indican que podría ocurrir, un opositor del acuerdo de paz, Iván Duque, candidato del partido de derecha Centro Democrático, gana la elección presidencial de Colombia el 27 de mayo".

La misma columna agrega, "una Colombia ingobernable sería un desastre para los intereses de Estados Unidos porque un aliado inestable —y el tercer país más poblado de América Latina— podría producir más cocaína, ahuyentar a los accionistas y exportar más delincuencia organizada".

###### Reciba toda la información de Contagio Radio en [[su correo]
