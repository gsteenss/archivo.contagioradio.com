Title: COALICO insta a ELN y Gobierno a incluir protección de menores de edad en agenda de diálogo
Date: 2017-08-31 15:00
Category: Nacional, Paz
Tags: Diálogos en Quito, Menores de edad, proceso de paz
Slug: coalico-insta-a-eln-y-gobierno-a-incluir-proteccion-de-menores-de-edad-en-agenda-de-dialogo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/08/OAfrica-a-Setba.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:Fundación Sebta] 

###### [31 Ago 2017] 

Varias organizaciones que trabajan en la defensa de los derechos de los niños y las niñas, y se agrupan en la plataforma COALICO, enviaron una carta al presidente Juan Manuel Santos y al Ejército de Liberación Nacional, pidiendo que incluyan en la agenda de diálogos de Quito, el cese de actos que violen los derechos de los niños, niñas y jóvenes, así como **la puesta en marcha de procesos claros y tempranos que garanticen la desvinculación de los menores de esta organización**.

De acuerdo con el documento, “**preocupa que aún en el camino recorrido entre el Gobierno y el ELN**, las afectaciones contra los niños y niñas por el conflicto armado no sea un tema prioritario de la agenda y parte de los actos humanitarios para afianzar y dar confianza en el desarrollo de esta negociación”. (Le puede interesar: ["ELN estudiaría cese unilateral si gobierno no cede a un cese bilateral"](https://archivo.contagioradio.com/eln-estudiaria-cese-unilateral-si-gobierno-no-cede-a-un-cese-bilateral/))

Las organizaciones entre las que se encuentran  la Asociación Cristiana Menonita para Justicia, Paz y Acción No Violencia –Justapaz, la Asociación Taller de Vida, Benposta Nación de Muchach@s, la Corporación Casa Amazonía, la Corporación Vínculos, la organización Defensa de Niñas y Niños internacional –DNI–, Colombia, la Fundación Creciendo Unidos y el Servicio Jesuita a Refugiados Colombia -SJR-, algunas de las que contribuyen en el proceso de salida de menores de las FARC, **afirman que si se activan pronto estos mecanismos se podría fortalecer el proceso** y una salida negociada por la construcción de la paz.

En ese sentido las organizaciones presentaron una serie de lecciones que ayudaron a concretar la salida de menores durante el proceso de paz entre las FARC-EP y el gobierno Nacional, y que **podrían tenerse en consideración entre los equipos de diálogo en Quito**. (Le puede interesar:["Modificaciones a la JEP violarían acuerdos de paz"](https://archivo.contagioradio.com/jep-debe-garantizar-derecho-a-la-justicia-para-las-victimas-ivan-cepeda/))

El primero de ellos es dar prioridad a la protección de los niños, niñas y jóvenes en la agenda de conversaciones; el segundo es que se facilite la discusión entre las partes para llegar a un entendimiento común en que los **menores sean considerados como víctimas del conflicto armado. **(Le puede interesar: ["Protocolo de reincorporación para menores de las FARC fue improvisado"](https://archivo.contagioradio.com/el-protocolo-de-reincorporacion-para-menores-que-salieron-de-farc-ep-fue-improvisado/))

En tercer lugar, manifiestan que se debe dar una integración de la familia, comunidad y la sociedad civil en el proceso de reintegración de los menores, como parte de la reconstrucción del tejido social y finalmente **preparar un escenario para que la reintegración de los menores sea una oportunidad que aporte a sus entornos**.

### **Las acciones del Estado para el proceso de reincorporación de menores de edad** 

En la misiva también manifiestan que es de gran importancia que el gobierno Nacional se prepare para responder y dar garantías en el proceso de reincorporación de los menores de edad a la sociedad civil, por tal razón exponen que debe haber una atención diferenciada que reconozca la particularidad de las víctimas.

Además, señalaron que debe existir un reconocimiento del principio de corresponsabilidad de los grupos armados y el Estado en el reclutamiento de menores de edad y que debe haber una reparación integral del daño causado a los niños, niñas y jóvenes.

###### Reciba toda la información de Contagio Radio en [[su correo]
