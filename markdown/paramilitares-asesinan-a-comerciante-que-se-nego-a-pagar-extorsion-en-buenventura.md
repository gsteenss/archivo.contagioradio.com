Title: Paramilitares asesinan a comerciante que se negó a pagar extorsión en Buenaventura
Date: 2015-04-06 02:22
Author: CtgAdm
Category: DDHH, Nacional
Tags: Armada Nacional, asesinatos en buenaventura, buenaventura, Militarización, paramilitares
Slug: paramilitares-asesinan-a-comerciante-que-se-nego-a-pagar-extorsion-en-buenventura
Status: published

###### Foto: Comisión de Justicia y Paz 

Hacia las 9:30 pm del pasado 3 de Abril, paramilitares asesinaron a Wilder Ubeimar, propietario de un granero ubicado en el punto conocido como Punta Icaco, barrio La Playita. El hecho fue cometido por alias "El Mono Cocha" , "Taulan" y Wiston González reconocido extorsionista, según indicaron testigos.

Wilder Ubeimar  es uno de los pocos comerciantes que se negó a pagar extorsiones a los paramilitares asentados en el barrio Alfonso López. Según información, el comerciante cerraría su negocio el domingo 5 de abril debido a la quiebra en la que se encontraba.

Por otra parte la Comisión de Justicia y Paz denuncia que: *“en el momento en que los paramilitares asesinaron al vendedor se encontraban unidades de la Infantería de Marina a menos de 80 metros del lugar.  Los ejecutores pasaron al lado de estos e ingresaron al Espacio Humanitario pasando por enfrente de policiales que evidentemente no les observaron al ejecutar la acción*”

Según la organización de DDHH, es preocupante que los mismos paramilitares han amenazado a habitantes del Espacio Humanitario Puente Nayero y a reconocidos líderes de las comunidades en barrios vecinos del sitio humanitario.

“*La semana pasada Orlando Castillo y dos líderes más fueron amenazados de muerte por lps paramilitares debido a la promoción de propuestas de protección a la vida y al territorio*.” Agrega la denuncia de la Comisión de Justicia y Paz.
