Title: "Ríosucio no contaba con un departamento de bomberos, ni con acueducto"
Date: 2020-11-30 14:10
Author: AdminContagio
Category: Actualidad, Comunidad
Tags: choco, Crisis social, Incendio, Riosucio
Slug: riosucio-no-contaba-con-un-departamento-de-bomberos-ni-mucho-con-acueducto
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/11/Captura-de-Pantalla-2020-11-30-a-las-4.08.22-p.-m..png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

Luego de que este sábado 28 de septiembre en horas de la madrugada se registrara un incendio en el municipio de Riosucio, departamento de Chocó, autoridades **reportan la muerte de una mujer y una menor de edad, 40 viviendas destruídas, y cerca de 300 personas afectadas.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Al mismo tiempo líderes y lideresas de esta región se han pronunciado indicando que **esta emergencia pudo ser contenida con una atención estatal oportuna**, destacando que así como muchos municipio de Chocó, Ríosucio es el reflejo del abandono estatal.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/WilliamYeffer/status/1332906765344759809","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/WilliamYeffer/status/1332906765344759809

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

Según lo señala en su cuenta de twitter **William Yeffer Vivas, personero municipal de Medellín**, el incendio se propagó de manera voraz no solo porqué el *"90% de las viviendas son de madera"*, sino porque Ríosucio o algún municipio cercano no contaban con un departamento de bomberos que pudiera atender la emergencia, y **tuvieron que esperar que este llegaran del departamento vecino, Antioquia.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Otra de las razones que se sumaron es la falta de un sistema de Acueducto, en el municipio, que impidió además que las comunidades tuvieran elementos para disminuir la fuerza de las llamas, sumado al mal manejo que hay de las fuentes hídricas que abastecen Ríosucio. ([Desplazamiento y acaparamiento: el precio del aguacate en Caldas](https://archivo.contagioradio.com/__trashed-5/))

<!-- /wp:paragraph -->

<!-- wp:heading -->

**"Lo que ocurrió en Ríosucio es solo el reflejo del olvido estatal en muchos municipios de Chocó"**
----------------------------------------------------------------------------------------------------

<!-- /wp:heading -->

<!-- wp:paragraph -->

**Yolanda Perea, lideresa social de Chocó** y defensora de los derechos de las víctimas de violencia sexual en el pacífico, señaló que este hecho no solo le afecta porqué en Ríosucio viven familiares y personas cercanas a ella, sino porqué se suma a **los hechos que se pudieron evitar si el *"Gobierno atendiera de verdad los problemas de fondo y no solo llegara a recoger escombros"*.**

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/yolaperea/status/1332952149572071426","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/yolaperea/status/1332952149572071426

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:quote -->

> *"Hasta cuando nuestro pueblo negros, campesinos e indigenas van a seguir llevando del bulto"*
>
> <cite>Yolanda Perea | Lideresa social de Chocó </cite>

<!-- /wp:quote -->

<!-- wp:paragraph -->

Perea señaló que hoy el departamento del Chocó enfrenta no solo una fuerte ola invernal que deja ceca de 35.000 damnificados y un incendio que destruyó los hogares de 50 familias, sino un histórico abandono, ***"si no es las destrucción de humildes viviendas construidas con esfuerzo y sin ningún tipo de ayuda del Gobierno, es la violencia de los grupos armados la que destruye a mi pueblo"***

<!-- /wp:paragraph -->

<!-- wp:quote -->

> *"La solución de todos los problemas que hay en Chocó no se resuelve poniendo la lupa en la capital del departamento, ésta es solo la punta del iceberg"*
>
> <cite>Yolanda Perea | Lideresa social de Chocó</cite>

<!-- /wp:quote -->

<!-- wp:paragraph -->

**Riosucio es un municipio con más de 50.000 habitantes que viven principalmente de la pesca y la tala de árboles**; según Perea conviven sin alcantarillado, sin acueducto, y con su principal afluente de agua, el [Río Atrato](https://www.wwf.org.co/?uNewsID=334373) intervenido por las acciones extractivistas.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

A esto se suma la falta empleabilidad, *"pese a los esfuerzos de las familias especialmente de las mujeres que habitan en este territorio **las posibilidades de emprender y trabajar son difíciles, aquí tienen que pensar que si hay para el desayuno no hay para las otras comidas".***

<!-- /wp:paragraph -->

<!-- wp:quote -->

> *"Sufrimos por el conflicto y la ola invernal que nos afecta cada vez que se crece el Río Atrato, la economía no crece ni avanza y encima se presentan este tipo de incendios"*
>
> <cite>Yolanda Perea | Lideresa social de Chocó</cite>

<!-- /wp:quote -->

<!-- wp:heading {"level":3} -->

### Se deben buscar de fondo soluciones a la crisis en Chocó

<!-- /wp:heading -->

<!-- wp:paragraph -->

La lideresa también destacó que ***"las problemáticas que viven las comunidad del Chocó no se dan producto de la pandemia, ni mucho menos de la ola invernal, es un problema histórico sin solución de fondo"***. ([66 empresas, entre ellas 4 bancos, tendrán responderle a reclamantes de tierras](https://archivo.contagioradio.com/66-empresas-entre-ellas-4-bancos-tendran-responderle-a-reclamantes-de-tierras/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Y agregó que a pesar de que al territorio ya llegaron diferentes organismos del Estado para atender la emergencia, aún persiste la incertidumbre de lo que va a pasar con las casas destruidas, *"si es que construyen las casas hay que pensar cómo va a seguir la vida de los ríosuceños, cuando van a seguir sin acueducto, sin alcantarillado, y sin un hospital"*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Finalmente enfatizó en que ellos como campesinado no están a la espera de que el Gobierno le solucione todo, ***"nosotros estamos pidiendo acciones tan simples como que se destape el río para que la gente que cultiva pueda sacarlo al pueblo**, o quienes se encuentran hacinados por el tapón del río puedan salir y no se reporte una nueva catástrofe"*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En las ultimas horas Yolanda Perea ha dispuesto diferentes canales de apoyo, que permitan reconstruir las casas afectadas, y recuperar un albergue que cobijaba a más de 25 familias de víctimas de la violencia en Chocó.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->
