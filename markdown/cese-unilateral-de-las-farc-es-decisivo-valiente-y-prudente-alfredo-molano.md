Title: Cese unilateral de las FARC es “decisivo, valiente y prudente” Alfredo Molano
Date: 2015-07-08 13:26
Category: Nacional, Paz
Tags: Alfredo Molano, Cese Bilateral de Fuego, Cese unilateral de fuego, Entrevista Humberto de la Calle, Estado Mayor Conjunto de las FARC, Juan Manuel Santos, Proceso de conversaciones de paz de la Habana
Slug: cese-unilateral-de-las-farc-es-decisivo-valiente-y-prudente-alfredo-molano
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/07/cese-unilaterla-farc-contagioradio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: josecardenas.com 

<iframe src="http://www.ivoox.com/player_ek_4739001_2_1.html?data=lZygm5WUdY6ZmKiakpWJd6KkkZKSmaiRdI6ZmKiakpKJe6ShkZKSmaiRh8bnxpDi0M7QpdXZ08bZjcnJb83V1JCzo7enb8bnjIqylIqcdIatpMnSxc7XrdfjhpewjdvFsMrZz9nSjd6PcYarpJKw0dPYpcjd0JC%2Fw8nNs4yhhpywj5k%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Alfredo Molano] 

###### [8 Jul 2015] 

Tras el anuncio del **Estado Mayor Conjunto de las FARC**, el sociólogo **Alfredo Molano** afirma que esta noticia es un motivo de celebración para la gente que lucha por la democracia y para aquellos que necesitan la paz. Además califica la decisión de esa guerrilla como “***un paso decisivo, valiente y prudente***”, pero lamentó que el gobierno no dé el mismo paso, porque en unos días se pueden volver a presentar nuevas agresiones reversen esa decisión.

Sin embargo, la decisión de las FARC, no se da solamente en atención a las manifestaciones de los países garantes y de líderes religiosos, así como de organizaciones sociales afirma Molano, quien reitera que el gobierno debería también atender ese llamado al Cese al fuego bilateral, que es un llamado a las dos partes.

Molano agrega que uno de los aspectos más importantes en este anuncio de Cese Unilateral, es que en el mes que duraría el cese se ahorrarían vidas de colombianos, *“se ahorrarían unas 100 o 200 vidas*”, eso es lo que permiten mostrar las treguas unilaterales, además se puede ir aclimatando un acuerdo de cese bilateral.

Respecto de las afirmaciones recientes de Humberto de la Calle, Alfredo Molano asegura que es muy poco viable la propuesta de zonas de concentración puesto que “*las FARC no van a entregar su principal arma que es la movilidad*”, **sin embargo es muy importante que se continúe avanzando en el acuerdo en torno a la justicia. Seguramente ese acuerdo está muy avanzado y lo que se necesita es un momento de calma para anunciarlo, afirma Molano.**
