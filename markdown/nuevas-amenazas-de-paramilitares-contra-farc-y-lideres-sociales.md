Title: Nuevas amenazas de paramilitares contra FARC y líderes sociales
Date: 2018-01-16 13:02
Category: DDHH, Nacional
Tags: AGC, amenazas a líderes sociales, amenazas al movimiento social, Movimiento social, paramilitares
Slug: nuevas-amenazas-de-paramilitares-contra-farc-y-lideres-sociales
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/01/WhatsApp-Image-2018-01-09-at-15.40.18.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: enteratecali] 

###### [16 Ene 2018] 

Un panfleto con la identificación del bloque suroriental del Pacífico de las autodenominadas Autodefensas Gaitanistas de Colombia amenazó con "volar" la sede del partido FARC en Cali y también con asesinar a líderes del movimiento social en esas regiones. En el panfleto, **que fue desmentido en la cuenta de twitter de las AGC**, los paramilitares indican que dan por terminada la tregua de fin de año por lo que van a reactivar las actividades criminales.

La amenaza está también está dirigida a movimientos como la Central Unitaria de Trabajadores del Valle y Cauca, el **Movimiento de Víctimas de Crímenes de Estado (Movice),** el Partido Comunista, Marcha Patriótica y Congreso de los Pueblos entre otros. Allí también hace referencia a que atacarán las sedes del partido político FARC que se encuentran en Cauca, Valle y Nariño.

### **Amenaza se da en el marco del reiterado ataque al movimiento social** 

La Fundación Comité de Solidaridad con los Presos Políticos manifestó que el movimiento social y los defensores de derechos humanos han venido siendo víctimas de un **reiterado ataque de grupos criminales**. Indican que estos hechos se han presentado en las regiones donde “le apuestan a la paz con justicia social”. (Le puede interesar: ["Paramilitares atentaron contra integrantes de la comunidad de paz de San José de Apartadó"](https://archivo.contagioradio.com/atentado-comunidad-san-jose-apartado/))

Recordaron además que “en 2017 el programa Somos Defensores reportó reporto que más de **193 defensores de derechos humanos**, líderes sociales y sindicales, fueron víctimas de algún tipo de agresión que pone en riesgo sus vidas”. De esas 193 agresiones, 136 han sido amenazas como la que recibieron en días pasados.

### **Paramilitarismo ha coptado distintos sectores de las grandes ciudades** 

Teniendo en cuenta la reestructuración de los grupos paramilitares, quienes han desarrollado una estrategia en la que utilizan diferentes nombres como las **Águilas Negras, urabeños, nuevos urabeños** entre otros, la Fundación indicó que esto “les ha permitido copar distintos sectores de las grandes ciudades como en el Valle del Cauca”.

Allí, han denunciado que los paramilitares se han disputado territorios en barrios y plazas de mercado teniendo como objetivo **controlar el tráfico de drogas y la extorsión**. Para estas organizaciones la situación en ciudades como Cali es preocupante por lo que han alertado que “Santiago de Cali, como muchas otras ciudades del país, es controlada por el paramilitarismo”. (Le puede interesar: ["La amenazas de paramilitares son reales y comprobables: Unión Patriótica"](https://archivo.contagioradio.com/las-amenazas-paramilitares-son-reales-y-comprobables-union-patriotica/))

Finalmente, con los hechos registrados, las organizaciones y defensores de derechos humanos le han exigido al Gobierno Nacional que realice las acciones necesarias para proteger la vida de quienes están siendo amenazados. También han pedido que el Estado **reconozca la continuidad del fenómeno** del paramilitarismo para así hacerle frente al mismo.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
