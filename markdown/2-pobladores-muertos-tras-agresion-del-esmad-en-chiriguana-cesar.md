Title: 2 pobladores muertos tras agresión del ESMAD en Chiriguaná, Cesar
Date: 2016-07-12 15:07
Category: DDHH, Nacional
Tags: Abuso de poder ESMAD, cesar, Chiriguaná, ESMAD, Hospital de Chiriguaná
Slug: 2-pobladores-muertos-tras-agresion-del-esmad-en-chiriguana-cesar
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/07/Chiriguaná.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: ] 

###### [12 Julio 2016 ]

[El pasado 30 de junio fue cerrado el segundo nivel del Hospital de Chiriguaná, Cesar y este lunes la población decidió salir a las calles para exigir que no se cierre la institución, que brinda atención a los habitantes de siete municipios más, cuando **efectivos del ESMAD y de la Policía Nacional arremetieron contra los pobladores**, dispararon contra tres de ellos, hirieron con cuchillos a otros tantos y sobre una persona pasaron una de las motos en las que se movilizaban.  ]

Según Sidian Martínez, poblador de la zona, los efectivos agredieron a quienes se estaban movilizando, dispararon contra el docente [Naimen Lara y pasaron una de las motos sobre un mototaxista, las dos personas murieron. El poblador agrega que mientras intentaban escapar, los **efectivos hirieron con cuchillos a varios compañeros, y a otros los golpearon con bolillos en la cabeza**.   ]

Martínez asevera que los problemas del Hospital se deben a las **prácticas de corrupción que han sido incluso evidenciadas por la Contraloría**, y que han determinado además de su cierre, el no pago de salarios a varios de sus empleados. Los habitantes del municipio han decidido continuar movilizándose para exigir que se atienda la crisis de la institución y que no se sigan violando los derechos de las comunidades.

<iframe src="https://co.ivoox.com/es/player_ej_12203045_2_1.html?data=kpefkpiUeJahhpywj5abaZS1lJyah5yncZOhhpywj5WRaZi3jpWah5yncbTdxc7O0JCxpdPohqigh6aossbuhpewjdXTps3VxdTfjajMrdPdyNrO0Iqnd4a1kpKSmaiRh9Di1cbUy9SPlsLYytSYj4qbh46o&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en [[Otra Mirada](http://bit.ly/1ICYhVU)] por Contagio Radio ] 
