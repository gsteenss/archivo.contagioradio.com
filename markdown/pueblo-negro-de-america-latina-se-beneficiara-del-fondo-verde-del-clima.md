Title: Fondo Verde del Clima reconoce al pueblo negro de América Latina  y el Caribe
Date: 2018-03-14 13:23
Category: Ambiente, Nacional
Tags: afrodescendientes, América Latina, derechos del pueblo negro, fondo verde del clima, pueblo negro
Slug: pueblo-negro-de-america-latina-se-beneficiara-del-fondo-verde-del-clima
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/04/Comunidades-negras-norte-del-Cauca.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Las 2 Orillas] 

###### [14 Mar 2018] 

Por primera vez ha sido integrada, de forma expresa, la población negra de América Latina y el Caribe, es decir que será reconocida por la Política de Pueblos Indígenas del Fondo Verde del Clima. Ahora, los derechos del pueblo negro deben ser garantizados, cuando por ejemplo, las empresas quieran realizar proyectos que afecten sus territorios y al ambiente.

Así lo aseguró la organización Ambiente y Sociedad desde donde se enfatizó que esto se logró gracias al trabajo del **Proceso de Comunidades Negras en Colombia** y demás organizaciones defensoras de los derechos humanos y del ambiente. Esto constituye “un primer paso para continuar trabajando por el respeto de los derechos humanos en el marco de desarrollo de proyectos a ser financiados por el Fondo Verde del Clima”.

### **¿Qué es el Fondo Verde del Clima?** 

Este fondo fue constituido como una **herramienta financiera** de la Convención Marco de las Naciones Unidas para el Cambio Climático. Busca “canalizar el dinero que los países aportan para la lucha contra los efectos y la mitigación del cambio climático”.

Desde allí se ha reconocido la necesidad de involucrar a las diferentes comunidades para promover su participación y respetar sus derechos. Por esto, **hizo tres llamados en 2017** para recibir insumos dentro del proyecto de Política de Pueblos Indígenas a lo que el Proceso de Comunidades Negras y la Asociación Ambiente y Sociedad enviaron su solicitud para que se reconociera “dentro del ámbito de aplicación a la población negra de América Latina y el Caribe”.

### **Comunidades negras serán reconocidas por los fondos que se movilizan hacia la lucha contra la mitigación del cambio climático** 

De acuerdo con Milena Bernal, coordinadora del programa de Justicia Climática de Ambiente y Sociedad, esta inclusión es importante para el pueblo negro en la medida en que **se reconoce la participación** de estas comunidades que han sido excluidas tradicionalmente. Afirmó que, “por primera vez se hace un reconocimiento expreso y se reafirma al pueblo negro de América Latina como beneficiario de las garantías que recoge la Política de Pueblos Indígenas del Foro Verde del Clima”.

Esto quiere decir que se expanden las garantías y el acceso a la información a esta población en lo que tiene que ver con el cambio climático, “específicamente en los fondos que se movilizan para la lucha contra este fenómeno”. Este reconocimiento se hizo teniendo en cuenta que el pueblo negro del continente **se reconoce como una población especial** que debe contar con unas garantías determinadas para el desarrollo de proyectos que puedan afectar su territorio.

### **Proyectos empresariales deberán respetar los derechos del pueblo negro** 

Adicionalmente, la abogada explicó que “cuando se quiera desarrollar un proyecto que afecte a las comunidades negras de América Latina y el Caribe, tendrá ciertas garantías en, materia de derechos que **deben ser respetados** por todas las entidades que ejecutan los proyectos y no solamente por el Fondo Verde del Clima que proporciona los recursos”.

Ejemplificó diciendo que “si en Colombia se quisiera desarrollar algún proyecto en territorio donde habita el pueblo negro a través del Programa de las Naciones Unidas para el Desarrollo, las entidades tienen que garantizar que **todos los derechos y principios** incluidos dentro de la Política de Pueblos Indígenas sean respetados”. (Le puede interesar:["US \$100 mil millones del Fondo Verde del Clima en manos de entidades que violan los DDHH"](https://archivo.contagioradio.com/us100-mil-millones-del-fvc-en-manos-de-multinacionales-que-violan-ddhh/))

Finalmente, esto significa que las acciones de **empresas privadas** deberán respetar unos mínimos como, por ejemplo, el desarrollo las consultas previas. Dijo que el Fondo está desarrollando un mecanismo de salvaguardas que debe integrar la Política de Pueblos Indígenas que ahora integró al pueblo negro.

<iframe id="audio_24464408" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_24464408_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
