Title: Superintendente de Salud desacata fallo para evitar liquidación de EPS indígena
Date: 2017-07-04 14:27
Category: DDHH, Nacional
Tags: indígenas, ONIC
Slug: superintendente-de-salud-eps-indigena-manexha
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/07/IMG_1663.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: ONIC] 

###### [04 Jul. 2017] 

A través de un comunicado dado a conocer por la ONIC, las autoridades indígenas Zenú y Embera han denunciado que **el Superintendente Nacional de Salud, Norman Julio Muñoz ha desacatado el fallo emitido por el Tribunal Administrativo de Córdoba** en el que se ordena no liquidar la EPS Manexha, de propiedad de las comunidades indígenas. Le puede interesar: [Desmonte de EPS indígena genera protestas del pueblo Zenú en Córdoba y Sucre](https://archivo.contagioradio.com/zenu-indigenas-salud/)

Según las comunidades esta actitud **“ha conllevado a la muerte de varios hermanos Zenues y Emberas por falta de atención en las EPS** Occidentales a las que fueron abruptamente trasladados a partir de la ilegal toma de posesión”.

Así mismo, dicen las comunidades indígenas, se acordó una reunión para el 30 de junio a las 2 pm en la que se establecería la ruta para la devolución de los usuarios, así como la financiación de la entidad, sin embargo no asistieron los representantes de la Superintendencia Nacional de Salud, la Defensoría del Pueblo ni la Procuraduría General de la Nación.

Dada esa situación **“las autoridades indígenas, rechazan la actitud de la Superintendencia Nacional de Salud de no asistir a las mesas de trabajo** convocadas, e invita al Ministerio Público a ser garante del cumplimiento del fallo judicial y de las mesas de trabajo instaladas”.

En ese sentido, las comunidades **han hecho un llamado al Superintendente Nacional de Salud para que acaten el fallo judicial**, así como a la Presidencia de la Republica, y el Ministerio de Salud y Protección Social para que sean parte de las mesas de trabajo que permitan dar solución a este tema.

Por último, los pueblos indígenas Zenú y Embera, han asegurado que se mantendrán en la ciudad de Bogotá hasta que la situación tenga una solución y “se cumpla sin más dilaciones el fallo judicial proferido”.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU).
