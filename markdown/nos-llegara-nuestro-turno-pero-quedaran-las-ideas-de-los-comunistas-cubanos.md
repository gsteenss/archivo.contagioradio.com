Title: "Nos llegará nuestro turno, pero quedarán las ideas de los comunistas cubanos"
Date: 2016-04-20 17:44
Category: El mundo, Política
Tags: Fidel Castro discurso PCC, reaparición Fidel Castro
Slug: nos-llegara-nuestro-turno-pero-quedaran-las-ideas-de-los-comunistas-cubanos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/04/fidel-castro.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: washingtontimes] 

###### [20 Abr 2016] 

Por segunda ocasión en este mes, reapareció públicamente el ex mandatario Fidel Castro, esta vez durante la clausura del VII Congreso del Partido Comunista Cubano, con un sentido y pausado discurso en el que puso de manifiesto la prevalencia en el tiempo que tendrá el comunismo cubano aun después de su partida.

El líder de la revolución cubana, próximo a cumplir 90 años de edad y retirado del poder desde 2006 por cuestiones de salud, manifestó que "**pronto seré como todos los demás, a todos nos llegará nuestro turno, pero quedarán las ideas de los comunistas cubanos**" mientras los cerca de mil asistentes le aplaudían tras cada una de sus intervenciones.

Castro, añadió que el legado del comunismo permanecerá "**como prueba de que en este planeta, si se trabaja con fervor y dignidad, se pueden producir los bienes materiales y culturales que los seres humanos necesitan**, y debemos luchar sin tregua para obtenerlos", comunicando a los hermanos de Latinoamerica y el mundo que "**el pueblo cubano vencerá**".

El ex mandatario se refirió a la importancia que tienen cada uno de los representantes por ser "escogidos por el propio pueblo revolucionario que en ellos delegó su autoridad" lo que "**significa para todos el rol más grande que han recibido en la vida**" lo que "se suma el privilegio de ser revolucionario que es fruto de nuestra propia conciencia". añadió.

Una parte de su intervención la dedico a narrar el origen de su pensamiento y convicciones, descubriendo el socialismo a los 20 años de edad mientras estudiaba leyes y ciencias políticas, de manera autodidacta y sin una guía en el estudio del marxismo-leninismo, y como se convirtió en comunista, una palabra que "expresa el concepto más distorsionado y calumniado de la historia por parte de aquellos que tuvieron el privilegio de explotar a los pobres" aseguró Castro.

Durante el tiempo de su formación sin "preceptor" Fidel tenía una "confianza total" en la Unión Soviética, por lo que afirmó que "no deberán transcurrir otros 70 años para que ocurra un acontecimiento como la revolución rusa, para que la humanidad tenga otro ejemplo de una grandiosa revolución social que significó un enorme paso en la lucha contra el colonialismo y su inseparable compañero, el imperialismo".

"El peligro mayor que hoy se cierne sobre la tierra deriva del poder destructivo del armamento moderno que podría socavar la paz del planeta y hacer imposible la vida humana sobre la superficie terrestre" advirtió Castro a la vez que se refirió al "gran problema" que tendrán las futuras generaciones para alimentarse contra los límites de recursos naturales que necesitan"; en lugares "sin tecnología a su alcance, ni lluvias, ni embalses, ni depósitos subterráneos".

Al cierre Fidel, felicitó a todos los asistentes en particular a su hermano Raúl por el "magnífico esfuerzo" realizado y finalizó diciendo: "**emprenderemos la marcha y perfeccionaremos lo que debemos perfeccionar con lealtad meridiana y la fuerza única, como Martí, Macedo y Gómez en marchas indetenibles**".

<iframe src="http://co.ivoox.com/es/player_ej_11243264_2_1.html?data=kpaflpiWepWhhpywj5WUaZS1kZWah5yncZGhhpywj5WRaZi3jpWah5yncafdxcrZjajFt9Xm0JCajarctNPZ1M7Rx9PYqYzYxpCw18fFcYarpJKw0dPYpcjd0JC%2Fw8nNs4yhhpywj5k%3D&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>
