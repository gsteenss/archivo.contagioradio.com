Title: Se teje impunidad por posibilidad que Justicia Penal Militar asuma caso de Nicolás Neira
Date: 2017-08-18 17:51
Category: DDHH, Nacional
Tags: ESMAD, nicolas neira, yuri neira
Slug: justicia-penal-militar-podria-asumir-el-caso-de-nicolas-neira
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/08/nicolc3a1s-neira-matera.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Colombia en Contexto] 

###### [10 Ago. 2017] 

Durante la audiencia de imputación de cargos en contra del agente del ESMAD Néstor Rodríguez Rua, acusado del homicidio de Nicolás Neira, el juez resolvió enviar el caso al **Consejo Superior de la Judicatura para que estudie la posibilidad de llevarlo ante la Justicia Penal Militar.**

La Fiscalía señala a Rúa como autor del homicidio de Neira luego de escuchar el testimonio del entonces capitán Julio César Torrijos Devia, quien se declaró culpable por encubrimiento a su subordinado, dando cumplimiento a la orden del Mayor del Escuadròn Móvil Antidisturbios ESMAD, Mauricio Infante Pinzón. Le puede interesar: [Caso de Nicolás Neira abre la puerta para esclarecer crímenes del ESMAD](https://archivo.contagioradio.com/caso-de-nicolas-neira-abre-la-puerta-para-esclarecer-crimenes-del-esmad/)

Si bien la Fiscalía manifestó durante la audiencia que el Consejo Superior de la Judicatura ya había dicho que ese caso era competencia de la justicia ordinaria, la decisión fue tomada antes de conocer la naturaleza del autor del homicidio, por lo que **el juez prefirió solicitar otra revisión y declararse impedido en el caso.**

Para Pedro Mahecha, abogado de la familia de Nicolás Neira, esta decisión del juez puede considerarse como gravísima por lo que **solicitarán a la Procuraduría que investigue la conducta del juez al desconocer la decisión previa del Consejo Superior de la Judicatura,** tomada hace 9 años.

“Además desconoce (el juez) una línea jurisprudencial de la Corte Constitucional donde claramente se define que la violación de los derechos humanos no es competencia de la Jurisdicción Penal Militar”, dijo Mahecha. Le puede interesar: [El prontuario del ESMAD](https://archivo.contagioradio.com/el-prontuario-del-esmad/).

######  Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU).

<div class="osd-sms-wrapper">

</div>
