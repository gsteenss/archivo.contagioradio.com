Title: Así fue la conmemoración de los 53 años de las FARC-EP en Bogotá
Date: 2017-05-28 15:30
Category: Nacional, Paz
Tags: Aniversario FARC-EP, proceso de paz
Slug: asi-fue-la-conmemoracion-de-los-53-anos-de-las-farc-ep-en-bogota
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/05/farc-cumple.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [28 May 2017] 

Este 27 y 28 de mayo la guerrilla de las FARC-EP conmemoró su 53 aniversario de nacimiento. En Bogotá se reunió parte del secretariado con algunos guerrilleros y guerrilleras, estudiantes, doncentes, artistas y familias, que tuvieron la **oportunidad de reencontrarse y re afirmar la construcción de un país diferente, ahora desde la política**.

Iván Márquez, manifestó que los logros que han alcanzado las FARC-EP en la búsqueda de la paz, son la visión que tuvieron los fundadores de esta guerrilla “pensamos cuánto hubiesen deseado nuestro padres fundadores, manuel Marulanda, Jacobo Arenas, y otros compañeros protagonizar la realización de este momento histórico, ellos y todos aquellos quienes ofrendaros su vida por la causa de los pobres, **seguirán vivo en los sentimientos de concordia de las generaciones venideras**”.

De igual forma, Márquez agradeció la colaboración de Hugo Chávez y Raúl Castro en los avances del proceso de paz y reconoció el acompañamiento de Noruega, Ecuador y Chile en cada uno de los momentos difíciles que ha tenido el camino por alcanzar la paz. Le puede interesar: ["FARC-EP celebra su aniversario en armas"](https://archivo.contagioradio.com/farc-ep-celebra-su-ultimo-aniversario-en-armas/)

La cuota musical estuvo a cargo de Martín Batalla y Black Esteban, quienes al unísono entonaron ante los asistentes el tema **“Reconciliación” la canción de cumpleaños número 53**. Cinco tortas y todas las y los guerrilleros que se encontraban en el recito se acercaron para tomarse la foto que celebra el fin de un camino y el inicio de otro en busca de justicia y paz para Colombia. Le puede interesar:["Esperamos sus hechos, presidente, haga honor a su palabra: Timochenco"](https://archivo.contagioradio.com/esperamos-sus-hechos-timoleon-jiimenez-a-santos/)

\  
 

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU).
