Title: Fiscalía demuestra su falta de compromiso con los desaparecidos del Palacio de Justicia
Date: 2019-08-28 14:37
Author: CtgAdm
Category: DDHH, Memoria
Tags: cuerpos, desaparecidos, Fiscal, Palacio de Justicia
Slug: fiscalia-desaparecidos-palacio-de-justicia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/08/palacio-de-justica-.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio  
] 

El pasado martes, la Fiscalía General de la Nación, en cabeza de dos de sus investigadores encargados del caso de la toma y retoma del Palacio de Justicia, afirmaron que allí no se habían presentado desapariciones forzadas, contradiciendo incluso sentencias judiciales de carácter internacional. Para familiares de víctimas, así como abogados que han llevado el caso, **estas declaraciones muestran una vez más la falta de compromiso del ente investigador con el esclarecimiento de los hechos ocurridos en noviembre de 1985.**

### **"No fueron casos de desapariciones forzadas sino que fueron casos de malas identificaciones o cuerpos mezclados"** 

En una entrevista con medios de comunicación corporativos, el fiscal investigador del Palacio de Justicia, Jorge Ricardo Sarmiento afirmó que "los hallazgos que Medicina Legal junto a la Fiscalía han alcanzado en estos últimos cinco años nos permiten afirmar que **no fueron casos de desapariciones forzadas sino que fueron casos  de malas identificaciones o cuerpos mezclados"**. (Le puede interesar: ["¿Quién responde por las entregas equivocadas del holocausto del P. de Justicia?"](https://archivo.contagioradio.com/quien-responde-por-las-entregas-equivocadas-del-holocausto-palacio-de-justicia/))

Según explicó Jairo Humberto Oróstegui, también fiscal investigador del caso, debido a que muchos de los cuerpos que fueron sacados del Palacio se enviaron a la fosa común del cementerio del sur, y en ese momento no había tecnología suficiente para identificar correctamente el ADN de las personas, algunos restos óseos fueron entregados de forma errónea. Aunque este miércoles la Fiscalía brindó declaraciones retractándose de esta hipótesis, familiares y sus abogados cuestionaron el pronunciamiento que intentaría cambiar la versión oficial de los hechos.

### **"Es una manera atroz de negar la desaparición forzada en Colombia"** 

Desde el encuentro organizado por la Comisión para el Esclarecimiento de la Verdad (CEV) y la Unidad de Búsqueda de Personas dadas por Desaparecidas (UBPD) en Pasto, Nariño, llamado "Encuentro por la Verdad, \#ReconocemosSuBúsqueda", **Pilar Navarrete, esposa de Héctor Jaime Beltrán (desaparecido del Palacio)**, afirmó que era "horrible y desastroso que después de dos sentencias, una internacional y otra nacional, después de 34 años, después de vídeos, fotos y algunas confesiones de personas diciendo lo que ocurrió, estén negando que existen los desaparecidos del Palacio de Justicia".

Navarrete afirmó que esta es una manera de decirle a las personas que justamente está homenajeando la Unidad y la Comisión, **"que dejen de buscar, es una manera atroz de negar la desaparición forzada en Colombia";** añadió que no ha pasado un año (de los casi 34 desde que ocurrió la toma y retoma del Palacio) sin que el Estado los revictimice, pero subrayó que pese a ello, "para las personas es claro que los desaparecidos del Palacio existen y que la desaparición forzada en el país existe".

(Le puede interesar: ["Segundo encuentro por la Verdad: un reconocimiento a familias de personas desaparecidas"](https://archivo.contagioradio.com/encuentro-verdad-desaparecidas/))

### **"No existen argumentos jurídicos ni técnicos para sostener dichas afirmaciones"** 

La **abogada experta en derecho internacional de la Comisión Intereclesial de Justicia y Paz, Diana Marcela Muriel** aseguró que "realmente no existen argumentos jurídicos ni técnicos para sostener dichas afirmaciones", porque los informes de entregas de cuerpos (que es una labor de carácter humanitario y no de esclarecimiento), dan cuenta de los estudios de ADN que se han practicado a los restos óseos con los que se ha establecido el parentesco de las familias y que sufrieron procesos de carbonización por las llamas, "pero no son rigurosos en dar cuenta de la causa y la forma de muerte de las víctimas, de ahí que no se pueda concluir técnica ni científicamente qué fue lo que pasó en el Palacio".

En ese sentido, la experta sostuvo que **las entregas de cuerpos no deben servir como pruebas, "porque no tienen condiciones técnicas para serlo"** ni para afirmar que estas personas no fueron desaparecidas. De hecho, la abogada recordó que ni siquiera se puede hablar de una entrega total de cuerpos, porque algunas familias solo han recibido partes de cuerpos, como en el caso de la familia de Cristina Guarín que solo recibió siete vértebras de ella. (Le puede interesar: ["Ejército deberá responder por torturas a 11 personas durante retoma del P. de Justicia"](https://archivo.contagioradio.com/torturas_palacio_justicia_retoma/))

La abogada relató que luego de la llegada de **Jorge Hernán Díaz Soto,**  Fiscal Primero Delegado ante la Corte Suprema de Justicia para el caso del Palacio de Justicia hace tres años, **las investigaciones no han avanzado**. Según explicó Muriel, debido a que el Fiscal asumió un enfoque que no procederá a adelantar ninguna acción judicial tendiente a imputar responsabilidad penal hasta que no se realicen todas las exhumaciones de los cuerpos que fueron enterrados en el cementerio del sur.

Decisión que se mantiene, pese a que hay pruebas testimoniales, de video y audio que reposan en el expediente judicial y podrían ser utilizadas para establecer responsabilidades y avanzar en el esclarecimiento de la verdad. Para la abogada, "eso **pone en evidencia la falta de seriedad de la Fiscalía, y la falta de contundencia con la que avanzan las investigaciones**, ad portas de celebrarse una audiencia de seguimiento el próximo 6 de septiembre por parte de la Corte Interamericana de Derechos Humanos " (Corte IDH).

### **La declaración de los fiscales será mostrada a Corte IDH como una prueba más en el caso Palacio de Justicia  
** 

Pilar Navarrete manifestó la probabilidad de que estas declaraciones se dieran porque **el próximo 6 de septiembre la Corte IDH realizará seguimiento a la sentencia emitida sobre Palacio de Justicia**, y el Estado busca hacer caso omiso. Por su parte, Muriel opinó que si esa es la estrategia, la Fiscalía erró, pues la Corte ya sentenció que sí hubo una alteración de la escena del Palacio y que hubo desaparición; razón por la que, intentar legitimar una versión de la historia diferente no resulta estratégico en el marco de un caso que ya fue solucionado.

La abogada concluyó que pese a la retractación, **las declaraciones fueron un atentado contra la verdad, la memoria y la dignidad de las víctimas,** razón por la que serán presentadas en la audiencia ante la Corte IDH en septiembre como una prueba más, para que este organismo reitere la obligación del Estado de investigar, y reitere los hechos que ya fueron establecidos, para que sea a partir de esta realidad judicial que se genere esclarecimiento. (Le puede interesar: ["32 años después Fiscalía sigue negando a los desaparecidos del P. de Justicia"](https://archivo.contagioradio.com/32-anos-despues-fiscalia-sigue-negando-a-los-desaparecidos-del-palacio-de-justicia/))

### **La UBPD, una oportunidad para atender de forma adecuada la desaparición forzada** 

Desde Pasto, Luz Marina Monzón, directora de la UBPD afirmó que la desaparición forzada en el Palacio es algo que "objetivamente todas las personas saben que ocurrió", razón por la que resulta sorprendente que se diga algo contrario. En consecuencia, expresó que el Estado debe cumplir la sentencia proferida por la Corte IDH, que tiene entre sus órdenes, "buscar a las personas que siguen desaparecidas, identificar los cuerpos no identificados y devolverlos a sus familiares"; labor en la que la Unidad apoyará a las familias para que puedan encontrar a sus seres queridos.

Ante el reto que implica buscar un universo de víctimas de más de 120 mil personas en el marco del conflicto, Monzón aseveró que la unidad siente una inmensa y enorme responsabilidad para "hacer esta tarea con mucho cuidado y respetando las expectativas de los familiares para no causarles más daño", y agregó que con la entrada en funcionamiento del Sistema Integral de Verdad, Justicia, Reparación y No Repetición, y de la Unidad, el Estado tiene la oportunidad de "hacerse cargo de algo que no se ha hecho cargo de forma adecuada como es la búsqueda de los desaparecidos".

> [\#ReconocemosSuBúsqueda](https://twitter.com/hashtag/ReconocemosSuB%C3%BAsqueda?src=hash&ref_src=twsrc%5Etfw) “Nos sorprende lo que trascendió sobre la supuesta no existencia de desaparecidos en el [\#PalaciodeJusticia](https://twitter.com/hashtag/PalaciodeJusticia?src=hash&ref_src=twsrc%5Etfw). Nos comprometemos a esclarecer la verdad humana, ética e histórica de los desaparecidos en el palacio”: Francisco De Roux ► <https://t.co/5Kkc6q26Ea> [pic.twitter.com/nEdPWhjjqA](https://t.co/nEdPWhjjqA)
>
> — Comisión de la Verdad (@ComisionVerdadC) [August 28, 2019](https://twitter.com/ComisionVerdadC/status/1166762622773006338?ref_src=twsrc%5Etfw)

<p>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
</p>
### **Familiares del Palacio de Justicia recusaron a los fiscales investigadores del caso**

Tras las declaraciones entregadas por los fiscales investigadores del caso y Claudia Adriana García, directora del Instituto Nacional de Medicina Legal, familiares de Carlos Augusto Rodríguez Vera (desaparecido de forma forzada del Palacio), formularon una recusación contra el "Fiscal Primero Delegado ante la Corte Suprema de Justicia y su equipo de Fiscales Investigadores, así como en contra de la Directora del Instituto de Medicina Legal" para que sean apartados de sus cargos, por "abierta parcialidad en sus actos y manifiesta temeridad en sus comportamientos 'investigativos'".

[Recusación fiscales Palacio de Justicia](https://www.scribd.com/document/423568052/Recusacion-fiscales-Palacio-de-Justicia#from_embed "View Recusación fiscales Palacio de Justicia on Scribd") by [Contagioradio](https://www.scribd.com/user/276652802/Contagioradio#from_embed "View Contagioradio's profile on Scribd") on Scribd

<iframe id="doc_70574" class="scribd_iframe_embed" title="Recusación fiscales Palacio de Justicia" src="https://es.scribd.com/embeds/423568052/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-90r0IyM1YRN3WdK5RD4H&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="0.6069986541049798"></iframe>

**Síguenos en Facebook:**  
<iframe id="audio_40609581" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_40609581_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>  
<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
