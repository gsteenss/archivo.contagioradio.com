Title: Los delitos de fiscales de la costa van más allá de la corrupción
Date: 2017-07-31 14:21
Category: Judicial, Nacional
Tags: congreso de los pueblos, Fiscalía
Slug: los-delitos-de-fiscales-de-la-costa-van-mas-alla-de-la-corrupcion
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/05/Justicia.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo Particular] 

###### [31 Jul 2017] 

Aunque la Fiscalía General de la Nación habla de una red de corrupción en la que estarían implicados algunos fiscales de la costa norte, **las actucaciones del ente investigador podrían dar cuenta de actividades ilícitas, mas allá del simple hecho de recibir sobornos**. Así lo denuncian organizaciones de Derechos Humanos y sociales que han denunciado un montaje judicial desde la fiscalía para encarcelar líderes.

“**Desde un comienzo hemos advertido que la metodología para procesar a los líderes sociales era falsear pruebas, clonar testigos con el propósito de llevar errores a la administración de justicia**” afirmó Molano, uno de los abogados defensores de los líderes en este caso, y agregó que estos hechos revelan “prácticas de corrupción” que tendrían como finalidad extorsionar personas con el objetivo de que no fuesen procesados o que sus procesos fueran archivados.

Las organizaciones sociales están exigiendo a la justicia que se ordene la libertad inmediata de los líderes detenidos arbitrariamente por decisiones de María Bernarda Puentes, reconocida porque su acusación incluía el **delito de "organizar marchas**". Las organizaciones argumentan que una persona que ha trabajado con paramilitares no tendría autoridad para encarcelar líderes sociales.

Una de las detenciones ordenadas se registró el pasado 22 de marzo, de este año, por la Fiscal, quien solicitó medida de aseguramiento con componente de destierro para la líder social e integrante de la Comisión de Interlocución del Sur de Bolívar, de Congreso de los Pueblo y la Cumbre Agraria, Étnica y Popular, Milena Quiroz, **esto por considerarla un peligro para la sociedad por el trabajo político que realizaba y convocar a movilizaciones**. (Le puede interesar.["Milena Quiroz fue condenada al destierro: abogada"](https://archivo.contagioradio.com/milena-quiroz-fue-condenada-al-destierro-abogados-de-la-defensa/))

De igual forma, las organizaciones sociales han denunciado que el proceso contra Quiroz, se encuentra minado de irregularidades, para el abogado Molano, **“hay unas actuaciones consistentes en un concierto para delinquir entre la Fiscal Puentes** y los investigadores de la Policía” que participaron en el caso.

Para el caso concreto de Quiroz y los demás líderes detenidos por la Fiscal Puentes, este miércoles se realizará una audiencia de apelación en la que se espera se debatan las nulidades del proceso que impulso la Fiscal y para que un juez independiente revise las pruebas y decida sobre la revocatoria de medida de aseguramiento. **Previamente ya se había citado a Puentes a dos audiencias**, sin embargo, siempre se negó a participar, dilatando el proceso. (Le puede interesar: ["Fiscalía no tiene argumentos para mantener en prisión a líderes sociales del Sur de Bolívar"](https://archivo.contagioradio.com/fiscalia-no-tiene-argumentos-para-mantener-en-prision-a-lideres-sociales-del-sur-de-bolivar-defensa/))

**El paramilitarismo en el departamento de Bolívar**

Comunidades del sur de Bolívar han denunciado en diferentes situaciones antes instancias estatales **el accionar de grupos autodenominados como Autodefensas Gaitanistas de Colombia**, e incluso la existencia de dos campamentos uno a 20 minutos de la cabecera del municipio de Norsí, en el corregimiento de Mina Estrella y otro en Montecristo.

Para Molano, la investigación en contra de la Fiscal Puentes evidencia que “la Fiscalía está actuando en el sur de Bolívar como un aparato de contención que **tiene como fin generar escenario de miedo, en los pobladores y en el movimiento social** para impedir que sigan ejerciendo sus luchas políticas y sociales”.

Además, las organizaciones sociales denunciaron que hay **45 casos más de fiscales investigados por actos de corrupción que favorecen principalmente intereses de grupos paramilitares** de diversas regiones y en donde también estarían involucrados funcionarios como jueces, la Policía Judicial, miembros del INPEC y otras instituciones.

Las organizaciones sociales, están exigiendo la liberación inmediata de todos los líderes denetenidos bajo las ordenes de Puentes y esperan que se tomen medidas urgentes frente a las denuncias de estas relaciones entre la Juticia y el paramilitarismo. (Le puede interesar:["Capturas de líderes de Congreso de los Pueblos podrían declararse ilegales"](https://archivo.contagioradio.com/capturas-congreso-de-los-pueblos/))

[Pronunciamiento serlidernoesdelito 31072017](https://www.scribd.com/document/355178019/Pronunciamiento-serlidernoesdelito-31072017#from_embed "View Pronunciamiento serlidernoesdelito 31072017 on Scribd") by [Contagioradio](https://www.scribd.com/user/276652802/Contagioradio#from_embed "View Contagioradio's profile on Scribd") on Scribd

<iframe id="doc_478" class="scribd_iframe_embed" src="https://www.scribd.com/embeds/355178019/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-at6ejDhlJXesVDglJSaT&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="0.7080062794348508"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio]{.s1}](http://bit.ly/1ICYhVU)[.]{.s1}
