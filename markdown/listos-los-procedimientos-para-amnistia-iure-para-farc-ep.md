Title: Listos los procedimientos para amnistía iure para FARC-EP
Date: 2017-02-19 08:17
Category: Nacional, Paz
Tags: acuerdos de paz, Amnistia, FARC, paz
Slug: listos-los-procedimientos-para-amnistia-iure-para-farc-ep
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/02/Crisis-carcelaria-e1482755978940.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Achivo] 

###### [19 Feb. 2017] 

Este 17 de febrero se dio a conocer el **decreto que regula y además establece los procedimientos para la efectiva implementación de la Ley de amnistía**, indulto y tratamientos penales especiales. Se trata del Decreto 277 de 2017 con el que se regula la amnistía e indulto por delitos como rebelión, sedición, asonada, entre otros y los delitos conexos.

De esta manera, las **autoridades judiciales deberán comenzar a aplicar estas disposiciones a personas cobijadas por la Ley 1820**, a propósito de los acuerdos de paz de La Habana y que complementa la entrada en vigencia de la Jurisdicción Especial para la Paz.

La amnistía de iure tiene como efecto la puesta en libertad inmediata y definitiva de aquellos, que estando privados de la libertad hayan sido beneficiados con esta medida. Le puede interesar: [Ley de amnistía es la más completa que se ha aprobado en Colombia](https://archivo.contagioradio.com/enrique-santiago/)

En cuanto al número de guerrilleros, milicianos y colaboradores de las FARC-EP que podrían verse beneficiados con la amnistía, manifiesta Enrique Santiago, asesor jurídico del equipo de paz que **podría rondar entre los 2.500 y los 3.ooo**. Le puede interesar: [Hay más de 700 solicitudes de amnistía a integrantes de las FARC sin respuesta](https://archivo.contagioradio.com/700-solicitudes-de-amnistia-sin-respuesta/)

Sin embargo asegura que es complicado determinarlo por que **en muchos casos los procesados han sido condenados en tribunales por delitos distintos al de rebelión**, lo que a su juicio se abstrae

De esta manera, **los jueces deberán comenzar casi que de inmediato, a realizar las respectivas amnistías e indultos** para los beneficiarios de la ley de amnistía, pues ya cuentan con un documento que regula y les entrega las disposiciones necesarias para comenzar a actuar.

En el documento, también se incluye la forma cómo las personas que hagan parte de los indultados o amnistiados, integrantes de las FARC deben ser trasladadas a las Zonas Veredales Transitorias de Normalización (ZVTN).

En el caso de la aplicación de la amnistía de iure para los integrantes de las FARC que no se encuentren privados de la libertad, esta será aplicada solamente **cuando el beneficiario haya dejado las armas. Información que será verificada para otorgarla. **Le puede interesar: [Ley de Amnistía, un mecanismo para afrontar la impunidad del pasado](https://archivo.contagioradio.com/ley-amnistia-mecanismo-afrontar-la-impunidad-del-pasado/)

El Decreto, que tiene fecha del 17 de febrero de 2017, asegura que debe regir a partir de ese día y que cualquier disposición anterior queda derogada, por lo que el paso a seguir, será **el inicio de las amnistías y los indultos de manera que los y las integrantes de las FARC puedan hacer su tránsito a la vida civil** como reza en los Acuerdos de Paz.

[Decreto 277 de 2017](https://www.scribd.com/document/339729993/Decreto-277-de-2017#from_embed "View Decreto 277 de 2017 on Scribd") by [Contagioradio](https://es.scribd.com/user/276652802/Contagioradio#from_embed "View Contagioradio's profile on Scribd") on Scribd

<iframe id="doc_76240" class="scribd_iframe_embed" src="https://www.scribd.com/embeds/339729993/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-OoCBphLHf8A5dvnQXYFm&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="0.6564774381368268"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio]{.s1}](http://bit.ly/1ICYhVU)[.]{.s1} {#reciba-toda-la-información-de-contagio-radio-en-su-correo-o-escúchela-de-lunes-a-viernes-de-8-a-10-de-la-mañana-en-otra-mirada-por-contagio-radio. .p1}

<div class="ssba ssba-wrap">

</div>
