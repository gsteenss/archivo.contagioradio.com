Title: El Esmad: ¿buscando lo que no se le ha perdido?
Date: 2017-05-03 07:44
Author: JOHAN MENDOZA TORRES
Category: Johan Mendoza, Opinion
Tags: Desmonte ESMAD, ESMAD
Slug: el-esmad-buscando-lo-que-no-se-le-ha-perdido
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/01/esmad-en-plaza-de-toros.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto:Desmonte del Esmad 

#### **Por: [Johan Mendoza Torres](https://archivo.contagioradio.com/johan-mendoza-torres/)** 

###### 3 May 2017 

La guerra, la contradicción, parecen ser los colores más acentuados de la historia de la humanidad, no obstante, esa condición, esa especie de “prisión” forma parte de nuestra naturaleza humana y no tenemos otra cosa por hacer más que afrontarla y perseguir, de acuerdo a cada época, la necesidad de instalarnos en la posición éticamente más defendible y actuar en consecuencia.

[Colombia, desde hace un par de años viene anunciando vientos de cambio. Las guerrillas están insertas en procesos de paz o de implementación de los mismos y si bien existen problemas, decir que las guerrillas llegan a su fin, no involucra ningún tipo de llamamiento a la celebración de nada, las cosas son demasiado serias, aún cuando todavía matan a tanta gente en Colombia por pensar políticamente diferente.  ]

[Hay que alejar de la política ese tipo de perversiones al estilo del pastorcito Arrázola, que salió celebrando la victoria del No durante el último plebiscito, aludiendo que “de Dios no se burlaba nadie”, es decir, realmente aludiendo que “de él no se burlaba nadie”; por supuesto aquí no hay nada que celebrar, pues no hubo derrotados, hubo quienes combatieron y otros que resistieron en el mortuorio marco de una cantidad de muertos ni la berraca.]

[A pesar de los vientos de cambio, una nueva guerra emerge, una nueva guerra anuncia no sólo su llegada (pues desde hace rato viene avanzando) sino que anuncia su incremento. Es una guerra que como hoyo negro viene absorbiendo parte sustancial de las luchas sociales. Esa nueva guerra es la que los funcionarios públicos, instalados en el poder ejecutivo, han promovido con un brazo armado que sigue fortaleciéndose y arrancando de raíz las posibilidades de una democracia real, de una democracia directa, me refiero pues, a ese brazo armado del poder dominante contemporáneo en Colombia llamado Esmad.]

[El Esmad es una fuerza que fundó Pastrana, consolidó Uribe, que Santos se negó a desmontar tajantemente y que hoy juega el papel de un juez que determina si una protesta social se queda contingentemente solo en protesta, o trasciende a los cambios que solicitan tantos y diversos grupos sociales.  ]

[El Esmad, es una fuerza militar que está desafiando los anhelos de democracia ante los ojos de todos, con el apoyo anti-sensacionalista de los medios masivos, a cuyos editores les parece un atropello que la policía antimotines en Venezuela arroje gases, pero son precavidos y bastante modositos a la hora de hablar del Esmad cuando arremete, mata y hasta termina incendiando barrios miserables en Bogotá.]

[Tristemente, estamos viendo cómo el Esmad sigue reprimiendo y destruyendo las peticiones más simples… Los comandantes del Esmad, bajo órdenes de los poderes ejecutivos de los municipios y ciudades, arremeten contra personas de todas las edades, contra discapacitados, contra todo elemento social que sume esfuerzos para enfrentarse a un poder dominante, sea cual sea su materialización.  ]

[El Esmad, sigue haciendo de las suyas y no hay lugar para admitir su existencia. Hoy se encuentran totalmente desenfocadas esas razones que lo justificaban sobre la base de una supuesta defensa contra multitudes organizadas con explosivos como en los fuertes tropeles universitarios de principio de siglo.]

[Eso es una falacia, pues en las universidades, el tropel ha sido domesticado al punto de su extinción. Los capuchos ya no son la justificación de la existencia del Esmad, y en los paros o protestas, los movimientos sociales no han desarrollado cuerpos de defensa con capacidad para hacer frente a esa fuerza efectiva y letal como es el Esmad.]

[La razón técnica para que el Esmad deje de existir es que emplea armas no letales (con las que a veces mata) que son exageradamente asimétricas para disipar una protesta con gente que no maneja ningún tipo de arma contundente salvo piedras y palos.]

[La razón legal para que el Esmad deje de existir, es que la injusticia del uso asimétrico de sus armas, se puede corroborar con las disposiciones legales de otros países, en donde ese tipo de instrumentos tienen prohibido su uso contra la población civil, así sean armas con el epíteto de: “no letales”.]

[La razón política para que el Esmad deje de existir, es que, si una protesta social compuesta por manifestantes que no van armados, no es disipada con diálogo político, sino con el Esmad, entonces los caminos de la manifestación y presión social hacia los gobernantes quedará limitado de raíz, dando como resultado que al final, la gente sólo tenga derecho a protestar desde el Facebook, de forma “segura” y sin que le haga daño a la estabilidad de los malos gobiernos.  ]

[¿Acaso no es desestabilizar a un mal gobierno la esencia de la protesta social? La protesta social no sirve para demostrar lo democrático que es un mal gobierno. Sino que ésta trata de desestabilizar políticamente a un mal gobierno, por eso es la esencia práctica de las posibilidades políticas de los movimientos sociales, y hoy el Esmad bajo órdenes del ejecutivo, está destruyendo y reprimiendo brutalmente esas posibilidades.  ]

[En este marco, para hablar de una nueva guerra, aún falta amarrar un par de cabos. No es equivocado pensar que tarde o temprano los movimientos políticos y sociales, tal y como ocurrió en otras épocas cuando el poder dominante puso las condiciones para el enfrentamiento, terminen desembocando en acciones que den respuestas contundentes a las agresiones que el Esmad hoy realiza… ese es el problema…]

[El Esmad está buscando lo que no se la ha perdido… ¿qué es lo que no se le ha perdido? … que la gente comprenda que resistirse de manera técnica y organizada a sus agresiones sea quizá la única manera de sobrevivir políticamente… ese es el marco para una nueva guerra, que en caso de que ocurra, y cuando obviamente desde los medios salgan a decir, “miren cómo agreden a la fuerza pública” se recuerde, qué ocurrió para que la gente se organizara y respondiera, ojalá se recuerde que la gente no le gusta la guerra de ningún tipo, pero que también, según lo demuestra la historia, termina reaccionando y respondiendo ante los desafíos o agresiones del poder dominante.]

#### [**Leer más columnas de Johan Mendoza Torres**](https://archivo.contagioradio.com/johan-mendoza-torres/)

------------------------------------------------------------------------

###### Las opiniones expresadas en este artículo son de exclusiva responsabilidad del autor y no necesariamente representan la opinión de Contagio Radio.
