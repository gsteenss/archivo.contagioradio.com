Title: Bosques de Antioquia han perdido el 65% de su extensión
Date: 2018-02-12 14:45
Category: Ambiente, Voces de la Tierra
Tags: Antioquia, bosques andinos, bosques secos, deforestación
Slug: bosques-de-antioquia-han-perdido-el-65-de-su-extension
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/02/bosques-andinos-e1518462767677.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Programa Bosques Andinos] 

###### [12 Feb 2018] 

El observatorio de Bosques de Antioquia alertó que solamente quedan **35% de los bosques que existían hace 10 años** en el departamento de Antioquia. A pesar de que le atribuyen a este fenómeno la ganadería extensiva, las comunidades de la sub región norte de Antioquia han denunciado que, para la realización del proyecto Hidroituango, se deberán talar de 4.500 hectáreas de bosque tropical.

De acuerdo con el observatorio, de los 2.7 millones de hectáreas que había en ese departamento, para 2015 quedaban **2.2 millones**. Además, de los bosques andinos, quedan un poco más de 5 mil hectáreas lo que corresponde al 35% del total que había antes. Esto hace al departamento de Antioquia la región con la tasa de deforestación más acelerada del país.

### **Urabá ha sido la región más afectada por afectación a bosques andinos** 

El Observatorio indicó que “la transformación del paisaje está relacionada **con el cambio del uso de la tierra** para actividades agropecuarias como la ganadería”. Además, la cobertura de bosques en Antioquia tuvo una pérdida de 19.700 hectáreas al año desde 1990 hasta 2015 donde los bosques secos fueron los más afectados pues presentaron una reducción del 55% para ese periodo. (Le puede interesar:["Comunidades fortalecen sus estrategias para conservar los bosques colombianos"](https://archivo.contagioradio.com/comunidades-fortalecen-sus-estrategias-para-conservar-los-bosques-colombianos/))

En su libro presentado este año "Bosques Andinos: estado actual y retos para su conservación en Antioquia", establece el Observatorio que “los municipios con mayor pérdida de cobertura de bosque fueron **Turbo, Murindó, Dabeiba, Mutatá y Chigorodó**”. Además, teniendo en cuenta el área de cada lugar, “los que presentaron mayores pérdidas fueron Itagüí, San Juan de Urabá, Carepa, Cisneros y Chigorodó”.

Esto hace que la región del Urabá sea la que mayores pérdidas de bosque andino ha tenido y está asociada a actividades como la ganadería y la deforestación. Además, recalcan que el **100% del agua disponible del Valle de Aburrá** proviene de los ecosistemas de bosque andino que está en riesgo.

### **Bosque seco tropical de Antioquia también está en riesgo** 

A este panorama, se suma el peligro que corren los bosques secos tropicales que se encuentran en la sub región norte de Antioquia debido a la realización de mega proyectos como **Hidroituango**. En diferentes oportunidades, el Movimientos Ríos vivos ha manifestado que, la inundación de la represa, va a afectar toda la flora y la fauna que se encuentra a lo largo de 12 municipios que se verán afectados con la construcción. (Le puede interesar:["ESMAD desaloja a campesinos de Sabanalarga en Antioquia"](https://archivo.contagioradio.com/esmad-desaloja-a-campesinos-de-sabanalarga-en-antioquia/))

Además, de acuerdo con el Movimiento y según los permisos ambientales que tiene el proyecto, “se tendrían que **talar 4.500 hectáreas** que hacen parte de la inundación”. Ante esto y teniendo en cuenta la fase en la que se encuentra el proyecto Hidroituango, “hasta el momento han sido taladas 5% del total esas hectáreas”. Esto además de afectar los ecosistemas, pone en riesgo la búsqueda de personas desaparecidas por el conflicto armado.

<div class="text_exposed_show">

###### Reciba toda la información de Contagio Radio en [[su correo]

</div>
