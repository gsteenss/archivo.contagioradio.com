Title: Cuidar y respetar la naturaleza es cuidar la vida, la obra de Dios.
Date: 2019-08-08 13:36
Author: A quien corresponde
Category: Opinion
Tags: Religión
Slug: cuidar-y-respetar-la-naturaleza-es-cuidar-la-vida-la-obra-de-dios
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/carretera.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

***Creaturas del Señor, bendigan al Señor***

(Daniel 3, 57)

Bogotá, 8 de agosto de 2019

 

*Reconocer nuestra dependencia de la naturaleza, *

* nos hace más responsables con ella y con la vida.*

Estimado

**Ministro ordenado**

Cristianos, cristiana, personas interesadas.

Cordial saludo,

Por una persona amiga que asiste a su iglesia, supe que en una celebración dominical les había dicho a los asistes que *“no hay que meterse en esas actividades que, bajo el pretexto de defensa del medio ambiente, se oponen al progreso y al desarrollo”.*

Por los comentarios de mi amiga, sé que usted es una persona buena, un buen ministro, una persona de buena voluntad. Creo que tuvo una formación religiosa parecida a la mayoría de pastores y ministros y que no ha tenido la oportunidad de conocer las reflexiones bíblicas, teológicas y socio-ambientales que a mí me ayudaron a abrir los ojos y la mente para leer y entender que el mensaje del Señor Jesús tiene relación con la realidad que nos afecta positiva o negativamente.

Por esto me atrevo a reflexionar con usted sobre la responsabilidad de los “líderes religiosos” con la vida de todos los seres vivos de la tierra, incluidos los seres humanos, con el cuidado de la obra de Dios de la que somos parte y que por nuestro ministerio tenemos una mayor responsabilidad.

Los creyentes en general y los cristianos en particular, no podemos desconocer la crisis ambiental, el calentamiento global, la contaminación de los ríos y mares con plásticos y mercurio, la contaminación de aire con gases de efecto invernadero, la destrucción de los bosques, en especial de la Amazonía, la destrucción de cientos de especies, etc. porque nos pone en riesgo como especie, a usted y a mí, a creyentes y no creyente. No podemos olvidar que Dios colocó al ser humano en medio del jardín (el mundo) para que *“lo guardara y lo cultivara”* (Génesis 2, 15)

Ante esta crisis no podemos ser espectadores por cuatro razones: primera, porque  está en riesgo la sobrevivencia de miles de especies, incluida la especie humana, es decir, de nosotros; segunda, porque la naturaleza, la creación es obra de Dios y como creyentes debemos respetarla y contemplar en ella al Creador; tercera, porque hay una interpretación bíblica que ha legitimado y justificado su destrucción por parte del ser humano, lo cual contradice una lectura responsable de la Palabra de Dios; cuarto, por el “antropocentrismo” (que debe ser superado y remplazado por el “biocentrismo”) que ha colocado la naturaleza bajo el dominio irresponsable del ser humano, desconociendo la interdependencia que garantiza todas las formas de vida en la tierra, incluida la humana.

La formación de la mayoría de los ministros en las iglesias ha estado alejada de la realidad, sin herramientas para analizarla, desconfiando de ella, sin ver su relación con nuestra salvación. No hemos aprendido a escuchar la voz de Dios que habla en la creación, en la naturaleza, que llama desde las realidades humanas, sociales y ambientales. Nos cuesta trabajo reconocer que Dios sigue actuando en la historia por medio de su Espíritu, como lo hizo en su Hijo Jesús de Nazaret.

Esta comprensión del mundo, de Dios, de la creación nos ha llevado a creer que podemos resolver los problemas que afectan los seres humanos y la naturaleza con buenas intenciones, con oraciones desencarnadas, con una vida religiosas desconectada de la vida, con cultos y ritos sin vida y acción, sin tener en cuenta lo que dice el Señor por medio del profeta Isaías: *“Cuando entran a visitarme y pisan mis atrios, ¿quién exige algo de sus manos? No me traigan más ofrendas sin valor, el humo del incienso es detestable. Lunas nuevas, sábados, asambleas... no aguanto reuniones y crímenes. Sus solemnidades y fiestas las detesto; se me han vuelto una carga que no soporto más. Cuando extienden las manos, cierro los ojos; aunque multipliquen las plegarias, no los escucharé. Sus manos están llenas de sangre… Lávense, purifíquense, aparten de mi vista sus malas acciones. Cesen de obrar mal, aprendan a obrar bien; busquen el derecho, socorran al oprimido; defiendan al huérfano, protejan a la viuda”* (Isaías 1, 12-17).

Con buenas intenciones pastores, líderes religiosos, sacerdotes, religiosas y religiosos hemos apoyado o legitimado proyectos extractivistas, minero-energéticos, agroindustriales o de infraestructura que hablaban maravillas de los beneficios del “desarrollo”, del “progreso” para las comunidades y la región, de la protección al “medioambiente”, de sostenibilidad ambiental, de inclusión, de responsabilidad social empresarial e incluso de apoyo a la obra del evangelio, pero luego encontramos daños sociales y ambientales irreparables: violencia, desintegración de las comunidades, enfermedades, desempleo, alcoholismo, prostitución, destrucción de la producción local, destrucción de los bosques, disminución y contaminación de las aguas, abandono de la región de sus habitantes y un largo etcétera.

Es necesario preguntarnos si ese “desarrollo” y ese “progreso” es querido por Dios;  si ese panorama de destrucción humano, social, ambiental muestra que estamos dejando actuar al Espíritu Santo, como decimos en predicaciones, como afirman personas creyentes en esas mismas regiones; si se puede hablar del “crecimiento de la obra del Evangelio”, solamente porque crece el número de creyentes mientras “se destruye la obra de Dios”.

Recordemos que la primera página de la Biblia dice que *“Al principio Dios creo que cielo y la tierra”* (Génesis 1,1); que Dios hizo la luz, el firmamento, la tierra, el mar, la hierba verde, arboles, los dos grandes astros (sol y luna), las estrellas, los cetáceos y los seres vivos que se deslizan por aguas, las aves aladas según su especie, las fieras según su especie, los animales domésticos según su especie, los reptiles del suelo según su especie (Cf. Génesis 1,1-24), “Y *vio Dios que era bueno”* (versos 10, 12,18,21,25) y al final concluye, *“Y vio Dios todo lo que había hecho: y era muy bueno”* (Gn 1, 31).

Al final de la vida Dios nos preguntará por lo que hemos hecho con su obra, su creación **“que era muy buena”.**

Fraternalmente, su hermano en la fe,

Alberto Franco, CSsR, J&P

<francoalberto9@gmail.com>

 
