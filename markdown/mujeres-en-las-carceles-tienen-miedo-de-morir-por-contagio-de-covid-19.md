Title: Mujeres en las cárceles tienen miedo de morir por contagio de Covid 19
Date: 2020-04-22 14:07
Author: AdminContagio
Category: Expreso Libertad, Nacional, Programas
Tags: carcel de mujeres, carceles de colombia, contagio, Covid-19, Expreso Libertad
Slug: mujeres-en-las-carceles-tienen-miedo-de-morir-por-contagio-de-covid-19
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/04/Carcel-de-mujeres.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

En este programa del **Expreso Libertad**, Jazmín Reyes y Marta Franco, ex prisioneras sociales integrantes del colectivo Mujeres Libres denuncian los riesgos a los que están expuestas las mujeres en las cárceles con la llegada del Covid 19.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

De acuerdo con las invitadas, en las cárceles de mujeres actualmente no hay atención médica, como tampoco se estaría cumpliendo con los protocolos anunciados desde el 13 de marzo para mitigar la pandemia en los centros de reclusión.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Asimismo, aseguran que desde el inicio del confinamiento, las mujeres en las cárceles han visto un aumento en la vulneración de sus derechos, como lo son el de la visita, ya que no se han planteado otras alternativas para garantizar este único mecanismo que tienen para conectarse con sus familias.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":5} -->

##### Escuche en este programa la situación que afrontan las mujeres en las cárceles tras la llegada del Covid 19:

<!-- /wp:heading -->

<!-- wp:core-embed/facebook {"url":"https://www.facebook.com/contagioradio/videos/591737041695496/","type":"video","providerNameSlug":"facebook","className":""} -->

<figure class="wp-block-embed-facebook wp-block-embed is-type-video is-provider-facebook">
<div class="wp-block-embed__wrapper">

https://www.facebook.com/contagioradio/videos/591737041695496/

</div>

</figure>
<!-- /wp:core-embed/facebook -->

<!-- wp:paragraph -->

[Otros programas de Expreso Libertad](https://archivo.contagioradio.com/programas/expreso-libertad/)

<!-- /wp:paragraph -->
