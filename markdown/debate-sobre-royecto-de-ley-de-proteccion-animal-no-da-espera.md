Title: “Debate sobre royecto de Ley de protección animal no da espera”
Date: 2015-03-17 19:27
Author: CtgAdm
Category: Animales, Nacional
Tags: Animales, Animalistas, Congreso, derechos de los animales, Juan Carlos Losada, Ley 84 de 1989, Ley Animalista, Proyecto de Ley
Slug: debate-sobre-royecto-de-ley-de-proteccion-animal-no-da-espera
Status: published

##### Foto: Contagio Radio 

<iframe src="http://www.ivoox.com/player_ek_4227274_2_1.html?data=lZefmZebeI6ZmKiakpqJd6Kll5KSmaiRdo6ZmKiakpKJe6ShkZKSmaiRlNPj2srQ1tSPqMafrcrmjabSrc7Vzc7g1saPt8bmhqigh6aVb9HmxtjS0NnFqNCfzsaSpZiJhpLVz8aYx9OPcYarpJKw0dPYpcjd0JC%2Fw8nNs4yhhpywj5k%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Juan Carlos Losada, Representante a la Cámara] 

**Este miércoles 18 de Marzo se tiene previsto el debate del proyecto de Ley de protección animal,** en la Comisión primera de Cámara de representantes, que fue propuesto por el representante a la cámara del Partido Liberal, Juan Carlos Losada.

De acuerdo a Losada, el proyecto se realiza de la mano de varias organizaciones animalistas, y con base a esas reuniones, se definieron dos ejes principales en este proyecto de Ley.

El primero es que **los animales sean declarados seres sintientes,** “**en este momento los animales  solo son considerados bienes muebles”**, por lo tanto, cuando una persona les hace daño, legalmente solo  se  penaliza como daño en bien ajeno, es por eso que “**estamos reformando el código civil,** consideramos que los animales no pueden ser tratados como cosas”, expresó el representante.

El segundo eje del Proyecto de Ley, se refiere a la penalización del maltrato animal, “que aquellas personas que maltraten animales o que por ejemplo, disparen a un perro en vía pública, o descuarticen a un caballo, o que maltraten al toro en las corralejas, tengan un castigo de tipo penal”, asegura Losada, quien agrega que de ser aprobado el proyecto, se fortalecerá y modificará la Ley 84 de 1989, en la que actualmente la multa máxima es de \$250.000, lo que para el congresista y las organizaciones animalistas, “**son multas absolutamente irrisorias y ridículas,** nosotros queremos pasar a multas entre 5 y 60 salarios mínimos legales vigentes”.

Este miércoles se citó a plenaria de Cámara en el Congreso de la República, sin embargo, el congresista cree que el tema animalista se debatirá mañana y será el primer proyecto que se discuta en la comisión, pues ese es el compromiso de la mesa directiva, "le hacemos un llamado a la mesa directiva de la comisión primera, **Colombia quiere que se debata el tema de la protección animal, quiere que se penalice el maltrato animal** y que se declare la sintienza de los animales”.

Para Juan Carlos Losada, es claro que Colombia es un país que quiere la paz y quiere estar libre de todo tipo de violencia, y eso debe incluir a los animales, “estoy seguro que la comisión primera le hará honor a los animales y será el primer proyecto que se debata en este nuevo comienzo de sesiones ordinarias del Congreso”.
