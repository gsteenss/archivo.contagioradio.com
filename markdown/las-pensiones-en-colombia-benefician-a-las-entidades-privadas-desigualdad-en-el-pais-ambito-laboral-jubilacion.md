Title: Sistema de pensiones en Colombia favorece a los más ricos
Date: 2017-06-15 14:25
Category: Economía, Nacional
Tags: OCDE, pensiones, Sistema pensional
Slug: las-pensiones-en-colombia-benefician-a-las-entidades-privadas-desigualdad-en-el-pais-ambito-laboral-jubilacion
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/06/cms-image-000059998-e1497553890878.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:  Confidencial Colombia] 

###### [15 Jun 2017]

A propósito de la propuesta de la Asobancaria, de establecer una reforma estructural que resuelva la crisis fiscal del sistema de pensiones y de las sugerencias de la OCDE de igualar la edad pensional entre hombres y mujeres, varios analistas concuerdan en que esto **aumentaría la brecha en la desigualdad en el país**. Han sido enfáticos en manifestar que Colombia tiene grandes problemas en el ámbito laboral poniendo en desventaja a las personas en edad de jubilación.

Para Mario Valencia, miembro de la Red por la Justicia Tributaria, “el sistema de pensiones como está diseñado hoy es un sistema de recaudo financiero que **beneficia a los grandes grupos económicos que no se preocupa por pensionar a los colombianos y colombianas**”. Igualmente afirmó que hay una preocupación grande en las sugerencias de la Organización para la Cooperación y el Desarrollo OCDE, de igualar la edad pensional entre hombres y mujeres a los 62 años. (Le puede interesar: "[OCDE propone igualar edad de jubilación entre hombres y mujeres](https://archivo.contagioradio.com/ocde-propone-igualar-edad-de-jubilacion-de-hombres-y-mujeres/)")

Esta sugerencia parece ir en contravía con la realidad que vive el país en donde, las **mujeres al ganar 20% menos de salario que los hombres, tendrían que trabajar más** del doble que ellos para poder conseguir una remuneración en su vejez.

Un reciente informe de la Universidad de la Sabana, dice que únicamente el 26% de personas mayores de 65 años gozan de una pensión. Adicionalmente, la gran informalidad, la desregulación del trabajo y la alta rotación en el empleo, no permite que se amplíe la cantidad de cotizantes en el país.

Valencia aseguró que las intenciones de Colombia para entrar al “club de los países ricos”, solo “**buscan generar más recursos para ser recaudados por las empresas y esto aumenta la brecha de desigualdad entre hombres y mujeres**”. Si bien Colombia ha estado empeñada en entrar a la OCDE, Valencia asegura que “el país no se parece en nada a los países de ese club en términos de pobreza, desigualdad, infraestructura e investigaciones de ciencia y tecnología”. (Le puede interesar: "[El precio de entrar a la OCDE](https://archivo.contagioradio.com/el-precio-de-entrar-a-la-oecd/)")

### **Colombianos y colombianas no alcanzan a ahorrar para pensionarse** 

Tal y como está el sistema de pensiones en el país, y con las intenciones del Gobierno Nacional de fortalecer los fondos privados, **una persona no alcanza a ahorrar en su vida laboral lo necesario para poderse pensionar**. Valencia explicó que “hay una competencia entre los fondos privados y el fondo público que lo que hace es debilitar el sistema y el gobierno tiene que dar más subsidios generándose así una crisis”.

Valencia afirmó que para poder superar la crisis del sistema de pensiones es necesario que el Gobierno Nacional tenga como prioridad los intereses de los trabajadores. Además, dijo que “**se debe revertir el sistema privado para regresar a un sistema público** que se base en la solidaridad generacional y no en la especulación”.

Bajo las recomendaciones de la OCDE, el gobierno de **Colombia ha querido aumentar las semanas de cotización, la edad de jubilación** para garantizar, como lo dice Valencia “que los fondos de pensiones se sigan fortaleciendo con los dineros de los colombianos y colombianas”.

<iframe id="audio_19289390" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_19289390_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU).
