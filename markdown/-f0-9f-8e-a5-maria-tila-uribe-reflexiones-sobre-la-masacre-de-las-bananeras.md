Title: María Tila Uribe, reflexiones sobre la masacre de las bananeras
Date: 2019-06-15 07:00
Author: CtgAdm
Category: Memoria, Sin Olvido
Tags: Bananeras, María Tila Uribe, Masacre de las bananeras
Slug: %f0%9f%8e%a5-maria-tila-uribe-reflexiones-sobre-la-masacre-de-las-bananeras
Status: published

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

######  

**María Tila Uribe** nació en Bogotá en 1931, **hija del líder del Partido Socialista Revolucionario Tomás Uribe Márquez y Enriqueta Jiménez** y sobrina de la primera mujer líder sindical del país **María Cano Márquez.**

Pionera en impulsar los primeros congresos obreros del país en los años veinte; educadora popular vinculada al proyecto alfabetizador del Padre Camilo Torres, a grupos comprometidos con las luchas de emancipación de las mujeres y co-fundadora del “Frente Unido Femenino”.

Durante más de 30 años **María Tila Uribe** ha investigado y recogido testimonios que la motivaron para escribir el libro “**... les regalamos el minuto que falta**” reflexiones sobre la masacre de las bananeras que espera llevar a las nuevas generaciones para evitar que el sangriento capitulo en la historia de Colombia caiga en el olvido. (Ver:["Los empresarios son los que están detrás de los asesinatos": líderes del Bajo Atrato y Urabá )](https://archivo.contagioradio.com/hasta-cuando-y-cuantos-mas-continuan-la-violencia-contra-lideres-sociales-del-bajo-atrato-y-uraba/)

[Ver más videos de Contagio Radio](https://archivo.contagioradio.com/galerias/videos/):

 
