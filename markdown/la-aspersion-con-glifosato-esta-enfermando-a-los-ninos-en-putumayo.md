Title: La aspersión con glifosato está enfermando a los niños en Putumayo
Date: 2015-09-18 18:09
Category: Comunidad, DDHH, Nacional
Tags: afectaciones en salud por glifosato, Fumigaciones con glifosatoo, Ildifonso Mendoza, Piñuña Blanco, Puerto Asís, Putumayo, Red de Derechos Humanos del Putumayo
Slug: la-aspersion-con-glifosato-esta-enfermando-a-los-ninos-en-putumayo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/IMG-20150918-WA0003.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Mesa de DDHH de Putumayo 

###### <iframe src="http://www.ivoox.com/player_ek_8463903_2_1.html?data=mZmjlZ6Ud46ZmKiak5WJd6KmmJKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRi9PV18rgjabKqcTowsjW0dPJt4zZz5Dax9PTtsbnjNXc1JDFt9HZ09jW0dPJt4zX0NOYydHNqo6ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Idilfonso Mendoza, Mesa de DDHH] 

###### [18 Sep 2015]

La **Red de Derechos Humanos del Putumayo**, denuncia que adultos, niños y niñas habitantes del corregimiento Piñuña Blanco del municipio de Puerto Asís, Putumayo, presentan graves **deterioros en su estado de salud**, ocasionados por las fumigaciones con glifosato realizadas el pasado 10 de agosto.

De acuerdo con las declaraciones de Ildifonso Mendoza, miembro de la Red, las mayores afectaciones se han presentado en la población infantil, con graves **brotes en la piel "tipo yagas o granos"**, por los cuales han llevado a los menores a clínicas privadas ante la falta de efectividad en la atención de los hospitales ubicados en la zona.

Las afectaciones en la piel, provocan en los niños estrés ante la picazón persistente, en los que podrían producirse infecciones. **Los casos más notorios reportados por la comunidad son el de dos menores de 1 y 2 años respectivamente**, y en otros empiezan a percibir los síntomas con pequeños brotes en su cuerpo.

Apesar de las determinaciones de cancelar las aspersiones con el agente químico, Mendoza reporta que "el gobierno nacional sigue persistiendo en este método tan inhumano", que afecta a las comunidades en donde "**incluso los techos de las casas quedan goteando glifosato"** al paso de las fumigaciones aéreas, contaminando además el aire del lugar.

Aunque en oportunidades anteriores se han llevado las **denuncias ante las autoridades competentes para el caso, como la personería y la defensoría sin obtener respuestas** concretas, deacuerdo con el líder comunitario la exigencia en este momento  "es que tengan atención médica los niños y que se pueda solucionar el problema de salud".
