Title: "La verdad es el único camino a la reconciliación" Víctimas de Crímenes de Estado
Date: 2018-03-06 15:43
Category: DDHH, Otra Mirada
Tags: Día de la Dignidad de las Víctimas de Crímenes de Estado, MOVICE, víctimas de crímenes de Estado
Slug: la-verdad-es-el-unico-camino-a-la-reconciliacion-victimas-de-crimenes-de-estado
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/02/desaparecidos.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo Partícular] 

###### [06 Mar 2018] 

Los familiares y víctimas de crímenes de Estado, enviaron una carta a la Comisión para el Esclarecimiento de la Verdad, la Convivencia y No Repetición, que no solo se encargue de la misión de encontrar a cada una de las personas desaparecidas en Colombia, sino que también avancen en las investigaciones de los responsables que **“decidieron asesinar, desaparecer y torturar a miles de ciudadanos por acción u omisión del Estado”**.

Esta petición se da 10 años después de que se realizara la gran movilización por las víctimas de crímenes de Estando, del 2008, que permitió el reconocimiento público de la existencia de acciones **violentas por parte de la Fuerza Pública y las instituciones Estatales contra la ciudadanía. **(Le puede interesar:["JEP es una oportunidad única para resarcir derechos de las víctimas: MOVICE"](https://archivo.contagioradio.com/la-impunidad-en-crimenes-de-estado-es-del-90-alejandra-gaviria/))

Hoy en día, organizaciones como el MOVICE (Movimiento de Víctimas de Crímenes de Estado) hablan de la existencia de 11.751 personas que fueron asesinadas durante las masacres cometidas por grupos paramilitares, con el auspicio y el silencio cómplice de las Fuerzas Militares. De estas, **968 fueron asesinadas en 153 masacres perpetradas directamente por la Fuerza Pública. Sumado a esto, la Fuerza Pública es responsable del asesinato selectivo de 2.423 personas**.

A si mismo, señalaron que aún no se conoce la verdad tras los más de **15.930** desaparecidos a manos de estructuras paramilitares en connivencia del Estado, ni se ha avanzado en la identificación de los responsables de los más de **1,463 casos de ejecuciones extrajudiciales cometidas por tropas de la Fuerza Pública**. (Le puede interesar:["Reclutador de falsos positivos en Soacha, se compromete con la verdad"](https://archivo.contagioradio.com/falsos_positivos_preacuerdo_madres_soacha/))

En ese sentido, los y las familiares y las víctimas de crímenes de Estado afirmaron que “la verdad es el único camino para la reconciliación, y esta solo se logrará si realmente las instituciones del Estado y las personas responsables de este largo capítulo de guerra se transforman, **se comprometen con la verdad y asumen el compromiso de no cometer nunca más, crímenes de Estado en Colombia”.**

###### Reciba toda la información de Contagio Radio en [[su correo]
