Title: Así sería el acuerdo sobre justicia que se firmaría hoy entre gobierno y FARC
Date: 2015-09-23 09:38
Category: Nacional, Paz
Tags: Acuerdo de Justicia, Derechos Humanos, FARC, firma de acuerdo de justicia, firma de acuerdo de justicia gobierno y farc, Presidente Santos, proceso de paz, Punto seis del proceso de paz, Timochenko en La Habana
Slug: acuerdo-sobre-justicia-farc-y-gobierno-de-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/dialogos-de-paz.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: elpilon.com.co 

###### [23 sep 2015]

El acuerdo se denominaría **Sistema integral de verdad justicia, reparación y no repetición**, buscaría que se acogieran miembros de las fuerzas militares, miembros de las FARC, instituciones civiles y empresarios que hayan **participado de acciones en el marco del conflicto**. Uno de los puntos claves será el sistema de Justicia basado en la verdad.

Se ha podido establecer que el día de hoy a las 4:00 pm, el presidente Juan Manuel Santos estaría en Cuba para **reunirse con el máximo jefe de las FARC**, Timochenko, el representante de Estados Unidos, Bernard Aronson y el Presidente cubano, Raul Castro para firmar el punto de Justicia con el cual el proceso de paz estaría en un punto de no retorno , siendo este un acuerdo histórico para el país.

Habría un **tribunal adhoc** conformado por jueces colombianos e internacionales los cuales se encargarían de revisar penas y ejecutar sentencias , las cuales estarían dadas de acuerdo a los **crímenes que sean reconocidos y que sean aceptados con la verdad**.

El tribunal manejaría **dos procedimientos** para las sentencias, uno en caso de que las personas que se acojan den a conocer verdad y reconozcan su participación y otro en caso de que no se reconozca la verdad y responsabilidad en estos hechos.

Las condenas irían con **penas máximas de 20 años de cárcel**, pero se brindarían ciertas amnistías por crímenes políticos, como la rebelión y por delitos conexos, como el narcotráfico. Se ha logrado establecer que no habrá granjas por carcel pero si habrá restricciones de los derechos civiles y libertades de las personas que resulten condenadas en el tribunal especial.

Otras de las sanciones que se contemplan en este acuerdo son **sanciones restaurativas** y la **participación de organizaciones de derechos humanos** quienes podrán presentar casos si creen que hay impunidad en hechos vinculados con el conflicto, caso que en ningún otro proceso de paz se ha presentado.

Noticia en desarrollo...
