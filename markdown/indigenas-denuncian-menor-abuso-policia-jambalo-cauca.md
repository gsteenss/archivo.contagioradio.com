Title: Comunidades indígenas denuncian que menor fue víctima de abuso de la Policía en Jambaló, Cauca
Date: 2020-09-13 21:31
Author: CtgAdm
Category: Actualidad, DDHH
Tags: Abuso policial, Agresiones contra menores de edad, Cauca, Jambaló
Slug: indigenas-denuncian-menor-abuso-policia-jambalo-cauca
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/policia-.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: Policía Nacional/ RCN Radio

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

En medio de las reiteradas denuncias contra la Policía Nacional y los sucesos ocurridos en la ciudad de Bogotá donde 12 personas fueron asesinadas por integrantes de la institución y otras 250 heridas en medio de las manifestaciones en rechazo al asesinato del abogado Javier Ordoñez; autoridades indígenas en Pitayó, Cauca, denuncian que un menor de 15 años fue víctima de abuso policial al ser sometido a humillación y procedimientos ilegales.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Según la denuncia hecha por las autoridades indígenas de Jambaló, el joven que reside en la vereda Mariposas, fue retenido y habría sido obligado a desnudarse por, supuestamente, portar un camuflado y material de comunicación, siendo detenido arbitrariamente y desnudado en la Estación. [(Lea también: Indígenas denuncian que Ejército no ha sido solución, sino parte del problema en Cauca)](https://archivo.contagioradio.com/indigenas-denuncian-que-ejercito-no-ha-sido-solucion-sino-parte-del-problema-en-cauca/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Al respecto, **Yesid Conda, Coordinador de derechos humanos del Consejo Regional Indígena del Cauca (CRIC)** atribuye los hechos a una intención de hacerle pasar por guerrillero pues en realidad, el menor “se dirigía a la cabecera municipal a comprar una cartulina para realizar sus trabajos de estudio".

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Abusos de la Policía continúan

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Durante los sucesos, el joven también fue esposado, le tomaron sus huellas dactilares y lo obligaron a firmar documentos, hecho considerado como irregular. Gracias a la reacción de la comunidad y la gobernadora indígena del territorio se produjo la libertad del joven, sin embargo, la Policía Nacional no se ha manifestado al respecto. [(Lea también: Cinco normativas que deben cambiarse para frenar delitos de la Policía)](https://archivo.contagioradio.com/cinco-normativas-que-deben-cambiarse-para-frenar-delitos-de-la-policia/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

La denuncia se da en medio de acusaciones hechas por parte de la[ONG Temblores](https://twitter.com/TembloresOng), que han dado a conocer cómo la tarde del pasado 10 de septiembre, en un procedimiento, tres mujeres habrían sido interceptadas para una requisa, llevadas al CAI de la carrera séptima con 26 y habrían sido víctimas de abuso y acoso sexual, violencia verbal, detención arbitraria y soborno. [(Lea también: \[En vídeo\] 10 delitos de la Policía en medio de las movilizaciones)](https://archivo.contagioradio.com/en-video-10-delitos-de-la-policia-en-medio-de-las-movilizaciones/)

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
