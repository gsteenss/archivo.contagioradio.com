Title: Habitantes de Caquetá tienen en su cuerpo 17 veces más mercurio que el aceptado
Date: 2016-11-16 13:41
Category: Ambiente, Nacional
Tags: amazonas, Caquetá, indígenas, Mercurio, Peces, Pescados
Slug: habitantes-caqueta-tienen-cuerpo-17-veces-mas-mercurio-permitido
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/panama_indigenas.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Fundación David Lynch] 

###### [16 Nov. 2016] 

Un grupo de científicos colombianos realizó un viaje a tierras Caqueteñas para investigar el **nivel de mercurio que tenían los habitantes de esta región en sus cuerpos, así como los alimentos que consumían.**

Realizaron un viaje que los terminó llevando a la Pedrera, en este departamento. Allí con la guía y autorización de los mayores, hicieron pequeños cortes al pelo de las habitantes y los guardaban cuidadosamente para poderlos analizar. **La muestra total fue de 200 personas entre grandes y chicos.**

Luego, decidieron adentrarse con los pescadores en el rio y **recoger muestras de 46 peces que fueron atrapados**. Todos de especies diversas, pasaron también por un proceso de análisis.

**Los resultados de este trabajo**, del que hicieron la Universidad de Cartagena, funcionarios de Parques Nacionales, miembros del Ministerio del Interior y líderes de comunidades indígenas asentadas **a lo largo del río Caquetá fueron verdaderamente preocupantes. **

Los niveles de concentraciones de mercurio en el cabello de las personas que fueron parte de la muestra en el Amazonas, revelaron **niveles excesivos de mercurio en el 94% de las muestras. Y lo más complicado, es que un 79% del total de estos resultados superaron los niveles permitidos por la Organización Mundial de la Salud – OMS- .**

En el territorio del Caquetá, la minería a cielo abierto ha generado que una mayor concentración en algunos lugares, puesto que en estas prácticas se utiliza el mercurio para separar las partículas de oro de lodo y tierra y en el proceso el mercurio que se desecha termina el las fuentes hídricas en donde afecta a los animales y por ende a las personas que los consumen.

Dentro de las conclusiones de la investigación, **los estudiosos aseguraron que urge buscar medidas que controlen la minería y prohíban prácticas que no aseguran la vida y la seguridad alimentaria de las personas que habitan este territorio. **Le puede interesar: [“El Amazonas nos reclama”](https://archivo.contagioradio.com/comunidades-se-reunen-para-defender-el-amazonas/)

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
