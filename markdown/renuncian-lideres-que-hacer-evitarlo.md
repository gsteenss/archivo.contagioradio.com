Title: ¿Por qué renuncian los líderes sociales y qué hacer para evitarlo?
Date: 2018-08-22 15:46
Category: DDHH, Movilización
Tags: Amenazas a defensores de Derechos Humnaos, Defensoría del Pueblo, lideres sociales, protección
Slug: renuncian-lideres-que-hacer-evitarlo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/08/lideres-sociales-renuncian.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Luis Galindo] 

###### [22 Ago 2018 ] 

Tras conocerse la **renuncia de 3 integrantes de la Mesa de Víctimas de Malambo,** Atlántico, y **el cese de actividades de 110 líderes sociales en Antioquia** a lo largo del año **por las constantes amenazas contra su vida,** el Abogado y miembro de la Coordinación Colombia-Europa-Estados Unidos (COEUROPA), Alberto Yepes, afirmó que lo peor que le puede pasar a una sociedad es perder esas voces.

Ante las múltiples amenazas que llegan por diversos medios y provenientes de diferentes actores, existe un escenario de riesgo inminente para los líderes sociales, hecho que es preocupante y debería llamar la atención del nuevo Gobierno, pero según Yepes, no se está afrontando con las medidas necesarias para proteger a los defensores de Derechos Humanos y en consecuencia a la democracia. (Le puede interesar: ["110 líderes sociales han renunciado a su labor en Antioquia por amenazas"](https://archivo.contagioradio.com/110-lideres-sociales-han-renunciado/))

**El Abogado señaló que sí el Gobierno no toma acciones eficaces de protección a quienes defienden los Derechos Humanos, la implementación del proceso de paz estaría en riesgo;** y adicionalmente, provocaría la ausencia de voces que denuncien la arbitrariedad, la injusticia, la impunidad y apoyen las comunidades en su demanda de satisfacción de necesidades.

### **La renuncia de líderes sociales demuestra que hay una sociedad que no ha respondido de forma concreta a su genocidio** 

Para el integrante de la COEUROPA, la ausencia de los liderazgos en las comunidades, "sería el reino en el que quienes cometen todo tipo de atrocidades se sentirían a gusto", y al tiempo, se convertiría en una sociedad autoritaria. (Le puede interesar: ["Bajo constante amenaza viven líderes sociales en Cauca"](https://archivo.contagioradio.com/bajo-amenaza-viven-lideres-en-cauca/))

Aunque Yepes resalta que desde las organizaciones sociales "se han hecho llamados permanentemente con el fin de tomar medidas de protección", y la Mesa Nacional de Garantías ha sido un espacio importante para tratar estos temas, todas las instituciones estatales deberían actuar garantizando la vida digna de los líderes. (Le puede interesar: ["En más del 90% de casos de asesinatos de líderes sociales aún no se alcanza justicia"](https://archivo.contagioradio.com/en-casos-de-lideres-sociales-no-se-alcanza-justicia/))

Adicionalmente, para el abogado las renuncias de los líderes sociales a sus procesos son una muestra de que "tenemos una sociedad enferma, que no ha respondido de forma concreta ante el asesinato de defensores de derechos humanos, que según la Defensoría del Pueblo, desde enero de 2016 hasta hoy completa **342 líderes asesinados**", **lo que constituye un verdadero genocidio.** (Le puede interesar: ["Águilas Negras amenazan a líderes sociales en Ciudad Bolivar"](https://archivo.contagioradio.com/aguilas-negras-amenaza-ciudad-bolivar/))

### **No nos podemos dejar derrotar por la desesperanza** 

Para Yepes, **es fundamental formular medidas que desmantelen las estructuras que atentan contra los líderes sociales,** y la ciudadanía debe continuar reclamando las condiciones para que los liderazgos sean protegidos. Adicionalmente, el abogado cree que es necesario respaldar sus procesos y luchas, porque finalmente, la defensa de los derechos humanos es una "suma de voluntades".

El integrante de COEUROPA concluyó que "el contexto demanda una movilización social para apoyar los procesos sociales. **No nos podemos dejar derrotar por la desesperanza".** (Le puede interesar: ["Aumenta el control paramilitar en el departamento de Antioquia"](https://archivo.contagioradio.com/aumenta-el-control-paramilitar-en-antioquia/))

<div class="permalink-in-reply-tos" data-component-term="in_reply_to">

</div>

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio](http://bit.ly/1ICYhVU)]
