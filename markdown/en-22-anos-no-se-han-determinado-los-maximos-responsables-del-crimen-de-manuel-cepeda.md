Title: En 22 años no se han determinado los máximos responsables del crimen de Manuel Cepeda
Date: 2016-08-09 17:29
Category: DDHH, Entrevistas
Tags: colectivo de Abogados José Alvear Restrepo, Crimen de Manuel Cepeda Vargas, Iván Cepeda, Manuel Cepeda Vargas
Slug: en-22-anos-no-se-han-determinado-los-maximos-responsables-del-crimen-de-manuel-cepeda
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/08/Manuel-Cepeda-Vargas.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: El Espectador ] 

###### [9 Ago 2016] 

La sentencia de la Corte Interamericana en el caso de Manuel Cepeda Vargas determinó la responsabilidad del Estado colombiano en su asesinato, un avance la búsqueda de verdad que no ha tenido correspondencia en el sistema judicial colombiano, pues según afirma la abogada Soraya Gutiérrez, del Colectivo José Alvear Restrepo, los **lentos avances no han permitido determinar los máximos responsables del crimen** y sólo se ha logrado llegar a los mandos medios y bajos de las estructuras militares y paramilitares que orquestaron el homicidio.

En este crimen tuvieron que ver altos mandos del Ejército Nacional, asegura la abogada, y agrega que antes de que se cometiera el asesinato se tenía conocimiento de la campaña de exterminio que se había puesto en marcha contra militantes de la Unión Patriótica, y pese a que la situación se denunció ante las autoridades del orden nacional, no se determinaron acciones para proteger la vida e integridad de los líderes; por lo que queda claro que **hubo coordinación de actores estatales y paramilitares**, pero hace falta llegar a los mayores responsables tanto por omisión como por acción.

En el marco de la investigación por este crimen, el paramilitar Luis Emilio Pereira, alias 'Huevo de pizca', dio detalles sobre las circunstancias del modo, tiempo y lugar en el que se cometió el homicidio y aseguró ser el enlace entre Carlos Castaño y Francisco Santos; sin embargo, **no se ha logrado que alguno de los altos funcionarios estatales sean involucrados en el proceso**. La abogada concluye asegurando que la decisión de sí el caso de Manuel Cepeda se lleva a la [[Jurisdicción Especial para la Paz](https://archivo.contagioradio.com/jurisdiccion-especial-para-la-paz/)] dependerá de su familia.

Por su parte el Senador Iván Cepeda asevera que no se trata solamente del esclarecimiento de la verdad y la justicia, sino de la restitución total de toda la bancada parlamentaria de la Unión Patriótica que fue víctima del genocidio, que incluye la restitución de los lugares que tenía el partido político en distintas corporaciones públicas y **no se deben confundir con la participación política de las FARC-EP** pues son dos temas diferentes que deben ser solucionados en el escenario de los Diálogos de Paz.

"Mi padre perteneció a una generación de líderes políticos (...) con una vocación indeclinable para **poner todas sus capacidades y posibilidades al servicio de un cambio político y social** y de una permanente protección de los derechos de los más necesitados, esa fue su convicción y el motivo de las acciones políticas que a muchos les costaron la vida", afirma el Senador y concluye asegurando que durante estos 22 años ha abonado un gran camino en la defensa de las víctimas y en la construcción de paz en Colombia, tal como lo hizo su papá.

<iframe src="http://co.ivoox.com/es/player_ej_12493418_2_1.html?data=kpehm5iYdZmhhpywj5aVaZS1k5eah5yncZOhhpywj5WRaZi3jpWah5yncbTj08bmw5CrudXdxtffx9-JdqSfpKa3o7eRaZi3jqjc0NnFq8rjjLfOxs7Tb46ZmKialg..&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe><iframe src="http://co.ivoox.com/es/player_ej_12493476_2_1.html?data=kpehm5iYe5ehhpywj5aZaZS1lJWah5yncZOhhpywj5WRaZi3jpWah5yncarqwtOYpcrUqcXVjKjO1dnWs4-hhpywj6jTstXVyM7cjbfFqMrjjJKSmaiReA..&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU) ]] 
