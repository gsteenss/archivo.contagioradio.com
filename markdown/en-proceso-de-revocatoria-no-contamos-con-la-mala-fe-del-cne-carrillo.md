Title: En proceso de revocatoria “No contamos con la mala fe del CNE” Carrillo
Date: 2017-09-11 16:22
Category: Entrevistas, Movilización
Tags: Alcaldía de Peñalosa, Revocatoria a Peñalosa
Slug: en-proceso-de-revocatoria-no-contamos-con-la-mala-fe-del-cne-carrillo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/09/CNE-Colombia.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: La Razón] 

###### [11 Sept 2017] 

Luego de conocerse que el Concejo Nacional Electoral podría anular las cuentas del comité Unidos Revocaremos a Peñalosa y con eso darle un freno al avance de la revocatoria que el Tribunal Administrativo de Cundinamarca pidió acelerar, Carlos Carrillo uno de los integrantes del comité afirmó que **no contaron con la mala fe del CNE y que esa instancia está actuando de acuerdo a sus intereses políticos.**

El freno se provocó debido a que el CNE establece topes para los aportes de particulares a las campañas de revocatoria en 41 millones de pesos, y en este caso, Sintrateléfonos, uno de los aportantes a la revocatoria habría donado una cifra cercana a lo**s 49 millones 580 mil pesos, de acuerdo al portal web La Silla Vacía**. Carrillo explica que un porcentaje de ese aporte sería en calidad de préstamo y por ello no se estaría violando el tope, sin embargo, el CNE ve cifras globales.

Además, el máximo estimado de aportes estaría en los **450 millones de pesos y en la campaña solo se invirtieron 111 millones**, sin embargo, tendría que demostrarse con cuentas ordenadas lo que argumenta el comité y esperar la votación, que se espera sea desfavorable para poder explicar de otra manera el aporte del sindicato.

La ponencia negativa también estaría compuesta por facturas que no se presentaron y movimientos de dinero previos al inicio de la campaña, no obstante, en ninguna otra revisión se han puesto los énfasis que el CNE le ha hecho a la campaña por la revocatoria. (Le puede interesar: ["No hay excusa para seguir dilatando la revocatoria:Unidos Revocaremos a Peñalosa"](https://archivo.contagioradio.com/no-hay-excusa-para-dilatar-revocatoria/))

"Hay algo evidente en ese informe y es la mala fe del Consejo Nacional Electoral, se están rasgando las vestiduras porque no tienen clara cuál la procedencia de unas fotocopias y es el mismo organismo que no vio cuándo ingresaron **4 millones de dorales que entraron a la campaña presidencial**” afirmó Carrillo.

Además, agregó que una de las primeras cosas que hay que tener en cuenta es el carácter político del CNE que desde el principio de la campaña ha maniobrado para tratar de impedir que se desarrolle la consulta ciudadana y se ha aprovechado de los vacíos en la ley en cuanto al tiempo que tendría para revisar y avalar las cuentas, ya que en esos casos no existe un tope de tiempo, **“la única manera de hacer que el CNE se dé prisa con sus decisiones o con el estudio de la respuesta, es a través de la movilización ciudadana**”.

### **En qué va la revocatoria del Alcalde Peñalosa** 

Una vez los comités de revocatoria a Enrique Peñalosa recolectaron las más de 400 mil firmas, que fueron validadas en la Registraduría Nacional, el alcalde inició un proceso para revisar cada una de las firmas, para ese entonces se tenía un plazo de **15 días en donde se realizaría este proceso**, sin embargo, poco tiempo después el CNE manifestó la revisión en los gastos de los comités.

Luego de una denuncia impuesta por el comité Unidos Revocaremos a Peñalosa, en donde le decían a la Registraduría que debía pronunciarse sobre los plazos para el estudio de las firmas, el Tribunal Superior de Cundinamarca falló a favor del comité y le manifestó a la Registraduría que debía expedir el certificado del cumplimiento de los requisitos para la revocatoria.

Actualmente la Resgitraduría expreso que impugnará esta decisión, debido a que aún no finaliza el proceso de revisión grafológica a las más de 400 mil firmas. (Le puede interesar: ["Revocatoria a Peñalosa podría realizarse en Noviembre"](https://archivo.contagioradio.com/revocatoria-a-enrique-penalosa-podria-realizarse-en-noviembre/))

<iframe id="audio_20809168" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_20809168_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
