Title: Saldo en rojo para los derechos civiles y políticos en Colombia
Date: 2016-10-19 15:34
Category: Nacional, Paz
Tags: CCEEUU, Comite de Derechos Humanos, derechos civiles, Derechos Humanos, Estado Colombiano, Ginebra, Libertad de expresión, paz
Slug: saldo-en-rojo-para-los-derechos-civiles-y-politicos-en-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/12/SALDO-EN-ROJO-PARA-LOS-DERECHOS-CIVILES-Y-POLÍTICOS-EN-COLOMBIA-12.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Panorama] 

###### [19 de Oct 2016] 

Así lo asegura un reciente informe publicado por la Coordinación Colombia Europa Estados Unidos (CCEEU), a propósito de la cita que se darán Estado y Organizaciones en Ginebra para realizar el séptimo examen de Colombia ante el Comité de Derechos Humanos, órgano de las Naciones Unidas que supervisa la aplicación del Pacto Internacional de Derechos Civiles y Políticos, ratificado por Colombia en 1969.

Pese a los avances que muestra el informe que fue presentado por el Estado al Comité, en el que asegura se han emprendido acciones encaminadas a cumplir las recomendaciones dadas a Colombia en el 2010, **las preocupaciones de los expertos y la información facilitada por organizaciones de la sociedad civil, muestran que no han sido suficientes dichos esfuerzos para garantizar los derechos civiles y políticos de los colombianos así como para prevenir y sancionar sus violaciones.** Le puede interesar: [19 defensores de derechos humanos fueron asesinados de julio a septiembre de 2016](https://archivo.contagioradio.com/19-defensores-de-derechos-humanos-fueron-asesinados-de-julio-a-septiembre/).

Dicho informe presentado  por la CCEEUU, asegura que entre los años 2010 y 2015- periodos en los cuales ha estado electo el presidente Juan Manuel Santos- **Colombia ha fallado en cerca de diez temas que son de suma importancia para dar cumplimiento a las recomendaciones**, tales como adoptar medidas por el Estado en relación con obligaciones en materia de derechos humanos, persistencia de conflicto armado, derecho a la vida, la desigualdad en el ejercicio de los derechos que enfrentan las mujeres y la población LGBT, las violaciones del derecho a la vida, la trata de personas, la explotación laboral y las precarias condiciones de reclusión de las personas privadas de libertad. Le recomendamos: [Paramilitares amenazan a organizaciones de derechos humanos del Valle del Cauca](https://archivo.contagioradio.com/organizaciones-ddhh-del-valle-del-cauca-amenazadas-por-paramilitares/).

En referencia al examen, **las organizaciones aseguran que debido al contexto por el que atraviesa Colombia, esta cita en Ginebra puede ser una de las formas de exigir al Estado colombiano celeridad en el cumplimiento de los derechos civiles y políticos** “hacer un llamado al Estado para avanzar en una pronta solución que permita iniciar la  fase de implementación manteniendo en todo momento a las víctimas y sus derechos en el centro del Acuerdo –de Paz-” dice un comunicado entregado por la CCEEUU. Leer más en: [Los derechos humanos un camino para la construcción de paz](https://archivo.contagioradio.com/los-derechos-humanos-un-camino-para-la-construccion-de-paz/).

**Los resultados y las recomendaciones que hará al Estado el Comité de Derechos Humanos será publicado el próximo 4 de noviembre,** último día de su 118.ª sesión en las que también estarán presentes Azerbaiyán, Eslovaquia, Jamaica, Marruecos, Moldavia, y Polonia.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
