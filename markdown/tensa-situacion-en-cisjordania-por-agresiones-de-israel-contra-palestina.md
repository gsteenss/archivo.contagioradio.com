Title: Tensa situación en Cisjordania por agresiones de Israel contra Palestina
Date: 2015-09-23 12:53
Category: DDHH, El mundo
Tags: Crisis humanitaria, Derecho al rezo, Derechos Humanos, Guerra Santa, Israel, Mezquita Al-Aqsa, ONU, Operación Margen Protector, Palestina, Radio derechos Humanos
Slug: tensa-situacion-en-cisjordania-por-agresiones-de-israel-contra-palestina
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/Mezquita.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: hispantv.com 

###### [23 Sep 2015]

El embajador palestino, Riyad Mansur denunció ante la ONU **nuevas provocaciones por parte de Israel en lugares sagrados de Jerusalén** e indicó que se están dando motivos para que se intensifiquen los enfrentamientos entre Palestina e Israel.

Israel que el día de hoy celebra el ” Yom Kipur” **restringió el acceso a la mezquita de Al Aqsa**, dando paso únicamente para hombres mayores de 50 años y mujeres de cualquier edad, desde el pasado lunes hasta nuevo aviso.

También en la frontera de Jerusalén Este, **desplegó miles de agentes de policía**, para evitar que se continúen presentando disturbios como el de la semana pasada en que decenas de palestinos y al menos cuatro oficiales de la Policía de Fronteras israelíes, resultaron **heridos por enfrentamientos entre fuerzas israelíes y palestinos**.

El embajador palestino ha indicado que pese a que la comunidad internacional ha pedido que **cesen las violaciones de derechos a los palestinos**, incluido su derecho al rezo, Israel “*ha hecho exactamente lo contrario*”, generando un ambiente poco propicio para la tranquilidad de las naciones y provocando una nueva intervención militar en territorio palestino.

Esta situación se da en el marco de protestas en los territorios ocupados de Jerusalén Este y Cisjordania, que se desencadenaron tras choques y restricciones en la mezquita de Al Aqsa.
