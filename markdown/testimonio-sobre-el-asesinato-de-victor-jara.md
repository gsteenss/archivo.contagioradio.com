Title: Testimonio sobre el asesinato de Víctor Jara
Date: 2017-09-18 13:28
Category: Uncategorized
Tags: Dictadura de Chile, Victor Jara
Slug: testimonio-sobre-el-asesinato-de-victor-jara
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/09/victor.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:Nodal] 

###### [17 Sept 2018]

### **¡A ese hijo de puta me lo traen para acá!”**

Gritó el oficial apuntando con su dedo a Víctor Jara, quien junto a unos 600 profesores y estudiantes de la UTE ingresábamos prisioneros con las manos en la nuca y a punta de bayonetas y culatazos al Estadio Chile, la tarde del miércoles 12 de septiembre de 1973. Era el día siguiente del golpe fascista. El día antes, el 11, Víctor debía cantar en el acto que se realizaría en la UTE, donde nuestro rector Enrique Kirberg recibiría al presidente Allende, quien anunciaría el llamado a plebiscito al pueblo de Chile. Sin embargo, la voz de Allende fue apagada en La Moneda en llamas y la guitarra de Víctor quedaría allí, destrozada por la bota militar en el bombardeo de la UTE, como testimonio más de la barbarie fascista.“¡A ese hijo de puta me lo traen para acá!”. Repitió iracundo el oficial. Casco hasta los ojos, rostro pintado, metralleta al hombro, granada al pecho, pistola y corvo al cinto, balanceando su cuerpo tensado y prepotente sobre sus botas negras.

“¡A ese huevón! ¡A ése!”. El soldado lo empuja sacándolo de la fila. “¡No me lo traten como señorita, carajo!”. Ante la orden, el soldado levanta su fusil y le da un feroz culatazo en la espalda de Víctor. Víctor cae de bruces, casi a los pies del oficial.

“¡Che, tu madre! Vos sos el Víctor Jara huevón. El cantor marxista ¡El cantor de pura mierda!”. Y, entonces, su bota se descarga furibunda una, dos, tres, 10 veces en el cuerpo, en el rostro de Víctor, quien trata de protegerse la cara con sus manos (ese rostro que cada vez que lo levanta esboza esa sonrisa, que nunca lo abandonó hasta su muerte). Esa misma sonrisa grande con que cantó desde siempre al amor y a la revolución.

“Yo te enseñaré hijo de puta a cantar canciones chilenas, ¡no comunistas!”.

El golpe de una bota sobre un cuerpo indefenso no se olvida jamás. El oficial sigue implacable su castigo, enceguecido de odio, lo increpa y patea. La bota maldita se incrusta en la carne del cantor. Nosotros, apuntados por los fusiles contemplamos con horror la tortura de nuestro querido trovador y pese a la orden de avanzar nos quedamos transidos frente al horror. Víctor yace en el suelo. Y no se queja. Ni pide clemencia. Sólo mira con su rostro campesino al torturador fascista. Este se desespera. Y de improviso desenfunda su pistola y pensamos con pavor que la descerrajará sobre Víctor. Pero, ahora le golpea con el cañón del arma, una y otra vez. Grita e increpa. Es histeria fascista.

Y, entonces, la sangre de Víctor comienza a empaparle su pelo, a cubrirle su frente, sus ojos. Y la expresión de su rostro ensangrentado se nos quedaría grabada para siempre en nuestras retinas.El oficial se cansa y de pronto detiene sus golpes. Mira a su alrededor y advierte los cientos de ojos testigos que en una larga hilera lo observan con espanto y con ira. Entonces, se descompone y vocifera.“¿Qué pasa huevones? ¡Que avancen estas mierdas¡ Y a este cabrón' se dirige a un soldado, “me lo pones en ese pasillo y al menor movimiento, lo matas! ¿Entendiste? ¡Carajo!

El Estadio Chile se iba llenando rápidamente con prisioneros políticos. Primero, 2 mil, luego seríamos más de 5 mil. Trabajadores heridos, ensangrentados, descalzos, con su ropa hecha jirones, bestialmente golpeados y humillados. El golpe fascista tuvo allí, como en todas partes, una bestialidad jamás vista. Las voces de los oficiales azuzando a los soldados a golpear, a patear, a humillar esta “escoria humana”, a la “cloaca marxista”, como lo espetan.

Hasta hoy día la gente nos pregunta si los miles de prisioneros del estadio presenciaron estas torturas de Víctor y la respuesta es que sólo unos pocos, sus compañeros de la UTE y los más cercanos, ya que el destino y la vida de cada uno estaba en juego y, además, el Estadio Chile era un multiescenario del horror, de la bestialidad más despiadada.

Allí arriba un oficial le cortaba la oreja con su corvo a un estudiante peruano, acusándolo por su piel morena de ser cubano. Allá, un niño de unos 12 años, de repente se levanta de su asiento y llamando a su padre corre enloquecido entre los prisioneros y un soldado le descarga su ametralladora. De pronto un soldado tropieza en las graderías con el pie de un obrero viejo y El príncipe, que así se hacía llamar uno de los oficiales a cargo, desde lo alto de los reflectores que nos enceguecían, le ordena que le golpee y el soldado toma el fusil por su cañón y quiebra su culata en la cabeza del trabajador, que se desangra hasta morir. Un grito de espanto nos sobrecoge. Desde lo alto de la gradería, un trabajador enloquecido se lanza al vacío al grito de ¡Viva Allende! y su cuerpo estalla en sangre en la cancha del estadio. Enceguecidos por los reflectores y bajo los cañones de las ametralladoras, llamadas “las sierras de Hitler”, siguen llegando nuevos prisioneros.

Víctor, herido, ensangrentado, permanece bajo custodia en uno de los pasillos del Estadio Chile. Sentado en el suelo de cemento, con prohibición de moverse. Desde ese lugar, contempla el horror del fascismo. Allí, en ese mismo estadio que lo aclamó en una noche del año 69 cuando gana el Primer Festival de la Nueva Canción Chilena, con su Plegaria de un labrador:

Levántate

Y mírate las manos

Para crecer, estréchala a tu hermano

Junto iremos unidos en la sangre

Hoy es el tiempo que puede ser mañana.

Juntos iremos unidos en la sangre

Ahora y en la horade nuestra muerte, amen.

Allí es obligado a permanecer la noche del miércoles 12 y parte del jueves 13, sin ingerir alimento alguno, ni siquiera agua. Víctor tiene varias costillas rotas, uno de sus ojos casi reventado, su cabeza y rostro ensangrentados y hematomas en todo su cuerpo. Y estando allí, es exhibido como trofeo por el oficial superior y por El príncipe ante las delegaciones de oficiales de las otras ramas castrenses y cada uno de ellos hace escarnio del cantor.

La tarde del jueves se produce un revuelo en el estadio. Llegan buses de la población La Legua. Se habla de enfrentamiento. Y bajan de los buses muchos presos, heridos y también muchos muertos. A raíz de este revuelo, se olvidan un poco de Víctor. Los soldados fueron requeridos a la entrada del estadio.

Entonces, aprovechamos para arrastrar a Víctor hasta las graderías. Le damos agua. Le limpiamos el rostro. Eludiendo la vigilancia de los reflectores y las “punto 50”, nos damos a la tarea de cambiar un poco el aspecto de Víctor. Queremos disfrazar su estampa conocida. Que pase a ser uno más entre los miles. Un viejo carpintero de la UTE le regala su chaquetón azul para cubrir su camisa campesina. Con un cortauñas le cortamos un poco su pelo ensortijado. Y cuando nos ordenan confeccionar listas de los presos para el traslado al Estadio Nacional, también disfrazamos su nombre y le inscribimos con su nombre completo: Víctor Lidio Jara Martínez. Pensábamos, con angustia, que si llegábamos con Víctor al Nacional, y escapábamos de la bestialidad fascista del “Chile”, podríamos, tal vez, salvar su vida.

Un estudiante nuestro ubica a un soldado conocido, le pide algo de alimento para Víctor. El soldado se excusa, dice que no tiene, pero más tarde aparece con un huevo crudo, lo único que pudo conseguir y Víctor toma el huevo y lo perfora con un fósforo en los dos extremos y comienza a chuparlo y nos dice, recuperando un tanto su risa y su alegría, “en mi tierra de Lonquén así aprendí a comer los huevos”. Y duerme con nosotros la noche del jueves, entre el calor de sus compañeros de infortunio y, entonces, le preguntamos que haría él, un cantor popular, un artista comprometido, un militante revolucionario, ahora en dictadura y su rostro se ensombrece previendo, quizás, la muerte. Hace recuerdos de su compañera, Joan, de Amanda y Manuela, sus hijas y del presidente Allende, muerto en La Moneda, de su amado pueblo, de su partido, de nuestro rector y de sus compañeros artistas. Su humanidad se desborda aquella fría noche de septiembre.

El viernes 14 estamos listos para partir al Nacional. Los fascistas parecen haberse olvidado de Víctor. Nos hacen formar para subir a unos buses, manos en alto y saltando. Y las bayonetas clavándonos. En el último minuto, una balacera nos vuelve a las graderías.

### **Fatídico 15-IX-73** 

Y llegamos al fatídico sábado 15 de septiembre de 1973. Cerca del mediodía tenemos noticias que saldrán en libertad algunos compañeros de la UTE. Frenéticos empezamos a escribirles a nuestras esposas, a nuestras madres, diciéndoles solamente que estábamos vivos. Víctor sentado entre nosotros me pide lápiz y papel. Yo le alcanzo esta libreta, cuyas tapas aún conservo. Y Víctor comienza a escribir, pensamos en una carta a Joan su compañera. Y escribe, escribe, con el apremio del presentimiento. De improviso, dos soldados lo toman y lo arrastran violentamente hasta un sector alto del estadio, donde se ubica un palco, gradería norte. El oficial llamado El príncipe tenía visitas, oficiales de la Marina. Y desde lejos vemos como uno de ellos comienza a insultar a Víctor, le grita histérico y le da golpes de puño. La tranquilidad que emana de los ojos de Víctor descompone a sus cancerberos. Los soldados reciben orden de golpearlo y comienzan con furia a descargar las culatas de sus fusiles en el cuerpo de Víctor. Dos veces alcanza a levantarse Víctor, herido, ensangrentado. Luego no vuelve a levantarse. Es la última vez que vemos con vida a nuestro querido trovador. Sus ojos se posan por última vez, sobre sus hermanos, su pueblo mancillado.

Aquella noche nos trasladan al Estadio Nacional y al salir al foyer del Estadio Chile vemos un espectáculo dantesco. Treinta o cuarenta cuerpos sin vida están botados allí y entre ellos, junto a Litre Quiroga, director de Prisiones del Gobierno Popular, también asesinado, el cuerpo inerte y el pecho perforado a balazos de nuestro querido Víctor Jara. 42 balas. La brutalidad fascista había concluido su criminal faena. Era la noche del sábado 15 de septiembre. Al día siguiente su cadáver ensangrentado, junto a otros, sería arrojado cerca del Cementerio Metropolitano.Esa noche, entre golpes y culatazos ingresamos prisioneros al Estadio Nacional. Y nuestras lágrimas de hombres quedaron en reguero, recordando tu canto y tu voz, amado Víctor, Víctor del pueblo:

.Yo no canto por cantar

Ni por tener buena voz

Canto porque la guitarra

Tiene sentido y razón.

Que no es guitarra de ricos

Ni cosa que se parezca

Mi canto es de los andamios

Para alcanzar las estrellas

Esa misma noche, ya en el Nacional, lleno de prisioneros, al buscar una hoja para escribir, me encontré en mi libreta, no con una carta, sino con los últimos versos de Víctor, que escribió unas horas antes de morir y que el mismo tituló Estadio Chile, conteniendo todo el horror y el espanto de aquellas horas. Inmediatamente acordamos guardar este poema. Un zapatero abrió la suela de mi zapato y allí escondimos las dos hojas del poema. Antes, yo hice dos copias de él, y junto al exsenador Ernesto Araneda, también preso, se las entregamos a un estudiante y a un médico que saldrían en libertad.Sin embargo, el joven es revisado por los militares en la puerta de salida y le descubren los versos de Víctor. Lo regresan y bajo tortura obtienen el origen del poema. Llegan a mí y me llevan al Velódromo, transformado en recinto de torturas e interrogatorios.

Me entregan a la FACh y tan pronto me arrojan de un culatazo a la pieza de tortura, el oficial me ordena sacarme el zapato donde oculto los versos. “¡Ese zapato, cabrón!”. Grita furibundo. Su brutalidad se me viene encima. Golpea el zapato hasta hacer salir las hojas escritas. Mi suerte estaba echada. Y comienzan las torturas, patadas, culatazos y la corriente horadando las entrañas, torturas destinadas a saber si existían más copias del poema. Y ¿por qué a los fascistas les interesaba el poema? Porque a cinco días del golpe fascista en Chile, el mundo entero, estremecido, alzaba su voz levantando las figuras y los nombres señeros de Salvador Allende y Víctor Jara y, en consecuencia, sus versos de denuncia, escritos antes del asesinato, había que sepultarlos.

Pero quedaba otra copia con los versos de Víctor, que esa noche debía salir del estadio.Entonces, se trataba de aguantar el dolor de la tortura. De la sangre. Yo sabía que cada minuto que soportara las flagelaciones en mi cuerpo, era el tiempo necesario para que el poema de Víctor atravesara las barreras del fascismo. Y, con orgullo debo decir que los torturadores no lograron lo que querían. Y una de las copias atravesó las alambradas y voló a la libertad y aquí están algunos versos de Víctor, de su último poema, Estadio Chile:

Somos cinco mil

En esta pequeña parte de la ciudad.

Somos cinco mil

¿Cuántos seremos en total

en las ciudades y en todo el país?

¡Cuanta humanidad,

hambre, frío, pánico, dolor, presión moral, terror y locura!

Somos diez mil manos menos que no producen

¿Cuántos somos en toda la Patria?

La sangre del compañero Presidente

golpea más fuerte que bombas y metrallas

Así golpeará nuestro puño nuevamente.

Estos versos recorrieron todo el planeta. Y las canciones de Víctor, de amor y rebeldía, de denuncia y compromiso, siguen conquistando a los jóvenes de todos los rincones de la Tierra.

El oficial fascista que ordenó acribillarlo debió quedar contento con su crimen, pensando que había silenciado la voz del cantor, sin saber que hay poetas y cantores como Víctor Jara que no mueren, que mueren para vivir, y que su voz y su canto seguirán vivos para siempre en el corazón de los pueblos.......

Testimonio sobre el asesinato de Víctor Jara de Boris Navía Pérez, en octubre del año 2003, en la inauguración de una escultura
