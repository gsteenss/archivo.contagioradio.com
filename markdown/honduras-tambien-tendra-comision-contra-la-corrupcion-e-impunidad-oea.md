Title: Honduras tendrá comisión contra la corrupción e impunidad: OEA
Date: 2015-10-04 07:54
Category: El mundo, Otra Mirada
Tags: honduras, Instituto de Seguridad Social, Misión contra corrupción e impunidad en Honduras, OEA, Organización Estados Americanos, Protestas en Honduras por corrupción
Slug: honduras-tambien-tendra-comision-contra-la-corrupcion-e-impunidad-oea
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/10/Marcha-de-las-antorchas.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: laprensa.hn 

###### [2 oct 2015]

La **Organización de Estados Americanos** (OEA) anunció que enviará una **misión de apoyo** a Honduras para estudiar los **casos de corrupción e impunidad** que se presentan en el país centroamericano.

Esta misión sería similar al CICIG que funciona en Guatemala y que logró la captura del presidente Otto Pérez Molina, el secretario general de la OEA, Luis Almagro, indicó que el objetivo de ésta comisión es "*mejorar la calidad de los servicios que presta el aparato de justicia en Honduras*".

La OEA toma esta decisión frente a la petición del mismo presidente hondureño, Juan Orlando Hernández, por el caso del **desfalco de los 300 millones de dólares del Instituto Hondureño de Seguridad Social** en el cual está involucrados altos exfuncionarios y funcionarios de su gobierno.

Almagro indicó que "e*sta misión de apoyo que será enviada contra la corrupción y la impunidad trabajará con juristas nacionales a fin de fortalecer el sistema judicial y asesorar a los órganos de control hondureños*".

La comisión **se presentaría el 12 de octubre** luego de cuatro meses de protestas del pueblo hondureño por el escándalo del Instituto de Seguridad Social.
