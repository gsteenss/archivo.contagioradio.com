Title: "La democracia y la soberanía están en cuarentena"
Date: 2020-05-29 12:01
Author: AdminContagio
Category: Actualidad, Política
Tags: Ejército, Gobierno, soberanía
Slug: la-democracia-y-la-soberania-estan-en-cuarentena
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/05/Ejercito-de-EE.UU_.-y-soberanía-de-Colombia.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:heading {"align":"right","level":6,"textColor":"cyan-bluish-gray"} -->

###### Foto: CCO {#foto-cco .has-cyan-bluish-gray-color .has-text-color .has-text-align-right}

<!-- /wp:heading -->

<!-- wp:paragraph -->

Activistas, líderes políticos y organizaciones defensoras de derechos humanos criticaron el [anuncio](https://co.usembassy.gov/es/mision-sfab-viene-a-colombia/) hecho por la embajada de los Estados Unidos en Colombia respecto a la llegada de una brigada de Asistencia de Fuerza de Seguridad (SFAB, por sus siglas en inglés) para apoyar la "lucha contra narcóticos". Entre otras razones, sobre la decisión se cuestiona lo que ello significa para la sobernía de Colombia y Venezuela.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### La llegada de tropas extranjeras: democracia y soberanía en cuarentena

<!-- /wp:heading -->

<!-- wp:paragraph -->

Alberto Yepes, coordinador del observatorio de DD.HH. y Derecho Internacional Humanitario de la Coordinación Colombia-Europa-EE.UU. (COEUROPA), afirma que la situación es preocupante porque el anuncio lo hizo la embajada antes que el propio Gobierno de Colombia, "lo cual demuestra la poca capacidad de decisión sobre asuntos soberanos que tiene este gobierno".

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En segundo lugar, parece un anuncio irreversible, en tanto se produce faltan apenas tres días para la llegada de las tropas (que arribarían el próximo lunes). Y en tercer lugar, el anuncio no estuvo precedido de un debate en el congreso. (Le puede interesar:["Represión y circo: elementos de la post-pandemia en Colombia"](https://archivo.contagioradio.com/represion-y-circo-elementos-de-la-post-pandemia-en-colombia/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En ese sentido, Yepes recuerda que el [artículo 173](https://www.constitucioncolombia.com/titulo-6/capitulo-4/articulo-173) de la Constitución exige como requisito que cualquier presencia de tropas extranjeras en territorio colombiano debe contar con un debate y autorización previa del senado. "Esto no se ha hecho y creemos que es una de las razones por las que el presidente Duque ha entorpecido el funcionamiento del Congreso: creando unas reglas que impiden su operación presencial".

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

De esta manera, para el experto, tendremos una situación en la que el Congreso no hace su labor de control político y en la que "la democracia y la soberanía están en cuarentena", mientras el presidente autoriza el arribo de tropas de forma autoritaria. (Le puede interesar: ["Proyecto de Renta Básica de Emergencia contaría con mayorías en el congreso"](https://archivo.contagioradio.com/proyecto-de-renta-basica-de-emergencia-contaria-con-mayorias-en-el-congreso/))

<!-- /wp:paragraph -->

<!-- wp:heading -->

Del Plan Colombia a la nueva 'guerra contra las drogas'
-------------------------------------------------------

<!-- /wp:heading -->

<!-- wp:paragraph -->

Yepes recuerda que el motivo señalado por la embajada para la llegada de las tropas es la 'guerra contra el narcotráfico' en momentos en que el Acuerdo de Paz impulsaba un enfoque distinto al de la confrontación, mediante el Plan Nacional Integral de Sustitución (PNIS). (Le puede interesar: ["Ejército quería que Ariolfo Sánchez fuera un "falso positivo": Comunidad de Anori, Antioquia"](https://archivo.contagioradio.com/ejercito-queria-que-ariolfo-sanchez-fuera-un-falso-positivo-comunidad-de-anori-antioquia/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Adicionalmente, el experto manifiesta que en momentos de pandemia, dicho programa ha sido desmantelado y se está aprovechando para desatar una campaña agresiva contra las comunidades sembradoras en departamentos como Norte de Santander, Antioquia, Cauca, Nariño, Córdoba o Putumayo.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/ovesede/status/1266013439765381121","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/ovesede/status/1266013439765381121

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

Pero la preocupación va más allá del cambio de enfoque, para el experto, los mismos argumentos a los que se apela en esta ocasión se usaron para impulsar el Plan Colombia, "que inició siendo una estrategia en la guerra contra las drogas pero terminó en una estrategia contrainsurgente". (Le puede interesar:["¿Qué implicó para el Putumayo la aplicación del Plan Colombia?"](https://archivo.contagioradio.com/que-implico-para-el-putumayo-la-aplicacion-del-plan-colombia/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

La explicación estaría en que estas tropas estarían en las llamadas Zonas Futuro, que son zonas de frontera y en las que hay una acción activa del Ejército de Liberación Nacional (ELN), de tal manera que la llegada de un nuevo actor armado podría intensificar el ya existente conflicto que padecen estos territorios.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

La bancada del partido[FARC](https://twitter.com/PartidoFARC/status/1266053854329782273) señaló que la llegada de las tropas podría ser una escala más en una serie de sucesos que han ocurrido en relación con la presencia militar de EE.UU. en amenaza a la soberanía de Venezuela. Por lo tanto, pidieron que no se permita convertir a Colombia en escenario de una confrontación internacional.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

De acuerdo con este planteamiento, Yepes sostiene que alguas Zonas Futuro están en frontera, lo que se podría aprovechar para impulsar eventos militares que podrían generar una situación de confrontación con el vecino país. Ante ello, manifiesta que no es con acciones de guerra como se protege la paz, ni mediante una fuerza extranjera que se defiende la soberanía nacional.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### ¿Qué queda por hacer?

<!-- /wp:heading -->

<!-- wp:paragraph -->

Ante la inminencia de la llegada de las tropas norteamericanas, el defensor de DD.HH. declara que es necesario que los y las ciudadanas ejerzan su derecho y deber a manifestarse sobre esta situación. Mientras se toman las acciones judiciales pertinentes por la vulneración al artículo 173 de la Constitución; en la misma medida, dice que el Congreso tiene que sumir su labor de control y llamar a juicio político al Presidente por estos hechos.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Sobre la llegada de las tropas algunas organizaciones ya se han manifestado, rechazando la nueva militarización extranjera en regiones como Catatumbo y sur de Córdoba. La [Asociación Campesina del Catatumbo (ASCAMCAT)](https://twitter.com/AscamcatOficia/status/1266010360328454150), también campesinos del sur de Córdoba y la [Cumbre Agraria, Campesina, Étnica y Popular](https://www.cumbreagraria.org/defendamos-la-soberania-nacional/) hacen parte de las organizaciones que hicieron el llamado a respetar la soberanía nacional y la labor del Congreso.

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
