Title: Los plebiscitos de 1957 y 2016 (2da parte)
Date: 2016-09-14 09:41
Category: Cesar, Opinion
Tags: Plebiscito, plebiscito por la paz
Slug: los-plebiscitos-de-1957-y-2016-2da-parte
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/09/plebiscito-2016.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: El Universal 

#### **[Cesar Torres del Río ](https://archivo.contagioradio.com/cesar-torres-del-rio/)** 

###### 14 Sep 2016 

**La resistencia ciudadana y de clases es el sello del plebiscito de 2016**[.  La primera ha estado encubierta por la segunda, pero no por ello deja de estar en primer plano.]

[Por los años cincuenta del pasado siglo la resistencia campesina-ciudadana encontró en una variedad de la lucha armada, la guerrillera, la opción para enfrentar la violencia del Estado y de los partidos liberal y conservador. En las urbes la movilización popular y de los trabajadores fue diversa y amplia aunque estrictamente controlada por arriba (Laureano Gómez y Gustavo Rojas Pinilla) con el apoyo de la jerarquía católica, la prensa y los gremios económico-políticos. El cierre del período llegó con el plebiscito de diciembre de 1957, que fue la expresión de la]**impunidad pactada**[ entre los sectores dirigentes de los partidos conservador y liberal.]

[El plebiscito convocado para el 2 de octubre se encuadra histórica y mundialmente en un momento]*[termidoriano]*[, de reacción, de guerra imperialista permanente contra los subalternos, contra los vencidos. Continentalmente, advertimos una evidente ofensiva neoliberal-extractivista de multinacionales y capital nacional que parcialmente ha logrado hacer retroceder a los gobiernos “progresistas” tipo Venezuela.]

[¿Cuál es el significado histórico que tiene el plebiscito?  ¿Qué expresa como acontecimiento?]

[En primer lugar no es la conclusión de una gesta heroica de las elites para alcanzar la paz:  presidentes Betancur, Barco, Gaviria-Constitución del 91, Pastrana y Santos. En tal caso estaríamos ante la versión de la historia tradicional, oficial, historicista-positivista: los “grandes” hombres hacen la historia y el “populacho” los sigue cual rebaño.]

[En segundo lugar, no es “nacional” como excepción. Otros pueblos han padecido la guerra y la terminaron con mecanismos transicionales, restaurativos, de justicia. De esas experiencias se nutrió durante cuatro años el proceso de acuerdo entre las partes; de allí que haya obtenido el beneplácito de importantes sectores de la denominada comunidad internacional, incluida la jerarquía católica y las Naciones Unidas.]

[En tercer lugar, el plebiscito aprobado,]*[per se]*[, no conduce a la paz; es un mecanismo que sella la terminación del conflicto armado con una parte de la insurgencia. Eso es todo. La paz continuará siendo un anhelo para procesar, en especial si consideramos que aún tienen presencia activa los paramilitares y que vienen lustros para que los puntos del acuerdo se realicen.]*[La guerra social continuará]*[.]

[En cuarto lugar, no será la vía expedita que nos proyectará hacia el “progreso”  en clara superación de la etapa subdesarrollada de la modernidad.  Digámoslo brevemente: el]*[comunismo ilustrado]*[ de las FARC, es decir su percepción de que la]*[Razón]*[ es la “soberana de la historia” (Hegel), es productivista y evolucionista; por tanto sostiene la creencia de que el tiempo histórico se ubica en una línea de continuidad infinita, vacía y homogénea; y lo que es más serio, elude la confrontación  al fetichismo de la mercancía – lo que está en el corazón de la crítica a la economía política.  El “progreso” que se defiende en los acuerdos con el Estado no es más, en realidad, que la catástrofe de la historia (Benjamin).]

[Además, si bien el 2 de octubre los colombianos aprobaremos el plebiscito no debemos dejar de señalar que las FARC no representan al conjunto social, a la mayoría del pueblo trabajador urbano y rural; repito, no nos representan ni copan el escenario alternativo. No adherimos al programa estratégico de las FARC ni compartimos su política de alianzas.  ]

[Tampoco podemos silenciarnos, pues votaremos por el Sí, frente al gobierno de Juan Manuel Santos. Neoliberal como Peña Nieto en México, Temer en Brasil y Macri en Argentina representa a los sectores dirigentes, al capital nacional e internacional, al imperialismo, a los vencedores.]

[Con todo, el plebiscito como acontecimiento conjuga y]*[condensa]*[ la resistencia ciudadana, urbana y campesina frente al Estado. Decenas de años soportando y sufriendo la guerra, pero también miles de días resistiendo.]**Por eso las víctimas están hoy en el centro de la lucha** ***política y social*** **en Colombia**[. Nunca antes lo estuvieron. Han conquistado su espacio y ya no lo perderán nunca. Ellas marcan la diferencia; están a la ofensiva como debe ser. Y estamos hablando de militares, indígenas, policías, campesinos, ciudadanos, cristianos, católicos, intelectuales, políticos, afrocolombianos, secuestrados, LGBTI, guerrilleros, niñ@s, mujeres, adultos mayores, discapacitados mentales y físicos …]

[Hablamos de víctimas, como sujetos plurales. Y con ellas se proyectan las memorias. Unas y otras integran un inacabado mosaico histórico en el que la justicia transicional, restaurativa, abrió las puertas al pasado que no pasa, a diferencia de la]**impunidad pactada**[ de las elites en el plebiscito de 1957.]

------------------------------------------------------------------------

###### [Las opiniones expresadas en este artículo son de exclusiva responsabilidad del autor y no necesariamente representan la opinión de la Contagio Radio.] 

[[                                                                                                                                           ]                                                                                     ]
