Title: 110 líderes sociales han renunciado a su labor en Antioquia por amenazas
Date: 2018-08-14 13:06
Category: DDHH, Paz
Tags: Amenazas, Antioquia, Coordinación Colombia- Europa-Estados Unidos, lideres sociales
Slug: 110-lideres-sociales-han-renunciado
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/08/imágenlíderes.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:@coeuropa] 

###### [14 Ago 2018] 

Tras la aparición de nuevos panfletos amenazantes contra funcionarios públicos y líderes sociales de los municipios Valdivia, Tarazá y Cáceres, en el Bajo Cauca Antioqueño, por parte de las Autodefensas Gaitanistas de Colombia (AGC) y un nuevo grupo llamado Agucan, el temor se ha expandido en la población, provocando la renuncia de 110 líderes al trabajo que venían adelantando en la región.

Oscar Zapata, integrante de la Coordinación Colombia-Europa-Estados Unidos (CCEEU) en Antioquia, asegura que se ha registrado **un incremento del 110% en amenazas contra defensores de Derechos Humanos,** lo que ha ocasionado desplazamientos masivos de estas personas. Adicionalmente, en el Bajo Cauca se presente un aumento del "**190% de asesinatos selectivos de ciudadanos".**

Estos hechos, sumados a la incapacidad del Estado de proteger la vida, la pasividad de las autoridades de control ante las denuncias de la CCEEU sobre una guerra entre los Caparrapos y las AGC, y la **"posible relación de estructuras ilegales con las autoridades",** determinan según Zapata, el que los líderes estén renunciando a su labor. (Le puede interesar: ["17 líderes sociales han sido asesinados este año en Antioquia"](https://archivo.contagioradio.com/17-lideres-sociales-han-sido-asesinados-este-ano-en-antioquia/))

### **110 líderes comunales han renunciado a su tarea en Antioquia** 

El investigador señaló que "hay un escenario de riesgo, en una de las zonas en las que más se han presentado casos de asesinatos de defensores de Derechos Humanos", pues según la Defensoría del Pueblo, entre 2016 y junio de 2018, **Antioquia es el segundo departamento con más líderes sociales asesinados (43), después de Cauca (78).** (Le puede interesar: "[Dos líderes asesinados durante el 20 de julio"](https://archivo.contagioradio.com/lideres-asesinados-el-20-de-julio/))

De acuerdo con información brindada por habitantes de Valdivia, Tarazá y Cáceres, el temor se viene apoderando nuevamente de las personas por cuenta de panfletos **amenazantes firmados por las autodenominadas AGC y un nuevo grupo que se hace llamar Agucan,** lo que ha silenciado procesos sociales, con la renuncia de 110 líderes comunales en la región.

La mayoría de amenazas se presentan vía mensaje de Whatsapp, y su intención es detener el activismo social y la defensa de los Derechos Humanos en el territorio. Sin embargo, Zapata afirma que los líderes en el Departamento seguirán trabajando con firmeza, y exigiendo al Gobierno las debidas garantías para la vida.

<iframe id="audio_27816401" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_27816401_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en] [su correo](http://bit.ly/1nvAO4u) [o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por] [Contagio Radio](http://bit.ly/1ICYhVU) 
