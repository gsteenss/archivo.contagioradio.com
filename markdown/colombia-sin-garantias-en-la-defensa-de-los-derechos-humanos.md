Title: Colombia sin garantías en la defensa de los derechos Humanos
Date: 2017-03-21 15:36
Category: DDHH, Nacional
Tags: asesinato defensores de derechos humanos, CIDH
Slug: colombia-sin-garantias-en-la-defensa-de-los-derechos-humanos
Status: published

###### [Foto: Archivo Particular] 

###### [21 Mar 2017] 

En lo corrido del año 2017, en Colombia se han registrado **26 asesinatos a defensores de derechos humanos en diferentes regiones del país, esta alarmante situación, que fue presentada ante la Corte Interamericana de Derechos Humanos**, evidencio que el gobierno colombiano sigue negando la sistematicidad de estos hechos y la existencia de grupos paramilitares, que de acuerdo con organizaciones defensoras de derechos humanos, siguen actuando en el territorio del país.

Puntualmente las organizaciones defensoras de derechos humanos exigieron en la audiencia la mejora y **puesta en marcha de los mecanismos institucionales para la protección de defensores de derechos humanos y la voluntad política y el esclarecimiento de las investigaciones en torno a sus asesinatos**. Le puede interesar:["120 Defensores de DDHH asesinados en 14 meses en Colombia: Defensoría" ](https://archivo.contagioradio.com/120-defensores-asesinados-defensoria/)

### **Colombia un Estado inoperante ante la defensa de los derechos Humanos** 

Luz Marina Monzón, defensora de derechos humanos e integrante de la Corporación Reiniciar, señaló que “**el asesinato, la estigmatización, las amenazas a lideresas, líderes y defensoras de derechos humanos es hoy una realidad en Colombia**” y el primer problema para superarlo es la “actitud negacionista” por parte del Estado. Le puede interesar:["Delegación Asturiana denuncia la reparamilitarización del territorio Colombiano"](https://archivo.contagioradio.com/delegacion-asturiana-denuncia-la-reparamilitarizacion-del-territorio-colombiano/)

A su vez, expresó que son preocupantes las afirmaciones tanto del Ministerio de Defensa como de la Fiscalía, al señalar que los **asesinatos a defensores de derechos humanos no son sistemáticos y que no necesariamente están asociados a la labor** que realizan, evidenciando de esta manera que la “Fiscalía se ha caracterizado por no asumir y desarrollar de manera coherente una estrategia e investigación no solo de los asesinatos sino de las amenazas”

Producto de ello es que **ninguna de las capturas o investigaciones en los que avanza la Fiscalía dan cuentan de cuáles son las estructuras que están detrás de estos asesinatos**.  De igual modo Monzón indicó 3 aspectos importantes que niega el Estado frente a la realidad del país: el primero es la permanencia de grupos paramilitares que estarían detrás de los asesinatos, el segundo es de la dimensión y el alcance de esta problemática y por último es decir que no se ataca a los defensores por su labor.

### **Mecanismos de protección a defensores de Derechos Humanos** 

Pedro Cortés, representante del Instituto Internacional sobre Raza Igualdad y Derechos Humanos, que trabaja con organizaciones Afro en Colombia, indicó “seguimos preocupados porque **no hay un enfoque diferencial que reconozca las característica e impactos particulares** que configuran las situaciones de riesgo y las violaciones sufridas por líderes y lideresas de estas comunidades”

Como ejemplo de tal situación, relató el caso de 3 de los directivos de AFRODES que fueron víctimas de atentados y fueron calificados por el **Programa de Protección de la Unidad de Protección a Víctimas, como un riesgo ordinario**. Motivo por el cual no recibieron medidas de protección adecuadas.

A estos casos se suma el de Erlinda Cueros, **lideresa que por sexta vez tendrá que enfrentar la evaluación de riesgo para poder recibir medidas de protección**, pese a que ya otras instancias han señalado como urgente que sea protegida. Le puede interesar:["Nuevo ataque contra defensores de DDHH en Putumayo"](https://archivo.contagioradio.com/ataque-defensores-ddhh-putumayo/)

Otra de las peticiones que las organizaciones de derechos humanos habían hecho, era la creación de medidas de protección colectivas, sin embargo, **el Estado no ha hecho las modificaciones** normativas para que la Unidad de Protección pueda reconocer esta dimensión colectiva.

En términos de los riesgos y protección a líderes y lideresas de comunidades LGTBI, la problemática es urgente; en tan solo el año 2015 se **registraron 106 asesinatos a líderes esta comunidad y el 95% de los casos se encuentran en la impunidad.**

El movimiento político Marcha Patriótica denunció en la audiencia, que **133 de sus integrantes han sido asesinados, 13 de ellas mujeres**, y reportaron 517 amenazas que demuestran la existencia de un plan de “exterminio” contra esta organización y una inoperancia por parte del Estado que actualmente solo tiene en investigación, de acuerdo con la Procuraduría General, dos de estos casos.

### **Las causas de los asesinatos según la Fiscalía General** 

Durante la audiencia, la delegación del gobierno señaló que bajo los compromisos adquiridos en el acuerdo de paz, se **han constituido instancias y políticas públicas adecuadas en prevención que brinden tranquilidad para la labor de los defensores**. Le puede interesar: ["Paramilitares en diferentes regiones del país"](https://archivo.contagioradio.com/paramilitares-arremeten-contra-diferentes-regiones-del-pais/)

La Fiscalía indicó que de acuerdo con sus investigaciones **hay 6 causas de los asesinatos a defensores de derechos humanos**: la primera es la disputa por las rentas ilícitas frente a la salida de las FARC de los territorios por parte de organizaciones criminales, la segunda son algunas disidencias de las FARC que buscan mantener control territorial, el tercero es la corrupción administrativa, la cuarta es la falta de creencia en las Instituciones Estatales, la quinta es la falta de conectividad de las localidades con los centros y sexto la violencia como lenguaje transversal a la realidad colombiana.

La Vice Fiscal reveló que hasta el momento se han registrado **74 personas vinculadas con los procesos**, 58 se encuentran privadas de la libertad, ** 4 casos tienen sentencia judicial, 6 casos han pasado a la etapa de juicio, 8 están en etapa de investigación**, 8 con órdenes de captura vigente y el resto se encuentra en etapa de indagación.

<div class="fb-video" data-href="https://www.facebook.com/contagioradio/videos/10154288664805812/" data-width="500" data-show-text="false">

> [Audiencias CIDH - Colombia](https://www.facebook.com/contagioradio/videos/10154288664805812/)Investigación de agresiones contra defensoras y defensores de derechos humanos en Colombia  
> Posted by [Contagio Radio](https://www.facebook.com/contagioradio/) on martes, 21 de marzo de 2017

</div>

###### Reciba toda la información de Contagio Radio en [[su correo]
