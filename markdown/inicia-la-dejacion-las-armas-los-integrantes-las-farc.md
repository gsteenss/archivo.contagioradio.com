Title: Inicia la dejación de armas de los integrantes de las FARC
Date: 2016-12-05 13:56
Category: Nacional, Paz
Tags: acuerdo de paz, desarme de las farc, Gobierno Nacional
Slug: inicia-la-dejacion-las-armas-los-integrantes-las-farc
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/conferencia-guerrillera-yari.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Gabriel Galindo, Contagio Radio 

###### [5 Dic 2016] 

Luego de que el pasado martes el Senado refrendara el acuerdo final de paz con 75 votos, y luego el miércoles, la Cámara de Representantes lo hiciera con 130, **este lunes es el Día+5.** Fecha en la que empieza el desplazamiento de los integrantes de la guerrilla de las FARC hacia las zonas de transición.

El presidente Juan Manuel Santos, dijo que el 30 de diciembre todos los guerrilleros deben estar  ubicados en dichas zonas, para que se pueda dar la dejación absoluta de las armas. **Primero serán tres meses, D+90 en los que se entregará el 30% de las armas; el siguiente mes D+120 se dejará otro 30%, y finalmente el 40% del armamento restante será entregado el día D+150.**

El Ministro de Defensa, Luis Carlos Villegas, anunció que el miércoles estará todo preparado para el proceso de desmovilización del grupo guerrillero. Serán 12.000 los uniformados que protegerán los corredores de movilidad y las 27 zonas verdales de transición. (Le puede interesar: [Ley de Amnistía urgente para avanzar en implementación de acuerdos](https://archivo.contagioradio.com/ley-de-amnistia-urgente-para-avanzar-en-implementacion-de-acuerdos/))

### ¿Qué sigue? 

Según el acuerdo final, el **D+1 la Fuerza Pública reorganizó el dispositivo de las tropas para facilitar el desplazamiento de las estructuras de la guerrilla** a las zonas de transición. Un delegado del Gobierno Nacional y uno de las FARC entregaron las coordenadas de la ubicación de las unidades de la Fuerza Pública y de la insurgencia, para que se adopten las medidas necesarias para posibilitar los desplazamientos.

Siendo así, desde el día D+5, es decir hoy, se inicia el desplazamiento a las ZVTN de las unidades de las FARC y el transporte del armamento individual, las distintas misiones, comisiones y Unidades Tácticas de Combate de los frentes de la guerrilla se desplazan hacia los puntos establecidos. Un proceso que será monitoreado y verificado por el mecanismo acordado.

**Desde el D+7 al día D+30 se realiza el transporte de las armas de acompañamiento,** del armamento de las milicias, las granadas y municiones, por parte de los integrantes de la guerrilla hacia las Zonas de Transición bajo el protocolo de seguridad, para luego empezar la labor de registro, identificación, monitoreo y verificación, recolección y almacenamiento del armamento.

-   **Registro:** Procedimiento técnico para consignar la cantidad y tipo de armas recibidas a las FARC por parte del Componente internacional del mecanismo de monitoreo y verificación (CI-MM&V).
-   **Identificación:** Corresponde al procedimiento técnico que permite caracterizar las armas de las FARC. Este procedimiento se hace únicamente con las armas individuales que porten los guerrilleros dentro de los campamentos.
-   **Monitoreo y verificación de la tenencia: C**ada uno de los integrantes de la guerrilla que permanecen en las Zonas porta un arma individual dentro del campamento. El Componente internacional realiza el monitoreo y verificación de la tenencia de este armamento con base en el registro e identificación realizado por los observadores internacionales. Teniendo en cuenta que dentro de los campamentos hay presencia permanente de la comunidad internacional.
-   **Recolección:** Es entendida como el procedimiento técnico mediante el cual el Componente internacional del mecanismo de monitoreo y verificación recibe todas las armas de las FARC.
-   **Almacenamiento del armamento:** En cada Zona, dentro de uno de los campamentos, hay un punto para el almacenamiento del armamento recibido por el Componente internacional, en contenedores dispuestos para tal fin. A este lugar sólo puede ingresar el CI-MM&V que efectúa monitoreo y verificación permanente.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)
