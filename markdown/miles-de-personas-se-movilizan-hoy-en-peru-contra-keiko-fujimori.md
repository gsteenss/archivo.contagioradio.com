Title: Miles de personas se movilizan en Perú contra Keiko Fujimori
Date: 2016-04-05 13:54
Category: El mundo, Política
Tags: Kaiko Fujimori, Perú
Slug: miles-de-personas-se-movilizan-hoy-en-peru-contra-keiko-fujimori
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/04/Keiko-Fujimori.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Perú 21 

###### [5 Abr 2016]

Diferentes sectores del movimiento social en Perú salen hoy en una multitudinaria movilización en contra de la candidatura de **Keiko Fujimori** y en conmemoración de las víctimas del golpe de Estado por parte de su padre, el expresidente **Alberto Fujimori en 1992.**

La candidatura de [Keiko Fujimori](https://archivo.contagioradio.com/?s=keiko) respaldada por el partido Fuerza Popular, tiene una fuerte oposición por parte de diversos sectores de la sociedad debido a las múltiples acusaciones por corrupción, actualmente enfrenta un proceso de apelación por la entrega de dineros en actividades proselitistas denunciada ante el Jurado Electoral, que falló a favor de la continuación de la campaña por considerar que las pruebas son insuficientes para frenar la candidatura.

Según Paloma Duarte periodista y miembro de 'La Junta', a pesar de la gran cantidad de pruebas presentadas al Jurado Electoral, que siga la campaña constituye un flagrante delito por permitir que la corrupción avance. Hecho que ha generado una gran movilización denominada **Keiko no va y que crítica la continuación de lo que fue el modelo político de Alberto Fujimori.**

Duarte plantea que **uno de los candidatos que ha resonado es Verónica Mendoza por el partido Frente Amplio**, que actualmente se encuentra en el tercer puesto en las encuestas. Mendoza está capitalizando el voto anti sistémico que existe en el Perú y refleja en su programa político cambios radicales con respecto a la constitución de 1993 correspondiente al golpe de Estado.

“hoy 5 de abril es una fecha de memoria muy importante, un día como hoy Fujimori comenzó su dictadura, y nosotros no olvidamos, por eso se están convocando las movilizaciones más grandes que se hayan visto en el Perú, hoy **vamos a reafirmar en las calles la gran cantidad de personas que estamos organizadas en contra del Fujimorismo”.**

La movilización saldrá de la Plaza de San Martín sobre las 5:00 pm  y recorrerá las avenidas Colmena, Wilson, Uruguay y Alfonso Ugarte hasta llegar a la Plaza Bolognesi. Continuará por Paseo Colón, avenida Abancay y Parque Universitario para luego retornar a la Plaza San Martín.

<iframe src="http://co.ivoox.com/es/player_ej_11055531_2_1.html?data=kpadl5qZd5Khhpywj5aZaZS1k56ah5yncZOhhpywj5WRaZi3jpWah5ynca7dzcrgjcnJb9HZ09jc0MbXb9TZjNLc2M7QrdvVz5DS0JC0qdOZpJiSpKaPp9Di1dfOjbDJrczjjKvij4qbh4630NPhw8zNs4zGwsnW0ZCRaZi3jpk%3D&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
