Title: Las preocupaciones que las víctimas le manifestaron a la CPI
Date: 2017-09-14 17:07
Category: DDHH, Nacional
Tags: Corte Penal Internacional, cpi, Fiscal Bendousa, Fiscalía, MOVICE
Slug: corte-penal-internacional-con-sus-ojos-sobre-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/04/VÍCTIMAS.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo] 

###### [14 Sept. 2017]

Los casos de 6 generales, la responsabilidad de mando, las libertades condicionales a militares, la obstrucción a la justicia y la impunidad en los casos de ejecuciones extrajudiciales, fueron los elementos centrales del encuentro entre organizaciones de víctimas, de derechos humanos y la fiscalía de la Corte Penal Internacional.

### **Víctimas y organizaciones sociales se reunieron con la Fiscal de la CPI** 

En un encuentro personal la Fiscal **Fatou Bensouda escuchó de manera atenta las preocupaciones de las víctimas y organizaciones** sociales que trabajan el tema de las ejecuciones extrajudiciales y los crímenes de Estado. Las preocupaciones giraron en torno a la falta de celeridad en las investigaciones contra los máximos responsables de estos hechos, la falta de imputación de cargos y los casos de obstrucción a la justicia.

Alexandra González, del MOVICE señaló que **“generales de alto rango solo se están investigando a 6 y solo hay avances en 1 caso** que es el del General Torres Escalante el cual ya se encuentra en libertad por haber suscrito el acta con la Secretaria Ejecutiva de la JEP. Decimos que por ejemplo hace más de un año se anunció que se iba a hacer imputación de cargos al general Mario Montoya y hasta el momento no ha pasado nada”

Además, recalcó Gonzáles, que los abogados se han encontrado en las audiencias con lo que parecería una práctica sistemática para no realizar imputaciones a altos mandos, señalamiento que habría generado una alta preocupación a la Fiscalía de la CPI.

**“Esto demuestra que Colombia en su sistema judicial tiene aún varias fallas** y sobre todo la continuidad en la promoción y ascenso de personas de la Fuerza Pública que están investigadas por estos crímenes y que al mantenerse en estos cargos de alto rango pueden obstruir tanto la investigación como la consecución de pruebas”.

### **Fiscalía no le ha entregado información a la CPI porque no la tiene** 

Según el MOVICE las cifras solicitadas por la Fiscal Bensouda no han sido enviadas a la CPI porque **la Fiscalía en Colombia no tiene un registro del estado de las investigaciones contra altos mando implicados** en ejecuciones extrajudiciales, por esa razón las que entregaron el estado y número de investigaciones fueron las organizaciones sociales.

“Por ejemplo en el General Torres Escalante tiene 5 procesos abiertos y solo 1 de ellos lo tiene activo judicialmente que es el proceso por el cual estaba preso, los otros 4 procesos de los cuales se le acusa más por omisión no hay ningún tipo de avance y esa información fue la que le dijimos a la Fiscal¨ En conclusión las víctimas y las organizaciones sociales que trabaja estos temas no tienen ninguna garantía para el avance de las investigaciones.

### **Es posible que la JEP no garantice el juzgamiento de altos mandos** 

Otra de las preocupaciones que le dieron a conocer a la Fiscal de la CPI es que la JEP no permita que los altos mandos civiles como ministros y otros asuman su responsabilidad en relación con las ejecuciones extrajudiciales. Le puede interesar: [CPI solicita que 29 generales y coroneles sean juzgados por ejecuciones extrajudiciales](https://archivo.contagioradio.com/corte_penal_internacional_falsos_positivos/)

Por otra parte les preocupan las libertades condicionales que deberían estar supeditadas al cumplimiento de los **compromisos en materia de restablecimiento de los derechos de las víctimas a la verdad, la justicia y la reparación integral.** En ese sentido muy pocos de los imputados que han manifestado su interés de acogerse a la JEP han cumplido esos requisitos.

“Han salido en libertad condicional 525 miembros del Ejército por el decreto 706. **Han quedado en libertad condicionada 241** y han sido trasladadas de centro de reclusión otras 69 personas y si nosotros si bien no queremos cárcel en sí misma, nos preocupa que estas salidas no se den en el marco de un compromiso real de verdad, justicia y reparación y que por el contrario digan que han sido injustamente acusados y que desmentirán a las víctimas” una actitud que además pondría en riesgos de seguridad a las víctimas.

### **Visita de la Fiscal es un llamado a la Corte Constitucional** 

Para el MOVICE esta visita manda un mensaje de alerta a la Corte Constitucional que en sus interpretaciones  a los actos legislativos y a las leyes como la de amnistía e indulto, ¨ha tenido en dos ocasiones unos pronunciamientos en los que desconocen parcialmente el Estatuto de Roma sobre todo en la responsabilidad de alto mando”.

**“Esta Corte Constitucional podrá ratificar que el derecho penal internacional sea un estándar aplicable** en nuestro país en la JEP y en todo lo que tiene que ver con justicia transicional” Le puede interesar: [Si Estado no juzga empresarios y mandos militares "que lo haga la CPI"](https://archivo.contagioradio.com/si-el-estado-no-puede-juzgar-empresarios-y-altos-mandos-militares-que-lo-haga-la-cpi/)

Por esta razón esperan las victimas que la visita de la Fiscal Bensouda le permita a la Corte decir que la justicia colombiana tiene el deber de hacer cumplir lo que el Estado colombiano ha ratificado internacionalmente “la Fiscal lo ha dicho, seguirán pendientes de Colombia”.

<iframe id="audio_20886546" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_20886546_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

<article id="post-46640" class="post__holder post-46640 post type-post status-publish format-standard has-post-thumbnail hentry category-actualidad category-ddhh tag-alirio-uribe tag-alvaro-uribe tag-chuzadas tag-das tag-interceptaciones-ilegales cat-73-id cat-13-id">
<div class="post_content">

<article id="post-20976" class="post__holder post-20976 post type-post status-publish format-standard has-post-thumbnail hentry category-ddhh category-entrevistas cat-13-id cat-15-id">
<div class="post_content">

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)

<div class="clear">

</div>

</div>

</article>

</div>

</article>

