Title: Asesinan a excombatientes que esperaban proyectos productivos en Nariño y Cauca
Date: 2019-06-14 12:28
Author: CtgAdm
Category: DDHH, Nacional
Tags: asesinatos de excombatientes, Cauca, El Tambo, nariño, Tumaco
Slug: asesinan-a-excombatientes-que-aguardaban-proyectos-productivos-en-narino-y-cauca
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/04/asesinados_farc-e1522961371379.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Teleantioquia] 

En menos de 48 horas han sido asesinados dos excombatientes de FARC, el primer caso sucedió el pasado 12 de junio en **Tumaco, Nariño** cuando **Alexander Saya Palacios** fue ultimado por desconocidos, mientras al día siguiente, **Rafael Polindara** quien también cumplía con su proceso de reincorporación en **El Tambo, Cauca** perdió la vida después de recibir impactos de arma de fuego sobre su humanidad.

Los hechos que aún son materia de investigación, se suman al asesinato el pasado 7 de junio de **Jorge Enrique Sepulveda**, exintegrante de FARC quien aguardaba a que iniciara su proyecto productivo de la mano de la ARN en Tierralta, Córdoba, lo que representa un incremento en el asesinato de excombatientes en 2019.

### Excombatientes trabajaban en su reincorporación 

Ambos pertenecían a la **Cooperativa Multiactiva especial Caminos de Paz (COMCAPAZ)**, la cual viene trabajando junto a la Agencia de Reincorporación y Normalización (ARN) en el desarrollo y propuesta de proyectos productivos orientados a la **piscicultura.**

Según el gerente de la cooperativa, ya son tres los excombatientes vinculados a esta organización los asesinados en 2019, "situación que ha prendido todas las alarmas al interior del proceso social de reincorporación". [(Lea también: Asesinan a Wilson Saavedra ex comandante de FARC que aportó a la paz)](https://archivo.contagioradio.com/asesinan-a-wilson-saavedra-ex-comandante-de-farc-que-aporto-a-la-paz/)

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
