Title: Los grises de los periodistas colombianos
Date: 2016-02-09 14:32
Category: Carolina, Opinion
Tags: día del periodista, Libertad de Prensa, Periodistas
Slug: los-grises-de-los-periodistas-colombianos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/02/periodistas.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: bocadepolen 

#### [Carolina Garzón Díaz](https://archivo.contagioradio.com/carolina-garzon-diaz/) - ([@E\_Vinna](https://twitter.com/E_Vinna)).

###### [9 Feb 2016]

[Desde las escuelas de periodismo y desde la vida misma se nos enseña que las personas, situaciones y los hechos no son blancos o negros, sino que tienen un amplio espectro de colores y muchos grises. Coincido, y creo que como periodistas decidimos sobre cuál de esos tonos escribir y contar las historias. Pero considero que esa paleta de colores aplica también a nosotros mismos como periodistas, entendiéndonos como seres humanos diversos con unos compromisos comunes en relación a nuestra profesión y ahí es dónde voy a profundizar aprovechando la fecha.]

[¿Cuál fecha? La conmemoración nacional del Día del periodista, un día que aplica sólo a Colombia en homenaje a la puesta en circulación del semanario "Papel Periódico de Santafé de Bogotá" el 9 de febrero de 1791. A nivel internacional el 8 de septiembre se conmemora el Día Internacional del Periodista pero en Colombia, además del día nacional y el internacional, en el año 2004 se declaró el 4 de agosto como el Día del Periodista y Comunicador. Es decir, Colombia tiene tres fechas que podemos usar de excusa para hablar de los periodistas, nuestra profesión, nuestros logros, deberes y nuestras responsabilidades.]

[Con estos elementos claros, voy directo a mi punto: los periodistas en Colombia hemos sido víctimas históricas de la violencia expresada en asesinatos, amenazas, tortura, atentados, robo de información y persecución judicial; quienes se han dedicado con responsabilidad y pasión a este oficio se han visto envueltos en una vorágine de intereses, crímenes e historias memorables. No desconozco su valentía y me sumo a las voces que exigen protección y justicia para los cientos de periodistas que arriesgan su vida en las regiones, los medios locales, en las emisoras comunitarias, los periódicos alternativos y cualquier otro medio, grande o pequeño.]

[Pero sería ciego, miope o descarado de mi parte ignorar que en la misma medida en que hay mujeres y hombres admirables, de ética impecable y gran seriedad, hay periodistas que, en términos de Kapuscinski, son unos cínicos.]

[Nuevamente, como cada año, invito a que esta fecha, la conmemoración del Día del Periodista, sea un ejercicio de doble dirección: por una parte, exaltando las virtudes y gran importancia de nuestro oficio, y en otro sentido, generando una reflexión (y autoreflexión) sobre el poder que tenemos en nuestras palabras. Para ambientar este análisis propongo basarnos en textos como los]*[Principios Internacionales de Ética Profesional del Periodismo de la UNESCO]*[, la]*[Resolución 1.003 del Consejo de Europa]*[, la]*[Declaración de Principios sobre la Conducta de los Periodistas de la FIP]*[ y el]*[Código Deontológico de la Profesión Periodística de la FAPE]*[.]

[Estos documentos, cortos en su extensión, pueden leerse en código de derechos y deberes de los periodistas. Allí concuerdan en que la labor del periodista requiere de la garantía de ambas cosas para su ejercicio pleno,] [coincidiendo en la necesidad de respetar los derechos al acceso a la información, la libertad de expresión, la libertad de investigación y condiciones dignas de trabajo; y cumplir con los deberes de respetar la verdad, no distorsionar la información, respetar la dignidad humana, oponerse a la violencia, rechazar toda discriminación y corregir las informaciones falsas o erróneas.]

[Todos los periodistas somos seres humanos diferentes con opiniones y miradas respetables en una amplia gama de grises, pero nuestro trabajo trae consigo una responsabilidad que no podemos ignorar y que la sociedad está en derecho de reclamarnos. Espero que este Día nacional del Periodista y esta columna amplíen el debate sobre el ejercicio del periodismo en nuestro país y sobre su rol en un escenario de postacuerdo de paz.]
