Title: Con “firmatón” sociedad refrendará el nuevo acuerdo de paz
Date: 2016-11-23 15:21
Category: Movilización, Paz
Tags: Conversaciones de paz de la habana, Derechos Humanos, dialogos de paz, FARC, Juan Manuel Santos
Slug: firmaton-nuevo-acuerdo-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/09/paz.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: contagioradio] 

###### [23 Nov 2016]

A partir de las 10 de la mañana, organizaciones sociales que han respaldado el proceso de paz, organizaciones de Derechos Humanos y de víctimas se darán cita en la Plaza de Bolívar para realizar una firmatón, que **será el espacio de refrendación popular de los acuerdos de paz**, según lo manifiestan quienes han participado en la convocatoria que se realiza a través de redes sociales.

**La firmatón** pretende que el **acuerdo, que se firmará de manera simultánea en el teatro Colón a partir de las 11 de la mañana entre el gobierno y las FARC- EP,** tenga también el respaldo de las personas que votaron por el SI en el plebiscito del 2 de Octubre, y también de las personas y organizaciones que, después de ese resultado, han encontrado la necesidad de terminar con la guerra, aseguran los convocantes.

Frente a esa necesidad, el representante a la cámara Alirio Uribe, afirmó en entrevista con Contagio Radio, que **la movilización social debe ser un escenario permanente, tanto para la refrendación del acuerdo, como para la implementación en el congreso** de la república.

### **Conclave y firmatón por la implementación** 

Todo este trámite ha estado acompañado de esa movilización, durante el proceso y después del plebiscito y por ello debe permanecer hasta que se haya implementado, afirma el congresista.

Por su parte Diana Gómez, vocera del Movimiento de Víctimas de Crímenes de Estado, reafirmó la convocatoria y agregó que el próximo martes 29 se darán cita, también en la Plaza de Bolívar, diversas organizaciones sociales y de DDHH para **seguir impulsando lo que han llamado el “conclave por la paz”,** como escenario de movilización permanente para exigir refrendación e implementación inmediata del acuerdo de paz.

Por su parte, los integrantes del movimiento **“Paz a la Calle”** han convicado a una “gran fiesta multicolor” que invita para que a partir de las 11.

![fista multicolor y firmatón](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/38f12385-7b8e-42c9-af3a-edbf292d123c.jpg){.wp-image-32732 .aligncenter width="407" height="215"}

###### Reciba toda la información de Contagio Radio en [[su correo]
