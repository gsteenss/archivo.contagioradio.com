Title: Cerca de 700 mil personas mueren en la región por falta de acceso de medicamentos
Date: 2016-12-05 15:19
Category: DDHH, Nacional
Tags: CCJ, CIDH, Medicamentos, Misión Salud, Salud
Slug: 700-mil-personas-mueren-en-la-region-por-falta-de-acceso-de-medicamentos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/02/pastilla.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Misión Salud] 

###### [5 Dic. 2016] 

Luego de más de dos años en los que cerca de 25 organizaciones de la sociedad civil estuvieron solicitando **una audiencia ante la Comisión Interamericana de Derechos Humanos (CIDH) para hablar sobre la falta de acceso a medicamentos en América Latina** y el Caribe, este órgano finalmente ha decidido escucharlas.

La audiencia que tendrá lugar el próximo 6 de diciembre en Ciudad de Panamá, en el marco del 159º Período Ordinario de Sesiones de la Comisión **será una oportunidad única para visibilizar el problema de la falta de acceso a medicamentos necesarios para la salud y la vida como una negación del derecho fundamental a la salud, sus causas y posibles soluciones.**

En entrevista para Contagio Radio Germán Holguín, director de Misión Salud e integrante de la Alianza LAC-Global por el Acceso a Medicamentos manifestó que “**esta es una problemática que debe ser abordada pues afecta a más de 2.000 millones de personas en el mundo de las cuales la mitad ni siquiera conoce lo que es un medicamento.** Lo que pasa es que este es un problema grande del cual existe muy poca conciencia”.

Adicionalmente la situación, según lo cuenta Holguín es muy “complicada”. Por ejemplo, **para América Latina, la Organización Mundial para la Salud OMS ha estimado que cerca de 700 mil personas mueren en la región por falta de acceso de medicamentos existentes. “eso es un drama, es una locura, tenemos que ponerle fin” agregó.**

Dentro de las causas de este “drama” como lo cataloga Holguín, se encuentran la **falta de investigación de tecnologías médicas aptas para prevenir y tratar las enfermedades, altos precios de los medicamentos y bloqueo de la oferta de medicamentos genéricos con precios asequibles, por parte de las grandes multinacionales farmacéuticas y sus gobiernos, entre otros.**

“La conclusión a la que uno debe llegar es que el bloqueo a los medicamentos genéricos no es un hecho fortuito como puede ser un terremoto, sino que obedece a políticas y estrategias de las multinacionales que tienen como propósito fortalecer su posición monopólica” contó Holguín.

Según el director de Misión Salud, las organizaciones que estarán presentes en esta audiencia esperan poder exponer las causas de esta realidad y de igual manera **solicitar que se declaren estas conductas bloqueadoras de medicamentos como  un crimen de lesa humanidad** “creemos que jurídicamente las conductas bloqueadoras de los medicamentos genéricos reúnen los 4 requisitos fundamentales de los crímenes de lesa humanidad, por lo tanto debería procederse a esa tipificación” concluyó. Le puede interesar: [El negocio de los medicamentos, otro ataque al derecho a la salud](https://archivo.contagioradio.com/el-negocio-de-los-medicamentos-otro-ataque-al-derecho-a-la-salud/)

<iframe id="audio_14635946" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_14635946_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u)  o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](https://archivo.contagioradio.com/). 

 
