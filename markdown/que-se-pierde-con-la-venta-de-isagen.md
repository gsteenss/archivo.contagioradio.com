Title: ¿Qué se pierde con la venta de ISAGEN?
Date: 2016-01-13 17:05
Category: Nacional
Tags: ISAGEN, Ministro de Hacienda
Slug: que-se-pierde-con-la-venta-de-isagen
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/01/Isagen-e1452721868620.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: [mikesbogotablog.blogspot.com]

<iframe src="http://www.ivoox.com/player_ek_10063160_2_1.html?data=kpWdmJiVepGhhpywj5eVaZS1k5eah5yncZOhhpywj5WRaZi3jpWah5yncabgjJ2dh5eZb8XZjNHOjcrHs8%2FjzoqwlYqliMKfxtOYpdTQs87WysaYzsaPscLixs%2FO0JDQpdSf1dfO0NiRaZi3jqjc0NnFq8rjjLfOxs7Tb46ZmKialg%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Libardo Sarmiento, analista económico] 

###### [13 Enero 2016]

El economista Libardo Sarmiento, asegura que el Ministro de Hacienda, Mauricio Cárdenas miente frente a la destinación de los recursos procedentes de la [[venta de ISAGEN](https://archivo.contagioradio.com/isagen-paso-a-bolsillo-de-la-canadiense-brookfield/)]. Según él, los **6,49 billones de pesos que pagó la firma canadiense Brookfield, se destinarían a tapar un hueco fiscal generado por los bajos precios del petróleo.**

Así mismo, señala que ese monto iría a parar en los gastos de funcionamiento de la estructura militarista (pese a que el país está próximo a concretar un proceso de paz) y para algunos pagos pendientes en distintos municipios, en detrimento de la inversión en el potencial productivo, científico y tecnológico del país.

“**A Santos sólo se le abona el tema de la paz, de resto sólo representa los grandes intereses del capital privado nacional y transnacional**”, expresa Samiento, quien agrega que se piensa en construir autopistas sin haber logrado resolver los problemas primarios de la sociedad colombiana, como lo es la salud.

Para el analista, la polémica decisión de Santos de vender [ISAGEN,](https://archivo.contagioradio.com/?s=isagen) se basa en una “mentalidad rentista”, que no tiene en cuenta que esta empresa es patrimonio estratégico, y en cambio toma decisiones buscando resultados inmediatos sin evaluar estratégicamente las repercusiones a largo plazo.

**“Ellos pueden hacer malabarismos de análisis financieros de corto plazo que son muy efímeros, pero el problema aquí es a largo plazo frente al desarrollo del país…** Lo absurdo de la oligarquía voraz y miope, es que no tiene una mirada a largo plazo, pues está saliendo de un recurso vital y estratégico como lo es la energía, que es necesaria para la viabilidad de una sociedad”.

El analista señaló algunas de los elementos más importantes que pierde el país con la venta de ISAGEN.

1.  **Autonomía:** Con la venta de ISAGEN, Colombia continúa remarcando desnacionalización y privatización del país, perdiendo así las bases de la autonomía, desaprovechando  la posibilidad de tener control  de un sector estratégico para la sostenibilidad del país.
2.  **Financiación:** La generadora de electricidad, era uno de los últimos bienes que le quedan al Estado para solucionar una crisis fiscal y estructural profunda que se ha generado con la caída de los precios del petróleo. De acuerdo con Libardo Sarmiento, las utilidades de ISAGEN han aumentado en un 53% en los últimos años, de manera que se está vendiendo “la joya de la corona”.
3.  **Riquezas ambientales:** Isagen posee un patrimonio ambiental de gran valor para Colombia y el mundo. Tiene bajo su control  más de 22 mil hectáreas de espejos de agua y bosques primarios y más de 5 mil millones de metros cúbicos de agua.

