Title: “El abrazo de la serpiente” llega a las salas de cine en Colombia
Date: 2015-03-27 03:05
Author: CtgAdm
Category: 24 Cuadros, eventos
Tags: Cine Colombia, Cinemateca Distrital, Ciro Guerra, el abrazo de la serpiente, Los viajes del viento, Richard Evan Schultes, Theodor Koch-Grunberg
Slug: el-abrazo-de-la-serpiente
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/01/El-abrazo-de-la-serpiente-Jan-Bijvoet-860x450_c.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

*La más reciente producción de Ciro Guerra (Los viajes del viento) llegará a las salas colombianas el 7 de Mayo.*

Un par de semanas después del lanzamiento del tráiler oficial de “El abrazo de la Serpiente” película de Ciudad Lunar dirigida por Ciro Guerra y producida por Cristina Gallego, se presenta oficialmente el póster de esta nueva producción colombiana.

[![el-abrazo-de-la-serpiente-contagio-radio](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/03/el-abrazo-de-la-serpiente-contagio-radio-210x300.jpg){.aligncenter .wp-image-6513 width="246" height="351"}](https://archivo.contagioradio.com/el-abrazo-de-la-serpiente/el-abrazo-de-la-serpiente-contagio-radio/)

La cinta realiza un viaje a la Amazonía, puntualmente a las selvas del Vaupés donde se desarrolla una historia de ficción Inspirada en los diarios de los primeros exploradores que recorrieron la Amazonía Colombiana: Theodor Koch-Grunberg y Richard Evan Schultes. Cuenta la historia del primer contacto, encuentro, acercamiento, traición, y posible amistad que trasciende la vida, entre Karamakate, un Chamán Amazónico, último sobreviviente de su tribu, y dos científicos que con cuarenta años de diferencia, recorren el Amazonas en busca de una planta sagrada que podría curar sus males.

El director Ciro  Guerra, reconocido nacional e internacionalmente por su anterior largometraje “Los viajes del viento”, selección oficial en la categoría ‘Una cierta mirada’ del Festival de Cine de Cannes, en el año 2009, destaca que su nueva producción  parte  desde la mirada de los indígenas y no de los exploradores, quienes ya han contado su historia “Pero los nativos no. Su historia es esta. Un pedazo de tierra del tamaño de un continente, que no se ha contado. Que no existe en el cine de nuestra América. Ese Amazonas ya se ha perdido. Pero en el cine, puede volver a existir”.

<iframe src="https://www.youtube.com/embed/FdOYd-21qaA" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

Por su parte, Cristina Gallego, productora de la cinta afirma que “Los ojos del mundo están puestos en la Amazonía, por su riqueza natural, pero debajo de todo ese inmenso verde hay conocimiento, ciencia, una visión del mundo que busca el equilibrio. También hay historias de sangre y dolor que llevaron al exterminio de muchas lenguas, comunidades, culturas, creencias. Tan dura ha sido la historia amazónica para el indígena que la respuesta ha sido el silencio, en el fondo lo que nos interesa de esta película es dar un espacio, una voz a quien no la ha tenido”.

La cinta llegará a las salas colombianas el próximo 7 de Mayo.  
Fuente: Litza Alarcón/Lizzeth Acosta
