Title: Incendios en Amazonía amenazan a 350 pueblos indígenas de Brasil y Bolivia
Date: 2019-08-23 18:36
Author: CtgAdm
Category: Ambiente, Comunidad
Tags: Amazonía, Bolivia, Brasil, jair bolsonaro, pueblos indígenas
Slug: incendios-en-amazonia-amenazan-a-350-pueblos-indigenas-de-brasil-y-bolivia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/08/Indígenas-Amazonía.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Coiab ] 

Los incendios en la Amazonía, región que ha perdido más de 500.000 hectáreas en los últimos 20 días, han llevado a los pueblos indígenas a declarar el estado de emergencia ambiental y humanitaria debido al impacto del fuego sobre el ecosistema y sus comunidades aledañas. **Se estima que son 350 pueblos originarios los que habitan entre Brasil y Bolivia sumando cerca de 1.5 millones de habitantes** de la cuenca amazónica los que podrían verse afectados por esta crisis ambiental.

Entre el 20 y 21 de agosto de 2019, de los 3.752 focos de incendios en Sudamérica, el 59% ocurrió en Brasil (25.871), 27.8% en Bolivia (12.144), 4.7% en Paraguay (2054) y 1.2% en Perú (522), situación que ha llevado a según datos preliminares más  de 100.000 indígenas damnificados.

Róbinson López, coordinador de cambio climático y biodiversidad de la Coordinadora de las Organizaciones Indígenas de la Cuenca Amazónica (COICA) expresa que además de las afectaciones ambientales, también están siendo vulnerados sus conocimientos tradicionales y espirituales.

> **"Los pueblos indígenas tenemos un relación intrínseca con los territorios y sin ellos no podemos vivir, ahí están nuestras plantas sagradas; han puesto en grave riesgo los conocimientos de la oralidad, nuestro territorio está caracterizado y se mira de una manera integral"**.

Uno de los casos más emblemáticos - señala Robinson - es el del bosque chiquitano en Bolivia, que tras 20 días de conflagración del fuego ha perdido [471.000 hectáreas de bosque endémico, una zona donde existen 554 especies de animales y más de 55 plantas de las que aún se desconoce cuántas han sido afectadas. Por su parte, el pueblo chiquitano que habita en Santa Cruz hoy]{.css-901oao .css-16my406 .r-1qd0xha .r-ad9z0x .r-bcqeeo .r-qvutc0}requiere atención humanitaria, agua potable y alimentos.

### La responsabilidad de los Gobiernos 

Mientras se señala al presidente Evo Morales de ser permisivo frente a acciones que han permitido la quema en el Bosque Seco Chiquitano derivando en la deforestación de más de 600.000 hectáreas, el coordinador indica que en Brasil, Jair Bolsonaro ha asumido una posición "regresiva" utilizando estrategias de desmonte de la protección ambiental, **"dando vía libre a empresarios y generando normativas que permiten el libre accesos a las tierras, el cultivo de soja y caña"**.

Para López, estas acciones estarían facilitando que la Amazonía sea **"la despensa para el extractivismo"** para actividades como la minería, la deforestación, la ganadería a gran escala y la ampliación de la frontera agrícola, "la culpa no la tienen los grupos ambientalistas, la responsabilidad es de estos gobiernos que no tienen clara la importancia del bioma de la Amazonía", reitera Róbinson.

Frente a estas políticas, considera que las demás naciones ejerzan estrategias de presión para sancionar a Gobiernos como el de Bolsonaro que cabe resaltar, se retiró del Acuerdo de París dentro de la Convención Marco de Naciones Unidas sobre Cambio Climático. [(Le puede interesar: arde la Amazonía: 72.843 focos de incendio registrados en 2019)](https://bit.ly/2No10hO)

### Afectaciones  a la Amazonía son graves y el daño irreversible 

Si el fuego sigue propagándose - advierte el coordinador de la COICA - no solo estarían en peligro los 350 grupos indígenas que habitan el Amazonas, también serían afectadas 6.7 millones de km2 de bosques, 44.000 especies de plantas, 2.200 especies de animales, 10% de la reserva de carbono del planeta, 2.500 especies de peces de agua dulce y del 17 al 20% del total del agua dulce del planeta.

Según información adicional proporcionada por el Instituto Nacional de Investigaciones Espaciales de Brasil (INPE), la deforestación en los estados Amazónicos en este país suma 10.788 km2 en el 2019, lo que equivale al tamaño de Jamaica. [(Le puede interesar: Siguen talando los derechos de la Amazonia)](https://archivo.contagioradio.com/siguen-talando-los-derechos-la-amazonia/)

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
