Title: Vergonzoso ausentismo en Cámara para aprobar conciliación de la JEP
Date: 2017-03-23 12:27
Category: Nacional, Paz
Tags: acuerdo de paz con las FARC, Cámara, Jurisdicción Especial de Paz, voces de paz
Slug: ausentismo-congreso-jurisdiccion-de-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/03/ausentismo-camara.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: notieconomicas] 

###### [23 Mar 2016]

Cuando se necesitaba un quorum decisorio de 83, los presentes solo llegaron a 81 y así no se pudo aprobar la conciliación del acto legislativo de la Jurisdicción Especial de Paz. Sin embargo **la responsabilidad no recae solamente sobre el ausentismo de los representantes a la cámara sino también en el gobierno nacional** que no habría cumplido todos los trámites a tiempo.

Según Inti Asprilla, representante a la Cámara por el Partido Verde, aunque muchos de los integrantes de las bancadas no se presentaron, hay una buena parte de responsabilidad por parte del **gobierno nacional que no hizo la publicidad a tiempo y tampoco alistó el texto para que los congresistas lo pudieran estudiar antes de llegar** a la plenaria y votar.

Esa actitud, según Aspirlla es “mediocre” y ha sido la constante en la presentación y trámite de los proyectos que tienen que ver con el acuerdo de paz con la guerrilla de las FARC-EP. Para Asprilla el gobierno tuvo que haber hecho la convocatoria con tiempo y **tener dispuestas todas las herramientas para que los representantes no tuvieran excusa alguna**.

La Cámara de Representantes, compuesta por 166 representantes debería completar la mitad más uno presentes para tener el quorum decisorio, sin embargo esto no se logró. Y esta **es la segunda vez que un trámite del acuerdo de paz se queda sin resolver por el ausentismo en el congres**o. ([Lea también: Operación tortuga a JEP](https://archivo.contagioradio.com/fast-track-operacion-tortuga-jep/))

### **¿Quiénes fueron los ausentes?** 

Inti Asprilla afirmó que los partidos que registraron más ausentismo fueron **Cambio Radical, el Partido Conservador y el Partido Liberal, del Centro Democrático** que ostenta 19 puestos en ese escenario, nunca se esperó la participación.

Por otra parte, frente a las posibles dudas sobre los puntos críticos de la JEP que rondan en torno a la cadena de mando y la participación de terceros en la guerra, Inti Asprilla señaló que ya los “goles” los hizo Cambio Radical en la plenaria del Senado por eso no había dudas que pudieran provocar la no asistencia, además **resaltó la presencia del nuevo ministro de justicia en la sesión.** ([Le puede interesar: Modificaciones a la JEP abren la puerta a la CPI](https://archivo.contagioradio.com/modificaciones-a-jurisdiccion-especial-de-paz-abririan-la-puerta-a-corte-penal-internacional/))**  
**

### **Sigue siendo necesaria la veeduría social** 

De seguir así el **trámite de la legislación del acuerdo de paz entre el gobierno y las FARC, se podría ver seriamente afectado** por el pleno de la campaña política que ya comenzó y por el inicio de las sesiones ordinarias del congreso que ponen sobre la agenda legislativa otros trámites.

Según otros congresistas y lideres sociales, así como integrantes del Movimiento Voces de Paz, la **presión de la ciudadanía se hace cada vez más urgente y necesaria** por lo que se ha demostrado en anteriores trámites en que el congreso se ve obligado a responder con responsabilidad al llamado de implementación. ([Lea también: La veeduría será fundamental para sacar adelante el acuerdo de paz](https://archivo.contagioradio.com/la-ciudadania-debera-ser-veedora-de-la-jurisdiccion-especial-de-paz/))

<iframe id="audio_17730508" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_17730508_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio]{.s1}](http://bit.ly/1ICYhVU)[.]{.s1} {#reciba-toda-la-información-de-contagio-radio-en-su-correo-o-escúchela-de-lunes-a-viernes-de-8-a-10-de-la-mañana-en-otra-mirada-por-contagio-radio. .p1}
