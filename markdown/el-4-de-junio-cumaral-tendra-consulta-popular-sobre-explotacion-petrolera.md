Title: El 4 de junio Cumaral tendrá consulta popular sobre explotación Petrolera
Date: 2017-06-03 11:45
Category: Ambiente
Tags: consulta popular, exploración de petróleo
Slug: el-4-de-junio-cumaral-tendra-consulta-popular-sobre-explotacion-petrolera
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/06/cumaral-e1496508276136.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo Particular] 

###### [03 Jun 2017]

Mañana los habitantes de **Cumaral, Meta, decidirán en las urnas si quieren que en su territorio se realice exploración y explotación de proyecto petroleros.** El alcalde de este municipio, Miguel Antonio Caro, ha señalado que la intención de la consulta popular es que la población decida sobre la importancia de la protección de los recursos y su destinación.

Los cumaraleños necesitan superar el umbral de 5.260 votos, para que se apruebe la la consulta y de el deberán tener un **50% más un voto**, que este en contra para que las actividades petroleras no vuelvan a realizarse en su territorio.

La pregunta que responderán los cumaraleños será ¿Está usted de acuerdo, ciudadano cumaraleño, con que dentro de la jurisdicción del municipio de Cumaral (Meta) se ejecuten actividades de exploración sísmica, perforación exploratoria y producción de hidrocarburos? y **se espera que a las urnas se acerquen más de 2.000 personas**. Le puede interesar:["Habitantes de Cumaral, Meta, decidirán si quieren o no explotación petrolera en su territorio"](https://archivo.contagioradio.com/38866/)

Actualmente en Cumaral, hay un 80% del territorio que se habría destinado a exploración y explotación de petroleo y se encuentra cubierto con un 90% de Bloques petroleros ( (Condor, CP04, LLA 35, LLA 59 y LLA69) y el proceso de consulta surge tras el rechazó de la ciudadanía a  la actividad petrolera **Mansarovar Energy Colombia LTDA, o también conocido como el Proyecto Llanos 69 –LL69. **

En un comunicado de prensa, el comité impulsor de la consulta popular señaló que estas actividades “**afectará de manera irreparable el recurso hídrico contenido en:  ríos, caños, nacederos, morichales, lagunas, fauna y flora** en general y aljibes, que incluso surten del recurso natural (agua) a otros municipios distintos a Cumaral como: El Calvario, Restrepo, Villavicencio y Medina, en los departamentos del Meta y Cundinamarca”.

######  Reciba toda la información de Contagio Radio en [[su correo]
