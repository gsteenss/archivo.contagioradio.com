Title: De enero a mayo de 2017 se reportan 7.371 personas desplazadas forzosamente
Date: 2017-06-20 20:04
Category: DDHH, Nacional
Tags: ACNUR, Desplazamiento forzado
Slug: enero_mayo_colombia_desplazamiento-forzado
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/Desplazados-e1481538333709.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Reporteros Asociados 

###### [20 Jun 2017] 

En 2016 ACNUR reportó que la cifra de **personas desplazadas en Colombia aumentó a 7,7 millones, es decir que incrementó en 300 mil personas. **Un número que  representa el mayor registro de todos los países del mundo en desplazados internos.

No obstante **se trata de una cifra histórica que data desde 1985.** La ACNUR, reconoce que cada año la cifra de deslazados nuevos ha disminuido, en el marco del acuerdo de paz firmado con las FARC.

Sin embargo desde la ACNUR se pone especial atención en zonas como el Catatumbo y el pacífico colombiano donde se concentra la mayor parte de **la población desplazada durante 2016. Son 7.371 personas, 2.056 familias afectadas.** De acuerdo con el Alto Comisionado de la ACNUR en Colombia, se han evidenciado 42 desplazamientos masivos en la costa pacífica, Norte de Santander y Arauca.

Rocío Castañeda, de la Unidad de Información Pública en Colombia ACNUR, señala que se trata de una situación vinculada a la presencia en esas regiones de “grupos armados por desmovilización, y otros nuevos”.

Sin embargo, lo que más interesa a la ACNUR, es la situación humanitaria. En esa medida **las acciones del gobierno deben ir dirigidas a la situación humanitaria** teniendo en cuenta que la población especialmente afectada con las comunidades indígenas y afrocolombianas. Una atención, en la que haga presencia el Estado, no solo desde la seguridad sino, en garantizar los servicios públicos, la salud, y educación.

<iframe id="audio_19394337" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_19394337_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio.](http://bit.ly/1ICYhVU) 
