Title: Con siembra de árboles ciudadanos defienden la Reserva Van Der Hammen
Date: 2017-08-04 15:44
Category: Ambiente, Nacional
Slug: con-siembra-de-arboles-ciudadanos-defienden-la-reserva-van-der-hammen
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/01/thomas4.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo Particular] 

###### [04 Ago 2017] 

Actualmente la Reserva se encuentra en un periodo de restauración, por este motivo, el próximo 7 de agosto en la Plaza Simón Bolívar diferentes organizaciones en defensa del medio ambiente realizaran una gran recolección de árboles nativos, con la intensión de llevarlos ante la CAR y que sean sembrados en la Reserva, **desde las 8 de la mañana hasta las 3 de la tarde, se estarán realizando diferentes actividades.**

Por su parte la Alcaldía distrital anunció que en un mes presentará a la CAR, el proyecto para realizar modificaciones a la Reserva Thomas Van Der Hammen, sin embargo, diferentes colectivos ambientales manifestaron que estarán al tanto de las posibles cambios al proyecto y continuaran en la defensa de uno de los pulmones de la Ciudad, **con una gran siembra de árboles.**

### **Distrito busca modificar la Reserva Van Der Hammen** 

De acuerdo con Sabina Rodríguez, integrante del Colectivo en Defensa de la Reserva Thomas Van Der Hammen, el proyecto y sus cambios, debería presentarse a puerta abierta para que **toda la ciudadanía tenga conocimiento de las afectaciones que podría sufrir la reserva.** (Le puede interesar:["Peñalosa obligado a responder por urbanización de la Reserva Thomas Van Der Hammen"](https://archivo.contagioradio.com/penalosa-van-der-hammen/))

“Estamos pendientes de hacer toda la incidencia ciudadana que sea necesaria, nos da un poco de temor que esta discusión se vaya a realizar a puerta cerrada, así que **haremos toda la presión para que desde la ciudadanía tengamos participación y podamos presentar todos los análisis** y estudios, para que no se cambie de decisión” afirmó Sabina.

Para la vocera del Colectivo en Defensa de la Reserva Van Der Hammen, la propuesta del Alcalde no ha modificado su intensión de urbanizar el cordón norte “**la propuesta del alcalde acaba con la propuesta de la Van Der Hammer**, que es un área robusta, lo suficientemente ancha para que cumpla la función que debe cumplir, la reserva es una apuesta de ciudad sostenible” manifestó Sabina. (Le puede interesar; ["Los enredos de los funcionarios que están detrás de la urbanización de la Van Der Hammen"](https://archivo.contagioradio.com/los-enredos-de-los-funcionarios-que-estan-detras-de-la-urbanizacion-de-la-van-der-hammen/))

<iframe id="audio_20173393" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_20173393_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)
