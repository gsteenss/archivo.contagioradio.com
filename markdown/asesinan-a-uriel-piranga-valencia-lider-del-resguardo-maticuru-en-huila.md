Title: Asesinan a Uriel Piranga Valencia, líder del Resguardo Maticurú, en Huila
Date: 2019-06-26 13:03
Author: CtgAdm
Category: Líderes sociales, Nacional
Tags: Asesinato de líderes indígenas, asesinato de líderes sociales
Slug: asesinan-a-uriel-piranga-valencia-lider-del-resguardo-maticuru-en-huila
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/lider-social.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Foto: Archivo] 

En la mañana del pasado 23 de junio, el líder indígena Uriel Piranga Valencia, fue **hallado muerto con varias heridas causadas por arma blanca. El hombre de 40 años fue encontrado en el costado de la vía entre la vereda Las Minas y el municipio del Pital**, en el departamento del Huila.

Según Álvaro Piranga Cruz, consejero de la Organización Nacional Indígena de Colombia (ONIC), el líder indígena pertenecía al Resguardo Maticurú, ubicado en el noroccidente del Caquetá, y se había trasladado hacía 20 días al Huila para trabajar como recolector de café en este municipio por la falta de oportunidades económicas en su municipio.

"Había llamado días antes a su familia para el regreso de su comunidad y en ese tiempo fue que se presentó este impasse, llegando a ser asesinado por personas desconocidas", afirmó Piranga Cruz, resaltando que en este momento no se conoce de más detallas de los hechos dado que no hubo testigos del crimen.

Piranga Valencia** lideró el Resguardo Indígena de Maticurú por dos periodos consecutivos como representante legal del 2016 hasta el 2017**, además acompañó los procesos organizativos del Consejo Regional Indígena del Orteguaza Medio Caquetá (CRIOMC). (Le puede interesar: "[Valle del Cauca de luto por el asesinato del líder social Luis Carlos Valencia](https://archivo.contagioradio.com/valle-del-cauca-de-luto-por-el-asesinato-del-lider-social-luis-carlos-valencia/)")

El consejero de la ONIC sostuvo que el líder indígena nunca fue blanco directo de amenazas por  parte de grupos armados ilegales, sin embargo resaltó que últimamente había circulado un panfleto de disidencias de las FARC dirigida a los líderes indígenas, campesinos y mujeres en el municipio de Milán, Caquetá. También afirmó la presencia de las Autodefensas Gaitanistas de Colombia en la zona.

"El miedo y la zozobra es permanente en la región toda vez que hay unas disidencias que están por de lado de nuestros resguardos, sobre todo en el municipio de Milán y Solano, toda vez que los dirigentes tenemos que afrontar esta situación", dijo el consejero.

A pesar de sus denuncias de la situación de seguridad a la Unidad Nacional de Protección, la Gobernación de Caquetá y la Fiscalía, el consejero sostuvo que no han recibido respuesta. Actualmente, las autoridades indígenas solicitan que los entes pertinentes investiguen y aclarecen los hechos detrás de la muerte de Piranga Cruz.

<iframe id="audio_37647973" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_37647973_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
