Title: Fuerte polémica en el magisterio por pre acuerdo de FECODE con gobierno
Date: 2015-05-06 18:03
Author: CtgAdm
Category: Educación, Nacional
Tags: ade, Asociación Distrital de Educadores, fecode, Paro nacional de profesores, Pre acuerdo con gobierno nacional, Tarsicio Mora, Wiliam Agudelo
Slug: preacuerdo-de-fecode-con-gobierno-nacional-no-satisface-pero-es-un-avance
Status: published

###### Foto: Archivo 

Según Tarsicio Mora, encargado de DDHH de la **Federación Colombiana de Educadores**, los avances han sido varios, uno de ellos el ajuste salarial del 12% en 4 años a pesar de que el desbalance salarial está en 28%, la reforma a la evaluación que ahora sería más integral, esto luego de una evaluación de un comité designado para el tema. En cuanto a la salud también se designa un comité que escoja la Entidad Promotora de Salud que prestará el servicio luego de un nuevo proceso de licitación.

Sin embargo, **Wiliam Agudelo**, presidente de la **Asociación Distrital de Educadores** afirma que los acuerdos a los que se llegó son insuficientes aunque si representan algunos aavances y que se espera que el gobierno cumpla con los compromisos que está asumiendo.

Para varios sindicatos de profesores en varias regiones del país, el acuerdo al que llegó FECODE indicaría una traición porque **los puntos acordados no se acercan a las expectativas del magisterio** y no van a representar un cambio visible en las condiciones de dignidad de la educación, ni tampoco el incremento necesario para que el reajuste satisfaga las necesidades de miles de ellos.

En Colombia, un maestro o maestra con 5 años de experiencia devenga cerca de \$ 1’200.000 sin importar los niveles de pos grado o especialización con los que cuente. Con el aumento acordado para el próximo año el incremento será 3% por encima del ajuste al SMLV, lo que podría representar un aumento total de cerca de 70.000 pesos.

Hacia las 2: 00 am, FECODE anunció el pre acuerdo y emitió el siguiente comunicado…

[Preacuerdos Son Benéficos a Todo El Magisterio](https://es.scribd.com/doc/264431202/Preacuerdos-Son-Beneficos-a-Todo-El-Magisterio "View Preacuerdos Son Benéficos a Todo El Magisterio on Scribd")

<iframe id="doc_65129" class="scribd_iframe_embed" src="https://www.scribd.com/embeds/264431202/content?start_page=1&amp;view_mode=scroll&amp;show_recommendations=true" width="65%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="undefined"></iframe>  
Las reacciones de los maestros no se hicieron esperar, compartimos algunas de ellas...  
[![fecode-profesores-contagio-radio](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/05/fecodeprofes.jpg){.wp-image-8201 .aligncenter width="421" height="376"}](https://archivo.contagioradio.com/preacuerdo-de-fecode-con-gobierno-nacional-no-satisface-pero-es-un-avance/fecodeprofes/)
