Title: Presos políticos de las FARC exigen que se cumpla ley de amnistía pactada
Date: 2017-06-27 16:38
Category: Nacional, Paz
Tags: FARC, Huelga de hambre, Ley de Amnistia, presos politicos
Slug: presos-politicos-de-las-farc-exigen-que-se-cumpla-la-ley-de-amnistia-de-los-acuerdos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/06/presos-politicos-e1498597659899.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Univisión] 

###### [27 Jun 2017]

De los aproximadamente 3 mil prisioneros políticos que ha reconocido las FARC y que se encuentran en centros penitenciarios del país, **sólo 800 han hecho el tránsito a las Zonas Veredales**. Por medio de una huelga de hambre, le han exigido al Gobierno Nacional que cumpla lo pactado en la ley 1820 de 2016 sobre amnistía, indulto y tratamientos penales especiales.

Según Jesús Santrich, miembro del secretariado de las FARC, “hace falta que por lo menos **2 mil guerrilleros y personas acusadas de serlo salgan de las cárceles como se acordó en la Habana** y ahora están atrapados en una telaraña jurídica”. En solidaridad con los presos políticos, Santrich se unió a la huelga de hambre manifestando que “los jueces de ejecución de penas y los fiscales se han inventado toda clase de trabas para impedir la excarcelación”. (Le puede interesar: "[Presos políticos de las FARC en Florencia inician huelga de hambre](https://archivo.contagioradio.com/presos_politicos_farc_huelga_carcelaria/)")

El impedimento para la salida de los presos políticos de la guerrilla es para Santrich “**uno de los más claros incumplimientos de los acuerdos por parte del gobierno** que se dan alrededor del tema de la amnistía”. Afirmó además que la huelga es indefinida hasta tanto salgan todos los prisioneros de las diferentes cárceles del país. “Yo creo que hay que tener seriedad y dignidad en este tema porque no podemos seguir en esa actitud de saboteo que tienen ciertas ramas de la institucionalidad”.

**Presos políticos reclaman que se aplique ley de amnistía e indulto**

El representante a la Cámara Alirio Uribe, manifestó que “los integrantes de las FARC que están en las cárceles y fuera de ellas van a empezar a utilizar estos mecanismos de denuncia como nueva forma de protesta”. Así, los presos políticos esperan que **la huelga de hambre no sea vista como un capricho** sino como una forma de reclamar los derechos que se les otorgaron en el acuerdo de paz de la Habana. (Le puede interesar: "[Las preocupaciones de las FARC luego de la dejación total de armas](https://archivo.contagioradio.com/sin-armas-farc-exige-materializacion-de-los-acuerdos/)")

Según Omar Mayusa, miembro de las FARC detenido en la Cárcel La Modelo, “ya hemos cumplido con los requisitos que se nos exigían para empezar el proceso de amnistía y no nos han dejado salir”. Por esta razón **más de 1100 presos políticos en Colombia decidieron comenzar una protesta de desobediencia civil** en los centros de reclusión.

En la Cárcel La Modelo de Bogotá hay 100 prisioneros políticos que se han unido a la huelga de hambre. Ellos han manifestados que **“no nos han dejado tener contacto con la prensa y tampoco habíamos podido hacer pancartas”**. Ante esto solicitan se les garantice su derecho constitucional a la protesta y aseguraron que continuarán con la manifestación hasta que la Fiscalía y los jueces dejen de obstaculizar los procesos de libertad.

<iframe id="audio_19508716" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_19508716_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU).
