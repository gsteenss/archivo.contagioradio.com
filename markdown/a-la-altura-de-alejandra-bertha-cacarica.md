Title: A la altura de Alejandra, Bertha, Cacarica
Date: 2017-04-11 13:36
Category: Camilo, Opinion
Tags: 9 de abril, Alejandra Gaviria, cacarica, día de las víctimas
Slug: a-la-altura-de-alejandra-bertha-cacarica
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/04/victimas.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Contagio Radio 

#### **Por[Camilo de Las Casas](https://archivo.contagioradio.com/camilo-de-las-casas/) ** 

###### 10 Abr 2017 

Ocurrió el domingo 9 de abril día nacional de homenaje a las víctimas; se palpó días antes,  el 6 de abril, el día de la firma de los decretos; se demostró en lo que se conoce del acto privado de reconocimiento de responsabilidad de las FARC EP con parte de sus afectados en el caso de El Nogal; estas víctimas están en el lugar de este momento histórico.

Escuchar a Alejandra Gaviria, a Bertha Freis, a los perpetrados por la Operación Génesis de Cacarica que son CAVIDA es experimentar la sintonía de sus historias y las historias que ellos representan con el nuevo tiempo. Mientras muchos nos quedamos en los vericuetos políticos y jurídicos del Sistema Integral acordado en el punto 5 entre las FARC EP y el gobierno, mientras sigue aminalada la mesa de Quito, con avances poco impactantes y sin decisiones de  fondo, en esas voces se va tejiendo lo nuevo.

Su actitud y posición lejos de ser vengativa o retaliatoria está basada en un  movimiento de la historia que está llevando a esa nueva sensibilidad tejida en años de dolor- transformado en potencia de una nueva democracia basada en la Verdad: la convergencia de muchas verdades y el desmoronamiento de muchas creencias arraigadas en 70 años de una última fase de nuestra violencia armada y política.

Sus palabras nacen del alma, y estás se encuentra en un giro frente a sus perpetradores directos e indirectos; su cuerpo mental y espiritual, así lo expresa. Si la memoria histórica, oficializada y no,  fue dejando a un lado el que  las guerrillas eran una expresión de víctimas, que luego en la prolongación y degradación, que los fue ubicando en otro lugar; hoy, la exclusión de las víctimas de Estado está desmoronando la proyección de la buena democracia y sus buenos democrátas, abriendo paso a lo nuevo, a eso que incómoda porque hiede a la podredumbre de ésta democracia de fosas, de muertos a bala, o esas verdades reconocidas por las victimas de las guerrillas, van llegando al mismo lugar.

El 6 de abril la exclusión del Movimiento de Víctimas de Estado, de por lo menos una silla en el Centro de Memoria, día de la firma de los decretos presidenciales para la creación de la Comisión de la Verdad, la Comisión de Búsqueda de Desaparecidos y las 5 personas que elegirán los integrantes de la Jurisdicción de Paz,  demostró la posición introyectada en un amplio sector de la clase política de una realidad que quiere ser negada, pero al tiempo, lo que puede resultar siendo paradójico la exclusión como inclusión.

Lo significativo allí fue la persistencia de ese grupo representativo de víctimas de Estado para  hacerse oír, con altura, respetuosamente, en medio de una seguridad que se vio impedida de silenciar unos estribillos y de negar exhibir una pancarta. Una actitud honesta, de ver la realidad cómo es y de comprender la enunciación en medio de la solemnidad. Y allí con precisión, con respeto, impecablemente las víctimas de Estado se refirieron a través de Alejandra Gaviria con la expresión amigos a los militares. Unas palabras con consecuencias definitivas la verdad y toda la verdad con el  acceso a esos archivos macabros del terror de Estado.

Nunca fue un pedido de rodillas, fue una expresión de dignidad, de la superación de nuestro maniqueísmo ancestral desde el origen de la llamada independencia,  enemigo amigo, para decirnos, amigo distinto, pero amigo, al fin y al cabo somos de la misma patria. Una actitud ética de reconocer lo débilmente avanzado, pero avanzado para apelar al presidente a comprometerse con la verdad, con toda verdad sin excepción y con un cierre maravilloso utópico y real, invitando a todas y todos,  a proteger la vida de los que dejan las armas.

El mismo giro y actitud  de las víctimas que persisten en la verdad ante un expresidente, hoy congresista, que en el día de las víctimas, en el recinto del Congreso se desesperó ante determinadas verdades, esas que seguramente, escudriñan en su interior como perpetrador, y sobre las cuales el mejor camino fue huir, irse a la plaza pública, donde sus ciegos cortesanos en los medios, le abren de par a par a su discurso de baratijas falsas en las que es experto. Y las víctimas allí, en el respeto de su propia dignidad y la de su contradictor apelando éticamente en las palabras de Alejandra Gaviria. Tal cual, como la Carta Abierta de CAVIDA, 20 años después de la operación Génesis. “La cárcel nos distancia de la verdad, de lo que es importante saber más que para juzgar para saber la verdad,  y reconstruir la relación de todas y todos como hijos de una misma patria y matria, hijos al fin y al cabo con diferencias”

Y en esa misma sintonía,  Bertha Freis, una de las víctimas de El Nogal. Su proceso de acercamiento a la organización en transición a la vida civil para conocer la verdad; proceso contra viento y marea del que quieren estar distantes directivas del Club y que  socios quieren entorpecer y deslegitimar a nombre de una corporación, evitando de esa manera que algunas aberrantes situaciones no tan santas de los políticos se conozcan. Una actitud en la que ese grupo de víctimas, muchas que aún no salen a la luz, pero que tejen  su esperanza sobre una verdad cierta, para que les ayude a  sanar, lo que ya han ido sanando, y seguir su proceso de reconstrucción y de un proyecto de nación.

Desconocer  ahora tiene sus consecuencias en el subconciente individual y colectivo, consecuencias para los políticos, para los perpetradores, hay un cambio histórico, y estamos con posibilidad de verlo, con paramilitarismo y  con neoliberalismo ZIDRES, con denegación de muchos derechos entre ellos los ambientales. (Ver: “[Las víctimas dimos ejemplo de respeto y dignidad” Alejandra Gaviria](https://archivo.contagioradio.com/las-victimas-dimos-ejemplo-de-respeto-y-dignidad-alejandra-gaviria/))

Muchos temen a la verdad, por eso, huyen, vociferan y amenazan. Otros muchos se acomodan, se silencian sin arriesgar. Otros tantos reconocen y se reconocen en la construcción de la verdad, en su propio verse y en el reconocerse, como parte de esta historia de violencia, como parte de lo nuevo, que puede ser absolutamente transformante, a diferencia de lo ocurrido en la Gran Violencia. Aquellas victimas que superan el victimismo, el recurso a seguir sufriendo, para reconocerse ellas mismas como los sujetos de lo nuevo, más allá de sentencias judiciales, de la impunidad social o de que los propios perpetrados algún día compartan la verdad que ellos se niegan a reconocer y a aceptar, allá ellos, se excluyen per se.

 Temer a la verdad es temer al alma que despierta, a la posibilidad de una democracia profunda que se hilvana desde el deshecho, desde la ruptura con las creencias que nos llevaron a percibirnos como enemigos en una misma patria, a comprender que igual en una democracia profunda el poder conciente nace de una nueva mentalidad del corazón hacia la justicia socio ambiental. El asunto de la exclusión es decisión de ellos, de ahí su fracaso de proyecto de país, lo que nos queda y nos corresponde es que a través de miles de Alejandras, de Berthas, y de Cacaricas,  incluirnos con absoluta altura y dignidad en este momento único de la historia.

#### [**Leer más columnas de Camilo de las Casas**](https://archivo.contagioradio.com/camilo-de-las-casas/) 

------------------------------------------------------------------------

######  Las opiniones expresadas en este artículo son de exclusiva responsabilidad del autor y no necesariamente representan la opinión de Contagio Radio.
