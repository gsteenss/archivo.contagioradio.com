Title: Adhesión de liberales a Duque es un "suicidio asistido del partido"
Date: 2018-05-31 14:18
Category: Nacional, Política
Tags: Iván Duque, Jorge Iván Duque, liberalismo, Partido Liberal
Slug: adhesion-de-liberales-a-duque-es-un-suicidio-asistido-del-partido
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/05/Diseño-sin-título-2.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Hora 7] 

###### [31 May 2018] 

Luego de que el director del Partido Liberal expresara públicamente su apoyo para la segunda vuelta electoral al candidato del **Centro Democrático, Iván Duque,** algunos liberales manifestaron su inconformidad y sentenciaron la muerte del tradicional partido. Sus integrantes afirmaron sentirse traicionados y recordaron que Duque no representa los valores liberales.

Senadores como Juan Manuel Galán afirmaron que, con la alianza entre liberales y la candidatura de Duque, **“se está traicionando al liberalismo colombiano”**. Por esto, indicó que parte del liberalismo que está “desencantado y decepcionado” va a buscar otros caminos de representación y participación.

### **El Uribismo ha representado los valores contrarios al liberalismo** 

De acuerdo con Andrés Guzmán, integrante del partido Liberal, con la decisión de Gaviria “ se está renunciando a los **valores y los principios** que ha tenido históricamente el partido”. Afirmó que, en la coyuntura política del país en los últimos años, el Uribismo ha representado de manera contraria los ideales liberales.

Aseguró que la defensa de las libertades, de los derechos y la división de poderes **“son la bandera del partido Liberal”** por lo que la toma de decisiones de algunos de los integrantes del partido, “es un contrasentido”. La decisión de Gaviria, que afirmó fue a puerta cerrada,  la consideró como “un suicidio asistido al partido”. (Le puede interesar:["Los retos al Congreso del Partido Liberal"](https://archivo.contagioradio.com/los-retos-al-congreso-del-partido-liberal/))

### **Apoyo a Duque fortalece la crisis que tiene el partido Liberal** 

Adicional a esto, recordó que el partido Liberal ha venido afrontando una gran crisis de **institucionalidad** que se ha evidenciado en el imaginario de los colombianos. La identidad liberal también se ha visto afectada y que se **refleja en los actos de corrupción** de algunos de sus integrantes.

Esto no ha generado desconfianza tan sólo en la ciudadanía sino también en los militantes del liberalismo. Guzmán enfatizó en que, por estas situaciones, algunos integrantes del partido **se han alejado del mismo** por lo que el día de ayer realizaron una manifestación en el primer piso del hotel Tequendama donde “catalogamos la adhesión al Uribismo como el entierro del partido”.

A pesar de la crisis que afrontan, sus integrantes han reiterado que las bases del partido son claras y **“hoy está amenazado por un sector ultra conservador**, que reduce las libertades y amenaza los escenarios donde se han defendido los derechos”. Guzmán, recordó que el partido “tenía que haber leído el momento histórico del país para poder presentarle a la ciudadanía un nuevo escenario”. (Le puede interesar:["Los partidos se convirtieron en empresas electorales: Carlos Medina"](https://archivo.contagioradio.com/46707/))

### **Bases liberales trabajarán por un acuerdo sobre lo fundamental** 

Frente a lo ocurrido, los liberales que no se van a adherir a la candidatura de Iván Duque, afirmaron que van a trabajar por “construir un acuerdo sobre lo fundamental”. A manera personal, Guzmán indicó que el candidato **Gustavo Petro** debe ampliarse hacia el centro “considerando un nombre para el Ministerio de Hacienda que refleje tranquilidad para algunos sectores”.

Recordó que hay una posibilidad para que las facciones del nuevo liberalismo concuerden con la Colombia Humana contando con que Petro entienda la coyuntura que vive el país. Enfatizó en que “Colombia **necesita un nuevo tiempo** y el liberalismo está más vigente que nunca”.

<iframe id="audio_26286159" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_26286159_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
