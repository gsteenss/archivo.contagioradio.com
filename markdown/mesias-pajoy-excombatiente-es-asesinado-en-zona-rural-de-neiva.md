Title: Mesías Pajoy excombatiente es asesinado en zona rural de Neiva
Date: 2020-12-24 10:21
Author: CtgAdm
Category: Actualidad, Paz
Tags: Asesinatos, excombatiente, FARC, Firmante de paz, Neiva
Slug: mesias-pajoy-excombatiente-es-asesinado-en-zona-rural-de-neiva
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/12/WhatsApp-Image-2020-12-24-at-9.54.11-AM.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

Este 24 de diciembre se registró una nueva masacre, en esta perdieron la vida tres personas, en una acción armada registrada en zona rural de Neiva, departamento de Huila. Según el Instituto de Estudios el Desarrollo y la Paz ([Indepaz](http://www.indepaz.org.co/informe-de-masacres-en-colombia-durante-el-2020/)).

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El hecho se registró e**ste miércoles 23 de diciembre sobre las 12:00 de la noche en la vereda Bajo Horizonte, corregimiento de Chapinero, zona rural de Neiva**, las tres víctimas fueron tres hombres, quienes perdieron la vida luego de ser atacados con arma de fuego.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Una de las víctimas fue identificada como **Mesías Pajoy Sabogal , quién había dejado las armas** en el año 2010 luego de pertenecer a la columna móvil Teófilo Forero Castro. ([Exjefes paramilitares y de las FARC-EP piden ser escuchados por la Comisión de la Verdad](https://archivo.contagioradio.com/exjefes-paramilitares-farc-piden-ser-escuchados-comision-verdad/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

La identidad de las otras dos personas que acompañaban al excombatiente se desconoce sin embargo fuentes locales indican que se pudo tratar de los trabajadores de la finca que el excombatiente había adquirido meses atrás y en donde sembraba café y mango.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Esto según información suministrada por el hijo de Pajoy al Diario del Huila, indicando que además a la fecha desconocían si existían amenazas en contra de su padre por parte de algún grupo o estructura armada.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Al consejo de seguridad que se llevará en las próximas horas en la ciudad de Neiva, se suma el llamado que hace el partido FARC para que se respete la vida de los y las firmantes de paz quienes denuncian qué **desde la Firma del Acuerdo de Paz en 2016, han sido asesinado más de 200 excombatientes**.

<!-- /wp:paragraph -->

<!-- wp:embed {"url":"https://twitter.com/PartidoFARC/status/1320337595612696577","type":"rich","providerNameSlug":"twitter","responsive":true,"className":""} -->

<figure class="wp-block-embed is-type-rich is-provider-twitter wp-block-embed-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/PartidoFARC/status/1320337595612696577

</div>

</figure>
<!-- /wp:embed -->

<!-- wp:block {"ref":78955} /-->
