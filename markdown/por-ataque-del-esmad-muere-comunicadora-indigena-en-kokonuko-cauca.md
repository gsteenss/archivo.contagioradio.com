Title: Por ataque del ESMAD muere comunicadora indígena en Kokonuko, Cauca
Date: 2017-10-09 12:56
Category: Movilización, Nacional
Tags: Efigenia Vásquez, liberación de la madre tierra
Slug: por-ataque-del-esmad-muere-comunicadora-indigena-en-kokonuko-cauca
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/10/renacer-kokonuko.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:Efigenia Vásquez] 

###### [09 Oct 2017] 

Indígenas del resguardo Kokonuko, en Cauca, que se encontraban en proceso de libración de la Madre Tierra  del predio Agua Tibia, **denunciaron el asesinato de la comunicadora Efigenia Vásquez, a manos del Escuadrón Móvil Antidisturbios ESMAD**. Los indígenas se encontraban en este lugar para exigirle al gobierno nacional que cumpliera lo pactado sobre la devolución de estas tierras al resguardo.

\[KGVID\]https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/10/WhatsApp-Video-2017-10-09-at-4.56.24-PM.mp4\[/KGVID\]

“La comunidad estaba en el ejercicio de retomar el espacio que les corresponde y fueron atacados por el ESMAD, de manera desenfrenada”, expresó Viviana Ipia, vicegobernadora del Resguardo de Kokonuko, además, agregó que las confrontaciones iniciaron sobre las **8:30 de la mañana y finalizaron hacia las 6 de la tarde, en donde se pudo establecer que Efigenia Vásquez había sido asesinada.**

Los indígenas denunciaron que en la arremetida del ESMAD, se usaron gases lacrimógenos y armas de fuego, afirmando que esta no sería la primera vez en que mueren indígenas a manos de integrantes de la Fuerza Pública, “**tenemos los casquillos de balas con los cuales arremetieron contra la comunidad”**, informó Ipia.

De igual forma, la vicegobernadora del Resguardo Kokonuno, manifestó que el ESMAD le ha dicho a los comuneros, que tienen la orden de disparar si llegan a ingresar al predio Aguas Tibias. Los enfrentamientos dejaron un reporte de **60 indígenas lesionados, 3 de ellos de gravedad y 1 persona asesinada**. (Le puede interesar: ["Policía ataca a misión humanitaria en el lugar de la masacre de Tumaco"](https://archivo.contagioradio.com/policia-ataca-mision-humanitaria-47729/))

### **Proceso de Liberación de la Madre Tierra en el Cauca** 

Viviana Ipia  señaló que el ejercicio **de liberación de la Madre Tierra se viene realizando desde hace más de 6 añ**os, con la finalidad de recuperar territorios que están en manos de terceros, como es el caso del predio Aguas Tibias, sobre el que ya se había pactado, con el gobierno Nacional, la compra y entrega de este proyecto para el 23 de septiembre.

En vista de los incumplimientos Ipia afirmó que los indígenas decidieron retomar el territorio para que el gobierno nacional cumpliera con lo que había acordado, razón por la cual en el proceso estaban **participando alrededor de 5 mil indígenas**. (Le puede interesar: ["Cinco indígenas heridos en represión del ESMAD en resguardo de Kokonuko, Cauca"](https://archivo.contagioradio.com/cinco-indigenas-heridos-en-represion-de-esmad-resguardo-kokonuko-en-cauca/))

Frente a esta situación los indígenas han expresado que continuaran en el territorio hasta que el gobierno cumpla con la devolución de este predio a los comuneros del Resguardo indígena de Kokonuko, **sin embargo, la vicegobernadora alertó sobre la llegada de más integrantes de la Fuerza Pública al lugar, que podría arriesgar la vida de los indígenas**.

<iframe id="audio_21355257" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_21355257_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

\

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
