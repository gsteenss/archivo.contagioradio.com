Title: Resguardo provincial de la Guajira denuncia restricción de acceso al agua por parte del Cerrejón
Date: 2017-09-08 15:26
Category: DDHH, Nacional
Tags: Cerrejón, El Cerrejón en La Guajira, Guajira, Mineria
Slug: comunidades-de-la-guajira-denuncian-restriccion-en-las-vias-por-parte-del-cerrejon
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/05/guajira2-e1494358244369.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Nación Wayúu] 

###### [08 Sept 2017] 

Las comunidades indígenas que habitan en la Guajira, **denunciaron el bloqueo y la restricción en las vías** que comunican al Resguardo Provincial en Barrancas con el Río Ranchería por parte del operador de carbones del Cerrejón. Han manifestado que no tienen agua potable ni medios de transporte para que los niños vayan a estudiar.

De acuerdo con las comunidades, el día de ayer **"una máquina de la multinacional bloqueó las vías de acceso al río".** Por esto no se han podido abastecer de agua que cargan en vehículos y motos. Manifestaron que el Río Ranchería es uno de sus sitios sagrados y que "el Cerrejón se ha creído dueño del río y nos está atropellando".

Según Felipe Rodríguez, presidente del Comité Cívico por la Dignidad de la Guajira, “han venido cerrando el acceso al Río Ranchería como si fuera propiedad privada de la multinacional, en la Guajira **el agua se ha convertido en un instrumento para controlar y desplazar a las comunidades**”.

Las quejas de las comunidades por este tipo de acciones no son nuevas, en repetidas ocasiones han manifestado que la minería a gran escala en el Departamento ha destruido por ejemplo, **12 mil hectáreas de bosque**. (Le puede interesar: ["La Guajira alista paro cívico indefinido"](https://archivo.contagioradio.com/guajira-alista-paro-indefinido/))

También han dicho que **hay una crisis de salud pública** producto la detonación de dinamita y el fraccionamiento de las rocas que "suelta un polvillo que inhalan las personas o lo consumen en el agua contaminada".

### **Cerrejón es la autoridad en la  Guajira** 

Rodríguez indicó que la falta de presencia estatal en el departamento ha sido reemplazada por el Cerrejón en la medida que **“acumula poder políticos mediático y militar que acalla a todo el mundo”.** Dijo también que el gobierno departamental ha estado al servicio del Cerrejón por lo que las comunidades no tienen el apoyo de las autoridades.

Inclusive, hizo alusión a que **“el periodismo también es cómplice de las multinacionales**, hay un premio el Departamental de Periodismo Cerrejón y están pendientes de la pauta que les da las las empresas mineras”. (Le puede interesar: ["Costos ambientales de acción de Cerrejón en la Guajira siguen en aumento"](https://archivo.contagioradio.com/cerrejon-guajira-costos-ambientales/))

Finalmente, Rodríguez recordó que las **comunidades están estructurando una movilización** a lo largo del departamento para “exigir respeto a los derechos”. Comentó que el Gobierno Nacional ha creado una política minera que ha ayudado a desplazar a miles de guajiros de sus territorios destrozando sus hogares, “la minería en el Cerrejón ha desplazado a más personas que los paramilitares o las guerrillas”.

En su página web la empresa Cerrejón, publicó un comunicado en que afirma que se realizaron trabajos de restauración de "una Berma como parte de los trabajos de mantenimiento que la empresa adelanta en sus predios" y el resguardo podrá seguir teniendo acceso peatonal al sector ignorando que los carros y las motos son el único medio de trasnporte del agua del río al resguardo.

Sin embargo no se ha tenido confirmación por parte de las comunidades de que ya pudieron tener acceso al agua y persiste la pregunta en torno a la privatización del río ranchería que estaría en terrenos de la empresa.

<iframe id="audio_20771667" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_20771667_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU).
