Title: Con cacerolas sigue el movimiento por la renuncia del Fiscal Néstor Martínez
Date: 2019-01-15 11:31
Author: AdminContagio
Category: Movilización
Tags: cacerolazo, Fiscal, Nestor Humberto Martínez
Slug: prepare-la-cacerola-para-exigir-este-17-de-enero-la-renuncia-del-fiscal-nestor-martinez
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/01/Planton-fiscal-Martinez-8-770x400.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [15 Ene 2019] 

El próximo 17 de enero se realizará el "cacerolazo contra el Fiscal" una segunda actividad que continúa exigiendo la renuncia de Néstor Humberto Martinéz. La cita será en  cada una de las sedes de este organismo a las 6:00pm, en donde de acuerdo con el colectivo \#RenuncieFiscal, esperan que haya un **gran "estruendo" en rechazo a que Martínez continúe en este cargo, pese a estar vinculado a escándalos de corrupción**.

Este nuevo momento de protesta es una respuesta al silencio de Martínez y los intentos por deslegitimar la movilización ciudadana que se tomó las sedes de la Fiscalía el pasado 11 de enero, por parte de "defensores y amigos del fiscal", razón por la cual, quienes promueven la iniciativa, invitaron a esos actores **"a ser responsables con sus palabras y dar el debate sobre los conflictos de interés de Néstor Humberto Martínez con argumentos y sin calumnias".**

### **Cacerolas en contra de la corrupción** 

El plantón del próximo 17 de enero se realizará en cada una de las sedes de la Fiscalía General de la Nación, en los distintos municipios y ciudades del país. Sin embargo en esta ocasión, se está convocando a la ciudadanía a que participe con sus cacerolas, de esta forma, si no puede asistir al plantón, **podrá hacer ruido desde donde quiera que este con su cacerola, exigiendo la renuncia del Fiscal.**

De igual forma, el colectivo informó que en los próximos días estará compartiendo una ruta de movilización y actividades en las que espera que toda la ciudadanía se sume y respaldaron la demanda de nulidad interpuesta contra la selección del Fiscal, interpuesta por diversas organizaciones defensoras de derechos humanos. (Le puede interesar: ["Las razones para pedir la nulidad en elección de Fiscal Néstor Humberto Martínez"](https://archivo.contagioradio.com/piden-nulidad-en-eleccion-del-fiscal-nestor-martinez-por-ocultar-informacion/))

###### Reciba toda la información de Contagio Radio en [[su correo]
