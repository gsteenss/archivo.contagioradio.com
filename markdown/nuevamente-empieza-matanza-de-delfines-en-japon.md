Title: Nuevamente empieza matanza de delfines en Japón
Date: 2015-09-07 13:51
Category: Ambiente, Voces de la Tierra
Tags: Animales, cazar
Slug: nuevamente-empieza-matanza-de-delfines-en-japon
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/delfines.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [foto:www.nuestromar.org] 

###### [7 Sep 2015] 

Nuevamente se  inicia la época de caza en Taiji Japón, donde **anualmente se caza a más de 20.000 delfines entre septiembre y abril. ** Los  delfines **son descuartizados y vendidos como comida, otros son llevados a parques acuáticos para realizar espectáculos**.

**El motivo de esta matanza se debe a la caza de ejemplares en cautiverio y la producción cárnica,** esta consiste en transportar al animal a un matadero donde son degollados y desangrados, en los últimos años este tipo de producción ha incrementado notablemente los índices de su consumo y  demanda.

Aunque se asegura que la mayoría de ciudadanos japoneses no están enterados de esta situación, diversas organizaciones están alarmadas por este crimen, por lo que colectivos como One Voice, Earth Island Institute y Elsa Nature Conservancy han viajado a Taiji para reportar las sangrientas matanzas de delfines.

Debido a la indignación que causa esta cruel actividad, cada año, las organizaciones defensoras del ambiente y los animales realizan protestas en las principales ciudades de España y Latinoamérica.
