Title: Sigue el riesgo para el arroyo Bruno a pesar de fallos de la Corte Constitucional
Date: 2017-12-22 16:47
Category: Ambiente, Nacional
Tags: Corte Constitucional, El Cerrejón, La Guajira
Slug: cerrejon_arroyo_bruno_mineria_corte_constitucional
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/04/Arroyo-Bruno.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Diario Norte] 

###### [22 Dic 2017] 

La empresa Carbones El Cerrejón continúa incumpliendo los pronunciamientos de la Corte Constitucional, frente a la necesidad de proteger el agua, la salud y la seguridad alimentaria de comunidades que dependen del arroyo Bruno. Así lo asegura Angela Ortíz, integrante del colectivo Fuerza Mujeres Wayúu.

Dicho pronunciamiento del alto tribunal, se da **ante la solicitud de las comunidades indígenas Wayúu de Paradero y La Gran Parada en la Guajira**, que pidieron protección al arroyo Bruno y su ecosistema frente al riesgo de la expansión de la minería hacia su cauce. Así pues, la Corte constató que en el desarrollo de esa actividad existen “Incertidumbres sobre los impactos ambientales y sociales del proyecto de modificación parcial del cauce del Arroyo Bruno”. ( Le puede interesar: [Cerrejón deberá suspender actividades mineras sobre arroyo Bruno)](https://archivo.contagioradio.com/?p=49554)

### **La orden de la Corte Constitucional** 

En ese sentido, la Corte ordenó que se realice un estudio técnico independiente ambiental, y la incorporación al Plan de Manejo Ambiental Integral vigente -PMAI- teniendo en cuenta: las consecuencias de la actividad minera en el ecosistema y puntualmente en el arroyo Bruno, los efectos del cambio climático y del calentamiento global en La Guajira, así como las intervenciones que históricamente Cerrejón ha efectuado sobre los cuerpos de agua.

Asimismo, específicamente sobre el arroyo el estudio debía contemplar la garantía de las funciones culturales, de abastecimiento, regulación y mantenimiento que cumple esta fuente de agua, el impacto en la oferta hídrica, las proyecciones de Cerrejón para intervenir en el futuro otros tramos del arroyo Bruno y los efectos acumulativos de estas intervenciones progresivas en el mismo.

Finalmente el estudio debe refirse al valor biológico de la cuenca del arroyo en el contexto del **Plan de Ordenación y Manejo de Cuenca Hidrográfica del Río Ranchería como del Esquema de Ordenamiento Territorial del municipio de Albania.**

**Los incumplimientos de El Cerrejón**

Pese a las observaciones que ya ha hecho la Corte, las comunidades aseguran que "El Cerrejón no las ha escuchado y el estado colombiano no las atiende con la debida diligencia: ha desconocido el principio de precaución y la necesaria prevención frente a los daños que la minería ha ocasionado durante casi 40 años de operación y frente a los riesgos que se mantienen con la autorización de expansión de esa actividad. Tampoco se ha analizado que el pueblo Wayúu ha perdido gran parte de su territorio y según la misma Corte se encuentra en riesgo de extinción".

De acuerdo con Fuerza Mujeres Wayúu, pese a que la Ley establece que todo proyecto extractivo debe estar precedido y acompañado de la certeza sobre su viabilidad ambiental, social y económica, en La Guajira no se ha llevado a cabo debidamente las consultas previas para preguntar a las comunidades si están de acuerdo o no con dicho proyecto.

### **El arroyo Bruno no está seco ** 

**"Vemos que por un lado va el pronunciamiento de la Corte, y por otro, Corpoguajira y la ANLA  que son directos responsables de que no se cumplan los fallos",** dice Ángela Ortíz, y agrega que aún hace falta un fallo definitivo del alto tribunal sobre el Arroyo Bruno. No obstante, la Corte ya ha verificado que, contrario a lo que dice el Plan de Manejo Ambiental, el arroyo no está seco y abastece a **comunidades wayúu, afrodescendientes y cabeceras municipales como las de Albania y Maicao**.

Además, pese a lo que ha dicho el alto tribunal, justo antes de una de sus primeras declaraciones sobre el arroyo que le daban la razón a las comunidades, la empresa ya había adelantado al canalización del arroyo, de manera que solo faltaría una etapa de las obras para la desviación del afluente.

Finalmente, Ortiz, denuncia que hace tres días se conoció que uno de los socios de El Cerrejón anunció que la empresa podría estar pensado en cerrar la mina, ya que no están de acuerdo con la política contra el cambio climático, de manera que esto preocupa a las comunidades **"pues no se conocería el plan de cierre de la mina, porque cuando se acabe el carbón o ya no sirva sacarlo van a dejarnos los huecos y la enfermedad en La Guajira".**

<iframe id="audio_22804408" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_22804408_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
