Title: Indonesia y Colombia los países que menos invierten en educación
Date: 2015-11-27 12:29
Category: Educación, Nacional
Tags: Derecho a la educación, Informe OCDE, la educacion en colombia
Slug: indonesia-y-colombia-los-paises-que-menos-invierten-en-educacion
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/11/informe-ocde.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [foto: Contagio Radio] 

###### [27 Nov 2015] 

"**Education at a Glance 2015", el más reciente informe** publicado por la Organización para la Cooperación y el Desarrollo Económico (OCDE), afirma que Colombia es el segundo país (de los que el organismo tuvo en cuenta para elaborar el informe) que menos invierte en educación, sólo por detrás de Indonesia.

De acuerdo con el documento, Colombia invierte 3 mil dólares anuales por habitante en materia de educación, lo que equivaldría a unos 10 millones de pesos aproximadamente. Pero la cifra caería considerablemente si se tiene en cuenta que el estudio se realizó desde 2012, año en que el dólar estaba por debajo de los 2000 pesos colombianos. En ese orden de ideas, la cifra rondaría los 8 millones de pesos.

A pesar de ello no deja de ser preocupante que la inversión que hace Colombia en educación y la calidad de la misma, aún están lejos de compararse con las demás naciones que se encuentran en la mitad y la punta del escalafón, a pesar de que una de las banderas de la administración consiste en mejorar y cubrir las necesidades de los colombianos en esta materia. Adicionalmente, el promedio planteado por la OCDE, es decir, la barrera que debe alcanzarse o superarse en materia de destinación de recursos a educación según el organismo es de 10 mil doscientos veinte dólares anuales por habitante.

El estudio afirma que “la demanda de alta calidad en la educación, puede significar altos costos de inversión por estudiante”. Esta hipótesis podría demostrarse con 16 de los 34 países miembros que están en el listado y pasan el promedio de la OCDE, entre ellos están Luxemburgo, Suiza y Noruega; que encabezan el ranking y que son reconocidos por la calidad y cobertura en educación.

[![Colombia-educacion1-593x600](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/11/Colombia-educacion1-593x600.jpg){.aligncenter .size-full .wp-image-17939 width="593" height="600"}](https://archivo.contagioradio.com/indonesia-y-colombia-los-paises-que-menos-invierten-en-educacion/colombia-educacion1-593x600/)
