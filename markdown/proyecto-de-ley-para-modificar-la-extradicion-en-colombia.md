Title: Estas son las razones para modificar Ley de extradición en Colombia
Date: 2015-08-11 15:43
Category: Entrevistas, Judicial, Política
Tags: Congreso de la República, DEA, Estados Unidos, Extradición en Colombia, FARC, paramilitares, Polo Democrático Alternativo, proceso de paz, Víctor Correa
Slug: proyecto-de-ley-para-modificar-la-extradicion-en-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/08/dea-agent-drogas-narcotrafico.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: [www.losreportesdelichi.com]

<iframe src="http://www.ivoox.com/player_ek_6539622_2_1.html?data=l5qgm5uWdo6ZmKiakpmJd6KnkpKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRmoa3lIqupsjYs9OfpNTf1MrFaZO3jNXfx9jJstXVjLXf0d7Jp9XjjMnSjbHJvYzkwtfOjdLTqMraysjO1JKJe6ShpNTb1sbLrdCfs8bRy9SPcYarpJKh&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Víctor Correa, representante a la Cámara] 

###### [11 Ago 2015] 

El representante a la cámara del Polo Democrático  Alternativo, Víctor Correa, radicó la semana pasada un proyecto de ley que modifica la extradición en Colombia. De acuerdo con el congresista se trata de una iniciativa que **fortalezca la soberanía del país y contribuya con el esclarecimiento de la verdad, construcción de la memoria y garantía de no repetición** en los crímenes cometidos en el marco del conflicto armado.

Según el representante, la actual figura de la extradición implica **violaciones a los derechos humanos, (incluso los de los niños, niñas y adolescentes),** además viola el **debido proceso**, impidiendo que la justicia colombiana trabaje.

Ante las múltiples críticas, Correa explica que este proyecto de Ley **no está dirigido a evitar la extradición de los jefes de las FARC**, ya que se trata de una modificación integral todo el tema relacionado con la extradición y la forma como esta opera en el país.

“Muchos de los narcotraficantes  extraditados hacia Estados Unidos, han terminado libres como parte de los pre acuerdos”, dice el congresista.

Por otro lado, afirmó que es contradictorio que partidos como el Centro Democrático estén en contra de este proyecto de Ley “**cuando el propio Álvaro Uribe estuvo en contra de la figura de extradición”.**

 
