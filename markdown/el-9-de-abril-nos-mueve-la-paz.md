Title: El 9 de abril, nos mueve la paz
Date: 2015-04-14 07:32
Author: CtgAdm
Category: Movilización, Otra Mirada, Paz, Video
Tags: 9 de abril, Cese al fuego, ELN, FARC, frente amplio, habana, marcha patriotica, onic congreso de los pueblos, paz, proceso de paz
Slug: el-9-de-abril-nos-mueve-la-paz
Status: published

###### Foto: Contagio Radio 

En el marco de la conmemoración del 9 de abril, en Colombia, se llevaron a cabo movilizaciones en todo el país, en apoyo al proceso de paz que se adelanta con la insurgencia de las FARC y el que se espera que inicie con el ELN, exigiendo el cese bilateral de hostilidades, y por las víctimas del conflicto armado.

\[embed\]https://youtu.be/kfNN0H-5iKU\[/embed\]
