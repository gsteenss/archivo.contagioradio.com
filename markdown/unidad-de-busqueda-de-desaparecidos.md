Title: En 2019 Unidad de Búsqueda de Desaparecidos llegará a 17 territorios
Date: 2019-03-21 15:12
Category: Nacional, Paz
Tags: conflicto armado, Unidad de Búsqueda de personas dadas por desaparecidas
Slug: unidad-de-busqueda-de-desaparecidos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/03/Entrega-restos-oseos-meta-210.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Archivo Contagio Radio 

###### 21 Mar 2019

Tras cumplirse un año de su lanzamiento, la **Unidad de Búsqueda de Personas dadas por Desaparecidas (UBPD)** está lista para adentrarse en 10 territorios con el propósito de establecer contacto con las víctimas, organizaciones y demás actores del conflicto, centralizar la información y comenzar una labor de pedagogía con las poblaciones, acciones que les permita desarrollar su labor durante los próximos 20 años.

**Luz Marina Monzón, directora de la UBPD** afirma que el corazón de la entidad para encontrar a los desaparecidos es la consecución de información, lo que puede ayudar a establecer hipotésis a las familias que continúan buscando el paradero de sus seres queridos, "aunque se encuentren cuerpos, no se han encontrado desaparecidos hasta que no se les dé una identidad", señala.

Monzón  es enfática en indicar que no existe una cifra exacta sobre las personas que desaparecieron como resultado del conflicto armado, pero se respalda en cifras del **Centro Nacional de Memoria Histórica que hace referencia a 83.036 registros de víctimas**, Medicina Legal, que registró  **142.653 casos desde 1950 de los cuales 97.176 continúan sin resolver y de la** **Unidad para la Atención y Reparación Integral a las Víctimas, la cual registra 171.150 víctimas** directas o indirectas de desaparición.

### **Avances ** 

Dentro de los resultados alcanzados en lo que va de 2019, la directora de la entidad destaca la construcción e importancia del marco jurídico que permitirá funcionar a la Unidad, además de informar sobre las 261 solicitudes individuales de búsqueda en las que están trabajando y las 463 acciones humanitarias que significarían una búsqueda colectiva de personas.

El avance más esperado llegará en el mes de mayo, cuando la Unidad de Búsqueda empezará su despliegue territorial en **Barranquilla, Sincelejo, Cali, Villavicencio, Rionegro,  Apartadó, San José del Guaviare, Barrancabermeja, Puerto Asís y Cúcuta **y se espera que para finales del año, la entidad cuente con equipos en un total de 17 ciudades.

Tales equipos territoriales tendrán como objetivo el establecimiento de relaciones de confianza con las personas que buscan a loa desaparecidos, así como de coordinación a nivel local y poder realizar pedagogía sobre lo qué significa el mecanismo y la oportunidad que tienen los colombianos de aportar en este proceso.

Dentro de los 10 puntos en los que comenzarán a realizar trabajo de campo, la directora explicó que esperan ubicar tres satélites que faciliten la labor de los equipos en **Tumaco, Popayán y Buenaventura, un** [**satélite**]** en el Catatumbo y uno en La Dorada. [(Lea también: Así funcionará la Unidad de Búsqueda de Personas Desaparecidas)](https://archivo.contagioradio.com/asi-funcionara-la-unidad-busqueda-personas-desaparecidas/)**

Asímismo indicó que voluntariamente, 10 personas que pertenecieron a las FARC y participaron de manera directa en el conflicto armado han decidido unirse a esta labor, la información que aporten podrá conmutarse con la de organizaciones de la sociedad civil, entidades del  Estado, 5 informes ejecutivos de 7 organizaciones, financiados por la agencia de cooperación internacional GIZ  y  los aportes de más de 700 familias que se han reunido con la UBPD para este propósito.

### **La prueba piloto de la Unidad de Búsqueda de Desaparecidos** 

Para concluir, Monzón indicó que desde la UBDP se han desarrollado pruebas piloto en Nariño y Norte de Santander, que permitirían el diagnóstico de los casos de cuerpos que han sido sometidos a necropsia médico-legal en Medicina Legal y la Fiscalía, y que continúan en condición de no identificados. Si los resultados son positivos, la Directora espera poder  **identificar cerca de  2.100 cuerpos:  500 en Nariño y 1.600 en Norte de Santander.**

###### Reciba toda la información de Contagio Radio en su correo o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por Contagio Radio. 
