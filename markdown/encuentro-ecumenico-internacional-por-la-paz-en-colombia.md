Title: Encuentro ecuménico internacional por la paz en Colombia
Date: 2015-04-08 18:30
Author: CtgAdm
Category: Nacional
Slug: encuentro-ecumenico-internacional-por-la-paz-en-colombia
Status: published

Conferencia pública de apertura: Iglesias y la construcción de paz en contextos de conflictos armado" que se desarrollará en Bogotá el 8 de abril entre las 5: 30 p.m y las 8: 30 p.m con la participación de diferentes ponentes de Colombia y el mundo.  
<iframe style="border: 0; outline: 0;" src="http://cdn.livestream.com/embed/contagioradioytv?layout=4&amp;height=340&amp;width=560&amp;autoplay=false" width="560" height="340" frameborder="0" scrolling="no"></iframe>

<div style="font-size: 11px; padding-top: 10px; text-align: center; width: 560px;">

Watch [live streaming video](http://original.livestream.com/?utm_source=lsplayer&utm_medium=embed&utm_campaign=footerlinks "live streaming video") from [contagioradioytv](http://original.livestream.com/contagioradioytv?utm_source=lsplayer&utm_medium=embed&utm_campaign=footerlinks "Watch contagioradioytv at livestream.com") at livestream.com

</div>
