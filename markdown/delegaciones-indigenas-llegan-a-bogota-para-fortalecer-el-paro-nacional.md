Title: Delegaciones indígenas llegan a Bogotá para fortalecer el Paro Nacional
Date: 2019-11-28 20:21
Author: CtgAdm
Category: DDHH, Paro Nacional
Tags: Cauca, Concejo Regional Indígena de Caldas, Concejo Regional Indigena del Huila, guardia indígena, indigena, ONIC, Paro Nacional, pueblos indígenas
Slug: delegaciones-indigenas-llegan-a-bogota-para-fortalecer-el-paro-nacional
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/CRIC-1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: CRIC Colombia Cauca] 

[Las delegaciones de los pueblos indígenas de diversas regiones del país, llegaron a la capital en la tarde de este 28 de noviembre para unirse a las organizaciones y manifestantes en el marco del Paro Nacional, que cumple ya ocho días consecutivos de protesta en las calles de ciudades y municipios a lo largo del territorio colombiano.]

[Para tal objetivo, se reunieron el **Concejo Nacional Indígena del Cauca (CRIC)** —en el marco de la Minga del Suroccidente Colombiano — donde se encuentran el **Concejo Regional Indígena de Caldas, el Concejo Regional Indigena del Huila** y las organizaciones campesinas y sociales del Cauca, y así de manera conjunta “seguir fortaleciendo esta gran jornada nacional”.][(Le puede interesar: Paro Nacional: una expresión ciudadana de contundente movilización).](https://archivo.contagioradio.com/paro-nacional-una-expresion-ciudadana-de-contundente-movilizacion/)

[Aida Quilqué, consejera de Derechos Humanos de la Organización Nacional Indígena de Colombia (ONIC), resalta la importancia del acompañamiento de los pueblos indígenas en el marco del Paro Nacional. Dentro de las movilizaciones a nivel nacional, los pueblos indígenas se encuentran en distintas actividades en su territorio, específicamente en la Vía Panamericana entre Cali y Popayán. De igual forma, una delegación de 50 guardias indígenas arribó a Bogotá donde se alojarán en la sede del CRIC en el barrio la Candelaria del centro de  la capital, haciendo desde allí los distintos acompañamientos en el marco de las jornadas del Paro Nacional. ]

### **El acompañamiento de los pueblos indígenas** 

[Desde principio del 2019  se conoció el proceso de la Minga Nacional por la Vida, un movimiento que por medio de la unión de diversas comunidades se prolongó por varios días, exigiendo al Gobierno inversión social en sus territorios y una inclusión étnica en el Plan Nacional de Desarrollo. Al conocer los procesos activos de los pueblos indígenas —como el referente de la Minga— en materia de derechos humanos y reivindicaciones políticas, Aida Quilqué reconoce el esfuerzo de la ciudadanía en las calles. ]

[“En principio tenemos que aplaudir ese gran compromiso que han tenidos las personas en gran parte de ciudades en el país. En el caso de los pueblos indígenas ya no somos sólo nosotros o algunas organizaciones sociales movilizadas” afirma Quilqué. ]

[Para Quilcue, este acompañamiento de los pueblos indígenas brinda fuerza al Paro Nacional, al mencionar que “hoy hay una generación de jóvenes y de niños que está despertando y a la vez se está dando cuenta de la realidad que está viviendo Colombia y eso nos llena de orgullo y mucha esperanza”. ]

[Distintas organizaciones indígenas resaltan la importancia de unir todas las organizaciones y grupos ciudadanos que hacen parte del Paro Nacional, ya que lo más importante en este caso es que “tenemos que seguir uniéndonos para caminar juntos y seguir construyendo agendas diversas representadas en esa Colombia en que estamos”.]

### **La fuerza indígena del Cauca se suma al resto del país** 

[Dentro de la movilización en el Cauca, existen organizaciones líderes que congregan a gran parte del pueblo indígena. En este caso, la ONIC ha sido de las más importantes. De igual forma, la Minga del Suroccidente Colombiano se ha sumado a este proceso, incluyendo y revisando temas de interés para incluir en el “marco de la propuesta nacional”.  ]

[Procesos paralelos en otras partes de Latinoamérica se han manifestado a lo largo de este año. Casos como el de Ecuador o el de Brasil han sido importantes referentes, tal como menciona Aida: “yo creo en la fuerza que tenemos los pueblos indígenas en el marco de las raíces que hoy nos identifican. Consideramos que estas luchas son en contra del neoliberalismo, en contra de la exclusión, de lo que vivimos en el Cauca y la Región Pacífica”.]

[Es importante mencionar el proceso llevado a cabo por los indígenas en Ecuador, donde a través de la protesta y la manifestación, lograron derogar el decreto 883 que estaba impulsando el presidente Lenín Moreno. “No es el momento de juntarnos solamente en Colombia sino a nivel continente. Si no defendemos nuestra casa grande seguramente será difícil desarrollar la vida de todos los seres que la habitamos”.][(Le puede interesar: No se detiene el exterminio hacia los pueblos indígenas).](https://archivo.contagioradio.com/no-se-detiene-el-exterminio-hacia-los-pueblos-indigenas/)

### **La evolución del despertar de la gente** 

[El Paro Nacional que comenzó el pasado 21 de noviembre ha sido una muestra de movilización masiva  que ha incluido cacerolazos y demás manifestaciones artísticas. Desde la experiencia indígena existen reflexiones importantes, como menciona Aída: “Yo podría hablar de la experiencia que ha tenido el movimiento indígena. Lo que fue 2007, 2008, continuando ahora las reivindicaciones del pueblo indígena caucano”.]

[“En ese momento —continúa Aída— recuerdo que nadie se podía mover porque nos habían dicho que era prohibido manifestarse, era la época del gobierno de Uribe. Durante ese proceso nació el movimiento llamado «La Liberación de la Grande Tierra» que luego llegó a llamarse la Gran Minga de Resistencia Social y Comunitaria”.]

[Para Aída, lo más importante de rescatar todos estos procesos es que los mismos vuelven a la vida en estas nuevas jornadas de protesta. “Nuestra apuesta era que no nos quedáramos únicamente con nuestras preocupaciones ya que aquí no solamente le vulneran los derechos a los indígenas sino a muchas personas. Hoy veo con mucha satisfacción que esta generación se ha despertado con mucha fuerza, nos llena de mucho orgullo y mucha esperanza, no solamente a nosotros sino a la sociedad colombiana”.]

### **Una generación que despierta en conjunto con los pueblos indígenas** 

[Uno de los motivos más importantes del desplazamiento de los representantes de los pueblos indígenas hacia la capital del país, ha sido la masiva participación de ciudadanos a lo largo del Paro Nacional y la búsqueda de una reivindicación conjunta de los derechos humanos en el país, “el camino y el caminar de la palabra nos dirá como iremos avanzando en este proceso tan grande que ha iniciado Colombia”, sentencia Aída. ]

[Desde las miradas de cada una de las comunidades, lo que pasa en Colombia no es una excepción. “No hemos renunciado al diálogo porque es un principio nuestro. Los pueblos indígenas con expresiones milenarias de resistencia y movilización tenemos que hacer parte de este proceso tan grande que ha iniciado en Colombia, Latinoamérica y ojalá que trascienda al mundo”.]

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe><iframe id="audio_44903021" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_44903021_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
