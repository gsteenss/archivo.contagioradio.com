Title: Min Defensa pone en riesgo la vida de líderes sociales: Somos Defensores
Date: 2017-12-19 16:37
Category: DDHH, Entrevistas
Tags: Juan Carlos Villegas, Min Defensa, Somos defensores
Slug: min-defensa-expone-la-vida-de-lideres-sociales-somos-defensores
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/12/líderes-rueda-de-prensa-2-.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [19 Dic 2017] 

Carlos Guevara, vocero de Somos Defensores, dar avances de investigaciones sobre asesinatos de líderes sociales es una responsabilidad de la fiscalía y no de Min Defensa. Además decir que los asesinatos obedecen a lios de faldas o de linderos, es dar un mensaje a los perpetradores de los homicidios para que estén tranquilos y puedan seguir asesinando.

Según Guevara hasta el momento la Fiscalía no ha dado a conocer informaciones que corroboren que hay esclarecimiento sobre el 62% de los casos de asesinatos a líderes y rechazó las afirmaciones de Luís Carlos Villegas, **debido a que están revelando información que hace parte de las investigaciones, acción que no le compete.**

“Lo que está haciendo el Ministro es dar una **justificación de que los crímenes que pasen de aquí en adelante contra defensores de derechos humanos sean maquillados**, y se hagan montajes sobre la muerte del líder o el defensor, es muy peligroso este mensaje” afirmó Guevara.

### **Fiscalía no ha revelado información sobre esclarecimiento de crímenes** 

Referente a la afirmación que hizo Villegas en donde manifiesta que hay un 62% de esclarecimientos sobre los casos de asesinatos a líderes sociales, Guevara señaló que hasta el momento la Fiscalía no ha revelado esa información, y que por el contrario de los más de 500 casos que se presentaron entre los años **2009 al 2016, solo se han esclarecido 28 con condenas, mientras que del 2017 solo hay 4 casos que tienen condenas**.

“Si vamos al término de lo legal el esclarecimiento debería señalar que ya hay una condena o una sentencia frente a ese caso, de lo contrario estaría cometiendo un delito de especulación o rompiendo la cadena procesal al sacar datos de un proceso de investigación en curso” expresó Guevara.

### **La violencia históricamente se ha negado en Colombia** 

Para Carlos Guevara la afirmación de Villegas al decir que el asesinato de líderes sociales es producto de **“líos de faldas”** hace parte de los encubrimientos que históricamente se han querido hacer con la violencia.

“**Así comenzó el desplazamiento forzado, cuando dijeron que era una migración interna de muchas personas**, luego con el tema de falsos positivos también señalaron que eran guerrilleros o guerrilleros vestidos de civil”.

### **Los esfuerzos de las instituciones** 

La Procuraduría General de la Nación informó que le solicitará a la Fiscalía que inicie una investigación contra el Ministro de Defensa por sus afirmaciones, en ese sentido Carlos Guevara afirmó que **aplaude la iniciativa y los esfuerzos desde instituciones como esta y la Defensoría del Pueblo**, sin embargo, manifestó que hay desorden y desinformación en el gobierno que se refleja en las cifras que dan sobre asesinatos y en la alocución del presidente Santos.

“Esto no puede ser una situación de pronunciamientos, **es una situación crítica que durante este año todas las mediciones tanto de gobierno como de comunidad internacional y civil, ha denotado que es un problema en crecimiento**” afirmó Guevara.

<iframe id="audio_22740433" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_22740433_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>
