Title: Putumayo: entre la guerra y la esperanza de paz
Date: 2015-06-09 12:43
Category: Comunidad, Otra Mirada
Tags: Frente Amplio por la PAz, Mesa de Organizaciones Sociales del Putumayo, Putumayo, Zona de reserva Campesina del Putumayo
Slug: putumayo-entre-la-guerra-y-la-esperanza-de-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/06/PUTUMAYO.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: contagioradio.com 

En la amazonia colombiana, el departamento del Putumayo cuenta con la mayor densidad poblacional, también una gran diversidad biológica y natural y la constante conflictividad entre los intereses empresariales en petróleo y extracción de Oro, así como la presencia histórica de las guerrillas y desde hace más de una década la fuerte militarización de la mano de las empresas petroleras.

**La cuestión petrolera**

Aunque desde el 2002 la empresa extractora Vetra, que desarrolla varias concesiones, ha instalado cerca de 32 pozos petroleros en 13 veredas, y la presencia militar se ha incrementado en razón de la instalación de dos batallones militares minero-energéticos, el problema petrolero no es nuevo en el departamento. Esta actividad ya se ha venido desarrollando desde hace más de 30 aaños.

El departamento cuenta con 39 fuentes hidrográficas entre las que se destacan el Río San Miguel, el Guamués, el Putumayo y Cuembí,  muchos de los ojos de agua que los alimentan se están secando por la presencia de las empresas petroleras. Las consecuencias para los habitantes de la región también son evidentes.

Los pozos profundos que se han construido como alternativa para la falta de agua potable ya presentan filtraciones de crudo lo que la hace no apta para el consumo. Según la Mesa de Organizaciones Sociales del departamento ya muchas de las comunidades, cerca de 1200 familias ya no tienen agua y deben consumirla embotellada.

Todo ello, debido a que la empresa Vetra, no está siguiendo los parámetros de seguridad y saneamiento ambiental debidos para la explotación petrolera que realiza. La Mesa de Organizaciones Sociales, afirma que en la audiencia que se realizará el próximo sábado, 13 de Junio, presentará los resultados de diversos muestreos que ha venido realizando durante este año y que evidencian el alto grado de responsabilidad ambiental de la extracción petrolera.

**Derechos Humanos, el otro punto oscuro del departamento**

Recientemente se ha denunciado que en lo que va corrido del 2015 ha sido asesinados 56 líderes sociales del departamento, y aunque se logró demostrar la inocencia de 8 lideres, entre ellos Wilmer Madroñedo, se han realizado capturas masivas de más de 100 personas, todas ellas vinculadas a procesos organizativos del departamento.

A pesar de la fuerte militarización se han presentado fuertes incursiones paramilitares en el municipio de Orito y se ha provocado el desplazamiento de miles de personas que buscan refugio en diversas comunidades rurales para evitar las condiciones deplorables en los grandes centros poblados.

**Fumigaciones aumentan en un 40%**

La presencia de los cultivos de coca para uso ilícito ha sido una de las fuentes de sobrevivencia ante la ausencia de otras fuentes económicas. Según diversos estudios, desde el anuncio de la Comisión especial de lucha antinarcóticos de suspender el uso del glifosato en las aspersiones aéreas, en el departamento dicha actividad ha aumentado en una magnitud cercana al 40%.

La **Mesa de Organizaciones Sociales** ha presentado al gobierno departamental y nacional un programa integral de sustitución y de desarrollo integral que ha encontrado oídos sordos, pues ninguna de las propuestas se ha aceptado como válida y mucho menos se ha comenzado  implementar con recursos estatales.

El **PLADIA** contempla no solamente planes de sustitución de cultivos uso ilícito, sino un plan de desarrollo integral que contemple la ancestralidad y los usos y costumbres de las comunidades campesinas, negras e indígenas que habitan el departamento.
