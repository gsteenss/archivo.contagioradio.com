Title: Joven agredido por el ESMAD permanece en estado de coma inducido
Date: 2016-04-26 15:45
Category: DDHH, Nacional
Tags: ESMAD, estudiante en coma, Universidad Distrital
Slug: joven-agredido-por-el-esmad-permanece-en-estado-de-coma-inducido
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/04/ESMAD_.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Publimetro ] 

###### [26 Abril 2016] 

El estudiante de la Universidad Distrital que fue agredido violentamente por efectivos del ESMAD durante la movilización del pasado jueves, **continúa recluido en el Hospital del Tunal, en estado de coma** y aunque el pronóstico es reservado, el último parte médico indicó mejoría.

De acuerdo con uno de sus compañeros, sobre la 1 de la tarde el ESMAD arremetió contra los estudiantes quienes se retiraron, minutos [[más tarde se inició una confrontación](https://archivo.contagioradio.com/en-estado-de-coma-estudiante-udistrital-agredido-por-esmad/)] entre los estudiantes, el ESMAD y la Policía que duró media hora. Después que se dispersaron los gases lacrimógenos se dieron cuenta que Miguel Ángel estaba caído en el piso con un herida en la parte superior de su cabeza, que se presume fue causada por **un gas, una recalsada o una aturdidora lanzada por el ESMAD y que terminó por fracturarle el cráneo**.

La jornada de movilización había sido promovida para denunciar las problemáticas internas que enfrenta la Universidad, entre ellas la [[elección antidemocrática del rector](https://archivo.contagioradio.com/consejo-superior-impide-eleccion-democratica-del-nuevo-rector-de-u-distrital/)]  impulsada por el Consejo Superior, el **desconocimiento de la reforma universitaria construida por trabajadores, estudiantes y profesores**, así como la más [[reciente reforma al programa de licenciaturas](https://archivo.contagioradio.com/hoy-movilizacion-nacional-por-la-autonomia-universitaria/)].

Según han denunciado, miembros del ESMAD han emitido comentarios denigrantes como "agradezcan que no lo matamos, eso le pasa por ser guerrillero", por redes sociales. "Una completa **estigmatización contra la protesta social que es un derecho constitucional**", como asegura un estudiante quien agrega que en la más reciente asamblea se reiteró su posición por el [[desmonte del ESMAD](https://archivo.contagioradio.com/10-anos-sin-oscar-salas-10-anos-de-total-impunidad/)].

<iframe src="http://co.ivoox.com/es/player_ej_11314791_2_1.html?data=kpagk5mbfZKhhpywj5WZaZS1k5yah5yncZOhhpywj5WRaZi3jpWah5yncabn1drRy8bSuMaftpCxy9jYtsrowtGah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ%3D%3D&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)]]
