Title: Comunidad entrega pruebas de participación policial en desaparición de jóvenes en Sucre
Date: 2019-12-06 15:50
Author: CtgAdm
Category: Nacional
Tags: Cauca, desapariciones, policia, Sucre
Slug: policias-de-sucre-cauca-detenidos-por-la-desaparicion-de-carlos-felipe-adarme-y-tulio-cortes
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/policia-.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:RCN Radio] 

El 5 de diciembre, luego de las denuncias e investigaciones de los pobladores de Sucre al sur de Cauca, fueron detenidos  dos patrulleros  de la estación de **Policía El Bordo**, quienes son sindicados de la detención y posterior desaparición de los jóvenes **Carlos Felipe Adarme y Tulio Cortés.** El hecho sucedió el 23 de noviembre en horas de la tarde, luego de ser abordados por un patrulla de la Policía en la región El Patía. Según testigos y cámaras ubicadas en la zona, fueron esposados por los uniformados y subidos a un vehículo particular.

Según el Concejal de Sucre, **Carlos Chilito, la exigencia de los habitantes fue crucial para que se abriera el caso de la desaparición de los jóvenes. L**a comunidad visitó en la noche del sábado la estación de policía y lo volvió hacer el domingo. "L*levamos más información recolectada para exigir respuestas, por la presión de la gente la policía decide abrir una investigación*" resaltó **Chilito**. ( Le puede interesar: [Paramilitares amenazan candidatos a la Alcaldía en Ovejas, Sucre](https://archivo.contagioradio.com/paramilitares-amenazan-candidatos-a-la-alcaldia-en-sucre/))

Asimismo, el Concejal agregó que la noche del domingo uno de los patrulleros que se encontraba en esta estación se quitó la vida.  "A*l parecer en la estación había un implicado, un patrullero que al ver la comunidad exigiendo y buscando, decide acabar con su vida,  un hecho lamentable y desconcertante, especialmente para nosotros que estamos alejados de las ciudades y no solemos ver este tipo de actos en nuestra región*", lamentó el Concejal. (Le puede interesar: [Sucre también dice NO a la minería en Santander](https://archivo.contagioradio.com/sucre-consulta-popular/))

"*Estamos consternados y más cuando esto parte de una institución a la que le teníamos confianza, la comunidad de Sucre está indignada y perdió la credibilidad ante las instituciones*", afirmó el cabildante luego de conocer el arresto de los dos patrulleros. Ademas, añadió lo siguiente: "*queremos que se haga justicia, porque ellos eran unos pelados sanos, que no se involucraban en hechos sospechosos , ni pleitos o en líos, necesitamos que eso se investigue y más aún que aparezcan pronto*".

### ¿**Porqué detuvieron a estos jóvenes en Sucre  y quién ordenó que lo hiciera?** 

Esta es la pregunta que se hacen los pobladores de Sucre. "Pueden surgir muchas hipótesis, una de ellas es que uno de los compañeros era la pareja  sentimental de la alcaldesa actual, y estuvo muy de frente a toda la campaña, evitando malas practicas y denunciando hechos fraudulentos"señaló el Concejal. De igual forma, afirmó que los pobladores han salido a hacer recorridos en los municipios vecinos y se han organizado por grupos para buscar a sus compañeros. Por otro lado, el Gaula de la Polícia y la Fiscalía investigan el caso de **Carlos Felipe Adarme y Tulio Cortés.**

 

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe><iframe id="audio_45285854" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_45285854_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
