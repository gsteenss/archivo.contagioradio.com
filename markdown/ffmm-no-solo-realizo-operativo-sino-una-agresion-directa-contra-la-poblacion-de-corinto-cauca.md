Title: "FFMM protagonizaron agresión directa contra la población de Corinto, Cauca"
Date: 2015-03-20 18:22
Author: CtgAdm
Category: DDHH, Nacional
Tags: Cauca, CTI, Derechos Humanos, ejercito, FARC, FFMM, Fuerzas militares, ONG, operaciones militares
Slug: ffmm-no-solo-realizo-operativo-sino-una-agresion-directa-contra-la-poblacion-de-corinto-cauca
Status: published

##### Foto: juanjosehorta.com 

##### <iframe src="http://www.ivoox.com/player_ek_4242045_2_1.html?data=lZehlJWYeY6ZmKiak5aJd6KolZKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRiqfBrpDaw9PNtNbgwtOYzsaPusbmxcbRjdjTptPZjNjW1trFp8qZpJiSpJjSb8bijKjO18jFcYarpJKw0dPYpcjd0JC%2Fw8nNs4yhhpywj5k%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>  
[Lizeth Montero, Red de Derechos Humanos Francisco Isaías Cifuentes] 

“**Es claro que la intensión de ejército, no era solo realizar el operativo, sino generar un escenario de agresión directa contra la población civil”,** es la conclusión de la misión de verificación en el municipio de Corinto, Cauca, de la Red de Derechos Humanos Francisco Isaías Cifuentes, a la que pertenece la abogada Lizeth Montero. Ella afirma que  las Fuerzas militares están manipulando la información, ya que la población civil fue objeto de múltiples agresiones, y por ende, la comunidad no puede ser presentada como la responsable de los hechos ocurridos el pasado 18 de marzo.

De acuerdo a la abogada, **el ejército desembarcó alrededor de 30 soldados y otros funcionarios del CTI,** para realizar una operación militar con el objetivo de capturar a un narcotraficante vinculado con el sexto frente de las FARC-EP, cerca a la población civil, tras ese hecho los pobladores denunciaron graves vulneraciones a los DDHH, fue por eso que el 19 de marzo se realizó una misión de verificación sobre la situación. Allí se encontró que **“a diferencia de lo que se ha presentado de parte de las FFMM, hubo una serie de vulneraciones contra los derechos humanos e infracciones al DIH, por parte del Estado”**, aseguró Montero.

De acuerdo a la ONG, a las 3:30 de la tarde, **4 helicópteros sobrevolaron la zona poblada por 105 familias.** Allí desembarcaron los militares y debido a la acción de los helicópteros, varios techos de las viviendas de la población fueron destruidos, además, varias casas tuvieron impactos de fuego.

Así mismo, el **ejército utilizó gases lacrimógenos que  generaron intoxicación y asfixia a 5 menores de edad y dos campesinos** que fueron encerrados y amordazados, además una mujer de 74 años sufrió varios hematomas en sus extremidades, y se interrumpió el fluido eléctrico por el daño que ocasionaron los helicópteros a los cables de la región.

Por otro lado, “los funcionarios no presentaron orden de captura, ni se identificación, simplemente rompieron puertas y golpearon campesinos... procedieron a la fuerza”, indicó, la abogada de la red de derechos humanos.

Para Montero, “es lamentable decir que esta acción ofensiva de la **Tropa de la Fuerza de Tarea conjunta Apolo, adscrito a la Tercera División del Ejército Nacional**, desencadeno un enfrentamiento con la insurgencia que estaba en una zona montañosa frente al corregimiento Río Negro hasta las 7:30 pm, pero la operación fue mucho más allá y no se tomaron medidas para asegurar que el procedimiento se produjera respetuosamente”.

Si bien la comunidad se congrego y desarrolló una acción, “**era necesario para repelar contra los actos de la policía”,** dice la abogada, quien agrega que es permanente la presencia de las fuerzas militares en ese sector del Cauca.
