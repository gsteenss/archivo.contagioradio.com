Title: A propósito del youtuber y el mundo de hoy
Date: 2016-05-04 06:00
Category: Cesar, Opinion
Tags: Cultura, feria del libro, youtubers
Slug: a-proposito-del-youtuber-y-el-mundo-de-hoy
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/05/maxresdefault.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Hola Soy German]

#### Por [César Torres Del Río ](https://archivo.contagioradio.com/cesar-torres-del-rio/) 

###### 4 May 2016 

Germán Garmendia y su canal en youtube, “HolaSoyGermán” (27.4 millones de seguidores), como unos cuantos youtuber más que se pueden apreciar en la red (PewDiePie, con 43.6 millones de seguidores) es un acontecimiento para estudiar y comprender.  Algun@s colegas periodistas han dado la bienvenida a este nuevo “sujeto” porque expresa una manera diferente de comunicarse y “sentir” la realidad; se trataría de algo así como un “malestar digital” de las generaciones contemporáneas, de las juventudes, frente a lo dado, frente al Mundo y las maneras de relacionarse, “malestar” que es identificado con lo que las generaciones anteriores hicieron frente a sus mayores (cultura, valores, escritura, conocimiento, nostalgias y utopías).

[Quisiera avanzar una vía de entendimiento del asunto a partir de la pregunta ¿Malestar contra el Mundo o adaptación a éste?  Ante la ausencia de alternativas creíbles y tangibles de vida - políticas, materiales, económicas, culturales, individuales y colectivas -, en]*[el aquí y el ahora]*[, nos parece que estamos ante un acomodamiento, una adaptación, al Mundo tal y como se lo vive, y sufre, hoy, adaptación que es al mismo tiempo una escapatoria de lo Real por la vía de lo digital. Hablo de “escapatoria”, no de aislamiento; hablo de adaptación no de complicidad.  ]

[Millones de jóvenes son críticos de la sociedad capitalista; se manifiestan contra la Guerra y sus múltiples variantes; odian el panoptismo y más aún si es el psiquiátrico; se rebelan contra la destrucción ecológica del planeta; son solidarios con las tragedias humanas; redescubren el anti-totalitarismo; alertan sobre nuevas prácticas sexuales; se burlan del patriarcalismo y sus atavismos; rechazan la Violencia de género y condenan a los pederastas; denuncian la exclusión social y cuestionan, a su modo, la ideología de la burguesía; el poder no los obnubila y la corrupción los mata.]*[Por todo esto no son cómplices]*[.]

[Esta visión crítica, este “desencanto del mundo”, se alterna con el “encantamiento” que la modernidad impulsa mediante la]**novedad**[ (o sea, lo viejo presentado como nuevo: “el eterno retorno de lo mismo” dice August Blanqui, una de cuyas expresiones es la moda). Y esta novedad es un imán poderoso. El “encantamiento” no es más que la reificación, es decir a la cosificación. La develación del secreto del mundo de la mercancía - hacer aparecer las relaciones sociales entre productores, entre personas y comunidades, como relaciones entre cosas, entre mercancías, sin vinculación alguna con el proceso de producción y con vida propia - no es aún asunto del conocimiento general de los jóvenes ni de los trabajadores formales e informales.  La]*[fantasmagoría]*[mercantil-digital, incluida la del espectáculo, hace las veces de la Vida, de la Realidad.]

[Parcialmente, y solo parcialmente, corresponde al mundo cosificado lo superficial del]*[youtuber]*[ cualquiera. La web, lo virtual, ofrece todo al alcance de la mano: deseos, placeres, fantasías, casinos, carros, mujeres, hombres, sexualidades, loterías, “amistades”, libertades, lenguajes vulgares, pornografía, músicas,  y por supuesto videos. Parafraseando a Slavoj Žižek, digamos que lo virtual es una “Segunda Naturaleza”, una “Vida 2.0”. En este plano 2.0  lo reprimido deja de serlo. Los usuarios se desinhiben; pueden ser transgresores frente a lo Real (la vida cotidiana, la manera de alimentarse, el uso de la ropa, el comportamiento higiénico, los miedos, el estudio, los actos de escribir y de comunicarse); su]*[yoes]*[ se transparentan, se explicitan, explotan, disfrutan. Así se escapan de lo Real-siniestro. Así se “participa” del Mundo;]*[por eso no están aislados]*[.]

[En este plano, como lo conocemos hasta hoy, el fenómeno youtuber desalienta la esperanza de la acción colectiva en lo Real; incentiva el “principio de res-pon-sa-bi-li-dad” (Hans Jonas), que implica la adaptación individual al Mundo en tanto que no se puede transformar, o por cuanto ya no vale la pena hacerlo. Sólo se nos ofrece un]*[devenir]*[  (¿Real?  ¿Digital?).]
