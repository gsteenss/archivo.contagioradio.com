Title: En cuidados intensivos se encuentra la salud en Colombia
Date: 2015-08-19 13:17
Category: DDHH, Entrevistas, infografia, Movilización
Tags: crisis de la salud, EPS, ESMAD, Medellin, Ministerio de Salud, Salud
Slug: en-colombia-la-salud-se-encuentra-en-cuidados-intensivos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/08/salud-.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Contagio Radio 

<iframe src="http://www.ivoox.com/player_ek_7128702_2_1.html?data=mJafmpyUdo6ZmKiakp6Jd6KnmpKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRjtDmz8bRw5DIqYzk09Thx9jYpdSf0dTfjcrQb8XZ08rQytSPpYzgwpDgw9HZqI6ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Clemencia Mayorga, Mesa Nacional por el Derecho a la Salud] 

###### [19 Ago 2015]

Médicos, profesionales de la salud, gerentes de hospitales, y pacientes, han decidido realizar una jornada de manifestaciones en todo el país, con el objetivo de defender el derecho a la salud y exigir el pago de la deuda de más de **12 billones de pesos que tienen las EPS´s con los hospitales del país,** lo que no ha permitido que estos presenten un buen servicio de salud a los y las colombianas.

Debido a esa extensa deuda, “**hay 640 hospitales en ‘alto riesgo financiero’**, es decir con iliquidez total y con riesgo de cierre de servicios”, explica Clemencia Mayorga, vocera de la Mesa Nacional por el Derecho a la salud, quien explica que los departamentos donde más se evidencia esta crisis es en **Antioquia, la Costa Atlántica, Quindío y Bogotá.**

Por otro lado, asegura que la **medida del gobierno de prestar dinero a las EPS´s por el monto de 1,5 billones de pesos, es “a todas luces una solución insuficiente"**, y además señala que ese capital debería ser inyectado directamente a los hospitales, y no a las Empresa Prestadoras de la Salud, ya que estas son las que propiciaron la actual crisis.

No solo se trata de la situación de los pacientes, también los trabajadores de la salud son víctimas de la crisis del sistema, ya que sus derechos laborales se ven afectados. **Algunos hospitales llevan 3 y 4 meses sin pagar el salario a sus empleados,** y además  el 70% de los trabajadores tiene contrato de prestación de servicios y otros contratos ilegales.

"Estamos en las calles para llamar la atención del derecho fundamental y constitucional de la salud", dice Mayorga, quien invita a los colombianos a participar en esta jornada pacífica, teniendo en cuenta que **“pacientes somos todos”,** expresa la vocera de la Mesa Nacional por el Derecho a la Salud.

Cabe resaltar que en las horas de la mañana varias personas que se encontraban realizando la manifestación de forma tranquila **en Medellín fueron agredidos por el ESMAD y la Alcaldía de Medellín.**

[![1440001462 (1)](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/08/1440001462-1.jpg){.aligncenter .wp-image-12376 width="573" height="799"}](https://archivo.contagioradio.com/en-colombia-la-salud-se-encuentra-en-cuidados-intensivos/1440001462-1/)
