Title: La verdad a medias del Procurador Ordoñez
Date: 2015-05-13 14:34
Author: CtgAdm
Category: Ambiente, Nacional
Tags: Agencia Internacional para la Investigación sobre el Cáncer, Alejandro Ordoñez, cáncer, Edgar Podlesky, estudios sobre el glifosato, fumigaciones con glifosato, Glifosato, Gobierno Nacional, IARC, INS, Instituto Nacional de Salud, Minsalud, Procurador
Slug: la-verdad-a-medias-del-procurador-ordonez
Status: published

###### Foto: [www.diarionco.net]

###### [13 de May 2015]

Este lunes el Procurador General de la Nación, Alejandro Ordoñez, usó un **informe del Instituto Nacional de Salud de 1984, denominado “Apreciaciones al informe emitido por la IARC** (Agencia Internacional para la Investigación sobre el Cáncer) y su potencial impacto en el uso del herbicida glifosato en Colombia”, como herramienta para criticar la recomendación del Ministerio de Salud sobre dejar de usar el glifosato para las fumigaciones aéreas.

El Procurador había asegurado que el gobierno ocultó el informe que al no conocerse “se evitaba que se supiera que esa decisión carece de fundamento científico”. Sin embargo, Ordoñez no tuvo en cuenta que en su afirmación solo estaba teniendo en cuenta una parte de todo el informe, ya que lo que concluyeron los expertos en esa época, fue **no usar el herbicida, ya que para ese momento no existía certeza sobre sus efectos en la salud,** y por lo tanto si se usaba el glifosato, se estaría experimentando con seres humanos.

Así mismo, según la INS, el informe nunca se ocultó, y además afirma que “el comunicado de la Procuraduría omitió la evaluación de la segunda parte del informe”.

Realmente se trataba de un análisis del concepto de la Agencia Internacional para la Investigación del Cáncer, IARC, sobre las consecuencias en la salud. Fue así, como el gobierno y el INS llamó a una **comisión de científicos liderados por el doctor Edgar Podlesky para estudiar la toxicidad del glifosato.**

Luego de diferentes reuniones y estudios, el Comité de expertos en herbicidas, recomendó al gobierno de la época, que  “desde el punto de vista de la salud humana y el impacto en el medio ambiente, el método químico debe ser el último en considerarse”. Según dicen los documentos del Comité, donde se agregó que **“no se recomienda el uso de ninguno de los herbicidas por vía aérea”.**

Respecto al glifosato, prefirieron poner en duda la toxicidad en humanos, por lo que no se aconsejó la ejecución de fumigaciones con este herbicida, debido a que eso implicaría experimentación en seres humanos.

Pero otros integrantes del Comité acompañaron la aprobación del uso de glifosato con un programa de vigilancia toxicológica y ambiental, por lo que **el 22 de mayo de 1984 el Ministro de Justicia de ese entonces habría dado el aval al glifosato.**

Podlesky, se molestó por la decisión del gobierno teniendo en cuenta que de acuerdo a él, no hubo un programa de seguimiento toxicológico, que era totalmente necesario si decidían darle aprobación a estas fumigaciones.

Finalmente, el microbiólogo ambiental pidió una explicación al INS y al Minsalud, pero lo que recibió fue que su nombre no apareciera en la publicación de la investigación, aunque él había sido la persona que lideró y coordinó todo el estudio. Por lo que Francisco Rossi, científico que aparece como compilador aunque asegura que realmente fue todo un trabajo hecho por el doctor Podlesky, de manera que indica que para 1984 **“los científicos dijeron que no pero los políticos dijeron que sí”**.
