Title: Campesinos bloquean vía Ocaña-Cúcuta, exigiendo cumplimiento de acuerdos de sustitución de cultivos
Date: 2017-09-16 09:40
Category: DDHH, Movilización, Nacional
Tags: Catatumbo, Erradicación de cultivos
Slug: campesinos-bloquean-via-ocana-cucuta-exigiendo-cumplimiento-de-acuerdos-de-sustitucion-de-cultivos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/06/Catatumbo-Contagio-Radio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo Particular] 

###### [16 Sept 2017] 

Campesinos del Catatumbo bloquean vía que comunica Cúcuta con Ocaña, a la altura de la Y de Astilleros, desde las 4 de la mañana y denuncian que desde las 5 am, se han presentado enfrentamientos con el ESMAD, **que dejan como resultado una persona capturada, dos personas heridas, la desaparición del jóven Ándres Buitrago Cuestas y varias motos quemadas**.

Los bloqueos de los campesinos buscan exigirle al gobierno que frene la erradicación forzada  de m25 mil hectáreas de hoja de coca y cumpla con el punto cuatro del acuerdo de paz de La Habana, en donde se compromete a realizar sustitución de cultivos ilícitos de forma concertada con las comunidades y sin represión. (Le puede interesar:["Erradicación forzada en el Catatumbo afecta a más de 300 mil personas](https://archivo.contagioradio.com/erradicacion-forzada-en-el-catatumbo-afecta-a-mas-de-300-mil-personas/)")

Además, la organización ASCAMCAT, manifestó que mientras se desarrollaban los enfrentamientos entre campesinos y el ESMAD, se reportó la desaparición de **Ándres Buitrago Cuestos, de 25 años, oriundo de la vereda San Martín de Loba. **

Los campesinos denuncian que desde la semana pasada, a la vereda el Treinta, ubicada en el municipio de Sardinata, hacen presencia el Ejército, la Policía y el ESMAD con la finalidad de hacer operaciones de erradicación forzada y violenta en la zona. Incumpliendo el acuerdo de paz de La Habana**, y el acuerdo colectivo firmado el pasado 9 de septiembre en el municipio de Tibú, sobre sustitución voluntaria.**

A través de un comunicado de prensa, la organización ASCAMCAT, señaló que los campesinos continuaran en movilización pacífica, mientras se establecen coordinaciones para hacer un llamado a paro regional y agregaron que continúan insistiendo en el diálogo con el gobierno del presidente Juan Manuel Santos, **para exigirle el cumplimiento de los acuerdos ya pactados con las comunidades campesinas.**

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
