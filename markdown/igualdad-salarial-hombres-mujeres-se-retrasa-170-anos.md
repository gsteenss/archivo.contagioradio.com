Title: Igualdad salarial entre hombres y mujeres se retrasa 170 años
Date: 2016-11-12 14:56
Category: Mujer, Otra Mirada
Tags: igualdad de género, mujeres
Slug: igualdad-salarial-hombres-mujeres-se-retrasa-170-anos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/protesta-mujeres-e1478904763832.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Feminnant 

###### [11 Nov 2016]

Esta semana inició una ola de protestas de movimientos feministas en Europa exigiendo igualdad salarial. Islandia y Francia iniciaron las primeras movilizaciones teniendo en cuenta que el más reciente informe del **Foro Económico Mundial**, FEM, concluye que la igualdad de género en el mundo podría retrasarse 170 años.

Así lo asegura el estudio denominado Global Gender Gan Report 2016, el que a partir del análisis en materia de educación, salud y supervivencia, oportunidades económicas y el poder político, se afirma que **hasta el año 2186 podrá existir igualdad entre hombres y mujeres.**

Es por eso que **Islandia**, el país con el índice más alto en garantías de los derechos de las mujeres, fue el primero en presenciar estas movilizaciones. Pese a los avances, **la diferencia salarial es del 14 %** y ello miles de mujeres dejaron sus puestos de trabajo y sobre las 3 de la tarde llenaron las calles para conmemorar el ‘Women's Day Off", cuando en 1975 se vuelve el año en el que el movimiento feminista se reune para reclamarle al Estado la disparidad entre los sueldos masculinos y los femeninos.

En **Francia,** también hubo jornada de movilizaciones promovida por la ministra de los Derechos de la Mujer, Laurence Rossignol. A la misma hora que en Islandia cientos de mujeres detuvieron sus laborales para protestar contra la desigualdad salarial. En ese país, **las mujeres reciben un 15% menos de remuneración que los hombres.** “Los 38,2 días laborables restantes (en 2016) representan las diferencia de salario entre mujeres y hombres”, indicó la ministra.

Por otra parte, en **España esta semana se conoció que aumentó la brecha salarial referente a la categoría de género**. De acuerdo con el Instituto Nacional de Estadística de España, en 2015 las mujeres obtuvieron un salario promedio de 1.700 dólares, mientras que los hombres recaudaron montos por 2.315 dólares, es decir 522 dólares más.

El informe del Foro Económico Mundial  señaló que este el **índice en desigualdad se situó en un 59%,** la mayor desde 2008, por lo que recomienda que los gobiernos y sus entidades responsables deben multiplicar los esfuerzos para garantizar la equidad entre mujeres y hombres.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
