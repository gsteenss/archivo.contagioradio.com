Title: Llegó el momento de la implementación de los acuerdos de paz
Date: 2016-11-27 08:15
Category: Entrevistas, Política
Slug: llego-el-reto-de-la-implementacion-de-los-acuerdos-de-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/congreso-e1478108708937.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:]

###### [24 Nov 2016]

Iván Cepeda señala que hay varios mecanismos de implementación de los acuerdos de paz. Una de ellas a través del congreso y el trámite de los puntos que necesitan reformas constitucionales, y otra la vía de los decretos presidenciales. Según el senador podría promulgarse así la ley de amnistía y la puesta en **marcha de la comisión de implementación que revisará las propuestas en el congreso.**

Aunque el presidente Santos, en su discurso en el acto protocolario de la firma del acuerdo de paz, señaló que el "Dia D" será el día de la refrendación, la discusión está abierta en torno a ese punto, pues **la amnistía sería una especie de parte de tranquilidad para los integrantes de las FARC-EP** que se movilizarán hacia las zonas veredales transitorias de normalización.

### **Comisión de implementación de los acuerdos de paz** 

“Hemos avanzado en defender el acuerdo”, el cese de hostilidades y la firma del acuerdo final, señala Cepeda, pero los próximos meses serán difíciles porque implican la tarea traducir el acuerdo en leyes, decretos y normas que no vayan a desvirtuar lo que está incluído en el acuerdo.

Por ello es importante el trabajo de la Comisión de implementación, porque tendrá que revisar y hacer ajustes a los proyectos que se presenten y que pretenden poner en la ley lo que se acordó en la Habana. Según Cepeda, esta situación es difícil porque en el senado convergen muchos intereses y se acerca la época preelectoral de cara al 2018. (Le puede interesar [Los retos de la implementación](https://archivo.contagioradio.com/estos-son-los-retos-de-implementacion-del-acuerdo-de-paz/))

Sin embargo, para el Senador, es necesario buscar las cosas que estén en el acuerdo y que puedan ser aprobadas por la vía del decreto presidencial. Además se señalan **dos posibles vías, una la del Faast Track y otra la que indique la Corte Constitucional** al definir las demandas que pesan sobre el plebiscito y el acto legislativo para la paz.

### **La semana siguiente estará refrendado el acuerdo de paz en el congreso** 

El Senador Iván Cepeda afirma que una vez firmado el nuevo acuerdo de paz entre el gobierno y las FARC-EP, se procede a consignarlo en el congreso para que se inicie el proceso de refrendación. Se prevé que ese trámite demore menos de 3 días, de acuerdo a lo que ha afirmado el Consejo de Estado, en torno a que el acuerdo deberá ser refrendado punto por punto y no en bloque.

<iframe id="audio_14023121" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_14023121_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
