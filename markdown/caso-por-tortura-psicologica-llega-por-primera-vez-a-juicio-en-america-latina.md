Title: Caso por tortura psicológica llega por primera vez a juicio en América Latina
Date: 2016-01-15 12:14
Category: DDHH, Nacional
Tags: Claudia Julieta Duque, das, Jaime Garzon
Slug: caso-por-tortura-psicologica-llega-por-primera-vez-a-juicio-en-america-latina
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/01/claudia_julieta_duque_contagioradio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: elespectador 

<iframe src="http://www.ivoox.com/player_ek_10086972_2_1.html?data=kpWdmpude5Ohhpywj5aWaZS1kZiah5yncZOhhpywj5WRaZi3jpWah5yncaTV1NSY0tTWb9Xj09ni1MaPtNTdxNTZh6iXaaOnyM7Qw5DQsMbbwpDd0dePtNPdzsrfw5DaqdufwpDXj4qbh4630NPhw8zNs4zGwsnW0ZCRaZi3jpk%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Claudia Julieta Duque] 

###### [15 Ene 2015] 

Este 13 y 14 de Enero se realizó en los juzgados especializados en la ciudad de Bogotá la primera etapa de juicio contra José Miguel Narváez, Enrique Alberto Ariza y Giancarlo Auqué, ex altos funcionarios del DAS, por la tortura psicológica contra la periodista [Claudia Julieta Duque](https://archivo.contagioradio.com/?s=claudia+julieta+duque). “**Se trata del primer caso de tortura psicológica como delito autónomo -es decir, no vinculado a otros delitos o como consecuencia de los mismos, que llega a etapa de juicio en América Latina**”.

Según la resolución de la Fiscalía los acusados “*actuaron a título de COAUTORES, teniendo en cuenta que ésta figura se manifiesta cuando plurales personas son componentes por voluntad propia de la misma causa al margen de la ley, valga decir, **empresa criminal, comparte reflexiva y conscientemente los fines ilícitos propuestos y comparten los medios delictivos para lograrlos***”.

### **La hoja de vida oculta de Narváez** 

Durante la esta etapa de juicio la parte civil que representa a la periodista Claudia Julieta Duque reveló los datos ocultos de la **hoja de vida de José Miguel Narváez**. Los datos presentados por Narváez para entrar al cargo en el DAS contienen su historial y en él se revela. Según los datos presentados el ex director del DAS es  Mayor de la reserva de las FF.MM con estudios en inteligencia militar y especialista en operaciones psicológicas.

Los días 10 y 11 de febrero de 2016 se ha programado el inicio del juicio por estos mismos hechos en contra de Rodolfo Medina Alemán, jefe del área de Contrainteligencia del DAS y Ronal Harbey Rivera Rodríguez, detective de Contrainteligencia y miembro del G-3 que tuvo a su cargo la persecución directa contra la periodista.

Además se revelaron los datos de gastos reservados del DAS en los que hay material probatorio de la participación de Narváez en actividades de la conocida "Operación Transmilenio" la cual dijo no haber conocido.

Para la periodista este juicio implica, en parte, **la posibilidad del esclarecimiento del periodista[Jaime Garzón](https://archivo.contagioradio.com/?s=jaime+garzon)**[,](https://archivo.contagioradio.com/?s=jaime+garzon) investigación por la cual se desprendieron las acciones de tortura de las que fue víctima ella y su familia, pero también se abre el camino para que otras víctimas de tortura psicológica acudan a los tribunales y se avance en la formalización de este crimen internacional.

Esta y más noticia las puede escuchar en [Otra Mirada](https://archivo.contagioradio.com/alaire.html), todos los días de 8 a 10 Am
