Title: "Monocultivo de caña esta arrinconando a comunidades indígenas" ACIN
Date: 2018-05-02 16:03
Category: DDHH, Nacional
Tags: ACIN, ASOCAÑA, Norte de Cauca
Slug: monocultivo-de-cana-esta-arrinconando-a-comunidades-indigenas-acin
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/05/AgenciaUN_0717_1_14.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Agencia UN] 

###### [02 May 2018] 

La Asociación de Cabildos Indígenas del Norte del Cauca, a través de un comunicado de prensa rechazó las afirmaciones del presidente de Asocaña, Juan Carlos Mira, que afirmó que las comunidades indígenas del norte del Cauca poseen más de 100 mil hectáreas de las cuales **solo aprovechan el 10 por ciento y gran parte de ellas están sembradas en cultivos ilícitos**.

Para Arcadio Mestizo, integrante de la Consejería de la ACIN, las afirmaciones de Mira, no solo son descontextualizadas, sino que además ignoran una lucha histórica que han emprendido las comunidades para que sus tierras les sean devueltas, en lo que se ha denominado **"Liberación de la madre tierra" o para que se de una repartición de las mismas de una forma más equitativa**.

"Para las comunidades indígenas, especialmente del Norte del Cauca, hay una necesidad de tierra, enmarcada en una propuesta que conoce el gobierno Nacional" afirmó Mestizo y agrego que están solicitando que **se dispongan 270 mil hectáreas de tierra que serían ocupadas por 112 mil indígenas**. (Le puede interesar:["Cerca de 19 empresas son las que han acaparado más tierras en Colombia"](https://archivo.contagioradio.com/cerca-de-19-empresas-son-las-que-han-acaparado-mas-tierras-en-colombia/))

En ese mismo sentido aseguró que la ACIN ha intentado entablar un diálogo tanto con el gobierno como con las empresas de caña, en donde se les ha expuesto la situación que afrontan los indígenas y se les ha manifestado que solo estarían pidiendo que se re distribuya una pequeña cantidad de tierra para poder sembrar, no solo a los indígenas, sino también a los afros y campesinos que habitan el territorio.

### **"El monocultivo de la caña esta arrinconando a las comunidades indígenas"**

Además, los indígenas han denunciado que el monocultivo de caña de azúcar ha generado diversos daños al ambiente y a los ecosistemas ancestrales, debido al uso excesivo del agua. Asimismo, afirmó que esta forma de economía esta arrinconando a las comunidades indígenas que no encuentran otro mecanismo para garantizar su supervivencia en el territorio."**El monocultivo de la caña lo que ha hecho es reducir la economía propia y sustentable** porque se necesita el territorio para mantener una economía comunitaria" señaló Mestizo.

La ACIN afirmó que los diálogos para ellos continúan abiertos con todos los sectores, desde que se den de manera articulados con los empresarios y las instituciones gubernamentales, sin embargo denunciaron que hay un incumplimiento por parte del Estado, debido a que mendiante la Constitución de 1991 se garantizan los derechos básicos a las comunidades indígenas.

Las aseveraciones de Mira, se dan en medio del debate abierto por el candidato a presidencia Gustavo Petro, que propuso en Puerto Tejada, Cauca, comprar las tierras de la empresa Incauca, para dárselas a los indígenas.

<iframe id="audio_25750512" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_25750512_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
