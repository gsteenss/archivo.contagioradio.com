Title: Farmacéuticas de EEUU son "la pobre viejecita" y vienen por más
Date: 2017-07-05 06:00
Category: Mision Salud, Opinion
Tags: Acceso a los medicamentos, Medicamentos
Slug: farmaceuticas-de-eeuu-son-la-pobre-viejecita-y-vienen-por-mas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/07/Trum-y-los-medicamentos.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Gage Skidmore 

#### **Por [Misión Salud](https://archivo.contagioradio.com/opinion/mision-salud/) - **@MisionSaludCo 

###### 5 Jul 2017

[La administración Trump considera que la mejor forma de resolver los problemas de los altos precios de los medicamentos en EE.UU. es que los demás países, entre ellos los de bajos ingresos, como Haití, los de ingresos medio-bajos, como Bolivia, El Salvador, Guatemala, Honduras y Nicaragua, y los de ingresos medio-altos, como Argentina, Brasil, Colombia y Perú, implementemos medidas que fortalezcan el monopolio de las grandes farmacéuticas multinacionales para que puedan cobrarnos precios más altos. Si a los lectores les parece que no tiene sentido, tranquilos. No son los únicos.]

[Este absurdo es, en resumidas cuentas, una de las estrategias que a puerta cerrada está cocinando la administración Trump en la Orden Ejecutiva][[“Reduciendo los costos de los productos biomédicos y fortaleciendo la innovación biomédica americana”]](https://www.citizen.org/sites/default/files/trum-drug-pricing-eo-draft.pdf#overlay-context=trump-drug-prices-order)[, que fue filtrada a la opinión pública en los últimos días y que aún está en su fase borrador. ]

[¿Cómo es la historia? El problema de la falta de acceso a medicamentos][[dejó de ser una situación exclusiva de los países de medianos y bajos ingresos para convertirse en una realidad mundial]](http://www.somosveedoresensalud.org/acciones-del-cvcs/entendamos-reporte-del-unhlp/)[, en gran medida gracias a los precios escandalosos cobrados por las multinacionales farmacéuticas en todo el planeta. El problema llegó con creces hasta a Estados Unidos. A manera de ejemplos de ello, una inyección utilizada en casos de reacciones alérgicas potencialmente mortales (que pueden ocurrir a cualquier persona en cualquier momento, especialmente en respuesta a medicamentos, alimentos o picaduras de insectos) pasó de costar][[US\$124 en diciembre de 2009 a US\$609 en Mayo de 2016]](http://money.cnn.com/2016/08/29/investing/epipen-price-rise-history/?iid=EL)[. Un aumento del 491%. Otro caso fue el famoso Daraprim®, medicamento utilizado para el VIH,][[que pasó de costar US\$13,5 por tableta en el 2010 a US\$750 en 2015]](http://money.cnn.com/2016/08/25/news/economy/daraprim-aids-drug-high-price/)[, un aumento de 5.555%. ]

[Ante este panorama el poder legislativo de E.E. U.U. inició reformas normativas, tales como los proyectos de Ley “][*[Detengan los precios especulativos]*](https://www.citizen.org/media/press-releases/brown-gillibrand-bill-would-stop-pharma-price-gouging)[” y “][*[Mejorando el acceso a medicamentos de prescripción asequibles]*](https://www.citizen.org/our-work/access-medicines/improving-access-to-affordable-prescription-drugs-act)*[”]*[,  que buscan generar acciones robustas para reducir los precios de los medicamentos en su país. No obstante, y a pesar de haber afirmado, en su primer discurso como Presidente, que las compañías farmacéuticas “][[se están librando de ser acusadas de asesinato]](https://www.statnews.com/2017/01/11/trump-drug-prices-news-conference/)[” por el elevado costo de los medicamentos, el Presidente Donald Trump se propone emitir una orden ejecutiva en la que, en vez de promover la utilización de las salvaguardas de la salud pública en su país para protegerlo de los abusos de los titulares de las patentes y de los evidentes efectos nocivos del monopolio farmacéutico o de promover explícitamente el avance en modelos alternativos de I+D en salud, entre otras actividades, ordena:]

-   [Asegurar que los gobiernos extranjeros no subvaloran injustamente la innovación estadounidense.]
-   [Examinar de manera amplia qué acuerdos comerciales (bilaterales o multilaterales) deben ser revisados para promover una mayor protección de la propiedad intelectual y competencia en el mercado global.]

[La Orden Ejecutiva contempla otras estrategias, por supuesto, pero lo que llama nuestra atención son dos elementos: por un lado, la forma abierta en que el Gobierno de Estados Unidos reafirma una vez más su política de proteger a como dé lugar a su industria farmacéutica aun cuando ello signifique la reducción del acceso a medicamentos y la vulneración del derecho a la salud en el resto del mundo. Desafortunadamente esta no es una actitud nueva, dados los precedentes que sienta año tras año la Oficina de Comercio de dicho país a través de la descalificación unilateral de sus socios comerciales con respecto a la propiedad intelectual a través del][[Informe Especial 301]](http://www.mision-salud.org/nuestras_acciones/acuerdos-comerciales/2017-otra-vez-el-301/)[.]

[Por otro lado, y esto sí es nuevo en el sector salud, nos alarma cómo políticamente la Casa Blanca está seduciendo a su propia industria farmacéutica con la promesa de endurecer la política exterior en defensa de los derechos de propiedad intelectual por encima de los derechos humanos fundamentales, a cambio de un comportamiento más decente en casa, es decir, de una reducción de precios de medicamentos en EEUU. Alarmante mensaje que, de sancionarse esta orden, se estaría transmitiendo a la sociedad estadounidense y al mundo, como ocurrió con el][[reciente anuncio del Presidente Trump con respecto al retiro de EE. UU. del Acuerdo de París sobre cambio climático]](http://www.bbc.com/mundo/noticias-internacional-40124921)[, en tiempos en que el planeta requiere con urgencia mecanismos que permitan fortalecer la justicia social global.]

[A pesar de reunir entre sus miembros a 19 de las 20 empresas farmacéuticas más grandes del mundo, cuyas][[ventas en el 2010 ascendieron a 483.000 millones de dólares]](http://www.mision-salud.org/documentos-y-libros/libro-la-guerra-contra-los-medicamentos-genericos-un-crimen-silencioso/)[, PhRMA se sigue quejando de que no tiene “nadita que comer”. Y por eso surgen iniciativas del ejecutivo estadounidense dirigidas a evitar su muerte por inanición, como la aquí descrita. Veremos si prosperan los quejidos de esta “][[pobre viejecita]](https://www.poemas-del-alma.com/rafael-pombo-la-pobre-viejecita.htm)[”.]

------------------------------------------------------------------------

###### [Las opiniones expresadas en este artículo son de exclusiva responsabilidad del autor y no necesariamente representan la opinión de Contagio Radio.] 
