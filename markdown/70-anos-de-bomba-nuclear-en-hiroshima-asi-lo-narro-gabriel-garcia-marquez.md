Title: 70 años de la bomba nuclear en Hiroshima: así lo narró Gabriel García Márquez
Date: 2015-08-06 14:27
Category: El mundo, Política
Tags: 70 años de Hiroshima, bomba atómica, Estados Unidos, Gabriel García Márquez, Hiroshima, Japón, Segunda Guerra Mundial, Truman
Slug: 70-anos-de-bomba-nuclear-en-hiroshima-asi-lo-narro-gabriel-garcia-marquez
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/08/Hiroshima.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: [danielniazi.com]

*El  6 de agosto de 1945, finalizando la Segunda Guerra Mundial, el presidente de Estados Unidos de ese entonces, Harry Truman, ordenó el primer bombardeo atómico que ocasionó un devastador saldo 140.000 muertes de japoneses, en su gran mayoría civiles, además de 360.000 heridos y una ciudad totalmente destruida.*

*Diez años después, en 1955, el periodista, escritor y Nobel de literatura, Gabriel García Márquez, narró ese acontecimiento a partir del relato del sacerdote jesuita español Pedro Arrupe, quien en ese momento vivía en Hiroshima y era el rector del noviciado de la compañía de Jesús de esa ciudad.*

### En Hiroshima, a un millón de grados centígrados 

Un testigo presencial de la devastación de Hiroshima por la bomba atómica está desde ayer en Bogotá: el sacerdote jesuita Pedro Arrupe, quien el 6 de agosto de 1945 -primer día de la era atómica- desempeñaba el cargo de rector del noviciado de la compañía de Jesús en Hiroshima. Por ser español y ser España un país neutral, el padre Arrupe continuaba en territorio japonés después de que el gobierno del Mikado había dispuesto de todos los extranjeros originarios de países beligerantes. No había guerra en Hiroshima.

Un testigo presencial de la devastación de Hiroshima por la bomba atómica está desde ayer en Bogotá: el sacerdote jesuita Pedro Arrupe, quien el 6 de agosto de 1945 -primer día de la era atómica- desempeñaba el cargo de rector del noviciado de la compañía de Jesús en Hiroshima. Por ser español y ser España un país neutral, el padre Arrupe continuaba en territorio japonés después de que el gobierno del Mikado había dispuesto de todos los extranjeros originarios de países beligerantes. No había guerra en Hiroshima. Curiosamente, en una de las principales ciudades japonesas, con 400.000 habitantes, de los cuales 30.000 eran militares, no se habían conocido los estragos de una guerra internacional de seis años: una sola bomba había sido arrojada sobre la ciudad, y sus habitantes tenían motivos para pensar que se trató de un bombardeo accidental, sin ninguna consecuencia.

[![ResidentialStreetInHiroshimaAfterA-Bombing](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/08/ResidentialStreetInHiroshimaAfterA-Bombing-1024x638.jpg){.aligncenter .wp-image-11973 .size-large width="604" height="376"}](https://archivo.contagioradio.com/70-anos-de-bomba-nuclear-en-hiroshima-asi-lo-narro-gabriel-garcia-marquez/residentialstreetinhiroshimaaftera-bombing/)

**Escuelas de 2.000 niños**

Sin embargo -cuenta el padre Arruple- la población civil estaba preparada para cualquier emergencia. La policía de Hiroshima tenía una organización perfecta, por medio de la cual se controlaba a una ciudad más grande y más poblada que cualquiera de las ciudades colombianas: una ciudad compuesta en general por la clase media japonesa, dedicada al comercio en pequeña escala y a la pesca fluvial. De los 100.000 habitantes 50.000 eran niños en edad escolar. Y es posible afirmar que el 6 de agosto de 1945, eso 50.000 niños estaban en la escuela, mientras sus padres se dirigían al trabajo. En el Japón la educación era obligatoria durante los 8 primeros años, y cada escuela de Hiroshima era un enorme local con capacidad para 2.000 niños.

**El último minuto**

Mientras Tokio, la capital, había sido devastada en gran parte por los constantes bombardeos, Hiroshima era una gigantesca ciudad intacta, con casas de madera construidas de madera liviana para disminuir el constante riesgo de los terremotos. Todos los habitantes, salvo los sacerdotes católicos y 500 japoneses, profesaban el culto Buda: había 750 templos, y apenas una pequeña parroquia católica en el centro mismo de la explosión, y una capilla en el noviciado, a 6 kilómetros de distancia.

A pesar de que nunca había padecido un bombardeo, la población de Hiroshima severamente disciplinada, se precipitaba a los refugios cada vez que sonaban las sirenas de alarma. Había numerosas sirenas distribuidas por toda la ciudad. El 6 de agosto de 1945, un poco antes de las ocho de la mañana, los ciudadanos que se dirigían a su labor, y los niños en la escuela (las clases comenzaban a las siete), oyeron sonar las sirenas y corrieron a los refugios antiaéreos. Poco después se anunció que había cesado el peligro y la ciudad reanudó su marcha normal.

**¡El flash!**

El padre Pedro Arrupe cuenta  que en ese instante, después de la misa y el desayuno, se encontraba en su alcoba cuando sonaron las sirenas de alarma. Luego oyó la señal de que había cesado el peligro. El día comenzaba como siempre. En el noviciado, a pesar de la distancia, se advertía perfectamente el movimiento de la  ciudad.

“De pronto vi un resplandor como el de la bombilla de un fotógrafo”, dice el padre Arrupe. Pero no recuerda haber escuchado la explosión. Hubo una vibración tremenda: las cosas saltaron de su escritorio y la alcoba fue invadida por una violenta tempestad de vidrios rotos, de pedazos de madera y ladrillos. Un sacerdote que avanzaba por el corredor fue arrastrado por un terrible huracán. Un segundo después surgió un silencio impenetrable, y el padre Arruple, incorporándose trabajosamente, pensó que había caído una bomba en el jardín.

[![hiroshima (1)](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/08/hiroshima-11-1024x809.jpg){.aligncenter .wp-image-11972 .size-large width="604" height="477"}](https://archivo.contagioradio.com/70-anos-de-bomba-nuclear-en-hiroshima-asi-lo-narro-gabriel-garcia-marquez/hiroshima-1-2/)

**¿Qué pasó?**

El antiguo rector del noviciado de Hiroshima, que tiene la apariencia de ser un hombre sereno, recuerda aquel instante particularmente por el silencio. Transcurrieron más de 10 minutos después del relámpago, sin que se hubiera dado cuenta de que la ciudad estaba en llamas. Los habitantes del noviciado tuvieron tiempo de inspeccionar el jardín, antes de que el humo blanco y espeso se disipara por completo y se viera, a seis kilómetros de distancia, el gigantesco e incontenible incendio que devoraba la ciudad.

“Ahora cualquiera entiende esto”, explica el padre Arrupe. Pero aquel día nadie había oído hablar de una bomba atómica ni de la posibilidad de que alguien la fabricara y la lanzara sobre una ciudad de 400.000 habitantes. Pensaron que se trataba de un accidente local, y los funcionarios del noviciado se dirigieron a la ciudad a prestar los primeros auxilios. Fueron en bicicleta.

**Recuerdo del Apocalipsis**

“No hay modo de describir lo que encontramos”, cuenta el sacerdote. Y dice sencillamente que hay que imaginar el caos: donde antes había calles no había sino escombros; donde había casas solo se encontraban ruinas, y en la terrible crepitación del incendio y el humo y el polvo, era imposible ver o escuchar algo que recordara la presencia humana.

Gente humilde de las aldeas vecinas trataban de llegar al centro de la catástrofe. Pero era imposible. Las enormes llamaradas de más de un ciento de metros de altura impedían el acceso a la ciudad. Antes del medio día comenzaron a desarrollarse fantásticos fenómenos atmosféricos.

**Un terremoto de laboratorio**

Primero fue la lluvia. Un violento aguacero se desplomó sobre la ciudad y extinguió las llamas en menos de una hora. Después fue un tremendo huracán que condujo por el aire enorme troncos de árboles calcinados, rueda de vehículos, animales muertos y toda clase es escombros. Por encima de las cabezas de los sobrevivientes , pasaron a considerable altura, volando, impulsados por el huracán, los destrozos de la catástrofe.

En aquel instante fueron aterradores, pero en la actualidad aquellos fenómenos están perfectamente explicados: la condensación de vapor provocada por la inconcebible elevación de la temperatura -que se ha calculado en un millón de grados centígrados- fue el origen de la lluvia torrencial. El vacío, la descompensación producida por la violenta absorción, dio origen al huracán apocalíptico que contribuyó a agravar la confusión y el terror.

**Las primeras víctimas**

El primer contacto que tuvo el padre Arrupe  con las víctimas de las catástrofe fue la visión de tres mujeres jóvenes , abrazadas, que con el cuerpo en carne viva surgieron de los escombros. Entonces comprendió que no s trataba de un incendio corriente: el cabello de las víctimas se desprendía con extrema facilidad y en pocas horas la ciudad había sido destruida por completo y sus habitantes reducidos a una confusa multitud de cadáveres y moribundos ambulantes.

Se ignoraba cuáles debían ser los primeros auxilios en aquel caso. No eran quemaduras corrientes. A un grupo de niños socorrido por el padre Arrupe, se le desprendía sin esfuerzo el cuero cabelludo. Entre piel y los huesos se encontraron pedazos de vidrios incrustados.

**A salvo en el río**

Hiroshima e una ciudad construida en las cinco islas formadas por el delta del río Otagawa. Cuatro brazos fluviales la atraviesan de lado a lado. Cuando estalló el caos, cuando las llamas gigantescas se levantaron en toda la ciudad, los sobrevivientes solo pensaron en correr hacia el agua. A las cinco de la tarde el padre Arrupe logró penetrar a la ciudad. Avanzó, con una multitud venida de las aldeas vecinas, por sobre escombros, y vio cuerpos destrozados, rostros de agonizantes desfigurados y los ríos densamente ocupados por una multitud caótica y delirante.

**“Los niños de Hiroshima”**

En la película “Los Niños de Hiroshima” -una película que el padre Arrupe no ha visto- se ha reconstruido la catástrofe, minuto a minuto. Por la descripción que hace el único testigo presencial que ha venido a Colombia, se advierte que la reconstrucción del filmes de una asombrosa fidelidad, de un milagroso realismo. La multitud se desplazó, como una gran masa flotante, hacia los diferentes brazos de los ríos. Y hubo una razón para que fueran mayores los estragos en la población infantil: a las 8:10 de la mañana, hora en que estalló la bomba, puede decirse que no había un niño en edad escolar cerca de sus padres. Todos estaban en la escuela. Cuando al atardecer empezaron a prestarse los primeros auxilios, los padres de familia estaban bajo los escombros de los hogares o los establecimientos comerciales. Y los niños, todos los de Hiroshima, confundidos, desfigurados y sin identificar; 50.000 niños estudiantes, estaban muertos, heridos o agonizando en masa, bajo los escombros de las escuelas.

![Llorando](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/08/Llorando.jpg){.aligncenter .wp-image-11970 .size-full width="600" height="400"}

**20 kilos de ácido bórico**

En Hiroshima había 260 médicos, 200 murieron instantáneamente a causa de la explosión. La mayoría de los restantes quedó herida. Los muy pocos sobrevivientes -entre ellos el padre Arrupe , graduado en medicina no disponía de ningún elemento para auxiliar a las víctimas. Las farmacias, los depósitos de drogas, habían desaparecido bajo los escombros. Y aun en el caso de que se hubiera dispuesto de elementos, se ignoraba por completo qué clase de tratamiento debía de aplicarse a las víctimas de aquella monstruosa explosión.

Los primeros heridos auxiliados por el padre Arrupe, sin embargo, fueron favorecidos por un acontecimiento todavía no explicado: en medio de la confusión un aldeano puso a disposición del sacerdote un saco con 20 kilos de ácido bórico. Fue el primer tratamiento que se les administró: cubrir todas las heridas con ácido bórico. En la actualidad, todos se encuentran en buen estado de salud, dice el padre Arrupe, quien todavía no puede entender qué hacía un campesino de Hiroshima con 20 kilos de ácido bórico en su casa.

**Tres causas de muerte**

El antiguo rector del noviciado de Hiroshima dice que en la ciudad no hubo pánico el 6 de agosto de 1945. La población recibió la catástrofe con su indolente fatalismo oriental. Los sobrevivientes se desplazaron hacia el agua no en busca de refrigeración  -que es una creencia generalizada- sino en busca de un lugar donde estuvieran a salvo de las llamas.

Resulta imposible establecer por la experiencia de Hiroshima, los verdaderos efectos de la bomba atómica. El lugar donde estalló -a 600 metros de altura, pues fue lanzada en paracaídas- era el centro geográfico y al mismo tiempo el centro comercial de la ciudad. En torno a ese centro, en una área de dos kilómetros y medio, los habitantes fueron víctimas inmediatas de la radioactividad, el calor y la explosión. En el área de dos kilómetros y medio en torno al centro de radioactividad, fueron víctimas de las reacciones térmicas y de la explosión. De allí en adelante, en un área de seis kilómetros en la cual se encontraba el noviciado de la Compañía de Jesús, las víctimas fueron ocasionadas exclusivamente por la explosión.

**La huella de un hombre**

El padre Arrupe opina que ninguna de las personas penetraron el área de radioactividad después de la explosión, sufrieron trastornos físicos o mentales posteriores. Él mismo penetró esa área seis horas después de la catástrofe, sin sufrir ninguna perturbación, pues el cabello que ahora le falta -aclara sonriente- se ha desprendido de su cabeza por causas diferentes a la radioactividad.

En el área de explosión hubo considerable cantidad de víctimas, ocasionadas por los escombros y los cristales esparcidos. En cambio, en el centro mismo de la explosión, en el área radioactiva, seis sacerdotes que se encontraban en la sede de la parroquia -un edificio de concreto-resultaron ilesos. Solo uno de ellos presentó más tarde trastornos físicos ocasionados por la radioactividad. En el edificio del banco de Osaka quedó estampada en la pared la silueta de un obrero que en el instante de la explosión ascendía por la escalera.

**Hoy**

La recuperación moral de Hiroshima fue casi inmediata. Al día siguiente de la catástrofe empezaron a recibirse auxilios de las ciudades vecinas. Durante seis días cada sobreviviente recibió una escudilla con 150 gramos de arroz. La fortaleza moral del pueblo fue superior a la bárbara y despiadada experiencia atómica. En menos de una semana se cremaron los cadáveres, se organizó a los sobrevivientes, se improvisaron los hospitales y se identificó a los millares de niños que quedaron a la deriva.

A fines de ese año la ciudad estaba rudimentaria pero totalmente reconstruida. Los escombros había sido removidos y las casas fabricadas de nuevo con latas de conserva, papel periódico y desperdicios la catástrofe. Desde el trágico 6 de agosto hasta el momento actual, ha sido reconstruida tres veces. La segunda vez fue de madera. En la actualidad, y en virtud de una ley japonesa que ordena que sea construida en concreto toda casa con más de dos plantas, la ciudad está completamente modernizada, y tiene la calle más ancha del mundo: más de cien metros. Pero para transitar por esa calle hacen falta las 240.000 personas que murieron en la explosión.

[![comp\_hiroshima\_01](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/08/comp_hiroshima_01-1024x735.jpg){.aligncenter .wp-image-11971 .size-large width="604" height="434"}](https://archivo.contagioradio.com/70-anos-de-bomba-nuclear-en-hiroshima-asi-lo-narro-gabriel-garcia-marquez/comp_hiroshima_01/)
