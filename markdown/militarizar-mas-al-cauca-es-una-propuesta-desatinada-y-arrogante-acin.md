Title: Militarizar más al Cauca es una propuesta "desatinada y arrogante": ACIN
Date: 2019-10-31 12:49
Author: CtgAdm
Category: Comunidad, Nacional
Tags: Cauca, Gobierno, indigena, militares
Slug: militarizar-mas-al-cauca-es-una-propuesta-desatinada-y-arrogante-acin
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/Cauca.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/Cauca-1.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: @IvanDuque  
] 

Integrantes de los pueblos indígenas señalaron que militarizar Cauca no resolvería los problemas de seguridad que se viven en el territorio, y más bien obedece a una forma de responder mediáticamente a lo ocurrido en el norte del Departamento. La respuesta surge luego del Consejo de Seguridad realizado en Santander de Quilichao, Cauca, donde el presidente Iván Duque anunció que al territorio llegarán otros 2.500 hombres de las Fuerzas Armadas, para evitar que ocurran masacres como la registrada en Tacueyó.

### **Más militares, ¿una solución para Cauca?** 

Luego de la masacre de Tacueyó que cobró la vida de cinco integrantes de los pueblos indígenas, y la posterior respuesta del Gobierno se ha ampliado la discusión sobre tres asuntos: La militarización del territorio, las propuesta de establecer una 'Carpa Blanca' y la autonomía de los territorios indígenas. **Eduin Capaz, coordinador de derechos humanos de la Asociación de Cabildos Indígenas del Norte de Cauca (ACIN)** habló sobre las tres situaciones.

Respecto al envío de 2.500 hombres al territorio por parte de la Fuerza de Despliegue Rápido (FUDRA) N° 4, Capaz sostuvo que "es una decisión desatinada, apresurada y a aveces, hasta arrogante por parte del Gobierno Nacional", porque no es la primera vez que se insiste en una medida militarista para un problema que es de tipo estructural. El líder indígena manifestó que en varias oportunidades han manifestado algunos de los problemas y no han sido escuchados, en cambio, se insiste en "este tipo de acción que no deja de ser mediática", pero que en el pasado no ha presentado resultados porque la situación del Cauca sigue igual.

Algunas de las peticiones que han surgido de los encuentros con el Gobierno se manifestaron durante la Minga indígena de este año en la que se reclamaban, entre otras cosas,presencia de organismos de control que tengan presencia en el territorio, el respeto por los acuerdos alcanzados en el marco del **Plan Nacional Integral de Sustitución (PNIS)** y una acción conjunta entre instituciones y comunidades para la toma de decisiones. (Le puede interesar: ["La minga indígena que reclama a Duque cumplir los acuerdos"](https://archivo.contagioradio.com/minga-indigena-duque/))

Adicionalmente, han pedido que se revisen las actuaciones de la Fuerza Pública en el territorio, porque a pesar de que se calcula que en el territorio hay cerca de 13 mil hombres de las FF.MM., su acción no ha sido eficaz y por el contrario, se ha visto cuestionada en cuanto a la protección que brindan a las comunidades. (Le puede interesar: ["Comunidades de C. responsabilizan al Ejército del asesinato de Flower Jair Trompeta"](https://archivo.contagioradio.com/comunidades-del-cauca-responsabilizan-al-ejercito-del-asesinato-de-flower-jair-trompeta/))

### **Carpa Blanca, o la interacción entre entidades y comunidad** 

La propuesta de Carpa Blanca también fue apoyada por el presidente Duque, se trata de una interacción entre entidades del Estado y comunidades para encontrar soluciones conjuntas a los distintos problemas identificados. La Defensoría del Pueblo propuso esta solución, cuyo resultado también estaría enfocado en lograr la confianza en las instituciones del Estado por parte de los pueblos indígenas. (Le puede interesar: ["Son urgentes las medidas diferenciales para proteger a defensoras de DD.HH."](https://archivo.contagioradio.com/son-urgentes-las-medidas-diferenciales-para-proteger-a-defensoras-de-dd-hh/))

Capaz sostuvo que en algunas comunidades indígenas ya se ha evaluado el tema, y se está pensando participar en el esquema "pero **necesitan la certeza y el requerimiento explícito de que no esté la fuerza pública**". Según el coordinador de derechos humanos, este pedido tendría dos razones: La coordinación debe tener un enfoque civil y no militar, y la confianza de las comunidades respecto a los uniformados se ha visto afectada por distintos hechos, que no se resolverán al estar en la misma organización.

### **Y sobre la autonomía indígena...** 

El tercer elemento que ha entrado a jugar, y se suele mencionar como un problema para hacer control efectivo de los territorios, es que el autogobierno indígena no permite a la Fuerza Pública ingresar armados a sus territorios y por lo tanto, son espacios 'incontrolables'. Sin embargo, Capaz aseguró que esta afirmación no es cierta, porque **actualmente "existen unidades de Fuerza Pública en territorios indígenas, los hay y seguramente los seguirá habiendo",** aún si no cuentan con el consentimiento de las autoridades.

A ello se suma que, aún con presencia militar al interior de los territorios o cerca a ellos, la violencia en Cauca no ha sido solucionada y las causas estructurales que la generan siguen vigentes. Por esta razón, Capaz concluyó que los **ataques a su autonomía ven que es "una tesis más para ocultar las ineficiencias por parte del Estado**". (Le puede interesar: ["Con alternativas a la militarización la guerra dejará de ser un negocio"](https://archivo.contagioradio.com/si-hay-alternativas-para-la-militarizacion-deje-de-ser-un-negocio/))

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe><iframe id="audio_44016540" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_44016540_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
