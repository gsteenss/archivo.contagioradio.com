Title: Comunidad "el Provenir" en Puerto Gaitan en riesgo de desplazamiento forzado
Date: 2015-09-16 16:35
Author: AdminContagio
Category: Comunidad, Nacional, Resistencias
Tags: Comunidad El Porvenir, Cumbre Agraria, Desplazamiento, Meta, Puerto Gaitán, victor carranza
Slug: comunidad-el-provenir-en-puerto-gaitan-en-riesgo-de-desplazamiento-forzado
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/Panorama-sans-titre247-1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto:Notimundo.in 

###### [16 SEP 2015]

<iframe src="http://www.ivoox.com/player_ek_8420752_2_1.html?data=mZmfkpyZdo6ZmKiak5eJd6KpmJKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRh9Dh1tPWxsbIb4amk8rZjbXWs9fZz87fh5eWb8bijLXix9fYs4y7ws7hw9OPqc%2Bf087S1czTb8XZjMmah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Miguel Briceño líder comunitario] 

En Puerto Gaitán, Meta, **miembros de las comunidades del Porvenir y de la vereda Mataratón denuncian que en los últimos días Asoyopo y AS Pedregra**l, han venido cercando y vendiendo tierras con el fin de desplazar aproximadamente 140 familias de campesinos que viven y cultivan estas tierras desde hace más de cincuenta años.

Miguel Briceño líder comunitario, indica que hay un encargado (Terrero) que **divide las tierras de a 50 hectáreas y las vende** a supuestas personas de asociaciones de desplazados, como el señor Abril, quien según Briceño podría tener vínculos con la familia Carranza.

Briceño dice indignado que **esta situación ha traído problemas para alimentar sus ganados y cultivar sus tierras**, además indica que el único **responsable de esta situación es el Incoder** (Instituto Colombiano de Desarrollo Rural) en vista de que “nunca han mirado a los campesinos, pero en cambio miran a los terratenientes” y agrega que “si los campesinos no tienen derecho a la tierra ¿quién lo tiene?, si nosotros somos quienes producimos”.

Por su parte la comunidad **ha colocado distintas querellas** ante la personería en Puerto Gaitán que hasta el momento no ofrecen respuestas eficientes para evitar el confinamiento al que han obligado a 140 familias, o el desplazamiento forzado del que están en riesgo de no pararse la parcelación de las más de 28.000 hectáreas sobre las que reclaman propiedad.

Mientras esto la decisión de la comunidad es quedarse “***nosotros no tenemos que pedirle permiso a nadie para trabajar en estas tierras, porque estas tierras son nuestras***”.

### **Una comunidad que ha resistido pacíficamente:** 

A la comunidad de El Porvenir en los años 80 llegaron los carranceros un grupo paramilitar que sembró el terror en la región y cuyo centro de entrenamiento era el Hato Caviona, vecino de las 25 mil has. del Porvenir.

Aunque se presentaron desplazamientos, asesinatos y otros crímenes de lesa humanidad, las comunidades resistieron pacíficamente, sin saber además de la violencia también se gestaba el intento de despojo con supuestas ventas de la finca a empresas y personas cercanas al clan Carranza, con documentos, amenazas e invasiones de parte de las hectáreas que hoy se disputan.

**Estas tierras son de las comunidades:**

En abril de este año Hollman Carranza, entregó al Presidente Santos, 28.000 hectáreas del predio desde donde hoy se hacen estas denuncias, para que los campesinos y víctimas de esta zona del Meta fueran reparadas. Sin embargo, la comunidad "El Porvenir" afirma que esas tierras nunca le pertenecieron a la familia Carranza por lo tanto no las podían entregar.

Estos terrenos habían sido adquiridos de manera irregular por Víctor Carranza, alias “El Patrón”, mediante la compra a supuestos campesinos y con ayuda de los paramilitares, como lo indicó la fiscalía en su momento “las autodefensas mantuvieron contactos con hombres armados de las ACMV  que se hacían llamar ‘Los Carranceros’, quienes mantenían relación con el señor Víctor Carranza”.
