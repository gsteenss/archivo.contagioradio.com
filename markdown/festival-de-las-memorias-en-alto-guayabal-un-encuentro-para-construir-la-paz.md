Title: Festival de las Memorias en Alto Guayabal: un encuentro para construir la paz
Date: 2019-05-20 18:25
Author: CtgAdm
Category: Comunidad, DDHH
Tags: Alto Guayabal, Chocó, Comunidades Indígena Embera, Festival de la Memoria
Slug: festival-de-las-memorias-en-alto-guayabal-un-encuentro-para-construir-la-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/Festival-de-las-Memoria.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Leonardo Lovera] 

Como parte de la comitiva que hizo parte del **Festival de las Memorias en Alto Guayabal, Chocó, celebrado del 17 al 19 mayo, Juan David Enciso**, del Centro de Estudios en Educación para la Paz de la Universidad de la Sabana elogió la oportunidad de adentrarse al interior de las comunidades indígenas y afrodescendientes del Chocó con el fin de seguir creando puentes que permitan la construcción de la paz desde las regiones.

El profesor afirmó que sintió muy acogido por la comunidad y destacó cómo los habitantes de estos resguardos, personas que han sido afectadas por el conflicto conllevan esa situación y salen adelante en medio de ese dolor, **"mantienen viva su ancestralidad, es una manera de tener un encuentro intercultural para pensar nuevas posibilidades de construir la paz".**

Del Festival de las Memorias también resaltó que a pesar de los fenómenos climáticos que conoció de primera mano y que podrían afectar la forma de vida de las comunidades embera de Alto Guayabal, estas **"han sabido forjar una armonía con la naturaleza"**, una cualidad a la que se suma las reuniones que desarrollan las comunidades para consolidar su cultura ancestral, demostrando  "la capacidad de organización y de cohesión al interior de estos grupos".

### Compartimos una misma verdad y es la de ser humanos

Enciso también se refirió a la necesidad de profundizar en cómo la violencia ha afectado la relación de las comunidades con su entorno y conceptos como memoria y  verdad  y en cómo puede restablecerse desentrañando la riqueza de sus pueblos a través de  lo histórico y lo cultural,  "hay una verdad que se desprende del hecho que tenemos una relación con nuestros seres queridos y esa relación es el tejido de nuestras vidas".  expresó.

El profesor concluye que aunque esa verdad a la que se refiere esté un poco más occidentalizada, "también tiene raíces ancestrales que junto a las tradiciones más nativas de los embera, cmparten una misma verdad y es la de ser humanos". [(Lea también JEP y comunidades hacen historia con primera audiencia en zona rural del Cacarica)](https://archivo.contagioradio.com/jep-comunidades-audiencia-cacarica/)

<iframe id="audio_36109296" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_36109296_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
