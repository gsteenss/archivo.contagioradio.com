Title: Cuatro causas que tienen en alto riesgo a defensores de DDHH
Date: 2017-07-19 16:30
Category: DDHH, Paz
Tags: Defensores de Derecho Humanos, Observatorios, OMCT, paz, tortura
Slug: cuatro-causas-por-las-cuales-la-paz-no-ha-llegado-para-quienes-defienden-los-ddhh
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/FIRMA-DEL-ACUERDO-DE-PAZ-10.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [19 Jul. 2017]

“La tasa de homicidios más baja en Colombia en 40 años en el contexto del proceso de paz contrasta con un clima de constantes amenazas y un fuerte aumento de asesinatos de defensores de derechos humanos” esa fue una de las conclusiones que entregó El Observatorio para la Protección de los Defensores de DDHH de la Organización Mundial contra la Tortura –OMCT – y la Federación Internacional de DDHH –FIDH- que realizó una misión internacional en diversos lugares del país.

De igual modo, los integrantes del Observatorio denunciaron que **luego de visitar del 11 al 19 de julio varios lugares como Bogotá, Norte de Santander, Antioquia y Cauca** pudieron constatar la grave situación que atraviesan los líderes y lideresas en Colombia.

Según **Miguel Martín, integrante del Observatorio y director de la oficina en Bruselas de la OMCT,** una de las  tendencias es el asesinato a lideres sociales que defienden la tierra y el medio ambiente en zonas rurales, quienes se ven enfrentados a constantes amenazas. Le puede interesar:

Gracias a la recolección de testimonios durante sus visitas el Observatorio encontró  cuatro causas estructurales por las que la paz no ha llegado a los defensores y defensoras de derechos humanos.

### **Primera causa: El paramilitarismo sí existe ** 

A través de la documentación de varios casos, el Observatorio asegura que pudo comprobar la connivencia entre funcionarios del Estado y grupos paramilitares en diversas partes del país como Norte de Santander y Antioquia. Le puede interesar: [En 15 días han asesinados 2 defensores de DDHH en el Cauca](https://archivo.contagioradio.com/en-menos-de-15-dias-han-asesinados-2-defensores-de-ddhh-en-el-cauca/)

Para Martín hay una multicausalidad de los ataques de estos grupos paramilitares, una de ellas es que **son personas que con su trabajo en defensa de los derechos humanos incomodan los intereses de estos grupos. **

Igualmente recalcó que de las zonas de las cuales las FARC ha salido el Estado no ha tenido una reacción rápida para llegar a estos territorios “este vacío de poder ha implicado unas disputas al control territorial y esto ha puesto la presión sobre los líderes sociales”.

### **Segunda Causa: Señalamientos y estigmatización a los defensores de DDHH** 

Pudieron constatar además que en algunos lugares **se mantienen los señalamientos y la estigmatización** por parte de las autoridades públicas o de los fiscales que llevan los casos de personas defensoras de derechos humanos que son detenidas. Le puede interesar: [120 defensores de DDHH asesinados en 14 meses en Colombia: Defensoría](https://archivo.contagioradio.com/120-defensores-asesinados-defensoria/)

“Uno debería esperar que en un escenario de posconflicto, en el que se supone está entrando Colombia, haya un cambio cultural de lo que es la defensa de los derechos humanos y sin embargo vemos cómo sigue habiendo instancias de difamación y estigmatización vinculándolos por ejemplo de ser parte del Ejército de Liberación Nacional (ELN)”.

### **La criminalización de la protesta social ** 

El observatorio recibió diversas denuncias del uso desmedido de la fuerza pública durante diversas protestas pacíficas en Colombia,  que se dieron en Cauca y Buenaventura durante el paro cívico realizado el pasado mes de mayo de 2017.

**“El paro cívico fue reprimido de forma brutal** y aunque no significa que cada protesta sea reprimida, sí que hay un patrón de utilizar un enfoque represivo para controlar las manifestaciones. El Estado tiene la obligación de garantizar el derecho a la protesta” aseveró Martín.

### **Estigmatización de Mujeres y Comunidad LGBTI** 

Durante su recorrido pudieron reunirse con organizaciones de mujeres y de la comunidad LGBTI quienes les compartieron las situaciones a las que se ven expuestas y expuestos por su trabajo en la defensa de los derechos humanos. ([Lea también: Asesinada Ruth Alicia López en Medellín](https://archivo.contagioradio.com/asesinada-lideresa-asokinchas-medellin/))

“En diversas reuniones que hemos tenido con organizaciones sociales hemos podido constatar que se ha producido un aumento de ataques contra mujeres y contra movimiento LGBTI. Nuestra hipótesis es que hay una aparente relación en el aumento en los ataques y la violencia verbal que hubo luego de la campaña del Plebiscito alrededor de la mal llamada ideología de género. Estos discursos tan negativos les han situado en la mira y pensamos que ahí hay una relación en el aumento de víctimas en los últimos meses y ese discurso”.

### **Tercera Causa: La impunidad es absoluta** 

Para el grupo integrante del Observatorio **la impunidad refuerza la vulnerabilidad de todas las personas que defienden los derechos humanos en Colombia.** Según cifras oficiales de la Fiscalía, a la fecha solamente se ha conocido de 5 sentencias condenatorias de casos de asesinatos a líderes y lideresas, en un país que completa 152 asesinatos solo desde 2016 hasta 2017.

Según Martín en Colombia se sigue sin realizar una acción rápida para investigar las amenazas y los asesinatos contra personas defensoras de derechos humanos "cuando hablamos de amenazas contra personas defensoras o de homicidios los resultados en las investigaciones son muy pobres”.

Para el Observatorio es preocupante que las instituciones no estén prestando atención a las amenazas contra líderes y lideresas. Además, manifiesta Martín que han visto que en los pocos casos que avanzan se da con los responsables materiales, mas no los intelectuales.

“La **única forma de prevenir estos ataques es ir un paso más allá y poder detener también a los autores intelectuales**, mientras este escenario no cambie, se manda un mensaje a todos los defensores de derechos humanos de que nada les garantiza no asesinarles y esto tienen un efecto perverso para los ellos, porque es el temor de saber que la institucionalidad no tiene suficiente capacidad o voluntad política para reaccionar”.

### **Última causa: Respuesta estatal débil** 

Si bien el Observatorio dice que identificó un amplio “entramado institucional” que se encarga de la protección de defensores y defensoras en Colombia, la respuesta sigue siendo muy débil, lo que desencadena en asesinatos y amenazas. Le puede interesar: [185 defensores de derechos humanos han sido asesinados en el último año](https://archivo.contagioradio.com/185-defensores-de-derechos-humanos-han-sido-asesinados/)

Manifiestan que esto se debe a la **insuficiencia de medidas que hagan frente a las causas estructurales que ponen en riesgo a los defensores**, la falta de implementación de los planes nacionales, las escasas garantías para ser defensores de DDHH en Colombia y la falta de una atención y unas medidas con enfoque diferencial de edad, género o nivel socioeconómico, por ejemplo.

### **Algunas recomendaciones luego de entregar los hallazgos preliminares** 

Las recomendaciones serán entregadas sobre todo para la institucionalidad por que de esta manera le recordarán al **Estado que tiene la obligación de respetar los derechos,** pero también de protegerlos. Le puede interesar: [Colombia es el segundo país más peligroso para defensores del ambiente](https://archivo.contagioradio.com/en-colombia-fueron-asesinados-37-ambientalistas-en-2016/)

“Hay que pasar de un discurso de protección al de garantías y esto implica ir por las causas estructurales que están detrás de los ataques (…) **hay que reformar el ESMAD para que haya control, vigilancia y sanción para las violaciones de los DDHH (…)** se debe mejorar la forma de investigar en la Fiscalía, hay que llegar hasta el final, ir por los actores intelectuales”.

<iframe id="audio_19895433" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_19895433_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU).
