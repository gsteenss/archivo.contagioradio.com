Title: Denuncian intención de destituir al General Villegas quien pidió perdón por asesinato de Dimar Torres
Date: 2019-04-29 17:33
Author: CtgAdm
Category: DDHH, Judicial
Tags: Dimar Torres
Slug: denuncian-intencion-de-destituir-al-general-villegas-quien-pidio-perdon-por-asesinato-de-dimar-torres
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/vide.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:ASCAMCAT] 

A través de su cuenta de Twitter, **el senador Antonio Sanguino** denunció que tras la visita de la Comisión de Paz al Catatumbo y de posterior petición de perdón del General Villegas por la muerte del excombatiente de Farc, Dimar Torres a la comunidad, el Ministerio de Defensa  ordenó abrir proceso penal y disciplinario contra el  general para darlo de baja.

Agrega que estas represalias del MinDefensa también se extienden al coronel Pérez, comandante del Batallón quien acompañó a Villegas  en sus declaraciones frente a la población de Convención, este ya fue notificado de su traslado y relevo del mando en el Batallón. [(Lea también: General Villegas reconoce asesinato de Dimar Torres mientras MinDefensa pretende ocultarlo) ](https://archivo.contagioradio.com/general-villegas-reconoce-asesinato-de-dimar-torres-mientras-mindefensa-pretende-ocultarlo/)

> [\#ATENCIÓN](https://twitter.com/hashtag/ATENCI%C3%93N?src=hash&ref_src=twsrc%5Etfw) | Luego de visita de la [@ComisiondePaz](https://twitter.com/ComisiondePaz?ref_src=twsrc%5Etfw) a Catatumbo y de la petición de perdón del General Villegas por la muerte de Dimar Torres, al parecer [@mindefensa](https://twitter.com/mindefensa?ref_src=twsrc%5Etfw) ordenó proceso penal y disciplinario contra el Gral para darlo de baja. En estos momentos es trasladado a Bucaramanga
>
> — Antonio Sanguino Senador (@AntonioSanguino) [29 de abril de 2019](https://twitter.com/AntonioSanguino/status/1122956160850698241?ref_src=twsrc%5Etfw)

<p>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
</p>
Adicionalmente, **Guillermo Botero, ministro de Defensa** expresó que el general Diego Luis Villegas no tenía autorización para realizar el pronunciamiento del pasado domingo 27 al exigir justicia y pedir perdón por el asesinato de Dimar Torres.

> El MinDefensa [@GuillermoBotero](https://twitter.com/GuillermoBotero?ref_src=twsrc%5Etfw) dice que el general Diego Luis Villegas no tenía autorización para hacer el pronunciamiento que hizo al pedir perdón por el asesinato del desmovilizado Dimar Torres, que tiene bajo investigación a un cabo del Ejército.
>
> — YolandaRuizCeballos (@YolandaRuizCe) [29 de abril de 2019](https://twitter.com/YolandaRuizCe/status/1122844959202213890?ref_src=twsrc%5Etfw)

<p>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
</p>
###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
