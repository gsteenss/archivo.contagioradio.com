Title: Dos terceras partes de la población de pingüinos están en grave riesgo
Date: 2017-04-25 19:41
Category: Animales
Tags: animales en peligro de extinción, pinguinos
Slug: dos-terceras-partes-la-poblacion-pinguinos-estan-grave-riesgo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/04/pinguinos-e1493166826625.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Taringa 

###### [25 Abr 2017] 

La segunda especie de ave marina más amenazada del mundo, celebra su día en medio de un panorama preocupante. Los pingüinos están bajo grave amenaza**, debido a los efectos del cambio climático y la sobrepesca.  **

Sin embargo, estas a veces desde el pasado vienen padeciendo las irresponsables actividades humanas, que han puesto en peligro la vida del planeta. Antes las personas cazaban a estos animales por su aceite, grasa y carne. Hoy debido a otra serie de actividades humanas, entre ellas las industriales, **el alimento de los pingüinos está en déficit.**

Y es que de acuerdo con un estudio de 2015 del Pew Charitable Trust, dos terceras partes de las **18 especies de pingüinos del planeta, de las Galápagos hasta la Antártida, están en peligro.**

Así lo advierten científicos como Christian Reiss, biólogo de la agencia estadounidense de observación oceánica y atmosférica, quien asegura a AFP que es imperante proteger la Antártida, "Los pingüinos son muy buenos embajadores para comprender la necesidad de proteger los recursos del océano Antártico", y agrega que estos animales, “son especies emblemáticas de este ecosistema y el futuro de su población dependerá de una gestión eficaz de su medio y de la comprensión del papel del calentamiento global y de los impactos humanos".

No obstante, existe una luz para estas aves. Luego de años de conversaciones y negociaciones, los 25 miembros de l**a Convención para la Conservación de los Recursos Marinos Antárticos**  aprobaron crear el mayor santuario marino del mundo en el continente, una zona protegida que abarcará el mar de Ross. Una bahía junto al Pacífico, **con más de 1,55 millones de km2, equivalente a las superficies  de Perú y Ecuador.**

Los científicos esperan que con estas medidas se empiece a garantizar la vida de esas aves marinas, pero además llaman la atención, sobre los terribles efectos del cambio climático sobre casi todas las especies del planeta.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
