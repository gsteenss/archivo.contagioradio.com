Title: Nos defendemos con colores, un año más sin 'Trípido'
Date: 2018-08-19 04:53
Author: ContagioRadio
Category: Sin Olvido
Tags: “Trípido”, Diego Felipe Becerra, falsos positivos
Slug: 7-anos-sin-tripido
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/08/diego-felipe-becerra1-contagio-radio-e1534672072851-770x400-1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: 

###### 19 Ago 2018 

Las pintas de color sobre el cemento con su nombre y la figura de 'Felix el gato', hacen  que el puente de la calle 116 con avenida Boyacá se distinga de cualquier otro en la ciudad de Bogotá. Un escenario de la infamia convertido en santuario dedicado a la memoria de **Diego Felipe Becerra Lizarazo**.

Trípido, como lo recuerdan sus amigos, apenas  tenía 16 años de edad. En el graffiti había encontrado la forma de expresar su sentir y plasmar su identidad con trazos de pintura en aerosol. Paradojicamente, la misma actividad por la que le arrebataron la vida es la que permite que hoy su nombre siga presente en el paisaje urbano de la ciudad.

El 19 de agosto de 2011 fue un viernes, Diego Felipe y sus amigos salieron a realizar unas pintas en la base del puente, allí la policía los encontró y persiguió. Trípido se escondió detrás de un árbol, hasta donde llegó el patrullero Wilmer Alarcón, quien lo increpó para hacerle una requisa, Diego trató de huir, recibiendo por parte del uniformado un disparo por la espalda. Lo llevaron a la Clínica Shaio donde poco pudieron hacer para salvarlo.

Después de asesinarlo, la policía intentó mancillar su nombre para ocultar la verdad, presentándolo como un delincuente común abatido tras asaltar un bus. Alteraron la escena del crimen, sembraron falsas evidencias, indujeron al conductor del servicio de transporte a dar falso testimonio; en suma hicieron un pacto de silencio para defender el 'honor' de la institución.

Sin embargo, la verdad logró abrirse paso en medio de tantas injurias. En 2012 fueron imputados 13 cargos al patrullero Alarcón por homicidio agravado, dado el conocimiento previo de que la víctima se encontraba desarmada. Más tarde, la Procuraduría General de la Nación sancionó con destitución e inhabilidad por 12 años a los comandantes de la estación de policía de la localidad de Suba, al subteniente Rosemberg Madrid coordinador del CAI Andes y el coronel Nelson Castillo, por obstruir la investigación de la Fiscalía.

Para el ente de control, las acciones cometidas por los uniformados fueron gravísimas, puesto que su deber era mantener la escena como fue encontrada, preservar los derechos del joven, y no presentarlo como un 'falso positivo urbano', tratando de legitimar su muerte con la imputación de un crimen inexistente.

En 2016 Alarcón fue condenado a 36 años y seis meses de prisión como autor material del crimen, sin embargo hoy se encuentra prófugo de la justicia. Gustavo Trejos, padre de Diego Felipe y uno de los mayores luchadores por la reivindicación su memoria, indicó que las autoridades le aseguraron que hay una circular roja emitida por la Interpol para ubicar y capturar al asesino, pero no ha tenido efecto porque en su criterio, no le conviene a dos generales de la república vinculados con la alteración de la escena lo que el patrullero pueda aportar en al caso.

Tanto Trejos como otros familiares y amigos han sido [revictimizados, hostigados, presionados y amenazados](https://archivo.contagioradio.com/dilaciones-y-amenazas-caracterizan-el-proceso-por-el-asesinato-de-diego-felipe-becerra/). A pesar de lo exhaustivo, doloroso y largo del proceso después de su muerte, los padres de Trípido han sido protagonistas en búsqueda de verdad y justicia. Además han propendido por la protección de los jóvenes grafiteros, apostando por ver a los muros como medios para comunicar y protestar.

“Después de la muerte de Diego Felipe ha cambiado la concepción sobre el grafiti. Ahora en Bogotá hay un marco legal para proteger a estos artistas y la Policía lo piensa dos veces antes de agredirlos. Nosotros trabajamos por una práctica responsable del grafiti y nuestro trabajo en algunos barrios de Bogotá es bien recibido y agradecido por la sociedad” aseguró el padre del joven.

Para Liliana, su mamá, “Hay algo que los adultos no entienden: no es solo rayar paredes, no es solo criticar lo que hacen los jóvenes. Tenemos que escuchar lo que ellos sienten. Ellos pintan las paredes para expresarse porque como sociedad no los estamos escuchando. El color en las calles relaja a las personas, la vida para muchos ya no es tan gris”.

El rostro de Diego Felipe y su sello distintivo, dan vida a los muros fríos del puente de extremo a extremo. Además su seudónimo artístico pervive en el 'Trípido Fest', un evento que se realiza cada año desde 2011 con actividades musicales, pedagógicas y artísticas en su memoria, y su nombre identifica desde 2015 una casa de la juventud en la localidad de Suba.
