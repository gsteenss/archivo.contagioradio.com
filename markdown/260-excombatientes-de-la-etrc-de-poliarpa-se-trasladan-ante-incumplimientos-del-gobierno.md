Title: Integrantes de FARC se van porque ni el 5% de la ETCR de Policarpa, Nariño se ha construido
Date: 2017-11-23 18:47
Category: Nacional, Paz
Tags: acuerdo de paz, excombatientes, FARC
Slug: 260-excombatientes-de-la-etrc-de-poliarpa-se-trasladan-ante-incumplimientos-del-gobierno
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/11/1508772878_161386_1508773024_noticia_normal.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Caracol Radio] 

###### [23 Nov 2017] 

[Un total 260 excombatientes que se encontraban ubicados en el Espacio de Reincorporación Policarpa, en Nariño, han decidido trasladarse de lugar debido a los incumplimientos del gobierno nacional en torno a la implementación, pues denuncian, entre otras cosas, que **ni el 5% de las construcciones necesarias para vivir en esa zona fueron entregadas.**]

[Así lo señala Ramiro Cortés, integrante de la FARC, quien asegura que la falta de condiciones los ha obligado a movilizarse, con el objetivo de buscar otras posibilidades para desarrollar proyectos productivos y para presionar al gobierno a que cumpla con los acuerdos de paz, en materia de reincorporación a la vida civil.]

[“No solamente no se implementó el acuerdo en materia de proyectos productivos, en Policarpa no hay clavada ni una estaca, no hay las construcciones necesarias y dignas para que vivan los excombatientes”, agrega que el **gobierno nacional y sus delegados se comprometieron con la comunidad a mejorar la vías, el acueducto, salud,** pero nada de eso se ha hecho, de tal manera que no existen las condiciones básicas para desarrollar algún proyecto productivo.]

[Además, lo poco que está construido desde febrero como el aula donde se realizan algunas capacitaciones y reuniones y otras tres casas están edificadas con madera de mala calidad. También **hay área de salud en la que cada vez que llueve se mete el agua,** explica el integrante de la FARC.]

**¿A dónde se irán los excombatientes?**

[Pese a los incumplimientos, los excombatientes señalan que “no hemos perdido la esperanza, estamos con el mismo entusiasmo. Movernos a un nuevo espacio es reactivar la esperanza para empezar otra etapa donde seguiremos trabajando en la reincorporación de los excombatientes a la vida civil”, dice Cortés.]

[Esta nueva etapa la vivirán en el  municipio de El Estrecho, ubicado entre el departamento de Patía en Nariño y el Cauca, muy cercano a la vía Panamaricana, donde ven mejores condiciones frente a la tierra y las carreteras para el desarrollo de proyectos productivos.]

<iframe id="audio_22332015" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_22332015_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
