Title: Red de comunicadores del Putumayo CONPAZ: Movilización campesina frena explotación petrolera
Date: 2015-02-06 21:25
Author: CtgAdm
Category: Comunidad, Movilización
Tags: Conpaz, Corpoamazonía, Perla Amazónica
Slug: red-de-comunicadores-del-putumayo-conpaz-movilizacion-campesina-frena-explotacion-petrolera
Status: published

###### **Foto:Red de comunicadores CONPAZ Putumayo** 

**Movilización campesina e indígena en Putumayo en defensa del medio ambiente contra la avanzada petrolera logra que la CAR Corpoamazonía suspenda proyecto sísmico a Gran Tierra Energy.**

El pasado 22 de diciembre denunciamos cómo con la venia de las instancias gubernamentales competentes, la petrolera Gran Tierra Energy pretendía profanar un santuario de flora, fauna y recursos hídricos gracias a la concesión del  Bloque Petrolero PUT10 2D.  Si bien esta pretensión colocaba en riesgo esta rica zona biodiversa, protegida por los PBOT y EOT de los municipios de Orito y Villagarzón respectivamente, ninguna autoridad ni instancia de control se habían pronunciado, a pesar de los múltiples acuerdos y tratados internacionales que en la materia tiene suscritos Colombia; lo que obligó a las comunidades a emprender acciones que frenaran lo que según ellos sería una amenaza directa contra la madre tierra y las pocas fuentes de agua pura que quedan en el Putumayo.

 En medio de la movilización, no cesaron los señalamientos, incluso acciones violentas en contra de estas comunidades que luchaban por salvaguardar para la humanidad los recursos de esta región.

Esta presión, posibilitó que el 20 de enero Corpoamazonía, le diera la razón a las comunidades campesinas e indígenas, declarando *“Determinantes Ambientales los suelos de protección del Plan Básico de Ordenamiento Territorial del municipio de Orito y el Esquema de Ordenamiento Territorial del municipio de Villagarzón, en el departamento del Putumayo, relacionados con la Reserva Forestal Protectora Alto Orito, y las Tierras Forestales de Protección”* **Resolución 0028 del 20 de enero de 2015 de Corpoamazonía.**

Además, dicha resolución reconoce que se trata de “una región biogeográfica de enorme importancia ecológica a nivel Internacional por su enorme diversidad en flora y fauna y por ser receptáculo de una gran reserva hidrológica, denominada Cordillera Real Oriental, que va desde el macizo colombiano hasta la depresión de Huancabamba, en el norte del Perú, ocupa más de nueve millones de hectáreas y circunda la gran cuenca amazónica…  En tal enorme variedad de paisajes vive la mayor riqueza de especies de los Andes del Norte, y quizá de toda Suramérica, en relación con su área. Aunque los inventarios son todavía incompletos. se conoce la existencia de más de 140 especies de anfibios (61 endémicas), 1.145 de aves (117 endémicas), más de 250 de mamíferos y 7 mil de plantas vasculares. En esta vertiente de los Andes se encuentran, además los mayores bloques de hábitat continuo para animales emblemáticos y amenazados, como el oso andino y la danta de montaña”.

Es por ello que esta resolución declara como uso prohibido en esta zona las actividades de minería, exploración y explotación de hidrocarburos, proyectos urbanísticos e industriales.

Esperamos que con esta determinación el gobierno colombiano avance en la adopción de todo tipo de medidas para poner freno a la avanzada mineroenergética que de manera acelerada va devastando selvas, montañas y llanuras, colocando en riesgo la pervivencia de sus especies, así como a las comunidades que las habitan.

Así mismo, aguardamos que se retiren las demandas interpuestas por la petrolera contra los manifestantes.
