Title: Violencia sexual cometida por el Ejercito sería considerado crimen de lesa humanidad
Date: 2015-04-29 14:04
Author: CtgAdm
Category: DDHH, Mujer, Nacional
Tags: cajar, colectivo de abogados, cpi, crimen, ECCHR, ejercito, fuero penal, jose alvear restrepo, lesa humanidad, mujer, sexual, sisma, violencia
Slug: violencia-sexual-cometida-por-el-ejercito-seria-considerado-crimen-de-lesa-humanidad
Status: published

###### Foto: Carmela María 

<iframe src="http://www.ivoox.com/player_ek_4424347_2_1.html?data=lZmflpiYe46ZmKiakpWJd6KkkZKSmaiRdI6ZmKiakpKJe6ShkZKSmaiRb7fd0NHS0MjNpYznxt3iw9GPp9DhxtnWxsaPtNDmjMrZjarOqdPXytncjdjJtoa3lIqupsaPp9Di1M6ah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Annelen Micus, del ECCHR] 

Organizaciones defensoras de los Derechos Humanos de las mujeres en Colombia presentaron este lunes 27 de abril un informe ante la Corte Penal Internacional sobre la violencia sexual en el marco del conflicto armado colombiano. En él se evidencia que, aunque todos los actores del conflicto han cometido crímenes sexuales contra las mujeres, aquellos cuyos victimarios son agentes estatales, como el Ejercito, son los que cuentan con niveles de impunidad cercanos al 100%.

El Centro Europeo por los Derechos Humanos y Constitucionales (ECCHR), Sisma Mujer y el Colectivo de Abogados José Alvear Restrepo (CAJAR), realizaron una investigación sobre este tema entre el año 2002 y el 2011 en Colombia, concluyendo que "durante el año 2014 cada tres días dos mujeres eran violadas dentro del contexto del conflicto armado colombiano. Sin embargo casi ningún caso de violencia sexual llevó a una condena, aún menos si el autor material era miembro de las Fuerzas Militares."

Las circunstancias y objetivos que rodean los casos estarían relacionados con operaciones militares y detenciones, y tendrían por objetivo controlar a la población y estigmatizar a las mujeres agredidas. Por cuanto los y las investigadoras aseguran que estos crímenes son premeditados, sistemáticos, y hacen parte de una estrategia de guerra del Ejercito.

Aunque la CPI ya ha determinado la existencia de crímenes sexuales por parte de otros actores armados, no ha reconocido la acción del Estado en este tipo de violencia. "Cuando se habla del Ejercito solo se habla de crímenes de guerra, pero no de crímenes sexuales, como crímenes de lesa humanidad", afirma Annelen Micus, del ECCHR.

Aún cuando son muchas las leyes y normas nacionales e internacionales que protegen a las mujeres ante este tipo de violencia, en el Informe se afirma que no se están implementando, y la impunidad es muy alta. "Casi en ningún caso se están realizando investigaciones adecuadas ni efectivas, y menos cuando se trata de actores estatales", asegura Micus.

Es por este motivo que las organizaciones solicitan a la CPI que intervenga en los crímenes de violencia sexual, si el Estado colombiano no quiere, o no puede hacerlo.

<div>

Esperan a demás que la Fiscalía de la Corte Penal Internacional tenga en cuenta estos estudios preliminares dentro del informe que presentará en los meses de noviembre/diciembre del 2015, ayudando a la efectiva judicialización de los crímenes cometidos, y la prevención de violencia sexual por parte del Ejercito.

</div>
