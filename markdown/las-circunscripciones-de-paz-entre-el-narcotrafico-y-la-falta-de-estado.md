Title: Preocupaciones en torno a las circunscripciones de paz
Date: 2017-06-07 14:49
Category: Nacional, Paz
Tags: acuerdo de paz, Circunscripciones de paz
Slug: las-circunscripciones-de-paz-entre-el-narcotrafico-y-la-falta-de-estado
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/05/pleno-del-congreso-e1495751460417.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Proteccion ONG] 

###### [07  Jun 2017] 

Tras las aprobaciones en la Comisión primera del Senado de las 16 circunscripciones de paz, son dos las preocupaciones que quedan frente a su legitimidad. La primera es que **exista intervención de los partidos políticos tradicionales en los candidatos** que se postulen a ellas y **la segunda es que estructuras del narcotráfico influyan en las elecciones posicionando candidatos**.

Las 16 circunscripciones de paz serán otorgadas en los territorios más afectados por la violencia y que, de acuerdo con la Senadora por la Alianza Verde Claudia López, nunca han tenido representación política en el Congreso. En esa medida, expresó que esta es la oportunidad para que tanto **campesinos como víctimas de la guerra puedan representar sus intereses en el Congreso.**

### **Los partidos políticos y su interés por más curules** 

En el debate sobre las jurisdicciones de paz, algunos partidos políticos expresaron su rechazo frente a la imposibilidad de poder lanzar candidatos para estas circunscripciones, sin embargo, la senadora manifestó que **“ni los partidos han representado a los campesinos, ni a los colombianos rurales**”, razón por la cual no tendría por qué acceder a estas elecciones.

Pese a que finalmente en la Comisión primera, todos los partidos aprobaron la proposición de no participar en estas elecciones con candidatos, no se podría descartar el riesgo de que los partidos acciones sus maquinarias políticas en estos sectores para postular a líderes sociales que estén a la disposición de sus intereses. Le puede interesar: ["Unidad de Restitución de Tierras le mete mico a la ley 1448"](https://archivo.contagioradio.com/unidad-de-restitucion-de-tierras-mete-un-mico-a-la-ley-1448/)

### **El Narcotráfico y su influencia en las circunscripciones de paz** 

Las 16 circunscripciones de paz están ubicadas en territorios que actualmente hacen parte del control y dominio de grupos ilegales del narcotráfico, posibilitando de esta forma, que **debido a la falta de acciones y medidas por parte del Estado, las curules puedan ser ocupadas por personas que estos grupos postulen**. Le puede interesar:["Dipaz corrobora control paramilitar en 7 territorios cercanos a zonas veredales"](https://archivo.contagioradio.com/dipaz-corrobora-presencia-de-paramilitares-en-zonas-veredales/)

Para Claudia López esto podría terminar en una **“instrumentalización” de los mecanismos democráticos y dando paso a una narcopolítica en el Congreso**.  Para intentar vigilar estos riegos la senadora generó una proposición que pretende que en los territorios en donde no hayan garantías de seguridad y control por parte del Estado se pueda suspender cualquier tipo de elecciones, incluyendo las circunscripciones de paz.

### **Circunscripciones de paz sin garantías en elecciones** 

Un informe de la Misión de Observación Electoral, señaló que además de estas problemáticas las zonas en donde estarán ubicadas las curules tienen los índices más altos de violencia con un total de **69 amenazas a líderes sociales y 59 asesinatos**. Además indicó que hay dificultades para establecer los orígenes políticos de los posibles candidatos y que se presentan inconvenientes en el número de personas ceduladas y complicaciones con la movilización de las personas para votar.

Estas circunscripciones reúnen el **6.5% del censo electoral del país, pese a que su área corresponde al 34% del territorio** y para las elecciones del año 2014 tan solo representaron el 11.5% de la votación. Le puede interesar: ["Estas son las recomendaciones de la Misión Electoral para la Reforma Política"](https://archivo.contagioradio.com/estas-son-las-recomendaciones-de-la-mision-electoral-para-la-reforma-politica/)

Otro de los inconvenientes que tendrán que afrontar las circunscripciones de paz serán los intereses de sectores como los empresariales que intentarían enviar sus candidatos para poder determinar el uso del territorio y la aprobación de proyectos en los deparatamentos.

[16 Circunscripciones de Paz](https://www.scribd.com/document/350675634/16-Circunscripciones-de-Paz?secret_password=t3XJ2BqNVdjWzInijOXj#from_embed "View 16 Circunscripciones de Paz on Scribd") by [Contagioradio](https://es.scribd.com/user/276652802/Contagioradio#from_embed "View Contagioradio's profile on Scribd") on Scribd

<iframe id="doc_41775" class="scribd_iframe_embed" src="https://www.scribd.com/embeds/350675634/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-l8i6guHEZJpXoJTMOJBm&amp;show_recommendations=true" width="600" height="800" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="0.6661732050333087"></iframe>  
<iframe id="audio_19140067" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_19140067_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

<div id="p61166151-m235-1-236" class="headings-container">

</div>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
