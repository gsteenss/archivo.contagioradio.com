Title: La represión y el terror armas de control social de Gobiernos Latinoamericanos
Date: 2017-06-30 16:54
Category: DDHH, El mundo
Tags: ESMAD, Represión
Slug: la-represion-y-el-terror-armas-de-control-social-de-gobiernos-latinoamericanos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/08/ESMAD-e1471644079239.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Prensa Rural] 

###### [30 Jun 2017] 

Finaliza en México el Encuentro “Enfoque psicosocial en América Latina: retos y perspectivas” un escenario que indagó cómo las políticas de **militarización y de represión que se han desarrollado en el continente, están afectando la salud mental de personas, familias, colectivos y comunidades.**

De acuerdo con Clemencia Correa, psicóloga de la Organización Aluna, en México, el uso del miedo y el terror como medida de control, utilizada por los diferentes gobiernos latinoaméricanos “está generando **aislamiento por el temor a organizarse para exigir sus derechos.** Nos hemos dado cuenta que este es un mecanismo común en todos los países y una estrategia para afectar a los movimientos sociales”.

Sin embargo, Correa asegura que las comunidades han encontrado otras formas de resistir a la represión por parte de los gobiernos, como lo son el arraigo al territorio**, el fortalecimiento de  la identidad de los pueblos originarios** y la reafirmación de la historia de las comunidades campesinas e indígenas. Le puede interesar: ["ESMAD impide labores de defensores de DD.HH y periodistas en paro de Buenaventura"](https://archivo.contagioradio.com/41556/)

En el encuentro se realizaron recomendaciones para que los gobiernos dejen de usar el terror y el miedo como forma de control social, “el llamamiento que hacemos desde este escenario es a no culpabilizar a las víctimas, defensores y periodistas de las problemáticas que están sucediendo, **sino a los estados, que agudizan la crisis de violación a los derechos humanos**”. Al evento asistieron países como Colombia, Bolivia, México y Chile.

<iframe id="audio_19566557" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_19566557_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU).
