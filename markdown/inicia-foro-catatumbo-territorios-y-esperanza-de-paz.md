Title: Inicia foro “Catatumbo: Territorios y Esperanza de Paz"
Date: 2016-09-16 15:23
Category: Nacional
Tags: Acuerdos de paz en Colombia, Catatumbo, construcción de paz
Slug: inicia-foro-catatumbo-territorios-y-esperanza-de-paz
Status: published

###### [ToFoto: Contagio Radio] 

###### [16 Sept 2016] 

El 17 de septiembre iniciará en Tibú Norte de Santander ** “Catatumbo: Territorios y Esperanza de Paz”**, un evento que pretende ser un escenario pedagógico de discusión y formación sobre **la construcción de paz desde las comunidades.** El foro finalizará **con el lanzamiento de la campaña de impulso regional “Paz sí es contigo”**.

El evento contará con tres espacios de participación: uno de formación, otro de diálogo y uno de debate, para generar herramientas que le permitan a las comunidades tener una apropiación y conocimiento de los acuerdos de paz firmados en la Habana.

Desde la inciativa se busca socializar con los asistentes la agenda de diálogos que se había acordado entre el ELN y el gobierno Nacional, para **instar a ambas partes a que instalen la mesa de negociación que propenda por establecer en el país, una paz completa**.

El evento, organizado por la Asociación Campesina del Catatumbo (ASCAMCAT) y por el Movimiento de Víctimas de Crímenes de Estado, espera que diferentes organizaciones y gremios confluyan en esta iniciativa y **logre aclarar dudas frente a lo acordado entre el gobierno y las FARC-EP  y la forma en la que aplicaría en los territorios.**

La región del Norte de Santander ha sido uno de los territorios más golpeados por el conflicto armado en Colombia, entre el año **1980 al 2013 se registraron 66 masacres y alrededor de 120.000 personas fueron desplazadas**, [en lo corrido de este año se han reportado 30 desapariciones y 3 asesinatos en la zona del Catatumbo](https://archivo.contagioradio.com/3-desapariciones-y-30-asesinatos-alarman-a-comunidades-del-catatumbo/).

En el año 2013 campesinos y campesinas de esta región realizaron el **Paro Nacional Agrario**, **en protesta al olvido estatal y a la crisis del agro** ,producida por las firmas de los Tratados de Libre Comercio, que culmino estableciendo la mesa de interlocusión con el gobierno, sin embargo pese a los compromisos adquiridos, los campesinos denuncian que los acuerdos solo [se han cumplido en un 30% sin establecer soluciones o propuestas de fondo a las problemáticas más importantes](https://archivo.contagioradio.com/gobierno-continua-incumpliendole-al-catatumbo/) como el acceso a la tierra o el reconocimiento de las zonas de reserva campesina.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
