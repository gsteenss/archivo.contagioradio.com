Title: Visita del CPI en Colombia respalda labor de la JEP
Date: 2020-01-29 19:32
Author: CtgAdm
Category: Nacional, Paz
Tags: acuerdo de paz, JEP, Vicimas, violencia sexual
Slug: visita-del-cpi-en-colombia-respalda-labor-de-la-jep
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/01/07112018-ICC-meet-Col-11.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: © ICC-CPI

<!-- /wp:paragraph -->

<!-- wp:html -->

<figure class="wp-block-audio">
<audio controls src="https://co.ivoox.com/es/sebastian-escobar-abogado-cajar-sobre-visita-cpi_md_47283418_wp_1.mp3">
</audio>
  

<figcaption>
Entrevista &gt; Sebastian Escobar | Abogado CAJAR

</figcaption>
</figure>
<!-- /wp:html -->

<!-- wp:paragraph -->

La reciente visita de la delegación de la Corte Penal Internacional (CPI) a Bogotá para evaluar temas como, procesos contra altos militares, casos de violencia sexual en el conflicto y crímenes contra líderes sociales, el mensaje es contundente ante la responsabilidad que tiene la Jurisdicción Especial para la Paz y el apoyo que necesita por parte del Gobierno para seguir ejecutando su trabajo.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### La CPI se reúne con el Estado

<!-- /wp:heading -->

<!-- wp:paragraph -->

La Corte estuvo del 19 al 23 de enero del 2020 en Bogotá, tiempo en el que visitó organismos como la Cancillería, la Fiscalía General y la Jurisdicción Especial para la Paz (JEP), la comisión este año estuvo encabezada por Fabricio Guariglia, director de la División de Enjuiciamiento de la Fiscalía.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Guariglia se encargó de preguntar a la cúpula del Bunker por los avances en temas específicos sobre *"promoción y expansión de los grupos paramilitares; desplazamiento forzoso, delitos sexuales y de género y los homicidios conocidos como falsos positivos*". Le puede interesar: (<https://www.icc-cpi.int/Pages/item.aspx?name=pr1510&ln=Spanish>)

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El abogado Sebastian Escobar integrante del Colectivo de Abogados José Albear Restrepo (Cajar) señaló que la Corte también se reunió con organizaciones de la sociedad civil, entre ellas las que trabajan temas de violencia sexual, "*mujeres de diferentes colectivos se encargaron de manifestar su preocupación sobre la justicia e impunidad en la investigación de estos crímenes*".

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Asimismo el abogado agregó que *"hasta el momento en la JEP no se registra ningún caso que se dedique puntualmente a la investigación de estos hechos de manera unificada*", y que solo se destacan investigaciones de años atrás que *"constatan la impunidad".* (Le puede interesar: <https://archivo.contagioradio.com/mas-de-2-mil-casos-de-falsos-positivos-bajo-la-lupa-de-la-corte-penal-internacional/>)

<!-- /wp:paragraph -->

<!-- wp:quote -->

> *"La Fiscalía reconoce como avances la existencia de investigaciones preliminares, pero para nosotros el fenómeno de que después de 10 o 15 años de investigar uno hechos todavía se tengan investigaciones preliminares sin significar un avance, **es un elemento que constata la impunidad**".*
>
> <cite> Sebastian Escobar - Cajar</cite>

<!-- /wp:quote -->

<!-- wp:heading {"level":3} -->

### Las conclusiones de la visita

<!-- /wp:heading -->

<!-- wp:paragraph -->

Según Escobar hasta ahora lo que ha manifestado la Fiscalía es que continúa analizando la información que le permita desarrollar alguna acción en el futuro, teniendo en cuenta un nuevo ingrediente que es la JEP. (Le puede interesar: <https://archivo.contagioradio.com/jep-deberia-definir-caso-mancuso-buscando-favorabilidad-para-las-victimas/>)

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

*"Hay un mensaje claro de la Fiscalía de la CPI que no es de esta visita, un mensaje que ha reiterado en los dos últimos años y es que **hay un apoyo a la JEP , que no es un cheque en  blanco, es una especie de respaldo condicionado a la espera que los avances de las investigaciones** allí se ajusten a los parámetros del Estatuto de Roma"*.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Dentro de los puntos que la Corte señaló en su comunicado sobre la evaluación, el primero hace referencia al **compromiso que deben asumir los órganos nacionales a cooperar con la JEP**, haciendo un especial énfasis en que deben *"proporcionar de manera expedita la información necesaria para que la JEP cumpla su mandato".*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por otro lado se refirieron a la importancia de*" ejecutar mecanismos rigurosos y efectivos para la implementación, verificación y monitoreo adecuado de las sanciones impuestas por la JEP*".

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por último señaló que la Fiscalía aún hace evaluación de los esfuerzos nacionales y que su trabajo aún esta en curso, junto con el desarrollo de indicadores,*"Creo que la Fiscalía tomó nota de las preocupaciones de la sociedad civil y lo que piensa hacer a futuro es diseñar herramientas para poder medir realmente el avance de los procesos judiciales a nivel nacional*".

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
