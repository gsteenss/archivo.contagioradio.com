Title: Video | Entrevista con Aureliano Carbonell, delegado de la mesa del ELN en Ecuador
Date: 2017-05-21 11:02
Category: Entrevistas, Paz
Tags: dialogos, dialogos de paz, ELN
Slug: entrevista-con-aureliano-carbonell-delegado-de-la-mesa-del-eln-en-quito
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/05/Aureliano-Carbonell-ELN.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Contagio Radio 

###### 21 May 2017 

**“Cada proceso de paz tiene sus particularidades y especifidades” con estas palabras abre la entrevista Aureliano Carbonell,** de formación sociólogo y uno de los hombres con más 25 años en las filas del ELN.

**Carbonell, integrante de la dirección nacional de esta guerrilla y delegado del ELN en la mesa de negociación**, habla con Contagio Radio, en Quito, Ecuador, al iniciarse el segundo ciclo de conversaciones entre esta guerrilla y el gobierno Nacional.

En este entrevista señala las diferencias de este proceso con otros que ha tenido esta guerrilla, el paramilitarismo, el papel de Juan Camilo Restrepo (delegado del gobierno en las conversaciones), la voluntad del ELN con la paz, el panorama ante las elecciones presidenciales, la situación del Chocó y de Camilo Torres.

“*Un elemento que caracteriza lo que estamos intentando es  la participación de la sociedad en el proceso*" resalta Aureliano Carbonell, como aspecto fundamental de esta negociación. **[Ver: Los retos del segundo ciclo de conversaciones del gobierno con ELN](https://archivo.contagioradio.com/inicia-segundo-ciclo-conversaciones-eln/)**

Esta conversación hace parte de **\#HablemosdePaz**, un espacio de Contagio Radio que tiene como objetivo construir desde diferentes voces  y desde diferentes perspectivas el sueño de paz que tiene el país.

###### Reciba toda la información de Contagio Radio en [[su correo]
