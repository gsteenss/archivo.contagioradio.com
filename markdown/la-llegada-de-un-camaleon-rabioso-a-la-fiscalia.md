Title: La llegada de un "camaleón rabioso" a la Fiscalía
Date: 2020-01-31 19:08
Author: CtgAdm
Category: Entrevistas, Política
Tags: Corte Suprema de Justicia, Francisco Barbosa, Iván Duque
Slug: la-llegada-de-un-camaleon-rabioso-a-la-fiscalia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/01/Fiscalía-de-Francisco-Barbosa.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:heading {"align":"right","level":6,"textColor":"cyan-bluish-gray"} -->

###### Foto: @FrBarbosaD {#foto-frbarbosad .has-cyan-bluish-gray-color .has-text-color .has-text-align-right}

<!-- /wp:heading -->

<!-- wp:paragraph -->

Este jueves se conoció que la Corte Suprema de Justicia decidió por unanimidad designar a Francisco Barbosa como nuevo Fiscal General de la Nación. La noticia se produce ocho meses después que Néstor Humberto Martínez renunciara a su cargo, en medio de cuestionamientos a las decisiones de la Corte y la necesidad urgente de tener una cabeza en la institución encargada de las investigaciones judiciales del país.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Recientemente el senador Roy Barreras advirtió sobre el riesgo de que la Corte Suprema no haya escogido los magistrados que la integrarán antes que se acabe el periodo de los actuales togados, otra decisión difícil sería la elección de Fiscal. Se precisaba tener un director de la Fiscalía en propiedad (no encargado) para investigar temas como el asesinato a líderes sociales y excombatientes.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/CorteSupremaJ/status/1222923649654624256","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/CorteSupremaJ/status/1222923649654624256

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:heading {"level":3} -->

### **"Me pregunto si es un camaleón rabioso"**

<!-- /wp:heading -->

<!-- wp:paragraph -->

Respecto a este nombramiento, **la líder política Ángela María Robledo** señaló que Barbosa era una persona con posiciones cambiantes y un negacionista. Su posición se explica en que cuando ella lo conoció, Barbosa era profesor de la Universidad Externado, "un gran defensor del proceso de paz" y de la justicia transicional en procesos de la guerra a la paz.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Pero luego, "en una voltereta inexplicable" que, tal vez, solo se entiende dada su amistad con el presidente Duque, se convirtió en enemigo de la Jurisdicción Especial para la Paz (JEP). Robledo recordó que como consejero presidencial para los DD.HH., incluso apoyó las objeciones a la JEP pese a que él mismo había intentado ser magistrado de la Jurisdicción pero no salió elegido.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En ese sentido, la Lideresa se **preguntó si Barbosa "es un camaleón rabioso".** Por otra parte, declaró que era un negacionista porque el año pasado, de cara a los informes de Naciones Unidas, Somos Defensores e Indepaz sobre el asesinato defensores de DD.HH., presentó cifras que no eran certeras para matizar el problema de ataques a estas personas.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por último, Robledo concluyó que en Colombia no ha habido Fiscales garantes, "son amigos del Gobierno de turno y por eso tenemos índices de impunidad del 94% y 98% para mujeres". (Le puede interesar:["Renuncia el fiscal general de la nación, Néstor Humberto Martínez"](https://archivo.contagioradio.com/renuncia-el-fiscal-general-de-la-nacion-nestor-humberto-martinez/))

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### **Víctimas del Palacio de Justicia: Lo que no cambiará con Barbosa en la Fiscalía**

<!-- /wp:heading -->

<!-- wp:paragraph -->

**Rene Guarín, hermano de Cristina Guarín (desaparecida en la toma y retoma del Palacio de Justicia)** señaló que los familiares del Palacio ven con preocupación que una persona que no es penalista y no tiene experiencia en litigio sea escogido como Fiscal. Guarín señaló que el perfil de Barbosa es más semejante al de un historiador.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En esa medida, **cabe la comparación con Néstor Humberto Martínez, que tampoco tenía experiencia litigando ni era penalista**. A su vez, Martínez Neira también salió del Palacio de Nariño (era Ministerio de la Presidencia) para la Fiscalía, aunque hubo más tiempo de diferencia entre la salida de un cargo y la llegada al otro, en contraste con Barbosa.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Sin embargo, un hecho que vale la pena resaltar es que Martínez venía cuestionado porque antes de ser Fiscal y servidor público, sirvió como asesor jurídico de empresas como Corficolombiana, que posteriormente se vería investigadas por la Fiscalía en el marco del caso de corrupción con Odebrecht.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Guarín también sostuvo que Martínez Neira dejó como fiscal encargado del proceso de Palacio a Jorge Hernán Díaz, que inició una campaña mediática para desestimar las desapariciones en el Palacio. Por lo tanto, señaló que la preocupación respecto a Francisco Barbosa es que el caso se quede congelado, y no se siga avanzando en la búsqueda de los familiares que faltan por aparecer.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### **Barbosa es un camaleón, pero como él, hay muchos**

<!-- /wp:heading -->

<!-- wp:paragraph -->

El **senador por el polo Wilson Arias** sostuvo que cualquier nombramiento de este Gobierno llevará el sello Duque: Será una persona allegada al corazón de la Casa de Nariño. Sobre Barbosa, el senador además dijo que era quien encabezaba el negacionismo del Gobierno Duque. (Le puede interesar: ["Postura de la Fiscalía sobre asesinato de líderes distorsiona la realidad de los territorios"](https://archivo.contagioradio.com/fiscalia-asesinato-lideres-distorsiona-realidad-territorios/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Pero desde otra perspectiva, respecto a los señalamientos del nuevo fiscal como un camaleón, Arias aseguró que **cómo Barbosa hay otras personas que funcionan de la misma manera**: El mismo presidente Duque que escribió artículos contra Uribe, Vargas Lleras y la independencia de Cambio Radical respecto a Duque o los congresistas que defienden una postura y luego, después de un tiempo o de una charla con el Gobierno, votan por la posición contraria.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### **"Necesitamos una Fiscalía que no haga competencia a la JEP"**

<!-- /wp:heading -->

<!-- wp:paragraph -->

**Diana Muriel, abogada de la Comisión Intereclesial de Justicia y Paz** coincidió con Guarín en la preocupación por un Fiscal que viene de tomar acciones en el Gobierno. Además, destacó que cuando participó como representante en las audiencias de la Comisión Interamericana de Derechos Humanos (CIDH), sus posturas fueron negacionistas de las amenazas contra líderes sociales.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Muriel dijo que no veía un cambio en la Fiscalía, y especialmente en atención a temas relacionados con la defensa de derechos humanos. En cuanto a la relación con la JEP, sostuvo que **era necesario que el ente de investigación "no le haga una competencia desleal"**, porque es vital la complementariedad entre ambas instituciones para evitar que las investigaciones tengan parálisis.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

La Abogada, sin embargo, concluyó que el nombramiento era muy reciente, por lo que habría que esperar algún tiempo para evaluar el trabajo de Barbosa. (Le puede interesar: ["Fiscalía demuestra su falta de compromiso con los desaparecidos del Palacio de Justicia"](https://archivo.contagioradio.com/fiscalia-desaparecidos-palacio-de-justicia/))

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### El nombramiento de Barbosa, ¿otro enroque de Duque?

<!-- /wp:heading -->

<!-- wp:paragraph -->

En el pasado, cuando algunos funcionarios se hicieron difíciles de defender en sus cargos como ocurrió con Guillermo de Defensa en el Ministerio de Defensa, Duque acudió al cambio de fichas de un lugar en el Estado a otro para mermar los ánimos. Fue así como Botero salió del cargo, el canciller Carlos Holmes Trujillo pasó al Min. Defensa y entró Claudia Blum al Ministerio de Relaciones Exteriores.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En el caso de Barbosa, tan pronto se comenzó a hablar del cambio, Duque decidió mover a Nancy Patricia Gutiérrez del Ministerio de Interior a la Consejería Presidencial para los Derechos Humanos (cargo que ocupaba el, ahora, Fiscal) y mover del Ministerio de Trabajo al Ministerio de Interior a Alicia Arango. (Le puede interesar:["Colombia necesita todo menos un fiscal de bolsillo: Comisión Colombiana de Juristas"](https://archivo.contagioradio.com/colombia-fiscal-de-bolsillo/))

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78980} /-->
