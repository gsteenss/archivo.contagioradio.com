Title: Panfleto de Águilas Negras amenaza a maestro de la sabiduría Wayúu
Date: 2019-02-04 15:39
Author: AdminContagio
Category: Comunidad, DDHH
Tags: Aguilas Negras, Amenazas a indígenas, La Guajira
Slug: panfleto-de-aguilas-negras-amenaza-maestro-de-la-sabiduria-wayuu
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/02/IMGP8402-770x400.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Nación Wayúu 

###### 03 Feb 2019 

El pasado 1 de febrero, fue  hallado un panfleto del grupo autodenominado Águilas Negras en la puerta de la casa del ex presidente de la ONIC y Maestro de la Sabiduría Wayúu, Armando Valbuena, la intimidación que va dirigida directamente hacia uno de los hijos del dirigente quien trabaja como docente en Manaure La Guajira también menciona al resto de su familia y se suma a la ola de intimidaciones que se han conocido a lo largo del territorio colombiano durante el primer mes del 2019.

Valbuena, actual secretario de la Comisión Étnica de la Onic hizo pública la amenaza contra él y su familia a través de su cuenta en Twitter compartiendo fotografías del panfleto donde son "declarados objetivo militar por entorpecer el desarrollo y ordenamiento territorial de Manaure”, el panfleto además advierte que él y su familia están ubicados y que su hijo tiene 72 horas para que abandone el municipio.

"Mi hijo ya salió del departamento porque no hay garantías concretas que permitan nuestra convivencia con la sociedad" afirma el dirigente quien explica que las amenazas surgen después de hacer frente a la expropiación de 42 lotes -  donde viven varias personas entre ellas Valbuena y su familia  - realizada por parte de la Alcaldía de Manaure que planea construir un malecón en aquel mismo lugar.

> Hago publica las amenazas de muerte contra mi y mi familia, las cuales dejaron en la puerta de la casa de mi familia en el Municipio de Manaure en el departamento de La Guajira. Cc [@luiskankui](https://twitter.com/luiskankui?ref_src=twsrc%5Etfw) [@ONIC\_Colombia](https://twitter.com/ONIC_Colombia?ref_src=twsrc%5Etfw) [@DefensoriaCol](https://twitter.com/DefensoriaCol?ref_src=twsrc%5Etfw) [@FelicianoValen](https://twitter.com/FelicianoValen?ref_src=twsrc%5Etfw) [@JUANCAELBROKY](https://twitter.com/JUANCAELBROKY?ref_src=twsrc%5Etfw) [@petrogustavo](https://twitter.com/petrogustavo?ref_src=twsrc%5Etfw) [pic.twitter.com/6v44RVUxpm](https://t.co/6v44RVUxpm)
>
> — ArmandoValbuena (@ArmandoWayuu) [31 de enero de 2019](https://twitter.com/ArmandoWayuu/status/1091117909634617346?ref_src=twsrc%5Etfw)

<p>
<script src="https://platform.twitter.com/widgets.js" async charset="utf-8"></script>
</p>
Ante la denuncia, la **Organización Indígena de Colombia, ONIC** ha rechazado la amenaza y ha pedido a la Defensoría del Pueblo y al Gobierno que realicen la debida investigan y protejan al líder y su familia; no es la primera vez que Armando Valbuena ha recibido amenazas, en 2016 también tuvo que enfrentar intimidaciones similares.

Tal como han señalado diversos analistas y el más reciente informe del **Instituto de Estudios Para el Desarrollo y la Paz (INDEPAZ)**, el nombre de las Águilas Negras es utilizado como una razón social que no obedece a la estructura de un grupo armado y que es usado para amenazar de muerte a organizaciones y líderes sociales e infundir terror “con fines políticos de ataque, con lenguajes de ultraderecha".

<iframe id="audio_32239921" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_32239921_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
