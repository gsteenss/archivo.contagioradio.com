Title: Asesinan a lideresa Gloria Ocampo en Putumayo
Date: 2020-01-08 11:18
Author: CtgAdm
Category: Líderes sociales, Nacional
Tags: Asesinatos, lideres sociales, Puerto Guzmán
Slug: asesinan-a-lideresa-gloria-ocampo-en-putumayo-2
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/01/gloria-ocampo.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo Familiar] 

Según las primeras informaciones, **Gloria Ocampo** de 37 años de edad, quien se desempañaba como presidenta y ahora secretaria de la Junta de Acción Comunal, JAC, en la vereda la Estrella del Municipio de Puerto Guzman en el departamento del Putumayo, fue asesinada este 7 de Enero en horas de la tarde.

En cuanto al asesinato de la lideresa, solamente se ha podido establecer, hasta el momento que dos hombres que se movilizaban en una motocicleta llegaron hasta la vivienda rural y dispararon contra Gloria y contra Heladio Moreno, quien se encontraba en la vivienda para el momento de los hechos. [Atentan contra la vida de la lideresa social Milena Quiroz](https://archivo.contagioradio.com/atentan-contra-vida-lideresa-social-milena-quiroz/)

**Gloria Ocampo** también se caracterizó por ser lideresa en la promoción de la sustitución de los cultivos de uso ilícito en el departamento y concretamente en su comunidad, a pesar de los incumplimientos por parte del gobierno nacional en cuanto a la aplicación de los planes integrales de sustitución, que además del subsidio entregado a las familias también prevé la asistencia técnica y el mejoramiento de las vías terciarias para facilitar la comercialización de los nuevos productos. [Líderesa cultural Lucy Villarreal fue asesinada en Llorente-Tumaco](https://archivo.contagioradio.com/lideresa-cultural-lucy-villarreal-fue-asesinada-en-llorente-tumaco/)

Noticia en desarrollo...
