Title: Esperamos que en Mutatá encontremos la paz: excombatientes de Ituango
Date: 2020-07-14 21:19
Author: CtgAdm
Category: Actualidad, DDHH
Tags: Ituango, Mutatá
Slug: esperamos-que-en-mutata-encontremos-la-paz-excombatientes-de-ituango
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/07/Ec1kLu4XYAU-GXy.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","fontSize":"small"} -->

Foto: @Carlozada\_FARC

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Este 15 de julio, cerca de 100 personas y al menos 45 familias personas en proceso de reincorporación que habitaban en el espacio territorial Román Ruíz en Ituango, Antioquia, se desplazarán hasta la vereda La Fortuna, del municipio de Mutatá, huyendo de la violencia que se ha registrado en la zona donde han sido asesinado 11 firmantes de la paz desde el 2016. Ante este éxodo la comunidad expresa la nostalgia que significa dejar su tierra y comenzar de cero en un nuevo espacio donde esperan, existan las garantías de una vida digna.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

**Gustavo Lopez Alcaraz, delegado de FARC ante el Consejo Territorial de Reincorporación** señala que son cerca de 18 a 20 horas de viaje con una caravana de 15 camiones, 6 buses, además de esquemas de protección y diferentes controles realizados por la Fuerza Pública que podrían hacer más largo viaje. [(Por amenazas y riesgos excombatientes de Ituango se desplazan a Mutatá)](https://archivo.contagioradio.com/por-amenazas-y-riesgos-excombatienes-de-ituango-se-desplazan-a-mutata/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

López Alcarez señala que hay dos razones principales para el desplazamiento, la primera es la falta de tierras y la inestabilidad ecónomica, "nadie trabaja con moral en cuanto a las labores agropecuarias, es como estar en un racho que no es de uno, es como prestado", explica. La segunda razón e**s la ausencia de garantías para la vida de los excombatientes y sus familias que han sido amenazadas.**

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

"Son posiciones encontradas, sabemos que es una salida forzada por la inseguridad, hay personas que tienen su arraigo y son naturales de la región, por esa parte hay nostalgia y preocupación", expresa el excombatiente - "pero también hay un deseo de estabilizarse en un lugar y las ganas de ver qué posibilidades hay". [(Le puede interesar: Más de 450 familias desplazadas en Ituango por enfrentamientos armados)](https://archivo.contagioradio.com/mas-de-450-familias-desplazadas-en-ituango-por-enfrentamientos-armados/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

[Emilio Archila,](https://twitter.com/EmilioJArchila) consejero Presidencial para la Estabilización y la Consolidación explicó que por petición del colectivo, **siete excombatientes y sus familias permanecerán en Santa Lucía para realizar seguimiento a dos de los proyectos productivos que se desarrollan en de Ituango**, al respecto Gustavo explica que existe mucha preocupación con relación a quienes permanecerán en el territorio pues son al menos 25 personas que prefieren permanecer en su tierra pues consideran que su familia y su futuro ya está arraigado en el municipio, pese a las amenazas que persisten.

<!-- /wp:paragraph -->

<!-- wp:quote -->

> La esperanza es mucha, esperamos que allá sintamos que se construya la paz, pensamos que es posible, allí no se ha visto esa violencia como se ve por aquí, eso es lo que espera esta población
>
> <cite>**Gustavo Lopez Alcaraz, delegado de FARC**</cite>

<!-- /wp:quote -->

<!-- wp:heading {"level":3} -->

### La partida a Mutatá no es el primer desplazamiento forzado

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Aunque al principio se esperaba salir cerca de las 4:00 de la tarde y llegar al casco urbano lo más temprano posible, el integrante del ETCR confirmó que cinco camiones llegaron a las 6:00 pm al espacio territorial y se dispuso a cargar los objetos de la población, otros 11 restantes llegaron cerca de las 8:00 pm al municipio y se espera lleguen a las 11:00 pm a la vereda, mientras se espera lleguen otros en las próximas horas. [(Le puede interesar: Asesinan a excombatiente de las FARC en el ETCR de Santa Lucía, Ituango)](https://archivo.contagioradio.com/asesinan-a-excombatiente-de-las-farc-en-el-etcr-de-santa-lucia-ituango/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Según el abogado de DD.HH. Camilo Fagua, este desplazamiento se da como consecuencia de la pérdida de soberanía de los territorios por parte del Estado. Antioquia es el tercer departamento con mayor número de excombatientes asesinados e Ituango es el municipio con mayor número de asesinatos con 11 registros, "además de ser una población desprotegida, **es un municipio dejado a merced de la violencia por la acción y omisión del Estado**, es imposible creer que al rededor de un ETCR se encuentren seis estructuras al margen de la le ante la mirada del Gobierno"

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Según el abogado, existen antecedentes de desplazamientos además del ocurrido en Ituango, el primero en la región de Gallo, Córdoba, donde al menos 60 excombatientes se desplazaron y hoy están asentados en Mutatá también, está el caso de la población de Vidrí, poblado del municipio de Vigía del Fuerte, Antioquia, que se vio forzada a desplazarse después de que el Gobierno, mediante el Decreto 982 del 7 de junio de 2018, suprimiera 2 de los 26 sitios destinados en aquel entonces para desarrollar la reincorporación.

<!-- /wp:paragraph -->
