Title: Los caficultores están pensando en un paro nacional
Date: 2019-04-26 15:15
Author: CtgAdm
Category: Comunidad, Economía
Tags: Café, Cafeteros, crisis, marcha, Paro Nacional
Slug: caficultores-pensando-paro-nacional
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/Cafeteros-y-sus-razones-para-ir-a-paro.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: @FernandezMJP] 

El pasado miércoles 24 de abril, **cafeteros y paneleros del país adelantaron una jornada de movilización en Armenia, Quindío**; allí, cerca de **5 mil caficultores** analizaron la situación de crisis por la que atraviesa el sector, presentando las medidas que debería tomar el Gobierno para solucionarla y estableciendo que de no encontrar soluciones, **Dignidad Cafetera realizará un llamado a paro nacional en los próximos días**.

Como lo explicó el integrante de esa organización Alonso Osorio, **a la ciudad de Armenia llegaron solo del Cauca unos 700 cafetero**s; y en conjunto con los cerca de 5 mil otros asistentes a la marcha, plantearon unos puntos claros al Gobierno para sacar al sector de la crisis. **El primer punto es reconocer que las familias están produciendo Café a pérdidas,** pues la Federación Nacional de Cafeteros dice que vale 782 mil pesos producir una carga del grano (el presidente Duque afirma que el costo es de 715 mil pesos), mientras que **el mercado la tasa en 650 mil pesos**.

Eso significa que l**os productores estarían perdiendo más de 100 mil pesos por carga de café**, mientras que se requieren ganancias cercanas a esa cifra para hacer rentable su cultivo. Los caficultores también piden que **se incluya en el Plan Nacional de Desarrollo un 4% del presupuesto para el Agro Colombiano**, porque actualmente la cifra es cercana al 0,8%; y que se incluya la idea de un crédito agropecuario para pequeños y medianos productores. Por último, reclaman que **se regule el precio de los fertilizantes, de forma que sea posible controlar el costo de producción**.

### **"Multinacionales hambrean a los cafeteros"**

Osorio aclaró que los cafeteros entienden que **el precio del Café no lo pone el Gobierno sino los emporios empresariales** porque de "20 mil millones de dólares que vale el negocio, 180 mil se lo quedan las multinacionales y 2 mil millones se reparten entre cerca de 25 millones de familias" que cultivan el grano; de allí que en la primera pancarta en la marcha se leía "Multinacionales hambrean a los cafeteros". Sin embargo, el caficultor indicó que **el Gobierno sí podría subsidiar la producción del grano**, y de hecho, ofreció cerca de 30 mil pesos por carga; mientras los manifestantes pidieron entre 70 y 120 mil pesos más, para poder obtener ganancias de su producción.

**El Café fue el principal producto de exportación en la primera mitad del siglo XX**, de su producción viven directamente cerca de dos millones y medio de personas, y cerca de diez lo hacen de forma indirecta; como lo expuso Osorio, **solo en el Cauca hay "40 millones de árboles de Café mientras hay 200 millones de matas de Coca**", y la sustitución de cultivos de uso ilícito no será posible mientras no sea rentable.

En ese sentido, en Armenia se decidió que se crearía una comisión para presentar estos puntos al Presidente, y esperar su respuesta sin tener que recurrir a movilizaciones; pero **en caso de que sus demandas no fueran escuchadas, Dignidad Cafetera definiría una fecha para el paro nacional.** (Le puede interesar: ["Miércoles 24 de abril: cafeteros marchan en Armenia por crisis del sector"](https://archivo.contagioradio.com/cafeteros-marchan-armenia-crisis/))

<iframe id="audio_34997558" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_34997558_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
