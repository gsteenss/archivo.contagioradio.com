Title: En 2025 Colombia tendrá un inventario de sus tierras gracias al catastro multipropósito
Date: 2019-07-19 01:37
Author: CtgAdm
Category: Nacional, Paz
Tags: Catastro, Propiedad, reforma rural, tierra
Slug: inventario-tierras-catastro-multiproposito
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/Catastro-Multipropósito.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Pagina10.com] 

Recientemente se hizo pública la firma de un crédito de Colombia con el **Banco Mundial por 100 millones de dólares** para adelantar el catastro multipropósito. La medida es una de las puntos en deuda de la implementación del punto uno del Acuerdo de Paz, y **servirá para establecer un inventario de la tierra que hay en el país, estableciendo su vocación y sus dueños. ** (Le puede interesar: ["En Colombia una comunidad campesina ha luchado más de 40 años por el derecho a laierra"](https://archivo.contagioradio.com/el-garzal-una-comunidad-campesina-ha-luchado-mas-de-40-anos-por-el-derecho-a-la-tierra/))

La abogada **Isabel Velásquez, integrante de la Comisión Intereclesial de Justicia y Paz,** explicó que según lo establecido en el Acuerdo, el término para adelantar el Catastro a partir del barrido predial era de siete años. En ese sentido, dijo que la Superintendencia de Notariado y Registro, la Agencia Nacional de Tierras y el Instituto Agustín Codazzi adelantaron un plan piloto con el que lograron un diagnóstico para iniciar el proceso.

Ahora, con los recursos que girará el Banco Mundial, las proyecciones del **Gobierno contempla adelantar el Catastro en el 60% del territorio nacional para 2022 y en todo el país para 2025**. Tener la información que permite un catastro multipropósito significa, según expresó la Abogada**, tener certeza sobre a quién pertenece la tierra para fines jurídicos y económicos;** en beneficio de la pequeña y mediana propiedad rural.

### **Que no haya catastro multipropósito ha permitido el despojo territorial y la informalidad** 

Entre las dificultades que podría encontrar la realización del catastro, y partiendo del plan piloto que ya fue adelantado, Velásquez destacó el cruce de información entre el sistema registral y catastral del inmueble, puesto que "muchas veces no coincide, y eso ha generado alteraciones en derechos adquiridos y en las transferencias de la propiedad". Adicionalmente, las instituciones han denunciado la falta de garantías para desarrollar su trabajo por problemas de orden público en los territorios.

La experta destacó la importancia de realizar un catastro cuyos fines no se limiten al recaudo como una forma de incidir en el ordenamiento social de la propiedad, razón por la que acentuó la importancia de su ejecución al afirmar que **el no tener un catastro multipropósito "ha permitido el despojo territorial y la informalidad de la propiedad".** (Le puede interesar: ["Lucha entre cifras y hechos, los resultados de la restitución de tierras"](https://archivo.contagioradio.com/asi-trabaja-unidad-restitucion-tierras/))

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>  
<iframe id="audio_38663168" frameborder="0" allowfullscreen scrolling="no" height="200" style="border:1px solid #EEE; box-sizing:border-box; width:100%;" src="https://co.ivoox.com/es/player_ej_38663168_4_1.html?c1=ff6600"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
