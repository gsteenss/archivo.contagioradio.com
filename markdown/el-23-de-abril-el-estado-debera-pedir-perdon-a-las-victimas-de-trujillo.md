Title: El 23 de abril el Estado deberá pedir perdón a las víctimas de Trujillo
Date: 2016-04-07 14:31
Category: DDHH, Nacional
Tags: AFAVIT, masacre Trujillo, víctimas Trujillo
Slug: el-23-de-abril-el-estado-debera-pedir-perdon-a-las-victimas-de-trujillo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/04/Víctimas-de-Trujillo.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: El Diario ] 

###### [7 Abril ]

Tras 22 años de las denuncias por las graves violaciones a los derechos humanos conocidas como '[[los hechos violentos de Trujillo](http://sinolvido.justiciaypazcolombia.com/2012/05/la-masacre-de-trujillo.html)]', ocurridos entre 1989 y 1992, se suscribió entre el Estado colombiano y los representantes de las víctimas un 'Acuerdo de Solución Amistosa' ante la CIDH, en el que se establece que un **alto funcionario del gobierno deberá llevar a cabo un acto público de reconocimiento de responsabilidad**.

Este "avance histórico para la verdad, la justicia, la reparación y las garantías de no repetición", como lo ha denominado la 'Asociación de Familiares de Víctimas de Trujillo' AFAVIT, hace que se reconozcan **42 nuevas víctimas de ejecuciones extrajudiciales y desapariciones forzadas**, que podrían llegar a ser más, dependiendo de los avances investigativos con los que debe cumplir el Estado colombiano, según indica el Acuerdo.

Ludivia Vanegas, integrante de AFAVIT, afirma que de ese cruento periodo de violencia en Trujillo, dejó **342 víctimas de asesinatos, desapariciones y desplazamientos forzados**, y agrega que el Acuerdo significa que "aún se pueden lograr el reconocimiento de todas las víctimas" y de la culpabilidad del Estado, al que le exigen hacerse presente en el municipio para que **"de la cara" a los familiares, frente a "tantos años de impunidad"**.

Años en los que han sido **revictimizados con constantes amenazas proferidas por grupos paramilitares**, a través de grafittis y daños en sus instalaciones, según asegura Ludivia, quien insiste en que las víctimas exigen "que haya verdad y que no quede todo en la impunidad", así como ser reparados, con medidas como que los altos mandos, tanto militares como paramilitares "paguen todo lo que hicieron, porque una cosa es decir y otra es tener a nuestros seres queridos, los que pudimos encontrar, con sus rostros destrozados por las torturas".

"Pedimos la reparación para todas nuestras viejitas que se están muriendo esperando que siquiera vengan a hacer un reconocimiento de lo que ha sucedido" afirma Ludivia, y agrega que como víctimas seguirán **luchando, denunciando las injusticias y resistiendo "para que los hechos no vuelvan a suceder"**.

El Acuerdo también establece que el compromiso frente a los mecanismos de garantías de no repetición, incluye el que el Estado fortalezca las iniciativas de memoria creadas por las víctimas, como el Parque Monumento, y **enfrente los grupos paramilitares que controlan social, económica, política y militarmente el municipio**; con el fin de asegurar la libertad plena de las víctimas y garantizar que reestablecezcan sus proyectos de vida.

El primer acto público en el marco de este Acuerdo se llevará a cabo entre el **23 y 24 de abril en el municipio de Trujillo**,** **y contará con la participación de organizaciones de víctimas, diversas organizaciones sociales y defensoras de derechos humanos, y altos funcionarios del Estado como Juan Fernando Cristo, actual Ministro del Interior.

<iframe src="http://co.ivoox.com/es/player_ej_11083268_2_1.html?data=kpadmpiWepmhhpywj5aYaZS1lp6ah5yncZOhhpywj5WRaZi3jpWah5ynca3pxc7jy8aPmsLixszO1YqWh4zdz9nSydfFstXZjMnSjaaqhbe9tYqfpZKJe6ShpNTb1sbLrdCfs8bRy9SPcYarpJKh&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)]

 

##### 
