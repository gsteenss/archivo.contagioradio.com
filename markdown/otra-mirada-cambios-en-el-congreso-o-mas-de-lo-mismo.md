Title: Otra Mirada: ¿cambios en el Congreso o más de lo mismo?
Date: 2020-07-22 09:43
Author: AdminContagio
Category: Otra Mirada, Otra Mirada, Programas
Tags: Arturo Char, Congreso de la República, Iván Marulanda, Senado
Slug: otra-mirada-cambios-en-el-congreso-o-mas-de-lo-mismo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/07/Congreso-de-la-República.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: Congreso

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Como forma de evitar los contagios, el Congreso del Estado se vio obligado a cambiar de modalidad y mantener sus sesiones virtuales; debido a esto, la legislatura tuvo que instalar de manera virtual el tercer periodo legislativo del Congreso de la República. (Le puede interesar: [ONIC denuncia intento de masacre contra comunidad indígena Awá](https://archivo.contagioradio.com/onic-denuncia-intento-de-masacre-contra-comunidad-indigena-awa/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

De esta manera, Arturo Char con 76 votos a favor, es el nuevo presidente del Senado. Char es investigado por la compra de votos y se ha ausentado en 140 de las 210 sesiones del Congreso. Además su familia, según la revista Forbes es una de las diez más ricas del país, dueña de las casi 371 sedes del almacén de cadena “Olímpica” y de otras 11 empresas como Olímpica Stereo.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Con este perfil, el papel del presidente Char es cuestionado, por esta razón, para analizar cómo se ven las cosas desde el mismo senado los participantes de este panel fueron Germán Robayo, subcoordinador de Misión Observatorio Electoral (MOE), Iván Marulanda, senador de la República del Partido Alianza Verde, Ángela María Robledo, representante de la Cámara Colombia Humana y Sandra Ramírez, segunda vicepresidenta del Senado del Partido FARC.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Los invitados explicaron por qué debe ser un motivo de preocupación para todos los colombianos que Arturo Char haya sido elegido para este nuevo cargo y si está en riesgo la democracia con los cambios que se están dando.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Igualmente, explican qué se puede esperar de quienes también fueron elegidos para desempeñar diferentes funciones en el Senado y qué consecuencias podrían tener esos nombramientos en llegar a acuerdos con las diferentes facciones que hay en el Congreso.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Iván Marulanda, senador de la República del Partido Alianza Verde, que obtuvo 20 votos, también comenta cuáles pueden ser las salidas y cómo se puede seguir luchando desde los partidos que, en sus palabras “representan al pueblo”. 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

También invitaron a los colombianos a analizar por quienes están votando para gobernar el país que les pertenece y no dejarse engañar con proyectos que aunque suenan llamativos en este momento de crisis económica, son engañosos.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

(Si desea escuchar el programa del 17 de julio: [¿Qué tanta independencia hay 2210 años después?](https://www.facebook.com/contagioradio/videos/900877967075430))

<!-- /wp:paragraph -->

<!-- wp:core-embed/facebook {"url":"https://www.facebook.com/contagioradio/videos/291521582185652","type":"video","providerNameSlug":"facebook","className":""} -->

<figure class="wp-block-embed-facebook wp-block-embed is-type-video is-provider-facebook">
<div class="wp-block-embed__wrapper">

https://www.facebook.com/contagioradio/videos/291521582185652

</div>

</figure>
<!-- /wp:core-embed/facebook -->

<!-- wp:block {"ref":78955} /-->
