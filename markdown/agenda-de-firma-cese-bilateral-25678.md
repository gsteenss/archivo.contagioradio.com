Title: Así será el acto protocolario de firma de Cese Bilateral en la Habana
Date: 2016-06-23 11:20
Category: Paz
Tags: cese bilateral del fuego, Conversaciones de paz con las FARC, FARC, Juan Manuel Santos
Slug: agenda-de-firma-cese-bilateral-25678
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/05/proceso-de-paz.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: elpais] 

###### [23 Jun 2016]

A partir de las 11:30 se tenía previsto el inicio del acto protocolario para la firma del acuerdo sobre Cese Bilateral entre el gobierno de Colombia y la guerrilla de las FARC. El acto se realizará en el salón de protocolo "el Laguito" en el centro de convenciones que ha albergado las conversaciones de paz desde hace 3 años.

**Agenda de hoy en la Habana:**  
La ceremonia comenzará sobre las 11:30 a.m. de Colombia, 12:30 m de Cuba.  
1. Ingreso de delegaciones de paz (Gobierno y Farc).  
2. Ingreso invitados especiales  
3. Ingreso presidentes invitados (Chile, Venezuela, El Salvador, República Dominicana) y secretario general de la ONU.  
4. Ingreso, en su orden: Raúl Castro, presidente de Cuba; Rodrigo Londoño Echeverry, cabecilla de las Farc; y Juan Manuel Santos, presidente de Colombia.  
5. Lectura comunicado conjunto por parte de garantes Cuba y Noruega.  
6. Firma del acuerdo entre Humberto de la Calle e Iván Márquez.  
7. Palabras Raúl Castro, Presidente de Cuba.  
8. Palabras Ban Ki Moon Secretario General de la ONU.  
9. Palabras de Timoleón Jiménez, jefe máximo de las FARC.  
10. Palabras presidente Juan Manuel Santos, presidente de Colombia.  
11. Ruedas de prensa delegaciones del Gobierno y Farc.
