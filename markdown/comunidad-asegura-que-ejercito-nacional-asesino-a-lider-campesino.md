Title: Comunidad asegura que Ejército Nacional asesinó a líder campesino
Date: 2016-09-12 11:59
Category: DDHH, Nacional
Tags: Agresiones fuerza pública, Agresiones por parte del ejército, Sur de Bolivar
Slug: comunidad-asegura-que-ejercito-nacional-asesino-a-lider-campesino
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/09/Militares-Sur-de-Bolívar.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Vanguardia ] 

###### [12 Sept 2016] 

Este domingo sobre las 6 de la tarde, fue asesinado el campesino Álvaro Rincón quien era miembro de la junta de acción comunal de la vereda de Patio Bonito, del municipio de San Pablo, Sur de Bolívar. Según información de los pobladores el **asesinato fue perpetrado por integrantes del Ejército Nacional**, quienes intentaron levantar el cuerpo sin cumplir con los protocolos.

De acuerdo con Gonzalo Montoya, Secretario de la 'Federación Agrominera del Sur de Bolívar', **entre las 5:30 y las 6 de la tarde fue asesinado el campesino Álvaro Rincón**, en la jurisdicción de San Pablo, en el punto conocido como Cuatro Vientos. Lo que no ha podido confirmarse es si el asesinato lo perpetró el Ejército cómo aseveran los pobladores, porque las condiciones climáticas no han permitido una verificación en la zona.

Las familias campesinas permanecen en el lugar del asesinato para impedir que los militares levanten el cuerpo sin cumplir con los protocolos, porque **temen que luego pueda reportarse como un caso de ejecución extrajudicial**, teniendo en cuenta que en la zona hay presencia de las guerrillas del ELN y las FARC-EP, y de grupos paramilitares.

Para los pobladores el hecho perturba la tensa calma que se viene viviendo en la región por cuenta de los diálogos de paz, por lo que planean que este lunes parta una comisión de verificación integrada por la Defensoría del Pueblo, la Personería Municipal y delegados del Comité Internacional de la Cruz Roja, para **establecer las condiciones en las que se dio el asesinato** y atender a la comunidad.

Vea también: [[Fiscalía, ESMAD, Ejército y Policía arremeten contra comunidad La Garita en Bolívar](https://archivo.contagioradio.com/fiscalia-esmad-ejercito-y-policia-arremeten-contra-comunidad-la-garita-en-bolivar/)]

[[Denuncian a FFMM por violar DDHH en Córdoba, Bolívar y Norte de Santander](https://archivo.contagioradio.com/denuncian-a-ffmm-por-violar-ddhh-en-cordoba-bolivar-y-norte-de-santander/)]

<iframe id="audio_12874746" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_12874746_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)]] 
