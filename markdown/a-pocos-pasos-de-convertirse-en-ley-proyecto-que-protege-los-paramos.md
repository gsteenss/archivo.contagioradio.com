Title: A pocos pasos de convertirse en ley proyecto que protege los páramos
Date: 2018-06-29 13:05
Category: Ambiente, Nacional
Tags: Ambiente, Ley de Páramos, páramos
Slug: a-pocos-pasos-de-convertirse-en-ley-proyecto-que-protege-los-paramos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/06/moor-2770791_960_720.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:  Zasilvape ] 

###### [29 Jun 2018] 

La Ley de páramos, que fue aprobada durante los jornadas extraordinarias del Congreso, logrará aumentar el marco legal que existe en Colombia para garantizar la preservación de los 37 páramos que existen en Colombia.

De acuerdo con Alejandra Aguilar, abogada investigadora de la organización Ambiente y Sociedad, esta ley resuelve algunos de los vacíos jurídicos respecto a la protección de estos ecosistemas que son de vital importancia porque son fabricas de agua y sumideros de carbono. Pero que son codiciados por su riqueza geológica: Hay gran cantidad de minerales (oro, petróleo, cobalto entre otros).

> ###  49% de los páramos del mundo, se encuentran en Colombia 

Colombia ya contaba con la Ley 99 de 1993, que resaltaba la importancia vital de estos ecosistemas, luego, el Plan Nacional de Desarrollo del 2011 limitó las actividades agrícolas o de extracción minero-energética en los páramos y posteriormente la sentencia C-035 del 2016 prohibe cualquier actividad minera en los páramos.

Sin embargo, según Aguilar no existía un régimen de transición para quienes desde antes realizaban actividades económicas en los paramos de minería artesanal o siembra. Ese vacío es el que viene a llenar esta nueva ley. De hecho, Aguilar sostiene que en Colombia "había una aproximado de 376 títulos mineros al interior de 26 de los 37 páramos que hay en el territorio nacional" en 2016.

Por esta razón, la Ley de Páramos, que pasó a sanción presidencial, establece unos regímenes de transición de sustitución y de conversión de actividades extractivas a otras como los pagos por servicios ambientales, e incorpora líneas de crédito y asistencia técnica para que se transforme la economía en estos ecosistemas.

Aguilar cree que  los titulos mineros que están vigentes se van a mantener por unos años, pero la transición se debe hacer, así como la delimitación de los páramos que ya viene realizando el instituto Humboldt y el Ministerio de Ambiente y Desarrollo Sostenible para proteger de daños ..... a estos importantes (...). (Le puede interesar: ["Delimitación de páramos debe ser concertada por las comunidades: Ambientalistas"](https://archivo.contagioradio.com/delimitacion-de-paramos-debe-ser-concertada-con-las-comunidades/))

<iframe id="audio_26802521" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_26802521_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)] 
