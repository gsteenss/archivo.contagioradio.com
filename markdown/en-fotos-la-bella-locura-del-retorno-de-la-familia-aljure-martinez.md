Title: En fotos: "La bella locura" del retorno de la familia Aljure Mártinez
Date: 2017-04-12 18:40
Category: Otra Mirada
Tags: familia Aljure, mapiripan, Wiliam Aljure
Slug: en-fotos-la-bella-locura-del-retorno-de-la-familia-aljure-martinez
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/04/retorno-familia-aljure-3.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

**William y Dumar Aljure**, nietos del legendario guerrillero liberal que en los años 50 enfrentó al ejército conservador en el departamento del Meta, tomaron la decisión de retornar a su tierra con la fuerza y compromiso que heredaron de su abuelo.

[El pasado 3 de abril, los hermanos Aljure Mártinez junto a sus familias y acompañantes nacionales e internacionales iniciaron el retorno a la **finca Santa Ana en el municipio de Mapiripán**, lugar del que fueron desplazados en 2012, Contagio Radio acompañó el regreso de la familia.]

La fecha seleccionada para el retorno fue significativa para la familia porque en los mismo días del año 1968 su abuelo Dumar Aljure, fue asesinado por el ejercito nacional luego de haber firmado un pacto de paz con el gobierno de ese entonces.

\
