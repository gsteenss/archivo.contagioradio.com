Title: ¿Qué es delito político?
Date: 2018-02-27 11:13
Category: Expreso Libertad
Tags: delito político, prisioneros políticos
Slug: delito-politico-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/12/Presos-politicos-2-e1519746085716.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Archivo 

En en este programa de **Expreso Libertad**, Fidel Rondón, encargado de la pedagogía sobre Ley de amnistía y ex prisionero político de la FARC, y Jhon León sub director de la Corporación Solidaridad Jurídica, nos cuentan qué es del delito político y qué significa ser prisionero político en Colombia.

<iframe id="audio_24107256" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_24107256_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Encuentre más información sobre Prisioneros políticos en[Expreso libertad](https://archivo.contagioradio.com/programas/expreso-libertad/)y escúchelo los martes a partir de las 11 a.m. en [Contagio Radio](https://archivo.contagioradio.com/alaire.html)
