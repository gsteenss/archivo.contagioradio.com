Title: Comunidades afectadas por Hidroituango siguen sin respuesta del Estado
Date: 2019-02-20 15:45
Author: AdminContagio
Category: Ambiente, Comunidad
Tags: EPM, Hidroituango, Río Cauca
Slug: comunidades-afectadas-hidroituango-siguen-sin-respuesta-del-estado
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/02/Captura-de-pantalla-25-770x400.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: [[@**PizarroMariaJo**] 

###### [20 Feb 2019] 

[Ante la emergencia ambiental, social y económica provocada por Hidroituango, congresistas de la Bancada Alternativa realizaron este lunes una audiencia en el municipio de Tarazá, Antioquia para escuchar las preocupaciones de las comunidades afectadas, quienes denuncian la ausencia de las autoridades para suministrar ayudas.]

En la audiencia, en que participaron 700 habitantes de la zona aguas abajo de la represa, las comunidades resaltaron que el **cauce del río Cauca a disminuido nuevamente a un nivel bajo su caudal ecológico en algunos sectores**, lo cual sigue ocasionado la mortandad de peces, la debilitación del comercio y afectaciones para las actividades de pescadores, barequeros y agricultores.

La representante a la Cámara María José Pizarro explica que esto se debe a la decisión de Empresas Públicas de Medellín (EPM) de cerrar nuevamente las compuerta 2 de la casa de máquinas de Hidroituango después de que concluyera la visita del Presidente Iván Duque el pasado 11 de febrero. Esto se suma a **la ausencia del Estado y EPM para brindar apoyo a las comunidades para mitigar los efectos de la crisis económica y ambiental.**

### **Debate sobre el futuro de Hidroituango** 

En la audiencia, según lo indicó la Representante, se plantearon dos visiones para el futuro de Hidroituango, por un lado, algunos sectores de las comunidades, apoyadas por el representante [León Fredy Muñoz, piden la desmantelamiento del megaproyecto y la renuncia de los directivos de EPM por el mal manejo de la crisis.  Otros surgieren el desembalse de la represa mientras que se terminan los arreglos al túnel de desviación. Sin embargo, entre todos acordaron en que las comunidades y el país necesita recibir información actualizada, por parte de las autoridades, sobre los riesgos y la viabilidad del proyecto.]

Adicionalmente, piden se aclare quienes son los responsables de la serie de crisis que ha generado la construcción del megaproyecto. La representante Pizarro indicó que los directivos de EPM además de los contratistas que tenían a su cargo la construcción son algunos de los relacionados con Hidroituango que deberían responder. Por tal razón, Pizarro criticó la ausencia del gerente de EPM, Jorge Londoño, y el Alcalde de Medellín, Federico Gutiérrez en la audiencia, lo cual demuestra el desinterés de la empresa y de la Alcaldía por escuchar a las comunidades.

"No han parecido en todo este tiempo y no fueron a una audiencia pública donde estaba toda la comunidad. Allí pudieron dar las respuestas," sostuvo la representante.

### **Los compromisos del Gobierno**

[Como resultado de esta audiencia, el Ministerio de Ambiente se comprometió a convocar un Consejo Nacional de Política Económica y Social (CONPES) para planear el desarrollo económico y social de la región. Sin embargo, Pizarro sostiene que este Consejo tiene que incluir la participación de líderes de organizaciones sociales, organizaciones de derechos humanos y de gremios para diseñar un CONPES que genere los impactos que esperan las comunidades.]

Además, la Bancada Alternativa del Congreso programó un debate de control político en que esperan contar con la participación de los directivos de EPM, la Alcaldía de Medellín y otras autoridades, quienes tendrán que responder por la crisis de Hidroituango.

<iframe id="audio_32712833" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_32712833_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
