Title: Gobierno del FMLN en el Salvador denuncia planes desestabilizadores
Date: 2015-08-21 06:04
Category: El mundo, Política
Tags: democracia, desestabilización gobierno, El Salvador, Movilización, pandillas, paramilitares
Slug: gobierno-del-fmln-en-el-salvador-denuncia-planes-desestabilizadores
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/08/Cerén-Ortiz.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Elsalvadornoticias 

###### [20 Ago 2015]

Medardo Gonzáles Secretario General del Frente Farabundo Martínez, **denuncia que el partido opositor Arena, busca desestabilizar el gobierno** de el actual mandatario Salvador Sánchez Cerén, mediante acciones dirigidas por *pandillas* que funcionan como grupos paramilitares.

Estos grupos que denuncia Gonzáles, a**fectan en distintas formas a la sociedad ya que atemorizan e intimidan a los pobladores del país centroamericano**, no solamente con asesinatos, sino también con extorsiones y vínculos control del narcotráfico.

Gonzales recordó que desde el partido opositor Arena se generó un movimiento en pro de la renuncia  del Presidente y del Vicepresidente Oscar Ortíz, quienes **han ganado en las tres últimas elecciones** y en su gobierno han implementado cambios profundos en la sociedad enfocados hacia la mejora en la calidad de vida de las comunidades en El Salvador.
