Title: Festival 'Ojo en la tinta' rinde homenaje a las poetizas olvidadas
Date: 2016-11-04 15:03
Category: Cultura, eventos
Slug: festival-homenaje-emilia-ayarza
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/Emilia-Ayarza.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: La Raíz Invertida 

##### 4 Nov 2016 

Con la figura de **Emilia Ayarza de Herrera**, poetiza colombiana destacada durante la primera mitad del siglo XX, el **8vo Festival de Poesía y Narrativa Ojo en la tinta** rendirá homenaje a las voces excluidas y olvidadas en la mayoria de la antologías que se han publicado hasta la fecha en el pais.

Ayarza de Herrera, fue además doctora en filosofía y letras de la Universidad de los Andes y **colaboradora de la Revista cultural Mito**. Los últimos diez años de su existencia los vivió en México, después de viajar por varios paises del mundo, allí conseguiría un premio por su cuento '**Juan mediocre se suena la nariz**' de 1962.

La remembranza de la escritora, nacida en Bogotá en 1919 y fallecida en Los Ángeles en 1966, estará a cargo de **Juan Manuel Roca y Alfredo Ayarza**, con una **lectura en voz alta de su poema 'Testamento', **por diez poetizas invitadas: María Tabares, Camila Charry Noriega, Laura Castillo, Luisa Fernanda Trujillo, María Paz Guerrero, Margarita Mejía, Laura Merchán, Diana Carolina Daza, Johanna Vanegas y María Cristina Castro.

El homenaje tendrá lugar este viernes **4 de noviembre a partir de las 6:30 p.m.** en el **Auditorio Aurelio Arturo de la Biblioteca Nacional de Bogotá** (Cl. 24 \#5-60) con entrada libre y gratuita. El Festival Ojo en la tinta, organizado por la revista latinoamericana de poesía 'La Raíz Invertida', terminará el próximo 19 de noviembre. Encuentre [Aquí](http://www.laraizinvertida.com/detalle.php?Id=2037)toda la programación del evento.

**DIÁLOGO ENTRE EL POETA Y YO**

Poeta, escucha:  
“Habla que tu voz dilata el aire.”  
Poeta, ¿qué es el grito de la vida?:  
“Es el reflejo de todos los silencios de la muerte.”  
Poeta, ¿qué es el sol?  
“Una claraboya dorada por donde vemos a Dios.”  
Poeta, ¿qué es la risa?  
“Es un puente sobre las aguas del llanto construido.”  
¿Y el corazón?  
“Es un niño que siempre juega a sufrir.”  
Poeta, ¿qué es la soledad?  
“La soledad, amiga mía, es la más dulce compañía.”  
Y tú poeta, ¿qué eres?  
“Yo soy la soledad”.

Emilia Ayarza
