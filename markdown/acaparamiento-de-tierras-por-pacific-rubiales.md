Title: Pacific Rubiales ha acaparado 48.600 Hectáreas de tierra en el Meta
Date: 2015-12-14 17:02
Category: Economía, Entrevistas
Tags: Acaparamiento de tierras en Meta, Acumulacion de tierras, Pacific rubiales, Puerto Gaitán, Río Paila acumula tierras en Altillanura, Zidres
Slug: acaparamiento-de-tierras-por-pacific-rubiales
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/12/Acumulación-de-tierras-en-Puerto-Gaitán.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: semillas.org 

<iframe src="http://www.ivoox.com/player_ek_9722166_2_1.html?data=mpyflJaaeo6ZmKiakpWJd6KkkZKSmaiRdI6ZmKiakpKJe6ShkZKSmaiRlNDmjNHcjdLJstDnjJmljdLNsIzcxsjhh6iXaaKl08rO1ZDFp8LkwtfOxsbXb9Hj05C9w8jNqsrXjLeah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Alberto Castilla, Polo Democrático] 

###### [14 Dic 2015 ]

[Alberto Castilla, senador del Polo Democrático Alternativo, denuncia grave caso de **acumulación irregular de baldíos en Puerto Gaitán, Meta por parte de la petrolera Pacific Rubiales**, previo a la aprobación de la Ley de [‘Zonas de Interés de Desarrollo Rural, Económico y Social’ ZIDRES](https://archivo.contagioradio.com/?s=ZIDRES), que permitiría transferencia de propiedad y acumulación de predios de la reforma agraria a empresarios.]

[La estrategia implementada por Pacific para acumular tierras ha sido la **constitución de fiducias** a las que ha entregado predios para administrar bajo la figura de fideicomisos, **burlando la legislación colombiana** puesto que por ley sólo es posible obtener 1 Unidad Agrícola Familiar en materia de  reforma agraria.]

[En total han sido 3 las filiales constituidas por la petrolera, entre ellas **‘Major International Oil SA’ a la que le han otorgado 24 predios baldíos, un total de 48.600 has**, en terrenos que a su vez han mostrado falsa tradición para argumentar que no son baldíos, un fenómeno que estaría también presentándose en Monterrey, Casanare.]

[Esta **evasión de responsabilidad en la acumulación de tierras por parte de empresas**, de acuerdo con el Senador, pretende ser legalizada a través de la aprobación de las ZIDRES en regiones como la Altillanura en las que otras empresas como Río Paila estarían acaparando un total de 47 mil has.]
