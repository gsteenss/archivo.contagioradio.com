Title: La UP: desde hace 30 años dando el sí por la paz
Date: 2016-09-14 17:05
Category: Paz, Política
Tags: Conversaciones de paz con las FARC, Diálogos de La Habana, Unión Patriótica
Slug: la-up-desde-hace-30-anos-dando-el-si-por-la-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/09/union-patriotica.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: smpmanizales] 

###### [13 Sept 2016 ] 

Se ha abandonado la educación pública y privatizado la salud, para financiar[ ]{.apple-converted-space}el aparato de guerra. Este momento histórico implica que se asuma el **compromiso de construir un país distinto**, en el que se puedan resolver los problemas sociales que han quedado represados en la agenda nacional. Justamente esta responsabilidad es la que ha asumido la Unión Patriótica para liderar la campaña 'Siempre La Paz'.

De la mano de todas las iglesias, sectores sociales y centrales unitarias, la Unión Patriótica ha realizado **actividades de pedagogía de paz en distintos barrios de la capital** colombiana para explicarle a los ciudadanos lo que implican los [[acuerdos pactados](https://archivo.contagioradio.com/acuerdo-final-de-paz-entre-gobierno-y-farc/)] entre el gobierno y la guerrilla de las FARC-EP y la posibilidad de que se solucionen las [[demandas de trabajo, salud y educación](https://archivo.contagioradio.com/4-retos-a-superar-para-lograr-una-paz-estable-y-duradera/)], una vez se haya superado el conflicto armado

Hace 30 años emergió la UP **con la idea de la paz y de una solución negociada al conflicto**; después de toda la tragedia que representó esta lucha, sus militantes hemos retomado la bandera, afirma Jaime Caicedo, miembro del Partido Comunista Colombiano y vocero de la Unión Patriótica, quien agrega que[ [es muy importante que los ciudadanos voten por el si](https://archivo.contagioradio.com/el-respaldo-a-los-acuerdos-en-el-plebiscito-es-el-primer-paso-hacia-la-paz/)] en el plebiscito por la paz, porque el no implicaría graves retrocesos.

De acuerdo con Caicedo, para el 26 de septiembre se tiene planeada una **gran movilización que culminará en la Plaza de Bolívar** y de la que participarán diversos sectores sociales que respaldan el momento histórico que está viviendo Colombia, en el que será posible que hayan cambios significativos para mejorar las condiciones de vida de la población, a través de mayor inversión social y la destinación de menos [[recursos para la guerra](https://archivo.contagioradio.com/durante-los-ultimos-52-anos-colombia-ha-invertido-179-000-millones-de-dolares-en-la-guerra/)].

###### [Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)]] 
