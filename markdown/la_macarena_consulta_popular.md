Title: Aprueban consulta popular que defina futuro de La Macarena, Meta
Date: 2017-09-21 16:00
Category: Ambiente, Entrevistas
Tags: consulta popular, La Macarena
Slug: la_macarena_consulta_popular
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/04/La-Macarena1-e1461181026531.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Econoticias] 

###### [21 Sep 2017] 

[La tala indiscriminada de bosques, la falta de inversión en ciencia y tecnología en la zona, licencias ambientales para extracción de hidrocarburos, el cultivo de palma africana y  ausencia de política de legalización de tierras, entre otras situaciones tienen en riesgo el Área de Manejo Especial de la Macarena, donde se encuentran riquezas ambientales como Caño Cristales. Ante dicha preocupación, los habitantes del municipio de La Macarena han logrado que en los próximos días se deba ser definir la fecha para que se lleve a cabo la consulta popular buscando proteger su territorio.]

[Así lo decidió este lunes el Tribunal Administrativo del Meta que avaló constitucionalmente la pregunta por medio de la cual los pobladores decidirán si permiten o no que se realicen actividades petroleras y mineras en ese municipio.]

[**“¿Está usted de acuerdo con que se ejecuten actividades de exploración sísmica, perforación, explotación y producción de hidrocarburos y explotación minera dentro de la jurisdicción del Municipio de la Macarena Meta?”,** dice la pregunta que podrán responder las 8.032 personas que se encuentran habilitadas para votar.]

### **¿Qué está en riesgo?** 

[De acuerdo con el concejal Pedro Nel Rocha, el Área de Manejo Especial de la Macarena abarca el **39,8% del departamento del Meta y el 8.8% del Guaviare, equivalente al 3.4% del territorio colombiano,** el cual constituye una zona estratégica desde el punto de vista biogeográfico, ya que cumple un papel fundamental de conexión entre los ecosistemas Andino, Orinocense y Amazónico, con una riqueza natural invaluable en la que confluyen cuatro Parques Nacionales que conectan desde el Sumapaz hasta Chiribiquete, en límites con el departamento del Caquetá y es la única franja de bosque por donde aún fluyen centenares de especies de fauna y flora endémica, que contribuyen al equilibrio ambiental de Colombia y la zona norte del continente suramericano.]

### **Denuncia ante la ONU** 

[**En la sesión del Comité Económico Social y Cultural de la ONU**, Victor Hugo Moreno, campesino que representa más de 200 organizaciones sociales del Área de Manejo Especial de la Macarena, AMEM, denunció esta situación detallando cada una de las problemáticas que aquejan a ese territorio.]

[Mediante un informe de 163 páginas, las organizaciones ambientales de la zona buscan llamar la atención de la comunidad internacional frente a las múltiples amenazas que afectan esa área, entre ellas la construcción de carreteras, el avance del cultivo de palma africana en el eje del rio Ariari, el licenciamiento indiscriminado para la explotación de hidrocarburos, el incremento en los cultivos de coca; el monopolio del turismo, el licenciamiento minero-energético en ecosistemas estratégicos y corredores de conectividad, así como el aprovechamiento forestal ilegal. **Una situación que provoca la tala de una hectárea de bosque cada tres minutos.**]

[El concejal Pedro Nel Rocha, explica que **hay 5 actos administrativos expedidos para la exploración petrolera. Uno de ellos muy cerca al río de siete colores** (Caño Cristales). Empresas como Hupecol y Emerald Energy son las que buscarían realizar actividades de hidrocarburos para lo cual ya hay 30 bloques de concesión destinados.]

Los pobladores exigen que se detengan dichas actividades, y en cambio el Estado colombiano acompañe a los campesinos en sus acciones en defensa del territorio, impulsado que ellas y ellos se conviertan en guardianes de La Macarena.  Asimismo solicitan que se tomen medidas razonables a tiempo para evitar que se profundicen los conflictos sociales y ambientales.

<iframe id="audio_21022083" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_21022083_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
