Title: Guatemala: Autoridades imputaron cargos a Otto Pérez Molina
Date: 2015-09-03 13:11
Category: El mundo, Judicial
Tags: Corrupción en Guatemala, Marchas en Guatemala, Movilización en Guatemala, Otto Pérez Molina, Renuncia Otto Pérez Molina
Slug: guatemala-se-imputaron-cargos-al-presidente-otto-perez-molina
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/Guatemala.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: www.prensalibre.com 

<iframe src="http://www.ivoox.com/player_ek_7822595_2_1.html?data=mJ2flJqdeY6ZmKialZWJd6KpmZKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRi9bV1craw9HFaZS1jMbi1tTWrcXVxcrgjc7RtNbowtfc0JDHpdPb0NiYw5DTuNXjjNWSpZiJfJrmxt%2BYz5KJe6ShpNTb1sbLrdCfs8bRy9SPcYarpJKh&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Claudia Samayoa, defensora DDHH en Guatemala.] 

###### [3 Sept 2015]

[Tras la carta de **renuncia formal del ahora ex presidente guatemalteco Otto Pérez Molina**, durante la audiencia de primera declaración del mandatario por el caso de corrupción, el juez Miguel Ángel Gálves dio lectura a los cargos que se le imputan, entre ellos asociación para delinquir,  defraudación aduanera y lavado de dinero.]

[Tras la finalización de la audiencia se espera que Pérez Molina sea conducido a una cárcel preventiva. Mientras tanto, las movilizaciones en la Plaza de La Constitución continuarán en los próximos días y seguirán después de las elecciones del domingo, donde se contempla una reducida participación causada por el inconformismo general.]

[En la audiencia,  el Ministerio Público de Guatemala aseguró, con base en grabaciones presentadas, que Otto Pérez Molina, conocía sobre la red de defraudación aduanera "La Línea", por la cual el mandatario se debió poner a disposición de la justicia.  ]

[Ante la imposición del actual vicepresidente Alejandro Maldonado como sucesor de Pérez Molina, se lidera una **iniciativa para que el Congreso apruebe una terna que sea propuesta por la sociedad civil**, pues además de ser de la línea del procesado, Maldonado “es un dinosaurio de la contrainsurgencia que sabe poco de política y representa la ultraderecha”, señala Claudia Samayoa, defensora de derechos humanos en Guatemala.]

[Al interior de las Fuerzas Militares, también golpeadas por la mafia, las posiciones a favor o en contra de Otto Pérez están divididas. Sin embargo, el acuerdo general es que **no habrá golpe de Estado, pues respetan la movilización de las bases sociales guatemaltecas**.]

[La policía guatemalteca no ha empredido acciones represivas en contra de la movilización social, razón por la cual los manifestantes han implementado acciones de solidaridad  con los agentes del orden, entendiento que hacen parte de un sector del pueblo también afectado por las medidas del gobierno. ]

[Samayoa, afirma que el sector empresarial privado ha decidido guardar silencio ante las movilizaciones, para ver de qué lado decanta la crisis y cómo puede establecer estrategias que le beneficie durante el próximo gobierno. ]
