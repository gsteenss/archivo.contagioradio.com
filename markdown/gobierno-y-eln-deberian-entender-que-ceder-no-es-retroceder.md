Title: Gobierno y ELN deberían entender que “Ceder no es retroceder”: Carlos Medina
Date: 2017-01-13 11:33
Category: Nacional, Paz
Tags: Juan Camilo Restrepo, Mesa de conversación de Quito, Odín Sánchez
Slug: gobierno-y-eln-deberian-entender-que-ceder-no-es-retroceder
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/10/eln-y-gobierno.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [13 Enero 2017] 

En el proceso de paz entre el gobierno y el ELN, por lo menos públicamente, ha dado a conocer que el **punto neurálgico tiene que ver con la liberación de Odín Sánchez por parte del ELN y el indulto de Juan Carlos Cuellar y Eduardo Martínez** por parte del gobierno. Tanto la guerrilla del ELN como el gobierno han reiterado sus posturas, lo cual es interpretado como una mala señal en cuanto al esperado inicio de la fase pública de conversaciones.

La reiteración por parte del presidente Juan Manuel Santos, de la liberación de **Odin Sánchez  es inamovible y condiciona el inicio de la fase pública** ha sido interpretada como una imposición por parte del gobierno. En algunas entrevistas publicadas en diversos medios de información, lo delegados del ELN han expresado que no se podrían dejar imponer una condición que no está pactada en el acuerdo inicial y que se estableció como un punto a desarrollar dentro de la agenda. Le puede interesar: ["Sin mesa con el ELN no habrá paz completa: Eduardo Celis"](https://archivo.contagioradio.com/sin-mesa-con-el-eln-no-habra-paz-completa-eduardo-celis/)

 A su vez los integrantes del **ELN afirman que es necesario el indulto de Juan Carlos Cuellar y Eduardo Martínez para que se integren a su delegación de paz**, sin embargo, la respuesta del gobierno ha sido negativa, argumentando que los indultos solamente podrían darse a personas que estén imputadas por el delito de rebelión, es decir, que no tengan condenas por crímenes que podrían no ser amnistiables o indultables en el caso de un acuerdo sobre esa materia.

Este panorama no ha sido ajeno a las organizaciones sociales y personas cercanas al proceso que han dado cuenta de varias propuestas. Por un lado **Monseñor Darío Monsalve propuso una comisión internacional para acuerdos humanitarios** que permitan avanzar en las condiciones de parte y parte, y por otro lado intelectuales colombianos han dicho que se libere a Odín Sánchez y que se declare la cesación de la acción penal contra integrantes del ELN para que se sumen a la delegación de paz. Le puede interesar: ["Propuestas de la sociedad civil claves para destrabar mesa ELN-Gobierno"](https://archivo.contagioradio.com/propuestas-de-la-sociedad-civil-son-claves-para-destrabar-mesa-eln-gobierno/)

**Carlos Medina Gallego**, investigador e integrante del Centro de Pensamiento de la Universidad Nacional, ha afirmado que tanto el ELN como el gobierno deberían entender que **“Ceder no es retroceder”** sino que podría ser avanzar hacia los objetivos planteados por las partes y sobre todo hacia la consecución de lo que han llamado en Colombia “Una paz completa”.

###### Reciba toda la información de Contagio Radio en [[su correo]
