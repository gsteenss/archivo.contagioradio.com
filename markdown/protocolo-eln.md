Title: Violar protocolo establecido con ELN sería un irrespeto a la comunidad internacional
Date: 2019-01-23 12:46
Author: AdminContagio
Category: Nacional, Paz
Tags: DIH, ELN, Noruega, paz, proceso de paz
Slug: protocolo-eln
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/01/Diseño-sin-título-2-770x400.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [22 Ene 2019] 

Luego de que se perpetrara el atentado contra la Escuela de Cadetes de Policía General Santander, se abrieron dos debates referentes al Derecho Internacional Humanitario así como el derecho internacional: La primera de las controversias tiene que ver con la legitimidad del ataque realizado por el Ejército de Liberación Nacional (ELN); la segunda, con el pedido que realizó el Gobierno de Colombia a Cuba para que entregue a los jefes negociadores de esa guerrilla que permanecen en La Habana.

Sobre la primera situación, el **abogado y director de la Comisión Colombiana de Juristas (CCJ), Gustavo Gallón**, aclaró que **el Derecho Internacional Humanitario (DIH) no es un conjunto de normas permisivas o que autoricen algo, por el contrario, "es un derecho que contiene prohibiciones"**. El jurista recordó que el DIH fue establecido para señalar a las partes en conflicto, ante la imposibilidad de parar la guerra, que hay ciertas cosas inadmisibles en la confrontación como rematar a los heridos, o actuar contra la población civil.

Teniendo esto en cuenta, la razón expuesta por el ELN en su comunicado sobre el atentado en la que justificaba su acción como legítima en el marco del DIH, obedece a una lectura errónea de este reglamento según la cual "lo que no está prohibido, está permitido por el DIH". (Le puede interesar: ["Buscamos la paz con todas sus complejidades, u optamos por la guerra con todo su dolor"](https://archivo.contagioradio.com/paz-o-guerra-con-dolor/))

Adicionalmente, Gallón sostuvo que si se aceptara a la General Santander como un objetivo militar legítimo por la formación que allí reciben personas que participarán en el futuro en la guerra, tendría que aceptarse la lógica empleada por miembros de la fuerza pública en San José de Apartadó, cuando mataron a hijos de personas que ellos consideraban que apoyaban a la guerrilla, previendo que se convirtieran en guerrilleros en el futuro.

### **"Gobierno no puede tomar una actitud de niño de colegio"** 

La segunda discusión tiene que ver con el pedido que hizo el Gobierno colombiano a La Habana para que entregue a los delegados del ELN en la mesa; **situación que violaría el protocolo establecido en caso de rompimiento de las negociaciones y avalado por Brasil, Ecuador, Chile, Cuba, Venezuela y Noruega**, en calidad de países garantes. (Le puede interesar: ["Colombia no puede omitir el DIH ni violar los protocolos de la mesa con el ELN"](https://archivo.contagioradio.com/colombia-no-puede-omitir-el-d-i-h-ni-violar-los-protocolos-de-la-mesa-con-eln/))

> [\#AlAire](https://twitter.com/hashtag/AlAire?src=hash&ref_src=twsrc%5Etfw) El protocolo establecido por ELN, Gobierno y Estados acompañantes en caso de ruptura de las negociaciones de paz. <https://t.co/mBizQIbcmt> [pic.twitter.com/T6iK2vxBJY](https://t.co/T6iK2vxBJY)
>
> — Contagio Radio (@Contagioradio1) [22 de enero de 2019](https://twitter.com/Contagioradio1/status/1087713207983226880?ref_src=twsrc%5Etfw)

<p>
<script src="https://platform.twitter.com/widgets.js" async charset="utf-8"></script>
</p>
Sobre esta situación, Gallón recordó que el propio uso de la bandera blanca, en el que un combatiente pide hablar con el contrario obliga al contrario a respetar su vida, escuchar lo que dice y permitirle volver sano y salvo unilateralmente a su espacio de batalla. Si esta situación es respetada, lo es mucho más otros pactos que se realicen para garantizar el desarrollo de negociaciones de paz entre partes enfrentadas.

Contrario a ello, el abogado consideró que **el presidente Duque está asumiendo una actitud infantil, "como niño de colegio que le echa la culpa al Gobierno antecesor",** al sostener que su gobierno no se siente vinculado con los protocolos establecidos. En ese sentido, Gallón recordó que los acuerdos que hace un Estado con otro Estado, en este caso los países garantes, no se hacen a titulo personal, por lo que resultan insultantes e irrespetuosas las afirmaciones del presidente Duque sobre la legalidad y legitimidad del protocolo.

Y añadió que **de no respetarse dicho protocolo: ninguna parte volvería a confiar en un garante, así como ningún país se ofrecería para serlo**; al tiempo que se aceptaría que la política de paz es una directriz de Gobierno, situación que Gallón calificó como "peligrosa", pues se podría argüir que el Acuerdo de Paz de 2016 también fue una política de Gobierno y no del Estado colombiano. (Le puede interesar: ["Colombia debe exigir que ELN y Gobierno se vuelvan a sentar a dialogar: Iván Cepeda"](https://archivo.contagioradio.com/sociedad-colombiana-debe-exigir-que-eln-y-gobierno-se-vuelvan-a-sentar-en-la-mesa-ivan-cepeda/))

Aunque el director de la CCJ considera poco probable que Cuba acepte el pedido del Gobierno colombiano, puesto que no puede traicionar su propia condición de garante, **en caso de que esto ocurriera "sería gravísimo para el derecho internacional, y para el desarrollo de futuros proceso de paz en Colombia".** (Le puede interesar: ["Mujeres víctimas piden no cerrar la puerta del diálogo con el ELN"](https://archivo.contagioradio.com/queremos-la-paz-lideresas-comunitarias-piden-al-gobierno-no-cerrar-puerta-al-dialogo-con-eln/))

### **Noruega ratificó su compromiso como garante del proceso** 

Este 22 de enero, el Real Ministerio de Asuntos Exteriores de **Noruega a través de un comunicado reafirmó su compromiso con la paz de Colombia**, evidenciado en su rol como país garante del proceso de paz con las FARC, al igual que con el ELN; e insistió en que el compromiso con su rol está vigente y por lo tanto, respetará todos los documentos firmados en medio del proceso, incluyendo el protocolo previamente establecido.

[![Comunicado Noruega](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/01/WhatsApp-Image-2019-01-22-at-12.35.45-PM-1-e1548197603414-463x800.jpeg){.alignnone .wp-image-60326 width="548" height="947"}](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/01/WhatsApp-Image-2019-01-22-at-12.35.45-PM-1.jpeg) [![WhatsApp Image 2019-01-22 at 12.35.45 PM (2)](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/01/WhatsApp-Image-2019-01-22-at-12.35.45-PM-2-e1548197787925-577x800.jpeg){.alignnone .size-medium .wp-image-60329 width="577" height="800"}](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/01/WhatsApp-Image-2019-01-22-at-12.35.45-PM-2-e1548197787925.jpeg)

<iframe id="audio_31690878" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_31690878_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
