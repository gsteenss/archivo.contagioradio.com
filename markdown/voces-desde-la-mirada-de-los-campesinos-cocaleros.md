Title: ¿Qué resultados arrojó la primera encuesta a campesinos cultivadores del PNIS?
Date: 2019-09-07 12:15
Author: CtgAdm
Category: Comunidad, eventos
Tags: campesinos, coca, Plan de Sustitución de Cultivos de Uso ilícito, PNIS
Slug: voces-desde-la-mirada-de-los-campesinos-cocaleros
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/09/Sustitucion-de-Cultivos.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/09/Unknown.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/09/Cultivadores.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/09/Coca-Bolivia.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:Universidad de los Andes] 

El cultivo de coca es una necesidad, no una opción. Esto es lo que diría cualquier campesino si se le preguntara; no constituye una mera justificación afirmarlo sino que las cifras y varias investigaciones académicas lo confirman. La Oficina de las **Naciones Unidas contra la Droga y el Delito** (UNODC) afirmó que son 6.350 familias en 29 municipios las que encuentran en estos cultivos su forma de supervivencia. (Le puede interesar: ["No es la coca, es la negligencia del Estado colombiano"](https://archivo.contagioradio.com/no-es-la-coca-es-la-negligencia-del-estado-colombiano/))

Una de las ocasiones para abordar un balance sobre este tema, desde la mirada de los campesinos, fue el evento **“Voces desde el campesinado cultivador; economía cocalera y Programa Nacional Integral de Sustitución de cultivos de uso ilícito”** (PNIS) celebrado en agosto en la Universidad del Rosario, en colaboración con la Universidad Nacional y organizaciones campesinas nacionales e internacionales. En el evento, fueron presentados los resultados de la encuesta realizada por ambas instituciones educativas, consultando a los directos beneficiarios del PNIS.

El profesor Francisco Gutiérrez Sanín, experto en tierras y profesor de la Universidad Nacional, dijo que esta era la **primera vez que se realizaba una encuesta entre cultivadores**; pues pese a lo obvio que parezca, nunca se había preguntado con los propios cultivadores los beneficios del PNIS, "que son los que más tendrían que ser consultados, tomando en cuenta que se habla de su pan cotidiano de trabajo sobre el que se desarrolla todas sus vidas". En segundo lugar, el profesor resaltó que el evento sirvió para disipar el prejuicio sobre los cultivadores de coca, porque "la clase media colombiana los ven como asociados a negocios criminales con comportamientos descontrolados, lo que no representa la realidad".

### **La responsabilidad política** 

Según declaró el experto en temas de tierras, los cultivos de coca se presentan  en zonas muy apartadas del país, hay grandes injusticias en las políticas públicas y el proceso electoral no cumple con las garantías básicas de participación social y supervisión local. **"Los campesinos tienen vulnerabilidades económicas y el Estado todavía no ha** **dado hasta ahora suficiente apoyo para los servicios básicos necesarios como infraestructuras o asistencia técnica y además, se han presentado demoras de los subsidios del PNIS".**

![Cultivadores de coca](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/09/Unknown.jpg){.wp-image-73193 .alignleft width="333" height="180"}

Muchas perspectivas políticas que fueron fundamentales en los PNIS, encuentran su base en el reconocimiento del problema en que los cultivos ilícitos existen por la pobreza, por el abandono del Estado, y sobre todo, por la secuela del conflicto armado. Vale la pena mencionar que, **de los más de 30 acuerdos** pactados para transitar hacia el desarrollo de economías licitas entre campesinos, integrantes de la Coordinadora Nacional de Cultivadores de Coca, Amapola y Marihuana (COCAAM), entes gubernamentales y alcaldías, **no se están implementando por no cumplir con los principios definidos en el acuerdo de paz**.

Por su parte, las legislaciones agropecuarias con el fin de gestionar las etapas posteriores al conflicto armado están incompletas, y no logran modificar la estructura de la propiedad de la tierra, según explicó el profesor. Adicionalmente, la falta de presencia del Estado en el territorio y la escasez de presupuestos y servicios de infraestructura, educación o sanidad que los municipios no consiguen proporcionar, asegura que no haya recursos necesarios para llevar a cabo los acuerdos.

### **Factores comunes de los territorios donde hay cultivos de uso ilícito**

El denominador común que se ha identificado en las zonas donde hay presencia de cultivos de uso ilícito, fue la alta **inequidad rural**. Por ejemplo, el poder de los ganaderos y de la agro industria está concentrado en pocas personas, mientras las empresas orientan su producción a la exportación, en lugar del consumo interno, lo que tampoco permite la subsistencia de los habitantes en los territorios.

En adición, más del 80% de cultivadores que participan en los planes ni siquiera tienen copia del contrato de sustitución, lo cuál indica una falencia grave en la formalización de los PNIS. Conjuntamente, desde el punto de vista económico, la mayoría de ellos han visto disminuidos sus ingresos; esto ha generado **desconfianza en el proceso y un potencial factor de violencia**.

![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/09/Coca-Bolivia.jpg){.aligncenter .size-full .wp-image-73197 width="627" height="352"}

Las comunidades sembradoras han señalado que un diálogo que asegure la participación pública, y el apoyo del Estado para brindar garantías de seguridad, serían ingredientes necesarios para cumplir con la reforma agraria e impulsar el Plan. En conclusión, además de la redistribución de tierra, el campesinado necesita acceso a crédito, insumo y asistencia técnicas, según afirmó Sanín. (Le puede interesar: ["Se agudizan agresiones contra campesinos que se acogieron a plan de sustitución en Putumayo"](https://archivo.contagioradio.com/se-agudizan-agresiones-contra-campesinos-que-se-acogieron-a-plan-de-sustitucion-en-putumayo/))

### **Paso a paso en avances** 

Hay también factores positivos que resultaron de esta investigación, por ejemplo, entre las familias cultivadoras **la distribución de la tierra es más equitativa,** y aunque no se ha cuantificado este dato "porque estamos hablando de cultivos de uso ilícito", sí se puede afirmar que con respecto a otras zonas del país, la propiedad no está en tan pocas manos. (Le puede intersar: ["El regreso del glifosato, una amenaza para campesinos en zonas cocaleras"](https://archivo.contagioradio.com/el-regreso-del-glifosato-una-amenaza-para-campesinos-en-zonas-cocaleras/))

Sanín concluyó, el PNIS es un elemento importante para la sustitución de cultivos, **para dar voces legales a los productores, resolviendo el problema de trabajar en un negocio que tiene que ver con actividades ilegales por definición**. Y añadió que si se desarrollaran más las medidas necesarias, y se consiguiera cumplir con los acuerdos alrededor del plan, sería una oportunidad de oro para el país.

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
