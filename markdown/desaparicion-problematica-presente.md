Title: Desaparición forzada, una problemática aún presente
Date: 2018-09-14 12:16
Author: AdminContagio
Category: DDHH, Nacional
Tags: Aumento paramilitarismo, Campaña Nacional contra la desaparición forzada, Desaparición forzada Colombia
Slug: desaparicion-problematica-presente
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/08/Medellín-desaparición.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto por: Archivo Contagio Radio] 

###### [14 Sept 2018]

[La desaparición forzada es una de las problemáticas q**ue más se visualiza en los diferentes países a nivel mundial** entre los cuales se encuentran: Argentina, con alrededor de 30.000 personas según Organizaciones de derechos humanos. Guatemala, que se calcula que existen actualmente entre 40.000 y 45.000. Y, en México, de acuerdo a cifras del Registro Nacional de Datos de Personas Extraviadas o Desaparecidas, al menos 30.973.]

[Además de los países mencionados anteriormente, hay varios más sin dejar atrás a Colombia, nación en la cual dicho problema se presenta desde hace muchos años atrás, dado en la mayoría de los casos a **raíz del conflicto interno generado por el paramilitarismo.**]

[En Colombia, según la cifra divulgada por el equipo del Observatorio de Memoria y Conflicto, del Centro Nacional de Memoria Histórica (CNMH), **82.998 personas fueron desaparecidas forzadamente desde 1958 hasta la actualidad**, cifra realmente elevada teniendo en cuenta que este es considerado un delito en la Ley 890 de 2004, recogido en el artículo 165 del Código Penal Colombiano.]

[Además, esta problemática también es nombrada en el artículo 12 de la Constitución Política de Colombia,  que dice: “**Nadie será sometido a desaparición forzada, a torturas ni a tratos o penas crueles, inhumanos o degradantes**.”]

[Según la Organización de las Naciones Unidas (ONU), la práctica generalizada y continuada de antedicho problema**, constituye un crimen de lesa humanidad, el cual debe ser percibido como uno de los más graves delitos,** y que conlleva a la violación de varios derechos que además, añade a cada familia involucrada a una incertidumbre interminable de no saber qué pasó, dónde están, y por tal razón, la no posibilidad de realizar el duelo.]

[Por lo tanto, la desaparición forzada es una adversidad que destroza a la familia debido al gran impacto que genera, tal y como lo afirma la directora de la Asociación de Familiares de Detenidos Desaparecidos (ASFADDES), Gloria Gómez: **“la desaparición forzada es un flagelo que destruye el núcleo familiar y social del desparecido por lo que en la familia el impacto de la desaparición de un ser querido es devastador"**.]

[Por último, y no menos importante, cabe resaltar que el día **30 de agosto, se lleva a cabo la conmemoración del Día Internacional de las Víctimas de Desaparición Forzada,** fecha que fue establecida por la Organización de las Naciones Unidas (ONU), desde el año 2011 con el fin de exigir justicia, verdad y honrar la memoria de los desaparecidos.]

###### [Reciba toda la información de Contagio Radio en [[su correo] 

<iframe id="audio_28585616" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_28585616_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>
