Title: Corte Suprema abre investigación formal contra Álvaro Uribe y Álvaro Prada
Date: 2018-07-24 16:31
Category: Nacional, Política
Tags: Alvaro Uribe, Corte Suprema de Justicia, falsos testigos, Iván Cepeda
Slug: corte-suprema-abre-investigacion-formal-contra-alvaro-uribe-y-alvaro-prada
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/07/Uribe-y-prada.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [24 Jul 2018]

A través de un comunicado de prensa, la  Corte Suprema de Justicia informó que abrirá investigación formal contra el senador Álvaro Uribe Vélez y Álvaro Prada, representante a la Cámara por el Centro Democrático en el 2014,  por el caso de falsos testigos. Puntualmente la Corte afirmó que logro reunir la cantidad de pruebas necesarias para llamar a indagatoria a estas dos personas, **para que respondan por los delitos de fraude procesal y soborno.**

Frente a este caso, la Corte estaba a la espera de la recolección suficiente de evidencia para continuar con el caso, en el que ya había manifestado que la indigación de investigar a Uribe por la manipulación de testigos, **obedecía a largas horas de grabación en las que aparece su voz relacionada con el tema**. Conclusión que el ex mandatario había calificado como una persecución por parte de la máxima instancia judicial frente a la cuál su abogado Jaime Granados interpondrá un recurso de reposición. La averigución de estos hechos fue realizada en los últimos meses por la Sala de Instrucción 2 de la Sala de Casación Penal.

**Uribe anuncia su renuncia del Senado**

A través de su cuenta de Twitter, el actual senador y presidente del partido Centro Democrático manifestó que "La Corte Suprema me llama a indagatoria, no me oyeron previamente, me siento moralmente impedido para ser senador, enviaré mi carta de renuncia para que mi defensa no interfiera con las tareas del Senado".

Actualmente en contra de Uribe hay otra investigación realizada por  el Tribunal superior de Medellín que compulsó copias contra el expresidente por su **presunta participación en las masacres de El Aro y La Granja**, así como en el asesinato del defensor de Derechos Humanos, Jesús María Valle. Hechos que ocurrieron mientras Uribe Vélez fue gobernador de Antioquía. (Le puede interesar: ["El historial de investigación contra Álvaro Uribe Vélez"](https://archivo.contagioradio.com/el-historial-de-investigaciones-contra-alvaro-uribe-velez/))

En declaraciones del asesinado paramilitar, Francisco Barreto, el exgobernador Uribe Vélez habría usado un helicóptero de la Gobernación de Antioquia en medio de la masacre de El Aro que tuvo lugar el 22 de octubre de 1997 y la habría corroborado personalmente.

### **Uribe quiere eludir a la Corte Suprema: Cepeda ** 

El senador Iván Cepeda expresó que este hecho "constituye un hito en la historia judicial del país", asimismo aseguró que esta decisión tendría que haber estado acompañada de una medida de aseguramiento, sin embargo, espera que el proceso marche con celeridad.

Frente a la renuncia de Uribe y el proceder jurídico que ahora tendría la Corte sobre él, Cepeda aseguró que **"esta hace parte de una estrategia para eludir a la Corte Suprema de justicia"** y que esta aún tiene competencias sobre el, porque los hechos en investigación fueron cometidos cuando Uribe se desempeño como senador.

Noticia en desarrollo ...
