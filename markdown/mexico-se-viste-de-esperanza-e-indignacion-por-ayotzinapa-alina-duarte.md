Title: México se viste de esperanza e indignación por Ayotzinapa: Alina Duarte
Date: 2015-09-26 08:00
Category: DDHH, El mundo
Tags: 43 normalistas desaparecidos, Ayotzinapa, Derechos Humanos, Jornada global por ayotzinapa, mexico, Movilización, Radio derechos Humanos
Slug: mexico-se-viste-de-esperanza-e-indignacion-por-ayotzinapa-alina-duarte
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/ayotzinapa-protestas.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: kaosenlared.com 

<iframe src="http://www.ivoox.com/player_ek_8615390_2_1.html?data=mZuel5iddI6ZmKiak5eJd6KnkpKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRkYa3lIqum93Np9Cf1MqY2M7XuMafxcqYx9jUqdPVz9%2FOjcqPrc%2FYyszbw8jNaaSnhqeg0JDUs9Ofot7c1t%2FNcYarpJKw0dPYpcjd0JC%2Fw8nNs4yhhpywj5k%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Alina Duarte , Revolución 3.o] 

###### [26 sep 2015] 

Tras cumplirse **un año de la desaparición de los 43 estudiantes normalistas** de la Escuela Rural de Ayotzinapa, se adelanta una nueva jornada global de movilización, encabezada por los padres e indignados de la sociedad mexicana,  exigiendo al gobierno de Enrique Peña Nieto revelar la verdad de lo sucedido con los jóvenes.

Alina Duarte, reportera de Revolución 3.o, asegura que "en México se vive una crisis política de derechos humanos, de violencia y de ilegitimidad", agudizada por las diferentes versiones que frente al caso ha presentado el gobierno nacional y que han sido [desmentidas por el grupo interdisciplinario de expertos independientes](https://archivo.contagioradio.com/giei-desmiente-version-oficial-sobre-la-desaparicion-de-43-estudiantes-de-ayotzinapa/) (GIEI).

Las manifestaciones provenientes desde diferentes puntos del planeta en solidaridad con las familias de los desaparecidos contrasta con la posición dilatoria de la justicia mexicana, por lo que expresa Duarte en esta fecha el país "se viste de esperanza e indignación", en medio de un panorama de violencia que se ha replicado en distintos estados y poblaciones.

**Estas son las actividades en apoyo a Ayotzinapa:**

Para la movilización durante la jornada global por Ayotzinapa, se preparan distintos colectivos, comunidades que se han organizado y hacen parte del  movimiento estudiantil.

Los padres de los 43 normalistas, realizaron un ayuno durante 43 horas desde el **23 de septiembre.**

La Embajada de México en Madrid, realizará diversos actos culturales en Marsella, Francia y presentarán el libro "Morir en México" del periodista J. Gibler , además de la presentación de una exposición fotográfica.

En Berlin (Alemania) se llevará a cabo una **doble Jornada de memoria y discusión** por el aniversario de la desaparición de los 43 estudiantes de Ayotzinapa, con proyecciónes musicales y rodadas en bicicleta.

El 26 de septiembre, **día denominado por los padres de los estudiantes como "El Día de la Indignación",** para el que** **se convocó a una **marcha multitudinaria** en la Ciudad de México, que partirá desde las inmediaciones de la residencia oficial de Los Pinos hasta el Zócalo capitalino a las doce del día.

A las exigencias de la sociedad mexicana se unen también distintos países del mundo que por medio de sus **embajadas participan de estas jornadas** como Argentina, Costa Rica, París y otras importantes ciudades del mundo.

Los usuarios de diferentes redes sociales en todo el mundo, podrán sumarse a la conmemoración utilizando la etiqueta  [~~\#~~AcciónGlobalPorAyotzinapa](https://twitter.com/hashtag/Acci%C3%B3nGlobalPorAyotzinapa?src=hash)
