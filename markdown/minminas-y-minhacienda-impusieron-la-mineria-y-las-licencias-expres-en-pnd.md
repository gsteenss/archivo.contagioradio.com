Title: Minminas y Minhacienda impusieron la minería y las licencias exprés en PND
Date: 2015-05-07 14:20
Author: CtgAdm
Category: Ambiente, Nacional
Tags: Angela Maria Robledo, Iván Cepeda, Licencias Exprés, Mauricio Cárdenas, Ministerio de hacienda, Ministerio del Minas y Energía, Partido Verde, Plan Nacional de Desarrollo, PND, Polo Democrático Alternativo, San Turbán
Slug: minminas-y-minhacienda-impusieron-la-mineria-y-las-licencias-expres-en-pnd
Status: published

##### Foto: elpueblo.com.co 

<iframe src="http://www.ivoox.com/player_ek_4462358_2_1.html?data=lZmjlJiZfI6ZmKiakp6Jd6KnlJKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRkcrizs7bw9iPvYzBytPVw8jNqc%2FYwpDWz9XZt8rZ09TbjdHFb87dz8rfh6iXaaK4wpDmjdHFt4zgysiah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Ángela María Robledo, representante a la cámara del Partido Alianza Verde] 

[[Finalmente]]**[la minería y las licencias exprés quedaron dentro del Plan Nacional de Desarrollo,]**[[ por medio del artículo 177 que permite la minería en páramos, y el 183, que avala el otorgamiento de las licencias exprés, gracias a los impulsos que les dieron los ministros de minas y de hacienda.]]

Según la representante a la cámara del Partido Alianza Verde, Ángela María Robledo, **en senado se logró eliminar los parágrafos 1 y 3 del artículo 177** que permitían que las concesiones mineras actuales continuaran con esa actividad, sin embargo, **en la conciliación el ministro de hacienda hizo que nuevamente se abriera la votación** de ese artículo “y se votó eliminando los parágrafos, pero en la conciliación se votó  el texto de Cámara (es decir con los parágrafo incluidos)”.

[![PND 2](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/05/PND-2.png){.wp-image-8246 .aligncenter width="399" height="208"}](https://archivo.contagioradio.com/minminas-y-minhacienda-impusieron-la-mineria-y-las-licencias-expres-en-pnd/pnd-2/) [![PND 1](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/05/PND-1.png){.wp-image-8247 .aligncenter width="398" height="224"}](https://archivo.contagioradio.com/minminas-y-minhacienda-impusieron-la-mineria-y-las-licencias-expres-en-pnd/pnd-1/)

Pese a que **el artículo 177 había sido modificado y el artículo 183 había sido eliminado en Senado,** ambos tenían la posibilidad de volver a ser incluidos en el texto final, ya que una comisión extraordinaria debía definir y unificar el texto para su aprobación final, y así fue, gracias a la presión que ejercieron los Ministerios de Minas y Energía y el de Hacienda, como lo asegura la representante del Partido Verde, Robledo y el senador del Polo Democrático Alternativo, Iván Cepeda, “**en la conciliación de media noche el Ministerio de Minas y Hacienda impusieron el texto que estaba en Cámara**, así que se podrá seguir explotando riqueza mineral en estas zonas que son santuarios ecológicos”, indicó Cepeda.

De esta manera, aunque para futuros proyectos mineros estará prohibido ejercer esta actividad en páramos, los **347 títulos mineros que existen en 26 zonas de páramo** del país, podrá continuar generando daños al ambiente.

[![Sin título](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/05/Sin-título1.png){.wp-image-8248 .aligncenter width="401" height="222"}](https://archivo.contagioradio.com/minminas-y-minhacienda-impusieron-la-mineria-y-las-licencias-expres-en-pnd/sin-titulo-5/)

“El articulo 177 tiene nombre propio, son tres empresas mineras,  una de ellas es el caso emblemático para Colombia en San Turbán”, dice la representante.

De esta manera, la locomotora minera predominó, y la minería y licencias exprés definitivamente se incluyeron en el PND, “**cada gran sector tiene su articulito… son adefesios frente a un país que es rico en su biodiversidad**,  pero no vamos a tener país para las futuras generaciones” concluye la congresista.
