Title: Dos líderes sociales de El Castillo, Meta fueron asesinados
Date: 2020-09-13 12:13
Author: CtgAdm
Category: Actualidad, DDHH
Tags: ariari, asesinato de líderes sociales
Slug: lideres-sociales-el-castillo-meta-fueron-asesinados
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/09/WhatsApp-Image-2020-09-13-at-12.10.21-PM.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: El Castillo, Meta / Contagio Radio

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

El pasado 12 de septiembre, la Corporación Claretiana Norman Pérez Bello informó que en el corregimiento de Medellín del Ariari, municipio de El Castillo, Meta fueron asesinados **Ramón Enrique Montejo y Simón Ochoa, integrantes de la junta de acción comunal de la vereda Caño Claro.** Según el Observatorio Nacional Comunal, para agosto, siete comunales fueron asesinados, 53 durante el año 2020 y 145 durante el Gobierno del presidente Iván Duque.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Según la organización, en horas de la tarde, hombres armados que se movilizaban en una motocicleta XTZ de color blanco llegaron a la vivienda de Ramón Montejo, presidente de la Junta de Acción Comunal y dispararon en varias ocasiones en su contra, pese a que el dirigente intento huir, las heridas ocasionaron su muerte. [(Lea también: Caminar para revivir las memorias de El Castillo, Meta)](https://archivo.contagioradio.com/caminar-para-revivir-las-memorias-de-el-castillo-meta/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Posteriormente, los armados también se dirigieron al lugar donde se encontraba Simón Ochoa, expresidente de la JAC, a quien también asesinaron. [(Le puede interesar: Tras 17 años de desaparición forzada entregarán cuerpo óseo de Jolman Lozano en Meta)](https://archivo.contagioradio.com/tras-17-anos-de-desaparicion-forzada-entregaran-restos-de-jolman-lozano-en-meta/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Los líderes sociales del municipio de El Castillo, han manifestado el temor que existe ante estos sucesos alteren la normalidad que se experimentaba en la zona rural. Al respecto, organizaciones como la [Comisión Intereclesial de Justicia y Paz](https://www.justiciaypazcolombia.com/) señalan que urgen medidas eficaces por parte del Estado "para garantizar la vida y libertad de expresión, y asociación de las comunidades del Ariari". [(Lea también: Oliverio Conejo, coordinador de salud indígena y su hija fueron asesinados en Cauca)](https://archivo.contagioradio.com/oliverio-conejo-coordinador-de-salud-indigena-y-su-hija-fueron-asesinados-en-cauca/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Cabe señalar que históricamente, la región de El Castillo, Meta ha sido estigmatizada política y socialmente por parte de agentes del Estado, abriendo paso para que narcotraficantes, paramilitares y guerrilleros desencadenaran, después de la creación de la Unión Patriótica, una violencia desmedida en contra de los castillenses. 

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### No solo es Meta, continúan ataques contra Juntas de Acción Comunal

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Durante el 11 de septiembre también fueron asesinados el dirigente comuna **Cristobal Ramos del barrio Villa Clemencia en Montelibano, Cordoba, mientras Yoni Valdés, presidente de la Asociación de Juntas de Acción Comunal en San José de Uré Cordoba fue víctima de un atentado.**

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Aunque los asesinatos contra líder sociales tienen diferentes orígenes a raíz de los múltiples actores armados que existen en los territorios, existe una sistematicidad que se ve evidenciada en el perfil de las víctimas, quienes en su accionar al interior de las juntas de acción comunal a menudo se oponen a economías ilegales y que desarrollan un ejercicio político en sus comunidades

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
