Title: Estado falla en la protección de derechos de las mujeres y las niñas
Date: 2018-05-04 13:16
Category: DDHH, Política
Tags: derechos de las mujeres, mujeres, Violencia contra las mujeres, violencia contra los niños
Slug: estado-ha-fallado-en-la-proteccion-de-los-derechos-de-las-mujeres-y-las-ninas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/05/mujeres.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Proyecto 341.com] 

###### [04 May 2018] 

El más reciente caso de violencia contra las niñas que fue registrado en Bogotá, una menor de tres años fue **víctima de abuso sexual** y maltrato, un hecho que continúa evidenciando la falta de garantías y protección para la vida de las mujeres. Las organizaciones que defienden los derechos de las mujeres enfatizaron en que hace falta un cambio cultural en la sociedad y cuestionaron la efectividad de las instituciones a la hora de proteger a los niños y las niñas del país.

De acuerdo con Sandra Maso, coordinadora de la organización Católicas por el Derecho a Decidir, la Ley 1257, que habla sobre la necesidad de proteger a las mujeres, **también incluye a las niñas**. Afirmó que “hace falta un énfasis en que las niñas deben tener una protección especial en tanto que son menores de edad”.

Afirmó que son las mujeres y las niñas que tienen menos recursos, quienes sufren en mayor medida la ausencia de atención y protección por parte del Estado. Por esto argumentó que **"la brecha de desigualdad y pobreza"** en el país se evidencia en la falta de garantías del Estado por brindar igualdad de condiciones ante sus ciudadanos.

### **Estado no ha protegido los derechos de las mujeres y los niños**

Sin embargo, recordó que esto debe ser entendido como un derecho por lo que el Estado tiene la responsabilidad de **proteger y atender a los niños y a las niñas.** Por esto, afirmó que en los casos como el presentado en Bogotá, “hay una omisión por parte del Estado que no tomó las medidas necesarias para proteger la salud y la vida de la menor”. (Le puede interesar:["Llegó la hora de que las mujeres cuestionen a los candidatos a la presidencia"](https://archivo.contagioradio.com/llego-la-hora-de-que-las-mujeres-cuestionen-a-los-candidatos-a-presidencia/))

Adicional a esto, manifestó que en Colombia hay una cantidad significativa de leyes que protegen los derechos de las mujeres pero **no hay regulaciones que implementen** de manera efectiva los mecanismo que disponen las normas. Por esto, manifestó que “hay un vacío en las leyes y una inoperancia total de las instancias que tienen que ver con la protección de los derechos de las mujeres”.

### **Sociedad se debe movilizar para rechazar la violencia contra las mujeres y los niños y niñas**

Ante este panorama, Maso hizo un llamado para que las organizaciones que defienden los derechos de las mujeres y los niños **se movilicen** y hagan que las instituciones “respondan y sean sancionadas por la omisión en la protección y seguimiento de estos casos”. Dijo que “hay que hacer denuncias contras estas instituciones para que le cumplan a la ciudadanía con las funciones que tienen y no están cumpliendo”.

También enfatizó en que la totalidad de la sociedad colombiana **debe manifestarse** ante este tipo de actos y ante la inoperancia del Estado. Esto, teniendo de presente “la impunidad y corrupción que están cometiendo al no cumplir con sus labores”.

Finalmente, dijo que las instituciones “se han encargado de **generar más violencia** contra las mujeres”. Argumentó que más allá de las leyes, debe haber una transformación cultural “para despertar la indignación de la gente” pues “la violencia contra las mujeres se ha vuelto un asunto naturalizado”.

<iframe id="audio_25793762" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_25793762_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]

 
