Title: Animalistas exigirán "no más viOLEncia contra los animales" este #9A
Date: 2015-04-08 22:30
Author: CtgAdm
Category: Animales, Voces de la Tierra
Tags: 9 de abril, Animales, Animalistas, Consulta Antitaurina, consulta popualr, Natalia Parra, paz
Slug: animalistas-exigiran-no-mas-violencia-contra-los-animales-este-9a
Status: published

##### Foto: contagioradio.com 

<iframe src="http://www.ivoox.com/player_ek_4326354_2_1.html?data=lZifmJiZeI6ZmKiakp6Jd6KokZKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRkNDnjMbby9LFsMrn1cbgjdnFscPdhqigh6adsozhwtfQysbWaaSnhqae0JDJsIyZk5imo5DUrcXdxtPR0ZDSs4yhhpywj6jTstXVyM7cjbfFqMrjjJKSmaiReA%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Natalia Parra, Plataforma ALTO] 

Este martes 7 de abril, organizaciones que hacen parte del movimiento animalista Bogotá sin Toreo y la ciudadanía en general, se reunieron para realizar **la asamblea organizacional de la Consulta Antitaurina.**

El evento, que fue con entrada libre, tuvo como objetivo socializar y discutir sobre herramientas, pautas, conceptos y estrategias, sobre la consulta popular que busca acabar con las corridas de toros en Bogotá.

A la asamblea asistieron personas diversas que no necesariamente son animalistas, pero que están en favor de la vida de los animales. De acuerdo a Natalia Parra, directora de la Plataforma ALTO e integrante de la mesa de trabajo de Onda Animalista de Contagio radio, este tipo de asambleas, “**hacen parte de ese esfuerzo que estamos realizando los ciudadanos por una Bogotá sin toreo**, para tener muy claro los asuntos discursivos con respecto a lo que tiene que ver con los fallos de la Corte, y lo que es una consulta popular”.

Además, se contó con la participación **del concejal de Bogotá, Roberto Sáenz**, como secretario de la bancada animalista, quien evidenció que el ambiente en el consejo  es favorable para que se logre el feliz trámite de la consulta.

Precisamente, la consulta antitaurina y la paz para los animales, porque “ellos también son víctimas”, serán una de las consignas de los animalistas para la marcha de este 9 de abril, donde pedirán “**no más viOLEncia contra los animales no humanos”.**

Esta será la tercera vez que los animalistas se vincularán a la movilización de este jueves y  se encontrarán al lado del Parque El Renacimiento cercano al Centro de Memoria, desde allí marcharán exigiendo paz y respeto a todos los seres vivos, ya que como dice Natalia Parra, “**no podemos hablar de una paz antropocéntrica, la paz es para todos”.**
