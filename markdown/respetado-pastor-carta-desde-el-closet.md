Title: Respetado pastor - Carta desde el Closet
Date: 2017-09-23 14:19
Category: Cartas desde el Closet, Opinion
Tags: Comunidad LGBTI, Iglesia cristiana, LGBTI
Slug: respetado-pastor-carta-desde-el-closet
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/09/PASTOS-DESDE-EL-CLOSET.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: pxhere 

#### [Septiembre 2017] 

**Respetado pastor**

Espero que esté bien.

[Perdóneme por escribirle anónimamente pero sentía la necesidad de desahogarme.]

[Me congrego en la Iglesia desde niño, primero acompañado de mi mamá y luego con mis amigos; ya tengo más de 20 años. He sido bendecido por el Señor y para mi vida ha sido muy importante el culto y la enseñanza; la alabanza ha fortalecido mi fe. Mis mejores amigos son de la Iglesia.]

[Le confieso que comencé a vivir un infierno cuando me fui dando cuenta que no sentía atracción por las mujeres; y lo peor, me atraían los hombres, no podía evitarlo. Cuando iba a Iglesia y la Palabra del Señor y sus enseñanzas decían que los homosexuales estaban condenados, que eran una aberración y que era aborrecido por Dios, me sentía muy mal.  ]

[Pero mi atracción por los hombres no la podía evitar. Oré intensamente, hice ayuno, leí la palabra de Dios y muchas veces le pedí a usted que orara al Señor por mi salvación sin decirle porqué. Usted lo hizo y nada cambió en mí. Me alejé de la Iglesia unos meses, volví, me arrepentí, lloré y nada ha cambiado, no me atraen las mujeres y sigue mi atracción por los hombres. Me aterroriza la idea de ser homosexual, pero no puedo evitar sentir que lo soy.]

[¿Cómo negar lo que siento, que no lo busqué y no lo pedí? ¿Por qué me pasó a mí si fui un buen creyente y fiel a la Iglesia? Pero la realidad es que desde niño nunca me llamaron la atención las niñas.]

[He seguido con atención sus predicaciones y he puesto en práctica sus enseñanzas para salir de esa perversión que lleva al infierno, para dejar ese pecado. Pero ha sido inútil, no he podido.]

[ Tengo miedo, mi fe está en crisis. No sé qué hacer con mi vida]

[P**arche para la vida, 8 **](https://archivo.contagioradio.com/cartas-desde-closet/)

------------------------------------------------------------------------

###### Esta texto hace parte de Cartas desde el Closet, escritos anónimos de personas con orientaciones de género diversas que no han podido compartir públicamente sus pensamientos e ideas por su orientación sexual. Parche por la Vida
