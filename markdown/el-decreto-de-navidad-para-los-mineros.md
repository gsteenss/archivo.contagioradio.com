Title: Andrée Viane - El decreto de navidad para los mineros
Date: 2015-01-17 20:17
Author: CtgAdm
Category: Opinion
Tags: Mineria
Slug: el-decreto-de-navidad-para-los-mineros
Status: published

Decreto 2691 de 23 de diciembre de 2014

**Por Andrée Viana y Rodrigo Negrete**

Uno de los Decretos de Navidad que firmó el Gabinete del Presidente Santos, fue una auténtica sorpresa.

De un lado, la sorpresa se explica porque muchos temíamos que el mensaje de la sentencia C-123 de 2014 no llegaría a producir efectos. En ese sentido, habría que celebrar la expedición del Decreto 2691 de 23 de diciembre de 2014, con el que podremos empezar una discusión seria y concreta del asunto.

Es importante que exista esta norma, porque revela con claridad el entendimiento del gobierno central sobre la autonomía de las entidades territoriales y el principio democrático que la fundamenta; aclara su posición sobre la distribución de competencias en materia de producción normativa, de ordenamiento territorial y de propiedad pública de las riquezas del subuselo.

Y, es importante porque nos permite recordarle al Gobierno, con voz de alarma, que el cumplimiento estricto de esas órdenes judiciales condiciona la vigencia del que ha sido defendido como uno de los pilares del código de minas: sólo acatando con rigor las órdenes de la Corte, el artículo 37 será constitucionalmente adecuado. O dicho de otra forma, entenderlo en sentido contrario o alejado de la única interpretación constitucionalmente posible (la de la sentencia C-123/14), y producir regulaciones y decisiones en esa línea, sólo complicará el camino hacia la seguridad jurídica, reclamada con tanto ahínco por el sector industrial.

De otro lado, la sorpresa viene por ahí: sin duda, un reglamento que desarrolle el artículo 37 entendido de cualquier manera distinta a la impuesta por la Corte, puede ser inconstitucional. Y puede que este decreto esté cubierto por una sombra de este tipo que acorte su vida útil.

Para saber de qué hablamos, en resumidas cuentas, según la Corte Constitucional, el artículo 37 sólo puede ser constitucional si en el proceso de autorización para la realización de actividades de exploración y explotación minera, es decir en el proceso para conceder un título minero se tienen en cuenta los aspectos de coordinación y concurrencia, que se fundan en el principio constitucional de autonomía territorial. Por eso, la sentencia ordenó a las autoridades mineras que acordaran con los entes territoriales el procedimiento para otorgar títulos mineros en sus territorios.

<!--more-->

Esa solución, tiene dos implicaciones principales, también según la Corte.

\(a) Primero, implica que la Nación continúe participando en el proceso, pero no como el único nivel competencial involucrado en la adopción de esa decisión tan transcendente para la vida local.

Esto significa que los municipios y distritos, pero también los corregimientos departamentales afectados por dicha decisión, deben poder participar de una forma activa y eficaz (la corte resalta estas palabras) en el proceso de adopción de la misma. Es decir que su opinión debe influir en la adopción de la decisión, sobre todo en asuntos axiales a la vida del municipio como son la protección de cuencas hídricas, la salubridad de la población y el desarrollo económico, social y cultural de sus comunidades

\(b) Y segundo, implica que se debe asegurar la participación de los municipios en la determinación de cinco aspectos concretos.: i) los fines que la exclusión de la actividad de exploración minera en determinadas áreas del territorio busque alcanzar, ii) las causas y condiciones que determinan que un área del territorio se declare como zona excluida de esta actividad; iii) la forma en que cada uno de los niveles competenciales participen en el proceso de creación normativa; iv) las funciones específicas que uno y otro nivel tendrá en ejercicio de dicha competencia y v) los parámetros que deban cumplir los procedimientos que se creen para declarar una zona excluida de la actividad minera.

Lo que la sentencia dispuso, además de ordenar la participación en el proceso para la decisión de otorgar los títulos, fue incluir a los municipios en el diseño de la norma que regulara ese proceso. Esa decisión de la corte busca corregir el exceso de centralismo que padece la lectura literal del artículo 37 del CM.

La Corte insiste en que existen intersecciones competenciales que deben ser resueltas de acuerdo con el principio de coordinación, y en consecuencia ordena que el modelo de decisión sobre proyectos mineros en territorio municipal sea participativo y garantice que esa participación de los entes locales sea activa y eficaz.

Eso sólo es posible con una auténtica discusión entre los niveles local y nacional, para que, entre todos, definan qué fines podrían alegarse para la exclusión de la actividad minera; cuáles causas y cuáles condiciones debería tener un área para poderse excluir de esa actividad; y acuerden la forma y facultades con que cada nivel participaría en el proceso de creación normativa en adelante.

Haber expedido el decreto sin agotar las discusiones sobre los puntos básicos señalados por la Corte no sólo es un primer síntoma de desacato de la sentencia, sino que además permitió la inclusión de fórmulas que habrían podido evitarse en un diálogo con la periferia. Algunas de esas fórmulas problemáticas se comentarán enseguida:

El decreto da un giro importante que acerca (un poquito más) el sistema de producción normativa del sector minero al modelo territorial diseñado en 1991, al levantar la prohibición de intervenir en la planeación minera desde los entes locales. Pero en estos temas los tanteos y timideces no suelen valer. La Corte en su sentencia impuso una solución más radical y previó que las medidas de protección pueden incluir la exclusión de actividades mineras en ciertas áreas municipales. Es más, expresamente prescribió una concertación entre el nivel central y el local para definir los fines que pueden justificar dicha exclusión.

Lamentablemente, no hubo consenso ni participación en la definición de esas finalidades ni de ninguno de los términos del decreto. Y en cambio se diluyó la posibilidad de que la exclusión de la actividad minera pudiera ser una de las medidas de protección que puede acordar un concejo municipal o distrital.

El reconocimiento del derecho de los municipios a proteger sus fuentes hídricas, su territorialidad y su estructura económica y social, no sólo fue unilateral, sino que su realización efectiva quedó sometida a requisitos que se adivinan, más bien, como importantes obstáculos. En el decreto, el principio de coordinación competencial se aplicó de forma tan débil y reducida, que no parece probable la adopción de una medida de protección que consista en la exclusión de un área, o que suponga la reducción del tamaño de los proyectos solicitados.

Así parece, porque el proceso que diseña el decreto es un trámite lejano al diálogo, más bien parecido a una diligencia en que los municipios sugieren medidas y el gobierno central las adopta o las rechaza.

El proceso que prescribe el decreto puede resumirse a grandes rasgos así: cuando los concejos municipales o distritales cambien sus planes, o planes básicos o esquemas de ordenamiento territorial, o en cualquier momento antes del próximo 23 de marzo (manteniendo la concordancia con los instrumentos de ordenamiento territorial existentes), podrán presentar al Ministerio de Minas las propuestas de medidas de protección, fundamentadas en estudios técnicos que deberá haber contratado y pagado el propio municipio.

Una vez recibida la solicitud el Ministerio la reporta a la ANM para que no se otorguen títulos en las áreas sobre las que los municipios hayan solicitado medidas de protección, hasta que éstas sean decididas. El Ministerio de Minas envía la propuesta a las autoridades nacionales competentes de las materias a las que se refieran las medidas, para que rindan un concepto técnico. Además, Minminas puede pedir el del DNP para establecer el impacto económico de las medidas de protección solicitadas, y consultará con las empresas del gremio sobre la conveniencia de los proyectos en tensión con las medidas solicitadas.

El Ministerio decide si adopta o no las medidas, con fundamento en unos principios sectoriales enunciados en el decreto: desarrollo sostenible, fortalecimiento económico del país, propiedad estatal de los recursos no renovables y su aprovechamiento eficiente.

El fallo dice que se debe garantizar el principio de coordinación en el proceso de autorización para las actividades mineras, lo que evoca un diálogo multinivel durante el trámite para conceder los títulos. En el contexto de la sentencia, el proceso se parecía más a una relación de coordinación entre entidades competentes, que a la formulación de una solicitud a una entidad con competencia exclusiva para decidir, en todo caso, sobre principios ajenos a los que gobiernan las determinaciones públicas sobre necesidades de la vida local.

En efecto, el gobierno central no va a decidir con fundamento en los principios de precaución y prevención, ni en el deber de planificación de los recursos naturales o en la obligación de favorecer las condiciones ambientales que garanticen el derecho al agua y a la soberanía alimentaria, tampoco en el principio de autonomía de los entes territoriales, y parece que aún menos en el principio pro-homine. Su decisión, según el decreto, dará prioridad a principios relacionados con el crecimiento económico y la eficiencia en el aprovechamiento de recursos, y teniendo en cuenta la opinión de las empresas y de las autoridades de planeación macroeconómica del país.

Hay que encender las alarmas frente a la posibilidad de desplazar principios ambientales y de los derechos humanos en favor de consignas del sector minero. Aun cuando un decreto no tenga capacidad normativa para modificar la estructura sustancial del Estado social de Derecho colombiano: nuestro sistema constitucional obliga a la Administración a decidir, siempre que haya duda y estén en juego los derechos humanos, en el sentido que mejor beneficie el principio pro-homine.

Y ésta era la clave que debería haberse consagrado para las discusiones en cada caso concreto: los derechos de las personas, que dependen de las medidas de protección del suelo, el agua y el aire, no son disponibles por ninguna autoridad, ni con base en los más apabullantes argumentos económicos, si existieran.

Pero hay un asunto que empeora el escenario y eleva el volumen de las alarmas: el decreto dispone que estas decisiones sólo se aplicarán a las solicitudes de títulos no concedidos al momento de su expedición, cuando debería haberse dispuesto que se aplicarían a todas las solicitudes que estaban en estudio desde el 5 de marzo de 2014, momento en que la sentencia C-123 de 2014 produce efectos.

No está claro que haya habido un paréntesis de prudencia en esos 9 meses de diferencia, en que la ANM haya suspendido la concesión de títulos mineros, de manera que es posible que los otorgados sin cumplir los mandatos de la Corte deban ser revocados, pues el procedimiento que los precede no respeta la única interpretación válida del artículo 37 del Código de Minas.

Finalmente, otro obstáculo hacia la realización efectiva del derecho a la participación local en el proceso de autorización de actividades mineras, es el del pago de los estudios a cargo de los municipios. Si bien puede tener sentido democrático que se hagan estudios encargados por entidades públicas no interesadas directamente en la realización de una actividad minera, el contexto del decreto indica que esos estudios deben ser generales, no para cada título minero en particular, y además que deben ser pagados por cada municipio.

El pago de estos estudios, si son como los plantea el decreto, no para discutir proyectos concretos sino con un alcance general y abstracto que justifique las decisiones sobre ordenación territorial, no sólo no parece estar cobijado por habilitación legal alguna, sino que podría conllevar un detrimento patrimonial injustificado, al menos por dos razones. La primera porque los impactos negativos de la minería en materia ambiental, social y cultural, ya están ampliamente documentados en el país, por la misma Corte Constitucional entre otras en las sentencias C-339 de 2002 y T-154 de 2013, y en diversos estudios técnicos entre los que se encuentran algunos producidos por el propio Estado Colombiano, como la obra “Minería en Colombia”, publicada por la Contraloría General de la República. La segunda porque en todo caso el gobierno central no decidirá de acuerdo con principios propios del derecho ambiental y de los derechos humanos, por lo que los estudios no tendrán mucha utilidad.

De otra parte, la disposición acerca del sujeto público que debe pagar esos estudios toca un punto grave de la realidad colombiana, que se refiere a la debilidad fiscal de muchos municipios y a las diferencias en la capacidad económica entre unos y otros. Así, el ejercicio del derecho a participar en las decisiones que afectan el destino de sus pobladores, queda sometido a la solidez de las arcas municipales.

Esto puede significar que se ponga en riesgo el principio de igualdad ante la ley: los municipios más pobres, que en muchos casos tienen gran potencial minero, no podrán ejercer su derecho en igualdad de condiciones que los más ricos, pues la calidad de los estudios que soporten las medidas de protección solicitadas dependerá, desde luego, de que pueda pagarse un buen equipo de expertos.

Pero además, esa condición de pago de los estudios revela una característica preocupante del decreto. Toda la norma impone una alta carga de responsabilidades a las entidades territoriales, como si la orden de la Corte hubiera ido dirigido a esas autoridades locales y no a las autoridades mineras del gobierno central. El gobierno central parece entender que la ANM está autorizada a seguir concediendo títulos mineros sin hacer mucho caso a la existencia del deber constitucional (artículo 313.19 de la Constitución) del Estado, en cabeza de los entes territoriales, de defender de manera permanente su patrimonio ecológico. En este decreto, en efecto, se sujeta la efectividad del principio de coordinación, y con ella la de la protección del medio ambiente, la salubridad y bienestar de los habitantes al engorroso trámite de modificación de los POT municipales, sumado al otro nuevo y sui generis trámite que trae su articulado.

En conclusión, el Decreto abre nuevas discusiones y permitirá continuar dando otras que ya son conocidas. Lo importante ahora es que se renueve el diálogo sobre el verdadero alcance de la descentralización, sobre su relación con los derechos de la gente, sobre su fundamento eco-social y su irreductible relación con el principio democrático.

Lo importante es que, de verdad, se abran las discusiones, que se sostengan con transparencia en el ámbito social, académico, periodístico o judicial, y, sobre todo, que conduzcan más pronto que tarde a un sistema de minería constitucional, más cercano a los estándares y buenas prácticas internacionales que el de la minería legal, tan defendido por los gremios del sector, que parecen estar cómodos con las normas echas a medida.
