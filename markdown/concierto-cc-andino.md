Title: Concierto en solidaridad con los detenidos por el caso del CC Andino
Date: 2017-08-02 16:28
Category: eventos
Tags: cc andino, Concierto, solidaridad
Slug: concierto-cc-andino
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/08/Concierto.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Comité libres e inocentes 

###### 02 Ago 2017 

Con el lema "La alegría esta con nosotros", este sábado 5 de Agosto se realizará el **concierto Resuena libertad**, un evento organizado en solidaridad con los jóvenes detenidos por las explosiones ocurridas en el Centro Comercial Andino de Bogotá el pasado 18 de junio.

Carol Ruiz, integrante del Comité libres e inocentes, asegura que la intención del evento es "**encontrarnos desde la alegría para expresar nuestra voz de afecto y apoyo y de solidaridad**" asegurando que desde el inicio de las investigaciones han creído en la inocencia de los 9 implicados por tratarse de defensores de la vida y los DDHH.

Ruíz asegura con convicción que se trata de un falso positivo judicial y que el evento servirá para "decir una vez mas **no a la estigmatización de la Universidad Pública, no a la persecución al pensamiento crítico**, que creemos en su inocencia, que los queremos y las queremos libres"

"Esto no es nuevo en Colombia, **hace dos años detuvieron estudiantes de Universidades públicas e integrantes de movimientos sociales**"asegura la joven en referencia al [Caso de los 13](https://archivo.contagioradio.com/estos-son-los-perfiles-de-las-personas-capturadas-en-bogota/)y en general a los diferentes líderes sociales que han sido víctimas de montajes judiciales por sus [posturas ideológicas](https://archivo.contagioradio.com/carcel-para-implicados-en-caso-andino-esta-motivada-por-posturas-ideologicas/).

Al llamado de los organizadores del concierto respondieron artistas como **Reincidentes Bta, Cuentos de los hermanos Grind, Novena nota, Bent, Yela Quim, Arenga, Samurai, Melatonina y Vizocios** quienes se estarán presentando en **Acto Latino**, k16 \#58a-55 desde las 2 de la tarde. El costo de la entrada, exclusiva para mayores de edad, es de 15.000 pesos.

Por tratarse de un evento en solidaridad, el dinero recogido por concepto de taquilla servirá para apoyar a los familiares de los jóvenes, quienes han tenido que **perseverar con fortaleza ante las diferentes decisiones judiciales y la presión mediática** que ha rodeado el caso, como señala Ruíz.

<iframe id="audio_20137797" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_20137797_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU).
