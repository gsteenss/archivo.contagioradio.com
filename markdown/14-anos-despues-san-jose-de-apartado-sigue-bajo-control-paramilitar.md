Title: 14 años después, San José de Apartadó sigue bajo control paramilitar
Date: 2019-05-13 18:02
Author: CtgAdm
Category: Comunidad, Memoria
Tags: Comunidad de Paz de San José de Apartadó, San josé de Apartadó
Slug: 14-anos-despues-san-jose-de-apartado-sigue-bajo-control-paramilitar
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/04/Comunidad-de-Paz-San-Jose.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Agencia de prensa IPC] 

Más de 14 años después de que militares y paramilitares conjuntamente acabaran con la vida de ocho personas de la comunidad de paz en San José de Apartado, en Antioquia, esta semana, **la Corte Suprema de Justicia condenó a seis militares del Ejército por su participación en la masacre del 2005**.

El alto tribunal encontró  al coronel Orlando Espinosa Beltrán, el mayor José Fernando Castaño López, los sargentos Henry Agudelo Cuasmayán Ortega y Ángel María Padilla Petro; y los cabos Ricardo Bastidas Candia y Sabaraín Cruz Reina como los responsables de este crimen.

Según uno de los habitantes, la decisión del alto tribunal afirma la posición de los pobladores de esta comunidad quienes siempre han sostenido que la Fuerza Pública tuvo una participación activa en la ejecución de la masacre, y que este delito ha quedado casi en la impunidad. (Le puede interesar: "[Así es el control paramilitar en San José de Apartadó](https://archivo.contagioradio.com/paramilitares-controlan-veredas-de-san-jose-de-apartado/)")

La comunidad pide que la Corte Suprema abre procesos legales en contra de los altos mandos, en particular **los generales de la Brigada XVII y la Séptima División**, que en ese tiempo tenían jurisdicción sobre esta región del Urabá antioqueño y que aún "no han sido sometido a ningún tipo de justicia".

"Todavía siguen buscando a los culpables materiales, los soldados, pero a pesar de eso, es importante que se siga aclarando las cosas y no como lo dijo el Estado hace muchos años en el 2005, que esto fue una masacre que fue perpetrada por las FARC sino que era un contingente del Ejército y de las Autodefensas Unidas de Colombia".

### **Siguen llegando los paramilitares a San José de Apartadó**

Los pobladores de esta comunidad de paz, que hace 22 años declaró su territorio libre de grupos armados, siguen pidiendo justicia por la masacre cometida en el 2005. Sin embargo, denuncian que la violencia sigue viva en esta región a raíz de incursiones paramilitares y la tolerancia del Ejército.

Tal como la comunidad lo venía denunciando desde el pasado 8 de mayo, el paramilitar Domingo Antonio Herrera conocido en la zona como "Chirri" amenazó a la comunidad con llegar acompañado de una comisión paramilitar a la vereda Resbalosa de San José de Apartadó y el pasado 11 de mayo, cumplió con su advertencia.

A través de Twitter, la comunidad anunció que llegaron ocho paramilitares, uniformados y armados con armas largas como militares, al sitio conocido como La Espabiladora de la vereda Resbalosa y que **las autoridades competentes hicieron "nada para controlar este fenómeno que cada día oprime más a nuestra región"**. Esto a pesar del número de miembros de Fuerza Pública que patrullan la zona.

Según el habitante de la comunidad, grupos paramilitares controlan las 32 veredas que conforman el corregimiento de San José de Apartadó, restringiendo la movilidad de los pobladores, controlando la siembra de pancoger y cobrando vacunas según la cantidad de animales que tenga el campesino o el negocio que  maneje. "**Hay algunas multas que están cobrando desde hasta 5 millones de pesos**", afirmó.

<iframe id="audio_35766598" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_35766598_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
