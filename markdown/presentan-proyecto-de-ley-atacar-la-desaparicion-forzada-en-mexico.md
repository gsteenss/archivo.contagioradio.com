Title: Presentan proyecto de ley contra la desaparición forzada en México
Date: 2015-08-19 17:21
Category: DDHH, El mundo, Movilización
Tags: 43 desaparecidos Ayotzinapa, Asesinatos de periodistas en México, Campaña Nacional contra la desaparición forzada, Comisión de Derechos Humanos del Senado de la República, Desaparición forzada en México, Nadin Reyes Maldonado
Slug: presentan-proyecto-de-ley-atacar-la-desaparicion-forzada-en-mexico
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/08/desaparicion_forzada_mexico_contagioradio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: laicismo.org 

###### [19 Ago 2015 ] 

La Campaña en Contra de la Desaparición Forzada en México presentó en Audiencia Pública, convocada por la C**omisión de Derechos Humanos del Senado de la República** de este país, una ponencia para impulsar una Ley General que prevenga, sancione, investigue y repare el delito de la desaparición forzada en México.

Nadin Reyes Maldonado, vocera de la campaña, afirma que esta iniciativa se viene trabajando desde el año 2010, siendo los hechos ocurridos en Ayotzinapa el detonante final para que tanto las autoridades como la sociedad civil, vieran la necesidad de aprobar una ley de esta envergadura. Intentos como este se ha querido desarrollar en otros momentos pero no se ha tenido el respaldo del poder legislativo.

El proyecto contempla:

### **Es necesario tipificar el delito de desaparición** 

Actualmente en México no existe una conceptualización apropiada de acuerdo a los marcos jurídicos internacionales. La iniciativa pretende que se apropie la definición elaborada por la CIDH, en la que se contempla la participación de agentes del Estado y con sanciones correspondientes con la gravedad de este delito.

### **Reclasificación del delito de desaparición forzada** 

Este delito no es, ni debe ser equiparable al secuestro. En el curso de las investigaciones, no debe existir la posibilidad de apelar a otro delito, cuando los indicios y la investigación adelantada denotan que se trata de desaparición forzada.

### **Consolidación de un registro nacional de desaparición forzada** 

Que permita saber cuantas personas han sido víctimas de este delito, y potenciar elementos que cualifiquen la investigación en la materia. Incluiría la creación de fiscalías autónomas en el país, así como la consolidación de mecanismos de coordinación a nivel nacional, para realizar un trabajo conjunto entre las entidades de gobierno y Estado para la búsqueda de personas reportadas como desaparecidas. Crear el Instituto Nacional Autónomo de Investigación Forense, para concretar el cotejo de cuerpos que aún no han sido identificados.

### **Garantías de no repetición** 

Que permita a los familiares de las víctimas conocer la verdad y acceder al derecho a la Justicia.

Según Nadin Reyes la aprobación de esta ley sería posible ahora, gracias a las reformas constitucionales promulgadas en el Congreso mexicano, en respuesta a la gravedad de los hechos de desaparición forzada de los **43 estudiantes en Ayotzinapa y que alertaron tanto a la sociedad civil mexicana** como a sus autoridades, sobre la necesidad de legislar al respecto.

Lo que es muestra de la actual voluntad política del Congreso, aun cuando exista una iniciativa de **ley propuesta por el actual presidente, en las que no se habla de desaparición forzada sino de personas no localizadas o extraviadas**, negando la existencia en el país de personas desaparecidas, que de ser aprobada violaría los derechos de las cerca de 66.000 víctimas de este delito, así como de sus familiares.
