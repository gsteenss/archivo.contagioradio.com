Title: Vía libre para la Consulta Popular Minera en Pijao
Date: 2016-10-13 16:00
Category: Ambiente y Sociedad, Nacional
Tags: consulta popular, defensa ambiental, empresas extractivas
Slug: via-libre-para-la-consulta-popular-minera-en-pijao
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/10/Pijao-9-Renunciamos-y-viajamos-e1476391724591.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Renunciamos y Viajamos] 

###### [13 Oct de 2016] 

El pasado martes, la Corte Constitucional emitió un fallo a favor de las comunidades, que **otorga potestad a los entes territoriales para decidir sobre el uso del suelo y el subsuelo de sus municipios.** Las comunidades de Pijao, Quindío, sentaron un precedente a nivel nacional sobre la defensa ambiental, que fue de gran importancia para la decisión tomada por la Corte.

En el año 2009 siendo concejal del municipio de Pijao, Quindío, Mónica Flores solicitó a la Agencia Nacional de Minería información acerca de los títulos mineros otorgados en este municipio. La ANM entrega un informe en el que **figuran 23 títulos otorgados, buena parte de ellos localizados en área protegida del páramo de Chilí. **Le puede interesar: [Minería en páramo de Santurbán no contó con estudios sobre impactos socio-ambientales](https://archivo.contagioradio.com/mineria-en-paramo-de-santurban-no-conto-con-estudios-sobre-impactos-socio-ambientales/)

Con base en dicha información, el Consejo municipal toma cartas en el asunto e invita a las empresas mineras y a los habitantes de las zonas afectadas, para tener un panorama más amplio de las afectaciones y tomar decisiones. Mónica Flores, ahora integrante del Comité Ambiental de Pijao y de la Fundación Pijao Cittaslow, señaló que después de esa reunión **iniciaron talleres de formación a líderes estudiantiles y comunitarios de distintas veredas.**

En el año 2014, campesinos, estudiantes y líderes comunitarios **deciden conformar un cabildo abierto, dentro de este espacio colectivo concluyen hacer una consulta popular.** Piden al Alcalde que solicite al Consejo de Estado realizar la consulta popular, que fue aprobada por el Consejo. Presentaron la pregunta ante el Tribunal Contencioso Administrativo y éste la denegó.

Flores, manifiesta que frente a ese panorama, la comunidad recogida en el Cabildo, acude al **centro de estudios jurídicos y sociales, Dejusticia, quienes desde entonces han acompañado el proceso.** En ese momento, Mónica Flores interpone una tutela en la que evidenció la violación del derecho a los ciudadanos de Pijao, a decidir sobre el uso de suelo de sus territorios.

El Consejo de Estado, en primera y segunda instancia, niega esta tutela, pero el trabajo de las comunidades no se detiene, continúan los talleres formativos sobre temas mineros y toman la consigna “Pijao libre de megaminería”. Flores manifiesta que el proceso desarrollado en Pijao, **“es un camino mas que se abre para otros territorios (…) sienta un precedente a nivel nacional y latinoamericano para la defensa de la vida en los territorios”. **Le puede interesar: [Consulta popular en ibagué busca frenar 35 títulos mineros](https://archivo.contagioradio.com/consulta-popular-en-ibague-busca-frenar-35-titulos-mineros/)

Según ésta integrante del Comité Ambiental, **las comunidades de Pijao, esperan poder realizar la Consulta Popular Minera en el primer semestre del 2018,** y durante estos dos años se quiere “hablar con el grupo de ciudadanos impulsores de la propuesta y las autoridades locales para volver a revitalizar los grupos de ciudadanos y replantear la pregunta”.

La Corte dice que las consultas mineras son legales, y que los entes territoriales pueden intervenir y frenar procesos mineros, pero “la sociedad civil empoderada es quien decide, son procesos largos pero hay que ser pacientes y educarse en temas de defensa ambiental” concluye Flores.

###### Reciba toda la información de Contagio Radio en su correo [[bit.ly/1nvAO4u]
