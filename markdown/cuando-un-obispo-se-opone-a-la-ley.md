Title: Cuando un obispo se opone a la Ley
Date: 2019-10-29 16:11
Author: CtgAdm
Category: Columnistas invitados, Opinion
Tags: aborto, Corte Consitucional, mujeres
Slug: cuando-un-obispo-se-opone-a-la-ley
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/unnamed.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<div>

[ Foto: Razón+Fe.]

 

En cumplimiento de una orden de la **Corte Constitucional**, el 24 de octubre el Ministro de Salud dio a conocer un borrador para la regulación del acceso a la interrupción voluntaria del embarazo en situaciones admitidas por la Ley.

##### Las voces en rechazo a la gestión del ministro Juan Pablo Uribe no se hicieron esperar. 

Juan Vicente Córdoba, obispo católico de Fontibón y presidente del Departamento de Vida de la Conferencia Episcopal de Colombia, afirmó a través de un comunicado difundido por la entidad que *“el aborto no es derecho fundamental, así la Corte lo afirme de manera irregular”.* El jesuita agregó que el **Ministerio de Salud** no cuenta con facultades para esa reglamentación, porque supuestamente el asunto es competencia del Congreso.

*“No se ven bien los representantes de la Iglesia católica pidiendo al Ministerio de Salud que viole la ley y, además, ayudando a esparcir desinformación para afectar los derechos de las mujeres colombianas”*, manifestó *El Espectador* en su editorial del 25 de octubre bajo el título *“La irresponsable presión de las iglesias contra el aborto”.*

El diario había informado sobre una carta del partido político cristiano Colombia Justa Libres al presidente Iván Duque, con la que le solicitaron *“ordenar al Ministerio de Salud inhibirse de expedir la resolución que sobre la interrupción voluntaria del embarazo pretende el mencionado despacho próximamente expedir”.*

##### Según *El Espectador,* en otras palabras, el partido le está diciendo al mandatario *“que rompa el equilibrio de poderes y desacate una sentencia judicial en firme”.* 

 

</div>

<div>

*“La falta de garantías a la intimidad de la mujer que decide abortar en el marco de causales despenalizadas, sumadas a otros obstáculos, hacen que las mujeres prefieran recurrir a instituciones clandestinas”,* manifestó el centro de investigación De Justicia a la periodista Catalina Oquendo, corresponsal en Colombia de *El País* de España.

</div>

 

<div>

Por “causa básica de embarazo terminado en aborto” al menos **828 mujeres** murieron en Colombia **entre 2005 y 2017,** según datos del Ministerio de Salud reunidos por la reportera para un artículo publicado en septiembre. Con cifras de la Fiscalía, la periodista añadió en su texto que en el mismo período **2.290 mujeres** fueron judicializadas; 499 de ellas con una edad entre 14 y 18 años; tres, con 11 y 12 años.

le puede interesar|[Mujeres en Colombia se suman para exigir aborto legal y seguro](https://archivo.contagioradio.com/mujeres-en-colombia-exigir-aborto-legar-y-seguro/)

</div>

 

<div>

Mientras mueren cientos de mujeres y otras tantas son criminalizadas por falta de la reglamentación, sigue la presión social sobre una población desprovista de garantías. No solo en las curias y en los púlpitos, también en las calles: con grupos religiosos que abordan en los andenes a las mujeres que acuden a instituciones como *Oriéntame* buscando asesoría. Pero para el obispo Juan Vicente Córdoba que eso ocurra no es objeto de crítica. Todo lo contrario: en su comunicado se manifiesta preocupado porque, supuestamente, se pretende impedir “la oración de personas que se reúnen frente a las clínicas abortistas para pedirle a Dios por el fin del aborto en Colombia y el mundo”.

#### ¿Más desinformación? 

A “el pueblo de Dios” le pide el obispo jesuita *“que manifieste públicamente su rechazo a esta reglamentación y utilice todos los medios a su alcance para expresar su opinión ante el Gobierno y la sociedad en general*”. ¿Pasando por encima de la ley, monseñor? ¿Desacatando el fallo de la Corte Constitucional? ¿Y si no todos los católicos están en contra de la medida?

*“Colombia es un Estado laico y la Iglesia puede opinar, pero no imponer una moral única al conjunto de la sociedad ni mucho menos negar los derechos de las mujeres”*, señalaron las activistas de Católicas por el Derecho a Decidir. *“No es justo con las mujeres que la jerarquía de la Iglesia imponga una mirada que rechaza, desconoce nuestros derechos y desprecia nuestra vida y dignidad”*, agregaron, sumándose a las críticas a la declaración difundida por la Conferencia Episcopal.

En víspera de elecciones, el  obispo católico de Cali, Darío de Jesús Monsalve, se preguntó si el debate sobre el aborto se estaba usando de nuevo “como oxígeno electoral a sectores extremistas, alineados con la opresión social y la violencia estructural”. Ya había ocurrido así antes del plebiscito sobre el Acuerdo de paz y durante la más reciente campaña presidencial en Colombia.

Así como no hay consenso en el catolicismo sobre la interrupción voluntaria del embarazo, tampoco lo hay en el resto de la sociedad colombiana. Con todo, la **Corte Constitucional,** entidad judicial encargada de velar por la integridad y la supremacía de la Ley , ya emitió su sentencia. Y ningún obispo, iglesia o partido político está por encima de la ley. ¿O sí?

*Miguel Estupiñán / [@HaciaElUmbral](https://twitter.com/HaciaElUmbral)*

</div>
