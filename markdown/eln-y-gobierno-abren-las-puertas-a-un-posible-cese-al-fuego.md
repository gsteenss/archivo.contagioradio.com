Title: ELN y Gobierno abren las puertas a un posible cese al fuego
Date: 2017-06-30 16:24
Category: Entrevistas, Paz
Tags: proceso de paz eln, Quito
Slug: eln-y-gobierno-abren-las-puertas-a-un-posible-cese-al-fuego
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/06/dialogo-eln-gobierno.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Cancillería Ecuador] 

###### [30 Jun 2017] 

Pablo Beltrán, integrante del equipo de conversaciones del ELN expresó que, culminado este segundo ciclo de diálogos con el gobierno Nacional, se podría llegar a **pactar un cese bilateral temporal antes de la llegada del Papa Francisco a Colombia** que iría acompañada de acciones humanitarias que estén en la vía de desescalonar el conflicto armado.

### **El cese bilateral y las condiciones que se necesitan** 

El cese bilateral al fuego, de acuerdo con Pablo Beltrán, se está gestionando en la mesa de conversaciones, sin embargo, para que este llegue a darse, Beltrán manifestó que **es necesario que haya un compromiso por parte del gobierno en la toma de acciones inmediatas que frenen el avance de los paramilitares en el territorio del país.**

“Debe haber un cese al fuego entre las partes que están armadas y a su vez acompañarlo de acciones humanitarias que **bajen la intensidad del conflicto y que favorezcan a la población y las comunidades**” manifestó Beltrán. Le puede interesar:["ELN está listo para firmar el cese al fuego bilateral"](https://archivo.contagioradio.com/42291/)

De igual forma señaló la posibilidad de** establecer una comisión técnica que se encargue de asuntos específicos,** uno de ellos es hacer seguimiento al cese bilateral si se llega a pactar.

### **Las acciones humanitarias** 

El segundo punto de la agenda de las conversaciones entre el ELN y el gobierno Nacional, es el desescalonamiento del conflicto armado a partir de acciones humanitarias, en este sentido Pablo Beltrán afirmó que **ambas partes están intentando establecer cuáles serían esas acciones que podrían gestar el inicio del cese bilateral.**

Pablo Beltrán aseguro que por ahora se está dialogando en la mesa sobre poner fin a la práctica de la retención, pero que aspiran a que el gobierno Nacional también tome medidas para** cesar las hostilidades que existen contra de los comunidades y líderes sociales**. Le puede interesar: ["Financiación, localización y verificación: puntos claves de un cese bilateral al fuego entre ELN y Gobierno"](https://archivo.contagioradio.com/cese-bilateral-eln/)

### **Los Acuerdos de Paz con las FARC-EP** 

En varias ocasiones el ELN ha manifestado su apoyo a los acuerdos de paz ya pactados entre el Gobierno Nacional y la guerrilla de las FARC-EP, sin embargo, han expresado su preocupación por los incumplimientos por parte del gobierno en puntos como la ley de Amnistías o la adecuación de las Zonas Verdales de Normalización.

El comandante de esta guerrilla aseveró frente a este punto, que debe haber una acción de respaldo por parte de la ciudadanía y el movimiento social a los acuerdos de paz con las FARC-EP “saludamos el paso que dan las FARC y su decisión política, **esperamos que puedan fortalecerse como organización y que todos los adversarios de estos procesos dejen de conspirar para acabar con esta guerrilla**”.

### **¿Elecciónes del 2018 serían el punto final para el proceso con el ELN?** 

Referente a las elecciones presidenciales que se darán en un año, Beltrán indicó que existe una necesidad por crear mayorías que respalden la continuación del proceso de paz con el ELN y agregó que de igual forma, **es necesario que exista un programa que unifique a los diferentes sectores que hacen parte de la izquierda** y así elegir a un representante que vaya a elecciones.

### **La participación de la sociedad en los diálogos ELN – Gobierno** 

Desde un principio esa guerrilla manifestó que la agenda de conversaciones giraría en torno a la participación social, Beltrán aseguró que ya se ha avanzado en términos del diseño de una propuesta que abriría la puerta a la participación social . Le puede interesar: ["Eln y Gobieron logran 3 acuerdos para continuar en la mesa de conversaciones de paz"](https://archivo.contagioradio.com/eln-y-gobierno-logran-3-acuerdos-para-continuar-en-la-mesa-de-conversaciones-de-paz/)

<iframe id="audio_19566281" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_19566281_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU).
