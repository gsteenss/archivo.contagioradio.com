Title: Fuertes disturbios en Kosovo por renacionalización de empresa metalúrgica
Date: 2015-01-28 06:21
Author: CtgAdm
Category: El mundo, Movilización
Tags: guerra de los balcanes, Kosovo, Serbia, Vetëvendosje
Slug: fuertes-disturbios-en-kosovo-por-renacionalizacion-de-empresa-metalurgica
Status: published

###### **Foto:Radio Serbia Internacional** 

Esta mañana se han presentado fuertes disturbios en Kosovo, en concreto en su capital Prístina, donde ha sido detenido el alcalde de la misma, conocido como **Shpend Ahmeti y perteneciente al movimiento Vetëvendosje (autodeterminación).**

Los enfrentamientos protagonizados por trabajadores de la empresa metalúrgica Trepça y miembros del movimiento político conocido como **Vetëvendosje han dejado un saldo de 27 manifestantes detenidos y 20 heridos entre policías y manifestantes.**

Las causas de las protestas son la renacionalización por parte de Kosovo de la empresa metalúrgica actualmente en manos de la administración serbia y de la cual hacen parte 3.000 trabajadores kosovares y dependen 20.000 miembros de sus familias.

La empresa esta situada al norte donde conviven serbokosovares en Mitohia, capital Mitrovica en una comunidad similar a un gueto, donde los moradores se declaran contrarios a la independéncia.

En el fondo de la cuestión está el recelo de que la administración Kosovar privatice la empresa lo que conllevaría a un reajuste de los empleados, es decir despidos y en un futuro posiblemente su cierre definitivo.

Es importante puntualizar que el movimiento " **Vetëvendosje" **que en español significa autodeterminación, es contrario a la injerencia extranjera, a la negociación del estatuto Kosovar con su vecina Serbia y en contra de las políticas neoliberales, es decir un movimiento distinto a los ya existentes alineados todos en posturas nacionalistas y claramente de izquierdas
