Title: El precio de entrar a la OECD
Date: 2017-04-25 09:33
Category: invitado, Opinion
Tags: Medicamentos, OECD
Slug: el-precio-de-entrar-a-la-oecd
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/04/oecd.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: OECD 

###### **25 Abr 2017 ** 

#### [**Francisco Rossi - Misión Salud **] 

El Presidente de la República anunció un paquete de medidas de lucha contra la corrupción, que entre otras incluye la reglamentación del ejercicio del lobby.  Interesante. Ese fue el primer  anuncio del ex Presidente Obama en su primer período presidencial. Sin resultado en Estados Unidos.  Y sin muchas perspectivas en Colombia por lo que estamos viendo en el proceso de ingreso de Colombia a la OECD.

Seamos claros;  la forma más costosa de la corrupción en el mundo, es la que consigue que se promulguen leyes, decretos y reglamentos para que, de manera legal pero ilegítima, todos los ciudadanos hagamos un aporte de nuestros cada vez más escasos ingresos, para el enriquecimiento de unas pocas empresas. Lo que les permite destinar enormes sumas, no a los sobornos en dinero, sino a otros sobornos que no son ilegales, especialmente la captura de funcionarios y legisladores que se benefician de muchas maneras de su conveniente miopía.

Tal y como lo ha explicado y denunciado OXFAM en sus informes sobre la concentración de la riqueza en el mundo en los últimos 5 años, en lo que representa el rasgo más obsceno de las democracias de mercado, 2 sectores son los que más han conseguido que gobiernos y legisladores produzcan regulaciones en su beneficio: el sector financiero y la industria farmacéutica y de salud.

El Presidente Santos quiere entrar a la OECD.  Pero por razones que preferimos no esculcar, el gremio de las farmacéuticas multinacionales en Colombia, AFIDRO, parece haber conseguido el derecho a ponerle al gobierno condiciones, que se van a traducir en que cada ciudadano colombiano va a tener que aportar de su bolsillo, para seguir pagando los escandalosos precios de los medicamentos.

AFIDRO ha hecho pública una carta muy arrogante, en la que, además de dirigirse a la Presidencia desde una posición de poder y de faltarle al respeto a un Ministro de Estado , le impone unas condiciones para que Colombia pueda ingresar a la OECD. Y amenaza. Juzguen ustedes: <https://www.afidro.org/wp-content/uploads/Carta-radicada-en-Presidencia-28-de-febrero-de-2017.pdf>

El Ministerio de Salud expidió una declaratoria de Interés público para el imatinib, en uno de los más sonados casos de intervención del gobierno para regular los abusivos precios de los medicamentos. Abusivos a juicio de todos los periodistas y académicos que han trabajado en este asunto en los últimos 10 años.  Abusivos porque la regulación lo único que hizo fue reducir los precios para que los colombianos no pagáramos más que los alemanes, los suizos o los noruegos por los mismos medicamentos. Recordemos  que, después de la regulación de los precios de más de 600 medicamentos, el Ministerio de salud estima que se han conseguido ahorros por cerca de medio millón de millones de pesos anuales, lo que en los últimos 4 años significa un par de billones de pesos. Un par de billones de pesos que, antes de la regulación, se fueron a las arcas de las multinacionales farmacéuticas. Legalmente.

Ahora, AFIDRO le exige al gobierno que para poder entrar a la OECD, legisle a su conveniencia. Dé marcha atrás en las decisiones para reducir el gasto en medicamentos. Desautorice a un Ministro de Estado.

Ya los colombianos experimentamos un primer incremento en los aportes a la salud y a las pensiones. Habrá más. El precio de entrar a la OECD. Parece que al Gobiernos Santos si le gusta lo que no le gusta a OXFAM; gobernar para las élites.  Así, la lucha contra la corrupción se concentrará en evitar los colados en transmilenio y en “sorprender” a los policías de tránsito que reciban coimas.

------------------------------------------------------------------------

###### Las opiniones expresadas en este artículo son de exclusiva responsabilidad del autor y no necesariamente representan la opinión de  Contagio Radio 
