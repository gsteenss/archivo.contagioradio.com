Title: Ambiente
Date: 2014-11-25 15:43
Author: AdminContagio
Slug: ambiente
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/12/AMBIENTE-.png" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/09/ambiente-y-comunidades-e1473705635492.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/02/ambiente.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

#### Programa:

### [VOCES DE LA TIERRA](https://archivo.contagioradio.com/programas/onda-animalista/)

#### Todos los martes desde las 11 a.m

[![Francisco Ceballos, el ciudadano que defiende la Serranía de los Paraguas](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/03/Captura-de-pantalla-35-310x310.png "Francisco Ceballos, el ciudadano que defiende la Serranía de los Paraguas"){width="310" height="310" sizes="(max-width: 310px) 100vw, 310px" srcset="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/03/Captura-de-pantalla-35-310x310.png 310w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/03/Captura-de-pantalla-35-150x150.png 150w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/03/Captura-de-pantalla-35-230x230.png 230w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/03/Captura-de-pantalla-35-360x360.png 360w"}](https://archivo.contagioradio.com/francisco-ceballos-ciudadano-defiende-la-serrania-los-paraguas/)  

###### [Francisco Ceballos, el ciudadano que defiende la Serranía de los Paraguas](https://archivo.contagioradio.com/francisco-ceballos-ciudadano-defiende-la-serrania-los-paraguas/)

[<time datetime="2019-03-07T18:04:15+00:00" title="2019-03-07T18:04:15+00:00">marzo 7, 2019</time>](https://archivo.contagioradio.com/2019/03/07/)  
[![Desalojan familias afectadas por Hidroituango de albergues de EPM](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/05/DdstP13V4AM4wWu-310x310.jpg "Desalojan familias afectadas por Hidroituango de albergues de EPM"){width="310" height="310" sizes="(max-width: 310px) 100vw, 310px" srcset="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/05/DdstP13V4AM4wWu-310x310.jpg 310w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/05/DdstP13V4AM4wWu-150x150.jpg 150w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/05/DdstP13V4AM4wWu-512x512.jpg 512w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/05/DdstP13V4AM4wWu-230x230.jpg 230w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/05/DdstP13V4AM4wWu-360x360.jpg 360w"}](https://archivo.contagioradio.com/desalojan-familias-afectadas-hidroituango-albergues-epm/)  

###### [Desalojan familias afectadas por Hidroituango de albergues de EPM](https://archivo.contagioradio.com/desalojan-familias-afectadas-hidroituango-albergues-epm/)

[<time datetime="2019-03-01T15:08:47+00:00" title="2019-03-01T15:08:47+00:00">marzo 1, 2019</time>](https://archivo.contagioradio.com/2019/03/01/)  
[![Incendios en la Sierra Nevada han afectado a más de 70 familias Arhuacas](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/02/Captura-de-pantalla-32-310x310.png "Incendios en la Sierra Nevada han afectado a más de 70 familias Arhuacas"){width="310" height="310" sizes="(max-width: 310px) 100vw, 310px" srcset="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/02/Captura-de-pantalla-32-310x310.png 310w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/02/Captura-de-pantalla-32-150x150.png 150w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/02/Captura-de-pantalla-32-230x230.png 230w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/02/Captura-de-pantalla-32-360x360.png 360w"}](https://archivo.contagioradio.com/incendios-la-sierra-nevada-afectado-mas-70-familias-arhuacas/)  

###### [Incendios en la Sierra Nevada han afectado a más de 70 familias Arhuacas](https://archivo.contagioradio.com/incendios-la-sierra-nevada-afectado-mas-70-familias-arhuacas/)

[<time datetime="2019-02-28T12:53:18+00:00" title="2019-02-28T12:53:18+00:00">febrero 28, 2019</time>](https://archivo.contagioradio.com/2019/02/28/)  
[![El Plan Nacional de Desarrollo, un respaldo al sector minero-energético](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/12/minería-310x310.jpg "El Plan Nacional de Desarrollo, un respaldo al sector minero-energético"){width="310" height="310" sizes="(max-width: 310px) 100vw, 310px" srcset="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/12/minería-310x310.jpg 310w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/12/minería-150x150.jpg 150w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/12/minería-512x512.jpg 512w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/12/minería-230x230.jpg 230w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/12/minería-360x360.jpg 360w"}](https://archivo.contagioradio.com/plan-nacional-desarrollo-respaldo-al-sector-minero-energetico/)  

###### [El Plan Nacional de Desarrollo, un respaldo al sector minero-energético](https://archivo.contagioradio.com/plan-nacional-desarrollo-respaldo-al-sector-minero-energetico/)

[<time datetime="2019-02-26T17:35:48+00:00" title="2019-02-26T17:35:48+00:00">febrero 26, 2019</time>](https://archivo.contagioradio.com/2019/02/26/)  
[![Minería podría estar detrás de la emergencia de inundaciones en Chocó](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/02/Captura-de-pantalla-28-310x310.png "Minería podría estar detrás de la emergencia de inundaciones en Chocó"){width="310" height="310" sizes="(max-width: 310px) 100vw, 310px" srcset="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/02/Captura-de-pantalla-28-310x310.png 310w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/02/Captura-de-pantalla-28-150x150.png 150w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/02/Captura-de-pantalla-28-512x512.png 512w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/02/Captura-de-pantalla-28-230x230.png 230w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/02/Captura-de-pantalla-28-360x360.png 360w"}](https://archivo.contagioradio.com/mineria-podria-estar-detras-la-emergencia-inundaciones-choco/)  

###### [Minería podría estar detrás de la emergencia de inundaciones en Chocó](https://archivo.contagioradio.com/mineria-podria-estar-detras-la-emergencia-inundaciones-choco/)

[<time datetime="2019-02-25T13:02:11+00:00" title="2019-02-25T13:02:11+00:00">febrero 25, 2019</time>](https://archivo.contagioradio.com/2019/02/25/)  
[![Comunidades de la Guajira demandaron a Cerrejón y piden anular licencia ambiental](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/11/el-cerrejon-310x310.jpg "Comunidades de la Guajira demandaron a Cerrejón y piden anular licencia ambiental"){width="310" height="310" sizes="(max-width: 310px) 100vw, 310px" srcset="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/11/el-cerrejon-310x310.jpg 310w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/11/el-cerrejon-150x150.jpg 150w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/11/el-cerrejon-512x512.jpg 512w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/11/el-cerrejon-230x230.jpg 230w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/11/el-cerrejon-360x360.jpg 360w"}](https://archivo.contagioradio.com/comunidades-la-guajira-presentan-demanda-cerrejon-estado/)  

###### [Comunidades de la Guajira demandaron a Cerrejón y piden anular licencia ambiental](https://archivo.contagioradio.com/comunidades-la-guajira-presentan-demanda-cerrejon-estado/)

[<time datetime="2019-02-23T19:25:25+00:00" title="2019-02-23T19:25:25+00:00">febrero 23, 2019</time>](https://archivo.contagioradio.com/2019/02/23/)  
[![Comunidades afectadas por Hidroituango siguen sin respuesta del Estado](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/02/Captura-de-pantalla-25-770x400-310x310.png "Comunidades afectadas por Hidroituango siguen sin respuesta del Estado"){width="310" height="310" sizes="(max-width: 310px) 100vw, 310px" srcset="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/02/Captura-de-pantalla-25-770x400-310x310.png 310w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/02/Captura-de-pantalla-25-770x400-150x150.png 150w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/02/Captura-de-pantalla-25-770x400-230x230.png 230w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/02/Captura-de-pantalla-25-770x400-360x360.png 360w"}](https://archivo.contagioradio.com/comunidades-afectadas-hidroituango-siguen-sin-respuesta-del-estado/)  

###### [Comunidades afectadas por Hidroituango siguen sin respuesta del Estado](https://archivo.contagioradio.com/comunidades-afectadas-hidroituango-siguen-sin-respuesta-del-estado/)

[<time datetime="2019-02-20T15:45:52+00:00" title="2019-02-20T15:45:52+00:00">febrero 20, 2019</time>](https://archivo.contagioradio.com/2019/02/20/)  
[![Peñalosa no atiende recomendaciones académicas sobre calidad del aire en Bogotá](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/02/bogota-2754208_1920-770x400-310x310.jpg "Peñalosa no atiende recomendaciones académicas sobre calidad del aire en Bogotá"){width="310" height="310" sizes="(max-width: 310px) 100vw, 310px" srcset="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/02/bogota-2754208_1920-770x400-310x310.jpg 310w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/02/bogota-2754208_1920-770x400-150x150.jpg 150w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/02/bogota-2754208_1920-770x400-230x230.jpg 230w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/02/bogota-2754208_1920-770x400-360x360.jpg 360w"}](https://archivo.contagioradio.com/penalosa-no-atiende-recomendaciones-academicas-calidad-del-aire-bogota/)  

###### [Peñalosa no atiende recomendaciones académicas sobre calidad del aire en Bogotá](https://archivo.contagioradio.com/penalosa-no-atiende-recomendaciones-academicas-calidad-del-aire-bogota/)

[<time datetime="2019-02-19T14:37:21+00:00" title="2019-02-19T14:37:21+00:00">febrero 19, 2019</time>](https://archivo.contagioradio.com/2019/02/19/)

[![consultas-populares](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2011/09/consultas-populares.png){width="1600" height="244"}](http://bit.ly/2vwVIpg%20)
