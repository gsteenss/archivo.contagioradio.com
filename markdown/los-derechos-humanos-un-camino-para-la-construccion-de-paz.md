Title: Los derechos humanos un camino para la construcción de Paz
Date: 2016-09-09 16:34
Category: Nacional, Paz
Tags: 9 Abril colombia se mueve por la paz, Conversaciones de paz en Colombia, Diálogos de paz Colombia, Diálogos Gobierno - FARC
Slug: los-derechos-humanos-un-camino-para-la-construccion-de-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/08/Colombia-celebra-acuerdo-FARC_LNCIMA20160623_0146_5.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: La Nación] 

<iframe src="https://co.ivoox.com/es/player_ek_12858906_2_1.html?data=kpell52ddJehhpywj5WYaZS1kpuah5yncZOhhpywj5WRaZi3jpWah5yncaXZ08rQytTXb8npzsbb0diPvYzX0NPg1tfZp8Tdhqigh6eXsozYxpDdw9-RaZi3jqjc0NnFq8rjjLfOxs7Tb46ZmKialg..&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [9 Sept 2016] 

[Pese a que cada vez Colombia se acerca más a un acuerdo final de paz entre el gobierno y la guerrilla de las FARC, los defensores de derechos humanos continúan siendo blanco de amenazas y asesinatos. Las cifras este año van en aumento en relación al mismo periodo de 2015, registrándose un total **de 314 agresiones, entre ellas 36 asesinatos, como lo evidencia el más reciente informe del programa somos defensores**. ]

[Sin embargo, diversos sectores sociales, movimientos campesinos, agremiaciones sindicales, organizaciones estudiantiles y colectivos barriales, ven los acuerdos de paz pactados entre el Gobierno colombiano y la guerrilla de las FARC como un escenario posible para que se cumplan las demandas de mejoramiento de las condiciones de salud, educación, trabajo y vivienda, y que a su vez, las movilizaciones sociales y **la defensa de los derechos humanos dejen de ser objeto de estigmatización y represión.  **]

[Y es que un primer paso, para que se dé este nuevo escenario, es respaldar los acuerdos de paz votando sí en el plebiscito, porque si bien el fin del conflicto armado con las FARC no resuelve los problemas de desempleo, informalidad laboral, bajos ingresos, hambre, desnutrición infantil, delincuencia y desigualdad, **estamos ad portas de cerrar la fábrica de víctimas en que se ha convertido Colombia y reconstruir el país que todos y todas nos merecemos. **]

###### [Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)]] 
