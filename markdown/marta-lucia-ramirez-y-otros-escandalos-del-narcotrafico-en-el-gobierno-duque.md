Title: Marta Lucia Ramírez y otros escándalos del narcotráfico en el gobierno Duque
Date: 2020-06-16 22:13
Author: PracticasCR
Category: Actualidad, Nacional
Tags: Bernardo Ramírez, Iván Duque, Marta Lucia Ramírez, Ñeñe Hernández
Slug: marta-lucia-ramirez-y-otros-escandalos-del-narcotrafico-en-el-gobierno-duque
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/06/IVAN-DUQUE-Y-MARTA-LUCIA-11-1068x712-640x427-1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: Marta Lucia Ramírez & Iván Duque / Galeón San José

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"left"} -->

Este jueves se dio a conocer a la opinión pública **el pago que realizó la vicepresidenta de Colombia Marta Lucia Ramírez por 150 mil dólares como fianza para liberar  a su hermano Bernardo Ramírez Blanco** quien para ese momento era investigado y posteriormente condenado a cuatro años de prisión por el delito de narcotráfico, fallo emitido por la Corte Federal del Distrito Sur de Florida en EE.UU.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Este pago lo realizó en compañía de su esposo **Álvaro Rincón sobre quien también han recaído recientes cuestionamientos por sus presuntos nexos comerciales con el narcotraficante Guillermo León Acevedo Giraldo, alias “Memo Fantasma”.**  

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Esta revelación es **la tercera implicación grave del gobierno de Iván Duque o su gabinete con el narcotráfico en lo corrido del 2020.**

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

En febrero de este año, agentes de **Policía Judicial localizaron un centro de procesamiento de coca en una finca de propiedad del entonces embajador de Colombia en Uruguay, Fernando Sanclemente.** La Fiscalía General reveló posteriormente que en el predio se encontraron 3 laboratorios con capacidad para producir cerca de dos **toneladas de cocaína al mes.**

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

El embajador **Sanclemente, ante su vinculación formal al proceso por parte de la Fiscalía, decidió pasar su carta de renuncia al cargo como diplomático, la cual fue aceptada por el Presidente Duque.**

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Poco después, en marzo, fueron filtradas las transcripciones de los audios del «Neñe» Hernández —que según el periodista e investigador Julián Martínez era el brazo político de una organización criminal dedicada al narcotráfico encabezada por «Marquitos Figueroa»— donde se hablaba de una **presunta financiación ilegal y compra de votos en la campaña presidencial del entonces candidato Iván Duque.**

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

En las conversaciones de estos audios también fue implicada la asesora de la Unidad de Trabajo Legislativo (UTL) del senador Álvaro Uribe Vélez, María Claudia «Caya» Daza  quien al ser relacionada en el caso también decidió renunciar a su cargo. (Le puede interesar: [El 'Ñeñe', Sanclemente y la degradación institucional en Colombia](https://archivo.contagioradio.com/el-nene-sanclemente-y-la-degradacion-institucional-en-colombia/))

<!-- /wp:paragraph -->

<!-- wp:heading {"textColor":"vivid-cyan-blue"} -->

[Política antidroga y erradicación forzada]{.has-inline-color .has-very-dark-gray-color} {#política-antidroga-y-erradicación-forzada .has-vivid-cyan-blue-color .has-text-color}
----------------------------------------------------------------------------------------

<!-- /wp:heading -->

<!-- wp:paragraph -->

Paradójicamente a los cuestionamientos en contra de su campaña y del actuar de algunos de los miembros de su gabinete, el presidente Iván Duque ha sido enfático en hablar de una [«lucha frontal»](https://www.facebook.com/watch/?v=523392905223802) que se adelanta contra el narcotráfico desde su Gobierno.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

En desarrollo de esta, **Duque ha insistido en retomar la aspersión con glifosato y ha arreciado en las labores de erradicación forzada de cultivos en los territorios por parte de las Fuerzas Militares.** Según Organizaciones como la [CCEEU](https://coeuropa.org.co/158/): «L**a represión en contra de la población campesina que subsiste del cultivo de hoja de coca ha llevado a la ejecución extrajudicial de al menos cuatro líderes campesinos e indígenas».** (Le puede interesar: [Ejército y narcotráfico: verdugos de la sustitución de la Coca](https://archivo.contagioradio.com/ejercito-y-narcotrafico-verdugos-de-la-sustitucion-de-la-coca/))

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->
