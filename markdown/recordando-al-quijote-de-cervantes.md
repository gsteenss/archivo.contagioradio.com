Title: Recordando al Quijote de Cervantes
Date: 2015-09-29 12:11
Category: Viaje Literario
Tags: 468 años del nacimiento de Cervantes, Don Quijote de la mancha, Miguel de Cervantes Saavedra
Slug: recordando-al-quijote-de-cervantes
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/don_quijote_7120-e1443546413882.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Ilustración: Eduardo Barcia Sánchez] 

###### <iframe src="http://www.ivoox.com/player_ek_8685539_2_1.html?data=mZull5qXfY6ZmKiakpeJd6KpkZKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRic2fqtHi1dnWqYy8ysnOzszTaZO3jLLWydrJsIzYxpCwx9fapc%2Foxtiah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Don Quijote de la Mancha (fragmento)] 

###### [29 Sep 2015]

Nacido en Alcalá de Henares, el 29 de Septiembre 1547, Miguel de Cervantes Saavedra fue un escritor español, autor de Don Quijote de la Mancha (1605 y 1615), obra cumbre de la literatura universal.

La vida de Cervantes fue una ininterrumpida serie de pequeños fracasos domésticos y profesionales, en la que no faltó ni el cautiverio, ni la injusta cárcel, ni la afrenta pública. No sólo no contaba con renta, sino que le costaba atraerse los favores de mecenas o protectores; a ello se sumó una particular mala fortuna que lo persiguió durante toda su vida.

Sólo al final, tras el éxito de las dos partes del Quijote, conoció cierta tranquilidad y pudo gozar del reconocimiento hacia su obra, pero siempre agobiado por las penurias económicas.

Entre el 22 y el 23 de abril de 1616 murió en su casa de Madrid, asistido por su esposa y una de sus sobrinas; envuelto en su hábito franciscano y con el rostro sin cubrir, fue enterrado en el convento de las trinitarias descalzas, en la entonces llamada calle de Cantarranas. Hoy se desconoce la localización exacta de su tumba.

Compartimos con ustedes un fragmento del Ilustre Hidalgo Don Quijote de la Mancha.
