Title: Podrían eliminar el servicio militar obligatorio en Colombia
Date: 2015-08-26 17:30
Category: Nacional, Paz
Tags: alirio uribe, Conversaciones de paz de la habana, ELN, FARC, Iván Cepeda, Juan Manuel Santos, paz, Victor Correa Vélez
Slug: podrian-eliminar-el-servicio-militar-obligatorio-en-colombia
Status: published

###### Foto: confidencialcolombia 

###### [26 Ago 2015]

La mañana de este miércoles un grupo de congresistas liderados por Iván Cepeda del Polo Democrático, presentaron un proyecto de acto legislativo que pretende reformar los artículos que reglamentan el servicio militar obligatorio. **La medida incluye el cambio del artículo 22 y el artículo 216 de la constitución política y establece la posibilidad de implementar el servicio social para la paz.**

Así como los congresistas del Polo Democrático en la construcción y formulación de la inciativa también participan diversas organizaciones sociales, de Derechos Humanos y de jóvenes que objetan conciencia como Tejido Juvenil Nacional Transformando a la Sociedad Tejuntas, la Asamblea Nacional de Objetores de Conciencia, Colectivo de investigación La Tulpa, Amaranto, Escuela Libertaria por la Educación Popular, Disentir, Alborada, entre otras.
