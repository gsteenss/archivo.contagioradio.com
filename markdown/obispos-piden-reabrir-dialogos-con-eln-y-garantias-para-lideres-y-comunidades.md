Title: Obispos piden reabrir diálogos con ELN y garantías para líderes y comunidades
Date: 2020-01-11 13:41
Author: CtgAdm
Category: Nacional, Paz
Tags: acuerdo de paz, Bojaya, Chocó, iglesia católica, Negociaciones con el ELN
Slug: obispos-piden-reabrir-dialogos-con-eln-y-garantias-para-lideres-y-comunidades
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/09/Chocó.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:] 

A través de un comunicado público firmado por diez obispos de Colombia, los prelados pidieron al gobierno colombiano que se atienda la emergencia humanitaria generada por el reiterado asesinato de líderes sociales, la connivencia de las FF.MM. con organizaciones criminales denunciada por diversos sectores sociales y la falta de garantías de vida digna para las comunidades en Colombia.

**El comunicado firmado por el Arzobispo de Cali, el Obispo de Quibdó, el Obispo de Apartadó, el obispo de Buenaventura, el obispo de Tumaco, el obispo de Popayán, el obispo de Itsmina – Tadó, el obispo de Mocoa, el obispo de Ipiales y el de Palmira,** aboga por la necesidad de retomar los diálogos con el ELN y buscar salidas para el acogimiento a la justicia de organizaciones criminales o de tipo paramilitar que operan en amplias regiones del país.

Estas mismas solicitudes y la de facilitar acuerdos humanitarios las realizaron más de 60 comunidades negras, indígenas y mestizas de varias regiones del país en el pasado mes de Noviembre en el marco de las movilizaciones de fin de año en el Paro Nacional y han sido reiteradas desde hace cerca de un año como salidas a la grave situación de DDHH que vive Colombia.

> Comunicado de los obispos del Pacífico y Suroccidente colombiano, sobre intensificación del conflicto armado. [pic.twitter.com/0lmAfieW3r](https://t.co/0lmAfieW3r)
>
> — Arquidiócesis Cali (@Arqui\_Cali) [January 11, 2020](https://twitter.com/Arqui_Cali/status/1216007080269766656?ref_src=twsrc%5Etfw)

<p>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
</p>
 

### Presidente Duque visita Bojayá y comunidad reitera denuncias y exigencias

Desde la denuncia de la ola de presencia y control paramilitar en Bojayá, Bajo y Medio Atrato y Dabeiba realizadas por organizaciones de DD.HH. como la Comisión de Justicia y Paz, diversos líderes y organizaciones han reiterado al Gobierno que **la salida a esta grave situación está en el respeto a los acuerdos de paz con FARC, el restablecimiento de la mesa con el ELN y las garantías de vida digna y cuidado y respeto territorial por parte de las FF.MM. y el Estado.**

En ese mismo sentido las personas que pudieron hacer presencia en Bojayá para la visita del presidente Iván Duque reiteraron las peticiones e hicieron énfasis en que el Gobierno ya conoce la situación, sabe de las denuncias y debe aplicar las medidas necesarias para resolver los problemas. [(Lea también: Obispos del Pacífico piden a candidatos atender crisis humanitaria)](https://archivo.contagioradio.com/obispos-pacifico-candidatos-crisis-humanitaria/)

Hasta el momento se desconocen las acciones del gobierno además de la militarización de los territorios que ha demostrado ser una medida ineficaz en contra del paramilitarismo y el narcotráfico que abundan en las regiones en las que se presentan los asesinatos de los y las líderes sociales.

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
