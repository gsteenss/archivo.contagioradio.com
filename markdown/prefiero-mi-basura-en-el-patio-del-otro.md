Title: Prefiero mi basura en el patio del otro
Date: 2017-08-16 20:19
Category: Opinion
Tags: Basuras en bogota, Basurero doña juana, Bogotá
Slug: prefiero-mi-basura-en-el-patio-del-otro
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/08/relleno-doña-juana-6.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Contagio Radio 

###### 16 Ago 2017 

#### **Por @KinEscribe** 

“Quienes protestan deberían pensar en los demás”, “van a llenar a Bogotá de basura por culpa de unos cuantos”, “el derecho a la protesta no puede pasar por encima de mis derechos” son algunas de las frases que, palabras más, palabras menos, se pueden encontrar por miles en la discusión que se abre por las protestas de los campesinos vecinos y vecinas del Botadero de Doña Juana, lastimosa discusión que deja ver algunos de los problemas de fondo en materia de salubridad y en materia de derechos de todos y todas.

Por una parte, el argumento de que la protesta no puede pasar por encima de los derechos de otros es casi una idea instalada desde la lógica del enemigo interno muy bien apropiada por las instituciones militares y estatales que no tardan en afirmar que se está bloqueando una vía para justificar la aplicación de la fuerza, muchas veces desmedida contra cualquier manifestación social. Tampoco faltan los ciudadanos que justifican dicha represión con ese mismo argumento.

Lastimosamente se queda corta la discusión sobre el tema, que bien podría centrarse en cómo hacer el manejo de las basuras que producimos en nuestras casas, escuelas y oficinas, o que podría llegar hasta la pregunta de en dónde se invierten los 4.100 millones de pesos que entrega el distrito para el tratamiento de las basuras.

Y es que en torno al manejo de las basuras de la capital las propuestas no han faltado y las han desarrollado los propios habitantes del sector con sus propios recursos. Una de ellas es que en cinco años es posible pagar los US\$1200 millones que costaría una planta para la gasificación de residuos sólidos y producir derivados del petróleo como el etanol y sustituir el enterramiento de la basura, otra es la termólisis para generar gas y mover turbinas para la producción de electricidad o incluso la construcción de biodigestores.

Todas estas propuestas, que ya fueron estudiadas y presentadas en la mesa de trabajo con el distrito, a corto plazo generarían un ahorro en costos ambientales, sociales y económicos a la capital, sin embargo, parece que han sido desechadas por los gobiernos distritales. ¿Por qué? ¿Acaso son los 5 billones de pesos que cuesta la licitación para las empresas recolectoras?

¿Será acaso por la influencia empresarial del Grupo Ethuss con el cual hoy por hoy, William Velez Sierra también maneja el negocio de las basuras en Cali, Barranquilla, Santa Marta, Valledupar, Riohacha, Maicao, Pereira, Ibagué y una decena de ciudades más según una denuncia publicada en el portal las dos orillas? ¿O mantener el negocio de las basuras de Bogotá en manos de Aseo Capital, LIME y Ciudad Limpia?

En todo caso la discusión termina desviada y la indignación ciudadana termina conducida por matrices de opinión impulsadas por los medios de información que puede que también tengan vínculos de amistad o financieros con los dueños de los conglomerados económicos a quienes no les interesa hablar de temas un poco más profundos.

En todo caso también, la salida que se plantea es que el operados del relleno cobre 900 pesitos más en las facturas para alcanzar a hacer el tratamiento de las basuras tal y como está o fumigar con sustancias que pueden ser más nocivas para la salud que los fétidos olores que invaden el aire del sur occidente de la capital.

Tal vez por eso, se hace necesario escuchar más a las personas que a los grandes medios de información y pellizcarse un poquito cuando a través de las pantallas del televisor se afirma que unos cuantos manifestantes violentos son los causantes de los grandes problemas del país.
