Title: Empresas serían investigadas por financiación de la guerra en el Tribunal Especial de Paz
Date: 2016-08-31 17:39
Category: Nacional, Paz
Tags: FARC, Gobierno, Paramilitarismo, proceso de paz, tribunal especial de paz
Slug: empresas-serian-investigadas-por-financiacion-de-la-guerra-en-el-tribunal-especial-de-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/08/paramilitares-e1472682962275.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Zona Cero 

###### [31 Ago 2016] 

Un informe de la sala de Justicia y Paz del Tribunal Superior de Bogotá, señala a más de **50 empresas por su presunta colaboración a grupos ilegales durante el conflicto armado.** Varias de ellas han sido mencionadas por paramilitares en distintas versiones en medio de los procesos judiciales por masacres, tortura, desaparición y desplazamiento forzado entre otros crímenes.

Según lo que se ha conocido, dichas empresas serían juzgados como terceros en la jurisdicción especial para la Paz, que resolvería si hubo o no responsabilidad directa, es decir si fueron **obligados a pagar “vacunas” o si por el contrario aportaron voluntariamente a los grupos en el marco del conflicto armado.** En el primer caso serían eximidas de responsabilidad penal y cesarían las investigaciones o los demás procedimientos de ley.

Dentro del grupo de 57 Compañías a**lgunas son señaladas de colaborar de manera voluntaria con el paramilitarismo y otro grupo de aquellas que se les endilga haberse beneficiado de su accionar criminal,** en cualquiera de los casos, serán las salas del tribunal especial de paz y sus magistrados los que definan si hay o no responsabilidad penal y además impondría las sanciones correspondientes según el delito que hayan cometido.

La lista revelada preliminarmente está conformada por 57 empresas del orden internacional, nacional y regional

1.  Postobón
2.  Chiquita Brands
3.  Ecopetrol
4.  Drummond Company
5.  Coca Cola
6.  Argos
7.  Ingenio San Carlos
8.  Indupalma
9.  Leonisa
10. Coltejer
11. Cadenalco
12. Probán
13. Bagatela S.A.
14. Agrícola Río Verde
15. Unibán S.A.
16. Sunisa S.A.
17. Tropical S.A.
18. Conserva S.A.
19. Banafrut
20. Banacol
21. Termotajen
22. Envigado Fútbol Club
23. Termotasajero
24. Cootranscúcuta Ltda
25. Norgas
26. Carbones la Mirla
27. Arrocera Galves
28. Ferretería el Palustre
29. Inducarga
30. Colminas
31. Cartagas
32. Intergas
33. Maderas del Darién
34. Sociedad de Agricultores y Ganaderos de Nariños (SAGAN)
35. Dirección Marítima y Portuaria de Tumaco
36. El Castillo de la Ropa (Ipiales)
37. Lácteos Andinos de Nariño
38. Cementos Diamante
39. Gas de Urabá
40. Fondo Ganadero del Tolima
41. Fondo Ganadero del Cesar
42. Fondo Ganadero de Córdoba
43. Centrales Eléctricas de Norte de Santander S.A.
44. Cenabastos de Cúcuta
45. Aguas Mansas y Vigilar Asociados
46. Suganar S.A.
47. Miro Seguridad (Medellín)
48. Servicentro ESSO Las Vegas
49. Estación de Servicio San Rafael
50. Pinturas El Cóndor
51. Transportes Botero Soto
52. Ladrilleros Asociados S.A.
53. Transportes Gómez Hernández
54. Transportes Sierra
55. Tejar de Pescadero
56. Inversiones Minagro Ltda
57. Porcícola Santa Clara

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)
