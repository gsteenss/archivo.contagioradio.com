Title: Junio: Gobierno para los bancos y crisis ambiental en el Amazonas
Date: 2020-12-31 06:11
Author: CtgAdm
Category: Actualidad, Especiales Contagio Radio, Resumen del año
Slug: junio-sistema-financiero-y-asesinatos-colectivos-en-lugar-de-masacres
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/12/disenos-para-notas-2-1.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

El mes de junio reafirmó lo que para muchos son los verdaderos intereses del gobierno Duque. Por una parte su actitud, calificada como de desprecio, ante el asesinato de los líderes sociales y excombatientes de las FARC fue evidente, el cambio de retórico al llamar homicidios colectivos a las masacres parecía más un maquillaje publicitario que una verdadera intención de resolver este problema que se convertía ya en un problema mayor.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

La definición del estado de emergencia para atender la pandemia le facilitó al uribismo un "gobierno por decreto" que además priorizó la salvación del sistema financiero por encima de las necesidades directas de una ciudadanía que comenzaba a sentir más profundo los rigores del aislamiento obligatorio. Gente con hambre en los barrios de las ciudades seguía clamando por una renta básica que comenzaba a tener los obstáculos esperados en el congreso por parte de la bancada de gobierno.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Y como si la crisis fuera poca por la confrontación entre la humanidad y las otras formas de vida, un incendio, al parecer provocado, en el amazonas de Brasil que ampliaba la frontera agrícola para los grandes terratenientes prácticamente agravó la crisis y nos puso a mirar de frente a lo que, de no pararse, puede ser un paso firme hacia la inhabitabilidad del planeta.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### ONU y CIDH estarían preparando pronunciamientos sobre situación de DDHH en Colombia: CEJIL

<!-- /wp:heading -->

<!-- wp:paragraph -->

Viviana Krsticevic Directora Ejecutiva del [Centro por la Justicia y el Derecho Internacional (CEJIL)](https://www.cejil.org/es), organización enfocada en la defensa de los derechos de poblaciones vulnerables ante omisiones o vulneraciones por parte de los estados en América Latina, habló en entrevista para Contagio Radio señalando la importancia de analizar desde dónde se originan las situaciones vulneratorias de derechos en Colombia y países de la región.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Krsticevic afirmó que **el autoritarismo con el que se opera desde algunos gobiernos lleva a que cualquier individuo que cuestione el actuar institucional o defienda los derechos de poblaciones vulnerables o movimientos sociales, sea visto como «enemigo del Estado y de las Fuerzas de Seguridad».**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Ver más: ([ONU y CIDH estarían preparando pronunciamientos sobre situación de DDHH en Colombia: CEJIL - Contagio Radio](https://archivo.contagioradio.com/onu-cidh-cejil-colombia/))

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### FFMM y bancos, son las prioridades de Iván Duque durante la pandemia: Coeuropa

<!-- /wp:heading -->

<!-- wp:paragraph -->

Organizaciones sociales han seguido de cerca la política institucional desplegada por el gobierno de Iván Duque a lo largo de la pandemia del Covid-19, los resultados un incremento en las acciones y represión por parte de la Fuerza Pública,** decretos que han priorizado a los bancos y grandes empresas, además de un incremento de la violencia en las re**giones caracterizada por un aumento en el asesinato de líderes sociales.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Desde la [Coordinación Colombia – Europa – Estados Unidos – CCEEU](https://coeuropa.org.co/158/) advierten que las acciones del gobierno han priorizado a los bancos por encima de otros sectores económicos y de la misma ciudadanía, lo cual, según su más reciente boletín «ha dejado **más de 5,4 millones de personas sin empleo en solo un mes de la emergencia económica y social decretada, aumentando el desempleo a un 32.6%».**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Ver más: ([FFMM y bancos, son las prioridades de Iván Duque durante la pandemia: Coeuropa](https://archivo.contagioradio.com/ffmm-y-bancos-son-las-prioridades-de-ivan-duque-durante-la-pandemia-coeuropa/))

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Amazonía habría perdido 19.600 millones de árboles en los últimos meses

<!-- /wp:heading -->

<!-- wp:paragraph -->

Este 9 de junio la Fundación para la Conservación y el Desarrollo Sostenible de la Amazonía, señaló que la deforestación en este departamento, supera las 75.031 hectáreas taladas, agregando que **tan solo en enero y abril del 2020 se tumbó la misma extensión que en todo el año pasado**.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

**Antonio Nobre investigador del Instituto Nacional de Pesquisas da Amazonía **(INPA), señaló que el Amazonas bombea 20,000 millones de toneladas de agua cada día hacia la atmósfera, regulando así las lluvias en la gran mayoría de Suramérica.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Ver más: ([Amazonía habría perdido 19.600 millones de árboles en los últimos meses](https://archivo.contagioradio.com/amazonia-deforestacion/))

<!-- /wp:paragraph -->
