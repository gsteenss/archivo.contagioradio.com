Title: "Gobierno no tiene voluntad política para buscar a desaparecidos forzados"
Date: 2015-05-26 19:56
Category: DDHH, Nacional, Paz
Tags: asfades, desaparecido, desaparición, forzada, forzado, moviese, nidia erika, ordoñez, Palacio de Justicia, paz, Santos
Slug: 45-mil-personas-en-colombia-la-desaparicion-ha-sido-sistematica-y-generalizada
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/05/3.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Contagio Radio 

<iframe src="http://www.ivoox.com/player_ek_4556686_2_1.html?data=lZqimJuceo6ZmKiakpaJd6KkmpKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRlNDZzsaSlKiPhdbmwpC6w9fNpYy4ysbnh5enb6LHp6axp7iRaZi3jqjc0NnFq8rjjLfOxs7Tb46ZmKialg%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Aura María Díaz, ASFADES] 

###### [26 may 2015] 

El 9 de septiembre de 1977 se registró de manera oficial el primer caso de desaparición forzada en Colombia: Se trata de Omaira Montoya, capturada ilegalmente y desaparecida por el Ejercito Nacional. El 22 de mayo del 2015, se denunció el *último caso*: Edison Torres, concejal del Polo Democrático del municipio López de Micay, en departamento del Cauca.

El Estado colombiano no tiene un registro de cuántas personas en Colombia han desaparecido forzadamente. Mientras la Fiscalía da cuenta de 60 mil personas, y la Unidad de Víctimas apunta a las 44 mil; las organizaciones de familiares víctimas de desaparición forzada, como ASFADES, Familiares Colombia, la Fundación Nidia Erika Bautista, MOVICE, y Desaparecidos del Palacio de Justicia, registran alrededor de 45 mil desaparecidas y desaparecidos forzados. Según denuncian, ha sido falta de voluntad política del Estado la que ha impedido que se establezca un registro oficial. No les falta razón si se considera, como anuncia el Informe Basta Ya, que agentes estatales son responsables del 84% de estos flagelos.

En el marco de la Semana Internacional del Detenido y Desaparecido, organizaciones de familiares víctimas de desapariciones forzadas convocaron a las entidades gubernamentales para realizar una rendición de cuentas en la que pudiera establecerse no solo la situación de la desaparición forzada en Colombia, sino, fundamentalmente, estrategias para la búsqueda, verdad y justicia. Ni la Fiscalía ni la Procuraduría asistieron al llamado de los ciudadanos.

[![Desaparecidos](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/05/3.jpg){.wp-image-9211 .alignleft width="353" height="212"}](https://archivo.contagioradio.com/45-mil-personas-en-colombia-la-desaparicion-ha-sido-sistematica-y-generalizada/3-5/)Para los familiares, esta es una muestra más del tratamiento que da el gobierno colombiano a la desaparición forzada: No solo la renuencia a participar en los espacios, sino la total falta de reconocimiento.

Si bien, en el marco del actual proceso de paz que sostiene el gobierno de Juan Manuel Santos con la guerrilla de las FARC, el Procurador General Alejandro Ordoñez ha insistido en la necesidad de declarar la desaparición forzada como un crimen de lesa humanidad, sólo ha mencionado la existencia de 400 casos que le atribuye las guerrillas, muchos de los cuales hacen referencia a militares que se encontraban en el desarrollo de operaciones. A pesar de tratarse de actores armados, la guerrilla se ha comprometido a esclarecer los casos de desaparición que les conciernan. No ha ocurrido lo mismo por parte del gobierno.

Actualmente, en la Procuraduría no existen investigaciones contra miembros del Estado por desapariciones forzadas. En Colombia no se ha retirado a un solo mando militar de sus funciones por cometer este delito contra civiles. Según denuncian los familiares, si se amplía el fuero penal militar esta situación se agudizaría: “El *yo* con *yo*”, ellos los desaparecen y ellos se investigan a sí mismos”, afirma Aura María Díaz, de ASFADES. No le falta razón si se considera que a agentes estatales como el F2, antigua SIJIN, se le comprobó responsabilidad en la desaparición de colombianos y colombianas, por años.

[![Desaparecidos](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/05/11.jpg){.wp-image-9209 .alignright width="289" height="166"}](https://archivo.contagioradio.com/45-mil-personas-en-colombia-la-desaparicion-ha-sido-sistematica-y-generalizada/1-6/)En el 2006 se creó la Convención de las Naciones Unidas sobre Desaparición Forzada. 22 países adhirieron; Colombia se abstuvo. Sólo hasta el año 2007, y como resultado de una campaña por parte de las y los familiares de desaparecidos para presionar al gobierno, el Estado Colombiano aceptó firmar la Convención, que fue ratificada por el Congreso solo hasta el año 2010 haciendo excepción sobre los capítulos 71 y 32, en los cuales se considera la creación de un Comité Contra la Desaparición Forzada en Colombia.

Mientras el Estado asevera que cuenta con las entidades suficientes para atender de manera adecuada esta problemática, las víctimas aseguran que la creación de un Comité Contra la Desaparición Forzada no solo es importante para que se reconozca la existencia de desaparecidas y desaparecidos de manera forzada en Colombia, sino también para que pueda darse un trabajo orientado específicamente a contrarrestar estos hechos violatorios a los DDHH, y el seguimiento pertinente.

![Desaparecidos](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/05/2.jpg){.wp-image-9210 .alignleft width="295" height="165"}“Si el gobierno quiere dar señales de buena voluntad de paz a un proceso que está enredado, debe ratificar su participación en la Convención. No es la solución definitiva, pero es un gran apoyo”, afirma Luz Marina Hache.

Es por este motivo que las organizaciones de familiares de desaparecidos y desaparecidas forzadas en Colombia han presentado un informe alterno a la ONU, señalando especialmente la necesidad de:

- Insistir en que Colombia establezca un Comité Contra la Desaparición Forzada, puesto que a la fecha no hay ninguna entidad encargada de hacer seguimiento a los casos de desaparecidos y desaparecidas en Colombia.

- Que entidades internacionales hagan seguimiento a la instituciones colombianas que adelantan investigaciones para presionar el adecuado desarrollo de las mismas.

- Establecer mecanismos de búsqueda de las personas con vida.

- Generar mecanismos reales de búsqueda urgente. Los familiares de víctimas de desaparición forzada aseguran que han llegado a presentarse casos en que la búsqueda inicia 2 años después de registrarse los hechos.

- Generar visitas a Colombia, para que entidades internacionales verifiquen la situación de desaparición forzada en Colombia, más allá de los informes presentados por el gobierno nacional.

**Enseñanzas del proceso en Argentina**

<iframe src="http://www.ivoox.com/player_ek_4556702_2_1.html?data=lZqimJyUdo6ZmKiakpuJd6Klk5KSmaiRdo6ZmKiakpKJe6ShkZKSmaiReJafzs7ZjdXJttTjz8bgh5ilb6bijKjcztTRpsrVjNHOjcnJt8LkwtfWxc6Jh5SZo5jbjc3Fb9TdxdSY1ZKJe6ShpNTb1sbLrdCfs8bRy9SPcYarpJKh&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Gustavo Germano, Argentina] 

“Esta visión que se tiene que de que nada avanza, de que no se va a poder, es un engaño”, asegura Gustavo Germano, hermano de Eduardo Germano, desaparecido forzado durante la dictadura en Argentina.

Tras el fin de la dictadura militar, la sociedad civil argentina presionó la creación de la Comisión Nacional sobre la Desaparición de Personas, CONADEP, que recibió cerca de 5.500 denuncias sobre desapariciones forzadas en ese país. Y, aunque la Comisión no tuvo ningún impacto jurídico, logró establecer la cantidad de personas que sufrieron ese flagelo, y los principales lugares de tortura y desaparición, como La Perla y el Centro Mecánico de la Armada. Esta base de datos e información se construyó a partir de las investigaciones de los familiares de desaparecidos, así como de sobrevivientes a torturas y asesinatos. El Estado no desclacificó archivos militares, incluso en el peroiodo post-dictadura. Los pocos documentos oficiales que se conocieron, se filtraron sin participación decisiva de los gobiernos.

Para Germano, el objetivo de la desaparición forzada en Argentina no fue el asesinato de la oposición, sino la implementación de un modelo económico. Y es por este motivo que durante la implementación dura de políticas neoliberales se implementó la Ley de Punto Final, que evitaba que se abrieran procesos de investigación contra civiles o militares vinculados con desapariciones forzadas.

El Centro de Estudios Legales y Sociales jugó un papel importante durante este periodo, al solicitar al estado procesos ciudadanos en los que, si bien era imposible juzgar penalmente a los responsables, se permitiera por lo menos conocer la verdad. Estos procesos ciudadanos fueron fundamentales en la medida en que, “sabiendo la verdad no puedes mirar para otro lado”.

Con el inicio de los gobiernos Kirchneristas, que anularon la Ley de Punto Final por inconstitucional, permitiendo que se iniciaran procesos jurídicos contra cientos de militares y civiles responsables de desapariciones forzadas durante la dictadura. Actualmente, alrededor de 700 personas cumplen castigos por este delito.

“La unidad es el elemento central. La unidad de lucha de todas las víctimas implicadas”, asegura Gustavo Germano.
