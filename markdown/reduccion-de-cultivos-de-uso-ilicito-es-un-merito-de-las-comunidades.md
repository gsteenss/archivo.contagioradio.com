Title: Reducción de cultivos de uso ilícito es un mérito de las comunidades
Date: 2019-08-06 18:09
Author: CtgAdm
Category: Comunidad, Nacional
Tags: Guaviare, Plan de Sustitución de Cultivos de Uso ilícito, PNIS, Putumayo
Slug: reduccion-de-cultivos-de-uso-ilicito-es-un-merito-de-las-comunidades
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/08/Sustitución.png" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/08/Departamentos-Sustitución.jpeg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo ] 

Según el más reciente informe de la Oficina de Naciones Unidas contra la Droga y el Delito (UNODC)  para 2018, luego de cuatro años de trabajo se logró detener la tendencia de crecimiento de los cultivos de uso ilícito en Colombia, pasando de 171.000 a 169.000 hectáreas, 2.000 menos de las que existían en 2017, cifra que aunque solo representa el 1.2% de reducción, refleja una eficacia en los niveles de resiembra que según la ONU, son inferiores al 1%  en los casos en que la sustitución fue concertada con las comunidades campesinas.

Para Indepaz y tal como lo afirma el investigador Salomón Majbub este es un "mérito de las comunidades, de su compromiso y voluntad de querer convertir su economía cocalera"  y no tanto del Gobierno de Iván Duque que le ha apostado todo a la erradicación forzada y quien asumió la presidencia el 7 agosto del 2018,  fecha en que quedaban muy pocos meses para generar un quiebre en la magnitud de crecimiento.

### A destacar dentro del informe 

El programa, que viene implementándose desde mayo del 2017 y al que se acogieron 99.000 familias revela que se ha generado un efecto de “balance”, mientras que en nueve departamentos, entre estos **Nariño, Putumayo, Meta, Chocó y Amazonas** se logró una reducción de más de 11.000 hectáreas, en otros como Norte de Santander, Bolívar y Cauca  se dio un incremento de 9.000.

[El informe](https://www.unodc.org/documents/colombia/2019/Agosto/Informe_Monitoreo_de_Territorios_Afectados_por_Cultivos_Ilicitos_2018.pdf) además indica que el 37 % del territorio ha permanecido sin cultivos de coca por tres años o más y que Caldas se convirtió en el primer departamento en dejar de registrar el cultivo de coca, mientras 22 departamentos incluidos **Arauca, Vaupés, Guainía, Cesar, Santander, Boyacá, Magdalena y La Guajira** tienen menos de 100 hectáreas y continúan avanzando.

Por otro lado, pese a que se registró una reducción del 54 % del territorio afectado por cultivos de coca en 2018, prevalece la tendencia a su concentración, es decir, hay más coca en menos territorio, en particular en municipios como Tibú, Norte de Santander; Tumaco, Nariño; Puerto Asís, Putumayo; El Tambo, Cauca; Sardinata, Norte de Santander; El Charco, Nariño; El Tarra, Norte de Santander; Orito, Putumayo; Tarazá, Antioquia, y Barbacoas, Nariño, terrritorios que suman el **44 % del total de cultivos de coca del país en 2018. **

De igual forma, la UNODC advierte que el 75 % de líderes sociales y defensores de derechos humanos asesinados entre 2016 y 2018 vivía en municipios con presencia de cultivos de coca, y el 71 % habitaba en 170 municipios donde se adelantan los programas de desarrollo con enfoque territorial, es decir que la probabilidad de que un líder social fuera asesinado en un municipio con coca durante el periodo 2016-2018 era 3,9 veces más alta que en cualquier otro lugar. [(Le puede interesar: Asesinan a Miguel Pérez impulsor de sustitución de cultivos de uso ilícito en Tarazá, Antioquia)](https://archivo.contagioradio.com/asesinan_miguel_perez_taraza_antioquia/)

\[caption id="attachment\_71868" align="aligncenter" width="720"\]![Sustitución](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/08/Sustitución.png){.size-full .wp-image-71868 width="720" height="356"} Informe de Monitoreo de Territorios Afectados por Cultivos de Uso Ilícto en 2018 de la UNODC\[/caption\]

### El discurso que avala la erradicación forzada de cultivos de uso ilícito

Pese a que el Gobierno se ha adjudicado este avance,  Majbub  advierte que este primer año de presidencia de Iván Duque y durante sus viajes, no ha realizado mención alguna sobre el programa de sustitución voluntaria y que por el contrario "se ha limitado a anunciar el retorno de las fumigaciones y exaltar números poco creíbles en materia de erradicación que se contradicen entre sus mismas instituciones".

En la actualidad, las comunidades que se acogieron a este plan voluntad, ya arrancaron la coca que se encontraba en sus cultivos y  ahora atraviesan una crisis porque no cuentan proyectos productivos  que garanticen su alimentación y economía. [(Lea también: Presidente Duque, fumigando no se acaba con el narcotráfico: lideresa Maydany Salcedo)](https://archivo.contagioradio.com/presidente-duque-fumigando-no-se-acaba-con-el-narcotrafico-lideresa-maydany-salcedo/)

Salomón Majbub, indica que además de la ausencia de proyectos productivos, el Gobierno ya realizó una compra cercana a los tres mil millones de pesos en glifosato chino, gasto previo a que la Corte Constitucional se pronunciara el 18 de julio.

\[caption id="attachment\_71869" align="aligncenter" width="908"\]![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/08/Departamentos-Sustitución-908x1024.jpeg){.size-large .wp-image-71869 width="908" height="1024"} Informe de Monitoreo de Territorios Afectados por Cultivos de Uso Ilícto en 2018 de la UNODC\[/caption\]

### El reto es no perder la confianza construida con las comunidades

En algunos territorios como Putumayo o Guaviare, sus comunidades se sienten **"inquietas y alarmadas"** pues han visto cómo las avionetas se han reactivado en los territorios y que significaría el regreso de la aspersión, algo que para el investigador es una "estrategia de guerra del Estado colombiano contra grupos armados del territorio pero  que termina teniendo una afectación negativa sobre las comunidades", desgastando la confianza que costó entablar en las regiones.

> Una vez más el Estado deslegitima estos territorios al no ofrecer salidas democráticas de transformación o de reconversión de la economía en estas comunidades, sino que les aplica una estrategia militar por medio de la fumigación

Si bien los cultivos de uso ilícito se redujeron en **Nariño, Guavire, Meta o Chocó,** en otros departamentos como **Norte de Santander, Cauca y Bolívar** se han presentado rezagos en el plan, una problemática a la que se suma  la decisión del Gobierno de cerrar la puerta a otras familias que han manifestado su deseo voluntad de ingresar al programa, dejándolas "a merced de la erradicación forzada".

Como recomendación final, Majbub expresó que mientras las políticas de erradicación tienen asignados recursos concretos, este mismo modelo debería replicarse en la sustitución voluntaria, cuyos recursos en la actualidad funcionan "a cuenta gotas", en lugar de otorgarle una fluidez mayor que permitiría un avance más eficaz.

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>  
<iframe id="audio_39648713" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_39648713_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
