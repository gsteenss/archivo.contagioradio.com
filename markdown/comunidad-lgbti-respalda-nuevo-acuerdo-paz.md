Title: Comunidad LGBTI respalda nuevo acuerdo de paz
Date: 2016-11-14 13:03
Category: LGBTI, Nacional
Tags: acuerdo de paz, Humberto de la Calle, La Habana, LGBTI, paz
Slug: comunidad-lgbti-respalda-nuevo-acuerdo-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/LGBTI-e1479146333517.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Colombia Diversa 

###### [14 Nov 2016]

Luego de que se conociera algunos apartes del contenido del nuevo acuerdo de paz entre la guerrilla de las FARC y el gobierno colombiano, organizaciones y activistas de la comunidad LGBTI, **expresaron su “satisfacción” afirmando que el acuerdo reconoce que esta población ha sido revictimizada** en el marco del conflicto armado interno.

De esa manera resaltaron que el tema continúa siendo transversal en los puntos pactados y se siguen los anteriores lineamientos para garantizar los derechos de la comunidad LGBTI, como lo dijo desde La Habana, Humberto de la Calle, jefe negociador de la delegación de paz del gobierno, “Ningún contenido del Acuerdo Final se entenderá o interpretará como la negación, restricción o menoscabo de los derechos de las personas”.

Este nuevo acuerdo mantiene el enfoque de género del anterior y además se especifica para evidenciar por qué la guerra ha golpeado de forma diferenciada a estos hombres y mujeres. Como consecuencia de ello, se reconoce que ellos y ellas requieren de acciones específicas para restablecer sus derechos. **“Las palabras de los negociadores ofrecen tranquilidad a los sectores sociales y en especial a las mujeres y población LGBTI.** Tenemos la confianza que el nuevo texto recoja las aspiraciones planteadas por nosotros y nosotras a las partes”, dice el comunicado.

Asimismo, aseguran que mantendrán la movilización social, promoverán el conocimiento y protección de los nuevos acuerdos, y además participaremos en la veeduría y participación ciudadana sobre el proceso de implementación de los mismos.

Finalmente, **proponen que se cree una subcomisión de género para dar seguimiento** a “la implementación del enfoque de género, a las acciones afirmativas y al principio de igualdad y no discriminación que atraviesan el acuerdo, con la participación de organizaciones de mujeres y LGBTI”.

Cabe recordar que la semana pasada más de 100 organizaciones de la comunidad LGBTI habían solicitado al presidente Juan Manuel Santos, una reunión para poder hablar sobre la renegociación del acuerdo de paz, pero Santos no mostraba señales de diálogo con ese sector.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
