Title: Dos heridos graves en la Delfina por ataques del ESMAD
Date: 2017-11-02 12:34
Category: Movilización, Nacional
Tags: Abuso de fuerza ESMAD, ESMAD, indígenas, indígenas de Colombia, Minga Indígena, Movilización Indígena
Slug: dos-heridos-de-gravedad-en-la-delfina-por-ataques-del-esmad
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/11/minga-indígena-e1509643967656.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: ACIVA-RP] 

###### [02 Nov 2017] 

Continúa la movilización indígena en el país y específicamente en el sector de la Delfina, en la vía que comunica a Buenaventura con Cali, las diferentes comunidades se encuentran concentradas exigiéndole al Gobierno Nacional que **atienda las demandas que han hecho públicas**. Además, han denunciado ataques violentos y hurto de equipos de comunicación por parte de la Fuerza Pública.

De acuerdo con Luis Tandioy, integrante de la Asociación de Cabildos Indígenas del Valle del Cauca Región Pacífica (ACIVA-RP), “la comunidad está solicitando de manera inmediata **el pronunciamiento del Gobierno Nacional** frente a la instalación de la mesa nacional en el territorio de la Delfina”. Afirmó que los conflictos en las movilizaciones, responden a la falta de atención de las instituciones a las solicitudes de la MINGA.

### **Indígenas han denunciado agresiones del ESMAD con artefactos explosivos** 

Tandioy manifestó que las diferentes organizaciones que han participado de las movilizaciones, han realizado varios comunicados a la opinión pública denunciando **la vulneración de los derechos humanos** de los mingueros. Dijo que la Fuerza Pública “ha venido disparando en el territorio indígena, donde la Guardia Indígena hace el ejercicio de control”. (Le puede interesar: ["Indígenas denuncian agresiones con arma de fuego por parte del ESMAD"](https://archivo.contagioradio.com/indigenas-denuncian-agresiones-con-arma-de-fuego-por-parte-del-esmad/))

Indicó que, a partir del 30 de octubre, día que empezó la protesta social, “se han presentado **dos heridos de gravedad debido a los ataques con diferentes artefactos** por parte del ESMAD”. Estas situaciones han sido denunciadas y atendidas por los mecanismos internacionales que están velando por el respeto de los derechos humanos de las personas que participan de la movilización.

Afirmó que los indígenas han contado con el acompañamiento **permanente de las Naciones Unidas**, la Defensoría del Pueblo de Buenaventura y regional del Valle y la Procuraduría del distrito de Buenaventura. Estas instituciones han estado pendientes de lo que sucede con las movilizaciones de la MINGA.

### **ACIVA-RP había denunciado la detención arbitraria de integrantes del equipo de comunicación** 

A la grave situación que están viviendo los indígenas en esta parte del país, se suma una denuncia del 31 de octubre donde la ACIVA-RP indicó que **soldados del ejército colombiano “realizaron actos de pillaje** (…) contra el equipo de comunicaciones de la organización indígena”. (Le puede interesar: ["Desde firma del Acuerdo de Paz reportan 40 indígenas asesinados: OPIAC"](https://archivo.contagioradio.com/indigenas-asesinados-acuerdo-paz/))

En el comunicado, manifiestan que los integrantes del equipo **fueron detenidos por varias horas** dentro del territorio del resguardo y “fueron sometidos a tortura psicológica, insultos y despojo de un cámara HD, una micro SD y un radio”.

Ante estas situaciones le solicitaron al coronel Sánchez del ejército Nacional y comandante de esa zona que **“responda por los actos de pillaje de sus subalternos”.** Además, confirmaron su firmeza con las movilizaciones en el marco de la Minga Indígena mientras esperan que el Gobierno Nacional se cumpla con los acuerdos pactados e incumplidos.

[ONIC - Denuncia pública de ORIVAC y ACIVA R](https://www.scribd.com/document/363312271/ONIC-Denuncia-pu-blica-de-ORIVAC-y-ACIVA-R#from_embed "View ONIC - Denuncia pública de ORIVAC y ACIVA R on Scribd") by [Anonymous 9gchRS](https://www.scribd.com/user/379499957/Anonymous-9gchRS#from_embed "View Anonymous 9gchRS's profile on Scribd") on Scribd

<iframe id="doc_32546" class="scribd_iframe_embed" title="ONIC - Denuncia pública de ORIVAC y ACIVA R" src="https://www.scribd.com/embeds/363312271/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-aDwnmXn52aQBjhfzGlRL&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="0.7729220222793488"></iframe><iframe id="audio_21846827" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_21846827_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 

 
