Title: Gobierno Duque niega el conflicto armado pero permite el porte de armas a civiles
Date: 2019-02-21 14:59
Category: DDHH, Nacional
Tags: defensa, guerra, paz, Porte de armas
Slug: conflicto-porte-de-armas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/02/Captura-de-pantalla-25-770x400.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Presidencia de la República] 

###### [21 Feb 2019] 

Luego de que el Ministerio de Defensa emitió la directiva con la que se permite la compra y porte de armas por parte de ciudadanos, **el analista y profesor en ciencias políticas Fernando Giraldo** señala los posibles efectos de permitir que el Estado seda el monopolio de las armas, y permita que la seguridad de las personas corra por su propia cuenta como ocurría hace siglos.

Giraldo recuerda que **en una sociedad moderna los civiles no tienen por qué portar armas, pues se supone que socialmente se ha acordado vivir en armonía y en paz**, delegando la capacidad de brindar seguridad al Estado mediante sus estructuras militares y de policía. Por el contrario, permitir que algunos ciudadanos estén armados es no "reconocer al otro como es con sus necesidades y perspectivas de vida", lo que deriva en la pérdida de libertad para quienes no pueden o no quieren tener armas.

Sobre la idea de que es una medida de protección y seguridad para algunas pocas personas, el Profesor señala que se trata por el contrario de un riesgo, pues u**na persona civil que está armada representa un riesgo para quienes no lo estén**; sumando a ello la gravedad de que el Estado reconozca que no puede proteger a sus ciudadanos, que es su labor elemental. (Le puede interesar: ["Duque, atienda primero la crisis humanitaria en La Guajira: Mujeres Wayúu"](https://archivo.contagioradio.com/guajira/))

### **¿El Gobierno no reconoce el conflicto pero sí reconoce la inseguridad?** 

El profesor añade que una situación clara de quienes han tenido el reconocimiento legítimo y legal que dan las urnas, es decir el partido de Gobierno, nunca han creído en la paz; muestra de ello es el nombramiento en el Centro Nacional de Memoria Histórica de un historiador que niega el conflicto armado en Colombia, y por lo tanto, niega que lo pactado en La Habana sea un Acuerdo de Paz. (Le puede interesar: ["La ruptura de confianza entre las víctimas y el Centro Nacional de Memoria Histórica"](https://archivo.contagioradio.com/victimas-memoria-historica/))

No obstante, como explica Giraldo, **si el Gobierno autoriza a los civiles para que se armen y protejan su propia seguridad es porque reconoce que hay una guerra,** que hay un conflicto armado del cual "las personas de bien" quieren protegerse. Por lo tanto, para el analísta, autorizar el porte de armas por parte de ciudadanos implica necesariamente reconocer que aún vivimos en medio del conflicto armado. (Le puede interesar: ["¿Plan Nacional de Desarrollo de Duque se olvidó de la paz?"](https://archivo.contagioradio.com/plan-de-desarrollo-olvido-paz/))

### **Colombia: Entre la democracia y la violencia** 

Giraldo no tiene dudas en que esta decisión es una posibilidad más para que hacer fracasar el proceso de paz; como él lo explica, fue un despropósito mismo que la implementación del proceso de paz haya quedado en manos de instituciones del Estado que no fueron capaces de erradicar la violencia en la política, de ello se deduce que esta sociedad en conjunto no está preparada para la democracia ni para la paz.

Siguiendo este análisis, la democracia no se trata de la división de poderes, o de ir a las urnas para elegir un representante, sino a 3 criterios fundamentales: madurez política, verdad y no violencia; ninguno de los 3 se cumplen a cabalidad en el país, pero mucho menos la no violencia, porque como señala Giraldo, se está haciendo uso del derecho a la guerra para, en este caso, permitir que civiles se armen.

<iframe id="audio_32742030" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_32742030_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
