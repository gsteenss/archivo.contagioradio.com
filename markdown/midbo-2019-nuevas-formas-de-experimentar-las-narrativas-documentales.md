Title: MIDBO 2019, nuevas formas de experimentar las narrativas documentales
Date: 2019-10-25 16:09
Author: CtgAdm
Category: 24 Cuadros, Cultura
Tags: Agnès Varda, Cine Documental, Luis Ospina, MIDBO
Slug: midbo-2019-nuevas-formas-de-experimentar-las-narrativas-documentales
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/MIDBO.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:] 

Desde sus inicios, la **Muestra Internacional Documental de Bogotá - MIDBO** ha trabajado con el objetivo de fortalecer la exhibición, reflexión, diálogo y el encuentro alrededor del cine de lo real, proponiendo un acercamiento único a través de nuevas formas de experimentar las narrativas, un elemento que será esencial para este 21ª edición que tendrá lugar del 29 de octubre al 7 de noviembre de 2019.

Tras una exitosa convocatoria a nivel nacional e internacional, a la que se inscribieron más de 400 películas de 26 países, fueron seleccionadas 78 piezas audiovisuales en las categorías: nacional (15), internacional (31) y emergentes (32), además de ser organizadas charlas con directores internacionales invitados, programas y funciones especiales que conforman un total de 100 películas.

Según Juan Pablo Franky, programador de MIDBO, este año la programación **"busca ir más allá de un formato tradicional del formato tradicional para que el espectador tenga experiencias nuevas y se pueda vincular al cine de otras formas"**.

Parte de estas novedades, se verán en la franja 'Encuentro Pensar lo Real', al ofrecer una muestra de obras realizadas en formatos y soportes no convencionales como instalaciones, video performances, documental para la red de realidad virtual, aumentada y 360°. Además se contará con  la participación del invitado español Gonzalo de Lucas, académico y programador mientras que 'Documental Expandido' exhibirá siete obras de diferentes países como **Colombia, Chile, Guatemala, Brasil, México, Ecuador y Francia.**

"Para nosotros es un gran reto invitar al espectador a acercarse al cine documental, las persona tienen en la mente que estas son películas con formatos clásicos, queremos mostrarle al público que el documental tiene otras formas de acercarse a la realidad" afirma Franky.

\[embed\]https://youtu.be/Knr7r7lY618\[/embed\]

Para este año, el festival de cine está inspirado en los planteamientos del cineasta Jonas Mekas y realizará tributos a algunos directores que fallecieron en los últimos meses como Luis Ospina, Agnès Varda. Si desea conocer más de MIDBO 2019, programción, horarios y salas de exhibición, le invitamos a consultar la pagina oficial de este evento [midbo.co](http://www.midbo.co/), la cuenta oficial en Facebook  y @midbo\_doc en Twitter y en Instagram  @midbo\_doc [(Lea también: "Todo comenzó por el fin" autorretrato documental de Caliwood)](https://archivo.contagioradio.com/todo-comenzo-por-el-fin-autorretrato-documental-de-caliwood/)

<iframe id="audio_43274584" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_43274584_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
