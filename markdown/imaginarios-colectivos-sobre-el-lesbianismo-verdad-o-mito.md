Title: Imaginarios colectivos sobre el lesbianismo: ¿Verdad o mito?
Date: 2015-08-14 17:56
Category: closet, LGBTI
Tags: Closet Abierto, Comunidad LGBTI, Derechos Humanos, Diversidad sexual, Estereotipos, Homosexualidad, Lesbianas
Slug: imaginarios-colectivos-sobre-el-lesbianismo-verdad-o-mito
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/08/Lesbianas.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### Foto: Contagio Radio 

###### <iframe src="http://www.ivoox.com/player_ek_6733952_2_1.html?data=l5yglZ6Zdo6ZmKial5uJd6KklpKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRlNPjyNfOz8aPt8rbzcaYrpKJe6ShpNTb1sbLrdCfs8bRy9SPcYarpJKh&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>  
[Programa 2 de Febrero] 

Closet Abierto abre la temática del programa con lo básico dentro de un grupo social minoritario, el significado de cada una de las siglas de la comunidad LGBTI, para muchos estas siglas no son del todo claras y de hecho, muchos no las asocian a una orientación sexual, en gran parte hay desconocimiento acerca de la diversidad que existe dentro de estas cinco letras.

La sigla L (lesbianas) corresponde a las mujeres que sienten atracción física, sentimental y emocional por otras mujeres, el programa es enfocado hacia los estereotipos de mujeres femeninas y masculinas conocidas como tomboy, asociado con el papel de hombre y mujer aunque sea una relación de dos mujeres.

La invitada Paula Amaya rompe con el estigma de las mujeres de apariencia masculina que cumplen el papel del “hombre” en una relación de dos mujeres, aclarando que el concepto que se tiene de las relaciones de parejas del mismo sexo es totalmente errado, no necesariamente debe haber un hombre y una mujer para encontrar un equilibrio, ella es el ejemplo de que la apariencia masculina no significa dominio en todas las ocasiones.
