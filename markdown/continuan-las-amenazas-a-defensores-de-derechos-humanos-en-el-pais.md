Title: Continúan las amenazas a Defensores de Derechos Humanos en el país
Date: 2017-12-18 17:00
Category: DDHH, Nacional
Tags: amenazas a líderes, Defensores de DD.HH
Slug: continuan-las-amenazas-a-defensores-de-derechos-humanos-en-el-pais
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/07/cancha-belen.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Okavelema blogspot] 

###### [18 Dic 2017] 

Continúan los hostigamientos y persecuciones a los defensores de derechos humanos, **el pasado 15 de diciembre Carlos Fernández, integrante de la Comisión de Justicia y paz, fue blanco de seguimientos cuando se dirigía a la sede de esta institución en Puerto Asís**, Putumayo.

De acuerdo con el defensor de derechos humanos, fue seguido por un hombre vestido de jean, camiseta azul y un bolso tipo carriel, por más de 300 metros, cuando iba a ser abordado el hombre huyó. Además, **los defensores de esta organización han expresado que durante lo corrido del año ya han sido víctimas de persecuciones**.

Los defensores se encuentran en el territorio haciendo un acompañamiento a comunidades en el territorio, en donde han logrado constatar las afectaciones socio ambientales generadas por la exploración y explotación petrolera, **la presencia y movimiento de nuevos grupos armados** y las graves consecuencias para los habitantes con los incumplimientos al Acuerdo de Paz, pactado con la FARC.

El pasado 9 de diciembre, integrantes de la Comisión Intereclesial de Justicia y Paz, también fueron víctimas de seguimientos en la carretera que de Caucheras conduce a Belén de Bajirá, en el departamento de Chocó, cuando fueron interceptados por un punto de control de las AGC, **en donde se tomaron registros fotográficos del vehículo en el que se movilizaban y luego pasó la información a través de un radio**.

De igual forma, el día 11 de diciembre, mientras se realizaban las exequias del líder social Hernán Bedoya, familiares e integrantes fueron hostigados por dos hombres que se movilizaban en una motocicleta y que han sido reconocidos en el territorio como campaneros que pasan información a estructuras armadas. (Le puede interesar: ["Aumentan las amenazas contra líderes sociales del Bajo Atrato y Urabá"](https://archivo.contagioradio.com/aumentan-las-amenazas-contra-lideres-sociales-del-bajo-atrato-y-uraba/))

Estas situaciones en el país, revelan el crecimiento y fuerza que están tomando en las diferentes regiones del país, que se suma al asesinato de líderes sociales y defensores de derechos humanos, **que ha cobrado la vida de más de 80 personas en lo corrido de este año**.

###### Reciba toda la información de Contagio Radio en [[su correo]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio]{.s1}](http://bit.ly/1ICYhVU)[.]{.s1}
