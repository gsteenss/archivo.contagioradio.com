Title: No se quedó en amenazas panfleto contra ocupantes de predios en Tierralta, Córdoba
Date: 2019-06-04 18:08
Author: CtgAdm
Category: Comunidad, Nacional
Tags: AGC, amenaza, cordoba, Panfleto
Slug: panfleto-contra-ocupantes-predios-tierralta-cordoba
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/Diseño-sin-título-1.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio  
] 

El pasado domingo 3 de junio, la Fundación Social Cordoberxia denunció que se cumplió con la intimidación que había aparecido en un panfleto, en el que **se amenazaba con la muerte a ocupantes por vías de hecho de tierras, líderes sociales y políticos en Tierralta Córdoba**. La ejecución de la amenaza ocurrió contra uno de los ocupantes de tierras, que resultó muerto tras recibir disparos por parte de hombres armados que lo atacaron, en momentos en que el ESMAD intentaba desalojar el predio ocupado.

### **¿Qué lleva a las personas a ocupar tierras?**

Andréz Chica, integrante de Cordoberxia, dijo que a pesar de que en Tierralta hay gran catidad de tierras, la mayoría de las personas no son propietarias; esta situación los lleva a vivir en invasiones. Chica recordó el caso del sector conocido como 9 de agosto: una invasión de unas 56 hectáreas en la que viven cerca de 17 mil personas desplazadas por la Hidroeléctrica Urra 1, que en 2017 fue legalizada y convertida formalmente en un barrio.

Ante la falta de propiedad, muchas familias han tenido que recurrir a la invasión para establecer sus lugares de vivienda; ante lo que muchos alcaldes han respondido mediante la compra de predios y legalización como en el caso del 9 de agosto. Según Chica, **hace cerca de 15 día un grupo de personas, venezolanas en su mayoría, ocuparon un predio, y posteriormente se trasladaron a tierras en propiedad del padre de[ Fabio Otero Avilés,]{.ms-rteStyle-Texto_Normal} alcalde de Tierralta.** (Le puede interesar: ["Cerca de 600 indígenas y campesinos fueron desplazados en Córdoba"](https://archivo.contagioradio.com/desplazan-600-indigenas-y-campesinos-en-tierraalta-onu/))

En ese momento, explicó el integrante de Cordoberxia, hizo presencia el ESMAD para sacar a las personas del predio; y **posteriormente apareció el panfleto,** firmado por las autodenominadas Autodefensas Gaitanistas de Colombia (AGC), que amenazaba a los ocupantes irregulares, al líder **Albeiro Begambre**, y al candidato de la Unión Patriótica a la alcaldía del Municipio. (Le puede intersar: ["Según la ONU, ya son 2.159 personas desplazadas forzosamente en Córdoba"](https://archivo.contagioradio.com/segun-la-onu-desplazamientos-forzados-en-cordoba-ascienden-a-2-159-personas/))

### **¿La amenaza cumplida?**

Para Chica, lo delicado de la situación es que **una de las amenazas se consumó ayer**. El homicidio ocurrió mientras el ESMAD arremetía nuevamente contra los ocupantes de predios, y aparecieron hombres en moto que dispararon contra dos jóvenes, causando la muerte de uno de ellos y heridas graves en el abdomen al otro. Adicionalmente, el líder Begambre tuvo que abandonar el territorio por falta de garantías para su vida.

Por estas razones, el Directivo de Cordoberxia pidió que se apliquen las rutas de protección que les permita a los líderes seguir desarrollando su trabajo y tener garantías para el mismo, pues recordó que solo en el mes de mayo fueron amenazados 4 líderes, mientras que 3 de ellos perdieron la vida en el departamento; adicionalmente pidió que se atienda la situación en materia de seguridad, actuando contra los actores ilegales que operan en Córdoba.

> Otro líder asesinado en el Sur de Córdoba, Luis Fernando Velásquez, líder en sustitución de cultivos y dos campesinos Jader Polo y Jader Pertuz, configura una masacre, en el Municipio San Juan de Ure, en el sector Batatalito, amenazada por los "caparrapos".
>
> — Aída Avella E (@AidaAvellaE) [3 de junio de 2019](https://twitter.com/AidaAvellaE/status/1135565918845771782?ref_src=twsrc%5Etfw)

<p>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
<iframe id="audio_36727838" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_36727838_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen">
</iframe>
</p>
###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
