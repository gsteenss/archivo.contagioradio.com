Title: Mil claveles blancos por la memoria de Klaus Zapata
Date: 2016-03-10 23:59
Category: Movilización, Nacional
Tags: Klaus Zapata, memoria, Paramilitarismo, soacha
Slug: mil-claveles-blancos-por-la-memoria-de-klaus-zapata
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/03/IMG-20160310-WA0045.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Contagio Radio 

<iframe src="http://co.ivoox.com/es/player_ek_10765155_2_1.html?data=kpWkmJqVeZahhpywj5WZaZS1lpeah5yncZOhhpywj5WRaZi3jpWah5ynca7dzZDQzsbaqc3Z1JDPzsbSp9DnjNXc1JDQpYzhxtLc1M7Fb8XZjLDZw9rXb7vV0cbhw5KJe6ShpNTb1sbLrdCfs8bRy9SPcYarpJKh&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### 10 Mar 2016 

Bajo un sol radiante, en la mañana de este jueves se dieron cita en el parque principal de Soacha, Cundinamarca, los familiares, amigos y compañeros de estudios del líder juvenil Klaus Zapata, asesinado el pasado domingo por un hombre del que aún se desconoce tanto la identidad como el paradero. Con tambores, banderas, pancartas, flores y canciones rindieron homenaje a la memoria de este joven defensor de los derechos del medio ambiente y exigieron que se esclarezcan los hechos y que este crimen no quede en la impunidad.

Una multitudinaria marcha de la que hicieron parte la Juventud Comunista, el Movimiento Ambiental Caminando el Territorio, la Red Juvenil de Soacha, el Comité Permanente por la Defensa de los Derechos Humanos, la Unión Patriótica, la Universidad Minuto de Dios y colectivos audiovisuales de Soacha y Ciudad Bolívar, acompañó con arengas el carro fúnebre que transportó el cuerpo de Klaus hasta el Cementerio Campos de Cristo en el que fue sepultado.

Charles Gonzales amigo cercano de Klaus, lo recuerda como un joven preocupado por las problemáticas ambientales de Soacha, una inquietud que lo llevó a hacer parte de distintos colectivos en los que aportó sus conocimientos audiovisuales. Charles, asegura que a Klaus “nunca se le veía triste, pese a que tuviera problemas siempre brindaba sonrisas”, razón por la que invita a quienes lo conocieron no derramar lágrimas en su honor, sino irradiar la alegría que lo caracterizaba para que su legado no muera.

Gabriel Romero, integrante del Movimiento Ambiental Caminando el Territorio y de la Red Juvenil de Soacha, asevera que compartió con Klaus sueños comunes, el más grande de ellos transformar el territorio para que los jóvenes no tuvieran miedo de salir a las calles, expresarse y construir una mejor sociedad. Para Gabriel este evento es un homenaje a las luchas de su compañero, a quien le costaba creer que en su territorio no se tolerara el pensamiento alternativo y hubiera indiferencia, violencia y estigmatización contra los jóvenes.

“Somos la vida y la alegría en tremenda lucha contra la tristeza y la muerte” se leía en una de las pancartas que elaboraron los amigos de este joven, una de ellas Jenny Teca quien recuerda a Klaus como un compañero que siempre llegaba con una sonrisa a la universidad aunque algo le hiciera falta, agrega que con este líder consolidaron proyectos que continuaran en su honor e insiste en que los claveles con los que se le rindió homenaje “representan la paz que hace falta para nosotros como pueblo, la paz para los jóvenes emprendedores y que quieren salir adelante”.
