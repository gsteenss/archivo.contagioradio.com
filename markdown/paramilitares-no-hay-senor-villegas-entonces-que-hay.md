Title: “Paramilitares no hay” … señor Villegas ¿entonces qué hay?
Date: 2017-02-17 06:00
Author: JOHAN MENDOZA TORRES
Category: Johan Mendoza, Opinion
Tags: Ministerio de defensa de Colombia, Paramilitarismo
Slug: paramilitares-no-hay-senor-villegas-entonces-que-hay
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/02/mindefensa.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto:MinDefensa 

#### [**Por: [Johan Mendoza Torres](https://archivo.contagioradio.com/johan-mendoza-torres/)**] 

###### 17 Feb 2017 

[Para muchos colombianos que han vivido de cerca el conflicto armado, y sobre todo para un grueso de comunidades que han estado de alguna u otra manera enfrentadas a poderes estatales, empresariales o multinacionales enormemente superiores, saben que la palabra “paramilitar” significa muerte. No obstante es pertinente reconocer que hablar de una fuerza paramilitar, conlleva a asegurar que se trata de un grupo armado que de manera paralela, pero no formal, acompaña la acción de las fuerzas militares; es decir, combaten a un mismo enemigo pero con distintos métodos, aclarando por supuesto que la distinción de esos métodos se basa en la legalidad o ilegalidad (atrocidad) de las acciones en un escenario de conflicto.   ]

[Durante el gobierno de Uribe “el expresidente inmune a la justicia” “la causa incausada”, varios miembros de las Fuerzas Militares ofrecieron el penoso espectáculo de promover directa o indirectamente una alianza para matar no sólo a la guerrilla, o a lo que pareciera u oliera a guerrilla, sino incluso, a quien no tuviera que ver, pero que al final salía mejor matarlo para tener algo qué mostrar en los medios informativos; esos fueron los tiempos de Ralito, los tiempos en que los comandantes paramilitares visitaban la casa de Nariño o el Club el Nogal, los tiempos de los falsos positivos.   ]

[Pues bien, actualmente las cosas no parecen bastante claras, ya que muchos dicen que con la desaparición de los Castaño y la extradición de los cabecillas de las AUC, la estructura paramilitar quedó destruida…]

[Sin embargo, comunidades de Cacarica en el bajo Atrato Chocano, en Mocoa y sus zonas aledañas, en La Gabarra Norte de Santander y en el Resguardo Indígena Wounaan Nonam en el Valle del cauca, están denunciando la continuidad y el crecimiento del paramilitarismo en un momento histórico en que la guerrilla de las Farc se encuentra dirigiéndose a las zonas de concentración.]

[La respuesta del ministro Villegas a las comunidades a través de los medios informativos en general fue tajante: “En Colombia no hay paramilitarismo”.  ]

[Si pensamos en las masacres y la escandalosa comunión entre jefes paramilitares y muchísimos funcionarios del gobierno uribista, o si pensamos en la fiel definición de la palabra “paramilitar” evidentemente, sería difícil asegurar que hoy hay paramilitarismo en todo el sentido de la palabra.]

[¿Si no hay paramilitarismo, entonces qué hay? Porque también es absurdo lo que Villegas hace respondiendo a las comunidades con el argumento debilucho y mentiroso de que simplemente es “delincuencia común”, eso es igual de absurdo a cuando en su época, el ideólogo del uribismo (señor Obdulio Gaviria) decía que en Colombia no había conflicto armado.]

[¿Qué está ocultando Villegas negando el paramilitarismo? Según dice, es para no darle estatus político a bandas de delincuentes, pero ¿acaso alguna vez tuvieron estatus político? ¿recibir instrucciones en el club el Nogal ya les daba estatus político? ¿entrar a la casa de Nariño les dio estatus político?  ]

[En esa época se les llamó paramilitares, pues la alianza con el Ejército y muchos funcionarios se hizo evidente, en los años 50 se les llamaba pájaros, luego se les intentó (con muchísima fuerza) llamar BACRIM, y ahora Villegas lo quiere reducir a “delincuencia común” siendo que llámense como se llamen, siempre han sido la mano armada que hace el trabajo sucio prestando servicios de seguridad o intimidación que claramente favorecen intereses de terratenientes, grandes empresas nacionales o multinacionales, o de aquellos compradores de tierras de muy buena fe.  ]

[Ponerle el rótulo “paramilitares” implica culposamente al ejército, y hoy al ejército lo tienen levantando el dedo pulgar por las carreteras, pues ya las Farc y el Eln están diciendo poco a poco adiós a la guerra… ponerle el rotulo de “paramilitares” justificaría el escenario para pensar que Santos mientras firma la paz, ordena a sus generales que apoyen o les presten helicópteros como hacían con las AUC a los que están generando miedo en el Chocó, en Norte de Santander o en el Valle del Cauca… la verdad, eso hoy sería complicadísimo de afirmar o comprobar.  ]

[Se podría aceptar que “paramilitarismo” no existe, tal y como afirma Villegas, pero lo que sería una completa locura y una burla con las comunidades, es salir a decir que se trata de “delincuencia común”. No son delincuentes comunes, son ejércitos que aunque hoy no están trabajando en paralelo con las Fuerzas Militares o con el Estado, sí continúan al servicio de una clase terrateniente muy definida en el país, asimismo, continúan favoreciendo intereses de grandes empresas nacionales y multinacionales y últimamente se ha denunciado que muchos oportunistas compraron tierras sin saber que habían sido despojadas violentamente.]

[En conclusión, aunque aceptáramos la tesis del señor Villegas afirmando que el paramilitarismo no existe en Colombia, eso de ninguna manera conduciría a concluir que por tanto los que están amenazando a las comunidades son simple y llana “delincuencia común” ¡NO! allí hay intereses muy poderosos, los mismos de siempre, los que financiaron Chulos, Pájaros, Paracos etc. a esos es que vale la pena identificar, pues son los que salen más beneficiados del ultraje a las comunidades, vale la pena voltear la mirada a quienes se oponen a restitución de tierras, a los que abogan por megaproyectos extractivos o a los que andan comprando de buena fe, una que otra tierrita para sus hijos.  ]

#### **[Ver más columnas de opinión de Johan Mendoza](https://archivo.contagioradio.com/johan-mendoza-torres/)** 

------------------------------------------------------------------------

###### Las opiniones expresadas en este artículo son de exclusiva responsabilidad del autor y no necesariamente representan la opinión de la Contagio Radio
