Title: Guardia campesino que acudió a llamado de auxilio fue asesinado en Corinto, Cauca
Date: 2019-03-19 17:49
Category: Comunidad, DDHH
Tags: Asesinatos contra campesinos, Corinto, guardia campesina
Slug: asesinan-guardia-campesina-corinto
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/03/Guardia-indígena.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: 

###### 19 Mar 2019 

El pasado 17 de marzo en [la vereda Miravalle de Corinto, Cauca fue asesinado]John Jairo Noscué, joven integrante de la Guardia Campesina que trabajaba en el [punto de control del sector El Tablón, tras acudir a un llamado de auxilio enviado en medio de un tiroteo que tuvo lugar en el sector conocido como 'La Cancha'.]

**Según Henry Danilo Rojas, integrante del Comité de Derechos Humanos de la Asociación de Reservas Campesinas de Corinto**, John Jairo era un joven activo dentro de la comunidad de El Tablón, trabajando en conjunto con su padre, "siempre demostró ser una persona comprometida con el tema de la seguridad campesina", recuerda Rojas. [(Lea también: Asesinan a Argemiro López, líder social y promotor de sustitución en Tumaco)](https://archivo.contagioradio.com/asesinan-a-argemiro-lopez-lider-social-y-promotor-de-sustitucion-de-cultivos-en-tumaco/)

A pesar de que los hechos son materia de investigación, lo que ha podido establecer la comunidad es que un grupo de personas armadas llegó a la vereda Miravalle e inició un tiroteo en el área de 'La Cancha' al parecer con intenciones de asesinar a otro comunero, ante el llamado de auxilio de las personas presentes,**  el joven Noscue acudió a ante la emergencia y perdió la vida al ser impactado por varios disparos.**

"Una de las visiones que se tiene dentro de la guardia campesina es acudir al llamado de quienes se encuentren en riesgo para repeler ataques indiscriminados" explica el defensor de DD.HH quien señala que los grupos armados que hacen presencia en la región como el EPL y Los Pelusos, tienen a Miravalle como punto estratégico para sus actividades.

Tras la muerte del joven comunero, **la población permanece en alerta** mientras la guardia campesina afirma que **fortalecerá los puestos de control y la seguridad del lugar** para impedir que personas ajenas a la vereda tengan acceso al territorio, y así evitar que se presenten nuevos hechos de violencia como el acontecido el pasado 17 de marzo y del que esperan, las autoridades puedan dar con los responsables.

###### Reciba toda la información de Contagio Radio en [[su correo]
