Title: 120 paramilitares se toman comunidad en la cuenca del río Cacarica
Date: 2015-10-07 15:45
Category: DDHH, Nacional
Tags: Autodefensas Gaitanistas de Colombia, cacarica, Curvarado, Defensoría del Pueblo, Ministerio del Interior, Paramilitares en Chocó, Paramilitarismo, Territorios colectivos de comunidades negras
Slug: 120-paramilitares-se-toman-comunidad-en-la-cuenca-del-rio-cacarica
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/10/paramilitarismo-en-Choco-foto1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### foto:contagioradio.com 

<iframe src="http://www.ivoox.com/player_ek_8847302_2_1.html?data=mZ2hmZiUdo6ZmKiakp2Jd6KnmpKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRdZOkjNXO1MbRrc3d1cbfx9iPt8af1dTaw9OPp9Dh1tPWxsbIb8bijNHOjcjZqc%2FXwpDRx9GPto6ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Habitante Cacarica] 

###### [7 oct 2015]

Habitantes de la Balsa, región del río Cacarica en el bajo Atrato chocoano, **denuncian que aproximadamente 120 paramilitares** de la estructura de las Autodefensas Gaitanistas de Colombia, **“gaitanistas”**, ingresaron a territorios colectivos de comunidades negras y han tomado el control de la comunidad de Balsa en donde se han desarrollado proyectos de tipo paramilitar luego de la Operación Génesis en 1997.

Los paramilitares de las AGC hyan obligado a pobladores de varias comunidades habitantes de la cuenca del Río Truandó, a asistir a reuniones en las que afirman que podrán seguir trabajando la tierra como hasta el momento, sin embargo las propias comunidades afirman que las mismas promesas las hacían antes de las grandes incursiones que acabaron con desplazamientos masivos en la región.

Los pobladores afirman que **en la zona hay presencia militar**, y además se construyó de manera inconsulta una base militar binacional en territorio colectivo de las comunidades negras. Desde esa base las FFMM ejercen control territorial pero no actúan en contra de las estructuras paramilitares que no han dejado de tener presencia en varios de los ríos afluentes del Atrato.

**Recordemos** que en la Balsa se había suspendido un proyecto de banano “baby”, los pobladores de esta zona consideran que esta reaparición paramilitar podría obedecer a “reconstruir la estructura de esos proyectos de plátano o banano”.
