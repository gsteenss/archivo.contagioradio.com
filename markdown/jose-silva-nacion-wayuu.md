Title: Pueblos indígenas de ocho departamentos se suman a la Minga
Date: 2019-03-28 18:43
Category: Comunidad, Movilización
Tags: Minga en cauca, Minga Nacional
Slug: jose-silva-nacion-wayuu
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/03/D2xhK6KXQAELE0u.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: ONIC] 

###### [2019] 

[A propósito de los 18 días que completa la Minga Nacional del Cauca y atendiendo al llamado realizado por la Organización Nacional Indígena de Colombia (ONIC), diversas comunidades de los departamentos de **Chocó, Nariño, Valle, Huila, Tolima, La Guajira, Amazonía y Putumayo** han expresado su voluntad de unirse a esta movilización que ha unificado a los pueblos alrededor de una misma causa: exigir que se cumplan los acuerdos que por más de 30 años han sido incumplidos por el Gobierno ]

### **La Guajira** 

Uno de los departamentos que ha demostrado su solidaridad con la Minga en La Guajira donde las comunidades Wayúu, han decidido vincularse activamente a las manifestaciones pacíficas que han adelantado en el Cauca y a la que se han sumado otros sectores y organizaciones sociales e internacionales.

"Esta lucha que están sosteniendo nuestros hermanos indígenas del Cauca no es ajenas a nosotros como pueblo Wayúu" afirma José Silva presidente del movimiento quien indica que se trata de una defensa por el derecho al territorio,a la vida y a un "reconocimiento ancestral y milenario" de los pueblos indígenas de Colombia.

### **Boyáca** 

Por su parte, **el pueblo U’wa compuesto por más de 7.500 indígenas** ha declarado que se tomarán de forma pacífica el sector de Cedeño ubicado entre Boyacá y Norte de Santander y la región del Sarare que delimita los departamentos de Boyacá y Arauca.

[De igual forma no descartan unirse al gran paro nacional programado para el próximo 25 de abril, además de rechazar la práctica del fracking, la defensa por la salud y la educación han pedido al Gobierno que tenga voluntad política. [(Le puede interesar: Indígenas y Gobierno establecen mesa técnica pero la Minga continúa)](https://archivo.contagioradio.com/indigenas-del-cauca-y-gobierno-establecen-mesa-tecnica-sin-embargo-la-minga-continua/)]

### **Putumayo** 

Asímismo, desde el fin de semana pasado, las comunidades indígenas **Ingas y Kamentsa** en el Putumayo también se manifestaron a favor de la minga del suroccidente del país y se movilizaron  hasta la vía que comunica este departamento con el Nariño en el Valle de Sibundoy.

### **Tolima** 

[En el departamento del Tolima, alrededor de mil indígenas Pijaos y Nasa de organizaciones como **CRIT (Consejo Regional Indígena del Tolima), ARIT (Asociación De Resguardos Indígenas Del Tolima ARIT,) y FICAT (Federación Regional Indígena del Tolima)** provenientes de las comunidades de Natagaima, Prado, Saldaña, Guamo, Ibagué, Coyaima y San Antonio.]

[Aunque los bloqueos no se permanentes, los representantes de dichas comunidades se ubicaron en en las cercanías de **la vía que de Natagaima conduce a Neiva en el Huila** donde adelantan bloqueos por cerca de cuatro horas tanto en la mañana como en la noche.  [(Lea también Conversaciones con el Gobierno han sido estériles: Minga del Cauca) ](https://archivo.contagioradio.com/conversaciones-gobierno-esteriles-minga-del-cauca/)]

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
