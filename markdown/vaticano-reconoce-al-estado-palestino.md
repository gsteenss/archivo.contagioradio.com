Title: Vaticano reconoce al Estado Palestino
Date: 2015-05-13 17:41
Author: CtgAdm
Category: El mundo, Otra Mirada
Tags: Acuerdo entre Vaticano y Palestina reconoce dos estados, Jorge Mario Bergoglio, Mahmud Abás, ONU reconoce Palestina, Palestina, Papa Francisco, Vaticano reconoce estado palestino
Slug: vaticano-reconoce-al-estado-palestino
Status: published

###### Foto:Nacion.com 

###### 12 May 2015 

Después de la **visita del sumo Pontífice ha Tierra Santa en Mayo**, El **Vaticano** anuncia que firmará la próxima semana un acuerdo bilateral **que reconocerá al Estado Palestino. **Este anuncio coíncide con la **visita** de **Mahmud Abás** el siguiente **sábado al Vaticano**, donde será **recibido** por el Papa **Jorge Mario Bergoglio**.

Esta visita coincide con la **canonización de dos monjas nacidas en territorio Palestino** y se suma a los gestos de acercamiento del Papa hacía el reconocimiento de una Palestina independiente.

Según Monseñor Camilleri “...el auspicio de una **solución de la cuestión palestina y del conflicto** entre israelíes en el ámbito de la **solución de dos Estados**...”.

El acuerdo también pretende **auspiciar un acuerdo de paz entre Palestino e Israelíes**, de tal forma el representante de la Santa Sede ha dicho que “...El acuerdo podría servir para alentar de algún modo a la comunidad internacional, y en particular a las partes más directamente implicadas, a emprender una acción más decisiva para **contribuir a alcanzar una paz duradera**...”.

Aunque no es el único objetivo, también pretende aunar en la **regulación y las funciones de la Iglesia Católica en Israel y Palestina**, de tal manera se pueda gestionar mejor las actividades católicas en Tierra Santa.

Los antecedentes de este acuerdo se remontan a **1994** cuando se reestablecieron las relaciones entre la **OLP y el Vaticano**, reforzado en 2000 por un acuerdo de cooperación y cuando la **ONU reconoció al estado Palestino,** el Vaticano envío un comunicado aceptando la creación de dos estados.

Después de la visita del Papa a Cuba y su entrevista con Fidel Castro, ha quedado claro el **nuevo carácter de las relaciones del Vaticano con la comunidad internacional** y el giro de su política exterior de cara a países que antes no eran recibdos por la Santa Sede.
