Title: Distrito pretende bajar el telón del Teatro La Mama
Date: 2016-07-01 14:38
Category: Cultura, Nacional
Tags: Cultura, Teatro la mama
Slug: distrito-pretende-bajar-el-telon-del-teatro-la-mama
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/07/teatro-la-mama.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Imagen09 

###### [01 Jul 2016] 

**El histórico teatro La Mama se quedará sin sede** debido a que el Instituto de Desarrollo Urbano (IDU), dueño del predio, le solicito al teatro **la devolución del lugar** para poder tener un espacio en donde reunirse con la comunidad y realizar otro tipo de actividades. Además señalan que sobre este emblemático escenario se realizará la ampliación de la calle 63 que conectará con con la carrera Séptima.

El teatro La Mama y el IDU tenían un contrato de comandato que se renovaba cada año con la finalidad de que el grupo de teatro pudiera permanecer ahí, sin embargo para esta ocasión **no se renovará debido a que este previo estaba incluido en el Plan de Desarrollo Distrital** como uno de los terrenos que volverían a ser parte de la alcaldía para ser usado como centro de reuniones y encuentros con la comunidad o un lugar para disponer material administrativo, mientras se inicia la ampliación de la calle 63.

En un comunicado de prensa el Teatro La Mama ha expresado que **sienten una gran preocupación ya que este hecho significa quedarse sin sala**, que no solo implica una pérdida para las personas que pertenecen al teatro, sino para toda la población capitalina, ya que se **cerrarían las puertas de  uno de los teatro más importantes y con mayor trayectoria en el país.**

El día de hoy el IDU dio a conocer en un comunicado que se reunirá la próxima semana con el representante legal del Teatro La Mama para tratar de establecer **un tiempo prudencial en el que el centro artístico pueda encontrar otro lugar donde re ubicarse,** con el apoyo del IDU.

###### Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)] 
