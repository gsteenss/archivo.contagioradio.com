Title: Compromiso de la Comisión de la Verdad es poner a la luz  la resistencia afrocolombiana
Date: 2020-07-18 11:01
Author: CtgAdm
Category: Actualidad, Nacional, Paz
Tags: Comisión de la Verdad, Comunidades afrodescendientes
Slug: compromiso-de-la-comision-de-la-verdad-es-poner-a-la-luz-la-resistencia-afrocolombiana
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/07/49194610701_5cec8fb2f7_c.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","fontSize":"small"} -->

Foto: Población afrocolombiana en Quibdó/ Comisión de la Verdad

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

A propósito del reconocimiento público que realizará la [Comisión de la Verdad](https://twitter.com/ComisionVerdadC)durante el mes de agosto sobre los impactos del racismo estructural y conflicto en las comunidades afrocolombianas, negras, raizales y palenqueras, sus líderes y lideresas también buscan exaltar sus formas de resistencias y de contribuir a la paz desde el territorio.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

**Orlando Castillo, líder del espacio humanitario Puente Nayero en Buenaventura** señala que en el marco del conflicto se ha venido agudizando la violencia en los territorios étnicos pese a la firma del Acuerdo de Paz, sobre todo por el incumplimiento por parte del Estado, lo que ha derivado que el Pacífico sea impactado por actores armados que hacen presencia en los territoriosuna razón más para reiterar la necesidad de que el Gobierno entable un cese bilateral con el ELN y abra la puerta al diálogo con otros grupos armados. "Nos hemos compartido en el principal objetivo de muerte en el país y no podemos desconocer que muchos de esos casos se han dado por la ausencia del Estado".

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Pese a ello, exalta los procesos de resistencia que se dan en el contexto urbano y rural del litoral y que han permitido que las comunidades sigan arraigadas al territorio, **"una de las grandes lecciones que el pueblo afro ha dado, es la forma de resistir"** resaltando cómo pese a las amenazas el pueblo negro continúa en pie de lucha.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

"Queremos que Colombia conozca los hechos que han ocurrido y nuestras formas de resistencia y es desde esos espacios como la Comisión de la Verdad que queremos saber quiénes han sido los culpables de lo que ha pasado en nuestros territorios", afirma Castillo, señalando que en el caso puntual del Naya es esencial que se conozca la verdad del territorio y cómo ha sido afectado el grupo humano que ha vivido en este, en particular las mujeres.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Después del Acuerdo la violencia contra los pueblos negros continúa

<!-- /wp:heading -->

<!-- wp:paragraph -->

Sobre el rol de la mujer afro, **Alicia Mosquera, parte de la Asociación de Mujeres de Rio Sucio Chocó, CLAMORES**, afirma que se trata de "una lucha que se está dando con fortaleza en el territorio por nuestros hijos, nosotras como mujeres campesinas no hemos podido retornar a nuestras tierras, porque no tenemos garantías por parte del Gobierno".

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Una situación que denuncia Alicia, se viene presentando incluso más de 20 años de la Operación Génesis , que a través de fuerzas combinadas militares e irregulares -paramilitares generó quema de casas, saqueos, amenazas de muerte y posteriores desplazamientos que hoy se siguen viendos, sumados a los reclutamientos de menores de edad por parte de los actores armados que persisten en los territorios, una verdad, que señala la lideresa, debe conocerse y exigir respuestas al Estado. [(Lea también: En Turbo persiste la dignidad en medio de la amenaza paramilitar)](https://archivo.contagioradio.com/en-turbo-persiste-la-dignidad-en-medio-de-la-amenaza-paramilitar/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

**Al respecto, Audes Jiménez, coordinadora territorial del Atlántico-, Norte de Bolívar y San Andrés de la Comisión de la Verdad** señala que la pandemia ha puesto en evidencia la crisis que viven en particular las poblaciones étnico raciales por lo que desde la Comisión de la Verdad y producto de su mandato, "estamos empeñados en que se haga un diálogo con todo los sectores y actores del país que tuvieron alguna actuación contra el pueblo negro afroamericano, palenquero y raizal", algo que han denominado, **La ruta del Cimarronaje "**

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Asegura que dicho diálogo buscar resaltar los factores de persistencia y escuchar las voces de los colectivos y personas violentadas y avanzar hacia una propuesta de reconciliación acercándose a los responsables y su voz, "es un proceso largo pero necesariamente tenemos que aportar desde donde estamos a que este proceso pueda ser una realidad, no solo se requiere una justicia transicional si no entrar en caminos de resistencia y coexistencia porque vamos a seguir en los mismos territorios".

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### "La población afrocolombiana históricamente tiene una visión colectiva"

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Con el territorio como eje central de los pueblos étnicos, Orlando señala que este debe ser conservado pues es lo que permite que "nos mantengamos como pueblo y permita generar las condiciones para que perdure esa riqueza y diversidad cultural.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

**"El territorio es fundamental para nosotros, el territorio es vida y e sumamente para el pueblo negro no como un bien económico de riqueza sino de vida y este ha sido usurpado, satanizado y ha sido destruido por el conflicto armado",** algo que señala la comisionada Ángela Salazar está ligado al racismo estrcutural del país. [(Lea también: Comisión de la Verdad rechaza la estigmatización y reitera su compromiso con la paz)](https://archivo.contagioradio.com/comision-de-la-verdad-rechaza-la-estigmatizacion-y-reitera-su-compromiso-con-la-paz/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

"Es un antecedente primordial el hecho de que somos personas esclavizadas y eso se ha mantenido en el imaginario colectivo del pueblo colombiano y desde allí necesitamos otras miradas y otros análisis para saber qué es lo que ha pasado con los pueblos negros"- explica, - "queremos esas verdades del sentir de cómo se me afectó a mí pero se afectó a mi comunidad, a mi familia a mi territorio".

<!-- /wp:paragraph -->

<!-- wp:quote -->

> La Comisión de la Verdad se acaba pero la verdad sigue construyéndose y esa es la que se debe construir
>
> <cite>Ángela Salazar</cite>

<!-- /wp:quote -->

<!-- wp:paragraph {"align":"justify"} -->

La comisionada afrocolombiana, quien ha enfocado su trabajo principalmente en mejorar los derechos de las mujeres trabajadoras bananeras en Urabá señaló que a través del capítulo étnico de la Comisión de la Verdad y su subcapítulo afro, se buscará un reconocimiento de las afectaciones y de la lucha y la resistencia histórica para mantenerse en el territorio y alzar la voz. [(Lea también: Primeras medidas cautelares étnicas de la JEP cobijan a comunidades del Bajo Atrato)](https://archivo.contagioradio.com/primeras-medidas-cautelares-etnicas-de-la-jep-cobijan-a-comunidades-del-bajo-atrato/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

"Tenemos que lograr que la sociedad colombiana entienda que tenemos los mismos derechos y que cuando se habla de desarrollo en nuestras comunidades es que consulten con los territorios afro, raizales, y palanqueros ese desarrollo que se quiere implementa y nos sentemos todos a trabajar en ese desarrollo sostenible", concluye la comisionada con relación a un trabajo que debe realizar la Comisión a lo largo de tres años de los cuales resta menos de la mitad de su tiempo límite.

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->
