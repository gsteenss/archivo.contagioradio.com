Title: Altos precios del dólar salvaron al Gobierno del paro nacional cafetero
Date: 2019-06-11 16:42
Author: CtgAdm
Category: Movilización, Nacional
Tags: Café, Cafeteros, Dólar, Paro Nacional
Slug: altos-precios-del-dolar-salvaron-al-gobierno-del-paro-nacional-cafetero
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/Paro-Nacional-Cafetero.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo  
] 

Organizaciones y gremios aplazaron la convocatoria a un paro nacional cafetero que se realizaría el pasado lunes 10 de junio dados los actuales precios del dólar y el costo que representa para el valor del grano. La postergación no significa que los problemas que los llevaron a pensar en el paro esten resueltos, pues como explicó **Fabio Arias, fiscal de la Central Unitaria de Trabajadores** (CUT), se requieren soluciones de fondo para garantizar que las más de 500 mil familias que viven directamente del Café, obtengan rentabilidad de su producción.

### **¿Por qué hicieron un llamado al paro nacional cafetero?**

La convocatoria a este paro inició el pasado 24 de abril, día en que una multitudinaria marcha de cafeteros se tomó Armenia, Quindío, para protestar por los precios bajos del grano, entre otras situaciones. **Allí, las organizaciones plantearon soluciones a la crisis económica del sector, y advirtieron que de no ser escuchados por el Gobierno** Nacional, convocarían a un paro. (Le puede interesar: ["Miércoles 24 de abril, cafeteros marchan en Armenia por crisis del sector"](https://archivo.contagioradio.com/cafeteros-marchan-armenia-crisis/))

La convocatoria ocurrió hacía finales de mayo, momento en que **las condiciones de producción se hicieron insostenibles para los caficultores y definieron la hora cero del paro para el pasado lunes 10 de junio**. No obstante, la fecha se aplazó porque el precio del dólar en Colombia, cercano a los 3.400 pesos, resultaba favorable para que productores recogieran sus cosechas y negocien con los grandes compradores.

### **Las circustancias que llevan al paro de cafeteros se mantienen**

Fabio Arias, fiscal de la Central Unitaria de Trabajadores (CUT), afirmó que **si bien es cierto que ahora pueden haber unas circunstancias coyunturales como el precio de la divisa extranjera, en términos generales hay un problema muy serio con el precio del café a nivel mundial,** pues "lo han venido reduciendo".

Una parte del problema proviene del control casi total que tienen solo 6 industrias del mercado, que son las que finalmente imponen la tasa de compra. Además, la aparición de otros países productores de Centro América y Asia que son capaces de exportar grandes cantidades del grano, han generado afectaciones en los cafeteros del país.

En esa medida, Arias explicó que la actual coyuntura presenta un precio estable del café, que según los propios productores llegaba a ser hasta 100 mil pesos menos de lo necesario para generar rentabilidad. Por esa razón, cada vez que el precio del dólar en relación con el peso disminuya, los cafeteros producirán a pérdidas sin que "la Federación (Nacional de Cafeteros) ni el Gobierno presten atención al tema", concluyó el Fiscal de la CUT. (Le puede interesar: ["Los caficultores están pensando en un paro nacional"](https://archivo.contagioradio.com/caficultores-pensando-paro-nacional/))

Por estas razones, el dirigente gremial sostuvo que **el paro se aplazó, "pero las circustancias por las cuales se hace se mantienen",** y agregó que la organización que él integra, estará lista para apoyarlos en sus movilizaciones. Entre las exigencias que se harían en el marco del paro están el aumento del aporte del Gobierno para crear un fondo de estabilización cafetero, la reducción en impuestos para los insumos utilizados en la producción del grano y la puesta en marcha de créditos de fomento e innovación técnica.

<iframe id="audio_36970025" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_36970025_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
