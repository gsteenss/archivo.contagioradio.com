Title: Este fin de semana se realizará el V Encuentro de Zonas de Reserva Campesina
Date: 2016-12-02 15:33
Category: Movilización, Nacional
Tags: Implementación de Acuerdos, zonas de reserva campesina
Slug: este-fin-de-semana-se-realizara-el-v-encuentro-de-zonas-de-reserva-campesina
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/12/zonas-de-reserva.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Prensa Rural] 

###### [2 Dic 2016]

Este fin de semana se llevará a cabo en Curumaní, Cesar, el V Encuentro de Zonas de Reservas Campesinas. Evento que pretende  reivindicar las zonas de reserva campesinas como una iniciativa de paz y **prepararlas de cara a todo lo que se vienen gestando desde la implementación de los Acuerdos de Paz de La Habana.**

Esto debido a que **la gran mayoría de zonas veredales están ubicadas en zonas de reserva campesina o muy cercana a ellas**, puesto que han sido estos territorios los que más han sufrido los impactos de la guerra.  Otro de los objetivos de este encuentro radica en abordar temas como Mujer, Ambiente, conflictos interculturales, con el ideal de construir nuevos escenarios desde donde gestar paz.

Además para Cesar Jerez vocero de la Asociación Nacional de Zonas de Reserva Campesina, se espera que “se tenga un a**vance programático significativo** y así el domingo 4 tener un panorama mucho más claro en el qué hacer de las zonas de reserva campesinas en el país”. Le puede interesar:["Cumbre Agraria suspende negociaciones con el gobierno"](https://archivo.contagioradio.com/cumbre-agraria-suspende-negociaciones-con-gobierno/)

Durante el evento se tendrán mesas de trabajo frente a cada uno de los puntos de implementación de los acuerdos y de los puntos que las comunidades han propuesto para que las zonas de reservas campesinas tengan garantías en su permanencia como el hecho de crear el Sistema y programa de zonas de reserva campesina que genere **más amplitud en acceso a la tierra, formalización de la propiedad de las fincas  campesinas** y mayor presencia estatal.

Para finalizar frente a las expectativas del cumplimiento del gobierno frente a las financiación y apoyo a las zonas de reserva campesinas, Jerez afirma que hacen parte de los acuerdos de paz y que en dado caso de que las instituciones no acciones a través de la colaboración, deberá ser el movimiento social el que exija que se implemente lo acordado. Se espera que al Encuentro **lleguen aproximadamente 2.000 personas** que hacen parte de las diferentes zonas de reservas campesinas del país, como las del Putumayo, Cauca, entre otras.

<iframe id="audio_14453640" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_14453640_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
