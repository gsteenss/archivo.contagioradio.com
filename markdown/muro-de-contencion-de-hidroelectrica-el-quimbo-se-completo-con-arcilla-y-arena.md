Title: Muro de contención de Hidroeléctrica El Quimbo se completó con "arcilla y arena"
Date: 2017-04-24 16:27
Category: Ambiente, Nacional
Tags: ASOQUIMBO, El Quimbo
Slug: muro-de-contencion-de-hidroelectrica-el-quimbo-se-completo-con-arcilla-y-arena
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/04/001-el-quimbo-e1493069174534.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Noticias Al Sur] 

###### [24 Abr 2017] 

La Asociación de Afectados por el proyecto Hidroeléctrico del Quimbo, denunciaron que el muro de contención de esta infraestructura tiene una fisura provocada por construir con materiales inadecuados, y que por esa razón **se podría generar una catástrofe sobre el departamento del Huila, que se evitaría si la ANLA ordena desmantelar la represa del Quimbo.**

De acuerdo con Miller Dussan, docente de la Universidad SurColombiana e integrante de AsoQuimbo, la base del muro, es decir la parte más ancha, tendría varias filtraciones de agua. Según un estudio **las fallas son provocadas por la mezcla de arcilla y arena**, materiales que con el tiempo son arrastradas por el agua. Esa fisura, para los geólogos, estaría a punto de llegar al límite permitido. Le puede interesar: ["El Quimbo ha dejado 1.500 familias en la pobreza absoluta"](https://archivo.contagioradio.com/hidroelectrica-el-quimbo-deja-pobreza-absoluta/)

Además, la represa solo tiene tres instrumentos sismológicos ubicados sobre el muro que no registran la actividad sísmica de toda la represa, este hecho para Dussan, significa que cualquier sismo inducido **podría generar una catástrofe sin que puedan generarse planes o medidas de contención**, debido a la falta de información que se tendría para poder realizarlos.

Otros estudios realizados por la Contraloría General de la República soportarían esta investigación, que a su vez, es de conocimiento para la ANLA y la empresa EMGESA quienes hicieron presencia de una **audiencia pública realizada el pasado 11 de noviembre en donde se pusieron de manifiesto estos riesgos**. Le puede interesar: ["Audiencia Pública busca frenar el Quimbo" ](https://archivo.contagioradio.com/audiencia-publica-busca-frenar-quimbo/)

AsoQuimbo está solicitando que se desmantele la represa, esto teniendo en cuenta que la empresa EMGESA ya se encuentra vaciando el Quimbo, debido a que tiene que corregir las fallas. Dussan señaló que “la Autoridad Nacional de Licencias Ambientales, el Ministerio de Ambiente y EMGESA **son los responsables de que exista una situación dramática con el proyecto Hidroeléctrico**”.

<iframe id="audio_18314223" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_18314223_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)
