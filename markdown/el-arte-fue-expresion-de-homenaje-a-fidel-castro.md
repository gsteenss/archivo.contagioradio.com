Title: El arte fue expresión de homenaje a Fidel Castro
Date: 2016-11-26 12:35
Category: Cultura, Otra Mirada
Tags: Cuba, Fidel Castro, Revolución Cubana
Slug: el-arte-fue-expresion-de-homenaje-a-fidel-castro
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/Cuba-Fidel-Castro-90-SF-10.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:  90 años Fidel Castro/REU] 

###### [26 Nov 2016] 

A  lo largo de su vida, Fidel Castro fue un hombre que produjo tanto odios como amor en el mundo, grandes escritores, artistas, políticos y músicos reflejaron en el arte, el culto, el amor y la admiración al Comandante en Jefe, que fallece a sus 90 años. Canciones como “Y en eso llegó Fidel” de Carlos Puebla o el mítico son cubano de Celina y Reutilio titulado “Que viva Fidel”, son tan solo algunas de las miles de letras, entre cuentos, poemas, crónicas, pinturas, que homenajearon a Fidel Castro.

Poema de Ernesto “Ché” Guevara a Fidel Castro 
---------------------------------------------

Vámonos ardiente profeta de la aurora,

por recónditos senderos inalámbricos

a liberar el verde caimán que tanto amas.

Vámonos, derrotando afrentas con la frente

plena de martianas estrellas insurrectas,

juremos lograr el triunfo o encontrar la muerte.

Cuando suene el primer disparo y se despierte

en virginal asombro la manigua entera,

allí, a tu lado, serenos combatientes,

nos tendrás.

Cuando tu voz derrame hacia los cuatro vientos

reforma agraria, justicia, pan, libertad,

allí, a tu lado, con idénticos acentos,

nos tendrás.

Y cuando llegue el final de la jornada

la sanitaria operación contra el tirano,

allí, a tu lado, aguardando la postrer batalla,

nos tendrás.

El día que la fiera se lama el flanco herido

donde el dardo nacionalizador le dé,

allí, a tu lado, con el corazón altivo,

nos tendrás.

No pienses que puedan menguar nuestra entereza

las decoradas pulgas armadas de regalos;

pedimos un fusil, sus balas y una peña.

Nada más.

Y si en nuestro camino se interpone el hierro,

pedimos un sudario de cubanas lágrimas

para que se cubran los guerrilleros huesos

en el tránsito a la historia americana.

Nada más

Poema de Pablo Neruda a Fidel Castro 
------------------------------------

Fidel, Fidel, los pueblos te agradecen

Palabras en acción y hechos que cantan

Por eso desde lejos te he traido

una copa del vino de mi patria:

Es la sangre de un pueblo subterráneo

que llega de la sombra de tu garganta.

Son mineros que viven hace siglos

sacando fuego de la tierra helada.

Van debajo del mar por los carbones

y cuando vuelven son como fantasmas.

Se acostumbraron a la noche eterna,

les robaron la luz de la jornada

y sin embargo aquí tienes la copa

de tantos sufrimientos y distancias:

La alegría del hombre encarcelado,

poblado de tinieblas y esperanzas

que adentro de la mina sabe cuando

llegó la primavera y su fragancia

porque sabe que el hombre está luchando

hasta alcanzar la claridad más ancha.

Y a Cuba ven los mineros australes,

los hijos solitarios de la Pampa,

los pastores del frío en Patagonia,

los padres del estaño y de la plata.

Los que casándose con la cordillera

sacan el cobre en Chuquicamata.

Los hombres de autobuses escondidos

en poblaciones puras de nostalgia.

Las mujeres de campo y talleres

Los niños que lloraron sus infancias

Esta es la copa, tómala Fidel

Está llena de tantas esperanzas

que al beberla sabrás que tu victoria

es como el viejo vino de mi patria

No lo hace un hombre

sino muchos hombres

No una uva sino muchas plantas:

No es una gota sino muchos ríos:

No un capitán sino muchas batallas:

Y están contigo porque representas

todo el honor de nuestra lucha larga

Y si cayera Cuba caeríamos

y vendríamos para levantarla.

Y si florece con todas sus flores

florecerá con nuestra propia savia.

Y si se atreven a tocar la frente

de Cuba por tus manos libertadas

encontrarán los puños de los pueblos

sacaremos las armas enterradas:

la sangre y el orgullo acudirán

a defender a Cuba bienamada.

Apartado del libro “Espejos” de Eduargo Galeano a Fidel Castro 
--------------------------------------------------------------

Sus enemigos dicen que fue rey sin corona y que confundía la unidad con la unanimidad.  
Y en eso sus enemigos tienen razón.  
Sus enemigos dicen que si Napoleón hubiera tenido un diario como el “granmma”, ningún francés se habría enterado del desastre de Waterloo.  
Y en eso sus enemigos tienen razón.  
Sus enemigos dicen que ejerció el poder hablando mucho y escuchando poco, porque estaba más acostumbrado a los ecos que a las voces.  
Y en eso sus enemigos tienen razón.  
Pero sus enemigos no dicen que no fue por posar para la Historia que puso el pecho a las balas cuando vino la invasión, que enfrentó a los huracanes de igual a igual, de huracán a huracán, que sobrevivió a 637 atentados, que su contagiosa energía fue decisiva para convertir una colonia en patria, y que no fue por hechizo de Mandinga ni por milagro de Dios que esa nueva patria pudo sobrevivir a 10 presidentes de los estados unidos, que tenían puesta la servilleta para almorzarla con cuchillo y tenedor.  
Y sus enemigo no dicen que Cuba es un raro país que no compite en la copa mundial del felpudo.  
Y no dicen que esta revolución, crecida en el castigo, es lo que pudo ser y no lo que quiso ser. Ni dicen en gran medida el muro entre el deseo y la realidad fue haciéndose mas alto y mas ancho gracias al bloqueo imperial, que ahogó el desarrollo de una democracia a la cubana, obligó a la militarización de la sociedad y otorgó a la burocracia, que para cada solución tiene un problema, las coartadas que necesita para justificarse y perpetuarse.

Y no dicen que a pesar de todos los pesares, a pesar de las agresiones de afuera y de las arbitrariedades de adentro, esta isla sufrida pero porfiadamente alegre ha generado la sociedad latinoamericana menos injusta.  
Y sus enemigos no dicen que esa hazaña fue obra del sacrificio de su pueblo, pero también fue obra de la tozuda voluntad y el anticuado sentido del honor de este caballero que siempre se batió por los perdedores, como aquel famoso colega suyo de los campos de Castilla.

"Y en eso llegó Fidel" canción escrita por Carlos Puebla a Fidel Castro 
-----------------------------------------------------------------------

https://www.youtube.com/watch?v=NwTIfVG1i\_g&feature=share

"Que viva Fidel" canción de Celina y Reutilio adaptada de la canción "Que viva Changon"
---------------------------------------------------------------------------------------

https://www.youtube.com/watch?v=2S\_OYual-Eg

"Muchas razones a defender" canción cubana en homenaje al Comandante en Jefe Fidel Castro
-----------------------------------------------------------------------------------------

https://www.youtube.com/watch?v=P5dR06bqQ5s
