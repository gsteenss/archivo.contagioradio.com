Title: Urabá fue laboratorio de lo que pasó entre militares y paramilitares en el resto del país: Coronel (R) Velásquez
Date: 2020-06-08 15:02
Author: CtgAdm
Category: Actualidad, Otra Mirada, Programas
Slug: uraba-fue-laboratorio-de-lo-que-paso-entre-militares-y-paramilitares-para-el-pais-coronel-r-velasquez
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/06/coronel-2.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

El Coronel (R) fue el segundo al mando de la Brigada XVII en Urabá, entre 1995 y 1996, luego, por apartarse de la manera de operar de su comandante fue removido del cargo poco a poco. Afirma que una muestra de ese "laboratorio" fueron las masacres ocurridas en 1995 que dejaron una gran cantidad de muertos, pero después, con Rito Alejo al mando, terminaron las masacres pero los muertos a cuentagota multiplicaron las víctimas.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

A todo ello lo denominó como *"el laboratorio que permitió experimentar y dar paso a los hechos que se han repetidos por años en todo el país".*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El coronel (r) agregó que el periodo entre 1995 fue uno de los mas violetos en Apartadó debido a que los militares eran evaluados por la cantidad de bajas que producían, ***"podría decirse que los paramilitares eran quienes les ayudaban a conseguir estas bajas enemigas disfrazando civiles o lo que fuera de guerrilleros".***

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Al visibilizar esta preocupante situación Velásquez propuso una estrategia diferente en la que las acciones del [Ejército](https://www.justiciaypazcolombia.com/se-incrementa-pie-de-fuerza-en-el-guayabero-bombardeos-indiscriminados-campesinos-heridos-capturados-y-desaparecidos-en-la-vereda-tercer-milenio/) se midieran por ***"las masacres impedidas, capturas con menos muertes, y en general intervenciones con la mejor cantidad de heridos***".

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Mientras bajaban las masacres y en su mayoría los indices de violencia en el Urabá sobre el año 1996, *"**el gobernador de Antioquia, Uribe, declaró públicamente que el Urabá estaba necesitando con urgencia cascos azules,** como para mediar en ese conflicto, porqué la nueva estrategia no funcionaba",* agregó el Velásquez.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

La entonces alcaldesa de Urabá, testigo de todo lo que pasaba en el territorio, *"y gran líder señaló que no se necesitaban los cascos azules, porque el ejército presente allí estaba cumpliendo con su labor como tenía que ser"*; comentario que según Velásquez nunca fue escuchado así como la estrategia propuesta por él, al contrario fue eliminada para retomar lo que se venía haciendo.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Al recordar estos hechos señaló, *"si en su momento el **mando militar que recibió el informe hubiera investigado bien lo que pasaba en Urabá con respecto a los paramilitares, no hubiera llegado al nivel tan preocupante** de desaparición forzada que tiene hoy Colombia"*.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Y reconoció que, *"luego de la desmovilización de los paramilitares, los militares y suboficiales quedaron sin esa herramienta, entonces es ahí cuando empiezan a hacer ellos mismos lo que antes hacía la insurgencia".* (Le puede interesar: [Luego de 13 años condenan a militares por 'Falsos Positivos' en Vista Hermosa, Meta](https://archivo.contagioradio.com/luego-de-13-anos-condenan-a-militares-por-falsos-positivos-en-vista-hermosa-meta/))

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Hay presidentes que deben responder políticamente por lo que hicieron los paramilitares, en Urabá y en toda Colombia

<!-- /wp:heading -->

<!-- wp:paragraph -->

Frente a los actos que evidenció y denunció, señaló que hay una responsabilidad de la verdad dividida, una por parte del Ejército y otra políticas, ***"durante este conflicto muchos gobiernos por no decir todos, han dicho que este problema es de los militares y se desentienden de sus responsabilidades intelectuales".***

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Si bien Velásquez desconoce la responsabilidad de las FFMM, afirmó que *"el desentendimiento de los políticos hizo que algunos militares interpretarán sus discursos como bien le pareció; **los políticos no fueron claros con lo que querían hacer o tal vez sí, pero lo disfrazan en sus discursos para lavarse las manos"**.*

<!-- /wp:paragraph -->

<!-- wp:quote -->

> *"**Estamos acostumbrados a mandar y obedecer, y en esa dinámica las órdenes deben ser claras, precisas, viables, y oportunas**, el militar no está acostumbrado a un discurso político para tratar de captar el mensaje de lo que hay que hacer"*
>
> <cite>****Carlos Velásquez**,** **Coronel** (r)</cite>

<!-- /wp:quote -->

<!-- wp:paragraph -->

El coronel (r) de modo similar analizó lo que pasó hace algunos meses en el marco del paro nacional, *"imagino a Duque dando la orden de investigar quién estaba detrás del Paro, y **Nicacio interpretando eso como empezar a [perfilar](https://archivo.contagioradio.com/prensa-bajo-la-mira/) e investigar a todo mundo"**.*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

De esa manera para Velásquez, la responsabilidad frente a varios de los acontecimientos que hoy recibe la JEP de los Militares, son responsabilidad de Álvaro Uribe, *"Uribe tiene que asumir su responsabilidad política, lo cual dudo mucho; pero como él los presidentes que encabezaron estas politicas de violencia"*.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Un antes y un después de pertenecer al Ejército Nacional

<!-- /wp:heading -->

<!-- wp:paragraph -->

El Coronel (r) Carlos Velásquez, afirmó que el deterioro de la imagen del Ejército en el últimos meses no le duele, *"**en su momento salí decepcionado de la institución y especialmente de los mandos que no supieron lo que debían hacer".***

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Y resaltó que, *"**esta es una oportunidad de ver el proceso que se está desarrollando en el país**, una oportunidad para pensar más en asuntos políticos grandes que aporten a la paz, la verdad y la reparación".*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Sin embargo determinó que con el actual Gobierno no será posible buscar ese camino, y germinar las semilla pacificadoras, "***éste Gobierno no tiene margen de maniobra para no ser uribista, y fue algo claro desde el mismo día que Duque llamó a Uribe presidente eterno".***

<!-- /wp:paragraph -->

<!-- wp:paragraph {"fontSize":"small"} -->

*Le puede interesar ver: Otra Mirada, opinión, información y análisis| Avances y participación de militares en la JEP.*

<!-- /wp:paragraph -->

<!-- wp:html -->  
<iframe src="https://www.facebook.com/plugins/video.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2Fvideos%2F287060888993982%2F&amp;show_text=true&amp;width=734&amp;height=523&amp;appId" width="734" height="523" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowtransparency="true" allow="encrypted-media" allowfullscreen="true"></iframe>  
<!-- /wp:html -->

<!-- wp:block {"ref":78955} /-->
