Title: ONU pide no estigmatizar a comunidades movilizadas en Paro Nacional
Date: 2016-06-08 15:46
Category: Paro Nacional
Tags: colombia, Minga Nacional, ONU, Paro Nacional
Slug: onu-llamo-a-no-estigmatizar-a-comunidades-movilizadas-en-paro-nacional
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/06/esmad-minga-contagioradio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: geopoliting] 

###### [8 Jun 2016]

En un comunicado emitido esta tarde, la oficina de las Naciones Unidas en Colombia, ONU, llamó a no estigmatizar a las personas en medio de la Movilización del Paro Nacional convocado por la Cumbre Agraria tras los **múltiples incumplimientos por parte del gobierno nacional a los acuerdos alcanzados tras el Paro Agrario de 2013.**

La ONU afirma que es importante que se use un lenguaje “respetuoso que no exacerbe las posiciones y aleje el diálogo”. Respecto a ese tema la misma Cumbre Agraria se ha referido a los **[señalamientos](https://archivo.contagioradio.com/cumbre-agraria-solicito-medidas-cautelares-de-la-cidh/)que ha realizado el Ministro de Defensa Luis Carlos Villegas** en relación a supuestas infiltraciones de la guerrilla del ELN en las [protestas adelantadas en más de 100 puntos del país.](https://archivo.contagioradio.com/category/programas/paro-nacional/)

Además la ONU en Colombia resaltó la importancia de **investigar los asesinatos de Gersaín Cerón, Marco Aurelio Díaz y Willington Quibrecama Nequirucama**, “ocurridas en el contexto de la protesta social” y agrego que está a disposición de las autoridades para esclarecer los hechos que rodearon esas muertes.

También instó al gobierno colombiano a cumplir los acuerdos a los que se llegue con las [comunidades campesinas e indígenas](https://archivo.contagioradio.com/los-colores-de-la-minga-nacional/) puesto que ese cumplimiento será la única garantía de paz estable y duradera “**la construcción de una paz sostenible y duradera depende del seguimiento, cumplimiento y evaluación de los diferentes  compromisos adquiridos** por las Partes en el pasado” señala el comunicado.
