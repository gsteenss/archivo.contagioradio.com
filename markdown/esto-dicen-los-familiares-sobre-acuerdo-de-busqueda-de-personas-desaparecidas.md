Title: Esto dicen los familiares sobre acuerdo de búsqueda de personas desaparecidas
Date: 2015-10-19 12:19
Category: DDHH, Entrevistas
Tags: Acuerdo busqueda de desaparecidos, ASFADDES, Delegación de paz de las FARC, Delegación de paz del Gobierno, Derechos Humanos, Desaparecidos palacio de justicia, FARC-EP, Gobierno, Masacre de lso 19 comerciantes, MOVICE, Radio derechos Humanos
Slug: esto-dicen-los-familiares-sobre-acuerdo-de-busqueda-de-personas-desaparecidas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/08/desaparecidos.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: contagioradio 

###### [19 oct 2015] 

Familiares de víctimas de desaparición forzada afirman que el acuerdo sobre búsqueda de personas desaparecidas en el marco de las conversaciones de paz es un avance muy importante, pero para que realmente funcione debe estar acompañado de sensibilidad, responsabilidad y respeto para las víctimas.

El Movimiento Nacional de Víctimas de Crímenes de Estado (MOVICE), la Asociación de Familiares de Detenidos Desaparecidos (ASFADDES), las familias de los comerciantes desparecidos en el Magdalen medio, los familiares de las víctimas del Palacio de Justicia y personas que buscan a sus familiares y exigiendo al Estado entrega y ubicación de las víctimas de la desaparición forzada.

Esta posibilidad se hace cercana con el **anuncio del acuerdo para la búsqueda de los desaparecidos **que se realizó este domingo entre el gobierno y las FARC-EP en la Habana, en el que las dos partes se comprometieron a entregar información y ubicación para la búsqueda de desaparecidos en medio del conflicto armado.

<iframe src="http://www.ivoox.com/player_ek_9079343_2_1.html?data=mpWkm5iYd46ZmKiak5yJd6KkkpKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRidTo0JDRy8jJsozg0NiYyMbRrc3dwtfS1ZDXs8PmxpDOxdrJtsXjjMnSjceJh5SZo6bg09rJqMKfxcqah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Gloría Gómez, ASFADDES] 

Gloría Gómez, coordinadora de ASFADDES indica que este acuerdo es celebrado pero que **necesita de un “mecanismo ágil, inmediato y de calidad, para la búsqueda de las personas desaparecidas”** que en su momento “no se asumió con la responsabilidad y la seriedad que merecía”, pero esta vez se da una propuesta que incluye “**una comisión con la participación de organizaciones sociales y de derechos humanos**, que permite tener más confianza que esto no va a ser una figura u otro aparato en letra muerta”.

Gómez indica que con estas acciones "humanitarias" se **muestra voluntad por parte de los actores del conflicto** y ahora en lo que se insiste es en que a los desaparecidos “los busquen, los encuentren, localicen y entreguen en condiciones de dignidad y respetando las creencias y ritos” de las familias. Además reconoce que estas desapariciones han sido responsabilidad en "*mayor porcentaje del Estado y paramilitares, pero cabe responsabilidad a guerrillas y grupos de Narcotráfico*".

<iframe src="http://www.ivoox.com/player_ek_9079410_2_1.html?data=mpWkm5mVdI6ZmKiakp2Jd6KlmJKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRidTo0JDRy8jJsozg0NiYyMbRrc3dwtfS1ZDXs8PmxpDOxdrJtsXjjMnSjceJh5SZo6bg09rJqMKfxcqah5yncY6ZmKia&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Camilo Villa, MOVICE] 

Camilo Villa, Vocero del MOVICE, indica que éste es un “acuerdo histórico” ya que los familiares de personas desaparecidas siempre han reclamado las “serias limitaciones” de los aparatos oficiales de búsqueda y  al contemplar **que el Comité Internacional de la Cruz Roja sea protagonista de esta nueva comisión** dará una “luz de esperanza para las víctimas”.

Villa afirma la importancia de que las FARC entreguen listado de fosas y sitios clandestinos de enterramientos, pero además **la relevancia de investigar los cementerios militares ya que estos** “*son un hoyo negro, porque este tema no se ha tocado*” y las fuerzas del Estado también han tenido parte en estas desapariciones.

Sin embargo, frente al optimismo que despierta este acuerdo, no se puede serlo totalmente,  algunas víctimas como René Guarín, familiar de los desaparecidos del Palacio de Justicia **indica que la voluntad del Estado en los últimos años para buscar a los desaparecidos, con estrategias de “impunidad”**, es un motivo que genera desconfianza Guarín afirma que “la búsqueda debe partir de la verdad que pongan las partes involucradas, en 30 años no hemos tenido la verdad del Estado colombiano, ni del F2 ni del DAS, ni de las unidades que actuaron en la retoma del palacio de justicia”.

<iframe src="http://www.ivoox.com/player_ek_9079308_2_1.html?data=mpWkm5iUfI6ZmKiakpuJd6KpmJKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRb6bn1dSYxs7Hqc%2BfzdTgjcvFscrgysbfx9iPt9DW08qYw8jZqdPY0JDRx5DGaaSnhqeu1dbZqcXVjMmah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Fanny Corzo] 

Fanny Corzo, hermana de Reinaldo Corzo, víctima de la masacre de los 19 comerciantes en el Magdalena Medio, afirma que pese a que desde **años atrás se implementó la búsqueda de los desaparecidos** “si no hay seriedad, compromiso real del Estado, que caso tiene que saquen leyes y nuevas acciones si no se van a ejecutar, si no hay respeto, ni seriedad con los familiares víctimas”.

Las partes comparten la opinión que **para solucionar esta situación "hace falta un trabajo gigantesco"** y muchos de ellos concuerdan en la necesitad de que exista "un banco de datos genético" que acumule la información de los familiares de los desaparecidos", pero para esto se necesita de "**voluntad política, mucha confianza y un trabajo masivo con acompañamiento internacional**", como indica Camilo Villa, vocero del MOVICE.
