Title: Decreto que prentede regular las licencias ambientales es inconstitucional
Date: 2015-02-12 18:48
Author: CtgAdm
Category: Ambiente, Nacional
Tags: Ambiente, Licencias Exprés, Minambiente, Plan Nacional de Desarrollo
Slug: decreto-que-prentede-regular-las-licencias-ambientales-es-inconstitucional
Status: published

##### Foto: [elpueblo.com]

<iframe src="http://www.ivoox.com/player_ek_4075283_2_1.html?data=lZWkl5ecd46ZmKiakp6Jd6KllpKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRiMbX08rh0ZDXs8PmxpDZy8jJssTdwtiYw9LGrcbi1cbZx9iPqdSfytPQ0dPXuMro1sjW0dPFsI6ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Rodrigo Negrete] 

El abogado Rodrigo Negrete, **solicitó al tribunal la suspensión provisional del decreto de licencias ambientales, mientras se soluciona la demanda contra el Ministerio del Medio Ambiente,** que tiene como fin, anular el decreto que firmó el gobierno en octubre del 2014, por el cual se facilita y agiliza los procedimientos requeridos para obtener el permiso obligatorio, a las empresas que busquen construir carreteras, extraer petróleo y realizar minería.

Para Negrete, **el decreto viola la ley y es inconstitucional, de manera que en el marco de la ley 1437 de 2011,** existe la posibilidad de que se suspenda provisionalmente el decreto, mientras se logra resolver la demanda, lo que aceleraría todo el trámite de la demanda que se presentó ante el Consejo de Estado.

Así mismo, sucede con otra de las polémicas ambientales generadas desde el Plan Nacional de Desarrollo “Todos por un nuevo país”. **A partir del PND, se plantea la posibilidad de que las compañías puedan construir vías en medio de los parques naturales,** sin embargo, esta propuesta también resulta inconstitucional para el abogado ambientalista, quien además asegura que el Plan no solo viola la constitución con ese aparte del articulado, sino que también lo hace “**tratando de justificar el decreto de las ‘licencias express’ (licencias ambientales)”** y resalta que “no se puede amparar una ley inconstitucional con una posterior”.

Uno de los argumentos de la demanda contra el Minambiente, es que **el decreto nunca fue consultado con las comunidades indígenas y afrodescendientes** que viven en los territorios donde se realizan proyectos petroleros o mineros. Las comunidades son quienes realmente se ven afectadas por los daños ambientales, económicos y socioculturales, por cuenta del desarrollo de dichas obras y además son las que deben ser consultadas si se pretende hacer algún cambio en la legislación.

**“Estas situaciones deben partir de un consenso nacional,** el gobierno sigue desconociendo: procesos locales, intereses locales, necesidades locales, consultas populares (…) el presidente como jefe de estado debería buscar consensos y no generar unos mensajes que desde el centro se desconocen por la periferia”, dijo el abogado.

La consulta popular es un derecho consignado en la Constitución de 1991, allí se decreta que se  deben consultar proyectos que generen cambios significativos en el uso del suelo como el turismo y la minería, entonces, indica Rodrigo Negrete, **“no se entiende como el gobierno dice que las consultas populares son ilegales”.**

Colombia cuenta con gran cantidad de políticas ambientales, pero **“en el fondo están privilegiando las actividades sectoriales y el medio ambiente sigue siendo concebido como  un obstáculo”**, señaló el abogado.
