Title: Alianza País encabeza las elecciones en Ecuador
Date: 2017-02-20 15:02
Category: El mundo, Política
Tags: Elecciones Ecuador 2017, Lenín Moreno Garcés, Rafael Correa
Slug: alianza-pais-encabeza-las-elecciones-en-ecuador
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/02/elecciones-Ecuador.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Metro Ecuador ] 

###### [20 Feb 2017] 

Hasta el momento el candidato a la presidencia de Ecuador Lenín Moreno Garcés, encabeza las elecciones en Ecuador con un 39, 13% de los votos, sin embargo, según la Constitución ecuatoriana, para ganar la primera vuelta se debe contar con el 40% de los sufragios, de ser así, **algunos analistas advierten que habría una “fuerte conglomeración de la derecha” para obstaculizar el ascenso de Moreno en segunda vuelta.**

Federico Larsen, docente universitario e investigador, señaló que si bien la cultura política ecuatoriana ha cambiado en los últimos años y “Correa puede mostrarlo como un acierto de él y su Gobierno”, hay muchos **intereses económicos en proyectos de extracción petrolera y minera, “que pueden definir el futuro de Ecuador”.**

Advirtió que “la constelación de Alianza País no es del todo homogénea” y eso podría representar un riesgo para que las coaliciones de derecha y neoliberales “como la que podría encabezar Lasso”, impidan que Lenín Moreno gane la contienda electoral. ([Le puede interesar: Crónica desde las calles del Ecuador en campaña](https://archivo.contagioradio.com/cronica-desde-las-calles-del-ecuador-en-campana/))

### Los retos de la nueva presidencia ecuatoriana 

Por otra parte, Larsen indica que muchas de las políticas impulsadas por Rafael Correa como la revolución ciudadana y la nacionalización de algunas empresas, fue positiva y mejoró aspectos económicos y sociales del país vecino, pero es necesario que **“se modifique el patrón neoliberal (…) porque siguen existiendo conflictos frente al sistema institucional estatal”.**

Manifestó que el reto es lograr “un mayor peso a nivel latinoamericano” a través de la concreción de propuestas como el Banco del Sur y otras iniciativas que **“beneficien y propicien la unidad de los países del ALBA”.**

También indicó que el escepticismo en los países latinoamericanos ha venido creciendo en los últimos años, y podría ser una señal de la necesidad de **“modernizar y ajustar la política electoral, por ejemplo que haya más gente joven en las candidaturas”**, pues la mayoría de la población que se abstiene de votar “son jóvenes entre los 16 y los 25 años”.

Resaltó que otro de los retos del nuevo gobierno ecuatoriano, es “resolver conflictos que alejan a sectores del pueblo” como las organizaciones ambientalistas y de mujeres, quienes vieron frustrado el proyecto que legalizaría el aborto puesto que Rafael Correa “amenazó con dejar el cargo si se aprobaba la ley”, puntualizó Larsen.

Por último, varios medios locales han informado de protestas por parte de los simpatizantes de Guillermo Lasso frente al Consejo Nacional Electoral, **quienes manifiestan hubo fraude en el conteo de votos y exigen que haya segunda vuelta.**

\[caption id="attachment\_36585" align="alignnone" width="1200"\]![Escrutinios Ecuador](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/02/chart.jpeg){.size-full .wp-image-36585 width="1200" height="900"} Resultados Generales Ecuador - Lunes, 20 Feb. 3:20pm\[/caption\]  
<iframe id="audio_17117055" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_17117055_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
