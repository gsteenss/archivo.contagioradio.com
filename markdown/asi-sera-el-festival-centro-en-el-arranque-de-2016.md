Title: Así será el Festival Centro en el arranque de 2016
Date: 2015-12-11 12:53
Category: Cultura, eventos
Tags: Centro Cultural Gabriel García Márquez, Crack Family, Festival Centro 2016, Fundación Gilberto Alzate Avendaño, Programación Festival Centro, Teatro Jorge Eliécer Gaitán
Slug: asi-sera-el-festival-centro-en-el-arranque-de-2016
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/12/festival-centro-log.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### 11 Dic 2015

Bajo la consigna "Vuelve a tu centro", el lunes 11 de enero inciará el **Festival Centro 2016**, primer cita del año con la cultura musical en la ciudad, organizado por la Fundación Gilberto Alzate Avendaño, entidad que ha presentado la programación oficial y el listado de artistas que participarán durante los 7 días que dura el evento.

De acuerdo con el equipo encargado de la organización de esta séptima edición "El festival es una invitación a volver al centro, al centro de la ciudad, pero también al centro de uno mismo. El arte y la música son catalizadores que permiten la interiorización", juego semiótico del que deriva la selección del slogan e imagen del evento.

La programación del Festival Centro, está dividida por días y franjas, siendo la Franja Familiar, la encargada de dar apertura al evento, El día X el martes 12, Día Pop el miércoles 13, Día Rock el jueves 14, Día World music el viernes 15, Día Fiesta el sábado 16, y al cierre el 17 de enero la Franja Infantil.

Los escenarios escogidos para las actividades del Festival son El Auditorio y Muelle de la Fundación Gilberto Alzate Avendaño, El Teatro Jorge Eliécer Gaitán, El Centro Cultural Gabriel García Márquez y la Fundación Rafael Pombo, con actividades gratuitas en la mayor parte de los espacios.

<iframe src="https://www.youtube.com/embed/-zJcZOktXTs" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>  
**A continuación compartimos la programación del Festival:**  
**Lunes 11 de enero – Franja familiar**  
-Banda Sinfónica Infantil y Juvenil de Tunja  
-Banda Juvenil de Chía  
-Banda Municipal de Cota  
-Banda Sinfónica de Cogua  
Jóvenes talentos  
Puro Joropo, Evolución, CRHAM, Los Rosa, La Subienda, Juan Kamoru, Psicoactivo, Los Onabru, Caballos de Fuerza, Fámez  
**Martes 12 de enero – Franja X**  
Los Compadres Recerdos  
Crack Family  
**Miércoles 13 de enero – Día Pop**  
-Juan Pablo Vega  
-Lina Lab  
-Laura y la Máquina de Escribir  
-Ságan  
**Jueves 14 de enero – Día Rock**  
-Narcos  
-Dante  
-Whites  
-Los Punsetes  
-Triple X  
**Viernes 15 de enero – World Music**  
-Madosini  
-Kombilesa Mi  
-Georgina Hassan  
-Totolincho  
-Joe Bataan  
-Coreguaje  
**Sábado 16 de enero – Franja infantil**  
Coreguaje, Canto por la vida, Madosini  
**Domingo 17 de enero – Franja infantil**  
-Ángela Tapiero y el Taller de la música  
-Mayo la Orquídea  
-Un Bosque Encantado  
-Todos Podemos Cantar
