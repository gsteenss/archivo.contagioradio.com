Title: Atentan contra sedes del Partido Comunista y Partido FARC a pocas semanas de elecciones
Date: 2019-10-11 10:03
Author: CtgAdm
Category: DDHH, Política
Tags: elecciones regionales, Partido Comunista, Partido FARC, Violencia Política
Slug: atentan-contra-sedes-del-partido-comunista-y-partido-farc-a-pocas-semanas-de-elecciones
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/Partido-Comunista.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:] 

En horas de la madrugada de este viernes, la sede del Partido Fuerza Alternativa Revolucionaria del Común (FARC) y la sede del Partido Comunista que es a su vez la sede nacional de la Unión Patriótica fueron atacadas con disparos y bomba incendiaria. Este hecho sucede a pocos días de elecciones regionales, las que han estado marcadas por una serie de hechos violentos contra partidos y candidatos, sin que existan garantías para los sectores de oposición.

En las fotografías que se han conocido de los hechos, se evidencian los impactos de bala sobre las ventanas blindadas y los vidrios rotos en la puerta principal, de igual forma, en la sede del partido FARC fue hallado un cartel con la palabra 'Regresamos'. [Le puede interesar: JEP abre el caso 006 sobre el exterminio de la Unión Patriótica)](https://archivo.contagioradio.com/jep-abre-caso-006-exterminio-la-union-patriotica/)

> El atentado terrorista contra la sede de la [@UP\_Colombia](https://twitter.com/UP_Colombia?ref_src=twsrc%5Etfw) y [@notipaco](https://twitter.com/notipaco?ref_src=twsrc%5Etfw) justo en el aniversario del asesinato de Pardo Leal es un mensaje claro de la extrema derecha en venganza por la audiencia de la justicia a su patrón y los avances del proceso de paz. [\#LosBuenosSomosMas](https://twitter.com/hashtag/LosBuenosSomosMas?src=hash&ref_src=twsrc%5Etfw) [pic.twitter.com/cflHWwmIwo](https://t.co/cflHWwmIwo)
>
> — \#LideresSocialesEnLaCPI (@ColombiaMuisca) [October 11, 2019](https://twitter.com/ColombiaMuisca/status/1182673808584577030?ref_src=twsrc%5Etfw)

<p>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
</p>
Para Jaime Caycedo, secretario general de general del Partido Comunista Colombiano esto suceso está relacionado con las amenazas que han surgido en las últimas semanas "desde un sector de las Autodefensas Unidas de Colombia que se han reactivado con base en una política de rearme" y que hoy en día no solo se encuentran en las zonas rurales sino que también están actuando en las ciudades, **aunque los daños son menores, dejan una huella de temor y de terror"**, afirma.  [(Lea también: Violencia, un mecanismo de competencia entre candidatos)](https://archivo.contagioradio.com/violencia-un-mecanismo-de-competencia-entre-candidatos/)

Caycedo señala que estos sucesos se dan en medio de una campaña electoral "desdichadamente en medio de la peor violencia y un baño de sangre en todo el país" por lo que se cuestiona sobre las garantías que el gobierno del presidente Duque le está otorgando a las fuera políticas en particular  a los sectores de la oposición  **"que estamos defendiendo un proceso de paz y su implementación"**.

"Tenemos que actuar conjuntamente para detener la violencia que pretende retomar el camino del genocidio y la persecución política  contra la oposición democrática" afirma el integrante del Partido Comunista quien señala que estos hechos sumados al asesinato de candidatos como el de Karina García, Orley García o Bernardo Betancurt, son una "discriminación tolerada por el Estado que no otorga garantías". [(Lea también: Tres medidas para frenar la violencia política según la MOE)](https://archivo.contagioradio.com/tres-medidas-para-frenar-la-violencia-politica-segun-la-moe/)

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/video.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2Fvideos%2F994356820902297%2F&amp;width=800&amp;show_text=false&amp;appId=894195857389402&amp;height=413" width="800" height="413" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>  
<iframe id="audio_43148726" frameborder="0" allowfullscreen scrolling="no" height="200" style="border:1px solid #EEE; box-sizing:border-box; width:100%;" src="https://co.ivoox.com/es/player_ej_43148726_4_1.html?c1=ff6600"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
