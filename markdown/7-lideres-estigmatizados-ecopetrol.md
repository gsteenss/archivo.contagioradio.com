Title: 7 líderes son estigmatizados y encarcelados por protestar contra Ecopetrol
Date: 2018-09-18 15:58
Author: AdminContagio
Category: DDHH, Nacional
Tags: Castilla La Nueva, congreso de los pueblos, Ecopetrol, Fiscalía
Slug: 7-lideres-estigmatizados-ecopetrol
Status: published

###### [Foto: Manifestación Castilla la Nueva] 

###### [18 Sep 2018] 

Organizaciones comunales y sindicales denunciaron que **siete líderes sociales de Castilla la Nueva, Meta,** son víctimas de un montaje judicial por parte de la Fiscalía, y auspiciado por  **Ecopetrol**, mediante la **Estructura de apoyo de Hidrocarburos "EDA",** buscando judicializarlos por su postura crítica frente a la empresa colombiana de petróleos.

Los líderes **Doris Sanchez, Fernando Barbosa, Nidia Muñoz, Jessica Hernandez, Gustavo Carrión, Sandra Patricia Ruiz y Flaminio Mendoza** también se los acusa por los delitos de tráfico y porte de arma de fuego, concierto para delinquir, terrorismo, violencia contra servidor publico, constreñimiento ilegal, violación a la libertad del trabajo y obstaculizar vía pública. (Le puede interesar: ["Ecopetrol lleva 15 años incumpliendo promesas en Castilla la Nueva"](https://archivo.contagioradio.com/ecopetrol-lleva-15-anos-incumpliendo-promesas-en-castilla-la-nueva/))

**Para Nestor Padilla, líder social de Castilla la Nueva,** las capturas que se produjeron desde el pasado 10 de septiembre son retaliaciones contra estas personas por haber hecho parte de las mesas de negociación y concertación que se han realizado con Ecopetrol. En dichas discusiones, se ha pedido a la empresa **que se emplee mano de obra local, se mejoren las condiciones de contratación para los habitantes de la región y se subsane el daño ambiental que padece el Municipio. **

Sin embargo, Padilla denuncia que **de las 77 actas firmadas** en reuniones con Ecopetrol, (algunas de ellas con José Cotelo, vice presidente de la compañía en la región), **ninguna presenta avances,** haciendo caso omiso de los pedidos de las comunidades. (Le puede interesar: ["Habitantes de Castilla la Nueva dicen no ser terroristas y protestas contra Ecopetrol continúan"](https://archivo.contagioradio.com/habitantes-de-castilla-la-nueva-dicen-no-ser-terroristas-y-protestas-contra-ecopetrol-continuan/))

### **Ser líder social no es un delito** 

Padilla señala que la comunidad de Castilla se ha movilizado exigiendo la liberación de los 7 líderes y líderesas, reclamando que no se criminalice las luchas sociales en medio de un ambiente de peligro para sus vidas. Adicionalmente, el activista afirma que las movilizaciones se seguirán adelantando para que Ecopetrol cumpla los acuerdos y retribuya de alguna forma los recursos que obtiene del territorio.

Por su parte, Congreso de los Pueblos pide que no se estigmatice los liderazgos sociales, porque como lo ha denunciado en diferentes ocasiones, **la judicialización de diferentes líderes o integrantes de comunidades en los territorios es una práctica que atenta contra el derecho a la protesta**; denuncia que se suma, al [pedido de retractación](https://archivo.contagioradio.com/retracte-ministro-defensa/) que han hecho organizaciones al ministro de Defensa por las declaraciones en las que señala que la protesta social es financiada por grupos criminales. (Le puede interesar: ["Congreso de los Pueblos denunció intento de falso positivo judicial por parte del Ejército"](https://archivo.contagioradio.com/congreso-de-los-pueblos-falso-positivo-judicial-por-parte-del-ejercito/))

Entre tanto, la audiencia para resolver la situación jurídica de los 7 líderes y líderesas se realizará el próximo **martes 25 de septiembre a las 3 de la tarde**, mientras la juez que lleva el caso revisa las pruebas que se han presentado en el proceso.

<iframe id="audio_28683398" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_28683398_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en ][[su correo] [[Contagio Radio]
