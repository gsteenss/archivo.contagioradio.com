Title: Los retos para Colombia frente al nuevo Fiscal
Date: 2016-07-13 16:07
Category: Judicial, Nacional
Tags: Comisión Colombiana de Juristas, Fiscalía General de la Nación, jurisdicción especial para la paz, Nestor Humberto Martínez
Slug: los-retos-para-colombia-frente-al-nuevo-fiscal
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/05/movilizacio-paz-contagioradio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo ] 

###### [13 Julio 2016]

Tras su elección como Fiscal General de la Nación, Nestor Humberto Martínez expuso tres propuestas que pasarían por la aprobación del Congreso y permitirían que, de un lado, **en los casos en los que el funcionario se declare impedido pueda actuar un fiscal 'ad hoc' elegido por la Corte Suprema**; de otra parte, haya una** **auditoria interna que reporte sus hallazgos a la Corte y finalmente, que frente a las faltas que pueda cometer en el ejercicio de sus funciones, no asuma el cargo el Vicefiscal sino uno temporal elegido por el alto tribunal.

Es una propuesta razonable e inteligente; sin embargo, teniendo en cuenta que Nestor Humberto Martínez ha sido el representante jurídico de grupos económicos importantes y personajes poderosos de la vida nacional e internacional, **sus compromisos particulares pueden dificultar el ejercicio de su labor en la Fiscalía, **se estima que [[hay altas probabilidades de que continuamente se declare impedido](https://archivo.contagioradio.com/nuevo-fiscal-no-es-prenda-de-garantia-para-las-victimas-conpaz/)], lo que sigue siendo una preocupación para diversos sectores, como asegura el abogado Gustavo Gallón, director de la Comisión Colombiana de Juristas.

La ciudadanía deberá estar muy vigilante para controlar la actuación de esta dependencia, afirma Gallón, e insiste en que **dependerá en gran parte de la veeduría ciudadana que la Fiscalía cumpla con sus funciones** en asuntos relacionados con el traslado de procesos a la [[Jurisdicción Especial para la Paz](https://archivo.contagioradio.com/jurisdiccion-especial-para-la-paz/)] y la elección del Fiscal que estará a cargo de la [[unidad especial para el desmonte de los grupos sucesores del paramilitarismo](https://archivo.contagioradio.com/este-es-el-texto-del-acuerdo-sobre-cese-bilateral-firmado-entre-gobierno-y-farc/)], temas sobre los que aún falta precisar detalles pero que requerirán aportes del nuevo Fiscal General.

<iframe src="http://co.ivoox.com/es/player_ej_12213535_2_1.html?data=kpefk5iZd5ahhpywj5abaZS1k56ah5yncZOhhpywj5WRaZi3jpWah5yncajp1NnO2NSPi8LgzYqwlYqmd8-Zk6iYpaiucYarpJKw0dPYpcjd0JC_w8nNs4yhhpywj5k.&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en [[Otra Mirada](http://bit.ly/1ICYhVU)] por Contagio Radio] 
