Title: Procuraduría suspende a Fabio Otero, alcalde de Tierralta, Córdoba
Date: 2019-07-03 10:33
Author: CtgAdm
Category: Judicial, Política
Tags: Alcaldía, cordoba, procuraduria, Tierralta
Slug: procuraduria-suspende-a-fabio-otero-alcalde-de-tierralta-cordoba
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/Alcalde-Fabio-Otero-Tierralta.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:  PGN\_Col] 

Por orden de la **Procuraduría General** fue suspendido por tres meses el alcalde de Tierralta, Córdoba, **Fabio Leonardo Otero e**n el marco de la investigación que se adelanta contra el mandatario, por presuntas irregularidades en el control del orden público, vinculadas a una invasión de lotes y que habrían derivado en el asesinato de la líder social **María del Pilar Hurtado** el pasado 21 de junio.

La Procuraduría Provincial de Montería indicó que si Fabio Otero continuaba en su cargo podría existir "una interferencia al proceso disciplinario" ante la posibilidad de que el alcalde de Tierralta despliegue acciones que **"podrían estar  encaminadas a la recuperación de uno de los predios invadidos, propiedad de su padre".** [(Con asesinato de María del Pilar Hurtado, paramilitares siguen cumpliendo amenazas en Córdoba)](https://archivo.contagioradio.com/con-asesinato-de-maria-del-pilar-hurtado-paramilitares-siguen-cumpliendo-amenazas-en-cordoba/)

La Procuraduría evaluó algunas pruebas dentro de las que están incluidas las órdenes que habría impartido Otero en el Consejo de Seguridad del pasado 30 de mayo y donde habría dado instrucciones precisas a la Policía para la recuperación del predio de su familia. Por este caso también está bajo investigación el **secretario de Gobierno de este municipio, Willington Ortiz Naranjo**. [(Narcotráfico, política y paramilitares conviven en Tierralta: Comisión de Paz)](https://archivo.contagioradio.com/narcotrafico-politica-y-paramilitares-conviven-en-tierralta-comision-de-paz/)

### Cohabitan paramilitares y política en Tierralta

Esta fue una de las conclusiones a las que pudo llegar la Comisión de Paz del Senado, quienes visitaron la zona a finales de junio en una misión humanitaria en la que según su testimonio y al hablar con la comunidad y las autoridades locales que en el municipio se pudo evidenciar que el asesinato de María del Pilar no se trató de un caso aislado sino que sucede de manera sistemática y que se está dando un resurgimiento de la parapolítica al interior del municipio.

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo] 
