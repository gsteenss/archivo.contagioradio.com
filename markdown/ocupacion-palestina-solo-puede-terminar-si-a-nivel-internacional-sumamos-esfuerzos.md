Title: Mujeres Rumbo a Gaza se encamina para romper el cerco de Israel
Date: 2016-07-14 14:21
Category: El mundo, Entrevistas
Tags: BDS, Cirsjordanía, Gaza, Intifada Palestina, Mujeres Rumbo a Gaza, Ocupación israelí, Palestina
Slug: ocupacion-palestina-solo-puede-terminar-si-a-nivel-internacional-sumamos-esfuerzos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/07/Flotilla-Rumbo-a-Gaza.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Palestina Libre ] 

###### [14 Julio 2016] 

Actualmente las mujeres palestinas enfrentan tres tipos de opresión, la provocada por la ocupación israelí en Cisjordania y en Gaza, la del patriarcado y **la de la mirada occidental que las recluido en una casa y no las ha hecho partícipes de la lucha por la liberación de su pueblo**, asegura Laura Arau, coordinadora de la iniciativa Mujeres Rumbo a Gaza.

Arau afirma que las mujeres de Palestina han resistido de forma pacífica con movimientos sociales de base y actividades agrícolas, académicas y laborales, a lo que se ha convertido vivir en Gaza, "la prisión al aire libre más grande del mundo", y de la que incluso **informes de la ONU han llegado a asegurar que para 2020 será inhabitable**, razón por la que llama a la solidaridad internacional.

Frente a la continúa violación a los derechos humanos por parte del gobierno de Israel, se han adelantando diversas iniciativas de carácter internacional lideradas por la coalición Flotilla de la Libertad, que en mayo de 2010 partió con **ocho embarcaciones que fueron asaltadas por el ejército israelí**, en seis de ellas habían 750 activistas y en las dos restantes, ayuda humanitaria. Durante el ataque fueron asesinadas nueve personas.

Las iniciativas han buscado romper con el bloqueo israelí a la Franja de Gaza, con el bloqueo físico pero también mental de algunos ciudadanos y **gobiernos occidentales que deberían hacer que se cumplan las resoluciones de la ONU** en relación con la ocupación de Palestina y lo que ha venido sucediendo allí hace 70 años, asevera Arau.

En esta ocasión serán mujeres de Turquía, Estados Unidos, Canadá, Italia, Nueva Zelanda, Suecia, Sudáfrica y España, principalmente, quienes partirán en septiembre de este año en embarcaciones con ayuda humanitaria para el pueblo palestino. Entre las invitadas se destaca Mairead Maguire, premio nobel de paz de 1976, quien junto a cientos de mujeres se movilizará para hacer frente al **silencio de la comunidad internacional que la hace cómplice de los vejámenes que ocurren en Palestina**.

"Palestina, [[sobretodo Gaza](https://archivo.contagioradio.com/gaza-se-ha-convertido-en-una-carcel-a-orillas-del-mar-mediterraneo/)], es un laboratorio de todo tipo de armamento militar, en varias ocasiones se ha evidenciado que luego de los ataques a la población, Israel incrementa la venta de armas" afirma Arau quien insiste en que allí también se ponen a prueba técnicas de represión ciudadana que se exportan a otras naciones. "La ocupación de Palestina sólo se puede terminar [[sí a nivel internacional sumamos esfuerzos](https://archivo.contagioradio.com/palestina-su-lucha-es-tambien-la-nuestra-kwara-kekana-lider-sudafricana/)] para evitar que Israel viole a diario no sólo los derechos humanos de la población, palestina sino también las leyes internacionales (...) basta ya de complicidades que matan".

<iframe src="http://co.ivoox.com/es/player_ej_12225234_2_1.html?data=kpeflJqWd5Whhpywj5aaaZS1lJyah5yncZOhhpywj5WRaZi3jpWah5ynca3V1tfOjabWpdaZk6iYr9rOqdPZ1JC_19LGs4zVjKzO3MaRaZi3jqjc0NnFq8rjjLfOxs7Tb46ZmKialg..&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)]]
