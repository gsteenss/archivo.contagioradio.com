Title: Esta es la agenda de movilización de la Cumbre Agraria para la próxima semana
Date: 2015-08-28 15:40
Category: Movilización, Nacional
Tags: agro colombiano, ANZORC, Cumbre Agraria, Movilización social, Paro agrario 2013
Slug: esta-es-la-agenda-de-movilizacion-de-la-cumbre-agraria-para-la-proxima-semana
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/08/DSC00835-1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Contagio Radio 

Del 30 de agosto al 5 de septiembre la Cumbre Agraria se movilizará en todo el país, como símbolo de indignación, campesina, étnica y popular expresando su rechazo frente a los incumplimientos del gobierno y sus políticas estatales que atentan contra el campo colombiano, como lo han afirmado.

Organizaciones sociales y campesinas, aseguran estar indignados por "los acuerdos incumplidos, las políticas regresivas, la democracia restringida, la estigmatización y discriminación de nuestras luchas y conquistas, la persecución política por parte del Estado son actos injustos y ofensivos contra la esperanza de paz con justicia social que anhela el pueblo colombiano".

Es por eso, que a partir del 30 de agosto, en diferentes lugares del país, específicamente en la capital, se desarrollarán jornadas de movilización con el objetivo de realizar una evaluación pública del proceso de negociación con el gobierno Santos.

Aquí la agenda de movilización.

[Agenda Jornadas de Indignación Campesina, Étnica y Popular](https://www.scribd.com/doc/276697355/Agenda-Jornadas-de-Indignacion-Campesina-Etnica-y-Popular "View Agenda Jornadas de Indignación Campesina, Étnica y Popular on Scribd") by [Contagioradio](https://www.scribd.com/contagioradio "View Contagioradio's profile on Scribd")  
<iframe id="doc_52792" class="scribd_iframe_embed" src="https://www.scribd.com/embeds/276697355/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-S8PZefkoXg0X4SfqN50n&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="0.7080062794348508"></iframe>
