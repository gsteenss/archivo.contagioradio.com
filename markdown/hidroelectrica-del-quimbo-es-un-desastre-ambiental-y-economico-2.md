Title: "Hidroeléctrica del Quimbo es un desastre ambiental y económico"
Date: 2015-02-10 19:21
Author: AdminContagio
Category: Ambiente, Economía, Movilización, Resistencias
Tags: Ambiente, Huila, Quimbo
Slug: hidroelectrica-del-quimbo-es-un-desastre-ambiental-y-economico-2
Status: published

##### Foto: [promotoresambientalesut.wordpress.com]

<iframe src="http://www.ivoox.com/player_ek_4064546_2_1.html?data=lZWjlpqYeo6ZmKiak5WJd6Knl5KSmaiRdo6ZmKiakpKJe6ShkZKSmaiRaZOmptLPw9HXqYzZzZC%2B187RptCfxtjhh6iXaaKljMrbjcjZrcXVxdTgjc7SuMbi1M7j0diJdpOZk6iYr87QsMahhpywj6jTstXVyM7cjbfFqMrjjJKSmaiReA%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Miller Dussán, Asoquimbo] 

**El Tribunal Administrativo del Huila decretó este 6 de febrero, medida cautelar de urgencia para congelar el llenado del embalse de El Quimbo**, de la empresa Emgesa, con el objetivo de prevenir un daño inminente sobre los rendimientos anuales de producción del embalse de Betania.

Para Miller Dussán, investigador de Asoquimbo, “es importante la decisión porque reconoce las afectaciones en la economía del Huila pero **no resuelve el problema de fondo”.**

**“El Quimbo está en cuidados intensivos, la única solución es suspender definitivamente las obras”,** asegura el investigador, que hace referencia a las daños ambientales, sociales y económicos que se han generado debido a la construcción del embalse, entre ellas, se resalta que va haber falta de oxigeno, por lo que desaparecerá la pesca.

Además, se han **destruido cadenas productivas, hallazgos arqueológicos, se ha producido desplazamientos forzados y sobre todo se está generando una deforestación irremediable,** lo que está acabado con la vida del ecosistema.

Son más de **33.000 personas** las que solicitan ser compensadas por los daños causados por la construcción de la represa, a eso se le suma que en la licencia ambiental, había quedado estipulado que **se debían restituir 5400 hectáreas** para los habitantes de las comunidades, sin embargo, “no se ha restituido ni un milímetro”, señala Dussan.

**El 16 de marzo se iniciará una movilización** desde el macizo colombiano hasta Bocas de Ceniza, en Magdalena, para que por medio de foros y debates sobre la defensa de los territorios y la vida, se le  exija a la Corte Constitucional y al Consejo de Estado, suspender estas obras del Huila que vulneran los derechos de las comunidades.
