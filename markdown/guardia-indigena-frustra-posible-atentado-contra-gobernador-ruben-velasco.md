Title: Guardia indígena frustra posible atentado contra gobernador Rubén Velasco
Date: 2019-01-14 16:50
Author: AdminContagio
Category: Nacional
Tags: Amenazas a indígenas, Norte del Cauca
Slug: guardia-indigena-frustra-posible-atentado-contra-gobernador-ruben-velasco
Status: published

###### Foto: Pulzo 

###### 14 Ene 2019 

[El Tejido de Defensa de la Vida y los Derechos Humanos de la Asociación de Cabildos Indígenas del Norte del Cauca (ACIN) informó que se previno un posible atentado el pasado 13 de enero contra  el gobernador **Ruben Orley Velasco,** autoridad ancestral del cabildo indígena de **Tacueyó, Toribío. **]

[Según información de la ONIC, entre las 7:00 y 7:30 pm de ese domingo, mientras el gobernador **Rubén Orley Velasco** se encontraba cerca a su residencia, personas armadas y vestidas de negro fueron vistas en los alrededores, lo cual activó las correspondientes alarmas comunitarias y procedimientos de control territorial de la Guardia Indígena Kiwe Thegnas, previniendo que se produjera un atentado.]

A través de un mensaje, **el gobernador Rubén Velasco indicó que los seguimientos han sido repetitivos pero que la reacción de la comunidad y la guardia indígena ha sido oportuna**, razón por la cual se debe continuar una constante vigilancia de las situaciones que podrían acontecer en el territorio. [(Le puede interesar: Recuperar su territorio le está costando la vida a indígenas en Cauca)](https://archivo.contagioradio.com/nos-estan-asesinando-llamado-de-comunidades-indigenas-del-cauca/)

La comunidad y las autoridades indígenas del plan de vida Proyecto Nasa se encuentran tras la pista de los desconocidos los cuales huyeron al activarse el control territorial, según Jaime Díaz, integrante de la comunidad, “el gobernador Velasco ha asumido una posición firme frente a la  intervención de grupos emergentes al margen de la ley en el territorio”, la cual ha sido la causante de estos seguimientos.   [ (Lea también: No hay tregua para indígenas y líderes sociales en Cauca)](https://archivo.contagioradio.com/no-tregua-indigenas-lideres-sociales-cauca/)[  
  
Dichos seguimientos se suman a los atentados presentados en Corinto, Caloto, Santander de Quilichao, Buenos Aires y Suárez, **el ataque contra el defensor de derechos humanos, Germán Valencia Medina y el** asesinato del gobernador de Huellas, Edwin Dagua. **** La comunidad reitera su rechazo a estos actos y hace un llamado a todas las comunidades de la región norte para permanecer unidos y actuar con “mayor contundencia” ante estas amenazas.]

###### Reciba toda la información de Contagio Radio en [[su correo]
