Title: «Cifras de asesinatos a líderes sociales son manipuladas por el Gobierno»
Date: 2020-08-03 22:58
Author: PracticasCR
Category: Actualidad, DDHH
Tags: asesinato de líderes sociales, defensores de derechos humanos, INDEPAZ, Líderes y lideresas sociales
Slug: cifras-de-asesinatos-a-lideres-sociales-son-manipuladas-por-el-gobierno
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/08/Asesinato-de-líderes-sociales.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/08/Consolidado-Indepaz-líderes-sociales-asesinados.png" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

El día de hoy se dio a conocer por parte de la Fiscalía General de la Nación la cifra de líderes sociales asesinados(as) después de la firma del Acuerdo de Paz en noviembre de 2016. Según el ente investigador la cifra ascendería a las 349 personas víctimas de homicidio.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Pese a que la cifra es bastante alta, es muy inferior a la que manejan organizaciones que hacen seguimiento y monitoreo constante a las agresiones de las que son víctimas los(as) líderes sociales y personas defensoras de derechos humanos.  El Instituto de Estudios para el Desarrollo y la Paz -Indepaz-, por ejemplo, según su más reciente [informe](http://www.indepaz.org.co/wp-content/uploads/2020/07/Informe-Especial-Asesinato-lideres-sociales-Nov2016-Jul2020-Indepaz.pdf) registró al menos **971 asesinatos luego de la firma del Acuerdo.** (Lea también:[971 defensores de DD.HH. y líderes han sido asesinados desde la firma del Acuerdo de paz](https://archivo.contagioradio.com/971-defensores-de-dd-hh-y-lideres-han-sido-asesinados-desde-la-firma-del-acuerdo-de-paz/))

<!-- /wp:paragraph -->

<!-- wp:image {"id":87818,"sizeSlug":"large"} -->

<figure class="wp-block-image size-large">
![Consolidado asesinato de líderes sociales desde la firma del Acuerdo - Indepaz](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/08/Consolidado-Indepaz-líderes-sociales-asesinados.png){.wp-image-87818}  

<figcaption>
Consolidado asesinato de líderes sociales desde la firma del Acuerdo - Indepaz

</figcaption>
</figure>
<!-- /wp:image -->

<!-- wp:paragraph -->

En el mismo sentido, la Consejería Presidencial para los Derechos Humanos, unidad adscrita al gobierno de Iván Duque, registró en su «boletín de homicidios contra los líderes sociales y defensores(as) de derechos humanos» un total de 45 asesinados en lo que va corrido de este año con corte al 25 de julio. También, **una cifra significativamente inferior si se compara con el estimado de Indepaz para el año 2020 que a la fecha registra un total de [173](http://www.indepaz.org.co/paz-al-liderazgo-social/) líderes sociales y personas defensoras de derechos humanos asesinados.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

La comparación de los reportes, daría cuenta de que las cifras institucionales estarían expresando apenas una tercera parte de los asesinados que documenta Indepaz en sus registros; tanto en el caso de la Fiscalía (perteneciente al poder judicial) como en el de la Consejería para los Derechos Humanos (perteneciente a la rama ejecutiva).

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Frente a esta notable discrepancia en las cifras; Camilo González Posso, director de Indepaz señaló que una de las principales razones que explican la diferencia es que **el Gobierno toma como base cifras parciales de la oficina de la Alta Comisionada de Naciones Unidas para los Derechos Humanos en Colombia -ACNUDH-.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El analista explica que Naciones Unidas emite reportes parciales en los que documenta los asesinatos pero aclara, primero que existen más bajo su observación pendientes por confirmar y segundo que su cobertura en el registro no es total, ya que, solo da cuenta de aquellos que le son notificados o han recibido denuncia.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

González Posso, afirmó que el Gobierno trabaja con un rezago en las cifras, pues el reporte publicado para el mes de julio da cuenta de las cifras de ACNUDH con corte a mayo. Esto lleva a que los datos, que publica la Consejería Presidencial, omitan los casos de asesinato que aún están bajo observación por Naciones Unidas y aquellos que se produjeron en junio y julio. Además, señala que una vez sean consolidados los registros **«*la cifra de Naciones Unidas a julio va a superar los 100 líderes asesinados*»**.

<!-- /wp:paragraph -->

<!-- wp:quote -->

> **El Gobierno lo sabe pero le conviene anunciar una cifra parcial. Hay una intención por parte del Gobierno de minimizar el problema manejando cifras parciales»**
>
> <cite>Camilo Gonzalez Posso, director de Indepaz.</cite>

<!-- /wp:quote -->

<!-- wp:heading {"level":3} -->

### Los asesinatos contra líderes, lideresas sociales y personas defensoras no es solo un tema de cifras

<!-- /wp:heading -->

<!-- wp:paragraph -->

Los registros de personas asesinadas no se pueden quedar en el mero dato estadístico,  no solo porque de lo que se habla es de vidas humanas, sino porque estas cifras son las que orientan la adopción de políticas y medidas por parte del Gobierno.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Camilo González Posso, señaló que la  subestimación de las cifras desorienta la acción del Estado porque minimiza el problema y da la idea de que el problema no está pasando o es asunto del pasado. Eso según el analista, tiene un efecto de «*bajar la guardia*» en las instituciones, generando la omisión de los agentes del Estado y una tergiversación en el diagnóstico que sirve como base para la estructuración de la política pública y de las estrategias del Gobierno y la Fuerza Pública para contener la problemática.

<!-- /wp:paragraph -->

<!-- wp:quote -->

> **Esto no es solo un problema de números sino que es un problema de debilitamiento de la acción del Estado para desvertebrar las organizaciones que se dirigen en contra de los líderes sociales, como consecuencia de un mal manejo de las cifras»**
>
> <cite>Camilo Gonzalez Posso, director de Indepaz.</cite>

<!-- /wp:quote -->

<!-- wp:heading {"level":3} -->

### «Manipular las cifras un descrédito para el Gobierno»

<!-- /wp:heading -->

<!-- wp:paragraph -->

Según González Posso la tergiversación de las cifras y el consecuente negacionismo de la problemática por parte del Gobierno conllevan a su propio descrédito pues asegura que más adelante el propio Gobierno va a ser quien se vea confrontado a las mismas cifras que cita como fuente que son las de ACNUDH.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El director de Indepaz, afirmó que hace un año **Naciones Unidas tuvo que salir a hacer una rectificación sobre el uso que el Gobierno estaba dando de sus cifras y que ello se reflejaba en los informes del Consejo de Seguridad de dicho organismo internacional.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Además,  manifestó que a la fecha no existe ningún consolidado de Naciones Unidas que registre los asesinatos contra líderes sociales en los dos años que lleva el gobierno de Duque, sin embargo este está diciendo que hay una disminución en los dos primeros años de su Gobierno; **esto es «*una manipulación de las cifras*»**, añadió González Posso.

<!-- /wp:paragraph -->

<!-- wp:quote -->

> **Es un descrédito para el Gobierno que por lograr un impacto mediático coyuntural quede reflejada la manipulación y malinterpretación  que hace de las cifras»**
>
> <cite>Camilo Gonzalez Posso, director de Indepaz.</cite>

<!-- /wp:quote -->

<!-- wp:block {"ref":78955} /-->
