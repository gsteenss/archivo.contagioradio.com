Title: Ya son 9 los municipios del Huila que se niegan a los proyectos extractivistas
Date: 2017-06-13 15:28
Category: Ambiente, Nacional
Slug: huila_municipios_acuerdo_no_mineria_proyectos_extractivistas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/06/1404520855-e1497385418448.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto:  turismo San Agustín 

###### [13 Jun 2017] 

El Concejo municipal de San Agustín, en el departamento del Huila, aprobó el acuerdo, por medio del cual, prohíbe la construcción **de proyectos hidroeléctricos, minería e hidrocarburos** en su territorio.

Se trata de una decisión basada en la voluntad popular, que gestionó la ciudadanía con el objetivo de defender su territorio de los daños que ya se ha visto que ocasionan los diversos tipos de proyectos extractivistas en otros lugares de Colombia.

Con esta medida, el departamento del Huila continúa consolidándose como una población que le dice NO a este tipo de actividades que irían en contra de la vocación agrícola de dichos territorios. **Tarqui, Pitalito, Oporapa, Acevedo, Altamira, Elias, El Agrado, y Timaná,** de esa misma región, se han negado a la realización de actividades de este tipo, mediante acuerdos autónomos.

**Cabe recodar que la Contraloría ha señalado por medio de informes que no existe la cantidad de regalías que asegura el gobierno por parte de la minería**, y en cambio muchas empresas mineras declaran renta en 0 pesos. A su vez, en aras de reparar los graves impactos ambientales, están haciendo obras públicas que realmente debe proveer el Estado.

Además, el abogado Rodrigo Negrete, señala que este panorama, debe hacer pensar al gobierno en consensos, ya que son varias las comunidades que mediante la re**visión de planes de ordenamiento territorial, la realización de consultas populares y los acuerdos autónomos**, han encontrado mecanismos constitucionales para defender sus territorios, ante las negativas del gobierno para debatir sobre el futuro y vocación de diversos lugares del país. [(Le puede interesar: Gobierno debe encontrar consensos frente a la minería)](https://archivo.contagioradio.com/gobierno_mineria_rodrigo_negrete_consultasoulares_tamesis/)

El anuncio del municipio de San Agustín de parte del Concejo se llega justo cuando el viceministro de Minas, Carlos Cante, defendiera la realización de proyectos mineros en los municipios del Huila, argumentando su papel en el posconflicto.

![Huila](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/06/huila_mineria.png){.alignnone .size-full .wp-image-42183 .aligncenter width="480" height="975"}

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU).
