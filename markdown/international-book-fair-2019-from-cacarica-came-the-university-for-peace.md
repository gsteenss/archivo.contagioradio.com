Title: International Book Fair 2019: From Cacarica came the University for Peace
Date: 2019-04-27 22:55
Author: CtgAdm
Category: English
Tags: Chocó, University for Peace
Slug: international-book-fair-2019-from-cacarica-came-the-university-for-peace
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/Cacarica-e1556406632890.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

Ledys Orejuela and Henry Angulo social leaders from Cacarica in the Chocó department, together with the director of “Comisión de Justicia y Paz” Danilo Rueda, shared with the attendees of the Bogota International Book Fair how their Univeristy for peace is moving forward.This University is an initiative opened in this region trying to choose forgiveness and education as a redemption method from the violent Genesis operation in February 1997 during the armed conflict. "We wish this University could be a space of acceptance and culture, a space for memory where we can all be included," says Ledys Orejuela who emphasizes the importance of forgiveness during this process.

**Education, essential for the human being**

Henry, a member of “Comunidades de Autodeterminación, Vida y Dignidad del Cacarica (CAVIDA a Self-Determination organization)adds that the proposal of the University for Peace is allowing the 25 communities of Cacarica - including the humanitarian zones of Nueva Esperanza and Nueva Vida - to promote education. They consider education’s right as fundamental to develop the human being".

He affirms: «We can achieve the dream of a new country, of being new woman and men building education and culture on truth and restorative justice. We can build it leaving that hatred, leaving that anger towards the other that yesterday hurt us and today we forgave. We dream a country where all of us can support and hug each other a place where solidarity flows, ".
