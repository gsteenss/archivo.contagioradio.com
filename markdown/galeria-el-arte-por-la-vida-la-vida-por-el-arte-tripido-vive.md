Title: Galería - El arte por la vida, la vida por el arte "Tripido" vive
Date: 2015-08-25 15:17
Category: Fotografia, Nacional, Sin Olvido
Tags: “Trípido”, Diego Felipe Becerra, Policía Nacional
Slug: galeria-el-arte-por-la-vida-la-vida-por-el-arte-tripido-vive
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/08/tripido-1.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/08/tripido1-1.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/08/tripido2-1.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/08/tripido3-1.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

"Tripido, 4 años de ausencia" fue uno de los mensajes que quedo grabado en el puente de la calle 116 con Av Boyacá,  donde jóvenes y familiares conmemoraron el asesinato de  Diego Felipe Becerra, grafitero [asesinado el 22 de agosto de 2011 por integrantes de la Policia Nacional. ](https://archivo.contagioradio.com/se-preparan-8-capturas-mas-por-el-crimen-de-diego-felipe-becerra/)

\

Leer [Se preparan 8 capturas más por el crimen de Diego Felipe Becerra](https://archivo.contagioradio.com/se-preparan-8-capturas-mas-por-el-crimen-de-diego-felipe-becerra/)

<figure>
[![tripido](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/08/tripido-1.jpg)](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/08/tripido-1.jpg)

</figure>
<figure>
[![tripido1](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/08/tripido1-1.jpg)](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/08/tripido1-1.jpg)

</figure>
<figure>
[![tripido2](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/08/tripido2-1.jpg)](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/08/tripido2-1.jpg)

</figure>
<figure>
[![tripido3](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/08/tripido3-1.jpg)](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/08/tripido3-1.jpg)

</figure>

