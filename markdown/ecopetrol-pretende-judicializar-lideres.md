Title: Líderes sociales denuncian estrategia de Ecopetrol para criminalizarlos
Date: 2018-09-07 15:01
Author: AdminContagio
Category: Ambiente, Nacional
Tags: Ecopetrol, lideres sociales, Magdalena Medio
Slug: ecopetrol-pretende-judicializar-lideres
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/09/Derrame-petróleo-Barranca.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo particular] 

###### [07 Sept 2018] 

La comunidad del Magdalena medio denunció **abuso de poder por parte de Ecopetrol** y los contratistas petroleros en los municipios de Barrancabermeja y San Vicente de Chucurí. Oscar Sampayo, integrante del Grupo de Estudios del Magdalena Medios, afirma que existe una alianza entre la Fiscalía, Min. Trabajo, Ecopetrol y una empresa de vigilancia local, que estarían elaborando **informes para crear procesos jurídicos en contra de líderes sociales** de la zona del Bajo San Vicente, mediante falsos testimonios.

Sampayo asegura que los defensores de la comunidad están siendo falsamente acusados de producir bloqueos y paros en contra del trabajo petrolero en el Magdalena Medio, y se están generando **documentos acusatorios provenientes de Ecopetrol contra líderes de la zona del Bajo San Vicente** y miembros del Comité de Concertación de la Vizcaina.

Cabe destacar, que las acusaciones sobe manifestaciones se realizaron en fechas en las que los lideres implicados **no se encontraban en el municipio.** (Le puede interesar: [Amenazan a ambientalista que exige medidas sobre desastre de petróleo en Lizama](https://archivo.contagioradio.com/amenazan-a-ambientalista-que-exige-medidas-sobre-desastre-de-petroleo/))

**Los documentos ya se encuentran en manos de las autoridades civiles de San Vicente de Chucurì y Barrancabermeja**, y según Sampayo, estas acusaciones se están realizando debido a las peticiones de los líderes a las empresas contratistas de invertir un mínimo de sus millonarias ganancias en el desarrollo de los territorios, realizar la contratación de personal con habitantes de la zona y brindarles las prestaciones laborales requeridas por la ley.

Para la comunidad, acusar y judicializar a los líderes sociales es la forma de e**vadir la normatividad impuesta por la Autoridad Nacional de Licencias Ambientales**, e ignorar las acciones criminales reales que azotan el Magdalena Medio.

### **¿De dónde viene el conflicto de la comunidad con Ecopetrol?** 

Luego del desastre ambiental producido en el pozo 158 La Lizama, se suspendieron los trámites y actividades petroleras en la zona por petición de la Autoridad Nacional de Licencias Ambientales,y de acuerdo con Oscar Sampayo, tras un periodo de receso **se han intensificado las actividades de wokover y el mantenimiento de pozos,** entre otros trabajos de infraestructura petrolera en la zona del Magdalena medio.

La amplificación de las zonas de explotación de combustibles fósiles estaría invadiendo predios campesinos sin ningún tipo de retribución a la comunidad ni pagos por servidumbre, **“quieren entrar porque son Ecopetrol, porque son el Estado”** afirmó Sampayo. Adicional a esto, manifiesta que se están llevando a cabo construcciones de estructuras de líneas de Fracking sin consentimiento de los habitantes. (Le puede interesar: [En menos de 6 meses se han presentado dos derrames de petroleo en el río Magdalena](https://archivo.contagioradio.com/en-menos-de-6-meses-se-han-presentado-dos-derrames-de-petroleo-en-el-rio-magdalena/))

Finalmente Sampayo le hace un llamado a la Fiscalía  y al Ministerio del Interior **“Necesitamos una institución sólida que evidencie lo que está pasando allí, y apoyo del Estado que vaya más allá de la presencia militar”** y se refirió al abuso de poder de Ecopetrol “Por muy poderosa que sea la compañía, tiene que haber un respeto a la norma”

<iframe id="audio_28416719" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_28416719_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU).] 
