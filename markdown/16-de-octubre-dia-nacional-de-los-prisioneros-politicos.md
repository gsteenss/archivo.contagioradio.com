Title: 16 de octubre día nacional de los prisioneros políticos
Date: 2016-10-15 10:29
Category: Judicial, Nacional
Tags: Comité de Solidaridad con los Presos Políticos
Slug: 16-de-octubre-dia-nacional-de-los-prisioneros-politicos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/10/presos-politicos-contagio-radio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: ContagioRadio] 

###### [15 de Oct 2016] 

En memoria del dirigente social Luis Carlos Cárdenas quien fue detenido y ejecutado de manera extrajudicial en octubre de 1973 y también por los **miles de presos y presas políticos que se encuentran recluidos en las cárceles colombianas** en condiciones que atentan contra los derechos fundamentales del ser humano, el 16 de octubre se conmemora el día nacional de las prisioneras y prisioneros políticos.

De acuerdo con la Fundación Comité de Solidaridad con los Presos políticos en Colombia la mayoría de **las personas privadas de la libertad por motivos políticos corresponden a profesores, estudiantes, líderes campesinos y comunales, obreros y trabajadores defensores de los Derechos Humanos** no alzados en armas. Esto, con relación a lo que la Red Internacional de Solidaridad con las Prisioneras y Prisioneros Políticos Colombianos señalan como la criminalización y represión por parte del Estado de las diferentes expresiones populares de resistencia, que se dan bajo la lógica del “*enemigo interno*”. Lea también: ["Más de 700 personas se reunirán por la libertad de las y los prisioneros políticos en Colombia"](https://archivo.contagioradio.com/mas-de-700-personas-se-reuniran-por-la-libertad-de-las-y-los-prisioneros-politicos-en-colombia/)

En Colombia en 2014 las organizaciones defensoras de derechos humanos establecían una cifra de **más de 7.000 presos políticos para la fecha**. En el marco del *II Encuentro Nacional e Internacional Larga Vida a Mariposas* llevado a cabo en marzo del 2015 se evidenciaron las constantes fallas por parte del Estado colombiano en la violación al Pacto Internacional de los Derechos Civiles y Políticos, la convención contra la tortura y/o tratos o penas crueles, inhumanas y degradantes.

Según el informe presentado al Comité de Naciones Unidas Contra la Tortura, y de acuerdo a fuentes Estatales y ONG de Derechos Humanos, en Colombia ha habido alrededor de **380 casos de torturas y tratos crueles e inhumanos desde 1998 hasta el 2015**.

Las 60 organizaciones nacionales e internacionales reunidas señalaron también **un aumento en las cárceles de las personas presas políticas**, durante el periodo de gobierno del expresidente Álvaro Uribe Vélez bajo su llamada Política de Seguridad Democrática.

A ello se sumaban los análisis llevados durante el Segundo Encuentro Nacional e Internacional, los cuales evidenciaron que conforme aumentaba la protesta social en Colombia durante el 2015, se **incrementaron de forma paralela el número de detenciones masivas y arbitrarias contra el sector popular** bajo falsas acusaciones de colaborar con la insurgencia.

**Para 2015 eran más de 9.000 presos y presas; el 90% correspondían a prisioneros políticos** o de consciencia, víctimas de los montajes judiciales y un 10% restante prisioneros políticos de guerra como es el caso de integrantes de las FARC-EP y el ELN los cuales se encuentran en 45 de las 139 cárceles del país.

Ante la profunda crisis del sistema carcelario y penitenciario en Colombia referente a la seguridad de los presos al interior de las cárceles, infraestructura, alimentación y salud, en lo que ellos denominan como un servicio que somete a tortura y pena de muerte, por cierto, abolida en Colombia desde 1910, pero aplicada a las personas privadas de la libertad. Los altos índices de hacinamiento son también dramáticos. **Para agosto de 2016 estos oscilaban entre el 32,2% y el 84,5%, según el Instituto Nacional Carcelario y Penitenciario INPEC.**

Las prisioneras y prisioneros políticos han decidido visibilizar la cruda realidad que se vive al interior a partir de jornadas de desobediencia civil, huelgas de hambre, jornadas culturales, entre otros. Un ejemplo emblemático fue el ocurrido este año en el mes de julio en el que las presas y presos políticos se cocieron la boca como manifestación de las constantes violaciones a sus derechos a los que son sometidos (as) y ante la mirada negligente del Estado.

###### Reciba toda la información de Contagio Radio en su correo [[bit.ly/1nvAO4u]
