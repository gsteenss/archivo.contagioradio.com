Title: "ELN debe generar credibilidad y reforzar su unidad interna" Carlos Velandia
Date: 2017-10-30 12:39
Category: Nacional, Paz
Tags: ELN, paz, Proceso de paz en Quito
Slug: eln-asesinato-indigena
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/12/ELN.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: ELN] 

###### [30 Oct 2017] 

A través de un comunicado de prensa el Frente de Guerra Omar Gómez, del ELN, reconoció el asesinato del líder indígena Aulio Lisarama, y explican que lo retuvieron por sospechar de sus vínculos con el Ejercito, en el comunicado señalan que el comunero "se niega a caminar y se abalanza sobre uno de nuestros guerrilleros, con el trágico desenlace conocido"

Para el gestor de paz Carlos Velandia, es un paso importante el reconocimiento por parte de las filas del ELN del asesinado de Lisarama, sin embargo, señaló que debe rechazarse el **crimen que se cometió contra una persona que estaba en completo estado de indefensión en medio de un cese bilateral**. (Le puede interesar: ["Asesinado gobernador indígena de resguardo Catrú, Dubasa y Ancoso, en el Alto Baudó Chocó"](https://archivo.contagioradio.com/asesinado-gobernador-indigena-del-resguardo/))

### **No procedía ni la retención ni el asesinato del líder indígena por parte del ELN** 

“Se está en un cese bilateral del fuego y de las hostilidades, hay un esfuerzo colosal de las partes que han estado 53 años enfrascadas en un conflicto armado que llegaron a un acuerdo, y ese acuerdo había que cuidarlo, **con mucho sentido de responsabilidad, por lo cual no procedía ni la retención ni el asesinato de absolutamente nadie**” afirmó el gestor de paz.

Frente al paso siguiente en la revisión de este acto, Velandia expresó que hay que esperar que el mecanismo de monitoreo y verificación del cese bilateral haga la correspondiente investigación, **se pronuncie, le cuente al país lo que ha sucedido y genere recomendaciones**.

Frente al ELN, Velandia manifestó que ahora esta guerrilla deberá hacer un doble esfuerzo para, de un lado, recuperar la confianza por parte de la sociedad, con acciones como tomar correctivos para evitar que algo así vuelva a suceder retirando a esta tropa del territorio y confinándolos para proteger a las comunidades, y del otro asegurar su unidad interna alrededor del proceso de paz. (Le puede interesar: ["Así serán las audiencias preparatoriade participación del ELN-Gobierno"](https://archivo.contagioradio.com/asi-seran-las-audiencias-preparatorias-de-participacion-en-dialogos-eln-y-gobierno/))

<iframe id="audio_21773924" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_21773924_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
