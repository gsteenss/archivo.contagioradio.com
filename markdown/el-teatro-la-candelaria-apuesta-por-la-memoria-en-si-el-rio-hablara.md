Title: El teatro La candelaria apuesta por la memoria en "Si el río hablara"
Date: 2015-08-27 17:14
Category: Cultura, eventos
Tags: Cesar Badillo, Festival de Teatro de Manizales, teatro la candelaria, Temporada teatro Bogotá
Slug: el-teatro-la-candelaria-apuesta-por-la-memoria-en-si-el-rio-hablara
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/08/unnamed-2.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [27 Ago 2015] 

Luego de su estreno en 2013 "Si el río hablara" obra dirigida por Cesar "Coco" Badillo regresa al Teatro la Candelaria en una nueva temporada; poniendo sobre la mesa el papel de la memoria de las víctimas, en tiempos en que la búsqueda de una paz duradera, y al derecho a la verdad con justicia social hace parte del diálogo nacional.

"Tratar de sanar, esa es la historia que se teje en 'Si el río hablara', y eso es lo que necesita el país. En la medida en que reconozcamos los dolores, se puede avanzar. Si como sociedad no nos reconocemos en el problema, vamos a seguir aplazando las soluciones, y va a estallar más adelante, que es lo que nos ha pasado con la guerra" expresa Badillo, agregando que "No se trata de hablar de 'postconflicto' como una moda, como un tema superficial, sino de reflexionar. Esa es la tarea de la sociedad, y en ese camino, del Teatro".

La producción teatral,  primera en ser estrenada por La Candelaria tras la salida por cuestiones de salud del maestro Santiago García, se presenta desde el 19 de agosto y tendrá sus últimas presentaciones los días jueves 27, viernes 28 y sábado 29 a las 7:30 p.m, para luego desplazarse a Manizales, donde hará parte del Festival de teatro de esa ciudad.

"Si el río hablara", es una de las producciones que hacen parte de la nueva etapa del tradicional teatro capitalino, temporada en la que se presentarán las obras "Cuerpos gloriosos", "Soma Mnemosine", y "Camilo" obra que desde su estreno ha tenido gran acogida por parte del público.

<div>

<div dir="ltr">

<div dir="ltr">

SINOPSIS: Dos mujeres y un hombre quieren salir de esa zona gris que es la pérdida del sentido de la vida, ocasionado por la guerra. Los cuerpos sin vida han dejado sus huellas inscritas en el agua, y permanecen suspendidas en el tiempo quebrado e inconcluso. "Mujer, Poeta y Devota", son los personajes encargados de crear las circunstancias para que el río hable.

FICHA TÉCNICA: Creación colectiva de Nohra Gonzalez Reyes, Alexandra Escobar Allión, Cesar Badillo. Asesoría dramaturgia de Felipe Vergara y el grupo. Escenografía de Mónica Bastos. Música de Edson Velandia. Construcción de muñecos por "Hombre del Sol". Dirección general por Cesar Badillo.

</div>

</div>

</div>
