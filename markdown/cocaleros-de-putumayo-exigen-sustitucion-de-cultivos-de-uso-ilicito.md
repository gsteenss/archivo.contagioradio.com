Title: Cocaleros de Putumayo exigen sustitución de cultivos de uso ilícito
Date: 2015-02-02 18:41
Author: CtgAdm
Category: Movilización, Resistencias
Tags: campesinos cocaleros, paz, Putumayo, Zona de Reserva Campesina
Slug: cocaleros-de-putumayo-exigen-sustitucion-de-cultivos-de-uso-ilicito
Status: published

###### Foto:hennergualma 

##### [Entrevista a Sandra Lagos:]<iframe src="http://www.ivoox.com/player_ek_4028750_2_1.html?data=lZWfmpyZdI6ZmKiakpyJd6KlmZKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRh8Lh0crgy9PTt4zV18bZw9OPtNPj0drS1dnFb8XZzZDU0cfNqdPi0JDg0cfWqYzk09Td18rXuI6ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe> 

Más de **1200 campesinos, indígenas y afrodescendientes del municipio de Puerto Asís se reunieron el pasado Sábado 31 de Enero** en el Coliseo de La institución Educativa San Francisco de Asís, para discutir sus propuestas en torno a la construcción de un **plan de sustitución gradual y concertada de los cultivos de coca** con el gobierno nacional.

La asamblea inició a las 10 am, los asistentes escucharon atentamente la presentación hecha por integrantes de la comisión de **sustitución gradual de cultivos de coca de la Mesa Regional de Organizaciones Sociales** quienes han venido adelantando la construcción de la propuesta retomando las exigencias históricas hechas desde las marchas cocaleras del 96 por las comunidades del departamento.

Después de conocer la propuesta los asistentes tuvieron el espacio para tomar el micrófono y plantear sus opiniones y propuestas, instalándose luego mesas de trabajo desarrolladas por corregimientos e inspecciones del municipio; ahí las comunidades se hicieron participes de una encuesta sobre los principales problemas de la región y la persistencia de los cultivos de coca, manifestando mediante una carta de intención su voluntad de entrar en un programa de sustitución gradual y concertada, manifestando que para ello el gobierno deberá cumplir primero con su palabra y establecer las **condiciones políticas, sociales, económicas, culturales y ambientales que les permita dejar definitivamente el cultivo de coca.**

Finalmente dejaron plasmado a través de dibujos y mensajes en carteleras sus **exigencias al gobierno nacional, así como la oportunidad de reconstruir la confianza perdida** por el histórico abandono y los continuos incumplimientos de los distintos gobiernos nacionales.

Estas asambleas se vienen desarrollando en todo el departamento. Hasta el momento se han congregado más de 3000 campesinos indígenas y afrodescendientes en los distintos lugares donde se ha realizado, la **próxima se llevará a cabo en el municipio de Puerto Leguízamo este viernes 6 de febrero** en la inspección de Piñuña Negro donde se espera nuevamente una masiva participación comunitaria.
