Title: Opinion
Date: 2018-03-16 14:58
Author: AdminContagio
Slug: opinion
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/05/OPINION-YOUTUBERS.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/02/Opinions-1.png" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/02/Opinions-2.png" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/02/Opinions-3.png" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/02/Opinions-4.png" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/02/Opinions-5.png" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/02/Opinions-6.png" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/03/Ana-María-Ribón-150x150.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/03/camilo-alvarez-150x150.png" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/03/Cindy-espitia-150x150.png" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/03/AIIH-150x150.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/03/Carlos-Julio-Diaz-L-150x150.png" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/03/00-foto-perfil-posible1-150x150.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/03/columnistas-invitados.png" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/archivo.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/foro-nacional-por-colombia.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/ANTONIO-GARCIA.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/justapaz.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/A-QUIEN-CORRESPONDE.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/Deborah-Anaya-Esguerra.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/03/WhatsApp-Image-2020-03-03-at-12.44.36.jpeg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

### OPINIÓN

[Opinion](https://archivo.contagioradio.com/categoria/opinion/)[![La Guajira, volver a soñar un territorio libre y vivo](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/03/Foto-demanda-cerrejon-300x225.jpg "La Guajira, volver a soñar un territorio libre y vivo"){width="300" height="225" sizes="(max-width: 300px) 100vw, 300px" srcset="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/03/Foto-demanda-cerrejon-300x225.jpg 300w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/03/Foto-demanda-cerrejon-768x576.jpg 768w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/03/Foto-demanda-cerrejon-370x278.jpg 370w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/03/Foto-demanda-cerrejon.jpg 800w"}](https://archivo.contagioradio.com/la-guajira-volver-a-sonar-un-territorio-libre-y-vivo/)  

##### [La Guajira, volver a soñar un territorio libre y vivo](https://archivo.contagioradio.com/la-guajira-volver-a-sonar-un-territorio-libre-y-vivo/)

[Carolina Garzon Diaz](https://archivo.contagioradio.com/author/carolina-garzon-diaz/)[<time datetime="2019-03-11T08:21:36+00:00" title="2019-03-11T08:21:36+00:00">marzo 11, 2019</time>](https://archivo.contagioradio.com/2019/03/11/)[0](https://archivo.contagioradio.com/la-guajira-volver-a-sonar-un-territorio-libre-y-vivo/#respond)36 años de presencia y actividad carbones del Cerrejón, el proyecto de explotación de carbón de mayor extensión en Colombia y la mina de carbón a cielo abierto más grande de Sur América.  
[![¿Adversarios o enemigos?](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/02/¿Adversarios-o-enemigos-550x385.jpg "¿Adversarios o enemigos?"){width="550" height="385" sizes="(max-width: 550px) 100vw, 550px" srcset="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/02/¿Adversarios-o-enemigos-550x385.jpg 550w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/02/¿Adversarios-o-enemigos-370x260.jpg 370w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/02/¿Adversarios-o-enemigos-110x78.jpg 110w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/02/¿Adversarios-o-enemigos-216x152.jpg 216w"}](https://archivo.contagioradio.com/adversarios-o-enemigos/)  

###### [¿Adversarios o enemigos?](https://archivo.contagioradio.com/adversarios-o-enemigos/)

[Johan Mendoza Torres](https://archivo.contagioradio.com/author/johan-mendoza-torres/)[<time datetime="2019-02-22T06:00:16+00:00" title="2019-02-22T06:00:16+00:00">febrero 22, 2019</time>](https://archivo.contagioradio.com/2019/02/22/)Quisimos ser adversarios, pero nos equipararon con bestias salvajes, mientras...  
[![Haití arde mientras el mundo lo ignora](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/02/haiti-arde-770x400-550x385.jpg "Haití arde mientras el mundo lo ignora"){width="550" height="385" sizes="(max-width: 550px) 100vw, 550px" srcset="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/02/haiti-arde-770x400-550x385.jpg 550w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/02/haiti-arde-770x400-370x260.jpg 370w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/02/haiti-arde-770x400-110x78.jpg 110w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/02/haiti-arde-770x400-216x152.jpg 216w"}](https://archivo.contagioradio.com/haiti-en-llamas/)  

###### [Haití arde mientras el mundo lo ignora](https://archivo.contagioradio.com/haiti-en-llamas/)

[Contagio Radio](https://archivo.contagioradio.com/author/admin/)[<time datetime="2019-02-13T17:37:26+00:00" title="2019-02-13T17:37:26+00:00">febrero 13, 2019</time>](https://archivo.contagioradio.com/2019/02/13/)Séptimo día de manifestaciones convocadas por la oposición y diferentes...  
[![Arias: no lloraremos por ti](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/02/ANDRÉS-FELIPE-ARIAS28SEP-770x400-550x385.jpg "Arias: no lloraremos por ti"){width="550" height="385" sizes="(max-width: 550px) 100vw, 550px" srcset="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/02/ANDRÉS-FELIPE-ARIAS28SEP-770x400-550x385.jpg 550w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/02/ANDRÉS-FELIPE-ARIAS28SEP-770x400-370x260.jpg 370w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/02/ANDRÉS-FELIPE-ARIAS28SEP-770x400-110x78.jpg 110w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/02/ANDRÉS-FELIPE-ARIAS28SEP-770x400-216x152.jpg 216w"}](https://archivo.contagioradio.com/61229-2/)  

###### [Arias: no lloraremos por ti](https://archivo.contagioradio.com/61229-2/)

[Johan Mendoza Torres](https://archivo.contagioradio.com/author/johan-mendoza-torres/)[<time datetime="2019-02-07T06:00:43+00:00" title="2019-02-07T06:00:43+00:00">febrero 7, 2019</time>](https://archivo.contagioradio.com/2019/02/07/)or más de que los medios corporativos de información anden...  
[![¿Polarizar o no polarizar Dr. Wasserman?](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/01/polarizar-o-no-polarizar-770x400-550x385.png "¿Polarizar o no polarizar Dr. Wasserman?"){width="550" height="385" sizes="(max-width: 550px) 100vw, 550px" srcset="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/01/polarizar-o-no-polarizar-770x400-550x385.png 550w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/01/polarizar-o-no-polarizar-770x400-370x260.png 370w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/01/polarizar-o-no-polarizar-770x400-110x78.png 110w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/01/polarizar-o-no-polarizar-770x400-216x152.png 216w"}](https://archivo.contagioradio.com/polarizar-o-no-polarizar-dr-wasserman/)  

###### [¿Polarizar o no polarizar Dr. Wasserman?](https://archivo.contagioradio.com/polarizar-o-no-polarizar-dr-wasserman/)

[Johan Mendoza Torres](https://archivo.contagioradio.com/author/johan-mendoza-torres/)[<time datetime="2019-01-19T07:24:22+00:00" title="2019-01-19T07:24:22+00:00">enero 19, 2019</time>](https://archivo.contagioradio.com/2019/01/19/)El movimiento a favor de la polarización no es tan...  
[![El 2019 en las urnas del mundo](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/01/presidente-y-vice-e1546803857848-770x400-550x385.jpg "El 2019 en las urnas del mundo"){width="550" height="385" sizes="(max-width: 550px) 100vw, 550px" srcset="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/01/presidente-y-vice-e1546803857848-770x400-550x385.jpg 550w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/01/presidente-y-vice-e1546803857848-770x400-370x260.jpg 370w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/01/presidente-y-vice-e1546803857848-770x400-110x78.jpg 110w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/01/presidente-y-vice-e1546803857848-770x400-216x152.jpg 216w"}](https://archivo.contagioradio.com/el-2019-en-las-urnas-del-mundo/)  

###### [El 2019 en las urnas del mundo](https://archivo.contagioradio.com/el-2019-en-las-urnas-del-mundo/)

[Contagio Radio](https://archivo.contagioradio.com/author/admin/)[<time datetime="2019-01-14T13:59:58+00:00" title="2019-01-14T13:59:58+00:00">enero 14, 2019</time>](https://archivo.contagioradio.com/2019/01/14/)En más de 60 Estados a lo largo y a...  
[![2018: Un año de movilizaciones sociales](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/12/movilización-censat-770x400-550x385.jpg "2018: Un año de movilizaciones sociales"){width="550" height="385" sizes="(max-width: 550px) 100vw, 550px" srcset="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/12/movilización-censat-770x400-550x385.jpg 550w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/12/movilización-censat-770x400-370x260.jpg 370w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/12/movilización-censat-770x400-110x78.jpg 110w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/12/movilización-censat-770x400-216x152.jpg 216w"}](https://archivo.contagioradio.com/2018-un-ano-de-movilizaciones-sociales/)  

###### [2018: Un año de movilizaciones sociales](https://archivo.contagioradio.com/2018-un-ano-de-movilizaciones-sociales/)

[Censat Agua Viva](https://archivo.contagioradio.com/author/censat/)[<time datetime="2018-12-23T11:55:27+00:00" title="2018-12-23T11:55:27+00:00">diciembre 23, 2018</time>](https://archivo.contagioradio.com/2018/12/23/)El 2018 terminó con una representativa y creativa movilización del...  
[![Mensaje de navidad para un Estado secuestrado](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/12/Mensaje-de-navidad-para-un-Estado-secuestrado-770x400-550x385.png "Mensaje de navidad para un Estado secuestrado"){width="550" height="385" sizes="(max-width: 550px) 100vw, 550px" srcset="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/12/Mensaje-de-navidad-para-un-Estado-secuestrado-770x400-550x385.png 550w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/12/Mensaje-de-navidad-para-un-Estado-secuestrado-770x400-370x260.png 370w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/12/Mensaje-de-navidad-para-un-Estado-secuestrado-770x400-110x78.png 110w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/12/Mensaje-de-navidad-para-un-Estado-secuestrado-770x400-216x152.png 216w"}](https://archivo.contagioradio.com/mensaje-de-navidad-para-un-estado-secuestrado/)  

###### [Mensaje de navidad para un Estado secuestrado](https://archivo.contagioradio.com/mensaje-de-navidad-para-un-estado-secuestrado/)

[Johan Mendoza Torres](https://archivo.contagioradio.com/author/johan-mendoza-torres/)[<time datetime="2018-12-19T06:00:46+00:00" title="2018-12-19T06:00:46+00:00">diciembre 19, 2018</time>](https://archivo.contagioradio.com/2018/12/19/)A esa gente, que tiene secuestrado al Estado Colombiano y...  
[](https://archivo.contagioradio.com/de-la-onirica-a-la-politica/)  

###### [De la onírica a la política](https://archivo.contagioradio.com/de-la-onirica-a-la-politica/)

[Johan Mendoza Torres](https://archivo.contagioradio.com/author/johan-mendoza-torres/)[<time datetime="2018-11-28T09:44:04+00:00" title="2018-11-28T09:44:04+00:00">noviembre 28, 2018</time>](https://archivo.contagioradio.com/2018/11/28/)Lo que hoy hacen por ejemplo Uribe, Carrasquilla y Martínez...  
[](https://archivo.contagioradio.com/las-licencias-obligatorias-una-herramienta-de-politica-publica-que-salva-vidas/)  

###### [Las licencias obligatorias, una herramienta de política pública que salva vidas](https://archivo.contagioradio.com/las-licencias-obligatorias-una-herramienta-de-politica-publica-que-salva-vidas/)

[Contagio Radio](https://archivo.contagioradio.com/author/admin/)[<time datetime="2018-11-10T07:00:26+00:00" title="2018-11-10T07:00:26+00:00">noviembre 10, 2018</time>](https://archivo.contagioradio.com/2018/11/10/)En salud se conoce como licencia obligatoria la autorización que...  
[![Desde el sótano](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/11/FUEGO-770x400-550x385.jpg "Desde el sótano"){width="550" height="385" sizes="(max-width: 550px) 100vw, 550px" srcset="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/11/FUEGO-770x400-550x385.jpg 550w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/11/FUEGO-770x400-370x260.jpg 370w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/11/FUEGO-770x400-110x78.jpg 110w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/11/FUEGO-770x400-216x152.jpg 216w"}](https://archivo.contagioradio.com/desde-el-sotano/)  

###### [Desde el sótano](https://archivo.contagioradio.com/desde-el-sotano/)

[Johan Mendoza Torres](https://archivo.contagioradio.com/author/johan-mendoza-torres/)[<time datetime="2018-11-09T15:33:08+00:00" title="2018-11-09T15:33:08+00:00">noviembre 9, 2018</time>](https://archivo.contagioradio.com/2018/11/09/)Según Schopenhauer toda verdad pasa por tres etapas. En la...  
[![La protesta sumisa   ](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/10/la-protesta-sumisa-1-550x385.jpg "La protesta sumisa   "){width="550" height="385" sizes="(max-width: 550px) 100vw, 550px" srcset="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/10/la-protesta-sumisa-1-550x385.jpg 550w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/10/la-protesta-sumisa-1-370x260.jpg 370w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/10/la-protesta-sumisa-1-110x78.jpg 110w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/10/la-protesta-sumisa-1-216x152.jpg 216w"}](https://archivo.contagioradio.com/la-protesta-sumisa/)  

###### [La protesta sumisa   ](https://archivo.contagioradio.com/la-protesta-sumisa/)

[Johan Mendoza Torres](https://archivo.contagioradio.com/author/johan-mendoza-torres/)[<time datetime="2018-10-25T09:55:22+00:00" title="2018-10-25T09:55:22+00:00">octubre 25, 2018</time>](https://archivo.contagioradio.com/2018/10/25/)Lograron ofender a los manifestantes más por un grafiti que...  
[](https://archivo.contagioradio.com/civilizacion-y-barbarie/)  

###### [Civilización y barbarie](https://archivo.contagioradio.com/civilizacion-y-barbarie/)

[Johan Mendoza Torres](https://archivo.contagioradio.com/author/johan-mendoza-torres/)[<time datetime="2018-10-12T07:00:29+00:00" title="2018-10-12T07:00:29+00:00">octubre 12, 2018</time>](https://archivo.contagioradio.com/2018/10/12/)Civilización y barbarie fundamentan todos los intentos por mantener intacto...
