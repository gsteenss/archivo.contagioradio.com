Title: John Paul Lederach: Overcoming apathy, the greatest challenge seeking the truth in Colombia
Date: 2019-04-29 16:14
Author: CtgAdm
Category: English
Tags: Colombia’s Truth Commission
Slug: john-paul-lederach-overcoming-apathy-the-greatest-challenge-seeking-the-truth-in-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/Schermata-2019-04-29-alle-15.50.12.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

The academic and peacebuilding expert, **John Paul Lederach**, reflects on a selection of 34 Peace Agreements that were used for the Truth Commission mechanisms in Colombia. He stated that we could learn from contained mistakes and successes seeing previous experiences to strengthen the work of the **Colombia’s Truth Commission**. The professor points out how the coexistence and non-repetition behaviors are important virtues for the Commission Colombia. In the course of the interview, **he refers to the opportunities and challenges that will be present during these three years of work in which the Commission will seek to clarify what happened for more than half a century of armed conflict**.

 

**How was the process carried out to select the commissions’** **models as an example for the Colombian's Truth Commission?**

As a starting point, truth commissions were chosen from which emerged from national peace agreements and above all from the comparative work of the [Kroc Institute](https://kroc.nd.edu/research/peace-processes-accords/pam-colombia/). There are commissions that have not had to do with peace agreements in the context of armed conflict and simply arose for other reasons like Argentina or other parts of the world.

They were a total of 34 agreements among these, 12 commissions were approved and another 2 fell before they started. We study their implementation and compare them. Colombia's Truth Commission has a deeper mandate, the former around the world were specific but did not contemplate coexistence or non-repetition and the focus was more on historical events. In South Africa made a process that had similarities with that of Colombia. But this combination of past, present and future is new, it also adds more complexity to our work and with less time. It is three years, rather short time given the complexity of the Colombian conflict over time.

 

**Which type of mistakes committed the commissions that failed in fulfill their mandate? **

A comparative work is very important as well as points out the events in a correct way. It is necessary to implement the right policies because often a commission of the truth can be a reason of political differences in a country and lead to misunderstandings. For example, in Nepal it took a long time to start the real commission and still today they are having difficulties founding the truth. For its part, Colombia started with the Commission in the time designated by the agreement what leads for a good path. Truth commissions must be transparent. People has to understand that they are not divided into bad and good, the country has to realize what happened and give to this sufferance a sense of justice. Most of the people involved were innocent, and Colombians  has to look at this conflict as something that have affected all the citizens, this is not a matter of  "them " is a matter of “us". The model of forgiving and forgetting as a recipe for change does not work, because it is transgenerational and affects the next generations; the change of behavior is the final purpose of that idea of ​​coexistence and not repetition, we will only know until later if the country manages to understand it and if it is possible that we join and look at the form that is not repeated.

 

**You indicate in one of your articles the importance of** **“Curiosity”** **because it calls for dialogue and awakens the interest of the people. How could curiosity wake up the consciousness in a country like Colombia? **

It is not easy, people  has to be able to understand that what is passed is part of their wider coexistence. It is not possible to avoid being part of the network of relationships affected by what has happened and moreover is not possible to ignore as if it did not exist.  Is a matter of consciousness and acceptation. Is not a matter of who is or is not right. The main challenge is to understand that this is part of who Colombians are and if we as human beings are not able to face who we are, there are many possibilities to repeat it.

![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/Schermata-2019-04-29-alle-15.50.12-300x88.png){.alignnone .wp-image-65992 width="375" height="110"}

The biggest challenge to overcome is the apathy and the lack of interest because many Colombian believes that it does not affect them. Transgenerational policies are important because the history of this war is having a tremendous impact on future generations. We have to call the future, listen to the people’s life stories and learn from them, this should be the starting point of the Commission.

 

**Some says that this Government has not given enough support to the Integral System of Truth, Justice, Reparation and Non-Repetition. Was this situation presented in other Commissions?**

Yes, there are parallels but the case of Colombia is different in one main aspect: in many places armed conflicts have occurred in countries with much more poverty than Colombia such as Nepal, Sierra Leone or Liberia. In that case, the Government did not  have the resources and much of the help came from international cooperation . The case of Colombia serves as an example for the world, that is the reason why many countries are watching closely to Colombia and how the Commission is going to achieve this transition in order to let peace lasting. This requires an investment of capital not only of money, but also of thought, skills and collective efforts.
