Title: Hay patrones comunes en asesinatos de defensores  de DDHH en el país
Date: 2017-05-18 13:29
Category: DDHH, Nacional
Tags: Asesinatos a líderes comunitarios, Asesinatos a líderes LGBT, liderezas
Slug: hay-patrones-comunes-en-asesinatos-de-defensores-de-ddhh-en-el-pais
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/02/derechos-humanos.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [18 mayo 2017]

**Los 180 asesinatos de defensores de Derechos Humanos en menos de un año**, han ocurrido en zonas que han sido desocupadas por integrantes de la FARC y que ahora tienen una fuerte presencia de militares y policías. Estas cifras y argumentos los presentó el representante por el Polo Democrático Alirio Uribe en audiencia sobre crímenes de defensores de Derechos Humanos.

Alirio Uribe, aseguró que “en los 170 municipios que tiene priorizado la fuerza pública, es donde han ocurrido los homicidios”. Esto significa, para el representante, que **el patrón que existe en estos hechos es que hay una responsabilidad del Estado,** bien sea porque hay una mutación de los paramilitares o porque la fuerza pública está haciendo uso de la información de la inteligencia del Estado para cometer los crímenes. Le puede interesar: ["185 defensores de derechos humanos han sido asesinados en el último año"](https://archivo.contagioradio.com/185-defensores-de-derechos-humanos-han-sido-asesinados/)

Cabe resaltar que a los asesinatos de los líderes sociales se suman los crímenes de ex guerrilleros y sus familiares. El Informe Especial del Movimiento Marcha Patriótica, que habla sobre la situación de derechos humanos, seguridad y protección, resalta que tan sólo en 12 días, entre el 14 de abril y el 25 de abril del 2017, **se ha reportado el homicidio de 2 integrantes activos de las FARC –EP y el homicidio de 6 familiares de integrantes activos de esa guerrilla** que se encuentran en zonas veredales.

Si bien el Estado ha negado la existencia de grupos paramilitares y la sistematicidad de los crímenes, el representante recordó que “la estructura del crimen organizado tiene de por medio a militares y policías que han sido destituidos por vínculos con las bandas criminales. **Se ha demostrado que en 25 casos de homicidio hubo participación activa del Estado”.** Ante esto y según Uribe “el gobierno debe tomar medidas contra las bandas criminales y asumir su responsabilidad.” Le puede interesar: ["Protección a defensores de derechos humanos como garantía de paz".](https://archivo.contagioradio.com/185-defensores-de-derechos-humanos-han-sido-asesinados/)

Durante la audiencia se dio cuenta que el en el sector rural, los líderes y liderezas que han apoyado procesos a favor de las zonas de reserva campesina, la sustitución de cultivos ilícitos, los indígenas que defienden sus territorios y los afrocolombianos ambientalistas, **son los que más riesgo tienen.**

Por su lado, l**os más afectados en el sector urbano** son las organizaciones de víctimas, líderes sociales, organizaciones de mujeres LGBTI y ambientalistas. En lo que respecta a las mujeres, en 2016 el 27% de los asesinatos han estado dirigidos hacia las defensoras de derechos humanos donde 14 han sido asesinadas.

Sin dejar a un lado los avances que ha tenido el proceso de paz para evitar que se sigan cometiendo asesinatos, Alirio Uribe recordó que **“el país se ha ahorrado más de 1200 muertes de militares, policías y guerrilleros”.** Igualmente condenó el crimen de 11 policías por parte del clan del golfo y celebró las capturas que se han hecho contra este grupo. Uribe recordó que “vamos a seguir denunciando y haciendo debates de control político porque las cifras de estos asesinatos deben ser cero”.

<iframe id="audio_18785452" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_18785452_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
