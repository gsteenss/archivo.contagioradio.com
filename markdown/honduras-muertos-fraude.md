Title: Honduras rechaza reconteo de votos por tribunal que "avaló el fraude"
Date: 2017-12-12 08:22
Category: El mundo, Política
Tags: fraude en elecciones, honduras, Juan Orlando Hernández, Salvador Nasralla
Slug: honduras-muertos-fraude
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/12/honduras_protestas.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: CNN ] 

###### [11 Dic 2017] 

El supuesto recuento de votos realizado por el Tribunal Electoral de Honduras habría confirmado la reelección de Juan Orlando Hernández como presidente. Sin embargo el pueblo hondureño y al Partido de la Alianza de Oposición contra la Dictadura aseguran que **ese reconteo se da por parte de una entidad parcializada a favor de Hernández y que habría participado en el fraude.**

"El Tribunal Supremo Electoral, controlado por el actual mandatario, ha querido seguir unilateralmente el conteo con el apoyo de la embajada de Estados Unidos, avalando un proceso que a todas luces no respeta la libertad popular",  indica Rodolfo Pastor, vocero de la Alianza de Oposición contra la Dictadura.

El vocero agrega que ante dicha situación, continuaron las movilizaciones de la ciudadanía durante este fin de semana en rechazo al resultado del reconteo y exigiendo la presencia de un organismo internacional que avale las actas y cuadernillos de votación.

### **Las movilizaciones y acciones jurídicas contra la reelección** 

La represión contra la movilización social ya deja 14 muertos, pero sin importar las acciones violentas del gobierno contra la protesta, este domingo, las manifestaciones se **concentraron en la embajada de Estados Unidos, donde el pueblo hondureño exigió al gobierno norteamericano,** "que respete la voluntad de la ciudadanía evidenciada en las urnas el pasado 26 de noviembre".

De igual manera se han anunciado una serie de movilizaciones en diferentes puntos del país durante esta semana ya que aseguran que no van a aceptar un resultado fraudulento. Sin embargo las denuncias van en aumento, dado que los militares se han volcado a las calles a levantar las barricadas instaladas por los manifestantes en varias provincias del país centroamericano.

En materia jurisprudencial, el pasado viernes los partidos que participaron en la elección pudieron presentar las impugnaciones por las cuales argumentan que hubo manipulación de los resultados. No obstante, Pastor asegura que lo más probable es que como dicho trámite lo debe revisar el mismo tribunal electoral, es probable que nuevamente se falle a favor de Hernández. (Le puede interesar: [Policía hondureña decidió que no reprimirá la movilización)](https://archivo.contagioradio.com/elecciones_honduras_fraude/)

El vocero asegura que en esta ocasión el fraude ha sido de una manera tan evidente ante la ciudadanía y los organismos internacionales, que **"la legitimidad de Juan Orlando Hernández como presidente es muy reducida",** dice Pastror, y añade que aunque "Hay una lucha difícil por delante debido al gobierno represivo y dictatoria la movilización social es la única opción que tiene el pueblo hondureño".

###### Reciba toda la información de Contagio Radio en [[su correo]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio]{.s1}](http://bit.ly/1ICYhVU)[.]{.s1}
