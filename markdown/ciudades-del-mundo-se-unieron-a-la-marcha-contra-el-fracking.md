Title: Ciudades del mundo se unieron a la marcha contra el fracking
Date: 2019-06-07 18:26
Author: CtgAdm
Category: Ambiente, Movilización
Tags: fracking
Slug: ciudades-del-mundo-se-unieron-a-la-marcha-contra-el-fracking
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/WhatsApp-Image-2019-06-07-at-5.57.15-PM.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

Mientras que más de 70 municipios de Colombia marcharon hoy en contra del fracking y la minería contaminante, manifestantes de ciudades del mundo como Roma, Buenos Aires y París, también salieron a las calles para mostrar su respaldo a esta iniciativa.

En la capital italiana, ambientalistas organizaron una "minga cultural" para sensibilizar a través de manifestaciones artísticas a la ciudadanía europea a lo que ocurre en Colombia y así, movilizarlos a tomar parte a la lucha en defensa del territorio andino. Así lo afirmó Gloria Mendiola, directora colombiana de la Asociación de Promoción Social Migras y la Red Cultural Internacional Colombia es Cultura. (Le puede interesar: "[Cien municipios saldrán en rechazo al fracking](https://archivo.contagioradio.com/mas-de-100-municipios-saldran-en-rechazo-al-fracking/)")

"**Para nosotros que estamos afuera y para nuestros compañeros colombianos es fundamental que haya una actividad de sensibilización en todo el mundo de manera que la opinión publica y la opinión internacional voltee la mirada hacia Colombia y ayuden a hacer presión para que el gobierno entienda que es importante defender nuestros territorios"**, afirmó la directora.

Mendiola explicó que fenómenos como el fracking tienen implicaciones nocivas para el ambiente y que es importante dar visibilidad a estas problemáticas en Europa a través de iniciativas alternativas dado que las noticias que llegan a Europa son fragmentadas y pueden no reflejar las realidades en Colombia.

“**Cuando se habla por ejemplo de asesinatos de lideres sociales las cifras nunca corresponden a lo que pasa en las noticias"**,  señaló la activista.

Finalmente agregó que “**la mejor ayuda es que la gente de las asociaciones o de los medios de comunicación abran sus canales, comuniquen, cuenten, mantengan la memoria**. Si no hay una civilidad correcta en nuestra cotidianidad es imposible salir de estos problemas en Colombia sino también en el exterior. La mirada tiene que estar atenta y constante y acciones como la de ustedes y como la nuestra”.

<iframe id="audio_36821624" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_36821624_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
