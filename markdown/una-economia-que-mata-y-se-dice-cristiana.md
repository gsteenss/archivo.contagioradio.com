Title: Una economía que mata y se dice cristiana
Date: 2020-04-10 21:28
Author: CtgAdm
Category: A quien corresponda, Opinion
Tags: cristianas, cristianismo., Diezmo, sistema económico
Slug: una-economia-que-mata-y-se-dice-cristiana
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/carretera.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right"} -->

**Con la medida que midan a los otros, los medirán a ustedes**. (Mateo 7,2) 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Bogotá, 28 de marzo del 2020

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

*Hay cristianos que no reconocemos los pecados personales, sociales y ambientales,  mientras exigimos a los demás que no pequen, o los criticamos por pecadores.  *

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Estimado 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

**Hermano en la fe.** Cristianos, cristianas, personas interesadas

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En tu círculo cristiano y político, el tema económico sólo aparece para motivar y justificar los diezmos y las ofrendas, no para ver las repercusiones en la vida concreta de todas las personas o para analizar su impacto, mucho menos para cuestionar un sistema económico que coloca al centro la ganancia, la acumulación y el dinero por encima del ser humano, de la naturaleza-creación, incluso de Dios, aunque diga lo contrario.   

<!-- /wp:paragraph -->

<!-- wp:heading {"level":5} -->

##### El dinero se necesita para la vida: comida, vivienda, vestido, salud, educación, comunicación, servicios básicos, incuso para lo religioso.

<!-- /wp:heading -->

<!-- wp:paragraph -->

Trabajamos para ganar el dinero necesario para vivir, según el dinero ganado y los valores que orienten la vida, tendremos una mejor o peor calidad de vida, entendida como la digna satisfacción de las necesidades básicas. Además, tenemos la aspiración de ganar un poco de lo que necesitamos hoy, para dejar algo para “mañana”. Hay quienes su trabajo o actividad económica, les garantiza una vida digna, pero a la mayoría de población, si tienen trabajo no les alcanza para vivir dignamente o no tienen trabajo estable y por eso carecen de lo necesario. Las mayorías trabajan duramente y con dificultad sobreviven a pesar de la riqueza que generan. Son empobrecidas.   

<!-- /wp:paragraph -->

<!-- wp:heading {"level":5} -->

##### La realidad en el mundo es muy grave.

<!-- /wp:heading -->

<!-- wp:paragraph -->

Veamos **el empobrecimiento y el hambre:** en el 2018, Colombia tenía 48.258.000 habitantes, según DANE, de ellos el 27 %, es decir, 13.029.000, viven la pobreza monetaria (personas cuyos ingresos no les alcanza para comprar una canasta básica de alimentos y no alimentos); el 19%, es decir, 9.450.000, pobreza multidimensional (personas con múltiples carencias básicas); el 7.2%, es decir, 3.508.000, pobreza extrema (ordinariamente pasan hambre y carecen de lo básico); hoy las cifras son mayores. En el mundo, según Manos Unidas, 821 millones de personas pasan hambre.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":5} -->

##### Y **los daños ambientales**...

<!-- /wp:heading -->

<!-- wp:paragraph -->

Solo en 2017, fueron destruidos 15,8 millones de hectáreas de bosques tropicales en el mundo (acabando con el hábitat de pueblos ancestrales y múltiples especies vivas) para monocultivos, productos suntuarios o combustibles; la minería y la industria extractiva, siguen envenenando las aguas, afectando salud humana y del planeta; la contaminación del aire causa la muerte prematura entre 6 y 7 millones de personas al año y cerca de 1.400 millones por enfermedades asociadas al consumo de agua contaminada. Todo para generar “riqueza”, “progreso” y “desarrollo”.  

<!-- /wp:paragraph -->

<!-- wp:heading {"level":5} -->

##### Veamos la otra cara de la realidad:

<!-- /wp:heading -->

<!-- wp:paragraph {"fontSize":"normal"} -->

*“La mayor e intolerable desigualdad es que un 1% de la humanidad (70 millones de personas) posea casi tanta riqueza como el 99% restante. Según Oxfam, ocho millonarios (todos ellos varones, 6 norteamericanos, un mexicano y un español) tiene más riqueza que la mitad más pobre del mundo (unos 3500 millones)”*. - **José Ignacio  GONZÁLEZ FAUS*,  ¿Apocalipsis hoy? Contra la entropía social*, Sal Terrae, 2019, 39.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Según el Banco Mundial, Colombia es el segundo país más desigual de América Latina y el séptimo en todo el mundo, Según la Comisión económica para América Latina –**CEPAL**, el 1 por ciento más rico de Colombia concentra el 20 por ciento del ingreso”. Una comparación para entender mejor: supongamos que hay 100 habitantes y que  ingreso total son 250 pollos, según estos datos la distribución es así: la persona (1) más rica se lleva 50 pollos y cada una de las 99 restantes 2 pollos. *“Un millón de hogares campesinos tiene menos tierra que una vaca”,* [titula un artículo de la revista semana,](https://sostenibilidad.semana.com/impacto/articulo/concentracion-de-la-tierra-en-colombia-el-1-por-ciento-de-las-fincas-mas-grandes-ocupan-el-81-por-ciento-de-la-tierra/40882) que señala que el 1% de las fincas de mayor tamaño tienen en su poder el 81 % de la tierra colombiana. 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Esta realidad se agrava con el coronavirus porque el **pueblo común y corriente** llevará la peor  parte, en muertos y en hambre.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

[Mons. Barreto, obispo de Quibdó, puso el “dedo en la llaga”](https://www.elespectador.com/colombia2020/opinion/no-es-un-asunto-de-limosnas-columna-910983). Con datos oficiales (Supersociedades y Superfinanciera), de las grandes empresas y del sector financiero, mostró que entre 2016 y el 2019 habían tenido utilidades (lo que queda después de descontar los gastos e inversiones) por más 270 billones de pesos; que en la última reforma tributaria, el gobierno exoneró de impuestos a las grandes empresas de 10 billones. El dinero necesario para enfrentar esta pandemia es entre 12 y 15 billones.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":5,"customTextColor":"#064864"} -->

##### *“Si sabemos en los bolsillos de quiénes está el dinero, lo más lógico es que podamos exigirles solidaridad en este momento de crisis para la Nación”.* 

<!-- /wp:heading -->

<!-- wp:paragraph -->

Además, la corrupción le cuesta al país 50 billones que van a parar al bolsillo del poder económico y político. 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Para entendernos: ¿Cuánto es un billón? Es un millón de millones, es decir, que si una persona se gana un (1) millón de pesos al mes, un billón es la plata que se ganan un (1) millón de personas en un mes. 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Es importante tener en cuenta que las leyes las hacen y aprueban políticos elegidos con el apoyo de grandes grupos económicos y financieros, es decir, que quedan endeudados y pagan  colocándose a su servicio, legislando en su beneficio y eligiendo funcionarios que les funcionan a ellos y no al pueblo, al que dicen servir. Ya sabemos, “quien pone la plata pone las condiciones”. 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Frente a esta realidad, hay unas “corrientes” religiosas que afirman que la situación económica de una persona es la “voluntad de Dios”, que si tiene dinero es bendecido por Dios, si no lo tiene es castigo de Dios, ya sea por su buena o mala viva.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Ordinariamente se evade la pregunta ética por la forma como se ha conseguido el dinero y no se aborda la justicia social (la justicia, valor central en la Palabra de Dios). Es cierto que una persona “espiritual” puede llevar una vida más austera y gastar menos y tener más ahorros que otra con los mismos ingresos y poco austera, pero millones de personas trabajan mucho y ganan poco y otros millones no tienen trabajo para satisfacer las necesidades básicas. 

<!-- /wp:paragraph -->

<!-- wp:heading {"level":5} -->

##### Esta realidad genera preguntas como estas:

<!-- /wp:heading -->

<!-- wp:list {"ordered":true} -->

1.  ¿Cómo entender que, si Dios es bueno y todopoderoso como predican, millones de personas mueran de hambre y permita que unos exploten a otros, los engañen, destruyan su medios de vida o no le importe las tragedias humanas?
2.  ¿El hambre, la miseria y empobrecimiento y la escandalosa acumulación de todos los bienes de la creación por parte de unos pocos, es voluntad de Dios? 
3.  ¿El actual manejo de la economía nivel nacional y mundial es justo, humano, lógico y de acuerdo a la Palabra de Dios y a la práctica de Jesús de Nazaret?
4.  ¿Cómo entender que miles de personas mueran de hambre y de enfermedades evitables, en países ricos que dicen creer en un Dios justo y misericordioso?
5.  ¿Crees que la falta alimento, salud, educación, agua potable, vivienda son temas “mundanos” en los cuales las iglesias no tienen nada qué hacer y qué decir?
6.  ¿Puede el cristianismo evadir la pregunta por la causa de estas profundas injusticias contras los seres humanos y la naturaleza? 

<!-- /wp:list -->

<!-- wp:paragraph -->

Recuerda que Jesús de Nazaret, en la parábola del juicio final (en la que muestra quienes se salvan y quienes se condenan), Mateo 25,31-64, llama a *los justos* a recibir el reino, es decir, la salvación y les explica la razón:

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

“*Porque tuve hambre y me dieron de comer, tuve sed y me dieron de beber, era emigrante y me recibieron, estaba desnudo y me vistieron, estaba enfermo y me visitaron, estaba encarcelado y me vinieron a ver*”. Ellos sorprendidos e incrédulos, preguntan: *“Señor, ¿cuándo te vimos hambriento y te alimentamos, sediento y te dimos de beber, emigrante y te recibimos, desnudo y te vestimos? ¿Cuándo te vimos enfermo o encarcelado y fuimos a visitarte?”* Entonces les contestará*: “Les aseguro que* ***lo que hayan hecho a uno solo de éstos, mis hermanos menores, me lo hicieron a mí”.*** Luego llama a los “malditos, es decir, los condenados y les explica la razón de sus condenación: “*Porque tuve hambre y no me dieron de comer, tuve sed y no me dieron de beber, era emigrante y no me recibieron, estaba desnudo y no me vistieron, estaba enfermo y encarcelado y no me visitaron”.* Ellos sorprendidos e incrédulos, preguntan: *“Señor, ¿cuándo te vimos hambriento o sediento, emigrante o desnudo, enfermo o encarcelado y no te socorrimos? Él responderá: Les aseguro que* ***lo que no hicieron a uno de estos más pequeños no me lo hicieron a mí”. ***  

<!-- /wp:paragraph -->

<!-- wp:heading {"level":5} -->

##### La actitud frente a los hambrientos, sedientos, desplazados o emigrantes, desnudos, enfermos y encarcelados es el criterio para determinar si alguien es buen cristiano y no las prácticas religiosas por importante que sean.

<!-- /wp:heading -->

<!-- wp:paragraph -->

Los millones de personas que pasan hambre, la contaminación de las aguas, el desplazamiento forzado, las necesidades básicas insatisfechas de las mayorías, el robo a la salud y su precariedad y la situación de las cárceles en Colombia, cuestionan profundamente a un país mayoritariamente cristiano (católico, evangélico o protestante). Esto es un escándalo para los que no creen. 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Lo cristiano es preguntar por las causas de la injusticia y el empobrecimiento, aunque genere conflictos, como le ocurrió a Helder Camera, un obispo brasileño, quien decía: “*Cuando doy limosna a los pobres dicen que soy un santo, cuando pregunto por qué hay pobres dicen que soy comunista”.* Palabras asumidas por líderes religiosos y cristianos de diversas iglesias y en diversas partes del mundo.  

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Fraternalmente, su hermano en la fe,

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

P. Alberto Franco, CSsR, J&P

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

<francoalberto9@gmail.com>  

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Ver Mas: [Nuestros columnistas](https://archivo.contagioradio.com/opinion/columnistas/)

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

  

<!-- /wp:paragraph -->
