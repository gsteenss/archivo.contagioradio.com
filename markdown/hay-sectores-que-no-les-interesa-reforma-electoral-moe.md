Title: Hay sectores que no les interesa una reforma electoral: MOE
Date: 2017-05-08 15:38
Category: Entrevistas, Política
Tags: Consejo Nacional Electoral, Corte Electoral, Reforma Electoral
Slug: hay-sectores-que-no-les-interesa-reforma-electoral-moe
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/09/dt.common.streams.StreamServer-e1475007648772.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo Particular] 

###### [08 May 2017] 

La próxima semana será el último plazo para que se presenten las recomendaciones a la propuesta de reforma electoral, hecha por la Misión Especial Electoral, que ha recibido fuertes críticas en dos puntos cruciales: **la creación de la Corte Electoral y la restructuración del Consejo Nacional Electoral.**

### **Una Corte Electoral para transformar la democracia** 

De acuerdo con Alejandra Barrios, directora de la Misión de Observación Electoral e integrante de la Misión Especial Electoral, la conformación de la Corte Electoral es primordial para que exista un cambió en la legitimidad de la democracia del país y argumentó que los miedos referentes a los súper poderes que tendría la Corte, **no son ciertos debido a que se mantiene el equilibrio de poderes.**

“Se mantiene la rama judicial con la Corte Constitucional, la Corte Suprema de Justicia, se mantiene el Consejo de Estado y se mantiene el Consejo Superior de la Judicatura, así que no estamos hablando de una Súper Corte” afirmó Barrios y explicó que el procedimiento que se realizará será **sacar de la sesión quinta del Consejo de Estado para transformarla en Corte Electoral.**

De igual modo, frente a las afirmaciones que se han hecho sobre el costo de crear una Corte Electoral, la directora de la MOE, expresó que es mucho más “**barato que todas las demandas que se tienen y elecciones atípicas**” por la falta de decisiones a tiempo. Le puede interesar: ["Estas son las recomendaciones de la Misión Electoral para la Reforma Política](https://archivo.contagioradio.com/estas-son-las-recomendaciones-de-la-mision-electoral-para-la-reforma-politica/)

**Un Consejo Nacional Electoral sin vicios partidistas**

Las modificaciones que se proponen en la reforma al Consejo Nacional Electoral tienen que ver con el origen de esta organización, que actualmente es partidista, **para crear una autoridad electoral con autonomía presupuestaría y personería jurídica,** separándola de los intereses políticos de los que actualmente hace parte.

Barrios señaló que la importancia de esta transformación radica en la falta de una autoridad que haga una veeduría y control sobre los partidos políticos “hoy a los partidos políticos no los gobierna ni dirige nadie, no tienen una autoridad que de manera seria ponga orden dentro de las organizaciones políticas y por eso vemos **los desórdenes en términos de avales, democracia interna, sin mecanismos de control reales que generan gobernadores corruptos**, concejales detenidos por relaciones con el narcotráfico …”.

De igual modo expresó que los partidos y tendencias políticas que están en contra de esta propuesta de reforma electoral tienen intenciones de **mantener la lentitud de la justicia y el “status quo” a través de estos mecanismos de control en la democracia**. Le puede interesar: ["Colombia tiene un sistema electoral que le permite elegir criminales: MOE"](https://archivo.contagioradio.com/sistema-electoral-elegir-criminales-35907/)

Sin embargo, Alejandra Barrios aseguró que la **Corte Electoral podría estar funcionando a finales de este año**, mientras que la democracia interna de los partidos que son lista cerrada con preponderancia de financiación estatal entraría en funcionamiento hasta el año 2019, al igual que las modificaciones al Consejo Nacional Electoral.

### **¿Hay garantías para las Circunscripciones de paz?** 

Otro de los temas que ha venido estudiando la Misión de Observación Electoral tiene que ver con las 16 circunscripciones de paz que nacen desde los Acuerdos de Paz de La Habana como un **escenario para ampliar la participación democrática de los sectores que han sido excluidos por el conflicto armado**.

No obstante, Alejandra Barrios señaló que para que estas tengan garantías es importante que haya ciertos ajustes por parte del Estado, como que **se instalen más puestos de votación en los territorios, que se brinden las condiciones para el ejercicio político**, mecanismos de seguimiento a las elecciones para que no haya participación de partidos políticos, entre otros.

Por la dificultad de esta elección de 2018, la MOE tiene pensando desplegar más observadores sobre el territorio y **activar un mecanismo de monitoreo y vigilancia que esté al tanto de las problemáticas** que se puedan presentar en las votaciones. Le puede interesar:["Curules para la paz no son un regalo para las FARC: Camilo Vargas"](https://archivo.contagioradio.com/curules-para-la-paz-no-son-un-regalo-para-las-farc-camilo-vargas/)

<iframe id="audio_18563820" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_18563820_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
