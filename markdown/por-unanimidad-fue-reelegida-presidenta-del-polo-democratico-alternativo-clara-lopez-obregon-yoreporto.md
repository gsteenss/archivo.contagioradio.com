Title: Por unanimidad fue reelegida presidenta Clara López como presidenta del Polo Democrático
Date: 2015-05-16 20:48
Author: CtgAdm
Category: Nacional, yoreporto
Tags: 1 de Mayo Bogotá, 30 de mayo, Alcadía de Bogotá, Alcaldía Mayor de Bogotá, Bogotá, candidatos a la alcaldía, Clara López, Comisión Ética, Elizabeth Cortés, Gustavo Triana, IV Congreso Nacional del Polo Democrático, Jorge Robledo, polo, Polo Democrático
Slug: por-unanimidad-fue-reelegida-presidenta-del-polo-democratico-alternativo-clara-lopez-obregon-yoreporto
Status: published

##### Foto: Polo Democrático Alternativo 

###### [16 de May 2015] 

<iframe src="http://www.ivoox.com/player_ek_4505761_2_1.html?data=lZqdl5yadY6ZmKiakpaJd6Kml5KSmaiRdo6ZmKiakpKJe6ShkZKSmaiRlNDmjNrbw9PNscrYwsmYyNrJb9PZxtHSyc7IpYzk08rgy8nJstXVjMnSzpC0s83jjKnSz9THto6ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Clara López, presidenta del Polo Democrático Alternativo] 

En el marco del segundo día del **IV Congreso Nacional del Polo Democrático Alternativo**, Clara López Obregón fue nuevamente elegida como la presidenta del partido y con lo cual se ratificó que el PDA está más unido que nunca.

“Para el Polo Democrático Alternativo es un verdadero éxito lo que se ha llevado a cabo durante estos dos días en su IV Congreso, en el cual ha salido un partido unido y fortalecido, con unas directivas, en algunos casos renovadas y en otros casos reafirmadas, pero con una declaración firme en favor del respaldo a la paz, sobre todo con una política de alianzas para ir a las elecciones locales con partidos afines alrededor de programas serios de progreso social para los municipios y las regiones, y desde luego, un espíritu que nos va a convocar para la construcción de partido de aquí a 2018, cuando esperamos obtener dentro de una gran convergencia democrática una opción firme para optar con éxito por la Presidencia de la República” dijo la dirigente política.

Por otro lado, el senador Jorge Enrique Robledo aseguró que **el Polo está más unido que nunca**: “Esta es la primera vez que todas las decisiones se toman por consenso. Los cargos directivos, la presidencia, las resoluciones políticas, la confirmación del ideario de unidad y la política de alianzas se han tomado por unanimidad, o sea que más unidad es prácticamente imposible y estoy seguro que así va a ocurrir con las decisiones de las elecciones que se van a tomar más adelante”.

En total, **el Congreso tuvo 766 delegados y delegadas, quienes fueron elegidos popularmente** y que hoy, además de ratificar la presidencia del partido, hicieron lo mismo de manera unánime con Gustavo Triana, Secretario General. También escogieron a Elizabeth Cortés como nueva veedora, quien se ha desempeñado como jueza penal de la República. De igual manera, se eligió Nuevo Comité Ejecutivo Nacional, Junta Directiva Nacional y Comisión de Ética, todos dentro de un gran espíritu unitario que ha congregado al partido alrededor de sus directivas y alrededor de su programa político.

“Lo más importante que dejó el Congreso es a un Polo unido y fortalecido, decidido a convertirse en gobierno. **Nosotros somos un partido de oposición, hemos tenido características posiciones en favor del cambio del modelo económico,** en favor de la construcción de un país con igualdad social, un país donde los empresarios, los campesinos, los trabajadores, las mujeres, los jóvenes y las nuevas ciudadanías tengan igualdad de  oportunidades y por eso, este proceso de unificación del Polo alrededor de su ideario original y a través de la proyección de una política firme de paz se constituye en un fortalecimiento enorme de nuestro partido”, aseguró López Obregón.

En materia de alianzas, el IV Congreso “dejó la determinación de tener una política de **alianzas amplia en la búsqueda de acuerdos con los sectores políticos y sociales** afines, sin vetos desde luego, para poder alrededor de programas concretos de defensa del territorio, de lo público, de la inversión social y del agua, avanzar en la defensa de una nueva manera de gobernar los territorios. Sin embargo, hay que dejar en claro que no habrá alianzas ni candidatos que estén vinculados a ningún factor armado y desde luego, que estén vinculados a la corrupción”, dijo la presidenta del Polo.

“Lo que hemos determinado es que haremos alianzas con sectores alternativos y afines que tengan la idea de acuerdo general de nuestro partido y esto los incluye a todos, no hay veto de ningún tipo”, agregó Robledo.

Según López Obregón, **la candidatura a la Alcaldía de Bogotá se va a decidir el 30 de mayo en la Coordinadora Distrital de Bogotá**: “Yo he sido ratificada en la presidencia del Polo hasta el próximo Congreso, pero tengo la impresión que el 31 de diciembre de este año voy a tener que dejar el cargo para dedicarme a la Alcaldía Mayor de Bogotá”.
