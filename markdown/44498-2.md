Title: Paramilitares imponen controles a transporte público en Chocó
Date: 2017-07-31 16:51
Category: DDHH, Nacional
Tags: Chocó, Curvradó, Mototxistas, paramilitares
Slug: 44498-2
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/06/curvarado.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [31 Jul. 2017] 

Según una denuncia de la Comisión Intereclesial de Justicia y Paz, paramilitares de las autodenominadas Autodefensas Gaitanistas de Colombia **–AGC- impusieron controles, afiliaciones, multas y sanciones a transportadores en el territorio colectivo del Curvaradó**, departamento del Chocó en medio de la fuerte presencia militar

La denuncia afirma que el 28 de junio en la comunidad de Cetino, territorio colectivo de Curvaradó, las AGC convocaron a reunión a todas las personas que se dedican al oficio de mototaxismo dentro de los sectores de Brisas, Cetino y Las Menas que son los tres sitios de entrada y salida del territorio colectivo, hacia el jiguamiandó, hacia belén de bajirá y hacia Antoquia. Le puede interesar:["Al aire" emisora comunitaria en Zona Humanitaria de Curvaradó](https://archivo.contagioradio.com/al-aire-emisora-comunitaria-en-zona-humanitaria-de-curvarado/)

“Los AGC ordenaron que antes del lunes 31 julio, **todos debían estar afiliados a la asociación de mototaxistas y cancelar una cuota de \$150.000.**  Aseguraron que después de esta fecha el pago sería de \$2.000.000. Sumado al valor de la cuota, los mototaxistas deberán entregar a los Gaitanistas \$4.000 diarios”.

Asegura la Comisión de Justicia y Paz que uno de los armados presente en la reunión dijo que el incumplimiento de esta orden implicará sanciones **"los que no se asocien, les quitamos la moto y sino la entregan, la quemamos".**

### **Unidades militares no reaccionan** 

En la denuncia aseveran que este control del territorio impactará la cotidianidad de las comunidades, sobre todo de quienes usan lo mototaxis como medio de transporte por ejemplo, para asistir a las clases de un colegio de la zona llamado AFLICOC, ubicado en la Zona Humanitaria Las Camelias. Le puede interesar: [Colegio AFLICOC conservando la cultura afro](https://archivo.contagioradio.com/colegio-aflicoc-conservando-la-cultura-afro/)

Concluye la misiva diciendo que, pese al conocimiento público de estos controles, **no existe reacción alguna en las unidades militares de Llano Rico del batallón de selva N°54. **Le puede interesar: [Control paramilitar se afianza en Bajo Atrato y Cauca](https://archivo.contagioradio.com/control-paramilitar-se-afianza-atrato-cauca/)

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU).
