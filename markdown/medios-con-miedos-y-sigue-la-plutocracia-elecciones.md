Title: Medios con miedos, y sigue la plutocracia
Date: 2018-03-12 07:48
Category: Opinion
Tags: CARACOL, Elecciones presidenciales, FARC, Petro, RCN
Slug: medios-con-miedos-y-sigue-la-plutocracia-elecciones
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/03/plutocracia1.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Eneko 

#### **Por [Camilo De Las Casas](https://archivo.contagioradio.com/camilo-de-las-casas/)**

###### 8 Mar 2018 

Guardianes de la libertad es una de las obras de Noam Chomsky en las que analiza la forma en que los medios empresariales en los Estados Unidos crean imágenes y sentimientos en la opinión o la masa para legitimar operaciones del poder político y económico en el mundo.

Los gatekeeper de las empresas comunicativas que operan en Colombia posan de “guardar  las libertades” en este escenario electoral han contribuido en la demonización y  a la minimización de apuestas políticas que expresan  cambios básicos dentro del establecimiento.

La demonización basada en la imagología del  castro chavismo creada por el Centro Democrático,  hoy asumida por Cambio Radical, Conservadores, sectores liberales, grupos de cristianos que participan en la política, ha tenido expresiones de violencia verbal, intolerancia contra el ex candidato Timoleón Jiménez del partido de las FARC y contra Gustavo Petro de Colombia Humana.

Los periodistas empresariales, que con ausencia de crítica, asumen dicha expresión han sido caja de resonancia de la misma. La simpatía de las FARC a la revolución Bolivariana, en ningún caso justifica que la estrategia uribista desarrolle estratagemas de uso de la violencia a través de sus ciegos fanáticos. El ánimo de justificación del exprocurador Ordónez, precandidato a la presidencia, a la violencia contra Timochenko se asumió como una anécdota o una información valorativa sin ningún matiz en  la construcción noticiosa.

Si bien pudieran existir dolores convertidos en sufrimiento en afectados por las acciones de las FARC cuando eran un movimiento político en armas; si bien puede haberse convertido ese sentimiento en odio y deseo de venganza, a los periodistas de los medios les corresponde la creación de contextos necesarios para un debate sano, más que una reproducción simplista de una persona que incide en un sector de los ciudadanos, justificando la violencia contra los que dejaron las armas, y que quiere desconocer que existe un mecanismo institucional para la rendición de cuentas.

La fórmula del poder establecido es conocida demonizar.  Lo demoniaco, heredado del maniqueísmo religioso y teológico, aún liberal o liberador, introyectó en el inconciente colectivo la imposibilidades de comprender las diferencias, los matices, y asumir pasionalmente que todo lo otro, lo extraño es malo, como si la realidad fuera buena y mala. En otras palabras,   envenenar el sentimiento de los ciudadanos, el alma de los nacionales, alimentar el amor ciego e imposibilitar  lo razonable para la discusión de lo público.

Es lo mismo de antes con las nuevas técnicas de la información y de la movilidad del sentimiento, En el pasado cómo aquella canción del folclor que refleja lo que ya vivimos  ¿A quién engañas abuelo?. Una demonización del otro para imposibilitar su palabra, para animar a la violencia como ocurrió en los 50. A unos los matan por godos y a otros por liberales. Y luego, ellos mismos en el exterior, los dirigentes políticos que alentaron el odio pactan, dejando en el olvido el país sepulcral a los de abajo.  Así ocurrió con la ley de los caballos muertos, con el catolicismo doctrinario que persiguió y proscribió el reformismo liberal, el socialismo y el comunismo, o como se continúa proyectando en las doctrinas del enemigo interno.

Un altísimo porcentaje de los periodistas herederos en el subconciente de esta cultura antidemocrática, y gracias al complejo de pirámide que les mueve, a los gatekeeper que les generan miedo, asumen el miedo como libertad, acríticamente reproducen que nuestros país está ad portas de iniciar una fase de revolución bolivariana, encarnada en Gustavo Petro.

Ante tal riesgo, al lado del miedo a que los excluidos se rebelen electoralmente, cubren informativamente a éste candidato  como un gran demonio y minimizan el respaldo en la plazas pública. Informativamente   preconizan sobre el demonio mostrando el riesgo que se corre de ser eventual candidato a una segunda vuelta. Ese pequeño demonio perseguiría a la prensa “libre” como Maduro;  expropiaría a los que tengan dos viviendas, como dicen que falsamente lo hizo Chávez, y como me lo expresó un taxista, ¡hasta la caja de betunes de un embolador están en riesgo!.

Tamaño sartal de exabruptos enunciados una y otra vez, entre redes, gracias a una combinación exitosa en versiones de medios empresariales,  sin que existan matices, o elementos de análisis mayores, están olvidando que la gente está hastiada, que su cotidiano está llegando hacia la desesperación ante la corrupción y la mentira.

Temen los medios a lo nuevo, es un falso miedo, desde ese temor infundado en su inconciente, operan irracionalmente. Colombia nunca será Venezuela, somos distintos, en nuestra cultura y nuestro ser- Nuestros problemas políticos son propios, los de ayer y los de hoy, no somos país petrolero, somos de otro talante, ni peor ni mejor que los venezolanos

La demonización cómo ocurrió en la reforma liberal de López Pumarejo, con el plebiscito tercamente proyectado por Santos para derrotar a Uribe, o como  con el “Panelo”, Gaitán, nos hablan del miedo de los poderosos a la democracia. Entre ellos están los oligopolios de los medios empresariales, en que con importantes excepciones de periodistas, que ven más allá de esos intereses, y con independencia de su simpatía con Petro, comprenden que algo nuevo está pasando, en medio de la cultura abstencionista en Colombia. El control mayoritario del parlamento será de la derecha, así  que si Petro fuera presidente, serán más celosos en el control al ejecutivo y obstaculizaran su ejercicio de poder, sin embargo, alimentan el miedo.

Así que lejos está hoy Petro de representar el ideario de las FARC y  el mal llamado Castro Chavismo. Simplemente, encarna el reformismo negado antes de la Constitución del 91 y después de ésta. La complejidad del conflicto agrario que no logró resolver la mesa de La Habana con todas las concesiones de las FARC EP a Santos ni tampoco lo lograra ningún acuerdo con el ELN, que seguirá en su guerra de guerrillas rural prolongada, en medio de una Colombia urbanizada, podría con el reformismo de Petro resolverse más sanamente, y eso se quiere seguir evitando . Medios con miedos que desconocen que aún la plutocracia sigue asegurada.

#### [Leer más columnas de opinión de  Camilo de las Casas](https://archivo.contagioradio.com/camilo-de-las-casas/)

------------------------------------------------------------------------

######  Las opiniones expresadas en este artículo son de exclusiva responsabilidad del autor y no necesariamente representan la opinión de Contagio Radio.
