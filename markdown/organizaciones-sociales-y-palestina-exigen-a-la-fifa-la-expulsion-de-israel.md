Title: Organizaciones sociales y Palestina exigen a la FIFA la expulsión de Israel
Date: 2015-05-28 15:29
Category: El mundo, Política
Tags: Apartheid Israel, Crímenes de guerra Israel, Expulsión Israel FIFA, Ocupación Gaza, Organizaciones internacionales contra Israel FIFA, Palestina FIFA
Slug: organizaciones-sociales-y-palestina-exigen-a-la-fifa-la-expulsion-de-israel
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/05/20150519_palestinians_footballprotest_afp.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto:NewsAsiaone.com 

###### [28May2015] 

**Palestina** y su federación de fútbol, junto con **organizaciones internacionales, y personalidades de carácter público** internacional han firmado un documento **exigiéndole a la FIFA, la expulsión de Israel** por las reiteradas **violaciones de los DDHH contra deportistas** y la selección de fútbol de Palestina.

Siguiendo el ejemplo de **1976**, cuando la FIFA decidió por **nueve votos a favor y uno en contra expulsar a Sudáfrica por el Apartheid**, la segregación racial y las reiteradas violaciones de DDHH contra la población negra.

Igual que esta iniciativa que pretendía finalizar con el brutal Apartheid en Sudáfrica, lo mismo sucede con Israel. De tal forma el **objetivo es finalizar con el Apartheid del Estado Sionista contra el pueblo Palestino.**

Muchas de las personalidades que en aquel momento sufrieron las brutales consecuencias del Apartheid en Sudáfrica como **Nelson Mandela, el arzobispo y Premio Nobel de la Paz Desmond Tutu, o el exministro Ronnie Kasrils** han calificado lo que está haciendo Israel con Palestina como más grave y una total y sistemática política de Apartheid contra el pueblo Palestino.

En el documento que demanda la expulsión de Israel de la FIFA se pide que "...**apliquen las reglas de la FIFA, que prohíben el racismo en su seno, exigen el juego limpio y no admiten que uno de sus propios miembros las pisotee** ensuciando la imagen y la reputación de la comunidad mundial del fútbol...".

Aludiendo también que la"... Convención Internacional contra el Apartheid en el deporte, adoptada por la Asamblea general de las Naciones Unidas en diciembre 1985, estipula: «**los contactos deportivos con cualquier país que practique el apartheid condona y refuerza el apartheid**»...".

Entre las violaciones cometidas por Israel contra el propio deporte Palestino están los **asesinatos de comentaristas de fútbol**, la muerte en combate de futbolistas de la Federación Palestina de fútbol, la **detención de miembros de sus equipos**, torturados y puestos en libertad sin cargo teniendo consecuencias que les ha impedido volver a ejercer.

Por otro lado debido a los numerosos **Checkpoints Israelíes, los equipos de fútbol Palestinos a penas pueden reunirse libremente** para entrenar impidiendo así el ejercicio de su propia profesión deportiva.

A la iniciativa se han sumado personalidades internacionales como **Ken Loach (Reino Unido)** Susan Abuhawa (Palestino-américaine) y Larissa Sansour (Reino Unido) o Nelson Mandela. En total son más de **200 organizaciones internacionales quienes han firmado el documento exigiendo la salida de Israel** hasta que finalice su política de Apartheid contra el pueblo Palestino.
