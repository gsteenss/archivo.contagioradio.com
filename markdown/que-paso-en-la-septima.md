Title: ¿Qué pasó en la séptima?
Date: 2016-04-25 08:27
Category: Opinion, Tatianna
Tags: Bogotá, carrera septima, Peñalosa
Slug: que-paso-en-la-septima
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/04/carrera-septima.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### foto: Archivo - Daniel Jiménez 

#### **Por [Tatianna Riveros](https://archivo.contagioradio.com/opinion/tatianna-riveros/)- [@[tatiannarive]

###### 25 Abr 2016 

Estuve en el centro de Bogotá, y la sorpresa fue enorme. Calles sin separadores para cicloruta, huecos en donde antes habían materas que adornaban la peatonal, ausencia de bicicletas desde museo del oro hasta la Calle 26, policías que desalojaban vendedores en la calle.

[Pasó una moto con dos policías; “robaron a alguien” fue lo primero que dije, seguí caminando por la séptima hacia el norte: hice una parada en el edificio Avianca “el que se incendió le dije a mi acompañante”, mientras lo señalaba; continuamos y nos encontramos con personas que a punta de carbón y tizas de colores pintaban personajes en el asfalto; nos detuvimos a mirar a copete y a sus compañeros, los cuyes que salen corriendo y se meten en alguna vasija y te hacen ganar seiscientos pesos. Empecé a extrañar algo. Me había bajado en la casa de Nariño y había caminado por toda la peatonal pero no estaba demarcada la cicloruta, y pensé ¿qué pasó en la séptima?]

[Pues bien, hace algunos meses salió una noticia en la que en la recuperada de Bogotá decidieron quitar las materas que estaban en la séptima, aduciendo que eran utilizadas como escondederos de droga y para que los señores indigentes hicieran sus necesidades. Así fue, levantaron una a una dichas materas pero la calle quedó marcada: los huecos siguen ahí, intactos. Me extrañó un poco no ver esas bicicletas amarillas y blancas, ni la carpa en Museo del oro en donde las alquilan, seguro los fines de semana no funcionan; hasta que noté que tampoco estaban los separadores que demarcaban la cicloruta (haciendo la claridad de que una cosa es la ciclorruta y otra la ciclovía).]

[Debo confesarles que se me aguaron los ojos cuando comprobé que no era ningún robo: Policías desalojaban vendedores, uno a uno recogían su mercancía y la guardaban mientras aquellos uniformados acosaban para agilizar su levantamiento. “No es justo” “no le hacen daño a nadie”, en efecto, soy de las que prefieren que la gente se dedique a la vida informal y no a alimentar odios y a hacerle daño a otros. Me acordé que la semana pasada, alguien muy cercano me había dicho que unos Policías habían llegado a desalojar a un cuentero y la gente los chifló, también me dijo que había como un tipo de “encuesta” que preguntaba sobre el cambio de la séptima peatonal y a hoy sigo pensando ¡eso no puede pasar!]

[No se puede perder un espacio, un corredor peatonal en donde el comercio es fluido, encuentras comida mexicana, cuadros, estatuas humanas, música, restaurantes y tiendas de ropa, sólo por darle vida a los vehículos. Mucho se ha dicho de que los índices de inseguridad e indigencia subieron con la peatonalización de la séptima, increíblemente no ví ni indigentes ni delincuencia, he visto más robos en estaciones de transmilenio que en el mismo Centro de la ciudad.]

[La invitación entonces es a que cuidemos lo que es nuestro, Bogotá no necesita ser recuperada porque siempre ha estado en nuestras manos.]

[Nota: esta no es una columna investigativa, es el resultado de mi tiempo de ocio y va más allá de Petro o de Peñalosa, no es una denuncia ni pretendo que alguien haga algo, constituye mi opinión, Ojalá usted que me lee si sepa qué pasó con la séptima.]

------------------------------------------------------------------------

###### Las opiniones expresadas en este artículo son de exclusiva responsabilidad del autor y no necesariamente representan la opinión de la Contagio Radio. 
