Title: Con Mestizo Órtiz ya son 36 los líderes sociales asesinados en Cauca
Date: 2019-10-03 14:30
Author: CtgAdm
Category: Líderes sociales, Nacional
Tags: aumenta vilencia, Cauca, fallas del Gobierno, homicidio, Líderes indígenas
Slug: 36-los-lideres-sociales-asesinados-en-cauca
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/03/Captura-de-pantalla-2019-03-20-a-las-12.51.53-p.m..png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:@AndresEliasGil] 

 

La Red de Derechos Humanos del Suroccidente Colombiano Francisco Isaías Cifuentes, denuncia el asesinato del indígena y defensor de DD.HH, Mestizo Ortiz, quien fue atacado el lunes 1 octubre por hombres armados en el Resguardo Indígena de San Francisco, vereda la Despensa. Por estos hechos, la organización hace responsable al Estado y al  Departamento de Policía de Cauca, puesto que se encontraban muy cerca del lugar de los hechos y no atendieron el llamado de la comunidad.

### Los hechos...

Según habitantes del Municipio de Toribio, Ortiz  se encontraba en el resguardo Indígena de San Francisco vereda la Despensa, cuando cerca de las 10:00 de la noche en la caseta de las ferias de la vereda, fue abordado por dos personas que se movilizaban en una motocicleta, quienes dispararon contra él, causándole la muerte de manera instantánea. (Le puede interesar:[Tres líderes sociales han sido asesinados en Cauca esta semana](https://archivo.contagioradio.com/tres-lideres-sociales-han-sido-asesinados-en-cauca-esta-semana/))

Ortiz era comunero indígena, defensor de Derechos Humanos y fundador e integrante activo de la Asociación Indígena “Avelino Ul”; hasta el momento se desconoce la razón de su homicidio y se descarta la posibilidad según los integrantes de la Red que tuviera amenazas en su contra. (Le puede interesar: [Comunidades indígenas denuncian incremento de la violencia en Tierradentro, Cauca](https://archivo.contagioradio.com/cabildos-asesinatos-tierradentro-cauca/))

### ¿Qué ocurre en el Cauca?

Según el Ministro de Ministro de Defensa Guillermo Botero, el  departamento del Cauca, cuenta con al rededor de 2.000 soldados, 450 de ellos integrados el 20 de agosto del 2019, luego del Consejo de Seguridad en Popayán; a pesar de esto la taza de asesinatos a lideres en este departamento sigue la mas altas del país, seguido por Antioquia y Nariño. lo que demostraría la ineficacia de la presencia de fuerzas de seguridad del Estado para controlar los asesinatos. (Le puede interesar: [Comunidades del Cauca no van a dejar que el terror se apodere de sus territorios](https://archivo.contagioradio.com/comunidades-cauca-no-terror/))

Reflejo de esto denuncia la Red exigió nuevamente al gobierno mayor atención a los líderes y liderases que defienden esta región; así mismo hicieron un llamado a los organismos de derechos humanos  a mantenerse atentos ante la crítica situación que afrontando el Departamento del Cauca, donde en el último año se han registrado un promedio de 36 líderes sociales asesinados, según un informe del Instituto de estudios para el desarrollo y la paz (Indepaz).

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
