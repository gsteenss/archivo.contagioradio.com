Title: Campesinos de Suárez, Cauca reclaman predios a la multinacional Smurfit Kappa
Date: 2020-07-22 22:40
Author: PracticasCR
Category: Actualidad, Nacional
Tags: campesinos, campesinos Cauca, Cauca, despojo de tierras
Slug: campesinos-de-suarez-cauca-reclaman-predios-a-la-multinacional-smurfit-kappa
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/07/Comunicado-campesinos-Suarez-Cauca.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/07/Campesinos-Suárez-Cauca.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

Comunidades campesinas del municipio de Suárez, Cauca **realizan hace 3 días una movilización por su derecho a la tierra en predios utilizados por la multinacional irlandesa Smurfit Kappa Cartón**.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En un comunicado emitido por los campesinos, señalan que desde el pasado 19 de julio  se encuentran ocupando la finca «La Carolina» propiedad de la citada multinacional en desarrollo del proceso de recuperación de predios. (Le puede interesar: [Denuncian que empresa bananera desarrolla campaña en contra de la Restitución de tierras](https://archivo.contagioradio.com/denuncian-que-empresa-bananera-desarrolla-campana-en-contra-de-la-restitucion-de-tierras/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por otra parte, denuncian el **«*olvido del Estado colombiano con la población campesina \[…\] por centrar sus acciones en la concentración de la tierra en poca gente y el desplazamiento forzado*».**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

También aluden en su comunicado sobre la necesidad de garantías alimentarias para las  comunidades, las cuales se han visto seriamente afectadas con la pandemia; la conservación de la cultura campesina y el «*derecho a la tierra*».

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Suárez, un municipio históricamente afectado por la violencia y el despojo de tierras

<!-- /wp:heading -->

<!-- wp:paragraph -->

Suarez es un municipio de composición afro, campesina e indígena, que ha tenido que padecer la violencia en los últimos años. Cada comunidad mantiene una lucha propia para defender la vida, la identidad y el territorio en una zona, donde al igual que en muchas otras regiones del país, ser líder o lideresa social es una labor perseguida y constreñida por los violentos, teniendo registro incluso, **de asesinatos como los de los líderes Dilio Corpus Güetio y Gilberto Valencia.** (Lea también: [971 defensores de DD.HH. y líderes han sido asesinados desde la firma del Acuerdo de paz](https://archivo.contagioradio.com/971-defensores-de-dd-hh-y-lideres-han-sido-asesinados-desde-la-firma-del-acuerdo-de-paz/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por otro lado, el Proceso de Comunidades Negras -[PCN](https://renacientes.net/blog/2010/05/29/continua-la-politica-de-despojo-de-la-tierra-y-el-territorio-en-colombia/)- también denunció en su momento **el destierro y el despojo** del que estaban siendo víctimas los habitantes del corregimiento La Toma en Suárez, Cauca; ante la llegada de empresas multinacionales como **Anglogold Ashanti, la Unión Fenosa, entre otras, a las que, según el PCN, les fueron concedidas más  de 10 mil hectáreas para explotación económica, esto es, «*más de la tercera parte del municipio*».**

<!-- /wp:paragraph -->

<!-- wp:image {"id":87118,"sizeSlug":"large"} -->

<figure class="wp-block-image size-large">
![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/07/Comunicado-campesinos-Suarez-Cauca.jpg){.wp-image-87118}  

<figcaption>
Comunicado oficial - Comunidad Campesina Suárez, Cauca

</figcaption>
</figure>
<!-- /wp:image -->

<!-- wp:block {"ref":78955} /-->
