Title: Tampoco hay docentes suficientes en la Universidad Nacional, sede Palmira, Valle
Date: 2015-02-26 23:34
Author: CtgAdm
Category: DDHH, Nacional
Tags: Acuerdo 2034, Déficit de la universidad pública, Derecho a la educación en Colombia, Mesa Amplia Nacional Estudiantil, Universidad Nacional Sede Palmira
Slug: tampoco-hay-docentes-suficientes-en-la-universidad-nacional-sede-palmira-valle
Status: published

###### Foto: prensauniversidad.blogspot 

<iframe src="http://www.ivoox.com/player_ek_4140276_2_1.html?data=lZahkpebeo6ZmKiakpyJd6KompKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRic%2Fo08rjy9jYpYzVjLXO0dHFb6jjz9%2BSpZiJhZLgxt%2BSlKiPma%2B1rZC9w9HRrdPVjoqkpZKns8%2FowszW0ZC2pcXd0JCah5yncZU%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Paola González, Representante estudiantil] 

En la sede de Palmira de la Universidad Nacional **tampoco hay docentes para iniciar el semestre de clases en varias carreras**, que según el calendario tendría que haber iniciado hace 4 semanas. Paola González, representante estudiantil de la sede, asegura que **no hay dinero para contratación de la planta docente**, ni para los subsidios a los que tienen derecho los estudiantes y mucho menos hay presupuesto para programas culturales y deportivos.

En cuanto al aspecto académico muchos de los estudiantes y las estudiantes **no pudieron inscribir las materias necesarias para adelantar su semestre por fallas en la plataforma** de inscripción, además muchos estudiantes decidieron inscribir las mismas materias, ello sumado a la falta de docentes produce un nivel de hacinamiento y de baja de la calidad en los cursos académicos.

En cuanto al aspecto administrativo, González señala que la sede Palmira de la Universidad Nacional tiene un **déficit presupuestal del 160  millones de pesos en la facultad de ingeniería**, por lo cual se estaría pensando en cerrar algunos programas para el primer semestre de 2016 puesto que no hay recursos suficientes para la contratación del personal necesario.

Por estas razones, los **más de 3200 estudiantes de esa sede**, decidieron entrar en un proceso de movilización que inicia con el desarrollo de asambleas estudiantiles y un censo de las dificultades de los estudiantes a través de recibimiento de casos individuales que son comunes. Adicionalmente, Paola señala que **se oponen a la candidatura de Ignacio Mantilla para ser reelegido como rector puesto que su administración ha sido “nefasta”.**
