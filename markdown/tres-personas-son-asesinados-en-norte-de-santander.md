Title: Tres personas son asesinados en Norte de Santander
Date: 2020-08-25 19:28
Author: CtgAdm
Category: Actualidad, DDHH
Slug: tres-personas-son-asesinados-en-norte-de-santander
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/02/Asesinato-lider-indigena.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

Este **25 de agosto sobre las 7:00 pm se conoció sobre una nueva masacre en el Norte de Santander, esta vez en la región del Catatumbo**, donde según fuentes del territorio fueron asesinados tres jóvenes.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El hecho se registró exactamente en zona rural del municipio de Ábrego, a varios kilómetros del Catatumbo, en cercanías al corregimiento de Capitán Largo, en donde personas armadas asesinaron con arma de fuego a tres personas.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/marthaperaltae/status/1298412112591622147","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/marthaperaltae/status/1298412112591622147

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

**Junior Maldonado, integrante de la Asociación Campesina de Catatumbo** ([Ascamcat](https://archivo.contagioradio.com/operaciones-militares-alteran-sus-resultados-en-la-lucha-del-narcotrafico/)), señaló, *"estamos ante un desangre en las regiones del país y en una desidia institucional"*.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Al mismo tiempo, **Martha Epieyú, presidenta del Movimiento Mais**, señaló que las autoridades tratan de llegar al lugar de los hechos, y añadió que, ***"solo un país movilizado, decidido y empoderado detendrá los ríos de sangre".***

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Este acto de violencia se da luego de que la **la Defensoría del Pueblo emitiera una alerta temprana**, sobre el riesgo que corrían las comunidades del Norte de Santander.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Hasta el momento **se desconoce la identidad de las víctimas, así como los responsables de este hecho,** sin embargo es una situación que agudiza aún más la situación de violencia que viene atravesando este departamento, especialmente regiones como el Catatumbo.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

*Información en desarrollo.*..

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Le puede interesar: [Otra Mirada: El Catatumbo entre fuego cruzado](https://archivo.contagioradio.com/otra-mirada-el-catatumbo-entre-fuego-cruzado/)

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
