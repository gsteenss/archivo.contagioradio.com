Title: Informe de Fiscalía sobre asesinatos a defensores de DD.HH es otro "falso positivo"
Date: 2017-07-12 13:35
Category: DDHH, Nacional
Tags: Fiscalía General de la Nación, marcha patriotica
Slug: informe-de-fiscalia-sobre-asesinatos-a-defensores-de-dd-hh-es-otro-falso-positivo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/02/Asesinan-a-Luz-Herminia-Olarte-lideresa-social-de-Yarumal.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [12 Jul 2017] 

El vocero de la plataforma Social Marcha Patriótica, David Flores, expresó que el informe expuesto por la Fiscalía sobre los resultados en las investigaciones de homicidios de defensores de derechos humanos, son una **“cortina de humo” que no responde frente a quienes son los autores intelectuales de estos asesinatos y cuales son los móviles, así como las estructuras tras esos crímenes.**

“Nos parece que este informe de la Fiscalía es más un intento de salirle al paso a todas las acusaciones por corrupción y por ineficacia de este Fiscal, **y lo que le está presentando realmente al país es un falso positivo judicial**” afirmó Flores. (Le puede interesar: ["Asesinan a Eugenio Rentería, integrante del comité cívico por la salvación del Chocó"](https://archivo.contagioradio.com/asesinan_integrante_comite_paro_choco/))

De acuerdo con la documentación que ha hech**o Marcha Patriótica se registraron 116 asesinatos durante el año 2016 y 65 en lo que va corrido de este año**, razón por la cual las investigaciones señaladas por la Fiscalía no responden al total de homicidios y no tendría concordancia con las cifras expuestas por la Defensoría del Pueblo, la Procuraduría General de la Nación y diferentes ONGS en el país que reconocen que son más de 87 los asesinatos a defensores de derechos humanos desde el 2016.

### **¿Quiénes son los autores intelectuales de los asesinatos a defensores de DD.HH?** 

David Flores manifestó que otro de los hechos que preocupa del informe expuesto por la Fiscalía, es que hace hincapié en los autores materiales de los asesinatos, pero **no muestra indicios en quienes estarían detrás de ellos y se beneficiarían económicamente y políticamente, con estos homicidios**.

“Mientras no se logre dar con los autores intelectuales, quienes orquestan y financian este tipo de atentados pues lastimosamente todo va a ser un sofisma de distracción” aseguró Flores y agregó que “las investigaciones y todo el accionar del Estado deberían ir dirigidos a **desmantelar esas organizaciones que tienen claros nexos entre política, economía, paramilitarismo y narcotráfico**”. (Le puede interesar: ["Asesinan a profesor Washington Cerdeño en Córdoba"](https://archivo.contagioradio.com/asesinan-a-profesor-en-cordoba/))

De igual forma, el vocero de Marcha Patriótica expresó que, pese a que hay capturas y sindicados, la **Fiscalía persiste en la hipótesis de que los asesinatos tienen como móvil riñas callejeras, dificultades de dinero o crímenes pasionales**, sin reconocer que el motivo principal sea su labor y trabajo como líderes sociales o defensores de derechos humanos.

### **Son 181 líderes asesinados en Colombia desde 2016** 

Referente a por qué Marcha Patriótica hace un registro de 181 líderes y defensores de derechos humanos asesinados, a diferencia de la ONU que tiene un reporte de 87, David Flores argumentó que es debido a que la ONU parte de una categoría distinta de líder social, que se refiere a defensores de derechos humanos formalmente reconocidos.

En los registros de Marcha patriótica, por el contrario, **se encuentran los líderes sociales, en donde también entran liderazgos comunitarios, afros, indígenas, líderes de juntas de acción comunal**, que de acuerdo con Flores “cuentan como tejido vital para el país”. (Le puede interesar: ["Hay un disfraz del desmonte del paramilitarismo: Mesa Nacional de Víctimas"](https://archivo.contagioradio.com/victimas-de-la-mesa-nacional-reclaman-proteccion-ante-amenazas-contra-su-vida/))

De los 181 asesinatos de líderes, 135 pertenecían a Marcha Patriótica y según David Flores, solo se han compulsado dos sentencias a sicarios, **dejando en la total impunidad al 90% de los homicidios**.

### **Los resultados de la Fiscalía sobre los asesinatos contra defensores de Derechos Humanos.** 

-   Crímenes reportados contra defensores de derechos humanos según la Fiscalia y ONU: 87
-   De los 87 crímenes reportados por la ONU, se tiene un avance en 45 investigaciones, es decir 51. 72%
-   Por estos casos hay 101 personas vinculadas y 71 privadas de la libertad, señaladas como posibles autores materiales de los asesinatos.
-   En cinco procesos ya hay sentencias y 13 están en etapa de juicio.
-   Los 87 homicidios a defensores confirmados por la ONU han ocurrido en 22 departamentos. Cauca es el departamento más afectado con 16 asesinatos, seguido por Antioquia con 11, Norte de Santander con 7, Valle con 7 y Nariño con 6.

<iframe id="audio_19768747" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_19768747_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
