Title: Vientos que se llevan fusiles y traen esperanza
Date: 2017-07-02 09:02
Category: Opinion
Tags: entrega de armas, FARC
Slug: vientos-se-llevan-fusiles-traen-esperanza
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/06/las-preocupaciones-de-las-farc-luego-de-la-entrega-de-armas.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

#### **Por Adriana Benjumea/[[@**AdrianaBenjume1**](https://twitter.com/AdrianaBenjume1)

###### 2 Jul 2017 

El 27 de junio de 2017 se registrará en la historia de Colombia como un día histórico, las Fuerzas Armadas Revolucionarias de Colombia – FARC- entregaron más de 7.000 armas, cumpliendo así con lo comprometido en el Acuerdo para la terminación del conflicto y la construcción de una paz estable y duradera firmado por esta guerrilla y el gobierno de Colombia el 24 de noviembre de 2016.

[Las armas, el fusil, el amor en la montaña, ponerse las botas, hicieron parte del lenguaje de la historia de Colombia y mentiría sino reconozco que la revolución fue una opción en un país empobrecido, en poblaciones perseguidas por pensar distinto, aquellas a quienes se robó la tierra, un campesinado expropiado. Factores como estos motivaron a compañeros del barrio, de estudio y de vida a la búsqueda de la justicia a través de la lucha armada, mientras otros y otras, quizás menos aguerridas nos quedamos escuchando las canciones de Ana y Jaime y soñando con una trasformación justa, posible:]*[“Yo pregunto a los presentes, si  no se han puesto a pensar, Que esta tierra es de nosotros y no del que tenga más”.]*

[En la década de los 70 Ana y Jaime emocionaban a hombres y mujeres con sus letras,]*[Ricardo Semillas]*[ y]*[Este Viento Amor]*[ ganaban premios internacionales, mientras en las universidades públicas se repetía el estribillo: “]*[Tu fusil amor es la música más libre bajo el sol, es sangre y es futuro del amor, tu fusil amor…”.]*

[Hago parte de la generación que nació en esa década y que tuvo el privilegio, que solo tuvimos pocos hombres y poquísimas mujeres, de acceder a la universidad pública, donde esas canciones acompañaron las fiestas con fogata y caja de vino barato, mientras soñábamos con “]*[desalambrar”]*[ y repetíamos una y otra vez, con la voz un poco afectada por el trago]*[“…porque si ya se unieron el fusil y el evangelio en las manos de Camilo…”]*[.  ]

[Todo parecía la ilusión de un puñado de hombres y mujeres pobres, jóvenes, que llegamos a la universidad pública como única posibilidad de ascender en la escala social, porque como decían las mamás y papás llegados del campo por violencia o por pobreza “mija, estudie para salir adelante, porque es la única herencia que le podemos dejar”.  ]

[Pero la Universidad se turbó, la guerra de los campos llegó a las ciudades y cobró muchas vidas dentro y fuera del claustro, vimos morir sin tregua, profesores, estudiantes, defensores y defensoras de derechos humanos. El paramilitarismo avanzó a pasos agigantados y ahora las canciones de Ana y Jaime, de Víctor Heredia, Silvio, Pablo y Violeta Parra que manifestaba sin vergüenza]*[“Que vivan los estudiantes, jardín de las alegrías. Son aves que no se asustan de animal ni policía”]*[terminaron siendo subversivas.]

[Hoy, cuatro décadas después, mi generación presencia un momento histórico. La emoción de lo vivido en Mesetas, Meta es inocultable, la guerra perdió la última batalla, serán las palabras y los argumentos políticos lo que nos confronte. Es irrefutable que menos armas circulando, son más vidas creando y soñando.]

[Soplan vientos de democracia, tendremos derecho a emprender nuevas luchas sin armas, a soñar, a crear, tendremos derecho a componer nuevas canciones]*[“Este viento amor que conmigo en la montaña está…silbando siempre esta nueva canción”.]*[  ]

------------------------------------------------------------------------

###### Las opiniones expresadas en este artículo son de exclusiva responsabilidad del autor y no necesariamente representan la opinión de Contagio Radio.
