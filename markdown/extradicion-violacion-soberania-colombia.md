Title: La extradición una violación a la soberanía de Colombia
Date: 2018-11-06 17:55
Author: AdminContagio
Category: Expreso Libertad
Tags: extradicion, presos politicos
Slug: extradicion-violacion-soberania-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/11/extradicion-770x400.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Zona Cero 

###### 6 Nov 2018

Colombia ha firmado convenios con otros países para que se realicen solicitudes de extradición. Algunos de los más significativos son los que se han establecido con Estados Unidos, sin embargo, las organizaciones defensoras de derechos humanos han denunciado que son desiguales y violan derechos fundamentales como la presunción de inocencia y la cadena procesal.

En este programa del Expreso Libertad, conozca cuáles son los derechos más vulnerados a las personas que han sido extraditadas y su relación con los prisioneros políticos.

<iframe id="audio_30612783" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_30612783_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Encuentre más información sobre Prisioneros políticos en[ Expreso libertad ](https://archivo.contagioradio.com/programas/expreso-libertad/)y escúchelo los martes a partir de las 11 a.m. en [Contagio Radio](https://archivo.contagioradio.com/alaire.html) 
