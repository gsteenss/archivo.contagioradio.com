Title: No hay tiempo para tristezas, vamos cantante comienza
Date: 2015-11-13 12:18
Category: En clave de son
Tags: El cantante Hector Lavoe, Fania All Stars, Hector Lavoe, Podcast de salsa, Ruben Blades, Willy Colón
Slug: no-hay-tiempo-para-tistezas-vamos-cantante-comienza
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/11/hector_lavoe-e1447432701968.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### [13 nov 2015] 

La canción dió nombre al artista y el le devolvió el favor haciendola inmortal, "El cantante" es un himno de la salsa y el "leti motiv" de Juan Pérez Martínez, el mismo que desapareció para dar paso a Hector Lavoe, el cantante de los cantantes, "la voz",  el rey de la puntualidad.

Escrita por Rubén Blades, producida impecablemente por Willie Colón e interpretada con la pasión y autenticidad que solo Hector Lavoe podría imprimirle a la música, a los versos y coros que hoy miles de personas conocen de memoria en todo el mundo, "Oh le lo la laaa, a la laa laaa"

Hector Juan Pérez Martínez, fue un talentoso cantante puertoriqueño, que desde muy pequeño conoció la desgracia. A causa de la muerte de su madre, emprendería un viaje a los Estados Unidos, siguiendo los pasos de su hermano, para tener mas oportunidades y poder hacer uso al máximo de su voz.

[![Hector Lavoe](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/11/hector-lavoe-el-dia-de-mi-suerte.jpg){.aligncenter .size-full .wp-image-17165 width="900" height="300"}](https://archivo.contagioradio.com/no-hay-tiempo-para-tistezas-vamos-cantante-comienza/hector-lavoe-el-dia-de-mi-suerte/)

Una vez entablecido en Norteamérica, conocería a quién seria su compañero, no solo musicalmente hablando, sino también de vida, otra de esas jovenes promesas de la salsa surgidas en una época única e irrepetible: Willie Colón. Gracias a la voz de Lavoe y la buena produccion de Colón conocerían el éxito a muy temprana edad.

Si bien Hector grabó con la orquesta New Yorker en el 65, su discografia oficial empezaría en el 67 con el álbum “El malo” junto a la orquesta de Willie Colón y se extendería a mas de 8 álbumes juntos, la genial dupla se separaría en el 75 a causa de la vida tan desordenada que llevaba “El Cantante de los Cantantes” lo que lo impulsaría a empezar su carrera como solista.

Su carrera como solista, no tendría punto bajo como si lo tendría su vida, por las diferentes pruebas que le colocaba la vida, sin embargo fue una de las voces mas vendidas para el sello Fania, ya que daría a sus numeros cerca de 9 álbumes, fuera de los conciertos que grabo en vivo junto a La Fania All Stars.

Lastimosamente el acenso en su carrera, iría en paralelo al de sus desgracias, ya que en fechas muy cercanas, moriría su suegra, su padre, el incendio de su apartamento y el mas debastador de los golpes la muerte de su hijo, golpe del cual según sus allegados jamás se recuperaría y lo llevaría hasta un fallído intento de suicidio.

Dichas tragedias, le darían el coraje para que sus interpretaciones fuerán cada vez mas autenticas y de corazón, por ello se entregaba en cuerpo y alma a su gente “Lo más grande de este mundo” como lo cantaba en su canción “Mi Gente” por eso el público lo aclamaba y cumplía la cita donde fuera la presentación de Lavoe. Por esta razón, productores abusarón de el con mucho trabajo, poca paga y droga, lo que iría opcando su voz y por supuesto su vida.

Este podcast tiene todos los detalles, que en esta entrada no se colocarón y por supuesto lo mejor de su discografia, un especial en dos partes a una de las figuras mas importantes de la salsa hast anuestros dias, recuerden “Es chevere ser grande, pero es más grande ser chevere”

<iframe src="http://www.ivoox.com/player_ek_9378415_2_1.html?data=mpikmpmVeY6ZmKiakpeJd6KllYqgo5eWcYarpJKfj4qbh46kjoqkpZKUcYarpJKy1dXJp8rVzZC1x8jYs9Ofrcbj0cqPtNPdzsrfw5DUpdPoxpKSmaiRh9Di1cbUy9SPlsLYytSYj4qbh46o&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Especial Hector Lavoe, Primera Parte] 

<iframe src="http://www.ivoox.com/player_ek_9378496_2_1.html?data=mpikmpmdeo6ZmKiakpeJd6Kkl4qgo5iVcYarpJKfj4qbh46kjoqkpZKUcYarpJKy1dXJp8rVzZC1x8jYs9Ofrcbj0cqPt8bb1tPRw5DUpdPoxpCah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Especial Hector Lavoe, Segunda Parte] 
