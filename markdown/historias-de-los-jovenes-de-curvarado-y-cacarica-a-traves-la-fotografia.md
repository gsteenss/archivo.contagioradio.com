Title: Historias de los jóvenes de Curvaradó y Cacarica a través de la fotografía
Date: 2015-07-02 12:36
Category: Comunidad, Fotografia, Otra Mirada
Tags: cacarica, Camelias, comunidades, Curvarado, Fotografia, Nueva Esperanza en Dios, Revelados
Slug: historias-de-los-jovenes-de-curvarado-y-cacarica-a-traves-la-fotografia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/07/yeferson-1-1.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/07/yeferson-2.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/07/Rosalba-1-1.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/07/Rosalba-2.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/07/Yimi-1-1.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: *Revelados* 

###### 2 Jul 2015 

Con el ánimo de potenciar el diálogo y la comunicación entre quienes hacen parte de las propuestas educativas de las zonas humanitarias Las Camelias (Colegio de la AFLICOC) y Nueva Esperanza en Dios (Colegio integral CAVIDA) ubicadas en las cuencas de los ríos **Curvaradó y Cacaricá**, respectivamente; se realizó un ejercicio de envió reciproco de cartas fotográficas entre ambas comunidades.

El ejercicio inició en la zona humanitaria Las Camelias donde los estudiantes, organizados por parejas, eligieron un tema, un lugar o una situación de su comunidad para compartirla con sus compañeros de la zona humanitaria Nueva Esperanza en Dios, a través de una fotografía y un relato sobre la misma. Este ejercicio se desarrolló a través de una técnica conocida como Fotografía Estenopeica, la cual consistente en la obtención de fotografías con cámaras básicas, elaboradas con cajas de cartón o latas vacías, selladas en su totalidad para no permitir el paso de la luz, salvo por un pequeño agujero del ancho de una aguja (estenopo) que hace las veces de lente y proyecta la imagen sobre un rollo o papel fotográfico, ubicado en la cara opuesta del orificio.

Una vez cada pareja construyó el relato y tomó la fotografía, las imágenes fueron reveladas en un cuarto oscuro, provisto de una luz roja, mediante un proceso químico que permite visualizar y multicopiar las fotografías. Finalmente, los relatos realizados en la zona humanitaria *Las Camelias,* fueron compartidos y leídos por las/os estudiantes de *Nueva Esperanza en Dios,* quienes mediante el mismo proceso respondieron a las cartas.

Dentro de los relatos se destacan las alusiones a la importancia del proceso de las zonas humanitarias para la vida de cada una/o de las/os jóvenes y adultas/os.

El ejercicio fue realizado por el *Club de Medios Audiovisuales y Muralismo Revelados* que centra su trabajo en  realización de intervenciones artísticas en distintos territorios, así como en la formación de públicos alrededor de artes visuales y audiovisuales como el muralismo, la serigrafía, el stencil, la fotografía (digital y estenopeica) y la producción audiovisual.

\

<figure>
[![yeferson-1](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/07/yeferson-1-1.jpg)](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/07/yeferson-1-1.jpg)

</figure>
<figure>
[![yeferson](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/07/yeferson-2.jpg)](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/07/yeferson-2.jpg)

</figure>
<figure>
[![Rosalba-1](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/07/Rosalba-1-1.jpg)](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/07/Rosalba-1-1.jpg)

</figure>
<figure>
[![Rosalba](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/07/Rosalba-2.jpg)](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/07/Rosalba-2.jpg)

</figure>
<figure>
[![Yimi-1](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/07/Yimi-1-1.jpg)](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/07/Yimi-1-1.jpg)

</figure>

