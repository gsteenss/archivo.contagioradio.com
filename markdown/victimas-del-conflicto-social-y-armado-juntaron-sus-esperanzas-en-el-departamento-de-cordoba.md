Title: Víctimas del conflicto social y armado juntaron sus esperanzas en el departamento de Córdoba
Date: 2016-04-13 08:14
Category: yoreporto
Tags: conflicto armado en Colombia, proceso de paz, víctimas
Slug: victimas-del-conflicto-social-y-armado-juntaron-sus-esperanzas-en-el-departamento-de-cordoba
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/04/Fundación-CordoberXia-1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

#### [**Fundación CordoberXia - Yo Reporto **] 

###### 11 Abr 2016 

El sábado 9 de abril 2016 convocados por la Fundación CordoberXia, desde las cinco de la tarde la Ronda del Sinú (Montería Córdoba) fue inundada por las voluntades de un pueblo urgido de paz.

[Allí Llegaron hombres y mujeres de diferentes barrios y comunidades de la ciudad de Montería  y municipios aledaños para conmemorar desde la alegría.]

[Cerca de 300 delegados y delegadas de organizaciones  de víctimas del departamento unieron sus voces para decir "No más guerra, sí a la paz con justicia social" ]

\[caption id="attachment\_22615" align="aligncenter" width="800"\][![Fundación CordoberXia](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/04/Fundación-CordoberXia.jpg){.wp-image-22615 .size-full width="800" height="533"}](https://archivo.contagioradio.com/victimas-del-conflicto-social-y-armado-juntaron-sus-esperanzas-en-el-departamento-de-cordoba/fundacion-cordoberxia/) Carlos Lugo - Fundación Cordoberxia\[/caption\]

[La Gran Caseta Popular por La Paz en conmemoración a las víctimas cordobesas, como le llamó la Fundación Cordoberxia, organización social integrante del Movimiento Político y Social Marcha Patriótica, se convirtió en un escenario de paz  y convivencia, donde la alegría y la esperanza fueron la bandera ondeante. Cantantes como Ely Osorio  y Carlos Lugo, entre otras agrupaciones, pusieron a bailar y a cantar a la gente.]

[En el desarrollo del evento se transmitieron saludos y mensajes de reconciliación, en especial la delegación de paz de las FARC-EP mediante un video comunico a las víctimas del departamento de Córdoba que]*[“Desde ya, la población cordobesa debe prepararse para participar de manera activa en las audiencias públicas que se efectúen con respecto a la Comisión del Esclarecimiento”]*[ y que]*[“Es fundamental suministrar la información para lograr dar con el paradero de estas personas o de sus restos y ofrecer un alivio a sus familias que les han buscado y esperados durante años”]*[. Entre otros saludos senador Iván Cepeda, el representante a la cámara Alirio Uribe y el vocero internacional de Marcha Patriótica, David Florez.]

[Con globos blancos, que se dejaron volar tan alto como nuestros sueños, las víctimas simbolizaron la necesidad de caminar hacia la concreción de una Paz estable y duradera con reales garantías de verdad, justicia, reparación y no repetición, en apoyo a la diálogos entre el Gobierno Nacional y las FARC-EP y desde luego el reciente con el ELN.  ]

[[Saludo de las FARC-EP a la Caseta por la Paz en Monteria Cordoba]](https://www.youtube.com/watch?v=l6LZkpbNJhw&list=PL4BUlQBKuHemBgA5JEUAimjdyGknZDO04&index=4)

------------------------------------------------------------------------

###### Las opiniones expresadas en este artículo son de exclusiva responsabilidad del autor y no necesariamente representan la opinión de la Contagio Radio. 
