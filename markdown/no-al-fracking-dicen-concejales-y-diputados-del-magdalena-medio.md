Title: NO al fracking dicen Concejales y Diputados del Magdalena Medio
Date: 2020-08-19 11:27
Author: PracticasCR
Category: Actualidad, Nacional
Tags: Alianza Colombia Libre de Fracking, fracking, No al Fracking
Slug: no-al-fracking-dicen-concejales-y-diputados-del-magdalena-medio
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/08/Proposicion-Aditiva-Fracking.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

Este martes a través de una rueda de prensa convocada por la [Alianza Colombia Libre de Fracking](https://colombialibredefracking.wordpress.com/), numerosos **concejales y diputados de diversos municipios y departamentos del Magdalena Medio rechazaron los pilotos de prueba de la técnica de fracturamiento para la extracción de petróleo (*fracking*)** basados en múltiples argumentos de índole técnica, social, política, ambiental económica y de salud pública.   (Lea también: **[No al fracking: Deuda de campaña que Duque debe cumplir](https://archivo.contagioradio.com/no-al-fracking-deuda-de-campana-que-duque-debe-cumplir/)**)

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

**Más de 20 concejales y diputados de diversos partidos y movimientos políticos pertenecientes a departamentos como Santander, Antioquia, Boyacá y Cesar, expresaron sus razones para oponerse al *fracking*** y coincidieron en señalar que es una técnica que afecta directamente las fuentes hídricas, y consecuentemente, la salud de las comunidades. (Le puede interesar: **[Radican en la CIDH alerta contra avance del fracking](https://archivo.contagioradio.com/ante-cidh-se-radica-alerta-en-contra-del-avance-del-fracking/)**)

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Frente a esto, Urbano Villa, concejal de Yondó, Antioquia, aseguró **que la extracción mediante la técnica de *fracking* genera la contaminación con metales pesados de los agroecosistemas que abastecen de alimento a las comunidades y por ello puede generar una grave afectación a la salud pública.** En el mismo sentido, se manifestó Juan Castañeda quien señaló que el *fracking* puede generar daños cromosómicos causados por algunas sustancias utilizadas para el desarrollo de dicha práctica, que pueden derivar incluso en cáncer.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Varios de los integrantes de los Concejos y las Asambleas Departamentales coincidieron en decir que **sus regiones y en general el país, no tienen una vocación eminentemente petrolera, motivo por el cual era un error centrar la economía nacional en esa industria**. En especial, si eso implicaba la implementación de técnicas nocivas a nivel social y ambiental. Por ello, según los concejales y diputados es indispensable apoyar desde el Gobierno Nacional el desarrollo de actividades productivas acordes con el potencial del país como la agricultura.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Alertan sobre un proyecto de ley en el Congreso que daría seguridad jurídica al *fracking*

<!-- /wp:heading -->

<!-- wp:paragraph -->

En medio de la rueda de prensa el diputado por el departamento de Santander, **Camilo Torres, alertó que a través de una ley que está en trámite en el Congreso, sobre la distribución de las regalías, se quiere imponer el *fracking*** por medio de algunos de sus artículos; pese a que, según apuntó el asambleísta, **la actividad petrolera en Santander —incluso aquella basada en la extracción convencional— «*ha causado estragos y dejó un desastre en los territorios*».**

<!-- /wp:paragraph -->

<!-- wp:quote -->

> Hay una ley que está en trámite que tiene un artículo, un gorila, donde pretenden meternos el *fracking»*
>
> <cite>Camilo Torres, Diputado del departamento de Santander</cite>

<!-- /wp:quote -->

<!-- wp:paragraph -->

En el mismo sentido, el senador Jorge Robledo se pronunció al respecto, señalando que  el Gobierno Nacional quiere «*meter un verdadero ‘Orangután’ a favor del fracking en la Ley De Regalías que se discute en Comisiones Quintas del Congreso*». (Le puede interesar: [Fracking: una apuesta gubernamental de un modelo inconveniente](https://archivo.contagioradio.com/fracking-una-apuesta-gubernamental-de-un-modelo-inconveniente/))

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/JERobledo/status/1295441975844450304","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/JERobledo/status/1295441975844450304

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:image {"id":88412,"sizeSlug":"large"} -->

<figure class="wp-block-image size-large">
![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/08/Proposicion-Aditiva-Fracking.jpg){.wp-image-88412}  

<figcaption>
Este es el texto del artículo en cuestión ,cuyo proponente es el senador Alejandro Corrales del Centro Democrático

</figcaption>
</figure>
<!-- /wp:image -->

<!-- wp:core-embed/facebook {"url":"https://www.facebook.com/contagioradio/videos/600968523946903"} -->

<figure class="wp-block-embed-facebook wp-block-embed">
<div class="wp-block-embed__wrapper">

https://www.facebook.com/contagioradio/videos/600968523946903

</div>

</figure>
<!-- /wp:core-embed/facebook -->

<!-- wp:block {"ref":78955} /-->
