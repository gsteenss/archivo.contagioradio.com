Title: JEP protege memoria de las víctimas en el CNMH
Date: 2020-05-08 00:24
Author: AdminContagio
Category: Actualidad, Memoria
Tags: CNMH, Darío Acevedo, memoria
Slug: jep-protege-memoria-de-las-victimas-en-el-cnmh
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/09/Dario-Acevedo.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:heading {"align":"right","level":6,"textColor":"cyan-bluish-gray"} -->

###### Foto: Senado de la República/ CNMH {#foto-senado-de-la-república-cnmh .has-cyan-bluish-gray-color .has-text-color .has-text-align-right}

<!-- /wp:heading -->

<!-- wp:paragraph -->

El pasado miércoles 5 de mayo, la Sección de Ausencia de Reconocimiento de Verdad y Responsabilidad de los Hechos y Conductas de la Jurisdicción Especial para la Paz (JEP) adoptó medidas cautelares de manera provisional para proteger la colección "Voces para transformar a Colombia", en poder del Centro Nacional de Memoria Histórica (CNMH).

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

La colección es el primer piloto de lo que será el Museo Nacional de Memoria, y con la decisión, la JEP "busca garantizar los derechos a la verdad, la memoria colectiva, la reparación simbólica, la participación y las medidas de satisfacción de las víctimas". (Le puede interesar: ["Con Darío Acevedo la memoria sobre el paramilitarismo está en serio riesgo: Antonio Sanguino"](https://archivo.contagioradio.com/con-dario-acevedo-la-memoria-sobre-el-paramilitarismo-esta-en-serio-riesgo-antonio-sanguino/))

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### "La memoria está en peligro"

<!-- /wp:heading -->

<!-- wp:paragraph -->

El Movimiento Nacional de Víctimas de Crímenes de Estado (MOVICE) adelantó una protesta el pasado miércoles 5 de febrero, cuando se instaló la primera piedra de lo que será el Museo Nacional de la Memoria, porque fueron excluídos del evento. La situación fue una nueva oportunidad para que las víctimas pidieran la renuncia del director del CNMH, Darío Acevedo, a quien señalan como negacionista del conflicto.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El senador Iván Cepeda explica que el Museo Nacional de Memoria será un lugar "muy importante para la sociedad colombiana y por lo tanto, requiere respetar la memoria de las víctimas de manera escrupulosa; pero vemos que esto puede que no sea así porque hemos venido señalando que Dario Acevedo ha emprendido una serie de acciones que pueden terminar borrando aspectos sustanciales de la memoria de las víctimas".

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Cepeda sostiene que en algunas exposiciones que se hicieron como parte del desarrollo del Museo de la Memoria, Acevedo "intentó o censuró directamente elementos aportados por las víctimas para exposiciones" sobre el genocidio de la Unión Patriótica y los mal llamados 'falsos positivos', por ejemplo. (Le puede interesar: ["Organizaciones retiran archivos del Centro de Memoria ante políticas negacionistas del director"](https://archivo.contagioradio.com/organizaciones-retiran-archivos-del-centro-de-memoria-ante-politicas-negacionistas-del-director/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En contraste, el senador recuerda que Acevedo "le da una importancia particular a lo que se considera la memoria de los hechos criminales contra ganaderos", dando mayor relevancia a unas víctimas, y a un relato sobre el conflicto en detrimento de otro. Razones que lo llevan a la conclusión de que "la memoria está en peligro".

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### La JEP, protectora de la memoria de las víctimas

<!-- /wp:heading -->

<!-- wp:paragraph -->

Tomando en cuenta esta conclusión, y que tras una queja disciplinaria y un debate de control político Acevedo sigue censurando la memoria de las víctimas Cepeda acudió a la JEP, "que es una instancia competente porque el CNMH hace parte del sistema de instituciones que están encargadas de la verdad y la memoria en Colombia", para que preserve esa memoria.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Gracias a la acción de Cepeda, la [JEP](https://www.jep.gov.co/Sala-de-Prensa/Paginas/JEP-adopta-medida-cautelar-provisional-sobre-el-CNMH-para-proteger-la-colecci%C3%B3n-%E2%80%9CVoces-para-transformar-a-Colombia%E2%80%9D-.aspx) consideró "urgente" decretar una medida provisional porque constató, "prima facie, un incumplimiento de los acuerdos realizados entre el CNMH y las víctimas que participaron en la creación de la mencionada colección". En ese sentido, ordenó al Centro de Memoria proteger la colección por un periodo inicial de 90 días, durante los cuales no se podrá modificar la colección y su contenido, al tiempo que deberá restaurar cualquier alteración hecha previamente.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Para el senador, lo relevante de esta decisión no es solo la protección a la memoria de las víctimas respecto a la colección, sino que también se sienta un precedente que, "tal vez, abra una vía para que las víctimas acudan a la JEP" para que se garanticen sus derechos en torno a la memoria con respecto a instituciones como el CNMH.

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->
