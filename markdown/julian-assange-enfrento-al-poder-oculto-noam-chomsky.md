Title: Julian Assange enfrentó al poder oculto: Noam Chomsky
Date: 2019-04-21 14:59
Author: CtgAdm
Category: El mundo
Tags: Julian Assange, noam chomsky, Wikileaks
Slug: julian-assange-enfrento-al-poder-oculto-noam-chomsky
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/106415589_053403361-1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto:PA 

Tras la detención el pasado 11 de abril de **Julian Assange** en la sede de la embajada ecuatoriana en Londres, varias son las voces a nivel mundial que se han sumado como apoyo para el periodista y activista informático, de quien se sabe a la fecha permanece incomunicado en una cárcel del Reino Unido. Una de las personalidades que se ha referido a la situación de Assange es el **analista, filosofo y politólogo estadounidense Noam Chomsky**.

A través de un [video](https://www.facebook.com/watch/?v=294518838107768) que viene circulando por las redes sociales, Chomsky ha manifestado que la situación que vive el australiano se debe básicamente a que ha realizado un "enorme servicio a todas las personas del mundo que atesoran los valores de la libertad y democracia y que por tanto **exigen el derecho a saber lo que sus gobernantes hacen**". La razón para que sea hoy "**uno de los criminales más peligrosos sobre la faz de la tierra**, perseguido con salvajismo por los gobernantes de las sociedades libres y democráticas" Chomsky lo explica a partir de algunas teorías que exponen e**l principio básico de gobierno** y cómo las acciones de Assange atentan en su contra.

Primero alude al profesor, político liberal y asesor gubernamental **Samuel Huntington** cuando este asegura que "los arquitectos del poder en los Estados Unidos deben crear **una fuerza que se pueda sentir pero no ver**. **El poder se mantiene fuerte cuando permanece en la oscuridad, expuesto a la luz solar es que comienza a evaporarse**", a partir de esa premisa Chomsky analiza que Julian Assange "**ha cometido el grave delito de exponer el poder de la luz solar** lo que puede causar que el poder se evapore **si la población aprovecha la oportunidad de convertirse en ciudadanos independientes de una sociedad libre**, en lugar de los esclavos de un poder maestro que opera en secreto".

Luego, acude a **David Hume** y su exposición sobre los principios de gobierno, cuando se refiere a la "la facilidad con la que muchos son gobernados por unos pocos y la sumisión implícita con la que los hombres renuncian a sus propios sentimientos y pasiones frente a los de sus gobernantes", tratando de sustentar las relaciones de poder que propone, complementa su argumento citando "**mientras la fuerza está siempre del lado de los gobernados, los gobernadores no tienen nada que los respalde sino la opinión**, por lo tanto es solo en una opinión que el gobierno está fundado y esta máxima se extiende a los más despóticos y más militarizados gobiernos, así como a los más libres y más populares".

A partir de esa relación, Chomsky aterriza el pensamiento de Hume a sociedades donde la lucha popular durante años ha ganado un "considerable grado de libertad" lo que ha facilitado que "**la fuerza está realmente del lado de los gobernados y los gobernantes no tienen nada que los apoye excepto la opinión**". Desde esa perspectiva, el filósofo presenta el valor que tiene hoy la industria de la relaciones públicas como el **medio de propaganda más grande de la historia**, cuando las élites hace un siglo comprendieron que "**había demasiada libertad para que el público pudiera ser controlado por la fuerza, sería necesario controlar las actitudes y opiniones**".

Desde ahí, es que asegura Chomsky **se crea un dispositivo para controlar a la población**, que consiste en "**operar en secreto para que los intrusos ignorantes y entrometidos permanezcan en su lugar alejados de las palancas de poder que no son de su incumbencia**", lo que afirma es la razón principal para clasificar los documentos internos so pretexto de ser resguardados por cuestiones de seguridad; excusa que ataca cuando se refiere a que "**lo que se mantiene en secreto muy rara vez tiene que ver con la seguridad, excepto la seguridad del poder dominante respecto a su enemigo doméstico, la gente "equivocada"**.

En su reflexión, Chomsky concluye que el crimen de Assange consiste en "**violar el principio fundamental del gobierno al levantar el velo del secreto que protege al poder del escrutinio y evita que se evapore**", un temor que se siente desde las instancias del poder por la capacidad que tiene de "c**onducir a la libertad auténtica y democracia si un público alarmado llega a comprender que la fuerza está del lado de los gobernados y si es así puede usar su fuerza, si es que eligen controlar su propio destino**". Finalmente, hace una llamad a todas las personas a agradecer a Julian "por su valor e integridad al brindarnos este precioso regalo que le ha costado mucho para nuestra vergüenza".

[English version](https://archivo.contagioradio.com/julian-assange-faced-the-hidden-power-noam-chomsky/)

###### Reciba toda la información de Contagio Radio en [[su correo]
