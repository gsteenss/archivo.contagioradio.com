Title: Askapena: Organización de solidaridad vasca con los pueblos oprimidos ilegalizada
Date: 2015-02-22 20:21
Author: CtgAdm
Category: El mundo, Otra Mirada
Tags: Abertzale, Askapena, Askapena ilegalalizada, ilegalizada
Slug: askapena-organizacion-de-solidaridad-vasca-con-los-pueblos-oprimidos-ilegalizada
Status: published

###### **Foto:Commons.wikimedia.org** 

**Askapena organización internacionalista vasca, ha sido ilegalizada y seis de sus miembros encausados** y con peticiones de hasta seis años de cárcel por la audiencia nacional española.

Después de la redada en 2010, contra Askapena y la detención de seis de sus miembros, así como las organizaciones Herriak Aske y Elkar Truke, esta última organización de comercio justo, todas acusadas de colaboración con banda armada.

La organización social vasca de carácter internacionalista ha trabajado **solidarizándose con pueblos oprimidos de todo el mundo como Sahara, Palestina, Colombia, chile, El Salvador; Venezuela o Cuba entre otros.**

A parte del trabajo de **visibilización de las luchas sociales de los pueblos oprimidos** también ha trabajado en la cooperación y en acciones de solidaridad desde el País Vasco.

Organizaciones sociales de derechos humanos de distintos países han calificado la redada, la ilegalización y las detenciones como un golpe represivo contra el internacionalismo vasco.
