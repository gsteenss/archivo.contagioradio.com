Title: Exigen libertad de Mateo Gutiérrez a través de Change.org
Date: 2017-03-01 17:44
Category: DDHH, Nacional
Tags: Falso positivo jurídico, Mateo Gutierrez
Slug: libertad-mateo-gutierrez-change-org
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/03/libertad-para-mateo.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Libertad para Mateo ] 

###### [1 Mar 2017] 

En tan solo dos horas familiares y amigos de Mateo Gutiérrez, **han alcanzado casi 500 firmas para respaldar el pedido de libertad inmediata para el joven** estudiante de 20 años, capturado la semana pasada y vinculado a la explosión de una bomba panfletaria el 18 de septiembre de 2015. Le puede interesar: ["Se conforma red de solidaridad en defensa de Mateo Gutiérrez"](https://archivo.contagioradio.com/red-solidaridad-defensa-mateo-gutierrez/)

La petición resalta que los argumentos de la defensa superan con creces a los del Fiscal 13 de la Unidad Nacional Antiterrorismo, que pretenden acusar de terrorismo, fabricación y tráfico de armas y concierto para delinquir, entre otros delitos, y **resalta que las pruebas son inconsistentes y que están basadas en el testimonio de una sola persona.**

La carta puede ser firmada en el siguiente enlace [https://goo.gl/gU0S7p](https://www.change.org/p/fiscal%C3%ADa-general-de-la-naci%C3%B3n-libertad-inmediata-para-mateo-guti%C3%A9rrez-falso-positivo-judicial)
