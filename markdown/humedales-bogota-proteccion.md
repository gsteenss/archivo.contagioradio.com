Title: Humedales y conflictos socioambientales: una mirada desde los jóvenes
Date: 2018-07-31 12:50
Category: Voces de la Tierra
Tags: Agua, Ambiente, humedales Bogotá
Slug: humedales-bogota-proteccion
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/07/Humedal_Cordoba-e1533059032847.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: @humedalesBogotá- Jorge Emmanuel Escobar 

###### 26 Jul 2018 

Actualmente, el registro ecológico oficial da cuenta de 15 humedales reconocidos y 19 no reconocidos en la ciudad de Bogotá, pero **¿qué son los humedales? ¿por qué son importantes para el planeta y por qué debemos protegerlos?** son algunas de las preguntas que motivaron la presente edición de Voces de la Tierra.

Para ello nos acompañaron **Diana Castro**, administradora y lider ambiental que desde pequeña ha defendido el humedal La Vaca; **Omar Clavijo,** investigador en conflictos socioambientales del OCA (Obs. De Conflictos Ambientales de la UNal); y **Camilo Vergara**, Coordinador Pedagógico del Diplomado Ambiental en Bicicleta. Todos jóvenes activistas y defensores y apasionados por los humedales de la capital colombiana.

<iframe id="audio_27395601" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_27395601_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Encuentre más información sobre Prisioneros políticos en[ Voces de la tierra ](https://archivo.contagioradio.com/programas/onda-animalista/)y escúchelo los jueves a partir de las 11 a.m. en [Contagio Radio](https://archivo.contagioradio.com/alaire.html) 
