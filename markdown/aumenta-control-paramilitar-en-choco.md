Title: Aumenta el control paramilitar en Chocó
Date: 2019-10-03 21:26
Author: CtgAdm
Category: Comunidad, Nacional
Tags: AGC, Chocó, neoparamilitares, paramilitares
Slug: aumenta-control-paramilitar-en-choco
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/Neoparamilitares-en-Chocó.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Comisión Intereclesial de Justicia y Paz] 

Recientemente la Comisión Intereclesial de Justicia y Paz denunció el c**reciente control paramilitar en territorios de Chocó, específicamente, en los territorios de Riosucio y Curbaradó**. Aunque no es la primera ocasión en que la Comisión denuncia situaciones que vulneran los derechos de las comunidades, si advierten de un creciente poder territorial que se consolida, ante la inacción del Estado colombiano. (Le puede interesar: ["«Van por nuestras tierras»: Informe entregado a la JEP sobre despojos en Urabá"](https://archivo.contagioradio.com/van-por-nuestras-tierras-informe/))

### **En Contexto: La dinámica paramilitar en el territorio** 

Diana Marcela Muriel, abogada de la Comisión, sostuvo que este control militar y territorial ejercido por parte de las autodenominadas Autodefensas Gaitanistas de Colombia (AGC) no es nuevo. Según recordó la abogada, este grupo es una estructura paramilitar que permanece en el territorio desde hace más de dos décadas porque **es heredera de las Autodefensas Unidas de Colombia (AUC) Bloque Bananero o Bloque Elmer Cárdenas,** que tras el proceso de desmovilización se mantuvieron.

Aunque su modo de operar cambió, la abogada expresó que por la acción de este grupo, las comunidades siguen sufriendo confinamientos, amenazas, desplazamientos masivos como los que se dieron a finales de los 90, y las mismas dinámicas de despojo territorial. Adicionalmente, **se presenta una estrategia que recae sobre los jóvenes como es el reclutamiento forzado que a ha aumentado.  
**

Tras la firma del Acuerdo de Paz en 2016, la Comisión ha presenciado un aumento en el control territorial de las autodenominadas AGC, que hacen amenazas directas a las personas, convocan a reuniones a las comunidades, dan órdenes de cómo vivir en el territorio, y han expresado de forma directa a la misma Comisión que no puede hacer acompañamiento a las comunidades.

En ese sentido, la Abogada cuestionó la implementación del Acuerdo, que incluía la creación de una unidad investigativa especial para el desmonte de estructuras paramilitares o herederas del paramilitarismo, y tendría que estar trabajando en la desestructuración de las AGC, pero hasta el momento no se ha visto su avance. (Le puede interesar:["Juicio sobre Los 12 Apóstoles revelaría la génesis del paramilitarismo en Colombia"](https://archivo.contagioradio.com/juicio-sobre-los-12-apostoles-revelaria-la-genesis-del-paramilitarismo-en-colombia/))

### **Las recientes denuncias sobre Pedeguita y Mancilla, y Curbaradó** 

La Comisión conoció el pasado 25 de septiembre que el grupo paramilitar AGC está construyendo una casa en el punto conocido como El Cable, en dirección a Cuatro Bocas, en el Territorio Colectivo de Pedeguita y Mancilla, Riosucio (Chocó). La construcción se está realizando en un predio que fue despojado en 2017 de sus legítimos dueños y según la organización, **allí permanecen integrantes de este grupo vestidos de civil, con radios y armas cortas, haciendo labores de campo y hostigando a transeúntes.**

En cuanto a la Zona Humanitaria Las Camelias, en el territorio colectivo de Curbaradó (Chocó), la Comisión señaló que **el pasado domingo dos hombres de las AGC ingresaron en horas de la madrugada a la zona desbloqueando la cerca.** Allí, se encontraban acompañantes internacionales e integrantes de la Comisión, que en conjunto con los habitantes les pidieron respeto por este territorio. Adicionalmente, la Comisión referenció que el jueves 19 de septiembre, 31 hombres del mismo grupo paramilitar transitaron cerca a la Zona Humanitaria.

### **¿Qué hay de la acción del Estado?** 

La abogada manifestó que una de las preocupaciones es que este territorio está altamente militarizado, pero se observa una ineficacia para combatir las estructura paramilitar, que según se ha podido identificar, **se desplaza en grupos de 100 o 150 personas, algunos con camuflados y armas largas, cerca del lugar de operaciones de unidades militares.** (Le puede interesar: ["Asedio del paramilitarismo continúa en territorios colectivos de Curbaradó y Jiguamiandó"](https://archivo.contagioradio.com/asedio-paramilitar-continua-en-territorios-colectivos-de-curbarado-y-jiguamiando/))

Asimismo, Muriel cuestionó la ausencia de instituciones de carácter civil del Estado en el territorio, lo que permite que las comunidades estén a merced de los violentos, porque no hay "una institución civil que reciba las denuncias de las personas". En la práctica, la abogada aseguró que "no existe el Estado en los territorios", por lo que pide al Ministerio de Defensa que cree un mecanismo para revisar el funcionamiento militar en la zona, y que lleguen las casas de justicia, Procuraduría, Fiscalía y Defensoría del Pueblo al territorio, y tengan presencia permanente.

Sumado a ello, la experta señaló que estas comunidades, especialmente la comunidad de Pedeguita y Mancilla, deberían ser beneficiadas con medidas cautelares por parte de la Jurisdicción Especial para la Paz (JEP), al evidenciar que todas **las situaciones que allí ocurren responden a una dinámica de apropiación de la tierra que mientras no sea desactivada, seguirá generando padecimientos a las comunidades.** (Le puede interesar: ["JEP y comunidades hacen historia con primera audiencia en zona rural del Cacarica"](https://archivo.contagioradio.com/jep-comunidades-audiencia-cacarica/))

### **Hay un proyecto económico en la región basado en la agroindustria** 

Según ha podido evidenciar la Comisión a lo largo de años de acompañamiento a las comunidades del territorio, en las zonas se presenta una dinámica de pugna por el territorio que tiene dos posturas: la presencia de un proyecto económico en la región basado en la agroindustria que pretende despojar a las comunidades para sembrar plátano o palma de aceite; y la vocación de una tierra fértil, en la que tradicionalmente se han asentado comunidades afro e indígenas, con propiedades colectivas sobre sus terrenos.

La abogada resumió que **una de las causas de la violencia es "el proyecto empresarial, respaldado y consolidado con una estructura criminal que opera en la zona** (...), claramente hay otras razones que deberán identificarse por parte de los organismos de investigación para determinar si está asociado, o no, con actividades ilícitas". En ese sentido, dijo que también hay otros negocios como los relacionados con el narcotráfico, que intentan consolidar corredores de movilidad. En este caso, el llamado a las autoridades es identificar y esclarecer este fenómeno y su posible relación con el proyecto empresarial.

### **Las formas de autoprotección de las comunidades** 

Tras los grandes desplazamientos que tuvieron lugar en la década de los 90, las comunidades crearon las figuras de zonas humanitarias y zonas de biodiversidad, como forma de protección amparada por el Derecho Internacional Humanitario (DIH). Las comunidades también han acudido a la Comisión Interamericana de Derechos Humanos (CIDH) para ver protegidos sus derechos mediante el otorgamiento de medidas cautelares.

Y más recientemente, también han realizado encuentros de las memorias, como un mecanismo para proteger el territorio, evitar que los daños por parte de actores violentos y empresariales se sigan presentando y reestablecer las zonas a su estado original. El siguiente de estos encuentros será el III Festival de las Memorias, que se realizará en Caño Manso, Curbaradó. (Le puede interesar: ["Del tres al seis de octubre se realizará el tercer Festival de las Memorias en Caño Manso, Curbaradó"](https://archivo.contagioradio.com/octubre-realizara-festival-de-las-memorias-en-cano-curvarado/))

<iframe id="audio_42763019" frameborder="0" allowfullscreen scrolling="no" height="200" style="border:1px solid #EEE; box-sizing:border-box; width:100%;" src="https://co.ivoox.com/es/player_ej_42763019_4_1.html?c1=ff6600"></iframe>  
**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
