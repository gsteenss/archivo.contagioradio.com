Title: Se unen en la Liga por la Democracia para defender la participación ciudadana
Date: 2019-01-31 18:39
Author: AdminContagio
Category: DDHH, Política
Tags: Bancada Alternativa, Consulta Previa, La Liga por la Democracia, lideres sociales
Slug: 300-lideres-sociales-conforman-nueva-organizacion-para-proteger-la-democracia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/01/liga-por-la-justicia-770x400.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Suministrada] 

###### [30 ene 2019] 

Más de 300 líderes sociales de todo el país, junto con congresistas de la Bancada Alternativa, se reunieron esta semana en Bogotá para conformar la **Liga por la Democracia,** una iniciativa que busca fortalecer los mecanismos de la participación ciudadana.

"Estamos acá con defensores y defensoras de derechos humanos y de la participación directa en Colombia para mirar en contexto que son las amenazas a la democracia y cuales son las acciones a seguir," afirmó la representante a la Cámara, Ángela María Robledo, a través de un videocomunicado.

En esta asamblea de cuatro días, los líderes sociales, quienes provienen de 80 municipios, determinaron que los tres principios democráticos más amenazados en el país son la vida de los líderes sociales, el derecho a la protesta pacifica y  los mecanismo de participación directa, como la consulta popular y la consulta previa.

Particularmente, rechazaron las amenazas a los mecanismos de participación democrática. En un comunicado, denunciaron las reglamentaciones del Gobierno que van en contra de los principios de la consulta previa, los planes de vida, el etnodesarrollo de las comunidades y la autodeterminación de los pueblos.

Al respeto, le exigieron al Gobierno el cumplimiento del derecho al consentimiento y la consulta previa libre e informada. Asimismo, reafirmaron el derecho de los ciudadanos de decidir sobre el ordenamiento territorial y la vocación de su municipio a través de la consulta popular, el cual fue frenada el año pasado por la Corte Constitucional.

Frente estos hechos, las organizaciones manifestaron su "decisión de seguir en la lucha por el ejercicio de este derecho, y lo utilizaremos para la defensa del agua, de los territorios, del ambiente, de la salud, de la educación, para el logro de la equidad, para la defensa de la paz, para seguir demandando la ampliación de la democracia y para lograr la efectiva protección del Estado frente al creciente número de homicidios y amenazas de líderes y lideresas sociales."

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
