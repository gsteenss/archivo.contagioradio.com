Title: Carlos Pino fue expulsado a pesar de tener acreditación diplomática y cédula vigente
Date: 2018-12-20 16:04
Author: AdminContagio
Category: DDHH, Nacional
Tags: Carlos Pino, Colombia Humana, gloria florez, Venezuela
Slug: la-captura-de-mi-esposo-no-puede-usarse-como-chivo-expiatorio-gloria-ines-florez
Status: published

###### [Foto: Espectador] 

###### [20 Dic 2018] 

Tras la captura de Carlos Pino, la dirigente política Gloria Inés Florez, integrante de la Colombia Humana, aseguró que la detención se dio en un marco de arbitrariedades y abusos por parte de la Fuerza Pública. Además señaló que el caso de su esposo es irregular y se puede convertir en un chivo expiatorio para justificar agresiones contra Venezuela.

Florez aseguró que si existieran pruebas o expedientes en contra de su esposo, de nacionalidad venezolana, ayer se hubiesen presentado, y recalcó que están pasando por encima de los derechos de su esposo y de su hijo, al sacarlo del país. (Le puede interesar: ["Cuatro acciones con las que se quiere atacar a la Colombia Humana"](https://archivo.contagioradio.com/atacar-colombia-humana/))

### **La detención de Carlos Pino** 

Pino fue capturado cuando se encontraba en un almuerzo de despedida de año de la Unión Nacional de Trabajadores Bancarios, posteriormente lo trasladaron hasta su vivienda en donde se encontraba Glorlia Flórez, su compañera sentimental. Ella indicó que los policías golpearon a su puerta y en un primer momento, le manifestaron que la detención de Pino era producto de un mal estacionamiento.

Durante el trámite que adelantaba la Policía, Pino enseñó su cédula de extranjería, que se vence hasta el 2020, y mostró la credencial de la Embajada de Venezuela. Posteriormente los agentes llamaron a Migración Colombia, que llegaron en dos camionetas y un vehículo, asegurando que debían corroborar el estatus migratorio de Pino.

Inmediatamente después los miembros de la entidad le quitan los documentos y pese a que tanto Gloria como su hijo informaron la situación del hombre, las autoridades se lo llevaron. "Yo les explico que estamos casados legalmente, les muestro el registro de nacimiento de mi hijo, **pero los oficiales de Migración Colombia estaban en una actitud muy autoritaria y arbitraria".**

Asimismo Florez expresó que cuando pidió la información sobre a qué lugar trasladarían a Pino, los oficiales de forma grosera se negaron a dársela e incluso no quisieron decirle el lugar en donde queda Migración Colombia. **Tiempo después su esposo logró hacerle llegar la información de que estaba en Catam en donde sería deportado a Venezuela. **

Por su lado, el director de Migración Colombia, Cristian Krüger, señaló que el operativo se desarrolló de forma legal con la finalidad de "evitar riesgos para la población colombiana", no obstante Florez manfestó que sí existieron violaciones en todo el protocolo de detención y que su esposo no es un peligro para la seguridad de Colombia.

A través de las redes sociales los pronunciamientos de diversos sectores no se hicieron esperar para rechazar la actuación de las autoridades colombianas. Alirio Uribe, abogado defensor de DDHH, aseguró que "no se puede jugar a la guerra" en referencia al estado de tensión en las relaciones entre Venezuela y Colombia que podrían desatar un conflicto más allá de lo diplomático si se permite su escalamiento.

###### Reciba toda la información de Contagio Radio en [[su correo]
