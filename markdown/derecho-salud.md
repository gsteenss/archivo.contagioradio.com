Title: El derecho a la salud: Propuestas y significados
Date: 2018-08-30 18:29
Category: Entrevistas, Nacional
Slug: derecho-salud
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/08/WhatsApp-Image-2018-08-30-at-6.15.19-PM.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Cátedra de Bioética] 

###### [22 Feb 2018] 

Este 31 de agosto se realizará el foro semestral de la **Cátedra Abierta de Bioética** en la Universidad el Bosque de Bogotá, que tendrá como tema central el derecho a la salud. Del evento participarán 4 expertos de diferentes entidades, quienes expondrán sus respuestas sobre, ¿como hacer efectivo el derecho a la salud? y ¿qué elementos tienen las personas para garantizarlo?

### **Derecho a la Salud** 

**Constaza Ovalle, coordinadora del departamento de Bioetica de la Universidad del Bosque,** afirmó que la Cátedra es un espacio abierto para todos los interesados en el tema, y en esta ocasión se trabajará en el área de salud. Esto, en razón de que recientemente el tema ha sido objeto de debates políticos, y la propuesta del foro es ver la concepción de lo que significa el derecho a la salud desde diferentes estamentos.

Del panel participaran el Presidente del Tribunal de Ética de Cundinamarca, **Edgar Montoya**; el Presidente de la Junta Médica Nacional, **Roberto Baquero;** el Coordinador del grupo de Sexualidad, Derechos Sexuales y Derechos Reproductivos del Ministerio de Salud, **Ricardo Luque** y el Director General de la Fundación Santafé, **Henry Gallardo. **

La entrada a este evento es libre e iniciará a partir de las 9:00 de la mañana, en el auditorio central de la **Universidad del Bosque, en la Av Carrera novena con calle 131 \#02.** (Le puede interesar: ["La salud de la democracia: Una mirada desde la bioética"](https://archivo.contagioradio.com/catedra_abierta_bioetica_universidad_bosque/))

Las personas que lo deseen pueden hacer la inscripción previamente en la página de la Universidad. Además la iniciativa cuenta con el respaldo de las organizaciones Iniciativa Unión por la Paz, Fundación Franz Weber, el Departamento de Bioética de la Universidad del Bosque, la Organización Vivamos Humanos, la Fundación Paz y Reconciliación, la Plataforma ALTO y Contagio Radio.

###### [Reciba toda la información de Contagio Radio en] [su correo](http://bit.ly/1nvAO4u) [o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por] [Contagio Radio](http://bit.ly/1ICYhVU). 
