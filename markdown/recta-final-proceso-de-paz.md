Title: Acuerdos de paz avanzan en la recta final
Date: 2016-05-04 14:57
Category: Entrevistas, Paz
Tags: constituyente, FARC, habana, Paramilitarismo, proceso de paz, Refrendación Plebiscito
Slug: recta-final-proceso-de-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/05/viene-la-paz-contagioradio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: renovacióncultural.blogspot] 

###### [4 May 2016]

El abogado [Enrique Santiago](https://archivo.contagioradio.com/?s=enrique+santiago), asesor de la mesa de conversaciones, aseguró que el proceso de conversaciones de paz está avanzando de manera juiciosa en los temas centrales de los puntos 3 y 6, es decir, fin del conflicto e implementación y refrendación. Según el abogado **es posible que un acuerdo final se cierre en el plazo sugerido que es 6 meses** después de la firma del acuerdo sobre justicia que se dio en Diciembre de 2015.

### **Mecanismos de verificación e implementación** 

Aunque de esta parte del acuerdo no se ha abierto al debate aún, si está en discusión la reciente propuesta del ex fiscal Eduardo Montealegre de elevar el acuerdo a la calidad de tratado internacional que podría ser respaldado por la Organización de las Naciones Unidas, lo que lo convierte en tratado de obligatorio cumplimiento y se incorporaría al ordenamiento jurídico interno por esa vía, lo cual sería definido por la Corte Constitucional.

Frente a la posibilidad planteada por el congreso y en la que insiste el presidente Santos, en el sentido de que prefiere que sea el congreso de la república el que avale los acuerdos el abogado plantea que en la propuesta de plebiscito no está muy claro, porque **[plebiscito](https://archivo.contagioradio.com/?s=plebiscito) es una consulta sobre una decisión presidencial** y el acuerdo va más allá de eso. Según el abogado “no es el mecanismo más correcto”.

Además afirmó Enrique Santiago que los actos legislativos que cursan en el congreso son tan necesarios como la aprobación de los acuerdos y la incorporación al ordenamiento jurídico nacional, lo cual no está contemplado en dichos actos.

### **Desmonte del paramilitarismo** 

Aunque el abogado recuerda que la premisa es que “nada está acordado hasta que todo esté acordado” afirma que se está avanzando mucho en la creación de una unidad especial para la investigación y desmantelamiento de los [grupos paramilitares](https://archivo.contagioradio.com/?s=paramilitarismo) acordada en el punto [74 del acuerdo sobre víctimas](https://archivo.contagioradio.com/sistema-integral-de-verdad-justicia-reparacion-y-no-repeticion/), se avanza también en los **mecanismos de respuesta a las alertas tempranas**, medidas de protección para activistas políticos y sociales entre otros temas.

Así las cosas en los próximos días se conocerá un acuerdo en ese sentido que también comprometa a la sociedad en general en el desmote del fenómeno. Varias organizaciones de derechos humanos y de la sociedad civil han planteado [diagnósticos e investigaciones](https://archivo.contagioradio.com/paramilitares-hacen-presencia-en-149-municipios-de-colombia-indepaz/)encaminadas a la elaboración de una **caracterización que permita un combate efectivo de estas estructuras.**

### **Cese bilateral y dejación de armas** 

Aunque parece que en este tema es ya un acuerdo tácito en torno a la [confrontación armada](https://archivo.contagioradio.com/?s=cese+bilateral) es necesario que se acuerde que los mecanismos de verificación en la dejación de armas y en la concentración en los llamados territorios de paz. En ese sentido ya la delegación de las **[FARC ha explicado mecanismos concretos como 3 anillos de seguridad](https://archivo.contagioradio.com/delegacion-de-paz-farc-desmiente-que-haya-crisis-en-proceso-de-paz/),** uno que estaría a cargo de las propias FARC, otro a cargo de los organismos multilaterales de verificación y un tercero a cargo de las fuerzas militares.

Con ese mecanismo tratarían de blindarse la seguridad de los integrantes de las FARC en las zonas campamentarias, mientras se aplican las medidas acordadas en torno a garantías de seguridad y de no repetición, tanto para los integrantes de esa guerrilla como para los líderes sociales y políticos amenazados.

### **Puntos en el congelador** 

Aunque algunos analistas señalan que los puntos pendientes en cada uno de los acuerdos firmados hasta el momento son cruciales, el ritmo de las conversaciones y el avance de los puntos de conclusión como el de final del conflicto y cese bilateral y definitivo, est;an relegando la discusión de los llamados “puntos en el congelador”.

Enrique Santiago propone que dichos puntos, 28 en total, sean parte **de la discusión de una asamblea constituyente** dada la profundidad y la serie de asuntos conexos con esos puntos. Cabe destacar que en el congelador hay temas tan gruesos como la extranjerización de la tierra, el latifundio y la delimitación de la propiedad privada, la extracción minera, los tratados de libre comercio entre otros.

<iframe src="http://co.ivoox.com/es/player_ej_11411373_2_1.html?data=kpahk5aXe5Shhpywj5eaaZS1kp6ah5yncZOhhpywj5WRaZi3jpWah5yncabi087e18qPl8Li1c7OydSPcYy1w9TUw8nTcYarpJKw0dPYpcjd0JC%2Fw8nNs4yhhpywj5k%3D&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
