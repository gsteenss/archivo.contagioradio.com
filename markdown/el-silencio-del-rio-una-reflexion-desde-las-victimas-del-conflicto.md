Title: "El silencio del Río", una reflexión desde las víctimas del conflicto
Date: 2016-07-20 19:20
Author: AdminContagio
Category: 24 Cuadros
Tags: Cine Colombiano, El silencio del rio película, Indiebo
Slug: el-silencio-del-rio-una-reflexion-desde-las-victimas-del-conflicto
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/07/silenciorio2.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### Foto: El silencio del Rio 

##### [20 Jul 2016]

Anselmo, Epifanio y un rio que los conecta, son los protagonistas de la opera prima del director y guionista colombiano Carlos Tribiño Mamby. “El silencio del río” narra la historia de un campesino que es acechado por la violencia y de un niño que ha perdido a su padre, haciendo una reflexión en torno a las víctimas del conflicto en el país.

El primer largometraje de Tribiño Mamby, obtuvo (entre otros) el premio a Mejor Película Colombiana en el Festival de Cine de Cartagena de 2015 y vuelve como parte de la selección oficial de cine colombiano de la [segunda edición de IndieBo](https://archivo.contagioradio.com/con-una-instalacion-en-los-heroes-inicia-indiebo-2016/), el festival de cine independiente de Bogotá. La premier, que tendrá como marco el día de la Independencia del país, se llevará a cabo en el Multiplex Cine Colombia de Andino a las 8:40 PM.

<iframe src="https://www.youtube.com/embed/_17ZZVoP7Ug" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

La muestra del INDIEBO2016 que terminará el próximo 4 de Julio, incluye más de 100 piezas cinematográficas de más de 35 países que han participado en reconocidos festivales como Tribeca, Cannes, Berlinale o Sundace. Películas, documentales y cortometrajes han sido proyectados en diferentes espacios de la capital. Consulte toda la programación [aquí.](http://indiebo.co/programacion/)
