Title: Boletín Informativo Agosto 5
Date: 2015-08-05 17:03
Category: datos
Tags: Accidentes aeronaves miliitares abren dudas por compra y mantenimiento, bombardeo en la vereda Diospeña, del cabildo indígena San Marcelino en Putumayo, Heike Hänsel envía carta a presidente Santos por Miguel Angel Beltrán, Indígenas Wounaan suspenden diálogo con alcaldía de Buenaventura, Informe Defensoría del pueblo no atención salud internos en Antioquia, Movilización estudiantes Corregimiento Apartadó colegio "El Mariano"
Slug: boletin-informativo-agosto-5
Status: published

*[Noticias del Día:]*

<iframe src="http://www.ivoox.com/player_ek_6093271_2_1.html?data=l5WmlZebdY6ZmKiakpqJd6KomJKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRhtDgxtmSpZiJhaXijK7byNTWscLoytvcjcnJb6Lb0Njh0ZCZcYarpJKw0dPYpcjd0JC%2Fw8nNs4yhhpywj5k%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

-Hoy se realiza una movilización en el centro del Corregimiento San José de Apartadó, en la región del Urabá Antioqueño, ya que los estudiantes del colegio "El Mariano", exigen que se reubique un puesto militar ubicado a 50 metros del nuevo plantel educativo, y además reclaman condiciones optimas para que los niños y niñas de esa región cuenten con una educación digna de y calidad.

-A través de una carta dirigida al Presidente Juan Manuel Santos, Heike Hänsel, parlamentaria Alemana y presidenta de la Subcomisión Naciones Unidas, Organizaciones Internacionales y Globalización, hizo un llamdo al gobierno nacional, rechazando la detención del profesor Miguel Ángel Beltrán, además se pide que se le respete el debido proceso.

-La comunidad indígena desplazada Wounaan decidió suspender el diálogo con la Alcaldía de Buenaventura, debido a que esta no ha garantizado las condiciones de la Ley de Restitución de Tierras 1448, para que 56 familias retornen a su territorio tras 7 meses de permanecer desplazados en el coliseo de buenaventura.

-Los recientes accidentes de aeronaves militares y de policía en los que murieron 27 personas en la última semana despiertan dudas sobre los contratos de compras y mantenimiento a las que deben someterse al trascurrir un tiempo medio de su vida útil. El Senador Iván Cepeda afirma que se deben realizar todas las investigaciones pertinentes para esclarecer las causas de los siniestros.

-Según un informe de la Delegada para la política criminal y penitenciaria de la Defensoría del Pueblo, desde el pasado 27 de Julio no hay atención en salud para los internos de 19 establecimientos penitenciarios del departamento de Antioquia. Según la Defensoría, el problema radicaría en inconvenientes con los contratos para el personal médico que atiende a los internos.

-Según la Mesa de Organizaciones Sociales del Putumayo, la madrugada de este miércoles, se presentó un bombardeo en la vereda Diospeña, del cabildo indígena San Marcelino del municipio de San Miguel a 4 horas de Puerto Asís en el departamento del Putumayo. Las primeras versiones de la comunidad dan cuenta de 8 casas averiadas y 10 personas heridas entre ellas algunos niños.
