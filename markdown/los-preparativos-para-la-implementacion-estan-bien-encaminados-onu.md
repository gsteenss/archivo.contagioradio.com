Title: "Los preparativos para la implementación están bien encaminados": ONU
Date: 2016-08-26 16:17
Category: Nacional, Paz
Tags: Acuerdos de paz en Colombia, Diálogos de paz Colombia, misión de verificación ONU
Slug: los-preparativos-para-la-implementacion-estan-bien-encaminados-onu
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/08/Visitas-a-zonas-veredales.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: El Colombiano ] 

###### [26 Ago 2016 ] 

Entre el 8 y el 15 de agosto las delegaciones tripartitas conformadas por representantes del Gobierno colombiano, las FARC-EP y la Misión de las Naciones Unidas en Colombia, junto a delegados del Comité Internacional de la Cruz Roja y los países garantes del proceso de negociaciones, realizaron las **visitas a las zonas veredales transitorias de normalización y los puntos transitorios de normalización** con el fin de verificar que las regiones postuladas cumplieran con las características necesarias.

Tras este periodo de visitas el Secretario General del Consejo de Seguridad sobre la Misión de las Naciones Unidas en Colombia, emitió un comunicado en el que las jornadas fueron exitosas pues se contó con la participación de representantes de autoridades municipales y [[pobladores locales quienes reaccionaron positivamente](https://archivo.contagioradio.com/las-visitas-de-verificacion-en-las-zonas-veredales-desde-las-comunidades/)] ante la **buena relación de trabajo que se evidenció entre los funcionarios colombianos y los combatientes de las FARC-EP**, demostrando así que "los preparativos están bien encaminados, no solo desde el punto de vista técnico, sino también desde el político".

"Si bien se ha dialogado con el Gobierno sobre una división de responsabilidades adecuada en lo que respecta a la prestación de servicios, aún no se ha resuelto la cuestión de la participación en la financiación de los gastos para las instalaciones y servicios utilizados conjuntamente por los integrantes del mecanismo de monitoreo y verificación" aseguró el Secretario en su comunicado, por lo que recomienda que la **Misión comparta con el Gobierno el costo de activación y funcionamiento del mecanismo**, excluyendo los gastos de seguridad que serían proporcionados por el Gobierno.

El mecanismo de monitoreo y verificación deberá cumplir con cinco tareas relacionadas con la dejación y destrucción del armamento en poder de las FARC-EP dentro de los 150 días posteriores a la entrada en vigor del acuerdo final de paz, por lo que **se desplegará en ocho oficinas regionales, con una sede nacional** que estará situada en Bogotá, para las que la Misión de la ONU en Colombia asignará prioridad a la contratación de mujeres.

<iframe id="doc_15750" class="scribd_iframe_embed" src="https://www.scribd.com/embeds/322269073/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-clckcv48WxOvZIicLfi5&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="0.7729220222793488"></iframe>

###### [Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)]o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)] ] 
