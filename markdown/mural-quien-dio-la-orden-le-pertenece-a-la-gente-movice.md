Title: Mural ¿quién dió la órden? le pertenece a la gente: MOVICE
Date: 2020-02-26 17:15
Author: CtgAdm
Category: Judicial, Memoria
Tags: MOVICE, Mural
Slug: mural-quien-dio-la-orden-le-pertenece-a-la-gente-movice
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/02/Campaña-por-la-Verdad-del-MOVICE.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:heading {"align":"right","level":6,"textColor":"cyan-bluish-gray"} -->

###### Foto: @asociacionminga {#foto-asociacionminga .has-cyan-bluish-gray-color .has-text-color .has-text-align-right}

<!-- /wp:heading -->

<!-- wp:paragraph -->

Tras conocerse el fallo de tutela del Juzgado 13 Civil del Circuito de Bogotá que ordenaba eliminar de murales, redes sociales y medios de comunicación la imagen de la Campaña por la Verdad, miles de personas en redes sociales optaron por viralizar la imagen y convertirla en tendencia. En la imagen se hace referencia a los casos de ejecuciones extrajudiciales en los que estarían involucrados altos mandos militares.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/JulianRoman/status/1232498299099181057","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/JulianRoman/status/1232498299099181057

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:heading {"level":3} -->

### **¿Quién dió la orden?**

<!-- /wp:heading -->

<!-- wp:paragraph -->

El pasado martes 25 de febrero un juez le ordenó al Movimiento Nacional de Víctimas de Crímenes de Estado (MOVICE) que retire el mural, así como cualquier reproducción en imagen, de la Campaña por la Verdad en la que aparecen mencionados altos mandos militares como[Mario Montoya Uribe](https://archivo.contagioradio.com/casos-responder-montoya-ante-jep/), [Nicasio Martínez](https://archivo.contagioradio.com/deberia-investigado-mayor-general-nicacio-martinez/) o [Marcos Evangelista Pinto](https://archivo.contagioradio.com/informe-presentado-la-jep-vincula-al-comandante-la-xiii-brigada-falsos-positivos/).

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Además de su figura, en el mural se aprecia también una calavera y las cifras de los llamados 'falsos positivos' con los que los uniformados tendrían responsabilidad. Además de las cifras, en la imagen se pregunta **"¿Quién dió la orden?, cuestionando la responsabilidad en el mando por estos hechos**.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Sin embargo, para el Juzgado, con el mural **se estaría afectando la presunción de inocencia y el buen nombre de los militares,** tomando en cuenta que ninguno de ellos ha sido condenado por estos actos. Esta determinación se tomó, por una [tutela](https://archivo.contagioradio.com/con-demandas-altos-mandos-militares-quieren-censurar-mural-sobre-falsos-positivos/) presentada por el general (r) Mario Montoya y el general Marcos Evangelista Pinto.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### **La respuesta del MOVICE**

<!-- /wp:heading -->

<!-- wp:paragraph -->

En una [comunicación](https://movimientodevictimas.org/mural-quien-dio-la-orden-ya-es-patrimonio-de-la-sociedad-movice/), el MOVICE manifestó que la acción se entabló contra el mural que fue pintado cerca de la Escuela Militar en Bogotá, y ya fue censurado por la Brigada 13 del Ejército Nacional que pintó de blanco lo que era en ese momento, una obra no terminada. En ese sentido, el Movimiento recuerda que **fue la censura del mural lo que provocó que muchas personas lo replicarán, haciendo imposible su control.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En segundo lugar, la organización recalcó que "la información del mural ha sido reconocida por fallos judiciales", y en el caso específico del general Pinto, la Jurisdicción Especial para la Paz (JEP) reconoció que en su periodo al mando del Batallón de Infantería No. 27 - Magdalena, hubo un incremento sustancial de muertes ilegítimamente presentadas como bajas en combate.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por último, el Movimiento pedirá la nulidad de la decisión y que la misma regrese a la primera instancia para que se vincule a todos los que compartieron la imagen y en caso de que esto no ocurra, acudiran a la Corte Constitucional para que se revise la decisión "por considerarla como un acto de censura". De igual forma, "recurrirá a mecanismos internacionales (...) a fin de que evalúen las acciones de censura de la Brigada 13 del Ejército Nacional y del Juez 13 Civil del Circuito de Bogotá".

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### **Mural, ¿quién dió la orden? es patrimonio de la sociedad: MOVICE**

<!-- /wp:heading -->

<!-- wp:paragraph -->

En la Comunicación el Movimiento afirma que la censura avivó el interés de la sociedad por reproducir el mural censura en imagen, y por lo tanto, su control y reproducción ya no le pertenecen. Tal como se volvió a ver recientemente, el MOVICE aseguró que el "mural ¿quién dió la orden? es patrimonio de la sociedad", y por lo tanto, el debate sobre qué hacer con dicha imagen debe ser abierto a todas las personas que lo han compartido, usado y se han apropiado del mismo.

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78980} /-->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->
