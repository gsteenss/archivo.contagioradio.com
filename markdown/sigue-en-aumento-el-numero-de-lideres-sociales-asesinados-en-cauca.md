Title: Sigue en aumento el número de líderes sociales asesinados en Cauca
Date: 2020-07-06 20:06
Author: PracticasCR
Category: DDHH, Nacional
Tags: Consejo Comunitario Afro Renacer del Cañón de Micay, Líderes asesinados, Líderes sociales del Cauca
Slug: sigue-en-aumento-el-numero-de-lideres-sociales-asesinados-en-cauca
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/muerte-de-lideres-sociales.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

Se confirmaron otros dos casos de líderes sociales asesinados este 5 de junio en la vereda Betani, zona rural del Tambo, Cauca. Se reportó que los líderes afrocolombianos eran Vanessa del Carmen Mena Ortiz y Armando Suárez Rodríguez, los cuales hacían parte del Consejo Comunitario Afro Renacer del Cañón de Micay.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Según las primeras denuncias suministradas por la Fundación Sumapaz vía Twitter, fueron dos casos diferentes en menos de 6 horas y los dos líderes fueron impactados con armas de fuego. (Le puede interesar: [En Cauca toda la comunidad se siente en riesgo](https://archivo.contagioradio.com/en-cauca-toda-la-comunidad-se-siente-en-riesgo/))

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/FunSumapaz/status/1280178261495078919","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/FunSumapaz/status/1280178261495078919

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:heading {"level":3,"customTextColor":"#10688e"} -->

### **[Situación actual en Cauca ]

<!-- /wp:heading -->

<!-- wp:paragraph -->

En lo corrido del año y desde la llegada del Coronavirus al país, **los actos de violencia por la presencia de grupos legales e ilegales y los asesinatos a líderes sociales y defensores de derechos humanos han aumentado** de manera preocupante y sobre todo en departamentos como el Cauca, Antioquia y Bolivar. 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Según el Instituto de Estudios para el Desarrollo y la Paz (INDEPAZ) en Colombia, en lo que va de 2020, fueron asesinados 157 líderes sociales y defensores de derechos humanos, siendo Cauca el departamento más afectado con 51 líderes y lideresas asesinados y de estos, 9 son afrodescendientes. (Si desea conocer la lista de asesinados: [Líderes sociales y Defesnores de Derechos Humanos asesinados en 2020](http://www.indepaz.org.co/paz-al-liderazgo-social/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Aún con estas cifras y con las múltiples denuncias hechas por las comunidades a la Fiscalía, a las autoridades y con la carta enviada por los 28 eurodiputados al Gobierno de Iván Duque expresando preocupación por el incremento en actos de violencia, los habitantes tanto del Cauca como de otros departamentos, siguen desprotegidos y amenazados por la falta de atención del Estado.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Mientras tanto, continua la Marcha por la Dignidad iniciada en el Cauca para denunciar el asesinato de los líderes asesinados y exigir al Gobierno Nacional, protección y atención por la situación actual. (Le puede interesar: <https://archivo.contagioradio.com/a-pasos-de-gigante-avanza-la-marcha-por-la-dignidad/>)

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->
