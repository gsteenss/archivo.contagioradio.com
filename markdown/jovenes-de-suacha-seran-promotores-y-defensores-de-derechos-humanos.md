Title: Jóvenes de Suacha serán promotores y defensores de Derechos Humanos
Date: 2015-05-27 12:49
Category: Comunidad, Nacional
Tags: Derechos Humanos, joven, jovenes, soacha, suacha
Slug: jovenes-de-suacha-seran-promotores-y-defensores-de-derechos-humanos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/05/DDHH.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Nicolás Escandón 

<iframe src="http://www.ivoox.com/player_ek_4558289_2_1.html?data=lZqimpecfY6ZmKiak5aJd6KnmpKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRjoa3lIqvldvJssbnjMnSjbjZpcTcwpDgx9eJh5SZopbbjdXWs87j1dTfx9iPvYzYxsvS0NjTtsbnjMnSjanJcYarpJKw0dPYpcjd0JC%2Fw8nNs4yhhpywj5k%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Heiner Gaitán, Red Juvenil de Suacha] 

###### [27 may 2015] 

30 jóvenes que participaron en la Escuela Itinerante de Derechos Humanos "Jóvenes construyendo un nuevo territorio", que se llevó a cabo en el municipio de Suacha\* entre el 11 de abril y el 30 de mayo, serán certificados como promotores de Derechos Humanos del municipio.

Con el apoyo en capacitación por parte del CPDH, en las 7 sesiones de la Escuela Itinerante se trataron temas como "movimientos sociales", "territorio", "objeción de conciencia", "proceso de paz" y "derecho internacional humanitario"; y se recorrieron lugares diferentes de Suacha, 6 comunas y 1 corregimiento para conocer la realidad humanitaria del municipio. Esto les permitió "ver como se empieza a despertar en la población joven un sentimiento de afinidad por su territorio", declaró Heiner Gaitán, de la Red Juvenil de Suacha.

<div>

Este grupo de jóvenes conformarán el Comité Juvenil por la Defensa de los Derechos Humanos, para estudiar a profundidad la realidad humanitaria del municipio, con una visión más amplia de la que maneja la administración local. Según Heiner, en lo relacionado con "un Derecho Humano que no se reconoce, el Derecho al territorio", los jóvenes planean denunciar la existencia de "carrusel de la minería" en Suacha, en que se entregan espacios para la explotación minera.

El cierre de la Escuela Itinerante se realizará el sábado 30 de mayo con actividades culturales.

</div>

##### \*Se escribe Suacha y no Soacha, a solicitud los y las jóvenes del territorio. 
