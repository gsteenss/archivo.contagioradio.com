Title: Unión Europea reafirma su apoyo a los liderazgos en Colombia
Date: 2020-03-12 17:40
Author: AdminContagio
Category: Actualidad, DDHH
Tags: Alemania, Embajadas, paz, Unión europea
Slug: union-europea-reafirma-su-apoyo-a-los-liderazgos-en-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/03/CNDP.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/03/apuesta-por-la-vida-en-el-CNPR-scaled.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/03/Diseño-sin-título-11.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

*Foto: ContagioRadio*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Como parte de la campaña \#DefensamosLaVida, este jueves en el Centro Nacional de Memoria, Paz y Reconciliación, representantes de los países de la Unión Europea, **por medio de la siembra de árboles afirman su compromiso con los defensores y defensoras de derechos humanos y los liderazgos de país y destacan su preocupación por el aumento de la violencia en contra de ellos y ellas** así como contra defensores de ddhh en el país.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

\#DefendamosLaVida es una propuesta implementada desde junio de 2019 para exigir la protección de líderes sociales en Colombia, así como el reconocimiento de la labor de los defensores de la vida, el ambiente y el territorio.

<!-- /wp:paragraph -->

<!-- wp:heading -->

" El compromiso de sembrar un árbol es igual que apoyar un líder, es la esperanza en el futuro"
-----------------------------------------------------------------------------------------------

<!-- /wp:heading -->

<!-- wp:paragraph -->

Estas son las palabras de **Peter Ptassek, embajador de Alemania en Colombia,** frente a la relación que para el tiene un líder social y un árbol, *"**quien siembra un árbol expresa la esperanza de un mañana, asume un compromiso a largo plazo de una vida**, asimismo pasa con el apoyo a los líderes sociales, es creer en el futuro de este país, un futuro basado en derechos humanos".*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

De igual forma resaltó que ante los hechos que han atentado contra la vida del líder chocoano [Leyner Palacios](https://archivo.contagioradio.com/leyner-palacios-escolta-asesinado/) en los últimos días, han decidido adoptarlo dentro de esta campaña, *"sabemos que esto no remplaza lo que debe estar haciendo el Gobierno en temas de seguridad, pero **queremos ofrecer seguridad a quienes defiende la vida y la seguridad de otros**"*.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

![Siembra de árboles por la vida](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/03/Diseño-sin-título-11.jpg){.wp-image-81995}

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por su parte **Gautier Mignot, embajador de Francia en Colombia** afirmó que quienes tienen la responsabilidad de proteger e investigar los ataques en contra de los liderazgos es el Estado directamente, y que ellos como embajadores han intentado unir fuerzas para acompañar todos los procesos en los territorios.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

*"**Quien mata o agrede a alguno de ellos se esta metiendo con nosotros, con la comunidad internacional**, por ello hoy de una manera simbólica con la siembra de esos tres árboles; tres como lo derechos fundamentales, vida, libertad y seguridad, afirmamos nuestro compromiso con la vida y la paz en Colombia"*, afirmó Mignot.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por su parte la embajadora de Irlanda, Alison Milton afirmó *"**hay que tener paciencia, Colombia puede ser un modelo de paz para el mundo, pero tiene que proteger este sistema que ha creado** y trabajar en el proceso que ya se tiene para lograr realmente un proceso de paz, justicia y no repetición*".

<!-- /wp:paragraph -->

<!-- wp:quote -->

> *"Hoy queremos que las comunidades mas lejanas de Colombia nos escuchen, que sepan que no están solo, que no vamos a huir si las cosas se ponen mas difíciles, estamos acá para apoyarlos".*
>
> <cite> Alison Milton | Embajadora de Irlanda </cite>

<!-- /wp:quote -->

<!-- wp:heading {"level":3} -->

### "Los líderes, la esperanza de los territorios" Unión Europea

<!-- /wp:heading -->

<!-- wp:paragraph -->

*"Si para los organismo internacionales es difícil lograr lazos con el Gobierno para la comunidad es mucho más difícil, por eso necesitamos el respaldo y el acompañamientos de todos ustedes"* , con estas palabras, Frankin Castañeda, director de Comité de Solidaridad con Presos Políticos destacó el apoyo al cese de la violencia que ha fracturando a las comunidades del Chocó, Cauca y Antioquia donde se han registrado los últimos asesinatos a líderes sociales.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Y reiteró también el respaldo a los informes presentados por el relator de Naciones Unidas y el Alto Comisionado para los Derechos Humanos, esto frente a los comentarios del Gobierno, *"los informes son una muestra clara de lo que estamos viviendo, la respuesta del Gobierno no solamente es grosera, sino es un ataque serio a los mecanismos que ha creado la humanidad para la vulneración de los DDHH"*.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por otro lado, Diana Sanchez, directora Asociación MINGA, afirmó, *"un defensor de la vida es aquel que desde que se levanta alza su voz en cada rincón del país contra la indolencia, las injusticias, la violencia, los liderazgos sociales son una luz que cuando se apaga deja oscuridad y muchas veces desolación en las comunidades"*.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Asímismo **Jose Antequera** Coordinador del Centro de Memoria, Paz y Reconciliación afirmó su compromiso con las acciones que respalden los liderazgos sociales y la pertenecía de estos a un lugar como el CMPR, "nuestro compromiso es contar del trabajo de esta campaña, del trabajo de los defensores en las comunidades, y a invitar a los jóvenes que nos visitan a que se unan y defiendan también la vida".

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Finalmente, y como parte fundamental de este acto **[Argemiro Bailarín](https://www.justiciaypazcolombia.com/29428-2/)** integrante del Cabildo Mayor del Resguardo Embera de Urada Jiguamiandó presentó ante los representares internacionales, 7 cartas dirigidas a las Fuerzas Armadas, los grupos Armados Ilegales, empresarios y el Presidente Duque, *"queremos que las comunidades dejemos de ser víctimas del fuego cruzado, queremos que se respete nuestra vida, y se implemente ya el Acuerdo de Paz"*, afirmó el líder.

<!-- /wp:paragraph -->
