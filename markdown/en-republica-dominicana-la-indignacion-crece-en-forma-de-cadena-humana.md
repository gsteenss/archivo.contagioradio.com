Title: En República Dominicana la indignación crece en forma de cadena humana
Date: 2015-10-30 15:59
Category: El mundo, Movilización
Tags: Cadena Humana Contra la Corrupción, Corrupción en República Dominicana, Manuel Robles, Poder Ciudadano República Dominicana
Slug: en-republica-dominicana-la-indignacion-crece-en-forma-de-cadena-humana
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/10/Corrupción.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: paisdistinto.com ] 

<iframe src="http://www.ivoox.com/player_ek_9210756_2_1.html?data=mpeekpyZeo6ZmKiakpWJd6KkkZKSmaiRdI6ZmKiakpKJe6ShkZKSmaiRh8LYxtPOjc3ZscLiwpDd0dePqc2fxcrfx8jMs4zVjNvW2M7Wb9Tdz5DQ0dfWudHXyoqwlYqmd8%2Bhhpywj6jTstXVyM7cjbfFqMrjjJKSmaiReA%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Manuel Robles,  'Poder Ciudadano'] 

###### [29 Oct 2015 ] 

[República Dominicana es una de las naciones de América Latina en el que la **corrupción ha permeado, en niveles alarmantes, las instituciones públicas**, por esto diversas organizaciones se han unido en una *[Cadena humana contra la corrupción]* que ha realizado varias movilizaciones en distintas regiones del país.]

[Manuel Robles, economista e integrante de 'Poder Ciudadano', asegura que “el Estado dominicano lleva muchos años funcionando con altos índices de corrupción”, en el que gran parte de los **recursos de la población** que deberían destinarse para resolver las necesidades de educación, salud, alimentación y transportes, principalmente, son **apropiados por funcionarios corruptos** y cuyos hechos quedan en la **total impunidad**. ]

[Pese a este fenómeno la ciudadanía dominicana se ha organizado y “cada día está más consciente” afirma Robles, respondiendo a las jornadas de movilización convocadas frente a instituciones públicas la como la Oficina de Ingenieros del Estado en la que ha **primado el fraude y el robo de lo público en distintas modalidades**. ]

[De acuerdo, con Robles, en estas movilizaciones la **Policía Nacional** ha impedido el ejercicio de la libre expresión, desconociendo las sentencias constitucionales que la avalan, como si se tratase de un **“régimen dictatorial”**. Con todo ello, la ciudadanía ha atendido el llamado de cada miércoles a los plantones programados, mostrando aumento en los niveles de conciencia y organización frente al **"derecho a vivir en una sociedad libre de corrupción"**. ]

[Este proceso de empoderamiento ciudadano exige **cambios en la administración de los recursos públicos**, agrega Robles, por lo que convocan a una asamblea general para el próximo domingo en la que se construirá un plan de acción para fortalecer estas luchas y reivindicaciones, así como el **apoyo de la comunidad internacional**. ]
