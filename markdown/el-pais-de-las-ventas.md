Title: El país de las ventas
Date: 2016-01-13 11:32
Category: Camilo Alvarez, Opinion
Tags: Germán Vargas Lleras, ISAGEN
Slug: el-pais-de-las-ventas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/01/venta-de-colombia.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: microjuris 

#### **[Camilo Álvarez Benítez](https://archivo.contagioradio.com/camilo-alvarez-benitez/)- @camiloalvarezb** 

###### [13 Ene 2016]

[El comienzo de año ni siquiera alcanzó a caer como un baldado de agua fría -el calentamiento global y el fenómeno del niño no lo permitieron- El 2016 es como un vaso medio lleno, - o medio vacío- dirá el seguidor de Coelho “depende del optimismo y superación personal que le empeñen”, el tema es que ya estando empeñados, el año empezó  fue con ventas.]

[Y es que nuestra idiosincrasia parece haberse especializado en el comercio, somos hábiles para los negocios, tenemos corredores de bolsa y carteristas de calle, que no es lo mismo pero es igual, hay quien vende lo suyo y quien vende lo de otro, quien vende su madre y quien vende ISAGEN, Comerciantes netos, impulsadores, emprendedores, gerentes y culebreros que han instalado el chip de la ganancia en nuestras relaciones.]

[Superada la etapa arcaica de sembrar valores hemos llegado por fin a vender valores como debe ser según el evangelio Neoliberal, La tarea que nos espera es mejorar las técnicas y capacidades para los negocios, es allí donde reside la esperanza del post-post-postconflicto. Y en eso sí que estamos graves, la falta de creatividad nos ronda.]

[Les comparto de nuevo año la respuesta de un amigo publicista cuando le pregunté por ideas frente a la Venta de ISAGEN:]

**Formulas para los negocios:**

[- Venda lo viejo como nuevo: La idea de renovación e innovación siempre será contundente, puede usted verlo en la moda, cada 10 años nos devolvemos 30 años, en política es igual, la renovación es Peñalosa, un pantalón entubado de los 70 en horma de concreto.]

[- Nunca revele el valor real de la mercancía y mucho menos de la ganancia: recuerde que el comerciante es un intermediario, que debe comprar más barato y vender más caro, tumbar y no ser tumbado reza el credo, hay que generar la idea en quien consume que va a comprar más barato.]

[- Cada consumo implica pequeños sacrificios: Módicas cuotas mensuales, apretones de cinturón, el futuro tiene cara de sacrificio, el progreso también, recuerde que hay predisposición en que todo puede mejorar, el vaso siempre está medio vacío la promesa es que algún día se llenará.]

[-La presentación es lo más importante: Los argumentos son inversamente proporcionales al deseo de consumir, si va a vender un bien público muéstrelo como obsoleto, si va a vender un bien privado véndalo como eficaz, una imagen vale más que mil palabras.]

[Ahora bien, Acerca de lo que me plantea de frenar la venta de ISAGEN, hay que hacer que su idea de movilización social VENDA, llamar a un paro no dice nada, tal vez si lo convoca un domingo que la gente tiene tiempo, pero lo primordial, haga un slogan por ejemplo:]

**Por la venta de ISAGEN Vargas lleras y el movimiento social volveremos a las vías de hecho.**

[Y Paute, eso sí paute bien, la protesta no se vende a sí misma, póngase en esta situación de publicista y pregúntese ¿cómo se vende un derecho? ¿Cómo se vende un paro? Pregúntele a Lucho Garzón que el sí sabe de eso; En todo caso le deseo un buen 2016, que la paz nos cumpla a todos y que le vaya bien, a su idea de frenar la venta de ISAGEN le deseo BUENA ENERGIA, apague y vámonos!!!]

[  ]
