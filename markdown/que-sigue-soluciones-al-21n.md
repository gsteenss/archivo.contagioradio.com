Title: ¿Qué sigue? soluciones al 21N
Date: 2019-11-25 16:30
Author: JOHAN MENDOZA TORRES
Category: Johan Mendoza, Opinion
Tags: cacerolazo, lideres sociales, Paro Nacional Colombia, protestas
Slug: que-sigue-soluciones-al-21n
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/75576508_1249873338532404_7737551985116708864_o.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/75576508_1249873338532404_7737551985116708864_o-1.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/76705221_1249873755199029_7211690620484583424_o.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Andres Zea/Contagio Radio] 

##### Es un hecho: 

Tenemos el poder popular para modificar las agendas de gobierno y las agendas mediáticas, el uribismo vive su momento más difícil en 17 años. Fuimos más de 2 millones solo en las calles de Bogotá; no me atrevo a hacer un cálculo si sumáramos Barranquilla, Tunja, Ibagué, Medellín, Cali, Villavicencio, Armenia, Pereira, Manizales etc.

**Algo sí es cierto: tenemos la mayoría.**

En vano las protestas individualistas al estilo de Vicky Dávila y su grupito de amigos, aunque poderosos, también se dieron cuenta, de que no tienen la última palabra. Ya no la tienen. La mayoría somos la gente que salió a marchar y que apoyó otro evento histórico nunca realizado en Colombia: **un cacerolazo nacional.**  
Asimismo, cabe mencionar que sin los celulares y redes sociales no hubiéramos podido demostrar El engaño de la noche del 22 noviembre de 2019 durante el toque de queda al pueblo bogotano; un engaño que condujo a gente de todos los estratos a la histeria y al pánico colectivos, pero que finalmente, gracias a los vídeos de valientes bogotanos y bogotanas e incluso las declaraciones del alcalde Peñalosa, logró salir de ese estado y darse cuenta hasta dónde llega *¡¡no la policía!! ¡¡por favor ellos reciben órdenes!!* sino el sector más radical del uribismo; no es una acusación, pero sí una fuerte suposición: ellos lo habrían planeado, nos estamos volviendo perros viejos como ellos, y ya los conocemos; eso se sabrá, no pronto, pero saldrá a la luz algún día.

##### Y bien, ¿qué sigue? 

porque no sería justo con los motivos *(varios)* de la protesta del **21N, 22N, 23N, 24N, 25N** *y contando*, que todo terminara en fiestas callejeras. Eso está bien, es también una manifestación de un contenido popular, a fin de cuentas, mejor bailar que violentar, pero también sería bueno reconocer que podremos celebrar con mayor razón política, cuando logremos algo concreto con este momento histórico.  
Es urgente que intelectuales de todas las disciplinas determinadamente anti-uribistas, (no es tiempo para timoratos) escribamos, dialoguemos y logremos orientar caminos de comprensión para toda esta situación; esa labor hoy más que nunca es necesaria. Salir a las plazas y parques a hacer uso de la razón pública. Solo así podría generarse el escenario para la emergencia de nuevos líderes sociales, pues no olvidemos que **los están matando.**

Le puede interesar: ([21 de noviembre: por la dignidad](https://archivo.contagioradio.com/21-de-noviembre-por-la-dignidad/))

Es momento de que los representantes que ganaron por elecciones en octubre estén con su gente eligiendo el mejor camino institucional para darle un surco a tantas demandas, pero también deben recordar que el movimiento del **21N** no tiene color de partido sencillamente porque los partidos parecen no representan hoy las aspiraciones populares en Colombia, eso es una verdad que deben respetar; el movimiento del 21N no tiene precedentes en Colombia, no es exclusivamente “obrero”, “estudiantil”, “campesino”, “feminista” o “indígena” y sin embargo representa a todos. Hay que darle una lectura colectiva; por eso el rechazo unánime a que el 21N se lo tomen “los políticos”. Entonces, en este marco tengo una propuesta y agradezco a *Contagio Radio* su publicación:

Realizar cabildos abiertos ciudadanos en las plazas y parques de todos los barrios, en los que por cabildo se genere un documento que sea firmado en público por sus asistentes. Posteriormente por redes sociales generar un sistema comunicativo y de coordinación que se encargue de reunir los documentos, hacerlos públicos y llevarlos al Palacio de Nariño. Se elegirían representantes de cada cabildo, especificando lugar de la reunión y fecha para llevar el documento en físico.

##### Estos cabildos podrían tener unos objetivos: 

1\) Definir el horizonte de la protesta social **¿para dónde vamos?**

2\) Conformar las bases representativas de este nuevo movimiento social. El movimiento del 21N no tienen precedentes, no queremos que nos representen políticos o partidos, pero sin duda, necesitamos representantes emergentes de todo esto, pues corremos el riesgo de no capitalizar es poder político que tienen las marchas hoy y que ganen los discursos de que solo queremos “*desestabilizar*”, algo que es muy desacertado, pues lo que he visto es que la gente quiere nuevas formas de gobierno, quiere que las cosas cambien en Colombia.

3\) Definir propuestas de solución a los conflictos sociales y armados en Colombia.

4\) Definir propuestas de democratización de cargos y funciones que son otorgadas literalmente “a dedo” en el país. Es necesario democratizar la estructura funcional del sistema político colombiano con nuevas ideas para tal fin. Sin un documento configurado desde las bases del movimiento 21N, el presidente pondrá las condiciones, y si bien no tiene apoyo popular para tomarse ese derecho; podrá hacerlo sin problema si no surge pronto una propuesta política, colectiva, de todo este momento histórico.
