Title: El miedo a que nos timen con la paz 
Date: 2016-08-02 08:50
Category: Carolina, Opinion
Tags: paz, proceso de paz
Slug: el-miedo-a-que-nos-timen-con-la-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/08/paz-mentiras.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Contagio Radio 

#### **[Carolina Garzón Díaz](https://archivo.contagioradio.com/carolina-garzon-diaz/) \* - [@E\_Vinna](https://twitter.com/E_Vinna)** 

###### 2 Ago 2016 

[El actual proceso entre el Gobierno y las Farc nos genera ilusión y esperanza a millones de personas que creemos firmemente que es la mejor salida posible al conflicto armado y que esto repercutirá en la construcción de paz.]

[Pero no desconocemos que este anhelo de paz, o de iniciar su construcción, está lleno de temores y recelos de las partes, especialmente, de la sociedad civil. De cara a la refrendación, el reto más difícil es la confianza porque la historia nos confronta con muchos ejemplos de incumplimientos, traiciones y fracasos.]

[Nuestro temor es comprensible. ¿Cómo olvidar el genocidio de la UP? ¿Cómo confiar en un gobierno que incumple los acuerdos con los sectores sociales? ¿Cómo confiar en un grupo guerrillero que ocasionó tanto daño a miles de personas en el país? Y lo más importante, ¿Cómo saber sí esta vez van a cumplir lo acordado y no reincidirán?]

[Pero creo que también tenemos miedo al mismo acto de confiar. Así como en las relaciones sentimentales, familiares y las amistades no es fácil reconstruir la confianza cuando hay antecedentes de mentiras y promesas incumplidas. Tememos confiar nuevamente en este proceso de paz y lo acordado porque no queremos equivocarnos. Nuestro miedo es quedar como unos ingenuos: no nos gusta sentirnos timados, sentir que “nos lo volvieron a hacer”, que “nos vieron la cara”, temor de sentirnos idiotas o crédulos.]

[Siempre será más sencillo o cómodo esperar que las cosas salgan mal y tener la satisfacción de decir “Yo sí sabía”, “Se lo dije”, “A mí no engañaban”, “¿Acaso qué esperaba?”. Es la privilegiada posición de quien no arriesga pero sí tiene el descaro de juzgar.]

[Pensando en nuestro temor y en la historia de nuestro país, este proceso para la terminación del conflicto y la construcción de una paz estable y duradera ha trabajado en las Garantías de No Repetición. Estas se contemplan principalmente como parte del Sistema integral de Verdad, Justicia, Reparación y no Repetición.]

[¿De qué se tratan? Según el punto 5.1.4. del Borrador de “Acuerdo sobre las Víctimas del Conflicto”, las Garantías de No Repetición buscan prevenir que las violaciones y el mismo conflicto armado vuelvan a ocurrir. Según este acuerdo, esto se logrará a través de cuatro mecanismos y medidas: (i) “el reconocimiento de las víctimas como ciudadanos y ciudadanas que vieron sus derechos vulnerados”, (ii) “el reconocimiento de lo ocurrido en el marco del conflicto y del esclarecimiento y rechazo de las graves violaciones a los derechos humanos y de las graves infracciones al Derecho Internacional Humanitario”; (iii) “mediante la lucha contra la impunidad, a la que contribuye de manera especial la Jurisdicción Especial para la Paz y también las medidas de esclarecimiento de la verdad y de reparación”; y (iv) “mediante la promoción de la convivencia sobre la base de los reconocimientos de responsabilidad que se hagan en el marco de la Comisión para el Esclarecimiento de la Verdad, la Convivencia y la No Repetición, de la Jurisdicción Especial para la Paz y de las medidas de reparación.”]

[Así, este punto en su totalidad, que se complementa con el reciente acuerdo sobre “Garantías de seguridad” logrado en el marco del tercer punto de la agenda, buscan la no repetición del conflicto, de las violaciones a los derechos cometidas por las partes y sobre todo, la construcción de una paz basada en la convivencia.]

[No tengamos miedo de ser timados al creer en la paz. Confiar, sí bien es un riesgo, en este caso no es una decisión impulsiva ni desinformada. Con el conocimiento de lo acordado y la convicción de trabajar por la exigencia del cumplimiento de los acuerdos, esta vez podemos arriesgarnos a confiar. Esperemos que el miedo no nos congele ni nos prive de un mejor futuro.]

###### [\*Comunicadora social y periodista con profundización en periodismo digital y sistemas convergentes. Columnista. Defensora de derechos humanos. (C) Magíster en educación.] 
