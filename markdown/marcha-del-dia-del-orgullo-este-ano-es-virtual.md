Title: Virtualmente celebran el Día del Orgullo LGBTQ+
Date: 2020-06-28 15:15
Author: CtgAdm
Category: Actualidad, LGBTI
Slug: marcha-del-dia-del-orgullo-este-ano-es-virtual
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/09/LGBTI-770x400.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

Este 28 de junio se celebra el día del Orgullo LGBTQ+, una fecha que año tras año congrega una gran cantidad de personas que se movilizan en torno a la conmemoración de los disturbios de Stonewall en 1969, que constituyeron uno de los primeros actos reivindicatorios de esta comunidad de los que se tiene registro.   

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por motivo de la cuarentena, al igual que muchos otros eventos, la marcha del orgullo de lesbianas, gays, bisexuales y personas trans no podrá ser realizada de manera presencial dadas las medidas de aislamiento obligatorio; no obstante, varios colectivos como Colombia Diversa están convocando para que el Día del Orgullo se celebre de manera virtual bajo la consigna **\#ResistimosConOrgullo**.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/ColombiaDiversa/status/1277225238737158147","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/ColombiaDiversa/status/1277225238737158147

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:heading {"level":3} -->

### La comunidad LGBT en medio de la cuarentena

<!-- /wp:heading -->

<!-- wp:paragraph -->

**Marcela Sánchez Buitrago, directora de Colombia Diversa**, asegura que las restricciones de movilidad, el confinamiento en los hogares y los prejuicios en torno a las personas LGBTQ+, han agravado las situaciones de violencia que estas personas viven.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Adicionalmente, sostiene que trabajar sobre temas de diversidad sexual en contextos tan adversos y precarios como los que se viven en algunos territorios de Colombia es bastante complejo; denuncia que existen diversos actos de violencia en contra de las personas LGBT+ entre los que se cuentan ***«homicidios, violencia policial y amenazas» y que «existe una altísima impunidad y muy poco avance en las investigaciones»*** que tienen que ver con dichos actos de violencia.

<!-- /wp:paragraph -->

<!-- wp:quote -->

>  *"En la comunidad no solo somos víctimas, también somos resistentes. **El orgullo se vive, el orgullo es lo que nos mantiene vivos y luchando. A pesar de que nos discriminen y violenten nosotros nos sentimos bien con lo que somos** y vamos a seguir trabajando para que se nos respete en nuestra diferencia -Marcela Sánchez Buitrago, directora de Colombia Diversa*"
>
> <cite>**Marcela Sánchez Buitrago| Directora de Colombia Diversa**</cite>

<!-- /wp:quote -->

<!-- wp:paragraph -->

Ante la difícil situación que ha planteado el aislamiento, Colombia Diversa, le ha apostado a orientar a aquellas personas pertenecientes a la comunidad LGBT ante diversas vulneraciones o discriminaciones de las que puedan ser víctimas, con el fin de hacer valer sus derechos, para lo cual desarrolló una especie de [instructivo](https://colombiadiversa.org/noticias/covid-19-aislamiento-y-derechos-de-personas-lgbt-en-colombia-segundo-especial/) en el que se contemplan las situaciones más comunes a las que se pueden ver expuestas las personas LGBT en medio del confinamiento.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

(Le puede interesar ver: **Otra Mirada: \#ResistimosConOrgullo **)

<!-- /wp:paragraph -->

<!-- wp:html -->  
<iframe src="https://www.facebook.com/plugins/video.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2Fvideos%2F317682382593872%2F&amp;show_text=false&amp;width=734&amp;height=413&amp;appId" width="734" height="413" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowtransparency="true" allow="encrypted-media" allowfullscreen="true"></iframe>  
<!-- /wp:html -->

<!-- wp:block {"ref":78955} /-->
