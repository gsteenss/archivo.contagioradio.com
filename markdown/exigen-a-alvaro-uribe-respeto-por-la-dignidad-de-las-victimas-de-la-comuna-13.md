Title: Exigen a Alvaro Uribe respeto por la dignidad de las víctimas de la Comuna 13
Date: 2015-10-22 15:38
Author: AdminContagio
Category: Nacional, Resistencias
Tags: Alvaro Uribe, Comuna 13, Corporación Jurídica Libertad, Grupos Interdisciplinarios por los Derechos Humanos, Mario Montoya, MOVICE, Mujeres Caminando por la Verdad, operación orion
Slug: exigen-a-alvaro-uribe-respeto-por-la-dignidad-de-las-victimas-de-la-comuna-13
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/10/comuna13_4_co0ntagioradio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: movice 

###### [22 Oct 2015]

El Colectivo de Mujeres Caminando por la Verdad, organización de mujeres víctimas de desapariciones forzadas en la Comuna 13, la Corporación Jurídica Libertad y los Grupos Interdisciplinarios por los Derechos Humanos exigieron que el Senador Uribe se retracte de las afirmaciones realizadas en su cuenta de Twitter en las que se refiere a la operación Orion.

Las víctimas señalan que siguen siendo víctimas de las políticas de agresión aplicadas durante el mandato de Uribe, y afirmaron que continuarán resistiendo ante "su cinismo e impunidad" y en la búsqueda de que los crímenes cometidos no se repitan.

Las organizaciones exigieron “que el ex mandatario reconozca su responsabilidad en las graves violaciones a los derechos humanos, y en la comisión de los crímenes de lesa humanidad que se cometieron en contra de toda la población durante las diferentes operaciones militares que se ordenaron en su mandato”.

En el comunicado las organizaciones de víctimas y de Derechos Humanos señalan que durante las diferentes operaciones militares realizadas en esa localidad se produjeron cerca de 600 víctimas directas, entre ellas 100 desapariciones forzadas aún si escalrecer

Durante su visita a la Comuna 13 el senador Alvaro Uribe publicó una serie de trinos entre los que afirma que la Operación Orión en 2002 y su gobierno “comenzó la construcción de la paz en Medellín” y en la que calificó la operación militar como una acción que llevó la paz y la tranquilidad a la Comuna. “Mis coterráneos de la Comuna 13 de Medellín agradecen la paz y la tranquilidad que les trajo la Operación Orion”

[![Uribe 1](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/10/Uribe-1.png){.wp-image-16173 .aligncenter width="293" height="240"}](https://archivo.contagioradio.com/exigen-a-alvaro-uribe-respeto-por-la-dignidad-de-las-victimas-de-la-comuna-13/uribe-1/)   [![Uribe 2](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/10/Uribe-2.png){.wp-image-16174 .aligncenter width="350" height="100"}](https://archivo.contagioradio.com/exigen-a-alvaro-uribe-respeto-por-la-dignidad-de-las-victimas-de-la-comuna-13/uribe-2/)

La visita del Senador se da en el marco de la campaña electoral que dirige el Senador a favor de su familiar Juan Carlos Vélez, a cinco días de conmemorarse 13 años de la Operación Orión y en medio de las labores de búsqueda que se realizan en el sector de la Escombrera en la misma localidad y en la que se buscan los restos de las víctimas de desaparición forzada, que según versiones de paramilitares, habrían sido arrojadas a ese sitio.

La Operación Orión, de las FFMM pertenecientes a la IV Brigada del Ejército, comandadas por el General Mario Montoya, hoy investigado por sus crímenes, se realizó entre el 16 y 17 de Octubre de 2002 a tan solo un mes de la posesión de Uribe como presidente. Según diversos análisis e informes de organismos de DDHH esa operación consolidó el control paramilitar que persiste hasta ahora.

 
