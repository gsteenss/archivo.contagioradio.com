Title: Alcaldía de Barranquilla quiere desalojar a 57 familias del Tamarindo
Date: 2015-04-14 12:41
Author: CtgAdm
Category: Comunidad, Nacional
Tags: 9 de abril, alcaldía de Barranquilla, Asotracampo, Atlántico, Barranquilla, Conpaz, Defensoría del Pueblo, El Mirador, Elsa Noguera, Escuadrón antidisturbios de la Policía Nacional, Espacio Huma, Tamarindo, Unidad de Víctimas
Slug: alcaldia-de-barranquilla-quiere-desalojar-a-57-familias-del-tamarindo
Status: published

##### Foto: Conpaz 

<iframe src="http://www.ivoox.com/player_ek_4352079_2_1.html?data=lZiilJWbfY6ZmKiakpWJd6KkkZKSmaiRdI6ZmKiakpKJe6ShkZKSmaiRhc3XwsmSpZiJhaXVjMnSjafFttPVz9biy9HQpYzl1s7S1MqPqMbnwtHczMbWb8KflpyYyMbRrc3dwtiah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Juan Martínez, Asotracampo] 

De 134 familias que estaban ubicadas en los predios del **Tamarindo, Barranquilla** hoy solo quedan **57 que llevan viviendo más de 10 años,** y actualmente se encuentran en peligro de desalojo sin contar con ninguna garantía, revictimizando a **89 familias** que habían migrado hacia esa región del Atlántico por cuenta del **conflicto armado**.

De acuerdo a Juan Martínez, representante de la Asociación de Desplazados y Campesinos del Tamarindo, Asotracampo, el desalojo de se debe a intereses privados de **familiares de la alcaldesa de Barranquilla, Elsa Noguera,** donde se pretende construir bodegas y zonas residenciales.

El pasado 9 de abril, mientras la Unidad de Víctimas, la gobernación y Defensoría del Pueblo hablaban de apoyo a las víctimas, ese mismo día en otra región del país, la **alcaldesa de Barranquilla** expresaba que ya no iba a haber más conversaciones con la comunidad del Tamarindo, y se realizaría la diligencia de desalojo, según afirma el vocero de Asotracampo.

Según, Martínez la comunidad internacional ha realizado un llamado para que se respeten los derechos de las víctimas que están siendo revictimizadas al ser desalojadas forzosamente, como ya sucedió en **el año 2008,** cuando un inspector de la Alcaldía de Barranquilla acompañado del Escuadrón antidisturbios, **ESMAD**, de la Policía Nacional y otros civiles armados con palos y machetes, **destruyeron los ranchos y los cultivos de varias familias que hasta ese momento estaban intentado rehacer sus vidas.**

Por el momento, **la alcaldía únicamente ofrece 24 casas y piensa realizar el desalojo sin cumplir con los acuerdos pactos en el 2014**, donde se les garantizaba a las familias su reubicación con acceso a una vida digna. Por su parte Juan Martínez, solo espera que haya una actuación rápida de las autoridades para que no se realice el desalojo.
