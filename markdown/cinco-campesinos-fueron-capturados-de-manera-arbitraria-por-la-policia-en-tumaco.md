Title: Cinco campesinos fueron capturados de manera arbitraria por la Policía en Tumaco
Date: 2018-04-26 13:52
Category: DDHH, Movilización
Tags: ASOMINUMA, nariño, Policía Antinarcóticos, Tumaco
Slug: cinco-campesinos-fueron-capturados-de-manera-arbitraria-por-la-policia-en-tumaco
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/10/Ejercito-Popular-de-Liberación-e1527526839809.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: EFE] 

###### [26 Abr 2018] 

La Asociación de Juntas de Acción Comunal de los ríos Mira, Nulpe y Mataje ASOMINUMA, denunciaron que el pasado 24 de abril, en la vereda el Vallenato, en el municipio de Alto Mira y Frontera, a las 10:00 am, unidades de Policía Antinarcóticos retuvieron arbitrariamente a **5 campesinos mayores de edad y a 6 menores de edad. Posteriormente fueron trasladados hasta la base de Antinarcóticos en Tumaco**, y allí luego de la intervención de defensores de derechos humanos fueron puestos en libertad.

De acuerdo con Diana Montilla, integrante de ASOMINUMA, el argumento que dio la Policía de Antinarcóticos para la retención, fue la revisión de antecedentes legales. Sin embargo, esa explicación no aclara por qué se retuvieron a menores de edad entre **los 4 a los 14 años**, ni porque la Defensoría de menores tampoco tuvo conocimiento sobre el mismo hecho.

“Es bastante reprochable la actitud que asumen los policías, en el sentido de que deciden dejar en libertad a los campesinos, dejándolos a su suerte en la ciudad de Tumaco, aún conociendo las condiciones tan difíciles que ha afrontado las comunidades del territorio del Alto Mira y **más los que pertenecen a ASOMINUMA, como los son las personas que fueron retenidas**” afirmó Montilla.

Las personas retornaron el pasado 25 de abril al territorio de Alto Mira y Frontera, y de acuerdo con Montilla, pesé a que no fueron víctimas de ningún tipo de agresión, si tienen un gran temor producto de la **estigmatización de la Fuerza Pública hacia los campesinos, y por la irregularidad de esta captura**. (Le puede interesar:["Ingreso de Ejército a resguardos pone en riesgo vida de indígenas en Tumaco, Nariño"](https://archivo.contagioradio.com/ingreso-de-ejercito-a-resguardos-pone-en-riesgo-vida-de-indigenas-en-tumaco-narino/))

A estos hechos se suma la intensidad del conflicto armado, las comunidades denunciaron el vuelo de un helicóptero hasta altas horas de la noche y enfrentamientos. Sin embargo, para Diana Montilla las agresiones por parte de la Fuerza Pública a los habitantes “es una situación que se está repitiendo y ES viviendo muy similar a cuando, en su momento, el territorio fue ocupado por las FARC”.

### **Agresiones contra Líder social Dario Ballesteros integrante de ASOMUNUMA** 

De igual forma, Montilla denunció que el pasado 13 de abril, el líder social Darío Ballesteros salió del territorio hacia la cabecera, el corregimiento de Llorente. **En horas de la noche fue víctima de un disparo en la columna, en cercanías a unidades del Ejército**. Actualmente se encuentra delicado de salud, en el Hospital de Pasto.

Ballesteros hace parte de una de las Juntas de ASUMINUMA. Frente a quiénes estarían detrás de estos hechos la defensora de derechos humanos aseguró que “creemos que se trataba de algún tipo de enfrentamiento, no sabemos nada desde el Ejército, o un enfrentamiento entre las mismas bandas que se ubican en el sector de Vaquería”.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
