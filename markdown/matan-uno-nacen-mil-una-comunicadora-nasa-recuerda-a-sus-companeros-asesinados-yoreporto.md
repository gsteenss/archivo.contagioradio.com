Title: “Matan uno, nacen mil”: una comunicadora nasa recuerda a sus compañeros asesinados #YoReporto
Date: 2020-08-26 20:32
Author: CtgAdm
Category: yoreporto
Tags: comunidades, comunidades indígenas, indigenas, memoria de colombia, Memoria y Territorio, muertes
Slug: matan-uno-nacen-mil-una-comunicadora-nasa-recuerda-a-sus-companeros-asesinados-yoreporto
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/08/IMG_0797.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### Foto: Andrés Zea - Contagio Radio 

[[[En un instante, Diana Collazos y todos los integrantes del pueblo nasa perdieron a dos compañeros de lucha. Abelardo Liz, comunicador de Nación Nasa Stereo, estación radial de Corinto, y José Ernesto Rivera, liberador de la madre tierra, fueron asesinados por el Ejército Nacional el 13 de agosto en medio de operativos de desalojo. Collazos, comunicadora nasa de Radio Payumat, recuerda a sus compañeros como exponentes de dos procesos fundamentales de la resistencia del pueblo nasa: la liberación de la palabra y de la madre tierra.]]

[[[La liberación de la palabra es el propósito del proceso de comunicación del pueblo nasa. Más que una búsqueda de clics o vistas, el impulso de los comunicadores nasa es su conocimiento profundo de los procesos de base de la comunidad. Ellos se involucran en las mingas y marchas para definir la agenda informativa, los formatos y ángulos de las historias. “Aquí primero nos involucramos en lo que está haciendo la comunidad para después salir a decir desde lo que eso sembró en mí”, explica Diana. Para ella, Abelardo Liz luchaba como comunicador en contra de la agenda estigmatizante impuesta desde lo medios tradicionales.]]

[[[La otra liberación es la de la madre tierra, un proceso de recuperación de territorios que los nasa reclaman como propios y hoy son cañaduzales para la producción de bioetanol. Grupos de liberadores bajan de las montañas a los valles, arman cambuches en los territorios que se proponen liberar, arrancan caña y siembran para alimentarse y repartir en marchas de la comida hacia barrios empobrecidos de Cali y otras ciudades del país. “El proceso de liberación de la madre tierra es pensarse a la tierra como persona, como una mujer que está siendo explotada, y violentada por las multinacionales y el gobierno, que la ha vendido” explica Diana. “\[La tierra\] es una mujer infértil y el proceso es para que ella vuelva a ser fértil”, recuerda que le explicó una vez uno de los mayores de la comunidad. ]]

[[[José Ernesto Rivera estaba de lleno en la liberación de la madre tierra. Poco se sabía sobre él, pero nadie dudaba de su compromiso con el pueblo nasa. Decía que del proceso saldría con los pies por delante, diana dice que ni así, que, a pesar de su asesinato, sigue marchando con los liberadores. Sus abuelos expresaron orgullo por su lucha y su hermano se sumó a la liberación. “Matan uno, nacen mil”, dice Diana, no como consuelo sino como certeza de que los pueblos indígenas de la región seguirán resistiendo. El Cauca, según la Fundación Paz y Reconciliación, es el actual epicentro nacional de los asesinatos de líderes sociales. Según Indepaz, van 64 asesinados en el departamento, 37 de ellos, integrantes de pueblos indígenas. “Nosotros hemos tenido que enterrar a muchos compañeros del proceso, pero en este caso, por ser tan cercanos, ha sido muy fuerte. ¿Qué nos dejan sembrado estos compañeros? el seguir caminando”. ]]

[[[Diana dedicó la primera mitad del 2020 a recoger historias sobre la búsqueda de la paz en los territorios indígenas del norte del Cauca. Como becaria del programa]][[[[<u>Viva Voz</u>]]](http://vivavoz.org)[[[, iniciativa de Memria.org y Luminate con el apoyo de la Comisión de la Verdad, entrevistó a cerca de 50 líderes de su comunidad. A partir de este archivo, produjo un podcast de]][[[[<u>cuatro episodios</u>]]](https://www.vivavoz.org/el-placer)[[[. En uno de ellos, explicó el propósito de la Guardia Indígena nasa que está liderando la resistencia a la violencia brutal. Esta es la guardia en las voces de los guardianes y las guardianas. ]]

[[[LA GUARDIA INDÍGENA SEGÚN SUS INTEGRANTES]]

\[embed\]https://soundcloud.com/user-399177277/la-guardia-indigena\[/embed\]

###### [**La sección Yo Reporto es un espacio de expresión libre en donde se refleja exclusivamente los puntos de vista de los autores y no compromete el pensamiento, la opinión, ni la linea editorial de Contagio Radio.**]
