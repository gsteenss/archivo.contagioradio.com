Title: Inicia mesa de diálogo entre Cerrejón y Nación Wayuu
Date: 2018-03-16 17:13
Category: Nacional, yoreporto
Tags: Cerrejón, Guajira, nación wayuu
Slug: mesa-wayuu-cerrejon
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/03/Nación-Wayuu.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Nación Wayuu 

##### 16 Mar 2018  
Por: Olga Mendoza - [\#YoReporto](https://archivo.contagioradio.com/?s=yo+reporto) 

El pasado 15 de marzo del 2018, en las instalaciones del auditorio del Hotel Waya Wayuu del Municipio de Albania, se llevó a cabo la instalación oficial de la Mesa de Diálogo y Negociación Cerrejón-Nación Wayuu en el marco del cumplimiento a la sentencia T-704/16 de la Corte Constitucional, en donde se establece “…Ordenar a la empresa El Cerrejón un plan inmediato de mitigación de daños ambientales, sociales y culturales en la zona, por lo cual deberá compensar los daños causados por la explotación de carbón al ambiente y a los derechos de las comunidades”.

El objetivo de la mesa es trabajar en la construcción de una agenda de desarrollo social, encaminada a atender las iniciativas y propuestas que se vienen gestando desde las comunidades, mediante alianzas público-privadas. A la mesa asistieron 250 miembros de la Nación Wayuu, entre Autoridades Tradicionales y líderes de las distintas comunidades, como Katsaliamana, Pesuapa, Kasischon, Japurarao, entre otras.

“Los palabreros saben, y hace parte de la tradición como etnia viva, que la palabra es un medio conciliador, y que por medio de la reparación de los daños se puede trascender cualquier conflicto. Es por eso que los diálogos incluyen la formulación e implementación de un plan de relacionamiento para la definición de principios y mecanismos que permitan fortalecer el relacionamiento futuro y el reconocimiento mutuo del territorio ancestral y de la operación minera”, explicó José Silva, Representante Legal de Movimiento Nación Wayuu. Agregó además “Dar este paso es gratificante después de cuatro décadas de explotación de carbón en la región, en donde nosotros no queremos ningún beneficio monetario e individual, lo que buscamos es inversión social en las comunidades, traducidas en programas y proyectos para nuestro bienestar y desarrollo".

Por su parte, Lina Echeverry, Vice presidenta de asuntos públicos y comunicaciones de Cerrejón manifestó, de acuerdo con el comunicado de prensa emitido por la Nación Wayuu, que: "Debemos soñar con una Guajira Próspera comprometida y con una Nación Wayuu que aproveche la oportunidad para sacar lo mejor de su tierra y salir adelante. Los invito a que nos encontremos y trabajemos en una ruta con principios metodologías y garantías para ambas partes, que permitan la consecución de buenos resultados". Además hizo una invitación a trabajar sobre cuatro pilares para la Guajira que se quiere en un futuro, estos son: la reconstrucción económica, el fortalecimiento de la oferta educativa, el dialogo entre actores y el arraigo y la identidad Guajira.

La mesa continúa hoy, día 16 de marzo, para establecer una agenda de trabajo que será definida, por parte de la Nación Wayuú, por su presidente y representante, José Silva, dos asesores jurídicos, un palabrero, un traductor, y asesores. Por parte de El Cerrejón, la mesa estará conformada por un coordinador, un asesor Jurídico, un traductor y una secretaria

Es importante entender que, como lo expresó José Silva en su discurso de apertura, “…A lo largo de estos últimos cuarenta años, el producto que ha resultado de las actividades extractivistas, ha enriquecido o beneficiado ampliamente a sectores de la sociedad colombiana y europea, pero los balances en la región son, desafortunadamente, desoladores: cifras de desnutrición en la región en más de 35.000 personas (Informe de la Defensoría del Pueblo, 2014), y más de 5.000 niños muertos en los últimos 8 años por causas asociadas a la desnutrición, a enfermedades respiratorias y diarreicas agudas”.

Por otro lado, el líder hizo énfasis en que “debido a las políticas mineras, las fuentes de agua de las comunidades fueron desviadas, contaminadas o desecadas, los animales de los que dependemos para vivir han muerto ante la ausencia de agua o territorio, y muchas especies han desaparecido como consecuencia de un medio ambiente deteriorado; nuestros medios tradicionales de subsistencia han sido alterados, nuestros territorios, sitios y cementerios sagrados, profanados; nuestras comunidades han sido desplazadas de sus territorios”.

Para cerrar su discurso, manifestó “Es necesario que ustedes sepan que hay heridas irreparables que han marcado nuestra etnia. Para nosotros el territorio es vida y cosmogonía, no es una concepción lineal o de linderos específicos. Para nosotros el territorio es un espacio que abarca no solo la dimensión material sino también abarca a los creadores de la naturaleza, a cada planta, animal, cada río, cada piedra y cada árbol, y que termina abarcándonos a nosotros como personas, incluyendo a nuestros muertos, y al lugar donde ellos habitan y a donde ellos van después de su muerte. Nuestro territorio no acaba en el límite de nuestra ranchería, sino que es un espacio multidimensional, en donde nos comunicamos a través de nuestros sueños con los espíritus, con los guías, en donde nuestros curanderos y curanderas, nuestros palabreros y autoridades tradicionales encuentran el destino de ellos mismos y de la comunidad. Cada parte del desierto es una parte de este universo que fue violentado a los pocos por la actividad minera. Cada explosión destruye, no solo a la madre tierra, sino a su vez toda una tradición ancestral que comprende nuestra concepción del mundo y el universo.”

Esperamos que la ciudadanía en general acompañe esta histórica mesa, y que a través de su seguimiento, podamos entre todos garantizar la reivindicación de los derechos de esta etnia.
