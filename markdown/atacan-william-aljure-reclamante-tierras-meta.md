Title: Atacan a William Aljure, reclamante de tierras de Mapiripan
Date: 2017-05-23 13:39
Category: DDHH, Nacional
Tags: lideres sociales
Slug: atacan-william-aljure-reclamante-tierras-meta
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/04/WhatsApp-Image-2017-04-02-at-8.52.57-PM.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Contagio Radio 

###### [23 May 2017] 

<div class="chapo">

En las últimas horas el reclamante de tierras William Aljure, **fue víctima de una agresión física por parte de sujetos desconocidos el puente Los Fundadores** mientras se dirigía desde la ciudad de Villavicencio a Bogotá, como lo denunció la Comisión Intereclesial de Justicia y Paz.

</div>

<div class="texte entry-content">

Según la denuncia, sobre las 9:20 de la mañana, Aljure observó que una persona que le tomaba fotografías. "El reclamante de tierras inquirió al hombre sobre las fotos que le estaba tomando y este de manera enérgica respondió: '*Pues será muy importante'*. Inmediatamente otro sujeto que tenía una cicatriz en el rostro atacó William por la espalda y manifestó: '*Podrá ser muy famoso'*, luego le propinó golpes en el rostro", dice la denuncia.

Sin embargo, los golpes pararon gracias a la intervención de varias personas que se encontraban en el lugar.  **Los testigos aseguran que uno de los hombres tenía una cicatriz en el rostro y portaba un arma blanca tipo puñal, y este mismo huyó hacia un potrero**, mientras el otro sujeto se se escapó en un vehículo de transporte público.

Cabe recodar que Aljure, es reclamante de tierras y víctima de reiteradas amenazas, en medio de su exigencia de derechos el predio Santa Ana, en el municipio de Mapiripán. **El líder campesino es beneficiario de medidas de protección por parte de la Unidad Nacional** de Protección; sin embargo al momento del ataque no contaba con el acompañamiento del Escolta asignado.

No obstante, el escolta de la UNP se negó acompañar el traslado del líder bajo el argumento de no haber recibido autorización para los viáticos. No obstante, la Comisión de Justicia y Paz señala que el responsable UNP de los escoltas asignados a la regional Meta, John Solaque, afirmó que los viáticos fueron aprobados.

**"Resulta sospechosa la actitud omisiva del escolta de la UNP",** dice la organización, que agrega que "Es necesario que la Unidad realice prontamente una reevaluación del estudio de riesgo de William y asigne medidas especiales de protección que supere el esquema blando con el que hasta ahora se cuenta (Escolta, chaleco y celular) teniendo en cuenta las continuas amenazas que ponen en riesgo la vida e integridad del líder reclamante de tierras".

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 

</div>
