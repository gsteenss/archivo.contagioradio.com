Title: Al Presidente le da miedo "debatir con gallardía": CRIC
Date: 2020-10-23 18:06
Author: CtgAdm
Category: Actualidad, DDHH
Tags: CRIC, Derechos Humanos, Minga
Slug: hay-un-presidente-en-colombia-que-le-da-miedo-mirar-al-pueblo-a-los-ojos-y-debatir-con-gallardia-cric
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/10/WhatsApp-Audio-2020-10-23-at-1.50.26-PM.ogg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

Este viernes por a travez de un comunicado el Concejo Regional Indígena del Cauca **(CRIC), dio a conocer su cancelación a toda la agenda programada con el Comisionado de Paz**, en todo el departamento del Cauca.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

La misiva enviada desde Popayán y dirigida a el Alto Comisionado para la Paz, Miguel Ceballos, tiene como argumento por parte del CRIC no asistir a ninguna de las reuniones que tenían planeadas con el Alto Comisionado, a razón de que este compromiso ***"transgrede el caminar de la Minga Nacional, y el de la 127 autoridades indígenas del Cauca y los 10 pueblos que la conforman".***

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Al mismo tiempo señalaron que la cancelación se da debido a que esta reunión ***"en ningún momento se está dando en el marco de la Minga nacional, como usted le pretende mostrar ante los medios y el país".***([Resistencia de comunidades indígenas durante conflicto armado](https://archivo.contagioradio.com/resistencia-de-comunidades-indigenas-durante-conflicto-armado/)[«La Minga es una expresión de unidad en medio de la violencia»](https://archivo.contagioradio.com/la-minga-es-una-expresion-de-unidad-en-medio-de-la-violencia/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Y agregaron, que a esta derogación dejan como constancia que ha sido el Gobierno Nacional **el que no ha querido atender a los múltiples llamados que han realizado como Minga y como CRIC**, y que al contrario esto hace parte de una estrategia qué pretende *"fraccionar el tejido colectivo de la Minga".*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Y finalmente, resaltaron que las autoridades indígenas integrantes del CRIC, no aceptan las apreciaciones políticas contrarias a los planteamientos de la Minga, y declaran que el motivo de la cancelación de la agenda establecida para el Cauca *"es porque **han analizado la actitud de desprecio y discriminación frente a un ejercicio serio, que veníamos consolidando** como garantía al derecho fundamental y constitucional a la vida, la paz y el territorio".*

<!-- /wp:paragraph -->

<!-- wp:audio {"id":91846} -->

<figure class="wp-block-audio">
<audio controls src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/10/WhatsApp-Audio-2020-10-23-at-1.50.26-PM.ogg">
</audio>
  

<figcaption>
Escuche acá los argumentos de esta decisión en la voz de Noelia Campo, Consejera de paz del Cric

</figcaption>
</figure>
<!-- /wp:audio -->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Continua la campaña de estigmatización en contra del CRIC y la Minga

<!-- /wp:heading -->

<!-- wp:paragraph -->

**Noelia Campo, Consejera de Paz del CRIC** y su referencia también a a los diferentes mensajes enviados por e el alto comisionado a través de su cuenta de Twitter y le pide,*"que no siga esquematizando y señalando por los medios de comunicación sobre el trabajo que estaba agendado junto con las comunidades indígenas, desconociendo la realidad por la cual se canceló la reunión".*

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/CRIC_Cauca/status/1319485400201777157","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/CRIC\_Cauca/status/1319485400201777157

</div>

</figure>
<!-- /wp:core-embed/twitter -->
