Title: Plan de Gobierno para frenar deforestación en Amazonía ha sido ineficaz
Date: 2019-04-16 18:09
Author: CtgAdm
Category: Ambiente, Política
Tags: Amazonía colombiana
Slug: plan-de-gobierno-para-frenar-deforestacion-en-amazonia-ha-sido-ineficaz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/Captura-de-pantalla-62.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Ministerio de Ambiente] 

La semana pasada, se realizó en el Congreso un debate de control político sobre la tala indiscriminada de bosques en la Amazonía, por medio del cual, representantes y ambientalistas criticaron fuertemente a las políticas del Gobierno nacional que hasta el momento ha fallado en producir resultados. Según estimaciones de ambientalistas, en 2018 se **perdieron cerca de 200.000 bosques en la Amazonía colombiana** mientras que en el 2017 se registraron cerca de 144.000 hectáreas deforestadas.

Ambientalistas piden que el Gobierno declare un estado de emergencia por la tasa acelerada de deforestación en esta región del país, sin embargo, la respuesta del Gobierno ha sido de diseñar estrategias sin consultar las comunidades afectadas ni examinar la complejidad de la problemática, el cual involucra numerosos actores y factores, y que en consecuencia, ofrecen pobres resultados.

### Consejo Nacional de Lucha contra la deforestación

Como lo anunció el Ministro de Ambiente Ricardo Lozano, la creación de un Consejo Nacional, encargado de “**desincentivar el accionar de los grupos y personas involucrados en la deforestación del país**”, sería una de las principales estrategias del Gobierno para frenar la pérdida de estos ecosistemas de la Amazonía. A pesar de haber sido presentado al público por primera vez en septiembre de 2018, aún no se conoce muchos detalles sobre este iniciativa, que estaría incluida en un artículo del nuevo Plan Nacional de Desarrollo (PND) que propone el Gobierno del Presidente Iván Duque.

Sin embargo, el Gobierno ha informado que este consejo sería conformado por los ministerios de Ambiente, Defensa, Justicia, Minas, la Procuraduría General de la Nación, Fiscalía General de la Nación, el Comando General de las Fuerzas Militares de Colombia y la Policía Nacional. Además,  el Ministro indicó que se había catalogado la falta de agua, la pérdida de la biodiversidad y la degradación del medio ambiente **como un problema de Seguridad Nacional por medio de la nueva política de defensa y seguridad de las Fuerzas Públicas**.

Al respecto, **Camilo Prieto, vocero del Movimiento Ambientalista Colombiano**, sostiene que si bien se requiere una estrategia militar para combatir las actividades de actores operando en la ilegalidad en la Amazonía, también se necesita subrayar la importancia de la participación de la Fiscalía y en especial, la Procuraduría en estos procesos de conservación de la Amazonía. Prieto explica que la reciente denuncia de un representante del Partido Centro Democrático quien se apropió de un terreno baldío de 7.000 hectáreas en Vichada evidencia la necesidad de el accionar de la Procuraduría. (Le puede interesar: "[Las mafias tras los incendios en la Amazonía](https://archivo.contagioradio.com/las-mafias-tras-los-incendios-la-amazonia/)")

### **Visión Amazonía**

En el 2016, el Gobierno Nacional anunció la creación de la iniciativa **Visión Amazonía que tenía como su objetivo reducir a cero la deforestación en la Amazonía colombiana para el año 2020**. Sin embargo, la tasa de tala de árboles sigue en aumento y a finales de 2018, el Instituto de Hidrología, Meteorología y Estudios Ambientales (IDEAM) reportó que el 75 por ciento de la deforestación en el país se concentra en la Amazonía. (Le puede interesar: "[Deforestación sigue aumentando en la Amazonía](https://archivo.contagioradio.com/deforestacion-siguen-aumentando-la-amazonia-colombiana/)")

Este programa además cuenta con el apoyo de los gobiernos de Noruega, Alemania y Reino Unido y con un presupuesto de **cerca de 85 millones de dólares**, lo cual lleva a Prieto concluir que no es suficiente que el Gobierno reciba inversiones de grandes dimensiones si no está dispuesto a responder a las complejidades del problema."**No es solo recursos sino como se invierten estos recursos.** Hay que ser enfáticos en un asunto y es que le ilegalidad y los mercados de la ilegalidad son los que se están apropiando de estos espacios".

Por tal razón, el ambientalista indica que se debería revisar las cifras que presenta el programa del Gobierno y si no se están dando los resultados esperados, se tendría que reevaluar esta estrategia. A esto se suma un llamado de los congresistas del Partido Liberal a la Procuraduría de investigar Visión Amazonía.

### Meta de deforestación en el Plan Nacional de Desarrollo

Tal como se había denunciado desde sectores ambientalistas, el Plan Nacional de Desarrollo incluye una **meta de deforestación de 220.000 hectáreas por año, es decir, el Gobierno Nacional toleraría la tala de 880.000 hectáreas de bosques** en el país durante su cuatro años de gobernación. "[Nosotros asumimos que esa línea base que está tomando el Gobierno como una claudicación ante las mafias de la deforestación y por otro lado, una decisión irresponsable que no está comprendiendo que la el mayor problema ambiental que tiene Colombia en este instante es justamente la deforestación", señaló el vocero del Movimiento Ambientalista.]

Como lo denunció la organización Dejusticia, la meta actual del PND va en contra vía del Acuerdo de Paris, del compromiso de alcanzar una tasa de cero deforestación en la Amazonia colombiana a 2020 como lo planteaba Visión Amazonía y de la sentencia 4360 de 2018 del Consejo de Estado que declara a la Amazonía sujeto de derechos.

Por otro lado, Prieto sostiene que si el Gobierno mantiene esta meta de deforestación, estrategias para preservar la Amazonía, como el Consejo Nacional, solo servirían como un espacio burocrático con poco acción y pobres resultados. Por lo tanto, organizaciones ambientalistas le han solicitado al Gobierno Nacional que modifique la meta de deforestación.

<iframe id="audio_34570263" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_34570263_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
