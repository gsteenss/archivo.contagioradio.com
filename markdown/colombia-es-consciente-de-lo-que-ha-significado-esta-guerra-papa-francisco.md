Title: "Colombia es consciente de lo que ha significado esta Guerra": Papa Francisco
Date: 2015-09-21 13:34
Category: Nacional, Paz
Tags: Angelus del domingo en Cuba, Cuba, Delegados de paz de las FARC, Papa, Plaza de la, proceso de paz, Visita a Cuba
Slug: colombia-es-consciente-de-lo-que-ha-significado-esta-guerra-papa-francisco
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/papa.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: RadioHabanaCuba 

###### 21 sep 2015

En la tradicional misa del Angelus, esta vez desde Cuba, en donde también se encuentra la delegación de paz del Gobierno y las FARC, el Papa hizo una mención especial por Colombia pidiendo porque el proceso de paz llegue a feliz término y **la reconciliación sea uno de los pilares fundamentales**.

“*No tenemos derecho a permitirnos otro fracaso más en este camino de paz y reconciliación*”, dijo el Papa, siendo este uno de los mensajes más contundentes en la intervención que congregó a miles de cubanos en la Plaza de la Revolución.

También el Sumo Pontífice se refirió a las **décadas de conflicto en Colombia y a los tantos que en búsqueda de la paz han perdido la vida** “Que la sangre vertida por miles de inocentes durante tantas décadas de conflicto armado, unida a aquella del Señor Jesucristo en la Cruz, sostenga todos los esfuerzos que se están haciendo”, dijo el Papa.

Por otra parte reconoció que "**Colombia es consiente de lo que ha significado esta guerra y que hoy en un esfuerzo renovado **y movido por la esperanza" el pueblo colombiano busque “construir una sociedad en paz” .

El dÍa de hoy el Papa se encuentra en Holguín y mañana ofrecería una misa en Santiago de Cuba antes de viajar a Estados Unidos.  
Esta es la agenda del Papa de lo que queda de visita a Cuba y lo que será la visita a EE.UU:

#### **Lunes, 21 de septiembre** 

#### 8:00 Salida desde el aeropuerto José Martí hacia Holguín. 

#### 9:20 Llegada al aeropuerto Frank País, en Holguín. 

#### 10:30 Santa Misa en la Plaza de la Revolución en Holguín. 

#### 15:45 Francisco bendecirá la ciudad desde la Loma de la Cruz. 

#### 16:40 Salida desde el aeropuerto de Frank País hacia Santiago de Cuba. 

#### 17:30 Llegada al aeropuerto Antonio Maceo, en Santiago de Cuba. 

#### 19:00 Encuentro con obispos en el Seminario de San Basilio Magno 

#### 19:45 Oración a Nuestra Señora de la Caridad con los obispos y el séquito papal en la basílica menor del Santuario de la Virgen de la Caridad del Cobre, en Santiago 

#### **Martes, 22 de septiembre** 

#### 8:00 Santa Misa en la basílica menor del Santuario de la Virgen de la Caridad del Cobre. 

#### 11:00 Encuentro con familias en la catedral de Nuestra Señora de la Asunción y bendición de la ciudad de Santiago. 

#### 12:15 Ceremonia de despedida en el aeropuerto Antonio Maceo, y salida hacia Washington D.C. 

#### 16:00 Llegada a Washington D.C. y recibimiento en la Fuerza Aérea de Andrews. 

#### **Miércoles, 23 de septiembre** 

#### 9:15 Ceremonia de bienvenida en el Jardín Sur de la Casa Blanca, donde Francisco pronunciará un discurso y visitará al Presidente de Estados Unidos. 

#### 11:00 Encuentro con obispos de Estados Unidos en la Catedral de San Mateo. 

#### 16:15 Santa Misa para la canonización de Fray Junípero Serra en el Santuario Nacional de la Inmaculada Concepción. 

#### **Jueves, 24 de septiembre** 

#### 9:20 El Papa visita el Congreso de los Estados Unidos. 

#### 11:15 Encuentro con personas sin hogar en el centro caritativo de la parroquia de San Patricio. 

#### 16:00 Salida en avión desde Washington D.C. hacia Nueva York. 

#### 17:00 Llegada al aeropuerto JFK de Nueva York. 

#### 18:45 Francisco celebra las Vísperas con sacerdotes, religiosos y religiosas en la Catedral de San Patricio. 

 

#### **Viernes, 25 de septiembre** 

#### 8:30 Francisco pronuncia un discurso en la sede de las Naciones Unidas en Nueva York. 

#### 11:30 El Papa participa en un encuentro interreligioso en el Ground Zero Memorial. 

#### 16:00 Francisco visita la escuela Nuestra Señora Reina de los Ángeles y se reúne con familias de inmigrantes en Harlem. 

#### 18:00 Santa Misa en el Madison Square Garden. 

 

#### **Sábado, 26 de septiembre** 

#### 8:40 Salida desde Nueva York hacia Filadelfia. 

#### 9:30 Llegada al Aeropuerto Internacional de Filadelfia. 

#### 10:10 Santa Misa con obispos, clero, religiosos y religiosas de Pennsylvania en la Catedral de los Santos Pedro y Pablo en Filadelfia. 

#### 16:45 Encuentro para la libertad religiosa con la comunidad hispana y otros inmigrantes en el Centro Comercial Independencia en Filadelfia. 

#### 19:30 Fiesta de las Familias y vigilia de oración en el B. Franklin Parkway en Filadelfia. 

 

#### **Domingo, 27 de septiembre** 

#### 9:15 Encuentro con los obispos invitados al Encuentro Mundial de las Familias en el Seminario St. Charles Borromeo en Filadelfia 

#### 11:00 Visita a los presos del Instituto Correccional Curran-Fromhold en Filadelfia 

#### 16:00 Santa Misa para clausurar el octavo Encuentro Mundial de las Familias en el B. Franklin Parkway en Filadelfia 

#### 19:00 Encuentro con el Comité organizador, los voluntarios y benefactores en el Aeropuerto Internacional de Filadelfia 

#### 19:45 Ceremonia de despedida y regreso a Roma 

 

#### **Lunes, 28 de septiembre** 

#### 10:00 Llegada al aeropuerto de Ciampino en Roma 

 
