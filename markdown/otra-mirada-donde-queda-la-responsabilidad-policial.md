Title: Otra Mirada: ¿Dónde queda la responsabilidad policial?
Date: 2020-09-18 14:18
Author: AdminContagio
Category: Otra Mirada, Programas
Tags: Otra Mirada, policia nacional
Slug: otra-mirada-donde-queda-la-responsabilidad-policial
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/09/WhatsApp-Image-2020-09-10-at-23.50.49.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: Contagio Radio/Andres Zea

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Las protestas que se presentaron en el país, debido al asesinato del abogado Javier Ordóñez a manos de agentes de la policía y los demás abusos cometidos por esa fuerza y que dejaron 13 personas muertas por el accionar de la entidad al disparar contra la ciudadanía. Estos actos, además, de las afirmaciones de la alcaldesa sobre el proceder de la policía hacen que se cuestione quién dio la orden de disparar contra los civiles y a quién se le otorga la responsabilidad por los actos. (Le puede interesar: [Cinco normativas que deben cambiarse para frenar delitos de la Policía](https://archivo.contagioradio.com/cinco-normativas-que-deben-cambiarse-para-frenar-delitos-de-la-policia/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Durante el programa de Otra Mirada sobre los abusos de poder por parte de la policia y los responsables de estos actos estuvieron Jorge Restrepo, director del Centro de investigación y estudios sobre conflictos armados, violencia armada y desarrollo (CERAC), Alirio Uribe, abogado y defensor de derechos humanos y Heidy Sánchez, concejala de Bogotá. 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Los invitados señalaron explicaron qué tan posible es que se encuentre y se determine quién dio la orden de disparar. 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Además, con relación al rol y las declaraciones de la alcaldesa Claudia López los invitados comparten que es cuestionable la responsabilidad y el control que tiene la alcaldesa de la Policía de Bogotá, señalando que al parecer se le obedece para reprimir las protestas pero no para evitar que se dispare con armas de fuego a civiles. 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Igualmente, explicaron cómo es el uso de las armas en movilizaciones y si es legal que los policías disparen cuando lo consideren necesario.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El abogado Alirio también señala que los pasos a seguir para que no se repitan estos hechos es cambiar la doctrina de la policia, del enemigo interno y las declaraciones que pueden inspirar a los policias a violar los derechos humanos. (Si desea escuchar el programa del 11 de septiembre: [Otra Mirada: Un pacto por la vida y la paz surge desde las comunidades](https://www.facebook.com/contagioradio/videos/353632849375956).)

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Si desea escuchar el programa completo [Haga click aquí](https://www.facebook.com/contagioradio/videos/247797646496230)

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
