Title: “Acá y donde sea estaremos luchando por los derechos de las comunidades” Wilmar Madroñedo
Date: 2015-06-10 10:57
Category: DDHH, Nacional
Tags: Asiagro, Captura irregular campesino ASIAGRO, Fensuagro, marcha patriotica, Mesa de Organizaciones Sociales del Putumayo, Putumayo, Wilmar Madroñedo
Slug: aca-y-donde-sea-estaremos-luchando-por-los-derechos-de-las-comunidades-wilmar-madronedo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/06/madroñedo.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: contagioradio.com 

######  <iframe src="http://www.ivoox.com/player_ek_4622303_2_1.html?data=lZuflJiUd46ZmKialJaJd6KplZKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRkcLY09SSpZiJhpLZxdSSlKiPsIa3lIqupsnJtoztjMnSyMrSt9DmjMnSjanJtsbXydTgja3ZscLi0NiYx9OPqc2hhpywj6jTstXVyM7cjbfFqMrjjJKSmaiReA%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe> 

##### [Wilmar Madroñedo, líder y defensor de Derechos Humanos.] 

###### [10 Jun 2015] 

“Para mi no fue una sorpresa que me detuvieran” afirma Wilmar Madroñedo, líder y defensor de Derechos Humanos en el departamento de Putumayo, porque sabían que detenerlos y judicializarlos es una manera de perseguir y desarticular la movilización, después del Paro Agrario. Wilmar fue detenido y judicializado desde el 23 de Octubre de 2013 y apenas hace algunas semanas recobró su libertad al demostrarse su inocencia.

Durante los 19 meses que duró su detención Wilmar se convirtió en un referente para los  reclusos de la cárcel de mediana seguridad de Mocoa, y en tan sólo 4 meses fue nombrado vocero de los internos. Lograron que se mejoraran las condiciones de alimentación y salud, y que muchos internos pudieran seguir pagando sus condenas en detención domiciliaria porque “nos convertimos en tinterillos” señala Madroñedo.

Su experiencia de judicialización resulta ser el ejemplo de la persecución judicial contra líderes campesinos, indígenas y afrodescendientes en el Putumayo, pero también se convierte en ejemplo del testimonio de las y los defensores de DDHH en Colombia, que a pesar de la persecución siguen apostándole a la vida con dignidad en los territorios.
