Title: Nueve comandantes del Ejército estarían implicados en falsos positivos: Human Rights Watch
Date: 2019-02-27 18:27
Category: DDHH, Nacional
Tags: Ejecuciones Extrajudiciales, ejercito, falsos positivos
Slug: nueve-comandantes-del-ejercito-estarian-implicados-en-falsos-positivos-human-rights-watch
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/03/Falsos-positivos.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: AFP] 

###### [27 Feb 2019] 

Nueve generales, que podrían estar involucrados en casos de ejecuciones extrajudiciales, fueron nombrados en posiciones claves en el Ejército, en diciembre del año pasado, según un informe de la organización Human Rights Watch. Uno de los señalados es **el actual comandante del Ejército, Nicacio de Jesús Martínez Espinel, vinculado presuntamente a ejecuciones extrajudiciales cometidos por soldados bajo su mando. **

Asimismo, según[el informe](https://www.hrw.org/es/news/2019/02/27/colombia-nuevos-comandantes-del-ejercito-estarian-vinculados-con-falsos-positivos), Human Rights Watch conoce de "evidencias creíbles" en contra de los nueve militares. Actualmente, la Fiscalía General de la Nación investiga a tres de ellos  — **Miguel Eduardo David Bastidas,** **Diego Luis Villegas Muñoz y Jorge Enrique Navarrete Jadeth  — **por presuntos ejecuciones extrajudiciales. Los otro seis militares no están bajo investigación, sin embargo, soldados que fueron sus subordinados si lo están o ya han sido condenados por ejecuciones extrajudiciales.

Según José Miguel Vivanco, director para las Américas de Human Rights Watch, "al nombrar a estos generales, el gobierno transmite a las tropas el preocupante mensaje de que cometer abusos puede no ser un obstáculo para avanzar en la carrera militar". Además afirmó que "las autoridades colombianas deberían impulsar investigaciones serias contra los generales creíblemente implicados en falsos positivos y no designarlos en los puestos más importantes del Ejército."

### **Los ascensos ** 

El pasado 10 de diciembre, el Ejército Nacional anunció el nuevo comandante del Ejército como Nicacio de Jesús Martínez Espinel, quien fue comandante de la Décima Brigada Blindada entre octubre de 2004 y enero de 2006. Durante este tiempo, fueron cometidos al menos de **23 ejecuciones por los soldados de esta brigada, es decir, subordinados de Martínez**. (Le puede interesar: [Nueva cúpula militar: ¿Vuelve la seguridad democrática?](https://archivo.contagioradio.com/nueva-cupula-militar-vuelve-la-seguridad-democratica/)")

Luego, el 21 de diciembre,  el Comando del Ejército Nacional designó a ocho de estos generales a cargos en el Estado Mayor. En rondas de ascensos de años previos, cada uno de los militares fueron señalados por la Human Rights Watch de estar posiblemente vinculados con casos de ejecuciones extrajudiciales, es decir, **el Ejército ya tendría conocimiento sobre los señalamientos en contra de los militares**.

En estos casos, los generales fueron comandantes de unidades a las que se atribuye una cantidad significativa de ejecuciones, e igualmente, hay varios procesos en donde los entonces comandantes firmaron documentos que aprobaban operaciones o pagos de informantes que realizaron ejecuciones extrajudiciales.

El informe sostiene que, de acuerdo con el derecho internacional, los comandantes "deben ser penalmente responsables **si sabían o deberían haber sabido que subordinados bajo su control efectivo estaban cometiendo estos delitos, pero no adoptaron todas las medidas necesarias y razonables a su alcance para prevenir o castigar estos actos**."

### **Los militares implicados** 

Los militares nombrados son**: Nicacio de Jesús Martínez Espinel**, comandante del Ejército; **Jorge Enrique Navarrete Jadeth**, jefe de la Jefatura de Estado Mayor; **Raúl Antonio Rodríguez Arévalo**, jefe de la Jefatura de Estado Mayor de Planeación y Políticas; **Adolfo León Hernández Martínez**, comandante del Comando de Transformación del Ejército del Futuro; **Diego Luis Villegas Muñoz**, comandante de la Fuerza de Tarea Vulcano; **Edgar Alberto Rodríguez Sánchez**, comandante de la Fuerza de Tarea Aquiles; **Raúl Hernando Flórez Cuervo**, comandante del Centro Nacional de Entrenamiento; **Miguel Eduardo David Bastidas**, comandante de la Décima Brigada Blindada; y **Marcos Evangelista Pinto Lizarazo**, comandante Décima Tercera Brigada.

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
