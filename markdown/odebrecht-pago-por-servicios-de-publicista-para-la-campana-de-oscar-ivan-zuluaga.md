Title: Los escándalos de corrupción que salpican al Centro Democrático
Date: 2017-01-31 16:25
Category: Judicial, Nacional
Tags: Alvaro Uribe, Odebrecht, oscar ivan zuluaga, Sobornos
Slug: odebrecht-pago-por-servicios-de-publicista-para-la-campana-de-oscar-ivan-zuluaga
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/01/9412794898_01d1a3a4a7_o.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: La Silla Vacía] 

###### [31 Ene. 2017] 

Son varios los escándalos en los que se ha visto envuelto el partido Centro Democrático, Odebrecht, el caso por las interceptaciones ilegales hechas por el hacker Sepúlveda, Reficar y varios de sus integrantes salpicados por corrupción son apenas algunos hechos recientes que han enfrentado.

### **Odebrecht y Óscar Iván Zuluaga** 

Siguen destapándose hechos de corrupción en Colombia por el caso Odebrecht, esta vez con la información entregada por el publicista José Eduardo Cavalcanti conocido como “Duda”, quien manifestó que **hubo una financiación de la campaña política de Óscar Iván Zuluaga en Brasil y en Latinoamérica.**

José Eduardo “Duda”, asesoró en el año 2014 la campaña de Óscar Iván Zuluaga, integrante del Partido Político Centro Democrático. Según las declaraciones dadas por “Duda” a la Fiscalía brasileña y reveladas por el medio de comunicación Veja, **el publicista había pedido a Zuluaga la suma de 4.3 millones de dólares por sus servicios**, valor que no fue en principio aceptado por la campaña.

Sin embargo, para poder contar con los servicios del publicista en dicha campaña, **Odebrecht se ofreció a pagar los gastos de José Eduardo Cavalcanti. Aportando en total 2.859 millones de dólares.**

Así mismo, Cavalcanti contó haberse reunido en São Paulo con el hijo del candidato Óscar Iván Zuluga, David Zuluaga y con una persona directiva de Odebrecht, fue allí cuando solicitó los 4.3 millones de dólares.

El integrante del Centro Democrático y candidato presidencial en 2014, Óscar Iván Zuluaga, dijo que él no puede hacerse responsable de la contabilidad de su campaña y de los acuerdos de pago con Odebrecht y negó todo lo relatado por “Duda”. Pese a ello, **“Duda” manifestó que “todo está en la contabilidad, con soportes de pago y registros de giros al exterior”.**

Cabe recordar que hace cerca de un mes, Zuluaga reconoció conocer del trabajo de Cavalcanti por recomendación de Odebrecht, además del viaje que en 2014 realizó su hijo a São Paulo para reunirse con Cavalcanti. Le puede interesar: [Odebrecht la punta del Iceberg de la corrupción en Colombia](http://bit.ly/2k14qoX)

### **Hacker Andrés Sepúlveda ** 

Además del caso Odebrecht, la campaña presidencial del integrante del Centro Democrático ha estado rodeada de actuaciones “delictivas”, así lo dijo la Fiscalía el 12 de Enero de 2017 luego de su decisión de archivar la investigación por el caso del hacker Sepúlveda. Le puede interesar: [Álvaro Uribe rehén de su propia táctica](https://archivo.contagioradio.com/alvaro-uribe-rehen-de-su-propia-tactica/)

Para el ente, en **la campaña de Zuluaga “varias personas se encargaron de cometer delitos con el propósito de atacar el proceso de paz en el año 2014”** que se llevaba a cabo en La Habana entre las FARC y el Gobierno Nacional.

En la actualidad son investigados por la campaña presidencial de Óscar Iván Zuluaga, David Zuluaga, hijo del ex candidato presidencial, el senador Álvaro Uribe Vélez y Luis Fernando Hoyos, exasesor espiritual del Centro Democrático. Le puede interesar: [Luis Alfonso Hoyos será juzgado en Colombia a pesar de estar prófugo](https://archivo.contagioradio.com/a-pesar-de-estar-profugo-luis-alfonso-hoyos-sera-juzgado-en-colombia/)

Ya en 2014 el periodista Daniel Coronell había denunciado a través de una de sus columnas de opinión, haber encontrado una diferencia de 1.400 millones de pesos con base en lo que en su momento Óscar Iván Zuluaga había asegurado costó la asesoría.

### **Reficar** 

El 4 de mayo, en medio de un debate de control político se dio a conocer que sobrecostos en la ampliación de la Refinería de Cartagena (Reficar) son consecuencia de la corrupción. En ese momento el senador Jorge Robledo del Polo democrático dijo que existían dos tipos de responsabilidades.

Una  de tipo penal, cuya sanción depende de la Contraloría y la Procuraduría, y otra política, en la que se hizo **evidente que el 50% compete al gobierno Uribe y el restante, a Santos**. Le puede interesar: [Pérdidas en Reficar equiparables al costo del metro para Bogotá](https://archivo.contagioradio.com/perdidas-en-reficar-equiparables-al-costo-del-metro-para-bogota/)

Este proyecto ha sido considerado por la Contraloría como el que más dinero le ha costado a Colombia en toda su historia, pues se calculan que por lo menos 4 mil millones de dólares de sobrecostos no podían ser justificados por el gobierno de esa época. Le puede interesar: Infografía: [El despilfarro financiero de Reficar](https://archivo.contagioradio.com/el-despilfarro-de-reficar/)

### **Otros integrantes del Centro Democrático implicados** 

Por el escándalo de los sobornos de Odebrecht el 14 de enero fue enviado a la cárcel **Gabriel García, exviceministro de Transporte en el gobierno de Álvaro Uribe Vélez**, acusado por cohecho impropio, interés indebido en la celebración de contratos y enriquecimiento ilícito.

Según lo dijo la Fiscalía, **García pidió el pago de 6.5 millones de dólares** para hacer que Odebrecht fuera la única habilitada para la licitación del tramo II de la Ruta del Sol.

Así mismo, el 15 de Enero se cumplió la legalización de la **captura del exsenador Otto Bula,** reemplazo de Mario Uribe -primo del senador Álvaro Uribe- condenado por parapolítica.

La investigación que se lleva en contra de Bula es por la adición de un contrato que había para que se construyera la Ruta del Sol tramo II, que tuvo un valor de casi \$800 mil millones de pesos.

La Fiscalía General en Colombia asegura haber establecido que Odebrecht hizo varios pagos para obtener contratos, por lo que ya se han abierto varias investigaciones.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)

<div class="ssba ssba-wrap">

</div>
