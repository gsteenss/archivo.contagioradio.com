Title: Corporalidad, género y otros grandes temas de Cinemigrante 2016
Date: 2016-04-24 11:00
Author: AdminContagio
Category: 24 Cuadros, eventos
Tags: Cinemigrante, DDHH, migraciones
Slug: corporalidad-genero-y-otros-grandes-temas-de-cinemigrante-2016
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/04/ENCABEZADO-WORD-Bogota-2016.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Cinemigrante 

###### [22 Abr 2016] 

La Muestra internacional de cine y formación en derechos humanos de las personas migrantes **"Cinemigrante"**, vuelve a Colombia con una selección de 45 producciones provenientes de diferentes lugares del mundo, invitados internacionales y actividades de formacíón.

Creado en Buenos Aires en 2010, Cinemigrante es en palabras de su directora Florencia Mazzadi "un espacio cinematográfico que intenta **generar una instancia de reflexión colectiva e individual de lo que significa para cada uno de nosotros el movernos por el territorio**, el haber nacido en un espacio territorial y la necesidad de salir en busca de un mejor destino".

El enfoque particular en Derechos Humanos de la población migrante que propone la Muestra, se define por el carácter global que posee la temática mostrando la "unidad de todas esas historias que se van repitiendo a lo largo de los continentes y que simplemente lo que **muestran la necesidad de poder desarrollarnos en igualdad de condiciones** hallamos nacido en el espacio que hallamos nacido" explica la directora.

Las condiciones en que actualmente se dan las migraciones en gran parte del mundo, invitan a la reflexión, planteada por Mazzadi, sobre la esencia de la movilización humana por los territorios "**no es un problema migrar, el problema es que no nos garantizan los derechos cuando migramos**" asegura.

Utilizar al cine como "herramienta de transformación", es una de las apuestas que **Cinemigrante** a realizado desde sus inicios, al notar que dentro del cine hay gran cantidad de producciones que participan en los Festivales más importantes del mundo, donde las identidades de sus realizadores están marcadas por la "**contemporaneidad migratoria**" que no pueden ser abarcadas dentro de una sola procedencia geocultural.

Dentro de las actividades que hacen parte de la programación de la Muestra destaca la presencia de la directora, artista visual, investigadora y docente de la Universidad de Barcelona María Ruido, encargada del seminario de Cine "**Género: memoria y decolonialidad**" y curadora de la temática **“Cuerpos que (im)portan. Territorio y cicatrices”** una sección que da cuenta de **"aquellos cuerpos que al sistema capitalista no le interesa por que no tienen un valor igualitario como corporalidad"** y que tendrá lugar entre el 2 y el 4 de Mayo.

Otra de las presencias destacadas de la presente edición es la del documentalista francés **Joris Lachaise**, encargado de abrir la muestra con su producción 'Ce Qu'il Reste De la Folie, y durante el evento interactuará con los asistentes, incluyendo las dos charlas que sostendrá en las cárceles La Picota y El Buen Pastor.

De manera paralela a la programación regular, se realizará la sección **‘Noches extrañas. Strangers in the Night’**, con presentaciones de media noche donde "los extraños y las extrañas salen a mostrar ese extrañamiento en que a los migrantes se nos coloca" como describe la directora de la muestra.

Cinemigrante inicia este martes 26 de abril desde las 6 p.m., en el auditorio Felix Restrepo de la Universidad Javeriana, sede Bogotá, en una gala que contará con la presencia del músico caribeño Charles King. Hasta el 4 de mayo el evento presentará 54 funciones en la capital para luego desplazarse del 10 al 14 del mismo mes a la ciudad de Medellín donde se realizarán 25 funciones.

###### <iframe src="http://co.ivoox.com/es/player_ej_11271010_2_1.html?data=kpafmZaUdZGhhpywj5aYaZS1lZ2ah5yncZOhhpywj5WRaZi3jpWah5yncafg0NfS0MjNpYzBwt%2Fnw8nNb46fpM7bx9LNq9PVz9nSj4qbh4630NPhw8zNs4zGwsnW0ZCRaZi3jpk%3D&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe> 

[Programación Por Día Cine Migrante Bogotá 2016](https://es.scribd.com/doc/310114729/Programacion-Por-Dia-Cine-Migrante-Bogota-2016 "View Programación Por Día Cine Migrante Bogotá 2016 on Scribd")

<iframe id="doc_63676" class="scribd_iframe_embed" src="https://www.scribd.com/embeds/310114729/content?start_page=1&amp;view_mode=scroll&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="undefined"></iframe>
