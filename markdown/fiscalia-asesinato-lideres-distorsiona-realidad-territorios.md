Title: Postura de la Fiscalía sobre asesinato de líderes distorsiona la realidad de los territorios
Date: 2020-01-21 17:24
Author: CtgAdm
Category: Entrevistas, Líderes sociales
Tags: asesinato de líderes sociales, Fiscalía, Negacionismo
Slug: fiscalia-asesinato-lideres-distorsiona-realidad-territorios
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/01/Fiscalía.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: Fiscalía de Colombia

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Mientras organizaciones defensoras de DD.HH. como Indepaz han denunciado el asesinato de 23 lideres sociales en lo corrido del 2020, es decir, el homicidio de casi una persona por día, para la Fiscalía, solo se ha asesinado a uno en enero, justificando que los casos son materia de investigación por lo que hasta entonces “no se les puede asignar la calidad de líder social”, declaraciones que desconocen los testimonios de quienes convivieron y conocían el trabajo de quienes han muerto por defender la paz en su territorio. [(Lea también: Colombia inicia el año con aumento de violencia contra líderes sociales)](https://archivo.contagioradio.com/colombia-inicia-el-ano-con-aumento-de-violencia-contra-lideres-sociales/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

El director del [Instituto de Estudios para el Desarrollo y la Paz (Indepaz)](http://www.indepaz.org.co/), Camilo González Posso, se refirió a las declaraciones del fiscal (e) Fabio Espitia, quien solo reconoció el asesinato de **la lideresa Gloria Ocampo el pasado 7 de enero, en Puerto Guzmán, Putumayo, cuando resulta curioso que no sean visibilizados otros casos que han sucedido en el mismo municipio, la mayoría contra miembros de Marcha Patriótica.** [(Lea también: Más de 200 integrantes de Marcha Patriótica han sido asesinados entre 2011 y 2020)](https://archivo.contagioradio.com/asesinatos-marcha-patriotica-2011-2020/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Según la misión de Naciones Unidas de la ONU, el 98% de los 250 asesinatos registrados en 2019 contra defensores de DD.HH. ocurrieron en lugares donde operan grupos criminales o armados con economías ilegales, de estos, la mitad ocurrieron en los departamentos de **Antioquia, Arauca, Cauca y Caquetá**, mientras que en 2020 a esta cifra se suman los departamentos de **Putumayo, Córdoba, Norte de Santander, y Chocó** . [(Le puede interesar: Asesinan a lideresa Gloria Ocampo en Putumayo)](https://archivo.contagioradio.com/asesinan-a-lideresa-gloria-ocampo-en-putumayo-2/)

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/PCatatumbo_FARC/status/1219282391111520258","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/PCatatumbo\_FARC/status/1219282391111520258

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:heading {"level":3} -->

### Se responde con una política de guerra

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Las declaraciones del fiscal encargado - señala, son una lectura equivocada que distorsionan la realidad de los territorios pues - no coinciden siquiera con las de las Naciones Unidas que para el 14 de enero había destacado el asesinato de 10 líderes sociales, "es un muy mal mensaje cuando lo que se necesita es una revaluación de políticas no solo en investigación sino judiciales y preventivas de fondo por parte del Gobierno.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Para González Posso es importante resaltar el hecho que buena parte de quienes han sido atacados ha sido por realizar la implementación del Acuerdo en su territorio o apoyar la sustitución de cultivos, y eso no se reconoce, en cambio hemos tenido anuncios sobre la aplicación de otras fuerzas que colocan en el centro de todo políticas represivas de guerra en lugar de dar respuestas a las demandas de los productores que están inscritos en las políticas de sustitución.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### ¿Por dónde empezar a vencer la guerra?

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

El director de Indepaz recalca que en la actualidad es necesario comprender que si bien no existen recetas precisas para detener el asesinato de líderes sociales, sí hay criterios entre ellos el de reconocer que el país se encuentra en una transición al posconflicto y que por lo tanto **"las políticas de guerra de hace 10 años son contraproducentes"**.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

El resultado de este accionar se ve con mayor preocupación en zonas donde pese al incremento en la violencia "no existe una mirada hacia atrás, por el contrario, es una expresión de movilización ciudadana en rechazo a la presencia de grupos armados, la corrupción y en general del regreso a la guerra", por lo que indica, se requiere un cese en la polarización desde las entidades como la Fiscalía que promueve la estigmatización y el negacionismo.

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
