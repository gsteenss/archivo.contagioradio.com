Title: A 33 años, aún no hay justicia para las víctimas de tortura del Palacio de Justicia
Date: 2018-04-18 11:54
Category: DDHH, Nacional
Tags: Fuerzas militares, Palacio de Justicia, Retoma del Palacio de Justicia, tortura, Víctimas del Palacio de justicia
Slug: a-33-anos-aun-no-hay-justicia-para-las-victimas-de-tortura-del-palacio-de-justicia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/01/palacio-de-justicia-e1517241421149.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo] 

###### [18 Abr 2018] 

Luego de que la Fiscalía General de la Nación citara a indagatoria a **siete militares en retiro por hechos de tortura** cometidos durante la retoma del Palacio de Justicia el 6 y 7 de noviembre de 1985, la defensa de las víctimas espera que no haya más dilaciones en el proceso y que la justicia actué de la forma adecuada.

De acuerdo con Eduardo Carreño, abogado de las víctimas del Palacio de Justicia e integrante del Colectivo de Abogados José Alvear Restrepo, la Corte Interamericana de Derechos Humanos **condenó al Estado colombiano por casos de tortura**, específicamente en los hechos cometidos contra Yolanda Santodomingo y Eduardo Matson. De esa investigación “se determinó que por lo menos 11 personas se han atrevido a denunciar y planear como fueron torturados por miembros del Ejército o la Policía”.

Por esto, “la Fiscalía delegada ante la Corte citó a indagatoria **desde el 2015 a 14 militares** de los cuales 7 ya han rendido su respectiva diligencia de descargos”. De esos militares están pendientes 7 que son Ferney Causayá Peña, Luis Fernando Nieto Velandia, Fernando Blanco Gómez, Bernardo Alfonso Garzón Garzón, Justo Eliseo Peña Sánchez, Siervo Antonio Buitrago Téllez y Antonio Rubay Jiménez Gómez.

### **Hay pruebas para imponer medida de aseguramiento a militares** 

Carreño indicó que hay pruebas de que estas personas participaron “en este tipo de tratamiento a las personas **que se encontraban en el Palacio de Justicia**”. Afirmó que cuando se terminen las diligencias que se esperan sean realizadas en Junio, “el proceso entra a calificar su situación jurídica, es decir, si se les impone la medida de aseguramiento o no”. (Le puede interesar:["¿Quién responde por las entregas equivocadas del Palacio de Justicia?"](https://archivo.contagioradio.com/quien-responde-por-las-entregas-equivocadas-del-holocausto-palacio-de-justicia/))

Ante esto, la defensa argumentó que “existen pruebas suficientes para demostrar estos hechos que **deben ser cobijados con medidas de detención**”. Sin embargo, recordó que la pena que estaba asignada para este delito “es de máximo 3 años, es decir que no había privación de la libertad”. Recordó que es importante que se dicte la medida “porque lo delitos se cometieron”.

### **33 años después aún hay impunidad sobre hechos del Palacio de Justicia** 

Estas diligencias ocurren 33 años después de los hechos en acciones que los abogados denominan como una **dilación en el proceso** y porque hubo una petición de los representantes de las víctimas ante la Corte Interamericana. Por esto, “hasta ahora se abre el proceso, o si no, no lo abren, aquí la administración de justicia no hace nada”.

Finalmente, el abogado afirmó que **“son contadas”** las víctimas del Palacio de Justicia que han recibido tratamientos psicológicos y acompañamiento por parte del Estado. Recalcó que las víctimas han tenido que realizar estos tratamientos por su parte y aún así, se han atrevido a denunciar los hechos de tortura a los que fueron sometidos.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 

<div class="osd-sms-wrapper">

</div>
