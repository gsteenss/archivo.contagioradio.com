Title: Fiscalía no ha entregado los restos hallados de desaparecidos del Palacio de Justicia
Date: 2015-12-14 14:36
Category: Judicial, Nacional
Tags: Alfonso Plazas Vega, Cristina del Pilar Guarín, Desaparecidos en Colombia, FFMM, M-19, Palacio de Justicia, René Guarín
Slug: fiscalia-no-ha-entregado-los-restos-hallados-de-desaparecidos-del-palacio-de-justicia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/10/Palacio-de-Justicia.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Contagio Radio 

<iframe src="http://www.ivoox.com/player_ek_9721904_2_1.html?data=mpyfk56UeI6ZmKiakp6Jd6Knl5KSmaiRdo6ZmKiakpKJe6ShkZKSmaiRisrnxMbZh6iXaaK4wpDb0ZDMpYzZz9nfx8zFqNCfzdTgjdfJt9Xj1JDVw9HQpcXj1JDRx5DIqdTV0caah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [René Guarín] 

###### [12 Dic 2015] 

Para los familiares de Luz Mary Portela de Leon, Cristina del Pilar Guarín y Lucy Amparo Ovideo resulta inexplicable que la fiscalía aún no les haya entregado los restos de las tres personas, [luego de casi dos meses del anuncio el pasado 20 de Octubre](https://archivo.contagioradio.com/habrian-hallado-restos-de-3-de-los-desaparecidos-del-palacio-de-justicia/). Según René Guarín, hermano de Cristina,  desde el anuncio no ha habido ninguna comunicación adicional por parte del ente investigador en cuanto a la entrega de los restos.

Guarín afirma que la fiscalía simplemente no ha tenido una comunicación y recuerda el caso de las dos guerrilleras del M-19 de las que se hallaron los restos según el anuncio de la fiscalía, ha pasado un año y tampoco se ha hecho una entrega efectiva.

“El llamado que yo hago a la fiscalía es para que hagan las gestiones para una entrega digna” y para una determinación de la verdad “sobre la cabeza de mando” que terminó con la muerte de Cristina del Pilar Guarin. Además hizo el llamado a la fiscalía delegada para que agilicen los trámites necesarios para que se entreguen los restos de manera digna.

Lo que habría que determinar, insiste Guarín, es acerca de la participación de la Escuela de Caballería y del Coronel Alfonso Plazas Vega en los crímenes cometidos. Frente a ello el hermano de Cristina advierte que aún no se conoce la sentencia de la CSJ, pero será una sentencia que hay que respetarla y no repetir el esquema del rechazo a las decisiones de las cortes. Sin embargo recuerda que la desaparición forzada se da en una serie de etapas en las que todas pruebas implican la participación de las FFMM.

Para el familiar la decisión de la CSJ puede que no se conozca este fin de semana, pero afirma que las tropas de la escuela de caballería "entregaban a las personas señaladas" es decir que del Palacio de Justicia salían "fichadas" y [por lo menos esa unidad militar es responsable porque es copartícipe de las desapariciones](https://archivo.contagioradio.com/14-militares-a-indagatoria-por-tortura-en-retoma-del-palacio-de-justicia/).
