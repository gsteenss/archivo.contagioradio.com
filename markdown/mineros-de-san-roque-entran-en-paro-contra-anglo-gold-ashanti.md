Title: Mineros de San Roque entran en paro contra Anglo Gold Ashanti
Date: 2017-09-11 13:21
Category: DDHH, Nacional
Tags: paro minero, San Roque
Slug: mineros-de-san-roque-entran-en-paro-contra-anglo-gold-ashanti
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/09/SAN-ROQUE-90-e1505152851155.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Página web municipio de San Roque] 

###### [11 Sept 2017] 

La privatización de los predios en los que han trabajado desde hace más de 30 años, la parálisis en los locales de fundidoras de oro y **la persecución de la policía que podría encarcelar a las personas que transporten el mineral para comercializarlo,** son algunas de las razones por las que cerca de 1500 mineros artesanales estarían condenados a desaparecer.

Según Gabriel Calderón, minero artesanal, todas estas medidas están favoreciendo a la empresa Anglo Gold Ashanti, que estaría quedándose con el monopolio para la extracción del mineral de la jurisdicción de este municipio. Además, durante los últimos días han tenido que vender el poco material extraído a comerciantes ilegales que **“pagan lo que quieren” por el oro que se les entrega.**

**El problema de la legalidad de las minas artesanales**

En el caso de Calderón su empresa está legalizada y paga los impuestos, pero a pesar de ello se impide la entrada a las minas porque se aduce que la empresa compró los predios, sin tener en cuenta que se hicieron todos los trámites de legalización de su trabajo. Además de esta empresa hay cerca de 600 más o personas que estarían perdiendo su forma de vida **“cuando ellos llegaron, yo ya estaba, llevo 6 años acá”** manifestó Calderón.

En un comunicado de prensa la multinacional Anglogold Ashanti manifestó que está “convencida en que la formalización es el único camino hacia el desarrollo de una minería responsable”, sin embargo, Calderón manifestó que pese a que ha interpuesto todo tipo de recursos en diferentes instancias, **“la persecución se debe a la multinacional”. **Le puede interesar: ["Policía usa francotiradores para reprimir protestas en Remedios y Segovia"](https://archivo.contagioradio.com/mineros-de-remedios-y-segovia-denuncian-presencia-de-francotiradores-en-el-paro/))

**El paro en Remedios y Segovia dejó al descubierto que gobierno y empresas tienen una misma agenda**

Fueron **32 días, 2 muertes y más de 17 heridos, los que se necesitaron para que el gobierno del departamento**, la empresa y los mineros llegaran a un acuerdo sobre cómo garantizar la pervivencia de estas familias y su derecho al trabajo. (Le puede interesar: ["Estos fueron los acuerdos que levantaron las protestas de mineros en Remedios y Segovia"](https://archivo.contagioradio.com/mineros-de-remedios-y-segovia-logran-formalizacion-de-sus-actividades/))

Los acuerdos en temas como la formalización de la actividad minera ancestral, el cumplimiento por 5 años de contratos con la multinacional Anglogold Ashanti y la caracterización de la minería tradicional, estarían por verse, puesto que requieren real voluntad **política por parte del gobierno departamental y una política clara de reglamentación** nacional para garantizar los derechos para los mineros ancestrales y artesanales

<iframe id="audio_20806762" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_20806762_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU).

<div class="osd-sms-wrapper">

</div>
