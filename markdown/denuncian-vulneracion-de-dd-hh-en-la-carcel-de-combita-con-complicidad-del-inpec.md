Title: Denuncian vulneración de DD.HH. en la cárcel de Cómbita con complicidad del INPEC
Date: 2019-11-11 13:06
Author: CtgAdm
Category: DDHH, Nacional
Tags: Boyacá, Combita, Violación a DDHH en colombia
Slug: denuncian-vulneracion-de-dd-hh-en-la-carcel-de-combita-con-complicidad-del-inpec
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/CARCEL-AGENTES.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo - Imagen de referencia] 

Desde la prisión de máxima seguridad de Cómbita en Boyacá, el **Movimiento Social Carcelario** alerta sobre la continua vulneración de derechos de la que están siendo víctimas los internos del patio tres por parte de los mal llamado 'plumas' en complicidad con agentes del Instituto Nacional Penitenciario y Carcelario. (INPEC).

Prácticas de tortura, extorsión y tratos crueles contra los internos están siendo cometidos por 30 irregulares internos bajo las ordenes de los caciques **Héctor Enrique Martínez Márquez y Giovanni Jesús Corcho Pérez** con la permisividad de algunos funcionarios del INPEC.  [(Le puede interesar: Grave situación de salud de presos en Cárcel de Cómbita, Boyacá)](https://archivo.contagioradio.com/grave-situacion-de-salud-de-presos-en-carcel-de-combita/)

La denuncia relata cómo **se hacen cobros semanales a las personas por permitirles lavarles la ropa, se venden las planchas que son asignadas como dormitorios a 700. 000 y 1.000.000 de pesos** para después de venderlas, desplazar al interno, además se cobra 200.000 por dejar cambiar personas a otro patio. [(Lea también: Presos de la Picota denuncian agresiones y torturas por parte del INPEC)](https://archivo.contagioradio.com/presos-de-la-picota-denuncian-brutalidad-por-parte-de-guardia-del-inpec/)

"Estás prácticas las cometen generando el terror ya que las personas que no pagan sus exigencias, las encierran y las golpean y no les permiten salir al patio para que no denuncien", señala la denuncia que además confirma que existen más de cinco personas desplazadas en la celda número uno de Cómbita.

Ante esta crisis carcelaria, los internos han pedido la intervención de los órganos de control como la Fiscalía, la Defensoría del pueblo y el acompañamiento de organizaciones de DD.HH, las que ya han alertado previamente que al interior de las cárceles sigue sin ser resuelto el problema de fondo y se trata de la privatización de los servicios, lo que genera que los derechos fundamentales de los reclusos se conviertan en un negocio.

En junio de 2019, dragoneantes ya habían denunciado que **existía un incremento en los actos de corrupción** por parte del entonces director del penal, el coronel German Ricaurte Tapias, la subdirectora de la institución y otros funcionarios quienes tenían un pacto con bandas criminales al interior de  Cómbita.

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
