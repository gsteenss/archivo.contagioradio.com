Title: "Gobierno debe encontrar consensos frente a la minería": Rodrigo Negrete
Date: 2017-06-03 20:03
Category: Ambiente, Nacional
Tags: consultas populares, Mineria, Tamesis
Slug: gobierno_mineria_rodrigo_negrete_consultasoulares_tamesis
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/09/mineriaypaz.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Contagio Radio 

###### [3 Jun 2017] 

La historia se repite. Luego de que el Concejo de Támesis, en Antioquia, de manera unánime decidiera prohibir la minería en su territorio, el gobierno nacional se ha manifestado en contra de esa decisión asegurando que los consejos municipales no tienen esas facultades, y por tal motivo **demandará esa medida.**

“El gobierno nacional ya nos ha acostumbrado a este tipo  de manifestaciones, ya no nos sorprende”, expresa el abogado Rodrigo Negrete, quien ha acompañado varios de estos procesos en distintos municipios.

En esa línea, **el abogado ambientalista expresa preferir que existan las demandas necesarias,** justamente para aclarar que se trata de decisiones amparadas por la propia Constitución, ya que son varias las veces que la Corte se ha pronunciado sobre la minería, dándole facultades a los concejos, al alcalde y avalando las consultas populares, de forma que se garantice la autonomía territorial y la participación de las comunidades.

Y es que la Corte desde el año 2011 le ha estado ordenando al gobierno que reforme el Código de Minas, pues “**A los consejos les corresponde la defensa del patrimonio ecológico según lo dicta la Constitución”**, señala Negrete. Sin embargo, el gobierno de Santos “sigue titulando el país para la minería, desconociendo las facultades locales y los intereses de las comunidades”.

### Daños de la minería 

Los títulos mineros que existen sobre 7.600 hectáreas en Támesis y el corregimiento Palocabildo, de Jericó, van en contravía de la vocación agrícola de estos lugares, donde ya se realizan actividades exploratorias mineras.

Para el ambientalista, uno de los principales  problemas tiene que ver con la confusión entre estado y nación. Según explica el abogado, en el subsuelo no solo hay los minerales, crudo y gas, sino también recursos como el agua. En esa medida, y de acuerdo con la Ley el **subsuelo pertenece al Estado y el núcleo esencial del Estado con los municipios.** De ahí que estos sean copropietarios del subsuelo, y por tanto lo que debe aplicarse son principios de coordinación para que las autoridades locales no sean excluidas.

Por otra parte, la minería es una actividad que no le genera las riquezas al país que el gobierno suele anunciar. **La misma Contraloría ha señalado por medio de informes que no existen tales regalías**, y en cambio muchas empresas mineras declaran renta en 0 pesos. A su vez, en aras de reparar los graves impactos ambientales, están haciendo obras públicas que realmente debe proveer el Estado.

El abogado enfatiza en que la Corte ha dicho que **la minera ha generado “más afectaciones que el conflicto armado”** pues son 7 millones de hectáreas de baldíos destinadas a la minería, impidiendo que estas sean adjudicadas a campesinos víctimas de la guerra. Por tanto, para el ambientalista es falsa la teoría de que los recursos que genera la minería son necesarios para financiar la paz.

### Consenso 

En medio del panorama, Negrete, asegura que lo que se evidencia es una rebelión de los municipios del país. Es así, como mediante la **revisión de planes de ordenamiento territorial, la realización de consultas populares y los acuerdos autónomos**, las comunidades han logrado encontrar mecanismos constitucionales para defender sus territorios.

“El gobierno debe reaccionar en función de buscar consensos y no seguir imponiendo la locomotora mineroenergética”, afirma el abogado, quien tiene conocimiento de que, cada vez son más los municipios que se están revelando en contra de las arbitrariedades del gobierno. **En el departamento del Huila, Pitalito, Oporapa, Timana, Acevedo, El Agrado, entre otros municipios ya han impedido la entrada de las empresas mineras y de hidrocarburos.**

<iframe id="audio_19084488" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_19084488_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
