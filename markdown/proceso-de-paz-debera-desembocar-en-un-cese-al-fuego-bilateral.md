Title: "Proceso de paz deberá desembocar en un Cese al fuego bilateral"
Date: 2015-05-23 12:14
Category: Nacional, Paz
Tags: cese unilateral de las FARC, Conversaciones de paz de la habana, delegacion de paz, ELN, FARC, Frente Amplio por la PAz, Juan Manuel Santos
Slug: proceso-de-paz-debera-desembocar-en-un-cese-al-fuego-bilateral
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/05/paz-colombia-contagio-radio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: eluniversal.com.mx 

<iframe src="http://www.ivoox.com/player_ek_4534909_2_1.html?data=lZqglp6UfY6ZmKiakp2Jd6KmmZKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRkNbd1JCyxtrFtsXjjKjSzs7XaZO3jMbbw9HNt9XVjMnSjdHFb6Tj09Xc1MbHrYa3lIqvldOPhdPX0M7fy5KJe6ShpNTb1sbLrdCfs8bRy9SPcYarpJKh&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Luis Eduardo Celis] 

###### [22, May, 2015]

Luego del bombardeo que este jueves 22 de Mayo dejó como resultado la muerte de 26 guerrilleros en el **municipio de Guapi, Departamento del Cauca,** el Secretariado del Estado Mayor de las FARC-EP anunció a través de su sitio en internet el rompimiento del cese al fuego que de manera unilateral había declarado desde el 20 de diciembre del año anterior.

De acuerdo con la apreciación de **Luis Eduardo Celis**, analista de la Corporación Arcoiris, con la decisión tomada por parte de la organización armada  **“***muy seguramente lo que vamos a ver en los próximos meses es un nuevo pulso militar (…) o sea, lo que vendrán son más muertes; de jóvenes colombianos de la guerrilla y del ejército***”.** Ante lo cual expresa se debe mantener la perseverancia en búsqueda de un cese bilateral.

Sin embargo, el analista no cree que la determinación de la guerrilla ponga en riesgo el proceso de paz porque ***“**tanto el Gobierno como las FARC han asumido que van a negociar en medio de la confrontación***”**, ya que existe una determinación de las dos partes para terminar la confrontación violenta. “*Hay un conflicto que hay que terminar y hay una negociación andando,  lo mejor es un cese bilateral, pero como no lo hemos logrado tenemos que seguir insistiendo en que la mesa avance*”.

La **suspensión por parte de la guerrilla no muestra falta de voluntad de paz** por parte del grupo armado, simplemente **“***contener una fuerza que se sigue atacando es difícil, es imposible***”** afirma Celis y sentencia que, de no encontrarse caminos para declarar el cese bilateral, “lo que vamos a encontrar son litros de sangre dolorosos, pero es la realidad de la guerra que hay que terminar”.
