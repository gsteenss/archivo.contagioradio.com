Title: Cinco perturbadoras consecuencias del cambio climático
Date: 2017-06-05 16:42
Category: Ambiente, Voces de la Tierra
Tags: Calentamiento global, cambio climatico, Donald Trump
Slug: consecuenciasde_cambio_climatico_ambiente
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/cambio-climatico-e1478798978460.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto:  Diario hoy 

###### [5 Jun 2017] 

En medio de los anuncios de Donald Trump, que hace unos días ordenó que Estados Unidos abandone el Acuerdo de París, este lunes se recuerda la importancia de mantener la lucha contra el cambio climático en el marco de la celebración del Día Mundial de Ambiente. El lema para este año es **"Conectar a las personas con la naturaleza".**

Frente a la decisión  de Trump, el parlamento europeo indica que se reunirá para discutir el futuro del Acuerdo de París a mediados de junio como lo manifestó el presidente del parlamento, Antonio Tajani.

**"Los acuerdos deben respetarse, deben honrarse los compromisos",** dijo Tajani, quien añadió que "es un error", la medida que ha tomado el residente estadounidense, no solo para los países que más están siendo afectados por el aumento de las temperaturas, sino para su propio  país.

### Impactos del cambio climático 

Mientras tanto, a propósito del Día Mundial del Ambiente, se hace necesario recordar cuáles son los principales  efectos “más perturbadores” del calentamiento global, a los que somete Trump al mundo con su anuncio.  Estos se evidencian en el más reciente informe del Panel Internacional de Cambio Climático (IPCC), **“Cambio Climático: Impactos, Adaptación y Vulnerabilidad”**.

Se trata de impactos que solo se pueden impedir, si desde ya se empieza a trabajar en una reducción inmediata y drástica para disminuir la contaminación, y con ello el aumento de las temperaturas.

**La producción de alimentos no estará a la par del crecimiento poblacional: **Este es uno de los temas que más afecta a Colombia, ya que se generarán problemas en la producción agrícola, la pesca y aumentará la acidificación de los mares.

**Las islas pequeñas enfrentarán grandes amenazas: **Con el aumento del nivel del mar las islas pequeñas empezarán  desaparecer, en el caso colombiano, la isla de San Andrés, podría inundarse en unos 50 años, como lo explica la Asociación Ambiente y Sociedad.

**El crecimiento del nivel del mar desplazará a millones de personas: **Según el informe del IPCC “las zonas costeras más bajas de todo el mundo representan solo el 2% de la tierra pero hospedan al 10% de la población mundial, unas 600 millones de personas (13% es población urbana)”.

**La adaptación no podrá prevenir todos los impactos negativos:**De acuerdo a los científicos la mitigación y la adaptación, son las formas de enfrentar este fenómeno, sin embargo, “hay algunos lugares y algunas amenazas para las que existen solo unas pocas opciones para adaptarse. Por ejemplo, sin importar lo que los humanos hagan ahora, el Ártico seguirá calentándose y derritiéndose, así mismo, los mares serán más ácidos”, señala la revista Smithsonian.

**A los 4ºC de calentamiento, el cambio climático será el impacto humano dominante en el planeta:**“Más del 80% de la superficie de la Tierra ha recibido el impacto de la actividad humana”, indica el Diario Ecología. Esto hace que el cambio climático sea el mayor factor de impacto en los ecosistemas.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
