Title: El aparato del Estado se revistió de legalidad para afectar la Movilización: Análisis
Date: 2020-09-24 14:49
Author: AdminContagio
Category: Actualidad, Movilización
Tags: 21s, brutalidad policial, Moviización Social
Slug: aparato-estado-revistio-legalidad-afectar-movilizacion
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/09/119855072_10157508386760812_2651008610152464028_o.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: Movilización social - Contagio Radio/ Andrés Zea

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Según la campaña Defender la Libertad, en Bogotá, durante la jornada del pasado 21 de septiembre, pese a la movilización pacífica 15 personas fueron heridas y 30 personas retenidas de manera arbitraria. Son continuos los ataques a la defensa de los DD.HH y la movilización por parte del Gobierno, los entes de control y la Fuerza Pública

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Óscar Ramírez, abogado del [Comité de Solidaridad con los Presos Políticos](https://twitter.com/CSPP_) describe como una forma de castigar la protesta, agregando que con el pasar del tiempo es evidente una tendencia a desconocer y convertir en un blanco a las personas que hacen un acompañamiento a los DD.HH, "no solamente el aparato ilegal atenta contra los defensores, la Fuerza Pública también lo ha convertido en un ejercicio sistemático", expresa.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Frente al actual contexto, Ramírez, integrante de la campaña Defender la Libertad, señala que Bogotá, en particular desde hace dos años ha tenido una escalada de violencia en las movilizaciones, originada por la institucionalidad con la intención de regular la protesta, un efecto que ha sido favorecido por el aislamiento preventivo y que **se evidenció en la ciudad en dos ocasiones, la primera en la cárcel La Modelo los pasados 21 y 22 de marzo y los sucesos del 9 de septiembre cuyo resultado fue el de más de 100 personas heridas y 14 personas que perdieron la vida fruto de la brutalidad policial.** [(Lea también: Reclusos en La Picota inician huelga de hambre por llegada de Covid-19 a cárceles)](https://archivo.contagioradio.com/reclusos-en-la-picota-inician-huelga-de-hambre-por-llegada-de-covid-19-a-carceles/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Al respecto, sobre los sucesos ocurridos en los centros carcelarios, el profesor y analista Daniel Libreros afirma que existe una naturalización de la muerte que trasciende la impunidad judicial y política al recordar que la entonces ministra de Justicia, Margarita Cabello fue elegida procuradora general. [(Le puede interesar: Cinco normativas que deben cambiarse para frenar delitos de la Policía](https://archivo.contagioradio.com/cinco-normativas-que-deben-cambiarse-para-frenar-delitos-de-la-policia/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

El abogado Ramírez complementa la idea, señalando que el Estado, además de "criminalizar el liderazgo social, reprimir la libertad de expresión y el derecho a la movilización" ha llegado a instancia de los organismos de control, que en la actualidad "están bajo el influjo de funcionarios que estuvieron subordinados a la Presidencia", además de otros escenarios como "el manoseo a las altas cortes y la presión mediática" que c**oncluye, tienen un impacto en la justicia y se ve reflejado en decisiones de gran trascendencia,** lo que generará no solo un mayor estallido social sino la continuidad del conflicto armado y de una violencia política que se alimenta de estas prácticas.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### El problema de la Policía es el mismo que el conjunto de las FF.MM

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Por su parte, Libreros señala que una de las problemáticas actuales de la Policía Nacional se deriva de su nuevo código , entre ellos los vacíos en la autonomía de sus acciones, lo que explicaría por qué en muchas ocasiones los comandantes de la institución señalan que no tienen una línea de mando y que ha llevado a que exista una confusión sobre quién es el último en la línea de mando, si se trata del ministro de Defensa o si se trata de las autoridades locales como los acaldes, derivando en una ausencia de responsabilidad política.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Agrega que con la creación del código de la Policía actual, no solo se perdió la oportunidad de tener una institución apta para la etapa después del Acuerdo de Paz sino de establecer compromisos con otros sectores de las Fuerzas Armadas, por lo que el empezar una reforma desde la Policía debe hacerse en conjunto con toda la institucionalidad, "no es solo una reforma a la Policia, se pueden cambiar los procecidimientos, pero su mentalidad y su doctrina de décadas que considera toda protesta legal ilegítima", **señala es el funcionamiento de la institucionalidad misma.**

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Allanamientos y señalamientos en ascenso

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Al igual que sucedió durante el Paro Nacional de 2019, contra colectivos artísticos y periodísticos, entre el 20 y la madrugada del 21 de septiembre, se realizaron en Bogotá 14 allanamientos en distintas localidades, algo que señala Libreros fue usado como un "mecanismo preventivo de contención, hecha bajo el mismo criterio de perseguir a la sociedad

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Dichas asociaciones que realizan las instituciones entre manifestantes y organizaciones al margen de la ley son calificadas por el abogado del CSPP como una práctica que ha tomado vigor y que ha demostrado cómo **"se cogió el aparato del Estado y se revistió de legalidad una serie de injerencias arbitrarias en la vida de las personas"** [(Lea también: Testimonio: Así son las torturas de la Policía a ciudadanos detenidos en la protesta social)](https://archivo.contagioradio.com/testimonio-asi-son-las-torturas-de-la-policia-a-ciudadanos-detenidos-en-la-protesta-social/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Estos allanamientos se dan a la luz, no solo de la estigmatización que enfrentan organizaciones sociales y colectivos como Juventud Rebelde, las múltiples amenazas y panfletos que han surgido contra estos procesos, fruto de dichos señalamientos por parte del Ministerio de Defensa sino de una reciente declaración de la Fiscalía que sostiene que los actos de vandalismo en Bogotá, son fruto de un plan de “guerra de cuarta generación” con comandos urbanos en Bogotá.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Al respecto, Francisco Marín, integrante de Juventud Rebelde, resalta que dicho patrón de criminalización se ve reflejado en otros procesos y al vincularles con grupos armados, se "agrede la cara visible de nuestro proceso organizativo sino que va a tener un impacto en los trabajos que tenemos en los barrios, territorios y en las zonas de conflicto".

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Agrega que una de sus mayores preocupaciones, no solo es la judicialización que se ha estimulado desde la Policía contra los integrantes de la organización, sino otras prácticas como la de vestir de civiles a policías e infiltrar las marchas, estrategia que desde la Fiscalía desconocen y atribuyen a disidencias de las FARC, al igual que otros hechos ocurridos desde noviembre de 2019 en medio del paro nacional, incluyendo la "creación de células clandestinas y comandos urbanos" en diversas partes del país [\[En vídeo\] 10 delitos de la Policía en medio de las movilizaciones](https://archivo.contagioradio.com/en-video-10-delitos-de-la-policia-en-medio-de-las-movilizaciones/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

A esto se suma, el reciente desconocimiento por parte de Carlos Holmes Trujillo al llamado de atención de la Corte Suprema y el fallo que ordena proteger el derecho a la protesta social debido a las agresiones sistemáticas de la Fuerza Pública.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Movilización social: se acerca un estallido social

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Para Libreros, lo ocurrido en la semana de Bogotá, no solo expresó "el odio que existe hacia la Policía sino también un desespero frente a la crisis que vive el país", sin embargo, considera que desde el Estado lo que se está haciendo es diseñar un entramado para fortalecer el terror, naturalizar la represión en la movilización y ganarse capas de la opinión pública", pese a reconocer y ser conscientes desde los discursos oficiales y las altas élites el estallido social que se acerca.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

"Por parte de los medios se focalizan en Javier Ordóñez pero los otros 13 muertos los esconden" afirma el profesor, relacionando los sucesos de Bogotá con las masacres de Llano Verde en el Valle del Cauca y otras regiones, que al principio tuvieron mucho impacto, pero **"se fueron diluyendo en discursos oficiales que adormecen a la población frente a la muerte"**, por lo que señala que aquí también está en disputa la interpretación de los hechos y la realidad del país.

<!-- /wp:paragraph -->

<!-- wp:quote -->

> "Hay que tener un horizonte para las luchas sociales, no es solo la indignación y la movilización que resulta ser muy importante, logra cuestionar a las autoridades pero no pasa a un proceso de transformación y buena parte de lo que pasa en las calles exige una voluntad política y para ello hay que generar un escenario político favorable de cara al 2022".
>
> <cite>Óscar Ramírez</cite>

<!-- /wp:quote -->

<!-- wp:paragraph {"align":"justify"} -->

Libreros concluye que este es tan solo un preámbulo al estallido de la protesta social que se acerca con la pospandemia, aunque reconoce que aun no hay un movimiento o mecanismo democrático que fortalezca y unifique la defensa de la movilización, que afirma, es urgente que surja frente a lo que ocurre en el país y se vea reflejado no solo en las calles sino en otros escenarios democráticos. [(Lea también: «Si no salimos nos van a masacrar a todos… volveremos a las calles»)](https://archivo.contagioradio.com/si-no-salimos-nos-van-a-masacrar-a-todos-volveremos-a-las-calles/)

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
