Title: Oswaldo Rojas, líder comunitario del Bajo Atrato fue asesinado por las autodenominadas AGC
Date: 2020-10-11 15:08
Author: CtgAdm
Category: Actualidad, DDHH
Tags: AGC, asesinato de líderes sociales, Bajo Atrato
Slug: oswaldo-rojas-lider-asesinado-bajo-atrato
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/02/lideres-sociales-770x400.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto:

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

El pasado 10 de octubre fue asesinado O**swaldo Rojas, líder comunitario de Gengado sobre la vía que comunica el caserío de Llano Rico a Cetino**, en la vía que conduce a Brisas al , territorio colectivo de Curbaradó en el Bajo Atrato. Organizaciones sociales señalan que el asesinato fue cometido por las autodenominadas Autodefensas Unidas de Colombia (AGC).

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Según la [Comisión de Justicia y Paz](https://www.justiciaypazcolombia.com/), personas que se desplazaban en moto sacaron por la fuerza a Oswaldo de su lugar de trabajo una hora antes de los sucesos; posteriormente los armados vestidos de civil lo mantuvieron amarrado de manos por varios minutos hasta que cerca de las 3:00 p.m. se escucharon disparos.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

El líder fue asesinado cerca de una plantación de teca, allí los paramilitares dejaron tirado el cuerpo del líder para después marcharse en sus motocicletas en las que según la Comisión, **"suelen movilizarse más de treinta de los paramilitares de las AGC".**

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Oswaldo Rojas quien fue un reconocido líder local de la zona, fue despojado de sus tierras por segunda ocasión cuando fueron intervenidas 60 hectáreas de su propiedad por terceros con intereses en agronegocios. Según algunas versiones, la ocupación habría sido legitimida por dos integrantes de la junta del Consejo Comunitario, "uno de ellos, públicamente conocido por sus relaciones de alto nivel con integrantes del partido Centro Democrático". señaló la organización de DD.HH.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Además, la Comisión informó que la casa de habitación y la propiedad que aún conservaba fue saqueada y se produjo un "hurto de sus cabezas de ganado, frutales e insumos agrícolas". [(Le puede interesar: En riesgo la vida de lider de restitución de tierras Eliodoro Polo)](https://archivo.contagioradio.com/lider-territorial-heliodoro-polo-senala-que-su-vida-corre-riesgo/)

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Control de las AGC continúa en el Bajo Atrato

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Semanas atrás en una reunión obligatoria, las personas de la comunidad fueron amenazadas con ser expulsadas del territorio sino asistían a una reunión citada por mandos de las AGC. En la reunión, el grupo paramilitar expresó **"que no iban a matar a nadie, pero líder del que se tuviera sospecha de haber tenido relación con las FARC y no trabajara con ellos \[...\]quedará bajo tierra”.**

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

La Comisión de Justicia Paz ha denunciado en múltiples ocasiones que el control territorial de las AGC en el Bajo Atrato se consolidó desde noviembre de 2016 incluso en medio de la presencia de unidades militares de un batallón Selva y la Policía Nacional en Pavarandó y Mutatá, evidenciando la ausencia de acciones estatales. [(Le puede interesar: En Turbo, campesinos están siendo obligados a entregar el 50% de las tierras que habitan y trabajan)](https://archivo.contagioradio.com/en-turbo-campesinos-estan-siendo-obligados-a-entregar-el-50-de-las-tierras-que-habitan-y-trabajan/)

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
