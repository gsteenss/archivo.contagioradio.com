Title: Cinco divisiones y diez brigadas en la mira de la Corte Penal por “falsos positivos”
Date: 2017-12-05 17:01
Category: DDHH, Nacional
Slug: cinco-divisiones-y-diez-brigadas-en-la-mira-de-la-cpi-por-los-llamados-falsos-positivos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/03/cpi.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: CPI] 

###### [05 Dic 2017]

El último informe de las actividades de examen preliminar que desarrolla la fiscalía de la Corte Penal Internacional señala que se han priorizado 5 casos, cada uno de ellos es una división que, a su vez, es responsable de las acciones de 10 brigadas. Adicionalmente el informe sostiene que siguen vigentes las preocupaciones sobre el alcance de la JEP y el juzgamiento efectivo de los responsables de graves crímenes.

En concreto se trata de la primera, segunda, cuarta, quinta y séptima divisiones de las que hacen parte 10 brigadas militares que estarían acusadas de perpetrar cerca de 1289 homicidios de personas civiles que luego fueron presentadas como muertas en combate y que fueron cometidos entre 2002 y 2009, y aunque señalan que no son fruto de una investigación exhaustiva si se trata de una muestra representativa.

### **29 altos mandos militares en el centro de las investigaciones** 

“También se ha tomado en consideración la escala, la forma de comisión y el impacto de los crímenes atribuidos a las unidades militares pertinentes” señala la CPI y menciona que en las investigaciones han logrado individualizar la responsabilidad de 29 altos mandos militares que tenían jurisdicción sobre las regiones en las que más crímenes se cometieron.

“la Fiscalía ha identificado 29 oficiales militares de alto rango que, según la información disponible, estaban a cargo de las divisiones y las brigadas en cuestión entre 2002 y 2009, y bajo cuyo mando se habrían presuntamente cometido un número elevado de homicidios conocidos como falsos positivos” señala el informe. [Lea también: CPI solicita que se investigue a 29 generales por "falsos positivos"](https://archivo.contagioradio.com/corte_penal_internacional_falsos_positivos/)

Adicionalmente la CPI concluyó que existen elementos razonables para creer que se han cometido crímenes de lesa humanidad como asesinatos, desplazamiento forzado, encarcelación o privación de la libertad, tortura, violación u otras formas de violencia sexual. Además señalan que se hay importantes denuncias sobre desplazamiento forzado atribuible a los paramilitares y una alta cifra de casos de violencia sexual atribuibles a integrantes de las guerrillas.

### **La CPI insistió en preocupaciones sobre la JEP** 

En el informe la Fiscalía de la CPI hizo énfasis en el texto de Amicus Curiae, presentado por esa entidad a la Corte Constitucional en la que insisten en la necesidad de definir las responsabilidades de mando, el énfasis en “graves” violaciones a Derechos Humanos, la participación directa o indirecta de personas en los crímenes en el marco del conflicto y la efectividad de las penas alternativas que se impongan en ese marco de justicia transicional. Lea también: [Si el Estado no juzga a militares y empresarios que lo haga la CPI"](https://archivo.contagioradio.com/si-el-estado-no-puede-juzgar-empresarios-y-altos-mandos-militares-que-lo-haga-la-cpi/)

Por último señala que seguirán atentos a la información que deberá ser entregada por parte de las autoridades judiciales colombianas y continuarán examinando los desarrollos relativos a la aplicación de la Jurisdicción Especial de Paz.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
