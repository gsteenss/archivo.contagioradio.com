Title: Temor en El Cocuy luego del asesinato de Libardo Arciniegas
Date: 2020-02-05 18:41
Author: CtgAdm
Category: DDHH, Nacional
Tags: asesinato, El Cocuy, Libardo Arciniegas
Slug: temor-en-el-cocuy-luego-del-asesinato-de-libardo-arciniegas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/02/Libardo-Arciniegas-el-Cocuy-1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:heading {"align":"right","level":6,"textColor":"cyan-bluish-gray"} -->

###### Foto: Petruss {#foto-petruss .has-cyan-bluish-gray-color .has-text-color .has-text-align-right}

<!-- /wp:heading -->

<!-- wp:paragraph -->

El pasado martes 4 de febrero se denunció el asesinato de **Libardo Arciniegas, líder comunitario de El Cocuy, Boyacá**. Ante la situación, que es nueva para el Municipio, otros líderes comunales señalan que la repercusión se sentirá en la comunidad por el temor a tener una posición de dirigencia social y ser amenazado por ella.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### **Libardo Arciniegas, un líder que trabajaba por su comunidad**

<!-- /wp:heading -->

<!-- wp:paragraph -->

Según se denunció, sobre las 4 de la tarde dos hombres armados llegaron en moto hasta la vereda en la que vivía Libardo, y allí lo asesinaron. De acuerdo a José Domingo Mora, presidente de una Junta de Acción Comunal (JAC) de El Cocuy, Arciniegas era un líder muy reconocido y alguien que trabajaba mucho con la comunidad.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Arciniegas no había denunciado amenazas en su contra, pero por otra parte, Mora señaló que **el Personero Municipal realizó dos reuniones el año pasado luego que las JAC del Cocuy aparecieran como sujetos en riesgo en la alerta temprana**[N° 026 –18](http://www.indepaz.org.co/wp-content/uploads/2018/03/AT-N%C2%B0-026-18-Defensores.pdf) de la Defensoría del Pueblo. (Le puede interesar:["En enero se registraron 55 hechos de violencia política contra líderes políticos, sociales y comunales"](https://archivo.contagioradio.com/en-enero-se-registraron-55-hechos-de-violencia-politica-contra-lideres-politicos-sociales-y-comunales/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Además de este municipio, aparecían las poblaciones de Güicán de la Sierra, Chiscas, El Espino, Susacon, Chita, Jericó, Socotá, Socha, Tasco, Mongua, Aquitania, Pesca, Pisba, Paya, Labranzagrande y Pajarito en Boyacá. Mora indicó que hay comentarios de personas sobre la llegada de grupos armados, "pero no son cosas concretas" y tampoco se han presentado amenazas en la región.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### **El temor de ser líder en Colombia**

<!-- /wp:heading -->

<!-- wp:paragraph -->

Ante la situación en Colombia, que en los primeros días del año llegaron a contarse más líderes sociales asesinados que días en el año, Domingo Mora dijo que el impacto del asesinato de Libardo recalará en la comunidad, porque **"es probable que ninguna persona vaya a seguir liderando" ante el temor de las amenazas o la muerte**.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En esta zona los liderazgos también tienen un enfoque importante en torno a la protección ambiental de los páramos y el Parque Nacional Natural El Cocuy, un ecosistema delicado que las comunidades buscan conservar. (Le puede interesar: ["Boyacá, el primer departamento en prohibir el uso de asbesto en el país"](https://archivo.contagioradio.com/boyaca-el-primer-departamento-en-prohibir-el-uso-de-asbesto-en-el-pais/))

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78980} /-->
