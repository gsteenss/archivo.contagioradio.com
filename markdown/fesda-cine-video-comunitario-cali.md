Title: FESDA 10 años de cine y video comunitario en Cali
Date: 2018-06-10 11:12
Author: AdminContagio
Category: 24 Cuadros
Tags: Cali, cine comunitario, FESDA
Slug: fesda-cine-video-comunitario-cali
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/06/imagen-boletin-prensa-e1528487187644.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: FESDA 

###### 09 Jun 2018 

El Festival Nacional de Cine y Video Comunitario del Distrito de Aguablanca - FESDA cumple 10 años de trabajo por la construcción de un espacio que incentive y fortalezca los procesos de creación de audiovisual comunitario en Colombia, así como formar un público frente a la recepción de estas obras.

Con la mirada en su objetivo fundacional, para la presente edición que tendrá lugar entre el 16 y el 20 de Octubre de 2018, el tema que da sentido es la celebración de sus 10 años con la afirmación "El guión es nuestro” recordando que las periferias del país han sido construidas por las comunidades.

Las categorías de la selección oficial nacional son: Documental, Ficción, Video clip, animación, video experimental y video universitario con comunidades, teniendo como condición que los trabajos enviados deben haber sido realizados con posterioridad al primero de enero de 2016.

Adicionalmente como parte de la conmemoración, el Festival abre la categoría "El guión es nuestro" para películas y videos de cualquier año que den cuenta de la construcción de las periferias del país por parte de las comunidades. Igualmente está abierta la convocatoria para ser parte de la Muestra Internacional Fesda, a la que pueden enviar sus piezas realizadores audiovisuales comunitarios de otros países.

Las piezas y la información requerida podrá ser enviada a travès de las plataformas web Click For Festivals, Movibeta o Festhome, enviada al correo electrònico festi.videocomunitario@gmail.com, o enviada en físico a la Biblioteca Nuevo Latir Calle 76 N. 28-20 B/Alfonso Bonilla Aragón en Cali hasta el próximo 15 de julio. Encuentre a continuación las bases de la convocatoria.

[Bases Convocatoria Fesda 2018](https://www.scribd.com/document/381361037/Bases-Convocatoria-Fesda-2018#from_embed "View Bases Convocatoria Fesda 2018 on Scribd") by [Contagioradio](https://www.scribd.com/user/276652802/Contagioradio#from_embed "View Contagioradio's profile on Scribd") on Scribd

###### <iframe id="doc_52965" class="scribd_iframe_embed" title="Bases Convocatoria Fesda 2018" src="https://www.scribd.com/embeds/381361037/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-lF5EvRSSa2lB8WcU8yHB&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="0.7729220222793488"></iframe> 

###### Encuentre más información sobre Cine en [24 Cuadros](https://archivo.contagioradio.com/programas/24-cuadros/) y los sábados a partir de las 11 a.m. en Contagio Radio 
