Title: En Putumayo el Estado ha dilatado plan de sustitución de cultivos de uso ilícito
Date: 2017-07-14 17:07
Category: Ambiente, Nacional
Tags: Gobierno, Putumayo, Sustitución de cultivos de uso ilícito
Slug: en-putumayo-el-estado-ha-dilatado-el-plan-de-sustitucion-de-cultivos-de-uso-ilicito
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/07/hojasdecoca.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: ] 

###### [14 Jul 2017] 

Comunidades de Puerto Asis en Putumayo, entre ellos los que habitan en la zona de reserva campesina, manifestaron que iniciaran una **movilización pacífica permanente debido a los incumplimientos del Estado en la implementación integral de los planes de sustitución de cultivos de uso ilícito.**

De acuerdo con Hanni Silva, campesina y líder de la zona de reserva de la Perla Amazónica, los funcionarios del gobierno que han ido a las reuniones entre las comunidades, no tienen el nivel de autoridad para cumplir los acuerdos, y por el contrario **dilatan los espacios, para luego intentar poner en contra a los campesinos** y decirles que no hay la cantidad suficiente de dinero para integrar a todos los campesinos en los planes de sustitución.

“El gobierno ha estado dilatando y dilatando esa clase de reuniones, mientras tanto les dicen a los presidentes de junta que la plaza no va a alcanzar para responderle a toda la gente para ingresar al plan de sustitución” señaló Silva y agregó que “los empleados enviados por el gobierno **tienen una forma mañosa de llegarle a las comunidades, quieren acabar con las organizaciones sociales, quieren poner en contra a las comunidades**”.

En una reunión de solo integrantes de las comunidades campesinas de cultivos de uso ilícito, los campesinos decidieron que le **exigirán al Estado el cumplimiento integral de los planes de sustitución de cultivos de uso ilícito y su aplicación de manera integral** en los territorios, que dejen de dilatar los escenarios y envíen funcionarios con capacidad de toma de decisiones para tramitar de inmediato las problemáticas de las comunidades.  (Le puede interesar: ["Plan de sustitución de cultivos debe ser integral o no durará: COCCAM"](https://archivo.contagioradio.com/sustitucion-cultivos-ilicitos-cocam/))

Los próximos 24 y 24 de julio se tendrá una nueva reunión entre los campesinos y el gobierno, en donde se espera que sean atendidas estas exigencias, sin embargo, los habitantes de estas comunidades han asegurado que continuarán con la movilización pacífica y **esperan que más comunidades afectadas por el incumplimiento estatal, se junten a la iniciativa**.  (Le puede interesar: ["9 recomendaciones para el gobierno sobre planes de sustitución de cultivos de uso ilícito"](https://archivo.contagioradio.com/9-recomendaciones-para-el-gobierno-sobre-planes-de-sustitucion-de-cultivos-ilicitos/))

<iframe id="audio_19806002" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_19806002_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
