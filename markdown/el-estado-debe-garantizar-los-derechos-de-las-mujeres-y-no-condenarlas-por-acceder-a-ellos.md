Title: El Estado debe garantizar los derechos de las mujeres y no condenarlas por acceder a ellos
Date: 2015-09-14 16:50
Category: Mujer, Nacional
Tags: Aborto en Colombia, Carolina Sabino, Casa de la Mujer, Corte Constitucional, derechos de las mujeres, Fiscalía General de la Nación, Womens Link WorlWide
Slug: el-estado-debe-garantizar-los-derechos-de-las-mujeres-y-no-condenarlas-por-acceder-a-ellos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/facetascarolinasabino1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: eluniversal.com.co] 

###### [14 sep 2015]

Desde el **año 2006, la Corte Constitucional dio vía libre para abortar en Colombia en tres casos específicos**, sin embargo, este fin de semana la Fiscalía buscó imputar cargos contra la actriz y cantante Carolina Sabino, por interrupción voluntaria del embarazo, a pesar de que su situación se encuentra dentro de la legalidad.

La organización defensora de los derechos de las mujeres, Women's Link Worldwide, emitió un comunicado donde se afirma que “**el caso de Carolina Sabino encaja perfectamente en la causal salud mental**”, así mismo, aseguran que con este señalamiento público hecho a la actriz se evidencia que persiste la persecución contra las mujeres por las decisiones que toman respecto a su cuerpo y su futuro.

<iframe src="http://www.ivoox.com/player_ek_8378270_2_1.html?data=mZikmpebdI6ZmKiak5yJd6Kll5KSmaiRdo6ZmKiakpKJe6ShkZKSmaiRic2fptjhw8nTb8XZw8qYycbWpc%2Foyt%2FO1JDQs9Sfxcrfx8jMs9SfxcqYzsbXb87py8rfx9iPvY6ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Cristina Rosero,Women´s Link] 

Cristina Rosero, abogada de Women's Link, indica que este caso compete a todas las mujeres, no solo por el tratamiento público que se le dio al hecho sino además porque el “*Estado tiene una responsabilidad de garantizar el acceso a este derecho*”.

<iframe src="http://www.ivoox.com/player_ek_8378273_2_1.html?data=mZikmpebd46ZmKiakpmJd6KnlZKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRic2fptjhw8nTb8XZw8qYycbWpc%2Foyt%2FO1JDQs9Sfxcrfx8jMs9SfxcqYzsbXb87py8rfx9iPvY6ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Olga Amparo Sánchez, Casa de la mujer] 

En el caso de Sabino, Olga Amparo Sánchez, directora de la Casa de la Mujer, dice que se “**está violando el derecho a la intimidad, a la confidencialidad, **además que se re victimiza a la actriz y  se coloca en la palestra el aborto sin averiguar las condiciones de este”.

Así mismo, ambas fuentes comparten el hecho que las autoridades deben informarse también sobre el tema para no caer en errores y criminalizaciones contra las mujeres por ejercer uno de sus derechos.

Por otra parte, desde las organizaciones de mujeres, se señala que es preocupante las informaciones que aparecieron en distintos medios y redes sociales en donde se indica que hay miembros de la Fiscalía que venden información, hecho por el cuál se conoció sobre este caso.

**Interceptación ilegal  y filtración de información:**

Llama la atención en este caso que la información fue **obtenida a través de la interceptación ilegal de una conversación entre Carolina y su hermanastra**, Lina Luna, y la información fue filtrada a través para los medios masivos, que sin analizarlo, publicaron la noticias tan pronto la obtuvieron.

Frente a las vías legales que debería tomar la actriz colombiana son “Protección por la vía de tutela por violación a sus derechos fundamentales, por violación a la  confidencialidad ya que algunos **medios publicaron parte de la historia clínica, la cual es un documento de reserva**”.

Esta mañana en un comunicado el Fiscal General indicó que él era “el primero en censurar el manejo inadecuado de esta información al interior de la entidad... Nunca autoricé la divulgación de la decisión tomada”, **situación que permite evidenciar una serie de comportamientos irregulares** al interior de la Fiscalía.

### **Causales de aborto:**

Según la sentencia C-355 del 2006, dada por  la Corte Constitucional las mujeres en Colombia tienen derecho a acceder a un aborto en tres casos que son:

1-Cuando su salud mental o física o su vida se encuentran en riesgo.  
2-Cuando el embarazo es producto de una violación o incesto.  
3-Cuando el feto presenta malformaciones que hacen inviable su vida fuera del útero.  
 
