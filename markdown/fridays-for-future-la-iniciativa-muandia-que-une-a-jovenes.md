Title: Fridays for future, la iniciativa ambiental que une a  jóvenes cada viernes
Date: 2019-09-28 14:03
Author: CtgAdm
Category: Ambiente
Tags: Ambiente, Calentamiento global, Europa, greenpeace, jovenes, Movilización, Mundo
Slug: fridays-for-future-la-iniciativa-muandia-que-une-a-jovenes
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/09/WhatsApp-Image-2019-09-27-at-10.22.20-AM.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Greenpeace/Eric De Mildt] 

Fridays for Future, se convirtió en una de las mayores movilizaciones ambientales lideradas por jóvenes al rededor del mundo, en donde se dan cita cada viernes en  las plazas principales para  exigir a los gobiernos garantías, leyes y un real compromiso que proteja el ambiente y frene el cambio climático; este movimiento surgió en el 2018 luego de la  primera intervención de Greta Thunberg ante el Parlamento Sueco. (Le puede interesar: [Las luchas ambientales tienen historia](https://archivo.contagioradio.com/las-luchas-ambientales-tienen-historia/))

**Fridays for Futures** ha dejado claro,  a través de sus plantones y manifestaciones,  que el principal problema del cambio climático es  el desconocimiento frente a este fenómeno. Según Martina Borghi, integrante de Greenpeace Italia estos encuentros por el planeta pretenden concienciar a los que gobiernan sobre la importancia de frenar esta crisis ambiental antes de que sea demasiado tarde, "si los gobiernos no apoyan a la gente, pagarán los plato rotos" afirmó la ambientalista. (Le puede interesar: [Ambientalistas piden respuestas a Minambiente ante resolución para realinderar reservas forestales](https://archivo.contagioradio.com/ambientalistas_respuesta_minambiente_reserva_thomas_van_der_hammen/))

Este 27 de septiembre se  movilizaron al rededor de  70.000 jóvenes en  más de 180 ciudades, entre ellas  Roma, New York, Sao Pablo, Santiago de Chile, Bueno Aires y Bogotá, estas movilizaciones han generado incomodidad en diferentes sectores económicos que buscan ocultar las consecuencia de sus acciones en contra del planeta "en consecuencia a las movilizaciones están saliendo a las luz  los esqueletos de las empresas contaminantes, y se ha convertido en un secreto a voces las acciones que hacen , todo esto gracias a que viernes tras viernes hemos salido a protestar " agregó Borghi, así mismo señaló que las medidas que deben tomar los gobiernos deben ser necesarias, inmediatas y efectivas.

**"ustedes dicen que aman a sus hijos por encima de todo, pero les están robando su futuro ante sus propios ojos” Greta Thunberg**

### Fridays for future una iniciativa que no se callará

Los cambios que se han generado desde esta movilización ambiental han sido notorios, especialmente desde la parte social, sin embargo lo más concreto que se tiene hasta ahora es el Acuerdo de París, que se presentó en la Convención Marco de las Naciones Unidas sobre el Cambio Climático y establece medidas para la reducción de la emisión de los gases de efecto invernadero, por medio de actividades de mitigación, adaptación y resilencia de los ecosistemas puestas en marcha por los gobiernos a partir del 2020.

Borghi señaló que desde la Revolución Industrial, se ha incrementado en 1.1 grados  la temperatura de la tierra, razón por la cual una de las principales metas de los Fridays for Future, es evitar  que llegue a los 2.0 grados; "ya nadie piensa en evitar porque no se puede , solo hay que hacerlo menos malo".

> **“Seas quién seas, estés donde estés, te necesitamos ahora, y no vamos a descansar hasta que no veamos un cambio real”**

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
