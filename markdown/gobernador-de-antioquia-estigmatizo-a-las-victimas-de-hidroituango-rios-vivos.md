Title: Movimiento Ríos Vivos Antioquia demandó al gobernador Luis Pérez
Date: 2018-12-20 15:50
Author: AdminContagio
Category: DDHH, Entrevistas
Tags: Antioquia, lideres sociales, Luis Pérez, Rios Vivos
Slug: gobernador-de-antioquia-estigmatizo-a-las-victimas-de-hidroituango-rios-vivos
Status: published

###### [Foto: Mi Oriente] 

###### [20 Dic 2018] 

El Movimiento Ríos Vivos instauró una demanda en contra del gobernador de Antioquia, Luís Pérez Gutiérrez, por una serie de afirmaciones que hizo, en las que de acuerdo con la vocera de esta plataforma, Isabel Cristina Zuleta, **"ha llegado al punto de injuriar y calumniar"** a quienes integran el movimiento. Hecho que habría repercutido en el aumento de amenazas a líderes y líderesas del territorio, que en lo corrido del año llegan 27 situaciones registradas.

"Lamentamos y es un asunto muy doloroso que la autoridad del departamento de Antioquia, **esté promoviendo de manera directa e indirecta los ataques en contra de una organización social, comunitaria como la nuestra**" señaló Zuleta.

Ríos Vivos aseguró que en el balance del 2018 lograron establecer un total de **27 amenazas, siendo octubre el mes en el que más hechos se presentaron.** Además reportaron 20 casos de seguimientos y vigilancia a integrantes de la plataforma.

### **Luis Pérez instó a la persecución en contra de Ríos Vivos Antioquia** 

El movimiento denunció que Luis Pérez ha instado a autoridades como la Fiscalía o la Procuraduría a iniciar una persecución en contra de quienes integran Ríos Vivos, con la finalidad de judicializarlos.

Posteriormete, según Zuleta, el gobernador también habría afirmado que las personas que pertenecen a esa plataforma, estarían **"extorsionando a los comerciantes del municipio de Ituango"** sin que se presentara algún tipo de prueba a la Fiscalía de estas acciones, situación que para la vocera, queda en el imaginario de la sociedad "como si fuera verdad". (Le puede interesar: ["Asesinan a Hugo Albeiro George líder social y afectado por Hidroituango"](https://archivo.contagioradio.com/asesinan-hugo-albeiro-george-lider-social-afectado-hidroituango/))

Asimismo, el movimiento rechazó las actuaciones de la Policía y la secretaria de gobierno de Antioquia, Victoria Eugenia Ramírez, tras el asesinato de Hugo Albeiro George, ya que manifestaron que no era integrante de Ríos Vivos, **"deslegitimando la organización social"** y el trabajo de él, que según Zuleta estuvo durante 8 años en esta plataforma.

Finalmente, Ríos Vivos reiteró una vez más el peligro en el que se encuentran las comunidades aledañas a la represa Hidroituango, debido a que se mantiene la alerta roja por una posible avalancha en el territorio y exigieron que tanto EPM como las entidades gubernamentales del departamento, tomen medidas urgentes que garanticen la seguridad de las comunidades.

<iframe id="audio_30939497" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_30939497_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
