Title: Continúa la explotación ilegal minera en Apartadorcito
Date: 2017-01-04 12:32
Category: Ambiente, Nacional
Tags: explotación minera en Curvaradó, Zona humanitaria Camelias
Slug: continua-la-explotacion-ilegal-minera-apartadorcito
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/12/minería.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Minería] 

###### [4 Enero 2017] 

Comunidades del territorio Colectivo del Curvaradó denuncian los **daños sociales y ambientales que está generando la explotación minera ilegal en la parte alta de la comunidad de Apartadorcito**. Este hecho viene generando contaminación en diferentes ríos y quebradas de la región, que afecta a por lo menos 12 comunidades, de igual forma se pudo establecer daños a la flora debido a deforestación que produce este tipo de minería, acabando con parte de las especie de este lugar.

Y es que esta explotación se estaría realizando en contra de la voluntad de las diferentes comunidades que hacen parte del territorio Colectivo y que a través de un r**eglamento interno prohíben este tipo de actividades en sus espacios**. En el lugar una comisión de verificación pudo constatar como hay retroexcavadoras y asentamientos de carpas de plásticos. Le puede interesar: "

De igual modo una de las personas que estaba en este lugar y que hacía parte de los trabajadores que están explotando afirmo que “**que el negocio era ilegal y que debían pagar a mucha gente incluso mencionó a la policía y a los paramilitares”** además agrego que se “habría extraído un promedio de 27/28 castellanas por “lavada” y que un solo día lograron sacar 80 castellanas”. Le puede interesar:["Militares ingresaron a zona Humanitaria de Nueva Vida"](https://archivo.contagioradio.com/militares-ingresaron-a-la-zona-humanitaria-de-nueva-vida-en-curvarado/)

Sobre el dueño del predio explotado, otro trabajador del lugar informó que era Leopoldo Gómez, hombre que de acuerdo con las comunidades no habita en este lugar y añadió que esta actividad al parecer **tendría la aprobación de CODECHOCÓ y de la Junta del Consejo Mayor del Curvaradó** quienes habrían aprobado la construcción de una carretera, sin hablar de la explotación minera.

Frente a estas actividades ilegales las comunidades que se encuentran en este lugar exigen que **intervención inmediata por parte de las autoridades correspondientes, como lo son el Ministerio de Ambiente y CODECHOCÓ** para poner un freno al daño ambiental que está produciendo. Le puede interesar:["Empresario ganadero amenaza de muerte a reclamante de tierras en Curvaradó"](https://archivo.contagioradio.com/empresario-ganadero-amenaza-a-reclamante-de-tierras-en-curvarado/)
