Title: Durante jornada contra la brutalidad policial joven es detenido y golpeado por policías
Date: 2016-02-25 16:08
Category: DDHH, Nacional
Tags: Asesinato de Diego Felipe Becerra, Batucada Feminista, brutalidad policial, Fundación Nicolás Neira, MOVICE
Slug: durante-jornada-contra-la-brutalidad-policial-joven-es-detenido-y-golpeado-por-policias
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/02/Brutalidad-policial-e1456335606927.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Blog Abuso de la Fuerza Pública 

<iframe src="http://co.ivoox.com/es/player_ek_10575031_2_1.html?data=kpWimZqUd5Khhpywj5WUaZS1kZWah5yncZGhhpywj5WRaZi3jpWah5yncaXp08bb1sqPrtDmz8bRw5DHs8%2Fo08aYzsaPptPp1cbZy8nFqIzk0NHWxc7FsIze0NvS0JDJt4yhhpywj6jTstXVyM7cjbfFqMrjjJKSmaiReA%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [25 Feb 2016] 

Durante las actividades de **la jornada contra la brutalidad policial,** este 24 de febrero, **Emilio Torres Vega** fue víctima de detención y malos tratos por parte de algunos agentes de la policía, según denuncia el Movimiento de Víctimas de Crímenes de Estado, MOVICE.

El hecho se presentó hacia las 11:30 de la noche, tras terminarse la jornada de actividades culturales. El joven se encontraba en la calle 15 con carrera 4, comiendo con un grupo de amigos en un establecimiento de comidas rápidas, como lo relata Lorena, amiga de Emilio.

“A un policía se le calló su gorra y Emilio la recogió mientras hablábamos con una habitante de calle, **al rato entró otro policía tratando mal a Emilio, empezó a pedirle los papeles y a sacarlo a empujones del lugar donde nos encontrábamos, yo le dije que eso era ilegal, pero insistió en llevar a Emilio al CAI de Las Aguas**. Allá un policía le pegó un golpe en el estómago que lo dejó en el piso, apagaron las luces donde él estaba, dijeron que lo iban a judicializar y lo iban a llevar a la UPJ”, cuenta Lorena, quien asegura que “**Emilio no estaba haciendo nada malo, él es una persona supremamente pacífica y muy conciliadora**, inclusive cuando el policía lo retuvo le dijo que si él lo había ofendido, se disculpaba”.

Aunque el abogado de Emilio intentó comunicarse con su defendido, la policía impidió que recibiera la llamada, diciendo que el joven no lo tenía permitido. Desde ese momento no se supo nada más de Emilio, por lo que su familia se encontraba desesperada llamando a la UPJ quienes no le daban razón del joven.

Finalmente, hacia las 10:30 de la mañana de hoy  se logró establecer que en la **UPJ lo habían registrado con un nombre equivocado, y ahora, ya se encuentra en libertad.**

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u)  o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](https://archivo.contagioradio.com/). 
