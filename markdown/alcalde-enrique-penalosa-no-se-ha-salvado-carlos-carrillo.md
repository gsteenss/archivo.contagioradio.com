Title: Alcalde Enrique Peñalosa no se ha salvado: Carlos Carrillo
Date: 2018-02-28 14:49
Category: Otra Mirada, Política
Tags: CNE, Enrique Peñalosa, revocatoria, Revocatoria Enrique Peñalosa
Slug: alcalde-enrique-penalosa-no-se-ha-salvado-carlos-carrillo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/02/DXE2pYbX4AAnu3U-e1519839217835.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Twitter Comosoc] 

###### [28 Feb 2018] 

Tras la decisión del Consejo Nacional Electoral de tumbar el proceso de revocatoria contra el alcalde de Bogotá, Enrique Peñalosa, quienes defienden la iniciativa aseguran que **“Peñalosa no se ha salvado”** y que ahora se necesita que se fortalezca la movilización en aras de exigir la renuncia del mandatario de la ciudad. El CNE argumentó que la decisión se dio porque el sindicato de la empresa de telefonía ETB, “sobrepasó los topes por 6 millones de pesos”.

De acuerdo con Carlos Carrillo, integrante del Comité promotor de la revocatoria, “el Consejo Nacional Electoral, desde hace 10 meses está haciendo cualquier tipo de marrulla **para evitar que los bogotanos vayamos a las urnas**”.

### **Donación del sindicato de la ETB no sobrepasó los límites** 

Sin embargo, afirmó que se trata de un “Concejo Nacional Electoral que archivó la investigación en contra de Óscar Iván Zuluaga por haber recibido millones de dólares de Odebrecht”. Además, Carrillo afirmó que no es cierto que se hayan sobrepasado los límites o topes individuales teniendo en cuenta que el sindicato de la ETB “aportó 40 millones de pesos y los **6 millones de la discordia fueron un préstamo**”.

Esto fue interpretado por el CNE como un aporte de la ETB y “así el sindicato hubiera aportado 50 o 60 millones de pesos, **tampoco existiría una razón para detener el proceso** porque los sindicatos no están contemplados en el código de comercio”. Sobre esto, la ley asegura que las organizaciones sociales, que incluyen a los sindicatos, no están cobijadas por el código de comercio por lo que no tendrían un tope para realizar sus donaciones. (Le puede interesar:["Con Peñalosa todo lo malo puede empeorar: Manuel Sarmiento"](https://archivo.contagioradio.com/con-penalosa-todo-lo-malo-puede-empeorar-manuel-sarmiento/))

### **No va a haber revocatoria en las urnas pero si en las calles** 

Según explicó Carrillo, “lo que hizo el CNE fue ponerle una **sentencia de muerte a la revocatoria**”.  Esto teniendo de presente que “la ley establece que, durante los últimos 12 meses del mandato del alcalde, no puede hacerse una revocatoria”. Además, durante los últimos 18 meses “no habría elecciones atípicas” y el presidente nombraría a un alcalde encargado.

Si hubiese revocatoria tendría que hacerse antes de junio y con lo que establece el fallo, que no se pronuncia sobre la viabilidad de expedir o no el certificado del proceso de revocatoria y que además abre investigación contra el promotor del proceso, Gustavo Merchán, **“la revocatoria está muerta”**. (Le puede interesar:["Pueblo Muisca de Bosa rechaza afirmaciones "malintencionadas" de Peñalosa"](https://archivo.contagioradio.com/pueblo-muisca-de-bosa-en-bogota-rechazo-afirmaciones-mal-intencionadas-de-enrique-penalosa/))

### **Movilización ciudadana se debe fortalecer** 

Sin embargo, han aclarado que el alcalde aún no se ha salvado y lo que queda por hacer es “darle sustancia a esta democracia y exigir la renuncia de Enrique Peñalosa”. Esto teniendo en cuenta que el alcalde tiene una imagen desfavorable** de cerca del 90%** y que “se ha amangualado con los sectores politiqueros de este país para salvarse se la democracia”. Por esto, le han hecho un llamado a los ciudadanos para que se organicen y se le exija la renuncia en las calles “como pasaría en cualquier sociedad democrática”.

Además, enfatizó en que en Colombia “hay un cuento de que la movilización ciudadana no sirve para nada y nos han dicho esto porque **es a lo único que le tienen miedo**”. Por esto, dijo que las expresiones de movilización ciudadana “son buenas para la democracia y toca seguir apuntándole a eso”. Esto “va a permitir derrotar a los verdaderamente poderosos que están detrás de Enrique Peñalosa como lo son Germán Vargas Lleras o Juan Manuel Santos”.

Finalmente, el magistrado del Consejo Nacional Electoral quien se opuso a tumbar la revocatoria, Armando Novoa, le explicó a Blu Radio que no existen evidencias de alguna violación a una norma de carácter estatutario para haber tomado esa decisión. En ese sentido, Carrillo indicó que los magistrados estarían cometiendo **prevaricato.**

<iframe id="audio_24134696" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_24134696_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
