Title: Hasta 60 salarios mínimos pagará quien maltrate a un animal
Date: 2015-12-03 10:35
Category: Animales, Nacional
Tags: Congreso de la Rapública, Juan Manuel Galán, Maltrato animal, penalización del maltrato animal, Proyecto de Ley 172, tauromaqui
Slug: hasta-60-salarios-minimos-pagara-quien-maltrate-a-un-animal
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/10/Marcha-No-a-las-corridas-de-toros-31-.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Contagio Radio 

Los defensores de los derechos de los animales celebran la decisión de la plenaria del senado de la república, que el día de ayer, aprobó en su cuarto debate el proyecto de Ley que penaliza el maltrato animal, con ponencia del senador Juan Manuel Galán.

Con la aprobación del proyecto de Ley 172 se tipifica el delito de maltrato animal, como lo explica el representante y autor de la Ley, Juan Carlos Losada, **“da de 12 a 36 meses de cárcel con unos agravantes, cuando se comete el maltrato enfrente de menores de edad, cuando el maltrato es de tipo sexual, conocido como zoofilia o bestialismo, eso tendrá penas mayores, cuando se trate de un funcionario público”,** además  genera multas de hasta 60 salarios mínimos mensuales vigentes para quienes maltraten un animal, así mismo abandonar a un animal será una conducta que será castigada con multas entre dos y 20 salarios mínimos.

La senadora Claudia López decidió retirar su proposición, que prohibía destinar bienes o recursos públicos para espectáculos crueles con animales como la tauromaquia o las peleas de gallos, al visibilizar que el congreso no está dispuesto a terminar con este tipo de actividades crueles, teniendo en cuenta que las bancadas del Centro Democrático y el Partido Conservador no aceptaron la propuesta. Sin embargo, dice Losada, **“se da un gran paso cuando se considera a los animales como seres sintientes”.**

Cabe recodar, que este último debate y los anteriores **fueron impulsados fuertemente por la sociedad civil por medio de la movilización** y también desde las gradas en el Congreso de la República desde donde se le exigía a los congresistas votar a favor de esta Ley.

Tras la aprobación en plenaria del Senado, se seguirá con la conciliación entre Senado y Cámara y posteriormente irá a sanción presidencial para que pueda entrar en vigencia.
