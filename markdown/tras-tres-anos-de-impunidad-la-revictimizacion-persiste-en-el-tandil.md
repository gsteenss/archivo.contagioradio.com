Title: Tras tres años de impunidad, la revictimización persiste en El Tandil
Date: 2020-10-11 12:00
Author: AdminContagio
Category: Actualidad, DDHH
Tags: brutalidad policial, Erradicación Forzada, Masacre de El Tandil, nariño
Slug: tras-tres-anos-de-impunidad-la-revictimizacion-persiste-en-el-tandil
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/10/image.png" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/10/El-Tandil.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: Conmemoración El Tandil / Contagio Radio

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Han transcurrido tres años desde que siete campesinos fueron asesinados y otros 100 fueron heridos a manos de la Fuerza Pública en El Tandil, Nariño Aunque han sido varios los esfuerzos por parte de la defensa de las víctimas, el caso continúa en la justicia penal militar en un hecho considerado de alta impunidad pues hasta el momento no existen personas sancionadas ni mucho menos condenadas por los hechos que ocurrieron el 5 de octubre de 2017.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

**La abogada de la Corporación Jurídica Yira Castro, Lucía Aldana** expresa que se han interpuesto deferentes recursos de nulidad y acciones de tutela, sin embargo hasta el momento han sido infructuosos. [(Lea también: A dos años de la masacre de El Tandil, en Tumaco persiste la impunidad)](https://archivo.contagioradio.com/a-dos-anos-de-la-masacre-de-el-tandil-en-tumaco-persiste-la-impunidad/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

En medio de un plantón realizado el pasado 8 de octubre con las víctimas frente a la Fiscalía en un acto simbólico en memoria de las personas que fallecieron y los heridos de aquel día, también fue radicado ante el ente investigativo una solicitud que busca se dé una colisión de competencias en el caso pidiendo que se eleve una solicitud ante el Consejo Superior de la Judicatura para que decida a que jurisdicción debe obedecer el caso y acudir a la justica penal ordinaria.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

**Elier Martínez, líder campesino de la vereda El Tandil.**, relata que la erradicación forzada ha sido una constante desde hace mucho tiempo y que en la actualidad hay labores por parte del Ejército y planes "Avispa" por parte de la Policía antinarcóticos realizando la aspersión con glifosato de forma manual.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Aunque El Tandil fue una de las veredas que acogió el punto 4 del Acuerdo de Paz ante el compromiso que el Estado colombiano y las entonces FARC EP adquirieron de llegar a los territorios y socializar el Programa Nacional Integral de Sustitución de Cultivos de Uso Ilícito (PNIS), el líder campesino resalta que en el territorio únicamente se realizo una asamblea y por lo tanto nunca se llegó a firmar el acuerdo que permitiera la sustitución voluntaria.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

**"Siempre ha estado la voluntad de querer sustituir pero desgraciadamente nunca ha llegado algo que nos motive a que lo hagamos**, si al Gobierno le interesara debería empezar haciendo las cosas bien, son más de 30 años haciéndolo de esa manera y hasta ahora nada les ha funcionado", afirma Elier Martínez.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### La confianza en El Tandil hacia la Fuerza Pública ya no existe

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Tanto Elier como Mellington Cortés, campesino y víctima de El Tandil, coinciden en que "la confianza hacia las fuerzas del orden del estado llamase Ejército o Policía se perdió", agregando que en el territorio su accionar sigo siendo de amedrantamiento contra el campesinado; **"hasta los niños ven a un militar y es como mirar al diablo, prácticamente",** relata uno de los líderes.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

La abogada Aldana señala además que personas de la comunidad han denunciado que ante la no suspensión prolongada de varios de los responsables y personas de la Fuerza Pública que participaron de la masacre, han vuelto a ser vistos entre la comunidad, un hecho que afirma, "revictimiza a la población y los afectados del suceso".

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Otro elemento que es de preocupación, agrega **es que en medio de situaciones de erradicación forzada, los habitantes denuncian que la Fuerza Pública les recuerda lo que pasó en la masacre hace tres años para intimidarles, generando temor en la comunidad.** [(Le recomendamos leer: Los testimonios que comprometen a la Fuerza Pública en masacre de Tumaco)](https://archivo.contagioradio.com/los-testimonios-que-comprometen-a-la-fuerza-publica-en-masacre-de-tumaco/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

"Encontrarse con ellos es complejo, se mantienen en ese ámbito de erradicación, pero es triste e indignante escuchar que de la voz de ellos salga que a las comunidades les va a pasar lo mismo que le pasó a la gente de El Tandil, uno no encuentra la explicación de por qué se comportan así", relata Elier Martínez. [(Le recomendamos leer: Ante militarización, comunidades en Tumaco piden respeto por sus derechos)](https://archivo.contagioradio.com/ante-militarizacion-comunidades-en-tumaco-piden-respeto-por-sus-derechos/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Por su parte, **Yuri Neira, padre de Nicolás Neira, estudiante de 15 años que fue asesinado por un proyectil del ESMAD en 2005** y quien desde entonces busca justicia y conciliación en el caso, señala que basta con remitirse a los hechos de violencia que ha ocurrido en Colombia para darse cuenta que la sociedad en general ha sido revictimizada por el Estado y sus "auxiliadores". [(Lea también: Así avanza la (in)justicia en el caso de Nicolás Neira)](https://archivo.contagioradio.com/asi-avanza-la-injusticia-en-el-caso-de-nicolas-neira/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

La abogada Aldana agrega que los recientes nombramientos de personas cercanas al Gobierno en cargos al interior de organismos de control, como la Fiscalía, Defensoría del Pueblo y la Procuraduría, pues dificultan el trabajo de organizaciones defensoras de DD.HH, "son fichas políticas del gobierno y cuando estos organismos deberían tener una visión imparcial y dar garantías para las víctimas, resultan convertidas en todo lo contrario lo que genera desconfianza".

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Agrega que la postura gubernamental de dar un trato militar a a la protesta social ha permeado a su vez a la Fuerza Pública yendo en contravía de un derecho fundamental, viéndose reflejado en el uso desproporcionado de la fuerza en contra de personas que ejercen el derecho a la protesta, no solo en El Tandil, el caso de Nicolás Neira sino también en los más recientes hechos ocurridos el pasado 9 de septiembre en la cuidad de Bogotá donde más de 100 personas heridas y 14 personas perdieron la vida fruto de la brutalidad policial. [(Lea también: El aparato del Estado se revistió de legalidad para afectar la Movilización: Análisis)](https://archivo.contagioradio.com/aparato-estado-revistio-legalidad-afectar-movilizacion/)

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Persiste la pregunta ¿Quién dio la orden?

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Ante recientes investigaciones por parte de organizaciones como el CINEP, que revelan cómo, durante el primer semestre fue la Policía la mayor Mellington señala que esta ha sido una situación histórica en la vereda y que basta con recordar cómo tan solo días después de la masacre, también fue atacada una misión humanitaria compuesta por la MAPP OEA, la ONU, organizaciones de derechos humanos y medios de comunicación, incluido Contagio Radio. [(Lea también: Policía, paramilitares y ejército son los mayores violadores de DDHH según el CINEP)](https://archivo.contagioradio.com/primer-semestre-2020-mayores-violaciones-dd-hh-cometidas-policia/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

"Qué más necesita el Gobierno, aquí solo hay un culpable, Luis Fernando González, mayor de la V División del Ejército y Javier Enrique Soto, capitán de la Policía, a ellos dos hay que preguntarles quién dio la orden para que dispararan, por qué lo hicieron y tomaron la decisión".

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Los campesinos del Tandil señalan que en medio de los diálogo que han mantenido con la Policía, sus integrantes afirman que deben cumplir con sus ordenes de erradicar o alguien más vendrá a hacerlo. **"Está evidenciado que las ordenes vienen de arriba",** afirma Elier agregando además la injerencia que tiene la agenda impuesta por el gobierno de Estados Unidos en su lucha contra el narcotráfico sobre Colombia. [(Le puede interesar: Cinco normativas que deben cambiarse para frenar delitos de la Policía)](https://archivo.contagioradio.com/cinco-normativas-que-deben-cambiarse-para-frenar-delitos-de-la-policia/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

"La policía es un órgano necesario, sin embargo lo que sí hay que analizar es cuando esta institución se vuelve una herramienta del poder oficial para enfocarla en ciertos tipos de población, hay una línea delgada entre el ejercicio de la fuerza y el abuso por lo que debe haber un mayor control y esfuerzos en la selección de su personal", concluye la abogada. [(Le recomendamos leer: Gobierno gastará \$13.800 millones en ESMAD y Policía)](https://archivo.contagioradio.com/gobierno-gastara-13-800-millones-en-esmad-y-policia/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Pese a que los avances en la justicia han sido nulos tras tres años de ocurrida la masacre, los habitantes de El Tandil piden al Estado que llegue a la vereda a través de inversión social, "cree colegios y centros de salud, y proyectos productivos" y no estigmatice más a la población.

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
