Title: Minga del 10 de Octubre avanza a pesar de la estigmatización
Date: 2020-10-07 21:44
Author: AdminContagio
Category: Actualidad, Nacional
Tags: Minga, Minga Indígena
Slug: minga-del-10-de-octubre-avanza-a-pesar-de-la-estigmatizacion
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/10/Minga-indigena.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

Tras los señalamientos del expresidente Álvaro Uribe, quien se encuentra recluido en su domicilio bajo medida de aseguramiento, expresando que la Minga Indígena convocada para el próximo 10 de octubre es «*una toma socialista del Estado*»; varios líderes indígenas y opinadores, señalaron que Uribe estaba estigmatizando y poniendo en peligro a la Minga y sus dirigentes.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El senador **Feliciano Valencia**, líder indígena y exconsejero del Consejo Regional Indígena del Cauca -CRIC-, expresó que con los trinos del exsenador Uribe, se estaba **estigmatizando e incentivando la violencia contra la movilización indígena.**

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/FelicianoValen/status/1313462353715920897","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/FelicianoValen/status/1313462353715920897

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

Por otra parte, **Jorge Sánchez** coordinador del CRIC, señaló que **la Minga es un proceso indígena cuyo único objetivo es reivindicar los derechos a la vida, la seguridad y el territorio de los pueblos étnicos.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El líder indígena expresó que el Gobierno Nacional tiene una conversación pendiente, desde el año pasado, con los pueblos indígenas del sur occidente colombiano, refiriéndose a la Minga llevada a cabo en Corinto en el 2019. Por otro lado, fue enfático en rechazar lo dicho por el exsenador Uribe en una cadena de [trinos](https://twitter.com/AlvaroUribeVel/status/1313422890319085570) titulada como «*Ojo con la Minga*».

<!-- /wp:paragraph -->

<!-- wp:quote -->

> «Yo creo que Uribe siempre nos ha estigmatizado y hoy debería dedicarse a defenderse de todos los crímenes de Estado que cometió cuando era Presidente, y no meterse en una reivindicación de los pueblos»
>
> <cite>Jorge Sanchez, coordinador del CRIC.</cite>

<!-- /wp:quote -->

<!-- wp:paragraph -->

Asimismo, Jorge Sánchez le pidió a Uribe que deje gobernar al presidente Duque y que evite generar más polarización pues «*eso no le hace bien al país*» y expresó «*que esté tranquilo en el Uberrimo pensando en defenderse de todas las masacres que ha cometido*». (Le puede interesar: [Juez 30 mantiene la detención de Álvaro Uribe](https://archivo.contagioradio.com/caso-uribe-ley-600-o-ley-906-justicia-o-impunidad/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Algunos sectores de la ciudadanía también rechazaron las declaraciones de Uribe y expresaron que estas se podrían traducir en violencia contra los indígenas.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/JUANCAELBROKY/status/1313510374046674944","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/JUANCAELBROKY/status/1313510374046674944

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/k_Moreno25/status/1313613059160768512","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/k\_Moreno25/status/1313613059160768512

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:heading {"level":3} -->

### La Minga Indígena fue convocada para el 10 de octubre

<!-- /wp:heading -->

<!-- wp:paragraph -->

En una carta abierta dirigida al presidente Iván Duque, el Consejo Regional Indígena del Cauca, solicitó su presencia para entablar un dialogo, al tiempo que le expusieron las principales problemáticas que aquejan a las comunidades del sur occidente colombiano. (Lea también: [En Gobierno Duque han ocurrido 1.200 violaciones de DDHH contra el pueblo Awá en Nariño](https://archivo.contagioradio.com/en-gobierno-duque-han-ocurrido-1-200-violaciones-de-ddhh-contra-el-pueblo-awa-en-narino/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

A través de la misiva, el CRIC, denunció la continuidad de la violencia sociopolítica en contra de pueblos y comunidades, expresada en **homicidios, masacres, atentados, amenazas, desapariciones y desplazamientos forzados; cometidos en contra de personas, líderes sociales, defensoras de derechos humanos, y de excombatientes de FARC.** Asimismo denunciaron el «*tratamiento militar a la protesta social, el uso excesivo de la fuerza y a la brutalidad policial*».

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/CRIC_Cauca/status/1313506232771186688","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/CRIC\_Cauca/status/1313506232771186688

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

En busca de informar a la sociedad colombiana la existencia de estas problemáticas y la necesidad de que el Gobierno Nacional adopte una política pública para hacer frente a las mismas; **la Minga congregará a varias comunidades indígenas que marcharan desde el 10 de octubre**, inicialmente hacia la ciudad de Cali, Valle del Cauca; y de no obtener un encuentro con el Presidente, se dirigirán hacia la capital del país Bogotá. (Lea también: [Minga indígena se reactivará el próximo 10 de Octubre](https://archivo.contagioradio.com/minga-indigena-se-reactivara-el-proximo-10-de-octubre/))

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->
