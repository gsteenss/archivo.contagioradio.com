Title: Izquierda Paraguaya lanza movimiento unitario, el Congreso Unitario del Pueblo
Date: 2015-03-03 19:55
Author: CtgAdm
Category: El mundo, Otra Mirada
Tags: conamuri, izquierda, paraguay, politica, unidad
Slug: izquierda-paraguaya-lanza-movimiento-unitario-el-congreso-unitario-del-pueblo
Status: published

###### Foto: Última Hora 

#####  

<iframe src="http://www.ivoox.com/player_ek_4160564_2_1.html?data=lZajkpqaeI6ZmKiak5eJd6KokZKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRjdvl1s7S1MnFb7HV08bU18bdpYzgwtPnw5DRs9fdzs7S0NnTb9biytnO1M7TaZO3jMrZjajTssjmjoqkpZKns8%2FowszW0ZC2pcXd0JCah5yncZU%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Perla Álvarez, CONAMURI Paraguay] 

El domingo 1ro de marzo se realizó el lanzamiento del Congreso Democrático del Pueblo, movimiento social y político unitario de la izquierda en Paraguay. A la constitución del Congreso asistieron más de 1.300 delegados y delegadas de organizaciones y partidos políticos progresistas y de izquierda de todo el país.

El objetivo planteado por la plataforma de convergencia, que reúne al Frente Guasu, a los partidos Paraguay Pyahurá y Kuña Pyrendá, a la Federación Nacional Campesina, a la Coordinadora de Organizaciones Campesinas, Indígenas y Populares, así como a centrales obreras y organizaciones juveniles y estudiantiles universitarias y secundaristas; es potenciar las acciones del pueblo uruguayo en contra de -lo que consideran- políticas privatizadoras por parte del gobierno de Horacio Cartes. El lema del Congreso Democrático del Pueblo es "contra la privatización, y por justicia, tierra y trabajo", según lo señala Perla Álvarez, de la Coordinación Nacional de Mujeres Trabajadoras, Rurales e Indígenas, CONAMURI.

En el año 2002, las organizaciones sociales y partidos políticos habían tenido una primera experiencia de un Congreso Democrático del Pueblo, convocado en ese entonces para exigir la derogación de la ley de privatizaciones 1615. El proyecto unitario culminó, sin embargo, una vez se alcanzó el objetivo, y la movilización y organización sufrieron el reflujo resultado del cansancio por parte del movimiento social.

Tras el golpe del Estado parlamentario que en el año 2012 sufrió el presidente Fernando Lugo (de corte progresista), las apuestas políticas de izquierda se replegaron. "El golpe fue preventivo, contra la población, que estaba en un despertar después de una larga dictadura militar de 35 años", afirma Álvarez.

Las organizaciones que actualmente integran el Congreso Democrático del Pueblo esperan que este sea la herramienta para potenciar las luchas sociales en el Paraguay para construir gobiernos alternativos en el largo plazo. "Queremos ser una fuerza aglutinadora del descontento popular que derrote las políticas que privatizan nuestros recursos y lo que nos queda de futuro. Creemos que se juega el futuro del país y de la región", señaló Perla Álvarez. Y si bien, no tienen una apuesta para las próximas elecciones regionales, parlamentarias y presidenciales, algunos de los movimientos que lo integran, como el Frente Guasu, si esperan presentarse con candidatos propios.
