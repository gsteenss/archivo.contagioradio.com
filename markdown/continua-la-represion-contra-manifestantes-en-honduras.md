Title: Continúa la represión contra manifestantes en Honduras
Date: 2018-01-26 14:39
Category: DDHH, El mundo
Tags: Fraude electoral, honduras, Juan Orlando Hernández, movilización en honduras
Slug: continua-la-represion-contra-manifestantes-en-honduras
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/12/honduras_policia-e1512497554999.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Reuters] 

###### [26 Ene 2018] 

En Honduras continúan las movilizaciones de los ciudadanos que protestan por lo que **han llamado una** **dictadura de Juan Orlando Hernández**, quien fue reelegido hace dos meses. Los manifestantes han argumentado que su elección fue ilegal y que además hubo fraude electoral. Hasta el momento han sido asesinadas 30 personas en medio de las protestas.

Para el 27 de enero, día que se posesiona Juan Orlando Hernández, la ciudadanía **continuará con las movilizaciones y protestas**. Han convocado a una caravana de automóviles el día de hoy en la capital de Honduras para rechazar la presidencia de actual y se espera que se realice un paro nacional con la toma de carreteras.

### **Represión contra los manifestantes continúa** 

La sociedad hondureña ha denunciado que, en el marco de las movilizaciones que se extienden a lo largo del país, continúa la represión y ha habido un aumento en la **detención de personas** que se oponen a que Juan Orlando Hernández continúe siendo presidente. La ciudadanía ha denunciado amenazas por parte de las Fuerzas de Seguridad del Estado como los son la Policía Militar del Orden Público. (Le puede interesar: ["Honduras rechaza reconteo de votos por tribunal que avaló fraude"](https://archivo.contagioradio.com/honduras-muertos-fraude/))

De acuerdo con Eliana Borges, periodista de Radio Progreso en Honduras, esta Policía **fue creada por el gobierno del actual presidente** “que está totalmente a su disposición para reprimir y capturar a las personas que se manifiesten en contra de la dictadura”. Dijo que ya hay 6 presos políticos que fueron enviados a una cárcel de máxima seguridad, entre ellos un funcionario público que se le acusó de asociación ilícita.

A la problemática se suma la reciente acusación del director de la Policía nombrado por Juan Orlando Hernández **por facilitar el paso de drogas hacia Estados Unidos**. “Esto es un nuevo elemento que se da en la crisis pos electoral que se está viviendo en Honduras, estamos esperando declaraciones de las autoridades, por ahora continúan las movilizaciones”.

### **Elecciones presidenciales fueron ilegales** 

Borges indicó que el proceso electoral de 2017 **“estuvo fundamentado bajo la ilegalidad** de la candidatura del actual presidente Juan Orlando Hernández puesto que ha violentado la constitución que prohíbe la reelección presidencial”. Dijo que el artículo 4 de la Constitución que indica que debe haber una alternabilidad en el poder, no fue atendido por Hernández. (Le puede interesar: ["Policía hondureña decidió que no reprimirá movilización ciudadana "contra fraude electoral"](https://archivo.contagioradio.com/elecciones_honduras_fraude/))

Además, una vez cerraron las urnas y comenzó el conteo de los votos, la Alianza de Oposición contra la Dictadura **denunció fraude electoral** y esto fue ratificado por la Organización de Estados Americanos OEA, quien pidió transparencia al momento de contar los votos para elegir el presidente.

"El Tribunal Supremo Electoral, controlado por el actual mandatario, ha querido **seguir unilateralmente el conteo** con el apoyo de la embajada de Estados Unidos, avalando un proceso que a todas luces no respeta la libertad popular", indicó en su momento Rodolfo Pastor, vocero de la Alianza de Oposición contra la Dictadura.

<iframe id="audio_23385272" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_23385272_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]

<div class="osd-sms-wrapper">

</div>
