Title: 'Bojayá entre fuegos cruzados', el documental que reconstruye la verdad de un pueblo
Date: 2019-09-20 13:04
Author: CtgAdm
Category: 24 Cuadros, Cultura
Tags: Bojaya, Cinemateca de Bogotá, Masacre de Bojayá
Slug: bojaya-entre-fuegos-cruzados-el-documental-que-reconstruye-la-verdad-de-un-pueblo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/09/Bojayá.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/09/Bojayá1.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: CIVP] 

Este 19 de septiembre, la Comisión  Interétnica de la Verdad de la Región Pacífico,  compartió la primera proyección de 'Bojayá, entre fuegos cruzados', cinta que recoge las memorias de  la masacre ocurrida en Bojayá el 2 de mayo de 2002 y que a su vez narra desde las voces de sus habitantes, la lucha por obtener la paz en su territorio.

La historia, dirigida por Oisín Kearney, recopila los relatos de cinco familias testigos de ese trágico día en que paramilitares de las Autodefensas Unidas de Colombia (AUC), en medio de una confrontación armada con las FARC, utilizaron a la comunidad de Bojayá, como escudos humanos y quienes atrapados en el fuego cruzado buscaron refugio en la iglesia local, antes de ser alcanzados por una pipeta lanzada por las FARC-EP al interior del templo. Aquel día fueron asesinadas 79 personas, en su mayoría mujeres y niños, otros 115 habitantes quedaron heridos.

> [\#HOY](https://twitter.com/hashtag/HOY?src=hash&ref_src=twsrc%5Etfw) [@PalaciosLeiner](https://twitter.com/PalaciosLeiner?ref_src=twsrc%5Etfw) y @⁨TimoFARC en el lanzamiento del documental Bojaya entre fuegos cruzados. Esto es construir PAZ. No hay camino a la paz, la PAZ es el camino [@Ccajar](https://twitter.com/Ccajar?ref_src=twsrc%5Etfw) ??? [pic.twitter.com/OB64pICVIM](https://t.co/OB64pICVIM)
>
> — Soraya G (@sorisgut) [September 20, 2019](https://twitter.com/sorisgut/status/1174882028845752320?ref_src=twsrc%5Etfw)

<p>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
</p>
El documental sigue el día a día de Leyner Palacios Asprilla, líder de la comunidad de Chocó que perdió a 32 de sus familiares en la masacre y quie**n mientras reconstruye su pasado, expone la peligrosa tarea de ser un líder social en Colombia.** [(Le puede interesar: Estado debe reconocer su responsabilidad y dar la cara a Bojayá y al mundo)](https://archivo.contagioradio.com/estado-debe-reconocer-su-responsabilidad-y-dar-la-cara-a-bojaya-y-al-mundo/)

Israel Zúñiga, excombatiente y uno de los responsables directos de esta masacre, y quien estuvo presente en la presentación de la película aseguró que "se trata de un proceso doloroso ante la incertidumbre que genera la continuidad de la violencia, transversalizando la vida de los colombianos, ese dolor se expresa y nos llena también de esperanza".

![Bojayá](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/09/Bojayá.jpg){.size-full .wp-image-73961 width="960" height="664"}

Foto: CIVP Comisión interétnica de la verdad de la región del Pacífico

A su vez, el documental aborda la firma del Acuerdo entre el Estado y las FARC, su proceso de reincorporación y la forma en que el perdón se convierte en una decisión transversal en este relato. De igual forma, el largometraje expone la ausencia de control territorial por parte del Estado y cómo grupos ilegales continúan explotando los recursos e infundiendo temor en el Chocó.

Leyner Palacios, protagonista del documental manifestó que es gratificante la acogida que se le ha dado a este filme, "a veces uno en Bojayá se siente perdido, solo en la mañana, pero estos rostros de solidaridad y compromiso nos reconfortan para seguir trabajando". [(Le puede interesar: El Cristo Negro una muestra de perdón, esperanza y paz en Bojayá)](https://archivo.contagioradio.com/el-cristo-negro-bojaya/)

 

<iframe src="https://www.youtube.com/embed/dImO8E6z98Q" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
