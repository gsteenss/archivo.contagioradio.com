Title: Cinco propuestas de mujeres que le apuestan a la paz
Date: 2019-09-05 12:14
Author: CtgAdm
Category: Mujer, Nacional
Tags: Comunidad LGBTI, III Encuentro internacional de Estudios Críticos de las Transiciones Políticas, Mujeres Campesinas, Mujeres Excombatientes
Slug: cinco-propuestas-de-mujeres-que-le-apuestan-a-la-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/09/Mujeres.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/09/Mujeres-Conversatorio.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: SandinoVictoria] 

En el marco del III Encuentro Internacional de Estudios Críticos de las Transiciones Políticas, cinco mujeres representantes de colectividades excombatientes, afro, campesinas y LGBTI se refirieron a las apuestas y experiencias que desde los territorios les permiten acoger las diversidades de la sociedad y construir paz a partir de estas.

### Mujeres: el rostro de la paz

En medio del diálogo, moderado por la senadora del Partido FARC, Victoria Sandino, **María Arrieta, campesina de Tierralta,** Córdoba señala que lo primordial debe ser la defensa de los acuerdos, exigir que se cumplan, acoger a los excombatientes e integrarlos a la sociedad, "si nosotros fuimos capaces de convivir con ellos cuando estaban armados, podemos convivir con ellos haciendo política" afirma.

**"La paz se construye con el diálogo, pero no quien hablamos todos los días, sino con quien tenemos diferencias, así hemos asumido las mujeres de Tierralta la superación del machismo",** agrega Arrieta [Campesinos del sur de Córdoba no permitirán fumigación con glifosato ni erradicación forzada](https://archivo.contagioradio.com/campesinos-del-sur-de-cordoba-no-permitiran-fumigacion-con-glifosato-ni-erradicacion-forzada/)

### Reconocerse en el otro

Por su parte la directora ejecutiva de la Fundación Grupo Acción y Apoyo a Personas Trans (GAAT), Laura Weinstein afirma que hay que comenzar a deconstruir y desmentir algo que se ha difundido de forma errónea como la 'ideología de género', y comenzar ' y reconocer al otro y verlo de la forma en que queremos que nos vean".

"El feminismo nos permite ser autónomas y definir nuestros destinos, si hemos sido capaces de deconstruir el género no hay ningún tipo de violencia o guerra que no podamos deconstruir, no les quepa la menor duda"

### Promover espacios con la comunidad 

 "El reto es reconciliarnos, reconocernos desde el entendimiento y comprender que todas y todos somos parte de un solo país", apunta Jenny Flórez, excombatiente que destaca la importancia de generar espacios comunitarios y un acercamiento con los diferentes sectores de la sociedad. [(Le puede interesar: Las mujeres integrantes de FARC avanzan en su reincorporación)](https://archivo.contagioradio.com/mujeres-farc-avanzan-reincorporacion/)

### Proteger al Sistema Integral de Justicia 

Bibiana Peñaralda, integrante de la Red Mariposas Alas Nuevas señala la importancia de reforzar las entidades que integran el Sistema Integral de Justicia: Comisión para el  Esclarecimiento de la Verdad, Unidad de Búsqueda de Personas dadas por Desaparecidas y Jurisdicción Especial para la Paz en particular desde las regiones, **"la paz es territorial pero lo que se está territorializando es la guerra"**.

### La igualdad de género como base 

Finalmente,Victoria Sandino, celebró la posibilidad de proponer soluciones desde las vivencias cotidianas, "desde nuestros sentires y saberes",  que les permiten construir a partir de sus diferencias, destacando la pariedad de género como un avance que debe existir para promover experiencias de paz.

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
