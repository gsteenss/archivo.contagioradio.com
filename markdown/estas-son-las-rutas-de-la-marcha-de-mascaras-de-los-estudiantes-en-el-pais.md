Title: Estas son las rutas de la marcha de máscaras de los estudiantes en el país
Date: 2019-10-31 09:14
Author: CtgAdm
Category: Estudiantes, Movilización
Tags: estudiantes, Movilización, Universidades
Slug: estas-son-las-rutas-de-la-marcha-de-mascaras-de-los-estudiantes-en-el-pais
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/IMG_2872-e1572531118949.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio  
] 

Este jueves 31 de octubre estudiantes universitarios llamaron a realizar una movilización de máscaras a nivel nacional, para manifestarse contra la represión sufrida por los estudiantes por parte del Escuadrón Móvil Antidisturbios (ESMAD), el artículo 44 del Presupuesto General de la Nación (PGN) que afectaría los recursos de las universidades estatales y la corrupción en las instituciones. (Le puede interesar: ["Los compromisos de Claudia López con la Universidad Distrital"](https://archivo.contagioradio.com/los-compromisos-de-claudia-lopez-con-la-universidad-distrital/))

### **Estudiantes de más de 20 Instituciones de Educación Superior marcharán este jueves, estas son sus rutas:  
** 

**En Bogotá**: Las universidades públicas y privadas se concentraron desde las 3 de la tarde, se encontrarán en Héroes a las 5 de la tarde, y posteriormente se dirigirán hasta la Calle 106, donde se verán hasta las 6:30 de la tarde.

**Universidad de Antioquia:** Los estudiantes se concentraron en la Plaza Barrientos a las 4:30 de la tarde.

**Universidad Nacional sede Medellín:** Se realizará el encuentro en el campus, a las 4:30 de la tarde.

**Colegio Mayor de Antioquia:** Iniciará la concentración en el hall del Bloque 'C' desde las 5 de la tarde.

**Universidad del Atlántico:** Encuentro a las 5:30 de la tarde en la Carrera 8 con avenida Murillo, y se desplazarán hasta la Plaza de la Paz.

**Universidad de Sucre:** Se concentrará a las 5:30 pm en la sede Puerta Roja de la Universidad.

**Universidad de Caldas:** La concentración se realizará en la Facultad de Bellas Artes desde las 9 de la mañana.

**Universidad Tecnológica de Pereira:** Los estudiantes se encontrarán en el Parque de los sapos a la 1:30 de la tarde.

La **Universidad de Cundinamarca,** sede Facatativa, se concentrará en la Caracola del parque Jaime Garzón a las 5 de la tarde.

**Universidad de los Llanos:** Se concentrará en el parque Villa Bolívar sobre las 4 de la tarde.

**Universidad Surcolombiana:** Sus estudiantes se encontrarán en Ágoras desde las 10 de la mañana.

**Universidad Industrial de Santander:** Los estudiantes se encontrarán en la entrada de sus sede principal a las 5 de la tarde.

**Universidad de Nariño:** Los jóvenes se concentrarán en la sede de Torobajo y Vipri desde las 5 de la tarde.

**Universidad Popular del Cesar:** Se concentrarán en el campus desde las 4:30 pm.

**Universidad del Valle:** Se realizarán tres puntos de concentración, en privadas a las 2 de la tarde, en Meléndez a las 3 de la tarde y en San Fernando a las 4 de la tarde.

**Universidad del Quindío:** Se encontrarán en el Peatonal de la Universidad a las 5 de la tarde.

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
