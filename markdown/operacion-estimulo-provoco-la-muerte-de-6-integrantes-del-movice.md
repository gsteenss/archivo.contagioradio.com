Title: En desarrollo de la Operación “Estímulo” murieron 6 integrantes del MOVICE
Date: 2015-02-11 17:07
Author: CtgAdm
Category: DDHH, Judicial, Paz
Tags: Actividades ilegales del DAS, Maria del Pilar Hurtado, MOVICE, Operacion estimulo
Slug: operacion-estimulo-provoco-la-muerte-de-6-integrantes-del-movice
Status: published

###### Foto: Contagio Radio 

<iframe src="http://www.ivoox.com/player_ek_4069303_2_1.html?data=lZWjm5iUd46ZmKiak5WJd6KpmJKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRh8LhytHcjbvNsM3VhpewjdvTp8bm0JDRx9GPkbDKqqiyj4qbh4630NPhw8zNs4zGwsnW0ZCRaZi3jpk%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Camilo Villa MOVICE] 

El objetivo de esta operación ilegal del DAS era  “*Involucrar a las Farc como promotora de la marcha del 06-MAR-2008*” organizada por el Movimiento Nacional de Víctimas de Crímenes de Estado. Las tareas realizadas por los agentes del DAS variaron desde la impresión de volantes de las FARC y repartirlos en 20 departamentos, hasta la **ubicación y persecución de las víctimas del Estado que participaron en la movilización.**

Camilo Villa, vocero del MOVICE denunció que la operación “Estímulo” del DAS, **provocó la muerte de 6 personas que participaron en la movilización** y que los crímenes no están siendo investigados. También afirma que desde el mismo año 2008 han venido denunciando la **interceptación de sus líneas telefónicas y sus correos electrónicos sin que hasta el momento se hayan neutralizado este tipo de operaciones**.

Adicionalmente, Villa resalta que durante mucho tiempo **hubo alocuciones presidenciales que señalaban directamente al MOVICE de ser "estafeta" de las guerrillas** y que es necesario establecer cuáles fueron las órdenes concretas que dio el presidente al DAS en ese entonces.

Para el vocero, resulta preocupante que se esté orientando la reflexión en torno a las operaciones ilegales del DAS sólo con el matiz de la entrega de María del Pilar Hurtado, cuando, tras bambalinas, **continúa funcionando el mismo aparato de seguridad y basado en los mismos criterios**. Muestra de ello son los conocidos escándalos como “Andrómeda” o las actividades del **Centro Democrático con el Hacker Andrés Sepúlveda.**

En un comunicado del MOVICE emitido el pasado martes, señala que “El MOVICE, **insiste en que la paz es un derecho que el Estado debe garantizar** y pasa por el reconocimiento de su responsabilidad en hechos victimizantes” haciendo un llamado al Estado colombiano y a la fiscalía para que estos hechos no queden en la impunidad.
