Title: Asesinado Jairo Chilito profesor de ASOINCA en Sucre, Cauca
Date: 2017-04-01 10:03
Category: DDHH
Tags: ASOINCA, Cauca, Docentes
Slug: asesinado-jairo-chilito-profesor-de-asoinca
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/08/asoinca.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo] 

###### [01 Abr. 2017]

Según las primeras informaciones el profesor **Jairo Arturo Chilito Muñoz, docente afiliado de la Asociación de profesores del Cauca -ASOINCA** -  y quién laboraba en el municipio de Sucre, fue hallado muerto en la tarde de este 31 de Marzo cerca de la cabecera municipal. Le puede interesar: [Durante 2016 han asesinado a 45 personas en el Cauca](https://archivo.contagioradio.com/durante-2016-han-asesinado-a-45-personas-en-el-cauca/)

El profesor **laboraba en la Vereda El Paraíso y transitaba por la Vereda La Ceja** en donde fue encontrado asesinado con varias heridas de arma blanca. Le puede interesar: [Red pública de salud del Cauca está lista para recibir a maestros: Asoinca](https://archivo.contagioradio.com/asionca-vuelve-a-paro-porque-gobierno-sigue-incumpliendo/)

**Hasta el momento se desconocen los móviles del asesinato**, sin embargo la zozobra en la comunidad es generalizada puesto que l**as operaciones paramilitares han llegado a los municipios vecinos como Mercaderes**. Le puede interesar: ['Águilas Negras' amenazan a organizaciones y lideres sociales del Cauca](https://archivo.contagioradio.com/panfletos-amenazantes-aguilas-negras/)

Noticia en desarrollo....

###### Reciba toda la información de Contagio Radio en [[su correo]
