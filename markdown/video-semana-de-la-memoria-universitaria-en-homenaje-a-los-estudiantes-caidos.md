Title: Video | Semana de la Memoria Universitaria en homenaje a los estudiantes caídos
Date: 2019-05-16 17:06
Author: CtgAdm
Category: Sin Olvido, Video
Tags: memoria, Semana de la memoria universitaria, Universidad Nacional
Slug: video-semana-de-la-memoria-universitaria-en-homenaje-a-los-estudiantes-caidos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/semana-de-la-memoria-universitaria-homenaje-a-los-estudiantes-caídos.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

Así se conmemoró la semana de la memoria universitaria, un espacio que  busca reivindicar y conmemorar **35 años de una serie de sucesos violentos contra estudiantes, profesores y trabajadores universitarios**, particularmente los ocurridos en 1984.
