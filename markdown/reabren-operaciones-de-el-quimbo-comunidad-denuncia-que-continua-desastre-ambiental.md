Title: Reabren operaciones de El Quimbo: comunidad denuncia que continúa desastre ambiental
Date: 2016-02-23 13:16
Category: Ambiente, Nacional
Tags: El Quimbo, EMGESA, Paro Nacional
Slug: reabren-operaciones-de-el-quimbo-comunidad-denuncia-que-continua-desastre-ambiental
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/02/Quimbo-e1456249525448.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: El Heraldo 

<iframe src="http://co.ivoox.com/es/player_ek_10545059_2_1.html?data=kpWilpqUeZqhhpywj5WcaZS1k5mah5yncZOhhpywj5WRaZi3jpWah5yncbPZwsffx9OPs9HZ08bQy9TSqdSfxcqYp9GPldbdzsfch5ilb8Tjztrby8nFqIzYxtPi0MjNpYzl1pKSmaiRh9Di1cbUy9SPlsLYytSYj4qbh46o&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Miller Dussan, ASOQUIMBO] 

###### [23 Feb 2016] 

El Tribunal Administrativo del Huila autorizó por un tiempo de 6 meses, que EMGESA mantenga prendida la represa El Quimbo, pese a que se sabe que la degradación del agua está por debajo de los niveles permitidos y continúan evidenciándose toneladas de peces muertos, como se denuncia desde ASOQUIMBO.

**“Nos preocupa cómo el Tribunal mantiene el silencio frente al memorial que envían los pescadores artesanales, mil familias perdieron el 100% de su actividad de pesca**”, dice Miller Dussán, investigador de ASOQUIMBO, quien añade que la medida que ordenó el magistrado Ramiro Aponte Pino continúa en la misma línea de “decisiones que son excluyentes y clasistas", pues las familias quedaron en la miseria absoluta y solo se favoreció intereses económicos.

Los pescadores artesanales habían solicitado al Tribunal, mantener la medida cautelar hasta que no se garantizara el restablecimiento de los derechos constitucionales al trabajo, la vida digna y al ambiente sano, “pero el tribunal prefirió dejar desamparadas a las familias”, señala el investigador.

De acuerdo con el seguimiento que se ha hecho desde Asoquimbo, se ha logrado identificar que **Emgesa contrató pescadores que están pendientes de los peces que aparecen muertos, para que estos, inmediatamente sean recogidos y sepultados,** con el fin de que no exista evidencia de las toneladas de animales muertos por la activación de la represa. Lo que es difícil de demostrar, teniendo en cuenta que, según, Miller Dussán, que ni la Agencia Nacional de Licencias Ambientales, ANLA y tampoco la Corporación Autónoma Regional del Magdalena realizan un seguimiento a la situación.

Frente a esta situación, la comunidad de familias afectadas por EMGESA, analiza interponer el recurso de apelación e instaurar una acción de tutela exigiendo el restablecimiento de los derechos constitucionales a los cuales no se refirió el tribunal administrativo, sin embargo, el tiempo ha hecho entender a la población que la demanda sería negada como siempre ha sucedido, indica Dussán.

**“Nos queda seguir en la resistencia, el 14 de marzo, día mundial contra las represas y en defensa del agua saldremos a protestar contra las decisiones de estos jueces** que uno se da cuenta que no favorecen a las personas afectadas por los proyectos hidroeléctricos”. En Pitalito, Garzón y Neiva desde las 9 am se iniciarán las jornadas de protesta, que se unirán unirá el 17 de mayo al paro nacional que organizan diversos gremios del país.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u)  o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](https://archivo.contagioradio.com/). 
