Title: Las razones para pedir la salida de Guillermo Botero del Min. Defensa
Date: 2019-10-16 19:05
Author: CtgAdm
Category: Nacional, Política
Tags: Guillermo Botero, Ministro de defensa, Moción de Censura, Roy Barreras
Slug: las-razones-para-pedir-la-salida-de-guillermo-botero-del-min-defensa
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/Botero-1.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/Botero-1-1.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/Botero-1-2-e1571270317183.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: @GuillermoBotero  
] 

Luego del debate de control político "Situación actual de la seguridad en Colombia" contra el ministro de defensa Guillermo Botero, el senador Roy Barreras en conjunto con otros senadores, radicaron una proposición de moción de censura en su contra, por considerar que no responde a las necesidades del cargo. Aunque esta es la segunda vez que se intenta una moción contra Botero, Barreras señaló que en esta ocasión la medida cuenta con más apoyo y razones de peso para ser llevada a cabo.

### **Nuevas razones para buscar la salida de Guillermo Botero** 

Roy Barreras, senador que impulsó la iniciativa explicó que la misma trata de alertar sobre los riesgos que vive el país, "que antes no teníamos y ahora sí tenemos". En ese sentido, en materia de seguridad externa, señaló que el país ahora está en una gran incertidumbre sobre lo que pueda pasar con Venezuela, "y estamos en riesgo constante de una confrontación militar indeseable". (Le puede interesar: ["Controlar Ltda, la empresa de Botero que se beneficiaría con la política de Defensa y Seguridad"](https://archivo.contagioradio.com/controlar-botero-beneficiaria-politica-defensa/))

Asimismo, señaló que tampoco hay confianza sobre la capacidad de defensa que tiene el país, porque el área de inteligencia quedó en cuestión  "después del ridículo que hicieron ante Naciones Unidas con informes falsos, con esas llamadas fotos de contexto". Por su parte, Barreras dijo que en materia de seguridad interna las cifras no respaldan a Botero: Después de recibir al ELN con 1.500 hombres, ahora dice que son 4.300, es decir que han triplicado su capacidad, cuando hasta 2018 el gobierno Santos había dejado una mesa de conversaciones de paz, y estaba "listo un cese bilateral".

Respecto a las disidencias, el senador señaló que según cifras del propio Botero, las mismas pasaron de tener 300 hombres a 1.300. Y en cuanto a cultivos de uso ilícito, cuestionó el programa de erradicación forzada llevado a cabo por este Gobierno, porque no está funcionando y hoy Colombia tiene las mismas hectáreas que antes, pero con un costo de "17 vidas humanas, más los miles de millones que vale la erradicación". Barreras concluyó que "después de 4 años de hablar de paz, llevamos 1 año hablando de una guerra con Venezuela, de las disidencias y del ELN".

### **"Este es el (ministro) más flojo, y es el que más está fallando en una materia muy grave"** 

Aunque Barreras consideró que la crítica podría extenderse a todo el Gobierno por su manejo de la defensa nacional, declaró que los presidentes tienen un gabinete de expertos que los asesoren en los principales temas del país porque no pueden ser especialistas en todo. Por lo tanto, aseguró que frente a la defensa nacional debe estar alguien que sepa de eso "y este no sabe", y añadió que "este es el (ministro) más flojo, y es el que está fallando en una materia muy grave que es la seguridad de todos los colombianos".

El senador por el partido de la U sostuvo que esta será la primera vez que se intente aprobar una moción de censura contra Botero en el senado, y contó con el aval de 15 senadores de todos los partidos, excepto el partido de Gobierno, por lo que auguró una buena posibilidad de 'estrenar' esta figura. Luego del debate de control político, el ministro tendrá una segunda oportunidad para explicar su gestión y 10 días después se realizará la votación para decidir su salida del cargo. (Le puede interesar: ["Fuerzas Militares están en crisis y deben superarla con reformas profundas"](https://archivo.contagioradio.com/fuerzas-militares-estan-en-crisis-y-deben-superarla-con-reformas-profundas/))

> [\#SeguridadNacional](https://twitter.com/hashtag/SeguridadNacional?src=hash&ref_src=twsrc%5Etfw) En seguridad Externa: Han escalado el conflicto con Venezuela y puesto en riesgo a Colombia. En Inteligencia frente a Venezuela: Han fracasado dándole informe para la vergüenza ante la ONU al Presidente. No previeron (o si?) que los rastrojos guiarán a Guaido?
>
> — Roy Barreras U1 (@RoyBarreras) [October 15, 2019](https://twitter.com/RoyBarreras/status/1184212779391213569?ref_src=twsrc%5Etfw)

<p>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
</p>
### **En contexto: Tres ministros de Duque han vivido el proceso de moción de censura, para Botero es la segunda vez** 

El primero en vivir un proceso de moción de censura fue Alberto Carrasquilla, que a finales del año pasado vio cuestionado su nombramiento como ministro de economía, por su participación en los denominadas 'bonos agua'. El senador Jorge Robledo estuvo al frente de esta iniciativa que no prosperó en Cámara de Representantes ni Senado. (Le puede interesar:["‘El que la hace la paga’ no aplica para el ministro Carrasquilla"](https://archivo.contagioradio.com/paga-ministro-carrasquilla/))

Luego, a mediados de este año y tras revelarse directrices al interior del Ejército Nacional que podrían generar violaciones a los derechos humanos, Guillermo Botero fue citado a un debate de control político y una posterior moción de censura que contó con el apoyo de partidos de oposición, pero no con el de partidos declarados independientes como Cambio Radical o Liberal. Por esta razón, el ministro se libró de la moción en la Cámara de Representantes. (Le puede interesar: ["Botero no es una persona competente ni ética para ser ministro de Defensa: David Racero"](https://archivo.contagioradio.com/botero-no-competente-ministro-defensa/))

La tercera ministra que pasó por este proceso fue Ángela María Orozco, a cargo del Ministerio de Transporte y cuestionada por la relación laboral que tuvo con el Grupo Aval en el pasado, tomando en cuenta que recientemente uno de los bancos que integran el Grupo intentó beneficiarse con una indemnización que ascendía a los 1,2 billones**,** luego de que se ordenara la liquidación del contrato para la construcción de la Ruta del Sol II. Las mayorías del Senado no se lograron conjurar en torno a este propósito, y Orozco también se mantuvo en su cargo.

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe><iframe id="audio_43274672" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_43274672_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
