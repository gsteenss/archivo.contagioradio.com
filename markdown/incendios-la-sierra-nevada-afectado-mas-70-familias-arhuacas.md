Title: Incendios en la Sierra Nevada han afectado a más de 70 familias Arhuacas
Date: 2019-02-28 12:53
Category: Ambiente, Comunidad
Tags: incendios forestales, Pueblo Arhuaco, Sierra Nevada
Slug: incendios-la-sierra-nevada-afectado-mas-70-familias-arhuacas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/02/Captura-de-pantalla-32.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Twitter] 

###### [28 Feb 2019] 

El pueblo Arhuaco de Séynimin, ubicado en las montañas de la Sierra Nevada, enfrenta una emergencia social y ambiental después de que, en la tarde del 24 de febrero, fuertes incendios forestales consumieran **la totalidad de las casas, enseres y cultivos de por lo menos 70 familias.**

Según Arukín Torres, líder del pueblo Arhuaco, el incendio comenzó a las 3 de la tarde el domingo, cuando una pequeña fogata en el pueblo se descontroló a causa de los fuertes vientos y provocó un gran incendio que arrasó con más de 800 hectáreas de la montaña, en la zona oriental del territorio Arhuaco. Entre los daños están incluidos la destrucción de escuelas, puestos de salud, las oficinas de la autoridad local, la casa de reflexión y la casa de reuniones comunitarias.

"Este tipo de incendio no se había vivido en la Sierra Nevada," afirmó Torres. "Realmente estamos consternados por la situación que estamos pasando, que es accidental, pero que ha sido difícil de controlar."

Desde la casa indígena en Valledupar, Cesar, el Comité de Emergencia del Resguardo Arhuaco se encuentra recolectando ayudas para entregar a las comunidades, sin embargo, ha sido difícil transportar los paquetes al pueblo de Séynimin por el clima y la geografía adversa de la Sierra.

Según Torres, los helicópteros del Ejército no son lo suficientemente grandes para soportar los fuertes vientos que actualmente azota la zona alta. "Eso limita las acciones efectivas que se puedan realizar".

Por tal razón, las comunidades instan al Gobierno a coordinar con la gobernación del Cesar y las administraciones municipales la adecuación de las herramientas y los recursos necesarios "para superar la emergencia y el restablecimiento de las condiciones para el goce de los derechos fundamentales" de las poblaciones afectadas.

Asimismo, las autoridades locales anuncian que lograron apagar las llamas en un 95% y por lo tanto, el Resguardo Arhuaco de la Sierra, junto con la Bancada Alternativa del Congreso, le piden al Gobierno que enfoque su atención en las 600 personas, quienes se encuentran albergadas en casas aledañas del pueblo. Exigen que el Gobierno declare la **emergencia social, ambiental, económica y cultural en Séynimin y la entrega de ayudas humanitarias y financieras** a las familias afectadas.

<iframe id="audio_32899214" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_32899214_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
