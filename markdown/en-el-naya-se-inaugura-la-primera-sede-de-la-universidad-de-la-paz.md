Title: En el Naya se inaugura la primera sede de la Universidad de la Paz
Date: 2016-06-10 14:58
Category: Educación, Nacional
Tags: Naya, paz, proceso de paz, víctimas
Slug: en-el-naya-se-inaugura-la-primera-sede-de-la-universidad-de-la-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/06/Naya-universidad-e1465587855730.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Comisión de Justicia y Paz 

###### [10 Jun 2016]

Se inauguró la primera sede de la Universidad de la Paz, una propuesta de las comunidades de la Red CONPAZ y **presentada a la Mesa de La Habana y al proceso con el ELN,** cuya sede se ubica en el corregimiento San Francisco, Naya.

**Más de 70 personas entre indígenas, afrocolombianos y mestizos que participaban en la 'Cátedra Abierta Sujetos Territoriales de Paz'**, participaron en la apertura de la primera sede de una universidad rural, con cantos y rituales espirituales, que fueron la bendición del lugar que inicialmente cuenta con hectárea y media para que allí se construya toda la planta física en la que se implementarían cursos técnicos y profesionales.

En la actualidad la sede a la orilla del río Naya cuenta con un espacio de alojamiento para 100 personas, dos salones, cocina y acceso a internet. Allí las comunidades ofrecen el lugar para que la Comisión de Esclarecimiento de la Verdad pueda sesionar. La sede del Naya, lleva el nombre de Juana Angulo, la afro asesinada en 2001 por paramilitares.

Uno de los posibles modelos en la construcción sería la Universidad Rural de Sucumbíos en Ecuador, que  ha sido construida con parámetros de sustentabilidad que satisfacen las necesidades de las personas y respetan el ambiente.

El enfoque técnico y académico está articulado en el momento histórico de la construcción de la paz. Tres ejes teóricos fundamentan la visión de la Universidad de Paz: **Participación y Derechos Político; Territorio y Ambiente y Género y otras identidades.**

La Red CONPAZ con apoyo de la Comisión de Justicia y Paz y en este caso con la decisión del Consejo Comunitario está buscando establecer **convenios con universidades públicas y privadas para dar inicio a algunas de las carreras ya acreditadas. **

<iframe src="http://co.ivoox.com/es/player_ej_11857752_2_1.html?data=kpall5ybeZOhhpywj5WcaZS1kpWah5yncZOhhpywj5WRaZi3jpWah5yncaLtxcaYrtreb6jVzsfcw5CRb6TDr7WuvJCRaZi3jqjc0NnFq8rjjLfOxs7Tb46ZmKialg..&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)
