Title: 2700 litros de agua cuesta la confección de una camiseta
Date: 2017-05-10 17:33
Category: Ambiente, Voces de la Tierra
Tags: Agua, cambio climatico, contaminación
Slug: litros-agua-camiseta
Status: published

###### Foto: Ecoosfera 

###### [10 May 2017] 

Cerca del 75% del planeta está cubierto por agua, sin embargo **sólo el 3% de la misma es dulce, y apenas un 1% es apta para el consumo humano.** Es decir que se trata de un recurso que fácilmente puede acabarse, y de hecho se encuentra en riesgo, debido a factores como la sobrepoblación y el cambio climático.

En medio de ese panorama es claro que el mundo incumplirá el Objetivo de Desarrollo Sostenible número 6, que insta a garantizar la disponibilidad de agua, su gestión sostenible y el saneamiento para todos, teniendo en cuenta que aproximadamente **700 millones de personas en todo el mundo tienen dificultades para acceder al agua potable.**

A su vez, el 90% del  agua que usa el ser humano, se devuelve a naturaleza sin ser tratada debidamente. Es decir que el agua que se está dejando nuevamente en la tierra y que usarán las nuevas generaciones, está contaminada. [(Le puede interesar radiografía del derecho fundamental al agua)](https://archivo.contagioradio.com/la-radiografia-del-derecho-fundamental-al-agua-colombia/)

### **[En el futuro]** 

El panorama para el 2017 no parece mejorar. De acuerdo con Naciones Unidas, para este año la demanda de agua aumentará significativamente en el mundo, especialmente por el aumento de la población y la realización de algunas actividades que requieren grandes cantidades de agua, como sucede con **el sector agropecuario que es responsable del 70% de gasto del líquido vital.** No obstante se ha analizado que el sector industrial, la producción de energía y la urbanización acelerada contribuirán al aumento de la demanda.

Como se sabe el líquido vital consumible proviene de las fuentes de agua dulce. Esta es la que permite la producción de alimento, pero también, de donde sale el algodón y la producción de energía eléctrica.

No obstante el consumo desmedido actual le permite a a la WWF concluir que** hoy la humanidad estaría necesitando 1,5 planetas, para satisfacer sus niveles de consumo,** según concluye en su "Informe Planeta Vivo". En esa misma línea, el Foro Económico Mundial ha calculado que para el 2030 habrá una demanda 40% más alta, que el planeta no podrá suministrar. [(Le puede interesar: Tu plato de carne contamina más que tu auto)](https://archivo.contagioradio.com/tu-plato-de-carne-contamina-mas-que-tu-auto-ambiente-y-sociedad/)

### **[Consumo diario de agua:]** 

-   3 litros de agua para producir litro y medio de agua en botella
-   40 litros para producir (1) ensalada
-   140 litros de agua para producir (1) taza de café
-   185 litros de agua para producir (1) kilo of tomate
-   330 litros de agua para producir (1) pan
-   960 litros de agua para producir (1) litro de vino
-   1100 litros para producir (1) litro de leche
-   1900 litros para producir (1) kilo de pasta
-   1500 litros para producir (1) kilo de azúcar
-   2700 litros para la confección de una camiseta de algodón
-   3400 litros para producir (1) kilo de arroz
-   11.000 litros para producir (1) par de pantalones jeans
-   15.000 litros para producir (1) kilo de carne

Estas cifras fueron reiteradas en medio de la **Convergencia Fe y Clima** que se desarrolló del 1 al 4 de Mayo en la ciudad de Rio de Janeiro en Brasil y en la que **jóvenes representantes de diversas religiones presentes América reiteraron su compromiso, basado en la fe, con la defensa del agua** y la tierra, así mismo exigieron que los gobiernos asuman compromisos más radicales y contundentes para frenar el calentamiento global.

En el marco de la convergencia y concretamente en el Foro sobre la Bahia de Guanabara en Río de Janeiro, convocado por el Instituto de Estudios de la Religión, [ISER](http://www.iser.org.br/site/), una de las mas contaminadas del país suramericano se realizó también un acto simbólico de sacralización del agua.

Neddy Astudillo, representante de [GreenFaith](http://www.greenfaith.org/) para América Latina, y quien compartió las escandalosas cifras con los y las asistentes aseguró que es necesario **hacer cambios urgentes para que el agua no siga siendo una de las principales víctimas de la actividad humana.**

###### Reciba toda la información de Contagio Radio en [[su correo]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio]{.s1}](http://bit.ly/1ICYhVU)[.]{.s1}
