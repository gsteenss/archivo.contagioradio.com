Title: "Noche Oscura Lugar Tranquilo" en La Candelaria
Date: 2017-11-08 10:02
Category: eventos
Tags: Bogotá, teatro, teatro la candelaria
Slug: noche-oscura-teatro
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/10/image001-3.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto:Teatro La Candelaria 

##### 30 Oct 2017 

[En cuatro funciones llega al teatro La Candelaria la obra **Noche Oscura Lugar Tranquilo** del director César “Coco” Badillo y el grupo Móvil.]

[La obra ganadora de la Beca para montaje de dramaturgias colombianas del Ministerio de Cultura 2017 y del Premio Nacional de Dramaturgia 2015, es **una historia llena de misterio y ovnis, que habla del dolor profundo**, de la pérdida de la certeza, la desconexión con la identidad nacional y de la ruptura de las estructuras tradicionales.]

[La pieza se centra en  tres personajes que intentan sobreponerse a una noche oscura, a través de la búsqueda espiritual y mística, para hallar  la salida de la crisis que sufren y no terminar en el abismo. ]

[Tener la esperanza de encontrar la solución a su vacío interior huyendo; denunciando el vacío de la existencia y una situación de desesperanza, aferrarse a la idea de haber atravesado la noche y encontrarse ya del otro lado, en donde siempre son las tres la mañana  y quizás aparezcan los ovnis.]

[Esta obra con carga poética que aborda inquietudes de una Colombia contemporánea se estará presentando del **9 al 12 de noviembre a las 8 pm** y el domingo 12 a las 6 pm. La boletería tendrá un costo \$24.000  para público general y \$12.000 para estudiantes y adultos mayores.]
