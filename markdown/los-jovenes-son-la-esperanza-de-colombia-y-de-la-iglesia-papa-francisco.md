Title: Papa pide a los jóvenes enseñar la cultura del encuentro y el perdón
Date: 2017-09-07 16:15
Category: Nacional, Paz
Tags: Discurso del Papa, Jovenes de Colombia, Papa, Papa Francisco, visita del Papa
Slug: los-jovenes-son-la-esperanza-de-colombia-y-de-la-iglesia-papa-francisco
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/09/papa-y-jovenes.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: El Tiempo] 

###### [07 Sept. 2017] 

Desde las 4 a.m. miles de jóvenes arribaron desde muchos lugares de Colombia y de América Latina a la Plaza de Bolívar, centro de la capital para esperar que el Papa Francisco se asomará por el balcón del Palacio Cardenalicio y se dirigiera a ellos. Sin importar el inclemente frío de la madrugada, el sol que comenzó a divisarse y los cerca de 3 filtros de seguridad que debieron cruzar, ellos y ellas expresaban a través de sus rostros la alegría y la ilusión de escuchar al sumo pontífice.

Pero fue tan solo a las 11 de la mañana en la que **Francisco pudo darse cuenta de todos los jóvenes que lo aguardaban desde hacía más de 6 horas,** sonriente saludó a los asistentes y abrió su discurso dando gracias a Dios por estar de visita en Colombia y por todo el bien que ha logrado hacer a este país.

“Vengo a aprender de ustedes, de su fe, de su fortaleza ante la adversidad (…) **el Señor no es selectivo, no excluye a nadie, el señor abraza a todos** y todos somos necesarios e importantes para él” recalcó Francisco. Quien además aseguró que Dios anima al pueblo a seguir buscando la Paz.

### **El Papa animó a incentivar la cultura del encuentro** 

"Basta un rico café, un refajo, o lo que sea, como excusa para suscitar el encuentro. Los jóvenes coinciden en la música, en el arte... ¡si hasta una final entre el Atlético Nacional y el América de Cali es ocasión para estar juntos! Ustedes pueden enseñarnos que la cultura del encuentro no es pensar, vivir, ni reaccionar todos del mismo modo; es **saber que más allá de nuestras diferencias somos todos parte de algo grande que nos une y nos trasciende, somos parte de este maravilloso País".**

Luego animó a los jóvenes a seguir trabajando por su país, por el futuro y aseguró que para él siempre es un motivo de alegría ver a tantos jóvenes “**en este día les digo por favor, mantengan viva la alegría, es signo del corazón joven** y si ustedes mantienen viva esa alegría con Jesús, nadie se las puede quitar”.

**Mónica Díaz integrante de la Pastoral Juvenil de la Diócesis de Girardot** quien asistió al evento manifestó que en la Plaza de Bolívar la alegría que se vivió era indescriptible “esto es una emoción inexplicable, son muchos jóvenes de todas partes”. Le puede interesar: ["El Papa nos está devolviendo a lo central del Evangelio"](https://archivo.contagioradio.com/46343/)

### **“En el mundo no todo es blanco ni negro”** 

Así lo dijo el Papa a los jóvenes, para quien también les aseveró que la vida cotidiana se resuelve en una amplia gama de tonalidades grises, pero que eso los puede llevar a una atmósfera de relativismo del que hay que tener cuidado.

“Los jóvenes tienen la capacidad no solo de juzgar y señalar desaciertos sino esa otra capacidad que es **la de comprender que incluso detrás de un error hay un sinfín de razones, de atenuantes**. Cuanto los necesita Colombia para ponerse en los zapatos del otro y lograr comprender”.

Por su parte Díaz manifiesta que tal y como lo dice el lema del Papa Francisco hay que dar el primer paso “y **como jóvenes tenemos que hacerlo, tenemos que cuidar la naturaleza, porque el egocentrismo no nos deja ver más allá** y como si el mundo no nos importara y el compromiso es que como católicos tenemos que comprometernos y cuidar lo que nos rodea”.

### **Hay que perdonar al prójimo** 

Dentro de su discurso que duró cerca de 20 minutos, el Papa les pidió a los jóvenes que miren adelante sin el lastre del odio “porque ustedes nos hacen ver todo el mundo que hay por delante. **Ustedes enfrentan el enorme desafío de ayudarnos a sanar nuestro corazón (…) los jóvenes son la esperanza de Colombia y de la Iglesia”.**

El Papa concluyó reconociendo la presencia de los enfermos, los más pobres, los marginados, necesitados y a los que estaban en sus casas “todos están en mi corazón”. Le puede interesar: [Visita del Papa debería aportar a la reconciliación](https://archivo.contagioradio.com/46297/)

<iframe id="audio_20751511" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_20751511_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU).

<div class="osd-sms-wrapper">

</div>
