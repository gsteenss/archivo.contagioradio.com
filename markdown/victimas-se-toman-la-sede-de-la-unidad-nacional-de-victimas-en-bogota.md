Title: Víctimas se toman la sede de la Unidad Nacional de Víctimas en Bogotá
Date: 2015-05-21 11:59
Author: CtgAdm
Category: Movilización, Nacional
Tags: Incumplimiento procesos reparación víctimas Bogotá, Reparación y retorno víctimas Montes de María, Toma sede Unidad Nacional de víctimas Bogotá, Unidad nacional de Víctimas Bogotá, Víctimas Colombia
Slug: victimas-se-toman-la-sede-de-la-unidad-nacional-de-victimas-en-bogota
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/05/2_257.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: RcnNoticias.com 

###### **Entrevista con [Fidel Serpa], víctima y activista de la toma de la sede de la Unidad de Víctimas en Bogotá:**

<iframe src="http://www.ivoox.com/player_ek_4528509_2_1.html?data=lZqfmpqUfY6ZmKiakpuJd6KmkpKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRmNDhwpDRx5DQpYznxsnSjcnJb83VjLrby8nFqIzYxpDDh6iXaaK4xNnWz8bXb8XZjKfcydTYaaSnhqaej4qbh4630NPhw8zNs4zGwsnW0ZCRaZi3jpk%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>  
**Víctimas se tomaron la sede de la Unidad Nacional de Víctimas en Bogotá** para exigir el **cumplimiento de los acuerdos** realizados con la institución en materia de reparación y retorno de los afectados.

Representaciones de **6 comunidades de Bolívar, Sucre y Santander**, en las que se han **incumplido los acuerdos,** se tomaron ayer la sede en Bogotá, con el objetivo de entregar un pliego de peticiones y **negociar el cumplimiento integro** de lo que por derecho les corresponde.

De esta forma, a parte de intentar que se cumplan los derechos de las víctimas, también se denuncia y se **visibiliza pacíficamente la situación de grave incumplimiento** por parte del Estado Colombiano de los acuerdos.

Las principales quejas de las víctimas hacen referencia a la **precariedad en temas de salud, laboral y educación, así como el pago de las ayudas que apenas habría sido de un 3% de lo acordado.** Por otro lado, también existen graves quejas contra los funcionarios que según las víctimas los tratan como objetos y no como sujetos de derechos plenos.

Según **Rosa Serpa**, víctima y participante de la toma, “...Venimos desde muy lejos porque **nos sentimos revictimizados.** Todo está mal en nuestros procesos, desde la falta de preparación de los funcionarios que nos guían, hasta los incumplimientos en los procesos de reparación que no han sido concretados....” .

Entre las representaciones de víctimas de la toma, hay casos como el del corregimiento de **Riachuelo, en Charalá, Santander,** donde la Unidad invirtió 1**2 millones de pesos en instrumentos musicales que nadie sabe utilizar y que aún no han sido usados**.

También desde esta comunidad denuncian el incumplimiento de los procesos de reparación colectiva donde solo se ha puesto **una placa en la plaza del pueblo, sin ni siquiera tramitar la ayuda humanitaria.**

Otro ejemplo es el del colectivo **Liga de Mujeres Desplazadas,** que denuncia la **muerte de una de sus miembros el año pasado a causa del hambre** sin haber recibido aún ayuda humanitaria por parte de la Unidad.

Entre las reivindicaciones de las víctimas, está la de **cambiar todos los gerentes y directores, ya que su trato en todo momento ha sido reprochable** debido a que les han presionado para firmar acuerdos que nunca han cumplido.

Según **Fidel Serpa**, víctima proveniente de los **Montes de María**, **no se van a ir hasta que se acuerde el cumplimiento de sus peticiones.** En su caso, fue **desplazado** hace mas de **15 años** y aún **no ha recibido las ayudas para el retorno** y su situación actual es de máxima precariedad en materia de salud y trabajo.

   
 
