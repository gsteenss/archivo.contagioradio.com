Title: Tortugas icoteas: las mayores víctimas en Semana Santa
Date: 2015-04-01 16:37
Author: CtgAdm
Category: Animales, Nacional
Tags: Animales, animales en vías de extinción, Barranquilla, Fauna silvestre, Icotea, Maltrato animal, Policía Ambiental, Semana Santa, Sociedad Protectora de Animales, Tortuga
Slug: tortugas-icoteas-las-mayores-victimas-en-semana-santa
Status: published

##### Foto: [diarioadn.co]

<iframe src="http://www.ivoox.com/player_ek_4290787_2_1.html?data=lZemkpyce46ZmKiakpyJd6KokpKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRmNDm1drUw9iPrcTj1crO1ZDaaaSnhqaxxdnNscLnjMrbjbjJscLiwpDAw9PYpY6ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Luz Stella Ordoñez, Sociedad Protectora de Animales de Barranquilla] 

<iframe src="http://www.ivoox.com/player_ek_4290806_2_1.html?data=lZemkp2Ueo6ZmKiakp2Jd6Kkk5KSmaiRdo6ZmKiakpKJe6ShkZKSmaiRmNDm1drUw5Ctp9DoxsaSlaaPuoa3lIqupsjYrc7VjMnSjdHFb7TZzsbbw5C3pc%2FowpDS0JCmpdPmwtPe15KJe6ShpNTb1sbLrdCfs8bRy9SPcYarpJKh&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Eber González, Jefe de Policía Ambiental de Barranquilla] 

En Semana Santa, el tráfico de fauna silvestre aumenta drásticamente, debido a la temporada de vacaciones y al no consumo de carne roja, que da paso a otras prácticas alimenticias como **el consumo de la tortuga icotea** que una de las principales víctimas de las celebraciones religiosas de estos días.

Este especie, habita en la Costa Atlántica colombiana, **y fue declarada por la Unión Internacional para la Conservación de la Naturaleza como una especie amenazada, s**in embargo, siguen siendo víctimas de los traficantes de animales silvestres, quienes las venden para ser consumidas, lo que conlleva a una muerte cruel y violenta, por medio de la cual  se les extrae la poca carne que tienen, por ejemplo, una tortuga adulta de esa especie, pesa aproximadamente **1.500 gramos, y sólo tiene 20 por ciento de carne en su cuerpo.**

Para, Luz Stella Ordoñez, Sociedad Protectora de Animales de Barranquilla se trata de **“un acto cruel e ilegal, que es un delito y se castiga** a quien consuma, trafique y tenga en cautiverio estos animales silvestres”. Ella, califica de “aberrante” la forma como los traficantes matan a la tortuga, con el objetivo de que las personas consuman unos cuantos gramos de carne, ya que  las condiciones de muerte de estos animales significan una tristeza y una “vergüenza para el ser humanos”, según lo expresa Ordoñez.

De acuerdo al comandante Eber González, Jefe de Policía Ambiental de Barranquilla, al inicio de Semana Santa, se han rescatado **151 especies silvestres víctimas de tráfico de especies, entre las cuales hay 45 tortugas icoteas.**

Sin embargo, tanto Luz Stella, como el comandante González, resaltan que hace 2 o 3 años las cifras eran mucho más alarmantes, ya que **los traficantes quemaban los árboles del Parque Natural Salamanca para sacar cientos de estos animales** que luego serían traficados.

Pero gracias a la actuación de las entidades ambientales y protectoras de animales se logró recuperar el parque, y por eso ya no es tan alto el número de especies incautadas, teniendo en cuenta que se ha realizado operativos para rescatar a las tortugas, iguanas y otras especies. Además, se desarrollan campañas preventivas de educación para que las personas comprendan la gravedad de la situación.

Finalmente, concluye Ordoñez, **“se debe respetar el derecho a la vida en todas sus manifestaciones”**, y quienes estén de vacaciones deben entender que hacer parte de la cadena de tráfico de especies, constituye un delito tipificado en la Ley 599 del 2000 del Código Penal. Así mismo, se puede denunciar estas prácticas con la Policía Ambiental o con la Sociedad Protectora de Animales de Barranquilla.
