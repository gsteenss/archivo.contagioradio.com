Title: “Se debe acelerar el desescalamiento" Frente Amplio por la Paz
Date: 2015-08-26 15:54
Category: Nacional, Paz
Tags: Alberto Franco, alirio uribe, Angela Maria Robledo, cese unilateral de las FARC, Constituyentes por la Paz, Conversaciones de paz de la habana, Coordinador Nacional Agrario, Cumbre Agraria, Desescalamiento del conflcito, desminado humanitario, DIPAZ, Frente Amplio por la PAz, Iván Cepeda, ONIC
Slug: se-debe-acelerar-el-desescalamiento-frente-amplio-por-la-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/08/rueda-de-prensa.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Iván Cepeda 

###### [26 Ago 2015] 

El informe que presenta el Frente Amplio por la Paz del primer mes de cese unilateral de las FARC y desescalamiento de las acciones por parte de las FFMM da cuenta de que el mes de Julio fue el menos violento en los últimos 40 años.

Sin embargo los representantes del Frente Amplio por la paz, de las Iglesias y los Movimientos por la Constituyentes mostraron su preocupación frente al avance del paramilistarismo en las regiones de Colombia y la estigmatización a grupos y organizaciones políticas.

La situación de los defensores de derechos humanos, la criminalización de la protesta social y la persecución al movimiento social, los bombardeos de las FFMM a los campamentos de guerrillero en tregua, el reconocimiento del asesinato de Genaro García y el riesgo de la operaciones de desminado humanitario por la presencia paramilitar son otros de los temas que evalúa el informe.

Uno de los elementos que se resalta de este informe es que es el resultado del trabajo conjunto de verificación de comunidades indígenas, campesinas y organizaciones sociales de diversos lugares del país.

El informe hace unas recomendaciones generales al Estado y a los actores del conflicto.

A la Fiscalía General investigar y judicializar las violaciones a los Derechos humanos en donde hayan participado funcionarios del Estado y fuerzas militares. A los miembros en contienda, principalmente a los miembros de las Fuerzas Militares para aplicar la excepción de inconstitucionalidad y desobediencia frente a cualquier acto u orden que ponga en peligro la paz. A que las FARC-EP mantengan el respeto por la vida de los colombianos y por último a que se garanticen condiciones de dignidad y los derechos de todos los presos políticos.

[V Informe Cese Unilateral Al Fuego Frente - Constituyentes e Iglesias Final](https://es.scribd.com/doc/276526618/V-Informe-Cese-Unilateral-Al-Fuego-Frente-Constituyentes-e-Iglesias-Final "View V Informe Cese Unilateral Al Fuego Frente - Constituyentes e Iglesias Final on Scribd") by [Contagioradio](https://www.scribd.com/contagioradio "View Contagioradio's profile on Scribd")

<iframe id="doc_24637" class="scribd_iframe_embed" src="https://www.scribd.com/embeds/276526618/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-tuSPxiQFvgZ4V0G1l3o8&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="0.7729220222793488"></iframe>
