Title: Desde hoy se vive la 'Festa' del teatro en Bogotá
Date: 2016-03-12 09:45
Category: Cultura, eventos
Tags: Festival de Teatro Alternativo de Bogotá, teatro la candelaria
Slug: festival-de-teatro-alternativo-de-bogota
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/03/SI-EL-RIO-HABLARA-2-e1457731613286.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Carlos Lema 

<iframe src="http://co.ivoox.com/es/player_ek_10768814_2_1.html?data=kpWkmJ2cdZWhhpywj5acaZS1kZyah5yncZOhhpywj5WRaZi3jpWah5yncaXZ1MnSjc3TvYznxpDjy9vJb83VjIqfmavJt9XVhpekjcnJsIzoxsbh1NSPqc%2Bfo9TU0dmJh5SZopaah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Patricia Ariza] 

###### 10 Feb 2016. 

Con la obra “Sí el Río Hablará”, historia que relata el conflicto armado en el país y la transformación de los ríos en fosas comunes, inicia en Bogotá una nueva edición del Festival de Teatro Alternativo, espacio que convoca al talento creativo local, nacional e internacional para visibilizar y reafirmar el Movimiento teatral colombiano.

Más de 60 agrupaciones provenientes de diferentes localidades de Bogotá, ciudades como Medellín, Pereira, Cali, Manizales y de países como México, España, Argentina, Canadá, Estados Unidos. Cuba, entre otros, harán parte de la programación oficial del evento compuesta por más de 100 funciones, talleres de formación artística y los encuentros latinoamericano de artistas y de poesía, a desarrollarse entre el 12 y el 27 de marzo.

**Homenaje al Teatro La Candelaria**

Como parte del reconocimiento al trabajo del Teatro La  Candelaria por sus 50 años de existencia, se presentarán 3 de las obras emblemáticas de la compañía: “[Sí El Río Hablara](https://archivo.contagioradio.com/el-teatro-la-candelaria-apuesta-por-la-memoria-en-si-el-rio-hablara/)”, bajo la dirección del maestro César Badillo, y las obras dirigidas por la maestra Patricia Ariza “[Soma Mnemosine, el Cuerpo de la Memoria](https://archivo.contagioradio.com/soma-mnemosine-cuerpo-personal-y-social/)”, un homenaje al maestro Santiago García, fundador de la tradicional compañía teatral y “[Camilo](https://archivo.contagioradio.com/camilo-torres-en-el-teatro-la-candelaria/)” inspirada en diferentes facetas de la vida del sacerdote y guerrillero colombiano.

De acuerdo con la maestra y directora del Festival, la presente edición es un homenaje al teatro colombiano, a los grupos, al público y fundamentalmente a las personas de a pie y especialmente a La Candelaria y a su "ardua labor por relatar el país, el conflicto y la valentía de nuestro pueblo en las tablas".

Adicionalmente algunas de las agrupaciones participantes en el Festival, se unirán a la conmemoración con la puesta en escena de la obra “Guadalupe Años Sin Cuenta”, una de las producciones con mayor acogida en la historia de la agrupación teatral fundada en junio de 1966.

<iframe src="https://player.vimeo.com/video/153428949" width="500" height="281" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

Todas las obras que hacen parte de la programación del Festival de Teatro Alternativo tienen un costo de \$12.000 pesos para el público general y \$8.000 pesos para estudiantes con carné vigente, con la posibilidad de realizar abonos de \$30.000 pesos para ingresar a 6 funciones.

   
 

###### Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)] 

 
