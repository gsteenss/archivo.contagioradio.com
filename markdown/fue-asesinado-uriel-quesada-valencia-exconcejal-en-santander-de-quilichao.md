Title: Fue asesinado Uriel Quesada Valencia exconcejal en Santander de Quilichao
Date: 2019-10-07 12:21
Author: CtgAdm
Category: Líderes sociales, Nacional
Tags: asesinato, Cauca, colombia, Consejal, homicidio, Santander de Quilichao, violencia
Slug: fue-asesinado-uriel-quesada-valencia-exconcejal-en-santander-de-quilichao
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/Uriel-Quesada-Valencia.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:Archivopersonal] 

Este domingo 6 de septiembre en el Barrio Belén, sector El Mirador en Santander de Quilichao, Cauca, fue asesinado el exconcejal Uriel Quesada Valencia de 53 años; quien según vecinos se vio envuelto en medio de una discusión con hombres armado desconocidos, quienes dispararon un proyectil que  lo hirió de gravedad a la altura de la  cabeza.

El acto se dio a las 6:30 am, minutos después de que fuera atacado el ex líder político, fue trasladado de urgencias al Hospital Francisco de Paula Santander ubicado a pocos minutos del lugar, sin embargo y por la gravedad de su lesión falleció. (Le puede interesar: [Asesinan a Dumar Noe Mestizo, joven líder indígena de Jambaló, Cauca](https://archivo.contagioradio.com/asesinan-a-dumar-noe-mestizo-joven-lider-indigena-de-jambalo-cauca/))

Uriel Quesada Valencia se desempeñó como concejal de Santander de Quilichao en 1996, se graduó como  abogado de la Universidad de Santiago de Cali, profesión que ejerció hasta los últimos días. (Le puede interesar:[No avanzan investigaciones sobre asesinatos de excombatientes](https://archivo.contagioradio.com/no-avanzan-investigaciones-sobre-asesinatos-de-excombatientes/))

Organismos judiciales buscan  a los responsables del asesinato exconcejal, de  los que aún se desconoce su identidad. Adicional según un informe recientemente publicado en el portal web de la Alcaldía municipal, son 32 homicidios en Santander de Quilichao en lo que va corrido del año, la gran mayoría por robos o disputas entre personas con alto grado de alcohol.

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
