Title: El derecho a la protesta social en el gobierno Duque
Date: 2018-10-02 11:28
Author: AdminContagio
Category: Expreso Libertad
Tags: Gobierno Duque, Movilizaciones, Protesta social
Slug: protesta-social-gobierno-duque
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/10/descarga-6.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Contagio Radio 

###### 2 Oct 2018 

En este programa del Expreso Libertad, hablamos con dos juristas que presentaron sus perspectivas frente a **lo que puede suceder con la protesta social**, tras las afirmaciones del ministro de defensa **Guillermo Botero en las que manifestaba que las personas que ejercen este derecho están usualmente apoyados por grupos al margen de la ley.** Premisa que criminaliza esta determinación constitucional y castigaría a quienes se animen a ejercerlo.

<iframe id="audio_30628833" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_30628833_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Encuentre más información sobre Prisioneros políticos en[ Expreso libertad ](https://archivo.contagioradio.com/programas/expreso-libertad/)y escúchelo los martes a partir de las 11 a.m. en [Contagio Radio](https://archivo.contagioradio.com/alaire.html) 
