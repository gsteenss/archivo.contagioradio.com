Title: Puerto Antioquia: masacres, asesinatos, despojo de tierras y financiamiento de grupos paramilitares
Date: 2020-05-16 11:54
Author: AdminContagio
Category: Columnistas, Nacional, Opinion
Tags: Antioquia, columna, Paramilitares en Antioquia y Chocó, paramilitarismo en Antioquia, puerto antioquia
Slug: puerto-antioquia-masacres-asesinatos-despojo-de-tierras-y-financiamiento-de-grupos-paramilitares
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/05/grafff.png" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/05/solo-graf.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/05/imggtt.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/05/imggtt-1.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/05/0c4d514e1f4a177e666a7a93ebfd1f98.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:heading {"align":"right","level":6,"textColor":"cyan-bluish-gray"} -->

###### Foto por: puertoantioquia.com {#foto-por-puertoantioquia.com .has-cyan-bluish-gray-color .has-text-color .has-text-align-right}

<!-- /wp:heading -->

<!-- wp:heading {"level":5} -->

##### Por: Carlos Montoya

<!-- /wp:heading -->

<!-- wp:paragraph -->

El pasado 20 de septiembre del 2019 la Gobernación de Antioquia expidió un Decreto Departamental firmado por el ex gobernador Luis Pérez Gutiérrez, donde declaraba varios predios en utilidad pública con el fin construir Puerto Antioquia.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El 27% de estos predios tienen relación con familiares, personas y empresas financiadoras de la campaña de Aníbal Gaviria a la Gobernación de Antioquia, como se demostró en la columna “[¿El Gobernador de Antioquia, comprará las fincas de los financiadores de su campaña?](https://archivo.contagioradio.com/el-gobernador-de-antioquia-anibal-gaviria-comprara-las-fincas-de-los-financiadores-de-su-campana/)”

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Sin embargo, lo anterior no es el único tema crítico con respecto a la compra de las fincas de los financiadores de la campaña de Aníbal Gaviria a la Gobernación, y de los predios declarados en utilidad pública para la construcción de Puerto Antioquia.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

A estas incongruencias se suma que algunas de estas empresas están en el listado de financiadores de los grupos paramilitares que entregó Raúl Emilio Hasbún ante Justicia y Paz en el 2006.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":5} -->

##### Listado que no solo nombra a las empresas y comercializadoras que financiaron a los paramilitares, sino también las fincas de donde salían los recursos. Esto significa la ubicación con georreferenciación de los predios mostrando rutas y sectores protegidos especialmente por este grupo ilegal.

<!-- /wp:heading -->

<!-- wp:paragraph -->

En este sentido, estas son las empresas que declararon en utilidad pública y que al tiempo están en el listado de Raúl Emilio Hasbún son: C.I BANACOL, Agrochiguiros, Agrícola El Retiro, Agrícola El Edén, Grupo 20, Hacienda Velaba, Inversiones Mapana LTDA, Inversiones Ucrania LTDA, Teodoro Solorzano González, C.I Banacol, Agropecuaria Bambú LTDA., Fincas Cibeles LTDA, Agropecuaria Yerbazal LTDA., Cultivos Rancho Alegre, Agrícola Santa María, Agropecuaria Bagatela y Agrícola Sara Palma S.A.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

También es importante resaltar que en la zona de influencia de Puerto Antioquia, desde finales de los ochenta, se han presentado masacres, homicidios y acciones por parte del narcotráfico que generan cuestionamientos sobre los verdaderos beneficiados con este puerto.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":5} -->

##### **Masacres, homicidios y narcotráfico**

<!-- /wp:heading -->

<!-- wp:paragraph -->

En 1988 se presentaron dos masacres en la zona de influencia de Puerto Antioquia y  por estos hechos el 17 de junio de 1991 el entonces Juzgado 103 de Orden Público-Seccional Bogotá, en primera instancia, condenó a Ricardo Rayo, Mario Zuluaga Espinal, Víctor Hugo Martínez Barragán, Víctor Suárez Sánchez, Luis Alfredo Rubio Rojas, Henry Pérez Jiménez, Marcelo de Jesús Pérez, Gonzalo Pérez, Adán Rojas Ospino, Hernán Giraldo Serna, Reinel Rojas y **Fidel Antonio Castaño Gil (Hermano de Carlos Castaño)**, como los principales responsables de los homicidios en las fincas “Honduras” y “La Negra”. Posteriormente, algunos de estos condenados se convertirían en los principales jefes de los grupos paramilitares en Colombia.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

La investigación de esta masacre mostró las primeras pruebas sobre la relación criminal entre el narcotráfico, el paramilitarismo, autoridades civiles y militares, y al tiempo, la persecución y asesinato a integrantes de la rama judicial.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Una de las víctimas fue la jueza Martha Lucía González, quien en un primer momento había ordenado la captura de las principales cabezas del Cartel de Medellín: Pablo Escobar y Gonzalo Rodríguez Gacha, y de los comandantes paramilitares [Henry Pérez y Fidel Castaño Gil](https://colombia2020.elespectador.com/pais/la-historia-del-paramilitarismo-en-colombia-segun-ronderos)**,** además de varios funcionarios públicos e integrantes de las Fuerzas Armadas involucrados en las dos masacres.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por haber tomado esta decisión padeció varios atentados que la obligaron salir de país; No obstante, como medida de retaliación, criminales asesinaron a su padre.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Posteriormente, asignan este caso a la jueza María Elena Díaz, quien igualmente ratifica la decisión de la juez Martha Lucía González. [Por este motivo, fue asesinada en Medellín el 28 de julio de 1989.](https://www.elespectador.com/colombia2020/justicia/verdad/alvaro-gonzalez-santana-la-memoria-de-un-abogado-inerme-articulo-857934)

<!-- /wp:paragraph -->

<!-- wp:heading {"level":5} -->

##### ***“En solidaridad con su muerte y para exigirle al Gobierno Nacional mayores garantías, más de dos mil funcionarios de la rama judicial cesaron sus actividades.”*** [Afirmaron los funcionarios de la rama judicial.](https://www.medellinabrazasuhistoria.com/asesinato-de-la-jueza-maria-elena-diaz/)

<!-- /wp:heading -->

<!-- wp:paragraph -->

Igualmente, en algunas de las fincas declaradas en utilidad pública fueron asesinados líderes sindicales y civiles como es el caso de los predios de la Agropecuaria Bagatela, ahora llamada Comercial Punta Arenas con las haciendas las Margaritas y las Bodegas.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En las Margaritas, asesinaron a los líderes sindicales Arcesio Gallego Lozano y Misael Antonio Moreno, el 13 de septiembre de 1995, y en la finca las Bodegas, al civil Evangelista Antonio Pajaro Ruiz el 27 de mayo de 1996. También está el caso de la finca El Oasis, probablemente de la familia Gaviria Correa, ya que es la única con ese nombre en la zona. En ella, según la sentencia de Helbert Veloza García alias “H.H”, del Tribunal de Superior de Bogotá de Justicia y Paz, asesinaron a Jesús Ernelio Andrade Becerra el 13 de agosto de 1996.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":5} -->

##### ***“El 13 de septiembre de 1995, un grupo de hombres armados llegaron hasta la empacadora de la finca “Las Margaritas”, ubicada en el corregimiento de “Nueva Colonia” del municipio de Turbo, en el sector de “La Llana”, allí preguntaron por Epifanio y Luis Carlos, al no encontrarlos, dispararon de forma indiscriminada en contra de los trabajadores de la finca, resultado muertos los señores Arcesio Gallego Lozano y Misael Antonio Moreno Córdoba, integrantes de Sintrainagro.”***

<!-- /wp:heading -->

<!-- wp:heading {"level":5} -->

##### **Hervert Veloza García, alias “H.H” los pronosticó**

<!-- /wp:heading -->

<!-- wp:paragraph -->

El ex líder paramilitar Hervert Veloza García, alias “H.H”, ya lo había pronosticado en sus declaraciones de Justicia y Paz sobre los realmente beneficiados con la guerra en Urabá señalando como principales actores a los grupos empresariales:

<!-- /wp:paragraph -->

<!-- wp:heading {"level":5} -->

##### ***“Los empresarios –bananeros-, estos empresarios que han abusado y han explotado a la comunidad de la zona de Urabá. Y todavía lo siguen haciendo. Yo lo he denunciado públicamente. Los bananeros son tan o más responsables que nosotros en todo lo que pasó en Urabá. Porque fuimos de finca en finca prohibiéndoles a los trabajadores a hacer paros armados. Uno iba uniformado y con un cuchillo en la mano, diciéndoles que el que hiciera paro lo matábamos, ¿por qué?, por buscar los beneficios de los empresarios bananeros. Ellos fueron los que se beneficiaron de la guerra. De ellos ninguno está pagando, ninguno está detenido, ninguno ha puesto un peso para la reparación y ellos fueron los que realmente se beneficiaron.***

<!-- /wp:heading -->

<!-- wp:paragraph -->

Sin embargo, entre las empresas cuestionadas llaman la atención los casos de la Agrícola Sara Palma, Agrícola Santa María y la Agrícola El Retiro, compañías con el mayor número de predios beneficiados directa e indirectamente con Puerto Antioquia. La Agrícola Santa María, según el certificado de Cámara de Comercio fue constituida el 8 de marzo de 1982, tiene 20 predios con alrededor de mil hectáreas en la zona de influencia de Puerto Antioquia. Uno de los accionistas mayoritarios es Jaime Henríquez Gallo, quien es mencionado en versiones libres de Justicia y Paz por financiamiento de grupos paramilitares y también beneficiado del escandaloso [programa Agro Ingreso Seguro](https://www.justiciaypazcolombia.com/informe-empresas-bananeras-vulneracion-de-derechos-humanos-2/).

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

La Agrícola Sara Palma, según el certificado de Cámara de Comercio fue constituida el 30 de noviembre de 1987 y tiene 10 predios con más de mil hectáreas en la zona de influencia de Puerto Antioquia. Los integrantes de la junta directiva en la actualidad son: Federico Gallego, Juan Jacobo Mejía, José Fernando Jaramillo, Raúl Retrepo Ramos, Héctor Fabio Trujillo, Carlos Aníbal Trujillo, Juan José Jaramillo, Oscar Enrique Penagos Garcés, este último mencionado como colaborador de narcotraficantes y paramilitares e igualmente beneficiado del programa de [Agro Ingreso Seguro en otras empresas que ha integrado](https://www.elespectador.com/noticias/politica/empresarios-senalados-de-financiar-paras-aportaron-camp-articulo-498118). También está Juan Estaban Álvarez, gerente de la empresa Grupo 20, compañía de la familia Gaviria Correa; y cierra el grupo, Jorge Julián Gaviria Correa, hermano del mandatario departamental. 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Para cerrar el repertorio de esta compañía, el Tribunal Superior de Antioquia Sala Civil Especializada en Restitución de Tierras, emitió el pasado 12 de marzo del 2020 una sentencia en contra de varias empresas bananeras entre ellas, Agrícola Sara Palma, obligándola a devolver una finca en la zona de influencia indirecta de Puerto Antioquia. Según el tribunal, ***“resulta inaceptable su actuar, pues contraría flagrantemente y choca contra un hecho cierto, público, ampliamente conocido y sabido por el común de los ciudadanos”,***refiriéndose a la violencia generalizada que se presentó en Urabá y el silencio cómplice de políticos y empresarios.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Finalmente, está el caso de la Agrícola El Retiro que tiene el antecedente de haber sido una de las fundadoras de BANADEX, antigua filial de Chiquita Brands, y recordada por ser una de las comercializadoras que recogía los tres centavos de dólar para financiar a los paramilitares.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por estos hechos uno de sus socios fundadores, Reinaldo Elías Escobar de la Hoz, tiene resolución de Acusación de la Fiscalía General de la Nación por los delitos de concierto para delinquir agravado, promoción y financiamiento de grupos armados ilegales.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Javier Ochoa Velásquez, que actualmente es integrante de la junta directiva de la Asociación de Bananeros de Colombia -AUGURA- también es acusado por el mismo delito y fue uno de los gerentes de la Agrícola El Retiro.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":5} -->

##### Esta empresa tiene en la actualidad 69 fincas que abarcan 3.749 hectáreas y 65 de estos predios están sobre la zona de influencia de Puerto Antioquia.

<!-- /wp:heading -->

<!-- wp:paragraph -->

Estos hechos no significan que los predios fuesen obtenidos de manera ilegal y hasta probablemente existan solicitudes de restitución sobre ellos, pero si genera muchas preguntas en torno a que se hayan empezado a comprar fincas en los años más crueles del conflicto armado; ya que solo, entre 1996 y 1998, la región de Urabá aportó el 45% de todas las víctimas de desplazamiento forzado y despojo forzado del país; en cambio, esta empresa, entraba a la zona a comprar 41 fincas…

<!-- /wp:paragraph -->

<!-- wp:image {"id":84320,"sizeSlug":"large"} -->

<figure class="wp-block-image size-large">
![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/05/solo-graf.jpg){.wp-image-84320}  

<figcaption>
Fuente: Instituto Popular de Capacitación –IPC-. Observatorio de Derechos Humanos N.18. Medellín, diciembre de 2015

</figcaption>
</figure>
<!-- /wp:image -->

<!-- wp:paragraph -->

En esa misma línea de empresas cuestionadas, está Bananeras de Urabá S.A.S, controlada por la señora Rosalba Zapata Cardona, beneficiada indirectamente con el proyecto de Puerto Antioquia.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Una sentencia de restitución de tierras en la vereda California del corregimiento de Nueva Colonia, en el Municipio de Turbo, afirma lo siguiente sobre la señora Zapata: **“*Jefes paramilitares como el señor Raúl Emilio Hasbún, quien con el apoyo del señor Felipe Echeverry, reconocido bananero de la región e hijo de la señora <u>Rosalba Zapata</u>****, representante de la Empresa Bananeras de Urabá, en medio de una reunión que tuvo lugar en el año 2003, en la vereda La Teca de Nueva Colonia, intimidaron a los reclamantes, con la presencia de personas pertenecientes a las AUC que portaban armas, y bajo el sofisma de que las parcelas que ocupaban le pertenecían al señor Raúl Emilio Hasbún, llevaron a los reclamantes a tener que pagar por ellas, táctica que los* *asfixió económicamente, al serles descontado por cuotas el valor que les impusieron, a través de la Empresa Banacol, superando así su capacidad económica.*”

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

La cercanía de la señora Rosalba Zapata con los exlíderes paramilitares es de conocimiento público y se demuestra cuando en mayo del 2011, fue anfitriona de un evento respaldado por autoridades privadas y públicas, entre ellas la Vicepresidencia de la República, alentando a que las víctimas le pidieran perdón a sus victimarios, en este caso, al [ex líder paramilitar Raúl Emilio Hasbún](https://noticias.canal1.com.co/que-tal-esto/victimas-piden-perdon/), y no al contrario como ha sucedido con sentencias de Justicia y Paz o de la Comisión Interamericana de Derechos Humanos. Es decir, el mundo al revés.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por otro lado, también llama la atención que la firma Ecoming, empresa de consultoría que analiza la gestión de Puerto Antioquia en su informe del 3 de noviembre de 2017, no reconoce la presencia de grupos armados ilegales en esa zona.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Sin embargo, en una de las vías de comunicación de interés del proyecto, hay un letrero pintado con las letras AGC (Autodefensas Gatanistas de Colombia) y un “punto” de control del Clan del Golfo al lado de la misma infraestructura.

<!-- /wp:paragraph -->

<!-- wp:image {"id":84322,"sizeSlug":"large"} -->

<figure class="wp-block-image size-large">
![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/05/imggtt-1-1024x768.jpg){.wp-image-84322}  

<figcaption>
Foto: 19 de Julio de 2019 sobre la vía San Pablo – Puerto Girón (Al fondo se ve un punto de control del Clan del Golfo

</figcaption>
</figure>
<!-- /wp:image -->

<!-- wp:paragraph -->

Adicionalmente, debe verificarse si algunas de las fincas declaradas en utilidad pública están siendo controladas por el Clan del Golfo, ello debido a que en ese corregimiento se han incautado entre el 2017 y 2019, alrededor de 3.500 kilos de droga y han capturado a varios integrantes de esta estructura ilegal en corregimientos aledaños  como Currulao. Siendo el caso de Ángel Eusebio Usuga David, alias “Chengo” o “Canoso”, el más destacado ya que es hermano de Otoniel (Líder del Clan del Golfo) y responsable de los laboratorios de cocaína en varias veredas de Turbo y jefe de testaferros para el lavado de dinero.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

De otro lado, el Observatorio de Tierras, integrado por varias universidades del Colombia, presentó una investigación en el 2016 donde demostró la relación entre las élites rurales empresariales, narcotráfico y paramilitarismo.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Dicha investigación presentaba como el Bloque Bananero de los paramilitares, responsable de la zona de influencia de Puerto Antioquia, convivió entre actores legales e ilegales para el [funcionamiento de la estructura armada.](https://editorial.urosario.edu.co/gpd-el-despojo-paramilitar-y-su-variacion-quienes-como-por-que.html)

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Probablemente, en la actualidad todavía existan algunas empresas que pueden tener relación con estructuras paramilitares, conocidas como el Clan del Golfo, especialmente en el Corregimiento de Nueva Colona y Currulao; ya que, si capturan a uno de los hermanos del máximo líder del Clan del Golfo, quien era responsable de laboratorios y lavado de activos, muestra la importancia estratégica que sigue teniendo la zona a pesar de la violencia que se ha vivido durante tantos años.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Todo esto al final, muestra que los realmente beneficiados con la construcción del Puerto Antioquia son gran parte de los empresarios bananeros de Urabá porque los predios declarados en utilidad pública y los que están a su alrededor, son casi en su totalidad de empresas mencionadas en el listado de ex líder paramilitar Raúl Emilio Hasbún como financiadoras de los grupos paramilitares. En cambio, a las víctimas de despojo de tierra les siguen poniendo trabas a sus procesos para recuperar sus predios acompañado del asesinato de sus líderes sociales, que a la fecha van 23.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Sin embargo, la justicia puede estar dando sus pasos para aportar a la reparación y la verdad, ya que el pasado 12 de marzo del 2020, el Tribunal Superior de Antioquia Sala Civil Especializada en Restitución de Tierras, ordenó compulsar copias a la Fiscalía General de la Nación a varias de las empresas con beneficiadas directa e indirectamente con Puerto Antioquia como son Bananeras de Urabá, UNIBAN y BANACOL.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

[Ver mas: nuestros columnistas](https://archivo.contagioradio.com/opinion/columnistas/)

<!-- /wp:paragraph -->
