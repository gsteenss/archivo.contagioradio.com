Title: Contrastes de humanidad en "La sal de la tierra"
Date: 2015-09-20 10:00
Author: AdminContagio
Category: 24 Cuadros
Tags: Documental "La sal de la tierra", Documentales 2014, Juliano Ribeiro Salgado, Sebastião Salgado Fotógrafo brasileño, Win Wenders
Slug: contrastes-de-humanidad-en-la-sal-de-la-tierra
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/Sebastiao-Salgado1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [20 sep 2015] 

40 años han pasado desde el día en que Sebastião Salgado decidio dejar de lado su prominente carrera de economista, para viajar alrededor del mundo en compañía de su cámara fotográfica y su enorme capacidad de capturar, con enorme belleza, realidades que integran la complejidad del comportamiento humano como ser social.

El documental "La sal de la tierra", dirigido por el reconocido documentalista Win Wenders y Juliano Ribeiro Salgado, es el relato de los pasos recorridos por Salgado, a través de caminos que lo llevaron a conocer la vida y la muerte; las hambrunas, las guerras, los desterrados, los conflictos internacionales y muchas situaciones más que ha logrado inmortalizar a través de su cámara.

Durante el primer minuto la película atrapa al espectador con una impactante imágen del trabajo de miles de personas en una mina de oro, la fotografía acompañada por la narración serena de Salgado es la antesala a conocer la mirada de un fotógrafo que llevará al espectador a cuestionar al humano en lo más profunda y a generar mil preguntas del mundo que habitamos.

Pero luego de llevarnos a odiar al humano, el director da una vuelta al documental para generar esperanza y esto lo hace con la narración de la la obra maestra "Genésis" un viaje de Salgado que propone demostrar la belleza del planeta, a través de lugares inimaginados, de la vida salvaje y de la forma que viven comunidades poco conocidas.

Una cinta estéticamente muy bien cuidada, en blanco y negro que muestra la relación cercana entre fotógrafo y director de la película y del aprecio que este último tiene con el trabajo que ha desarrollado por más de 40 años Salgado,

<iframe src="https://www.youtube.com/embed/nbNAXC8kqT0" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

"La Sal de la Tierra", estuvo nominado al Óscar al mejor documental largo de 2014, ganó el Premio del Público en el Festival Internacional de Cine de San Sebastião, el Premio de la Audiencia en el Festival Internacional de Cine de Tromso y el César a la mejor película documental en 2014. Se estará presentando desde el 24 de septiembre en las salas de Cine Colombia.

Una película que ningún amante de la fotografía debería dejar de ver, seguro saldrán con mil preguntas de la humanidad.
