Title: Campesinos cumplen cuarentena mientras Ejército  erradica forzadamente en Cauca
Date: 2020-04-01 08:59
Author: CtgAdm
Category: Actualidad, Comunidad
Tags: Cauca, Ejército Nacional, Erradicación Forzada
Slug: campesinos-cumplen-cuarentena-mientras-ejercito-erradica-forzadamente-en-cauca
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/03/Erradicacipon.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: Erradicación Forzada/ COCCAM

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Pese a las medidas de aislamiento implementadas en el país por parte del Gobierno para afrontar la crisis del Covid-19, **organizaciones sociales de Norte de Santander, Nariño, Caquetá y Putumayo, denuncian que en medio de la cuarentena se han intensificado los operativos de erradicación forzada,** particularmente en municipios donde se firmaron acuerdos colectivos del Programa Nacional Integral de Sustitución de Cultivos de Uso Ilícito (PNIS).

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Según Leider Miranda, delegado nacional de la Coordinadora Nacional de Cultivadores de Coca, Amapola y Marihuana [(COCCAM),](https://twitter.com/COCCAMColombia) el pasado 30 de marzo el Ejército Nacional arribó al corregimiento de El Carmelo en Cajibío, Cauca para avanzar en los procedimiento de erradicación forzada, generando riesgos en las comunidades que sí se han apropiado de las medidas de cuarentena. [(Le puede interesar: Más de 10.000 familias en Putumayo afectadas por incumplimiento del Gobierno en acuerdo de sustitución)](https://archivo.contagioradio.com/mas-de-10-000-familias-en-putumayo-afectadas-por-incumplimiento-del-gobierno-en-acuerdo-de-sustitucion/)

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### "Gobierno incumple Acuerdo de paz y decreto de cuarentena"

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Aunque con la firma del acuerdo de paz, las comunidades manifestaron su volutnad de buscar otras alternativas para cambiar la economía de la que dependen a través del PNIS, el Gobierno continúa con la erradicación forzada incumpliendo lo pactado, "le dicen a las comunidades que deben guardarse, no salgan de sus casas, para prevenir pero entonces nos preguntamos por qué no ponen en cuarentena a las Fuerzas Militares?" cuestiona el delegado de COCCAM.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Las comunidades que permanecen en sus casas ya han advertido que si el Ejército va a erradicar no permitirán la erradicación, pues existe un acuerdo y van a salir a defender su comida y la forma en que sostienen a sus familias, "no queremos que las familias entren en choque con las Fuerzas Militares", afirma Leider quien resalta que esta es la tercera vez que tropas irrumpen en Cajibio. [(Le puede interesar: Aspersión aérea con glifosato, una estrategia fallida que se repite)](https://archivo.contagioradio.com/la-aspersion-aerea-con-glifosato-no-es-una-estrategia-efectiva/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

A esta petición se suman otros sectores como la Coalición Acciones para el Cambio (APC) quienes solicitan al Gobierno suspender los operativos de erradicación forzada durante la contingencia del Covid-19 y así garantizar los derechos de las poblaciones rurales.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### La erradicación avanza incluso en cuarentena

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

En medio de hechos similares el 26 de marzo fue asesinado Alejandro Carvajal, joven de 20 años, en la vereda Santa Teresita, municipio de Sardinata, Norte de Santander, según la organización ASCAMCAT por integrantes del Ejercito Nacional en medio de un operativo de erradicación. [(Alejandro Carvajal, joven de 20 años asesinado por el Ejército en Catatumbo: ASCAMCAT)](https://archivo.contagioradio.com/alejando-carvajal-joven-de-20-anos-asesinado-por-el-ejercito-en-catatumbo-ascamcat/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

De igual forma este 31 de marzo, en horas de la mañana, se presentó un episodio similar en las veredas de La Chamba y Casa Vieja en Ancuya, Nariño cuando el Ejército arribó a realizar otro operativo de erradicación forzada en un municipio donde también se firmó un acuerdo colectivo con el PNIS que hasta el momento no ha comenzado su implementaicón.

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
