Title: Es un hecho: el fracking llega a Colombia
Date: 2017-01-25 11:39
Category: Ambiente, Voces de la Tierra
Tags: colombia, fracking
Slug: hecho-fracking-llega-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/impactos-del-fracking-colombia1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: blog.gesternova 

###### [25 Ene 2017] 

[Como ya lo habían previsto organizaciones ambientalistas, Portafolio, publicó una noticia en la que aseguran que la **Agencia Nacional de Hidrocarburos,** ANH, prepara un documento en el que se establecerían los términos para que en Colombia se empiece a extraer crudo con base en la técnica del fracking.]

[Aunque Agencia Nacional de Licencias Ambientales, ANLA, había asegurado que no hay solicitudes en curso para exploraciones de fracking, y desde hace meses organizaciones como CORDATEC, había denunciado que en San Martín, Cesar la empresa Conocophilips adelanta actividades para llevar a cabo el fracturamiento hidráulico, lo cierto, es que **los temores de las comunidades y los ambientalistas eran ciertas,** y la amenaza del fracking sería un hecho.]

[La situación es preocupante, pues de son 43 bloques petroleros en Colombia los que aparecen clasificados como] **‘yacimientos no convencionales de hidrocarburos’,** [es decir, que podrían ser destinados para fracturamiento hidráulico o fracking, como lo evidencia un mapa realizado por Fidel Mingorance, de acuerdo con los datos del informe ‘Mapa de tierras’ de la Autoridad Nacional de Hidrocarburos, ANH, publicado el pasado 19 de octubre de 2016.]

### [Consecuencias del Fracking] 

[Aunque se reconoce que la demanda interna de petróleo que tiene el país sobrepasa la producción petrolera actual, y de hecho, actualmente apenas se cuenta con 7 años de reservas tanto de gas como de petróleo, desde el Movimiento Ambientalista Colombiano advierte que se debe pensar en los costos ambientales que generaría la práctica del fracking en un país donde el]**75% de los recursos hídricos se encuentran en las fuentes subterráneas.**

[El fracturamiento hidráulico requiere de entre]**9 y 26 millones de litros de agua por pozo y año, pero** **solo puede reutilizar un 15% de esa agua,** [explica Camilo Prieto, del Movimiento. Aunque Colombia estudia la manera de reducir las tasas de material radioactivo en el agua que sale residual del fracking, no se ha logrado en ninguna parte del planeta disminuir esos índices hasta obtener los niveles aceptados por los entes de salud pública del mundo, para que se pueda demostrar que esa técnica se puede realizar de manera responsable.]

**Otra de las preocupaciones es que en el país no existen estudios de geotectónica amplios.** L[os análisis de hidrogeología son completamente limitados, lo que quiere decir, que no hay mapas geológicos certeros sobre la situación en la que se encuentra la zona donde se pensaría realizar el Fracking, y si llega a haber una fuente de agua subterránea donde se lleve a cabo esa actividad, sería muy posible que esta termine con]**residuos tóxicos y sustancias radioactivas que pueden ocasionar cáncer, o el gas metano puede generar que el agua se vuelva inflamable.** [[(Le puede interesar: Informe revela impactos del fracking en América Latina)]](https://archivo.contagioradio.com/impactos-del-fracking-en-6-paises-de-latinoamerica/)

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
