Title: Memoria de las mujeres desplazadas vuelve al teatro
Date: 2017-07-10 13:00
Category: eventos
Tags: colombia, Corporación Colombiana de Teatro, Desplazamiento, mujeres, teatro
Slug: memoria-mujeres-desplazadas-teatro
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/12/15419745_1239857412775877_1848446789254520855_o.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### Foto: CC de Teatro 

##### 10 Jul 2017 

Con el propósito de continuar con su trabajo de llevar a través del arte dramático mensajes que aporten a la restauración del tejido social y de paz, la Corporación Colombiana de Teatro presenta **'Memoria', un pieza teatral construída colectivamente** por Nohra Gonzales, Alexandra Escobar y la maestra Patricia Ariza, su directora.

La dramaturgia en 'Memoria' esta encaminada a **realizar un homenaje a todas aquellas mujeres víctimas de la violencia en Colombia**, población que mayoritariamente ha sufrido los embates del desplazamiento forzado, es decir, personas que son obligadas a abaldonar sus tierras y enfrentarse a difíciles situaciones en otros lugares del país.

Con esa motivación, la obra de Tramaluna Teatro hace una **recopilación y presentación de esas memorias para decir “Un nunca jamás”** a todas aquellas situaciones en donde se encuentran que las mujeres que con temor han vivido, viven y huyen del conflicto armado del país. Le puede interesar: [Patricia Ariza, una vida por la cultura y la paz](https://archivo.contagioradio.com/patricia-ariza-una-vida-por-la-cultura-y-la-paz/).

![memoria 2](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/12/15370033_1241569432604675_4121158225658051016_o.jpg){.size-full .wp-image-33471 .aligncenter width="2048" height="1272"}

La obra se presentará del 13 al 15 de Julio en la sala de teatro Seki Sano - Calle 12 \# 2 – 65, y los días 9 y 10 de diciembre se presentara a las 7:30 p.m. Boletería 12.000 para estudiantes y adultos mayores y 20.000 en general para publico en general.
