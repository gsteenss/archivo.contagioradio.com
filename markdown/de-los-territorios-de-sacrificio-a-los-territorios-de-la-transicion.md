Title: De los territorios de sacrificio a los territorios de la Transición
Date: 2019-10-03 17:08
Author: Censat
Category: CENSAT, Opinion
Tags: CENSAT, Censat agua Viva, Cultura, defensa del territorio, naturaleza, territorio, Territorios
Slug: de-los-territorios-de-sacrificio-a-los-territorios-de-la-transicion
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/CA200316.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/dfdggbggfbgfbfgbfgbfg.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto de: Javier de la Cuadra. 

Los gobiernos nacionales y regionales, así como el sector empresarial han decidido que el Caribe colombiano sea un territorio de sacrificio, “*en el sentido de ser un espacio dispuesto para las necesidades del capital, es decir que se estructura y asume su forma para garantizar la acumulación*” (Saccucci, 2018).

Esta situación no es nueva ni exclusiva del extractivismo minero de carbón, muchas bonanzas han pasado por las tierras caribeñas (las perlas, el palo de tinte, la marihuana, el algodón),  enriqueciendo a unos pocos y dejando en cambio un desolador panorama de pobreza, hambre y dependencia.

Esta territorialidad del sacrificio construida por los intereses del capital “*da cuenta también de un reordenamiento espacial que modifica patrones de uso del suelo*” (Saccucci, 2018) y que de diversas formas determina qué actividades y cuáles relaciones se establecen entre actores que coproducen este territorio.  Precisamente, estas relaciones en su mayoría clientelares o de patrón/empleado se basan en un consenso social que posibilita al capital disponer del territorio, configurarlo y controlarlo. Se amalgama una red de complicidades que están mediadas por el trabajo, una falsa idea de desarrollo y un espejismo de bienestar asociado a la bonanza que convenientemente sostiene la posibilidad del capital para la apropiación.

Precisamente, esta arquitectura territorial fue desplegada en la Guajira luego que el pasado mes de agosto, la sección primera de la sala contenciosa administrativa del Consejo de Estado admitiera una demanda de nulidad contra la licencia ambiental del complejo minero presentada por líderes de comunidades indígenas wayuu, 3 senadores de la república y 3 organizaciones no gubernamentales. La demanda busca frenar los impactos socio ambientales que ha producido la minería de carbón a gran escala en esta región, que se decreten medidas cautelares que garanticen la no expansión de la actividad extractiva en el territorio y que se evalúe el proceso de licenciamiento de Cerrejón que ha tenido grandes irregularidades.

(le puede interesar: [Sentidos ambientales de la participación](https://archivo.contagioradio.com/sentidos-ambientales-de-la-participacion/))

#### Frente a esta acción: 

 El viernes 6 de septiembre, con el lema “Cerrejón somos todos”, cientos de personas que trabajan en el complejo minero salieron a las calles para defender a la empresa y su supuesta ejemplar gestión. Así mismo, las voces de funcionarios del gobierno regional retumbaron en los medios de comunicación empresariales locales y nacionales insistiendo que la empresa ha sido crucial en la economía Guajira y que sería una equivocación admitir su cierre. La reacción hostil al legítimo derecho de las comunidades afectadas, organizaciones y senadores de la república para pedir explicaciones y medidas frente a la terrible situación del departamento y al cumplimiento de los mínimos legales, contó con un despliegue mediático importante, hostigamiento a algunos líderes indígenas accionantes, condicionamientos a los trabajadores y sus familias frente a la presencia en la marcha y una campaña de desinformación que la empresa difundió con la complicidad de muchas y muchos habitantes de la región. Esta situación muestra claramente un consenso social construido por la empresa mediante relaciones de dependencia económica y financiación que ha permitido que el negocio del carbón produzca un territorio desigual y una opinión basada en espejismos.

Según el Plan de Desarrollo Departamental 2016 – 2019 las causas de la pobreza en la Guajira son múltiples, pero se cuenta como unas de las principales el deterioro del sector agropecuario y lo que llaman la reducción de la oferta de recursos del entorno. Estas dos causas están directamente ligadas con la actividad minera a gran escala que ha monopolizado los usos del suelo para la minería y que ha impactado más de 12 fuentes hídricas de la baja y media Guajira, necesarias para el abastecimiento humano y no humano.

En este sentido, nos preguntamos:

¿qué será lo que motiva a ciertas personas de la región, al gobierno y a los medios a defender una actividad depredadora de la naturaleza, las culturas indígenas y la vida?

¿Será que el lucro y los sistemas de valoración del territorio que operan en el caribe colombiano se basan en marcar esta región como un espacio vacío en donde sólo el extractivismo le da sentido?

#### ¡Nos negamos a admitir esta realidad! 

El Caribe colombiano es un territorio que resguarda el saber, la historia y la cultura de pueblos milenarios. En sus ríos y en sus valles, la música, la literatura y la poesía han brotado producto de su belleza y exuberancia. Nos negamos a que la minería de carbón, el fracking, la explotación de gas y otras actividades se instalen o sigan allí como un territorio para sacrificar. Por el contrario, el Caribe es y puede ser el lugar de la transición, una transición justa, participativa, para los pueblos y desde los pueblos. En esta transición la claridad de las obligaciones del estado y las empresas frente a un cierre integral de la mina, la reparación estructural del territorio, la construcción conjunta de horizontes económicos sustentables y la garantía de la justicia ambiental y social para esta región es imprescindible. Así mismo la reflexión frente al futuro de los trabajadores del sector minero, la diversificación de la economía y la atención de los daños perpetuos de la minería es una tarea pendiente y urgente en la construcción de políticas públicas.

No hay que esperar 20 años más, esta coyuntura nos permite identificar la necesidad de exigir al estado colombiano un compromiso real con las comunidades y el territorio. Es posible y se puede soñar un Caribe fuera de la dependencia del carbón y de sus altos costos sociales y ambientales.
