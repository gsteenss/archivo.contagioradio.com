Title: Cada tres días asesinan a un joven en Puerto Claver, Antioquia
Date: 2016-01-22 15:11
Category: DDHH, Nacional
Tags: enfrentamientos guerrilla y paramilitares el bagre, paramilitares, puerto claver
Slug: cada-tres-dias-asesinan-a-un-joven-en-puerto-claver-antioquia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/01/Puerto-Claver-Antioquia.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: OCHA Colombia. ] 

<iframe src="http://www.ivoox.com/player_ek_10168403_2_1.html?data=kpWemJ2YdJShhpywj5WbaZS1kZyah5yncZOhhpywj5WRaZi3jpWah5yncaTVxcaY1tfJt4zYhqigh6aopdSfwtjS1c7Spc%2BfwpDi0JDOs9fZz5DS0JC0ucbm1dSYpdHFusbmhpewjaaRaZi3jqjc0NnFq8rjjLfOxs7Tb46ZmKialg%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Mauricio Sánchez, AHERAMIGUA] 

###### [22 Ene 2016. ] 

[Tras enfrentamientos entre grupos paramilitares y la compañía Gerardo Guevara de las FARC-EP en el corregimiento de Puerto Claver, municipio de El Bagre, Antioquia **más de 600 campesinos se han visto obligados a desplazarse forzadamente al casco urbano**, como lo asevera Mauricio Sánchez, integrante de la Asociación de Hermandades Agroecológicas y Mineras de Guamoco AHERAMIGUA.]

[**En el Bajo Cauca antioqueño durante este mes se ha incrementado la violencia** por cuenta de [enfrentamientos entre grupos guerrilleros y paramilitares](https://archivo.contagioradio.com/4-mil-personas-en-riesgo-de-desplazamiento-por-enfrentamientos-en-el-bagre-antioquia/), principalmente en El Bagre. Según afirma Sánchez, los combates se han dado en las veredas altas del corregimiento de Puerto Claver, iniciaron el 9 de enero y se extendieron hasta el 18, y dejaron como resultado 3 pobladores desaparecidos por los paramilitares y cerca de 20 personas muertas que aún no han sido identificadas.]

[En los últimos días el Ejército aseguró haber encontrado **dos cuerpos sin vida enterrados y uno expuesto al aire libre y decapitado**, de acuerdo con pobladores, estos cuerpos podrían corresponder a los de los tres campesinos que fueron desaparecidos.]

[Esta situación es preocupante pues se ha venido presentando **reiteradamente desde agosto de 2015**, cuando se hizo un pacto entre las comunidades y los grupos armados en el que estos últimos se comprometían a respetar los derechos de los pobladores; sin embargo, el acuerdo fue violado por los armados y hoy el momento es tan crítico que **cada 3 o 4 días aparecen jóvenes asesinados**, “la fuerza pública dice que son retaliaciones entre bandas criminales pero **nosotros sabemos que son los grupos paramilitares que están operando en la región**”, asegura Sánchez. ]

[Desde la ciudad de Medellín se está concretando una **comisión de verificación que llegaría este sábado a Puerto Claver para evaluar la situación y regresaría el próximo jueves**, con el fin de sistematizar la información recogida en terreno, pues de acuerdo con Sánchez "los medios de comunicación no dan las informaciones como son, sino que la tergiversan y niegan que hay en la región grupos paramilitares operando”. ]

###### Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio] ](http://bit.ly/1ICYhVU) 
