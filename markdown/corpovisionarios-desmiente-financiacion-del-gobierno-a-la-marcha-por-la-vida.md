Title: Corpovisionarios desmiente financiación del gobierno a la “marcha por la vida”
Date: 2015-02-12 18:38
Author: CtgAdm
Category: Nacional, Paz
Tags: Conversaciones de paz en Colombia, Corpovisionarios, Marcha por la Vida
Slug: corpovisionarios-desmiente-financiacion-del-gobierno-a-la-marcha-por-la-vida
Status: published

###### Foto: ContagioRadio 

<iframe src="http://www.ivoox.com/player_ek_4076719_2_1.html?data=lZWkmJyVfY6ZmKiakpiJd6KomJKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRjMbi096Yr9rSttPV2pDRx5Cns9Pk0NvW1c7TssLmytTgj4qbh4630NPhw8zNs4zGwsnW0ZCRaZi3jpk%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Henry Munray, Corpovisionarios] 

 

##### <iframe src="http://www.ivoox.com/player_ek_4076724_2_1.html?data=lZWkmJyWeI6ZmKiakpqJd6KnlpKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRh9Dm0dTjy9jNs8%2FV087c1ZKJe6ShpNTb1sbLrdCfs8bRy9SPcYarpJKh&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe> 

##### [Resultado de investigación, percepción sobre proceso de paz] 

Corpovisionarios reveló resultados de los contratos que mantuvo con la oficina de Alto Comisionado para la Paz, para desmentir las afirmaciones hechas por Álvaro Uribe y el Centro Democrático, en el sentido de que sería el mismo gobierno actuando en nombre de otras entidades, y reiterar la independencia de la **Marcha por la Vida del 8 de marzo.**

Para el Henry Munrray, representante legal de la organización, el objetivo del contrato que ha generado controversia, de "realizar una movilización ciudadana", fue sacada de su contexto. "**Todos los contratos de Corpovisionarios -afirma Munrray- tiene como objetivo la movilización ciudadana porque ese es el objetivo de la cultura ciudadana**. Movilización ciudadana se entiende sociológicamente como el **proceso apropiación por parte de la ciudadanía** de ciertos contenidos y de ciertas actitudes; y **no como salir a marchar.**"

Al tratar principalmente temas relacionados con pedagogía y políticas públicas, la organización dice que sus contratos son mayoritariamente con instituciones estatales. **Corpovisionarios ha tenido contratos con gobiernos de México, Brasil, Suecia, e incluso con más de 54 instituciones públicas en Colombia**, incluso el gobierno de Álvaro Uribe Vélez. Por ello, considera Munrray, su organización no debe avergonzarse de realizar contratos con la Oficina del Alto Comisionado para la Paz.

Para esta organización, por lo tanto, existe un "mal pensamiento" en la sociedad colombiana que busca tergiversar la relación de la Corporación con el gobierno, y de Antanas Mockus y su invitación a "marchar por la vida" junto a Ivan Cepeda, el propio Álvaro Uribe, y la sociedad colombiana en general.

En meses pasados la organización **denunció el robo de algunos de sus equipos electrónicos**, sin embargo, para evitar malos entendidos, afirman que es mejor “no relacionar el robo con la divulgación de la información por parte del senador Uribe.

Según algunos analistas la intención que perseguía el uribismo, con las afirmaciones en torno a los contratos de la oficina del Alto Comisionado para la Paz, era desprestigiar la movilización en favor de la paz y con la venia de los medios de información el daño está hecho. Sin embargo, también afirman que **la movilización ciudadana a favor de la paz debe seguir su curso y dejar en firme el respaldo al proceso.**
