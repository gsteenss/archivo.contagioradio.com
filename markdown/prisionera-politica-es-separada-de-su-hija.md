Title: Prisionera política es separada de su hija recién nacida
Date: 2015-12-28 17:30
Category: DDHH, Nacional
Tags: carceles, presos politicos
Slug: prisionera-politica-es-separada-de-su-hija
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/12/Foto-de-José-Alejandro-Gómez-Cárcel-El-Buen-Pastor-Pabellón-5.-.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: José Alejandro Gómez] 

**Yamile Mancilla, [prisionera política](https://archivo.contagioradio.com/?s=prisioneros)** recluida en la cárcel el Buen Pastor en Bogotá fue separada de su hija recién nacida, ante este hecho organizaciones de derechos humanos han solicitado a Ana Sofia Hidalgo, directora del lugar carcelario la actuación pronta para que la madre pueda reunirse nuevamente con su hija prematura quien se encuentra en el Hospital Simón Bolivar.

La Corporación Solidaridad Jurídica  argumenta en el  derecho de petición con fecha 26 de diciembre  que este hecho es una violación a los derechos de la menor y exigen una acción humanitaria pronta por parte del centro de reclusión. ([Asciende a 10 mil el número de prisioneros políticos en Colombia](https://archivo.contagioradio.com/10-mil-prisioneros-politicos-en-colombia/))

\[caption id="attachment\_18906" align="aligncenter" width="447"\][![carta CSPP](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/12/carta-CSPP.png){.wp-image-18906 .size-full width="447" height="572"}](https://archivo.contagioradio.com/prisionera-politica-es-separada-de-su-hija/carta-cspp/) Carta enviada a la directora Ana Sofia Hidalgo, Buen Pastor\[/caption\]
