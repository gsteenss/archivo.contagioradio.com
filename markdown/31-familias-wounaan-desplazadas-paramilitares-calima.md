Title: 31 familias Wounaan son desplazadas por paramilitares en Bajo Calima
Date: 2017-02-11 11:16
Category: DDHH, Nacional
Tags: Buenaventura comunidad wounaan, colombia, Desplazamiento, indígenas
Slug: 31-familias-wounaan-desplazadas-paramilitares-calima
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/02/a4f0e7d9-0b3f-4258-a0a1-892b95ef69f3.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: PBI Colombia 

###### 11 Feb 2017 

El Gobernador del Resguardo Indigena Wounaan Nonam Santa Rosa de Guayacán, del Bajo Calima, denunció que desde la madrugada del sábado inició el **desplazamiento forzado de 31 familias, causado por las constantes operaciones armadas criminales**, de lo que se presume son grupos paramilitares que rondan el Resguardo Humanitario Biodivierso y zonas aledañas.

El desplazamiento de las comunidades, se presenta ante la **desesperada situación** que vienen denunciando desde hace 15 días, por la presencia de **un grupo no mayor a diez hombres vestidos de negro que portan armas largas**, quienes los han hostigado, amenazado e incluso maltratado y torturado como en el caso del comunero[Jose Cley Chamapuro](https://archivo.contagioradio.com/paramilitares-torturaron-a-indigena/) ocurrido el pasado 4 de febrero.

### Una comunidad azotada por la violencia que vuelve a ser desplazada 

De acuerdo con lo manifestado por las comunidades, **las primeras 6 familias llegaron al puerto de Buenaventura promediando las 8:30 de la mañana**. Hasta el momento son 38 indígenas (6 Lactantes, 18 niños-as, 6 mujeres, 2 adultos mayores, 6 hombres) "Salimos a Buenaventura con miedo, pues sabemos que en todos los barrios hay paramilitares, pero tenemos que hacerlo, por que **no podemos esperar a que nos maten**, no podemos renunciar a exigir nuestros derechos" manifestó una de las mujeres en su arribo al puerto.

Desde la llegada de los irregulares, los habitantes del Resguardo se encontraban en estado de sitio, **sin poder movilizarse por el territorio para conseguir sus alimentos y suplir sus necesidades básicas**. Los hombres armados se desplazan sin ningún tipo de restricción entre los ríos Calima y San Juan, territorio dominado por los Urabeños. ([Le puede interesar: Desplazamiento forzado de 16 familias indígenas en Buenaventura](https://archivo.contagioradio.com/hombres-armados-desplazan-16-familias-indigenas-en-buenaventura/))

Los indígenas Wounaan Nonam, aseguran que, **a pesar de contar con medidas cautelares de la CIDH, la nula respuesta del Estado a la presente situación es la causa de este segundo desplazamiento, después del ocurrido en 2010**, que afecta la construcción de plan de vida humanitario y biodiverso que la comunidad construye como propuesta de paz desde el año 2011.

<iframe id="audio_16996682" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_16996682_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
