Title: 400 estudiantes de la Universidad Pedagógica marchan en apoyo a la Minga Nacional
Date: 2016-06-07 14:44
Category: Movilización, Paro Nacional
Tags: Marcha Universidad Pedagógica, Minga Nacional, Paro en Colombia, Universidad Pedagógica Nacional
Slug: 400-estudiantes-de-la-universidad-pedagogica-marchan-en-apoyo-a-la-minga-nacional
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/06/Marcha-UPN-Minga-Nacional.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Congreso de los Pueblos ] 

###### [7 Junio 2016 ] 

Las universidades públicas de la capital colombiana, se unen al paro nacional con el fin de apoyar a la Minga Social, Indígena y Popular. Viviana Téllez representante estudiantil de la Universidad Pedagógica Nacional, afirma que este martes se realiza un **carnaval desde la calle 72 con carrera 11 hasta el Ministerio de Agricultura, en el que se movilizan entre 300 y 400 personas** de diferentes organizaciones como el Congreso de los Pueblos, Marcha Patriótica y colectivos académicos.

Tellez asevera que esta manifestación es una fiel muestra de indignación, no solamente desde los procesos agrarios, sino desde el sector educativo, pues sí se habla de lograr un país con justicia social, la lucha es por los indígenas, los campesinos y por supuesto por los maestros. La estudiante denuncia que la actual crisis educativa es nacional y se caracteriza por el **desfinanciamiento estatal, la represión a la libertad de pensamiento y las regresivas reformas académicas**, como la que el Ministerio de Educación pretende contra las licenciaturas.

Tellez concluye afirmando que la Universidad Pedagógica apoyará las movilizaciones de la Universidad Distrital, entre ellas el homenaje a [[Miguel Angel Barbosa](https://archivo.contagioradio.com/fallecio-estudiante-udistrital-agredido-por-el-esmad/)]. Así mismo, planean escenarios de discusión en los que participaran además de estas dos universidades, la Universidad Nacional de Colombia, en defensa de los derechos humanos, la vida y la tierra y en apoyo a la [[Minga Nacional](https://archivo.contagioradio.com/programas/paro-nacional/)].

\[audio mp3="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/06/Viviana-Tellez.mp3"\]\[/audio\]

###### [Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU) ]] 
