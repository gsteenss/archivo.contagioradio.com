Title: Huilenses exigen que Emgesa acate orden de la Corte Constitucional
Date: 2015-12-11 12:53
Category: Ambiente, Entrevistas
Tags: Alejandro Ordoñez, ANLA, ASOQUIMBO, Corte Constitucional, EMGESA, Hidroeléctrica El Quimbo, megaproyectos en colombia, Miller Dussán, Procuraduría General de la Nación, Quimbo, represas en Colombia
Slug: huilenses-exigen-que-decision-de-la-corte-constitucional-sobre-el-quimbo-sea-acatada
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/12/Quimbo.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: [plataformasur.blogia.com]

<iframe src="http://www.ivoox.com/player_ek_9671745_2_1.html?data=mpukk5yYeY6ZmKiak5qJd6KnkpKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRjNbdzcrb1crXb8bsyszS0JDVucafxcrQy9jNaaSnhqeg0JDIqYzgwpCw0dfYqYy30NPg1s7YucTd0NOah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [11 Dic 2015] 

Respetando la petición del Tribunal Administrativo del Huila, la Corte Constitucional ordenó a la multinacional Emgesa suspender inmediatamente el funcionamiento de la hidroeléctrica El Quimbo, debido a que el **Presidente Juan Manuel Santos habría pasado por encima de la medida cautelar que había ordenado el Tribunal, mediante una acción popular interpuesta por la población** con la que se evidenciaba que no se había cumplido con el plan de manejo ambiental.

Por medio del decreto 1979 del 6 de octubre 2015, Santos había autorizado el inicio de generación de energía eléctrica con esa represa pese a que de acuerdo con la Corporación Autónoma del Magdalena,  CAM, **no se había removido la biomasa en 1.000 hectáreas del vaso del embalse, **que era uno de los requisitos teniendo en cuenta que se podría provocar daños ambientales irreparables que afectan la salud humana.

A su vez, el procurador General de la Nación, Alejandro Ordoñez, le  solicitó a la Corte Constitucional que **se derogara el Decreto 1979 de 2015 advirtiendo que la intención del Gobierno era evadir la orden judicial** que suspendió la generación de energía. [(Lea también "Con autorización ilegal de Santos se autoriza llenado de El Quimbo)](https://archivo.contagioradio.com/con-permiso-de-santos-se-inicio-llenado-del-quimbo/)

Cabe recodar que la construcción de la hidroeléctrica ya ha dejado más de 12 mil familias desplazadas, además no ha reparado a los pobladores que se quedaron sin tierras, y de las 2.900 hectáreas de terrenos que debían ser entregadas a los campesinos, “no se ha entregado ni un milímetro de tierra a la fecha”, denuncia Dusán, quien adicional, asegura que EMGESA ha actuado como quiere, gracias a la subordinación de la Autoridad Nacional de Licencias Ambientales, ANLA,  frente a la empresa.

Eso mismo afirma el gobernador del departamento del departamento del Huila, quien señala que EMGESA no ha materializado ninguno de los acuerdos pactados en la licencia ambiental.

La comunidad actualmente exige que sean incluidas, en el censo de afectados por la represa, **las más 36 mil personas que se declaran víctimas de este megaproyecto, y además, exigen que un organismo independiente sea el que garantice que la decisión de la Corte** Constitucional y además se cumplan las medidas ambientales y sociales que están en la licencia ambiental.

Por su parte, Emgesa aseguró mediante un comunicado que no se le ha notificado el pronunciamiento, por lo dijo que cuando conozca el documento oficial, lo analizará y se manifestará frente a esa decisión, lo que para la población significa que la empresa no acatará la orden, ya que según Jennifer Chavarro, también integrante de Asoquimbo, **"Emgesa siempre se burla de todas las ordenes judiciales",** de manera que debe haber movilización social para que se cumpla el fallo de la Corte Constitucional. [(Lea también “ANLA se subordina ante Emgesa").](https://archivo.contagioradio.com/anla-se-subordina-ante-emgesa-asoquimbo/)
