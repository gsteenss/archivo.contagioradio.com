Title: Comunidad LGBTI seguirá insistiendo en que no se dejarán sacar del acuerdo
Date: 2016-11-11 17:30
Category: LGBTI, Nacional
Tags: acuerdo de paz, Comunidad LGBTI, Juan Manuel Santos, LGBTI, mujeres
Slug: comunidad-lgbti-seguira-insistiendo-que-no-se-dejaran-sacar-de-acuerdo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/lgbti.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Humanist Society Scotland ] 

###### [11 Nov. 2016] 

Más de 100 organizaciones de la comunidad LGBTI que apoyaron el plebiscito por la paz, han solicitado al presidente Juan Manuel Santos, una **reunión para poder hablar sobre los recientes hechos de renegociación del acuerdo de paz que se está llevando a cabo en La Habana.**

Sin embargo, hasta el momento las organizaciones no han recibido respuesta alguna, más que el primer mandatario no cuenta con una agenda para poder atender a esta comunidad.

En la misiva las organizaciones aseguran que desde hace más de un mes han estado intentando reunirse con el primer mandatario sin embargo “**hemos visto cómo la Presidencia dialoga tanto con los sectores que impulsaron el No, como con otros sectores que promovieron el SI, pero respecto a las organizaciones y activistas LGBTI su actitud ha sido la de evitar cualquier referencia directa a nosotros”, afirma el escrito.**

De igual forma, Marcela Sánchez directora de Colombia Diversa aseguró que no se podría decir que se sacó el tema de la comunidad de LGBTI de los acuerdos “yo creo que ni el gobierno ni las Farc van a pasar de tener un acuerdo que era modelo en el mundo por la inclusión de la perspectiva de género y de grupos vulnerables a retroceder en este tema”.

Pese a esta negativa del gobierno nacional, Sánchez afirmó que desde las diversas organización de la comunidad LGBTI continuarán apoyando el proceso de paz “vamos a seguir insistiendo, no nos pueden sacar del acuerdo ni desconocer como actor político. Seguramente el presidente tendrá que escucharnos en algún momento y si no lo vamos a seguir haciendo a través de los medios y las redes sociales” concluyó.

###### Reciba toda la información de Contagio Radio en su correo [[bit.ly/1nvAO4u]
