Title: Infografía: 14 razones para no vender ETB
Date: 2016-05-01 08:09
Category: infografia
Tags: infografia, Venta ETB
Slug: infografia-14-razones-para-no-vender-etb
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/05/cacerolazo-etb-9.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

\[caption id="attachment\_23361" align="aligncenter" width="618"\][![razones para no vender la ETB](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/04/RAZONESPARANOVENDERETB.jpg){.wp-image-23361 .size-full width="618" height="1177"}](https://archivo.contagioradio.com/enrique-penaloza-anunciala-venta-de-la-empresa-publica-etb/razonesparanovenderetb/) El alcalde Enrique Peñalosa anuncio la venta de la empresa pública ETB durante la presentación del Plan de Desarrollo para Bogotá, ante el Concejo de la capital. Sin embargo desde los sindicatos, partidos políticos y redes sociales se han expresado razones para no vender la empresa de los bogotanos.\[/caption\]
