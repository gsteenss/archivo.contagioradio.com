Title: Víctimas apelan decisión de libertad para Álvaro Uribe y piden compulsar copias contra fiscal Gabriel Jaimes
Date: 2020-10-10 13:22
Author: CtgAdm
Category: Actualidad, DDHH
Tags: Corte Suprema de Justicia, Fiscalía General de la Nación, Iván Cepeda
Slug: victimas-apelan-decision-libertad-para-alvaro-uribe-piden-compulsar-copias-contra-fiscal-gabriel-jaimes
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/09/Caso-Uribe.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"justify"} -->

Ante la decisión tomada por la jueza 30 de control de garantías de Bogotá de revocar la medida de aseguramiento del exsenador Álvaro Uribe Vélez, el senador Iván Cepeda, víctima en el proceso que se adelanta contra el exmandatario por los delitos de soborno y fraude procesal a través de su abogado expresó que apelará la decisión a una segunda instancia y continuará luchando en los estrados judiciales, denunciando la responsabilidad del excongresista en los hechos de los que se le acusa.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Procesado Uribe "escoge juez, procedimiento y elimina todo lo adelantado por la justicia"

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Por su parte, **Reynaldo Villalba, representante del senador Cepeda, pide al juez de segunda instancia que revoque la decisión tomada y que mantenga vigente la medida de aseguramiento**. Pidió además que se determine si se deben compulsar copias e investigar de forma disciplinaria y penal al fiscal Gabriel Jaimes por su pronunciamiento y el desconocimiento y críticas hechas a las acciones tomadas por la Corte Suprema de Justicia.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

De igual forma reiteró que independientemente de si el caso avanzaba por la ley 600 o la ley 906 en ambas existía la medida de aseguramiento y con los mismos fines por lo que estaban ante una figura que coincide en nombre y en lo sustancial, que debía ser tenida en cuenta.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

El abogado expreso que "hay que buscar los mecanismos de adecuación bajo estos principios para que se ajuste a los pilares constitucionales sin que afecten las garantías de quienes participan en el proceso". Además alertó que esta decisión, de no existir una segunda instancia dejaría sin piso la seguridad penal frente a los actuales procesos que adelanta la Corte Suprema de Justicia contra el exsenador, además afectaría de manera grave la estabilidad del sistema penal pues **"es una puerta abierta a la fuga de procesados de la Corte Suprema de Justicia".**

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Villalba agregó que pese a que se señaló que se desconocía el derecho a la defensa, este se habían materializado "llevando un montón de testigos para desvirtuar los hechos que puso de presente la Corte Suprema de Justicia" y que por el contrario **las amenazas contra su representado, el senador Iván Cepeda, habían ido en aumento con especialidad después de la medida de aseguramiento** como consecuencia de declaraciones hechas por Álvaro Uribe a través de medios de comunicación y redes sociales.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

En ese sentido el exfiscal Perdomo aseguró que la jueza debió argumentar cuáles eran las garantías que se le estaban negando al procesado para poder argumentar que primero no se está violando el precedente y segundo que se está respetando el debido proceso y los derechos de todas las partes.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

El abogado Villalba además solicitó que los 23 anexos que fueron enviados a la jueza de garantías se han enviados al juez de segunda instancia solicitud que fue acogida por la jueza Clara Ximena Salcedo quien durante su intervención sostuvo que no era posible concebir "la validez de una medida de aseguramiento sin una previa formulación de imputación”, señalando que que su libertad en el nuevo sistema sólo se puede dar después de la imputación de cargos.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Lo que argumenta la Fiscalía

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Este martes la Jueza 30 de Control de Garantías, Clara Ximena Salcedo ya había decretado adelantar el proceso en contra del expresidente Álvaro Uribe a través del régimen procesal de la Ley 906 de 2004. Posteriormente señaló contrario a lo que argumentan las víctimas acreditadas, que no se puede equiparar la indagatoria en el viejo sistema y la imputación de cargos en el nuevo sistema.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

La jueza además expresó que no revisaría "la providencia de la Sala de Instrucción de la Corte Suprema de Justicia”. Por tanto, no hizo alusión a los documentos de 1.500 páginas con los que la Corte Suprema de Justicia sustentaba la decisión de la medida de prisión domiciliaria contra el exsenador. [(Lea también: Medios masivos "han sido incapaces de apartar su línea editorial del oficialismo" O Rincón)](https://archivo.contagioradio.com/medios-masivos-han-sido-incapaces-de-apartar-su-linea-editorial-del-oficialismo-o-rincon/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

No obstante, ante dicha decisión tomada por la jueza, el senador expresó que esta, **"no altera mi intima convicción de respeto por la justicia y de acatamiento a sus decisiones, no obstante o compartimos esa decisión y por supuesto vamos a apelarla".** La decisión de las víctimas se basa en que para ellas hay un desconocimiento de sus derechos y de un avance probatorio desestimado por la jueza Ximena Salcedo y que negaría lo avanzado por la alta corte.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/IvanCepedaCast/status/1314960982406508547","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/IvanCepedaCast/status/1314960982406508547

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph {"align":"justify"} -->

Del mismo modo Cepeda expresó su preocupación ante la actuación del fiscal Gabriel Jaimes a quien señaló de desarrollar "un libreto político" en el caso evidenciado su parcialidad por lo que considera, no existe "ninguna garantía para los derechos de las víctimas en este proceso".

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

El [senador Cepeda](https://twitter.com/IvanCepedaCast)ya había señalado en semanas pasadas que llevaría el caso ante la Comisión Interamericana de Derechos Humanos (CIDH) y presentará un informe a Michelle Bachelet, alta Comisionada de la ONU para los Derechos Humanos para así evidenciar la presión que se ejerció sobre la Corte Suprema de Justicia. [(Le recomendamos leer: Iván Cepeda recusará a fiscal y llevará caso Uribe a instancias internacionales)](https://archivo.contagioradio.com/ivan-cepeda-recusara-a-fiscal-y-llevara-caso-uribe-a-instancias-internacionales/)

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
