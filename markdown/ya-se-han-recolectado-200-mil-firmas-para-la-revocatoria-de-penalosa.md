Title: Ya se han recolectado 200 mil firmas para la revocatoria de Peñalosa
Date: 2017-02-28 16:16
Category: DDHH, Nacional
Tags: ETB, revocatoria Peñalosa
Slug: ya-se-han-recolectado-200-mil-firmas-para-la-revocatoria-de-penalosa
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/02/3373268_n_vir3.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo Particular] 

###### [28 Feb 2017] 

Desde que se inició la recolección de firmas para revocatoria del alcalde Enrique Peñalosa **se han recogido aproximadamente 200.000**, **haciendo falta solo 100 mil más para cumplir el límite impuesto por la Registraduría Nacional** y restando 5 meses para que culmine el plazo de entrega de los formularios.

Entre los motivos que exponen, tanto organizaciones sindicales como la ciudadanía para realizar la revocatoria, se encuentra la **venta de la Empresa de Telecomunicaciones de Bogotá (ETB) que hoy tuvo su cabildo abierto** y en donde hizo presencia tanto el alcalde Enrique Peñalosa, el presidente de la ETB Jorge Castellanos, como las organizaciones que convocaron el escenario para evitar la venta de esta empresa.

El alcalde, en medio de la exposición de motivos, señaló que **“la decisión de la venta de la ETB ya se tomó” debido a la aprobación del Concejo**. Además afirmó que la ETB es una “inversión empresarial de riesgo”, mientras que se podría invertir con esos mismos recursos en otros rubros como Educación o Salud. Le puede interesar: ["Listos formatos de recolección de firmas para la revocatoria de Peñalosa"](https://archivo.contagioradio.com/listos-formatos-de-recoleccion-de-firmas-para-revocatoria-de-penalosa/)

Sin embargo, Leonardo Arguello, integrante de Sintrateléfonos, indicó que “L**a Empresa de Telecomunicaciones de Bogotá, es sólida, es estable, genera riqueza y ha entregado unos dividendos importantes** al Distrito para inversión social, educación, entre otras cosas”. Le puede interesar: ["Peñalosa tendrá que responder sobre venta de la ETB en cabildo abierto"](https://archivo.contagioradio.com/penalosa-tendra-que-responder-sobre-venta-de-etb-en-cabildo-abierto/)

Y añadió que el problema de esta empresa es la falta de estrategia para que la misma crezca y aumente sus ganancias “la empresa no hizo más despliegue de fibra óptica en el país, no hay un plan estratégico de publicidad, **en Colombia no conocen nuestros servicios, la empresa quedo estática porque hay un plan de austeridad que nos lleva a quedarnos atrasados”.**

Una segunda jornada de Cabildo se debe realizar en el plazo de 8 días en donde la Alcaldía y el Presidente de la ETB, deben responder a las inquietudes que presenta la ciudadanía en este escenario. **De igual forma Sintrateléfonos expresó que continúan evaluando la posibilidad de hacer una consulta popular para evitar la venta de la ETB**

<iframe id="audio_17278055" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_17278055_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)
