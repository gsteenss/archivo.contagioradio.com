Title: Campañas políticas deben hablar del cambio climático en el Foro Al clima con el país
Date: 2018-04-27 14:45
Category: Ambiente, Política
Tags: cambio climatico, eleccion, Presidente 2018
Slug: campanas-politicas-deben-hablar-del-cambio-climatico-en-el-foro-al-clima-con-el-pais
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/02/Rio-amazonas-e1482197399239.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Viajes Mollar] 

###### [27 Abr 2018]

El próximo 2 de mayo, se llevará a cabo  el foro  “Al clima con el país: Ideas que no quedan en el aire”, un escenario que busca desde la juventud hacer  un llamado, a las diferentes campañas presidenciales y a un futuro presidente de Colombia, sobre el cambio climático como un desafío que debe ser atendido de manera urgente para salvar los ecosistemas y  para ser tenido en cuenta a la hora la toma de decisiones en el país

Para los jóvenes, Colombia junto con otros 195 estados, se comprometió por la lucha contra al cambio climático en el **“Acuerdo de París”**, aprobado a nivel nacional mediante la Ley 1884 de 2017 y recientemente avalado por la Corte Constitucional, razón por la cual el país, debe asumir este compromiso contra el cambio climático.

Debido a esto, los jóvenes le exigen al Estado que los puntos establecidos  en el acuerdo se materialicen en acciones prontas y efectivas para que el país se adapte al cambio climático y mitigue los factores que lo provocan. **Se espera que cada uno de los representantes de los partidos políticos que asistan den a conocer sus propuestas sobre protección al ambiente** y pactos para no llegar al punto de no retorno con el cambio climático.

Jhoanna Cifuentes integrante de ClimaLab, una de las organizaciones autoras de la iniciativa, expresa que **“el gobierno entrante definirá en gran parte como Colombia afrontará el desafío del cambio** climático de ahora en adelante y eso para ésta y las futuras generaciones significa mucho".

**Los llamados que la juventud le hace a las campañas presidenciales**

Con la iniciativa Al clima por el país: Ideas que no quedan en el aire", los jóvenes dieron a conocer 7 puntos que deberían tener en cuenta las campañas presidenciales para tratar el cambio climático:

1.  a\) Reconocer a la educación como eje fundamental para la transformación de la sociedad.
2.  b\) Aumentar la inversión en ciencia, tecnología e innovación que necesarias para poder establecer estrategias y soluciones para afrontar las consecuencias del cambio climático.
3.  c\) Integrar los conocimientos de las comunidades campesinas, indígenas y afro para la generación de soluciones en la lucha frente al cambio climático.
4.  d\) Incluir la adaptación al cambio climático y la gestión del riesgo como prioritarios en los planes y políticas nacionales.
5.  e\) Considerar la conservación de los ecosistemas como eje central de nuestro aporte en la lucha contra el cambio climático.
6.  f\) Impulsar la diversificación de la matriz energética y hacer una transición a energías limpias, como la eólica y la solar.
7.  g\) Implementar el principio de precaución frente a prácticas que tengan probabilidad de generar afectaciones graves a las comunidades y a los ecosistemas.

[**Es momento de hablar del clima**]

El debate se llevara a cabo el próximo **el próximo 2 de mayo en el auditorio Fundadores de la Universidad EAN desde las 6 p.m**. Tendrá dos espacios una consulta virtual juvenil y un foro nacional; donde se contara con la participación de líderes juveniles, organizaciones ambientalistas, expertos y sociedad civil en general. (Le puede interesar: ["En Barrancabermeja esperan que propuesta de consulta popular no sea una cto de politiquería"](https://archivo.contagioradio.com/barrancabermeja_consulta_popular/))

La iniciativa es impulsada por ClimaLab con la cooperación de la Fundación Heinrich Boell Colombia y participa también la Universidad EAN,  Asociación Ambiente y Sociedad, Sapiens Colombia,  Red Nacional de Jóvenes de Ambiente, Red por la Justicia Ambiental, Concienbíciate, Revista Ambiental Catorce 6, Contagio Radio, Movimiento Ambientalista Colombiano, Movimiento Climático de Jóvenes Colombianos, CLIC, La Ciudad Verde y ECOmunidad.

###### Reciba toda la información de Contagio Radio en [[su correo]
