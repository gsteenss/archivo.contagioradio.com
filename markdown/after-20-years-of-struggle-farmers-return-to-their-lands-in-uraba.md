Title: After 20 years of struggle, farmers return to their lands in Urabá
Date: 2019-05-29 12:17
Author: CtgAdm
Category: English
Tags: Lands, Urabá Antioqueño
Slug: after-20-years-of-struggle-farmers-return-to-their-lands-in-uraba
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/Tierras-1.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

[Twenty years after they were forcibly displaced from Guacamayas, a hamlet located in the Urabá region, eight Colombian families returned to their homes on May 15, after a Supreme Court decision ordered their lands be restituted.]{lang="EN"}

In 1996, these families were forced to sell their parcels, under pressure from real estate brokers and the [United Self-Defense Forces of Colombia, a now extinct paramilitary group. Still, they fought to have their lands returned via a drawn-out legal process that lasted between 10 and 11 years and ended last year. (Related: "[Academics around the world ask Duque for serious human rights policy](https://archivo.contagioradio.com/more-than-250-academics-criticize-duque-for-violence-against-activists/)")]{lang="EN"}

According to one of the beneficiaries, Juan Durán Guerra, these properties include parcels of 40, 50, 60 and even 70 hectacres. Durán Guerra said that despite the hardships ahead, such as the continued presence of paramilitary groups in the area, he believes that "things are coming to light, so that everyone can see and so that they see that we have a right to what is ours."

**“[Returning to our land is a blessing]{lang="EN"}”**

[While in the past these lands were stolen, Durán Guerra expressed elation at the opportunity to own the titles to his home. "Starting today, we're going to work on our ranches, where there are pastures, and livestock. We're going to move ahead", the farmer said. ]{lang="EN"}

Another beneficiary, María Candelaria, said there were moments of uncertainty as they struggled to have their lands returned. "Nothing is clear for me yet because the only thing that I know is that I plan to leave these lands to my children, which is what my dad fought hard for before he left this world."

![Restitución de tierras](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/Tierras-2-225x300.jpeg){.aligncenter .wp-image-67171 width="311" height="415"}
