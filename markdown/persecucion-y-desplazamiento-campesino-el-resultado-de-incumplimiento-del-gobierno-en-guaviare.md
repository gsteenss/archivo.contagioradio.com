Title: Persecución y desplazamiento campesino: el resultado de incumplimiento del Gobierno en Guaviare
Date: 2020-05-28 11:14
Author: CtgAdm
Category: Nacional
Tags: Abuso de la Fuerza Pública, Erradicación Forzada
Slug: persecucion-y-desplazamiento-campesino-el-resultado-de-incumplimiento-del-gobierno-en-guaviare
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/09/hoja-de-coca-1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: Erradicación en Guaviare

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Cachicamo es una de las siete veredas no suscritas **al Programa Nacional Integral de Sustitución de cultivos de Uso Ilícito (PNIS)** en la región de Guaviare y Meta donde continuó la siembra en medio de una interlocución constante con organismos del Gobierno, sin embargo la erradicación forzada que está en auge en medio de la cuarentena ha llevado a una tensión entre 800 campesinos y la Fuerza Pública que ha derivado en ataques hechos por la Fuerza Pública a agricultores y el abandono de veredas como resultado de la ausencia de oportunidades

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

El pasado 27 de mayo se conoció que en medio de un operativo de erradicación forzada, **fueron heridos tres campesinos con bala de fusil en las piernas: Alfonso Monsalve de 57 años, Cristhian Valanta de 24 año y Fernando Lozada Roa de 40 años**, fueron atendidos al interior de la comunidad, sin embargo no han podido desplazarse a San José del Guaviare y se espera puedan hacerlo en los próximos días. [(Lea también: Campesinos cumplen cuarentena mientras Ejército erradica forzadamente en Cauca)](https://archivo.contagioradio.com/campesinos-cumplen-cuarentena-mientras-ejercito-erradica-forzadamente-en-cauca/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Eliana Montoya Panche, integrante de la Alianza Étnica y Campesina del Guaviare, comunicadora popular relata que el problema de la erradicación forzada proviene incluso desde el 2017 cuando incluso en aquel entonces el general Enrique Zapateiro se comprometió con las comunidades de Guaviare y Meta a una serie de acuerdos en los que se comprometió a dar fin con esta práctica en la región, sin embargo, no se cumplió con lo pactado. [(Lea también: Catatumbo necesita un acuerdo humanitario, no erradicación forzada)](https://archivo.contagioradio.com/catatumbo-sigue-clamando-por-un-acuerdo-humanitario-que-ponga-fin-a-la-guerra/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Denuncia que en medio de la situación, el Ejército está vinculando a los dirigentes sociales y a la comunidad con disidencias de las FARC y grupos al margen de la ley, **"no tienen nada que ver la gente solo está defendiendo sus cultivos que es con lo que pueden llevar a sus hijos a estudiar"** afirma Eliana Montoya quien denuncia que la erradicación manual se detuvo pues el Gobierno incumplió con lo pactado. [(Le recomendamos leer: Alejandro Carvajal, joven de 20 años asesinado por el Ejército en Catatumbo: ASCAMCAT)](https://archivo.contagioradio.com/alejando-carvajal-joven-de-20-anos-asesinado-por-el-ejercito-en-catatumbo-ascamcat/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Las comunidades responsabilizan a los generales Eduardo Enrique Zapateiro y Raúl Hernando Florez, al cargo de las operaciones de erradicación forzosa y represión en Guaviare y Sur del Meta de lo que pueda ocurrir a los habitante a raíz de estos señalamientos. [(Le puede interesar: Ejército pone en riesgo a población de Piamonte, Cauca)](https://archivo.contagioradio.com/ejercito-pone-en-riesgo-a-poblacion-de-piamonte-cauca/)

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### La erradicación manual sí fue exitosa en Guaviare y Meta

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Aunque entre 2017 y 2018, Guaviare, el sur del Meta y el occidente del Vichada, fueron una de las primeras regiones en donde se desarrollaron los pilotos de sustitución manual vinculando a cultivadores de coca y entregándole a 5.344 familias desembolsos de recursos para su sostenimiento e inversión según la Alta Consejería para el Posconflicto, la situación en la actualidad dista de haber beneficiado a las comunidades

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Mientras Para finales de 2017 había 6.838 hectáreas de hoja de coca sembradas en Guaviare. y se certificó según la ONU el levantamiento de 2.583 hectáreas y en una segunda verificación el levantamiento de raíz de 1.285 hectáreas, en 2018 el número de cosechas al año presentó un incremento de 5,3 en 2018 en dicha región con 7.285 hectáreas.  
  
Para 2020 veredas como **La Tigra o Bellavista se encuentran casi en su totalidad deshabitadas,** pues a raíz de los incumplimientos del PNIS, las familias decidieron irse a buscar una mejor suerte, "cuando el programa debería garantizar que las personas se quedan lo que ha hecho ha sido un desplazamiento masivo" explica la comunicadora. [(Le recomendamos leer: Tras Acuerdo de Paz han sido asesinados 7 beneficiarios del PNIS en Córdoba)](https://archivo.contagioradio.com/tras-acuerdo-de-paz-han-sido-asesinados-7-beneficiarios-del-pnis-en-cordoba/)

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
