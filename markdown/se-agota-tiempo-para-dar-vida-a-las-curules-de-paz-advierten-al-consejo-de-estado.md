Title: Se agota el tiempo para dar vida a las curules de la paz advierten al Consejo de Estado
Date: 2020-12-09 20:27
Author: CtgAdm
Category: Actualidad, DDHH
Tags: Curules de la Paz
Slug: se-agota-tiempo-para-dar-vida-a-las-curules-de-paz-advierten-al-consejo-de-estado
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/Congreso-curules.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: Curules de Paz / Senado.gov.co

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Han pasado tres años y aún no se conoce un pronunciamiento del [Consejo de Estado](http://www.consejodeestado.gov.co/) que defina el futuro de las 16 Circunscripciones para la Paz diseñadas para amplificar la voz de las víctimas del conflicto en el Congreso. Defensores de las curules advierten que continuas dilaciones por parte de la bancada de Gobierno y organizaciones de víctimas **impedirían que las víctimas tuvieran una representación propia en la Cámara para el próximo periodo legislativo**, incumpliendo uno de los puntos centrales del Acuerdo de Paz.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Tres presidentes del Senado han pasado y ninguno ha definido el futuro de las circunscripciones de la paz

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

En 2017 la plenaria del Senado votó la conciliación del proyecto, fijando la mayoría para avalar el proyecto en 51 votos, sin embargo, algunas impedimentos de congresistas y el rechazo de partidos políticos como el Centro Democrático o el Partido Conservador limitó los votos a 50 dando lugar a un debate juridico.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Para salvar los escaños, **el exministro del Interior, Guillermo Rivera** demandó ante el Consejo de Estado el acto administrativo con el que se archivaban las curules de paz, sin embargo este proceso no ha avanzado al ritmo esperado y sigue sin decidirse si se alcanzó la mayoría para aprobar la reforma con los 50 votos.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Son varias las iniciativas que han buscado dar paso a las curules; en 2018 el senador Roy Barreras elevó una solicitud al presidente del Congreso, en ese entonces Ernesto Macías, senador del Centro Democrático para que promulgara el acto legislativo de las 16 circunscripciones especiales de paz, solicitud que negó y archivó.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Al finalizar el 2019 Barreras a través de una tutela negada en principio por el Tribunal Administrativo de Cundinamarca, solicitó a la Corte Constitucional se protegieran "los derechos a la igualdad, al debido proceso, y al participación política, vulnerados por la Mesa Directiva del Senado de la República", aclarando que la cantidad de votos necesarios para la aprobación del Proyecto **era 50 y no 51 como decía Macías, teniendo en cuenta que 4 senadores estaban capturados o inhabilitados para votar.**

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Pasaron los meses y aunque en enero de 2020, el presidente del Senado de aquel entonces Lidio García, tomó la decisión de revivir el acto legislativo que da vida las 16 curules de paz para las víctimas, **la Comisión de Conciliación del Senado se declaró no competente para autorizar esa medida**, sin embargo, pidió a las directivas del Congreso elevaran esta consulta a la Sala de Consulta del Consejo de Estado, institución que tiene la última decisión sobre el caso.[(Lea también: Víctimas del conflicto llevan más de tres años esperando sus curules)](https://archivo.contagioradio.com/victimas-del-conflicto-llevan-mas-de-tres-anos-esperando-sus-curules/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

A lo largo de estos años, el Consejo de Estado no solo ha negado las solicitudes para retirar la demanda contra el acto legislativo por parte de un Gobierno que también se opone a las circunscripciones como fueron establecidas en el Acuerdo. A su vez ha rechazado las de congresistas del Centro Democrático como María Fernanda Cabal, José Jaime Uscátegui, y grupos de víctimas que sostienen que las 180.000 víctimas que habitan en las zonas de las circunscripciones de paz no los representan en su totalidad.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Aunque las solicitudes de los congresistas del partido de Gobierno y las organizaciones han sido negadas, no existen avances pues se han presentado enseguida recursos de súplica que han impedido este continúe su propósito original, **es decir el retiro de la decisión que negó las curules.** Guillermo Rivera ya había expresado que los congresistas que se oponen a las curules, eran conscientes de que dichos recursos no prosperarían "pues la ley exige unos tiempos en los que una persona puede pedir ser parte del proceso" y dicho plazo se cumplió en 2019. [(Le recomendamos leer: Propuesta del Gobierno sobre Curules de Paz busca desconocer lo acordado en La Habana)](https://archivo.contagioradio.com/curules-gobierno-desconocer-acuerdo-habana/)

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### El trasegar de las curules en medio de un año con pandemia

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

En septiembre de este año, a través de una carta dirigida al Consejo de Estado, los demandantes y personas que han apoyado la implementación del Acuerdo solicitaron al Consejo de Estado dictar sentencia sobre las curules de la paz. De igual forma, para el pasado 2 de diciembre, Rivera presentó una **solicitud de impulso procesal**, denunciando que terceros han presentado maniobras dilatorias para que el proceso no avance y las 16 curules no puedan ser ocupadas por las víctimas en las elecciones de 2022.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Si existiera una victoria jurídica, la ley tendría que ser promulgada por la Presidencia y luego revisada en la Corte Constitucional, lo que llevaría un tiempo adicional y de no votarse en 2022, perderían total validez pues tal como fue establecido en el Acuerdo, las curules para la Paz serían transitorias durante ocho años finalizando en 2026. [(Le puede interesar: No podemos hablar de paz sin nuestra participación política: víctimas del conflicto)](https://archivo.contagioradio.com/no-podemos-hablar-de-paz-sin-nuestra-participacion-politica-victimas-del-conflicto/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Al respecto, personas como Juan Fernando Cristo, exministro de interior e integrante de Defendamos La Paz, han expresado que lo que se hizo en 2017 fue **"una maniobra política para debilitar estas curules, y se debe poner nuevamente en vigencia algo que ya fue legal".** Resalta que las curules fueron aprobadas por la plenaria del Senado en diciembre de 2017 y luego avaladas por un fallo de la Corte Constitucional y que estos **"han sido tres años de una larga espera de las víctimas para que se reconozcan sus derechos políticos en los territorios".**

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Cabe recordar que están en juego 16 escaños que estarían divididos en los departamentos que han sufrido mayores índices de violencia como Arauca, Antioquia, Bolívar, Cauca, Caquetá, Cesar, Córdoba, Chocó, Meta, Nariño, Norte de Santander, Putumayo y Tolima; los congresistas electos para Cámara de Representantes estarían de manera temporal y durante los períodos de 2018-2022 y 2022-2026. [(Lea también: Tras cuatro años de la firma del Acuerdo de Paz, la deuda más grande es con el campo colombiano)](https://archivo.contagioradio.com/tras-cuatro-anos-firma-del-acuerdo-de-paz-la-deuda-mas-grande-es-con-el-campo-colombiano/)

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
