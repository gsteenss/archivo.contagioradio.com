Title: El virus inoculado
Date: 2020-07-04 12:15
Author: AdminContagio
Category: Columnistas invitados, Opinion
Tags: Baile, musica, Salsa, Salsa colombiana
Slug: el-virus-inoculado
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/10/hurd-salsa-36x24-sold.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:heading {"level":5} -->

##### Por: Ángela Galvis Ardila.

<!-- /wp:heading -->

<!-- wp:paragraph -->

No recuerdo ya hace cuanto la vida me inoculó un virus, pero no fue cualquier virus. No. Fue uno muy potente, uno que me hace estremecer, que me hace sentir viva, que me ha acompañado en mis momentos más felices, pero también en los más aciagos, que me convierte en un ser nostálgico, pero también en el más resplandeciente.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":5} -->

##### Ese virus se llama Salsa.

<!-- /wp:heading -->

<!-- wp:paragraph -->

Y digo que no recuerdo de su inoculación, porque no fue como suele suceder en estos casos: por herencia familiar, o por tradición cultural, o porque una amiga o amigo me hubiera hecho adquirir la pasión. Dentro de mi memoria, en retrospectiva, me veo en mi cuarto siendo una niña, con una grabadora en las piernas intentando encontrar el mejor lugar para que la frecuencia de una emisora de AM entrara, y yo lograra, por fin, escuchar –así fuera mal- la música que ponía Radio K. Era una música que no se oía en mi pueblo, que no se oía en mi casa, que no oían las personas que me rodeaban, pero aun así fue una música por la que fácilmente sentí fascinación. Fue allí, en donde por primera vez oí la grandiosa voz de quien me conduciría por los caminos de este gusto visceral, oí por primera vez a Héctor Lavoe y su icónica canción El periódico de ayer.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

También descubrí sus fraseos sarcásticos en medio de El cantante, sus sentencias lacónicas pero ciertas en Todo tiene su final y sus descargas timbaleras en Aguanilé. En ese pequeño aparato del que salía una luz verde que resplandecía en la oscuridad, ya hoy obsoleto, escuché a Cortijo y su Combo cantando El Negro Bembón, con el tiempo supe que la voz virtuosa que me había maravillado en ese momento era la de otro enorme, nadie más y nadie menos, que el señor Ismael Rivera, quien luego como solista me haría sumergir en las aguas del soneo y de los ritmos caribeños, además de que me llevaría a transitar por canciones alegremente tristes como El incomprendido, una joya musical que ocupa el podio dentro de la banda sonora de mi vida.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":5} -->

##### Trasladarme a Bogotá trayendo inyectada en mis venas la salsa fue de las mejores cosas que me pudo pasar, pues fue llegar a un mundo en donde ya no tenía que buscar el rincón de un cuarto para oír el ecuahey de Rivera, ni la voz de Lavoe, ni el azúcar de Celia, ni la fuerza de Rafael Itier con el Gran Combo, ni el trombón de Willie Colón, porque este nuevo mundo, enorme para mí, ponía al alcance de mis manos la salsa que yo quisiera y estuviera dispuesta a oír.

<!-- /wp:heading -->

<!-- wp:paragraph -->

Los Van Van, Son 14, Orquesta Aragón, Hermanos Lebrón, Richie Ray y Bobbie Cruz, Ray Barreto, Ismael Miranda, Roberto Roena, Tito Curet, Cheo Feliciano, y muchos otros más, hacían parte del gran espectro musical que podía conseguir en ese lugar fascinante al que intentaba ir cada domingo para comprar un nuevo cassette.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El mercado de las pulgas se fue convirtiendo en mi plan anhelado del fin de semana, por todo el ambiente que lo rodeaba y que para mí era mágico. Allí vi bailar de manera embriagante un bogaloo por una pareja profesional; escuché hablar con experticia y detalles a sabedores de este género musical que, perfectamente, me los podía cruzar ofreciéndome un LP de Joe Cuba y su sexteto.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":5} -->

##### Esta ciudad tan inmensa como agreste, también puso a mi alcance lugares de salsa de todos los tamaños y estilos, con los personajes más diversos, con unas dinámicas sociales y raciales únicas.

<!-- /wp:heading -->

<!-- wp:paragraph -->

Entrar a uno de esos sitios era sentirme, si no en el paraíso, sí muy cerca, pues era experimentar en mis sentidos toda una estética cultural, de la que, poco a poco, me fui sintiendo parte. En estos lugares y por esos tiempos, descubrí las letras de otro de los grandes, que marcó en mí un sentido idealista del mundo: el panameño Ruben Blades y sus canciones impregnadas de poesía, de denuncias a la injusticia social, de odas al barrio, de discursos políticos confrontacionales.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Hoy, después de tantos años de haber oído por primera vez a Lavoe, de haber comprometido mis primeros sueldos para poder comprar toda la obra de La Fania, de haber convertido en salsero a mi esposo siendo él un rockero puro, de haberme sentido parte de esa lucha identitaria de Blades, siempre termino escuchando con el mismo fervor los bongós, los timbales, las trompetas, la clave, el piano, las voces y la fuerza de la salsa, de esa salsa de los 70 que es un volcán de musicalidad, de realidad, de heterogeneidad, y vuelve a resonar en mi corazón con la misma o más intensidad que antes.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

No tengo ni nunca tuve pretensiones de coleccionista ni de teórica de la salsa, y menos aún de referente musical, simplemente soy una amante del género, de su sonido, de su armonía, de su lírica, de la importancia como movimiento social, y este tiempo de pandemia en donde el pasado se añora, el presente es una tribulación de sentimientos y el futuro es más incierto que nunca, quedará encapsulado en mi memoria, como era de esperarse, con una canción de salsa que he oído día tras día: Maestra vida, porque nada más real que esta vida nos da, y nos quita, y nos quita y nos da.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":6,"textColor":"vivid-cyan-blue"} -->

###### [Le puede interesar: Nuestros columnistas](https://archivo.contagioradio.com/opinion/columnistas/) {#le-puede-interesar-nuestros-columnistas .has-vivid-cyan-blue-color .has-text-color}

<!-- /wp:heading -->
