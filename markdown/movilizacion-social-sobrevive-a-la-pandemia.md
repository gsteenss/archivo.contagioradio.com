Title: Movilización social sobrevive a la pandemia
Date: 2020-07-13 22:31
Author: PracticasCR
Category: Actualidad, Nacional
Tags: Cauca, Marcha por la dignidad, Movilización social, violencia
Slug: movilizacion-social-sobrevive-a-la-pandemia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/07/Movilización-social-Marcha-por-la-Dignidad.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Movilización Social - Marcha por la Dignidad / [Javier Jimenez](https://www.instagram.com/ghostx__/)

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Tras el arribó de la Marcha por la Dignidad el pasado viernes a Bogotá, se ha hecho evidente que **pese a la situación de pandemia que enfrenta el mundo, la movilización social en procura de la reivindicación de derechos colectivos sigue presente.** (Lea también: **[Otra Mirada: Con la marcha por la dignidad](https://archivo.contagioradio.com/otra-mirada-con-la-marcha-por-la-dignidad/)**)

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Muchas comunidades que viven en los territorios más afectados por la violencia se ven imposibilitados de atender la consigna «quédate en casa», ya sea por precariedad económica y la necesidad de obtener fuentes de ingreso que les permitan su mínima subsistencia, o bien porque confinados en sus casas se convierten en blancos más fáciles para los violentos.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

José Santos, de la Coordinación Nacional del [Proceso de Comunidades Negras](https://renacientes.net/), quien acompañó la Marcha por la Dignidad manifestó que la cruda situación de violencia que se vive en los territorios, obligó a emprender este largo recorrido con el fin de visibilizar esa problemática.

<!-- /wp:paragraph -->

<!-- wp:quote -->

> **Aún en medio de la pandemia nos vemos obligados a movilizarnos porque nos están matando y sacando de los territorios»**
>
> <cite>José Santos, Coordinación Nacional del Proceso de Comunidades Negras</cite>

<!-- /wp:quote -->

<!-- wp:heading {"level":3} -->

### **La dura situación de violencia en el** territorio

<!-- /wp:heading -->

<!-- wp:paragraph -->

José Santos manifestó que el Consejo Comunitario Afrorenacer del Micay del municipio de El Tambo le notificó desde diciembre pasado al Ministerio del Interior las diversas afectaciones que se están dando en el territorio, incluida la violencia que se ejerce sobre las comunidades, y que la única respuesta por parte de dicha institución fue que «se apoyaran en la policía».

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

**El líder denunció que en marzo entraron al territorio grupos armados como el «Carlos Patiño», una disidencia de las FARC**, los cuales empezaron a saquear las casas, a amenazar a la gente y a finales de ese mes asesinaron a una primera persona en la vereda Betania y a una en la comunidad de Honduras.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

También aseguró que en abril dos jóvenes más fueron asesinados en la vereda Agua Clara y que la semana pasada se presentó el asesinato de otras dos personas asesinadas a machete en el rio Micay. (Lea también: [Sigue en aumento el número de líderes sociales asesinados en Cauca](https://archivo.contagioradio.com/sigue-en-aumento-el-numero-de-lideres-sociales-asesinados-en-cauca/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por estos, y más hechos de violencia, fue que un grupo de marchantes decidió emprender la Marcha por la Dignidad desde el Cauca hasta Bogotá aún en medio de la pandemia, ya que según aseguró José Santos, aún **si no los mata la pandemia, sí lo harán los grupos que operan en los territorios.**

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### **Agresiones en la movilización social**

<!-- /wp:heading -->

<!-- wp:paragraph -->

Frente a los seguimientos e infiltraciones de las que fue objeto la Marcha por la Dignidad, José Santos señaló que efectivamente **sufrieron hostigamiento por parte de la Policía y que en algunos municipios quería impedirse el avance de la marcha.** «Esos son gajes del oficio» aseguró el líder afrocolombiano.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Sin embargo, enfatizó en que aún más grave que esos hechos,  es **la situación de violencia que padecen comunidades negras, indígenas y campesinas en los territorios ante un total desinterés del Gobierno Nacional** que no ha tomado acción alguna para que cese esta situación.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Adicionalmente, manifestó que tras su arribo a la Capital con la Marcha por la Dignidad, están adelantando una agenda de coordinación y colaboración con entes gubernamentales, organizaciones y embajadas de otros países.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Mientras adelantan esta gestión, José Santos señaló que darán tiempo a la llegada de **otras manifestaciones y marchas que provienen de Bucaramanga, Barrancabermeja, Arauca y otras regiones del país**, lo cual les permitirá juntar voces y acciones para visibilizar sus luchas y remediar la difícil situación que se vive en los territorios.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

De modo que se espera que estas iniciativas de movilización sigan surgiendo e incrementándose pese al escenario de pandemia y confinamiento. (Le puede interesar: [**Manifestación Social Audiovisual ¡A proyectar el próximo 21 de julio!**](https://archivo.contagioradio.com/manifestacion-social-audiovisual-a-proyectar-el-proximo-21-de-julio/))

<!-- /wp:paragraph -->

<!-- wp:core-embed/facebook {"url":"https://www.facebook.com/contagioradio/videos/420089758948255","type":"video","providerNameSlug":"facebook","className":""} -->

<figure class="wp-block-embed-facebook wp-block-embed is-type-video is-provider-facebook">
<div class="wp-block-embed__wrapper">

https://www.facebook.com/contagioradio/videos/420089758948255

</div>

<figcaption>
Otra Mirada: Movilización social en tiempos de pandemia

</figcaption>
</figure>
<!-- /wp:core-embed/facebook -->

<!-- wp:block {"ref":78955} /-->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->
