Title: Duque firma "sentencia de muerte a los ecosistemas" con piloto de Fracking en Colombia
Date: 2020-12-29 10:27
Author: CtgAdm
Category: Actualidad, Ambiente
Tags: #Duque, #Ecosistemas, #Fracking, #PuertoWilches, #Santader
Slug: duque-firma-sentencia-de-muerte-a-los-ecosistemas-con-piloto-de-fracking-en-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/11/No-al-fracking.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

El 24 de diciembre el presidente Iván Duque firmó el contrato para llevar a cabo el primer piloto de fracking en Colombia. Situación que no solo ha generado el rechazo del movimiento ambientalista que denuncia **“se firmó una sentencia de muerte en contra de los ecosistemas”**; sino que se suma a las promesas incumplidas de su campaña presidencial, en donde aseguró que no permitiría este tipo de exploraciones en su mandato.

<!-- /wp:paragraph -->

<!-- wp:heading -->

El proyecto Kale favorece intereses de multinacionales
------------------------------------------------------

<!-- /wp:heading -->

<!-- wp:paragraph -->

El proyecto llamado "Kale" se llevará a cabo por Ecopetrol con un contrato de 76 mil millones de dólares. Este se desarrollará en Puerto Wilches, Santander en la **cuenca media del río Magdalena y contará con 455 hectárea**s; se proyecta que las perforaciones inicien a mediados del año 2021, una vez obtengan los trámites de licenciamiento ambiental.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

No obstante, el congresista del partido Verde, Julián Rodríguez Sastoque denunció que si bien el contrato se anuncia como un “proyecto piloto de investigación orientado a los yacimientos no convencionales” autoriza la técnica del fracturamiento hidráulico, es decir el Fracking. Este hecho daría paso a que otros proyectos piloto solicitados por multinacionales como Conocophilips o Drumond puedan ser autorizados. (Le puede interesar:["Ni hoy ni nunca, entrevista Censat Fracking")](https://censat.org/es/analisis/fracking-ni-hoy-ni-nunca-entrevista-a-tatiana-roa-9615)

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

“De esta manera se oficializa la política de destrucción ambiental de Iván Duque, a través de este contrato que pone en riesgo la vida, la supervivencia de las especies, de nuestros ecosistemas, de nuestros páramos y la protección de nuestras fuentes hídricas”. (Le puede interesar: "[El fracking no es compatible con el desarrollo sostenible: Procuraduría](https://archivo.contagioradio.com/el-fracking-no-es-compatible-con-el-desarrollo-sostenible-procuraduria/)")

<!-- /wp:paragraph -->

<!-- wp:heading -->

"Duque firmó la sentencia contra los ecosistemas"
-------------------------------------------------

<!-- /wp:heading -->

<!-- wp:paragraph -->

Rodríguez también recordó que durante la campaña electoral, el actual presidente Duque afirmó que “el fracking en Colombia tiene demasiadas complejidades, y tiene afectaciones graves a acuíferos subterráneos, ecosistemas diversos, y además a mucha biodiversidad. **Yo no voy a dejar que se haga un solo proyecto de fracking en Colombia que afecte los ecosistemas, que afecte acuíferos y las cuencas**”.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Afirmaciones que van en contravía de sus promesas de campaña y que según Rodríguez evidencias los millonarios intereses detrás de este respaldo al fracking que es "una sentencia de muerte a los ecosistemas".

<!-- /wp:paragraph -->

<!-- wp:embed {"url":"https://twitter.com/ElJuliSastoque/status/1343685438536617985","type":"rich","providerNameSlug":"twitter","responsive":true,"className":""} -->

<figure class="wp-block-embed is-type-rich is-provider-twitter wp-block-embed-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/ElJuliSastoque/status/1343685438536617985

</div>

</figure>
<!-- /wp:embed -->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->
