Title: Más de 100 indígenas heridos dejan dos semanas de Liberación de la Madre Tierra en el Cauca
Date: 2015-03-06 17:31
Author: CtgAdm
Category: Movilización, Nacional
Tags: Indígenas del Cauca, ONIC
Slug: mas-de-100-indigenas-heridos-dejan-dos-semanas-de-liberacion-de-la-madre-tierra-en-el-cauca
Status: published

###### Foto: ONIC 

Durante las últimas dos semanas la represión por parte de las FFMM y de policía se han recrudecido contra las comunidades indígenas del norte del Cauca, tanto en el municipio de Corinto como en Santander de Quilichao, dónde se concentran cerca de 12000 indígenas nasa. Según Hector Dicué, vocero de la ACIN, ya son más de 100 comuneros heridos, dos de ellos de gravedad.

\[caption id="attachment\_5675" align="alignleft" width="279"\][![ONIC](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/03/indigenas-cauca2-contagio-radio1.jpg){.wp-image-5675 width="279" height="209"}](https://archivo.contagioradio.com/actualidad/mas-de-100-indigenas-heridos-dejan-dos-semanas-de-liberacion-de-la-madre-tierra-en-el-cauca/attachment/indigenas-cauca2-contagio-radio-2/) ONIC\[/caption\]

A pesar de que el pasado viernes 27 de Febrero y tras un saldo de 40 heridos, se llegó a un acuerdo de no agresión, el mismo fin de semana se presentaron varias agresiones por parte del ESMAD y la represión no ha terminado. Ante esta difícil situación el gobierno nacional no se ha pronunciado y tampoco ha respondido satisfactoriamente a la exigencia de las comunidades que piden presencia en el sitio de la protesta.

Este 5 de Marzo, las comunidades indígenas del Valle del Cauca afirmaron que se suman a la protesta tras el recrudecimiento de la represión. La ORIVAC afirma que el proceso de liberación de la madre tierra que se adelanta en el Cauca es una justa lucha, no solamente porque es la reparación prometida tras la masacre del Nilo, sino porque es un derecho ancestral.

Desde el pasado 23 de Febrero, comunidades indígenas del Norte del Cauca realizan lo que han llamado "proceso de liberación de la madre tierra" para hacer valer las promesas incumplidas del gobierno en cuanto a la entrega de tierras como parte de la reparación colectiva por la masacre del Nilo que contempla 30000 hectáreas.

\[caption id="attachment\_5676" align="alignleft" width="284"\][![Foto tomada de Twitter](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/03/indigenas-cauca3-contagio-radio.jpg){.wp-image-5676 width="284" height="213"}](https://archivo.contagioradio.com/actualidad/mas-de-100-indigenas-heridos-dejan-dos-semanas-de-liberacion-de-la-madre-tierra-en-el-cauca/attachment/indigenas-cauca3-contagio-radio/) Foto tomada de Twitter\[/caption\]

Las tierras reclamadas por los indígenas en los municipios de Corinto, Miranda y Santander de Quilichao, asciende a 6500 hc que el gobierno tiene alquiladas a los ingenios del Cauca, sobre los cuales pesan los intereses del propio ministro de agricultura que no se ha pronunciado sobre la reclamación de los indígenas.
