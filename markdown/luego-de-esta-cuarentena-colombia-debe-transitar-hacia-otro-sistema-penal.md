Title: Luego de esta cuarentena Colombia debe transitar hacia otro sistema penal
Date: 2020-05-05 17:30
Author: AdminContagio
Category: Expreso Libertad, Nacional, Programas
Tags: carceles de colombia, Covid-19, cuarentena, Expreso Libertad
Slug: luego-de-esta-cuarentena-colombia-debe-transitar-hacia-otro-sistema-penal
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/05/Crisis-carcelaria-en-el-COVID-19-una-bomba-de-tiempo.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:heading {"align":"right","level":6,"textColor":"cyan-bluish-gray"} -->

###### Foto por: Escuela Nacional Sindical {#foto-por-escuela-nacional-sindical .has-cyan-bluish-gray-color .has-text-color .has-text-align-right}

<!-- /wp:heading -->

<!-- wp:paragraph -->

La pandemia del Covid 19 y su llegada a las cárceles de Colombia, ha evidenciado la violación histórica de los derechos humanos que vive esta población y la poca acción por parte del Estado para garantizar la vida de quienes se encuentran privados de la libertad.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En este programa del **Expreso Libertad**, Camilo Villegas Rondón, abogado y docente, especialista en Instituciones juridico penales, el decano de la Facultad de Sociología de la Universidad Santo Tómas, Miguel Urra y la defensora de derechos humanos Camila Forero dialogaron sobre la urgencia por replantear el sistema penitenciario.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Una conversación que visibilizó las problemáticas de una sociedad que continúa relacionando el castigo con la privación y vulneración hacia la humanidad y que permite máximos de crueldad Les invitamos a escucharlo.

<!-- /wp:paragraph -->

<!-- wp:core-embed/facebook {"url":"https://www.facebook.com/contagioradio/videos/659610334894558/","type":"video","providerNameSlug":"facebook","className":""} -->

<figure class="wp-block-embed-facebook wp-block-embed is-type-video is-provider-facebook">
<div class="wp-block-embed__wrapper">

https://www.facebook.com/contagioradio/videos/659610334894558/

</div>

</figure>
<!-- /wp:core-embed/facebook -->

<!-- wp:paragraph -->

[Otros programas de Expreso Libertad](https://archivo.contagioradio.com/programas/expreso-libertad/)

<!-- /wp:paragraph -->
