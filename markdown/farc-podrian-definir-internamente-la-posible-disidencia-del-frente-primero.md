Title: FARC podrían solucionar internamente la situación del frente primero
Date: 2016-07-07 14:49
Category: Nacional, Paz
Tags: Conversaciones de paz de la habana, FARC, Frente Primero
Slug: farc-podrian-definir-internamente-la-posible-disidencia-del-frente-primero
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/07/farc-campamento-guerrilla-afp.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: afp] 

###### [7 jul 2016]

Las FARC podrían solucionar internamente la situación del frente primero definiendo una comisión para acercarse a las razones expuestas por esa estructura y buscar una salida diferente, afirma Luis Celis, analista político.

En la tarde de este miercoles se dio a conocer un comunicado en que ese frente afirma que no se acogerá a los acuerdos que se alcancen en la Habana. En el comunicado agregan que [no entregarán las armas](https://archivo.contagioradio.com/las-claves-del-cese-bilateral-entre-el-gobierno-y-las-farc/) "por considerar que la política del Estado colombiano y sus aliados sólo buscan el desarme y la desmovilización de las guerrillas"

Aunque no se ha conocido una confirmación oficial del comunicado difundido ayer, muchas voces de analistas coinciden en afirmar que se puede dar por cierta esa comunicación, puesto que en casi **ningún proceso de paz se logra una decisión unánime por parte de una guerrilla y casi siempre hay pequeñas disidencias.**

Luis Eduardo Celis, analista de la Fundación Paz y Reconciliación, afirma que en este momento las FARC tienen espacio político para intentar dirimir las diferencias internamente, y que en ello ya habría una comisión de la delegación de paz. Sin embargo, cuando este tipo de situaciones se presentan, **es posible que reversen decisiones tomadas según los acuerdos a los que se llegue en [medio de los acercamientos](https://archivo.contagioradio.com/estos-son-los-sitios-de-las-zonas-de-concentracion-de-integrantes-de-las-farc/)**[.](https://archivo.contagioradio.com/estos-son-los-sitios-de-las-zonas-de-concentracion-de-integrantes-de-las-farc/)

Además, Celis afirma que habría por lo menos 2 estructuras más de las FARC que podrían tomar una decisión en ese sentido, lo que no aporta para pasar de un conflicto armado a un conflicto en el que la sociedad civil es pieza fundamental de la exigibilidad de derechos. Así las cosas, si no se acogen todos los frentes de las FARC a los acuerdos, **se seguiría legitimando un discurso de guerra** y difícilmente se destinaría el presupuesto de la guerra para atender las exigencias del movimiento social.

Acerca de la estructura del **Frente Primero**, una de las características que señala Celis, es que **es un frente financiero**, por lo tanto el número de sus integrantes no es muy amplio pero están ubicados en el departamento del Guaviare en límites con Brasil y en esa región el tema de los cultivos de uso ilícito y los intereses empresariales para explotación de minerales como el COLTAN es muy fuerte de tal manera que podría garantizarse el sostenimiento.

<iframe src="http://co.ivoox.com/es/player_ej_12153569_2_1.html?data=kpeel5iZepqhhpywj5aVaZS1lJiah5yncZOhhpywj5WRaZi3jpWah5ynca3pytiYp8nZpdPY0JCwx9HNt4ampJCu0MbQrdTowpKSmaiRh9Di1cbUy9SPlsLYytSYj4qbh46o&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en[ [su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)] 
