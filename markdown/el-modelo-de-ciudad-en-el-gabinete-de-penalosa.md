Title: El modelo de ciudad en el gabinete de Peñalosa
Date: 2015-12-15 09:08
Category: Otra Mirada, Política
Tags: Alcaldía de Bogotá, Bogotá, Enrique Peñalosa, Gabinete, Secretarías en Bogotá
Slug: el-modelo-de-ciudad-en-el-gabinete-de-penalosa
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/12/peñalosa-y-su-equipo-.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Fernando Vergara 

###### [14 Dic 2015] 

Desde su campaña política para la alcaldía de Bogotá, el burgomaestre electo de la capital, no ocultó que su gabinete estaría compuesto por personas con capacidades gerenciales, que tengan experiencia en el sector público-privado y compartan su visión de ciudad. Con el nombramiento de algunos de sus secretarios y directores puede registrarse en sus perfiles profesionales un factor común: son personas con habilidades gerenciales: la mayoría de ellos, posee estudios en economía y han trabajado en sectores propios y ajenos a su cartera.

Una de los más grandes interrogantes, consiste en cuánto cambiará el modelo de ciudad y a quienes beneficiaría: sus partidarios creen que a la ciudad y la oposición piensa que a los que financiaron su campaña a la alcaldía. De igual manera, ha causado controversia el nombramiento de una funcionaria que hizo parte del gabinete de Samuel Moreno (Beatriz Arbeláez) y otra que es presidenta de una empresa minera y dirigirá la Secretaría de Integración Social. Más allá de esos cuestionamientos, es pertinente conocer a quienes manejarán las carteras esas entidades y qué le puede esperar a Bogotá.

### **Secretaría de Seguridad** 

**Daniel Mejía:** Será el encargado de encabezar la nueva Secretaría de Seguridad que está en proceso de aprobarse en el Concejo de Bogotá. Actualmente es director del Observatorio de Seguridad y Drogas de la Universidad de los Andes. Una de las principales preocupaciones en torno al trabajo de esta secretaría gira en torno a que Mejía dice que va a reestructurar los Centros de Atención a Drogodependientes (CAMAD), creados desde la administración de Gustavo Petro y que han sido un programa bandera en cuanto al tratamiento de las personas adictas en el marco de una enfermedad y no un delito.

Mejía estudió Economía en la Universidad de los Andes y tiene un doctorado en esa misma área en la Universidad de Brown. Ha ejercido como investigador, profesor y economista en distintas instituciones entre las que se encuentran: el Banco de la República, Fedesarrollo, la facultad de economía de la Universidad de Los Andes y el Banco Interamericano de Desarrollo.

### **Secretaria de Educación:** 

**María Victoria Angulo **Como nueva , Angulo afirma que mantendrá la política de jornada única en los colegios con el programa 40x40 de la administración saliente, junto con las iniciativas que fomenten la participación social y la acción ciudadana. Menciona que se han hecho avances en esa materia sobre los cuales se debe seguir trabajando. Sin embargo una de las preocupaciones gira en torno a la posibilidad de reabrir los convenios para educación básica y media, lo cual implicaría un detrimento de la educación pública.

Esta economista de la Universidad de Los Andes, tiene dos maestrías: la primera en Desarrollo Económico de esa misma universidad; la segunda en ** **Análisis Económico Aplicado de la Universitat Pompeu Fabra de Barcelona y ha trabajado en el sector público durante varios años en el Ministerio de Desarrollo, en Planeación Nacional y fue Secretaria de Hacienda en el segundo mandato de Antanas Mockus, además de hacer parte del Ministerio de Educación entre 2004 y 2011. Actualmente es directora de la Fundación de Empresarios por la Educación (EXE).

### **Secretaría de Hacienda: ** 

**Beatriz Arbeláez.** La funcionaria electa ya había ocupado ese cargo durante la administración de Samuel Moreno. Después de la destitución del burgomaestre, la alcaldesa encargada Clara López prescindió de sus servicios por su presunto interés de privatizar la ETB. No obstante, la economista de la Universidad Externado que posee estudios de especialización y maestrías en las áreas de finanzas y economía en la Universidad de Los Andes, Columbia y Strathclyde se ha desempeñado en entidades como el Ministerio de Hacienda, Bancafé, Colpensiones y Fogafín. Actualmente era la vicepresidenta financiera de Bancóldex, el Banco de Desarrollo Empresarial y Comercio Exterior de Colombia.

### **Secretaría de Integración** 

**María Consuelo Araujo.** Como presidenta desde 2013 de la compañía minera Gran Colombian Gold, llega una vieja conocida de Peñalosa a liderar la Secretaría de Integración Social: estará una mujer perteneciente a una familia con importante dinastía política. Araujo es profesional en Relaciones Internacionales de la Universidad Externado y es especialista en Gobierno, Gerencia y Asuntos Públicos de la Universidad de Columbia. Durante la primera administración de Enrique Peñalosa, fue directora del Jardín Botánico y posteriormente, en el segundo mandato de Antanas Mockus se desempeñó como directora del IDRD y posteriormente, Ministra de Cultura entre el 2002 y el 2006. Posteriormente, renunció a su puesto como canciller en el segundo periodo de Álvaro Uribe, debido a que su padre y hermano fueron vinculados con nexos de parapolítica. ** **

### **Secretaría de Salud** 

**Luis Gonzalo Morales:** Este médico especialista en Gerencia de Hospitales, con maestría en Salud Pública y doctorado en Medicina, será Secretario de Salud por segunda vez –ya lo había sido durante la primera administración de Peñalosa-. En declaraciones dadas al diario El Espectador, asegura que serán tres sus retos como jefe de cartera: “capitalizar la EPS Capital Salud, de la que es dueña la Alcaldía en 51%; superar el déficit de los hospitales públicos y avanzar en la infraestructura”, afirma. Hizo parte de la Dirección de Aseguramiento en Salud, Riesgos Profesionales y Pensiones en el Ministerio de Salud. Morales reconoce que la actual administración ha hecho avances en materia de gestión con programas como el de “Territorios Saludables”; no obstante, dice que puede mejorarse. Su perfil está orientado hacia el área técnica y gerencial de la rama de la salud.

### **Secretaría de Hábitat** 

**María Carolina Castillo:** La recién nombrada Secretaria del Hábitat, tendrá como prioridades, recuperar los planes de Vivienda de Interés Prioritario en la ciudad. Castillo es abogada de la Universidad Externado de Colombia y ha integrado a entidades en el sector público: en la Fiscalía General de la Nación, ejerció como Fiscal asignada a la Escuela de Estudios e Investigaciones Criminalísticas y Ciencias Forenses y en el Ministerio del Interior como Directora para la Democracia y la Participación Ciudadana y Directora de Asuntos Legislativos. Hasta hacer parte del gabinete de Peñalosa estuvo en el Ministerio de Vivienda en el cargo de Viceministra de Agua desde diciembre de 2014.

### **Secretaría de Planeación Distrital** 

**Andrés Ortiz:** Es gerente de la Constructora Contexto Urbano y encabezará la Secretaría de Planeación Distrital. Es arquitecto de la Universidad Javeriana y cursó estudios de posgrado en urbanismo en Londres. En una entrevista realizada junto con el exdirector de Fedelonjas Sergio Mutis y publicada por el IDU, Ortiz afirmó que es necesario estructurar un Plan de Ordenamiento Territorial que vaya en sintonía con la dinámica de las regiones y con territorios que no estén zonificados en cuanto a cuestiones como el comercio, el trabajo y la educación. Una de sus principales gestiones consistirá en presentar un POT que sea aprobado, puesto que el propuesto por la alcaldía de Gustavo Petro no recibió el visto bueno de parte del Concejo de Bogotá.

### **Secretaría de Movilidad** 

**Juan Pablo Bocarejo:** El recién nombrado Secretario de Movilidad se desempeña actualmente como profesor asociado de la Universidad de Los Andes, institución en la que se tituló como ingeniero civil. Estudió en la Universidad de París, en donde obtuvo un título de maestría y otro de doctorado. Bocarejo dirigirá la Secretaría en donde se definirá el futuro del metro en Bogotá, del cual aún no se tiene certeza sobre si gestionará sus planes con los estudios del metro planteados durante la administración Petro o si se realizarán nuevos estudios con una propuesta diferente.

### **Transmilenio** 

**Alexandra Rojas:** Como Gerente de Transmilenio, esta ingeniera industrial de la Universidad de Los Andes y economista de la Universidad de Maryland, se convierte en una de las personas de confianza de Enrique Peñalosa. Fue sub-Secretaria de Hacienda del alcalde electo y también se ha desempeñado en el Departamento Nacional de Transmilenio y dirigió el Fondo de Prevención Vial. Ha sido consultora en proyectos orientados hacia implementación de políticas públicas y gerencia pública.

### **Uaesp** 

**Beatriz Elena Cárdenas:** Se desempeñará como directora de la **Unidad Administrativa especial de Servicios Públicos **Uaesp, en donde opera uno de los proyectos insignia de la actual administración: Basura cero, en que el sistema de recolección de basuras está operado por el Distrito, se integra a los recicladores por primera vez en el esquema de aseo y se busca crear la cultura del reciclaje. Una de las posibilidades que se plantea es que el sistema regresaría a manejarse por el sector privado. Fue secretaría general del Ministerio de las Tecnologías y las Comunicaciones.

### **IDIPRON** 

**Wilfredo Grajales:** Es sacerdote salesiano. Tiene estudios en Teología y Ciencias Sociales. Peñalosa nombró a Grajales como director del IDIPRON. Ha trabajado en la Organización Corpovisionarios en compañía de Antanas Mockus. Recientemente se presentó como candidato del MAIS a la alcaldía de Cúcuta. Uno de los interrogantes con su nombramiento consiste en si se continuarán proyectos de apropiación del arte, tecnología y apropiación de los espacios que se están desarrollando desde esta administración o si se les daría un nuevo enfoque.

**Darío Montenegro:** Será el nuevo gerente de Canal Capital. Gerenció la RTVC, Canal 13, NTC y fue comisionado de la CNTV.

**Orlando Molano:** Estará al frente del IDRD.

**Jorge Catellanos:** Estará al frente de la ETB. Fue presidente de Bancafé, director de Fogafín y ha hecho parte de compañías como Correval y JP Morgan.

**Juan Ángel:** Será el nuevo director de IDARTES. Es actor y ha hecho parte de varias producciones colombianas. Ha intercedido por mejorar el reconocimiento del trabajo artístico en Colombia y ha apoyado a Peñalosa en varias de sus campañas.

**Eva María Uribe:** Presidirá la Empresa de Acueducto y Alcantarillado de Bogotá. Se ha desempeñado como Intendente de Servicios Públicos Domiciliarios; hizo parte del Ministerio de Minas y trabajó en la División de Infraestructura y Mercados Financieros del Banco Interamericano de Desarrollo.

Aunque aún es temprano para sacar conclusiones, se puede inferir que en materias como Ordenamiento Territorial, Seguridad, Hacienda, Integración Social y aseo, podría haber cambios sustanciales en cuanto a los perfiles que se tienen en la actual administración. Sin embargo, esto solo se sabrá cuando cada funcionario se posesione en su cargo a partir del próximo primero de enero.
