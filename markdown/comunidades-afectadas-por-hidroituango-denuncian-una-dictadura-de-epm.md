Title: Comunidades afectadas por Hidroituango denuncian una "dictadura" de EPM
Date: 2018-05-21 12:27
Category: Ambiente, Nacional
Tags: bajo cauca antioqueño, comunidades afectadas por hidroituango, EPM, Hidroituango
Slug: comunidades-afectadas-por-hidroituango-denuncian-una-dictadura-de-epm
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/05/DdstP13V4AM4wWu.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Movimiento Ríos Vivos] 

###### [21 May 2018] 

Continúa la crítica situación para las comunidades afectadas por Hidroituango en el Bajo Cauca Antioqueño. Tarazá, Puerto Valdivia y Cárceres han recibido la orden de evacuación debido a que en la noche del domingo 20 de mayo se presentó **un nuevo taponamiento en uno de los túneles de desviación** ocasionando una disminución del caudal del río aguas abajo y generando la posibilidad de una nueva avalancha. Las comunidades denunciaron que hay un desconocimiento de la situación y que EPM no ha sido clara con la información.

De acuerdo con Isabel Cristina Zuleta, integrante del Movimiento Ríos Vivos, “EPM ha ocultado información y **sigue engañando a las comunidades**”. Afirmó que las comunidades que habitan en la parte alta de la presa, no tienen claridad de lo que pueda suceder y denunció que hay una alerta roja para las comunidades de Valdivia, Cáceres y Tarazá que han visto como ha disminuido el caudal del río.

### **"EPM está imponiendo una dictadura" Ríos Vivos ** 

Ríos Vivos ha venido denunciando **las fallas en la atención a las personas afectadas** e indicó que es “EPM quien decide qué tienen que hacer las comunidades, cómo duermen, cuándo comen, cuándo se levantan”. En ese sentido, Zuleta afirmó que “el Estado está doblegado a la empresa y hay una preocupación porque nos han ocultado información”.

Desde hace varios días, las comunidades han alertado sobre la preocupación que genera la **disminución del caudal del río** aguas abajo y el aumento desmedido en la parte alta de la presa. Según Zuleta, “sólo hasta anoche nos dieron la razón de la preocupación. En algunos sectores el agua ha disminuido más de lo que disminuyó cuando se produjo la avalancha del 12 de mayo”. (Le puede interesar:["Hidroituango: Hay que liberar el río para evitar una catástrofe"](https://archivo.contagioradio.com/hidroituango-hay-que-liberar-el-rio-para-evitar-una-catastrofe/))

Ríos Vivos informó que “la empresa (EPM) ha marcado a las personas, **les ha puesto una manilla** y quienes la tengan pueden entrar a los albergues y pueden recibir apoyos humanitarios”. Además, “les imponen horarios para estar en el albergue y les tienen controlada toda la vida”.

Esta situación ha dificultado la organización de las personas y el procedimiento no ha sido claro. Zuleta confirmó que “hay más de **4 mil personas en el casco urbano** de Puerto Valdivia” y “quienes no tienen la manilla no están en los albergues y no han recibido ningún tipo de ayuda”. Este hacinamiento ha generado problemas de sanidad y hasta el momento no ha existido respuesta por parte de ninguna autoridad o institución para resolver la situación.

### **Las personas no saben hacia donde evacuar** 

En medio de la confusión, desde Ríos Vivos han denunciado que los sitios de albergue de Puerto Valdivia **ya están llenos** y las personas al escuchar las alarmas de evacuación “no saben para dónde ir”. Hecho que está generando caos, mientras siguen sonanado varias alarmas indicándole a la población que hay posibilidades de una nueva avalancha.

Adicionalmente, desde las comunidades habían elegido a diferentes líderes sociales para que hicieran presencia en el sitio de mando unificado, pero han denunciado que no se les **permitió el ingreso** porque “el defensor regional de Antioquia les prohibió el ingreso debido a los protocolos cuando están decidiendo por la vida de las personas”.

De igual modo, la petición que había hecho el Movimiento Ríos Vivos, para que fueran incluidos en las labores de organización de las personas evacuadas y afectadas, no fue resuelta. Por esto, Zuleta reiteró que los **movimientos sociales deben ser tenidos en cuenta** y recordó que necesitan de un albergue para los integrantes del Movimiento que cuentan con riesgos contra su vida debido a las constantes amenazas que han sufrido.

### **Comunidades reiteran llamado para que dejen correr el río libremente** 

Ríos Vivos afirmó que la emergencia se debe a una obstrucción que no permite que baje el agua del río Cauca. Las comunidades aguas abajo indicaron que el agua no es la del Cauca sino de **otros afluentes hídricos** que alimentan como el Río Ituango. (Le puede interesar:["Video: Hidroituango, una bomba de tiempo"](https://archivo.contagioradio.com/video-hidroituango-una-bomba-de-tiempo/))

Es posible que la presión del agua del río Cauca se esté presentando en las montañas, **aumentando la posibilidad de nuevos derrumbes**. “Puede suceder que las montañas, no sólo aguas arriba sino también abajo se puedan estar desmoronando por la erosión que ocasiona el bajo nivel del agua”, informó el Movimiento. Para ellos, de no dejar correr libremente el río, se podría presentar una emergencia en menos de 3 días.

Finalmente, Ríos Vivos recordó que se está haciendo una labor de recolección de ayudas en Medellín y Bogotá. Además, está habilitada la cuenta de ahorros de **Bancolombia 319-814716-39** a nombre de la Asociación de Mujeres Defensoras del Agua y la Vida. Con ese dinero las organizaciones sociales harán la compra de enseres y medicinas para las comunidades afectadas.

<iframe id="audio_26094048" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_26094048_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 

<div class="osd-sms-wrapper">

</div>

 
