Title: Violencia ambiental
Date: 2015-05-07 06:00
Author: CtgAdm
Category: invitado, Opinion
Tags: ambientalistas, Cañafisto, Dabeiba, Encuesta Nacional Ambiental, Espíritu Santo, Hidroituango, medio ambiente, megaproyectos, Parque Nacional Natural Nudo del Paramillo
Slug: violencia-ambiental
Status: published

###### Fotos: Ríos Vivos 

#### Por [Isabel Zuleta ](https://archivo.contagioradio.com/isabel-cristina/) -[~~@~~ISAZULETA](https://twitter.com/ISAZULETA)

¿De qué hay que liberar a la madre tierra? ¿De qué está presa? ¿De qué hay que libera a la naturaleza, a los ríos y a la vida? ¿Por qué asesinar, desaparecer, amenazar, señalar, judicializar y perseguir a quienes pretenden liberar a la naturaleza y con ello posibilitar la vida en el planeta?

La mayor manifestación de la violencia en sus múltiples formas es la injusticia con la cual no hay posibilidad de libertad, por eso todas las luchas de los pueblos por la justicia, incluida la ambiental, tienen el horizonte de la libertad. El capitalismo ambidiestro conoce esta realidad y por eso extermina los impulsos de libertad que son de dignidad expresados en la organización comunitaria. No hay otra manera de sustentar su interés por concentrar cada vez más capital que impedir la justicia.

La violencia socio-política se manifiesta también en las injusticias hídricas, energéticas, de acceso a la tierra, alimentarias, de salud, de educación, por la primacía del capital, por la acumulación por despojo. La violencia ambiental se sustenta en la economía por encima de la política que tiene presa a la madre tierra  y a la humanidad entera.

Las luchas ambientales son planetarias. La contradicción capital - naturaleza es cada vez más evidente, es el capital o es la naturaleza, no hay puntos medios. El modelo capitalista esta destruyendo la vida, de esto son conscientes hasta los citadinos tan dispersos y alejados de la madre tierra, los resultados de la Encuesta Nacional Ambiental (Realizada sólo en las ciudades) así lo muestran. El segundo tema de mayor preocupación en el mundo es el ambiente (49.6%) después de la economía (52%). Esta tensión ha sido resuelta hasta ahora con el “sacrificio” de ciertos territorios y la “protección” interesada de otros, pero el exterminio avanza y los lugares de sacrificio tendrán que extenderse.

La contradicción de los citadinos colombianos es por un lado considerar el tema ambiental prioritario para el mundo y por otro ubicarlo en el quinto lugar para el país y en el cuarto para la ciudad (Encuesta Nacional Ambiental) disputando los primeros lugares con la salud y la seguridad que sin duda tienen que ver con el ambiente o como si el país y las ciudades no hicieran parte del mundo. La percepción de los citadinos con relación al ambiente para el planeta es una especie de intuición lejana y distante o tan pegada a los ojos y al cuerpo que no les permite mirar, quizás sea por las enfermedades, el aire irrespirable o la irritación en los ojos, lo que es evidente es que no logran darse cuenta de lo que ocasionan con estar concentrados en grandes urbes insostenibles.

El conflicto armado interno en Colombia ha traído graves consecuencias ambientales. Los guerristas de las ciudades burbuja que desdeñan del sufrimiento del campo con los bombardeos los piden a gritos desconociendo los impactos en la naturaleza como la destrucción de bosques y la contaminación de aguas. Un sin fin de acciones militares que de un lado o del otro, aunque la escala importa y mucho, han dañado los ecosistemas de los cuales dependemos múltiples especies. Las consecuencias ambientales del conflicto armado no han sido calculadas y hacen parte de la memoria histórica que aún se le debe al país.

El cese al fuego bilateral también es una exigencia ambiental porque no es cierto que la guerra haya protegido o liberado de la destrucción a la naturaleza. Los hechos violentos han generado miedo en los destructores y esto ha permitido la conservación de algunos ecosistemas, la posibilidad de un acuerdo entre las insurgencias y el Estado se ve como un escenario de riesgo para estos. Hay que decirlo claramente, más que el miedo de las empresas y colonizadores lo que ha habido allí es un desinterés del aparato estatal en estos territorios porque cuando este interés se despierta no ha habido violencia que frene la destrucción, por el contrario el Estado convierte la violencia en funcional a la destrucción.

Es el caso de Hidroituango una zona altamente conflictiva en la que ha existido presencia histórica de diferentes frentes de las FARC EP, este hecho logro disuadir a colonizadores y favoreció las condiciones para una alta conservación del bosque seco tropical en vía de extinción en el planeta pero no logro disuadir al Estado que es el que esta perpetrando la destrucción y para quitar del lado el obstáculo de la violencia la exacerbo con la presencia de nuevos batallones, la construcción de bases militares y puestos de policía con dineros propios de la obra para obtener el control de un territorio en el que antes del capital no había habido ningún interés, más que el militar. El Parque Nacional Natural Nudo del Paramillo esta siendo cercado por grandes represas primero Urrá I, ahora Hidroituango, en proyecto Urrá II, Espíritu Santo, Cañafisto, otras en Dabeiba en el Bajo Cauca y 12 megaproyectos más que expresan la contundencia de la violencia ambiental y el interés por el control de un territorio que no sólo es un ecosistema estratégico en términos ambientales sino y principalmente en términos económicos y militares.

En esta zona la violencia social y ambiental a servido para desocupar el territorio y hacer más fácil la destrucción ¿Pero cuales son los reales intereses? ¿Quienes tienen el control? ¿La violencia ambiental es de utilidad pública?

El obstáculo del conflicto armado para los intereses económicos es meramente económico, es decir, sale más costoso el proyecto por la alta inversión que representa, pero la salida es fácil la militarización de cuenta del Estado, lo único necesario es el interés de este.

 
