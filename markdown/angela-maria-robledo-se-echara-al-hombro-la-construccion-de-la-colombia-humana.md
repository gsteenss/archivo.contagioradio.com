Title: Ángela María Robledo se echará al hombro la construcción de la Colombia Humana
Date: 2019-04-29 16:52
Author: CtgAdm
Category: Nacional, Política
Tags: Angela Maria Robledo, Colombia Humana, Congreso de la República
Slug: angela-maria-robledo-se-echara-al-hombro-la-construccion-de-la-colombia-humana
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/D5BpuWHXoAA9Tsx.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:] 

La ex representante a la Cámara Ángela María Robledo afirma que a pesar de respetar la institucionalidad y tener enormes interrogantes respecto a la decisión que tomó la Sala Quinta del Consejo de Estado, buscará agotar los recursos para restablecer sus derechos para permanecer en el Congreso.

Robledo advirtió que su caso es diferente al de Antanas Mockus quien también perdió su curul en el Senado pero busca alternativas jurídicas para recuperarla. Si bien son caminos similares al de Antanas son situaciones diferentes, "pero creo que el  propósito era el mismo, **"intentar llevarnos al mismo lodazal donde se han movido las prácticas clientelistas de tantos políticos"** concluye.

Frente a la posibilidad de participar en las contiendas electorales por la Alcaldía de Bogotá, señala que existe una coalición conformada por Colombia Humana, el Polo Democrático, el Partido Verde, Decentes y los liberales independientes donde  se habla de buscar un acuerdo programático y un mecanismo para establecer qué candidato sería el que habría que apoyar, pero que por encima de su participación o no, el propósito es recuperar la ciudad  señaló y manifestando que "espera apoyar la candidatura de Gustavo Petro  para el 2022". (Lea también: [Pérdida de curul de Ángela María Robledo es un golpe a la oposición](https://archivo.contagioradio.com/perdida-de-curul-de-angela-maria-robledo-es-un-golpe-a-la-oposicion/))

**Fe de errata**

Debido a un fallo de comunicación durante la entrevista telefónica, fue malinterpretada su declaración, Ángela María Robledo no ha descartado su participación en las elecciones por la Alcaldía de Bogotá,  ofrecemos disculpas a nuestros lectores y oyentes.

<iframe id="audio_35104013" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_35104013_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
