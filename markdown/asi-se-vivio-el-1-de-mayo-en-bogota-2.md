Title: Así se vivió el 1 de Mayo en Bogotá
Date: 2016-05-01 23:08
Category: Otra Mirada, Reportajes
Tags: 1 de mayo, Bogotá, dia del trabajo
Slug: asi-se-vivio-el-1-de-mayo-en-bogota-2
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/05/1-de-mayo-7-2016.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Fotos: Gabriel Galindo 

Las movilizaciones del **primero de mayo, día internacional de los trabajadores** estuvieron llenas de colorido en la ciudad de Bogotá. Sindicatos, partidos políticos, movimientos estudiantiles y juveniles, se reunieron para marchar por la carrera séptima. El recorrido inició en el Planetario Distrital hasta la Plaza de Bolivar, las exigencias de la marcha fueron cantadas, coreadas y escritas en las paredes, una de las más reiteradas por los trabajadores fue la oposición a la v[enta de la Empresa de Teléfonos de Bogotá](https://archivo.contagioradio.com/enrique-penaloza-anunciala-venta-de-la-empresa-publica-etb/) (ETB), el desacuerdo  con el  modelo de salud que ha instaurado el Alcalde Peñalosa en  la ciudad capitalina. La soberanía nacional y el grito de un trabajo digno para todos y todas fueron otras de las peticiones de los trabajadores.

La movilización terminó hacia la una de la tarde, hora en la que algunos jóvenes lanzaron piedras contra integrantes de la Policía, según el reporte de la Alcaldía hay veinte jóvenes detenidos que se encuentran en la Unidad Permanente de Justicia y cinco personas heridas.  (UPJ)

\
