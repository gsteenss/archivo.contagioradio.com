Title: Anualmente se presentan 118.000 tutelas por el derecho a la salud en Colombia
Date: 2015-08-21 16:40
Category: DDHH, Movilización, Nacional
Tags: acción de tutela, Alejandro Gaviria, crisis de la salud, Mesa Nacional de la Salud, ministerio, Ministerio de Salud, Tutelas por el derecho a la salud
Slug: anualmente-se-presentan-118-tutelas-por-el-derecho-a-la-salud-en-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/08/image-e1451422339367.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: diarioadn.com 

###### [21 ago 2015]

En su más reciente informe la sobre la salud en Colombia la Defensoría del Pueblo presentó cifras  que demuestra que las **tutelas han aumentado considerablemente de 115.000 a 118.000** en tan solo el último año, con solicitudes para tratamientos, medicamentos y concesión de citas médicas, es decir, más de 360 tutelas diarias para que se garantice este derecho.

La situación que plantea la Defensoría **es alarmante en la medida que el 70% de las tutelas que imponen los ciudadanos es en materia de violaciones al derecho fundamental**, los ciudadanos realizan  solicitudes que ya están incluidas en el Plan Obligatorio de Salud.

Sin embargo en el debate que se llevó a cabo  en el congreso de la República que contó con la participación del Ministro de Salud, Alejandro Gaviria, indicó que "es mentira decir que Colombia tiene crisis de salud pública" y **desmintió la veracidad de estas cifras indicando que estas tutelas se han estabilizado en los últimos años** y mencionó  una cifra muy por debajo de las que presenta la Defensoría en cuanto a las tutelas anuales, con tan solo 1000 al año.

En el debate que contó con la participación de tan solo 11 representantes a la cámara de 166 que son en total, causando la indignación de muchos ciudadanos que a través de las redes sociales manifestaron su inconformidad por la falta de seriedad con la que se trata el tema del derecho a la salud.

Con este informe la Defensoría del pueblo hizo un llamado  para que las entidades promotoras de salud (EPS) presten un servicio de calidad a los colombianos y que el gobierno preste atención a sus deberes con la salud de los colombianos. Vea también: [En Colombia el derecho a la salud está en cuidados intensivos](https://archivo.contagioradio.com/en-colombia-la-salud-se-encuentra-en-cuidados-intensivos/)
