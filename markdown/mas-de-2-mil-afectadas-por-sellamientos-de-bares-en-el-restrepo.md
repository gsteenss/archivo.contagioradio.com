Title: Más de 2 mil afectadas por sellamientos de bares en el Restrepo
Date: 2016-09-16 13:46
Category: Movilización, Nacional
Tags: administración Enrique Peñalosa, Alcaldía de Bogotá, Secretaría de Ambiente
Slug: mas-de-2-mil-afectadas-por-sellamientos-de-bares-en-el-restrepo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/09/Sellamiento-de-bares.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Cundinamarca ] 

###### [16 Sept 2016] 

Este jueves, la Asociación de Comerciantes Nocturnos Formales de la Zona Rosa del Barrio Restrepo en Bogotá ASORUMBA, se tomó pacíficamente las instalaciones de la alcaldía local de Antonio Nariño para rechazar la medida de sellamiento interpuesta a varios de los **bares y las discotecas que ya han cumplido con la insonorización**, exigida por la Secretaria Distrital de Ambiente.

Según la Asociación, **el 70% de los establecimientos han invertido más de \$500 millones en las insonorizaciones**; sin embargo, la Secretaria de Ambiente lleva más de siete meses sin realizar las visitas de verificación para levantar los sellos que han impedido el normal funcionamiento de los locales y que ha afectado indirectamente la subsistencia de por lo menos 2 mil familias que dependen del comercio que se genera alrededor de los bares y las discotecas.

Los comerciantes se dirigieron a la alcaldía para exigir la instalación de una mesa de trabajo en la que hayan delegados de la Secretaria Distrital de Ambiente, la Secretaria de Gobierno, la Personería de Bogotá, ediles de la localidad y representantes de ASORUMBA, y en la que se acuerde la **revisión de los expedientes de las discotecas que permanecen selladas **y que tienen en eminente riesgo de quiebra al sector comercial, como lo asegura el edil Víctor Silva.

El edil asevera que la mesa de diálogo ya se instaló y sesionará durante este viernes, pues **son por lo menos 140 los locales comerciales que están sellados**, la mayoría de ellos con un mínimo de 40 empleados, que esperan que la Secretaria Distrital de Ambiente verifique que las insonorizaciones ya se realizaron y levanten los sellos.

Vea también: [[Administración Peñalosa ha desalojado 640 vendedores informales](https://archivo.contagioradio.com/administracion-de-penalosa-ha-desalojado-640-vendedores-informales-en-los-ultimos-dias/)]

###### [Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)]] 

 
