Title: Frente al posconflicto 88 municipios en riesgo extremo por presencia paramilitar
Date: 2016-03-23 13:07
Category: Nacional, Paz
Tags: Fundación paz y reconcilización, paramilitarismp, posconflicto, proceso de paz
Slug: para-el-posconflicto-88-municipios-estan-en-riesgo-extremo-por-presencia-paramilitar
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/03/Posconflicto.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Gobernación Cundinamarca 

###### 23 Mar 201 

‘Los retos del posconflicto. Justicia, seguridad y mercados ilegales’, es el nuevo informe que publicó la Fundación Paz y Reconciliación, en que se señala que  **88 municipios de Colombia se encuentran en riesgo extremo tras fin del conflicto con las Farc.**

Los grupos neoparamilitares son la principal amenaza para departamentos como **Nariño, Chocó, Antioquia, Cauca, Putumayo y Bolívar.  **Los municipios Roberto Payán Barbacoas, Olaya Herrera y Magüí, de Nariño; San Miguel y Puerto Asís, de Putumayo, y Timbiquí y El Tambo, del Cauca.

"Ahora podemos decir que la desmovilización de las Autodefensas Unidas de Colombia tuvo un carácter parcial y que los mandos medios y los reductos paramilitares que persistieron después de cerrado el ciclo de negociación fueron el reservorio de las nuevas bandas criminales”, asegura el informe.

Según el estudio que present este miércoles León Valencia y Ariel Avila en La Habana, **el 25,5 % de los municipios del país tendrá algún tipo de vulnerabilidad.** Además la presencia de otros grupos armados ilegales, como bandas criminales (neoparamilitares) o el ELN, es uno de los factores por los que 281 poblaciones, en **26 departamentos, “podrían continuar con diversos riesgos o viviendo épocas violentas”. **

Valencia, director de la Fundación, explica que la clasificación  del riesgo de las diferentes zonas del país se realizó con indicadores frente a actividades ilícitas, condiciones sociales, necesidades básicas insatisfechas, condiciones de acceso y cuál es la presencia institucional.

Al ser los grupos neoparamilitares la principal amenaza para el posconflcito, el informe indica que en los últimos 8 años, **han sido condenados 61 parlamentarios por su alianza con los paramilitares** y han estado en investigación otros 67, por lo que se sostiene que el reto también tienen que ver con una reconciliación política en el país.

**“Se trata entonces de una doble reconciliación: la de las guerrillas con el Estado y la de las élites con la legalidad democrática”,** de manera que se concluye que el punto inicial es que las comunidades de esos municipios en alto riesgo de violencia en el posconflicto entiendan lo que significa el fin de la guerra para sus regiones y la confianza en las instituciones.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)
