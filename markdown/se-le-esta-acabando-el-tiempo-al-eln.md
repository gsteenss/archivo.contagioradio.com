Title: ¿Se le está acabando el tiempo al ELN?
Date: 2016-02-02 15:57
Category: Nacional, Paz
Tags: conversaciones de paz con el ELN, ELN, FARC, paz en colombia
Slug: se-le-esta-acabando-el-tiempo-al-eln
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/02/ELN-4-contagioradio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: lasdosorillas 

###### [01 Feb 2016]

Luego del comunicado conocido ayer, en que la guerrilla del **[ELN afirma estar listo desde el mes de Noviembre](https://archivo.contagioradio.com/desde-noviembre-eln-esta-listo-para-la-fase-publica-de-proceso-de-paz/)**, para el inicio de la fase pública de conversaciones de paz con el gobierno, el alto comisionado de paz, Frank Pearl emitió un comunicado en el que afirma que no es cierto que no se hayan definido fechas para un encuentro posterior a la última reunión y que a esa guerrilla se le está acabando el tiempo para iniciar la fase pública de  las conversaciones de paz.

Algunos allegados a este proceso afirman que la contradicción en los comunicados puede estar basada en que es necesario **mostrar una posición dura antes de un anuncio en ese sentido**, pero por otra parte podría ser que la dilación y este tipo de anuncios pretenden desestabilizar a esa guerrilla para buscar una reacción que abriría la puerta de una disputa en el discurso.

Si se tiene en cuenta la segunda opción, podría estar encaminada a dilatar el inicio de las conversaciones para evitar que se dé una convergencia de sectores sociales tras la firma de un acuerdo de paz conjunto, esto, si se tiene en cuenta que desde algunos de esos sectores **se está hablando de un gobierno de transición que pondría en riesgo los intereses empresariales** presentes en los territorios, afirman analistas.

Así las cosas el anuncio del inicio de la fase pública de conversaciones de paz con el ELN estaría muy próximo a definirse o por el contrario, **podría incluso, demorarse hasta después de la firma de los acuerdos de paz con la guerrilla de las FARC.**

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
