Title: Estudiantes se movilizan en defensa de Inmigrantes en Estados Unidos
Date: 2016-11-16 13:12
Category: El mundo, Movilización
Tags: estudiantes, inmigrantes estados unidos
Slug: estudiantes-se-movilizan-en-defensa-de-inmigrantes-en-estados-unidos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/estudiantes-EEuu-e1479319305513.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Periódico Noroeste] 

###### [16 Nov 2016] 

Masivas movilizaciones se han  presentado en lo corrido de esta semana en Estados Unidos por parte de estudiantes de secundaria y universidades de todo el país. Los jóvenes están exigiendo que **no se realicen las deportaciones anunciadas por Trump y otras medidas contra indocumentados e inmigrantes en el país.**

Ayer estudiantes de los diferentes colegios públicos de Los Ángeles, Estados Unidos salieron a la calles con pancartas con mensajes como “mantengámonos juntos” y banderas de diferentes nacionalidades, para expresarle a Donlad Trump que no están de acuerdo con lagunas de las medidas anunciadas por el, como la **deportación de 3 millones de inmigrantes que se encuentran en el país.**

Hoy el turno es para estudiantes de diferentes universidades del país, como Harvard o Yale, que además hicieron una petición para que los centros educativos protejan a estudiantes indocumentados amenazados por las posibles de portaciones, que se denominarían “Campus Santuarios”. Le puede interesar:["Xenofobia no debe tener lugar en la política Estadounidense: WOLA"](https://archivo.contagioradio.com/xenofobia-no-debe-tener-un-lugar-en-la-politica-estadounidense-wola/)

Las movilizaciones se presentaran en diferentes ciudades del país y se espera que participen miles de estudiantes y docentes, de igual forma es posible que otras **80 universidades se sumen a estos “Campus Santuarios” y las protestas estudiantiles.**

Esta iniciativa surge del programa de Acción Diferida o DACA, que pretende que estudiantes que vivieron toda su adolescencia en Estados Unidos y sigan en calidad de indocumentados, puedan permanecer en el país. Le puede interesar:["Donald Trump no es nuestro presidente: Latinos en USA"](https://archivo.contagioradio.com/trump-no-es-nuestro-presidente-latinos-en-usa/)

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)

<div class="ssba ssba-wrap">

</div>
