Title: ¿Por qué el 98% de los trabajadores del Cerrejón están dispuestos a iniciar una huelga?
Date: 2018-01-31 13:14
Category: Movilización, Nacional
Tags: Cerrejón, Huelga, Huelga de trabajadores, Sintracarbón
Slug: por-que-el-98-de-los-trabajadores-del-cerrejon-estan-dispuestos-a-iniciar-una-huelga
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/01/DU4K2a9XkAAdiFD.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Twitter Alberto Castilla] 

###### [31 Ene 2018] 

Los trabajadores de la empresa multinacional Carbones Cerrejón Limited realizaron el 29 de enero una votación para convocar a huelga o someter las diferencias con la empresa por medio de un tribunal de arbitramento. El **98.95% de los trabajadores votaron por iniciar una huelga** una vez se finalice el periodo de arreglo directo el 7 de febrero.

### **Protestan por mejores condiciones laborales y salariales** 

Los trabajadores han demostrado su preocupación por temas como la **tercerización laboral** que según la Central Unitaria de Trabajo CUT, afecta a más de 5.000 empleados de esa empresa. Por esto, el 29 de noviembre dieron inicio a las negociaciones colectivas y la ley supone un límite de 20 días de negociaciones directas.

Igor Díaz, dirigente nacional del sindicato de trabajadores de la industria del carbón, Sintracarbón, manifestó que esos 20 días fueron prorrogados por las partes “por 20 días más y convocamos a los trabajadores para llevar a cabo los escrutinios”. Ahora, “quedan **10 días para definir la hora cero** pero si no hay una opción por parte de la compañía Cerrejón para finalizar el conflicto, asumimos la decisión de empezar la huelga”.

### **Estos son algunos de los puntos por los que protestan los trabajadores** 

Según la CUT, la empresa “no ofrece mejores condiciones laborales para los trabajadores tercerizados, no plantea avances en temas de seguridad, salud y medio ambiente; y los incrementos de salario y bonos **no están acordes con el buen momento económico** y financiero por la que atraviesa la multinacional”. (Le puede interesar: ["Resguardo indígena gana pelea por la tierra al Cerrejón en la Guajira"](https://archivo.contagioradio.com/reguardo_nuevo_espinal_la_guajira_comunidades_el_cerrejon/))

Afirma que la tonelada de carbón se vende hoy en el mercado internacional por encima de los **90 dólares** que supone el doble de los costos no oficiales de producción y esto no ha significado ningún beneficio para los trabajadores. Por esto, Sintracarbón había propuesto un aumento del 12% en los salarios y la empresa se mantiene en contemplar un aumento sólo del 5.5%.

Esto lo reafirma Igor Díaz quien además añade que “en la historia laboral ha habido problemas en los temas de salud y accidentes laborales que **no son atendidos** ni por las entidades prestadoras de salud ni por las administradoras de riesgo”. Le han dicho a la empresa que, si hacen una minería muy riesgosa, debe haber condiciones básicas laborales. (Le puede interesar:["Cerrejón deberá suspender actividades mineras sobre arroyo Bruno"](https://archivo.contagioradio.com/cerrejon-debera-suspender-actividades-mineras-sobre-arroyo-bruno/))

También pusieron sobre la mesa el tema ambiental que afecta no sólo a quienes trabajan en la mina sino también a las **comunidades que se encuentran alrededor del complejo minero**. Así mismo, quienes se encuentran vinculados bajo el sistema de tercerización “trabajan más de 12 horas diarias, más de 20 días de trabajo y eso debe cambiar teniendo en cuenta que tienen altas sumas de ganancia que se debe reflejar no sólo en beneficios para los trabajadores directos”.

Finalmente, los trabajadores han mantenido su voluntad para seguir negociando, pero han dicho que la última palabra la tiene la empresa. Dicen que es **Cerrejón quien debe hacer el mayor esfuerzo** para lograr un acuerdo con los trabajadores. “Los trabajadores nos dieron el aval, si Cerrejón no cambia su posición iremos a la huelga con todas las consecuencias que esto genera”.

<iframe id="audio_23477886" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_23477886_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 

<div class="osd-sms-wrapper">

</div>
