Title: Comunidades de La Guajira siembran esperanza y construyen paz
Date: 2016-09-23 15:29
Category: Nacional, Paz
Tags: Cumbres de paz, FARC, La Guajira, proceso de paz. gobierno
Slug: comunidades-de-la-guajira-siembran-esperanza-y-construyen-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/01/cumbres-de-paz-poster.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Cumbre Agraria 

###### [23 Sep 2016] 

**Este 23 y 24 de septiembre,** La Guajira es sede de las Cumbres de Paz regionales que se vienen realizando este año en todo el país. Esta vez, las comunidades negras e indígenas se reunirán para discutir cuáles son temas claves para que la paz también pueda construirse desde las comunidades.

El encuentro tiene como fin impulsar un proceso de reactivación de dinámicas sociales para la consolidación de una **agenda conjunta de paz e iniciativas de convivencia para el posconflicto,** así mismo, se busca trazar rutas efectivas para hacer realidad la justicia socio-ambiental y equidad étnica para las comunidades de esta región del país.

En el caso de La Guajira, será importante identificar los conflictos territoriales y sus caminos de solución, así como la clarificación, **reflexión y difusión de los contenidos del acuerdo de paz entre el Gobierno y FARC. ** Finalmente se discutirá sobre seguimiento y cumplimiento de los acuerdos firmados con el gobierno regional y nacional en el marco de las pasadas movilizaciones de la Cumbre Agraria.

De acuerdo con Anastasio Espeleta, integrante de la Organización Wayúu Painwashi e integrante del Comité Político de la ONIC, lo que se espera es desarrollar propuestas que sirvan para corregir las problemáticas que atraviesa el que departamento en torno a la escases de agua, la minería que afecta a los territorios, la escases de alimentos y el abandono estatal.

“**Para nosotros la paz, se traduce en el bienestar, garantías para la pervivencia cultural y física, donde la minería a gran escala no afecte los modelos de vida de los wayúu.** Para nosotros la paz es que las políticas públicas no desequilibren los ejes fundamentales para la pervivencia de los pueblos, y sea reconocido y puesto en práctica nuestro goce de la autonomía”, explica Espeleta.

Esta novena versión de las Cumbres Regionales de Paz de la Cumbre Agraria **“Sembrando Esperanza, Cosechando País”** se ha realizado también en Tolima, Huila, Cundinamarca, Boyacá, Arauca, Meta, Vichada, Eje Cafetero y Caldas, Magdalena Medio, Sincelejo, Córdoba, Valledupar y continuará su rumbo a Cauca, Catatumbo, Choco, Nariño, para finalizar en la Cumbre Nacional de Paz a principios del mes de diciembre en la ciudad de Bogotá.

<iframe id="audio_13032675" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_13032675_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
