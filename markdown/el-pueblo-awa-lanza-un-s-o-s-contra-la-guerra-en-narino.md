Title: El Pueblo Awá lanza un S.O.S contra la guerra en Nariño
Date: 2020-10-01 19:24
Author: CtgAdm
Category: Actualidad, DDHH
Tags: #Awá, #Indígenas, #Nariño, #Vaki
Slug: el-pueblo-awa-lanza-un-s-o-s-contra-la-guerra-en-narino
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/10/WhatsApp-Image-2020-10-01-at-6.43.30-PM.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

La violencia que golpea al departamento de Nariño, sumada a la situación de pandemia, han generado una crisis humanitaria para el Pueblo Awá, que actualmente tiene 230 familias en confinamiento sin condiciones básicas para vivir. Por este motivo, esta comunidad ancestral lanzó una vaki, con la que pretenden recolectar más de **30 millones de pesos** que servirán para suplir estas necesidades y evitar la desaparición de estos saberes ancestrales.

<!-- /wp:paragraph -->

<!-- wp:heading -->

La Violencia está acabando con el Pueblo Awá
--------------------------------------------

<!-- /wp:heading -->

<!-- wp:paragraph -->

En lo corrido de este año, la Unidad Indígena del Pueblo Awá ha denunciado 40 hechos violentos provocados por grupos armados legales e ilegales. **Entre estos hechos se encuentran: asesinatos (17), masacres (3), amenazas (10), desplazamientos masivos (7), extorsiones (2), enfrentamientos entre grupos armados (2) y atentados (1). **

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Este tipo de situaciones han provocado distintas afectaciones psicosociales a mujeres, niños y niñas de la comunidad, que no han sido atendidas. Además, aunque no contabilizados, se tiene conocimiento de casos de violencia sexual, reclutamiento forzado y allanamientos.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Igualmente, es importante recordar que en el Capítulo Étnico, el Pueblo Awá está contemplado como sujeto colectivo de derechos priorizado, por lo tanto debe garantizarse su protección. (Le puede interesar: ["Pueblo Awa en alto riesgo por amenazas, desplazamientos y asesinatos"](https://archivo.contagioradio.com/pueblo-awa-en-alto-riesgo-por-amenazas-desplazamientos-y-asesinatos/))

<!-- /wp:paragraph -->

<!-- wp:heading -->

¿Cómo puedes ayudar en esta situación?
--------------------------------------

<!-- /wp:heading -->

<!-- wp:paragraph -->

Frente a este contexto, se abrió una vaki con la que la comunidad espera recolectar donaciones que servirán para la compra de paquetes alimentarios y enceres para estas familias. Asimismo, y debido a las precarias condiciones, se priorizarán alimentos de granos para cinco resguardos.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Si usted quiere unirse y aportar en esta vaki solo debe ingresar al siguiente link: [Atender crisis humanitaria de familias desplazadas \#SOSAwá](https://vaki.co/vaki/unipadonaton2020?fbclid=IwAR1F7VIQNhctttKMQnL0sIac5OomRDsrNKsessFl8wwImImjJzXFkdP-LdA). Finalmente, en un comunicado, el Pueblo Awá afirma que "es urgente llamar a la solidaridad para que 714 integrantes Awá, entre niñas, niños, mujeres y adultos mayores tengan alimentos para sobrevivir al desplazamiento o el confinamiento por la violencia".

<!-- /wp:paragraph -->
