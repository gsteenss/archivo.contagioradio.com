Title: Legalizan matrimonio igualitario en Estados Unidos
Date: 2015-07-01 10:59
Category: El mundo, LGBTI
Tags: Barack Obama, Comunidad LGBTI, Derechos Humanos, Estados Unidos, Love Wins, matrimonio igualitario, Nueva York, Orgullo Gay, Stonewall
Slug: estados-unidos-falla-a-favor-de-la-libertad-y-diversidad-sexual
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/06/image558f37624323a9.76859780.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

En el marco de la celebración del **día internacional del orgullo gay**, el tribunal supremo de **Estados Unidos** falló a favor del **matrimonio igualitario** en todo el país el pasado viernes, en ocasiones anteriores, el gobierno de **Barack Obama** se había pronunciado a favor de los derechos de la comunidad **LGBTI**, pero hasta el momento no se había dado este paso.

La votación se definió por cinco votos a favor y cuatro en contra, la corte decidió que “el matrimonio es un **derecho fundamental** basado en la libertad garantizada por la Constitución y negar a las parejas del mismo sexo el acceso a esta institución es un acto discriminatorio que viola su dignidad”.

En adelante por ningún motivo se podrá negar la posibilidad a parejas del mismo sexo a unirse en el vínculo matrimonial; el matrimonio se convirtió en un **derecho constitucional** desde el **26 de junio de 2015**, después de conocer el fallo, **Obama** se pronunció, "esta decisión fortalecerá todas las comunidades y ofrecerá a todas las parejas homosexuales la dignidad que merecen".

El fallo histórico causó revuelo en las redes sociales y reacciones de todo tipo, en su mayoría a favor de este cambio; organizaciones de todo el mundo celebraron el paso que dio **Estados Unidos** y fue un motivo más de conmemoración para el **28 de Junio**, después de los disturbios del **Stonewall en Nueva York en 1969**.

En Colombia la organización **Colombia Diversa**, se pronunció ante este fallo: “Desde Colombia Diversa celebramos esta decisión que pone fin a una injusticia y que beneficiará a las parejas del mismo sexo en los **Estados Unidos**, entre las cuales hay miles de colombianos y colombianas. En nuestro país, la **Corte Constitucional** deberá decidir definitivamente sobre el matrimonio igualitario y la invitamos a que siga el digno ejemplo de la Corte de los Estados Unidos y proteja definitivamente a las familias del mismo sexo”.

Con el numeral **\#LoveWins** gran parte del mundo apoyó la decisión del tribunal en las redes sociales e importantes lugares históricos se pintaron de los colores de la bandera gay.

\

[  
](https://archivo.contagioradio.com/estados-unidos-falla-a-favor-de-la-libertad-y-diversidad-sexual/ciemgnmw8aaazuq/)
