Title: Proyecto urbanístico "Lagos de Torca" no llena los requisitos para ser legal
Date: 2019-12-18 16:59
Author: CtgAdm
Category: Ambiente, Judicial
Tags: Ambiente, Bogotá, Lagos de Torca, Reserva Thomas Van der Hammen
Slug: proyecto-urbanistico-lagos-de-torca-no-llena-los-requisitos-para-ser-legal
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/RFvdH4-005.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/547584_1.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:@danielbernalb] 

Este 18 de diciembre se conoció la decisión de un juez en Bogotá, quien **dictó una medida de suspensión a los proyectos inmobiliarios promovidos por el alcalde Peñalosa dentro del proyecto Lagos de Torca**, vecino de la reserva Van Der Hammen. (Le puede interesar: [Peñalosa desconoce dos siglos de investigación sobre Van Der Hammen](https://archivo.contagioradio.com/penalosa-van-der-hammen-2/))

Medida que se logró tras la demanda presentada por  la Concejal María Fernanda Rojas el 7 de marzo de 2019, en contra de los decretos 088 de 2017  y 049 de 2018, que *e*stablecen las normas para el ámbito de aplicación del Plan de Ordenamiento Zonal del Norte, *“se argumentó que la inclusión de **esa franja de terreno dentro del proyecto Lagos de Torca viola una serie de normas y jurisprudencia, y no ha llenado los requisitos de ley para que pueda ser urbanizada**”, afirmó la Concejal. *

Las razones presentadas en el oficio fueron tres: infracción de las normas en que deberían fundarse, falsa motivación, y desviación de las atribuciones propias de quien los profirió; lo que significa que el Alcalde se extralimita en las funciones que puede ejercer y toma acciones sobre las leyes que competen a esta regulación.  (Le puede interesar: [POT de Peñalosa satisface intereses económicos y no las necesidades de Bogotá](https://archivo.contagioradio.com/pot-satisface-intereses-economicos/))

### Se suspenden solo 3 planes de los 30 del Proyecto Lagos de Torca

El proyecto tiene en total 1800 hectáreas donde vienen incluidos los cementerios, áreas protegidas como el humedal Torca- Guaymaral, la porción que conecta los cerros con la reserva Van Der Hammen y  el corredor urbano de la Autopista Norte, con una extensión de 86 hectáreas.

María Mercedes Maldonado, Ex-secretaria de Hábitat y Planeación de Bogotá y defensora del ambiente, afirmó que esto no es una suspensión total del proyecto pues solo se se detienen 3 de los 30 planes planteados en esta obra, y agregó que está de acuerdo con varias acciones de este proyecto, y que en colectivo ambiental no se oponen a la construcción de vías como la Suba- Cota, "Esta es una vía muy importante para la comunidad de Zorrillo y para los Colegios aledaños, en tanto se priorice el cumplimiento de  la ley, con estudios, licencias y diagnósticos ambientales, que justifiquen la compatibilidad y viabilidad de estos" agregó Maldonado.

> "Podemos decir que salvamos la reserva Van Der Hammen dando por hecho que la Alcaldesa electa está comprometida con implementar su plan ambiental" María Mercedes Maldonado

Maldonado también  afirmó, "Creemos que este es el último suelo de explanación que se realizará al norte de Bogotá, y confiamos en que la alcaldesa Claudia López hará un seguimiento riguroso a los planes en la reserva, así como los proyectos de expansión territorial".

Por último ante el anuncio de suspensión la Alcaldía actual aseguró que apelará la decisión del tribunal en los próximos 5 días; por su parte  la Concejal Rojas señaló: “Confío plenamente en la sustentación que hemos presentado, donde se demuestra que es ilegal construir en esos terrenos y que, de todas formas, no se han llenado los requisitos de ley para hacerlo”.

 

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe><iframe id="audio_45775432" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_45775432_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]

 
