Title: Familias de diputados del Valle les rinden homenaje a 17 años de su secuestro
Date: 2019-04-12 02:40
Author: CtgAdm
Category: Entrevistas, Memoria
Tags: conmemoración, diputados del valle, memoria, Valle del Cauca
Slug: familias-diputados-valle-rinden-homenaje
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/Diputados-del-Valle.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: @CentroMemoriaH] 

Este jueves se cumplieron 17 años del secuestro de los 12 diputados del Valle del Cauca por parte de las FARC, el hecho inició en 2002 en la sede de la Asamblea del departamento y culminó con el asesinato de 11 de ellos en 2007. La calle 9 de Cali fue renombrada como la Calle de Los Diputados, recordando la memoria de Héctor Arismendy, Carlos Barragán, Carlos Charry, Ramiro Echeverry,  Francisco Giraldo, Jairo Javier Hoyos, Juan Carlos Narváez, Nacianceno Orozco, Edison Pérez, Alberto Quintero y Rufino Varela.

Carolina Charry, hija de Carlos Charry, dijo que a pesar de que esta es una fecha muy dolorosa porque les recuerda "el inicio del drama", también es una oportunidad para ver con otros ojos los hechos, y el acto de renombrar la calle, enaltece la memoria de los familiares, rindiendo un homenaje a sus seres queridos y recordando que situaciones como estas no se deben repetir. (Le puede interesar:["El respeto a la diferencia debe ser el patrimonio de los colombianos: Gestores de paz del Valle"](https://archivo.contagioradio.com/la-personas-deben-poder-expresar-ideas-sin-temor-a-ser-asesinadas-red-de-gestores-de-paz/))

Charry señaló que las familias de los diputados están unidas en torno a la defensa del legado de sus familiares,  con "ganas de seguir luchando por el país y que estos hechos no se olviden"; por eso, este acto fue preparado hace más de un año por hijos y seres queridos de los funcionarios. También participaron en la apertura de un museograma, en el que trabajaron junto al Centro Nacional de Memoria Histórica, en el que se puede encontrar los perfiles de los diputados, y puede ser visitado por todos los colombianos en la capital del Valle.

### **"Queremos seguir apostando a la JEP y al Acuerdo de Paz"**

> Charry afirmó que los familiares de los diputados siempre han sido un grupo apegado a la institucionalidad y respetuosos de la misma, por eso, en este momento delicado en que constantemente se ataca el Sistema Integral de Verdad, Justicia, Reparación y No Repetición, quieren seguir apostando a la Jurisdicción Especial para la Paz (JEP) y al Acuerdo de Paz.

En días recientes los familiares de los diputados se reunieron con el procurador Fernando Carrillo, y allí él renovó el compromiso hecho hace dos años de acompañar los procesos que llevan en la justicia ordinaria, así como en la JEP, garantizando que se cumplan todos los derechos de las familias. (Le puede interesar: ["Comisión de la Verdad se convierte en una esperanza para organizaciones del Caribe"](https://archivo.contagioradio.com/comision-de-la-verdad-se-convierte-en-una-esperanza-para-organizaciones-del-caribe/))

La hija de Carlos Charry sobre estos procesos expresó que sabían como hecho certero que sus familiares no regresarán, razón por la que quieren una verdad  “exhaustiva, detallada, precisa, sin dilaciones donde podamos conocer de fondo qué pasó, quienes estuvieron comprometidos, si hubo complicidad del Estado en el hecho”, en conclusión, una verdad “con la que podamos sentirnos satisfechos”.

<iframe id="audio_34405823" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_34405823_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
