Title: Con este concurso se busca proteger el Bosque Bavaria
Date: 2019-10-29 12:27
Author: CtgAdm
Category: Ambiente
Tags: Ambiente, Bosque Bavaria, Pulmon de bogotá
Slug: con-este-concurso-se-busca-proteger-el-bosque-bavaria
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/EGZr3WDWsAAbaki.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:][@BosqueBav]

El Bosque Bavaria, ubicado en la antigua sede de la fábrica de cervecería Bavaria S.A, en la localidad de Kennedy en Bogotá, y amenazado con por planes de la alcaldía de Bogotá, que pretende erradicar **48.6 hectáreas de cobertura vegetal en una de las** z**onas con mayores índices de contaminación del aire,** para construir un proyecto llamado Plan Parcial de Renovación Urbana Bavaria Fábrica.

Para las comunidades que han tenido la posibilidad de conocerlo y que han construido una relación histórica y cultural con este ecosistema urbano, n**o cabe duda de que el Bosque presta un servicios ecosistemico  imprescindible para la ciudad**, convirtiéndose en el pulmón del suroccidente bogotano  por su función como invaluable sumidero de carbono, su habilidad como eficiente regulador de la temperatura y la radiación solar, su capacidad como productor de cerca de 1000 toneladas de oxígeno al año y su potencialidad para albergar, refugiar, alimentar y facilitar el tránsito de una diversidad nativa y migratoria de especies de avifauna.

Es por ello, que **reconociendo y exaltando su importancia como un cercado enigma verde que atrae a escritoras y escritores de lo cotidiano**, profesionales y aficionados en una amplia gama generacional, donde todas las voces y saberes son instados al dialogo y al mutuo reconocimiento como interlocutores válidos, es que el  proceso en defensa del Bosque Bavaria ha decidido coordinar la primera versión del Bosque Bavaria en 100 palabras.

Una iniciativa busca inspirar a todos y todas las participantes a escribir un relato breve sobre el Bosque Bavaria, fomentado de esta manera la producción escrita sobre este ecosistema urbano y patrimonio ecológico de la ciudad. (Le puede interesar: [Piden que el Bosque Bavaria sea reserva forestal protegida: Ambientalistas](https://archivo.contagioradio.com/piden-que-el-bosque-bavaria-sea-reserva-forestal-protegida-ambientalistas/))

Para conocer sobre los parámetros, tiempos y categorías de de este concurso pueden seguir el link [https://forms.gle/WsKThzjFQrfd6oBt5 ](https://docs.google.com/forms/d/e/1FAIpQLSe_gFo0E2GcsawoUFGklYVdWNNZmNdjg8xfbGEwXOmdKc4KFQ/viewform?usp=send_form) y así conocer más sobre este lugar e inspirarse con su diversidad.

 

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
