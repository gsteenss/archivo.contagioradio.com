Title: Mujeres en escena por la paz, 27 años por la reconciliación y la convivencia
Date: 2018-08-17 17:32
Category: Cultura
Tags: Festival Mujeres en escena por la paz, teatro, teatro la candelaria
Slug: festival-mujeres-en-escena-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/08/mujeres-en-escena-fest.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Corporación Colombiana de Teatro 

###### 16 Ago 2018 

Del 17 al 21 de agosto Bogotá vivirá un evento único en Colombia, de carácter popular y que durante 26 años se ha enfocado en la mujer como arte y parte de la realidad del país: el **Festival de Mujeres en Escena por la Paz**, que este año presenta como lema 'el papel de las mujeres en la reconciliación y la convivencia'.

El Festival presentará durante 5 días, obras de **Carolina Vivas, Susana Uribe, Patricia Ariza, Beatriz Monsalve, Fabiana Medina**, entre otras destacadas directoras de la escena distrital y nacional, en escenarios como **Quinta Porra, Sala Seki Sano, Casa Tea y Umbral Teatro** y no convencionales como **la Morada, Centro Cultural Casa Bolívar, Abya - Abyala y el Trueque**.

Como invitada internacional el evento contará con la participación de la dramaturga **Hye Ja Ju, de la República de Corea del Sur** presentando la obra “El diario de una joven llamada Ana Frank”, bajo la coordinación de Ramiro Antonio Sandoval, y la participación de 12 actores coreanos y 2 colombianos en escena.

<iframe src="https://player.vimeo.com/video/285011286" width="640" height="360" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

**Imperdibles de esta edición**

El viernes 17 de agosto, se realizará el **3er Encuentro Nacional Mujeres por la Paz,** “la paz con mirada de mujer”, con la participación de delegadas regionales y distritales que han venido trabajando desde hace 6 años tejiendo alianzas por la paz de Colombia.

El sábado 18 tendrá lugar el **Encuentro Polifónico Mujeres, Paz, Reconciliación y Convivencia**, de 2:00 a 6:00 pm; en una jornada de reflexión en torno a dos temas: El papel de las mujeres en la construcción de escenarios de paz y las mujeres y el territorio, con la participación de destacadas figuras femeninas.

Además, el Festival de Mujeres en escena por la Paz, se suma al movimiento poético mundial, **“Word Poetry Movement”** , lectura y canto poético de mujeres, con 3 días dedicados a la poesía, la música y el canto.

![Prog.](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/08/Prog.-720x458.jpg){.alignnone .size-medium .wp-image-55821 width="720" height="458"} ![Prog. 2](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/08/Prog.-2-720x458.jpg){.alignnone .size-medium .wp-image-55822 width="720" height="458"}
