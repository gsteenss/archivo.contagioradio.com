Title: El campesinado continúa luchando por la paz, la tierra y el buen vivir
Date: 2018-01-24 12:20
Category: Nacional, yoreporto
Tags: Fensuagro, zonas de reserva campesina
Slug: el-campesinado-continua-luchando-por-la-paz-la-tierra-y-el-buen-vivir
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/01/20836011058_e00a72f58a_o.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Congreso de los Pueblos 

###### 24 Ene 2017 

##### **Yo Reporto** 

Entre las montañas de la cordillera occidental y el cañón del río Cauca se encuentra el municipio de Suarez, al norte del departamento del Cauca. Territorio que alberga a campesinos, afros e indígenas que se encuentran organizados en consejos comunitarios, asociaciones campesinas y resguardos indígenas, un conjunto de comunidades culturales y étnicas que han vivido el abandono estatal desde décadas atrás y que desde sus expresiones organizativas le apuestan al buen vivir en armonía con la naturaleza.

En el corregimiento de La Betulia al occidente del municipio, fue el lugar escogido por parte la Asociación de Trabajadores Campesinos de la cordillera del municipio de Suarez ASOCORDILLERA, para la socialización de la figura de ordenamiento territorial ZONA DE RESERVA CAMPESINA -ZRC-, en cabeza del equipo técnico de la Coordinación Campesina de la Federación Nacional Sindical Unitaria Agropecuaria –FENSUAGRO- en el departamento del Cauca.

[La comunidad campesina organizada en ASOCORDILLERA busca implementar en su territorio la Zona de Reserva Campesina, figura reglamentada en la ley 160 de 1994 capítulo XIII y en el acuerdo de paz firmado con el Estado Colombiano y el ahora partido político de la paz Fuerza Alternativa Revolucionaria del Común -FARC-, en el punto 1, Reforma Rural Integral donde se tiene en cuenta las ZRC ya que “son iniciativas agrarias que contribuyen a la construcción de paz, a la garantía de los derechos políticos, económicos, sociales y culturales de los campesinos y campesinas.”]

[![reunion fensuagro](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/01/reunion-fensuagro-800x530.jpg){.alignnone .size-medium .wp-image-50813 width="800" height="530"}](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/01/reunion-fensuagro.jpg)

[La iniciativa de crear la ZRC en la cordillera de Suarez, surge a partir del abandono estatal que ha vivido históricamente el municipio y ante las diferentes situaciones que se originan en el territorio, como lo son las tensiones culturales y la minería depredadora que acaba con el territorio. Por ello, se busca consolidar la ZRC para superar estos problemas, implementar el acuerdo de paz en torno al desarrollo del campo y la paz territorial y sobre todo, superar las difíciles condiciones que han afectado a las comunidades.]

[Hoy son las campesinas y campesinos de Suarez que junto al campesinado caucano, siguen regando la semilla para cosechar vida, dignificar el territorio y generar buen vivir para las comunidades rurales: Las Zonas de Reserva Campesina avanzan a pesar de la falta de voluntad política del establecimiento!]

[23 de enero de 2018]

**Equipo Técnico FENSUAGRO Cauca**

------------------------------------------------------------------------

###### Yo reporto es una sesión de Contagio Radio en el que organizaciones y personas pueden publicar sus notas sobre diversos temas, entre ellos; movimiento social, derechos humanos, ambiente/ Las opiniones expresadas en este artículo son de exclusiva responsabilidad del autor y no necesariamente representan la opinión de Contagio Radio. 
