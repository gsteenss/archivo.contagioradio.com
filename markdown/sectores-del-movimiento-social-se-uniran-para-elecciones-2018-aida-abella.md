Title: La apuesta de la Unión Patriótica para la Unidad de cara a elecciones 2018
Date: 2017-07-13 17:48
Category: Entrevistas, Paz
Tags: Aída Abella, Unión Patriótica
Slug: sectores-del-movimiento-social-se-uniran-para-elecciones-2018-aida-abella
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/07/union-patriotica.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Prensa Rural] 

###### [13 Jul 2017] 

Diferentes sectores políticos y del movimiento social, se juntarían bajo una misma propuesta electoral para las votaciones del 2018, así lo manifestó la presidenta de la Unión Patriótica, Aida Avella quién considera que esta es una oportunidad para que **el país tenga un nuevo oxígeno no solo en las elecciones presidenciales sino también en Senado y Cámara.**

La propuesta de la Unión Patriótica, consistiría en generar una lista de 100 candidatos para el Senado y dependiendo de los cupos generar otra para representantes a la cámara, con **líderes sociales que tengan como principio apoyar la implementación de los acuerdos de paz y las garantías de los derechos de la ciudadanía, entre otros.** (Le puede interesar:["Se tene de nuevo la unidad de la izquierda en el congreso de la Unión Patriótica"](https://archivo.contagioradio.com/union-patriotica-unidad-izquierda/))

“Debemos dar un ejemplo en el país, el proceso de paz abre la perspectiva para que todos nos sentemos, no tenemos muchas diferencias, **es gente que se ha jugado mucho por Colombia, unos en el proceso de paz otros contra la corrupción**” afirmó Aida. Dentro de las personas que han sido convocadas a participar de esta iniciativa se encuentran Clara López, Piedad Córdoba, Jaime Caycedo, Claudia López.

De acuerdo con Avella, las personas que conformarán estas listas también deberán ser intachables moralmente y serán de distintas corrientes políticas, que representen la posibilidad de cambio para una **ciudadanía cansada de las formas de hacer política clientelista y corrupta**. (Le puede interesar:["La Unión Patriótica quiere demostrar que hay otra forma de hacer política"](https://archivo.contagioradio.com/la-union-patriotica-quiere-demostrar-que-hay-otra-forma-de-gobernar/))

Respecto a que se pueda dar una participación de los integrantes de las FARC-EP en estas listas, Avella aseguró que la posibilidad no está cerrada, sin embargo, señaló que esto dependería de las definiciones internas que tome esa organización al convertirse en partido político. **De igual forma otros sectores como el Polo Democrático, la plataforma MAIS y organizaciones del movimiento social, también están siendo convocados.**

<iframe id="audio_19790901" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_19790901_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
