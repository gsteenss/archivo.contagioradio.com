Title: Hay un plan para asesinar líderes en Chocó: Comisión de Justicia y Paz
Date: 2018-07-23 14:30
Category: DDHH, Movilización
Tags: asesinato de líderes sociales, Comisión Intereclesial de Justicia y Paz, Derechos Humanos, lideres sociales
Slug: plan-para-asesinar-lideres-en-choco
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/07/lideres.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [23 Jul 2018] 

La Comisión Intereclesial de Justicia y Paz denunció, a través de un comunicado de prensa, que **un grupo criminal estaría "diseñando un plan para asesinar o desterrar" a líderes y lideresas de restitución de tierras** de Curvaradó, Pedeguita, Mansilla y Tumaradó en el Bajo Atrato chocoano, pese a que hay una fuerte presencia de Fuerza Pública en los territorios.

Según información brindada por la organización, algunos de los líderes se encuentran en Zonas Humanitarias en las que hace cerca de dos semanas han visto "una intensa movilidad en motocicletas a altas horas de la noche, hostigando los reclamantes de predios colectivos". Además, han observado que "**civiles con armas cortas llegan a la casas, encienden focos de luz y golpean las puertas de las viviendas,** hablando fuertemente para intimidar a los líderes".

### **Tras sometimiento del Clan del Golfo se organizan nuevas estructuras armadas** 

El plan para asesinar líderes sociales lo conocieron algunos líderes por información que les dio uno de los sicarios, quien pidió reserva sobre su identidad.  Esta persona afirmó que las motocicletas patrullan en recorridos que van desde "Casa Negra" donde está ubicado alias 'Cristóbal', hasta los lugares de residencia de los pobladores y líderes del Consejo Comunitario de Pedeguita y Mansilla, en Tumaradó. También hay otra zona de patrullaje que va desde Llano Rico, hasta La Nevera en Curvaradó.

<div class="gdlr-blog-content">

De acuerdo con el testimonio de esta fuente confidencial, **los movimientos responden a la acogida del Clan del Golfo ante la nueva Ley de Sometimiento de Bandas Criminales;** que provocó la creación de estructuras armadas, creadas por empresarios, para impedir la restitución de tierras. (Le puede interesar: ["Se necesita reforma a Ley de Sometimiento para que entren otras bandas criminales"](https://archivo.contagioradio.com/se-necesita-reforma-a-ley-de-sometimiento-para-que-entren-nuevas-bandas-criminales/))

De igual forma, el sicario expresó que **"hay contactos con unidades militares y policiales para que los asesinatos sean perfectos"**, e indicó que esta es una fase inicial y no enfrentará problemas por parte del nuevo Gobierno.

### **El miedo se esparce por el Atrato ** 

Este plan para atentar contra los líderes sociales ha causado miedo entre los pobladores del Bajo Atrato en razón de las amenazas recibidas, luego de que denunciaran los niveles de relación entre la estructural ilegal de las Autodefensas Gaitanistas de Colombia y agentes del Estado, así como sus vínculos con el tráfico de drogas y el control territorial de ese grupo ilegal.

Aunque esta situación es conocida desde el año pasado, y se ha incrementado el pie de fuerza en la región, **la violencia estructural persiste por la "tolerancia y complicidad de la fuerza pública"** que permite la operación de las estructuras criminales en contra de las comunidades, como lo detalla el comunicado. (Le puede interesar: ["Militarización del Cauca no ha impedido asesinato de dos líderes sociales esta semana"](https://archivo.contagioradio.com/militarizacion-del-cauca-no-ha-impedido-asesinatos-de-dos-lideres-sociales-esta-semana/))

Por esas razones, la Comisión de Justicia y Paz pide al Gobierno colombiano que se adopten las medidas de protección efectivas para proteger la vida e integridad de las comunidades cercanas al Atrato; y que se asegure el trabajo de las instituciones encargadas de desarrollar las labores propias de un Estado Social de Derecho.

</div>

###### [Reciba toda la información de Contagio Radio en] [[su correo] [[Contagio Radio]
