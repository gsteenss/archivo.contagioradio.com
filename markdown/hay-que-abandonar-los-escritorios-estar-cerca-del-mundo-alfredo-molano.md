Title: Hay que abandonar los escritorios, estar cerca del mundo: Alfredo Molano
Date: 2016-11-04 12:50
Category: Nacional
Tags: Alfredo Molano, Molano, paz, Periodismo Molano, Periodistas, Premio Simon Bolivar, Premio Toda un Vida, sociologo
Slug: hay-que-abandonar-los-escritorios-estar-cerca-del-mundo-alfredo-molano
Status: published

###### [Foto:Contagio Radio] 

###### [4 Nov. de 2016] 

*"Señoras y señores, Antonia, escribir, vivir”, *de esta manera abrió su discurso **Alfredo Molano Bravo en la ceremonia de los premios Simón Bolívar celebrados en Bogotá.**

Allí, el sociólogo, periodista y escritor ganó el máximo galardón, el **premio a toda una vida y obra** en la que Molano ha plasmado a través de las letras, cientos de historias gestadas en los rincones de Colombia.

Cerca de 20 libros y un sin número de artículos en los que ha mostrado el conflicto armado que se incrustó en las entrañas de este país y que ha vivido por más de 50 años, fueron la clave para hacer merecedor a Molano del premio.

Alfredo, asegura que ha recibido este premio no solo por su trabajo sino por la época por la que pasa el país “**yo creo que los vientos están soplando para otro lado, quizás hace 5 años o una cosa así, no me hubieran dado ese premio**. Los vientos cambian”.

En su discurso, el periodista también aseguró que en la universidad su escritura se había vuelto acartonada y seca, no encontraba ni el tono ni el tema porque sus lectores eran profesores.

Pero agregó que por eso lo que hay que hacer es salirse de las aulas “yo a los sociólogos, a los politólogos, a los economistas o cualquier profesión pero sobre todo **a los periodistas, les digo que abandonen los escritorios, hay que salir, estar más cerca del mundo. Es simple, lo que pasa es que tiene precios y no solo económicos”.**

De igual manera, Molano afirmó que nunca realizó su trabajo buscando un premio, pero que lo agradece porque **“es un reconocimiento a la gente con la que yo trabajo, con la que yo hablo, que yo edito, digámoslo así.** En ese sentido me parece que el jurado miró más allá de los convencionalismos y de las posiciones políticamente correctas”.

El profesor Molano asegura que la sociedad civil está interesada en mirar ese otro país “que no está solo compuesto por insurgentes, sino por la gente, por los colonos, por los negros, por los indígenas, la gente marginada, por la gente desconocida, anónima".

Para Alfredo Molano, el mensaje que envía este premio a los periodistas es que **hay que volver a hacer el periodismo de salir a la calle y estar con las fuentes** “no es mirar el periodismo desde el internet, sino caminando, oyendo la gente, sudando con ella, sufriendo con ella, alegrándose con ella, es decir más cercana a la vida cotidiana, de esa llamada Colombia profunda” puntualizó.

Y concluye diciendo que pese a que se ha considerado como un “migrante” de los nuevos medios de comunicación en internet, **no desconoce la importancia que tienen estos para el periodismo, pero recalca que “aun así, es necesario darle ese tono al trabajo periodístico que se adquiere con la relación personal, con la gente”. **Le puede interesar: [“El éxito dependió de las enseñanzas aprendidas del pasado”: Alfredo Molano](https://archivo.contagioradio.com/el-exito-dependio-de-las-ensenanzas-aprendidas-del-pasado-alfredo-molano/)

<iframe id="audio_13616727" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_13616727_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en su correo [[bit.ly/1nvAO4u]
