Title: "Caso de Claudia Morales evidencia el miedo que tienen las víctimas a denunciar"
Date: 2018-01-26 13:24
Category: Mujer, Nacional
Tags: Alvaro Uribe, Claudia Morales, Violencia Sexual en colombia
Slug: caso_claudia_morales_alvaro_uribe
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/01/claudia_morales_gustavo_torrijos_0.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Gustavo Torrijos] 

###### [25 Ene 2018]

Muchas son las reacciones que ha generado la columna de la periodista Claudia Morales, en la que expuso su caso de violencia sexual por parte de un personaje poderoso. Personalidades del medio, han referido que el hombre al que se refiere la comunicadora podría ser Álvaro Uribe Vélez, Sin embargo, ¿Ha sido adecuado el manejo que se ha dado de este caso?

Sandra Mazo, integrante de la organización feminista Católicas por el Derecho a Decidir, admite que le parece problemático plantear que las víctimas tienen derecho a guardar silencio, **"ya que el silencio ha sido el recurso que ha usado el victimario para que no denuncien".** Además, reconoce que nunca le aconsejaría a una mujer resguardarse en ese derecho de no decir nada. Sin embargo, señala que esta situación evidencia lo que ha sido la impunidad en Colombia frente a estos hechos de violencia contra las mujeres.

"Estoy segura que ella quisiera denunciar a la persona que le hizo lo que le hizo, es un suceso que ha marcado su historia. Poner el tema en el país, la está poniendo en una situación de revictmización, pero también está poniendo el tema de la impunidad, **es ver el tema de cómo las víctimas se siente tan maniatadas para poder hablar y denunciar**", dice defensora de los derechos de las mujeres.

En una reciente entrevista que le Revista Semana le hizo a la periodista, Claudia reafirmó su derecho al silencio, y contó que la vicefiscal María Paulina Riveros la llamó para decirle que, cómo está consagrado en la Ley, las autoridades iniciarán las investigaciones pertinentes para hallar al agresor. No obstante, pocas expectativas se tienen, si no existe un plan integral que permita que no solo Claudia sino otras víctimas puedan acceder a la justicia, teniendo en cuenta que en el país **existe un 96% de impunidad en los casos de violencia sexual.**

En esa línea, Sandra Mazo, señala que el papel de la sociedad no es hacer del caso una noticia de "farándula", lo que se tiene que hacer es exigir un sistema de justicia y sanciones a los victimarios. "Deberíamos poner este caso de Claudia como ejemplerizante. El asunto es qué tan efectiva es o no la justicia de este país, qué está haciendo por acompañar a Claudia y conducir el proceso a que se sepa la verdad y se condene quien haya sido", dice.

### **¿Cómo debe ser la respuesta de un servidor público si es acusado?** 

Según Mazo, "lo que debe hacer la justicia es acercarse al caso tratando de buscar al delincuente, que es alguien con poder, que la tiene encerrada, con miedo y en la lupa y la mira de todo un país que está hablando de ella y no buscando quien fue el que le generó un daño irreparable", Precisamente, en medio de esa atmósfera que se ha tejido en la sociedad sobre quién fue esa persona que la agredió.

Para la feminista, el problema que ha generado la revictimización de la periodista, es que al ser una persona con poder **este tipo de victimarios "hacen que las víctimas se conviertan en victimarios y generar miedo para que la sociedad no hable, tanto que la sociedad está cayendo la defensa del silencio".**

En ese mismo sentido, las organizaciones feministas han tildado la respuesta del expresidente como un acto de revictmización.

> Omito comentar sobre el burdo ataque político,he sido decente con las mujeres a lo largo de mi vida.Nuestra oficina de prensa debe publicar viajes presidenciales en cuya comitiva estuvo la señora, nombre de las personas que estaban a cargo de la seguridad y sus obligaciones
>
> — Álvaro Uribe Vélez (@AlvaroUribeVel) [25 de enero de 2018](https://twitter.com/AlvaroUribeVel/status/956316053033963523?ref_src=twsrc%5Etfw)

<p style="text-align: justify;">
<script src="https://platform.twitter.com/widgets.js" async charset="utf-8"></script>
</p>
"Lo esperable de una persona que tiene un cargo público es lamentar y mostrar indignación por el abuso contra Claudia. **Una persona que tenga ética, que tenga principios,  que luche contra la impunidad y por los derechos y esté construyendo políticas debe despreciar el acto de la vulneración a Claudia e instar a las autoridades para que investiguen",** asimismo, manifiesta que Uribe debió mostrar toda la voluntad para que se desarrollen las investigaciones , si existe alguna duda sobre él, pero además **debió  demostrar su compromiso para luchar contra la violencia contra las mujeres.**

<iframe id="audio_23364893" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_23364893_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio]{.s1}](http://bit.ly/1ICYhVU)[.]{.s1}
