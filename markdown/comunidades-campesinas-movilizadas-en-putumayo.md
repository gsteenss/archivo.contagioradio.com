Title: Comunidades campesinas movilizadas en Putumayo son agredidas por la fuerza pública
Date: 2020-03-11 10:44
Author: AdminContagio
Category: yoreporto
Tags: ESMAD, PNIS, Putumayo, yo reporto
Slug: comunidades-campesinas-movilizadas-en-putumayo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/03/unnamed.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

Campesinos del corredor Puerto Vega Teteyé, en el municipio de Puerto Asís (Putumayo), denuncian que el **ESMAD** se encuentra agrediendo la movilización que se realiza por los graves incumplimientos del **PNIS** (Plan Nacional Integral de Sustitución) en la región.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Desde el pasado 15 febrero las comunidades han buscado a todas las autoridades locales y departamentales con el fin de ser escuchados y exigir que sean cumplidos los acuerdos de sustitución voluntaria de cultivos de uso ilícito en el marco del **PNIS** sin ningún resultado, razón por la cual las comunidades decidieran desde el 11 de marzo bloquear la circulación de camiones de las empresas petroleras.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":5} -->

##### “La movilización es la única forma de que nos escuchen, pues hemos buscado dialogar con todas las instituciones y autoridades sin ningún resultado” agregó líder que se encuentra en la región.

<!-- /wp:heading -->

<!-- wp:paragraph -->

Las comunidades le exigen al gobierno nacional una mesa de concertación y diálogo directamente en Putumayo y no en Bogotá como ha propuesto el Gobierno, las comunidades esperan que en esa mesa se le dé trámite a los incumplimientos reiterados a los acuerdos y se le dé continuidad a los programas de sustitución.  
Las personas que se encuentran en esta parte del departamento exigen que suspenda temporalmente la erradicación forzada, no se adelanten capturas y procesos de judicialización contra las y los campesinos que ejercen su derecho a la protesta y no se estigmatice la movilización y las comunidades.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Ver mas:[Yo Reporto](https://archivo.contagioradio.com/?s=yo+reporto)

<!-- /wp:paragraph -->
