Title: ¿Se pueden conciliar propuestas del No y acuerdos de paz?
Date: 2016-11-10 18:05
Category: Judicial, Paz
Tags: #AcuerdosYA, Acuerdos de paz de la Habana
Slug: se-pueden-conciliar-propuestas-del-no-y-acuerdos-de-paz
Status: published

###### [Foto: Mesa en la Habana] 

###### [10 de Nov 2016] 

Un grupo de organizaciones sociales y académicos del país se dieron a la tarea de redactar 21 recomendaciones jurídicas y políticas conciliadoras sobre los acuerdos de paz de La Habana y las propuestas de los sectores del No, con la finalidad de respetar la decisión del plebiscito y enfrentar las modificaciones que deban hacerse al acuerdo **sin pasar por encima de los avances en términos de derechos humanos, la centralidad de las víctimas, la reforma rural integral, la participación  ciudadana y enfoque de género.**

Frente al análisis de las propuestas del No que hicieron, los puntos que encontraron es que se dieron muchos rechazos, críticas pero pocas propuestas. Sobre las exigencias, Juan Carlos Ospina coordinador de incidencia nacional de la Comisión Colombiana de Juristas señalo que “**muchas de ellas son ajustes a los acuerdos para que de esa manera se de trámite a algunos de los planteamientos de los sectores del No**”, esto sin restringir las condiciones favorables que ya se encuentran en los acuerdos o que retrocedan en derechos humanos.

Sin embargo, Ospina es específico en señalar que las propuestas que son regresivas no se tomaron en cuenta para generar recomendaciones, **“las propuestas en las que se pueden hacer consensos son las que tengan vocación de generar medidas que aclaren el acuerdo  o robustezcan los acuerdos y sean viables”.**

Finalmente Juan Carlos considera que lo importante de estas recomendaciones es generar discusiones y acuerdos que eviten confusiones frente a **mitos que se han generado en el acuerdo de paz** y que afectan la implemetanción del mismo. Le puede interesar: ["Jurisdicción especial y víctimas: puntos neurálgicos en la Habana"](https://archivo.contagioradio.com/jurisdiccion-especial-y-victimas-puntos-neuralgicos-en-la-habana/)

La construcción del documento se dio a través de varias fases, la primera consistió en que las diferentes organizaciones que participaron de estas recomendaciones, dieron sus puntos de vista frente al acuerdo de paz, en un segundo momento elaboraron un mapa de las propuestas de los sectores del No y a partir de esto **definieron los elementos que son objeto de discusión, para posteriormente realizar las recomendaciones.**

###### Reciba toda la información de Contagio Radio en su correo [[bit.ly/1nvAO4u]
