Title: Polo Democrático todavía no tiene candidato
Date: 2016-12-12 16:36
Category: Nacional, Política
Tags: Candidato presidencial, Iván Cepeda, Jorge Robledo, Polo Democrático
Slug: robledo-no-es-el-candidato-del-polo-33604
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/12/Polo-contra-Peñalosa.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [12 Dic. 2016] 

Este fin de semana se llevó a cabo la Junta Nacional del Polo Democrático alternativo, en el que diversas posiciones se suscitaron en torno a temas como el de la elección del próximo candidato a la presidencia por ese partido.

Según el senador Iván Cepeda, **la mitad del partido, los cuales representan un cierto sector de tendencias que están agrupados como "Polo Paz", no estuvieron en dicha junta** “no estuvimos porque varios de los delegados que hacen parte de las minorías poblacionales y de la comunidad LGBTI fueron excluidos”.

El malestar se presentó luego de conocerse que varios delegados a la Junta Nacional resultaran apartados por una serie de trámites que se realizaron ante el Consejo Nacional Electoral “esa fue una circunstancia que nos hizo tener que abandonar la Junta Nacional” dijo Cepeda.

Para el senador de esta colectividad, ellos se encuentran totalmente identificados como una fuerza de oposición “nunca hemos renunciado a esa condición” y agrega **“lo que si no nos parece es que emerja una candidatura que se dice que es de convergencia pero excluye a una cantidad muy significativa de sectores”.**

En este punto Cepeda hace referencia a la candidatura del senador de esa misma colectividad, Jorge Robledo a las próximas elecciones presidenciales.

“Robledo ha dicho que no está dispuesto a ningún diálogo con sectores que representa Gustavo Petro, Piedad Córdoba, la Marcha Patriótica, que no está de acuerdo tampoco con explorar algún tipo de diálogos con el partido político que surja de las Farc, es prácticamente una candidatura unipersonal”.

Según Iván Cepeda **el sector que se ha denominado polo paz no se prestaría para esta candidatura “excluyente”** y asegura que no hay que “entregarse ni al Santismo, pero tampoco al uribismo, ni a los sectores de extrema derecha que quieren acabar con el proceso de paz”.

**“A la brava ni a las patadas vamos a apoyar ninguna candidatura, ni ningún congreso porque no es un proceso democrático”** aseveró Cepeda. A lo que también agregó que al estar dividida la colectividad sería bastante “miope” intentar tramitar las decisiones de una manera “brusca”.

Y concluye aseverando que **el senador Jorge Robledo no es el candidato del Polo Democrático “Robledo podrá ser el candidato del Moir y de otros sectores que lo respaldan y muy bien eso lo respetamos, pero no es el candidato del Polo.** Eso lo definiremos en un Congreso y no se ha realizado”.  Le puede interesar: [El Polo retoma política unitaria de izquierda](https://archivo.contagioradio.com/el-polo-cumple-10-anos-y-retoma-politica-unitaria-de-izquierda/)

<iframe id="audio_14905738" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_14905738_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio.](http://bit.ly/1ICYhVU)

 
